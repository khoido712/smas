﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Web;
using SMAS.Business.Common;
using System.Security.Cryptography;
using System.Text;
using System.Threading;
using System.Net;
using System.Net.Sockets;
using log4net;
using WCF.Composite;
using SMAS.VTUtils.Log;

namespace WCF
{
    public class WCFUtils
    {
        private static ILog _logger = LogManager.GetLogger(typeof(WCFUtils).Name);

        public static string GetConnectionString
        {
            get
            {
                return System.Web.Configuration.WebConfigurationManager.ConnectionStrings["OraAspNetConnectionString"].ToString();
            }
        }

        /// <summary>
        /// Copy value from source object to target with same properties
        /// </summary>
        /// <param name="sourceObject"></param>
        /// <param name="targetObject"></param>
        /// <param name="ExceptVirtual"></param>
        /// <param name="NullValue"></param>
        public static void CopyObject(object sourceObject, object targetObject)
        {
            Type f = sourceObject.GetType();
            PropertyInfo[] fproperties = f.GetProperties();

            Type m = targetObject.GetType();
            PropertyInfo[] mproperties = m.GetProperties();
            List<string> tmp = new List<string>();
            foreach (PropertyInfo p in mproperties)
            {
                tmp.Add(p.Name);
            }

            foreach (PropertyInfo p in fproperties)
            {
                if (tmp.Contains(p.Name))
                {
                    object value = p.GetValue(sourceObject, null);
                    if (value == null)
                    {
                        //Check kieu date time tra ve value = datetime.now
                        if (p.PropertyType == typeof(DateTime))
                        {
                            m.GetProperty(p.Name).SetValue(targetObject, DateTime.Now, null);
                        }
                        //Check kieu int tra ve gia tri 0
                        if (p.PropertyType == typeof(int?) || p.PropertyType == typeof(double?))
                        {
                            m.GetProperty(p.Name).SetValue(targetObject, 0, null);
                        }
                        //Check kieu double tra ve gia tri false
                        if (p.PropertyType == typeof(bool?))
                        {
                            m.GetProperty(p.Name).SetValue(targetObject, false, null);
                        }
                    }
                    m.GetProperty(p.Name).SetValue(targetObject, value, null);
                }
            }
        }

        /// <summary>
        /// Lấy thông tin buổi học
        /// </summary>
        /// <param name="Section"></param>
        /// <returns></returns>
        public static string GetSection(int Section)
        {
            if (Section == 1) return GlobalConstants.SECTION_MONING;
            if (Section == 2) return GlobalConstants.SECTION_AFTERNOON;
            if (Section == 3) return GlobalConstants.SECTION_EVENING;
            return string.Empty;
        }


        public static int CountMarkColHasValue(Subject_Mark m)
        {
            int count = 0;
            if (m.M1 != "")
                count++;
            if (m.M2 != "")
                count++;
            if (m.M3 != "")
                count++;
            if (m.M4 != "")
                count++;
            if (m.M5 != "")
                count++;
            if (m.P1 != "")
                count++;
            if (m.P2 != "")
                count++;
            if (m.P3 != "")
                count++;
            if (m.P4 != "")
                count++;
            if (m.P5 != "")
                count++;
            if (m.V1 != "")
                count++;
            if (m.V2 != "")
                count++;
            if (m.V3 != "")
                count++;
            if (m.V4 != "")
                count++;
            if (m.V5 != "")
                count++;
            if (count == 0) count = 1;
            return count;
        }


        public static decimal SumMarkColHasValue(Subject_Mark x)
        {
            //sua cho nay

            return GetNewDecimal(x.M1, 0) + GetNewDecimal(x.M2, 0) + GetNewDecimal(x.M3, 0) + GetNewDecimal(x.M4, 0) + GetNewDecimal(x.M5, 0) + GetNewDecimal(x.P1, 0) + GetNewDecimal(x.P2, 0) + GetNewDecimal(x.P3, 0) + GetNewDecimal(x.P4, 0) + GetNewDecimal(x.P5, 0) + GetNewDecimal(x.V1, 0) + GetNewDecimal(x.V2, 0) + GetNewDecimal(x.V3, 0) + GetNewDecimal(x.V4, 0) + GetNewDecimal(x.V5, 0);
        }

        public static decimal GetNewDecimal(object val, decimal def = 0m)
        {
            if (val != null && val.ToString() != "")
            {
                Utils.GetDecimal(val, def);
            }
            return 0m;
        }

        #region check authen pass cua WEBSERVICE duoc truyen tu bccs
        /// <summary>
        /// check authen pass cua WEBSERVICE duoc truyen tu bccs
        /// van dang su dung authen tren webservice BCCS MAIN
        /// </summary>
        /// <author date="2014/02/21">HaiVT</author>
        /// <param name="authenPass">pass xac thuc duoc truyen tu bcss</param>
        /// <returns></returns>
        public static string CheckAuthenPass(string authenPass)
        {
            //MD5CryptoServiceProvider myMD5 = new MD5CryptoServiceProvider();
            //byte[] authenPassArray = System.Text.Encoding.UTF8.GetBytes(authenPass);
            //authenPassArray = myMD5.ComputeHash(authenPassArray);
            //StringBuilder sb = new StringBuilder();
            //for (int i =0, size =authenPassArray.Length; i<size; i++)
            //{                
            //    sb.Append(authenPassArray[i].ToString("x").ToLower());
            //}

            //authenPass = sb.ToString();
            // todo: check authen pass trong db smas3 
            if (1 == 1)
            {
                return "1";
            }
            else
            {
                //return "9";
            }
        }
        #endregion

       
        
    }
}