﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WCF
{
    public class WCFConstant
    {
        public const int RESPONSE_SUCCESS = 1; // Tra ve ma thanh cong
        public const int RESPONSE_EXCEPTION = 0; // Tra ve ma exception
        public const int RESPONSE_SCHOOLID_NOT_FOUND = -1; // Tra ve ma khong tim thay ma truong
        public const int RESPONSE_ACADEMICYEARID_NOT_FOUND = -2; // Tra ve ma khong tim thay nam hoc
        public const int RESPONSE_CLASSID_NOT_FOUND = -3; // Tra ve ma khong tim thay lop
        public const int RESPONSE_APPLIED_LEVEL_NOT_FOUND = -5; // Tra ve ma khong tim thay cap hoc
        public const int RESPONSE_EMPLOYEEID_NOT_FOUND = -6; // Tra ve ma khong tim thay nhan vien
        public const int RESPONSE_PUPIL_PROFILEID_NOT_FOUND = -7; // Tra ve ma khong tim thay hoc sinh
        public const int RESPONSE_EDUCATION_LEVEL_NOT_FOUND = -8; // Tra ve ma khong tim thay khoi
        public const int RESPONSE_SUPERVISING_DEPT_NOT_FOUND = -9; // Tra ve ma khong tim thay phong ban
        public const int RESPONSE_SEMESTER_NOT_FOUND = -10; // Tra ve ma khong tim thay hoc ky
        public const int RESPONSE_PERIOD_NOT_FOUND = -10; // Tra ve ma khong tim thay dot

        public const int SCHOOL_PRIMARY_LEVEL = 1; // Cap tieu hoc
        public const int SCHOOL_JUNIOR_LEVEL = 2; // Cap 2
        public const int SCHOOL_HIGH_LEVEL = 3; // Cap 3
        public const int SCHOOL_KINDERGARTEN_LEVEL = 4; // Nha tre
        public const int SCHOOL_PRESCHOOL_LEVEL = 5; // Mau giao

        public const string LEVEL_EXPERT = "(Giỏi)"; // Hoc luc gioi
        public const string LEVEL_GOOD = "(Khá)"; // Hoc luc kha
        public const string LEVEL_NORMAL = "(TB)"; // Hoc luc trung binh
        public const string LEVEL_WEAK = "(Yếu)"; // Hoc luc yeu
        public const string LEVEL_FOOL = "(Kém)"; // Hoc luc kem

        public const string GRADE_PRIMARY = "Cấp 1";
        public const string GRADE_SECONDARY = "Cấp 2";
        public const string GRADE_HIGHTSCHOOL = "Cấp 3";
        public const string GRADE_CHILDRENT_1ST = "Nhà trẻ";
        public const string GRADE_CHILDRENT_2ST = "Mẫu giáo"; 
		
		public const int MIN_PRIMARY = 1;
        public const int MAX_PRIMARY = 5;
        public const int MIN_SECONDARY = 6;
        public const int MAX_SECONDARY = 9;
        public const int MIN_TERTIARY = 10;
        public const int MAX_TERTIARY = 12;
        public const int MIN_KINDERGARTEN = 13;
        public const int MAX_KINDERGARTEN = 15;
        public const int MIN_PRESCHOOL = 16;
        public const int MAX_PRESCHOOL = 18;
		
		 /// <summary>
        /// Dang hoc
        /// </summary>
        public const int PUPIL_STATUS_STUDYING = 1;
        /// <summary>
        /// da tot nghiep
        /// </summary>
        public const int PUPIL_STATUS_GRADUATED = 2;
        /// <summary>
        /// da chuyen truong
        /// </summary>
        public const int PUPIL_STATUS_MOVED_TO_OTHER_SCHOOL = 3;
        /// <summary>
        /// da thoi hoc
        /// </summary>
        public const int PUPIL_STATUS_LEAVED_OFF = 4;
        /// <summary>
        /// da chuyen lop
        /// </summary>
        public const int PUPIL_STATUS_MOVED_TO_OTHER_CLASS = 5;

        /// <summary>
        /// format string.Format "{0}{1}"
        /// </summary>
        public const string FORMAT_0_1 = "{0}{1}";

        public const string COURSE_COMPLETE = "Hoàn thành CT lớp {0}";
        public const string NOT_COURSE_COMPLETE = "Chưa hoàn thành CT lớp {0}";

        public const string EDUCATION_LEVEL_COMPLETE = "Hoàn thành chương trình lớp {0} ";
        public const string EDUCATION_LEVEL_NOT_COMPLETE = "Chưa hoàn thành chương trình lớp {0} ";

        public const string EMIS_FOLDER_FIRST_ROOT = "DAUNAM/";
        public const string EMIS_FOLDER_MIDDLE_ROOT = "GIUANAM/";
        public const string EMIS_FOLDER_END_ROOT = "CUOINAM/";

        public const string EMIS_FOLDER_FIRST_P = "HoSo_TH_T9";
        public const string EMIS_FOLDER_FIRST_S = "HoSo_THCS_T9";
        public const string EMIS_FOLDER_FIRST_T = "HoSo_THPT_T9";
        public const string EMIS_FOLDER_FIRST_PS = "HoSo_PTCS_T9";
        public const string EMIS_FOLDER_FIRST_ST = "HoSo_PTTH_T9";
        public const string EMIS_FOLDER_FIRST_PST = "HoSo_PT_T9";
        public const string EMIS_FOLDER_FIRST_GDTX = "HoSo_TTGDTX_T9";

        public const string EMIS_FOLDER_MIDDLE_P = "HoSo_TieuHoc_T12";
        public const string EMIS_FOLDER_MIDDLE_S = "HoSo_THCS_T12";
        public const string EMIS_FOLDER_MIDDLE_T = "HoSo_THPT_T12";
        public const string EMIS_FOLDER_MIDDLE_PS = "HoSo_PTCS_T12";
        public const string EMIS_FOLDER_MIDDLE_ST = "HoSo_PTTH_T12";
        public const string EMIS_FOLDER_MIDDLE_PST = "HoSo_PT_T12";
        public const string EMIS_FOLDER_MIDDLE_GDTX = "HoSo_TTGDTX_T12";

        public const string EMIS_FOLDER_END_P = "HoSo_TH_T5";
        public const string EMIS_FOLDER_END_S = "HoSo_THCS_T5";
        public const string EMIS_FOLDER_END_T = "HoSo_THPT_T5";
        public const string EMIS_FOLDER_END_PS = "HoSo_PTCS_T5";
        public const string EMIS_FOLDER_END_ST = "HoSo_PTTH_T5";
        public const string EMIS_FOLDER_END_PST = "HoSo_PT_T5";
        public const string EMIS_FOLDER_END_GDTX = "HoSo_TTGDTX_T5";

        /// <summary>
        /// dau xuyet trai
        /// </summary>
        public const char CHAR_SLASH = '/';

        /// <summary>
        /// Truong GDTX
        /// </summary>
        public const int EMIS_TRAINING_TYPE_GDTX = 3;

        /// <summary>
        /// Cap 1
        /// </summary>
        public const int EMIS_EDUCATION_P = 1;

        /// <summary>
        /// cap 2
        /// </summary>
        public const int EMIS_EDUCATION_S = 2;

        /// <summary>
        /// cap 3
        /// </summary>
        public const int EMIS_EDUCATION_T = 4;

        /// <summary>
        /// cap 1,2
        /// </summary>
        public const int EMIS_EDUCATION_PS = 3;

        /// <summary>
        /// cap 2,3
        /// </summary>
        public const int EMIS_EDUCATION_ST = 6;

        /// <summary>
        /// cap 1,2,3
        /// </summary>
        public const int EMIS_EDUCATION_PST = 7;

        public const int PERROD_FIRST = 1;
        public const int PERROD_MIDDLE = 2;
        public const int PERROD_END = 3;


    }
}