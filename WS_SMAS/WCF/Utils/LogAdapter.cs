﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using System.Web;
using Oracle.DataAccess.Client;
using SMAS.Models.Models;

namespace WCF
{
    public class LogAdapter
    {
        //public const string Query = "Insert into ACTION_AUDIT (USER_ID,ROLE_ID,GROUP_ID,CONTROLLER,ACTION,PARAMETER,BEGIN_AUDIT_TIME,END_AUDIT_TIME,IP,REFERER,USER_AGENT,OBJECT_ID,OLD_OBJECT_VALUE,NEW_OBJECT_VALUE,DESCRIPTION,USER_FUNCTION,USER_ACTION,USER_DESCRIPTION,PARTITION_MONTH,EXECUTED_DURATION,STATUS_REQUEST) values (:1,:2,:3,:4,:5,:6,:7,:8,:9,:10,:11,:12,:13,:14,:15,:16,:17,:18,:19,:20,:21)";
        public const string Query = "Insert into ACTION_AUDIT (USER_ID,CONTROLLER,ACTION,PARAMETER,BEGIN_AUDIT_TIME,END_AUDIT_TIME,IP,REFERER,OBJECT_ID,OLD_OBJECT_VALUE,NEW_OBJECT_VALUE,DESCRIPTION,USER_FUNCTION,USER_ACTION,USER_DESCRIPTION,LAST_DIGIT_NUMBER_SCHOOL,EXECUTED_DURATION,STATUS_REQUEST) values (:1,:2,:3,:4,:5,:6,:7,:8,:9,:10,:11,:12,:13,:14,:15,:16,:17,:18)";
        private static List<List<object>> ListData;
        public static List<OracleDbType> ListParam = new List<OracleDbType>()
        {
            OracleDbType.Int32,
            OracleDbType.NVarchar2,
            OracleDbType.NVarchar2,
            OracleDbType.NVarchar2,
            OracleDbType.TimeStamp,
            OracleDbType.TimeStamp,
            OracleDbType.NVarchar2,
            OracleDbType.NVarchar2,
            OracleDbType.Long,
            OracleDbType.NVarchar2,
            OracleDbType.NVarchar2,
            OracleDbType.NVarchar2,
            OracleDbType.NVarchar2,
            OracleDbType.NVarchar2,
            OracleDbType.NVarchar2,
            OracleDbType.Int32,
            OracleDbType.Int32,
            OracleDbType.Int32
        };
        private static int GetMaxBulk()
        {
            return Int32.Parse(ConfigurationManager.AppSettings["MaxBulkLog"]);
        }

        public static void Insert(ActionAudit ActionAudit)
        {
            try
            {
                if (ListData == null)
                {
                    ListData = new List<List<object>>();
                    for (int i = 0; i < ListParam.Count; i++)
                    {
                        ListData.Add(new List<object>());
                    }
                }
                List<object> data = new List<object>()
            {
                ActionAudit.UserID,
                ActionAudit.Controller,
                ActionAudit.Action,
                ActionAudit.Parameter,
                ActionAudit.BeginAuditTime,
                ActionAudit.EndAuditTime,
                ActionAudit.IP,
                ActionAudit.Referer,
                ActionAudit.ObjectId,
                ActionAudit.oldObjectValue,
                ActionAudit.newObjectValue,
                ActionAudit.Description,
                ActionAudit.userFunction,
                ActionAudit.userAction,
                ActionAudit.userDescription,
                ActionAudit.LastDigitNumberSchool,
                ActionAudit.ExecutedDuration,
                ActionAudit.StatusRequest
            };
                for (int i = 0; i < ListParam.Count; i++)
                {
                    ListData[i].Add(ConvertDataSqlToOracle(ListParam[i], data[i]));
                }
                if (ListData[0].Count >= GetMaxBulk())
                {
                    PushToDatabase();
                }
            }
            catch
            {
                ListData = null;
            }

        }

        private static void PushToDatabase()
        {
            List<List<object>> list = new List<List<object>>();
            lock (ListData)
            {
                list = ListData.ToList();
                ListData = new List<List<object>>();
                for (int i = 0; i < ListParam.Count; i++)
                {
                    ListData.Add(new List<object>());
                }
            }
            Task.Factory.StartNew(() => new LoggingThread(list).Log());
        }

        static object ConvertDataSqlToOracle(OracleDbType OracleDbType, object value)
        {
            try
            {
                object res = value;
                if (value != null)
                {
                    switch (OracleDbType)
                    {
                        case OracleDbType.Raw:
                            if (value is Guid)
                            {
                                res = ((Guid)value).ToByteArray();
                            }
                            break;
                        case OracleDbType.Byte:
                        case OracleDbType.Int16:
                        case OracleDbType.Int32:
                        case OracleDbType.Int64:
                        case OracleDbType.Long:
                            if (value is bool)
                            {
                                res = (bool)value ? 1 : 0;
                            }
                            else
                            {
                                res = long.Parse(value.ToString().Split('.')[0]);
                            }
                            break;
                        case OracleDbType.Decimal:
                        case OracleDbType.Double:
                            if (value is bool)
                            {
                                res = (bool)value ? 1 : 0;
                            }
                            else
                            {
                                res = Double.Parse(value.ToString());
                            }
                            break;
                        case OracleDbType.Date:
                        case OracleDbType.TimeStamp:
                            if (value is TimeSpan)
                            {
                                res = new DateTime(1900, 1, 1, ((TimeSpan)value).Hours,
                                    ((TimeSpan)value).Minutes, ((TimeSpan)value).Milliseconds);
                            }
                            else
                            {
                                res = value;
                            }
                            break;
                        case OracleDbType.Clob:
                        case OracleDbType.NChar:
                        case OracleDbType.NClob:
                        case OracleDbType.NVarchar2:
                        case OracleDbType.Varchar2:
                        case OracleDbType.Char:
                            res = value.ToString() == "" ? " " : value.ToString();
                            break;
                    }
                }
                else
                {
                    if (OracleDbType == OracleDbType.Clob
                        || OracleDbType == OracleDbType.NChar
                        || OracleDbType == OracleDbType.NClob
                        || OracleDbType == OracleDbType.NVarchar2
                        || OracleDbType == OracleDbType.Varchar2
                        || OracleDbType == OracleDbType.Char)
                    {
                        res = " ";
                    }
                }
                return res;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("Không thể convert dữ liệu ({0}) sang kiểu {1}", value, OracleDbType.ToString()));
            }
        }
    }

    public class LoggingThread
    {

        List<List<object>> ListData;

        public LoggingThread(List<List<object>> List)
        {
            ListData = List.ToList();
        }

        public void Log()
        {
            try
            {
                OracleConnection conn = new OracleConnection(ConfigurationManager.AppSettings["OraAspNetConnectionString"]);
                Thread.Sleep(1000);
                conn.Open();
                try
                {
                    OracleCommand cmd = new OracleCommand(LogAdapter.Query, conn);
                    cmd.CommandTimeout = 0;
                    for (int i = 0; i < LogAdapter.ListParam.Count; i++)
                    {
                        OracleParameter param = new OracleParameter();
                        param.OracleDbType = LogAdapter.ListParam[i];
                        param.Value = ListData[i].ToArray();
                        cmd.Parameters.Add(param);
                    }
                    cmd.ArrayBindCount = ListData[0].Count;
                    cmd.ExecuteNonQuery();
                }
                catch (Exception ex1)
                {
                }
                finally
                {
                    conn.Close();
                }
            }
            catch (Exception ex2)
            {
            }
        }
    }
}