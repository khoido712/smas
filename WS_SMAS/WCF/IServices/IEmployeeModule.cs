﻿using SMAS.Business.BusinessObject;
using SMAS.Business.IBusiness;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;
using WCF.Composite;
using WCF.Composite.Sync;

namespace WCF
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the interface name "IEmployeeModule" in both code and config file together.
    [ServiceContract]
    public partial interface IEmployeeModule
    {
        /// <summary>
        /// lấy giáo viên chủ nhiệm của lớp theo ưu tiên, 
        /// nếu có tồn tại giáo viên làm thay trong thời gian hiện tại thì trả về làm thay, 
        /// ngược lại lấy giao viên chủ nhiệm trong khai báo lớp học
        /// </summary>
        /// <param name="ClassID"></param>
        /// <param name="AcademicYearID"></param>
        /// <param name="SchoolID"></param>
        /// <returns>neu khong ton tai return 0</returns>
        [OperationContract]
        IntResultResponse GetHeadTeacherID(int ClassID, int AcademicYearID, int SchoolID);

        /// <summary>
        /// Lay danh sach GVCN tu danh sach ma lop truyen sang
        /// </summary>
        /// <param name="LstClassID"></param>
        /// <param name="AcademicYearID"></param>
        /// <param name="SchoolID"></param>
        /// <returns></returns>
        [OperationContract]
        ListEmployeeLiteResponse GetHeadTeacherByClassIDs(List<int> LstClassID, int AcademicYearID, int SchoolID);

        /// <summary>
        /// Lấy avatar của giáo viên theo ID
        /// </summary>
        /// <param name="TeacherID"></param>
        /// <returns>neu khong ton tai return null</returns>
        [OperationContract]
        ByteArrayResultResponse GetTeacherImage(int TeacherID);

        /// <summary>
        /// Lấy danh sách giáo viên theo dk tìm kiếm nằm trong lstEmployeeID + SchoolID 
        /// </summary>
        /// <param name="TeacherRequest"></param>
        /// <returns>Neu khong ton tai return new object</returns>
        [OperationContract]
        ListEmployeeLiteResponse GetListTeacherByCondition(TeacherRequest ObjTeacherRequest);

        /// <summary>
        /// Lấy giáo viên theo ID
        /// </summary>
        /// <param name="TeacherID"></param>
        /// <returns>Neu khong ton tai return new object</returns>
        [OperationContract]
        EmployeeFullInfoResponse GetTeacherByID(int TeacherID);

        /// <summary>
        /// Lấy nhân viên phòng/sở theo ID
        /// </summary>
        /// <param name="EmployeeID"></param>
        /// <returns>Neu khong ton tai return new object</returns>
        [OperationContract]
        EmployeeFullInfoResponse GetEmployeeByID(int EmployeeID);

        /// <summary>
        /// Danh sách lấy tất cả các giáo viên đang làm việc của trường
        /// </summary>
        /// <param name="SchoolID"></param>
        /// <param name="Status"></param>
        /// <returns>Neu khong ton tai return new object 0 phan tu</returns>
        [OperationContract]
        ListEmployeeFullInfoResponse GetAllTeacherInSchool(int SchoolID, int Status);

        /// <summary>
        /// Lấy danh sách giáo viên theo danh sách ID truyền vào
        /// </summary>
        /// <param name="lstEmplyeeID"></param>
        /// <returns>Neu khong ton tai return new object 0 phan tu</returns>
        [OperationContract]
        ListEmployeeResponse GetListTeacher(List<int> lstEmplyeeID);

        #region Lấy danh sách giáo viên dang lam viec theo danh sách ID truyền vào
        /// <summary>
        /// Lấy danh sách giáo viên dang lam viec theo danh sách ID truyền vào
        /// </summary>
        /// <param name="lstEmplyeeID"></param>
        /// <returns>Neu khong ton tai return new object 0 phan tu</returns>
        [OperationContract]
        ListEmployeeResponse GetListTeacherWorking(List<int> lstEmplyeeID);
        #endregion

        /// <summary>
        /// Lấy danh sách tẩt cả giáo viên đang làm việc trong trường
        /// </summary>
        /// <param name="ObjTeacherOfSchool"></param>
        /// <returns>Neu khong ton tai return new object 0 phan tu</returns>
        [OperationContract]
        ListEmployeeResponse GetListTeacherOfSchool(TeacherOfSchoolRequest ObjTeacherOfSchool);

        #region AnhVD9 20151006 - Lấy danh sách GV toàn trường - Chuyển sang dạng list chuỗi
        /// <summary>
        /// Lấy danh sách GV theo dạng chuỗi - Giảm data trả về
        /// </summary>
        /// <param name="ObjTeacherOfSchool"></param>
        /// <returns></returns>
        [OperationContract]
        ListEmployeeResponse GetTeachersOfSchoolformatString(TeacherOfSchoolRequest ObjTeacherOfSchool);
        #endregion

        /// <summary>
        /// đếm số lượng giáo viên đang làm việc trong trường
        /// </summary>
        /// <param name="ObjTeacherOfSchool"></param>
        /// <returns>Neu khong ton tai return 0</returns>
        [OperationContract]
        IntResultResponse CountTeacherOfSchool(int schoolID, int academicYearID);

        /// <summary>
        /// Lấy ds giáo viên của trường theo khối học, theo list ID giáo viên truyền vào
        /// </summary>
        /// <param name="AcademicYear"></param>
        /// <param name="AppliedLevel"></param>
        /// <param name="lstTeacherID"></param>
        /// <returns>Neu khong ton tai return new object 0 phan tu</returns>
        [OperationContract]
        ListEmployeeLiteResponse GetListTeacherByAppliedLevel(int AcademicYearID, int AppliedLevel, List<int> lstTeacherID);

        /// <summary>
        /// Lấy danh sách username theo list employeeID truyền vào
        /// </summary>
        /// <param name="objTeacherRequest"></param>
        /// <returns></returns>
        [OperationContract]
        ListAccountByTeacherResponse GetListUsernameByEmployeeID(TeacherRequest objTeacherRequest);
        /// <summary>
        /// lấy danh sách giáo viên bộ môn của lớp
        /// </summary>
        /// <param name="academicYearID"></param>
        /// <param name="classID"></param>
        /// <param name="semesterID"></param>
        /// <returns></returns>
        [OperationContract]
        ListEmployeeLiteResponse GetListTeachingAssign(int academicYearID, int classID, int semesterID);
		
		 /// <summary>
        /// Lay tat car useraccount khong quan tam den trang thai isactive
        /// </summary>
        /// <param name="objTeacherRequest"></param>
        /// <returns></returns>
        [OperationContract]
        ListAccountByTeacherResponse GetListFullUsernameByEmployeeID(TeacherRequest objTeacherRequest);
        /// <summary>
        /// lay danh sach giao vien chu nhiem theo khoi
        /// </summary>
        /// <param name="EducationLevelID"></param>
        /// <param name="AcademicYearID"></param>
        /// <param name="SchoolID"></param>
        /// <returns></returns>
        [OperationContract]
        ListHeadTeacherByClassReponse GetListHeadTeacherByEducationLevelID(int EducationLevelID, int AcademicYearID, int SchoolID);

        [OperationContract]
        ListWorkGroupTypeResponse GetListWorkGroupType();

        [OperationContract]
        ListEmployeeResponse GetListTeacherOfSchoolForContactGroup(TeacherOfSchoolRequest ObjTeacherOfSchool);

       

        [OperationContract]
        ListEmployeeResponse GetListTeacherOfSchoolForReport(TeacherOfSchoolRequest ObjTeacherOfSchool);

        [OperationContract]
        ListEmployeeResponse GetListTeacherWithTeachingSubject(int SchoolID, int AcademicYearID, int AppliedLevel, int EducationLevel, int ClassID, int SubjectID, int Semester);

    }
}
