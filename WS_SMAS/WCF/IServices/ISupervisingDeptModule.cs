﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;
using WCF.Composite;

namespace WCF
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the interface name "ISupervisingDeptModule" in both code and config file together.
    [ServiceContract]
    public interface ISupervisingDeptModule
    {
        /// <summary>
        /// Lấy danh sách trường học theo cấp quản lý và danh sách ID trường
        /// </summary>
        /// <param name="SupervisingDeptID"></param>
        /// <param name="SchoolIDList"></param>
        /// <returns></returns>
        [OperationContract]
        ListSchoolProfileLiteResponse GetListSchoolByListID(int SupervisingDeptID, List<int> SchoolIDList);

        /// <summary>
        /// Lay ID phong thuoc quan ly cua don vi dau vao
        /// </summary>
        /// <param name="SupervisingDeptID"></param>
        /// <returns></returns>
        [OperationContract]
        ListIntResultResponse GetListSupervisingDeptIDByID(int SupervisingDeptID);
        /// <summary>
        /// ham lay thong tin nhan vien va ten phong ban quan ly cua nhan vien do
        /// </summary>
        /// <param name="objEmployeeRequest"></param>
        /// <returns></returns>
        [OperationContract]
        ListEmployeeSupervisingDeptReponse GetlistEmployeeByListID(List<EmployeeRequest> objEmployeeRequest);
        /// <summary>
        /// Lấy thông tin nhân viên phòng sở theo List ID truyền vào
        /// </summary>
        /// <param name="EmployeeIDList"></param>
        /// <returns></returns>
        [OperationContract]
        ListEmployeeSupervisingDeptReponse GetListEmployeeNameByListEmployeeID(List<int> EmployeeIDList);

        /// <summary>
        /// Lay danh sach can bo theo dieu kien dau vao - khong co phan trang tung phan
        /// </summary>
        /// <author date="2014/02/18">HaiVT</author>
        /// <param name="SuperisingDeptID">don vi phong so</param>
        /// <returns></returns>
        [OperationContract]
        ListEmployeeSupervisingDeptReponse GetEmployeeBySuperVisingDeptID(int SupervisingDeptID, bool SupervisingRole, bool SupervisingSubRole);

        /// <summary>
        /// Lấy thông tin nhà trường trực thuộc đơn vị - có phân trang từng phần
        /// </summary>
        /// <param name="EducationGrade"></param>
        /// <param name="SuperVisingDeptID"></param>
        /// <param name="SchoolName"></param>
        /// <param name="PageNum"></param>
        /// <param name="PageSize"></param>
        /// <returns></returns>
        [OperationContract]
        ListSchoolProfileLiteByRegionResponse GetSchoolByRegion(int EducationGrade, int SuperVisingDeptID, string SchoolName
            , int PageNum, int PageSize);

        /// <summary>
        /// Lay thong tin cap truc thuoc don vi
        /// </summary>
        /// <param name="SuperVisingDeptID"></param>
        /// <returns></returns>
        [OperationContract]
        ListSupervisingDeptLiteResponse GetListSuperVisingDeptByID(int SuperVisingDeptID);

        /// <summary>
        /// Lay danh sach can bo theo dieu kien dau vao - Co phan trang tung phan
        /// </summary>
        /// <param name="CurrentEmployeeID"></param>
        /// <param name="SearchContent"></param>
        /// <param name="SuperisingDeptID"></param>
        /// <param name="PageNum"></param>
        /// <param name="PageSize"></param>
        /// <returns></returns>
        [OperationContract]
        ListEmployeeSupervisingDeptPaginateReponse GetEmployeeBySuperVisingDept(int CurrentEmployeeID, string SearchContent, int SupervisingDeptID
            , int PageNum, int PageSize);

        #region Tìm kiếm tât cả nhân viên thuộc đơn vị kể cả nhân viên của đơn vị con.
        /// <summary>
        /// Tìm kiếm tât cả nhân viên thuộc đơn vị kể cả nhân viên của đơn vị con.
        /// </summary>
        /// <author date="2014/02/20">HaiVT</author>
        /// <param name="CurrentEmployeeID">dk where khac voi currentEmployeeID</param>
        /// <param name="SearchContent">noi dung tim kiem</param>
        /// <param name="SupervisingDeptID">id phong so</param>
        /// <returns></returns>
        [OperationContract]
        ListIntResultResponse GetEmployeeBySuperVisingDeptNoPaging(int CurrentEmployeeID, string SearchContent, int SupervisingDeptID);
        #endregion

        #region lấy SuperVisingDeptID là đơn vị trực thuộc phòng sở thì lấy ra phòng/sở tương ứng
        /// <summary>
        /// lấy SuperVisingDeptID là đơn vị trực thuộc phòng sở thì lấy ra phòng/sở tương ứng
        /// </summary>
        /// <param name="superVisingDeptID">id don vi</param>
        /// <returns></returns>
        [OperationContract]
        SupervisingDeptResponse GetParentBySupervisingDeptID(int superVisingDeptID);
        #endregion

        
       /// <summary>
        /// Lấy thông tin nhà trường trực thuộc đơn vị -không phân trang từng phần
        /// </summary>
        /// <param name="EducationGrade"></param>
        /// <param name="SuperVisingDeptID"></param>
        /// <param name="SchoolName"></param>
        /// <param name="PageNum"></param>
        /// <param name="PageSize"></param>
        /// <returns></returns>
        [OperationContract]
        ListSchoolProfileLiteResponse GetSchoolByRegionNotPaging(int EducationGrade, int SupervisingDeptID, string SchoolName);

        #region Dành cho CTV/ĐL
        [OperationContract]
        LstEducationUnitResponse GetAllEducationUnit(int ProvinceID, int DistrictID);

        [OperationContract]
        LstEducationUnitResponse GetAllEducationUnitPaging(int ProvinceID, int DistrictID, int page, int size);

        [OperationContract]
        bool ActiveSMAS(string userName);
        #endregion
    }
}
