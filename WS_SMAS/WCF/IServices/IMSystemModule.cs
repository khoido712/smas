﻿using SMAS.Models.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;
using WCF.Composite;
using WCF.Composite.Migrate;
using WCF.Composite.Sync;

namespace WCF.Services
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the interface name "IMSystemModule" in both code and config file together.
    [ServiceContract]
    public interface IMSystemModule
    {
        [OperationContract]
        #region Migrate DL tu he thong cua Hue

        /// <summary>
        /// Them moi danh sach truong
        /// </summary>
        /// <param name="lstSchool"></param>
        /// <returns></returns>
        BoolResultResponse SchoolInsert(List<MSchoolProfileDefault> lstSchoolInput, int provinceId, int year);

        [OperationContract]
        MSchoolProfile GetSchoolProfile(int provinceId, int appliedLevelId);

        [OperationContract]
        BoolResultResponse UpdateSchool(List<MSchoolProfileDefault> lstSchool);

        /// <summary>
        /// Lay danh sach nam hoc theo ID truong
        /// </summary>
        /// <param name="lstSchoolId"></param>
        /// <param name="year"></param>
        /// <returns></returns>
        [OperationContract]
        MAcademicYearResponse GetAcademicYear(List<int> lstSchoolId, int year);

        [OperationContract]
        MAcademicYearResponse GetAcademicYearbyProvince(int provinceId, int year, int synchronizeId, int node, int lengthNode);




        #region Danh muc
        [OperationContract]
        MSubjectCat GetSubject(int appliedLevelId);


        [OperationContract]
        /// <summary>
        /// Lay danh sach quan huyen theo tinh thanh
        /// </summary>
        /// <param name="provinceId"></param>
        /// <returns></returns>
        List<MDistrict> GetDistrict(int provinceId);

        [OperationContract]
        List<MSupervisingDept> GetSupersingDept(int provinceId);

        [OperationContract]
        /// <summary>
        /// Danh dan toc
        /// </summary>
        /// <returns></returns>
        List<MEthnic> GetEthnic();
        [OperationContract]
        List<MWorkType> GetWorkType();

        #endregion

        #region Lop hoc

        /// <summary>
        /// Danh sach mon hoc
        /// </summary>
        /// <param name="academicYearId"></param>
        /// <returns></returns>
        [OperationContract]
        MClassProfile GetClassProfile(int academicYearId);

        [OperationContract]
        BoolResultResponse ClassInsert(List<MClassProfileDafault> lstClassProfile);

        [OperationContract]
        MSchoolSubject GetSchoolSubject(int academicYearId, int schoolId);

        [OperationContract]
        BoolResultResponse InsertSchoolSubject(List<MSchoolSubjectDefault> lstSchoolSubject);

        [OperationContract]
        MClassSubject GetClassSubject(int academicYearId, int schoolId);

        [OperationContract]
        BoolResultResponse InsertClassSubject(List<MClassSubjectDefault> lstClassSubject);
        #endregion


        #endregion

        [OperationContract]
        bool UpdateHasPassword(string userName, int pagesize);


        #region Dong bo du lieu cho HCM
        /// <summary>
        /// Lay danh sach truong chua dong bo
        /// </summary>
        /// <param name="form"></param>
        /// <returns></returns>
        [OperationContract]
        TransSchoolProfileResponse GetSyncSchoolProfile(SyncAcademicYear form);

        [OperationContract]
        BoolResultResponse UpdateSyncSchool(List<int> lstSchool, string sourceDB);

        [OperationContract]
        TransSubjectCatResponse GetSyncSubjectCat(SyncAcademicYear form);

        [OperationContract]
        BoolResultResponse UpdateSyncSubjectCat(List<int> lstSubjectCat);


        [OperationContract]
        TransClassProfileResponse GetSyncClassProfile(SyncAcademicYear form);

        [OperationContract]
        BoolResultResponse UpdateSyncClassProfile(List<int> lstClass, string sourceDB);


        [OperationContract]
        TransClassSubjectResponse GetSyncClassSubject(SyncAcademicYear form);

        [OperationContract]
        BoolResultResponse UpdateSyncClassSubject(List<long> lstClassSubject, SyncAcademicYear form);


        [OperationContract]
        /// <summary>
        /// Lay danh sach truong theo tinh thanh, nam
        /// </summary>
        /// <param name="form"></param>
        /// <returns></returns>
        TransAcademicYearResponse GetSyncAcademicYear(SyncAcademicYear form);


        #endregion

        [OperationContract]
        TableContract GetAll(string sqlquery, string tableName);

        [OperationContract]
        bool ExcuteSql(List<string> lstSql);
    }
}
