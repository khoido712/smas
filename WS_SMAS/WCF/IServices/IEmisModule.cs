﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.ServiceModel.Web;
using System.Text;
using WCF.Composite.EMIS;
using WCF.Request;

namespace WCF.IServices
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the interface name "IEmisModule" in both code and config file together.
    [ServiceContract]
    public interface IEmisModule
    {
        [OperationContract]
        [WebInvoke(Method = "POST", UriTemplate = "GetValue", ResponseFormat = WebMessageFormat.Json)]
        EmisComposite GetEmisValue(EmisAPIRequest request);
    }
}
