﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;
using WCF.Composite;
using WCF.Composite.Migrate;
using WCF.Composite.Sync;

namespace WCF.Services
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the interface name "IMPupilProfileModule" in both code and config file together.
    [ServiceContract]
    public interface IMPupilProfileModule
    {
        #region sync Hue

       

        [OperationContract]
        MPupilProfile GetPupilProfile(int academicYearId, int SchoolId);

        [OperationContract]
        MPupilProfile GetPupilProfilebyId(int academicYearId, int SchoolId, List<int> lstSoucePupilId);

        [OperationContract]
        BoolResultResponse InsertPupilFile(List<MPupilProfileDefault> lstPupilFile, int academicYearId, int SchoolId, int year);

        [OperationContract]
        BoolResultResponse UpdatePupilFile(List<MPupilProfileDefault> lstPupilFile, int academicYearId, int SchoolId, int year);


        [OperationContract]
        MMarkRecord GetMarkRecord(List<int> lstPupilId, int academicYearId, int schoolId, List<int> lstSubjectId, List<int> lstSemesterId);

        [OperationContract]
        BoolResultResponse InsertMarkRecord(List<MMarkRecordDefault> lstMarkRecord, int academicYearId, int schoolId);

        [OperationContract]
        BoolResultResponse InsertJudgeRecord(List<MJudgeRecordDefault> lstJudgeRecord, int academicYearId, int schoolId);

        [OperationContract]
        BoolResultResponse InsertSummedRecord(List<MSummedRecordDefault> lstSummedRecord, int academicYearId, int schoolId);

        [OperationContract]
        MPupilRanking GetPupilRaning(List<int> lstPupilId, int academicYearId, int schoolId, List<int> lstSemesterId);

        [OperationContract]
        BoolResultResponse InsertPupilRanking(List<MPupilRankingDefault> lstPupilRanking, int academicYearId, int schoolId);

        [OperationContract]
        BoolResultResponse UpdatePupilRanking(List<MPupilRankingDefault> lstPupilRanking, int academicYearId, int schoolId);

        #endregion


        #region Sync HCM
        [OperationContract]
        /// <summary>
        /// Lay danh sach hoc sinh chua dong bo
        /// </summary>
        /// <param name="form"></param>
        /// <returns></returns>
        TransPupilProfileResponse GetSyncPupilFile(SyncAcademicYear form);

        /// <summary>
        /// Cap nhat tinh trang dong bo file
        /// </summary>
        /// <param name="lstPupilFileId"></param>
        /// <param name="form"></param>
        /// <returns></returns>
        [OperationContract]
        BoolResultResponse UpdateSyncPupilFile(List<int> lstPupilFileId, SyncAcademicYear form);

        [OperationContract]
        TransMarkRecordResponse GetSyncMarkRecord(SyncAcademicYear form);

        [OperationContract]
        BoolResultResponse UpdateSyncMarkRecord(List<long> lstMark, SyncAcademicYear form);


        [OperationContract]
        TransJudgeRecordResponse GetSyncJudgekRecord(SyncAcademicYear form);

        [OperationContract]
        BoolResultResponse UpdateSyncjudgeRecord(List<long> lstMark, SyncAcademicYear form);


        [OperationContract]
        TransPupilRankingResponse GetSyncPupilRanking(SyncAcademicYear form);

        [OperationContract]
        BoolResultResponse UpdateSyncPupilRanking(List<long> lstRanking, SyncAcademicYear form);


        [OperationContract]
        TransEvaluationCommentsResponse GetSyncEvaluationComments(SyncAcademicYear form);

        [OperationContract]
        BoolResultResponse UpdateSyncEvaluationComments(List<long> lstEvaluationComments, SyncAcademicYear form);


        /// <summary>
        /// Lay thong tin diem kiem tra cuoi ky cap 1
        /// </summary>
        /// <param name="form"></param>
        /// <returns></returns>
        [OperationContract]
        TransSummedEvaluationResponse GetSyncSummedEvaluation(SyncAcademicYear form);

        /// <summary>
        /// Cap nhat thong tin  diem kiem tra cuoi ky cap 1
        /// </summary>
        /// <param name="lstEvaluationComments"></param>
        /// <param name="form"></param>
        /// <returns></returns>
        [OperationContract]
        BoolResultResponse UpdateSyncSummedEvaluation(List<long> lstSummedEvaluation, SyncAcademicYear form);


        /// <summary>
        /// Lay thong tin danh gia cuoi ky cap 1
        /// </summary>
        /// <param name="form"></param>
        /// <returns></returns>
        [OperationContract]
        TransSummedEndingEvaluationResponse GetSyncSummedEnding(SyncAcademicYear form);

        /// <summary>
        /// Cap nhat thong tin danh gia cuoi ky cap 1
        /// </summary>
        /// <param name="lstEvaluationComments"></param>
        /// <param name="form"></param>
        /// <returns></returns>
        [OperationContract]
        BoolResultResponse UpdateSyncSummedEnding(List<long> lstSummedEndingId, SyncAcademicYear form);

        #endregion
    }
}
