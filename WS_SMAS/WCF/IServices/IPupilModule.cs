﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;
using WCF.Composite;
using WCF.Composite.Sync;

namespace WCF
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the interface name "IPupilModule" in both code and config file together.
    [ServiceContract()]
    public interface IPupilModule
    {
        #region For Thread
        /// <summary>
        /// Them moi diem
        /// </summary>
        /// <param name="lstMarkRecordComposite"></param>
        /// <param name="objCollection"></param>
        /// <returns></returns>
        [OperationContract]
        bool InsertMarkRecord(List<MarkRecordComposite> lstMarkRecordComposite, CollectionComposite objCollection);

        /// <summary>
        /// Tu dong tien hanh tong ket diem 
        /// </summary>
        /// <param name="UserID"></param>
        /// <param name="lstPupilID"></param>
        /// <param name="Semester"></param>
        /// <param name="PeriodID"></param>
        /// <param name="SchoolID"></param>
        /// <param name="AcademicYearID"></param>
        /// <param name="ClassID"></param>
        /// <param name="SubjectID"></param>
        [OperationContract]
        string AutoSummupRecord(int UserID, List<int> lstPupilID, int Semester, int? PeriodID, int SchoolID, int AcademicYearID, int ClassID, int SubjectID);

        ///// <summary>
        ///// Tổng kết điểm tự động
        ///// </summary>
        ///// <param name="lstPupilID"></param>
        ///// <param name="SchoolID"></param>
        ///// <param name="AcademicYearID"></param>
        ///// <param name="ClassID"></param>
        ///// <param name="Semester"></param>
        ///// <param name="PeriodID"></param>
        ///// <returns></returns>
        [OperationContract]
        string AutoMarkSummary(List<int> lstPupilID, int SchoolID, int AcademicYearID, int ClassID, int Semester, int? PeriodID);

        ///// <summary>
        ///// Xếp hạng tự động
        ///// </summary>
        ///// <param name="lstPupilID"></param>
        ///// <param name="SchoolID"></param>
        ///// <param name="AcademicYearID"></param>
        ///// <param name="ClassID"></param>
        ///// <param name="Semester"></param>
        ///// <param name="PeriodID"></param>
        ///// <returns></returns>
        [OperationContract]
        string AutoRankingPupil(List<int> lstPupilID, int SchoolID, int AcademicYearID, int ClassID, int Semester, int? PeriodID);

        ///// <summary>
        ///// Xếp loại tự động
        ///// </summary>
        ///// <param name="lstPupilID"></param>
        ///// <param name="SchoolID"></param>
        ///// <param name="AcademicYearID"></param>
        ///// <param name="ClassID"></param>
        ///// <param name="Semester"></param>
        ///// <param name="PeriodID"></param>
        ///// <returns></returns>
        [OperationContract]
        string AutoClassifyingPupil(List<int> lstPupilID, int SchoolID, int AcademicYearID, int ClassID, int Semester);

        #endregion

        #region For SMSEdu

        [OperationContract]
        GridTableResponse GetPupilWithPaging(int page, int size, int academicYearId , int schoolId, List<int> educationLevels, int? classId, string code, string fullname, List<int> notInPupilIds, List<int> forceInPupilIds, List<int> searchInPupilIds   );

        /// <summary>
        /// Lấy avatar của hoc sinh theo ID
        /// </summary>
        /// <param name="PupilProfileID"></param>
        /// <returns></returns>
        [OperationContract]
        ByteArrayResultResponse GetPupilImage(int PupilProfileID);

        /// <summary>
        /// lấy học sinh theo ID 
        /// </summary>
        /// <param name="PupilProfileID"></param>
        /// <returns></returns>
        [OperationContract]
        PupilProfileLiteResponse GetPupilProfileByID(int PupilProfileID);

        /// <summary>
        /// lấy học sinh theo ID + year
        /// </summary>
        /// <param name="PupilProfileID"></param>
        /// <returns></returns>
        [OperationContract]
        PupilProfileLiteResponse GetPupilProfileByYear(int PupilProfileID, int Year);

        /// lấy học sinh theo danh sach ID + year
        /// </summary>
        /// <param name="PupilProfileID"></param>
        /// <returns></returns>
        [OperationContract]
        List<PupilProfileLite> GetPupilsByYear(List<int> PupilIDs, int Year);

        /// <summary>
        /// lấy học sinh theo PupilCode
        /// </summary>
        /// <author date="2014/02/18">HaiVT</author>
        /// <param name="schoolCode">ma truong</param>
        /// <param name="pupilCode">ma hoc sinh</param>
        /// <returns></returns>
        [OperationContract]
        PupilProfileLiteResponse GetPupilProfileByPupilCode(int schoolID, string pupilCode);


        /// <summary>
        /// lấy học sinh theo PupilCode
        /// </summary>
        /// <author date="2014/02/18">HaiVT</author>
        /// <param name="schoolCode">ma truong</param>
        /// <param name="pupilCode">ma hoc sinh</param>
        /// <returns></returns>
        [OperationContract]
        string GetPupilInfoByPupilCode(string schoolCode, string pupilCode);

        /// <summary>
        /// lấy danh sách các lớp đã học của học sinh
        /// </summary>
        /// <param name="schoolID"></param>
        /// <param name="academicYearID"></param>
        /// <param name="educationGrade"></param>
        /// <returns></returns>
        [OperationContract]
        ListPupilProfileLiteResponse GetListPupilOfClass(int PupilProfileID);

        /// <summary>
        /// Lấy danh sách học sinh theo dk tìm kiếm nằm trong 
        /// ListPupilProfileID + SchoolID + academicYearID
        /// </summary>
        /// <param name="objPupilProfile"></param>
        /// <returns></returns>
        [OperationContract]
        ListPupilProfileResponse GetListPupilByCondition(PupilProfileRequest objPupilProfile);

        /// <summary>
        /// lấy ds điểm đợt của học sinh trong lớp.Content format:
        /// Toán:<TBM>
        /// Thể dục:<HLM>
        /// Trung bình các môn:<8.0>
        /// Học lực:<Khá>
        /// Hạnh kiểm:<Tốt>
        /// Xếp hạng:<1>
        /// </summary>
        /// <param name="SchoolID">ID trường</param>
        /// <param name="ClassID">ID lớp học</param>
        /// <param name="Year">Năm học</param>
        /// <param name="PeriodID">ID đợt</param>
        /// <returns></returns>
        [OperationContract]
        ListMarkOfPeriodResponse GetListMarkRecordTime(int SchoolID, int ClassID, int AcademicYearID, int PeriodID);

        /// <summary>
        /// điểm của học sinh trong lớp.Content format:
        /// Cấp 1:Toan:7(học lực) Thể dục:Đ,CĐ
        /// Hạnh kiểm:Đ
        /// Xếp loại giáo dục:Khá
        /// Nghỉ có phép:1
        /// Nghỉ không phép:1
        /// Cấp 2,3:Toán:<TBM> Thể dục<HLM>
        /// Trung bình các môn:<8,0>
        /// Học lực:<Khá>
        /// Hạnh kiểm:<Tốt>
        /// Xếp hạng:<1>
        /// Danh hiệu thi đua:<xuất sắc>>
        /// </summary>
        /// <param name="SchoolID">ID trường</param>
        /// <param name="ClassID">ID lớp học</param>
        /// <param name="Year">Năm học</param>
        /// <param name="Semester">học kỳ</param>
        /// <param name="EducationLevel">Cấp học (TH, THCS, THPT)</param>
        /// <returns></returns>
        [OperationContract]
        ListMarkOfSemesterResponse GetListMarkRecordSemester(int SchoolID, int ClassID,
            int AcademicYearID, int Semester);

        /// <summary>
        /// Cấp 1:Toán T1,2,3 GK T4,5 CK
        /// Thể dục NX1,2,3,4,5
        /// Cấp 2,3:Toán M P V KTHK 
        /// Thể Dục M P V KTHK
        /// - nghi co phep:2
        /// - nghi khong phep:1
        /// </summary>
        /// <param name="SchoolID"></param>
        /// <param name="ClassID"></param>
        /// <param name="EducationLevel"></param>
        /// <param name="Year"></param>
        /// <param name="DateStart"></param>
        /// <param name="DateEnd"></param>
        /// <param name="CheckAbsent"></param>
        /// <returns></returns>
        [OperationContract]
        ListMarkRecordResponse GetMarkRecordByClass(int SchoolID, int ClassID, int EducationLevel,
            int AcademicYearID, DateTime DateStart, DateTime DateEnd, bool CheckAbsent, int Semester);

        /// <summary>
        /// Lấy thông tin học sinh trong lớp học
        /// </summary>
        /// <param name="SchoolID"></param>
        /// <param name="AcademicYearID"></param>
        /// <param name="ClassID"></param>
        /// <returns></returns>
        [OperationContract]
        ListPupilProfileLiteResponse GetListPupilByClass(int SchoolID, int AcademicYearID, int ClassID);
        /// <summary>
        /// hàm lấy TBmôn,HL,Hạnh kiểm của học sinh
        /// </summary>
        /// <param name="PupilProfileID"></param>
        /// <param name="AcademicYearID"></param>
        /// <param name="ClassID"></param>
        /// <returns></returns>
        [OperationContract]
        ListMarkRecordResponse GetListMarkRecordOfPupil(int PupilProfileID, int AcademicYearID, int ClassID);

        /// <summary>
        /// Lấy danh sách học sinh theo điều kiện truyền vào
        /// </summary>
        /// <param name="SchoolID"></param>
        /// <param name="AcademicYearID"></param>
        /// <param name="Grade"></param>
        /// <param name="EducationLevelID"></param>
        /// <param name="ClassID"></param>
        /// <returns></returns>
        [OperationContract]
        ListPupilProfileLiteResponse GetListPupil(int SchoolID, int AcademicYearID, int EducationLevelID, int ClassID, int GradeID = 0);

        [OperationContract]
        ListPupilResponseFormatString GetListPupilformatString(int SchoolID, int AcademicYearID, int EducationLevelID, int ClassID, int GradeID = 0);
        [OperationContract]
        List<int> GetListPupilID(int SchoolID, int AcademicYearID, int EducationLevelID, int ClassID, int GradeID = 0);

        [OperationContract]
        ListPupilProfileLiteResponse GetListPupilByName(int SchoolID, int AcademicYearID, int? ClassID, int Grade, string nameOfPupil);

        /// <summary>
        /// Lấy danh sách học sinh theo điều kiện truyền vào
        /// </summary>
        /// <param name="SchoolID"></param>
        /// <param name="AcademicYearID"></param>
        /// <param name="EducationLevelID"></param>
        /// <returns></returns>
        [OperationContract]
        ListPupilProfileLiteResponse GetListPupilBySchoolID(int SchoolID, int AcademicYearID, int? EducationLevelID, int grade);

        /// <summary>
        /// Lay danh sach hoc sinh theo truong co phan trang
        /// </summary>
        /// <param name="SchoolID"></param>
        /// <param name="AcademicYearID"></param>
        /// <param name="ClassID"></param>
        /// <param name="PupilCode"></param>
        /// <param name="Pupilname"></param>
        /// <param name="EducationLevelID"></param>
        /// <param name="grade"></param>
        /// <param name="Page"></param>
        /// <param name="NumPage"></param>
        /// <param name="totalRecord"></param>
        /// <returns></returns>
        [OperationContract]
        ListPupilProfileLiteResponse GetListPupilBySchoolIDPaging(int SchoolID, int AcademicYearID, int ClassID, string PupilCode, string Pupilname, int? EducationLevelID, int grade, int Page, int NumPage, bool isRegisContract, int teacherID);

        [OperationContract]
        ListPupilResponseFormatString GetPupilsbySchoolIDPagingformatString(int SchoolID, int AcademicYearID, int ClassID, string PupilCode, string Pupilname, int? EducationLevelID, int grade, int Page, int NumPage, bool isRegisContract, int teacherID);
        /// <summary>
        /// Lấy danh sách học sinh theo lớp
        /// </summary>
        /// <param name="SchoolID"></param>
        /// <param name="AcademicYearID"></param>
        /// <param name="ClassID"></param>
        /// <returns></returns>
        [OperationContract]
        ListPupilProfileLiteResponse GetListPupilByClassID(int SchoolID, int AcademicYearID, int? ClassID);

        /// <summary>
        /// Lấy thông tin rèn luyện của học sinh
        /// </summary>
        /// <param name="SchoolID"></param>
        /// <param name="AcademicYearID"></param>
        /// <param name="ClassID"></param>
        /// <returns></returns>
        [OperationContract]
        ListPupilTrainingResponse GetListTraningInfoToday(int SchoolID, int AcademicYearID, int ClassID);

        #region lay thong tin chuyen can tuan/thang/mo rong cho hoc sinh
        /// <summary>
        /// lay thong tin chuyen can tuan/thang/mo rong cho hoc sinh
        /// </summary>
        /// <author date="14/04/10">HaiVT</author>
        /// <param name="SchoolID">id truong</param>
        /// <param name="AcademicYearID">id nam hoc</param>
        /// <param name="ClassID">id lop hoc</param>
        /// <param name="startTime">thoi gian bat dau</param>
        /// <param name="endTime">thoi gian ket thuc</param>
        /// <returns></returns>
        [OperationContract]
        ListPupilTrainingResponse GetListTraningInfoByTime(int SchoolID, int AcademicYearID, int ClassID, DateTime startTime, DateTime endTime);
        #endregion

        #region lay thong tin vi pham, nghi hoc, mien giam cua hoc sinh
        /// <summary>
        /// lay thong tin vi pham, nghi hoc, mien giam cua hoc sinh
        /// </summary>
        /// <author date="2014/02/28">HaiVT</author>
        /// <param name="schoolID">id truong</param>
        /// <param name="classID">id lop</param>
        /// <param name="academicYearID">id nam hoc</param>
        /// <param name="pupilID">id hoc sinh</param>
        /// <returns></returns>
        [OperationContract]
        PupilFaultAbsenceExemptResponse GetPupilFaultAbsenceExcempt(int schoolID, int classID, int academicYearID, int pupilID);
        #endregion

        /// <summary>
        /// Lấy ds học sinh trong khối theo đk đầu vào của những học sinh đang học
        /// </summary>
        /// <param name="ChoolID"></param>
        /// <param name="AcademicYearID"></param>
        /// <param name="EducationID"></param>
        /// <returns></returns>
        [OperationContract]
        ListPupilByEducationLevelResponse GetListPupilByEducationLevelID(int SchoolID, int AcademicYearID, int EducationLevelID);

        /// <summary>
        /// Trả về list hoc sinh, order theo OrderNumber của ClassProfile, ClassName, OrderInClass của PupilOfClass, FullName học sinh (giống hồ sơ học sinh).
        /// PupilProfileID: ID học sinh
        /// FullName: Tên học sinh
        /// ClassName: Tên lớp
        /// </summary>
        /// <param name="listPupilProfileID"></param>
        /// <param name="SchoolID"></param>
        /// <param name="AcademicYearID"></param>
        /// <param name="EducationLevelID"></param>
        /// <returns></returns>
        [OperationContract]
        ListPupilProfileClassResponse GetListPupilByListPupilProfileID(List<int> listPupilProfileID, int SchoolID, int AcademicYearID);

        #region kiem tra co ton tai hoc sinh voi nam duoc chon hay khong
        /// <summary>
        /// kiem tra co ton tai hoc sinh voi nam duoc chon hay khong        
        /// </summary>
        /// <author date="2014/02/18">HaiVT</author>
        /// <param name="schoolID">id truong</param>
        /// <param name="pupilID">id hoc sinh</param>
        /// <param name="year">nam hoc</param>
        /// <returns>Neu ton tai tra ve True, nguoc lai la False</returns>
        [OperationContract]
        BoolResultResponse CheckPupilWithYear(int schoolID, int pupilID, int year);
        #endregion

        #region lay thong tin hoc sinh gom id + ten ho theo id hoc sinh truyen vao khong quan tam den trang thai hoc sinh, nam hoc
        /// <summary>
        /// lay thong tin hoc sinh gom id + ten ho theo id hoc sinh truyen vao khong quan tam den trang thai hoc sinh, nam hoc
        /// </summary>
        /// <author date="2014/02/25">HaiVT</author>
        /// <param name="pupilProfileList">list id hoc sinh</param>
        /// <returns></returns>
        [OperationContract]
        ListPupilProfileClassResponse GetListPupilByListID(List<int> pupilProfileList);
        #endregion
        #endregion

        #region For Thư viện điện tử
        /// <summary>
        /// Lấy thông tin điểm học sinh
        /// </summary>
        /// <param name="schoolID"></param>
        /// <param name="pupilCode"></param>
        /// <param name="year"></param>
        /// <param name="semester"></param>
        /// <returns></returns>
        [OperationContract]
        PupilFile_Mark GetPupilFileMark(int schoolID, string pupilCode, int year, int semester);

        /// <summary>
        /// Lấy thông tin học sinh cùng thông tin lớp học và năm học.
        /// </summary>
        /// <param name="pupilID">ID của học sinh</param>
        /// <param name="schoolID">ID của trường</param>
        /// <param name="year">Năm học</param>
        /// <returns>Thông tin học sinh cùng thông tin lớp học và năm học.</returns>
        [OperationContract]
        PupilProfileForSelectPupilView FindPupilProfile(int pupilID, int schoolID, int year);

        /// <summary>
        /// Lấy danh sách thông tin học sinh cho SParent
        /// </summary>
        /// <param name="lstPupilID"></param>
        /// <param name="lstSchoolID"></param>
        /// <param name="lstYear"></param>
        /// <returns></returns>
        [OperationContract]
        ListPupilView FindListPupilProfile(List<int> lstPupilID, List<int> lstSchoolID, List<int> lstYear);
        /// <summary>
        /// Hàm lấy thông tin vi phạm nghỉ học miễn giảm khen thưởng
        /// </summary>
        /// <param name="objInfomation"></param>
        /// <returns></returns>
        [OperationContract]
        NewInformationOfPupilResponse GetNewInformationOfPupil(InformationRequest objInfomation);
        #endregion

        [OperationContract]
        ListMarkRecordResponse GetListPupilTranscript(int PupilProfileID, int SchoolID, int ClassID, int AcademicYearID, int EducationGrade, int SemesterID, bool isShowAllMark);

        [OperationContract]
        PupilProfileLiteResponse GetPupilProfileDetailByID(int PupilProfileID);

        /// <summary>
        /// Kiem tra thong tin cua hoc sinh phuc vu viec dang ky dich vu qua SMS
        /// </summary>
        /// <param name="PupilProfileID"></param>
        /// <returns></returns>
        [OperationContract]
        PupilProfile4RegisterResponse GetPupilProfile4RegisterByID(List<int> listPupilProfileID);

        #region khen thuong, ky luat hoc sinh
        /// <summary>
        /// lay khen thuong ky luat cua hoc sinh
        /// </summary>
        /// <param name="schoolID">id truong</param>
        /// <param name="classID">id lop hoc</param>
        /// <param name="academicYearID">id nam hoc</param>
        /// <param name="pupilID">id hoc sinh</param>
        /// <returns></returns>
        [OperationContract]
        PupilPraiseDisciplineResponse GetPupilPraiseDiscipline(int PupilID, int SchoolID);
        #endregion
        /// <summary>
        /// lay diem tong ket cua hoc sinh de thong ke nhung mon tot nhat
        /// </summary>
        /// <param name="PupilProfileID"></param>
        /// <param name="AcademicYearID"></param>
        /// <param name="ClassID"></param>
        /// <returns></returns>
        [OperationContract]
        ListGoodBadSubjectResponse GetListGoodBadSubject(int PupilProfileID, int AcademicYearID, int ClassID, int SemesterID, int Grade);

        /// <summary>
        /// Lsy danh sach ID cua hoc sinh
        /// </summary>
        /// <param name="SchoolID"></param>
        /// <param name="Grade"></param>
        /// <param name="Year"></param>
        /// <returns></returns>
        [OperationContract]
        ListIntResultResponse GetListPupilProfileID(int SchoolID, int Grade, int Year);
        [OperationContract]
        /// <summary>
        /// lay lop hoc cua hoc sinh dang theo hoc trong namw
        /// </summary>
        /// <param name="AcademicYearID"></param>
        /// <param name="PupilProfileID"></param>
        /// <returns></returns>
        PupilOfClassResponse GetPupilOfClassByID(int AcademicYearID, int PupilProfileID);
        /// <summary>
        /// lay danh sach hoc sinh + giao vien chu nhiem theo nam hoc truyen vao, neu co giao vien lam thay thi lay lam thay
        /// </summary>
        /// <author date="2014/02/25">HaiVT</author>
        /// <param name="pupilProfileList">list id hoc sinh</param>
        /// <returns></returns>
        [OperationContract]
        ListPupilProfileClassResponse GetListPupilSendSMS(List<int> pupilProfileList, int academicYearID);

        /// <summary>
        /// Danh sach hoc sinh theo list id
        /// </summary>
        /// <param name="pupilProfileList"></param>
        /// <returns></returns>
        [OperationContract]
        ListPupilProfileClassResponse GetListPupilStatictisSMSByListID(List<int> pupilProfileList, int AcademicYearID);

        /// <summary>
        /// Lấy danh sách học sinh cho thong ke tin nhan SMS_SCHOOOL
        /// Lay ra danh sach tat ca hoc sinh, nhung hoc sinh nao co trang thay chuyen truong hoac chuyen lop thi khong co trong khoang thoi gian thong ke
        /// </summary>
        /// <param name="SchoolID"></param>
        /// <param name="AcademicYearID"></param>
        /// <param name="Grade"></param>
        /// <param name="EducationLevelID"></param>
        /// <param name="ClassID"></param>
        /// <returns></returns>
        [OperationContract]
        ListPupilProfileLiteResponse GetListPupilStatictis(int SchoolID, int AcademicYearID, int EducationLevelID, int ClassID, int GradeID = 0);

        /// <summary>
        /// List thong tin nhan xet cap 1
        /// Danh gia cuoi ky truyen monthID == 11
        /// </summary>
        /// <param name="schoolID"></param>
        /// <param name="academicYearID"></param>
        /// <param name="classID"></param>
        /// <param name="semesterID"></param>
        /// <param name="monthID"></param>
        /// <param name="userAccountID"> Id account đăng nhập </param>
        /// 
        /// <param name="viewAll"> xét quyền có cho view không </param>
        /// <returns></returns>
        [OperationContract]
        EvaluationCommentsComposite getEvaluationCommnetsBySchoolOfPrimary(List<long> pupilIDList, int schoolID, int academicYearID, int classID, int semesterID, int monthID, int userAccountID, bool viewAll = true);

        /// <summary>
        /// Lay thong tin nhan xet sparent
        /// </summary>
        /// <param name="schoolID"></param>
        /// <param name="academicYearID"></param>
        /// <param name="classID"></param>
        /// <param name="semesterID"></param>
        /// <param name="monthID"></param>
        /// <param name="pupilID"></param>
        /// <param name="viewAllmark"></param>
        /// <param name="viewSemester1"></param>
        /// <param name="viewSemester2"></param>
        /// <param name="strLockMark"></param>
        /// <returns></returns>
        [OperationContract]
        EvaluationCommentsPupilComposite getEvaluationCommentsSparentMonthBySchool(int schoolID, int academicYearID, int classID, int semesterID, int monthID, int pupilID, bool viewAllmark, bool viewSemester1, bool viewSemester2);

        /// <summary>
        /// lấy thông tin học sinh phục vụ hiển thị ở trang PHHS - mô hình VNEN cho THCS
        /// </summary>
        /// <param name="schoolID"></param>
        /// <param name="academicYearID"></param>
        /// <param name="classID"></param>
        /// <param name="semesterID"></param>
        /// <param name="pupilID"></param>
        /// <returns></returns>
        [OperationContract]
        EvaluationCommentsPupilComposite getEvaluationCommentVNENSParent(int schoolID, int academicYearID, int classID, int semesterID, int pupilID);
        /// <summary>
        /// Lay danh sach hoc sinh boi list ID
        /// </summary>
        /// <param name="pupilIDList"></param>
        /// <param name="SchoolID"></param>
        /// <param name="AcademicYearID"></param>
        /// <param name="ClassID"></param>
        /// <param name="EducationLevelID"></param>
        /// <param name="PupilCode"></param>
        /// <param name="Pupilname"></param>
        /// <param name="grade"></param>
        /// <returns></returns>
        [OperationContract]
        ListPupilProfileLiteResponse GetListPupilByListPupilID(List<int> pupilIDList, int SchoolID, int AcademicYearID, int ClassID, int? EducationLevelID, string PupilCode, string Pupilname, int grade, int teacherID);
        /// <summary>
        /// Danh sach hoc sinh co phan trang theo list ID hoc sinh co hop dong
        /// </summary>
        /// <param name="pupilIDList">danh sach Id hoc sinh co hop dong</param>
        /// <param name="SchoolID"></param>
        /// <param name="AcademicYearID"></param>
        /// <param name="ClassID"></param>
        /// <param name="EducationLevelID"></param>
        /// <param name="PupilCode"></param>
        /// <param name="Pupilname"></param>
        /// <param name="grade"></param>
        /// <param name="currentPage"></param>
        /// <param name="pageSize"></param>
        /// <returns></returns>
        [OperationContract]
        ListPupilProfileLiteResponse GetListPupilByListPupilIDPaging(List<int> pupilIDList, int SchoolID, int AcademicYearID, int ClassID, int? EducationLevelID, string PupilCode, string Pupilname, int grade, int currentPage, int pageSize, int teacherID);

        [OperationContract]
        ListPupilResponseFormatString GetPupilsByIDsPagingformatString(List<int> pupilIDList, int SchoolID, int AcademicYearID, int ClassID, int? EducationLevelID, string PupilCode, string Pupilname, int grade, int currentPage, int pageSize, int teacherID);

        /// <summary>
        /// Lay toan bo danh sach hoc sinh cua truong theo dk tim kiem
        /// </summary>
        /// <param name="SchoolID"></param>
        /// <param name="AcademicYearID"></param>
        /// <param name="ClassID"></param>
        /// <param name="PupilCode"></param>
        /// <param name="Pupilname"></param>
        /// <param name="EducationLevelID"></param>
        /// <param name="grade"></param>
        /// <returns></returns>
        [OperationContract]
        ListPupilProfileLiteResponse GetListPupilBySchoolIDFull(int SchoolID, int AcademicYearID, int ClassID, string PupilCode, string Pupilname, int? EducationLevelID, int grade, int teacherID);


        [OperationContract]
        List<string> GetPupilsformatString(int SchoolID, int AcademicYearID, int ClassID, string PupilCode, string Pupilname, int? EducationLevelID, int grade, int teacherID);
        /// <summary>
        ///  Anhnph - Cap nhat hoc sinh co dang ky HD
        /// </summary>
        /// <modifer data="3/2/2015">AnhVD9</modifer>
        /// <param name="lstPupilUpdate"></param>
        /// <param name="SchoolID"></param>
        /// <param name="AcademicYearID"></param>
        /// <param name="UserID"></param>
        /// <returns></returns>
        [OperationContract]
        bool updateRegisterContractPupil(List<int> lstPupilOfClassIDUpdate);

        /// <summary>
        /// Lay danh sach diem thi theo hoc ky
        /// </summary>
        /// <param name="SchoolID"></param>
        /// <param name="ClassID"></param>
        /// <param name="AcademicYearID"></param>
        /// <param name="Semester"></param>
        /// <returns></returns>
        [OperationContract]
        ExamMarkSemesterComposite GetListExamMarkSemester(List<int> lstPupilID, int SchoolID, int ClassID, int AcademicYearID, int Semester);

        /// <summary>
        /// lấy nhận xét học sinh theo mô hình mới VNEN
        /// </summary>
        /// <param name="pupilIDList"></param>
        /// <param name="schoolID"></param>
        /// <param name="academicYearID"></param>
        /// <param name="classID"></param>
        /// <param name="semesterID"></param>
        /// <param name="monthID"></param>
        /// <returns></returns>
        [OperationContract]
        EvaluationCommentsComposite getCommentSchoolVNEN(List<long> pupilIDList, int schoolID, int academicYearID, int classID, int semesterID, int monthID);

        /// <summary>
        /// Lấy danh sách kết quả thi của học sinh
        /// </summary>
        /// <param name="pupilIDList"></param>
        /// <param name="schoolID"></param>
        /// <param name="academicYearID"></param>
        /// <param name="classID"></param>
        /// <param name="semesterID"></param>
        /// <param name="examID"></param>
        /// <returns></returns>
        [OperationContract]
        ListExamMarkPupilResponse getExamResultInfo(List<long> pupilIDList, int schoolID, int academicYearID, int classID, int semesterID, int examID);

        /// <summary>
        /// Lấy thông tin kỳ thi
        /// </summary>
        /// <param name="pupilIDList"></param>
        /// <param name="schoolID"></param>
        /// <param name="academicYearID"></param>
        /// <param name="classID"></param>
        /// <param name="semesterID"></param>
        /// <param name="examID"></param>
        /// <returns></returns>
        [OperationContract]
        ListExamSubjectResponse getExamScheduleInfo(List<long> pupilIDList, int schoolID, int academicYearID, int classID, int semesterID, int examID);

       
        [OperationContract]
        List<CommentOfPupilResponse> GetCommentOfPupil(List<int> lstPupilID, int SchoolID, int AcademicYearID, int ClassID, int SubjectID, int SemesterID, int DayOfMonth, DateTime FromDate, DateTime ToDate, int NoDate, int NoSubject);

        #region viethd31: Lay thong tin TT22
        [OperationContract]
        RatedCommentPupilComposite GetListRatedCommentPupil(List<long> pupilIDList, int schoolID, int academicYearID, int classID, int type, int semesterID);

        [OperationContract]
        TT22ResultComposite GetTT22Result(int schoolId, int academicYearId, int educationLevel, int classId, int pupilId);
        #endregion

        /// <summary>
        /// Viethd31: Ban tin tu dinh nghia
        /// </summary>
        /// <param name="pupilIDList"></param>
        /// <param name="schoolID"></param>
        /// <param name="academicYearID"></param>
        /// <param name="classID"></param>
        /// <param name="grade"></param>
        /// <param name="template"></param>
        /// <param name="periodType"></param>
        /// <param name="forVNEN"></param>
        /// <param name="lstContentTypeCode"></param>
        /// <param name="nameDisplay"></param>
        /// <param name="semester"></param>
        /// <param name="month"></param>
        /// <param name="year"></param>
        /// <param name="fromDate"></param>
        /// <param name="toDate"></param>
        /// <returns></returns>
        [OperationContract]
        CustomSMSComposite GetCustomSMS(List<long> pupilIDList, int schoolID, int academicYearID, int classID, int grade, string template, int periodType, bool forVNEN, List<string> lstContentTypeCode, int? nameDisplay,
           int? semester = null, int? month = null, int? year = null, DateTime? fromDate = null, DateTime? toDate = null);

        #region viethd31: Mam non
        [OperationContract]
        PreschoolResultComposite GetMealMenuOfPupil(List<long> pupilIDList, int schoolID, int academicYearID, int classID, int type, DateTime fromDate, DateTime toDate);

        [OperationContract]
        PreschoolResultComposite GetPhysicalResultOfPupil(List<long> pupilIDList, int schoolID, int academicYearID, int classID, int month);

        [OperationContract]
        PreschoolResultComposite GetGrowthEvaluationOfPupil(List<long> pupilIDList, int schoolID, int academicYearID, int educationLevel, int classID);

        [OperationContract]
        PreschoolResultComposite GetDailyActivityOfPupil(List<long> pupilIDList, int schoolID, int academicYearID, int classID, DateTime date);

        [OperationContract]
        PreschoolResultComposite GetGoodTicketOfPupil(List<long> pupilIDList, int schoolID, int academicYearID, int classID, int type, DateTime month, int week);
        #endregion

    }
}
