﻿using System.Collections.Generic;
using System.ServiceModel;
using WCF.Composite;
using WCF.Composite.Migrate;
using WCF.Composite.Sync;

namespace WCF.Services
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the interface name "IMEmployeeModule" in both code and config file together.
    [ServiceContract]
    public interface IMEmployeeModule
    {
        #region Sync Hue
        [OperationContract]
        MEmployee GetEmployeebySchoolId(int schoolId);

        [OperationContract]
        BoolResultResponse InsertEmployee(List<MEmployeeDefault> lstEmploye);

        [OperationContract]

        BoolResultResponse UpdateEmployee(List<MEmployeeDefault> lstEmploye);

        [OperationContract]
        /// <summary>
        /// Danh sach to bo mon
        /// </summary>
        /// <param name="schoolId"></param>
        /// <returns></returns>
        MSchoolFaculty GetFaculty(int schoolId);



        [OperationContract]

        BoolResultResponse InsertFaculty(List<MSchoolFacultyDefault> lstSchoolFaculty);

        #endregion

        #region Sync HCM
        [OperationContract]
        /// <summary>
        /// Lay danh sach can bo
        /// </summary>
        /// <param name="form"></param>
        /// <returns></returns>
        TransEmployeeResponse GetSyncEmployee(SyncAcademicYear form);

        /// <summary>
        /// Cap nhat danh sach can bo da dong bo
        /// </summary>
        /// <param name="lstEmployeeId"></param>
        /// <param name="form"></param>
        /// <returns></returns>
        [OperationContract]
        BoolResultResponse UpdateSyncEmployee(List<int> lstEmployeeId, SyncAcademicYear form);

        #endregion


    }
}
