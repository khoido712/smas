﻿using SMAS.Models.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using SMAS.Business.IBusiness;
using System.Text;
using WCF.Composite;
using SMAS.Models.Models.CustomModels;
using WCF.Composite.Sync;

namespace WCF
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the interface name "ISystemModule" in both code and config file together.
    [ServiceContract]
    public interface ISystemModule
    {
        /// <summary>
        /// Lấy danh sách lớp học trong trường theo năm học
        /// </summary>
        /// <param name="SchoolID"></param>
        /// <param name="AcademicYearID"></param>
        /// <returns></returns>
        [OperationContract]
        ListClassProfileLiteResponse GetListClassBySchool(int SchoolID, int AcademicYearID);

        /// <summary>
        /// Lấy thông tin lớp học theo ID
        /// </summary>
        /// <param name="ClassID"></param>
        /// <returns></returns>
        [OperationContract]
        ClassProfileLiteResponse GetClassProfileByID(int ClassID);

        /// <summary>
        /// Lấy danh sách lớp học theo quyền giáo viên chủ nhiệm
        /// </summary>
        /// <param name="SchoolID"></param>
        /// <param name="AcademicYearID"></param>
        /// <param name="TeacherID"></param>
        /// <returns></returns>
        [OperationContract]
        ListClassProfileLiteResponse GetListClassByHeadTeacher(int SchoolID, int AcademicYearID, int TeacherID);

        [OperationContract]
        ListClassProfileLiteResponse GetListClassByRoleHeadSubjectTeacher(int SchoolID, int AcademicYearID, int AppliedLevel, int UserAccountID);

        /// <summary>
        /// Lấy danh sách môn học theo lớp
        /// </summary>
        /// <param name="SchoolID"></param>
        /// <param name="ClassID"></param>
        /// <returns></returns>
        [OperationContract]
        ListSubjectCatLiteResponse GetListSubjectInClass(int SchoolID, int ClassID);

        /// <summary>
        /// Lấy thông tin thời khóa biểu của lớp
        /// </summary>
        /// <param name="SchoolID"></param>
        /// <param name="AcademicYearID"></param>
        /// <param name="ClassID"></param>
        /// <returns></returns>
        [OperationContract]
        StringResultResponse GetListCalendarOfPupil(int AcademicYearID, int ClassID, int Semester);
        /// <summary>
        /// lấy danh sách thời khóa biểu của lớp
        /// </summary>
        /// <param name="academicYearID"></param>
        /// <param name="classID"></param>
        /// <param name="semesterID"></param>
        /// <returns></returns>
        [OperationContract]
        ListCalendarResponse GetListCalendar(int academicYearID, int classID, int semesterID);

        /// <summary>
        /// Lấy danh sách khối học theo lớp
        /// </summary>
        /// <param name="Grade"></param>
        /// <returns></returns>
        [OperationContract]
        ListEducationLevelResponse GetListLevel(int Grade);

        /// <summary>
        /// Lấy danh sách lớp học theo năm học, cấp học, khối học
        /// </summary>
        /// <param name="AcademicYearID"></param>
        /// <param name="Grade"></param>
        /// <param name="EducationLevelID"></param>
        /// <returns></returns>
        [OperationContract]
        ListClassProfileLiteResponse GetListClass(int AcademicYearID, int EducationLevelID);

        /// <summary>
        /// Kiểm tra trường đã đăng ký sử dụng SMS Parent
        /// </summary>
        /// <param name="SchoolID"></param>
        /// <returns></returns>
        [OperationContract]
        BoolResultResponse IsSMSParentActive(int SchoolID);

        /// <summary>
        /// Lấy danh sách đợt theo học kỳ
        /// </summary>
        /// <param name="AcademicYearID"></param>
        /// <param name="Semester"></param>
        /// <returns></returns>
        [OperationContract]
        ListPeriodDeclarationResponse GetTimeMarkBySemester(int AcademicYearID, int Semester);

        /// <summary>
        /// Lấy thông tin nhà trường theo ID
        /// </summary>
        /// <param name="SchoolID"></param>
        /// <returns></returns>
        [OperationContract]
        SchoolProfileResponse GetSchoolByID(int SchoolID);

        /// <summary>
        /// lấy hình đại điện của trường
        /// </summary>
        /// <param name="schoolID">id truong</param>
        /// <returns></returns>
        [OperationContract]
        ByteArrayResultResponse GetSchoolImage(int schoolID);

        /// <summary>
        /// Lấy thông tin nhà trường từ tên đăng nhập
        /// </summary>
        /// <param name="UserName"></param>
        /// <returns></returns>
        [OperationContract]
        SchoolProfileResponse GetSchoolByUserName(string UserName);

        [OperationContract]
        SchoolProfileResponse GetUnitByAdminAccount(string UserName);

        /// <summary>
        /// lay truong theo schoolCode
        /// </summary>
        /// <author date="2014/02/18">HaiVT</author>
        /// <param name="schoolCode">ma truong</param>
        /// <returns></returns>
        [OperationContract]
        SchoolProfileResponse GetSchoolBySchoolCode(string schoolCode);

        /// <summary>
        /// Lấy thông tin trong bảng UserAccount theo tên user
        /// </summary>
        /// <param name="UserName"></param>
        /// <returns></returns>
        [OperationContract]
        UserAccountResponse GetUserAccountByUserName(string UserName);

        /// <summary>
        /// Kiểm tra Giáo viên có phải là BGH của trường
        /// </summary>
        /// <param name="TeacherID"></param>
        /// <returns></returns>
        [OperationContract]
        BoolResultResponse CheckPrincipleByID(int TeacherID);

        /// <summary>
        /// Lấy thông tin phòng sở theo ID
        /// </summary>
        /// <param name="SupervisingDeptID"></param>
        /// <returns></returns>
        [OperationContract]
        SupervisingDeptResponse GetSupervisingDeptByID(int SupervisingDeptID);

        /// <summary>
        /// Lấy thông tin năm học theo ID
        /// </summary>
        /// <param name="AcademicYearID"></param>
        /// <returns></returns>
        [OperationContract]
        AcademicYearResponse GetAcademicYearByID(int AcademicYearID);

        /// <summary>
        /// get academicYearID 
        /// </summary>
        /// <author date="2014/02/18">HaiVT</author>
        /// <param name="schoolID">id truong</param>
        /// <returns></returns>
        [OperationContract]
        AcademicYearResponse GetCurrentAcademicYearOfSchool(int schoolID);

        #region lay nam hoc theo schoolID va Year
        /// <summary>
        /// lay nam hoc theo schoolID va Year
        /// </summary>
        /// <author date="2014/02/25">HaiVT</author>
        /// <param name="schoolID">id truong</param>
        /// <param name="year">nam hoc</param>
        /// <returns></returns>
        [OperationContract]
        AcademicYearResponse GetAcademicYearBySchoolIDAndYear(int schoolID, int year);
        #endregion

        /// <summary>
        /// Lấy thông tin cấp học của nhà trường
        /// </summary>
        /// <param name="SchoolID"></param>
        /// <returns></returns>
        [OperationContract]
        GradeResponse GetSchoolGradeBySchoolID(int SchoolID);

        /// <summary>
        /// Insert log
        /// </summary>
        /// <param name="ActionAuditComposite"></param>
        [OperationContract]
        BoolResultResponse InsertLog(ActionAuditRequest ActionAuditComposite);

        /// <summary>
        /// Lấy đơn vị quản lý sở/phòng theo AdminID (là UserAccountID)
        /// </summary>
        /// <param name="AdminID"></param>
        /// <returns></returns>
        [OperationContract]
        SupervisingDeptResponse GetSupervisingDeptByAdminID(int AdminID);

        /// <summary>
        /// Kiem tra xem trong co phai la GDTX hay khong
        /// </summary>
        /// <param name="SchoolID"></param>
        /// <returns></returns>
        [OperationContract]
        BoolResultResponse GetVocationType(int SchoolID);

        #region kiem tra co ton tai username trong he thong hay khong
        /// <summary>
        /// kiem tra co ton tai username trong he thong hay khong
        /// </summary>
        /// <author date="2014/02/21">HaiVT</author>
        /// <param name="userLogin"></param>
        /// <param name="passLogin"></param>
        /// <param name="accName"></param>
        /// <returns></returns>
        [OperationContract]
        string CheckAccExitEncrypt(string authenPass, string accName);
        #endregion

        #region active smas
        /// <summary>
        /// active smas        
        /// </summary>
        /// <param name="authenPass">ma xac thuc</param>
        /// <param name="userName">username</param>
        /// <returns></returns>
        [OperationContract]
        string ActiveSMASEncrypt(string authenPass, string userName);
        #endregion

        #region deactive smas
        /// <summary>
        /// deactive smas
        /// </summary>
        /// <param name="authenPass">ma xac thuc</param>
        /// <param name="userName">user name</param>
        /// <returns></returns>
        [OperationContract]
        string DeactiveSMASEncrypt(string authenPass, string userName);
        #endregion

        #region active sms teacher
        /// <summary>
        /// active sms teacher
        /// </summary>
        /// <param name="authenPass">ma xac thuc</param>
        /// <param name="userName">tai khoan truong</param>
        /// <returns></returns>
        [OperationContract]
        string ActiveSMSTeacherEncrypt(string authenPass, string userName);
        #endregion

        #region deactive SMSTeacher
        /// <summary>
        /// deactive sms teacher
        /// </summary>
        /// <author date="2014/02/22">HaiVT</author>
        /// <param name="authenUser">user xac thuc</param>
        /// <param name="authenPass">ma xac thuc</param>
        /// <param name="userName">acc truong</param>
        /// <param name="DeactiveCode"></param>
        /// <returns></returns>
        [OperationContract]
        string DeactiveSMSTeacherEncrypt(string authenUser, string authenPass, string userName, string DeactiveCode);
        #endregion

        #region active smsParent
        /// <summary>
        /// active smsParent
        /// </summary>
        /// <author date="2014/02/22">HaiVT</author>
        /// <param name="authen">ma xac thuc</param>
        /// <param name="accName">user truong</param>
        /// <returns></returns>
        [OperationContract]
        string ActiveSMSParent(string authenPass, string userName);
        #endregion

        #region deactive sms parent
        /// <summary>
        /// deactive sms parent
        /// </summary>
        /// <author date="2014/02/22">HaiVT</author>
        /// <param name="authenPass">ma xac thuc</param>
        /// <param name="userName">user truong</param>
        /// <returns></returns>
        [OperationContract]
        string DeactiveSMSParent(string authenPass, string userName);
        #endregion

        #region Check trang thai SMSTeacher
        /// <summary>
        /// Check trang thai SMSTeacher
        /// param: authen ma xac thuc, accName account truong
        /// returns: 9 khong xac dinh site HCM/TQ, 10 -> 13 loi, -1 khong thanh cong; 0 khong kich hoat sms, 1 kick hoat khong gioi han, 2 kick hoat gioi han, 3 chan cat
        /// </summary>
        /// <author date="2014/02/22">HaiVT</author>
        /// <param name="authenPass">ma xac thuc</param>
        /// <param name="userName">account truong</param>
        /// <returns>string: 1</returns>        
        [OperationContract]
        string CheckSMSTeacher(string authenPass, string userName);
        #endregion

        [OperationContract]
        CalendarScheduleRespone LoadScheduleCalendar(DateTime applyDate, int SchoolID, int AcademicYearID, int EducationGrade, int ClassID, int Semester, int DayOfWeek);
        /// <summary>
        /// lấy số con điểm được khai báo theo học kỳ
        /// </summary>
        /// <param name="SchoolID"></param>
        /// <param name="AcademicYearID"></param>
        /// <param name="SemesterID"></param>
        /// <returns></returns>
        [OperationContract]
        SemesterDeclarationResponse GetSemesterDeclaration(int SchoolID, int AcademicYearID, int SemesterID);

        #region get userName by schoolID
        /// <summary>
        /// lay usrename by schoolID
        /// </summary>
        /// <author date="2014/03/11">HaiVT</author>
        /// <param name="schoolID">id truong</param>
        /// <returns></returns>
        [OperationContract]
        StringResultResponse GetUserNameBySchoolID(int schoolID);
        #endregion

        /// <summary>
        /// Lay quyen cua nguoi dung
        /// </summary>
        /// <param name="Controller"></param>
        /// <param name="UserAccountID"></param>
        /// <returns></returns>
        [OperationContract]
        EPermission GetMenuPermission(string Controller, int UserAccountID);

        #region SMASPortal
        /// <summary>
        /// lay danh sach tinh
        /// </summary>
        /// <returns></returns>
        [OperationContract]
        ListProvinceResponse GetAllProvince();

        /// <summary>
        /// Lay quan/huyen theo tinh
        /// </summary>
        /// <param name="ProvinceID"></param>
        /// <param name="DistrictID"></param>
        /// <returns></returns>
        [OperationContract]
        ListDistrictResponse GetDistrictByProvinceID(int ProvinceID, int DistrictID);

        /// <summary>
        /// Lay danh sach cac truong trong mot huyen
        /// </summary>
        /// <param name="DistrictID"></param>
        /// <param name="Grade"></param>
        /// <returns></returns>
        [OperationContract]
        ListSchoolProfileLiteResponse GetSchoolInDistrict(int DistrictID, int Grade);

        /// <summary>
        /// Lay danh sach truong
        /// </summary>
        /// <param name="SchoolID"></param>
        /// <returns></returns>
        [OperationContract]
        ListSchoolProfileLiteResponse GetSchoolByIDPortal(int SchoolID);


        /// <summary>
        /// Lay nam hoc
        /// </summary>
        /// <param name="SchoolID"></param>
        /// <returns></returns>
        [OperationContract]
        ListAcademicYearResponse GetAcademicYearPortal(int SchoolID);
        #endregion

        #region Quanglm - Bo sung cac ham lay thong tin truong phuc vu tool quan ly thue doi tac nhap lieu
        /// <summary>
        /// Quanglm - 20140926
        /// Lay danh sach truong theo quan/huyen
        /// </summary>
        /// <param name="DistrictID"></param>
        /// <returns></returns>
        [OperationContract]
        ListSchoolProfileLiteResponse GetSchoolByDistrictID(int DistrictID);

        /// <summary>
        /// Quanglm - Lay danh sach truong theo danh sach ID truyen vao
        /// </summary>
        /// <param name="DistrictID"></param>
        /// <returns></returns>
        [OperationContract]
        ListSchoolProfileLiteResponse GetSchoolByListID(List<int> listSchoolID);
        #endregion

        /// <summary>
        /// lay thong tin thang nhan xet cap 1
        /// Danh muc khong the rong (insert bang tay vao DB)
        /// </summary>
        /// <param name="semesterID"></param>
        /// <returns></returns>
        [OperationContract]
        List<MonthComments> getListMonthEvaluation();

        /// <summary>
        /// lay thong tin khoa diem cua lop
        /// </summary>
        /// <param name="schoolID"></param>
        /// <param name="classID"></param>
        /// <returns></returns>
        [OperationContract]
        string getLockMark(int schoolID, int classID);
        /// <summary>
        /// Anhnph1 - Lay danh sach nam hoc
        /// </summary>
        /// <returns></returns>
        [OperationContract]
        List<int> getYear();

        /// <summary>
        /// lay danh sach truong theo thanh pho hoac quan huyen
        /// </summary>
        /// <param name="ProvinceID"></param>
        /// <param name="DistrictID"></param>
        /// <returns></returns>
        [OperationContract]
        ListSchoolProfileLiteResponse GetSchoolByDistrictAndApplied(int DistrictID, int? Applied);

        /// <summary>
        /// Lay thong tin cua truong hoac phong so co cau hinh su dung tra cuuu diem trong web portal
        /// </summary>
        /// <param name="ID">ID cua truong hoac phong so</param>
        /// <returns></returns>
        [OperationContract]
        SchoolPortalComposite GetSchoolPortal(int ID, bool isSchool);

        /// <summary>
        /// ham check quan/huyen trong tinh thanh, truong trong quan/huyen
        /// </summary>
        /// <param name="schoolId"></param>
        /// <param name="provinceId"></param>
        /// <param name="districtID"></param>
        /// <returns></returns>
        [OperationContract]
        int CheckSchoolInPorvince(int schoolId, int provinceId, int districtID);


        #region Thanh toan phi dich vu SMSEDU qua Bankplus
        /// <summary>
        /// Lay thong tin tai khoan Khach hang
        /// </summary>
        /// <author date="13/10/2015">AnhVD9</author>
        /// <param name="UserName"></param>
        /// <returns></returns>
        [OperationContract]
        EducationUnitDefault GetAccountInfoByUserName(string UserName);
        #endregion

        #region 20151110 - AnhVD9 - Báo cáo phục vụ Thống kê - Điều hành
        /// <summary>
        /// Lấy báo cáo hiện trạng SMAS - phục vụ Thống kê - Điều hành
        /// </summary>
        /// <author date="11/09/2015">AnhVD9</author>
        /// <param name="ProvinceId"></param>
        /// <param name="DistrictId"></param>
        /// <param name="Semester"></param>
        /// <param name="Year"></param>
        /// <returns></returns>
        [OperationContract]
        ListReportObjectResponse GetCurrentSituationSMASReport(int ProvinceId, int DistrictId, int Semester, int Year);
        #endregion

        /// <summary>
        /// Kiem tra kio theo mo hinh VNEN
        /// </summary>
        /// <param name="ClassID"></param>
        /// <returns></returns>
        [OperationContract]
        BoolResultResponse IsVNENClass(int ClassID);

        #region Cấu hình gói cước
        /// <summary>
        /// Lấy danh sách thông tin trường có phân trang 
        /// </summary>
        /// <author date="27/02/2016">Anhvd9</author>
        /// <param name="req"></param>
        /// <returns></returns>
        [OperationContract]
        ListSchoolResponse GetListSchool(SchoolRequest req);

        /// <summary>
        /// lấy danh sách mã trường để cấu hình
        /// </summary>
        /// <author date="27/02/2016">Anhvd9</author>
        /// <param name="req"></param>
        /// <returns></returns>
        [OperationContract]
        List<int> GetListSchoolID(SchoolRequest req);
        #endregion

        #region 20160302 Anhvd9 - Bổ sung 2 bản tin liên quan tới Kỳ thi
        /// <summary>
        /// Lấy thông tin chi tiết của kỳ thi
        /// </summary>
        /// <param name="req"></param>
        /// <returns></returns>
        [OperationContract]
        ListExamResponse GetExamOfSchool(ExaminationRequest req);
        #endregion

        #region 20160421 Anhvd9 - lấy danh sách tổ bộ môn
        /// <summary>
        /// lay danh sach to bo mon
        /// </summary>
        /// <param name="SchoolID"></param>
        /// <returns></returns>
        [OperationContract]
        List<SchoolFacultyDefault> getSchoolFalculty(int SchoolID);
        #endregion
        
        /// <summary>
        /// lay thong tin tinh/thanh, quan/huyen
        /// </summary>
        /// <param name="provinceID"></param>
        /// <param name="districtID"></param>
        /// <returns></returns>
        [OperationContract]
        DistrictResponse GetDistrictByID(int provinceID, int districtID);

        [OperationContract]
        ListSubjectCatLiteResponse GetListSubjectByClass(int academicYearId, int schoolId, int classId, int semester);

    }
}
