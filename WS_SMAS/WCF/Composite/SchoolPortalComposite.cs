﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Web;

namespace WCF.Composite
{
    [DataContract]
    public class SchoolPortalComposite
    {
        [DataMember]
        public int SchoolPortalID { get; set; }
        [DataMember]
        public int ID { get; set; }
        [DataMember]
        public bool IsSchool { get; set; }
        [DataMember]
        public System.DateTime CreateTime { get; set; }
        [DataMember]
        public Nullable<System.DateTime> UpdateTime { get; set; }
    }
}