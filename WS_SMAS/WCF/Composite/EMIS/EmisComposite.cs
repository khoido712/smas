﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Web;

namespace WCF.Composite.EMIS
{
    [DataContract]
    public class EmisComposite
    {
        public bool isValid { get; set; }
        [DataMember(Name="data")]
        public List<EmisData> Data { get; set; }
    }
    public class EmisData
    {
        [DataMember(Name = "Error_code")]
        public string ErrorCode { get; set; }

        [DataMember(Name = "Message")]
        public string Message { get; set; }

        [DataMember(Name = "School_code")]
        public string SchoolCode { get; set; }

        [DataMember(Name = "Indicator_code")]
        public string IndicatorCode { get; set; }

        [DataMember(Name = "Value")]
        public string Value { get; set; }
    }
}