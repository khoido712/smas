﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Web;

namespace WCF.Composite
{
    [DataContract]
    public class PeriodDeclarationDefault
    {
        [DataMember]
        public int AcademicYearID { get; set; }
        [DataMember]
        public bool ContaintSemesterMark { get; set; }
        [DataMember]
        public DateTime EndDate { get; set; }
        [DataMember]
        public DateTime FromDate { get; set; }
        [DataMember]
        public int? InterviewMark { get; set; }
        [DataMember]
        public bool IsLock { get; set; }
        [DataMember]
        public int PeriodDeclarationID { get; set; }
        [DataMember]
        public string Resolution { get; set; }
        [DataMember]
        public int SchoolID { get; set; }
        [DataMember]
        public int Semester { get; set; }
        [DataMember]
        public int StartIndexOfInterviewMark { get; set; }
        [DataMember]
        public int StartIndexOfTwiceCoeffiecientMark { get; set; }
        [DataMember]
        public int StartIndexOfWritingMark { get; set; }
        [DataMember]
        public int? TwiceCoeffiecientMark { get; set; }
        [DataMember]
        public int? WritingMark { get; set; }
        [DataMember]
        public int Year { get; set; }
    }
    /// <summary>
    /// DataContract gom danh sach thuoc tinh
    /// </summary>
    [DataContract]
    public class ListPeriodDeclarationResponse
    {
        private List<PeriodDeclarationDefault> lstPeriodDeclarationDefault;
        /// <summary>
        /// ma loi tra ve
        /// </summary>
        [DataMember]
        public int ValidationCode { get; set; }
        /// <summary>
        /// danh sach Object dinh kem
        /// </summary>
        [DataMember]
        public List<PeriodDeclarationDefault> LstPeriodDeclarationDefault
        {
            get
            {
                return lstPeriodDeclarationDefault;
            }
            set
            {
                //Check null gan de thuc hien new item
                if (value == null)
                {
                    this.lstPeriodDeclarationDefault = new List<PeriodDeclarationDefault>();
                }
                else
                {
                    this.lstPeriodDeclarationDefault = value;
                }
            }
        }
    }
    [DataContract]
    public class PeriodDeclarationResponse
    {
        private PeriodDeclarationDefault periodDeclaration;
        [DataMember]
        public int ValidationCode { get; set; }
        /// <summary>
        /// danh sach Object dinh kem
        /// </summary>
        [DataMember]
        public PeriodDeclarationDefault ObjPeriodDeclaration
        {
            get
            {
                return periodDeclaration;
            }
            set
            {
                if (value == null)
                {
                    periodDeclaration = new PeriodDeclarationDefault();
                }
                else
                {
                    periodDeclaration = value;
                }
            }
        }
    }
}