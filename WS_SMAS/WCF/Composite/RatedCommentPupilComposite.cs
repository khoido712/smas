﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Web;

namespace WCF.Composite
{
    [DataContract]
    public class RatedCommentPupilComposite
    {
        private List<RatedCommentData> _lstResult;

        [DataMember]
        public int ValidateCode { get; set; }

        [DataMember]
        public List<RatedCommentData> ListResult
        {
            get
            {
                return _lstResult;
            }
            set
            {
                //Check null gan de thuc hien khoi tao 
                if (value == null)
                {
                    this._lstResult = new List<RatedCommentData>();
                }
                else
                {
                    this._lstResult = value;
                }
            }
        }

    }

    [DataContract]
    public class RatedCommentData
    {
        [DataMember]
        public int PupilID { get; set; }

        [DataMember]
        public string SMSContent { get; set; }
    }
}