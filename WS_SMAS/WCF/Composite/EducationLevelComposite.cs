﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Web;

namespace WCF.Composite
{
    [DataContract]
    public class EducationLevelDefault
    {
        /// <summary>
        /// ID khối học
        /// </summary>
        [DataMember]
        public int EducationLevelID { get; set; }

        /// <summary>
        /// Tên khối học
        /// </summary>
        [DataMember]
        public string EducationLevelName { get; set; }
    }

    /// <summary>
    /// DataContract gom 1 object thuoc tinh
    /// </summary>
    [DataContract]
    public class EducationLevelResponse
    {
        private EducationLevelDefault objEducationLevelDefault;
        /// <summary>
        /// ma loi tra ve
        /// </summary>
        [DataMember]
        public string ValidateCode { get; set; }
        /// <summary>
        /// Object dinh kem
        /// </summary>
        [DataMember]
        public EducationLevelDefault ObjEducationLevelDefault
        {
            get
            {
                return objEducationLevelDefault;
            }
            set
            {
                //Check null gan de thuc hien new item
                if (value == null)
                {
                    this.objEducationLevelDefault = new EducationLevelDefault();
                }
                else
                {
                    this.objEducationLevelDefault = value;
                }
            }
        }
    }

    /// <summary>
    /// DataContract gom danh sach thuoc tinh
    /// </summary>
    [DataContract]
    public class ListEducationLevelResponse
    {
        private List<EducationLevelDefault> lstEducationLevelDefault;
        /// <summary>
        /// ma loi tra ve
        /// </summary>
        [DataMember]
        public int ValidationCode { get; set; }
        /// <summary>
        /// danh sach Object dinh kem
        /// </summary>
        [DataMember]
        public List<EducationLevelDefault> LstEducationLevelDefault
        {
            get
            {
                return lstEducationLevelDefault;
            }
            set
            {
                //Check null gan de thuc hien new item
                if (value == null)
                {
                    this.lstEducationLevelDefault = new List<EducationLevelDefault>();
                }
                else
                {
                    this.lstEducationLevelDefault = value;
                }
            }
        }
    }
}