﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Web;

namespace WCF.Composite
{
    public class CalendarScheduleRespone
    {
        private List<CalendarSheduleLite> lstCalendarSheduleLite;
        /// <summary>
        /// ma loi tra ve
        /// </summary>
        [DataMember]
        public int ValidationCode { get; set; }
        [DataMember]
        public List<CalendarSheduleLite> LstCalendarShedule
        {
            get
            {
                return lstCalendarSheduleLite;
            }
            set
            {
                //Check null gan de thuc hien khoi tao 
                if (value == null)
                {
                    this.lstCalendarSheduleLite = new List<CalendarSheduleLite>();
                }
                else
                {
                    this.lstCalendarSheduleLite = value;
                }
            }
        }

        [DataContract]
        public class CalendarSheduleLite
        {
            /// <summary>
            /// Tiet hoc
            /// </summary>
            [DataMember]
            public int NumberOfPeriod { get; set; }
            /// <summary>
            /// Buoi hoc
            /// </summary>
            [DataMember]
            public int Section { get; set; }
            /// <summary>
            /// ID cua calendar Object
            /// </summary>
            [DataMember]
            public int CalendarID { get; set; }
            /// <summary>
            /// ID Mon hoc duoc chon
            /// </summary>
            [DataMember]
            public int SubjectID { get; set; }
            /// <summary>
            /// Ten mon hoc
            /// </summary>
            [DataMember]
            public string SubjectName { get; set; }
            /// <summary>
            /// Shedule ID
            /// </summary
            [DataMember]
            public int CalendarScheduleID { get; set; }
            /// <summary>
            /// Ten bai hoc
            /// </summary>
            [DataMember]
            public string SubjectTitle { get; set; }
            /// <summary>
            /// Noi dung chuan bi
            /// </summary>
            [DataMember]
            public string Preparation { get; set; }
            /// <summary>
            /// Bai tap ve nha
            /// </summary>
            [DataMember]
            public string HomeWork { get; set; }
            /// <summary>
            /// Ngay ap dung
            /// </summary>
            /// 
        }
    }
}