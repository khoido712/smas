﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Web;

namespace WCF.Composite
{
    [DataContract]
    public class SupervisingDeptDefault
    {
        /// <summary>
        /// Tài khoản
        /// </summary>
        [DataMember]
        public string SupervisingDeptUserName { get; set; }

        /// <summary>
        /// Địa chỉ
        /// </summary>
        [DataMember]
        public string Address { get; set; }

        /// <summary>
        /// User admin
        /// </summary>
        [DataMember]
        public int AdminID { get; set; }

        [DataMember]
        public int AreaID { get; set; }
        /// <summary>
        /// Tên miền
        /// </summary>
        [DataMember]
        public string AreaName { get; set; }


        [DataMember]
        public int DistrictID { get; set; }
        /// <summary>
        /// Huyện
        /// </summary>
        [DataMember]
        public string DistrictName { get; set; }

        /// <summary>
        /// Email
        /// </summary>
        [DataMember]
        public string Email { get; set; }

        /// <summary>
        /// Fax
        /// </summary>
        [DataMember]
        public string Fax { get; set; }

        /// <summary>
        /// HierachyLevel
        /// </summary>
        [DataMember]
        public int HierachyLevel { get; set; }

        /// <summary>
        /// ID đơn vị cha
        /// </summary>
        [DataMember]
        public int ParentID { get; set; }

        [DataMember]
        public int ProvinceID { get; set; }
        /// <summary>
        /// Tỉnh
        /// </summary>
        [DataMember]
        public string ProvinceName { get; set; }

        /// <summary>
        /// Mã
        /// </summary>
        [DataMember]
        public string SupervisingDeptCode { get; set; }

        /// <summary>
        /// ID
        /// </summary>
        [DataMember]
        public int SupervisingDeptID { get; set; }

        /// <summary>
        /// Tên
        /// </summary>
        [DataMember]
        public string SupervisingDeptName { get; set; }

        /// <summary>
        /// SĐT
        /// </summary>
        [DataMember]
        public string Telephone { get; set; }

        /// <summary>
        /// Website
        /// </summary>
        [DataMember]
        public string Website { get; set; }

        /// <summary>
        /// active SMAS
        /// </summary>
        [DataMember]
        public bool IsActiveSMAS { get; set; }

        /// <summary>
        /// Traver path
        /// </summary>
        [DataMember]
        public string TraversalPath { get; set; }

        /// <summary>
        /// loai hinh active smas
        /// </summary>
        [DataMember]
        public int SMSActiveType { get; set; }

        [DataMember]
        public DateTime EstablishedDate { get; set; }

    }

    /// <summary>
    /// DataContract gom danh sach thuoc tinh
    /// </summary>
    [DataContract]
    public class SupervisingDeptResponse
    {
        private SupervisingDeptDefault objSupervisingDept;
        /// <summary>
        /// ma loi tra ve
        /// </summary>
        [DataMember]
        public int ValidationCode { get; set; }
        /// <summary>
        /// Object dinh kem
        /// </summary>
        [DataMember]
        public SupervisingDeptDefault ObjSupervisingDept
        {
            get
            {
                return objSupervisingDept;
            }
            set
            {
                //Check null gan de thuc hien khoi tao 
                if (value == null)
                {
                    this.objSupervisingDept = new SupervisingDeptDefault();
                }
                else
                {
                    this.objSupervisingDept = value;
                }
            }
        }
    }


    [DataContract]
    public class SupervisingDeptLite
    {
        /// <summary>
        /// ID đơn vị cha
        /// </summary>
        [DataMember]
        public int ParentID { get; set; }

        /// <summary>
        /// ID
        /// </summary>
        [DataMember]
        public int SupervisingDeptID { get; set; }

        /// <summary>
        /// Tên
        /// </summary>
        [DataMember]
        public string SupervisingDeptName { get; set; }

        /// <summary>
        /// cap don vi
        /// </summary>
        [DataMember]
        public int HierachyLevel { get; set; }

        /// <summary>
        /// cay don vi
        /// </summary>
        [DataMember]
        public string TraversalPath { get; set; }
    }

    /// <summary>
    /// DataContract gom danh thong tin tra ve va ma ket qua
    /// </summary>
    [DataContract]
    public class ListSupervisingDeptLiteResponse
    {
        private List<SupervisingDeptLite> lstListSupervisingDeptLiteResponse;
        /// <summary>
        /// ma loi tra ve
        /// </summary>
        [DataMember]
        public int ValidationCode { get; set; }
        /// <summary>
        /// Object dinh kem
        /// </summary>
        [DataMember]
        public List<SupervisingDeptLite> LstListSupervisingDeptLiteResponse
        {
            get
            {
                return lstListSupervisingDeptLiteResponse;
            }
            set
            {
                //Check null gan de thuc hien new item
                if (value == null)
                {
                    this.lstListSupervisingDeptLiteResponse = new List<SupervisingDeptLite>();
                }
                else
                {
                    this.lstListSupervisingDeptLiteResponse = value;
                }
            }
        }
    }
}