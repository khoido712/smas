﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Web;

namespace WCF.Composite
{
    [DataContract]
    public class ReportComposite
    {

    }

    [DataContract]
    public class ReportObjectDefault
    {
        /// <summary>
        /// Ma truong
        /// </summary>
        [DataMember]
        public int SchoolID { get; set; }

        /// <summary>
        /// Ten Tinh
        /// </summary>
        [DataMember]
        public string ProvinceName { get; set; }

        /// <summary>
        /// Ten Huyen
        /// </summary>
        [DataMember]
        public string DistrictName { get; set; }

        /// <summary>
        /// Ten truong
        /// </summary>
        [DataMember]
        public string SchoolName { get; set; }

        /// <summary>
        /// Tai khoan
        /// </summary>
        [DataMember]
        public string UserName { get; set; }

        [DataMember]
        public int SoLopKhaiBao { get; set; }

        [DataMember]
        public int SoGVKhaiBao { get; set; }

        [DataMember]
        public int SoHSKhaiBao { get; set; }

        [DataMember]
        public int SoMonHocKhaiBao { get; set; }

        [DataMember]
        public int SoGVPhanCong { get; set; }

        [DataMember]
        public int SoHSXLHK1 { get; set; }

        [DataMember]
        public int SoHSXLHK2 { get; set; }

        [DataMember]
        public int SoHSXLCaNam { get; set; }
    }

    [DataContract]
    public class ListReportObjectResponse
    {
        private List<ReportObjectDefault> lstReportObjectDefault;
        /// <summary>
        /// ma loi tra ve
        /// </summary>
        [DataMember]
        public int ValidationCode { get; set; }
        /// <summary>
        /// Object dinh kem
        /// </summary>
        [DataMember]
        public List<ReportObjectDefault> LstReportObjectDefault
        {
            get
            {
                return lstReportObjectDefault;
            }
            set
            {
                //Check null gan de thuc hien khoi tao 
                if (value == null)
                {
                    this.lstReportObjectDefault = new List<ReportObjectDefault>();
                }
                else
                {
                    this.lstReportObjectDefault = value;
                }
            }
        }

    }
}