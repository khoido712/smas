﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Web;
using SMAS.Models.Models;

namespace WCF.Composite
{
    [DataContract]
    public class PupilOfClassComposite
    {
        [DataMember]
        public int PupilOfClassID { get; set; }
        [DataMember]
        public int PupilID { get; set; }
        [DataMember]
        public int ClassID { get; set; }
        [DataMember]
        public int SchoolID { get; set; }
        [DataMember]
        public int AcademicYearID { get; set; }
        [DataMember]
        public int EducationLevelID { get; set; }
        [DataMember]
        public Nullable<int> Year { get; set; }
        [DataMember]
        public Nullable<System.DateTime> AssignedDate { get; set; }
        [DataMember]
        public DateTime LeavingDate { get; set; }
        [DataMember]
        public Nullable<int> OrderInClass { get; set; }
        [DataMember]
        public string Description { get; set; }
        [DataMember]
        public Nullable<bool> NoConductEstimation { get; set; }
        [DataMember]
        public int Status { get; set; }
        [DataMember]
        public Nullable<int> FirstSemesterExemptType { get; set; }
        [DataMember]
        public Nullable<int> SecondSemesterExemptType { get; set; }
        [DataMember]
        public PupilProfile PupilProfile { get; set; }
        [DataMember]
        public bool? isShow { get; set; }
        [DataMember]
        public string Name { get; set; }
        [DataMember]
        public string PupilFullName { get; set; }
        [DataMember]
        public string PupilCode { get; set; }
        [DataMember]
        public DateTime? EndDate { get; set; }
        [DataMember]
        public string ClassName { get; set; }
        [DataMember]
        public int? Genre { get; set; }
        [DataMember]
        public int? EthnicID { get; set; }
        [DataMember]
        public DateTime Birthday { get; set; }
        [DataMember]
        public int? OrderPupil { get; set; }

        /// <summary>
        /// Dang ky HD
        /// </summary>     
        [DataMember]
        public bool? IsRegisterContract { get; set; }
    }
}