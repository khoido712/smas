﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Web;

namespace WCF.Composite
{
    [DataContract]
    public class EvaluationCommentsList
    {
        /// <summary>
        /// ma hoc sinh
        /// </summary>
        [DataMember]
        public int PupilID { get; set; }

        /// <summary>
        ///  ten hoc sinh
        /// </summary>
        [DataMember]
        public string PupilName { get; set; }

        /// <summary>
        /// chuoi nhan xet theo thang
        /// </summary>
        [DataMember]
        public string EvaluationMonth { get; set; }
    }

    [DataContract]
    public class EvaluationCommentsComposite
    {
        private List<EvaluationCommentsList> listResult;

        [DataMember]
        public int ValidateCode { get; set; }

        [DataMember]
        public List<EvaluationCommentsList> ListResult
        {
            get
            {
                return listResult;
            }
            set
            {
                //Check null gan de thuc hien khoi tao 
                if (value == null)
                {
                    this.listResult = new List<EvaluationCommentsList>();
                }
                else
                {
                    this.listResult = value;
                }
            }
        }



    }
}