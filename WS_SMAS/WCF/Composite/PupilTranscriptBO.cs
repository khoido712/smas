﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WCF.Composite
{
    public class PupilTranscriptBO
    {
        public int PupilProfileID { get; set; }
        public int ClassID { get; set; }
        public int SubjectID { get; set; }
        public string SubjectName { get; set; }
        public decimal? SummedUpMark { get; set; }
        public string JudgementResult { get; set; }
        public int? Semester { get; set; }
        public int IsCommenting { get; set; }
        public string Title { get; set; }
        public decimal? Mark { get; set; }
    }
}