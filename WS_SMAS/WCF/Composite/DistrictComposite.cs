﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Web;

namespace WCF.Composite
{
    [DataContract]
    public class DistrictDefault
    {
        [DataMember]
        public int DistrictID { get; set; }

        [DataMember]
        public string DistrictCode { get; set; }

        [DataMember]
        public string DistrictName { get; set; }

        [DataMember]
        public string ShortName { get; set; }

        [DataMember]
        public string Description { get; set; }

        [DataMember]
        public int ProvinceID { get; set; }

        [DataMember]
        public string ProvinceCode { get; set; }

        [DataMember]
        public string ProvinceName { get; set; }

        [DataMember]
        public string P_ShortName { get; set; }

        [DataMember]
        public string P_Description { get; set; }
    }

    [DataContract]
    public class DistrictResponse
    {
        private DistrictDefault objDistrict;
        /// <summary>
        /// ma loi tra ve
        /// </summary>
        [DataMember]
        public int ValidationCode { get; set; }
        /// <summary>
        /// Object dinh kem
        /// </summary>
        [DataMember]
        public DistrictDefault ObjDistrict
        {
            get
            {
                return objDistrict;
            }
            set
            {
                //Check null gan de thuc hien khoi tao 
                if (value == null)
                {
                    this.objDistrict = new DistrictDefault();
                }
                else
                {
                    this.objDistrict = value;
                }
            }
        }
    }

    [DataContract]
    public class ListDistrictResponse
    {
        private List<DistrictDefault> lstDistrictDefault;
        /// <summary>
        /// ma loi tra ve
        /// </summary>
        [DataMember]
        public int ValidationCode { get; set; }
        /// <summary>
        /// danh sach Object dinh kem
        /// </summary>
        [DataMember]
        public List<DistrictDefault> LstDistrictDefault
        {
            get
            {
                return lstDistrictDefault;
            }
            set
            {
                //Check null gan de thuc hien new item
                if (value == null)
                {
                    this.lstDistrictDefault = new List<DistrictDefault>();
                }
                else
                {
                    this.lstDistrictDefault = value;
                }
            }
        }
    }
}