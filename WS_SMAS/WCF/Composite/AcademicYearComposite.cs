﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Web;

namespace WCF.Composite
{
    [DataContract]
    public class AcademicYearDefault
    {
        /// <summary>
        /// ID
        /// </summary>
        [DataMember]
        public int AcademicYearID { get; set; }        

        /// <summary>
        /// Tên hiển thị
        /// </summary>
        [DataMember]
        public string DisplayTitle { get; set; }        

        /// <summary>
        /// Kết thúc kỳ I
        /// </summary>
        [DataMember]
        public DateTime FirstSemesterEndDate { get; set; }

        /// <summary>
        /// Bắt đầu kỳ I
        /// </summary>
        [DataMember]
        public DateTime FirstSemesterStartDate { get; set; }      

        /// <summary>
        /// ID trường
        /// </summary>
        [DataMember]
        public int SchoolID { get; set; }      
        /// <summary>
        /// hiển thị avatar
        /// </summary>
        [DataMember]
        public bool? IsShowAvatar { get; set; }
        /// <summary>
        /// hiển thị thời khóa biểu
        /// </summary>
        [DataMember]
        public bool? IsShowCalendar { get; set; }
        /// <summary>
        /// tên trường
        /// </summary>
  
        [DataMember]
        public string SchoolName { get; set; }

        /// <summary>
        /// Kết thúc kỳ II
        /// </summary>
        [DataMember]
        public DateTime SecondSemesterEndDate { get; set; }

        /// <summary>
        /// Bắt đầu kỳ II
        /// </summary>
        [DataMember]
        public DateTime SecondSemesterStartDate { get; set; }        

        /// <summary>
        /// Năm học
        /// </summary>
        [DataMember]
        public int Year { get; set; }
    }

    /// <summary>
    /// DataContract gom danh sach thuoc tinh
    /// </summary>
    [DataContract]
    public class AcademicYearResponse
    {
        private AcademicYearDefault objAcademicYear;
        /// <summary>
        /// ma loi tra ve
        /// </summary>
        [DataMember]
        public int ValidationCode { get; set; }
        /// <summary>
        /// Object dinh kem
        /// </summary>
        [DataMember]
        public AcademicYearDefault ObjAcademicYear
        {
            get
            {
                return objAcademicYear;
            }
            set
            {
                //Check null gan de thuc hien khoi tao 
                if (value == null)
                {
                    this.objAcademicYear = new AcademicYearDefault();
                }
                else
                {
                    this.objAcademicYear = value;
                }
            }
        }
    }

    [DataContract]
    public class ListAcademicYearResponse
    {
        private List<AcademicYearDefault> lstAcademicYearResponse;
        /// <summary>
        /// ma loi tra ve
        /// </summary>
        [DataMember]
        public int ValidationCode { get; set; }
        /// <summary>
        /// danh sach Object dinh kem
        /// </summary>
        [DataMember]
        public List<AcademicYearDefault> LstAcademicYearDefault
        {
            get
            {
                return lstAcademicYearResponse;
            }
            set
            {
                //Check null gan de thuc hien new item
                if (value == null)
                {
                    this.lstAcademicYearResponse = new List<AcademicYearDefault>();
                }
                else
                {
                    this.lstAcademicYearResponse = value;
                }
            }
        }
    }
}