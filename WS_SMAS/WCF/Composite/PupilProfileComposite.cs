﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Web;

namespace WCF.Composite
{
    #region Object Request
    [DataContract]
    public class PupilProfileRequest
    {
        /// <summary>
        /// danh sach ID học sinh
        /// </summary>
        [DataMember]
        public List<int> lstPupilProfileID { get; set; }

        /// <summary>
        /// ID truong
        /// </summary>
        [DataMember]
        public int SchoolID { get; set; }

        /// <summary>
        /// ID nam hoc
        /// </summary>
        [DataMember]
        public int AcademicYearID { get; set; }
    }

    [DataContract]
    public class InformationRequest
    {
        [DataMember]
        public int AcademicYearID { get; set; }
        [DataMember]
        public int SchoolID { get; set; }
        [DataMember]
        public int ClassID { get; set; }
        [DataMember]
        public int SemesterID { get; set; }
        [DataMember]
        public int PupilProfileID { get; set; }
        [DataMember]
        public int EducationGrade { get; set; }
        [DataMember]
        public int Year { get; set; }
        [DataMember]
        public bool IsLoadNewInfo { get; set; }
    }

    #endregion

    #region Object Response
    [DataContract]
    public class PupilByEducationLevel
    {
        /// <summary>
        ///  ID học sinh
        /// </summary>
        [DataMember]
        public int PupilProfileID { get; set; }

        /// <summary>
        /// ID lớp
        /// </summary>
        [DataMember]
        public int ClassID { get; set; }


        /// <summary>
        /// Tên lớp
        /// </summary>
        [DataMember]
        public string ClassName { get; set; }
    }

    [DataContract]
    public class PupilProfileClass
    {
        /// <summary>
        /// danh sach ID
        /// </summary>
        [DataMember]
        public int PupilProfileID { get; set; }

        /// <summary>
        /// Ten day du
        /// </summary>
        [DataMember]
        public string FullName { get; set; }

        [DataMember]
        public string Name { get; set; }

        /// <summary>
        /// Ten lop
        /// </summary>
        [DataMember]
        public string ClassName { get; set; }

        [DataMember]
        public int HeadTeacherID { get; set; }
		
		[DataMember]
        public string PupilCode { get; set; }

        [DataMember]
        public int OrderID { get; set; }

        [DataMember]
        public int ClassID { get; set; }
		
		[DataMember]
        public int EudcationLevelID { get; set; }
        [DataMember]
        public int Status { get; set; }


        [DataMember]
        public int OrderInClass { get; set; }
    }

    [DataContract]
    public class PupilProfileDefault
    {
        /// <summary>
        /// danh sach ID
        /// </summary>
        [DataMember]
        public int PupilProfileID { get; set; }

        /// <summary>
        /// Ten day du
        /// </summary>
        [DataMember]
        public string FullName { get; set; }

        /// <summary>
        /// Ten giao vien chu nhiem
        /// </summary>
        [DataMember]
        public int HeadTeacher { get; set; }

        /// <summary>
        /// Ten giao vien lam thay chu nhiem
        /// </summary>
        [DataMember]
        public int HeadTeacherSubstitution { get; set; }
		
		/// <summary>
        /// Ma hoc sinh
        /// </summary>
        [DataMember]
        public string PupilCode { get; set; }

        /// <summary>
        /// Lop hien tai
        /// </summary>
        [DataMember]
        public int ClassID { get; set; }

        /// <summary>
        /// Ten lop hien tai
        /// </summary>
        [DataMember]
        public string ClassName { get; set; }

        /// <summary>
        /// Trang thai hoc sinh
        /// </summary>
        [DataMember]
        public int Status { get; set; }

        /// <summary>
        /// ID chi tiet hoc sinh trong lop 
        /// </summary>
        [DataMember]
        public int PupilOfClassID { get; set; }

        /// <summary>
        /// Khoi hoc
        /// </summary>
        [DataMember]
        public int EducationLevelID { get; set; }
    }

    [DataContract]
    public class MarkOfPeriodDefault
    {
        /// <summary>
        /// danh sach ID
        /// </summary>
        [DataMember]
        public int PupilProfileID { get; set; }

        /// <summary>
        /// điểm đợt của học sinh trong lớp.Content format:
        /// Toán:<TBM>
        /// Thể dục:<HLM>
        /// Trung bình các môn:<8.0>
        /// Học lực:<Khá>
        /// Hạnh kiểm:<Tốt>
        /// Xếp hạng:<1>
        /// </summary>
        [DataMember]
        public string Content { get; set; }
    }

    [DataContract]
    public class MarkOfSemesterDefault
    {
        /// <summary>
        /// danh sach ID
        /// </summary>
        [DataMember]
        public int PupilProfileID { get; set; }

        /// <summary>
        /// điểm của học sinh trong lớp.Content format:
        /// Cấp 1:Toan:7(học lực) Thể dục:Đ,CĐ
        /// Hạnh kiểm:Đ
        /// Xếp loại giáo dục:Khá
        /// Nghỉ có phép:1
        /// Nghỉ không phép:1
        /// Cấp 2,3:Toán:<TBM> Thể dục<HLM>
        /// Trung bình các môn:<8,0>
        /// Học lực:<Khá>
        /// Hạnh kiểm:<Tốt>
        /// Xếp hạng:<1>
        /// Danh hiệu thi đua:<xuất sắc>>
        /// </summary>
        [DataMember]
        public string Content { get; set; }
    }

    [DataContract]
    public class PupilAbsentResponse
    {
        /// <summary>
        ///  ID hoc sinh
        /// </summary>
        [DataMember]
        public int PupilProfileID { get; set; }

        /// <summary>
        /// - nghi co phep:<soluong>
        /// - nghi khong phep:<soluong>
        /// </summary>
        [DataMember]
        public string Absent { get; set; }
    }
    [DataContract]
    public class PupilRankingLite
    {
        public int PeriodID { get; set; }
        [DataMember]
        public int PupilProfileID { get; set; }

        [DataMember]
        public string AverageHKI { get; set; }
        [DataMember]
        public string CapacityHKI { get; set; }
        [DataMember]
        public string ConductHKI { get; set; }
        [DataMember]
        public string AverageHKII { get; set; }
        [DataMember]
        public string CapacityHKII { get; set; }
        [DataMember]
        public string ConductHKII { get; set; }
        [DataMember]
        public string AverageCN { get; set; }
        [DataMember]
        public string CapacityCN { get; set; }
        [DataMember]
        public string ConductCN { get; set; }


    }
    [DataContract]
    public class MarkRecordDefault
    {
        /// <summary>
        /// danh sach ID
        /// </summary>
        [DataMember]
        public int PupilProfileID { get; set; }

        /// <summary>
        /// Cấp 1:Toán T1,2,3 GK T4,5 CK
        /// Thể dục NX1,2,3,4,5
        /// Cấp 2,3:Toán M P V KTHK 
        /// Thể Dục M P V KTHK
        /// - nghi co phep:2
        /// - nghi khong phep:1
        /// </summary>
        [DataMember]
        public string Content { get; set; }
    }
    [DataContract]
    public class MarkRecordLite
    {
        [DataMember]
        public int? SubjectID { get; set; }
        
        [DataMember]
        public string SubjectName { get; set; }
        
        [DataMember]
        public string SummedUpMark { get; set; }
        
        [DataMember]
        public string JudgementResult { get; set; }
        
        [DataMember]
        public int IsCommenting { get; set; }
        
        [DataMember]
        public int? SemesterID { get; set; }
        
        [DataMember]
        public List<MarkColumnResponse> DiemMieng { get; set; }
        
        [DataMember]
        public List<MarkColumnResponse> Diem15P { get; set; }
        
        [DataMember]
        public List<MarkColumnResponse> Diem1T { get; set; }
        
        [DataMember]
        public string DiemCuoiKy { get; set; }
        
        [DataMember]
        public string DiemTBHKI { get; set; }
        
        [DataMember]
        public string DiemTBHKII { get; set; }
        
        [DataMember]
        public string DiemTBCN { get; set; }
    }

    /// <summary>
    /// object de luu con diem tra ve theo thu tu cot diem va diem
    /// </summary>
    /// <author date="2014/03/07">HaiVT</author>
    [DataContract]
    public class MarkColumnResponse
    {
        /// <summary>
        /// thu tu con diem
        /// </summary>
        [DataMember]
        public int Order { get; set; }

        /// <summary>
        /// gia tri diem
        /// </summary>
        [DataMember]
        public string Mark { get; set; }
    }

    [DataContract]
    public class PupilProfile4RegisterLite
    {
        /// <summary>
        /// ID học sinh
        /// </summary>
        [DataMember]
        public int PupilProfileID { get; set; }

        /// <summary>
        /// Tên đầy đủ học sinh
        /// </summary>
        [DataMember]
        public string FullName { get; set; }

        /// <summary>
        /// Tình trạng
        /// </summary>
        [DataMember]
        public int Status { get; set; }

        /// <summary>
        /// ma hoc sinh
        /// </summary>
        [DataMember]
        public string PupilCode { get; set; }

        /// <summary>
        /// id truong hien tai
        /// </summary>
        [DataMember]
        public int SchoolID { get; set; }

        /// <summary>
        /// id tinh
        /// </summary>
        [DataMember]
        public int ProvinceID { get; set; }

        /// <summary>
        /// ID nam hoc
        /// </summary>
        [DataMember]
        public int AcademicYearID { get; set; }

        /// <summary>
        /// Nam cua nam hoc
        /// </summary>
        [DataMember]
        public int Year { get; set; }

        /// <summary>
        /// id truong hien tai
        /// </summary>
        [DataMember]
        public string SchoolCode { get; set; }

        /// <summary>
        /// id truong hien tai
        /// </summary>
        [DataMember]
        public string SchoolName { get; set; }

        /// <summary>
        /// Kich hoat dich vu SMS Parent
        /// </summary>
        [DataMember]
        public int SMSParentActiveType { get; set; }

        /// <summary>
        /// Dong y viec dang ky dich vu qua SMS
        /// </summary>
        [DataMember]
        public int SMSParentRegisterType { get; set; }

        /// <summary>
        /// Ho ten bo
        /// </summary>
        [DataMember]
        public string FatherName { get; set; }

        /// <summary>
        /// Ho ten me
        /// </summary>
        [DataMember]
        public string MotherName { get; set; }

        /// <summary>
        /// Ho ten do dau
        /// </summary>
        [DataMember]
        public string SponsorName { get; set; }

        /// <summary>
        /// SDT bo
        /// </summary>
        [DataMember]
        public string FatherPhone { get; set; }

        /// <summary>
        /// SDT me
        /// </summary>
        [DataMember]
        public string MotherPhone { get; set; }

        /// <summary>
        /// SDT do dau
        /// </summary>
        [DataMember]
        public string SponsorPhone { get; set; }
    }

    [DataContract]
    public class PupilProfileLite
    {
        /// <summary>
        /// ID học sinh
        /// </summary>
        [DataMember]
        public int PupilProfileID { get; set; }

        /// <summary>
        /// ID lớp học hiện tại
        /// </summary>
        [DataMember]
        public int CurrentClassID { get; set; }

        [DataMember]
        public string CurrentClassName { get; set; }

        /// <summary>
        /// ID năm học hiện tại
        /// </summary>
        [DataMember]
        public int CurrentAcademicYearID { get; set; }

        /// <summary>
        /// Tên đầy đủ học sinh
        /// </summary>
        [DataMember]
        public string FullName { get; set; }

        /// <summary>
        /// Tình trạng
        /// </summary>
        [DataMember]
        public int Status { get; set; }

        /// <summary>
        /// ma hoc sinh
        /// </summary>
        [DataMember]
        public string PupilCode { get; set; }

        /// <summary>
        /// id truong hien tai
        /// </summary>
        [DataMember]
        public int CurrentSchoolID { get; set; }

        /// <summary>
        /// ngay sinh
        /// </summary>
        [DataMember]
        public DateTime Birthday { get; set; }

        /// <summary>
        /// ID hoc sinh trong lop
        /// </summary>
        [DataMember]
        public int PupilOfClassID { get; set; }

        [DataMember]
        public int Genre { get; set; }
    }
    [DataContract]
    public class PupilProfileDetail
    {
        [DataMember]
        public int? OrderNumber { get; set; }        
        [DataMember]
        public int PupilID { get; set; }
        [DataMember]
        public int LevelID { get; set; }
        [DataMember]
        public string PupilCode { get; set; }
        [DataMember]
        public string FullName { get; set; }
        [DataMember]
        public DateTime? BirthDate { get; set; }
        [DataMember]
        public bool? Sex { get; set; }
        [DataMember]
        public int SchoolID { get; set; }
        [DataMember]
        public string SchoolName { get; set; }
        [DataMember]
        public int? ClassID { get; set; }
        [DataMember]
        public string ClassName { get; set; }
        [DataMember]
        public int? ClassType { get; set; }
        [DataMember]
        public string FamilyTypeName { get; set; }
        [DataMember]
        public int EducationLevel { get; set; }
        [DataMember]
        public int? UnderLevel { get; set; }
        [DataMember]
        public bool? StatusGuardian { get; set; }
        [DataMember]
        public DateTime? DateYPT { get; set; }
        [DataMember]
        public DateTime? dateCP { get; set; }//ngay ket nap dang
        [DataMember]
        public DateTime? DateCYU { get; set; }
        [DataMember]
        public DateTime? DateInto { get; set; }
        [DataMember]
        public int? TypeInto { get; set; }
        [DataMember]
        public int? TrainingType { get; set; }

        [DataMember]
        public int CurrentAcademicYearID { get; set; }
        [DataMember]
        public int? AreaID { get; set; }
        [DataMember]
        public string AreaName { get; set; }
        [DataMember]
        public int CurrentStatus { get; set; }
        [DataMember]
        public int ProvinceID { get; set; }
        [DataMember]
        public Nullable<int> DistrictID { get; set; }
        [DataMember]
        public Nullable<int> CommuneID { get; set; }
        [DataMember]
        public int? EthnicID { get; set; }
        [DataMember]
        public int? ReligionID { get; set; }
        [DataMember]
        public Nullable<short> PolicyTargetID { get; set; }
        [DataMember]
        public int? PolicyType { get; set; }
        [DataMember]
        public string PolicyRegimeName { get; set; }
        [DataMember]
        public int? FamilyTypeID { get; set; }
        [DataMember]
        public Nullable<short> PriorityTypeID { get; set; }
        [DataMember]
        public string PriorityName { get; set; }
        [DataMember]
        public Nullable<short> PreviousGraduationLevelID { get; set; }
        [DataMember]
        public int Genre { get; set; }
        [DataMember]
        public string BirthPlace { get; set; }
        [DataMember]
        public string HomeTown { get; set; }
        [DataMember]
        public string Telephone { get; set; }
        [DataMember]
        public string Mobile { get; set; }
        [DataMember]
        public string Email { get; set; }
        [DataMember]
        public string TempResidentalAddress { get; set; }
        [DataMember]
        public string PermanentResidentalAddress { get; set; }
        [DataMember]
        public Boolean? IsDisabled { get; set; }
        [DataMember]
        public int? BloodType { get; set; }
        [DataMember]
        public decimal? EnrolmentMark { get; set; }
        [DataMember]
        public byte ForeignLanguageTraining { get; set; }
        [DataMember]
        public Boolean? IsYoungPioneerMember { get; set; }
        [DataMember]
        public DateTime? YoungPioneerJoinedDate { get; set; }
        [DataMember]
        public string YoungPioneerJoinedPlace { get; set; }
        [DataMember]
        public Boolean? IsYouthLeageMember { get; set; }
        [DataMember]
        public DateTime? YouthLeagueJoinedDate { get; set; }
        [DataMember]
        public string YouthLeagueJoinedPlace { get; set; }
        [DataMember]
        public Boolean? IsCommunistPartyMember { get; set; }
        [DataMember]
        public DateTime? CommunistPartyJoinedDate { get; set; }
        [DataMember]
        public string CommunistPartyJoinedPlace { get; set; }
        [DataMember]
        public string FatherFullName { get; set; }
        [DataMember]
        public DateTime? FatherBirthDate { get; set; }
        [DataMember]
        public string FatherJob { get; set; }
        [DataMember]
        public string MotherFullName { get; set; }
        [DataMember]
        public string FatherMobile { get; set; }
        [DataMember]
        public DateTime? MotherBirthDate { get; set; }
        [DataMember]
        public string MotherJob { get; set; }
        [DataMember]
        public string MotherMobile { get; set; }
        [DataMember]
        public string SponsorFullName { get; set; }
        [DataMember]
        public DateTime? SponsorBirthDate { get; set; }
        [DataMember]
        public string SponsorJob { get; set; }
        [DataMember]
        public int ProfileStatus { get; set; }
        [DataMember]
        public string LangCertificates { get; set; }
        [DataMember]
        public string CentificateIT { get; set; }
        [DataMember]
        public int? Studytype { get; set; }
        [DataMember]
        public int? LanguageLearning { get; set; }
        [DataMember]
        public Boolean IsActive { get; set; }
        [DataMember]
        public DateTime? ModifiedDate { get; set; }
        [DataMember]
        public int EducationLevelID { get; set; }
        [DataMember]
        public Nullable<Decimal> HeightAtBirth { get; set; }
        [DataMember]
        public Nullable<Decimal> WeightAtBirth { get; set; }
        [DataMember]
        public string FatherEmail { get; set; }
        [DataMember]
        public string MotherEmail { get; set; }
        [DataMember]
        public string SponsorEmail { get; set; }
        [DataMember]
        public Nullable<Byte> ChildOrder { get; set; }
        [DataMember]
        public Nullable<Byte> NumberOfChild { get; set; }
        [DataMember]
        public Nullable<Byte> EatingHabit { get; set; }
        [DataMember]
        public Nullable<Byte> ReactionTrainingHabit { get; set; }
        [DataMember]
        public Nullable<Byte> AttitudeInStrangeScene { get; set; }
        [DataMember]
        public string OtherHabit { get; set; }
        [DataMember]
        public string FavoriteToy { get; set; }
        [DataMember]
        public Nullable<Int32> EvaluationConfigID { get; set; }
        [DataMember]
        public string Name { get; set; }

        [DataMember]
        public Nullable<bool> IsResidentIn { get; set; }
        [DataMember]
        public int? DisabledTypeID { get; set; }
        [DataMember]
        public string DisabledTypeName { get; set; }
        [DataMember]
        public string DisabledSeverity { get; set; }
        [DataMember]
        public System.DateTime? EnrolmentDate { get; set; }
        [DataMember]
        public string SponsorMobile { get; set; }
        [DataMember]
        public System.DateTime CreatedDate { get; set; }
        [DataMember]
        public Nullable<byte> EnrolmentType { get; set; }
        [DataMember]
        public int? OrderInClass { get; set; }
        [DataMember]
        //public PupilOfClass PupilOfClass { get; set; }
        public string EthnicName { get; set; }
        [DataMember]
        public string ReligionName { get; set; }
        [DataMember]
        public string FamilyName { get; set; }
        [DataMember]
        public string PolicyTargetName { get; set; }
        [DataMember]
        public string ProvinceName { get; set; }
        [DataMember]
        public string DistrictName { get; set; }
        [DataMember]
        public string CommuneName { get; set; }
        [DataMember]
        public Nullable<int> PolicyRegimeID { get; set; }
        [DataMember]
        public bool IsSupportForLearning { get; set; }
        [DataMember]
        public bool? IsFatherSMS { get; set; }
        [DataMember]
        public bool? IsMotherSMS { get; set; }
        [DataMember]
        public bool? IsSponsorSMS { get; set; }
        [DataMember]
        public string EducationLevelName { get; set; }
        [DataMember]
        public bool? IsRegisterContract { get; set; }

        /// <summary>
        /// tutv them vao dung de update cot isregiscontract khi co dang ky hop dong
        /// </summary>
        [DataMember]
        public int PupilOfClassID { get; set; }
    }
    [DataContract]
    public class PupilOfClassLite
    {
        [DataMember]
        public int AcademicYearID { get; set; }
        [DataMember]
        public int? Year { get; set; }
        [DataMember]
        public string SchoolName { get; set; }
        [DataMember]
        public int ClassID { get; set; }
        [DataMember]
        public string ClassName { get; set; }
        [DataMember]
        public int EducationLevelID { get; set; }
        [DataMember]
        public int EducationGrade { get; set; }
        [DataMember]
        public int? Section { get; set; }
    }

    [DataContract]
    public class PupilTrainingDefault
    {
        /// <summary>
        /// ID học sinh
        /// </summary>
        [DataMember]
        public int PupilProfileID { get; set; }

        /// <summary>
        /// Thông tin về học sinh có thể có: vi phạm, điểm danh, ...
        /// </summary>
        [DataMember]
        public string Content { get; set; }

    }

    [DataContract]
    public class PupilFaultAbsenceExempt
    {
        /// <summary>
        /// ma hoc sinh
        /// </summary>
        [DataMember]
        public int PupilProfileID { get; set; }

        /// <summary>
        /// ten vi pham
        /// </summary>
        [DataMember]
        public string FaultName { get; set; }

        /// <summary>
        /// so lan vi pham
        /// </summary>
        [DataMember]
        public int NumberOfFault { get; set; }

        /// <summary>
        /// ngay vi pham
        /// </summary>
        [DataMember]
        public string FaultDate { get; set; }

        /// <summary>
        /// ngay nghi hoc
        /// </summary>
        [DataMember]
        public string AbsenceDate { get; set; }

        /// <summary>
        /// buoi nghi hoc
        /// </summary>
        [DataMember]
        public int AbsenceSection { get; set; }

        /// <summary>
        /// nghi co phep : P, khong phep K
        /// </summary>
        [DataMember]
        public string AbsenceType { get; set; }

        /// <summary>
        /// mon hoc mien giam
        /// </summary>
        [DataMember]
        public string ExemptName { get; set; }

        /// <summary>
        /// mien giam hoc ky 1
        /// </summary>
        [DataMember]
        public string ExemptFirstSemester { get; set; }


        /// <summary>
        /// mien giam hoc ky 2
        /// </summary>
        [DataMember]
        public string ExemptSecondSemester { get; set; }
    }

    [DataContract]
    public class CalendarLite
    {
        [DataMember]
        public int DayOfWeek { get; set; }
        [DataMember]
        public int? Section { get; set; }
        [DataMember]
        public int? SubjectOrder { get; set; }
        [DataMember]
        public int? NumberOfPeriod { get; set; }
        [DataMember]
        public string SubjectName { get; set; }
        [DataMember]
        public string Color { get; set; }

    }

    [DataContract]
    public class NewInformationOfPupil
    {
        [DataMember]
        public int PupilProfileID { get; set; }
        [DataMember]
        public int OrderID { get; set; }
        [DataMember]
        public int ClassID { get; set; }
        [DataMember]
        public int InforID { get; set; }
        [DataMember]
        public DateTime? ActionDate { get; set; }
        [DataMember]
        public int Type { get; set; }
        [DataMember]
        public string TypeName { get; set; }
        [DataMember]
        public string Content { get; set; }

    }
    [DataContract]
    public class CommentOfPupilResponse
    {
        [DataMember]
        public int PupilID { get; set; }
        [DataMember]
        public int ClassID { get; set; }
        [DataMember]
        public int SubjectID { get; set; }
        [DataMember]
        public string Comment { get; set; }
        [DataMember]
        public DateTime DateTimeComment { get; set; }
        [DataMember]
        public int SemesterID { get; set; }
    }
    #endregion

    #region Object Response kem validation code

    /// <summary>
    /// DataContract gom danh sach thuoc tinh
    /// </summary>
    [DataContract]
    public class ListPupilProfileLiteResponse
    {
        private List<PupilProfileLite> lstPupilProfileLite;
        private List<PupilOfClassLite> lstPupilOfClass;
        private List<PupilProfileDetail> lstPupilProfileDetail;
        /// <summary>
        /// ma loi tra ve
        /// </summary>
        [DataMember]
        public int ValidationCode { get; set; }

        /// <summary>
        /// Tong so trang neu chon tim kiem theo khoi
        /// </summary>
        [DataMember]
        public int TotalRecord { get; set; }

        /// <summary>
        /// Object dinh kem
        /// </summary>
        [DataMember]
        public List<PupilProfileLite> LstPupilProfileLite
        {
            get
            {
                return lstPupilProfileLite;
            }
            set
            {
                //Check null gan de thuc hien khoi tao 
                if (value == null)
                {
                    this.lstPupilProfileLite = new List<PupilProfileLite>();
                }
                else
                {
                    this.lstPupilProfileLite = value;
                }
            }
        }
        [DataMember]
        public List<PupilOfClassLite> LstPupilOfClass
        {
            get 
            {
                return lstPupilOfClass;
            }
            set 
            {
                if (value == null)
                {
                    lstPupilOfClass = new List<PupilOfClassLite>();
                }
                else
                {
                    lstPupilOfClass = value;
                }
            }
        }
        /// <summary>
        /// Object dinh kem
        /// </summary>
        [DataMember]
        public List<PupilProfileDetail> listPupilProfileDetail
        {
            get
            {
                return lstPupilProfileDetail;
            }
            set
            {
                //Check null gan de thuc hien khoi tao 
                if (value == null)
                {
                    this.lstPupilProfileDetail = new List<PupilProfileDetail>();
                }
                else
                {
                    this.lstPupilProfileDetail = value;
                }
            }
        }

    }
    [DataContract]
    public class PupilOfClassResponse
    {
        private PupilOfClassLite objPupilOfClass;
        [DataMember]
        public int ValidationCode{get;set;}
        [DataMember]
        public PupilOfClassLite ObjPupilOfClass
        {
            get
            {
                return objPupilOfClass;
            }
            set
            {
                if (value == null)
	            {
		            objPupilOfClass = new PupilOfClassLite();
	            }
                else
	            {
                    objPupilOfClass = value;
	            }
            }
        }
    }

    /// <summary>
    /// DataContract gom danh sach thuoc tinh
    /// </summary>
    [DataContract]
    public class ListPupilProfileResponse
    {
        private List<PupilProfileDefault> lstPupilProfileDefault;
        /// <summary>
        /// ma loi tra ve
        /// </summary>
        [DataMember]
        public int ValidationCode { get; set; }
        /// <summary>
        /// Object dinh kem
        /// </summary>
        [DataMember]
        public List<PupilProfileDefault> LstPupilProfileDefault
        {
            get
            {
                return lstPupilProfileDefault;
            }
            set
            {
                //Check null gan de thuc hien khoi tao 
                if (value == null)
                {
                    this.lstPupilProfileDefault = new List<PupilProfileDefault>();
                }
                else
                {
                    this.lstPupilProfileDefault = value;
                }
            }
        }

    }

    /// <summary>
    /// DataContract gom thong tin hoc sinh phuc vu dang ky dich vu qua SMS
    /// </summary>
    [DataContract]
    public class PupilProfile4RegisterResponse
    {
        private List<PupilProfile4RegisterLite> objPupilProfile4RegisterLite;
        /// <summary>
        /// ma loi tra ve
        /// </summary>
        [DataMember]
        public int ValidationCode { get; set; }
        /// <summary>
        /// Object dinh kem
        /// </summary>
        [DataMember]
        public List<PupilProfile4RegisterLite> ObjPupilProfile4RegisterLite
        {
            get
            {
                return objPupilProfile4RegisterLite;
            }
            set
            {
                //Check null gan de thuc hien khoi tao 
                if (value == null)
                {
                    this.objPupilProfile4RegisterLite = new List<PupilProfile4RegisterLite>();
                }
                else
                {
                    this.objPupilProfile4RegisterLite = value;
    }
            }
        }
    }

    [DataContract]
    public class GridTableResponse
    {
        private List<PupilProfileLite> _pupilProfileLiteList;
        /// <summary>
        /// ma loi tra ve
        /// </summary>
        [DataMember]
        public int ValidationCode { get; set; }

        /// <summary>
        /// Object dinh kem
        /// </summary>
        [DataMember]
        public List<PupilProfileLite> PupilProfileLiteList
        {
            get
            {
                if (_pupilProfileLiteList == null)
                {
                    return new List<PupilProfileLite>();
                }
                else
                {
                    return _pupilProfileLiteList;
                }
            }
            set { _pupilProfileLiteList = value; }
            
        }

        [DataMember]
        public int Total { get; set; }

        [DataMember]
        public int Page { get; set; }

        [DataMember]
        public int Size { get; set; }

    }

    /// <summary>
    /// DataContract gom thuoc tinh
    /// </summary>
    [DataContract]
    public class PupilProfileLiteResponse
    {
        private PupilProfileLite objPupilProfileLite;
        private PupilProfileDetail objPupilProfileDetail;
        /// <summary>
        /// ma loi tra ve
        /// </summary>
        [DataMember]
        public int ValidationCode { get; set; }
        /// <summary>
        /// Object dinh kem
        /// </summary>
        [DataMember]
        public PupilProfileLite ObjPupilProfileLite
        {
            get
            {
                return objPupilProfileLite;
            }
            set
            {
                //Check null gan de thuc hien khoi tao 
                if (value == null)
                {
                    this.objPupilProfileLite = new PupilProfileLite();
                }
                else
                {
                    this.objPupilProfileLite = value;
                }
            }
        }
        /// <summary>
        /// Object hoc sinh cho Sparent
        /// </summary>
        [DataMember]
        public PupilProfileDetail ObjPupilProfileDetail
        {
            get
            {
                return objPupilProfileDetail;
            }
            set
            {
                if (value == null)
                {
                    objPupilProfileDetail = new PupilProfileDetail();
                }
                else
                {
                    objPupilProfileDetail = value;
                }
            }
        }
    }

    /// <summary>
    /// DataContract gom danh sach thuoc tinh
    /// </summary>
    [DataContract]
    public class ListMarkOfPeriodResponse
    {
        private List<MarkOfPeriodDefault> lstMarkOfPeriodDefault;
        /// <summary>
        /// ma loi tra ve
        /// </summary>
        [DataMember]
        public int ValidationCode { get; set; }
        /// <summary>
        /// Object dinh kem
        /// </summary>
        [DataMember]
        public List<MarkOfPeriodDefault> LstMarkOfPeriodDefault
        {
            get
            {
                return lstMarkOfPeriodDefault;
            }
            set
            {
                //Check null gan de thuc hien khoi tao 
                if (value == null)
                {
                    this.lstMarkOfPeriodDefault = new List<MarkOfPeriodDefault>();
                }
                else
                {
                    this.lstMarkOfPeriodDefault = value;
                }
            }
        }
    }

    /// <summary>
    /// DataContract gom danh sach thuoc tinh
    /// </summary>
    [DataContract]
    public class ListMarkOfSemesterResponse
    {
        private List<MarkOfSemesterDefault> lstMarkOfSemesterDefault;
        /// <summary>
        /// ma loi tra ve
        /// </summary>
        [DataMember]
        public int ValidationCode { get; set; }
        /// <summary>
        /// Object dinh kem
        /// </summary>
        [DataMember]
        public List<MarkOfSemesterDefault> LstMarkOfSemesterDefault
        {
            get
            {
                return lstMarkOfSemesterDefault;
            }
            set
            {
                //Check null gan de thuc hien khoi tao 
                if (value == null)
                {
                    this.lstMarkOfSemesterDefault = new List<MarkOfSemesterDefault>();
                }
                else
                {
                    this.lstMarkOfSemesterDefault = value;
                }
            }
        }
    }

    /// <summary>
    /// DataContract gom danh sach thuoc tinh
    /// </summary>
    [DataContract]
    public class ListMarkRecordResponse
    {
        private List<MarkRecordDefault> lstMarkRecordDefault;
        private List<PupilRankingLite> lstPupilRankingLite;
        private List<MarkRecordLite> lstMarkRecordLite;
        /// <summary>
        /// ma loi tra ve
        /// </summary>
        [DataMember]
        public int ValidationCode { get; set; }
        /// <summary>
        /// Object dinh kem
        /// </summary>
        [DataMember]
        public List<MarkRecordDefault> LstMarkRecordDefault
        {
            get
            {
                return lstMarkRecordDefault;
            }
            set
            {
                //Check null gan de thuc hien khoi tao 
                if (value == null)
                {
                    this.lstMarkRecordDefault = new List<MarkRecordDefault>();
                }
                else
                {
                    this.lstMarkRecordDefault = value;
                }
            }
        }
        [DataMember]
        public List<PupilRankingLite> LstPupilRankingLite
        {
            get 
            {
                return lstPupilRankingLite;
            }
            set 
            {
                if (value == null)
                {
                    lstPupilRankingLite = new List<PupilRankingLite>();
                }
                else
                {
                    lstPupilRankingLite = value;
                }
            }
        }
        [DataMember]
        public List<MarkRecordLite> LstMarkRecordLite
        {
            get
            {
                return lstMarkRecordLite;
            }
            set
            {
                if (value == null)
                {
                    lstMarkRecordLite = new List<MarkRecordLite>();
                }
                else
                {
                    lstMarkRecordLite = value;
                }
            }
        }
    }
    /// <summary>
    /// ListGoodBadSubject
    /// </summary>
    public class ListGoodBadSubjectResponse
    {
        private List<string> lstGoodSubject;
        private List<string> lstBadSubject;
        [DataMember]
        public int ValidateCode { get; set; }
        [DataMember]
        public List<string> LstGoodSubject
        {
            get
            {
                return lstGoodSubject;
            }
            set
            {
                if (value == null)
                {
                    lstGoodSubject = new List<string>();
                }
                else
                {
                    lstGoodSubject = value;
                }
            }
        }
        public List<string> LstBadSubject
        {
             get
            {
                return lstBadSubject;
            }
            set
            {
                if (value == null)
                {
                    lstBadSubject = new List<string>();
                }
                else
                {
                    lstBadSubject = value;
                }
            }
        }
    }
    /// <summary>
    /// lay nhung mon hoc tot nhat va yeu nhat
    /// </summary>
    [DataContract]
    public class GoodBadSubject
    {
        [DataMember]
        public int SubjectID { get; set; }
        [DataMember]
        public string SubjectName { get; set; }
        [DataMember]
        public int? SubjectOrder { get; set; }
        [DataMember]
        public int? SemesterID { get; set; }
        [DataMember]
        public int IsCommiting { get; set; }
        [DataMember]
        public int? IsCommentSubject { get; set; }
        [DataMember]
        public decimal? TotalMark { get; set; }
    }

    /// <summary>
    /// DataContract gom danh sach thuoc tinh
    /// </summary>
    [DataContract]
    public class ListPupilTrainingResponse
    {
        private List<PupilTrainingDefault> lstPupilTrainingDefault;
        /// <summary>
        /// ma loi tra ve
        /// </summary>
        [DataMember]
        public int ValidationCode { get; set; }
        /// <summary>
        /// Object dinh kem
        /// </summary>
        [DataMember]
        public List<PupilTrainingDefault> LstPupilTrainingDefault
        {
            get
            {
                return lstPupilTrainingDefault;
            }
            set
            {
                //Check null gan de thuc hien khoi tao 
                if (value == null)
                {
                    this.lstPupilTrainingDefault = new List<PupilTrainingDefault>();
                }
                else
                {
                    this.lstPupilTrainingDefault = value;
                }
            }
        }
    }

    /// <summary>
    /// DataContract gom danh sach thuoc tinh
    /// </summary>
    [DataContract]
    public class ListPupilByEducationLevelResponse
    {
        private List<PupilByEducationLevel> lstPupilByEducationLevel;
        
        /// <summary>
        /// ma loi tra ve
        /// </summary>
        [DataMember]
        public int ValidationCode { get; set; }
        
        /// <summary>
        /// Object dinh kem
        /// </summary>
        [DataMember]
        public List<PupilByEducationLevel> LstPupilByEducationLevel
        {
            get
            {
                return lstPupilByEducationLevel;
            }
            set
            {
                //Check null gan de thuc hien khoi tao 
                if (value == null)
                {
                    this.lstPupilByEducationLevel = new List<PupilByEducationLevel>();
                }
                else
                {
                    this.lstPupilByEducationLevel = value;
                }
            }
        }
    }

    /// <summary>
    /// DataContract gom danh sach thuoc tinh
    /// </summary>
    [DataContract]
    public class ListPupilProfileClassResponse
    {
        private List<PupilProfileClass> lstPupilProfileClass;

        /// <summary>
        /// ma loi tra ve
        /// </summary>
        [DataMember]
        public int ValidationCode { get; set; }

        /// <summary>
        /// Object dinh kem
        /// </summary>
        [DataMember]
        public List<PupilProfileClass> LstPupilProfileClass
        {
            get
            {
                return lstPupilProfileClass;
            }
            set
            {
                //Check null gan de thuc hien khoi tao 
                if (value == null)
                {
                    this.lstPupilProfileClass = new List<PupilProfileClass>();
                }
                else
                {
                    this.lstPupilProfileClass = value;
                }
            }
        }
    }

    [DataContract]
    public class ListCalendarResponse
    {
        [DataMember]
        public int ValidationCode { get; set; }

        private List<CalendarLite> _lstCalendar;

        [DataMember]
        public List<CalendarLite> LstCalendar
        {
            get
            {
                return _lstCalendar;
            }
            set
            {
                if (value == null)
                {
                    this._lstCalendar = new List<CalendarLite>();
                }
                else
                {
                    this._lstCalendar = value;
                }
            }
        }
    }

    [DataContract]
    public class NewInformationOfPupilResponse
    {
        [DataMember]
        public int ValidationCode { get; set; }
        private List<NewInformationOfPupil> _lstInformationOfPupil;
        [DataMember]
        public List<NewInformationOfPupil> lstInformationOfPupil
        {
            get
            {
                return _lstInformationOfPupil;
            }
            set
            {
                if (value == null)
	            {
                    this._lstInformationOfPupil = new List<NewInformationOfPupil>();
	            }
                else
	            {
                    this._lstInformationOfPupil = value;
	            }
            }
        }
    }

    public class PupilFaultAbsenceExemptResponse
    {
        private List<PupilFaultAbsenceExempt> _pupilFaultAbsenceExempt;

        /// <summary>
        /// ma loi tra ve
        /// </summary>
        [DataMember]
        public int ValidationCode { get; set; }

        /// <summary>
        /// Object dinh kem
        /// </summary>
        [DataMember]
        public List<PupilFaultAbsenceExempt> PupilFaultAbsenceExempt
        {
            get
            {
                return _pupilFaultAbsenceExempt;
            }
            set
            {
                //Check null gan de thuc hien khoi tao 
                if (value == null)
                {
                    this._pupilFaultAbsenceExempt = new List<PupilFaultAbsenceExempt>();
                }
                else
                {
                    this._pupilFaultAbsenceExempt = value;
                }
            }
        }
    }

    [DataContract]
    public class PupilPraiseDisciplineDefault
    {
        /// <summary>
        /// id hoc sinh
        /// </summary>
        [DataMember]
        public int PupilID { get; set; }

        /// <summary>
        /// ngay khen thuong
        /// </summary>
        [DataMember]
        public string PraiseDate { get; set; }

        /// <summary>
        /// hinh thuc khen thuong
        /// </summary>
        [DataMember]
        public string PraiseTypeName { get; set; }

        /// <summary>
        /// dia diem khen thuong
        /// </summary>
        [DataMember]
        public string PraiseLevel { get; set; }

        /// <summary>
        /// noi dung khen thuong
        /// </summary>
        [DataMember]
        public string PraiseDescription { get; set; }

        /// <summary>
        /// ngay ky luat
        /// </summary>
        [DataMember]
        public string DisciplineDate { get; set; }

        /// <summary>
        /// hinh thuc ky thuc
        /// </summary>
        [DataMember]
        public string DisciplineTypeName { get; set; }

        /// <summary>
        /// dia diem ky luat
        /// </summary>
        [DataMember]
        public string DisciplineLevel { get; set; }

        /// <summary>
        /// noi dung ky luat
        /// </summary>
        [DataMember]
        public string DisciplineDescription { get; set; }
    }

    [DataContract]
    public class PupilPraiseDisciplineResponse
    {
        private List<PupilPraiseDisciplineDefault> _praiseDiscipline;

        /// <summary>
        /// ma loi tra ve
        /// </summary>
        [DataMember]
        public int ValidationCode { get; set; }

        /// <summary>
        /// Object dinh kem
        /// </summary>
        [DataMember]
        public List<PupilPraiseDisciplineDefault> PupilPraiseDiscipline
        {
            get
            {
                return _praiseDiscipline;
            }
            set
            {
                //Check null gan de thuc hien khoi tao 
                if (value == null)
                {
                    this._praiseDiscipline = new List<PupilPraiseDisciplineDefault>();
                }
                else
                {
                    this._praiseDiscipline = value;
                }
            }
        }
    }
    #endregion

    #region Object Response danh sách HS dạng chuỗi
    [DataContract]
    public class ListPupilResponseFormatString
    {
        private List<string> lstPupilStr;
        /// <summary>
        /// ma loi tra ve
        /// </summary>
        [DataMember]
        public int ValidationCode { get; set; }

        /// <summary>
        /// Tong so trang neu chon tim kiem theo khoi
        /// </summary>
        [DataMember]
        public int TotalRecord { get; set; }

        /// <summary>
        /// Object dinh kem
        /// </summary>
        [DataMember]
        public List<string> LstPupilStr
        {
            get
            {
                return lstPupilStr;
            }
            set
            {
                //Check null gan de thuc hien khoi tao 
                if (value == null)
                {
                    this.lstPupilStr = new List<string>();
                }
                else
                {
                    this.lstPupilStr = value;
                }
            }
        }
    }
    #endregion
}