﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Web;

namespace WCF.Composite
{
    [DataContract]
    public class EvaluationMonthComposite
    {
        [DataMember]
        public int MonthID { get; set; }

        [DataMember]
        public int MonthName { get; set; }
    }
}