﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Web;

namespace WCF.Composite
{
    [DataContract]
    public class MarkRecordComposite
    {
        [DataMember]
        public int PupilID { get; set; }
        [DataMember]
        public int ClassID { get; set; }
        [DataMember]
        public int SchoolID { get; set; }
        [DataMember]
        public int AcademicYearID { get; set; }
        [DataMember]
        public int SubjectID { get; set; }
        [DataMember]
        public int MarkTypeID { get; set; }
        [DataMember]
        public int Year { get; set; }
        [DataMember]
        public Nullable<int> Semester { get; set; }
        [DataMember]
        public Nullable<int> PeriodID { get; set; }
        [DataMember]
        public string Title { get; set; }
        [DataMember]
        public decimal Mark { get; set; }
        [DataMember]
        public int OrderNumber { get; set; }
        [DataMember]
        public Nullable<System.DateTime> MarkedDate { get; set; }
        [DataMember]
        public Nullable<System.DateTime> CreatedDate { get; set; }
        [DataMember]
        public Nullable<System.DateTime> ModifiedDate { get; set; }
        [DataMember]
        public bool IsSummedUp { get; set; }

        #region SummedUp
        [DataMember]
        public decimal SummedUpMark { get; set; }
        [DataMember]
        public int IsCommenting { get; set; }
        [DataMember]
        public string JudgementResult { get; set; }
        [DataMember]
        public Nullable<System.DateTime> SummedUpDate { get; set; }
        [DataMember]
        public Nullable<decimal> ReTestMark { get; set; }
        #endregion
    }

    [DataContract]
    public class CollectionComposite
    {
        [DataMember]
        public int UserAccountID { get; set; }
        [DataMember]
        public int AcademicYearID { get; set; }
        [DataMember]
        public int SchoolID { get; set; }
        [DataMember]
        public int ClassID { get; set; }
        [DataMember]
        public int SubjectID { get; set; }
        [DataMember]
        public int SemesterID { get; set; }
        [DataMember]
        public int? PeriodID { get; set; }
        [DataMember]
        public bool IsCheck { get; set; }
        [DataMember]
        public int AppliedLevel { get; set; }
        [DataMember]
        public int Year { get; set; }
    }
}