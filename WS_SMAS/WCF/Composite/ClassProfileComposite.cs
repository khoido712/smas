﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Web;

namespace WCF.Composite
{
    [DataContract]
    public class ClassProfileLite
    {
        /// <summary>
        /// Cấp học
        /// </summary>        
        [DataMember]
        public int Grade { get; set; }

        /// <summary>
        /// ID khối học
        /// </summary>
        [DataMember]
        public int EducationLevelID { get; set; }

        /// <summary>
        /// ID lớp học
        /// </summary>
        [DataMember]
        public int ClassProfileID { get; set; }

        /// <summary>
        /// Tên lớp học
        /// </summary>
        [DataMember]
        public string ClassName { get; set; }
        /// <summary>
        /// Buổi học
        /// </summary>
        [DataMember]
        public int? ClassSection { get; set; }

        /// <summary>
        /// Thứ tự lớp học
        /// </summary>
        [DataMember]
        public int OrderNumber { get; set; }

        [DataMember]
        public bool IsVNEN { get; set; }
    }

    /// <summary>
    /// Object co thuoc tinh bao gom danh sach ClassProfileLiteResponse
    /// </summary>
    [DataContract]
    public class ListClassProfileLiteResponse
    {
        private List<ClassProfileLite> lstClassProfileLite;
        /// <summary>
        /// ma loi tra ve
        /// </summary>
        [DataMember]
        public int ValidationCode { get; set; }
        /// <summary>
        /// Object dinh kem
        /// </summary>
        [DataMember]
        public List<ClassProfileLite> LstClassProfileLite
        {
            get{
                return lstClassProfileLite;
            }
            set{
                //Check null gan de thuc hien new item
                if(value == null)
                {
                    this.lstClassProfileLite = new List<ClassProfileLite>();
                }
                else{
                    this.lstClassProfileLite = value;
                }
            }
        }
    }
    /// <summary>
    /// Object co thuoc tinh bao gom doi tuong ClassProfileLiteResponse
    /// </summary>
    [DataContract]
    public class ClassProfileLiteResponse
    {
        private ClassProfileLite objClassProfileLite;
        /// <summary>
        /// ma loi tra ve
        /// </summary>
        [DataMember]
        public int ValidationCode { get; set; }
        /// <summary>
        /// Object dinh kem
        /// </summary>
        [DataMember]
        public ClassProfileLite ObjClassProfileLite
        {
            get
            {
                return objClassProfileLite;
            }
            set
            {
                //Check null gan de thuc hien new item
                if (value == null)
                {
                    this.objClassProfileLite = new ClassProfileLite();
                }
                else
                {
                    this.objClassProfileLite = value;
                }
            }
        }
    }

}