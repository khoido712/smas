﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Web;

namespace WCF.Composite.Migrate
{
    [DataContract]
    public class MMarkRecordDefault
    {
        [DataMember]
        public long MarkRecordID { get; set; }
        [DataMember]
        public int PupilID { get; set; }
        [DataMember]
        public int ClassID { get; set; }
        [DataMember]
        public int SchoolID { get; set; }
        [DataMember]
        public int AcademicYearID { get; set; }
        [DataMember]
        public int SubjectID { get; set; }
        [DataMember]
        public int MarkTypeID { get; set; }
        [DataMember]
        public Nullable<int> Semester { get; set; }
        [DataMember]
        public string Title { get; set; }
        [DataMember]
        public decimal Mark { get; set; }
        [DataMember]
        public int OrderNumber { get; set; }
        [DataMember]
        public Nullable<System.DateTime> MarkedDate { get; set; }
        [DataMember]
        public Nullable<System.DateTime> CreatedDate { get; set; }
        [DataMember]
        public Nullable<System.DateTime> ModifiedDate { get; set; }
        [DataMember]
        public Nullable<int> M_ProvinceID { get; set; }
        [DataMember]
        public Nullable<int> M_OldID { get; set; }
        [DataMember]
        public Nullable<int> M_IsByC { get; set; }
        [DataMember]
        public Nullable<bool> IsOldData { get; set; }
        [DataMember]
        public Nullable<bool> IsSMS { get; set; }
        [DataMember]
        public Nullable<decimal> OldMark { get; set; }
        [DataMember]
        public string M_MarkRecord { get; set; }
        [DataMember]
        public string MSourcedb { get; set; }
        [DataMember]
        public int Last2digitNumberSchool { get; set; }
        [DataMember]
        public int CreatedAcademicYear { get; set; }
        [DataMember]
        public Nullable<int> SynchronizeID { get; set; }
        [DataMember]
        public Nullable<int> Year { get; set; }
        [DataMember]
        public Nullable<int> PeriodID { get; set; }
        [DataMember]
        public Nullable<int> LogChange { get; set; }
    }

    [DataContract]
    public class MJudgeRecordDefault
    {
        [DataMember]
        public long JudgeRecordID { get; set; }
        [DataMember]
        public int PupilID { get; set; }
        [DataMember]
        public int ClassID { get; set; }
        [DataMember]
        public int SchoolID { get; set; }
        [DataMember]
        public int AcademicYearID { get; set; }
        [DataMember]
        public int SubjectID { get; set; }
        [DataMember]
        public int MarkTypeID { get; set; }
        [DataMember]
        public Nullable<int> Semester { get; set; }
        [DataMember]
        public string Judgement { get; set; }
        [DataMember]
        public string ReTestJudgement { get; set; }
        [DataMember]
        public int OrderNumber { get; set; }
        [DataMember]
        public Nullable<System.DateTime> MarkedDate { get; set; }
        [DataMember]
        public Nullable<System.DateTime> CreatedDate { get; set; }
        [DataMember]
        public Nullable<System.DateTime> ModifiedDate { get; set; }
        [DataMember]
        public string Title { get; set; }
        [DataMember]
        public Nullable<int> M_ProvinceID { get; set; }
        [DataMember]
        public Nullable<int> M_OldID { get; set; }
        [DataMember]
        public Nullable<bool> IsOldData { get; set; }
        [DataMember]
        public Nullable<bool> IsSMS { get; set; }
        [DataMember]
        public string OldJudgement { get; set; }
        [DataMember]
        public string MJudgement { get; set; }
        [DataMember]
        public string MSourcedb { get; set; }
        [DataMember]
        public int Last2digitNumberSchool { get; set; }
        [DataMember]
        public int CreatedAcademicYear { get; set; }
        [DataMember]
        public Nullable<int> SynchronizeID { get; set; }
        [DataMember]
        public Nullable<int> Year { get; set; }
        [DataMember]
        public Nullable<int> PeriodID { get; set; }
        [DataMember]
        public Nullable<int> LogChange { get; set; }
    }



    [DataContract]
    public class MSummedRecordDefault
    {
        [DataMember]
        public long SummedUpRecordID { get; set; }
        [DataMember]
        public int PupilID { get; set; }
        [DataMember]
        public int ClassID { get; set; }
        [DataMember]
        public int SchoolID { get; set; }
        [DataMember]
        public int AcademicYearID { get; set; }
        [DataMember]
        public int SubjectID { get; set; }
        [DataMember]
        public int IsCommenting { get; set; }
        [DataMember]
        public Nullable<int> Semester { get; set; }
        [DataMember]
        public Nullable<int> PeriodID { get; set; }
        [DataMember]
        public Nullable<decimal> SummedUpMark { get; set; }
        [DataMember]
        public string JudgementResult { get; set; }
        [DataMember]
        public string Comment { get; set; }
        [DataMember]
        public Nullable<System.DateTime> SummedUpDate { get; set; }
        [DataMember]
        public Nullable<decimal> ReTestMark { get; set; }
        [DataMember]
        public string ReTestJudgement { get; set; }
        [DataMember]
        public Nullable<int> M_ProvinceID { get; set; }
        [DataMember]
        public Nullable<int> M_OldID { get; set; }
        [DataMember]
        public Nullable<bool> IsOldData { get; set; }
        [DataMember]
        public string MSourcedb { get; set; }
        [DataMember]
        public int Last2digitNumberSchool { get; set; }
        [DataMember]
        public int CreatedAcademicYear { get; set; }
        [DataMember]
        public Nullable<int> SynchronizeID { get; set; }
        [DataMember]
        public Nullable<int> Year { get; set; }
        
    }

    [DataContract]
    public class MPupilRankingDefault
    {
        [DataMember]
        public long PupilRankingID { get; set; }
        [DataMember]
        public int PupilID { get; set; }
        [DataMember]
        public int ClassID { get; set; }
        [DataMember]
        public int AcademicYearID { get; set; }
        [DataMember]
        public int EducationLevelID { get; set; }
        [DataMember]
        public Nullable<int> Semester { get; set; }
        [DataMember]
        public Nullable<int> PeriodID { get; set; }
        [DataMember]
        public Nullable<decimal> AverageMark { get; set; }
        [DataMember]
        public Nullable<int> TotalAbsentDaysWithPermission { get; set; }
        [DataMember]
        public Nullable<int> TotalAbsentDaysWithoutPermission { get; set; }
        [DataMember]
        public Nullable<int> CapacityLevelID { get; set; }
        [DataMember]
        public Nullable<int> ConductLevelID { get; set; }
        [DataMember]
        public Nullable<int> Rank { get; set; }
        [DataMember]
        public Nullable<System.DateTime> RankingDate { get; set; }
        [DataMember]
        public Nullable<int> StudyingJudgementID { get; set; }
        [DataMember]
        public int SchoolID { get; set; }
        [DataMember]
        public Nullable<bool> IsCategory { get; set; }
        [DataMember]
        public Nullable<int> M_ProvinceID { get; set; }
        [DataMember]
        public Nullable<int> M_OldID { get; set; }
        [DataMember]
        public string MSourcedb { get; set; }
        [DataMember]
        public int Last2digitNumberSchool { get; set; }
        [DataMember]
        public int CreatedAcademicYear { get; set; }
        [DataMember]
        public Nullable<int> SynchronizeID { get; set; }
        [DataMember]
        public Nullable<int> Year { get; set; }
        [DataMember]
        public string PupilRankingComment { get; set; }
    }

    [DataContract]
    public class MMarkRecord
    {
        [DataMember]
        public List<MMarkRecordDefault> ListMarkRecord { get; set; }

        [DataMember]
        public List<MJudgeRecordDefault> ListJudgeMarkRecord { get; set; }

        [DataMember]
        public List<MSummedRecordDefault> ListSummedRecord { get; set; }


        /// <summary>
        /// Tra ve thanh cong hay that bai
        /// </summary>
        [DataMember]
        public bool Susscess { get; set; }

        [DataMember]
        public string Message { get; set; }
    }

    [DataContract]
    public class MPupilRanking
    {
        [DataMember]
        public List<MPupilRankingDefault> ListPupilRanking { get; set; }

        /// <summary>
        /// Tra ve thanh cong hay that bai
        /// </summary>
        [DataMember]
        public bool Susscess { get; set; }

        [DataMember]
        public string Message { get; set; }
    }




}