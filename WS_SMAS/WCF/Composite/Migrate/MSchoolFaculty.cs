﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Web;

namespace WCF.Composite.Migrate
{
    [DataContract]
    public class MSchoolFacultyDefault
    {
        [DataMember]
        public int SchoolFacultyID { get; set; }
        [DataMember]
        public int SchoolID { get; set; }
        [DataMember]
        public string FacultyName { get; set; }
        [DataMember]
        public string Description { get; set; }
        [DataMember]
        public Nullable<System.DateTime> CreatedDate { get; set; }
        [DataMember]
        public bool IsActive { get; set; }
        [DataMember]
        public Nullable<System.DateTime> ModifiedDate { get; set; }
        [DataMember]
        public Nullable<int> M_ProvinceID { get; set; }
        [DataMember]
        public Nullable<int> M_OldID { get; set; }
        [DataMember]
        public string MSourcedb { get; set; }
        [DataMember]
        public Nullable<int> SynchronizeID { get; set; }
    }

    [DataContract]
    public class MSchoolFaculty
    {
        [DataMember]
        public List<MSchoolFacultyDefault> ListSchhoolFaculty { get; set; }

        /// <summary>
        /// Tra ve thanh cong hay that bai
        /// </summary>
        [DataMember]
        public bool Susscess { get; set; }

        [DataMember]
        public string Message { get; set; }
    }

}