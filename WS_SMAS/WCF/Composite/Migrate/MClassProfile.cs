﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Web;

namespace WCF.Composite.Migrate
{
    [DataContract]
    public class MClassProfileDafault
    {
        [DataMember]
        public int ClassProfileID { get; set; }
        [DataMember]
        public int AcademicYearID { get; set; }
        [DataMember]
        public int SchoolID { get; set; }
        [DataMember]
        public Nullable<int> SchoolSubsidiaryID { get; set; }
        [DataMember]
        public bool IsCombinedClass { get; set; }
        [DataMember]
        public string CombinedClassCode { get; set; }
        [DataMember]
        public Nullable<int> SubCommitteeID { get; set; }
        [DataMember]
        public Nullable<int> HeadTeacherID { get; set; }
        [DataMember]
        public int EducationLevelID { get; set; }
        [DataMember]
        public string DisplayName { get; set; }
        [DataMember]
        public string Description { get; set; }
        [DataMember]
        public Nullable<System.DateTime> StartDate { get; set; }
        [DataMember]
        public Nullable<System.DateTime> EndDate { get; set; }
        [DataMember]
        public Nullable<bool> IsSpecializedClass { get; set; }
        [DataMember]
        public Nullable<int> FirstForeignLanguageID { get; set; }
        [DataMember]
        public Nullable<int> SecondForeignLanguageID { get; set; }
        [DataMember]
        public Nullable<int> ApprenticeshipGroupID { get; set; }
        [DataMember]
        public Nullable<int> Section { get; set; }
        [DataMember]
        public Nullable<bool> IsITClass { get; set; }
        [DataMember]
        public Nullable<int> M_ProvinceID { get; set; }
        [DataMember]
        public Nullable<int> M_OldID { get; set; }
        [DataMember]
        public string MSourcedb { get; set; }
        [DataMember]
        public Nullable<int> SynchronizeID { get; set; }
        [DataMember]
        public Nullable<int> OrderNumber { get; set; }
        [DataMember]
        public string VemisCode { get; set; }
        [DataMember]
        public Nullable<bool> IsVnenClass { get; set; }
        [DataMember]
        public Nullable<int> SeperateKey { get; set; }
        [DataMember]
        public Nullable<bool> IsActive { get; set; }
    }


    [DataContract]
    public class MClassProfile
    {
        [DataMember]
        public List<MClassProfileDafault> ListClassProfile { get; set; }

        /// <summary>
        /// Tra ve thanh cong hay that bai
        /// </summary>
        [DataMember]
        public bool Susscess { get; set; }
        /// <summary>
        /// Ma loi tra ve
        /// </summary>
        [DataMember]
        public int ValidationCode { get; set; }

        [DataMember]
        public string Message { get; set; }
    }
}