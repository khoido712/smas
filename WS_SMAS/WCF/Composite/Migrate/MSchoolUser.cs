﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Web;

namespace WCF.Composite.Migrate
{
    [DataContract]
    public class MSchoolUserDefault
    {
        [DataMember]
        public int SchoolId { get; set; }

        [DataMember]
        public int OldSchoolId { get; set; }
        [DataMember]
        public string UserName { get; set; }

        [DataMember]
        public int UserAccountId { get; set; }
        [DataMember]
        public List<int> listRole { get; set; }
    }

    [DataContract]
    public class MSchoolUser
    {
        [DataMember]
        public List<MSchoolUserDefault> ListSchoolUser { get; set; }

        /// <summary>
        /// Tra ve thanh cong hay that bai
        /// </summary>
        [DataMember]
        public bool Susscess { get; set; }
        /// <summary>
        /// Ma loi tra ve
        /// </summary>
        [DataMember]
        public int ValidationCode { get; set; }

        [DataMember]
        public string Message { get; set; }
    }
}