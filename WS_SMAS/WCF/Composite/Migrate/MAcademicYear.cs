﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Web;

namespace WCF.Composite.Migrate
{
    [DataContract]
    public partial class MAcademicYearDefault
    {
        [DataMember]
        public int AcademicYearID { get; set; }
        [DataMember]
        public int SchoolID { get; set; }
        [DataMember]
        public string DisplayTitle { get; set; }
        [DataMember]
        public int Year { get; set; }
        [DataMember]
        public Nullable<System.DateTime> FirstSemesterStartDate { get; set; }
        [DataMember]
        public Nullable<System.DateTime> FirstSemesterEndDate { get; set; }
        [DataMember]
        public Nullable<System.DateTime> SecondSemesterStartDate { get; set; }
        [DataMember]
        public Nullable<System.DateTime> SecondSemesterEndDate { get; set; }
        [DataMember]
        public int ConductEstimationType { get; set; }
        [DataMember]
        public int RankingCriteria { get; set; }
        [DataMember]
        public bool IsSecondSemesterToSemesterAll { get; set; }
        [DataMember]
        public bool IsTeacherCanViewAll { get; set; }
        [DataMember]
        public bool IsHeadTeacherCanSendSMS { get; set; }
        [DataMember]
        public bool IsSubjectTeacherCanSendSMS { get; set; }
        [DataMember]
        public Nullable<int> M_ProvinceID { get; set; }
        [DataMember]
        public Nullable<int> M_OldID { get; set; }
        [DataMember]
        public Nullable<bool> IsShowAvatar { get; set; }
        [DataMember]
        public Nullable<bool> IsShowCalendar { get; set; }
        [DataMember]
        public string MSourcedb { get; set; }
        [DataMember]
        public Nullable<int> CreatedYear { get; set; }
        [DataMember]
        public Nullable<int> SynchronizeID { get; set; }
        [DataMember]
        public Nullable<bool> IsClassification { get; set; }
        [DataMember]
        public Nullable<bool> IsLockNumOrdinal { get; set; }
        [DataMember]
        public Nullable<bool> IsLockPupilProfile { get; set; }
        [DataMember]
        public Nullable<int> IsMovedHistory { get; set; }
        [DataMember]
        public Nullable<bool> IsActive { get; set; }
        [DataMember]
        public int SourceSchoolId { get; set; }

        [DataMember]
        public int EducationGrade { get; set; }
    }

    [DataContract]
    public class MAcademicYearResponse
    {
        [DataMember]
        public List<MAcademicYearDefault> ListAcademicYear { get; set; }

        /// <summary>
        /// Tra ve thanh cong hay that bai
        /// </summary>
        [DataMember]
        public bool Susscess { get; set; }
        /// <summary>
        /// Ma loi tra ve
        /// </summary>
        [DataMember]
        public int ValidationCode { get; set; }

        [DataMember]
        public string Message { get; set; }
    }
}