﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Web;

namespace WCF.Composite.Migrate
{
    [DataContract]
    public class TableContract
    {
        [DataMember]
        public System.Data.DataTable ListData { get; set; }

        [DataMember]
        public bool Success { get; set; }
        [DataMember]
        public string Description { get; set; }
    }
}