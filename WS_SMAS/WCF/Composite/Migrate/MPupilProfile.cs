﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Web;

namespace WCF.Composite.Migrate
{
    [DataContract]
    public class MPupilProfileDefault
    {
        [DataMember]
        public int PupilProfileID { get; set; }
        [DataMember]
        public int ClassID { get; set; }
        [DataMember]
        public int SchoolID { get; set; }
        [DataMember]
        public int AcademicYearID { get; set; }
        [DataMember]
        public Nullable<int> AreaID { get; set; }
        [DataMember]
        public int ProvinceID { get; set; }
        [DataMember]
        public Nullable<int> DistrictID { get; set; }
        [DataMember]
        public Nullable<int> CommuneID { get; set; }
        [DataMember]
        public Nullable<int> EthnicID { get; set; }
        [DataMember]
        public Nullable<int> ReligionID { get; set; }
        [DataMember]
        public Nullable<int> PolicyTargetID { get; set; }
        [DataMember]
        public Nullable<int> FamilyTypeID { get; set; }
        [DataMember]
        public Nullable<int> PriorityTypeID { get; set; }
        [DataMember]
        public Nullable<int> PreviousGraduationLevelID { get; set; }
        [DataMember]
        public string PupilCode { get; set; }
        [DataMember]
        public string Name { get; set; }
        [DataMember]
        public string FullName { get; set; }
        [DataMember]
        public int Genre { get; set; }
        
        [DataMember]
        public System.DateTime BirthDate { get; set; }
        [DataMember]
        public string BirthPlace { get; set; }
        [DataMember]
        public string HomeTown { get; set; }
        [DataMember]
        public string Telephone { get; set; }
        [DataMember]
        public string Mobile { get; set; }
        [DataMember]
        public string Email { get; set; }
        [DataMember]
        public string TempResidentalAddress { get; set; }
        [DataMember]
        public string PermanentResidentalAddress { get; set; }
        [DataMember]
        public Nullable<bool> IsResidentIn { get; set; }
        [DataMember]
        public Nullable<bool> IsDisabled { get; set; }
        [DataMember]
        public Nullable<int> DisabledTypeID { get; set; }
        [DataMember]
        public string DisabledSeverity { get; set; }
        [DataMember]
        public Nullable<int> BloodType { get; set; }
        [DataMember]
        public Nullable<decimal> EnrolmentMark { get; set; }
        [DataMember]
        public System.DateTime EnrolmentDate { get; set; }
        [DataMember]
        public int ForeignLanguageTraining { get; set; }
        [DataMember]
        public Nullable<bool> IsYoungPioneerMember { get; set; }
        [DataMember]
        public Nullable<System.DateTime> YoungPioneerJoinedDate { get; set; }
        [DataMember]
        public string YoungPioneerJoinedPlace { get; set; }
        [DataMember]
        public Nullable<bool> IsYouthLeageMember { get; set; }
        [DataMember]
        public Nullable<System.DateTime> YouthLeagueJoinedDate { get; set; }
        [DataMember]
        public string YouthLeagueJoinedPlace { get; set; }
        [DataMember]
        public Nullable<bool> IsCommunistPartyMember { get; set; }
        [DataMember]
        public Nullable<System.DateTime> CommunistPartyJoinedDate { get; set; }
        [DataMember]
        public string CommunistPartyJoinedPlace { get; set; }
        [DataMember]
        public string FatherFullName { get; set; }
        [DataMember]
        public Nullable<System.DateTime> FatherBirthDate { get; set; }
        [DataMember]
        public string FatherJob { get; set; }
        [DataMember]
        public string FatherMobile { get; set; }
        [DataMember]
        public string MotherFullName { get; set; }
        [DataMember]
        public Nullable<System.DateTime> MotherBirthDate { get; set; }
        [DataMember]
        public string MotherJob { get; set; }
        [DataMember]
        public string MotherMobile { get; set; }
        [DataMember]
        public string SponsorFullName { get; set; }
        [DataMember]
        public Nullable<System.DateTime> SponsorBirthDate { get; set; }
        [DataMember]
        public string SponsorJob { get; set; }
        [DataMember]
        public string SponsorMobile { get; set; }
        [DataMember]
        public int ProfileStatus { get; set; }
        [DataMember]
        public System.DateTime CreatedDate { get; set; }
        [DataMember]
        public bool IsActive { get; set; }
        [DataMember]
        public Nullable<System.DateTime> ModifiedDate { get; set; }
        [DataMember]
        public Nullable<decimal> HeightAtBirth { get; set; }
        [DataMember]
        public string FatherEmail { get; set; }
        [DataMember]
        public string MotherEmail { get; set; }
        [DataMember]
        public string SponsorEmail { get; set; }
        [DataMember]
        public Nullable<int> ChildOrder { get; set; }
        [DataMember]
        public Nullable<int> NumberOfChild { get; set; }
        [DataMember]
        public Nullable<int> EatingHabit { get; set; }
        [DataMember]
        public Nullable<int> ReactionTrainingHabit { get; set; }
        [DataMember]
        public Nullable<int> AttitudeInStrangeScene { get; set; }
        [DataMember]
        public string OtherHabit { get; set; }
        [DataMember]
        public string FavoriteToy { get; set; }
        [DataMember]
        public Nullable<int> EvaluationConfigID { get; set; }
        [DataMember]
        public Nullable<decimal> WeightAtBirth { get; set; }
        [DataMember]
        public Nullable<int> PolicyRegimeID { get; set; }
        [DataMember]
        public Nullable<int> SupportingPolicy { get; set; }
        [DataMember]
        public Nullable<bool> IsReceiveRiceSubsidy { get; set; }
        [DataMember]
        public Nullable<bool> IsResettlementTarget { get; set; }
        [DataMember]
        public Nullable<bool> IsSupportForLearning { get; set; }
        [DataMember]
        public Nullable<int> ClassType { get; set; }
        [DataMember]
        public Nullable<int> M_ProvinceID { get; set; }
        [DataMember]
        public Nullable<int> M_OldID { get; set; }
        [DataMember]
        public Nullable<bool> IsFatherSMS { get; set; }
        [DataMember]
        public Nullable<bool> IsMotherSMS { get; set; }
        [DataMember]
        public Nullable<bool> IsSponsorSMS { get; set; }
       
        [DataMember]
        public string MSourcedb { get; set; }
        [DataMember]
        public Nullable<int> PupilLearningType { get; set; }
        [DataMember]
        public Nullable<int> LanguageCertificateID { get; set; }
        [DataMember]
        public Nullable<int> ItCertificateID { get; set; }
        [DataMember]
        public string ItCertificate { get; set; }
        [DataMember]
        public string LanguageCertificate { get; set; }
        [DataMember]
        public Nullable<int> Last2digitNumberSchool { get; set; }
        [DataMember]
        public Nullable<int> SynchronizeID { get; set; }
        [DataMember]
        public Nullable<int> VillageID { get; set; }
        [DataMember]
        public string FavouriteTypeOfDish { get; set; }
        [DataMember]
        public string HateTypeOfDish { get; set; }
        [DataMember]
        public string IdentifyNumber { get; set; }
        [DataMember]
        public Nullable<int> HomePlace { get; set; }
    }


    [DataContract]
    public class MPupilProfile
    {
        [DataMember]
        public List<MPupilProfileDefault> ListPupilFile { get; set; }

        /// <summary>
        /// Tra ve thanh cong hay that bai
        /// </summary>
        [DataMember]
        public bool Susscess { get; set; }
        
        [DataMember]
        public string Message { get; set; }
    }
}