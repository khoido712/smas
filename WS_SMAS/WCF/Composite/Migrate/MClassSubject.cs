﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Web;

namespace WCF.Composite.Migrate
{
    [DataContract]
    public partial class MClassSubjectDefault
    {
        [DataMember]
        public long ClassSubjectID { get; set; }
        [DataMember]
        public int ClassID { get; set; }
        [DataMember]
        public int SubjectID { get; set; }
        [DataMember]
        public Nullable<bool> IsSpecializedSubject { get; set; }
        [DataMember]
        public Nullable<decimal> SectionPerWeekFirstSemester { get; set; }
        [DataMember]
        public Nullable<decimal> SectionPerWeekSecondSemester { get; set; }
        [DataMember]
        public Nullable<System.DateTime> StartDate { get; set; }
        [DataMember]
        public Nullable<System.DateTime> EndDate { get; set; }
        [DataMember]
        public Nullable<bool> IsSecondForeignLanguage { get; set; }
        [DataMember]
        public Nullable<int> OrderInSchoolReport { get; set; }
        [DataMember]
        public Nullable<int> FirstSemesterCoefficient { get; set; }
        [DataMember]
        public Nullable<int> SecondSemesterCoefficient { get; set; }
        [DataMember]
        public Nullable<int> AppliedType { get; set; }
        [DataMember]
        public Nullable<int> IsCommenting { get; set; }
        [DataMember]
        public Nullable<int> M_ProvinceID { get; set; }
        [DataMember]
        public Nullable<int> M_OldID { get; set; }
        [DataMember]
        public string MSourcedb { get; set; }
        [DataMember]
        public int Last2digitNumberSchool { get; set; }
        [DataMember]
        public Nullable<int> CreatedAcademicYear { get; set; }
        [DataMember]
        public Nullable<int> SynchronizeID { get; set; }
        [DataMember]
        public Nullable<int> SubjectIDIncrease { get; set; }
        [DataMember]
        public Nullable<int> MMinFirstSemester { get; set; }
        [DataMember]
        public Nullable<int> PMinFirstSemester { get; set; }
        [DataMember]
        public Nullable<int> VMinFirstSemester { get; set; }
        [DataMember]
        public Nullable<int> MMinSecondSemester { get; set; }
        [DataMember]
        public Nullable<int> PMinSecondSemester { get; set; }
        [DataMember]
        public Nullable<int> VMinSecondSemester { get; set; }
        [DataMember]
        public Nullable<bool> IsSubjectVNEN { get; set; }

        [DataMember]
        public int EducationLevelId { get; set; }
    }

    [DataContract]
    public class MClassSubject
    {
        [DataMember]
        public List<MClassSubjectDefault> ListClassSubject { get; set; }

        /// <summary>
        /// Tra ve thanh cong hay that bai
        /// </summary>
        [DataMember]
        public bool Susscess { get; set; }

        [DataMember]
        public string Message { get; set; }
    }


    [DataContract]
    public partial class MSchoolSubjectDefault
    {
        [DataMember]
        public int SchoolSubjectID { get; set; }
        [DataMember]
        public int SubjectID { get; set; }
        [DataMember]
        public int SchoolID { get; set; }
        [DataMember]
        public int AcademicYearID { get; set; }
        [DataMember]
        public Nullable<System.DateTime> StartDate { get; set; }
        [DataMember]
        public Nullable<System.DateTime> EndDate { get; set; }
        [DataMember]
        public Nullable<int> FirstSemesterCoefficient { get; set; }
        [DataMember]
        public Nullable<int> SecondSemesterCoefficient { get; set; }
        [DataMember]
        public Nullable<int> AppliedType { get; set; }
        [DataMember]
        public Nullable<int> IsCommenting { get; set; }
        [DataMember]
        public Nullable<decimal> SectionPerWeekFirstSemester { get; set; }
        [DataMember]
        public Nullable<decimal> SectionPerWeekSecondSemester { get; set; }
        [DataMember]
        public Nullable<int> EducationLevelID { get; set; }
        [DataMember]
        public Nullable<int> M_ProvinceID { get; set; }
        [DataMember]
        public Nullable<int> M_OldID { get; set; }
        [DataMember]
        public string MSourcedb { get; set; }
        [DataMember]
        public Nullable<int> Last2digitNumberSchool { get; set; }
        [DataMember]
        public Nullable<int> CreatedAcademicYear { get; set; }
        [DataMember]
        public Nullable<int> SynchronizeID { get; set; }
    }

    [DataContract]
    public class MSchoolSubject
    {
        [DataMember]
        public List<MSchoolSubjectDefault> ListSchoolSubject { get; set; }

        /// <summary>
        /// Tra ve thanh cong hay that bai
        /// </summary>
        [DataMember]
        public bool Susscess { get; set; }

        [DataMember]
        public string Message { get; set; }
    }
}