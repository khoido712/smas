﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Web;

namespace WCF.Composite.Migrate
{
    [DataContract]
    public class MEmployeeDefault
    {
        [DataMember]
        public int EmployeeID { get; set; }
        [DataMember]
        public string EmployeeCode { get; set; }
        [DataMember]
        public int EmployeeType { get; set; }
        [DataMember]
        public Nullable<int> EmploymentStatus { get; set; }
        [DataMember]
        public Nullable<int> SupervisingDeptID { get; set; }
        [DataMember]
        public Nullable<int> SchoolID { get; set; }
        [DataMember]
        public string Name { get; set; }
        [DataMember]
        public string FullName { get; set; }
        [DataMember]
        public Nullable<System.DateTime> BirthDate { get; set; }
        [DataMember]
        public string BirthPlace { get; set; }
        [DataMember]
        public string Telephone { get; set; }
        [DataMember]
        public string Mobile { get; set; }
        [DataMember]
        public string Email { get; set; }
        [DataMember]
        public string TempResidentalAddress { get; set; }
        [DataMember]
        public string PermanentResidentalAddress { get; set; }
        [DataMember]
        public bool Genre { get; set; }
        [DataMember]
        public string Alias { get; set; }
        [DataMember]
        public Nullable<int> MariageStatus { get; set; }
        [DataMember]
        public string ForeignLanguageQualification { get; set; }
        [DataMember]
        public string HealthStatus { get; set; }
        [DataMember]
        public Nullable<System.DateTime> JoinedDate { get; set; }
        [DataMember]
        public Nullable<System.DateTime> StartingDate { get; set; }
        [DataMember]
        public string IdentityNumber { get; set; }
        [DataMember]
        public Nullable<System.DateTime> IdentityIssuedDate { get; set; }
        [DataMember]
        public string IdentityIssuedPlace { get; set; }
        [DataMember]
        public string HomeTown { get; set; }
        [DataMember]
        public Nullable<bool> DedicatedForYoungLeague { get; set; }
        [DataMember]
        public Nullable<int> FamilyTypeID { get; set; }
        [DataMember]
        public Nullable<int> StaffPositionID { get; set; }
        [DataMember]
        public Nullable<int> EthnicID { get; set; }
        [DataMember]
        public Nullable<int> ReligionID { get; set; }
        [DataMember]
        public Nullable<int> GraduationLevelID { get; set; }
        [DataMember]
        public Nullable<int> ContractID { get; set; }
        [DataMember]
        public Nullable<int> QualificationTypeID { get; set; }
        [DataMember]
        public Nullable<int> SpecialityCatID { get; set; }
        [DataMember]
        public Nullable<int> QualificationLevelID { get; set; }
        [DataMember]
        public Nullable<int> ITQualificationLevelID { get; set; }
        [DataMember]
        public Nullable<int> ForeignLanguageGradeID { get; set; }
        [DataMember]
        public Nullable<int> PoliticalGradeID { get; set; }
        [DataMember]
        public Nullable<int> StateManagementGradeID { get; set; }
        [DataMember]
        public Nullable<int> EducationalManagementGradeID { get; set; }
        [DataMember]
        public Nullable<int> WorkTypeID { get; set; }
        [DataMember]
        public Nullable<int> PrimarilyAssignedSubjectID { get; set; }
        [DataMember]
        public Nullable<int> SchoolFacultyID { get; set; }
        [DataMember]
        public Nullable<bool> IsYouthLeageMember { get; set; }
        [DataMember]
        public Nullable<System.DateTime> YouthLeagueJoinedDate { get; set; }
        [DataMember]
        public string YouthLeagueJoinedPlace { get; set; }
        [DataMember]
        public Nullable<bool> IsCommunistPartyMember { get; set; }
        [DataMember]
        public Nullable<System.DateTime> CommunistPartyJoinedDate { get; set; }
        [DataMember]
        public string CommunistPartyJoinedPlace { get; set; }
        [DataMember]
        public string FatherFullName { get; set; }
        [DataMember]
        public Nullable<System.DateTime> FatherBirthDate { get; set; }
        [DataMember]
        public string FatherJob { get; set; }
        [DataMember]
        public string FatherWorkingPlace { get; set; }
        [DataMember]
        public string MotherFullName { get; set; }
        [DataMember]
        public Nullable<System.DateTime> MotherBirthDate { get; set; }
        [DataMember]
        public string MotherJob { get; set; }
        [DataMember]
        public string MotherWorkingPlace { get; set; }
        [DataMember]
        public string SpouseFullName { get; set; }
        [DataMember]
        public Nullable<System.DateTime> SpouseBirthDate { get; set; }
        [DataMember]
        public string SpouseJob { get; set; }
        [DataMember]
        public string SpouseWorkingPlace { get; set; }
        [DataMember]
        public string Description { get; set; }
        [DataMember]
        public Nullable<System.DateTime> CreatedDate { get; set; }
        [DataMember]
        public bool IsActive { get; set; }
        [DataMember]
        public Nullable<System.DateTime> ModifiedDate { get; set; }
        [DataMember]
        public System.DateTime IntoSchoolDate { get; set; }
        public Nullable<int> AppliedLevel { get; set; }
        [DataMember]
        public Nullable<int> ContractTypeID { get; set; }
        [DataMember]
        public Nullable<bool> RegularRefresher { get; set; }
        [DataMember]
        public Nullable<int> TrainingLevelID { get; set; }
        [DataMember]
        public string M_FamilyTypeName { get; set; }
        [DataMember]
        public Nullable<int> M_OldEmployeeID { get; set; }
        [DataMember]
        public Nullable<int> M_OldTeacherID { get; set; }
        [DataMember]
        public Nullable<int> M_ProvinceID { get; set; }
        [DataMember]
        public Nullable<int> M_OldID { get; set; }
        [DataMember]
        public Nullable<int> WorkGroupTypeID { get; set; }
        
        [DataMember]
        public string Note { get; set; }
        [DataMember]
        public string MSourcedb { get; set; }
        [DataMember]
        public Nullable<int> SynchronizeID { get; set; }
        [DataMember]
        public Nullable<bool> IsSyndicate { get; set; }
        [DataMember]
        public Nullable<System.DateTime> SyndicateDate { get; set; }
        [DataMember]
        public Nullable<bool> MotelRoomOutsite { get; set; }
        [DataMember]
        public Nullable<bool> TeacherDuties { get; set; }
        [DataMember]
        public Nullable<bool> NeedTeacherDuties { get; set; }
    }

    [DataContract]
    public class MEmployee
    {
        [DataMember]
        public List<MEmployeeDefault> ListEmployee { get; set; }

        /// <summary>
        /// Tra ve thanh cong hay that bai
        /// </summary>
        [DataMember]
        public bool Susscess { get; set; }
        
        [DataMember]
        public string Message { get; set; }
    }
}