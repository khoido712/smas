﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Web;

namespace WCF.Composite.Migrate
{
    [DataContract]
    public class MWorkType
    {
        [DataMember]
        public int WorkTypeID { get; set; }
        [DataMember]
        public string Resolution { get; set; }
        [DataMember]
        public int WorkGroupTypeID { get; set; }
       
    }
}