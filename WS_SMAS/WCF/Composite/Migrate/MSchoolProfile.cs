﻿using SMAS.Models.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Web;

namespace WCF.Composite.Migrate
{
    [DataContract]
    public partial class MSchoolProfileDefault
    {
        [DataMember]
        public int SchoolProfileID { get; set; }
        [DataMember]
        public string SchoolCode { get; set; }
        [DataMember]
        public string SchoolName { get; set; }
        [DataMember]
        public string ShortName { get; set; }
        [DataMember]
        public Nullable<int> AreaID { get; set; }
        [DataMember]
        public Nullable<int> ProvinceID { get; set; }
        [DataMember]
        public Nullable<int> DistrictID { get; set; }
        [DataMember]
        public Nullable<int> CommuneID { get; set; }
        [DataMember]
        public Nullable<int> SupervisingDeptID { get; set; }
        [DataMember]
        public Nullable<int> TrainingTypeID { get; set; }
        [DataMember]
        public Nullable<int> SchoolTypeID { get; set; }
        [DataMember]
        public string Telephone { get; set; }
        [DataMember]
        public string Fax { get; set; }
        [DataMember]
        public string Email { get; set; }
        [DataMember]
        public string Address { get; set; }
        [DataMember]
        public Nullable<System.DateTime> EstablishedDate { get; set; }
        [DataMember]
        public string Website { get; set; }
        [DataMember]
        public int EducationGrade { get; set; }
        [DataMember]
        public string ReportTile { get; set; }
        [DataMember]
        public string HeadMasterName { get; set; }
        [DataMember]
        public string HeadMasterPhone { get; set; }
        [DataMember]
        public bool HasSubsidiary { get; set; }
        [DataMember]
        public Nullable<System.DateTime> CreatedDate { get; set; }
        [DataMember]
        public bool IsActive { get; set; }
        [DataMember]
        public Nullable<bool> IsNewSchoolModel { get; set; }
        [DataMember]
        public Nullable<int> InSpecialDifficultZone { get; set; }
        [DataMember]
        public Nullable<System.DateTime> ModifiedDate { get; set; }
        [DataMember]
        public Nullable<int> AdminID { get; set; }
        [DataMember]
        public string SchoolYearTitle { get; set; }
        [DataMember]
        public int SMSTeacherActiveType { get; set; }
        [DataMember]
        public int SMSParentActiveType { get; set; }
        [DataMember]
        public string CancelActiveReason { get; set; }
        [DataMember]
        public Nullable<System.Guid> M_OldAdminID { get; set; }
        public Nullable<int> M_ProvinceID { get; set; }
        [DataMember]
        public Nullable<int> M_OldID { get; set; }
        [DataMember]
        public Nullable<bool> IsActiveSMAS { get; set; }
        [DataMember]
        public byte[] Image { get; set; }
        [DataMember]
        public string MSourcedb { get; set; }
        [DataMember]
        public Nullable<int> SynchronizeID { get; set; }
        [DataMember]
        public string SchoolExamCode { get; set; }
        [DataMember]
        public Nullable<int> ProductVersion { get; set; }
        [DataMember]
        public string UserName { get; set; }

        [DataMember]
        public List<int> ListRole { get; set; }
    }
    [DataContract]
    public class MSchoolProfile
    {
        [DataMember]
        public List<MSchoolProfileDefault> ListSchoolProfile { get; set; }

        /// <summary>
        /// Tra ve thanh cong hay that bai
        /// </summary>
        [DataMember]
        public bool Susscess { get; set; }
        /// <summary>
        /// Ma loi tra ve
        /// </summary>
        [DataMember]
        public int ValidationCode { get; set; }

        [DataMember]
        public string Message { get; set; }
    }
}