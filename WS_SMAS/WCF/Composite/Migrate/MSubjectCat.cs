﻿using System;
using System.Collections.Generic;
using System.Runtime.Serialization;

namespace WCF.Composite.Migrate
{
    [DataContract]
    public partial class MSubjectCatDefault
    {
        [DataMember]
        public int SubjectCatID { get; set; }
        [DataMember]
        public string SubjectName { get; set; }
        [DataMember]
        public string Abbreviation { get; set; }
        [DataMember]
        public string DisplayName { get; set; }
        [DataMember]
        public bool MiddleSemesterTest { get; set; }
        [DataMember]
        public bool IsExemptible { get; set; }
        [DataMember]
        public int IsCommenting { get; set; }
        [DataMember]
        public bool HasPractice { get; set; }
        [DataMember]
        public bool IsForeignLanguage { get; set; }
        [DataMember]
        public bool IsCoreSubject { get; set; }
        [DataMember]
        public bool IsApprenticeshipSubject { get; set; }
        [DataMember]
        public string Description { get; set; }
        [DataMember]
        public Nullable<System.DateTime> CreatedDate { get; set; }
        [DataMember]
        public bool IsActive { get; set; }
        [DataMember]
        public Nullable<System.DateTime> ModifiedDate { get; set; }
        [DataMember]
        public Nullable<int> ApprenticeshipGroupID { get; set; }
        [DataMember]
        public Nullable<int> AppliedLevel { get; set; }
        [DataMember]
        public Nullable<int> OrderInSubject { get; set; }
        [DataMember]
        public bool IsEditIsCommentting { get; set; }
        [DataMember]
        public bool ReadAndWriteMiddleTest { get; set; }
        [DataMember]
        public string Color { get; set; }
        [DataMember]
        public Nullable<int> SynchronizeID { get; set; }
        [DataMember]
        public string Code { get; set; }
    }

    [DataContract]
    public class MSubjectCat
    {
        [DataMember]
        public List<MSubjectCatDefault> ListSubjectCat { get; set; }

        /// <summary>
        /// Tra ve thanh cong hay that bai
        /// </summary>
        [DataMember]
        public bool Susscess { get; set; }
        /// <summary>
        /// Ma loi tra ve
        /// </summary>
        [DataMember]
        public int ValidationCode { get; set; }

        [DataMember]
        public string Message { get; set; }
    }
}