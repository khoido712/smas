﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Web;

namespace WCF.Composite.Migrate
{
    [DataContract]
    public class MSupervisingDept
    {
        [DataMember]
        public int SupervisingDeptId { get; set; }

        [DataMember]
        public string SupervisingDeptName { get; set; }

        [DataMember]
        public int OldId { get; set; }

    }
}