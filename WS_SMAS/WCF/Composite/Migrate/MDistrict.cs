﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Web;

namespace WCF.Composite.Migrate
{
    [DataContract]
    public class MDistrict
    {
        [DataMember]
        public int DistrictId { get; set; }

        [DataMember]
        public string DistrictName { get; set; }

        [DataMember]
        public int OldId { get; set; }

    }
}