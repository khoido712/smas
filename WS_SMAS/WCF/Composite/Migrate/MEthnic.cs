﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Web;

namespace WCF.Composite.Migrate
{

    [DataContract]
    public class MEthnic
    {
        [DataMember]
        public int EthnicID { get; set; }
        [DataMember]
        public string EthnicCode { get; set; }
        [DataMember]
        public string EthnicName { get; set; }
        [DataMember]
        public bool IsMinority { get; set; }
        [DataMember]
        public string Description { get; set; }
        [DataMember]
        public Nullable<System.DateTime> CreatedDate { get; set; }
        [DataMember]
        public bool IsActive { get; set; }
        public Nullable<System.DateTime> ModifiedDate { get; set; }
        [DataMember]
        public Nullable<int> SynchronizeID { get; set; }
    }
}