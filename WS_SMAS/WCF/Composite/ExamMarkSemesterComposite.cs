﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Web;

namespace WCF.Composite
{
    [DataContract]
    public class ExamMarkSemesterComposite
    {
        private List<ExamMarkSemester> lstExamMarkSemester;
        /// <summary>
        /// ma loi tra ve
        /// </summary>
        [DataMember]
        public int ValidationCode { get; set; }
        /// <summary>
        /// Object dinh kem
        /// </summary>
        [DataMember]
        public List<ExamMarkSemester> ListExamMarkSemester
        {
            get
            {
                return lstExamMarkSemester;
            }
            set
            {
                //Check null gan de thuc hien new item
                if (value == null)
                {
                    this.lstExamMarkSemester = new List<ExamMarkSemester>();
                }
                else
                {
                    this.lstExamMarkSemester = value;
                }
            }
        }
    }

    #region ObjectResponse
    [DataContract]
    public class ExamMarkSemester
    {
        [DataMember]
        public int PupilID { get; set; }
        [DataMember]
        public string Content { get; set; }
    }
    #endregion
}