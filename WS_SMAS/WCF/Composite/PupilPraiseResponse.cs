﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Web;

namespace WCF.Composite
{
    [DataContract]
    public class ListPupilPraiseResponse
    {
        private List<PupilPraiseLite> lstPupilPraise;
        [DataMember]
        public bool ValidateCode { get; set; }
        [DataMember]
        public List<PupilPraiseLite> LstPupilPraise
        {
            get
            {
                return lstPupilPraise;
            }
            set
            {
                if (value == null)
                {
                    lstPupilPraise = new List<PupilPraiseLite>();
                }
                else
                {
                    lstPupilPraise = value;
                }
            }
        }
    }
    [DataContract]
    public class PupilPraiseLite
    {
        [DataMember]
        public int PraiseID { get; set; }
        [DataMember]
        public String PraiseName { get; set; }// hinh thuc khen thuong
        [DataMember]
        public String PraiseContent { get; set; }// noi dung khen thuong
        [DataMember]
        public DateTime? DateSate { get; set; }// ngay dat
        [DataMember]
        public Int16? PraiseSemester { get; set; }// hoc ky
        [DataMember]
        public String PraiseLevel { get; set; }// lop
        [DataMember]
        public int? PraiseYear { get; set; }// nam hoc
        [DataMember]
        public int? PraiseBy { get; set; }//noi khen thuong
    }
}