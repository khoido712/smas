﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Web;

namespace WCF.Composite
{
    public class SubjectCatLite
    {
        /// ID môn học
        /// </summary>
        [DataMember]
        public int SubjectID { get; set; }

        /// <summary>
        /// Tên môn học
        /// </summary>
        [DataMember]
        public string SubjectName { get; set; }

    }
    /// <summary>
    /// Object co thuoc tinh bao gom doi tuong List<SubjectCatLiteResponse>
    /// </summary>
    [DataContract]
    public class ListSubjectCatLiteResponse
    {
        private List<SubjectCatLite> lstSubjectCatLite;
        /// <summary>
        /// ma loi tra ve
        /// </summary>
        [DataMember]
        public int ValidationCode { get; set; }
        /// <summary>
        /// Object dinh kem
        /// </summary>
        [DataMember]
        public List<SubjectCatLite> LstSubjectCatLite
        {
            get
            {
                return lstSubjectCatLite;
            }
            set
            {
                //Check null gan de thuc hien new item
                if (value == null)
                {
                    this.lstSubjectCatLite = new List<SubjectCatLite>();
                }
                else
                {
                    this.lstSubjectCatLite = value;
                }
            }
        }
    }
}