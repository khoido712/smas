﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Web;

namespace WCF.Composite
{
    [DataContract]
    public class SchoolFacultyDefault
    {
        [DataMember]
        public int SchoolID { get; set; }
        [DataMember]
        public int SchoolFacultyID { get; set; }
        [DataMember]
        public string FacultyName { get; set; }
        [DataMember]
        public bool IsActive { get; set; }
        [DataMember]
        public DateTime CreateTime { get; set; }
    }
}