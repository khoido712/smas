﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Web;
using SMAS.Models.Models;

namespace WCF.Composite
{
    /// <summary>
    /// Lớp dữ liệu dùng cho trang lựa chọn học sinh
    /// </summary>
    [DataContract]
    public class PupilProfileForSelectPupilView
    {
        [DataMember]
        public int PupilID;

        [DataMember]
        public string PupilName;

        [DataMember]
        public string PupilCode;

        [DataMember]
        public DateTime PupilBrithday;

        [DataMember]
        public int PupilGenre;

        [DataMember]
        public int ClassID;

        [DataMember]
        public string ClassName;

        [DataMember]
        public int SchoolID;

        [DataMember]
        public string SchoolName;

        [DataMember]
        public int AcademicID;

        [DataMember]
        public int AcademicYear;

        [DataMember]
        public DateTime AcademicStartDateOfSemester1;

        [DataMember]
        public DateTime AcademicDueDateOfSemester1;

        [DataMember]
        public DateTime AcademicStartDateOfSemester2;

        [DataMember]
        public DateTime AcademicDueDateOfSemester2;

        [DataMember]
        public DateTime? NextAcademicStartDateOfSemester1;

        [DataMember]
        public DateTime? NextAcademicDueDateOfSemester1;

        [DataMember]
        public DateTime? NextAcademicStartDateOfSemester2;

        [DataMember]
        public DateTime? NextAcademicDueDateOfSemester2;

        [DataMember]
        public int Status { get; set; }

        [DataMember]
        public int? ProvinceID { get; set; }

        [DataMember]
        public string ProvinceName { get; set; }

        [DataMember]
        public int? DistrictID { get; set; }

        [DataMember]
        public string DistrictName { get; set; }

        [DataMember]
        public int? CommuneID { get; set; }

        [DataMember]
        public string CommuneName { get; set; }
        [DataMember]
        public string Address { get; set; }
        [DataMember]
        public string SchoolPhoneNumber { get; set; }
        [DataMember]
        public string HeadMasterName { get; set; }
        [DataMember]
        public string HeadMasterPhone { get; set; }
    }

    [DataContract]
    public class ListPupilView
    {
        private List<PupilProfileForSelectPupilView> lstPupilView;
        /// <summary>
        /// ma loi tra ve
        /// </summary>
        [DataMember]
        public int ValidationCode { get; set; }
        /// <summary>
        /// Object dinh kem
        /// </summary>
        [DataMember]
        public List<PupilProfileForSelectPupilView> LstPupilView
        {
            get
            {
                return lstPupilView;
            }
            set
            {
                if (value == null)
                {
                    this.lstPupilView = new List<PupilProfileForSelectPupilView>();
                }
                else
                {
                    this.lstPupilView = value;
                }
            }
        }
    }
}