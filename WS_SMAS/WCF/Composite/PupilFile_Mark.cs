﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using SMAS.Models.Models;
using System.Runtime.Serialization;

namespace WCF.Composite
{
    [DataContract]
    public class PupilAbsent
    {
        public PupilAbsent()
        {
        }
        [DataMember]
        public DateTime AbsentDate { get; set; }
        [DataMember]
        public int AcademicYearID { get; set; }
        [DataMember]
        public int ClassID { get; set; }
        [DataMember]
        public int? CreatedAcademicYear { get; set; }
        [DataMember]
        public int EducationLevelID { get; set; }
        [DataMember]
        public bool? IsAccepted { get; set; }
        [DataMember]
        public bool? IsSMS { get; set; }
        [DataMember]
        public int? Last2digitNumberSchool { get; set; }
        [DataMember]
        public int? M_OldID { get; set; }
        [DataMember]
        public int? M_ProvinceID { get; set; }
        [DataMember]
        public string MSourcedb { get; set; }
        [DataMember]
        public int PupilAbsenceID { get; set; }
        [DataMember]
        public string PupilCode { get; set; }
        [DataMember]
        public int PupilID { get; set; }
        [DataMember]
        public int SchoolID { get; set; }
        [DataMember]
        public int Section { get; set; }
    }

    [DataContract]
    public class PupilFile_Mark
    {
        public PupilFile_Mark()
        {
        }
        string _ppCode = "", _ppFullName = "", _ppBirthday = "", _birthPlace = "", _ppSchoolName = "", _ppClassName = "", _ppConduct = "", _ppConductCN = "", _ppEmulation = "", _tBCM = "", _tBCMCN = "", _xLHL = "", _xLHLCN = "", _level = "", _hk = "";

        [DataMember]
        public string PpConductCN
        {
            get { return _ppConductCN; }
            set { _ppConductCN = value; }
        }

        [DataMember]
        public string XLHLCN
        {
            get { return _xLHLCN; }
            set { _xLHLCN = value; }
        }

        [DataMember]
        public string TBCMCN
        {
            get { return _tBCMCN; }
            set { _tBCMCN = value; }
        }
        int _ppID = 0;
        int schoolYear = 0;

        [DataMember]
        public int SchoolYear
        {
            get { return schoolYear; }
            set { schoolYear = value; }
        }

        [DataMember]
        public int PpID
        {
            get { return _ppID; }
            set { _ppID = value; }
        }
        [DataMember]
        public string Hk
        {
            get { return _hk; }
            set { _hk = value; }
        }

        [DataMember]
        public string Level
        {
            get { return _level; }
            set { _level = value; }
        }

        [DataMember]
        public string XLHL
        {
            get { return _xLHL; }
            set { _xLHL = value; }
        }
        List<TimeTable_Mark> _tkbSang = new List<TimeTable_Mark>();

        [DataMember]
        public List<TimeTable_Mark> TkbSang
        {
            get { return _tkbSang; }
            set { _tkbSang = value; }
        }
        List<TimeTable_Mark> _tkbChieu = new List<TimeTable_Mark>();

        [DataMember]
        public List<TimeTable_Mark> TkbChieu
        {
            get { return _tkbChieu; }
            set { _tkbChieu = value; }
        }
        List<Praise_Mark> _listDiscipline = new List<Praise_Mark>();

        [DataMember]
        public List<Praise_Mark> ListDiscipline
        {
            get { return _listDiscipline; }
            set { _listDiscipline = value; }
        }
        List<Praise_Mark> _listPraise = new List<Praise_Mark>();

        [DataMember]
        public List<Praise_Mark> ListPraise
        {
            get { return _listPraise; }
            set { _listPraise = value; }
        }
        List<PupilAbsent> listAbsent = new List<PupilAbsent>();

        [DataMember]
        public List<PupilAbsent> ListAbsent
        {
            get { return listAbsent; }
            set { listAbsent = value; }
        }
        List<Subject_Mark> lstSubjectMark = new List<Subject_Mark>();
        List<Subject_Mark> lstSubjectJudgeMark = new List<Subject_Mark>();
        List<Subject_Mark> lstTop3Subject = new List<Subject_Mark>();
        List<Subject_Mark> lstBot3Subject = new List<Subject_Mark>();

        [DataMember]
        public List<Subject_Mark> LstBot3Subject
        {
            get { return lstBot3Subject; }
            set { lstBot3Subject = value; }
        }

        [DataMember]
        public List<Subject_Mark> LstTop3Subject
        {
            get { return lstTop3Subject; }
            set { lstTop3Subject = value; }
        }
        [DataMember]
        public string TBCM
        {
            get { return _tBCM; }
            set { _tBCM = value; }
        }

        [DataMember]
        public string PpEmulation
        {
            get { return _ppEmulation; }
            set { _ppEmulation = value; }
        }

        [DataMember]
        public string BirthPlace
        {
            get { return _birthPlace; }
            set { _birthPlace = value; }
        }

        [DataMember]
        public string PpConduct
        {
            get { return _ppConduct; }
            set { _ppConduct = value; }
        }

        [DataMember]
        public List<Subject_Mark> LstSubjectMark
        {
            get { return lstSubjectMark; }
            set { lstSubjectMark = value; }
        }

        [DataMember]
        public List<Subject_Mark> LstSubjectJudgeMark
        {
            get { return lstSubjectJudgeMark; }
            set { lstSubjectJudgeMark = value; }
        }

        [DataMember]
        public string PpClassName
        {
            get { return _ppClassName; }
            set { _ppClassName = value; }
        }

        [DataMember]
        public string PpSchoolName
        {
            get { return _ppSchoolName; }
            set { _ppSchoolName = value; }
        }

        [DataMember]
        public string PpBirthday
        {
            get { return _ppBirthday; }
            set { _ppBirthday = value; }
        }

        [DataMember]
        public string PpFullName
        {
            get { return _ppFullName; }
            set { _ppFullName = value; }
        }

        [DataMember]
        public string PpCode
        {
            get { return _ppCode; }
            set { _ppCode = value; }
        }
    }
    [DataContract]
    public class Subject_Mark
    {
        public Subject_Mark() { }
        string _subjectCode = "", _subjectName = "";
        string _m1 = "", _m2 = "", _m3 = "", _m4 = "", _m5 = "", _p1 = "", _p2 = "", _p3 = "", _p4 = "", _p5 = "", _v1 = "", _v2 = "", _v3 = "", _v4 = "", _v5 = "", _v6 = "", _v7 = "", _v8 = "", _kthk = "", _tbm = "", _tbm2 = "", _tbmCN = "";
        string _teacherName = "", _teacherEmail = "", _teacherPhone = "", _teacherID = "";
        bool subjectType;

        [DataMember]
        public string TeacherID
        {
            get { return _teacherID; }
            set { _teacherID = value; }
        }
        [DataMember]
        public string TeacherPhone
        {
            get { return _teacherPhone; }
            set { _teacherPhone = value; }
        }

        [DataMember]
        public string TeacherEmail
        {
            get { return _teacherEmail; }
            set { _teacherEmail = value; }
        }

        [DataMember]
        public string TeacherName
        {
            get { return _teacherName; }
            set { _teacherName = value; }
        }

        [DataMember]
        public string TbmCN
        {
            get { return _tbmCN; }
            set { _tbmCN = value; }
        }

        [DataMember]
        public string SubjectName
        {
            get { return _subjectName; }
            set { _subjectName = value; }
        }

        [DataMember]
        public string SubjectCode
        {
            get { return _subjectCode; }
            set { _subjectCode = value; }
        }

        [DataMember]
        public bool SubjectType
        {
            get
            {
                return subjectType;
            }
            set
            {
                subjectType = value;
            }
        }

        [DataMember]
        public string Tbm
        {
            get { return _tbm; }
            set { _tbm = value; }
        }
        [DataMember]
        public string Tbm2
        {
            get { return _tbm2; }
            set { _tbm2 = value; }
        }
        [DataMember]
        public string Kthk
        {
            get { return _kthk; }
            set { _kthk = value; }
        }

        [DataMember]
        public string V8
        {
            get
            {
                return _v8;
            }
            set
            {
                _v8 = value;
            }
        }

        [DataMember]
        public string V7
        {
            get
            {
                return _v7;
            }
            set
            {
                _v7 = value;
            }
        }

        [DataMember]
        public string V6
        {
            get
            {
                return _v6;
            }
            set
            {
                _v6 = value;
            }
        }

        [DataMember]
        public string V5
        {
            get { return _v5; }
            set { _v5 = value; }
        }

        [DataMember]
        public string V4
        {
            get { return _v4; }
            set { _v4 = value; }
        }

        [DataMember]
        public string V3
        {
            get { return _v3; }
            set { _v3 = value; }
        }

        [DataMember]
        public string V2
        {
            get { return _v2; }
            set { _v2 = value; }
        }

        [DataMember]
        public string V1
        {
            get { return _v1; }
            set { _v1 = value; }
        }

        [DataMember]
        public string P5
        {
            get { return _p5; }
            set { _p5 = value; }
        }

        [DataMember]
        public string P4
        {
            get { return _p4; }
            set { _p4 = value; }
        }

        [DataMember]
        public string P3
        {
            get { return _p3; }
            set { _p3 = value; }
        }

        [DataMember]
        public string P2
        {
            get { return _p2; }
            set { _p2 = value; }
        }

        [DataMember]
        public string P1
        {
            get { return _p1; }
            set { _p1 = value; }
        }

        [DataMember]
        public string M5
        {
            get { return _m5; }
            set { _m5 = value; }
        }

        [DataMember]
        public string M4
        {
            get { return _m4; }
            set { _m4 = value; }
        }

        [DataMember]
        public string M3
        {
            get { return _m3; }
            set { _m3 = value; }
        }

        [DataMember]
        public string M2
        {
            get { return _m2; }
            set { _m2 = value; }
        }

        [DataMember]
        public string M1
        {
            get { return _m1; }
            set { _m1 = value; }
        }
    }
    [DataContract]
    public class Praise_Mark
    {
        string _date = "", _hinhThuc = "", _phamVi = "", _noiDung = "";

        [DataMember]
        public string NoiDung
        {
            get { return _noiDung; }
            set { _noiDung = value; }
        }

        [DataMember]
        public string PhamVi
        {
            get { return _phamVi; }
            set { _phamVi = value; }
        }

        [DataMember]
        public string HinhThuc
        {
            get { return _hinhThuc; }
            set { _hinhThuc = value; }
        }

        [DataMember]
        public string Date
        {
            get { return _date; }
            set { _date = value; }
        }
    }
    [DataContract]
    public class TimeTable_Mark
    {
        public TimeTable_Mark() { }
        string _thu = "", _tiet = "", _monhoc = "", _giaoVien = "";

        [DataMember]
        public string GiaoVien
        {
            get { return _giaoVien; }
            set { _giaoVien = value; }
        }

        [DataMember]
        public string Monhoc
        {
            get { return _monhoc; }
            set { _monhoc = value; }
        }

        [DataMember]
        public string Tiet
        {
            get { return _tiet; }
            set { _tiet = value; }
        }

        [DataMember]
        public string Thu
        {
            get { return _thu; }
            set { _thu = value; }
        }
    }
}