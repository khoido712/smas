﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Web;

namespace WCF.Composite
{
    [DataContract]
    public class TT22ResultComposite
    {
        [DataMember]
        public int ValidateCode { get; set; }

        [DataMember]
        public string RewardSemester1 { get; set; }

        [DataMember]
        public string RewardSemester2 { get; set; }

        [DataMember]
        public string EndYearResult { get; set; }

        private List<TT22MHHDGDResultComposite> lstMHHDGDSemester1 { get; set; }

        private List<TT22MHHDGDResultComposite> lstMHHDGDSemester2 { get; set; }

        private List<TT22CapacityQualityComposite> lstCapacityResult { get; set; }

        private List<TT22CapacityQualityComposite> lstQualityResult { get; set; }

        [DataMember]
        public List<TT22MHHDGDResultComposite> LstMHHDGDSemester1
        {
            get
            {
                return lstMHHDGDSemester1;
            }
            set
            {
                //Check null gan de thuc hien khoi tao 
                if (value == null)
                {
                    this.lstMHHDGDSemester1 = new List<TT22MHHDGDResultComposite>();
                }
                else
                {
                    this.lstMHHDGDSemester1 = value;
                }
            }
        }

        [DataMember]
        public List<TT22MHHDGDResultComposite> LstMHHDGDSemester2
        {
            get
            {
                return lstMHHDGDSemester2;
            }
            set
            {
                //Check null gan de thuc hien khoi tao 
                if (value == null)
                {
                    this.lstMHHDGDSemester2 = new List<TT22MHHDGDResultComposite>();
                }
                else
                {
                    this.lstMHHDGDSemester2 = value;
                }
            }
        }

        [DataMember]
        public List<TT22CapacityQualityComposite> LstCapacityResult
        {
            get
            {
                return lstCapacityResult;
            }
            set
            {
                //Check null gan de thuc hien khoi tao 
                if (value == null)
                {
                    this.lstCapacityResult = new List<TT22CapacityQualityComposite>();
                }
                else
                {
                    this.lstCapacityResult = value;
                }
            }
        }

        [DataMember]
        public List<TT22CapacityQualityComposite> LstQualityResult
        {
            get
            {
                return lstQualityResult;
            }
            set
            {
                //Check null gan de thuc hien khoi tao 
                if (value == null)
                {
                    this.lstQualityResult = new List<TT22CapacityQualityComposite>();
                }
                else
                {
                    this.lstQualityResult = value;
                }
            }
        }

    }

    [DataContract]
    public class TT22MHHDGDResultComposite
    {
        [DataMember]
        public string SubjectName { get; set; }

        [DataMember]
        public string SubjectCode { get; set; }

        [DataMember]
        public int IsComment { get; set; }

        [DataMember]
        public string ResultAchivedMidleSemester { get; set; }

        [DataMember]
        public string PeriodMarkMidleSemester { get; set; }

        [DataMember]
        public string ResultAchivedEndleSemester { get; set; }

        [DataMember]
        public string PeriodMarkEndSemester { get; set; }

        [DataMember]
        public string Comment { get; set; }

    }

    [DataContract]
    public class TT22CapacityQualityComposite
    {
        [DataMember]
        public string CriteriaName { get; set; }

        [DataMember]
        public string ResultMidleSemester1 { get; set; }

        [DataMember]
        public string ResultEndSemester1 { get; set; }

        [DataMember]
        public string CommentSemester1 { get; set; }

        [DataMember]
        public string ResultMidleSemester2 { get; set; }

        [DataMember]
        public string ResultEndSemester2 { get; set; }

        [DataMember]
        public string CommentSemester2 { get; set; }

    }
}