﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Web;

namespace WCF.Composite
{
    [DataContract]
    public class SchoolProfileDefault
    {
        /// <summary>
        /// Địa chỉ
        /// </summary>
        [DataMember]
        public string Address { get; set; }

        /// <summary>
        /// Khu vuc
        /// </summary>
        [DataMember]
        public int AreaID { get; set; }

        /// <summary>
        /// User Admin của trường
        /// </summary>
        [DataMember]
        public int AdminID { get; set; }


        [DataMember]
        public int CommuneID { get; set; }
        /// <summary>
        /// Xã
        /// </summary>
        [DataMember]
        public string CommuneName { get; set; }

        /// <summary>
        /// ma quan huyen
        /// </summary>
        [DataMember]
        public int DistrictID { get; set; }

        /// <summary>
        /// Huyện
        /// </summary>
        [DataMember]
        public string DistrictName { get; set; }

        /// <summary>
        /// Cấp học
        /// </summary>
        [DataMember]
        public int EducationGrade { get; set; }

        /// <summary>
        ///  Email
        /// </summary>
        [DataMember]
        public string Email { get; set; }

        /// <summary>
        /// Ngày thành lập
        /// </summary>
        [DataMember]
        public DateTime EstablishedDate { get; set; }

        /// <summary>
        /// Fax
        /// </summary>
        /// 
        [DataMember]
        public string Fax { get; set; }
        /// <summary>
        /// Tên hiệu trường
        /// </summary>
        [DataMember]
        public string HeadMasterName { get; set; }

        /// <summary>
        /// SĐT hiệu trưởng
        /// </summary>
        [DataMember]
        public string HeadMasterPhone { get; set; }

        /// <summary>
        /// ma tinh thanh
        /// </summary>
        [DataMember]
        public int ProvinceID { get; set; }

        /// <summary>
        /// Tỉnh
        /// </summary>
        [DataMember]
        public string ProvinceName { get; set; }

        /// <summary>
        /// Mã trường
        /// </summary>
        [DataMember]
        public string SchoolCode { get; set; }

        /// <summary>
        /// Tên trường
        /// </summary>
        [DataMember]
        public string SchoolName { get; set; }

        /// <summary>
        /// ID trường
        /// </summary>
        [DataMember]
        public int SchoolProfileID { get; set; }

        /// <summary>
        /// Tieu de nam hoc
        /// </summary>
        [DataMember]
        public string SchoolYearTitle { get; set; }

        /// <summary>
        /// Tên viết tắt
        /// </summary>
        [DataMember]
        public string ShortName { get; set; }

        /// <summary>
        /// ID trường
        /// </summary>
        [DataMember]
        public int SupervisingDeptID { get; set; }

        /// <summary>
        /// Tên đơn vị quản lý
        /// </summary>
        [DataMember]
        public string SupervisingDeptName { get; set; }


        [DataMember]
        public string SchoolUserName { get; set; }
        /// <summary>
        /// ĐT
        /// </summary>
        [DataMember]
        public string Telephone { get; set; }

        /// <summary>
        /// Web
        /// </summary>
        [DataMember]
        public string Website { get; set; }

        /// <summary>
        /// Web
        /// </summary>
        [DataMember]
        public int SMSParentActiveType { get; set; }

        /// <summary>
        /// Web
        /// </summary>
        [DataMember]
        public int SMSTeacherActiveType { get; set; }
        
        /// <summary>
        /// Mo hinh truong hoc moi (VNEN)
        /// </summary>
        [DataMember]
        public bool IsNewSchoolModel { get; set; }

        /// <summary>
        /// Phien ban truong dang su dung
        /// </summary>
        [DataMember]
        public byte ProductVersion { get; set; }

        /// <summary>
        /// La truong giao duc thuong xuyen (GDTX)
        /// </summary>
        [DataMember]
        public bool VocationType { get; set; }
    }

    /// <summary>
    /// DataContract gom danh sach thuoc tinh
    /// </summary>
    [DataContract]
    public class SchoolProfileResponse
    {
        private SchoolProfileDefault objSchoolProfileDefault;
        /// <summary>
        /// ma loi tra ve
        /// </summary>
        [DataMember]
        public int ValidationCode { get; set; }
        /// <summary>
        /// Object dinh kem
        /// </summary>
        [DataMember]
        public SchoolProfileDefault ObjSchoolProfileDefault
        {
            get
            {
                return objSchoolProfileDefault;
            }
            set
            {
                //Check null gan de thuc hien khoi tao 
                if (value == null)
                {
                    this.objSchoolProfileDefault = new SchoolProfileDefault();
                }
                else
                {
                    this.objSchoolProfileDefault = value;
                }
            }
        }
    }

    [DataContract]
    public class SchoolProfileLite
    {
        /// <summary>
        /// ID trường
        /// </summary>
        [DataMember]
        public int SchoolID { get; set; }

        /// <summary>
        /// Tên trường
        /// </summary>
        [DataMember]
        public string SchoolName { get; set; }

        /// <summary>
        /// Tên huyện
        /// </summary>
        [DataMember]
        public string DistrictName { get; set; }

        /// <summary>
        /// Tên hiệu trường
        /// </summary>
        [DataMember]
        public string HeadMasterName { get; set; }

        /// <summary>
        /// SĐT hiệu trưởng
        /// </summary>
        [DataMember]
        public string HeadMasterPhone { get; set; }

        /// <summary>
        /// id phong ban
        /// </summary>
        [DataMember]
        public int SupervisingDeptID { get; set; }

        /// <summary>
        /// id phong ban
        /// </summary>
        [DataMember]
        public int ProvinceID { get; set; }

        [DataMember]
        public int DistrictID { get; set; }

        [DataMember]
        public int EducationGrade { get; set; }

        [DataMember]
        public bool IsActive { get; set; }  
    }

    /// <summary>
    /// DataContract gom danh thong tin tra ve va ma ket qua
    /// </summary>
    [DataContract]
    public class ListSchoolProfileLiteResponse
    {
        private List<SchoolProfileLite> lstSchoolProfileLite;
        /// <summary>
        /// ma loi tra ve
        /// </summary>
        [DataMember]
        public int ValidationCode { get; set; }
        /// <summary>
        /// Object dinh kem
        /// </summary>
        [DataMember]
        public List<SchoolProfileLite> LstSchoolProfileLite
        {
            get
            {
                return lstSchoolProfileLite;
            }
            set
            {
                //Check null gan de thuc hien new item
                if (value == null)
                {
                    this.lstSchoolProfileLite = new List<SchoolProfileLite>();
                }
                else
                {
                    this.lstSchoolProfileLite = value;
                }
            }
        }
    }

    /// <summary>
    /// DataContract gom danh thong tin tra ve va ma ket qua
    /// </summary>
    [DataContract]
    public class ListSchoolProfileLiteByRegionResponse
    {
        private List<SchoolProfileLite> lstSchoolProfileLite;
        /// <summary>
        /// ma loi tra ve
        /// </summary>
        [DataMember]
        public int ValidationCode { get; set; }
        /// <summary>
        /// Object dinh kem
        /// </summary>
        [DataMember]
        public List<SchoolProfileLite> LstSchoolProfileLite
        {
            get
            {
                return lstSchoolProfileLite;
            }
            set
            {
                //Check null gan de thuc hien new item
                if (value == null)
                {
                    this.lstSchoolProfileLite = new List<SchoolProfileLite>();
                }
                else
                {
                    this.lstSchoolProfileLite = value;
                }
            }
        }

        /// <summary>
        /// Tổng số bản ghi
        /// </summary>
        [DataMember]
        public int TotalRecord { get; set; }
    }


    [DataContract]
    public class SchoolRequest
    {
        [DataMember]
        public int ProvinceID { get; set; }

        [DataMember]
        public int DistrictID { get; set; }
        [DataMember]
        public string SchoolName { get; set; }
        [DataMember]
        public string UserName { get; set; }
        [DataMember]
        public int Status { get; set; }

        [DataMember]
        public int Total { get; set; }

        [DataMember]
        public int Page { get; set; }
        [DataMember]
        public int PageSize { get; set; }

        [DataMember]
        public List<int> LstSchoolID { get; set; }
    }

    [DataContract]
    public class ListSchoolResponse
    {
        private List<SchoolProfileDefault> lstSchoolProfile;
        /// <summary>
        /// ma loi tra ve
        /// </summary>
        [DataMember]
        public int ValidationCode { get; set; }
        /// <summary>
        /// Object dinh kem
        /// </summary>
        [DataMember]
        public List<SchoolProfileDefault> LstSchoolProfile
        {
            get
            {
                return lstSchoolProfile;
            }
            set
            {
                //Check null gan de thuc hien new item
                if (value == null)
                {
                    this.lstSchoolProfile = new List<SchoolProfileDefault>();
                }
                else
                {
                    this.lstSchoolProfile = value;
                }
            }
        }

        /// <summary>
        /// Tong so object
        /// </summary>
        [DataMember]
        public int Total { get; set; }
    }
}