﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Web;

namespace WCF.Composite
{
     [DataContract]
    public class ProvinceDefault
    {
        [DataMember]
        public int ProvinceID { get; set; }

        [DataMember]
        public string ProvinceCode { get; set; }

        [DataMember]
        public string ProvinceName { get; set; }

        [DataMember]
        public string ShortName { get; set; }

        [DataMember]
        public string Description { get; set; }
    }

    [DataContract]
     public class ProvinceResponse
     {
         private ProvinceDefault objProvince;
         /// <summary>
         /// ma loi tra ve
         /// </summary>
         [DataMember]
         public int ValidationCode { get; set; }
         /// <summary>
         /// Object dinh kem
         /// </summary>
         [DataMember]
         public ProvinceDefault ObjProvince
         {
             get
             {
                 return objProvince;
             }
             set
             {
                 //Check null gan de thuc hien khoi tao 
                 if (value == null)
                 {
                     this.objProvince = new ProvinceDefault();
                 }
                 else
                 {
                     this.objProvince = value;
                 }
             }
         }
     }

    [DataContract]
    public class ListProvinceResponse
    {
        private List<ProvinceDefault> lstProvinceDefault;
        /// <summary>
        /// ma loi tra ve
        /// </summary>
        [DataMember]
        public int ValidationCode { get; set; }
        /// <summary>
        /// danh sach Object dinh kem
        /// </summary>
        [DataMember]
        public List<ProvinceDefault> LstProvinceDefault
        {
            get
            {
                return lstProvinceDefault;
            }
            set
            {
                //Check null gan de thuc hien new item
                if (value == null)
                {
                    this.lstProvinceDefault = new List<ProvinceDefault>();
                }
                else
                {
                    this.lstProvinceDefault = value;
                }
            }
        }
    }
}