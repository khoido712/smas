﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Web;

namespace WCF.Composite
{
    [DataContract]
    public class UserAccountDefault
    {
        /// <summary>
        /// Tên giáo viên
        /// </summary>
        [DataMember]
        public string EmployeeName { get; set; }

        /// <summary>
        /// ID giáo viên
        /// </summary>
        [DataMember]
        public int EmployeeID { get; set; }

        /// <summary>
        /// Trạng thái
        /// </summary>
        [DataMember]
        public bool IsActive { get; set; }

        /// <summary>
        /// Là user admin hay không
        /// </summary>
        [DataMember]
        public bool IsAdmin { get; set; }

        /// <summary>
        /// UserAccountID
        /// </summary>
        [DataMember]
        public int UserAccountID { get; set; }

        /// <summary>
        /// GUID mapping voi membership
        /// </summary>
        [DataMember]
        public Guid MembershipUserID { get; set; }
    }
    [DataContract]
    public class SemesterDeclarationLite
    {
        [DataMember]
        public int SemesterDeclarationID { get; set; }
        [DataMember]
        public int InterviewMark { get; set; }//điểm miệng
        [DataMember]
        public int WritingMark { get; set; }//điểm 15P
        [DataMember]
        public int TwiceCoeffiecientMark { get; set; }//điểm 1T
        [DataMember]
        public bool? IsLock { get; set; }//Khóa/Chưa khóa
    }
    /// <summary>
    /// DataContract gom danh sach thuoc tinh
    /// </summary>
    [DataContract]
    public class UserAccountResponse
    {
        private UserAccountDefault objUserAccount;
        /// <summary>
        /// ma loi tra ve
        /// </summary>
        [DataMember]
        public int ValidationCode { get; set; }
        /// <summary>
        /// Object dinh kem
        /// </summary>
        [DataMember]
        public UserAccountDefault ObjUserAccount
        {
            get
            {
                return objUserAccount;
            }
            set
            {
                //Check null gan de thuc hien khoi tao 
                if (value == null)
                {
                    this.objUserAccount = new UserAccountDefault();
                }
                else
                {
                    this.objUserAccount = value;
                }
            }
        }
    }

    [DataContract]
    public class SemesterDeclarationResponse
    {
        private List<SemesterDeclarationLite> lstSemesterDeclaration;
        private SemesterDeclarationLite objSemesterDeclaration;
        /// <summary>
        /// ma loi tra ve
        /// </summary>
        [DataMember]
        public int ValidationCode { get; set; }
        /// <summary>
        /// Object dinh kem
        /// </summary>
        [DataMember]
        public List<SemesterDeclarationLite> LstSemesterDeclaration
        {
            get
            {
                return lstSemesterDeclaration;
            }
            set
            {
                if (value == null)
                {
                    lstSemesterDeclaration = new List<SemesterDeclarationLite>();
                }
                else
                {
                    lstSemesterDeclaration = value;
                }
            }
        }
        [DataMember]
        public SemesterDeclarationLite ObjSemesterDeclaration
        {
            get
            {
                return objSemesterDeclaration;
            }
            set
            {
                if (value == null)
                {
                    objSemesterDeclaration = new SemesterDeclarationLite();
                }
                else
                {
                    objSemesterDeclaration = value;
                }
            }
        }
    }

}