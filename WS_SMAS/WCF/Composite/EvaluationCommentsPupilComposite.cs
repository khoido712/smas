﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Web;

namespace WCF.Composite
{

    [DataContract]
    public class EvaluationCommentsPupil
    {
        /// <summary>
        /// validation code Object
        /// </summary>
        public int ValidationCode { get; set; }
        /// <summary>
        /// ma hoc sinh
        /// </summary>
        [DataMember]
        public int PupilID { get; set; }

        [DataMember]
        public int EvaluationID { get; set; }

        /// <summary>
        /// ID hoc ky
        /// </summary>
        [DataMember]
        public int SemesterID { get; set; }

        /// <summary>
        ///  ten hoc sinh
        /// </summary>
        [DataMember]
        public string PupilName { get; set; }

        /// <summary>
        /// Nhan xet thang 1
        /// </summary>
        [DataMember]
        public string EvaluationMonth1 { get; set; }
        /// <summary>
        /// Nhan xet thang 1
        /// </summary>
        [DataMember]
        public string EvaluationMonth2 { get; set; }
        /// <summary>
        /// Nhan xet thang 1
        /// </summary>
        [DataMember]
        public string EvaluationMonth3 { get; set; }
        /// <summary>
        /// Nhan xet thang 1
        /// </summary>
        [DataMember]
        public string EvaluationMonth4 { get; set; }
        /// <summary>
        /// Nhan xet thang 1
        /// </summary>
        [DataMember]
        public string EvaluationMonth5 { get; set; }
        /// <summary>
        /// Nhan xet thang 1
        /// </summary>
        [DataMember]
        public string EvaluationMonth6 { get; set; }
        /// <summary>
        /// Nhan xet thang 1
        /// </summary>
        [DataMember]
        public string EvaluationMonth7 { get; set; }
        /// <summary>
        /// Nhan xet thang 1
        /// </summary>
        [DataMember]
        public string EvaluationMonth8 { get; set; }
        /// <summary>
        /// Nhan xet thang 1
        /// </summary>
        [DataMember]
        public string EvaluationMonth9 { get; set; }
        /// <summary>
        /// Nhan xet thang 1
        /// </summary>
        [DataMember]
        public string EvaluationMonth10 { get; set; }

        /// <summary>
        /// ID mon hoc
        /// </summary>
        [DataMember]
        public int SubjectID { get; set; }

        /// <summary>
        /// ID mon hoc
        /// </summary>
        [DataMember]
        public string SubjectName { get; set; }

        /// <summary>
        /// diem voi mon tinh diem
        /// </summary>
        [DataMember]
        public int? Mark { get; set; }

        /// <summary>
        /// Danh gia
        /// </summary>
        [DataMember]
        public string EvaluationEnding { get; set; }

        /// <summary>
        /// Nhan xet
        /// </summary>
        [DataMember]
        public string EndingComments { get; set; }

        /// <summary>
        /// Xac dinh mon nhan xet mon tinh diem
        /// 0: tinh diem,1: nhan xet
        /// </summary>
        [DataMember]
        public int isCommenting { get; set; }

    }

    [DataContract]
    public class SubjectList
    {
        /// <summary>
        /// ID mon hoc
        /// </summary>
        [DataMember]
        public int SubjectID { get; set; }

        /// <summary>
        /// ID mon hoc
        /// </summary>
        [DataMember]
        public string SubjectName { get; set; }

        /// <summary>
        /// Xac dinh mon nhan xet mon tinh diem
        /// 0: tinh diem,1: nhan xet
        /// </summary>
        [DataMember]
        public int isCommenting { get; set; }

        [DataMember]
        public int? OrderSubject { get; set; }

        [DataMember]
        public bool? IsSubjectVNEN { get; set; }
    }

    [DataContract]
    public class EducationQuality
    {
        /// <summary>
        /// ma hoc sinh
        /// </summary>
        [DataMember]
        public int PupilID { get; set; }

        /// <summary>
        /// ID mon hoc, ID tieu chi danh gia
        /// </summary>
        [DataMember]
        public int SubjectID { get; set; }

        [DataMember]
        public string SubjectName { get; set; }

        /// <summary>
        /// Xac dinh mon nhan xet mon tinh diem
        /// 0: tinh diem,1: nhan xet
        /// </summary>
        [DataMember]
        public int isCommenting { get; set; }

        /// <summary>
        /// diem voi mon tinh diem
        /// </summary>
        [DataMember]
        public decimal? Mark { get; set; }

        /// <summary>
        /// Danh gia
        /// </summary>
        [DataMember]
        public string EvaluationEnding { get; set; }

        /// <summary>
        /// Nhan xet
        /// </summary>
        [DataMember]
        public string EndingComments { get; set; }

        [DataMember]
        public int SemesterID { get; set; }

        [DataMember]
        public int EvaluationID { get; set; }

        /// <summary>
        /// diem voi mon nhan xet
        /// </summary>
        [DataMember]
        public string JudgeComment { get; set; }
    }

    [DataContract]
    public class CapacitiesAndQualities
    {
        /// <summary>
        /// ma hoc sinh
        /// </summary>
        [DataMember]
        public int PupilID { get; set; }

        /// <summary>
        /// ID mon hoc, ID tieu chi danh gia
        /// </summary>
        [DataMember]
        public int EvaluationCretirea { get; set; }

        /// <summary>
        /// Xac dinh mon nhan xet mon tinh diem
        /// 0: tinh diem,1: nhan xet
        /// </summary>
        [DataMember]
        public int isCommenting { get; set; }

        /// <summary>
        /// Danh gia
        /// </summary>
        [DataMember]
        public string EvaluationEnding { get; set; }

        /// <summary>
        /// Nhan xet
        /// </summary>
        [DataMember]
        public string EndingComments { get; set; }

        [DataMember]
        public int SemesterID { get; set; }

        [DataMember]
        public int EvaluationID { get; set; }

    }

    [DataContract]
    public class RewardFinalAndSummedEnding
    {
        [DataMember]
        public string OutStandingSemester1 { get; set; }

        [DataMember]
        public string OutStandingSemester2 { get; set; }

        [DataMember]
        public string RewardSemester1 { get; set; }

        [DataMember]
        public string RewardSemester2 { get; set; }

        [DataMember]
        public string EvaluationFinal { get; set; }

        #region VNEN
        [DataMember]
        public string CorrectiveCommentSem1 { get; set; }

        [DataMember]
        public string CorrectiveCommentSem2 { get; set; }

        [DataMember]
        public string CapacityEvaluationSem1 { get; set; }

        [DataMember]
        public string CapacityEvaluationSem2 { get; set; }

        [DataMember]
        public string QualityEvaluationSem1 { get; set; }

        [DataMember]
        public string QualityEvaluationSem2 { get; set; }
        #endregion
    }

    [DataContract]
    public class EvaluationCommentsPupilComposite
    {
        private List<EvaluationCommentsPupil> listResult;
        private List<SubjectList> listSubject;
        private List<EducationQuality> listEducationQuality;
        private List<CapacitiesAndQualities> listCapacitiesAndQualities;
        private RewardFinalAndSummedEnding rewardFinalAndSummedEnding;

        [DataMember]
        public int ValidateCode { get; set; }

        [DataMember]
        public List<EvaluationCommentsPupil> ListResult
        {
            get
            {
                return listResult;
            }
            set
            {
                //Check null gan de thuc hien khoi tao 
                if (value == null)
                {
                    this.listResult = new List<EvaluationCommentsPupil>();
                }
                else
                {
                    this.listResult = value;
                }
            }
        }

        [DataMember]
        public List<SubjectList> ListSubject
        {
            get
            {
                return listSubject;
            }
            set
            {
                //Check null gan de thuc hien khoi tao 
                if (value == null)
                {
                    this.listSubject = new List<SubjectList>();
                }
                else
                {
                    this.listSubject = value;
                }
            }
        }

        [DataMember]
        public List<EducationQuality> ListEducationQuality
        {
            get
            {
                return listEducationQuality;
            }
            set
            {
                //Check null gan de thuc hien khoi tao 
                if (value == null)
                {
                    this.listEducationQuality = new List<EducationQuality>();
                }
                else
                {
                    this.listEducationQuality = value;
                }
            }
        }


        [DataMember]
        public List<CapacitiesAndQualities> ListCapacitiesAndQualities
        {
            get
            {
                return listCapacitiesAndQualities;
            }
            set
            {
                //Check null gan de thuc hien khoi tao 
                if (value == null)
                {
                    this.listCapacitiesAndQualities = new List<CapacitiesAndQualities>();
                }
                else
                {
                    this.listCapacitiesAndQualities = value;
                }
            }
        }

        [DataMember]
        public RewardFinalAndSummedEnding RewardFinalAndSummedEnding
        {
            get
            {
                return rewardFinalAndSummedEnding;
            }
            set
            {
                if (value == null)
                {
                    this.rewardFinalAndSummedEnding = null;
                }
                else
                {
                    this.rewardFinalAndSummedEnding = value;
                }
            }
        }

    }
}
