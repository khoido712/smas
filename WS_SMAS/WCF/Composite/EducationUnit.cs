﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Web;

namespace WCF.Composite
{
    [DataContract]
    public class EducationUnitResponse
    {
        /// <summary>
        /// ID ĐVGD
        /// </summary>
        [DataMember]
        public int EducationUnitID { get; set; }


        /// <summary>
        /// Loại ĐVGD
        /// </summary>
        [DataMember]
        public int EducationUnitType { get; set; }

        /// <summary>
        /// Cấp học
        /// </summary>
        [DataMember]
        public int EducationGrade { get; set; }

        /// <summary>
        /// Mã ĐVGD
        /// </summary>
        [DataMember]
        public string EducationUnitCode { get; set; }

        /// <summary>
        /// Tên ĐVGD
        /// </summary>
        [DataMember]
        public string EducationUnitName { get; set; }

        /// <summary>
        /// Địa chỉ
        /// </summary>
        [DataMember]
        public string Address { get; set; }

        /// <summary>
        /// ma vung mien
        /// </summary>
        [DataMember]
        public int AreaID { get; set; }
        
        /// <summary>
        /// ma tinh thanh
        /// </summary>
        [DataMember]
        public int ProvinceID { get; set; }

        /// <summary>
        /// ma quan huyen
        /// </summary>
        [DataMember]
        public int DistrictID { get; set; }


        /// <summary>
        /// Mã Xã/Phường
        /// </summary>
        [DataMember]
        public int CommuneID { get; set; }
        /// <summary>
        /// Xã
        /// </summary>
        [DataMember]
        public string CommuneName { get; set; }

        /// <summary>
        /// Ma Thon/Xom
        /// </summary>
        [DataMember]
        public int VillageID { get; set; }

        /// <summary>
        /// Ten Thon/Xom
        /// </summary>
        [DataMember]
        public string VillageName { get; set; }

        /// <summary>
        ///  Email
        /// </summary>
        [DataMember]
        public string Email { get; set; }

        /// <summary>
        /// Ngày thành lập
        /// </summary>
        [DataMember]
        public DateTime EstablishedDate { get; set; }

        /// <summary>
        /// Tên chủ ĐVGD
        /// </summary>
        [DataMember]
        public string EducationUnitOwnerName { get; set; }

        /// <summary>
        /// SĐT chu ĐVGD
        /// </summary>
        [DataMember]
        public string EducationUnitOwnerPhone { get; set; }

        /// <summary>
        /// ID cha (nếu có)
        /// </summary>
        [DataMember]
        public int ParentID { get; set; }

        /// <summary>
        /// ID cha (nếu có)
        /// </summary>
        [DataMember]
        public int HierachyLevel { get; set; }
        
        /// <summary>
        /// Tên tai khoan
        /// </summary>
        [DataMember]
        public string EducationUnitUserName { get; set; }

        /// <summary>
        /// ĐT
        /// </summary>
        [DataMember]
        public string Telephone { get; set; }

        /// <summary>
        /// Web
        /// </summary>
        [DataMember]
        public string Website { get; set; }
    }

    [DataContract]
    public class LstEducationUnitResponse
    {
        private List<EducationUnitResponse> listEducationUnitResponse;
        /// <summary>
        /// ma loi tra ve
        /// </summary>
        [DataMember]
        public int ValidationCode { get; set; }
        /// <summary>
        /// Object dinh kem
        /// </summary>
        [DataMember]
        public List<EducationUnitResponse> ListEducationUnitResponse
        {
            get
            {
                return listEducationUnitResponse;
            }
            set
            {
                //Check null gan de thuc hien new item
                if (value == null)
                {
                    this.listEducationUnitResponse = new List<EducationUnitResponse>();
                }
                else
                {
                    this.listEducationUnitResponse = value;
                }
            }
        }
    }


    #region Object su dung de thanh toan phi dich vu SMSEDU qua BP
    public class EducationUnitDefault
    {
        /// <summary>
        /// ID ĐVGD
        /// </summary>
        [DataMember]
        public int EducationUnitID { get; set; }

        /// <summary>
        /// Loại ĐVGD
        /// </summary>
        [DataMember]
        public int EducationUnitType { get; set; }


        /// <summary>
        /// Mã ĐVGD
        /// </summary>
        [DataMember]
        public string EducationUnitCode { get; set; }

        /// <summary>
        /// Tên ĐVGD
        /// </summary>
        [DataMember]
        public string EducationUnitName { get; set; }

        /// <summary>
        /// Tinh trang
        /// </summary>
        [DataMember]
        public bool IsActive { get; set; }

        /// <summary>
        /// Trang thai kich hoat SMAS
        /// </summary>
        [DataMember]
        public bool IsActiveSmas { get; set; }

        /// <summary>
        /// Trang thai SMS cho Teacher
        /// </summary>
        [DataMember]
        public int SMSActiveType { get; set; }

        /// <summary>
        /// Trang thai Kich hoat tai khoan quan tri
        /// </summary>
        [DataMember]
        public bool IsApproved { get; set; }

        /// <summary>
        /// ma loi tra ve
        /// </summary>
        [DataMember]
        public int ValidationCode { get; set; }
    }
    #endregion
}