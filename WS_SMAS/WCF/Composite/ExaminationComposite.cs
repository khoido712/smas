﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Web;

namespace WCF.Composite
{
    [DataContract]
    public class ExaminationRequest
    {
        [DataMember]
        public int SchoolID { get; set; }
        [DataMember]
        public int AcademicYearID { get; set; }
        [DataMember]
        public int Semester { get; set; }
        [DataMember]
        public long ExaminationID { get; set; }

        [DataMember]
        public int AppliedLevel { get; set; }
    }

    [DataContract]
    public class ExaminationResponse
    {
        [DataMember]
        public long ExaminationID { get; set; }
        [DataMember]
        public int SchoolID { get; set; }
        [DataMember]
        public int AcademicYearID { get; set; }
        [DataMember]
        public int Semester { get; set; }
        [DataMember]
        public string ExaminationName { get; set; }
        [DataMember]
        public bool MarkInput { get; set; }
        [DataMember]
        public bool MarkClosing { get; set; }

        [DataMember]
        public List<ExamSubjectResponse> LstExamSubject { get; set; }
    }

    [DataContract]
    public class ListExamResponse
    {
        private List<ExaminationResponse> lstExamination;
        /// <summary>
        /// ma loi tra ve
        /// </summary>
        [DataMember]
        public int ValidationCode { get; set; }
        /// <summary>
        /// Object dinh kem
        /// </summary>
        [DataMember]
        public List<ExaminationResponse> LstExamination
        {
            get
            {
                return lstExamination;
            }
            set
            {
                if (value == null)
                {
                    this.lstExamination = new List<ExaminationResponse>();
                }
                else
                {
                    this.lstExamination = value;
                }
            }
        }
    }


    [DataContract]
    public class ExamSubjectResponse
    {
        [DataMember]
        public long ExamSubjectID { get; set; }
        [DataMember]
        public long ExaminationID { get; set; }
        [DataMember]
        public string ExaminationName { get; set; }
        [DataMember]
        public int SubjectID { get; set; }
        [DataMember]
        public string SubjectName { get; set; }
        [DataMember]
        public string ScheduleContent { get; set; }

        [DataMember]
        public int PupilID { get; set; }

        [DataMember]
        public string PupilName { get; set; }
    }

    [DataContract]
    public class ListExamSubjectResponse
    {
        private List<ExamSubjectResponse> lstExamSubject;
        /// <summary>
        /// ma loi tra ve
        /// </summary>
        [DataMember]
        public int ValidationCode { get; set; }
        /// <summary>
        /// Object dinh kem
        /// </summary>
        [DataMember]
        public List<ExamSubjectResponse> LstExamSubject
        {
            get
            {
                return lstExamSubject;
            }
            set
            {
                if (value == null)
                {
                    this.lstExamSubject = new List<ExamSubjectResponse>();
                }
                else
                {
                    this.lstExamSubject = value;
                }
            }
        }
    }


    [DataContract]
    public class ExamMarkPupilResponse
    {
        /// <summary>
        /// ma hoc sinh
        /// </summary>
        [DataMember]
        public int PupilID { get; set; }

        /// <summary>
        ///  ten hoc sinh
        /// </summary>
        [DataMember]
        public string PupilName { get; set; }

        /// <summary>
        /// Ket qua ky thi
        /// </summary>
        [DataMember]
        public string ExamResult { get; set; }

        [DataMember]
        public long ExaminationID { get; set; }
        [DataMember]
        public string ExaminationName { get; set; }
    }

    [DataContract]
    public class ListExamMarkPupilResponse
    {
        private List<ExamMarkPupilResponse> lstExamMarkPupil;

        [DataMember]
        public int ValidateCode { get; set; }

        [DataMember]
        public List<ExamMarkPupilResponse> LstExamMarkPupil
        {
            get
            {
                return lstExamMarkPupil;
            }
            set
            {
                if (value == null)
                {
                    this.lstExamMarkPupil = new List<ExamMarkPupilResponse>();
                }
                else
                {
                    this.lstExamMarkPupil = value;
                }
            }
        }
    }
}