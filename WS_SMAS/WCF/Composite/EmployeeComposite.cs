﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Web;

namespace WCF.Composite
{
    #region Object Request

    [DataContract]
    public class TeacherRequest
    {
        /// <summary>
        /// danh sách ID Giao vien
        /// </summary>
        [DataMember]
        public List<int> lstTeacherID { get; set; }

        /// <summary>
        /// ID trường
        /// </summary>
        [DataMember]
        public int SchoolID { get; set; }
    }

    [DataContract]
    public class EmployeeRequest
    {
        /// <summary>
        /// list ID nguoi nhan tin nhan
        /// </summary>
        [DataMember]
        public int EmployeeID { get; set; }
        /// <summary>
        /// list ID Don vi nhan tin nhan
        /// </summary>
        [DataMember]
        public int SupervisingDeptID { get; set; }
    }

    [DataContract]
    public class TeacherOfSchoolRequest
    {

        /// <summary>
        /// ID năm học hiện tại
        /// </summary>
        [DataMember]
        public int AcademicYearID { get; set; }

        /// <summary>
        /// ID trường
        /// </summary>
        [DataMember]
        public int SchoolID { get; set; }
        [DataMember]
        public bool? IsStatus { get; set; }
    }


    [DataContract]
    public class HeadTeacher
    {
        [DataMember]
        public int TeacherID { get; set; }
        [DataMember]
        public int ClassID { get; set; }
    }
    #endregion

    #region Object Response

    [DataContract]
    public class AccountByTeacherID
    {
        /// <summary>
        /// ID Nhân viên, giáo viên
        /// </summary>
        [DataMember]
        public int EmployeeID { get; set; }

        /// <summary>
        /// Tên đầy đủ
        /// </summary>
        [DataMember]
        public string Username { get; set; }
    }

    [DataContract]
    public class ListAccountByTeacherResponse
    {
        private List<AccountByTeacherID> lstAccountByTeacher;
        /// <summary>
        /// ma loi tra ve
        /// </summary>
        [DataMember]
        public int ValidationCode { get; set; }
        /// <summary>
        /// Object dinh kem
        /// </summary>
        [DataMember]
        public List<AccountByTeacherID> LstAccountByTeacher
        {
            get
            {
                return lstAccountByTeacher;
            }
            set
            {
                //Check null gan de thuc hien new item
                if (value == null)
                {
                    this.lstAccountByTeacher = new List<AccountByTeacherID>();
                }
                else
                {
                    this.lstAccountByTeacher = value;
                }
            }
        }
    }

    [DataContract]
    public class EmployeeLite
    {
        /// <summary>
        /// ID Nhân viên, giáo viên
        /// </summary>
        [DataMember]
        public int EmployeeID { get; set; }

        /// <summary>
        /// Tên đầy đủ
        /// </summary>
        [DataMember]
        public string FullName { get; set; }

        /// <summary>
        /// Ma giao vien
        /// </summary>
        [DataMember]
        public string EmployeeCode { get; set; }

        /// <summary>
        /// Số điện thoại
        /// </summary>
        [DataMember]
        public string Mobile { get; set; }

        /// <summary>
        /// ID môn học phụ trách
        /// </summary>
        [DataMember]
        public int SubjectID { get; set; }

        /// <summary>
        /// Tên môn học phụ trách
        /// </summary>
        [DataMember]
        public string SubjectName { get; set; }
		
		[DataMember]
        public string ShortName { get; set; }

        /// <summary>
        /// Lop chu nhiem neu co
        /// </summary>
        [DataMember]
        public int ClassID { get; set; }

    }

    /// <summary>
    /// Object co thuoc tinh bao gom danh sach EmployeeLite
    /// </summary>
    [DataContract]
    public class ListEmployeeLiteResponse
    {
        private List<EmployeeLite> lstEmployeeLite;
        /// <summary>
        /// ma loi tra ve
        /// </summary>
        [DataMember]
        public int ValidationCode { get; set; }
        /// <summary>
        /// Object dinh kem
        /// </summary>
        [DataMember]
        public List<EmployeeLite> LstEmployeeLite
        {
            get
            {
                return lstEmployeeLite;
            }
            set
            {
                //Check null gan de thuc hien new item
                if (value == null)
                {
                    this.lstEmployeeLite = new List<EmployeeLite>();
                }
                else
                {
                    this.lstEmployeeLite = value;
                }
            }
        }
    }

    [DataContract]
    public class EmployeeFullInfo
    {
        /// <summary>
        /// Bí danh
        /// </summary>
        [DataMember]
        public string Alias { get; set; }

        /// <summary>
        /// Cấp dạy
        /// </summary>
        [DataMember]
        public int AppliedLevel { get; set; }

        /// <summary>
        /// Ngày sinh
        /// </summary>
        [DataMember]
        public DateTime BirthDate { get; set; }

        /// <summary>
        /// Nơi sinh
        /// </summary>
        [DataMember]
        public string BirthPlace { get; set; }

        /// <summary>
        /// Ngày tạo
        /// </summary>
        [DataMember]
        public DateTime CreatedDate { get; set; }

        /// <summary>
        /// Email
        /// </summary>
        [DataMember]
        public string Email { get; set; }

        /// <summary>
        /// Mã nhân viên
        /// </summary>
        [DataMember]
        public string EmployeeCode { get; set; }

        /// <summary>
        /// ID nhân viên
        /// </summary>
        [DataMember]
        public int EmployeeID { get; set; }

        /// <summary>
        /// Loại nhân viên (Giáo viên, Nhân viên phòng sở)
        /// </summary>
        [DataMember]
        public int EmployeeType { get; set; }

        /// <summary>
        /// Trạng thái
        /// </summary>
        [DataMember]
        public int EmploymentStatus { get; set; }

        /// <summary>
        /// Tên đầy đủ
        /// </summary>
        [DataMember]
        public string FullName { get; set; }

        /// <summary>
        /// Giới tính
        /// </summary>
        [DataMember]
        public bool Genre { get; set; }

        /// <summary>
        /// Ngày vào trường
        /// </summary>
        [DataMember]
        public DateTime JoinedDate { get; set; }

        /// <summary>
        /// Điện thoại bàn
        /// </summary>
        [DataMember]
        public string Mobile { get; set; }

        /// <summary>
        /// Tên viết tắt (Dùng để sort)
        /// </summary>
        [DataMember]
        public string Name { get; set; }

        /// <summary>
        /// Ghi chú
        /// </summary>
        [DataMember]
        public string Note { get; set; }

        /// <summary>
        /// Địa chỉ thường trú
        /// </summary>
        [DataMember]
        public string PermanentResidentalAddress { get; set; }

        /// <summary>
        /// 
        /// </summary>
        [DataMember]
        public int SchoolFacultyID { get; set; }

        /// <summary>
        /// 
        /// </summary>
        [DataMember]
        public string SchoolFacltyName { get; set; }

        /// <summary>
        /// ID trường
        /// </summary>
        [DataMember]
        public int SchoolID { get; set; }

        /// <summary>
        /// Chức vụ
        /// </summary>
        [DataMember]
        public int StaffPositionID { get; set; }

        /// <summary>
        /// Ngày vào trường
        /// </summary>
        [DataMember]
        public DateTime StartingDate { get; set; }

        /// <summary>
        /// ID phòng trực thuộc
        /// </summary>
        [DataMember]
        public int SupervisingDeptID { get; set; }

        /// <summary>
        /// Điện thoại bàn
        /// </summary>
        [DataMember]
        public string Telephone { get; set; }
    }
    /// <summary>
    /// Object co thuoc tinh bao gom doi tuong EmployeeFullInfo
    /// </summary>
    [DataContract]
    public class EmployeeFullInfoResponse
    {
        private EmployeeFullInfo objEmployeeFullInfo;
        /// <summary>
        /// ma loi tra ve
        /// </summary>
        [DataMember]
        public int ValidationCode { get; set; }
        /// <summary>
        /// Object dinh kem
        /// </summary>
        [DataMember]
        public EmployeeFullInfo ObjEmployeeFullInfo
        {
            get
            {
                return objEmployeeFullInfo;
            }
            set
            {
                //Check null gan de thuc hien new item
                if (value == null)
                {
                    this.objEmployeeFullInfo = new EmployeeFullInfo();
                }
                else
                {
                    this.objEmployeeFullInfo = value;
                }
            }
        }
    }

    /// <summary>
    /// Object co thuoc tinh bao gom danh sach EmployeeLite
    /// </summary>
    [DataContract]
    public class ListEmployeeFullInfoResponse
    {
        private List<EmployeeFullInfo> lstEmployeeFullInfo;
        /// <summary>
        /// ma loi tra ve
        /// </summary>
        [DataMember]
        public int ValidationCode { get; set; }
        /// <summary>
        /// Object dinh kem
        /// </summary>
        [DataMember]
        public List<EmployeeFullInfo> LstEmployeeFullInfo
        {
            get
            {
                return lstEmployeeFullInfo;
            }
            set
            {
                //Check null gan de thuc hien new item
                if (value == null)
                {
                    this.lstEmployeeFullInfo = new List<EmployeeFullInfo>();
                }
                else
                {
                    this.lstEmployeeFullInfo = value;
                }
            }
        }
    }

    [DataContract]
    public class EmployeeDefault:IEquatable<EmployeeDefault>
    {
        /// <summary>
        /// ID Nhân viên, giáo viên
        /// </summary>
        [DataMember]
        public int EmployeeID { get; set; }

        /// <summary>
        /// Mã nhân viên, giáo viên
        /// </summary>
        [DataMember]
        public string EmployeeCode { get; set; }

        /// <summary>
        /// Tên đầy đủ
        /// </summary>
        [DataMember]
        public string FullName { get; set; }

        /// <summary>
        /// Giới tính
        /// </summary>
        [DataMember]
        public bool Genre { get; set; }

        /// <summary>
        /// Tên viết tắt (Dùng để sort)
        /// </summary>
        [DataMember]
        public string Name { get; set; }
        /// <summary>
        /// Ten tai khoan
        /// </summary>
        [DataMember]
        public string AccountName { get; set; }
        /// <summary>
        /// Trạng thái
        /// </summary>
        [DataMember]
        public int EmploymentStatus { get; set; }
        /// <summary>
        /// Điện thoại
        /// </summary>
        [DataMember]
        public string Mobile { get; set; }
        /// <summary>
        /// Điện thoại bàn
        /// </summary>
        [DataMember]
        public string Telephone { get; set; }

        /// <summary>
        /// EmployeeType
        /// </summary>
        [DataMember]
        public int EmployeeType { get; set; }

        /// <summary>
        /// ID To bo mon
        /// </summary>
        [DataMember]
        public int FacultyID { get; set; }

        /// <summary>
        /// Ten to bo mon
        /// </summary>
        [DataMember]
        public string FacultyName { get; set; }

        /// <summary>
        /// Doi tuong
        /// </summary>
        [DataMember]
        public int WorkGroupTypeID { get; set; }

        /// <summary>
        /// Ten Doi tuong
        /// </summary>
        [DataMember]
        public string WorkGroupTypeName { get; set; }

        /// <summary>
        /// Cong viec
        /// </summary>
        [DataMember]
        public int WorkTypeID { get; set; }

        /// <summary>
        /// Ten cong viec
        /// </summary>
        [DataMember]
        public string WorkTypeName { get; set; }

        /// <summary>
        /// Trang thai
        /// </summary>
        [DataMember]
        public bool IsActive { get; set; }

        [DataMember]
        public bool IsHeadTeacher { get; set; }

        [DataMember]
        public bool IsSubjectTeacher { get; set; }

        [DataMember]
        public bool IsYouthLeageMember { get; set; }

        [DataMember]
        public bool IsCommunistPartyMember { get; set; }

        [DataMember]
        public bool IsSyndicate { get; set; }

        [DataMember]
        public string TeacherSubject { get; set; }

        public bool Equals(EmployeeDefault other)
        {
            if (EmployeeID == other.EmployeeID)
                return true;

            return false;
        }

        public override int GetHashCode()
        {
            return EmployeeID.GetHashCode();
        }
    }
    /// <summary>
    /// Object co thuoc tinh bao gom danh sach Employee
    /// </summary>
    [DataContract]
    public class ListEmployeeResponse
    {
        private List<EmployeeDefault> lstEmployee;

        /// <summary>
        /// Dạng chuỗi
        /// </summary>
        /// <modifier>AnhVD</modifier>
        private List<string> lstEmployeeStr;
        /// <summary>
        /// ma loi tra ve
        /// </summary>
        [DataMember]
        public int ValidationCode { get; set; }

        /// <summary>
        /// Object dinh kem
        /// </summary>
        [DataMember]
        public List<EmployeeDefault> LstEmployee
        {
            get
            {
                return lstEmployee;
            }
            set
            {
                //Check null gan de thuc hien new item
                if (value == null)
                {
                    this.lstEmployee = new List<EmployeeDefault>();
                }
                else
                {
                    this.lstEmployee = value;
                }
            }
        }

        [DataMember]
        public List<String> LstEmployeeStr
        {
            get
            {
                return lstEmployeeStr;
            }
            set
            {
                if (value == null)
                {
                    this.lstEmployeeStr = new List<string>();
                }
                else
                {
                    this.lstEmployeeStr = value;
                }
            }
        }
    }


    [DataContract]
    public class EmployeeSupervisingDept
    {
        /// <summary>
        /// ID Nhân viên, giáo viên
        /// </summary>
        [DataMember]
        public int EmployeeID { get; set; }

        /// <summary>
        /// Tên nhân viên
        /// </summary>
        [DataMember]
        public string EmployeeName { get; set; }

        [DataMember]
        public int SupervisingDeptID { get; set; }

        /// <summary>
        /// Tên phòng ban
        /// </summary>
        [DataMember]
        public string SupervisingDeptName { get; set; }

        /// <summary>
        /// id phong ban la don vi cha gan nhat
        /// </summary>
        [DataMember]
        public int ParentSupervisingDeptID { get; set; }

        /// <summary>
        /// ten don vi phong ban cha gan nhat
        /// </summary>
        [DataMember]
        public string ParentSupervisingDeptName { get; set; }

        /// <summary>
        /// Mã cán bộ
        /// </summary>
        [DataMember]
        public string EmployeeCode { get; set; }

        //Số ĐT
        [DataMember]
        public string EmployeeMobile { get; set; }
    }
    /// <summary>
    /// Object co thuoc tinh bao gom danh sach Employee
    /// </summary>
    [DataContract]
    public class ListEmployeeSupervisingDeptReponse
    {
        private List<EmployeeSupervisingDept> lstEmployeeSupervisingDept;
        /// <summary>
        /// ma loi tra ve
        /// </summary>
        [DataMember]
        public int ValidationCode { get; set; }
        /// <summary>
        /// Object dinh kem
        /// </summary>
        [DataMember]
        public List<EmployeeSupervisingDept> LstEmployeeSupervisingDept
        {
            get
            {
                return lstEmployeeSupervisingDept;
            }
            set
            {
                //Check null gan de thuc hien new item
                if (value == null)
                {
                    this.lstEmployeeSupervisingDept = new List<EmployeeSupervisingDept>();
                }
                else
                {
                    this.lstEmployeeSupervisingDept = value;
                }
            }
        }
    }

    /// <summary>
    /// Object co thuoc tinh bao gom danh sach Employee
    /// </summary>
    [DataContract]
    public class ListEmployeeSupervisingDeptPaginateReponse
    {
        private List<EmployeeSupervisingDept> lstEmployeeSupervisingDept;

        /// <summary>
        /// ma loi tra ve
        /// </summary>
        [DataMember]
        public int ValidationCode { get; set; }

        /// <summary>
        /// Tong so ban ghi
        /// </summary>
        [DataMember]
        public int TotalRecord { get; set; }

        /// <summary>
        /// Object dinh kem
        /// </summary>
        [DataMember]
        public List<EmployeeSupervisingDept> LstEmployeeSupervisingDept
        {
            get
            {
                return lstEmployeeSupervisingDept;
            }
            set
            {
                //Check null gan de thuc hien new item
                if (value == null)
                {
                    this.lstEmployeeSupervisingDept = new List<EmployeeSupervisingDept>();
                }
                else
                {
                    this.lstEmployeeSupervisingDept = value;
                }
            }
        }
    }

    [DataContract]
    public class ListHeadTeacherByClassReponse
    {
        private List<HeadTeacher> listHeadTeacherByClass;
        [DataMember]
        public int ValidationCode { get; set; }
        [DataMember]
        public List<HeadTeacher> LstHeadTeacherByClass
        {
            get
            {
                return listHeadTeacherByClass;
            }
            set
            {
                if (value == null)
                {
                    this.listHeadTeacherByClass = new List<HeadTeacher>();
                }
                else
                {
                    this.listHeadTeacherByClass = value;
                }
            }
        }
    }

    [DataContract]
    public class ListWorkGroupTypeResponse
    {
        private List<WorkGroupType> lstWorkGroupType;
        [DataMember]
        public int ValidateCode { get; set; }
        [DataMember]
        public List<WorkGroupType> LstWorkGroupType
        {
            get
            {
                return lstWorkGroupType;
            }
            set
            {
                if (value == null)
                {
                    this.lstWorkGroupType = new List<WorkGroupType>();
                }
                else
                {
                    this.lstWorkGroupType = value;
                }
            }
        }

    }

    [DataContract]
    public class WorkGroupType
    {
        [DataMember]
        public int WorkGroupTypeID { get; set; }

        [DataMember]
        public string Resolution { get; set; }
    }

    #endregion
}