﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Web;

namespace WCF.Composite
{
    [DataContract]
    public class GradeDefault
    {
        /// <summary>
        /// Tên cấp
        /// </summary>
        [DataMember]
        public string GradeName { get; set; }

        /// <summary>
        /// Cấp
        /// </summary>
        [DataMember]
        public int GradeID { get; set; }
    }

    /// <summary>
    /// DataContract gom danh sach thuoc tinh
    /// </summary>
    [DataContract]
    public class GradeResponse
    {
        private List<GradeDefault> lstGrade;
        /// <summary>
        /// ma loi tra ve
        /// </summary>
        [DataMember]
        public int ValidationCode { get; set; }
        /// <summary>
        /// Object dinh kem
        /// </summary>
        [DataMember]
        public List<GradeDefault> LstGrade
        {
            get
            {
                return lstGrade;
            }
            set
            {
                //Check null gan de thuc hien khoi tao 
                if (value == null)
                {
                    this.lstGrade = new List<GradeDefault>();
                }
                else
                {
                    this.lstGrade = value;
                }
            }
        }
    }
}