﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Web;

namespace WCF.Composite
{
    /// <summary>
    /// Class luu kiem du lieu bool co ban tra ve
    /// </summary>
    [DataContract]
    public class BoolResultResponse
    {
        /// <summary>
        /// ma loi tra ve
        /// </summary>
        [DataMember]
        public int ValidationCode { get; set; }
        /// <summary>
        /// Object dinh kem
        /// </summary>
        [DataMember]
        public bool Result { get; set; }

        [DataMember]
        public string Description { get; set; }

    }

    /// <summary>
    /// Class luu kiem du lieu int co ban tra ve
    /// </summary>
    [DataContract]
    public class IntResultResponse
    {
        /// <summary>
        /// ma loi tra ve
        /// </summary>
        [DataMember]
        public int ValidationCode { get; set; }
        /// <summary>
        /// Object dinh kem
        /// </summary>
        [DataMember]
        public int Result { get; set; }
    }

    /// <summary>
    /// Class luu kiem du lieu mang byte co ban tra ve
    /// </summary>
    [DataContract]
    public class ByteArrayResultResponse
    {
        /// <summary>
        /// ma loi tra ve
        /// </summary>
        [DataMember]
        public int ValidationCode { get; set; }
        /// <summary>
        /// Object dinh kem
        /// </summary>
        [DataMember]
        public byte[] Result { get; set; }
    }

    /// <summary>
    /// Class luu kiem du lieu string co ban tra ve
    /// </summary>
    [DataContract]
    public class StringResultResponse
    {
        /// <summary>
        /// ma loi tra ve
        /// </summary>
        [DataMember]
        public int ValidationCode { get; set; }
        /// <summary>
        /// Object dinh kem
        /// </summary>
        [DataMember]
        public string Result { get; set; }
    }

    // <summary>
    /// Class luu kiem du lieu List int
    /// </summary>
    [DataContract]
    public class ListIntResultResponse
    {
        /// <summary>
        /// ma loi tra ve
        /// </summary>
        [DataMember]
        public int ValidationCode { get; set; }
        /// <summary>
        /// Object dinh kem
        /// </summary>
        [DataMember]
        public List<int> Result { get; set; }
    }
}