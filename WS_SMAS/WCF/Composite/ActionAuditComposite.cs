﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Web;

namespace WCF.Composite
{
    [DataContract]
    public class ActionAuditRequest
    {
        [DataMember]
        public string Action { get; set; }
        [DataMember]
        public int ActionAuditID { get; set; }
        [DataMember]
        public DateTime BeginAuditTime { get; set; }
        [DataMember]
        public string Controller { get; set; }
        [DataMember]
        public string Description { get; set; }
        [DataMember]
        public DateTime EndAuditTime { get; set; }
        [DataMember]
        public int? GroupID { get; set; }
        [DataMember]
        public string IP { get; set; }
        [DataMember]
        public int? LastDigitNumberSchool { get; set; } 
        [DataMember]
        public string newObjectValue { get; set; }
        [DataMember]
        public string oldObjectValue { get; set; }
        [DataMember]
        public string Parameter { get; set; }
        [DataMember]
        public string Referer { get; set; }
        [DataMember]
        public int? RoleID { get; set; }
        [DataMember]
        public string User_Agent { get; set; }
        [DataMember]
        public int UserID { get; set; }
        [DataMember]
        public string userAction { get; set; }
        [DataMember]
        public string userDescription { get; set; }
        [DataMember]
        public string userFunction { get; set; }
    }
}