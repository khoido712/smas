﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Web;

namespace WCF.Composite
{
    [Flags]
    [DataContract]
    public enum EPermission
    {
        [EnumMember]
        None = 0,
        [EnumMember]
        View = 1,
        [EnumMember]
        Create = 2,
        [EnumMember]
        Edit = 4,
        [EnumMember]
        Delete = 8
    }
}