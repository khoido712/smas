﻿using System;
using System.Collections.Generic;
using System.Runtime.Serialization;

namespace WCF.Composite.Sync
{
    [DataContract]
    public class TransSubjectCatDefault
    {
        /// <summary> 
        /// ID môn học
        /// </summary>
        /// 
        [DataMember]
        public int SubjectCatId { get; set; }
        /// <summary> 
        /// Tên môn học
        /// </summary>
        /// 
        [DataMember]
        public string SubjectName { get; set; }
        /// <summary> 
        /// Tên viết tắt của môn học
        /// </summary>
        /// 
        [DataMember]
        public string Abbreviation { get; set; }
        /// <summary> 
        /// Tên hiển thị của môn học
        /// </summary>
        /// 
        [DataMember]
        public string DisplayName { get; set; }
        /// <summary> 
        /// Xác định môn học là môn ngoại ngữ hay không?
        /// </summary>
        /// 
        [DataMember]
        public bool IsForeignLanguage { get; set; }
        /// <summary> 
        /// Xác định môn học là môn nghề hay không (áp dụng cho cấp 2,3)
        /// </summary>
        /// 
        [DataMember]
        public bool IsApprenticeshipSubject { get; set; }
        /// <summary> 
        /// Xác định môn học là môn chấm điểm hay nhận xét?
        /// </summary>
        /// 
        [DataMember]
        public int IsCommenting { get; set; }
        /// <summary> 
        /// Xác định môn học có thể miễn giảm được hay không?
        /// </summary>
        /// 
        [DataMember]
        public bool IsExemptible { get; set; }
        /// <summary> 
        /// Xác định môn học là môn có thực hành hay không?
        /// </summary>
        /// 
        [DataMember]
        public bool HasPractice { get; set; }
        /// <summary> 
        /// Có thể sửa kiểu môn
        /// </summary>
        /// 
        [DataMember]
        public bool IsEditIsCommentting { get; set; }
        /// <summary> 
        /// Mô tả về môn học
        /// </summary>
        /// 

        [DataMember]
        public string Description { get; set; }
        /// <summary> 
        /// Nhóm môn nghề;
        /// </summary>

        [DataMember]
        public int? ApprenticeshipGroupID { get; set; }
        /// <summary> 
        /// Cấp học áp dụng:
        /// </summary>
        [DataMember]
        public int? AppliedLevel { get; set; }


        [DataMember]
        public int? SynchronizeID { get; set; }

        [DataMember]
        public DateTime? ModifiedDate { get; set; }
    }

    [DataContract]
    public class TransSubjectCatResponse
    {
        [DataMember]
        public List<TransSubjectCatDefault> ListSubjectCat { get; set; }

        /// <summary>
        /// Tra ve thanh cong hay that bai
        /// </summary>
        [DataMember]
        public bool Susscess { get; set; }
        /// <summary>
        /// Ma loi tra ve
        /// </summary>
        [DataMember]
        public int ValidationCode { get; set; }

        [DataMember]
        public string MessageReponse { get; set; }
    }
}