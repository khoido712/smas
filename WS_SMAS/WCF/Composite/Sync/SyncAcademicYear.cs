﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Web;

namespace WCF.Composite.Sync
{
    [DataContract]
    public class SyncAcademicYear
    {
        [DataMember]
        public int AcademicYearId { get; set; }
        [DataMember]
        public int SchoolId { get; set; }
        [DataMember]
        public int EducationGradeId { get; set; }
        /// <summary>
        /// so ban ghi moi lan dong bo
        /// </summary>
        [DataMember]
        public int RecordNumber { get; set; }


        [DataMember]
        public int ProvinceId { get; set; }
        [DataMember]
        public int Year { get; set; }
        /// <summary>
        /// Thoi gian thuc hien update du lieu(tinh theo ngay): Ngay hien tai- UpdateTime
        /// </summary>
        /// 
        [DataMember]
        public int UpdateTime { get; set; }

        [DataMember]
        public DateTime ModifyDate
        {
            get { return DateTime.Now.AddDays(-UpdateTime); }
            set { }
        }

        [DataMember]
        /// <summary>
        /// Danh sach cap truong duoc dong bo
        /// </summary>
        public List<int> LstEducationGradeId { get; set; }

        [DataMember]
        public string SourceDB { get; set; }

        public List<string> ListModifyDate
        {
            get
            {
                List<string> lstVal = new List<string>();
                string valDate = "";
                for (int i = 0; i < UpdateTime; i++)
                {
                    valDate = DateTime.Now.AddDays(-i).ToString("dd/MM/yyyy");
                    lstVal.Add(valDate);
                }
                return lstVal;
            }
        }

        
    }

    [DataContract]
    public class TransAcademicYearResponse
    {
        [DataMember]
        public List<SyncAcademicYear> ListAcademicYear { get; set; }

        /// <summary>
        /// Tra ve thanh cong hay that bai
        /// </summary>
        [DataMember]
        public bool Susscess { get; set; }
        /// <summary>
        /// Ma loi tra ve
        /// </summary>
        [DataMember]
        public int ValidationCode { get; set; }

        [DataMember]
        public string Message { get; set; }
    }
}