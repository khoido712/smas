﻿using System;
using System.Collections.Generic;
using System.Runtime.Serialization;

namespace WCF.Composite.Sync
{
    [DataContract]
    public class TransSummedEvaluationDefault
    {
        /// <summary> 
        /// Danh gia cuoi ky
        /// </summary>
        [DataMember]
        public long SummedEvaluationId { get; set; }

        [DataMember]
        /// <summary> 
        /// ID học sinh
        /// </summary>
        public int PupilId { get; set; }

        [DataMember]
        /// <summary> 
        /// Học kỳ:
        /// </summary>
        public int Semester { get; set; }

        [DataMember]
        /// <summary> 
        /// ID môn học, hoặc năng lực, phẩm chất
        /// </summary>
        public int EvaluationCriteriaId { get; set; }

        [DataMember]
        /// <summary> 
        /// Tiêu chí đánh giá: Xem sheet 2
        /// </summary>
        public int EvaluationId { get; set; }

        [DataMember]
        /// <summary> 
        /// Điểm kiểm tra cuối kỳ
        /// </summary>
        public int? PeriodicEndingMark { get; set; }

        [DataMember]
        /// <summary> 
        /// Đánh giá cuối kỳ môn học: HT, CHT
        /// </summary>
        public string EndingEvaluation { get; set; }

        [DataMember]
        /// <summary> 
        /// Nhận xét cuối kỳ
        /// </summary>
        public string EndingComment { get; set; }

        [DataMember]
        /// <summary> 
        /// Điểm thi lại
        /// </summary>
        public int? RetestMark { get; set; }

        [DataMember]
        /// <summary> 
        /// Đánh giá rèn luyện lại: Đ, CĐ
        /// </summary>
        public string EvaluationRetraining { get; set; }

        [DataMember]
        public int Year { get; set; }

        [DataMember]
        public int? SynchronizeID { get; set; }

        [DataMember]
        public DateTime? UpdateTime { get; set; }


    }

    [DataContract]
    public class TransSummedEvaluationResponse
    {
        [DataMember]
        public List<TransSummedEvaluationDefault> ListSummedEvaluation { get; set; }

        /// <summary>
        /// Tra ve thanh cong hay that bai
        /// </summary>
        [DataMember]
        public bool Susscess { get; set; }
        /// <summary>
        /// Ma loi tra ve
        /// </summary>
        [DataMember]
        public int ValidationCode { get; set; }

        [DataMember]
        public string Message { get; set; }
    }
}
