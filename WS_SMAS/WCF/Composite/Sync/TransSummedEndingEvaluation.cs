﻿using System;
using System.Collections.Generic;
using System.Runtime.Serialization;

namespace WCF.Composite.Sync
{
    [DataContract]
    public class TransSummedEndingEvaluationDefault
    {
        [DataMember]
        /// <summary> 
        /// Danh gia cuoi ky
        /// </summary>
        public long SummedEndingEvaluationId { get; set; }

        [DataMember]
        /// <summary> 
        /// ID học sinh
        /// </summary>
        public int PupilId { get; set; }

        [DataMember]
        /// <summary> 
        /// Học kỳ:
        /// </summary>
        public int Semester { get; set; }

        [DataMember]

        /// <summary> 
        /// Tiêu chí đánh giá: Xem sheet 2
        /// </summary>
        public int EvaluationId { get; set; }

        [DataMember]
        /// <summary> 
        /// Đánh giá cuối kỳ: HT, CHT
        /// </summary>
        public string EndingEvaluation { get; set; }

        [DataMember]
        /// <summary> 
        /// Đánh giá rèn luyện lại: Đ, CĐ
        /// </summary>
        public string EvaluationRetraining { get; set; }

        [DataMember]
        /// <summary> 
        /// Đánh giá bổ sung: HT, CHT
        /// </summary>
        public string RateAdd { get; set; }

        [DataMember]
        public int Year { get; set; }


        [DataMember]
        public int? SynchronizeID { get; set; }

        [DataMember]
        public DateTime? UpdateTime { get; set; }

    }

    [DataContract]
    public class TransSummedEndingEvaluationResponse
    {
        [DataMember]
        public List<TransSummedEndingEvaluationDefault> ListSummedEnding { get; set; }

        /// <summary>
        /// Tra ve thanh cong hay that bai
        /// </summary>
        [DataMember]
        public bool Susscess { get; set; }
        /// <summary>
        /// Ma loi tra ve
        /// </summary>
        [DataMember]
        public int ValidationCode { get; set; }

        [DataMember]
        public string Message { get; set; }
    }
}
