﻿using System;
using System.Collections.Generic;
using System.Runtime.Serialization;

namespace WCF.Composite.Sync
{
    [DataContract]
    public class TransSummedUpRecordDefault
    {
        [DataMember]
        /// <summary> 
        /// ID tổng kết môn học
        /// </summary>
        public long SummedUpRecordId { get; set; }
        [DataMember]
        /// <summary> 
        /// ID Học sinh
        /// </summary>
        public int PupilId { get; set; }
        [DataMember]
        /// <summary> 
        /// ID Môn học nhận xét
        /// </summary>
        public int SubjectId { get; set; }
        [DataMember]
        /// <summary> 
        /// Xác định môn học là môn chấm điểm hay nhận xét
        /// </summary>
        public int IsCommenting { get; set; }
        [DataMember]

        /// <summary> 
        /// Học kỳ:
        /// </summary>
        public int Semester { get; set; }
        [DataMember]

        /// <summary> 
        /// - Nếu IsCommenting == 0: Điểm trung bình môn
        /// </summary>
        public decimal? SummedUpMark { get; set; }
        [DataMember]


        /// <summary> 
        /// - Nếu IsCommenting == 0: Không có ý nghĩa
        /// </summary>
        public string JudgementResult { get; set; }
        [DataMember]

        /// <summary> 
        /// Điểm thi lại
        /// </summary>
        public decimal? ReTestMark { get; set; }
        [DataMember]
        /// <summary> 
        /// Kết quả đánh giá thi lại
        /// </summary>
        public string ReTestJudgement { get; set; }
        [DataMember]

        /// <summary> 
        /// Nhận xét của giáo viên về sự tiến bộ của học sinh hoặc những điểm học sinh cần cố gắng:
        /// </summary>
        public string Comment { get; set; }
        [DataMember]

        public int Year { get; set; }
        
        [DataMember]
        public int? SynchronizeID { get; set; }

        [DataMember]
        public DateTime? UpdateTime { get; set; }


    }


    [DataContract]
    public class TransSummedUpRecordResponse
    {
        [DataMember]
        public List<TransSummedUpRecordDefault> ListSumedUpRecord { get; set; }

        /// <summary>
        /// Tra ve thanh cong hay that bai
        /// </summary>
        [DataMember]
        public bool Susscess { get; set; }
        /// <summary>
        /// Ma loi tra ve
        /// </summary>
        [DataMember]
        public int ValidationCode { get; set; }

        [DataMember]
        public string Message { get; set; }
    }
}
