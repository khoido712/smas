﻿using System;
using System.Collections.Generic;
using System.Runtime.Serialization;

namespace WCF.Composite.Sync
{
    [DataContract]
    public class TransPupilOfClassDefault
    {
        [DataMember]
        /// <summary> 
        /// ID phân bổ học sinh
        /// </summary>
        public int PupilOfClassId { get; set; }

        [DataMember]
        /// <summary> 
        /// Học sinh: PupilProfile ==> PupilProfileID
        /// </summary>
        public int PupilId { get; set; }

        [DataMember]
        /// <summary> 
        /// Lớp học: ClassProfile ==> ClassProfileID
        /// </summary>
        public int ClassId { get; set; }

        [DataMember]
        /// <summary> 
        /// Năm học
        /// </summary>
        public int Year { get; set; }

        [DataMember]
        /// <summary> 
        /// Số thứ tự trong lớp
        /// </summary>
        public int OrderInClass { get; set; }

        [DataMember]
        /// <summary> 
        /// Thông tin bổ sung
        /// </summary>
        public string Description { get; set; }

        [DataMember]
        /// <summary> 
        /// Xác định học sinh có thuộc diện không xếp loại hạnh kiểm hay không (chỉ áp dụng cho các trường GDTX)
        /// </summary>
        public bool NoConductEstimation { get; set; }

        [DataMember]
        /// <summary> 
        /// Trạng thái của học sinh: Đang học: 1,  Đã tốt nghiệp: 2, Đã chuyển trường: 3, Đã thôi học: 4,  Đã chuyển lớp: 5
        /// </summary>
        public int Status { get; set; }
        
        [DataMember]
        public string SourceDB { get; set; }
    }

    [DataContract]
    public class TransPupilOfClassResponse
    {
        [DataMember]
        List<TransPupilOfClassDefault> ListPupilClass { get; set; }

        /// <summary>
        /// Tra ve thanh cong hay that bai
        /// </summary>
        [DataMember]
        public bool Susscess { get; set; }
        /// <summary>
        /// Ma loi tra ve
        /// </summary>
        [DataMember]
        public int ValidationCode { get; set; }

        [DataMember]
        public string Message { get; set; }
    }
}
