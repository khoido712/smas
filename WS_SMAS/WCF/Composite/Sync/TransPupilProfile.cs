﻿using System;
using System.Collections.Generic;
using System.Runtime.Serialization;

namespace WCF.Composite.Sync
{
    [DataContract]
    public class TransPupilProfileDefault
    {
        [DataMember]
        /// <summary> 
        /// ID học sinh
        /// </summary>
        public int PupilProfileId { get; set; }

        [DataMember]
        /// <summary> 
        /// Khu vực
        /// </summary>
        public string Area { get; set; }

        [DataMember]
        /// <summary> 
        /// Tỉnh
        /// </summary>
        public string Province { get; set; }

        [DataMember]
        /// <summary> 
        /// Quận, huyện
        /// </summary>
        public string District { get; set; }

        [DataMember]
        /// <summary> 
        /// Phường / Xã 
        /// </summary>
        public string Commune { get; set; }

        [DataMember]
        /// <summary> 
        /// Dân tộc
        /// </summary>
        public string Ethnic { get; set; }

        [DataMember]
        /// <summary> 
        /// Tôn giáo
        /// </summary>
        public string Religion { get; set; }

        [DataMember]
        /// <summary> 
        /// Đối tượng chính sách
        /// </summary>
        public string PolicyTargetId { get; set; }

        [DataMember]
        /// <summary> 
        /// ID Thành phần gia đình
        /// </summary>
        public int FamilyTypeId { get; set; }

        [DataMember]
        /// <summary> 
        /// ID Đối tượng ưu tiên
        /// </summary>
        public int PriorityTypeId { get; set; }

        [DataMember]
        /// <summary> 
        /// Xếp loại học tốt nghiệp cấp dưới:
        /// </summary>
        public int PreviousGraduationLevel { get; set; }

        [DataMember]
        /// <summary> 
        /// Mã học sinh
        /// </summary>
        public string PupilCode { get; set; }

        [DataMember]
        /// <summary> 
        /// Tên đầy đủ
        /// </summary>
        public string FullName { get; set; }

        [DataMember]
        /// <summary> 
        /// Giới tính:(1): nam,  (0): nữ, (2): không xác định
        /// </summary>
        public int Genre { get; set; }

        [DataMember]
        /// <summary> 
        /// Ngày sinh
        /// </summary>
        public DateTime BirthDate { get; set; }

        [DataMember]
        /// <summary> 
        /// Nơi sinh
        /// </summary>
        public string BirthPlace { get; set; }

        [DataMember]
        /// <summary> 
        /// Quê quán
        /// </summary>
        public string HomeTown { get; set; }

        [DataMember]
        /// <summary> 
        /// Ảnh chân dung (giới hạn kích cỡ ảnh <=8kB)
        /// </summary>
        public byte[] Image { get; set; }

        [DataMember]
        /// <summary> 
        /// Số điện thoại liên lạc (cố định)
        /// </summary>
        public string Telephone { get; set; }

        [DataMember]
        /// <summary> 
        /// Số mobile liên lạc. Sử dụng để gửi tin nhắn
        /// </summary>
        public string Mobile { get; set; }

        [DataMember]
        /// <summary> 
        /// Email liên hệ
        /// </summary>
        public string Email { get; set; }

        [DataMember]
        /// <summary> 
        /// Địa chỉ tạm trú
        /// </summary>
        public string TempResidentalAddress { get; set; }

        [DataMember]
        /// <summary> 
        /// Địa chỉ thường trú
        /// </summary>
        public string PermanentResidentalAddress { get; set; }

        [DataMember]
        /// <summary> 
        /// - 1: Ở nội trú / bán trú
        /// </summary>
        public bool IsResidentIn { get; set; }

        [DataMember]
        /// <summary> 
        /// - 1: Là đối tượng khuyết tật
        /// </summary>
        public bool IsDisabled { get; set; }

        [DataMember]

        /// <summary> 
        /// Loại khuyết tật
        /// </summary>
        public int DisabledTypeId { get; set; }

        [DataMember]
        /// <summary> 
        /// Mức độ khuyết tật
        /// </summary>
        public string DisabledSeverity { get; set; }

        [DataMember]
        /// <summary> 
        /// Nhóm máu:(1): Nhóm A, (2): Nhóm B,(3): Nhóm AB,(4): Nhóm O,(0): Nhóm không xác định
        /// </summary>
        public int BloodType { get; set; }

        [DataMember]

        /// <summary> 
        /// Ngày vào trường
        /// </summary>
        public DateTime EnrolmentDate { get; set; }

        [DataMember]
        /// <summary> 
        /// Hệ học ngoại ngữ: FOREIGN_LANGUAGE_TRAINING,(1): Hệ học 3 năm,(2): Hệ học 7 năm,(0): Không xác định
        /// </summary>
        public int ForeignLanguageTraining { get; set; }

        [DataMember]

        /// <summary> 
        /// 1: Có là đội viên
        /// </summary>
        public bool IsYoungPioneerMember { get; set; }


        [DataMember]

        /// <summary> 
        /// Ngày vào Đội
        /// </summary>
        public DateTime? YoungPioneerJoinedDate { get; set; }

        [DataMember]
        /// <summary> 
        /// Nơi vào Đội
        /// </summary>
        public string YoungPioneerJoinedPlace { get; set; }

        [DataMember]
        /// <summary> 
        /// 1: Có là đoàn viên
        /// </summary>
        public bool IsYouthLeageMember { get; set; }

        [DataMember]

        /// <summary> 
        /// Ngày vào Đoàn
        /// </summary>
        public DateTime? YouthLeagueJoinedDate { get; set; }

        [DataMember]
        /// <summary> 
        /// Nơi vào Đoàn
        /// </summary>
        public string YouthLeagueJoinedPlace { get; set; }

        [DataMember]
        /// <summary> 
        /// 1: Có là đảng viên
        /// </summary>
        public bool IsCommunistPartyMember { get; set; }

        [DataMember]

        /// <summary> 
        /// Ngày vào Đảng
        /// </summary>
        public DateTime? CommunistPartyJoinedDate { get; set; }

        [DataMember]
        /// <summary> 
        /// Nơi vào Đảng
        /// </summary>
        public string CommunistPartyJoinedPlace { get; set; }

        [DataMember]
        /// <summary> 
        /// Tên đầy đủ của cha
        /// </summary>
        public string FatherFullName { get; set; }

        [DataMember]
        /// <summary> 
        /// Ngày sinh của cha
        /// </summary>
        public DateTime? FatherBirthDate { get; set; }

        [DataMember]
        /// <summary> 
        /// Nghề nghiệp của cha
        /// </summary>
        public string FatherJob { get; set; }

        [DataMember]
        /// <summary> 
        /// Số mobile của cha
        /// </summary>
        public string FatherMobile { get; set; }

        [DataMember]
        /// <summary> 
        /// Tên đầy đủ của mẹ
        /// </summary>
        public string MotherFullName { get; set; }
        [DataMember]
        /// <summary> 
        /// Ngày sinh của mẹ
        /// </summary>
        public DateTime? MotherBirthDate { get; set; }

        [DataMember]
        /// <summary> 
        /// Nghề nghiệp của mẹ
        /// </summary>
        public string MotherJob { get; set; }

        [DataMember]
        /// <summary> 
        /// Số mobile của mẹ
        /// </summary>
        public string MotherMobile { get; set; }

        [DataMember]
        /// <summary> 
        /// Tên đầy đủ của người đỡ đầu
        /// </summary>
        public string SponsorFullName { get; set; }

        [DataMember]
        /// <summary> 
        /// Ngày sinh của người đỡ đầu
        /// </summary>
        public DateTime? SponsorBirthDate { get; set; }

        [DataMember]
        /// <summary> 
        /// Nghề nghiệp của người đỡ đầu
        /// </summary>
        public String SponsorJob { get; set; }

        [DataMember]
        /// <summary> 
        /// Số mobile của người đỡ đầu
        /// </summary>
        public string SponsorMobile { get; set; }


        [DataMember]
        /// <summary> 
        /// Trạng thái hồ sơ học sinh:(1): Đang học,(2): Đã tốt nghiệp,(3): Đã chuyển trường,(4): Đã nghỉ học,(5): Đã chuyển lớp,(0): Không xác định
        /// </summary>
        public int ProfileStatus { get; set; }

        [DataMember]
        /// <summary> 
        /// Tên trích xuất từ fullname. Sử dụng để tăng tốc khi sắp xếp
        /// </summary>
        public string Name { get; set; }

        [DataMember]
        /// <summary> 
        /// Chế độ chính sách: xem sheet 2
        /// </summary>
        public int PolicyRegimeId { get; set; }

        [DataMember]
        /// <summary> 
        /// Hỗ trợ chi phí học tập; 1 có, 0 Không
        /// </summary>
        public bool IsSupportForLearning { get; set; }

       
        [DataMember]
        public int? SynchronizeID { get; set; }

        [DataMember]
        public DateTime? ModifiedDate { get; set; }

        [DataMember]
        public bool IsActive { get; set; }

        [DataMember]
        public string SourceDB { get; set; }

    }

    [DataContract]
    public class TransPupilProfileResponse
    {
        [DataMember]
        public List<TransPupilProfileDefault> ListPupilfile { get; set; }

        [DataMember]
        public List<TransPupilOfClassDefault> ListPupilOfClass { get; set; }

        /// <summary>
        /// Tra ve thanh cong hay that bai
        /// </summary>
        [DataMember]
        public bool Susscess { get; set; }
        /// <summary>
        /// Ma loi tra ve
        /// </summary>
        [DataMember]
        public int ValidationCode { get; set; }

        [DataMember]
        public string Message { get; set; }
    }
}
