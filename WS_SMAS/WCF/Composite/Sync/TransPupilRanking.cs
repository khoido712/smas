﻿using System;
using System.Collections.Generic;
using System.Runtime.Serialization;

namespace WCF.Composite.Sync
{
    [DataContract]
    public class TransPupilRankingDefault
    {
        [DataMember]
        /// <summary> 
        /// ID xếp loại
        /// </summary>
        public long PupilRankingId { get; set; }

        [DataMember]
        /// <summary> 
        /// Học sinh:
        /// </summary>
        public int PupilId { get; set; }

        [DataMember]
        /// <summary> 
        /// Khối cấp học: xem sheet 2
        /// </summary>
        public int EducationLevelId { get; set; }

        [DataMember]
        /// <summary> 
        /// Học kỳ:
        /// </summary>
        public int? Semester { get; set; }

        [DataMember]
        /// <summary> 
        /// Điểm trung bình các môn trong giai đoan / học kỳ / năm học
        /// </summary>
        public decimal? AverageMark { get; set; }

        [DataMember]
        /// <summary> 
        /// Số ngày nghỉ có phép trong đợt (giai đoạn) / học kỳ / năm học
        /// </summary>
        public int? TotalAbsentDaysWithPermission { get; set; }

        [DataMember]
        /// <summary> 
        /// Số ngày nghỉ không phép trong đợt (giai đoạn) / học kỳ / năm học
        /// </summary>
        public int? TotalAbsentDaysWithoutPermission { get; set; }

        [DataMember]
        /// <summary> 
        /// Xếp loại học lực: xem sheet 2
        /// </summary>
        public int? CapacityLevelId { get; set; }

        [DataMember]
        /// <summary> 
        /// Xếp loại hạnh kiểm: Xem sheet 2
        /// </summary>
        public int? ConductLevelId { get; set; }

        [DataMember]
        /// <summary> 
        /// Thứ tự xếp hạng trong lớp
        /// </summary>
        public int? Rank { get; set; }

        [DataMember]
        /// <summary> 
        /// Quyết định học sinh thuộc diện
        /// 1: Lên lớp
        /// 2: Thi lại
        /// 3: Rèn luyện lại
        /// 4: Ở lại lớp
        /// </summary>
        public int? StudyingJudgementId { get; set; }

        [DataMember]

        /// <summary> 
        /// Đã được xếp loại học sinh hay chưa?
        /// </summary>
        public bool? IsCategory { get; set; }

        [DataMember]
        /// <summary> 
        /// Danh hiệu thi đua: Xem sheet 2
        /// </summary>
        public int? HonourAchivementTypeId { get; set; }

        [DataMember]
        public int? Year { get; set; }


        [DataMember]
        public int? SynchronizeID { get; set; }

        [DataMember]
        public DateTime? RankingDate { get; set; }

        [DataMember]
        public string SourceDB { get; set; }

    }

    [DataContract]
    public class TransPupilRankingResponse
    {
        [DataMember]
        public List<TransPupilRankingDefault> ListRanking { get; set; }

        /// <summary>
        /// Tra ve thanh cong hay that bai
        /// </summary>
        [DataMember]
        public bool Susscess { get; set; }
        /// <summary>
        /// Ma loi tra ve
        /// </summary>
        [DataMember]
        public int ValidationCode { get; set; }

        [DataMember]
        public string Message { get; set; }
    }
}
