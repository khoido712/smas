﻿using System.Collections.Generic;
using System.Runtime.Serialization;

namespace WCF.Composite.Sync
{
    [DataContract]
    public class TransClassSubjectDefault
    {
        [DataMember]
        /// <summary> 
        /// ID thiết lập môn học cho lớp
        /// </summary>
        public long ClassSubjectId { get; set; }
        /// <summary> 
        /// Lớp học
        /// </summary>
        /// 
        [DataMember]
        public int ClassId { get; set; }
        /// <summary> 
        /// Môn học
        /// </summary>
        /// 
        [DataMember]
        public int SubjectId { get; set; }
        /// <summary> 
        /// Xác định có phải là môn chuyên không ? - 0,NULL: Không phải môn chuyên, 1: Là môn chuyên
        /// </summary>
        /// 
        [DataMember]
        public bool IsSpecializedSubject { get; set; }

        /// <summary> 
        /// Số tiết học / tuần học kỳ 1
        /// </summary>
        /// 
        [DataMember]
        public decimal SectionPerWeekFirstSemester { get; set; }
        /// <summary> 
        /// Số tiết học / tuần học kỳ 2
        /// </summary>
        /// 
        [DataMember]
        public decimal SectionPerWeekSecondSemester { get; set; }
        /// <summary> 
        /// Xác định môn học là môn chấm điểm hay nhận xét ?0: Môn chấm điểm, 1: Môn nhận xét, 2: Tính điểm kết hợp nhận xét
        /// </summary>
        /// 
        [DataMember]
        public int IsCommenting { get; set; }

        /// <summary> 
        /// Có phải môn ngoại ngữ 2 không:
        /// </summary>
        /// 
        [DataMember]
        public bool IsSecondForeignLanguage { get; set; }

        /// <summary> 
        /// Hệ số điểm kì 1
        /// </summary>
        /// 
        [DataMember]
        public int FirstSemesterCoefficient { get; set; }
        /// <summary> 
        /// Hệ số điểm kì 2
        /// </summary>
        /// 
        [DataMember]
        public int SecondSemesterCoefficient { get; set; }

        [DataMember]
        public int? SynchronizeID { get; set; }

        [DataMember]
        public string MSourceDB { get; set; }

    }

    [DataContract]
    public class TransClassSubjectResponse
    {
        [DataMember]
        public List<TransClassSubjectDefault> ListSubjectClass { get; set; }

        /// <summary>
        /// Tra ve thanh cong hay that bai
        /// </summary>
        [DataMember]
        public bool Susscess { get; set; }
        /// <summary>
        /// Ma loi tra ve
        /// </summary>
        [DataMember]
        public int ValidationCode { get; set; }

        [DataMember]
        public string Message { get; set; }
    }


}
