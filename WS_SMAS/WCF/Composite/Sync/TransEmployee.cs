﻿using System;
using System.Collections.Generic;
using System.Runtime.Serialization;

namespace WCF.Composite.Sync
{
    [DataContract]
    public class TransEmployeeDefault
    {
        [DataMember]
        /// <summary> 
        /// ID cán bộ
        /// </summary>
        public int EmployeeId { get; set; }
        /// <summary> 
        /// Mã nhân viên / giáo viên. Tham khảo Sinh mã giáo viên / nhân viên tự động
        /// </summary>
        /// 
        [DataMember]
        public string EmployeeCode { get; set; }
        /// <summary> 
        /// Xác định kiểu nhân viên: Tham chiếu từ tham số hệ thống EMPLOYEE_TYPE
        /// </summary>
        /// 
        [DataMember]
        public int EmployeeType { get; set; }
        /// <summary> 
        /// Giáo viên (1)/ Nhân viên / chuyên viên cấp trường (2)/Cán bộ  quản lý(3)
        /// </summary>

        /// <summary> 
        /// Trạng thái làm việc: tham số hệ thống EMPLOYMENT_STATUS: Đang làm việc bình thường (1)/Chuyển công tác khác (2)/ Nghỉ hưu (3)/Tạm nghỉ (4)/Không xác định (0)
        /// </summary>
        /// 
        [DataMember]
        public int EmploymentStatus { get; set; }
        /// <summary> 
        /// SchoolProfile ==> SchoolProfileID
        /// </summary>
        /// 
        [DataMember]
        public int SchoolId { get; set; }

        /// <summary> 
        /// Tên đầy đủ
        /// </summary>
        /// 
        [DataMember]
        public string FullName { get; set; }
        /// <summary> 
        /// Ngày sinh
        /// </summary>
        /// 
        [DataMember]
        public DateTime BirthDate { get; set; }
        /// <summary> 
        /// Nơi sinh
        /// </summary>
        /// 
        [DataMember]
        public string BirthPlace { get; set; }
        /// <summary> 
        /// Số điện thoại liên lạc (điện thoại bàn)
        /// </summary>
        /// 
        [DataMember]
        public string Telephone { get; set; }
        /// <summary> 
        /// Số mobile liên lạc. Sử dụng để gửi tin nhắn
        /// </summary>
        /// 
        [DataMember]
        public string Mobile { get; set; }
        /// <summary> 
        /// Email liên hệ
        /// </summary>
        /// 
        [DataMember]
        public string Email { get; set; }
        /// <summary> 
        /// Địa chỉ tạm trú (chưa sử dụng)
        /// </summary>
        /// 
        [DataMember]
        public string TempResidentalAddress { get; set; }
        /// <summary> 
        /// Địa chỉ thường trú
        /// </summary>
        /// 
        [DataMember]
        public string PermanentResidentalAddress { get; set; }
        /// <summary> 
        /// Giới tính: Tham chiếu từ tham số hệ thống GENRE:(1): nam,(0): nữ,(2): không xác định
        /// </summary>
        /// 
        [DataMember]
        public int Genre { get; set; }


        /// <summary> 
        /// Bí danh
        /// </summary>
        /// 
        [DataMember]
        public string Alias { get; set; }
        /// <summary> 
        /// Tình trạng hôn nhân: tham chiếu thông tin từ tham số hệ thống MARIAGE_STATUS: Độc thân (1)/ Đã kết hôn (2)/Đã li hôn (3)/Góa chồng / vợ (4)/Không xác định (0)
        /// </summary>
        /// 
        [DataMember]
        public int MariageStatus { get; set; }
        /// <summary> 
        /// Tình trạng sức khỏe hiện tại
        /// </summary>
        /// 
        [DataMember]
        public string HealthStatus { get; set; }
        /// <summary> 
        /// Ngày vào biên chế
        /// </summary>
        /// 
        [DataMember]
        public DateTime? JoinedDate { get; set; }
        /// <summary> 
        /// Ngày vào nghề
        /// </summary>
        /// 
        [DataMember]
        public DateTime? StartingDate { get; set; }
        /// <summary> 
        /// Số Chứng minh thư
        /// </summary>
        /// 
        [DataMember]
        public string IdentityNumber { get; set; }
        /// <summary> 
        /// Ngày cấp chứng minh thư
        /// </summary>
        /// 
        [DataMember]
        public DateTime? IdentityIssuedDate { get; set; }
        /// <summary> 
        /// Nơi cấp CMT
        /// </summary>
        /// 
        [DataMember]
        public string IdentityIssuedPlace { get; set; }
        /// <summary> 
        /// Quê quán
        /// </summary>
        /// 
        [DataMember]
        public string HomeTown { get; set; }
        /// <summary> 
        /// Ảnh chân dung (giới hạn kích cỡ ảnh <=1Mb)
        /// </summary>
        /// 
        [DataMember]
        public byte[] Image { get; set; }
        /// <summary> 
        /// Xác định có phải là chuyên trách Đoàn / Đội hay không:
        /// </summary>
        /// 
        [DataMember]
        public bool DedicatedForYoungLeague { get; set; }


        /// <summary> 
        /// Thành phần gia đình: Xem sheet 2
        /// </summary>
        /// 
        [DataMember]
        public int FamilyTypeId { get; set; }
        /// <summary> 
        /// Chức vụ
        /// </summary>
        /// 
        [DataMember]
        public string StaffPosition { get; set; }
        /// <summary> 
        /// Dân tộc
        /// </summary>
        /// 
        [DataMember]
        public string Ethnic { get; set; }
        /// <summary> 
        /// Tôn giáo
        /// </summary>
        /// 
        [DataMember]
        public string Religion { get; set; }
        /// <summary> 
        /// Cấu hình trình độ / cấp bậc đào tạo
        /// </summary>
        /// 
        [DataMember]
        public string GraduationLevel { get; set; }
        /// <summary> 
        /// Hình thức đào tạo: Xem sheet 2
        /// </summary>
        /// 
        [DataMember]
        public int QualificationTypeId { get; set; }
        /// <summary> 
        /// Chuyên ngành đào tạo
        /// </summary>
        /// 
        [DataMember]
        public string SpecialityCat { get; set; }
        /// <summary> 
        /// Trình độ văn hóa
        /// </summary>
        /// 
        [DataMember]
        public string QualificationLevel { get; set; }
        /// <summary> 
        /// Trình độ tin học
        /// </summary>
        /// 
        [DataMember]
        public string ItQualificationLevel { get; set; }
        /// <summary> 
        /// Trình độ ngoại ngữ
        /// </summary>
        /// 
        [DataMember]
        public string ForeignLanguageGrade { get; set; }
        /// <summary> 
        /// Trình độ lý luận chính trị
        /// </summary>
        /// 
        [DataMember]
        public string PoliticalGrade { get; set; }
        /// <summary> 
        /// Trình độ quản lý nhà nước
        /// </summary>
        
        [DataMember]
        public string StateManagementGrade { get; set; }
        /// <summary> 
        /// Trình độ quản lý giáo dục
        /// </summary>
        /// 
        [DataMember]
        public string EducationalManagementGrade { get; set; }
        /// <summary> 
        /// Loại công việc
        /// </summary>
        /// 
        [DataMember]
        public string WorkType { get; set; }
        /// <summary> 
        /// Thuộc tổ bộ môn
        /// </summary>
        /// 
        [DataMember]
        public string SchoolFaculty { get; set; }
        /// <summary> 
        /// Xác định có phải là đoàn viên không ?
        /// </summary>
        /// 
        [DataMember]
        public bool IsYouthLeageMember { get; set; }


        /// <summary> 
        /// Ngày vào Đoàn
        /// </summary>
        /// 
        [DataMember]
        public DateTime? YouthLeagueJoinedDate { get; set; }
        /// <summary> 
        /// Nơi vào Đoàn
        /// </summary>
        /// 
        [DataMember]
        public string YouthLeagueJoinedPlace { get; set; }
        /// <summary> 
        /// Xác định có phải là đảng viên không ?
        /// </summary>
        /// 
        [DataMember]
        public bool IsCommunistPartyMember { get; set; }


        /// <summary> 
        /// Ngày vào Đảng
        /// </summary>
        /// 
        [DataMember]
        public DateTime? CommunistPartyJoinedDate { get; set; }
        /// <summary> 
        /// Nơi vào Đảng
        /// </summary>
        /// 
        [DataMember]
        public string CommunistPartyJoinedPlace { get; set; }
        /// <summary> 
        /// Tên đầy đủ của cha
        /// </summary>
        /// 
        [DataMember]
        public string FatherFullName { get; set; }
        /// <summary> 
        /// Ngày sinh của cha
        /// </summary>
        /// 
        [DataMember]
        public DateTime? FatherBirthDate { get; set; }
        /// <summary> 
        /// Nghề nghiệp của cha
        /// </summary>
        /// 
        [DataMember]
        public string FatherJob { get; set; }
        /// <summary> 
        /// Nơi làm việc hiện tại của cha
        /// </summary>
        /// 
        [DataMember]
        public string FatherWorkingPlace { get; set; }
        /// <summary> 
        /// Tên đầy đủ của mẹ
        /// </summary>
        /// 
        [DataMember]
        public string MotherFullName { get; set; }
        /// <summary> 
        /// Ngày sinh của mẹ
        /// </summary>
        /// 
        [DataMember]
        public DateTime? MotherBirthDate { get; set; }
        /// <summary> 
        /// Nghề nghiệp của mẹ
        /// </summary>
        /// 
        [DataMember]
        public string MotherJob { get; set; }
        /// <summary> 
        /// Nơi làm việc hiện tại của mẹ
        /// </summary>
        /// 
        [DataMember]
        public string MotherWorkingPlace { get; set; }
        /// <summary> 
        /// Tên đầy đủ của vợ / chồng
        /// </summary>
        /// 
        [DataMember]
        public string SpouseFullName { get; set; }
        /// <summary> 
        /// Ngày sinh của vợ / chồng
        /// </summary>
        /// 
        [DataMember]
        public DateTime? SpouseBirthDate { get; set; }
        /// <summary> 
        /// Nghề nghiệp của vợ / chồng
        /// </summary>
        /// 
        [DataMember]
        public string SpouseJob { get; set; }
        /// <summary> 
        /// Nơi làm việc hiện tại của vợ / chồng
        /// </summary>
        /// 
        [DataMember]
        public string SpouseWorkingPlace { get; set; }
        /// <summary> 
        /// Mô tả chức danh
        /// </summary>
        /// 
        [DataMember]
        public string Description { get; set; }
        /// <summary> 
        /// Trích xuất từ fullname. Sử dụng để tăng tốc tìm kiếm lúc sắp xếp theo tên.
        /// </summary>
        /// 
        [DataMember]
        public string Name { get; set; }
        /// <summary> 
        /// Ngày vào trường
        /// </summary>
        /// 
        [DataMember]
        public DateTime? IntoSchoolDate { get; set; }
        /// <summary> 
        /// Cấp dạy chính
        /// </summary>
        /// 
        [DataMember]
        public int AppliedLevel { get; set; }
        /// <summary> 
        /// Loại hợp đồng: xem sheet 2
        /// </summary>
        /// 
        [DataMember]
        public int ContractTypeId { get; set; }
        /// <summary> 
        /// Bồi dưỡng thường xuyên
        /// </summary>
        /// 
        [DataMember]
        public bool RegularRefresher { get; set; }
        /// <summary> 
        /// Trình độ đào tạo
        /// </summary>
        /// 
        [DataMember]
        public string TrainingLevel { get; set; }

        [DataMember]
        public int? SynchronizeID { get; set; }

        [DataMember]
        public DateTime? ModifiedDate { get; set; }


        [DataMember]
        public bool IsActive { get; set; }

        [DataMember]
        public string SourceDB { get; set; }
       

    }

    [DataContract]
    public class TransEmployeeResponse
    {
        [DataMember]
        public List<TransEmployeeDefault> ListEmployees { get; set; }

        /// <summary>
        /// Tra ve thanh cong hay that bai
        /// </summary>
        [DataMember]
        public bool Susscess { get; set; }
        /// <summary>
        /// Ma loi tra ve
        /// </summary>
        [DataMember]
        public int ValidationCode { get; set; }

        [DataMember]
        public string Message{ get; set; }
    }
}
