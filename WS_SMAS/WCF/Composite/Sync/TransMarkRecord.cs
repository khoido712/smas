﻿using System;
using System.Collections.Generic;
using System.Runtime.Serialization;

namespace WCF.Composite.Sync
{
    [DataContract]
    public class TransMarkRecordDefault
    {
        [DataMember]
        /// <summary> 
        /// ID mục hồ sơ điểm
        /// </summary>
        public long MarkRecordId { get; set; }

        [DataMember]
        /// <summary> 
        /// Học sinh
        /// </summary>
        public int PupilId { get; set; }

        [DataMember]
        /// <summary> 
        /// ID Môn học tính điểm
        /// </summary>
        public int SubjectId { get; set; }

        [DataMember]
        /// <summary> 
        /// Loại điểm: Xem sheet 2
        /// </summary>
        public int MarkTypeId { get; set; }

        [DataMember]
        /// <summary> 
        /// Học kỳ: SEMESTER_OF_YEAR
        /// </summary>
        public int Semester { get; set; }

        [DataMember]
        /// <summary> 
        /// Tiêu đề hiển thị con điểm 
        /// </summary>
        public string Title { get; set; }

        [DataMember]
        /// <summary> 
        /// Giá trị điểm
        /// </summary>
        public decimal Mark { get; set; }

        [DataMember]
        /// <summary> 
        /// Thứ tự con điểm trong từng loại điểm
        /// </summary>
        public int OrderNumber { get; set; }

        [DataMember]
        public int Year { get; set; }

        [DataMember]
        public int? SynchronizeID { get; set; }

        [DataMember]
        public DateTime? ModifiedDate { get; set; }

        [DataMember]
        public string SourceDB { get; set; }

    }

    [DataContract]
    public class TransMarkRecordResponse
    {
        [DataMember]
        public List<TransMarkRecordDefault> ListMarkRecord { get; set; }

        [DataMember]
        public List<TransSummedUpRecordDefault> ListSummed { get; set; }
        /// <summary>
        /// Tra ve thanh cong hay that bai
        /// </summary>
        [DataMember]
        public bool Susscess { get; set; }
        /// <summary>
        /// Ma loi tra ve
        /// </summary>
        [DataMember]
        public int ValidationCode { get; set; }

        [DataMember]
        public string Message { get; set; }
    }
}
