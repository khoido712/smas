﻿using System.Collections.Generic;
using System.Runtime.Serialization;

namespace WCF.Composite.Sync
{

    [DataContract]
    public class TransClassProfileDefault
    {
        [DataMember]
        public int ClassProfileId { get; set; }

        [DataMember]
        public int Year { get; set; }

        [DataMember]
        public int SchoolId { get; set; }

        [DataMember]
        public int? SubCommitteeId { get; set; }

        [DataMember]
        public int? HeadTeacherId { get; set; }

        [DataMember]
        public int EducationLevelId { get; set; }

        [DataMember]
        public string DisplayName { get; set; }

        [DataMember]
        public string Description { get; set; }

        [DataMember]
        public bool? IsSpecializedClass { get; set; }

        [DataMember]
        public bool IsCombinedClass { get; set; }


        [DataMember]
        public int? FirstForeignLanguageId { get; set; }


        [DataMember]
        public int? SecondForeignLanguageId { get; set; }


        [DataMember]
        public int? Section { get; set; }

        [DataMember]
        public int? SynchronizeID { get; set; }
        [DataMember]
        public bool? IsActive { get; set; }

        [DataMember]
        public string SourceDB { get; set; }
    }

    [DataContract]
    public class TransClassProfileResponse
    {
        [DataMember]
        public List<TransClassProfileDefault> ListClassProfile { get; set; }

        /// <summary>
        /// Tra ve thanh cong hay that bai
        /// </summary>
        [DataMember]
        public bool Susscess { get; set; }
        /// <summary>
        /// Ma loi tra ve
        /// </summary>
        [DataMember]
        public int ValidationCode { get; set; }

        [DataMember]
        public string Message { get; set; }
    }
}