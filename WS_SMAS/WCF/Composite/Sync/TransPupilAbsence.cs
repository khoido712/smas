﻿using System;
using System.Collections.Generic;
using System.Runtime.Serialization;

namespace WCF.Composite.Sync
{
    [DataContract]
    public class TransPupilAbsenceDefault
    {
        [DataMember]
        /// <summary> 
        /// ID lượt vắng mặt
        /// </summary>
        public long PupilAbsenceId { get; set; }

        [DataMember]
        /// <summary> 
        /// ID Học sinh vắng mặt
        /// </summary>
        public int PupilId { get; set; }

        [DataMember]
        /// <summary> 
        /// ID Khối học: Xem sheet 2
        /// </summary>
        public int EducationLevelId { get; set; }

        [DataMember]
        /// <summary> 
        /// - 1: Buổi sáng
        /// </summary>
        public int Section { get; set; }

        [DataMember]
        /// <summary> 
        /// Ngày điểm danh vắng mặt
        /// </summary>
        public DateTime AbsentDate { get; set; }

        [DataMember]
        /// <summary> 
        /// - 1: Có phép
        /// </summary>
        public bool IsAccepted { get; set; }


        [DataMember]
        public int? SynchronizeID { get; set; }


    }

    [DataContract]
    public class TransPupilAbsenceResponse
    {
        [DataMember]
        public List<TransPupilAbsenceDefault> ListPupilAbsen { get; set; }

        /// <summary>
        /// Tra ve thanh cong hay that bai
        /// </summary>
        [DataMember]
        public bool Susscess { get; set; }
        /// <summary>
        /// Ma loi tra ve
        /// </summary>
        [DataMember]
        public int ValidationCode { get; set; }

        [DataMember]
        public string Message { get; set; }
    }
}
