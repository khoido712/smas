﻿using System;
using System.Collections.Generic;
using System.Runtime.Serialization;

namespace WCF.Composite.Sync
{
    [DataContract]
    public class TransJudgeRecordDefault
    {
        [DataMember]
        /// <summary> 
        /// ID mục hồ sơ kết quả đánh giá
        /// </summary>
        public long JudgeRecordId { get; set; }

        [DataMember]
        /// <summary> 
        /// ID Học sinh
        /// </summary>
        public int PupilId { get; set; }

        [DataMember]
        /// <summary> 
        /// ID Môn học nhận xét
        /// </summary>
        public int SubjectId { get; set; }

        [DataMember]
        /// <summary> 
        /// Loại điểm: Xem sheet 2
        /// </summary>
        public int MarkTypeId { get; set; }

        [DataMember]
        /// <summary> 
        /// Học kỳ: tham số hệ thống SEMESTER_OF_YEAR
        /// </summary>
        public int Semester { get; set; }

        [DataMember]
        /// <summary> 
        /// Kết quả đánh giá thi lần 1
        /// </summary>
        public string Judgement { get; set; }

        [DataMember]
        /// <summary> 
        /// Thứ tự con điểm: bắt đầu từ 1.
        /// </summary>
        public int OrderNumber { get; set; }
        [DataMember]
        /// <summary> 
        /// Lưu tiêu đề con điểm
        /// </summary>
        public string Title { get; set; }

        [DataMember]
        public int Year { get; set; }

        [DataMember]
        public int? SynchronizeID { get; set; }

        [DataMember]
        public DateTime? ModifiedDate { get; set; }
        [DataMember]
        public string SourceDB { get; set; }
    }

    [DataContract]
    public class TransJudgeRecordResponse
    {
        [DataMember]
        public List<TransJudgeRecordDefault> ListJudgeRecord { get; set; }

        [DataMember]
        public List<TransSummedUpRecordDefault> ListSummed { get; set; }

        /// <summary>
        /// Tra ve thanh cong hay that bai
        /// </summary>
        [DataMember]
        public bool Susscess { get; set; }
        /// <summary>
        /// Ma loi tra ve
        /// </summary>
        [DataMember]
        public int ValidationCode { get; set; }

        [DataMember]
        public string Message { get; set; }
    }


}
