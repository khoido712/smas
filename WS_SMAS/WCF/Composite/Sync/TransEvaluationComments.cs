﻿using System;
using System.Collections.Generic;
using System.Runtime.Serialization;

namespace WCF.Composite.Sync
{
    [DataContract]
    public class TransEvaluationCommentsDefault
    {

        /// <summary> 
        /// Bảng nhận xét tháng của GV theo TT 30

        /// </summary>
        [DataMember]
        public long EvaluationCommentId { get; set; }
        /// <summary> 
        /// ID học sinh
        /// </summary>
        /// 
        [DataMember]
        public int PupilId { get; set; }
        /// <summary> 
        /// Học kỳ:
        /// </summary>
        /// 
        [DataMember]
        public int Semester { get; set; }

        /// <summary> 
        /// ID môn học, hoặc năng lực, phẩm chất
        /// </summary>
        /// 
        [DataMember]
        public int EvaluationCriteriaId { get; set; }
        /// <summary> 
        /// Tiêu chí đánh giá: Xem sheet 2
        /// </summary>
        /// 
        [DataMember]
        public int EvaluationId { get; set; }
        /// <summary> 
        /// 1: GVBM; 2:GVCN
        /// </summary>
        /// 
        [DataMember]
        public int TypeOfTeacher { get; set; }
        /// <summary> 
        /// Nhận xét tháng 1/6
        /// </summary>
        /// 
        [DataMember]
        public string CommentM1M6 { get; set; }
        /// <summary> 
        /// Nhận xét tháng 2/7
        /// </summary>
        /// 
        [DataMember]
        public string CommentM2M7 { get; set; }
        /// <summary> 
        /// Nhận xét tháng 3/8
        /// </summary>
        /// 
        [DataMember]
        public string CommentM3M8 { get; set; }
        /// <summary> 
        /// Nhận xét tháng 4/9
        /// </summary>
        /// 
        [DataMember]
        public string CommentM4M9 { get; set; }
        /// <summary> 
        /// Nhận xét tháng 5/10
        /// </summary>
        /// 
        [DataMember]
        public string CommentM5M10 { get; set; }


        [DataMember]
        public int Year { get; set; }

        [DataMember]
        public int? SynchronizeID { get; set; }

        [DataMember]
        public DateTime? UpdateTime { get; set; }

    }

    [DataContract]
    public class TransEvaluationCommentsResponse
    {
        [DataMember]
        public List<TransEvaluationCommentsDefault> ListEvaluationComments { get; set; }

        /// <summary>
        /// Tra ve thanh cong hay that bai
        /// </summary>
        [DataMember]
        public bool Susscess { get; set; }
        /// <summary>
        /// Ma loi tra ve
        /// </summary>
        [DataMember]
        public int ValidationCode { get; set; }

        [DataMember]
        public string Message { get; set; }
    }
}
