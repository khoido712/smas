﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Web;

namespace WCF.Request
{
    [DataContract]
    public class EmisAPIRequest
    {
        [DataMember(Name = "Username")]
        public string UserName { get; set; }

        [DataMember(Name = "Password")]
        public string Pass { get; set; }

        [DataMember(Name = "school_code")]
        public string SchoolCode { get; set; }

        [DataMember(Name = "applied_level")]
        public int AppliedLevelID { get; set; }

        [DataMember(Name = "sheet_code")]
        public string SheetCode { get; set; }

        [DataMember(Name = "file_name")]
        public string ReportCode { get; set; }

        [DataMember(Name = "year")]
        public int Year { get; set; }

        [DataMember(Name = "indicator_codes")]
        public List<string> IndicatorCode { get; set; }
    }
}