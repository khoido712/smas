﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Configuration;
using System.Web.Security;
using System.Web.SessionState;

namespace WCF
{
    public class Global : System.Web.HttpApplication
    {

        protected void Application_Start(object sender, EventArgs e)
        {
            log4net.Config.XmlConfigurator.Configure();
            EncryptConfig();
        }

        private void EncryptConfig()
        {
            string encrypt = ConfigurationManager.AppSettings["encrypt"].ToString();
            if (encrypt.Equals("true"))
            {
                Configuration objConfig = WebConfigurationManager.OpenWebConfiguration("~");
                ConfigurationSection objSection = objConfig.GetSection("connectionStrings");
                if (!objSection.SectionInformation.IsProtected)
                {
                    objSection.SectionInformation.ProtectSection("RsaProtectedConfigurationProvider");
                    objConfig.Save();
                }
            }
            else
            {
                Configuration objConfig = WebConfigurationManager.OpenWebConfiguration("~");
                ConfigurationSection section = objConfig.GetSection("connectionStrings");
                if (section.SectionInformation.IsProtected)
                {
                    section.SectionInformation.UnprotectSection();
                    objConfig.Save();
                }
            }
        }
    }
}