﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;
using WCF.IServices;
using WCF.Composite.EMIS;
using WCF.Request;
using SMAS.Business.IBusiness;
using SMAS.Models.Models;
using SMAS.Business.Business;
using log4net;
using System.Configuration;
using Oracle.DataAccess.Client;
using System.Data;
using System.IO;
using System.Text.RegularExpressions;

namespace WCF.Services
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the class name "EmisModule" in code, svc and config file together.
    // NOTE: In order to launch WCF Test Client for testing this service, please select EmisModule.svc or EmisModule.svc.cs at the Solution Explorer and start debugging.
    public class EmisModule : IEmisModule
    {
        private static string PathPhysicalEmis = "C:/EMIS/";
        private static string ExtensionFile = "*.sql";
        private static string SCHOOL_ID = "&SCHOOL_ID";
        private static string YEAR_ID = "&YEAR_ID";
        private static string PROVINCE_ID = "&PROVINCE_ID";
        #region Declare Variable
        private IClassProfileBusiness ClassProfileBusiness;
        private IEmployeeBusiness EmployeeBusiness;
        private IClassSubjectBusiness ClassSubjectBusiness;
        private ICalendarBusiness CalendarBusiness;
        private ISemeterDeclarationBusiness SemesterDeclarationBusiness;
        private IEducationLevelBusiness EducationLevelBusiness;
        private ISchoolProfileBusiness SchoolProfileBusiness;
        private IPeriodDeclarationBusiness PeriodDeclarationBusiness;
        private IUserAccountBusiness UserAccountBusiness;
        private ISupervisingDeptBusiness SupervisingDeptBusiness;
        private IAcademicYearBusiness AcademicYearBusiness;
        private ISubjectCatBusiness SubjectCatBusiness;
        private ITeachingRegistrationBusiness TeachingRegistrationBusiness;
        private ICalendarScheduleBusiness CalendarScheduleBusiness;
        private Iaspnet_UsersBusiness Aspnet_UsersBusiness;
        private IUserGroupBusiness UserGroupBusiness;
        private IGroupMenuBusiness GroupMenuBusiness;
        private IProvinceBusiness ProvinceBusiness;
        private IDistrictBusiness DistrictBusiness;
        private IMIdmappingBusiness MIdmappingBusiness;
        private IMonthCommentsBusiness MonthCommentsBusiness;
        private ILockMarkPrimaryBusiness LockMarkPrimaryBusiness;
        private IPushNotifyRequestBusiness PushNotifyRequestBusiness;
        private IApplicationConfigBusiness ApplicationConfigBusiness;
        private ISchoolFacultyBusiness SchoolFacultyBusiness;
        private IAreaBusiness AreaBusiness;
        private IExaminationsBusiness ExaminationsBusiness;
        private IExamSubjectBusiness ExamSubjectBusiness;
        private Iaspnet_MembershipBusiness aspnet_MembershipBusiness;
        private static readonly log4net.ILog logger = LogManager.GetLogger(typeof(SystemModule));

        private SMASEntities context;

        public EmisModule()
        {
            DeclareBusiness();
        }
        /// <summary>
        /// Khoi tao business cung context
        /// </summary>
        private void DeclareBusiness()
        {
            context = new SMASEntities();
            ClassProfileBusiness = new ClassProfileBusiness(logger, context);
            EmployeeBusiness = new EmployeeBusiness(logger, context);
            ClassSubjectBusiness = new ClassSubjectBusiness(logger, context);
            CalendarBusiness = new CalendarBusiness(logger, context);
            EducationLevelBusiness = new EducationLevelBusiness(logger, context);
            SchoolProfileBusiness = new SchoolProfileBusiness(logger, context);
            PeriodDeclarationBusiness = new PeriodDeclarationBusiness(logger, context);
            UserAccountBusiness = new UserAccountBusiness(logger, context);
            SupervisingDeptBusiness = new SupervisingDeptBusiness(logger, context);
            AcademicYearBusiness = new AcademicYearBusiness(logger, context);
            SubjectCatBusiness = new SubjectCatBusiness(logger, context);
            TeachingRegistrationBusiness = new TeachingRegistrationBusiness(logger, context);
            CalendarScheduleBusiness = new CalendarScheduleBusiness(logger, context);
            SemesterDeclarationBusiness = new SemeterDeclarationBusiness(logger, context);
            Aspnet_UsersBusiness = new aspnet_UsersBusiness(logger, context);
            UserGroupBusiness = new UserGroupBusiness(logger, context);
            GroupMenuBusiness = new GroupMenuBusiness(logger, context);
            ProvinceBusiness = new ProvinceBusiness(logger, context);
            DistrictBusiness = new DistrictBusiness(logger, context);
            MIdmappingBusiness = new MIdmappingBusiness(logger, context);
            MonthCommentsBusiness = new MonthCommentsBusiness(logger, context);
            LockMarkPrimaryBusiness = new LockMarkPrimaryBusiness(logger, context);
            PushNotifyRequestBusiness = new PushNotifyRequestBusiness(logger, context);
            ApplicationConfigBusiness = new ApplicationConfigBusiness(logger, context);
            ExaminationsBusiness = new ExaminationsBusiness(logger, context);
            ExamSubjectBusiness = new ExamSubjectBusiness(logger, context);
            SchoolFacultyBusiness = new SchoolFacultyBusiness(logger, context);
            AreaBusiness = new AreaBusiness(logger, context);
            aspnet_MembershipBusiness = new aspnet_MembershipBusiness(logger, context);
        }
        #endregion
        public EmisComposite GetEmisValue(EmisAPIRequest request)
        {
            EmisComposite response = new EmisComposite();
            try
            {
                #region doc du lieu tu folder
                List<EmisData> lstEmisData = new List<EmisData>();
                EmisData objEmisData = null;
                List<EMISArrrayInsertBO> lstEmisInsertBO = new List<EMISArrrayInsertBO>();
                EMISArrrayInsertBO objEmisInsertBO = null;
                string FolderByGrade = string.Empty;
                string reportCode = string.Empty;
                SchoolProfile objSP = SchoolProfileBusiness.All.Where(p => p.SyncCode == request.SchoolCode).FirstOrDefault();
                if (objSP == null)
                {
                    response.isValid = false;
                    return response;
                }
                AcademicYear objay = AcademicYearBusiness.All.Where(p => p.SchoolID == objSP.SchoolProfileID && p.Year == request.Year).FirstOrDefault();
                int period = 0;
                if (request.ReportCode.Contains("T9"))
                {
                    period = 1;
                }
                else if (request.ReportCode.Contains("T12"))
                {
                    period = 2;
                }
                else if (request.ReportCode.Contains("T5"))
                {
                    period = 3;
                }
                FolderByGrade = this.GetFolderScript(objSP.EducationGrade, period, objSP.TrainingTypeID.Value);
                reportCode = FolderByGrade.Substring(FolderByGrade.LastIndexOf('/') + 1);
                string fileName = FolderByGrade + "/" + request.SheetCode + ".sql";
                lstEmisInsertBO = this.ReadFileConvertToSQL(objSP.EducationGrade, period, objSP.TrainingTypeID.Value, objSP.SchoolProfileID, objay.AcademicYearID, objay.Year, reportCode, objSP.DistrictID.Value, objSP.ProvinceID.Value, fileName);
                //loc ra nhung ma tra ve theo request
                lstEmisInsertBO = lstEmisInsertBO.Where(p => request.IndicatorCode.Contains(p.cellAddress)).ToList();

                string indicatorCode = string.Empty;
                for (int i = 0; i < request.IndicatorCode.Count; i++)
                {
                    indicatorCode = request.IndicatorCode[i];
                    objEmisData = new EmisData();
                    

                    objEmisData.SchoolCode = request.SchoolCode;
                    objEmisData.IndicatorCode = indicatorCode;
                    objEmisInsertBO = lstEmisInsertBO.Where(p => p.cellAddress.Equals(indicatorCode)).FirstOrDefault();
                    if (objEmisInsertBO != null)
                    {
                        objEmisData.ErrorCode = "";
                        objEmisData.Value = objEmisInsertBO.cellValue;
                    }
                    else
                    {
                        objEmisData.ErrorCode = "1";
                        objEmisData.Message = "Mã tiêu chí không tồn tại";
                    }
                    lstEmisData.Add(objEmisData);
                }
                response.Data = lstEmisData;
                response.isValid = true;
                #endregion
            }
            catch (Exception ex)
            {
                logger.Error(ex.Message, ex);
                response.isValid = false;
            }
            return response;
        }
        private string GetFolderScript(int grade, int period, int trainingType)
        {
            string pathFolder = string.Empty;
            if (trainingType == WCFConstant.EMIS_TRAINING_TYPE_GDTX)
            {
                if (period == WCFConstant.PERROD_FIRST)
                {
                    pathFolder = PathPhysicalEmis + WCFConstant.EMIS_FOLDER_FIRST_ROOT + WCFConstant.EMIS_FOLDER_FIRST_GDTX;
                }
                else if (period == WCFConstant.PERROD_MIDDLE)
                {
                    pathFolder = PathPhysicalEmis + WCFConstant.EMIS_FOLDER_MIDDLE_ROOT + WCFConstant.EMIS_FOLDER_MIDDLE_GDTX;
                }
                else if (period == WCFConstant.PERROD_END)
                {
                    pathFolder = PathPhysicalEmis + WCFConstant.EMIS_FOLDER_END_ROOT + WCFConstant.EMIS_FOLDER_END_GDTX;
                }
            }
            else
            {
                if (period == WCFConstant.PERROD_FIRST)// dau nam
                {
                    string pathFirst = PathPhysicalEmis + WCFConstant.EMIS_FOLDER_FIRST_ROOT;
                    if (grade == WCFConstant.EMIS_EDUCATION_P)
                    {
                        pathFolder = pathFirst + WCFConstant.EMIS_FOLDER_FIRST_P;
                    }
                    else if (grade == WCFConstant.EMIS_EDUCATION_S)
                    {
                        pathFolder = pathFirst + WCFConstant.EMIS_FOLDER_FIRST_S;
                    }
                    else if (grade == WCFConstant.EMIS_EDUCATION_T)
                    {
                        pathFolder = pathFirst + WCFConstant.EMIS_FOLDER_FIRST_T;
                    }
                    else if (grade == WCFConstant.EMIS_EDUCATION_PS)
                    {
                        pathFolder = pathFirst + WCFConstant.EMIS_FOLDER_FIRST_PS;
                    }
                    else if (grade == WCFConstant.EMIS_EDUCATION_ST)
                    {
                        pathFolder = pathFirst + WCFConstant.EMIS_FOLDER_FIRST_ST;
                    }
                    else if (grade == WCFConstant.EMIS_EDUCATION_PST)
                    {
                        pathFolder = pathFirst + WCFConstant.EMIS_FOLDER_FIRST_PST;
                    }
                }
                else if (period == WCFConstant.PERROD_MIDDLE)//giua nam
                {
                    string pathMiddle = PathPhysicalEmis + WCFConstant.EMIS_FOLDER_MIDDLE_ROOT;
                    if (grade == WCFConstant.EMIS_EDUCATION_P)
                    {
                        pathFolder = pathMiddle + WCFConstant.EMIS_FOLDER_MIDDLE_P;
                    }
                    else if (grade == WCFConstant.EMIS_EDUCATION_S)
                    {
                        pathFolder = pathMiddle + WCFConstant.EMIS_FOLDER_MIDDLE_S;
                    }
                    else if (grade == WCFConstant.EMIS_EDUCATION_T)
                    {
                        pathFolder = pathMiddle + WCFConstant.EMIS_FOLDER_MIDDLE_T;
                    }
                    else if (grade == WCFConstant.EMIS_EDUCATION_PS)
                    {
                        pathFolder = pathMiddle + WCFConstant.EMIS_FOLDER_MIDDLE_PS;
                    }
                    else if (grade == WCFConstant.EMIS_EDUCATION_ST)
                    {
                        pathFolder = pathMiddle + WCFConstant.EMIS_FOLDER_MIDDLE_ST;
                    }
                    else if (grade == WCFConstant.EMIS_EDUCATION_PST)
                    {
                        pathFolder = pathMiddle + WCFConstant.EMIS_FOLDER_MIDDLE_PST;
                    }
                }
                else if (period == WCFConstant.PERROD_END) //cuoi nam
                {
                    string pathEnd = PathPhysicalEmis + WCFConstant.EMIS_FOLDER_END_ROOT;
                    if (grade == WCFConstant.EMIS_EDUCATION_P)
                    {
                        pathFolder = pathEnd + WCFConstant.EMIS_FOLDER_END_P;
                    }
                    else if (grade == WCFConstant.EMIS_EDUCATION_S)
                    {
                        pathFolder = pathEnd + WCFConstant.EMIS_FOLDER_END_S;
                    }
                    else if (grade == WCFConstant.EMIS_EDUCATION_T)
                    {
                        pathFolder = pathEnd + WCFConstant.EMIS_FOLDER_END_T;
                    }
                    else if (grade == WCFConstant.EMIS_EDUCATION_PS)
                    {
                        pathFolder = pathEnd + WCFConstant.EMIS_FOLDER_END_PS;
                    }
                    else if (grade == WCFConstant.EMIS_EDUCATION_ST)
                    {
                        pathFolder = pathEnd + WCFConstant.EMIS_FOLDER_END_ST;
                    }
                    else if (grade == WCFConstant.EMIS_EDUCATION_PST)
                    {
                        pathFolder = pathEnd + WCFConstant.EMIS_FOLDER_END_PST;
                    }
                }
            }
            return pathFolder;
        }
        private List<EMISArrrayInsertBO> ReadFileConvertToSQL(int grade, int period, int trainingType, int schoolID, int academicYearID, int year, string reportCode, int districtID, int provinceID, string filename)
        {
            string strSql = string.Empty;
            string sqlnew = string.Empty;
            List<EMISArrrayInsertBO> lstInsertBO = new List<EMISArrrayInsertBO>();
            List<EMISArrrayInsertBO> lsttmp = new List<EMISArrrayInsertBO>();
            try
            {
                string[] arrSql = null;
                string line_to_delete = "--";
                var blockComments = @"/\*(.*?)\*/";
                string strSqlNew = string.Empty;
                strSql = string.Empty;
                strSqlNew = string.Empty;
                StreamReader stremReader = new StreamReader(filename);
                while ((strSql = stremReader.ReadLine()) != null)
                {
                    if (strSql.Trim().StartsWith(line_to_delete)) continue;
                    else if (strSql.Contains(line_to_delete))
                    {
                        strSql = Regex.Replace(strSql, @"\t|\n|\r", "");
                        strSql = strSql.Trim().Substring(0, (strSql.Trim().IndexOf(line_to_delete)));
                    }
                    strSqlNew += strSql + " ";
                }

                strSql = this.AdjustmentSQL(strSqlNew);
                //block comment
                strSql = Regex.Replace(strSql, blockComments, me =>
                {
                    if (me.Value.StartsWith("/*") || me.Value.StartsWith("//"))
                        return me.Value.StartsWith("//") ? Environment.NewLine : "";
                    return me.Value;
                }, RegexOptions.Singleline);

                if (!string.IsNullOrEmpty(strSql))
                {
                    strSql = strSql.Replace(SCHOOL_ID, schoolID.ToString())
                                   .Replace(YEAR_ID, academicYearID.ToString())
                                   .Replace(PROVINCE_ID, provinceID.ToString());
                    arrSql = strSql.Split(new Char[] { WCFConstant.CHAR_SLASH }, StringSplitOptions.RemoveEmptyEntries);
                }
                logger.Info("Im OK");
                if (arrSql != null && arrSql.Length > 0)
                {
                    for (int j = 0; j < arrSql.Length; j++)
                    {
                        if (!string.IsNullOrWhiteSpace(arrSql[j]))
                        {
                            lsttmp = this.GetDataFromSQL(arrSql[j], filename, schoolID, academicYearID, year, reportCode, districtID, provinceID);
                            if (lsttmp.Count > 0)
                            {
                                lstInsertBO.AddRange(lsttmp);    
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                logger.Error(ex.Message, ex);
            }
            logger.Info("lstInsertCount " + lstInsertBO.Count);
            return lstInsertBO;
        }
        private List<EMISArrrayInsertBO> GetDataFromSQL(string sql, string sheetname, int schoolID, int academicYear, int year, string reportCode, int districtID, int provinceID)
        {
            List<EMISArrrayInsertBO> lstInsertBO = new List<EMISArrrayInsertBO>();
            EMISArrrayInsertBO objBO = null;
            try
            {
                string strConnect = ConfigurationManager.ConnectionStrings["OraAspNetConnectionString"].ToString();
                if (String.IsNullOrEmpty(strConnect))
                {
                    return new List<EMISArrrayInsertBO>();
                }
                using (OracleConnection oConn = new OracleConnection(strConnect))
                {
                    try
                    {
                        if (oConn == null)
                        {
                            return new List<EMISArrrayInsertBO>();
                        }
                        if (oConn.State != ConnectionState.Open)
                        {
                            oConn.Open();
                        }
                        using (OracleCommand command = new OracleCommand(sql, oConn))
                        {
                            command.Connection = oConn;
                            DataTable dt = new DataTable();

                            command.CommandType = CommandType.Text;
                            OracleDataReader reader = command.ExecuteReader();
                            dt.Load(reader);
                            reader.Dispose();
                            string sqlresult = string.Empty;
                            object cellValue = null;

                            foreach (DataRow row in dt.Rows)
                            {
                                foreach (DataColumn column in dt.Columns)
                                {
                                    objBO = new EMISArrrayInsertBO();
                                    cellValue = row[column] != null ? row[column] : null;
                                    objBO.cellAddress = column.ColumnName;
                                    objBO.cellValue = cellValue.ToString();
                                    lstInsertBO.Add(objBO);
                                }
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        logger.Error(ex.Message, ex);
                    }
                    finally
                    {
                        if (oConn != null)
                        {
                            oConn.Close();
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                logger.Error(ex.Message, ex);
            }
            if (lstInsertBO.Count > 0)
            {
                return lstInsertBO;
            }
            return null;
        }
        private string AdjustmentSQL(string SQL)
        {
            SQL = SQL.Replace("from", " from ")
                .Replace("where", " where ")
                .Replace("\t", " ")
                .Replace(";", "");
            return SQL;
        }
        private class EMISArrrayInsertBO
        {
            public string cellValue { get; set; }
            public string cellAddress { get; set; }
        }
        private class arrInsertBO
        {
            public string[] arrCellValue { get; set; }
            public string[] arrAddress { get; set; }
        }
    }
    
}
