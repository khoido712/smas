﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;
using log4net;
using SMAS.Business.Business;
using SMAS.Business.Common;
using SMAS.Business.IBusiness;
using SMAS.Models.Models;
using WCF.Composite;
using System.Web.Security;

namespace WCF
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the class name "SupervisingDeptModule" in code, svc and config file together.
    // NOTE: In order to launch WCF Test Client for testing this service, please select SupervisingDeptModule.svc or SupervisingDeptModule.svc.cs at the Solution Explorer and start debugging.
    public class SupervisingDeptModule : ISupervisingDeptModule
    {
        #region Declare Variable
        private IEmployeeBusiness EmployeeBusiness;
        private IClassSubjectBusiness ClassSubjectBusiness;
        private ISchoolProfileBusiness SchoolProfileBusiness;
        private ISupervisingDeptBusiness SupervisingDeptBusiness;
        private IAcademicYearBusiness AcademicYearBusiness;
        private IUserAccountBusiness UserAccountBusiness;
        private static readonly log4net.ILog logger = LogManager.GetLogger(typeof(SystemModule));
        /// <summary>
        /// Khoi tao business cung context
        /// </summary>
        private void DeclareBusiness()
        {
            SMASEntities context = new SMASEntities();
            EmployeeBusiness = new EmployeeBusiness(logger, context);
            ClassSubjectBusiness = new ClassSubjectBusiness(logger, context);
            SchoolProfileBusiness = new SchoolProfileBusiness(logger, context);
            SupervisingDeptBusiness = new SupervisingDeptBusiness(logger, context);
            AcademicYearBusiness = new AcademicYearBusiness(logger, context);
            UserAccountBusiness = new UserAccountBusiness(logger, context);
        }
        #endregion

        /// <summary>
        /// Lấy danh sách trường học theo cấp quản lý và danh sách ID trường
        /// </summary>
        /// <param name="SupervisingDeptID"></param>
        /// <param name="SchoolIDList"></param>
        /// <returns></returns>
        public ListSchoolProfileLiteResponse GetListSchoolByListID(int SupervisingDeptID, List<int> SchoolIDList)
        {
            ListSchoolProfileLiteResponse objListSchoolProfileLiteResponse = new ListSchoolProfileLiteResponse();
            try
            {
                //Khoi tao Business
                DeclareBusiness();
                // Lay danh sach tat cac don vi quan ly thuoc quan ly cua don vi truyen vao hoac chinh no
                string traversalPath = "\\" + SupervisingDeptID + "\\";
                IQueryable<SupervisingDept> listSupervisingDept = SupervisingDeptBusiness.AllNoTracking
                    .Where(o => o.IsActive && o.TraversalPath.Contains(traversalPath) || o.SupervisingDeptID == SupervisingDeptID);
                IQueryable<SchoolProfile> iQSchool = SchoolProfileBusiness.AllNoTracking
                    .Where(o => listSupervisingDept.Any(s => s.SupervisingDeptID == o.SupervisingDeptID));
                if (SchoolIDList != null && SchoolIDList.Count > 0)
                {
                    iQSchool = iQSchool.Where(o => SchoolIDList.Contains(o.SchoolProfileID));
                }

                List<SchoolProfileLite> listSchool = iQSchool.Select(o => new SchoolProfileLite
                {
                    SchoolID = o.SchoolProfileID,
                    SchoolName = o.SchoolName,
                    HeadMasterName = o.HeadMasterName,
                    HeadMasterPhone = o.HeadMasterPhone,
                    DistrictName = string.Empty                    
                }).ToList();                

                //gan validationCode thanh cong
                objListSchoolProfileLiteResponse.ValidationCode = WCFConstant.RESPONSE_SUCCESS;
                //gan gia tri thuoc tinh dinh kem 
                objListSchoolProfileLiteResponse.LstSchoolProfileLite = listSchool;
            }
            catch (Exception)
            {
                //gan validationCode exception
                objListSchoolProfileLiteResponse.ValidationCode = WCFConstant.RESPONSE_EXCEPTION;
            }
            return objListSchoolProfileLiteResponse;
        }

        /// <summary>
        /// Lay ID phong thuoc quan ly cua don vi dau vao
        /// </summary>
        /// <param name="SupervisingDeptID"></param>
        /// <returns></returns>
        public ListIntResultResponse GetListSupervisingDeptIDByID(int SupervisingDeptID)
        {
            ListIntResultResponse objListIntResultResponse = new ListIntResultResponse();
            try
            {
                //Khoi tao Business
                DeclareBusiness();
                SupervisingDept supervisingDept = SupervisingDeptBusiness.Find(SupervisingDeptID);
                if (supervisingDept == null)
                {
                    objListIntResultResponse.ValidationCode = WCFConstant.RESPONSE_SUPERVISING_DEPT_NOT_FOUND;
                    return objListIntResultResponse;
                }
                Dictionary<string, object> dic = new Dictionary<string, object>();
                dic["IsActive"] = true;
                dic["ParentID"] = SupervisingDeptID;
                dic["HierachyLevel"] = supervisingDept.HierachyLevel + 1;
                List<int> listSupervisingDeptID = SupervisingDeptBusiness.Search(dic)
                    .Select(o => o.SupervisingDeptID).ToList();
                //gan validationCode thanh cong
                objListIntResultResponse.ValidationCode = WCFConstant.RESPONSE_SUCCESS;
                //gan gia tri thuoc tinh dinh kem 
                objListIntResultResponse.Result = listSupervisingDeptID;
            }
            catch (Exception)
            {
                //gan validationCode exception
                objListIntResultResponse.ValidationCode = WCFConstant.RESPONSE_EXCEPTION;
            }
            return objListIntResultResponse;
        }

        public ListEmployeeSupervisingDeptReponse GetlistEmployeeByListID(List<EmployeeRequest> objEmployeeRequest)
        {
            ListEmployeeSupervisingDeptReponse objListEmployeeSupervisingDeptReponse = new ListEmployeeSupervisingDeptReponse();
            try
            {
                DeclareBusiness();
                List<int> lstEmployeeID = objEmployeeRequest.Select(p => p.EmployeeID).ToList();
                List<int> lstSupervisingDeptID = objEmployeeRequest.Select(p => p.SupervisingDeptID).ToList();
                List<EmployeeSupervisingDept> resultEmp = (from e in EmployeeBusiness.All
                                                           where lstEmployeeID.Contains(e.EmployeeID)
                                                           select new EmployeeSupervisingDept
                                                           {
                                                               EmployeeID = e.EmployeeID,
                                                               EmployeeName = e.FullName,
                                                               EmployeeCode = e.EmployeeCode,
                                                               EmployeeMobile = e.Mobile,
                                                           }).ToList();

                List<EmployeeSupervisingDept> resultDept = (from s in SupervisingDeptBusiness.All
                                                            where lstSupervisingDeptID.Contains(s.SupervisingDeptID)
                                                            select new EmployeeSupervisingDept
                                                            {
                                                                SupervisingDeptID = s.SupervisingDeptID,
                                                                SupervisingDeptName = s.SupervisingDeptName
                                                            }).ToList();

                List<EmployeeSupervisingDept> result = (from p in objEmployeeRequest
                                                        join e in resultEmp on p.EmployeeID equals e.EmployeeID
                                                        join s in resultDept on p.SupervisingDeptID equals s.SupervisingDeptID
                                                        select new EmployeeSupervisingDept
                                                        {
                                                            EmployeeID = e.EmployeeID,
                                                            EmployeeName = e.EmployeeName,
                                                            EmployeeCode = e.EmployeeCode,
                                                            EmployeeMobile = e.EmployeeMobile,
                                                            SupervisingDeptID = s.SupervisingDeptID,
                                                            SupervisingDeptName = s.SupervisingDeptName
                                                        }).ToList();

                //gan validationCode thanh cong
                objListEmployeeSupervisingDeptReponse.ValidationCode = WCFConstant.RESPONSE_SUCCESS;
                //gan gia tri thuoc tinh dinh kem 
                objListEmployeeSupervisingDeptReponse.LstEmployeeSupervisingDept = result;
            }
            catch (Exception)
            {
                //gan validationCode exception
                objListEmployeeSupervisingDeptReponse.ValidationCode = WCFConstant.RESPONSE_EXCEPTION;
            }
            return objListEmployeeSupervisingDeptReponse;
        }

        /// <summary>
        /// Lấy thông tin nhân viên phòng sở theo List ID truyền vào
        /// </summary>
        /// <param name="EmployeeIDList"></param>
        /// <returns></returns>
        public ListEmployeeSupervisingDeptReponse GetListEmployeeNameByListEmployeeID(List<int> EmployeeIDList)
        {
            ListEmployeeSupervisingDeptReponse objListEmployeeSupervisingDeptReponse = new ListEmployeeSupervisingDeptReponse();
            try
            {
                //Khoi tao Business
                DeclareBusiness();
                List<EmployeeSupervisingDept> listEmployee = null;
                if (EmployeeIDList != null && EmployeeIDList.Count > 0)
                {
                    listEmployee = EmployeeBusiness.AllNoTracking.Where(o => EmployeeIDList.Contains(o.EmployeeID) && o.Mobile != null)
                        .Select(o => new EmployeeSupervisingDept
                        {
                            EmployeeID = o.EmployeeID,
                            EmployeeName = o.FullName,
                            EmployeeCode = o.EmployeeCode,
                            EmployeeMobile = o.Mobile,
                            SupervisingDeptID = o.SupervisingDeptID.HasValue ? o.SupervisingDeptID.Value : 0,
                            ParentSupervisingDeptName = o.SupervisingDept.SupervisingDeptName,
                            ParentSupervisingDeptID = o.SupervisingDept.ParentID.HasValue ? o.SupervisingDept.ParentID.Value : 0
                        }).ToList();
                }

                // gan du lieu ten don vi cha
                List<int> superId = listEmployee.Where(p => p.SupervisingDeptID != 0).Select(p => p.SupervisingDeptID).Distinct().ToList();
                if (superId != null && superId.Count > 0)
                {
                    string superName = string.Empty;
                    SupervisingDept item = null;
                    List<EmployeeSupervisingDept> itemList = new List<EmployeeSupervisingDept>();
                    string[] traversalPathArray = null;
                    for (int ik = superId.Count - 1; ik >= 0; ik--)
                    {
                        item = SupervisingDeptBusiness.Find(superId[ik]);
                        itemList = listEmployee.Where(p => p.SupervisingDeptID == item.SupervisingDeptID).ToList();
                        if (item.HierachyLevel == SystemParamsInFile.EDUCATION_HIERACHY_LEVEL_PROVINCE_OFFICE || item.HierachyLevel == SystemParamsInFile.EDUCATION_HIERACHY_LEVEL_DISTRICT_OFFICE)
                        {
                            for (int iz = itemList.Count - 1; iz >= 0; iz--)
                            {
                                itemList[iz].ParentSupervisingDeptName = item.SupervisingDeptName;
                            }
                        }
                        else
                        {
                            traversalPathArray = item.TraversalPath.Split(new char[] { '\\' }, StringSplitOptions.RemoveEmptyEntries);
                            for (int i = traversalPathArray.Length - 1; i >= 0; i--)
                            {
                                item = SupervisingDeptBusiness.Find(Convert.ToInt32(traversalPathArray[i]));
                                if (item.HierachyLevel == SystemParamsInFile.EDUCATION_HIERACHY_LEVEL_PROVINCE_OFFICE || item.HierachyLevel == SystemParamsInFile.EDUCATION_HIERACHY_LEVEL_DISTRICT_OFFICE)
                                {
                                    break;
                                }
                            }

                            for (int iz = itemList.Count - 1; iz >= 0; iz--)
                            {
                                itemList[iz].ParentSupervisingDeptName = item.SupervisingDeptName;
                            }
                        }

                    }

                }

                //gan validationCode thanh cong
                objListEmployeeSupervisingDeptReponse.ValidationCode = WCFConstant.RESPONSE_SUCCESS;
                //gan gia tri thuoc tinh dinh kem 
                objListEmployeeSupervisingDeptReponse.LstEmployeeSupervisingDept = listEmployee;
            }
            catch (Exception)
            {
                //gan validationCode exception
                objListEmployeeSupervisingDeptReponse.ValidationCode = WCFConstant.RESPONSE_EXCEPTION;
            }
            return objListEmployeeSupervisingDeptReponse;
        }

        /// <summary>
        /// Lấy thông tin nhà trường trực thuộc đơn vị - có phân trang từng phần
        /// </summary>
        /// <param name="EducationGrade"></param>
        /// <param name="SuperVisingDeptID"></param>
        /// <param name="SchoolName"></param>
        /// <param name="PageNum"></param>
        /// <param name="PageSize"></param>
        /// <returns></returns>
        public ListSchoolProfileLiteByRegionResponse GetSchoolByRegion(int EducationGrade, int SupervisingDeptID, string SchoolName
            , int PageNum, int PageSize)
        {
            ListSchoolProfileLiteByRegionResponse objListSchoolProfileLiteByRegionResponse = new ListSchoolProfileLiteByRegionResponse();
            try
            {
                //Khoi tao Business
                DeclareBusiness();
                // Lay danh sach tat cac don vi quan ly thuoc quan ly cua don vi truyen vao hoac chinh no
                string traversalPath = "\\" + SupervisingDeptID + "\\";
                IQueryable<SupervisingDept> listSupervisingDept = SupervisingDeptBusiness.AllNoTracking
                    .Where(o => o.IsActive && (o.TraversalPath.Contains(traversalPath) || o.SupervisingDeptID == SupervisingDeptID));
                IQueryable<SchoolProfile> iQSchool = SchoolProfileBusiness.AllNoTracking
                    .Where(o => listSupervisingDept.Any(s => s.SupervisingDeptID == o.SupervisingDeptID) 
					&& o.IsActiveSMAS == true 
					&& !string.IsNullOrEmpty(o.HeadMasterPhone.Trim())
					 && o.IsActive == true
					);

                EducationGrade = EducationGrade > 0 ? Utils.GradeToBinary(EducationGrade) : (int)0;
                if (EducationGrade > 0)
                {
                    iQSchool = iQSchool.Where(o => (o.EducationGrade & EducationGrade) != 0);
                }

                if (!string.IsNullOrWhiteSpace(SchoolName))
                {
                    SchoolName = SchoolName.Trim();
                    iQSchool = iQSchool.Where(o => o.SchoolName.ToLower().Contains(SchoolName.ToLower()));
                }			

                int totalRecord = iQSchool.Count();
                List<SchoolProfileLite> listSchool = iQSchool
                    .Select(o => new SchoolProfileLite
                    {
                        SchoolID = o.SchoolProfileID,
                        SchoolName = o.SchoolName,
                        HeadMasterName = o.HeadMasterName,
                        HeadMasterPhone = o.HeadMasterPhone,
                        DistrictName = o.District.DistrictName
                    }).OrderBy(o => o.SchoolName).Skip((PageNum - 1) * PageSize).Take(PageSize).ToList();

                //gan validationCode thanh cong
                objListSchoolProfileLiteByRegionResponse.ValidationCode = WCFConstant.RESPONSE_SUCCESS;
                //gan gia tri thuoc tinh dinh kem 
                objListSchoolProfileLiteByRegionResponse.LstSchoolProfileLite = listSchool;
                // Tong so ban ghi
                objListSchoolProfileLiteByRegionResponse.TotalRecord = totalRecord;
            }
            catch (Exception)
            {
                //gan validationCode exception
                objListSchoolProfileLiteByRegionResponse.ValidationCode = WCFConstant.RESPONSE_EXCEPTION;
            }
            return objListSchoolProfileLiteByRegionResponse;
        }

        /// <summary>
        /// Lay thong tin cap truc thuoc don vi
        /// </summary>
        /// <param name="SuperVisingDeptID"></param>
        /// <returns></returns>
        public ListSupervisingDeptLiteResponse GetListSuperVisingDeptByID(int SupervisingDeptID)
        {
            ListSupervisingDeptLiteResponse objListSupervisingDeptLiteResponse = new ListSupervisingDeptLiteResponse();
            try
            {
                //Khoi tao Business
                DeclareBusiness();
                SupervisingDept supervisingDept = SupervisingDeptBusiness.Find(SupervisingDeptID);
                if (supervisingDept == null)
                {
                    objListSupervisingDeptLiteResponse.ValidationCode = WCFConstant.RESPONSE_SUPERVISING_DEPT_NOT_FOUND;
                    return objListSupervisingDeptLiteResponse;
                }
                // Neu don vi dau vao khong phai phong so thi tra lai danh sach rong
                if (supervisingDept.HierachyLevel != SystemParamsInFile.EDUCATION_HIERACHY_LEVEL_PROVINCE_OFFICE
                    && supervisingDept.HierachyLevel != SystemParamsInFile.EDUCATION_HIERACHY_LEVEL_PROVINCE_OFFICE_DEPARTMENT
                    && supervisingDept.HierachyLevel != SystemParamsInFile.EDUCATION_HIERACHY_LEVEL_DISTRICT_OFFICE
                    && supervisingDept.HierachyLevel != SystemParamsInFile.EDUCATION_HIERACHY_LEVEL_DISTRICT_OFFICE)
                {
                    objListSupervisingDeptLiteResponse.ValidationCode = WCFConstant.RESPONSE_SUCCESS;
                    return objListSupervisingDeptLiteResponse;
                }
                // ID don vi thuc te se lay danh sach don vi con truc thuoc
                int trueSupervisingDeptID = 0;
                if (supervisingDept.HierachyLevel == SystemParamsInFile.EDUCATION_HIERACHY_LEVEL_PROVINCE_OFFICE
                    || supervisingDept.HierachyLevel == SystemParamsInFile.EDUCATION_HIERACHY_LEVEL_DISTRICT_OFFICE)
                {
                    trueSupervisingDeptID = SupervisingDeptID;
                }
                else
                {
                    // Lay tat ca cac ID cha de lay danh sach don vi truc thuoc cua ID cha
                    string traversalPath = supervisingDept.TraversalPath;
                    string[] parentIds = traversalPath.Split('\\');
                    foreach (string parentId in parentIds)
                    {
                        if (!string.IsNullOrWhiteSpace(parentId))
                        {
                            SupervisingDept st = SupervisingDeptBusiness.Find(int.Parse(parentId));
                            if (st.HierachyLevel == SystemParamsInFile.EDUCATION_HIERACHY_LEVEL_PROVINCE_OFFICE
                                || st.HierachyLevel == SystemParamsInFile.EDUCATION_HIERACHY_LEVEL_DISTRICT_OFFICE)
                            {
                                trueSupervisingDeptID = st.SupervisingDeptID;
                                break;
                            }
                        }
                    }
                }
                // Neu van khong co ID cap phong hoac so tuong ung thi tra lai danh sach rong
                List<SupervisingDeptLite> listSupervisingDept = null;
                if (trueSupervisingDeptID > 0)
                {
                    Dictionary<string, object> dic = new Dictionary<string, object>();
                    dic["IsActive"] = true;
                    dic["ParentID"] = trueSupervisingDeptID;
                    listSupervisingDept = SupervisingDeptBusiness.Search(dic)
                        .Select(o => new SupervisingDeptLite
                        {
                            SupervisingDeptID = o.SupervisingDeptID,
                            SupervisingDeptName = o.SupervisingDeptName,
                            ParentID = o.ParentID.HasValue ? o.ParentID.Value : 0,
                            HierachyLevel = o.HierachyLevel,
                            TraversalPath = o.TraversalPath
                        }).OrderBy(o => o.SupervisingDeptName).ToList();
                }
                //gan validationCode thanh cong
                objListSupervisingDeptLiteResponse.ValidationCode = WCFConstant.RESPONSE_SUCCESS;
                //gan gia tri thuoc tinh dinh kem 
                objListSupervisingDeptLiteResponse.LstListSupervisingDeptLiteResponse = listSupervisingDept;
            }
            catch (Exception)
            {
                //gan validationCode exception
                objListSupervisingDeptLiteResponse.ValidationCode = WCFConstant.RESPONSE_EXCEPTION;
            }
            return objListSupervisingDeptLiteResponse;
        }

        #region Lay danh sach can bo theo dieu kien dau vao - Khong Co phan trang tung phan
        /// <summary>
        /// Lay danh sach can bo theo dieu kien dau vao - Khong Co phan trang tung phan
        /// </summary>
        /// <author date="2014/02/18">HaiVT</author>
        /// <param name="SuperisingDeptID">don vi phong so</param>
        /// <returns></returns>
        public ListEmployeeSupervisingDeptReponse GetEmployeeBySuperVisingDeptID(int SupervisingDeptID,bool SupervisingRole,bool SupervisingSubRole)
        {
            ListEmployeeSupervisingDeptReponse objListEmployeeSupervisingDeptPaginateReponse = new ListEmployeeSupervisingDeptReponse();
            try
            {
                //Khoi tao Business
                DeclareBusiness();
                string traversalPath = string.Empty;
                SupervisingDept objSuperVisingDept = SupervisingDeptBusiness.Find(SupervisingDeptID);
                traversalPath = objSuperVisingDept.TraversalPath + SupervisingDeptID + "\\";
                //IQueryable<Employee> iqEmployee = EmployeeBusiness.SearchEmployee(SupervisingDeptID, dic).Where(p=>p.Mobile != null);
                List<EmployeeSupervisingDept> listEmployee = (from e in EmployeeBusiness.All
                                                              join s in SupervisingDeptBusiness.All on e.SupervisingDeptID equals s.SupervisingDeptID
                                                              where e.IsActive
                                                              && (s.TraversalPath.Contains(traversalPath) || e.SupervisingDeptID == SupervisingDeptID)
                                                              && ((SupervisingRole && (s.HierachyLevel == 3 || s.HierachyLevel == 4))
                                                                    || (SupervisingSubRole && (s.HierachyLevel == 5 || s.HierachyLevel == 6)))
                                                              && e.Mobile != null
                                                              orderby e.FullName
                                                              select new EmployeeSupervisingDept
                                                              {
                                                                  EmployeeName = e.FullName
                                                              }).ToList();

                //gan validationCode thanh cong
                objListEmployeeSupervisingDeptPaginateReponse.ValidationCode = WCFConstant.RESPONSE_SUCCESS;
                //gan gia tri thuoc tinh dinh kem 
                objListEmployeeSupervisingDeptPaginateReponse.LstEmployeeSupervisingDept = listEmployee;
            }
            catch (Exception)
            {
                //gan validationCode exception
                objListEmployeeSupervisingDeptPaginateReponse.ValidationCode = WCFConstant.RESPONSE_EXCEPTION;
            }
            return objListEmployeeSupervisingDeptPaginateReponse;
        }
        #endregion

        /// <summary>
        /// Lay danh sach can bo theo dieu kien dau vao - Co phan trang tung phan
        /// </summary>
        /// <param name="CurrentEmployeeID"></param>
        /// <param name="SearchContent"></param>
        /// <param name="SuperisingDeptID"></param>
        /// <param name="PageNum"></param>
        /// <param name="PageSize"></param>
        /// <returns></returns>
        public ListEmployeeSupervisingDeptPaginateReponse GetEmployeeBySuperVisingDept(int CurrentEmployeeID, string SearchContent, int SupervisingDeptID
            , int PageNum, int PageSize)
        {
            ListEmployeeSupervisingDeptPaginateReponse objListEmployeeSupervisingDeptPaginateReponse = new ListEmployeeSupervisingDeptPaginateReponse();
            try
            {
                //Khoi tao Business
                DeclareBusiness();
                Dictionary<string, object> dic = new Dictionary<string, object>();
                dic["TraversalPath"] = "\\" + SupervisingDeptID + "\\";
                IQueryable<Employee> iqEmployee = EmployeeBusiness.SearchEmployee(SupervisingDeptID, dic)
                    .Where(o => o.EmployeeID != CurrentEmployeeID && !string.IsNullOrEmpty(o.Mobile.Trim()));
                if (!string.IsNullOrWhiteSpace(SearchContent))
                {
                    iqEmployee = iqEmployee.Where(o => o.EmployeeCode.ToLower().Contains(SearchContent.Trim().ToLower())
                        || o.FullName.ToLower().Contains(SearchContent.Trim().ToLower()));
                }               
                int totalRecord = iqEmployee.Count();
                List<EmployeeSupervisingDept> listEmployee = iqEmployee
                    .Select(o => new EmployeeSupervisingDept
                    {
                        EmployeeID = o.EmployeeID,
                        EmployeeCode = o.EmployeeCode,
                        EmployeeName = o.FullName,
                        EmployeeMobile = o.Mobile,
                        SupervisingDeptName = o.SupervisingDept.SupervisingDeptName,
                    }).OrderBy(o => o.EmployeeName).Skip((PageNum - 1) * PageSize).Take(PageSize).ToList();

                //gan validationCode thanh cong
                objListEmployeeSupervisingDeptPaginateReponse.ValidationCode = WCFConstant.RESPONSE_SUCCESS;
                //gan gia tri thuoc tinh dinh kem 
                objListEmployeeSupervisingDeptPaginateReponse.LstEmployeeSupervisingDept = listEmployee;
                // Tong so ban ghi
                objListEmployeeSupervisingDeptPaginateReponse.TotalRecord = totalRecord;
            }
            catch (Exception)
            {
                //gan validationCode exception
                objListEmployeeSupervisingDeptPaginateReponse.ValidationCode = WCFConstant.RESPONSE_EXCEPTION;
            }
            return objListEmployeeSupervisingDeptPaginateReponse;
        }

        #region Tìm kiếm tât cả nhân viên thuộc đơn vị kể cả nhân viên của đơn vị con.
        /// <summary>
        /// Tìm kiếm tât cả nhân viên thuộc đơn vị kể cả nhân viên của đơn vị con.
        /// </summary>
        /// <author date="2014/02/20">HaiVT</author>
        /// <param name="CurrentEmployeeID">dk where khac voi currentEmployeeID</param>
        /// <param name="SearchContent">noi dung tim kiem</param>
        /// <param name="SupervisingDeptID">id phong so</param>
        /// <returns></returns>
        public ListIntResultResponse GetEmployeeBySuperVisingDeptNoPaging(int CurrentEmployeeID, string SearchContent, int SupervisingDeptID)
        {
            ListIntResultResponse objListEmployeeSupervisingDeptPaginateReponse = new ListIntResultResponse();
            try
            {
                //Khoi tao Business
                DeclareBusiness();
                Dictionary<string, object> dic = new Dictionary<string, object>();
                dic["TraversalPath"] = "\\" + SupervisingDeptID + "\\";
                IQueryable<Employee> iqEmployee = EmployeeBusiness.SearchEmployee(SupervisingDeptID, dic)
                    .Where(o => o.EmployeeID != CurrentEmployeeID && !string.IsNullOrEmpty(o.Mobile.Trim()));
                if (!string.IsNullOrWhiteSpace(SearchContent))
                {
                    iqEmployee = iqEmployee.Where(o => o.EmployeeCode.ToUpper().Contains(SearchContent.Trim().ToUpper())
                        || o.FullName.ToUpper().Contains(SearchContent.Trim().ToUpper()));
                }                

                //gan validationCode thanh cong
                objListEmployeeSupervisingDeptPaginateReponse.ValidationCode = WCFConstant.RESPONSE_SUCCESS;
                //gan gia tri thuoc tinh dinh kem 
                objListEmployeeSupervisingDeptPaginateReponse.Result = iqEmployee.Select(p => p.EmployeeID).ToList();
            }
            catch (Exception)
            {
                //gan validationCode exception
                objListEmployeeSupervisingDeptPaginateReponse.ValidationCode = WCFConstant.RESPONSE_EXCEPTION;
            }
            return objListEmployeeSupervisingDeptPaginateReponse;
        }
        #endregion

        #region lấy SuperVisingDeptID là đơn vị trực thuộc phòng sở thì lấy ra phòng/sở tương ứng
        /// <summary>
        /// lấy SuperVisingDeptID là đơn vị trực thuộc phòng sở thì lấy ra phòng/sở tương ứng
        /// </summary>
        /// <param name="superVisingDeptID">id don vi</param>
        /// <returns></returns>
        public SupervisingDeptResponse GetParentBySupervisingDeptID(int superVisingDeptID)
        {
            SupervisingDeptResponse result = new SupervisingDeptResponse();
            try
            {
                //Khoi tao Business
                DeclareBusiness();
                SupervisingDept supervisingDept = SupervisingDeptBusiness.Find(superVisingDeptID);                
                SupervisingDeptDefault supervisingDeptDefault = new SupervisingDeptDefault();
                if (supervisingDept != null)
                {
                    if (supervisingDept.HierachyLevel == SystemParamsInFile.EDUCATION_HIERACHY_LEVEL_PROVINCE_OFFICE_DEPARTMENT || supervisingDept.HierachyLevel == SystemParamsInFile.EDUCATION_HIERACHY_LEVEL_DISTRICT_OFFICE_DEPARTMENT)
                    {
                        string[] traversalPathArray = supervisingDept.TraversalPath.Split(new char[] { '\\' }, StringSplitOptions.RemoveEmptyEntries);
                        for (int i = traversalPathArray.Length - 1; i >= 0; i-- )
                        {
                            supervisingDept = SupervisingDeptBusiness.Find(Convert.ToInt32(traversalPathArray[i]));
                            if (supervisingDept.HierachyLevel == SystemParamsInFile.EDUCATION_HIERACHY_LEVEL_PROVINCE_OFFICE || supervisingDept.HierachyLevel == SystemParamsInFile.EDUCATION_HIERACHY_LEVEL_DISTRICT_OFFICE)
                            {
                                break;
                            }
                        }
                    }				
					
                    supervisingDeptDefault = new SupervisingDeptDefault()
                    {
                        Address = supervisingDept.Address,
                        AdminID = supervisingDept.AdminID.HasValue ? supervisingDept.AdminID.Value : 0,
                        AreaID = supervisingDept.AreaID.HasValue ? supervisingDept.AreaID.Value : 0,
                        AreaName = supervisingDept.AreaID.HasValue ? supervisingDept.Area.AreaName : string.Empty,
                        ProvinceID = supervisingDept.ProvinceID.HasValue ? supervisingDept.ProvinceID.Value : 0,
                        ProvinceName = supervisingDept.ProvinceID.HasValue ? supervisingDept.Province.ProvinceName : string.Empty,
                        DistrictID = supervisingDept.DistrictID.HasValue ? supervisingDept.DistrictID.Value : 0,
                        DistrictName = supervisingDept.DistrictID.HasValue ? supervisingDept.District.DistrictName : string.Empty,
                        Email = supervisingDept.Email,
                        Fax = supervisingDept.Fax,
                        HierachyLevel = supervisingDept.HierachyLevel,
                        ParentID = supervisingDept.ParentID.HasValue ? supervisingDept.ParentID.Value : 0,
                        SupervisingDeptUserName = supervisingDept.UserAccount != null && supervisingDept.UserAccount.aspnet_Users != null ?
                                            supervisingDept.UserAccount.aspnet_Users.UserName : "",
                        SupervisingDeptCode = supervisingDept.SupervisingDeptCode,
                        SupervisingDeptID = supervisingDept.SupervisingDeptID,
                        SupervisingDeptName = supervisingDept.SupervisingDeptName,
                        Telephone = supervisingDept.Telephone,
                        Website = supervisingDept.Website,
                        TraversalPath = supervisingDept.TraversalPath,
						SMSActiveType = supervisingDept.SMSActiveType.HasValue ? supervisingDept.SMSActiveType.Value : 0,
                        IsActiveSMAS = supervisingDept.IsActiveSMAS.HasValue ? supervisingDept.IsActiveSMAS.Value : false,
                        EstablishedDate = supervisingDept.CreatedDate.HasValue ? supervisingDept.CreatedDate.Value : new DateTime()
                    };
                }
                //gan validationCode thanh cong
                result.ValidationCode = WCFConstant.RESPONSE_SUCCESS;
                //gan gia tri thuoc tinh dinh kem 
                result.ObjSupervisingDept = supervisingDeptDefault;
            }
            catch (Exception ex)
            {
                //gan validationCode Exception
                result.ValidationCode = WCFConstant.RESPONSE_EXCEPTION;
            }
            return result;
        }
        #endregion

        /// <summary>
        /// Lấy thông tin nhà trường trực thuộc đơn vị -không phân trang từng phần
        /// </summary>
        /// <param name="EducationGrade"></param>
        /// <param name="SuperVisingDeptID"></param>
        /// <param name="SchoolName"></param>
        /// <param name="PageNum"></param>
        /// <param name="PageSize"></param>
        /// <returns></returns>
        public ListSchoolProfileLiteResponse GetSchoolByRegionNotPaging(int EducationGrade, int SupervisingDeptID, string SchoolName)
        {
            ListSchoolProfileLiteResponse objListSchoolProfileLiteResponse = new ListSchoolProfileLiteResponse();
            try
            {
                //Khoi tao Business
                DeclareBusiness();
                // Lay danh sach tat cac don vi quan ly thuoc quan ly cua don vi truyen vao hoac chinh no
                string traversalPath = "\\" + SupervisingDeptID + "\\";
                IQueryable<SupervisingDept> listSupervisingDept = SupervisingDeptBusiness.AllNoTracking
                    .Where(o => o.IsActive 
                        && (o.TraversalPath.Contains(traversalPath) || o.SupervisingDeptID == SupervisingDeptID));
                IQueryable<SchoolProfile> iQSchool = SchoolProfileBusiness.AllNoTracking
                    .Where(o => listSupervisingDept.Any(s => s.SupervisingDeptID == o.SupervisingDeptID) 
					&& o.IsActiveSMAS == true 
					&& !string.IsNullOrEmpty(o.HeadMasterPhone.Trim())
					 && o.IsActive == true
					);

                EducationGrade = EducationGrade > 0 ? Utils.GradeToBinary(EducationGrade) : (int)0;
                if (EducationGrade > 0)
                {
                    iQSchool = iQSchool.Where(o => (o.EducationGrade & EducationGrade) != 0);
                }

                if (!string.IsNullOrWhiteSpace(SchoolName))
                {
                    iQSchool = iQSchool.Where(o => o.SchoolName.ToLower().Contains(SchoolName.Trim().ToLower()));
                }

                int totalRecord = iQSchool.Count();
                List<SchoolProfileLite> listSchool = (iQSchool
                    .Select(o => new SchoolProfileLite
                    {
                        SchoolID = o.SchoolProfileID,
                        SchoolName = o.SchoolName,
                        HeadMasterName = o.HeadMasterName,
                        HeadMasterPhone = o.HeadMasterPhone,
                        DistrictName = o.District.DistrictName
                    })).ToList();

                //gan validationCode thanh cong
                objListSchoolProfileLiteResponse.ValidationCode = WCFConstant.RESPONSE_SUCCESS;
                //gan gia tri thuoc tinh dinh kem 
                objListSchoolProfileLiteResponse.LstSchoolProfileLite = listSchool;
            }
            catch (Exception)
            {
                //gan validationCode exception
                objListSchoolProfileLiteResponse.ValidationCode = WCFConstant.RESPONSE_EXCEPTION;
            }
            return objListSchoolProfileLiteResponse;
        }


        #region Lấy danh sách ĐVGD làm HĐ của Hệ thống dành cho CTV/ĐL
        /// <summary>
        /// Lay các ĐVGD theo Tỉnh/thành, Quận/Huyện
        /// </summary>
        /// <param name="ProvinceID"></param>
        /// <param name="DistrictID"></param>
        /// <returns></returns>
        public LstEducationUnitResponse GetAllEducationUnit(int ProvinceID, int DistrictID)
        {
            LstEducationUnitResponse objLstEducationUnitResponse = new LstEducationUnitResponse();
            try
            {
                //Khoi tao Business
                DeclareBusiness();
                List<EducationUnitResponse> ListResponse = new List<EducationUnitResponse>();
                IQueryable<SupervisingDept> querySuper = SupervisingDeptBusiness.All;
                IQueryable<SchoolProfile> querySchool = SchoolProfileBusiness.All;

                if (ProvinceID > 0)
                {
                    querySuper = querySuper.Where(o => o.ProvinceID == ProvinceID);
                    querySchool = querySchool.Where(o => o.ProvinceID == ProvinceID);
                }
                if (DistrictID > 0)
                {
                    querySuper = querySuper.Where(o => o.DistrictID == DistrictID);
                    querySchool = querySchool.Where(o => o.DistrictID == DistrictID);
                }

                // Lay thong tin So (thuoc ca Quan/Huyen)
                ListResponse.AddRange(querySuper.Where(o => o.AdminID > 0 && o.IsActive == true 
                                    && o.HierachyLevel == GlobalConstants.EDUCATION_HIERACHY_LEVEL_PROVINCE_OFFICE)
                                .Select(o => new EducationUnitResponse {
                                    EducationUnitID = o.SupervisingDeptID,
                                    EducationUnitCode = o.SupervisingDeptCode,
                                    EducationUnitName = o.SupervisingDeptName,
                                    EducationUnitType = 3,
                                    EducationUnitUserName = o.UserAccount.aspnet_Users.UserName,
                                    Email = o.Email,
                                    HierachyLevel = o.HierachyLevel,
                                    ParentID = o.ParentID.HasValue ? o.ParentID.Value : 0,
                                    EstablishedDate = o.CreatedDate.HasValue ? o.CreatedDate.Value : DateTime.Now,
                                    AreaID = o.AreaID.HasValue ? o.AreaID.Value : 0,
                                    ProvinceID = o.ProvinceID.HasValue ? o.ProvinceID.Value : 0,
                                    DistrictID = o.DistrictID.HasValue ? o.DistrictID.Value : 0,
                                    Address = o.Address,
                                    Telephone = o.Telephone
                                }).ToList());
                
                
                // Lay thong tin phong quan ly
                ListResponse.AddRange(querySuper.Where(o => o.AdminID > 0  && o.IsActive == true 
                                && o.HierachyLevel == GlobalConstants.EDUCATION_HIERACHY_LEVEL_DISTRICT_OFFICE)
                                .Select(o => new EducationUnitResponse()
                                {
                                    EducationUnitID = o.SupervisingDeptID,
                                    EducationUnitCode = o.SupervisingDeptCode,
                                    EducationUnitName = o.SupervisingDeptName,
                                    EducationUnitType = 2,
                                    EducationUnitUserName = o.UserAccount.aspnet_Users.UserName,
                                    Email = o.Email,
                                    HierachyLevel = (short)o.HierachyLevel,
                                    ParentID = o.ParentID.HasValue ? o.ParentID.Value : 0,
                                    EstablishedDate = o.CreatedDate.HasValue ? o.CreatedDate.Value : DateTime.Now,
                                    AreaID = o.AreaID.HasValue ? o.AreaID.Value : 0,
                                    ProvinceID = o.ProvinceID.HasValue ? o.ProvinceID.Value : 0,
                                    DistrictID = o.DistrictID.HasValue ? o.DistrictID.Value : 0,
                                    Address = o.Address,
                                    Telephone = o.Telephone
                                }).ToList());

                // Lay thong tin truong
                ListResponse.AddRange(querySchool.Where(o => o.IsActive == true && o.AdminID > 0).Select(o => new EducationUnitResponse()
                               {
                                   EducationUnitID = o.SchoolProfileID,
                                   EducationUnitCode = o.SchoolCode,
                                   EducationUnitName = o.SchoolName,
                                   EducationUnitType = 1,
                                   EducationUnitUserName = o.UserAccount.aspnet_Users.UserName,
                                   Email = o.Email,
                                   EstablishedDate = o.EstablishedDate.HasValue ? o.EstablishedDate.Value : DateTime.Now,
                                   AreaID = o.AreaID.HasValue ? o.AreaID.Value : 0,
                                   ProvinceID = o.ProvinceID.HasValue ? o.ProvinceID.Value : 0,
                                   DistrictID = o.DistrictID.HasValue ? o.DistrictID.Value : 0,
                                   CommuneID = o.CommuneID.HasValue ? o.CommuneID.Value : 0,
                                   Address = o.Address,
                                   EducationUnitOwnerName = o.HeadMasterName,
                                   EducationUnitOwnerPhone = o.HeadMasterPhone,
                                   Telephone = o.Telephone,
                                   EducationGrade = o.EducationGrade
                               }).ToList());

                //gan validationCode thanh cong
                objLstEducationUnitResponse.ValidationCode = WCFConstant.RESPONSE_SUCCESS;
                //gan gia tri thuoc tinh dinh kem 
                objLstEducationUnitResponse.ListEducationUnitResponse = ListResponse;
            }
            catch (Exception ex)
            {
                //gan validationCode exception
                objLstEducationUnitResponse.ValidationCode = WCFConstant.RESPONSE_EXCEPTION;
            }
            return objLstEducationUnitResponse;
        }
        public LstEducationUnitResponse GetAllEducationUnitPaging(int ProvinceID, int DistrictID, int page, int size)
        {
            LstEducationUnitResponse objLstEducationUnitResponse = new LstEducationUnitResponse();
            try
            {
                //Khoi tao Business
                DeclareBusiness();
                IQueryable<EducationUnitResponse> ListResponse;
                IQueryable<SupervisingDept> querySuper = SupervisingDeptBusiness.All;
                IQueryable<SchoolProfile> querySchool = SchoolProfileBusiness.All;

                if (ProvinceID > 0)
                {
                    querySuper = querySuper.Where(o => o.ProvinceID == ProvinceID);
                    querySchool = querySchool.Where(o => o.ProvinceID == ProvinceID);
                }
                if (DistrictID > 0)
                {
                    querySuper = querySuper.Where(o => o.DistrictID == DistrictID);
                    querySchool = querySchool.Where(o => o.DistrictID == DistrictID);
                }

                // Lay thong tin So (thuoc ca Quan/Huyen)
                ListResponse = querySuper.Where(o => o.AdminID > 0 && o.IsActive == true
                                    && o.HierachyLevel == GlobalConstants.EDUCATION_HIERACHY_LEVEL_PROVINCE_OFFICE)
                                .Select(o => new EducationUnitResponse
                                {
                                    EducationUnitID = o.SupervisingDeptID,
                                    EducationUnitCode = o.SupervisingDeptCode,
                                    EducationUnitName = o.SupervisingDeptName,
                                    EducationUnitType = 3,
                                    EducationUnitUserName = o.UserAccount.aspnet_Users.UserName,
                                    Email = o.Email,
                                    HierachyLevel = o.HierachyLevel,
                                    ParentID = o.ParentID.HasValue ? o.ParentID.Value : 0,
                                    EstablishedDate = o.CreatedDate.HasValue ? o.CreatedDate.Value : DateTime.Now,
                                    AreaID = o.AreaID.HasValue ? o.AreaID.Value : 0,
                                    ProvinceID = o.ProvinceID.HasValue ? o.ProvinceID.Value : 0,
                                    DistrictID = o.DistrictID.HasValue ? o.DistrictID.Value : 0,
                                    Address = o.Address,
                                    Telephone = o.Telephone
                                });


                // Lay thong tin phong quan ly
                ListResponse.Union(querySuper.Where(o => o.AdminID > 0 && o.IsActive == true
                                && o.HierachyLevel == GlobalConstants.EDUCATION_HIERACHY_LEVEL_DISTRICT_OFFICE)
                                .Select(o => new EducationUnitResponse()
                                {
                                    EducationUnitID = o.SupervisingDeptID,
                                    EducationUnitCode = o.SupervisingDeptCode,
                                    EducationUnitName = o.SupervisingDeptName,
                                    EducationUnitType = 2,
                                    EducationUnitUserName = o.UserAccount.aspnet_Users.UserName,
                                    Email = o.Email,
                                    HierachyLevel = (short)o.HierachyLevel,
                                    ParentID = o.ParentID.HasValue ? o.ParentID.Value : 0,
                                    EstablishedDate = o.CreatedDate.HasValue ? o.CreatedDate.Value : DateTime.Now,
                                    AreaID = o.AreaID.HasValue ? o.AreaID.Value : 0,
                                    ProvinceID = o.ProvinceID.HasValue ? o.ProvinceID.Value : 0,
                                    DistrictID = o.DistrictID.HasValue ? o.DistrictID.Value : 0,
                                    Address = o.Address,
                                    Telephone = o.Telephone
                                }));

                // Lay thong tin truong
                ListResponse.Union(querySchool.Where(o => o.IsActive == true && o.AdminID > 0).Select(o => new EducationUnitResponse()
                {
                    EducationUnitID = o.SchoolProfileID,
                    EducationUnitCode = o.SchoolCode,
                    EducationUnitName = o.SchoolName,
                    EducationUnitType = 1,
                    EducationUnitUserName = o.UserAccount.aspnet_Users.UserName,
                    Email = o.Email,
                    EstablishedDate = o.EstablishedDate.HasValue ? o.EstablishedDate.Value : DateTime.Now,
                    AreaID = o.AreaID.HasValue ? o.AreaID.Value : 0,
                    ProvinceID = o.ProvinceID.HasValue ? o.ProvinceID.Value : 0,
                    DistrictID = o.DistrictID.HasValue ? o.DistrictID.Value : 0,
                    CommuneID = o.CommuneID.HasValue ? o.CommuneID.Value : 0,
                    Address = o.Address,
                    EducationUnitOwnerName = o.HeadMasterName,
                    EducationUnitOwnerPhone = o.HeadMasterPhone,
                    Telephone = o.Telephone,
                    EducationGrade = o.EducationGrade
                }));

                //gan validationCode thanh cong
                objLstEducationUnitResponse.ValidationCode = WCFConstant.RESPONSE_SUCCESS;
                //gan gia tri thuoc tinh dinh kem 
                objLstEducationUnitResponse.ListEducationUnitResponse = ListResponse.Skip((page - 1) * size).Take(size).ToList();
            }
            catch (Exception ex)
            {
                //gan validationCode exception
                objLstEducationUnitResponse.ValidationCode = WCFConstant.RESPONSE_EXCEPTION;
            }
            return objLstEducationUnitResponse;
        }

        public bool ActiveSMAS(string userName)
        {
            try
            {
                DeclareBusiness();
                MembershipUser objMembershipUser;
                objMembershipUser = Membership.GetUser(userName);
                if (objMembershipUser != null)
                {
                    Guid userId = (Guid)objMembershipUser.ProviderUserKey;
                    UserAccount objUserAccount = UserAccountBusiness.GetUserByProviderUserKey(userId);
                    if (objUserAccount.IsAdmin)
                    {
                        objMembershipUser.IsApproved = true;
                        SchoolProfile sp = SchoolProfileBusiness.All.FirstOrDefault(p => p.AdminID == objUserAccount.UserAccountID);
                        if (sp != null)
                        {
                            sp.IsActiveSMAS = true;
                            Membership.Provider.UpdateUser(objMembershipUser);
                            SchoolProfileBusiness.Save();
                            return true;
                        }
                        else
                        {
                            SupervisingDept supervisingDept = SupervisingDeptBusiness.All.FirstOrDefault(p => p.AdminID == objUserAccount.UserAccountID);
                            if (supervisingDept != null)
                            {
                                supervisingDept.IsActiveSMAS = true;
                                Membership.Provider.UpdateUser(objMembershipUser);
                                SupervisingDeptBusiness.Save();
                                return true;
                            }
                        }
                        
                    }
                }
            }
            catch (Exception ex)
            {
                return false;
            }
            return false;
        }
        #endregion
    }
}