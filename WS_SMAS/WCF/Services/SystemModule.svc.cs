﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Security;
using SMAS.Business.Common;
using SMAS.Business.IBusiness;
using SMAS.Models.Models;
using WCF.Composite;
using log4net;
using SMAS.Business.Business;
using System.Data.Objects;
using WCF.Composite.Sync;
using SMAS.VTUtils.Log;

namespace WCF
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the class name "SystemModule" in code, svc and config file together.
    // NOTE: In order to launch WCF Test Client for testing this service, please select SystemModule.svc or SystemModule.svc.cs at the Solution Explorer and start debugging.
    public partial class SystemModule : ISystemModule
    {
        #region Declare Variable
        private IClassProfileBusiness ClassProfileBusiness;
        private IEmployeeBusiness EmployeeBusiness;
        private IClassSubjectBusiness ClassSubjectBusiness;
        private ICalendarBusiness CalendarBusiness;
        private ISemeterDeclarationBusiness SemesterDeclarationBusiness;
        private IEducationLevelBusiness EducationLevelBusiness;
        private ISchoolProfileBusiness SchoolProfileBusiness;
        private IPeriodDeclarationBusiness PeriodDeclarationBusiness;
        private IUserAccountBusiness UserAccountBusiness;
        private ISupervisingDeptBusiness SupervisingDeptBusiness;
        private IAcademicYearBusiness AcademicYearBusiness;
        private ISubjectCatBusiness SubjectCatBusiness;
        private ITeachingRegistrationBusiness TeachingRegistrationBusiness;
        private ICalendarScheduleBusiness CalendarScheduleBusiness;
        private Iaspnet_UsersBusiness Aspnet_UsersBusiness;
        private IUserGroupBusiness UserGroupBusiness;
        private IGroupMenuBusiness GroupMenuBusiness;
        private IProvinceBusiness ProvinceBusiness;
        private IDistrictBusiness DistrictBusiness;
        private IMIdmappingBusiness MIdmappingBusiness;
        private IMonthCommentsBusiness MonthCommentsBusiness;
        private ILockMarkPrimaryBusiness LockMarkPrimaryBusiness;
        private IPushNotifyRequestBusiness PushNotifyRequestBusiness;
        private IApplicationConfigBusiness ApplicationConfigBusiness;
        private ISchoolFacultyBusiness SchoolFacultyBusiness;
        private IAreaBusiness AreaBusiness;
        private IExaminationsBusiness ExaminationsBusiness;
        private IExamSubjectBusiness ExamSubjectBusiness;
        private Iaspnet_MembershipBusiness aspnet_MembershipBusiness;
        private static readonly log4net.ILog logger = LogManager.GetLogger(typeof(SystemModule));

        private SMASEntities context;

        public SystemModule()
        {
            DeclareBusiness();
        }
        /// <summary>
        /// Khoi tao business cung context
        /// </summary>
        private void DeclareBusiness()
        {
            context = new SMASEntities();
            ClassProfileBusiness = new ClassProfileBusiness(logger, context);
            EmployeeBusiness = new EmployeeBusiness(logger, context);
            ClassSubjectBusiness = new ClassSubjectBusiness(logger, context);
            CalendarBusiness = new CalendarBusiness(logger, context);
            EducationLevelBusiness = new EducationLevelBusiness(logger, context);
            SchoolProfileBusiness = new SchoolProfileBusiness(logger, context);
            PeriodDeclarationBusiness = new PeriodDeclarationBusiness(logger, context);
            UserAccountBusiness = new UserAccountBusiness(logger, context);
            SupervisingDeptBusiness = new SupervisingDeptBusiness(logger, context);
            AcademicYearBusiness = new AcademicYearBusiness(logger, context);
            SubjectCatBusiness = new SubjectCatBusiness(logger, context);
            TeachingRegistrationBusiness = new TeachingRegistrationBusiness(logger, context);
            CalendarScheduleBusiness = new CalendarScheduleBusiness(logger, context);
            SemesterDeclarationBusiness = new SemeterDeclarationBusiness(logger, context);
            Aspnet_UsersBusiness = new aspnet_UsersBusiness(logger, context);
            UserGroupBusiness = new UserGroupBusiness(logger, context);
            GroupMenuBusiness = new GroupMenuBusiness(logger, context);
            ProvinceBusiness = new ProvinceBusiness(logger, context);
            DistrictBusiness = new DistrictBusiness(logger, context);
            MIdmappingBusiness = new MIdmappingBusiness(logger, context);
            MonthCommentsBusiness = new MonthCommentsBusiness(logger, context);
            LockMarkPrimaryBusiness = new LockMarkPrimaryBusiness(logger, context);
            PushNotifyRequestBusiness = new PushNotifyRequestBusiness(logger, context);
            ApplicationConfigBusiness = new ApplicationConfigBusiness(logger, context);
            ExaminationsBusiness = new ExaminationsBusiness(logger, context);
            ExamSubjectBusiness = new ExamSubjectBusiness(logger, context);
            SchoolFacultyBusiness = new SchoolFacultyBusiness(logger, context);
            AreaBusiness = new AreaBusiness(logger, context);
            aspnet_MembershipBusiness = new aspnet_MembershipBusiness(logger, context);
        }
        #endregion

        /// <summary>
        /// Lấy danh sách lớp học của trường trong năm học
        /// </summary>
        /// <param name="SchoolID"></param>
        /// <param name="AcademicYearID"></param>
        /// <returns>ObjectClassProfileLiteResponse</returns>
        public ListClassProfileLiteResponse GetListClassBySchool(int SchoolID, int AcademicYearID)
        {
            ListClassProfileLiteResponse objClassProfileLiteResponse = new ListClassProfileLiteResponse();
            try
            {
                List<ClassProfileLite> listClass = ClassProfileBusiness.SearchBySchool(SchoolID, new Dictionary<string, object>(){
                {"AcademicYearID", AcademicYearID}
                }).Select(o => new ClassProfileLite
                {
                    Grade = o.EducationLevel.Grade,
                    EducationLevelID = o.EducationLevelID,
                    ClassProfileID = o.ClassProfileID,
                    ClassName = o.DisplayName,
                    OrderNumber = o.OrderNumber.HasValue ? o.OrderNumber.Value : 0,
                    IsVNEN = o.IsVnenClass.HasValue ? o.IsVnenClass.Value : false
                }).ToList();
                //gan validationCode thanh cong
                objClassProfileLiteResponse.ValidationCode = WCFConstant.RESPONSE_SUCCESS;
                //gan gia tri thuoc tinh dinh kem 
                objClassProfileLiteResponse.LstClassProfileLite = listClass.OrderBy(c => c.OrderNumber).ThenBy(c => c.ClassName).ToList();
            }
            catch (Exception)
            {
                //gan validationCode exception
                objClassProfileLiteResponse.ValidationCode = WCFConstant.RESPONSE_EXCEPTION;
            }
            return objClassProfileLiteResponse;
        }

        /// <summary>
        /// Lấy thông tin lớp học theo ID
        /// </summary>
        /// <param name="ClassID"></param>
        /// <returns></returns>
        public ClassProfileLiteResponse GetClassProfileByID(int ClassID)
        {
            //Khai bao Doi tuong tra ve
            ClassProfileLiteResponse objClassProfileLiteResponse = new ClassProfileLiteResponse();
            //Khai bao doi tuong thuoc tinh
            ClassProfileLite objClassProfileLite = new ClassProfileLite();
            try
            {
                ClassProfile cp = ClassProfileBusiness.Find(ClassID);
                if (cp != null)
                {
                    objClassProfileLite = new ClassProfileLite()
                    {
                        Grade = cp.EducationLevel.Grade,
                        EducationLevelID = cp.EducationLevelID,
                        ClassProfileID = cp.ClassProfileID,
                        ClassName = cp.DisplayName,
                        OrderNumber = cp.OrderNumber.HasValue ? cp.OrderNumber.Value : 0,
                        ClassSection = cp.Section
                    };
                }
                //Gan ma thanh cong
                objClassProfileLiteResponse.ValidationCode = WCFConstant.RESPONSE_SUCCESS;
                //Gan object
                objClassProfileLiteResponse.ObjClassProfileLite = objClassProfileLite;
            }
            catch
            {
                //gan ma exception
                objClassProfileLiteResponse.ValidationCode = WCFConstant.RESPONSE_EXCEPTION;
            }
            return objClassProfileLiteResponse;
        }

        /// <summary>
        /// Lấy danh sách lớp học theo quyền giáo viên chủ nhiệm
        /// </summary>
        /// <param name="SchoolID"></param>
        /// <param name="AcademicYearID"></param>
        /// <param name="TeacherID"></param>
        /// <returns>object voi validationCode</returns>
        public ListClassProfileLiteResponse GetListClassByHeadTeacher(int SchoolID, int AcademicYearID, int TeacherID)
        {
            ListClassProfileLiteResponse objListClassProfileLiteResponse = new ListClassProfileLiteResponse();
            try
            {
                List<ClassProfileLite> listClass = ClassProfileBusiness.SearchBySchool(SchoolID, new Dictionary<string, object>(){
                {"AcademicYearID", AcademicYearID},
                {"TeacherByRoleID", TeacherID},
                {"Type", SystemParamsInFile.SUPERVISING_PERMISSION_HEAD_TEACHER}
                }).Select(o => new ClassProfileLite
                {
                    Grade = o.EducationLevel.Grade,
                    EducationLevelID = o.EducationLevelID,
                    ClassProfileID = o.ClassProfileID,
                    ClassName = o.DisplayName,
                    OrderNumber = o.OrderNumber.HasValue ? o.OrderNumber.Value : 0
                }).ToList();
                objListClassProfileLiteResponse.ValidationCode = WCFConstant.RESPONSE_SUCCESS;
                objListClassProfileLiteResponse.LstClassProfileLite = listClass;
            }
            catch
            {
                objListClassProfileLiteResponse.ValidationCode = WCFConstant.RESPONSE_EXCEPTION;
            }
            return objListClassProfileLiteResponse;
        }

        /// <summary>
        /// Lay danh sach lop hoc GVCN/GCBM
        /// </summary>
        /// <param name="SchoolID"></param>
        /// <param name="AcademicYearID"></param>
        /// <param name="AppliedLevel"></param>
        /// <param name="UserAccountID"></param>
        /// <returns></returns>
        public ListClassProfileLiteResponse GetListClassByRoleHeadSubjectTeacher(int SchoolID, int AcademicYearID, int AppliedLevel, int UserAccountID)
        {
            ListClassProfileLiteResponse objListClassProfileLiteResponse = new ListClassProfileLiteResponse();
            try
            {
                List<ClassProfileLite> listClass = ClassProfileBusiness.SearchBySchool(SchoolID, new Dictionary<string, object>(){
                {"AcademicYearID", AcademicYearID},
                {"AppliedLevel", AppliedLevel},
                {"UserAccountID", UserAccountID},
                {"Type", SystemParamsInFile.TEACHER_ROLE_HEAD_SUBJECTTEACHER}
                }).Select(o => new ClassProfileLite
                {
                    Grade = o.EducationLevel.Grade,
                    EducationLevelID = o.EducationLevelID,
                    ClassProfileID = o.ClassProfileID,
                    ClassName = o.DisplayName,
                    OrderNumber = o.OrderNumber.HasValue ? o.OrderNumber.Value : 0
                }).ToList();
                objListClassProfileLiteResponse.ValidationCode = WCFConstant.RESPONSE_SUCCESS;
                objListClassProfileLiteResponse.LstClassProfileLite = listClass;
            }
            catch
            {
                objListClassProfileLiteResponse.ValidationCode = WCFConstant.RESPONSE_EXCEPTION;
            }
            return objListClassProfileLiteResponse;
        }
        /// <summary>
        /// Lấy danh sách môn học theo lớp
        /// </summary>
        /// <param name="SchoolID"></param>
        /// <param name="ClassID"></param>
        /// <returns></returns>
        public ListSubjectCatLiteResponse GetListSubjectInClass(int SchoolID, int ClassID)
        {
            ListSubjectCatLiteResponse objListSubjectCatLiteResponse = new ListSubjectCatLiteResponse();
            try
            {
                List<SubjectCatLite> listSubject = ClassSubjectBusiness.SearchBySchool(SchoolID, new Dictionary<string, object>(){
                {"ClassID", ClassID}
                }).Select(o => new SubjectCatLite
                {
                    SubjectID = o.SubjectID,
                    SubjectName = o.SubjectCat.DisplayName
                }).ToList();
                //Gan validation code
                objListSubjectCatLiteResponse.ValidationCode = WCFConstant.RESPONSE_SUCCESS;
                //gan gia tri thuoc tinh dinh kem 
                objListSubjectCatLiteResponse.LstSubjectCatLite = listSubject;
            }
            catch
            {
                //Gan validation code
                objListSubjectCatLiteResponse.ValidationCode = WCFConstant.RESPONSE_EXCEPTION;
            }
            return objListSubjectCatLiteResponse;
        }

        /// <summary>
        /// Lấy thông tin thời khóa biểu theo lớp
        /// </summary>
        /// <param name="SchoolID"></param>
        /// <param name="AcademicYearID"></param>
        /// <param name="ClassID"></param>
        /// <returns></returns>
        public StringResultResponse GetListCalendarOfPupil(int AcademicYearID, int ClassID, int Semester)
        {
            StringResultResponse objResult = new StringResultResponse();
            try
            {
                var listCalendar = CalendarBusiness.All.Where(o => o.AcademicYearID == AcademicYearID
                    && o.ClassID == ClassID
                    && o.Semester == Semester && o.IsActive)
                    .Select(o => new
                    {
                        o.SubjectCat.DisplayName,
                        o.Section,
                        o.DayOfWeek,
                        o.SubjectOrder
                    }).ToList();
                string calendarSchedule = "";
                // Các thứ lớp có học
                List<int> listDayOfWeek = listCalendar.Select(o => o.DayOfWeek).Distinct().OrderBy(d => d).ToList();
                int count = listDayOfWeek.Count;
                for (int i = 0; i < count; i++)
                {
                    string calendarByDay = string.Empty;
                    // Thứ
                    int d = listDayOfWeek[i];
                    var listByDay = listCalendar.Where(o => o.DayOfWeek == d).ToList();
                    var listBySection = listByDay.Select(o => o.Section).Distinct().OrderBy(s => s).ToList();
                    calendarSchedule += "T" + d + "-";
                    foreach (int s in listBySection)
                    {
                        // Buổi
                        string sectionName = WCFUtils.GetSection(s);
                        calendarByDay += sectionName + ": ";
                        // Môn học
                        var listSubjectName = listByDay.Where(o => o.Section == s).OrderBy(o => o.SubjectOrder);
                        string subjectName = string.Empty;
                        int countSubject = 0;
                        int sumSubjectName = listSubjectName.Count();
                        int index = 0;
                        foreach (var item in listSubjectName)
                        {
                            index++;
                            if (!item.DisplayName.Equals(subjectName))
                            {
                                if (!string.IsNullOrEmpty(subjectName))
                                {
                                    if (countSubject > 1)
                                    {
                                        calendarByDay += string.Format("{0}({1}), ", subjectName, countSubject);
                                    }
                                    else
                                    {
                                        calendarByDay += string.Format("{0}, ", subjectName, countSubject);
                                    }
                                }
                                subjectName = item.DisplayName;
                                countSubject = 0;
                            }

                            if (subjectName == item.DisplayName)
                            {
                                countSubject++;
                            }

                            if (sumSubjectName == index)
                            {
                                if (countSubject > 1)
                                {
                                    calendarByDay += string.Format("{0}({1})", subjectName, countSubject);
                                }
                                else
                                {
                                    calendarByDay += string.Format("{0}", subjectName);
                                }
                            }
                        }

                        // Thêm vào dấu phân cách
                        calendarByDay += "; ";
                    }

                    calendarSchedule += calendarByDay;
                    if (i != count - 1)
                    {
                        // Xuống dòng với mỗi thứ
                        calendarSchedule = calendarSchedule.Substring(0, calendarSchedule.Length - 2);
                        calendarSchedule += "\n";
                    }
                }
                objResult.ValidationCode = WCFConstant.RESPONSE_SUCCESS;
                calendarSchedule = calendarSchedule.Substring(0, calendarSchedule.Length - 2);
                objResult.Result = calendarSchedule;
                return objResult;
            }
            catch
            {
                //Neu loi tra ve chinh ma loi
                objResult.ValidationCode = WCFConstant.RESPONSE_EXCEPTION;
                objResult.Result = "";
                return objResult;
            }

        }

        public ListCalendarResponse GetListCalendar(int academicYearID, int classID, int semesterID)
        {
            ListCalendarResponse lstCalendarResponse = new ListCalendarResponse();
            try
            {
                List<CalendarLite> lstCalendar = (from c in CalendarBusiness.All
                                                  join s in SubjectCatBusiness.All on c.SubjectID equals s.SubjectCatID
                                                  where c.AcademicYearID == academicYearID
                                                  && c.ClassID == classID && (c.Semester == semesterID || semesterID == 0) && c.IsActive
                                                  select new CalendarLite
                                                  {
                                                      DayOfWeek = c.DayOfWeek,
                                                      Section = c.Section,
                                                      SubjectOrder = c.SubjectOrder,
                                                      NumberOfPeriod = c.NumberOfPeriod,
                                                      SubjectName = s.SubjectName,
                                                      Color = s.Color
                                                  }).ToList();
                lstCalendarResponse.ValidationCode = WCFConstant.RESPONSE_SUCCESS;
                lstCalendarResponse.LstCalendar = lstCalendar;
            }
            catch
            {
                lstCalendarResponse.ValidationCode = WCFConstant.RESPONSE_EXCEPTION;
            }
            return lstCalendarResponse;
        }

        /// <summary>
        /// Lấy danh sách khối học theo lớp
        /// </summary>
        /// <param name="Grade"></param>
        /// <returns></returns>
        public ListEducationLevelResponse GetListLevel(int Grade)
        {
            ListEducationLevelResponse lstEducationLevelResponse = new ListEducationLevelResponse();
            try
            {
                List<EducationLevelDefault> listEducationLevel = EducationLevelBusiness.GetByGrade(Grade).Select(o =>
                   new EducationLevelDefault
                   {
                       EducationLevelID = o.EducationLevelID,
                       EducationLevelName = o.Resolution
                   }).OrderBy(o => o.EducationLevelID).ToList();
                //Gan validation code
                lstEducationLevelResponse.ValidationCode = WCFConstant.RESPONSE_SUCCESS;
                //gan gia tri thuoc tinh dinh kem 
                lstEducationLevelResponse.LstEducationLevelDefault = listEducationLevel;
            }
            catch (Exception ex)
            {
                lstEducationLevelResponse.ValidationCode = WCFConstant.RESPONSE_EXCEPTION;
            }
            return lstEducationLevelResponse;
        }

        /// <summary>
        /// Lấy danh sách lớp học theo năm học, cấp học, khối học
        /// </summary>
        /// <param name="AcademicYearID"></param>
        /// <param name="Grade"></param>
        /// <param name="EducationLevelID"></param>
        /// <returns></returns>
        public ListClassProfileLiteResponse GetListClass(int AcademicYearID, int EducationLevelID)
        {
            ListClassProfileLiteResponse objClassProfileLiteResponse = new ListClassProfileLiteResponse();
            //Khoi tao Business
            try
            {
                List<ClassProfileLite> listClass = ClassProfileBusiness.SearchByAcademicYear(AcademicYearID, new Dictionary<string, object>(){
                {"EducationLevelID", EducationLevelID}
                    }).Select(o => new ClassProfileLite
                    {
                        Grade = o.EducationLevel.Grade,
                        EducationLevelID = o.EducationLevelID,
                        ClassProfileID = o.ClassProfileID,
                        ClassName = o.DisplayName,
                        OrderNumber = o.OrderNumber.HasValue ? o.OrderNumber.Value : 0
                    }).OrderBy(o => o.EducationLevelID).ThenBy(o => o.OrderNumber).ThenBy(o => o.ClassName).ToList();
                //gan validationCode thanh cong
                objClassProfileLiteResponse.ValidationCode = WCFConstant.RESPONSE_SUCCESS;
                //gan gia tri thuoc tinh dinh kem 
                objClassProfileLiteResponse.LstClassProfileLite = listClass;
            }
            catch (Exception)
            {
                //gan validationCode exception
                objClassProfileLiteResponse.ValidationCode = WCFConstant.RESPONSE_EXCEPTION;
            }
            return objClassProfileLiteResponse;
        }

        /// <summary>
        /// Kiểm tra trường đã đăng ký sử dụng SMS Parent
        /// </summary>
        /// <param name="SchoolID"></param>
        /// <returns></returns>
        public BoolResultResponse IsSMSParentActive(int SchoolID)
        {
            BoolResultResponse objResult = new BoolResultResponse();
            try
            {
                SchoolProfile sp = SchoolProfileBusiness.Find(SchoolID);
                if (sp == null)
                {
                    //gan gia tri thuoc tinh dinh kem 
                    objResult.Result = false;
                }
                if (sp.SMSParentActiveType == SystemParamsInFile.COMMON_STATUS_ISACTIVE)
                {
                    //gan gia tri thuoc tinh dinh kem 
                    objResult.Result = true;
                }
                //gan validationCode thanh cong
                objResult.ValidationCode = WCFConstant.RESPONSE_SUCCESS;

            }
            catch
            {
                //gan validationCode Exception
                objResult.ValidationCode = WCFConstant.RESPONSE_EXCEPTION;
                //gan gia tri thuoc tinh dinh kem 
                objResult.Result = false;
            }
            return objResult;
        }

        /// <summary>
        /// Lấy danh sách đợt theo học kỳ
        /// Update 07/02/2014 - Neu truyen vao semester = 0 lay tat ca cac dot
        /// </summary>
        /// <param name="AcademicYearID"></param>
        /// <param name="Semester"></param>
        /// <returns></returns>
        public ListPeriodDeclarationResponse GetTimeMarkBySemester(int AcademicYearID, int Semester)
        {
            ListPeriodDeclarationResponse lstPeriodDeclarationResponse = new ListPeriodDeclarationResponse();
            try
            {
                IDictionary<string, object> SearchDic = new Dictionary<string, object>();
                SearchDic["AcademicYearID"] = AcademicYearID;
                //Neu truyen vao hoc ky = 0 thi lay tat ca cac dot cua nam
                if (Semester != 0)
                {
                    SearchDic["Semester"] = Semester;
                }
                List<PeriodDeclarationDefault> listPeriod = PeriodDeclarationBusiness.Search(SearchDic).Select(o => new PeriodDeclarationDefault
                {
                    AcademicYearID = o.AcademicYearID,
                    ContaintSemesterMark = o.ContaintSemesterMark.HasValue ? o.ContaintSemesterMark.Value : false,
                    EndDate = o.EndDate.HasValue ? o.EndDate.Value : DateTime.Now,
                    FromDate = o.FromDate.HasValue ? o.FromDate.Value : DateTime.Now,
                    InterviewMark = o.InterviewMark.HasValue ? o.InterviewMark.Value : 0,
                    IsLock = o.IsLock.HasValue ? o.IsLock.Value : false,
                    PeriodDeclarationID = o.PeriodDeclarationID,
                    Resolution = o.Resolution,
                    SchoolID = o.SchoolID.HasValue ? o.SchoolID.Value : 0,
                    Semester = o.Semester.HasValue ? o.Semester.Value : 0,
                    StartIndexOfInterviewMark = o.StartIndexOfInterviewMark.HasValue ? o.StartIndexOfInterviewMark.Value : 0,
                    StartIndexOfTwiceCoeffiecientMark = o.StartIndexOfTwiceCoeffiecientMark.HasValue ? o.StartIndexOfTwiceCoeffiecientMark.Value : 0,
                    StartIndexOfWritingMark = o.StartIndexOfWritingMark.HasValue ? o.StartIndexOfWritingMark.Value : 0,
                    TwiceCoeffiecientMark = o.TwiceCoeffiecientMark.HasValue ? o.TwiceCoeffiecientMark.Value : 0,
                    WritingMark = o.WritingMark.HasValue ? o.WritingMark.Value : 0,
                    Year = o.Year.HasValue ? o.Year.Value : 0
                }).ToList();
                //gan validationCode thanh cong
                lstPeriodDeclarationResponse.ValidationCode = WCFConstant.RESPONSE_SUCCESS;
                //gan gia tri thuoc tinh dinh kem 
                lstPeriodDeclarationResponse.LstPeriodDeclarationDefault = listPeriod;
            }
            catch
            {
                //gan validationCode Exception
                lstPeriodDeclarationResponse.ValidationCode = WCFConstant.RESPONSE_EXCEPTION;
            }
            return lstPeriodDeclarationResponse;
        }

        /// <summary>
        /// Lấy thông tin nhà trường theo ID
        /// </summary>
        /// <param name="SchoolID"></param>
        /// <returns></returns>
        public SchoolProfileResponse GetSchoolByID(int SchoolID)
        {
            SchoolProfileResponse lstSchoolProfileResponse = new SchoolProfileResponse();
            try
            {
                SchoolProfile sp = SchoolProfileBusiness.Find(SchoolID);
                SchoolProfileDefault spc = new SchoolProfileDefault()
                {
                    Address = sp.Address,
                    AdminID = sp.AdminID.HasValue ? sp.AdminID.Value : 0,
                    CommuneID = sp.CommuneID.HasValue ? sp.CommuneID.Value : 0,
                    CommuneName = sp.CommuneID.HasValue ? sp.Commune.CommuneName : string.Empty,
                    ProvinceID = sp.ProvinceID.HasValue ? sp.ProvinceID.Value : 0,
                    ProvinceName = sp.ProvinceID.HasValue ? sp.Province.ProvinceName : string.Empty,
                    DistrictID = sp.DistrictID.HasValue ? sp.DistrictID.Value : 0,
                    DistrictName = sp.DistrictID.HasValue ? sp.District.DistrictName : string.Empty,
                    EducationGrade = sp.EducationGrade,
                    Email = sp.Email,
                    EstablishedDate = sp.EstablishedDate.HasValue ? sp.EstablishedDate.Value : new DateTime(),
                    Fax = sp.Fax,
                    HeadMasterName = sp.HeadMasterName,
                    HeadMasterPhone = sp.HeadMasterPhone,

                    SchoolCode = sp.SchoolCode,
                    SchoolName = sp.SchoolName,
                    SchoolProfileID = sp.SchoolProfileID,
                    SchoolYearTitle = sp.SchoolYearTitle,
                    ShortName = sp.ShortName,
                    SupervisingDeptID = sp.SupervisingDeptID.HasValue ? sp.SupervisingDept.SupervisingDeptID : 0,
                    SupervisingDeptName = sp.SupervisingDeptID.HasValue ? sp.SupervisingDept.SupervisingDeptName : string.Empty,
                    Telephone = sp.Telephone,
                    Website = sp.Website,
                    SMSParentActiveType = sp.SMSParentActiveType,
                    SMSTeacherActiveType = sp.SMSTeacherActiveType,
                    IsNewSchoolModel = sp.IsNewSchoolModel.HasValue ? sp.IsNewSchoolModel.Value : false,
                    SchoolUserName = sp.UserAccount.aspnet_Users.UserName,
                    ProductVersion = (byte)(sp.ProductVersion.HasValue ? sp.ProductVersion.Value : GlobalConstants.PRODUCT_VERSION_ID_ADVANCE)
                };
                //gan validationCode thanh cong
                lstSchoolProfileResponse.ValidationCode = WCFConstant.RESPONSE_SUCCESS;
                //gan gia tri thuoc tinh dinh kem 
                lstSchoolProfileResponse.ObjSchoolProfileDefault = spc;
            }
            catch
            {
                //gan validationCode Exception
                lstSchoolProfileResponse.ValidationCode = WCFConstant.RESPONSE_EXCEPTION;
            }
            return lstSchoolProfileResponse;
        }

        /// <summary>
        /// Lấy thông tin nhà trường từ tên đăng nhập
        /// </summary>
        /// <param name="UserName"></param>
        /// <returns></returns>
        public SchoolProfileResponse GetSchoolByUserName(string UserName)
        {
            SchoolProfileResponse objSchoolProfileResponse = new SchoolProfileResponse();
            try
            {
                MembershipUser objMembershipUser;
                objMembershipUser = Membership.GetUser(UserName);
                Guid userId = (Guid)objMembershipUser.ProviderUserKey;
                UserAccount objUserAccount = UserAccountBusiness.GetUserByProviderUserKey(userId);
                SchoolProfile sp = null;
                // Nếu là admin trường
                if (objUserAccount.IsAdmin)
                {
                    sp = SchoolProfileBusiness.All.Where(o => o.AdminID == objUserAccount.UserAccountID).FirstOrDefault();
                }
                else
                {
                    // User bình thường
                    if (objUserAccount.EmployeeID.HasValue)
                    {
                        if (objUserAccount.Employee.SchoolID.HasValue)
                        {
                            sp = SchoolProfileBusiness.Find(objUserAccount.Employee.SchoolID.Value);
                        }
                    }
                }
                // Nếu có trường được tìm thấy
                SchoolProfileDefault spc = null;
                if (sp != null)
                {
                    spc = new SchoolProfileDefault()
                    {
                        Address = sp.Address,
                        AdminID = sp.AdminID.HasValue ? sp.AdminID.Value : 0,
                        AreaID = sp.AreaID.HasValue ? sp.AreaID.Value : 0,
                        CommuneID = sp.CommuneID.HasValue ? sp.CommuneID.Value : 0,
                        CommuneName = sp.CommuneID.HasValue ? sp.Commune.CommuneName : string.Empty,
                        DistrictID = sp.DistrictID.HasValue ? sp.DistrictID.Value : 0,
                        DistrictName = sp.DistrictID.HasValue ? sp.District.DistrictName : string.Empty,
                        EducationGrade = sp.EducationGrade,
                        Email = sp.Email,
                        EstablishedDate = sp.EstablishedDate.HasValue ? sp.EstablishedDate.Value : new DateTime(),
                        Fax = sp.Fax,
                        HeadMasterName = sp.HeadMasterName,
                        HeadMasterPhone = sp.HeadMasterPhone,
                        ProvinceID = sp.ProvinceID.HasValue ? sp.ProvinceID.Value : 0,
                        ProvinceName = sp.ProvinceID.HasValue ? sp.Province.ProvinceName : string.Empty,
                        SchoolCode = sp.SchoolCode,
                        SchoolName = sp.SchoolName,
                        SchoolProfileID = sp.SchoolProfileID,
                        SchoolYearTitle = sp.SchoolYearTitle,
                        ShortName = sp.ShortName,
                        SupervisingDeptID = sp.SupervisingDeptID.HasValue ? sp.SupervisingDept.SupervisingDeptID : 0,
                        SupervisingDeptName = sp.SupervisingDeptID.HasValue ? sp.SupervisingDept.SupervisingDeptName : string.Empty,
                        Telephone = sp.Telephone,
                        Website = sp.Website,
                        SMSParentActiveType = sp.SMSParentActiveType,
                        SMSTeacherActiveType = sp.SMSTeacherActiveType,
                        IsNewSchoolModel = sp.IsNewSchoolModel.HasValue ? sp.IsNewSchoolModel.Value : false,
                        SchoolUserName = sp.UserAccount.aspnet_Users.UserName,
                        ProductVersion = (byte)(sp.ProductVersion.HasValue ? sp.ProductVersion.Value : GlobalConstants.PRODUCT_VERSION_ID_ADVANCE),
                        VocationType = sp.TrainingType != null && sp.TrainingType.Resolution == "GDTX"
                    };
                }
                //gan validationCode thanh cong
                objSchoolProfileResponse.ValidationCode = WCFConstant.RESPONSE_SUCCESS;
                //gan gia tri thuoc tinh dinh kem 
                objSchoolProfileResponse.ObjSchoolProfileDefault = spc;
            }
            catch
            {
                //gan validationCode Exception
                objSchoolProfileResponse.ValidationCode = WCFConstant.RESPONSE_EXCEPTION;
            }
            return objSchoolProfileResponse;
        }

        #region lay truong theo schoolCode
        /// <summary>
        /// lay truong theo schoolCode
        /// </summary>
        /// <author date="2014/02/18">HaiVT</author>
        /// <param name="schoolCode">ma truong</param>
        /// <returns></returns>
        public SchoolProfileResponse GetSchoolBySchoolCode(string schoolCode)
        {
            SchoolProfileResponse lstSchoolProfileResponse = new SchoolProfileResponse();
            try
            {
                //IDictionary<string, object> dic = new Dictionary<string, object>();
                //dic["SchoolCode"] = schoolCode;
                SchoolProfile sp = SchoolProfileBusiness.All.Where(p => schoolCode.ToLower().Equals(p.SchoolCode.ToLower())).FirstOrDefault();
                if (sp != null)
                {
                    SchoolProfileDefault spc = new SchoolProfileDefault()
                    {
                        Address = sp.Address,
                        AdminID = sp.AdminID.HasValue ? sp.AdminID.Value : 0,
                        CommuneName = sp.CommuneID.HasValue ? sp.Commune.CommuneName : string.Empty,
                        DistrictID = sp.DistrictID.HasValue ? sp.DistrictID.Value : 0,
                        DistrictName = sp.DistrictID.HasValue ? sp.District.DistrictName : string.Empty,
                        EducationGrade = sp.EducationGrade,
                        Email = sp.Email,
                        EstablishedDate = sp.EstablishedDate.HasValue ? sp.EstablishedDate.Value : new DateTime(),
                        Fax = sp.Fax,
                        HeadMasterName = sp.HeadMasterName,
                        HeadMasterPhone = sp.HeadMasterPhone,
                        ProvinceID = sp.ProvinceID.HasValue ? sp.ProvinceID.Value : 0,
                        ProvinceName = sp.ProvinceID.HasValue ? sp.Province.ProvinceName : string.Empty,
                        SchoolCode = sp.SchoolCode,
                        SchoolName = sp.SchoolName,
                        SchoolProfileID = sp.SchoolProfileID,
                        SchoolYearTitle = sp.SchoolYearTitle,
                        ShortName = sp.ShortName,
                        SupervisingDeptID = sp.SupervisingDeptID.HasValue ? sp.SupervisingDept.SupervisingDeptID : 0,
                        SupervisingDeptName = sp.SupervisingDeptID.HasValue ? sp.SupervisingDept.SupervisingDeptName : string.Empty,
                        Telephone = sp.Telephone,
                        Website = sp.Website,
                        SMSParentActiveType = sp.SMSParentActiveType,
                        SMSTeacherActiveType = sp.SMSTeacherActiveType
                    };
                    //gan validationCode thanh cong
                    lstSchoolProfileResponse.ValidationCode = WCFConstant.RESPONSE_SUCCESS;
                    //gan gia tri thuoc tinh dinh kem 
                    lstSchoolProfileResponse.ObjSchoolProfileDefault = spc;
                }
                else
                {
                    lstSchoolProfileResponse.ValidationCode = WCFConstant.RESPONSE_EXCEPTION;
                }
            }
            catch
            {
                //gan validationCode Exception
                lstSchoolProfileResponse.ValidationCode = WCFConstant.RESPONSE_EXCEPTION;
            }
            return lstSchoolProfileResponse;
        }
        #endregion

        #region lấy hình đại điện của trường
        /// <summary>
        /// lấy hình đại điện của trường
        /// </summary>
        /// <param name="schoolID">id truong</param>
        /// <returns></returns>
        public ByteArrayResultResponse GetSchoolImage(int schoolID)
        {
            ByteArrayResultResponse objResult = new ByteArrayResultResponse();
            try
            {
                SchoolProfile schoolProfile = SchoolProfileBusiness.Find(schoolID);
                if (schoolProfile != null)
                {
                    //gan gia tri thuoc tinh dinh kem 
                    objResult.Result = schoolProfile.Image;
                }
                else
                {
                    objResult.Result = new byte[0]; //Tra ve mang byte 0 phan tu
                }
                //gan validationCode thanh cong
                objResult.ValidationCode = WCFConstant.RESPONSE_SUCCESS;
            }
            catch
            {
                //gan validationCode Exception
                objResult.ValidationCode = WCFConstant.RESPONSE_EXCEPTION;
                //gan gia tri thuoc tinh dinh kem 
                objResult.Result = new byte[0]; //Tra ve mang byte 0 phan tu
            }
            return objResult;
        }
        #endregion

        /// <summary>
        /// Lấy thông tin trong bảng UserAccount theo tên user
        /// </summary>
        /// <param name="UserName"></param>
        /// <returns></returns>
        public UserAccountResponse GetUserAccountByUserName(string UserName)
        {
            UserAccountResponse objUserAccountResponse = new UserAccountResponse();
            try
            {
                MembershipUser objMembershipUser;
                objMembershipUser = Membership.GetUser(UserName);
                Guid userId = (Guid)objMembershipUser.ProviderUserKey;
                UserAccount objUserAccount = UserAccountBusiness.GetUserByProviderUserKey(userId);
                Employee objEmployee = UserAccountBusiness.MapFromUserAccountToEmployee(objUserAccount.UserAccountID);
                UserAccountDefault uac = new UserAccountDefault()
                {
                    UserAccountID = objUserAccount.UserAccountID,
                    EmployeeID = objEmployee != null ? objEmployee.EmployeeID : 0,
                    EmployeeName = objEmployee != null ? objEmployee.FullName : string.Empty,
                    IsActive = objUserAccount.IsActive,
                    IsAdmin = objUserAccount.IsAdmin,
                    MembershipUserID = objUserAccount.GUID.Value
                };
                //gan validationCode thanh cong
                objUserAccountResponse.ValidationCode = WCFConstant.RESPONSE_SUCCESS;
                //gan gia tri thuoc tinh dinh kem 
                objUserAccountResponse.ObjUserAccount = uac;
            }
            catch
            {
                //gan validationCode Exception
                objUserAccountResponse.ValidationCode = WCFConstant.RESPONSE_EXCEPTION;
            }
            return objUserAccountResponse;
        }

        /// <summary>
        /// Kiểm tra Giáo viên có phải là BGH của trường
        /// </summary>
        /// <param name="TeacherID"></param>
        /// <returns></returns>
        public BoolResultResponse CheckPrincipleByID(int TeacherID)
        {
            BoolResultResponse objResult = new BoolResultResponse();
            try
            {
                Employee employee = EmployeeBusiness.Find(TeacherID);
                //gan gia tri thuoc tinh dinh kem 
                objResult.Result = false;
                if (employee == null)
                {
                    //gan gia tri thuoc tinh dinh kem 
                    objResult.Result = false;
                }
                if (employee.WorkGroupTypeID.HasValue && employee.WorkGroupTypeID.Value == SystemParamsInFile.WORK_GROUP_TYPE_BANGIAMHIEU)
                {
                    //gan gia tri thuoc tinh dinh kem 
                    objResult.Result = true;
                }
                //gan validationCode thanh cong
                objResult.ValidationCode = WCFConstant.RESPONSE_SUCCESS;

            }
            catch
            {
                //gan validationCode Exception
                objResult.ValidationCode = WCFConstant.RESPONSE_EXCEPTION;
                //gan gia tri thuoc tinh dinh kem 
                objResult.Result = false;
            }
            return objResult;
        }

        /// <summary>
        /// Lấy thông tin phòng sở theo ID
        /// </summary>
        /// <param name="SupervisingDeptID"></param>
        /// <returns></returns>
        public SupervisingDeptResponse GetSupervisingDeptByID(int SupervisingDeptID)
        {
            SupervisingDeptResponse objSupervisingDeptResponse = new SupervisingDeptResponse();
            try
            {
                SupervisingDept sd = SupervisingDeptBusiness.Find(SupervisingDeptID);
                SupervisingDeptDefault sdc = new SupervisingDeptDefault();
                if (sd != null)
                {
                    sdc = new SupervisingDeptDefault()
                    {
                        SupervisingDeptID = sd.SupervisingDeptID,
                        SupervisingDeptUserName = (sd.UserAccount != null) ? sd.UserAccount.aspnet_Users.LoweredUserName : string.Empty,
                        SupervisingDeptCode = sd.SupervisingDeptCode,
                        SupervisingDeptName = sd.SupervisingDeptName,
                        ProvinceID = sd.ProvinceID.HasValue ? sd.ProvinceID.Value : 0,
                        ProvinceName = sd.ProvinceID.HasValue ? sd.Province.ProvinceName : string.Empty,
                        DistrictID = sd.DistrictID.HasValue ? sd.DistrictID.Value : 0,
                        DistrictName = sd.DistrictID.HasValue ? sd.District.DistrictName : string.Empty,
                        AreaID = sd.AreaID.HasValue ? sd.AreaID.Value : 0,
                        AreaName = sd.AreaID.HasValue ? sd.Area.AreaName : string.Empty,
                        Address = sd.Address,
                        AdminID = sd.AdminID.HasValue ? sd.AdminID.Value : 0,
                        Email = sd.Email,
                        Fax = sd.Fax,
                        HierachyLevel = sd.HierachyLevel,
                        ParentID = sd.ParentID.HasValue ? sd.ParentID.Value : 0,

                        EstablishedDate = sd.CreatedDate.HasValue ? sd.CreatedDate.Value : new DateTime(),
                        Telephone = sd.Telephone,
                        Website = sd.Website,
                        TraversalPath = sd.TraversalPath,
                        IsActiveSMAS = (sd.IsActiveSMAS == null ? false : sd.IsActiveSMAS.Value),
                        SMSActiveType = (sd.SMSActiveType == null ? 0 : sd.SMSActiveType.Value)
                    };
                }
                //gan validationCode thanh cong
                objSupervisingDeptResponse.ValidationCode = WCFConstant.RESPONSE_SUCCESS;
                //gan gia tri thuoc tinh dinh kem 
                objSupervisingDeptResponse.ObjSupervisingDept = sdc;
            }
            catch (Exception ex)
            {
                //gan validationCode Exception
                objSupervisingDeptResponse.ValidationCode = WCFConstant.RESPONSE_EXCEPTION;
            }
            return objSupervisingDeptResponse;
        }

        /// <summary>
        /// Lấy thông tin năm học theo ID
        /// </summary>
        /// <param name="AcademicYearID"></param>
        /// <returns></returns>
        public AcademicYearResponse GetAcademicYearByID(int AcademicYearID)
        {
            AcademicYearResponse objAcademicYearResponse = new AcademicYearResponse();
            try
            {
                AcademicYear ac = AcademicYearBusiness.Find(AcademicYearID);
                AcademicYearDefault objAcademicYear = new AcademicYearDefault();
                if (ac != null)
                {
                    objAcademicYear = new AcademicYearDefault()
                    {
                        AcademicYearID = ac.AcademicYearID,
                        DisplayTitle = ac.DisplayTitle,
                        FirstSemesterEndDate = ac.FirstSemesterEndDate.Value,
                        FirstSemesterStartDate = ac.FirstSemesterStartDate.Value,
                        SchoolID = ac.SchoolID,
                        SecondSemesterEndDate = ac.SecondSemesterEndDate.Value,
                        SecondSemesterStartDate = ac.SecondSemesterStartDate.Value,
                        Year = ac.Year,
                        SchoolName = ac.SchoolProfile.SchoolName,
                        IsShowAvatar = ac.IsShowAvatar,
                        IsShowCalendar = ac.IsShowCalendar
                    };
                }
                //gan validationCode thanh cong
                objAcademicYearResponse.ValidationCode = WCFConstant.RESPONSE_SUCCESS;
                //gan gia tri thuoc tinh dinh kem 
                objAcademicYearResponse.ObjAcademicYear = objAcademicYear;
            }
            catch
            {
                //gan validationCode Exception
                objAcademicYearResponse.ValidationCode = WCFConstant.RESPONSE_EXCEPTION;
            }
            return objAcademicYearResponse;
        }

        #region lấy năm học của trường ứng với thời điểm hiện tại
        /// <summary>
        /// get academicYearID 
        /// </summary>
        /// <author date="2014/02/18">HaiVT</author>
        /// <param name="schoolID">id truong</param>
        /// <returns></returns>
        public AcademicYearResponse GetCurrentAcademicYearOfSchool(int schoolID)
        {
            AcademicYearResponse objAcademicYearResponse = new AcademicYearResponse();
            try
            {
                IDictionary<string, object> dic = new Dictionary<string, object>();
                dic["SchoolID"] = schoolID;
                AcademicYear ac = AcademicYearBusiness.Search(dic).FirstOrDefault(p =>
                    (EntityFunctions.TruncateTime(p.FirstSemesterStartDate) <= EntityFunctions.TruncateTime(DateTime.Now) && EntityFunctions.TruncateTime(p.FirstSemesterEndDate) >= EntityFunctions.TruncateTime(DateTime.Now))
                    || (EntityFunctions.TruncateTime(p.SecondSemesterStartDate) <= EntityFunctions.TruncateTime(DateTime.Now) && EntityFunctions.TruncateTime(p.SecondSemesterEndDate) >= EntityFunctions.TruncateTime(DateTime.Now)));
                AcademicYearDefault objAcademicYear = new AcademicYearDefault();
                if (ac != null)
                {
                    objAcademicYear = new AcademicYearDefault()
                    {
                        AcademicYearID = ac.AcademicYearID,
                        DisplayTitle = ac.DisplayTitle,
                        FirstSemesterEndDate = ac.FirstSemesterEndDate.Value,
                        FirstSemesterStartDate = ac.FirstSemesterStartDate.Value,
                        SchoolID = ac.SchoolID,
                        SecondSemesterEndDate = ac.SecondSemesterEndDate.Value,
                        SecondSemesterStartDate = ac.SecondSemesterStartDate.Value,
                        Year = ac.Year
                    };
                    //gan validationCode thanh cong
                    objAcademicYearResponse.ValidationCode = WCFConstant.RESPONSE_SUCCESS;
                    //gan gia tri thuoc tinh dinh kem 
                    objAcademicYearResponse.ObjAcademicYear = objAcademicYear;
                }
            }
            catch (Exception ex)
            {
                LogExtensions.GetCurrentClass = System.Reflection.MethodBase.GetCurrentMethod().DeclaringType;
                string para = string.Format("schoolID={0}", schoolID);
                LogExtensions.ErrorExt(logger, DateTime.Now, para, ex.Message, "null");
                //gan validationCode Exception
                objAcademicYearResponse.ValidationCode = WCFConstant.RESPONSE_EXCEPTION;
            }

            return objAcademicYearResponse;
        }
        #endregion

        #region lay nam hoc theo schoolID va Year
        /// <summary>
        /// lay nam hoc theo schoolID va Year
        /// </summary>
        /// <author date="2014/02/25">HaiVT</author>
        /// <param name="schoolID">id truong</param>
        /// <param name="year">nam hoc</param>
        /// <returns></returns>
        public AcademicYearResponse GetAcademicYearBySchoolIDAndYear(int schoolID, int year)
        {
            AcademicYearResponse objAcademicYearResponse = new AcademicYearResponse();
            try
            {
                IDictionary<string, object> dic = new Dictionary<string, object>();
                dic["SchoolID"] = schoolID;
                DateTime dateTimeNow = DateTime.Now.Date;
                AcademicYear ac = AcademicYearBusiness.Search(dic).Where(p => p.Year == year).FirstOrDefault();
                AcademicYearDefault objAcademicYear = new AcademicYearDefault();
                if (ac != null)
                {
                    objAcademicYear = new AcademicYearDefault()
                    {
                        AcademicYearID = ac.AcademicYearID,
                        DisplayTitle = ac.DisplayTitle,
                        FirstSemesterEndDate = ac.FirstSemesterEndDate.Value,
                        FirstSemesterStartDate = ac.FirstSemesterStartDate.Value,
                        SchoolID = ac.SchoolID,
                        SecondSemesterEndDate = ac.SecondSemesterEndDate.Value,
                        SecondSemesterStartDate = ac.SecondSemesterStartDate.Value,
                        Year = ac.Year
                    };
                    //gan validationCode thanh cong
                    objAcademicYearResponse.ValidationCode = WCFConstant.RESPONSE_SUCCESS;
                    //gan gia tri thuoc tinh dinh kem 
                    objAcademicYearResponse.ObjAcademicYear = objAcademicYear;
                }
            }
            catch (Exception ex)
            {
                LogExtensions.GetCurrentClass = System.Reflection.MethodBase.GetCurrentMethod().DeclaringType;
                string para = string.Format("schoolID={0}, year={1}", schoolID, year);
                LogExtensions.ErrorExt(logger, DateTime.Now, para, ex.Message, "null");
                //gan validationCode Exception
                objAcademicYearResponse.ValidationCode = WCFConstant.RESPONSE_EXCEPTION;
            }

            return objAcademicYearResponse;
        }
        #endregion

        /// <summary>
        /// Lấy thông tin cấp học của nhà trường
        /// </summary>
        /// <param name="SchoolID"></param>
        /// <returns></returns>
        public GradeResponse GetSchoolGradeBySchoolID(int SchoolID)
        {
            GradeResponse objGradeResponse = new GradeResponse();
            try
            {
                SchoolProfile sp = SchoolProfileBusiness.Find(SchoolID);
                int EducationGrade = sp.EducationGrade;
                List<GradeDefault> listGradeInfo = new List<GradeDefault>();
                // Lay thong tin cap
                if ((Utils.GradeToBinary(1) & EducationGrade) != 0)
                {
                    GradeDefault gc = new GradeDefault();
                    gc.GradeID = 1;
                    gc.GradeName = WCFConstant.GRADE_PRIMARY;
                    listGradeInfo.Add(gc);
                }

                if ((Utils.GradeToBinary(2) & EducationGrade) != 0)
                {
                    GradeDefault gc = new GradeDefault();
                    gc.GradeID = 2;
                    gc.GradeName = WCFConstant.GRADE_SECONDARY;
                    listGradeInfo.Add(gc);
                }

                if ((Utils.GradeToBinary(3) & EducationGrade) != 0)
                {
                    GradeDefault gc = new GradeDefault();
                    gc.GradeID = 3;
                    gc.GradeName = WCFConstant.GRADE_HIGHTSCHOOL;
                    listGradeInfo.Add(gc);
                }
                if ((Utils.GradeToBinary(4) & EducationGrade) != 0)
                {
                    GradeDefault gc = new GradeDefault();
                    gc.GradeID = 4;
                    gc.GradeName = WCFConstant.GRADE_CHILDRENT_1ST;
                    listGradeInfo.Add(gc);
                }
                if ((Utils.GradeToBinary(5) & EducationGrade) != 0)
                {
                    GradeDefault gc = new GradeDefault();
                    gc.GradeID = 5;
                    gc.GradeName = WCFConstant.GRADE_CHILDRENT_2ST;
                    listGradeInfo.Add(gc);
                }
                //gan validationCode thanh cong
                objGradeResponse.ValidationCode = WCFConstant.RESPONSE_SUCCESS;
                //gan gia tri thuoc tinh dinh kem 
                objGradeResponse.LstGrade = listGradeInfo;
            }
            catch
            {
                //gan validationCode Exception
                objGradeResponse.ValidationCode = WCFConstant.RESPONSE_EXCEPTION;
            }
            return objGradeResponse;
        }

        /// <summary>
        /// Insert log
        /// </summary>
        /// <param name="ActionAuditComposite"></param>
        public BoolResultResponse InsertLog(ActionAuditRequest ActionAuditComposite)
        {
            BoolResultResponse objActionAudit = new BoolResultResponse();
            try
            {
                ActionAudit actionAudit = new ActionAudit();
                WCFUtils.CopyObject(ActionAuditComposite, actionAudit);
                LogAdapter.Insert(actionAudit);
                //gan validationCode thanh cong
                objActionAudit.ValidationCode = WCFConstant.RESPONSE_SUCCESS;
                //gan gia tri thuoc tinh dinh kem 
                objActionAudit.Result = true;
            }
            catch
            {
                //gan validationCode Exception
                objActionAudit.ValidationCode = WCFConstant.RESPONSE_EXCEPTION;
                //gan gia tri thuoc tinh dinh kem 
                objActionAudit.Result = false;
            }
            return objActionAudit;
        }

        /// <summary>
        /// Lấy đơn vị quản lý sở/phòng theo AdminID (là UserAccountID)
        /// </summary>
        /// <param name="AdminID"></param>
        /// <returns></returns>
        public SupervisingDeptResponse GetSupervisingDeptByAdminID(int AdminID)
        {
            SupervisingDeptResponse objSupervisingDeptResponse = new SupervisingDeptResponse();
            try
            {
                SupervisingDept sd = SupervisingDeptBusiness.GetSupervisingDeptOfUser(AdminID);
                SupervisingDeptDefault sdc = new SupervisingDeptDefault();
                if (sd != null)
                {
                    //Kiem tra neu la admin phong so thi gan thuoc tinh
                    if (sd.AdminID.HasValue && sd.AdminID == AdminID)
                    {
                        sdc = new SupervisingDeptDefault()
                        {
                            SupervisingDeptID = sd.SupervisingDeptID,
                            SupervisingDeptUserName = sd.UserAccount.aspnet_Users.LoweredUserName,
                            SupervisingDeptCode = sd.SupervisingDeptCode,
                            SupervisingDeptName = sd.SupervisingDeptName,
                            ProvinceID = sd.ProvinceID.HasValue ? sd.ProvinceID.Value : 0,
                            ProvinceName = sd.ProvinceID.HasValue ? sd.Province.ProvinceName : string.Empty,
                            DistrictID = sd.DistrictID.HasValue ? sd.DistrictID.Value : 0,
                            DistrictName = sd.DistrictID.HasValue ? sd.District.DistrictName : string.Empty,
                            AreaID = sd.AreaID.HasValue ? sd.AreaID.Value : 0,
                            AreaName = sd.AreaID.HasValue ? sd.Area.AreaName : string.Empty,
                            Address = sd.Address,
                            AdminID = sd.AdminID.HasValue ? sd.AdminID.Value : 0,
                            Email = sd.Email,
                            Fax = sd.Fax,
                            HierachyLevel = sd.HierachyLevel,
                            ParentID = sd.ParentID.HasValue ? sd.ParentID.Value : 0,

                            EstablishedDate = sd.CreatedDate.HasValue ? sd.CreatedDate.Value : new DateTime(),
                            Telephone = sd.Telephone,
                            Website = sd.Website,
                            TraversalPath = sd.TraversalPath,
                            IsActiveSMAS = (sd.IsActiveSMAS == null ? false : sd.IsActiveSMAS.Value),
                            SMSActiveType = (sd.SMSActiveType == null ? 0 : sd.SMSActiveType.Value),
                        };
                    }
                }
                //gan validationCode thanh cong
                objSupervisingDeptResponse.ValidationCode = WCFConstant.RESPONSE_SUCCESS;
                //gan gia tri thuoc tinh dinh kem 
                objSupervisingDeptResponse.ObjSupervisingDept = sdc;
            }
            catch
            {
                //gan validationCode Exception
                objSupervisingDeptResponse.ValidationCode = WCFConstant.RESPONSE_EXCEPTION;
            }
            return objSupervisingDeptResponse;
        }

        /// <summary>
        /// Kiem tra xem trong co phai la GDTX hay khong
        /// </summary>
        /// <param name="SchoolID"></param>
        /// <returns></returns>
        public BoolResultResponse GetVocationType(int SchoolID)
        {
            BoolResultResponse objRes = new BoolResultResponse();
            try
            {
                string strGDTX = "GDTX";
                SchoolProfile sp = SchoolProfileBusiness.Find(SchoolID);
                bool isGDTX = (sp != null && sp.TrainingType != null && sp.TrainingType.Resolution == strGDTX);
                //gan validationCode thanh cong
                objRes.ValidationCode = WCFConstant.RESPONSE_SUCCESS;
                //gan gia tri thuoc tinh dinh kem 
                objRes.Result = isGDTX;
            }
            catch
            {
                //gan validationCode Exception
                objRes.ValidationCode = WCFConstant.RESPONSE_EXCEPTION;
                //gan gia tri thuoc tinh dinh kem 
                objRes.Result = false;
            }
            return objRes;
        }

        #region kiem tra co ton tai username trong he thong hay khong
        /// <summary>
        /// kiem tra co ton tai username trong he thong hay khong
        /// </summary>
        /// <author date="2014/02/21">HaiVT</author>
        /// <param name="userLogin"></param>
        /// <param name="authenPass"></param>
        /// <param name="accName"></param>
        /// <returns></returns>
        public string CheckAccExitEncrypt(string authenPass, string accName)
        {
            LogExtensions.GetCurrentClass = System.Reflection.MethodBase.GetCurrentMethod().DeclaringType;
            DateTime eventDate = DateTime.Now;
            string para = string.Format("authenPass={0}, accName={1}", authenPass, accName);
            try
            {
                DateTime dateTimeNow = DateTime.Now;
                string code = WCFUtils.CheckAuthenPass(authenPass);
                if ("1".Equals(code))
                {
                    MembershipUser memberShipUser = Membership.GetUser(accName);
                    if (memberShipUser != null)
                    {
                        code = "1";
                    }
                    else
                    {
                        code = "0";
                    }
                    LogExtensions.InfoExt(logger, LogExtensions.LOG_TYPE_END_ACTION, eventDate, para, "Kiểm tra account tồn tại bằng account và pass", accName);
                }
                return code;
            }
            catch (Exception ex)
            {
                LogExtensions.ErrorExt(logger, DateTime.Now, para, ex.Message, "null");
                return "-1";
            }
        }
        #endregion

        #region active smas
        /// <summary>
        /// active smas        
        /// </summary>
        /// <author date="2014/02/22">HaiVT</author>
        /// <param name="authenPass">ma xac thuc</param>
        /// <param name="userName">username</param>
        /// <returns></returns>
        public string ActiveSMASEncrypt(string authenPass, string userName)
        {
            LogExtensions.GetCurrentClass = System.Reflection.MethodBase.GetCurrentMethod().DeclaringType;
            string para = string.Format("userName={0}", userName);
            DateTime dateTimeNow = DateTime.Now;
            string code = string.Empty;
            try
            {
                code = WCFUtils.CheckAuthenPass(authenPass);
                if ("1".Equals(code))
                {
                    MembershipUser objMembershipUser;
                    objMembershipUser = Membership.GetUser(userName);
                    if (objMembershipUser != null)
                    {
                        Guid userId = (Guid)objMembershipUser.ProviderUserKey;
                        UserAccount objUserAccount = UserAccountBusiness.GetUserByProviderUserKey(userId);
                        if (objUserAccount.IsAdmin)
                        {
                            objMembershipUser.IsApproved = true;
                            SchoolProfile sp = SchoolProfileBusiness.All.Where(p => p.AdminID == objUserAccount.UserAccountID).FirstOrDefault();
                            if (sp != null)
                            {
                                sp.IsActiveSMAS = true;
                                code = "1";
                                Membership.Provider.UpdateUser(objMembershipUser);
                            }
                            else
                            {
                                SupervisingDept supervisingDept = SupervisingDeptBusiness.All.Where(p => p.AdminID == objUserAccount.UserAccountID).FirstOrDefault();
                                if (supervisingDept != null)
                                {
                                    supervisingDept.IsActiveSMAS = true;
                                    code = "1";
                                    Membership.Provider.UpdateUser(objMembershipUser);
                                }
                                else
                                {
                                    code = "6";
                                }
                            }
                            SchoolProfileBusiness.Save();
                        }
                    }
                    else
                    {
                        code = "0";
                    }
                }

                LogExtensions.InfoExt(logger, LogExtensions.LOG_TYPE_END_ACTION, dateTimeNow, para, "Active Smas by account", userName);
            }
            catch (Exception ex)
            {
                LogExtensions.ErrorExt(logger, DateTime.Now, para, ex.Message, "null");
                code = "-1";
            }

            return code;
        }
        #endregion

        #region deactive smas
        /// <summary>
        /// deactive smas
        /// </summary>
        /// <author date="2014/02/22">HaiVT</author>
        /// <param name="authenPass">ma xac thuc</param>
        /// <param name="userName">user name</param>
        /// <returns></returns>
        public string DeactiveSMASEncrypt(string authenPass, string userName)
        {
            LogExtensions.GetCurrentClass = System.Reflection.MethodBase.GetCurrentMethod().DeclaringType;
            string para = string.Format("authenPass={0}, userName={1}", authenPass, userName);
            string code = string.Empty;
            DateTime dateTimeNow = DateTime.Now;
            try
            {
                code = WCFUtils.CheckAuthenPass(authenPass);
                code = WCFUtils.CheckAuthenPass(authenPass);
                if ("1".Equals(code))
                {
                    MembershipUser objMembershipUser;
                    objMembershipUser = Membership.GetUser(userName);
                    if (objMembershipUser != null)
                    {
                        Guid userId = (Guid)objMembershipUser.ProviderUserKey;
                        UserAccount objUserAccount = UserAccountBusiness.GetUserByProviderUserKey(userId);
                        if (objUserAccount.IsAdmin)
                        {
                            objMembershipUser.IsApproved = false;
                            SchoolProfile sp = SchoolProfileBusiness.All.Where(p => p.AdminID == objUserAccount.UserAccountID).FirstOrDefault();
                            if (sp != null)
                            {
                                sp.IsActiveSMAS = false;
                                code = "1";
                                Membership.Provider.UpdateUser(objMembershipUser);
                            }
                            else
                            {
                                SupervisingDept supervisingDept = SupervisingDeptBusiness.All.Where(p => p.AdminID == objUserAccount.UserAccountID).FirstOrDefault();
                                if (supervisingDept != null)
                                {
                                    supervisingDept.IsActiveSMAS = false;
                                    code = "1";
                                    Membership.Provider.UpdateUser(objMembershipUser);
                                }
                                else
                                {
                                    code = "6";
                                }

                            }

                            SchoolProfileBusiness.Save();
                        }
                    }
                    else
                    {
                        code = "0";
                    }
                }

                LogExtensions.InfoExt(logger, LogExtensions.LOG_TYPE_END_ACTION, dateTimeNow, para, "deactive Smas by account", userName);
            }
            catch (Exception ex)
            {
                LogExtensions.ErrorExt(logger, DateTime.Now, para, ex.Message, "null");
            }

            return code;
        }
        #endregion

        #region active sms teacher
        /// <summary>
        /// active sms teacher
        /// </summary>
        /// <author date="2014/02/22">HaiVT</author>
        /// <param name="authenPass">ma xac thuc</param>
        /// <param name="userName">tai khoan truong</param>
        /// <returns></returns>
        public string ActiveSMSTeacherEncrypt(string authenPass, string userName)
        {
            LogExtensions.GetCurrentClass = System.Reflection.MethodBase.GetCurrentMethod().DeclaringType;
            string para = string.Format("authenPass={0}, userName={1}", authenPass, userName);
            string code = string.Empty;
            DateTime dateTimeNow = DateTime.Now;
            try
            {
                code = WCFUtils.CheckAuthenPass(authenPass);
                if ("1".Equals(code))
                {
                    LogExtensions.InfoExt(logger, LogExtensions.LOG_TYPE_START_ACTION, dateTimeNow, para, "Process Active SMS teacher by account", userName);
                    code = WCFUtils.CheckAuthenPass(authenPass);
                    if ("1".Equals(code))
                    {
                        MembershipUser objMembershipUser;
                        objMembershipUser = Membership.GetUser(userName);
                        if (objMembershipUser != null)
                        {
                            Guid userId = (Guid)objMembershipUser.ProviderUserKey;
                            UserAccount objUserAccount = UserAccountBusiness.GetUserByProviderUserKey(userId);
                            if (objUserAccount.IsAdmin)
                            {
                                SchoolProfile sp = SchoolProfileBusiness.All.Where(p => p.AdminID == objUserAccount.UserAccountID).FirstOrDefault();
                                int schoolID = sp.SchoolProfileID;
                                if (sp != null)
                                {
                                    sp.SMSTeacherActiveType = 2;
                                    code = "1";
                                }
                                else
                                {
                                    SupervisingDept supervisingDept = SupervisingDeptBusiness.All.Where(p => p.AdminID == objUserAccount.UserAccountID).FirstOrDefault();
                                    if (supervisingDept != null)
                                    {
                                        objMembershipUser.IsApproved = true;
                                        supervisingDept.SMSActiveType = 2;
                                        code = "1";
                                    }
                                    else
                                    {
                                        code = "3";
                                    }
                                }

                                SchoolProfileBusiness.Save();

                            }
                            else
                            {
                                code = "9";
                            }
                        }
                        else
                        {
                            code = "9";
                        }
                    }
                    LogExtensions.InfoExt(logger, LogExtensions.LOG_TYPE_END_ACTION, dateTimeNow, para, "Active SMS teacher by account", userName);
                }
            }
            catch (Exception ex)
            {

                LogExtensions.ErrorExt(logger, DateTime.Now, para, ex.Message, "null");
                code = "-1";
            }

            return code;
        }
        #endregion

        #region deactive SMSTeacher
        /// <summary>
        /// deactive sms teacher
        /// </summary>
        /// <author date="2014/02/22">HaiVT</author>
        /// <param name="authenUser">user xac thuc</param>
        /// <param name="authenPass">ma xac thuc</param>
        /// <param name="userName">acc truong</param>
        /// <param name="DeactiveCode"></param>
        /// <returns></returns>
        public string DeactiveSMSTeacherEncrypt(string authenUser, string authenPass, string userName, string DeactiveCode)
        {
            LogExtensions.GetCurrentClass = System.Reflection.MethodBase.GetCurrentMethod().DeclaringType;
            string para = string.Format("authenUser=={0}, authenPass={1}, userName={2}, DeactiveCode={3}", authenUser, authenPass, userName, DeactiveCode);
            string code = string.Empty;
            DateTime dateTimeNow = DateTime.Now;
            try
            {
                code = WCFUtils.CheckAuthenPass(authenPass);
                if ("1".Equals(code))
                {
                    LogExtensions.InfoExt(logger, LogExtensions.LOG_TYPE_START_ACTION, dateTimeNow, para, "Process Deactive SMS teacher by account", userName);
                    code = WCFUtils.CheckAuthenPass(authenPass);
                    if ("1".Equals(code))
                    {
                        MembershipUser objMembershipUser;
                        objMembershipUser = Membership.GetUser(userName);
                        if (objMembershipUser != null)
                        {
                            Guid userId = (Guid)objMembershipUser.ProviderUserKey;
                            UserAccount objUserAccount = UserAccountBusiness.GetUserByProviderUserKey(userId);
                            if (objUserAccount.IsAdmin)
                            {
                                SchoolProfile sp = SchoolProfileBusiness.All.Where(p => p.AdminID == objUserAccount.UserAccountID).FirstOrDefault();
                                int schoolID = sp != null ? sp.SchoolProfileID : 0;
                                if (sp != null)
                                {
                                    if (!string.IsNullOrEmpty(authenUser) && authenUser.ToLower().Equals("payment"))
                                    {
                                        sp.SMSTeacherActiveType = 3;
                                    }
                                    else
                                    {
                                        sp.SMSTeacherActiveType = 0;
                                    }
                                    code = "1";
                                }
                                else
                                {
                                    SupervisingDept supervisingDept = SupervisingDeptBusiness.All.Where(p => p.AdminID == objUserAccount.UserAccountID).FirstOrDefault();
                                    if (supervisingDept != null)
                                    {
                                        supervisingDept.SMSActiveType = 0;
                                        code = "1";
                                    }
                                    else
                                    {
                                        code = "9";
                                    }
                                }

                                SchoolProfileBusiness.Save();

                            }
                            else
                            {
                                code = "9";
                            }
                        }
                        else
                        {
                            code = "9";
                        }

                        LogExtensions.InfoExt(logger, LogExtensions.LOG_TYPE_END_ACTION, dateTimeNow, para, "Deactive SMS teacher by account", userName);
                    }
                }
            }
            catch (Exception ex)
            {

                LogExtensions.ErrorExt(logger, DateTime.Now, para, ex.Message, "null");
                code = "-1";
            }

            return code;
        }
        #endregion

        #region active smsParent
        /// <summary>
        /// active smsParent
        /// </summary>
        /// <author date="2014/02/22">HaiVT</author>
        /// <param name="authen">ma xac thuc</param>
        /// <param name="accName">user truong</param>
        /// <returns></returns>
        public string ActiveSMSParent(string authenPass, string userName)
        {
            LogExtensions.GetCurrentClass = System.Reflection.MethodBase.GetCurrentMethod().DeclaringType;
            string para = string.Format("userName={0}", userName);
            string code = string.Empty;
            DateTime dateTimeNow = DateTime.Now;
            try
            {
                LogExtensions.InfoExt(logger, LogExtensions.LOG_TYPE_START_ACTION, dateTimeNow, para, "Process Active SMS Parent by account", userName);
                code = WCFUtils.CheckAuthenPass(authenPass);
                if ("1".Equals(code))
                {
                    MembershipUser objMembershipUser;
                    objMembershipUser = Membership.GetUser(userName);
                    if (objMembershipUser != null)
                    {
                        Guid userId = (Guid)objMembershipUser.ProviderUserKey;
                        UserAccount objUserAccount = UserAccountBusiness.GetUserByProviderUserKey(userId);
                        if (objUserAccount.IsAdmin)
                        {
                            SchoolProfile sp = SchoolProfileBusiness.All.Where(p => p.AdminID == objUserAccount.UserAccountID).FirstOrDefault();
                            if (sp != null)
                            {
                                sp.SMSParentActiveType = 1;
                                code = "1";
                            }
                            else
                            {
                                code = "3";
                            }

                            SchoolProfileBusiness.Save();
                        }
                        else
                        {
                            code = "3";
                        }
                    }
                    else
                    {
                        code = "9";
                    }

                    LogExtensions.InfoExt(logger, LogExtensions.LOG_TYPE_END_ACTION, dateTimeNow, para, "Active SMS Parent by account", userName);
                }
            }
            catch (Exception ex)
            {
                LogExtensions.ErrorExt(logger, DateTime.Now, para, ex.Message, "null");
                code = "-1";
            }

            return code;
        }
        #endregion

        #region deactive sms parent
        /// <summary>
        /// deactive sms parent
        /// </summary>
        /// <author date="2014/02/22">HaiVT</author>
        /// <param name="authenPass">ma xac thuc</param>
        /// <param name="userName">user truong</param>
        /// <returns></returns>
        public string DeactiveSMSParent(string authenPass, string userName)
        {
            LogExtensions.GetCurrentClass = System.Reflection.MethodBase.GetCurrentMethod().DeclaringType;
            string para = string.Format("userName={0}", userName);
            string code = string.Empty;
            DateTime dateTimeNow = DateTime.Now;
            try
            {
                code = WCFUtils.CheckAuthenPass(authenPass);
                if ("1".Equals(code))
                {
                    LogExtensions.InfoExt(logger, LogExtensions.LOG_TYPE_START_ACTION, dateTimeNow, para, "Process Deactive SMS Parent by account", userName);
                    MembershipUser objMembershipUser;
                    objMembershipUser = Membership.GetUser(userName);
                    if (objMembershipUser != null)
                    {
                        Guid userId = (Guid)objMembershipUser.ProviderUserKey;
                        UserAccount objUserAccount = UserAccountBusiness.GetUserByProviderUserKey(userId);
                        if (objUserAccount.IsAdmin)
                        {
                            SchoolProfile sp = SchoolProfileBusiness.All.Where(p => p.AdminID == objUserAccount.UserAccountID).FirstOrDefault();
                            if (sp != null)
                            {
                                sp.SMSParentActiveType = 0;
                                code = "1";
                            }
                            else
                            {
                                code = "3";
                            }

                            SchoolProfileBusiness.Save();
                        }
                        else
                        {
                            code = "3";
                        }
                    }
                    else
                    {
                        code = "9";
                    }

                    LogExtensions.InfoExt(logger, LogExtensions.LOG_TYPE_END_ACTION, dateTimeNow, para, "Deactive SMS Parent by account", userName);
                }
            }
            catch (Exception ex)
            {
                LogExtensions.ErrorExt(logger, DateTime.Now, para, ex.Message, "null");
                code = "-1";
            }

            return code;
        }
        #endregion

        #region Check trang thai SMSTeacher
        /// <summary>
        /// Check trang thai SMSTeacher
        /// param: authen ma xac thuc, accName account truong
        /// returns: 9 khong xac dinh site HCM/TQ, 10 -> 13 loi, -1 khong thanh cong; 0 khong kich hoat sms, 1 kick hoat khong gioi han, 2 kick hoat gioi han, 3 chan cat
        /// </summary>
        /// <author date="2014/02/22">HaiVT</author>
        /// <param name="authenPass">ma xac thuc</param>
        /// <param name="userName">account truong</param>
        /// <returns>string: 1</returns>        
        public string CheckSMSTeacher(string authenPass, string userName)
        {
            LogExtensions.GetCurrentClass = System.Reflection.MethodBase.GetCurrentMethod().DeclaringType;
            string para = string.Format("userName={0}", userName);
            string code = string.Empty;
            DateTime dateTimeNow = DateTime.Now;
            try
            {
                code = WCFUtils.CheckAuthenPass(authenPass);
                if ("1".Equals(code))
                {
                    LogExtensions.InfoExt(logger, LogExtensions.LOG_TYPE_START_ACTION, dateTimeNow, para, "Process CheckSMSTeacher by account", userName);
                    MembershipUser objMembershipUser;
                    objMembershipUser = Membership.GetUser(userName);
                    if (objMembershipUser != null)
                    {
                        Guid userId = (Guid)objMembershipUser.ProviderUserKey;
                        UserAccount objUserAccount = UserAccountBusiness.GetUserByProviderUserKey(userId);
                        if (objUserAccount.IsAdmin)
                        {
                            SchoolProfile sp = SchoolProfileBusiness.All.Where(p => p.AdminID == objUserAccount.UserAccountID).FirstOrDefault();
                            if (sp != null)
                            {
                                code = sp.SMSTeacherActiveType.ToString();
                            }
                            else
                            {
                                code = "11";
                            }

                            SchoolProfileBusiness.Save();
                        }
                        else
                        {
                            code = "11";
                        }
                    }
                    else
                    {
                        code = "0";
                    }

                    LogExtensions.InfoExt(logger, LogExtensions.LOG_TYPE_END_ACTION, dateTimeNow, para, "CheckSMSTeacher by account", userName);
                }
            }
            catch (Exception ex)
            {

                LogExtensions.ErrorExt(logger, DateTime.Now, para, ex.Message, "null");
                code = "-1";
            }

            return code;
        }
        #endregion

        #region Load Schedule
        public CalendarScheduleRespone LoadScheduleCalendar(DateTime applyDate, int SchoolID, int AcademicYearID, int EducationGrade, int ClassID, int Semester, int DayOfWeek)
        {
            CalendarScheduleRespone obj = new CalendarScheduleRespone();
            try
            {
                List<CalendarScheduleRespone.CalendarSheduleLite> lstRes = new List<CalendarScheduleRespone.CalendarSheduleLite>();
                //Lay thong tin calendar cua ngay hien tai
                //Search condinations
                Dictionary<string, object> dic = new Dictionary<string, object>();
                dic["SchoolID"] = SchoolID;
                dic["AcademicYearID"] = AcademicYearID;
                dic["AppliedLevel"] = EducationGrade;
                dic["ClassID"] = ClassID;
                dic["DayOfWeek"] = DayOfWeek;
                dic["DaySelected"] = applyDate.Date;
                dic["Semester"] = Semester;

                List<Calendar> lstCalendar = CalendarBusiness.Search(dic).ToList();
                List<CalendarSchedule> lstSchedule = CalendarScheduleBusiness.Search(dic).ToList();
                Calendar tmpCalendar = null;
                CalendarSchedule tmpSchedule = null;
                CalendarScheduleRespone.CalendarSheduleLite tmpMdl = null;
                for (int i = 0; i < lstCalendar.Count; i++)
                {
                    tmpCalendar = lstCalendar[i];
                    tmpSchedule = lstSchedule.Where(a => a.CalendarID == tmpCalendar.CalendarID).FirstOrDefault();
                    if (tmpCalendar != null)
                    {
                        //Bind Schedule to Model
                        tmpMdl = new CalendarScheduleRespone.CalendarSheduleLite();
                        tmpMdl.CalendarID = tmpCalendar.CalendarID;
                        tmpMdl.NumberOfPeriod = (tmpCalendar.NumberOfPeriod.HasValue) ? tmpCalendar.NumberOfPeriod.Value : 0;
                        tmpMdl.Section = (tmpCalendar.Section.HasValue) ? tmpCalendar.Section.Value : 0;
                        tmpMdl.SubjectID = tmpCalendar.SubjectID;
                        tmpMdl.SubjectName = tmpCalendar.SubjectCat.DisplayName;
                        if (tmpSchedule != null)
                        {
                            tmpMdl.Preparation = tmpSchedule.Preparation;
                            tmpMdl.HomeWork = tmpSchedule.HomeWork;
                            tmpMdl.CalendarScheduleID = tmpSchedule.CalendarScheduleID;
                            tmpMdl.SubjectTitle = tmpSchedule.SubjectTitle;
                        }
                        lstRes.Add(tmpMdl);
                    }
                }
                obj.ValidationCode = WCFConstant.RESPONSE_SUCCESS;
                obj.LstCalendarShedule = lstRes;

            }
            catch (Exception ex)
            {
                //gan validationCode exception
                obj.ValidationCode = WCFConstant.RESPONSE_EXCEPTION;

            }
            return obj;
        }
        #endregion

        public SemesterDeclarationResponse GetSemesterDeclaration(int SchoolID, int AcademicYearID, int SemesterID)
        {
            SemesterDeclarationResponse objSemesterDeclaration = new SemesterDeclarationResponse();
            try
            {
                AcademicYear academicYear = AcademicYearBusiness.Find(AcademicYearID);
                if (academicYear != null)
                {
                    academicYear.FirstSemesterEndDate = academicYear.FirstSemesterEndDate.Value.Date.AddHours(23).AddMinutes(59).AddSeconds(59);
                    academicYear.SecondSemesterEndDate = academicYear.SecondSemesterEndDate.Value.Date.AddHours(23).AddMinutes(59).AddSeconds(59);
                    objSemesterDeclaration.ObjSemesterDeclaration = (from s in SemesterDeclarationBusiness.All
                                                                     where s.SchoolID == SchoolID
                                                                     && s.AcademicYearID == AcademicYearID
                                                                     && (
                                                                        (SemesterID == 1 && s.AppliedDate <= academicYear.FirstSemesterEndDate)
                                                                        || (SemesterID == 2 && s.AppliedDate <= academicYear.SecondSemesterEndDate)
                                                                     )
                                                                     orderby s.AppliedDate descending
                                                                     select new SemesterDeclarationLite
                                                                     {
                                                                         SemesterDeclarationID = s.SemeterDeclarationID,
                                                                         InterviewMark = s.InterviewMark,
                                                                         WritingMark = s.WritingMark,
                                                                         TwiceCoeffiecientMark = s.TwiceCoeffiecientMark,
                                                                         IsLock = s.IsLock
                                                                     }).FirstOrDefault();
                }

                objSemesterDeclaration.ValidationCode = WCFConstant.RESPONSE_SUCCESS;
            }
            catch (Exception ex)
            {
                LogExtensions.GetCurrentClass = System.Reflection.MethodBase.GetCurrentMethod().DeclaringType;
                string para = string.Format("academicYearId={0}, schoolId={1},SemesterID={2}", AcademicYearID, SchoolID, SemesterID);
                LogExtensions.ErrorExt(logger, DateTime.Now, para, ex.Message, "null");
                objSemesterDeclaration.ValidationCode = WCFConstant.RESPONSE_EXCEPTION;
            }
            return objSemesterDeclaration;
        }

        #region get userName by schoolID
        /// <summary>
        /// lay usrename by schoolID
        /// </summary>
        /// <author date="2014/03/11">HaiVT</author>
        /// <param name="schoolID">id truong</param>
        /// <returns></returns>
        public StringResultResponse GetUserNameBySchoolID(int schoolID)
        {
            StringResultResponse result = new StringResultResponse();
            try
            {
                var userName = (from ua in UserAccountBusiness.All
                                join asp in Aspnet_UsersBusiness.All on ua.GUID equals asp.UserId
                                join sp in SchoolProfileBusiness.All on ua.UserAccountID equals sp.AdminID
                                where sp.SchoolProfileID == schoolID
                                select asp.UserName).FirstOrDefault();

                //gan validationCode thanh cong
                result.ValidationCode = WCFConstant.RESPONSE_SUCCESS;
                //gan gia tri thuoc tinh dinh kem 
                result.Result = userName;
            }
            catch (Exception ex)
            {
                LogExtensions.GetCurrentClass = System.Reflection.MethodBase.GetCurrentMethod().DeclaringType;
                string para = string.Format("schoolID={0}", schoolID);
                LogExtensions.ErrorExt(logger, DateTime.Now, para, ex.Message, "null");
                //gan validationCode Exception
                result.ValidationCode = WCFConstant.RESPONSE_EXCEPTION;
            }

            return result;
        }
        #endregion

        #region check Class is VNEN 
        public BoolResultResponse IsVNENClass(int ClassID)
        {
            BoolResultResponse objRes = new BoolResultResponse();
            try
            {
                ClassProfile cp = ClassProfileBusiness.Find(ClassID);
                objRes.ValidationCode = WCFConstant.RESPONSE_SUCCESS;
                if (cp != null) objRes.Result = cp.IsVnenClass.HasValue ? cp.IsVnenClass.Value : false;
                else objRes.Result = false;
            }
            catch
            {
                objRes.ValidationCode = WCFConstant.RESPONSE_EXCEPTION;
                objRes.Result = false;
            }
            return objRes;
        }
        #endregion

        public EPermission GetMenuPermission(string Controller, int UserAccountID)
        {
            EPermission per = EPermission.None;
            //if (this.UserAccountBusiness == null)
            //{
            //    this.UserAccountBusiness = new UserAccountBusiness(logger, this.context);
            //}
            if (this.UserAccountBusiness.IsAdmin(UserAccountID)) // Neu la quyen quan tri hien thi quyen cao nhat (cu the la quyen xoa)
            {
                per = EPermission.View | EPermission.Create | EPermission.Edit | EPermission.Delete;
            }
            else
            {
                IEnumerable<GroupCat> objUserGroup = UserGroupBusiness.GetGroupsOfUser(UserAccountID);
                var listGroupCatID = objUserGroup.Select(u => u.GroupCatID).ToList();
                if (objUserGroup != null && objUserGroup.Count() > 0)
                {
                    List<GroupMenu> lstGroupMenu = GroupMenuBusiness.All
                        .Where(gm => listGroupCatID.Contains(gm.GroupID)
                            && gm.Menu.URL.ToLower().Contains(Controller.ToLower()))
                        .ToList();
                    foreach (var groupMenu in lstGroupMenu)
                    {
                        switch (groupMenu.Permission)
                        {
                            case SystemParamsInFile.PER_VIEW:
                                per = per | EPermission.View;
                                break;
                            case SystemParamsInFile.PER_CREATE:
                                per = per | EPermission.Create;
                                break;
                            case SystemParamsInFile.PER_UPDATE:
                                per = per | EPermission.Edit;
                                break;
                            case SystemParamsInFile.PER_DELETE:
                                per = per | EPermission.Delete;
                                break;
                            default:
                                break;
                        }
                    }
                }
            }
            return per;
        }

        #region Tra cuu Diem
        /// <summary>
        /// Lay toan bo danh sach tinh/thanh
        /// </summary>
        /// <autyhor>AnhVD 20140714</autyhor>
        /// <returns></returns>
        public ListProvinceResponse GetAllProvince()
        {
            ListProvinceResponse lstProvinceResponse = new ListProvinceResponse();
            try
            {
                List<ProvinceDefault> listProvince = ProvinceBusiness.Search(new Dictionary<string, object>() { }).Select(o =>
                   new ProvinceDefault
                   {
                       ProvinceID = o.ProvinceID,
                       ProvinceCode = o.ProvinceCode,
                       ProvinceName = o.ProvinceName,
                       ShortName = o.ShortName,
                       Description = o.Description
                   }).OrderBy(o => o.ProvinceID).ToList();
                //Gan validation code
                lstProvinceResponse.ValidationCode = WCFConstant.RESPONSE_SUCCESS;
                //gan gia tri thuoc tinh dinh kem 
                lstProvinceResponse.LstProvinceDefault = listProvince;
            }
            catch (Exception ex)
            {
                lstProvinceResponse.ValidationCode = WCFConstant.RESPONSE_EXCEPTION;
            }
            return lstProvinceResponse;

        }

        /// <summary>
        /// Lay danh sach quan/huyen theo tinh thanh
        /// </summary>
        /// <returns></returns>
        public ListDistrictResponse GetDistrictByProvinceID(int ProvinceID, int DistrictID)
        {
            ListDistrictResponse lstDistrictResponse = new ListDistrictResponse();
            try
            {
                Dictionary<string, object> dic = new Dictionary<string, object>() {
                      {   "ProvinceID", ProvinceID },
                    {   "DistrictID", DistrictID }


                };
                List<DistrictDefault> lstDistrict = DistrictBusiness.Search(dic).Select(o =>
                   new DistrictDefault
                   {
                       DistrictID = o.DistrictID,
                       DistrictName = o.DistrictName,
                       DistrictCode = o.DistrictCode,
                       ShortName = o.ShortName,
                       Description = o.Description
                   }).OrderBy(o => o.DistrictID).ToList();
                //Gan validation code
                lstDistrictResponse.ValidationCode = WCFConstant.RESPONSE_SUCCESS;
                //gan gia tri thuoc tinh dinh kem 
                lstDistrictResponse.LstDistrictDefault = lstDistrict;
            }
            catch (Exception ex)
            {
                lstDistrictResponse.ValidationCode = WCFConstant.RESPONSE_EXCEPTION;
            }
            return lstDistrictResponse;

        }

        /// <summary>
        /// Lay danh sach quan/huyen theo tinh thanh
        /// </summary>
        /// <returns></returns>
        public ListSchoolProfileLiteResponse GetSchoolInDistrict(int DistrictID, int Grade)
        {
            ListSchoolProfileLiteResponse ListSchoolProfileLiteResponse = new ListSchoolProfileLiteResponse();
            try
            {
                var district = DistrictBusiness.Find(DistrictID);
                //Gan validation code
                ListSchoolProfileLiteResponse.ValidationCode = WCFConstant.RESPONSE_SUCCESS;
                //gan gia tri thuoc tinh dinh kem 
                ListSchoolProfileLiteResponse.LstSchoolProfileLite =
                    district.SchoolProfiles
                    .Where(d => d.IsActive && (d.EducationGrade & Grade) > 0)
                    .Select(s =>
                       new SchoolProfileLite()
                       {
                           SchoolID = s.SchoolProfileID,
                           SchoolName = s.SchoolName,
                           EducationGrade = s.EducationGrade,
                           ProvinceID = s.ProvinceID ?? 0,
                           SupervisingDeptID = s.SupervisingDeptID ?? 0,
                           DistrictID = s.DistrictID ?? 0,
                           HeadMasterName = s.HeadMasterName,
                           HeadMasterPhone = s.HeadMasterPhone,
                           IsActive = s.IsActive
                       })
                   .OrderBy(o => o.DistrictID)
                   .ToList();
            }
            catch (Exception ex)
            {
                ListSchoolProfileLiteResponse.ValidationCode = WCFConstant.RESPONSE_EXCEPTION;
            }
            return ListSchoolProfileLiteResponse;

        }

        /// <summary>
        /// lay danh sach truong
        /// </summary>
        /// <author>AnhVD 20140714</author>
        /// <param name="ProvinceID"></param>
        /// <param name="DistrictID"></param>
        /// <returns></returns>
        public ListSchoolProfileLiteResponse GetSchoolByIDPortal(int SchoolID)
        {
            ListSchoolProfileLiteResponse lstSchoolProfileLiteResponse = new ListSchoolProfileLiteResponse();
            try
            {
                List<SchoolProfileLite> lstSchoolProfileLite = new List<SchoolProfileLite>();
                Dictionary<string, object> dic = new Dictionary<string, object>();
                dic["NewTableName"] = "SCHOOL_PROFILE";
                dic["OldTableName"] = "Adm.School";
                dic["OldID"] = SchoolID;

                int S3SchoolID = SchoolID;
                // Neu co du lieu SMAS2 mapping
                List<MIdmapping> lstMIDMapping = MIdmappingBusiness.Search(dic).ToList();
                if (lstMIDMapping != null && lstMIDMapping.Count > 0)
                {
                    // lay NewID
                    MIdmapping objMIdMapping = lstMIDMapping.FirstOrDefault();
                    if (objMIdMapping.IsSMAS3.HasValue && objMIdMapping.IsSMAS3.Value)
                    {
                        S3SchoolID = objMIdMapping.NewID.Value;
                    }
                    else
                    {
                        S3SchoolID = objMIdMapping.OldID.Value;
                    }
                }

                SchoolProfile objSchool = SchoolProfileBusiness.Find(S3SchoolID);
                lstSchoolProfileLite.Add(new SchoolProfileLite()
                {
                    SchoolID = objSchool.SchoolProfileID,
                    SchoolName = objSchool.SchoolName,
                    ProvinceID = objSchool.ProvinceID.HasValue ? objSchool.ProvinceID.Value : 0,
                    DistrictID = objSchool.DistrictID.HasValue ? objSchool.DistrictID.Value : 0,
                    EducationGrade = objSchool.EducationGrade,
                    IsActive = objSchool.IsActive
                });
                //Gan validation code
                lstSchoolProfileLiteResponse.ValidationCode = WCFConstant.RESPONSE_SUCCESS;
                //gan gia tri thuoc tinh dinh kem 
                lstSchoolProfileLiteResponse.LstSchoolProfileLite = lstSchoolProfileLite;
            }
            catch (Exception ex)
            {
                lstSchoolProfileLiteResponse.ValidationCode = WCFConstant.RESPONSE_EXCEPTION;
            }
            return lstSchoolProfileLiteResponse;

        }

        /// <summary>
        /// Lay thogn tin nam hoc
        /// </summary>
        /// <param name="SchoolID"></param>
        /// <returns></returns>
        public ListAcademicYearResponse GetAcademicYearPortal(int SchoolID)
        {
            ListAcademicYearResponse lstAcademicYearResponse = new ListAcademicYearResponse();
            try
            {
                Dictionary<string, object> dic = new Dictionary<string, object>() {
                    {   "SchoolID", SchoolID },

                };
                List<AcademicYearDefault> lstAcademicYear = AcademicYearBusiness.Search(dic).Select(o =>
                   new AcademicYearDefault
                   {
                       AcademicYearID = o.AcademicYearID,
                       Year = o.Year,
                       SchoolID = o.SchoolID,
                       DisplayTitle = o.DisplayTitle,
                       SchoolName = o.SchoolProfile.SchoolName,
                       FirstSemesterStartDate = o.FirstSemesterStartDate.Value,
                       FirstSemesterEndDate = o.FirstSemesterEndDate.Value,
                       SecondSemesterStartDate = o.SecondSemesterStartDate.Value,
                       SecondSemesterEndDate = o.SecondSemesterEndDate.Value
                   }).OrderBy(o => o.Year).ToList();
                //Gan validation code
                lstAcademicYearResponse.ValidationCode = WCFConstant.RESPONSE_SUCCESS;
                //gan gia tri thuoc tinh dinh kem 
                lstAcademicYearResponse.LstAcademicYearDefault = lstAcademicYear;
            }
            catch (Exception ex)
            {
                lstAcademicYearResponse.ValidationCode = WCFConstant.RESPONSE_EXCEPTION;
            }
            return lstAcademicYearResponse;

        }
        #endregion

        #region Quanglm - Bo sung API lay dang sach truong phuc vu cho tool quan ly doi tac nhap lieu
        /// <summary>
        /// Quanglm - 20140926
        /// Lay danh sach truong theo quan/huyen
        /// </summary>
        /// <param name="DistrictID"></param>
        /// <returns></returns>
        public ListSchoolProfileLiteResponse GetSchoolByDistrictID(int DistrictID)
        {
            ListSchoolProfileLiteResponse ListSchoolProfileLiteResponse = new ListSchoolProfileLiteResponse();
            try
            {
                //Gan validation code
                ListSchoolProfileLiteResponse.ValidationCode = WCFConstant.RESPONSE_SUCCESS;
                //gan gia tri thuoc tinh dinh kem 
                ListSchoolProfileLiteResponse.LstSchoolProfileLite =
                    SchoolProfileBusiness.All
                    .Where(o => o.IsActive && o.DistrictID == DistrictID)
                    .OrderBy(o => o.SchoolName)
                    .Select(s =>
                       new SchoolProfileLite()
                       {
                           SchoolID = s.SchoolProfileID,
                           SchoolName = s.SchoolName,
                           DistrictName = s.District.DistrictName,
                           EducationGrade = s.EducationGrade,
                           ProvinceID = s.ProvinceID ?? 0,
                           SupervisingDeptID = s.SupervisingDeptID ?? 0,
                           DistrictID = s.DistrictID ?? 0,
                           HeadMasterName = s.HeadMasterName,
                           HeadMasterPhone = s.HeadMasterPhone,
                           IsActive = s.IsActive
                       })
                   .ToList();
            }
            catch
            {
                ListSchoolProfileLiteResponse.ValidationCode = WCFConstant.RESPONSE_EXCEPTION;
            }
            return ListSchoolProfileLiteResponse;
        }

        /// <summary>
        /// lay schoolID theo provinceID hoac theo districtID
        /// </summary>
        /// <param name="DistrictID"></param>
        /// <returns></returns>
        public ListSchoolProfileLiteResponse GetSchoolByDistrictAndApplied(int DistrictID, int? Applied)
        {
            ListSchoolProfileLiteResponse ListSchoolProfileLiteResponse = new ListSchoolProfileLiteResponse();
            try
            {
                //Gan validation code
                ListSchoolProfileLiteResponse.ValidationCode = WCFConstant.RESPONSE_SUCCESS;
                //gan gia tri thuoc tinh dinh kem 
                ListSchoolProfileLiteResponse.LstSchoolProfileLite =
                    SchoolProfileBusiness.All
                    .Where(o => o.IsActive && o.DistrictID == DistrictID &&
                        (
                             (Applied == 1 && (o.EducationGrade == 1 || o.EducationGrade == 3 || o.EducationGrade == 7 || o.EducationGrade == 31))//Applied = 1:lay cap 1,12,123
                             || (Applied == 2 && (o.EducationGrade == 2 || o.EducationGrade == 3 || o.EducationGrade == 6 || o.EducationGrade == 7 || o.EducationGrade == 31))//Applied = 2:lay cap 2,12,23,123
                             || (Applied == 3 && (o.EducationGrade == 4 || o.EducationGrade == 6 || o.EducationGrade == 7 || o.EducationGrade == 31))//Applied = 3:lay cap 3,123,23
                             || Applied == 0
                        ))
                    .OrderBy(o => o.SchoolName)
                    .Select(s =>
                       new SchoolProfileLite()
                       {
                           SchoolID = s.SchoolProfileID,
                           SchoolName = s.SchoolName,
                           DistrictName = s.District.DistrictName,
                           EducationGrade = s.EducationGrade,
                           ProvinceID = s.ProvinceID ?? 0,
                           SupervisingDeptID = s.SupervisingDeptID ?? 0,
                           DistrictID = s.DistrictID ?? 0,
                           HeadMasterName = s.HeadMasterName,
                           HeadMasterPhone = s.HeadMasterPhone,
                           IsActive = s.IsActive
                       })
                   .ToList();
            }
            catch
            {
                ListSchoolProfileLiteResponse.ValidationCode = WCFConstant.RESPONSE_EXCEPTION;
            }
            return ListSchoolProfileLiteResponse;
        }

        /// <summary>
        /// Lay danh sach truong theo danh sach ID truong
        /// </summary>
        /// <param name="listSchoolID"></param>
        /// <returns></returns>
        public ListSchoolProfileLiteResponse GetSchoolByListID(List<int> listSchoolID)
        {
            ListSchoolProfileLiteResponse ListSchoolProfileLiteResponse = new ListSchoolProfileLiteResponse();
            try
            {
                //Gan validation code
                ListSchoolProfileLiteResponse.ValidationCode = WCFConstant.RESPONSE_SUCCESS;
                //gan gia tri thuoc tinh dinh kem 
                ListSchoolProfileLiteResponse.LstSchoolProfileLite =
                    SchoolProfileBusiness.All
                    .Where(o => o.IsActive && listSchoolID.Contains(o.SchoolProfileID))
                    .OrderBy(o => o.SchoolName)
                    .Select(s =>
                       new SchoolProfileLite()
                       {
                           SchoolID = s.SchoolProfileID,
                           SchoolName = s.SchoolName,
                           EducationGrade = s.EducationGrade,
                           ProvinceID = s.ProvinceID ?? 0,
                           DistrictName = s.District.DistrictName,
                           SupervisingDeptID = s.SupervisingDeptID ?? 0,
                           DistrictID = s.DistrictID ?? 0,
                           HeadMasterName = s.HeadMasterName,
                           HeadMasterPhone = s.HeadMasterPhone,
                           IsActive = s.IsActive
                       })
                   .ToList();
            }
            catch
            {
                ListSchoolProfileLiteResponse.ValidationCode = WCFConstant.RESPONSE_EXCEPTION;
            }
            return ListSchoolProfileLiteResponse;
        }

        #endregion

        #region lay thang nhan xet cho cap 1- Danh muc không thể rỗng (insert bằng tay vào DB)
        public List<MonthComments> getListMonthEvaluation()
        {
            List<MonthComments> list = MonthCommentsBusiness.getMonthCommentsList();
            return list;
        }
        #endregion

        public string getLockMark(int schoolID, int classID)
        {
            string strLockMark = string.Empty;
            LockMarkPrimary lockmarkObj = LockMarkPrimaryBusiness.All.Where(p => p.SchoolID == schoolID && p.ClassID == classID).FirstOrDefault();
            if (lockmarkObj != null)
            {
                strLockMark = lockmarkObj.LockPrimary;
            }
            return strLockMark;
        }

        /// <summary>
        /// Anhnph1 - Lay danh sach nam hoc
        /// </summary>
        /// <returns></returns>        
        public List<int> getYear()
        {
            List<int> lstYear = new List<int>();
            lstYear = AcademicYearBusiness.All.Select(p => p.Year).Distinct().ToList();
            return lstYear;
        }


        #region Lay thong tin truong co dung web portal khong ?
        public SchoolPortalComposite GetSchoolPortal(int ID, bool isSchool)
        {
            SchoolPortalComposite schoolPortalObj = (from s in this.context.SchoolPortal
                                                     where s.ID == ID
                                                     && s.IsSchool == isSchool
                                                     select new SchoolPortalComposite
                                                     {
                                                         SchoolPortalID = s.SchoolPortalID,
                                                         ID = s.ID,
                                                         IsSchool = s.IsSchool,
                                                         CreateTime = s.CreateTime,
                                                         UpdateTime = s.UpdateTime
                                                     }).FirstOrDefault();
            return schoolPortalObj;
        }

        public int CheckSchoolInPorvince(int schoolId, int provinceId, int districtID)
        {
            District districtObj = (from p in this.context.District
                                    where p.ProvinceID == provinceId
                                    && p.DistrictID == districtID
                                    select p).FirstOrDefault();
            if (districtObj == null) return 1; //quan huyen khong nam trong tinh thanh

            SchoolProfile schoolPorifleObj = (from s in this.context.SchoolProfile
                                              where s.DistrictID == districtObj.DistrictID
                                              && s.SchoolProfileID == schoolId
                                              select s).FirstOrDefault();
            if (schoolPorifleObj == null) return 2; //truong khong co trong quan huyen

            return 0; // khong co loi , cho phep tra cuu
        }


        #endregion

        #region Thanh toan phi dich vu SMSEDU qua Bankplus
        /// <summary>
        /// Lay thong tin tai khoan Khach hang
        /// </summary>
        /// <author date="13/10/2015">AnhVD9</author>
        /// <param name="UserName"></param>
        /// <returns></returns>
        public EducationUnitDefault GetAccountInfoByUserName(string UserName)
        {
            EducationUnitDefault response = new EducationUnitDefault();
            try
            {
                MembershipUser objMembershipUser = Membership.GetUser(UserName);
                Guid userId = (Guid)objMembershipUser.ProviderUserKey;
                UserAccount objUserAccount = UserAccountBusiness.GetUserByProviderUserKey(userId);

                // Nếu là admin (trường hoặc Phòng Sở)
                if (objUserAccount.IsAdmin)
                {
                    SchoolProfile sp = SchoolProfileBusiness.All.FirstOrDefault(o => o.AdminID == objUserAccount.UserAccountID);
                    if (sp != null)
                    {
                        response.EducationUnitID = sp.SchoolProfileID;
                        response.EducationUnitCode = sp.SchoolCode;
                        response.EducationUnitName = sp.SchoolName;
                        response.EducationUnitType = 1;
                        response.IsActive = sp.IsActive;
                        response.IsActiveSmas = sp.IsActiveSMAS.HasValue ? sp.IsActiveSMAS.Value : false;
                        response.SMSActiveType = sp.SMSTeacherActiveType;
                        response.IsApproved = objMembershipUser.IsApproved;
                        response.ValidationCode = WCFConstant.RESPONSE_SUCCESS;

                    }
                    else
                    {
                        SupervisingDept sd = SupervisingDeptBusiness.All.FirstOrDefault(o => o.AdminID == objUserAccount.UserAccountID);
                        if (sd != null)
                        {
                            response.EducationUnitID = sd.SupervisingDeptID;
                            response.EducationUnitCode = sd.SupervisingDeptCode;
                            response.EducationUnitName = sd.SupervisingDeptName;
                            response.EducationUnitType = 2;
                            response.IsActive = sd.IsActive;
                            response.IsActiveSmas = sd.IsActiveSMAS.HasValue ? sd.IsActiveSMAS.Value : false;
                            response.SMSActiveType = sd.SMSActiveType.HasValue ? sd.SMSActiveType.Value : 0;
                            response.IsApproved = objMembershipUser.IsApproved;
                            response.ValidationCode = WCFConstant.RESPONSE_SUCCESS;
                        }
                    }
                }
            }
            catch
            {
                //gan validationCode Exception
                response.ValidationCode = WCFConstant.RESPONSE_EXCEPTION;
            }
            return response;
        }
        #endregion

        #region 20151110 - AnhVD9 - Báo cáo phục vụ Thống kê - Điều hành
        /// <summary>
        /// Lấy báo cáo hiện trạng SMAS - phục vụ Thống kê - Điều hành
        /// </summary>
        /// <author date="11/09/2015">AnhVD9</author
        /// <param name="ProvinceId"></param>
        /// <param name="DistrictId"></param>
        /// <param name="Semester"></param>
        /// <param name="Year"></param>
        /// <returns></returns>
        public ListReportObjectResponse GetCurrentSituationSMASReport(int ProvinceId, int DistrictId, int Semester, int Year)
        {
            ListReportObjectResponse objResultResponse = new ListReportObjectResponse();
            try
            {
                objResultResponse.ValidationCode = WCFConstant.RESPONSE_SUCCESS;
                objResultResponse.LstReportObjectDefault = this.context.GetCurrentSituationSMASReport(ProvinceId, DistrictId, Semester, Year).Select(o => new ReportObjectDefault
                {
                    SchoolID = o.SchoolID,
                    ProvinceName = o.ProvinceName,
                    DistrictName = o.DistrictName,
                    SchoolName = o.SchoolName,
                    UserName = o.UserName,

                    SoLopKhaiBao = o.SoLopKhaiBao,
                    SoGVKhaiBao = o.SoGVKhaiBao,
                    SoHSKhaiBao = o.SoHSKhaiBao,
                    SoMonHocKhaiBao = o.SoMonHocKhaiBao,
                    SoGVPhanCong = o.SoGVPhanCong,
                    SoHSXLHK1 = o.SoHSXLHK1,
                    SoHSXLHK2 = o.SoHSXLHK2,
                    SoHSXLCaNam = o.SoHSXLCaNam
                }).ToList();
            }
            catch
            {
                //gan validationCode Exception
                objResultResponse.ValidationCode = WCFConstant.RESPONSE_EXCEPTION;
            }
            return objResultResponse;
        }
        #endregion

        #region Cấu hình gói cước
        /// <summary>
        /// lấy danh sách mã trường để cấu hình
        /// </summary>
        /// <author date="27/02/2016">Anhvd9</author>
        /// <param name="req"></param>
        /// <returns></returns>
        public List<int> GetListSchoolID(SchoolRequest req)
        {
            int provinceId = req.ProvinceID;
            int districtId = req.DistrictID;
            string schoolName = req.SchoolName.ToLower();
            string userName = req.UserName.ToLower();
            try
            {
                IQueryable<SchoolProfileDefault> iqSchools = (from sp in SchoolProfileBusiness.All
                                                              join ua in UserAccountBusiness.All on sp.AdminID equals ua.UserAccountID
                                                              join us in Aspnet_UsersBusiness.All on ua.GUID equals us.UserId
                                                              where sp.IsActive && sp.IsActiveSMAS.HasValue && sp.IsActiveSMAS.Value
                                                              && ua.IsActive
                                                              select new SchoolProfileDefault()
                                                              {
                                                                  SchoolProfileID = sp.SchoolProfileID,
                                                                  DistrictID = sp.DistrictID.HasValue ? sp.DistrictID.Value : 0,
                                                                  ProvinceID = sp.ProvinceID.HasValue ? sp.ProvinceID.Value : 0,
                                                                  SchoolName = sp.SchoolName,
                                                                  SchoolUserName = us.LoweredUserName
                                                              });
                if (provinceId > 0) iqSchools = iqSchools.Where(o => o.ProvinceID == provinceId);
                if (districtId > 0) iqSchools = iqSchools.Where(o => o.DistrictID == districtId);
                if (!string.IsNullOrEmpty(schoolName)) iqSchools = iqSchools.Where(o => o.SchoolName.ToLower().Contains(schoolName));
                if (!string.IsNullOrEmpty(userName)) iqSchools = iqSchools.Where(o => o.SchoolUserName.ToLower().Contains(userName));

                return iqSchools.Select(o => o.SchoolProfileID).ToList();
            }
            catch (Exception ex)
            {
                LogExtensions.GetCurrentClass = System.Reflection.MethodBase.GetCurrentMethod().DeclaringType;
                string para = string.Format("provinceId={0}, districtId={1}, schoolName={2},userName={3} ", provinceId, districtId, schoolName, userName);
                LogExtensions.ErrorExt(logger, DateTime.Now, para, ex.Message, "null");
                return null;
            }
        }

        /// <summary>
        /// Lấy danh sách thông tin trường có phân trang 
        /// </summary>
        /// <author date="27/02/2016">Anhvd9</author>
        /// <param name="req"></param>
        /// <returns></returns>
        public ListSchoolResponse GetListSchool(SchoolRequest req)
        {
            int provinceId = req.ProvinceID;
            int districtId = req.DistrictID;
            string schoolName = req.SchoolName.ToLower();
            string userName = req.UserName.ToLower();
            int status = req.Status;
            List<int> lstSchoolId = req.LstSchoolID;

            int currentPage = req.Page;
            int pageSize = req.PageSize;

            ListSchoolResponse result = new ListSchoolResponse();
            try
            {
                IQueryable<SchoolProfileDefault> iqSchools = (from sp in SchoolProfileBusiness.All
                                                              join ua in UserAccountBusiness.All on sp.AdminID equals ua.UserAccountID
                                                              join us in Aspnet_UsersBusiness.All on ua.GUID equals us.UserId
                                                              where sp.IsActive && sp.IsActiveSMAS.HasValue && sp.IsActiveSMAS.Value
                                                              && ua.IsActive
                                                              select new SchoolProfileDefault()
                                                              {
                                                                  SchoolProfileID = sp.SchoolProfileID,
                                                                  Address = sp.Address,
                                                                  CommuneID = sp.CommuneID.HasValue ? sp.CommuneID.Value : 0,
                                                                  CommuneName = sp.CommuneID.HasValue ? sp.Commune.CommuneName : string.Empty,
                                                                  DistrictID = sp.DistrictID.HasValue ? sp.DistrictID.Value : 0,
                                                                  DistrictName = sp.DistrictID.HasValue ? sp.District.DistrictName : string.Empty,
                                                                  EducationGrade = sp.EducationGrade,
                                                                  Email = sp.Email,
                                                                  EstablishedDate = sp.EstablishedDate.HasValue ? sp.EstablishedDate.Value : new DateTime(),
                                                                  HeadMasterName = sp.HeadMasterName,
                                                                  HeadMasterPhone = sp.HeadMasterPhone,
                                                                  Telephone = sp.Telephone,
                                                                  ProvinceID = sp.ProvinceID.HasValue ? sp.ProvinceID.Value : 0,
                                                                  ProvinceName = sp.ProvinceID.HasValue ? sp.Province.ProvinceName : string.Empty,
                                                                  SchoolCode = sp.SchoolCode,
                                                                  SchoolName = sp.SchoolName,
                                                                  AreaID = sp.AreaID.HasValue ? sp.AreaID.Value : 0,
                                                                  SchoolUserName = us.LoweredUserName,
                                                                  Website = sp.Website
                                                              });
                if (provinceId > 0) iqSchools = iqSchools.Where(o => o.ProvinceID == provinceId);
                if (districtId > 0) iqSchools = iqSchools.Where(o => o.DistrictID == districtId);
                if (!string.IsNullOrEmpty(schoolName)) iqSchools = iqSchools.Where(o => o.SchoolName.ToLower().Contains(schoolName));
                if (!string.IsNullOrEmpty(userName)) iqSchools = iqSchools.Where(o => o.SchoolUserName.ToLower().Contains(userName));
                if (lstSchoolId != null && lstSchoolId.Count > 0)
                {
                    if (status == 2) iqSchools = iqSchools.Where(o => !lstSchoolId.Contains(o.SchoolProfileID));
                    else iqSchools = iqSchools.Where(o => lstSchoolId.Contains(o.SchoolProfileID));
                }

                result.ValidationCode = WCFConstant.RESPONSE_SUCCESS;
                result.Total = iqSchools.Count();
                result.LstSchoolProfile = iqSchools.OrderBy(o => o.ProvinceName).ThenBy(o => o.DistrictName).ThenBy(o => o.SchoolName)
                                            .Skip((currentPage - 1) * pageSize).Take(pageSize).ToList();
            }
            catch (Exception ex)
            {
                LogExtensions.GetCurrentClass = System.Reflection.MethodBase.GetCurrentMethod().DeclaringType;
                string para = string.Format("provinceId={0}, districtId={1}, schoolName={2},userName={3} ", provinceId, districtId, schoolName, userName);
                LogExtensions.ErrorExt(logger, DateTime.Now, para, ex.Message, "null");
                result.ValidationCode = WCFConstant.RESPONSE_EXCEPTION;
            }

            return result;
        }
        #endregion

        #region 20160302 Anhvd9 - Bổ sung 2 bản tin liên quan tới Kỳ thi
        /// <summary>
        /// Lấy thông tin chi tiết của kỳ thi
        /// </summary>
        /// <param name="req"></param>
        /// <returns></returns>
        public ListExamResponse GetExamOfSchool(ExaminationRequest req)
        {
            int schoolID = req.SchoolID;
            int academicYearID = req.AcademicYearID;
            int semester = req.Semester;
            long examID = req.ExaminationID;
            int appliedLevel = req.AppliedLevel;
            ListExamResponse result = new ListExamResponse();
            try
            {
                DeclareBusiness();
                List<ExaminationResponse> lstExam = (from ex in ExaminationsBusiness.All
                                                     where (examID == 0 || ex.ExaminationsID == examID)
                                                     && ex.SchoolID == schoolID && ex.AcademicYearID == academicYearID
                                                     && ex.AppliedLevel == appliedLevel
                                                     && ex.SemesterID == semester //&& ex.MarkClosing == true
                                                     orderby ex.CreateTime descending
                                                     select new ExaminationResponse()
                                                     {
                                                         ExaminationID = ex.ExaminationsID,
                                                         ExaminationName = ex.ExaminationsName,
                                                         SchoolID = ex.SchoolID,
                                                         AcademicYearID = ex.AcademicYearID,
                                                         Semester = ex.SemesterID,
                                                         MarkInput = ex.MarkInput.HasValue ? ex.MarkInput.Value : false,
                                                         MarkClosing = ex.MarkClosing.HasValue ? ex.MarkClosing.Value : false
                                                     }).ToList();
                if (examID > 0)
                {
                    lstExam[0].LstExamSubject = (from es in ExamSubjectBusiness.All
                                                 join su in SubjectCatBusiness.All on es.SubjectID equals su.SubjectCatID
                                                 where es.ExaminationsID == examID
                                                 select new ExamSubjectResponse()
                                                 {
                                                     ExamSubjectID = es.ExamSubjectID,
                                                     ExaminationID = es.ExaminationsID,
                                                     SubjectID = es.SubjectID,
                                                     SubjectName = su.SubjectName,
                                                     ScheduleContent = es.SchedulesExam
                                                 }).ToList();

                }

                result.ValidationCode = WCFConstant.RESPONSE_SUCCESS;
                result.LstExamination = lstExam;
            }
            catch (Exception ex)
            {
                LogExtensions.GetCurrentClass = System.Reflection.MethodBase.GetCurrentMethod().DeclaringType;
                string para = string.Format("schoolID={0}, academicYearID={1}, examID={2},appliedLevel={3} ", schoolID, academicYearID, examID, appliedLevel);
                LogExtensions.ErrorExt(logger, DateTime.Now, para, ex.Message, "null");
                result.ValidationCode = WCFConstant.RESPONSE_EXCEPTION;
            }
            return result;
        }
        #endregion

        #region 20160421 Anhvd9 - lấy danh sách tổ bộ môn
        /// <summary>
        /// lay danh sach to bo mon
        /// </summary>
        /// <param name="SchoolID"></param>
        /// <returns></returns>
        public List<SchoolFacultyDefault> getSchoolFalculty(int SchoolID)
        {
            List<SchoolFacultyDefault> lstFaculty = new List<SchoolFacultyDefault>();
            try
            {
                lstFaculty = SchoolFacultyBusiness.SearchBySchool(SchoolID, new Dictionary<string, object>() { })
                    .Select(o => new SchoolFacultyDefault
                    {
                        SchoolID = o.SchoolID,
                        SchoolFacultyID = o.SchoolFacultyID,
                        FacultyName = o.FacultyName,
                        IsActive = o.IsActive,
                        CreateTime = o.CreatedDate.HasValue ? o.CreatedDate.Value : new DateTime()
                    }).OrderBy(o => o.FacultyName).ToList();

            }
            catch (Exception ex)
            {
                LogExtensions.GetCurrentClass = System.Reflection.MethodBase.GetCurrentMethod().DeclaringType;
                string para = string.Format("SchoolID={0}", SchoolID);
                LogExtensions.ErrorExt(logger, DateTime.Now, para, ex.Message, "null");
            }
            return lstFaculty;
        }
        #endregion

        /// <summary>
        /// lay thong tin tinh/thanh, quan/huyen
        /// </summary>
        /// <param name="provinceID"></param>
        /// <param name="districtID"></param>
        /// <returns></returns>
        public DistrictResponse GetDistrictByID(int provinceID, int districtID)
        {
            DistrictResponse response = new DistrictResponse();
            DistrictDefault district = new DistrictDefault();
            try
            {
                var pObj = ProvinceBusiness.Find(provinceID);
                if (pObj != null)
                {
                    district.ProvinceID = pObj.ProvinceID;
                    district.ProvinceName = pObj.ProvinceName;
                    district.ProvinceCode = pObj.ProvinceCode;
                    district.P_ShortName = pObj.ShortName;
                    district.P_Description = pObj.Description;
                }
                var dObj = DistrictBusiness.Find(districtID);
                if (dObj != null)
                {
                    district.DistrictID = dObj.DistrictID;
                    district.DistrictName = dObj.DistrictName;
                    district.DistrictCode = dObj.DistrictCode;
                    district.ShortName = dObj.ShortName;
                    district.Description = dObj.Description;
                }

                //Gan validation code
                response.ValidationCode = WCFConstant.RESPONSE_SUCCESS;
                //gan gia tri thuoc tinh dinh kem 
                response.ObjDistrict = district;
            }
            catch (Exception ex)
            {
                LogExtensions.GetCurrentClass = System.Reflection.MethodBase.GetCurrentMethod().DeclaringType;
                string para = string.Format("provinceID={0}, districtID={1}", provinceID, districtID);
                LogExtensions.ErrorExt(logger, DateTime.Now, para, ex.Message, "null");
                response.ValidationCode = WCFConstant.RESPONSE_EXCEPTION;
            }
            return response;

        }


        public SchoolProfileResponse GetUnitByAdminAccount(string UserName)
        {
            SchoolProfileResponse objSchoolProfileResponse = new SchoolProfileResponse();
            try
            {
                MembershipUser objMembershipUser;
                objMembershipUser = Membership.GetUser(UserName);
                Guid userId = (Guid)objMembershipUser.ProviderUserKey;
                UserAccount objUserAccount = UserAccountBusiness.GetUserByProviderUserKey(userId);
                SchoolProfile sp = null;
                SupervisingDept sd = null;
                SchoolProfileDefault spd = null;
                // Nếu là admin trường
                if (objUserAccount.IsAdmin)
                {
                    sp = SchoolProfileBusiness.All.FirstOrDefault(o => o.AdminID == objUserAccount.UserAccountID);
                    sd = SupervisingDeptBusiness.All.FirstOrDefault(o=>o.AdminID == objUserAccount.UserAccountID);
                    if (sp != null)
                    {
                        spd = new SchoolProfileDefault
                        {
                            SchoolProfileID = sp.SchoolProfileID,
                            SchoolName = sp.SchoolName,
                            ProvinceName = sp.Province!=null?sp.Province.ProvinceName:string.Empty,

                        };
    }
                    else if (sd != null)
                    {
                        spd = new SchoolProfileDefault
                        {
                            SupervisingDeptID = sd.SupervisingDeptID,
                            SupervisingDeptName = sd.SupervisingDeptName,
                            ProvinceName = sd.Province != null ? sd.Province.ProvinceName : string.Empty
                        };
                    }
                }

                //gan validationCode thanh cong
                objSchoolProfileResponse.ValidationCode = WCFConstant.RESPONSE_SUCCESS;
                //gan gia tri thuoc tinh dinh kem 
                objSchoolProfileResponse.ObjSchoolProfileDefault = spd;
            }
            catch
            {
                //gan validationCode Exception
                objSchoolProfileResponse.ValidationCode = WCFConstant.RESPONSE_EXCEPTION;
            }
            return objSchoolProfileResponse;
        }

        public ListSubjectCatLiteResponse GetListSubjectByClass(int academicYearId, int schoolId, int classId, int semester)
        {
            ListSubjectCatLiteResponse res = new ListSubjectCatLiteResponse();
            try
            {
                IDictionary<string, object> dic = new Dictionary<string, object>();
                dic["ClassID"] = classId;
                dic["AcademicYearID"] = academicYearId;
                dic["Semester"] = semester;

                res.LstSubjectCatLite = this.ClassSubjectBusiness.SearchBySchool(schoolId, dic).Select(o => new SubjectCatLite
                {
                    SubjectID = o.SubjectID,
                    SubjectName = o.SubjectCat.DisplayName
                }).ToList();
                res.ValidationCode = WCFConstant.RESPONSE_SUCCESS;
            }
            catch (Exception ex)
            {
                LogExtensions.GetCurrentClass = System.Reflection.MethodBase.GetCurrentMethod().DeclaringType;
                string para = string.Empty;
                LogExtensions.ErrorExt(logger, DateTime.Now, para, ex.Message, "null");
                res.ValidationCode = WCFConstant.RESPONSE_EXCEPTION;
            }
            return res;
        }
    }
}
