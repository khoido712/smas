﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;
using SMAS.Business.IBusiness;
using SMAS.Models.Models;
using SMAS.Business.Common;
using log4net;
using SMAS.Business.Business;
using WCF.Composite;
using System.Web.Security;
using WCF.Composite.Sync;
using SMAS.VTUtils.Log;

namespace WCF
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the class name "EmployeeModule" in code, svc and config file together.
    // NOTE: In order to launch WCF Test Client for testing this service, please select EmployeeModule.svc or EmployeeModule.svc.cs at the Solution Explorer and start debugging.
    public class EmployeeModule : IEmployeeModule
    {
        #region CONSTANT
        private const string DELIMITER_SIGN = "#";
        #endregion

        #region Declare Variable
        private IEmployeeBusiness EmployeeBusiness;
        private IClassProfileBusiness ClassProfileBusiness;
        private IAcademicYearBusiness AcademicYearBusiness;
        private IUserAccountBusiness UserAccountBusiness;
        private ISchoolProfileBusiness SchoolProfileBusiness;
        private ITeachingAssignmentBusiness TeachingAssignmentBusiness;
        private ISubjectCatBusiness SubjectCatBusiness;
        private IHeadTeacherSubstitutionBusiness HeadTeacherSubstitutionBusiness;
        private IClassSupervisorAssignmentBusiness ClassSupervisorAssignmentBusiness;
        private IWorkGroupTypeBusiness WorkGroupTypeBusiness;
        private IWorkTypeBusiness WorkTypeBusiness;
        private IEducationLevelBusiness EducationLevelBusiness;
        private Iaspnet_UsersBusiness aspnet_UsersBusiness;
        private static readonly log4net.ILog logger = LogManager.GetLogger(typeof(EmployeeModule));
        /// <summary>
        /// Khoi tao business cung context
        /// </summary>
        private void DeclareBusiness()
        {
            SMASEntities context = new SMASEntities();
            EmployeeBusiness = new EmployeeBusiness(logger, context);
            ClassProfileBusiness = new ClassProfileBusiness(logger, context);
            AcademicYearBusiness = new AcademicYearBusiness(logger, context);
            UserAccountBusiness = new UserAccountBusiness(logger, context);
            SchoolProfileBusiness = new SchoolProfileBusiness(logger, context);
            TeachingAssignmentBusiness = new TeachingAssignmentBusiness(logger, context);
            SubjectCatBusiness = new SubjectCatBusiness(logger, context);
            HeadTeacherSubstitutionBusiness = new HeadTeacherSubstitutionBusiness(logger, context);
            ClassSupervisorAssignmentBusiness = new ClassSupervisorAssignmentBusiness(logger, context);
            WorkGroupTypeBusiness = new WorkGroupTypeBusiness(logger, context);
            WorkTypeBusiness = new WorkTypeBusiness(logger, context);
            EducationLevelBusiness = new EducationLevelBusiness(logger, context);
            aspnet_UsersBusiness = new aspnet_UsersBusiness(logger, context);
        }

        #endregion

        #region Function mapping interface
        /// <summary>
        /// lấy giáo viên chủ nhiệm của lớp theo ưu tiên, 
        /// nếu có tồn tại giáo viên làm thay trong thời gian hiện tại thì trả về làm thay, 
        /// ngược lại lấy giao viên chủ nhiệm trong khai báo lớp học
        /// bổ sung giáo viên có quyền giáo là GVCN 
        /// </summary>
        /// <modifier date="14/11/2014">AnhVD9</modifier>
        /// <param name="ClassID"></param>
        /// <param name="AcademicYearID"></param>
        /// <param name="SchoolID"></param>
        /// <returns>neu khong ton tai return 0</returns>
        public IntResultResponse GetHeadTeacherID(int ClassID, int AcademicYearID, int SchoolID)
        {

            IntResultResponse res = new IntResultResponse();
            try
            {
                //Khoi tao Business
                DeclareBusiness();
                int ReturnValue = 0;
                DateTime dateTimeLast = DateTime.Now.Date;
                int? headTeacherID = 0;
                HeadTeacherSubstitution objHeadTeachSubstitution = (from t in HeadTeacherSubstitutionBusiness.All
                                                                    where t.SchoolID == SchoolID
                                                                    && t.AcademicYearID == AcademicYearID
                                                                    && t.ClassID == ClassID
                                                                    select t).FirstOrDefault();
                if (objHeadTeachSubstitution != null)
                {
                    if (objHeadTeachSubstitution.SubstituedHeadTeacher > 0 && (objHeadTeachSubstitution.AssignedDate <= dateTimeLast && dateTimeLast <= objHeadTeachSubstitution.EndDate))
                    {
                        headTeacherID = objHeadTeachSubstitution.SubstituedHeadTeacher;
                    }
                }

                if (headTeacherID.HasValue && headTeacherID.Value > 0)
                {
                    ReturnValue = headTeacherID.Value;
                }
                else
                {
                    ClassProfile objClassProfile = ClassProfileBusiness.Find(ClassID);
                    if (objClassProfile != null && objClassProfile.HeadTeacherID != null)
                    {
                        ReturnValue = objClassProfile.HeadTeacherID.Value;
                    }
                    // AnhVD9 - Bổ sung quyền giáo vụ là GVCN
                    if (ReturnValue == 0)
                    {
                        ClassSupervisorAssignment objClassSuperAssignment = (from csa in ClassSupervisorAssignmentBusiness.All
                                                                             where csa.SchoolID == SchoolID
                                                                             && csa.AcademicYearID == AcademicYearID
                                                                             && csa.ClassID == ClassID
                                                                             && csa.PermissionLevel == GlobalConstants.SUPERVISING_PERMISSION_HEAD_TEACHER
                                                                             select csa).FirstOrDefault();
                        if (objClassSuperAssignment != null)
                        {
                            ReturnValue = objClassSuperAssignment.TeacherID;
                        }
                    }
                }

                res.ValidationCode = WCFConstant.RESPONSE_SUCCESS;
                res.Result = ReturnValue;
            }
            catch (Exception ex)
            {
                string method = string.Format("{0}.{1}", System.Reflection.MethodBase.GetCurrentMethod().DeclaringType.FullName, System.Reflection.MethodBase.GetCurrentMethod().Name);
                string paraList = string.Format("ClassID={0},  AcademicYearID={1}, SchoolID={2}", ClassID, AcademicYearID, SchoolID);
                LogExtensions.ErrorExt(logger, DateTime.Now, method, paraList, ex.Message, "null");
                //gan validationCode exception
                res.ValidationCode = WCFConstant.RESPONSE_EXCEPTION;
            }
            return res;
        }


        /// <summary>
        /// lay danh sach GVCN theo danh sach lop cho truoc
        /// </summary>
        /// <author date="21/09/2015">AnhVD9</author>
        /// <param name="ClassID"></param>
        /// <param name="AcademicYearID"></param>
        /// <param name="SchoolID"></param>
        /// <returns></returns>
        public ListEmployeeLiteResponse GetHeadTeacherByClassIDs(List<int> LstClassID, int AcademicYearID, int SchoolID)
        {
            ListEmployeeLiteResponse res = new ListEmployeeLiteResponse();
            try
            {
                DeclareBusiness();
                DateTime dateTimeLast = DateTime.Now.Date;
                List<EmployeeLite> Teachers = (from t in HeadTeacherSubstitutionBusiness.All
                                               where t.SchoolID == SchoolID
                                                   && t.AcademicYearID == AcademicYearID
                                                   && LstClassID.Contains(t.ClassID)
                                                   && t.AssignedDate <= dateTimeLast
                                                   && t.EndDate >= dateTimeLast
                                                   && t.SubstituedHeadTeacher > 0
                                               select new EmployeeLite()
                                               {
                                                   EmployeeID = t.SubstituedHeadTeacher,
                                                   ClassID = t.ClassID,
                                                   FullName = t.Employee.FullName,
                                                   Mobile = t.Employee.Mobile
                                               }).ToList();

                List<int> notSubtituedTeacherIds = LstClassID.Except(Teachers.Select(o => o.ClassID).ToList()).ToList();

                if (notSubtituedTeacherIds.Count > 0)
                {
                    Teachers = Teachers.Union(ClassProfileBusiness.All.Where(o => notSubtituedTeacherIds.Contains(o.ClassProfileID)
                                 && o.HeadTeacherID > 0).Select(o => new EmployeeLite()
                                 {
                                     EmployeeID = o.HeadTeacherID.Value,
                                     ClassID = o.ClassProfileID,
                                     FullName = o.Employee.FullName,
                                     Mobile = o.Employee.Mobile
                                 }).ToList()).ToList();

                    notSubtituedTeacherIds = LstClassID.Except(Teachers.Select(o => o.ClassID).ToList()).ToList();

                    if (notSubtituedTeacherIds.Count > 0)
                    {
                        Teachers = Teachers.Union((from csa in ClassSupervisorAssignmentBusiness.All
                                                   where csa.SchoolID == SchoolID
                                                   && csa.AcademicYearID == AcademicYearID
                                                   && notSubtituedTeacherIds.Contains(csa.ClassID)
                                                   && csa.PermissionLevel == GlobalConstants.SUPERVISING_PERMISSION_HEAD_TEACHER
                                                   select new EmployeeLite()
                                                   {
                                                       EmployeeID = csa.TeacherID,
                                                       ClassID = csa.ClassID,
                                                       FullName = csa.Employee.FullName,
                                                       Mobile = csa.Employee.Mobile
                                                   }).ToList()).ToList();
                    }
                }
                res.ValidationCode = WCFConstant.RESPONSE_SUCCESS;
                res.LstEmployeeLite = Teachers;
            }
            catch (Exception ex)
            {
                string method = string.Format("{0}.{1}", System.Reflection.MethodBase.GetCurrentMethod().DeclaringType.FullName, System.Reflection.MethodBase.GetCurrentMethod().Name);
                string para = string.Format("AcademicYearID={0}, SchoolID={1}", AcademicYearID, SchoolID);
                LogExtensions.ErrorExt(logger, DateTime.Now, method, para, ex.Message, "null");
                res.ValidationCode = WCFConstant.RESPONSE_EXCEPTION;
            }
            return res;
        }

        public ListHeadTeacherByClassReponse GetListHeadTeacherByEducationLevelID(int EducationLevelID, int AcademicYearID, int SchoolID)
        {
            ListHeadTeacherByClassReponse objHeadTeacherByClass = new ListHeadTeacherByClassReponse();
            try
            {
                //Khoi tao Business
                DeclareBusiness();
                int ReturnValue = 0;
                int? headTeacherID = 0;
                DateTime dateTimeLast = DateTime.Now.Date;

                List<HeadTeacherSubstitution> lstHeadTeacherSubstitution = (from t in HeadTeacherSubstitutionBusiness.All
                                                                            where t.SchoolID == SchoolID
                                                                            && t.AcademicYearID == AcademicYearID
                                                                            && t.ClassProfile.EducationLevelID == EducationLevelID
                                                                            select t).ToList();

                List<ClassProfile> lstClassProfile = (from cp in ClassProfileBusiness.All
                                                      where cp.SchoolID == SchoolID
                                                      && cp.AcademicYearID == AcademicYearID
                                                      && cp.EducationLevelID == EducationLevelID
                                                      select cp).ToList();

                HeadTeacherSubstitution objHeadTeacherSubstitution = null;
                List<HeadTeacher> lstHead = new List<HeadTeacher>();
                HeadTeacher objHead = null;
                ClassProfile objClassProfile = null;

                for (int i = 0; i < lstClassProfile.Count; i++)//duyet tung lop cua khoi de lay GVCN tuong ung voi tung lop
                {
                    ReturnValue = 0;
                    objHead = new HeadTeacher();
                    objClassProfile = lstClassProfile[i];
                    objHeadTeacherSubstitution = lstHeadTeacherSubstitution.Where(e => e.ClassID == objClassProfile.ClassProfileID).FirstOrDefault();

                    if (objHeadTeacherSubstitution != null)
                    {
                        if (objHeadTeacherSubstitution.SubstituedHeadTeacher > 0 && (objHeadTeacherSubstitution.AssignedDate <= dateTimeLast && dateTimeLast <= objHeadTeacherSubstitution.EndDate))
                        {
                            headTeacherID = objHeadTeacherSubstitution.SubstituedHeadTeacher;
                        }
                    }

                    if (headTeacherID.HasValue && headTeacherID.Value > 0)
                    {
                        ReturnValue = headTeacherID.Value;
                    }
                    else
                    {
                        if (objClassProfile != null && objClassProfile.HeadTeacherID != null)
                        {
                            ReturnValue = objClassProfile.HeadTeacherID.Value;
                        }
                        // AnhVD9 - Bổ sung quyền giáo vụ là GVCN
                        if (ReturnValue == 0)
                        {
                            ClassSupervisorAssignment objClassSuperAssignment = (from csa in ClassSupervisorAssignmentBusiness.All
                                                                                 where csa.SchoolID == SchoolID
                                                                                 && csa.AcademicYearID == AcademicYearID
                                                                                 && csa.ClassID == objClassProfile.ClassProfileID
                                                                                 && csa.PermissionLevel == GlobalConstants.SUPERVISING_PERMISSION_HEAD_TEACHER
                                                                                 select csa).FirstOrDefault();
                            if (objClassSuperAssignment != null)
                            {
                                ReturnValue = objClassSuperAssignment.TeacherID;
                            }
                        }
                    }
                    objHead.ClassID = objClassProfile.ClassProfileID;
                    objHead.TeacherID = ReturnValue;
                    lstHead.Add(objHead);
                }
                objHeadTeacherByClass.ValidationCode = WCFConstant.RESPONSE_SUCCESS;
                objHeadTeacherByClass.LstHeadTeacherByClass = lstHead;

                return objHeadTeacherByClass;
            }
            catch (Exception ex)
            {
                string method = string.Format("{0}.{1}", System.Reflection.MethodBase.GetCurrentMethod().DeclaringType.FullName, System.Reflection.MethodBase.GetCurrentMethod().Name);
                string para = string.Format("int EducationLevelID={0}, int AcademicYearID={1}, int SchoolID={2}", EducationLevelID, AcademicYearID, SchoolID);
                LogExtensions.ErrorExt(logger, DateTime.Now, method, para, ex.Message, "null");
                //gan validationCode exception
                objHeadTeacherByClass.ValidationCode = WCFConstant.RESPONSE_EXCEPTION;
            }
            return objHeadTeacherByClass;
        }

        /// <summary>
        /// Lấy avatar của giáo viên theo ID
        /// </summary>
        /// <param name="TeacherID"></param>
        /// <returns>neu khong ton tai return null</returns>
        public ByteArrayResultResponse GetTeacherImage(int TeacherID)
        {//Khoi tao Business
            ByteArrayResultResponse res = new ByteArrayResultResponse();
            try
            {
                //Khoi tao Business
                DeclareBusiness();
                Employee objEmployee = EmployeeBusiness.Find(TeacherID);
                if (objEmployee != null)
                {
                    byte[] employeeImage = objEmployee.Image;
                    res.Result = employeeImage;
                    res.ValidationCode = WCFConstant.RESPONSE_SUCCESS;
                }
            }
            catch (Exception)
            {
                //gan validationCode exception
                res.ValidationCode = WCFConstant.RESPONSE_EXCEPTION;
            }
            return res;
        }

        /// <summary>
        /// Lấy danh sách giáo viên theo dk tìm kiếm nằm trong lstEmployeeID + SchoolID 
        /// </summary>
        /// <modify date="2014/02/18" modifier="HaiVT">them employeeCode</modifier>
        /// <param name="TeacherRequest"></param>
        /// <returns>Neu khong ton tai return new object</returns>
        public ListEmployeeLiteResponse GetListTeacherByCondition(TeacherRequest ObjTeacherRequest)
        {
            ListEmployeeLiteResponse listEmployeeLiteResponse = new ListEmployeeLiteResponse();
            try
            {
                //Khoi tao Business
                DeclareBusiness();
                int SchoolID = ObjTeacherRequest.SchoolID;
                List<int> lstTeacherID = ObjTeacherRequest.lstTeacherID;
                IQueryable<Employee> IQEmployee = EmployeeBusiness.SearchTeacher(SchoolID, new Dictionary<string, object>());
                IQueryable<EmployeeLite> IQFilterEmployee = from e in IQEmployee
                                                            join t in lstTeacherID on e.EmployeeID equals t
                                                            orderby e.Name
                                                            where e.EmployeeType == SystemParamsInFile.EMPLOYEE_TYPE_SCHOOL_TEACHER
                                                            select new EmployeeLite
                                                            {
                                                                EmployeeID = e.EmployeeID,
                                                                FullName = e.FullName,
                                                                EmployeeCode = e.EmployeeCode,
                                                                Mobile = e.Mobile,
                                                                ShortName = e.Name
                                                            };
                listEmployeeLiteResponse.LstEmployeeLite = IQFilterEmployee.ToList();
                listEmployeeLiteResponse.ValidationCode = WCFConstant.RESPONSE_SUCCESS;
            }
            catch (Exception)
            {
                //gan validationCode exception
                listEmployeeLiteResponse.ValidationCode = WCFConstant.RESPONSE_EXCEPTION;
            }
            return listEmployeeLiteResponse;
        }

        /// <summary>
        /// Lấy giáo viên theo ID
        /// </summary>
        /// <param name="TeacherID"></param>
        /// <returns>Neu khong ton tai return new object</returns>
        public EmployeeFullInfoResponse GetTeacherByID(int TeacherID)
        {
            EmployeeFullInfoResponse objEmployeeResponse = new EmployeeFullInfoResponse();
            try
            {
                //Khoi tao Business
                DeclareBusiness();
                Employee objEmployee = EmployeeBusiness.Find(TeacherID);
                //Neu object khong null va la phan loai giao vien
                if (objEmployee != null && objEmployee.EmployeeType == SystemParamsInFile.EMPLOYEE_TYPE_SCHOOL_TEACHER)
                {
                    EmployeeFullInfo objEmployeeInfo = new EmployeeFullInfo();
                    WCFUtils.CopyObject(objEmployee, objEmployeeInfo);
                    objEmployeeResponse.ValidationCode = WCFConstant.RESPONSE_SUCCESS;
                    objEmployeeResponse.ObjEmployeeFullInfo = objEmployeeInfo;
                }
            }
            catch (Exception)
            {
                //gan validationCode exception
                objEmployeeResponse.ValidationCode = WCFConstant.RESPONSE_EXCEPTION;
            }
            return objEmployeeResponse;
        }

        /// <summary>
        /// Lấy nhân viên phòng/sở theo ID
        /// </summary>
        /// <param name="EmployeeID"></param>
        /// <returns>Neu khong ton tai return new object</returns>
        public EmployeeFullInfoResponse GetEmployeeByID(int EmployeeID)
        {
            EmployeeFullInfoResponse objEmployeeResponse = new EmployeeFullInfoResponse();
            try
            {
                //Khoi tao Business
                DeclareBusiness();
                Employee objEmployee = EmployeeBusiness.Find(EmployeeID);
                //Neu object khong null va la phan loai giao vien
                if (objEmployee != null && objEmployee.EmployeeType == SystemParamsInFile.EMPLOYEE_TYPE_SUPERVISING_DEPARTMENT_STAFF)
                {
                    EmployeeFullInfo objEmployeeFull = new EmployeeFullInfo();
                    WCFUtils.CopyObject(objEmployee, objEmployeeFull);
                    objEmployeeResponse.ObjEmployeeFullInfo = objEmployeeFull;
                    objEmployeeResponse.ValidationCode = WCFConstant.RESPONSE_SUCCESS;
                }
            }
            catch (Exception)
            {
                //gan validationCode exception
                objEmployeeResponse.ValidationCode = WCFConstant.RESPONSE_EXCEPTION;
            }
            return objEmployeeResponse;
        }

        /// <summary>
        /// Danh sách lấy tất cả các giáo viên đang làm việc của trường
        /// </summary>
        /// <param name="SchoolID"></param>
        /// <param name="Status"></param>
        /// <returns>Neu khong ton tai return new list 0 phan tu</returns>
        public ListEmployeeFullInfoResponse GetAllTeacherInSchool(int SchoolID, int Status)
        {
            ListEmployeeFullInfoResponse listEmployeeFullInfoResponse = new ListEmployeeFullInfoResponse();
            try
            {
                //Khoi tao Business
                DeclareBusiness();
                IDictionary<string, object> dic = new Dictionary<string, object>();
                dic["CurrentEmployeeStatus"] = Status;
                dic["EmployeeType"] = SystemParamsInFile.EMPLOYEE_TYPE_SCHOOL_TEACHER;
                List<Employee> lstEmployee = EmployeeBusiness.SearchTeacher(SchoolID, dic).ToList();
                List<EmployeeFullInfo> lstEmployeeFull = new List<EmployeeFullInfo>();
                //Neu object khong null va la phan loai giao vien
                if (lstEmployee.Count() > 0)
                {
                    for (int i = 0; i < lstEmployee.Count(); i++)
                    {
                        EmployeeFullInfo objEmployeeFull = new EmployeeFullInfo();
                        WCFUtils.CopyObject(lstEmployee[i], objEmployeeFull);
                        lstEmployeeFull.Add(objEmployeeFull);
                    }
                }
                listEmployeeFullInfoResponse.LstEmployeeFullInfo = lstEmployeeFull;
                listEmployeeFullInfoResponse.ValidationCode = WCFConstant.RESPONSE_SUCCESS;
            }
            catch (Exception)
            {
                //gan validationCode exception
                listEmployeeFullInfoResponse.ValidationCode = WCFConstant.RESPONSE_EXCEPTION;
            }
            return listEmployeeFullInfoResponse;
        }

        /// <summary>
        /// Lấy danh sách giáo viên theo danh sách ID truyền vào
        /// </summary>
        /// <param name="lstEmplyeeID"></param>
        /// <returns>Neu khong ton tai return new object 0 phan tu</returns>
        public ListEmployeeResponse GetListTeacher(List<int> lstEmplyeeID)
        {
            ListEmployeeResponse listEmployeeResponse = new ListEmployeeResponse();
            try
            {
                //Khoi tao Business
                DeclareBusiness();
                if (lstEmplyeeID.Count == 0)
                {
                    listEmployeeResponse.ValidationCode = WCFConstant.RESPONSE_EMPLOYEEID_NOT_FOUND;
                    return listEmployeeResponse;
                }

                List<EmployeeDefault> lstFilterEmployee = (from e in EmployeeBusiness.All
                                                           join f in lstEmplyeeID on e.EmployeeID equals f
                                                           where e.EmployeeType == SystemParamsInFile.EMPLOYEE_TYPE_SCHOOL_TEACHER
                                                           select new EmployeeDefault
                                                           {
                                                               EmployeeID = e.EmployeeID,
                                                               EmployeeCode = e.EmployeeCode,
                                                               FullName = e.FullName,
                                                               Genre = e.Genre,
                                                               Name = e.Name,
                                                               Mobile = e.Mobile,
                                                               IsActive = e.IsActive,
                                                               Telephone = e.Telephone,
                                                               EmploymentStatus = e.EmploymentStatus.Value,
                                                               FacultyID = e.SchoolFaculty.SchoolFacultyID,
                                                               FacultyName = e.SchoolFaculty.FacultyName
                                                               //WorkGroupTypeID = e.WorkGroupTypeID.HasValue ? e.WorkGroupTypeID.Value : 0,
                                                               //WorkGroupTypeName = e.WorkGroupType != null ? e.WorkGroupType.Resolution : "",
                                                               //WorkTypeID = e.WorkTypeID.HasValue ? e.WorkTypeID.Value : 0,
                                                               //WorkTypeName = e.WorkType != null ? e.WorkType.Resolution : "",
                                                           }).ToList();
                listEmployeeResponse.ValidationCode = WCFConstant.RESPONSE_SUCCESS;
                listEmployeeResponse.LstEmployee = lstFilterEmployee;
            }
            catch (Exception)
            {
                //gan validationCode exception
                listEmployeeResponse.ValidationCode = WCFConstant.RESPONSE_EXCEPTION;
            }
            return listEmployeeResponse;
        }

        #region Lấy danh sách giáo viên dang lam viec theo danh sách ID truyền vào
        /// <summary>
        /// Lấy danh sách giáo viên dang lam viec theo danh sách ID truyền vào
        /// </summary>
        /// <param name="lstEmplyeeID"></param>
        /// <returns>Neu khong ton tai return new object 0 phan tu</returns>
        public ListEmployeeResponse GetListTeacherWorking(List<int> lstEmplyeeID)
        {
            ListEmployeeResponse listEmployeeResponse = new ListEmployeeResponse();
            try
            {
                //Khoi tao Business
                DeclareBusiness();
                if (lstEmplyeeID.Count == 0)
                {
                    listEmployeeResponse.ValidationCode = WCFConstant.RESPONSE_EMPLOYEEID_NOT_FOUND;
                    return listEmployeeResponse;
                }

                List<EmployeeDefault> lstFilterEmployee = (from e in EmployeeBusiness.All
                                                           join f in lstEmplyeeID on e.EmployeeID equals f
                                                           where e.EmployeeType == SystemParamsInFile.EMPLOYEE_TYPE_SCHOOL_TEACHER
                                                           && e.EmploymentStatus == SystemParamsInFile.EMPLOYMENT_STATUS_WORKING
                                                           && e.IsActive == true
                                                           select new EmployeeDefault
                                                           {
                                                               EmployeeID = e.EmployeeID,
                                                               EmployeeCode = e.EmployeeCode,
                                                               FullName = e.FullName,
                                                               Genre = e.Genre,
                                                               Name = e.Name,
                                                               Mobile = e.Mobile,
                                                               Telephone = e.Telephone,
                                                               EmploymentStatus = e.EmploymentStatus.Value
                                                           }).ToList();

                listEmployeeResponse.ValidationCode = WCFConstant.RESPONSE_SUCCESS;
                listEmployeeResponse.LstEmployee = lstFilterEmployee;
            }
            catch (Exception)
            {
                //gan validationCode exception
                listEmployeeResponse.ValidationCode = WCFConstant.RESPONSE_EXCEPTION;
            }
            return listEmployeeResponse;
        }
        #endregion

        /// <summary>
        /// Lấy danh sách tẩt cả giáo viên đang làm việc trong trường
        /// </summary>
        /// <param name="ObjTeacherOfSchool"></param>
        /// <returns>Neu khong ton tai return new object 0 phan tu</returns>
        public ListEmployeeResponse GetListTeacherOfSchool(TeacherOfSchoolRequest ObjTeacherOfSchool)
        {
            ListEmployeeResponse listEmployeeResponse = new ListEmployeeResponse();
            try
            {
                //Khoi tao Business
                DeclareBusiness();
                //Neu khong truyen nam hoc vao doi tuong tra ve new object 0 phan tu
                if (ObjTeacherOfSchool.AcademicYearID == 0)
                {
                    return listEmployeeResponse;
                }
                bool? isStatus = ObjTeacherOfSchool.IsStatus;
                IDictionary<string, object> dic = new Dictionary<string, object>();
                dic["AcademicYearID"] = ObjTeacherOfSchool.AcademicYearID;
                dic["EmployeeType"] = SystemParamsInFile.EMPLOYEE_TYPE_SCHOOL_TEACHER;
                if (!isStatus.HasValue)
                {
                    dic["CurrentEmployeeStatus"] = SystemParamsInFile.EMPLOYMENT_STATUS_WORKING;
                }
                dic["isFromEduActive"] = isStatus;
                IQueryable<Employee> lstEmployee = EmployeeBusiness.SearchTeacher(ObjTeacherOfSchool.SchoolID, dic);
                List<EmployeeDefault> lstEmployeeDefault = (from e in lstEmployee
                                                            select new EmployeeDefault
                                                            {
                                                                EmployeeID = e.EmployeeID,
                                                                EmployeeCode = e.EmployeeCode,
                                                                FullName = e.FullName,
                                                                EmployeeType = e.EmployeeType,
                                                                Genre = e.Genre,
                                                                Name = e.Name,
                                                                IsActive = e.IsActive,
                                                                Telephone = e.Telephone,
                                                                Mobile = e.Mobile,
                                                                EmploymentStatus = e.EmploymentStatus.Value,
                                                                //FacultyID = e.SchoolFacultyID.HasValue ? e.SchoolFacultyID.Value : 0,
                                                                //FacultyName = e.SchoolFacultyID.HasValue ? e.SchoolFaculty.FacultyName : string.Empty,
                                                                //WorkGroupTypeID = e.WorkGroupTypeID.HasValue ? e.WorkGroupTypeID.Value : 0,
                                                                //WorkGroupTypeName = e.WorkGroupType != null ? e.WorkGroupType.Resolution : "",
                                                                //WorkTypeID = e.WorkTypeID.HasValue ? e.WorkTypeID.Value : 0,
                                                                //WorkTypeName = e.WorkType != null ? e.WorkType.Resolution : ""
                                                            }).ToList().OrderBy(p => p.Name).ToList();
                listEmployeeResponse.ValidationCode = WCFConstant.RESPONSE_SUCCESS;
                listEmployeeResponse.LstEmployee = lstEmployeeDefault;
            }
            catch (Exception)
            {
                //gan validationCode exception
                listEmployeeResponse.ValidationCode = WCFConstant.RESPONSE_EXCEPTION;
            }
            return listEmployeeResponse;
        }

        #region AnhVD9 20151006 - Lấy danh sách GV toàn trường - Chuyển sang dạng list chuỗi
        /// <summary>
        /// Lấy danh sách tẩt cả giáo viên đang làm việc trong trường
        /// </summary>
        /// <author date="06/10/2015">AnhVD9</author>
        /// <param name="ObjTeacherOfSchool"></param>
        /// <returns>Neu khong ton tai return new object 0 phan tu</returns>
        public ListEmployeeResponse GetTeachersOfSchoolformatString(TeacherOfSchoolRequest ObjTeacherOfSchool)
        {
            ListEmployeeResponse listEmployeeResponse = new ListEmployeeResponse();
            try
            {
                //Khoi tao Business
                DeclareBusiness();
                //Neu khong truyen nam hoc vao doi tuong tra ve new object 0 phan tu
                if (ObjTeacherOfSchool.AcademicYearID == 0) return listEmployeeResponse;

                bool? isStatus = ObjTeacherOfSchool.IsStatus;
                IQueryable<EmployeeDefault> iqEmployee = (from e in EmployeeBusiness.All
                                                          where e.SchoolID == ObjTeacherOfSchool.SchoolID
                                                            && e.EmployeeType == SystemParamsInFile.EMPLOYEE_TYPE_SCHOOL_TEACHER
                                                            && e.EmploymentStatus == SystemParamsInFile.EMPLOYMENT_STATUS_WORKING
                                                            && e.IsActive == true
                                                          select new EmployeeDefault
                                                          {
                                                              EmployeeID = e.EmployeeID,
                                                              EmployeeCode = e.EmployeeCode,
                                                              FullName = e.FullName,
                                                              Name = e.Name,
                                                              Genre = e.Genre,
                                                              Mobile = e.Mobile,
                                                              EmploymentStatus = e.EmploymentStatus.Value,
                                                              IsActive = e.IsActive,
                                                              FacultyName = e.SchoolFaculty != null ? e.SchoolFaculty.FacultyName : null
                                                          }).OrderBy(p => p.Name);
                List<string> teachers = new List<string>();
                StringBuilder builder = null;
                foreach (EmployeeDefault item in iqEmployee)
                {
                    builder = new StringBuilder();
                    builder.Append(item.EmployeeID).Append(DELIMITER_SIGN)
                            .Append(item.EmployeeCode).Append(DELIMITER_SIGN)
                            .Append(item.FullName).Append(DELIMITER_SIGN)
                            .Append(item.Name).Append(DELIMITER_SIGN)
                            .Append(item.Genre).Append(DELIMITER_SIGN)
                            .Append(item.Mobile).Append(DELIMITER_SIGN)
                            .Append(item.EmploymentStatus).Append(DELIMITER_SIGN)
                            .Append(item.IsActive).Append(DELIMITER_SIGN)
                            .Append(item.FacultyName);
                    teachers.Add(builder.ToString());
                }
                listEmployeeResponse.ValidationCode = WCFConstant.RESPONSE_SUCCESS;
                listEmployeeResponse.LstEmployeeStr = teachers;
            }
            catch (Exception ex)
            {
                //gan validationCode exception
                listEmployeeResponse.ValidationCode = WCFConstant.RESPONSE_EXCEPTION;
            }
            return listEmployeeResponse;
        }
        #endregion

        #region đếm số lượng giáo viên đang làm việc trong trường
        /// <summary>
        /// đếm số lượng giáo viên đang làm việc trong trường
        /// </summary>
        /// <param name="ObjTeacherOfSchool"></param>
        /// <returns>Neu khong ton tai return 0</returns>
        public IntResultResponse CountTeacherOfSchool(int schoolID, int academicYearID)
        {
            IntResultResponse intResultResponse = new IntResultResponse();
            try
            {
                //Khoi tao Business
                DeclareBusiness();
                //Neu khong truyen nam hoc vao doi tuong tra ve new object 0 phan tu
                if (academicYearID == 0)
                {
                    intResultResponse.Result = 0;
                }
                else
                {
                    IDictionary<string, object> dic = new Dictionary<string, object>();
                    dic["AcademicYearID"] = academicYearID;
                    dic["EmployeeType"] = SystemParamsInFile.EMPLOYEE_TYPE_SCHOOL_TEACHER;
                    intResultResponse.Result = EmployeeBusiness.SearchTeacher(schoolID, dic).Count();
                }
                intResultResponse.ValidationCode = WCFConstant.RESPONSE_SUCCESS;
            }
            catch (Exception)
            {
                //gan validationCode exception
                intResultResponse.ValidationCode = WCFConstant.RESPONSE_EXCEPTION;
            }
            return intResultResponse;
        }
        #endregion

        /// <summary>
        /// Lấy ds giáo viên của trường theo khối học, theo list ID giáo viên truyền vào
        /// </summary>
        /// <param name="AcademicYear"></param>
        /// <param name="AppliedLevel"></param>
        /// <param name="lstTeacherID"></param>
        /// <returns>Neu khong ton tai return new object 0 phan tu</returns>
        public ListEmployeeLiteResponse GetListTeacherByAppliedLevel(int AcademicYearID, int AppliedLevel, List<int> lstTeacherID)
        {
            ListEmployeeLiteResponse listEmployeeLiteResponse = new ListEmployeeLiteResponse();
            try
            {
                //Khoi tao Business
                DeclareBusiness();
                AcademicYear objAcademicYear = AcademicYearBusiness.Find(AcademicYearID);
                IDictionary<string, object> dic = new Dictionary<string, object>();
                dic["AcademicYearID"] = AcademicYearID;
                dic["EmployeeType"] = SystemParamsInFile.EMPLOYEE_TYPE_SCHOOL_TEACHER;
                IQueryable<Employee> IQEmployee = EmployeeBusiness.SearchTeacher(objAcademicYear.SchoolID, dic);

                List<EmployeeLite> lstFilterEmployee = (from e in IQEmployee
                                                        join f in lstTeacherID on e.EmployeeID equals f
                                                        where e.AppliedLevel == AppliedLevel
                                                        select new EmployeeLite
                                                        {
                                                            EmployeeID = e.EmployeeID,
                                                            FullName = e.FullName
                                                        }).ToList();
                listEmployeeLiteResponse.ValidationCode = WCFConstant.RESPONSE_SUCCESS;
                listEmployeeLiteResponse.LstEmployeeLite = lstFilterEmployee;
            }
            catch (Exception)
            {
                //gan validationCode exception
                listEmployeeLiteResponse.ValidationCode = WCFConstant.RESPONSE_EXCEPTION;
            }

            return listEmployeeLiteResponse;
        }

        /// <summary>
        /// Lấy danh sách username theo list employeeID truyền vào
        /// </summary>
        /// <param name="objTeacherRequest"></param>
        /// <returns></returns>
        public ListAccountByTeacherResponse GetListUsernameByEmployeeID(TeacherRequest objTeacherRequest)
        {
            ListAccountByTeacherResponse objResult = new ListAccountByTeacherResponse();
            try
            {
                //Khoi tao Business
                DeclareBusiness();
                SchoolProfile objSchoolProfile = SchoolProfileBusiness.Find(objTeacherRequest.SchoolID);
                List<UserAccount> UserAccountList = UserAccountBusiness.GetUsersNormalCreatedBy(objSchoolProfile.AdminID.Value).ToList();

                List<AccountByTeacherID> lstAccountByTeacherID = (from a in UserAccountList
                                                                  join e in objTeacherRequest.lstTeacherID on a.EmployeeID equals e
                                                                  select new AccountByTeacherID
                                                                  {
                                                                      EmployeeID = a.EmployeeID.Value,
                                                                      Username = Membership.GetUser(a.GUID).UserName
                                                                  }).ToList();

                //gan validationCode thanh cong
                objResult.ValidationCode = WCFConstant.RESPONSE_SUCCESS;
                //gan gia tri thuoc tinh dinh kem 
                objResult.LstAccountByTeacher = lstAccountByTeacherID;
            }
            catch
            {
                //gan validationCode Exception
                objResult.ValidationCode = WCFConstant.RESPONSE_EXCEPTION;
            }
            return objResult;
        }

        public ListEmployeeLiteResponse GetListTeachingAssign(int academicYearID, int classID, int semesterID)
        {
            ListEmployeeLiteResponse lstEmployeeLiteResponse = new ListEmployeeLiteResponse();
            try
            {
                DeclareBusiness();
                List<EmployeeLite> lstEmployee = (from t in TeachingAssignmentBusiness.All
                                                  join e in EmployeeBusiness.All on t.TeacherID equals e.EmployeeID
                                                  join sc in SubjectCatBusiness.All on t.SubjectID equals sc.SubjectCatID
                                                  where t.AcademicYearID == academicYearID
                                                  && t.ClassID == classID && t.Semester == semesterID
                                                  orderby sc.OrderInSubject
                                                  select new EmployeeLite
                                                  {
                                                      EmployeeID = t.TeacherID,
                                                      FullName = e.FullName,
                                                      Mobile = e.Mobile,
                                                      SubjectID = sc.SubjectCatID,
                                                      SubjectName = sc.SubjectName
                                                  }).ToList();
                lstEmployeeLiteResponse.ValidationCode = WCFConstant.RESPONSE_SUCCESS;
                lstEmployeeLiteResponse.LstEmployeeLite = lstEmployee;
            }
            catch
            {
                lstEmployeeLiteResponse.ValidationCode = WCFConstant.RESPONSE_EXCEPTION;
            }
            return lstEmployeeLiteResponse;
        }


        /// <summary>
        /// Lay tat car useraccount khong quan tam den trang thai isactive
        /// </summary>
        /// <param name="objTeacherRequest"></param>
        /// <returns></returns>
        public ListAccountByTeacherResponse GetListFullUsernameByEmployeeID(TeacherRequest objTeacherRequest)
        {
            ListAccountByTeacherResponse objResult = new ListAccountByTeacherResponse();
            try
            {
                //Khoi tao Business
                DeclareBusiness();
                SchoolProfile objSchoolProfile = SchoolProfileBusiness.Find(objTeacherRequest.SchoolID);
                List<UserAccount> UserAccountList = UserAccountBusiness.All.Where(u => u.CreatedUserID == objSchoolProfile.AdminID.Value && u.IsAdmin == false).ToList();
                List<AccountByTeacherID> lstAccountByTeacherID = (from a in UserAccountList
                                                                  join e in objTeacherRequest.lstTeacherID on a.EmployeeID equals e
                                                                  select new AccountByTeacherID
                                                                  {
                                                                      EmployeeID = a.EmployeeID.Value,
                                                                      Username = Membership.GetUser(a.GUID).UserName
                                                                  }).ToList();

                //gan validationCode thanh cong
                objResult.ValidationCode = WCFConstant.RESPONSE_SUCCESS;
                //gan gia tri thuoc tinh dinh kem 
                objResult.LstAccountByTeacher = lstAccountByTeacherID;
            }
            catch
            {
                //gan validationCode Exception
                objResult.ValidationCode = WCFConstant.RESPONSE_EXCEPTION;
            }
            return objResult;

        }

        /// <summary>
        /// Lay danh sach loai cong viec cua can bo
        /// </summary>
        /// <param name="schoolId"></param>
        /// <returns></returns>
        public ListWorkGroupTypeResponse GetListWorkGroupType()
        {
            ListWorkGroupTypeResponse response = new ListWorkGroupTypeResponse();

            try
            {
                //Khoi tao Business
                DeclareBusiness();

                List<WCF.Composite.WorkGroupType> lstWorkGroupType = WorkGroupTypeBusiness.Search(new Dictionary<string, object>())
                                                                    .OrderBy(o => o.Resolution)
                                                                    .Select(o => new WCF.Composite.WorkGroupType
                                                                    {
                                                                        Resolution = o.Resolution,
                                                                        WorkGroupTypeID = o.WorkGroupTypeID
                                                                    }).ToList();


                //gan validationCode thanh cong
                response.ValidateCode = WCFConstant.RESPONSE_SUCCESS;
                //gan gia tri thuoc tinh dinh kem 
                response.LstWorkGroupType = lstWorkGroupType;
            }
            catch
            {
                //gan validationCode Exception
                response.ValidateCode = WCFConstant.RESPONSE_EXCEPTION;
            }
            return response;
        }

        /// <summary>
        /// Lay danh sach giao vien cho chuc nang nhom lien lac
        /// </summary>
        /// <param name="ObjTeacherOfSchool"></param>
        /// <returns></returns>
        public ListEmployeeResponse GetListTeacherOfSchoolForContactGroup(TeacherOfSchoolRequest ObjTeacherOfSchool)
        {
            ListEmployeeResponse listEmployeeResponse = new ListEmployeeResponse();
            try
            {
                //Khoi tao Business
                DeclareBusiness();
                //Neu khong truyen nam hoc vao doi tuong tra ve new object 0 phan tu
                if (ObjTeacherOfSchool.AcademicYearID == 0)
                {
                    return listEmployeeResponse;
                }
                bool? isStatus = ObjTeacherOfSchool.IsStatus;
                IDictionary<string, object> dic = new Dictionary<string, object>();
                dic["AcademicYearID"] = ObjTeacherOfSchool.AcademicYearID;
                dic["EmployeeType"] = SystemParamsInFile.EMPLOYEE_TYPE_SCHOOL_TEACHER;
                if (!isStatus.HasValue)
                {
                    dic["CurrentEmployeeStatus"] = SystemParamsInFile.EMPLOYMENT_STATUS_WORKING;
                }
                dic["isFromEduActive"] = isStatus;
                IQueryable<Employee> lstEmployee = EmployeeBusiness.SearchTeacher(ObjTeacherOfSchool.SchoolID, dic);
                //lay danh sach GVCN cua truong
                List<int> lstHeadTeacherId = ClassProfileBusiness.All
                                                .Where(o => o.AcademicYearID == ObjTeacherOfSchool.AcademicYearID
                                                    && o.HeadTeacherID.HasValue).Select(o => o.HeadTeacherID.Value).Distinct().ToList();

                //Lay danh sach GVBM cua truong
                int partition = UtilsBusiness.GetPartionId(ObjTeacherOfSchool.SchoolID);
                List<int> lstSubjectTeacherId = TeachingAssignmentBusiness.All
                    .Where(o => o.AcademicYearID == ObjTeacherOfSchool.AcademicYearID
                    && o.IsActive == true
                    && o.Last2digitNumberSchool == partition
                    ).Select(o => o.TeacherID).Distinct().ToList();

                List<EmployeeDefault> lstEmployeeDefault = (from e in lstEmployee
                                                            join u in UserAccountBusiness.All on e.EmployeeID equals u.EmployeeID into des
                                                            from x in des.DefaultIfEmpty()
                                                            join a in aspnet_UsersBusiness.All on x.GUID equals a.UserId into des2
                                                            from y in des2.DefaultIfEmpty()
                                                            select new EmployeeDefault
                                                            {
                                                                EmployeeID = e.EmployeeID,
                                                                EmployeeCode = e.EmployeeCode,
                                                                FullName = e.FullName,
                                                                EmployeeType = e.EmployeeType,
                                                                Genre = e.Genre,
                                                                Name = e.Name,
                                                                AccountName = y != null ? y.UserName : null,
                                                                IsActive = e.IsActive,
                                                                Telephone = e.Telephone,
                                                                Mobile = e.Mobile,
                                                                EmploymentStatus = e.EmploymentStatus.Value,
                                                                FacultyID = e.SchoolFacultyID.HasValue ? e.SchoolFacultyID.Value : 0,
                                                                FacultyName = e.SchoolFaculty != null ? e.SchoolFaculty.FacultyName : null,
                                                                WorkGroupTypeID = e.WorkGroupTypeID.HasValue ? e.WorkGroupTypeID.Value : 0,
                                                                WorkGroupTypeName = e.WorkGroupType != null ? e.WorkGroupType.Resolution : null,
                                                                WorkTypeID = e.WorkTypeID.HasValue ? e.WorkTypeID.Value : 0,
                                                                WorkTypeName = e.WorkType != null ? e.WorkType.Resolution : null,
                                                                IsCommunistPartyMember = e.IsCommunistPartyMember.HasValue && e.IsCommunistPartyMember.Value == true,
                                                                IsSyndicate = e.IsSyndicate.HasValue && e.IsSyndicate.Value == true,
                                                                IsYouthLeageMember = e.IsYouthLeageMember.HasValue && e.IsYouthLeageMember.Value == true,
                                                                IsHeadTeacher = lstHeadTeacherId.Contains(e.EmployeeID),
                                                                IsSubjectTeacher = lstSubjectTeacherId.Contains(e.EmployeeID)
                                                            }).ToList().OrderBy(p => p.Name).ToList();
                listEmployeeResponse.ValidationCode = WCFConstant.RESPONSE_SUCCESS;
                listEmployeeResponse.LstEmployee = lstEmployeeDefault;
            }
            catch (Exception)
            {
                //gan validationCode exception
                listEmployeeResponse.ValidationCode = WCFConstant.RESPONSE_EXCEPTION;
            }
            return listEmployeeResponse;
        }

        /// <summary>
        /// Lay danh sach giao vien cho chuc nang bao cao
        /// </summary>
        /// <param name="ObjTeacherOfSchool"></param>
        /// <returns></returns>
        public ListEmployeeResponse GetListTeacherOfSchoolForReport(TeacherOfSchoolRequest ObjTeacherOfSchool)
        {
            ListEmployeeResponse listEmployeeResponse = new ListEmployeeResponse();
            try
            {
                //Khoi tao Business
                DeclareBusiness();
                //Neu khong truyen nam hoc vao doi tuong tra ve new object 0 phan tu
                if (ObjTeacherOfSchool.AcademicYearID == 0)
                {
                    return listEmployeeResponse;
                }
                bool? isStatus = ObjTeacherOfSchool.IsStatus;
                IDictionary<string, object> dic = new Dictionary<string, object>();
                dic["AcademicYearID"] = ObjTeacherOfSchool.AcademicYearID;
                dic["EmployeeType"] = SystemParamsInFile.EMPLOYEE_TYPE_SCHOOL_TEACHER;

                IQueryable<Employee> lstEmployee = EmployeeBusiness.SearchTeacher(ObjTeacherOfSchool.SchoolID, dic);


                List<EmployeeDefault> lstEmployeeDefault = (from e in lstEmployee
                                                            join u in UserAccountBusiness.All on e.EmployeeID equals u.EmployeeID into des
                                                            from x in des.DefaultIfEmpty()
                                                            join a in aspnet_UsersBusiness.All on x.GUID equals a.UserId into des2
                                                            from y in des2.DefaultIfEmpty()
                                                            select new EmployeeDefault
                                                            {
                                                                EmployeeID = e.EmployeeID,
                                                                EmployeeCode = e.EmployeeCode,
                                                                FullName = e.FullName,
                                                                EmployeeType = e.EmployeeType,
                                                                Genre = e.Genre,
                                                                Name = e.Name,
                                                                AccountName = y != null ? y.UserName : null,
                                                                IsActive = e.IsActive,
                                                                Telephone = e.Telephone,
                                                                Mobile = e.Mobile,
                                                                EmploymentStatus = e.EmploymentStatus.Value,
                                                                FacultyID = e.SchoolFacultyID.HasValue ? e.SchoolFacultyID.Value : 0,
                                                                FacultyName = e.SchoolFaculty != null ? e.SchoolFaculty.FacultyName : null,
                                                                WorkGroupTypeID = e.WorkGroupTypeID.HasValue ? e.WorkGroupTypeID.Value : 0,
                                                                WorkGroupTypeName = e.WorkGroupType != null ? e.WorkGroupType.Resolution : null,
                                                                WorkTypeID = e.WorkTypeID.HasValue ? e.WorkTypeID.Value : 0,
                                                                WorkTypeName = e.WorkType != null ? e.WorkType.Resolution : null,
                                                                IsCommunistPartyMember = e.IsCommunistPartyMember.HasValue && e.IsCommunistPartyMember.Value == true,
                                                                IsSyndicate = e.IsSyndicate.HasValue && e.IsSyndicate.Value == true,
                                                                IsYouthLeageMember = e.IsYouthLeageMember.HasValue && e.IsYouthLeageMember.Value == true

                                                            }).ToList().OrderBy(p => p.Name).ToList();
                listEmployeeResponse.ValidationCode = WCFConstant.RESPONSE_SUCCESS;
                listEmployeeResponse.LstEmployee = lstEmployeeDefault;
            }
            catch (Exception)
            {
                //gan validationCode exception
                listEmployeeResponse.ValidationCode = WCFConstant.RESPONSE_EXCEPTION;
            }
            return listEmployeeResponse;
        }

        public ListEmployeeResponse GetListTeacherWithTeachingSubject(int SchoolID, int AcademicYearID, int AppliedLevel, int EducationLevel, int ClassID, int SubjectID, int Semester)
        {
            ListEmployeeResponse listEmployeeResponse = new ListEmployeeResponse();

            try
            {
                //Khoi tao Business
                DeclareBusiness();

                List<EmployeeDefault> lstEmployee = new List<EmployeeDefault>();

                List<int> lstEducationLevel = EducationLevelBusiness.GetByGrade(AppliedLevel).Select(o=>o.EducationLevelID).ToList();

                List<int> lstClassId = ClassProfileBusiness.All.Where(o=>o.AcademicYearID == AcademicYearID && o.IsActive.HasValue
                    && o.IsActive.Value && lstEducationLevel.Contains(o.EducationLevelID)).Select(o=>o.ClassProfileID).ToList();

                //Lay giao vien chu nhiem cua lop
                ClassProfile cp = ClassProfileBusiness.Find(ClassID);
                if (cp.Employee != null && cp.Employee.IsActive == true && (cp.Employee.EmploymentStatus == GlobalConstants.EMPLOYMENT_STATUS_WORKING || cp.Employee.EmploymentStatus == GlobalConstants.EMPLOYMENT_STATUS_BREATHER))
                {
                    lstEmployee.Add(new EmployeeDefault
                        {
                            EmployeeID = cp.Employee.EmployeeID,
                            FullName = cp.Employee.FullName,
                            FacultyName = cp.Employee.SchoolFaculty.FacultyName,
                            Mobile = cp.Employee.Mobile
                        });
                }

                //Lay giao vu
                List<ClassSupervisorAssignment> lsClassSupervisorAssignmentAll = ClassSupervisorAssignmentBusiness.All.Where(o => o.AcademicYearID == AcademicYearID && lstClassId.Contains(o.ClassID)
                    && (o.PermissionLevel == SystemParamsInFile.SUPERVISING_PERMISSION_HEAD_TEACHER || o.PermissionLevel == SystemParamsInFile.SUPERVISING_PERMISSION_SUBJECT_TEACHER)).ToList();
                List<ClassSupervisorAssignment> lsClassSupervisorAssignment = lsClassSupervisorAssignmentAll.Where(o => o.ClassID == ClassID && o.Employee.IsActive == true && (o.Employee.EmploymentStatus == GlobalConstants.EMPLOYMENT_STATUS_WORKING || o.Employee.EmploymentStatus == GlobalConstants.EMPLOYMENT_STATUS_BREATHER)).ToList();

                lstEmployee.AddRange(lsClassSupervisorAssignment.Select(o => new EmployeeDefault
                    {
                        EmployeeID = o.TeacherID,
                        FullName = o.Employee.FullName,
                        FacultyName = o.Employee.SchoolFaculty.FacultyName,
                        Mobile = o.Employee.Mobile
                    }));

                //Giao vien bo mon
                IDictionary<string, object> dic = new Dictionary<string, object>();
                dic["Semester"] = Semester;
                dic["EducationLevel"] = EducationLevel;
                dic["ClassID"] = ClassID;
                dic["AcademicYearID"] = AcademicYearID;
                dic["SubjectID"] = SubjectID;
                dic["RemoveVNEN"] = true;
                dic["IsActive"] = true;

                var lstGVBM = TeachingAssignmentBusiness.SearchBySchool(SchoolID, dic).Where(o => o.Employee.EmploymentStatus == GlobalConstants.EMPLOYMENT_STATUS_WORKING || o.Employee.EmploymentStatus == GlobalConstants.EMPLOYMENT_STATUS_BREATHER);
                lstEmployee.AddRange(lstGVBM.Select(o => new EmployeeDefault
                {
                    EmployeeID = o.TeacherID,
                    FullName = o.Employee.FullName,
                    FacultyName = o.Employee.SchoolFaculty.FacultyName,
                    Mobile = o.Employee.Mobile
                }));

                //Fill thong tin mon day
                //Lay danh sach cac lop chu nhiem
                List<ClassProfile> lstCp = ClassProfileBusiness.All.Where(o => o.AcademicYearID == AcademicYearID
                    && o.IsActive.HasValue && o.IsActive.Value
                    && o.HeadTeacherID.HasValue
                    && lstClassId.Contains(o.ClassProfileID)).ToList();

                //Danh sach phan cong giang day
                dic = new Dictionary<string, object>();
                dic["Semester"] = Semester;
                dic["AcademicYearID"] = AcademicYearID;
                dic["RemoveVNEN"] = true;

                var lstTeachingAssignmentAll = TeachingAssignmentBusiness.SearchBySchool(SchoolID, dic)
                    .Where(o => o.ClassID.HasValue && lstClassId.Contains(o.ClassID.Value)).ToList();

                lstEmployee = lstEmployee.Distinct().ToList();

                for (int i = 0; i < lstEmployee.Count; i++)
                {
                    EmployeeDefault emp = lstEmployee[i];
                    StringBuilder sb = new StringBuilder();

                    //Lay danh sach mon day
                    var lstTa = (from ta in lstTeachingAssignmentAll.Where(o => o.TeacherID == emp.EmployeeID).OrderBy(o=>o.ClassProfile.EducationLevelID)
                                     .ThenBy(o => o.ClassProfile.OrderNumber != null ? o.ClassProfile.OrderNumber : 0).ThenBy(o => o.ClassProfile.DisplayName)
                                 group ta by new { ta.SubjectID, ta.SubjectCat.SubjectName } into g
                                 select new
                                 {
                                     SubjectID = g.Key.SubjectID,
                                     SubjectName = g.Key.SubjectName,
                                     ClassesName = g.Select(o => o.ClassProfile.DisplayName)
                                 }).ToList();


                    for (int j = 0; j < lstTa.Count; j++)
                    {
                        var ta = lstTa[j];
                        sb.Append(string.Format("{0} ({1})", ta.SubjectName, string.Join(", ", ta.ClassesName)));
                        sb.Append(", ");
                    }

                    //Danh sach cac lop chu nhiem
                    var lstHeadClass = lstCp.Where(o => o.HeadTeacherID == emp.EmployeeID).OrderBy(o => o.EducationLevelID)
                                     .ThenBy(o => o.OrderNumber != null ? o.OrderNumber : 0).ThenBy(o => o.DisplayName).ToList();
                    if (lstHeadClass.Count > 0)
                    {
                        sb.Append(string.Format("Chủ nhiệm ({0})", string.Join(", ", lstHeadClass.Select(o => o.DisplayName))));
                        sb.Append(", ");
                    }

                    //Danh sach cac lop giao vu
                    var lsClassSupervisorAssignmentByTeacher = lsClassSupervisorAssignmentAll.Where(o => o.TeacherID == emp.EmployeeID)
                        .OrderBy(o => o.ClassProfile.EducationLevelID)
                                     .ThenBy(o => o.ClassProfile.OrderNumber != null ? o.ClassProfile.OrderNumber : 0).ThenBy(o => o.ClassProfile.DisplayName).ToList();

                    if (lsClassSupervisorAssignmentByTeacher.Count > 0)
                    {
                        sb.Append(string.Format("Giáo vụ ({0})", string.Join(", ", lsClassSupervisorAssignmentByTeacher.Select(o => o.ClassProfile.DisplayName).Distinct().ToList())));
                    }

                    emp.TeacherSubject = sb.ToString();
                    if (emp.TeacherSubject.EndsWith(", "))
                    {
                        emp.TeacherSubject = emp.TeacherSubject.Remove(emp.TeacherSubject.Count() - 2, 2);
                    }
                }

                listEmployeeResponse.ValidationCode = WCFConstant.RESPONSE_SUCCESS;
                listEmployeeResponse.LstEmployee = lstEmployee;
            }
            catch (Exception e)
            {
                listEmployeeResponse.ValidationCode = WCFConstant.RESPONSE_EXCEPTION;
                listEmployeeResponse.LstEmployee = new List<EmployeeDefault>();
            }

            
            return listEmployeeResponse;
        }
        #endregion
    }
}
