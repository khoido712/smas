﻿using log4net;
using SMAS.Business.Business;
using SMAS.Business.Common;
using SMAS.Business.IBusiness;
using SMAS.Models.Models;
using SMAS.VTUtils.Log;
using System;
using System.Collections.Generic;
using System.Linq;
using WCF.Composite;
using WCF.Composite.Migrate;
using WCF.Composite.Sync;

namespace WCF.Services
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the class name "MPupilProfileModule" in code, svc and config file together.
    // NOTE: In order to launch WCF Test Client for testing this service, please select MPupilProfileModule.svc or MPupilProfileModule.svc.cs at the Solution Explorer and start debugging.
    public class MPupilProfileModule : IMPupilProfileModule
    {

        #region Declare Variable    

        private IMarkRecordBusiness MarkRecordBusiness;
        private IClassSubjectBusiness ClassSubjectBusiness;
        private IJudgeRecordBusiness JudgeRecordBusiness;
        private IPupilProfileBusiness PupilProfileBusiness;
        private IClassProfileBusiness ClassProfileBusiness;
        private IEmployeeBusiness EmployeeBusiness;
        private IHeadTeacherSubstitutionBusiness HeadTeacherSubstitutionBusiness;
        private ISummedUpRecordBusiness SummedUpRecordBusiness;
        private ISummedUpRecordClassBusiness SummedUpRecordClassBusiness;
        private IPupilRankingBusiness PupilRankingBusiness;
        private IPupilEmulationBusiness PupilEmulationBusiness;
        private IPeriodDeclarationBusiness PeriodDeclarationBusiness;
        private IPupilAbsenceBusiness PupilAbsenceBusiness;
        private IPupilFaultBusiness PupilFaultBusiness;
        private IPupilPraiseBusiness PupilPraiseBusiness;
        private IPupilDisciplineBusiness PupilDisciplineBusiness;
        private IPupilOfClassBusiness PupilOfClassBusiness;
        private IEducationLevelBusiness EducationLevelBusiness;
        private ISchoolProfileBusiness SchoolProfileBusiness;
        private IAcademicYearBusiness AcademicYearBusiness;
        private ISubjectCatBusiness SubjectCatBusiness;
        private IMarkTypeBusiness MarkTypeBusiness;
        private IExemptedSubjectBusiness ExemptedSubjectBusiness;
        private IForeignLanguageGradeBusiness ForeignLanguageGradeBusiness;
        private IPupilOfSchoolBusiness PupilOfSchoolBusiness;
        private IFaultCriteriaBusiness FaultCriteriaBusiness;
        private IFaultGroupBusiness FaultGroupBusiness;
        private ITeachingAssignmentBusiness TeachingAssignmentBusiness;
        private IVMarkRecordBusiness VMarkRecordBusiness;
        private IVJudgeRecordBusiness VJudgeRecordBusiness;
        private IVSummedUpRecordBusiness VSummedUpRecordBusiness;
        private IVPupilRankingBusiness VPupilRankingBusiness;
        private ICalendarBusiness CalendarBusiness;
        private IConductLevelBusiness ConductLevelBusiness;
        private ICapacityLevelBusiness CapacityLevelBusiness;
        private IEvaluationCommentsBusiness EvaluationCommentsBusiness;
        private ISummedEvaluationBusiness SummedEvaluationBusiness;
        private ISummedEndingEvaluationBusiness SummedEndingEvaluationBusiness;
        private IRewardFinalBusiness RewardFinalBusiness;
        private IRewardCommentFinalBusiness RewardCommentFinalBusiness;
        private ISchoolCodeConfigBusiness SchoolCodeConfigBusiness;
        private IEvaluationCommentsHistoryBusiness EvaluationCommentsHistoryBusiness;
        private ISummedEvaluationHistoryBusiness SummedEvaluationHistoryBusiness;
        private ITeacherNoteBookMonthBusiness TeacherNoteBookMonthBusiness;
        private ITeacherNoteBookSemesterBusiness TeacherNoteBookSemesterBusiness;
        private IReviewBookPupilBusiness ReviewBookPupilBusiness;
        private IUpdateRewardBusiness UpdateRewardBusiness;

        private IExaminationsBusiness ExaminationsBusiness;
        private IExamGroupBusiness ExamGroupBusiness;
        private IExamSubjectBusiness ExamSubjectBusiness;
        private IExamPupilBusiness ExamPupilBusiness;
        private IExamInputMarkBusiness ExamInputMarkBusiness;
        private ICommentOfPupilBusiness CommentOfPupilBusiness;
        private IRatedCommentPupilBusiness RatedCommentPupilBusiness;
        private IRatedCommentPupilHistoryBusiness RatedCommentPupilHistoryBusiness;
        private IEvaluationCriteriaBusiness EvaluationCriteriaBusiness;
        private IEvaluationRewardBusiness EvaluationRewardBusiness;
        private static readonly ILog logger = LogManager.GetLogger(typeof(MPupilProfileModule));

        #endregion

        public MPupilProfileModule()
        {

        }
        /// <summary>
        /// Khoi tao business cung context
        /// </summary>
        private SMASEntities DeclareBusiness()
        {
            var context = new SMASEntities();
            context.Configuration.AutoDetectChangesEnabled = false;
            context.Configuration.ValidateOnSaveEnabled = false;
            this.MarkRecordBusiness = new MarkRecordBusiness(logger, context);
            this.JudgeRecordBusiness = new JudgeRecordBusiness(logger, context);
            this.ClassSubjectBusiness = new ClassSubjectBusiness(logger, context);
            this.PupilProfileBusiness = new PupilProfileBusiness(logger, context);
            this.ClassProfileBusiness = new ClassProfileBusiness(logger, context);
            this.EmployeeBusiness = new EmployeeBusiness(logger, context);
            this.HeadTeacherSubstitutionBusiness = new HeadTeacherSubstitutionBusiness(logger, context);
            this.SummedUpRecordBusiness = new SummedUpRecordBusiness(logger, context);
            this.SummedUpRecordClassBusiness = new SummedUpRecordClassBusiness(logger, context);
            this.PupilRankingBusiness = new PupilRankingBusiness(logger, context);
            this.PeriodDeclarationBusiness = new PeriodDeclarationBusiness(logger, context);
            this.PupilAbsenceBusiness = new PupilAbsenceBusiness(logger, context);
            this.PupilFaultBusiness = new PupilFaultBusiness(logger, context);
            this.PupilOfClassBusiness = new PupilOfClassBusiness(logger, context);
            this.EducationLevelBusiness = new EducationLevelBusiness(logger, context);
            this.SchoolProfileBusiness = new SchoolProfileBusiness(logger, context);
            this.PupilEmulationBusiness = new PupilEmulationBusiness(logger, context);
            this.AcademicYearBusiness = new AcademicYearBusiness(logger, context);
            this.SubjectCatBusiness = new SubjectCatBusiness(logger, context);
            this.MarkTypeBusiness = new MarkTypeBusiness(logger, context);
            this.PupilPraiseBusiness = new PupilPraiseBusiness(logger, context);
            this.PupilDisciplineBusiness = new PupilDisciplineBusiness(logger, context);
            this.ExemptedSubjectBusiness = new ExemptedSubjectBusiness(logger, context);
            this.ForeignLanguageGradeBusiness = new ForeignLanguageGradeBusiness(logger, context);
            this.PupilOfSchoolBusiness = new PupilOfSchoolBusiness(logger, context);
            this.FaultCriteriaBusiness = new FaultCriteriaBusiness(logger, context);
            this.FaultGroupBusiness = new FaultGroupBusiness(logger, context);
            this.TeachingAssignmentBusiness = new TeachingAssignmentBusiness(logger, context);
            this.CalendarBusiness = new CalendarBusiness(logger, context);
            this.VJudgeRecordBusiness = new VJudgeRecordBusiness(logger, context);
            this.VSummedUpRecordBusiness = new VSummedUpRecordBusiness(logger, context);
            this.VMarkRecordBusiness = new VMarkRecordBusiness(logger, context);
            this.VPupilRankingBusiness = new VPupilRankingBusiness(logger, context);
            this.ConductLevelBusiness = new ConductLevelBusiness(logger, context);
            this.CapacityLevelBusiness = new CapacityLevelBusiness(logger, context);
            this.EvaluationCommentsBusiness = new EvaluationCommentsBusiness(logger, context);
            this.SummedEvaluationBusiness = new SummedEvaluationBusiness(logger, context);
            this.SummedEndingEvaluationBusiness = new SummedEndingEvaluationBusiness(logger, context);
            this.RewardFinalBusiness = new RewardFinalBusiness(logger, context);
            this.RewardCommentFinalBusiness = new RewardCommentFinalBusiness(logger, context);
            this.SchoolCodeConfigBusiness = new SchoolCodeConfigBusiness(logger, context);
            this.EvaluationCommentsHistoryBusiness = new EvaluationCommentsHistoryBusiness(logger, context);
            this.SummedEvaluationHistoryBusiness = new SummedEvaluationHistoryBusiness(logger, context);
            this.TeacherNoteBookMonthBusiness = new TeacherNoteBookMonthBusiness(logger, context);
            this.TeacherNoteBookSemesterBusiness = new TeacherNoteBookSemesterBusiness(logger, context);
            this.ReviewBookPupilBusiness = new ReviewBookPupilBusiness(logger, context);
            this.UpdateRewardBusiness = new UpdateRewardBusiness(logger, context);

            this.ExaminationsBusiness = new ExaminationsBusiness(logger, context);
            this.ExamGroupBusiness = new ExamGroupBusiness(logger, context);
            this.ExamSubjectBusiness = new ExamSubjectBusiness(logger, context);
            this.ExamPupilBusiness = new ExamPupilBusiness(logger, context);
            this.ExamInputMarkBusiness = new ExamInputMarkBusiness(logger, context);
            this.CommentOfPupilBusiness = new CommentOfPupilBusiness(logger, context);
            this.RatedCommentPupilBusiness = new RatedCommentPupilBusiness(logger, context);
            this.RatedCommentPupilHistoryBusiness = new RatedCommentPupilHistoryBusiness(logger, context);
            this.EvaluationCriteriaBusiness = new EvaluationCriteriaBusiness(logger, context);
            this.EvaluationRewardBusiness = new EvaluationRewardBusiness(logger, context);

            return context;
        }

        public void SetAutoDetectChangesEnabled(SMASEntities context, bool isAutoDetectChangesEnabled)
        {
            context.Configuration.ValidateOnSaveEnabled = isAutoDetectChangesEnabled;
            context.Configuration.AutoDetectChangesEnabled = isAutoDetectChangesEnabled;

            /*if (context != null)
            {
                context.Dispose();
            }*/
        }

        #region Sync Hue


        #region Hoc sinh


        public MPupilProfile GetPupilProfile(int academicYearId, int SchoolId)
        {
            SMASEntities context = DeclareBusiness();
            MPupilProfile retVal;
            try
            {
                var iqPupilFile = from pp in context.PupilProfile.Where(s => s.IsActive == true)
                                  join pc in context.PupilOfClass.Where(p => p.AcademicYearID == academicYearId && p.SchoolID == SchoolId) on pp.PupilProfileID equals pc.PupilID
                                  select new MPupilProfileDefault
                                  {
                                      AcademicYearID = pc.AcademicYearID,
                                      SchoolID = pc.SchoolID,
                                      ProfileStatus = pc.Status,
                                      ClassID = pc.ClassID,
                                      M_OldID = pp.M_OldID,
                                      PupilCode = pp.PupilCode,
                                      PupilProfileID = pp.PupilProfileID,
                                      FullName = pp.FullName,
                                      SynchronizeID = pp.SynchronizeID
                                  };
                var lstPupil = iqPupilFile.ToList();
                return retVal = new MPupilProfile
                {
                    ListPupilFile = lstPupil,
                    Message = "Lay thong tin hoc sinh thanh cong",
                    Susscess = true
                };
            }
            catch (Exception ex)
            {
                string method = string.Format("{0}.{1}", System.Reflection.MethodBase.GetCurrentMethod().DeclaringType.FullName, System.Reflection.MethodBase.GetCurrentMethod().Name);
                string para = string.Format(" academicYearId={0},  SchoolId={1}", academicYearId, SchoolId);
                LogExtensions.ErrorExt(logger, DateTime.Now, method, para, ex.Message, "null");
                return retVal = new MPupilProfile
                {
                    ListPupilFile = new List<MPupilProfileDefault>(),
                    Message = "Loi trong qua trinh lay thong tin hoc sinh thanh cong. Chi tiet: " + ex.Message,
                    Susscess = false
                };
            }
            finally
            {
                SetAutoDetectChangesEnabled(context, true);
            }
        }

        /// <summary>
        /// listSourcePupilId la id hoc sinh cua doi tuong nguon
        /// </summary>
        /// <param name="academicYearId"></param>
        /// <param name="SchoolId"></param>
        /// <param name="lstSoucePupilId"></param>
        /// <returns></returns>
        public MPupilProfile GetPupilProfilebyId(int academicYearId, int SchoolId, List<int> lstSoucePupilId)
        {
            SMASEntities context = DeclareBusiness();
            MPupilProfile retVal;
            try
            {
                var iqPP = context.PupilProfile.Where(s => s.IsActive == true && s.CurrentSchoolID == SchoolId);
                if (lstSoucePupilId.Count > 0)
                {
                    iqPP = iqPP.Where(p => p.M_OldID.HasValue && lstSoucePupilId.Contains(p.M_OldID.Value));
                }
                var iqPupilFile = from pp in iqPP
                                  join pc in context.PupilOfClass.Where(p => p.AcademicYearID == academicYearId && p.SchoolID == SchoolId) on pp.PupilProfileID equals pc.PupilID
                                  select new MPupilProfileDefault
                                  {
                                      AcademicYearID = pc.AcademicYearID,
                                      SchoolID = pc.SchoolID,
                                      ProfileStatus = pc.Status,
                                      ClassID = pc.ClassID,
                                      M_OldID = pp.M_OldID,
                                      PupilCode = pp.PupilCode,
                                      PupilProfileID = pp.PupilProfileID,
                                      FullName = pp.FullName,
                                      SynchronizeID = pp.SynchronizeID
                                  };
                var lstPupil = iqPupilFile.ToList();
                return retVal = new MPupilProfile
                {
                    ListPupilFile = lstPupil,
                    Message = "Lay thong tin hoc sinh thanh cong",
                    Susscess = true
                };
            }
            catch (Exception ex)
            {
                string method = string.Format("{0}.{1}", System.Reflection.MethodBase.GetCurrentMethod().DeclaringType.FullName, System.Reflection.MethodBase.GetCurrentMethod().Name);
                string para = string.Format(" academicYearId={0},  SchoolId={1}", academicYearId, SchoolId);
                LogExtensions.ErrorExt(logger, DateTime.Now, method, para, ex.Message, "null");
                return retVal = new MPupilProfile
                {
                    ListPupilFile = new List<MPupilProfileDefault>(),
                    Message = "Loi trong qua trinh lay thong tin hoc sinh thanh cong. Chi tiet: " + ex.Message,
                    Susscess = false
                };
            }
            finally
            {
                SetAutoDetectChangesEnabled(context, true);
            }
        }

        public BoolResultResponse InsertPupilFile(List<MPupilProfileDefault> lstPupilFile, int academicYearId, int SchoolId, int year)
        {
            SMASEntities context = DeclareBusiness();
            try
            {
                int partitionId = UtilsBusiness.GetPartionId(SchoolId);
                List<PupilProfile> lstInsertPupilProfile = new List<PupilProfile>();
                MPupilProfileDefault objPupil;
                PupilProfile objInsertPupil;
                for (int i = 0; i < lstPupilFile.Count; i++)
                {
                    objPupil = lstPupilFile[i];

                    #region Map Object
                    objInsertPupil = new PupilProfile
                    {
                        AreaID = objPupil.AreaID,
                        BirthDate = objPupil.BirthDate,
                        BirthPlace = objPupil.BirthPlace,
                        BloodType = objPupil.BloodType,
                        ClassType = objPupil.ClassType,
                        CommuneID = objPupil.CommuneID,
                        CreatedDate = objPupil.CreatedDate,
                        CommunistPartyJoinedDate = objPupil.CommunistPartyJoinedDate,
                        CommunistPartyJoinedPlace = objPupil.CommunistPartyJoinedPlace,
                        CurrentAcademicYearID = objPupil.AcademicYearID,
                        CurrentClassID = objPupil.ClassID,
                        CurrentSchoolID = objPupil.SchoolID,
                        DisabledSeverity = objPupil.DisabledSeverity,
                        DisabledTypeID = objPupil.DisabledTypeID,
                        DistrictID = objPupil.DistrictID,
                        Email = objPupil.Email,
                        EatingHabit = objPupil.EatingHabit,
                        EnrolmentDate = objPupil.EnrolmentDate,
                        EthnicID = objPupil.DistrictID,
                        EnrolmentMark = objPupil.EnrolmentMark,
                        EvaluationConfigID = objPupil.EvaluationConfigID,
                        FamilyTypeID = objPupil.FamilyTypeID,
                        FatherBirthDate = objPupil.FatherBirthDate,
                        FatherEmail = objPupil.FatherEmail,
                        FatherFullName = objPupil.FatherFullName,
                        FatherJob = objPupil.FatherJob,
                        FatherMobile = objPupil.FatherMobile,
                        FavoriteToy = objPupil.FavoriteToy,
                        FavouriteTypeOfDish = objPupil.FavouriteTypeOfDish,
                        FullName = objPupil.FullName,
                        Genre = objPupil.Genre,
                        HateTypeOfDish = objPupil.HateTypeOfDish,
                        HeightAtBirth = objPupil.HeightAtBirth,
                        HomePlace = objPupil.HomePlace,
                        HomeTown = objPupil.HomeTown,
                        IdentifyNumber = objPupil.IdentifyNumber,
                        IsActive = objPupil.IsActive,
                        IsCommunistPartyMember = objPupil.IsCommunistPartyMember,
                        IsDisabled = objPupil.IsDisabled,
                        IsFatherSMS = objPupil.IsFatherSMS,
                        IsMotherSMS = objPupil.IsMotherSMS,
                        IsReceiveRiceSubsidy = objPupil.IsReceiveRiceSubsidy,
                        IsResettlementTarget = objPupil.IsResettlementTarget,
                        IsResidentIn = objPupil.IsResidentIn,
                        IsSponsorSMS = objPupil.IsSponsorSMS,
                        IsSupportForLearning = objPupil.IsSupportForLearning,
                        IsYoungPioneerMember = objPupil.IsYoungPioneerMember,
                        IsYouthLeageMember = objPupil.IsYouthLeageMember,
                        ItCertificate = objPupil.ItCertificate,
                        ItCertificateID = objPupil.ItCertificateID,
                        LanguageCertificate = objPupil.LanguageCertificate,
                        Mobile = objPupil.Mobile,
                        MotherEmail = objPupil.MotherEmail,
                        MotherBirthDate = objPupil.MotherBirthDate,
                        MotherJob = objPupil.MotherJob,
                        MotherFullName = objPupil.MotherFullName,
                        MotherMobile = objPupil.MotherMobile,
                        MSourcedb = objPupil.MSourcedb,
                        M_OldID = objPupil.M_OldID,
                        M_ProvinceID = objPupil.M_ProvinceID,
                        Name = objPupil.Name,
                        NumberOfChild = objPupil.NumberOfChild,
                        OtherHabit = objPupil.OtherHabit,
                        PolicyRegimeID = objPupil.PolicyRegimeID,
                        PolicyTargetID = objPupil.PolicyTargetID,
                        PreviousGraduationLevelID = objPupil.PreviousGraduationLevelID,
                        PriorityTypeID = objPupil.PriorityTypeID,
                        ProfileStatus = objPupil.ProfileStatus,
                        ProvinceID = objPupil.ProvinceID,
                        PupilCode = objPupil.PupilCode,
                        ReligionID = objPupil.ReligionID,
                        SponsorBirthDate = objPupil.SponsorBirthDate,
                        SponsorEmail = objPupil.SponsorEmail,
                        SponsorFullName = objPupil.SponsorFullName,
                        SponsorJob = objPupil.SponsorJob,
                        SynchronizeID = objPupil.SynchronizeID,
                        SponsorMobile = objPupil.SponsorMobile,
                        Telephone = objPupil.Telephone,
                        VillageID = objPupil.VillageID,
                        WeightAtBirth = objPupil.WeightAtBirth,
                        ForeignLanguageTraining = objPupil.ForeignLanguageTraining,
                        LanguageCertificateID = objPupil.LanguageCertificateID,
                        Last2digitNumberSchool = objPupil.Last2digitNumberSchool,
                        PupilLearningType = objPupil.PupilLearningType,
                        TempResidentalAddress = objPupil.TempResidentalAddress,
                        ReactionTrainingHabit = objPupil.ReactionTrainingHabit,
                        YoungPioneerJoinedDate = objPupil.YoungPioneerJoinedDate,
                        YoungPioneerJoinedPlace = objPupil.YoungPioneerJoinedPlace,
                        YouthLeagueJoinedDate = objPupil.YouthLeagueJoinedDate,
                        YouthLeagueJoinedPlace = objPupil.YouthLeagueJoinedPlace,
                        PermanentResidentalAddress = objPupil.PermanentResidentalAddress,

                    };

                    #endregion

                    lstInsertPupilProfile.Add(objInsertPupil);
                }
                int retValInsert = 0;
                if (lstInsertPupilProfile.Count > 0)
                {
                    retValInsert = PupilProfileBusiness.BulkInsert(WCFUtils.GetConnectionString, lstInsertPupilProfile, ColumnMapping.Instance.PupilProfile(), "PupilProfileID", "Image");
                }
                if (retValInsert <= 0)
                {
                    return new BoolResultResponse
                    {
                        Description = "Khong them moi duoc du lieu hoc sinh",
                        Result = false
                    };
                }
                List<int> lstOldId = lstPupilFile.Where(s => s.M_OldID.HasValue).Select(s => s.M_OldID ?? 0).ToList();
                List<PupilProfile> lstPupilDB = (from pf in context.PupilProfile
                                                 where pf.CurrentAcademicYearID == academicYearId
                                                 && pf.CurrentSchoolID == SchoolId
                                                 && (pf.M_OldID.HasValue && lstOldId.Contains(pf.M_OldID.Value))
                                                 select pf).ToList();
                PupilProfile objPFDB = null;

                //Tao doi tuong luu bang PupilOfSchool va PupilOfClass
                List<PupilOfSchool> lstPOS = new List<PupilOfSchool>();
                PupilOfSchool objPOS = null;
                List<PupilOfClass> lstPOC = new List<PupilOfClass>();
                PupilOfClass objPOC = null;
                string pupilCode = string.Empty;
                for (int i = 0; i < lstPupilDB.Count; i++)
                {

                    objPFDB = lstPupilDB[i];

                    objPOS = new PupilOfSchool();

                    objPOS.PupilID = objPFDB.PupilProfileID;
                    objPOS.SchoolID = objPFDB.CurrentSchoolID;
                    objPOS.EnrolmentDate = objPFDB.EnrolmentDate;
                    lstPOS.Add(objPOS);

                    objPOC = new PupilOfClass();
                    objPOC.PupilID = objPFDB.PupilProfileID;
                    objPOC.ClassID = objPFDB.CurrentClassID;
                    objPOC.SchoolID = objPFDB.CurrentSchoolID;
                    objPOC.AcademicYearID = objPFDB.CurrentAcademicYearID;
                    objPOC.Year = year;
                    objPOC.CreatedAcademicYear = year;
                    objPOC.Last2digitNumberSchool = partitionId;
                    objPOC.AssignedDate = objPFDB.EnrolmentDate;
                    objPOC.Status = objPFDB.ProfileStatus;
                    objPOC.OrderInClass = 0;
                    lstPOC.Add(objPOC);
                }

                retValInsert = PupilOfSchoolBusiness.BulkInsert(WCFUtils.GetConnectionString, lstPOS, ColumnMapping.Instance.PupilOfSchool(), "PupilOfSchoolID");
                retValInsert += PupilOfClassBusiness.BulkInsert(WCFUtils.GetConnectionString, lstPOC, ColumnMapping.Instance.PupilOfClass(), "PupilOfClassID");


                if (retValInsert > 0)
                {
                    return new BoolResultResponse
                    {
                        Description = "Them moi hoc sinh thanh cong",
                        Result = true
                    };
                }

                return new BoolResultResponse
                {
                    Description = "Them moi hoc sinh that bai",
                    Result = true
                };
            }
            catch (Exception ex)
            {
                string method = string.Format("{0}.{1}", System.Reflection.MethodBase.GetCurrentMethod().DeclaringType.FullName, System.Reflection.MethodBase.GetCurrentMethod().Name);
                string para = string.Format("academicYearId={0}, SchoolId={1}, year={2}", academicYearId, SchoolId, year);
                LogExtensions.ErrorExt(logger, DateTime.Now, method, para, ex.Message, "null");
                return new BoolResultResponse
                {
                    Result = false,
                    Description = ex.Message,
                };
            }
            finally
            {
                SetAutoDetectChangesEnabled(context, true);
            }
        }

        public BoolResultResponse UpdatePupilFile(List<MPupilProfileDefault> lstPupilFile, int academicYearId, int SchoolId, int year)
        {
            SMASEntities context = DeclareBusiness();
            try
            {
                int partitionId = UtilsBusiness.GetPartionId(SchoolId);
                List<PupilProfile> lstInsertPupilProfile = new List<PupilProfile>();
                MPupilProfileDefault objPupil;
                PupilProfile objUpdatePupil;
                for (int i = 0; i < lstPupilFile.Count; i++)
                {
                    objPupil = lstPupilFile[i];

                    #region Map Object
                    objUpdatePupil = new PupilProfile
                    {
                        AreaID = objPupil.AreaID,
                        BirthDate = objPupil.BirthDate,
                        BirthPlace = objPupil.BirthPlace,
                        BloodType = objPupil.BloodType,
                        ClassType = objPupil.ClassType,
                        CommuneID = objPupil.CommuneID,
                        CreatedDate = objPupil.CreatedDate,
                        CommunistPartyJoinedDate = objPupil.CommunistPartyJoinedDate,
                        CommunistPartyJoinedPlace = objPupil.CommunistPartyJoinedPlace,
                        CurrentAcademicYearID = objPupil.AcademicYearID,
                        CurrentClassID = objPupil.ClassID,
                        CurrentSchoolID = objPupil.SchoolID,
                        DisabledSeverity = objPupil.DisabledSeverity,
                        DisabledTypeID = objPupil.DisabledTypeID,
                        DistrictID = objPupil.DistrictID,
                        Email = objPupil.Email,
                        EatingHabit = objPupil.EatingHabit,
                        EnrolmentDate = objPupil.EnrolmentDate,
                        EthnicID = objPupil.DistrictID,
                        EnrolmentMark = objPupil.EnrolmentMark,
                        EvaluationConfigID = objPupil.EvaluationConfigID,
                        FamilyTypeID = objPupil.FamilyTypeID,
                        FatherBirthDate = objPupil.FatherBirthDate,
                        FatherEmail = objPupil.FatherEmail,
                        FatherFullName = objPupil.FatherFullName,
                        FatherJob = objPupil.FatherJob,
                        FatherMobile = objPupil.FatherMobile,
                        FavoriteToy = objPupil.FavoriteToy,
                        FavouriteTypeOfDish = objPupil.FavouriteTypeOfDish,
                        FullName = objPupil.FullName,
                        Genre = objPupil.Genre,
                        HateTypeOfDish = objPupil.HateTypeOfDish,
                        HeightAtBirth = objPupil.HeightAtBirth,
                        HomePlace = objPupil.HomePlace,
                        HomeTown = objPupil.HomeTown,
                        IdentifyNumber = objPupil.IdentifyNumber,
                        IsActive = objPupil.IsActive,
                        IsCommunistPartyMember = objPupil.IsCommunistPartyMember,
                        IsDisabled = objPupil.IsDisabled,
                        IsFatherSMS = objPupil.IsFatherSMS,
                        IsMotherSMS = objPupil.IsMotherSMS,
                        IsReceiveRiceSubsidy = objPupil.IsReceiveRiceSubsidy,
                        IsResettlementTarget = objPupil.IsResettlementTarget,
                        IsResidentIn = objPupil.IsResidentIn,
                        IsSponsorSMS = objPupil.IsSponsorSMS,
                        IsSupportForLearning = objPupil.IsSupportForLearning,
                        IsYoungPioneerMember = objPupil.IsYoungPioneerMember,
                        IsYouthLeageMember = objPupil.IsYouthLeageMember,
                        ItCertificate = objPupil.ItCertificate,
                        ItCertificateID = objPupil.ItCertificateID,
                        LanguageCertificate = objPupil.LanguageCertificate,
                        Mobile = objPupil.Mobile,
                        MotherEmail = objPupil.MotherEmail,
                        MotherBirthDate = objPupil.MotherBirthDate,
                        MotherJob = objPupil.MotherJob,
                        MotherFullName = objPupil.MotherFullName,
                        MotherMobile = objPupil.MotherMobile,
                        MSourcedb = objPupil.MSourcedb,
                        M_OldID = objPupil.M_OldID,
                        M_ProvinceID = objPupil.M_ProvinceID,
                        Name = objPupil.Name,
                        NumberOfChild = objPupil.NumberOfChild,
                        OtherHabit = objPupil.OtherHabit,
                        PolicyRegimeID = objPupil.PolicyRegimeID,
                        PolicyTargetID = objPupil.PolicyTargetID,
                        PreviousGraduationLevelID = objPupil.PreviousGraduationLevelID,
                        PriorityTypeID = objPupil.PriorityTypeID,
                        ProfileStatus = objPupil.ProfileStatus,
                        ProvinceID = objPupil.ProvinceID,
                        PupilCode = objPupil.PupilCode,
                        ReligionID = objPupil.ReligionID,
                        SponsorBirthDate = objPupil.SponsorBirthDate,
                        SponsorEmail = objPupil.SponsorEmail,
                        SponsorFullName = objPupil.SponsorFullName,
                        SponsorJob = objPupil.SponsorJob,
                        SynchronizeID = objPupil.SynchronizeID,
                        SponsorMobile = objPupil.SponsorMobile,
                        Telephone = objPupil.Telephone,
                        VillageID = objPupil.VillageID,
                        WeightAtBirth = objPupil.WeightAtBirth,
                        ForeignLanguageTraining = objPupil.ForeignLanguageTraining,
                        LanguageCertificateID = objPupil.LanguageCertificateID,
                        Last2digitNumberSchool = objPupil.Last2digitNumberSchool,
                        PupilLearningType = objPupil.PupilLearningType,
                        TempResidentalAddress = objPupil.TempResidentalAddress,
                        ReactionTrainingHabit = objPupil.ReactionTrainingHabit,
                        YoungPioneerJoinedDate = objPupil.YoungPioneerJoinedDate,
                        YoungPioneerJoinedPlace = objPupil.YoungPioneerJoinedPlace,
                        YouthLeagueJoinedDate = objPupil.YouthLeagueJoinedDate,
                        YouthLeagueJoinedPlace = objPupil.YouthLeagueJoinedPlace,
                        PermanentResidentalAddress = objPupil.PermanentResidentalAddress,
                        PupilProfileID = objPupil.PupilProfileID
                    };

                    #endregion

                    context.PupilProfile.Attach(objUpdatePupil);
                    context.Entry(objUpdatePupil).State = System.Data.EntityState.Modified;
                }

                var retVal = context.SaveChanges();

                return new BoolResultResponse
                {
                    Description = "Them moi hoc sinh that bai",
                    Result = true
                };
            }
            catch (Exception ex)
            {
                string method = string.Format("{0}.{1}", System.Reflection.MethodBase.GetCurrentMethod().DeclaringType.FullName, System.Reflection.MethodBase.GetCurrentMethod().Name);
                string para = string.Format("academicYearId={0}, SchoolId={1}, year={2}", academicYearId, SchoolId, year);
                LogExtensions.ErrorExt(logger, DateTime.Now, method, para, ex.Message, "null");
                return new BoolResultResponse
                {
                    Result = false,
                    Description = ex.Message,
                };
            }
            finally
            {
                SetAutoDetectChangesEnabled(context, true);
            }
        }


        #endregion

        #region Diem
        public MMarkRecord GetMarkRecord(List<int> lstPupilId, int academicYearId, int schoolId, List<int> lstSubjectId, List<int> lstSemesterId)
        {
            SMASEntities context = DeclareBusiness();
            try
            {
                int partitionId = UtilsBusiness.GetPartionId(schoolId);

                var iqMark = context.MarkRecord.Where(m => m.Last2digitNumberSchool == partitionId && m.SchoolID == schoolId && m.AcademicYearID == academicYearId);
                var iqJudgeRecord = context.JudgeRecord.Where(m => m.Last2digitNumberSchool == partitionId && m.SchoolID == schoolId && m.AcademicYearID == academicYearId);
                var iqSummedUpRecord = context.SummedUpRecord.Where(m => m.Last2digitNumberSchool == partitionId && m.SchoolID == schoolId && m.AcademicYearID == academicYearId);

                if (lstPupilId.Count > 0)
                {
                    iqMark = iqMark.Where(m => lstPupilId.Contains(m.PupilID));
                    iqJudgeRecord = iqJudgeRecord.Where(m => lstPupilId.Contains(m.PupilID));
                    iqSummedUpRecord = iqSummedUpRecord.Where(m => lstPupilId.Contains(m.PupilID));
                }
                if (lstSemesterId != null && lstSemesterId.Count > 0)
                {
                    iqMark = iqMark.Where(m => lstSemesterId.Contains(m.Semester.Value));
                    iqJudgeRecord = iqJudgeRecord.Where(m => lstSemesterId.Contains(m.Semester.Value));
                    iqSummedUpRecord = iqSummedUpRecord.Where(m => lstSemesterId.Contains(m.Semester.Value));
                }

                if (lstSubjectId != null && lstSubjectId.Count > 0)
                {
                    iqMark = iqMark.Where(m => lstSubjectId.Contains(m.SubjectID));
                    iqJudgeRecord = iqJudgeRecord.Where(m => lstSubjectId.Contains(m.SubjectID));
                    iqSummedUpRecord = iqSummedUpRecord.Where(m => lstSubjectId.Contains(m.SubjectID));
                }


                var listMark = (from m in iqMark
                                select new MMarkRecordDefault
                                {
                                    AcademicYearID = m.AcademicYearID,
                                    ClassID = m.ClassID,
                                    CreatedAcademicYear = m.CreatedAcademicYear,
                                    CreatedDate = m.CreatedDate,
                                    IsOldData = m.IsOldData,
                                    IsSMS = m.IsSMS,
                                    Last2digitNumberSchool = m.Last2digitNumberSchool,
                                    LogChange = m.LogChange,
                                    Mark = m.Mark,
                                    MarkedDate = m.MarkedDate,
                                    MarkRecordID = m.MarkRecordID,
                                    MarkTypeID = m.MarkTypeID,
                                    ModifiedDate = m.ModifiedDate,
                                    MSourcedb = m.MSourcedb,
                                    M_IsByC = m.M_IsByC,
                                    M_MarkRecord = m.M_MarkRecord,
                                    M_OldID = m.M_OldID,
                                    M_ProvinceID = m.M_ProvinceID,
                                    OldMark = m.OldMark,
                                    OrderNumber = m.OrderNumber,
                                    PeriodID = m.PeriodID,
                                    PupilID = m.PupilID,
                                    SchoolID = m.SchoolID,
                                    Semester = m.Semester,
                                    SubjectID = m.SubjectID,
                                    SynchronizeID = m.SynchronizeID,
                                    Title = m.Title,
                                    Year = m.Year
                                }).ToList();

                var listJudMark = (from m in iqJudgeRecord
                                   select new MJudgeRecordDefault
                                   {
                                       AcademicYearID = m.AcademicYearID,
                                       ClassID = m.ClassID,
                                       CreatedAcademicYear = m.CreatedAcademicYear,
                                       CreatedDate = m.CreatedDate,
                                       IsOldData = m.IsOldData,
                                       IsSMS = m.IsSMS,
                                       Last2digitNumberSchool = m.Last2digitNumberSchool,
                                       LogChange = m.LogChange,
                                       Judgement = m.Judgement,
                                       JudgeRecordID = m.JudgeRecordID,
                                       MarkedDate = m.MarkedDate,
                                       MJudgement = m.MJudgement,
                                       OldJudgement = m.OldJudgement,
                                       ReTestJudgement = m.ReTestJudgement,
                                       MarkTypeID = m.MarkTypeID,
                                       ModifiedDate = m.ModifiedDate,
                                       MSourcedb = m.MSourcedb,
                                       M_OldID = m.M_OldID,
                                       M_ProvinceID = m.M_ProvinceID,
                                       OrderNumber = m.OrderNumber,
                                       PeriodID = m.PeriodID,
                                       PupilID = m.PupilID,
                                       SchoolID = m.SchoolID,
                                       Semester = m.Semester,
                                       SubjectID = m.SubjectID,
                                       SynchronizeID = m.SynchronizeID,
                                       Title = m.Title,
                                       Year = m.Year
                                   }).ToList();

                var lstSummedRecord = (from m in iqSummedUpRecord
                                       select new MSummedRecordDefault
                                       {
                                           AcademicYearID = m.AcademicYearID,
                                           ClassID = m.ClassID,
                                           CreatedAcademicYear = m.CreatedAcademicYear,
                                           IsOldData = m.IsOldData,
                                           Last2digitNumberSchool = m.Last2digitNumberSchool,
                                           MSourcedb = m.MSourcedb,
                                           M_OldID = m.M_OldID,
                                           M_ProvinceID = m.M_ProvinceID,
                                           PeriodID = m.PeriodID,
                                           PupilID = m.PupilID,
                                           SchoolID = m.SchoolID,
                                           Semester = m.Semester,
                                           SubjectID = m.SubjectID,
                                           SynchronizeID = m.SynchronizeID,
                                           Year = m.Year,
                                           Comment = m.Comment,
                                           IsCommenting = m.IsCommenting,
                                           JudgementResult = m.JudgementResult,
                                           ReTestJudgement = m.ReTestJudgement,
                                           ReTestMark = m.ReTestMark,
                                           SummedUpMark = m.SummedUpMark,
                                           SummedUpDate = m.SummedUpDate,
                                           SummedUpRecordID = m.SummedUpRecordID
                                       }).ToList();

                return new MMarkRecord
                {
                    Message = "Lay du lieu diem  thanh cong",
                    Susscess = true,
                    ListMarkRecord = listMark,
                    ListJudgeMarkRecord = listJudMark,
                    ListSummedRecord = lstSummedRecord
                };
            }
            catch (Exception ex)
            {

                string method = string.Format("{0}.{1}", System.Reflection.MethodBase.GetCurrentMethod().DeclaringType.FullName, System.Reflection.MethodBase.GetCurrentMethod().Name);
                string para = string.Format("academicYearId={0}, schoolId={1}", academicYearId, schoolId);
                LogExtensions.ErrorExt(logger, DateTime.Now, method, para, ex.Message, "null");
                return new MMarkRecord
                {
                    Susscess = false,
                    Message = ex.Message,
                };
            }
            finally
            {
                SetAutoDetectChangesEnabled(context, true);
            }
        }

        public BoolResultResponse InsertMarkRecord(List<MMarkRecordDefault> lstMarkRecord, int academicYearId, int schoolId)
        {
            SMASEntities context = DeclareBusiness();
            try
            {
                var lstInsertMark = lstMarkRecord.Select(m => new MarkRecord
                {
                    AcademicYearID = m.AcademicYearID,
                    ClassID = m.ClassID,
                    CreatedAcademicYear = m.CreatedAcademicYear,
                    CreatedDate = m.CreatedDate,
                    IsOldData = m.IsOldData,
                    IsSMS = m.IsSMS,
                    Last2digitNumberSchool = m.Last2digitNumberSchool,
                    LogChange = m.LogChange,
                    Mark = m.Mark,
                    MarkedDate = m.MarkedDate,
                    MarkRecordID = m.MarkRecordID,
                    MarkTypeID = m.MarkTypeID,
                    ModifiedDate = m.ModifiedDate,
                    MSourcedb = m.MSourcedb,
                    M_IsByC = m.M_IsByC,
                    M_MarkRecord = m.M_MarkRecord,
                    M_OldID = m.M_OldID,
                    M_ProvinceID = m.M_ProvinceID,
                    OldMark = m.OldMark,
                    OrderNumber = m.OrderNumber,
                    PeriodID = m.PeriodID,
                    PupilID = m.PupilID,
                    SchoolID = m.SchoolID,
                    Semester = m.Semester,
                    SubjectID = m.SubjectID,
                    SynchronizeID = m.SynchronizeID,
                    Title = m.Title,
                    Year = m.Year
                }).ToList();

                var retVal = MarkRecordBusiness.BulkInsert(WCFUtils.GetConnectionString, lstInsertMark, ColumnMapping.Instance.MarkRecord(), "MarkRecordID");
                if (retVal > 0)
                {
                    return new BoolResultResponse
                    {
                        Description = "Them moi diem thanh cong",
                        Result = true
                    };
                }
                return new BoolResultResponse
                {
                    Result = false,
                    Description = "Khong insert duoc diem"
                };
            }
            catch (Exception ex)
            {
                string method = string.Format("{0}.{1}", System.Reflection.MethodBase.GetCurrentMethod().DeclaringType.FullName, System.Reflection.MethodBase.GetCurrentMethod().Name);
                string para = string.Format("academicYearId={0}, schoolId={1}", academicYearId, schoolId);
                LogExtensions.ErrorExt(logger, DateTime.Now, method, para, ex.Message, "null");
                return new BoolResultResponse
                {
                    Result = false,
                    Description = ex.Message,
                };
            }
            finally
            {
                SetAutoDetectChangesEnabled(context, true);
            }

        }

        public BoolResultResponse InsertJudgeRecord(List<MJudgeRecordDefault> lstJudgeRecord, int academicYearId, int schoolId)
        {
            SMASEntities context = DeclareBusiness();
            try
            {
                var lstInsertMark = lstJudgeRecord.Select(m => new JudgeRecord
                {
                    AcademicYearID = m.AcademicYearID,
                    ClassID = m.ClassID,
                    CreatedAcademicYear = m.CreatedAcademicYear,
                    CreatedDate = m.CreatedDate,
                    IsOldData = m.IsOldData,
                    IsSMS = m.IsSMS,
                    Last2digitNumberSchool = m.Last2digitNumberSchool,
                    LogChange = m.LogChange,
                    Judgement = m.Judgement,
                    JudgeRecordID = m.JudgeRecordID,
                    MarkedDate = m.MarkedDate,
                    MJudgement = m.MJudgement,
                    OldJudgement = m.OldJudgement,
                    ReTestJudgement = m.ReTestJudgement,
                    MarkTypeID = m.MarkTypeID,
                    ModifiedDate = m.ModifiedDate,
                    MSourcedb = m.MSourcedb,
                    M_OldID = m.M_OldID,
                    M_ProvinceID = m.M_ProvinceID,
                    OrderNumber = m.OrderNumber,
                    PeriodID = m.PeriodID,
                    PupilID = m.PupilID,
                    SchoolID = m.SchoolID,
                    Semester = m.Semester,
                    SubjectID = m.SubjectID,
                    SynchronizeID = m.SynchronizeID,
                    Title = m.Title,
                    Year = m.Year
                }).ToList();

                var retVal = JudgeRecordBusiness.BulkInsert(WCFUtils.GetConnectionString, lstInsertMark, ColumnMapping.Instance.JudgeRecord(), "JudgeRecordID");
                if (retVal > 0)
                {
                    return new BoolResultResponse
                    {
                        Description = "Them moi diem NX thanh cong",
                        Result = true
                    };
                }
                return new BoolResultResponse
                {
                    Result = false,
                    Description = "Khong insert duoc diem NX"
                };
            }
            catch (Exception ex)
            {
                string method = string.Format("{0}.{1}", System.Reflection.MethodBase.GetCurrentMethod().DeclaringType.FullName, System.Reflection.MethodBase.GetCurrentMethod().Name);
                string para = string.Format("academicYearId={0}, schoolId={1}", academicYearId, schoolId);
                LogExtensions.ErrorExt(logger, DateTime.Now, method, para, ex.Message, "null");
                return new BoolResultResponse
                {
                    Result = false,
                    Description = ex.Message,
                };
            }
            finally
            {
                SetAutoDetectChangesEnabled(context, true);
            }
        }

        public BoolResultResponse InsertSummedRecord(List<MSummedRecordDefault> lstSummedRecord, int academicYearId, int schoolId)
        {
            SMASEntities context = DeclareBusiness();
            try
            {
                var lstInsertMark = lstSummedRecord.Select(m => new SummedUpRecord
                {
                    AcademicYearID = m.AcademicYearID,
                    ClassID = m.ClassID,
                    CreatedAcademicYear = m.CreatedAcademicYear,
                    IsOldData = m.IsOldData,
                    Last2digitNumberSchool = m.Last2digitNumberSchool,
                    MSourcedb = m.MSourcedb,
                    M_OldID = m.M_OldID,
                    M_ProvinceID = m.M_ProvinceID,
                    PeriodID = m.PeriodID,
                    PupilID = m.PupilID,
                    SchoolID = m.SchoolID,
                    Semester = m.Semester,
                    SubjectID = m.SubjectID,
                    SynchronizeID = m.SynchronizeID,
                    Year = m.Year,
                    Comment = m.Comment,
                    IsCommenting = m.IsCommenting,
                    JudgementResult = m.JudgementResult,
                    ReTestJudgement = m.ReTestJudgement,
                    ReTestMark = m.ReTestMark,
                    SummedUpMark = m.SummedUpMark,
                    SummedUpDate = m.SummedUpDate,
                    SummedUpRecordID = m.SummedUpRecordID
                }).ToList();

                var retVal = SummedUpRecordBusiness.BulkInsert(WCFUtils.GetConnectionString, lstInsertMark, ColumnMapping.Instance.SummedUpRecord(), "SummedUpRecordID");
                if (retVal > 0)
                {
                    return new BoolResultResponse
                    {
                        Description = "Them moi diem TBM thanh cong",
                        Result = true
                    };
                }
                return new BoolResultResponse
                {
                    Result = false,
                    Description = "Khong insert duoc diem TBM"
                };
            }
            catch (Exception ex)
            {
                string method = string.Format("{0}.{1}", System.Reflection.MethodBase.GetCurrentMethod().DeclaringType.FullName, System.Reflection.MethodBase.GetCurrentMethod().Name);
                string para = string.Format("academicYearId={0}, schoolId={1}", academicYearId, schoolId);
                LogExtensions.ErrorExt(logger, DateTime.Now, method, para, ex.Message, "null");
                return new BoolResultResponse
                {
                    Result = false,
                    Description = ex.Message,
                };
            }
            finally
            {
                SetAutoDetectChangesEnabled(context, true);
            }
        }

        public MPupilRanking GetPupilRaning(List<int> lstPupilId, int academicYearId, int schoolId, List<int> lstSemesterId)
        {
            SMASEntities context = DeclareBusiness();
            try
            {
                int partitionId = UtilsBusiness.GetPartionId(schoolId);

                var iqMark = context.PupilRanking.Where(m => m.Last2digitNumberSchool == partitionId && m.SchoolID == schoolId && m.AcademicYearID == academicYearId);

                if (lstPupilId.Count > 0)
                {
                    iqMark = iqMark.Where(m => lstPupilId.Contains(m.PupilID));
                }
                if (lstSemesterId != null && lstSemesterId.Count > 0)
                {
                    iqMark = iqMark.Where(m => lstSemesterId.Contains(m.Semester.Value));
                }

                var listMark = (from m in iqMark
                                select new MPupilRankingDefault
                                {
                                    AcademicYearID = m.AcademicYearID,
                                    ClassID = m.ClassID,
                                    CreatedAcademicYear = m.CreatedAcademicYear,
                                    PeriodID = m.PeriodID,
                                    PupilID = m.PupilID,
                                    SchoolID = m.SchoolID,
                                    Semester = m.Semester,
                                    SynchronizeID = m.SynchronizeID,
                                    Year = m.Year,
                                    AverageMark = m.AverageMark,
                                    CapacityLevelID = m.CapacityLevelID,
                                    ConductLevelID = m.ConductLevelID,
                                    EducationLevelID = m.EducationLevelID,
                                    IsCategory = m.IsCategory,
                                    Last2digitNumberSchool = m.Last2digitNumberSchool,
                                    MSourcedb = m.MSourcedb,
                                    M_OldID = m.M_OldID,
                                    M_ProvinceID = m.M_ProvinceID,
                                    PupilRankingComment = m.PupilRankingComment,
                                    PupilRankingID = m.PupilRankingID,
                                    Rank = m.Rank,
                                    RankingDate = m.RankingDate,
                                    StudyingJudgementID = m.StudyingJudgementID,
                                    TotalAbsentDaysWithoutPermission = m.TotalAbsentDaysWithoutPermission,
                                    TotalAbsentDaysWithPermission = m.TotalAbsentDaysWithPermission
                                }).ToList();

                return new MPupilRanking
                {
                    Message = "Lay du lieu diem  thanh cong",
                    Susscess = true,
                    ListPupilRanking = listMark,
                };
            }
            catch (Exception ex)
            {

                string method = string.Format("{0}.{1}", System.Reflection.MethodBase.GetCurrentMethod().DeclaringType.FullName, System.Reflection.MethodBase.GetCurrentMethod().Name);
                string para = string.Format("academicYearId={0}, schoolId={1}", academicYearId, schoolId);
                LogExtensions.ErrorExt(logger, DateTime.Now, method, para, ex.Message, "null");
                return new MPupilRanking
                {
                    Susscess = false,
                    Message = ex.Message,
                };
            }
            finally
            {
                SetAutoDetectChangesEnabled(context, true);
            }
        }

        public BoolResultResponse InsertPupilRanking(List<MPupilRankingDefault> lstPupilRanking, int academicYearId, int schoolId)
        {
            SMASEntities context = DeclareBusiness();
            LogExtensions.GetCurrentClass = System.Reflection.MethodBase.GetCurrentMethod().DeclaringType;
            try
            {
                var lstInsertMark = lstPupilRanking.Select(m => new PupilRanking
                {
                    AcademicYearID = m.AcademicYearID,
                    ClassID = m.ClassID,
                    CreatedAcademicYear = m.CreatedAcademicYear,
                    AverageMark = m.AverageMark,
                    CapacityLevelID = m.CapacityLevelID,
                    ConductLevelID = m.ConductLevelID,
                    EducationLevelID = m.EducationLevelID,
                    IsCategory = m.IsCategory,
                    Last2digitNumberSchool = m.Last2digitNumberSchool,
                    MSourcedb = m.MSourcedb,
                    M_OldID = m.M_OldID,
                    M_ProvinceID = m.M_ProvinceID,
                    PeriodID = m.PeriodID,
                    PupilID = m.PupilID,
                    PupilRankingComment = m.PupilRankingComment,
                    Rank = m.Rank,
                    SchoolID = m.SchoolID,
                    RankingDate = m.RankingDate,
                    Semester = m.Semester,
                    StudyingJudgementID = m.StudyingJudgementID,
                    SynchronizeID = m.SynchronizeID,
                    TotalAbsentDaysWithoutPermission = m.TotalAbsentDaysWithoutPermission,
                    TotalAbsentDaysWithPermission = m.TotalAbsentDaysWithPermission,
                    Year = m.Year
                }).ToList();

                var retVal = PupilRankingBusiness.BulkInsert(WCFUtils.GetConnectionString, lstInsertMark, ColumnMapping.Instance.PupilRanking(), "PupilRankingID");
                if (retVal > 0)
                {
                    return new BoolResultResponse
                    {
                        Description = "Them moi diem thanh cong",
                        Result = true
                    };
                }
                return new BoolResultResponse
                {
                    Result = false,
                    Description = "Khong insert duoc diem"
                };
            }
            catch (Exception ex)
            {
                string para = string.Format("academicYearId={0}, schoolId={1}", academicYearId, schoolId);
                LogExtensions.ErrorExt(logger, DateTime.Now, para, ex.Message, "null");
                return new BoolResultResponse
                {
                    Result = false,
                    Description = ex.Message,
                };
            }
            finally
            {
                SetAutoDetectChangesEnabled(context, true);
            }
        }

        public BoolResultResponse UpdatePupilRanking(List<MPupilRankingDefault> lstPupilRanking, int academicYearId, int schoolId)
        {
            SMASEntities context = DeclareBusiness();
            try
            {
                PupilRanking objPupilRanking;
                foreach (var m in lstPupilRanking)
                {
                    objPupilRanking = new PupilRanking
                    {
                        AcademicYearID = m.AcademicYearID,
                        ClassID = m.ClassID,
                        CreatedAcademicYear = m.CreatedAcademicYear,
                        AverageMark = m.AverageMark,
                        CapacityLevelID = m.CapacityLevelID,
                        ConductLevelID = m.ConductLevelID,
                        EducationLevelID = m.EducationLevelID,
                        IsCategory = m.IsCategory,
                        Last2digitNumberSchool = m.Last2digitNumberSchool,
                        MSourcedb = m.MSourcedb,
                        M_OldID = m.M_OldID,
                        M_ProvinceID = m.M_ProvinceID,
                        PeriodID = m.PeriodID,
                        PupilID = m.PupilID,
                        PupilRankingComment = m.PupilRankingComment,
                        Rank = m.Rank,
                        SchoolID = m.SchoolID,
                        RankingDate = m.RankingDate,
                        Semester = m.Semester,
                        StudyingJudgementID = m.StudyingJudgementID,
                        SynchronizeID = m.SynchronizeID,
                        TotalAbsentDaysWithoutPermission = m.TotalAbsentDaysWithoutPermission,
                        TotalAbsentDaysWithPermission = m.TotalAbsentDaysWithPermission,
                        Year = m.Year
                    };

                    context.PupilRanking.Attach(objPupilRanking);
                    context.Entry(objPupilRanking).State = System.Data.EntityState.Modified;

                }


                var retVal = context.SaveChanges();
                if (retVal > 0)
                {
                    return new BoolResultResponse
                    {
                        Description = "Cap nhat diem tong ket thanh cong",
                        Result = true
                    };
                }
                return new BoolResultResponse
                {
                    Result = false,
                    Description = "Khong cap nhat duoc diem tong ket"
                };
            }
            catch (Exception ex)
            {
                LogExtensions.GetCurrentClass = System.Reflection.MethodBase.GetCurrentMethod().DeclaringType;
                string para = string.Format("academicYearId={0}, schoolId={1}", academicYearId, schoolId);
                LogExtensions.ErrorExt(logger, DateTime.Now, para, ex.Message, "null");
                return new BoolResultResponse
                {
                    Result = false,
                    Description = ex.Message,
                };
            }
            finally
            {
                SetAutoDetectChangesEnabled(context, true);
            }
        }


        #endregion

        #endregion

        #region SyncHCM

        public TransPupilProfileResponse GetSyncPupilFile(SyncAcademicYear form)
        {
            SMASEntities context = DeclareBusiness();
            TransPupilProfileResponse retResult = new TransPupilProfileResponse();
            retResult.Susscess = false;
            retResult.ListPupilfile = new List<TransPupilProfileDefault>();
            retResult.ListPupilOfClass = new List<TransPupilOfClassDefault>();
            retResult.ValidationCode = WCFConstant.RESPONSE_PUPIL_PROFILEID_NOT_FOUND;
            try
            {
                SetAutoDetectChangesEnabled(context, false);

                IQueryable<PupilProfile> iqPupil = context.PupilProfile.AsNoTracking();
                /*iqPupil = iqPupil.Where(p => ((!p.SynchronizeID.HasValue || p.SynchronizeID == 0)
                                              || (p.SynchronizeID > 0 && p.ModifiedDate >= form.ModifyDate && !form.ListModifyDate.Contains(p.MSourcedb)))
                                         );*/

                iqPupil = iqPupil.Where(p => (string.IsNullOrEmpty(p.MSourcedb) || !p.MSourcedb.Contains(form.SourceDB)) ||
                                           (p.MSourcedb.Contains(form.SourceDB) && p.ModifiedDate >= form.ModifyDate)
                                      );
                iqPupil = iqPupil.Where(p => p.CurrentSchoolID == form.SchoolId && p.CurrentAcademicYearID == form.AcademicYearId);
                var lstPupil = iqPupil.Take(form.RecordNumber).ToList();
                if (!lstPupil.Any())
                {
                    retResult.Message = "Khong co du lieu hoc sinh nam AcademicYearId=" + form.AcademicYearId;
                    return retResult;
                }
                var lstTransPupil = (from pp in lstPupil
                                     select new TransPupilProfileDefault
                                     {
                                         Area = (pp.Area == null) ? "" : pp.Area.AreaName,
                                         BirthDate = pp.BirthDate,
                                         BirthPlace = pp.BirthPlace,
                                         BloodType = pp.BloodType ?? 0,
                                         Commune = pp.Commune == null ? "" : pp.Commune.CommuneName,
                                         CommunistPartyJoinedDate = pp.CommunistPartyJoinedDate,
                                         CommunistPartyJoinedPlace = pp.CommunistPartyJoinedPlace,
                                         DisabledSeverity = pp.DisabledSeverity,
                                         DisabledTypeId = pp.DisabledTypeID ?? 0,
                                         District = (pp.District == null) ? "" : pp.District.DistrictName,
                                         Email = pp.Email,
                                         EnrolmentDate = pp.EnrolmentDate,
                                         Ethnic = (pp.Ethnic == null) ? "" : pp.Ethnic.EthnicName,
                                         FamilyTypeId = pp.FamilyTypeID ?? 0,
                                         FatherBirthDate = pp.FatherBirthDate,
                                         FatherFullName = pp.FatherFullName,
                                         FatherJob = pp.FatherJob,
                                         FatherMobile = pp.FatherMobile,
                                         ForeignLanguageTraining = pp.ForeignLanguageTraining,
                                         FullName = pp.FullName,
                                         Genre = pp.Genre,
                                         HomeTown = pp.HomeTown,
                                         IsCommunistPartyMember = pp.IsCommunistPartyMember ?? false,
                                         IsDisabled = pp.IsDisabled ?? false,
                                         IsResidentIn = pp.IsResidentIn ?? false,
                                         IsSupportForLearning = pp.IsSupportForLearning ?? false,
                                         IsYoungPioneerMember = pp.IsYoungPioneerMember ?? false,
                                         IsYouthLeageMember = pp.IsYouthLeageMember ?? false,
                                         Mobile = pp.Mobile,
                                         MotherBirthDate = pp.MotherBirthDate,
                                         MotherFullName = pp.MotherFullName,
                                         MotherJob = pp.MotherJob,
                                         MotherMobile = pp.MotherMobile,
                                         Name = pp.Name,
                                         PermanentResidentalAddress = pp.PermanentResidentalAddress,
                                         PolicyRegimeId = pp.PolicyRegimeID ?? 0,
                                         PolicyTargetId = (pp.PolicyTarget != null) ? pp.PolicyTarget.Resolution : "",
                                         PreviousGraduationLevel = pp.PreviousGraduationLevelID ?? 0,
                                         PriorityTypeId = pp.PriorityTypeID ?? 0,
                                         ProfileStatus = pp.ProfileStatus,
                                         Province = (pp.Province == null) ? "" : pp.Province.ProvinceName,
                                         PupilCode = pp.PupilCode,
                                         PupilProfileId = pp.PupilProfileID,
                                         Religion = (pp.Religion != null) ? pp.Religion.Resolution : "",
                                         SponsorBirthDate = pp.SponsorBirthDate,
                                         SponsorFullName = pp.SponsorFullName,
                                         SponsorJob = pp.SponsorJob,
                                         SponsorMobile = pp.SponsorMobile,
                                         Telephone = pp.Telephone,
                                         TempResidentalAddress = pp.TempResidentalAddress,
                                         YoungPioneerJoinedDate = pp.YoungPioneerJoinedDate,
                                         YoungPioneerJoinedPlace = pp.YoungPioneerJoinedPlace,
                                         YouthLeagueJoinedDate = pp.YouthLeagueJoinedDate,
                                         YouthLeagueJoinedPlace = pp.YouthLeagueJoinedPlace,
                                         SynchronizeID = pp.SynchronizeID ?? 0,
                                         IsActive = pp.IsActive,
                                         ModifiedDate = pp.ModifiedDate,
                                         Image = pp.Image,
                                         SourceDB = pp.MSourcedb
                                     }).ToList();

                List<int> lstPupilId = lstPupil.Select(p => p.PupilProfileID).ToList();


                var lstPupilOfClass = (from poc in context.PupilOfClass.AsNoTracking()
                                       where poc.SchoolID == form.SchoolId
                                             && poc.AcademicYearID == form.AcademicYearId
                                             && lstPupilId.Contains(poc.PupilID)
                                       select new TransPupilOfClassDefault
                                       {
                                           ClassId = poc.ClassID,
                                           Description = poc.Description,
                                           NoConductEstimation = poc.NoConductEstimation ?? false,
                                           OrderInClass = poc.OrderInClass ?? 0,
                                           PupilId = poc.PupilID,
                                           PupilOfClassId = poc.PupilOfClassID,
                                           Status = poc.Status,
                                           Year = poc.Year ?? form.Year,
                                           SourceDB = poc.MSourcedb
                                       }).ToList();

                retResult.ListPupilfile = lstTransPupil;
                retResult.ListPupilOfClass = lstPupilOfClass;
                retResult.ValidationCode = WCFConstant.RESPONSE_SUCCESS;
                retResult.Susscess = true;
                return retResult;
            }
            catch (Exception ex)
            {
                LogExtensions.GetCurrentClass = System.Reflection.MethodBase.GetCurrentMethod().DeclaringType;
                string para = string.Format("academicYearId={0}, schoolId={1}", form.AcademicYearId, form.SchoolId);
                LogExtensions.ErrorExt(logger, DateTime.Now, para, ex.Message, "null");
                retResult.Susscess = false;
                retResult.ValidationCode = WCFConstant.RESPONSE_EXCEPTION;
                retResult.Message = ex.Message;
                return retResult;
            }
            finally
            {
                SetAutoDetectChangesEnabled(context, true);
            }
        }

        public BoolResultResponse UpdateSyncPupilFile(List<int> lstPupilFileId, SyncAcademicYear form)
        {
            SMASEntities context = DeclareBusiness();
            BoolResultResponse retResulst = new BoolResultResponse();
            retResulst.Result = true;
            retResulst.ValidationCode = WCFConstant.RESPONSE_SUCCESS;
            try
            {
                SetAutoDetectChangesEnabled(context, false);

                if (lstPupilFileId == null || lstPupilFileId.Count == 0)
                {
                    retResulst.Result = false;
                    return retResulst;
                }

                List<PupilProfile> lstPupilProfile = context.PupilProfile.Where(s => s.CurrentSchoolID == form.SchoolId
                                                                                && s.CurrentAcademicYearID == form.AcademicYearId
                                                                                && lstPupilFileId.Contains(s.PupilProfileID))
                                                                                .ToList();
                if (lstPupilProfile == null || lstPupilProfile.Count == 0)
                {
                    return retResulst;
                }
                for (int i = 0; i < lstPupilProfile.Count; i++)
                {
                    PupilProfile pupilUpdate = lstPupilProfile[i]; //PupilProfileBusiness.Find(lstPupilFileId[i]);
                    string sourcedb = UtilsBusiness.CreateSourceDB(pupilUpdate.MSourcedb, form.SourceDB);
                    pupilUpdate.SynchronizeID = 1;
                    pupilUpdate.MSourcedb = sourcedb;
                    context.PupilProfile.Attach(pupilUpdate);
                    context.Entry(pupilUpdate).State = System.Data.EntityState.Modified;
                }
                context.SaveChanges();
                return retResulst;

            }
            catch (Exception ex)
            {
                LogExtensions.GetCurrentClass = System.Reflection.MethodBase.GetCurrentMethod().DeclaringType;
                string para = string.Format("academicYearId={0}, schoolId={1}", form.AcademicYearId, form.SchoolId);
                LogExtensions.ErrorExt(logger, DateTime.Now, para, ex.Message, "null");
                retResulst.Result = false;
                retResulst.ValidationCode = WCFConstant.RESPONSE_EXCEPTION;
                return retResulst;
            }
            finally
            {
                SetAutoDetectChangesEnabled(context, true);
            }
        }

        public TransMarkRecordResponse GetSyncMarkRecord(SyncAcademicYear form)
        {
            SMASEntities context = DeclareBusiness();
            TransMarkRecordResponse retResult = new TransMarkRecordResponse();
            retResult.Susscess = false;
            retResult.ListMarkRecord = new List<TransMarkRecordDefault>();
            retResult.ValidationCode = WCFConstant.RESPONSE_PUPIL_PROFILEID_NOT_FOUND;
            try
            {
                SetAutoDetectChangesEnabled(context, false);
                int partitionId = UtilsBusiness.GetPartionId(form.SchoolId);
                var iqMark = context.MarkRecord.AsNoTracking().Where(m => m.Last2digitNumberSchool == partitionId && m.AcademicYearID == form.AcademicYearId);
                iqMark = iqMark.Where(m => !m.SynchronizeID.HasValue || (m.SynchronizeID > 0 && m.ModifiedDate >= form.ModifyDate));
                /*iqMark = iqMark.Where(p => (string.IsNullOrEmpty(p.MSourcedb) || !p.MSourcedb.Contains(form.SourceDB)) ||
                                           (p.MSourcedb.Contains(form.SourceDB) && p.ModifiedDate >= form.ModifyDate)*/
                                      //);
                iqMark = iqMark.Take(form.RecordNumber);
                var lstMark = iqMark.ToList();
                if (!lstMark.Any())
                {
                    retResult.Message = "Khong co du lieu diem nam hoc AcademicYearId=" + form.AcademicYearId;
                    return retResult;
                }

                var lstMarkTransfer = lstMark.Select(m => new TransMarkRecordDefault
                {
                    Mark = m.Mark,
                    MarkRecordId = m.MarkRecordID,
                    MarkTypeId = m.MarkTypeID,
                    OrderNumber = (byte)m.OrderNumber,
                    PupilId = m.PupilID,
                    Semester = (byte)(m.Semester ?? 0),
                    SubjectId = m.SubjectID,
                    Title = m.Title,
                    ModifiedDate = m.ModifiedDate,
                    SynchronizeID = m.SynchronizeID ?? 0,
                    Year = m.Year ?? form.Year,
                    SourceDB = m.MSourcedb

                }).ToList();

                List<int> lstPupilId = lstMark.Select(m => m.PupilID).Distinct().ToList();
                List<int> lstSubjectId = lstMark.Select(m => m.SubjectID).Distinct().ToList();
                List<int> lstSemesterId = lstMark.Select(m => m.Semester.Value).Distinct().ToList();


                var iqSummed = context.SummedUpRecord.Where(s => s.Last2digitNumberSchool == partitionId && s.AcademicYearID == form.AcademicYearId);
                iqSummed = iqSummed.Where(s => (!s.PeriodID.HasValue || s.PeriodID == 0));
                iqSummed = iqSummed.Where(s => lstPupilId.Contains(s.PupilID));
                iqSummed = iqSummed.Where(s => lstSubjectId.Contains(s.SubjectID));
                iqSummed = iqSummed.Where(s => lstSemesterId.Contains(s.Semester.Value));


                var lstSummed = iqSummed.ToList();
                List<TransSummedUpRecordDefault> lstTransSummed = new List<TransSummedUpRecordDefault>();
                if (lstSummed.Any())
                {
                    lstTransSummed = lstSummed.Select(s => new TransSummedUpRecordDefault
                    {
                        Comment = s.Comment,
                        IsCommenting = s.IsCommenting,
                        JudgementResult = s.JudgementResult,
                        PupilId = s.PupilID,
                        ReTestJudgement = s.ReTestJudgement,
                        ReTestMark = s.ReTestMark,
                        Semester = s.Semester ?? 0,
                        SubjectId = s.SubjectID,
                        SummedUpMark = s.SummedUpMark,
                        SummedUpRecordId = s.SummedUpRecordID,
                        SynchronizeID = s.SynchronizeID,
                        UpdateTime = s.SummedUpDate,
                        Year = s.CreatedAcademicYear
                    }).ToList();
                }

                retResult.ListMarkRecord = lstMarkTransfer;
                retResult.ListSummed = lstTransSummed;
                retResult.ValidationCode = WCFConstant.RESPONSE_SUCCESS;
                retResult.Susscess = true;
                return retResult;
            }
            catch (Exception ex)
            {
                LogExtensions.GetCurrentClass = System.Reflection.MethodBase.GetCurrentMethod().DeclaringType;
                string para = string.Format("academicYearId={0}, schoolId={1}", form.AcademicYearId, form.SchoolId);
                LogExtensions.ErrorExt(logger, DateTime.Now, para, ex.Message, "null");
                retResult.Susscess = false;
                retResult.ValidationCode = WCFConstant.RESPONSE_EXCEPTION;
                retResult.Message = ex.StackTrace;
                return retResult;
            }
            finally
            {
                SetAutoDetectChangesEnabled(context, true);
            }
        }

        public BoolResultResponse UpdateSyncMarkRecord(List<long> lstMark, SyncAcademicYear form)
        {
            SMASEntities context = DeclareBusiness();
            BoolResultResponse retResulst = new BoolResultResponse();
            retResulst.Result = true;
            retResulst.ValidationCode = WCFConstant.RESPONSE_SUCCESS;
            try
            {
                SetAutoDetectChangesEnabled(context, false);

                if (lstMark == null || lstMark.Count == 0)
                {
                    retResulst.Result = false;
                    return retResulst;
                }

                int partitionId = UtilsBusiness.GetPartionId(form.SchoolId);
                var iqMarkUpdate = context.MarkRecord.Where(m => m.Last2digitNumberSchool == partitionId && m.AcademicYearID == form.AcademicYearId);
                iqMarkUpdate = iqMarkUpdate.Where(m => lstMark.Contains(m.MarkRecordID));
                var lstMarkUpdate = iqMarkUpdate.ToList();
                for (int i = 0; i < lstMark.Count; i++)
                {
                    MarkRecord markUpdate = lstMarkUpdate[i];
                    string sourcedb = UtilsBusiness.CreateSourceDB(markUpdate.MSourcedb, form.SourceDB);
                    markUpdate.SynchronizeID = 1;
                    markUpdate.M_ProvinceID = form.ProvinceId;
                    markUpdate.MSourcedb = sourcedb;
                    context.MarkRecord.Attach(markUpdate);
                    context.Entry(markUpdate).State = System.Data.EntityState.Modified;
                }
                context.SaveChanges();
                return retResulst;

            }
            catch (Exception ex)
            {
                LogExtensions.GetCurrentClass = System.Reflection.MethodBase.GetCurrentMethod().DeclaringType;
                string para = string.Format("academicYearId={0}, schoolId={1}", form.AcademicYearId, form.SchoolId);
                LogExtensions.ErrorExt(logger, DateTime.Now, para, ex.Message, "null");
                retResulst.Result = false;
                retResulst.ValidationCode = WCFConstant.RESPONSE_EXCEPTION;
                retResulst.Description = ex.Message;
                return retResulst;
            }
            finally
            {
                SetAutoDetectChangesEnabled(context, true);
            }
        }

        public TransJudgeRecordResponse GetSyncJudgekRecord(SyncAcademicYear form)
        {
            SMASEntities context = DeclareBusiness();
            TransJudgeRecordResponse retResult = new TransJudgeRecordResponse();
            retResult.Susscess = false;
            retResult.ListJudgeRecord = new List<TransJudgeRecordDefault>();
            retResult.ValidationCode = WCFConstant.RESPONSE_PUPIL_PROFILEID_NOT_FOUND;
            try
            {
                SetAutoDetectChangesEnabled(context, false);
                int partitionId = UtilsBusiness.GetPartionId(form.SchoolId);
                var iqMark = context.JudgeRecord.Where(m => m.Last2digitNumberSchool == partitionId && m.AcademicYearID == form.AcademicYearId);
                //iqMark = iqMark.Where(m => !m.SynchronizeID.HasValue || (m.SynchronizeID > 0 && m.ModifiedDate >= form.ModifyDate));
                iqMark = iqMark.Where(p => (string.IsNullOrEmpty(p.MSourcedb) || !p.MSourcedb.Contains(form.SourceDB)) ||
                                           (p.MSourcedb.Contains(form.SourceDB) && p.ModifiedDate >= form.ModifyDate)
                                      );

                iqMark = iqMark.Take(form.RecordNumber);
                var lstMark = iqMark.ToList();
                if (!lstMark.Any())
                {
                    retResult.Message = "Khong co du lieu diem nhan xet cap 2 nam hoc AcademicYearId=" + form.AcademicYearId;
                    return retResult;
                }

                var lstMarkTransfer = lstMark.Select(m => new TransJudgeRecordDefault
                {
                    OrderNumber = m.OrderNumber,
                    PupilId = m.PupilID,
                    Semester = (byte)(m.Semester ?? 0),
                    SubjectId = m.SubjectID,
                    Title = m.Title,
                    ModifiedDate = m.ModifiedDate,
                    SynchronizeID = m.SynchronizeID ?? 0,
                    Year = m.Year ?? form.Year,
                    Judgement = m.Judgement,
                    JudgeRecordId = m.JudgeRecordID,
                    MarkTypeId = (short)m.MarkTypeID,
                    SourceDB = m.MSourcedb
                }).ToList();

                List<int> lstPupilId = lstMark.Select(m => m.PupilID).Distinct().ToList();
                List<int> lstSubjectId = lstMark.Select(m => m.SubjectID).Distinct().ToList();
                List<int> lstSemesterId = lstMark.Select(m => m.Semester.Value).Distinct().ToList();


                var iqSummed = context.SummedUpRecord.Where(s => s.Last2digitNumberSchool == partitionId && s.AcademicYearID == form.AcademicYearId);
                iqSummed = iqSummed.Where(s => (!s.PeriodID.HasValue || s.PeriodID == 0));
                iqSummed = iqSummed.Where(s => lstPupilId.Contains(s.PupilID));
                iqSummed = iqSummed.Where(s => lstSubjectId.Contains(s.SubjectID));
                iqSummed = iqSummed.Where(s => lstSemesterId.Contains(s.Semester.Value));


                var lstSummed = iqSummed.ToList();
                List<TransSummedUpRecordDefault> lstTransSummed = new List<TransSummedUpRecordDefault>();
                if (lstSummed.Any())
                {
                    lstTransSummed = lstSummed.Select(s => new TransSummedUpRecordDefault
                    {
                        Comment = s.Comment,
                        IsCommenting = s.IsCommenting,
                        JudgementResult = s.JudgementResult,
                        PupilId = s.PupilID,
                        ReTestJudgement = s.ReTestJudgement,
                        ReTestMark = s.ReTestMark,
                        Semester = s.Semester ?? 0,
                        SubjectId = s.SubjectID,
                        SummedUpMark = s.SummedUpMark,
                        SummedUpRecordId = s.SummedUpRecordID,
                        SynchronizeID = s.SynchronizeID,
                        UpdateTime = s.SummedUpDate,
                        Year = s.CreatedAcademicYear
                    }).ToList();
                }

                retResult.ListJudgeRecord = lstMarkTransfer;
                retResult.ListSummed = lstTransSummed;
                retResult.ValidationCode = WCFConstant.RESPONSE_SUCCESS;
                retResult.Susscess = true;
                return retResult;
            }
            catch (Exception ex)
            {
                LogExtensions.GetCurrentClass = System.Reflection.MethodBase.GetCurrentMethod().DeclaringType;
                string para = string.Format("academicYearId={0}, schoolId={1}", form.AcademicYearId, form.SchoolId);
                LogExtensions.ErrorExt(logger, DateTime.Now, para, ex.Message, "null");
                retResult.Susscess = false;
                retResult.ValidationCode = WCFConstant.RESPONSE_EXCEPTION;
                retResult.Message = ex.Message;
                return retResult;
            }
            finally
            {
                SetAutoDetectChangesEnabled(context, true);
            }
        }

        public BoolResultResponse UpdateSyncjudgeRecord(List<long> lstMark, SyncAcademicYear form)
        {
            SMASEntities context = DeclareBusiness();
            BoolResultResponse retResulst = new BoolResultResponse();
            retResulst.Result = true;
            retResulst.ValidationCode = WCFConstant.RESPONSE_SUCCESS;
            try
            {

                if (lstMark == null || lstMark.Count == 0)
                {
                    retResulst.Result = false;
                    return retResulst;
                }
                SetAutoDetectChangesEnabled(context, false);

                int partitionId = UtilsBusiness.GetPartionId(form.SchoolId);
                var iqMarkUpdate = JudgeRecordBusiness.AllNoTracking.Where(m => m.Last2digitNumberSchool == partitionId && m.AcademicYearID == form.AcademicYearId);
                iqMarkUpdate = iqMarkUpdate.Where(m => lstMark.Contains(m.JudgeRecordID));
                var lstMarkUpdate = iqMarkUpdate.ToList();
                for (int i = 0; i < lstMark.Count; i++)
                {
                    JudgeRecord markUpdate = lstMarkUpdate[i];
                    string sourcedb = UtilsBusiness.CreateSourceDB(markUpdate.MSourcedb, form.SourceDB);
                    markUpdate.SynchronizeID = 1;
                    markUpdate.M_ProvinceID = form.ProvinceId;
                    markUpdate.MSourcedb = sourcedb;
                    context.JudgeRecord.Attach(markUpdate);
                    context.Entry(markUpdate).State = System.Data.EntityState.Modified;
                }
                context.SaveChanges();
                return retResulst;

            }
            catch (Exception ex)
            {
                LogExtensions.GetCurrentClass = System.Reflection.MethodBase.GetCurrentMethod().DeclaringType;
                string para = string.Format("academicYearId={0}, schoolId={1}", form.AcademicYearId, form.SchoolId);
                LogExtensions.ErrorExt(logger, DateTime.Now, para, ex.Message, "null");
                retResulst.Result = false;
                retResulst.ValidationCode = WCFConstant.RESPONSE_EXCEPTION;
                return retResulst;
            }
            finally
            {
                SetAutoDetectChangesEnabled(context, true);
            }
        }

        public TransPupilRankingResponse GetSyncPupilRanking(SyncAcademicYear form)
        {
            SMASEntities context = DeclareBusiness();
            TransPupilRankingResponse retResult = new TransPupilRankingResponse();
            retResult.Susscess = false;
            retResult.ListRanking = new List<TransPupilRankingDefault>();
            retResult.ValidationCode = WCFConstant.RESPONSE_PUPIL_PROFILEID_NOT_FOUND;
            try
            {
                SetAutoDetectChangesEnabled(context, false);

                int partitionId = UtilsBusiness.GetPartionId(form.SchoolId);
                var iqPupilRanking = context.PupilRanking.Where(m => m.Last2digitNumberSchool == partitionId && m.AcademicYearID == form.AcademicYearId);
                iqPupilRanking = iqPupilRanking.Where(m => m.PeriodID == 0 || !m.PeriodID.HasValue);
                //iqPupilRanking = iqPupilRanking.Where(m => !m.SynchronizeID.HasValue || (m.SynchronizeID > 0 && m.RankingDate >= form.ModifyDate));
                iqPupilRanking = iqPupilRanking.Where(p => (string.IsNullOrEmpty(p.MSourcedb) || !p.MSourcedb.Contains(form.SourceDB)) ||
                                          (p.MSourcedb.Contains(form.SourceDB) && p.RankingDate >= form.ModifyDate)
                                     );

                iqPupilRanking = iqPupilRanking.Take(form.RecordNumber);

                var iqPupilEmulation = context.PupilEmulation.Where(p => p.Last2digitNumberSchool == partitionId && p.AcademicYearID == form.AcademicYearId);

                var iqRanking = from pr in iqPupilRanking
                                join pe in iqPupilEmulation on new { pr.PupilID, pr.Semester } equals new { pe.PupilID, pe.Semester } into lpre
                                from x in lpre.DefaultIfEmpty()
                                select new TransPupilRankingDefault
                                {
                                    AverageMark = pr.AverageMark ?? 0,
                                    CapacityLevelId = pr.CapacityLevelID,
                                    ConductLevelId = pr.ConductLevelID,
                                    EducationLevelId = pr.EducationLevelID,
                                    HonourAchivementTypeId = x.HonourAchivementTypeID,
                                    IsCategory = pr.IsCategory,
                                    PupilRankingId = pr.PupilRankingID,
                                    Rank = pr.Rank,
                                    StudyingJudgementId = pr.StudyingJudgementID,
                                    TotalAbsentDaysWithoutPermission = pr.TotalAbsentDaysWithoutPermission,
                                    TotalAbsentDaysWithPermission = pr.TotalAbsentDaysWithPermission,
                                    PupilId = pr.PupilID,
                                    Semester = pr.Semester,
                                    RankingDate = pr.RankingDate,
                                    SynchronizeID = pr.SynchronizeID,
                                    Year = pr.Year,
                                    SourceDB = pr.MSourcedb
                                };

                var lstPupilRanking = iqRanking.ToList();
                if (!lstPupilRanking.Any())
                {
                    retResult.Message = "Khong co du lieu tong ket, xep loai nam hoc AcademicYearId=" + form.AcademicYearId;
                    return retResult;
                }
                retResult.ListRanking = lstPupilRanking;
                retResult.ValidationCode = WCFConstant.RESPONSE_SUCCESS;
                retResult.Susscess = true;
                return retResult;
            }
            catch (Exception ex)
            {
                LogExtensions.GetCurrentClass = System.Reflection.MethodBase.GetCurrentMethod().DeclaringType;
                string para = string.Format("academicYearId={0}, schoolId={1}", form.AcademicYearId, form.SchoolId);
                LogExtensions.ErrorExt(logger, DateTime.Now, para, ex.Message, "null");
                retResult.Susscess = false;
                retResult.ValidationCode = WCFConstant.RESPONSE_EXCEPTION;
                retResult.Message = ex.Message;
                return retResult;
            }
            finally
            {
                SetAutoDetectChangesEnabled(context, true);
            }
        }

        public BoolResultResponse UpdateSyncPupilRanking(List<long> lstRanking, SyncAcademicYear form)
        {
            SMASEntities context = DeclareBusiness();
            BoolResultResponse retResulst = new BoolResultResponse();
            retResulst.Result = true;
            retResulst.ValidationCode = WCFConstant.RESPONSE_SUCCESS;
            try
            {

                if (lstRanking == null || lstRanking.Count == 0)
                {
                    retResulst.Result = false;
                    return retResulst;
                }
                SetAutoDetectChangesEnabled(context, false);
                int partitionId = UtilsBusiness.GetPartionId(form.SchoolId);
                var iqPupilRankingUpdate = context.PupilRanking.Where(m => m.Last2digitNumberSchool == partitionId && m.AcademicYearID == form.AcademicYearId);
                iqPupilRankingUpdate = iqPupilRankingUpdate.Where(m => lstRanking.Contains(m.PupilRankingID));
                var lstPupilRankingUpdate = iqPupilRankingUpdate.ToList();
                for (int i = 0; i < lstPupilRankingUpdate.Count; i++)
                {
                    PupilRanking markUpdate = lstPupilRankingUpdate[i];
                    string sourcedb = UtilsBusiness.CreateSourceDB(markUpdate.MSourcedb, form.SourceDB);
                    markUpdate.SynchronizeID = 1;
                    markUpdate.M_ProvinceID = form.ProvinceId;
                    markUpdate.MSourcedb = sourcedb;
                    context.PupilRanking.Attach(markUpdate);
                    context.Entry(markUpdate).State = System.Data.EntityState.Modified;
                }
                context.SaveChanges();
                return retResulst;

            }
            catch (Exception ex)
            {
                LogExtensions.GetCurrentClass = System.Reflection.MethodBase.GetCurrentMethod().DeclaringType;
                string para = string.Format("academicYearId={0}, schoolId={1}", form.AcademicYearId, form.SchoolId);
                LogExtensions.ErrorExt(logger, DateTime.Now, para, ex.Message, "null");
                retResulst.Result = false;
                retResulst.ValidationCode = WCFConstant.RESPONSE_EXCEPTION;
                retResulst.Description = ex.Message;
                return retResulst;
            }
            finally
            {
                SetAutoDetectChangesEnabled(context, true);
            }
        }

        public TransEvaluationCommentsResponse GetSyncEvaluationComments(SyncAcademicYear form)
        {
            TransEvaluationCommentsResponse retResult = new TransEvaluationCommentsResponse();
            retResult.Susscess = false;
            retResult.ListEvaluationComments = new List<TransEvaluationCommentsDefault>();
            retResult.ValidationCode = WCFConstant.RESPONSE_PUPIL_PROFILEID_NOT_FOUND;
            try
            {
                DeclareBusiness();
                EvaluationCommentsBusiness.SetAutoDetectChangesEnabled(false);
                int partitionId = UtilsBusiness.GetPartionId(form.SchoolId);
                var iqEvaluationComment = EvaluationCommentsBusiness.AllNoTracking.Where(e => e.LastDigitSchoolID == partitionId && e.AcademicYearID == form.AcademicYearId);
                iqEvaluationComment = iqEvaluationComment.Where(e => ((!e.SynchronizeID.HasValue || e.SynchronizeID == 0) || (e.SynchronizeID > 0 && e.UpdateTime >= form.ModifyDate)));
                iqEvaluationComment = iqEvaluationComment.Take(form.RecordNumber);
                var lstEvaluationComent = iqEvaluationComment.ToList();
                if (!lstEvaluationComent.Any())
                {
                    retResult.Message = string.Format(" Khong co du lieu nhan xet tieu hoc, SchoolId={0} AcademicYearId={1}", form.SchoolId, form.AcademicYearId);
                    return retResult;
                }
                var lstTransEvaluationComment = lstEvaluationComent.Select(e => new TransEvaluationCommentsDefault
                {
                    CommentM1M6 = e.CommentsM1M6,
                    CommentM2M7 = e.CommentsM2M7,
                    CommentM3M8 = e.CommentsM3M8,
                    CommentM4M9 = e.CommentsM4M9,
                    CommentM5M10 = e.CommentsM5M10,
                    EvaluationCommentId = e.EvaluationCommentsID,
                    EvaluationCriteriaId = e.EvaluationCriteriaID,
                    EvaluationId = e.EvaluationID,
                    PupilId = e.PupilID,
                    Semester = e.SemesterID,
                    TypeOfTeacher = e.TypeOfTeacher,
                    SynchronizeID = e.SynchronizeID ?? 0,
                    UpdateTime = e.UpdateTime,
                    Year = form.Year
                }).ToList();

                retResult.ListEvaluationComments = lstTransEvaluationComment;
                retResult.ValidationCode = WCFConstant.RESPONSE_SUCCESS;
                retResult.Susscess = true;
                return retResult;
            }
            catch (Exception ex)
            {
                LogExtensions.GetCurrentClass = System.Reflection.MethodBase.GetCurrentMethod().DeclaringType;
                string para = string.Format("academicYearId={0}, schoolId={1}", form.AcademicYearId, form.SchoolId);
                LogExtensions.ErrorExt(logger, DateTime.Now, para, ex.Message, "null");
                retResult.Susscess = false;
                retResult.ValidationCode = WCFConstant.RESPONSE_EXCEPTION;
                retResult.Message = ex.StackTrace;
                return retResult;
            }
            finally
            {
                EvaluationCommentsBusiness.SetAutoDetectChangesEnabled(true);
            }
        }

        public BoolResultResponse UpdateSyncEvaluationComments(List<long> lstEvaluationComments, SyncAcademicYear form)
        {
            BoolResultResponse retResulst = new BoolResultResponse();
            retResulst.Result = true;
            retResulst.ValidationCode = WCFConstant.RESPONSE_SUCCESS;
            try
            {
                DeclareBusiness();
                EvaluationCommentsBusiness.SetAutoDetectChangesEnabled(false);

                if (!lstEvaluationComments.Any())
                {
                    retResulst.Result = false;
                    return retResulst;
                }

                int partitionId = UtilsBusiness.GetPartionId(form.SchoolId);
                var iqEvaluationComments = EvaluationCommentsBusiness.AllNoTracking.Where(m => m.LastDigitSchoolID == partitionId && m.AcademicYearID == form.AcademicYearId);
                iqEvaluationComments = iqEvaluationComments.Where(m => lstEvaluationComments.Contains(m.EvaluationCommentsID));
                var lstEvaluationUpdate = iqEvaluationComments.ToList();
                for (int i = 0; i < lstEvaluationUpdate.Count; i++)
                {
                    EvaluationComments markUpdate = lstEvaluationUpdate[i];
                    markUpdate.SynchronizeID = 1;

                    EvaluationCommentsBusiness.Update(markUpdate);
                }
                EvaluationCommentsBusiness.Save();
                return retResulst;

            }
            catch (Exception ex)
            {
                LogExtensions.GetCurrentClass = System.Reflection.MethodBase.GetCurrentMethod().DeclaringType;
                string para = string.Format("academicYearId={0}, schoolId={1}", form.AcademicYearId, form.SchoolId);
                LogExtensions.ErrorExt(logger, DateTime.Now, para, ex.Message, "null");
                retResulst.Result = false;
                retResulst.ValidationCode = WCFConstant.RESPONSE_EXCEPTION;
                return retResulst;
            }
            finally
            {
                EvaluationCommentsBusiness.SetAutoDetectChangesEnabled(true);
            }
        }

        public TransSummedEvaluationResponse GetSyncSummedEvaluation(SyncAcademicYear form)
        {
            TransSummedEvaluationResponse retResult = new TransSummedEvaluationResponse();
            retResult.Susscess = false;
            retResult.ListSummedEvaluation = new List<TransSummedEvaluationDefault>();
            retResult.ValidationCode = WCFConstant.RESPONSE_PUPIL_PROFILEID_NOT_FOUND;
            try
            {
                DeclareBusiness();
                SummedEvaluationBusiness.SetAutoDetectChangesEnabled(false);
                int partitionId = UtilsBusiness.GetPartionId(form.SchoolId);
                var iqEvaluationComment = SummedEvaluationBusiness.AllNoTracking.Where(e => e.LastDigitSchoolID == partitionId && e.AcademicYearID == form.AcademicYearId);
                iqEvaluationComment = iqEvaluationComment.Where(e => (!e.SynchronizeID.HasValue || e.SynchronizeID == 0) || (e.SynchronizeID > 0 && e.UpdateTime >= form.ModifyDate));
                iqEvaluationComment = iqEvaluationComment.Take(form.RecordNumber);
                var lstSummedEvaluation = iqEvaluationComment.ToList();
                if (!lstSummedEvaluation.Any())
                {
                    retResult.Message = "Khong co du lieu danh gia cuoi ky cap 1, SchoolId = " + form.SchoolId;
                    return retResult;
                }

                var lstTrans = lstSummedEvaluation.Select(s => new TransSummedEvaluationDefault
                {
                    EndingComment = s.EndingComments,
                    EndingEvaluation = s.EndingEvaluation,
                    EvaluationCriteriaId = s.EvaluationCriteriaID,
                    EvaluationId = s.EvaluationID,
                    EvaluationRetraining = s.EvaluationReTraining,
                    PeriodicEndingMark = s.PeriodicEndingMark,
                    PupilId = s.PupilID,
                    RetestMark = s.RetestMark,
                    Semester = s.SemesterID,
                    SummedEvaluationId = s.SummedEvaluationID,
                    SynchronizeID = s.SynchronizeID ?? 0,
                    UpdateTime = s.UpdateTime,
                    Year = form.Year

                }).ToList();


                retResult.ListSummedEvaluation = lstTrans;
                retResult.ValidationCode = WCFConstant.RESPONSE_SUCCESS;
                retResult.Susscess = true;
                return retResult;
            }
            catch (Exception ex)
            {
                LogExtensions.GetCurrentClass = System.Reflection.MethodBase.GetCurrentMethod().DeclaringType;
                string para = string.Format("academicYearId={0}, schoolId={1}", form.AcademicYearId, form.SchoolId);
                LogExtensions.ErrorExt(logger, DateTime.Now, para, ex.Message, "null");
                retResult.Susscess = false;
                retResult.ValidationCode = WCFConstant.RESPONSE_EXCEPTION;
                retResult.Message = ex.StackTrace;
                return retResult;
            }
            finally
            {
                SummedEvaluationBusiness.SetAutoDetectChangesEnabled(true);
            }
        }

        public BoolResultResponse UpdateSyncSummedEvaluation(List<long> lstSummedEvaluation, SyncAcademicYear form)
        {
            BoolResultResponse retResulst = new BoolResultResponse();
            retResulst.Result = true;
            retResulst.ValidationCode = WCFConstant.RESPONSE_SUCCESS;
            try
            {
                DeclareBusiness();
                SummedEvaluationBusiness.SetAutoDetectChangesEnabled(false);

                if (!lstSummedEvaluation.Any())
                {
                    retResulst.Result = false;
                    return retResulst;
                }

                int partitionId = UtilsBusiness.GetPartionId(form.SchoolId);
                var iqSummedEvaluation = SummedEvaluationBusiness.AllNoTracking.Where(m => m.LastDigitSchoolID == partitionId && m.AcademicYearID == form.AcademicYearId);
                iqSummedEvaluation = iqSummedEvaluation.Where(m => lstSummedEvaluation.Contains(m.SummedEvaluationID));
                var lstEvaluationUpdate = iqSummedEvaluation.ToList();
                for (int i = 0; i < lstEvaluationUpdate.Count; i++)
                {
                    SummedEvaluation markUpdate = lstEvaluationUpdate[i];
                    markUpdate.SynchronizeID = 1;
                    SummedEvaluationBusiness.Update(markUpdate);
                }
                SummedEvaluationBusiness.Save();
                return retResulst;

            }
            catch (Exception ex)
            {
                LogExtensions.GetCurrentClass = System.Reflection.MethodBase.GetCurrentMethod().DeclaringType;
                string para = string.Format("academicYearId={0}, schoolId={1}", form.AcademicYearId, form.SchoolId);
                LogExtensions.ErrorExt(logger, DateTime.Now, para, ex.Message, "null");
                retResulst.Result = false;
                retResulst.ValidationCode = WCFConstant.RESPONSE_EXCEPTION;
                return retResulst;
            }
            finally
            {
                SummedEvaluationBusiness.SetAutoDetectChangesEnabled(true);
            }
        }

        public TransSummedEndingEvaluationResponse GetSyncSummedEnding(SyncAcademicYear form)
        {
            TransSummedEndingEvaluationResponse retResult = new TransSummedEndingEvaluationResponse();
            retResult.Susscess = false;
            retResult.ListSummedEnding = new List<TransSummedEndingEvaluationDefault>();
            retResult.ValidationCode = WCFConstant.RESPONSE_PUPIL_PROFILEID_NOT_FOUND;
            try
            {
                DeclareBusiness();
                SummedEndingEvaluationBusiness.SetAutoDetectChangesEnabled(false);
                int partitionId = UtilsBusiness.GetPartionId(form.SchoolId);
                var iqSummedEnding = SummedEndingEvaluationBusiness.AllNoTracking.Where(e => e.LastDigitSchoolID == partitionId && e.AcademicYearID == form.AcademicYearId);
                iqSummedEnding = iqSummedEnding.Where(e => !e.SynchronizeID.HasValue || (e.SynchronizeID > 0 && e.UpdateTime >= form.ModifyDate));
                iqSummedEnding = iqSummedEnding.Take(form.RecordNumber);
                var lstSummedEnding = iqSummedEnding.ToList();
                if (!lstSummedEnding.Any())
                {                    
                    retResult.Message = "Khong co du lieu nhan xet tieu hoc, SchoolId = " + form.SchoolId;
                    return retResult;
                }
                var lstTransSummedEnding = lstSummedEnding.Select(e => new TransSummedEndingEvaluationDefault
                {
                    EndingEvaluation = e.EndingEvaluation,
                    EvaluationId = e.EvaluationID,
                    EvaluationRetraining = e.EvaluationReTraining,
                    PupilId = e.PupilID,
                    RateAdd = (e.RateAdd.HasValue) ? e.RateAdd == 1 ? "HT" : "CHT" : "",
                    Semester = e.SemesterID,
                    SummedEndingEvaluationId = e.SummedEndingEvaluationID,
                    SynchronizeID = e.SynchronizeID,
                    UpdateTime = e.UpdateTime,
                    Year = form.Year
                }).ToList();


                retResult.ListSummedEnding = lstTransSummedEnding;
                retResult.ValidationCode = WCFConstant.RESPONSE_SUCCESS;
                retResult.Susscess = true;
                return retResult;
            }
            catch (Exception ex)
            {
                LogExtensions.GetCurrentClass = System.Reflection.MethodBase.GetCurrentMethod().DeclaringType;
                string para = string.Format("academicYearId={0}, schoolId={1}", form.AcademicYearId, form.SchoolId);
                LogExtensions.ErrorExt(logger, DateTime.Now, para, ex.Message, "null");
                retResult.Susscess = false;
                retResult.ValidationCode = WCFConstant.RESPONSE_EXCEPTION;
                retResult.Message = ex.StackTrace;
                return retResult;
            }
            finally
            {
                SummedEndingEvaluationBusiness.SetAutoDetectChangesEnabled(true);
            }
        }

        public BoolResultResponse UpdateSyncSummedEnding(List<long> lstSummedEndingId, SyncAcademicYear form)
        {
            BoolResultResponse retResulst = new BoolResultResponse();
            retResulst.Result = true;
            retResulst.ValidationCode = WCFConstant.RESPONSE_SUCCESS;
            try
            {
                DeclareBusiness();
                SummedEndingEvaluationBusiness.SetAutoDetectChangesEnabled(false);

                if (!lstSummedEndingId.Any())
                {
                    retResulst.Result = false;
                    return retResulst;
                }

                int partitionId = UtilsBusiness.GetPartionId(form.SchoolId);
                var iqSummedEvaluation = SummedEndingEvaluationBusiness.AllNoTracking.Where(m => m.LastDigitSchoolID == partitionId && m.AcademicYearID == form.AcademicYearId);
                iqSummedEvaluation = SummedEndingEvaluationBusiness.AllNoTracking.Where(m => lstSummedEndingId.Contains(m.SummedEndingEvaluationID));
                var lstEvaluationUpdate = iqSummedEvaluation.ToList();
                for (int i = 0; i < lstEvaluationUpdate.Count; i++)
                {
                    SummedEndingEvaluation markUpdate = lstEvaluationUpdate[i];
                    markUpdate.SynchronizeID = 1;
                    SummedEndingEvaluationBusiness.Update(markUpdate);
                }
                SummedEndingEvaluationBusiness.Save();
                return retResulst;

            }
            catch (Exception ex)
            {
                LogExtensions.GetCurrentClass = System.Reflection.MethodBase.GetCurrentMethod().DeclaringType;
                string para = string.Format("academicYearId={0}, schoolId={1}", form.AcademicYearId, form.SchoolId);
                LogExtensions.ErrorExt(logger, DateTime.Now, para, ex.Message, "null");
                retResulst.Result = false;
                retResulst.ValidationCode = WCFConstant.RESPONSE_EXCEPTION;
                return retResulst;
            }
            finally
            {
                SummedEndingEvaluationBusiness.SetAutoDetectChangesEnabled(true);
            }
        }

        #endregion
    }
}
