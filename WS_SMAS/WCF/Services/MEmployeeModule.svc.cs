﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;
using SMAS.Models.Models;
using WCF.Composite;
using WCF.Composite.Migrate;
using SMAS.Business.IBusiness;
using log4net;
using SMAS.Business.Business;
using SMAS.Business.Common;
using WCF.Composite.Sync;
using SMAS.VTUtils.Log;

namespace WCF.Services
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the class name "MEmployeeModule" in code, svc and config file together.
    // NOTE: In order to launch WCF Test Client for testing this service, please select MEmployeeModule.svc or MEmployeeModule.svc.cs at the Solution Explorer and start debugging.
    public class MEmployeeModule : IMEmployeeModule
    {
        #region Declare Variable
        private IEmployeeBusiness EmployeeBusiness;
        private ISchoolFacultyBusiness SchoolFacultyBusiness;
        private static readonly log4net.ILog logger = LogManager.GetLogger(typeof(MEmployeeModule));

        public MEmployeeModule()
        {

        }
        #endregion  

        private SMASEntities DeclareBusiness()
        {
            var context = new SMASEntities();
            context.Configuration.AutoDetectChangesEnabled = false;
            context.Configuration.ValidateOnSaveEnabled = false;

            EmployeeBusiness = new EmployeeBusiness(logger, context);
            SchoolFacultyBusiness = new SchoolFacultyBusiness(logger, context);

            return context;
        }
        public void SetAutoDetectChangesEnabled(SMASEntities context, bool isAutoDetectChangesEnabled)
        {
            context.Configuration.ValidateOnSaveEnabled = isAutoDetectChangesEnabled;
            context.Configuration.AutoDetectChangesEnabled = isAutoDetectChangesEnabled;

            /*if (context != null)
            {
                context.Dispose();
            }*/
        }

        #region SyncHue

       
        public MEmployee GetEmployeebySchoolId(int schoolId)
        {
            MEmployee retval;
            SMASEntities context = DeclareBusiness();
            try
            {
                List<Employee> lstEmployee = context.Employee.Where(s => s.SchoolID == schoolId && s.IsActive).ToList();
                if (lstEmployee == null)
                {
                    return new MEmployee
                    {
                        ListEmployee = new List<MEmployeeDefault>(),
                        Susscess = true,
                        Message = "Chua co du lieu giao vien"
                    };
                }

                List<MEmployeeDefault> lstVal = lstEmployee.Select(s => new MEmployeeDefault
                {
                    Alias = s.Alias,
                    AppliedLevel = s.AppliedLevel,
                    BirthDate = s.BirthDate,
                    BirthPlace = s.BirthPlace,
                    CommunistPartyJoinedDate = s.CommunistPartyJoinedDate,
                    CommunistPartyJoinedPlace = s.CommunistPartyJoinedPlace,
                    ContractID = s.ContractID,
                    ContractTypeID = s.ContractTypeID,
                    CreatedDate = s.CreatedDate,
                    DedicatedForYoungLeague = s.DedicatedForYoungLeague,
                    Description = s.Description,
                    EducationalManagementGradeID = s.EducationalManagementGradeID,
                    Email = s.Email,
                    EmployeeCode = s.EmployeeCode,
                    EmployeeID = s.EmployeeID,
                    EmployeeType = s.EmployeeType,
                    EmploymentStatus = s.EmploymentStatus,
                    EthnicID = s.EthnicID,
                    FamilyTypeID = s.FamilyTypeID,
                    FatherBirthDate = s.FatherBirthDate,
                    FatherFullName = s.FatherFullName,
                    FatherJob = s.FatherJob,
                    FatherWorkingPlace = s.FatherWorkingPlace,
                    ForeignLanguageGradeID = s.ForeignLanguageGradeID,
                    ForeignLanguageQualification = s.ForeignLanguageQualification,
                    FullName = s.FullName,
                    Genre = s.Genre,
                    GraduationLevelID = s.GraduationLevelID,
                    HealthStatus = s.HealthStatus,
                    HomeTown = s.HomeTown,
                    IdentityIssuedDate = s.IdentityIssuedDate,
                    IdentityIssuedPlace = s.IdentityIssuedPlace,
                    IdentityNumber = s.IdentityNumber,
                    IntoSchoolDate = s.IntoSchoolDate,
                    IsActive = s.IsActive,
                    IsCommunistPartyMember = s.IsCommunistPartyMember,
                    IsSyndicate = s.IsSyndicate,
                    IsYouthLeageMember = s.IsYouthLeageMember,
                    ITQualificationLevelID = s.ITQualificationLevelID,
                    JoinedDate = s.JoinedDate,
                    MariageStatus = s.MariageStatus,
                    Mobile = s.Mobile,
                    ModifiedDate = s.ModifiedDate,
                    MotelRoomOutsite = s.MotelRoomOutsite,
                    MotherBirthDate = s.MotherBirthDate,
                    MotherFullName = s.MotherFullName,
                    MotherJob = s.MotherJob,
                    MotherWorkingPlace = s.MotherWorkingPlace,
                    MSourcedb = s.MSourcedb,
                    M_FamilyTypeName = s.M_FamilyTypeName,
                    M_OldEmployeeID = s.M_OldEmployeeID,
                    M_OldID = s.M_OldID,
                    M_OldTeacherID = s.M_OldTeacherID,
                    M_ProvinceID = s.M_ProvinceID,
                    Name = s.Name,
                    NeedTeacherDuties = s.NeedTeacherDuties,
                    Note = s.Note,
                    PermanentResidentalAddress = s.PermanentResidentalAddress,
                    PoliticalGradeID = s.PoliticalGradeID,
                    PrimarilyAssignedSubjectID = s.PrimarilyAssignedSubjectID,
                    QualificationLevelID = s.QualificationLevelID,
                    QualificationTypeID = s.QualificationTypeID,
                    RegularRefresher = s.RegularRefresher,
                    ReligionID = s.ReligionID,
                    SchoolFacultyID = s.SchoolFacultyID,
                    SchoolID = s.SchoolID,
                    SpecialityCatID = s.SpecialityCatID,
                    SpouseBirthDate = s.SpouseBirthDate,
                    SpouseFullName = s.SpouseFullName,
                    TempResidentalAddress = s.TempResidentalAddress,
                    SpouseJob = s.SpouseJob,
                    SpouseWorkingPlace = s.SpouseWorkingPlace,
                    StaffPositionID = s.StaffPositionID,
                    StartingDate = s.StartingDate,
                    StateManagementGradeID = s.StateManagementGradeID,
                    SupervisingDeptID = s.SupervisingDeptID,
                    SynchronizeID = s.SynchronizeID,
                    SyndicateDate = s.SyndicateDate,
                    TeacherDuties = s.TeacherDuties,
                    Telephone = s.Telephone,
                    TrainingLevelID = s.TrainingLevelID,
                    WorkGroupTypeID = s.WorkGroupTypeID,
                    WorkTypeID = s.WorkTypeID,
                    YouthLeagueJoinedDate = s.YouthLeagueJoinedDate,
                    YouthLeagueJoinedPlace = s.YouthLeagueJoinedPlace
                }).ToList();
                retval = new MEmployee
                {
                    ListEmployee = lstVal,
                    Susscess = true,
                    Message = "Lay danh sach can bo thanh cong"
                };
                return retval;
            }
            catch (Exception ex)
            {
                string method = string.Format("{0}.{1}", System.Reflection.MethodBase.GetCurrentMethod().DeclaringType.FullName, System.Reflection.MethodBase.GetCurrentMethod().Name);
                string para = string.Format("SchoolId={0}",schoolId);
                LogExtensions.ErrorExt(logger, DateTime.Now, method, para, ex.Message, "null");
                retval = new MEmployee
                {
                    ListEmployee = new List<MEmployeeDefault>(),
                    Susscess = false,
                    Message = ex.Message
                };
                return retval;
            }
            finally
            {
                SetAutoDetectChangesEnabled(context, true);
            }
        }

        public MSchoolFaculty GetFaculty(int schoolId)
        {
            MSchoolFaculty retval;
            SMASEntities context = DeclareBusiness();
            try
            {
                List<SchoolFaculty> lstFaculty = context.SchoolFaculty.Where(s => s.SchoolID == schoolId && s.IsActive).ToList();
                if (lstFaculty == null)
                {
                    return new MSchoolFaculty
                    {
                        ListSchhoolFaculty = new List<MSchoolFacultyDefault>(),
                        Susscess = true,
                        Message = "Chua co du lieu giao vien"
                    };

                }

                List<MSchoolFacultyDefault> lstVal = lstFaculty.Select(s => new MSchoolFacultyDefault
                {
                    CreatedDate = s.CreatedDate,
                    Description = s.Description,
                    FacultyName = s.FacultyName,
                    IsActive = s.IsActive,
                    ModifiedDate = s.ModifiedDate,
                    MSourcedb = s.MSourcedb,
                    M_OldID = s.M_OldID,
                    M_ProvinceID = s.M_ProvinceID,
                    SchoolFacultyID = s.SchoolFacultyID,
                    SchoolID = s.SchoolID,
                    SynchronizeID = s.SynchronizeID
                }).ToList();
                retval = new MSchoolFaculty
                {
                    ListSchhoolFaculty = lstVal,
                    Susscess = true,
                    Message = "Lay danh sach to bo mon thanh cong"
                };
                return retval;
            }
            catch (Exception ex)
            {
                string method = string.Format("{0}.{1}", System.Reflection.MethodBase.GetCurrentMethod().DeclaringType.FullName, System.Reflection.MethodBase.GetCurrentMethod().Name);
                string para = string.Format("SchoolId={0}", schoolId);
                LogExtensions.ErrorExt(logger, DateTime.Now, method, para, ex.Message, "null");
                retval = new MSchoolFaculty
                {
                    ListSchhoolFaculty = new List<MSchoolFacultyDefault>(),
                    Susscess = false,
                    Message = ex.Message
                };
                return retval;
            }
            finally
            {
                SetAutoDetectChangesEnabled(context, true);
            }
        }

        public BoolResultResponse InsertEmployee(List<MEmployeeDefault> lstEmploye)
        {
            SMASEntities context = DeclareBusiness();
            try
            {
                if (lstEmploye == null || lstEmploye.Count == 0)
                {
                    return new BoolResultResponse
                    {
                        Result = true,
                        Description = "Khong co du lieu can bo"
                    };
                }
                List<Employee> lstInsertEmployee = new List<Employee>();
                Employee emp;
                foreach (var s in lstEmploye)
                {
                    emp = new Employee
                    {
                        Alias = s.Alias,
                        AppliedLevel = s.AppliedLevel,
                        BirthDate = s.BirthDate,
                        BirthPlace = s.BirthPlace,
                        CommunistPartyJoinedDate = s.CommunistPartyJoinedDate,
                        CommunistPartyJoinedPlace = s.CommunistPartyJoinedPlace,
                        ContractID = s.ContractID,
                        ContractTypeID = s.ContractTypeID,
                        CreatedDate = s.CreatedDate,
                        DedicatedForYoungLeague = s.DedicatedForYoungLeague,
                        Description = s.Description,
                        EducationalManagementGradeID = s.EducationalManagementGradeID,
                        Email = s.Email,
                        EmployeeCode = s.EmployeeCode,
                        EmployeeID = s.EmployeeID,
                        EmployeeType = s.EmployeeType,
                        EmploymentStatus = s.EmploymentStatus,
                        EthnicID = s.EthnicID,
                        FamilyTypeID = s.FamilyTypeID,
                        FatherBirthDate = s.FatherBirthDate,
                        FatherFullName = s.FatherFullName,
                        FatherJob = s.FatherJob,
                        FatherWorkingPlace = s.FatherWorkingPlace,
                        ForeignLanguageGradeID = s.ForeignLanguageGradeID,
                        ForeignLanguageQualification = s.ForeignLanguageQualification,
                        FullName = s.FullName,
                        Genre = s.Genre,
                        GraduationLevelID = s.GraduationLevelID,
                        HealthStatus = s.HealthStatus,
                        HomeTown = s.HomeTown,
                        IdentityIssuedDate = s.IdentityIssuedDate,
                        IdentityIssuedPlace = s.IdentityIssuedPlace,
                        IdentityNumber = s.IdentityNumber,
                        IntoSchoolDate = s.IntoSchoolDate,
                        IsActive = s.IsActive,
                        IsCommunistPartyMember = s.IsCommunistPartyMember,
                        IsSyndicate = s.IsSyndicate,
                        IsYouthLeageMember = s.IsYouthLeageMember,
                        ITQualificationLevelID = s.ITQualificationLevelID,
                        JoinedDate = s.JoinedDate,
                        MariageStatus = s.MariageStatus,
                        Mobile = s.Mobile,
                        ModifiedDate = s.ModifiedDate,
                        MotelRoomOutsite = s.MotelRoomOutsite,
                        MotherBirthDate = s.MotherBirthDate,
                        MotherFullName = s.MotherFullName,
                        MotherJob = s.MotherJob,
                        MotherWorkingPlace = s.MotherWorkingPlace,
                        MSourcedb = s.MSourcedb,
                        M_FamilyTypeName = s.M_FamilyTypeName,
                        M_OldEmployeeID = s.M_OldEmployeeID,
                        M_OldID = s.M_OldID,
                        M_OldTeacherID = s.M_OldTeacherID,
                        M_ProvinceID = s.M_ProvinceID,
                        Name = s.Name,
                        NeedTeacherDuties = s.NeedTeacherDuties,
                        Note = s.Note,
                        PermanentResidentalAddress = s.PermanentResidentalAddress,
                        PoliticalGradeID = s.PoliticalGradeID,
                        PrimarilyAssignedSubjectID = s.PrimarilyAssignedSubjectID,
                        QualificationLevelID = s.QualificationLevelID,
                        QualificationTypeID = s.QualificationTypeID,
                        RegularRefresher = s.RegularRefresher,
                        ReligionID = s.ReligionID,
                        SchoolFacultyID = s.SchoolFacultyID,
                        SchoolID = s.SchoolID,
                        SpecialityCatID = s.SpecialityCatID,
                        SpouseBirthDate = s.SpouseBirthDate,
                        SpouseFullName = s.SpouseFullName,
                        TempResidentalAddress = s.TempResidentalAddress,
                        SpouseJob = s.SpouseJob,
                        SpouseWorkingPlace = s.SpouseWorkingPlace,
                        StaffPositionID = s.StaffPositionID,
                        StartingDate = s.StartingDate,
                        StateManagementGradeID = s.StateManagementGradeID,
                        SupervisingDeptID = s.SupervisingDeptID,
                        SynchronizeID = s.SynchronizeID,
                        SyndicateDate = s.SyndicateDate,
                        TeacherDuties = s.TeacherDuties,
                        Telephone = s.Telephone,
                        TrainingLevelID = s.TrainingLevelID,
                        WorkGroupTypeID = s.WorkGroupTypeID,
                        WorkTypeID = s.WorkTypeID,
                        YouthLeagueJoinedDate = s.YouthLeagueJoinedDate,
                        YouthLeagueJoinedPlace = s.YouthLeagueJoinedPlace
                    };
                    lstInsertEmployee.Add(emp);
                }
                if (lstInsertEmployee.Count > 0)
                {
                    var retVal = EmployeeBusiness.BulkInsert(WCFUtils.GetConnectionString, lstInsertEmployee, ColumnMapping.Instance.Employee(), "EmployeeID", "Image");
                    if (retVal > 0)
                    {
                        return new BoolResultResponse
                        {
                            Result = true,
                            Description = "Them moi can bo thanh cong"
                        };
                    }

                }
                return new BoolResultResponse
                {
                    Result = false,
                    Description = "Them moi can bo khong thanh cong"
                };
            }
            catch (Exception ex)
            {
                string method = string.Format("{0}.{1}", System.Reflection.MethodBase.GetCurrentMethod().DeclaringType.FullName, System.Reflection.MethodBase.GetCurrentMethod().Name);
                string para = "null";
                LogExtensions.ErrorExt(logger, DateTime.Now, method, para, ex.Message, "null");
                return new BoolResultResponse
                {
                    Description = ex.Message,
                    Result = false,
                };
            }
            finally
            {
                SetAutoDetectChangesEnabled(context, true);
            }
        }

        public BoolResultResponse InsertFaculty(List<MSchoolFacultyDefault> lstSchoolFaculty)
        {
            SMASEntities context = DeclareBusiness();
            try
            {
                if (lstSchoolFaculty == null || lstSchoolFaculty.Count == 0)
                {
                    return new BoolResultResponse
                    {
                        Result = true,
                        Description = "Khong co du lieu to bo mon"
                    };
                }
                List<SchoolFaculty> lstInsertSchoolFaculty = new List<SchoolFaculty>();
                SchoolFaculty emp;
                foreach (var s in lstSchoolFaculty)
                {
                    emp = new SchoolFaculty
                    {
                        CreatedDate = DateTime.Now,
                        Description = s.Description,
                        FacultyName = s.FacultyName,
                        IsActive = true,
                        MSourcedb = s.MSourcedb,
                        M_OldID = s.M_OldID,
                        M_ProvinceID = s.M_ProvinceID,
                        SchoolID = s.SchoolID,
                        SynchronizeID = s.SynchronizeID
                    };
                    lstInsertSchoolFaculty.Add(emp);
                }
                if (lstInsertSchoolFaculty.Count > 0)
                {
                    var retVal = SchoolFacultyBusiness.BulkInsert(WCFUtils.GetConnectionString, lstInsertSchoolFaculty, ColumnMapping.Instance.SchoolFaculty(), "EmployeeID", "SchoolFacultyID");
                    if (retVal > 0)
                    {
                        return new BoolResultResponse
                        {
                            Result = true,
                            Description = "Them moi to bo mon thanh cong"
                        };
                    }

                }
                return new BoolResultResponse
                {
                    Result = false,
                    Description = "Them moi to bo mon khong thanh cong"
                };
            }
            catch (Exception ex)
            {
                string method = string.Format("{0}.{1}", System.Reflection.MethodBase.GetCurrentMethod().DeclaringType.FullName, System.Reflection.MethodBase.GetCurrentMethod().Name);
                string para = "null";
                LogExtensions.ErrorExt(logger, DateTime.Now, method, para, ex.Message, "null");
                return new BoolResultResponse
                {
                    Description = ex.Message,
                    Result = false,
                };
            }
            finally
            {
                SetAutoDetectChangesEnabled(context, true);
            }
        }

        public BoolResultResponse UpdateEmployee(List<MEmployeeDefault> lstEmploye)
        {
            SMASEntities context = DeclareBusiness();
            try
            {
                if (lstEmploye == null || lstEmploye.Count == 0)
                {
                    return new BoolResultResponse
                    {
                        Result = true,
                        Description = "Khong co du lieu can bo de cap nhat"
                    };
                }
                Employee emp;
                foreach (var s in lstEmploye)
                {
                    emp = new Employee
                    {
                        Alias = s.Alias,
                        AppliedLevel = s.AppliedLevel,
                        BirthDate = s.BirthDate,
                        BirthPlace = s.BirthPlace,
                        CommunistPartyJoinedDate = s.CommunistPartyJoinedDate,
                        CommunistPartyJoinedPlace = s.CommunistPartyJoinedPlace,
                        ContractID = s.ContractID,
                        ContractTypeID = s.ContractTypeID,
                        CreatedDate = s.CreatedDate,
                        DedicatedForYoungLeague = s.DedicatedForYoungLeague,
                        Description = s.Description,
                        EducationalManagementGradeID = s.EducationalManagementGradeID,
                        Email = s.Email,
                        EmployeeCode = s.EmployeeCode,
                        EmployeeID = s.EmployeeID,
                        EmployeeType = s.EmployeeType,
                        EmploymentStatus = s.EmploymentStatus,
                        EthnicID = s.EthnicID,
                        FamilyTypeID = s.FamilyTypeID,
                        FatherBirthDate = s.FatherBirthDate,
                        FatherFullName = s.FatherFullName,
                        FatherJob = s.FatherJob,
                        FatherWorkingPlace = s.FatherWorkingPlace,
                        ForeignLanguageGradeID = s.ForeignLanguageGradeID,
                        ForeignLanguageQualification = s.ForeignLanguageQualification,
                        FullName = s.FullName,
                        Genre = s.Genre,
                        GraduationLevelID = s.GraduationLevelID,
                        HealthStatus = s.HealthStatus,
                        HomeTown = s.HomeTown,
                        IdentityIssuedDate = s.IdentityIssuedDate,
                        IdentityIssuedPlace = s.IdentityIssuedPlace,
                        IdentityNumber = s.IdentityNumber,
                        IntoSchoolDate = s.IntoSchoolDate,
                        IsActive = s.IsActive,
                        IsCommunistPartyMember = s.IsCommunistPartyMember,
                        IsSyndicate = s.IsSyndicate,
                        IsYouthLeageMember = s.IsYouthLeageMember,
                        ITQualificationLevelID = s.ITQualificationLevelID,
                        JoinedDate = s.JoinedDate,
                        MariageStatus = s.MariageStatus,
                        Mobile = s.Mobile,
                        ModifiedDate = s.ModifiedDate,
                        MotelRoomOutsite = s.MotelRoomOutsite,
                        MotherBirthDate = s.MotherBirthDate,
                        MotherFullName = s.MotherFullName,
                        MotherJob = s.MotherJob,
                        MotherWorkingPlace = s.MotherWorkingPlace,
                        MSourcedb = s.MSourcedb,
                        M_FamilyTypeName = s.M_FamilyTypeName,
                        M_OldEmployeeID = s.M_OldEmployeeID,
                        M_OldID = s.M_OldID,
                        M_OldTeacherID = s.M_OldTeacherID,
                        M_ProvinceID = s.M_ProvinceID,
                        Name = s.Name,
                        NeedTeacherDuties = s.NeedTeacherDuties,
                        Note = s.Note,
                        PermanentResidentalAddress = s.PermanentResidentalAddress,
                        PoliticalGradeID = s.PoliticalGradeID,
                        PrimarilyAssignedSubjectID = s.PrimarilyAssignedSubjectID,
                        QualificationLevelID = s.QualificationLevelID,
                        QualificationTypeID = s.QualificationTypeID,
                        RegularRefresher = s.RegularRefresher,
                        ReligionID = s.ReligionID,
                        SchoolFacultyID = s.SchoolFacultyID,
                        SchoolID = s.SchoolID,
                        SpecialityCatID = s.SpecialityCatID,
                        SpouseBirthDate = s.SpouseBirthDate,
                        SpouseFullName = s.SpouseFullName,
                        TempResidentalAddress = s.TempResidentalAddress,
                        SpouseJob = s.SpouseJob,
                        SpouseWorkingPlace = s.SpouseWorkingPlace,
                        StaffPositionID = s.StaffPositionID,
                        StartingDate = s.StartingDate,
                        StateManagementGradeID = s.StateManagementGradeID,
                        SupervisingDeptID = s.SupervisingDeptID,
                        SynchronizeID = s.SynchronizeID,
                        SyndicateDate = s.SyndicateDate,
                        TeacherDuties = s.TeacherDuties,
                        Telephone = s.Telephone,
                        TrainingLevelID = s.TrainingLevelID,
                        WorkGroupTypeID = s.WorkGroupTypeID,
                        WorkTypeID = s.WorkTypeID,
                        YouthLeagueJoinedDate = s.YouthLeagueJoinedDate,
                        YouthLeagueJoinedPlace = s.YouthLeagueJoinedPlace
                    };
                    context.Employee.Add(emp);
                    context.Entry(emp).State = System.Data.EntityState.Modified;
                }
                context.SaveChanges();
                return new BoolResultResponse
                {
                    Result = true,
                    Description = "Cap nhat can bo thanh cong"
                };
            }
            catch (Exception ex)
            {
                string method = string.Format("{0}.{1}", System.Reflection.MethodBase.GetCurrentMethod().DeclaringType.FullName, System.Reflection.MethodBase.GetCurrentMethod().Name);
                string para = "null";
                LogExtensions.ErrorExt(logger, DateTime.Now, method, para, ex.Message, "null");
                return new BoolResultResponse
                {
                    Description = ex.Message,
                    Result = false,
                };
            }
            finally
            {
                SetAutoDetectChangesEnabled(context, true);
            }
        }

        #endregion


        #region Sync HCM

        public TransEmployeeResponse GetSyncEmployee(SyncAcademicYear form)
        {
            SMASEntities context = DeclareBusiness();
            TransEmployeeResponse retResult = new TransEmployeeResponse();
            retResult.Susscess = false;
            retResult.ListEmployees = new List<TransEmployeeDefault>();
            retResult.ValidationCode = -1;
            try
            {
                
                string formatDate = "dd/MM/yyyy";
                String modifileVN = form.ModifyDate.ToString(formatDate);

                var iqEmployee = context.Employee.Where(c => c.SchoolID == form.SchoolId);
                /*iqEmployee = iqEmployee.Where(c => (c.IsActive && (!c.SynchronizeID.HasValue || c.SynchronizeID == 0))
                                                || (c.SynchronizeID > 0 && c.ModifiedDate >= form.ModifyDate && !form.ListModifyDate.Contains(c.MSourcedb)));*/
                iqEmployee = iqEmployee.Where(p => (string.IsNullOrEmpty(p.MSourcedb) || !p.MSourcedb.Contains(form.SourceDB)) ||
                                          (p.MSourcedb.Contains(form.SourceDB) && p.ModifiedDate >= form.ModifyDate)
                                     );

                iqEmployee = iqEmployee.Take(form.RecordNumber);
                var lstEmployee = iqEmployee.ToList();
                if (!lstEmployee.Any())
                {
                    retResult.Message = "Khong co du lieu can bo SChoolId=" + form.SchoolId;
                    retResult.ListEmployees = new List<TransEmployeeDefault>();
                    retResult.Susscess = false;
                    return retResult;
                }

                var lstTransEmployee = lstEmployee.Select(c => new TransEmployeeDefault
                {
                    Alias = c.Alias,
                    AppliedLevel = c.AppliedLevel ?? 0,
                    BirthDate = c.BirthDate ?? new DateTime(),
                    BirthPlace = c.BirthPlace,
                    CommunistPartyJoinedDate = c.CommunistPartyJoinedDate,
                    CommunistPartyJoinedPlace = c.CommunistPartyJoinedPlace,
                    ContractTypeId = c.ContractTypeID ?? 0,
                    PermanentResidentalAddress = c.PermanentResidentalAddress,
                    TempResidentalAddress = c.TempResidentalAddress,
                    DedicatedForYoungLeague = c.DedicatedForYoungLeague ?? false,
                    Description = c.Description,
                    EducationalManagementGrade = (c.EducationalManagementGrade != null) ? c.EducationalManagementGrade.Resolution : "",
                    Email = c.Email,
                    EmployeeCode = c.EmployeeCode,
                    EmployeeId = c.EmployeeID,
                    EmployeeType = c.EmployeeType,
                    EmploymentStatus = c.EmploymentStatus ?? 0,
                    Ethnic = (c.Ethnic != null) ? c.Ethnic.EthnicName : "",
                    FamilyTypeId = c.FamilyTypeID ?? 0,
                    FatherBirthDate = c.FatherBirthDate,
                    FatherFullName = c.FatherFullName,
                    FatherJob = c.FatherJob,
                    FatherWorkingPlace = c.FatherWorkingPlace,
                    ForeignLanguageGrade = (c.ForeignLanguageGrade != null) ? c.ForeignLanguageGrade.Resolution : "",
                    FullName = c.FullName,
                    Genre = c.Genre ? 1 : 0,
                    GraduationLevel = (c.GraduationLevel != null) ? c.GraduationLevel.Resolution : "",
                    HealthStatus = c.HealthStatus,
                    HomeTown = c.HomeTown,
                    IdentityIssuedDate = c.IdentityIssuedDate,
                    IdentityIssuedPlace = c.IdentityIssuedPlace,
                    IdentityNumber = c.IdentityNumber,
                    IntoSchoolDate = c.IntoSchoolDate,
                    IsCommunistPartyMember = c.IsCommunistPartyMember ?? false,
                    IsYouthLeageMember = c.IsYouthLeageMember ?? false,
                    ItQualificationLevel = (c.ITQualificationLevel != null) ? c.ITQualificationLevel.Resolution : "",
                    JoinedDate = c.JoinedDate,
                    MariageStatus = c.MariageStatus ?? 0,
                    Mobile = c.Mobile,
                    MotherBirthDate = c.MotherBirthDate,
                    MotherFullName = c.MotherFullName,
                    MotherJob = c.MotherJob,
                    MotherWorkingPlace = c.MotherWorkingPlace,
                    Name = c.Name,
                    PoliticalGrade = (c.PoliticalGrade != null) ? c.PoliticalGrade.Resolution : "",
                    QualificationLevel = (c.QualificationLevel != null) ? c.QualificationLevel.Resolution : "",
                    QualificationTypeId = c.QualificationTypeID ?? 0,
                    RegularRefresher = c.RegularRefresher ?? false,
                    Religion = (c.Religion != null) ? c.Religion.Resolution : "",
                    SchoolFaculty = (c.SchoolFaculty != null) ? c.SchoolFaculty.FacultyName : "",
                    SchoolId = c.SchoolID ?? 0,
                    SpecialityCat = (c.SpecialityCat != null) ? c.SpecialityCat.Resolution : "",
                    SpouseBirthDate = c.SpouseBirthDate,
                    SpouseFullName = c.SpouseFullName,
                    SpouseJob = c.SpouseJob,
                    SpouseWorkingPlace = c.SpouseWorkingPlace,
                    StaffPosition = (c.StaffPosition != null) ? c.StaffPosition.Resolution : "",
                    StartingDate = c.StartingDate,
                    StateManagementGrade = (c.StateManagementGrade != null) ? c.StateManagementGrade.Resolution : "",
                    Telephone = c.Telephone,
                    TrainingLevel = (c.TrainingLevel != null) ? c.TrainingLevel.Resolution : "",
                    WorkType = (c.WorkType != null) ? c.WorkType.Resolution : "",
                    YouthLeagueJoinedDate = c.YouthLeagueJoinedDate,
                    YouthLeagueJoinedPlace = c.YouthLeagueJoinedPlace,
                    SynchronizeID = c.SynchronizeID,
                    IsActive = c.IsActive,
                    ModifiedDate = c.ModifiedDate,
                    SourceDB=c.MSourcedb
                }).ToList();

                retResult.ListEmployees = lstTransEmployee;
                retResult.Susscess = true;
                retResult.Message = "Thanh cong";
                return retResult;

            }
            catch (Exception ex)
            {

                string method = string.Format("{0}.{1}", System.Reflection.MethodBase.GetCurrentMethod().DeclaringType.FullName, System.Reflection.MethodBase.GetCurrentMethod().Name);
                string para = "null";
                LogExtensions.ErrorExt(logger, DateTime.Now, method, para, ex.Message, "null");
                retResult.Susscess = false;
                retResult.ValidationCode = WCFConstant.RESPONSE_EXCEPTION;
                retResult.Message = ex.Message;
                return retResult;
            }
            finally
            {
                SetAutoDetectChangesEnabled(context, true);
            }

        }

        public BoolResultResponse UpdateSyncEmployee(List<int> lstEmployeeId, SyncAcademicYear form)
        {
            SMASEntities context = DeclareBusiness();
            BoolResultResponse retResulst = new BoolResultResponse();
            retResulst.Result = true;
            retResulst.ValidationCode = WCFConstant.RESPONSE_SUCCESS;
            try
            {

                SetAutoDetectChangesEnabled(context, false);
                List<Employee> lstEmployeeUpdate = context.Employee.Where(e => e.SchoolID == form.SchoolId && lstEmployeeId.Any(e1 => e1 == e.EmployeeID)).ToList();
                for (int i = 0; i < lstEmployeeUpdate.Count; i++)
                {
                    Employee employeeUpdate = lstEmployeeUpdate[i];
                    employeeUpdate.SynchronizeID = 1;
                    employeeUpdate.MSourcedb = form.SourceDB;
                    context.Employee.Attach(employeeUpdate);
                    context.Entry(employeeUpdate).State = System.Data.EntityState.Modified;
                }
                EmployeeBusiness.Save();
                return retResulst;
            }
            catch (Exception ex)
            {

                string method = string.Format("{0}.{1}", System.Reflection.MethodBase.GetCurrentMethod().DeclaringType.FullName, System.Reflection.MethodBase.GetCurrentMethod().Name);
                string para = "null";
                LogExtensions.ErrorExt(logger, DateTime.Now, method, para, ex.Message, "null");
                retResulst.Result = false;
                retResulst.ValidationCode = WCFConstant.RESPONSE_EXCEPTION;
                return retResulst;
            }
            finally
            {
                SetAutoDetectChangesEnabled(context, true);
            }
        }

        #endregion
    }
}
