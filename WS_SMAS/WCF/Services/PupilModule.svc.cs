﻿using log4net;
using SMAS.Business.Business;
using SMAS.Business.BusinessObject;
using SMAS.Business.Common;
using SMAS.Business.IBusiness;
using SMAS.Models.Models;
using SMAS.Models.Models.CustomModels;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;
using WCF.Composite;
using System.Data.Objects;
using WCF.Composite.Sync;
using System.Text.RegularExpressions;
using SMAS.VTUtils.Excel.ContextExt;
using SMAS.VTUtils.Log;

namespace WCF
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the class name "PupilModule" in code, svc and config file together.
    // NOTE: In order to launch WCF Test Client for testing this service, please select PupilModule.svc or PupilModule.svc.cs at the Solution Explorer and start debugging.
    public class PupilModule : IPupilModule
    {
        #region CONSTANT
        private const string DELIMITER_SIGN = "#";
        #endregion
        #region Declare Variable
        private SMASEntities Context;
        private IMarkRecordBusiness MarkRecordBusiness;
        private IClassSubjectBusiness ClassSubjectBusiness;
        private IJudgeRecordBusiness JudgeRecordBusiness;
        private IPupilProfileBusiness PupilProfileBusiness;
        private IClassProfileBusiness ClassProfileBusiness;
        private IEmployeeBusiness EmployeeBusiness;
        private IHeadTeacherSubstitutionBusiness HeadTeacherSubstitutionBusiness;
        private ISummedUpRecordBusiness SummedUpRecordBusiness;
        private ISummedUpRecordClassBusiness SummedUpRecordClassBusiness;
        private IPupilRankingBusiness PupilRankingBusiness;
        private IPupilEmulationBusiness PupilEmulationBusiness;
        private IPeriodDeclarationBusiness PeriodDeclarationBusiness;
        private IPupilAbsenceBusiness PupilAbsenceBusiness;
        private IPupilFaultBusiness PupilFaultBusiness;
        private IPupilPraiseBusiness PupilPraiseBusiness;
        private IPupilDisciplineBusiness PupilDisciplineBusiness;
        private IPupilOfClassBusiness PupilOfClassBusiness;
        private IEducationLevelBusiness EducationLevelBusiness;
        private ISchoolProfileBusiness SchoolProfileBusiness;
        private IAcademicYearBusiness AcademicYearBusiness;
        private ISubjectCatBusiness SubjectCatBusiness;
        private IMarkTypeBusiness MarkTypeBusiness;
        private IExemptedSubjectBusiness ExemptedSubjectBusiness;
        private IForeignLanguageGradeBusiness ForeignLanguageGradeBusiness;
        private IPupilOfSchoolBusiness PupilOfSchoolBusiness;
        private IFaultCriteriaBusiness FaultCriteriaBusiness;
        private IFaultGroupBusiness FaultGroupBusiness;
        private ITeachingAssignmentBusiness TeachingAssignmentBusiness;
        private IVMarkRecordBusiness VMarkRecordBusiness;
        private IVJudgeRecordBusiness VJudgeRecordBusiness;
        private IVSummedUpRecordBusiness VSummedUpRecordBusiness;
        private IVPupilRankingBusiness VPupilRankingBusiness;
        private ICalendarBusiness CalendarBusiness;
        private IConductLevelBusiness ConductLevelBusiness;
        private ICapacityLevelBusiness CapacityLevelBusiness;
        private IEvaluationCommentsBusiness EvaluationCommentsBusiness;
        private ISummedEvaluationBusiness SummedEvaluationBusiness;
        private ISummedEndingEvaluationBusiness SummedEndingEvaluationBusiness;
        private IRewardFinalBusiness RewardFinalBusiness;
        private IRewardCommentFinalBusiness RewardCommentFinalBusiness;
        private ISchoolCodeConfigBusiness SchoolCodeConfigBusiness;
        private IEvaluationCommentsHistoryBusiness EvaluationCommentsHistoryBusiness;
        private ISummedEvaluationHistoryBusiness SummedEvaluationHistoryBusiness;
        private ITeacherNoteBookMonthBusiness TeacherNoteBookMonthBusiness;
        private ITeacherNoteBookSemesterBusiness TeacherNoteBookSemesterBusiness;
        private IReviewBookPupilBusiness ReviewBookPupilBusiness;
        private IUpdateRewardBusiness UpdateRewardBusiness;

        private IExaminationsBusiness ExaminationsBusiness;
        private IExamGroupBusiness ExamGroupBusiness;
        private IExamSubjectBusiness ExamSubjectBusiness;
        private IExamPupilBusiness ExamPupilBusiness;
        private IExamInputMarkBusiness ExamInputMarkBusiness;
        private ICommentOfPupilBusiness CommentOfPupilBusiness;
        private IRatedCommentPupilBusiness RatedCommentPupilBusiness;
        private IRatedCommentPupilHistoryBusiness RatedCommentPupilHistoryBusiness;
        private IEvaluationCriteriaBusiness EvaluationCriteriaBusiness;
        private IEvaluationRewardBusiness EvaluationRewardBusiness;
        private IWeeklyMealMenuBusiness WeeklyMealMenuBusiness;
        private IMealCategoryBusiness MealCategoryBusiness;
        private IPhysicalExaminationBusiness PhysicalExaminationBusiness;
        private IDeclareEvaluationGroupBusiness DeclareEvaluationGroupBusiness;
        private IDeclareEvaluationIndexBusiness DeclareEvaluationIndexBusiness;
        private IGrowthEvaluationBusiness GrowthEvaluationBusiness;
        private IActivityOfPupilBusiness ActivityOfPupilBusiness;
        private IGoodChildrenTicketBusiness GoodChildrenTicketBusiness;
        private IPupilRankingHistoryBusiness PupilRankingHistoryBusiness;
        private static readonly log4net.ILog logger = LogManager.GetLogger(typeof(PupilModule));


        /// <summary>
        /// string.Format "Nghi co phep: {0}"
        /// </summary>
        private const string FORMAT_ABSENCE_P_0 = "Nghỉ có phép: {0}";

        /// <summary>
        /// string.Format "Nghi khong phep: {0}"
        /// </summary>
        private const string FORMAT_ABSENCE_K_0 = "Nghỉ không phép: {0}";

        /// <summary>
        /// format "{0}({1}), "
        /// </summary>
        private const string FORMAT_FAULT_0_1 = "{0}{1} ({2}), ";

        /// <summary>
        /// Khoi tao business cung context
        /// </summary>
        private void DeclareBusiness()
        {
            SMASEntities context = new SMASEntities();
            this.Context = context;
            this.MarkRecordBusiness = new MarkRecordBusiness(logger, context);
            this.JudgeRecordBusiness = new JudgeRecordBusiness(logger, context);
            this.ClassSubjectBusiness = new ClassSubjectBusiness(logger, context);
            this.PupilProfileBusiness = new PupilProfileBusiness(logger, context);
            this.ClassProfileBusiness = new ClassProfileBusiness(logger, context);
            this.EmployeeBusiness = new EmployeeBusiness(logger, context);
            this.HeadTeacherSubstitutionBusiness = new HeadTeacherSubstitutionBusiness(logger, context);
            this.SummedUpRecordBusiness = new SummedUpRecordBusiness(logger, context);
            this.SummedUpRecordClassBusiness = new SummedUpRecordClassBusiness(logger, context);
            this.PupilRankingBusiness = new PupilRankingBusiness(logger, context);
            this.PeriodDeclarationBusiness = new PeriodDeclarationBusiness(logger, context);
            this.PupilAbsenceBusiness = new PupilAbsenceBusiness(logger, context);
            this.PupilFaultBusiness = new PupilFaultBusiness(logger, context);
            this.PupilOfClassBusiness = new PupilOfClassBusiness(logger, context);
            this.EducationLevelBusiness = new EducationLevelBusiness(logger, context);
            this.SchoolProfileBusiness = new SchoolProfileBusiness(logger, context);
            this.PupilEmulationBusiness = new PupilEmulationBusiness(logger, context);
            this.AcademicYearBusiness = new AcademicYearBusiness(logger, context);
            this.SubjectCatBusiness = new SubjectCatBusiness(logger, context);
            this.MarkTypeBusiness = new MarkTypeBusiness(logger, context);
            this.PupilPraiseBusiness = new PupilPraiseBusiness(logger, context);
            this.PupilDisciplineBusiness = new PupilDisciplineBusiness(logger, context);
            this.ExemptedSubjectBusiness = new ExemptedSubjectBusiness(logger, context);
            this.ForeignLanguageGradeBusiness = new ForeignLanguageGradeBusiness(logger, context);
            this.PupilOfSchoolBusiness = new PupilOfSchoolBusiness(logger, context);
            this.FaultCriteriaBusiness = new FaultCriteriaBusiness(logger, context);
            this.FaultGroupBusiness = new FaultGroupBusiness(logger, context);
            this.TeachingAssignmentBusiness = new TeachingAssignmentBusiness(logger, context);
            this.CalendarBusiness = new CalendarBusiness(logger, context);
            this.VJudgeRecordBusiness = new VJudgeRecordBusiness(logger, context);
            this.VSummedUpRecordBusiness = new VSummedUpRecordBusiness(logger, context);
            this.VMarkRecordBusiness = new VMarkRecordBusiness(logger, context);
            this.VPupilRankingBusiness = new VPupilRankingBusiness(logger, context);
            this.ConductLevelBusiness = new ConductLevelBusiness(logger, context);
            this.CapacityLevelBusiness = new CapacityLevelBusiness(logger, context);
            this.EvaluationCommentsBusiness = new EvaluationCommentsBusiness(logger, context);
            this.SummedEvaluationBusiness = new SummedEvaluationBusiness(logger, context);
            this.SummedEndingEvaluationBusiness = new SummedEndingEvaluationBusiness(logger, context);
            this.RewardFinalBusiness = new RewardFinalBusiness(logger, context);
            this.RewardCommentFinalBusiness = new RewardCommentFinalBusiness(logger, context);
            this.SchoolCodeConfigBusiness = new SchoolCodeConfigBusiness(logger, context);
            this.EvaluationCommentsHistoryBusiness = new EvaluationCommentsHistoryBusiness(logger, context);
            this.SummedEvaluationHistoryBusiness = new SummedEvaluationHistoryBusiness(logger, context);
            this.TeacherNoteBookMonthBusiness = new TeacherNoteBookMonthBusiness(logger, context);
            this.TeacherNoteBookSemesterBusiness = new TeacherNoteBookSemesterBusiness(logger, context);
            this.ReviewBookPupilBusiness = new ReviewBookPupilBusiness(logger, context);
            this.UpdateRewardBusiness = new UpdateRewardBusiness(logger, context);

            this.ExaminationsBusiness = new ExaminationsBusiness(logger, context);
            this.ExamGroupBusiness = new ExamGroupBusiness(logger, context);
            this.ExamSubjectBusiness = new ExamSubjectBusiness(logger, context);
            this.ExamPupilBusiness = new ExamPupilBusiness(logger, context);
            this.ExamInputMarkBusiness = new ExamInputMarkBusiness(logger, context);
            this.CommentOfPupilBusiness = new CommentOfPupilBusiness(logger, context);
            this.RatedCommentPupilBusiness = new RatedCommentPupilBusiness(logger, context);
            this.RatedCommentPupilHistoryBusiness = new RatedCommentPupilHistoryBusiness(logger, context);
            this.EvaluationCriteriaBusiness = new EvaluationCriteriaBusiness(logger, context);
            this.EvaluationRewardBusiness = new EvaluationRewardBusiness(logger, context);
            this.WeeklyMealMenuBusiness = new WeeklyMealMenuBusiness(logger, context);
            this.MealCategoryBusiness = new MealCategoryBusiness(logger, context);
            this.PhysicalExaminationBusiness = new PhysicalExaminationBusiness(logger, context);
            this.DeclareEvaluationIndexBusiness = new DeclareEvaluationIndexBusiness(logger, context);
            this.DeclareEvaluationGroupBusiness = new DeclareEvaluationGroupBusiness(logger, context);
            this.GrowthEvaluationBusiness = new GrowthEvaluationBusiness(logger, context);
            this.ActivityOfPupilBusiness = new ActivityOfPupilBusiness(logger, context);
            this.GoodChildrenTicketBusiness = new GoodChildrenTicketBusiness(logger, context);
            this.PupilRankingHistoryBusiness = new PupilRankingHistoryBusiness(logger, context);
        }

        #endregion

        #region Function mapping interface For Thread

        /// <summary>
        /// Them moi diem
        /// </summary>
        /// <param name="lstMarkRecordComposite"></param>
        /// <param name="objCollection"></param>
        /// <returns></returns>
        public bool InsertMarkRecord(List<MarkRecordComposite> lstMarkRecordComposite, CollectionComposite objCollection)
        {
            try
            {
                //Khoi tao Business
                DeclareBusiness();
                List<MarkRecord> listMarkRecordObject = new List<MarkRecord>();
                List<SummedUpRecord> lstSummedUpRecord = new List<SummedUpRecord>();
                MarkRecord objMarkRecord = new MarkRecord();
                SummedUpRecord objSummedUpRecord = new SummedUpRecord();
                for (int i = 0; i < lstMarkRecordComposite.Count; i++)
                {
                    //Diem trung binh mon
                    if (lstMarkRecordComposite[i].IsSummedUp)
                    {
                        objSummedUpRecord = new SummedUpRecord();
                        objSummedUpRecord.PupilID = lstMarkRecordComposite[i].PupilID;
                        objSummedUpRecord.ClassID = objCollection.ClassID;
                        objSummedUpRecord.AcademicYearID = objCollection.AcademicYearID;
                        objSummedUpRecord.SchoolID = objCollection.SchoolID;
                        objSummedUpRecord.SubjectID = objCollection.SubjectID;
                        objSummedUpRecord.CreatedAcademicYear = objCollection.Year;
                        objSummedUpRecord.Semester = objCollection.SemesterID;
                        objSummedUpRecord.PeriodID = objCollection.PeriodID;
                        objSummedUpRecord.SummedUpMark = lstMarkRecordComposite[i].SummedUpMark;
                        objSummedUpRecord.JudgementResult = lstMarkRecordComposite[i].JudgementResult;
                        objSummedUpRecord.IsCommenting = lstMarkRecordComposite[i].IsCommenting;
                        lstSummedUpRecord.Add(objSummedUpRecord);
                    }
                    //Diem chi tiet
                    objMarkRecord = new MarkRecord();
                    objMarkRecord.PupilID = lstMarkRecordComposite[i].PupilID;
                    objMarkRecord.ClassID = objCollection.ClassID;
                    objMarkRecord.AcademicYearID = objCollection.AcademicYearID;
                    objMarkRecord.SchoolID = objCollection.SchoolID;
                    objMarkRecord.SubjectID = objCollection.SubjectID;
                    objMarkRecord.MarkedDate = DateTime.Now.Date;
                    objMarkRecord.CreatedAcademicYear = objCollection.Year;
                    objMarkRecord.Semester = objCollection.SemesterID;
                    objMarkRecord.MarkTypeID = lstMarkRecordComposite[i].MarkTypeID;
                    objMarkRecord.Mark = lstMarkRecordComposite[i].Mark;
                    objMarkRecord.OrderNumber = lstMarkRecordComposite[i].OrderNumber;
                    objMarkRecord.Title = lstMarkRecordComposite[i].Title;
                    listMarkRecordObject.Add(objMarkRecord);
                }
                //namta- thay doi theo nghiep vu
                MarkRecordBusiness.InsertMarkRecord(objCollection.UserAccountID, listMarkRecordObject, lstSummedUpRecord, objCollection.SemesterID, objCollection.PeriodID, new List<int>(), objCollection.SchoolID, objCollection.AcademicYearID, objCollection.AppliedLevel, objCollection.ClassID, objCollection.SubjectID, null);

                MarkRecordBusiness.Save();
                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }

        /// <summary>
        /// Tu dong tien hanh tong ket diem 
        /// </summary>
        /// <param name="UserID"></param>
        /// <param name="lstPupilID"></param>
        /// <param name="Semester"></param>
        /// <param name="PeriodID"></param>
        /// <param name="SchoolID"></param>
        /// <param name="AcademicYearID"></param>
        /// <param name="ClassID"></param>
        /// <param name="SubjectID"></param>
        public string AutoSummupRecord(int UserID, List<int> lstPupilID, int Semester, int? PeriodID, int SchoolID, int AcademicYearID, int ClassID, int SubjectID)
        {
            //Khoi tao Business
            DeclareBusiness();
            //xac dinh xem mon hoc la mon nhan xet hay tinh diem

            IEnumerable<ClassSubject> classSubject = ClassSubjectBusiness.SearchByClass(ClassID, new Dictionary<string, object>() { { "SubjectID", SubjectID } }).ToList();
            if (classSubject != null && classSubject.Count() > 0)
            {
                ClassSubject cl = classSubject.FirstOrDefault();
                //Neu la mon tinh diem
                if (cl.IsCommenting != SystemParamsInFile.ISCOMMENTING_TYPE_JUDGE)
                {
                    string result = MarkRecordBusiness.AutoSummupRecord(UserID, lstPupilID, Semester, PeriodID, SchoolID, AcademicYearID,
                        ClassID, SubjectID);
                    return result;
                }
                else
                {
                    string result = JudgeRecordBusiness.AutoSummupRecord(UserID, lstPupilID, Semester, PeriodID, SchoolID, AcademicYearID,
                        ClassID, SubjectID);
                    return result;
                }
            }
            else
            {
                //return ra ma loi
                return "Subject has not in SYSTEM";
            }
        }

        ///// <summary>
        ///// Tổng kết điểm tự động
        ///// </summary>
        ///// <param name="lstPupilID"></param>
        ///// <param name="SchoolID"></param>
        ///// <param name="AcademicYearID"></param>
        ///// <param name="ClassID"></param>
        ///// <param name="Semester"></param>
        ///// <param name="PeriodID"></param>
        ///// <returns></returns>
        public string AutoMarkSummary(List<int> lstPupilID, int SchoolID, int AcademicYearID, int ClassID, int Semester, int? PeriodID)
        {
            //Khoi tao Business
            DeclareBusiness();
            // Chay ham nghiep vu
            return SummedUpRecordClassBusiness.AutoMarkSummary(lstPupilID, SchoolID, AcademicYearID, ClassID, Semester, PeriodID);
        }

        ///// <summary>
        ///// Xếp hạng tự động
        ///// </summary>
        ///// <param name="lstPupilID"></param>
        ///// <param name="SchoolID"></param>
        ///// <param name="AcademicYearID"></param>
        ///// <param name="ClassID"></param>
        ///// <param name="Semester"></param>
        ///// <param name="PeriodID"></param>
        ///// <returns></returns>
        public string AutoRankingPupil(List<int> lstPupilID, int SchoolID, int AcademicYearID, int ClassID, int Semester, int? PeriodID)
        {
            //Khoi tao Business
            DeclareBusiness();
            // Chay ham nghiep vu
            return SummedUpRecordClassBusiness.AutoRankingPupil(lstPupilID, SchoolID, AcademicYearID, ClassID, Semester, PeriodID);
        }

        ///// <summary>
        ///// Xếp loại
        ///// </summary>
        ///// <param name="lstPupilID"></param>
        ///// <param name="SchoolID"></param>
        ///// <param name="AcademicYearID"></param>
        ///// <param name="ClassID"></param>
        ///// <param name="Semester"></param>
        ///// <param name="PeriodID"></param>
        ///// <returns></returns>
        public string AutoClassifyingPupil(List<int> lstPupilID, int SchoolID, int AcademicYearID, int ClassID, int Semester)
        {
            //Khoi tao Business
            DeclareBusiness();
            // Chay ham nghiep vu
            return SummedUpRecordClassBusiness.AutoClassifyingPupil(lstPupilID, SchoolID, AcademicYearID, ClassID, Semester);
        }
        #endregion

        #region Function for SLLDT  - Namta
        /// <summary>
        /// Get Mark and info's pupil
        /// </summary>
        /// <param name="schoolID"></param>
        /// <param name="pupilCode"></param>
        /// <param name="year"></param>
        /// <param name="semester"></param>
        /// <returns></returns>
        public PupilFile_Mark GetPupilFileMark(int schoolID, string pupilCode, int year, int semester)
        {
            DeclareBusiness();
            PupilFile_Mark PupilFileMark = new PupilFile_Mark();
            pupilCode = pupilCode.Trim();
            Dictionary<string, object> dicPupil = new Dictionary<string, object>();
            dicPupil["PupilCode"] = pupilCode;
            //lay pupilid
            IQueryable<PupilProfileBO> ListPupilProfile = PupilProfileBusiness.SearchBySchool(schoolID, dicPupil).Where(u => u.PupilCode.ToUpper().Equals(pupilCode.ToUpper()));

            //lay academicyear cua truong
            Dictionary<string, object> dicAcad = new Dictionary<string, object>();
            dicAcad["Year"] = year;
            IQueryable<AcademicYear> listAcad = AcademicYearBusiness.SearchBySchool(schoolID, dicAcad);
            AcademicYear acad = new AcademicYear();

            if (listAcad != null && listAcad.Count() > 0)
            {
                acad = listAcad.FirstOrDefault();
            }

            if (ListPupilProfile != null && ListPupilProfile.Count() > 0)
            {
                //lay thong tin chung cua hoc sinh
                var Pupil = ListPupilProfile.FirstOrDefault();

                //lay thong tin hoc sinh hoc lop tuong ung
                Dictionary<string, object> dicPOC = new Dictionary<string, object>();
                dicPOC["PupilID"] = Pupil.PupilProfileID;
                dicPOC["AcademicYearID"] = acad.AcademicYearID;
                dicPOC["Year"] = year;
                dicPOC["Semester"] = semester;
                dicPOC["Check"] = "Check";
                PupilOfClass pupilOfClass = PupilOfClassBusiness.SearchBySchool(schoolID, dicPOC).Where(u => u.Status == SystemParamsInFile.PUPIL_STATUS_STUDYING || u.Status == SystemParamsInFile.PUPIL_STATUS_GRADUATED).FirstOrDefault();

                #region Thong tin chung hoc sinh
                //id hoc sinh
                PupilFileMark.PpID = Pupil.PupilProfileID;
                //ho ten
                PupilFileMark.PpFullName = Pupil.FullName;
                //ma hoc sinh
                PupilFileMark.PpCode = pupilCode;
                //ten truong
                PupilFileMark.PpSchoolName = Pupil.SchoolName;
                PupilFileMark.BirthPlace = Pupil.BirthPlace;
                PupilFileMark.SchoolYear = year;
                PupilFileMark.PpBirthday = Pupil.BirthDate.Date.ToString("dd/MM/yyyy");
                //hoc ky
                PupilFileMark.Hk = semester.ToString();
                //cap hoc
                #endregion

                if (pupilOfClass != null)
                {

                    PupilFileMark.PpClassName = pupilOfClass.ClassProfile.DisplayName;
                    PupilFileMark.Level = pupilOfClass.ClassProfile.EducationLevel.Grade.ToString();


                    //lay thong tin diem hoc luc

                    #region Lay hanh kiem
                    Dictionary<string, object> dicPupilRanking = new Dictionary<string, object>();
                    dicPupilRanking["AcademicYearID"] = acad.AcademicYearID;
                    dicPupilRanking["ClassID"] = pupilOfClass.ClassID;
                    dicPupilRanking["PupilID"] = Pupil.PupilProfileID;

                    IEnumerable<VPupilRanking> pupilRanking = VPupilRankingBusiness.SearchBySchool(schoolID, dicPupilRanking).ToList();

                    List<ConductLevel> ConductLevel = ConductLevelBusiness.All.ToList();
                    List<CapacityLevel> CapacityLevel = CapacityLevelBusiness.All.ToList();
                    //lay theo hoc ky tuong ung
                    var PupilSemester = pupilRanking.Where(o => o.Semester == semester).FirstOrDefault();
                    if (PupilSemester != null)
                    {
                        //hanh kiem
                        PupilFileMark.PpConduct = PupilSemester.ConductLevelID != null && PupilSemester.ConductLevelID != 0 ? ConductLevel.Where(cl => cl.ConductLevelID == PupilSemester.ConductLevelID).FirstOrDefault().Resolution : "";
                        //diem TBCM hk
                        PupilFileMark.TBCM = PupilSemester.AverageMark != null ? PupilSemester.AverageMark.Value.ToString("0.#") : String.Empty;
                        //xep loai hoc ky
                        PupilFileMark.XLHL = PupilSemester.CapacityLevelID != null && PupilSemester.CapacityLevelID != 0 ? CapacityLevel.Where(cl => cl.CapacityLevelID == PupilSemester.CapacityLevelID).FirstOrDefault().CapacityLevel1 : "";


                    }
                    //lay theo ky
                    int semesterAll = SystemParamsInFile.SEMESTER_OF_YEAR_ALL;
                    if (PupilFileMark.Level == SystemParamsInFile.APPLIED_LEVEL_PRIMARY.ToString())
                    {
                        semesterAll = SystemParamsInFile.SEMESTER_OF_YEAR_SECOND;
                    }
                    var PupilRankYear = pupilRanking.Where(o => o.Semester >= semesterAll).OrderByDescending(u => u.Semester).FirstOrDefault();
                    if (PupilRankYear != null)
                    {
                        //hanh kiem
                        PupilFileMark.PpConductCN = PupilRankYear.ConductLevelID != null && PupilRankYear.ConductLevelID != 0 ? ConductLevel.Where(cl => cl.ConductLevelID == PupilRankYear.ConductLevelID).FirstOrDefault().Resolution : "";
                        //diem TBCM 
                        PupilFileMark.TBCMCN = PupilRankYear.AverageMark != null ? PupilRankYear.AverageMark.Value.ToString("0.#") : String.Empty;
                        //xep loai 
                        PupilFileMark.XLHLCN = PupilRankYear.CapacityLevelID != null && PupilRankYear.ConductLevelID != 0 ? CapacityLevel.Where(cl => cl.CapacityLevelID == PupilRankYear.CapacityLevelID).FirstOrDefault().CapacityLevel1 : "";
                    }
                    #endregion



                    #region Lay thong tin diem,giao vien,mon hoc

                    //lay danh sach mon hoc cua hoc sinh
                    IEnumerable<ClassSubject> listClassSubject = ClassSubjectBusiness.SearchByClass(pupilOfClass.ClassID).OrderBy(u => u.SubjectCat.OrderInSubject).ThenBy(u => u.SubjectCat.DisplayName).ToList();

                    //Lay danh sach phan cong giang day cua lop
                    Dictionary<string, object> dicTeacher = new Dictionary<string, object>();
                    dicTeacher["ClassID"] = pupilOfClass.ClassID;
                    dicTeacher["Semester"] = semester;
                    IEnumerable<TeachingAssignmentBO> listTeachingAssignment = TeachingAssignmentBusiness.GetTeachingAssigment(schoolID, dicTeacher).ToList();

                    List<int> ListEmployeeID = listTeachingAssignment.Where(o => o.TeacherID != null).Select(u => u.TeacherID.Value).ToList();
                    //Lay danh sach giao vien cua truong
                    IEnumerable<Employee> ListEmployee = EmployeeBusiness.SearchTeacher(schoolID, new Dictionary<string, object>()).Where(u => ListEmployeeID.Contains(u.EmployeeID)).ToList();

                    //Lay danh sach diem cua hoc sinh
                    Dictionary<string, object> dicMark = new Dictionary<string, object>();
                    dicMark["ClassID"] = pupilOfClass.ClassID;
                    dicMark["Semester"] = semester;
                    dicMark["AcademicYearID"] = acad.AcademicYearID;
                    dicMark["PupilID"] = Pupil.PupilProfileID;
                    IEnumerable<VMarkRecord> ListMarkAll = VMarkRecordBusiness.SearchBySchool(schoolID, dicMark).ToList();

                    //Lay danh sach diem cua hoc sinh mon nhan xet
                    Dictionary<string, object> dicJudge = new Dictionary<string, object>();
                    dicJudge["ClassID"] = pupilOfClass.ClassID;
                    dicJudge["Semester"] = semester;
                    dicJudge["AcademicYearID"] = acad.AcademicYearID;
                    dicJudge["PupilID"] = Pupil.PupilProfileID;
                    IEnumerable<VJudgeRecord> ListJudgeAll = VJudgeRecordBusiness.SearchBySchool(schoolID, dicJudge).ToList();

                    //Lay danh sach diem cua hoc sinh mon nhan xet cap 1
                    Dictionary<string, object> dicJudgePri = new Dictionary<string, object>();
                    dicJudgePri["ClassID"] = pupilOfClass.ClassID;
                    dicJudgePri["AcademicYearID"] = acad.AcademicYearID;
                    dicJudgePri["PupilID"] = Pupil.PupilProfileID;
                    IEnumerable<VJudgeRecord> ListJudgePri = VJudgeRecordBusiness.SearchBySchool(schoolID, dicJudgePri).ToList();

                    //Lay danh sach trung binh mon cua lop
                    Dictionary<string, object> dicSum = new Dictionary<string, object>();
                    dicSum["ClassID"] = pupilOfClass.ClassID;
                    dicSum["AcademicYearID"] = acad.AcademicYearID;
                    dicSum["PupilID"] = Pupil.PupilProfileID;
                    IEnumerable<VSummedUpRecord> ListSumAll = VSummedUpRecordBusiness.SearchBySchool(schoolID, dicSum).ToList();

                    if (listClassSubject != null && listClassSubject.Count() > 0)
                    {
                        List<Subject_Mark> lstSubjectMark = new List<Subject_Mark>();
                        foreach (ClassSubject ClassSubject in listClassSubject)
                        {



                            //lay thong tin giao vien giang day
                            TeachingAssignmentBO TeachingAssignment = listTeachingAssignment.Where(u => u.SubjectID == ClassSubject.SubjectID).FirstOrDefault();
                            Subject_Mark Subject_Mark = new Subject_Mark();

                            #region TT Mon Hoc
                            //Dien thong tin mon hoc
                            SubjectCat subject = ClassSubject.SubjectCat;

                            Subject_Mark.SubjectCode = subject.Abbreviation;
                            Subject_Mark.SubjectName = subject.DisplayName;
                            Subject_Mark.SubjectType = ClassSubject.IsCommenting == SystemParamsInFile.ISCOMMENTING_TYPE_JUDGE;

                            #endregion


                            //lay diem tbm hoc ky
                            VSummedUpRecord SummedUpRecordSem = ListSumAll.Where(o => o.SubjectID == ClassSubject.SubjectID && o.Semester == semester).FirstOrDefault();
                            //lay diem tbm ca nam
                            VSummedUpRecord SummedUpRecordAll = ListSumAll.Where(o => o.SubjectID == ClassSubject.SubjectID && o.Semester >= SystemParamsInFile.SEMESTER_OF_YEAR_ALL).OrderByDescending(u => u.Semester).FirstOrDefault();
                            #region TT giao vien
                            if (TeachingAssignment != null)
                            {
                                Employee Teacher = ListEmployee.Where(u => u.EmployeeID == TeachingAssignment.TeacherID).FirstOrDefault();
                                Subject_Mark.TeacherID = TeachingAssignment.TeacherID.ToString();
                                if (Teacher != null)
                                {
                                    Subject_Mark.TeacherName = TeachingAssignment.TeacherName.ToString();
                                    Subject_Mark.TeacherPhone = Teacher.Mobile;
                                    Subject_Mark.TeacherEmail = Teacher.Email;
                                }
                            }
                            #endregion

                            //thong tin diem

                            #region Du lieu diem

                            #region reset gia tri diem
                            Subject_Mark.M1 = Subject_Mark.M2 = Subject_Mark.M3 = Subject_Mark.M4 = Subject_Mark.M5 = Subject_Mark.V1 = Subject_Mark.V2 = Subject_Mark.V3 = Subject_Mark.V4 = Subject_Mark.V5 = Subject_Mark.V6 = Subject_Mark.V7 = Subject_Mark.V8 = Subject_Mark.P1 = Subject_Mark.P2 = Subject_Mark.P3 = Subject_Mark.P4 = Subject_Mark.P5 = Subject_Mark.Kthk = Subject_Mark.Tbm = Subject_Mark.Tbm2 = Subject_Mark.TbmCN = "";
                            #endregion
                            //cap 1
                            if (PupilFileMark.Level == SystemParamsInFile.APPLIED_LEVEL_PRIMARY.ToString())
                            {
                                if (ClassSubject.IsCommenting == SystemParamsInFile.ISCOMMENTING_TYPE_MARK)
                                {
                                    #region Mon Tinh diem
                                    List<VMarkRecord> ListMarkSubject = ListMarkAll.Where(u => u.SubjectID == ClassSubject.SubjectID).ToList();
                                    foreach (VMarkRecord mrbo in ListMarkSubject)
                                    {
                                        if (semester == SystemParamsInFile.SEMESTER_OF_YEAR_FIRST)
                                        {
                                            if (mrbo.MarkedDate != null)
                                            {
                                                if (mrbo.MarkedDate.Value.Month == 9)
                                                {
                                                    Subject_Mark.M1 = Subject_Mark.M1 + mrbo.Mark.ToString() + " ";
                                                }
                                                if (mrbo.MarkedDate.Value.Month == 10)
                                                {
                                                    Subject_Mark.M2 = Subject_Mark.M2 + mrbo.Mark.ToString() + " ";
                                                }
                                                if (mrbo.MarkedDate.Value.Month == 11)
                                                {
                                                    Subject_Mark.M3 = Subject_Mark.M3 + mrbo.Mark.ToString() + " ";
                                                }
                                                if (mrbo.MarkedDate.Value.Month == 12)
                                                {
                                                    Subject_Mark.M4 = Subject_Mark.M4 + mrbo.Mark.ToString() + " ";
                                                }
                                                if (mrbo.MarkedDate.Value.Month == 1)
                                                {
                                                    Subject_Mark.M5 = Subject_Mark.M5 + mrbo.Mark.ToString() + " ";
                                                }
                                            }
                                            if (mrbo.Title == SystemParamsInFile.GK1)
                                            {
                                                Subject_Mark.P1 = mrbo.Mark.ToString();
                                            }
                                            if (mrbo.Title == SystemParamsInFile.CK1)
                                            {
                                                Subject_Mark.P2 = mrbo.Mark.ToString();
                                            }
                                            if (mrbo.Title == SystemParamsInFile.CK1)
                                            {
                                                Subject_Mark.P3 = mrbo.Mark.ToString();
                                            }
                                            //diem doc GK1
                                            if (mrbo.Title == SystemParamsInFile.DGK1)
                                            {
                                                Subject_Mark.P5 = mrbo.Mark.ToString("0.#");
                                            }
                                            //diem viet GK1
                                            if (mrbo.Title == SystemParamsInFile.VGK1)
                                            {
                                                Subject_Mark.V1 = mrbo.Mark.ToString("0.#");
                                            }

                                            //diem doc GK2
                                            if (mrbo.Title == SystemParamsInFile.DCK1)
                                            {
                                                Subject_Mark.V2 = mrbo.Mark.ToString("0.#");
                                            }
                                            //diem viet GK2
                                            if (mrbo.Title == SystemParamsInFile.VCK1)
                                            {
                                                Subject_Mark.V3 = mrbo.Mark.ToString("0.#");
                                            }


                                            //xep loai hoc luc 1
                                            if (SummedUpRecordSem != null)
                                            {
                                                if (SummedUpRecordSem.SummedUpMark >= 9)
                                                    Subject_Mark.P4 = SystemParamsInFile.XL_GIOI;

                                                if (SummedUpRecordSem.SummedUpMark >= 7 && SummedUpRecordSem.SummedUpMark < 9)
                                                    Subject_Mark.P4 = SystemParamsInFile.XL_KHA;

                                                if (SummedUpRecordSem.SummedUpMark >= 5 && SummedUpRecordSem.SummedUpMark < 7)
                                                    Subject_Mark.P4 = SystemParamsInFile.XL_TRUNGBINH;

                                                if (SummedUpRecordSem.SummedUpMark < 5)
                                                    Subject_Mark.P4 = SystemParamsInFile.XL_YEU;
                                            }
                                        }
                                        else if (semester == SystemParamsInFile.SEMESTER_OF_YEAR_SECOND)
                                        {
                                            if (mrbo.MarkedDate != null)
                                            {
                                                if (mrbo.MarkedDate.Value.Month == 2)
                                                {
                                                    Subject_Mark.M1 = Subject_Mark.M1 + mrbo.Mark.ToString() + " ";
                                                }
                                                if (mrbo.MarkedDate.Value.Month == 3)
                                                {
                                                    Subject_Mark.M2 = Subject_Mark.M2 + mrbo.Mark.ToString() + " ";
                                                }
                                                if (mrbo.MarkedDate.Value.Month == 4)
                                                {
                                                    Subject_Mark.M3 = Subject_Mark.M3 + mrbo.Mark.ToString() + " ";
                                                }
                                                if (mrbo.MarkedDate.Value.Month == 5)
                                                {
                                                    Subject_Mark.M4 = Subject_Mark.M4 + mrbo.Mark.ToString() + " ";
                                                }
                                                if (mrbo.MarkedDate.Value.Month == 6)
                                                {
                                                    Subject_Mark.M5 = Subject_Mark.M5 + mrbo.Mark.ToString() + " ";
                                                }
                                            }
                                            if (mrbo.Title == SystemParamsInFile.GK2)
                                            {
                                                Subject_Mark.P1 = mrbo.Mark.ToString();
                                            }
                                            if (mrbo.Title == SystemParamsInFile.CN)
                                            {
                                                Subject_Mark.P2 = mrbo.Mark.ToString();
                                            }
                                            if (mrbo.Title == SystemParamsInFile.CN)
                                            {
                                                Subject_Mark.P3 = mrbo.Mark.ToString();
                                            }
                                            //diem doc GK1
                                            if (mrbo.Title == SystemParamsInFile.DGK2)
                                            {
                                                Subject_Mark.P5 = mrbo.Mark.ToString("0.#");
                                            }
                                            //diem viet GK1
                                            if (mrbo.Title == SystemParamsInFile.VGK2)
                                            {
                                                Subject_Mark.V1 = mrbo.Mark.ToString("0.#");
                                            }

                                            //diem doc GK2
                                            if (mrbo.Title == SystemParamsInFile.DCN)
                                            {
                                                Subject_Mark.V2 = mrbo.Mark.ToString("0.#");
                                            }
                                            //diem viet GK2
                                            if (mrbo.Title == SystemParamsInFile.VCN)
                                            {
                                                Subject_Mark.V3 = mrbo.Mark.ToString("0.#");
                                            }
                                            //xep loai hoc luc 1
                                            if (SummedUpRecordAll != null)
                                            {
                                                if (SummedUpRecordAll.SummedUpMark >= 9)
                                                    Subject_Mark.P4 = SystemParamsInFile.XL_GIOI;

                                                if (SummedUpRecordAll.SummedUpMark >= 7 && SummedUpRecordAll.SummedUpMark < 9)
                                                    Subject_Mark.P4 = SystemParamsInFile.XL_KHA;

                                                if (SummedUpRecordAll.SummedUpMark >= 5 && SummedUpRecordAll.SummedUpMark < 7)
                                                    Subject_Mark.P4 = SystemParamsInFile.XL_TRUNGBINH;

                                                if (SummedUpRecordAll.SummedUpMark < 5)
                                                    Subject_Mark.P4 = SystemParamsInFile.XL_YEU;
                                            }
                                        }
                                    }
                                    #endregion
                                    PupilFileMark.LstSubjectMark.Add(Subject_Mark);
                                }
                                else
                                {
                                    #region Mon Nhan xet
                                    List<VJudgeRecord> ListJudgeSubject = ListJudgePri.Where(u => u.SubjectID == ClassSubject.SubjectID).ToList();
                                    foreach (VJudgeRecord mrbo in ListJudgeSubject)
                                    {
                                        if (mrbo.Semester == SystemParamsInFile.SEMESTER_OF_YEAR_FIRST)
                                        {
                                            if (mrbo.OrderNumber == 1)
                                            {
                                                Subject_Mark.M1 = mrbo.Judgement.ToString();
                                            }
                                            if (mrbo.OrderNumber == 2)
                                            {
                                                Subject_Mark.M2 = mrbo.Judgement.ToString();
                                            }
                                            if (mrbo.OrderNumber == 3)
                                            {
                                                Subject_Mark.M3 = mrbo.Judgement.ToString();
                                            }
                                            if (mrbo.OrderNumber == 4)
                                            {
                                                Subject_Mark.M4 = mrbo.Judgement.ToString();
                                            }
                                            if (mrbo.OrderNumber == 5)
                                            {
                                                Subject_Mark.M5 = mrbo.Judgement.ToString();
                                            }

                                        }
                                        else if (mrbo.Semester == SystemParamsInFile.SEMESTER_OF_YEAR_SECOND && semester == SystemParamsInFile.SEMESTER_OF_YEAR_SECOND)
                                        {
                                            if (mrbo.OrderNumber == 1)
                                            {
                                                Subject_Mark.P1 = mrbo.Judgement.ToString();
                                            }
                                            if (mrbo.OrderNumber == 2)
                                            {
                                                Subject_Mark.P2 = mrbo.Judgement.ToString();
                                            }
                                            if (mrbo.OrderNumber == 3)
                                            {
                                                Subject_Mark.P3 = mrbo.Judgement.ToString();
                                            }
                                            if (mrbo.OrderNumber == 4)
                                            {
                                                Subject_Mark.P4 = mrbo.Judgement.ToString();
                                            }
                                            if (mrbo.OrderNumber == 5)
                                            {
                                                Subject_Mark.P5 = mrbo.Judgement.ToString();
                                            }

                                            //xep loai hoc luc 1

                                        }
                                    }
                                    if (SummedUpRecordSem != null)
                                    {
                                        Subject_Mark.Kthk = SummedUpRecordSem.JudgementResult;
                                    }

                                    #endregion
                                    PupilFileMark.LstSubjectJudgeMark.Add(Subject_Mark);
                                }
                            }//cap 2,3
                            else if (PupilFileMark.Level == SystemParamsInFile.APPLIED_LEVEL_SECONDARY.ToString() || PupilFileMark.Level == SystemParamsInFile.APPLIED_LEVEL_TERTIARY.ToString())
                            {
                                //Mon Tinh diem
                                if (ClassSubject.IsCommenting == SystemParamsInFile.ISCOMMENTING_TYPE_MARK)
                                {
                                    #region Mon Tinh Diem
                                    List<VMarkRecord> ListMarkSubject = ListMarkAll.Where(u => u.SubjectID == ClassSubject.SubjectID).ToList();
                                    foreach (VMarkRecord mrbo in ListMarkSubject)
                                    {

                                        #region TT diem
                                        if (mrbo.Title.Equals("M1"))
                                        {
                                            Subject_Mark.M1 = mrbo.Mark.ToString();
                                        }
                                        else if (mrbo.Title.Equals("M2"))
                                        {
                                            Subject_Mark.M2 = mrbo.Mark.ToString();
                                        }
                                        else if (mrbo.Title.Equals("M3"))
                                        {
                                            Subject_Mark.M3 = mrbo.Mark.ToString();
                                        }
                                        else if (mrbo.Title.Equals("M4"))
                                        {
                                            Subject_Mark.M4 = mrbo.Mark.ToString();
                                        }
                                        else if (mrbo.Title.Equals("M5"))
                                        {
                                            Subject_Mark.M5 = mrbo.Mark.ToString();
                                        }
                                        else if (mrbo.Title.Equals("P1"))
                                        {
                                            Subject_Mark.P1 = mrbo.Mark.ToString("0.#");
                                        }
                                        else if (mrbo.Title.Equals("P2"))
                                        {
                                            Subject_Mark.P2 = mrbo.Mark.ToString("0.#");
                                        }
                                        else if (mrbo.Title.Equals("P3"))
                                        {
                                            Subject_Mark.P3 = mrbo.Mark.ToString("0.#");
                                        }
                                        else if (mrbo.Title.Equals("P4"))
                                        {
                                            Subject_Mark.P4 = mrbo.Mark.ToString("0.#");
                                        }
                                        else if (mrbo.Title.Equals("P5"))
                                        {
                                            Subject_Mark.P5 = mrbo.Mark.ToString("0.#");
                                        }
                                        else if (mrbo.Title.Equals("V1"))
                                        {
                                            Subject_Mark.V1 = mrbo.Mark.ToString("0.#");
                                        }
                                        else if (mrbo.Title.Equals("V2"))
                                        {
                                            Subject_Mark.V2 = mrbo.Mark.ToString("0.#");
                                        }
                                        else if (mrbo.Title.Equals("V3"))
                                        {
                                            Subject_Mark.V3 = mrbo.Mark.ToString("0.#");
                                        }
                                        else if (mrbo.Title.Equals("V4"))
                                        {
                                            Subject_Mark.V4 = mrbo.Mark.ToString("0.#");
                                        }
                                        else if (mrbo.Title.Equals("V5"))
                                        {
                                            Subject_Mark.V5 = mrbo.Mark.ToString("0.#");
                                        }
                                        else if (mrbo.Title.Equals("V6"))
                                        {
                                            Subject_Mark.V6 = mrbo.Mark.ToString("0.#");
                                        }
                                        else if (mrbo.Title.Equals("V7"))
                                        {
                                            Subject_Mark.V7 = mrbo.Mark.ToString("0.#");
                                        }
                                        else if (mrbo.Title.Equals("V8"))
                                        {
                                            Subject_Mark.V8 = mrbo.Mark.ToString("0.#");
                                        }
                                        else if (mrbo.Title.Equals("HK"))
                                        {
                                            Subject_Mark.Kthk = mrbo.Mark.ToString("0.#");
                                        }
                                        #endregion

                                    }
                                    #region TT diem TB

                                    if (SummedUpRecordSem != null)
                                    {
                                        Subject_Mark.Tbm = SummedUpRecordSem.SummedUpMark.HasValue ? SummedUpRecordSem.SummedUpMark.Value.ToString("0.#") : string.Empty;
                                        Subject_Mark.Tbm2 = ConvertToJudge(SummedUpRecordSem.SummedUpMark);
                                    }
                                    if (SummedUpRecordAll != null)
                                    {
                                        Subject_Mark.TbmCN = SummedUpRecordAll.SummedUpMark.HasValue ? SummedUpRecordAll.SummedUpMark.Value.ToString("0.#") : string.Empty;
                                    }
                                    #endregion
                                    #endregion
                                    PupilFileMark.LstSubjectMark.Add(Subject_Mark);
                                }
                                else if (ClassSubject.IsCommenting == SystemParamsInFile.ISCOMMENTING_TYPE_JUDGE)
                                {
                                    #region Mon NhanXet
                                    List<VJudgeRecord> ListJudgeSubject = ListJudgeAll.Where(u => u.SubjectID == ClassSubject.SubjectID).ToList();
                                    foreach (VJudgeRecord mrbo in ListJudgeSubject)
                                    {

                                        #region TT diem
                                        if (mrbo.Title.Equals("M1"))
                                        {
                                            Subject_Mark.M1 = mrbo.Judgement.ToString();
                                        }
                                        else if (mrbo.Title.Equals("M2"))
                                        {
                                            Subject_Mark.M2 = mrbo.Judgement.ToString();
                                        }
                                        else if (mrbo.Title.Equals("M3"))
                                        {
                                            Subject_Mark.M3 = mrbo.Judgement.ToString();
                                        }
                                        else if (mrbo.Title.Equals("M4"))
                                        {
                                            Subject_Mark.M4 = mrbo.Judgement.ToString();
                                        }
                                        else if (mrbo.Title.Equals("M5"))
                                        {
                                            Subject_Mark.M5 = mrbo.Judgement.ToString();
                                        }
                                        else if (mrbo.Title.Equals("P1"))
                                        {
                                            Subject_Mark.P1 = mrbo.Judgement.ToString();
                                        }
                                        else if (mrbo.Title.Equals("P1"))
                                        {
                                            Subject_Mark.P1 = mrbo.Judgement.ToString();
                                        }
                                        else if (mrbo.Title.Equals("P2"))
                                        {
                                            Subject_Mark.P2 = mrbo.Judgement.ToString();
                                        }
                                        else if (mrbo.Title.Equals("P3"))
                                        {
                                            Subject_Mark.P3 = mrbo.Judgement.ToString();
                                        }
                                        else if (mrbo.Title.Equals("P4"))
                                        {
                                            Subject_Mark.P4 = mrbo.Judgement.ToString();
                                        }
                                        else if (mrbo.Title.Equals("P5"))
                                        {
                                            Subject_Mark.P5 = mrbo.Judgement.ToString();
                                        }
                                        else if (mrbo.Title.Equals("V1"))
                                        {
                                            Subject_Mark.V1 = mrbo.Judgement.ToString();
                                        }
                                        else if (mrbo.Title.Equals("V2"))
                                        {
                                            Subject_Mark.V2 = mrbo.Judgement.ToString();
                                        }
                                        else if (mrbo.Title.Equals("V3"))
                                        {
                                            Subject_Mark.V3 = mrbo.Judgement.ToString();
                                        }
                                        else if (mrbo.Title.Equals("V4"))
                                        {
                                            Subject_Mark.V4 = mrbo.Judgement.ToString();
                                        }
                                        else if (mrbo.Title.Equals("V5"))
                                        {
                                            Subject_Mark.V5 = mrbo.Judgement.ToString();
                                        }
                                        else if (mrbo.Title.Equals("V6"))
                                        {
                                            Subject_Mark.V6 = mrbo.Judgement.ToString();
                                        }
                                        else if (mrbo.Title.Equals("V7"))
                                        {
                                            Subject_Mark.V7 = mrbo.Judgement.ToString();
                                        }
                                        else if (mrbo.Title.Equals("V8"))
                                        {
                                            Subject_Mark.V8 = mrbo.Judgement.ToString();
                                        }
                                        else if (mrbo.Title.Equals("HK"))
                                        {
                                            Subject_Mark.Kthk = mrbo.Judgement.ToString();
                                        }
                                        #endregion

                                    }
                                    #region TT diem TB

                                    if (SummedUpRecordSem != null)
                                    {
                                        Subject_Mark.Tbm = SummedUpRecordSem.JudgementResult != null ? SummedUpRecordSem.JudgementResult.ToString() : string.Empty;
                                        Subject_Mark.Tbm2 = SummedUpRecordSem.JudgementResult != null ? SummedUpRecordSem.JudgementResult.ToString() : string.Empty;
                                    }
                                    if (SummedUpRecordAll != null)
                                    {
                                        Subject_Mark.TbmCN = SummedUpRecordAll.JudgementResult != null ? SummedUpRecordAll.JudgementResult.ToString() : string.Empty;
                                    }
                                    #endregion
                                    #endregion
                                    PupilFileMark.LstSubjectMark.Add(Subject_Mark);
                                }
                            }
                            #endregion

                        }


                    }
                    #endregion

                    #region Danh hieu thi dua, khen thuong, ky luat
                    #region DHTD
                    Dictionary<string, object> dicEmu = new Dictionary<string, object>();
                    dicEmu["PupilID"] = Pupil.PupilProfileID;
                    dicEmu["AcademicYearID"] = acad.AcademicYearID;
                    dicEmu["Semester"] = semester;
                    IEnumerable<PupilEmulation> listPupilEmulation = PupilEmulationBusiness.SearchBySchool(schoolID, dicEmu).ToList();

                    if (listPupilEmulation != null && listPupilEmulation.Count() > 0)
                    {
                        PupilFileMark.PpEmulation = listPupilEmulation.FirstOrDefault().HonourAchivementType.Resolution;
                    }
                    else
                    {
                        PupilFileMark.PpEmulation = "Không có danh hiệu";
                    }
                    #endregion
                    #region khenthuong
                    Dictionary<string, object> dicPraise = new Dictionary<string, object>();
                    dicPraise["PupilID"] = Pupil.PupilProfileID;
                    dicPraise["AcademicYearID"] = acad.AcademicYearID;

                    IEnumerable<PupilPraise> listPupilPraise = PupilPraiseBusiness.SearchBySchool(schoolID, dicPraise).OrderByDescending(u => u.PraiseDate).ToList();
                    if (listPupilPraise != null && listPupilPraise.Count() > 0)
                    {
                        foreach (PupilPraise pr in listPupilPraise)
                        {
                            PraiseType PraiseType = pr.PraiseType;
                            var PraiseLevel = "";

                            switch (pr.PraiseLevel)
                            {
                                case 1:
                                    PraiseLevel = "Bộ giáo dục";
                                    break;
                                case 2:
                                    PraiseLevel = "Sở giáo dục";
                                    break;
                                case 3:
                                    PraiseLevel = "Phòng giáo dục";
                                    break;
                                case 4:
                                    PraiseLevel = "Cấp Trường";
                                    break;
                                case 5:
                                    PraiseLevel = "Cấp Lớp";
                                    break;
                                default:
                                    PraiseLevel = "";
                                    break;
                            }


                            PupilFileMark.ListPraise.Add(new Praise_Mark()
                            {
                                Date = pr.PraiseDate == null ? "" : pr.PraiseDate.ToString("dd/MM/yyyy"),
                                HinhThuc = PraiseType.Resolution,
                                PhamVi = PraiseLevel,
                                NoiDung = pr.Description
                            });
                        }
                    }
                    #endregion
                    #region kyluat
                    Dictionary<string, object> dicDes = new Dictionary<string, object>();
                    dicDes["PupilID"] = Pupil.PupilProfileID;
                    dicDes["AcademicYearID"] = acad.AcademicYearID;

                    IEnumerable<PupilDiscipline> listPupilDesc = PupilDisciplineBusiness.SearchBySchool(schoolID, dicDes).OrderByDescending(u => u.DisciplinedDate).ToList();
                    if (listPupilPraise != null && listPupilPraise.Count() > 0)
                    {
                        foreach (PupilDiscipline pd in listPupilDesc)
                        {
                            DisciplineType DisciplineType = pd.DisciplineType;
                            var DisciplineLevel = "";

                            switch (pd.DisciplineLevel)
                            {
                                case 1:
                                    DisciplineLevel = "Bộ giáo dục";
                                    break;
                                case 2:
                                    DisciplineLevel = "Sở giáo dục";
                                    break;
                                case 3:
                                    DisciplineLevel = "Phòng giáo dục";
                                    break;
                                case 4:
                                    DisciplineLevel = "Cấp Trường";
                                    break;
                                case 5:
                                    DisciplineLevel = "Cấp Lớp";
                                    break;
                                default:
                                    DisciplineLevel = "";
                                    break;
                            }


                            PupilFileMark.ListDiscipline.Add(new Praise_Mark()
                            {
                                Date = pd.DisciplinedDate == null ? "" : pd.DisciplinedDate.ToString("dd/MM/yyyy"),
                                HinhThuc = DisciplineType.Resolution,
                                PhamVi = DisciplineLevel,
                                NoiDung = pd.Description
                            });
                        }
                    }
                    #endregion

                    #endregion

                    #region KQHT 3 môn tốt nhất
                    //Ends
                    //Get Top the best 3 subjects, the worst 3 subjects
                    if (PupilFileMark.Level != "1" && PupilFileMark.LstSubjectMark.Count > 0)//Hien tai ho tro cho cap 2&3
                    {
                        var first = PupilFileMark.LstSubjectMark.First();
                        if (first.Tbm != "")//Neu da co diem trung binh mon
                        {
                            var lstSubjectMarks = new List<Subject_Mark>();
                            if (semester == 1)
                            {
                                lstSubjectMarks.AddRange(PupilFileMark.LstSubjectMark.Where(s => s.Tbm2 == "Giỏi" && !s.SubjectType).OrderByDescending(s => Utils.GetDecimal(s.Tbm, 0.0M)));
                                lstSubjectMarks.AddRange(PupilFileMark.LstSubjectMark.Where(s => s.Tbm == "Giỏi" && s.SubjectType));
                                lstSubjectMarks.AddRange(PupilFileMark.LstSubjectMark.Where(s => s.Tbm2 == "Khá" && !s.SubjectType).OrderByDescending(s => Utils.GetDecimal(s.Tbm, 0.0M)));
                                lstSubjectMarks.AddRange(PupilFileMark.LstSubjectMark.Where(s => s.Tbm == "Khá" && s.SubjectType));
                                lstSubjectMarks.AddRange(PupilFileMark.LstSubjectMark.Where(s => s.Tbm2 == "Trung bình" && !s.SubjectType).OrderByDescending(s => Utils.GetDecimal(s.Tbm, 0.0M)));
                                lstSubjectMarks.AddRange(PupilFileMark.LstSubjectMark.Where(s => s.Tbm == "Trung bình" && s.SubjectType));
                                lstSubjectMarks.AddRange(PupilFileMark.LstSubjectMark.Where(s => s.Tbm2 == "Yếu" && !s.SubjectType).OrderByDescending(s => Utils.GetDecimal(s.Tbm, 0.0M)));
                                lstSubjectMarks.AddRange(PupilFileMark.LstSubjectMark.Where(s => s.Tbm == "Yếu" && s.SubjectType));
                                lstSubjectMarks.AddRange(PupilFileMark.LstSubjectMark.Where(s => s.Tbm2 == "Kém" && !s.SubjectType).OrderByDescending(s => Utils.GetDecimal(s.Tbm, 0.0M)));
                                lstSubjectMarks.AddRange(PupilFileMark.LstSubjectMark.Where(s => s.Tbm == "Kém" && s.SubjectType));
                            }
                            else//Semester 2, CN
                            {
                                lstSubjectMarks = PupilFileMark.LstSubjectMark.Where(s => s.Tbm != "" && !s.SubjectType).ToList().OrderByDescending(s => Utils.GetDecimal(s.Tbm, 0.0M)).ToList();
                                //lstSubjectMarks.AddRange(PupilFileMark.LstSubjectMark.Where(s => s.SubjectType).OrderByDescending(s => s.Tbm));
                                //lstSubjectMarks.AddRange(PupilFileMark.LstSubjectMark.Where(s => !s.SubjectType && s.Tbm != "" && Utils.GetDecimal(s.Tbm, 5.0M) < 5.0M).OrderByDescending(s => s.Tbm));
                            }
                            PupilFileMark.LstTop3Subject = lstSubjectMarks.Take(3).ToList<Subject_Mark>();
                            PupilFileMark.LstBot3Subject = lstSubjectMarks.Where(u => !u.SubjectType).Skip(lstSubjectMarks.Where(u => !u.SubjectType).ToList().Count - 3).Take(3).ToList<Subject_Mark>();
                            //PupilFileMark.LstTop3Subject = PupilFileMark.LstSubjectMark.Take(3).ToList<Subject_Mark>();
                            //PupilFileMark.LstBot3Subject = PupilFileMark.LstSubjectMark.OrderBy(x => x.Tbm).Take(3).ToList<Subject_Mark>();
                        }
                        else
                        {

                            ////Nguoc lai sap xep theo trung binh cac cot diem
                            ////Ham nay dang chua order
                            var lst = PupilFileMark.LstSubjectMark.Where(u => !u.SubjectType).ToList().OrderByDescending(x => WCFUtils.SumMarkColHasValue(x) / WCFUtils.CountMarkColHasValue(x));
                            PupilFileMark.LstTop3Subject = lst.Take(3).ToList<Subject_Mark>();
                            PupilFileMark.LstBot3Subject = lst.Skip(lst.Count() - 3).Take(3).ToList<Subject_Mark>();
                        }
                    }
                    #endregion

                    #region Thông tin điểm danh
                    Dictionary<string, object> dicAbsence = new Dictionary<string, object>();
                    dicAbsence["PupilID"] = Pupil.PupilProfileID;
                    dicAbsence["AcademicYearID"] = acad.AcademicYearID;
                    dicAbsence["ClassID"] = pupilOfClass.ClassID;
                    IEnumerable<PupilAbsence> listPupilAbsence = PupilAbsenceBusiness.SearchBySchool(schoolID, dicAbsence).OrderByDescending(x => x.AbsentDate).ToList();

                    if (listPupilAbsence != null && listPupilAbsence.Count() > 0)
                    {
                        foreach (PupilAbsence PupilAbsence in listPupilAbsence)
                        {
                            PupilFileMark.ListAbsent.Add(new PupilAbsent()
                            {
                                AbsentDate = PupilAbsence.AbsentDate,
                                AcademicYearID = PupilAbsence.AcademicYearID,
                                ClassID = PupilAbsence.ClassID,
                                CreatedAcademicYear = PupilAbsence.CreatedAcademicYear,
                                EducationLevelID = PupilAbsence.EducationLevelID,
                                IsAccepted = PupilAbsence.IsAccepted,
                                IsSMS = PupilAbsence.IsSMS,
                                Last2digitNumberSchool = PupilAbsence.Last2digitNumberSchool,
                                PupilAbsenceID = PupilAbsence.PupilAbsenceID,
                                PupilCode = PupilAbsence.PupilCode,
                                PupilID = PupilAbsence.PupilID,
                                SchoolID = PupilAbsence.SchoolID,
                                Section = PupilAbsence.Section

                            });
                        }
                    }

                    #endregion

                    #region Thời khóa biểu
                    IDictionary<string, object> dicCal = new Dictionary<string, object>();
                    dicCal["ClassID"] = pupilOfClass.ClassID;
                    dicCal["AcademicYearID"] = acad.AcademicYearID;
                    dicCal["Semester"] = semester;
                    var ListCalendar = CalendarBusiness.Search(dicCal).ToList();

                    if (ListCalendar != null && ListCalendar.Count() > 0)
                    {
                        foreach (Calendar cal in ListCalendar)
                        {
                            //lay thong tin giao vien giang day
                            TeachingAssignmentBO TeachingAssignment = listTeachingAssignment.Where(u => u.SubjectID == cal.SubjectID).FirstOrDefault();
                            if (cal.Section == SystemParamsInFile.SECTION_MORNING)
                            {
                                PupilFileMark.TkbSang.Add(new TimeTable_Mark()
                                {
                                    Thu = cal.DayOfWeek.ToString(),
                                    Tiet = cal.NumberOfPeriod.ToString(),
                                    Monhoc = cal.SubjectCat.SubjectName,
                                    GiaoVien = TeachingAssignment != null ? TeachingAssignment.TeacherName : ""
                                });
                            }
                            else if (cal.Section == SystemParamsInFile.SECTION_AFTERNOON)
                            {
                                PupilFileMark.TkbChieu.Add(new TimeTable_Mark()
                                {
                                    Thu = cal.DayOfWeek.ToString(),
                                    Tiet = cal.NumberOfPeriod.ToString(),
                                    Monhoc = cal.SubjectCat.SubjectName,
                                    GiaoVien = TeachingAssignment != null ? TeachingAssignment.TeacherName : ""
                                });
                            }
                        }
                    }
                    #endregion
                }
            }
            return PupilFileMark;
        }
        #endregion

        #region Function mapping interface For SMSEdu

        /// <summary>
        /// Lấy avatar của hoc sinh theo ID
        /// </summary>
        /// <param name="PupilProfileID"></param>
        /// <returns>null neu khong tim thay</returns>
        public ByteArrayResultResponse GetPupilImage(int PupilProfileID)
        {
            ByteArrayResultResponse objResult = new ByteArrayResultResponse();
            try
            {
                //Khoi tao Business
                DeclareBusiness();
                PupilProfile objPupilProfile = PupilProfileBusiness.Find(PupilProfileID);
                if (objPupilProfile != null)
                {
                    //gan gia tri thuoc tinh dinh kem 
                    objResult.Result = objPupilProfile.Image;
                }
                else
                {
                    objResult.Result = new byte[0]; //Tra ve mang byte 0 phan tu
                }
                //gan validationCode thanh cong
                objResult.ValidationCode = WCFConstant.RESPONSE_SUCCESS;
            }
            catch
            {
                //gan validationCode Exception
                objResult.ValidationCode = WCFConstant.RESPONSE_EXCEPTION;
                //gan gia tri thuoc tinh dinh kem 
                objResult.Result = new byte[0]; //Tra ve mang byte 0 phan tu
            }
            return objResult;
        }

        /// <summary>
        /// lấy học sinh theo ID 
        /// </summary>
        /// <param name="PupilProfileID"></param>
        /// <returns></returns>
        public PupilProfileLiteResponse GetPupilProfileByID(int PupilProfileID)
        {
            PupilProfileLiteResponse objResultResponse = new PupilProfileLiteResponse();
            try
            {
                //Khoi tao Business
                DeclareBusiness();
                PupilProfile objPupilProfile = PupilProfileBusiness.Find(PupilProfileID);
                PupilProfileLite objPupilProfileLite = new PupilProfileLite();
                if (objPupilProfile != null)
                {
                    objPupilProfileLite.CurrentClassID = objPupilProfile.CurrentClassID;
                    objPupilProfileLite.FullName = objPupilProfile.FullName;
                    objPupilProfileLite.PupilProfileID = objPupilProfile.PupilProfileID;
                    objPupilProfileLite.Status = objPupilProfile.ProfileStatus;
                    objPupilProfileLite.Birthday = objPupilProfile.BirthDate;
                    objPupilProfileLite.PupilCode = objPupilProfile.PupilCode;
                    objPupilProfileLite.CurrentAcademicYearID = objPupilProfile.CurrentAcademicYearID;
                }
                //gan validationCode thanh cong
                objResultResponse.ValidationCode = WCFConstant.RESPONSE_SUCCESS;
                //gan gia tri thuoc tinh dinh kem 
                objResultResponse.ObjPupilProfileLite = objPupilProfileLite;
            }
            catch
            {
                //gan validationCode Exception
                objResultResponse.ValidationCode = WCFConstant.RESPONSE_EXCEPTION;
            }
            return objResultResponse;
        }

        /// <summary>
        /// lay thong tin hoc sinh theo dieu kien
        /// </summary>
        /// <param name="PupilProfileID"></param>
        /// <param name="Year"></param>
        /// <returns></returns>
        public PupilProfileLiteResponse GetPupilProfileByYear(int PupilProfileID, int Year)
        {
            PupilProfileLiteResponse objResultResponse = new PupilProfileLiteResponse();
            try
            {
                //Khoi tao Business
                DeclareBusiness();
                objResultResponse.ObjPupilProfileLite = (from p in PupilOfClassBusiness.All
                                                         where p.AcademicYear.Year == Year
                                                        && p.PupilID == PupilProfileID
                                                        && p.Status == GlobalConstants.PUPIL_STATUS_STUDYING
                                                         select new PupilProfileLite
                                                         {
                                                             CurrentClassID = p.ClassID,
                                                             PupilProfileID = p.PupilID,
                                                             CurrentAcademicYearID = p.AcademicYearID,
                                                             FullName = p.PupilProfile.FullName,
                                                             PupilOfClassID = p.PupilOfClassID
                                                         }).FirstOrDefault();

                //gan validationCode thanh cong
                objResultResponse.ValidationCode = WCFConstant.RESPONSE_SUCCESS;
            }
            catch
            {
                //gan validationCode Exception
                objResultResponse.ValidationCode = WCFConstant.RESPONSE_EXCEPTION;
            }
            return objResultResponse;
        }

        /// <summary>
        /// lay danh sach hoc sinh theo danh sach id va nam hoc
        /// </summary>
        /// <param name="PupilIDs"></param>
        /// <param name="Year"></param>
        /// <returns></returns>
        public List<PupilProfileLite> GetPupilsByYear(List<int> PupilIDs, int Year)
        {
            try
            {
                //Khoi tao Business
                DeclareBusiness();
                return (from p in PupilOfClassBusiness.All
                        where p.AcademicYear.Year == Year
                        && PupilIDs.Contains(p.PupilID)
                        && p.Status == GlobalConstants.PUPIL_STATUS_STUDYING
                        select new PupilProfileLite
                        {
                            CurrentClassID = p.ClassID,
                            PupilProfileID = p.PupilID,
                            CurrentAcademicYearID = p.AcademicYearID,
                            FullName = p.PupilProfile.FullName,
                            PupilOfClassID = p.PupilOfClassID
                        }).ToList();
            }
            catch
            {
                return null;
            }
        }

        public PupilOfClassResponse GetPupilOfClassByID(int AcademicYearID, int PupilProfileID)
        {
            PupilOfClassResponse pupilOfClassResponse = new PupilOfClassResponse();
            try
            {
                DeclareBusiness();
                pupilOfClassResponse.ObjPupilOfClass = (from p in PupilOfClassBusiness.All
                                                        join cp in ClassProfileBusiness.All on p.ClassID equals cp.ClassProfileID
                                                        where p.AcademicYearID == AcademicYearID
                                                        && p.PupilID == PupilProfileID
                                                        && cp.IsActive == true
                                                        && (p.Status == GlobalConstants.PUPIL_STATUS_STUDYING
                                                        && cp.AcademicYearID == AcademicYearID
                                                            || p.Status == GlobalConstants.PUPIL_STATUS_GRADUATED)
                                                        select new PupilOfClassLite
                                                        {
                                                            ClassID = p.ClassID,
                                                            ClassName = cp.DisplayName,
                                                            EducationLevelID = cp.EducationLevelID,
                                                            EducationGrade = cp.EducationLevel.Grade,
                                                            Section = cp.Section
                                                        }).FirstOrDefault();
                pupilOfClassResponse.ValidationCode = WCFConstant.RESPONSE_SUCCESS;
            }
            catch (Exception ex)
            {
                LogExtensions.GetCurrentClass = System.Reflection.MethodBase.GetCurrentMethod().DeclaringType;
                string para = string.Format("academicYearId={0}, PupilProfileID={1}", AcademicYearID, PupilProfileID);
                LogExtensions.ErrorExt(logger, DateTime.Now, para, ex.Message, "null");
                pupilOfClassResponse.ValidationCode = WCFConstant.RESPONSE_EXCEPTION;
            }
            return pupilOfClassResponse;
        }

        public PupilProfileLiteResponse GetPupilProfileDetailByID(int PupilProfileID)
        {
            PupilProfileLiteResponse objPupilProfileLite = new PupilProfileLiteResponse();
            try
            {
                DeclareBusiness();
                objPupilProfileLite.ObjPupilProfileDetail = (from p in PupilProfileBusiness.All
                                                             join cp in ClassProfileBusiness.All on p.CurrentClassID equals cp.ClassProfileID
                                                             join sp in SchoolProfileBusiness.All on p.CurrentSchoolID equals sp.SchoolProfileID
                                                             join poc in PupilOfSchoolBusiness.All on p.PupilProfileID equals poc.PupilID
                                                             join l in ForeignLanguageGradeBusiness.All on p.LanguageCertificateID equals l.ForeignLanguageGradeID
                                                             into l_join
                                                             from lr in l_join.DefaultIfEmpty()
                                                             where p.PupilProfileID == PupilProfileID
                                                             && cp.IsActive == true
                                                             select new PupilProfileDetail
                                                             {
                                                                 PupilID = p.PupilProfileID,
                                                                 EducationLevelID = cp.EducationLevelID,
                                                                 ClassName = cp.DisplayName,
                                                                 PupilCode = p.PupilCode,
                                                                 TypeInto = poc.EnrolmentType,
                                                                 ProfileStatus = p.ProfileStatus,
                                                                 ClassType = p.ClassType,
                                                                 FullName = p.FullName,
                                                                 Genre = p.Genre,
                                                                 BirthDate = p.BirthDate,
                                                                 BirthPlace = p.BirthPlace,
                                                                 HomeTown = p.HomeTown,
                                                                 DateInto = p.EnrolmentDate,
                                                                 UnderLevel = p.PreviousGraduationLevelID,
                                                                 StatusGuardian = p.IsResidentIn,
                                                                 LanguageLearning = p.ForeignLanguageTraining,
                                                                 TrainingType = sp.TrainingTypeID,
                                                                 LangCertificates = lr.Resolution,//chung chi ngoai ngu
                                                                 CentificateIT = p.ItCertificate,//chung chi tin hoc
                                                                 Studytype = p.PupilLearningType,//he hoc:vua hoc vua lam,tu hoc co huong dan..
                                                                 EthnicName = p.Ethnic.EthnicName,
                                                                 ReligionName = p.Religion.Resolution,
                                                                 PolicyTargetName = p.PolicyTarget.Resolution,
                                                                 PolicyType = p.PolicyRegimeID,
                                                                 PolicyRegimeName = p.PolicyRegime.Resolution,
                                                                 PriorityName = p.PriorityType.Resolution,
                                                                 AreaName = p.Area.AreaName,
                                                                 FamilyTypeName = p.FamilyType.Resolution,
                                                                 BloodType = p.BloodType,
                                                                 DateYPT = p.YoungPioneerJoinedDate,
                                                                 DateCYU = p.YouthLeagueJoinedDate,
                                                                 dateCP = p.CommunistPartyJoinedDate,
                                                                 DisabledTypeName = p.DisabledType.Resolution,
                                                                 ProvinceName = p.Province.ProvinceName,
                                                                 DisabledSeverity = p.DisabledSeverity,
                                                                 DistrictName = p.District.DistrictName,
                                                                 CommuneName = p.Commune.CommuneName,
                                                                 PermanentResidentalAddress = p.PermanentResidentalAddress,
                                                                 TempResidentalAddress = p.TempResidentalAddress,
                                                                 Telephone = p.Telephone,
                                                                 Mobile = p.Mobile,
                                                                 FatherFullName = p.FatherFullName,
                                                                 MotherFullName = p.MotherFullName,
                                                                 SponsorFullName = p.SponsorFullName,
                                                                 FatherBirthDate = p.FatherBirthDate,
                                                                 MotherBirthDate = p.MotherBirthDate,
                                                                 SponsorBirthDate = p.SponsorBirthDate,
                                                                 FatherJob = p.FatherJob,
                                                                 MotherJob = p.MotherJob,
                                                                 SponsorJob = p.SponsorJob,
                                                                 FatherMobile = p.FatherMobile,
                                                                 MotherMobile = p.MotherMobile,
                                                                 SponsorMobile = p.SponsorMobile,
                                                                 IsFatherSMS = p.IsFatherSMS,
                                                                 IsMotherSMS = p.IsMotherSMS,
                                                                 IsSponsorSMS = p.IsSponsorSMS,
                                                                 SchoolName = sp.SchoolName,
                                                                 SchoolID = sp.SchoolProfileID
                                                             }).FirstOrDefault();
                objPupilProfileLite.ValidationCode = WCFConstant.RESPONSE_SUCCESS;
            }
            catch (Exception ex)
            {
                LogExtensions.GetCurrentClass = System.Reflection.MethodBase.GetCurrentMethod().DeclaringType;
                string para = string.Format("PupilProfileID={0}", PupilProfileID);
                LogExtensions.ErrorExt(logger, DateTime.Now, para, ex.Message, "null");
                objPupilProfileLite.ValidationCode = WCFConstant.RESPONSE_EXCEPTION;
            }

            return objPupilProfileLite;
        }

        /// <summary>
        /// Kiem tra thong tin cua hoc sinh phuc vu viec dang ky dich vu qua SMS
        /// </summary>
        /// <param name="PupilProfileID"></param>
        /// <returns></returns>
        public PupilProfile4RegisterResponse GetPupilProfile4RegisterByID(List<int> listPupilProfileID)
        {
            PupilProfile4RegisterResponse objPupilProfile4RegisterLite = new PupilProfile4RegisterResponse();
            try
            {
                DeclareBusiness();
                objPupilProfile4RegisterLite.ObjPupilProfile4RegisterLite = (from p in PupilProfileBusiness.All
                                                                             join sp in SchoolProfileBusiness.All on p.CurrentSchoolID equals sp.SchoolProfileID
                                                                             join ac in AcademicYearBusiness.All on p.CurrentAcademicYearID equals ac.AcademicYearID
                                                                             where listPupilProfileID.Contains(p.PupilProfileID) && p.IsActive == true
                                                                             && p.ProfileStatus == SystemParamsInFile.PUPIL_STATUS_STUDYING
                                                                             && ac.IsActive == true
                                                                             select new PupilProfile4RegisterLite
                                                                             {
                                                                                 PupilProfileID = p.PupilProfileID,
                                                                                 PupilCode = p.PupilCode,
                                                                                 FullName = p.FullName,
                                                                                 SchoolID = p.CurrentSchoolID,
                                                                                 SchoolCode = sp.SchoolCode,
                                                                                 SchoolName = sp.SchoolName,
                                                                                 ProvinceID = sp.Province.ProvinceID,
                                                                                 AcademicYearID = ac.AcademicYearID,
                                                                                 Year = ac.Year,
                                                                                 FatherPhone = p.FatherMobile,
                                                                                 MotherPhone = p.MotherMobile,
                                                                                 SponsorPhone = p.SponsorMobile,
                                                                                 FatherName = p.FatherFullName,
                                                                                 MotherName = p.MotherFullName,
                                                                                 SponsorName = p.SponsorFullName,
                                                                                 Status = p.ProfileStatus,
                                                                                 SMSParentActiveType = sp.SMSParentActiveType,
                                                                                 //SMSParentRegisterType = sp.SMSParentRegisterType.HasValue ? sp.SMSParentRegisterType.Value : 0
                                                                             }).ToList();
                objPupilProfile4RegisterLite.ValidationCode = WCFConstant.RESPONSE_SUCCESS;
            }
            catch (Exception ex)
            {
                LogExtensions.GetCurrentClass = System.Reflection.MethodBase.GetCurrentMethod().DeclaringType;
                string para = "null";
                LogExtensions.ErrorExt(logger, DateTime.Now, para, ex.Message, "null");
                objPupilProfile4RegisterLite.ValidationCode = WCFConstant.RESPONSE_EXCEPTION;
            }

            return objPupilProfile4RegisterLite;
        }

        #region lấy học sinh theo PupilCode
        /// <summary>
        /// lấy học sinh theo PupilCode
        /// </summary>
        /// <author date="2014/02/18">HaiVT</author>
        /// <param name="schoolCode">ma truong</param>
        /// <param name="pupilCode">ma hoc sinh</param>
        /// <returns></returns>
        public PupilProfileLiteResponse GetPupilProfileByPupilCode(int schoolID, string pupilCode)
        {
            PupilProfileLiteResponse objResultResponse = new PupilProfileLiteResponse();
            try
            {
                //Khoi tao Business
                DeclareBusiness();
                IDictionary<string, object> dic = new Dictionary<string, object>();
                dic["CurrentSchoolID"] = schoolID;
                dic["ProfileStatus"] = GlobalConstants.PUPIL_STATUS_STUDYING;
                PupilProfileLite objPupilProfileLite = new PupilProfileLite();
                if (!string.IsNullOrEmpty(pupilCode))
                {
                    pupilCode = pupilCode.ToUpper();
                    PupilProfile pupilProfile = (from pp in PupilProfileBusiness.All
                                                 where pp.CurrentSchoolID == schoolID
                                                 && pp.PupilCode.ToUpper().Equals(pupilCode)
                                                 && pp.PupilOfClasses.Where(p => p.AcademicYearID == pp.CurrentAcademicYearID && p.Status == GlobalConstants.PUPIL_STATUS_STUDYING).Count() > 0
                                                 select pp).FirstOrDefault();
                    PupilProfileBO objPupilProfile = PupilProfileBusiness.Search(dic).Where(p => p.PupilCode.ToUpper().Equals(pupilCode)).FirstOrDefault();
                    if (pupilProfile != null)
                    {
                        objPupilProfileLite.CurrentClassID = pupilProfile.CurrentClassID;
                        objPupilProfileLite.FullName = pupilProfile.FullName;
                        objPupilProfileLite.PupilProfileID = pupilProfile.PupilProfileID;
                        objPupilProfileLite.Status = pupilProfile.ProfileStatus;
                        objPupilProfileLite.CurrentSchoolID = pupilProfile.CurrentSchoolID;
                        objPupilProfileLite.Birthday = pupilProfile.BirthDate;
                    }
                }

                //gan validationCode thanh cong
                objResultResponse.ValidationCode = WCFConstant.RESPONSE_SUCCESS;
                //gan gia tri thuoc tinh dinh kem 
                objResultResponse.ObjPupilProfileLite = objPupilProfileLite;
            }
            catch (Exception ex)
            {
                LogExtensions.GetCurrentClass = System.Reflection.MethodBase.GetCurrentMethod().DeclaringType;
                string para = string.Format("schoolID={0}, pupilCode={1}", schoolID, pupilCode);
                LogExtensions.ErrorExt(logger, DateTime.Now, para, ex.Message, "null");
                //gan validationCode Exception
                objResultResponse.ValidationCode = WCFConstant.RESPONSE_EXCEPTION;
            }
            return objResultResponse;
        }
        #endregion

        #region Lấy Thông tin HS - BCCS - Tunning AnhVD9
        public string GetPupilInfoByPupilCode(string schoolCode, string pupilCode)
        {
            string INFOR = string.Empty;
            try
            {
                //Khoi tao Business
                DeclareBusiness();
                SchoolProfile sp = SchoolProfileBusiness.All.FirstOrDefault(p => schoolCode.ToUpper().Equals(p.SchoolCode.ToUpper()) && p.IsActive == true);
                if (sp != null)
                {
                    PupilProfile pupil = (from pp in PupilProfileBusiness.All
                                          where pp.CurrentSchoolID == sp.SchoolProfileID
                                          && pp.PupilCode.ToUpper().Equals(pupilCode.ToUpper())
                                          && pp.PupilOfClasses.Where(p => p.AcademicYearID == pp.CurrentAcademicYearID && p.Status == GlobalConstants.PUPIL_STATUS_STUDYING).Count() > 0
                                          select pp).FirstOrDefault();
                    if (pupil != null && pupil.PupilProfileID > 0 && pupil.CurrentClassID > 0)
                    {
                        ClassProfile cp = ClassProfileBusiness.Find(pupil.CurrentClassID);
                        if (cp != null)
                        {
                            string DATE_FORMAT = "dd/MM/yyyy";
                            StringBuilder sb = new StringBuilder();
                            sb.Append(sp.SchoolName)
                                .Append(DELIMITER_SIGN)
                                .Append(sp.ProvinceID)
                                .Append(DELIMITER_SIGN)
                                .Append(sp.ProvinceID.HasValue ? sp.Province.ProvinceName : string.Empty)
                                .Append(DELIMITER_SIGN)
                                .Append(sp.DistrictID)
                                .Append(DELIMITER_SIGN)
                                .Append(sp.DistrictID.HasValue ? sp.District.DistrictName : string.Empty)
                                .Append(DELIMITER_SIGN)
                                .Append(pupil.FullName)
                                .Append(DELIMITER_SIGN)
                                .Append(pupil.BirthDate.ToString(DATE_FORMAT))
                                .Append(DELIMITER_SIGN)
                                .Append(cp.DisplayName);
                            INFOR = sb.ToString();
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                LogExtensions.GetCurrentClass = System.Reflection.MethodBase.GetCurrentMethod().DeclaringType;
                string para = string.Format("schoolCode={0}, pupilCode={1}", schoolCode, pupilCode);
                LogExtensions.ErrorExt(logger, DateTime.Now, para, ex.Message, "null");
                INFOR = "Error";
            }
            return INFOR;
        }
        #endregion


        /// <summary>
        /// Lấy danh sách học sinh theo dk tìm kiếm nằm trong 
        /// ListPupilProfileID + SchoolID + academicYearID
        /// </summary>
        /// <param name="objPupilProfile"></param>
        /// <returns></returns>
        public ListPupilProfileResponse GetListPupilByCondition(PupilProfileRequest objPupilProfile)
        {
            ListPupilProfileResponse objResultResponse = new ListPupilProfileResponse();
            try
            {
                //Khoi tao Business
                DeclareBusiness();
                IDictionary<string, object> dic = new Dictionary<string, object>();
                dic["AcademicYearID"] = objPupilProfile.AcademicYearID;
                dic["SchoolID"] = objPupilProfile.SchoolID;
                dic["ProfileStatus"] = GlobalConstants.PUPIL_STATUS_STUDYING;
                //Lay len danh sach hoc sinh trong truong
                //Chiendd: 23/09/2015, tunning ham lay thong tin hoc sinh
                List<PupilOfClassBO> lstPupilProfile = (from pp in PupilProfileBusiness.SearchBySchool(objPupilProfile.SchoolID, dic)
                                                        join poc in PupilOfClassBusiness.All on pp.PupilProfileID equals poc.PupilID
                                                        join cp in ClassProfileBusiness.AllNoTracking on poc.ClassID equals cp.ClassProfileID
                                                        where poc.SchoolID == objPupilProfile.SchoolID && poc.AcademicYearID == objPupilProfile.AcademicYearID
                                                         && poc.Status == GlobalConstants.PUPIL_STATUS_STUDYING
                                                         && cp.AcademicYearID == objPupilProfile.AcademicYearID
                                                         //Loc danh sach theo ID truyen vao
                                                         && objPupilProfile.lstPupilProfileID.Contains(pp.PupilProfileID)
                                                         && cp.IsActive == true
                                                        select new PupilOfClassBO
                                                        {
                                                            AcademicYearID = poc.AcademicYearID,
                                                            ClassID = poc.ClassID,
                                                            AssignedDate = poc.AssignedDate,
                                                            ClassName = cp.DisplayName,
                                                            EducationLevelID = cp.EducationLevelID,
                                                            PupilFullName = pp.FullName,
                                                            PupilCode = pp.PupilCode,
                                                            PupilOfClassID = poc.PupilOfClassID,
                                                            Status = poc.Status,
                                                            //Thay cho HeadTeacherID
                                                            SubjectID = (cp.HeadTeacherID > 0) ? cp.HeadTeacherID.Value : 0,
                                                            PupilID = poc.PupilID
                                                        }).ToList();

                //Lay len danh sach lop hoc trong truong (Dung de join va lay Headteacher)
                List<ClassProfile> lstClassProfile = ClassProfileBusiness.SearchBySchool(objPupilProfile.SchoolID, dic).ToList();
                //Lay danh sach giao vien trong nam hoc
                List<Employee> lstEmployee = EmployeeBusiness.SearchTeacher(objPupilProfile.SchoolID, dic).ToList();
                //Lay len danh sach giao vien lam thay chu nhiem
                List<HeadTeacherSubstitution> lstHeadTeacherSubstitution = HeadTeacherSubstitutionBusiness.Search(dic).ToList();
                //Loc danh sach giao vien lam thay chu nhiem de lay len ID ,FullName, va ClassID(dung de left join)
                DateTime dateTimeFirst = DateTime.Now.Date;
                DateTime dateTimeLast = dateTimeFirst.AddHours(23).AddMinutes(59).AddSeconds(59);
                var lstHeadTeacherSubstitutionFilter = (from h in lstHeadTeacherSubstitution
                                                        join e in lstEmployee on h.HeadTeacherSubstitutionID equals e.EmployeeID
                                                        where h.AssignedDate <= dateTimeLast
                                                        && h.EndDate >= dateTimeFirst
                                                        select new
                                                        {
                                                            EmployeeID = e.EmployeeID,
                                                            FullName = e.FullName,
                                                            ClassID = h.ClassID
                                                        }).ToList();

                //Loc hoc sinh de truyen vao object tra ve
                var lstPupilProfileResponseFilter = (from p in lstPupilProfile
                                                         //Left join de lay len Headteacher Subtituation (lam thay chu nhiem)
                                                     join h in lstHeadTeacherSubstitutionFilter on p.ClassID equals h.ClassID into j1
                                                     from j2 in j1.DefaultIfEmpty()
                                                         //left Join de thuc hien lay len Headteacher (Giao vien chu nhiem)
                                                     join e in lstEmployee on p.SubjectID equals e.EmployeeID into ej
                                                     from ej2 in ej.DefaultIfEmpty()
                                                     select new PupilProfileDefault
                                                     {
                                                         PupilProfileID = p.PupilID,
                                                         PupilCode = p.PupilCode,
                                                         FullName = p.PupilFullName,
                                                         HeadTeacher = (ej2 == null ? 0 : ej2.EmployeeID),
                                                         HeadTeacherSubstitution = (j2 == null ? 0 : j2.EmployeeID),
                                                         ClassID = p.ClassID,
                                                         ClassName = p.ClassName,
                                                         Status = 1,
                                                         PupilOfClassID = p.PupilOfClassID,
                                                         EducationLevelID = p.EducationLevelID
                                                     });
                //gan validationCode thanh cong
                objResultResponse.ValidationCode = WCFConstant.RESPONSE_SUCCESS;
                //gan gia tri thuoc tinh dinh kem 
                objResultResponse.LstPupilProfileDefault = lstPupilProfileResponseFilter.ToList();
            }

            catch (Exception ex)
            {
                LogExtensions.GetCurrentClass = System.Reflection.MethodBase.GetCurrentMethod().DeclaringType;
                string para = "Null";
                LogExtensions.ErrorExt(logger, DateTime.Now, para, ex.Message, "null");
                //gan validationCode Exception
                objResultResponse.ValidationCode = WCFConstant.RESPONSE_EXCEPTION;
            }
            return objResultResponse;
        }

        /// <summary>
        /// lấy ds điểm đợt của học sinh trong lớp.Content format:
        /// Toán:<TBM>
        /// Thể dục:<HLM>
        /// Trung bình các môn:<8.0>
        /// Học lực:<Khá>
        /// Hạnh kiểm:<Tốt>
        /// Xếp hạng:<1>
        /// </summary>
        /// <param name="SchoolID">ID trường</param>
        /// <param name="ClassID">ID lớp học</param>
        /// <param name="Year">Năm học</param>
        /// <param name="PeriodID">ID đợt</param>
        /// <returns></returns>
        public ListMarkOfPeriodResponse GetListMarkRecordTime(int SchoolID, int ClassID, int AcademicYearID, int PeriodID)
        {
            ListMarkOfPeriodResponse objResultResponse = new ListMarkOfPeriodResponse();
            try
            {
                if (AcademicYearID == 0)
                {
                    //gan validationCode thanh cong
                    objResultResponse.ValidationCode = WCFConstant.RESPONSE_ACADEMICYEARID_NOT_FOUND;
                    objResultResponse.LstMarkOfPeriodDefault = new List<MarkOfPeriodDefault>();

                }
                else if (PeriodID == 0)
                {
                    //gan validationCode thanh cong
                    objResultResponse.ValidationCode = WCFConstant.RESPONSE_PERIOD_NOT_FOUND;
                    objResultResponse.LstMarkOfPeriodDefault = new List<MarkOfPeriodDefault>();
                }
                else
                {
                    //Khoi tao Business
                    DeclareBusiness();
                    //Khai bao bien
                    int Semester = 0;//Hoc ky
                    //Lay thong tin ten mon hoc cua lop
                    IQueryable<ClassSubject> lstSubjectClass = ClassSubjectBusiness.SearchByClass(ClassID);
                    List<SubjectCat> lstSubject = lstSubjectClass.Select(a => a.SubjectCat).Distinct().ToList();
                    //lay thong tin dot nam trong hoc ky nao va nam hoc nao
                    PeriodDeclaration objPeriodDeclaration = PeriodDeclarationBusiness.Find(PeriodID);
                    Semester = objPeriodDeclaration.Semester.Value;
                    AcademicYearID = objPeriodDeclaration.AcademicYearID;
                    //Lay len danh sach hoc sinh cua lop
                    List<PupilOfClassBO> IQPupilOfClass = PupilOfClassBusiness.GetPupilInClass(ClassID).ToList();
                    //lay diem chi tiet trung binh cua cac mon
                    List<SummedUpRecordBO> IQSummedUpRecord = (from s in SummedUpRecordBusiness.GetSummedUpRecord(AcademicYearID, SchoolID, Semester, ClassID, PeriodID)
                                                               join sc in SubjectCatBusiness.All on s.SubjectID equals sc.SubjectCatID
                                                               orderby sc.OrderInSubject
                                                               select s).ToList();
                    //lay diem trung binh cua tat ca cac mon theo dot + lay hoc luc, hanh kiem, xep hang theo dot
                    List<PupilOfClassRanking> IQPupilRanking = PupilRankingBusiness.GetPupilRankingOfClass(AcademicYearID, SchoolID, Semester, ClassID, PeriodID).ToList();
                    //Fill vao Content
                    //Loc nhung hoc sinh co diem  mon nhan xet phat sinh
                    var lstMarkJudgeGroup = (from m in IQSummedUpRecord
                                             where m.JudgementResult != null
                                             orderby m.OrderInClass
                                             group m by m.PupilID into g
                                             select new
                                             {
                                                 PupilProfileID = g.Key,
                                                 JudgeContent = (g.Select(x => (x.SubjectName + ": " + x.JudgementResult.ToString() + " "))).Aggregate((a, b) => (a + "\n" + b))
                                             }).ToList();
                    var lstMarkRecordPeriodGroup = (from m in IQSummedUpRecord
                                                    where m.SummedUpMark != null
                                                    orderby m.OrderInClass
                                                    group m by m.PupilID into g
                                                    select new
                                                    {
                                                        PupilProfileID = g.Key,
                                                        MarkContent = (g.Select(x => (x.SubjectName + ": " + (x.SummedUpMark.HasValue ? x.SummedUpMark.Value.ToString("0.0") : x.SummedUpMark.ToString()) + " "))).Aggregate((a, b) => (a + "\n" + b))
                                                    }).ToList();
                    //Fill Diem thanh phan 
                    List<MarkOfPeriodDefault> IQMarkOfPeriodResponse = (from p in IQPupilOfClass
                                                                        join j in lstMarkJudgeGroup on p.PupilID equals j.PupilProfileID into j1
                                                                        from g1 in j1.DefaultIfEmpty()
                                                                        join m in lstMarkRecordPeriodGroup on p.PupilID equals m.PupilProfileID into m1
                                                                        from g2 in m1.DefaultIfEmpty()
                                                                        select new MarkOfPeriodDefault
                                                                        {
                                                                            PupilProfileID = p.PupilID,
                                                                            Content = (g2 == null ? "" : g2.MarkContent) + (g2 != null && g2.MarkContent != "" ? "\n" : "") + (g1 == null ? "" : g1.JudgeContent)
                                                                        }).Distinct().ToList();
                    //Fill hoc luc, hanh kiem, xep hang
                    List<MarkOfPeriodDefault> IQFilterMarkOfPeriodResponse = (from mr in IQMarkOfPeriodResponse
                                                                              join pr in IQPupilRanking on mr.PupilProfileID equals pr.PupilID into g1
                                                                              from g2 in g1.DefaultIfEmpty()
                                                                              where mr.Content != ""
                                                                              select new MarkOfPeriodDefault
                                                                              {
                                                                                  PupilProfileID = mr.PupilProfileID,
                                                                                  Content = mr.Content +
                                                                                  //neu khong ton tai pupilranking
                                                                                  (g2 == null ?
                                                                                  string.Empty
                                                                                  :
                                                                                  //Neu ton tai pupilranking                                                                                  
                                                                                  (g2.AverageMark == null ? "" : "\nTrung bình các môn: " + (g2.AverageMark.HasValue ? g2.AverageMark.Value.ToString("0.0") : string.Empty)) +
                                                                                  (g2.CapacityLevel == null ? "" : "\nHọc lực:" + g2.CapacityLevel) +
                                                                                  (g2.ConductLevel == null ? "" : "\nHạnh kiểm: " + g2.ConductLevel) +
                                                                                  (g2.Rank == null ? "" : "\nXếp hạng: " + g2.Rank.ToString()))
                                                                              }).ToList();
                    //gan validationCode thanh cong
                    objResultResponse.ValidationCode = WCFConstant.RESPONSE_SUCCESS;
                    //gan gia tri thuoc tinh dinh kem 
                    objResultResponse.LstMarkOfPeriodDefault = IQFilterMarkOfPeriodResponse;
                }
            }
            catch (Exception ex)
            {
                LogExtensions.GetCurrentClass = System.Reflection.MethodBase.GetCurrentMethod().DeclaringType;
                string para = string.Format("SchoolID={0}, ClassID={1}, AcademicYearID={2},PeriodID", SchoolID, ClassID, AcademicYearID, PeriodID);
                LogExtensions.ErrorExt(logger, DateTime.Now, para, ex.Message, "null");
                //gan validationCode Exception
                objResultResponse.ValidationCode = WCFConstant.RESPONSE_EXCEPTION;
            }
            return objResultResponse;
        }

        /// <summary>
        /// điểm của học sinh trong lớp.Content format:
        /// Cấp 1:Toan:7(học lực) Thể dục:Đ,CĐ
        /// Hạnh kiểm:Đ
        /// Xếp loại giáo dục:Khá
        /// Nghỉ có phép:1
        /// Nghỉ không phép:1
        /// Cấp 2,3:Toán:<TBM> Thể dục<HLM>
        /// Trung bình các môn:<8,0>
        /// Học lực:<Khá>
        /// Hạnh kiểm:<Tốt>
        /// Xếp hạng:<1>
        /// Danh hiệu thi đua:<xuất sắc>>
        /// </summary>
        /// <param name="SchoolID">ID trường</param>
        /// <param name="ClassID">ID lớp học</param>
        /// <param name="Year">Năm học</param>
        /// <param name="Semester">học kỳ</param>
        /// <param name="EducationLevel">Cấp học (TH, THCS, THPT)</param>
        /// <returns></returns>
        public ListMarkOfSemesterResponse GetListMarkRecordSemester(int SchoolID, int ClassID, int AcademicYearID, int Semester)
        {
            ListMarkOfSemesterResponse objResultResponse = new ListMarkOfSemesterResponse();
            try
            {
                //Validate semester
                if (Semester != GlobalConstants.SEMESTER_OF_YEAR_FIRST && Semester != GlobalConstants.SEMESTER_OF_YEAR_SECOND
                    && Semester != GlobalConstants.SEMESTER_OF_YEAR_ALL)
                {

                    //gan validationCode thanh cong
                    objResultResponse.ValidationCode = WCFConstant.RESPONSE_SEMESTER_NOT_FOUND;
                    objResultResponse.LstMarkOfSemesterDefault = new List<MarkOfSemesterDefault>();
                }
                else
                {
                    //Khoi tao Business
                    DeclareBusiness();
                    AcademicYear objAcademicYear = (from a in AcademicYearBusiness.All
                                                    where a.SchoolID == SchoolID
                                                    && a.AcademicYearID == AcademicYearID
                                                    select a).FirstOrDefault();
                    DateTime FromDateHKI = DateTime.Now;
                    DateTime ToDateHKI = DateTime.Now;
                    DateTime FromDateHKII = DateTime.Now;
                    DateTime ToDateHKII = DateTime.Now;
                    if (objAcademicYear != null)
                    {
                        FromDateHKI = objAcademicYear.FirstSemesterStartDate.Value.Date;
                        ToDateHKI = objAcademicYear.FirstSemesterEndDate.Value.Date.AddHours(23).AddMinutes(59).AddSeconds(59);
                        FromDateHKII = objAcademicYear.SecondSemesterStartDate.Value.Date;
                        ToDateHKII = objAcademicYear.SecondSemesterEndDate.Value.Date.AddHours(23).AddMinutes(59).AddSeconds(59);
                    }
                    //Cap hoc
                    int grade = ClassProfileBusiness.Find(ClassID).EducationLevel.Grade;
                    List<ClassSubjectBO> listClassSubject = ClassSubjectBusiness.SearchByClass(ClassID, new Dictionary<string, object>() { { "Semester", Semester } }).Select(o => new ClassSubjectBO
                    {
                        SubjectID = o.SubjectID,
                        DisplayName = o.SubjectCat.DisplayName,
                        IsCommenting = o.IsCommenting,
                        OrderInSubject = o.SubjectCat.OrderInSubject
                    }).OrderBy(o => o.IsCommenting).ThenBy(o => o.OrderInSubject).ThenBy(o => o.DisplayName).ToList();
                    //lay diem danh theo trong 1 thang cua hoc sinh
                    //chiendd1: bo sung parttition
                    int partitionId = SchoolID % 100;
                    IQueryable<PupilAbsence> lstPupilAbsent = (from p in PupilAbsenceBusiness.All
                                                               where p.ClassID == ClassID
                                                               && p.SchoolID == SchoolID
                                                               && p.AcademicYearID == AcademicYearID
                                                               && p.Last2digitNumberSchool == partitionId
                                                               && ((grade > 1 && ((Semester == 1 && p.AbsentDate >= FromDateHKI && p.AbsentDate <= ToDateHKI)
                                                                                 || (Semester == 2 && p.AbsentDate >= FromDateHKII && p.AbsentDate <= ToDateHKII)
                                                                                 || (Semester == 3 && ((p.AbsentDate >= FromDateHKI && p.AbsentDate <= ToDateHKI)
                                                                                                   || (p.AbsentDate >= FromDateHKII && p.AbsentDate <= ToDateHKII)))))
                                                               || (grade == 1 && ((Semester == 1 && p.AbsentDate >= FromDateHKI && p.AbsentDate <= ToDateHKI)
                                                                              || (Semester == 2 && ((p.AbsentDate >= FromDateHKI && p.AbsentDate <= ToDateHKI)
                                                                                                || (p.AbsentDate >= FromDateHKII && p.AbsentDate <= ToDateHKII))))))
                                                               select p);

                    //lay diem chi tiet trung binh cua cac mon
                    List<SummedUpRecordBO> listSummedUpRecord = SummedUpRecordBusiness.GetSummedUpRecord(AcademicYearID, SchoolID, Semester, ClassID, null).ToList();
                    //lay diem trung binh cua tat ca cac mon theo dot + lay hoc luc, hanh kiem, xep hang theo dot
                    List<PupilOfClassRanking> listPupilRanking = PupilRankingBusiness.GetPupilRankingOfClass(AcademicYearID, SchoolID, Semester, ClassID, null).ToList();
                    //lay danh hieu thi dua
                    List<PupilEmulationBO> listPupilEmulation = PupilEmulationBusiness.SearchBySchool(SchoolID, new Dictionary<string, object>() {
                { "AcademicYearID", AcademicYearID } ,
                { "ClassID", ClassID },
                { "Semester", Semester }
                }).Select(o => new PupilEmulationBO
                {
                    PupilID = o.PupilID,
                    HonourAchivementTypeResolution = o.HonourAchivementType.Resolution
                }).ToList();
                    List<MarkOfSemesterDefault> listMarkOfSemesterResponse = new List<MarkOfSemesterDefault>();
                    // Lay thong tin cho tung hoc sinh
                    // Thong tin diem theo hoc sinh
                    List<SummedUpRecordBO> listPupilSum = new List<SummedUpRecordBO>();
                    // Thong tin khen thuong
                    PupilEmulationBO pupilEmulationBO = new PupilEmulationBO();
                    //Object mapping vao list
                    MarkOfSemesterDefault objMarkOfSemester = new MarkOfSemesterDefault();
                    //object mapping thong tin diem trung binh mon (dung de tao content)
                    SummedUpRecordBO objSummedUpRecordBO = new SummedUpRecordBO();
                    int countAbsentP = 0;
                    int countAbsentK = 0;
                    foreach (PupilOfClassRanking pupilRank in listPupilRanking)
                    {
                        int pupilID = pupilRank.PupilID;
                        // Khoi tao item
                        objMarkOfSemester = new MarkOfSemesterDefault();
                        // Lay len thong tin diem cua hoc sinh
                        listPupilSum = listSummedUpRecord.Where(o => o.PupilID == pupilID).ToList();
                        // Lay len thon tin khen thuong
                        pupilEmulationBO = listPupilEmulation.Find(o => o.PupilID == pupilID);
                        // Lay thong tin hoc luc hanh kiem
                        string pupilInfo = "";
                        int countSubject = listClassSubject.Count;
                        countAbsentP = lstPupilAbsent.Where(p => p.PupilID == pupilID && p.IsAccepted == true).Count();
                        countAbsentK = lstPupilAbsent.Where(p => p.PupilID == pupilID && p.IsAccepted == false).Count();
                        // Thong diem diem theo mon
                        foreach (ClassSubjectBO cs in listClassSubject)
                        {
                            objSummedUpRecordBO = listPupilSum.Find(o => o.SubjectID == cs.SubjectID);
                            if (objSummedUpRecordBO != null)
                            {
                                if (!objSummedUpRecordBO.SummedUpMark.HasValue && string.IsNullOrEmpty(objSummedUpRecordBO.JudgementResult))
                                {
                                    continue;
                                }
                                pupilInfo += cs.DisplayName + ": " + (cs.IsCommenting.Value == SystemParamsInFile.ISCOMMENTING_TYPE_JUDGE ? objSummedUpRecordBO.JudgementResult + "\n" : (objSummedUpRecordBO.SummedUpMark.HasValue ? (grade > 1 ? objSummedUpRecordBO.SummedUpMark.Value.ToString("0.0") : objSummedUpRecordBO.SummedUpMark.Value.ToString("0.#")) : string.Empty)) + (grade > 1 && objSummedUpRecordBO.IsCommenting == SystemParamsInFile.ISCOMMENTING_TYPE_MARK ? "\n" : "");
                                if (grade == 1 && objSummedUpRecordBO.SummedUpMark.HasValue)
                                {
                                    decimal SummedUpMark = objSummedUpRecordBO.SummedUpMark.Value;
                                    if (SummedUpMark > GlobalConstants.EXCELLENT_MARK)
                                    {
                                        pupilInfo += WCFConstant.LEVEL_EXPERT + "\n";
                                    }
                                    else if (SummedUpMark > GlobalConstants.GOOD_MARK_PRIMARY)
                                    {
                                        pupilInfo += WCFConstant.LEVEL_GOOD + "\n";
                                    }
                                    else if (SummedUpMark >= GlobalConstants.NORMAL_MARK)
                                    {
                                        pupilInfo += WCFConstant.LEVEL_NORMAL + "\n";
                                    }
                                    else
                                    {
                                        pupilInfo += WCFConstant.LEVEL_WEAK + "\n";
                                    }
                                }
                            }
                        }
                        if (pupilInfo.EndsWith("\n"))
                        {
                            pupilInfo = pupilInfo.Substring(0, pupilInfo.Length - 1);
                        }
                        // Neu la cap 1 thi bo sung them thong tin nghi hoc
                        if (grade == 1)
                        {
                            if (!string.IsNullOrEmpty(pupilRank.ConductLevel))
                            {
                                pupilInfo += "\nHạnh kiểm: " + pupilRank.ConductLevel;
                            }

                            if (!string.IsNullOrEmpty(pupilRank.CapacityLevel))
                            {
                                pupilInfo += "\nXếp loại giáo dục: " + pupilRank.CapacityLevel;
                            }

                            if (pupilEmulationBO != null && !string.IsNullOrEmpty(pupilEmulationBO.HonourAchivementTypeResolution))
                            {
                                pupilInfo += "\nDanh hiệu: " + pupilEmulationBO.HonourAchivementTypeResolution;
                            }

                            if (countAbsentP > 0)
                            {
                                pupilInfo += "\nNghỉ có phép: " + countAbsentP;
                            }

                            if (countAbsentK > 0)
                            {
                                pupilInfo += "\nNghỉ không phép: " + countAbsentK;
                            }
                        }
                        else
                        {
                            if (pupilRank.AverageMark.HasValue && pupilRank.AverageMark.Value > 0)
                            {
                                pupilInfo += "\nTrung bình các môn: " + pupilRank.AverageMark.Value.ToString("0.0");
                            }

                            if (!string.IsNullOrEmpty(pupilRank.CapacityLevel))
                            {
                                pupilInfo += "\nHọc lực: " + pupilRank.CapacityLevel;
                            }

                            if (!string.IsNullOrEmpty(pupilRank.ConductLevel))
                            {
                                pupilInfo += "\nHạnh kiểm: " + pupilRank.ConductLevel;
                            }

                            if (pupilRank.Rank.HasValue && pupilRank.Rank.Value > 0)
                            {
                                pupilInfo += "\nXếp hạng: " + pupilRank.Rank.ToString();
                            }

                            if (pupilEmulationBO != null && !string.IsNullOrEmpty(pupilEmulationBO.HonourAchivementTypeResolution))
                            {
                                pupilInfo += "\nDanh hiệu thi đua: " + pupilEmulationBO.HonourAchivementTypeResolution;
                            }

                            if (countAbsentP > 0)
                            {
                                pupilInfo += "\nNghỉ có phép: " + countAbsentP;
                            }

                            if (countAbsentK > 0)
                            {
                                pupilInfo += "\nNghỉ không phép: " + countAbsentK;
                            }

                        }
                        // Thong tin diem TBM
                        // gan thong tin chi tiet object de add
                        objMarkOfSemester.Content = pupilInfo;
                        objMarkOfSemester.PupilProfileID = pupilID;
                        // Gan vao list de tra ve
                        listMarkOfSemesterResponse.Add(objMarkOfSemester);
                    }
                    //gan validationCode thanh cong
                    objResultResponse.ValidationCode = WCFConstant.RESPONSE_SUCCESS;
                    //gan gia tri thuoc tinh dinh kem 
                    objResultResponse.LstMarkOfSemesterDefault = listMarkOfSemesterResponse;
                }
            }
            catch (Exception ex)
            {
                LogExtensions.GetCurrentClass = System.Reflection.MethodBase.GetCurrentMethod().DeclaringType;
                string para = string.Format("SchoolID={0}, ClassID={1}, AcademicYearID={2}, Semester={3}", SchoolID, ClassID, AcademicYearID, Semester);
                LogExtensions.ErrorExt(logger, DateTime.Now, para, ex.Message, "null");
                //gan validationCode Exception
                objResultResponse.ValidationCode = WCFConstant.RESPONSE_EXCEPTION;
            }
            return objResultResponse;
        }

        /// <summary>
        /// Cấp 1:Toán T1,2,3 GK T4,5 CK
        /// Thể dục NX1,2,3,4,5
        /// Cấp 2,3:Toán M P V KTHK 
        /// Thể Dục M P V KTHK
        /// - nghi co phep:2
        /// - nghi khong phep:1
        /// </summary>
        /// <param name="SchoolID"></param>
        /// <param name="ClassID"></param>
        /// <param name="EducationLevel"></param>
        /// <param name="Year"></param>
        /// <param name="DateStart"></param>
        /// <param name="DateEnd"></param>
        /// <param name="CheckAbsent"></param>
        /// <returns></returns>
        public ListMarkRecordResponse GetMarkRecordByClass(int SchoolID, int ClassID, int EducationLevel,
             int AcademicYearID, DateTime DateStart, DateTime DateEnd, bool CheckAbsent, int Semester)
        {
            ListMarkRecordResponse objResultResponse = new ListMarkRecordResponse();
            try
            {
                //Kiem tra neu EducationLevel <=0 hoac EducationLevel >= 13
                if (EducationLevel <= 0 || EducationLevel >= 13)
                {
                    //gan validationCode thanh cong
                    objResultResponse.ValidationCode = WCFConstant.RESPONSE_EDUCATION_LEVEL_NOT_FOUND;
                    return objResultResponse;
                }
                //Khoi tao Business
                DeclareBusiness();
                // Kiem tra truong hoac nam hoc khong ton tai
                if (SchoolProfileBusiness.Find(SchoolID) == null)
                {
                    //gan validationCode thanh cong
                    objResultResponse.ValidationCode = WCFConstant.RESPONSE_SCHOOLID_NOT_FOUND;
                    return objResultResponse;
                }
                if (AcademicYearBusiness.Find(AcademicYearID) == null)
                {
                    //gan validationCode thanh cong
                    objResultResponse.ValidationCode = WCFConstant.RESPONSE_ACADEMICYEARID_NOT_FOUND;
                    return objResultResponse;
                }

                //lay len diem chi tiet cac mon theo datetime
                IDictionary<string, object> dic = new Dictionary<string, object>();
                dic["SchoolID"] = SchoolID;
                dic["ClassID"] = ClassID;
                dic["AcademicYearID"] = AcademicYearID;
                dic["Semester"] = Semester;
                //Lay len diem cua lop
                IQueryable<MarkRecordWSBO> IQMarkRecord = MarkRecordBusiness.GetMardRecordOfAllSubjectInClass(dic);
                //Lay diem nhan xet cua lop
                IQueryable<JudgeRecordWSBO> IQJudgeRecord = JudgeRecordBusiness.GetAllJudgeRecordOfClass(dic);

                //Loc thoi gian cham diem mon tinh diem
                //Lay lai thoi gian bat dau va ket thuc cho chinh xac
                DateTime fromDate = new DateTime(DateStart.Year, DateStart.Month, DateStart.Day);
                DateTime toDate = new DateTime(DateEnd.Year, DateEnd.Month, DateEnd.Day).AddDays(1);
                bool isPrimary = EducationLevel >= 1 && EducationLevel <= 5;
                var IQFilterMarkRecordTmp = (from m in IQMarkRecord
                                             where m.MarkedDate >= fromDate
                                             && m.MarkedDate < toDate
                                             select new
                                             {
                                                 m.PupilID,
                                                 m.SubjectName,
                                                 m.SubjectOrderNumber,
                                                 m.MarkOrderNumber,
                                                 m.Mark,
                                                 m.Title,
                                                 m.MarkTypeID,
                                                 m.MarkedDate,
                                                 m.MarkredDatePrimary
                                             }).ToList();

                var IQFilterMarkRecord = (from m in IQFilterMarkRecordTmp
                                          where (isPrimary && !m.Title.Contains("VGK") && !m.Title.Contains("ĐGK")
                                                           && !m.Title.Contains("VCK") && !m.Title.Contains("ĐCK")
                                                           && !m.Title.Contains("VCN") && !m.Title.Contains("ĐCN"))
                                          || (isPrimary == false)
                                          select new
                                          {
                                              m.PupilID,
                                              m.SubjectName,
                                              m.SubjectOrderNumber,
                                              m.MarkOrderNumber,
                                              Mark = m.Mark.HasValue ? m.Mark.Value.ToString("0.#") : string.Empty,
                                              Title = !isPrimary && m.Title.Contains("M") ? "M" : !isPrimary && m.Title.Contains("P") ? "15P" : !isPrimary && m.Title.Contains("V")
                                                        ? "1T" : !isPrimary && m.Title.Contains("HK") ? "KTHK" : isPrimary && m.Title.Contains("ĐTX") && m.MarkredDatePrimary.HasValue
                                                        ? m.Title + (SetMonthWithPrimary(m.MarkredDatePrimary.Value.Month))
                                                        : isPrimary && m.Title.Contains("GK") ? "GK" : isPrimary && (m.Title.Contains("CK") || m.Title.Contains("CN")) ? "CK" : m.Title + " ",
                                              m.MarkTypeID
                                          }).ToList();

                //Loc thoi gian cham diem mon nhan xet
                var LsFilterJudgeRecordTmp = (from j in IQJudgeRecord
                                              where j.MarkedDate >= fromDate
                                              && j.MarkedDate < toDate
                                              select new
                                              {
                                                  j.PupilID,
                                                  j.SubjectID,
                                                  j.SubjectName,
                                                  j.SubjectOrderNumber,
                                                  j.MarkOrderNumber,
                                                  Judgement = j.Judgement == null ? string.Empty : j.Judgement,
                                                  j.MarkTypeID,
                                                  Title = j.Title,
                                                  EducationLevelID = j.EducationLevelID
                                              }).ToList();

                var LsFilterJudgeRecord = (from j in LsFilterJudgeRecordTmp
                                           select new
                                           {
                                               j.PupilID,
                                               j.SubjectID,
                                               j.SubjectName,
                                               j.SubjectOrderNumber,
                                               j.MarkOrderNumber,
                                               Judgement = j.Judgement == null ? string.Empty : j.Judgement,
                                               j.MarkTypeID,
                                               Title = j.Title.Contains("P") ? "15P" : j.Title.Contains("V") ? "1T" : j.Title.Contains("M") ? "M" : isPrimary ? "NX" + (Semester == 2 ? (j.EducationLevelID > 2 ? j.MarkOrderNumber + 5 : j.MarkOrderNumber + 4) : j.MarkOrderNumber) : (!isPrimary && j.Title.Contains("HK") ? "KTHK" : j.Title),
                                           }).ToList();
                //Loc nhung hoc sinh co diem mon tin diem phat sinh
                var IQMarkRecordGroup = (from p in IQFilterMarkRecord
                                         orderby p.MarkTypeID, p.SubjectOrderNumber, p.SubjectName, p.MarkOrderNumber
                                         group p by new
                                         {
                                             p.PupilID,
                                             p.SubjectName,
                                             p.SubjectOrderNumber,
                                             p.Title
                                         } into g
                                         select new
                                         {
                                             PupilID = g.Key.PupilID.Value,
                                             SubjectName = g.Key.SubjectName,
                                             SubjectOrderNumber = g.Key.SubjectOrderNumber,
                                             OrderPrimaryID = SetOrderPrimary(isPrimary, g.Key.Title),
                                             MarkContent = (isPrimary && g.Key.Title.Contains("ĐTX")) ? (g.Key.Title.Replace("ĐTX", "T") + ": " + g.Select(x => x.Mark).Aggregate((a, b) => (a + " " + b)))
                                                    : isPrimary && (g.Key.Title.Contains("GK") || g.Key.Title.Contains("CK")) ? (g.Key.Title + ": " + g.Select(x => x.Mark).Aggregate((a, b) => (a + ", " + b))) : (g.Key.Title + ": " + g.Select(x => x.Mark).Aggregate((a, b) => (a + " " + b)))
                                         }).ToList();
                //Nhom diem cua cac mon tinh diem lai
                var IQMarkRecordResponse = (from mg in IQMarkRecordGroup
                                            orderby mg.SubjectOrderNumber, mg.SubjectName, mg.OrderPrimaryID
                                            group mg
                                            by new
                                            {
                                                mg.PupilID,
                                                mg.SubjectName
                                            }
                                                into g
                                            select new MarkRecordDefault
                                            {
                                                PupilProfileID = g.Key.PupilID,
                                                Content = "-" + g.Key.SubjectName + "\n" + g.Select(x => x.MarkContent).Aggregate((a, b) => (a + "\n" + b)) + "\n"
                                            }).ToList();

                // neu la cap 1 thi lay diem kiem tra hoc ky mon nhan xet trong summed_up_record                
                //var judgeRecordPrimary = (
                //                          from poc in PupilOfClassBusiness.All
                //                          join sur in SummedUpRecordBusiness.All on poc.PupilID equals sur.PupilID
                //                          where sur.ClassID == ClassID
                //                          && sur.AcademicYearID == AcademicYearID
                //                          && sur.Semester == Semester
                //                          && poc.ClassID == ClassID
                //                          && poc.AcademicYearID == AcademicYearID
                //                          && poc.Status == GlobalConstants.PUPIL_STATUS_STUDYING
                //                          && sur.JudgementResult != null
                //                          && isPrimary
                //                          select new
                //                          {
                //                              sur.PupilID,
                //                              sur.SubjectID,
                //                              sur.JudgementResult
                //                          }).ToList();

                //isPrimary = isPrimary && judgeRecordPrimary != null && judgeRecordPrimary.Count > 0;

                //Loc nhung hoc sinh co diem  mon nhan xet phat sinh
                var IQMarkJudgeGroup = (from p in LsFilterJudgeRecord
                                        orderby p.MarkTypeID, p.MarkOrderNumber
                                        group p by new
                                        {
                                            p.PupilID,
                                            p.SubjectName,
                                            p.SubjectID,
                                            p.SubjectOrderNumber,
                                            Title = p.Title
                                        } into g
                                        select new
                                        {
                                            PupilID = g.Key.PupilID.Value,
                                            SubjectName = g.Key.SubjectName,
                                            SubjectID = g.Key.SubjectID,
                                            SubjectOrderNumber = g.Key.SubjectOrderNumber,
                                            JudgementContent = g.Key.Title
                                                + ": " + g.Select(x => (x.Judgement.ToString())).Aggregate((a, b) => (a + " " + b))
                                            //((g.Key.Title.Contains("NX")
                                            //  && isPrimary
                                            //  )
                                            //? g.Select(x => (x.Judgement.ToString())).Aggregate((a, b) => (a + " " + b))
                                            //+ (g.Key.Title == LsFilterJudgeRecord.Where(p => p.PupilID == g.Key.PupilID && p.SubjectID == g.Key.SubjectID && p.Title.Contains("NX")).OrderByDescending(p => (Convert.ToInt32(p.Title.Replace("NX", "")))).Select(p => p.Title).FirstOrDefault()
                                            //        && judgeRecordPrimary.Where(p => p.SubjectID == g.Key.SubjectID && p.PupilID == g.Key.PupilID).ToList() != null
                                            //        && judgeRecordPrimary.Where(p => p.SubjectID == g.Key.SubjectID && p.PupilID == g.Key.PupilID).Count() > 0
                                            //        ? ("\nHK: " + judgeRecordPrimary.Where(p => p.SubjectID == g.Key.SubjectID && p.PupilID == g.Key.PupilID).Select(x => (x.JudgementResult.ToString())).Aggregate((a, b) => (a + " " + b)))
                                            //        : string.Empty)
                                            //: g.Select(x => (x.Judgement.ToString())).Aggregate((a, b) => (a + " " + b)))
                                        }).ToList();

                //Nhom diem cua cac mon nhan xet lai
                var IQMarkJudgeResponse = (from mg in IQMarkJudgeGroup
                                           orderby mg.SubjectOrderNumber, mg.SubjectName
                                           group mg
                                            by new
                                            {
                                                mg.PupilID,
                                                mg.SubjectName,
                                                mg.SubjectID
                                            }
                                               into g
                                           select new MarkRecordDefault
                                           {
                                               PupilProfileID = g.Key.PupilID,
                                               Content = "-" + g.Key.SubjectName + "\n"
                                                        + g.Select(x => x.JudgementContent).Aggregate((a, b) => (a + "\n" + b)) + "\n"
                                           }).ToList();
                // loc hoc sinh trung nhau
                IQMarkJudgeResponse = (from p in IQMarkJudgeResponse
                                       group p by new { p.PupilProfileID }
                                           into g
                                       select new MarkRecordDefault
                                       {
                                           PupilProfileID = g.Key.PupilProfileID,
                                           Content = g.Where(p => p.PupilProfileID == g.Key.PupilProfileID).Select(p => p.Content).Aggregate((a, b) => (a + b))
                                       }).ToList();

                IQMarkRecordResponse = (from p in IQMarkRecordResponse
                                        group p by new { p.PupilProfileID }
                                            into g
                                        select new MarkRecordDefault
                                        {
                                            PupilProfileID = g.Key.PupilProfileID,
                                            Content = g.Where(p => p.PupilProfileID == g.Key.PupilProfileID).Select(p => p.Content).Aggregate((a, b) => (a + b))
                                        }).ToList();

                // Bo sung them mon
                foreach (var item in IQMarkJudgeResponse)
                {
                    var t = IQMarkRecordResponse.FirstOrDefault(o => o.PupilProfileID == item.PupilProfileID);
                    if (t != null)
                    {
                        t.Content += item.Content;
                        t.Content = t.Content.Substring(0, t.Content.Length - 1);
                    }
                    else
                    {
                        IQMarkRecordResponse.Add(item);
                    }
                }
                //Filter nhung hoc sinh co content moi hien thi
                //if (EducationLevel == WCFConstant.SCHOOL_PRIMARY_LEVEL)// Neu la truong tieu hoc
                //{
                //    //gan gia tri thuoc tinh dinh kem 
                //    objResultResponse.LstMarkRecordDefault = IQMarkRecordResponse;
                //}
                //else
                //{
                if (CheckAbsent)
                {
                    DateStart = DateTime.Now.Date.AddMonths(-1).AddDays(1);
                    DateEnd = DateTime.Now.Date.AddHours(23).AddMinutes(59).AddSeconds(59);
                    //lay len danh sach hoc sinh nghi hoc
                    List<PupilAbsence> IQPupilAbsence = PupilAbsenceBusiness.SearchBySchool(SchoolID, dic).Where(p => p.AbsentDate >= DateStart && p.AbsentDate <= DateEnd).ToList();

                    //Filter de Count so ngay nghi khong phep cua tung hoc sinh trong lop
                    var IQPupilAbsencePCount = (from p in IQPupilAbsence
                                                where p.IsAccepted == true
                                                group p by new
                                                {
                                                    p.PupilID
                                                } into g
                                                select new
                                                {
                                                    PupilProfileID = g.Key.PupilID,
                                                    Absent = g.Count() > 0 ? "\n-Nghỉ có phép: " + g.Count().ToString() : string.Empty
                                                }).ToList();
                    //Filter de Count so ngay nghi co phep cua tung hoc sinh trong lop
                    var IQPupilAbsenceKCount = (from p in IQPupilAbsence
                                                where p.IsAccepted == false
                                                group p by new
                                                {
                                                    p.PupilID
                                                } into g
                                                select new
                                                {
                                                    PupilProfileID = g.Key.PupilID,
                                                    Absent = g.Count() > 0 ? "\n-Nghỉ không phép: " + g.Count().ToString() : string.Empty
                                                }).ToList();

                    //Loc nhung hoc sinh co diem phat sinh
                    List<MarkRecordDefault> IQFilterMarkRecordResponse = (from mr in IQMarkRecordResponse
                                                                          join fa in IQPupilAbsencePCount on mr.PupilProfileID equals fa.PupilProfileID into jP
                                                                          from g1 in jP.DefaultIfEmpty()
                                                                          join pa in IQPupilAbsenceKCount on mr.PupilProfileID equals pa.PupilProfileID into jK
                                                                          from g2 in jK.DefaultIfEmpty()
                                                                          select new MarkRecordDefault
                                                                          {
                                                                              PupilProfileID = mr.PupilProfileID,
                                                                              Content = (mr.Content.EndsWith("\n") ? mr.Content.Substring(0, mr.Content.Length - 1) : mr.Content) +
                                                                              (g1 == null ? string.Empty : g1.Absent) +
                                                                              (g2 == null ? string.Empty : g2.Absent)
                                                                          }).Distinct().ToList();

                    //gan gia tri thuoc tinh dinh kem 
                    objResultResponse.LstMarkRecordDefault = IQFilterMarkRecordResponse;
                }
                else
                {
                    //gan gia tri thuoc tinh dinh kem 
                    objResultResponse.LstMarkRecordDefault = IQMarkRecordResponse;
                }
                //}

                //gan validationCode thanh cong
                objResultResponse.ValidationCode = WCFConstant.RESPONSE_SUCCESS;

            }
            catch (Exception ex)
            {
                LogExtensions.GetCurrentClass = System.Reflection.MethodBase.GetCurrentMethod().DeclaringType;
                string para = string.Format(" SchoolID={0}, ClassID={1}, EducationLevel={2},AcademicYearID={3}, DateStart={4}, DateEnd={5}, CheckAbsent={6}, Semester={7}",
                                                SchoolID, ClassID, EducationLevel, AcademicYearID, DateStart, DateEnd, CheckAbsent, Semester);
                LogExtensions.ErrorExt(logger, DateTime.Now, para, ex.Message, "null");
                //gan validationCode Exception
                objResultResponse.ValidationCode = WCFConstant.RESPONSE_EXCEPTION;
            }
            return objResultResponse;
        }

        /// <summary>
        /// Lấy thông tin học sinh theo lớp học
        /// </summary>
        /// <param name="SchoolID"></param>
        /// <param name="AcademicYearID"></param>
        /// <param name="ClassID"></param>
        /// <returns></returns>
        public ListPupilProfileLiteResponse GetListPupilByClass(int SchoolID, int AcademicYearID, int ClassID)
        {
            ListPupilProfileLiteResponse objResultResponse = new ListPupilProfileLiteResponse();
            try
            {
                //Khoi tao Business
                DeclareBusiness();
                List<PupilProfileLite> listPupil = (from poc in PupilOfClassBusiness.All
                                                    join pp in PupilProfileBusiness.All on poc.PupilID equals pp.PupilProfileID
                                                    where poc.SchoolID == SchoolID && poc.AcademicYearID == AcademicYearID
                                                    && poc.ClassID == ClassID && pp.IsActive
                                                    select new PupilProfileLite
                                                    {
                                                        PupilProfileID = poc.PupilID,
                                                        CurrentClassID = pp.CurrentClassID,
                                                        FullName = pp.FullName,
                                                        Status = poc.Status
                                                    }).ToList();
                //gan validationCode thanh cong
                objResultResponse.ValidationCode = WCFConstant.RESPONSE_SUCCESS;
                //gan gia tri thuoc tinh dinh kem 
                objResultResponse.LstPupilProfileLite = listPupil;
            }
            catch
            {
                //gan validationCode Exception
                objResultResponse.ValidationCode = WCFConstant.RESPONSE_EXCEPTION;
            }
            return objResultResponse;
        }

        public ListMarkRecordResponse GetListMarkRecordOfPupil(int PupilProfileID, int AcademicYearID, int ClassID)
        {
            ListMarkRecordResponse objMarkRecord = new ListMarkRecordResponse();
            try
            {
                DeclareBusiness();
                List<PupilRankingLite> lstPupilRanking = new List<PupilRankingLite>();
                PupilRankingLite objPupilRankingLite = null;
                PupilRankingBO item = null;

                AcademicYear acaYear = AcademicYearBusiness.Find(AcademicYearID);
                bool isMovedHistory = UtilsBusiness.IsMoveHistory(acaYear);

                List<CapacityLevel> lstCapacityLevel = CapacityLevelBusiness.All.ToList();

                List<PupilRankingBO> lstPupilRankingLite = new List<PupilRankingBO>();
                if (isMovedHistory)
                {
                    lstPupilRankingLite = (from p in PupilRankingHistoryBusiness.All
                                           where p.PupilID == PupilProfileID
                                           && p.AcademicYearID == AcademicYearID
                                           && p.ClassID == ClassID
                                           select new PupilRankingBO 
                                           {
                                               PeriodID = p.PeriodID,
                                               PupilID = p.PupilID,
                                               Semester = p.Semester,
                                               CapacityLevelID = p.CapacityLevelID,
                                               ConductLevelID = p.ConductLevelID,
                                               AverageMark = p.AverageMark
                                           }).ToList();

                    LogExtensions.ErrorExt(logger, DateTime.Now,null, "Lay history " + AcademicYearID, "null");
                }
                else
                {
                    lstPupilRankingLite = (from p in PupilRankingBusiness.All
                                           where p.PupilID == PupilProfileID
                                           && p.AcademicYearID == AcademicYearID
                                           && p.ClassID == ClassID
                                           select new PupilRankingBO
                                           {
                                               PeriodID = p.PeriodID,
                                               PupilID = p.PupilID,
                                               Semester = p.Semester,
                                               CapacityLevelID = p.CapacityLevelID,
                                               ConductLevelID = p.ConductLevelID,
                                               AverageMark = p.AverageMark
                                           }).ToList();

                    LogExtensions.ErrorExt(logger, DateTime.Now, null, "Khong  history " + AcademicYearID, "null");
                }

                
                if (lstPupilRankingLite != null)
                {
                    objPupilRankingLite = new PupilRankingLite();
                    for (int i = 0; i < lstPupilRankingLite.Count; i++)
                    {
                        item = lstPupilRankingLite[i];
                        if (item.PeriodID == null)
                        {
                            objPupilRankingLite.PupilProfileID = item.PupilID.GetValueOrDefault();

                            CapacityLevel cap = CapacityLevelBusiness.Find(item.CapacityLevelID);
                            ConductLevel con = ConductLevelBusiness.Find(item.ConductLevelID);

                            if (item.Semester == 1)
                            {
                                objPupilRankingLite.CapacityHKI = cap != null ? cap.CapacityLevel1 : string.Empty;
                                objPupilRankingLite.ConductHKI = con != null ? con.Resolution : string.Empty;
                                objPupilRankingLite.AverageHKI = item.AverageMark.HasValue ? item.AverageMark.Value.ToString("0.0") : string.Empty;
                            }
                            else if (item.Semester == 2)
                            {
                                objPupilRankingLite.CapacityHKII = cap != null ? cap.CapacityLevel1 : string.Empty;
                                objPupilRankingLite.ConductHKII = con != null ? con.Resolution : string.Empty;
                                objPupilRankingLite.AverageHKII = item.AverageMark.HasValue ? item.AverageMark.Value.ToString("0.0") : string.Empty;

                            }
                            else if (item.Semester == 3)
                            {
                                objPupilRankingLite.CapacityCN = cap != null ? cap.CapacityLevel1 : string.Empty;
                                objPupilRankingLite.ConductCN = con != null ? con.Resolution : string.Empty;
                                objPupilRankingLite.AverageCN = item.AverageMark.HasValue ? item.AverageMark.Value.ToString("0.0") : string.Empty;
                            }
                            else if (item.Semester == 4)//nếu có thi lại thì lấy điểm thi lại
                            {
                                objPupilRankingLite.CapacityCN = cap != null ? cap.CapacityLevel1 : string.Empty;
                                objPupilRankingLite.ConductCN = con != null ? con.Resolution : string.Empty;
                                objPupilRankingLite.AverageCN = item.AverageMark.HasValue ? item.AverageMark.Value.ToString("0.0") : string.Empty;
                            }
                        }
                    }
                    lstPupilRanking.Add(objPupilRankingLite);
                }
                objMarkRecord.LstPupilRankingLite = lstPupilRanking;
                objMarkRecord.ValidationCode = WCFConstant.RESPONSE_SUCCESS;
            }
            catch (Exception ex)
            {
                LogExtensions.GetCurrentClass = System.Reflection.MethodBase.GetCurrentMethod().DeclaringType;
                string para = string.Format("PupilProfileID={0}, AcademicYearID={1}, ClassID={2}", PupilProfileID, AcademicYearID, ClassID);
                LogExtensions.ErrorExt(logger, DateTime.Now, para, ex.Message, "null");
                objMarkRecord.ValidationCode = WCFConstant.RESPONSE_EXCEPTION;
            }
            return objMarkRecord;
        }

        public ListPupilProfileLiteResponse GetListPupilOfClass(int PupilProfileID)
        {
            ListPupilProfileLiteResponse objPupilProfile = new ListPupilProfileLiteResponse();
            try
            {
                DeclareBusiness();
                objPupilProfile.LstPupilOfClass = (from poc in PupilOfClassBusiness.All
                                                   join c in ClassProfileBusiness.All on poc.ClassID equals c.ClassProfileID
                                                   join s in SchoolProfileBusiness.All on poc.SchoolID equals s.SchoolProfileID
                                                   where poc.PupilID == PupilProfileID
                                                   && (poc.Status == GlobalConstants.PUPIL_STATUS_STUDYING
                                                        || poc.Status == GlobalConstants.PUPIL_STATUS_GRADUATED)
                                                   select new PupilOfClassLite
                                                   {
                                                       AcademicYearID = poc.AcademicYearID,
                                                       Year = poc.AcademicYear.Year,
                                                       ClassID = c.ClassProfileID,
                                                       ClassName = c.DisplayName,
                                                       SchoolName = s.SchoolName
                                                   }).OrderByDescending(p => p.Year).ToList();
                objPupilProfile.ValidationCode = WCFConstant.RESPONSE_SUCCESS;
                return objPupilProfile;
            }
            catch (Exception ex)
            {
                LogExtensions.GetCurrentClass = System.Reflection.MethodBase.GetCurrentMethod().DeclaringType;
                string para = string.Format("PupilProfileID={0}", PupilProfileID);
                LogExtensions.ErrorExt(logger, DateTime.Now, para, ex.Message, "null");
                objPupilProfile.ValidationCode = WCFConstant.RESPONSE_EXCEPTION;
                return objPupilProfile;
            }
        }

        /// <summary>
        /// Lấy danh sách học sinh
        /// </summary>
        /// <param name="SchoolID"></param>
        /// <param name="AcademicYearID"></param>
        /// <param name="Grade"></param>
        /// <param name="EducationLevelID"></param>
        /// <param name="ClassID"></param>
        /// <returns></returns>
        public ListPupilProfileLiteResponse GetListPupil(int SchoolID, int AcademicYearID, int EducationLevelID, int ClassID, int GradeID = 0)
        {
            ListPupilProfileLiteResponse objResultResponse = new ListPupilProfileLiteResponse();
            try
            {
                //Khoi tao Business
                DeclareBusiness();
                List<PupilProfileLite> listPupil = new List<PupilProfileLite>();
                // Tách làm 2 nhánh để trong trường hợp chỉ có mỗi lớp không phải join vào bảng CLASS
                if (ClassID == 0)
                {
                    listPupil = (from poc in PupilOfClassBusiness.All
                                 join pp in PupilProfileBusiness.All on poc.PupilID equals pp.PupilProfileID
                                 where poc.SchoolID == SchoolID && poc.AcademicYearID == AcademicYearID && pp.IsActive
                                 && (GradeID == 0 || poc.ClassProfile.EducationLevel.Grade == GradeID)
                                 && (poc.ClassProfile.EducationLevelID == EducationLevelID || EducationLevelID == 0)
                                 && (poc.Status == GlobalConstants.PUPIL_STATUS_STUDYING || poc.Status == GlobalConstants.PUPIL_STATUS_GRADUATED)
                                 orderby poc.OrderInClass, pp.Name, pp.FullName
                                 select new PupilProfileLite
                                 {
                                     PupilProfileID = poc.PupilID,
                                     CurrentClassID = poc.ClassID,
                                     FullName = pp.FullName,
                                     Status = poc.Status,
                                     PupilCode = pp.PupilCode,
                                 }).ToList();
                }
                else
                {
                    // kiem tra dam bao ton tai du lieu trong db
                    ClassID = ClassProfileBusiness.All.Where(p => p.SchoolID == SchoolID && p.AcademicYearID == AcademicYearID && p.ClassProfileID == ClassID).Select(p => p.ClassProfileID).FirstOrDefault();
                    if (ClassID > 0)
                    {
                        listPupil = (from poc in PupilOfClassBusiness.All
                                     join pp in PupilProfileBusiness.All on poc.PupilID equals pp.PupilProfileID
                                     where poc.SchoolID == SchoolID && poc.AcademicYearID == AcademicYearID && pp.IsActive
                                     && pp.CurrentSchoolID == SchoolID && poc.ClassID == ClassID
                                     && (poc.Status == GlobalConstants.PUPIL_STATUS_STUDYING || poc.Status == GlobalConstants.PUPIL_STATUS_GRADUATED)
                                     orderby poc.OrderInClass, pp.Name, pp.FullName
                                     select new PupilProfileLite
                                     {
                                         PupilProfileID = poc.PupilID,
                                         CurrentClassID = pp.CurrentClassID,
                                         FullName = pp.FullName,
                                         Status = poc.Status,
                                         PupilCode = pp.PupilCode,
                                     }).ToList();

                    }
                }
                //gan validationCode thanh cong
                objResultResponse.ValidationCode = WCFConstant.RESPONSE_SUCCESS;
                //gan gia tri thuoc tinh dinh kem 
                objResultResponse.LstPupilProfileLite = listPupil;
            }
            catch
            {
                //gan validationCode Exception
                objResultResponse.ValidationCode = WCFConstant.RESPONSE_EXCEPTION;
            }
            return objResultResponse;
        }

        #region lấy danh sách học sinh theo dạng mảng chuỗi - AnhVD9
        /// <summary>
        /// lay danh sach hoc sinh theo lop
        /// </summary>
        /// <param name="SchoolID"></param>
        /// <param name="AcademicYearID"></param>
        /// <param name="EducationLevelID"></param>
        /// <param name="ClassID"></param>
        /// <param name="GradeID"></param>
        /// <returns></returns>
        public ListPupilResponseFormatString GetListPupilformatString(int SchoolID, int AcademicYearID, int EducationLevelID, int ClassID, int GradeID = 0)
        {
            ListPupilResponseFormatString response = new ListPupilResponseFormatString();
            try
            {
                DeclareBusiness();
                IQueryable<PupilProfileLite> iqPupil = null;

                // Tách làm 2 nhánh để trong trường hợp chỉ có mỗi lớp không phải join vào bảng CLASS
                if (ClassID == 0)
                {
                    IQueryable<PupilOfClass> iqPOC = from poc in PupilOfClassBusiness.All
                                                     where poc.SchoolID == SchoolID && poc.AcademicYearID == AcademicYearID && poc.PupilProfile.IsActive
                                                     && (poc.Status == GlobalConstants.PUPIL_STATUS_STUDYING || poc.Status == GlobalConstants.PUPIL_STATUS_GRADUATED)
                                                     orderby poc.ClassProfile.EducationLevelID, poc.ClassProfile.OrderNumber, poc.ClassProfile.DisplayName, poc.OrderInClass, poc.PupilProfile.Name, poc.PupilProfile.FullName
                                                     select poc;
                    // tránh where điều kiện @constant = value
                    if (GradeID > 0)
                    {
                        iqPOC = iqPOC.Where(o => o.ClassProfile.EducationLevel.Grade == GradeID);
                    }

                    if (EducationLevelID > 0)
                    {
                        iqPOC = iqPOC.Where(o => o.ClassProfile.EducationLevelID == EducationLevelID);
                    }

                    iqPupil = (from poc in iqPOC
                               select new PupilProfileLite
                               {
                                   PupilProfileID = poc.PupilID,
                                   CurrentClassID = poc.ClassID,
                                   FullName = poc.PupilProfile.FullName,
                                   PupilCode = poc.PupilProfile.PupilCode,
                                   Status = poc.Status,
                                   CurrentClassName = poc.ClassProfile.DisplayName
                               });
                }
                else
                {
                    if (ClassID > 0)
                    {
                        iqPupil = (from poc in PupilOfClassBusiness.All
                                   where poc.SchoolID == SchoolID && poc.AcademicYearID == AcademicYearID && poc.PupilProfile.IsActive && poc.ClassID == ClassID
                                   && (poc.Status == GlobalConstants.PUPIL_STATUS_STUDYING || poc.Status == GlobalConstants.PUPIL_STATUS_GRADUATED)
                                   orderby poc.OrderInClass, poc.PupilProfile.Name, poc.PupilProfile.FullName
                                   select new PupilProfileLite
                                   {
                                       PupilProfileID = poc.PupilID,
                                       CurrentClassID = poc.ClassID,
                                       FullName = poc.PupilProfile.FullName,
                                       PupilCode = poc.PupilProfile.PupilCode,
                                       Status = poc.Status,
                                       CurrentClassName = poc.ClassProfile.DisplayName
                                   });

                    }
                }
                List<string> pupils = new List<string>();
                StringBuilder builder = null;
                foreach (PupilProfileLite item in iqPupil)
                {
                    builder = new StringBuilder();
                    builder.Append(item.PupilProfileID).Append(DELIMITER_SIGN)
                            .Append(item.CurrentClassID).Append(DELIMITER_SIGN)
                            .Append(item.PupilCode).Append(DELIMITER_SIGN)
                            .Append(item.FullName).Append(DELIMITER_SIGN)
                            .Append(item.CurrentClassName).Append(DELIMITER_SIGN)
                            .Append(item.Status)
                            ;
                    pupils.Add(builder.ToString());
                }

                response.ValidationCode = WCFConstant.RESPONSE_SUCCESS;
                response.LstPupilStr = pupils;
            }
            catch
            {
                response.ValidationCode = WCFConstant.RESPONSE_EXCEPTION;
            }
            return response;
        }
        #endregion

        #region lấy danh sách mã học sinh
        /// <summary>
        /// lay danh sach ma hoc sinh
        /// </summary>
        /// <param name="SchoolID"></param>
        /// <param name="AcademicYearID"></param>
        /// <param name="EducationLevelID"></param>
        /// <param name="ClassID"></param>
        /// <param name="GradeID"></param>
        /// <returns></returns>
        public List<int> GetListPupilID(int SchoolID, int AcademicYearID, int EducationLevelID, int ClassID, int GradeID = 0)
        {
            try
            {
                DeclareBusiness();
                if (ClassID == 0)
                {
                    IQueryable<PupilOfClass> iqPOC = from poc in PupilOfClassBusiness.All
                                                     where poc.SchoolID == SchoolID
                                                        && poc.AcademicYearID == AcademicYearID
                                                        && poc.PupilProfile.IsActive
                                                        && (poc.Status == GlobalConstants.PUPIL_STATUS_STUDYING || poc.Status == GlobalConstants.PUPIL_STATUS_GRADUATED)
                                                     select poc;
                    if (GradeID > 0) iqPOC = iqPOC.Where(o => o.ClassProfile.EducationLevel.Grade == GradeID);
                    if (EducationLevelID > 0) iqPOC = iqPOC.Where(o => o.ClassProfile.EducationLevelID == EducationLevelID);
                    return iqPOC.Select(o => o.PupilID).ToList();
                }
                else
                {
                    return (from poc in PupilOfClassBusiness.All
                            where poc.SchoolID == SchoolID
                            && poc.AcademicYearID == AcademicYearID
                            && poc.PupilProfile.IsActive
                            && poc.ClassID == ClassID
                            && (poc.Status == GlobalConstants.PUPIL_STATUS_STUDYING || poc.Status == GlobalConstants.PUPIL_STATUS_GRADUATED)
                            select poc.PupilID).ToList();


                }
            }
            catch (Exception ex)
            {
                LogExtensions.GetCurrentClass = System.Reflection.MethodBase.GetCurrentMethod().DeclaringType;
                string para = string.Format("SchoolID={0}, AcademicYearID={1}, EducationLevelID={2}, ClassID={3}", SchoolID, AcademicYearID, EducationLevelID, ClassID);
                LogExtensions.ErrorExt(logger, DateTime.Now, para, ex.Message, "null");
                return null;
            }
        }
        #endregion
        public ListPupilProfileLiteResponse GetListPupilByName(int SchoolID, int AcademicYearID, int? ClassID, int Grade, string nameOfPupil)
        {
            ListPupilProfileLiteResponse objResultResponse = new ListPupilProfileLiteResponse();
            try
            {
                //Khoi tao Business
                DeclareBusiness();
                string loweredNameOfPupil = nameOfPupil.ToLower();
                List<int> gradesOfClass = new List<int>();
                if ((Grade & 1) == 1) gradesOfClass.Add(1);// Them cap 1 vao danh sach cac cap tim kiem
                if ((Grade & 2) == 2) gradesOfClass.Add(2);// Them cap 2 vao danh sach cac cap tim kiem
                if ((Grade & 4) == 4) gradesOfClass.Add(3);// Them cap 3 vao danh sach cac cap tim kiem
                if ((Grade & 8) == 8) gradesOfClass.Add(4);// Them cap 4 vao danh sach cac cap tim kiem
                if ((Grade & 16) == 16) gradesOfClass.Add(5);// Them cap 5 vao danh sach cac cap tim kiem
                objResultResponse.listPupilProfileDetail =
                    (from poc in PupilOfClassBusiness.All
                     join pp in PupilProfileBusiness.All on poc.PupilID equals pp.PupilProfileID
                     where poc.SchoolID == SchoolID // tim theo truong
                         && poc.AcademicYearID == AcademicYearID // tim theo nam
                         && pp.IsActive
                         && (poc.ClassID == ClassID || !ClassID.HasValue) // tim theo lop neu can
                         && gradesOfClass.Contains(poc.ClassProfile.EducationLevel.Grade)  // tim theo cap hoc
                         && pp.FullName.ToLower().Contains(loweredNameOfPupil) // tim theo ten
                     select new PupilProfileDetail
                     {
                         PupilID = poc.PupilID,
                         Name = pp.Name,
                         FullName = pp.FullName,
                         PupilCode = pp.PupilCode,
                         BirthDate = pp.BirthDate,

                         FatherFullName = pp.FatherFullName,
                         MotherFullName = pp.MotherFullName,
                         SponsorFullName = pp.SponsorFullName,
                         FatherMobile = pp.FatherMobile,
                         MotherMobile = pp.MotherMobile,
                         SponsorMobile = pp.SponsorMobile,
                         Genre = pp.Genre,
                         Mobile = pp.Mobile,

                         ClassID = poc.ClassID,
                         ClassName = poc.ClassProfile.DisplayName,
                         SchoolName = poc.SchoolProfile.SchoolName,
                         EducationLevelID = poc.ClassProfile.EducationLevelID,
                         OrderInClass = poc.OrderInClass,
                         CurrentStatus = poc.Status
                     })
                    .ToList();
                //gan validationCode thanh cong
                objResultResponse.ValidationCode = WCFConstant.RESPONSE_SUCCESS;
            }
            catch
            {
                //gan validationCode Exception
                objResultResponse.ValidationCode = WCFConstant.RESPONSE_EXCEPTION;
            }
            return objResultResponse;
        }

        public ListPupilProfileLiteResponse GetListPupilBySchoolID(int SchoolID, int AcademicYearID, int? EducationLevelID, int grade)
        {
            ListPupilProfileLiteResponse objPupilProfile = new ListPupilProfileLiteResponse();
            try
            {
                DeclareBusiness();
                IDictionary<string, object> dic = new Dictionary<string, object>();
                dic["SchoolID"] = SchoolID;
                dic["AcademicYearID"] = AcademicYearID;
                dic["EducationLevelID"] = EducationLevelID;
                dic["AppliedLevel"] = grade;
                dic["Check"] = "check";

                objPupilProfile.listPupilProfileDetail = (from poc in PupilOfClassBusiness.Search(dic)
                                                          join pp in PupilProfileBusiness.AllNoTracking on poc.PupilID equals pp.PupilProfileID
                                                          join cp in ClassProfileBusiness.AllNoTracking.Where(cp => cp.AcademicYearID == AcademicYearID) on poc.ClassID equals cp.ClassProfileID
                                                          join ed in EducationLevelBusiness.AllNoTracking on cp.EducationLevelID equals ed.EducationLevelID
                                                          join sp in SchoolProfileBusiness.AllNoTracking.Where(sp => sp.SchoolProfileID == SchoolID) on poc.SchoolID equals sp.SchoolProfileID
                                                          where (poc.Status == GlobalConstants.PUPIL_STATUS_STUDYING)
                                                          select new PupilProfileDetail
                                                          {
                                                              PupilID = poc.PupilID,
                                                              PupilCode = pp.PupilCode,
                                                              CurrentAcademicYearID = poc.AcademicYearID,
                                                              ClassID = poc.ClassID,
                                                              FullName = pp.FullName,
                                                              ClassName = cp.DisplayName,
                                                              SchoolName = sp.SchoolName,
                                                              EducationLevelID = cp.EducationLevelID,
                                                              Name = pp.Name,
                                                              FatherFullName = pp.FatherFullName,
                                                              MotherFullName = pp.MotherFullName,
                                                              SponsorFullName = pp.SponsorFullName,
                                                              FatherMobile = pp.FatherMobile,
                                                              MotherMobile = pp.MotherMobile,
                                                              SponsorMobile = pp.SponsorMobile,
                                                              Genre = pp.Genre,
                                                              Mobile = pp.Mobile,
                                                              OrderInClass = poc.OrderInClass,
                                                              BirthDate = pp.BirthDate,
                                                              EducationLevelName = ed.Resolution,
                                                              IsRegisterContract = poc.IsRegisterContract,
                                                              OrderNumber = cp.OrderNumber.HasValue ? cp.OrderNumber.Value : 0,
                                                              CurrentStatus = poc.Status
                                                          }).OrderBy(p => p.EducationLevelID).ThenBy(p => p.OrderNumber).ThenBy(p => p.ClassName).ThenBy(p => p.OrderInClass).ThenBy(p => p.Name).ToList();
                objPupilProfile.ValidationCode = WCFConstant.RESPONSE_SUCCESS;
                return objPupilProfile;
            }
            catch (Exception ex)
            {
                LogExtensions.GetCurrentClass = System.Reflection.MethodBase.GetCurrentMethod().DeclaringType;
                string para = string.Format("SchoolID={0}, AcademicYearID={1}, EducationLevelID={2}, grade={3}", SchoolID, AcademicYearID, EducationLevelID, grade);
                LogExtensions.ErrorExt(logger, DateTime.Now, para, ex.Message, "null");
                objPupilProfile.ValidationCode = WCFConstant.RESPONSE_EXCEPTION;
                return objPupilProfile;
            }
        }

        public ListPupilProfileLiteResponse GetListPupilByListPupilID(List<int> pupilIDList, int SchoolID, int AcademicYearID, int ClassID, int? EducationLevelID, string PupilCode, string Pupilname, int grade, int teacherID)
        {
            ListPupilProfileLiteResponse objPupilProfile = new ListPupilProfileLiteResponse();
            try
            {
                DeclareBusiness();
                IDictionary<string, object> dic = new Dictionary<string, object>();
                dic["SchoolID"] = SchoolID;
                dic["AcademicYearID"] = AcademicYearID;
                dic["EducationLevelID"] = EducationLevelID;
                dic["AppliedLevel"] = grade;
                dic["Check"] = "check";
                dic["ClassID"] = ClassID;
                dic["FullName"] = Pupilname;
                dic["PupilCode"] = PupilCode;
                if (teacherID > 0)
                {
                    List<int> listClassID = this.GetListClassByHeadTeacher(AcademicYearID, SchoolID, teacherID).Select(p => p.ClassProfileID).ToList();
                    if (listClassID != null && listClassID.Count > 0)
                    {
                        dic["ListClassID"] = listClassID;
                    }
                }
                objPupilProfile.listPupilProfileDetail = (from poc in PupilOfClassBusiness.Search(dic)
                                                          join pp in PupilProfileBusiness.All on poc.PupilID equals pp.PupilProfileID
                                                          join cp in ClassProfileBusiness.All.Where(cp => cp.AcademicYearID == AcademicYearID) on poc.ClassID equals cp.ClassProfileID
                                                          join ed in EducationLevelBusiness.All on cp.EducationLevelID equals ed.EducationLevelID
                                                          join sp in SchoolProfileBusiness.All.Where(sp => sp.SchoolProfileID == SchoolID) on poc.SchoolID equals sp.SchoolProfileID
                                                          where sp.SchoolProfileID == SchoolID
                                                          && pupilIDList.Contains(poc.PupilID)
                                                          && poc.PupilID == pp.PupilProfileID
                                                          && poc.Status != GlobalConstants.PUPIL_STATUS_MOVED_TO_OTHER_CLASS
                                                          select new PupilProfileDetail
                                                          {
                                                              PupilID = poc.PupilID,
                                                              PupilCode = pp.PupilCode,
                                                              CurrentAcademicYearID = poc.AcademicYearID,
                                                              ClassID = poc.ClassID,
                                                              FullName = pp.FullName,
                                                              ClassName = cp.DisplayName,
                                                              SchoolName = sp.SchoolName,
                                                              EducationLevelID = cp.EducationLevelID,
                                                              Name = pp.Name,
                                                              FatherFullName = pp.FatherFullName,
                                                              MotherFullName = pp.MotherFullName,
                                                              SponsorFullName = pp.SponsorFullName,
                                                              FatherMobile = pp.FatherMobile,
                                                              MotherMobile = pp.MotherMobile,
                                                              SponsorMobile = pp.SponsorMobile,
                                                              Genre = pp.Genre,
                                                              Mobile = pp.Mobile,
                                                              OrderInClass = poc.OrderInClass,
                                                              BirthDate = pp.BirthDate,
                                                              EducationLevelName = ed.Resolution,
                                                              IsRegisterContract = poc.IsRegisterContract,
                                                              OrderNumber = cp.OrderNumber.HasValue ? cp.OrderNumber.Value : 0,
                                                              CurrentStatus = poc.Status,
                                                              PupilOfClassID = poc.PupilOfClassID
                                                          }).OrderBy(p => p.EducationLevelID).ThenBy(p => p.OrderNumber).ThenBy(p => p.ClassName).ThenBy(p => p.OrderInClass).ThenBy(p => p.Name).ToList();

                objPupilProfile.ValidationCode = WCFConstant.RESPONSE_SUCCESS;
                return objPupilProfile;
            }
            catch (Exception ex)
            {
                LogExtensions.GetCurrentClass = System.Reflection.MethodBase.GetCurrentMethod().DeclaringType;
                string para = string.Format("SchoolID={0}, AcademicYearID={1}, ClassID={2},  EducationLevelID={3}, PupilCode={4}, Pupilname={5}, grade={6}, teacherID={7}", SchoolID, AcademicYearID, ClassID, EducationLevelID, PupilCode, Pupilname, grade, teacherID);
                LogExtensions.ErrorExt(logger, DateTime.Now, para, ex.Message, "null");
                objPupilProfile.ValidationCode = WCFConstant.RESPONSE_EXCEPTION;
                return objPupilProfile;
            }
        }

        #region Lấy danh sách học sinh từ danh sách ID truyền sang (format dạng chuỗi để giảm kích thước, KO dùng ToList để tránh cao tải)
        /// <summary>
        /// Lay danh sach hoc sinh tu danh sach Ma hoc sinh cho truoc
        /// </summary>
        /// <param name="pupilIDList"></param>
        /// <param name="SchoolID"></param>
        /// <param name="AcademicYearID"></param>
        /// <param name="ClassID"></param>
        /// <param name="EducationLevelID"></param>
        /// <param name="PupilCode"></param>
        /// <param name="Pupilname"></param>
        /// <param name="grade"></param>
        /// <param name="teacherID"></param>
        /// <returns></returns>
        public List<string> GetPupilsByIDsformatString(List<int> pupilIDList, int SchoolID, int AcademicYearID, int ClassID, int? EducationLevelID, string PupilCode, string Pupilname, int grade, int teacherID)
        {
            List<string> pupils = new List<string>();
            try
            {
                DeclareBusiness();
                IDictionary<string, object> dic = new Dictionary<string, object>();
                dic["SchoolID"] = SchoolID;
                dic["AcademicYearID"] = AcademicYearID;
                dic["EducationLevelID"] = EducationLevelID;
                dic["AppliedLevel"] = grade;
                dic["Check"] = "check";
                dic["ClassID"] = ClassID;
                dic["FullName"] = Pupilname;
                dic["PupilCode"] = PupilCode;
                if (teacherID > 0)
                {
                    List<int> listClassID = this.GetListClassByHeadTeacher(AcademicYearID, SchoolID, teacherID).Select(p => p.ClassProfileID).ToList();
                    if (listClassID != null && listClassID.Count > 0)
                    {
                        dic["ListClassID"] = listClassID;
                    }
                }
                IQueryable<PupilProfileDetail> iqPupil = (from poc in PupilOfClassBusiness.Search(dic)
                                                          join pp in PupilProfileBusiness.All on poc.PupilID equals pp.PupilProfileID
                                                          join cp in ClassProfileBusiness.All on poc.ClassID equals cp.ClassProfileID
                                                          join ed in EducationLevelBusiness.All on cp.EducationLevelID equals ed.EducationLevelID
                                                          join sp in SchoolProfileBusiness.All on poc.SchoolID equals sp.SchoolProfileID
                                                          where sp.SchoolProfileID == SchoolID
                                                          && pupilIDList.Contains(poc.PupilID)
                                                          && poc.PupilID == pp.PupilProfileID
                                                          && poc.Status != GlobalConstants.PUPIL_STATUS_MOVED_TO_OTHER_CLASS
                                                          select new PupilProfileDetail
                                                          {
                                                              PupilID = poc.PupilID,
                                                              PupilCode = pp.PupilCode,
                                                              CurrentAcademicYearID = poc.AcademicYearID,
                                                              ClassID = poc.ClassID,
                                                              FullName = pp.FullName,
                                                              ClassName = cp.DisplayName,
                                                              SchoolName = sp.SchoolName,
                                                              EducationLevelID = cp.EducationLevelID,
                                                              Name = pp.Name,
                                                              FatherFullName = pp.FatherFullName,
                                                              MotherFullName = pp.MotherFullName,
                                                              SponsorFullName = pp.SponsorFullName,
                                                              FatherMobile = pp.FatherMobile,
                                                              MotherMobile = pp.MotherMobile,
                                                              SponsorMobile = pp.SponsorMobile,
                                                              Genre = pp.Genre,
                                                              Mobile = pp.Mobile,
                                                              OrderInClass = poc.OrderInClass,
                                                              EducationLevelName = ed.Resolution,
                                                              IsRegisterContract = poc.IsRegisterContract,
                                                              OrderNumber = cp.OrderNumber.HasValue ? cp.OrderNumber.Value : 0,
                                                              CurrentStatus = poc.Status,
                                                              PupilOfClassID = poc.PupilOfClassID
                                                          }).OrderBy(p => p.EducationLevelID)
                                                          .ThenBy(p => p.OrderNumber)
                                                          .ThenBy(p => p.ClassName)
                                                          .ThenBy(p => p.OrderInClass)
                                                          .ThenBy(p => p.Name);
                StringBuilder builder = null;
                foreach (PupilProfileDetail item in iqPupil)
                {
                    builder = new StringBuilder();
                    builder.Append(item.PupilID).Append(DELIMITER_SIGN)
                            .Append(item.PupilCode).Append(DELIMITER_SIGN)
                            .Append(item.FullName).Append(DELIMITER_SIGN)
                            .Append(item.Name).Append(DELIMITER_SIGN)
                            .Append(item.Genre).Append(DELIMITER_SIGN)
                            .Append(item.CurrentStatus).Append(DELIMITER_SIGN)
                            .Append(item.ClassID).Append(DELIMITER_SIGN)
                            .Append(item.ClassName).Append(DELIMITER_SIGN)
                            .Append(item.EducationLevelID).Append(DELIMITER_SIGN)
                            .Append(item.EducationLevelName).Append(DELIMITER_SIGN)

                            .Append(item.FatherMobile).Append(DELIMITER_SIGN)
                            .Append(item.MotherMobile).Append(DELIMITER_SIGN)
                            .Append(item.SponsorMobile).Append(DELIMITER_SIGN)
                            .Append(item.OrderNumber).Append(DELIMITER_SIGN)
                            .Append(item.OrderInClass).Append(DELIMITER_SIGN)
                            .Append(item.PupilOfClassID).Append(DELIMITER_SIGN)
                            .Append(item.IsRegisterContract);
                    pupils.Add(builder.ToString());
                }
                return pupils;
            }
            catch (Exception ex)
            {
                LogExtensions.GetCurrentClass = System.Reflection.MethodBase.GetCurrentMethod().DeclaringType;
                string para = string.Format("SchoolID={0}, AcademicYearID={1}, ClassID={2},  EducationLevelID={3}, PupilCode={4}, Pupilname={5}, grade={6}, teacherID={7}", SchoolID, AcademicYearID, ClassID, EducationLevelID, PupilCode, Pupilname, grade, teacherID);
                LogExtensions.ErrorExt(logger, DateTime.Now, para, ex.Message, "null");
                return pupils;
            }
        }
        #endregion

        /// Lay danh sach hoc sinh theo list pupilID co hop dong phan trang co trang thai
        public ListPupilProfileLiteResponse GetListPupilByListPupilIDPaging(List<int> pupilIDList, int SchoolID, int AcademicYearID, int ClassID, int? EducationLevelID, string PupilCode, string Pupilname, int grade, int currentPage, int pageSize, int teacherID)
        {
            ListPupilProfileLiteResponse objPupilProfile = new ListPupilProfileLiteResponse();
            try
            {
                DeclareBusiness();
                IDictionary<string, object> dic = new Dictionary<string, object>();
                dic["SchoolID"] = SchoolID;
                dic["AcademicYearID"] = AcademicYearID;
                dic["EducationLevelID"] = EducationLevelID;
                dic["AppliedLevel"] = grade;
                dic["Check"] = "check";
                dic["ClassID"] = ClassID;
                dic["FullName"] = Pupilname;
                dic["PupilCode"] = PupilCode;

                if (teacherID > 0)
                {
                    List<int> listClassID = this.GetListClassByHeadTeacher(AcademicYearID, SchoolID, teacherID).Select(p => p.ClassProfileID).ToList();
                    if (listClassID != null && listClassID.Count > 0)
                    {
                        dic["ListClassID"] = listClassID;
                    }
                }
                IQueryable<PupilProfileDetail> iqPupilProfile = (from poc in PupilOfClassBusiness.Search(dic)
                                                                 join pp in PupilProfileBusiness.All on poc.PupilID equals pp.PupilProfileID
                                                                 join cp in ClassProfileBusiness.All on poc.ClassID equals cp.ClassProfileID
                                                                 join ed in EducationLevelBusiness.All on cp.EducationLevelID equals ed.EducationLevelID
                                                                 join sp in SchoolProfileBusiness.All on poc.SchoolID equals sp.SchoolProfileID
                                                                 where sp.SchoolProfileID == SchoolID
                                                                 && pupilIDList.Contains(poc.PupilID)
                                                                 && poc.PupilID == pp.PupilProfileID
                                                                 && poc.Status != GlobalConstants.PUPIL_STATUS_MOVED_TO_OTHER_CLASS
                                                                 select new PupilProfileDetail
                                                                 {
                                                                     PupilID = poc.PupilID,
                                                                     PupilCode = pp.PupilCode,
                                                                     CurrentAcademicYearID = poc.AcademicYearID,
                                                                     ClassID = poc.ClassID,
                                                                     FullName = pp.FullName,
                                                                     ClassName = cp.DisplayName,
                                                                     SchoolName = sp.SchoolName,
                                                                     EducationLevelID = cp.EducationLevelID,
                                                                     Name = pp.Name,
                                                                     FatherFullName = pp.FatherFullName,
                                                                     MotherFullName = pp.MotherFullName,
                                                                     SponsorFullName = pp.SponsorFullName,
                                                                     FatherMobile = pp.FatherMobile,
                                                                     MotherMobile = pp.MotherMobile,
                                                                     SponsorMobile = pp.SponsorMobile,
                                                                     Genre = pp.Genre,
                                                                     Mobile = pp.Mobile,
                                                                     OrderInClass = poc.OrderInClass,
                                                                     BirthDate = pp.BirthDate,
                                                                     EducationLevelName = ed.Resolution,
                                                                     IsRegisterContract = poc.IsRegisterContract,
                                                                     CurrentStatus = poc.Status,
                                                                     OrderNumber = cp.OrderNumber.HasValue ? cp.OrderNumber.Value : 0,
                                                                     PupilOfClassID = poc.PupilOfClassID
                                                                 }).OrderBy(p => p.EducationLevelID).ThenBy(p => p.OrderNumber).ThenBy(p => p.ClassName).ThenBy(p => p.OrderInClass).ThenBy(p => p.Name);
                if (ClassID <= 0)
                {
                    objPupilProfile.listPupilProfileDetail = iqPupilProfile.Skip((currentPage - 1) * pageSize).Take(pageSize).ToList();
                    objPupilProfile.TotalRecord = iqPupilProfile.Count();
                }
                else
                {
                    objPupilProfile.listPupilProfileDetail = iqPupilProfile.ToList();
                }

                objPupilProfile.ValidationCode = WCFConstant.RESPONSE_SUCCESS;
                return objPupilProfile;
            }
            catch (Exception ex)
            {
                LogExtensions.GetCurrentClass = System.Reflection.MethodBase.GetCurrentMethod().DeclaringType;
                string para = string.Format("SchoolID={0}, AcademicYearID={1}, ClassID={2},  EducationLevelID={3}, PupilCode={4}, Pupilname={5}, grade={6}, teacherID={7}", SchoolID, AcademicYearID, ClassID, EducationLevelID, PupilCode, Pupilname, grade, teacherID);
                LogExtensions.ErrorExt(logger, DateTime.Now, para, ex.Message, "null");
                objPupilProfile.ValidationCode = WCFConstant.RESPONSE_EXCEPTION;
                return objPupilProfile;
            }
        }

        #region AnHVD9 - Lấy danh sách học sinh (có phân trang) từ danh sách ID truyền sang (format dạng chuỗi để giảm kích thước, KO dùng ToList để tránh cao tải)
        /// <summary>
        /// Lay danh sach hoc sinh tu danh sach Ma hoc sinh cho truoc
        /// </summary>
        /// <param name="pupilIDList"></param>
        /// <param name="SchoolID"></param>
        /// <param name="AcademicYearID"></param>
        /// <param name="ClassID"></param>
        /// <param name="EducationLevelID"></param>
        /// <param name="PupilCode"></param>
        /// <param name="Pupilname"></param>
        /// <param name="grade"></param>
        /// <param name="teacherID"></param>
        /// <returns></returns>
        public ListPupilResponseFormatString GetPupilsByIDsPagingformatString(List<int> pupilIDList, int SchoolID, int AcademicYearID, int ClassID, int? EducationLevelID, string PupilCode, string Pupilname, int grade, int currentPage, int pageSize, int teacherID)
        {
            ListPupilResponseFormatString objPupilProfile = new ListPupilResponseFormatString();
            try
            {
                DeclareBusiness();
                IDictionary<string, object> dic = new Dictionary<string, object>();
                dic["SchoolID"] = SchoolID;
                dic["AcademicYearID"] = AcademicYearID;
                dic["EducationLevelID"] = EducationLevelID;
                dic["AppliedLevel"] = grade;
                dic["Check"] = "check";
                dic["ClassID"] = ClassID;
                dic["FullName"] = Pupilname;
                dic["PupilCode"] = PupilCode;

                if (teacherID > 0)
                {
                    List<int> listClassID = this.GetListClassByHeadTeacher(AcademicYearID, SchoolID, teacherID).Select(p => p.ClassProfileID).ToList();
                    if (listClassID != null && listClassID.Count > 0)
                    {
                        dic["ListClassID"] = listClassID;
                    }
                }
                IQueryable<PupilProfileDetail> iqPupil = (from poc in PupilOfClassBusiness.Search(dic)
                                                          join pp in PupilProfileBusiness.All on poc.PupilID equals pp.PupilProfileID
                                                          join cp in ClassProfileBusiness.All on poc.ClassID equals cp.ClassProfileID
                                                          join ed in EducationLevelBusiness.All on cp.EducationLevelID equals ed.EducationLevelID
                                                          join sp in SchoolProfileBusiness.All on poc.SchoolID equals sp.SchoolProfileID
                                                          where sp.SchoolProfileID == SchoolID
                                                          && pupilIDList.Contains(poc.PupilID)
                                                          && poc.PupilID == pp.PupilProfileID
                                                          && poc.Status != GlobalConstants.PUPIL_STATUS_MOVED_TO_OTHER_CLASS
                                                          select new PupilProfileDetail
                                                          {
                                                              PupilID = poc.PupilID,
                                                              PupilCode = pp.PupilCode,
                                                              CurrentAcademicYearID = poc.AcademicYearID,
                                                              ClassID = poc.ClassID,
                                                              FullName = pp.FullName,
                                                              ClassName = cp.DisplayName,
                                                              SchoolName = sp.SchoolName,
                                                              EducationLevelID = cp.EducationLevelID,
                                                              Name = pp.Name,
                                                              FatherFullName = pp.FatherFullName,
                                                              MotherFullName = pp.MotherFullName,
                                                              SponsorFullName = pp.SponsorFullName,
                                                              FatherMobile = pp.FatherMobile,
                                                              MotherMobile = pp.MotherMobile,
                                                              SponsorMobile = pp.SponsorMobile,
                                                              Genre = pp.Genre,
                                                              Mobile = pp.Mobile,
                                                              OrderInClass = poc.OrderInClass,
                                                              BirthDate = pp.BirthDate,
                                                              EducationLevelName = ed.Resolution,
                                                              IsRegisterContract = poc.IsRegisterContract,
                                                              CurrentStatus = poc.Status,
                                                              OrderNumber = cp.OrderNumber.HasValue ? cp.OrderNumber.Value : 0,
                                                              PupilOfClassID = poc.PupilOfClassID
                                                          }).OrderBy(p => p.EducationLevelID)
                                                          .ThenBy(p => p.OrderNumber)
                                                          .ThenBy(p => p.ClassName)
                                                          .ThenBy(p => p.OrderInClass)
                                                          .ThenBy(p => p.Name);

                objPupilProfile.TotalRecord = iqPupil.Count();
                if (ClassID <= 0)
                {
                    iqPupil = iqPupil.Skip((currentPage - 1) * pageSize).Take(pageSize);
                }

                StringBuilder builder = null;
                List<string> pupils = new List<string>();
                foreach (PupilProfileDetail item in iqPupil)
                {
                    builder = new StringBuilder();
                    builder.Append(item.PupilID).Append(DELIMITER_SIGN)
                            .Append(item.PupilCode).Append(DELIMITER_SIGN)
                            .Append(item.FullName).Append(DELIMITER_SIGN)
                            .Append(item.Name).Append(DELIMITER_SIGN)
                            .Append(item.Genre).Append(DELIMITER_SIGN)
                            .Append(item.CurrentStatus).Append(DELIMITER_SIGN)
                            .Append(item.ClassID).Append(DELIMITER_SIGN)
                            .Append(item.ClassName).Append(DELIMITER_SIGN)
                            .Append(item.EducationLevelID).Append(DELIMITER_SIGN)
                            .Append(item.EducationLevelName).Append(DELIMITER_SIGN)

                            .Append(item.FatherMobile).Append(DELIMITER_SIGN)
                            .Append(item.MotherMobile).Append(DELIMITER_SIGN)
                            .Append(item.SponsorMobile).Append(DELIMITER_SIGN)
                            .Append(item.OrderNumber).Append(DELIMITER_SIGN)
                            .Append(item.OrderInClass).Append(DELIMITER_SIGN)
                            .Append(item.PupilOfClassID).Append(DELIMITER_SIGN)
                            .Append(item.IsRegisterContract);
                    pupils.Add(builder.ToString());
                }
                objPupilProfile.ValidationCode = WCFConstant.RESPONSE_SUCCESS;
                objPupilProfile.LstPupilStr = pupils;

                return objPupilProfile;
            }
            catch (Exception ex)
            {
                LogExtensions.GetCurrentClass = System.Reflection.MethodBase.GetCurrentMethod().DeclaringType;
                string para = string.Format("SchoolID={0}, AcademicYearID={1}, ClassID={2},  EducationLevelID={3}, PupilCode={4}, Pupilname={5}, grade={6}, teacherID={7}", SchoolID, AcademicYearID, ClassID, EducationLevelID, PupilCode, Pupilname, grade, teacherID);
                LogExtensions.ErrorExt(logger, DateTime.Now, para, ex.Message, "null");
                objPupilProfile.ValidationCode = WCFConstant.RESPONSE_EXCEPTION;
                return objPupilProfile;
            }
        }
        #endregion

        //Danh sach ho sinh cua truong, khong quan tam den hop dong
        public ListPupilProfileLiteResponse GetListPupilBySchoolIDPaging(int SchoolID, int AcademicYearID, int ClassID, string PupilCode, string Pupilname, int? EducationLevelID, int grade, int Page, int NumPage, bool isRegisContract, int teacherID)
        {
            ListPupilProfileLiteResponse objPupilProfile = new ListPupilProfileLiteResponse();
            try
            {
                DeclareBusiness();
                IDictionary<string, object> dic = new Dictionary<string, object>();
                dic["SchoolID"] = SchoolID;
                dic["AcademicYearID"] = AcademicYearID;
                dic["EducationLevelID"] = EducationLevelID;
                dic["AppliedLevel"] = grade;
                dic["Check"] = "check";
                dic["ClassID"] = ClassID;
                dic["PupilCode"] = PupilCode;
                dic["FullName"] = Pupilname;
                if (teacherID > 0)
                {
                    List<int> listClassID = this.GetListClassByHeadTeacher(AcademicYearID, SchoolID, teacherID).Select(p => p.ClassProfileID).ToList();
                    if (listClassID != null && listClassID.Count > 0)
                    {
                        dic["ListClassID"] = listClassID;
                    }
                }

                IQueryable<PupilProfileDetail> listPupilProfileDetail = (from poc in PupilOfClassBusiness.Search(dic)
                                                                         join pp in PupilProfileBusiness.All on poc.PupilID equals pp.PupilProfileID
                                                                         join cp in ClassProfileBusiness.All.Where(c => c.AcademicYearID == AcademicYearID) on poc.ClassID equals cp.ClassProfileID
                                                                         join ed in EducationLevelBusiness.All on cp.EducationLevelID equals ed.EducationLevelID
                                                                         join sp in SchoolProfileBusiness.All.Where(c => c.SchoolProfileID == SchoolID) on poc.SchoolID equals sp.SchoolProfileID // review : schoolprofile
                                                                         where ((!isRegisContract && (poc.IsRegisterContract == null || poc.IsRegisterContract == false)) || isRegisContract == true)
                                                                                         && poc.Status != GlobalConstants.PUPIL_STATUS_MOVED_TO_OTHER_CLASS
                                                                         select new PupilProfileDetail
                                                                         {
                                                                             PupilID = poc.PupilID,
                                                                             PupilCode = pp.PupilCode,
                                                                             CurrentAcademicYearID = poc.AcademicYearID,
                                                                             ClassID = poc.ClassID,
                                                                             FullName = pp.FullName,
                                                                             ClassName = cp.DisplayName,
                                                                             SchoolName = sp.SchoolName,
                                                                             EducationLevelID = cp.EducationLevelID,
                                                                             Name = pp.Name,
                                                                             FatherFullName = pp.FatherFullName,
                                                                             MotherFullName = pp.MotherFullName,
                                                                             SponsorFullName = pp.SponsorFullName,
                                                                             FatherMobile = pp.FatherMobile,
                                                                             MotherMobile = pp.MotherMobile,
                                                                             SponsorMobile = pp.SponsorMobile,
                                                                             Genre = pp.Genre,
                                                                             Mobile = pp.Mobile,
                                                                             OrderInClass = poc.OrderInClass,
                                                                             BirthDate = pp.BirthDate,
                                                                             EducationLevelName = ed.Resolution,
                                                                             IsRegisterContract = poc.IsRegisterContract,
                                                                             CurrentStatus = poc.Status,
                                                                             OrderNumber = cp.OrderNumber.HasValue ? cp.OrderNumber.Value : 0,
                                                                             PupilOfClassID = poc.PupilOfClassID
                                                                         }).OrderBy(p => p.EducationLevelID)
                                                                    .ThenBy(p => p.OrderNumber)
                                                                    .ThenBy(p => p.ClassName)
                                                                    .ThenBy(p => p.OrderInClass)
                                                                    .ThenBy(p => p.Name);

                List<PupilProfileDetail> listPupilResult = new List<PupilProfileDetail>();
                if (ClassID > 0)
                {
                    listPupilResult = listPupilProfileDetail.ToList();
                }
                else
                {
                    listPupilResult = listPupilProfileDetail.Skip((Page - 1) * NumPage).Take(NumPage).ToList();
                }

                objPupilProfile.TotalRecord = listPupilProfileDetail.Count();
                objPupilProfile.listPupilProfileDetail = listPupilResult;
                objPupilProfile.ValidationCode = WCFConstant.RESPONSE_SUCCESS;
                return objPupilProfile;
            }
            catch (Exception ex)
            {
                LogExtensions.GetCurrentClass = System.Reflection.MethodBase.GetCurrentMethod().DeclaringType;
                string para = string.Format("SchoolID={0}, AcademicYearID={1}, ClassID={2},  EducationLevelID={3}, PupilCode={4}, Pupilname={5}, grade={6}, teacherID={7}", SchoolID, AcademicYearID, ClassID, EducationLevelID, PupilCode, Pupilname, grade, teacherID);
                LogExtensions.ErrorExt(logger, DateTime.Now, para, ex.Message, "null");
                objPupilProfile.ValidationCode = WCFConstant.RESPONSE_EXCEPTION;
                return objPupilProfile;
            }
        }

        #region AnHVD9 - Lấy danh sách học sinh (có phân trang) theo trường (format dạng chuỗi để giảm kích thước, KO dùng ToList để tránh cao tải)
        /// <summary>
        /// lay danh sach hoc sinh theo truong (co phan trang), ket qua tra ve theo dang noi chuoi
        /// </summary>
        /// <param name="SchoolID"></param>
        /// <param name="AcademicYearID"></param>
        /// <param name="ClassID"></param>
        /// <param name="PupilCode"></param>
        /// <param name="Pupilname"></param>
        /// <param name="EducationLevelID"></param>
        /// <param name="grade"></param>
        /// <param name="Page"></param>
        /// <param name="NumPage"></param>
        /// <param name="isRegisContract"></param>
        /// <param name="teacherID"></param>
        /// <returns></returns>
        public ListPupilResponseFormatString GetPupilsbySchoolIDPagingformatString(int SchoolID, int AcademicYearID, int ClassID, string PupilCode, string Pupilname, int? EducationLevelID, int grade, int Page, int NumPage, bool isRegisContract, int teacherID)
        {
            ListPupilResponseFormatString objPupilProfile = new ListPupilResponseFormatString();
            try
            {
                DeclareBusiness();
                IDictionary<string, object> dic = new Dictionary<string, object>();
                dic["SchoolID"] = SchoolID;
                dic["AcademicYearID"] = AcademicYearID;
                dic["EducationLevelID"] = EducationLevelID;
                dic["AppliedLevel"] = grade;
                dic["Check"] = "check";
                dic["ClassID"] = ClassID;
                dic["PupilCode"] = PupilCode;
                dic["FullName"] = Pupilname;
                if (teacherID > 0)
                {
                    List<int> listClassID = this.GetListClassByHeadTeacher(AcademicYearID, SchoolID, teacherID).Select(p => p.ClassProfileID).ToList();
                    if (listClassID != null && listClassID.Count > 0)
                    {
                        dic["ListClassID"] = listClassID;
                    }
                    //viethd31: Fix loi khi GV khong dc phan cong lop nao lai tim ra toan bo HS cac lop
                    else
                    {
                        dic["ListClassID"] = new List<int> { -1 };
                    }
                }

                IQueryable<PupilProfileDetail> iqPupils = (from poc in PupilOfClassBusiness.Search(dic)
                                                           join pp in PupilProfileBusiness.All on poc.PupilID equals pp.PupilProfileID
                                                           join cp in ClassProfileBusiness.All on poc.ClassID equals cp.ClassProfileID
                                                           join ed in EducationLevelBusiness.All on cp.EducationLevelID equals ed.EducationLevelID
                                                           join sp in SchoolProfileBusiness.All on poc.SchoolID equals sp.SchoolProfileID // review : schoolprofile
                                                           where ((!isRegisContract && (poc.IsRegisterContract == null || poc.IsRegisterContract == false)) || isRegisContract == true)
                                                                           && poc.Status != GlobalConstants.PUPIL_STATUS_MOVED_TO_OTHER_CLASS
                                                           select new PupilProfileDetail
                                                           {
                                                               PupilID = poc.PupilID,
                                                               PupilCode = pp.PupilCode,
                                                               CurrentAcademicYearID = poc.AcademicYearID,
                                                               ClassID = poc.ClassID,
                                                               FullName = pp.FullName,
                                                               ClassName = cp.DisplayName,
                                                               SchoolName = sp.SchoolName,
                                                               EducationLevelID = cp.EducationLevelID,
                                                               Name = pp.Name,
                                                               FatherFullName = pp.FatherFullName,
                                                               MotherFullName = pp.MotherFullName,
                                                               SponsorFullName = pp.SponsorFullName,
                                                               FatherMobile = pp.FatherMobile,
                                                               MotherMobile = pp.MotherMobile,
                                                               SponsorMobile = pp.SponsorMobile,
                                                               Genre = pp.Genre,
                                                               Mobile = pp.Mobile,
                                                               OrderInClass = poc.OrderInClass,
                                                               BirthDate = pp.BirthDate,
                                                               EducationLevelName = ed.Resolution,
                                                               IsRegisterContract = poc.IsRegisterContract,
                                                               CurrentStatus = poc.Status,
                                                               OrderNumber = cp.OrderNumber.HasValue ? cp.OrderNumber.Value : 0,
                                                               PupilOfClassID = poc.PupilOfClassID
                                                           }).OrderBy(p => p.EducationLevelID)
                                                                    .ThenBy(p => p.OrderNumber)
                                                                    .ThenBy(p => p.ClassName)
                                                                    .ThenBy(p => p.OrderInClass)
                                                                    .ThenBy(p => p.Name);

                objPupilProfile.TotalRecord = iqPupils.Count();
                if (ClassID <= 0)
                {
                    iqPupils = iqPupils.Skip((Page - 1) * NumPage).Take(NumPage);
                }
                StringBuilder builder = null;
                List<string> pupils = new List<string>();
                foreach (PupilProfileDetail item in iqPupils)
                {
                    builder = new StringBuilder();
                    builder.Append(item.PupilID).Append(DELIMITER_SIGN)
                            .Append(item.PupilCode).Append(DELIMITER_SIGN)
                            .Append(item.FullName).Append(DELIMITER_SIGN)
                            .Append(item.Name).Append(DELIMITER_SIGN)
                            .Append(item.Genre).Append(DELIMITER_SIGN)
                            .Append(item.CurrentStatus).Append(DELIMITER_SIGN)
                            .Append(item.ClassID).Append(DELIMITER_SIGN)
                            .Append(item.ClassName).Append(DELIMITER_SIGN)
                            .Append(item.EducationLevelID).Append(DELIMITER_SIGN)
                            .Append(item.EducationLevelName).Append(DELIMITER_SIGN)
                            .Append(item.FatherFullName).Append(DELIMITER_SIGN)
                            .Append(item.MotherFullName).Append(DELIMITER_SIGN)
                            .Append(item.FatherMobile).Append(DELIMITER_SIGN)
                            .Append(item.MotherMobile).Append(DELIMITER_SIGN)
                            .Append(item.SponsorMobile).Append(DELIMITER_SIGN)
                            .Append(item.OrderNumber).Append(DELIMITER_SIGN)
                            .Append(item.OrderInClass).Append(DELIMITER_SIGN)
                            .Append(item.PupilOfClassID).Append(DELIMITER_SIGN)
                            .Append(item.IsRegisterContract);
                    pupils.Add(builder.ToString());
                }
                objPupilProfile.ValidationCode = WCFConstant.RESPONSE_SUCCESS;
                objPupilProfile.LstPupilStr = pupils;
                return objPupilProfile;
            }
            catch (Exception ex)
            {
                LogExtensions.GetCurrentClass = System.Reflection.MethodBase.GetCurrentMethod().DeclaringType;
                string para = string.Format("SchoolID={0}, AcademicYearID={1}, ClassID={2},  EducationLevelID={3}, PupilCode={4}, Pupilname={5}, grade={6}, teacherID={7}", SchoolID, AcademicYearID, ClassID, EducationLevelID, PupilCode, Pupilname, grade, teacherID);
                LogExtensions.ErrorExt(logger, DateTime.Now, para, ex.Message, "null");
                objPupilProfile.ValidationCode = WCFConstant.RESPONSE_EXCEPTION;
                return objPupilProfile;
            }
        }
        #endregion

        //Lay dah sach hoc sinh toan truong
        public ListPupilProfileLiteResponse GetListPupilBySchoolIDFull(int SchoolID, int AcademicYearID, int ClassID, string PupilCode, string Pupilname, int? EducationLevelID, int grade, int teacherID)
        {
            ListPupilProfileLiteResponse objPupilProfile = new ListPupilProfileLiteResponse();
            try
            {
                DeclareBusiness();
                IDictionary<string, object> dic = new Dictionary<string, object>();
                dic["SchoolID"] = SchoolID;
                dic["AcademicYearID"] = AcademicYearID;
                dic["EducationLevelID"] = EducationLevelID;
                dic["AppliedLevel"] = grade;
                dic["Check"] = "check";
                dic["ClassID"] = ClassID;
                dic["PupilCode"] = PupilCode;
                dic["FullName"] = Pupilname;
                if (teacherID > 0)
                {
                    List<int> listClassID = this.GetListClassByHeadTeacher(AcademicYearID, SchoolID, teacherID).Select(p => p.ClassProfileID).ToList();
                    if (listClassID != null && listClassID.Count > 0)
                    {
                        dic["ListClassID"] = listClassID;
                    }
                }
                List<PupilProfileDetail> listPupilProfileDetail = (from poc in PupilOfClassBusiness.Search(dic)
                                                                   join pp in PupilProfileBusiness.All on poc.PupilID equals pp.PupilProfileID
                                                                   join cp in ClassProfileBusiness.All.Where(cp => cp.AcademicYearID == AcademicYearID) on poc.ClassID equals cp.ClassProfileID
                                                                   join ed in EducationLevelBusiness.All on cp.EducationLevelID equals ed.EducationLevelID
                                                                   join sp in SchoolProfileBusiness.All.Where(sp => sp.SchoolProfileID == SchoolID) on poc.SchoolID equals sp.SchoolProfileID
                                                                   where poc.Status != GlobalConstants.PUPIL_STATUS_MOVED_TO_OTHER_CLASS
                                                                   select new PupilProfileDetail
                                                                   {
                                                                       PupilID = poc.PupilID,
                                                                       PupilCode = pp.PupilCode,
                                                                       CurrentAcademicYearID = poc.AcademicYearID,
                                                                       ClassID = poc.ClassID,
                                                                       FullName = pp.FullName,
                                                                       ClassName = cp.DisplayName,
                                                                       SchoolName = sp.SchoolName,
                                                                       EducationLevelID = cp.EducationLevelID,
                                                                       Name = pp.Name,
                                                                       FatherFullName = pp.FatherFullName,
                                                                       MotherFullName = pp.MotherFullName,
                                                                       SponsorFullName = pp.SponsorFullName,
                                                                       FatherMobile = pp.FatherMobile,
                                                                       MotherMobile = pp.MotherMobile,
                                                                       SponsorMobile = pp.SponsorMobile,
                                                                       Genre = pp.Genre,
                                                                       Mobile = pp.Mobile,
                                                                       OrderInClass = poc.OrderInClass,
                                                                       BirthDate = pp.BirthDate,
                                                                       EducationLevelName = ed.Resolution,
                                                                       IsRegisterContract = poc.IsRegisterContract,
                                                                       OrderNumber = cp.OrderNumber.HasValue ? cp.OrderNumber.Value : 0,
                                                                       CurrentStatus = poc.Status,
                                                                       PupilOfClassID = poc.PupilOfClassID
                                                                   }).OrderBy(p => p.EducationLevelID).ThenBy(p => p.OrderNumber).ThenBy(p => p.ClassName).ThenBy(p => p.OrderInClass).ThenBy(p => p.Name).ToList();

                objPupilProfile.listPupilProfileDetail = listPupilProfileDetail;
                objPupilProfile.ValidationCode = WCFConstant.RESPONSE_SUCCESS;
                return objPupilProfile;
            }
            catch (Exception ex)
            {
                LogExtensions.GetCurrentClass = System.Reflection.MethodBase.GetCurrentMethod().DeclaringType;
                string para = string.Format("SchoolID={0}, AcademicYearID={1}, ClassID={2},  EducationLevelID={3}, PupilCode={4}, Pupilname={5}, grade={6}, teacherID={7}", SchoolID, AcademicYearID, ClassID, EducationLevelID, PupilCode, Pupilname, grade, teacherID);
                LogExtensions.ErrorExt(logger, DateTime.Now, para, ex.Message, "null");
                objPupilProfile.ValidationCode = WCFConstant.RESPONSE_EXCEPTION;
                return objPupilProfile;
            }
        }

        #region Lấy danh sách học sinh dưới dạng chuỗi - tối ưu data trả về (AnhVD9)
        /// <summary>
        /// Lay danh sach hoc sinh theo cac thong tin
        /// 
        /// </summary>
        /// <param name="SchoolID"></param>
        /// <param name="AcademicYearID"></param>
        /// <param name="ClassID"></param>
        /// <param name="PupilCode"></param>
        /// <param name="Pupilname"></param>
        /// <param name="EducationLevelID"></param>
        /// <param name="grade"></param>
        /// <param name="teacherID"></param>
        /// <returns>Danh sách dưới dạng chuỗi để giảm kích thước dữ liệu cho response</returns>
        public List<string> GetPupilsformatString(int SchoolID, int AcademicYearID, int ClassID, string PupilCode, string Pupilname, int? EducationLevelID, int grade, int teacherID)
        {
            List<string> pupils = new List<string>();
            try
            {
                DeclareBusiness();
                IDictionary<string, object> dic = new Dictionary<string, object>();
                dic["SchoolID"] = SchoolID;
                dic["AcademicYearID"] = AcademicYearID;
                dic["EducationLevelID"] = EducationLevelID;
                dic["AppliedLevel"] = grade;
                dic["Check"] = "check";
                dic["ClassID"] = ClassID;
                dic["PupilCode"] = PupilCode;
                dic["FullName"] = Pupilname;
                if (teacherID > 0)
                {
                    List<int> listClassID = this.GetListClassByHeadTeacher(AcademicYearID, SchoolID, teacherID).Select(p => p.ClassProfileID).ToList();
                    if (listClassID != null && listClassID.Count > 0)
                    {
                        dic["ListClassID"] = listClassID;
                    }
                }
                IQueryable<PupilProfileDetail> iqPupilDetail = (from poc in PupilOfClassBusiness.Search(dic)
                                                                join pp in PupilProfileBusiness.All on poc.PupilID equals pp.PupilProfileID
                                                                join cp in ClassProfileBusiness.All on poc.ClassID equals cp.ClassProfileID
                                                                join ed in EducationLevelBusiness.All on cp.EducationLevelID equals ed.EducationLevelID
                                                                join sp in SchoolProfileBusiness.All on poc.SchoolID equals sp.SchoolProfileID
                                                                where poc.Status != GlobalConstants.PUPIL_STATUS_MOVED_TO_OTHER_CLASS
                                                                select new PupilProfileDetail
                                                                {
                                                                    PupilID = poc.PupilID,
                                                                    PupilCode = pp.PupilCode,
                                                                    CurrentAcademicYearID = poc.AcademicYearID,
                                                                    ClassID = poc.ClassID,
                                                                    FullName = pp.FullName,
                                                                    ClassName = cp.DisplayName,
                                                                    SchoolName = sp.SchoolName,
                                                                    EducationLevelID = cp.EducationLevelID,
                                                                    Name = pp.Name,
                                                                    FatherFullName = pp.FatherFullName,
                                                                    MotherFullName = pp.MotherFullName,
                                                                    SponsorFullName = pp.SponsorFullName,
                                                                    FatherMobile = pp.FatherMobile,
                                                                    MotherMobile = pp.MotherMobile,
                                                                    SponsorMobile = pp.SponsorMobile,
                                                                    Genre = pp.Genre,
                                                                    Mobile = pp.Mobile,
                                                                    OrderInClass = poc.OrderInClass,
                                                                    BirthDate = pp.BirthDate,
                                                                    EducationLevelName = ed.Resolution,
                                                                    IsRegisterContract = poc.IsRegisterContract,
                                                                    OrderNumber = cp.OrderNumber.HasValue ? cp.OrderNumber.Value : 0,
                                                                    CurrentStatus = poc.Status,
                                                                    PupilOfClassID = poc.PupilOfClassID
                                                                }).OrderBy(p => p.EducationLevelID)
                                                                   .ThenBy(p => p.OrderNumber)
                                                                   .ThenBy(p => p.ClassName)
                                                                   .ThenBy(p => p.OrderInClass)
                                                                   .ThenBy(p => p.Name);
                StringBuilder builder = null;
                foreach (PupilProfileDetail item in iqPupilDetail)
                {
                    builder = new StringBuilder();
                    builder.Append(item.PupilID).Append(DELIMITER_SIGN)
                            .Append(item.PupilCode).Append(DELIMITER_SIGN)
                            .Append(item.FullName).Append(DELIMITER_SIGN)
                            .Append(item.Genre).Append(DELIMITER_SIGN)
                            .Append(item.CurrentStatus).Append(DELIMITER_SIGN)
                            .Append(item.ClassID).Append(DELIMITER_SIGN)
                            .Append(item.ClassName).Append(DELIMITER_SIGN)
                            .Append(item.EducationLevelID).Append(DELIMITER_SIGN)
                            .Append(item.EducationLevelName).Append(DELIMITER_SIGN)

                            .Append(item.FatherMobile).Append(DELIMITER_SIGN)
                            .Append(item.MotherMobile).Append(DELIMITER_SIGN)
                            .Append(item.SponsorMobile).Append(DELIMITER_SIGN)

                            .Append(item.OrderInClass).Append(DELIMITER_SIGN)
                            .Append(item.PupilOfClassID);
                    pupils.Add(builder.ToString());
                }
                return pupils;
            }
            catch (Exception ex)
            {
                LogExtensions.GetCurrentClass = System.Reflection.MethodBase.GetCurrentMethod().DeclaringType;
                string para = string.Format("SchoolID={0}, AcademicYearID={1}, ClassID={2},  EducationLevelID={3}, PupilCode={4}, Pupilname={5}, grade={6}, teacherID={7}", SchoolID, AcademicYearID, ClassID, EducationLevelID, PupilCode, Pupilname, grade, teacherID);
                LogExtensions.ErrorExt(logger, DateTime.Now, para, ex.Message, "null");
                return pupils;
            }
        }
        #endregion
        public ListPupilProfileLiteResponse GetListPupilByClassID(int SchoolID, int AcademicYearID, int? ClassID)
        {
            ListPupilProfileLiteResponse objPupilProfile = new ListPupilProfileLiteResponse();
            try
            {
                DeclareBusiness();

                IDictionary<string, object> dic = new Dictionary<string, object>();
                dic["SchoolID"] = SchoolID;
                dic["AcademicYearID"] = AcademicYearID;
                dic["ClassID"] = ClassID;
                dic["Check"] = "check";

                objPupilProfile.listPupilProfileDetail = (from poc in PupilOfClassBusiness.Search(dic)
                                                          join pp in PupilProfileBusiness.All on poc.PupilID equals pp.PupilProfileID
                                                          join cp in ClassProfileBusiness.All.Where(c => c.AcademicYearID == AcademicYearID) on poc.ClassID equals cp.ClassProfileID
                                                          join ed in EducationLevelBusiness.All on cp.EducationLevelID equals ed.EducationLevelID
                                                          join sp in SchoolProfileBusiness.All.Where(sp => sp.SchoolProfileID == SchoolID) on poc.SchoolID equals sp.SchoolProfileID
                                                          where (poc.Status == GlobalConstants.PUPIL_STATUS_STUDYING)
                                                          && cp.IsActive == true
                                                          select new PupilProfileDetail
                                                          {
                                                              PupilID = poc.PupilID,
                                                              PupilCode = pp.PupilCode,
                                                              CurrentAcademicYearID = poc.AcademicYearID,
                                                              ClassID = poc.ClassID,
                                                              FullName = pp.FullName,
                                                              ClassName = cp.DisplayName,
                                                              SchoolName = sp.SchoolName,
                                                              EducationLevelID = cp.EducationLevelID,
                                                              Name = pp.Name,
                                                              FatherFullName = pp.FatherFullName,
                                                              MotherFullName = pp.MotherFullName,
                                                              SponsorFullName = pp.SponsorFullName,
                                                              FatherMobile = pp.FatherMobile,
                                                              MotherMobile = pp.MotherMobile,
                                                              SponsorMobile = pp.SponsorMobile,
                                                              Genre = pp.Genre,
                                                              Mobile = pp.Mobile,
                                                              OrderInClass = poc.OrderInClass,
                                                              BirthDate = pp.BirthDate,
                                                              EducationLevelName = ed.Resolution
                                                          }).ToList();
                objPupilProfile.ValidationCode = WCFConstant.RESPONSE_SUCCESS;
                return objPupilProfile;
            }
            catch (Exception ex)
            {
                LogExtensions.GetCurrentClass = System.Reflection.MethodBase.GetCurrentMethod().DeclaringType;
                string para = string.Format("SchoolID={0}, AcademicYearID={1}, ClassID={2}", SchoolID, AcademicYearID, ClassID);
                LogExtensions.ErrorExt(logger, DateTime.Now, para, ex.Message, "null");
                objPupilProfile.ValidationCode = WCFConstant.RESPONSE_EXCEPTION;
                return objPupilProfile;
            }
        }

        /// <summary>
        /// Lấy thông tin rèn luyện của học sinh
        /// </summary>
        /// <param name="SchoolID"></param>
        /// <param name="AcademicYearID"></param>
        /// <param name="ClassID"></param>
        /// <returns></returns>
        public ListPupilTrainingResponse GetListTraningInfoToday(int SchoolID, int AcademicYearID, int ClassID)
        {
            ListPupilTrainingResponse objResultResponse = new ListPupilTrainingResponse();
            DateTime DateTimeNow = DateTime.Now;
            try
            {
                //Khoi tao Business
                DeclareBusiness();
                List<PupilTrainingDefault> listPupilTraining = new List<PupilTrainingDefault>();
                #region Thông tin điểm danh
                //lay len thong tin diem danh cua lop
                IDictionary<string, object> dicAbsent = new Dictionary<string, object>();
                dicAbsent["ClassID"] = ClassID;
                dicAbsent["AcademicYearID"] = AcademicYearID;
                dicAbsent["AbsentDate"] = DateTimeNow;
                List<PupilAbsence> listPupilAbsence = PupilAbsenceBusiness.SearchBySchool(SchoolID, dicAbsent).OrderBy(p => p.Section).ToList();
                List<int> listPupilID = listPupilAbsence.Select(o => o.PupilID).Distinct().ToList();
                Dictionary<int, string> dicPupilContent = new Dictionary<int, string>();
                List<PupilAbsence> lstAbsenceByPupil = null;
                //Tao content cho object
                foreach (int pupilID in listPupilID)
                {
                    lstAbsenceByPupil = listPupilAbsence.Where(o => o.PupilID == pupilID).ToList();
                    string absenceInfo = string.Empty;
                    foreach (var item in lstAbsenceByPupil)
                    {
                        string reason = "không phép";
                        if (item.IsAccepted.HasValue && item.IsAccepted.Value)
                        {
                            reason = "có phép";
                        }
                        absenceInfo += WCFUtils.GetSection(item.Section) + ": Nghỉ học " + reason + "\n";
                    }
                    dicPupilContent[pupilID] = absenceInfo;
                }
                #endregion

                #region Thông tin vi phạm
                //Loc thong tin vi pham cua lop
                IDictionary<string, object> dicFault = new Dictionary<string, object>();
                dicFault["ClassID"] = ClassID;
                dicFault["AcademicYearID"] = AcademicYearID;
                dicFault["ViolatedDate"] = DateTimeNow;
                List<PupilFault> lstPupilFault = PupilFaultBusiness.SearchBySchool(SchoolID, dicFault).ToList();
                //Loc thong tin hoc sinh cua mot lop
                IDictionary<string, object> dicPupil = new Dictionary<string, object>();
                dicPupil["ClassID"] = ClassID;
                dicPupil["AcademicYearID"] = AcademicYearID;
                List<PupilProfileBO> LsPupilProfile = PupilProfileBusiness.SearchBySchool(SchoolID, dicPupil).ToList();

                var listFault = (from p in lstPupilFault
                                 join pp in LsPupilProfile on p.PupilID equals pp.PupilProfileID
                                 select new
                                 {
                                     PupilID = p.PupilID,
                                     FaultName = p.FaultCriteria.Resolution,
                                     NumberOfFault = p.NumberOfFault,
                                     Note = p.Note
                                 }).Distinct().ToList().GroupBy(u => new { u.PupilID })
                                   .Select(u => new
                                   {
                                       PupilID = u.Key.PupilID,
                                       FaultName = (u.Select(v => v.FaultName + " " + v.Note + (string.IsNullOrEmpty(v.Note) ? string.Empty : " ") + (v.NumberOfFault.HasValue && v.NumberOfFault.Value > 1 ? "(" + v.NumberOfFault + ")" : string.Empty))).Aggregate((a, b) => (a + ", " + b))
                                   }).ToList();


                foreach (var f in listFault)
                {
                    if (dicPupilContent.ContainsKey(f.PupilID))
                    {
                        // 20152219 AnhVD9 - Bỏ từ Vi phạm
                        dicPupilContent[f.PupilID] = dicPupilContent[f.PupilID] + f.FaultName;
                    }
                    else
                    {
                        dicPupilContent[f.PupilID] = f.FaultName;
                    }
                }
                listPupilID = dicPupilContent.Keys.ToList();
                foreach (int pupilID in listPupilID)
                {
                    PupilTrainingDefault ppic = new PupilTrainingDefault();
                    ppic.PupilProfileID = pupilID;
                    ppic.Content = dicPupilContent[pupilID];
                    listPupilTraining.Add(ppic);
                }

                #endregion

                //gan validationCode thanh cong
                objResultResponse.ValidationCode = WCFConstant.RESPONSE_SUCCESS;
                //gan gia tri thuoc tinh dinh kem 
                objResultResponse.LstPupilTrainingDefault = listPupilTraining;
            }
            catch (Exception ex)
            {
                LogExtensions.GetCurrentClass = System.Reflection.MethodBase.GetCurrentMethod().DeclaringType;
                string para = string.Format("SchoolID={0}, AcademicYearID={1}, ClassID={2}", SchoolID, AcademicYearID, ClassID);
                LogExtensions.ErrorExt(logger, DateTime.Now, para, ex.Message, "null");
                //gan validationCode Exception
                objResultResponse.ValidationCode = WCFConstant.RESPONSE_EXCEPTION;
            }
            return objResultResponse;
        }

        #region lay thong tin chuyen can tuan/thang/mo rong cho hoc sinh
        /// <summary>
        /// lay thong tin chuyen can tuan/thang/mo rong cho hoc sinh
        /// </summary>
        /// <author date="14/04/10">HaiVT</author>
        /// <param name="SchoolID">id truong</param>
        /// <param name="AcademicYearID">id nam hoc</param>
        /// <param name="ClassID">id lop hoc</param>
        /// <param name="startTime">thoi gian bat dau</param>
        /// <param name="endTime">thoi gian ket thuc</param>
        /// <returns></returns>
        public ListPupilTrainingResponse GetListTraningInfoByTime(int SchoolID, int AcademicYearID, int ClassID, DateTime startTime, DateTime endTime)
        {
            ListPupilTrainingResponse objResultResponse = new ListPupilTrainingResponse();
            DateTime DateTimeNow = DateTime.Now;
            try
            {
                //Khoi tao Business
                DeclareBusiness();
                List<PupilTrainingDefault> listPupilTraining = new List<PupilTrainingDefault>();
                #region Thông tin điểm danh/ vi pham
                //lay len thong tin diem danh cua lop
                IDictionary<string, object> dicAbsent = new Dictionary<string, object>();
                dicAbsent["ClassID"] = ClassID;
                dicAbsent["AcademicYearID"] = AcademicYearID;
                // thong tin diem danh cua hoc sinh
                List<PupilAbsence> lstAbsence = PupilAbsenceBusiness.SearchBySchool(SchoolID, dicAbsent)
                    .Where(p => EntityFunctions.TruncateTime(p.AbsentDate) >= EntityFunctions.TruncateTime(startTime)
                        && EntityFunctions.TruncateTime(p.AbsentDate) <= EntityFunctions.TruncateTime(endTime)).ToList();

                // thong tin vi pham cua hoc sinh
                var lstFault = (from f in PupilFaultBusiness.AllNoTracking
                                join lf in FaultCriteriaBusiness.AllNoTracking on f.FaultID equals lf.FaultCriteriaID
                                join fg in FaultGroupBusiness.AllNoTracking on lf.GroupID equals fg.FaultGroupID
                                where f.SchoolID == SchoolID
                                && f.AcademicYearID == AcademicYearID
                                && EntityFunctions.TruncateTime(f.ViolatedDate) >= EntityFunctions.TruncateTime(startTime)
                                && EntityFunctions.TruncateTime(f.ViolatedDate) <= EntityFunctions.TruncateTime(endTime)
                                select new FaultByTime
                                {
                                    PupilID = f.PupilID,
                                    FaultID = f.FaultID,
                                    NumberOfFault = f.NumberOfFault,
                                    Resolution = lf.Resolution,
                                    Note = f.Note
                                }).ToList();

                List<int> pupilIds = new List<int>();
                if (lstAbsence != null && lstAbsence.Count > 0)
                {
                    pupilIds = lstAbsence.Select(p => p.PupilID).Distinct().ToList();
                }

                if (lstFault != null && lstFault.Count > 0)
                {
                    pupilIds.AddRange(lstFault.Select(p => p.PupilID).Distinct().ToList());
                    pupilIds = pupilIds.Distinct().ToList();
                }

                List<PupilAbsence> itemAbsences = null;

                //Tao content cho object
                int pupilID = 0;
                string content = string.Empty;
                int cp = 0, kp = 0;
                PupilTrainingDefault itemResponse = null;

                for (int i = pupilIds.Count - 1; i >= 0; i--)
                {
                    content = string.Empty;
                    itemResponse = new PupilTrainingDefault();
                    pupilID = pupilIds[i];
                    // diem danh
                    itemAbsences = lstAbsence.Where(o => o.PupilID == pupilID).ToList();
                    if (itemAbsences != null && itemAbsences.Count > 0)
                    {
                        cp = itemAbsences.Where(p => p.IsAccepted == true).Count();
                        kp = itemAbsences.Where(p => p.IsAccepted != true).Count();
                        if (cp > 0)
                        {
                            content += string.Format(WCFConstant.FORMAT_0_1, string.Format(FORMAT_ABSENCE_P_0, cp), Environment.NewLine);
                        }

                        if (kp > 0)
                        {
                            content += string.Format(WCFConstant.FORMAT_0_1, string.Format(FORMAT_ABSENCE_K_0, kp), Environment.NewLine);
                        }
                    }

                    //Vi pham
                    var itemFaults = lstFault.Where(a => a.PupilID == pupilID).GroupBy(l => new { l.FaultID, l.Resolution })
                            .Select(g => new
                            {
                                FaultID = g.Key.FaultID,
                                Resolution = g.Key.Resolution,
                                Note = string.Join(", ", g.Where(o => !string.IsNullOrWhiteSpace(o.Note))
                                    .Select(x => x.Note).Distinct()),
                                Number = g.Sum(x => x.NumberOfFault)
                            }).ToList();


                    if (itemFaults != null && itemFaults.Count > 0)
                    {
                        for (int ik = 0, iksize = itemFaults.Count; ik < iksize; ik++)
                        {
                            var itemFaulst = itemFaults[ik];
                            if (itemFaulst.Number > 1)
                            {
                                content += string.Format("{0}{1} ({2}), ", string.IsNullOrEmpty(itemFaulst.Resolution) ? string.Empty : itemFaulst.Resolution, string.IsNullOrEmpty(itemFaulst.Note) ? string.Empty : " " + itemFaulst.Note, itemFaulst.Number);
                            }
                            else
                            {
                                content += string.Format("{0}{1}, ", string.IsNullOrEmpty(itemFaulst.Resolution) ? string.Empty : itemFaulst.Resolution, string.IsNullOrEmpty(itemFaulst.Note) ? string.Empty : " " + itemFaulst.Note);
                            }


                        }

                        // cat bo dau phay
                        content = content.Substring(0, content.Length - 2);
                    }

                    itemResponse.PupilProfileID = pupilID;
                    itemResponse.Content = content;
                    listPupilTraining.Add(itemResponse);
                }
                #endregion

                //gan validationCode thanh cong
                objResultResponse.ValidationCode = WCFConstant.RESPONSE_SUCCESS;
                //gan gia tri thuoc tinh dinh kem 
                objResultResponse.LstPupilTrainingDefault = listPupilTraining;
            }
            catch
            {
                //gan validationCode Exception
                objResultResponse.ValidationCode = WCFConstant.RESPONSE_EXCEPTION;
            }
            return objResultResponse;
        }
        #endregion

        #region lay thong tin vi pham, nghi hoc, mien giam cua hoc sinh
        /// <summary>
        /// lay thong tin vi pham, nghi hoc, mien giam cua hoc sinh
        /// </summary>
        /// <author date="2014/02/28">HaiVT</author>
        /// <param name="schoolID">id truong</param>
        /// <param name="classID">id lop</param>
        /// <param name="academicYearID">id nam hoc</param>
        /// <param name="pupilID">id hoc sinh</param>
        /// <returns></returns>
        public PupilFaultAbsenceExemptResponse GetPupilFaultAbsenceExcempt(int schoolID, int classID, int academicYearID, int pupilID)
        {
            PupilFaultAbsenceExemptResponse response = new PupilFaultAbsenceExemptResponse();
            DateTime DateTimeNow = DateTime.Now;
            try
            {
                //Khoi tao Business
                DeclareBusiness();
                #region Thông tin vi phạm
                IDictionary<string, object> dic = new Dictionary<string, object>();
                //dic["ClassID"] = classID;
                dic["AcademicYearID"] = academicYearID;
                dic["PupilID"] = pupilID;
                List<PupilFault> pupilFault = (from p in PupilFaultBusiness.All
                                               where p.PupilID == pupilID
                                               && p.AcademicYearID == academicYearID
                                               orderby p.ViolatedDate descending
                                               select p).ToList();
                //PupilFaultBusiness.SearchBySchool(schoolID, dic).OrderByDescending(p => p.ViolatedDate).ToList();
                #endregion

                #region Thông tin điểm danh
                List<PupilAbsence> pupilAbsence = (from p in PupilAbsenceBusiness.All
                                                   where p.PupilID == pupilID
                                                   && p.AcademicYearID == academicYearID
                                                   orderby p.AbsentDate descending
                                                   select p).ToList();
                //PupilAbsenceBusiness.SearchBySchool(schoolID, dic).OrderByDescending(p => p.AbsentDate).ToList();
                #endregion

                #region thong tin mien giam
                List<ExemptedSubject> pupilExempt = (from p in ExemptedSubjectBusiness.All
                                                     where p.PupilID == pupilID
                                                     && p.AcademicYearID == academicYearID
                                                     orderby p.SubjectCat.OrderInSubject
                                                     select p).ToList();
                //ExemptedSubjectBusiness.SearchBySchool(schoolID, dic).OrderBy(p => p.SubjectCat.OrderInSubject).ToList();
                #endregion

                // tao list response
                int countFault = pupilFault.Count;
                int countAbsence = pupilAbsence.Count;
                int countExempt = pupilExempt.Count;
                int count = Math.Max(countFault, countAbsence);
                count = Math.Max(count, countAbsence);
                List<PupilFaultAbsenceExempt> pupilFaultAbsenceExemptList = new List<PupilFaultAbsenceExempt>();
                PupilFaultAbsenceExempt itemResponse = null;
                PupilFault itemFault = null;
                PupilAbsence itemAbsence = null;
                ExemptedSubject itemExempt = null;
                List<FaultCriteria> faultCriteria = null;
                if (countFault > 0)
                {
                    faultCriteria = (from p in pupilFault
                                     select new FaultCriteria
                                     {
                                         FaultCriteriaID = p.FaultID,
                                         Resolution = p.FaultCriteria.Resolution
                                     }).ToList();
                }

                for (int i = 0; i < count; i++)
                {
                    itemResponse = new PupilFaultAbsenceExempt();
                    if (countFault > i)
                    {
                        itemFault = pupilFault[i];
                        itemResponse.PupilProfileID = itemFault.PupilID;
                        itemResponse.FaultName = faultCriteria.Where(p => p.FaultCriteriaID == itemFault.FaultID).Select(p => p.Resolution).FirstOrDefault();
                        itemResponse.NumberOfFault = itemFault.NumberOfFault != null ? itemFault.NumberOfFault.Value : 0;
                        itemResponse.FaultDate = itemFault.ViolatedDate.ToString("dd/MM/yyyy");
                    }

                    if (countAbsence > i)
                    {
                        itemAbsence = pupilAbsence[i];
                        itemResponse.AbsenceDate = itemAbsence.AbsentDate.ToString("dd/MM/yyyy");
                        itemResponse.AbsenceSection = itemAbsence.Section;
                        itemResponse.AbsenceType = itemAbsence.IsAccepted.HasValue && itemAbsence.IsAccepted.Value ? "P" : "K";
                    }

                    if (countExempt > i)
                    {
                        itemExempt = pupilExempt[i];
                        itemResponse.ExemptName = itemExempt.SubjectCat.SubjectName;
                        itemResponse.ExemptFirstSemester = itemExempt.FirstSemesterExemptType.HasValue ? itemExempt.FirstSemesterExemptType.Value == 1 ? "Miễn thực hành" : "Miễn toàn phần" : string.Empty;
                        itemResponse.ExemptSecondSemester = itemExempt.SecondSemesterExemptType.HasValue ? itemExempt.SecondSemesterExemptType.Value == 1 ? "Miễn thực hành" : "Miễn toàn phần" : string.Empty;
                    }

                    pupilFaultAbsenceExemptList.Add(itemResponse);
                }

                //gan validationCode thanh cong
                response.ValidationCode = WCFConstant.RESPONSE_SUCCESS;
                //gan gia tri thuoc tinh dinh kem 
                response.PupilFaultAbsenceExempt = pupilFaultAbsenceExemptList;
            }
            catch (Exception ex)
            {
                LogExtensions.GetCurrentClass = System.Reflection.MethodBase.GetCurrentMethod().DeclaringType;
                string para = string.Format("schoolID={0}, classID={1}, academicYearID={2}, pupilID={3}", schoolID, classID, academicYearID, pupilID);
                LogExtensions.ErrorExt(logger, DateTime.Now, para, ex.Message, "null");
                //gan validationCode Exception
                response.ValidationCode = WCFConstant.RESPONSE_EXCEPTION;
            }
            return response;
        }
        #endregion

        /// <summary>
        /// Lấy ds học sinh trong khối theo đk đầu vào của những học sinh đang học
        /// </summary>
        /// <param name="ChoolID"></param>
        /// <param name="AcademicYearID"></param>
        /// <param name="EducationID"></param>
        /// <returns></returns>
        public ListPupilByEducationLevelResponse GetListPupilByEducationLevelID(int SchoolID, int AcademicYearID, int EducationLevelID)
        {
            ListPupilByEducationLevelResponse objResultResponse = new ListPupilByEducationLevelResponse();
            try
            {
                //Khoi tao Business
                DeclareBusiness();
                List<PupilByEducationLevel> listPupil = (from poc in PupilOfClassBusiness.All
                                                         join pp in PupilProfileBusiness.All on poc.PupilID equals pp.PupilProfileID
                                                         join cp in ClassProfileBusiness.AllNoTracking on poc.ClassID equals cp.ClassProfileID
                                                         where poc.SchoolID == SchoolID && poc.AcademicYearID == AcademicYearID
                                                         && poc.ClassProfile.EducationLevelID == EducationLevelID
                                                         && cp.AcademicYearID == AcademicYearID
                                                         && (poc.Status == GlobalConstants.PUPIL_STATUS_STUDYING
                                                            || poc.Status == GlobalConstants.PUPIL_STATUS_GRADUATED)
                                                         && pp.IsActive
                                                         && cp.IsActive == true
                                                         select new PupilByEducationLevel
                                                         {
                                                             PupilProfileID = poc.PupilID,
                                                             ClassID = poc.ClassID,
                                                             ClassName = poc.ClassProfile.DisplayName
                                                         }).ToList();
                //gan validationCode thanh cong
                objResultResponse.ValidationCode = WCFConstant.RESPONSE_SUCCESS;
                //gan gia tri thuoc tinh dinh kem 
                objResultResponse.LstPupilByEducationLevel = listPupil;
            }
            catch
            {
                //gan validationCode Exception
                objResultResponse.ValidationCode = WCFConstant.RESPONSE_EXCEPTION;
            }
            return objResultResponse;
        }

        /// <summary>
        /// Trả về list hoc sinh, order theo OrderNumber của ClassProfile, ClassName, OrderInClass của PupilOfClass, FullName học sinh (giống hồ sơ học sinh).
        /// PupilProfileID: ID học sinh
        /// FullName: Tên học sinh
        /// ClassName: Tên lớp
        /// </summary>
        /// <param name="listPupilProfileID"></param>
        /// <param name="SchoolID"></param>
        /// <param name="AcademicYearID"></param>
        /// <param name="EducationLevelID"></param>
        /// <returns></returns>
        public ListPupilProfileClassResponse GetListPupilByListPupilProfileID(List<int> listPupilProfileID, int SchoolID, int AcademicYearID)
        {
            ListPupilProfileClassResponse objResultResponse = new ListPupilProfileClassResponse();
            try
            {
                //Khoi tao Business
                DeclareBusiness();
                List<PupilProfileClass> listPupil = (from poc in PupilOfClassBusiness.All
                                                     join c in ClassProfileBusiness.All on poc.ClassID equals c.ClassProfileID
                                                     join pp in PupilProfileBusiness.All on poc.PupilID equals pp.PupilProfileID
                                                     where poc.SchoolID == SchoolID
                                                     && poc.AcademicYearID == AcademicYearID
                                                     && poc.Status == GlobalConstants.PUPIL_STATUS_STUDYING
                                                     && pp.IsActive
                                                     && c.IsActive == true
                                                     && listPupilProfileID.Contains(poc.PupilID)
                                                     orderby (c.OrderNumber.HasValue ? c.OrderNumber : 0),
                                                     c.DisplayName,
                                                     poc.OrderInClass,
                                                     pp.Name,
                                                     pp.FullName
                                                     select new PupilProfileClass
                                                     {
                                                         PupilProfileID = poc.PupilID,
                                                         FullName = pp.FullName,
                                                         ClassName = c.DisplayName
                                                     }).ToList();
                //gan validationCode thanh cong
                objResultResponse.ValidationCode = WCFConstant.RESPONSE_SUCCESS;
                //gan gia tri thuoc tinh dinh kem 
                objResultResponse.LstPupilProfileClass = listPupil;
            }
            catch
            {
                //gan validationCode Exception
                objResultResponse.ValidationCode = WCFConstant.RESPONSE_EXCEPTION;
            }
            return objResultResponse;
        }

        #region lay thong tin hoc sinh gom id + ten ho theo id hoc sinh truyen vao khong quan tam den trang thai hoc sinh, nam hoc
        /// <summary>
        /// lay thong tin hoc sinh gom id + ten ho theo id hoc sinh truyen vao khong quan tam den trang thai hoc sinh, nam hoc
        /// </summary>
        /// <author date="2014/02/25">HaiVT</author>
        /// <param name="pupilProfileList">list id hoc sinh</param>
        /// <returns></returns>
        public ListPupilProfileClassResponse GetListPupilByListID(List<int> pupilProfileList)
        {
            ListPupilProfileClassResponse objResultResponse = new ListPupilProfileClassResponse();
            try
            {
                //Khoi tao Business
                DeclareBusiness();
                List<PupilProfileClass> listPupil = (from pp in PupilProfileBusiness.All
                                                     where pp.IsActive
                                                     && pupilProfileList.Contains(pp.PupilProfileID)
                                                     select new PupilProfileClass
                                                     {
                                                         PupilProfileID = pp.PupilProfileID,
                                                         ClassID = pp.CurrentClassID,
                                                         FullName = pp.FullName,
                                                         Status = pp.ProfileStatus
                                                     }).ToList();
                //gan validationCode thanh cong
                objResultResponse.ValidationCode = WCFConstant.RESPONSE_SUCCESS;
                //gan gia tri thuoc tinh dinh kem 
                objResultResponse.LstPupilProfileClass = listPupil;
            }
            catch (Exception ex)
            {
                LogExtensions.GetCurrentClass = System.Reflection.MethodBase.GetCurrentMethod().DeclaringType;
                string para = "Null";
                LogExtensions.ErrorExt(logger, DateTime.Now, para, ex.Message, "null");
                //gan validationCode Exception
                objResultResponse.ValidationCode = WCFConstant.RESPONSE_EXCEPTION;
            }

            return objResultResponse;
        }
        #endregion

        #region lay danh sach hoc sinh + giao vien chu nhiem theo nam hoc truyen vao, neu co giao vien lam thay thi lay lam thay
        /// <summary>
        /// lay danh sach hoc sinh + giao vien chu nhiem theo nam hoc truyen vao, neu co giao vien lam thay thi lay lam thay
        /// </summary>
        /// <author date="2014/02/25">HaiVT</author>
        /// <param name="pupilProfileList">list id hoc sinh</param>
        /// <returns></returns>
        public ListPupilProfileClassResponse GetListPupilSendSMS(List<int> pupilProfileList, int academicYearID)
        {
            ListPupilProfileClassResponse objResultResponse = new ListPupilProfileClassResponse();
            try
            {
                //Khoi tao Business
                DeclareBusiness();
                int numOfSplit = 2000;
                List<PupilProfileClass> listPupil = new List<PupilProfileClass>();
                List<PupilProfileClass> listPupiltemp = null;
                DateTime dateTimeLast = DateTime.Now.Date;
                List<int> pupilProfileIDListTemp = new List<int>();
                if (pupilProfileList.Count > numOfSplit)
                {
                    int numOfPupil = (int)Math.Ceiling((decimal)pupilProfileList.Count / numOfSplit);
                    for (int i = 0; i < numOfPupil; i++)
                    {
                        listPupiltemp = new List<PupilProfileClass>();
                        pupilProfileIDListTemp = pupilProfileList.Skip((i) * numOfSplit).Take(numOfSplit).ToList();
                        var iq = from pp in PupilProfileBusiness.All
                                 join poc in PupilOfClassBusiness.All on pp.PupilProfileID equals poc.PupilID
                                 join cp in ClassProfileBusiness.All on poc.ClassID equals cp.ClassProfileID
                                 join hts in HeadTeacherSubstitutionBusiness.All on
                                    new { ClassID = cp.ClassProfileID } equals new { ClassID = hts.ClassID } into hts_l
                                 from hts_j in hts_l.Where(p => p.AssignedDate <= dateTimeLast && dateTimeLast <= p.EndDate && p.AcademicYearID == academicYearID).DefaultIfEmpty()
                                 where pp.IsActive
                                 && pupilProfileIDListTemp.Contains(pp.PupilProfileID)
                                 && poc.AcademicYearID == academicYearID
                                 && cp.AcademicYearID == academicYearID
                                 && (poc.Status == GlobalConstants.PUPIL_STATUS_STUDYING || poc.Status == GlobalConstants.PUPIL_STATUS_GRADUATED)
                                 select new PupilProfileClass
                                 {
                                     PupilProfileID = pp.PupilProfileID,
                                     FullName = pp.FullName,
                                     HeadTeacherID = hts_j.SubstituedHeadTeacher > 0 ? hts_j.SubstituedHeadTeacher : cp.HeadTeacherID.HasValue ? cp.HeadTeacherID.Value : 0
                                 };
                        listPupiltemp = iq.ToList();

                        listPupil.AddRange(listPupiltemp);
                    }

                }
                else
                {
                    var iq = from pp in PupilProfileBusiness.All
                             join poc in PupilOfClassBusiness.All on pp.PupilProfileID equals poc.PupilID
                             join cp in ClassProfileBusiness.All on poc.ClassID equals cp.ClassProfileID
                             join hts in HeadTeacherSubstitutionBusiness.All on
                                new { ClassID = cp.ClassProfileID } equals new { ClassID = hts.ClassID } into hts_l
                             from hts_j in hts_l.Where(p => p.AssignedDate <= dateTimeLast && dateTimeLast <= p.EndDate && p.AcademicYearID == academicYearID).DefaultIfEmpty()
                             where pp.IsActive
                             && pupilProfileList.Contains(pp.PupilProfileID)
                             && poc.AcademicYearID == academicYearID
                             && cp.AcademicYearID == academicYearID
                             && (poc.Status == GlobalConstants.PUPIL_STATUS_STUDYING || poc.Status == GlobalConstants.PUPIL_STATUS_GRADUATED)
                             select new PupilProfileClass
                             {
                                 PupilProfileID = pp.PupilProfileID,
                                 FullName = pp.FullName,
                                 HeadTeacherID = hts_j.SubstituedHeadTeacher > 0 ? hts_j.SubstituedHeadTeacher : cp.HeadTeacherID.HasValue ? cp.HeadTeacherID.Value : 0
                             };
                    listPupil = iq.ToList();
                }

                //gan validationCode thanh cong
                objResultResponse.ValidationCode = WCFConstant.RESPONSE_SUCCESS;
                //gan gia tri thuoc tinh dinh kem 
                objResultResponse.LstPupilProfileClass = listPupil;
            }
            catch (Exception ex)
            {
                LogExtensions.GetCurrentClass = System.Reflection.MethodBase.GetCurrentMethod().DeclaringType;
                string para = string.Format("academicYearId={0}", academicYearID);
                LogExtensions.ErrorExt(logger, DateTime.Now, para, ex.Message, "null");
                //gan validationCode Exception
                objResultResponse.ValidationCode = WCFConstant.RESPONSE_EXCEPTION;
            }

            return objResultResponse;
        }
        #endregion

        #region kiem tra co ton tai hoc sinh voi nam duoc chon hay khong
        /// <summary>
        /// kiem tra co ton tai hoc sinh voi nam duoc chon hay khong        
        /// </summary>
        /// <author date="2014/02/18">HaiVT</author>
        /// <param name="schoolID">id truong</param>
        /// <param name="pupilID">id hoc sinh</param>
        /// <param name="year">nam hoc</param>
        /// <returns>Neu ton tai tra ve True, nguoc lai la False</returns>
        public BoolResultResponse CheckPupilWithYear(int schoolID, int pupilID, int year)
        {
            BoolResultResponse result = new BoolResultResponse();
            try
            {
                //Khoi tao Business
                DeclareBusiness();
                IDictionary<string, object> dic = new Dictionary<string, object>();
                // kiem tra nam hoc va truong
                dic["SchoolID"] = schoolID;
                dic["Year"] = year;
                AcademicYear academicYear = AcademicYearBusiness.Search(dic).FirstOrDefault();
                if (academicYear != null)
                {

                }
                dic["PupilID"] = pupilID;
                dic["AcademicYearID"] = academicYear.AcademicYearID;
                dic["Status"] = GlobalConstants.PUPIL_STATUS_STUDYING;
                if (PupilOfClassBusiness.Search(dic).Count() > 0)
                {
                    result.Result = true;
                }
                //gan validationCode thanh cong
                result.ValidationCode = WCFConstant.RESPONSE_SUCCESS;
            }
            catch
            {
                //gan validationCode Exception
                result.ValidationCode = WCFConstant.RESPONSE_EXCEPTION;
            }
            return result;
        }
        #endregion

        /// <summary>
        /// Lấy thông tin học sinh dang hoc, chuyen truong, bo hoc, tot nghiep va thông tin lớp học và năm học.
        /// </summary>
        /// <param name="pupilID">ID của học sinh</param>
        /// <param name="schoolID">ID của trường</param>
        /// <param name="year">Năm học</param>
        /// <returns>Thông tin học sinh cùng thông tin lớp học và năm học.</returns>
        public PupilProfileForSelectPupilView FindPupilProfile(int pupilID, int schoolID, int year)
        {
            PupilProfileForSelectPupilView result = new PupilProfileForSelectPupilView();
            try
            {
                //Khoi tao Business
                DeclareBusiness();
                IDictionary<string, object> dic = new Dictionary<string, object>();
                dic["SchoolID"] = schoolID;
                dic["Year"] = year;
                AcademicYear academicYear = this.AcademicYearBusiness.Search(dic).FirstOrDefault();
                if (academicYear == null) return null;

                var poc = PupilOfClassBusiness.AllNoTracking
                                .Where(p => p.PupilID == pupilID
                                    && p.SchoolID == schoolID && p.AcademicYearID == academicYear.AcademicYearID
                                    && p.Status != GlobalConstants.PUPIL_STATUS_MOVED_TO_OTHER_CLASS)
                                .FirstOrDefault();
                if (poc != null)
                {
                    result = new PupilProfileForSelectPupilView
                    {
                        PupilID = poc.PupilProfile.PupilProfileID,
                        PupilName = poc.PupilProfile.FullName,
                        PupilGenre = poc.PupilProfile.Genre,
                        PupilBrithday = poc.PupilProfile.BirthDate,
                        PupilCode = poc.PupilProfile.PupilCode,
                        AcademicID = academicYear.AcademicYearID,
                        AcademicYear = academicYear.Year,
                        AcademicStartDateOfSemester1 = academicYear.FirstSemesterStartDate.Value,
                        AcademicDueDateOfSemester1 = academicYear.FirstSemesterEndDate.Value,
                        AcademicStartDateOfSemester2 = academicYear.SecondSemesterStartDate.Value,
                        AcademicDueDateOfSemester2 = academicYear.SecondSemesterEndDate.Value,
                        ClassID = poc.ClassProfile.ClassProfileID,
                        ClassName = poc.ClassProfile.DisplayName,
                        SchoolID = academicYear.SchoolProfile.SchoolProfileID,
                        SchoolName = academicYear.SchoolProfile.SchoolName,
                        Status = poc.Status,
                        SchoolPhoneNumber = academicYear.SchoolProfile.Telephone,
                        HeadMasterName = academicYear.SchoolProfile.HeadMasterName,
                        HeadMasterPhone = academicYear.SchoolProfile.HeadMasterPhone,
                        Address = academicYear.SchoolProfile.Address,
                        ProvinceID = academicYear.SchoolProfile.ProvinceID,
                        ProvinceName = academicYear.SchoolProfile.Province.ProvinceName,
                        DistrictID = academicYear.SchoolProfile.DistrictID,
                        DistrictName = academicYear.SchoolProfile.District.DistrictName,
                        CommuneID = academicYear.SchoolProfile.CommuneID,
                        CommuneName = academicYear.SchoolProfile.CommuneID > 0 ? academicYear.SchoolProfile.Commune.CommuneName : ""
                    };
                }
            }
            catch (Exception ex)
            {
                LogExtensions.GetCurrentClass = System.Reflection.MethodBase.GetCurrentMethod().DeclaringType;
                string para = string.Format("pupilID={0}, schoolID={1},year={2}", pupilID, schoolID, year);
                LogExtensions.ErrorExt(logger, DateTime.Now, para, ex.Message, "null");
            }
            return result;
        }

        /// <summary>
        /// Lấy danh sách thông tin học sinh cho SParent
        /// </summary>
        /// <param name="lstPupilID"></param>
        /// <param name="lstSchoolID"></param>
        /// <param name="lstYear"></param>
        /// <returns></returns>
        public ListPupilView FindListPupilProfile(List<int> lstPupilID, List<int> lstSchoolID, List<int> lstYear)
        {
            ListPupilView result = new ListPupilView();
            try
            {
                DeclareBusiness();
                int schoolID, pupilID, year;
                List<PupilProfileForSelectPupilView> lstView = new List<PupilProfileForSelectPupilView>();
                PupilProfileForSelectPupilView obj = null;
                for (int i = 0; i < lstPupilID.Count; i++)
                {
                    schoolID = lstSchoolID[i];
                    pupilID = lstPupilID[i];
                    year = lstYear[i];
                    //chiendd1: 04/05/2016. sua lai cach lay du lieu
                    AcademicYear academicYear = AcademicYearBusiness.All.FirstOrDefault(o => o.SchoolID == schoolID && o.Year == year && o.IsActive == true);
                    if (academicYear == null) continue;

                    var poc = PupilOfClassBusiness.AllNoTracking.FirstOrDefault(p => p.PupilID == pupilID
                                            && p.SchoolID == schoolID && p.AcademicYearID == academicYear.AcademicYearID
                                            && p.Status != GlobalConstants.PUPIL_STATUS_MOVED_TO_OTHER_CLASS);
                    if (poc != null)
                    {
                        obj = new PupilProfileForSelectPupilView
                        {
                            PupilID = poc.PupilProfile.PupilProfileID,
                            PupilName = poc.PupilProfile.FullName,
                            PupilGenre = poc.PupilProfile.Genre,
                            PupilBrithday = poc.PupilProfile.BirthDate,
                            PupilCode = poc.PupilProfile.PupilCode,
                            AcademicID = academicYear.AcademicYearID,
                            AcademicYear = academicYear.Year,
                            AcademicStartDateOfSemester1 = academicYear.FirstSemesterStartDate.Value,
                            AcademicDueDateOfSemester1 = academicYear.FirstSemesterEndDate.Value,
                            AcademicStartDateOfSemester2 = academicYear.SecondSemesterStartDate.Value,
                            AcademicDueDateOfSemester2 = academicYear.SecondSemesterEndDate.Value,
                            ClassID = poc.ClassProfile.ClassProfileID,
                            ClassName = poc.ClassProfile.DisplayName,
                            SchoolID = academicYear.SchoolProfile.SchoolProfileID,
                            SchoolName = academicYear.SchoolProfile.SchoolName,
                            Status = poc.Status,
                            SchoolPhoneNumber = academicYear.SchoolProfile.Telephone,
                            HeadMasterName = academicYear.SchoolProfile.HeadMasterName,
                            HeadMasterPhone = academicYear.SchoolProfile.HeadMasterPhone,
                            Address = academicYear.SchoolProfile.Address,
                            ProvinceID = academicYear.SchoolProfile.ProvinceID,
                            ProvinceName = academicYear.SchoolProfile.Province.ProvinceName,
                            DistrictID = academicYear.SchoolProfile.DistrictID,
                            DistrictName = academicYear.SchoolProfile.District.DistrictName,
                            CommuneID = academicYear.SchoolProfile.CommuneID,
                            CommuneName = academicYear.SchoolProfile.CommuneID > 0 ? academicYear.SchoolProfile.Commune.CommuneName : ""
                        };

                        academicYear = AcademicYearBusiness.All.FirstOrDefault(o => o.SchoolID == schoolID && o.Year == year + 1 && o.IsActive == true);
                        if (academicYear != null)
                        {
                            obj.NextAcademicStartDateOfSemester1 = academicYear.FirstSemesterStartDate;
                            obj.NextAcademicDueDateOfSemester1 = academicYear.FirstSemesterEndDate;
                            obj.NextAcademicStartDateOfSemester2 = academicYear.SecondSemesterStartDate;
                            obj.NextAcademicDueDateOfSemester2 = academicYear.SecondSemesterEndDate;
                        }
                        else
                        {
                            academicYear = AcademicYearBusiness.All.FirstOrDefault(o => o.SchoolID == schoolID && o.Year == year + 2 && o.IsActive == true);
                            if (academicYear != null)
                            {
                                obj.NextAcademicStartDateOfSemester1 = academicYear.FirstSemesterStartDate;
                                obj.NextAcademicDueDateOfSemester1 = academicYear.FirstSemesterEndDate;
                                obj.NextAcademicStartDateOfSemester2 = academicYear.SecondSemesterStartDate;
                                obj.NextAcademicDueDateOfSemester2 = academicYear.SecondSemesterEndDate;
                            }
                        }

                        if (obj != null) lstView.Add(obj);
                    }
                }
                result.ValidationCode = WCFConstant.RESPONSE_SUCCESS;
                result.LstPupilView = lstView;
            }
            catch (Exception ex)
            {
                LogExtensions.GetCurrentClass = System.Reflection.MethodBase.GetCurrentMethod().DeclaringType;
                string para = "null";
                LogExtensions.ErrorExt(logger, DateTime.Now, para, ex.Message, "null");
                result.ValidationCode = WCFConstant.RESPONSE_EXCEPTION;
            }
            return result;
        }

        #region Lấy các thông tin mới nhất của học sinh: vi phạm, nghỉ học, miễn giảm...
        public NewInformationOfPupilResponse GetNewInformationOfPupil(InformationRequest objInfomation)
        {
            NewInformationOfPupilResponse InFormationOfPupilResponse = new NewInformationOfPupilResponse();
            try
            {
                DeclareBusiness();
                int schoolID = objInfomation.SchoolID;
                // set condition get information in the last 6 days
                DateTime fromDate = DateTime.Now.Date.AddDays(-6);
                DateTime toDate = DateTime.Now.Date.AddHours(23).AddMinutes(59).AddSeconds(59);

                //Set Time
                objInfomation.IsLoadNewInfo = true;
                IDictionary<string, object> dic = new Dictionary<string, object>()
                {
                    {"SchoolID",objInfomation.SchoolID}, 
                    //{"ClassID",objInfomation.ClassID},
                    {"AcademicYearID",objInfomation.AcademicYearID},
                    {"PupilID",objInfomation.PupilProfileID},
                    {"FromViolatedDate",fromDate},
                    {"ToViolatedDate",toDate},
                    {"FromAbsentDate",fromDate},
                    {"ToAbsentDate",toDate},
                    {"FromPraiseDate",fromDate},
                    {"ToPraiseDate",toDate},
                    {"FromDisciplinedDate",fromDate},
                    {"ToDisciplinedDate",toDate}
                };
                List<PupilFault> lstPupilFault = new List<PupilFault>();
                var iqFaults = PupilFaultBusiness.SearchBySchool(schoolID, dic);
                if (iqFaults != null && iqFaults.Count() > 0) lstPupilFault = iqFaults.OrderByDescending(p => p.ViolatedDate).ToList();

                List<PupilAbsence> lstPupilAbsent = new List<PupilAbsence>();
                var iqPupilAbsent = PupilAbsenceBusiness.SearchBySchool(schoolID, dic);
                if (iqPupilAbsent != null && iqPupilAbsent.Count() > 0) lstPupilAbsent = iqPupilAbsent.OrderByDescending(p => p.AbsentDate).ToList();

                List<PupilPraise> lstPupilPraise = new List<PupilPraise>();
                var iqPupilPraise = PupilPraiseBusiness.SearchBySchool(schoolID, dic);
                if (iqPupilPraise != null && iqPupilPraise.Count() > 0) lstPupilPraise = iqPupilPraise.OrderByDescending(p => p.PraiseDate).ToList();

                List<PupilDiscipline> lstPupilDiscipline = new List<PupilDiscipline>();
                var iqPupilDiscipline = PupilDisciplineBusiness.SearchBySchool(schoolID, dic);
                if (iqPupilDiscipline != null && iqPupilDiscipline.Count() > 0) lstPupilDiscipline = iqPupilDiscipline.OrderByDescending(p => p.DisciplinedDate).ToList();

                List<NewInformationOfPupil> LstRet = GetContent(lstPupilFault, lstPupilAbsent, lstPupilPraise, lstPupilDiscipline, fromDate, toDate, objInfomation.AcademicYearID, objInfomation.SemesterID);
                InFormationOfPupilResponse.lstInformationOfPupil = LstRet;
                InFormationOfPupilResponse.ValidationCode = WCFConstant.RESPONSE_SUCCESS;
                return InFormationOfPupilResponse;
            }
            catch (Exception ex)
            {
                LogExtensions.GetCurrentClass = System.Reflection.MethodBase.GetCurrentMethod().DeclaringType;
                string para = "null";
                LogExtensions.ErrorExt(logger, DateTime.Now, para, ex.Message, "null");
                InFormationOfPupilResponse.ValidationCode = WCFConstant.RESPONSE_EXCEPTION;
                return new NewInformationOfPupilResponse();
            }

        }
        private List<NewInformationOfPupil> GetContent(List<PupilFault> faults, List<PupilAbsence> absents, List<PupilPraise> praises, List<PupilDiscipline> discriplines, DateTime fromDate, DateTime toDate, int academicYearID, int semesterID)
        {
            AcademicYear objAcademicYear = AcademicYearBusiness.Find(academicYearID);
            List<NewInformationOfPupil> LstNewInfo = new List<NewInformationOfPupil>();
            DateTime date = toDate.Date;
            DateTime starDate;
            DateTime endDate;
            if (semesterID == 1)
            {
                starDate = objAcademicYear.FirstSemesterStartDate.Value.Date;
                endDate = objAcademicYear.SecondSemesterStartDate.Value.Date.AddHours(23).AddMinutes(59).AddSeconds(59);
                faults = faults.Where(f => f.ViolatedDate >= starDate && f.ViolatedDate < endDate).ToList();
                absents = absents.Where(a => a.AbsentDate >= starDate && a.AbsentDate < endDate).ToList();
                praises = praises.Where(p => p.PraiseDate >= starDate && p.PraiseDate < endDate).ToList();
                discriplines = discriplines.Where(d => d.DisciplinedDate >= starDate && d.DisciplinedDate <= endDate).ToList();
            }
            else if (semesterID == 2)
            {
                starDate = objAcademicYear.SecondSemesterStartDate.Value.Date;
                endDate = objAcademicYear.SecondSemesterEndDate.Value.Date.AddHours(23).AddHours(59).AddSeconds(59);
                faults = faults.Where(f => f.ViolatedDate >= starDate && f.ViolatedDate <= endDate).ToList();
                absents = absents.Where(a => a.AbsentDate >= starDate && a.AbsentDate <= endDate).ToList();
                praises = praises.Where(p => p.PraiseDate >= starDate && p.PraiseDate <= endDate).ToList();
                discriplines = discriplines.Where(d => d.DisciplinedDate >= starDate && d.DisciplinedDate <= endDate).ToList();
            }
            bool STOP = false;
            for (int i = 0; i < 8; i++)
            {
                date = toDate.AddDays(-i).Date;
                // vi phạm                
                if (faults != null && faults.Count > 0)
                {
                    STOP = false;
                    do
                    {
                        PupilFault fault = faults.FirstOrDefault(); // lấy vị trí đầu tiên
                        if (fault != null)
                        {
                            LstNewInfo.Add(new NewInformationOfPupil()
                            {
                                OrderID = 1,
                                PupilProfileID = fault.PupilID,
                                ClassID = fault.ClassID,
                                ActionDate = fault.ViolatedDate,
                                TypeName = "Vi phạm",
                                Content = fault.FaultCriteria.Resolution
                            });
                            faults.RemoveAt(0); // remove khỏi vị trí đầu tiên
                        }
                        else STOP = true;
                    }
                    while (faults.Count > 0 && !STOP);
                }

                // điểm danh                
                if (absents != null && absents.Count > 0)
                {
                    STOP = false;
                    do
                    {
                        PupilAbsence absent = absents.FirstOrDefault();
                        if (absent != null)
                        {
                            LstNewInfo.Add(new NewInformationOfPupil()
                            {
                                OrderID = 2,
                                PupilProfileID = absent.PupilID,
                                ClassID = absent.ClassID,
                                ActionDate = absent.AbsentDate,
                                TypeName = "Điểm danh",
                                Content = String.Format("Nghỉ học {0} phép buổi {1}", absent.IsAccepted == true ? "có" : "không",
                                         absent.Section > 2 ? "tối" : absent.Section > 1 ? "chiều" : "sáng")
                            });
                            absents.RemoveAt(0);
                        }
                        else STOP = true;
                    }
                    while (absents.Count > 0 && !STOP);
                }

                // Dia diem 
                IDictionary<int, string> location = new Dictionary<int, string>{
                            {1, "Bộ giáo dục" },
                            {2, "Sở giáo dục" },
                            {3, "Phòng giáo dục" },
                            {4, "Trường" },
                            {5, "Lớp" }};
                // End Constant
                // Khen thuong

                if (praises != null && praises.Count > 0)
                {
                    STOP = false;
                    do
                    {
                        PupilPraise praise = praises.FirstOrDefault();
                        if (praise != null)
                        {
                            LstNewInfo.Add(new NewInformationOfPupil()
                            {
                                OrderID = 3,
                                ActionDate = praise.PraiseDate,
                                TypeName = "Khen thưởng",
                                Content = String.Format("{0}. Địa điểm: {1}", praise.PraiseType.Resolution,
                                praise.PraiseLevel > 0 ? location[praise.PraiseLevel] : String.Empty)
                            });
                            praises.RemoveAt(0);
                        }
                        else STOP = true;
                    }
                    while (praises.Count > 0 && !STOP);
                }

                if (discriplines != null && discriplines.Count > 0)
                {
                    // Ky luat
                    STOP = false;
                    do
                    {
                        PupilDiscipline dis = discriplines.FirstOrDefault();
                        if (dis != null)
                        {
                            LstNewInfo.Add(new NewInformationOfPupil()
                            {
                                OrderID = 4,
                                ActionDate = dis.DisciplinedDate,
                                TypeName = "Kỷ luật",
                                Content = String.Format("{0}. Địa điểm: {1}", dis.DisciplineType.Resolution,
                                dis.DisciplineLevel > 0 ? location[dis.DisciplineLevel] : String.Empty)
                            });
                            discriplines.RemoveAt(0);
                        }
                        else STOP = true;
                    }
                    while (discriplines.Count > 0 && !STOP);
                }
            } // End for 7 days
            return LstNewInfo.OrderByDescending(p => p.ActionDate).ThenBy(p => p.OrderID).ToList();
        }
        #endregion

        public ListMarkRecordResponse GetListPupilTranscript(int PupilProfileID, int SchoolID, int ClassID, int AcademicYearID, int EducationGrade, int SemesterID, bool isShowAllMark)
        {
            ListMarkRecordResponse objMarkRecordResponse = new ListMarkRecordResponse();
            List<MarkRecordLite> lstMarkRecordLite = new List<MarkRecordLite>();
            MarkRecordLite objMarkRecordLite = null;
            List<PupilTranscriptBO> lstPupilTranscript = new List<PupilTranscriptBO>();
            try
            {

                DeclareBusiness();
                AcademicYear objAcademicYear = AcademicYearBusiness.Find(AcademicYearID);
                if (objAcademicYear == null)
                {
                    return null;
                }
                // Danh sach cac cot diem bi khoa cua hoc ky 1
                Dictionary<int, string> dicLockHKI = MarkRecordBusiness.GetLockMarkTitleBySubject(SchoolID, AcademicYearID, ClassID, GlobalConstants.SEMESTER_OF_YEAR_FIRST);
                // Danh sach cac cot diem bi khoa cua hoc ky 2
                Dictionary<int, string> dicLockHKII = MarkRecordBusiness.GetLockMarkTitleBySubject(SchoolID, AcademicYearID, ClassID, GlobalConstants.SEMESTER_OF_YEAR_SECOND);
                List<MarkRecordBO> lstMarkRecordBO;
                //Chiendd1: 04/05/2016, bo sung dieu kien where parttition
                // Sua lai cau lenh join
                int partitionId = SchoolID % 100;
                if (UtilsBusiness.IsMoveHistory(objAcademicYear))
                {
                    IMarkRecordHistoryBusiness markRecordHistoryBusiness = new MarkRecordHistoryBusiness(logger, Context);
                    lstMarkRecordBO = (from mr in markRecordHistoryBusiness.AllNoTracking
                                           //join c in ClassSubjectBusiness.All on mr.SubjectID equals c.SubjectID
                                       join cs in SubjectCatBusiness.AllNoTracking on mr.SubjectID equals cs.SubjectCatID
                                       where mr.PupilID == PupilProfileID
                                       && mr.SchoolID == SchoolID
                                       && mr.AcademicYearID == AcademicYearID
                                       && mr.ClassID == ClassID
                                       //&& c.ClassID == ClassID
                                       && mr.Semester == SemesterID
                                       && mr.Last2digitNumberSchool == partitionId
                                       select new MarkRecordBO
                                       {
                                           SubjectID = mr.SubjectID,
                                           SubjectName = cs.SubjectName,
                                           Title = mr.Title,
                                           Mark = mr.Mark,
                                           Semester = mr.Semester,
                                           MarkTypeID = mr.MarkTypeID
                                       }).ToList();
                }
                else
                {
                    lstMarkRecordBO = (from mr in MarkRecordBusiness.AllNoTracking
                                           //join c in ClassSubjectBusiness.All on mr.SubjectID equals c.SubjectID
                                       join cs in SubjectCatBusiness.AllNoTracking on mr.SubjectID equals cs.SubjectCatID
                                       where mr.PupilID == PupilProfileID
                                       && mr.SchoolID == SchoolID
                                       && mr.AcademicYearID == AcademicYearID
                                       && mr.ClassID == ClassID
                                       //&& c.ClassID == ClassID
                                       && mr.Semester == SemesterID
                                       && mr.Last2digitNumberSchool == partitionId
                                       select new MarkRecordBO
                                       {
                                           SubjectID = mr.SubjectID,
                                           //SubjectName = mr.SubjectCat.SubjectName,
                                           SubjectName = cs.SubjectName,
                                           Title = mr.Title,
                                           Mark = mr.Mark,
                                           Semester = mr.Semester,
                                           MarkTypeID = mr.MarkTypeID
                                       }).ToList();
                }

                List<JudgeRecordBO> lstJudgeRecordBO;
                if (UtilsBusiness.IsMoveHistory(objAcademicYear))
                {
                    IJudgeRecordHistoryBusiness judgeRecordHistoryBusiness = new JudgeRecordHistoryBusiness(logger, Context);
                    lstJudgeRecordBO = (from jr in judgeRecordHistoryBusiness.AllNoTracking
                                            //join c in ClassSubjectBusiness.All on jr.SubjectID equals c.SubjectID
                                        join cs in SubjectCatBusiness.AllNoTracking on jr.SubjectID equals cs.SubjectCatID
                                        where jr.PupilID == PupilProfileID
                                        && jr.SchoolID == SchoolID
                                        && jr.AcademicYearID == AcademicYearID
                                        && jr.ClassID == ClassID
                                        //&& c.ClassID == ClassID
                                        && jr.Semester == SemesterID
                                        && jr.Last2digitNumberSchool == partitionId
                                        select new JudgeRecordBO
                                        {
                                            SubjectID = jr.SubjectID,
                                            //SubjectName = jr.SubjectCat.SubjectName,
                                            SubjectName = cs.SubjectName,
                                            Title = jr.Title,
                                            Judgement = jr.Judgement,
                                            Semester = jr.Semester,
                                            MarkTypeID = jr.MarkTypeID
                                        }).ToList();
                }
                else
                {
                    lstJudgeRecordBO = (from jr in JudgeRecordBusiness.AllNoTracking
                                            //join c in ClassSubjectBusiness.All on jr.SubjectID equals c.SubjectID
                                        join cs in SubjectCatBusiness.AllNoTracking on jr.SubjectID equals cs.SubjectCatID
                                        where jr.PupilID == PupilProfileID
                                        && jr.SchoolID == SchoolID
                                        && jr.AcademicYearID == AcademicYearID
                                        && jr.ClassID == ClassID
                                        //&& c.ClassID == ClassID
                                        && jr.Semester == SemesterID
                                        && jr.Last2digitNumberSchool == partitionId
                                        select new JudgeRecordBO
                                        {
                                            SubjectID = jr.SubjectID,
                                            //SubjectName = jr.SubjectCat.SubjectName,
                                            SubjectName = cs.SubjectName,
                                            Title = jr.Title,
                                            Judgement = jr.Judgement,
                                            Semester = jr.Semester,
                                            MarkTypeID = jr.MarkTypeID
                                        }).ToList();
                }
                List<SummedUpRecordOfPupil> lstSummedUpRecordBO;
                if (UtilsBusiness.IsMoveHistory(objAcademicYear))
                {
                    ISummedUpRecordHistoryBusiness summedUpRecordHistoryBusiness = new SummedUpRecordHistoryBusiness(logger, Context);
                    lstSummedUpRecordBO = (from sur in summedUpRecordHistoryBusiness.AllNoTracking
                                               //join c in ClassSubjectBusiness.All on sur.SubjectID equals c.SubjectID
                                           join cs in SubjectCatBusiness.AllNoTracking on sur.SubjectID equals cs.SubjectCatID
                                           where sur.PupilID == PupilProfileID
                                           && sur.SchoolID == SchoolID
                                           && sur.AcademicYearID == AcademicYearID
                                           && sur.ClassID == ClassID
                                           //&& c.ClassID == ClassID
                                           && sur.PeriodID == null
                                           orderby sur.Semester
                                           select new SummedUpRecordOfPupil
                                           {
                                               SubjectID = sur.SubjectID,
                                               SubjectName = cs.SubjectName,
                                               IsCommenting = sur.IsCommenting,
                                               SummedUpMark = sur.Semester > 3 ? sur.ReTestMark : sur.SummedUpMark,
                                               RetestMark = sur.ReTestMark,
                                               RetestJudge = sur.ReTestJudgement,
                                               JudgementResult = sur.Semester > 3 ? sur.ReTestJudgement : sur.JudgementResult,
                                               Semester = sur.Semester
                                           }).ToList();
                }
                else
                {
                    lstSummedUpRecordBO = (from sur in SummedUpRecordBusiness.AllNoTracking
                                               //join c in ClassSubjectBusiness.All on sur.SubjectID equals c.SubjectID
                                           join cs in SubjectCatBusiness.AllNoTracking on sur.SubjectID equals cs.SubjectCatID
                                           where sur.PupilID == PupilProfileID
                                           && sur.SchoolID == SchoolID
                                           && sur.AcademicYearID == AcademicYearID
                                           && sur.ClassID == ClassID
                                           //&& c.ClassID == ClassID
                                           && sur.PeriodID == null
                                           orderby sur.Semester
                                           select new SummedUpRecordOfPupil
                                           {
                                               SubjectID = sur.SubjectID,
                                               SubjectName = cs.SubjectName,
                                               IsCommenting = sur.IsCommenting,
                                               SummedUpMark = sur.Semester > 3 ? sur.ReTestMark : sur.SummedUpMark,
                                               RetestMark = sur.ReTestMark,
                                               RetestJudge = sur.ReTestJudgement,
                                               JudgementResult = sur.Semester > 3 ? sur.ReTestJudgement : sur.JudgementResult,
                                               Semester = sur.Semester
                                           }).ToList();
                }
                // order mon hoc
                var subjects = (from p in SubjectCatBusiness.AllNoTracking
                                join cs in ClassSubjectBusiness.AllNoTracking on p.SubjectCatID equals cs.SubjectID

                                where cs.ClassID == ClassID
                               && cs.Last2digitNumberSchool == partitionId
                                orderby p.IsCommenting, p.OrderInSubject
                                select new { SubjectID = p.SubjectCatID, SubjectName = p.SubjectName }).ToList();

                #region Cấp I
                if (EducationGrade == 1)
                {
                    for (int i = 0, size = subjects.Count; i < size; i++)
                    {
                        var subject = subjects[i];
                        var lstTemp = lstSummedUpRecordBO.Where(p => p.SubjectID == subject.SubjectID).ToList();
                        objMarkRecordLite = new MarkRecordLite();
                        objMarkRecordLite.SubjectID = subject.SubjectID;
                        objMarkRecordLite.SubjectName = subject.SubjectName;
                        if (lstTemp != null && lstTemp.Count > 0)
                        {
                            objMarkRecordLite.DiemTBHKI = lstTemp.Where(p => p.Semester == 1).Select(p => p.IsCommenting == 0 ? (p.SummedUpMark.HasValue ? p.SummedUpMark.ToString() : "") : p.JudgementResult).FirstOrDefault();
                            objMarkRecordLite.DiemTBHKII = lstTemp.Where(p => p.Semester >= 2).OrderByDescending(p => p.Semester).Select(p => p.IsCommenting == 0 ? (p.RetestMark.HasValue ? p.RetestMark.Value.ToString() : p.SummedUpMark.HasValue ? p.SummedUpMark.ToString() : "") : (!string.IsNullOrEmpty(p.RetestJudge) ? p.RetestJudge.ToString() : !string.IsNullOrEmpty(p.JudgementResult) ? p.JudgementResult.ToString() : string.Empty)).FirstOrDefault();
                        }

                        lstMarkRecordLite.Add(objMarkRecordLite);
                    }
                }
                #endregion
                #region Cấp II/III
                else
                {
                    string lockMarkHKI = string.Empty;
                    string lockMarkHKII = string.Empty;
                    int subjectID = 0;
                    for (int i = 0, size = subjects.Count; i < size; i++)
                    {
                        var subject = subjects[i];
                        var itemMarkRecord = lstMarkRecordBO.Where(p => p.SubjectID == subject.SubjectID).ToList();
                        var itemJudgeRecord = lstJudgeRecordBO.Where(p => p.SubjectID == subject.SubjectID).ToList();
                        var itemSummedUpRecord = lstSummedUpRecordBO.Where(p => p.SubjectID == subject.SubjectID).ToList();
                        objMarkRecordLite = new MarkRecordLite();
                        subjectID = subject.SubjectID;
                        objMarkRecordLite.SubjectID = subjectID;
                        lockMarkHKI = dicLockHKI.Count > 0 && dicLockHKI.ContainsKey(subjectID) ? dicLockHKI[subjectID] : string.Empty;
                        lockMarkHKII = dicLockHKII.Count > 0 && dicLockHKII.ContainsKey(subjectID) ? dicLockHKII[subjectID] : string.Empty;
                        objMarkRecordLite.SubjectName = subject.SubjectName;
                        if (itemMarkRecord != null && itemMarkRecord.Count > 0)
                        {
                            if (isShowAllMark || (SemesterID == 1 ? lockMarkHKI.Contains("LHK") : lockMarkHKII.Contains("LHK")))
                            {
                                objMarkRecordLite.DiemMieng = itemMarkRecord.Where(p => p.Title.Contains("M")).OrderBy(p => p.Title).Select(p => new MarkColumnResponse { Order = int.Parse(p.Title.Replace("M", string.Empty)), Mark = p.Mark.ToString("0") }).ToList();
                                objMarkRecordLite.Diem15P = itemMarkRecord.Where(p => p.Title.Contains("P")).OrderBy(p => p.Title).Select(p => new MarkColumnResponse { Order = int.Parse(p.Title.Replace("P", string.Empty)), Mark = p.Mark.ToString("0.0") }).ToList();
                                objMarkRecordLite.Diem1T = itemMarkRecord.Where(p => p.Title.Contains("V")).OrderBy(p => p.Title).Select(p => new MarkColumnResponse { Order = int.Parse(p.Title.Replace("V", string.Empty)), Mark = p.Mark.ToString("0.0") }).ToList();
                                objMarkRecordLite.DiemCuoiKy = itemMarkRecord.Where(p => p.Title.Contains("HK")).Select(p => p.Mark.ToString("0.0")).FirstOrDefault();
                            }
                            else
                            {
                                objMarkRecordLite.DiemMieng = itemMarkRecord.Where(p => p.Title.Contains("M") && (SemesterID == 1 ? lockMarkHKI.Contains(p.Title) : lockMarkHKII.Contains(p.Title))).OrderBy(p => p.Title).Select(p => new MarkColumnResponse { Order = int.Parse(p.Title.Replace("M", string.Empty)), Mark = p.Mark.ToString("0") }).ToList();
                                objMarkRecordLite.Diem15P = itemMarkRecord.Where(p => p.Title.Contains("P") && (SemesterID == 1 ? lockMarkHKI.Contains(p.Title) : lockMarkHKII.Contains(p.Title))).OrderBy(p => p.Title).Select(p => new MarkColumnResponse { Order = int.Parse(p.Title.Replace("P", string.Empty)), Mark = p.Mark.ToString("0.0") }).ToList();
                                objMarkRecordLite.Diem1T = itemMarkRecord.Where(p => p.Title.Contains("V") && (SemesterID == 1 ? lockMarkHKI.Contains(p.Title) : lockMarkHKII.Contains(p.Title))).OrderBy(p => p.Title).Select(p => new MarkColumnResponse { Order = int.Parse(p.Title.Replace("V", string.Empty)), Mark = p.Mark.ToString("0.0") }).ToList();
                                objMarkRecordLite.DiemCuoiKy = itemMarkRecord.Where(p => p.Title.Contains("HK") && (SemesterID == 1 ? lockMarkHKI.Contains(p.Title) : lockMarkHKII.Contains(p.Title))).Select(p => p.Mark.ToString("0.0")).FirstOrDefault();
                            }
                        }
                        if (itemJudgeRecord != null && itemJudgeRecord.Count > 0)
                        {
                            if (isShowAllMark || (SemesterID == 1 ? lockMarkHKI.Contains("LHK") : lockMarkHKII.Contains("LHK")))
                            {
                                objMarkRecordLite.DiemMieng = itemJudgeRecord.Where(p => p.Title.Contains("M")).OrderBy(p => p.Title).Select(p => new MarkColumnResponse { Order = int.Parse(p.Title.Replace("M", string.Empty)), Mark = p.Judgement }).ToList();
                                objMarkRecordLite.Diem15P = itemJudgeRecord.Where(p => p.Title.Contains("P")).OrderBy(p => p.Title).Select(p => new MarkColumnResponse { Order = int.Parse(p.Title.Replace("P", string.Empty)), Mark = p.Judgement }).ToList();
                                objMarkRecordLite.Diem1T = itemJudgeRecord.Where(p => p.Title.Contains("V")).OrderBy(p => p.Title).Select(p => new MarkColumnResponse { Order = int.Parse(p.Title.Replace("V", string.Empty)), Mark = p.Judgement }).ToList();
                                objMarkRecordLite.DiemCuoiKy = itemJudgeRecord.Where(p => p.Title.Contains("HK")).Select(p => p.Judgement).FirstOrDefault();
                            }
                            else
                            {
                                objMarkRecordLite.DiemMieng = itemJudgeRecord.Where(p => p.Title.Contains("M") && (SemesterID == 1 ? lockMarkHKI.Contains(p.Title) : lockMarkHKII.Contains(p.Title))).OrderBy(p => p.Title).Select(p => new MarkColumnResponse { Order = int.Parse(p.Title.Replace("M", string.Empty)), Mark = p.Judgement }).ToList();
                                objMarkRecordLite.Diem15P = itemJudgeRecord.Where(p => p.Title.Contains("P") && (SemesterID == 1 ? lockMarkHKI.Contains(p.Title) : lockMarkHKII.Contains(p.Title))).OrderBy(p => p.Title).Select(p => new MarkColumnResponse { Order = int.Parse(p.Title.Replace("P", string.Empty)), Mark = p.Judgement }).ToList();
                                objMarkRecordLite.Diem1T = itemJudgeRecord.Where(p => p.Title.Contains("V") && (SemesterID == 1 ? lockMarkHKI.Contains(p.Title) : lockMarkHKII.Contains(p.Title))).OrderBy(p => p.Title).Select(p => new MarkColumnResponse { Order = int.Parse(p.Title.Replace("V", string.Empty)), Mark = p.Judgement }).ToList();
                                objMarkRecordLite.DiemCuoiKy = itemJudgeRecord.Where(p => p.Title.Contains("HK") && (SemesterID == 1 ? lockMarkHKI.Contains(p.Title) : lockMarkHKII.Contains(p.Title))).Select(p => p.Judgement).FirstOrDefault();
                            }
                        }

                        if (itemSummedUpRecord != null && itemSummedUpRecord.Count > 0)
                        {

                            if (itemSummedUpRecord[0].IsCommenting == 0)
                            {
                                objMarkRecordLite.DiemTBHKI = lockMarkHKI.Contains("LHK") || isShowAllMark ? itemSummedUpRecord.Where(p => p.Semester == 1).Select(p => p.SummedUpMark.HasValue ? p.SummedUpMark.Value.ToString("0.0") : string.Empty).FirstOrDefault() : string.Empty;
                                objMarkRecordLite.DiemTBHKII = lockMarkHKII.Contains("LHK") || isShowAllMark ? itemSummedUpRecord.Where(p => p.Semester == 2).Select(p => p.SummedUpMark.HasValue ? p.SummedUpMark.Value.ToString("0.0") : string.Empty).FirstOrDefault() : string.Empty;
                                objMarkRecordLite.DiemTBCN = lockMarkHKII.Contains("LHK") || isShowAllMark ? itemSummedUpRecord.Where(p => p.Semester > 2).OrderByDescending(p => p.Semester).Select(p => p.SummedUpMark.HasValue ? p.SummedUpMark.Value.ToString("0.0") : string.Empty).FirstOrDefault() : string.Empty;
                            }
                            else
                            {
                                objMarkRecordLite.DiemTBHKI = lockMarkHKI.Contains("LHK") || isShowAllMark ? itemSummedUpRecord.Where(p => p.Semester == 1).Select(p => p.JudgementResult).FirstOrDefault() : string.Empty;
                                objMarkRecordLite.DiemTBHKII = lockMarkHKII.Contains("LHK") || isShowAllMark ? itemSummedUpRecord.Where(p => p.Semester == 2).Select(p => p.JudgementResult).FirstOrDefault() : string.Empty;
                                objMarkRecordLite.DiemTBCN = lockMarkHKII.Contains("LHK") || isShowAllMark ? itemSummedUpRecord.Where(p => p.Semester > 2).OrderByDescending(p => p.Semester).Select(p => p.JudgementResult).FirstOrDefault() : string.Empty;
                            }
                        }
                        lstMarkRecordLite.Add(objMarkRecordLite);
                    }
                }
                #endregion
                objMarkRecordResponse.LstMarkRecordLite = lstMarkRecordLite;
                objMarkRecordResponse.ValidationCode = WCFConstant.RESPONSE_SUCCESS;

            }
            catch (Exception ex)
            {
                LogExtensions.GetCurrentClass = System.Reflection.MethodBase.GetCurrentMethod().DeclaringType;
                string para = string.Format(" PupilProfileID={0}, SchoolID={1}, ClassID={2}, AcademicYearID={3}, EducationGrade={4}, SemesterID={5}, isShowAllMark={6}", PupilProfileID, SchoolID, ClassID, AcademicYearID, EducationGrade, SemesterID, isShowAllMark);
                LogExtensions.ErrorExt(logger, DateTime.Now, para, ex.Message, "null");
                objMarkRecordResponse.ValidationCode = WCFConstant.RESPONSE_EXCEPTION;
            }
            return objMarkRecordResponse;
        }

        #region khen thuong, ky luat hoc sinh
        /// <summary>
        /// lay khen thuong ky luat cua hoc sinh
        /// </summary>
        /// <param name="schoolID">id truong</param>
        /// <param name="classID">id lop hoc</param>
        /// <param name="academicYearID">id nam hoc</param>
        /// <param name="pupilID">id hoc sinh</param>
        /// <returns></returns>
        public PupilPraiseDisciplineResponse GetPupilPraiseDiscipline(int PupilID, int SchoolID)
        {
            PupilPraiseDisciplineResponse response = new PupilPraiseDisciplineResponse();
            DateTime DateTimeNow = DateTime.Now;
            try
            {
                //Khoi tao Business
                DeclareBusiness();
                #region Thông tin khen thuong
                IDictionary<string, object> dic = new Dictionary<string, object>();
                //dic["ClassID"] = classID;
                //dic["AcademicYearID"] = academicYearID;
                dic["PupilID"] = PupilID;
                List<PupilPraise> pupilPraise = (from pr in PupilPraiseBusiness.All
                                                 where pr.PupilID == PupilID
                                                 orderby pr.PraiseDate descending
                                                 select pr).ToList();
                #endregion

                #region thong tin ky luat
                List<PupilDiscipline> pupilDiscipline = (from pd in PupilDisciplineBusiness.All
                                                         where pd.PupilID == PupilID
                                                         orderby pd.DisciplinedDate descending
                                                         select pd).ToList();
                #endregion

                // tao list response
                int countPraise = pupilPraise.Count;
                int countDiscipline = pupilDiscipline.Count;
                int count = Math.Max(countPraise, countDiscipline);
                count = Math.Max(count, countDiscipline);
                List<PupilPraiseDisciplineDefault> pupilPraiseDisciplineList = new List<PupilPraiseDisciplineDefault>();
                PupilPraiseDisciplineDefault itemResponse = null;
                PupilPraise itemPraise = null;
                PupilDiscipline itemDiscipline = null;
                List<PraiseType> priseType = null;
                List<DisciplineType> disciplineType = null;
                if (countPraise > 0)
                {
                    priseType = (from p in pupilPraise
                                 select new PraiseType
                                 {
                                     PraiseTypeID = p.PraiseTypeID,
                                     Resolution = p.PraiseType.Resolution
                                 }).ToList();
                }

                if (countDiscipline > 0)
                {
                    disciplineType = (from p in pupilDiscipline
                                      select new DisciplineType
                                      {
                                          DisciplineTypeID = p.DisciplineTypeID,
                                          Resolution = p.DisciplineType.Resolution
                                      }).ToList();
                }

                for (int i = 0; i < count; i++)
                {
                    itemResponse = new PupilPraiseDisciplineDefault();
                    if (countPraise > i)
                    {
                        itemPraise = pupilPraise[i];
                        itemResponse.PupilID = itemPraise.PupilID;
                        itemResponse.PraiseDate = itemPraise.PraiseDate.ToString("dd/MM/yyyy");
                        itemResponse.PraiseTypeName = priseType.Where(p => p.PraiseTypeID == itemPraise.PraiseTypeID).Select(p => p.Resolution).FirstOrDefault();
                        itemResponse.PraiseLevel = itemPraise.PraiseLevel == 1 ? "Cấp bộ" : itemPraise.PraiseLevel == 2 ? "Cấp sở, cấp phòng thuộc bộ" : itemPraise.PraiseLevel == 3 ? "Cấp phòng thuộc sở"
                            : itemPraise.PraiseLevel == 4 ? "Cấp trường" : itemPraise.PraiseLevel == 5 ? "Cấp lớp" : "Không xác định";
                        itemResponse.PraiseDescription = itemPraise.Description;
                    }

                    if (countDiscipline > i)
                    {
                        itemDiscipline = pupilDiscipline[i];
                        itemPraise.PupilID = itemDiscipline.PupilID;
                        itemResponse.DisciplineDate = itemDiscipline.DisciplinedDate.ToString("dd/MM/yyyy");
                        itemResponse.DisciplineTypeName = disciplineType.Where(p => p.DisciplineTypeID == itemDiscipline.DisciplineTypeID).Select(p => p.Resolution).FirstOrDefault();
                        itemResponse.DisciplineLevel = itemDiscipline.DisciplineLevel == 1 ? "Cấp bộ" : itemDiscipline.DisciplineLevel == 2 ? "Cấp sở, cấp phòng thuộc bộ" : itemDiscipline.DisciplineLevel == 3 ? "Cấp phòng thuộc sở"
                            : itemDiscipline.DisciplineLevel == 4 ? "Cấp trường" : itemDiscipline.DisciplineLevel == 5 ? "Cấp lớp" : "Không xác định";
                        itemResponse.DisciplineDescription = itemDiscipline.Description;
                    }

                    pupilPraiseDisciplineList.Add(itemResponse);
                }

                //gan validationCode thanh cong
                response.ValidationCode = WCFConstant.RESPONSE_SUCCESS;
                //gan gia tri thuoc tinh dinh kem 
                response.PupilPraiseDiscipline = pupilPraiseDisciplineList;
            }
            catch (Exception ex)
            {
                LogExtensions.GetCurrentClass = System.Reflection.MethodBase.GetCurrentMethod().DeclaringType;
                string para = string.Format("PupilID={0}, SchoolID={1}", PupilID, SchoolID);
                LogExtensions.ErrorExt(logger, DateTime.Now, para, ex.Message, "null");
                //gan validationCode Exception
                response.ValidationCode = WCFConstant.RESPONSE_EXCEPTION;
            }
            return response;
        }

        /// <summary>
        /// Danh sach hoc sinh theo list id
        /// </summary>
        /// <param name="pupilProfileList"></param>
        /// <returns></returns>
        public ListPupilProfileClassResponse GetListPupilStatictisSMSByListID(List<int> pupilProfileList, int AcademicYearID)
        {
            ListPupilProfileClassResponse objResultResponse = new ListPupilProfileClassResponse();
            try
            {
                //Khoi tao Business
                DeclareBusiness();
                List<PupilProfileClass> listPupil = (from pp in PupilProfileBusiness.All
                                                     join c in PupilOfClassBusiness.All on pp.PupilProfileID equals c.PupilID
                                                     where pupilProfileList.Contains(pp.PupilProfileID)
                                                       && c.AcademicYearID == AcademicYearID
                                                     select new PupilProfileClass
                                                     {
                                                         PupilProfileID = pp.PupilProfileID,
                                                         PupilCode = pp.PupilCode,
                                                         FullName = pp.FullName,
                                                         ClassName = c.ClassProfile.DisplayName,
                                                         ClassID = c.ClassID,
                                                         Status = c.Status,
                                                         OrderID = pp.ClassProfile.OrderNumber.HasValue ? pp.ClassProfile.OrderNumber.Value : 0,
                                                         EudcationLevelID = c.ClassProfile.EducationLevelID,
                                                         HeadTeacherID = c.ClassProfile.HeadTeacherID ?? 0,
                                                         OrderInClass = c.OrderInClass.HasValue ? c.OrderInClass.Value : 0,
                                                         Name = pp.Name
                                                     }).ToList();
                //gan validationCode thanh cong
                objResultResponse.ValidationCode = WCFConstant.RESPONSE_SUCCESS;
                //gan gia tri thuoc tinh dinh kem 
                objResultResponse.LstPupilProfileClass = listPupil;
            }
            catch (Exception ex)
            {
                LogExtensions.GetCurrentClass = System.Reflection.MethodBase.GetCurrentMethod().DeclaringType;
                string para = string.Format("academicYearId={0}", AcademicYearID);
                LogExtensions.ErrorExt(logger, DateTime.Now, para, ex.Message, "null");
                //gan validationCode Exception
                objResultResponse.ValidationCode = WCFConstant.RESPONSE_EXCEPTION;
            }

            return objResultResponse;
        }
        public ListGoodBadSubjectResponse GetListGoodBadSubject(int PupilProfileID, int AcademicYearID, int ClassID, int SemesterID, int Grade)
        {
            ListGoodBadSubjectResponse objGoodBadSubject = new ListGoodBadSubjectResponse();
            try
            {
                DeclareBusiness();
                ISummedUpRecordHistoryBusiness summedUpRecordHistoryBusiness = new SummedUpRecordHistoryBusiness(logger, Context);
                AcademicYear academicYear = AcademicYearBusiness.Find(AcademicYearID);
                var subjects = UtilsBusiness.IsMoveHistory(academicYear) ?
                                (from m in summedUpRecordHistoryBusiness.All
                                 where m.PupilID == PupilProfileID
                                 && m.AcademicYearID == AcademicYearID
                                 && m.ClassID == ClassID
                                 && m.PeriodID == null
                                 && (m.SummedUpMark.HasValue || !string.IsNullOrEmpty(m.JudgementResult))
                                 //&& m.Semester == SemesterID
                                 orderby m.SubjectCat.OrderInSubject
                                 select new
                                 {
                                     SubjectName = m.SubjectCat.SubjectName,
                                     SemesterID = m.Semester,
                                     SubjectOrder = m.SubjectCat.OrderInSubject,
                                     IsCommentSubject = m.IsCommenting,
                                     TotalMark = m.SummedUpMark,
                                     JudgeMark = m.JudgementResult
                                 })
                                :
                                (from m in SummedUpRecordBusiness.All
                                 where m.PupilID == PupilProfileID
                                 && m.AcademicYearID == AcademicYearID
                                 && m.ClassID == ClassID
                                 && m.PeriodID == null
                                 && (m.SummedUpMark.HasValue || !string.IsNullOrEmpty(m.JudgementResult))
                                 //&& m.Semester == SemesterID
                                 orderby m.SubjectCat.OrderInSubject
                                 select new
                                 {
                                     SubjectName = m.SubjectCat.SubjectName,
                                     SemesterID = m.Semester,
                                     SubjectOrder = m.SubjectCat.OrderInSubject,
                                     IsCommentSubject = m.IsCommenting,
                                     TotalMark = m.SummedUpMark,
                                     JudgeMark = m.JudgementResult
                                 });
                List<string> goodSubject = new List<string>();
                List<string> badSubject = new List<string>();

                if (Grade == 1)
                {
                    var temp = subjects.Where(s => s.SemesterID == GlobalConstants.SEMESTER_OF_YEAR_SECOND).ToList();
                    if (temp != null && temp.Count > 0)
                    {
                        goodSubject = temp.Where(s => (string.IsNullOrEmpty(s.JudgeMark) || s.JudgeMark != "B")).OrderBy(s => s.IsCommentSubject)
                            .ThenByDescending(s => s.TotalMark)
                            .ThenByDescending(p => p.JudgeMark == "A+" ? 3 : p.JudgeMark == "A" ? 2 : 1).Select(p => p.SubjectName).Take(3).ToList();
                        badSubject = temp.Where(s => (string.IsNullOrEmpty(s.JudgeMark) || (s.JudgeMark != "A+" && s.JudgeMark != "A"))).OrderBy(s => s.IsCommentSubject)
                            .ThenBy(s => s.TotalMark)
                            .ThenBy(p => p.JudgeMark == "A+" ? 3 : p.JudgeMark == "A" ? 2 : 1).Select(p => p.SubjectName).Take(3).ToList();
                    }
                    else
                    {
                        temp = subjects.Where(s => s.SemesterID == GlobalConstants.SEMESTER_OF_YEAR_FIRST).ToList();
                        goodSubject = temp.Where(s => (string.IsNullOrEmpty(s.JudgeMark) || s.JudgeMark != "B")).OrderBy(s => s.IsCommentSubject)
                            .ThenByDescending(s => s.TotalMark)
                            .ThenByDescending(p => p.JudgeMark == "A+" ? 3 : p.JudgeMark == "A" ? 2 : 1).Select(p => p.SubjectName).Take(3).ToList();
                        badSubject = temp.Where(s => (string.IsNullOrEmpty(s.JudgeMark) || (s.JudgeMark != "A+" && s.JudgeMark != "A"))).OrderBy(s => s.IsCommentSubject)
                            .ThenBy(s => s.TotalMark)
                            .ThenBy(p => p.JudgeMark == "A+" ? 3 : p.JudgeMark == "A" ? 2 : 1).Select(p => p.SubjectName).Take(3).ToList();
                    }

                }
                else
                {
                    if (SemesterID == GlobalConstants.SEMESTER_OF_YEAR_FIRST)
                    {
                        var temp = subjects.Where(s => s.SemesterID == GlobalConstants.SEMESTER_OF_YEAR_FIRST).ToList();
                        goodSubject = temp.Where(s => s.JudgeMark != "CĐ").OrderBy(s => s.IsCommentSubject).ThenByDescending(s => (s.TotalMark.HasValue ? s.TotalMark.Value : s.JudgeMark == "Đ" ? 1 : 0)).Select(s => s.SubjectName).Take(3).ToList();
                        badSubject = temp.Where(s => s.JudgeMark != "Đ").OrderBy(s => s.IsCommentSubject).ThenBy(s => (s.TotalMark.HasValue ? s.TotalMark.Value : s.JudgeMark == "Đ" ? 1 : 0)).Select(s => s.SubjectName).Take(3).ToList();
                    }
                    else
                    {
                        var temp = subjects.Where(s => s.SemesterID == GlobalConstants.SEMESTER_OF_YEAR_ALL).ToList();
                        if (temp != null && temp.Count > 0)
                        {
                            goodSubject = temp.Where(s => s.JudgeMark != "CĐ").OrderBy(s => s.IsCommentSubject).ThenByDescending(s => (s.TotalMark.HasValue ? s.TotalMark.Value : s.JudgeMark == "Đ" ? 1 : 0)).Select(s => s.SubjectName).Take(3).ToList();
                            badSubject = temp.Where(s => s.JudgeMark != "Đ").OrderBy(s => s.IsCommentSubject).ThenBy(s => (s.TotalMark.HasValue ? s.TotalMark.Value : s.JudgeMark == "Đ" ? 1 : 0)).Select(s => s.SubjectName).Take(3).ToList();
                        }
                        else
                        {
                            temp = subjects.Where(s => s.SemesterID == GlobalConstants.SEMESTER_OF_YEAR_SECOND).ToList();
                            goodSubject = temp.Where(s => s.JudgeMark != "CĐ").OrderBy(s => s.IsCommentSubject).ThenByDescending(s => (s.TotalMark.HasValue ? s.TotalMark.Value : s.JudgeMark == "Đ" ? 1 : 0)).Select(s => s.SubjectName).Take(3).ToList();
                            badSubject = temp.Where(s => s.JudgeMark != "Đ").OrderBy(s => s.IsCommentSubject).ThenBy(s => (s.TotalMark.HasValue ? s.TotalMark.Value : s.JudgeMark == "Đ" ? 1 : 0)).Select(s => s.SubjectName).Take(3).ToList();
                        }
                    }
                }

                // mon tot nhat
                objGoodBadSubject.ValidateCode = WCFConstant.RESPONSE_SUCCESS;
                objGoodBadSubject.LstGoodSubject = goodSubject;
                objGoodBadSubject.LstBadSubject = badSubject;
            }
            catch (Exception ex)
            {
                LogExtensions.GetCurrentClass = System.Reflection.MethodBase.GetCurrentMethod().DeclaringType;
                string para = string.Format("PupilProfileID={0}, AcademicYearID={1}, ClassID={2}, SemesterID={3}, Grade={4}", PupilProfileID, AcademicYearID, ClassID, SemesterID, Grade);
                LogExtensions.ErrorExt(logger, DateTime.Now, para, ex.Message, "null");
                objGoodBadSubject.ValidateCode = WCFConstant.RESPONSE_EXCEPTION;
            }
            return objGoodBadSubject;
        }

        #endregion

        /// <summary>
        ///  Lấy danh sách học sinh cho thong ke tin nhan SMS_SCHOOOL
        /// Lay ra danh sach tat ca hoc sinh, nhung hoc sinh nao co trang thay chuyen truong hoac chuyen lop thi khong co trong khoang thoi gian thong ke
        /// </summary>
        /// <param name="SchoolID"></param>
        /// <param name="AcademicYearID"></param>
        /// <param name="EducationLevelID"></param>
        /// <param name="ClassID"></param>
        /// <param name="GradeID"></param>
        /// <param name="ToDate"></param>
        /// <param name="FromDate"></param>
        /// <returns></returns>
        public ListPupilProfileLiteResponse GetListPupilStatictis(int SchoolID, int AcademicYearID, int EducationLevelID, int ClassID, int GradeID = 0)
        {
            ListPupilProfileLiteResponse objResultResponse = new ListPupilProfileLiteResponse();
            try
            {
                //Khoi tao Business
                DeclareBusiness();
                List<PupilProfileLite> listPupil = new List<PupilProfileLite>();
                // Tách làm 2 nhánh để trong trường hợp chỉ có mỗi lớp không phải join vào bảng CLASS
                if (ClassID == 0)
                {
                    var classProfileQuery = ClassProfileBusiness.All.Where(p => p.SchoolID == SchoolID && p.AcademicYearID == AcademicYearID && (GradeID > 0 ? p.EducationLevel.Grade == GradeID : true));
                    if (EducationLevelID > 0)
                    {
                        classProfileQuery = classProfileQuery.Where(p => p.EducationLevelID == EducationLevelID);
                    }

                    var classProfileList = classProfileQuery.ToList();
                    if (classProfileList != null && classProfileList.Count > 0)
                    {
                        var classProfileIDList = classProfileList.Select(p => p.ClassProfileID).ToList();
                        listPupil = (from poc in PupilOfClassBusiness.All
                                     join pp in PupilProfileBusiness.All on poc.PupilID equals pp.PupilProfileID
                                     where poc.SchoolID == SchoolID && poc.AcademicYearID == AcademicYearID
                                     && pp.CurrentSchoolID == SchoolID && classProfileIDList.Contains(poc.ClassID)
                                     orderby poc.OrderInClass, pp.Name, pp.FullName
                                     select new PupilProfileLite
                                     {
                                         PupilProfileID = poc.PupilID,
                                         CurrentClassID = pp.CurrentClassID,
                                         FullName = pp.FullName,
                                         Status = poc.Status,
                                         PupilCode = pp.PupilCode,
                                     }).ToList();
                    }
                }
                else
                {
                    // kiem tra dam bao ton tai du lieu trong db
                    ClassID = ClassProfileBusiness.All.Where(p => p.SchoolID == SchoolID && p.AcademicYearID == AcademicYearID && p.ClassProfileID == ClassID).Select(p => p.ClassProfileID).FirstOrDefault();
                    if (ClassID > 0)
                    {
                        listPupil = (from poc in PupilOfClassBusiness.All
                                     join pp in PupilProfileBusiness.All on poc.PupilID equals pp.PupilProfileID
                                     where poc.SchoolID == SchoolID && poc.AcademicYearID == AcademicYearID
                                     && pp.CurrentSchoolID == SchoolID && poc.ClassID == ClassID
                                     orderby poc.OrderInClass, pp.Name, pp.FullName
                                     select new PupilProfileLite
                                     {
                                         PupilProfileID = poc.PupilID,
                                         CurrentClassID = pp.CurrentClassID,
                                         FullName = pp.FullName,
                                         Status = poc.Status,
                                         PupilCode = pp.PupilCode,
                                     }).ToList();

                    }
                }
                //gan validationCode thanh cong
                objResultResponse.ValidationCode = WCFConstant.RESPONSE_SUCCESS;
                //gan gia tri thuoc tinh dinh kem 
                objResultResponse.LstPupilProfileLite = listPupil;
            }
            catch
            {
                //gan validationCode Exception
                objResultResponse.ValidationCode = WCFConstant.RESPONSE_EXCEPTION;
            }
            return objResultResponse;
        }


        #region Lay danh sach ID cua hoc sinh
        /// <summary>
        /// Lsy danh sach ID cua hoc sinh
        /// </summary>
        /// <param name="SchoolID"></param>
        /// <param name="Grade"></param>
        /// <param name="Year"></param>
        /// <returns></returns>
        public ListIntResultResponse GetListPupilProfileID(int SchoolID, int Grade, int Year)
        {
            ListIntResultResponse obj = new ListIntResultResponse();
            try
            {
                int Min = 0;
                int Max = 0;
                if (Grade == WCFConstant.SCHOOL_PRIMARY_LEVEL)//cap 1
                {
                    Min = WCFConstant.MIN_PRIMARY;
                    Max = WCFConstant.MAX_PRIMARY;
                }
                else if (Grade == WCFConstant.SCHOOL_JUNIOR_LEVEL)//cap 2
                {
                    Min = WCFConstant.MIN_SECONDARY;
                    Max = WCFConstant.MAX_SECONDARY;
                }
                else if (Grade == WCFConstant.SCHOOL_HIGH_LEVEL)//cap 3
                {
                    Min = WCFConstant.MIN_TERTIARY;
                    Max = WCFConstant.MAX_TERTIARY;
                }
                else if (Grade == WCFConstant.SCHOOL_KINDERGARTEN_LEVEL) // Nha tre
                {
                    Min = WCFConstant.MIN_KINDERGARTEN;
                    Max = WCFConstant.MAX_KINDERGARTEN;
                }
                else if (Grade == WCFConstant.SCHOOL_PRESCHOOL_LEVEL) // Mau giao
                {
                    Min = WCFConstant.MIN_PRESCHOOL;
                    Max = WCFConstant.MAX_PRESCHOOL;
                }
                //Khoi tao Business
                DeclareBusiness();
                List<int> PupilProFileIDList = (from c in ClassProfileBusiness.All
                                                join pc in PupilOfClassBusiness.All on c.ClassProfileID equals pc.ClassID
                                                where pc.SchoolID == SchoolID
                                                && pc.Year == Year
                                                && (c.EducationLevelID >= Min && c.EducationLevelID <= Max)
                                                select pc.PupilID).Distinct().ToList();
                //gan validationCode thanh cong
                obj.ValidationCode = WCFConstant.RESPONSE_SUCCESS;
                //gan gia tri thuoc tinh dinh kem 
                obj.Result = PupilProFileIDList;
            }
            catch
            {
                //gan validationCode Exception
                obj.ValidationCode = WCFConstant.RESPONSE_EXCEPTION;
            }
            return obj;
        }
        #endregion
        #endregion

        #region lay du lieu gui tin nhan smsedu - TT30 cho Tieu hoc
        public EvaluationCommentsComposite getEvaluationCommnetsBySchoolOfPrimary(List<long> pupilIDList, int schoolID, int academicYearID, int classID, int semesterID, int monthID, int userAccountID, bool viewAll = true)
        {
            DeclareBusiness();
            AcademicYear academicYear = AcademicYearBusiness.Find(academicYearID);
            EvaluationCommentsComposite objResult = new EvaluationCommentsComposite();
            if (UtilsBusiness.IsMoveHistory(academicYear)) objResult = getEvaluationCommnetsBySchoolOfPrimaryHistory(pupilIDList, schoolID, academicYearID, classID, semesterID, monthID, userAccountID, viewAll);
            else objResult = getEvaluationCommnetsBySchoolOfPrimaryNoHistory(pupilIDList, schoolID, academicYearID, classID, semesterID, monthID, userAccountID, viewAll);
            return objResult;
        }

        private EvaluationCommentsComposite getEvaluationCommnetsBySchoolOfPrimaryNoHistory(List<long> pupilIDList, int schoolID, int academicYearID, int classID, int semesterID, int monthID, int userAccountID, bool viewAll = true)
        {
            EvaluationCommentsComposite objResult = new EvaluationCommentsComposite();
            try
            {
                DeclareBusiness();
                int TypeOfTeacher = 2;// GVCN
                int partitionId = UtilsBusiness.GetPartionId(schoolID);
                int partitionIDReward = UtilsBusiness.GetPartionId(schoolID, 20);
                //list hoc sinh trong lop
                List<PupilOfClassBO> pupilOfClassList = (from poc in PupilOfClassBusiness.GetPupilInClass(classID)
                                                         where pupilIDList.Contains(poc.PupilID)
                                                         select poc).OrderBy(c => c.OrderInClass).ThenBy(c => c.Name).ThenBy(c => c.PupilFullName).ToList();

                //list du lieu tra ve (1 hoc sinh thi co 1 dong nhan xet GLGD,PC,NL)
                List<EvaluationCommentsList> listResult = new List<EvaluationCommentsList>();
                EvaluationCommentsList obj = null;
                PupilOfClassBO pupilOfClassBO = null;
                int pupilID = 0;
                StringBuilder monthComments = null;
                string title = string.Empty;
                if (pupilOfClassList != null && pupilOfClassList.Count > 0)
                {
                    if (monthID < GlobalConstants.MonthID_11 && monthID > 0)
                    {
                        #region Danh gia theo thang
                        //list hoc sinh duoc danh gia GLGD nang luc và pham chat.
                        List<EvaluationComments> listEvaluationComments = (from ev in EvaluationCommentsBusiness.All
                                                                           where
                                                                           ev.LastDigitSchoolID == partitionId
                                                                           && ev.AcademicYearID == academicYearID
                                                                            && ev.ClassID == classID
                                                                            && ev.SemesterID == semesterID
                                                                            && ev.TypeOfTeacher == TypeOfTeacher
                                                                            && pupilIDList.Contains(ev.PupilID)
                                                                           select ev).ToList();
                        for (int i = 0; i < pupilOfClassList.Count; i++)
                        {
                            pupilOfClassBO = pupilOfClassList[i];
                            pupilID = pupilOfClassBO.PupilID;
                            obj = new EvaluationCommentsList();
                            obj.PupilID = pupilID;
                            obj.PupilName = pupilOfClassBO.PupilFullName;
                            monthComments = new StringBuilder();
                            //tao chuoi nhan xet (CLGD,PC,NL)
                            List<EvaluationComments> listCommentsBypupilID = listEvaluationComments.Where(p => p.PupilID == pupilID).ToList();

                            monthComments.Append("A) Môn học và HĐGD: ");
                            if (monthID == GlobalConstants.MonthID_1 || monthID == GlobalConstants.MonthID_6)
                            {
                                monthComments.Append(listCommentsBypupilID.Where(p => p.EvaluationID == GlobalConstants.TAB_EDUCATION_BOOKMARK_PRIMARY).Select(p => p.CommentsM1M6).FirstOrDefault());
                            }
                            else if (monthID == GlobalConstants.MonthID_2 || monthID == GlobalConstants.MonthID_7)
                            {
                                monthComments.Append(listCommentsBypupilID.Where(p => p.EvaluationID == GlobalConstants.TAB_EDUCATION_BOOKMARK_PRIMARY).Select(p => p.CommentsM2M7).FirstOrDefault());
                            }
                            else if (monthID == GlobalConstants.MonthID_3 || monthID == GlobalConstants.MonthID_8)
                            {
                                monthComments.Append(listCommentsBypupilID.Where(p => p.EvaluationID == GlobalConstants.TAB_EDUCATION_BOOKMARK_PRIMARY).Select(p => p.CommentsM3M8).FirstOrDefault());
                            }
                            else if (monthID == GlobalConstants.MonthID_4 || monthID == GlobalConstants.MonthID_9)
                            {
                                monthComments.Append(listCommentsBypupilID.Where(p => p.EvaluationID == GlobalConstants.TAB_EDUCATION_BOOKMARK_PRIMARY).Select(p => p.CommentsM4M9).FirstOrDefault());
                            }
                            else if (monthID == GlobalConstants.MonthID_5 || monthID == GlobalConstants.MonthID_10)
                            {
                                monthComments.Append(listCommentsBypupilID.Where(p => p.EvaluationID == GlobalConstants.TAB_EDUCATION_BOOKMARK_PRIMARY).Select(p => p.CommentsM5M10).FirstOrDefault());
                            }

                            if (monthComments != null)
                            {
                                monthComments.Append("\r\n");
                            }

                            monthComments.Append("B) Năng lực: ");
                            if (monthID == GlobalConstants.MonthID_1 || monthID == GlobalConstants.MonthID_6)
                            {
                                monthComments.Append(listCommentsBypupilID.Where(p => p.EvaluationID == GlobalConstants.TAB_CAPACTIES_BOOKMARK_PRIMARY).Select(p => p.CommentsM1M6).FirstOrDefault());
                            }
                            else if (monthID == GlobalConstants.MonthID_2 || monthID == GlobalConstants.MonthID_7)
                            {
                                monthComments.Append(listCommentsBypupilID.Where(p => p.EvaluationID == GlobalConstants.TAB_CAPACTIES_BOOKMARK_PRIMARY).Select(p => p.CommentsM2M7).FirstOrDefault());
                            }
                            else if (monthID == GlobalConstants.MonthID_3 || monthID == GlobalConstants.MonthID_8)
                            {
                                monthComments.Append(listCommentsBypupilID.Where(p => p.EvaluationID == GlobalConstants.TAB_CAPACTIES_BOOKMARK_PRIMARY).Select(p => p.CommentsM3M8).FirstOrDefault());
                            }
                            else if (monthID == GlobalConstants.MonthID_4 || monthID == GlobalConstants.MonthID_9)
                            {
                                monthComments.Append(listCommentsBypupilID.Where(p => p.EvaluationID == GlobalConstants.TAB_CAPACTIES_BOOKMARK_PRIMARY).Select(p => p.CommentsM4M9).FirstOrDefault());
                            }
                            else if (monthID == GlobalConstants.MonthID_5 || monthID == GlobalConstants.MonthID_10)
                            {
                                monthComments.Append(listCommentsBypupilID.Where(p => p.EvaluationID == GlobalConstants.TAB_CAPACTIES_BOOKMARK_PRIMARY).Select(p => p.CommentsM5M10).FirstOrDefault());
                            }
                            if (monthComments != null)
                            {
                                monthComments.Append("\r\n");
                            }

                            monthComments.Append("C) Phẩm chất: ");
                            if (monthID == GlobalConstants.MonthID_1 || monthID == GlobalConstants.MonthID_6)
                            {
                                monthComments.Append(listCommentsBypupilID.Where(p => p.EvaluationID == GlobalConstants.TAB_QUALITIES_BOOKMARK_PRIMARY).Select(p => p.CommentsM1M6).FirstOrDefault());
                            }
                            else if (monthID == GlobalConstants.MonthID_2 || monthID == GlobalConstants.MonthID_7)
                            {
                                monthComments.Append(listCommentsBypupilID.Where(p => p.EvaluationID == GlobalConstants.TAB_QUALITIES_BOOKMARK_PRIMARY).Select(p => p.CommentsM2M7).FirstOrDefault());
                            }
                            else if (monthID == GlobalConstants.MonthID_3 || monthID == GlobalConstants.MonthID_8)
                            {
                                monthComments.Append(listCommentsBypupilID.Where(p => p.EvaluationID == GlobalConstants.TAB_QUALITIES_BOOKMARK_PRIMARY).Select(p => p.CommentsM3M8).FirstOrDefault());
                            }
                            else if (monthID == GlobalConstants.MonthID_4 || monthID == GlobalConstants.MonthID_9)
                            {
                                monthComments.Append(listCommentsBypupilID.Where(p => p.EvaluationID == GlobalConstants.TAB_QUALITIES_BOOKMARK_PRIMARY).Select(p => p.CommentsM4M9).FirstOrDefault());
                            }
                            else if (monthID == GlobalConstants.MonthID_5 || monthID == GlobalConstants.MonthID_10)
                            {
                                monthComments.Append(listCommentsBypupilID.Where(p => p.EvaluationID == GlobalConstants.TAB_QUALITIES_BOOKMARK_PRIMARY).Select(p => p.CommentsM5M10).FirstOrDefault());
                            }
                            if (monthComments != null)
                            {
                                monthComments.Append("\r\n");
                            }

                            if (monthComments != null)
                            {
                                obj.EvaluationMonth = monthComments.ToString();
                            }
                            listResult.Add(obj);
                        }
                        #endregion
                    }
                    else if (monthID == GlobalConstants.MonthID_11)
                    {
                        #region Ket qua hoc ky
                        monthComments = new StringBuilder();
                        title = string.Empty;
                        //list mon hoc
                        List<SubjectCatBO> lsClassSubject = new List<SubjectCatBO>();
                        List<SummedEndingEvaluation> listSummedEndingEvaluationByPupilID = new List<SummedEndingEvaluation>();
                        if (classID > 0)
                        {
                            lsClassSubject = (from sb in ClassSubjectBusiness.All
                                              join sub in SubjectCatBusiness.All on sb.SubjectID equals sub.SubjectCatID
                                              orderby sb.IsCommenting, sub.OrderInSubject
                                              where sb.ClassID == classID
                                              && sub.IsActive == true
                                              && ((semesterID == GlobalConstants.SEMESTER_OF_YEAR_FIRST && sb.SectionPerWeekFirstSemester > 0)
                                              || (semesterID == GlobalConstants.SEMESTER_OF_YEAR_SECOND && sb.SectionPerWeekSecondSemester > 0))
                                              select new SubjectCatBO
                                              {
                                                  SubjectCatID = sb.SubjectID,
                                                  SubjectName = sub.SubjectName,
                                                  IsCommenting = sb.IsCommenting
                                              }
                                             ).ToList();
                        }

                        // list danh gia CLGD
                        List<SummedEvaluation> listSummedEvaluationRes = (from ev in SummedEvaluationBusiness.All
                                                                          where ev.LastDigitSchoolID == partitionId
                                                                           && ev.AcademicYearID == academicYearID
                                                                           && ev.ClassID == classID
                                                                           && ev.SemesterID == semesterID
                                                                           && pupilIDList.Contains(ev.PupilID)
                                                                          select ev).ToList();
                        //List Danh gia cua hoc sinh theo hoc ky
                        List<SummedEndingEvaluation> listSummedEndingEvaluationRes = (from sn in SummedEndingEvaluationBusiness.All
                                                                                      where sn.LastDigitSchoolID == partitionId
                                                                                      && sn.AcademicYearID == academicYearID
                                                                                      && sn.ClassID == classID
                                                                                      && (sn.SemesterID == semesterID || sn.SemesterID == GlobalConstants.SEMESTER_OF_YEAR_PRIMARY)
                                                                                      && pupilIDList.Contains(sn.PupilID)
                                                                                      select sn).ToList();
                        //List Danh sach khen thuong
                        List<RewardCommentFinal> listRewardCommentsFinalRes = (from rf in RewardCommentFinalBusiness.All
                                                                               where rf.LastDigitSchoolID == partitionIDReward
                                                                               && rf.AcademicYearID == academicYearID
                                                                               && rf.ClassID == classID
                                                                               && rf.SemesterID == semesterID
                                                                               && pupilIDList.Contains(rf.PupilID)
                                                                               select rf).ToList();

                        for (int i = 0; i < pupilOfClassList.Count; i++)
                        {
                            pupilOfClassBO = pupilOfClassList[i];
                            pupilID = pupilOfClassBO.PupilID;
                            obj = new EvaluationCommentsList();
                            obj.PupilID = pupilID;
                            obj.PupilName = pupilOfClassBO.PupilFullName;
                            //lay thong tinh diem va danh gia mon hoc
                            List<SummedEvaluation> listSummedEvaluationByPupilID = listSummedEvaluationRes.Where(p => p.PupilID == pupilID && p.EvaluationCriteriaID != 0).ToList();
                            SummedEvaluation summedEvaluationObj = null;
                            SubjectCatBO subjectBOOBJ = null;
                            int? iscommeting = null;
                            int subjectID = 0;
                            string subjectName = string.Empty;
                            monthComments = new StringBuilder();
                            for (int j = 0; j < lsClassSubject.Count; j++)
                            {
                                subjectBOOBJ = lsClassSubject[j];
                                subjectID = subjectBOOBJ.SubjectCatID;
                                summedEvaluationObj = listSummedEvaluationByPupilID.Where(p => p.EvaluationCriteriaID == subjectID).FirstOrDefault();// 1 hoc sinh 1 hoc ki chi co duy nhat 1 dong
                                subjectName = subjectBOOBJ.SubjectName;
                                iscommeting = subjectBOOBJ.IsCommenting; //0: tinh diem, 1: nhan xet

                                // if (!string.IsNullOrEmpty(summedEvaluationObj.EndingEvaluation) || summedEvaluationObj.PeriodicEndingMark.HasValue)
                                // {
                                monthComments.Append(subjectName + ": ");
                                if (summedEvaluationObj != null)
                                {
                                    if (!string.IsNullOrEmpty(summedEvaluationObj.EndingEvaluation))
                                    {
                                        monthComments.Append(summedEvaluationObj.EndingEvaluation); // danh gia
                                    }

                                    if (iscommeting != GlobalConstants.SUBJECTCAT_ISCOMMENTING_MARK)
                                    {
                                        if (j != lsClassSubject.Count - 1)
                                        {
                                            monthComments.Append("; ");
                                        }
                                    }
                                    else
                                    {
                                        if (!string.IsNullOrEmpty(summedEvaluationObj.EndingEvaluation))
                                        {
                                            if (summedEvaluationObj.PeriodicEndingMark.HasValue)
                                            {
                                                monthComments.Append(", KTCK " + summedEvaluationObj.PeriodicEndingMark + "; ");
                                            }
                                        }
                                        else
                                        {
                                            if (summedEvaluationObj.PeriodicEndingMark.HasValue)
                                            {
                                                monthComments.Append(" KTCK " + summedEvaluationObj.PeriodicEndingMark + "; ");
                                            }
                                        }
                                    }
                                }
                                //  }
                            }
                            if (monthComments != null)
                            {
                                monthComments.Append("\r\n");
                            }

                            // lay thong tin danh gia NL,PC
                            listSummedEndingEvaluationByPupilID = listSummedEndingEvaluationRes.Where(p => p.PupilID == pupilID && p.SemesterID == semesterID).ToList();// max 2 record

                            monthComments.Append("Năng lực: ");
                            monthComments.Append(listSummedEndingEvaluationByPupilID.Where(p => p.EvaluationID == GlobalConstants.TAB_CAPACTIES_BOOKMARK_PRIMARY).Select(p => ("Đ".Equals(p.EndingEvaluation) ? "Đạt" : "Chưa đạt")).FirstOrDefault());
                            monthComments.Append("\r\n");

                            monthComments.Append("Phẩm chất: ");
                            monthComments.Append(listSummedEndingEvaluationByPupilID.Where(p => p.EvaluationID == GlobalConstants.TAB_QUALITIES_BOOKMARK_PRIMARY).Select(p => ("Đ".Equals(p.EndingEvaluation) ? "Đạt" : "Chưa đạt")).FirstOrDefault());
                            monthComments.Append("\r\n");

                            //lay thong tin khen thuong
                            string strRewardID = listRewardCommentsFinalRes.Where(p => p.PupilID == pupilID).Select(p => p.RewardID).FirstOrDefault(); // khen thuong
                            monthComments.Append("Khen thưởng: ");
                            if (!string.IsNullOrEmpty(strRewardID))
                            {
                                List<int> listInt = strRewardID.Split(new[] { "," }, StringSplitOptions.RemoveEmptyEntries).Select(p => int.Parse(p)).ToList();
                                List<RewardFinal> rewardFinalList = RewardFinalBusiness.All.Where(p => p.SchoolID == schoolID).ToList();
                                string rewardMone = string.Empty;
                                for (int g = 0; g < listInt.Count; g++)
                                {
                                    rewardMone = rewardFinalList.Where(p => p.RewardFinalID == listInt[g]).Select(p => p.RewardMode).FirstOrDefault();
                                    if (!string.IsNullOrWhiteSpace(rewardMone))
                                    {
                                        monthComments.Append(rewardMone);
                                        if (g != listInt.Count - 1)
                                        {
                                            monthComments.Append(", ");
                                        }
                                    }
                                }
                            }
                            monthComments.Append("\r\n");

                            // Danh gia hoan thanh chuong trinh neu la hoc ki 2
                            if (semesterID == GlobalConstants.SEMESTER_OF_YEAR_SECOND)
                            {
                                monthComments.Append("Đánh giá: ");
                                string evaluationEnding = listSummedEndingEvaluationRes
                                    .Where(p => p.PupilID == pupilID && p.SemesterID == GlobalConstants.SEMESTER_OF_YEAR_PRIMARY && p.EvaluationID == 4)
                                    .Select(p => !string.IsNullOrEmpty(p.EvaluationReTraining) ? p.EvaluationReTraining : p.EndingEvaluation).FirstOrDefault();
                                if ("HT".Equals(evaluationEnding))
                                {
                                    monthComments.Append(string.Format(WCFConstant.COURSE_COMPLETE, pupilOfClassBO.EducationLevelID));
                                }
                                else if ("CHT".Equals(evaluationEnding))
                                {
                                    monthComments.Append(string.Format(WCFConstant.NOT_COURSE_COMPLETE, pupilOfClassBO.EducationLevelID));
                                }
                            }
                            if (monthComments != null)
                            {
                                obj.EvaluationMonth = monthComments.ToString();
                            }
                            listResult.Add(obj);
                        }
                        #endregion
                    }
                    else
                    {
                        #region Nhan xet cuoi ky
                        List<RewardCommentFinal> listRewardCommentsFinalRes = (from rf in RewardCommentFinalBusiness.All
                                                                               where rf.LastDigitSchoolID == partitionIDReward
                                                                               && rf.AcademicYearID == academicYearID
                                                                               && rf.ClassID == classID
                                                                               && rf.SemesterID == semesterID
                                                                               && pupilIDList.Contains(rf.PupilID)
                                                                               select rf).ToList();
                        for (int i = 0; i < pupilOfClassList.Count; i++)
                        {
                            monthComments = new StringBuilder();
                            pupilOfClassBO = pupilOfClassList[i];
                            pupilID = pupilOfClassBO.PupilID;
                            obj = new EvaluationCommentsList();
                            obj.PupilID = pupilID;
                            obj.PupilName = pupilOfClassBO.PupilFullName;

                            // lay thong tin nhan xet
                            monthComments.Append(listRewardCommentsFinalRes.Where(p => p.PupilID == pupilID).Select(p => p.OutstandingAchievement).FirstOrDefault());

                            if (monthComments != null)
                            {
                                obj.EvaluationMonth = monthComments.ToString();
                            }
                            listResult.Add(obj);
                        }
                        #endregion
                    }
                }
                objResult.ListResult = listResult;
                objResult.ValidateCode = WCFConstant.RESPONSE_SUCCESS;
            }
            catch (Exception ex)
            {
                LogExtensions.GetCurrentClass = System.Reflection.MethodBase.GetCurrentMethod().DeclaringType;
                string para = string.Format("schoolID={0}, academicYearID={1}, classID={2}, semesterID={3}, monthID={4}, userAccountID={5}", schoolID, academicYearID, classID, semesterID, monthID, userAccountID);
                LogExtensions.ErrorExt(logger, DateTime.Now, para, ex.Message, "null");
                objResult.ValidateCode = WCFConstant.RESPONSE_EXCEPTION;
            }
            return objResult;
        }

        private EvaluationCommentsComposite getEvaluationCommnetsBySchoolOfPrimaryHistory(List<long> pupilIDList, int schoolID, int academicYearID, int classID, int semesterID, int monthID, int userAccountID, bool viewAll = true)
        {
            EvaluationCommentsComposite objResult = new EvaluationCommentsComposite();
            try
            {
                DeclareBusiness();
                int TypeOfTeacher = 2;// GVCN
                int partitionId = UtilsBusiness.GetPartionId(schoolID);
                int partitionIDReward = UtilsBusiness.GetPartionId(schoolID, 20);
                //list hoc sinh trong lop
                List<PupilOfClassBO> pupilOfClassList = (from poc in PupilOfClassBusiness.GetPupilInClass(classID)
                                                         where pupilIDList.Contains(poc.PupilID)
                                                         select poc).OrderBy(c => c.OrderInClass).ThenBy(c => c.Name).ThenBy(c => c.PupilFullName).ToList();

                //list du lieu tra ve (1 hoc sinh thi co 1 dong nhan xet GLGD,PC,NL)
                List<EvaluationCommentsList> listResult = new List<EvaluationCommentsList>();
                EvaluationCommentsList obj = null;
                PupilOfClassBO pupilOfClassBO = null;
                int pupilID = 0;
                StringBuilder monthComments = null;
                string title = string.Empty;
                if (pupilOfClassList != null && pupilOfClassList.Count > 0)
                {
                    if (monthID < GlobalConstants.MonthID_11 && monthID > 0)
                    {
                        #region Danh gia theo thang
                        //list hoc sinh duoc danh gia GLGD nang luc và pham chat.
                        List<EvaluationCommentsHistory> listEvaluationComments = (from ev in EvaluationCommentsHistoryBusiness.All
                                                                                  where
                                                                                  ev.LastDigitSchoolID == partitionId
                                                                                  && ev.AcademicYearID == academicYearID
                                                                                   && ev.ClassID == classID
                                                                                   && ev.SemesterID == semesterID
                                                                                   && ev.TypeOfTeacher == TypeOfTeacher
                                                                                   && pupilIDList.Contains(ev.PupilID)
                                                                                  select ev).ToList();
                        for (int i = 0; i < pupilOfClassList.Count; i++)
                        {
                            pupilOfClassBO = pupilOfClassList[i];
                            pupilID = pupilOfClassBO.PupilID;
                            obj = new EvaluationCommentsList();
                            obj.PupilID = pupilID;
                            obj.PupilName = pupilOfClassBO.PupilFullName;
                            monthComments = new StringBuilder();
                            //tao chuoi nhan xet (CLGD,PC,NL)
                            List<EvaluationCommentsHistory> listCommentsBypupilID = listEvaluationComments.Where(p => p.PupilID == pupilID).ToList();
                            EvaluationCommentsHistory evaluationCommentsObj = null;
                            for (int j = 0; j < listCommentsBypupilID.Count; j++)
                            {
                                if ((j + 1) == GlobalConstants.TAB_EDUCATION_BOOKMARK_PRIMARY)
                                {
                                    evaluationCommentsObj = listCommentsBypupilID.Where(p => p.EvaluationID == GlobalConstants.TAB_EDUCATION_BOOKMARK_PRIMARY).FirstOrDefault();
                                    if ((!string.IsNullOrEmpty(evaluationCommentsObj.CommentsM1M6) && (monthID == GlobalConstants.MonthID_1 || monthID == GlobalConstants.MonthID_6))
                                        || (!string.IsNullOrEmpty(evaluationCommentsObj.CommentsM2M7) && (monthID == GlobalConstants.MonthID_2 || monthID == GlobalConstants.MonthID_7))
                                        || (!string.IsNullOrEmpty(evaluationCommentsObj.CommentsM3M8) && (monthID == GlobalConstants.MonthID_3 || monthID == GlobalConstants.MonthID_8))
                                        || (!string.IsNullOrEmpty(evaluationCommentsObj.CommentsM4M9) && (monthID == GlobalConstants.MonthID_4 || monthID == GlobalConstants.MonthID_9))
                                        || (!string.IsNullOrEmpty(evaluationCommentsObj.CommentsM5M10) && (monthID == GlobalConstants.MonthID_5 || monthID == GlobalConstants.MonthID_10)))
                                    {
                                        monthComments.Append("A) Môn học và HĐGD: ");
                                        if (monthID == GlobalConstants.MonthID_1 || monthID == GlobalConstants.MonthID_6)
                                        {
                                            monthComments.Append(listCommentsBypupilID.Where(p => p.EvaluationID == GlobalConstants.TAB_EDUCATION_BOOKMARK_PRIMARY).Select(p => p.CommentsM1M6).FirstOrDefault());
                                        }
                                        else if (monthID == GlobalConstants.MonthID_2 || monthID == GlobalConstants.MonthID_7)
                                        {
                                            monthComments.Append(listCommentsBypupilID.Where(p => p.EvaluationID == GlobalConstants.TAB_EDUCATION_BOOKMARK_PRIMARY).Select(p => p.CommentsM2M7).FirstOrDefault());
                                        }
                                        else if (monthID == GlobalConstants.MonthID_3 || monthID == GlobalConstants.MonthID_8)
                                        {
                                            monthComments.Append(listCommentsBypupilID.Where(p => p.EvaluationID == GlobalConstants.TAB_EDUCATION_BOOKMARK_PRIMARY).Select(p => p.CommentsM3M8).FirstOrDefault());
                                        }
                                        else if (monthID == GlobalConstants.MonthID_4 || monthID == GlobalConstants.MonthID_9)
                                        {
                                            monthComments.Append(listCommentsBypupilID.Where(p => p.EvaluationID == GlobalConstants.TAB_EDUCATION_BOOKMARK_PRIMARY).Select(p => p.CommentsM4M9).FirstOrDefault());
                                        }
                                        else if (monthID == GlobalConstants.MonthID_5 || monthID == GlobalConstants.MonthID_10)
                                        {
                                            monthComments.Append(listCommentsBypupilID.Where(p => p.EvaluationID == GlobalConstants.TAB_EDUCATION_BOOKMARK_PRIMARY).Select(p => p.CommentsM5M10).FirstOrDefault());
                                        }

                                        if (monthComments != null)
                                        {
                                            monthComments.Append("\r\n");
                                        }
                                    }
                                }
                                else if ((j + 1) == GlobalConstants.TAB_CAPACTIES_BOOKMARK_PRIMARY)
                                {
                                    evaluationCommentsObj = listCommentsBypupilID.Where(p => p.EvaluationID == GlobalConstants.TAB_CAPACTIES_BOOKMARK_PRIMARY).FirstOrDefault();
                                    if ((!string.IsNullOrEmpty(evaluationCommentsObj.CommentsM1M6) && (monthID == GlobalConstants.MonthID_1 || monthID == GlobalConstants.MonthID_6))
                                       || (!string.IsNullOrEmpty(evaluationCommentsObj.CommentsM2M7) && (monthID == GlobalConstants.MonthID_2 || monthID == GlobalConstants.MonthID_7))
                                       || (!string.IsNullOrEmpty(evaluationCommentsObj.CommentsM3M8) && (monthID == GlobalConstants.MonthID_3 || monthID == GlobalConstants.MonthID_8))
                                       || (!string.IsNullOrEmpty(evaluationCommentsObj.CommentsM4M9) && (monthID == GlobalConstants.MonthID_4 || monthID == GlobalConstants.MonthID_9))
                                       || (!string.IsNullOrEmpty(evaluationCommentsObj.CommentsM5M10) && (monthID == GlobalConstants.MonthID_5 || monthID == GlobalConstants.MonthID_10)))
                                    {
                                        monthComments.Append("B) Năng lực: ");
                                        if (monthID == GlobalConstants.MonthID_1 || monthID == GlobalConstants.MonthID_6)
                                        {
                                            monthComments.Append(listCommentsBypupilID.Where(p => p.EvaluationID == GlobalConstants.TAB_CAPACTIES_BOOKMARK_PRIMARY).Select(p => p.CommentsM1M6).FirstOrDefault());
                                        }
                                        else if (monthID == GlobalConstants.MonthID_2 || monthID == GlobalConstants.MonthID_7)
                                        {
                                            monthComments.Append(listCommentsBypupilID.Where(p => p.EvaluationID == GlobalConstants.TAB_CAPACTIES_BOOKMARK_PRIMARY).Select(p => p.CommentsM2M7).FirstOrDefault());
                                        }
                                        else if (monthID == GlobalConstants.MonthID_3 || monthID == GlobalConstants.MonthID_8)
                                        {
                                            monthComments.Append(listCommentsBypupilID.Where(p => p.EvaluationID == GlobalConstants.TAB_CAPACTIES_BOOKMARK_PRIMARY).Select(p => p.CommentsM3M8).FirstOrDefault());
                                        }
                                        else if (monthID == GlobalConstants.MonthID_4 || monthID == GlobalConstants.MonthID_9)
                                        {
                                            monthComments.Append(listCommentsBypupilID.Where(p => p.EvaluationID == GlobalConstants.TAB_CAPACTIES_BOOKMARK_PRIMARY).Select(p => p.CommentsM4M9).FirstOrDefault());
                                        }
                                        else if (monthID == GlobalConstants.MonthID_5 || monthID == GlobalConstants.MonthID_10)
                                        {
                                            monthComments.Append(listCommentsBypupilID.Where(p => p.EvaluationID == GlobalConstants.TAB_CAPACTIES_BOOKMARK_PRIMARY).Select(p => p.CommentsM5M10).FirstOrDefault());
                                        }
                                        if (monthComments != null)
                                        {
                                            monthComments.Append("\r\n");
                                        }
                                    }
                                }
                                else if ((j + 1) == GlobalConstants.TAB_QUALITIES_BOOKMARK_PRIMARY)
                                {
                                    evaluationCommentsObj = listCommentsBypupilID.Where(p => p.EvaluationID == GlobalConstants.TAB_QUALITIES_BOOKMARK_PRIMARY).FirstOrDefault();
                                    if ((!string.IsNullOrEmpty(evaluationCommentsObj.CommentsM1M6) && (monthID == GlobalConstants.MonthID_1 || monthID == GlobalConstants.MonthID_6))
                                        || (!string.IsNullOrEmpty(evaluationCommentsObj.CommentsM2M7) && (monthID == GlobalConstants.MonthID_2 || monthID == GlobalConstants.MonthID_7))
                                        || (!string.IsNullOrEmpty(evaluationCommentsObj.CommentsM3M8) && (monthID == GlobalConstants.MonthID_3 || monthID == GlobalConstants.MonthID_8))
                                        || (!string.IsNullOrEmpty(evaluationCommentsObj.CommentsM4M9) && (monthID == GlobalConstants.MonthID_4 || monthID == GlobalConstants.MonthID_9))
                                        || (!string.IsNullOrEmpty(evaluationCommentsObj.CommentsM5M10) && (monthID == GlobalConstants.MonthID_5 || monthID == GlobalConstants.MonthID_10)))
                                    {
                                        monthComments.Append("C) Phẩm chất: ");
                                        if (monthID == GlobalConstants.MonthID_1 || monthID == GlobalConstants.MonthID_6)
                                        {
                                            monthComments.Append(listCommentsBypupilID.Where(p => p.EvaluationID == GlobalConstants.TAB_QUALITIES_BOOKMARK_PRIMARY).Select(p => p.CommentsM1M6).FirstOrDefault());
                                        }
                                        else if (monthID == GlobalConstants.MonthID_2 || monthID == GlobalConstants.MonthID_7)
                                        {
                                            monthComments.Append(listCommentsBypupilID.Where(p => p.EvaluationID == GlobalConstants.TAB_QUALITIES_BOOKMARK_PRIMARY).Select(p => p.CommentsM2M7).FirstOrDefault());
                                        }
                                        else if (monthID == GlobalConstants.MonthID_3 || monthID == GlobalConstants.MonthID_8)
                                        {
                                            monthComments.Append(listCommentsBypupilID.Where(p => p.EvaluationID == GlobalConstants.TAB_QUALITIES_BOOKMARK_PRIMARY).Select(p => p.CommentsM3M8).FirstOrDefault());
                                        }
                                        else if (monthID == GlobalConstants.MonthID_4 || monthID == GlobalConstants.MonthID_9)
                                        {
                                            monthComments.Append(listCommentsBypupilID.Where(p => p.EvaluationID == GlobalConstants.TAB_QUALITIES_BOOKMARK_PRIMARY).Select(p => p.CommentsM4M9).FirstOrDefault());
                                        }
                                        else if (monthID == GlobalConstants.MonthID_5 || monthID == GlobalConstants.MonthID_10)
                                        {
                                            monthComments.Append(listCommentsBypupilID.Where(p => p.EvaluationID == GlobalConstants.TAB_QUALITIES_BOOKMARK_PRIMARY).Select(p => p.CommentsM5M10).FirstOrDefault());
                                        }
                                        if (monthComments != null)
                                        {
                                            monthComments.Append("\r\n");
                                        }
                                    }
                                }
                            }
                            if (monthComments != null)
                            {
                                obj.EvaluationMonth = monthComments.ToString();
                            }
                            listResult.Add(obj);
                        }
                        #endregion
                    }
                    else if (monthID == GlobalConstants.MonthID_11)
                    {
                        #region Ket qua hoc ky
                        monthComments = new StringBuilder();
                        title = string.Empty;
                        //list mon hoc
                        List<SubjectCat> lsClassSubject = new List<SubjectCat>();
                        if (classID > 0)
                        {
                            lsClassSubject = (from sb in ClassSubjectBusiness.All
                                              join sub in SubjectCatBusiness.All on sb.SubjectID equals sub.SubjectCatID
                                              orderby sub.OrderInSubject, sub.DisplayName
                                              where sb.ClassID == classID
                                              && sub.IsActive == true
                                              select sub
                                             ).ToList();
                        }

                        // list danh gia CLGD
                        List<SummedEvaluationHistory> listSummedEvaluationRes = (from ev in SummedEvaluationHistoryBusiness.All
                                                                                 where ev.LastDigitSchoolID == partitionId
                                                                                  && ev.AcademicYearID == academicYearID
                                                                                  && ev.ClassID == classID
                                                                                  && ev.SemesterID == semesterID
                                                                                  && pupilIDList.Contains(ev.PupilID)
                                                                                 select ev).ToList();
                        //List Danh gia cua hoc sinh theo hoc ky
                        List<SummedEndingEvaluation> listSummedEndingEvaluationRes = (from sn in SummedEndingEvaluationBusiness.All
                                                                                      where sn.LastDigitSchoolID == partitionId
                                                                                      && sn.AcademicYearID == academicYearID
                                                                                      && sn.ClassID == classID
                                                                                      && (sn.SemesterID == semesterID || sn.SemesterID == GlobalConstants.SEMESTER_OF_YEAR_PRIMARY)
                                                                                      && pupilIDList.Contains(sn.PupilID)
                                                                                      select sn).ToList();
                        //List Danh sach khen thuong
                        List<RewardCommentFinal> listRewardCommentsFinalRes = (from rf in RewardCommentFinalBusiness.All
                                                                               where rf.LastDigitSchoolID == partitionIDReward
                                                                               && rf.AcademicYearID == academicYearID
                                                                               && rf.ClassID == classID
                                                                               && rf.SemesterID == semesterID
                                                                               && pupilIDList.Contains(rf.PupilID)
                                                                               select rf).ToList();

                        for (int i = 0; i < pupilOfClassList.Count; i++)
                        {
                            pupilOfClassBO = pupilOfClassList[i];
                            pupilID = pupilOfClassBO.PupilID;
                            obj = new EvaluationCommentsList();
                            obj.PupilID = pupilID;
                            obj.PupilName = pupilOfClassBO.PupilFullName;
                            //lay thong tinh diem va danh gia mon hoc
                            List<SummedEvaluationHistory> listSummedEvaluationByPupilID = listSummedEvaluationRes.Where(p => p.PupilID == pupilID && p.EvaluationCriteriaID != 0).ToList();
                            SummedEvaluationHistory summedEvaluationObj = null;
                            int iscommeting = -1;
                            int subjectID = 0;
                            string subjectName = string.Empty;
                            monthComments = new StringBuilder();
                            for (int j = 0; j < listSummedEvaluationByPupilID.Count; j++)
                            {
                                summedEvaluationObj = listSummedEvaluationByPupilID[j];
                                subjectID = summedEvaluationObj.EvaluationCriteriaID;
                                subjectName = lsClassSubject.Where(p => p.SubjectCatID == subjectID).Select(p => p.SubjectName).FirstOrDefault();
                                iscommeting = lsClassSubject.Where(p => p.SubjectCatID == subjectID).Select(p => p.IsCommenting).FirstOrDefault(); //0: tinh diem, 1: nhan xet

                                if (!string.IsNullOrEmpty(summedEvaluationObj.EndingEvaluation) || summedEvaluationObj.PeriodicEndingMark.HasValue)
                                {
                                    monthComments.Append(subjectName + ": ");
                                    if (!string.IsNullOrEmpty(summedEvaluationObj.EndingEvaluation))
                                    {
                                        monthComments.Append(summedEvaluationObj.EndingEvaluation); // danh gia
                                    }

                                    if (iscommeting != GlobalConstants.SUBJECTCAT_ISCOMMENTING_MARK)
                                    {
                                        monthComments.Append("; ");
                                    }

                                    if (iscommeting == GlobalConstants.SUBJECTCAT_ISCOMMENTING_MARK)
                                    {
                                        if (!string.IsNullOrEmpty(summedEvaluationObj.EndingEvaluation))
                                        {
                                            if (summedEvaluationObj.PeriodicEndingMark.HasValue)
                                            {
                                                monthComments.Append(", KTCK: " + summedEvaluationObj.PeriodicEndingMark + "; ");
                                            }
                                        }
                                        else
                                        {
                                            if (summedEvaluationObj.PeriodicEndingMark.HasValue)
                                            {
                                                monthComments.Append(" KTCK: " + summedEvaluationObj.PeriodicEndingMark + "; ");
                                            }
                                        }
                                    }
                                }
                            }
                            if (monthComments != null)
                            {
                                monthComments.Append("\r\n");
                            }

                            // lay thong tin danh gia NL,PC
                            List<SummedEndingEvaluation> listSummedEndingEvaluationByPupilID = listSummedEndingEvaluationRes.Where(p => p.PupilID == pupilID && p.SemesterID == semesterID).ToList();// max 2 record
                            string evalutionResult = string.Empty;
                            for (int k = 0; k < listSummedEndingEvaluationByPupilID.Count; k++)
                            {
                                if (listSummedEndingEvaluationByPupilID[k].EvaluationID == GlobalConstants.TAB_CAPACTIES_BOOKMARK_PRIMARY)
                                {
                                    evalutionResult = listSummedEndingEvaluationByPupilID.Where(p => p.EvaluationID == GlobalConstants.TAB_CAPACTIES_BOOKMARK_PRIMARY).Select(p => p.EndingEvaluation).FirstOrDefault();
                                    if (!string.IsNullOrWhiteSpace(evalutionResult))
                                    {
                                        monthComments.Append("Năng lực: ");
                                        monthComments.Append("Đ".Equals(evalutionResult) ? "Đạt" : "Chưa đạt");
                                        monthComments.Append("\r\n");
                                    }
                                }
                                else if (listSummedEndingEvaluationByPupilID[k].EvaluationID == GlobalConstants.TAB_QUALITIES_BOOKMARK_PRIMARY)
                                {
                                    evalutionResult = listSummedEndingEvaluationByPupilID.Where(p => p.EvaluationID == GlobalConstants.TAB_QUALITIES_BOOKMARK_PRIMARY).Select(p => p.EndingEvaluation).FirstOrDefault();
                                    if (!string.IsNullOrWhiteSpace(evalutionResult))
                                    {
                                        monthComments.Append("Phẩm chất: ");
                                        monthComments.Append("Đ".Equals(evalutionResult) ? "Đạt" : "Chưa đạt");
                                        monthComments.Append("\r\n");
                                    }
                                }
                            }

                            //lay thong tin khen thuong
                            string strRewardID = listRewardCommentsFinalRes.Where(p => p.PupilID == pupilID).Select(p => p.RewardID).FirstOrDefault(); // khen thuong
                            if (!string.IsNullOrEmpty(strRewardID))
                            {
                                List<int> listInt = strRewardID.Split(new[] { "," }, StringSplitOptions.RemoveEmptyEntries).Select(p => int.Parse(p)).ToList();
                                List<RewardFinal> rewardFinalList = RewardFinalBusiness.All.Where(p => p.SchoolID == schoolID).ToList();
                                string rewardMone = string.Empty;
                                for (int g = 0; g < listInt.Count; g++)
                                {
                                    rewardMone = rewardFinalList.Where(p => p.RewardFinalID == listInt[g]).Select(p => p.RewardMode).FirstOrDefault();
                                    if (!string.IsNullOrWhiteSpace(rewardMone))
                                    {
                                        monthComments.Append(rewardMone);
                                        if (g == listInt.Count - 1)
                                        {
                                            monthComments.Append(", ");
                                        }
                                    }
                                }
                                if (listInt != null && listInt.Count > 0)
                                {
                                    monthComments.Append("\r\n");
                                }
                            }

                            // Danh gia hoan thanh chuong trinh neu la hoc ki 2
                            if (semesterID == GlobalConstants.SEMESTER_OF_YEAR_SECOND)
                            {
                                string evaluationEnding = string.Empty;
                                evaluationEnding = listSummedEndingEvaluationRes
                                    .Where(p => p.PupilID == pupilID && p.SemesterID == GlobalConstants.SEMESTER_OF_YEAR_PRIMARY && p.EvaluationID == 4)
                                    .Select(p => !string.IsNullOrEmpty(p.EvaluationReTraining) ? p.EvaluationReTraining : p.EndingEvaluation).FirstOrDefault();
                                monthComments.Append(evaluationEnding);
                            }

                            if (monthComments != null)
                            {
                                obj.EvaluationMonth = monthComments.ToString();
                            }
                            listResult.Add(obj);
                        }
                        #endregion
                    }
                    else
                    {
                        #region Nhan xet cuoi ky
                        List<RewardCommentFinal> listRewardCommentsFinalRes = (from rf in RewardCommentFinalBusiness.All
                                                                               where rf.LastDigitSchoolID == partitionIDReward
                                                                               && rf.AcademicYearID == academicYearID
                                                                               && rf.ClassID == classID
                                                                               && rf.SemesterID == semesterID
                                                                               && pupilIDList.Contains(rf.PupilID)
                                                                               select rf).ToList();
                        for (int i = 0; i < pupilOfClassList.Count; i++)
                        {
                            pupilOfClassBO = pupilOfClassList[i];
                            pupilID = pupilOfClassBO.PupilID;
                            obj = new EvaluationCommentsList();
                            obj.PupilID = pupilID;
                            obj.PupilName = pupilOfClassBO.PupilFullName;

                            // lay thong tin nhan xet
                            monthComments.Append(listRewardCommentsFinalRes.Where(p => p.PupilID == pupilID).Select(p => p.OutstandingAchievement).FirstOrDefault());
                            if (monthComments != null)
                            {
                                obj.EvaluationMonth = monthComments.ToString();
                            }
                            listResult.Add(obj);
                        }
                        #endregion
                    }
                }
                objResult.ListResult = listResult;
                objResult.ValidateCode = WCFConstant.RESPONSE_SUCCESS;
            }
            catch (Exception ex)
            {
                LogExtensions.GetCurrentClass = System.Reflection.MethodBase.GetCurrentMethod().DeclaringType;
                string para = string.Format("schoolID={0}, academicYearID={1}, classID={2}, semesterID={3}, monthID={4}, userAccountID={5}", schoolID, academicYearID, classID, semesterID, monthID, userAccountID);
                LogExtensions.ErrorExt(logger, DateTime.Now, para, ex.Message, "null");
                objResult.ValidateCode = WCFConstant.RESPONSE_EXCEPTION;
            }
            return objResult;
        }
        #endregion

        #region lay du lieu sparent - TT30 cho Tieu hoc, VNEN cho THCS
        public EvaluationCommentsPupilComposite getEvaluationCommentsSparentMonthBySchool(int schoolID, int academicYearID, int classID, int semesterID, int monthID, int pupilID, bool viewAllmark, bool viewSemester1, bool viewSemester2)
        {
            DeclareBusiness();
            AcademicYear academicYear = AcademicYearBusiness.Find(academicYearID);
            EvaluationCommentsPupilComposite ObjResult = new EvaluationCommentsPupilComposite();
            if (UtilsBusiness.IsMoveHistory(academicYear))
                ObjResult = getEvaluationCommentsSparentMonthBySchoolHistory(schoolID, academicYearID, classID, semesterID, monthID, pupilID, viewAllmark, viewSemester1, viewSemester2);
            else ObjResult = getEvaluationCommentsSparentMonthBySchoolNoHistory(schoolID, academicYearID, classID, semesterID, monthID, pupilID, viewAllmark, viewSemester1, viewSemester2);
            return ObjResult;
        }

        public EvaluationCommentsPupilComposite getEvaluationCommentsSparentMonthBySchoolNoHistory(int schoolID, int academicYearID, int classID, int semesterID, int monthID, int pupilID, bool viewAllmark, bool viewSemester1, bool viewSemester2)
        {
            //Khoi tao Business
            DeclareBusiness();
            int TypeOfTeacher = 2;// GVCN
            int partitionId = UtilsBusiness.GetPartionId(schoolID);
            int partitionReward = UtilsBusiness.GetPartionId(schoolID, 20);
            ClassProfile classProfileBO = ClassProfileBusiness.All.Where(p => p.ClassProfileID == classID).FirstOrDefault();
            EvaluationCommentsPupilComposite ObjResult = new EvaluationCommentsPupilComposite();
            List<EvaluationCommentsPupil> listComments = new List<EvaluationCommentsPupil>();
            List<EducationQuality> listEducationQuality = new List<EducationQuality>();
            List<CapacitiesAndQualities> listCapacitiesAndQualities = new List<CapacitiesAndQualities>();
            List<SubjectList> listSubjectResult = new List<SubjectList>();


            try
            {
                #region Nhan xet thang cua 1 hoc sinh
                EvaluationCommentsPupil resultObj = null;
                List<EvaluationComments> evaluationCommentsList = (from ev in EvaluationCommentsBusiness.All
                                                                   where
                                                                   ev.LastDigitSchoolID == partitionId
                                                                   && ev.AcademicYearID == academicYearID
                                                                    && ev.ClassID == classID
                                                                    && (ev.SemesterID == GlobalConstants.SEMESTER_OF_YEAR_SECOND || ev.SemesterID == GlobalConstants.SEMESTER_OF_YEAR_FIRST)
                                                                    && ev.TypeOfTeacher == TypeOfTeacher
                                                                    && ev.PupilID == pupilID
                                                                   select ev).ToList();
                //if (viewAllmark)
                //{
                if (evaluationCommentsList != null && evaluationCommentsList.Count > 0)
                {
                    for (int i = 0; i < evaluationCommentsList.Count; i++)
                    {
                        EvaluationComments objRes = evaluationCommentsList[i];
                        resultObj = new EvaluationCommentsPupil();
                        resultObj.PupilID = objRes.PupilID;
                        resultObj.EvaluationID = objRes.EvaluationID;
                        resultObj.SemesterID = objRes.SemesterID;
                        if (objRes.SemesterID == GlobalConstants.SEMESTER_OF_YEAR_FIRST)
                        {
                            resultObj.EvaluationMonth1 = objRes.CommentsM1M6;
                            resultObj.EvaluationMonth2 = objRes.CommentsM2M7;
                            resultObj.EvaluationMonth3 = objRes.CommentsM3M8;
                            resultObj.EvaluationMonth4 = objRes.CommentsM4M9;
                            resultObj.EvaluationMonth5 = objRes.CommentsM5M10;
                        }

                        if (objRes.SemesterID == GlobalConstants.SEMESTER_OF_YEAR_SECOND)
                        {
                            resultObj.EvaluationMonth6 = objRes.CommentsM1M6;
                            resultObj.EvaluationMonth7 = objRes.CommentsM2M7;
                            resultObj.EvaluationMonth8 = objRes.CommentsM3M8;
                            resultObj.EvaluationMonth9 = objRes.CommentsM4M9;
                            resultObj.EvaluationMonth10 = objRes.CommentsM5M10;
                        }
                        listComments.Add(resultObj);
                        ObjResult.ListResult = listComments;
                        ObjResult.ValidateCode = WCFConstant.RESPONSE_SUCCESS;
                    }
                }
                //}
                //else
                //{
                //    //chi xem cac thang bi khoa
                //    if (evaluationCommentsList != null && evaluationCommentsList.Count > 0)
                //    {
                //        for (int i = 0; i < evaluationCommentsList.Count; i++)
                //        {
                //            EvaluationComments objRes = evaluationCommentsList[i];
                //            resultObj = new EvaluationCommentsPupil();
                //            resultObj.PupilID = objRes.PupilID;
                //            resultObj.EvaluationID = objRes.EvaluationID;
                //            resultObj.SemesterID = objRes.SemesterID;
                //            if (objRes.SemesterID == GlobalConstants.SEMESTER_OF_YEAR_FIRST)
                //            {
                //                if (strLockMark.Contains("T1"))
                //                {
                //                    resultObj.EvaluationMonth1 = objRes.CommentsM1M6;
                //                }
                //                if (strLockMark.Contains("T2"))
                //                {
                //                    resultObj.EvaluationMonth2 = objRes.CommentsM2M7;
                //                }
                //                if (strLockMark.Contains("T3"))
                //                {
                //                    resultObj.EvaluationMonth3 = objRes.CommentsM3M8;
                //                }
                //                if (strLockMark.Contains("T4"))
                //                {
                //                    resultObj.EvaluationMonth4 = objRes.CommentsM4M9;
                //                }
                //                if (strLockMark.Contains("T5"))
                //                {
                //                    resultObj.EvaluationMonth5 = objRes.CommentsM5M10;
                //                }
                //            }

                //            if (objRes.SemesterID == GlobalConstants.SEMESTER_OF_YEAR_SECOND)
                //            {
                //                if (strLockMark.Contains("T6"))
                //                {
                //                    resultObj.EvaluationMonth6 = objRes.CommentsM1M6;
                //                }
                //                if (strLockMark.Contains("T7"))
                //                {
                //                    resultObj.EvaluationMonth7 = objRes.CommentsM2M7;
                //                }
                //                if (strLockMark.Contains("T8"))
                //                {
                //                    resultObj.EvaluationMonth8 = objRes.CommentsM3M8;
                //                }
                //                if (strLockMark.Contains("T9"))
                //                {
                //                    resultObj.EvaluationMonth9 = objRes.CommentsM4M9;
                //                }
                //                if (strLockMark.Contains("T10"))
                //                {
                //                    resultObj.EvaluationMonth10 = objRes.CommentsM5M10;
                //                }
                //            }
                //            listComments.Add(resultObj);
                //            ObjResult.ListResult = listComments;
                //        }
                //    }
                //}

                #endregion

                #region Danh gia ket qua nam hoc

                SummedEvaluation summedEvaluationEducationObj = null;
                // danh sach mon hoc cua lop
                List<SubjectList> listSubject = ClassSubjectBusiness.SearchBySchool(schoolID, new Dictionary<string, object>(){
                {"ClassID", classID}
                }).Select(o => new SubjectList
                {
                    SubjectID = o.SubjectID,
                    SubjectName = o.SubjectCat.DisplayName,
                    isCommenting = o.IsCommenting.HasValue ? o.IsCommenting.Value : -1,
                    OrderSubject = o.SubjectCat.OrderInSubject
                }).ToList();

                // list nhan xet danh gia CLGD, pham chat, nang luc
                List<SummedEvaluation> listSummedEvaluationRes = (from ev in SummedEvaluationBusiness.All
                                                                  where ev.LastDigitSchoolID == partitionId
                                                                   && ev.AcademicYearID == academicYearID
                                                                   && ev.ClassID == classID
                                                                   && ev.PupilID == pupilID
                                                                  select ev).ToList();

                // list danh gia pham chat, nang luc
                List<SummedEndingEvaluation> listSummedEndingQualitiesRes = (from sn in SummedEndingEvaluationBusiness.All
                                                                             where sn.LastDigitSchoolID == partitionId
                                                                             && sn.AcademicYearID == academicYearID
                                                                             && sn.ClassID == classID
                                                                             && sn.PupilID == pupilID
                                                                             && (sn.EvaluationID == GlobalConstants.TAB_CAPACTIES_BOOKMARK_PRIMARY || sn.EvaluationID == GlobalConstants.TAB_QUALITIES_BOOKMARK_PRIMARY)
                                                                             select sn).ToList();

                //list GLGD
                int isCommenting = -1;
                List<SummedEvaluation> listSummedEvaluationEducationRes = listSummedEvaluationRes.Where(p => p.EvaluationID == GlobalConstants.TAB_EDUCATION_BOOKMARK_PRIMARY && p.EvaluationCriteriaID != 0).ToList();
                if (listSummedEvaluationEducationRes != null && listSummedEvaluationEducationRes.Count > 0)
                {
                    EducationQuality educationObj = null;
                    for (int i = 0; i < listSummedEvaluationEducationRes.Count; i++)
                    {
                        summedEvaluationEducationObj = listSummedEvaluationEducationRes[i];

                        if ((!viewSemester1 && summedEvaluationEducationObj.SemesterID == GlobalConstants.SEMESTER_OF_YEAR_FIRST)
                           || (!viewSemester2 && summedEvaluationEducationObj.SemesterID == GlobalConstants.SEMESTER_OF_YEAR_SECOND)) continue;

                        educationObj = new EducationQuality();
                        educationObj.PupilID = summedEvaluationEducationObj.PupilID;
                        educationObj.EndingComments = summedEvaluationEducationObj.EndingComments;
                        educationObj.EvaluationEnding = summedEvaluationEducationObj.EndingEvaluation;// danh gia
                        educationObj.SubjectID = summedEvaluationEducationObj.EvaluationCriteriaID;
                        educationObj.SubjectName = listSubject.Where(p => p.SubjectID == summedEvaluationEducationObj.EvaluationCriteriaID).Select(p => p.SubjectName).FirstOrDefault();
                        educationObj.SemesterID = summedEvaluationEducationObj.SemesterID;
                        isCommenting = listSubject.Where(p => p.SubjectID == summedEvaluationEducationObj.EvaluationCriteriaID).Select(p => p.isCommenting).FirstOrDefault();
                        if (isCommenting == GlobalConstants.SUBJECTCAT_ISCOMMENTING_MARK)// mon tinh diem
                        {
                            educationObj.Mark = summedEvaluationEducationObj.PeriodicEndingMark;
                        }
                        listEducationQuality.Add(educationObj);
                        ObjResult.ListEducationQuality = listEducationQuality;
                    }
                }

                // list Nang luc, Pham chat
                List<SummedEvaluation> listSummedEvaluationEQ = listSummedEvaluationRes.Where(p => p.EvaluationID == GlobalConstants.TAB_CAPACTIES_BOOKMARK_PRIMARY || p.EvaluationID == GlobalConstants.TAB_QUALITIES_BOOKMARK_PRIMARY).ToList();
                SummedEvaluation summedEvaluationObj = null;
                CapacitiesAndQualities CQObj = null;
                SummedEndingEvaluation summedEndingObjTmp = null;
                if (listSummedEvaluationEQ != null && listSummedEvaluationEQ.Count > 0)
                {
                    for (int j = 0; j < listSummedEvaluationEQ.Count; j++)
                    {
                        summedEvaluationObj = listSummedEvaluationEQ[j];

                        if ((!viewSemester1 && summedEvaluationObj.SemesterID == GlobalConstants.SEMESTER_OF_YEAR_FIRST)
                           || (!viewSemester2 && summedEvaluationObj.SemesterID == GlobalConstants.SEMESTER_OF_YEAR_SECOND)) continue;

                        CQObj = new CapacitiesAndQualities();
                        CQObj.PupilID = summedEvaluationObj.PupilID;
                        CQObj.SemesterID = summedEvaluationObj.SemesterID;
                        CQObj.EndingComments = summedEvaluationObj.EndingComments;
                        CQObj.EvaluationCretirea = summedEvaluationObj.EvaluationCriteriaID;
                        CQObj.EvaluationID = summedEvaluationObj.EvaluationID;
                        summedEndingObjTmp = listSummedEndingQualitiesRes.Where(p => p.SemesterID == summedEvaluationObj.SemesterID).FirstOrDefault();
                        if (summedEndingObjTmp != null)
                        {
                            CQObj.EvaluationEnding = summedEndingObjTmp.EndingEvaluation;
                        }
                        listCapacitiesAndQualities.Add(CQObj);
                        ObjResult.ListCapacitiesAndQualities = listCapacitiesAndQualities;
                    }
                }
                #endregion

                ObjResult.ListSubject = listSubject;// danh sach mon hoc
                ObjResult.ValidateCode = WCFConstant.RESPONSE_SUCCESS;
            }
            catch (Exception ex)
            {
                LogExtensions.GetCurrentClass = System.Reflection.MethodBase.GetCurrentMethod().DeclaringType;
                string para = string.Format("schoolID={0}, academicYearID={1}, classID={2}, semesterID={3}, monthID={4}, pupilID={5}, viewAllmark={6}, viewSemester1={7}, viewSemester2={8}",
                                            schoolID, academicYearID, classID, semesterID, monthID, pupilID, viewAllmark, viewSemester1, viewSemester2);
                LogExtensions.ErrorExt(logger, DateTime.Now, para, ex.Message, "null");
                ObjResult.ValidateCode = WCFConstant.RESPONSE_EXCEPTION;
            }


            #region dieu can quan tam giup do (lay 2 hoc ki)
            List<RewardCommentFinal> listRewardComments = RewardCommentFinalBusiness.All.Where(p => p.LastDigitSchoolID == partitionReward && p.AcademicYearID == academicYearID && p.PupilID == pupilID && p.SchoolID == schoolID && p.ClassID == classID).ToList();
            SummedEndingEvaluation summedEndingEvaluationObj = SummedEndingEvaluationBusiness.All.Where(p => p.LastDigitSchoolID == partitionId && p.AcademicYearID == academicYearID && p.SchoolID == schoolID && p.PupilID == pupilID && p.EvaluationID == 4 && p.SemesterID == 4).FirstOrDefault();


            RewardFinalAndSummedEnding objRewardandSummedEnding = new RewardFinalAndSummedEnding();

            List<RewardFinal> lstRewardFinal = RewardFinalBusiness.All.Where(p => p.SchoolID == schoolID).ToList();
            RewardFinal objRewardFinal = null;
            IDictionary<int, string> DicRewardFinal = new Dictionary<int, string>();
            for (int i = 0; i < lstRewardFinal.Count; i++)
            {
                objRewardFinal = lstRewardFinal[i];
                DicRewardFinal.Add(objRewardFinal.RewardFinalID, objRewardFinal.RewardMode);
            }

            List<string> lstStrSemester1 = new List<string>();
            List<string> lstStrSemester2 = new List<string>();
            string strRewardIDSemester1 = listRewardComments.Where(p => p.SemesterID == GlobalConstants.SEMESTER_OF_YEAR_FIRST).Select(p => p.RewardID).FirstOrDefault();
            string strRewardIDSemester2 = listRewardComments.Where(p => p.SemesterID == GlobalConstants.SEMESTER_OF_YEAR_SECOND).Select(p => p.RewardID).FirstOrDefault();
            string strRewardSemester1 = string.Empty;
            string strRewardSemester2 = string.Empty;

            if (!string.IsNullOrEmpty(strRewardIDSemester1))
            {
                lstStrSemester1 = strRewardIDSemester1.Split(new string[] { "," }, StringSplitOptions.RemoveEmptyEntries).ToList();
            }

            if (!string.IsNullOrEmpty(strRewardIDSemester2))
            {
                lstStrSemester2 = strRewardIDSemester2.Split(new string[] { "," }, StringSplitOptions.RemoveEmptyEntries).ToList();
            }

            for (int j = 0; j < lstStrSemester1.Count; j++)
            {
                strRewardSemester1 += DicRewardFinal[int.Parse(lstStrSemester1[j])] + "; ";
            }

            for (int k = 0; k < lstStrSemester2.Count; k++)
            {
                strRewardSemester2 += DicRewardFinal[int.Parse(lstStrSemester2[k])] + "; ";
            }

            if (viewSemester1)
            {
                objRewardandSummedEnding.OutStandingSemester1 = listRewardComments.Where(p => p.SemesterID == GlobalConstants.SEMESTER_OF_YEAR_FIRST).Select(p => p.OutstandingAchievement).FirstOrDefault();
                if (!string.IsNullOrEmpty(strRewardSemester1))
                {
                    strRewardSemester1 = strRewardSemester1.Remove(strRewardSemester1.Length - 2, 2) + ".";
                }
                objRewardandSummedEnding.RewardSemester1 = strRewardSemester1;
            }

            if (viewSemester2)
            {
                string strEvaluationFinal = string.Empty;
                if (summedEndingEvaluationObj != null)
                {
                    strEvaluationFinal = summedEndingEvaluationObj.EndingEvaluation;// hoan thanh chua hoan thanh
                    objRewardandSummedEnding.EvaluationFinal = "HT".Equals(strEvaluationFinal) ? string.Format(WCFConstant.EDUCATION_LEVEL_COMPLETE, classProfileBO.EducationLevelID) : ("CHT".Equals(strEvaluationFinal) ? string.Format(WCFConstant.EDUCATION_LEVEL_NOT_COMPLETE, classProfileBO.EducationLevelID) : string.Empty);
                }

                objRewardandSummedEnding.OutStandingSemester2 = listRewardComments.Where(p => p.SemesterID == GlobalConstants.SEMESTER_OF_YEAR_SECOND).Select(p => p.OutstandingAchievement).FirstOrDefault();
                if (!string.IsNullOrEmpty(strRewardSemester2))
                {
                    strRewardSemester2 = strRewardSemester2.Remove(strRewardSemester2.Length - 2, 2) + ".";
                }
                objRewardandSummedEnding.RewardSemester2 = strRewardSemester2;
            }

            ObjResult.RewardFinalAndSummedEnding = objRewardandSummedEnding;
            #endregion

            return ObjResult;
        }

        public EvaluationCommentsPupilComposite getEvaluationCommentsSparentMonthBySchoolHistory(int schoolID, int academicYearID, int classID, int semesterID, int monthID, int pupilID, bool viewAllmark, bool viewSemester1, bool viewSemester2)
        {
            //Khoi tao Business
            DeclareBusiness();
            int TypeOfTeacher = 2;// GVCN
            int partitionId = UtilsBusiness.GetPartionId(schoolID);
            int partitionReward = UtilsBusiness.GetPartionId(schoolID, 20);
            ClassProfile classProfileBO = ClassProfileBusiness.All.Where(p => p.ClassProfileID == classID).FirstOrDefault();
            EvaluationCommentsPupilComposite ObjResult = new EvaluationCommentsPupilComposite();
            List<EvaluationCommentsPupil> listComments = new List<EvaluationCommentsPupil>();
            List<EducationQuality> listEducationQuality = new List<EducationQuality>();
            List<CapacitiesAndQualities> listCapacitiesAndQualities = new List<CapacitiesAndQualities>();
            List<SubjectList> listSubjectResult = new List<SubjectList>();

            try
            {
                #region Nhan xet thang cua 1 hoc sinh
                EvaluationCommentsPupil resultObj = null;
                List<EvaluationCommentsHistory> evaluationCommentsList = (from ev in EvaluationCommentsHistoryBusiness.All
                                                                          where
                                                                          ev.LastDigitSchoolID == partitionId
                                                                          && ev.AcademicYearID == academicYearID
                                                                           && ev.ClassID == classID
                                                                           && (ev.SemesterID == GlobalConstants.SEMESTER_OF_YEAR_SECOND || ev.SemesterID == GlobalConstants.SEMESTER_OF_YEAR_FIRST)
                                                                           && ev.TypeOfTeacher == TypeOfTeacher
                                                                           && ev.PupilID == pupilID
                                                                          select ev).ToList();
                //if (viewAllmark)
                //{
                if (evaluationCommentsList != null && evaluationCommentsList.Count > 0)
                {
                    for (int i = 0; i < evaluationCommentsList.Count; i++)
                    {
                        EvaluationCommentsHistory objRes = evaluationCommentsList[i];
                        resultObj = new EvaluationCommentsPupil();
                        resultObj.PupilID = objRes.PupilID;
                        resultObj.EvaluationID = objRes.EvaluationID;
                        resultObj.SemesterID = objRes.SemesterID;
                        if (objRes.SemesterID == GlobalConstants.SEMESTER_OF_YEAR_FIRST)
                        {
                            resultObj.EvaluationMonth1 = objRes.CommentsM1M6;
                            resultObj.EvaluationMonth2 = objRes.CommentsM2M7;
                            resultObj.EvaluationMonth3 = objRes.CommentsM3M8;
                            resultObj.EvaluationMonth4 = objRes.CommentsM4M9;
                            resultObj.EvaluationMonth5 = objRes.CommentsM5M10;
                        }

                        if (objRes.SemesterID == GlobalConstants.SEMESTER_OF_YEAR_SECOND)
                        {
                            resultObj.EvaluationMonth6 = objRes.CommentsM1M6;
                            resultObj.EvaluationMonth7 = objRes.CommentsM2M7;
                            resultObj.EvaluationMonth8 = objRes.CommentsM3M8;
                            resultObj.EvaluationMonth9 = objRes.CommentsM4M9;
                            resultObj.EvaluationMonth10 = objRes.CommentsM5M10;
                        }
                        listComments.Add(resultObj);
                        ObjResult.ListResult = listComments;
                        ObjResult.ValidateCode = WCFConstant.RESPONSE_SUCCESS;
                    }
                }
                //}
                //else
                //{
                //    //chi xem cac thang bi khoa
                //    if (evaluationCommentsList != null && evaluationCommentsList.Count > 0)
                //    {
                //        for (int i = 0; i < evaluationCommentsList.Count; i++)
                //        {
                //            EvaluationCommentsHistory objRes = evaluationCommentsList[i];
                //            resultObj = new EvaluationCommentsPupil();
                //            resultObj.PupilID = objRes.PupilID;
                //            resultObj.EvaluationID = objRes.EvaluationID;
                //            resultObj.SemesterID = objRes.SemesterID;
                //            if (objRes.SemesterID == GlobalConstants.SEMESTER_OF_YEAR_FIRST)
                //            {
                //                if (strLockMark.Contains("T1"))
                //                {
                //                    resultObj.EvaluationMonth1 = objRes.CommentsM1M6;
                //                }
                //                if (strLockMark.Contains("T2"))
                //                {
                //                    resultObj.EvaluationMonth2 = objRes.CommentsM2M7;
                //                }
                //                if (strLockMark.Contains("T3"))
                //                {
                //                    resultObj.EvaluationMonth3 = objRes.CommentsM3M8;
                //                }
                //                if (strLockMark.Contains("T4"))
                //                {
                //                    resultObj.EvaluationMonth4 = objRes.CommentsM4M9;
                //                }
                //                if (strLockMark.Contains("T5"))
                //                {
                //                    resultObj.EvaluationMonth5 = objRes.CommentsM5M10;
                //                }
                //            }

                //            if (objRes.SemesterID == GlobalConstants.SEMESTER_OF_YEAR_SECOND)
                //            {
                //                if (strLockMark.Contains("T6"))
                //                {
                //                    resultObj.EvaluationMonth6 = objRes.CommentsM1M6;
                //                }
                //                if (strLockMark.Contains("T7"))
                //                {
                //                    resultObj.EvaluationMonth7 = objRes.CommentsM2M7;
                //                }
                //                if (strLockMark.Contains("T8"))
                //                {
                //                    resultObj.EvaluationMonth8 = objRes.CommentsM3M8;
                //                }
                //                if (strLockMark.Contains("T9"))
                //                {
                //                    resultObj.EvaluationMonth9 = objRes.CommentsM4M9;
                //                }
                //                if (strLockMark.Contains("T10"))
                //                {
                //                    resultObj.EvaluationMonth10 = objRes.CommentsM5M10;
                //                }
                //            }
                //            listComments.Add(resultObj);
                //            ObjResult.ListResult = listComments;
                //        }
                //    }
                //}

                #endregion

                #region Danh gia ket qua nam hoc
                SummedEvaluationHistory summedEvaluationEducationObj = null;
                // danh sach mon hoc cua lop
                List<SubjectList> listSubject = ClassSubjectBusiness.SearchBySchool(schoolID, new Dictionary<string, object>(){
                {"ClassID", classID}
                }).Select(o => new SubjectList
                {
                    SubjectID = o.SubjectID,
                    SubjectName = o.SubjectCat.DisplayName,
                    isCommenting = o.IsCommenting.HasValue ? o.IsCommenting.Value : -1,
                    OrderSubject = o.SubjectCat.OrderInSubject
                }).ToList();

                // list nhan xet danh gia CLGD, pham chat, nang luc
                List<SummedEvaluationHistory> listSummedEvaluationRes = (from ev in SummedEvaluationHistoryBusiness.All
                                                                         where ev.LastDigitSchoolID == partitionId
                                                                          && ev.AcademicYearID == academicYearID
                                                                          && ev.ClassID == classID
                                                                          && ev.PupilID == pupilID
                                                                         select ev).ToList();

                // list danh gia pham chat, nang luc
                List<SummedEndingEvaluation> listSummedEndingQualitiesRes = (from sn in SummedEndingEvaluationBusiness.All
                                                                             where sn.LastDigitSchoolID == partitionId
                                                                             && sn.AcademicYearID == academicYearID
                                                                             && sn.ClassID == classID
                                                                             && sn.PupilID == pupilID
                                                                             && (sn.EvaluationID == GlobalConstants.TAB_CAPACTIES_BOOKMARK_PRIMARY || sn.EvaluationID == GlobalConstants.TAB_QUALITIES_BOOKMARK_PRIMARY)
                                                                             select sn).ToList();

                //list GLGD
                int isCommenting = -1;
                List<SummedEvaluationHistory> listSummedEvaluationEducationRes = listSummedEvaluationRes.Where(p => p.EvaluationID == GlobalConstants.TAB_EDUCATION_BOOKMARK_PRIMARY && p.EvaluationCriteriaID != 0).ToList();
                if (listSummedEvaluationEducationRes != null && listSummedEvaluationEducationRes.Count > 0)
                {
                    EducationQuality educationObj = null;
                    for (int i = 0; i < listSummedEvaluationEducationRes.Count; i++)
                    {
                        summedEvaluationEducationObj = listSummedEvaluationEducationRes[i];

                        if ((!viewSemester1 && summedEvaluationEducationObj.SemesterID == GlobalConstants.SEMESTER_OF_YEAR_FIRST)
                           || (!viewSemester2 && summedEvaluationEducationObj.SemesterID == GlobalConstants.SEMESTER_OF_YEAR_SECOND)) continue;

                        educationObj = new EducationQuality();
                        educationObj.PupilID = summedEvaluationEducationObj.PupilID;
                        educationObj.EndingComments = summedEvaluationEducationObj.EndingComments;
                        educationObj.EvaluationEnding = summedEvaluationEducationObj.EndingEvaluation;// danh gia
                        educationObj.SubjectID = summedEvaluationEducationObj.EvaluationCriteriaID;
                        educationObj.SubjectName = listSubject.Where(p => p.SubjectID == summedEvaluationEducationObj.EvaluationCriteriaID).Select(p => p.SubjectName).FirstOrDefault();
                        educationObj.SemesterID = summedEvaluationEducationObj.SemesterID;
                        isCommenting = listSubject.Where(p => p.SubjectID == summedEvaluationEducationObj.EvaluationCriteriaID).Select(p => p.isCommenting).FirstOrDefault();
                        if (isCommenting == GlobalConstants.SUBJECTCAT_ISCOMMENTING_MARK)// mon tinh diem
                        {
                            educationObj.Mark = summedEvaluationEducationObj.PeriodicEndingMark;
                        }
                        listEducationQuality.Add(educationObj);
                        ObjResult.ListEducationQuality = listEducationQuality;
                    }
                }

                // list Nang luc, Pham chat
                List<SummedEvaluationHistory> listSummedEvaluationEQ = listSummedEvaluationRes.Where(p => p.EvaluationID == GlobalConstants.TAB_CAPACTIES_BOOKMARK_PRIMARY || p.EvaluationID == GlobalConstants.TAB_QUALITIES_BOOKMARK_PRIMARY).ToList();
                SummedEvaluationHistory summedEvaluationObj = null;
                CapacitiesAndQualities CQObj = null;
                SummedEndingEvaluation summedEndingObjTmp = null;
                if (listSummedEvaluationEQ != null && listSummedEvaluationEQ.Count > 0)
                {
                    for (int j = 0; j < listSummedEvaluationEQ.Count; j++)
                    {
                        summedEvaluationObj = listSummedEvaluationEQ[j];

                        if ((!viewSemester1 && summedEvaluationObj.SemesterID == GlobalConstants.SEMESTER_OF_YEAR_FIRST)
                           || (!viewSemester2 && summedEvaluationObj.SemesterID == GlobalConstants.SEMESTER_OF_YEAR_SECOND)) continue;

                        CQObj = new CapacitiesAndQualities();
                        CQObj.PupilID = summedEvaluationObj.PupilID;
                        CQObj.SemesterID = summedEvaluationObj.SemesterID;
                        CQObj.EndingComments = summedEvaluationObj.EndingComments;
                        CQObj.EvaluationCretirea = summedEvaluationObj.EvaluationCriteriaID;
                        CQObj.EvaluationID = summedEvaluationObj.EvaluationID;
                        summedEndingObjTmp = listSummedEndingQualitiesRes.Where(p => p.SemesterID == summedEvaluationObj.SemesterID).FirstOrDefault();
                        if (summedEndingObjTmp != null)
                        {
                            CQObj.EvaluationEnding = summedEndingObjTmp.EndingEvaluation;
                        }
                        listCapacitiesAndQualities.Add(CQObj);
                        ObjResult.ListCapacitiesAndQualities = listCapacitiesAndQualities;
                    }
                }
                #endregion

                ObjResult.ListSubject = listSubject;// danh sach mon hoc
                ObjResult.ValidateCode = WCFConstant.RESPONSE_SUCCESS;
            }
            catch (Exception ex)
            {
                LogExtensions.GetCurrentClass = System.Reflection.MethodBase.GetCurrentMethod().DeclaringType;
                string para = string.Format("schoolID={0}, academicYearID={1}, classID={2}, semesterID={3}, monthID={4}, pupilID={5}, viewAllmark={6}, viewSemester1={7}, viewSemester2={8}",
                                            schoolID, academicYearID, classID, semesterID, monthID, pupilID, viewAllmark, viewSemester1, viewSemester2);
                LogExtensions.ErrorExt(logger, DateTime.Now, para, ex.Message, "null");
                ObjResult.ValidateCode = WCFConstant.RESPONSE_EXCEPTION;
            }


            #region dieu can quan tam giup do (lay 2 hoc ki)
            List<RewardCommentFinal> listRewardComments = RewardCommentFinalBusiness.All.Where(p => p.LastDigitSchoolID == partitionReward && p.AcademicYearID == academicYearID && p.PupilID == pupilID && p.SchoolID == schoolID && p.ClassID == classID).ToList();
            SummedEndingEvaluation summedEndingEvaluationObj = SummedEndingEvaluationBusiness.All.Where(p => p.LastDigitSchoolID == partitionId && p.AcademicYearID == academicYearID && p.SchoolID == schoolID && p.PupilID == pupilID && p.EvaluationID == 4 && p.SemesterID == 4).FirstOrDefault();


            RewardFinalAndSummedEnding objRewardandSummedEnding = new RewardFinalAndSummedEnding();

            List<RewardFinal> lstRewardFinal = RewardFinalBusiness.All.Where(p => p.SchoolID == schoolID).ToList();
            RewardFinal objRewardFinal = null;
            IDictionary<int, string> DicRewardFinal = new Dictionary<int, string>();
            for (int i = 0; i < lstRewardFinal.Count; i++)
            {
                objRewardFinal = lstRewardFinal[i];
                DicRewardFinal.Add(objRewardFinal.RewardFinalID, objRewardFinal.RewardMode);
            }

            List<string> lstStrSemester1 = new List<string>();
            List<string> lstStrSemester2 = new List<string>();
            string strRewardIDSemester1 = listRewardComments.Where(p => p.SemesterID == GlobalConstants.SEMESTER_OF_YEAR_FIRST).Select(p => p.RewardID).FirstOrDefault();
            string strRewardIDSemester2 = listRewardComments.Where(p => p.SemesterID == GlobalConstants.SEMESTER_OF_YEAR_SECOND).Select(p => p.RewardID).FirstOrDefault();
            string strRewardSemester1 = string.Empty;
            string strRewardSemester2 = string.Empty;

            if (!string.IsNullOrEmpty(strRewardIDSemester1))
            {
                lstStrSemester1 = strRewardIDSemester1.Split(new string[] { "," }, StringSplitOptions.RemoveEmptyEntries).ToList();
            }

            if (!string.IsNullOrEmpty(strRewardIDSemester2))
            {
                lstStrSemester2 = strRewardIDSemester2.Split(new string[] { "," }, StringSplitOptions.RemoveEmptyEntries).ToList();
            }

            for (int j = 0; j < lstStrSemester1.Count; j++)
            {
                strRewardSemester1 += DicRewardFinal[int.Parse(lstStrSemester1[j])] + "; ";
            }

            for (int k = 0; k < lstStrSemester2.Count; k++)
            {
                strRewardSemester2 += DicRewardFinal[int.Parse(lstStrSemester2[k])] + "; ";
            }

            if (viewSemester1)
            {
                objRewardandSummedEnding.OutStandingSemester1 = listRewardComments.Where(p => p.SemesterID == GlobalConstants.SEMESTER_OF_YEAR_FIRST).Select(p => p.OutstandingAchievement).FirstOrDefault();
                if (!string.IsNullOrEmpty(strRewardSemester1))
                {
                    strRewardSemester1 = strRewardSemester1.Remove(strRewardSemester1.Length - 2, 2) + ".";
                }
                objRewardandSummedEnding.RewardSemester1 = strRewardSemester1;
            }

            if (viewSemester2)
            {
                string strEvaluationFinal = string.Empty;
                if (summedEndingEvaluationObj != null)
                {
                    strEvaluationFinal = summedEndingEvaluationObj.EndingEvaluation;// hoan thanh chua hoan thanh
                    objRewardandSummedEnding.EvaluationFinal = "HT".Equals(strEvaluationFinal) ? string.Format(WCFConstant.EDUCATION_LEVEL_COMPLETE, classProfileBO.EducationLevelID) : ("CHT".Equals(strEvaluationFinal) ? string.Format(WCFConstant.EDUCATION_LEVEL_NOT_COMPLETE, classProfileBO.EducationLevelID) : string.Empty);
                }

                objRewardandSummedEnding.OutStandingSemester2 = listRewardComments.Where(p => p.SemesterID == GlobalConstants.SEMESTER_OF_YEAR_SECOND).Select(p => p.OutstandingAchievement).FirstOrDefault();
                if (!string.IsNullOrEmpty(strRewardSemester2))
                {
                    strRewardSemester2 = strRewardSemester2.Remove(strRewardSemester2.Length - 2, 2) + ".";
                }
                objRewardandSummedEnding.RewardSemester2 = strRewardSemester2;
            }

            ObjResult.RewardFinalAndSummedEnding = objRewardandSummedEnding;
            #endregion

            return ObjResult;
        }

        public TT22ResultComposite GetTT22Result(int schoolId, int academicYearId, int educationLevel, int classId, int pupilId)
        {
            DeclareBusiness();

            TT22ResultComposite result = new TT22ResultComposite();

            try
            {
                int partitionId = UtilsBusiness.GetPartionId(schoolId);

                AcademicYear ay = AcademicYearBusiness.Find(academicYearId);

                IDictionary<string, object> dic;
                //Danh sach mon hoc
                dic = new Dictionary<string, object>();
                dic["SchoolID"] = schoolId;
                dic["AcademicYearID"] = academicYearId;
                dic["ClassID"] = classId;
                dic["EducationLevelID"] = educationLevel;

                List<ClassSubject> lstSubject = ClassSubjectBusiness.SearchBySchool(schoolId, dic).OrderBy(o => o.IsCommenting).ThenBy(o => o.SubjectCat.OrderInSubject).ThenBy(o => o.SubjectCat.DisplayName).ToList();

                //Danh sach mon hoc HK1
                List<ClassSubject> lstSubjectSem1 = lstSubject.Where(o => o.SectionPerWeekFirstSemester > 0).ToList();

                //Danh sach mon hoc HK2
                List<ClassSubject> lstSubjectSem2 = lstSubject.Where(o => o.SectionPerWeekSecondSemester > 0).ToList();

                //thong tin diem va nhan xet cac mon

                IDictionary<string, object> dicRate = new Dictionary<string, object>()
                {
                    {"AcademicYearID",academicYearId},
                    {"SchoolID",schoolId},
                    {"ClassID",classId},
                    {"PupilID", pupilId},

                };
                List<RatedCommentPupil> lstRatedCommentPupil = RatedCommentPupilBusiness.Search(dicRate).ToList();


                IDictionary<string, object> dicSEE = new Dictionary<string, object>()
            {
                {"AcademicYearID",academicYearId},
                {"SchoolID",schoolId},
                {"SemesterID",GlobalConstants.SEMESTER_OF_EVALUATION_RESULT},
                {"EducationLevelID",educationLevel},
                {"ClassID",classId},
                {"PupilID",pupilId},
                {"Semester", 4}
            };
                SummedEndingEvaluationBO objSeeBO = SummedEndingEvaluationBusiness.Search(dicSEE).FirstOrDefault();

                //Thong tin khen thuong
                IDictionary<string, object> dicER = new Dictionary<string, object>()
            {
                {"SchoolID",schoolId},
                {"AcademicYearID",academicYearId},
                {"ClassID", classId},
                {"PupilID",pupilId}
            };
                List<EvaluationReward> lstEvaluationReward = EvaluationRewardBusiness.Search(dicER).ToList();

                List<EvaluationCriteria> lstEvaluationCriteria = EvaluationCriteriaBusiness.All.OrderBy(p => p.EvaluationCriteriaID).ToList();

                //Khen thuong ky 1
                List<EvaluationReward> lstErSem1 = lstEvaluationReward.Where(o => o.SemesterID == 1 && (o.RewardID == 1 || o.RewardID == 2)).ToList();
                string RewardNameSem1 = string.Empty;

                EvaluationReward objER = null;
                for (int i = 0; i < lstErSem1.Count; i++)
                {
                    objER = lstErSem1[i];

                    RewardNameSem1 += objER.Content;
                    if (i < lstErSem1.Count - 1)
                    {
                        RewardNameSem1 += ", ";
                    }

                }

                //Khen thuong ky 2

                List<EvaluationReward> lstErSem2 = lstEvaluationReward.Where(o => o.SemesterID == 2 && (o.RewardID == 1 || o.RewardID == 2)).ToList();
                string RewardNameSem2 = (objSeeBO != null && objSeeBO.RewardID.HasValue) ? "Học sinh xuất sắc" : "";
                if (!string.IsNullOrEmpty(RewardNameSem2) && lstErSem2.Count > 0)
                {
                    RewardNameSem2 += ", ";
                }
                for (int i = 0; i < lstErSem2.Count; i++)
                {
                    objER = lstErSem2[i];

                    RewardNameSem2 += objER.Content;
                    if (i < lstErSem2.Count - 1)
                    {
                        RewardNameSem2 += ", ";
                    }

                }

                //Danh gia cuoi nam
                string endResult = string.Empty;
                if (objSeeBO != null)
                {
                    if (objSeeBO.EndingEvaluation == "HT"
                            || (objSeeBO.EndingEvaluation == "CHT" && objSeeBO.RateAdd.HasValue && objSeeBO.RateAdd.Value == 1))
                    {
                        endResult = "Hoàn thành CT lớp" + educationLevel.ToString();
                    }

                    if (objSeeBO.EndingEvaluation == "CHT" && objSeeBO.RateAdd != 1)
                    {
                        endResult = "Chưa hoàn thành CT lớp học";
                    }
                }

                result.EndYearResult = endResult;
                result.RewardSemester1 = RewardNameSem1;
                result.RewardSemester2 = RewardNameSem2;

                #region Mon hoc & HDGD
                //Hoc ky 1
                List<TT22MHHDGDResultComposite> lstMHSem1 = new List<TT22MHHDGDResultComposite>();
                result.LstMHHDGDSemester1 = lstMHSem1;

                List<RatedCommentPupil> lstRatedSem1 = lstRatedCommentPupil.Where(o => o.SemesterID == 1 && o.EvaluationID == 1).ToList();

                for (int i = 0; i < lstSubjectSem1.Count; i++)
                {
                    ClassSubject subject = lstSubjectSem1[i];

                    RatedCommentPupil rcp = lstRatedSem1.FirstOrDefault(o => o.SubjectID == subject.SubjectID);
                    TT22MHHDGDResultComposite MHSem1 = new TT22MHHDGDResultComposite();
                    lstMHSem1.Add(MHSem1);

                    MHSem1.SubjectName = subject.SubjectCat.DisplayName;
                    MHSem1.IsComment = subject.IsCommenting.GetValueOrDefault();
                    MHSem1.SubjectCode = subject.SubjectCat.Abbreviation;

                    if (rcp != null)
                    {
                        MHSem1.ResultAchivedMidleSemester = rcp.MiddleEvaluation == 1 ? GlobalConstants.EVALUATION_GOOD_COMPLETE : rcp.MiddleEvaluation == 2 ?
                                   GlobalConstants.EVALUATION_COMPLETE : rcp.MiddleEvaluation == 3 ? GlobalConstants.EVALUATION_NOT_COMPLETE : "";
                        MHSem1.ResultAchivedEndleSemester = rcp.EndingEvaluation == 1 ? GlobalConstants.EVALUATION_GOOD_COMPLETE : rcp.EndingEvaluation == 2 ?
                                            GlobalConstants.EVALUATION_COMPLETE : rcp.EndingEvaluation == 3 ? GlobalConstants.EVALUATION_NOT_COMPLETE : "";

                        MHSem1.Comment = rcp.Comment;

                        if (subject.IsCommenting.HasValue && subject.IsCommenting.Value == 1)
                        {
                            MHSem1.PeriodMarkMidleSemester = rcp.PeriodicMiddleJudgement;
                            MHSem1.PeriodMarkEndSemester = rcp.PeriodicEndingJudgement;
                        }
                        else
                        {

                            MHSem1.PeriodMarkMidleSemester = rcp.PeriodicMiddleMark.HasValue ? rcp.PeriodicMiddleMark.Value.ToString() : string.Empty;
                            MHSem1.PeriodMarkEndSemester = rcp.PeriodicEndingMark.HasValue ? rcp.PeriodicEndingMark.Value.ToString() : string.Empty;
                        }
                    }
                }

                //Hoc ky 2
                List<TT22MHHDGDResultComposite> lstMHSem2 = new List<TT22MHHDGDResultComposite>();

                List<RatedCommentPupil> lstRatedSem2 = lstRatedCommentPupil.Where(o => o.SemesterID == 2 && o.EvaluationID == 1).ToList();
                result.LstMHHDGDSemester2 = lstMHSem2;

                for (int i = 0; i < lstSubjectSem2.Count; i++)
                {
                    ClassSubject subject = lstSubjectSem2[i];

                    RatedCommentPupil rcp = lstRatedSem2.FirstOrDefault(o => o.SubjectID == subject.SubjectID);
                    TT22MHHDGDResultComposite MHSem2 = new TT22MHHDGDResultComposite();
                    lstMHSem2.Add(MHSem2);

                    MHSem2.SubjectName = subject.SubjectCat.DisplayName;
                    MHSem2.IsComment = subject.IsCommenting.GetValueOrDefault();
                    MHSem2.SubjectCode = subject.SubjectCat.Abbreviation;

                    if (rcp != null)
                    {
                        MHSem2.ResultAchivedMidleSemester = rcp.MiddleEvaluation == 1 ? GlobalConstants.EVALUATION_GOOD_COMPLETE : rcp.MiddleEvaluation == 2 ?
                                   GlobalConstants.EVALUATION_COMPLETE : rcp.MiddleEvaluation == 3 ? GlobalConstants.EVALUATION_NOT_COMPLETE : "";
                        MHSem2.ResultAchivedEndleSemester = rcp.EndingEvaluation == 1 ? GlobalConstants.EVALUATION_GOOD_COMPLETE : rcp.EndingEvaluation == 2 ?
                                            GlobalConstants.EVALUATION_COMPLETE : rcp.EndingEvaluation == 3 ? GlobalConstants.EVALUATION_NOT_COMPLETE : "";

                        MHSem2.Comment = rcp.Comment;

                        if (subject.IsCommenting.HasValue && subject.IsCommenting.Value == 1)
                        {
                            MHSem2.PeriodMarkMidleSemester = rcp.PeriodicMiddleJudgement;
                            MHSem2.PeriodMarkEndSemester = rcp.PeriodicEndingJudgement;
                        }
                        else
                        {

                            MHSem2.PeriodMarkMidleSemester = rcp.PeriodicMiddleMark.HasValue ? rcp.PeriodicMiddleMark.Value.ToString() : string.Empty;
                            MHSem2.PeriodMarkEndSemester = rcp.PeriodicEndingMark.HasValue ? rcp.PeriodicEndingMark.Value.ToString() : string.Empty;
                        }
                    }
                }


                //nang luc
                List<EvaluationCriteria> lstEcNL = lstEvaluationCriteria.Where(o => o.TypeID == 2).ToList();
                List<RatedCommentPupil> lstRatedNL = lstRatedCommentPupil.Where(o => o.EvaluationID == 2).ToList();

                List<TT22CapacityQualityComposite> lstCapacityResult = new List<TT22CapacityQualityComposite>();
                result.LstCapacityResult = lstCapacityResult;

                TT22CapacityQualityComposite tmpCapa;
                for (int i = 0; i < lstEcNL.Count; i++)
                {
                    EvaluationCriteria ec = lstEcNL[i];
                    tmpCapa = new TT22CapacityQualityComposite();
                    lstCapacityResult.Add(tmpCapa);

                    tmpCapa.CriteriaName = ec.CriteriaName;

                    RatedCommentPupil rcpSem1 = lstRatedNL.FirstOrDefault(o => o.SemesterID == 1 && o.SubjectID == ec.EvaluationCriteriaID);
                    RatedCommentPupil rcpSem2 = lstRatedNL.FirstOrDefault(o => o.SemesterID == 2 && o.SubjectID == ec.EvaluationCriteriaID);

                    if (rcpSem1 != null)
                    {
                        tmpCapa.ResultMidleSemester1 = rcpSem1.CapacityMiddleEvaluation == 1 ? "Tốt" : rcpSem1.CapacityMiddleEvaluation == 2 ?
                                    "Đạt" : rcpSem1.CapacityMiddleEvaluation == 3 ? "Cần cố gắng" : "";

                        tmpCapa.ResultEndSemester1 = rcpSem1.CapacityEndingEvaluation == 1 ? "Tốt" : rcpSem1.CapacityEndingEvaluation == 2 ?
                                    "Đạt" : rcpSem1.CapacityEndingEvaluation == 3 ? "Cần cố gắng" : "";

                        tmpCapa.CommentSemester1 = rcpSem1.Comment;

                    }

                    if (rcpSem2 != null)
                    {
                        tmpCapa.ResultMidleSemester2 = rcpSem2.CapacityMiddleEvaluation == 1 ? "Tốt" : rcpSem2.CapacityMiddleEvaluation == 2 ?
                                    "Đạt" : rcpSem2.CapacityMiddleEvaluation == 3 ? "Cần cố gắng" : "";
                        tmpCapa.ResultEndSemester2 = rcpSem2.CapacityEndingEvaluation == 1 ? "Tốt" : rcpSem2.CapacityEndingEvaluation == 2 ?
                                    "Đạt" : rcpSem2.CapacityEndingEvaluation == 3 ? "Cần cố gắng" : "";
                        tmpCapa.CommentSemester2 = rcpSem2.Comment;
                    }
                }


                //Pham chat
                List<EvaluationCriteria> lstEcPC = lstEvaluationCriteria.Where(o => o.TypeID == 3).ToList();
                List<RatedCommentPupil> lstRatedPC = lstRatedCommentPupil.Where(o => o.EvaluationID == 3).ToList();

                List<TT22CapacityQualityComposite> lstQualityResult = new List<TT22CapacityQualityComposite>();
                result.LstQualityResult = lstQualityResult;

                TT22CapacityQualityComposite tmpQuality;
                for (int i = 0; i < lstEcPC.Count; i++)
                {
                    EvaluationCriteria ec = lstEcPC[i];
                    tmpQuality = new TT22CapacityQualityComposite();
                    lstQualityResult.Add(tmpQuality);

                    tmpQuality.CriteriaName = ec.CriteriaName;

                    RatedCommentPupil rcpSem1 = lstRatedPC.FirstOrDefault(o => o.SemesterID == 1 && o.SubjectID == ec.EvaluationCriteriaID);
                    RatedCommentPupil rcpSem2 = lstRatedPC.FirstOrDefault(o => o.SemesterID == 2 && o.SubjectID == ec.EvaluationCriteriaID);

                    if (rcpSem1 != null)
                    {
                        tmpQuality.ResultMidleSemester1 = rcpSem1.QualityMiddleEvaluation == 1 ? "Tốt" : rcpSem1.QualityMiddleEvaluation == 2 ?
                                    "Đạt" : rcpSem1.QualityMiddleEvaluation == 3 ? "Cần cố gắng" : "";
                        tmpQuality.ResultEndSemester1 = rcpSem1.QualityEndingEvaluation == 1 ? "Tốt" : rcpSem1.QualityEndingEvaluation == 2 ?
                                    "Đạt" : rcpSem1.QualityEndingEvaluation == 3 ? "Cần cố gắng" : "";
                        tmpQuality.CommentSemester1 = rcpSem1.Comment;
                    }

                    if (rcpSem2 != null)
                    {
                        tmpQuality.ResultMidleSemester2 = rcpSem2.QualityMiddleEvaluation == 1 ? "Tốt" : rcpSem2.QualityMiddleEvaluation == 2 ?
                                    "Đạt" : rcpSem2.QualityMiddleEvaluation == 3 ? "Cần cố gắng" : "";
                        tmpQuality.ResultEndSemester2 = rcpSem2.QualityEndingEvaluation == 1 ? "Tốt" : rcpSem2.QualityEndingEvaluation == 2 ?
                                    "Đạt" : rcpSem2.QualityEndingEvaluation == 3 ? "Cần cố gắng" : "";
                        tmpQuality.CommentSemester2 = rcpSem2.Comment;
                    }
                }

                result.ValidateCode = WCFConstant.RESPONSE_SUCCESS;
            }
            catch (Exception ex)
            {
                LogExtensions.GetCurrentClass = System.Reflection.MethodBase.GetCurrentMethod().DeclaringType;
                string para = string.Format("schoolId={0}, academicYearId={1}, educationLevel={2}, classId={3}, pupilId={4}", schoolId, academicYearId, educationLevel, classId, pupilId);
                LogExtensions.ErrorExt(logger, DateTime.Now, para, ex.Message, "null");
                result.ValidateCode = WCFConstant.RESPONSE_EXCEPTION;
            }

            return result;
            #endregion
        }

        /// <summary>
        /// lấy thông tin học sinh phục vụ hiển thị ở trang PHHS
        /// </summary>
        /// <param name="schoolID"></param>
        /// <param name="academicYearID"></param>
        /// <param name="classID"></param>
        /// <param name="semesterID"></param>
        /// <param name="monthID"></param>
        /// <param name="pupilID"></param>
        /// <returns></returns>
        public EvaluationCommentsPupilComposite getEvaluationCommentVNENSParent(int schoolID, int academicYearID, int classID, int semesterID, int pupilID)
        {
            DeclareBusiness();
            int partitionId = UtilsBusiness.GetPartionId(schoolID);
            ClassProfile classProfileBO = ClassProfileBusiness.All.Where(p => p.ClassProfileID == classID).FirstOrDefault();
            EvaluationCommentsPupilComposite ObjResult = new EvaluationCommentsPupilComposite();
            List<EducationQuality> lstEducationQuality = new List<EducationQuality>();
            List<SubjectList> lstSubjectResult = new List<SubjectList>();
            try
            {
                // danh sach mon hoc cua lop
                List<SubjectList> listSubject = ClassSubjectBusiness.SearchBySchool(schoolID, new Dictionary<string, object>(){
                    {"AcademicYearID", academicYearID},
                    {"ClassID", classID}
                }).Select(o => new SubjectList
                {
                    SubjectID = o.SubjectID,
                    SubjectName = o.SubjectCat.DisplayName,
                    isCommenting = o.IsCommenting.HasValue ? o.IsCommenting.Value : -1,
                    OrderSubject = o.SubjectCat.OrderInSubject,
                    IsSubjectVNEN = o.IsSubjectVNEN
                }).OrderByDescending(o => o.IsSubjectVNEN).OrderBy(o => o.OrderSubject).ToList();

                RewardFinalAndSummedEnding ObjFinalContent = new RewardFinalAndSummedEnding();
                #region Danh gia, khen thuong
                List<ReviewBookPupil> lstReview = (from rv in ReviewBookPupilBusiness.All
                                                   where rv.SchoolID == schoolID && rv.PartitionID == partitionId
                                                       && rv.AcademicYearID == academicYearID && rv.ClassID == classID
                                                       && rv.PupilID == pupilID
                                                   select rv).ToList();
                foreach (var item in lstReview)
                {
                    if (item.SemesterID == GlobalConstants.SEMESTER_OF_YEAR_FIRST)
                    {
                        ObjFinalContent.OutStandingSemester1 = item.CQComment;
                        ObjFinalContent.CorrectiveCommentSem1 = item.CommentAdd;
                        if (item.CapacityRate == GlobalConstants.REVIEW_TYPE_FINISH) ObjFinalContent.CapacityEvaluationSem1 = "Đạt";
                        else if (item.CapacityRate == GlobalConstants.REVIEW_TYPE_NOT_FINISH) ObjFinalContent.CapacityEvaluationSem1 = "Chưa đạt";
                        if (item.QualityRate == GlobalConstants.REVIEW_TYPE_FINISH) ObjFinalContent.QualityEvaluationSem1 = "Đạt";
                        else if (item.QualityRate == GlobalConstants.REVIEW_TYPE_NOT_FINISH) ObjFinalContent.QualityEvaluationSem1 = "Chưa đạt";
                    }
                    else if (item.SemesterID == GlobalConstants.SEMESTER_OF_YEAR_SECOND)
                    {
                        if (item.RateAndYear == GlobalConstants.REVIEW_TYPE_FINISH)
                            ObjFinalContent.EvaluationFinal = "Hoàn thành CT lớp 6";
                        else if (item.RateAndYear == GlobalConstants.REVIEW_TYPE_NOT_FINISH)
                        {
                            if (!item.RateAdd.HasValue) ObjFinalContent.EvaluationFinal = "Chưa hoàn thành CT lớp 6";
                            else if (item.RateAdd == GlobalConstants.REVIEW_TYPE_FINISH) ObjFinalContent.EvaluationFinal = "Hoàn thành CT lớp 6";
                            else if (item.RateAdd == GlobalConstants.REVIEW_TYPE_NOT_FINISH) ObjFinalContent.EvaluationFinal = "Chưa hoàn thành CT lớp 6";
                        }

                        ObjFinalContent.OutStandingSemester2 = item.CQComment;
                        ObjFinalContent.CorrectiveCommentSem2 = item.CommentAdd;
                        if (item.CapacitySummer.HasValue)
                        {
                            if (item.CapacitySummer == GlobalConstants.REVIEW_TYPE_FINISH) ObjFinalContent.CapacityEvaluationSem2 = "Đạt";
                            else if (item.CapacitySummer == GlobalConstants.REVIEW_TYPE_NOT_FINISH) ObjFinalContent.CapacityEvaluationSem2 = "Chưa đạt";
                        }
                        else
                        {
                            if (item.CapacityRate == GlobalConstants.REVIEW_TYPE_FINISH) ObjFinalContent.CapacityEvaluationSem2 = "Đạt";
                            else if (item.CapacityRate == GlobalConstants.REVIEW_TYPE_NOT_FINISH) ObjFinalContent.CapacityEvaluationSem2 = "Chưa đạt";
                        }
                        if (item.QualitySummer.HasValue)
                        {
                            if (item.QualitySummer == GlobalConstants.REVIEW_TYPE_FINISH) ObjFinalContent.QualityEvaluationSem2 = "Đạt";
                            else if (item.QualitySummer == GlobalConstants.REVIEW_TYPE_NOT_FINISH) ObjFinalContent.QualityEvaluationSem2 = "Chưa đạt";
                        }
                        else
                        {
                            if (item.QualityRate == GlobalConstants.REVIEW_TYPE_FINISH) ObjFinalContent.QualityEvaluationSem2 = "Đạt";
                            else if (item.QualityRate == GlobalConstants.REVIEW_TYPE_NOT_FINISH) ObjFinalContent.QualityEvaluationSem2 = "Chưa đạt";
                        }
                    }
                }


                #region Khen thuong
                List<UpdateReward> lstRewardComment = (from ur in UpdateRewardBusiness.All
                                                       where ur.SchoolID == schoolID && ur.PartitionID == partitionId
                                                              && ur.AcademicYearID == academicYearID
                                                              && ur.ClassID == classID
                                                              && ur.PupilID == pupilID
                                                       select ur).ToList();

                List<RewardFinal> lstRewardFinal = RewardFinalBusiness.All.Where(p => p.SchoolID == schoolID).ToList();
                RewardFinal objRewardFinal = null;
                IDictionary<int, string> DicRewardFinal = new Dictionary<int, string>();
                for (int i = 0; i < lstRewardFinal.Count; i++)
                {
                    objRewardFinal = lstRewardFinal[i];
                    DicRewardFinal.Add(objRewardFinal.RewardFinalID, objRewardFinal.RewardMode);
                }

                List<string> lstRewardIDSem1 = new List<string>();
                List<string> lstRewardIDSem2 = new List<string>();
                string strRewardIDSem1 = lstRewardComment.Where(p => p.SemesterID == GlobalConstants.SEMESTER_OF_YEAR_FIRST).Select(p => p.Rewards).FirstOrDefault();
                string strRewardIDSem2 = lstRewardComment.Where(p => p.SemesterID == GlobalConstants.SEMESTER_OF_YEAR_SECOND).Select(p => p.Rewards).FirstOrDefault();
                string strRewardSemester1 = string.Empty;
                string strRewardSemester2 = string.Empty;

                if (!string.IsNullOrEmpty(strRewardIDSem1))
                {
                    lstRewardIDSem1 = strRewardIDSem1.Split(new string[] { "," }, StringSplitOptions.RemoveEmptyEntries).ToList();
                }

                if (!string.IsNullOrEmpty(strRewardIDSem2))
                {
                    lstRewardIDSem2 = strRewardIDSem2.Split(new string[] { "," }, StringSplitOptions.RemoveEmptyEntries).ToList();
                }

                for (int i = 0, c1 = lstRewardIDSem1.Count; i < c1; i++)
                {
                    strRewardSemester1 += DicRewardFinal[int.Parse(lstRewardIDSem1[i])];
                    if (i < c1 - 1) strRewardSemester1 += "; ";
                }

                for (int i = 0, c2 = lstRewardIDSem2.Count; i < c2; i++)
                {
                    strRewardSemester2 += DicRewardFinal[int.Parse(lstRewardIDSem2[i])];
                    if (i < c2 - 1) strRewardSemester2 += "; ";
                }
                ObjFinalContent.RewardSemester1 = strRewardSemester1;
                ObjFinalContent.RewardSemester2 = strRewardSemester2;

                ObjResult.RewardFinalAndSummedEnding = ObjFinalContent;
                #endregion

                #endregion

                #region Mon hoc va hoat dong giao duc
                List<TeacherNoteBookMonth> lstNoteMonth = (from note in TeacherNoteBookMonthBusiness.All
                                                           where note.SchoolID == schoolID && note.PartitionID == partitionId
                                                               && note.AcademicYearID == academicYearID && note.ClassID == classID && note.PupilID == pupilID
                                                           select note).ToList();

                List<TeacherNoteBookSemester> lstNoteSem = (from note in TeacherNoteBookSemesterBusiness.All
                                                            where note.SchoolID == schoolID && note.PartitionID == partitionId
                                                                && note.AcademicYearID == academicYearID && note.ClassID == classID && note.PupilID == pupilID
                                                            select note).ToList();

                List<SummedUpRecord> lstSummedUpRecord = (from su in SummedUpRecordBusiness.All
                                                          where su.SchoolID == schoolID && su.Last2digitNumberSchool == partitionId
                                                           && su.AcademicYearID == academicYearID && su.ClassID == classID && su.PupilID == pupilID
                                                           && su.PeriodID == null
                                                           && (su.Semester == GlobalConstants.SEMESTER_OF_YEAR_FIRST || su.Semester == GlobalConstants.SEMESTER_OF_YEAR_ALL)
                                                          select su).ToList();

                TeacherNoteBookMonth objNoteMonth = null;
                TeacherNoteBookSemester objNoteSem = null;
                List<SummedUpRecord> lstSummedUpMarkSubject = null;
                if (listSubject != null && listSubject.Count > 0)
                {
                    EducationQuality educationObj = null;
                    SubjectList objSubject = null;
                    for (int i = 0; i < listSubject.Count; i++)
                    {
                        objSubject = listSubject[i];
                        if (objSubject.IsSubjectVNEN == true)
                        {
                            #region Mon hoc theo mo hinh VNEN
                            List<TeacherNoteBookSemester> lstNoteSemPerSubject = lstNoteSem.Where(o => o.SubjectID == objSubject.SubjectID).ToList();
                            foreach (var item in lstNoteSemPerSubject)
                            {
                                educationObj = new EducationQuality();
                                educationObj.SubjectID = objSubject.SubjectID;
                                educationObj.SubjectName = objSubject.SubjectName;
                                educationObj.SemesterID = item.SemesterID.Value;
                                objNoteSem = lstNoteSemPerSubject.FirstOrDefault(o => o.SemesterID == item.SemesterID);
                                if (objNoteSem != null)
                                {
                                    if (objSubject.isCommenting == GlobalConstants.ISCOMMENTING_TYPE_MARK) educationObj.Mark = objNoteSem.PERIODIC_SCORE_END;
                                    else if (objSubject.isCommenting == GlobalConstants.ISCOMMENTING_TYPE_JUDGE) educationObj.JudgeComment = objNoteSem.PERIODIC_SCORE_END_JUDGLE;

                                    if (objNoteSem.Rate == GlobalConstants.REVIEW_TYPE_FINISH) educationObj.EvaluationEnding = GlobalConstants.SHORTFINISH;
                                    else if (objNoteSem.Rate == GlobalConstants.REVIEW_TYPE_NOT_FINISH) educationObj.EvaluationEnding = GlobalConstants.SHORTNOTFINISH;
                                }
                                if (item.SemesterID == GlobalConstants.SEMESTER_OF_YEAR_FIRST) objNoteMonth = lstNoteMonth.FirstOrDefault(o => o.SubjectID == objSubject.SubjectID && o.MonthID == GlobalConstants.MonthID_EndSemester1);
                                else if (item.SemesterID == GlobalConstants.SEMESTER_OF_YEAR_SECOND) objNoteMonth = lstNoteMonth.FirstOrDefault(o => o.SubjectID == objSubject.SubjectID && o.MonthID == GlobalConstants.MonthID_EndSemester2);

                                if (objNoteMonth != null) educationObj.EndingComments = string.Format("{0}\r\n{1}", objNoteMonth.CommentSubject, objNoteMonth.CommentCQ);

                                lstEducationQuality.Add(educationObj);
                            }
                            #endregion
                        }
                        else
                        {
                            #region TT58
                            lstSummedUpMarkSubject = lstSummedUpRecord.Where(o => o.SubjectID == objSubject.SubjectID).ToList();
                            foreach (var item in lstSummedUpMarkSubject)
                            {
                                educationObj = new EducationQuality();
                                educationObj.SubjectID = objSubject.SubjectID;
                                educationObj.SubjectName = objSubject.SubjectName;
                                educationObj.SemesterID = item.Semester.HasValue ? item.Semester.Value : 0;

                                if (objSubject.isCommenting == GlobalConstants.SUBJECTCAT_ISCOMMENTING_MARK) educationObj.Mark = item.SummedUpMark;
                                else if (objSubject.isCommenting == GlobalConstants.SUBJECTCAT_ISCOMMENTING_COMMENT) educationObj.JudgeComment = item.JudgementResult;

                                lstEducationQuality.Add(educationObj);
                            }
                            #endregion
                        }
                    }
                }
                ObjResult.ListEducationQuality = lstEducationQuality;
                #endregion

                // Danh sach mon hoc
                ObjResult.ListSubject = listSubject;
                ObjResult.ValidateCode = WCFConstant.RESPONSE_SUCCESS;
            }
            catch (Exception ex)
            {
                LogExtensions.GetCurrentClass = System.Reflection.MethodBase.GetCurrentMethod().DeclaringType;
                string para = string.Format("schoolID={0}, academicYearID={1}, classID={2}, semesterID={3}, pupilID={4}", schoolID, academicYearID, classID, semesterID, pupilID);
                LogExtensions.ErrorExt(logger, DateTime.Now, para, ex.Message, "null");
                ObjResult.ValidateCode = WCFConstant.RESPONSE_EXCEPTION;
            }

            return ObjResult;
        }
        #endregion


        #region private method
        /// <summary>
        /// xep order cho cac mon hoc cua cap 1
        /// </summary>
        /// <author date="2014/03/09">HaiVT</author>
        /// <param name="isPrimary">true neu cap 1</param>
        /// <param name="title">title con diem</param>
        /// <returns></returns>
        private int SetOrderPrimary(bool isPrimary, string title)
        {
            int orderID = 0;
            if (isPrimary)
            {
                if (title.Contains("ĐTX1") || title.Contains("ĐTX6"))
                {
                    orderID = 1;
                }

                if (title.Contains("ĐTX2") || title.Contains("ĐTX7"))
                {
                    orderID = 2;
                }

                if (title.Contains("ĐTX3"))
                {
                    orderID = 3;
                }

                if (title.Contains("GK"))
                {
                    orderID = 4;
                }

                if (title.Contains("ĐTX4") || title.Contains("ĐTX8"))
                {
                    orderID = 5;
                }

                if (title.Contains("ĐTX5") || title.Contains("ĐTX9"))
                {
                    orderID = 5;
                }

                if (title.Contains("CK"))
                {
                    orderID = 6;
                }
            }

            return orderID;
        }
        public static string GetJudgeMark(decimal? mark)
        {
            if (mark == 1.0M)
                return "Đ";
            else if (mark == 0.0M)
                return "CĐ";
            return String.Empty;
        }
        public static string GetJudgePrimary(decimal? mark)
        {
            if (mark == 6)
                return "A+";
            else if (mark == 7)
                return "A";
            else if (mark == 8)
                return "B";
            return String.Empty;
        }
        public static string GetJudgeMark1(decimal? mark)
        {
            if (mark == 1.0M)
                return "Giỏi";
            else if (mark == 2.0M)
                return "Khá";
            else if (mark == 3.0M)
                return "Trung bình";
            else if (mark == 4.0M)
                return "Yếu";
            else if (mark == 5.0M)
                return "Kém";
            return String.Empty;
        }
        public static string ConvertToJudge(decimal? mark)
        {
            if (mark >= 8.0M && mark <= 10.0M)
                return "Giỏi";
            else if (mark >= 6.5M && mark <= 7.9M)
                return "Khá";
            else if (mark >= 5.0M && mark <= 6.4M)
                return "Trung bình";
            else if (mark >= 3.5M && mark <= 4.9M)
                return "Yếu";
            else if (mark >= 0.0M && mark <= 3.4M)
                return "Kém";
            return String.Empty;
        }

        private int SetMonthWithPrimary(int month)
        {
            int Item = 0;
            if (month == 1)
            {
                Item = 5;
            }
            else if (month == 2)
            {
                Item = 6;
            }
            else if (month == 3)
            {
                Item = 7;
            }
            else if (month == 4)
            {
                Item = 8;
            }
            else if (month == 5)
            {
                Item = 9;
            }
            else if (month == 9)
            {
                Item = 1;
            }
            else if (month == 10)
            {
                Item = 2;
            }
            else if (month == 11)
            {
                Item = 3;
            }
            else if (month == 12)
            {
                Item = 4;
            }
            return Item;
        }
        #endregion

        #region private class
        private class FaultByTime
        {
            public int PupilID { get; set; }

            public int FaultID { get; set; }

            public int? NumberOfFault { get; set; }

            public string Resolution { get; set; }

            public string Note { get; set; }
        }

        private class FaultByTimeLite
        {
            public int FaultID { get; set; }

            public string Resolution { get; set; }

            public string Note { get; set; }
        }

        private class RecordBO
        {
            public int SubjectID;
            public string SubjectName;
            public string Title;
            public int? Semester;
            public int MarkTypeID;
        }

        private class MarkRecordBO : RecordBO
        {
            public decimal Mark;
        }

        private class JudgeRecordBO : RecordBO
        {
            public string Judgement;
        }

        private class SummedUpRecordOfPupil
        {
            public int SubjectID;
            public string SubjectName;
            public int? Semester;
            public int IsCommenting;
            public decimal? SummedUpMark;
            public decimal? RetestMark;
            public string RetestJudge;
            public string JudgementResult;

        }
        #endregion

        #region Cap nhat danh sach dang ky HD
        /// <summary>
        ///  Anhnph - Cap nhat hoc sinh co dang ky HD
        /// </summary>
        /// <modifer data="3/2/2015">AnhVD9</modifer>
        /// <param name="lstPupilUpdate"></param>
        /// <param name="SchoolID"></param>
        /// <param name="AcademicYearID"></param>
        /// <param name="UserID"></param>
        /// <returns></returns>
        public bool updateRegisterContractPupil(List<int> lstPupilOfClassIDUpdate)
        {
            try
            {
                if (lstPupilOfClassIDUpdate == null || lstPupilOfClassIDUpdate.Count == 0) return true;
                //Khoi tao Business
                DeclareBusiness();

                this.Context.Configuration.AutoDetectChangesEnabled = false;
                this.Context.Configuration.ValidateOnSaveEnabled = false;
                List<PupilOfClass> LstPOC = this.Context.PupilOfClass.Where(o => lstPupilOfClassIDUpdate.Contains(o.PupilOfClassID) && o.Status == GlobalConstants.PUPIL_STATUS_STUDYING).ToList();
                for (int i = LstPOC.Count - 1; i >= 0; i--)
                {
                    LstPOC[i].IsRegisterContract = true;
                }
                this.Context.Configuration.AutoDetectChangesEnabled = true;
                this.Context.Configuration.ValidateOnSaveEnabled = true;
                this.Context.SaveChanges();
            }
            catch (Exception ex)
            {
                LogExtensions.GetCurrentClass = System.Reflection.MethodBase.GetCurrentMethod().DeclaringType;
                string para = "Null";
                LogExtensions.ErrorExt(logger, DateTime.Now, para, ex.Message, "null");
                return false;
            }
            return true;
        }

        #endregion

        #region lay du lieu TT22
        /// <summary>
        /// Lay noi dung tin nhan TT22
        /// </summary>
        /// <param name="pupilIDList">List PupilID</param>
        /// <param name="schoolID"></param>
        /// <param name="academicYearID"></param>
        /// <param name="classID"></param>
        /// <param name="type">1: Ket qua giua ky, 2: Ket qua cuoi ky, 3: Nhan xet cuoi ky</param>
        /// <param name="semesterID"></param>
        /// <returns></returns>
        public RatedCommentPupilComposite GetListRatedCommentPupil(List<long> pupilIDList, int schoolID, int academicYearID, int classID, int type, int semesterID)
        {
            RatedCommentPupilComposite objResult = new RatedCommentPupilComposite();
            try
            {
                DeclareBusiness();

                IDictionary<string, object> dic;

                //list hoc sinh trong lop
                List<PupilOfClassBO> lstPupilOfClass = (from poc in PupilOfClassBusiness.GetPupilInClass(classID)
                                                        where pupilIDList.Contains(poc.PupilID)
                                                        select poc)
                                                         .OrderBy(c => c.OrderInClass)
                                                         .ThenBy(c => c.Name)
                                                         .ThenBy(c => c.PupilFullName).ToList();

                //list mon hoc
                List<SubjectCatBO> lstClassSubject = new List<SubjectCatBO>();
                if (classID > 0)
                {
                    lstClassSubject = (from sb in ClassSubjectBusiness.All
                                       join sub in SubjectCatBusiness.All on sb.SubjectID equals sub.SubjectCatID
                                       orderby sb.IsCommenting, sub.OrderInSubject
                                       where sb.ClassID == classID
                                       && sub.IsActive == true
                                       && ((semesterID == GlobalConstants.SEMESTER_OF_YEAR_FIRST && sb.SectionPerWeekFirstSemester > 0)
                                       || (semesterID == GlobalConstants.SEMESTER_OF_YEAR_SECOND && sb.SectionPerWeekSecondSemester > 0))
                                       select new SubjectCatBO
                                       {
                                           SubjectCatID = sb.SubjectID,
                                           SubjectName = sub.SubjectName,
                                           IsCommenting = sb.IsCommenting
                                       }
                                     ).ToList();
                }

                //Khen thuong 
                dic = new Dictionary<string, object>()
                {
                    {"SchoolID",schoolID},
                    {"AcademicYearID",academicYearID},
                    {"lstClassID",new List<int>{classID}},
                    {"Semester", GlobalConstants.SEMESTER_OF_EVALUATION_RESULT}
                };
                List<SummedEndingEvaluationBO> lstSEE = SummedEndingEvaluationBusiness.Search(dic).ToList();

                dic = new Dictionary<string, object>()
                {
                    {"SchoolID",schoolID},
                    {"AcademicYearID",academicYearID},
                    {"lstClassID",new List<int>{classID}},
                    {"SemesterID", semesterID},
                    {"lstPupilID", pupilIDList.ConvertAll(i => (int)i)},

                };

                List<EvaluationReward> lstEvaluationReward = EvaluationRewardBusiness.Search(dic).ToList();

                //List loai nang luc va pham chat
                List<EvaluationCriteria> lstEc = EvaluationCriteriaBusiness.All.ToList();
                List<EvaluationCriteria> lstEcNL = lstEc.Where(o => o.TypeID == 2).OrderBy(o => o.EvaluationCriteriaID).ToList();
                List<EvaluationCriteria> lstEcPC = lstEc.Where(o => o.TypeID == 3).OrderBy(o => o.EvaluationCriteriaID).ToList();

                List<RatedCommentData> lstData = new List<RatedCommentData>();
                RatedCommentData objData;

                dic = new Dictionary<string, object>();
                dic["SchoolID"] = schoolID;
                dic["AcademicYearID"] = academicYearID;
                dic["ClassID"] = classID;
                dic["SemesterID"] = semesterID;

                AcademicYear objAy = AcademicYearBusiness.Find(academicYearID);

                List<RatedCommentPupilBO> lstRatedCommentAll;
                if (UtilsBusiness.IsMoveHistory(objAy))
                {
                    lstRatedCommentAll = RatedCommentPupilHistoryBusiness.Search(dic)
                        .Select(o => new RatedCommentPupilBO
                        {
                            CapacityEndingEvaluation = o.CapacityEndingEvaluation,
                            CapacityMiddleEvaluation = o.CapacityMiddleEvaluation,
                            Comment = o.Comment,
                            EndingEvaluation = o.EndingEvaluation,
                            EvaluationID = o.EvaluationID,
                            MiddleEvaluation = o.MiddleEvaluation,
                            PeriodicEndingJudgement = o.PeriodicEndingJudgement,
                            PeriodicEndingMark = o.PeriodicEndingMark,
                            PeriodicMiddleJudgement = o.PeriodicMiddleJudgement,
                            PeriodicMiddleMark = o.PeriodicMiddleMark,
                            PupilID = o.PupilID,
                            QualityEndingEvaluation = o.QualityEndingEvaluation,
                            QualityMiddleEvaluation = o.QualityMiddleEvaluation,
                            RetestMark = o.RetestMark,
                            SemesterID = o.SemesterID,
                            SubjectID = o.SubjectID

                        }).ToList();
                }
                else
                {
                    lstRatedCommentAll = RatedCommentPupilBusiness.Search(dic)
                        .Select(o => new RatedCommentPupilBO
                        {
                            CapacityEndingEvaluation = o.CapacityEndingEvaluation,
                            CapacityMiddleEvaluation = o.CapacityMiddleEvaluation,
                            Comment = o.Comment,
                            EndingEvaluation = o.EndingEvaluation,
                            EvaluationID = o.EvaluationID,
                            MiddleEvaluation = o.MiddleEvaluation,
                            PeriodicEndingJudgement = o.PeriodicEndingJudgement,
                            PeriodicEndingMark = o.PeriodicEndingMark,
                            PeriodicMiddleJudgement = o.PeriodicMiddleJudgement,
                            PeriodicMiddleMark = o.PeriodicMiddleMark,
                            PupilID = o.PupilID,
                            QualityEndingEvaluation = o.QualityEndingEvaluation,
                            QualityMiddleEvaluation = o.QualityMiddleEvaluation,
                            RetestMark = o.RetestMark,
                            SemesterID = o.SemesterID,
                            SubjectID = o.SubjectID

                        }).ToList();
                }

                for (int i = 0; i < lstPupilOfClass.Count; i++)
                {
                    PupilOfClassBO poc = lstPupilOfClass[i];
                    objData = new RatedCommentData();
                    objData.PupilID = poc.PupilID;

                    List<RatedCommentPupilBO> lstRatedCommentPupil = lstRatedCommentAll.Where(o => o.PupilID == poc.PupilID).ToList();
                    StringBuilder sb = new StringBuilder();

                    string tempNL;
                    string tempPC;
                    string tempKT;
                    //Tong hop ket qua
                    switch (type)
                    {
                        //Ket qua giua ky
                        case 1:
                            //Mon hoc & HDGD
                            for (int j = 0; j < lstClassSubject.Count; j++)
                            {
                                SubjectCatBO subject = lstClassSubject[j];

                                RatedCommentPupilBO ratedCommentSubject = lstRatedCommentPupil
                                    .Where(o => o.EvaluationID == 1 && o.SubjectID == subject.SubjectCatID).FirstOrDefault();

                                if (ratedCommentSubject != null)
                                {
                                    string evaluation = ratedCommentSubject.MiddleEvaluation == 1 ? "T"
                                        : (ratedCommentSubject.MiddleEvaluation == 2 ? "H" :
                                        (ratedCommentSubject.MiddleEvaluation == 3 ? "C" : string.Empty));

                                    string mark;

                                    if (subject.IsCommenting.HasValue && subject.IsCommenting == 1)
                                    {
                                        mark = ratedCommentSubject.PeriodicMiddleJudgement;
                                    }
                                    else
                                    {
                                        mark = ratedCommentSubject.PeriodicMiddleMark.HasValue ?
                                            ratedCommentSubject.PeriodicMiddleMark.Value.ToString() : string.Empty;
                                    }

                                    if (!string.IsNullOrEmpty(evaluation) || !string.IsNullOrEmpty(mark))
                                    {
                                        sb.Append(subject.SubjectName);
                                        sb.Append(": ");
                                        if (!string.IsNullOrEmpty(evaluation))
                                        {
                                            sb.Append(evaluation);
                                        }

                                        if (!string.IsNullOrEmpty(evaluation) && !string.IsNullOrEmpty(mark))
                                        {
                                            sb.Append(",");
                                        }

                                        if (!string.IsNullOrEmpty(mark))
                                        {
                                            sb.Append(mark);
                                        }

                                        sb.Append(";");
                                    }
                                }
                            }
                            sb.Append("\n");

                            //Nang luc
                            tempNL = string.Empty;
                            for (int j = 0; j < lstEcNL.Count; j++)
                            {
                                EvaluationCriteria ec = lstEcNL[j];

                                RatedCommentPupilBO ratedCommentSubject = lstRatedCommentPupil
                                    .Where(o => o.EvaluationID == 2 && o.SubjectID == ec.EvaluationCriteriaID).FirstOrDefault();

                                if (ratedCommentSubject != null)
                                {
                                    string evaluation = ratedCommentSubject.CapacityMiddleEvaluation == 1 ? "T"
                                        : (ratedCommentSubject.CapacityMiddleEvaluation == 2 ? "Đ" :
                                        (ratedCommentSubject.CapacityMiddleEvaluation == 3 ? "C" : string.Empty));

                                    if (!string.IsNullOrEmpty(evaluation))
                                    {
                                        tempNL += evaluation;
                                        tempNL += ";";
                                    }
                                }
                            }
                            if (!string.IsNullOrWhiteSpace(tempNL))
                            {
                                sb.Append("Năng lực: ");
                                sb.Append(tempNL);
                                sb.Append("\n");
                            }

                            //Pham chat
                            tempPC = string.Empty;
                            for (int j = 0; j < lstEcPC.Count; j++)
                            {
                                EvaluationCriteria ec = lstEcPC[j];

                                RatedCommentPupilBO ratedCommentSubject = lstRatedCommentPupil
                                    .Where(o => o.EvaluationID == 3 && o.SubjectID == ec.EvaluationCriteriaID).FirstOrDefault();

                                if (ratedCommentSubject != null)
                                {
                                    string evaluation = ratedCommentSubject.QualityMiddleEvaluation == 1 ? "T"
                                        : (ratedCommentSubject.QualityMiddleEvaluation == 2 ? "Đ" :
                                        (ratedCommentSubject.QualityMiddleEvaluation == 3 ? "C" : string.Empty));

                                    if (!string.IsNullOrEmpty(evaluation))
                                    {
                                        tempPC += evaluation;
                                        tempPC += ";";
                                    }
                                }
                            }

                            if (!string.IsNullOrWhiteSpace(tempPC))
                            {
                                sb.Append("Phẩm chất: ");
                                sb.Append(tempPC);
                                sb.Append("\n");
                            }

                            break;
                        //Ket qua cuoi ky
                        case 2:
                            //Mon hoc & HDGD
                            for (int j = 0; j < lstClassSubject.Count; j++)
                            {
                                SubjectCatBO subject = lstClassSubject[j];

                                RatedCommentPupilBO ratedCommentSubject = lstRatedCommentPupil
                                    .Where(o => o.EvaluationID == 1 && o.SubjectID == subject.SubjectCatID).FirstOrDefault();

                                if (ratedCommentSubject != null)
                                {
                                    string evaluation = ratedCommentSubject.EndingEvaluation == 1 ? "T"
                                        : (ratedCommentSubject.EndingEvaluation == 2 ? "H" :
                                        (ratedCommentSubject.EndingEvaluation == 3 ? "C" : string.Empty));

                                    string mark;

                                    if (subject.IsCommenting.HasValue && subject.IsCommenting == 1)
                                    {
                                        mark = ratedCommentSubject.PeriodicEndingJudgement;
                                    }
                                    else
                                    {
                                        mark = ratedCommentSubject.PeriodicEndingMark.HasValue ?
                                            ratedCommentSubject.PeriodicEndingMark.Value.ToString() : string.Empty;
                                    }

                                    if (!string.IsNullOrEmpty(evaluation) || !string.IsNullOrEmpty(mark))
                                    {
                                        sb.Append(subject.SubjectName);
                                        sb.Append(": ");
                                        if (!string.IsNullOrEmpty(evaluation))
                                        {
                                            sb.Append(evaluation);
                                        }

                                        if (!string.IsNullOrEmpty(evaluation) && !string.IsNullOrEmpty(mark))
                                        {
                                            sb.Append(",");
                                        }

                                        if (!string.IsNullOrEmpty(mark))
                                        {
                                            sb.Append(mark);
                                        }

                                        sb.Append(";");
                                    }
                                }
                            }
                            sb.Append("\n");

                            //Nang luc

                            tempNL = string.Empty;
                            for (int j = 0; j < lstEcNL.Count; j++)
                            {
                                EvaluationCriteria ec = lstEcNL[j];

                                RatedCommentPupilBO ratedCommentSubject = lstRatedCommentPupil
                                    .Where(o => o.EvaluationID == 2 && o.SubjectID == ec.EvaluationCriteriaID).FirstOrDefault();

                                if (ratedCommentSubject != null)
                                {
                                    string evaluation = ratedCommentSubject.CapacityEndingEvaluation == 1 ? "T"
                                        : (ratedCommentSubject.CapacityEndingEvaluation == 2 ? "Đ" :
                                        (ratedCommentSubject.CapacityEndingEvaluation == 3 ? "C" : string.Empty));

                                    if (!string.IsNullOrEmpty(evaluation))
                                    {
                                        tempNL += evaluation;
                                        tempNL += ";";
                                    }
                                }
                            }

                            if (!string.IsNullOrWhiteSpace(tempNL))
                            {
                                sb.Append("Năng lực: ");
                                sb.Append(tempNL);
                                sb.Append("\n");
                            }

                            //Pham chat

                            tempPC = string.Empty;
                            for (int j = 0; j < lstEcPC.Count; j++)
                            {
                                EvaluationCriteria ec = lstEcPC[j];

                                RatedCommentPupilBO ratedCommentSubject = lstRatedCommentPupil
                                    .Where(o => o.EvaluationID == 3 && o.SubjectID == ec.EvaluationCriteriaID).FirstOrDefault();

                                if (ratedCommentSubject != null)
                                {
                                    string evaluation = ratedCommentSubject.QualityEndingEvaluation == 1 ? "T"
                                        : (ratedCommentSubject.QualityEndingEvaluation == 2 ? "Đ" :
                                        (ratedCommentSubject.QualityEndingEvaluation == 3 ? "C" : string.Empty));

                                    if (!string.IsNullOrEmpty(evaluation))
                                    {
                                        tempPC += evaluation;
                                        tempPC += ";";
                                    }
                                }
                            }

                            if (!string.IsNullOrWhiteSpace(tempPC))
                            {
                                sb.Append("Phẩm chất: ");
                                sb.Append(tempPC);
                                sb.Append("\n");
                            }

                            //Khen thuong

                            tempKT = string.Empty;
                            SummedEndingEvaluationBO see = lstSEE.Where(o => o.PupilID == poc.PupilID && semesterID == GlobalConstants.SEMESTER_OF_YEAR_SECOND && o.RewardID != null && o.RewardID != 0).FirstOrDefault();
                            if (see != null)
                            {
                                tempKT += "Học sinh xuất sắc";
                                tempKT += ";";
                            }

                            List<EvaluationReward> lstEvaluationRewardPupil = lstEvaluationReward.Where(o => o.PupilID == poc.PupilID).ToList();

                            for (int j = 0; j < lstEvaluationRewardPupil.Count; j++)
                            {
                                EvaluationReward er = lstEvaluationRewardPupil[j];
                                string rewardName = er.RewardID == 1 || er.RewardID == 2 ? er.Content : string.Empty;
                                if (!string.IsNullOrEmpty(rewardName))
                                {
                                    tempKT += rewardName;
                                    tempKT += ";";
                                }
                            }

                            if (!string.IsNullOrWhiteSpace(tempKT))
                            {
                                sb.Append("Khen thưởng: ");
                                sb.Append(tempKT);
                                sb.Append("\n");
                            }

                            if (semesterID == 2)
                            {
                                SummedEndingEvaluationBO objSee = lstSEE.Where(o => o.PupilID == poc.PupilID).FirstOrDefault();

                                if (objSee != null)
                                {

                                    string evaluation = string.Empty;
                                    if (objSee.EndingEvaluation == "HT")
                                    {
                                        evaluation = "Được lên lớp";
                                    }
                                    else if (objSee.EndingEvaluation == "CHT" && objSee.RateAdd != 0 && objSee.RateAdd != 1)
                                    {
                                        evaluation = "Chưa hoàn thành chương trình lớp học";
                                    }
                                    else if (objSee.EndingEvaluation == "CHT" && objSee.RateAdd == 1)
                                    {
                                        evaluation = "Được lên lớp";
                                    }
                                    else if (objSee.EndingEvaluation == "CHT" && objSee.RateAdd == 0)
                                    {
                                        evaluation = "Ở lại lớp";
                                    }

                                    sb.Append(evaluation);
                                }
                            }

                            break;
                        //Nhat xet cuoi ky
                        case 3:
                            //Mon hoc & HDGD
                            for (int j = 0; j < lstClassSubject.Count; j++)
                            {
                                SubjectCatBO subject = lstClassSubject[j];

                                RatedCommentPupilBO ratedCommentSubject = lstRatedCommentPupil
                                    .Where(o => o.EvaluationID == 1 && o.SubjectID == subject.SubjectCatID).FirstOrDefault();

                                if (ratedCommentSubject != null)
                                {
                                    string comment = ratedCommentSubject.Comment;

                                    if (!string.IsNullOrEmpty(comment))
                                    {
                                        sb.Append(subject.SubjectName);
                                        sb.Append(": ");
                                        sb.Append(comment);
                                        sb.Append(";");
                                    }
                                }
                            }
                            sb.Append("\n");

                            //Nang luc

                            tempNL = string.Empty;

                            RatedCommentPupilBO objRatedComment = lstRatedCommentPupil
                                .Where(o => o.EvaluationID == 2).FirstOrDefault();

                            if (objRatedComment != null)
                            {
                                string comment = objRatedComment.Comment;

                                if (!string.IsNullOrEmpty(comment))
                                {
                                    tempNL += comment;
                                    tempNL += ";";
                                }
                            }

                            if (!string.IsNullOrWhiteSpace(tempNL))
                            {
                                sb.Append("Năng lực: ");
                                sb.Append(tempNL);
                                sb.Append("\n");
                            }

                            //Pham chat
                            tempPC = string.Empty;

                            objRatedComment = lstRatedCommentPupil
                                .Where(o => o.EvaluationID == 3).FirstOrDefault();

                            if (objRatedComment != null)
                            {
                                string comment = objRatedComment.Comment;

                                if (!string.IsNullOrEmpty(comment))
                                {
                                    tempPC += comment;
                                    tempPC += ";";
                                }
                            }


                            if (!string.IsNullOrWhiteSpace(tempPC))
                            {
                                sb.Append("Phẩm chất: ");
                                sb.Append(tempPC);
                            }
                            break;
                    }

                    objData.SMSContent = sb.ToString().Trim();
                    lstData.Add(objData);
                }

                objResult.ListResult = lstData;
                objResult.ValidateCode = WCFConstant.RESPONSE_SUCCESS;
            }
            catch (Exception ex)
            {
                LogExtensions.GetCurrentClass = System.Reflection.MethodBase.GetCurrentMethod().DeclaringType;
                string para = string.Format("schoolID={0}, academicYearID={1}, classID={2}, type={3}, semesterID={4}", schoolID, academicYearID, classID, type, semesterID);
                LogExtensions.ErrorExt(logger, DateTime.Now, para, ex.Message, "null");
                objResult.ValidateCode = WCFConstant.RESPONSE_EXCEPTION;
            }
            return objResult;
        }
        #endregion

        #region viethd31 Mam non
        /// <summary>
        /// Thong bao thuc don ngay va thuc don tuan
        /// </summary>
        /// <param name="pupilIDList"></param>
        /// <param name="schoolID"></param>
        /// <param name="academicYearID"></param>
        /// <param name="classID"></param>
        /// <param name="type"></param>
        /// <param name="fromDate"></param>
        /// <param name="toDate"></param>
        /// <returns></returns>
        public PreschoolResultComposite GetMealMenuOfPupil(List<long> pupilIDList, int schoolID, int academicYearID, int classID, int type, DateTime fromDate, DateTime toDate)
        {
            PreschoolResultComposite objResult = new PreschoolResultComposite();
            try
            {
                DeclareBusiness();

                //list hoc sinh trong lop
                List<PupilOfClassBO> lstPupilOfClass = (from poc in PupilOfClassBusiness.GetPupilInClass(classID)
                                                        where pupilIDList.Contains(poc.PupilID)
                                                        select poc)
                                                         .OrderBy(c => c.OrderInClass)
                                                         .ThenBy(c => c.Name)
                                                         .ThenBy(c => c.PupilFullName).ToList();

                //Lay ra danh sach bua an
                List<MealCategory> lstMealCategory = this.MealCategoryBusiness.GetMealCategoriesBySchool(schoolID);

                //Lay thuc don cua lop
                IDictionary<string, object> dic = new Dictionary<string, object>();
                dic["SchoolID"] = schoolID;
                dic["AcademicYearID"] = academicYearID;
                dic["ClassID"] = classID;
                dic["DateFrom"] = fromDate;
                dic["DateTo"] = toDate;

                List<WeeklyMealMenu> lstWmm = this.WeeklyMealMenuBusiness.Search(dic).OrderBy(o=>o.OrderNumber).ToList();
                StringBuilder sb = new StringBuilder();

                //Thuc don ngay
                if (type == 1)
                {
                    List<WeeklyMealMenu> lstWmmByDay = lstWmm.Where(o => o.DateOfMeal.CompareTo(fromDate) == 0).ToList();

                    for (int i = 0; i < lstMealCategory.Count; i++)
                    {
                        MealCategory mc = lstMealCategory[i];
                        List<WeeklyMealMenu> lstWmmOfCat = lstWmmByDay.Where(o => o.MealCategoryID == mc.MealCategoryID).ToList();

                        if (lstWmmOfCat.Count > 0)
                        {
                            sb.Append(mc.MealName + ": ");
                            for (int j = 0; j < lstWmmOfCat.Count; j++)
                            {
                                WeeklyMealMenu wmm = lstWmmOfCat[j];
                                sb.Append(wmm.DishName);

                                if (j < lstWmmOfCat.Count - 1)
                                {
                                    sb.Append(", ");
                                }
                                
                            }

                            sb.Append("\n");
                        }
                    }
                }
                //Thuc don tuan
                else
                {
                    for (DateTime date = fromDate; date.CompareTo(toDate) <= 0; date = date.AddDays(1))
                    {
                        List<WeeklyMealMenu> lstWmmByDay = lstWmm.Where(o => o.DateOfMeal.CompareTo(date) == 0).ToList();
                        if (lstWmmByDay.Count > 0)
                        {
                            sb.Append(date.ToString("dd/MM") + ".");

                            int index = 0;
                            int catCount = lstWmmByDay.Select(o => o.MealCategoryID).Distinct().Count();
                            for (int i = 0; i < lstMealCategory.Count; i++)
                            {
                                MealCategory mc = lstMealCategory[i];
                                List<WeeklyMealMenu> lstWmmOfCat = lstWmmByDay.Where(o => o.MealCategoryID == mc.MealCategoryID).ToList();

                                if (lstWmmOfCat.Count > 0)
                                {
                                    sb.Append(mc.MealName + ": ");
                                    for (int j = 0; j < lstWmmOfCat.Count; j++)
                                    {
                                        WeeklyMealMenu wmm = lstWmmOfCat[j];
                                        sb.Append(wmm.DishName);

                                        if (j < lstWmmOfCat.Count - 1)
                                        {
                                            sb.Append(", ");
                                        }

                                    }

                                    if (index < catCount - 1)
                                    {
                                        sb.Append(";");
                                    }

                                    index++;
                                }
                            }

                            sb.Append("\n");
                        }
                    }
                }

                List<PreschoolResultData> lstData = new List<PreschoolResultData>();
                PreschoolResultData objData;

                for (int i = 0; i < lstPupilOfClass.Count; i++)
                {
                    PupilOfClassBO poc = lstPupilOfClass[i];
                    objData = new PreschoolResultData();
                    objData.PupilID = poc.PupilID;
                    objData.SMSContent = sb.ToString().Trim();
                   
                    lstData.Add(objData);
                }

                objResult.ListResult = lstData;
                objResult.ValidateCode = WCFConstant.RESPONSE_SUCCESS;
            }
            catch (Exception ex)
            {
                logger.Error("Exception", ex);
                objResult.ValidateCode = WCFConstant.RESPONSE_EXCEPTION;
            }
            return objResult;
        }

        /// <summary>
        /// Can do suc khoe
        /// </summary>
        /// <param name="pupilIDList"></param>
        /// <param name="schoolID"></param>
        /// <param name="academicYearID"></param>
        /// <param name="classID"></param>
        /// <param name="month"></param>
        /// <returns></returns>
        public PreschoolResultComposite GetPhysicalResultOfPupil(List<long> pupilIDList, int schoolID, int academicYearID, int classID, int month)
        {
            PreschoolResultComposite objResult = new PreschoolResultComposite();
            try
            {
                DeclareBusiness();

                //list hoc sinh trong lop
                List<PupilOfClassBO> lstPupilOfClass = (from poc in PupilOfClassBusiness.GetPupilInClass(classID)
                                                        where pupilIDList.Contains(poc.PupilID)
                                                        select poc)
                                                         .OrderBy(c => c.OrderInClass)
                                                         .ThenBy(c => c.Name)
                                                         .ThenBy(c => c.PupilFullName).ToList();

                //Lay danh sach ket qua can do suc khoe
                IDictionary<string, object> dic = new Dictionary<string, object>();
                dic["SchoolID"] = schoolID;
                dic["AcademicYearID"] = academicYearID;
                dic["ClassID"] = classID;
                dic["MonthID"] = month;

                List<PhysicalExamination> lstPe = this.PhysicalExaminationBusiness.Search(dic).ToList();

                List<PreschoolResultData> lstData = new List<PreschoolResultData>();
                PreschoolResultData objData;

                for (int i = 0; i < lstPupilOfClass.Count; i++)
                {
                    PupilOfClassBO poc = lstPupilOfClass[i];
                    StringBuilder sb = new StringBuilder();

                    objData = new PreschoolResultData();
                    objData.PupilID = poc.PupilID;

                    PhysicalExamination peOfPupil = lstPe.FirstOrDefault(o => o.PupilID == poc.PupilID);

                    if (peOfPupil != null)
                    {
                        if (peOfPupil.Height.HasValue)
                        {
                            sb.Append(string.Format("Cao: {0} cm - {1}", String.Format("{0:0.#}", peOfPupil.Height.Value), this.GetPhysicalState(peOfPupil.PhysicalHeightStatus, 1)));
                            sb.Append("\n");
                        }

                        if (peOfPupil.Weight.HasValue)
                        {
                            sb.Append(string.Format("Nặng: {0} kg - {1}", String.Format("{0:0.#}", peOfPupil.Weight.Value), this.GetPhysicalState(peOfPupil.PhysicalWeightStatus, 2)));
                        }

                    }

                    objData.SMSContent = sb.ToString().Trim();

                    lstData.Add(objData);
                }

                objResult.ListResult = lstData;
                objResult.ValidateCode = WCFConstant.RESPONSE_SUCCESS;
            }
            catch (Exception ex)
            {
                logger.Error("Exception", ex);
                objResult.ValidateCode = WCFConstant.RESPONSE_EXCEPTION;
            }
            return objResult;
        }

        private string GetPhysicalState(int? stt, int type)
        {
            //Chieu cao
            if(type == 1)
            {
                switch (stt.Value)
                {
                    case 1:
                        return "Cao hơn so với tuổi";
                    case 2:
                        return "Bình thường";
                    case 3:
                        return "Thấp còi độ 1";
                    case 4:
                        return "Thấp còi độ 2";
                    default:
                        return string.Empty;
                }
            }
            //Can nang
            else
            {
                switch (stt.Value)
                {
                    case 1:
                        return "Cân nặng hơn so với tuổi";
                    case 2:
                        return "Bình thường";
                    case 3:
                        return "Suy dinh dưỡng nhẹ";
                    case 4:
                        return "Suy dinh dưỡng nặng";
                    default:
                        return string.Empty;
                }
            }
        }

        /// <summary>
        /// Danh gia su phat trien
        /// </summary>
        /// <param name="pupilIDList"></param>
        /// <param name="schoolID"></param>
        /// <param name="academicYearID"></param>
        /// <param name="educationLevel"></param>
        /// <param name="classID"></param>
        /// <returns></returns>
        public PreschoolResultComposite GetGrowthEvaluationOfPupil(List<long> pupilIDList, int schoolID, int academicYearID, int educationLevel, int classID)
        {
            PreschoolResultComposite objResult = new PreschoolResultComposite();
            try
            {
                DeclareBusiness();

                //list hoc sinh trong lop
                List<PupilOfClassBO> lstPupilOfClass = (from poc in PupilOfClassBusiness.GetPupilInClass(classID)
                                                        where pupilIDList.Contains(poc.PupilID)
                                                        select poc)
                                                         .OrderBy(c => c.OrderInClass)
                                                         .ThenBy(c => c.Name)
                                                         .ThenBy(c => c.PupilFullName).ToList();

                int educationLevelID = 0;
                ClassProfile cp = ClassProfileBusiness.Find(classID);
                if (cp != null)
                {
                    educationLevelID = cp.EducationLevelID;
                }
                //Lay danh sach linh vuc
                List<DeclareEvaluationGroupBO> lstDeg = this.DeclareEvaluationGroupBusiness.GetListDeclareEvaluationGroupByLevel(schoolID, academicYearID, educationLevelID);

                //Lay danh sach danh gia su phat trien
                IDictionary<string, object> dic = new Dictionary<string, object>();
                dic["SchoolID"] = schoolID;
                dic["AcademicYearID"] = academicYearID;
                dic["ClassID"] = classID;
                //dic["lstPupilID"] = pupilIDList;
                List<GrowthEvaluation> lstGe = (from g in this.GrowthEvaluationBusiness.Search(dic)
                                                join i in this.DeclareEvaluationIndexBusiness.All
                                                    .Where(o => o.AcademicYearID == academicYearID && o.SchoolID == schoolID && o.EducationLevelID == educationLevelID)
                                                    on new { p1 = g.DeclareEvaluationGroupID, p2 = g.DeclareEvaluationIndexID } equals new { p1 = i.EvaluationDevelopmentGroID, p2 = i.EvaluationDevelopmentID }
                                                select g).ToList();

                List<PreschoolResultData> lstData = new List<PreschoolResultData>();
                PreschoolResultData objData;

                for (int i = 0; i < lstPupilOfClass.Count; i++)
                {
                    PupilOfClassBO poc = lstPupilOfClass[i];
                    StringBuilder sb = new StringBuilder();

                    objData = new PreschoolResultData();
                    objData.PupilID = poc.PupilID;

                    for (int j = 0; j < lstDeg.Count; j++)
                    {
                        DeclareEvaluationGroupBO deg = lstDeg[j];
                        int count = lstGe.Where(o => o.PupilID == poc.PupilID && o.DeclareEvaluationGroupID == deg.EvaluationGroupID).Count();
                        int dCount = lstGe.Where(o => o.PupilID == poc.PupilID && o.DeclareEvaluationGroupID == deg.EvaluationGroupID && (o.EvaluationType == 1 || o.EvaluationType == 3)).Count();

                        if (count > 0)
                        {
                            sb.Append(string.Format("{0}: Đạt {1}/{2}", deg.EvaluationDevelopmentGroupName, dCount, count));
                            sb.Append("\n");
                        }
                    }

                    objData.SMSContent = sb.ToString().Trim();

                    lstData.Add(objData);
                }

                objResult.ListResult = lstData;
                objResult.ValidateCode = WCFConstant.RESPONSE_SUCCESS;
            }
            catch (Exception ex)
            {
                logger.Error("Exception", ex);
                objResult.ValidateCode = WCFConstant.RESPONSE_EXCEPTION;
            }
            return objResult;
        }

        /// <summary>
        /// Danh gia hoat dong
        /// </summary>
        /// <param name="pupilIDList"></param>
        /// <param name="schoolID"></param>
        /// <param name="academicYearID"></param>
        /// <param name="classID"></param>
        /// <param name="date"></param>
        /// <returns></returns>
        public PreschoolResultComposite GetDailyActivityOfPupil(List<long> pupilIDList, int schoolID, int academicYearID, int classID, DateTime date)
        {
            PreschoolResultComposite objResult = new PreschoolResultComposite();
            try
            {
                DeclareBusiness();

                //list hoc sinh trong lop
                List<PupilOfClassBO> lstPupilOfClass = (from poc in PupilOfClassBusiness.GetPupilInClass(classID)
                                                        where pupilIDList.Contains(poc.PupilID)
                                                        select poc)
                                                         .OrderBy(c => c.OrderInClass)
                                                         .ThenBy(c => c.Name)
                                                         .ThenBy(c => c.PupilFullName).ToList();

                //Lay danh sach danh gia hoat dong ngay
                List<ActivityOfPupilToSMS> lstAct = this.ActivityOfPupilBusiness.GetActivityOfPupilToSMS(schoolID, academicYearID, classID, date.Date);

                List<PreschoolResultData> lstData = new List<PreschoolResultData>();
                PreschoolResultData objData;

                for (int i = 0; i < lstPupilOfClass.Count; i++)
                {
                    PupilOfClassBO poc = lstPupilOfClass[i];

                    objData = new PreschoolResultData();
                    objData.PupilID = poc.PupilID;

                    ActivityOfPupilToSMS act = lstAct.FirstOrDefault(o => o.PupilID == poc.PupilID);

                    if (act != null)
                    {
                        objData.SMSContent = act.ContentActivity.Trim();
                    }

                    lstData.Add(objData);
                }

                objResult.ListResult = lstData;
                objResult.ValidateCode = WCFConstant.RESPONSE_SUCCESS;
            }
            catch (Exception ex)
            {
                logger.Error("Exception", ex);
                objResult.ValidateCode = WCFConstant.RESPONSE_EXCEPTION;
            }
            return objResult;
        }

        /// <summary>
        /// Be ngoan tuan, thang
        /// </summary>
        /// <param name="pupilIDList"></param>
        /// <param name="schoolID"></param>
        /// <param name="academicYearID"></param>
        /// <param name="classID"></param>
        /// <param name="type"></param>
        /// <param name="month"></param>
        /// <param name="week"></param>
        /// <returns></returns>
        public PreschoolResultComposite GetGoodTicketOfPupil(List<long> pupilIDList, int schoolID, int academicYearID, int classID, int type, DateTime month, int week)
        {
            PreschoolResultComposite objResult = new PreschoolResultComposite();
            try
            {
                DeclareBusiness();

                //list hoc sinh trong lop
                List<PupilOfClassBO> lstPupilOfClass = (from poc in PupilOfClassBusiness.GetPupilInClass(classID)
                                                        where pupilIDList.Contains(poc.PupilID)
                                                        select poc)
                                                         .OrderBy(c => c.OrderInClass)
                                                         .ThenBy(c => c.Name)
                                                         .ThenBy(c => c.PupilFullName).ToList();

                //Lay danh sach phieu be ngoan
                IDictionary<string, object> dic = new Dictionary<string, object>();
                dic["IsGood"] = true;
                dic["Month"] = month;
                dic["SchoolID"] = schoolID;
                dic["ClassID"] = classID;
                dic["AcademicYearID"] = academicYearID;
                //tuan
                if (type == 1)
                {
                    dic["WeekInMonth"] = week;
                }
                //thang
                //else
                //{
                //    dic["WeekInMonth"] = 5;
                //}

                List<GoodChildrenTicket> lstGct = this.GoodChildrenTicketBusiness.SearchBySchool(schoolID, dic).ToList();
                
                List<PreschoolResultData> lstData = new List<PreschoolResultData>();
                PreschoolResultData objData;

                for (int i = 0; i < lstPupilOfClass.Count; i++)
                {
                    PupilOfClassBO poc = lstPupilOfClass[i];
                    StringBuilder sb = new StringBuilder();
                    objData = new PreschoolResultData();
                    objData.PupilID = poc.PupilID;

                    if (type == 1)
                    {
                        GoodChildrenTicket cgt = lstGct.FirstOrDefault(o => o.PupilID == poc.PupilID);

                        if (cgt != null)
                        {
                            //Tuan
                            if (type == 1)
                            {
                                sb.Append(string.Format("đạt danh hiệu bé ngoan tuần {0} tháng {1}. ", week, month.Month));
                                if (!string.IsNullOrWhiteSpace(cgt.Note))
                                {
                                    sb.Append("Nhận xét gv: ").Append(cgt.Note.Trim());
                                }
                            }
                            //Thang
                            else
                            {
                                sb.Append(string.Format("đạt danh hiệu bé ngoan tháng {0}. ", month.Month));
                                if (!string.IsNullOrWhiteSpace(cgt.Note))
                                {
                                    sb.Append("Nhận xét gv: ").Append(cgt.Note.Trim());
                                }
                            }
                        }
                    }
                    else
                    {
                        int goodCountInMonth = lstGct.Where(o=> o.PupilID == poc.PupilID && o.WeekInMonth != 5).Count();
                        if (goodCountInMonth == 4)
                        {
                            sb.Append(string.Format("đạt danh hiệu bé ngoan tháng {0}. ", month.Month));

                            GoodChildrenTicket cgt = lstGct.FirstOrDefault(o => o.PupilID == poc.PupilID && o.WeekInMonth == 5);
                            if (cgt != null && !string.IsNullOrWhiteSpace(cgt.Note))
                            {
                                sb.Append("Nhận xét gv: ").Append(cgt.Note.Trim());
                            }
                        }
                    }

                    objData.SMSContent = sb.ToString().Trim();

                    lstData.Add(objData);
                }

                objResult.ListResult = lstData;
                objResult.ValidateCode = WCFConstant.RESPONSE_SUCCESS;
            }
            catch (Exception ex)
            {
                logger.Error("Exception", ex);
                objResult.ValidateCode = WCFConstant.RESPONSE_EXCEPTION;
            }
            return objResult;
        }
        #endregion

        #region Tong hop tin nhan tu dinh nghia
        public CustomSMSComposite GetCustomSMS(List<long> pupilIDList, int schoolID, int academicYearID, int classID, int grade, string template, int periodType, bool forVNEN, List<string> lstContentTypeCode, int? nameDisplay,
            int? semester = null, int? month = null, int? year = null, DateTime? fromDate = null, DateTime? toDate = null)
        {
            CustomSMSComposite objResult = new CustomSMSComposite();
            try
            {
                DeclareBusiness();

                //list hoc sinh trong lop
                List<PupilOfClassBO> lstPupilOfClass = (from poc in PupilOfClassBusiness.GetPupilInClass(classID)
                                                        where pupilIDList.Contains(poc.PupilID)
                                                        select poc)
                                                         .OrderBy(c => c.OrderInClass)
                                                         .ThenBy(c => c.Name)
                                                         .ThenBy(c => c.PupilFullName).ToList();


                List<CustomSMSData> lstData = new List<CustomSMSData>();
                CustomSMSData objData;

                //Lay hoc ky da chon
                int bigSemester = 0;
                if (semester == 1 || semester == 3)
                {
                    bigSemester = 1;
                }
                else if (semester == 2 || semester == 4)
                {
                    bigSemester = 2;
                }
                else if (semester == 5)
                {
                    bigSemester = 3;
                }

                //lay hoc ky hien tai
                int curSemester = 0;
                DateTime dateTimeNow = DateTime.Now;
                AcademicYear objAcademicYear = (from a in AcademicYearBusiness.All
                                                where a.SchoolID == schoolID
                                                && a.AcademicYearID == academicYearID
                                                select a).FirstOrDefault();

                if (objAcademicYear != null)
                {
                    if (objAcademicYear.FirstSemesterStartDate.Value.Date <= dateTimeNow && objAcademicYear.SecondSemesterStartDate.Value.Date.AddDays(-1).AddHours(23).AddMinutes(59).AddSeconds(59).AddMilliseconds(59) >= dateTimeNow)
                    {
                        curSemester = 1;
                    }
                    // Anhvd9 - Cho phep nhan tin toi 31.8 neu nam hoc cu da ket thuc
                    if (objAcademicYear.SecondSemesterStartDate.Value.Date <= dateTimeNow && new DateTime(objAcademicYear.SecondSemesterEndDate.Value.Year, 8, 31, 23, 59, 59) >= dateTimeNow)
                    {
                        curSemester = 2;
                    }
                }

                DateTime startTime = default(DateTime);
                DateTime endTime = default(DateTime);
                if (periodType == GlobalConstants.PERIOD_TYPE_SEMESTER)
                {

                    DateTime FromDateHKI = DateTime.Now;
                    DateTime ToDateHKI = DateTime.Now;
                    DateTime FromDateHKII = DateTime.Now;
                    DateTime ToDateHKII = DateTime.Now;
                    if (objAcademicYear != null)
                    {
                        FromDateHKI = objAcademicYear.FirstSemesterStartDate.Value.Date;
                        ToDateHKI = objAcademicYear.FirstSemesterEndDate.Value.Date.AddHours(23).AddMinutes(59).AddSeconds(59);
                        FromDateHKII = objAcademicYear.SecondSemesterStartDate.Value.Date;
                        ToDateHKII = objAcademicYear.SecondSemesterEndDate.Value.Date.AddHours(23).AddMinutes(59).AddSeconds(59);
                    }

                    if (bigSemester == 1)
                    {
                        startTime = FromDateHKI;
                        endTime = ToDateHKI;
                    }
                    else if (bigSemester == 2)
                    {
                        startTime = FromDateHKII;
                        endTime = ToDateHKII;
                    }
                    else if (bigSemester == 3)
                    {
                        startTime = FromDateHKI;
                        endTime = ToDateHKII;
                    }
                }
                else
                {
                    if (periodType == GlobalConstants.PERIOD_TYPE_DAILY)
                    {
                        startTime = DateTime.Now.Date;
                        endTime = DateTime.Now.Date;
                    }
                    else if (periodType == GlobalConstants.PERIOD_TYPE_WEEKLY)
                    {
                        startTime = DateTime.Now.AddDays(-6).Date;
                        endTime = DateTime.Now.Date;
                    }
                    else if (periodType == GlobalConstants.PERIOD_TYPE_MONTHLY)
                    {
                        if (!forVNEN)
                        {
                            startTime = DateTime.Now.AddMonths(-1).AddDays(1).Date;
                            endTime = DateTime.Now.Date;
                        }
                        else
                        {
                            startTime = new DateTime(year.Value, month.Value, 1);
                            endTime = new DateTime(year.Value, month.Value, DateTime.DaysInMonth(year.Value, month.Value));
                            if (DateTime.Compare(endTime, DateTime.Now.Date) >= 0)
                            {
                                endTime = DateTime.Now.Date;
                            }
                        }
                    }
                    else if (periodType == GlobalConstants.PERIOD_TYPE_CUSTOM)
                    {
                        startTime = fromDate.Value;
                        endTime = toDate.Value;
                    }
                }

                IDictionary<string, object> dic = new Dictionary<string, object>();
                #region thoi gian dinh ky
                //Thoi gian dinh ky
                string TGDK = string.Empty;
                switch (periodType)
                {
                    case GlobalConstants.PERIOD_TYPE_DAILY:
                        TGDK = string.Format("ngày {0}", DateTime.Now.Date.ToString("dd/MM/yyyy"));
                        break;
                    case GlobalConstants.PERIOD_TYPE_WEEKLY:
                        TGDK = string.Format("từ {0} đến {1}", startTime.ToString("dd/MM/yyyy"), endTime.ToString("dd/MM/yyyy"));
                        break;
                    case GlobalConstants.PERIOD_TYPE_MONTHLY:

                        TGDK = string.Format("từ {0} đến {1}", startTime.ToString("dd/MM/yyyy"), endTime.ToString("dd/MM/yyyy"));
                        break;
                    case GlobalConstants.PERIOD_TYPE_SEMESTER:
                        TGDK = semester == 1 ? "học kỳ 1" : (semester == 2 ? "học kỳ 2" : (semester == 3 ? "giữa kỳ 1" : (semester == 4 ? "giữa kỳ 2" : (semester == 5 ? "cả năm" : string.Empty))));
                        break;
                    case GlobalConstants.PERIOD_TYPE_CUSTOM:
                        TGDK = string.Format("từ {0} đến {1}", startTime.ToString("dd/MM/yyyy"), endTime.ToString("dd/MM/yyyy"));
                        break;
                }

                dic["TGDK"] = TGDK;
                #endregion

                #region Diem danh
                if (lstContentTypeCode.Contains("DD"))
                {
                    List<CustomSMSData> listPupilTraining = new List<CustomSMSData>();
                    int partitionId = schoolID % 100;


                    // thong tin diem danh cua hoc sinh
                    var lstPupilAbsent = (from p in PupilAbsenceBusiness.All
                                          where p.ClassID == classID
                                          && p.SchoolID == schoolID
                                          && p.AcademicYearID == academicYearID
                                          && p.Last2digitNumberSchool == partitionId
                                          && p.AbsentDate >= startTime
                                          && p.AbsentDate <= endTime
                                          select p).ToList();


                    List<int> pupilIds = new List<int>();
                    if (lstPupilAbsent != null && lstPupilAbsent.Count > 0)
                    {
                        pupilIds = lstPupilAbsent.Select(p => p.PupilID).Distinct().ToList();
                    }

                    List<PupilAbsence> itemAbsences = null;
                    //Tao content cho object
                    int pupilID = 0;
                    string content = string.Empty;
                    int cp = 0, kp = 0;
                    CustomSMSData item = null;
                    for (int i = pupilIds.Count - 1; i >= 0; i--)
                    {
                        content = string.Empty;
                        item = new CustomSMSData();
                        pupilID = pupilIds[i];
                        // diem danh
                        itemAbsences = lstPupilAbsent.Where(o => o.PupilID == pupilID).ToList();
                        if (itemAbsences != null && itemAbsences.Count > 0)
                        {
                            cp = itemAbsences.Where(p => p.IsAccepted == true).Count();
                            kp = itemAbsences.Where(p => p.IsAccepted != true).Count();
                            if (cp > 0)
                            {
                                content += string.Format(WCFConstant.FORMAT_0_1, string.Format("Nghỉ học có phép: {0}", cp), Environment.NewLine);
                            }

                            if (kp > 0)
                            {
                                content += string.Format(WCFConstant.FORMAT_0_1, string.Format("Nghỉ học không phép: {0}", kp), Environment.NewLine);
                            }
                        }

                        item.PupilID = pupilID;
                        item.SMSContent = content;
                        listPupilTraining.Add(item);
                    }

                    dic["DD"] = listPupilTraining;
                }

                #endregion

                #region Vi pham
                if (lstContentTypeCode.Contains("VP"))
                {
                    List<CustomSMSData> listPupilViolate = new List<CustomSMSData>();

                    // thong tin vi pham cua hoc sinh
                    var lstFault = (from f in PupilFaultBusiness.AllNoTracking
                                    join lf in FaultCriteriaBusiness.AllNoTracking on f.FaultID equals lf.FaultCriteriaID
                                    join fg in FaultGroupBusiness.AllNoTracking on lf.GroupID equals fg.FaultGroupID
                                    where f.SchoolID == schoolID
                                    && f.AcademicYearID == academicYearID
                                    && EntityFunctions.TruncateTime(f.ViolatedDate) >= EntityFunctions.TruncateTime(startTime)
                                    && EntityFunctions.TruncateTime(f.ViolatedDate) <= EntityFunctions.TruncateTime(endTime)
                                    select new FaultByTime
                                    {
                                        PupilID = f.PupilID,
                                        FaultID = f.FaultID,
                                        NumberOfFault = f.NumberOfFault,
                                        Resolution = lf.Resolution,
                                        Note = f.Note
                                    }).ToList();

                    List<int> pupilIds = new List<int>();

                    if (lstFault != null && lstFault.Count > 0)
                    {
                        pupilIds.AddRange(lstFault.Select(p => p.PupilID).Distinct().ToList());
                        pupilIds = pupilIds.Distinct().ToList();
                    }

                    //Tao content cho object
                    int pupilID = 0;
                    string content = string.Empty;
                    CustomSMSData item = null;
                    for (int i = pupilIds.Count - 1; i >= 0; i--)
                    {
                        content = string.Empty;
                        item = new CustomSMSData();
                        pupilID = pupilIds[i];

                        //Vi pham
                        var itemFaults = lstFault.Where(a => a.PupilID == pupilID).GroupBy(l => new { l.FaultID, l.Resolution })
                            .Select(g => new
                            {
                                FaultID = g.Key.FaultID,
                                Resolution = g.Key.Resolution,
                                Note = string.Join(", ", g.Where(o => !string.IsNullOrWhiteSpace(o.Note))
                                    .Select(x => x.Note).Distinct()),
                                Number = g.Sum(x => x.NumberOfFault)
                            }).ToList();


                        if (itemFaults != null && itemFaults.Count > 0)
                        {
                            content = "Vi phạm: ";
                            for (int ik = 0, iksize = itemFaults.Count; ik < iksize; ik++)
                            {
                                var itemFaulst = itemFaults[ik];
                                if (itemFaulst.Number > 1)
                                {
                                    content += string.Format("{0}{1} ({2}), ", string.IsNullOrEmpty(itemFaulst.Resolution) ? string.Empty : itemFaulst.Resolution, string.IsNullOrEmpty(itemFaulst.Note) ? string.Empty : " " + itemFaulst.Note, itemFaulst.Number);
                                }
                                else
                                {
                                    content += string.Format("{0}{1}, ", string.IsNullOrEmpty(itemFaulst.Resolution) ? string.Empty : itemFaulst.Resolution, string.IsNullOrEmpty(itemFaulst.Note) ? string.Empty : " " + itemFaulst.Note);
                                }
                            }

                            // cat bo dau phay
                            content = content.Substring(0, content.Length - 2);
                        }

                        item.PupilID = pupilID;
                        item.SMSContent = content;
                        listPupilViolate.Add(item);
                    }

                    dic["VP"] = listPupilViolate;
                }
                #endregion

                #region Cap 2, 3 khong VNEN
                if ((grade == GlobalConstants.APPLIED_LEVEL_SECONDARY || grade == GlobalConstants.APPLIED_LEVEL_TERTIARY) && forVNEN == false)
                {
                    #region Diem
                    if (lstContentTypeCode.Contains("DIEM"))
                    {

                        //lay len diem chi tiet cac mon theo datetime
                        IDictionary<string, object> tmpDic = new Dictionary<string, object>();
                        tmpDic["SchoolID"] = schoolID;
                        tmpDic["ClassID"] = classID;
                        tmpDic["AcademicYearID"] = academicYearID;
                        tmpDic["Semester"] = curSemester;
                        //Lay len diem cua lop
                        IQueryable<MarkRecordWSBO> IQMarkRecord = MarkRecordBusiness.GetMardRecordOfAllSubjectInClass(tmpDic);
                        //Lay diem nhan xet cua lop
                        IQueryable<JudgeRecordWSBO> IQJudgeRecord = JudgeRecordBusiness.GetAllJudgeRecordOfClass(tmpDic);

                        //Loc thoi gian cham diem mon tinh diem
                        //Lay lai thoi gian bat dau va ket thuc cho chinh xac
                        DateTime fromDateMark = new DateTime(startTime.Year, startTime.Month, startTime.Day);
                        DateTime toDateMark = new DateTime(endTime.Year, endTime.Month, endTime.Day).AddDays(1);
                        bool isPrimary = false;
                        var IQFilterMarkRecordTmp = (from m in IQMarkRecord
                                                     where m.MarkedDate >= fromDateMark
                                                     && m.MarkedDate < toDateMark
                                                     select new
                                                     {
                                                         m.PupilID,
                                                         m.SubjectName,
                                                         m.SubjectOrderNumber,
                                                         m.MarkOrderNumber,
                                                         m.Mark,
                                                         m.Title,
                                                         m.MarkTypeID,
                                                         m.MarkedDate,
                                                         m.MarkredDatePrimary
                                                     }).ToList();

                        var IQFilterMarkRecord = (from m in IQFilterMarkRecordTmp
                                                  where (isPrimary && !m.Title.Contains("VGK") && !m.Title.Contains("ĐGK")
                                                                   && !m.Title.Contains("VCK") && !m.Title.Contains("ĐCK")
                                                                   && !m.Title.Contains("VCN") && !m.Title.Contains("ĐCN"))
                                                  || (isPrimary == false)
                                                  select new
                                                  {
                                                      m.PupilID,
                                                      m.SubjectName,
                                                      m.SubjectOrderNumber,
                                                      m.MarkOrderNumber,
                                                      Mark = m.Mark.HasValue ? m.Mark.Value.ToString("0.#") : string.Empty,
                                                      Title = !isPrimary && m.Title.Contains("M") ? "M" : !isPrimary && m.Title.Contains("P") ? "15P" : !isPrimary && m.Title.Contains("V")
                                                                ? "1T" : !isPrimary && m.Title.Contains("HK") ? "KTHK" : isPrimary && m.Title.Contains("ĐTX") && m.MarkredDatePrimary.HasValue
                                                                ? m.Title + (SetMonthWithPrimary(m.MarkredDatePrimary.Value.Month))
                                                                : isPrimary && m.Title.Contains("GK") ? "GK" : isPrimary && (m.Title.Contains("CK") || m.Title.Contains("CN")) ? "CK" : m.Title + " ",
                                                      m.MarkTypeID
                                                  }).ToList();

                        //Loc thoi gian cham diem mon nhan xet
                        var LsFilterJudgeRecordTmp = (from j in IQJudgeRecord
                                                      where j.MarkedDate >= fromDateMark
                                                      && j.MarkedDate < toDateMark
                                                      select new
                                                      {
                                                          j.PupilID,
                                                          j.SubjectID,
                                                          j.SubjectName,
                                                          j.SubjectOrderNumber,
                                                          j.MarkOrderNumber,
                                                          Judgement = j.Judgement == null ? string.Empty : j.Judgement,
                                                          j.MarkTypeID,
                                                          Title = j.Title,
                                                          EducationLevelID = j.EducationLevelID
                                                      }).ToList();

                        var LsFilterJudgeRecord = (from j in LsFilterJudgeRecordTmp
                                                   select new
                                                   {
                                                       j.PupilID,
                                                       j.SubjectID,
                                                       j.SubjectName,
                                                       j.SubjectOrderNumber,
                                                       j.MarkOrderNumber,
                                                       Judgement = j.Judgement == null ? string.Empty : j.Judgement,
                                                       j.MarkTypeID,
                                                       Title = j.Title.Contains("P") ? "15P" : j.Title.Contains("V") ? "1T" : j.Title.Contains("M") ? "M" : isPrimary ? "NX" + (curSemester == 2 ? (j.EducationLevelID > 2 ? j.MarkOrderNumber + 5 : j.MarkOrderNumber + 4) : j.MarkOrderNumber) : (!isPrimary && j.Title.Contains("HK") ? "KTHK" : j.Title),
                                                   }).ToList();
                        //Loc nhung hoc sinh co diem mon tin diem phat sinh
                        var IQMarkRecordGroup = (from p in IQFilterMarkRecord
                                                 orderby p.MarkTypeID, p.SubjectOrderNumber, p.SubjectName, p.MarkOrderNumber
                                                 group p by new
                                                 {
                                                     p.PupilID,
                                                     p.SubjectName,
                                                     p.SubjectOrderNumber,
                                                     p.Title
                                                 } into g
                                                 select new
                                                 {
                                                     PupilID = g.Key.PupilID.Value,
                                                     SubjectName = g.Key.SubjectName,
                                                     SubjectOrderNumber = g.Key.SubjectOrderNumber,
                                                     OrderPrimaryID = SetOrderPrimary(isPrimary, g.Key.Title),
                                                     MarkContent = (isPrimary && g.Key.Title.Contains("ĐTX")) ? (g.Key.Title.Replace("ĐTX", "T") + ": " + g.Select(x => x.Mark).Aggregate((a, b) => (a + " " + b)))
                                                            : isPrimary && (g.Key.Title.Contains("GK") || g.Key.Title.Contains("CK")) ? (g.Key.Title + ": " + g.Select(x => x.Mark).Aggregate((a, b) => (a + ", " + b))) : (g.Key.Title + ": " + g.Select(x => x.Mark).Aggregate((a, b) => (a + " " + b)))
                                                 }).ToList();
                        //Nhom diem cua cac mon tinh diem lai
                        var IQMarkRecordResponse = (from mg in IQMarkRecordGroup
                                                    orderby mg.SubjectOrderNumber, mg.SubjectName, mg.OrderPrimaryID
                                                    group mg
                                                    by new
                                                    {
                                                        mg.PupilID,
                                                        mg.SubjectName
                                                    }
                                                        into g
                                                    select new CustomSMSData
                                                    {
                                                        PupilID = g.Key.PupilID,
                                                        SMSContent = "-" + g.Key.SubjectName + "\n" + g.Select(x => x.MarkContent).Aggregate((a, b) => (a + "\n" + b)) + "\n"
                                                    }).ToList();


                        //Loc nhung hoc sinh co diem  mon nhan xet phat sinh
                        var IQMarkJudgeGroup = (from p in LsFilterJudgeRecord
                                                orderby p.MarkTypeID, p.MarkOrderNumber
                                                group p by new
                                                {
                                                    p.PupilID,
                                                    p.SubjectName,
                                                    p.SubjectID,
                                                    p.SubjectOrderNumber,
                                                    Title = p.Title
                                                } into g
                                                select new
                                                {
                                                    PupilID = g.Key.PupilID.Value,
                                                    SubjectName = g.Key.SubjectName,
                                                    SubjectID = g.Key.SubjectID,
                                                    SubjectOrderNumber = g.Key.SubjectOrderNumber,
                                                    JudgementContent = g.Key.Title
                                                        + ": " + g.Select(x => (x.Judgement.ToString())).Aggregate((a, b) => (a + " " + b))

                                                }).ToList();

                        //Nhom diem cua cac mon nhan xet lai
                        var IQMarkJudgeResponse = (from mg in IQMarkJudgeGroup
                                                   orderby mg.SubjectOrderNumber, mg.SubjectName
                                                   group mg
                                                    by new
                                                    {
                                                        mg.PupilID,
                                                        mg.SubjectName,
                                                        mg.SubjectID
                                                    }
                                                       into g
                                                   select new CustomSMSData
                                                   {
                                                       PupilID = g.Key.PupilID,
                                                       SMSContent = "-" + g.Key.SubjectName + "\n"
                                                                + g.Select(x => x.JudgementContent).Aggregate((a, b) => (a + "\n" + b)) + "\n"
                                                   }).ToList();
                        // loc hoc sinh trung nhau
                        IQMarkJudgeResponse = (from p in IQMarkJudgeResponse
                                               group p by new { p.PupilID }
                                                   into g
                                               select new CustomSMSData
                                               {
                                                   PupilID = g.Key.PupilID,
                                                   SMSContent = g.Where(p => p.PupilID == g.Key.PupilID).Select(p => p.SMSContent).Aggregate((a, b) => (a + b))
                                               }).ToList();

                        IQMarkRecordResponse = (from p in IQMarkRecordResponse
                                                group p by new { p.PupilID }
                                                    into g
                                                select new CustomSMSData
                                                {
                                                    PupilID = g.Key.PupilID,
                                                    SMSContent = g.Where(p => p.PupilID == g.Key.PupilID).Select(p => p.SMSContent).Aggregate((a, b) => (a + b))
                                                }).ToList();

                        // Bo sung them mon
                        foreach (var item in IQMarkJudgeResponse)
                        {
                            var t = IQMarkRecordResponse.FirstOrDefault(o => o.PupilID == item.PupilID);
                            if (t != null)
                            {
                                t.SMSContent += item.SMSContent;
                                t.SMSContent = t.SMSContent.Substring(0, t.SMSContent.Length - 1);
                            }
                            else
                            {
                                IQMarkRecordResponse.Add(item);
                            }
                        }

                        dic["DIEM"] = IQMarkRecordResponse;
                    }


                    #endregion

                    #region Trung binh mon
                    if (lstContentTypeCode.Contains("TBM"))
                    {
                        List<ClassSubjectBO> listClassSubject = ClassSubjectBusiness.SearchByClass(classID, new Dictionary<string, object>() { { "Semester", bigSemester } }).Select(o => new ClassSubjectBO
                        {
                            SubjectID = o.SubjectID,
                            DisplayName = o.SubjectCat.DisplayName,
                            IsCommenting = o.IsCommenting,
                            OrderInSubject = o.SubjectCat.OrderInSubject
                        }).OrderBy(o => o.IsCommenting).ThenBy(o => o.OrderInSubject).ThenBy(o => o.DisplayName).ToList();

                        //lay diem chi tiet trung binh cua cac mon
                        List<SummedUpRecordBO> listSummedUpRecord = SummedUpRecordBusiness.GetSummedUpRecord(academicYearID, schoolID, bigSemester, classID, null).ToList();
                        //lay diem trung binh cua tat ca cac mon theo dot + lay hoc luc, hanh kiem, xep hang theo dot
                        List<PupilOfClassRanking> listPupilRanking = PupilRankingBusiness.GetPupilRankingOfClass(academicYearID, schoolID, bigSemester, classID, null).ToList();

                        List<CustomSMSData> lstMarkTBM = new List<CustomSMSData>();

                        List<SummedUpRecordBO> listPupilSum = new List<SummedUpRecordBO>();
                        //Object mapping vao list
                        CustomSMSData objMarkOfSemester = new CustomSMSData();
                        //object mapping thong tin diem trung binh mon (dung de tao content)
                        SummedUpRecordBO objSummedUpRecordBO = new SummedUpRecordBO();

                        foreach (PupilOfClassRanking pupilRank in listPupilRanking)
                        {
                            int pupilID = pupilRank.PupilID;
                            // Khoi tao item
                            objMarkOfSemester = new CustomSMSData();
                            // Lay len thong tin diem cua hoc sinh
                            listPupilSum = listSummedUpRecord.Where(o => o.PupilID == pupilID).ToList();

                            // Lay thong tin hoc luc hanh kiem
                            string pupilInfo = "";
                            int countSubject = listClassSubject.Count;

                            // Thong diem diem theo mon
                            foreach (ClassSubjectBO cs in listClassSubject)
                            {
                                objSummedUpRecordBO = listPupilSum.Find(o => o.SubjectID == cs.SubjectID);
                                if (objSummedUpRecordBO != null)
                                {
                                    if (!objSummedUpRecordBO.SummedUpMark.HasValue && string.IsNullOrEmpty(objSummedUpRecordBO.JudgementResult))
                                    {
                                        continue;
                                    }
                                    pupilInfo += cs.DisplayName + ": " + (cs.IsCommenting.Value == SystemParamsInFile.ISCOMMENTING_TYPE_JUDGE ? objSummedUpRecordBO.JudgementResult + "\n" : (objSummedUpRecordBO.SummedUpMark.HasValue ? (grade > 1 ? objSummedUpRecordBO.SummedUpMark.Value.ToString("0.0") : objSummedUpRecordBO.SummedUpMark.Value.ToString("0.#")) : string.Empty)) + (grade > 1 && objSummedUpRecordBO.IsCommenting == SystemParamsInFile.ISCOMMENTING_TYPE_MARK ? "\n" : "");

                                }
                            }
                            if (pupilInfo.EndsWith("\n"))
                            {
                                pupilInfo = pupilInfo.Substring(0, pupilInfo.Length - 1);
                            }

                            // gan thong tin chi tiet object de add
                            objMarkOfSemester.SMSContent = pupilInfo;
                            objMarkOfSemester.PupilID = pupilID;
                            // Gan vao list de tra ve
                            lstMarkTBM.Add(objMarkOfSemester);
                        }

                        dic["TBM"] = lstMarkTBM;
                    }
                    #endregion

                    #region Trung binh cac mon, hoc luc, hanh kiem, danh hieu thi dua
                    if (lstContentTypeCode.Contains("TBCM") || lstContentTypeCode.Contains("HL") || lstContentTypeCode.Contains("HK") || lstContentTypeCode.Contains("XH") || lstContentTypeCode.Contains("DHTD"))
                    {
                        //lay diem trung binh cua tat ca cac mon theo dot + lay hoc luc, hanh kiem, xep hang theo dot
                        List<PupilOfClassRanking> listPupilRanking = PupilRankingBusiness.GetPupilRankingOfClass(academicYearID, schoolID, bigSemester, classID, null).ToList();

                        //lay danh hieu thi dua
                        List<PupilEmulationBO> listPupilEmulation = PupilEmulationBusiness.SearchBySchool(schoolID, new Dictionary<string, object>() {
                            { "AcademicYearID", academicYearID } ,
                            { "ClassID", classID },
                            { "Semester", bigSemester }
                            }).Select(o => new PupilEmulationBO
                            {
                                PupilID = o.PupilID,
                                HonourAchivementTypeResolution = o.HonourAchivementType.Resolution
                            }).ToList();

                        List<CustomSMSData> listTBCM = new List<CustomSMSData>();
                        List<CustomSMSData> listHL = new List<CustomSMSData>();
                        List<CustomSMSData> listHK = new List<CustomSMSData>();
                        List<CustomSMSData> listXH = new List<CustomSMSData>();
                        List<CustomSMSData> listDHTH = new List<CustomSMSData>();

                        //Object mapping vao list
                        CustomSMSData objTBCM = new CustomSMSData();
                        CustomSMSData objHL = new CustomSMSData();
                        CustomSMSData objHK = new CustomSMSData();
                        CustomSMSData objXH = new CustomSMSData();
                        CustomSMSData objDHTD = new CustomSMSData();

                        // Thong tin khen thuong
                        PupilEmulationBO pupilEmulationBO = new PupilEmulationBO();

                        foreach (PupilOfClassRanking pupilRank in listPupilRanking)
                        {
                            int pupilID = pupilRank.PupilID;

                            // Lay len thon tin khen thuong
                            pupilEmulationBO = listPupilEmulation.Find(o => o.PupilID == pupilID);

                            // Khoi tao item
                            objTBCM = new CustomSMSData();
                            objHL = new CustomSMSData();
                            objHK = new CustomSMSData();
                            objXH = new CustomSMSData();
                            objDHTD = new CustomSMSData();

                            //TBM
                            string pupilInfo = string.Empty;

                            if (pupilRank.AverageMark.HasValue && pupilRank.AverageMark.Value > 0)
                            {
                                pupilInfo += "\nTrung bình các môn: " + pupilRank.AverageMark.Value.ToString("0.0");
                            }

                            // gan thong tin chi tiet object de add
                            objTBCM.SMSContent = pupilInfo;
                            objTBCM.PupilID = pupilID;
                            // Gan vao list de tra ve
                            listTBCM.Add(objTBCM);

                            //Hoc luc
                            pupilInfo = string.Empty;

                            if (!string.IsNullOrEmpty(pupilRank.CapacityLevel))
                            {
                                pupilInfo += "\nHọc lực: " + pupilRank.CapacityLevel;
                            }

                            // gan thong tin chi tiet object de add
                            objHL.SMSContent = pupilInfo;
                            objHL.PupilID = pupilID;
                            // Gan vao list de tra ve
                            listHL.Add(objHL);

                            //Hanh kiem
                            pupilInfo = string.Empty;

                            if (!string.IsNullOrEmpty(pupilRank.ConductLevel))
                            {
                                pupilInfo += "\nHạnh kiểm: " + pupilRank.ConductLevel;
                            }

                            // gan thong tin chi tiet object de add
                            objHK.SMSContent = pupilInfo;
                            objHK.PupilID = pupilID;
                            // Gan vao list de tra ve
                            listHK.Add(objHK);

                            //Xep hang
                            pupilInfo = string.Empty;

                            if (pupilRank.Rank.HasValue && pupilRank.Rank.Value > 0)
                            {
                                pupilInfo += "\nXếp hạng: " + pupilRank.Rank.ToString();
                            }

                            // gan thong tin chi tiet object de add
                            objXH.SMSContent = pupilInfo;
                            objXH.PupilID = pupilID;
                            // Gan vao list de tra ve
                            listXH.Add(objXH);

                            //Danh hieu thi dua
                            pupilInfo = string.Empty;

                            if (pupilEmulationBO != null && !string.IsNullOrEmpty(pupilEmulationBO.HonourAchivementTypeResolution))
                            {
                                pupilInfo += "\nDanh hiệu thi đua: " + pupilEmulationBO.HonourAchivementTypeResolution;
                            }

                            // gan thong tin chi tiet object de add
                            objDHTD.SMSContent = pupilInfo;
                            objDHTD.PupilID = pupilID;
                            // Gan vao list de tra ve
                            listDHTH.Add(objDHTD);

                        }

                        dic["TBCM"] = listTBCM;
                        dic["HL"] = listHL;
                        dic["HK"] = listHK;
                        dic["XH"] = listXH;
                        dic["DHTD"] = listDHTH;
                    }
                    #endregion
                }
                #endregion

                #region Cap 2 VNEN
                if (grade == GlobalConstants.APPLIED_LEVEL_SECONDARY && forVNEN == true)
                {
                    AcademicYear academicYear = AcademicYearBusiness.Find(academicYearID);
                    int partitionId = UtilsBusiness.GetPartionId(schoolID);
                    int semesterVNEN = bigSemester == 3 ? 2 : bigSemester;

                    List<CustomSMSData> listNXT = new List<CustomSMSData>();

                    CustomSMSData obj = null;
                    PupilOfClassBO pupilOfClassBO = null;
                    int pupilID = 0;
                    StringBuilder monthComments = null, pupilContent = null;
                    string MonthStr = string.Empty;

                    //nhan xet thang

                    MonthStr = string.Format("{0}{1}", year, month);

                    int v_monthID = 0;
                    if (Int32.TryParse(MonthStr, out v_monthID))
                    {
                        List<TeacherNoteBookMonthBO> listNotesMonth = (from note in TeacherNoteBookMonthBusiness.All
                                                                       join sc in SubjectCatBusiness.All on note.SubjectID equals sc.SubjectCatID
                                                                       where note.SchoolID == schoolID && note.PartitionID == partitionId
                                                                           && note.AcademicYearID == academicYearID && note.ClassID == classID
                                                                           && note.MonthID == v_monthID
                                                                            && pupilIDList.Contains(note.PupilID)
                                                                       orderby sc.OrderInSubject
                                                                       select new TeacherNoteBookMonthBO
                                                                       {
                                                                           PupilID = note.PupilID,
                                                                           SubjectID = note.SubjectID,
                                                                           SubjectName = sc.SubjectName,
                                                                           MonthID = note.MonthID,
                                                                           CommentCQ = note.CommentCQ,
                                                                           CommentSubject = note.CommentSubject
                                                                       }).ToList();
                        TeacherNoteBookMonthBO noteSubjectMonthBO = null;
                        for (int i = 0; i < lstPupilOfClass.Count; i++)
                        {
                            pupilOfClassBO = lstPupilOfClass[i];
                            pupilID = pupilOfClassBO.PupilID;
                            obj = new CustomSMSData();
                            obj.PupilID = pupilID;
                            monthComments = new StringBuilder();

                            List<TeacherNoteBookMonthBO> listCommentsBypupilID = listNotesMonth.Where(p => p.PupilID == pupilID).ToList();
                            for (int j = 0; j < listCommentsBypupilID.Count; j++)
                            {
                                noteSubjectMonthBO = listCommentsBypupilID[j];
                                if (!string.IsNullOrEmpty(noteSubjectMonthBO.CommentSubject) || !string.IsNullOrEmpty(noteSubjectMonthBO.CommentCQ))
                                {
                                    monthComments.Append(j + 1).Append("/").Append(noteSubjectMonthBO.SubjectName).Append(":\r\n");
                                    if (!string.IsNullOrEmpty(noteSubjectMonthBO.CommentSubject)) monthComments.AppendLine(noteSubjectMonthBO.CommentSubject);
                                    if (!string.IsNullOrEmpty(noteSubjectMonthBO.CommentCQ)) monthComments.AppendLine(noteSubjectMonthBO.CommentCQ);
                                }

                            }
                            if (monthComments != null && monthComments.Length > 0)
                            {
                                obj.SMSContent = monthComments.ToString();
                            }
                            else
                            {
                                obj.SMSContent = string.Empty;
                            }
                            listNXT.Add(obj);
                        }

                        dic["NXT"] = listNXT;

                    }

                    //Danh gia mon hoc
                    List<SubjectCatBO> lsClassSubject = new List<SubjectCatBO>();

                    lsClassSubject = (from sb in ClassSubjectBusiness.All
                                      join sub in SubjectCatBusiness.All on sb.SubjectID equals sub.SubjectCatID
                                      orderby sb.IsSubjectVNEN descending, sb.IsCommenting, sub.OrderInSubject
                                      where sb.ClassID == classID
                                      && sub.IsActive == true
                                      && ((semesterVNEN == GlobalConstants.SEMESTER_OF_YEAR_FIRST && sb.SectionPerWeekFirstSemester > 0)
                                      || (semesterVNEN == GlobalConstants.SEMESTER_OF_YEAR_SECOND && sb.SectionPerWeekSecondSemester > 0))
                                      select new SubjectCatBO
                                      {
                                          SubjectCatID = sb.SubjectID,
                                          SubjectName = sub.SubjectName,
                                          IsCommenting = sb.IsCommenting,
                                          IsSubjectVNEN = sb.IsSubjectVNEN
                                      }).ToList();

                    // Danh sach danh gia So tay GV
                    List<TeacherNoteBookSemester> listNotesSem = (from tbs in TeacherNoteBookSemesterBusiness.All
                                                                  where tbs.SchoolID == schoolID && tbs.PartitionID == partitionId
                                                                   && tbs.AcademicYearID == academicYearID
                                                                   && tbs.ClassID == classID
                                                                   && tbs.SemesterID == semesterVNEN
                                                                   && pupilIDList.Contains(tbs.PupilID)
                                                                  select tbs).ToList();
                    // Thong tin theo TT58
                    List<SummedUpRecord> listSummedUpRecord = (from su in SummedUpRecordBusiness.All
                                                               where su.SchoolID == schoolID && su.Last2digitNumberSchool == partitionId
                                                                && su.AcademicYearID == academicYearID
                                                                && su.ClassID == classID
                                                                && ((semesterVNEN == GlobalConstants.SEMESTER_OF_YEAR_SECOND && su.Semester == GlobalConstants.SEMESTER_OF_YEAR_ALL)
                                                                 || (semesterVNEN == GlobalConstants.SEMESTER_OF_YEAR_FIRST && su.Semester == semesterVNEN))
                                                                && su.PeriodID == null
                                                                && pupilIDList.Contains(su.PupilID)
                                                               select su).ToList();
                    // Danh sach danh gia So tay HS
                    List<ReviewBookPupil> listReviewPupil = (from rv in ReviewBookPupilBusiness.All
                                                             where rv.SchoolID == schoolID && rv.PartitionID == partitionId
                                                                && rv.AcademicYearID == academicYearID && rv.ClassID == classID
                                                                && rv.SemesterID == semesterVNEN
                                                             && pupilIDList.Contains(rv.PupilID)
                                                             select rv).ToList();
                    // Danh sach khen thuong
                    List<UpdateReward> listRewardComments = (from ur in UpdateRewardBusiness.All
                                                             where ur.SchoolID == schoolID && ur.PartitionID == partitionId
                                                                        && ur.AcademicYearID == academicYearID
                                                                        && ur.ClassID == classID
                                                                        && ur.SemesterID == semesterVNEN
                                                                        && pupilIDList.Contains(ur.PupilID)
                                                             select ur).ToList();


                    List<TeacherNoteBookSemester> lstNotesPerPupil = null;
                    TeacherNoteBookSemester objNotesPupil = null;
                    SubjectCatBO objSubjectBO = null;
                    SummedUpRecord objSummedUpRecord = null;
                    ReviewBookPupil objReviewPupil = null;

                    List<CustomSMSData> lstDGMH = new List<CustomSMSData>();
                    List<CustomSMSData> lstNL = new List<CustomSMSData>();
                    List<CustomSMSData> lstPC = new List<CustomSMSData>();
                    List<CustomSMSData> lstKT = new List<CustomSMSData>();
                    List<CustomSMSData> lstDGCN = new List<CustomSMSData>();
                    List<CustomSMSData> lstNXCK = new List<CustomSMSData>();

                    for (int i = 0; i < lstPupilOfClass.Count; i++)
                    {
                        pupilOfClassBO = lstPupilOfClass[i];
                        pupilID = pupilOfClassBO.PupilID;
                        obj = new CustomSMSData();
                        obj.PupilID = pupilID;
                        monthComments = new StringBuilder();
                        // Danh gia mon hoc
                        lstNotesPerPupil = listNotesSem.Where(p => p.PupilID == pupilID).ToList();
                        for (int j = 0; j < lsClassSubject.Count; j++)
                        {
                            objSubjectBO = lsClassSubject[j];
                            pupilContent = new StringBuilder();

                            if (objSubjectBO.IsSubjectVNEN == true)
                            {
                                objNotesPupil = lstNotesPerPupil.FirstOrDefault(o => o.SubjectID == objSubjectBO.SubjectCatID);
                                if (objNotesPupil != null)
                                {
                                    if (objNotesPupil.Rate == GlobalConstants.REVIEW_TYPE_FINISH) pupilContent.Append(GlobalConstants.SHORTFINISH).Append(", ");
                                    else if (objNotesPupil.Rate == GlobalConstants.REVIEW_TYPE_NOT_FINISH) pupilContent.Append(GlobalConstants.SHORTNOTFINISH).Append(", ");

                                    if (objSubjectBO.IsCommenting == GlobalConstants.SUBJECTCAT_ISCOMMENTING_MARK && objNotesPupil.PERIODIC_SCORE_END.HasValue) pupilContent.Append("KTCK ").Append(objNotesPupil.PERIODIC_SCORE_END.Value.ToString("#.#")).Append("; ");
                                    else if (objSubjectBO.IsCommenting == GlobalConstants.SUBJECTCAT_ISCOMMENTING_COMMENT && !String.IsNullOrEmpty(objNotesPupil.PERIODIC_SCORE_END_JUDGLE)) pupilContent.Append("KTCK ").Append(objNotesPupil.PERIODIC_SCORE_END_JUDGLE).Append("; ");
                                }
                            }
                            else
                            {
                                objSummedUpRecord = listSummedUpRecord.FirstOrDefault(o => o.PupilID == pupilID && o.SubjectID == objSubjectBO.SubjectCatID);
                                if (objSummedUpRecord != null)
                                {
                                    if (objSubjectBO.IsCommenting == GlobalConstants.SUBJECTCAT_ISCOMMENTING_MARK && objSummedUpRecord.SummedUpMark.HasValue) pupilContent.Append(objSummedUpRecord.SummedUpMark.Value.ToString("#.#")).Append("; ");
                                    else if (objSubjectBO.IsCommenting == GlobalConstants.SUBJECTCAT_ISCOMMENTING_COMMENT && !String.IsNullOrEmpty(objSummedUpRecord.JudgementResult)) pupilContent.Append(objSummedUpRecord.JudgementResult).Append("; ");
                                }
                            }

                            if (pupilContent.Length > 0) monthComments.Append(objSubjectBO.SubjectName).Append(": ").Append(pupilContent.ToString());
                        }

                        obj.SMSContent = monthComments.ToString();
                        lstDGMH.Add(obj);

                        // Nang luc
                        obj = new CustomSMSData();
                        obj.PupilID = pupilID;
                        monthComments = new StringBuilder();

                        objReviewPupil = listReviewPupil.FirstOrDefault(o => o.PupilID == pupilID);
                        if (objReviewPupil != null)
                        {
                            if (objReviewPupil.CapacityRate == GlobalConstants.REVIEW_TYPE_FINISH) monthComments.AppendLine("Năng lực: Đ");
                            else if (objReviewPupil.CapacityRate == GlobalConstants.REVIEW_TYPE_NOT_FINISH) monthComments.AppendLine("Năng lực: CĐ");
                        }

                        obj.SMSContent = monthComments.ToString();
                        lstNL.Add(obj);

                        // Pham chat
                        obj = new CustomSMSData();
                        obj.PupilID = pupilID;
                        monthComments = new StringBuilder();

                        if (objReviewPupil != null)
                        {
                            if (objReviewPupil.QualityRate == GlobalConstants.REVIEW_TYPE_FINISH) monthComments.AppendLine("Phẩm chất: Đ");
                            else if (objReviewPupil.QualityRate == GlobalConstants.REVIEW_TYPE_NOT_FINISH) monthComments.AppendLine("Phẩm chất: CĐ");
                        }

                        obj.SMSContent = monthComments.ToString();
                        lstPC.Add(obj);

                        // Khen thuong
                        obj = new CustomSMSData();
                        obj.PupilID = pupilID;
                        monthComments = new StringBuilder();
                        string strRewardID = listRewardComments.Where(p => p.PupilID == pupilID).Select(p => p.Rewards).FirstOrDefault();
                        if (!string.IsNullOrEmpty(strRewardID))
                        {
                            List<int> listInt = strRewardID.Split(new[] { "," }, StringSplitOptions.RemoveEmptyEntries).Select(p => int.Parse(p)).ToList();
                            List<RewardFinal> rewardFinalList = RewardFinalBusiness.All.Where(p => p.SchoolID == schoolID).ToList();
                            string rewardMode = string.Empty;
                            pupilContent = new StringBuilder();
                            for (int g = 0; g < listInt.Count; g++)
                            {
                                rewardMode = rewardFinalList.Where(p => p.RewardFinalID == listInt[g]).Select(p => p.RewardMode).FirstOrDefault();
                                if (!string.IsNullOrWhiteSpace(rewardMode))
                                {
                                    pupilContent.Append(rewardMode);
                                    if (g != listInt.Count - 1)
                                    {
                                        pupilContent.Append("; ");
                                    }
                                }
                            }
                            if (pupilContent.Length > 0) monthComments.Append("Khen thưởng: ").AppendLine(pupilContent.ToString());
                        }

                        obj.SMSContent = monthComments.ToString();
                        lstKT.Add(obj);

                        // Danh gia cuoi nam
                        obj = new CustomSMSData();
                        obj.PupilID = pupilID;
                        monthComments = new StringBuilder();
                        if (semesterVNEN == GlobalConstants.SEMESTER_OF_YEAR_SECOND)
                        {
                            if (objReviewPupil != null)
                            {
                                if (objReviewPupil.RateAndYear == GlobalConstants.REVIEW_TYPE_FINISH)
                                {
                                    monthComments.AppendLine("Được lên lớp");
                                }
                                else if (objReviewPupil.RateAndYear == GlobalConstants.REVIEW_TYPE_NOT_FINISH)
                                {
                                    if (!objReviewPupil.RateAdd.HasValue) monthComments.AppendLine("Chưa hoàn thành chương trình lớp học");
                                    else if (objReviewPupil.RateAdd == GlobalConstants.REVIEW_TYPE_FINISH) monthComments.AppendLine("Được lên lớp");
                                    else if (objReviewPupil.RateAdd == GlobalConstants.REVIEW_TYPE_NOT_FINISH) monthComments.AppendLine("Ở lại lớp");
                                }
                            }
                        }

                        obj.SMSContent = monthComments.ToString();
                        lstDGCN.Add(obj);

                        //NXCK
                        monthComments = new StringBuilder();
                        pupilID = pupilOfClassBO.PupilID;
                        obj = new CustomSMSData();
                        obj.PupilID = pupilID;

                        // lay thong tin nhan xet (So tay hoc sinh)
                        if (objReviewPupil != null)
                        {
                            if (!string.IsNullOrEmpty(objReviewPupil.CQComment)) monthComments.AppendLine(string.Format("Biểu hiện nổi bật: {0}", objReviewPupil.CQComment));
                            if (!string.IsNullOrEmpty(objReviewPupil.CommentAdd)) monthComments.AppendLine(string.Format("Điều cần khắc phục giúp đỡ, rèn luyện thêm: {0}", objReviewPupil.CommentAdd));
                        }


                        if (monthComments != null && monthComments.Length > 0)
                        {
                            obj.SMSContent = monthComments.ToString();
                        }
                        lstNXCK.Add(obj);
                    }

                    dic["DGMH"] = lstDGMH;
                    dic["DGCN"] = lstDGCN;
                    dic["NL"] = lstNL;
                    dic["PC"] = lstPC;
                    dic["KT"] = lstKT;
                    dic["NXCK"] = lstNXCK;


                }
                #endregion

                #region Cap 1
                else
                {
                    IDictionary<string, object> tmpDic = new Dictionary<string, object>();
                    //list mon hoc
                    List<SubjectCatBO> lstClassSubject = new List<SubjectCatBO>();
                    if (classID > 0)
                    {
                        lstClassSubject = (from sb in ClassSubjectBusiness.All
                                           join sub in SubjectCatBusiness.All on sb.SubjectID equals sub.SubjectCatID
                                           orderby sb.IsCommenting, sub.OrderInSubject
                                           where sb.ClassID == classID
                                           && sub.IsActive == true
                                           && ((bigSemester == GlobalConstants.SEMESTER_OF_YEAR_FIRST && sb.SectionPerWeekFirstSemester > 0)
                                           || (bigSemester == GlobalConstants.SEMESTER_OF_YEAR_SECOND && sb.SectionPerWeekSecondSemester > 0))
                                           select new SubjectCatBO
                                           {
                                               SubjectCatID = sb.SubjectID,
                                               SubjectName = sub.SubjectName,
                                               IsCommenting = sb.IsCommenting
                                           }
                                         ).ToList();
                    }

                    //Khen thuong 
                    tmpDic = new Dictionary<string, object>()
                    {
                        {"SchoolID",schoolID},
                        {"AcademicYearID",academicYearID},
                        {"lstClassID",new List<int>{classID}},
                        {"Semester", GlobalConstants.SEMESTER_OF_EVALUATION_RESULT}
                    };
                    List<SummedEndingEvaluationBO> lstSEE = SummedEndingEvaluationBusiness.Search(tmpDic).ToList();

                    tmpDic = new Dictionary<string, object>()
                    {
                        {"SchoolID",schoolID},
                        {"AcademicYearID",academicYearID},
                        {"lstClassID",new List<int>{classID}},
                        {"SemesterID", bigSemester},
                        {"lstPupilID", pupilIDList.ConvertAll(i => (int)i)},

                    };

                    List<EvaluationReward> lstEvaluationReward = EvaluationRewardBusiness.Search(tmpDic).ToList();

                    //List loai nang luc va pham chat
                    List<EvaluationCriteria> lstEc = EvaluationCriteriaBusiness.All.ToList();
                    List<EvaluationCriteria> lstEcNL = lstEc.Where(o => o.TypeID == 2).OrderBy(o => o.EvaluationCriteriaID).ToList();
                    List<EvaluationCriteria> lstEcPC = lstEc.Where(o => o.TypeID == 3).OrderBy(o => o.EvaluationCriteriaID).ToList();

                    tmpDic = new Dictionary<string, object>();
                    tmpDic["SchoolID"] = schoolID;
                    tmpDic["AcademicYearID"] = academicYearID;
                    tmpDic["ClassID"] = classID;
                    tmpDic["SemesterID"] = bigSemester;

                    AcademicYear objAy = AcademicYearBusiness.Find(academicYearID);

                    List<RatedCommentPupilBO> lstRatedCommentAll;
                    if (UtilsBusiness.IsMoveHistory(objAy))
                    {
                        lstRatedCommentAll = RatedCommentPupilHistoryBusiness.Search(tmpDic)
                            .Select(o => new RatedCommentPupilBO
                            {
                                CapacityEndingEvaluation = o.CapacityEndingEvaluation,
                                CapacityMiddleEvaluation = o.CapacityMiddleEvaluation,
                                Comment = o.Comment,
                                EndingEvaluation = o.EndingEvaluation,
                                EvaluationID = o.EvaluationID,
                                MiddleEvaluation = o.MiddleEvaluation,
                                PeriodicEndingJudgement = o.PeriodicEndingJudgement,
                                PeriodicEndingMark = o.PeriodicEndingMark,
                                PeriodicMiddleJudgement = o.PeriodicMiddleJudgement,
                                PeriodicMiddleMark = o.PeriodicMiddleMark,
                                PupilID = o.PupilID,
                                QualityEndingEvaluation = o.QualityEndingEvaluation,
                                QualityMiddleEvaluation = o.QualityMiddleEvaluation,
                                RetestMark = o.RetestMark,
                                SemesterID = o.SemesterID,
                                SubjectID = o.SubjectID

                            }).ToList();
                    }
                    else
                    {
                        lstRatedCommentAll = RatedCommentPupilBusiness.Search(tmpDic)
                            .Select(o => new RatedCommentPupilBO
                            {
                                CapacityEndingEvaluation = o.CapacityEndingEvaluation,
                                CapacityMiddleEvaluation = o.CapacityMiddleEvaluation,
                                Comment = o.Comment,
                                EndingEvaluation = o.EndingEvaluation,
                                EvaluationID = o.EvaluationID,
                                MiddleEvaluation = o.MiddleEvaluation,
                                PeriodicEndingJudgement = o.PeriodicEndingJudgement,
                                PeriodicEndingMark = o.PeriodicEndingMark,
                                PeriodicMiddleJudgement = o.PeriodicMiddleJudgement,
                                PeriodicMiddleMark = o.PeriodicMiddleMark,
                                PupilID = o.PupilID,
                                QualityEndingEvaluation = o.QualityEndingEvaluation,
                                QualityMiddleEvaluation = o.QualityMiddleEvaluation,
                                RetestMark = o.RetestMark,
                                SemesterID = o.SemesterID,
                                SubjectID = o.SubjectID

                            }).ToList();
                    }

                    List<CustomSMSData> lstMHHDGD = new List<CustomSMSData>();
                    List<CustomSMSData> lstNL = new List<CustomSMSData>();
                    List<CustomSMSData> lstPC = new List<CustomSMSData>();
                    List<CustomSMSData> lstKT = new List<CustomSMSData>();
                    List<CustomSMSData> lstKQNH = new List<CustomSMSData>();
                    CustomSMSData obj;

                    int type = semester == 3 || semester == 4 ? 1 : 2;
                    for (int i = 0; i < lstPupilOfClass.Count; i++)
                    {
                        PupilOfClassBO poc = lstPupilOfClass[i];

                        List<RatedCommentPupilBO> lstRatedCommentPupil = lstRatedCommentAll.Where(o => o.PupilID == poc.PupilID).ToList();


                        string tempNL;
                        string tempPC;
                        string tempKT;
                        //Tong hop ket qua
                        StringBuilder sb = new StringBuilder();
                        switch (type)
                        {
                            //Ket qua giua ky
                            case 1:
                                //Mon hoc & HDGD
                                obj = new CustomSMSData();
                                obj.PupilID = poc.PupilID;
                                sb = new StringBuilder();
                                for (int j = 0; j < lstClassSubject.Count; j++)
                                {
                                    SubjectCatBO subject = lstClassSubject[j];

                                    RatedCommentPupilBO ratedCommentSubject = lstRatedCommentPupil
                                        .Where(o => o.EvaluationID == 1 && o.SubjectID == subject.SubjectCatID).FirstOrDefault();

                                    if (ratedCommentSubject != null)
                                    {
                                        string evaluation = ratedCommentSubject.MiddleEvaluation == 1 ? "T"
                                            : (ratedCommentSubject.MiddleEvaluation == 2 ? "H" :
                                            (ratedCommentSubject.MiddleEvaluation == 3 ? "C" : string.Empty));

                                        string mark;

                                        if (subject.IsCommenting.HasValue && subject.IsCommenting == 1)
                                        {
                                            mark = ratedCommentSubject.PeriodicMiddleJudgement;
                                        }
                                        else
                                        {
                                            mark = ratedCommentSubject.PeriodicMiddleMark.HasValue ?
                                                ratedCommentSubject.PeriodicMiddleMark.Value.ToString() : string.Empty;
                                        }

                                        if (!string.IsNullOrEmpty(evaluation) || !string.IsNullOrEmpty(mark))
                                        {
                                            sb.Append(subject.SubjectName);
                                            sb.Append(": ");
                                            if (!string.IsNullOrEmpty(evaluation))
                                            {
                                                sb.Append(evaluation);
                                            }

                                            if (!string.IsNullOrEmpty(evaluation) && !string.IsNullOrEmpty(mark))
                                            {
                                                sb.Append(",");
                                            }

                                            if (!string.IsNullOrEmpty(mark))
                                            {
                                                sb.Append(mark);
                                            }

                                            sb.Append(";");
                                        }
                                    }
                                }

                                obj.SMSContent = sb.ToString();
                                lstMHHDGD.Add(obj);

                                //Nang luc
                                obj = new CustomSMSData();
                                obj.PupilID = poc.PupilID;
                                sb = new StringBuilder();

                                tempNL = string.Empty;
                                for (int j = 0; j < lstEcNL.Count; j++)
                                {
                                    EvaluationCriteria ec = lstEcNL[j];

                                    RatedCommentPupilBO ratedCommentSubject = lstRatedCommentPupil
                                        .Where(o => o.EvaluationID == 2 && o.SubjectID == ec.EvaluationCriteriaID).FirstOrDefault();

                                    if (ratedCommentSubject != null)
                                    {
                                        string evaluation = ratedCommentSubject.CapacityMiddleEvaluation == 1 ? "T"
                                            : (ratedCommentSubject.CapacityMiddleEvaluation == 2 ? "Đ" :
                                            (ratedCommentSubject.CapacityMiddleEvaluation == 3 ? "C" : string.Empty));

                                        if (!string.IsNullOrEmpty(evaluation))
                                        {
                                            tempNL += evaluation;
                                            tempNL += ";";
                                        }
                                    }
                                }
                                if (!string.IsNullOrWhiteSpace(tempNL))
                                {
                                    sb.Append("Năng lực: ");
                                    sb.Append(tempNL);
                                }

                                obj.SMSContent = sb.ToString();
                                lstNL.Add(obj);

                                //Pham chat
                                obj = new CustomSMSData();
                                obj.PupilID = poc.PupilID;
                                sb = new StringBuilder();

                                tempPC = string.Empty;
                                for (int j = 0; j < lstEcPC.Count; j++)
                                {
                                    EvaluationCriteria ec = lstEcPC[j];

                                    RatedCommentPupilBO ratedCommentSubject = lstRatedCommentPupil
                                        .Where(o => o.EvaluationID == 3 && o.SubjectID == ec.EvaluationCriteriaID).FirstOrDefault();

                                    if (ratedCommentSubject != null)
                                    {
                                        string evaluation = ratedCommentSubject.QualityMiddleEvaluation == 1 ? "T"
                                            : (ratedCommentSubject.QualityMiddleEvaluation == 2 ? "Đ" :
                                            (ratedCommentSubject.QualityMiddleEvaluation == 3 ? "C" : string.Empty));

                                        if (!string.IsNullOrEmpty(evaluation))
                                        {
                                            tempPC += evaluation;
                                            tempPC += ";";
                                        }
                                    }
                                }

                                if (!string.IsNullOrWhiteSpace(tempPC))
                                {
                                    sb.Append("Phẩm chất: ");
                                    sb.Append(tempPC);
                                }

                                obj.SMSContent = sb.ToString();
                                lstPC.Add(obj);
                                break;
                            //Ket qua cuoi ky
                            case 2:
                                //Mon hoc & HDGD
                                obj = new CustomSMSData();
                                obj.PupilID = poc.PupilID;
                                sb = new StringBuilder();

                                for (int j = 0; j < lstClassSubject.Count; j++)
                                {
                                    SubjectCatBO subject = lstClassSubject[j];

                                    RatedCommentPupilBO ratedCommentSubject = lstRatedCommentPupil
                                        .Where(o => o.EvaluationID == 1 && o.SubjectID == subject.SubjectCatID).FirstOrDefault();

                                    if (ratedCommentSubject != null)
                                    {
                                        string evaluation = ratedCommentSubject.EndingEvaluation == 1 ? "T"
                                            : (ratedCommentSubject.EndingEvaluation == 2 ? "H" :
                                            (ratedCommentSubject.EndingEvaluation == 3 ? "C" : string.Empty));

                                        string mark;

                                        if (subject.IsCommenting.HasValue && subject.IsCommenting == 1)
                                        {
                                            mark = ratedCommentSubject.PeriodicEndingJudgement;
                                        }
                                        else
                                        {
                                            mark = ratedCommentSubject.PeriodicEndingMark.HasValue ?
                                                ratedCommentSubject.PeriodicEndingMark.Value.ToString() : string.Empty;
                                        }

                                        if (!string.IsNullOrEmpty(evaluation) || !string.IsNullOrEmpty(mark))
                                        {
                                            sb.Append(subject.SubjectName);
                                            sb.Append(": ");
                                            if (!string.IsNullOrEmpty(evaluation))
                                            {
                                                sb.Append(evaluation);
                                            }

                                            if (!string.IsNullOrEmpty(evaluation) && !string.IsNullOrEmpty(mark))
                                            {
                                                sb.Append(",");
                                            }

                                            if (!string.IsNullOrEmpty(mark))
                                            {
                                                sb.Append(mark);
                                            }

                                            sb.Append(";");
                                        }
                                    }
                                }
                                obj.SMSContent = sb.ToString();
                                lstMHHDGD.Add(obj);

                                //Nang luc
                                obj = new CustomSMSData();
                                obj.PupilID = poc.PupilID;
                                sb = new StringBuilder();
                                tempNL = string.Empty;
                                for (int j = 0; j < lstEcNL.Count; j++)
                                {
                                    EvaluationCriteria ec = lstEcNL[j];

                                    RatedCommentPupilBO ratedCommentSubject = lstRatedCommentPupil
                                        .Where(o => o.EvaluationID == 2 && o.SubjectID == ec.EvaluationCriteriaID).FirstOrDefault();

                                    if (ratedCommentSubject != null)
                                    {
                                        string evaluation = ratedCommentSubject.CapacityEndingEvaluation == 1 ? "T"
                                            : (ratedCommentSubject.CapacityEndingEvaluation == 2 ? "Đ" :
                                            (ratedCommentSubject.CapacityEndingEvaluation == 3 ? "C" : string.Empty));

                                        if (!string.IsNullOrEmpty(evaluation))
                                        {
                                            tempNL += evaluation;
                                            tempNL += ";";
                                        }
                                    }
                                }

                                if (!string.IsNullOrWhiteSpace(tempNL))
                                {
                                    sb.Append("Năng lực: ");
                                    sb.Append(tempNL);

                                }

                                obj.SMSContent = sb.ToString();
                                lstNL.Add(obj);

                                //Pham chat
                                obj = new CustomSMSData();
                                obj.PupilID = poc.PupilID;
                                sb = new StringBuilder();
                                tempPC = string.Empty;
                                for (int j = 0; j < lstEcPC.Count; j++)
                                {
                                    EvaluationCriteria ec = lstEcPC[j];

                                    RatedCommentPupilBO ratedCommentSubject = lstRatedCommentPupil
                                        .Where(o => o.EvaluationID == 3 && o.SubjectID == ec.EvaluationCriteriaID).FirstOrDefault();

                                    if (ratedCommentSubject != null)
                                    {
                                        string evaluation = ratedCommentSubject.QualityEndingEvaluation == 1 ? "T"
                                            : (ratedCommentSubject.QualityEndingEvaluation == 2 ? "Đ" :
                                            (ratedCommentSubject.QualityEndingEvaluation == 3 ? "C" : string.Empty));

                                        if (!string.IsNullOrEmpty(evaluation))
                                        {
                                            tempPC += evaluation;
                                            tempPC += ";";
                                        }
                                    }
                                }

                                if (!string.IsNullOrWhiteSpace(tempPC))
                                {
                                    sb.Append("Phẩm chất: ");
                                    sb.Append(tempPC);

                                }

                                obj.SMSContent = sb.ToString();
                                lstPC.Add(obj);

                                break;
                        }

                        //Khen thuong
                        if (semester == GlobalConstants.SEMESTER_OF_YEAR_SECOND || semester == GlobalConstants.SEMESTER_OF_YEAR_FIRST)
                        {
                            obj = new CustomSMSData();
                            obj.PupilID = poc.PupilID;
                            sb = new StringBuilder();
                            tempKT = string.Empty;
                            SummedEndingEvaluationBO see = lstSEE.Where(o => o.PupilID == poc.PupilID && bigSemester == GlobalConstants.SEMESTER_OF_YEAR_SECOND && o.RewardID != null && o.RewardID != 0).FirstOrDefault();
                            if (see != null)
                            {
                                tempKT += "Học sinh xuất sắc";
                                tempKT += ";";
                            }

                            List<EvaluationReward> lstEvaluationRewardPupil = lstEvaluationReward.Where(o => o.PupilID == poc.PupilID).ToList();

                            for (int j = 0; j < lstEvaluationRewardPupil.Count; j++)
                            {
                                EvaluationReward er = lstEvaluationRewardPupil[j];
                                string rewardName = er.RewardID == 1 || er.RewardID == 2 ? er.Content : string.Empty;
                                if (!string.IsNullOrEmpty(rewardName))
                                {
                                    tempKT += rewardName;
                                    tempKT += ";";
                                }
                            }

                            if (!string.IsNullOrWhiteSpace(tempKT))
                            {
                                sb.Append("Khen thưởng: ");
                                sb.Append(tempKT);

                            }
                            obj.SMSContent = sb.ToString();
                            lstKT.Add(obj);
                        }

                        if (semester == GlobalConstants.SEMESTER_OF_YEAR_SECOND)
                        {
                            //Ket qua nam hoc
                            obj = new CustomSMSData();
                            obj.PupilID = poc.PupilID;
                            sb = new StringBuilder();

                            SummedEndingEvaluationBO objSee = lstSEE.Where(o => o.PupilID == poc.PupilID).FirstOrDefault();

                            if (objSee != null)
                            {

                                string evaluation = string.Empty;
                                if (objSee.EndingEvaluation == "HT")
                                {
                                    evaluation = "Được lên lớp";
                                }
                                else if (objSee.EndingEvaluation == "CHT" && objSee.RateAdd != 0 && objSee.RateAdd != 1)
                                {
                                    evaluation = "Chưa hoàn thành chương trình lớp học";
                                }
                                else if (objSee.EndingEvaluation == "CHT" && objSee.RateAdd == 1)
                                {
                                    evaluation = "Được lên lớp";
                                }
                                else if (objSee.EndingEvaluation == "CHT" && objSee.RateAdd == 0)
                                {
                                    evaluation = "Ở lại lớp";
                                }

                                sb.Append(evaluation);
                            }

                            obj.SMSContent = sb.ToString();
                            lstKQNH.Add(obj);

                        }


                    }


                    dic["MHHDGD"] = lstMHHDGD;
                    dic["NL"] = lstNL;
                    dic["PC"] = lstPC;
                    dic["KQNH"] = lstKQNH;
                    dic["KT"] = lstKT;

                }

                #endregion

                bool isEmpty;
                for (int i = 0; i < lstPupilOfClass.Count; i++)
                {
                    PupilOfClassBO poc = lstPupilOfClass[i];
                    isEmpty = true;
                    objData = new CustomSMSData();
                    objData.PupilID = poc.PupilID;

                    string content = template;

                    for (int j = 0; j < lstContentTypeCode.Count; j++)
                    {
                        string code = lstContentTypeCode[j];
                        string contentOfCode = string.Empty;
                        if (code == "THS")
                        {
                            contentOfCode = poc.PupilFullName.FormatName(nameDisplay);
                        }
                        else if (code == "TGDK")
                        {
                            contentOfCode = TGDK;
                        }
                        else
                        {
                            if (dic.ContainsKey(code))
                            {
                                List<CustomSMSData> lstSMS = (List<CustomSMSData>)dic[code];
                                if (lstSMS != null)
                                {
                                    CustomSMSData sms = lstSMS.FirstOrDefault(o => o.PupilID == poc.PupilID);
                                    if (sms != null && !string.IsNullOrWhiteSpace(sms.SMSContent))
                                    {
                                        contentOfCode = sms.SMSContent.Trim();
                                        isEmpty = false;
                                    }
                                }
                            }
                        }

                        content = Regex.Replace(content.Replace("{" + code + "}", contentOfCode), @"(\r\n)+", "\r\n", RegexOptions.Singleline | RegexOptions.Compiled | RegexOptions.ExplicitCapture | RegexOptions.IgnoreCase);
                    }

                    if (lstContentTypeCode.Except(new string[] { "THS", "TGDK" }).Count() == 0)
                    {
                        isEmpty = false;
                    }

                    if (!isEmpty)
                    {
                        objData.SMSContent = content.Trim();
                    }
                    else
                    {
                        objData.SMSContent = string.Empty;
                    }
                    lstData.Add(objData);
                }

                objResult.ListResult = lstData;
                objResult.ValidateCode = WCFConstant.RESPONSE_SUCCESS;
            }
            catch (Exception ex)
            {
                LogExtensions.GetCurrentClass = System.Reflection.MethodBase.GetCurrentMethod().DeclaringType;
                string para = string.Format("schoolID={0}, academicYearID={1}, classID={2}, ", schoolID, academicYearID, classID);
                LogExtensions.ErrorExt(logger, DateTime.Now, para, ex.Message, "null");
                objResult.ValidateCode = WCFConstant.RESPONSE_EXCEPTION;
            }
            return objResult;
        }
        #endregion

        private List<ClassProfileLite> GetListClassByHeadTeacher(int AcademicYearID, int SchoolID, int TeacherID)
        {
            DeclareBusiness();
            List<ClassProfileLite> listResult = ClassProfileBusiness.SearchBySchool(SchoolID, new Dictionary<string, object>(){
                {"AcademicYearID", AcademicYearID},
                {"TeacherByRoleID", TeacherID},
                {"Type", SystemParamsInFile.SUPERVISING_PERMISSION_HEAD_TEACHER}
                }).Select(o => new ClassProfileLite
                {
                    Grade = o.EducationLevel.Grade,
                    EducationLevelID = o.EducationLevelID,
                    ClassProfileID = o.ClassProfileID,
                    ClassName = o.DisplayName,
                    OrderNumber = o.OrderNumber.HasValue ? o.OrderNumber.Value : 0
                }).ToList();
            return listResult;
        }

        #region Lay danh sach diem hoc ky
        public ExamMarkSemesterComposite GetListExamMarkSemester(List<int> lstPupilID, int SchoolID, int ClassID, int AcademicYearID, int Semester)
        {
            ExamMarkSemesterComposite ObjExamMarkSemesterResponse = new ExamMarkSemesterComposite();

            try
            {
                if (Semester == GlobalConstants.SEMESTER_OF_YEAR_ALL)
                {
                    ObjExamMarkSemesterResponse.ValidationCode = WCFConstant.RESPONSE_SEMESTER_NOT_FOUND;
                    ObjExamMarkSemesterResponse.ListExamMarkSemester = new List<ExamMarkSemester>();
                }
                else
                {
                    DeclareBusiness();
                    int grade = ClassProfileBusiness.Find(ClassID).EducationLevel.Grade;
                    #region Danh sach mon hoc cua lop
                    List<ClassSubjectBO> listClassSubject = ClassSubjectBusiness.SearchByClass(ClassID, new Dictionary<string, object>()
                    { { "Semester", Semester } }).Select(o => new ClassSubjectBO
                    {
                        SubjectID = o.SubjectID,
                        DisplayName = o.SubjectCat.DisplayName,
                        IsCommenting = o.IsCommenting,
                        OrderInSubject = o.SubjectCat.OrderInSubject
                    }).OrderBy(p => p.IsCommenting.HasValue ? p.IsCommenting.Value : 0).ThenBy(o => o.OrderInSubject).ThenBy(o => o.DisplayName).ToList();
                    #endregion

                    List<ExamMarkSemester> LstExamMarkSemseter = new List<ExamMarkSemester>();
                    ExamMarkSemester examMarkSemesterObj = null;
                    int PupilID = 0;
                    StringBuilder Content = null;
                    ClassSubjectBO classSubjectObj = null;
                    if (grade != GlobalConstants.PRIMARY_EDUCATION_SCHOOL)
                    {
                        #region Lay diem cap 2,3
                        List<int> listMarkSubjectID = listClassSubject.Where(p => p.IsCommenting == GlobalConstants.ISCOMMENTING_TYPE_MARK).Select(p => p.SubjectID).ToList();
                        List<int> listJudgeSubjectID = listClassSubject.Where(p => p.IsCommenting == GlobalConstants.ISCOMMENTING_TYPE_JUDGE).Select(p => p.SubjectID).ToList();

                        Dictionary<string, object> dic = new Dictionary<string, object>();
                        dic["AcademicYearID"] = AcademicYearID;
                        dic["ClassID"] = ClassID;
                        dic["Semester"] = Semester;
                        dic["Title"] = GlobalConstants.MARK_TYPE_HK;
                        dic["ListPupilID"] = lstPupilID;

                        //Lay diem thi theo mon tinh diem
                        List<VMarkRecord> lstMark = VMarkRecordBusiness.SearchBySchool(SchoolID, dic).Where(m => listMarkSubjectID.Contains(m.SubjectID)).ToList();
                        //Lay diem mon nhan xet
                        List<VJudgeRecord> lstJudge = VJudgeRecordBusiness.SearchBySchool(SchoolID, dic).Where(m => listJudgeSubjectID.Contains(m.SubjectID)).ToList();

                        List<VMarkRecord> lstMarkRecordWithPupil = null;
                        List<VJudgeRecord> lstJudgeRecordWithPupil = null;
                        List<VMarkRecord> lstMarkRecordWithSubject = null;
                        List<VJudgeRecord> lstJudgeRecordWithSubject = null;

                        for (int k = 0, num = lstPupilID.Count; k < num; k++)
                        {
                            PupilID = lstPupilID[k];
                            examMarkSemesterObj = new ExamMarkSemester();
                            Content = new StringBuilder();
                            examMarkSemesterObj.PupilID = PupilID;
                            lstMarkRecordWithPupil = lstMark.Where(p => p.PupilID == PupilID).ToList();
                            lstJudgeRecordWithPupil = lstJudge.Where(p => p.PupilID == PupilID).ToList();
                            for (int i = 0; i < listClassSubject.Count; i++)
                            {
                                classSubjectObj = listClassSubject[i];

                                lstMarkRecordWithSubject = lstMarkRecordWithPupil.Where(p => p.SubjectID == classSubjectObj.SubjectID).ToList();
                                if (lstMarkRecordWithSubject != null && lstMarkRecordWithSubject.Count > 0)
                                {
                                    foreach (var item in lstMarkRecordWithSubject)
                                        Content.Append(classSubjectObj.DisplayName).Append(":").Append(string.Format("{0:0.#}", item.Mark)).Append("\n");
                                }

                                lstJudgeRecordWithSubject = lstJudgeRecordWithPupil.Where(p => p.SubjectID == classSubjectObj.SubjectID).ToList();
                                if (lstJudgeRecordWithSubject != null && lstJudgeRecordWithSubject.Count > 0)
                                {
                                    foreach (var item in lstJudgeRecordWithSubject)
                                        Content.Append(classSubjectObj.DisplayName).Append(":").Append(item.Judgement).Append("\n");
                                }
                            }
                            if (Content.Length > 0) examMarkSemesterObj.Content = Content.ToString();
                            LstExamMarkSemseter.Add(examMarkSemesterObj);
                        }

                        ObjExamMarkSemesterResponse.ListExamMarkSemester = LstExamMarkSemseter;
                        #endregion
                    }
                    else
                    {
                        #region lay diem cap 1
                        // chi lay nhung mon tinh diem
                        SummedEvaluation summedEvaluationObj = null;
                        int partition = UtilsBusiness.GetPartionId(SchoolID);
                        listClassSubject = listClassSubject.Where(p => p.IsCommenting == GlobalConstants.ISCOMMENTING_TYPE_MARK).ToList();
                        List<int> listSubjectID = listClassSubject.Select(p => p.SubjectID).ToList();

                        List<SummedEvaluation> lstSummedEvaluation = (from m in SummedEvaluationBusiness.All
                                                                      where m.LastDigitSchoolID == partition
                                                                       && m.AcademicYearID == AcademicYearID
                                                                       && m.SchoolID == SchoolID
                                                                       && m.ClassID == ClassID
                                                                       && m.SemesterID == Semester
                                                                       && listSubjectID.Contains(m.EvaluationCriteriaID)
                                                                       && lstPupilID.Contains(m.PupilID)
                                                                      select m).ToList();
                        List<SummedEvaluation> lstSummedEvaluationPupil = null;
                        List<SummedEvaluation> lstSummedEvaluationSub = null;
                        for (int k = 0, num = lstPupilID.Count; k < num; k++)
                        {
                            PupilID = lstPupilID[k];
                            lstSummedEvaluationPupil = lstSummedEvaluation.Where(p => p.PupilID == PupilID).ToList();
                            examMarkSemesterObj = new ExamMarkSemester();
                            Content = new StringBuilder();
                            examMarkSemesterObj.PupilID = PupilID;

                            for (int i = 0; i < listClassSubject.Count; i++)
                            {
                                classSubjectObj = listClassSubject[i];
                                if (lstSummedEvaluationPupil != null && lstSummedEvaluationPupil.Count > 0)
                                {
                                    lstSummedEvaluationSub = lstSummedEvaluationPupil.Where(p => p.EvaluationCriteriaID == classSubjectObj.SubjectID).ToList();
                                    if (lstSummedEvaluationSub != null && lstSummedEvaluationSub.Count > 0)
                                    {
                                        foreach (var item in lstSummedEvaluationSub)
                                            Content.Append(classSubjectObj.DisplayName).Append(":").Append(string.Format("{0:0.#}", summedEvaluationObj.PeriodicEndingMark)).Append("\n");
                                    }
                                }
                            }
                            if (Content.Length > 0) examMarkSemesterObj.Content = Content.ToString();
                            LstExamMarkSemseter.Add(examMarkSemesterObj);
                        }
                        ObjExamMarkSemesterResponse.ListExamMarkSemester = LstExamMarkSemseter;

                        #endregion
                    }
                }

                ObjExamMarkSemesterResponse.ValidationCode = WCFConstant.RESPONSE_SUCCESS;
            }
            catch (Exception ex)
            {
                LogExtensions.GetCurrentClass = System.Reflection.MethodBase.GetCurrentMethod().DeclaringType;
                string para = string.Format("SchoolID={0}, ClassID={1}, AcademicYearID={2}, Semester={3}", SchoolID, ClassID, AcademicYearID, Semester);
                LogExtensions.ErrorExt(logger, DateTime.Now, para, ex.Message, "null");
                ObjExamMarkSemesterResponse.ValidationCode = WCFConstant.RESPONSE_EXCEPTION;
            }

            return ObjExamMarkSemesterResponse;

        }
        #endregion

        #region Lay thong tin nhan xet de gui tin cho cac lop moi theo mo hinh VNEN - SMSEdu
        /// <summary>
        /// lấy nhận xét học sinh theo mô hình mới VNEN
        /// </summary>
        /// <param name="pupilIDList"></param>
        /// <param name="schoolID"></param>
        /// <param name="academicYearID"></param>
        /// <param name="classID"></param>
        /// <param name="semesterID"></param>
        /// <param name="monthID"></param>
        /// <returns></returns>
        public EvaluationCommentsComposite getCommentSchoolVNEN(List<long> pupilIDList, int schoolID, int academicYearID, int classID, int semesterID, int monthID)
        {
            EvaluationCommentsComposite objResult = new EvaluationCommentsComposite();
            try
            {
                DeclareBusiness();
                AcademicYear academicYear = AcademicYearBusiness.Find(academicYearID);
                int partitionId = UtilsBusiness.GetPartionId(schoolID);
                List<PupilOfClassBO> pupilOfClassList = (from poc in PupilOfClassBusiness.GetPupilInClass(classID)
                                                         where pupilIDList.Contains(poc.PupilID)
                                                         select poc).ToList();

                List<EvaluationCommentsList> listResult = new List<EvaluationCommentsList>();

                if (pupilOfClassList != null && pupilOfClassList.Count > 0)
                {
                    EvaluationCommentsList obj = null;
                    PupilOfClassBO pupilOfClassBO = null;
                    int pupilID = 0;
                    StringBuilder monthComments = null, pupilContent = null;
                    string MonthStr = string.Empty;

                    if (monthID > 0 && monthID <= GlobalConstants.MonthID_12)
                    {
                        int Year = academicYear.Year;
                        if (monthID <= 8 && semesterID == GlobalConstants.SEMESTER_OF_YEAR_SECOND) Year = academicYear.Year + 1;
                        #region Danh gia theo thang
                        MonthStr = string.Format("{0}{1}", Year, monthID);

                        int v_monthID = 0;
                        if (Int32.TryParse(MonthStr, out v_monthID))
                        {
                            List<TeacherNoteBookMonthBO> listNotesMonth = (from note in TeacherNoteBookMonthBusiness.All
                                                                           join sc in SubjectCatBusiness.All on note.SubjectID equals sc.SubjectCatID
                                                                           where note.SchoolID == schoolID && note.PartitionID == partitionId
                                                                               && note.AcademicYearID == academicYearID && note.ClassID == classID
                                                                               && note.MonthID == v_monthID
                                                                                && pupilIDList.Contains(note.PupilID)
                                                                           orderby sc.OrderInSubject
                                                                           select new TeacherNoteBookMonthBO
                                                                           {
                                                                               PupilID = note.PupilID,
                                                                               SubjectID = note.SubjectID,
                                                                               SubjectName = sc.SubjectName,
                                                                               MonthID = note.MonthID,
                                                                               CommentCQ = note.CommentCQ,
                                                                               CommentSubject = note.CommentSubject
                                                                           }).ToList();
                            TeacherNoteBookMonthBO noteSubjectMonthBO = null;
                            for (int i = 0; i < pupilOfClassList.Count; i++)
                            {
                                pupilOfClassBO = pupilOfClassList[i];
                                pupilID = pupilOfClassBO.PupilID;
                                obj = new EvaluationCommentsList();
                                obj.PupilID = pupilID;
                                obj.PupilName = pupilOfClassBO.PupilFullName;
                                monthComments = new StringBuilder();

                                List<TeacherNoteBookMonthBO> listCommentsBypupilID = listNotesMonth.Where(p => p.PupilID == pupilID).ToList();
                                for (int j = 0; j < listCommentsBypupilID.Count; j++)
                                {
                                    noteSubjectMonthBO = listCommentsBypupilID[j];
                                    if (!string.IsNullOrEmpty(noteSubjectMonthBO.CommentSubject) || !string.IsNullOrEmpty(noteSubjectMonthBO.CommentCQ))
                                    {
                                        monthComments.Append(j + 1).Append("/").Append(noteSubjectMonthBO.SubjectName).Append(":\r\n");
                                        if (!string.IsNullOrEmpty(noteSubjectMonthBO.CommentSubject)) monthComments.AppendLine(noteSubjectMonthBO.CommentSubject);
                                        if (!string.IsNullOrEmpty(noteSubjectMonthBO.CommentCQ)) monthComments.AppendLine(noteSubjectMonthBO.CommentCQ);
                                    }

                                }
                                if (monthComments != null && monthComments.Length > 0)
                                {
                                    obj.EvaluationMonth = monthComments.ToString();
                                }
                                listResult.Add(obj);
                            }
                        }
                        #endregion
                    }
                    else if (monthID == GlobalConstants.MonthID_EndSemester1 || monthID == GlobalConstants.MonthID_EndSemester2)
                    {
                        #region Nhan xet cuoi ky
                        List<ReviewBookPupil> listReviewPupil = (from rv in ReviewBookPupilBusiness.All
                                                                 where rv.SchoolID == schoolID && rv.PartitionID == partitionId
                                                                    && rv.AcademicYearID == academicYearID && rv.ClassID == classID
                                                                    && rv.SemesterID == semesterID
                                                                 && pupilIDList.Contains(rv.PupilID)
                                                                 select rv).ToList();
                        ReviewBookPupil reviewPupil = null;
                        for (int i = 0; i < pupilOfClassList.Count; i++)
                        {
                            monthComments = new StringBuilder();
                            pupilOfClassBO = pupilOfClassList[i];
                            pupilID = pupilOfClassBO.PupilID;
                            obj = new EvaluationCommentsList();
                            obj.PupilID = pupilID;
                            obj.PupilName = pupilOfClassBO.PupilFullName;

                            // lay thong tin nhan xet (So tay hoc sinh)
                            reviewPupil = listReviewPupil.FirstOrDefault(p => p.PupilID == pupilID);
                            if (reviewPupil != null)
                            {
                                if (!string.IsNullOrEmpty(reviewPupil.CQComment)) monthComments.AppendLine(string.Format("Biểu hiện nổi bật: {0}", reviewPupil.CQComment));
                                if (!string.IsNullOrEmpty(reviewPupil.CommentAdd)) monthComments.AppendLine(string.Format("Điều cần khắc phục giúp đỡ, rèn luyện thêm: {0}", reviewPupil.CommentAdd));
                            }


                            if (monthComments != null && monthComments.Length > 0)
                            {
                                obj.EvaluationMonth = monthComments.ToString();
                            }
                            listResult.Add(obj);
                        }
                        #endregion
                    }
                    else
                    {
                        #region Ket qua hoc ky
                        List<SubjectCatBO> lsClassSubject = new List<SubjectCatBO>();
                        if (classID > 0)
                        {
                            lsClassSubject = (from sb in ClassSubjectBusiness.All
                                              join sub in SubjectCatBusiness.All on sb.SubjectID equals sub.SubjectCatID
                                              orderby sb.IsSubjectVNEN descending, sb.IsCommenting, sub.OrderInSubject
                                              where sb.ClassID == classID
                                              && sub.IsActive == true
                                              && ((semesterID == GlobalConstants.SEMESTER_OF_YEAR_FIRST && sb.SectionPerWeekFirstSemester > 0)
                                              || (semesterID == GlobalConstants.SEMESTER_OF_YEAR_SECOND && sb.SectionPerWeekSecondSemester > 0))
                                              select new SubjectCatBO
                                              {
                                                  SubjectCatID = sb.SubjectID,
                                                  SubjectName = sub.SubjectName,
                                                  IsCommenting = sb.IsCommenting,
                                                  IsSubjectVNEN = sb.IsSubjectVNEN
                                              }).ToList();
                        }

                        // Danh sach danh gia So tay GV
                        List<TeacherNoteBookSemester> listNotesSem = (from tbs in TeacherNoteBookSemesterBusiness.All
                                                                      where tbs.SchoolID == schoolID && tbs.PartitionID == partitionId
                                                                       && tbs.AcademicYearID == academicYearID
                                                                       && tbs.ClassID == classID
                                                                       && tbs.SemesterID == semesterID
                                                                       && pupilIDList.Contains(tbs.PupilID)
                                                                      select tbs).ToList();
                        // Thong tin theo TT58
                        List<SummedUpRecord> listSummedUpRecord = (from su in SummedUpRecordBusiness.All
                                                                   where su.SchoolID == schoolID && su.Last2digitNumberSchool == partitionId
                                                                    && su.AcademicYearID == academicYearID
                                                                    && su.ClassID == classID
                                                                    && ((semesterID == GlobalConstants.SEMESTER_OF_YEAR_SECOND && su.Semester == GlobalConstants.SEMESTER_OF_YEAR_ALL)
                                                                     || (semesterID == GlobalConstants.SEMESTER_OF_YEAR_FIRST && su.Semester == semesterID))
                                                                    && su.PeriodID == null
                                                                    && pupilIDList.Contains(su.PupilID)
                                                                   select su).ToList();
                        // Danh sach danh gia So tay HS
                        List<ReviewBookPupil> listReviewPupil = (from rv in ReviewBookPupilBusiness.All
                                                                 where rv.SchoolID == schoolID && rv.PartitionID == partitionId
                                                                    && rv.AcademicYearID == academicYearID && rv.ClassID == classID
                                                                    && rv.SemesterID == semesterID
                                                                 && pupilIDList.Contains(rv.PupilID)
                                                                 select rv).ToList();
                        // Danh sach khen thuong
                        //List<RewardCommentFinal> listRewardCommentsFinal = (from rf in RewardCommentFinalBusiness.All
                        //                                                       where rf.SchoolID == schoolID && rf.LastDigitSchoolID == partitionId
                        //                                                       && rf.AcademicYearID == academicYearID
                        //                                                       && rf.ClassID == classID
                        //                                                       && rf.SemesterID == semesterID
                        //                                                       && pupilIDList.Contains(rf.PupilID)
                        //                                                       select rf).ToList();
                        List<UpdateReward> listRewardComments = (from ur in UpdateRewardBusiness.All
                                                                 where ur.SchoolID == schoolID && ur.PartitionID == partitionId
                                                                            && ur.AcademicYearID == academicYearID
                                                                            && ur.ClassID == classID
                                                                            && ur.SemesterID == semesterID
                                                                            && pupilIDList.Contains(ur.PupilID)
                                                                 select ur).ToList();
                        List<TeacherNoteBookSemester> lstNotesPerPupil = null;
                        TeacherNoteBookSemester objNotesPupil = null;
                        SubjectCatBO objSubjectBO = null;
                        SummedUpRecord objSummedUpRecord = null;
                        ReviewBookPupil objReviewPupil = null;
                        for (int i = 0; i < pupilOfClassList.Count; i++)
                        {
                            pupilOfClassBO = pupilOfClassList[i];
                            pupilID = pupilOfClassBO.PupilID;
                            obj = new EvaluationCommentsList();
                            obj.PupilID = pupilID;
                            obj.PupilName = pupilOfClassBO.PupilFullName;
                            monthComments = new StringBuilder();
                            // Danh gia mon hoc
                            lstNotesPerPupil = listNotesSem.Where(p => p.PupilID == pupilID).ToList();
                            for (int j = 0; j < lsClassSubject.Count; j++)
                            {
                                objSubjectBO = lsClassSubject[j];
                                pupilContent = new StringBuilder();

                                if (objSubjectBO.IsSubjectVNEN == true)
                                {
                                    objNotesPupil = lstNotesPerPupil.FirstOrDefault(o => o.SubjectID == objSubjectBO.SubjectCatID);
                                    if (objNotesPupil != null)
                                    {
                                        if (objNotesPupil.Rate == GlobalConstants.REVIEW_TYPE_FINISH) pupilContent.Append(GlobalConstants.SHORTFINISH).Append(", ");
                                        else if (objNotesPupil.Rate == GlobalConstants.REVIEW_TYPE_NOT_FINISH) pupilContent.Append(GlobalConstants.SHORTNOTFINISH).Append(", ");

                                        if (objSubjectBO.IsCommenting == GlobalConstants.SUBJECTCAT_ISCOMMENTING_MARK && objNotesPupil.AVERAGE_MARK.HasValue) pupilContent.Append(objNotesPupil.AVERAGE_MARK.Value.ToString("#.#")).Append("; ");
                                        else if (objSubjectBO.IsCommenting == GlobalConstants.SUBJECTCAT_ISCOMMENTING_COMMENT && !String.IsNullOrEmpty(objNotesPupil.AVERAGE_MARK_JUDGE)) pupilContent.Append(objNotesPupil.AVERAGE_MARK_JUDGE).Append("; ");
                                    }
                                }
                                else
                                {
                                    objSummedUpRecord = listSummedUpRecord.FirstOrDefault(o => o.PupilID == pupilID && o.SubjectID == objSubjectBO.SubjectCatID);
                                    if (objSummedUpRecord != null)
                                    {
                                        if (objSubjectBO.IsCommenting == GlobalConstants.SUBJECTCAT_ISCOMMENTING_MARK && objSummedUpRecord.SummedUpMark.HasValue) pupilContent.Append(objSummedUpRecord.SummedUpMark.Value.ToString("#.#")).Append("; ");
                                        else if (objSubjectBO.IsCommenting == GlobalConstants.SUBJECTCAT_ISCOMMENTING_COMMENT && !String.IsNullOrEmpty(objSummedUpRecord.JudgementResult)) pupilContent.Append(objSummedUpRecord.JudgementResult).Append("; ");
                                    }
                                }

                                if (pupilContent.Length > 0) monthComments.Append(objSubjectBO.SubjectName).Append(": ").Append(pupilContent.ToString());
                            }

                            if (monthComments.Length > 0) monthComments.Append("\r\n");

                            // Nang luc, pham chat
                            objReviewPupil = listReviewPupil.FirstOrDefault(o => o.PupilID == pupilID);
                            if (objReviewPupil != null)
                            {
                                string strNL = string.Empty;

                                if (objReviewPupil.CapacityRate == 1)
                                {
                                    strNL = "T";
                                }
                                else if (objReviewPupil.CapacityRate == 2)
                                {
                                    strNL = "Đ";
                                }
                                else if (objReviewPupil.CapacityRate == 3)
                                {
                                    strNL = "C";
                                }

                                if (!string.IsNullOrEmpty(strNL))
                                {
                                    monthComments.AppendLine("Năng lực: " + strNL);
                                }

                                string strPC = string.Empty;

                                if (objReviewPupil.QualityRate == 1)
                                {
                                    strPC = "T";
                                }
                                else if (objReviewPupil.QualityRate == 2)
                                {
                                    strPC = "Đ";
                                }
                                else if (objReviewPupil.QualityRate == 3)
                                {
                                    strPC = "C";
                                }

                                if (!string.IsNullOrEmpty(strPC))
                                {
                                    monthComments.AppendLine("Phẩm chất: " + strPC);
                                }

                            }

                            // Khen thuong
                            string strRewardID = listRewardComments.Where(p => p.PupilID == pupilID).Select(p => p.Rewards).FirstOrDefault();
                            if (!string.IsNullOrEmpty(strRewardID))
                            {
                                List<int> listInt = strRewardID.Split(new[] { "," }, StringSplitOptions.RemoveEmptyEntries).Select(p => int.Parse(p)).ToList();
                                List<RewardFinal> rewardFinalList = RewardFinalBusiness.All.Where(p => p.SchoolID == schoolID).ToList();
                                string rewardMode = string.Empty;
                                pupilContent = new StringBuilder();
                                for (int g = 0; g < listInt.Count; g++)
                                {
                                    rewardMode = rewardFinalList.Where(p => p.RewardFinalID == listInt[g]).Select(p => p.RewardMode).FirstOrDefault();
                                    if (!string.IsNullOrWhiteSpace(rewardMode))
                                    {
                                        pupilContent.Append(rewardMode);
                                        if (g != listInt.Count - 1)
                                        {
                                            pupilContent.Append("; ");
                                        }
                                    }
                                }
                                if (pupilContent.Length > 0) monthComments.Append("Khen thưởng: ").AppendLine(pupilContent.ToString());
                            }

                            // Danh gia cuoi nam
                            if (semesterID == GlobalConstants.SEMESTER_OF_YEAR_SECOND)
                            {
                                if (objReviewPupil != null)
                                {
                                    if (objReviewPupil.RateAndYear == GlobalConstants.REVIEW_TYPE_FINISH)
                                    {
                                        monthComments.AppendLine("Được lên lớp");
                                    }
                                    else if (objReviewPupil.RateAndYear == GlobalConstants.REVIEW_TYPE_NOT_FINISH)
                                    {
                                        if (!objReviewPupil.RateAdd.HasValue) monthComments.AppendLine("Chưa hoàn thành chương trình lớp học");
                                        else if (objReviewPupil.RateAdd == GlobalConstants.REVIEW_TYPE_FINISH) monthComments.AppendLine("Được lên lớp");
                                        else if (objReviewPupil.RateAdd == GlobalConstants.REVIEW_TYPE_NOT_FINISH) monthComments.AppendLine("Ở lại lớp");
                                    }
                                }
                            }

                            if (monthComments != null && monthComments.Length > 0)
                            {
                                obj.EvaluationMonth = monthComments.ToString();
                            }
                            listResult.Add(obj);
                        }
                        #endregion
                    }
                }

                objResult.ListResult = listResult;
                objResult.ValidateCode = WCFConstant.RESPONSE_SUCCESS;
            }
            catch (Exception ex)
            {
                LogExtensions.GetCurrentClass = System.Reflection.MethodBase.GetCurrentMethod().DeclaringType;
                string para = string.Format("schoolID={0}, academicYearID={1}, classID={2}, semesterID={3}, monthID={4}", schoolID, academicYearID, classID, semesterID, monthID);
                LogExtensions.ErrorExt(logger, DateTime.Now, para, ex.Message, "null");
                objResult.ValidateCode = WCFConstant.RESPONSE_EXCEPTION;
            }
            return objResult;
        }
        #endregion

        #region 20160302 Anhvd9 - Lấy thông tin kết quả kỳ thi
        /// <summary>
        /// Lấy danh sách kết quả thi của học sinh
        /// </summary>
        /// <param name="pupilIDList"></param>
        /// <param name="schoolID"></param>
        /// <param name="academicYearID"></param>
        /// <param name="classID"></param>
        /// <param name="semesterID"></param>
        /// <param name="examID"></param>
        /// <returns></returns>
        public ListExamMarkPupilResponse getExamResultInfo(List<long> pupilIDList, int schoolID, int academicYearID, int classID, int semesterID, int examID)
        {
            ListExamMarkPupilResponse objResult = new ListExamMarkPupilResponse();
            try
            {
                DeclareBusiness();
                List<PupilOfClassBO> pupilOfClassList = (from poc in PupilOfClassBusiness.GetPupilInClass(classID)
                                                         where pupilIDList.Contains(poc.PupilID)
                                                         select poc).ToList();

                List<ExamMarkPupilResponse> listResult = new List<ExamMarkPupilResponse>();

                if (pupilOfClassList != null && pupilOfClassList.Count > 0)
                {
                    int partitionId = UtilsBusiness.GetPartionId(schoolID);
                    ExamMarkPupilResponse obj = null;
                    PupilOfClassBO pupilOfClassBO = null;
                    int pupilID = 0;
                    StringBuilder pupilContent = null;

                    IQueryable<ExamInputMarkBO> lstExamMarkInput = (from eim in ExamInputMarkBusiness.All
                                                                    join sc in SubjectCatBusiness.All on eim.SubjectID equals sc.SubjectCatID
                                                                    where eim.SchoolID == schoolID && eim.LastDigitSchoolID == partitionId
                                                                    && eim.AcademicYearID == academicYearID
                                                                    && eim.ClassID == classID && pupilIDList.Contains(eim.PupilID)
                                                                    && eim.ExaminationsID == examID
                                                                    orderby sc.OrderInSubject
                                                                    select new ExamInputMarkBO
                                                                    {
                                                                        PupilID = eim.PupilID,
                                                                        SubjectID = eim.SubjectID,
                                                                        SubjectName = sc.SubjectName,
                                                                        ClassID = eim.ClassID,
                                                                        ExamMark = eim.ExamMark,
                                                                        ActualMark = eim.ActualMark,
                                                                        ExamJudgeMark = eim.ExamJudgeMark
                                                                    });
                    Examinations exam = ExaminationsBusiness.Find(examID);
                    List<ExamInputMarkBO> lstMarkEachPupil = null;
                    ExamInputMarkBO objExamInputMarkBO = null;
                    for (int i = 0; i < pupilOfClassList.Count; i++)
                    {
                        pupilOfClassBO = pupilOfClassList[i];
                        pupilID = pupilOfClassBO.PupilID;
                        obj = new ExamMarkPupilResponse();
                        obj.PupilID = pupilID;
                        obj.PupilName = pupilOfClassBO.PupilFullName;
                        if (exam != null)
                        {
                            obj.ExaminationID = examID;
                            obj.ExaminationName = exam.ExaminationsName;
                        }
                        pupilContent = new StringBuilder();

                        lstMarkEachPupil = lstExamMarkInput.Where(p => p.PupilID == pupilID).ToList();
                        bool isFirst = true;
                        for (int j = 0; j < lstMarkEachPupil.Count; j++)
                        {
                            objExamInputMarkBO = lstMarkEachPupil[j];
                            if (objExamInputMarkBO.ActualMark.HasValue || !string.IsNullOrEmpty(objExamInputMarkBO.ExamJudgeMark))
                            {
                                if (isFirst) isFirst = false;
                                else pupilContent.Append("; ");
                                pupilContent.Append(objExamInputMarkBO.SubjectName).Append(" ");
                                if (objExamInputMarkBO.ActualMark.HasValue) pupilContent.Append(objExamInputMarkBO.ActualMark.Value.ToString("#0.#"));
                                else if (!string.IsNullOrEmpty(objExamInputMarkBO.ExamJudgeMark)) pupilContent.Append(objExamInputMarkBO.ExamJudgeMark);
                            }
                        }
                        if (pupilContent != null && pupilContent.Length > 0) obj.ExamResult = pupilContent.ToString();
                        listResult.Add(obj);
                    }
                }


                objResult.LstExamMarkPupil = listResult;
                objResult.ValidateCode = WCFConstant.RESPONSE_SUCCESS;
            }
            catch (Exception ex)
            {
                LogExtensions.GetCurrentClass = System.Reflection.MethodBase.GetCurrentMethod().DeclaringType;
                string para = string.Format("schoolID={0}, academicYearID={1}, classID={2}, semesterID={3}, examID={4}", schoolID, academicYearID, classID, semesterID, examID);
                LogExtensions.ErrorExt(logger, DateTime.Now, para, ex.Message, "null");
                objResult.ValidateCode = WCFConstant.RESPONSE_EXCEPTION;
            }
            return objResult;
        }

        /// <summary>
        /// Lấy thông tin kỳ thi
        /// </summary>
        /// <param name="pupilIDList"></param>
        /// <param name="schoolID"></param>
        /// <param name="academicYearID"></param>
        /// <param name="classID"></param>
        /// <param name="semesterID"></param>
        /// <param name="examID"></param>
        /// <returns></returns>
        public ListExamSubjectResponse getExamScheduleInfo(List<long> pupilIDList, int schoolID, int academicYearID, int classID, int semesterID, int examID)
        {
            ListExamSubjectResponse objResult = new ListExamSubjectResponse();
            try
            {
                DeclareBusiness();
                List<PupilOfClassBO> pupilOfClassList = (from poc in PupilOfClassBusiness.GetPupilInClass(classID)
                                                         where pupilIDList.Contains(poc.PupilID)
                                                         select poc).ToList();

                List<ExamSubjectResponse> listResult = new List<ExamSubjectResponse>();
                ExamSubjectResponse obj;
                if (pupilOfClassList != null && pupilOfClassList.Count > 0)
                {
                    int partitionId = UtilsBusiness.GetPartionId(schoolID);
                    PupilOfClassBO pupilOfClassBO = null;
                    int pupilID = 0;
                    StringBuilder pupilContent = null;

                    IQueryable<ExamPupilBO> iqExamPupil = (from ex in ExaminationsBusiness.All
                                                           join eg in ExamGroupBusiness.All on ex.ExaminationsID equals eg.ExaminationsID
                                                           join ep in ExamPupilBusiness.All on new { ex.ExaminationsID, eg.ExamGroupID } equals new { ep.ExaminationsID, ep.ExamGroupID }
                                                           join es in ExamSubjectBusiness.All on new { ex.ExaminationsID, eg.ExamGroupID } equals new { es.ExaminationsID, es.ExamGroupID }
                                                           join sc in SubjectCatBusiness.All on es.SubjectID equals sc.SubjectCatID
                                                           where ex.SchoolID == schoolID && ep.LastDigitSchoolID == partitionId
                                                           && ex.AcademicYearID == academicYearID
                                                           && pupilIDList.Contains(ep.PupilID)
                                                           && ex.ExaminationsID == examID
                                                           orderby sc.OrderInSubject
                                                           select new ExamPupilBO
                                                           {
                                                               ExaminationsID = ex.ExaminationsID,
                                                               ExaminationName = ex.ExaminationsName,
                                                               PupilID = ep.PupilID,
                                                               ExamGroupID = eg.ExamGroupID,
                                                               SubjectID = es.SubjectID,
                                                               SubjectName = sc.SubjectName,
                                                               SchedulesExam = es.SchedulesExam
                                                           });
                    List<ExamPupilBO> lstGroupEachPupil = null;
                    ExamPupilBO objExamDetailBO = null;
                    for (int i = 0; i < pupilOfClassList.Count; i++)
                    {
                        pupilOfClassBO = pupilOfClassList[i];
                        pupilID = pupilOfClassBO.PupilID;
                        obj = new ExamSubjectResponse();
                        obj.PupilID = pupilID;
                        obj.PupilName = pupilOfClassBO.PupilFullName;

                        pupilContent = new StringBuilder();
                        bool isFirst = true;
                        lstGroupEachPupil = iqExamPupil.Where(p => p.PupilID == pupilID).ToList();
                        for (int j = 0; j < lstGroupEachPupil.Count; j++)
                        {
                            objExamDetailBO = lstGroupEachPupil[j];
                            if (isFirst)
                            {
                                isFirst = false;
                                obj.ExaminationID = objExamDetailBO.ExaminationsID;
                                obj.ExaminationName = objExamDetailBO.ExaminationName;
                            }
                            if (!string.IsNullOrEmpty(objExamDetailBO.SchedulesExam))
                                pupilContent.Append(objExamDetailBO.SubjectName).Append(" ").Append(objExamDetailBO.SchedulesExam).Append("; ");
                        }

                        if (pupilContent != null && pupilContent.Length > 0)
                        {
                            if (pupilContent.ToString().EndsWith("; ")) pupilContent = pupilContent.Remove(pupilContent.Length - 2, 2);
                            obj.ScheduleContent = pupilContent.ToString();

                        }

                        listResult.Add(obj);
                    }
                }


                objResult.LstExamSubject = listResult;
                objResult.ValidationCode = WCFConstant.RESPONSE_SUCCESS;
            }
            catch (Exception ex)
            {
                LogExtensions.GetCurrentClass = System.Reflection.MethodBase.GetCurrentMethod().DeclaringType;
                string para = string.Format("schoolID={0}, academicYearID={1}, classID={2}, semesterID={3}, examID={4}", schoolID, academicYearID, classID, semesterID, examID);
                LogExtensions.ErrorExt(logger, DateTime.Now, para, ex.Message, "null");
                objResult.ValidationCode = WCFConstant.RESPONSE_EXCEPTION;
            }
            return objResult;
        }


        #endregion


        public GridTableResponse GetPupilWithPaging(int page, int size, int academicYearId, int schoolId, List<int> educationLevels, int? classId, string code,
            string fullname, List<int> notInPupilIds, List<int> forceInPupilIds, List<int> searchInPupilIds)
        {
            SMASEntities context = new SMASEntities();
            this.PupilProfileBusiness = new PupilProfileBusiness(logger, context);
            int total;
            List<PupilProfile> list = PupilProfileBusiness.GetPupilWithPaging(page, size, academicYearId, schoolId, educationLevels, classId, code, fullname, notInPupilIds, forceInPupilIds, searchInPupilIds, out total);
            var result = new GridTableResponse()
            {
                Size = size,
                Page = page,
                Total = total,
                PupilProfileLiteList = list.Select(x => new PupilProfileLite()
                {
                    PupilProfileID = x.PupilProfileID,
                    CurrentClassID = x.CurrentClassID,
                    CurrentAcademicYearID = x.CurrentAcademicYearID,
                    FullName = x.FullName,
                    Status = x.ProfileStatus,
                    PupilCode = x.PupilCode,
                    CurrentSchoolID = x.CurrentSchoolID,
                    Birthday = x.BirthDate,
                    PupilOfClassID = x.CurrentClassID,
                    Genre = x.Genre

                }).ToList()
            };
            return result;
        }

        #region lay nhan xet cua hoc sinh
        public List<CommentOfPupilResponse> GetCommentOfPupil(List<int> lstPupilID, int SchoolID, int AcademicYearID, int ClassID, int SubjectID, int SemesterID, int DayOfMonth, DateTime FromDate, DateTime ToDate, int NoDate, int NoSubject)
        {
            DeclareBusiness();
            string SPACE = "\r\n";
            List<CommentOfPupilResponse> lstResult = new List<CommentOfPupilResponse>();
            CommentOfPupilResponse objResult = null;
            IDictionary<string, object> dic = new Dictionary<string, object>()
            {
                {"SchoolID",SchoolID},
                {"AcademicYearID",AcademicYearID},
                {"lstPupilID",lstPupilID},
                {"ClassID",ClassID},
                {"SubjectID",SubjectID},
                {"SemesterID",SemesterID}
            };

            DateTime fDate = DateTime.ParseExact(FromDate.ToString("dd/MM/yyyy"), "dd/MM/yyyy", null);
            DateTime tDate = DateTime.ParseExact(ToDate.ToString("dd/MM/yyyy"), "dd/MM/yyyy", null);

            List<CommentOfPupilBO> lstCommentOfPupil = new List<CommentOfPupilBO>();
            List<CommentOfPupilBO> lsttmp = null;
            CommentOfPupilBO objCommentOfPupil = null;
            lstCommentOfPupil = (from cop in CommentOfPupilBusiness.Search(dic).Where(p => fDate <= p.DateTimeComment && p.DateTimeComment <= tDate)
                                 join sc in SubjectCatBusiness.All on cop.SubjectID equals sc.SubjectCatID
                                 select new CommentOfPupilBO
                                 {
                                     PupilID = cop.PupilID,
                                     ClassID = cop.ClassID,
                                     SubjectID = cop.SubjectID,
                                     SubjectName = sc.DisplayName,
                                     DateTimeComment = cop.DateTimeComment,
                                     MonthID = cop.MonthID,
                                     Comment = cop.Comment,
                                     OrderInSubject = sc.OrderInSubject,
                                     CreateTime = cop.CreateTime,
                                     UpdateTime = cop.UpdateTime,
                                     LogChangeID = cop.LogChangeID
                                 }).ToList();
            int PupilID = 0;
            string CommentOfPupils = string.Empty;
            for (int i = 0; i < lstPupilID.Count; i++)
            {
                objResult = new CommentOfPupilResponse();
                CommentOfPupils = string.Empty;
                PupilID = lstPupilID[i];
                objResult.PupilID = PupilID;
                lsttmp = lstCommentOfPupil.Where(p => p.PupilID == PupilID).OrderBy(p => p.DateTimeComment).ThenBy(p => p.OrderInSubject).ToList();
                for (int j = 0; j < lsttmp.Count; j++)
                {
                    objCommentOfPupil = lsttmp[j];
                    if (!string.IsNullOrEmpty(objCommentOfPupil.Comment))
                    {
                        if (DayOfMonth > 0 && SubjectID > 0)
                        {
                            CommentOfPupils += objCommentOfPupil.Comment + (j < lsttmp.Count - 1 ? SPACE : "");
                        }
                        else
                        {
                            if (NoDate == 1)
                            {
                                if (NoSubject == 1)
                                {
                                    CommentOfPupils += objCommentOfPupil.Comment + (j < lsttmp.Count - 1 ? SPACE : "");
                                }
                                else
                                {
                                    CommentOfPupils += "[" + objCommentOfPupil.SubjectName + "]: " + objCommentOfPupil.Comment + (j < lsttmp.Count - 1 ? SPACE : "");
                                }
                            }
                            else
                            {
                                if (NoSubject == 1)
                                {
                                    CommentOfPupils += "[" + objCommentOfPupil.DateTimeComment.ToString("dd/MM/yyyy") + "]" + objCommentOfPupil.Comment + (j < lsttmp.Count - 1 ? SPACE : "");
                                }
                                else
                                {
                                    CommentOfPupils += "[" + objCommentOfPupil.DateTimeComment.ToString("dd/MM/yyyy") + "][" + objCommentOfPupil.SubjectName + "]: " + objCommentOfPupil.Comment + (j < lsttmp.Count - 1 ? SPACE : "");
                                }
                            }
                        }
                    }
                }
                objResult.Comment = CommentOfPupils;
                lstResult.Add(objResult);
            }
            return lstResult;
        }
        #endregion

    }
}