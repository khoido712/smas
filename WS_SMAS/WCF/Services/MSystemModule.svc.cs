﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;
using SMAS.Models.Models;
using WCF.Composite;
using WCF.Composite.Migrate;
using SMAS.Business.IBusiness;
using log4net;
using SMAS.Business.Business;
using SMAS.Business.Common;
using System.Web.Security;
using SMAS.VTUtils.Excel.ContextExt;
using System.Data;
using WCF.Composite.Sync;
using System.Data.Entity;
using System.Data.Common;
using System.Dynamic;
using Oracle.DataAccess.Client;
using SMAS.VTUtils.Log;

namespace WCF.Services
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the class name "MSystemModule" in code, svc and config file together.
    // NOTE: In order to launch WCF Test Client for testing this service, please select MSystemModule.svc or MSystemModule.svc.cs at the Solution Explorer and start debugging.
    public class MSystemModule : IMSystemModule
    {
        #region Declare Variable

        private IClassProfileBusiness ClassProfileBusiness;
        private IClassSubjectBusiness ClassSubjectBusiness;
        private ISchoolProfileBusiness SchoolProfileBusiness;
        private IUserAccountBusiness UserAccountBusiness;
        private IAcademicYearBusiness AcademicYearBusiness;
        private IUserGroupBusiness UserGroupBusiness;
        private IGroupMenuBusiness GroupMenuBusiness;
        private IGroupCatBusiness GroupCatBusiness;
        private ISchoolSubjectBusiness SchoolSubjectBusiness;

        private static readonly log4net.ILog logger = LogManager.GetLogger(typeof(MSystemModule));

        //private SMASEntities context;

        public MSystemModule()
        {
            //DeclareBusiness();
        }
        /// <summary>
        /// Khoi tao business cung context
        /// </summary>
        private SMASEntities DeclareBusiness()
        {
            var context = new SMASEntities();
            context.Configuration.AutoDetectChangesEnabled = false;
            context.Configuration.ValidateOnSaveEnabled = false;
            ClassSubjectBusiness = new ClassSubjectBusiness(logger, context);
            SchoolProfileBusiness = new SchoolProfileBusiness(logger, context);
            UserAccountBusiness = new UserAccountBusiness(logger, context);
            AcademicYearBusiness = new AcademicYearBusiness(logger, context);
            UserGroupBusiness = new UserGroupBusiness(logger, context);
            GroupMenuBusiness = new GroupMenuBusiness(logger, context);
            GroupCatBusiness = new GroupCatBusiness(logger, context);
            ClassProfileBusiness = new ClassProfileBusiness(logger, context);
            SchoolSubjectBusiness = new SchoolSubjectBusiness(logger, context);

            return context;
        }

        public void SetAutoDetectChangesEnabled(SMASEntities context, bool isAutoDetectChangesEnabled)
        {
            context.Configuration.ValidateOnSaveEnabled = isAutoDetectChangesEnabled;
            context.Configuration.AutoDetectChangesEnabled = isAutoDetectChangesEnabled;

            /*if (context != null)
            {
                context.Dispose();
            }*/
        }

        #endregion

        #region nam hoc
        public MAcademicYearResponse GetAcademicYear(List<int> lstSchoolId, int year)
        {
            MAcademicYearResponse retval = new MAcademicYearResponse();
            SMASEntities context = DeclareBusiness();
            try
            {
                var iqAcademicYear = context.AcademicYear.Where(s => (s.IsActive.HasValue && s.IsActive.Value) && lstSchoolId.Contains(s.SchoolID) && s.Year == year);

                var lstAcademicYear = iqAcademicYear.Select(a => new MAcademicYearDefault
                {
                    AcademicYearID = a.AcademicYearID,
                    ConductEstimationType = a.ConductEstimationType,
                    CreatedYear = a.CreatedYear,
                    DisplayTitle = a.DisplayTitle,
                    FirstSemesterEndDate = a.FirstSemesterEndDate,
                    FirstSemesterStartDate = a.FirstSemesterStartDate,
                    IsActive = a.IsActive,
                    IsClassification = a.IsClassification,
                    IsHeadTeacherCanSendSMS = a.IsHeadTeacherCanSendSMS,
                    IsLockNumOrdinal = a.IsLockNumOrdinal,
                    IsLockPupilProfile = a.IsLockPupilProfile,
                    IsMovedHistory = a.IsMovedHistory,
                    IsSecondSemesterToSemesterAll = a.IsSecondSemesterToSemesterAll,
                    IsShowAvatar = a.IsShowAvatar,
                    IsTeacherCanViewAll = a.IsTeacherCanViewAll,
                    IsShowCalendar = a.IsShowCalendar,
                    IsSubjectTeacherCanSendSMS = a.IsSubjectTeacherCanSendSMS,
                    MSourcedb = a.MSourcedb,
                    M_OldID = a.M_OldID,
                    M_ProvinceID = a.M_ProvinceID,
                    RankingCriteria = a.RankingCriteria,
                    SchoolID = a.SchoolID,
                    SecondSemesterEndDate = a.SecondSemesterEndDate,
                    SecondSemesterStartDate = a.SecondSemesterStartDate,
                    SynchronizeID = a.SynchronizeID,
                    Year = a.Year,
                }).ToList();
                retval.ListAcademicYear = lstAcademicYear;
                retval.Susscess = true;
                retval.ValidationCode = WCFConstant.RESPONSE_SUCCESS;
                retval.Message = "Lay thong tin nam hoc thanh cong. Tong so ban ghi :" + lstAcademicYear.Count;
                return retval;
            }
            catch (Exception ex)
            {

                LogExtensions.GetCurrentClass = System.Reflection.MethodBase.GetCurrentMethod().DeclaringType;
                string para = string.Format("year={0} ", year);
                LogExtensions.ErrorExt(logger, DateTime.Now, para, ex.Message, "null");
                retval.ListAcademicYear = null;
                retval.Susscess = false;
                retval.ValidationCode = WCFConstant.RESPONSE_EXCEPTION;
                retval.Message = ex.Message;
                return retval;
            }
            finally
            {
                SetAutoDetectChangesEnabled(context, true);
            }
        }
        public MAcademicYearResponse GetAcademicYearbyProvince(int provinceId, int year, int synchronizeId, int node, int lengthNode)
        {
            MAcademicYearResponse retval = new MAcademicYearResponse();
            SMASEntities context = DeclareBusiness();
            try
            {
                var iqAcademicYear = context.AcademicYear.Where(s => (s.IsActive.HasValue && s.IsActive.Value) && s.Year == year);
                var iqSchool = context.SchoolProfile.Where(s => s.IsActive && s.ProvinceID == provinceId);
                if (synchronizeId > 0)
                {
                    iqSchool = iqSchool.Where(s => s.SynchronizeID > 0);
                }
                if (node > 0 && lengthNode > 0)
                {
                    iqSchool = iqSchool.Where(s => s.SchoolProfileID % lengthNode == node);
                }
                var iqRetval = from a in iqAcademicYear
                               join s in iqSchool on a.SchoolID equals s.SchoolProfileID
                               select new MAcademicYearDefault
                               {
                                   AcademicYearID = a.AcademicYearID,
                                   ConductEstimationType = a.ConductEstimationType,
                                   CreatedYear = a.CreatedYear,
                                   DisplayTitle = a.DisplayTitle,
                                   FirstSemesterEndDate = a.FirstSemesterEndDate,
                                   FirstSemesterStartDate = a.FirstSemesterStartDate,
                                   IsActive = a.IsActive,
                                   IsClassification = a.IsClassification,
                                   IsHeadTeacherCanSendSMS = a.IsHeadTeacherCanSendSMS,
                                   IsLockNumOrdinal = a.IsLockNumOrdinal,
                                   IsLockPupilProfile = a.IsLockPupilProfile,
                                   IsMovedHistory = a.IsMovedHistory,
                                   IsSecondSemesterToSemesterAll = a.IsSecondSemesterToSemesterAll,
                                   IsShowAvatar = a.IsShowAvatar,
                                   IsTeacherCanViewAll = a.IsTeacherCanViewAll,
                                   IsShowCalendar = a.IsShowCalendar,
                                   IsSubjectTeacherCanSendSMS = a.IsSubjectTeacherCanSendSMS,
                                   MSourcedb = a.MSourcedb,
                                   M_OldID = a.M_OldID,
                                   M_ProvinceID = a.M_ProvinceID,
                                   RankingCriteria = a.RankingCriteria,
                                   SchoolID = a.SchoolID,
                                   SecondSemesterEndDate = a.SecondSemesterEndDate,
                                   SecondSemesterStartDate = a.SecondSemesterStartDate,
                                   SynchronizeID = a.SynchronizeID,
                                   Year = a.Year,
                                   SourceSchoolId = s.M_OldID.HasValue ? s.M_OldID.Value : 0,
                                   EducationGrade = s.EducationGrade

                               };
                var lstAcademicYear = iqRetval.ToList();
                retval.ListAcademicYear = lstAcademicYear;
                retval.Susscess = true;
                retval.ValidationCode = WCFConstant.RESPONSE_SUCCESS;
                retval.Message = "Lay thong tin nam hoc thanh cong. Tong so ban ghi :" + lstAcademicYear.Count;
                return retval;
            }
            catch (Exception ex)
            {
                LogExtensions.GetCurrentClass = System.Reflection.MethodBase.GetCurrentMethod().DeclaringType;
                string para = string.Format("provinceId={0}, year={1}", provinceId, year);
                LogExtensions.ErrorExt(logger, DateTime.Now, para, ex.Message, "null");
                retval.ListAcademicYear = null;
                retval.Susscess = false;
                retval.ValidationCode = WCFConstant.RESPONSE_EXCEPTION;
                retval.Message = ex.Message;
                return retval;
            }
            finally
            {
                SetAutoDetectChangesEnabled(context, true);
            }
        }

        #endregion

        #region truong 

        public BoolResultResponse SchoolInsert(List<MSchoolProfileDefault> lstSchoolInput, int provinceId, int year)
        {

            BoolResultResponse retval = new BoolResultResponse();
            string constr = WCFUtils.GetConnectionString;

            if (string.IsNullOrEmpty(constr))
            {
                return new BoolResultResponse
                {
                    Result = false,
                    Description = "Khong lay duoc chuoi ket noi",
                };
            }

            SMASEntities context = DeclareBusiness();
            try
            {
                if (lstSchoolInput == null || lstSchoolInput.Count == 0)
                {
                    retval.Result = true;
                    retval.ValidationCode = WCFConstant.RESPONSE_EXCEPTION;
                    retval.Description = "Không có dữ liệu trường";
                    return retval;
                }

                retval.Result = false;
                SchoolProfile schoolInsert;
                UserAccount userAccount;
                List<SchoolProfile> lstSchoolInsert = new List<SchoolProfile>();
                List<UserAccount> lstUserAccount = new List<UserAccount>();
                List<string> lstExistsUser = new List<string>();

                var mapcolumn = new ColumnMapping();

                for (int i = 0; i < lstSchoolInput.Count; i++)
                {
                    var s = lstSchoolInput[i];

                    var exitsUser = Membership.GetUser(s.UserName.Trim());
                    if (exitsUser != null)
                    {
                        lstExistsUser.Add(s.UserName);
                        continue;
                    }

                    schoolInsert = new SchoolProfile
                    {
                        AdminID = s.AdminID,
                        Address = s.Address,
                        AreaID = s.AreaID,
                        CommuneID = s.CommuneID,
                        Fax = s.Fax,
                        CreatedDate = s.CreatedDate,
                        DistrictID = s.DistrictID,
                        EducationGrade = s.EducationGrade,
                        Email = s.Email,
                        HeadMasterName = s.HeadMasterName,
                        HeadMasterPhone = s.HeadMasterPhone,
                        IsActive = s.IsActive,
                        IsActiveSMAS = s.IsActive,
                        MSourcedb = s.SchoolCode,
                        SchoolCode = s.SchoolCode,
                        M_OldID = s.M_OldID,
                        SchoolName = s.SchoolName,
                        SchoolTypeID = s.SchoolTypeID,
                        ShortName = s.SchoolCode,
                        SynchronizeID = s.SynchronizeID,
                        Website = s.Website,
                        TrainingTypeID = s.TrainingTypeID,
                        ProductVersion = s.ProductVersion,
                        ProvinceID = s.ProvinceID,
                        SupervisingDeptID = s.SupervisingDeptID,
                        Telephone = s.Telephone,
                        HasSubsidiary = s.HasSubsidiary,
                        SchoolYearTitle = s.SchoolYearTitle,
                        SMSParentActiveType = s.SMSParentActiveType,
                        SMSTeacherActiveType = s.SMSTeacherActiveType
                    };
                    lstSchoolInsert.Add(schoolInsert);


                    //Tao userName
                    string password = Utils.GenPassRandom(8);
                    var m = Membership.CreateUser(s.UserName, password);
                    userAccount = new UserAccount
                    {
                        CreatedDate = DateTime.Now,
                        CreatedUserID = 0,
                        //EmployeeID = null,
                        //FirstLoginDate = null,
                        IsActive = true,
                        IsAdmin = true,
                        IsParent = false,
                        IsOperationalUser = false,
                        LoginFalseCount = 0,
                        SelectLevel = 0,
                        GUID = (Guid)m.ProviderUserKey
                    };
                    lstUserAccount.Add(userAccount);
                }
                if (lstUserAccount.Count == 0)
                {
                    return new BoolResultResponse
                    {
                        Result = false,
                        Description = "Khong lay duoc thong tin Username"
                    };
                }
                int retVal1 = SchoolProfileBusiness.BulkInsert(constr, lstSchoolInsert, mapcolumn.SchoolProfile(), "SchoolProfileID", "Image");
                UserAccountBusiness.BulkInsert(constr, lstUserAccount, mapcolumn.UserAccount(), "UserAccountID");

                //Danh sach id truong cu
                List<int> lstOldSchooId = lstSchoolInput.Where(s => s.M_OldID.HasValue).Select(s => s.M_OldID.Value).ToList();
                //Lay danh sach truong da duoc dong bo theo ID truong cu
                var AddedSchool = context.SchoolProfile.Where(s => s.IsActive && s.ProvinceID == provinceId && s.M_OldID.HasValue && lstOldSchooId.Contains(s.M_OldID.Value)).ToList();
                if (AddedSchool == null || AddedSchool.Count == 0)
                {
                    return retval;
                }

                #region Them quyen cho userName
                ///Danh sach userAccount moi them
                ///
                List<string> lstUserName = lstSchoolInput.Where(s => s.UserName != "").Select(s => s.UserName.Trim()).ToList();

                //List<MSchoolUserDefault> lstUserAccountAdded = getUserAccount(lstUserName, lstExistsUser);

                List<MSchoolUserDefault> lstUserAccountAdded = (from ac in context.UserAccount
                                                                join u in context.aspnet_Users on ac.GUID equals u.UserId
                                                                where lstUserName.Contains(u.UserName)
                                                                select new MSchoolUserDefault
                                                                {
                                                                    UserAccountId = ac.UserAccountID,
                                                                    UserName = u.UserName
                                                                }).ToList();
                //Add Role
                List<MSchoolUserDefault> lstSchoolRetval = new List<MSchoolUserDefault>();
                List<GroupCat> lstGroupCat = new List<GroupCat>();
                if (lstUserAccountAdded == null || lstUserAccountAdded.Count == 0)
                {
                    retval = new BoolResultResponse
                    {
                        Description = "Khong lay duoc thong tin userAccount",
                        Result = false,
                        ValidationCode = 0
                    };
                    return retval;
                }
                List<int> lstRole;
                MSchoolProfileDefault sourceSchool;
                foreach (var item in lstUserAccountAdded)
                {
                    sourceSchool = lstSchoolInput.FirstOrDefault(s => s.UserName.Trim().ToLower() == item.UserName.Trim().ToLower());
                    if (sourceSchool == null)
                    {
                        continue;
                    }
                    item.OldSchoolId = sourceSchool.M_OldID.HasValue ? sourceSchool.M_OldID.Value : 0;
                    lstSchoolRetval.Add(item);

                    /*if (sourceSchool.ListRole == null)
                    {                        
                        continue;
                    }
                    lstRole = sourceSchool.ListRole;
                    try
                    {
                        foreach (int role in lstRole)
                        {                            
                            //Lấy danh sách các bản ghi lstDefaultGroup.
                            Dictionary<string, object> SearchRole = new Dictionary<string, object>();
                            SearchRole["RoleID"] = role;
                            List<DefaultGroup> lstDefaultGroup = context.DefaultGroup.Where(s => s.RoleID == role).ToList();
                            if (lstDefaultGroup == null || lstDefaultGroup.Count == 0)
                            {
                                continue;
                            }                            
                            //Lấy danh sách các bản ghi DefaultGroupMenu
                            List<int> lstDefaultGroupID = lstDefaultGroup.Select(u => u.DefaultGroupID).ToList();
                            List<DefaultGroupMenu> lstDefaultGroupMenu = context.DefaultGroupMenu.Where(u => lstDefaultGroupID.Contains(u.DefaultGroupID)).ToList();
                            if (lstDefaultGroupMenu == null || lstDefaultGroupMenu.Count == 0)
                            {
                                continue;
                            }                            
                            foreach (DefaultGroup df in lstDefaultGroup)
                            {
                                if (df == null)
                                {
                                    continue;
                                }
                                logger.Info("Thuc hien add groupMenu vao list");
                                List<DefaultGroupMenu> lsGroupMenu = lstDefaultGroupMenu.Where(o => o.DefaultGroupID == df.DefaultGroupID).ToList();
                                if (lsGroupMenu == null || lsGroupMenu.Count == 0)
                                {
                                    continue;
                                }
                                GroupCat g = new GroupCat();
                                g.RoleID = role;
                                g.CreatedUserID = 1;
                                g.GroupName = df.DefaultGroupName;
                                g.Description = df.Description;
                                g.CreatedDate = DateTime.Now;
                                g.IsActive = true;
                                g.AdminUserID = item.UserAccountId;
                                foreach (DefaultGroupMenu dfm in lsGroupMenu)
                                {
                                    GroupMenu gm = new GroupMenu();
                                    gm.MenuID = dfm.MenuID;
                                    gm.Permission = dfm.Permission;
                                    g.GroupMenus.Add(gm);
                                }
                                //GroupCatBusiness.Insert(g);
                                lstGroupCat.Add(g);
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        LogExtensions.GetCurrentClass = System.Reflection.MethodBase.GetCurrentMethod().DeclaringType;
                        string para = "null";
                        LogExtensions.ErrorExt(logger, DateTime.Now, para, ex.Message, "null");
                    }*/


                }
                /*if (lstGroupCat != null && lstGroupCat.Count > 0)
                {
                    GroupCatBusiness.BulkInsert(constr, lstGroupCat, mapcolumn.GroupCat(), "GroupCatID");
                }*/
                #endregion

                #region Tao nam hoc moi                
                List<int> lstSchoolId = AddedSchool.Select(s => s.SchoolProfileID).ToList();
                var lstAcademicYear = context.AcademicYear.Where(a => lstSchoolId.Contains(a.SchoolID) && a.IsActive == true && a.Year == year).ToList();
                AcademicYear objAcademicYear;
                List<AcademicYear> lstAcademicYearInsert = new List<AcademicYear>();
                SchoolProfile schoolUpdate;
                foreach (var item in lstSchoolRetval)
                {

                    schoolUpdate = AddedSchool.FirstOrDefault(s => s.M_OldID == item.OldSchoolId);
                    if (schoolUpdate == null)
                    {
                        continue;
                    }

                    //logger.Info("Cap nhạt adminId cho truong : " + schoolUpdate.SchoolProfileID);
                    schoolUpdate.AdminID = item.UserAccountId;
                    schoolUpdate.ModifiedDate = DateTime.Now;
                    //SchoolProfileBusiness.Update(schoolUpdate);
                    context.SchoolProfile.Attach(schoolUpdate);
                    context.Entry(schoolUpdate).State = EntityState.Modified;

                    //logger.Info("Cap nhat admin cho truong " + schoolUpdate.SchoolProfileID);

                    //Thuc hien tao nam hoc moi
                    objAcademicYear = lstAcademicYear != null ? lstAcademicYear.FirstOrDefault(s => s.SchoolID == schoolUpdate.SchoolProfileID) : null;
                    if (objAcademicYear != null)
                    {
                        continue;
                    }

                    objAcademicYear = new AcademicYear
                    {
                        CreatedYear = year,
                        Year = year,
                        DisplayTitle = string.Format("{0} - {1}", year, year + 1),
                        FirstSemesterStartDate = new DateTime(year, 8, 15),
                        FirstSemesterEndDate = new DateTime(year, 12, 31),
                        IsActive = true,
                        IsHeadTeacherCanSendSMS = true,
                        SchoolID = schoolUpdate.SchoolProfileID,
                        SynchronizeID = 1,
                        SecondSemesterStartDate = new DateTime(year + 1, 1, 5),
                        SecondSemesterEndDate = new DateTime(year + 1, 5, 31),
                    };

                    context.AcademicYear.Attach(objAcademicYear);
                    context.Entry(objAcademicYear).State = EntityState.Added;
                    //lstAcademicYearInsert.Add(objAcademicYear);

                }
                context.SaveChanges();
                retval = new BoolResultResponse
                {
                    Result = true,
                    Description = "Migrate thông tin trường thành công",
                    ValidationCode = WCFConstant.RESPONSE_SUCCESS
                };
                return retval;
                /*var bulkInsert = AcademicYearBusiness.BulkInsert(constr, lstAcademicYearInsert, mapcolumn.AcademicYear(), "AcademicYearId");
                if (bulkInsert > 0)
                {
                    retval = new BoolResultResponse
                    {
                        Result = true,
                        Description = "Migrate thông tin trường thành công",
                        ValidationCode = WCFConstant.RESPONSE_SUCCESS
                    };
                    return retval;
                }

                return new BoolResultResponse
                {
                    Result = false,
                    Description = "Migrate thông tin trường that bai"
                };*/
                #endregion

            }
            catch (Exception ex)
            {
                LogExtensions.GetCurrentClass = System.Reflection.MethodBase.GetCurrentMethod().DeclaringType;
                string para = string.Format("provinceId={0}, year={1}", provinceId, year);
                LogExtensions.ErrorExt(logger, DateTime.Now, para, ex.Message, "null");
                retval.Result = false;
                retval.ValidationCode = WCFConstant.RESPONSE_EXCEPTION;
                retval.Description = ex.Message;
                return retval;
            }
            finally
            {
                SetAutoDetectChangesEnabled(context, true);
            }
        }

        public BoolResultResponse UpdateSchool(List<MSchoolProfileDefault> lstSchool)
        {
            BoolResultResponse retval = new BoolResultResponse();
            SMASEntities context = DeclareBusiness();
            try
            {
                if (lstSchool == null || lstSchool.Count == 0)
                {
                    retval.Result = false;
                    retval.ValidationCode = WCFConstant.RESPONSE_EXCEPTION;
                    retval.Description = "Không có dữ liệu trường";
                    return retval;
                }
                retval.Result = false;
                SchoolProfile schoolUpdate;
                for (int i = 0; i < lstSchool.Count; i++)
                {
                    schoolUpdate = new SchoolProfile();
                    Utils.Clone(lstSchool[i], schoolUpdate);
                    context.SchoolProfile.Attach(schoolUpdate);
                    context.Entry(schoolUpdate).State = EntityState.Modified;
                }
                context.SaveChanges();
                retval.Result = true;
                retval.Description = "Migrate thông tin trường thành công";
                retval.ValidationCode = WCFConstant.RESPONSE_SUCCESS;
                return retval;
            }
            catch (Exception ex)
            {
                LogExtensions.GetCurrentClass = System.Reflection.MethodBase.GetCurrentMethod().DeclaringType;
                string para = "null";
                LogExtensions.ErrorExt(logger, DateTime.Now, para, ex.Message, "null");
                retval.Result = false;
                retval.ValidationCode = WCFConstant.RESPONSE_EXCEPTION;
                retval.Description = ex.Message;
                return retval;
            }

        }

        public MSchoolProfile GetSchoolProfile(int provinceId, int appliedLevelId)
        {
            MSchoolProfile retval = new MSchoolProfile();
            SMASEntities context = DeclareBusiness();
            try
            {
                var iSchool = context.SchoolProfile.Where(s => s.IsActive && s.ProvinceID == provinceId);
                if (appliedLevelId > 0)
                {
                    List<int> lstApp = UtilsBusiness.GetEducationGrade(appliedLevelId);
                    iSchool = iSchool.Where(s => lstApp.Contains(s.EducationGrade));
                }

                var lstSchool = iSchool.Select(s => new MSchoolProfileDefault
                {
                    Address = s.Address,
                    AdminID = s.AdminID,
                    AreaID = s.AreaID,
                    CancelActiveReason = s.CancelActiveReason,
                    IsActive = s.IsActive,
                    CommuneID = s.CommuneID,
                    CreatedDate = s.CreatedDate,
                    DistrictID = s.DistrictID,
                    EducationGrade = s.EducationGrade,
                    Email = s.Email,
                    EstablishedDate = s.EstablishedDate,
                    Fax = s.Fax,
                    HasSubsidiary = s.HasSubsidiary,
                    HeadMasterName = s.HeadMasterName,
                    HeadMasterPhone = s.HeadMasterPhone,
                    Image = s.Image,
                    InSpecialDifficultZone = s.InSpecialDifficultZone,
                    IsActiveSMAS = s.IsActiveSMAS,
                    IsNewSchoolModel = s.IsNewSchoolModel,
                    ModifiedDate = s.ModifiedDate,
                    MSourcedb = s.MSourcedb,
                    M_OldAdminID = s.M_OldAdminID,
                    M_OldID = s.M_OldID,
                    M_ProvinceID = s.M_ProvinceID,
                    ProductVersion = s.ProductVersion,
                    ProvinceID = s.ProvinceID,
                    ReportTile = s.ReportTile,
                    SchoolCode = s.SchoolCode,
                    SchoolExamCode = s.SchoolExamCode,
                    SchoolName = s.SchoolName,
                    SchoolProfileID = s.SchoolProfileID,
                    SchoolTypeID = s.SchoolTypeID,
                    SchoolYearTitle = s.SchoolYearTitle,
                    ShortName = s.ShortName,
                    SMSParentActiveType = s.SMSParentActiveType,
                    SMSTeacherActiveType = s.SMSTeacherActiveType,
                    SupervisingDeptID = s.SupervisingDeptID,
                    SynchronizeID = s.SynchronizeID,
                    Telephone = s.Telephone,
                    TrainingTypeID = s.TrainingTypeID,
                    Website = s.Website

                }).ToList();
                retval.ListSchoolProfile = lstSchool;
                retval.Susscess = true;
                retval.ValidationCode = WCFConstant.RESPONSE_SUCCESS;
                retval.Message = "Lay thong tin truong thanh cong. Tong so ban ghi :" + lstSchool.Count;
                return retval;

            }
            catch (Exception ex)
            {
                LogExtensions.GetCurrentClass = System.Reflection.MethodBase.GetCurrentMethod().DeclaringType;
                string para = string.Format("provinceId={0}, appliedLevelId={1}", provinceId, appliedLevelId);
                LogExtensions.ErrorExt(logger, DateTime.Now, para, ex.Message, "null");
                retval.ListSchoolProfile = null;
                retval.Susscess = false;
                retval.ValidationCode = WCFConstant.RESPONSE_EXCEPTION;
                retval.Message = ex.Message;
                return retval;
            }
            finally
            {
                SetAutoDetectChangesEnabled(context, true);
            }
        }


        #endregion

        #region danh muc

        public MSubjectCat GetSubject(int appliedLevelId)
        {
            MSubjectCat retval = new MSubjectCat();
            SMASEntities context = DeclareBusiness();
            try
            {
                var iqSubject = context.SubjectCat.Where(s => s.IsActive && (s.AppliedLevel == appliedLevelId || s.AppliedLevel == null));

                var lstSubject = iqSubject.Select(s => new MSubjectCatDefault
                {
                    Abbreviation = s.Abbreviation,
                    AppliedLevel = s.AppliedLevel,
                    ApprenticeshipGroupID = s.ApprenticeshipGroupID,
                    Code = s.Code,
                    Color = s.Color,
                    CreatedDate = s.CreatedDate,
                    Description = s.Description,
                    DisplayName = s.DisplayName,
                    HasPractice = s.HasPractice,
                    IsActive = s.IsActive,
                    IsApprenticeshipSubject = s.IsApprenticeshipSubject,
                    IsCommenting = s.IsCommenting,
                    IsCoreSubject = s.IsCoreSubject,
                    IsEditIsCommentting = s.IsEditIsCommentting,
                    ReadAndWriteMiddleTest = s.ReadAndWriteMiddleTest,
                    IsExemptible = s.IsExemptible,
                    IsForeignLanguage = s.IsForeignLanguage,
                    MiddleSemesterTest = s.MiddleSemesterTest,
                    ModifiedDate = s.ModifiedDate,
                    OrderInSubject = s.OrderInSubject,
                    SubjectCatID = s.SubjectCatID,
                    SubjectName = s.SubjectName,
                    SynchronizeID = s.SynchronizeID
                }).ToList();
                retval.ListSubjectCat = lstSubject;
                retval.Susscess = true;
                retval.ValidationCode = WCFConstant.RESPONSE_SUCCESS;
                retval.Message = "Lay thong tin mon hoc thanh cong. Tong so ban ghi :" + lstSubject.Count;
                return retval;
            }
            catch (Exception ex)
            {
                LogExtensions.GetCurrentClass = System.Reflection.MethodBase.GetCurrentMethod().DeclaringType;
                string para = string.Format("appliedLevelId={0}", appliedLevelId);
                LogExtensions.ErrorExt(logger, DateTime.Now, para, ex.Message, "null");
                retval.ListSubjectCat = null;
                retval.Susscess = false;
                retval.ValidationCode = WCFConstant.RESPONSE_EXCEPTION;
                retval.Message = ex.Message;
                return retval;
            }
            finally
            {
                SetAutoDetectChangesEnabled(context, true);
            }
        }

        /// <summary>
        /// Lay danh sach quan huyen theo tinh thanh
        /// </summary>
        /// <param name="provinceId"></param>
        /// <returns></returns>
        public List<MDistrict> GetDistrict(int provinceId)
        {
            SMASEntities context = DeclareBusiness();
            try
            {

                var lstDistrict = context.District
                                    .Where(d => d.ProvinceID == provinceId)
                                    .Select(d => new MDistrict
                                    {
                                        DistrictId = d.DistrictID,
                                        DistrictName = d.DistrictName,
                                        OldId = d.M_OldID ?? 0
                                    }).ToList();
                return lstDistrict;
            }
            catch (Exception ex)
            {
                LogExtensions.GetCurrentClass = System.Reflection.MethodBase.GetCurrentMethod().DeclaringType;
                string para = string.Format("provinceId={0}", provinceId);
                LogExtensions.ErrorExt(logger, DateTime.Now, para, ex.Message, "null");
                return new List<MDistrict>();

            }
            finally
            {
                SetAutoDetectChangesEnabled(context, true);
            }
        }

        /// <summary>
        /// Danh sach don vi quan ly
        /// </summary>
        /// <param name="provinceId"></param>
        /// <returns></returns>
        public List<MSupervisingDept> GetSupersingDept(int provinceId)
        {
            SMASEntities context = DeclareBusiness();
            try
            {

                var lstSuper = context.SupervisingDept
                                    .Where(d => d.ProvinceID == provinceId)
                                    .Select(d => new MSupervisingDept
                                    {
                                        SupervisingDeptId = d.SupervisingDeptID,
                                        SupervisingDeptName = d.SupervisingDeptName,
                                        OldId = d.M_OldID ?? 0
                                    }).ToList();
                return lstSuper;
            }
            catch (Exception ex)
            {

                LogExtensions.GetCurrentClass = System.Reflection.MethodBase.GetCurrentMethod().DeclaringType;
                string para = string.Format("provinceId={0}", provinceId);
                LogExtensions.ErrorExt(logger, DateTime.Now, para, ex.Message, "null");
                return new List<MSupervisingDept>();

            }
            finally
            {
                SetAutoDetectChangesEnabled(context, true);
            }
        }



        /// <summary>
        /// Lay danh sach quan huyen theo tinh thanh
        /// </summary>
        /// <param name="provinceId"></param>
        /// <returns></returns>
        public List<MEthnic> GetEthnic()
        {
            SMASEntities context = DeclareBusiness();
            try
            {

                var lstDistrict = context.Ethnic
                                    .Where(d => d.IsActive)
                                    .Select(d => new MEthnic
                                    {
                                        CreatedDate = d.CreatedDate,
                                        Description = d.Description,
                                        EthnicCode = d.EthnicCode,
                                        EthnicID = d.EthnicID,
                                        EthnicName = d.EthnicName,
                                        IsActive = d.IsActive,
                                        IsMinority = d.IsMinority,
                                        ModifiedDate = d.ModifiedDate,
                                        SynchronizeID = d.SynchronizeID
                                    }).ToList();
                return lstDistrict;
            }
            catch (Exception ex)
            {
                LogExtensions.GetCurrentClass = System.Reflection.MethodBase.GetCurrentMethod().DeclaringType;
                string para = "null";
                LogExtensions.ErrorExt(logger, DateTime.Now, para, ex.Message, "null");
                return new List<MEthnic>();

            }
            finally
            {
                SetAutoDetectChangesEnabled(context, true);
            }
        }

        public List<MWorkType> GetWorkType()
        {
            SMASEntities context = DeclareBusiness();
            try
            {

                var lstWorkType = context.WorkType
                                    .Where(d => d.IsActive)
                                    .Select(d => new MWorkType
                                    {
                                        Resolution = d.Resolution,
                                        WorkGroupTypeID = d.WorkGroupTypeID,
                                        WorkTypeID = d.WorkTypeID
                                    }).ToList();
                return lstWorkType;
            }
            catch (Exception ex)
            {
                LogExtensions.GetCurrentClass = System.Reflection.MethodBase.GetCurrentMethod().DeclaringType;
                string para = "null";
                LogExtensions.ErrorExt(logger, DateTime.Now, para, ex.Message, "null");
                return new List<MWorkType>();

            }
            finally
            {
                SetAutoDetectChangesEnabled(context, true);
            }
        }

        #endregion

        #region Lop hoc
        public MClassProfile GetClassProfile(int academicYearId)
        {
            MClassProfile retval = new MClassProfile();
            SMASEntities context = DeclareBusiness();
            try
            {
                var iqClassProfile = context.ClassProfile.Where(c => c.IsActive == true && c.AcademicYearID == academicYearId);

                var lstClassProfile = iqClassProfile.Select(c => new MClassProfileDafault
                {
                    ClassProfileID = c.ClassProfileID,
                    AcademicYearID = c.AcademicYearID,
                    SchoolID = c.SchoolID,
                    SchoolSubsidiaryID = c.SchoolSubsidiaryID,
                    IsCombinedClass = c.IsCombinedClass,
                    CombinedClassCode = c.CombinedClassCode,
                    SubCommitteeID = c.SubCommitteeID,
                    HeadTeacherID = c.HeadTeacherID,
                    EducationLevelID = c.EducationLevelID,
                    DisplayName = c.DisplayName,
                    Description = c.Description,
                    StartDate = c.StartDate,
                    EndDate = c.EndDate,
                    IsSpecializedClass = c.IsSpecializedClass,
                    FirstForeignLanguageID = c.FirstForeignLanguageID,
                    SecondForeignLanguageID = c.SecondForeignLanguageID,
                    ApprenticeshipGroupID = c.ApprenticeshipGroupID,
                    Section = c.Section,
                    IsITClass = c.IsITClass,
                    M_ProvinceID = c.M_ProvinceID,
                    M_OldID = c.M_OldID,
                    MSourcedb = c.MSourcedb,
                    SynchronizeID = c.SynchronizeID,
                    OrderNumber = c.OrderNumber,
                    VemisCode = c.VemisCode,
                    IsVnenClass = c.IsVnenClass,
                    SeperateKey = c.SeperateKey,
                    IsActive = c.IsActive

                }).ToList();
                retval.ListClassProfile = lstClassProfile;
                retval.Susscess = true;
                retval.ValidationCode = WCFConstant.RESPONSE_SUCCESS;
                retval.Message = "Lay thong tin lop hoc thanh cong. Tong so ban ghi :" + lstClassProfile.Count;
                return retval;
            }
            catch (Exception ex)
            {

                LogExtensions.GetCurrentClass = System.Reflection.MethodBase.GetCurrentMethod().DeclaringType;
                string para = string.Format("academicYearId={0}", academicYearId);
                LogExtensions.ErrorExt(logger, DateTime.Now, para, ex.Message, "null");
                retval.ListClassProfile = null;
                retval.Susscess = false;
                retval.ValidationCode = WCFConstant.RESPONSE_EXCEPTION;
                retval.Message = ex.Message;
                return retval;
            }
            finally
            {
                SetAutoDetectChangesEnabled(context, true);
            }
        }

        public BoolResultResponse ClassInsert(List<MClassProfileDafault> lstClassProfile)
        {
            BoolResultResponse retval = new BoolResultResponse();
            var context = DeclareBusiness();
            try
            {

                if (lstClassProfile == null || lstClassProfile.Count == 0)
                {
                    retval.Result = true;
                    retval.ValidationCode = WCFConstant.RESPONSE_EXCEPTION;
                    retval.Description = "Không có dữ liệu lop hoc";
                    return retval;
                }


                retval.Result = false;
                //SchoolProfile schoolInsert;
                List<ClassProfile> lstClassInsert = new List<ClassProfile>();
                lstClassInsert = lstClassProfile.Select(c => new ClassProfile
                {
                    AcademicYearID = c.AcademicYearID,
                    SchoolID = c.SchoolID,
                    SchoolSubsidiaryID = c.SchoolSubsidiaryID,
                    IsCombinedClass = c.IsCombinedClass,
                    CombinedClassCode = c.CombinedClassCode,
                    SubCommitteeID = c.SubCommitteeID,
                    HeadTeacherID = c.HeadTeacherID,
                    EducationLevelID = c.EducationLevelID,
                    DisplayName = c.DisplayName,
                    Description = c.Description,
                    StartDate = c.StartDate,
                    EndDate = c.EndDate,
                    IsSpecializedClass = c.IsSpecializedClass,
                    FirstForeignLanguageID = c.FirstForeignLanguageID,
                    SecondForeignLanguageID = c.SecondForeignLanguageID,
                    ApprenticeshipGroupID = c.ApprenticeshipGroupID,
                    Section = c.Section,
                    IsITClass = c.IsITClass,
                    M_ProvinceID = c.M_ProvinceID,
                    M_OldID = c.M_OldID,
                    MSourcedb = c.MSourcedb,
                    SynchronizeID = c.SynchronizeID,
                    OrderNumber = c.OrderNumber,
                    VemisCode = c.VemisCode,
                    IsVnenClass = c.IsVnenClass,
                    SeperateKey = c.SeperateKey,
                    IsActive = c.IsActive,

                }).ToList();

                var map = new ColumnMapping();
                var ret1 = ClassProfileBusiness.BulkInsert(WCFUtils.GetConnectionString, lstClassInsert, map.ClassProfile(), "ClassProfileID");
                if (ret1 > 0)
                {
                    retval.Result = true;
                    retval.Description = "Them moi lop hoc thanh cong";
                    retval.ValidationCode = WCFConstant.RESPONSE_SUCCESS;
                    return retval;
                }
                retval.Result = false;
                retval.Description = "Them moi lop hoc khong thanh cong";
                retval.ValidationCode = 0;
                return retval;
            }
            catch (Exception ex)
            {
                LogExtensions.GetCurrentClass = System.Reflection.MethodBase.GetCurrentMethod().DeclaringType;
                string para = "null";
                LogExtensions.ErrorExt(logger, DateTime.Now, para, ex.Message, "null");
                retval.Result = false;
                retval.ValidationCode = WCFConstant.RESPONSE_EXCEPTION;
                retval.Description = ex.Message;
                return retval;
            }
            finally
            {
                SetAutoDetectChangesEnabled(context, true);
            }
        }


        public MSchoolSubject GetSchoolSubject(int academicYearId, int schoolId)
        {
            MSchoolSubject retval = new MSchoolSubject();
            SMASEntities context = DeclareBusiness();
            try
            {
                var iqSchoolSubject = context.SchoolSubject.Where(c => c.AcademicYearID == academicYearId && c.SchoolID == schoolId);

                var lstSchoolSubject = iqSchoolSubject.Select(c => new MSchoolSubjectDefault
                {
                    AcademicYearID = c.AcademicYearID,
                    AppliedType = c.AppliedType,
                    CreatedAcademicYear = c.CreatedAcademicYear,
                    EducationLevelID = c.EducationLevelID,
                    EndDate = c.EndDate,
                    FirstSemesterCoefficient = c.FirstSemesterCoefficient,
                    IsCommenting = c.IsCommenting,
                    Last2digitNumberSchool = c.Last2digitNumberSchool,
                    MSourcedb = c.MSourcedb,
                    M_OldID = c.M_OldID,
                    M_ProvinceID = c.M_ProvinceID,
                    SchoolID = c.SchoolID,
                    SchoolSubjectID = c.SchoolSubjectID,
                    SecondSemesterCoefficient = c.SecondSemesterCoefficient,
                    SectionPerWeekFirstSemester = c.SectionPerWeekFirstSemester,
                    SectionPerWeekSecondSemester = c.SectionPerWeekSecondSemester,
                    StartDate = c.StartDate,
                    SubjectID = c.SubjectID,
                    SynchronizeID = c.SynchronizeID
                }).ToList();
                retval.ListSchoolSubject = lstSchoolSubject;
                retval.Susscess = true;
                retval.Message = "Lay thong tin mon hoc cho truong thanh cong. Tong so ban ghi :" + lstSchoolSubject.Count;
                return retval;
            }
            catch (Exception ex)
            {

                LogExtensions.GetCurrentClass = System.Reflection.MethodBase.GetCurrentMethod().DeclaringType;
                string para = string.Format("academicYearId={0}, schoolId={1}", academicYearId, schoolId);
                LogExtensions.ErrorExt(logger, DateTime.Now, para, ex.Message, "null");
                retval.ListSchoolSubject = new List<MSchoolSubjectDefault>();
                retval.Susscess = false;
                retval.Message = ex.Message;
                return retval;
            }
            finally
            {
                SetAutoDetectChangesEnabled(context, true);
            }
        }

        public BoolResultResponse InsertSchoolSubject(List<MSchoolSubjectDefault> lstSchoolSubject)
        {
            BoolResultResponse retval = new BoolResultResponse();
            SMASEntities context = DeclareBusiness();
            try
            {

                if (lstSchoolSubject == null || lstSchoolSubject.Count == 0)
                {
                    retval.Result = true;
                    retval.ValidationCode = WCFConstant.RESPONSE_EXCEPTION;
                    retval.Description = "Không có dữ liệu mon hoc cho truong";
                    return retval;
                }


                retval.Result = false;
                //SchoolProfile schoolInsert;
                List<SchoolSubject> lstInsertSchoolSubject = new List<SchoolSubject>();
                lstInsertSchoolSubject = lstSchoolSubject.Select(c => new SchoolSubject
                {
                    AcademicYearID = c.AcademicYearID,
                    AppliedType = c.AppliedType,
                    CreatedAcademicYear = c.CreatedAcademicYear,
                    EducationLevelID = c.EducationLevelID,
                    EndDate = c.EndDate,
                    FirstSemesterCoefficient = c.FirstSemesterCoefficient,
                    IsCommenting = c.IsCommenting,
                    Last2digitNumberSchool = c.Last2digitNumberSchool,
                    MSourcedb = c.MSourcedb,
                    M_OldID = c.M_OldID,
                    M_ProvinceID = c.M_ProvinceID,
                    SchoolID = c.SchoolID,
                    SecondSemesterCoefficient = c.SecondSemesterCoefficient,
                    SectionPerWeekFirstSemester = c.SectionPerWeekFirstSemester,
                    SectionPerWeekSecondSemester = c.SectionPerWeekSecondSemester,
                    StartDate = c.StartDate,
                    SubjectID = c.SubjectID,
                    SynchronizeID = c.SynchronizeID
                }).ToList();

                var map = new ColumnMapping();
                var ret1 = SchoolSubjectBusiness.BulkInsert(WCFUtils.GetConnectionString, lstInsertSchoolSubject, map.SchoolSubject(), "SchoolSubjectID");
                if (ret1 > 0)
                {
                    retval.Result = true;
                    retval.Description = "Them moi mon hoc cho truong thanh cong";
                    retval.ValidationCode = WCFConstant.RESPONSE_SUCCESS;
                    return retval;
                }
                retval.Result = false;
                retval.Description = "Them moi mon hoc cho truong khong thanh cong";
                retval.ValidationCode = 0;
                return retval;
            }
            catch (Exception ex)
            {
                LogExtensions.GetCurrentClass = System.Reflection.MethodBase.GetCurrentMethod().DeclaringType;
                string para = "null";
                LogExtensions.ErrorExt(logger, DateTime.Now, para, ex.Message, "null");
                retval.Result = false;
                retval.ValidationCode = WCFConstant.RESPONSE_EXCEPTION;
                retval.Description = ex.Message;
                return retval;
            }
            finally
            {
                SetAutoDetectChangesEnabled(context, true);
            }
        }


        public MClassSubject GetClassSubject(int academicYearId, int schoolId)
        {
            MClassSubject retval = new MClassSubject();
            SMASEntities context = DeclareBusiness();
            try
            {
                int partitionId = UtilsBusiness.GetPartionId(schoolId);
                var iqClassSubject = from cs in context.ClassSubject
                                     join cp in context.ClassProfile on cs.ClassID equals cp.ClassProfileID
                                     where cp.AcademicYearID == academicYearId
                                     && cs.Last2digitNumberSchool == partitionId
                                     && cp.IsActive == true
                                     select new MClassSubjectDefault
                                     {
                                         AppliedType = cs.AppliedType,
                                         ClassID = cs.ClassID,
                                         CreatedAcademicYear = cs.CreatedAcademicYear,
                                         EndDate = cs.EndDate,
                                         FirstSemesterCoefficient = cs.FirstSemesterCoefficient,
                                         IsCommenting = cs.IsCommenting,
                                         IsSecondForeignLanguage = cs.IsSecondForeignLanguage,
                                         IsSpecializedSubject = cs.IsSpecializedSubject,
                                         IsSubjectVNEN = cs.IsSubjectVNEN,
                                         Last2digitNumberSchool = cs.Last2digitNumberSchool,
                                         MMinFirstSemester = cs.MMinFirstSemester,
                                         MMinSecondSemester = cs.MMinSecondSemester,
                                         MSourcedb = cs.MSourcedb,
                                         M_OldID = cs.M_OldID,
                                         M_ProvinceID = cs.M_ProvinceID,
                                         PMinFirstSemester = cs.PMinFirstSemester,
                                         PMinSecondSemester = cs.PMinSecondSemester,
                                         SecondSemesterCoefficient = cs.SecondSemesterCoefficient,
                                         OrderInSchoolReport = cs.OrderInSchoolReport,
                                         SectionPerWeekFirstSemester = cs.SectionPerWeekFirstSemester,
                                         SectionPerWeekSecondSemester = cs.SectionPerWeekSecondSemester,
                                         StartDate = cs.StartDate,
                                         SubjectID = cs.SubjectID,
                                         SubjectIDIncrease = cs.SubjectIDIncrease,
                                         VMinFirstSemester = cs.VMinFirstSemester,
                                         VMinSecondSemester = cs.VMinSecondSemester,
                                         SynchronizeID = cs.SynchronizeID,
                                     };

                var lstClassSubject = iqClassSubject.ToList();
                retval.ListClassSubject = lstClassSubject;
                retval.Susscess = true;
                retval.Message = "Lay thong tin mon hoc cho lop thanh cong. Tong so ban ghi :" + lstClassSubject.Count;
                return retval;
            }
            catch (Exception ex)
            {
                LogExtensions.GetCurrentClass = System.Reflection.MethodBase.GetCurrentMethod().DeclaringType;
                string para = string.Format("academicYearId={0}, schoolId={1}", academicYearId, schoolId);
                LogExtensions.ErrorExt(logger, DateTime.Now, para, ex.Message, "null");
                retval.ListClassSubject = new List<MClassSubjectDefault>();
                retval.Susscess = false;
                retval.Message = ex.Message;
                return retval;
            }
            finally
            {
                SetAutoDetectChangesEnabled(context, true);
            }
        }

        public BoolResultResponse InsertClassSubject(List<MClassSubjectDefault> lstClassSubject)
        {
            BoolResultResponse retval = new BoolResultResponse();
            SMASEntities context = DeclareBusiness();
            try
            {

                if (lstClassSubject == null || lstClassSubject.Count == 0)
                {
                    retval.Result = true;
                    retval.ValidationCode = WCFConstant.RESPONSE_EXCEPTION;
                    retval.Description = "Không có dữ liệu mon hoc cho truong";
                    return retval;
                }

                retval.Result = false;
                List<ClassSubject> lstInsertClassSubject = new List<ClassSubject>();
                lstInsertClassSubject = lstClassSubject.Select(c => new ClassSubject
                {
                    AppliedType = c.AppliedType,
                    ClassID = c.ClassID,
                    CreatedAcademicYear = c.CreatedAcademicYear,
                    EndDate = c.EndDate,
                    FirstSemesterCoefficient = c.FirstSemesterCoefficient,
                    IsCommenting = c.IsCommenting,
                    IsSecondForeignLanguage = c.IsSecondForeignLanguage,
                    IsSpecializedSubject = c.IsSpecializedSubject,
                    IsSubjectVNEN = c.IsSubjectVNEN,
                    Last2digitNumberSchool = c.Last2digitNumberSchool,
                    MMinFirstSemester = c.MMinFirstSemester,
                    MMinSecondSemester = c.MMinSecondSemester,
                    MSourcedb = c.MSourcedb,
                    M_OldID = c.M_OldID,
                    M_ProvinceID = c.M_ProvinceID,
                    PMinFirstSemester = c.PMinFirstSemester,
                    PMinSecondSemester = c.PMinSecondSemester,
                    SecondSemesterCoefficient = c.SecondSemesterCoefficient,
                    OrderInSchoolReport = c.OrderInSchoolReport,
                    SectionPerWeekFirstSemester = c.SectionPerWeekFirstSemester,
                    SectionPerWeekSecondSemester = c.SectionPerWeekSecondSemester,
                    StartDate = c.StartDate,
                    SubjectID = c.SubjectID,
                    SubjectIDIncrease = c.SubjectIDIncrease,
                    VMinFirstSemester = c.VMinFirstSemester,
                    VMinSecondSemester = c.VMinSecondSemester,
                    SynchronizeID = c.SynchronizeID,
                }).ToList();

                var map = new ColumnMapping();
                var ret1 = ClassSubjectBusiness.BulkInsert(WCFUtils.GetConnectionString, lstInsertClassSubject, map.ClassSubject(), "ClassSubjectID");
                if (ret1 > 0)
                {
                    retval.Result = true;
                    retval.Description = "Them moi mon hoc cho lop thanh cong";
                    retval.ValidationCode = WCFConstant.RESPONSE_SUCCESS;
                    return retval;
                }
                retval.Result = false;
                retval.Description = "Them moi mon hoc cho lop khong thanh cong";
                retval.ValidationCode = 0;
                return retval;
            }
            catch (Exception ex)
            {
                LogExtensions.GetCurrentClass = System.Reflection.MethodBase.GetCurrentMethod().DeclaringType;
                string para = "null";
                LogExtensions.ErrorExt(logger, DateTime.Now, para, ex.Message, "null");
                retval.Result = false;
                retval.ValidationCode = WCFConstant.RESPONSE_EXCEPTION;
                retval.Description = ex.Message;
                return retval;
            }
            finally
            {
                SetAutoDetectChangesEnabled(context, true);
            }
        }



        #endregion


        #region Membership
        public bool UpdateHasPassword(string userName, int pagesize)
        {
            var context = DeclareBusiness();
            try
            {

                MembershipProvider hashedProvider = (MembershipProvider)Membership.Providers["OracleMembershipProvider"];
                MembershipProvider encryptedProvider = (MembershipProvider)Membership.Providers["OracleEncryptedProvider"];

                if (hashedProvider == null || encryptedProvider == null)
                {
                    return false;
                }
                List<string> lstNotUser = new List<string>();
                lstNotUser.Add("nan_thcs_nghiaduc");
                lstNotUser.Add("sla_ttgdtx_msn");
                lstNotUser.Add("nan_thcs_nghialam");
                lstNotUser.Add("tnh_th_thitran");
                lstNotUser.Add("hgg_th_xinman");
                var iqMembership = from m in context.aspnet_Membership.Where(s => s.PasswordFormat == 2 && s.IsApproved == 1)
                                   join u in context.aspnet_Users on m.UserId equals u.UserId
                                   where !u.UserName.Contains("_delete")
                                   && !lstNotUser.Contains(u.UserName)
                                   && m.IsLockedOut == 0
                                   select new
                                   {
                                       UserName = u.UserName.Trim(),
                                       UserId = u.UserId
                                   };
                if (!string.IsNullOrEmpty(userName))
                {
                    iqMembership = iqMembership.Where(m => m.UserName.ToLower() == userName.Trim().ToLower());
                }
                iqMembership = iqMembership.Take(pagesize);
                var mem = iqMembership.ToList();
                if (mem == null || mem.Count == 0)
                {
                    return false;
                }

                Dictionary<string, object> dicPass = new Dictionary<string, object>();
                foreach (var m in mem)
                {
                    var user = context.aspnet_Membership.Find(m.UserId);
                    if (user == null)
                    {
                        continue;
                    }
                    try
                    {
                        //Get pass
                        MembershipUser user2 = encryptedProvider.GetUser(m.UserName, false);
                        dicPass.Add(m.UserId.ToString(), user2.GetPassword());

                        user.PasswordFormat = 1;
                        context.aspnet_Membership.Attach(user);
                        context.Entry(user).State = EntityState.Modified;
                    }
                    catch (Exception ex)
                    {
                        logger.Info("Khong lay duoc passs UserName=" + m.UserName);
                    }

                }
                context.SaveChanges();

                bool retval = false;
                string pass = "";
                foreach (var m in mem)
                {
                    pass = dicPass[m.UserId.ToString()].ToString();
                    if (string.IsNullOrEmpty(pass))
                    {
                        continue;
                    }
                    try
                    {
                        MembershipUser user = Membership.GetUser(m.UserName, false);
                        string pasreset = user.ResetPassword();
                        retval = user.ChangePassword(pasreset, pass);

                    }
                    catch (Exception ex)
                    {
                        LogExtensions.GetCurrentClass = System.Reflection.MethodBase.GetCurrentMethod().DeclaringType;
                        string para = string.Format("userName={0}, pagesize={1}", userName, pagesize);
                        LogExtensions.ErrorExt(logger, DateTime.Now, para, ex.Message, "null");
                        retval = false;
                    }

                    //retval = hashedProvider.ChangePassword(m.UserName, pass, pass);


                }
                return retval;
            }
            catch (Exception ex)
            {

                LogExtensions.GetCurrentClass = System.Reflection.MethodBase.GetCurrentMethod().DeclaringType;
                string para = string.Format("userName={0}, pagesize={1}", userName, pagesize);
                LogExtensions.ErrorExt(logger, DateTime.Now, para, ex.Message, "null");
                return false;
            }
            finally
            {
                SetAutoDetectChangesEnabled(context, true);
            }
        }
        #endregion



        #region Sync HCM
        public TransSchoolProfileResponse GetSyncSchoolProfile(SyncAcademicYear form)
        {
            TransSchoolProfileResponse retVal = new TransSchoolProfileResponse();
            SMASEntities context = DeclareBusiness();
            try
            {
                SetAutoDetectChangesEnabled(context, false);
                IQueryable<SchoolProfile> iqSchool = context.SchoolProfile;
                iqSchool = iqSchool.Where(s => s.IsActive && s.ProvinceID == form.ProvinceId);
                if (form.LstEducationGradeId != null && form.LstEducationGradeId.Count > 0)
                {
                    iqSchool = iqSchool.Where(s => form.LstEducationGradeId.Contains(s.EducationGrade));
                }
                string formatDate = "dd/MM/yyyy";
                String modifyDateVN = DateTime.Now.ToString(formatDate);

                /*iqSchool = iqSchool.Where(s => (!s.SynchronizeID.HasValue || s.SynchronizeID == 0)
                                                || (s.SynchronizeID > 0 && s.ModifiedDate >= form.ModifyDate && !form.ListModifyDate.Contains(s.MSourcedb))

                                         );*/
                iqSchool = iqSchool.Where(s => (string.IsNullOrEmpty(s.MSourcedb) || !s.MSourcedb.Contains(form.SourceDB)) ||
                                               (s.MSourcedb.Contains(form.SourceDB) && s.ModifiedDate >= form.ModifyDate)
                                          );

                iqSchool = iqSchool.Take(form.RecordNumber);
                var iProvince = context.Province.AsNoTracking().Where(p => p.ProvinceID == form.ProvinceId);
                var iDistrict = context.District.AsNoTracking().Where(d => d.ProvinceID == form.ProvinceId);
                var iSuperDept = context.SupervisingDept.AsNoTracking().Where(s => s.ProvinceID == form.ProvinceId);
                var iArea = context.Area.AsNoTracking();
                var lstSchool = (from sp in iqSchool
                                 join p in iProvince on sp.ProvinceID equals p.ProvinceID
                                 join d in iDistrict on sp.DistrictID equals d.DistrictID
                                 join super in iSuperDept on sp.SupervisingDeptID equals super.SupervisingDeptID
                                 join ar in iArea on sp.AreaID equals ar.AreaID
                                 select new TransSchoolProfileDefault
                                 {
                                     Address = sp.Address,
                                     AdminID = sp.AdminID,
                                     AreaID = sp.AreaID,
                                     CancelActiveReason = sp.CancelActiveReason,
                                     CommuneID = sp.CommuneID,
                                     CreatedDate = sp.CreatedDate,
                                     DistrictID = sp.DistrictID,
                                     EducationGrade = sp.EducationGrade,
                                     Email = sp.Email,
                                     Area = ar.AreaName,
                                     Commue = "",
                                     District = d.DistrictName,
                                     Fax = sp.Fax,
                                     EstablishedDate = sp.EstablishedDate,
                                     HeadMasterName = sp.HeadMasterName,
                                     HeadMasterPhone = sp.HeadMasterPhone,
                                     Province = p.ProvinceName,
                                     SchoolCode = sp.SchoolCode,
                                     SchoolName = sp.SchoolName,
                                     SchoolProfileID = sp.SchoolProfileID,
                                     SchoolTypeID = sp.SchoolTypeID ?? 0,
                                     SupervisingDept = super.SupervisingDeptName,
                                     Telephone = sp.Telephone,
                                     TrainingTypeID = sp.TrainingTypeID ?? 0,
                                     Website = sp.Website,
                                     HasSubsidiary = sp.HasSubsidiary,
                                     InSpecialDifficultZone = sp.InSpecialDifficultZone,
                                     IsActive = sp.IsActive,
                                     IsActiveSMAS = sp.IsActiveSMAS,
                                     IsNewSchoolModel = sp.IsNewSchoolModel,
                                     ModifiedDate = sp.ModifiedDate,
                                     ProductVersion = sp.ProductVersion,
                                     ProvinceID = sp.ProvinceID,
                                     ReportTile = sp.ReportTile,
                                     SchoolExamCode = sp.SchoolExamCode,
                                     SchoolYearTitle = sp.SchoolYearTitle,
                                     ShortName = sp.ShortName,
                                     SMSParentActiveType = sp.SMSParentActiveType,
                                     SMSTeacherActiveType = sp.SMSTeacherActiveType,
                                     SupervisingDeptID = sp.SupervisingDeptID,
                                     SynchronizeID = sp.SynchronizeID,
                                     MSource = sp.MSourcedb

                                 }).ToList();
                if (!lstSchool.Any())
                {
                    retVal.Message = "Khong co du lieu thay doi de dong bo bang school";
                    retVal.Susscess = false;
                    retVal.ValidationCode = WCFConstant.RESPONSE_CLASSID_NOT_FOUND;
                    retVal.ListSchoolProfile = new List<TransSchoolProfileDefault>();
                    return retVal;
                }
                retVal.Message = "Thành công";
                retVal.Susscess = true;
                retVal.ValidationCode = WCFConstant.RESPONSE_SUCCESS;
                retVal.ListSchoolProfile = lstSchool;
                return retVal;
            }
            catch (Exception ex)
            {
                LogExtensions.GetCurrentClass = System.Reflection.MethodBase.GetCurrentMethod().DeclaringType;
                string para = string.Format("academicYearId={0}, schoolId={1}", form.AcademicYearId, form.SchoolId);
                LogExtensions.ErrorExt(logger, DateTime.Now, para, ex.Message, "null");
                retVal.Susscess = false;
                retVal.ValidationCode = WCFConstant.RESPONSE_EXCEPTION;
                retVal.Message = ex.StackTrace;
                return retVal;
            }
            finally
            {
                SetAutoDetectChangesEnabled(context, true);
            }

        }

        public BoolResultResponse UpdateSyncSchool(List<int> lstSchool, string sourceDB)
        {
            BoolResultResponse retval = new BoolResultResponse();
            SMASEntities context = DeclareBusiness();
            try
            {
                if (lstSchool == null || lstSchool.Count == 0)
                {
                    retval.Result = false;
                    return retval;
                }
                SetAutoDetectChangesEnabled(context, false);
                List<SchoolProfile> lstSchoolUpdate = SchoolProfileBusiness.All.Where(c => lstSchool.Contains(c.SchoolProfileID)).ToList();
                for (int i = 0; i < lstSchoolUpdate.Count; i++)
                {
                    SchoolProfile spUpdate = lstSchoolUpdate[i];
                    string sourcedb = UtilsBusiness.CreateSourceDB(spUpdate.MSourcedb, sourceDB);
                    spUpdate.SynchronizeID = 1;
                    spUpdate.MSourcedb = sourcedb;
                    context.SchoolProfile.Attach(spUpdate);
                    context.Entry(spUpdate).State = EntityState.Modified;
                }
                context.SaveChanges();
                retval.Result = true;
                retval.ValidationCode = WCFConstant.RESPONSE_SUCCESS;

                return retval;
            }
            catch (Exception ex)
            {
                LogExtensions.GetCurrentClass = System.Reflection.MethodBase.GetCurrentMethod().DeclaringType;
                string para = string.Format("sourceDB={0}", sourceDB);
                LogExtensions.ErrorExt(logger, DateTime.Now, para, ex.Message, "null");
                retval.Result = false;
                retval.ValidationCode = WCFConstant.RESPONSE_EXCEPTION;

                return retval;
            }
            finally
            {
                SetAutoDetectChangesEnabled(context, true);
            }
        }

        public TransSubjectCatResponse GetSyncSubjectCat(SyncAcademicYear form)
        {
            SMASEntities context = DeclareBusiness();
            TransSubjectCatResponse retSubject = new TransSubjectCatResponse();
            retSubject.Susscess = false;
            retSubject.ListSubjectCat = new List<TransSubjectCatDefault>();
            retSubject.ValidationCode = 0;
            try
            {
                SetAutoDetectChangesEnabled(context, false);

                IQueryable<SubjectCat> iqSubject = context.SubjectCat.AsNoTracking();
                iqSubject = iqSubject.Where(s => s.IsActive && (!s.SynchronizeID.HasValue || s.SynchronizeID == 0 || s.ModifiedDate >= form.ModifyDate));
                iqSubject = iqSubject.Where(s => s.SubjectCatID > 0);
                iqSubject = iqSubject.Take(form.RecordNumber);

                var lstSubject = (from s in iqSubject
                                  select new TransSubjectCatDefault
                                  {
                                      Abbreviation = s.Abbreviation,
                                      AppliedLevel = s.AppliedLevel,
                                      ApprenticeshipGroupID = s.ApprenticeshipGroupID,
                                      Description = s.Description,
                                      DisplayName = s.DisplayName,
                                      HasPractice = s.HasPractice,
                                      IsApprenticeshipSubject = s.IsApprenticeshipSubject,
                                      IsCommenting = s.IsCommenting,
                                      IsEditIsCommentting = s.IsEditIsCommentting,
                                      IsExemptible = s.IsExemptible,
                                      IsForeignLanguage = s.IsForeignLanguage,
                                      SubjectCatId = s.SubjectCatID,
                                      SubjectName = s.SubjectName,
                                      SynchronizeID = s.SynchronizeID,
                                      ModifiedDate = s.ModifiedDate
                                  }).ToList();
                retSubject.ListSubjectCat = lstSubject;
                retSubject.ValidationCode = WCFConstant.RESPONSE_SUCCESS;
                retSubject.Susscess = true;
                return retSubject;
            }
            catch (Exception ex)
            {
                LogExtensions.GetCurrentClass = System.Reflection.MethodBase.GetCurrentMethod().DeclaringType;
                string para = string.Format("academicYearId={0}, schoolId={1}", form.AcademicYearId, form.SchoolId);
                LogExtensions.ErrorExt(logger, DateTime.Now, para, ex.Message, "null");
                retSubject.MessageReponse = ex.StackTrace;
                retSubject.ValidationCode = WCFConstant.RESPONSE_EXCEPTION;
                return retSubject;
            }
            finally
            {
                SetAutoDetectChangesEnabled(context, true);
            }
        }

        public BoolResultResponse UpdateSyncSubjectCat(List<int> lstSubjectCat)
        {
            SMASEntities context = DeclareBusiness();
            BoolResultResponse retResulst = new BoolResultResponse();
            retResulst.Result = true;
            retResulst.ValidationCode = WCFConstant.RESPONSE_SUCCESS;
            try
            {

                if (lstSubjectCat == null || lstSubjectCat.Count == 0)
                {
                    retResulst.Result = false;
                    return retResulst;
                }
                SetAutoDetectChangesEnabled(context, false);
                List<SubjectCat> lstSubjectUpdate = context.SubjectCat.Where(c => lstSubjectCat.Contains(c.SubjectCatID)).ToList();
                for (int i = 0; i < lstSubjectUpdate.Count; i++)
                {
                    SubjectCat subjectUpdate = lstSubjectUpdate[i];
                    subjectUpdate.SynchronizeID = 1;
                    context.SubjectCat.Attach(subjectUpdate);
                    context.Entry(subjectUpdate).State = EntityState.Modified;
                }
                context.SaveChanges();
                return retResulst;

            }
            catch (Exception ex)
            {

                LogExtensions.GetCurrentClass = System.Reflection.MethodBase.GetCurrentMethod().DeclaringType;
                string para = "null";
                LogExtensions.ErrorExt(logger, DateTime.Now, para, ex.Message, "null");
                retResulst.Result = false;
                retResulst.ValidationCode = WCFConstant.RESPONSE_EXCEPTION;
                retResulst.Description = ex.Message;
                return retResulst;
            }
            finally
            {
                SetAutoDetectChangesEnabled(context, true);
            }
        }

        public TransClassProfileResponse GetSyncClassProfile(SyncAcademicYear form)
        {
            SMASEntities context = DeclareBusiness();
            TransClassProfileResponse retResult = new TransClassProfileResponse();
            retResult.Susscess = false;
            retResult.ListClassProfile = new List<TransClassProfileDefault>();
            retResult.ValidationCode = WCFConstant.RESPONSE_CLASSID_NOT_FOUND;
            try
            {
                SetAutoDetectChangesEnabled(context, false);
                var iQClass = from cp in context.ClassProfile.AsNoTracking()
                              where cp.AcademicYearID == form.AcademicYearId
                              select cp;
                iQClass = iQClass.Where(s => (string.IsNullOrEmpty(s.MSourcedb) || !s.MSourcedb.Contains(form.SourceDB)) ||
                                              (s.MSourcedb.Contains(form.SourceDB)) && s.IsActive == false);
                //iQClass = iQClass.Where(c => !c.SynchronizeID.HasValue || c.SynchronizeID == 0 || c.IsActive == false);
                if (form.RecordNumber > 0)
                {
                    iQClass = iQClass.Take(form.RecordNumber);
                }
                var lstTransClass = (from cp in iQClass
                                     select new TransClassProfileDefault
                                     {
                                         ClassProfileId = cp.ClassProfileID,
                                         Description = cp.Description,
                                         DisplayName = cp.DisplayName,
                                         EducationLevelId = cp.EducationLevelID,
                                         FirstForeignLanguageId = cp.FirstForeignLanguageID,
                                         HeadTeacherId = cp.HeadTeacherID,
                                         IsCombinedClass = cp.IsCombinedClass,
                                         IsSpecializedClass = cp.IsSpecializedClass,
                                         SchoolId = cp.SchoolID,
                                         SecondForeignLanguageId = cp.SecondForeignLanguageID,
                                         Section = cp.Section,
                                         SubCommitteeId = cp.SubCommitteeID,
                                         SynchronizeID = cp.SynchronizeID,
                                         IsActive = cp.IsActive,
                                         Year = form.Year,
                                         SourceDB = cp.MSourcedb
                                     }).ToList();
                retResult.ListClassProfile = lstTransClass;
                retResult.ValidationCode = WCFConstant.RESPONSE_SUCCESS;
                retResult.Susscess = true;
                return retResult;
            }
            catch (Exception ex)
            {
                LogExtensions.GetCurrentClass = System.Reflection.MethodBase.GetCurrentMethod().DeclaringType;
                string para = string.Format("academicYearId={0}, schoolId={1}", form.AcademicYearId, form.SchoolId);
                LogExtensions.ErrorExt(logger, DateTime.Now, para, ex.Message, "null");
                retResult.Susscess = false;
                retResult.ValidationCode = WCFConstant.RESPONSE_EXCEPTION;
                retResult.Message = ex.Message;
                return retResult;
            }
            finally
            {
                SetAutoDetectChangesEnabled(context, true);
            }
        }

        public BoolResultResponse UpdateSyncClassProfile(List<int> lstClass, string sourceDB)
        {
            SMASEntities context = DeclareBusiness();
            BoolResultResponse retResulst = new BoolResultResponse();
            retResulst.Result = true;
            retResulst.ValidationCode = WCFConstant.RESPONSE_SUCCESS;
            try
            {

                if (lstClass == null || lstClass.Count == 0)
                {
                    retResulst.Result = false;
                    return retResulst;
                }
                SetAutoDetectChangesEnabled(context, false);
                List<ClassProfile> lstClassUpdate = context.ClassProfile.Where(c => lstClass.Contains(c.ClassProfileID)).ToList();
                for (int i = 0; i < lstClassUpdate.Count; i++)
                {
                    ClassProfile classUpdate = lstClassUpdate[i];
                    string sourcedb = UtilsBusiness.CreateSourceDB(classUpdate.MSourcedb, sourceDB);
                    classUpdate.SynchronizeID = 1;
                    classUpdate.MSourcedb = sourcedb;
                    context.ClassProfile.Attach(classUpdate);
                    context.Entry(classUpdate).State = EntityState.Modified;
                }
                context.SaveChanges();
                return retResulst;

            }
            catch (Exception ex)
            {

                LogExtensions.GetCurrentClass = System.Reflection.MethodBase.GetCurrentMethod().DeclaringType;
                string para = string.Format("sourceDB={0}", sourceDB);
                LogExtensions.ErrorExt(logger, DateTime.Now, para, ex.Message, "null");
                retResulst.Result = false;
                retResulst.ValidationCode = WCFConstant.RESPONSE_EXCEPTION;

                return retResulst;
            }
            finally
            {
                SetAutoDetectChangesEnabled(context, true);
            }
        }

        public TransClassSubjectResponse GetSyncClassSubject(SyncAcademicYear form)
        {
            SMASEntities context = DeclareBusiness();
            TransClassSubjectResponse retResult = new TransClassSubjectResponse();
            retResult.Susscess = false;
            retResult.ListSubjectClass = new List<TransClassSubjectDefault>();
            retResult.ValidationCode = WCFConstant.RESPONSE_CLASSID_NOT_FOUND;
            try
            {
                SetAutoDetectChangesEnabled(context, false);
                var iqClassSubject = from cs in context.ClassSubject.AsNoTracking()
                                     join cp in context.ClassProfile.AsNoTracking() on cs.ClassID equals cp.ClassProfileID
                                     where cs.Last2digitNumberSchool == form.SchoolId % 100
                                           && cp.AcademicYearID == form.AcademicYearId
                                     //&& (!cs.SynchronizeID.HasValue || cs.SynchronizeID == 0 || cp.IsActive == false)
                                     select cs;
                iqClassSubject = iqClassSubject.Where(s => (string.IsNullOrEmpty(s.MSourcedb) || !s.MSourcedb.Contains(form.SourceDB)));
                iqClassSubject = iqClassSubject.Take(form.RecordNumber);
                var lstClassSubject = iqClassSubject.ToList();
                if (lstClassSubject.Count == 0)
                {
                    return retResult;
                }
                List<TransClassSubjectDefault> lstClassSubjectTrans = lstClassSubject.Select(cs =>
                                           new TransClassSubjectDefault
                                           {
                                               ClassId = cs.ClassID,
                                               ClassSubjectId = cs.ClassSubjectID,
                                               FirstSemesterCoefficient = cs.FirstSemesterCoefficient ?? 0,
                                               IsCommenting = cs.IsCommenting ?? 0,
                                               IsSecondForeignLanguage = cs.IsSecondForeignLanguage ?? false,
                                               IsSpecializedSubject = cs.IsSpecializedSubject ?? false,
                                               SecondSemesterCoefficient = cs.SecondSemesterCoefficient ?? 0,
                                               SectionPerWeekFirstSemester = cs.SectionPerWeekFirstSemester ?? 0,
                                               SectionPerWeekSecondSemester = cs.SectionPerWeekSecondSemester ?? 0,
                                               SubjectId = cs.SubjectID,
                                               MSourceDB = cs.MSourcedb
                                           }
                                            ).ToList();

                retResult.ListSubjectClass = lstClassSubjectTrans;
                retResult.ValidationCode = WCFConstant.RESPONSE_SUCCESS;
                retResult.Susscess = true;
                return retResult;
            }
            catch (Exception ex)
            {
                LogExtensions.GetCurrentClass = System.Reflection.MethodBase.GetCurrentMethod().DeclaringType;
                string para = string.Format("academicYearId={0}, schoolId={1}", form.AcademicYearId, form.SchoolId);
                LogExtensions.ErrorExt(logger, DateTime.Now, para, ex.Message, "null");
                retResult.Susscess = false;
                retResult.ValidationCode = WCFConstant.RESPONSE_EXCEPTION;
                retResult.Message = ex.StackTrace;
                return retResult;
            }
            finally
            {
                SetAutoDetectChangesEnabled(context, true);
            }
        }

        public BoolResultResponse UpdateSyncClassSubject(List<long> lstClassSubject, SyncAcademicYear form)
        {
            SMASEntities context = DeclareBusiness();
            BoolResultResponse retResulst = new BoolResultResponse();
            retResulst.Result = true;
            retResulst.ValidationCode = WCFConstant.RESPONSE_SUCCESS;
            try
            {
                SetAutoDetectChangesEnabled(context, false);

                if (lstClassSubject == null || lstClassSubject.Count == 0)
                {
                    retResulst.Result = false;
                    return retResulst;
                }
                int partitionId = UtilsBusiness.GetPartionId(form.SchoolId);
                List<ClassSubject> lstClassSubjectUpdate = ClassSubjectBusiness.All.Where(c => lstClassSubject.Contains(c.ClassSubjectID) && c.Last2digitNumberSchool == partitionId).ToList();
                for (int i = 0; i < lstClassSubjectUpdate.Count; i++)
                {
                    ClassSubject objSubjectClass = lstClassSubjectUpdate[i];
                    string sourcedb = UtilsBusiness.CreateSourceDB(objSubjectClass.MSourcedb, form.SourceDB);
                    objSubjectClass.SynchronizeID = 1;
                    objSubjectClass.MSourcedb = sourcedb;
                    context.ClassSubject.Attach(objSubjectClass);
                    context.Entry(objSubjectClass).State = EntityState.Modified;
                }
                context.SaveChanges();
                return retResulst;
            }
            catch (Exception ex)
            {

                LogExtensions.GetCurrentClass = System.Reflection.MethodBase.GetCurrentMethod().DeclaringType;
                string para = string.Format("academicYearId={0}, schoolId={1}", form.AcademicYearId, form.SchoolId);
                LogExtensions.ErrorExt(logger, DateTime.Now, para, ex.Message, "null");
                retResulst.Result = false;
                retResulst.ValidationCode = WCFConstant.RESPONSE_EXCEPTION;
                retResulst.Description = ex.Message;
                return retResulst;
            }
            finally
            {
                SetAutoDetectChangesEnabled(context, true);
            }
        }

        public TransAcademicYearResponse GetSyncAcademicYear(SyncAcademicYear form)
        {
            SMASEntities context = DeclareBusiness();
            TransAcademicYearResponse retResult = new TransAcademicYearResponse();
            retResult.Susscess = false;
            retResult.ListAcademicYear = new List<SyncAcademicYear>();
            retResult.ValidationCode = WCFConstant.RESPONSE_CLASSID_NOT_FOUND;
            try
            {

                SetAutoDetectChangesEnabled(context, false);
                IQueryable<SyncAcademicYear> iqSchool = from ac in context.AcademicYear.AsNoTracking()
                                                        join sp in context.SchoolProfile.AsNoTracking() on ac.SchoolID equals sp.SchoolProfileID
                                                        where sp.ProvinceID == form.ProvinceId
                                                              && ac.Year == form.Year
                                                              && sp.IsActive
                                                              && ac.IsActive == true
                                                              && form.LstEducationGradeId.Contains(sp.EducationGrade)
                                                        select new SyncAcademicYear
                                                        {
                                                            AcademicYearId = ac.AcademicYearID,
                                                            SchoolId = ac.SchoolID,
                                                            Year = ac.Year,
                                                            EducationGradeId = sp.EducationGrade
                                                        };
                if (form.SchoolId > 0)
                {
                    iqSchool = iqSchool.Where(s => s.SchoolId == form.SchoolId);
                }
                List<SyncAcademicYear> lstSchool = iqSchool.ToList();

                retResult.ListAcademicYear = lstSchool;
                retResult.ValidationCode = WCFConstant.RESPONSE_SUCCESS;
                retResult.Susscess = true;
                retResult.Message = "Thanh cong";
                return retResult;
            }
            catch (Exception ex)
            {
                LogExtensions.GetCurrentClass = System.Reflection.MethodBase.GetCurrentMethod().DeclaringType;
                string para = string.Format("academicYearId={0}, schoolId={1}", form.AcademicYearId, form.SchoolId);
                LogExtensions.ErrorExt(logger, DateTime.Now, para, ex.Message, "null");
                retResult.Susscess = false;
                retResult.ValidationCode = WCFConstant.RESPONSE_EXCEPTION;
                retResult.Message = ex.Message;
                return retResult;
            }
            finally
            {
                SetAutoDetectChangesEnabled(context, true);
            }
        }





        #endregion


        public TableContract GetAll(string sqlquery, string tableName)
        {
            SMASEntities context = DeclareBusiness();
            try
            {
                string consString = context.Database.Connection.ConnectionString;
                TableContract tc = new TableContract();
                using (OracleConnection conn = new OracleConnection(consString))
                {
                    try
                    {
                        if (conn.State != ConnectionState.Open)
                        {
                            conn.Open();
                        }

                        OracleCommand cmd = new OracleCommand(sqlquery, conn);
                        cmd.CommandTimeout = 0;
                        cmd.CommandType = CommandType.Text;
                        var tb = new DataTable(tableName);
                        OracleDataAdapter dac = new OracleDataAdapter(cmd);
                        dac.Fill(tb);

                        tc.ListData = tb;
                        tc.Success = true;
                        tc.Description = "Lay du lieu thanh cong";
                        return tc;
                    }
                    catch (Exception ex)
                    {
                        LogExtensions.GetCurrentClass = System.Reflection.MethodBase.GetCurrentMethod().DeclaringType;
                        string para = string.Format("sqlquery={0}, tableName={1}", sqlquery, tableName);
                        LogExtensions.ErrorExt(logger, DateTime.Now, para, ex.Message, "null");
                        return new TableContract
                        {
                            ListData = new DataTable(),
                            Success = false,
                            Description = ex.Message
                        };
                    }
                    finally
                    {
                        if (conn != null)
                        {
                            conn.Close();
                        }
                    }
                }


            }
            catch (Exception ex)
            {

                LogExtensions.GetCurrentClass = System.Reflection.MethodBase.GetCurrentMethod().DeclaringType;
                string para = string.Format("sqlquery={0}, tableName={1}", sqlquery, tableName);
                LogExtensions.ErrorExt(logger, DateTime.Now, para, ex.Message, "null");
                return new TableContract
                {
                    ListData = new DataTable(),
                    Description = ex.Message,
                    Success = false
                };
            }
            finally
            {
                SetAutoDetectChangesEnabled(context, true);
            }
        }

        public bool ExcuteSql(List<string> lstSql)
        {
            SMASEntities context = DeclareBusiness();
            LogExtensions.GetCurrentClass = System.Reflection.MethodBase.GetCurrentMethod().DeclaringType;
            try
            {
                string consString = context.Database.Connection.ConnectionString;
                TableContract tc = new TableContract();
                using (OracleConnection conn = new OracleConnection(consString))
                {
                    try
                    {
                        if (conn.State != ConnectionState.Open)
                        {
                            conn.Open();
                        }

                        using (OracleTransaction tran = conn.BeginTransaction())
                        {
                            try
                            {
                                OracleCommand cmd = null;
                                foreach (string sql in lstSql)
                                {
                                    //LogExtensions.InfoExt(logger, LogExtensions.LOG_TYPE_INFO, DateTime.Now, "sql=" + sql, "cau lenh thuc hien", "Null");
                                    cmd = new OracleCommand(sql, conn);
                                    cmd.CommandTimeout = 0;
                                    cmd.ExecuteNonQuery();
                                }

                                tran.Commit();
                            }
                            catch (Exception ex)
                            {
                                tran.Rollback();

                                string para = "null";
                                LogExtensions.ErrorExt(logger, DateTime.Now, para, ex.Message + "Chi tiet loi: " + ex.StackTrace, "null");
                                return false;
                            }
                            finally
                            {
                                tran.Dispose();
                            }
                        }
                        return true;
                    }
                    catch (Exception ex)
                    {
                        string para = "null";
                        LogExtensions.ErrorExt(logger, DateTime.Now, para, ex.Message + "Chi tiet loi: " + ex.StackTrace, "null");
                        return false;
                    }
                    finally
                    {
                        if (conn != null)
                        {
                            conn.Close();
                        }
                    }
                }


            }
            catch (Exception ex)
            {
                string para = "null";
                LogExtensions.ErrorExt(logger, DateTime.Now, para, ex.Message, "null");
                return false;
            }
            finally
            {
                SetAutoDetectChangesEnabled(context, true);
            }
        }
    }
}
