﻿using Payment.ServiceLoader.ILoader;
using Payment.ServiceLoader.ScratchCardAPIService;
using Payment.ServiceLoader.Utility;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Security;
using System.Security.Cryptography.X509Certificates;
using System.Text;
using System.Threading.Tasks;

namespace Payment.ServiceLoader.Loader
{
    public class ScratchCardServiceLoader : IScratchCardServiceLoader
    {
        #region contructor/declare

        private ScratchCardAPIClient _scratchCardAPIClient;

        private ScratchCardAPIClient ScratchCardAPIClient
        {
            get
            {
                if (_scratchCardAPIClient == null || _scratchCardAPIClient.State == System.ServiceModel.CommunicationState.Closed)
                {
                    _scratchCardAPIClient = new ScratchCardAPIClient();
                }

                return _scratchCardAPIClient;
            }
        }
        #endregion

        #region call topupcard api
        /// <summary>
        /// call topupcard API
        /// </summary>
        /// <param name="partnerID">id doi tac do vt cung cap</param>
        /// <param name="passPhare">mat khau cua doi tac do vt cung cap</param>
        /// <param name="cardSerial">chuoi so in tren the cao</param>
        /// <param name="pinCard">chuoi so lay duoc sau khi cao lop bac</param>
        /// <param name="transId">ma giao dich tu sinh yyMMddHHmmssPPPPPPPPPPXXX, PP la partnerID, XXX la so ngau nhien 3 so</param>
        /// <param name="provider">Ten nha cung cap</param>
        /// <param name="serviceName">Ten dich vu cung cap</param>
        /// <returns></returns>
        public TopupLoaderResult Topup(string partnerID, string passPhare, string cardSerial, string pinCard, string transId, string provider = "SMAS", string serviceName = "QLNT")
        {
            try
            {
                SSLValidator.OverrideValidation();
                var r = ScratchCardAPIClient.topupCard(partnerID, passPhare, cardSerial, pinCard, transId, provider, serviceName);
                TopupLoaderResult result = new TopupLoaderResult
                {
                    ErrorCode = r.errorCode,
                    ErrorMessage = r.errorMessage,
                    TransID = r.transId,
                    OriginTransID = r.originTransId,
                    TopupTime = r.responseTime,
                    TopupTimeSpecified = r.responseTimeSpecified,
                    CardSerial = r.cardSerial,
                    Amount = 0
                };

                // convert so tien ve decimal
                decimal amount = 0;
                if (decimal.TryParse(r.amount, out amount))
                {
                    result.Amount = amount;
                }

                return result;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                if (ScratchCardAPIClient.State == System.ServiceModel.CommunicationState.Faulted)
                {
                    ScratchCardAPIClient.Abort();
                }
                else
                {
                    if (ScratchCardAPIClient.State == System.ServiceModel.CommunicationState.Opened)
                    {
                        ScratchCardAPIClient.Close();
                    }
                }
            }
        }

        #endregion        
    }
}
