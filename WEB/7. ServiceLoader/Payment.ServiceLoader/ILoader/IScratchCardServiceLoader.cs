﻿using Payment.ServiceLoader.Utility;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Payment.ServiceLoader.ILoader
{
    public interface IScratchCardServiceLoader
    {
        #region call topupcard api
        /// <summary>
        /// call topupcard API
        /// </summary>
        /// <param name="partnerID">id doi tac do vt cung cap</param>
        /// <param name="passPhare">mat khau cua doi tac do vt cung cap</param>
        /// <param name="cardSerial">chuoi so in tren the cao</param>
        /// <param name="pinCard">chuoi so lay duoc sau khi cao lop bac</param>
        /// <param name="transId">ma giao dich tu sinh yyMMddHHmmssPPPPPPPPPPXXX, PP la partnerID, XXX la so ngau nhien 3 so</param>
        /// <param name="provider">Ten nha cung cap</param>
        /// <param name="serviceName">Ten dich vu cung cap</param>
        /// <returns></returns>
        TopupLoaderResult Topup(string partnerID, string passPhare, string cardSerial, string pinCard, string transId, string provider = "SMAS", string serviceName = "QLNT");
        #endregion
    }
}
