/**
 * The Viettel License
 *
 * Copyright 2012 Viettel Telecom. All rights reserved.
 * VIETTEL PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */

/** 
* @author  
* @version $Revision: $
*/

using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Linq.Expressions;
using System.Web;
using log4net;

using SMAS.DAL.Repository;
using SMAS.DAL.IRepository;
using SMAS.Business.IBusiness;
using SMAS.Models.Models;

namespace SMAS.Business.Business
{ 
    public partial class EmployeeHistoryStatusBusiness : GenericBussiness<EmployeeHistoryStatus>, IEmployeeHistoryStatusBusiness
    {  
		IEmployeeHistoryStatusRepository  EmployeeHistoryStatusRepository;
        ISchoolProfileBusiness SchoolBusiness;
        ISupervisingDeptBusiness SupervisingDeptBusiness;

        public EmployeeHistoryStatusBusiness(ILog logger, SMASEntities context = null)
            : base(logger)
        {
            if (context == null) context = new SMASEntities();
            this.context = context;
			this.EmployeeHistoryStatusRepository = new EmployeeHistoryStatusRepository(context);
            this.SchoolBusiness = new SchoolProfileBusiness(logger, context);
            this.SupervisingDeptBusiness = new SupervisingDeptBusiness(logger, this.context);
			repository = EmployeeHistoryStatusRepository;
        }
        
    }
}