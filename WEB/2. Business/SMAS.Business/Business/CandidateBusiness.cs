/**
 * The Viettel License
 *
 * Copyright 2012 Viettel Telecom. All rights reserved.
 * VIETTEL PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */

/** 
* @author  
* @version $Revision: $
*/

using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Linq.Expressions;
using System.Web;
using log4net;

using SMAS.DAL.Repository;
using SMAS.DAL.IRepository;
using SMAS.Business.IBusiness;
using SMAS.Models.Models;

namespace SMAS.Business.Business
{ 
    public partial class CandidateBusiness : GenericBussiness<Candidate>, ICandidateBusiness
    {  
        //IExaminationRepository ExaminationRepository;
         ISchoolProfileRepository SchoolProfileRepository;
		ICandidateRepository  CandidateRepository;
        IEducationLevelRepository EducationLevelRepository;
        IClassProfileRepository ClassProfileRepository;
        IProcessedReportParameterRepository ProcessedReportParameterRepository;
        IProcessedReportRepository ProcessedReportRepository;
		public CandidateBusiness(ILog logger, SMASEntities context = null) : base(logger)
        {
			if (context == null){context = new SMASEntities();}this.context = context;
			this.CandidateRepository = new CandidateRepository(context);
            this.SchoolProfileRepository = new SchoolProfileRepository(context);
            this.EducationLevelRepository = new EducationLevelRepository(context);
            this.ClassProfileRepository = new ClassProfileRepository(context);
            this.ProcessedReportParameterRepository = new ProcessedReportParameterRepository(context);
            this.ProcessedReportRepository = new ProcessedReportRepository(context);
			repository = CandidateRepository;
        }
        
    }
}