/**
 * The Viettel License
 *
 * Copyright 2012 Viettel Telecom. All rights reserved.
 * VIETTEL PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */

/** 
* @author  
* @version $Revision: $
*/

using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Linq.Expressions;
using System.Web;
using log4net;

using SMAS.DAL.Repository;
using SMAS.DAL.IRepository;
using SMAS.Business.IBusiness;
using SMAS.Models.Models;

namespace SMAS.Business.Business
{ 
    public partial class ExamSupervisoryAssignmentBusiness : GenericBussiness<ExamSupervisoryAssignment>, IExamSupervisoryAssignmentBusiness
    {  
		IExamSupervisoryAssignmentRepository  ExamSupervisoryAssignmentRepository;
        IExaminationsRepository ExaminationsRepository;
        IExamGroupRepository ExamGroupRepository;
        IEmployeeRepository EmployeeRepository;
        ISchoolFacultyRepository SchoolFacultyRepository;
        IEthnicRepository EthnicRepository;
        IExamSupervisoryRepository ExamSupervisoryRepository;
        IExamRoomRepository ExamRoomRepository;
        ISubjectCatRepository SubjectCatRepository;
        IExamSubjectRepository ExamSubjectRepository;
        IProcessedReportParameterRepository ProcessedReportParameterRepository;
        IProcessedReportRepository ProcessedReportRepository;

		public ExamSupervisoryAssignmentBusiness(ILog logger, SMASEntities context = null) : base(logger)
        {
			if (context == null) { context = new SMASEntities(); } this.context = context;

			this.ExamSupervisoryAssignmentRepository = new ExamSupervisoryAssignmentRepository(context);
            this.ExaminationsRepository = new ExaminationsRepository(context);
            this.ExamGroupRepository = new ExamGroupRepository(context);
            this.EmployeeRepository = new EmployeeRepository(context);
            this.SchoolFacultyRepository = new SchoolFacultyRepository(context);
            this.EthnicRepository = new EthnicRepository(context);
            this.ExamSupervisoryRepository = new ExamSupervisoryRepository(context);
            this.ExamRoomRepository = new ExamRoomRepository(context);
            this.SubjectCatRepository = new SubjectCatRepository(context);
            this.ExamSubjectRepository = new ExamSubjectRepository(context);
            this.ProcessedReportParameterRepository = new ProcessedReportParameterRepository(context);
            this.ProcessedReportRepository = new ProcessedReportRepository(context);
			repository = ExamSupervisoryAssignmentRepository;
        }
        
    }
}