/**
 * The Viettel License
 *
 * Copyright 2012 Viettel Telecom. All rights reserved.
 * VIETTEL PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */

/** 
* @author  
* @version $Revision: $
*/

using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Linq.Expressions;
using System.Web;
using log4net;

using SMAS.DAL.Repository;
using SMAS.DAL.IRepository;
using SMAS.Business.IBusiness;
using SMAS.Models.Models;

namespace SMAS.Business.Business
{
    public partial class ViolatedCandidateBusiness : GenericBussiness<ViolatedCandidate>, IViolatedCandidateBusiness
    {
        IViolatedCandidateRepository ViolatedCandidateRepository;

        ISchoolProfileBusiness SchoolProfileBusiness { get; set; }
        ICandidateBusiness CandidateBusiness { get; set; }
        IExamViolationTypeBusiness ExamViolationTypeBusiness { get; set; }
        IExaminationBusiness ExaminationBusiness { get; set; }
        IAcademicYearBusiness AcademicYearBusiness { get; set; }
        IReportDefinitionBusiness ReportDefinitionBusiness { get; set; }

        public ViolatedCandidateBusiness(ILog logger, SMASEntities context = null)
            : base(logger)
        {
            if (context == null) 
                context = new SMASEntities();
            this.context = context;
            this.ViolatedCandidateRepository = new ViolatedCandidateRepository(context);
            repository = ViolatedCandidateRepository;

            this.SchoolProfileBusiness = new SchoolProfileBusiness(logger, this.context);
            this.CandidateBusiness = new CandidateBusiness(logger, this.context);
            this.ExamViolationTypeBusiness = new ExamViolationTypeBusiness(logger, this.context);
            this.ExaminationBusiness = new ExaminationBusiness(logger, this.context);
            this.AcademicYearBusiness = new AcademicYearBusiness(logger, this.context);
            this.ReportDefinitionBusiness = new ReportDefinitionBusiness(logger, this.context);
        }

    }
}