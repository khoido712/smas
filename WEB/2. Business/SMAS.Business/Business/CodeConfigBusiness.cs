﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SMAS.Business.IBusiness;
using SMAS.Models.Models;
using log4net;
using SMAS.DAL.IRepository;
using SMAS.DAL.Repository;

namespace SMAS.Business.Business
{
    public partial class CodeConfigBusiness : GenericBussiness<CodeConfig>, ICodeConfigBusiness
    {
        ICodeConfigRepository CodeConfigRepository;
        IEducationLevelRepository EducationLevelRepository;
        IPupilProfileRepository PupilProfileRepository;
        IPupilOfSchoolRepository PupilOfSchoolRepository;
        IEmployeeHistoryStatusRepository EmployeeHistoryStatusRepository;
        IEmployeeRepository EmployeeRepository;        

        public CodeConfigBusiness(ILog logger, SMASEntities context = null)
            : base(logger)
        {
            if (context == null) context = new SMASEntities(); 
            this.context = context;
            //context = new SMASEntities();
            this.CodeConfigRepository = new CodeConfigRepository(this.context);
            this.EducationLevelRepository = new EducationLevelRepository(this.context);
            this.PupilProfileRepository = new PupilProfileRepository(this.context);
            this.PupilOfSchoolRepository = new PupilOfSchoolRepository(this.context);
            this.EmployeeHistoryStatusRepository = new EmployeeHistoryStatusRepository(this.context);
            this.EmployeeRepository = new EmployeeRepository(this.context);           
            repository = CodeConfigRepository;
        }
    }
}