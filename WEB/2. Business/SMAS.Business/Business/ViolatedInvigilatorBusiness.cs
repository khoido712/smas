/**
 * The Viettel License
 *
 * Copyright 2012 Viettel Telecom. All rights reserved.
 * VIETTEL PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */

/** 
* @author  
* @version $Revision: $
*/

using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Linq.Expressions;
using System.Web;
using log4net;

using SMAS.DAL.Repository;
using SMAS.DAL.IRepository;
using SMAS.Business.IBusiness;
using SMAS.Models.Models;

namespace SMAS.Business.Business
{
    public partial class ViolatedInvigilatorBusiness : GenericBussiness<ViolatedInvigilator>, IViolatedInvigilatorBusiness
    {
        IViolatedInvigilatorRepository ViolatedInvigilatorRepository;

        ISchoolProfileBusiness SchoolProfileBusiness { get; set; }
        IInvigilatorBusiness InvigilatorBusiness { get; set; }
        IInvigilatorAssignmentBusiness InvigilatorAssignmentBusiness { get; set; }
        IExamViolationTypeBusiness ExamViolationTypeBusiness { get; set; }
        IExaminationBusiness ExaminationBusiness { get; set; }
        IAcademicYearBusiness AcademicYearBusiness { get; set; }

        public ViolatedInvigilatorBusiness(ILog logger, SMASEntities context = null)
            : base(logger)
        {
            if (context == null)
                context = new SMASEntities();
            this.context = context;
            this.ViolatedInvigilatorRepository = new ViolatedInvigilatorRepository(context);
            repository = ViolatedInvigilatorRepository;

            this.SchoolProfileBusiness = new SchoolProfileBusiness(logger, this.context);
            this.InvigilatorBusiness = new InvigilatorBusiness(logger, this.context);
            this.ExamViolationTypeBusiness = new ExamViolationTypeBusiness(logger, this.context);
            this.ExaminationBusiness = new ExaminationBusiness(logger, this.context);
            this.AcademicYearBusiness = new AcademicYearBusiness(logger, this.context);
            this.InvigilatorAssignmentBusiness = new InvigilatorAssignmentBusiness(logger, this.context);
        }
    }
}