﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Linq.Expressions;
using System.Web;
using log4net;

using SMAS.DAL.Repository;
using SMAS.DAL.IRepository;
using SMAS.Business.IBusiness;
using SMAS.Models.Models;

namespace SMAS.Business.Business
{
    public partial class AlertMessageBusiness : GenericBussiness<AlertMessage>, IAlertMessageBusiness
    {
        
        IAlertMessageRepository  AlertMessageRepository;
        IEmployeeRepository EmployeeRepository;
        IUserAccountRepository UserAccountRepository;
        IAuthenticationTokenRepository AuthenticationTokenRepository;
        ISchoolProfileRepository SchoolProfileRepository;
        public AlertMessageBusiness(ILog logger, SMASEntities context = null)
            : base(logger)
        {
			if (context == null)
            {
                context = new SMASEntities();
            }

            this.context = context;


            this.AlertMessageRepository = new AlertMessageRepository(context);
            repository = AlertMessageRepository;

            this.EmployeeRepository = new EmployeeRepository(context);
            this.UserAccountRepository = new UserAccountRepository(context);
            this.AuthenticationTokenRepository = new AuthenticationTokenRepository(context);
            this.SchoolProfileRepository = new SchoolProfileRepository(context);
        }
    }
}
