/**
 * The Viettel License
 *
 * Copyright 2012 Viettel Telecom. All rights reserved.
 * VIETTEL PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */

/** 
* @author  
* @version $Revision: $
*/

using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Linq.Expressions;
using System.Web;
using log4net;

using SMAS.DAL.Repository;
using SMAS.DAL.IRepository;
using SMAS.Business.IBusiness;
using SMAS.Models.Models;

namespace SMAS.Business.Business
{
    public partial class PupilRetestRegistrationBusiness : GenericBussiness<PupilRetestRegistration>, IPupilRetestRegistrationBusiness
    {
        IProcessedReportParameterRepository ProcessedReportParameterRepository;
        IProcessedReportRepository ProcessedReportRepository;
        IPupilRetestRegistrationRepository PupilRetestRegistrationRepository;
        IPupilOfClassRepository PupilOfClassRepository;
        IPupilProfileRepository PupilProfileRepository;
        IPupilRankingRepository PupilRankingRepository;
        IAcademicYearRepository AcademicYearRepository;
        IClassProfileRepository ClassProfileRepository;
        ISummedUpRecordRepository SummedUpRecordRepository;
        IMarkRecordRepository MarkRecordRepository;
        IJudgeRecordRepository JudgeRecordRepository;
        IClassSubjectRepository ClassSubjectRepository;
        ISchoolProfileRepository SchoolProfileRepository;
        IEducationLevelRepository EducationLevelRepository;

        public PupilRetestRegistrationBusiness(ILog logger, SMASEntities context = null)
            : base(logger)
        {
            if (context == null) { context = new SMASEntities(); } this.context = context;
            this.PupilRetestRegistrationRepository = new PupilRetestRegistrationRepository(context);
            this.ProcessedReportParameterRepository = new ProcessedReportParameterRepository(context);
            this.ProcessedReportRepository = new ProcessedReportRepository(context);
            this.PupilOfClassRepository = new PupilOfClassRepository(context);
            this.PupilRankingRepository = new PupilRankingRepository(context);
            this.AcademicYearRepository = new AcademicYearRepository(context);
            this.ClassProfileRepository = new ClassProfileRepository(context);
            this.SummedUpRecordRepository = new SummedUpRecordRepository(context);
            this.PupilProfileRepository = new PupilProfileRepository(context);
            this.MarkRecordRepository = new MarkRecordRepository(context);
            this.JudgeRecordRepository = new JudgeRecordRepository(context);
            this.ClassSubjectRepository = new DAL.Repository.ClassSubjectRepository(context);
            this.SchoolProfileRepository = new DAL.Repository.SchoolProfileRepository(context);
            this.EducationLevelRepository = new DAL.Repository.EducationLevelRepository(context);
            repository = PupilRetestRegistrationRepository;

        }

    }
}