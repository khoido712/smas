/**
 * The Viettel License
 *
 * Copyright 2012 Viettel Telecom. All rights reserved.
 * VIETTEL PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */

/** 
* @author  
* @version $Revision: $
*/

using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Linq.Expressions;
using System.Web;
using log4net;

using SMAS.DAL.Repository;
using SMAS.DAL.IRepository;
using SMAS.Business.IBusiness;
using SMAS.Models.Models;

namespace SMAS.Business.Business
{ 
    public partial class ExamSupervisoryViolateBusiness : GenericBussiness<ExamSupervisoryViolate>, IExamSupervisoryViolateBusiness
    {  
		IExamSupervisoryViolateRepository  ExamSupervisoryViolateRepository;
        IExamSupervisoryRepository ExamSupervisoryRepository;
        IEmployeeRepository EmployeeRepository;
        IEthnicRepository EthnicRepository;
        ISubjectCatRepository SubjectCatRepository;
        IExamRoomRepository ExamRoomRepository;
        IExamViolationTypeRepository ExamViolationTypeRepository;
        IExamGroupRepository ExamGroupRepository;
        IProcessedReportParameterRepository ProcessedReportParameterRepository;
        IProcessedReportRepository ProcessedReportRepository;

		public ExamSupervisoryViolateBusiness(ILog logger, SMASEntities context = null) : base(logger)
        {
			if (context == null) { context = new SMASEntities(); } this.context = context;

			this.ExamSupervisoryViolateRepository = new ExamSupervisoryViolateRepository(context);
            this.EmployeeRepository = new EmployeeRepository(context);
            this.EthnicRepository = new EthnicRepository(context);
            this.SubjectCatRepository = new SubjectCatRepository(context);
            this.ExamRoomRepository = new ExamRoomRepository(context);
            this.ExamViolationTypeRepository = new ExamViolationTypeRepository(context);
            this.ExamSupervisoryRepository = new ExamSupervisoryRepository(context);
            this.ExamGroupRepository = new ExamGroupRepository(context);
            this.ProcessedReportParameterRepository = new ProcessedReportParameterRepository(context);
            this.ProcessedReportRepository = new ProcessedReportRepository(context);

			repository = ExamSupervisoryViolateRepository;
        }
        
    }
}