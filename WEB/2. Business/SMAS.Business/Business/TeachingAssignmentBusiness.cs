/**
 * The Viettel License
 *
 * Copyright 2012 Viettel Telecom. All rights reserved.
 * VIETTEL PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */

/** 
* @author  
* @version $Revision: $
*/

using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Linq.Expressions;
using System.Web;
using log4net;

using SMAS.DAL.Repository;
using SMAS.DAL.IRepository;
using SMAS.Business.IBusiness;
using SMAS.Models.Models;

namespace SMAS.Business.Business
{ 
    public partial class TeachingAssignmentBusiness : GenericBussiness<TeachingAssignment>, ITeachingAssignmentBusiness
    {  
		ITeachingAssignmentRepository  TeachingAssignmentRepository;

        ICalendarRepository CalendarRepository;
        IClassProfileRepository ClassProfileRepository;
        IClassSubjectRepository ClassSubjectRepository;
        ISubjectCatRepository SubjectCatRepository;
        ISchoolFacultyRepository SchoolFacultyRepository;
        IEmployeeRepository EmployeeRepository;
        IEthnicRepository EthnicRepository;

        public TeachingAssignmentBusiness(ILog logger, SMASEntities context = null)
            : base(logger)
        {
            if (context == null){context = new SMASEntities();}this.context = context;
           
            this.CalendarRepository = new CalendarRepository(context);
            this.ClassProfileRepository = new ClassProfileRepository(context);
            this.ClassSubjectRepository = new ClassSubjectRepository(context);
			this.TeachingAssignmentRepository = new TeachingAssignmentRepository(context);
            this.SubjectCatRepository = new SubjectCatRepository(context);
            this.SchoolFacultyRepository = new SchoolFacultyRepository(context);
            this.EmployeeRepository = new EmployeeRepository(context);
            this.EthnicRepository = new EthnicRepository(context);
			repository = TeachingAssignmentRepository;
        }
        
    }
}