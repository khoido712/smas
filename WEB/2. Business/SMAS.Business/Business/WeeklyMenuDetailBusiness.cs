/**
 * The Viettel License
 *
 * Copyright 2012 Viettel Telecom. All rights reserved.
 * VIETTEL PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */

/** 
* @author  
* @version $Revision: $
*/

using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Linq.Expressions;
using System.Web;
using log4net;

using SMAS.DAL.Repository;
using SMAS.DAL.IRepository;
using SMAS.Business.IBusiness;
using SMAS.Models.Models;

namespace SMAS.Business.Business
{ 
    public partial class WeeklyMenuDetailBusiness : GenericBussiness<WeeklyMenuDetail>, IWeeklyMenuDetailBusiness
    { 
		IWeeklyMenuDetailRepository  WeeklyMenuDetailRepository;
        IWeeklyMenuRepository WeeklyMenuRepository;
        public WeeklyMenuDetailBusiness(ILog logger, SMASEntities context = null)
            : base(logger)
        {
            //context = new SMASEntities();
            //this.WeeklyMenuDetailRepository = new WeeklyMenuDetailRepository(context);
            //repository = WeeklyMenuDetailRepository;
            //this.WeeklyMenuRepository = new WeeklyMenuRepository(context);

            if (context == null) { context = new SMASEntities(); } this.context = context;
            this.WeeklyMenuDetailRepository = new WeeklyMenuDetailRepository(context);
            repository = WeeklyMenuDetailRepository;
            this.WeeklyMenuRepository = new WeeklyMenuRepository(context);
        }
        
    }
}