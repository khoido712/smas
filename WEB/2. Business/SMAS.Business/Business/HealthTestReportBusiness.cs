﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SMAS.Models.Models;
using SMAS.Business.IBusiness;
using SMAS.DAL.IRepository;
using SMAS.DAL.Repository;
using log4net;

namespace SMAS.Business.Business
{
    public partial class HealthTestReportBusiness : GenericBussiness<ProcessedReport>, IHealthTestReportBusiness
    {

        IProcessedReportRepository ProcessedReportRepository;
        IProcessedReportParameterRepository ProcessedReportParameterRepository;
        IMonitoringBookRepository MonitoringBookRepository;
        public HealthTestReportBusiness(ILog logger, SMASEntities context = null)
            : base(logger)
        {
            if (context == null) { context = new SMASEntities(); } this.context = context;
            this.ProcessedReportParameterRepository = new ProcessedReportParameterRepository(context);
            this.ProcessedReportRepository = new ProcessedReportRepository(context);
            this.MonitoringBookRepository = new MonitoringBookRepository(context);
        }
    }
}
