﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SMAS.Models.Models;
using SMAS.DAL.IRepository;
using log4net;
using SMAS.DAL.Repository;
using SMAS.Business.IBusiness;

namespace SMAS.Business.Business
{
    public partial class DOETExamPupilBusiness : GenericBussiness<DOETExamPupil>, IDOETExamPupilBusiness
    {
        IDOETExamPupilRepository DOETExamPupilRepository;
        IDOETExamGroupRepository DOETExamGroupRepository;
        IDOETExamSubjectRepository DOETExamSubjectRepository;
        IProcessedReportParameterRepository ProcessedReportParameterRepository;
        IProcessedReportRepository ProcessedReportRepository;
        public DOETExamPupilBusiness(ILog logger, SMASEntities context = null)
            : base(logger)
        {
            if (context == null) { context = new SMASEntities(); }
            this.context = context;
            this.DOETExamPupilRepository = new DOETExamPupilRepository(this.context);
            repository = DOETExamPupilRepository;
            this.DOETExamGroupRepository = new DOETExamGroupRepository(this.context);
            this.DOETExamSubjectRepository = new DOETExamSubjectRepository(this.context);
            this.ProcessedReportParameterRepository = new ProcessedReportParameterRepository(this.context);
            this.ProcessedReportRepository = new ProcessedReportRepository(this.context);
        }
    }
}