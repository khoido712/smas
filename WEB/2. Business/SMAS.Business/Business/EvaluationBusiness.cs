﻿using System.Web;
using log4net;

using SMAS.DAL.Repository;
using SMAS.DAL.IRepository;
using SMAS.Business.IBusiness;
using SMAS.Models.Models;

namespace SMAS.Business.Business
{
    public partial class EvaluationBusiness : GenericBussiness<Evaluation>, IEvaluationBusiness
    {
        IEvaluationRepository EvaluationRepository;

         public EvaluationBusiness(ILog logger, SMASEntities context = null)
            : base(logger)
        {
            if (context == null) { context = new SMASEntities(); } this.context = context;
            this.EvaluationRepository = new EvaluationRepository(context);
            repository = EvaluationRepository;
        }
    }
}
