/**
 * The Viettel License
 *
 * Copyright 2012 Viettel Telecom. All rights reserved.
 * VIETTEL PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */

/** 
* @author  
* @version $Revision: $
*/

using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Linq.Expressions;
using System.Web;
using log4net;

using SMAS.DAL.Repository;
using SMAS.DAL.IRepository;
using SMAS.Business.IBusiness;
using SMAS.Models.Models;

namespace SMAS.Business.Business
{ 
    public partial class FoodCatBusiness : GenericBussiness<FoodCat>, IFoodCatBusiness
    {  
		IFoodCatRepository  FoodCatRepository;
        IFoodMineralRepository FoodMineralRepository;
        INutritionalNormRepository NutritionalNormRepository;
        INutritionalNormMineralRepository NutritionalNormMineralRepository;
        IDailyMenuDetailRepository DailyMenuDetailRepository;
        IDishDetailRepository DishDetailRepository;
        IEnergyDistributionRepository EnergyDistributionRepository;
        public FoodCatBusiness(ILog logger, SMASEntities context = null)
            : base(logger)
        {
            if (context == null)
            {
                context = new SMASEntities();
            }

            this.context = context;

			this.FoodCatRepository = new FoodCatRepository(context);
            this.FoodMineralRepository = new FoodMineralRepository(context);
            this.NutritionalNormRepository = new NutritionalNormRepository(context);
            this.NutritionalNormMineralRepository = new NutritionalNormMineralRepository(context);
            this.DailyMenuDetailRepository = new DailyMenuDetailRepository(context);
            this.EnergyDistributionRepository = new EnergyDistributionRepository(context);
            this.DishDetailRepository = new DishDetailRepository(context);
			repository = FoodCatRepository;
        }
        
    }
}