/**
 * The Viettel License
 *
 * Copyright 2012 Viettel Telecom. All rights reserved.
 * VIETTEL PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */

/** 
* @author  
* @version $Revision: $
*/

using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Linq.Expressions;
using System.Web;
using log4net;

using SMAS.DAL.Repository;
using SMAS.DAL.IRepository;
using SMAS.Business.IBusiness;
using SMAS.Models.Models;

namespace SMAS.Business.Business
{ 
    public partial class ExamPupilAbsenceBusiness : GenericBussiness<ExamPupilAbsence>, IExamPupilAbsenceBusiness
    {  
		IExamPupilAbsenceRepository  ExamPupilAbsenceRepository;
        IExamPupilRepository ExamPupilRepository;
        IPupilProfileRepository PupilProfileRepository;
        IExamRoomRepository ExamRoomRepository;
        ISubjectCatRepository SubjectCatRepository;
        IProcessedReportParameterRepository ProcessedReportParameterRepository;
        IProcessedReportRepository ProcessedReportRepository;
        IPupilOfClassRepository PupilOfClassRepository;
        IClassProfileRepository ClassProfileRepository;
        IExamBagRepository ExamBagRepository;

		public ExamPupilAbsenceBusiness(ILog logger, SMASEntities context = null) : base(logger)
        {
			if (context == null) { context = new SMASEntities(); } this.context = context;

			this.ExamPupilAbsenceRepository = new ExamPupilAbsenceRepository(context);
            this.ExamPupilRepository = new ExamPupilRepository(context);
            this.PupilProfileRepository = new PupilProfileRepository(context);
            this.ExamRoomRepository = new ExamRoomRepository(context);
            this.SubjectCatRepository = new SubjectCatRepository(context);
            this.ProcessedReportParameterRepository = new ProcessedReportParameterRepository(context);
            this.ProcessedReportRepository = new ProcessedReportRepository(context);
            this.PupilOfClassRepository = new PupilOfClassRepository(context);
            this.ClassProfileRepository = new ClassProfileRepository(context);
            this.ExamBagRepository = new ExamBagRepository(context);
			repository = ExamPupilAbsenceRepository;
        }
        
    }
}