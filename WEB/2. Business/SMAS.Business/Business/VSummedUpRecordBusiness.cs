/**
 * The Viettel License
 *
 * Copyright 2012 Viettel Telecom. All rights reserved.
 * VIETTEL PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */

/** 
* @author  
* @version $Revision: $
*/

using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Linq.Expressions;
using System.Web;
using log4net;

using SMAS.DAL.Repository;
using SMAS.DAL.IRepository;
using SMAS.Business.IBusiness;
using SMAS.Models.Models;

namespace SMAS.Business.Business
{
    public partial class VSummedUpRecordBusiness : GenericBussiness<VSummedUpRecord>, IVSummedUpRecordBusiness
    {
        IVSummedUpRecordRepository VSummedUpRecordRepository;
        IPupilOfClassRepository PupilOfClassRepository;
        IPupilRankingRepository PupilRankingRepository;
        IClassSubjectRepository ClassSubjectRepository;
        public VSummedUpRecordBusiness(ILog logger, SMASEntities context = null)
            : base(logger)
        {
            if (context == null) { context = new SMASEntities(); } this.context = context;
            this.VSummedUpRecordRepository = new VSummedUpRecordRepository(context);
            this.PupilOfClassRepository = new PupilOfClassRepository(context);
            this.PupilRankingRepository = new PupilRankingRepository(context);
            this.ClassSubjectRepository = new ClassSubjectRepository(context);
            repository = VSummedUpRecordRepository;
        }

    }
}