/**
 * The Viettel License
 *
 * Copyright 2012 Viettel Telecom. All rights reserved.
 * VIETTEL PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */

/** 
* @author  
* @version $Revision: $
*/

using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Linq.Expressions;
using System.Web;
using log4net;

using SMAS.DAL.Repository;
using SMAS.DAL.IRepository;
using SMAS.Business.IBusiness;
using SMAS.Models.Models;

namespace SMAS.Business.Business
{
    public partial class ExemptedSubjectBusiness : GenericBussiness<ExemptedSubject>, IExemptedSubjectBusiness
    {
        IExemptedSubjectRepository ExemptedSubjectRepository;
        IPupilProfileBusiness PupilProfileBusiness;
        IClassProfileBusiness ClassProfileBusiness;
        IClassSubjectBusiness ClassSubjectBusiness;
        ISchoolProfileBusiness SchoolProfileBusiness;
        IAcademicYearBusiness AcademicYearBusiness;
        IEducationLevelBusiness EducationLevelBusiness;
        ISubjectCatBusiness SubjectCatBusiness;
        IExemptedObjectTypeBusiness ExemptedObjectTypeBusiness;
        IPupilOfClassBusiness PupilOfClassBusiness;
        IMarkRecordBusiness MarkRecordBusiness;
        IJudgeRecordBusiness JudgeRecordBusiness;


        public ExemptedSubjectBusiness(ILog logger, SMASEntities context = null)
            : base(logger)
        {
            if (context == null) context = new SMASEntities(); 
            this.context = context;
            this.ExemptedSubjectRepository = new ExemptedSubjectRepository(context);
            this.PupilProfileBusiness = new PupilProfileBusiness(logger, context);
            this.ClassProfileBusiness = new ClassProfileBusiness(logger, context);
            this.AcademicYearBusiness = new AcademicYearBusiness(logger, context);
            this.EducationLevelBusiness = new EducationLevelBusiness(logger, context);
            this.SubjectCatBusiness = new SubjectCatBusiness(logger, context);
            this.SchoolProfileBusiness = new SchoolProfileBusiness(logger, context);
            this.ExemptedObjectTypeBusiness = new ExemptedObjectTypeBusiness(logger, context);
            this.PupilOfClassBusiness = new PupilOfClassBusiness(logger, context);
            this.ClassSubjectBusiness = new ClassSubjectBusiness(logger, context);
            this.MarkRecordBusiness = new MarkRecordBusiness(logger, context);
            this.JudgeRecordBusiness = new JudgeRecordBusiness(logger, context);
            repository = ExemptedSubjectRepository;
        }

    }
}