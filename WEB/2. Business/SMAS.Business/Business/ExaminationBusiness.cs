/**
 * The Viettel License
 *
 * Copyright 2012 Viettel Telecom. All rights reserved.
 * VIETTEL PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */

/**
* @author
* @version $Revision: $
*/

using log4net;
using SMAS.Business.IBusiness;
using SMAS.DAL.IRepository;
using SMAS.DAL.Repository;
using SMAS.Models.Models;

namespace SMAS.Business.Business
{
    public partial class ExaminationBusiness : GenericBussiness<Examination>, IExaminationBusiness
    {
        private IExaminationRepository ExaminationRepository;
        private IAcademicYearRepository AcademicYearRepository;
        private ISchoolProfileRepository SchoolProfileRepository;
        private IExaminationSubjectRepository ExaminationSubjectRepository;
        private ICandidateRepository CandidateRepository;
       
        public ExaminationBusiness(ILog logger, SMASEntities context = null)
            : base(logger)
        {
            if (context == null) { context = new SMASEntities(); } this.context = context;
            this.ExaminationRepository = new ExaminationRepository(context);
            this.AcademicYearRepository = new AcademicYearRepository(context);
            this.SchoolProfileRepository = new SchoolProfileRepository(context);
            this.ExaminationSubjectRepository = new ExaminationSubjectRepository(context);
            this.CandidateRepository = new CandidateRepository(context);
            repository = ExaminationRepository;
        }
    }
}