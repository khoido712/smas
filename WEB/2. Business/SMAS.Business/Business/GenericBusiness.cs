﻿/**
 * The Viettel License
 *
 * Copyright 2012 Viettel Telecom. All rights reserved.
 * VIETTEL PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */

/** 
* @author  
* @version $Revision: $
*/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Data.Objects;
using log4net;
using SMAS.DAL.IRepository;
using SMAS.Business.IBusiness;
using SMAS.Models.Models;
using SMAS.Business.Common;
using System.Reflection;
using System.Text.RegularExpressions;
using System.Data.Entity.Infrastructure;
using SMAS.Business.BusinessObject;


namespace SMAS.Business.Business
{
    public class GenericBussiness<T> : IGenericBussiness<T> where T : class
    {
        protected IGenericRepository<T> repository;
        protected ILog logger;
        public SMASEntities context;
        //public string connectionString;
        public GenericBussiness(ILog logger, SMASEntities context = null)
        {
            if (context == null)
            {
                context = new SMASEntities();
            }
            context.Configuration.AutoDetectChangesEnabled = false;
            context.Configuration.ValidateOnSaveEnabled = false;
            this.logger = logger;
            /*if (String.IsNullOrEmpty(connectionString))
            {
                connectionString = System.Web.Configuration.WebConfigurationManager.ConnectionStrings["OraAspNetConnectionString"].ToString();
            }*/
        }
        //add
        public GenericBussiness()
        {

        }
        //

        public virtual IQueryable<T> All
        {
            get { return repository.All; }
        }

        public virtual IQueryable<T> AllNoTracking
        {
            get { return repository.AllNoTracking; }
        }

        public virtual IQueryable<T> AllIncluding(params Expression<Func<T, object>>[] includeProperties)
        {
            return repository.AllIncluding(includeProperties);
        }

        public virtual T Find(object id)
        {
            return repository.Find(id);
        }

        public virtual T Insert(T entityToInsert)
        {
            Type t = entityToInsert.GetType();
            PropertyInfo md = t.GetProperty("CreatedDate");
            if (md != null && md.GetValue(entityToInsert, null) == null)
            {
                md.SetValue(entityToInsert, DateTime.Now, null);
            }
            repository.Insert(entityToInsert);
            return entityToInsert;
        }

        public virtual T Update(T entityToUpdate)
        {
            Type t = entityToUpdate.GetType();
            PropertyInfo md = t.GetProperty("ModifiedDate");
            if (md != null)
            {
                md.SetValue(entityToUpdate, DateTime.Now, null);
            }
            repository.Update(entityToUpdate);
            return entityToUpdate;
        }

        public virtual T BaseUpdate(T entityToUpdate)
        {
            Type t = entityToUpdate.GetType();
            PropertyInfo md = t.GetProperty("ModifiedDate");
            if (md != null)
            {
                md.SetValue(entityToUpdate, DateTime.Now, null);
            }
            repository.Update(entityToUpdate);
            return entityToUpdate;
        }

        public virtual T UpdateNotModifiedDate(T entityToUpdate)
        {
            repository.Update(entityToUpdate);
            return entityToUpdate;
        }

        public virtual void Delete(object id)
        {
            repository.Delete(id);

        }

        public virtual void DeleteAll(List<T> entities)
        {
            repository.DeleteAll(entities);
        }

        public virtual void Save()
        {

            repository.Save();
        }

        public virtual void SaveDetect()
        {
            repository.SaveDetect();
        }

        public virtual void Rollback()
        {

        }
        public void UpdateExistsObjectStateManager(T entityToUpdate)
        {
            repository.UpdateExistsObjectStateManager(entityToUpdate);
        }

        public virtual void Dispose()
        {
            repository.Dispose();
        }

        public virtual void Delete(object id, bool HasIsActive)
        {
            if (HasIsActive)
            {
                T o = repository.Find(id);
                Type t = o.GetType();
                PropertyInfo ia = t.GetProperty("IsActive");
                ia.SetValue(o, false, null);

                PropertyInfo md = t.GetProperty("ModifiedDate");
                if (md != null)
                {
                    md.SetValue(o, DateTime.Now, null);
                }
                repository.Update(o);
            }
            else
            {
                repository.Delete(id);
            }
        }

        public virtual void CheckDuplicate(string valueToCheck, string schemaName, string tableName, string columnName, bool? isActive, int? currentId, string ResKey)
        {
            bool isDuplicate = repository.CheckDuplicate(valueToCheck, schemaName, tableName, columnName, isActive, currentId);
            if (isDuplicate)
            {
                List<object> Params = new List<object>();
                Params.Add(ResKey);
                throw new BusinessException("Common_Validate_Duplicate", Params);
            }
        }

        public virtual void CheckDuplicateCouple(string valueToCheck1, string valueToCheck2, string schemaName, string tableName, string columnName1, string columnName2, bool? isActive, int? currentId, string ResKey)
        {
            bool isDuplicate = repository.CheckDuplicateCouple(valueToCheck1, valueToCheck2, schemaName, tableName, columnName1, columnName2, isActive, currentId);
            if (isDuplicate)
            {
                List<object> Params = new List<object>();
                Params.Add(ResKey);
                throw new BusinessException("Common_Validate_Duplicate", Params);
            }
        }

        public virtual void CheckCompatiable(string valueToCheck1, string valueToCheck2, string schemaName, string tableName, string columnName1, string columnName2, bool? isActive = false)
        {
            bool isCompatiable = repository.CheckDuplicateCouple(valueToCheck1, valueToCheck2, schemaName, tableName, columnName1, columnName2, isActive, 0);
            if (!isCompatiable)
            {
                throw new BusinessException("Common_Validate_DataNotCompatible");
            }
        }

        /// <summary>
        /// Kiểm tra không trùng
        /// </summary>
        /// <param name="valueToCheck1"> valueToCheck1</param>
        /// <param name="valueToCheck2">The value to check2.</param>
        /// <param name="schemaName">Name of the schema.</param>
        /// <param name="tableName">Name of the table.</param>
        /// <param name="columnName1">The column name1.</param>
        /// <param name="columnName2">The column name2.</param>
        /// <param name="isActive">if set to <c>true</c> [is active].</param>
        /// <param name="currentId">The current id.</param>
        /// <param name="ResKey">The res key.</param>
        /// <author>
        /// hath
        /// </author>
        /// <remarks>
        /// 9/6/2012
        /// </remarks>
        public virtual void CheckNotDuplicateCouple(string valueToCheck1, string valueToCheck2, string schemaName, string tableName, string columnName1, string columnName2, bool? isActive, int? currentId, string ResKey)
        {
            bool isDuplicate = repository.CheckDuplicateCouple(valueToCheck1, valueToCheck2, schemaName, tableName, columnName1, columnName2, isActive, currentId);
            if (isDuplicate)
            {
                return;
            }
            else
            {
                List<object> Params = new List<object>();
                Params.Add(ResKey);
                throw new BusinessException("Common_Validate_NotDuplicate", Params);
            }
        }


        public virtual void CheckConstraintsWithExceptTable(string schemaName, string tableName, string ExceptedTableName, int? id, string ResKey)
        {
            bool hasConstraint = repository.CheckConstraintsWithExceptTable(schemaName, tableName, ExceptedTableName, id);
            if (hasConstraint)
            {
                List<object> Params = new List<object>();
                Params.Add(ResKey);
                throw new BusinessException("Common_Validate_Using", Params);
            }
        }

        public virtual void CheckConstraints(string schemaName, string tableName, int? id, string ResKey)
        {
            bool hasConstraint = repository.CheckConstraints(schemaName, tableName, id);
            if (hasConstraint)
            {
                List<object> Params = new List<object>();
                Params.Add(ResKey);
                throw new BusinessException("Common_Validate_Using", Params);
            }
        }

        public virtual void CheckConstraintsNotDelete(string schemaName, string tableName, int? id, string ResKey)
        {
            bool hasConstraint = repository.CheckConstraints(schemaName, tableName, id);
            if (hasConstraint)
            {
                List<object> Params = new List<object>();
                Params.Add(ResKey);
                throw new BusinessException("Common_Validate_Using_Not_Delete", Params);
            }
        }

        public virtual void CheckConstraintsWithIsActive(string schemaName, string tableName, int? id, string ResKey)
        {
            bool hasConstraint = repository.CheckConstraintsWithIsActive(schemaName, tableName, id);
            if (hasConstraint)
            {
                List<object> Params = new List<object>();
                Params.Add(ResKey);
                throw new BusinessException("Common_Validate_Using", Params);
            }
        }
        public virtual void CheckAvailable(int? id, string ResKey, bool isActive = true)
        {
            if (id == null)
                return;
            bool isAvailable = true;
            T o = this.Find(id);
            if (o == null)
            {
                isAvailable = false;
            }
            else if (isActive)
            {
                Type t = o.GetType();
                PropertyInfo p = t.GetProperty("IsActive");
                if (p != null)
                {
                    bool isA = (bool)p.GetValue(o, null);
                    isAvailable = isA;
                }
            }

            if (!isAvailable)
            {
                List<object> Params = new List<object>();
                Params.Add(ResKey);
                throw new BusinessException("Common_Validate_NotAvailable", Params);
            }
        }
        public virtual void CheckAvailable(long? id, string ResKey, bool isActive = true)
        {
            if (id == null)
                return;
            bool isAvailable = true;
            T o = this.Find(id);
            if (o == null)
            {
                isAvailable = false;
            }
            else if (isActive)
            {
                Type t = o.GetType();
                PropertyInfo p = t.GetProperty("IsActive");
                if (p != null)
                {
                    bool isA = (bool)p.GetValue(o, null);
                    isAvailable = isA;
                }
            }

            if (!isAvailable)
            {
                List<object> Params = new List<object>();
                Params.Add(ResKey);
                throw new BusinessException("Common_Validate_NotAvailable", Params);
            }
        }

        /// <summary>
        /// Chiendd
        /// Insert du lieu du sung co che ArrayBindCount cua Oracle
        /// >0: Insert thanh cong, 0: Khong thanh cong
        /// </summary>
        /// <param name="listObj"></param>
        /// <param name="batchSize"></param>
        public virtual int BulkInsert(List<T> lstInsert, IDictionary<string, object> columnMappings, params string[] remvedcolumn)
        {
            if (lstInsert == null || lstInsert.Count == 0)
            {
                return 0;
            }

            List<List<T>> lstVal = UtilsBusiness.SplitListToMultipleList(lstInsert);
            int retVal = 0;
            for (int i = 0; i < lstVal.Count; i++)
            {
                retVal += repository.BulkInsert(lstVal[i], columnMappings, remvedcolumn);
            }
            return retVal;
        }

        /// <summary>
        /// Thuc hien insert va xoa du lieu tu store
        /// </summary>
        /// <param name="lstInsert"></param>
        /// <param name="columnMappings"></param>
        /// <param name="sqlProcedure"></param>
        /// <param name="lstParaDelete"></param>
        /// <param name="remvedcolumn"></param>
        /// <returns></returns>

        public int BulkInsertAndDelete(List<T> lstInsert, IDictionary<string, object> columnMappings, string sqlProcedure, List<CustomType> lstParaDelete, params string[] remvedcolumn)
        {
            if ((lstInsert == null || lstInsert.Count == 0) && (lstParaDelete.Count == 0))
            {
                return 0;
            }
            int retVal = 0;
            retVal = repository.BulkInsertAndDelete(lstInsert, columnMappings, sqlProcedure, lstParaDelete, remvedcolumn);
            /*List<List<T>> lstVal = UtilsBusiness.SplitListToMultipleList(lstInsert,1000);
            
            for (int i = 0; i < lstVal.Count; i++)
            {
                retVal += 
            }*/
            return retVal;
        }


        public int BulkInsert(string stringConnection, List<T> lstInsert, IDictionary<string, object> columnMappings, params string[] remvedcolumn)
        {
            if (lstInsert == null || lstInsert.Count == 0)
            {
                return 0;
            }
            int retVal = 0;
            retVal += repository.BulkInsert(stringConnection, lstInsert, columnMappings, remvedcolumn);

            return retVal;
        }

        public virtual void DeleteMarkRecoreByCondition(int SchoolID, int AcademicYearID, int ClassID, int SubjectID, int Semester,
            List<int> listPupilID, bool? IsMarkedDate = null, int? PeriodID = null, string strMarkTitle = null)
        {
            ObjectContext objectContext = ((IObjectContextAdapter)context).ObjectContext;
            string sqlDelMark = "DELETE FROM " + this.GetTableName() + " WHERE SchoolID = {0} AND AcademicYearID = {1}"
            + " AND ClassID = {2} AND SubjectID = {3} AND Semester = {4}"
            + " AND PupilID in (" + string.Join(",", listPupilID.ToArray()) + ")";
            List<object> listParam = new List<object>();
            listParam.Add(SchoolID);
            listParam.Add(AcademicYearID);
            listParam.Add(ClassID);
            listParam.Add(SubjectID);
            listParam.Add(Semester);
            if (PeriodID.HasValue && strMarkTitle != null)
            {
                sqlDelMark += " AND Title in ('" + strMarkTitle.Replace(",", "','") + "')";
            }
            if (IsMarkedDate.HasValue)
            {
                if (IsMarkedDate.Value)
                {
                    sqlDelMark += " AND MarkedDate is not NULL";
                }
                else
                {
                    sqlDelMark += " AND MarkedDate is null";
                }
            }
            objectContext.ExecuteStoreCommand(sqlDelMark, listParam.ToArray());
        }

        /// <summary>
        /// ham loc ra nhung hoc sinh khong dang ky hoc mon chuyen/mon tu chon
        /// </summary>
        /// <author>HoanTV5</author>
        /// <param name="iqPupilOfClass">danh sach hoc sinh trong lop</param>
        /// <param name="dic">dieu kien dung cho ham lay ds hs dang ky mon chuyen/tu chon:
        ///                   SchoolID,AcademicYearID,ClassID,SubjectID,SemesterID,PartitionID(YearID)</param>
        /// <returns></returns>
        public List<PupilOfClassBO> GetPupilOfClassInSpecialize(IQueryable<PupilOfClassBO> iqPupilOfClass, IDictionary<string, object> dic)
        {
            List<PupilOfClassBO> lstPOC = new List<PupilOfClassBO>();
            List<RegisterSubjectSpecializeBO> lstRegis = RegisterSubjectSpecializeBusiness.GetlistSubjectByClass(dic);
            int semesterID = Utils.GetInt(dic, "SemesterID");
            int countRegis = 0;
            foreach (var item in iqPupilOfClass)
            {
                if (item.AppliedType == GlobalConstants.APPLIED_SUBJECT_ELECTIVE
                    || item.AppliedType == GlobalConstants.APPLIED_SUBJECT_ELECTIVE_PRIORITIE
                    || item.AppliedType == GlobalConstants.APPLIED_SUBJECT_ELECTIVE_SCORE)
                {
                    countRegis = lstRegis.Where(p => p.ClassID == item.ClassID && p.PupilID == item.PupilID && p.SubjectID == item.SubjectID && (semesterID == 3 ? (p.SemesterID == GlobalConstants.SEMESTER_OF_YEAR_FIRST || p.SemesterID == GlobalConstants.SEMESTER_OF_YEAR_SECOND) : p.SemesterID == semesterID)).Count();
                    if (countRegis > 0)
                    {
                        lstPOC.Add(item);
                        continue;
                    }
                    else
                    {
                        continue;
                    }
                }
                lstPOC.Add(item);
            }

            //Danh sach hoc sinh mien giam            
            if (semesterID == 1)
            {
                dic["HasFirstSemester"] = true;
            }
            if (semesterID == 2)
            {
                dic["HasSecondSemester"] = true;
            }
            dic["SemesterID"] = semesterID;
            List<ExemptedSubject> lstExempted = ExemptedSubjectBusiness.Search(dic).ToList();
            //end hoc sinh mien giam

            List<PupilOfClassBO> pupilOfClassRemove = null;
            ExemptedSubject exemptedSubjectTmp = null;
            if (lstExempted != null && lstExempted.Count > 0)
            {
                for (int i = 0; i < lstExempted.Count; i++)
                {
                    exemptedSubjectTmp = lstExempted[i];
                    pupilOfClassRemove = lstPOC.Where(p => p.EducationLevelID == exemptedSubjectTmp.EducationLevelID
                        && p.ClassID == exemptedSubjectTmp.ClassID
                        && p.PupilID == exemptedSubjectTmp.PupilID
                        && p.SubjectID == exemptedSubjectTmp.SubjectID
                        ).ToList();
                    for (int j = 0; j < pupilOfClassRemove.Count; j++)
                    {
                        lstPOC.Remove(pupilOfClassRemove[j]);
                    }
                }
            }
            return lstPOC;
        }



        /// <summary>
        /// QuangLM
        /// Lay ten bang trong CSDL 
        /// </summary>
        /// <returns></returns>
        private string GetTableName()
        {
            ObjectContext objectContext = ((IObjectContextAdapter)context).ObjectContext;
            string sql = objectContext.CreateObjectSet<T>().ToTraceString();
            Regex regex = new Regex("FROM (?<table>.*) ");
            Match match = regex.Match(sql);
            string tableName = match.Groups["table"].Value;
            return tableName;
        }


        public TResult GetNextSeq<TResult>()
        {
            return repository.GetNextSeq<TResult>();
        }
        public TResult GetNextSeq<TResult>(string seqName)
        {
            return repository.GetNextSeq<TResult>(seqName.ToUpper());
        }

        /// <summary>
        /// set isAutoDetectChangesEnabled=false de tang hieu nang khi thuc hien them moi, xoa,sua  
        /// </summary>
        /// <param name="isAutoDetectChangesEnabled"></param>
        public void SetAutoDetectChangesEnabled(bool isAutoDetectChangesEnabled)
        {
            repository.SetAutoDetectChangesEnabled(isAutoDetectChangesEnabled);
        }

        public bool ExcuteSql(List<string> lstSql)
        {
            return repository.ExcuteSql(lstSql);
        }

        #region khai bao business
        private IAcademicYearBusiness _AcademicYearBusiness;
        public IAcademicYearBusiness AcademicYearBusiness
        {
            get
            {
                if (_AcademicYearBusiness == null)
                {
                    _AcademicYearBusiness = new AcademicYearBusiness(this.logger, context);
                }
                return _AcademicYearBusiness;
            }
        }

        private IManagementHealthInsuranceBusiness _ManagementHealthInsuranceBusiness;
        public IManagementHealthInsuranceBusiness ManagementHealthInsuranceBusiness
        {
            get
            {
                if (_ManagementHealthInsuranceBusiness == null)
                {
                    _ManagementHealthInsuranceBusiness = new ManagementHealthInsuranceBusiness(this.logger, context);
                }
                return _ManagementHealthInsuranceBusiness;
            }
        }

        private IPupilRepeatedBusiness _PupilRepeatedBusiness;
        public IPupilRepeatedBusiness PupilRepeatedBusiness
        {
            get
            {
                if (_PupilRepeatedBusiness == null)
                {
                    _PupilRepeatedBusiness = new PupilRepeatedBusiness(this.logger, context);
                }
                return _PupilRepeatedBusiness;
            }
        }

        private IAcademicYearOfProvinceBusiness _AcademicYearOfProvinceBusiness;
        public IAcademicYearOfProvinceBusiness AcademicYearOfProvinceBusiness
        {
            get
            {
                if (_AcademicYearOfProvinceBusiness == null)
                {
                    _AcademicYearOfProvinceBusiness = new AcademicYearOfProvinceBusiness(this.logger, context);
                }
                return _AcademicYearOfProvinceBusiness;
            }
        }

        private IActionAuditBusiness _ActionAuditBusiness;
        public IActionAuditBusiness ActionAuditBusiness
        {
            get
            {
                if (_ActionAuditBusiness == null)
                {
                    _ActionAuditBusiness = new ActionAuditBusiness(this.logger, context);
                }
                return _ActionAuditBusiness;
            }
        }

        private IActivityBusiness _ActivityBusiness;
        public IActivityBusiness ActivityBusiness
        {
            get
            {
                if (_ActivityBusiness == null)
                {
                    _ActivityBusiness = new ActivityBusiness(this.logger, context);
                }
                return _ActivityBusiness;
            }
        }



        private IActivityPlanBusiness _ActivityPlanBusiness;
        public IActivityPlanBusiness ActivityPlanBusiness
        {
            get
            {
                if (_ActivityPlanBusiness == null)
                {
                    _ActivityPlanBusiness = new ActivityPlanBusiness(this.logger, context);
                }
                return _ActivityPlanBusiness;
            }
        }

        private ITeacherNoteBookMonthBusiness _TeacherNotebookMonthBusiness;
        public ITeacherNoteBookMonthBusiness TeacherNoteBookMonthBusiness
        {
            get
            {
                if (_TeacherNotebookMonthBusiness == null)
                {
                    _TeacherNotebookMonthBusiness = new TeacherNoteBookMonthBusiness(this.logger, context);
                }
                return _TeacherNotebookMonthBusiness;
            }
        }

        private ITeacherNoteBookSemesterBusiness _TeacherNotebookSemesterBusiness;
        public ITeacherNoteBookSemesterBusiness TeacherNoteBookSemesterBusiness
        {
            get
            {
                if (_TeacherNotebookSemesterBusiness == null)
                {
                    _TeacherNotebookSemesterBusiness = new TeacherNoteBookSemesterBusiness(this.logger, context);
                }
                return _TeacherNotebookSemesterBusiness;
            }
        }

        private IActivityPlanClassBusiness _ActivityPlanClassBusiness;
        public IActivityPlanClassBusiness ActivityPlanClassBusiness
        {
            get
            {
                if (_ActivityPlanClassBusiness == null)
                {
                    _ActivityPlanClassBusiness = new ActivityPlanClassBusiness(this.logger, context);
                }
                return _ActivityPlanClassBusiness;
            }
        }

        private IActivityTypeBusiness _ActivityTypeBusiness;
        public IActivityTypeBusiness ActivityTypeBusiness
        {
            get
            {
                if (_ActivityTypeBusiness == null)
                {
                    _ActivityTypeBusiness = new ActivityTypeBusiness(this.logger, context);
                }
                return _ActivityTypeBusiness;
            }
        }

        private IAddMenuForChildrenBusiness _AddMenuForChildrenBusiness;
        public IAddMenuForChildrenBusiness AddMenuForChildrenBusiness
        {
            get
            {
                if (_AddMenuForChildrenBusiness == null)
                {
                    _AddMenuForChildrenBusiness = new AddMenuForChildrenBusiness(this.logger, context);
                }
                return _AddMenuForChildrenBusiness;
            }
        }

        private IApprenticeshipClassBusiness _ApprenticeshipClassBusiness;
        public IApprenticeshipClassBusiness ApprenticeshipClassBusiness
        {
            get
            {
                if (_ApprenticeshipClassBusiness == null)
                {
                    _ApprenticeshipClassBusiness = new ApprenticeshipClassBusiness(this.logger, context);
                }
                return _ApprenticeshipClassBusiness;
            }
        }

        private IApprenticeshipGroupBusiness _ApprenticeshipGroupBusiness;
        public IApprenticeshipGroupBusiness ApprenticeshipGroupBusiness
        {
            get
            {
                if (_ApprenticeshipGroupBusiness == null)
                {
                    _ApprenticeshipGroupBusiness = new ApprenticeshipGroupBusiness(this.logger, context);
                }
                return _ApprenticeshipGroupBusiness;
            }
        }

        private ITrainingLevelBusiness _TrainingLevelBusiness;
        public ITrainingLevelBusiness TrainingLevelBusiness
        {
            get
            {
                if (_TrainingLevelBusiness == null)
                {
                    _TrainingLevelBusiness = new TrainingLevelBusiness(this.logger, context);
                }
                return _TrainingLevelBusiness;
            }
        }



        private IApprenticeshipSubjectBusiness _ApprenticeshipSubjectBusiness;
        public IApprenticeshipSubjectBusiness ApprenticeshipSubjectBusiness
        {
            get
            {
                if (_ApprenticeshipSubjectBusiness == null)
                {
                    _ApprenticeshipSubjectBusiness = new ApprenticeshipSubjectBusiness(this.logger, context);
                }
                return _ApprenticeshipSubjectBusiness;
            }
        }

        private IApprenticeshipTrainingAbsenceBusiness _ApprenticeshipTrainingAbsenceBusiness;
        public IApprenticeshipTrainingAbsenceBusiness ApprenticeshipTrainingAbsenceBusiness
        {
            get
            {
                if (_ApprenticeshipTrainingAbsenceBusiness == null)
                {
                    _ApprenticeshipTrainingAbsenceBusiness = new ApprenticeshipTrainingAbsenceBusiness(this.logger, context);
                }
                return _ApprenticeshipTrainingAbsenceBusiness;
            }
        }

        private IApprenticeshipTrainingBusiness _ApprenticeshipTrainingBusiness;
        public IApprenticeshipTrainingBusiness ApprenticeshipTrainingBusiness
        {
            get
            {
                if (_ApprenticeshipTrainingBusiness == null)
                {
                    _ApprenticeshipTrainingBusiness = new ApprenticeshipTrainingBusiness(this.logger, context);
                }
                return _ApprenticeshipTrainingBusiness;
            }
        }

        private IApprenticeshipTrainingScheduleBusiness _ApprenticeshipTrainingScheduleBusiness;
        public IApprenticeshipTrainingScheduleBusiness ApprenticeshipTrainingScheduleBusiness
        {
            get
            {
                if (_ApprenticeshipTrainingScheduleBusiness == null)
                {
                    _ApprenticeshipTrainingScheduleBusiness = new ApprenticeshipTrainingScheduleBusiness(this.logger, context);
                }
                return _ApprenticeshipTrainingScheduleBusiness;
            }
        }

        private IAreaBusiness _AreaBusiness;
        public IAreaBusiness AreaBusiness
        {
            get
            {
                if (_AreaBusiness == null)
                {
                    _AreaBusiness = new AreaBusiness(this.logger, context);
                }
                return _AreaBusiness;
            }
        }

        private Iaspnet_ApplicationsBusiness _aspnet_ApplicationsBusiness;
        public Iaspnet_ApplicationsBusiness aspnet_ApplicationsBusiness
        {
            get
            {
                if (_aspnet_ApplicationsBusiness == null)
                {
                    _aspnet_ApplicationsBusiness = new aspnet_ApplicationsBusiness(this.logger, context);
                }
                return _aspnet_ApplicationsBusiness;
            }
        }

        private Iaspnet_MembershipBusiness _aspnet_MembershipBusiness;
        public Iaspnet_MembershipBusiness aspnet_MembershipBusiness
        {
            get
            {
                if (_aspnet_MembershipBusiness == null)
                {
                    _aspnet_MembershipBusiness = new aspnet_MembershipBusiness(this.logger, context);
                }
                return _aspnet_MembershipBusiness;
            }
        }

        //private Iaspnet_SchemaVersionsBusiness _aspnet_SchemaVersionsBusiness;
        //public Iaspnet_SchemaVersionsBusiness aspnet_SchemaVersionsBusiness
        //{
        //    get
        //    {
        //        if (_aspnet_SchemaVersionsBusiness == null)
        //        {
        //            _aspnet_SchemaVersionsBusiness = new aspnet_SchemaVersionsBusiness(this.logger, context);
        //        }
        //        return _aspnet_SchemaVersionsBusiness;
        //    }
        //}

        private Iaspnet_UsersBusiness _aspnet_UsersBusiness;
        public Iaspnet_UsersBusiness aspnet_UsersBusiness
        {
            get
            {
                if (_aspnet_UsersBusiness == null)
                {
                    _aspnet_UsersBusiness = new aspnet_UsersBusiness(this.logger, context);
                }
                return _aspnet_UsersBusiness;
            }
        }

        private IAverageMarkBySubjectBusiness _AverageMarkBySubjectBusiness;
        public IAverageMarkBySubjectBusiness AverageMarkBySubjectBusiness
        {
            get
            {
                if (_AverageMarkBySubjectBusiness == null)
                {
                    _AverageMarkBySubjectBusiness = new AverageMarkBySubjectBusiness(this.logger, context);
                }
                return _AverageMarkBySubjectBusiness;
            }
        }

        private ICalendarBusiness _CalendarBusiness;
        public ICalendarBusiness CalendarBusiness
        {
            get
            {
                if (_CalendarBusiness == null)
                {
                    _CalendarBusiness = new CalendarBusiness(this.logger, context);
                }
                return _CalendarBusiness;
            }
        }

        private ICandidateAbsenceBusiness _CandidateAbsenceBusiness;
        public ICandidateAbsenceBusiness CandidateAbsenceBusiness
        {
            get
            {
                if (_CandidateAbsenceBusiness == null)
                {
                    _CandidateAbsenceBusiness = new CandidateAbsenceBusiness(this.logger, context);
                }
                return _CandidateAbsenceBusiness;
            }
        }

        private ICandidateBusiness _CandidateBusiness;
        public ICandidateBusiness CandidateBusiness
        {
            get
            {
                if (_CandidateBusiness == null)
                {
                    _CandidateBusiness = new CandidateBusiness(this.logger, context);
                }
                return _CandidateBusiness;
            }
        }

        private ICapacityLevelBusiness _CapacityLevelBusiness;
        public ICapacityLevelBusiness CapacityLevelBusiness
        {
            get
            {
                if (_CapacityLevelBusiness == null)
                {
                    _CapacityLevelBusiness = new CapacityLevelBusiness(this.logger, context);
                }
                return _CapacityLevelBusiness;
            }
        }

        private ICapacityStatisticBusiness _CapacityStatisticBusiness;
        public ICapacityStatisticBusiness CapacityStatisticBusiness
        {
            get
            {
                if (_CapacityStatisticBusiness == null)
                {
                    _CapacityStatisticBusiness = new CapacityStatisticBusiness(this.logger, context);
                }
                return _CapacityStatisticBusiness;
            }
        }

        private IClassAssigmentBusiness _ClassAssigmentBusiness;
        public IClassAssigmentBusiness ClassAssigmentBusiness
        {
            get
            {
                if (_ClassAssigmentBusiness == null)
                {
                    _ClassAssigmentBusiness = new ClassAssigmentBusiness(this.logger, context);
                }
                return _ClassAssigmentBusiness;
            }
        }

        private IClassEmulationBusiness _ClassEmulationBusiness;
        public IClassEmulationBusiness ClassEmulationBusiness
        {
            get
            {
                if (_ClassEmulationBusiness == null)
                {
                    _ClassEmulationBusiness = new ClassEmulationBusiness(this.logger, context);
                }
                return _ClassEmulationBusiness;
            }
        }

        private IClassEmulationDetailBusiness _ClassEmulationDetailBusiness;
        public IClassEmulationDetailBusiness ClassEmulationDetailBusiness
        {
            get
            {
                if (_ClassEmulationDetailBusiness == null)
                {
                    _ClassEmulationDetailBusiness = new ClassEmulationDetailBusiness(this.logger, context);
                }
                return _ClassEmulationDetailBusiness;
            }
        }

        private IClassForwardingBusiness _ClassForwardingBusiness;
        public IClassForwardingBusiness ClassForwardingBusiness
        {
            get
            {
                if (_ClassForwardingBusiness == null)
                {
                    _ClassForwardingBusiness = new ClassForwardingBusiness(this.logger, context);
                }
                return _ClassForwardingBusiness;
            }
        }

        private IClassificationLevelHealthBusiness _ClassificationLevelHealthBusiness;
        public IClassificationLevelHealthBusiness ClassificationLevelHealthBusiness
        {
            get
            {
                if (_ClassificationLevelHealthBusiness == null)
                {
                    _ClassificationLevelHealthBusiness = new ClassificationLevelHealthBusiness(this.logger, context);
                }
                return _ClassificationLevelHealthBusiness;
            }
        }

        private IClassifiedCapacityBusiness _ClassifiedCapacityBusiness;
        public IClassifiedCapacityBusiness ClassifiedCapacityBusiness
        {
            get
            {
                if (_ClassifiedCapacityBusiness == null)
                {
                    _ClassifiedCapacityBusiness = new ClassifiedCapacityBusiness(this.logger, context);
                }
                return _ClassifiedCapacityBusiness;
            }
        }

        private IClassifiedCapacityBySubjectBusiness _ClassifiedCapacityBySubjectBusiness;
        public IClassifiedCapacityBySubjectBusiness ClassifiedCapacityBySubjectBusiness
        {
            get
            {
                if (_ClassifiedCapacityBySubjectBusiness == null)
                {
                    _ClassifiedCapacityBySubjectBusiness = new ClassifiedCapacityBySubjectBusiness(this.logger, context);
                }
                return _ClassifiedCapacityBySubjectBusiness;
            }
        }

        private IClassifiedConductBusiness _ClassifiedConductBusiness;
        public IClassifiedConductBusiness ClassifiedConductBusiness
        {
            get
            {
                if (_ClassifiedConductBusiness == null)
                {
                    _ClassifiedConductBusiness = new ClassifiedConductBusiness(this.logger, context);
                }
                return _ClassifiedConductBusiness;
            }
        }

        private IClassMovementBusiness _ClassMovementBusiness;
        public IClassMovementBusiness ClassMovementBusiness
        {
            get
            {
                if (_ClassMovementBusiness == null)
                {
                    _ClassMovementBusiness = new ClassMovementBusiness(this.logger, context);
                }
                return _ClassMovementBusiness;
            }
        }

        private IClassProfileBusiness _ClassProfileBusiness;
        public IClassProfileBusiness ClassProfileBusiness
        {
            get
            {
                if (_ClassProfileBusiness == null)
                {
                    _ClassProfileBusiness = new ClassProfileBusiness(this.logger, context);
                }
                return _ClassProfileBusiness;
            }
        }
        private ITrainingProgramBusiness _TrainingProgramBusiness;
        public ITrainingProgramBusiness TrainingProgramBusiness
        {
            get
            {
                if (_TrainingProgramBusiness == null)
                {
                    _TrainingProgramBusiness = new TrainingProgramBusiness(this.logger, context);
                }
                return _TrainingProgramBusiness;
            }
        }
        private IClassPropertyCatBusiness _ClassPropertyCatBusiness;
        public IClassPropertyCatBusiness ClassPropertyCatBusiness
        {
            get
            {
                if (_ClassPropertyCatBusiness == null)
                {
                    _ClassPropertyCatBusiness = new ClassPropertyCatBusiness(this.logger, context);
                }
                return _ClassPropertyCatBusiness;
            }
        }
        private IClassSpecialtyCatBusiness _ClassSpecialtyCatBusiness;
        public IClassSpecialtyCatBusiness ClassSpecialtyCatBusiness
        {
            get
            {
                if (_ClassSpecialtyCatBusiness == null)
                {
                    _ClassSpecialtyCatBusiness = new ClassSpecialtyCatBusiness(this.logger, context);
                }
                return _ClassSpecialtyCatBusiness;
            }
        }
        private IClassSubjectBusiness _ClassSubjectBusiness;
        public IClassSubjectBusiness ClassSubjectBusiness
        {
            get
            {
                if (_ClassSubjectBusiness == null)
                {
                    _ClassSubjectBusiness = new ClassSubjectBusiness(this.logger, context);
                }
                return _ClassSubjectBusiness;
            }
        }

        private IClassSupervisorAssignmentBusiness _ClassSupervisorAssignmentBusiness;
        public IClassSupervisorAssignmentBusiness ClassSupervisorAssignmentBusiness
        {
            get
            {
                if (_ClassSupervisorAssignmentBusiness == null)
                {
                    _ClassSupervisorAssignmentBusiness = new ClassSupervisorAssignmentBusiness(this.logger, context);
                }
                return _ClassSupervisorAssignmentBusiness;
            }
        }

        private ICodeConfigBusiness _CodeConfigBusiness;
        public ICodeConfigBusiness CodeConfigBusiness
        {
            get
            {
                if (_CodeConfigBusiness == null)
                {
                    _CodeConfigBusiness = new CodeConfigBusiness(this.logger, context);
                }
                return _CodeConfigBusiness;
            }
        }

        private ICommuneBusiness _CommuneBusiness;
        public ICommuneBusiness CommuneBusiness
        {
            get
            {
                if (_CommuneBusiness == null)
                {
                    _CommuneBusiness = new CommuneBusiness(this.logger, context);
                }
                return _CommuneBusiness;
            }
        }

        private IConcurrentWorkAssignmentBusiness _ConcurrentWorkAssignmentBusiness;
        public IConcurrentWorkAssignmentBusiness ConcurrentWorkAssignmentBusiness
        {
            get
            {
                if (_ConcurrentWorkAssignmentBusiness == null)
                {
                    _ConcurrentWorkAssignmentBusiness = new ConcurrentWorkAssignmentBusiness(this.logger, context);
                }
                return _ConcurrentWorkAssignmentBusiness;
            }
        }

        private IConcurrentWorkReplacementBusiness _ConcurrentWorkReplacementBusiness;
        public IConcurrentWorkReplacementBusiness ConcurrentWorkReplacementBusiness
        {
            get
            {
                if (_ConcurrentWorkReplacementBusiness == null)
                {
                    _ConcurrentWorkReplacementBusiness = new ConcurrentWorkReplacementBusiness(this.logger, context);
                }
                return _ConcurrentWorkReplacementBusiness;
            }
        }

        private IConcurrentWorkTypeBusiness _ConcurrentWorkTypeBusiness;
        public IConcurrentWorkTypeBusiness ConcurrentWorkTypeBusiness
        {
            get
            {
                if (_ConcurrentWorkTypeBusiness == null)
                {
                    _ConcurrentWorkTypeBusiness = new ConcurrentWorkTypeBusiness(this.logger, context);
                }
                return _ConcurrentWorkTypeBusiness;
            }
        }

        private IConductConfigByCapacityBusiness _ConductConfigByCapacityBusiness;
        public IConductConfigByCapacityBusiness ConductConfigByCapacityBusiness
        {
            get
            {
                if (_ConductConfigByCapacityBusiness == null)
                {
                    _ConductConfigByCapacityBusiness = new ConductConfigByCapacityBusiness(this.logger, context);
                }
                return _ConductConfigByCapacityBusiness;
            }
        }

        private IConductConfigByViolationBusiness _ConductConfigByViolationBusiness;
        public IConductConfigByViolationBusiness ConductConfigByViolationBusiness
        {
            get
            {
                if (_ConductConfigByViolationBusiness == null)
                {
                    _ConductConfigByViolationBusiness = new ConductConfigByViolationBusiness(this.logger, context);
                }
                return _ConductConfigByViolationBusiness;
            }
        }

        private IConductEvaluationDetailBusiness _ConductEvaluationDetailBusiness;
        public IConductEvaluationDetailBusiness ConductEvaluationDetailBusiness
        {
            get
            {
                if (_ConductEvaluationDetailBusiness == null)
                {
                    _ConductEvaluationDetailBusiness = new ConductEvaluationDetailBusiness(this.logger, context);
                }
                return _ConductEvaluationDetailBusiness;
            }
        }

        private IConductLevelBusiness _ConductLevelBusiness;
        public IConductLevelBusiness ConductLevelBusiness
        {
            get
            {
                if (_ConductLevelBusiness == null)
                {
                    _ConductLevelBusiness = new ConductLevelBusiness(this.logger, context);
                }
                return _ConductLevelBusiness;
            }
        }

        private IConductStatisticBusiness _ConductStatisticBusiness;
        public IConductStatisticBusiness ConductStatisticBusiness
        {
            get
            {
                if (_ConductStatisticBusiness == null)
                {
                    _ConductStatisticBusiness = new ConductStatisticBusiness(this.logger, context);
                }
                return _ConductStatisticBusiness;
            }
        }

        private IContractBusiness _ContractBusiness;
        public IContractBusiness ContractBusiness
        {
            get
            {
                if (_ContractBusiness == null)
                {
                    _ContractBusiness = new ContractBusiness(this.logger, context);
                }
                return _ContractBusiness;
            }
        }

        private IContractTypeBusiness _ContractTypeBusiness;
        public IContractTypeBusiness ContractTypeBusiness
        {
            get
            {
                if (_ContractTypeBusiness == null)
                {
                    _ContractTypeBusiness = new ContractTypeBusiness(this.logger, context);
                }
                return _ContractTypeBusiness;
            }
        }

        private IDailyDishCostBusiness _DailyDishCostBusiness;
        public IDailyDishCostBusiness DailyDishCostBusiness
        {
            get
            {
                if (_DailyDishCostBusiness == null)
                {
                    _DailyDishCostBusiness = new DailyDishCostBusiness(this.logger, context);
                }
                return _DailyDishCostBusiness;
            }
        }

        private IDailyExpenseBusiness _DailyExpenseBusiness;
        public IDailyExpenseBusiness DailyExpenseBusiness
        {
            get
            {
                if (_DailyExpenseBusiness == null)
                {
                    _DailyExpenseBusiness = new DailyExpenseBusiness(this.logger, context);
                }
                return _DailyExpenseBusiness;
            }
        }

        private IDailyFoodInspectionBusiness _DailyFoodInspectionBusiness;
        public IDailyFoodInspectionBusiness DailyFoodInspectionBusiness
        {
            get
            {
                if (_DailyFoodInspectionBusiness == null)
                {
                    _DailyFoodInspectionBusiness = new DailyFoodInspectionBusiness(this.logger, context);
                }
                return _DailyFoodInspectionBusiness;
            }
        }

        private IDailyMenuBusiness _DailyMenuBusiness;
        public IDailyMenuBusiness DailyMenuBusiness
        {
            get
            {
                if (_DailyMenuBusiness == null)
                {
                    _DailyMenuBusiness = new DailyMenuBusiness(this.logger, context);
                }
                return _DailyMenuBusiness;
            }
        }

        private IDailyMenuDetailBusiness _DailyMenuDetailBusiness;
        public IDailyMenuDetailBusiness DailyMenuDetailBusiness
        {
            get
            {
                if (_DailyMenuDetailBusiness == null)
                {
                    _DailyMenuDetailBusiness = new DailyMenuDetailBusiness(this.logger, context);
                }
                return _DailyMenuDetailBusiness;
            }
        }

        private IDailyMenuFoodBusiness _DailyMenuFoodBusiness;
        public IDailyMenuFoodBusiness DailyMenuFoodBusiness
        {
            get
            {
                if (_DailyMenuFoodBusiness == null)
                {
                    _DailyMenuFoodBusiness = new DailyMenuFoodBusiness(this.logger, context);
                }
                return _DailyMenuFoodBusiness;
            }
        }

        private IDailyOtherServiceInspectionBusiness _DailyOtherServiceInspectionBusiness;
        public IDailyOtherServiceInspectionBusiness DailyOtherServiceInspectionBusiness
        {
            get
            {
                if (_DailyOtherServiceInspectionBusiness == null)
                {
                    _DailyOtherServiceInspectionBusiness = new DailyOtherServiceInspectionBusiness(this.logger, context);
                }
                return _DailyOtherServiceInspectionBusiness;
            }
        }

        private IDefaultGroupBusiness _DefaultGroupBusiness;
        public IDefaultGroupBusiness DefaultGroupBusiness
        {
            get
            {
                if (_DefaultGroupBusiness == null)
                {
                    _DefaultGroupBusiness = new DefaultGroupBusiness(this.logger, context);
                }
                return _DefaultGroupBusiness;
            }
        }

        private IDefaultGroupMenuBusiness _DefaultGroupMenuBusiness;
        public IDefaultGroupMenuBusiness DefaultGroupMenuBusiness
        {
            get
            {
                if (_DefaultGroupMenuBusiness == null)
                {
                    _DefaultGroupMenuBusiness = new DefaultGroupMenuBusiness(this.logger, context);
                }
                return _DefaultGroupMenuBusiness;
            }
        }

        private IDetachableHeadBagBusiness _DetachableHeadBagBusiness;
        public IDetachableHeadBagBusiness DetachableHeadBagBusiness
        {
            get
            {
                if (_DetachableHeadBagBusiness == null)
                {
                    _DetachableHeadBagBusiness = new DetachableHeadBagBusiness(this.logger, context);
                }
                return _DetachableHeadBagBusiness;
            }
        }

        private IDetachableHeadMappingBusiness _DetachableHeadMappingBusiness;
        public IDetachableHeadMappingBusiness DetachableHeadMappingBusiness
        {
            get
            {
                if (_DetachableHeadMappingBusiness == null)
                {
                    _DetachableHeadMappingBusiness = new DetachableHeadMappingBusiness(this.logger, context);
                }
                return _DetachableHeadMappingBusiness;
            }
        }

        private IDetailClassifiedBySubjectBusiness _DetailClassifiedBySubjectBusiness;
        public IDetailClassifiedBySubjectBusiness DetailClassifiedBySubjectBusiness
        {
            get
            {
                if (_DetailClassifiedBySubjectBusiness == null)
                {
                    _DetailClassifiedBySubjectBusiness = new DetailClassifiedBySubjectBusiness(this.logger, context);
                }
                return _DetailClassifiedBySubjectBusiness;
            }
        }

        private IDevelopmentOfChildrenBusiness _DevelopmentOfChildrenBusiness;
        public IDevelopmentOfChildrenBusiness DevelopmentOfChildrenBusiness
        {
            get
            {
                if (_DevelopmentOfChildrenBusiness == null)
                {
                    _DevelopmentOfChildrenBusiness = new DevelopmentOfChildrenBusiness(this.logger, context);
                }
                return _DevelopmentOfChildrenBusiness;
            }
        }

        private IDevelopmentOfChildrenReportBusiness _DevelopmentOfChildrenReportBusiness;
        public IDevelopmentOfChildrenReportBusiness DevelopmentOfChildrenReportBusiness
        {
            get
            {
                if (_DevelopmentOfChildrenReportBusiness == null)
                {
                    _DevelopmentOfChildrenReportBusiness = new DevelopmentOfChildrenReportBusiness(this.logger, context);
                }
                return _DevelopmentOfChildrenReportBusiness;
            }
        }

        private IDisabledTypeBusiness _DisabledTypeBusiness;
        public IDisabledTypeBusiness DisabledTypeBusiness
        {
            get
            {
                if (_DisabledTypeBusiness == null)
                {
                    _DisabledTypeBusiness = new DisabledTypeBusiness(this.logger, context);
                }
                return _DisabledTypeBusiness;
            }
        }

        private IDisciplineTypeBusiness _DisciplineTypeBusiness;
        public IDisciplineTypeBusiness DisciplineTypeBusiness
        {
            get
            {
                if (_DisciplineTypeBusiness == null)
                {
                    _DisciplineTypeBusiness = new DisciplineTypeBusiness(this.logger, context);
                }
                return _DisciplineTypeBusiness;
            }
        }

        private IDishCatBusiness _DishCatBusiness;
        public IDishCatBusiness DishCatBusiness
        {
            get
            {
                if (_DishCatBusiness == null)
                {
                    _DishCatBusiness = new DishCatBusiness(this.logger, context);
                }
                return _DishCatBusiness;
            }
        }

        private IDishDetailBusiness _DishDetailBusiness;
        public IDishDetailBusiness DishDetailBusiness
        {
            get
            {
                if (_DishDetailBusiness == null)
                {
                    _DishDetailBusiness = new DishDetailBusiness(this.logger, context);
                }
                return _DishDetailBusiness;
            }
        }

        private IDishInspectionBusiness _DishInspectionBusiness;
        public IDishInspectionBusiness DishInspectionBusiness
        {
            get
            {
                if (_DishInspectionBusiness == null)
                {
                    _DishInspectionBusiness = new DishInspectionBusiness(this.logger, context);
                }
                return _DishInspectionBusiness;
            }
        }

        private IReportPreliminarySendSGDBusiness _ReportPreliminarySendSGDBusiness;
        public IReportPreliminarySendSGDBusiness ReportPreliminarySendSGDBusiness
        {
            get
            {
                if (_ReportPreliminarySendSGDBusiness == null)
                {
                    _ReportPreliminarySendSGDBusiness = new ReportPreliminarySendSGDBusiness(this.logger, context);
                }
                return _ReportPreliminarySendSGDBusiness;
            }
        }

        private IDistrictBusiness _DistrictBusiness;
        public IDistrictBusiness DistrictBusiness
        {
            get
            {
                if (_DistrictBusiness == null)
                {
                    _DistrictBusiness = new DistrictBusiness(this.logger, context);
                }
                return _DistrictBusiness;
            }
        }

        private IEatingGroupBusiness _EatingGroupBusiness;
        public IEatingGroupBusiness EatingGroupBusiness
        {
            get
            {
                if (_EatingGroupBusiness == null)
                {
                    _EatingGroupBusiness = new EatingGroupBusiness(this.logger, context);
                }
                return _EatingGroupBusiness;
            }
        }

        private IEatingGroupClassBusiness _EatingGroupClassBusiness;
        public IEatingGroupClassBusiness EatingGroupClassBusiness
        {
            get
            {
                if (_EatingGroupClassBusiness == null)
                {
                    _EatingGroupClassBusiness = new EatingGroupClassBusiness(this.logger, context);
                }
                return _EatingGroupClassBusiness;
            }
        }

        private IEducationalManagementGradeBusiness _EducationalManagementGradeBusiness;
        public IEducationalManagementGradeBusiness EducationalManagementGradeBusiness
        {
            get
            {
                if (_EducationalManagementGradeBusiness == null)
                {
                    _EducationalManagementGradeBusiness = new EducationalManagementGradeBusiness(this.logger, context);
                }
                return _EducationalManagementGradeBusiness;
            }
        }

        private IEducationLevelBusiness _EducationLevelBusiness;
        public IEducationLevelBusiness EducationLevelBusiness
        {
            get
            {
                if (_EducationLevelBusiness == null)
                {
                    _EducationLevelBusiness = new EducationLevelBusiness(this.logger, context);
                }
                return _EducationLevelBusiness;
            }
        }



        private IEmployeeBusiness _EmployeeBusiness;
        public IEmployeeBusiness EmployeeBusiness
        {
            get
            {
                if (_EmployeeBusiness == null)
                {
                    _EmployeeBusiness = new EmployeeBusiness(this.logger, context);
                }
                return _EmployeeBusiness;
            }
        }

        private IEmployeeHistoryStatusBusiness _EmployeeHistoryStatusBusiness;
        public IEmployeeHistoryStatusBusiness EmployeeHistoryStatusBusiness
        {
            get
            {
                if (_EmployeeHistoryStatusBusiness == null)
                {
                    _EmployeeHistoryStatusBusiness = new EmployeeHistoryStatusBusiness(this.logger, context);
                }
                return _EmployeeHistoryStatusBusiness;
            }
        }

        private IEmployeePraiseDisciplineBusiness _EmployeePraiseDisciplineBusiness;
        public IEmployeePraiseDisciplineBusiness EmployeePraiseDisciplineBusiness
        {
            get
            {
                if (_EmployeePraiseDisciplineBusiness == null)
                {
                    _EmployeePraiseDisciplineBusiness = new EmployeePraiseDisciplineBusiness(this.logger, context);
                }
                return _EmployeePraiseDisciplineBusiness;
            }
        }

        private IEmployeeQualificationBusiness _EmployeeQualificationBusiness;
        public IEmployeeQualificationBusiness EmployeeQualificationBusiness
        {
            get
            {
                if (_EmployeeQualificationBusiness == null)
                {
                    _EmployeeQualificationBusiness = new EmployeeQualificationBusiness(this.logger, context);
                }
                return _EmployeeQualificationBusiness;
            }
        }

        private IEmployeeSalaryBusiness _EmployeeSalaryBusiness;
        public IEmployeeSalaryBusiness EmployeeSalaryBusiness
        {
            get
            {
                if (_EmployeeSalaryBusiness == null)
                {
                    _EmployeeSalaryBusiness = new EmployeeSalaryBusiness(this.logger, context);
                }
                return _EmployeeSalaryBusiness;
            }
        }

        private IEmployeeScaleBusiness _EmployeeScaleBusiness;
        public IEmployeeScaleBusiness EmployeeScaleBusiness
        {
            get
            {
                if (_EmployeeScaleBusiness == null)
                {
                    _EmployeeScaleBusiness = new EmployeeScaleBusiness(this.logger, context);
                }
                return _EmployeeScaleBusiness;
            }
        }

        private IEmployeeWorkingHistoryBusiness _EmployeeWorkingHistoryBusiness;
        public IEmployeeWorkingHistoryBusiness EmployeeWorkingHistoryBusiness
        {
            get
            {
                if (_EmployeeWorkingHistoryBusiness == null)
                {
                    _EmployeeWorkingHistoryBusiness = new EmployeeWorkingHistoryBusiness(this.logger, context);
                }
                return _EmployeeWorkingHistoryBusiness;
            }
        }

        private IEmployeeWorkMovementBusiness _EmployeeWorkMovementBusiness;
        public IEmployeeWorkMovementBusiness EmployeeWorkMovementBusiness
        {
            get
            {
                if (_EmployeeWorkMovementBusiness == null)
                {
                    _EmployeeWorkMovementBusiness = new EmployeeWorkMovementBusiness(this.logger, context);
                }
                return _EmployeeWorkMovementBusiness;
            }
        }

        private IEmployeeWorkTypeBusiness _EmployeeWorkTypeBusiness;
        public IEmployeeWorkTypeBusiness EmployeeWorkTypeBusiness
        {
            get
            {
                if (_EmployeeWorkTypeBusiness == null)
                {
                    _EmployeeWorkTypeBusiness = new EmployeeWorkTypeBusiness(this.logger, context);
                }
                return _EmployeeWorkTypeBusiness;
            }
        }

        private IEmulationCriteriaBusiness _EmulationCriteriaBusiness;
        public IEmulationCriteriaBusiness EmulationCriteriaBusiness
        {
            get
            {
                if (_EmulationCriteriaBusiness == null)
                {
                    _EmulationCriteriaBusiness = new EmulationCriteriaBusiness(this.logger, context);
                }
                return _EmulationCriteriaBusiness;
            }
        }

        private IEmulationTitleBusiness _EmulationTitleBusiness;
        public IEmulationTitleBusiness EmulationTitleBusiness
        {
            get
            {
                if (_EmulationTitleBusiness == null)
                {
                    _EmulationTitleBusiness = new EmulationTitleBusiness(this.logger, context);
                }
                return _EmulationTitleBusiness;
            }
        }

        private IEnergyDistributionBusiness _EnergyDistributionBusiness;
        public IEnergyDistributionBusiness EnergyDistributionBusiness
        {
            get
            {
                if (_EnergyDistributionBusiness == null)
                {
                    _EnergyDistributionBusiness = new EnergyDistributionBusiness(this.logger, context);
                }
                return _EnergyDistributionBusiness;
            }
        }

        private IEthnicBusiness _EthnicBusiness;
        public IEthnicBusiness EthnicBusiness
        {
            get
            {
                if (_EthnicBusiness == null)
                {
                    _EthnicBusiness = new EthnicBusiness(this.logger, context);
                }
                return _EthnicBusiness;
            }
        }

        private IEvaluationConfigBusiness _EvaluationConfigBusiness;
        public IEvaluationConfigBusiness EvaluationConfigBusiness
        {
            get
            {
                if (_EvaluationConfigBusiness == null)
                {
                    _EvaluationConfigBusiness = new EvaluationConfigBusiness(this.logger, context);
                }
                return _EvaluationConfigBusiness;
            }
        }

        private IEvaluationDevelopmentBusiness _EvaluationDevelopmentBusiness;
        public IEvaluationDevelopmentBusiness EvaluationDevelopmentBusiness
        {
            get
            {
                if (_EvaluationDevelopmentBusiness == null)
                {
                    _EvaluationDevelopmentBusiness = new EvaluationDevelopmentBusiness(this.logger, context);
                }
                return _EvaluationDevelopmentBusiness;
            }
        }

        private IEvaluationDevelopmentGroupBusiness _EvaluationDevelopmentGroupBusiness;
        public IEvaluationDevelopmentGroupBusiness EvaluationDevelopmentGroupBusiness
        {
            get
            {
                if (_EvaluationDevelopmentGroupBusiness == null)
                {
                    _EvaluationDevelopmentGroupBusiness = new EvaluationDevelopmentGroupBusiness(this.logger, context);
                }
                return _EvaluationDevelopmentGroupBusiness;
            }
        }



        private IDeclareEvaluationGroupBusiness _DeclareEvaluationGroupBusiness;
        public IDeclareEvaluationGroupBusiness DeclareEvaluationGroupBusiness
        {
            get
            {
                if (_DeclareEvaluationGroupBusiness == null)
                {
                    _DeclareEvaluationGroupBusiness = new DeclareEvaluationGroupBusiness(this.logger, context);
                }
                return _DeclareEvaluationGroupBusiness;
            }
        }

        private IDeclareEvaluationIndexBusiness _DeclareEvaluationIndexBusiness;
        public IDeclareEvaluationIndexBusiness DeclareEvaluationIndexBusiness
        {
            get
            {
                if (_DeclareEvaluationIndexBusiness == null)
                {
                    _DeclareEvaluationIndexBusiness = new DeclareEvaluationIndexBusiness(this.logger, context);
                }
                return _DeclareEvaluationIndexBusiness;
            }
        }

        private IExaminationBusiness _ExaminationBusiness;
        public IExaminationBusiness ExaminationBusiness
        {
            get
            {
                if (_ExaminationBusiness == null)
                {
                    _ExaminationBusiness = new ExaminationBusiness(this.logger, context);
                }
                return _ExaminationBusiness;
            }
        }

        private IExaminationRoomBusiness _ExaminationRoomBusiness;
        public IExaminationRoomBusiness ExaminationRoomBusiness
        {
            get
            {
                if (_ExaminationRoomBusiness == null)
                {
                    _ExaminationRoomBusiness = new ExaminationRoomBusiness(this.logger, context);
                }
                return _ExaminationRoomBusiness;
            }
        }

        private IExaminationSubjectBusiness _ExaminationSubjectBusiness;
        public IExaminationSubjectBusiness ExaminationSubjectBusiness
        {
            get
            {
                if (_ExaminationSubjectBusiness == null)
                {
                    _ExaminationSubjectBusiness = new ExaminationSubjectBusiness(this.logger, context);
                }
                return _ExaminationSubjectBusiness;
            }
        }

        private IExamViolationTypeBusiness _ExamViolationTypeBusiness;
        public IExamViolationTypeBusiness ExamViolationTypeBusiness
        {
            get
            {
                if (_ExamViolationTypeBusiness == null)
                {
                    _ExamViolationTypeBusiness = new ExamViolationTypeBusiness(this.logger, context);
                }
                return _ExamViolationTypeBusiness;
            }
        }

        private IExemptedObjectTypeBusiness _ExemptedObjectTypeBusiness;
        public IExemptedObjectTypeBusiness ExemptedObjectTypeBusiness
        {
            get
            {
                if (_ExemptedObjectTypeBusiness == null)
                {
                    _ExemptedObjectTypeBusiness = new ExemptedObjectTypeBusiness(this.logger, context);
                }
                return _ExemptedObjectTypeBusiness;
            }
        }

        private IExemptedSubjectBusiness _ExemptedSubjectBusiness;
        public IExemptedSubjectBusiness ExemptedSubjectBusiness
        {
            get
            {
                if (_ExemptedSubjectBusiness == null)
                {
                    _ExemptedSubjectBusiness = new ExemptedSubjectBusiness(this.logger, context);
                }
                return _ExemptedSubjectBusiness;
            }
        }

        private IExperienceTypeBusiness _ExperienceTypeBusiness;
        public IExperienceTypeBusiness ExperienceTypeBusiness
        {
            get
            {
                if (_ExperienceTypeBusiness == null)
                {
                    _ExperienceTypeBusiness = new ExperienceTypeBusiness(this.logger, context);
                }
                return _ExperienceTypeBusiness;
            }
        }

        private IFamilyTypeBusiness _FamilyTypeBusiness;
        public IFamilyTypeBusiness FamilyTypeBusiness
        {
            get
            {
                if (_FamilyTypeBusiness == null)
                {
                    _FamilyTypeBusiness = new FamilyTypeBusiness(this.logger, context);
                }
                return _FamilyTypeBusiness;
            }
        }

        private IFaultCriteriaBusiness _FaultCriteriaBusiness;
        public IFaultCriteriaBusiness FaultCriteriaBusiness
        {
            get
            {
                if (_FaultCriteriaBusiness == null)
                {
                    _FaultCriteriaBusiness = new FaultCriteriaBusiness(this.logger, context);
                }
                return _FaultCriteriaBusiness;
            }
        }

        private IFaultGroupBusiness _FaultGroupBusiness;
        public IFaultGroupBusiness FaultGroupBusiness
        {
            get
            {
                if (_FaultGroupBusiness == null)
                {
                    _FaultGroupBusiness = new FaultGroupBusiness(this.logger, context);
                }
                return _FaultGroupBusiness;
            }
        }

        private IFlowSituationBusiness _FlowSituationBusiness;
        public IFlowSituationBusiness FlowSituationBusiness
        {
            get
            {
                if (_FlowSituationBusiness == null)
                {
                    _FlowSituationBusiness = new FlowSituationBusiness(this.logger, context);
                }
                return _FlowSituationBusiness;
            }
        }

        private IFoodCatBusiness _FoodCatBusiness;
        public IFoodCatBusiness FoodCatBusiness
        {
            get
            {
                if (_FoodCatBusiness == null)
                {
                    _FoodCatBusiness = new FoodCatBusiness(this.logger, context);
                }
                return _FoodCatBusiness;
            }
        }

        private IFoodGroupBusiness _FoodGroupBusiness;
        public IFoodGroupBusiness FoodGroupBusiness
        {
            get
            {
                if (_FoodGroupBusiness == null)
                {
                    _FoodGroupBusiness = new FoodGroupBusiness(this.logger, context);
                }
                return _FoodGroupBusiness;
            }
        }

        private IFoodHabitOfChildrenBusiness _FoodHabitOfChildrenBusiness;
        public IFoodHabitOfChildrenBusiness FoodHabitOfChildrenBusiness
        {
            get
            {
                if (_FoodHabitOfChildrenBusiness == null)
                {
                    _FoodHabitOfChildrenBusiness = new FoodHabitOfChildrenBusiness(this.logger, context);
                }
                return _FoodHabitOfChildrenBusiness;
            }
        }

        private IFoodMineralBusiness _FoodMineralBusiness;
        public IFoodMineralBusiness FoodMineralBusiness
        {
            get
            {
                if (_FoodMineralBusiness == null)
                {
                    _FoodMineralBusiness = new FoodMineralBusiness(this.logger, context);
                }
                return _FoodMineralBusiness;
            }
        }

        private IFoodPackingBusiness _FoodPackingBusiness;
        public IFoodPackingBusiness FoodPackingBusiness
        {
            get
            {
                if (_FoodPackingBusiness == null)
                {
                    _FoodPackingBusiness = new FoodPackingBusiness(this.logger, context);
                }
                return _FoodPackingBusiness;
            }
        }

        private IForeignLanguageGradeBusiness _ForeignLanguageGradeBusiness;
        public IForeignLanguageGradeBusiness ForeignLanguageGradeBusiness
        {
            get
            {
                if (_ForeignLanguageGradeBusiness == null)
                {
                    _ForeignLanguageGradeBusiness = new ForeignLanguageGradeBusiness(this.logger, context);
                }
                return _ForeignLanguageGradeBusiness;
            }
        }

        private IGoodChildrenTicketBusiness _GoodChildrenTicketBusiness;
        public IGoodChildrenTicketBusiness GoodChildrenTicketBusiness
        {
            get
            {
                if (_GoodChildrenTicketBusiness == null)
                {
                    _GoodChildrenTicketBusiness = new GoodChildrenTicketBusiness(this.logger, context);
                }
                return _GoodChildrenTicketBusiness;
            }
        }

        private IGraduationLevelBusiness _GraduationLevelBusiness;
        public IGraduationLevelBusiness GraduationLevelBusiness
        {
            get
            {
                if (_GraduationLevelBusiness == null)
                {
                    _GraduationLevelBusiness = new GraduationLevelBusiness(this.logger, context);
                }
                return _GraduationLevelBusiness;
            }
        }

        private IGroupCatBusiness _GroupCatBusiness;
        public IGroupCatBusiness GroupCatBusiness
        {
            get
            {
                if (_GroupCatBusiness == null)
                {
                    _GroupCatBusiness = new GroupCatBusiness(this.logger, context);
                }
                return _GroupCatBusiness;
            }
        }

        private IGroupMenuBusiness _GroupMenuBusiness;
        public IGroupMenuBusiness GroupMenuBusiness
        {
            get
            {
                if (_GroupMenuBusiness == null)
                {
                    _GroupMenuBusiness = new GroupMenuBusiness(this.logger, context);
                }
                return _GroupMenuBusiness;
            }
        }

        private IHabitDetailBusiness _HabitDetailBusiness;
        public IHabitDetailBusiness HabitDetailBusiness
        {
            get
            {
                if (_HabitDetailBusiness == null)
                {
                    _HabitDetailBusiness = new HabitDetailBusiness(this.logger, context);
                }
                return _HabitDetailBusiness;
            }
        }

        private IHabitGroupBusiness _HabitGroupBusiness;
        public IHabitGroupBusiness HabitGroupBusiness
        {
            get
            {
                if (_HabitGroupBusiness == null)
                {
                    _HabitGroupBusiness = new HabitGroupBusiness(this.logger, context);
                }
                return _HabitGroupBusiness;
            }
        }

        private IHabitOfChildrenBusiness _HabitOfChildrenBusiness;
        public IHabitOfChildrenBusiness HabitOfChildrenBusiness
        {
            get
            {
                if (_HabitOfChildrenBusiness == null)
                {
                    _HabitOfChildrenBusiness = new HabitOfChildrenBusiness(this.logger, context);
                }
                return _HabitOfChildrenBusiness;
            }
        }

        private IHeadTeacherSubstitutionBusiness _HeadTeacherSubstitutionBusiness;
        public IHeadTeacherSubstitutionBusiness HeadTeacherSubstitutionBusiness
        {
            get
            {
                if (_HeadTeacherSubstitutionBusiness == null)
                {
                    _HeadTeacherSubstitutionBusiness = new HeadTeacherSubstitutionBusiness(this.logger, context);
                }
                return _HeadTeacherSubstitutionBusiness;
            }
        }

        private IHonourAchivementBusiness _HonourAchivementBusiness;
        public IHonourAchivementBusiness HonourAchivementBusiness
        {
            get
            {
                if (_HonourAchivementBusiness == null)
                {
                    _HonourAchivementBusiness = new HonourAchivementBusiness(this.logger, context);
                }
                return _HonourAchivementBusiness;
            }
        }

        private IHonourAchivementTypeBusiness _HonourAchivementTypeBusiness;
        public IHonourAchivementTypeBusiness HonourAchivementTypeBusiness
        {
            get
            {
                if (_HonourAchivementTypeBusiness == null)
                {
                    _HonourAchivementTypeBusiness = new HonourAchivementTypeBusiness(this.logger, context);
                }
                return _HonourAchivementTypeBusiness;
            }
        }

        private IInvigilatorAssignmentBusiness _InvigilatorAssignmentBusiness;
        public IInvigilatorAssignmentBusiness InvigilatorAssignmentBusiness
        {
            get
            {
                if (_InvigilatorAssignmentBusiness == null)
                {
                    _InvigilatorAssignmentBusiness = new InvigilatorAssignmentBusiness(this.logger, context);
                }
                return _InvigilatorAssignmentBusiness;
            }
        }

        private IInvigilatorBusiness _InvigilatorBusiness;
        public IInvigilatorBusiness InvigilatorBusiness
        {
            get
            {
                if (_InvigilatorBusiness == null)
                {
                    _InvigilatorBusiness = new InvigilatorBusiness(this.logger, context);
                }
                return _InvigilatorBusiness;
            }
        }

        private IITQualificationLevelBusiness _ITQualificationLevelBusiness;
        public IITQualificationLevelBusiness ITQualificationLevelBusiness
        {
            get
            {
                if (_ITQualificationLevelBusiness == null)
                {
                    _ITQualificationLevelBusiness = new ITQualificationLevelBusiness(this.logger, context);
                }
                return _ITQualificationLevelBusiness;
            }
        }

        private IJudgeRecordBusiness _JudgeRecordBusiness;
        public IJudgeRecordBusiness JudgeRecordBusiness
        {
            get
            {
                if (_JudgeRecordBusiness == null)
                {
                    _JudgeRecordBusiness = new JudgeRecordBusiness(this.logger, context);
                }
                return _JudgeRecordBusiness;
            }
        }

        private IJudgeRecordHistoryBusiness _JudgeRecordHistoryBusiness;
        public IJudgeRecordHistoryBusiness JudgeRecordHistoryBusiness
        {
            get
            {
                if (_JudgeRecordHistoryBusiness == null)
                {
                    _JudgeRecordHistoryBusiness = new JudgeRecordHistoryBusiness(this.logger, context);
                }
                return _JudgeRecordHistoryBusiness;
            }
        }

        private ILeavingReasonBusiness _LeavingReasonBusiness;
        public ILeavingReasonBusiness LeavingReasonBusiness
        {
            get
            {
                if (_LeavingReasonBusiness == null)
                {
                    _LeavingReasonBusiness = new LeavingReasonBusiness(this.logger, context);
                }
                return _LeavingReasonBusiness;
            }
        }

        private ILockedMarkDetailBusiness _LockedMarkDetailBusiness;
        public ILockedMarkDetailBusiness LockedMarkDetailBusiness
        {
            get
            {
                if (_LockedMarkDetailBusiness == null)
                {
                    _LockedMarkDetailBusiness = new LockedMarkDetailBusiness(this.logger, context);
                }
                return _LockedMarkDetailBusiness;
            }
        }

        private ILockInputSupervisingDeptBusiness _LockInputSupervisingDeptBusiness;
        public ILockInputSupervisingDeptBusiness LockInputSupervisingDeptBusiness
        {
            get
            {
                if (_LockInputSupervisingDeptBusiness == null)
                {
                    _LockInputSupervisingDeptBusiness = new LockInputSupervisingDeptBusiness(this.logger, context);
                }
                return _LockInputSupervisingDeptBusiness;
            }
        }

        private ILookupInfoReportBusiness _LookupInfoReportBusiness;
        public ILookupInfoReportBusiness LookupInfoReportBusiness
        {
            get
            {
                if (_LookupInfoReportBusiness == null)
                {
                    _LookupInfoReportBusiness = new LookupInfoReportBusiness(this.logger, context);
                }
                return _LookupInfoReportBusiness;
            }
        }

        private IMarkOfSemesterBusiness _MarkOfSemesterBusiness;
        public IMarkOfSemesterBusiness MarkOfSemesterBusiness
        {
            get
            {
                if (_MarkOfSemesterBusiness == null)
                {
                    _MarkOfSemesterBusiness = new MarkOfSemesterBusiness(this.logger, context);
                }
                return _MarkOfSemesterBusiness;
            }
        }

        private IMarkOfSemesterByJudgeSubjectBusiness _MarkOfSemesterByJudgeSubjectBusiness;
        public IMarkOfSemesterByJudgeSubjectBusiness MarkOfSemesterByJudgeSubjectBusiness
        {
            get
            {
                if (_MarkOfSemesterByJudgeSubjectBusiness == null)
                {
                    _MarkOfSemesterByJudgeSubjectBusiness = new MarkOfSemesterByJudgeSubjectBusiness(this.logger, context);
                }
                return _MarkOfSemesterByJudgeSubjectBusiness;
            }
        }

        private IMarkOfSemesterBySubjectBusiness _MarkOfSemesterBySubjectBusiness;
        public IMarkOfSemesterBySubjectBusiness MarkOfSemesterBySubjectBusiness
        {
            get
            {
                if (_MarkOfSemesterBySubjectBusiness == null)
                {
                    _MarkOfSemesterBySubjectBusiness = new MarkOfSemesterBySubjectBusiness(this.logger, context);
                }
                return _MarkOfSemesterBySubjectBusiness;
            }
        }

        private IMarkRecordBusiness _MarkRecordBusiness;
        public IMarkRecordBusiness MarkRecordBusiness
        {
            get
            {
                if (_MarkRecordBusiness == null)
                {
                    _MarkRecordBusiness = new MarkRecordBusiness(this.logger, context);
                }
                return _MarkRecordBusiness;
            }
        }

        private IMarkRecordHistoryBusiness _MarkRecordHistoryBusiness;
        public IMarkRecordHistoryBusiness MarkRecordHistoryBusiness
        {
            get
            {
                if (_MarkRecordHistoryBusiness == null)
                {
                    _MarkRecordHistoryBusiness = new MarkRecordHistoryBusiness(this.logger, context);
                }
                return _MarkRecordHistoryBusiness;
            }
        }

        private IMarkStatisticBusiness _MarkStatisticBusiness;
        public IMarkStatisticBusiness MarkStatisticBusiness
        {
            get
            {
                if (_MarkStatisticBusiness == null)
                {
                    _MarkStatisticBusiness = new MarkStatisticBusiness(this.logger, context);
                }
                return _MarkStatisticBusiness;
            }
        }

        private IMarkTypeBusiness _MarkTypeBusiness;
        public IMarkTypeBusiness MarkTypeBusiness
        {
            get
            {
                if (_MarkTypeBusiness == null)
                {
                    _MarkTypeBusiness = new MarkTypeBusiness(this.logger, context);
                }
                return _MarkTypeBusiness;
            }
        }

        private IMasterBookBusiness _MasterBookBusiness;
        public IMasterBookBusiness MasterBookBusiness
        {
            get
            {
                if (_MasterBookBusiness == null)
                {
                    _MasterBookBusiness = new MasterBookBusiness(this.logger, context);
                }
                return _MasterBookBusiness;
            }
        }

        private IMealCatBusiness _MealCatBusiness;
        public IMealCatBusiness MealCatBusiness
        {
            get
            {
                if (_MealCatBusiness == null)
                {
                    _MealCatBusiness = new MealCatBusiness(this.logger, context);
                }
                return _MealCatBusiness;
            }
        }

        private IMenuBusiness _MenuBusiness;
        public IMenuBusiness MenuBusiness
        {
            get
            {
                if (_MenuBusiness == null)
                {
                    _MenuBusiness = new MenuBusiness(this.logger, context);
                }
                return _MenuBusiness;
            }
        }

        private IMenuPermissionBusiness _MenuPermissionBusiness;
        public IMenuPermissionBusiness MenuPermissionBusiness
        {
            get
            {
                if (_MenuPermissionBusiness == null)
                {
                    _MenuPermissionBusiness = new MenuPermissionBusiness(this.logger, context);
                }
                return _MenuPermissionBusiness;
            }
        }

        private IMinenalCatBusiness _MinenalCatBusiness;
        public IMinenalCatBusiness MinenalCatBusiness
        {
            get
            {
                if (_MinenalCatBusiness == null)
                {
                    _MinenalCatBusiness = new MinenalCatBusiness(this.logger, context);
                }
                return _MinenalCatBusiness;
            }
        }

        private IMonthlyBalanceBusiness _MonthlyBalanceBusiness;
        public IMonthlyBalanceBusiness MonthlyBalanceBusiness
        {
            get
            {
                if (_MonthlyBalanceBusiness == null)
                {
                    _MonthlyBalanceBusiness = new MonthlyBalanceBusiness(this.logger, context);
                }
                return _MonthlyBalanceBusiness;
            }
        }

        private IMonthlyLockBusiness _MonthlyLockBusiness;
        public IMonthlyLockBusiness MonthlyLockBusiness
        {
            get
            {
                if (_MonthlyLockBusiness == null)
                {
                    _MonthlyLockBusiness = new MonthlyLockBusiness(this.logger, context);
                }
                return _MonthlyLockBusiness;
            }
        }

        private IMovementAcceptanceBusiness _MovementAcceptanceBusiness;
        public IMovementAcceptanceBusiness MovementAcceptanceBusiness
        {
            get
            {
                if (_MovementAcceptanceBusiness == null)
                {
                    _MovementAcceptanceBusiness = new MovementAcceptanceBusiness(this.logger, context);
                }
                return _MovementAcceptanceBusiness;
            }
        }

        private INotEatingChildrenBusiness _NotEatingChildrenBusiness;
        public INotEatingChildrenBusiness NotEatingChildrenBusiness
        {
            get
            {
                if (_NotEatingChildrenBusiness == null)
                {
                    _NotEatingChildrenBusiness = new NotEatingChildrenBusiness(this.logger, context);
                }
                return _NotEatingChildrenBusiness;
            }
        }

        private INutritionalNormBusiness _NutritionalNormBusiness;
        public INutritionalNormBusiness NutritionalNormBusiness
        {
            get
            {
                if (_NutritionalNormBusiness == null)
                {
                    _NutritionalNormBusiness = new NutritionalNormBusiness(this.logger, context);
                }
                return _NutritionalNormBusiness;
            }
        }

        private INutritionalNormMineralBusiness _NutritionalNormMineralBusiness;
        public INutritionalNormMineralBusiness NutritionalNormMineralBusiness
        {
            get
            {
                if (_NutritionalNormMineralBusiness == null)
                {
                    _NutritionalNormMineralBusiness = new NutritionalNormMineralBusiness(this.logger, context);
                }
                return _NutritionalNormMineralBusiness;
            }
        }

        private IOtherServiceBusiness _OtherServiceBusiness;
        public IOtherServiceBusiness OtherServiceBusiness
        {
            get
            {
                if (_OtherServiceBusiness == null)
                {
                    _OtherServiceBusiness = new OtherServiceBusiness(this.logger, context);
                }
                return _OtherServiceBusiness;
            }
        }

        private IPackageBusiness _PackageBusiness;
        public IPackageBusiness PackageBusiness
        {
            get
            {
                if (_PackageBusiness == null)
                {
                    _PackageBusiness = new PackageBusiness(this.logger, context);
                }
                return _PackageBusiness;
            }
        }



        private IParticularPupilBusiness _ParticularPupilBusiness;
        public IParticularPupilBusiness ParticularPupilBusiness
        {
            get
            {
                if (_ParticularPupilBusiness == null)
                {
                    _ParticularPupilBusiness = new ParticularPupilBusiness(this.logger, context);
                }
                return _ParticularPupilBusiness;
            }
        }

        private IParticularPupilCharacteristicBusiness _ParticularPupilCharacteristicBusiness;
        public IParticularPupilCharacteristicBusiness ParticularPupilCharacteristicBusiness
        {
            get
            {
                if (_ParticularPupilCharacteristicBusiness == null)
                {
                    _ParticularPupilCharacteristicBusiness = new ParticularPupilCharacteristicBusiness(this.logger, context);
                }
                return _ParticularPupilCharacteristicBusiness;
            }
        }

        private IParticularPupilTreatmentBusiness _ParticularPupilTreatmentBusiness;
        public IParticularPupilTreatmentBusiness ParticularPupilTreatmentBusiness
        {
            get
            {
                if (_ParticularPupilTreatmentBusiness == null)
                {
                    _ParticularPupilTreatmentBusiness = new ParticularPupilTreatmentBusiness(this.logger, context);
                }
                return _ParticularPupilTreatmentBusiness;
            }
        }

        private IPeriodDeclarationBusiness _PeriodDeclarationBusiness;
        public IPeriodDeclarationBusiness PeriodDeclarationBusiness
        {
            get
            {
                if (_PeriodDeclarationBusiness == null)
                {
                    _PeriodDeclarationBusiness = new PeriodDeclarationBusiness(this.logger, context);
                }
                return _PeriodDeclarationBusiness;
            }
        }

        private IPointReportBusiness _PointReportBusiness;
        public IPointReportBusiness PointReportBusiness
        {
            get
            {
                if (_PointReportBusiness == null)
                {
                    _PointReportBusiness = new PointReportBusiness(this.logger, context);
                }
                return _PointReportBusiness;
            }
        }

        private IPolicyTargetBusiness _PolicyTargetBusiness;
        public IPolicyTargetBusiness PolicyTargetBusiness
        {
            get
            {
                if (_PolicyTargetBusiness == null)
                {
                    _PolicyTargetBusiness = new PolicyTargetBusiness(this.logger, context);
                }
                return _PolicyTargetBusiness;
            }
        }

        private IPoliticalGradeBusiness _PoliticalGradeBusiness;
        public IPoliticalGradeBusiness PoliticalGradeBusiness
        {
            get
            {
                if (_PoliticalGradeBusiness == null)
                {
                    _PoliticalGradeBusiness = new PoliticalGradeBusiness(this.logger, context);
                }
                return _PoliticalGradeBusiness;
            }
        }

        private IPraiseTypeBusiness _PraiseTypeBusiness;
        public IPraiseTypeBusiness PraiseTypeBusiness
        {
            get
            {
                if (_PraiseTypeBusiness == null)
                {
                    _PraiseTypeBusiness = new PraiseTypeBusiness(this.logger, context);
                }
                return _PraiseTypeBusiness;
            }
        }

        private IPriorityTypeBusiness _PriorityTypeBusiness;
        public IPriorityTypeBusiness PriorityTypeBusiness
        {
            get
            {
                if (_PriorityTypeBusiness == null)
                {
                    _PriorityTypeBusiness = new PriorityTypeBusiness(this.logger, context);
                }
                return _PriorityTypeBusiness;
            }
        }

        private IProcessedReportBusiness _ProcessedReportBusiness;
        public IProcessedReportBusiness ProcessedReportBusiness
        {
            get
            {
                if (_ProcessedReportBusiness == null)
                {
                    _ProcessedReportBusiness = new ProcessedReportBusiness(this.logger, context);
                }
                return _ProcessedReportBusiness;
            }
        }

        private IPropertyOfClassBusiness _PropertyOfClassBusiness;
        public IPropertyOfClassBusiness PropertyOfClassBusiness
        {
            get
            {
                if (_PropertyOfClassBusiness == null)
                {
                    _PropertyOfClassBusiness = new PropertyOfClassBusiness(this.logger, context);
                }
                return _PropertyOfClassBusiness;
            }
        }

        private IPropertyOfSchoolBusiness _PropertyOfSchoolBusiness;
        public IPropertyOfSchoolBusiness PropertyOfSchoolBusiness
        {
            get
            {
                if (_PropertyOfSchoolBusiness == null)
                {
                    _PropertyOfSchoolBusiness = new PropertyOfSchoolBusiness(this.logger, context);
                }
                return _PropertyOfSchoolBusiness;
            }
        }

        private IProvinceBusiness _ProvinceBusiness;
        public IProvinceBusiness ProvinceBusiness
        {
            get
            {
                if (_ProvinceBusiness == null)
                {
                    _ProvinceBusiness = new ProvinceBusiness(this.logger, context);
                }
                return _ProvinceBusiness;
            }
        }

        private IPupilAbsenceBusiness _PupilAbsenceBusiness;
        public IPupilAbsenceBusiness PupilAbsenceBusiness
        {
            get
            {
                if (_PupilAbsenceBusiness == null)
                {
                    _PupilAbsenceBusiness = new PupilAbsenceBusiness(this.logger, context);
                }
                return _PupilAbsenceBusiness;
            }
        }

        private IPupilDisciplineBusiness _PupilDisciplineBusiness;
        public IPupilDisciplineBusiness PupilDisciplineBusiness
        {
            get
            {
                if (_PupilDisciplineBusiness == null)
                {
                    _PupilDisciplineBusiness = new PupilDisciplineBusiness(this.logger, context);
                }
                return _PupilDisciplineBusiness;
            }
        }

        private IPupilEmulationBusiness _PupilEmulationBusiness;
        public IPupilEmulationBusiness PupilEmulationBusiness
        {
            get
            {
                if (_PupilEmulationBusiness == null)
                {
                    _PupilEmulationBusiness = new PupilEmulationBusiness(this.logger, context);
                }
                return _PupilEmulationBusiness;
            }
        }

        private IPupilFaultBusiness _PupilFaultBusiness;
        public IPupilFaultBusiness PupilFaultBusiness
        {
            get
            {
                if (_PupilFaultBusiness == null)
                {
                    _PupilFaultBusiness = new PupilFaultBusiness(this.logger, context);
                }
                return _PupilFaultBusiness;
            }
        }

        private IPupilGraduationBusiness _PupilGraduationBusiness;
        public IPupilGraduationBusiness PupilGraduationBusiness
        {
            get
            {
                if (_PupilGraduationBusiness == null)
                {
                    _PupilGraduationBusiness = new PupilGraduationBusiness(this.logger, context);
                }
                return _PupilGraduationBusiness;
            }
        }

        private IGraduationApprovalBusiness _GraduationApprovalBusiness;
        public IGraduationApprovalBusiness GraduationApprovalBusiness
        {
            get
            {
                if (_GraduationApprovalBusiness == null)
                {
                    _GraduationApprovalBusiness = new GraduationApprovalBusiness(this.logger, context);
                }
                return _GraduationApprovalBusiness;
            }
        }


        private IPupilLeavingOffBusiness _PupilLeavingOffBusiness;
        public IPupilLeavingOffBusiness PupilLeavingOffBusiness
        {
            get
            {
                if (_PupilLeavingOffBusiness == null)
                {
                    _PupilLeavingOffBusiness = new PupilLeavingOffBusiness(this.logger, context);
                }
                return _PupilLeavingOffBusiness;
            }
        }

        private IPupilOfClassBusiness _PupilOfClassBusiness;
        public IPupilOfClassBusiness PupilOfClassBusiness
        {
            get
            {
                if (_PupilOfClassBusiness == null)
                {
                    _PupilOfClassBusiness = new PupilOfClassBusiness(this.logger, context);
                }
                return _PupilOfClassBusiness;
            }
        }

        private IPupilOfSchoolBusiness _PupilOfSchoolBusiness;
        public IPupilOfSchoolBusiness PupilOfSchoolBusiness
        {
            get
            {
                if (_PupilOfSchoolBusiness == null)
                {
                    _PupilOfSchoolBusiness = new PupilOfSchoolBusiness(this.logger, context);
                }
                return _PupilOfSchoolBusiness;
            }
        }

        private IPupilPraiseBusiness _PupilPraiseBusiness;
        public IPupilPraiseBusiness PupilPraiseBusiness
        {
            get
            {
                if (_PupilPraiseBusiness == null)
                {
                    _PupilPraiseBusiness = new PupilPraiseBusiness(this.logger, context);
                }
                return _PupilPraiseBusiness;
            }
        }

        private IPupilProfileBusiness _PupilProfileBusiness;
        public IPupilProfileBusiness PupilProfileBusiness
        {
            get
            {
                if (_PupilProfileBusiness == null)
                {
                    _PupilProfileBusiness = new PupilProfileBusiness(this.logger, context);
                }
                return _PupilProfileBusiness;
            }
        }

        private IPupilRankingBusiness _PupilRankingBusiness;
        public IPupilRankingBusiness PupilRankingBusiness
        {
            get
            {
                if (_PupilRankingBusiness == null)
                {
                    _PupilRankingBusiness = new PupilRankingBusiness(this.logger, context);
                }
                return _PupilRankingBusiness;
            }
        }

        private IPupilRetestBusiness _PupilRetestBusiness;
        public IPupilRetestBusiness PupilRetestBusiness
        {
            get
            {
                if (_PupilRetestBusiness == null)
                {
                    _PupilRetestBusiness = new PupilRetestBusiness(this.logger, context);
                }
                return _PupilRetestBusiness;
            }
        }

        //private IPupilReviewEvaluateBusiness _PupilReviewEvaluateBusiness;
        //public IPupilReviewEvaluateBusiness PupilReviewEvaluateBusiness
        //{
        //    get
        //    {
        //        if (_PupilReviewEvaluateBusiness == null)
        //        {
        //            _PupilReviewEvaluateBusiness = new PupilReviewEvaluateBusiness(this.logger, context);
        //        }
        //        return _PupilReviewEvaluateBusiness;
        //    }
        //}

        private IPupilRetestRegistrationBusiness _PupilRetestRegistrationBusiness;
        public IPupilRetestRegistrationBusiness PupilRetestRegistrationBusiness
        {
            get
            {
                if (_PupilRetestRegistrationBusiness == null)
                {
                    _PupilRetestRegistrationBusiness = new PupilRetestRegistrationBusiness(this.logger, context);
                }
                return _PupilRetestRegistrationBusiness;
            }
        }

        private IPupilRewardReportBusiness _PupilRewardReportBusiness;
        public IPupilRewardReportBusiness PupilRewardReportBusiness
        {
            get
            {
                if (_PupilRewardReportBusiness == null)
                {
                    _PupilRewardReportBusiness = new PupilRewardReportBusiness(this.logger, context);
                }
                return _PupilRewardReportBusiness;
            }
        }

        private IQualificationGradeBusiness _QualificationGradeBusiness;
        public IQualificationGradeBusiness QualificationGradeBusiness
        {
            get
            {
                if (_QualificationGradeBusiness == null)
                {
                    _QualificationGradeBusiness = new QualificationGradeBusiness(this.logger, context);
                }
                return _QualificationGradeBusiness;
            }
        }

        private IQualificationLevelBusiness _QualificationLevelBusiness;
        public IQualificationLevelBusiness QualificationLevelBusiness
        {
            get
            {
                if (_QualificationLevelBusiness == null)
                {
                    _QualificationLevelBusiness = new QualificationLevelBusiness(this.logger, context);
                }
                return _QualificationLevelBusiness;
            }
        }

        private IQualificationTypeBusiness _QualificationTypeBusiness;
        public IQualificationTypeBusiness QualificationTypeBusiness
        {
            get
            {
                if (_QualificationTypeBusiness == null)
                {
                    _QualificationTypeBusiness = new QualificationTypeBusiness(this.logger, context);
                }
                return _QualificationTypeBusiness;
            }
        }

        private IReligionBusiness _ReligionBusiness;
        public IReligionBusiness ReligionBusiness
        {
            get
            {
                if (_ReligionBusiness == null)
                {
                    _ReligionBusiness = new ReligionBusiness(this.logger, context);
                }
                return _ReligionBusiness;
            }
        }

        //private IReportAverageMarkByPeriodBusiness _ReportAverageMarkByPeriodBusiness;
        //public IReportAverageMarkByPeriodBusiness ReportAverageMarkByPeriodBusiness
        //{
        //    get
        //    {
        //        if (_ReportAverageMarkByPeriodBusiness == null)
        //        {
        //            _ReportAverageMarkByPeriodBusiness = new ReportAverageMarkByPeriodBusiness(this.logger, context);
        //        }
        //        return _ReportAverageMarkByPeriodBusiness;
        //    }
        //}
        private IAverageMarkByPeriodBusiness _AverageMarkByPeriodBusiness;
        public IAverageMarkByPeriodBusiness AverageMarkByPeriodBusiness
        {
            get
            {
                if (_AverageMarkByPeriodBusiness == null)
                {
                    _AverageMarkByPeriodBusiness = new AverageMarkByPeriodBusiness(this.logger, context);
                }
                return _AverageMarkByPeriodBusiness;
            }
        }

        private IReportDefinitionBusiness _ReportDefinitionBusiness;
        public IReportDefinitionBusiness ReportDefinitionBusiness
        {
            get
            {
                if (_ReportDefinitionBusiness == null)
                {
                    _ReportDefinitionBusiness = new ReportDefinitionBusiness(this.logger, context);
                }
                return _ReportDefinitionBusiness;
            }
        }

        private IReportEmployeeProfileBusiness _ReportEmployeeProfileBusiness;
        public IReportEmployeeProfileBusiness ReportEmployeeProfileBusiness
        {
            get
            {
                if (_ReportEmployeeProfileBusiness == null)
                {
                    _ReportEmployeeProfileBusiness = new ReportEmployeeProfileBusiness(this.logger, context);
                }
                return _ReportEmployeeProfileBusiness;
            }
        }

        private IReportNumberOfPupilByTeacherBusiness _ReportNumberOfPupilByTeacherBusiness;
        public IReportNumberOfPupilByTeacherBusiness ReportNumberOfPupilByTeacherBusiness
        {
            get
            {
                if (_ReportNumberOfPupilByTeacherBusiness == null)
                {
                    _ReportNumberOfPupilByTeacherBusiness = new ReportNumberOfPupilByTeacherBusiness(this.logger, context);
                }
                return _ReportNumberOfPupilByTeacherBusiness;
            }
        }

        private IReportPupilSynthesisBusiness _ReportPupilSynthesisBusiness;
        public IReportPupilSynthesisBusiness ReportPupilSynthesisBusiness
        {
            get
            {
                if (_ReportPupilSynthesisBusiness == null)
                {
                    _ReportPupilSynthesisBusiness = new ReportPupilSynthesisBusiness(this.logger, context);
                }
                return _ReportPupilSynthesisBusiness;
            }
        }

        private IReportResultEndOfYearBusiness _ReportResultEndOfYearBusiness;
        public IReportResultEndOfYearBusiness ReportResultEndOfYearBusiness
        {
            get
            {
                if (_ReportResultEndOfYearBusiness == null)
                {
                    _ReportResultEndOfYearBusiness = new ReportResultEndOfYearBusiness(this.logger, context);
                }
                return _ReportResultEndOfYearBusiness;
            }
        }

        private IReportSituationOfViolationBusiness _ReportSituationOfViolationBusiness;
        public IReportSituationOfViolationBusiness ReportSituationOfViolationBusiness
        {
            get
            {
                if (_ReportSituationOfViolationBusiness == null)
                {
                    _ReportSituationOfViolationBusiness = new ReportSituationOfViolationBusiness(this.logger, context);
                }
                return _ReportSituationOfViolationBusiness;
            }
        }

        private IReportSynthesisCapacityConductByPeriodBusiness _ReportSynthesisCapacityConductByPeriodBusiness;
        public IReportSynthesisCapacityConductByPeriodBusiness ReportSynthesisCapacityConductByPeriodBusiness
        {
            get
            {
                if (_ReportSynthesisCapacityConductByPeriodBusiness == null)
                {
                    _ReportSynthesisCapacityConductByPeriodBusiness = new ReportSynthesisCapacityConductByPeriodBusiness(this.logger, context);
                }
                return _ReportSynthesisCapacityConductByPeriodBusiness;
            }
        }

        private IReportTeachingAssignmentBusiness _ReportTeachingAssignmentBusiness;
        public IReportTeachingAssignmentBusiness ReportTeachingAssignmentBusiness
        {
            get
            {
                if (_ReportTeachingAssignmentBusiness == null)
                {
                    _ReportTeachingAssignmentBusiness = new ReportTeachingAssignmentBusiness(this.logger, context);
                }
                return _ReportTeachingAssignmentBusiness;
            }
        }

        private IRoleBusiness _RoleBusiness;
        public IRoleBusiness RoleBusiness
        {
            get
            {
                if (_RoleBusiness == null)
                {
                    _RoleBusiness = new RoleBusiness(this.logger, context);
                }
                return _RoleBusiness;
            }
        }

        private IRoleMenuBusiness _RoleMenuBusiness;
        public IRoleMenuBusiness RoleMenuBusiness
        {
            get
            {
                if (_RoleMenuBusiness == null)
                {
                    _RoleMenuBusiness = new RoleMenuBusiness(this.logger, context);
                }
                return _RoleMenuBusiness;
            }
        }

        private ISalaryLevelBusiness _SalaryLevelBusiness;
        public ISalaryLevelBusiness SalaryLevelBusiness
        {
            get
            {
                if (_SalaryLevelBusiness == null)
                {
                    _SalaryLevelBusiness = new SalaryLevelBusiness(this.logger, context);
                }
                return _SalaryLevelBusiness;
            }
        }

        private ISchoolCodeConfigBusiness _SchoolCodeConfigBusiness;
        public ISchoolCodeConfigBusiness SchoolCodeConfigBusiness
        {
            get
            {
                if (_SchoolCodeConfigBusiness == null)
                {
                    _SchoolCodeConfigBusiness = new SchoolCodeConfigBusiness(this.logger, context);
                }
                return _SchoolCodeConfigBusiness;
            }
        }

        private ISchoolFacultyBusiness _SchoolFacultyBusiness;
        public ISchoolFacultyBusiness SchoolFacultyBusiness
        {
            get
            {
                if (_SchoolFacultyBusiness == null)
                {
                    _SchoolFacultyBusiness = new SchoolFacultyBusiness(this.logger, context);
                }
                return _SchoolFacultyBusiness;
            }
        }

        private ISchoolMovementBusiness _SchoolMovementBusiness;
        public ISchoolMovementBusiness SchoolMovementBusiness
        {
            get
            {
                if (_SchoolMovementBusiness == null)
                {
                    _SchoolMovementBusiness = new SchoolMovementBusiness(this.logger, context);
                }
                return _SchoolMovementBusiness;
            }
        }

        private ISchoolProfileBusiness _SchoolProfileBusiness;
        public ISchoolProfileBusiness SchoolProfileBusiness
        {
            get
            {
                if (_SchoolProfileBusiness == null)
                {
                    _SchoolProfileBusiness = new SchoolProfileBusiness(this.logger, context);
                }
                return _SchoolProfileBusiness;
            }
        }

        private ISchoolPropertyCatBusiness _SchoolPropertyCatBusiness;
        public ISchoolPropertyCatBusiness SchoolPropertyCatBusiness
        {
            get
            {
                if (_SchoolPropertyCatBusiness == null)
                {
                    _SchoolPropertyCatBusiness = new SchoolPropertyCatBusiness(this.logger, context);
                }
                return _SchoolPropertyCatBusiness;
            }
        }

        private ISchoolSubjectBusiness _SchoolSubjectBusiness;
        public ISchoolSubjectBusiness SchoolSubjectBusiness
        {
            get
            {
                if (_SchoolSubjectBusiness == null)
                {
                    _SchoolSubjectBusiness = new SchoolSubjectBusiness(this.logger, context);
                }
                return _SchoolSubjectBusiness;
            }
        }

        private ISchoolSubsidiaryBusiness _SchoolSubsidiaryBusiness;
        public ISchoolSubsidiaryBusiness SchoolSubsidiaryBusiness
        {
            get
            {
                if (_SchoolSubsidiaryBusiness == null)
                {
                    _SchoolSubsidiaryBusiness = new SchoolSubsidiaryBusiness(this.logger, context);
                }
                return _SchoolSubsidiaryBusiness;
            }
        }

        private ISchoolTypeBusiness _SchoolTypeBusiness;
        public ISchoolTypeBusiness SchoolTypeBusiness
        {
            get
            {
                if (_SchoolTypeBusiness == null)
                {
                    _SchoolTypeBusiness = new SchoolTypeBusiness(this.logger, context);
                }
                return _SchoolTypeBusiness;
            }
        }


        private ISendInfoBusiness _SendInfoBusiness;
        public ISendInfoBusiness SendInfoBusiness
        {
            get
            {
                if (_SendInfoBusiness == null)
                {
                    _SendInfoBusiness = new SendInfoBusiness(this.logger, context);
                }
                return _SendInfoBusiness;
            }
        }



        private IServiceBusiness _ServiceBusiness;
        public IServiceBusiness ServiceBusiness
        {
            get
            {
                if (_ServiceBusiness == null)
                {
                    _ServiceBusiness = new ServiceBusiness(this.logger, context);
                }
                return _ServiceBusiness;
            }
        }





        private ISpecialityCatBusiness _SpecialityCatBusiness;
        public ISpecialityCatBusiness SpecialityCatBusiness
        {
            get
            {
                if (_SpecialityCatBusiness == null)
                {
                    _SpecialityCatBusiness = new SpecialityCatBusiness(this.logger, context);
                }
                return _SpecialityCatBusiness;
            }
        }

        private IStaffPositionBusiness _StaffPositionBusiness;
        public IStaffPositionBusiness StaffPositionBusiness
        {
            get
            {
                if (_StaffPositionBusiness == null)
                {
                    _StaffPositionBusiness = new StaffPositionBusiness(this.logger, context);
                }
                return _StaffPositionBusiness;
            }
        }

        private IStateManagementGradeBusiness _StateManagementGradeBusiness;
        public IStateManagementGradeBusiness StateManagementGradeBusiness
        {
            get
            {
                if (_StateManagementGradeBusiness == null)
                {
                    _StateManagementGradeBusiness = new StateManagementGradeBusiness(this.logger, context);
                }
                return _StateManagementGradeBusiness;
            }
        }

        private IStatisticsConfigBusiness _StatisticsConfigBusiness;
        public IStatisticsConfigBusiness StatisticsConfigBusiness
        {
            get
            {
                if (_StatisticsConfigBusiness == null)
                {
                    _StatisticsConfigBusiness = new StatisticsConfigBusiness(this.logger, context);
                }
                return _StatisticsConfigBusiness;
            }
        }

        private IStatisticsForUnitBusiness _StatisticsForUnitBusiness;
        public IStatisticsForUnitBusiness StatisticsForUnitBusiness
        {
            get
            {
                if (_StatisticsForUnitBusiness == null)
                {
                    _StatisticsForUnitBusiness = new StatisticsForUnitBusiness(this.logger, context);
                }
                return _StatisticsForUnitBusiness;
            }
        }

        private IStudyingJudgementBusiness _StudyingJudgementBusiness;
        public IStudyingJudgementBusiness StudyingJudgementBusiness
        {
            get
            {
                if (_StudyingJudgementBusiness == null)
                {
                    _StudyingJudgementBusiness = new StudyingJudgementBusiness(this.logger, context);
                }
                return _StudyingJudgementBusiness;
            }
        }

        private ISubCommitteeBusiness _SubCommitteeBusiness;
        public ISubCommitteeBusiness SubCommitteeBusiness
        {
            get
            {
                if (_SubCommitteeBusiness == null)
                {
                    _SubCommitteeBusiness = new SubCommitteeBusiness(this.logger, context);
                }
                return _SubCommitteeBusiness;
            }
        }

        private ISubjectCatBusiness _SubjectCatBusiness;
        public ISubjectCatBusiness SubjectCatBusiness
        {
            get
            {
                if (_SubjectCatBusiness == null)
                {
                    _SubjectCatBusiness = new SubjectCatBusiness(this.logger, context);
                }
                return _SubjectCatBusiness;
            }
        }

        private ISummedUpRecordBusiness _SummedUpRecordBusiness;
        public ISummedUpRecordBusiness SummedUpRecordBusiness
        {
            get
            {
                if (_SummedUpRecordBusiness == null)
                {
                    _SummedUpRecordBusiness = new SummedUpRecordBusiness(this.logger, context);
                }
                return _SummedUpRecordBusiness;
            }
        }

        private ISummedUpRecordHistoryBusiness _SummedUpRecordHistoryBusiness;
        public ISummedUpRecordHistoryBusiness SummedUpRecordHistoryBusiness
        {
            get
            {
                if (_SummedUpRecordHistoryBusiness == null)
                {
                    _SummedUpRecordHistoryBusiness = new SummedUpRecordHistoryBusiness(this.logger, context);
                }
                return _SummedUpRecordHistoryBusiness;
            }
        }

        private ISupervisingDeptBusiness _SupervisingDeptBusiness;
        public ISupervisingDeptBusiness SupervisingDeptBusiness
        {
            get
            {
                if (_SupervisingDeptBusiness == null)
                {
                    _SupervisingDeptBusiness = new SupervisingDeptBusiness(this.logger, context);
                }
                return _SupervisingDeptBusiness;
            }
        }

        private ISupplierBusiness _SupplierBusiness;
        public ISupplierBusiness SupplierBusiness
        {
            get
            {
                if (_SupplierBusiness == null)
                {
                    _SupplierBusiness = new SupplierBusiness(this.logger, context);
                }
                return _SupplierBusiness;
            }
        }

        private ISymbolConfigBusiness _SymbolConfigBusiness;
        public ISymbolConfigBusiness SymbolConfigBusiness
        {
            get
            {
                if (_SymbolConfigBusiness == null)
                {
                    _SymbolConfigBusiness = new SymbolConfigBusiness(this.logger, context);
                }
                return _SymbolConfigBusiness;
            }
        }

        private ISynthesisCapacityConductBusiness _SynthesisCapacityConductBusiness;
        public ISynthesisCapacityConductBusiness SynthesisCapacityConductBusiness
        {
            get
            {
                if (_SynthesisCapacityConductBusiness == null)
                {
                    _SynthesisCapacityConductBusiness = new SynthesisCapacityConductBusiness(this.logger, context);
                }
                return _SynthesisCapacityConductBusiness;
            }
        }

        private ISynthesisSubjectByEducationLevelBusiness _SynthesisSubjectByEducationLevelBusiness;
        public ISynthesisSubjectByEducationLevelBusiness SynthesisSubjectByEducationLevelBusiness
        {
            get
            {
                if (_SynthesisSubjectByEducationLevelBusiness == null)
                {
                    _SynthesisSubjectByEducationLevelBusiness = new SynthesisSubjectByEducationLevelBusiness(this.logger, context);
                }
                return _SynthesisSubjectByEducationLevelBusiness;
            }
        }

        private IsysdiagramBusiness _sysdiagramBusiness;
        public IsysdiagramBusiness sysdiagramBusiness
        {
            get
            {
                if (_sysdiagramBusiness == null)
                {
                    _sysdiagramBusiness = new sysdiagramBusiness(this.logger, context);
                }
                return _sysdiagramBusiness;
            }
        }

        private ISystemParameterBusiness _SystemParameterBusiness;
        public ISystemParameterBusiness SystemParameterBusiness
        {
            get
            {
                if (_SystemParameterBusiness == null)
                {
                    _SystemParameterBusiness = new SystemParameterBusiness(this.logger, context);
                }
                return _SystemParameterBusiness;
            }
        }

        private ITeacherGradeBusiness _TeacherGradeBusiness;
        public ITeacherGradeBusiness TeacherGradeBusiness
        {
            get
            {
                if (_TeacherGradeBusiness == null)
                {
                    _TeacherGradeBusiness = new TeacherGradeBusiness(this.logger, context);
                }
                return _TeacherGradeBusiness;
            }
        }

        private ITeacherGradingBusiness _TeacherGradingBusiness;
        public ITeacherGradingBusiness TeacherGradingBusiness
        {
            get
            {
                if (_TeacherGradingBusiness == null)
                {
                    _TeacherGradingBusiness = new TeacherGradingBusiness(this.logger, context);
                }
                return _TeacherGradingBusiness;
            }
        }

        private ITeacherOfFacultyBusiness _TeacherOfFacultyBusiness;
        public ITeacherOfFacultyBusiness TeacherOfFacultyBusiness
        {
            get
            {
                if (_TeacherOfFacultyBusiness == null)
                {
                    _TeacherOfFacultyBusiness = new TeacherOfFacultyBusiness(this.logger, context);
                }
                return _TeacherOfFacultyBusiness;
            }
        }

        private ITeachingAssignmentBusiness _TeachingAssignmentBusiness;
        public ITeachingAssignmentBusiness TeachingAssignmentBusiness
        {
            get
            {
                if (_TeachingAssignmentBusiness == null)
                {
                    _TeachingAssignmentBusiness = new TeachingAssignmentBusiness(this.logger, context);
                }
                return _TeachingAssignmentBusiness;
            }
        }

        private ITeachingExperienceBusiness _TeachingExperienceBusiness;
        public ITeachingExperienceBusiness TeachingExperienceBusiness
        {
            get
            {
                if (_TeachingExperienceBusiness == null)
                {
                    _TeachingExperienceBusiness = new TeachingExperienceBusiness(this.logger, context);
                }
                return _TeachingExperienceBusiness;
            }
        }

        private ITeachingRegistrationBusiness _TeachingRegistrationBusiness;
        public ITeachingRegistrationBusiness TeachingRegistrationBusiness
        {
            get
            {
                if (_TeachingRegistrationBusiness == null)
                {
                    _TeachingRegistrationBusiness = new TeachingRegistrationBusiness(this.logger, context);
                }
                return _TeachingRegistrationBusiness;
            }
        }

        private ITeachingTargetRegistrationBusiness _TeachingTargetRegistrationBusiness;
        public ITeachingTargetRegistrationBusiness TeachingTargetRegistrationBusiness
        {
            get
            {
                if (_TeachingTargetRegistrationBusiness == null)
                {
                    _TeachingTargetRegistrationBusiness = new TeachingTargetRegistrationBusiness(this.logger, context);
                }
                return _TeachingTargetRegistrationBusiness;
            }
        }

        private ITrainingTypeBusiness _TrainingTypeBusiness;
        public ITrainingTypeBusiness TrainingTypeBusiness
        {
            get
            {
                if (_TrainingTypeBusiness == null)
                {
                    _TrainingTypeBusiness = new TrainingTypeBusiness(this.logger, context);
                }
                return _TrainingTypeBusiness;
            }
        }

        private ITranscriptOfPrimaryClassBusiness _TranscriptOfPrimaryClassBusiness;
        public ITranscriptOfPrimaryClassBusiness TranscriptOfPrimaryClassBusiness
        {
            get
            {
                if (_TranscriptOfPrimaryClassBusiness == null)
                {
                    _TranscriptOfPrimaryClassBusiness = new TranscriptOfPrimaryClassBusiness(this.logger, context);
                }
                return _TranscriptOfPrimaryClassBusiness;
            }
        }

        private ITranscriptsBusiness _TranscriptsBusiness;
        public ITranscriptsBusiness TranscriptsBusiness
        {
            get
            {
                if (_TranscriptsBusiness == null)
                {
                    _TranscriptsBusiness = new TranscriptsBusiness(this.logger, context);
                }
                return _TranscriptsBusiness;
            }
        }

        private ITranscriptsByPeriodBusiness _TranscriptsByPeriodBusiness;
        public ITranscriptsByPeriodBusiness TranscriptsByPeriodBusiness
        {
            get
            {
                if (_TranscriptsByPeriodBusiness == null)
                {
                    _TranscriptsByPeriodBusiness = new TranscriptsByPeriodBusiness(this.logger, context);
                }
                return _TranscriptsByPeriodBusiness;
            }
        }

        private ITranscriptsBySemesterBusiness _TranscriptsBySemesterBusiness;
        public ITranscriptsBySemesterBusiness TranscriptsBySemesterBusiness
        {
            get
            {
                if (_TranscriptsBySemesterBusiness == null)
                {
                    _TranscriptsBySemesterBusiness = new TranscriptsBySemesterBusiness(this.logger, context);
                }
                return _TranscriptsBySemesterBusiness;
            }
        }

        private ISemeterDeclarationBusiness _SemeterDeclarationBusiness;
        public ISemeterDeclarationBusiness SemeterDeclarationBusiness
        {
            get
            {
                if (_SemeterDeclarationBusiness == null)
                {
                    _SemeterDeclarationBusiness = new SemeterDeclarationBusiness(this.logger, context);
                }
                return _SemeterDeclarationBusiness;
            }
        }

        private ITypeConfigBusiness _TypeConfigBusiness;
        public ITypeConfigBusiness TypeConfigBusiness
        {
            get
            {
                if (_TypeConfigBusiness == null)
                {
                    _TypeConfigBusiness = new TypeConfigBusiness(this.logger, context);
                }
                return _TypeConfigBusiness;
            }
        }

        private ITypeOfDishBusiness _TypeOfDishBusiness;
        public ITypeOfDishBusiness TypeOfDishBusiness
        {
            get
            {
                if (_TypeOfDishBusiness == null)
                {
                    _TypeOfDishBusiness = new TypeOfDishBusiness(this.logger, context);
                }
                return _TypeOfDishBusiness;
            }
        }

        private ITypeOfFoodBusiness _TypeOfFoodBusiness;
        public ITypeOfFoodBusiness TypeOfFoodBusiness
        {
            get
            {
                if (_TypeOfFoodBusiness == null)
                {
                    _TypeOfFoodBusiness = new TypeOfFoodBusiness(this.logger, context);
                }
                return _TypeOfFoodBusiness;
            }
        }

        private IUpdateRewardBusiness _UpdateRewardBusiness;
        public IUpdateRewardBusiness UpdateRewardBusiness
        {
            get
            {
                if (_UpdateRewardBusiness == null)
                {
                    _UpdateRewardBusiness = new UpdateRewardBusiness(this.logger, context);
                }
                return _UpdateRewardBusiness;
            }
        }

        private IUserAccountBusiness _UserAccountBusiness;
        public IUserAccountBusiness UserAccountBusiness
        {
            get
            {
                if (_UserAccountBusiness == null)
                {
                    _UserAccountBusiness = new UserAccountBusiness(this.logger, context);
                }
                return _UserAccountBusiness;
            }
        }

        private IUserGroupBusiness _UserGroupBusiness;
        public IUserGroupBusiness UserGroupBusiness
        {
            get
            {
                if (_UserGroupBusiness == null)
                {
                    _UserGroupBusiness = new UserGroupBusiness(this.logger, context);
                }
                return _UserGroupBusiness;
            }
        }

        private IUserProvinceBusiness _UserProvinceBusiness;
        public IUserProvinceBusiness UserProvinceBusiness
        {
            get
            {
                if (_UserProvinceBusiness == null)
                {
                    _UserProvinceBusiness = new UserProvinceBusiness(this.logger, context);
                }
                return _UserProvinceBusiness;
            }
        }

        private IUserRoleBusiness _UserRoleBusiness;
        public IUserRoleBusiness UserRoleBusiness
        {
            get
            {
                if (_UserRoleBusiness == null)
                {
                    _UserRoleBusiness = new UserRoleBusiness(this.logger, context);
                }
                return _UserRoleBusiness;
            }
        }

        private IViolatedCandidateBusiness _ViolatedCandidateBusiness;
        public IViolatedCandidateBusiness ViolatedCandidateBusiness
        {
            get
            {
                if (_ViolatedCandidateBusiness == null)
                {
                    _ViolatedCandidateBusiness = new ViolatedCandidateBusiness(this.logger, context);
                }
                return _ViolatedCandidateBusiness;
            }
        }

        private IViolatedInvigilatorBusiness _ViolatedInvigilatorBusiness;
        public IViolatedInvigilatorBusiness ViolatedInvigilatorBusiness
        {
            get
            {
                if (_ViolatedInvigilatorBusiness == null)
                {
                    _ViolatedInvigilatorBusiness = new ViolatedInvigilatorBusiness(this.logger, context);
                }
                return _ViolatedInvigilatorBusiness;
            }
        }

        private IWeeklyMenuBusiness _WeeklyMenuBusiness;
        public IWeeklyMenuBusiness WeeklyMenuBusiness
        {
            get
            {
                if (_WeeklyMenuBusiness == null)
                {
                    _WeeklyMenuBusiness = new WeeklyMenuBusiness(this.logger, context);
                }
                return _WeeklyMenuBusiness;
            }
        }

        private IWeeklyMenuDetailBusiness _WeeklyMenuDetailBusiness;
        public IWeeklyMenuDetailBusiness WeeklyMenuDetailBusiness
        {
            get
            {
                if (_WeeklyMenuDetailBusiness == null)
                {
                    _WeeklyMenuDetailBusiness = new WeeklyMenuDetailBusiness(this.logger, context);
                }
                return _WeeklyMenuDetailBusiness;
            }
        }

        private IWorkGroupTypeBusiness _WorkGroupTypeBusiness;
        public IWorkGroupTypeBusiness WorkGroupTypeBusiness
        {
            get
            {
                if (_WorkGroupTypeBusiness == null)
                {
                    _WorkGroupTypeBusiness = new WorkGroupTypeBusiness(this.logger, context);
                }
                return _WorkGroupTypeBusiness;
            }
        }

        private IWorkFlowUserBusiness _WorkFlowUserBusiness;
        public IWorkFlowUserBusiness WorkFlowUserBusiness
        {
            get
            {
                if (_WorkFlowUserBusiness == null)
                {
                    _WorkFlowUserBusiness = new WorkFlowUserBusiness(this.logger, context);
                }
                return _WorkFlowUserBusiness;
            }
        }

        private IWorkTypeBusiness _WorkTypeBusiness;
        public IWorkTypeBusiness WorkTypeBusiness
        {
            get
            {
                if (_WorkTypeBusiness == null)
                {
                    _WorkTypeBusiness = new WorkTypeBusiness(this.logger, context);
                }
                return _WorkTypeBusiness;
            }
        }

        private IProvinceSubjectBusiness _ProvinceSubjectBusiness;
        public IProvinceSubjectBusiness ProvinceSubjectBusiness
        {
            get
            {
                if (_ProvinceSubjectBusiness == null)
                {
                    _ProvinceSubjectBusiness = new ProvinceSubjectBusiness(this.logger, context);
                }
                return _ProvinceSubjectBusiness;
            }
        }

        private IEducationProgramBusiness _EducationProgramBusiness;
        public IEducationProgramBusiness EducationProgramBusiness
        {
            get
            {
                if (_EducationProgramBusiness == null)
                {
                    _EducationProgramBusiness = new EducationProgramBusiness(this.logger, context);
                }
                return _EducationProgramBusiness;
            }
        }
        private IClassificationCriteriaBusiness _ClassificationCriteriaBusiness;
        public IClassificationCriteriaBusiness ClassificationCriteriaBusiness
        {
            get
            {
                if (_ClassificationCriteriaBusiness == null)
                {
                    _ClassificationCriteriaBusiness = new ClassificationCriteriaBusiness(this.logger, context);
                }
                return _ClassificationCriteriaBusiness;
            }
        }
        private IClassificationCriteriaDetailBusiness _ClassificationCriteriaDetailBusiness;
        public IClassificationCriteriaDetailBusiness ClassificationCriteriaDetailBusiness
        {
            get
            {
                if (_ClassificationCriteriaDetailBusiness == null)
                {
                    _ClassificationCriteriaDetailBusiness = new ClassificationCriteriaDetailBusiness(this.logger, context);
                }
                return _ClassificationCriteriaDetailBusiness;
            }
        }

        private IMonitoringBookBusiness _MonitoringBookBusiness;
        public IMonitoringBookBusiness MonitoringBookBusiness
        {
            get
            {
                if (_MonitoringBookBusiness == null)
                {
                    _MonitoringBookBusiness = new MonitoringBookBusiness(this.logger, context);
                }
                return _MonitoringBookBusiness;
            }
        }
        private IStatisticSubcommitteeOfTertiaryBusiness _StatisticSubcommitteeOfTertiaryBusiness;
        public IStatisticSubcommitteeOfTertiaryBusiness StatisticSubcommitteeOfTertiaryBusiness
        {
            get
            {
                if (_StatisticSubcommitteeOfTertiaryBusiness == null)
                {
                    _StatisticSubcommitteeOfTertiaryBusiness = new StatisticSubcommitteeOfTertiaryBusiness(this.logger, context);
                }
                return _StatisticSubcommitteeOfTertiaryBusiness;
            }
        }
        private IEyeTestBusiness _EyeTestBusiness;
        public IEyeTestBusiness EyeTestBusiness
        {
            get
            {
                if (_EyeTestBusiness == null)
                {
                    _EyeTestBusiness = new EyeTestBusiness(this.logger, context);
                }
                return _EyeTestBusiness;
            }
        }

        private IDentalTestBusiness _DentalTestBusiness;
        public IDentalTestBusiness DentalTestBusiness
        {
            get
            {
                if (_DentalTestBusiness == null)
                {
                    _DentalTestBusiness = new DentalTestBusiness(this.logger, context);
                }
                return _DentalTestBusiness;
            }
        }

        private IENTTestBusiness _ENTTestBusiness;
        public IENTTestBusiness ENTTestBusiness
        {
            get
            {
                if (_ENTTestBusiness == null)
                {
                    _ENTTestBusiness = new ENTTestBusiness(this.logger, context);
                }
                return _ENTTestBusiness;
            }
        }

        private ISpineTestBusiness _SpineTestBusiness;
        public ISpineTestBusiness SpineTestBusiness
        {
            get
            {
                if (_SpineTestBusiness == null)
                {
                    _SpineTestBusiness = new SpineTestBusiness(this.logger, context);
                }
                return _SpineTestBusiness;
            }
        }

        private IOverallTestBusiness _OverallTestBusiness;
        public IOverallTestBusiness OverallTestBusiness
        {
            get
            {
                if (_OverallTestBusiness == null)
                {
                    _OverallTestBusiness = new OverallTestBusiness(this.logger, context);
                }
                return _OverallTestBusiness;
            }
        }

        private IPhysicalTestBusiness _PhysicalTestBusiness;
        public IPhysicalTestBusiness PhysicalTestBusiness
        {
            get
            {
                if (_PhysicalTestBusiness == null)
                {
                    _PhysicalTestBusiness = new PhysicalTestBusiness(this.logger, context);
                }
                return _PhysicalTestBusiness;
            }
        }

        private IStatisticSubjectOfTertiaryBusiness _StatisticSubjectOfTertiaryBusiness;
        public IStatisticSubjectOfTertiaryBusiness StatisticSubjectOfTertiaryBusiness
        {
            get
            {
                if (_StatisticSubjectOfTertiaryBusiness == null)
                {
                    _StatisticSubjectOfTertiaryBusiness = new StatisticSubjectOfTertiaryBusiness(this.logger, context);
                }
                return _StatisticSubjectOfTertiaryBusiness;
            }
        }
        private ISummedUpRecordClassBusiness _SummedUpRecordClassBusiness;
        public ISummedUpRecordClassBusiness SummedUpRecordClassBusiness
        {
            get
            {
                if (_SummedUpRecordClassBusiness == null)
                {
                    _SummedUpRecordClassBusiness = new SummedUpRecordClassBusiness(this.logger, context);
                }
                return _SummedUpRecordClassBusiness;
            }
        }

        private IThreadMarkBusiness _ThreadMarkBusiness;
        public IThreadMarkBusiness ThreadMarkBusiness
        {
            get
            {
                if (_ThreadMarkBusiness == null)
                {
                    _ThreadMarkBusiness = new ThreadMarkBusiness(this.logger, context);
                }
                return _ThreadMarkBusiness;
            }
        }

        private IVJudgeRecordBusiness _VJudgeRecordBusiness;
        public IVJudgeRecordBusiness VJudgeRecordBusiness
        {
            get
            {
                if (_VJudgeRecordBusiness == null)
                {
                    _VJudgeRecordBusiness = new VJudgeRecordBusiness(this.logger, context);
                }
                return _VJudgeRecordBusiness;
            }
        }

        private IVMarkRecordBusiness _VMarkRecordBusiness;
        public IVMarkRecordBusiness VMarkRecordBusiness
        {
            get
            {
                if (_VMarkRecordBusiness == null)
                {
                    _VMarkRecordBusiness = new VMarkRecordBusiness(this.logger, context);
                }
                return _VMarkRecordBusiness;
            }
        }

        private IStatisticLevelReportBusiness _StatisticLevelReportBusiness;
        public IStatisticLevelReportBusiness StatisticLevelReportBusiness
        {
            get
            {
                if (_StatisticLevelReportBusiness == null)
                {
                    _StatisticLevelReportBusiness = new StatisticLevelReportBusiness(this.logger, context);
                }
                return _StatisticLevelReportBusiness;
            }
        }

        private IStatisticLevelConfigBusiness _StatisticLevelConfigBusiness;
        public IStatisticLevelConfigBusiness StatisticLevelConfigBusiness
        {
            get
            {
                if (_StatisticLevelConfigBusiness == null)
                {
                    _StatisticLevelConfigBusiness = new StatisticLevelConfigBusiness(this.logger, context);
                }
                return _StatisticLevelConfigBusiness;
            }
        }

        private IVSummedUpRecordBusiness _VSummedUpRecordBusiness;
        public IVSummedUpRecordBusiness VSummedUpRecordBusiness
        {
            get
            {
                if (_VSummedUpRecordBusiness == null)
                {
                    _VSummedUpRecordBusiness = new VSummedUpRecordBusiness(this.logger, context);
                }
                return _VSummedUpRecordBusiness;
            }
        }

        private IVPupilRankingBusiness _VPupilRankingBusiness;
        public IVPupilRankingBusiness VPupilRankingBusiness
        {
            get
            {
                if (_VPupilRankingBusiness == null)
                {
                    _VPupilRankingBusiness = new VPupilRankingBusiness(this.logger, context);
                }
                return _VPupilRankingBusiness;
            }
        }
        private ILockTrainingBusiness _LockTrainingBusiness;
        public ILockTrainingBusiness LockTrainingBusiness
        {
            get
            {
                if (_LockTrainingBusiness == null)
                {
                    _LockTrainingBusiness = new LockTrainingBusiness(this.logger, context);
                }
                return _LockTrainingBusiness;
            }
        }

        private ILockMarkPrimaryBusiness _LockMarkPrimaryBusiness;
        public ILockMarkPrimaryBusiness LockMarkPrimaryBusiness
        {
            get
            {
                if (_LockMarkPrimaryBusiness == null)
                {
                    _LockMarkPrimaryBusiness = new LockMarkPrimaryBusiness(this.logger, context);
                }
                return _LockMarkPrimaryBusiness;
            }
        }

        private ILockRatedCommentPupilBusiness _LockRatedCommentPupilBusiness;
        public ILockRatedCommentPupilBusiness LockRatedCommentPupilBusiness
        {
            get
            {
                if (_LockRatedCommentPupilBusiness == null)
                {
                    _LockRatedCommentPupilBusiness = new LockRatedCommentPupilBusiness(this.logger, context);
                }
                return _LockRatedCommentPupilBusiness;
            }
        }

        private IEmisRequestBusiness _EmisRequestBusiness;
        public IEmisRequestBusiness EmisRequestBusiness
        {
            get
            {
                if (_EmisRequestBusiness == null)
                {
                    _EmisRequestBusiness = new EmisRequestBusiness(this.logger, context);
                }
                return _EmisRequestBusiness;
            }
        }

        private IMonthCommentsBusiness _MonthCommentsBusiness;
        public IMonthCommentsBusiness MonthCommentsBusiness
        {
            get
            {
                if (_MonthCommentsBusiness == null)
                {
                    _MonthCommentsBusiness = new MonthCommentsBusiness(this.logger, context);
                }
                return _MonthCommentsBusiness;
            }
        }

        private IEvaluationCriteriaBusiness _EvaluationCriteriaBusiness;
        public IEvaluationCriteriaBusiness EvaluationCriteriaBusiness
        {
            get
            {
                if (_EvaluationCriteriaBusiness == null)
                {
                    _EvaluationCriteriaBusiness = new EvaluationCriteriaBusiness(this.logger, context);
                }
                return _EvaluationCriteriaBusiness;
            }
        }

        private IEvaluationCommentsBusiness _EvaluationCommentsBusiness;
        public IEvaluationCommentsBusiness EvaluationCommentsBusiness
        {
            get
            {
                if (_EvaluationCommentsBusiness == null)
                {
                    _EvaluationCommentsBusiness = new EvaluationCommentsBusiness(this.logger, context);
                }
                return _EvaluationCommentsBusiness;
            }
        }

        private IEvaluationBusiness _EvaluationBusiness;
        public IEvaluationBusiness EvaluationBusiness
        {
            get
            {
                if (_EvaluationBusiness == null)
                {
                    _EvaluationBusiness = new EvaluationBusiness(this.logger, context);
                }
                return _EvaluationBusiness;
            }
        }

        private IEvaluationCommentsHistoryBusiness _EvaluationCommentsHistoryBusiness;
        public IEvaluationCommentsHistoryBusiness EvaluationCommentsHistoryBusiness
        {
            get
            {
                if (_EvaluationCommentsHistoryBusiness == null)
                {
                    _EvaluationCommentsHistoryBusiness = new EvaluationCommentsHistoryBusiness(this.logger, context);
                }
                return _EvaluationCommentsHistoryBusiness;
            }
        }


        private ISummedEvaluationBusiness _ISummedEvaluationBusiness;
        public ISummedEvaluationBusiness SummedEvaluationBusiness
        {
            get
            {
                if (_ISummedEvaluationBusiness == null)
                {
                    _ISummedEvaluationBusiness = new SummedEvaluationBusiness(this.logger, context);
                }
                return _ISummedEvaluationBusiness;
            }
        }

        private ISummedEvaluationHistoryBusiness _ISummedEvaluationHistoryBusiness;
        public ISummedEvaluationHistoryBusiness SummedEvaluationHistoryBusiness
        {
            get
            {
                if (_ISummedEvaluationHistoryBusiness == null)
                {
                    _ISummedEvaluationHistoryBusiness = new SummedEvaluationHistoryBusiness(this.logger, context);
                }
                return _ISummedEvaluationHistoryBusiness;
            }
        }


        private ISummedEndingEvaluationBusiness _SummedEndingEvaluationBusiness;
        public ISummedEndingEvaluationBusiness SummedEndingEvaluationBusiness
        {
            get
            {
                if (_SummedEndingEvaluationBusiness == null)
                {
                    _SummedEndingEvaluationBusiness = new SummedEndingEvaluationBusiness(this.logger, context);
                }
                return _SummedEndingEvaluationBusiness;
            }
        }

        private IRewardCommentFinalBusiness _RewardCommentFinalBusiness;
        public IRewardCommentFinalBusiness RewardCommentFinalBusiness
        {
            get
            {
                if (_RewardCommentFinalBusiness == null)
                {
                    _RewardCommentFinalBusiness = new RewardCommentFinalBusiness(this.logger, context);
                }
                return _RewardCommentFinalBusiness;
            }
        }

        private IRegisterSubjectSpecializeBusiness _RegisterSubjectSpecializeBusiness;
        public IRegisterSubjectSpecializeBusiness RegisterSubjectSpecializeBusiness
        {
            get
            {
                if (_RegisterSubjectSpecializeBusiness == null)
                {
                    _RegisterSubjectSpecializeBusiness = new RegisterSubjectSpecializeBusiness(this.logger, context);
                }
                return _RegisterSubjectSpecializeBusiness;
            }
        }

        private IRewardFinalBusiness _RewardFinalBusiness;
        public IRewardFinalBusiness RewardFinalBusiness
        {
            get
            {
                if (_RewardFinalBusiness == null)
                {
                    _RewardFinalBusiness = new RewardFinalBusiness(this.logger, context);
                }
                return _RewardFinalBusiness;
            }
        }

        private IExaminationsBusiness _ExaminationsBusiness;
        public IExaminationsBusiness ExaminationsBusiness
        {
            get
            {
                if (_ExaminationsBusiness == null)
                {
                    _ExaminationsBusiness = new ExaminationsBusiness(this.logger, context);
                }
                return _ExaminationsBusiness;
            }
        }

        private IExamGroupBusiness _ExamGroupBusiness;
        public IExamGroupBusiness ExamGroupBusiness
        {
            get
            {
                if (_ExamGroupBusiness == null)
                {
                    _ExamGroupBusiness = new ExamGroupBusiness(this.logger, context);
                }
                return _ExamGroupBusiness;
            }
        }

        private IExamSubjectBusiness _ExamSubjectBusiness;
        public IExamSubjectBusiness ExamSubjectBusiness
        {
            get
            {
                if (_ExamSubjectBusiness == null)
                {
                    _ExamSubjectBusiness = new ExamSubjectBusiness(this.logger, context);
                }
                return _ExamSubjectBusiness;
            }
        }

        private IExamPupilBusiness _ExamPupilBusiness;
        public IExamPupilBusiness ExamPupilBusiness
        {
            get
            {
                if (_ExamPupilBusiness == null)
                {
                    _ExamPupilBusiness = new ExamPupilBusiness(this.logger, context);
                }
                return _ExamPupilBusiness;
            }
        }

        private IExamRoomBusiness _ExamRoomBusiness;
        public IExamRoomBusiness ExamRoomBusiness
        {
            get
            {
                if (_ExamRoomBusiness == null)
                {
                    _ExamRoomBusiness = new ExamRoomBusiness(this.logger, context);
                }
                return _ExamRoomBusiness;
            }
        }

        private IExamSupervisoryBusiness _ExamSupervisoryBusiness;
        public IExamSupervisoryBusiness ExamSupervisoryBusiness
        {
            get
            {
                if (_ExamSupervisoryBusiness == null)
                {
                    _ExamSupervisoryBusiness = new ExamSupervisoryBusiness(this.logger, context);
                }
                return _ExamSupervisoryBusiness;
            }
        }

        private IExamSupervisoryAssignmentBusiness _ExamSupervisoryAssignmentBusiness;
        public IExamSupervisoryAssignmentBusiness ExamSupervisoryAssignmentBusiness
        {
            get
            {
                if (_ExamSupervisoryAssignmentBusiness == null)
                {
                    _ExamSupervisoryAssignmentBusiness = new ExamSupervisoryAssignmentBusiness(this.logger, context);
                }
                return _ExamSupervisoryAssignmentBusiness;
            }
        }

        private IExamBagBusiness _ExamBagBusiness;
        public IExamBagBusiness ExamBagBusiness
        {
            get
            {
                if (_ExamBagBusiness == null)
                {
                    _ExamBagBusiness = new ExamBagBusiness(this.logger, context);
                }
                return _ExamBagBusiness;
            }
        }

        private IExamDetachableBagBusiness _ExamDetachableBagBusiness;
        public IExamDetachableBagBusiness ExamDetachableBagBusiness
        {
            get
            {
                if (_ExamDetachableBagBusiness == null)
                {
                    _ExamDetachableBagBusiness = new ExamDetachableBagBusiness(this.logger, context);
                }
                return _ExamDetachableBagBusiness;
            }
        }

        private IExamPupilViolateBusiness _ExamPupilViolateBusiness;
        public IExamPupilViolateBusiness ExamPupilViolateBusiness
        {
            get
            {
                if (_ExamPupilViolateBusiness == null)
                {
                    _ExamPupilViolateBusiness = new ExamPupilViolateBusiness(this.logger, context);
                }
                return _ExamPupilViolateBusiness;
            }
        }

        private IExamSupervisoryViolateBusiness _ExamSupervisoryViolateBusiness;
        public IExamSupervisoryViolateBusiness ExamSupervisoryViolateBusiness
        {
            get
            {
                if (_ExamSupervisoryViolateBusiness == null)
                {
                    _ExamSupervisoryViolateBusiness = new ExamSupervisoryViolateBusiness(this.logger, context);
                }
                return _ExamSupervisoryViolateBusiness;
            }
        }

        private IExamPupilAbsenceBusiness _ExamPupilAbsenceBusiness;
        public IExamPupilAbsenceBusiness ExamPupilAbsenceBusiness
        {
            get
            {
                if (_ExamPupilAbsenceBusiness == null)
                {
                    _ExamPupilAbsenceBusiness = new ExamPupilAbsenceBusiness(this.logger, context);
                }
                return _ExamPupilAbsenceBusiness;
            }
        }

        private IExamInputMarkAssignedBusiness _ExamInputMarkAssignedBusiness;
        public IExamInputMarkAssignedBusiness ExamInputMarkAssignedBusiness
        {
            get
            {
                if (_ExamInputMarkAssignedBusiness == null)
                {
                    _ExamInputMarkAssignedBusiness = new ExamInputMarkAssignedBusiness(this.logger, context);
                }
                return _ExamInputMarkAssignedBusiness;
            }
        }

        private IExamInputMarkBusiness _ExamInputMarkBusiness;
        public IExamInputMarkBusiness ExamInputMarkBusiness
        {
            get
            {
                if (_ExamInputMarkBusiness == null)
                {
                    _ExamInputMarkBusiness = new ExamInputMarkBusiness(this.logger, context);
                }
                return _ExamInputMarkBusiness;
            }
        }


        private IExamCandenceBagBusiness _ExamCandenceBagBusiness;
        public IExamCandenceBagBusiness ExamCandenceBagBusiness
        {
            get
            {
                if (_ExamCandenceBagBusiness == null)
                {
                    _ExamCandenceBagBusiness = new ExamCandenceBagBusiness(this.logger, context);
                }
                return _ExamCandenceBagBusiness;
            }
        }

        private IEmployeeBreatherBusiness _EmployeeBreatherBusiness;
        public IEmployeeBreatherBusiness EmployeeBreatherBusiness
        {
            get
            {
                if (_EmployeeBreatherBusiness == null)
                {
                    _EmployeeBreatherBusiness = new EmployeeBreatherBusiness(this.logger, context);
                }
                return _EmployeeBreatherBusiness;
            }
        }

        private IEducationQualityStatisticBusiness _EducationQualityStatisticBusiness;
        public IEducationQualityStatisticBusiness EducationQualityStatisticBusiness
        {
            get
            {
                if (_EducationQualityStatisticBusiness == null)
                {
                    _EducationQualityStatisticBusiness = new EducationQualityStatisticBusiness(this.logger, context);
                }
                return _EducationQualityStatisticBusiness;
            }
        }

        private IApplicationConfigBusiness _ApplicationConfigBusiness;
        public IApplicationConfigBusiness ApplicationConfigBusiness
        {
            get
            {
                if (_ApplicationConfigBusiness == null)
                {
                    _ApplicationConfigBusiness = new ApplicationConfigBusiness(this.logger, context);
                }
                return _ApplicationConfigBusiness;
            }
        }
        //private IMarkBookPrimaryConfigBusiness _MarkBookPrimaryConfigBusiness;
        //public IMarkBookPrimaryConfigBusiness MarkBookPrimaryConfigBusiness
        //{
        //    get
        //    {
        //        if (_MarkBookPrimaryConfigBusiness == null)
        //        {
        //            _MarkBookPrimaryConfigBusiness = new MarkBookPrimaryConfigBusiness(this.logger, context);
        //        }
        //        return _MarkBookPrimaryConfigBusiness;
        //    }
        //}

        private IReportRequestBusiness _ReportRequestBusiness;
        public IReportRequestBusiness ReportRequestBusiness
        {
            get
            {
                if (_ReportRequestBusiness == null)
                {
                    _ReportRequestBusiness = new ReportRequestBusiness(this.logger, context);
                }
                return _ReportRequestBusiness;
            }
        }

        private IIndicatorBusiness _IndicatorBusiness;
        public IIndicatorBusiness IndicatorBusiness
        {
            get
            {
                if (_IndicatorBusiness == null)
                {
                    _IndicatorBusiness = new IndicatorBusiness(this.logger, context);
                }
                return _IndicatorBusiness;
            }
        }

        private IIndicatorDataBusiness _IndicatorDataBusiness;
        public IIndicatorDataBusiness IndicatorDataBusiness
        {
            get
            {
                if (_IndicatorDataBusiness == null)
                {
                    _IndicatorDataBusiness = new IndicatorDataBusiness(this.logger, context);
                }
                return _IndicatorDataBusiness;
            }
        }

        private IConfigConductRankingBusiness _ConfigConductRankingBusiness;
        public IConfigConductRankingBusiness ConfigConductRankingBusiness
        {
            get
            {
                if (_ConfigConductRankingBusiness == null)
                {
                    _ConfigConductRankingBusiness = new ConfigConductRankingBusiness(this.logger, context);
                }
                return _ConfigConductRankingBusiness;
            }
        }

        private IPushNotifyRequestBusiness _PushNotifyRequestBusiness;
        public IPushNotifyRequestBusiness PushNotifyRequestBusiness
        {
            get
            {
                if (_PushNotifyRequestBusiness == null)
                {
                    _PushNotifyRequestBusiness = new PushNotifyRequestBusiness(this.logger, context);
                }
                return _PushNotifyRequestBusiness;
            }
        }

        private IDOETExaminationsBusiness _DOETExaminationsBusiness;
        public IDOETExaminationsBusiness DOETExaminationsBusiness
        {
            get
            {
                if (_DOETExaminationsBusiness == null)
                {
                    _DOETExaminationsBusiness = new DOETExaminationsBusiness(this.logger, context);
                }
                return _DOETExaminationsBusiness;
            }
        }

        private IDOETExamGroupBusiness _DOETExamGroupBusiness;
        public IDOETExamGroupBusiness DOETExamGroupBusiness
        {
            get
            {
                if (_DOETExamGroupBusiness == null)
                {
                    _DOETExamGroupBusiness = new DOETExamGroupBusiness(this.logger, context);
                }
                return _DOETExamGroupBusiness;
            }
        }

        private IDOETSubjectBusiness _DOETSubjectBusiness;
        public IDOETSubjectBusiness DOETSubjectBusiness
        {
            get
            {
                if (_DOETSubjectBusiness == null)
                {
                    _DOETSubjectBusiness = new DOETSubjectBusiness(this.logger, context);
                }
                return _DOETSubjectBusiness;
            }
        }

        private IDOETExamSubjectBusiness _DOETExamSubjectBusiness;
        public IDOETExamSubjectBusiness DOETExamSubjectBusiness
        {
            get
            {
                if (_DOETExamSubjectBusiness == null)
                {
                    _DOETExamSubjectBusiness = new DOETExamSubjectBusiness(this.logger, context);
                }
                return _DOETExamSubjectBusiness;
            }
        }

        private IDOETExamPupilBusiness _DOETExamPupilBusiness;
        public IDOETExamPupilBusiness DOETExamPupilBusiness
        {
            get
            {
                if (_DOETExamPupilBusiness == null)
                {
                    _DOETExamPupilBusiness = new DOETExamPupilBusiness(this.logger, context);
                }
                return _DOETExamPupilBusiness;
            }
        }

        private IDOETExamMarkBusiness _DOETExamMarkBusiness;
        public IDOETExamMarkBusiness DOETExamMarkBusiness
        {
            get
            {
                if (_DOETExamMarkBusiness == null)
                {
                    _DOETExamMarkBusiness = new DOETExamMarkBusiness(this.logger, context);
                }
                return _DOETExamMarkBusiness;
            }
        }

        private IReviewBookPupilBusiness _ReviewBookPupilBusiness;
        public IReviewBookPupilBusiness ReviewBookPupilBusiness
        {
            get
            {
                if (_ReviewBookPupilBusiness == null)
                {
                    _ReviewBookPupilBusiness = new ReviewBookPupilBusiness(this.logger, context);
                }
                return _ReviewBookPupilBusiness;
            }
        }

        private IHtmlContentCodeBusiness _HtmlContentCodeBusiness;
        public IHtmlContentCodeBusiness HtmlContentCodeBusiness
        {
            get
            {
                if (_HtmlContentCodeBusiness == null)
                {
                    _HtmlContentCodeBusiness = new HtmlContentCodeBusiness(this.logger, context);
                }
                return _HtmlContentCodeBusiness;
            }
        }

        private IHtmlTemplateBusiness _HtmlTemplateBusiness;
        public IHtmlTemplateBusiness HtmlTemplateBusiness
        {
            get
            {
                if (_HtmlTemplateBusiness == null)
                {
                    _HtmlTemplateBusiness = new HtmlTemplateBusiness(this.logger, context);
                }
                return _HtmlTemplateBusiness;
            }
        }

        private ITemplateBusiness _TemplateBusiness;
        public ITemplateBusiness TemplateBusiness
        {
            get
            {
                if (_TemplateBusiness == null)
                {
                    _TemplateBusiness = new TemplateBusiness(this.logger, context);
                }
                return _TemplateBusiness;
            }
        }

        private IVacationReasonBusiness _VacationReasonBusiness;
        public IVacationReasonBusiness VacationReasonBusiness
        {
            get
            {
                if (_VacationReasonBusiness == null)
                {
                    _VacationReasonBusiness = new VacationReasonBusiness(this.logger, context);
                }
                return _VacationReasonBusiness;
            }
        }

        private IThreadMovedDataBusiness _ThreadMovedDataBusiness;
        public IThreadMovedDataBusiness ThreadMovedDataBusiness
        {
            get
            {
                if (_ThreadMovedDataBusiness == null)
                {
                    _ThreadMovedDataBusiness = new ThreadMovedDataBusiness(this.logger, context);
                }
                return _ThreadMovedDataBusiness;
            }
        }

        private IPupilRankingHistoryBusiness _PupilRankingHistoryBusiness;
        public IPupilRankingHistoryBusiness PupilRankingHistoryBusiness
        {
            get
            {
                if (_PupilRankingHistoryBusiness == null)
                {
                    _PupilRankingHistoryBusiness = new PupilRankingHistoryBusiness(this.logger, context);
                }
                return _PupilRankingHistoryBusiness;
            }
        }

        private IWorkFlowBusiness _IWorkFlowBusiness;
        public IWorkFlowBusiness WorkFlowBusiness
        {
            get
            {
                if (_IWorkFlowBusiness == null)
                {
                    _IWorkFlowBusiness = new WorkFlowBusiness(this.logger, context);
                }
                return _IWorkFlowBusiness;
            }
        }
        private ICollectionCommentsBusiness _ICollectionCommentsBusiness;
        public ICollectionCommentsBusiness CollectionCommentsBusiness
        {
            get
            {
                if (_ICollectionCommentsBusiness == null)
                {
                    _ICollectionCommentsBusiness = new CollectionCommentsBusiness(this.logger, context);
                }
                return _ICollectionCommentsBusiness;
            }
        }
        private ICommentsOfKindergartenBusiness _ICommentsOfKindergartenBusiness;
        public ICommentsOfKindergartenBusiness CommentsOfKindergartenBusiness
        {
            get
            {
                if (_ICommentsOfKindergartenBusiness == null)
                {
                    _ICommentsOfKindergartenBusiness = new CommentsOfKindergartenBusiness(this.logger, context);
                }
                return _ICommentsOfKindergartenBusiness;
            }
        }

        private IDistributeProgramBusiness _IDistributeProgramBusiness;
        public IDistributeProgramBusiness DistributeProgramBusiness
        {
            get
            {
                if (_IDistributeProgramBusiness == null)
                {
                    _IDistributeProgramBusiness = new DistributeProgramBusiness(this.logger, context);
                }
                return _IDistributeProgramBusiness;
            }
        }
        private IDistributeProgramSystemBusiness _IDistributeProgramSystemBusiness;
        public IDistributeProgramSystemBusiness DistributeProgramSystemBusiness
        {
            get
            {
                if (_IDistributeProgramSystemBusiness == null)
                {
                    _IDistributeProgramSystemBusiness = new DistributeProgramSystemBusiness(this.logger, context);
                }
                return _IDistributeProgramSystemBusiness;
            }
        }
        private ISchoolWeekBusiness _ISchoolWeekBusiness;
        public ISchoolWeekBusiness SchoolWeekBusiness
        {
            get
            {
                if (_ISchoolWeekBusiness == null)
                {
                    _ISchoolWeekBusiness = new SchoolWeekBusiness(this.logger, context);
                }
                return _ISchoolWeekBusiness;
            }
        }
        private ITeachingScheduleBusiness _ITeachingScheduleBusiness;
        public ITeachingScheduleBusiness TeachingScheduleBusiness
        {
            get
            {
                if (_ITeachingScheduleBusiness == null)
                {
                    _ITeachingScheduleBusiness = new TeachingScheduleBusiness(this.logger, context);
                }
                return _ITeachingScheduleBusiness;
            }
        }
        private ITeachingScheduleApprovalBusiness _ITeachingScheduleApprovalBusiness;
        public ITeachingScheduleApprovalBusiness TeachingScheduleApprovalBusiness
        {
            get
            {
                if (_ITeachingScheduleApprovalBusiness == null)
                {
                    _ITeachingScheduleApprovalBusiness = new TeachingScheduleApprovalBusiness(this.logger, context);
                }
                return _ITeachingScheduleApprovalBusiness;
            }
        }
        private IRatedCommentPupilBusiness _IRatedCommentPupilBusiness;
        public IRatedCommentPupilBusiness RatedCommentPupilBusiness
        {
            get
            {
                if (_IRatedCommentPupilBusiness == null)
                {
                    _IRatedCommentPupilBusiness = new RatedCommentPupilBusiness(this.logger, context);
                }
                return _IRatedCommentPupilBusiness;
            }
        }
        private IGrowthEvaluationBusiness _GrowthEvaluationBusiness;
        public IGrowthEvaluationBusiness GrowthEvaluationBusiness
        {
            get
            {
                if (_GrowthEvaluationBusiness == null)
                {
                    _GrowthEvaluationBusiness = new GrowthEvaluationBusiness(this.logger, context);
                }
                return _GrowthEvaluationBusiness;
            }
        }
        private IAssignSubjectConfigBusiness _AssignSubjectConfigBusiness;
        public IAssignSubjectConfigBusiness AssignSubjectConfigBusiness
        {
            get
            {
                if (_AssignSubjectConfigBusiness == null)
                {
                    _AssignSubjectConfigBusiness = new AssignSubjectConfigBusiness(this.logger, context);
                }
                return _AssignSubjectConfigBusiness;
            }
        }
        private IRatedCommentPupilHistoryBusiness _IRatedCommentPupilHistoryBusiness;
        public IRatedCommentPupilHistoryBusiness RatedCommentPupilHistoryBusiness
        {
            get
            {
                if (_IRatedCommentPupilHistoryBusiness == null)
                {
                    _IRatedCommentPupilHistoryBusiness = new RatedCommentPupilHistoryBusiness(this.logger, context);
                }
                return _IRatedCommentPupilHistoryBusiness;
            }
        }
        private ICommentOfPupilBusiness _ICommentOfPupilBusiness;
        public ICommentOfPupilBusiness CommentOfPupilBusiness
        {
            get
            {
                if (_ICommentOfPupilBusiness == null)
                {
                    _ICommentOfPupilBusiness = new CommentOfPupilBusiness(this.logger, context);
                }
                return _ICommentOfPupilBusiness;
            }
        }
        private ICommentOfPupilHistoryBusiness _ICommentOfPupilHistoryBusiness;
        public ICommentOfPupilHistoryBusiness CommentOfPupilHistoryBusiness
        {
            get
            {
                if (_ICommentOfPupilHistoryBusiness == null)
                {
                    _ICommentOfPupilHistoryBusiness = new CommentOfPupilHistoryBusiness(this.logger, context);
                }
                return _ICommentOfPupilHistoryBusiness;
            }
        }
        private IEvaluationRewardBusiness _IEvaluationRewardBusiness;
        public IEvaluationRewardBusiness EvaluationRewardBusiness
        {
            get
            {
                if (_IEvaluationRewardBusiness == null)
                {
                    _IEvaluationRewardBusiness = new EvaluationRewardBusiness(this.logger, context);
                }
                return _IEvaluationRewardBusiness;
            }
        }
        private IPhysicalExaminationBusiness _IPhysicalExaminationBusiness;
        public IPhysicalExaminationBusiness PhysicalExaminationBusiness
        {
            get
            {
                if (_IPhysicalExaminationBusiness == null)
                {
                    _IPhysicalExaminationBusiness = new PhysicalExaminationBusiness(this.logger, context);
                }
                return _IPhysicalExaminationBusiness;
            }
        }

        private IRestoreDataBusiness _IRestoreDataBusiness;
        public IRestoreDataBusiness RestoreDataBusiness
        {
            get
            {
                if (_IRestoreDataBusiness == null)
                {
                    _IRestoreDataBusiness = new RestoreDataBusiness(this.logger, context);
                }
                return _IRestoreDataBusiness;
            }
        }

        private IRestoreDataDetailBusiness _IRestoreDataDetailBusiness;
        public IRestoreDataDetailBusiness RestoreDataDetailBusiness
        {
            get
            {
                if (_IRestoreDataDetailBusiness == null)
                {
                    _IRestoreDataDetailBusiness = new RestoreDataDetailBusiness(this.logger, context);
                }
                return _IRestoreDataDetailBusiness;
            }
        }

        private IQuickHintBusiness _IQuickHintBusiness;
        public IQuickHintBusiness QuickHintBusiness
        {
            get
            {
                if (_IQuickHintBusiness == null)
                {
                    _IQuickHintBusiness = new QuickHintBusiness(this.logger, context);
                }
                return _IQuickHintBusiness;
            }
        }

        private IUsingMarkbookCommentBusiness _UsingMarkbookCommentBusiness;
        public IUsingMarkbookCommentBusiness UsingMarkbookCommentBusiness
        {
            get
            {
                if (_UsingMarkbookCommentBusiness == null)
                {
                    _UsingMarkbookCommentBusiness = new UsingMarkbookCommentBusiness(this.logger, context);
                }
                return _UsingMarkbookCommentBusiness;
            }
        }

private IMealCategoryBusiness _MealCategoryBusiness;
        public IMealCategoryBusiness MealCategoryBusiness
        {
            get
            {
                if (_MealCategoryBusiness == null)
                {
                    _MealCategoryBusiness = new MealCategoryBusiness(this.logger, context);
                }
                return _MealCategoryBusiness;
            }
        }
        private IWeeklyMealMenuBusiness _WeeklyMealMenuBusiness;
        public IWeeklyMealMenuBusiness WeeklyMealMenuBusiness
        {
            get
            {
                if (_WeeklyMealMenuBusiness == null)
                {
                    _WeeklyMealMenuBusiness = new WeeklyMealMenuBusiness(this.logger, context);
                }
                return _WeeklyMealMenuBusiness;
            }
        }
        private IDOETExamSupervisoryBusiness _DOETExamSupervisoryBusiness;
        public IDOETExamSupervisoryBusiness DOETExamSupervisoryBusiness
        {
            get
            {
                if (_DOETExamSupervisoryBusiness == null)
                {
                    _DOETExamSupervisoryBusiness = new DOETExamSupervisoryBusiness(this.logger, context);
                }
                return _DOETExamSupervisoryBusiness;
            }
        }

        private IColumnDescriptionBusiness _ColumnDescriptionBusiness;
        public IColumnDescriptionBusiness ColumnDescriptionBusiness
        {
            get
            {
                if (_ColumnDescriptionBusiness == null)
                {
                    _ColumnDescriptionBusiness = new ColumnDescriptionBusiness(this.logger, context);
                }
                return _ColumnDescriptionBusiness;
            }
        }

        private IEmployeeEvaluationBusiness _EmployeeEvaluationBusiness;
        public IEmployeeEvaluationBusiness EmployeeEvaluationBusiness
        {
            get
            {
                if (_EmployeeEvaluationBusiness == null)
                {
                    _EmployeeEvaluationBusiness = new EmployeeEvaluationBusiness(this.logger, context);
                }
                return _EmployeeEvaluationBusiness;
            }
        }

        private IEvaluationFieldBusiness _EvaluationFieldBusiness;
        public IEvaluationFieldBusiness EvaluationFieldBusiness
        {
            get
            {
                if (_EvaluationFieldBusiness == null)
                {
                    _EvaluationFieldBusiness = new EvaluationFieldBusiness(this.logger, context);
                }
                return _EvaluationFieldBusiness;
            }
        }

        private IEvaluationLevelBusiness _EvaluationLevelBusiness;
        public IEvaluationLevelBusiness EvaluationLevelBusiness
        {
            get
            {
                if (_EvaluationLevelBusiness == null)
                {
                    _EvaluationLevelBusiness = new EvaluationLevelBusiness(this.logger, context);
                }
                return _EvaluationLevelBusiness;
            }
        }

        private IForeignLanguageCatBusiness _ForeignLanguageCatBusiness;
        public IForeignLanguageCatBusiness ForeignLanguageCatBusiness
        {
            get
            {
                if (_ForeignLanguageCatBusiness == null)
                {
                    _ForeignLanguageCatBusiness = new ForeignLanguageCatBusiness(this.logger, context);
                }
                return _ForeignLanguageCatBusiness;
            }
        }

        private IProfileTemplateBusiness _ProfileTemplateBusiness;
        public IProfileTemplateBusiness ProfileTemplateBusiness
        {
            get
            {
                if (_ProfileTemplateBusiness == null)
                {
                    _ProfileTemplateBusiness = new ProfileTemplateBusiness(this.logger, context);
                }
                return _ProfileTemplateBusiness;
            }
        }

        private IVRatedCommentPupilBusiness _VRatedCommentPupilBusiness;
        public IVRatedCommentPupilBusiness VRatedCommentPupilBusiness
        {
            get
            {
                if (_VRatedCommentPupilBusiness == null)
                {
                    _VRatedCommentPupilBusiness = new VRatedCommentPupilBusiness(this.logger, context);
                }
                return _VRatedCommentPupilBusiness;
            }
        }
        #endregion
#region Business EDU
        private ISMSTypeDetailBusiness _SMSTypeDetailBusiness;
        public ISMSTypeDetailBusiness SMSTypeDetailBusiness
        {
            get
            {
                if (_SMSTypeDetailBusiness == null)
                {
                    _SMSTypeDetailBusiness = new SMSTypeDetailBusiness(this.logger, context);
                }
                return _SMSTypeDetailBusiness;
            }
        }
        private ISMSTypeBusiness _SMSTypeBusiness;
        public ISMSTypeBusiness SMSTypeBusiness
        {
            get
            {
                if (_SMSTypeBusiness == null)
                {
                    _SMSTypeBusiness = new SMSTypeBusiness(this.logger, context);
                }
                return _SMSTypeBusiness;
            }
        }
        private ICustomSMSTypeBusiness _CustomSMSTypeBusiness;
        public ICustomSMSTypeBusiness CustomSMSTypeBusiness
        {
            get
            {
                if (_CustomSMSTypeBusiness == null)
                {
                    _CustomSMSTypeBusiness = new CustomSMSTypeBusiness(this.logger, context);
                }
                return _CustomSMSTypeBusiness;
            }
        }
        private ISchoolConfigBusiness _SchoolConfigBusiness;
        public ISchoolConfigBusiness SchoolConfigBusiness
        {
            get
            {
                if (_SchoolConfigBusiness == null)
                {
                    _SchoolConfigBusiness = new SchoolConfigBusiness(this.logger, context);
                }
                return _SchoolConfigBusiness;
            }
        }
        private ISMSParentContractInClassDetailBusiness _SMSParentContractInClassDetailBusiness;
        public ISMSParentContractInClassDetailBusiness SMSParentContractInClassDetailBusiness
        {
            get
            {
                if (_SMSParentContractInClassDetailBusiness == null)
                {
                    _SMSParentContractInClassDetailBusiness = new SMSParentContractInClassDetailBusiness(this.logger, context);
                }
                return _SMSParentContractInClassDetailBusiness;
            }
        }
        private ISMSParentSentDetailBusiness _SMSParentSentDetailBusiness;
        public ISMSParentSentDetailBusiness SMSParentSentDetailBusiness
        {
            get
            {
                if (_SMSParentSentDetailBusiness == null)
                {
                    _SMSParentSentDetailBusiness = new SMSParentSentDetailBusiness(this.logger, context);
                }
                return _SMSParentSentDetailBusiness;
            }
        }
        private ISMSParentContractBusiness _SMSParentContractBusiness;
        public ISMSParentContractBusiness SMSParentContractBusiness
        {
            get
            {
                if (_SMSParentContractBusiness == null)
                {
                    _SMSParentContractBusiness = new SMSParentContractBusiness(this.logger, context);
                }
                return _SMSParentContractBusiness;
            }
        }
        private ISMSParentContractDetailBusiness _SMSParentContractDetailBusiness;
        public ISMSParentContractDetailBusiness SMSParentContractDetailBusiness
        {
            get
            {
                if (_SMSParentContractDetailBusiness == null)
                {
                    _SMSParentContractDetailBusiness = new SMSParentContractDetailBusiness(this.logger, context);
                }
                return _SMSParentContractDetailBusiness;
            }
        }
        private IServicePackageDetailBusiness _ServicePackageDetailBusiness;
        public IServicePackageDetailBusiness ServicePackageDetailBusiness
        {
            get
            {
                if (_ServicePackageDetailBusiness == null)
                {
                    _ServicePackageDetailBusiness = new ServicePackageDetailBusiness(this.logger, context);
                }
                return _ServicePackageDetailBusiness;
            }
        }
        private IServicePackageDeclareBusiness _ServicePackageDeclareBusiness;
        public IServicePackageDeclareBusiness ServicePackageDeclareBusiness
        {
            get
            {
                if (_ServicePackageDeclareBusiness == null)
                {
                    _ServicePackageDeclareBusiness = new ServicePackageDeclareBusiness(this.logger, context);
                }
                return _ServicePackageDeclareBusiness;
            }
        }

        private IServicePackageBusiness _ServicePackageBusiness;
        public IServicePackageBusiness ServicePackageBusiness
        {
            get
            {
                if (_ServicePackageBusiness == null)
                {
                    _ServicePackageBusiness = new ServicePackageBusiness(this.logger, context);
                }
                return _ServicePackageBusiness;
            }
        }

        private IServicePackageSchoolDetailBusiness _ServicePackageSchoolDetailBusiness;
        public IServicePackageSchoolDetailBusiness ServicePackageSchoolDetailBusiness
        {
            get
            {
                if (_ServicePackageSchoolDetailBusiness == null)
                {
                    _ServicePackageSchoolDetailBusiness = new ServicePackageSchoolDetailBusiness(this.logger, context);
                }
                return _ServicePackageSchoolDetailBusiness;
            }
        }

        private ISentRecordBusiness _SentRecordBusiness;
        public ISentRecordBusiness SentRecordBusiness
        {
            get
            {
                if (_SentRecordBusiness == null)
                {
                    _SentRecordBusiness = new SentRecordBusiness(this.logger, context);
                }
                return _SentRecordBusiness;
            }
        }

        private IRegisterSMSFreeBusiness _RegisterSMSFreeBusiness;
        public IRegisterSMSFreeBusiness RegisterSMSFreeBusiness
        {
            get
            {
                if (_RegisterSMSFreeBusiness == null)
                {
                    _RegisterSMSFreeBusiness = new RegisterSMSFreeBusiness(this.logger, context);
                }
                return _RegisterSMSFreeBusiness;
            }
        }

        private ISMSParentContractExtraBusiness _SMSParentContractExtraBusiness;
        public ISMSParentContractExtraBusiness SMSParentContractExtraBusiness
        {
            get
            {
                if (_SMSParentContractExtraBusiness == null)
                {
                    _SMSParentContractExtraBusiness = new SMSParentContractExtraBusiness(this.logger, context);
                }
                return _SMSParentContractExtraBusiness;
            }
        }
        private ISMSHistoryBusiness _SMSHistoryBusiness;
        public ISMSHistoryBusiness SMSHistoryBusiness
        {
            get
            {
                if (_SMSHistoryBusiness == null)
                {
                    _SMSHistoryBusiness = new SMSHistoryBusiness(this.logger, context);
                }
                return _SMSHistoryBusiness;
            }
        }

        private IEWalletBusiness _EWalletBusiness;
        public IEWalletBusiness EWalletBusiness
        {
            get
            {
                if (_EWalletBusiness == null)
                {
                    _EWalletBusiness = new EWalletBusiness(this.logger, context);
                }
                return _EWalletBusiness;
            }
        }

        private IEWalletTransactionBusiness _EWalletTransactionBusiness;
        public IEWalletTransactionBusiness EWalletTransactionBusiness
        {
            get
            {
                if (_EWalletTransactionBusiness == null)
                {
                    _EWalletTransactionBusiness = new EWalletTransactionBusiness(this.logger, context);
                }
                return _EWalletTransactionBusiness;
            }
        }

        private IEwalletTransactionTypeBusiness _EwalletTransactionTypeBusiness;
        public IEwalletTransactionTypeBusiness EwalletTransactionTypeBusiness
        {
            get
            {
                if (_EwalletTransactionTypeBusiness == null)
                {
                    _EwalletTransactionTypeBusiness = new EwalletTransactionTypeBusiness(this.logger, context);
                }
                return _EwalletTransactionTypeBusiness;
            }
        }

        private ITopupBusiness _TopupBusiness;
        public ITopupBusiness TopupBusiness
        {
            get
            {
                if (_TopupBusiness == null)
                {
                    _TopupBusiness = new TopupBusiness(this.logger, context);
                }
                return _TopupBusiness;
            }
        }

        private ITeacherContactBusiness _TeacherContactBusiness;
        public ITeacherContactBusiness TeacherContactBusiness
        {
            get
            {
                if (_TeacherContactBusiness == null)
                {
                    _TeacherContactBusiness = new TeacherContactBusiness(this.logger, context);
                }
                return _TeacherContactBusiness;
            }
        }

        private IPromotionBusiness _PromotionBusiness;
        public IPromotionBusiness PromotionBusiness
        {
            get
            {
                if (_PromotionBusiness == null)
                {
                    _PromotionBusiness = new PromotionBusiness(this.logger, context);
                }
                return _PromotionBusiness;
            }
        }

        private IPromotionDetailBusiness _PromotionDetailBusiness;
        public IPromotionDetailBusiness PromotionDetailBusiness
        {
            get
            {
                if (_PromotionDetailBusiness == null)
                {
                    _PromotionDetailBusiness = new PromotionDetailBusiness(this.logger, context);
                }
                return _PromotionDetailBusiness;
            }
        }

        private IReceivedRecordBusiness _ReceivedRecordBusiness;
        public IReceivedRecordBusiness ReceivedRecordBusiness
        {
            get
            {
                if (_ReceivedRecordBusiness == null)
                {
                    _ReceivedRecordBusiness = new ReceivedRecordBusiness(this.logger, context);
                }
                return _ReceivedRecordBusiness;
            }
        }
        private IBrandnameRegistrationBusiness _BrandnameRegistrationBusiness;
        public IBrandnameRegistrationBusiness BrandnameRegistrationBusiness
        {
            get
            {
                if (_BrandnameRegistrationBusiness == null)
                {
                    _BrandnameRegistrationBusiness = new BrandnameRegistrationBusiness(this.logger, context);
                }
                return _BrandnameRegistrationBusiness;
            }
        }
        private IBrandnameProviderBusiness _BrandnameProviderBusiness;
        public IBrandnameProviderBusiness BrandnameProviderBusiness
        {
            get
            {
                if (_BrandnameProviderBusiness == null)
                {
                    _BrandnameProviderBusiness = new BrandnameProviderBusiness(this.logger, context);
                }
                return _BrandnameProviderBusiness;
            }
        }
        private ITimerConfigBusiness _TimerConfigBusiness;
        public ITimerConfigBusiness TimerConfigBusiness
        {
            get
            {
                if (_TimerConfigBusiness == null)
                {
                    _TimerConfigBusiness = new TimerConfigBusiness(this.logger, context);
                }
                return _TimerConfigBusiness;
            }
        }
        private ITimerTransactionBusiness _TimerTransactionBusiness;
        public ITimerTransactionBusiness TimerTransactionBusiness
        {
            get
            {
                if (_TimerTransactionBusiness == null)
                {
                    _TimerTransactionBusiness = new TimerTransactionBusiness(this.logger, context);
                }
                return _TimerTransactionBusiness;
            }
        }
        
        #endregion

        #region SCS Entities
        private ILocationBusiness _ILocationBusiness;
        public ILocationBusiness LocationBusiness
        {
            get
            {
                if (_ILocationBusiness == null)
                {
                    _ILocationBusiness = new LocationBusiness(this.logger, context);
                }
                return _ILocationBusiness;
            }
        }


        private IActivityOfClassBusiness _IActivityOfClassBusiness;
        public IActivityOfClassBusiness ActivityOfClassBusiness
        {
            get
            {
                if (_IActivityOfClassBusiness == null)
                {
                    _IActivityOfClassBusiness = new ActivityOfClassBusiness(this.logger, context);
                }
                return _IActivityOfClassBusiness;
            }
        }

        private IManagementHealthInsuranceBusiness _IManagementHealthInsuranceBusiness;
        public IManagementHealthInsuranceBusiness IManagementHealthInsuranceBusiness
        {
            get
            {
                if (_IManagementHealthInsuranceBusiness == null)
                {
                    _IManagementHealthInsuranceBusiness = new ManagementHealthInsuranceBusiness(this.logger, context);
                }
                return _IManagementHealthInsuranceBusiness;
            }
        }

        private IActivityOfPupilBusiness _IActivityOfPupilBusiness;
        public IActivityOfPupilBusiness ActivityOfPupilBusiness
        {
            get
            {
                if (_IActivityOfPupilBusiness == null)
                {
                    _IActivityOfPupilBusiness = new ActivityOfPupilBusiness(this.logger, context);
                }
                return _IActivityOfPupilBusiness;
            }
        }

        private IContactGroupBusiness _IContactGroupBusiness;
        public IContactGroupBusiness ContactGroupBusiness
        {
            get
            {
                if (_IContactGroupBusiness == null)
                {
                    _IContactGroupBusiness = new ContactGroupBusiness(this.logger, context);
                }
                return _IContactGroupBusiness;
            }
        }

        private IEmployeeContactBusiness _IEmployeeContactBusiness;
        public IEmployeeContactBusiness EmployeeContactBusiness
        {
            get
            {
                if (_IEmployeeContactBusiness == null)
                {
                    _IEmployeeContactBusiness = new EmployeeContactBusiness(this.logger, context);
                }
                return _IEmployeeContactBusiness;
            }
        }

        private IMTBusiness _IMTBusiness;
        public IMTBusiness MTBusiness
        {
            get
            {
                if (_IMTBusiness == null)
                {
                    _IMTBusiness = new MTBusiness(this.logger, context);
                }
                return _IMTBusiness;
            }
        }

        private IHistorySMSBusiness _HistorySMSBusiness;
        public IHistorySMSBusiness HistorySMSBusiness
        {
            get
            {
                if (_HistorySMSBusiness == null)
                {
                    _HistorySMSBusiness = new HistorySMSBusiness(this.logger, context);
                }
                return _HistorySMSBusiness;
            }
        }

        private ICalendarScheduleBusiness _ICalendarScheduleBusiness;
        public ICalendarScheduleBusiness CalendarScheduleBusiness
        {
            get
            {
                if (_ICalendarScheduleBusiness == null)
                {
                    _ICalendarScheduleBusiness = new CalendarScheduleBusiness(this.logger, context);
                }
                return _ICalendarScheduleBusiness;
            }
        }


        private ISkillTitleBusiness _SkillTitleBusiness;
        public ISkillTitleBusiness SkillTitleBusiness
        {
            get
            {
                if (_SkillTitleBusiness == null)
                {
                    _SkillTitleBusiness = new SkillTitleBusiness(this.logger, context);
                }
                return _SkillTitleBusiness;
            }
        }

        private ISkillBusiness _ISkillBusiness;
        public ISkillBusiness SkillBusiness
        {
            get
            {
                if (_ISkillBusiness == null)
                {
                    _ISkillBusiness = new SkillBusiness(this.logger, context);
                }
                return _ISkillBusiness;
            }
        }

        private ISkillTitleClassBusiness _ISkillTitleClassBusiness;
        public ISkillTitleClassBusiness SkillTitleClassBusiness
        {
            get
            {
                if (_ISkillTitleClassBusiness == null)
                {
                    _ISkillTitleClassBusiness = new SkillTitleClassBusiness(this.logger, context);
                }
                return _ISkillTitleClassBusiness;
            }
        }

        private ISkillOfClassBusiness _ISkillOfClassBusiness;
        public ISkillOfClassBusiness SkillOfClassBusiness
        {
            get
            {
                if (_ISkillOfClassBusiness == null)
                {
                    _ISkillOfClassBusiness = new SkillOfClassBusiness(this.logger, context);
                }
                return _ISkillOfClassBusiness;
            }
        }


        private ISkillOfPupilBusiness _ISkillOfPupilBusiness;
        public ISkillOfPupilBusiness SkillOfPupilBusiness
        {
            get
            {
                if (_ISkillOfPupilBusiness == null)
                {
                    _ISkillOfPupilBusiness = new SkillOfPupilBusiness(this.logger, context);
                }
                return _ISkillOfPupilBusiness;
            }
        }
        private IStatisticCMBusiness _StatisticCMBusiness;
        public IStatisticCMBusiness StatisticCMBusiness
        {
            get
            {
                if (_StatisticCMBusiness == null)
                {
                    _StatisticCMBusiness = new StatisticCMBusiness(this.logger, context);
                }
                return _StatisticCMBusiness;
            }
        }
        private ICBCollaboratorBusiness _CBCollaboratorBusiness;
        public ICBCollaboratorBusiness CBCollaboratorBusiness
        {
            get
            {
                if (_CBCollaboratorBusiness == null)
                {
                    _CBCollaboratorBusiness = new CBCollaboratorBusiness(this.logger, context);
                }
                return _CBCollaboratorBusiness;
            }
        }
        private ICBContractBusiness _CBContractBusiness;
        public ICBContractBusiness CBContractBusiness
        {
            get
            {
                if (_CBContractBusiness == null)
                {
                    _CBContractBusiness = new CBContractBusiness(this.logger, context);
                }
                return _CBContractBusiness;
            }
        }
        private ICBContractTransactionBusiness _CBContractTransactionBusiness;
        public ICBContractTransactionBusiness CBContractTransactionBusiness
        {
            get
            {
                if (_CBContractTransactionBusiness == null)
                {
                    _CBContractTransactionBusiness = new CBContractTransactionBusiness(this.logger, context);
                }
                return _CBContractTransactionBusiness;
            }
        }
        private ICBChannelTypeBusiness _CBChannelTypeBusiness;
        public ICBChannelTypeBusiness CBChannelTypeBusiness
        {
            get
            {
                if (_CBChannelTypeBusiness == null)
                {
                    _CBChannelTypeBusiness = new CBChannelTypeBusiness(this.logger, context);
                }
                return _CBChannelTypeBusiness;
            }
        }
        private ICBManagingAscriptionBusiness _CBManagingAscriptionBusiness;
        public ICBManagingAscriptionBusiness CBManagingAscriptionBusiness
        {
            get
            {
                if (_CBManagingAscriptionBusiness == null)
                {
                    _CBManagingAscriptionBusiness = new CBManagingAscriptionBusiness(this.logger, context);
                }
                return _CBManagingAscriptionBusiness;
            }
        }
        private ICBRoamingModeBusiness _CBRoamingModeBusiness;
        public ICBRoamingModeBusiness CBRoamingModeBusiness
        {
            get
            {
                if (_CBRoamingModeBusiness == null)
                {
                    _CBRoamingModeBusiness = new CBRoamingModeBusiness(this.logger, context);
                }
                return _CBRoamingModeBusiness;
            }
        }
        private ICBShopBusiness _CBShopBusiness;
        public ICBShopBusiness CBShopBusiness
        {
            get
            {
                if (_CBShopBusiness == null)
                {
                    _CBShopBusiness = new CBShopBusiness(this.logger, context);
                }
                return _CBShopBusiness;
            }
        }
       
        #endregion
    }
}