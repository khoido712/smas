﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SMAS.Models.Models;
using SMAS.Business.IBusiness;
using log4net;
using SMAS.DAL.IRepository;
using SMAS.DAL.Repository;

namespace SMAS.Business.Business
{
    public partial class SchoolCodeConfigBusiness: GenericBussiness<SchoolCodeConfig>, ISchoolCodeConfigBusiness
    {
      
        ISchoolCodeConfigRepository SchoolCodeConfigRepository;

        public SchoolCodeConfigBusiness(ILog logger, SMASEntities context = null)
            : base(logger)
        {
            if (context == null) { context = new SMASEntities(); } this.context = context;
            this.SchoolCodeConfigRepository = new SchoolCodeConfigRepository(context);
            repository = SchoolCodeConfigRepository;
        }
    }
}