﻿/**
 * The Viettel License
 *
 * Copyright 2012 Viettel Telecom. All rights reserved.
 * VIETTEL PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */

/** 
* @author  
* @version $Revision: $
*/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using log4net;
using SMAS.Business.IBusiness;
using SMAS.DAL.IRepository;
using SMAS.DAL.Repository;
using SMAS.Models.Models;

namespace SMAS.Business.Business
{
    public partial class ThreadMarkBusiness : GenericBussiness<ThreadMark>, IThreadMarkBusiness
    {
        IThreadMarkRepository ThreadMarkRepository;
        public ThreadMarkBusiness(ILog logger, SMASEntities context = null)
            : base(logger)
        {
            if (context == null) { context = new SMASEntities(); } this.context = context;
            this.ThreadMarkRepository = new ThreadMarkRepository(context);
            repository = ThreadMarkRepository;
           
        }
    }
}
