/**
 * The Viettel License
 *
 * Copyright 2012 Viettel Telecom. All rights reserved.
 * VIETTEL PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */

/** 
* @author  
* @version $Revision: $
*/

using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Linq.Expressions;
using System.Web;
using log4net;

using SMAS.DAL.Repository;
using SMAS.DAL.IRepository;
using SMAS.Business.IBusiness;
using SMAS.Models.Models;

namespace SMAS.Business.Business
{ 
    public partial class ParticularPupilTreatmentBusiness : GenericBussiness<ParticularPupilTreatment>, IParticularPupilTreatmentBusiness
    {  
		SMASEntities context;
		IParticularPupilTreatmentRepository  ParticularPupilTreatmentRepository;
        IParticularPupilBusiness ParticularPupilBusiness;
        IPupilProfileBusiness PupilProfileBusiness;
        IAcademicYearBusiness AcademicYearBusiness;
        IPupilOfClassBusiness PupilOfClassBusiness;

		public ParticularPupilTreatmentBusiness(ILog logger, SMASEntities context = null) : base(logger)
        {
			if (context == null){context = new SMASEntities();}this.context = context;
			this.ParticularPupilTreatmentRepository = new ParticularPupilTreatmentRepository(context);
            this.ParticularPupilBusiness = new ParticularPupilBusiness(logger, context);
            this.PupilProfileBusiness = new PupilProfileBusiness(logger, context);
            this.AcademicYearBusiness = new AcademicYearBusiness(logger, context);
            this.PupilOfClassBusiness = new PupilOfClassBusiness(logger, context);
            repository = ParticularPupilTreatmentRepository;
        }
        
    }
}