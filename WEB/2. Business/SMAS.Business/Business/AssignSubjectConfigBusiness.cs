﻿using log4net;
using SMAS.Business.IBusiness;
using SMAS.DAL.IRepository;
using SMAS.DAL.Repository;
using SMAS.Models.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SMAS.Business.Business
{
    public partial class AssignSubjectConfigBusiness : GenericBussiness<AssignSubjectConfig>, IAssignSubjectConfigBusiness
    {
        IAssignSubjectConfigRepository AssignSubjectConfigRepository;

        public AssignSubjectConfigBusiness(ILog logger, SMASEntities context = null)
            : base(logger)
        {
            if (context == null) { context = new SMASEntities(); } this.context = context;
            this.AssignSubjectConfigRepository = new AssignSubjectConfigRepository(context);
            repository = AssignSubjectConfigRepository;
        }
    }
}
