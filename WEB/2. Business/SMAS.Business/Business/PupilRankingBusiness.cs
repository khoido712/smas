/**
 * The Viettel License
 *
 * Copyright 2012 Viettel Telecom. All rights reserved.
 * VIETTEL PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */

/** 
* @author  
* @version $Revision: $
*/

using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Linq.Expressions;
using System.Web;
using log4net;

using SMAS.DAL.Repository;
using SMAS.DAL.IRepository;
using SMAS.Business.IBusiness;
using SMAS.Models.Models;

namespace SMAS.Business.Business
{
    public partial class PupilRankingBusiness : GenericBussiness<PupilRanking>, IPupilRankingBusiness
    {
        IPupilRankingRepository PupilRankingRepository;
        IVPupilRankingRepository VPupilRankingRepository;
        IPupilOfClassRepository PupilOfClassRepository;
        IPupilFaultRepository PupilFaultRepository;
        IPupilEmulationRepository PupilEmulationRepository;
        ISummedUpRecordRepository SummedUpRecordRepository;
        IClassSubjectRepository ClassSubjectRepository;
        ISchoolMovementRepository SchoolMovementRepository;
        IClassMovementRepository ClassMovementRepository;
        IExemptedSubjectRepository ExemptedSubjectRepository;
        IPupilAbsenceRepository PupilAbsenceRepository;
        IClassProfileRepository ClassProfileRepository;
        IPupilRetestRegistrationRepository PupilRetestRegistrationRepository;
        IConductEvaluationDetailRepository ConductEvaluationDetailRepository;
        IConductLevelRepository ConductLevelRepository;
        IPupilProfileRepository PupilProfileRepository;
        IPupilLeavingOffRepository PupilLeavingOffRepository;
        IPeriodDeclarationRepository PeriodDeclarationRepository;
        IConductConfigByViolationRepository ConductConfigByViolationRepository;
        IAcademicYearRepository AcademicYearRepository;
        ICapacityLevelRepository CapacityLevelRepository;
        ISummedUpRecordClassRepository SummedUpRecordClassRepository;
        IProcessedReportRepository ProcessedReportRepository;
        IProcessedReportParameterRepository ProcessedReportParameterRepository;
        
        public PupilRankingBusiness(ILog logger, SMASEntities context = null)
            : base(logger)
        {
            if (context == null) { context = new SMASEntities(); } this.context = context;
            this.PupilRankingRepository = new PupilRankingRepository(context);
            this.PupilOfClassRepository = new PupilOfClassRepository(context);
            this.PupilFaultRepository = new PupilFaultRepository(context);
            this.PupilEmulationRepository = new PupilEmulationRepository(context);
            this.SummedUpRecordRepository = new SummedUpRecordRepository(context);
            this.ClassSubjectRepository = new ClassSubjectRepository(context);
            this.ExemptedSubjectRepository = new ExemptedSubjectRepository(context);
            this.PupilAbsenceRepository = new PupilAbsenceRepository(context);
            this.ClassProfileRepository = new ClassProfileRepository(context);
            this.PupilRetestRegistrationRepository = new PupilRetestRegistrationRepository(context);
            this.ConductEvaluationDetailRepository = new ConductEvaluationDetailRepository(context);
            this.ConductLevelRepository = new ConductLevelRepository(context);
            this.PupilProfileRepository = new PupilProfileRepository(context);
            this.PeriodDeclarationRepository = new PeriodDeclarationRepository(context);
            this.ConductConfigByViolationRepository = new ConductConfigByViolationRepository(context);
            this.AcademicYearRepository = new AcademicYearRepository(context);
            this.PupilLeavingOffRepository = new PupilLeavingOffRepository(context);
            this.SchoolMovementRepository = new SchoolMovementRepository(context);
            this.ClassMovementRepository = new ClassMovementRepository(context);
            this.CapacityLevelRepository = new CapacityLevelRepository(context);
            this.SummedUpRecordClassRepository = new SummedUpRecordClassRepository(context);
            this.VPupilRankingRepository = new VPupilRankingRepository(context);
            this.ProcessedReportRepository = new ProcessedReportRepository(context);
            this.ProcessedReportParameterRepository = new ProcessedReportParameterRepository(context);
            repository = PupilRankingRepository;
        }
    }
}