/**
 * The Viettel License
 *
 * Copyright 2012 Viettel Telecom. All rights reserved.
 * VIETTEL PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */

/**
* @author
* @version $Revision: $
*/

using log4net;
using SMAS.Business.IBusiness;
using SMAS.DAL.IRepository;
using SMAS.DAL.Repository;
using SMAS.Models.Models;

namespace SMAS.Business.Business
{
    public partial class ExaminationSubjectBusiness : GenericBussiness<ExaminationSubject>, IExaminationSubjectBusiness
    {
        private IExaminationSubjectRepository ExaminationSubjectRepository;
        private ISchoolProfileRepository SchoolProfileRepository;
        private IExaminationRepository ExaminationRepository;
        private IEducationLevelRepository EducationLevelRepository;
        private ICandidateRepository CandidateRepository;
        private IExaminationRoomRepository ExaminationRoomRepository;
        public ExaminationSubjectBusiness(ILog logger, SMASEntities context = null)
            : base(logger)
        {
            if (context == null) { context = new SMASEntities(); } this.context = context;
            this.ExaminationSubjectRepository = new ExaminationSubjectRepository(context);
            this.SchoolProfileRepository = new SchoolProfileRepository(context);
            this.ExaminationRepository = new ExaminationRepository(context);
            this.EducationLevelRepository = new EducationLevelRepository(context);
            this.CandidateRepository = new CandidateRepository(context);
            this.ExaminationRoomRepository = new ExaminationRoomRepository(context);
            repository = ExaminationSubjectRepository;
        }
    }
}