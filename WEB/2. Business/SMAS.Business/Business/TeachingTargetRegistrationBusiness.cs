/**
 * The Viettel License
 *
 * Copyright 2012 Viettel Telecom. All rights reserved.
 * VIETTEL PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */

/** 
* @author  
* @version $Revision: $
*/

using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Linq.Expressions;
using System.Web;
using log4net;

using SMAS.DAL.Repository;
using SMAS.DAL.IRepository;
using SMAS.Business.IBusiness;
using SMAS.Models.Models;

namespace SMAS.Business.Business
{
    public partial class TeachingTargetRegistrationBusiness : GenericBussiness<TeachingTargetRegistration>, ITeachingTargetRegistrationBusiness
    {
        ITeachingTargetRegistrationRepository TeachingTargetRegistrationRepository;
        //IEmployeeBusiness EmployeeBusiness;
        //IAcademicYearBusiness AcademicYearBusiness;
        //ITeachingAssignmentBusiness TeachingAssignmentBusiness;
        public TeachingTargetRegistrationBusiness(ILog logger, SMASEntities context = null)
            : base(logger)
        {
            if (context == null){context = new SMASEntities();}this.context = context;
            this.TeachingTargetRegistrationRepository = new TeachingTargetRegistrationRepository(context);
            repository = TeachingTargetRegistrationRepository;
        }

        //public TeachingTargetRegistrationBusiness(ILog logger, IEmployeeBusiness EmployeeBusiness, ITeachingAssignmentBusiness TeachingAssignmentBusiness,
        //    IAcademicYearBusiness AcademicYearBusiness)
        //    : base(logger)
        //{
        //    if (context == null){context = new SMASEntities();}this.context = context;
        //    this.TeachingTargetRegistrationRepository = new TeachingTargetRegistrationRepository(context);
        //    repository = TeachingTargetRegistrationRepository;
        //    this.EmployeeBusiness = EmployeeBusiness;
        //    this.TeachingAssignmentBusiness = TeachingAssignmentBusiness;
        //    this.AcademicYearBusiness = AcademicYearBusiness;
        //}
    }
}