﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SMAS.Models.Models;
using SMAS.DAL.IRepository;
using log4net;
using SMAS.DAL.Repository;
using SMAS.Business.IBusiness;

namespace SMAS.Business.Business
{
    public partial class DistributeProgramBusiness : GenericBussiness<DistributeProgram>, IDistributeProgramBusiness
    {
        IDistributeProgramRepository DistributeProgramRepository;

        public DistributeProgramBusiness(ILog logger, SMASEntities context = null)
            : base(logger)
        {
            if (context == null) { context = new SMASEntities(); }
            this.context = context;
            this.DistributeProgramRepository = new DistributeProgramRepository(this.context);

            repository = DistributeProgramRepository;
        }
    }
}