﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Linq.Expressions;
using System.Web;
using log4net;

using SMAS.DAL.Repository;
using SMAS.DAL.IRepository;
using SMAS.Business.IBusiness;
using SMAS.Models.Models;

namespace SMAS.Business.Business
{
    public partial class PhysicalExaminationBusiness : GenericBussiness<PhysicalExamination>, IPhysicalExaminationBusiness
    {
        IPhysicalExaminationRepository PhysicalExaminationRepository;
        IDevelopmentStandardRepository DevelopmentStandardRepository;
        IProcessedReportParameterRepository ProcessedReportParameterRepository;
        IProcessedReportRepository ProcessedReportRepository;
        public PhysicalExaminationBusiness(ILog logger, SMASEntities context = null)
            : base(logger)
        {
            if (context == null) { context = new SMASEntities(); } this.context = context;
            this.PhysicalExaminationRepository = new PhysicalExaminationRepository(context);
            repository = PhysicalExaminationRepository;
            this.DevelopmentStandardRepository = new DevelopmentStandardRepository(context);
            this.ProcessedReportRepository = new ProcessedReportRepository(context);
            this.ProcessedReportParameterRepository = new ProcessedReportParameterRepository(context);
        }   
    }
}
