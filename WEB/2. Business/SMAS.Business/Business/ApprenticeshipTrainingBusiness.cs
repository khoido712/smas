/**
 * The Viettel License
 *
 * Copyright 2012 Viettel Telecom. All rights reserved.
 * VIETTEL PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */

/** 
* @author  
* @version $Revision: $
*/

using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Linq.Expressions;
using System.Web;
using log4net;

using SMAS.DAL.Repository;
using SMAS.DAL.IRepository;
using SMAS.Business.IBusiness;
using SMAS.Models.Models;

namespace SMAS.Business.Business
{
    public partial class ApprenticeshipTrainingBusiness : GenericBussiness<ApprenticeshipTraining>, IApprenticeshipTrainingBusiness
    {
        IApprenticeshipTrainingRepository ApprenticeshipTrainingRepository;
        IClassProfileRepository ClassProfileRepository;
        ISchoolProfileRepository SchoolProfileRepository;
        IAcademicYearRepository AcademicYearRepository;
        IPupilProfileRepository PupilProfileRepository;
        IEducationLevelRepository EducationLevelRepository;
        IPupilOfClassRepository PupilOfClassRepository;
        ISummedUpRecordRepository SummedUpRecordRepository;
        IApprenticeshipClassRepository ApprenticeshipClassRepository;
        IEmployeeRepository EmployeeRepository;

        public ApprenticeshipTrainingBusiness(ILog logger, SMASEntities context = null)
            : base(logger)
        {

            if (context == null) { context = new SMASEntities(); } this.context = context;

            this.ApprenticeshipTrainingRepository = new ApprenticeshipTrainingRepository(context);
            this.ClassProfileRepository = new ClassProfileRepository(context);
            this.SchoolProfileRepository = new SchoolProfileRepository(context);
            this.AcademicYearRepository = new AcademicYearRepository(context);
            this.PupilProfileRepository = new PupilProfileRepository(context);
            this.EducationLevelRepository = new EducationLevelRepository(context);
            this.PupilOfClassRepository = new PupilOfClassRepository(context);
            this.SummedUpRecordRepository = new SummedUpRecordRepository(context);
            this.ApprenticeshipClassRepository = new ApprenticeshipClassRepository(context);
            this.EmployeeRepository = new EmployeeRepository(context);
            repository = ApprenticeshipTrainingRepository;


        }

    }
}