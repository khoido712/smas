
/**
 * The Viettel License
 *
 * Copyright 2012 Viettel Telecom. All rights reserved.
 * VIETTEL PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */

/** 
* @author  
* @version $Revision: $
*/

using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Linq.Expressions;
using System.Web;
using log4net;

using SMAS.DAL.Repository;
using SMAS.DAL.IRepository;
using SMAS.Business.IBusiness;
using SMAS.Models.Models;

namespace SMAS.Business.Business
{ 
    public partial class MonitoringBookBusiness : GenericBussiness<MonitoringBook>, IMonitoringBookBusiness
    {
		IMonitoringBookRepository  MonitoringBookRepository;
        IPupilOfClassRepository PupilOfClassRepository;
        IOverallTestRepository OverallTestRepository;
        IClassProfileRepository ClassProfileRepository;
        IPupilProfileRepository PupilProfileRepository;
        public MonitoringBookBusiness(ILog logger, SMASEntities context = null)
            : base(logger)
        {
            if (context == null) { context = new SMASEntities(); } this.context = context;
			this.MonitoringBookRepository = new MonitoringBookRepository(context);
			repository = MonitoringBookRepository;
            this.PupilOfClassRepository = new PupilOfClassRepository(context);
            this.OverallTestRepository = new OverallTestRepository(context);
            this.ClassProfileRepository = new ClassProfileRepository(context);
            this.PupilProfileRepository = new PupilProfileRepository(context);
        }
    }
}

