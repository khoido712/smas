/**
 * The Viettel License
 *
 * Copyright 2012 Viettel Telecom. All rights reserved.
 * VIETTEL PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */

/** 
* @author  
* @version $Revision: $
*/

using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Linq.Expressions;
using System.Web;
using log4net;

using SMAS.DAL.Repository;
using SMAS.DAL.IRepository;
using SMAS.Business.IBusiness;
using SMAS.Models.Models;

namespace SMAS.Business.Business
{ 
    public partial class ExamInputMarkBusiness : GenericBussiness<ExamInputMark>, IExamInputMarkBusiness
    {  
		IExamInputMarkRepository  ExamInputMarkRepository;
        ISubjectCatRepository SubjectCatRepository;
        IPupilProfileRepository PupilProfileRepository;
        IClassProfileRepository ClassProfileRepository;
        IEmployeeRepository EmployeeRepository;
        IExamRoomRepository ExamRoomRepository;
        IExamPupilRepository ExamPupilRepository;
        IPupilOfClassRepository PupilOfClassRepository;
        IExamPupilViolateRepository ExamPupilViolateRepository;
        IExamViolationTypeRepository ExamViolationTypeRepository;
        IProcessedReportParameterRepository ProcessedReportParameterRepository;
        IProcessedReportRepository ProcessedReportRepository;
        IExamCandenceBagRepository ExamCandenceBagRepository;
        IExamDetachableBagRepository ExamDetachableBagRepository;
        IExamGroupRepository ExamGroupRepository;
        IExamSubjectRepository ExamSubjectRepository;
        IExaminationsRepository ExaminationsRepository;
        IExamInputMarkAssignedRepository ExamInputMarkAssignedRepository;
        ISchoolSubjectRepository SchoolSubjectRepository;

		public ExamInputMarkBusiness(ILog logger, SMASEntities context = null) : base(logger)
        {
			if (context == null) { context = new SMASEntities(); } this.context = context;

			this.ExamInputMarkRepository = new ExamInputMarkRepository(context);
            this.SubjectCatRepository = new SubjectCatRepository(context);
            this.PupilProfileRepository = new PupilProfileRepository(context);
            this.ClassProfileRepository = new ClassProfileRepository(context);
            this.EmployeeRepository = new EmployeeRepository(context);
            this.ExamRoomRepository = new ExamRoomRepository(context);
            this.ExamPupilRepository = new ExamPupilRepository(context);
            this.PupilOfClassRepository = new PupilOfClassRepository(context);
            this.ExamPupilViolateRepository = new ExamPupilViolateRepository(context);
            this.ExamViolationTypeRepository = new ExamViolationTypeRepository(context);
            this.ProcessedReportParameterRepository = new ProcessedReportParameterRepository(context);
            this.ProcessedReportRepository = new ProcessedReportRepository(context);
            this.ExamCandenceBagRepository = new ExamCandenceBagRepository(context);
            this.ExamDetachableBagRepository = new ExamDetachableBagRepository(context);
            this.ExamGroupRepository = new ExamGroupRepository(context);
            this.ExamSubjectRepository = new ExamSubjectRepository(context);
            this.ExamInputMarkAssignedRepository = new ExamInputMarkAssignedRepository(context);
            this.ExaminationsRepository = new ExaminationsRepository(context);
            this.SchoolSubjectRepository = new SchoolSubjectRepository(context);

			repository = ExamInputMarkRepository;
        }
    }
}