/**
 * The Viettel License
 *
 * Copyright 2012 Viettel Telecom. All rights reserved.
 * VIETTEL PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */

/** 
* @author  
* @version $Revision: $
*/

using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Linq.Expressions;
using System.Web;
using log4net;

using SMAS.DAL.Repository;
using SMAS.DAL.IRepository;
using SMAS.Business.IBusiness;
using SMAS.Models.Models;

namespace SMAS.Business.Business
{ 
    public partial class JudgeRecordBusiness : GenericBussiness<JudgeRecord>, IJudgeRecordBusiness
    {  
		IJudgeRecordRepository  JudgeRecordRepository;
        IMarkRecordRepository MarkRecordRepository;
        IPupilProfileRepository PupilProfileRepository;
        IMarkTypeRepository MarkTypeRepository;
        IPupilOfClassRepository PupilOfClassRepository;
        ISummedUpRecordRepository SummedUpRecordRepository;        
        IClassProfileRepository ClassProfileRepository;
        IProcessedReportParameterRepository ProcessedReportParameterRepository;
        IProcessedReportRepository ProcessedReportRepository;
		public JudgeRecordBusiness(ILog logger, SMASEntities context = null) : base(logger)
        {
			if (context == null){context = new SMASEntities();}this.context = context;
			this.JudgeRecordRepository = new JudgeRecordRepository(context);
            MarkRecordRepository = new MarkRecordRepository(context);
            PupilProfileRepository = new PupilProfileRepository(context);
            MarkTypeRepository = new MarkTypeRepository(context);
            PupilOfClassRepository = new PupilOfClassRepository(context);
            SummedUpRecordRepository = new SummedUpRecordRepository(context);
            this.ClassProfileRepository = new ClassProfileRepository(context);
            this.ProcessedReportParameterRepository = new ProcessedReportParameterRepository(context);
            this.ProcessedReportRepository = new ProcessedReportRepository(context);
			repository = JudgeRecordRepository;
        }

        public override JudgeRecord Insert(JudgeRecord JudgeRecord)
        {
            base.Insert(JudgeRecord);
            return JudgeRecord;
        }
    }
}