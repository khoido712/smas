/**
 * The Viettel License
 *
 * Copyright 2012 Viettel Telecom. All rights reserved.
 * VIETTEL PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */

/** 
* @author  
* @version $Revision: $
*/

using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Linq.Expressions;
using System.Web;
using log4net;

using SMAS.DAL.Repository;
using SMAS.DAL.IRepository;
using SMAS.Business.IBusiness;
using SMAS.Models.Models;

namespace SMAS.Business.Business
{ 
    public partial class ExamPupilViolateBusiness : GenericBussiness<ExamPupilViolate>, IExamPupilViolateBusiness
    {  
		IExamPupilViolateRepository  ExamPupilViolateRepository;
        IExamPupilRepository ExamPupilRepository;
        IExamGroupRepository ExamGroupRepository;
        IPupilProfileRepository PupilProfileRepository;
        IExamViolationTypeRepository ExamViolationTypeRepository;
        IExamRoomRepository ExamRoomRepository;
        ISubjectCatRepository SubjectCatRepository;
        IPupilOfClassRepository PupilOfClassRepository;
        IExamBagRepository ExamBagRepository;
        IProcessedReportParameterRepository ProcessedReportParameterRepository;
        IProcessedReportRepository ProcessedReportRepository;

		public ExamPupilViolateBusiness(ILog logger, SMASEntities context = null) : base(logger)
        {
			if (context == null) { context = new SMASEntities(); } this.context = context;

			this.ExamPupilViolateRepository = new ExamPupilViolateRepository(context);
            this.ExamPupilRepository = new ExamPupilRepository(context);
            this.PupilProfileRepository = new PupilProfileRepository(context);
            this.ExamViolationTypeRepository = new ExamViolationTypeRepository(context);
            this.ExamRoomRepository = new ExamRoomRepository(context);
            this.SubjectCatRepository = new SubjectCatRepository(context);
            this.PupilOfClassRepository = new PupilOfClassRepository(context);
            this.ExamBagRepository = new ExamBagRepository(context);
            this.ProcessedReportParameterRepository = new ProcessedReportParameterRepository(context);
            this.ProcessedReportRepository = new ProcessedReportRepository(context);
            this.ExamGroupRepository = new ExamGroupRepository(context);

			repository = ExamPupilViolateRepository;
        }
        
    }
}