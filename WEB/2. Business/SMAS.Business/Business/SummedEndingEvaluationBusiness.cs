﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Linq.Expressions;
using System.Web;
using log4net;

using SMAS.DAL.Repository;
using SMAS.DAL.IRepository;
using SMAS.Business.IBusiness;
using SMAS.Models.Models;

namespace SMAS.Business.Business
{
    public partial class SummedEndingEvaluationBusiness : GenericBussiness<SummedEndingEvaluation>, ISummedEndingEvaluationBusiness
    {
        IProcessedReportParameterRepository ProcessedReportParameterRepository;
        IProcessedReportRepository ProcessedReportRepository;
        ISummedEndingEvaluationRepository SummedEndingEvaluationRepository;
        IPupilProfileRepository PupilProfileRepository;
        IClassProfileRepository ClassProfileRepository;
        IClassSubjectRepository ClassSubjectRepository;
        ISchoolProfileRepository SchoolProfileRepository;

        public SummedEndingEvaluationBusiness(ILog logger, SMASEntities context = null)
            : base(logger)
        {
            if (context == null) { context = new SMASEntities(); } this.context = context;
            this.SummedEndingEvaluationRepository = new SummedEndingEvaluationRepository  (context);
            this.PupilProfileRepository = new PupilProfileRepository(context);
            this.ClassProfileRepository = new ClassProfileRepository(context);
            this.ClassSubjectRepository = new ClassSubjectRepository(context);
            this.SchoolProfileRepository = new SchoolProfileRepository(context);
            this.ProcessedReportParameterRepository = new ProcessedReportParameterRepository(context);
            this.ProcessedReportRepository = new ProcessedReportRepository(context);
            repository = SummedEndingEvaluationRepository;            
        }
    }
}
