﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SMAS.Business.IBusiness;
using SMAS.Business.BusinessObject;
using log4net;
using SMAS.Models.Models;
using SMAS.DAL.IRepository;
using SMAS.DAL.Repository;

namespace SMAS.Business.Business
{
    public partial class StatisticsForUnitBusiness : GenericBussiness<StatisticsForUnitBO>, IStatisticsForUnitBusiness
    {
        IProcessedReportRepository ProcessedReportRepository;
        IProcessedReportParameterRepository ProcessedReportParameterRepository;
       
        public StatisticsForUnitBusiness(ILog logger, SMASEntities context = null)
            : base(logger)
        {
            if (context == null) context = new SMASEntities();
            this.context = context;
            this.ProcessedReportParameterRepository = new ProcessedReportParameterRepository(context);
            this.ProcessedReportRepository = new ProcessedReportRepository(context);
        }
    }
}