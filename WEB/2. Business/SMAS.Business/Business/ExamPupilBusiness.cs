/**
 * The Viettel License
 *
 * Copyright 2012 Viettel Telecom. All rights reserved.
 * VIETTEL PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */

/** 
* @author  
* @version $Revision: $
*/

using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Linq.Expressions;
using System.Web;
using log4net;

using SMAS.DAL.Repository;
using SMAS.DAL.IRepository;
using SMAS.Business.IBusiness;
using SMAS.Models.Models;

namespace SMAS.Business.Business
{ 
    public partial class ExamPupilBusiness : GenericBussiness<ExamPupil>, IExamPupilBusiness
    {  
		IExamPupilRepository  ExamPupilRepository;
        IExamGroupRepository ExamGroupRepository;
        IPupilProfileRepository PupilProfileRepository;
        IPupilOfClassRepository PupilOfClassRepository;
        IClassProfileRepository ClassProfileRepository;
        IExamInputMarkRepository ExamInputMarkRepository;
        IExamSubjectRepository ExamSubjectRepository;
        IExamRoomRepository ExamRoomRepository;
        IExamPupilAbsenceRepository ExamPupilAbsenceRepository;
        IProcessedReportParameterRepository ProcessedReportParameterRepository;
        IProcessedReportRepository ProcessedReportRepository;
        IEducationLevelRepository EducationLevelRepository;
        IEthnicRepository EthnicRepository;

        public ExamPupilBusiness(ILog logger, SMASEntities context = null) : base(logger)
        {
			if (context == null) { context = new SMASEntities(); } this.context = context;

			this.ExamPupilRepository = new ExamPupilRepository(context);
			repository = ExamPupilRepository;
            this.ExamGroupRepository = new ExamGroupRepository(context);
            this.PupilProfileRepository = new PupilProfileRepository(context);
            this.PupilOfClassRepository = new PupilOfClassRepository(context);
            this.ClassProfileRepository = new ClassProfileRepository(context);
            this.ExamInputMarkRepository = new ExamInputMarkRepository(context);
            this.ExamSubjectRepository = new ExamSubjectRepository(context);
            this.ExamRoomRepository = new ExamRoomRepository(context);
            this.ExamPupilAbsenceRepository = new ExamPupilAbsenceRepository(context);
            this.ProcessedReportParameterRepository = new ProcessedReportParameterRepository(context);
            this.ProcessedReportRepository = new ProcessedReportRepository(context);
            this.EducationLevelRepository = new EducationLevelRepository(context);
            this.EthnicRepository = new EthnicRepository(context);
        }
        
    }
}