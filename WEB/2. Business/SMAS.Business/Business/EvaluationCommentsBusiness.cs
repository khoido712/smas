﻿using System.Data.Entity;
using System.Linq;
using System.Linq.Expressions;
using System.Web;
using log4net;

using SMAS.DAL.Repository;
using SMAS.DAL.IRepository;
using SMAS.Business.IBusiness;
using SMAS.Models.Models;

namespace SMAS.Business.Business
{
    public partial class EvaluationCommentsBusiness : GenericBussiness<EvaluationComments>, IEvaluationCommentsBusiness
    {
        IEvaluationCommentsRepository EvaluationCommentsRepository;
        IProcessedReportParameterRepository ProcessedReportParameterRepository;
        IProcessedReportRepository ProcessedReportRepository;

        public EvaluationCommentsBusiness(ILog logger, SMASEntities context = null)
            : base(logger)
        {
            if (context == null) { context = new SMASEntities(); } this.context = context;
            this.EvaluationCommentsRepository = new EvaluationCommentsRepository(context);
            repository = EvaluationCommentsRepository;

            ProcessedReportRepository = new ProcessedReportRepository(context);
            ProcessedReportParameterRepository = new ProcessedReportParameterRepository(context);
        }
    }
}
