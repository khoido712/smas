﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Linq.Expressions;
using System.Web;
using log4net;

using SMAS.DAL.Repository;
using SMAS.DAL.IRepository;
using SMAS.Business.IBusiness;
using SMAS.Models.Models;

namespace SMAS.Business.Business
{
    public partial class BookmarkedFunctionBusiness : GenericBussiness<BookmarkedFunction>, IBookmarkedFunctionBusiness
    {
        
        IBookmarkedFunctionRepository  BookmarkedFunctionRepository;
        public BookmarkedFunctionBusiness(ILog logger, SMASEntities context = null)
            : base(logger)
        {
			if (context == null)
            {
                context = new SMASEntities();
            }

            this.context = context;

            this.BookmarkedFunctionRepository = new BookmarkedFunctionRepository(context);
            repository = BookmarkedFunctionRepository;
        }
    }
}
