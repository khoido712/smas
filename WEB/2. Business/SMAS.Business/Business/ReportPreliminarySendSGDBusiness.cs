﻿using log4net;
using SMAS.Business.IBusiness;
using SMAS.DAL.IRepository;
using SMAS.DAL.Repository;
using SMAS.Models.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SMAS.Business.Business
{
    public partial class ReportPreliminarySendSGDBusiness : GenericBussiness<AcademicYear>, IReportPreliminarySendSGDBusiness
    {
        IClassProfileRepository ClassProfileRepository;
        IClassSubjectRepository ClassSubjectRepository;
        IProcessedReportParameterRepository ProcessedReportParameterRepository;
        ITeachingAssignmentRepository TeachingAssignmentRepository;
        ISubjectCatRepository SubjectCatRepository;
        public ReportPreliminarySendSGDBusiness(ILog logger, SMASEntities context = null)
            : base(logger)
        {
            if (context == null) { context = new SMASEntities(); } this.context = context;
            this.ClassProfileRepository = new ClassProfileRepository(context);
            this.ClassSubjectRepository = new ClassSubjectRepository(context);
            this.ProcessedReportParameterRepository = new ProcessedReportParameterRepository(context);
            this.TeachingAssignmentRepository = new TeachingAssignmentRepository(context);
            this.SubjectCatRepository = new SubjectCatRepository(context);
        }
    }
}
