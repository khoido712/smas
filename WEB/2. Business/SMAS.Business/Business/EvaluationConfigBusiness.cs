/**
 * The Viettel License
 *
 * Copyright 2012 Viettel Telecom. All rights reserved.
 * VIETTEL PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */

/** 
* @author  
* @version $Revision: $
*/

using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Linq.Expressions;
using System.Web;
using log4net;

using SMAS.DAL.Repository;
using SMAS.DAL.IRepository;
using SMAS.Business.IBusiness;
using SMAS.Models.Models;

namespace SMAS.Business.Business
{ 
    public partial class EvaluationConfigBusiness : GenericBussiness<EvaluationConfig>, IEvaluationConfigBusiness
    {  
		IEvaluationConfigRepository  EvaluationConfigRepository;
        public EvaluationConfigBusiness(ILog logger, SMASEntities context = null)
            : base(logger)
        {
            if (context == null)
            {
                context = new SMASEntities();
            }

            this.context = context;

			this.EvaluationConfigRepository = new EvaluationConfigRepository(context);
			repository = EvaluationConfigRepository;
        }
        
    }
}