﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SMAS.Models.Models;
using log4net;
using SMAS.Business.IBusiness;
using SMAS.DAL.IRepository;
using SMAS.DAL.Repository;

namespace SMAS.Business.Business
{
    public partial class ParticularPupilCharacteristicBusiness : GenericBussiness<ParticularPupilCharacteristic>, IParticularPupilCharacteristicBusiness
    {
        IParticularPupilCharacteristicRepository ParticularPupilCharacteristic;

        public ParticularPupilCharacteristicBusiness(ILog logger, SMASEntities context = null)
            : base(logger)
        {
            if (context == null) { context = new SMASEntities(); } this.context = context;
            this.ParticularPupilCharacteristic = new ParticularPupilCharacteristicRepository(this.context);            
            repository = ParticularPupilCharacteristic;
        }

    }
}