﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Linq.Expressions;
using System.Web;
using log4net;

using SMAS.DAL.Repository;
using SMAS.DAL.IRepository;
using SMAS.Business.IBusiness;
using SMAS.Models.Models;

namespace SMAS.Business.Business
{
    public partial class ReportEmployeeProfileBusiness : GenericBussiness<ProcessedReport>, IReportEmployeeProfileBusiness
    {
       
        IProcessedReportParameterRepository ProcessedReportParameterRepository;
        IProcessedReportRepository ProcessedReportRepository;
        IEmployeeHistoryStatusRepository EmployeeHistoryStatusRepository;
        ITeacherOfFacultyRepository TeacherOfFacultyRepository;
        IEmployeeQualificationRepository EmployeeQualificationRepository;
        public ReportEmployeeProfileBusiness(ILog logger, SMASEntities context = null)
            : base(logger)
        {
            if (context == null) { context = new SMASEntities(); } this.context = context;
            this.ProcessedReportRepository = new ProcessedReportRepository(context);
            this.ProcessedReportParameterRepository = new ProcessedReportParameterRepository(context);
            this.EmployeeHistoryStatusRepository = new EmployeeHistoryStatusRepository(context);
            this.TeacherOfFacultyRepository = new TeacherOfFacultyRepository(context);
            this.EmployeeQualificationRepository = new EmployeeQualificationRepository(context);
        }
    }
}
