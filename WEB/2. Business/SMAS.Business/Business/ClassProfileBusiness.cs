/**
 * The Viettel License
 *
 * Copyright 2012 Viettel Telecom. All rights reserved.
 * VIETTEL PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */

/** 
* @author  
* @version $Revision: $
*/

using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Linq.Expressions;
using System.Web;
using log4net;
using SMAS.Business.IBusiness;
using SMAS.Models.Models;
using SMAS.DAL.IRepository;
using SMAS.DAL.Repository;

namespace SMAS.Business.Business
{
    public partial class ClassProfileBusiness : GenericBussiness<ClassProfile>, IClassProfileBusiness
    {
        // Repository
        IClassProfileRepository ClassProfileRepository;
        IClassSupervisorAssignmentRepository ClassSupervisorAssignmentRepository;
        ITeachingAssignmentRepository TeachingAssignmentRepository;
        IHeadTeacherSubstitutionRepository HeadTeacherSubstitutionRepository;
        IProcessedReportRepository ProcessedReportRepository;
        IProcessedReportParameterRepository ProcessedReportParameterRepository;
        ITrainingProgramRepository TrainingProgramRepository;
        IClassSpecialtyCatRepository ClassSpecialtyCatRepository;
        ISchoolProfileRepository SchoolProfileRepository;
             
        public ClassProfileBusiness(ILog logger, SMASEntities context = null)
            : base(logger)
        {

            if (context == null) { context = new SMASEntities(); } this.context = context;

            this.ClassProfileRepository = new ClassProfileRepository(context);
            repository = ClassProfileRepository;
            this.ClassSupervisorAssignmentRepository = new ClassSupervisorAssignmentRepository(context);
            this.TeachingAssignmentRepository = new TeachingAssignmentRepository(context);
            this.HeadTeacherSubstitutionRepository = new HeadTeacherSubstitutionRepository(context);
            this.ProcessedReportRepository = new ProcessedReportRepository(context);
            this.ProcessedReportParameterRepository = new ProcessedReportParameterRepository(context);
            this.TrainingProgramRepository = new TrainingProgramRepository(context);
            this.ClassSpecialtyCatRepository = new ClassSpecialtyCatRepository(context);
            this.SchoolProfileRepository = new SchoolProfileRepository(context);
        }
    }
}