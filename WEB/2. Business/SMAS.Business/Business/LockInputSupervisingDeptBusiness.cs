﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Linq.Expressions;
using System.Web;
using log4net;

using SMAS.DAL.Repository;
using SMAS.DAL.IRepository;
using SMAS.Business.IBusiness;
using SMAS.Models.Models;

namespace SMAS.Business.Business
{
    public partial class LockInputSupervisingDeptBusiness : GenericBussiness<LockInputSupervisingDept>, ILockInputSupervisingDeptBusiness
    {
        ILockInputSupervisingDeptRepository LockInputSupervisingDeptRepository;     
        ISchoolProfileRepository SchoolProfileRepository;
        public LockInputSupervisingDeptBusiness(ILog logger, SMASEntities context = null)
            : base(logger)
        {
            if (context == null) { context = new SMASEntities(); } this.context = context;
            this.LockInputSupervisingDeptRepository = new LockInputSupervisingDeptRepository(context);
            repository = LockInputSupervisingDeptRepository;
            this.SchoolProfileRepository = new SchoolProfileRepository(context);
        }
    }
}
