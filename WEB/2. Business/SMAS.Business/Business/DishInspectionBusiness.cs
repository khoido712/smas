/**
 * The Viettel License
 *
 * Copyright 2012 Viettel Telecom. All rights reserved.
 * VIETTEL PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */

/** 
* @author  
* @version $Revision: $
*/

using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Linq.Expressions;
using System.Web;
using log4net;

using SMAS.DAL.Repository;
using SMAS.DAL.IRepository;
using SMAS.Business.IBusiness;
using SMAS.Models.Models;

namespace SMAS.Business.Business
{
    public partial class DishInspectionBusiness : GenericBussiness<DishInspection>, IDishInspectionBusiness
    {
        IDishInspectionRepository DishInspectionRepository;
        IDailyFoodInspectionRepository DailyFoodInspectionRepository;
        IEatingGroupRepository EatingGroupRepository;
        IFoodCatRepository FoodCatRepository;
        IDailyOtherServiceInspectionRepository DailyOtherServiceInspectionRepository;
        IOtherServiceRepository OtherServiceRepository;
        IDailyMenuRepository DailyMenuRepository;
        public DishInspectionBusiness(ILog logger, SMASEntities context = null)
            : base(logger)
        {
            if (context == null)
            {
                context = new SMASEntities();
            }
            this.context = context;
            this.DishInspectionRepository = new DishInspectionRepository(context);
            repository = DishInspectionRepository;
            // Cac repository khac
            DailyFoodInspectionRepository = new DailyFoodInspectionRepository(context);
            EatingGroupRepository = new EatingGroupRepository(context);
            FoodCatRepository = new FoodCatRepository(context);
            DailyOtherServiceInspectionRepository = new DailyOtherServiceInspectionRepository(context);
            OtherServiceRepository = new OtherServiceRepository(context);
            DailyMenuRepository = new DailyMenuRepository(context);
        }

    }
}