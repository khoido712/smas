/**
 * The Viettel License
 *
 * Copyright 2012 Viettel Telecom. All rights reserved.
 * VIETTEL PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */

/** 
* @author  
* @version $Revision: $
*/

using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Linq.Expressions;
using System.Web;
using log4net;

using SMAS.DAL.Repository;
using SMAS.DAL.IRepository;
using SMAS.Business.IBusiness;
using SMAS.Models.Models;

namespace SMAS.Business.Business
{ 
    public partial class ConductStatisticBusiness : GenericBussiness<ConductStatistic>, IConductStatisticBusiness
    {  
        ISchoolProfileRepository SchoolProfileRepository;
        IDistrictRepository DistrictRepository;
        IProcessedReportParameterRepository ProcessedReportParameterRepository;
        IProcessedReportRepository ProcessedReportRepository;
        ISupervisingDeptRepository SupervisingDeptRepository;
        IConductStatisticRepository ConductStatisticRepository;
        IProvinceRepository ProvinceRepository;

        public ConductStatisticBusiness(ILog logger, SMASEntities context = null)
            : base(logger)
        {
            if (context == null) { context = new SMASEntities(); } this.context = context;
            this.SchoolProfileRepository = new SchoolProfileRepository(context);
            this.DistrictRepository = new DistrictRepository(context);
            this.ProcessedReportParameterRepository = new ProcessedReportParameterRepository(context);
            this.ProcessedReportRepository = new ProcessedReportRepository(context);
            this.SupervisingDeptRepository = new SupervisingDeptRepository(context);
            this.ConductStatisticRepository = new ConductStatisticRepository(context);
            this.ProvinceRepository = new ProvinceRepository(context);
            repository = ConductStatisticRepository;
        }
        
    }
}