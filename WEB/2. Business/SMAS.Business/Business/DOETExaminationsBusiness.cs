﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SMAS.Models.Models;
using SMAS.Business.IBusiness;
using log4net;
using SMAS.DAL.Repository;
using SMAS.DAL.IRepository;

namespace SMAS.Business.Business
{
    public partial class DOETExaminationsBusiness : GenericBussiness<DOETExaminations>, IDOETExaminationsBusiness
    {
        IDOETExaminationsRepository DOETExaminationsRepository;

        public DOETExaminationsBusiness(ILog logger, SMASEntities context = null)
            : base(logger)
        {
            if (context == null) { context = new SMASEntities(); } this.context = context;
            this.DOETExaminationsRepository = new DOETExaminationsRepository(context);
            repository = DOETExaminationsRepository;
            
        }
    }
}
