﻿using log4net;
using SMAS.Business.IBusiness;
using SMAS.DAL.IRepository;
using SMAS.DAL.Repository;
using SMAS.Models.Models;

namespace SMAS.Business.Business
{
    public partial class DOETExamSupervisoryBusiness : GenericBussiness<DOETExamSupervisory>, IDOETExamSupervisoryBusiness
    {
        IDOETExamSupervisoryRepository DOETExamSupervisoryRepository;

        public DOETExamSupervisoryBusiness(ILog logger, SMASEntities context = null)
            : base(logger)
        {
            if (context == null) { context = new SMASEntities(); } this.context = context;
            this.DOETExamSupervisoryRepository = new DOETExamSupervisoryRepository(context);
            repository = DOETExamSupervisoryRepository;

        }
    }
}
