/**
 * The Viettel License
 *
 * Copyright 2012 Viettel Telecom. All rights reserved.
 * VIETTEL PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */

/** 
* @author  
* @version $Revision: $
*/

using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Linq.Expressions;
using System.Web;
using log4net;

using SMAS.DAL.Repository;
using SMAS.DAL.IRepository;
using SMAS.Business.IBusiness;
using SMAS.Models.Models;

namespace SMAS.Business.Business
{ 
    public partial class ExamInputMarkAssignedBusiness : GenericBussiness<ExamInputMarkAssigned>, IExamInputMarkAssignedBusiness
    {  
		IExamInputMarkAssignedRepository  ExamInputMarkAssignedRepository;
        ISubjectCatRepository SubjectCatRepository;
        IEmployeeRepository EmployeeRepository;
        ISchoolFacultyRepository SchoolFacultyRepository;
        IEthnicRepository EthnicRepository;

		public ExamInputMarkAssignedBusiness(ILog logger, SMASEntities context = null) : base(logger)
        {
			if (context == null) { context = new SMASEntities(); } this.context = context;

			this.ExamInputMarkAssignedRepository = new ExamInputMarkAssignedRepository(context);
            this.SubjectCatRepository = new SubjectCatRepository(context);
            this.SchoolFacultyRepository = new SchoolFacultyRepository(context);
            this.EmployeeRepository = new EmployeeRepository(context);
            this.EthnicRepository = new EthnicRepository(context);
			repository = ExamInputMarkAssignedRepository;
        }
        
    }
}