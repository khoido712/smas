/**
 * The Viettel License
 *
 * Copyright 2012 Viettel Telecom. All rights reserved.
 * VIETTEL PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */

/** 
* @author  
* @version $Revision: $
*/

using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Linq.Expressions;
using System.Web;
using log4net;

using SMAS.DAL.Repository;
using SMAS.DAL.IRepository;
using SMAS.Business.IBusiness;
using SMAS.Models.Models;

namespace SMAS.Business.Business
{
    public partial class CalendarBusiness : GenericBussiness<Calendar>, ICalendarBusiness
    {
        ICalendarRepository CalendarRepository;
        ISubjectCatRepository SubjectCatRepository;
        IClassProfileRepository ClassProfileRepository;
        ITeachingAssignmentRepository TeachingAssignmentRepository;
        IClassSupervisorAssignmentRepository ClassSupervisorAssignmentReposiory;
        IHeadTeacherSubstitutionRepository HeadTeacherSubstitutionRepository;
        
        public CalendarBusiness(ILog logger, SMASEntities context = null)
            : base(logger)
        {

            if (context == null) { context = new SMASEntities(); } this.context = context;

            this.context = context;

            this.CalendarRepository = new CalendarRepository(context);
            this.SubjectCatRepository = new SubjectCatRepository(context);
            this.ClassProfileRepository = new ClassProfileRepository(context);
            this.TeachingAssignmentRepository = new TeachingAssignmentRepository(context);
            this.ClassSupervisorAssignmentReposiory = new ClassSupervisorAssignmentRepository(context);
            this.HeadTeacherSubstitutionRepository = new HeadTeacherSubstitutionRepository(context);
                        
            repository = CalendarRepository;
        }

    }
}