﻿/**
 * The Viettel License
 *
 * Copyright 2012 Viettel Telecom. All rights reserved.
 * VIETTEL PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */

/** 
* @author  
* @version $Revision: $
*/
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SMAS.Models.Models;
using log4net;
using SMAS.DAL.Repository;
using SMAS.DAL.IRepository;
using SMAS.Business.IBusiness;
using SMAS.Business.BusinessObject;
namespace SMAS.Business.Business
{
    public partial class TranscriptsBusiness : GenericBussiness<Transcripts>, ITranscriptsBusiness
    {
        IProcessedReportRepository ProcessedReportRepository;
        IProcessedReportParameterRepository ProcessedReportParameterRepository;
        public TranscriptsBusiness(ILog logger, SMASEntities context = null)
            : base(logger)
        {
            if (context == null) { context = new SMASEntities(); } this.context = context;
            ProcessedReportRepository = new ProcessedReportRepository(context);
            ProcessedReportParameterRepository = new ProcessedReportParameterRepository(context);
        }
    }
}
