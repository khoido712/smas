﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using log4net;
using System.Threading.Tasks;
using SMAS.DAL.Repository;
using SMAS.DAL.IRepository;
using SMAS.Business.IBusiness;
using SMAS.Models.Models;

namespace SMAS.Business.Business
{
    public partial class ThreadMovedDataBusiness : GenericBussiness<ThreadMovedData>, IThreadMovedDataBusiness
    {
        IThreadMovedDataRepository ThreadMovedDataRepository;
        public ThreadMovedDataBusiness(ILog logger, SMASEntities context = null)
            : base(logger)
        {
            if (context == null) { context = new SMASEntities(); } this.context = context;
            this.ThreadMovedDataRepository = new ThreadMovedDataRepository(context);
            repository = ThreadMovedDataRepository;
           
        }
    }
}
