﻿
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Linq.Expressions;
using System.Web;
using log4net;

using SMAS.DAL.Repository;
using SMAS.DAL.IRepository;
using SMAS.Business.IBusiness;
using SMAS.Models.Models;
using SMAS.Business.Common;
using SMAS.Business.BusinessObject;
using System.Transactions;

namespace SMAS.Business.Business
{
    public partial class BookmarkedFunctionBusiness
    {
        public IQueryable<BookmarkedFunction> Search(IDictionary<string, object> dic)
        {
            string functionName = Utils.GetString(dic, "FunctionName");
            int schoolID = Utils.GetInt(dic, "SchoolID");
            int levelID = Utils.GetInt(dic, "LevelID");
            int userID = Utils.GetInt(dic, "UserID");
            int menuID = Utils.GetInt(dic, "MenuID");
            IQueryable<BookmarkedFunction> iq = this.BookmarkedFunctionRepository.All;
            if (functionName != null && functionName.Length > 0)
            {
                iq = iq.Where(o => o.FunctionName.ToLower().Contains(functionName.ToLower()));
            }
            if (schoolID > 0)
            {
                iq = iq.Where(o => o.SchoolID == schoolID);
            }
            if (levelID > 0)
            {
                iq = iq.Where(o => o.LevelID == levelID);
            }
            if (userID > 0)
            {
                iq = iq.Where(o => o.UserID == userID);
            }
            if (menuID > 0)
            {
                iq = iq.Where(o => o.MenuID == menuID);
            }
            return iq;
        }

        public List<BookmarkedFunction> GetBookmarkedFunctionOfUser(int schoolId, int academicYearId, int appliedLevel, int userId)
        {
            IQueryable<BookmarkedFunction> q = this.All.Where(o => o.UserID == userId);
            if (schoolId > 0)
            {
                q = q.Where(o => o.SchoolID == schoolId);
            }
            if (academicYearId > 0)
            {
                q = q.Where(o => o.AcademicYearID == academicYearId);
            }
            if (appliedLevel > 0)
            {
                q = q.Where(o => o.LevelID == appliedLevel);
            }
            return q.ToList();
        }
        public void Insert(BookmarkedFunction obj)
        {
            ValidationMetadata.ValidateObject(obj);
            base.Insert(obj);
        }
        public void Update(BookmarkedFunction obj)
        {
            ValidationMetadata.ValidateObject(obj);
            base.Update(obj);
        }
        public void Delete(int id)
        {
            base.Delete(id, true);
        }
    }
}
