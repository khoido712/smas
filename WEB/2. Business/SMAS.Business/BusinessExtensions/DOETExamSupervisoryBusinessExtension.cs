﻿/**
 * The Viettel License
 *
 * Copyright 2012 Viettel Telecom. All rights reserved.
 * VIETTEL PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */

/** 
* @author  
* @version $Revision: $
*/

using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Linq.Expressions;
using System.Web;
using log4net;
using SMAS.DAL.Repository;
using SMAS.DAL.IRepository;
using SMAS.Business.IBusiness;
using SMAS.Models.Models;
using SMAS.Business.Common;
using SMAS.Business.BusinessObject;
using System.Data.Entity.Infrastructure;
using System.Data.Objects;
using System.IO;
using SMAS.VTUtils.Excel.Export;
using SMAS.VTUtils.Log;

namespace SMAS.Business.Business
{
    public partial class DOETExamSupervisoryBusiness
    {

        /// <summary>
        /// Lay danh sach can bo phan cong giam thi
        /// </summary>
        /// <param name="examinationId"></param>
        /// <param name="schoolId"></param>
        /// <param name="academicYearId"></param>
        /// <returns></returns>
        public List<DOETExamSupervisoryBO> GetExamEmpoyee(int examinationId, int schoolId, int academicYearId, int employeeType)
        {
            IQueryable<DOETExamSupervisoryBO> queryable = from doetsb in DOETExamSupervisoryBusiness.All.Where(s => s.ExaminationsID == examinationId && s.SchoolID == schoolId && s.AcademicYearID == academicYearId)
                                                          join emp in EmployeeBusiness.All.Where(s => s.SchoolID == schoolId && s.IsActive) on doetsb.TeacherID equals emp.EmployeeID
                                                          join tr in TrainingLevelBusiness.All.Where(s => s.IsActive) on emp.TrainingLevelID equals tr.TrainingLevelID into empts
                                                          from et in empts.DefaultIfEmpty()
                                                          join dt in EthnicBusiness.All.Where(s => s.IsActive) on emp.EthnicID equals dt.EthnicID into ed
                                                          from dantoc in ed.DefaultIfEmpty()
                                                          select new DOETExamSupervisoryBO
                                                          {
                                                              AcademicYearID = doetsb.AcademicYearID,
                                                              CreateTime = doetsb.CreateTime,
                                                              ExamGroupID = doetsb.ExamGroupID,
                                                              ExaminationsID = doetsb.ExaminationsID,
                                                              ExamSupervisoryID = doetsb.ExamSupervisoryID,
                                                              FullName = emp.FullName,
                                                              MSourcedb = doetsb.MSourcedb,
                                                              SchoolID = doetsb.SchoolID,
                                                              EthnicId = emp.EthnicID,
                                                              SupervisoryType = doetsb.SupervisoryType,
                                                              SyncSourceId = doetsb.SyncSourceId,
                                                              SyncTime = doetsb.SyncTime,
                                                              TeacherID = doetsb.TeacherID,
                                                              TrainingLevelID = emp.TrainingLevelID,
                                                              UpdateTime = doetsb.UpdateTime,
                                                              EmployeeCode = emp.EmployeeCode,
                                                              TrainingLevelName = et.Resolution,
                                                              //ExamEmployeeStatus = doetsb.SyncTime.HasValue ? "Đã gửi" : "",
                                                              Genre = emp.Genre,
                                                              EthnicName = dantoc.EthnicName,
                                                              BirthDay = emp.BirthDate
                                                          };
            if (employeeType > 0)
            {
                queryable = queryable.Where(s => s.SupervisoryType == employeeType);
            }
            List<DOETExamSupervisoryBO> lstVal = queryable.OrderBy(s => s.EmployeeCode).ToList();
            return lstVal;

        }

        public List<DOETExamSupervisoryBO> GetEmployeeNotRegis(int examinationId, int employeeType, int schoolId, int academicYearId, List<int> lstEmployeeRegisId)
        {
            //List<DOETExamSupervisory> lstEmpRegis = DOETExamSupervisoryBusiness.All.Where(s => s.ExaminationsID == examinationId && s.SupervisoryType == employeeType && s.AcademicYearID == academicYearId && s.SchoolID == schoolId).ToList();

            IQueryable<DOETExamSupervisoryBO> queryable = from emp in EmployeeBusiness.All.Where(s => s.SchoolID == schoolId && s.IsActive && s.EmploymentStatus == GlobalConstants.EMPLOYMENT_STATUS_WORKING)
                                                          join tr in TrainingLevelBusiness.All.Where(s => s.IsActive) on emp.TrainingLevelID equals tr.TrainingLevelID into empts
                                                          from et in empts.DefaultIfEmpty()
                                                          join dt in EthnicBusiness.All.Where(s => s.IsActive) on emp.EthnicID equals dt.EthnicID into ed
                                                          from dantoc in ed.DefaultIfEmpty()
                                                          select new DOETExamSupervisoryBO
                                                          {
                                                              FullName = emp.FullName,
                                                              EthnicId = emp.EthnicID,
                                                              TrainingLevelID = emp.TrainingLevelID,
                                                              TeacherID = emp.EmployeeID,
                                                              EmployeeCode = emp.EmployeeCode,
                                                              TrainingLevelName = et.Resolution,
                                                              Genre = emp.Genre,
                                                              EthnicName = dantoc.EthnicName,
                                                              BirthDay = emp.BirthDate,
                                                              Mobile = emp.Mobile
                                                          };
            List<DOETExamSupervisoryBO> lstEmployee = queryable.ToList();
            if (lstEmployeeRegisId != null && lstEmployeeRegisId.Count > 0)
            {
                lstEmployee = lstEmployee.Where(s => !lstEmployeeRegisId.Contains(s.TeacherID)).ToList();
            }
            return lstEmployee;


        }

    }
}