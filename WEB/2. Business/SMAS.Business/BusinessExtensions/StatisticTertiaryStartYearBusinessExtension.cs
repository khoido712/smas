﻿using SMAS.Business.Common;
using SMAS.Models.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SMAS.Business.Business
{
    /// <summary>
    /// 
    /// </summary>
    public partial class StatisticTertiaryStartYearBusiness
    {
        public void InsertStatisticTertiaryStartYear(StatisticTertiaryStartYear entity)
        {
            this.Insert(entity);
            this.Save();
        }
        public IQueryable<StatisticTertiaryStartYear> Search(IDictionary<string, object> dic)
        {
            int SchoolID = Utils.GetInt(dic, "SchoolID");
            int SupervisingDeptID = Utils.GetInt(dic, "SupervisingDeptID");
            bool? ReportStatus = Utils.GetNullableBool(dic, "ReportStatus");
            DateTime? ReportDate = Utils.GetDateTime(dic, "ReportDate");


            IQueryable<StatisticTertiaryStartYear> lst = StatisticTertiaryStartYearRepository.All;

            if (SchoolID != 0)
            {
                lst = lst.Where(o => o.SchoolID == SchoolID);
            }
            if (SupervisingDeptID != 0)
            {
                lst = lst.Where(o => o.SupervisingDeptID == SupervisingDeptID);
            }
            if (SupervisingDeptID != 0)
            {
                lst = lst.Where(o => o.SupervisingDeptID == SupervisingDeptID);
            }
            if (ReportStatus.HasValue)
            {
                lst = lst.Where(o => o.ReportStatus == ReportStatus);
            }
            if (ReportDate.HasValue)
            {
                lst = lst.Where(o => o.ReportDate == ReportDate);
            }
            return lst;

        }
    }
}
