﻿/**
 * The Viettel License
 *
 * Copyright 2012 Viettel Telecom. All rights reserved.
 * VIETTEL PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */

/** 
* @author  tungnd
* @version $Revision: $
*/

using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Linq.Expressions;
using System.Web;
using log4net;

using SMAS.DAL.Repository;
using SMAS.DAL.IRepository;
using SMAS.Business.IBusiness;
using SMAS.Models.Models;
using SMAS.Business.Common;

namespace SMAS.Business.Business
{
    public partial class PupilEmulationBusiness
    {
        #region InsertPupilEmulation
        /// <summary>
        /// Thêm mới thông tin danh hiệu thi đua học sinh theo học kỳ / năm học
        /// </summary>
        /// <author>tungnd</author>
        /// <date>15/10/2012</date>
        /// <param name="ethnic">Đối tượng PupilEmulation</param>
        /// <returns>Đối tượng PupilEmulation</returns>

        public void InsertPupilEmulation(int UserID, List<PupilRanking> lstPupilRanking, List<PupilEmulation> lstPupilEmulation, int AppliedLevel)
        {
            ValidationMetadata.ValidateObject(lstPupilEmulation);
            for (int i = 0; i < lstPupilEmulation.Count; i++)
            {
                //  PupilID và ClassID not compatible trong PupilProfile
                IDictionary<string, object> PupilProfileCompatible = new Dictionary<string, object>();
                PupilProfileCompatible["PupilProfileID"] = lstPupilEmulation[i].PupilID;
                PupilProfileCompatible["CurrentClassID"] = lstPupilEmulation[i].ClassID;

                bool PupilProfileCompatibleExist = new PupilProfileRepository(this.context).ExistsRow(GlobalConstants.PUPIL_SCHEMA, "PupilProfile", PupilProfileCompatible, null);

                if (!PupilProfileCompatibleExist)
                {
                    throw new BusinessException("Common_Validate_NotCompatible");
                }

                //  AcademicYearID và ClassID not compatible trong ClassProfile
                IDictionary<string, object> ClassProfileCompatible = new Dictionary<string, object>();
                ClassProfileCompatible["AcademicYearID"] = lstPupilEmulation[i].AcademicYearID;
                ClassProfileCompatible["ClassProfileID"] = lstPupilEmulation[i].ClassID;

                bool ClassProfileCompatibleExist = new PupilProfileRepository(this.context).ExistsRow(GlobalConstants.SCHOOL_SCHEMA, "ClassProfile", ClassProfileCompatible, null);

                if (!ClassProfileCompatibleExist)
                {
                    throw new BusinessException("Common_Validate_NotCompatible");
                }

                //  kiểm tra ProfileStatus trong PupilOfClass

                IQueryable<PupilOfClass> lstPupilOfClass = PupilOfClassBusiness.All.Where(o => (o.PupilID == lstPupilEmulation[i].PupilID));
                PupilOfClass PupilOfClass = lstPupilOfClass.FirstOrDefault();

                if (PupilOfClass.Status != 1)
                {
                    throw new BusinessException("Common_Validate_NotProfileStatus");
                }

                if (lstPupilRanking[i].AverageMark == null || lstPupilRanking[i].CapacityLevelID == null || lstPupilRanking[i].ConductLevelID == null)
                {
                    throw new BusinessException("Common_Validate_NotPupilRanking");
                }
                if (lstPupilRanking[i].CapacityLevelID != 1 && lstPupilRanking[i].CapacityLevelID != 2 & lstPupilRanking[i].ConductLevelID != 1 && lstPupilRanking[i].ConductLevelID != 2 && lstPupilRanking[i].ConductLevelID != 3)
                {
                    throw new BusinessException("Common_Validate_NotRank");
                }

                if (UtilsBusiness.HasHeadTeacherPermission(UserID, lstPupilEmulation[i].ClassID))
                {

                    IQueryable<ClassProfile> lstClassProfile = ClassProfileBusiness.All.Where(o => (o.ClassProfileID == lstPupilEmulation[i].ClassID));
                    ClassProfile ClassProfile = lstClassProfile.FirstOrDefault();
                    //
                    if (AppliedLevel == 2 || AppliedLevel == 3)
                    {
                        if (lstPupilRanking[i].CapacityLevelID == 1 && lstPupilRanking[i].ConductLevelID == GlobalConstants.CONDUCT_TYPE_GOOD_SECONDARY || lstPupilRanking[i].ConductLevelID == GlobalConstants.CONDUCT_TYPE_GOOD_TERTIARY)
                        {
                            lstPupilEmulation[i].HonourAchivementTypeID = 1;
                        }
                        //

                        if (lstPupilRanking[i].CapacityLevelID == 2 && lstPupilRanking[i].ConductLevelID == 3 || lstPupilRanking[i].ConductLevelID == 4 || lstPupilRanking[i].ConductLevelID == 7 || lstPupilRanking[i].ConductLevelID == 8)
                        {
                            lstPupilEmulation[i].HonourAchivementTypeID = 2;
                        }
                        //
                        if (lstPupilRanking[i].ConductLevelID == 3 || lstPupilRanking[i].ConductLevelID == 4 || lstPupilRanking[i].ConductLevelID == 5 || lstPupilRanking[i].ConductLevelID == 7 || lstPupilRanking[i].ConductLevelID == 8 || lstPupilRanking[i].ConductLevelID == 9
                            && lstPupilRanking[i].CapacityLevelID <= 3 && lstPupilRanking[i].TotalAbsentDaysWithPermission + lstPupilRanking[i].TotalAbsentDaysWithoutPermission <= 45
                            && ClassProfile.EducationLevelID == 9 || ClassProfile.EducationLevelID == 12)
                        {
                            lstPupilRanking[i].StudyingJudgementID = GlobalConstants.STUDYING_JUDGEMENT_GRADUATION;
                        }
                        //
                        if (lstPupilRanking[i].ConductLevelID == 3 || lstPupilRanking[i].ConductLevelID == 4 || lstPupilRanking[i].ConductLevelID == 5 || lstPupilRanking[i].ConductLevelID == 7 || lstPupilRanking[i].ConductLevelID == 8 || lstPupilRanking[i].ConductLevelID == 9
                        && lstPupilRanking[i].CapacityLevelID <= 3 && lstPupilRanking[i].TotalAbsentDaysWithPermission + lstPupilRanking[i].TotalAbsentDaysWithoutPermission <= 45
                        && ClassProfile.EducationLevelID != 9 || ClassProfile.EducationLevelID != 12)
                        {
                            lstPupilRanking[i].StudyingJudgementID = GlobalConstants.STUDYING_JUDGEMENT_UPCLASS;
                        }
                        //
                        if (lstPupilRanking[i].CapacityLevelID == 5 || lstPupilRanking[i].TotalAbsentDaysWithPermission + lstPupilRanking[i].TotalAbsentDaysWithoutPermission >= 45
                            || lstPupilRanking[i].ConductLevelID == 6 || lstPupilRanking[i].ConductLevelID == 10 && lstPupilRanking[i].CapacityLevelID == 4)
                        {
                            lstPupilRanking[i].StudyingJudgementID = GlobalConstants.STUDYING_JUDGEMENT_STAYCLASS;
                        }



                        //
                        if (lstPupilRanking[i].ConductLevelID == 3 || lstPupilRanking[i].ConductLevelID == 4 || lstPupilRanking[i].ConductLevelID == 5 || lstPupilRanking[i].ConductLevelID == 7 || lstPupilRanking[i].ConductLevelID == 8 || lstPupilRanking[i].ConductLevelID == 9
                        && lstPupilRanking[i].CapacityLevelID == 4)
                        {
                            lstPupilRanking[i].StudyingJudgementID = GlobalConstants.STUDYING_JUDGEMENT_RETEST;
                        }

                        //
                        if (lstPupilRanking[i].ConductLevelID == 6 || lstPupilRanking[i].ConductLevelID == 10 && lstPupilRanking[i].CapacityLevelID <= 3)
                        {
                            lstPupilRanking[i].StudyingJudgementID = GlobalConstants.STUDYING_JUDGEMENT_BACKTRAINING;
                        }

                        // PupilRankingBusiness.RankingPupil(UserID, lstPupilRanking);

                        IDictionary<string, object> PupilEmulationSearch = new Dictionary<string, object>();
                        PupilEmulationSearch["PupilID"] = lstPupilEmulation[i].PupilID;
                        PupilEmulationSearch["ClassID"] = lstPupilEmulation[i].ClassID;
                        PupilEmulationSearch["Semester "] = lstPupilEmulation[i].Semester;


                        bool PupilEmulationSearchExist = new PupilEmulationRepository(this.context).ExistsRow(GlobalConstants.PUPIL_SCHEMA, "PupilEmulation", PupilEmulationSearch, null);

                        if (!PupilEmulationSearchExist)
                        {
                            base.Insert(lstPupilEmulation[i]);
                        }

                        else
                        {

                            base.Update(lstPupilEmulation[i]);
                        }
                    }

                }

            }


        }


        #endregion

        #region Search
        /// <summary>
        /// Tìm kiếm thông tin danh hiệu thi đua học sinh theo học kỳ / năm học
        /// </summary>
        /// <author>tungnd</author>
        /// <date>15/10/2012</date>
        /// <param name="ethnic">Đối tượng PupilEmulation</param>
        /// <returns>Đối tượng PupilEmulation</returns>

        private IQueryable<PupilEmulation> Search(IDictionary<string, object> SearchPupilEmulation)
        {
            int PupilEmulationID = Utils.GetInt(SearchPupilEmulation, "PupilEmulationID");
            int PupilID = Utils.GetInt(SearchPupilEmulation, "PupilID");
            int ClassID = Utils.GetInt(SearchPupilEmulation, "ClassID");
            int SchoolID = Utils.GetInt(SearchPupilEmulation, "SchoolID");
            int AcademicYearID = Utils.GetInt(SearchPupilEmulation, "AcademicYearID");
            int Year = Utils.GetInt(SearchPupilEmulation, "Year");
            int Semester = Utils.GetInt(SearchPupilEmulation, "Semester");
            int HonourAchivementTypeID = Utils.GetInt(SearchPupilEmulation, "HonourAchivementTypeID");
            DateTime? CreatedDate = Utils.GetDateTime(SearchPupilEmulation, "CreatedDate");
            List<int> lstClassID = Utils.GetIntList(SearchPupilEmulation, "lstClassID");

            IQueryable<PupilEmulation> lsPupilEmulation = this.PupilEmulationRepository.All;
            if (ClassID != 0)
            {
                lsPupilEmulation = from pe in lsPupilEmulation
                                   join cp in ClassProfileBusiness.All on pe.ClassID equals cp.ClassProfileID
                                   where (!cp.IsActive.HasValue || (cp.IsActive.HasValue && cp.IsActive.Value))
                                   && pe.ClassID == ClassID
                                   select pe;
            }
            // bắt đầu tìm kiếm
            if (PupilEmulationID != 0)
            {
                lsPupilEmulation = lsPupilEmulation.Where(pe => (pe.PupilEmulationID == PupilEmulationID));
            }
            if (PupilID != 0)
            {
                lsPupilEmulation = lsPupilEmulation.Where(pe => (pe.PupilID == PupilID));
            }
            if (SchoolID != 0)
            {
                int partitionId = UtilsBusiness.GetPartionId(SchoolID);
                lsPupilEmulation = lsPupilEmulation.Where(pe => pe.SchoolID == SchoolID && pe.Last2digitNumberSchool == partitionId);
            }
            if (AcademicYearID != 0)
            {
                lsPupilEmulation = lsPupilEmulation.Where(pe => (pe.AcademicYearID == AcademicYearID));
            }
            if (Year != 0)
            {
                lsPupilEmulation = lsPupilEmulation.Where(pe => (pe.Year == Year));
            }
            if (Semester != 0)
            {
                lsPupilEmulation = lsPupilEmulation.Where(pe => (pe.Semester == Semester));
            }
            if (HonourAchivementTypeID != 0)
            {
                lsPupilEmulation = lsPupilEmulation.Where(pe => (pe.HonourAchivementTypeID == HonourAchivementTypeID));
            }
            if (CreatedDate != null)
            {
                lsPupilEmulation = lsPupilEmulation.Where(pe => (pe.CreatedDate == CreatedDate));
            }
            if (lstClassID.Count > 0)
            {
                lsPupilEmulation = lsPupilEmulation.Where(p => lstClassID.Contains(p.ClassID));
            }
            return lsPupilEmulation;
        }

        #endregion

        #region SearchBySchool
        /// <summary>
        /// Lấy ra thông tin từ mã trường
        /// </summary>
        /// <author>tungnd</author>
        /// <date>15/10/2012</date>
        /// <param name="ethnic">Đối tượng PupilEmulation</param>
        /// <returns>Đối tượng PupilEmulation</returns>
        public IQueryable<PupilEmulation> SearchBySchool(int SchoolID, IDictionary<string, object> SearchInfo)
        {
            if (SchoolID == 0)
            {
                return null;
            }
            else
            {
                SearchInfo["SchoolID"] = SchoolID;
                return Search(SearchInfo);

            }

        }

        #endregion


    }
}