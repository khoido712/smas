/**
 * The Viettel License
 *
 * Copyright 2012 Viettel Telecom. All rights reserved.
 * VIETTEL PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */

/** 
* @author  
* @version $Revision: $
*/

using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Linq.Expressions;
using System.Web;
using log4net;

using SMAS.DAL.Repository;
using SMAS.DAL.IRepository;
using SMAS.Business.IBusiness;
using SMAS.Models.Models;
using SMAS.Business.Common;

namespace SMAS.Business.Business
{ 
    public partial class MinenalCatBusiness
    {
        #region Search
        public IQueryable<MinenalCat> Search(IDictionary<string, object> SearchInfo)
        {

            int MineralID = Utils.GetInt(SearchInfo, "MineralCatID");
            string MineralName = Utils.GetString(SearchInfo, "MineralName");
            bool IsActive = Utils.GetBool(SearchInfo, "IsActive");

            IQueryable<MinenalCat> lstMinenalCat = this.MinenalCatRepository.All;

            if (MineralID != 0)
            {
                lstMinenalCat = lstMinenalCat.Where(o => o.MinenalCatID == MineralID);
            }
            if (MineralName.Length != 0)
            {
                lstMinenalCat = lstMinenalCat.Where(o => o.MinenalName == MineralName);
            }
            if (IsActive != false)
            {
                lstMinenalCat = lstMinenalCat.Where(o => o.IsActive == IsActive);
            }

            return lstMinenalCat;
        }
        #endregion
        
    }
}