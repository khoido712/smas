﻿/**
 * The Viettel License
 *
 * Copyright 2012 Viettel Telecom. All rights reserved.
 * VIETTEL PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */

/** 
* @author  admin
* @version $Revision: $
*/

using System;
using System.Collections.Generic;
using System.Linq;
using SMAS.Models.Models;
using SMAS.Business.Common;
using SMAS.Business.BusinessObject;

namespace SMAS.Business.Business
{
    public partial class WorkFlowUserBusiness
    {
        public IQueryable<WorkFlowUserBO> Search(IDictionary<string, object> dic)
        {
            IQueryable<WorkFlowUserBO> iqWorkFlow = from ws in WorkFlowBusiness.All
                                                    join wu in WorkFlowUserBusiness.All on ws.WorkFlowID equals wu.WorkFlowID
                                                    where ws.IsActive
                                                    select new WorkFlowUserBO
                                                    {
                                                        IsAutoAlert = wu.IsAutoAlert.HasValue ? wu.IsAutoAlert.Value : false,
                                                        IsPopUp = wu.IsPopUp.HasValue ? wu.IsPopUp.Value : false,
                                                        OrderID = wu.OrderID,
                                                        UserID = wu.UserID,
                                                        WorkFlowID = ws.WorkFlowID,
                                                        WorkFlowName = ws.WorkFlowName,
                                                        ViewName = ws.ViewName,
                                                        DefaultTab = wu.DefaultTab,
                                                        AppliedLevel = ws.AppliedLevel,
                                                        ProductVersion = ws.ProductVersion,
                                                        AvailableAdmin = ws.AvailableAdmin.HasValue ? ws.AvailableAdmin.Value : false,
                                                        AvailableSup = ws.AvailableSup.HasValue ? ws.AvailableSup.Value : false,
                                                        AvailableTeacher = ws.AvailableTeacher.HasValue ? ws.AvailableTeacher.Value : false
                                                    };


            int userId = Utils.GetInt(dic, "UserID");
            int appliedLevel = Utils.GetInt(dic, "AppliedLevel");
            int productVersion = Utils.GetInt(dic, "ProductVersion");
            int workFlowID = Utils.GetInt(dic, "WorkFlowID");
            if (userId > 0)
            {
                iqWorkFlow = iqWorkFlow.Where(w => w.UserID == userId);
            }
            if (appliedLevel > 0)
            {
                string strAppliedLevel = appliedLevel.ToString();
                iqWorkFlow = iqWorkFlow.Where(o => o.AppliedLevel.Contains(strAppliedLevel));
            }
            if (productVersion > 0)
            {
                string strPr = productVersion.ToString();
                iqWorkFlow = iqWorkFlow.Where(o => o.ProductVersion.Contains(strPr));
            }
            if (workFlowID > 0)
            {
                iqWorkFlow = iqWorkFlow.Where(w => w.WorkFlowID == workFlowID);
            }
            return iqWorkFlow;
        }

        public List<FirstDataFunctionBO> GetFirstDataFunction(IDictionary<string, object> dic, int tabFunction)
        {
            try
            {
                SetAutoDetectChangesEnabled(false);
                int appliedLevel = Utils.GetInt(dic, "AppliedLevel");
                int productVersion = Utils.GetInt(dic, "ProductVersion");
                int academicYearId = Utils.GetInt(dic, "AcademicYearId");
                int semester = Utils.GetInt(dic, "Semester");
                AcademicYear objAcademicYear = AcademicYearBusiness.Find(academicYearId);
                if (objAcademicYear == null)
                {
                    return new List<FirstDataFunctionBO>();
                }
                List<FirstDataFunctionBO> lstVal = new List<FirstDataFunctionBO>();
                IEnumerable<WorkFlow> ieWorkFlow = StaticData.listWorkFlow.Where(f => f.ParentID == tabFunction);
                SchoolProfile school = SchoolProfileBusiness.Find(objAcademicYear.SchoolID);

                if (appliedLevel > 0)
                {
                    ieWorkFlow = ieWorkFlow.Where(f => f.AppliedLevel != null && f.AppliedLevel.Contains(appliedLevel.ToString()));
                }

                if (productVersion > 0)
                {
                    ieWorkFlow = ieWorkFlow.Where(f => f.ProductVersion != null && f.ProductVersion.Contains(productVersion.ToString()));
                }

                //Neu truong khong hoc VNEN thi khong lay chuc nang khai bao mon hoc VNEN
                if (school.IsNewSchoolModel == null || school.IsNewSchoolModel == false)
                {
                    ieWorkFlow = ieWorkFlow.Where(f => f.WorkFlowID != WorkFlowConstants.FUNC_CLASS_SUBJECT_VNEN);
                }
                List<WorkFlow> lstWorkFlow = ieWorkFlow.ToList();
                if (lstWorkFlow.Count == 0)
                {
                    return new List<FirstDataFunctionBO>();
                }

                //Danh sach khoi hoc theo cap hoc
                List<int> lstEducationLevelId = EducationLevelBusiness.AllNoTracking.Where(e => e.Grade == appliedLevel).Select(e => e.EducationLevelID).ToList();

                /*int numTask = 1;
                Task[] arrTask = new Task[numTask];
                List<WorkFlow> lstWorkFlowTask;
                for (int z = 0; z < numTask; z++)
                {
                    int currentIndex = z;

                    arrTask[z] = Task.Factory.StartNew(() =>
                    {

                        lstWorkFlowTask = lstWorkFlow.Where(w => w.WorkFlowID % numTask == currentIndex).ToList();
                        if (lstWorkFlowTask != null && lstWorkFlowTask.Count > 0)
                        {
                            for (int i = 0; i < lstWorkFlowTask.Count; i++)
                            {
                                objWorkFlow = lstWorkFlowTask[i];
                                objFunction = new FirstDataFunctionBO
                                {
                                    FunctionName = objWorkFlow.WorkFlowName,
                                    FunctionId = objWorkFlow.WorkFlowID,
                                    Url = objWorkFlow.Url,
                                    AppliedLevel = objWorkFlow.AppliedLevel,
                                    ProductVersion = objWorkFlow.ProductVersion,
                                    TabFunction = objWorkFlow.ParentID
                                };
                                FillDataIntoDataFunctionBO(objWorkFlow, objFunction, objAcademicYear, appliedLevel, lstEducationLevelId, lstVal);
                            }
                        }

                    });
                }
                Task.WaitAll(arrTask);*/
                for (int i = 0; i < lstWorkFlow.Count; i++)
                {
                    var objWorkFlow = lstWorkFlow[i];
                    if (objWorkFlow.WorkFlowID == WorkFlowConstants.FUNC_CLASS_SUBJECT_VNEN)
                    {
                        //Set lai menu cho chuc nang khai bao mon hoc VNEN
                        var arrConfig = !String.IsNullOrEmpty(objWorkFlow.ConfigAction)
                            ? objWorkFlow.ConfigAction.Split(',')
                            : new string[2];
                        if (!arrConfig.Any())
                        {
                            arrConfig[0] = "6";
                            arrConfig[1] = "7";
                        }
                        string url = Utils.RemoveLastIndexCharacter(objWorkFlow.Url, "&");
                        objWorkFlow.Url = String.Format("{0}&Pro={1}", url, productVersion == GlobalConstants.PRODUCT_VERSION_ID_ADVANCE ? arrConfig[0] : arrConfig[1]);
                    }
                    var objFunction = new FirstDataFunctionBO
                    {
                        FunctionName = objWorkFlow.WorkFlowName,
                        FunctionId = objWorkFlow.WorkFlowID,
                        Url = objWorkFlow.Url,
                        AppliedLevel = objWorkFlow.AppliedLevel,
                        ProductVersion = objWorkFlow.ProductVersion,
                        TabFunction = objWorkFlow.ParentID
                    };
                    FillDataIntoDataFunctionBO(objWorkFlow, objFunction, objAcademicYear, appliedLevel, semester, lstEducationLevelId, lstVal);
                }
                SetAutoDetectChangesEnabled(true);
                return lstVal.OrderBy(c => c.FunctionId).ToList();
            }
            finally
            {
                SetAutoDetectChangesEnabled(true);
            }
        }


        private void FillDataIntoDataFunctionBO(WorkFlow objWorkFlow, FirstDataFunctionBO objFunction, AcademicYear objAcademicYear, int appliedLevel, int semester, List<int> lstEducationLevelId, List<FirstDataFunctionBO> lstVal)
        {
            if (objAcademicYear == null)
            {
                return;
            }
            //IClassProfileBusiness ClassProfileBusiness = new ClassProfileBusiness(logger, context);
            //IEducationLevelBusiness EducationLevelBusiness = new EducationLevelBusiness(logger, context);
            //ISchoolFacultyBusiness SchoolFacultyBusiness = new SchoolFacultyBusiness(logger, context);
            IDictionary<string, object> searchInfo = new Dictionary<string, object>();
            searchInfo["SchoolID"] = objAcademicYear.SchoolID;
            switch (objWorkFlow.WorkFlowID)
            {

                case WorkFlowConstants.FUNC_ACADEMIC_YEAR:
                    #region 1. Thoi gian nam hoc

                    //Đã khai báo: nếu rơi vào các trường hợp sau
                    //- Ngày hiện tại thuộc năm học đang thao tác
                    //- Ngày hiện tại không thuộc năm học đang thao tác, nhưng năm hiện tại trùng với năm bắt đầu của năm học đã khai báo.
                    if ((objAcademicYear.FirstSemesterStartDate <= DateTime.Now && DateTime.Now <= objAcademicYear.SecondSemesterEndDate) || objAcademicYear.Year == DateTime.Now.Year)
                    {
                        objFunction.StatusName = WorkFlowConstants.STATUS_DECLARED;
                        objFunction.Contents = String.Format("Năm học hiện tại: {0}{1}{2}", WorkFlowConstants.HTML_SPAN_COLOR_ORANGE, objAcademicYear.DisplayTitle, WorkFlowConstants.HTML_END_SPAN);
                    }
                    else
                    {
                        objFunction.StatusName = WorkFlowConstants.STATUS_NOT_DECLARE;
                        objFunction.Contents = "";
                    }
                    lstVal.Add(objFunction);
                    #endregion
                    break;
                case WorkFlowConstants.FUNC_SCHOOL_FACULTY:
                    #region 2. To bo mon

                    List<SchoolFaculty> lstFaculty = SchoolFacultyBusiness.AllNoTracking.Where(s => s.SchoolID == objAcademicYear.SchoolID && s.IsActive).ToList();
                    if (lstFaculty.Count == 0)
                    {
                        objFunction.StatusName = WorkFlowConstants.STATUS_NOT_DECLARE;
                        objFunction.Contents = "";
                    }
                    else
                    {
                        objFunction.StatusName = WorkFlowConstants.STATUS_DECLARED;
                        string content = "Tổ bộ môn đã khai báo: ";
                        for (int k = 0; k < lstFaculty.Count; k++)
                        {
                            content += String.Format("{0}, ", lstFaculty[k].FacultyName);
                        }
                        objFunction.Contents = Utils.RemoveLastIndexCharacter(content, ",");

                    }
                    lstVal.Add(objFunction);
                    #endregion
                    break;
                case WorkFlowConstants.FUNC_SCHOOL_CLASS:
                    #region 3. Khai bao lop hoc

                    var lstClass = (from e in EducationLevelBusiness.AllNoTracking.Where(c => lstEducationLevelId.Contains(c.EducationLevelID))
                                    join c in ClassProfileBusiness.AllNoTracking.Where(c => c.AcademicYearID == objAcademicYear.AcademicYearID && c.IsActive==true) on e.EducationLevelID equals c.EducationLevelID into ec
                                    from lec in ec.DefaultIfEmpty()
                                    select new { e.EducationLevelID, e.Resolution, lec.ClassProfileID } into x
                                    group x by new { x.EducationLevelID, x.Resolution } into g
                                    select new
                                    {
                                        g.Key.EducationLevelID,
                                        g.Key.Resolution,
                                        Count = g.Count(c => c.ClassProfileID > 0)
                                    }).OrderBy(x => x.EducationLevelID).ToList();

                    int countClassNotDeclare = (lstClass.Count == 0) ? 0 : lstClass.Count(c => c.Count == 0);

                    //Neu khong tim thay du lieu hoac tat cac cac lop deu chua khai bao=> Chua khai bao mon hoc
                    if (lstClass.Count == 0 || (countClassNotDeclare > 0 && countClassNotDeclare == lstClass.Count)) //Chua khai bao
                    {
                        objFunction.StatusName = WorkFlowConstants.STATUS_NOT_DECLARE;
                        objFunction.Contents = "Chưa có lớp nào được tạo";
                    }
                    else if (countClassNotDeclare > 0) // Khai bao chua day du
                    {
                        int sumClass = lstClass.Sum(c => c.Count);
                        objFunction.StatusName = WorkFlowConstants.STATUS_NOT_FULL_DECLARE;
                        objFunction.Contents = String.Format("{0}{1}{2} lớp học đã tạo. ", WorkFlowConstants.HTML_SPAN_COLOR_ORANGE, sumClass, WorkFlowConstants.HTML_END_SPAN);
                        for (int k = 0; k < lstClass.Count; k++)
                        {
                            if (lstClass[k].Count == 0)
                            {
                                continue;
                            }
                            objFunction.Contents += String.Format(" {0}: {1}{2}{3};", lstClass[k].Resolution, WorkFlowConstants.HTML_SPAN_COLOR_ORANGE, lstClass[k].Count, WorkFlowConstants.HTML_END_SPAN);
                        }
                        objFunction.Contents = Utils.RemoveLastIndexCharacter(objFunction.Contents, ";");
                    }
                    else //Khai bao day du
                    {
                        int sumClass = lstClass.Sum(c => c.Count);
                        objFunction.StatusName = WorkFlowConstants.STATUS_DECLARED;
                        objFunction.Contents = String.Format("{0}{1}{2} lớp học đã tạo. ", WorkFlowConstants.HTML_SPAN_COLOR_ORANGE, sumClass, WorkFlowConstants.HTML_END_SPAN);
                        for (int k = 0; k < lstClass.Count; k++)
                        {
                            objFunction.Contents += String.Format(" {0}: {1}{2}{3};", lstClass[k].Resolution, WorkFlowConstants.HTML_SPAN_COLOR_ORANGE, lstClass[k].Count, WorkFlowConstants.HTML_END_SPAN);
                        }
                        objFunction.Contents = Utils.RemoveLastIndexCharacter(objFunction.Contents, ";");
                    }
                    //Khai bao chua day d
                    lstVal.Add(objFunction);
                    #endregion
                    break;

                case WorkFlowConstants.FUNC_CLASS_SUBJECT:
                    #region 4. Khai bao mon hoc cho lop
                    var iqSubjectClass = from cp in ClassProfileBusiness.SearchByAcademicYear(objAcademicYear.AcademicYearID, searchInfo).Where(c => lstEducationLevelId.Contains(c.EducationLevelID))
                                         join cs in ClassSubjectBusiness.SearchBySchool(objAcademicYear.SchoolID, searchInfo) on cp.ClassProfileID equals cs.ClassID into cps
                                         from lec in cps.DefaultIfEmpty()
                                         select new { cp.ClassProfileID, lec.SubjectID } into x
                                         group x by new { x.ClassProfileID } into g
                                         select new
                                         {
                                             g.Key.ClassProfileID,
                                             Count = g.Count(c => c.SubjectID > 0)
                                         };
                    var lstSubjectClass = iqSubjectClass.ToList();
                    //Chua khai bao mon hoc
                    int countSubjectClassNot = (lstSubjectClass.Count == 0) ? 0 : lstSubjectClass.Count(c => c.Count == 0);
                    //Neu lop hoc chua duoc khai bao hoac tat cac cac lop chua khai bao mon hoc=> Chua khai bao
                    if (lstSubjectClass.Count == 0 || (countSubjectClassNot > 0 && countSubjectClassNot == lstSubjectClass.Count))
                    {
                        objFunction.StatusName = WorkFlowConstants.STATUS_NOT_DECLARE;
                        objFunction.Contents = "Chưa khai báo môn học cho lớp";
                    }
                    else if (countSubjectClassNot > 0)//Khai bao chua day du
                    {
                        int coutClass = lstSubjectClass.Count(c => c.Count == 0);
                        objFunction.StatusName = WorkFlowConstants.STATUS_NOT_FULL_DECLARE;
                        objFunction.Contents = String.Format("{0}{1}{2} lớp chưa được khai báo môn học.", WorkFlowConstants.HTML_SPAN_COLOR_ORANGE, coutClass, WorkFlowConstants.HTML_END_SPAN);
                    }
                    else
                    {
                        int coutClass = lstSubjectClass.Count(c => c.Count > 0);
                        objFunction.StatusName = WorkFlowConstants.STATUS_DECLARED;
                        objFunction.Contents = String.Format("{0}{1}{2} lớp đã được khai báo môn học.", WorkFlowConstants.HTML_SPAN_COLOR_ORANGE, coutClass, WorkFlowConstants.HTML_END_SPAN);
                    }

                    lstVal.Add(objFunction);
                    #endregion
                    break;

                case WorkFlowConstants.FUNC_CLASS_SUBJECT_VNEN:
                    #region 5. Khai bao mon hoc cho lop VNEN
                    var iqSubjectClassVnen = from cp in ClassProfileBusiness.SearchByAcademicYear(objAcademicYear.AcademicYearID, searchInfo).Where(c => lstEducationLevelId.Contains(c.EducationLevelID) && c.IsVnenClass == true)
                                             join cs in ClassSubjectBusiness.SearchBySchool(objAcademicYear.SchoolID, searchInfo).Where(c => c.IsSubjectVNEN == true) on cp.ClassProfileID equals cs.ClassID into cps
                                             from lec in cps.DefaultIfEmpty()
                                             select new { cp.ClassProfileID, lec.SubjectID } into x
                                             group x by new { x.ClassProfileID } into g
                                             select new
                                             {
                                                 g.Key.ClassProfileID,
                                                 Count = g.Count(c => c.SubjectID > 0)
                                             };
                    var lstSubjectClassVnen = iqSubjectClassVnen.ToList();
                    //Dem mon hoc chua duoc khai bao
                    int countNotDeracle = lstSubjectClassVnen.Count(c => c.Count == 0);
                    //Khong hoc chuong trinh VNEN
                    if (lstSubjectClassVnen.Count == 0)
                    {
                        objFunction.StatusName = "";
                        objFunction.Contents = "Không có lớp học theo chương trình VNEN";
                    }
                    else if (countNotDeracle == lstSubjectClassVnen.Count)//Chua khai bao
                    {
                        objFunction.StatusName = WorkFlowConstants.STATUS_NOT_DECLARE;
                        objFunction.Contents = "Chưa khai báo môn học cho lớp VNEN";
                    }
                    else if (countNotDeracle > 0)//Khai bao chua day du
                    {
                        objFunction.StatusName = WorkFlowConstants.STATUS_NOT_FULL_DECLARE;
                        objFunction.Contents = String.Format("{0}{1}{2} lớp chưa được khai báo môn học cho lớp VNEN.", WorkFlowConstants.HTML_SPAN_COLOR_ORANGE, countNotDeracle, WorkFlowConstants.HTML_END_SPAN);
                    }
                    else
                    {
                        int coutClass = lstSubjectClassVnen.Count;
                        objFunction.StatusName = WorkFlowConstants.STATUS_DECLARED;
                        objFunction.Contents = String.Format("{0}{1}{2} lớp đã được khai báo môn học cho  lớp VNEN.", WorkFlowConstants.HTML_SPAN_COLOR_ORANGE, coutClass, WorkFlowConstants.HTML_END_SPAN);
                    }

                    lstVal.Add(objFunction);
                    #endregion
                    break;

                case WorkFlowConstants.FUNC_TRANSFER_DATA:
                    #region 6. Ket chuyen du lieu
                    //Lay nam hoc truoc
                    AcademicYear preAcademicYear = AcademicYearBusiness.AllNoTracking.FirstOrDefault(a => a.SchoolID == objAcademicYear.SchoolID && a.Year == objAcademicYear.Year - 1 && a.IsActive==true);
                    if (preAcademicYear == null)
                    {
                        objFunction.StatusName = WorkFlowConstants.STATUS_EXECUTED;
                        objFunction.Contents = "Không có hồ sơ của năm học cũ";
                        lstVal.Add(objFunction);
                        break;
                    }
                    int lastEducationId = 5;
                    //Khong lay cac lop cuoi cap
                    if (appliedLevel == GlobalConstants.APPLIED_LEVEL_PRIMARY)
                    {
                        lastEducationId = GlobalConstants.EDUCATION_LEVEL_ID5;
                    }
                    else if (appliedLevel == GlobalConstants.APPLIED_LEVEL_SECONDARY)
                    {
                        lastEducationId = GlobalConstants.EDUCATION_LEVEL_ID9;
                    }
                    else if (appliedLevel == GlobalConstants.APPLIED_LEVEL_TERTIARY)
                    {
                        lastEducationId = GlobalConstants.EDUCATION_LEVEL_ID12;
                    }
                    var iqForwadClass = from cp in ClassProfileBusiness.SearchByAcademicYear(preAcademicYear.AcademicYearID)
                                        join cf in ClassForwardingBusiness.SearchByAcademicYear(objAcademicYear.AcademicYearID, searchInfo) on cp.ClassProfileID equals cf.ClosedClassID into cpf
                                        from lec in cpf.DefaultIfEmpty()
                                        where cp.EducationLevelID < lastEducationId
                                        && lstEducationLevelId.Contains(cp.EducationLevelID)
                                        select new { cp.ClassProfileID, lec.ClosedClassID } into x
                                        group x by new { x.ClassProfileID } into g
                                        select new
                                        {
                                            g.Key.ClassProfileID,
                                            CountForwardClass = g.Count(c => c.ClosedClassID >= 0)
                                        };

                    var lstForwadClass = iqForwadClass.ToList();
                    //So lop chua duoc ket chuyen
                    int countNotFullForward = lstForwadClass.Count(c => c.CountForwardClass == 0);

                    //Da thuc hien: Không tồn tại lớp học của năm học cũ
                    if (lstForwadClass.Count == 0)
                    {
                        objFunction.StatusName = WorkFlowConstants.STATUS_EXECUTED;
                        objFunction.Contents = "Không có hồ sơ của năm học cũ";
                    }
                    else if (countNotFullForward > 0)
                    {
                        //So lop da ket chuyen
                        int countClassForwarded = lstForwadClass.Count(c => c.CountForwardClass > 0);
                        //Chua thuc hien: Tat ca cac lop chua thuc hien ket chuyen
                        if (countNotFullForward == lstForwadClass.Count)
                        {
                            objFunction.StatusName = WorkFlowConstants.STATUS_NOT_EXECUTE;
                        }
                        else //Chưa thực hiện đầy đủ: tồn tại lớp học của năm học cũ chưa được kết chuyển sang năm học mới
                        {
                            objFunction.StatusName = WorkFlowConstants.STATUS_NOT_FULL_EXECUTE;
                        }
                        //objFunction.Contents = "Không có hồ sơ của năm học cũ";
                        objFunction.Contents = String.Format("{0}{1}/{2}{3} lớp đã được kết chuyển hồ sơ học sinh sang năm học mới", WorkFlowConstants.HTML_SPAN_COLOR_ORANGE, countClassForwarded, lstForwadClass.Count, WorkFlowConstants.HTML_END_SPAN);
                    }
                    else
                    {
                        objFunction.StatusName = WorkFlowConstants.STATUS_EXECUTED;
                        int countClassForwarded = lstForwadClass.Count(c => c.CountForwardClass > 0);
                        objFunction.Contents = String.Format("{0}{1}/{2}{3} lớp đã được kết chuyển hồ sơ học sinh sang năm học mới", WorkFlowConstants.HTML_SPAN_COLOR_ORANGE, countClassForwarded, lstForwadClass.Count, WorkFlowConstants.HTML_END_SPAN);
                    }

                    lstVal.Add(objFunction);
                    #endregion
                    break;

                case WorkFlowConstants.FUNC_EMPLOYEE_PROFILE:
                    #region 7. Ho so can bo

                    searchInfo["EmploymentStatus"] = GlobalConstants.EMPLOYMENT_STATUS_WORKING;
                    var iqEmPloyee = from wf in WorkGroupTypeBusiness.All
                                     join e in EmployeeBusiness.SearchTeacher(objAcademicYear.SchoolID, searchInfo) on wf.WorkGroupTypeID equals e.WorkGroupTypeID into wfe
                                     from lec in wfe.DefaultIfEmpty()
                                     select new { wf.WorkGroupTypeID, wf.Resolution, lec.EmployeeID } into x
                                     group x by new { x.WorkGroupTypeID, x.Resolution } into g
                                     select new
                                     {
                                         g.Key.WorkGroupTypeID,
                                         g.Key.Resolution,
                                         Count = g.Count(c => c.EmployeeID > 0)
                                     };

                    var lstEmployee = iqEmPloyee.OrderBy(c => c.WorkGroupTypeID).ToList();
                    if (lstEmployee.Count == 0)
                    {
                        objFunction.StatusName = WorkFlowConstants.STATUS_NOT_EXECUTE;
                        objFunction.Contents = "Chưa cập nhật hồ sơ cán bộ";
                    }
                    else
                    {
                        objFunction.StatusName = WorkFlowConstants.STATUS_EXECUTED;
                        objFunction.Contents = String.Format("Tổng số hồ sơ: {0}{1}{2}; ", WorkFlowConstants.HTML_SPAN_COLOR_ORANGE, lstEmployee.Sum(c => c.Count), WorkFlowConstants.HTML_END_SPAN);
                        for (int i = 0; i < lstEmployee.Count; i++)
                        {
                            objFunction.Contents += String.Format("{0}: {1}{2}{3}; ", lstEmployee[i].Resolution, WorkFlowConstants.HTML_SPAN_COLOR_ORANGE, lstEmployee[i].Count, WorkFlowConstants.HTML_END_SPAN);
                        }
                        objFunction.Contents = Utils.RemoveLastIndexCharacter(objFunction.Contents, ";");
                    }
                    lstVal.Add(objFunction);
                    #endregion
                    break;

                case WorkFlowConstants.FUNC_PUPIL_PROFILE:
                    #region 8. Ho so hoc sinh
                    searchInfo["Status"] = GlobalConstants.PUPIL_STATUS_STUDYING;
                    searchInfo["AcademicYearID"] = objAcademicYear.AcademicYearID;
                    var iqPupil = from cp in ClassProfileBusiness.SearchByAcademicYear(objAcademicYear.AcademicYearID, searchInfo).Where(c => lstEducationLevelId.Contains(c.EducationLevelID))
                                  join poc in PupilOfClassBusiness.SearchBySchool(objAcademicYear.SchoolID, searchInfo) on cp.ClassProfileID equals poc.ClassID into cpp
                                  from lec in cpp.DefaultIfEmpty()
                                  select new { cp.ClassProfileID, lec.PupilID } into x
                                  group x by new { x.ClassProfileID } into g
                                  select new
                                  {
                                      g.Key.ClassProfileID,
                                      CountPupil = g.Count(p => p.PupilID > 0)
                                  };
                    var lstPupil = iqPupil.ToList();
                    //Sp lop chua co ho so hoc sinh
                    int countPupilNotDeclare = lstPupil.Count(p => p.CountPupil == 0);

                    //Neu chua khai bao lop 
                    if (lstPupil.Count == 0)
                    {
                        objFunction.StatusName = WorkFlowConstants.STATUS_NOT_EXECUTE;
                        objFunction.Contents = "Chưa khai báo lớp học";
                    }
                    else if (countPupilNotDeclare > 0)
                    {
                        //tat cac cac lop chua khai bao hs=> Chua thuc hien
                        if (countPupilNotDeclare == lstPupil.Count)
                        {
                            objFunction.StatusName = WorkFlowConstants.STATUS_NOT_EXECUTE;
                        }
                        else
                        {
                            objFunction.StatusName = WorkFlowConstants.STATUS_NOT_FULL_EXECUTE;
                        }
                        //So lop chua co hoc sinh                        
                        objFunction.Contents = String.Format("{0}{1}/{2}{3} lớp chưa có học sinh", WorkFlowConstants.HTML_SPAN_COLOR_ORANGE, countPupilNotDeclare, lstPupil.Count, WorkFlowConstants.HTML_END_SPAN);
                    }
                    else
                    {
                        objFunction.StatusName = WorkFlowConstants.STATUS_EXECUTED;
                        //So lop chua co ho so hoc sinh
                        objFunction.Contents = String.Format("{0}{1}/{2}{3} lớp chưa có học sinh", WorkFlowConstants.HTML_SPAN_COLOR_ORANGE, countPupilNotDeclare, lstPupil.Count, WorkFlowConstants.HTML_END_SPAN);
                    }
                    lstVal.Add(objFunction);
                    #endregion
                    break;

                case WorkFlowConstants.FUNC_TEACHING_ASSIGNMENT:
                    #region 9. Phan cong giang day
                    searchInfo["AcademicYearID"] = objAcademicYear.AcademicYearID;
                    searchInfo["Semester"] = semester;
                    var iqTeacherAssignment = from cp in ClassProfileBusiness.SearchByAcademicYear(objAcademicYear.AcademicYearID, searchInfo).Where(c => lstEducationLevelId.Contains(c.EducationLevelID))
                                              join cs in ClassSubjectBusiness.SearchBySchool(objAcademicYear.SchoolID, searchInfo) on cp.ClassProfileID equals cs.ClassID
                                              into cps
                                              from t12 in cps.DefaultIfEmpty()
                                              join tc in TeachingAssignmentBusiness.SearchBySchool(objAcademicYear.SchoolID, searchInfo) on new { c1 = cp.ClassProfileID, c2 = t12.SubjectID } equals new { c1 = tc.ClassID ?? 0, c2 = tc.SubjectID }
                                              into cptc
                                              from t13 in cptc.DefaultIfEmpty()
                                              //where t12.SubjectID == t13.SubjectID
                                              select new { cp.ClassProfileID, t12.SubjectID, t13.TeachingAssignmentID } into x
                                              group x by new { x.ClassProfileID } into g
                                              select new
                                              {
                                                  g.Key.ClassProfileID,
                                                  CountSubjectAssignment = g.Count(c => c.SubjectID > 0),
                                                  CountTeachingAssignment = g.Count(c => c.TeachingAssignmentID > 0)
                                              };
                    var lstTeacherAssignment = iqTeacherAssignment.ToList();

                    //So mon hoc khai bao cho lop
                    int countSubjectAssignment = (lstTeacherAssignment == null || lstTeacherAssignment.Count == 0) ? 0 : lstTeacherAssignment.Sum(c => c.CountSubjectAssignment);
                    //So mon hoc phan cong giang day
                    int countTeachingAssignment = (lstTeacherAssignment == null || lstTeacherAssignment.Count == 0) ? 0 : lstTeacherAssignment.Sum(c => c.CountTeachingAssignment);

                    ////Chưa khai báo lớp học hoặc môn học: - Chưa có lớp nào được khai báo
                    if (lstTeacherAssignment == null || lstTeacherAssignment.Count == 0)
                    {
                        objFunction.StatusName = String.Format("{0}Chưa khai báo lớp học hoặc môn học{1}", WorkFlowConstants.HTML_SPAN_COLOR_ORANGE, WorkFlowConstants.HTML_END_SPAN);
                        objFunction.Contents = "Chưa khai báo lớp học";
                    }
                    ////Chưa khai báo lớp học hoặc môn học: - Tất cả các lớp chưa được khai báo môn học
                    else if (countSubjectAssignment == 0)
                    {
                        objFunction.StatusName = String.Format("{0}Chưa khai báo lớp học hoặc môn học{1}", WorkFlowConstants.HTML_SPAN_COLOR_ORANGE, WorkFlowConstants.HTML_END_SPAN);
                        objFunction.Contents = "Chưa khai báo môn học cho lớp";
                    }
                    //Chưa thực hiện:  tất cả các môn của tất cả các lớp chưa được phân công phụ trách
                    else if (countTeachingAssignment == 0)
                    {
                        objFunction.StatusName = WorkFlowConstants.STATUS_NOT_EXECUTE;
                        objFunction.Contents = "Chưa có lớp nào được phân công giảng dạy đầy đủ";
                    }
                    //Chưa thực hiện đầy đủ: tồn tại môn học của lớp chưa được phân giáo viên phụ trách
                    else if (countTeachingAssignment > 0 && countSubjectAssignment > countTeachingAssignment)
                    {
                        objFunction.StatusName = WorkFlowConstants.STATUS_NOT_FULL_EXECUTE;
                        //Lop duoc phan cong giang day
                        int countClassTeaching = lstTeacherAssignment.Count(c => c.CountTeachingAssignment >= c.CountSubjectAssignment);
                        objFunction.Contents = String.Format("{0}{1}/{2}{3} lớp đã được phân công giảng dạy đầy đủ", WorkFlowConstants.HTML_SPAN_COLOR_ORANGE, countClassTeaching, lstTeacherAssignment.Count, WorkFlowConstants.HTML_END_SPAN);
                    }
                    else
                    {
                        objFunction.StatusName = WorkFlowConstants.STATUS_EXECUTED;
                        //Lop duoc phan cong giang day
                        int countTeaching = lstTeacherAssignment.Count(c => c.CountTeachingAssignment >= c.CountSubjectAssignment);
                        objFunction.Contents = String.Format("{0}{1}/{2}{3} lớp đã được phân công giảng dạy đầy đủ", WorkFlowConstants.HTML_SPAN_COLOR_ORANGE, countTeaching, lstTeacherAssignment.Count, WorkFlowConstants.HTML_END_SPAN);
                    }
                    lstVal.Add(objFunction);
                    #endregion
                    break;

                case WorkFlowConstants.FUNC_HEAD_TEACHER:
                    #region 10. Phan cong chu nhiem
                    var iqClassHeadTeacher = from cp in ClassProfileBusiness.SearchByAcademicYear(objAcademicYear.AcademicYearID, searchInfo).Where(c => lstEducationLevelId.Contains(c.EducationLevelID))
                                             select new { cp.ClassProfileID, cp.HeadTeacherID } into x
                                             group x by new { x.ClassProfileID } into g
                                             select new
                                             {
                                                 g.Key.ClassProfileID,
                                                 Count = g.Count(c => c.HeadTeacherID > 0)
                                             };
                    var lstClassHeadTeacher = iqClassHeadTeacher.ToList();
                    //So lop chua co giao vien CN
                    int countClassNotHeaderTeacher = (lstClassHeadTeacher == null || lstClassHeadTeacher.Count == 0) ? 0 : lstClassHeadTeacher.Count(c => c.Count == 0);
                    if (lstClassHeadTeacher == null || lstClassHeadTeacher.Count == 0)//Chua khai bao lop hoc
                    {
                        objFunction.StatusName = String.Format("{0}Chưa khai báo lớp học{1}", WorkFlowConstants.HTML_SPAN_COLOR_ORANGE, WorkFlowConstants.HTML_END_SPAN);
                        objFunction.Contents = "Chưa khai báo lớp học";
                    }
                    else if (countClassNotHeaderTeacher == lstClassHeadTeacher.Count)//Tat ca cac lop chua khai bao giao vien CN
                    {
                        objFunction.StatusName = WorkFlowConstants.STATUS_NOT_EXECUTE;
                        objFunction.Contents = "Chưa có lớp nào được phân công chủ nhiệm";
                    }
                    else if (countClassNotHeaderTeacher > 0)//tồn tại lớp chưa khai báo giáo viên chủ nhiệm
                    {
                        objFunction.StatusName = WorkFlowConstants.STATUS_NOT_FULL_EXECUTE;
                        objFunction.Contents = String.Format("{0}{1}/{2}{3} lớp đã được phân công chủ nhiệm", WorkFlowConstants.HTML_SPAN_COLOR_ORANGE, lstClassHeadTeacher.Count(c => c.Count > 0), lstClassHeadTeacher.Count, WorkFlowConstants.HTML_END_SPAN);
                    }
                    else //Tất cả các lớp của cấp học đã khai báo giáo viên chủ nhiệm
                    {
                        objFunction.StatusName = WorkFlowConstants.STATUS_EXECUTED;
                        objFunction.Contents = String.Format("{0}{1}/{2}{3} lớp đã được phân công chủ nhiệm", WorkFlowConstants.HTML_SPAN_COLOR_ORANGE, lstClassHeadTeacher.Count(c => c.Count > 0), lstClassHeadTeacher.Count, WorkFlowConstants.HTML_END_SPAN);
                    }
                    lstVal.Add(objFunction);
                    #endregion
                    break;

                case WorkFlowConstants.FUNC_MARK_CONFIG:
                    #region 11. Cau hinh so con diem
                    objFunction.StatusName = "";
                    objFunction.Contents = objWorkFlow.WorkFlowContent;
                    lstVal.Add(objFunction);
                    #endregion
                    break;
                case WorkFlowConstants.FUNC_MARK_PERIOD_CONFIG:
                    #region 12. Cau hinh so con diem theo dot
                    objFunction.StatusName = "";
                    objFunction.Contents = objWorkFlow.WorkFlowContent;
                    lstVal.Add(objFunction);
                    #endregion
                    break;

                case WorkFlowConstants.FUNC_COMMON_CONFIG:
                    #region 13. Cau hinh chung
                    objFunction.StatusName = "";
                    objFunction.Contents = appliedLevel == GlobalConstants.APPLIED_LEVEL_PRIMARY ? "Cho phép cấu hình xem thông tin, cấu hình khóa dữ liệu, cấu hình sinh mã tự động,…" : objWorkFlow.WorkFlowContent;

                    lstVal.Add(objFunction);
                    #endregion
                    break;
            }
        }
    }
}