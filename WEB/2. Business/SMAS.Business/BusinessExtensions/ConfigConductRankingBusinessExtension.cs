/**
 * The Viettel License
 *
 * Copyright 2012 Viettel Telecom. All rights reserved.
 * VIETTEL areaPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */

/** 
* @author  
* @version $Revision: $
*/

using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Linq.Expressions;
using System.Web;
using log4net;

using SMAS.Business.BusinessObject;
using SMAS.DAL.Repository;
using SMAS.Models.Models;
using SMAS.Business.Common;
using System.Transactions;
using System.IO;
using SMAS.VTUtils.Excel.Export;
using SMAS.VTUtils.Log;

namespace SMAS.Business.Business
{
    /// <summary>
    /// 
    /// </summary>
    public partial class ConfigConductRankingBusiness
    {
        public IQueryable<ConfigConductRanking> GetAllConfigConductRanking(IDictionary<string, object> dic)
        {
            int schoolID = Utils.GetInt(dic, "SchoolID");
            int academicYearID = Utils.GetInt(dic, "AcademicYearID");
            int classID = Utils.GetInt(dic, "ClassID");
            int educationLevelID = Utils.GetInt(dic, "EducationLevelID");
            int semesterID = Utils.GetInt(dic, "SemesterID");
            int periodID = Utils.GetInt(dic, "PeriodID");
            IQueryable<ConfigConductRanking> iqConfigConductRanking = this.All;
            if (schoolID > 0)
            {
                iqConfigConductRanking = iqConfigConductRanking.Where(p => p.SchoolID == schoolID);
            }

            if (academicYearID > 0)
            {
                iqConfigConductRanking = iqConfigConductRanking.Where(p => p.AcademicYearID == academicYearID);
            }

            if (classID > 0)
            {
                iqConfigConductRanking = iqConfigConductRanking.Where(p => p.ClassID == classID);
            }

            if (educationLevelID > 0)
            {
                iqConfigConductRanking = iqConfigConductRanking.Where(p => p.EducationLevelID == educationLevelID);
            }

            if (semesterID > 0)
            {
                iqConfigConductRanking = iqConfigConductRanking.Where(p => p.SemesterID == semesterID);
            }

            if (periodID > 0)
            {
                iqConfigConductRanking = iqConfigConductRanking.Where(p => p.PeriodID == periodID);
            }

            return iqConfigConductRanking;
        }

        public void Insert(IDictionary<string, object> dic, List<ConfigConductRanking> lstInsert)
        {
            try
            {

                if (lstInsert == null || lstInsert.Count == 0)
                {
                    return;
                }
                context.Configuration.AutoDetectChangesEnabled = false;
                context.Configuration.ValidateOnSaveEnabled = false;
                //kiem tra chi insert nhung gia tri chua co trong ConfigConductRanking
                List<ConfigConductRanking> lstCCR = GetAllConfigConductRanking(dic).ToList();

                //khoi tao list them
                List<ConfigConductRanking> lstAdd = new List<ConfigConductRanking>();
                ConfigConductRanking objCCR = null;
                int conductSPID = Utils.GetInt(dic, "conductSPID");
                if (lstInsert != null && lstInsert.Count > 0)
                {
                    foreach (var item in lstInsert)
                    {
                        if (conductSPID == 0)
                        {
                            objCCR = lstCCR.Where(p => p.EducationLevelID == item.EducationLevelID
                                                && p.SemesterID == item.SemesterID
                                                && p.ClassID == item.ClassID).FirstOrDefault();
                        }
                        else
                        {
                            objCCR = lstCCR.Where(p => p.EducationLevelID == item.EducationLevelID
                                                && p.PeriodID == item.PeriodID
                                                && p.ClassID == item.ClassID).FirstOrDefault();
                        }
                        if (objCCR == null)
                        {
                            lstAdd.Add(item);
                        }
                    }
                }

                //add du lieu
                if (lstAdd != null && lstAdd.Count > 0)
                {
                    foreach (ConfigConductRanking item in lstAdd)
                    {
                        this.Insert(item);
                    }
                    this.Save();
                }
            }
            catch (Exception ex)
            {
                
                LogExtensions.ErrorExt(logger, DateTime.Now, "Insert", "null", ex);
            }
            finally
            {
                context.Configuration.AutoDetectChangesEnabled = true;
                context.Configuration.ValidateOnSaveEnabled = true;
            }
        }

        public void Delete(IDictionary<string, object> dic, List<ConfigConductRanking> lstDelete)
        {
            try
            {

                if (lstDelete == null || lstDelete.Count == 0)
                {
                    return;
                }
                context.Configuration.AutoDetectChangesEnabled = false;
                context.Configuration.ValidateOnSaveEnabled = false;
                //kiem tra chi insert nhung gia tri chua co trong ConfigConductRanking
                List<ConfigConductRanking> lstCCR = GetAllConfigConductRanking(dic).ToList();

                //khoi tao list them
                List<ConfigConductRanking> lstDel = new List<ConfigConductRanking>();
                ConfigConductRanking objCCR = null;
                int conductSPID = Utils.GetInt(dic, "conductSPID");
                if (lstDelete != null && lstDelete.Count > 0)
                {
                    foreach (var item in lstDelete)
                    {
                        if (conductSPID == 0)
                        {
                            objCCR = lstCCR.Where(p => p.EducationLevelID == item.EducationLevelID
                                                && p.SemesterID == item.SemesterID
                                                && p.ClassID == item.ClassID).FirstOrDefault();
                        }
                        else
                        {
                            objCCR = lstCCR.Where(p => p.EducationLevelID == item.EducationLevelID
                                                && p.PeriodID == item.PeriodID
                                                && p.ClassID == item.ClassID).FirstOrDefault();
                        }
                        if (objCCR != null)
                        {
                            lstDel.Add(objCCR);
                        }
                    }
                }

                //add du lieu
                if (lstDel != null && lstDel.Count > 0)
                {
                    ConfigConductRankingBusiness.DeleteAll(lstDel);
                    ConfigConductRankingBusiness.Save();
                }
            }
            catch (Exception ex)
            {
                
                LogExtensions.ErrorExt(logger, DateTime.Now, "Delete", "null", ex);
            }
            finally
            {
                context.Configuration.AutoDetectChangesEnabled = true;
                context.Configuration.ValidateOnSaveEnabled = true;
            }

        }
    }
}
