﻿
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Linq.Expressions;
using System.Web;
using log4net;

using SMAS.DAL.Repository;
using SMAS.DAL.IRepository;
using SMAS.Business.IBusiness;
using SMAS.Models.Models;
using SMAS.Business.Common;
using SMAS.Business.BusinessObject;
using System.Transactions;

namespace SMAS.Business.Business
{
    public partial class AlertMessageDetailBusiness
    {
        //tanla4
        //13/11/2014

        public IQueryable<AlertMessageDetail> Search(IDictionary<string, object> dic)
        {           
            int userID = Utils.GetInt(dic, "UserID");

            IQueryable<AlertMessageDetail> lstAlertMessageDetail = this.AlertMessageDetailRepository.All.Where(o => o.UserID == userID);

            return lstAlertMessageDetail;
        }

        public void Insert(AlertMessageDetail alertMessageDetail)
        {
            //Check Require, max length
            ValidationMetadata.ValidateObject(alertMessageDetail);           
                       
            base.Insert(alertMessageDetail);
        }
    }
}
