/**
 * The Viettel License
 *
 * Copyright 2012 Viettel Telecom. All rights reserved.
 * VIETTEL PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */

/** 
* @author  
* @version $Revision: $
*/

using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Linq.Expressions;
using System.Web;
using log4net;

using SMAS.DAL.Repository;
using SMAS.DAL.IRepository;
using SMAS.Business.IBusiness;
using SMAS.Models.Models;
using SMAS.Business.Common;

namespace SMAS.Business.Business
{ 
    public partial class EvaluationConfigBusiness
    {

        #region Search


        /// <summary>    
        /// </summary>
        /// <author>trangdd</author>
        /// <date>06/11/2012</date>    
        /// <returns>IQueryable<Ethnic></returns>
        public IQueryable<EvaluationConfig> Search(IDictionary<string, object> dic)
        {
            string EvaluationConfigName = Utils.GetString(dic, "EvaluationConfigName");            
            int TypeConfigID = Utils.GetInt(dic, "TypeConfigID");            
            bool? IsActive = Utils.GetIsActive(dic, "IsActive");
            IQueryable<EvaluationConfig> Query = EvaluationConfigRepository.All;
            if (IsActive.HasValue)
            {
                Query = Query.Where(cfh => cfh.IsActive == IsActive);
            }
            if (EvaluationConfigName.Trim().Length != 0)
            {
                Query = Query.Where(cfh => cfh.EvaluationConfigName.Contains(EvaluationConfigName));
            }
            if (TypeConfigID != 0)
            {
                Query = Query.Where(cfh => cfh.TypeConfigID == TypeConfigID);
            }

            return Query;

        }
        #endregion
    }
}