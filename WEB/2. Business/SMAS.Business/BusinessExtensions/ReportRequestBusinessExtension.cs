/**
 * The Viettel License
 *
 * Copyright 2012 Viettel Telecom. All rights reserved.
 * VIETTEL PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */

/** 
* @author  
* @version $Revision: $
*/

using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Linq.Expressions;
using System.Web;
using log4net;
using SMAS.DAL.Repository;
using SMAS.DAL.IRepository;
using SMAS.Business.IBusiness;
using SMAS.Models.Models;
using SMAS.Business.Common;
using SMAS.Business.BusinessObject;
using System.Transactions;
using SMAS.VTUtils.Log;

namespace SMAS.Business.Business
{
    public partial class ReportRequestBusiness
    {
        public IQueryable<ReportRequest> Search(IDictionary<string, object> search)
        {
            int reportRequestID = Utils.GetInt(search, "ReportRequestID");
            int schoolID = Utils.GetInt(search, "SchoolID");
            int provinceID = Utils.GetInt(search, "ProvinceID");
            int districtID = Utils.GetInt(search, "DistrictID");
            int semesterID = Utils.GetInt(search, "SemesterID");
            int academicYearID = Utils.GetInt(search, "AcademicYearID");
            int year = Utils.GetInt(search, "Year");
            int educationGrade = Utils.GetInt(search, "AppliedLevel");
            bool status = Utils.GetBool(search, "Status", true);

            IQueryable<ReportRequest> query = this.All.Where(o => o.Status == status);

            if (reportRequestID > 0)
            {
                query = query.Where(o => o.ReportRequestID == reportRequestID);
            }
            if (schoolID > 0)
            {
                query = query.Where(o => o.SchoolID == schoolID);
            }
            if (provinceID > 0)
            {
                query = query.Where(o => o.ProvinceID == provinceID);
            }
            if (districtID > 0)
            {
                query = query.Where(o => o.DistrictID == districtID);
            }
            if (semesterID > 0)
            {
                query = query.Where(o => o.SemesterID == semesterID);
            }
            if (academicYearID > 0)
            {
                query = query.Where(o => o.AcademicYearID == academicYearID);
            }
            if (year > 0)
            {
                query = query.Where(o => o.Year == year);
            }
            if (educationGrade > 0)
            {
                query = query.Where(o => o.EducationGrade == educationGrade);
            }

            return query;
        }

        public void InsertListReportRequest(IQueryable<ReportRequestBO> list)
        {
            try
            {
                context.Configuration.AutoDetectChangesEnabled = false;
                context.Configuration.ValidateOnSaveEnabled = false;
                List<ReportRequestBO> listRequest = list.ToList();
                List<ReportRequest> listInsert = new List<ReportRequest>();
                List<ReportRequest> listUpdate = new List<ReportRequest>();
                ReportRequest insert = null;
                ReportRequestBO temp = null;

                for (int i = 0; i < listRequest.Count; i++)
                {
                    temp = listRequest[i];

                    insert = (from rr in ReportRequestBusiness.All.Where(o => o.SchoolID == temp.SchoolID && o.SemesterID == temp.SemesterID
                        && o.ProvinceID == temp.ProvinceID && o.DistrictID == temp.DistrictID && o.EducationGrade == temp.EducationGrade
                        && o.AcademicYearID == temp.AcademicYearID && o.Year == temp.Year)
                              select rr).FirstOrDefault();

                    if (insert == null)
                    {
                        insert = new ReportRequest();
                        insert.ReportRequestID = ReportRequestBusiness.GetNextSeq<int>();
                        insert.SchoolID = temp.SchoolID;
                        insert.ProvinceID = temp.ProvinceID;
                        insert.DistrictID = temp.DistrictID;
                        insert.SemesterID = temp.SemesterID;
                        insert.EducationGrade = temp.EducationGrade;
                        insert.AcademicYearID = temp.AcademicYearID;
                        insert.Year = temp.Year;
                        insert.CreateTime = temp.CreateTime;
                        insert.Status = temp.Status;
                        listInsert.Add(insert);
                    }
                    else
                    {
                        insert.UpdateTime = DateTime.Now;
                        insert.Status = false;
                        listUpdate.Add(insert);
                    }
                }

                if (listInsert != null && listInsert.Count > 0)
                {
                    for (int i = 0; i < listInsert.Count; i++)
                    {
                        ReportRequestBusiness.Insert(listInsert[i]);
                    }
                }

                if (listUpdate != null && listUpdate.Count > 0)
                {
                    for (int i = 0; i < listUpdate.Count; i++)
                    {
                        ReportRequestBusiness.Update(listUpdate[i]);
                    }
                }
                ReportRequestBusiness.Save();
            }
            catch (Exception ex)
            {
                
                LogExtensions.ErrorExt(logger, DateTime.Now, "InsertListReportRequest", "null", ex);
            }
            finally
            {
                context.Configuration.AutoDetectChangesEnabled = true;
                context.Configuration.ValidateOnSaveEnabled = true;
            }
        }

        private IDictionary<string, object> ColumnMappings()
        {
            Dictionary<string, object> columnMap = new Dictionary<string, object>();
            columnMap.Add("ReportRequestID", "REPORT_REQUEST_ID");
            columnMap.Add("SchoolID", "SCHOOL_ID");
            columnMap.Add("ProvinceID", "PROVINCE_ID");
            columnMap.Add("DistrictID", "DISTRICT_ID");
            columnMap.Add("SemesterID", "SEMESTER_ID");
            columnMap.Add("AcademicYearID", "ACADEMIC_YEAR_ID");
            columnMap.Add("EducationGrade", "EDUCATION_GRADE");
            columnMap.Add("Year", "YEAR");
            columnMap.Add("CreateTime", "CREATE_TIME");
            columnMap.Add("UpdateTime", "UPDATE_TIME");
            columnMap.Add("Status", "STATUS");
            return columnMap;
        }
    }
}
