﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Linq.Expressions;
using System.Web;
using log4net;

using SMAS.DAL.Repository;
using SMAS.DAL.IRepository;
using SMAS.Business.IBusiness;
using SMAS.Models.Models;
using SMAS.Business.BusinessObject;
using SMAS.Business.Common;
using System.IO;
using SMAS.VTUtils.Excel.Export;
using SMAS.Business.Business;

namespace SMAS.Business.Business
{
    public class StatisticOfTertiaryStartYearBusiness : GenericBussiness<ProcessedReport>, IStatisticOfTertiaryStartYearBusiness
    {
        IProcessedReportParameterRepository ProcessedReportParameterRepository;
        IProcessedReportRepository ProcessedReportRepository;
        IEducationLevelRepository EducationLevelRepository;
        IAcademicYearRepository AcademicYearRepository;
        ISchoolProfileRepository SchoolProfileRepository;
        public StatisticOfTertiaryStartYearBusiness(ILog logger, SMASEntities context = null)
            : base(logger)
        {
            if (context == null) { context = new SMASEntities(); } this.context = context;
            
            this.ProcessedReportParameterRepository = new ProcessedReportParameterRepository(context);
            this.ProcessedReportRepository = new ProcessedReportRepository(context);
            this.EducationLevelRepository = new EducationLevelRepository(context);
            this.AcademicYearRepository = new AcademicYearRepository(context);
            this.SchoolProfileRepository = new SchoolProfileRepository(context);
        }
        public string GetHashKey(StatisticOfTertiaryStartYearBO entity)
        {
            IDictionary<string, object> dic = new Dictionary<string, object> {
                {"AcademicYearID", entity.AcademicYearID},
                {"SchoolID", entity.SchoolID},
            };
            return ReportUtils.GetHashKey(dic);
        }
        
        public ProcessedReport InsertStatisticOfTertiaryStartYear(StatisticOfTertiaryStartYearBO entity, Stream data)
        {

            string reportCode = SystemParamsInFile.REPORT_THPT_DAU_NAM;

            ReportDefinition reportDef = ReportDefinitionBusiness.GetByCode(reportCode);
            ProcessedReport pr = new ProcessedReport();
            pr.ReportCode = reportCode;
            pr.ProcessedDate = DateTime.Now;
            pr.InputParameterHashKey = GetHashKey(entity);
            pr.ReportData = ReportUtils.Compress(data);
            string outputNamePattern = reportDef.OutputNamePattern;
            pr.ReportName = ReportUtils.RemoveSpecialCharacters(outputNamePattern) + "." + reportDef.OutputFormat;

            IDictionary<string, object> dic = new Dictionary<string, object> {
                {"AcademicYearID", entity.AcademicYearID},
                {"SchoolID", entity.SchoolID}
            };
            ProcessedReportParameterRepository.Insert(dic, pr);
            ProcessedReportRepository.Insert(pr);
            ProcessedReportRepository.Save();
            return pr;
        }
       
    }
}
