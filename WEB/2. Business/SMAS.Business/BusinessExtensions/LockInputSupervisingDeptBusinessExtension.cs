﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Linq.Expressions;
using System.Web;
using log4net;

using SMAS.DAL.Repository;
using SMAS.DAL.IRepository;
using SMAS.Business.IBusiness;
using SMAS.Models.Models;
using SMAS.Business.BusinessObject;
using SMAS.Business.Common;

namespace SMAS.Business.Business
{
    public partial class LockInputSupervisingDeptBusiness
    {
        public void Insert(List<LockInputSupervisingDept> lstViewModel, int SupervisingDeptID)
        {
            List<LockInputSupervisingDept> lstDB = LockInputSupervisingDeptBusiness.All.ToList();
            LockInputSupervisingDept objDB = null;
            for (int item = 0; item<lstViewModel.Count(); item++)
            {
                objDB = lstDB.Where(x => x.SchoolID == lstViewModel[item].SchoolID && x.AcademicYearID == lstViewModel[item].AcademicYearID ).FirstOrDefault();
                if(objDB == null)
                {
                    objDB = new LockInputSupervisingDept() { 
                        AcademicYearID = lstViewModel[item].AcademicYearID,
                        AppliedLevelID = lstViewModel[item].AppliedLevelID,
                        SchoolID = lstViewModel[item].SchoolID,
                        LockTitle = lstViewModel[item].LockTitle,
                        UserLock1ID = lstViewModel[item].UserLock1ID,
                        UserLock2ID = lstViewModel[item].UserLock2ID,
                        CreateDate = DateTime.Now
                    };
                    this.Insert(objDB);
                }
                else if (objDB != null)
                {
                    #region xet truong hop Update
                    if (objDB.LockTitle == "HK1")
                    {
                        // khoa them HK2
                        if (lstViewModel[item].LockTitle == "HK1,HK2") {
                            objDB.AppliedLevelID = lstViewModel[item].AppliedLevelID;
                            objDB.LockTitle = lstViewModel[item].LockTitle;
                            objDB.UserLock2ID = SupervisingDeptID;
                            objDB.CreateDate = DateTime.Now;
                            
                            this.Update(objDB);
                        }
                            //mo HK1 khoa HK2
                        else if (lstViewModel[item].LockTitle == "HK2") {

                                objDB.AppliedLevelID = lstViewModel[item].AppliedLevelID;
                                objDB.LockTitle = lstViewModel[item].LockTitle;
                                objDB.UserUnlock1ID = SupervisingDeptID;
                                objDB.ModifiedLock1Date = DateTime.Now;
                                objDB.UserLock2ID = SupervisingDeptID;
                                objDB.CreateDate = DateTime.Now;
                            this.Update(objDB);
                        }
                        //Mo khoa HK1
                        else if (lstViewModel[item].LockTitle == "" || lstViewModel[item].LockTitle == null) 
                        {
                            objDB.AppliedLevelID = lstViewModel[item].AppliedLevelID;
                                objDB.LockTitle = lstViewModel[item].LockTitle;
                                objDB.UserUnlock1ID = SupervisingDeptID;
                                objDB.ModifiedLock1Date = DateTime.Now;

                            this.Update(objDB);
                        }
                        else if (lstViewModel[item].AppliedLevelID != objDB.AppliedLevelID) {
                            objDB.AppliedLevelID = lstViewModel[item].AppliedLevelID;
                            this.Update(objDB);
                        }
                    }
                    else if (objDB.LockTitle == "HK2")
                    {
                        // khoa HK1 mo HK2
                        if (lstViewModel[item].LockTitle == "HK1") 
                        {
                            objDB.AppliedLevelID = lstViewModel[item].AppliedLevelID;
                            objDB.LockTitle = lstViewModel[item].LockTitle;
                            objDB.UserLock1ID = SupervisingDeptID;
                            objDB.CreateDate = DateTime.Now;
                            objDB.UserUnlock2ID = SupervisingDeptID;
                            objDB.ModifiedLock2Date = DateTime.Now;

                            this.Update(objDB);
                        }
                        //khoa them HK1
                        else if (lstViewModel[item].LockTitle == "HK1,HK2")
                        {

                            objDB.AppliedLevelID = lstViewModel[item].AppliedLevelID;
                                objDB.LockTitle = lstViewModel[item].LockTitle;
                                objDB.UserLock1ID = SupervisingDeptID;
                                objDB.CreateDate = DateTime.Now;

                            this.Update(objDB);
                        }
                        // mo khoa HK2
                        else if (lstViewModel[item].LockTitle == "" || lstViewModel[item].LockTitle == null)
                        {
                            objDB.AppliedLevelID = lstViewModel[item].AppliedLevelID;
                                objDB.LockTitle = lstViewModel[item].LockTitle;
                                objDB.UserUnlock2ID = SupervisingDeptID;
                                objDB.ModifiedLock2Date = DateTime.Now;                             

                            this.Update(objDB);
                        }                  
                        else if (lstViewModel[item].AppliedLevelID != objDB.AppliedLevelID)
                        {
                            objDB.AppliedLevelID = lstViewModel[item].AppliedLevelID;
                            this.Update(objDB);
                        }
                    }
                    else if (objDB.LockTitle == "HK1,HK2")
                    {
                        //mo khoa HK2
                        if (lstViewModel[item].LockTitle == "HK1") {
                            objDB.AppliedLevelID = lstViewModel[item].AppliedLevelID;
                                objDB.LockTitle = lstViewModel[item].LockTitle;
                                objDB.UserUnlock2ID = SupervisingDeptID;
                                objDB.ModifiedLock2Date = DateTime.Now;

                            this.Update(objDB);
                        }
                        //mo khoa HK1
                        else if (lstViewModel[item].LockTitle == "HK2") {
                            objDB.AppliedLevelID = lstViewModel[item].AppliedLevelID;
                                objDB.LockTitle = lstViewModel[item].LockTitle;
                                objDB.UserUnlock1ID = SupervisingDeptID;
                                objDB.ModifiedLock1Date = DateTime.Now;

                            this.Update(objDB);
                        }
                        // mo khoa tat ca
                        else if (lstViewModel[item].LockTitle == "" || lstViewModel[item].LockTitle == null)
                        {
                            objDB.AppliedLevelID = lstViewModel[item].AppliedLevelID;
                                objDB.LockTitle = lstViewModel[item].LockTitle;
                                objDB.UserUnlock1ID = SupervisingDeptID;
                                objDB.ModifiedLock1Date = DateTime.Now;
                                objDB.UserUnlock2ID = SupervisingDeptID;
                                objDB.ModifiedLock2Date = DateTime.Now;
                            this.Update(objDB);
                        }
                        else if (lstViewModel[item].AppliedLevelID != objDB.AppliedLevelID)
                        {
                            objDB.AppliedLevelID = lstViewModel[item].AppliedLevelID;
                            this.Update(objDB);
                        }
                    }

                    else if (objDB.LockTitle == "" || objDB.LockTitle == null) 
                    {
                        // khoa HK1 sau khi da mo khoa HK1
                        if (lstViewModel[item].LockTitle == "HK1") 
                        {
                            objDB.AppliedLevelID = lstViewModel[item].AppliedLevelID;
                                objDB.LockTitle = lstViewModel[item].LockTitle;
                                objDB.CreateDate = DateTime.Now;
                                objDB.UserLock1ID = SupervisingDeptID;
 
                            this.Update(objDB);
                        }

                        // khoa HK2 sau khi da mo khoa HK2
                        else if (lstViewModel[item].LockTitle == "HK2")
                        {
                            objDB.AppliedLevelID = lstViewModel[item].AppliedLevelID;
                                objDB.LockTitle = lstViewModel[item].LockTitle;
                                objDB.CreateDate = DateTime.Now;
                                objDB.UserLock2ID = SupervisingDeptID;

                            this.Update(objDB);
                        }
                        else if (lstViewModel[item].LockTitle == "HK1,HK2")
                        {
                            objDB.AppliedLevelID = lstViewModel[item].AppliedLevelID;
                                objDB.LockTitle = lstViewModel[item].LockTitle;
                                objDB.CreateDate = DateTime.Now;
                                objDB.UserLock2ID = SupervisingDeptID;
                                objDB.UserLock1ID = SupervisingDeptID;
                            
                            this.Update(objDB);
                        }
                        else if (lstViewModel[item].AppliedLevelID != objDB.AppliedLevelID)
                        {
                            objDB.AppliedLevelID = lstViewModel[item].AppliedLevelID;
                            this.Update(objDB);
                        }
                    }

                    #endregion

                }

            }

            this.Save();
        }

        public void InsertTemp(List<LockInputSupervisingDept> lstViewModel, int SupervisingDeptID) 
        { 
            List<LockInputSupervisingDept> lstDB = LockInputSupervisingDeptBusiness.All.ToList();
            LockInputSupervisingDept objDB = null;
            for (int item = 0; item < lstViewModel.Count(); item++)
            {
                objDB = lstDB.Where(x => x.SchoolID == lstViewModel[item].SchoolID && 
                                        x.AcademicYearID == lstViewModel[item].AcademicYearID &&
                                        x.AppliedLevelID == lstViewModel[item].AppliedLevelID).FirstOrDefault();
                if (objDB == null)
                {
                    objDB = new LockInputSupervisingDept()
                    {
                        AcademicYearID = lstViewModel[item].AcademicYearID,
                        AppliedLevelID = lstViewModel[item].AppliedLevelID,
                        SchoolID = lstViewModel[item].SchoolID,
                        LockTitle = lstViewModel[item].LockTitle,
                        UserLock1ID = lstViewModel[item].UserLock1ID,
                        UserLock2ID = lstViewModel[item].UserLock2ID,
                        CreateDate = DateTime.Now
                    };
                    this.Insert(objDB);
                }
                else if (objDB != null)
                {
                    // Trong DATABASE cap 1 dang la HK1
                    if (objDB.LockTitle == "HK1")
                    {
                        // khoa them HK2
                        if (lstViewModel[item].LockTitle == "HK1,HK2")
                        {
                            objDB.AppliedLevelID = lstViewModel[item].AppliedLevelID;
                            objDB.LockTitle = lstViewModel[item].LockTitle;
                            objDB.UserLock2ID = SupervisingDeptID;
                            objDB.CreateDate = DateTime.Now;

                            this.Update(objDB);
                        }
                        //mo HK1 khoa HK2
                        else if (lstViewModel[item].LockTitle == "HK2")
                        {

                            objDB.AppliedLevelID = lstViewModel[item].AppliedLevelID;
                            objDB.LockTitle = lstViewModel[item].LockTitle;
                            objDB.UserUnlock1ID = SupervisingDeptID;
                            objDB.ModifiedLock1Date = DateTime.Now;
                            objDB.UserLock2ID = SupervisingDeptID;
                            objDB.CreateDate = DateTime.Now;

                            this.Update(objDB);
                        }
                        //Mo khoa HK1
                        else if (lstViewModel[item].LockTitle == "" || lstViewModel[item].LockTitle == null)
                        {
                            objDB.AppliedLevelID = lstViewModel[item].AppliedLevelID;
                            objDB.LockTitle = lstViewModel[item].LockTitle;
                            objDB.UserUnlock1ID = SupervisingDeptID;
                            objDB.ModifiedLock1Date = DateTime.Now;

                            this.Update(objDB);
                        }                       
                    }

                    // Trong DATABASE cap 1 dang la HK2
                    else if (objDB.LockTitle == "HK2")
                    {
                        // khoa HK1 mo HK2
                        if (lstViewModel[item].LockTitle == "HK1")
                        {
                            objDB.AppliedLevelID = lstViewModel[item].AppliedLevelID;
                            objDB.LockTitle = lstViewModel[item].LockTitle;
                            objDB.UserLock1ID = SupervisingDeptID;
                            objDB.CreateDate = DateTime.Now;
                            objDB.UserUnlock2ID = SupervisingDeptID;
                            objDB.ModifiedLock2Date = DateTime.Now;

                            this.Update(objDB);
                        }
                        //khoa them HK1
                        else if (lstViewModel[item].LockTitle == "HK1,HK2")
                        {

                            objDB.AppliedLevelID = lstViewModel[item].AppliedLevelID;
                            objDB.LockTitle = lstViewModel[item].LockTitle;
                                objDB.CreateDate = DateTime.Now;
                            objDB.UserLock1ID = SupervisingDeptID;

                            this.Update(objDB);
                        }
                        // mo khoa HK2
                        else if (lstViewModel[item].LockTitle == "" || lstViewModel[item].LockTitle == null)
                        {
                            objDB.AppliedLevelID = lstViewModel[item].AppliedLevelID;
                            objDB.LockTitle = lstViewModel[item].LockTitle;
                            objDB.UserUnlock2ID = SupervisingDeptID;
                            objDB.ModifiedLock2Date = DateTime.Now;

                            this.Update(objDB);
                        }
                    }

                    // Trong DATABASE cap 1 dang la HK1,HK2
                    else if (objDB.LockTitle == "HK1,HK2")
                    {
                        //mo khoa HK2
                        if (lstViewModel[item].LockTitle == "HK1")
                        {
                            objDB.AppliedLevelID = lstViewModel[item].AppliedLevelID;
                            objDB.LockTitle = lstViewModel[item].LockTitle;
                            objDB.UserUnlock2ID = SupervisingDeptID;
                            objDB.ModifiedLock2Date = DateTime.Now;

                            this.Update(objDB);
                        }
                        //mo khoa HK1
                        else if (lstViewModel[item].LockTitle == "HK2")
                        {
                            objDB.AppliedLevelID = lstViewModel[item].AppliedLevelID;
                            objDB.LockTitle = lstViewModel[item].LockTitle;
                            objDB.UserUnlock1ID = SupervisingDeptID;
                            objDB.ModifiedLock1Date = DateTime.Now;

                            this.Update(objDB);
                        }
                        // mo khoa tat ca
                        else if (lstViewModel[item].LockTitle == "" || lstViewModel[item].LockTitle == null)
                        {
                            objDB.AppliedLevelID = lstViewModel[item].AppliedLevelID;
                            objDB.LockTitle = lstViewModel[item].LockTitle;
                            objDB.UserUnlock1ID = SupervisingDeptID;
                            objDB.ModifiedLock1Date = DateTime.Now;
                            objDB.UserUnlock2ID = SupervisingDeptID;
                            objDB.ModifiedLock2Date = DateTime.Now;
                            this.Update(objDB);
                        }
                    }
                     
                    // Trong DATABASE cap 1 dang la "" hoac null
                    else if (objDB.LockTitle == "" || objDB.LockTitle == null)
                    {
                        // khoa HK1 sau khi da mo khoa HK1
                        if (lstViewModel[item].LockTitle == "HK1")
                        {
                            objDB.AppliedLevelID = lstViewModel[item].AppliedLevelID;
                            objDB.LockTitle = lstViewModel[item].LockTitle;
                            objDB.CreateDate = DateTime.Now;
                            objDB.UserLock1ID = SupervisingDeptID;

                            this.Update(objDB);
                        }

                        // khoa HK2 sau khi da mo khoa HK2
                        else if (lstViewModel[item].LockTitle == "HK2")
                        {
                            objDB.AppliedLevelID = lstViewModel[item].AppliedLevelID;
                            objDB.LockTitle = lstViewModel[item].LockTitle;
                            objDB.CreateDate = DateTime.Now;
                            objDB.UserLock2ID = SupervisingDeptID;

                            this.Update(objDB);
                        }
                        else if (lstViewModel[item].LockTitle == "HK1,HK2")
                        {
                            objDB.AppliedLevelID = lstViewModel[item].AppliedLevelID;
                            objDB.LockTitle = lstViewModel[item].LockTitle;
                            objDB.CreateDate = DateTime.Now;
                            objDB.UserLock2ID = SupervisingDeptID;
                            objDB.UserLock1ID = SupervisingDeptID;

                            this.Update(objDB);
                        }                      
                    }               
                }
            }
        }

        private List<int> GetListIDFromString(string str)
        {
            string[] idArr;
            if (str != "")
            {
                idArr = str.Split(',');
            }
            else
            {
                idArr = new string[] { };
            }
            List<int> lstID = idArr.Length > 0 ? idArr.ToList().Distinct().Select(o => Convert.ToInt32(o)).ToList() :
                                            new List<int>();

            return lstID;
        }

        public void InsertTempAll(List<LockInputSupervisingDept> lstViewModel, int SupervisingDeptID, string AppliedString, string chkHK1, string chkHK2)
        {
            List<LockInputSupervisingDept> lstDB = LockInputSupervisingDeptBusiness.All.ToList();
            LockInputSupervisingDept objDB = null;
            int checkSuper = GetHierachyLevelIDByUserAccountID(SupervisingDeptID);
            if (AppliedString == "")
            {
                string Nullstring = "";
            }
            else
            {
                for (int item = 0; item < lstViewModel.Count(); item++)
                {
                    string CheckAppliedString = AppliedString;
                    #region xet applied
                    // neu grade = 7 hoac 31
                    #region 7 || 31
                    if (lstViewModel[item].AppliedLevelID == 7 || lstViewModel[item].AppliedLevelID == 31) 
                    {
                        if (AppliedString == "1,2,3")
                        {
                            CheckAppliedString = "1,2,3";
                        }
                        else if (AppliedString == "1,2")
                        {
                            CheckAppliedString = "1,2";
                        }
                        else if (AppliedString == "2,3")
                        {
                            CheckAppliedString = "2,3";
                        }
                        else if (AppliedString == "1,3")
                        {
                            CheckAppliedString = "1,3";
                        }
                        else if (AppliedString == "1")
                        {
                            CheckAppliedString = "1";
                        }
                        else if (AppliedString == "2")
                        {
                            CheckAppliedString = "2";
                        }
                        else if (AppliedString == "3")
                        {
                            CheckAppliedString = "3";
                        }
                    }
                    #endregion

                    // neu grade = 6 => 2,3
                    #region 6
                    if (lstViewModel[item].AppliedLevelID == 6)
                    {
                        if (AppliedString == "1,2,3")
                        {
                            CheckAppliedString = "2,3";
                        }
                        else if (AppliedString == "1,2")
                        {
                            CheckAppliedString = "2";
                        }
                        else if (AppliedString == "2,3")
                        {
                            CheckAppliedString = "2,3";
                        }
                        else if (AppliedString == "1,3")
                        {
                            CheckAppliedString = "3";
                        }
                        else if (AppliedString == "1")
                        {
                            CheckAppliedString = "";
                        }
                        else if (AppliedString == "2")
                        {
                            CheckAppliedString = "2";
                        }
                        else if (AppliedString == "3")
                        {
                            CheckAppliedString = "3";
                        }
                    }
                    #endregion

                    // neu grade = 3 => 1,2
                    #region 3
                    if (lstViewModel[item].AppliedLevelID == 3)
                    {
                        if (AppliedString == "1,2,3")
                        {
                            CheckAppliedString = "1,2";
                        }
                        else if (AppliedString == "1,2")
                        {
                            CheckAppliedString = "1,2";
                        }
                        else if (AppliedString == "2,3")
                        {
                            CheckAppliedString = "2";
                        }
                        else if (AppliedString == "1,3")
                        {
                            CheckAppliedString = "1";
                        }
                        else if (AppliedString == "1")
                        {
                            CheckAppliedString = "1";
                        }
                        else if (AppliedString == "2")
                        {
                            CheckAppliedString = "2";
                        }
                        else if (AppliedString == "3")
                        {
                            CheckAppliedString = "";
                        }
                    }
                    #endregion

                    // neu grade = 4 => 3
                    #region 4
                    if (lstViewModel[item].AppliedLevelID == 4)
                    {
                        if (AppliedString == "1,2,3")
                        {
                            CheckAppliedString = "3";
                        }
                        else if (AppliedString == "1,2")
                        {
                            CheckAppliedString = "";
                        }
                        else if (AppliedString == "2,3")
                        {
                            CheckAppliedString = "3";
                        }
                        else if (AppliedString == "1,3")
                        {
                            CheckAppliedString = "3";
                        }
                        else if (AppliedString == "1")
                        {
                            CheckAppliedString = "";
                        }
                        else if (AppliedString == "2")
                        {
                            CheckAppliedString = "";
                        }
                        else if (AppliedString == "3")
                        {
                            CheckAppliedString = "3";
                        }
                    }
                    #endregion

                    // neu grade = 2 => 2
                    #region 2
                    if (lstViewModel[item].AppliedLevelID == 2)
                    {
                        if (AppliedString == "1,2,3")
                        {
                            CheckAppliedString = "2";
                        }
                        else if (AppliedString == "1,2")
                        {
                            CheckAppliedString = "2";
                        }
                        else if (AppliedString == "2,3")
                        {
                            CheckAppliedString = "2";
                        }
                        else if (AppliedString == "1,3")
                        {
                            CheckAppliedString = "";
                        }
                        else if (AppliedString == "1")
                        {
                            CheckAppliedString = "";
                        }
                        else if (AppliedString == "2")
                        {
                            CheckAppliedString = "2";
                        }
                        else if (AppliedString == "3")
                        {
                            CheckAppliedString = "";
                        }
                    }
                    #endregion

                    // neu grade = 1 => 1
                    #region 1
                    if (lstViewModel[item].AppliedLevelID == 1)
                    {
                        if (AppliedString == "1,2,3")
                        {
                            CheckAppliedString = "1";
                        }
                        else if (AppliedString == "1,2")
                        {
                            CheckAppliedString = "1";
                        }
                        else if (AppliedString == "2,3")
                        {
                            CheckAppliedString = "";
                        }
                        else if (AppliedString == "1,3")
                        {
                            CheckAppliedString = "1";
                        }
                        else if (AppliedString == "1")
                        {
                            CheckAppliedString = "1";
                        }
                        else if (AppliedString == "2")
                        {
                            CheckAppliedString = "";
                        }
                        else if (AppliedString == "3")
                        {
                            CheckAppliedString = "";
                        }
                    }
                    #endregion
                    #endregion
                    List<int> AppliedIDOf1School = null;
                    if (CheckAppliedString != "")
                    {
                        AppliedIDOf1School = GetListIDFromString(CheckAppliedString);
                    }
                    else
                    {
                        continue;
                    }

                    for (int itemAp = 0; itemAp < AppliedIDOf1School.Count(); itemAp++)
                    {
                        objDB = lstDB.Where(x => x.SchoolID == lstViewModel[item].SchoolID &&
                                        x.AcademicYearID == lstViewModel[item].AcademicYearID &&
                                        x.AppliedLevelID == AppliedIDOf1School[itemAp]).FirstOrDefault();

                        if (objDB == null)
                        {
                            objDB = new LockInputSupervisingDept()
                            {
                                AcademicYearID = lstViewModel[item].AcademicYearID,
                                AppliedLevelID = AppliedIDOf1School[itemAp],
                                SchoolID = lstViewModel[item].SchoolID,
                                LockTitle = lstViewModel[item].LockTitle,
                                UserLock1ID = lstViewModel[item].UserLock1ID,
                                UserLock2ID = lstViewModel[item].UserLock2ID,
                                CreateDate = DateTime.Now
                            };
                            this.Insert(objDB);
                        }

                        else
                        {

                            if (objDB.LockTitle == "HK1")
                            {
                                // khoa them HK2
                                if (lstViewModel[item].LockTitle == "HK1,HK2")
                                {
                                    objDB.AppliedLevelID = AppliedIDOf1School[itemAp];
                                    objDB.LockTitle = lstViewModel[item].LockTitle;
                                        objDB.UserLock2ID = SupervisingDeptID;
                                        objDB.CreateDate = DateTime.Now;
                                    
                                    this.Update(objDB);
                                }
                                //mo HK1 khoa HK2
                                else if (lstViewModel[item].LockTitle == "HK2")
                                {
                                    objDB.AppliedLevelID = AppliedIDOf1School[itemAp];
                                    objDB.LockTitle = lstViewModel[item].LockTitle;
                                    objDB.UserUnlock1ID = SupervisingDeptID;
                                    objDB.ModifiedLock1Date = DateTime.Now;
                                        objDB.UserLock2ID = SupervisingDeptID;
                                        objDB.CreateDate = DateTime.Now;
                                    this.Update(objDB);
                                }
                                //Mo khoa HK1
                                else if (lstViewModel[item].LockTitle == "" || lstViewModel[item].LockTitle == null)
                                {
                                    objDB.AppliedLevelID = AppliedIDOf1School[itemAp];
                                    objDB.LockTitle = lstViewModel[item].LockTitle;
                                    objDB.UserUnlock1ID = SupervisingDeptID;
                                    objDB.ModifiedLock1Date = DateTime.Now;

                                    this.Update(objDB);
                                }
                            }

                    // Trong DATABASE cap 1 dang la HK2
                            else if (objDB.LockTitle == "HK2")
                            {
                                // khoa HK1 mo HK2
                                if (lstViewModel[item].LockTitle == "HK1")
                                {
                                    objDB.AppliedLevelID = AppliedIDOf1School[itemAp];
                                    objDB.LockTitle = lstViewModel[item].LockTitle;
                                        objDB.UserLock1ID = SupervisingDeptID;
                                        objDB.CreateDate = DateTime.Now;
                                    objDB.UserUnlock2ID = SupervisingDeptID;
                                    objDB.ModifiedLock2Date = DateTime.Now;

                            this.Update(objDB);
                        }
                                //khoa them HK1
                                else if (lstViewModel[item].LockTitle == "HK1,HK2")
                                {

                                    objDB.AppliedLevelID = AppliedIDOf1School[itemAp];
                                    objDB.LockTitle = lstViewModel[item].LockTitle;
                                    objDB.UserLock1ID = SupervisingDeptID;
                                        objDB.UserLock1ID = SupervisingDeptID;
                                        objDB.CreateDate = DateTime.Now;

                                    this.Update(objDB);
                                }
                                // mo khoa HK2
                                else if (lstViewModel[item].LockTitle == "" || lstViewModel[item].LockTitle == null)
                                {
                                    objDB.AppliedLevelID = AppliedIDOf1School[itemAp];
                                    objDB.LockTitle = lstViewModel[item].LockTitle;
                                    objDB.UserUnlock2ID = SupervisingDeptID;
                                    objDB.ModifiedLock2Date = DateTime.Now;

                                    this.Update(objDB);
                                }
                            }

                            // Trong DATABASE cap 1 dang la HK1,HK2
                            else if (objDB.LockTitle == "HK1,HK2")
                            {
                                //mo khoa HK2
                                if (lstViewModel[item].LockTitle == "HK1")
                                {
                                    objDB.AppliedLevelID = AppliedIDOf1School[itemAp]; ;
                                    objDB.LockTitle = lstViewModel[item].LockTitle;
                                    objDB.UserUnlock2ID = SupervisingDeptID;
                                    objDB.ModifiedLock2Date = DateTime.Now;

                                    this.Update(objDB);
                                }
                                //mo khoa HK1
                                else if (lstViewModel[item].LockTitle == "HK2")
                                {
                                    objDB.AppliedLevelID = AppliedIDOf1School[itemAp]; ;
                                    objDB.LockTitle = lstViewModel[item].LockTitle;
                                    objDB.UserUnlock1ID = SupervisingDeptID;
                                    objDB.ModifiedLock1Date = DateTime.Now;

                                    this.Update(objDB);
                                }
                                // mo khoa tat ca
                                else if (lstViewModel[item].LockTitle == "" || lstViewModel[item].LockTitle == null)
                                {
                                    objDB.AppliedLevelID = AppliedIDOf1School[itemAp]; ;
                                    objDB.LockTitle = lstViewModel[item].LockTitle;
                                    objDB.UserUnlock1ID = SupervisingDeptID;
                                    objDB.ModifiedLock1Date = DateTime.Now;
                                    objDB.UserUnlock2ID = SupervisingDeptID;
                                    objDB.ModifiedLock2Date = DateTime.Now;
                                    this.Update(objDB);
                                }
                            }

                            // Trong DATABASE cap 1 dang la "" hoac null
                            else if (objDB.LockTitle == "" || objDB.LockTitle == null)
                            {
                                // khoa HK1 sau khi da mo khoa HK1
                                if (lstViewModel[item].LockTitle == "HK1")
                                {                                 
                                        objDB.AppliedLevelID = AppliedIDOf1School[itemAp];
                                        objDB.LockTitle = lstViewModel[item].LockTitle;
                                        objDB.CreateDate = DateTime.Now;
                                        objDB.UserLock1ID = SupervisingDeptID;
                                        this.Update(objDB);
                                }

                                // khoa HK2 sau khi da mo khoa HK2
                                else if (lstViewModel[item].LockTitle == "HK2")
                                {
                                        objDB.AppliedLevelID = AppliedIDOf1School[itemAp];
                                        objDB.LockTitle = lstViewModel[item].LockTitle;
                                        objDB.CreateDate = DateTime.Now;
                                        objDB.UserLock2ID = SupervisingDeptID;
                                        this.Update(objDB);

                                }
                                else if (lstViewModel[item].LockTitle == "HK1,HK2")
                        {
                                    objDB.AppliedLevelID = AppliedIDOf1School[itemAp];
                                    objDB.LockTitle = lstViewModel[item].LockTitle;
                                    objDB.CreateDate = DateTime.Now;
                                    objDB.UserLock2ID = SupervisingDeptID;
                                    objDB.UserLock1ID = SupervisingDeptID;

                            this.Update(objDB);
                        }
                    }
                        }
                }
                }
            }
        }       

        public void InsertTempAllSubSuper(List<LockInputSupervisingDept> lstViewModel, int SupervisingDeptID, string AppliedString, string chkHK1, string chkHK2)
        {
            List<LockInputSupervisingDept> lstDB = LockInputSupervisingDeptBusiness.All.ToList();
            LockInputSupervisingDept objDB = null;
            int checkSuper = GetHierachyLevelIDByUserAccountID(SupervisingDeptID);
            if (AppliedString == "")
            {
                string Nullstring = "";
            }
            else
            {
                for (int item = 0; item < lstViewModel.Count(); item++)
                {
                    string CheckAppliedString = AppliedString;
                    #region xet applied
                    // neu grade = 7 hoac 31
                    #region 7 || 31
                    if (lstViewModel[item].AppliedLevelID == 7 || lstViewModel[item].AppliedLevelID == 31)
                    {
                        if (AppliedString == "1,2,3")
                        {
                            CheckAppliedString = "1,2,3";
                        }
                        else if (AppliedString == "1,2")
                        {
                            CheckAppliedString = "1,2";
                        }
                        else if (AppliedString == "2,3")
                        {
                            CheckAppliedString = "2,3";
                        }
                        else if (AppliedString == "1,3")
                        {
                            CheckAppliedString = "1,3";
                        }
                        else if (AppliedString == "1")
                        {
                            CheckAppliedString = "1";
                        }
                        else if (AppliedString == "2")
                        {
                            CheckAppliedString = "2";
                        }
                        else if (AppliedString == "3")
                        {
                            CheckAppliedString = "3";
                        }
                    }
                    #endregion

                    // neu grade = 6 => 2,3
                    #region 6
                    if (lstViewModel[item].AppliedLevelID == 6)
                    {
                        if (AppliedString == "1,2,3")
                        {
                            CheckAppliedString = "2,3";
                        }
                        else if (AppliedString == "1,2")
                        {
                            CheckAppliedString = "2";
                        }
                        else if (AppliedString == "2,3")
                        {
                            CheckAppliedString = "2,3";
                        }
                        else if (AppliedString == "1,3")
                        {
                            CheckAppliedString = "3";
                        }
                        else if (AppliedString == "1")
                        {
                            CheckAppliedString = "";
                        }
                        else if (AppliedString == "2")
                        {
                            CheckAppliedString = "2";
                        }
                        else if (AppliedString == "3")
                        {
                            CheckAppliedString = "3";
                        }
                    }
                    #endregion

                    // neu grade = 3 => 1,2
                    #region 3
                    if (lstViewModel[item].AppliedLevelID == 3)
                    {
                        if (AppliedString == "1,2,3")
                        {
                            CheckAppliedString = "1,2";
                        }
                        else if (AppliedString == "1,2")
                        {
                            CheckAppliedString = "1,2";
                        }
                        else if (AppliedString == "2,3")
                        {
                            CheckAppliedString = "2";
                        }
                        else if (AppliedString == "1,3")
                        {
                            CheckAppliedString = "1";
                        }
                        else if (AppliedString == "1")
                        {
                            CheckAppliedString = "1";
                        }
                        else if (AppliedString == "2")
                        {
                            CheckAppliedString = "2";
                        }
                        else if (AppliedString == "3")
                        {
                            CheckAppliedString = "";
                        }
                    }
                    #endregion

                    // neu grade = 4 => 3
                    #region 4
                    if (lstViewModel[item].AppliedLevelID == 4)
                    {
                        if (AppliedString == "1,2,3")
                        {
                            CheckAppliedString = "3";
                        }
                        else if (AppliedString == "1,2")
                        {
                            CheckAppliedString = "";
                        }
                        else if (AppliedString == "2,3")
                        {
                            CheckAppliedString = "3";
                        }
                        else if (AppliedString == "1,3")
                        {
                            CheckAppliedString = "3";
                        }
                        else if (AppliedString == "1")
                        {
                            CheckAppliedString = "";
                        }
                        else if (AppliedString == "2")
                        {
                            CheckAppliedString = "";
                        }
                        else if (AppliedString == "3")
                        {
                            CheckAppliedString = "3";
                        }
                    }
                    #endregion

                    // neu grade = 2 => 2
                    #region 2
                    if (lstViewModel[item].AppliedLevelID == 2)
                    {
                        if (AppliedString == "1,2,3")
                        {
                            CheckAppliedString = "2";
                        }
                        else if (AppliedString == "1,2")
                        {
                            CheckAppliedString = "2";
                        }
                        else if (AppliedString == "2,3")
                        {
                            CheckAppliedString = "2";
                        }
                        else if (AppliedString == "1,3")
                        {
                            CheckAppliedString = "";
                        }
                        else if (AppliedString == "1")
                        {
                            CheckAppliedString = "";
                        }
                        else if (AppliedString == "2")
                        {
                            CheckAppliedString = "2";
                        }
                        else if (AppliedString == "3")
                        {
                            CheckAppliedString = "";
                        }
                    }
                    #endregion

                    // neu grade = 1 => 1
                    #region 1
                    if (lstViewModel[item].AppliedLevelID == 1)
                    {
                        if (AppliedString == "1,2,3")
                        {
                            CheckAppliedString = "1";
                        }
                        else if (AppliedString == "1,2")
                        {
                            CheckAppliedString = "1";
                        }
                        else if (AppliedString == "2,3")
                        {
                            CheckAppliedString = "";
                        }
                        else if (AppliedString == "1,3")
                        {
                            CheckAppliedString = "1";
                        }
                        else if (AppliedString == "1")
                        {
                            CheckAppliedString = "1";
                        }
                        else if (AppliedString == "2")
                        {
                            CheckAppliedString = "";
                        }
                        else if (AppliedString == "3")
                        {
                            CheckAppliedString = "";
                        }
                    }
                    #endregion
                    #endregion
                    List<int> AppliedIDOf1School = null;
                    if (CheckAppliedString != "")
                    {
                        AppliedIDOf1School = GetListIDFromString(CheckAppliedString);
                    }
                    else
                    {
                        continue;
                    }

                    for (int itemAp = 0; itemAp < AppliedIDOf1School.Count(); itemAp++)
                    {
                        objDB = lstDB.Where(x => x.SchoolID == lstViewModel[item].SchoolID &&
                                        x.AcademicYearID == lstViewModel[item].AcademicYearID &&
                                        x.AppliedLevelID == AppliedIDOf1School[itemAp]).FirstOrDefault();

                        if (objDB == null)
                        {
                            objDB = new LockInputSupervisingDept()
                            {
                                AcademicYearID = lstViewModel[item].AcademicYearID,
                                AppliedLevelID = AppliedIDOf1School[itemAp],
                                SchoolID = lstViewModel[item].SchoolID,
                                LockTitle = lstViewModel[item].LockTitle,
                                UserLock1ID = lstViewModel[item].UserLock1ID,
                                UserLock2ID = lstViewModel[item].UserLock2ID,
                                CreateDate = DateTime.Now
                            };
                            this.Insert(objDB);
                        }

                        else
                        {

                            if (objDB.LockTitle == "HK1")
                            {
                                // khoa them HK2
                                if (lstViewModel[item].LockTitle == "HK1,HK2")
                                {
                                    objDB.AppliedLevelID = AppliedIDOf1School[itemAp];
                                    objDB.LockTitle = lstViewModel[item].LockTitle;
                                    objDB.UserLock2ID = SupervisingDeptID;
                                        objDB.CreateDate = DateTime.Now;
                                    this.Update(objDB);
                                }
                                //mo HK1 khoa HK2
                                else if (lstViewModel[item].LockTitle == "HK2")
                                {
                                    if (GetHierachyLevelIDByUserAccountID(Int32.Parse(objDB.UserLock1ID.ToString())) == 5)
                                    {
                                        objDB.UserUnlock1ID = SupervisingDeptID;
                                        objDB.ModifiedLock1Date = DateTime.Now;
                                        objDB.LockTitle = lstViewModel[item].LockTitle;
                                        objDB.CreateDate = DateTime.Now;
                                        objDB.AppliedLevelID = AppliedIDOf1School[itemAp];
                                        objDB.UserLock2ID = SupervisingDeptID;
                                        this.Update(objDB);
                                    }
                                    else
                                    {
                                        objDB.LockTitle = "HK1,HK2";
                                        objDB.CreateDate = DateTime.Now;
                                    objDB.UserLock2ID = SupervisingDeptID;
                                        this.Update(objDB);
                                    }
                                }
                                //Mo khoa HK1
                                else if (lstViewModel[item].LockTitle == "" || lstViewModel[item].LockTitle == null)
                                {
                                    if (GetHierachyLevelIDByUserAccountID(Int32.Parse(objDB.UserLock1ID.ToString())) == 5)
                                    {
                                        objDB.AppliedLevelID = AppliedIDOf1School[itemAp];
                                        objDB.LockTitle = lstViewModel[item].LockTitle;
                                        objDB.UserUnlock1ID = SupervisingDeptID;
                                        objDB.ModifiedLock1Date = DateTime.Now;

                                        this.Update(objDB);
                                    }
                                }
                            }

                    // Trong DATABASE cap 1 dang la HK2
                            else if (objDB.LockTitle == "HK2")
                            {
                                // khoa HK1 mo HK2
                                if (lstViewModel[item].LockTitle == "HK1")
                                {
                                    if (GetHierachyLevelIDByUserAccountID(Int32.Parse(objDB.UserLock2ID.ToString())) == 5)
                                    {
                                        objDB.UserUnlock2ID = SupervisingDeptID;
                                        objDB.ModifiedLock2Date = DateTime.Now;
                                        objDB.LockTitle = lstViewModel[item].LockTitle;
                                    objDB.AppliedLevelID = AppliedIDOf1School[itemAp];                                 
                                    objDB.UserLock1ID = SupervisingDeptID;
                                        objDB.CreateDate = DateTime.Now;
                                        this.Update(objDB);
                                    }
                                    else
                                    {
                                        objDB.LockTitle = "HK1,HK2";
                                        objDB.CreateDate = DateTime.Now;
                                        objDB.UserLock1ID = SupervisingDeptID;
                                        this.Update(objDB);
                                    }
                                }
                                //khoa them HK1
                                else if (lstViewModel[item].LockTitle == "HK1,HK2")
                                {
                                    objDB.AppliedLevelID = AppliedIDOf1School[itemAp];
                                    objDB.LockTitle = lstViewModel[item].LockTitle;
                                    objDB.UserLock1ID = SupervisingDeptID;
                                        objDB.CreateDate = DateTime.Now;
                                    this.Update(objDB);
                                }
                                // mo khoa HK2
                                else if (lstViewModel[item].LockTitle == "" || lstViewModel[item].LockTitle == null)
                                {
                                    if (GetHierachyLevelIDByUserAccountID(Int32.Parse(objDB.UserLock2ID.ToString())) == 5)
                                    {
                                        objDB.AppliedLevelID = AppliedIDOf1School[itemAp];
                                        objDB.LockTitle = lstViewModel[item].LockTitle;
                                        objDB.UserUnlock2ID = SupervisingDeptID;
                                        objDB.ModifiedLock2Date = DateTime.Now;

                                        this.Update(objDB);
        }
                                }
                            }

                            // Trong DATABASE cap 1 dang la HK1,HK2
                            else if (objDB.LockTitle == "HK1,HK2")
                            {
                                //mo khoa HK2
                                if (lstViewModel[item].LockTitle == "HK1")
                                {
                                    if ((GetHierachyLevelIDByUserAccountID(Int32.Parse(objDB.UserLock2ID.ToString())) == 5 &&
                                        GetHierachyLevelIDByUserAccountID(Int32.Parse(objDB.UserLock1ID.ToString())) == 5) ||
                                        (GetHierachyLevelIDByUserAccountID(Int32.Parse(objDB.UserLock2ID.ToString())) == 5 &&
                                        GetHierachyLevelIDByUserAccountID(Int32.Parse(objDB.UserLock1ID.ToString())) == 3))
                                    {
                                        objDB.AppliedLevelID = AppliedIDOf1School[itemAp]; 
                                        objDB.LockTitle = lstViewModel[item].LockTitle;
                                        objDB.UserUnlock2ID = SupervisingDeptID;
                                        objDB.ModifiedLock2Date = DateTime.Now;

                                        this.Update(objDB);
                                    }
                                }
                                //mo khoa HK1
                                else if (lstViewModel[item].LockTitle == "HK2")
                                {
                                    if ((GetHierachyLevelIDByUserAccountID(Int32.Parse(objDB.UserLock2ID.ToString())) == 5 &&
                                        GetHierachyLevelIDByUserAccountID(Int32.Parse(objDB.UserLock1ID.ToString())) == 5) ||
                                        (GetHierachyLevelIDByUserAccountID(Int32.Parse(objDB.UserLock2ID.ToString())) == 3 &&
                                        GetHierachyLevelIDByUserAccountID(Int32.Parse(objDB.UserLock1ID.ToString())) == 5))
                                    {
                                        objDB.AppliedLevelID = AppliedIDOf1School[itemAp];
                                        objDB.LockTitle = lstViewModel[item].LockTitle;
                                        objDB.UserUnlock1ID = SupervisingDeptID;
                                        objDB.ModifiedLock1Date = DateTime.Now;

                                        this.Update(objDB);
                                    }
                                }
                                // mo khoa tat ca
                                else if (lstViewModel[item].LockTitle == "" || lstViewModel[item].LockTitle == null)
                                {
                                    if (GetHierachyLevelIDByUserAccountID(Int32.Parse(objDB.UserLock2ID.ToString())) == 5 &&
                                GetHierachyLevelIDByUserAccountID(Int32.Parse(objDB.UserLock1ID.ToString())) == 5)
                                    {
                                        objDB.AppliedLevelID = AppliedIDOf1School[itemAp];
                                        objDB.LockTitle = lstViewModel[item].LockTitle;
                                        objDB.UserUnlock1ID = SupervisingDeptID;
                                        objDB.ModifiedLock1Date = DateTime.Now;
                                        objDB.UserUnlock2ID = SupervisingDeptID;
                                        objDB.ModifiedLock2Date = DateTime.Now;
                                        this.Update(objDB);
                                    }
                                    else if (GetHierachyLevelIDByUserAccountID(Int32.Parse(objDB.UserLock2ID.ToString())) == 3 &&
                                        GetHierachyLevelIDByUserAccountID(Int32.Parse(objDB.UserLock1ID.ToString())) == 5)
                                    {
                                        objDB.AppliedLevelID = AppliedIDOf1School[itemAp];
                                        objDB.LockTitle = "HK2";
                                        objDB.UserUnlock1ID = SupervisingDeptID;
                                        objDB.ModifiedLock1Date = DateTime.Now;
                                        this.Update(objDB);
                                    }
                                    else if (GetHierachyLevelIDByUserAccountID(Int32.Parse(objDB.UserLock2ID.ToString())) == 5 &&
                                        GetHierachyLevelIDByUserAccountID(Int32.Parse(objDB.UserLock1ID.ToString())) == 3)
                                    {
                                        objDB.AppliedLevelID = AppliedIDOf1School[itemAp];
                                        objDB.LockTitle = "HK1";
                                        objDB.UserUnlock2ID = SupervisingDeptID;
                                        objDB.ModifiedLock2Date = DateTime.Now;
                                        this.Update(objDB);
                                    }                                   
                                }
                            }

                            // Trong DATABASE cap 1 dang la "" hoac null
                            else if (objDB.LockTitle == "" || objDB.LockTitle == null)
                            {
                                // khoa HK1 sau khi da mo khoa HK1
                                if (lstViewModel[item].LockTitle == "HK1")
                                {
                                    objDB.AppliedLevelID = AppliedIDOf1School[itemAp];
                                    objDB.LockTitle = lstViewModel[item].LockTitle;
                                    objDB.CreateDate = DateTime.Now;
                                    objDB.UserLock1ID = SupervisingDeptID;

                                    this.Update(objDB);
                                }

                                // khoa HK2 sau khi da mo khoa HK2
                                else if (lstViewModel[item].LockTitle == "HK2")
                                {
                                    objDB.AppliedLevelID = AppliedIDOf1School[itemAp];
                                    objDB.LockTitle = lstViewModel[item].LockTitle;
                                    objDB.CreateDate = DateTime.Now;
                                    objDB.UserLock2ID = SupervisingDeptID;

                                    this.Update(objDB);
                                }
                                else if (lstViewModel[item].LockTitle == "HK1,HK2")
                                {
                                    objDB.AppliedLevelID = AppliedIDOf1School[itemAp];
                                    objDB.LockTitle = lstViewModel[item].LockTitle;
                                    objDB.CreateDate = DateTime.Now;
                                    objDB.UserLock2ID = SupervisingDeptID;
                                    objDB.UserLock1ID = SupervisingDeptID;

                                    this.Update(objDB);
                                }
                                
                            }
                        }
                    }
                }
            }
        }

        public int GetHierachyLevelIDByUserAccountID(int UserAccountID)
        {
            int HierachyLevelID = 0;
            SupervisingDept objSupervisingDept = SupervisingDeptBusiness.All.Where(p => p.AdminID == UserAccountID).FirstOrDefault();
            HierachyLevelID = objSupervisingDept != null ? objSupervisingDept.HierachyLevel : 0;
            return HierachyLevelID;
        }

        public void InsertAll(List<LockInputSupervisingDept> lstViewModel, int SupervisingDeptID)
        {
            List<LockInputSupervisingDept> lstDB = LockInputSupervisingDeptBusiness.All.ToList();
            LockInputSupervisingDept objDB = null;
            int checkSuper = GetHierachyLevelIDByUserAccountID(SupervisingDeptID);
            for (int item = 0; item < lstViewModel.Count(); item++)
            {
                objDB = lstDB.Where(x => x.SchoolID == lstViewModel[item].SchoolID && x.AcademicYearID == lstViewModel[item].AcademicYearID).FirstOrDefault();
                if (objDB == null)
                {
                    objDB = new LockInputSupervisingDept()
                    {
                        AcademicYearID = lstViewModel[item].AcademicYearID,
                        AppliedLevelID = lstViewModel[item].AppliedLevelID,
                        SchoolID = lstViewModel[item].SchoolID,
                        LockTitle = lstViewModel[item].LockTitle,
                        UserLock1ID = lstViewModel[item].UserLock1ID,
                        UserLock2ID = lstViewModel[item].UserLock2ID,
                        CreateDate = DateTime.Now
                    };
                    this.Insert(objDB);
                }
                else if (objDB != null)
                {

                    //if (GetHierachyLevelIDByUserAccountID(Int32.Parse(objDB.UserLock1ID.ToString())) == 3)


                    #region xet truong hop Update
                    if (objDB.LockTitle == "HK1")
                    {
                        // khoa them HK2
                        if (lstViewModel[item].LockTitle == "HK1,HK2")
                        {                           
                            objDB.LockTitle = lstViewModel[item].LockTitle;
                            objDB.UserLock2ID = SupervisingDeptID;
                            objDB.CreateDate = DateTime.Now;
                            this.Update(objDB);
                        }
                        //mo HK1 khoa HK2
                        else if (lstViewModel[item].LockTitle == "HK2")
                        {
                            if (GetHierachyLevelIDByUserAccountID(Int32.Parse(objDB.UserLock1ID.ToString())) == 5)
                            {
                                objDB.UserUnlock1ID = SupervisingDeptID;
                                objDB.ModifiedLock1Date = DateTime.Now;
                                objDB.LockTitle = lstViewModel[item].LockTitle;
                            }
                            else {
                                objDB.LockTitle = "HK1,HK2";
                            }
                            //objDB.UserUnlock1ID = SupervisingDeptID;
                            //objDB.ModifiedLock1Date = DateTime.Now;
                            objDB.UserLock2ID = SupervisingDeptID;
                            objDB.CreateDate = DateTime.Now;
                            this.Update(objDB);
                        }
                        //Mo khoa HK1
                        else if (lstViewModel[item].LockTitle == "" || lstViewModel[item].LockTitle == null)
                        {
                            if (GetHierachyLevelIDByUserAccountID(Int32.Parse(objDB.UserLock1ID.ToString())) == 5) {
                                objDB.LockTitle = lstViewModel[item].LockTitle;
                                objDB.UserUnlock1ID = SupervisingDeptID;
                                objDB.ModifiedLock1Date = DateTime.Now;

                                this.Update(objDB);
                            }
                            
                        }
                    }
                    else if (objDB.LockTitle == "HK2")
                    {
                        // khoa HK1 mo HK2
                        if (lstViewModel[item].LockTitle == "HK1")
                        {
                            if (GetHierachyLevelIDByUserAccountID(Int32.Parse(objDB.UserLock2ID.ToString())) == 5)
                            {
                                objDB.UserUnlock2ID = SupervisingDeptID;
                                objDB.ModifiedLock2Date = DateTime.Now;
                                objDB.LockTitle = lstViewModel[item].LockTitle;
                            }
                            else {
                                objDB.LockTitle = "HK1,HK2";
                            }
                            
                            objDB.UserLock1ID = SupervisingDeptID;
                            objDB.CreateDate = DateTime.Now;
                            

                            this.Update(objDB);
                        }
                        //khoa them HK1
                        else if (lstViewModel[item].LockTitle == "HK1,HK2")
                        {
                            objDB.LockTitle = lstViewModel[item].LockTitle;
                            objDB.UserLock1ID = SupervisingDeptID;
                            objDB.CreateDate = DateTime.Now;

                            this.Update(objDB);
                        }
                        // mo khoa HK2
                        else if (lstViewModel[item].LockTitle == "" || lstViewModel[item].LockTitle == null)
                        {
                            if (GetHierachyLevelIDByUserAccountID(Int32.Parse(objDB.UserLock2ID.ToString())) == 5)
                            {
                                objDB.LockTitle = lstViewModel[item].LockTitle;
                                objDB.UserUnlock2ID = SupervisingDeptID;
                                objDB.ModifiedLock2Date = DateTime.Now;

                                this.Update(objDB);
                            }                          
                        }
                    }
                    else if (objDB.LockTitle == "HK1,HK2")
                    {
                        //mo khoa HK2
                        if (lstViewModel[item].LockTitle == "HK1")
                        {
                            if ((GetHierachyLevelIDByUserAccountID(Int32.Parse(objDB.UserLock2ID.ToString())) == 5 &&
                                GetHierachyLevelIDByUserAccountID(Int32.Parse(objDB.UserLock1ID.ToString())) == 5) ||
                                (GetHierachyLevelIDByUserAccountID(Int32.Parse(objDB.UserLock2ID.ToString())) == 5 &&
                                GetHierachyLevelIDByUserAccountID(Int32.Parse(objDB.UserLock1ID.ToString())) == 3))
                            {
                                objDB.LockTitle = lstViewModel[item].LockTitle;
                                objDB.UserUnlock2ID = SupervisingDeptID;
                                objDB.ModifiedLock2Date = DateTime.Now;

                                this.Update(objDB);
                            }
                            
                        }
                        //mo khoa HK1
                        else if (lstViewModel[item].LockTitle == "HK2")
                        {
                            if ((GetHierachyLevelIDByUserAccountID(Int32.Parse(objDB.UserLock2ID.ToString())) == 5 &&
                                GetHierachyLevelIDByUserAccountID(Int32.Parse(objDB.UserLock1ID.ToString())) == 5) ||
                                (GetHierachyLevelIDByUserAccountID(Int32.Parse(objDB.UserLock2ID.ToString())) == 3 &&
                                GetHierachyLevelIDByUserAccountID(Int32.Parse(objDB.UserLock1ID.ToString())) == 5))
                            {
                                objDB.LockTitle = lstViewModel[item].LockTitle;
                                objDB.UserUnlock1ID = SupervisingDeptID;
                                objDB.ModifiedLock1Date = DateTime.Now;

                                this.Update(objDB);
                            }                            
                        }
                        // mo khoa tat ca
                        else if (lstViewModel[item].LockTitle == "" || lstViewModel[item].LockTitle == null)
                        {
                            if (GetHierachyLevelIDByUserAccountID(Int32.Parse(objDB.UserLock2ID.ToString())) == 5 &&
                                GetHierachyLevelIDByUserAccountID(Int32.Parse(objDB.UserLock1ID.ToString())) == 5)
                            {
                                objDB.LockTitle = lstViewModel[item].LockTitle;
                                objDB.UserUnlock1ID = SupervisingDeptID;
                                objDB.ModifiedLock1Date = DateTime.Now;
                                objDB.UserUnlock2ID = SupervisingDeptID;
                                objDB.ModifiedLock2Date = DateTime.Now;
                                this.Update(objDB);
                            }
                            else if (GetHierachyLevelIDByUserAccountID(Int32.Parse(objDB.UserLock2ID.ToString())) == 3 &&
                                GetHierachyLevelIDByUserAccountID(Int32.Parse(objDB.UserLock1ID.ToString())) == 5) 
                            {
                                objDB.LockTitle = "HK2";
                                objDB.UserUnlock1ID = SupervisingDeptID;
                                objDB.ModifiedLock1Date = DateTime.Now;
                                this.Update(objDB);
                            }
                            else if (GetHierachyLevelIDByUserAccountID(Int32.Parse(objDB.UserLock2ID.ToString())) == 5 &&
                                GetHierachyLevelIDByUserAccountID(Int32.Parse(objDB.UserLock1ID.ToString())) == 3)
                            {
                                objDB.LockTitle = "HK1";
                                objDB.UserUnlock2ID = SupervisingDeptID;
                                objDB.ModifiedLock2Date = DateTime.Now;
                                this.Update(objDB);
                            }
                        }
                    }

                    else if (objDB.LockTitle == "" || objDB.LockTitle == null)
                    {
                        // khoa HK1 sau khi da mo khoa HK1
                        if (lstViewModel[item].LockTitle == "HK1")
                        {

                            objDB.LockTitle = lstViewModel[item].LockTitle;
                            objDB.CreateDate = DateTime.Now;
                            objDB.UserLock1ID = SupervisingDeptID;

                            this.Update(objDB);
                        }

                        // khoa HK2 sau khi da mo khoa HK2
                        else if (lstViewModel[item].LockTitle == "HK2")
                        {

                            objDB.LockTitle = lstViewModel[item].LockTitle;
                            objDB.CreateDate = DateTime.Now;
                            objDB.UserLock2ID = SupervisingDeptID;

                            this.Update(objDB);
                        }
                        else if (lstViewModel[item].LockTitle == "HK1,HK2")
                        {

                            objDB.LockTitle = lstViewModel[item].LockTitle;
                            objDB.CreateDate = DateTime.Now;
                            objDB.UserLock2ID = SupervisingDeptID;
                            objDB.UserLock1ID = SupervisingDeptID;

                            this.Update(objDB);
                        }
                    }

                    #endregion
                }
            }
            this.Save();
        }
    }
}
