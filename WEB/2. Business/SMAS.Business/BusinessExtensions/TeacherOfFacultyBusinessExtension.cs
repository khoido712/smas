﻿/**
 * The Viettel License
 *
 * Copyright 2012 Viettel Telecom. All rights reserved.
 * VIETTEL PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */

/** 
* @author  
* @version $Revision: $
*/

using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Linq.Expressions;
using System.Web;
using log4net;

using SMAS.DAL.Repository;
using SMAS.DAL.IRepository;
using SMAS.Business.IBusiness;
using SMAS.Models.Models;
using SMAS.Business.Common;

namespace SMAS.Business.Business
{ 
    public partial class TeacherOfFacultyBusiness
    {  
        #region Search
        /// <summary>
        /// Tìm kiếm giáo viên trong tổ/nhóm
        /// </summary>
        /// <param name="dic">tham số</param>
        /// <returns>Danh sách kết quả tìm kiếm</returns>
        public IQueryable<TeacherOfFaculty> Search(IDictionary<string, object> dic)
        {
            int FacultyID = Utils.GetInt(dic, "FacultyID");
            int TeacherID = Utils.GetInt(dic, "TeacherID");
            DateTime? StartDate = Utils.GetDateTime(dic, "StartDate");
            bool? isActive = Utils.GetIsActive(dic, "IsActive");
            List<int> lstFacultyID = Utils.GetIntList(dic, "LstFacultyID ");


            IQueryable<TeacherOfFaculty> lstTeacherOfFaculty = this.TeacherOfFacultyRepository.All;
            if (FacultyID > 0)
            {
                lstTeacherOfFaculty = lstTeacherOfFaculty.Where(t => t.FacultyID == FacultyID);
            }
		
            if (TeacherID > 0)
            {
                lstTeacherOfFaculty = lstTeacherOfFaculty.Where(t => t.TeacherID == TeacherID);
            }

            if (StartDate != null)
            {
                lstTeacherOfFaculty = lstTeacherOfFaculty.Where(t => t.StartDate.HasValue && t.StartDate.Value.Day == StartDate.Value.Day && 
                    t.StartDate.Value.Month == StartDate.Value.Month && t.StartDate.Value.Year == StartDate.Value.Year);
            }

            if (lstFacultyID != null && lstFacultyID.Count > 0)
            {
                lstTeacherOfFaculty = lstTeacherOfFaculty.Where(t => lstFacultyID.Contains(t.FacultyID));
            }

            return lstTeacherOfFaculty;
        }
        #endregion
        public void DeleteTeacherInFaculty(int FacultyID)
        {
            var lstDel = this.Search(new Dictionary<string, object>{          
            {"FacultyID", FacultyID}
            }).ToList();
            this.DeleteAll(lstDel);
        }
        
    }
}