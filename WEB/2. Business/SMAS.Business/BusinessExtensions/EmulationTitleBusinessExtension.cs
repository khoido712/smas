﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SMAS.Business.BusinessObject;
using SMAS.Business.Business;
using SMAS.Business.Common;
using SMAS.Models.Models;
using System.IO;
using SMAS.VTUtils.Excel.Export;
using SMAS.DAL.Repository;

namespace SMAS.Business.Business
{
    public partial class EmulationTitleBusiness
    {
        #region Lấy mảng băm cho các tham số đầu vào
        /// <summary>
        /// minhh - 26/11/2012
        ///Lấy mảng băm cho các tham số đầu vào 
        /// </summary>
        /// <param name="entity"></param>
        /// <returns></returns>
        public string GetHashKey(EmulationTitle entity)
        {
            IDictionary<string, object> dic = new Dictionary<string, object> {
                {"Semester", entity.Semester},
                {"AcademicYearID", entity.AcademicYearID},
                {"AppliedLevel", entity.AppliedLevel}
            };
            return ReportUtils.GetHashKey(dic);
        }
        #endregion

        #region Lưu lại thông tin
        /// <summary>
        /// minhh - 26/11/2012
        /// Lưu lại thông tin 
        /// </summary>
        /// <param name="entity"></param>
        /// <param name="data"></param>
        /// <returns></returns>
        public ProcessedReport InsertEmulationTitle(EmulationTitle entity, Stream data)
        {

            string reportCode = SystemParamsInFile.REPORT_THONG_KE_DANH_HIEU_THI_DUA;

            ReportDefinition reportDef = ReportDefinitionBusiness.GetByCode(reportCode);
            ProcessedReport pr = new ProcessedReport();
            pr.ReportCode = reportCode;
            pr.ProcessedDate = DateTime.Now;
            pr.InputParameterHashKey = GetHashKey(entity);
            pr.ReportData = ReportUtils.Compress(data);

            //Tạo tên file
            string outputNamePattern = reportDef.OutputNamePattern;
            string semester = ReportUtils.ConvertSemesterForReportName(entity.Semester);

            outputNamePattern = outputNamePattern.Replace("[SchoolLevel]", ReportUtils.ConvertAppliedLevelForReportName(entity.AppliedLevel));
            outputNamePattern = outputNamePattern.Replace("[Semester]", semester);
            pr.ReportName = ReportUtils.RemoveSpecialCharacters(outputNamePattern) + "." + reportDef.OutputFormat;

            IDictionary<string, object> dic = new Dictionary<string, object> {
                {"Semester", entity.Semester},
                {"AcademicYearID", entity.AcademicYearID}
            };
            ProcessedReportParameterRepository.Insert(dic, pr);
            ProcessedReportRepository.Insert(pr);
            ProcessedReportRepository.Save();
            return pr;
        }
        #endregion

        #region Lấy bảng thống kê xếp hạng hạnh kiểm được cập nhật mới nhất
        /// <summary>
        /// <author>minhh</author>
        /// Lấy bảng thống kê xếp hạng hạnh kiểm được cập nhật mới nhất
        /// </summary>
        /// <param name="entity"></param>
        /// <returns></returns>
        public ProcessedReport GetEmulationTitle(EmulationTitle entity)
        {
            string reportCode = SystemParamsInFile.REPORT_THONG_KE_DANH_HIEU_THI_DUA;
            string inputParameterHashKey = GetHashKey(entity);
            return ProcessedReportBusiness.GetProcessedReport(reportCode, inputParameterHashKey);
        }
        #endregion

        #region Lưu lại thông tin xếp hạng hạnh kiểm
        /// <summary>
        /// <author>minhh</author>
        /// Lưu lại thông tin xếp hạng hạnh kiểm
        /// </summary>
        /// <param name="entity"></param>
        /// <returns></returns>
        public Stream CreateEmulationTitle(EmulationTitle entity)
        {
            string reportCode = SystemParamsInFile.REPORT_THONG_KE_DANH_HIEU_THI_DUA;
            ReportDefinition reportDef = ReportDefinitionBusiness.GetByCode(reportCode);
            string templatePath = SystemParamsInFile.TEMPLATE_FOLDER + reportDef.TemplateDirectory + "/" + reportDef.TemplateName;
            IVTWorkbook oBook = VTExport.OpenWorkbook(templatePath);

            //Lấy sheet 
            IVTWorksheet firstSheet = oBook.GetSheet(1);

            IVTRange firstRange = firstSheet.GetRange("B9", "H9");
            IVTRange midRange = firstSheet.GetRange("B10", "H10");
            IVTRange lastRange = firstSheet.GetRange("B11", "H11");
            IVTRange rangeEducation = firstSheet.GetRange("A9", "A9");
            IVTRange rangeBoldFont = firstSheet.GetRange("E1", "H1");
            IVTRange rangeTC = firstSheet.GetRange("B12", "H12");
            IVTRange rangeTT = firstSheet.GetRange("A13", "H13");


            #region Xu ly du lieu
            //fill dữ liệu vào sheet thông tin chung
            AcademicYear academicYear = AcademicYearBusiness.Find(entity.AcademicYearID);
            SchoolProfile school = academicYear.SchoolProfile;

            string supervisingDeptName = UtilsBusiness.GetSupervisingDeptName(school.SchoolProfileID, entity.AppliedLevel).ToUpper();
            string schoolName = school.SchoolName.ToUpper();

            string semester = "";
            if (entity.Semester == SystemParamsInFile.SEMESTER_OF_YEAR_FIRST) semester = "HỌC KỲ I";
            if (entity.Semester == SystemParamsInFile.SEMESTER_OF_YEAR_SECOND) semester = "HỌC KỲ II";
            if (entity.Semester == SystemParamsInFile.SEMESTER_OF_YEAR_ALL) semester = "CẢ NĂM";

            string semesterAndAcademic = semester + " - năm học " + academicYear.DisplayTitle;
            int excellentHonourID = HonourAchivementTypeBusiness.All.Where(o => o.Type == SystemParamsInFile.HONOUR_ACHIVEMENT_TYPE_PUPIL && o.IsActive == true && o.Resolution.Equals("Học sinh giỏi")).FirstOrDefault().HonourAchivementTypeID;
            int goodHonourID = HonourAchivementTypeBusiness.All.Where(o => o.Type == SystemParamsInFile.HONOUR_ACHIVEMENT_TYPE_PUPIL && o.IsActive == true && o.Resolution.Equals("Học sinh tiên tiến")).FirstOrDefault().HonourAchivementTypeID;
            List<EducationLevel> lstEducationLevel = EducationLevelBusiness.GetByGrade(entity.AppliedLevel).ToList();
            AcademicYear Aca = AcademicYearBusiness.Find(entity.AcademicYearID);

            Dictionary<string, object> dic = new Dictionary<string, object> 
            {
                {"AcademicYearID", entity.AcademicYearID}, 
                {"Semester", entity.Semester}
            };
            List<PupilEmulation> listPupilEmulationAllStatus = PupilEmulationBusiness.SearchBySchool(entity.SchoolID, dic).ToList();//dung chung
            //Danh sach lop hoc (dung chung)
            IQueryable<ClassProfile> lsClass = ClassProfileBusiness.SearchBySchool(entity.SchoolID, new Dictionary<string, object> {
                                                {"AppliedLevel", entity.AppliedLevel},
                                                {"AcademicYearID", entity.AcademicYearID},
                                                {"IsVNEN", true}}).OrderBy(o => o.DisplayName);


            IQueryable<PupilOfClass> lsPupilOfClassSemester1 = PupilOfClassBusiness.SearchBySchool(entity.SchoolID, new Dictionary<string, object> {
                                                {"AppliedLevel", entity.AppliedLevel},
                                                {"AcademicYearID", entity.AcademicYearID}
                                                //{"EthnicID",entity.EthnicID},
                                               // {"FemaleID",entity.FemaleID}
            }).AddCriteriaSemester(AcademicYearBusiness.Find(entity.AcademicYearID), entity.Semester);
            List<PupilOfClass> lsPupilOfClassSemester = lsPupilOfClassSemester1.Where(o => o.AssignedDate == lsPupilOfClassSemester1.Where(u => u.PupilID == o.PupilID && u.ClassID == o.ClassID).Select(u => u.AssignedDate).Max()).ToList();

            List<PupilOfClass> lstPupilOfClassSemesterFemale = new List<PupilOfClass>();
            if (entity.FemaleID > 0)
            {
                lsPupilOfClassSemester1 = PupilOfClassBusiness.SearchBySchool(entity.SchoolID, new Dictionary<string, object> {
                                                {"AppliedLevel", entity.AppliedLevel},
                                                {"AcademicYearID", entity.AcademicYearID},
                                               {"FemaleID",entity.FemaleID}
                                        }).AddCriteriaSemester(AcademicYearBusiness.Find(entity.AcademicYearID), entity.Semester);
                lstPupilOfClassSemesterFemale = lsPupilOfClassSemester1.Where(o => o.AssignedDate == lsPupilOfClassSemester1.Where(u => u.PupilID == o.PupilID && u.ClassID == o.ClassID).Select(u => u.AssignedDate).Max()).ToList();
            }

            List<PupilOfClass> lstPupilOfClassSemesterEthnic = new List<PupilOfClass>();
            if (entity.EthnicID > 0)
            {
                lsPupilOfClassSemester1 = PupilOfClassBusiness.SearchBySchool(entity.SchoolID, new Dictionary<string, object> {
                                                {"AppliedLevel", entity.AppliedLevel},
                                                {"AcademicYearID", entity.AcademicYearID},
                                              {"EthnicID",entity.EthnicID}
                                        }).AddCriteriaSemester(AcademicYearBusiness.Find(entity.AcademicYearID), entity.Semester);
                lstPupilOfClassSemesterEthnic = lsPupilOfClassSemester1.Where(o => o.AssignedDate == lsPupilOfClassSemester1.Where(u => u.PupilID == o.PupilID && u.ClassID == o.ClassID).Select(u => u.AssignedDate).Max()).ToList();
            }

            List<PupilOfClass> lstPupilOfClassSemesterFemaleEthnic = new List<PupilOfClass>();
            if (entity.FemaleEthnicID > 0)
            {
                lsPupilOfClassSemester1 = PupilOfClassBusiness.SearchBySchool(entity.SchoolID, new Dictionary<string, object> {
                                                {"AppliedLevel", entity.AppliedLevel},
                                                {"AcademicYearID", entity.AcademicYearID},
                                              {"EthnicID",entity.EthnicID},
                                              {"FemaleID",entity.FemaleID}
                                        }).AddCriteriaSemester(AcademicYearBusiness.Find(entity.AcademicYearID), entity.Semester);
                lstPupilOfClassSemesterFemaleEthnic = lsPupilOfClassSemester1.Where(o => o.AssignedDate == lsPupilOfClassSemester1.Where(u => u.PupilID == o.PupilID && u.ClassID == o.ClassID).Select(u => u.AssignedDate).Max()).ToList();
            }
            
            IDictionary<string, object> dicCATP = new Dictionary<string, object>()
                {
                    {"SchoolID",entity.SchoolID},
                    {"AcademicYearID",entity.AcademicYearID},
                    {"Semester",entity.Semester},
                    {"Type",SystemParamsInFile.GET_NUMPUPIL_IN_CLASS_TYPE0}
                   // {"EthnicID",entity.EthnicID},
                   // {"FemaleID",entity.FemaleID}
                };
            IQueryable<ClassInfoBO> iqClassInfoBO = PupilOfClassBusiness.GetClassAndTotalPupil(lsClass, dicCATP);
            List<ClassInfoBO> ListClass = iqClassInfoBO.ToList();

            //Chi lay thong tin cua hoc sinh dang hoc trong ky
            List<PupilEmulation> listPupilEmulation = (from u in listPupilEmulationAllStatus
                                                       join poc in lsPupilOfClassSemester on new { u.PupilID, u.ClassID } equals new {poc.PupilID, poc.ClassID }
                                                       select u).ToList();


            int countAllPupilOfSchool = 0;
            int countAllGoodOfSchool = 0;
            int countAllFairOfSchool = 0;
            int countAllRealPupilOfSchool = 0;

            int startRow = 9;
            int startGroupRow = 9;
            int startmin = startRow;

            #region Tao sheet dau tien
            // Tao sheet moi
            IVTWorksheet sheet = oBook.CopySheetToLast(firstSheet, "H8");
            sheet.SetCellValue("A2", supervisingDeptName);
            sheet.SetCellValue("A3", schoolName);
            sheet.SetCellValue("A5", semesterAndAcademic);

            for (int p = 0; p < lstEducationLevel.Count; p++)
            {
                int countAllPupilOfEducation = 0;
                int countRealPupilOfEducation = 0;
                int countAllGoodPupil = 0;
                double percentAllGoodPupil = 0;
                int countAllFairPupil = 0;
                double percentAllFairPupil = 0;
                int startSum = startRow;
                sheet.CopyPasteSameRowHeigh(rangeEducation, startGroupRow);

                //Ten khoi
                sheet.SetCellValue(startGroupRow, 1, lstEducationLevel[p].Resolution);

                //Lấy danh sách lớp thuoc khối đó

                //Danh sach lop thuoc khoi
                List<ClassInfoBO> listFirstCP = ListClass.Where(o => o.EducationLevelID == lstEducationLevel[p].EducationLevelID)
                    .OrderBy(o => o.ClassOrderNumber.HasValue ? o.ClassOrderNumber : 0).ThenBy(o => o.ClassName).ToList();

                //Duyet trong tung lop
                int countCP = listFirstCP.Count;
                for (int i = 0; i < countCP; i++)
                {
                    //Copy row style
                    if (i == 0)
                    {
                        sheet.CopyPaste(firstRange, startRow, 2);
                    }
                    else if (i == countCP - 1)
                    {
                        sheet.CopyPaste(lastRange, startRow, 2);
                    }
                    else
                    {
                        sheet.CopyPaste(midRange, startRow, 2);
                    }

                    //Ten lop
                    sheet.SetCellValue(startRow, 2, listFirstCP[i].ClassName);

                    //Si so
                    int sumPupil = listFirstCP[i].NumOfPupil;
                    sheet.SetCellValue(startRow, 3, sumPupil);
                    countAllPupilOfEducation += sumPupil;

                    //Si so thuc te
                    //Sỹ số thực tế: Thực hiện việc count số lượng học sinh ở trạng thái đang học hoặc đã tốt nghiệp bằng cách count trong bảng PupilOfClass với các tham số truyền vào

                    int countRealPupil = listFirstCP[i].ActualNumOfPupil;
                    countRealPupilOfEducation += countRealPupil;
                    sheet.SetCellValue(startRow, 4, countRealPupil);

                    //Hoc sinh gioi
                    List<PupilEmulation> listGoodPupilEmulation = new List<PupilEmulation>();

                    listGoodPupilEmulation = listPupilEmulation.Where(o => (o.ClassID == listFirstCP[i].ClassID && o.HonourAchivementTypeID == excellentHonourID)).ToList();

                    double percentGood = 0;
                    if (countRealPupil > 0) percentGood = 100 * 1.0 * listGoodPupilEmulation.Count() / (countRealPupil * 1.0);
                    sheet.SetCellValue(startRow, 5, listGoodPupilEmulation.Count());
                    sheet.SetCellValue(startRow, 6, "=IF(D" + startRow + ">0" + "," + "ROUND(E" + startRow + "/" + "D" + startRow + "*100,2),0)");
                    countAllGoodPupil += listGoodPupilEmulation.Count();

                    //Hoc sinh tien tien
                    List<PupilEmulation> listRankingOfClassFair = new List<PupilEmulation>();
                    listRankingOfClassFair = listPupilEmulation.Where(o => (o.ClassID == listFirstCP[i].ClassID && o.HonourAchivementTypeID == goodHonourID)).ToList();

                    double percentFair = 0;
                    if (countRealPupil > 0) percentFair = 100 * listRankingOfClassFair.Count() / (countRealPupil * 1.0);
                    sheet.SetCellValue(startRow, 7, listRankingOfClassFair.Count());
                    sheet.SetCellValue(startRow, 8, "=IF(D" + startRow + ">0" + "," + "ROUND(G" + startRow + "/" + "D" + startRow + "*100,2),0)");
                    countAllFairPupil += listRankingOfClassFair.Count();

                    startRow++;
                }

                countAllPupilOfSchool += countAllPupilOfEducation;
                countAllRealPupilOfSchool += countRealPupilOfEducation;
                countAllGoodOfSchool += countAllGoodPupil;
                countAllFairOfSchool += countAllFairPupil;
                if (countRealPupilOfEducation > 0) percentAllGoodPupil = 100 * 1.0 * countAllGoodPupil / (1.0 * countRealPupilOfEducation);
                if (countRealPupilOfEducation > 0) percentAllFairPupil = 100 * 1.0 * countAllFairPupil / (1.0 * countRealPupilOfEducation);

                sheet.CopyPasteSameRowHeigh(rangeTC, startRow);

                sheet.SetCellValue(startRow, 2, "TC");
                sheet.SetCellValue(startRow, 3, "=SUM(C" + startSum.ToString() + ":C" + (startRow - 1).ToString() + ")");
                sheet.SetCellValue(startRow, 4, "=SUM(D" + startSum.ToString() + ":D" + (startRow - 1).ToString() + ")");
                sheet.SetCellValue(startRow, 5, "=SUM(E" + startSum.ToString() + ":E" + (startRow - 1).ToString() + ")");
                sheet.SetCellValue(startRow, 6, "=IF(D" + startRow + ">0" + "," + "ROUND(E" + startRow + "/" + "D" + startRow + "*100,2),0)");
                sheet.SetCellValue(startRow, 7, "=SUM(G" + startSum.ToString() + ":G" + (startRow - 1).ToString() + ")");
                sheet.SetCellValue(startRow, 8, "=IF(D" + startRow + ">0" + "," + "ROUND(G" + startRow + "/" + "D" + startRow + "*100,2),0)");


                if (listFirstCP.Count > 0)
                {
                    sheet.MergeColumn(1, startGroupRow, startGroupRow + listFirstCP.Count);
                    sheet.GetRange(startGroupRow, 1, startGroupRow + listFirstCP.Count, 1).SetBorder(VTBorderStyle.Solid, VTBorderWeight.Medium, VTBorderIndex.EdgeLeft);
                }

                sheet.GetRange(startGroupRow, 1, startRow, 8).SetBorder(VTBorderStyle.Solid, VTBorderWeight.Medium, VTBorderIndex.Around);
                startRow++;
                startGroupRow = startGroupRow + listFirstCP.Count + 1;
            }

            sheet.CopyPaste(rangeTT, startRow, 1);

            double percentAllGoodOfSchool = 0;
            if (countAllRealPupilOfSchool > 0) percentAllGoodOfSchool = 100 * 1.0 * countAllGoodOfSchool / (1.0 * countAllRealPupilOfSchool);
            double percentAllFairOfSchool = 0;
            if (countAllRealPupilOfSchool > 0) percentAllFairOfSchool = 100 * 1.0 * countAllFairOfSchool / (1.0 * countAllRealPupilOfSchool);
            sheet.SetCellValue(startRow, 1, "T.Trường");
            sheet.SetCellValue(startRow, 2, "");
            sheet.SetCellValue(startRow, 3, "=SUM(C" + startmin.ToString() + ":C" + (startRow - 1).ToString() + ")/2");
            sheet.SetCellValue(startRow, 4, "=SUM(D" + startmin.ToString() + ":D" + (startRow - 1).ToString() + ")/2");
            sheet.SetCellValue(startRow, 5, "=SUM(E" + startmin.ToString() + ":E" + (startRow - 1).ToString() + ")/2");
            sheet.SetCellValue(startRow, 6, "=IF(D" + startRow + ">0" + "," + "ROUND(E" + startRow + "/" + "D" + startRow + "*100,2),0)");//Math.Round(percentAllGoodOfSchool, 2, MidpointRounding.AwayFromZero));
            sheet.SetCellValue(startRow, 7, "=SUM(G" + startmin.ToString() + ":G" + (startRow - 1).ToString() + ")/2");
            sheet.SetCellValue(startRow, 8, "=IF(D" + startRow + ">0" + "," + "ROUND(G" + startRow + "/" + "D" + startRow + "*100,2),0)");
            sheet.FitAllColumnsOnOnePage = true;
            #endregion
            sheet.Name = "Danh Hieu Thi Dua";

            #endregion

            #region Tao sheet nu
            if (entity.FemaleID > 0)
            {
                 countAllPupilOfSchool = 0;
                 countAllGoodOfSchool = 0;
                 countAllFairOfSchool = 0;
                 countAllRealPupilOfSchool = 0;

                 startRow = 9;
                 startGroupRow = 9;
                 startmin = startRow;

                IDictionary<string, object> dicCATPFemale = new Dictionary<string, object>()
                {
                    {"SchoolID",entity.SchoolID},
                    {"AcademicYearID",entity.AcademicYearID},
                    {"Semester",entity.Semester},
                    {"Type",SystemParamsInFile.GET_NUMPUPIL_IN_CLASS_TYPE0},                    
                    {"FemaleID",entity.FemaleID}
                };
                iqClassInfoBO = PupilOfClassBusiness.GetClassAndTotalPupil(lsClass, dicCATPFemale);
                ListClass = iqClassInfoBO.ToList();

                //Chi lay thong tin cua hoc sinh dang hoc trong ky
                listPupilEmulation = (from u in listPupilEmulationAllStatus
                                      join poc in lstPupilOfClassSemesterFemale on new { u.PupilID, u.ClassID } equals new {poc.PupilID,poc.ClassID }
                                      select u).ToList();

                // Tao sheet moi
                IVTWorksheet sheet_female = oBook.CopySheetToLast(firstSheet, "H8");
                sheet_female.SetCellValue("A2", supervisingDeptName);
                sheet_female.SetCellValue("A3", schoolName);
                sheet_female.SetCellValue("A5", semesterAndAcademic);

                for (int p = 0; p < lstEducationLevel.Count; p++)
                {
                    int countAllPupilOfEducation = 0;
                    int countRealPupilOfEducation = 0;
                    int countAllGoodPupil = 0;
                    double percentAllGoodPupil = 0;
                    int countAllFairPupil = 0;
                    double percentAllFairPupil = 0;
                    int startSum = startRow;
                    sheet_female.CopyPasteSameRowHeigh(rangeEducation, startGroupRow);

                    //Ten khoi
                    sheet_female.SetCellValue(startGroupRow, 1, lstEducationLevel[p].Resolution);

                    //Lấy danh sách lớp thuoc khối đó

                    //Danh sach lop thuoc khoi
                    List<ClassInfoBO> listFirstCP = ListClass.Where(o => o.EducationLevelID == lstEducationLevel[p].EducationLevelID)
                        .OrderBy(o => o.ClassOrderNumber.HasValue ? o.ClassOrderNumber : 0).ThenBy(o => o.ClassName).ToList();

                    //Duyet trong tung lop
                    int countCP = listFirstCP.Count;
                    for (int i = 0; i < countCP; i++)
                    {
                        //Copy row style
                        if (i == 0)
                        {
                            sheet_female.CopyPaste(firstRange, startRow, 2);
                        }
                        else if (i == countCP - 1)
                        {
                            sheet_female.CopyPaste(lastRange, startRow, 2);
                        }
                        else
                        {
                            sheet_female.CopyPaste(midRange, startRow, 2);
                        }

                        //Ten lop
                        sheet_female.SetCellValue(startRow, 2, listFirstCP[i].ClassName);

                        //Si so
                        int sumPupil = listFirstCP[i].NumOfPupil;
                        sheet_female.SetCellValue(startRow, 3, sumPupil);
                        countAllPupilOfEducation += sumPupil;

                        //Si so thuc te
                        //Sỹ số thực tế: Thực hiện việc count số lượng học sinh ở trạng thái đang học hoặc đã tốt nghiệp bằng cách count trong bảng PupilOfClass với các tham số truyền vào

                        int countRealPupil = listFirstCP[i].ActualNumOfPupil;
                        countRealPupilOfEducation += countRealPupil;
                        sheet_female.SetCellValue(startRow, 4, countRealPupil);

                        //Hoc sinh gioi
                        List<PupilEmulation> listGoodPupilEmulation = new List<PupilEmulation>();

                        listGoodPupilEmulation = listPupilEmulation.Where(o => (o.ClassID == listFirstCP[i].ClassID && o.HonourAchivementTypeID == excellentHonourID)).ToList();

                        double percentGood = 0;
                        if (countRealPupil > 0) percentGood = 100 * 1.0 * listGoodPupilEmulation.Count() / (countRealPupil * 1.0);
                        sheet_female.SetCellValue(startRow, 5, listGoodPupilEmulation.Count());
                        sheet_female.SetCellValue(startRow, 6, "=IF(D" + startRow + ">0" + "," + "ROUND(E" + startRow + "/" + "D" + startRow + "*100,2),0)");
                        countAllGoodPupil += listGoodPupilEmulation.Count();

                        //Hoc sinh tien tien
                        List<PupilEmulation> listRankingOfClassFair = new List<PupilEmulation>();
                        listRankingOfClassFair = listPupilEmulation.Where(o => (o.ClassID == listFirstCP[i].ClassID && o.HonourAchivementTypeID == goodHonourID)).ToList();

                        double percentFair = 0;
                        if (countRealPupil > 0) percentFair = 100 * listRankingOfClassFair.Count() / (countRealPupil * 1.0);
                        sheet_female.SetCellValue(startRow, 7, listRankingOfClassFair.Count());
                        sheet_female.SetCellValue(startRow, 8, "=IF(D" + startRow + ">0" + "," + "ROUND(G" + startRow + "/" + "D" + startRow + "*100,2),0)");
                        countAllFairPupil += listRankingOfClassFair.Count();

                        startRow++;
                    }

                    countAllPupilOfSchool += countAllPupilOfEducation;
                    countAllRealPupilOfSchool += countRealPupilOfEducation;
                    countAllGoodOfSchool += countAllGoodPupil;
                    countAllFairOfSchool += countAllFairPupil;
                    if (countRealPupilOfEducation > 0) percentAllGoodPupil = 100 * 1.0 * countAllGoodPupil / (1.0 * countRealPupilOfEducation);
                    if (countRealPupilOfEducation > 0) percentAllFairPupil = 100 * 1.0 * countAllFairPupil / (1.0 * countRealPupilOfEducation);

                    sheet_female.CopyPasteSameRowHeigh(rangeTC, startRow);

                    sheet_female.SetCellValue(startRow, 2, "TC");
                    sheet_female.SetCellValue(startRow, 3, "=SUM(C" + startSum.ToString() + ":C" + (startRow - 1).ToString() + ")");
                    sheet_female.SetCellValue(startRow, 4, "=SUM(D" + startSum.ToString() + ":D" + (startRow - 1).ToString() + ")");
                    sheet_female.SetCellValue(startRow, 5, "=SUM(E" + startSum.ToString() + ":E" + (startRow - 1).ToString() + ")");
                    sheet_female.SetCellValue(startRow, 6, "=IF(D" + startRow + ">0" + "," + "ROUND(E" + startRow + "/" + "D" + startRow + "*100,2),0)");
                    sheet_female.SetCellValue(startRow, 7, "=SUM(G" + startSum.ToString() + ":G" + (startRow - 1).ToString() + ")");
                    sheet_female.SetCellValue(startRow, 8, "=IF(D" + startRow + ">0" + "," + "ROUND(G" + startRow + "/" + "D" + startRow + "*100,2),0)");


                    if (listFirstCP.Count > 0)
                    {
                        sheet_female.MergeColumn(1, startGroupRow, startGroupRow + listFirstCP.Count);
                        sheet_female.GetRange(startGroupRow, 1, startGroupRow + listFirstCP.Count, 1).SetBorder(VTBorderStyle.Solid, VTBorderWeight.Medium, VTBorderIndex.EdgeLeft);
                    }

                    sheet_female.GetRange(startGroupRow, 1, startRow, 8).SetBorder(VTBorderStyle.Solid, VTBorderWeight.Medium, VTBorderIndex.Around);
                    startRow++;
                    startGroupRow = startGroupRow + listFirstCP.Count + 1;
                }

                sheet_female.CopyPaste(rangeTT, startRow, 1);

                double percentAllGoodOfSchoolFemale = 0;
                if (countAllRealPupilOfSchool > 0) percentAllGoodOfSchoolFemale = 100 * 1.0 * countAllGoodOfSchool / (1.0 * countAllRealPupilOfSchool);
                double percentAllFairOfSchoolFemale = 0;
                if (countAllRealPupilOfSchool > 0) percentAllFairOfSchoolFemale = 100 * 1.0 * countAllFairOfSchool / (1.0 * countAllRealPupilOfSchool);
                sheet_female.SetCellValue(startRow, 1, "T.Trường");
                sheet_female.SetCellValue(startRow, 2, "");
                sheet_female.SetCellValue(startRow, 3, "=SUM(C" + startmin.ToString() + ":C" + (startRow - 1).ToString() + ")/2");
                sheet_female.SetCellValue(startRow, 4, "=SUM(D" + startmin.ToString() + ":D" + (startRow - 1).ToString() + ")/2");
                sheet_female.SetCellValue(startRow, 5, "=SUM(E" + startmin.ToString() + ":E" + (startRow - 1).ToString() + ")/2");
                sheet_female.SetCellValue(startRow, 6, "=IF(D" + startRow + ">0" + "," + "ROUND(E" + startRow + "/" + "D" + startRow + "*100,2),0)");//Math.Round(percentAllGoodOfSchool, 2, MidpointRounding.AwayFromZero));
                sheet_female.SetCellValue(startRow, 7, "=SUM(G" + startmin.ToString() + ":G" + (startRow - 1).ToString() + ")/2");
                sheet_female.SetCellValue(startRow, 8, "=IF(D" + startRow + ">0" + "," + "ROUND(G" + startRow + "/" + "D" + startRow + "*100,2),0)");
                sheet_female.FitAllColumnsOnOnePage = true;
                sheet_female.Name = "HS_Nu";
            }
            #endregion

            #region Tao sheet dan toc
            if (entity.EthnicID > 0)
            {
                countAllPupilOfSchool = 0;
                countAllGoodOfSchool = 0;
                countAllFairOfSchool = 0;
                countAllRealPupilOfSchool = 0;

                startRow = 9;
                startGroupRow = 9;
                startmin = startRow;

                IDictionary<string, object> dicCATPEthnic = new Dictionary<string, object>()
                {
                    {"SchoolID",entity.SchoolID},
                    {"AcademicYearID",entity.AcademicYearID},
                    {"Semester",entity.Semester},
                    {"Type",SystemParamsInFile.GET_NUMPUPIL_IN_CLASS_TYPE0},
                    {"EthnicID",entity.EthnicID}                    
                };
                iqClassInfoBO = PupilOfClassBusiness.GetClassAndTotalPupil(lsClass, dicCATPEthnic);
                ListClass = iqClassInfoBO.ToList();

                //Chi lay thong tin cua hoc sinh dang hoc trong ky
                listPupilEmulation = (from u in listPupilEmulationAllStatus
                                      join poc in lstPupilOfClassSemesterEthnic on new { u.PupilID, u.ClassID } equals new { poc.PupilID,poc.ClassID}
                                      select u).ToList();



                // Tao sheet moi
                IVTWorksheet sheet_ethnic = oBook.CopySheetToLast(firstSheet, "H8");
                sheet_ethnic.SetCellValue("A2", supervisingDeptName);
                sheet_ethnic.SetCellValue("A3", schoolName);
                sheet_ethnic.SetCellValue("A5", semesterAndAcademic);

                for (int p = 0; p < lstEducationLevel.Count; p++)
                {
                    int countAllPupilOfEducation = 0;
                    int countRealPupilOfEducation = 0;
                    int countAllGoodPupil = 0;
                    double percentAllGoodPupil = 0;
                    int countAllFairPupil = 0;
                    double percentAllFairPupil = 0;
                    int startSum = startRow;
                    sheet_ethnic.CopyPasteSameRowHeigh(rangeEducation, startGroupRow);

                    //Ten khoi
                    sheet_ethnic.SetCellValue(startGroupRow, 1, lstEducationLevel[p].Resolution);

                    //Lấy danh sách lớp thuoc khối đó

                    //Danh sach lop thuoc khoi
                    List<ClassInfoBO> listFirstCP = ListClass.Where(o => o.EducationLevelID == lstEducationLevel[p].EducationLevelID)
                        .OrderBy(o => o.ClassOrderNumber.HasValue ? o.ClassOrderNumber : 0).ThenBy(o => o.ClassName).ToList();

                    //Duyet trong tung lop
                    int countCP = listFirstCP.Count;
                    for (int i = 0; i < countCP; i++)
                    {
                        //Copy row style
                        if (i == 0)
                        {
                            sheet_ethnic.CopyPaste(firstRange, startRow, 2);
                        }
                        else if (i == countCP - 1)
                        {
                            sheet_ethnic.CopyPaste(lastRange, startRow, 2);
                        }
                        else
                        {
                            sheet_ethnic.CopyPaste(midRange, startRow, 2);
                        }

                        //Ten lop
                        sheet_ethnic.SetCellValue(startRow, 2, listFirstCP[i].ClassName);

                        //Si so
                        int sumPupil = listFirstCP[i].NumOfPupil;
                        sheet_ethnic.SetCellValue(startRow, 3, sumPupil);
                        countAllPupilOfEducation += sumPupil;

                        //Si so thuc te
                        //Sỹ số thực tế: Thực hiện việc count số lượng học sinh ở trạng thái đang học hoặc đã tốt nghiệp bằng cách count trong bảng PupilOfClass với các tham số truyền vào

                        int countRealPupil = listFirstCP[i].ActualNumOfPupil;
                        countRealPupilOfEducation += countRealPupil;
                        sheet_ethnic.SetCellValue(startRow, 4, countRealPupil);

                        //Hoc sinh gioi
                        List<PupilEmulation> listGoodPupilEmulation = new List<PupilEmulation>();

                        listGoodPupilEmulation = listPupilEmulation.Where(o => (o.ClassID == listFirstCP[i].ClassID && o.HonourAchivementTypeID == excellentHonourID)).ToList();

                        double percentGood = 0;
                        if (countRealPupil > 0) percentGood = 100 * 1.0 * listGoodPupilEmulation.Count() / (countRealPupil * 1.0);
                        sheet_ethnic.SetCellValue(startRow, 5, listGoodPupilEmulation.Count());
                        sheet_ethnic.SetCellValue(startRow, 6, "=IF(D" + startRow + ">0" + "," + "ROUND(E" + startRow + "/" + "D" + startRow + "*100,2),0)");
                        countAllGoodPupil += listGoodPupilEmulation.Count();

                        //Hoc sinh tien tien
                        List<PupilEmulation> listRankingOfClassFair = new List<PupilEmulation>();
                        listRankingOfClassFair = listPupilEmulation.Where(o => (o.ClassID == listFirstCP[i].ClassID && o.HonourAchivementTypeID == goodHonourID)).ToList();

                        double percentFair = 0;
                        if (countRealPupil > 0) percentFair = 100 * listRankingOfClassFair.Count() / (countRealPupil * 1.0);
                        sheet_ethnic.SetCellValue(startRow, 7, listRankingOfClassFair.Count());
                        sheet_ethnic.SetCellValue(startRow, 8, "=IF(D" + startRow + ">0" + "," + "ROUND(G" + startRow + "/" + "D" + startRow + "*100,2),0)");
                        countAllFairPupil += listRankingOfClassFair.Count();

                        startRow++;
                    }

                    countAllPupilOfSchool += countAllPupilOfEducation;
                    countAllRealPupilOfSchool += countRealPupilOfEducation;
                    countAllGoodOfSchool += countAllGoodPupil;
                    countAllFairOfSchool += countAllFairPupil;
                    if (countRealPupilOfEducation > 0) percentAllGoodPupil = 100 * 1.0 * countAllGoodPupil / (1.0 * countRealPupilOfEducation);
                    if (countRealPupilOfEducation > 0) percentAllFairPupil = 100 * 1.0 * countAllFairPupil / (1.0 * countRealPupilOfEducation);

                    sheet_ethnic.CopyPasteSameRowHeigh(rangeTC, startRow);

                    sheet_ethnic.SetCellValue(startRow, 2, "TC");
                    sheet_ethnic.SetCellValue(startRow, 3, "=SUM(C" + startSum.ToString() + ":C" + (startRow - 1).ToString() + ")");
                    sheet_ethnic.SetCellValue(startRow, 4, "=SUM(D" + startSum.ToString() + ":D" + (startRow - 1).ToString() + ")");
                    sheet_ethnic.SetCellValue(startRow, 5, "=SUM(E" + startSum.ToString() + ":E" + (startRow - 1).ToString() + ")");
                    sheet_ethnic.SetCellValue(startRow, 6, "=IF(D" + startRow + ">0" + "," + "ROUND(E" + startRow + "/" + "D" + startRow + "*100,2),0)");
                    sheet_ethnic.SetCellValue(startRow, 7, "=SUM(G" + startSum.ToString() + ":G" + (startRow - 1).ToString() + ")");
                    sheet_ethnic.SetCellValue(startRow, 8, "=IF(D" + startRow + ">0" + "," + "ROUND(G" + startRow + "/" + "D" + startRow + "*100,2),0)");


                    if (listFirstCP.Count > 0)
                    {
                        sheet_ethnic.MergeColumn(1, startGroupRow, startGroupRow + listFirstCP.Count);
                        sheet_ethnic.GetRange(startGroupRow, 1, startGroupRow + listFirstCP.Count, 1).SetBorder(VTBorderStyle.Solid, VTBorderWeight.Medium, VTBorderIndex.EdgeLeft);
                    }

                    sheet_ethnic.GetRange(startGroupRow, 1, startRow, 8).SetBorder(VTBorderStyle.Solid, VTBorderWeight.Medium, VTBorderIndex.Around);
                    startRow++;
                    startGroupRow = startGroupRow + listFirstCP.Count + 1;
                }

                sheet_ethnic.CopyPaste(rangeTT, startRow, 1);

                double percentAllGoodOfSchoolFemale = 0;
                if (countAllRealPupilOfSchool > 0) percentAllGoodOfSchoolFemale = 100 * 1.0 * countAllGoodOfSchool / (1.0 * countAllRealPupilOfSchool);
                double percentAllFairOfSchoolFemale = 0;
                if (countAllRealPupilOfSchool > 0) percentAllFairOfSchoolFemale = 100 * 1.0 * countAllFairOfSchool / (1.0 * countAllRealPupilOfSchool);
                sheet_ethnic.SetCellValue(startRow, 1, "T.Trường");
                sheet_ethnic.SetCellValue(startRow, 2, "");
                sheet_ethnic.SetCellValue(startRow, 3, "=SUM(C" + startmin.ToString() + ":C" + (startRow - 1).ToString() + ")/2");
                sheet_ethnic.SetCellValue(startRow, 4, "=SUM(D" + startmin.ToString() + ":D" + (startRow - 1).ToString() + ")/2");
                sheet_ethnic.SetCellValue(startRow, 5, "=SUM(E" + startmin.ToString() + ":E" + (startRow - 1).ToString() + ")/2");
                sheet_ethnic.SetCellValue(startRow, 6, "=IF(D" + startRow + ">0" + "," + "ROUND(E" + startRow + "/" + "D" + startRow + "*100,2),0)");//Math.Round(percentAllGoodOfSchool, 2, MidpointRounding.AwayFromZero));
                sheet_ethnic.SetCellValue(startRow, 7, "=SUM(G" + startmin.ToString() + ":G" + (startRow - 1).ToString() + ")/2");
                sheet_ethnic.SetCellValue(startRow, 8, "=IF(D" + startRow + ">0" + "," + "ROUND(G" + startRow + "/" + "D" + startRow + "*100,2),0)");
                sheet_ethnic.FitAllColumnsOnOnePage = true;
                sheet_ethnic.Name = "HS_DanToc";
            }
            #endregion

            #region Tao sheet nu dan toc
            if (entity.FemaleEthnicID > 0)
            {
                countAllPupilOfSchool = 0;
                countAllGoodOfSchool = 0;
                countAllFairOfSchool = 0;
                countAllRealPupilOfSchool = 0;
                startRow = 9;
                startGroupRow = 9;
                startmin = startRow;

                IDictionary<string, object> dicCATPFeth = new Dictionary<string, object>()
                {
                    {"SchoolID",entity.SchoolID},
                    {"AcademicYearID",entity.AcademicYearID},
                    {"Semester",entity.Semester},
                    {"Type",SystemParamsInFile.GET_NUMPUPIL_IN_CLASS_TYPE0},
                    {"EthnicID",entity.EthnicID},
                    {"FemaleID",entity.FemaleID}
                };
                iqClassInfoBO = PupilOfClassBusiness.GetClassAndTotalPupil(lsClass, dicCATPFeth);
                ListClass = iqClassInfoBO.ToList();

                //Chi lay thong tin cua hoc sinh dang hoc trong ky
                listPupilEmulation = (from u in listPupilEmulationAllStatus
                                      join poc in lstPupilOfClassSemesterFemaleEthnic on new { u.PupilID, u.ClassID } equals new { poc.PupilID,poc.ClassID}
                                      select u).ToList();

                // Tao sheet moi
                IVTWorksheet sheet_ethnic = oBook.CopySheetToLast(firstSheet, "H8");
                sheet_ethnic.SetCellValue("A2", supervisingDeptName);
                sheet_ethnic.SetCellValue("A3", schoolName);
                sheet_ethnic.SetCellValue("A5", semesterAndAcademic);

                for (int p = 0; p < lstEducationLevel.Count; p++)
                {
                    int countAllPupilOfEducation = 0;
                    int countRealPupilOfEducation = 0;
                    int countAllGoodPupil = 0;
                    double percentAllGoodPupil = 0;
                    int countAllFairPupil = 0;
                    double percentAllFairPupil = 0;
                    int startSum = startRow;
                    sheet_ethnic.CopyPasteSameRowHeigh(rangeEducation, startGroupRow);

                    //Ten khoi
                    sheet_ethnic.SetCellValue(startGroupRow, 1, lstEducationLevel[p].Resolution);

                    //Lấy danh sách lớp thuoc khối đó

                    //Danh sach lop thuoc khoi
                    List<ClassInfoBO> listFirstCP = ListClass.Where(o => o.EducationLevelID == lstEducationLevel[p].EducationLevelID)
                        .OrderBy(o => o.ClassOrderNumber.HasValue ? o.ClassOrderNumber : 0).ThenBy(o => o.ClassName).ToList();

                    //Duyet trong tung lop
                    int countCP = listFirstCP.Count;
                    for (int i = 0; i < countCP; i++)
                    {
                        //Copy row style
                        if (i == 0)
                        {
                            sheet_ethnic.CopyPaste(firstRange, startRow, 2);
                        }
                        else if (i == countCP - 1)
                        {
                            sheet_ethnic.CopyPaste(lastRange, startRow, 2);
                        }
                        else
                        {
                            sheet_ethnic.CopyPaste(midRange, startRow, 2);
                        }

                        //Ten lop
                        sheet_ethnic.SetCellValue(startRow, 2, listFirstCP[i].ClassName);

                        //Si so
                        int sumPupil = listFirstCP[i].NumOfPupil;
                        sheet_ethnic.SetCellValue(startRow, 3, sumPupil);
                        countAllPupilOfEducation += sumPupil;

                        //Si so thuc te
                        //Sỹ số thực tế: Thực hiện việc count số lượng học sinh ở trạng thái đang học hoặc đã tốt nghiệp bằng cách count trong bảng PupilOfClass với các tham số truyền vào

                        int countRealPupil = listFirstCP[i].ActualNumOfPupil;
                        countRealPupilOfEducation += countRealPupil;
                        sheet_ethnic.SetCellValue(startRow, 4, countRealPupil);

                        //Hoc sinh gioi
                        List<PupilEmulation> listGoodPupilEmulation = new List<PupilEmulation>();

                        listGoodPupilEmulation = listPupilEmulation.Where(o => (o.ClassID == listFirstCP[i].ClassID && o.HonourAchivementTypeID == excellentHonourID)).ToList();

                        double percentGood = 0;
                        if (countRealPupil > 0) percentGood = 100 * 1.0 * listGoodPupilEmulation.Count() / (countRealPupil * 1.0);
                        sheet_ethnic.SetCellValue(startRow, 5, listGoodPupilEmulation.Count());
                        sheet_ethnic.SetCellValue(startRow, 6, "=IF(D" + startRow + ">0" + "," + "ROUND(E" + startRow + "/" + "D" + startRow + "*100,2),0)");
                        countAllGoodPupil += listGoodPupilEmulation.Count();

                        //Hoc sinh tien tien
                        List<PupilEmulation> listRankingOfClassFair = new List<PupilEmulation>();
                        listRankingOfClassFair = listPupilEmulation.Where(o => (o.ClassID == listFirstCP[i].ClassID && o.HonourAchivementTypeID == goodHonourID)).ToList();

                        double percentFair = 0;
                        if (countRealPupil > 0) percentFair = 100 * listRankingOfClassFair.Count() / (countRealPupil * 1.0);
                        sheet_ethnic.SetCellValue(startRow, 7, listRankingOfClassFair.Count());
                        sheet_ethnic.SetCellValue(startRow, 8, "=IF(D" + startRow + ">0" + "," + "ROUND(G" + startRow + "/" + "D" + startRow + "*100,2),0)");
                        countAllFairPupil += listRankingOfClassFair.Count();

                        startRow++;
                    }

                    countAllPupilOfSchool += countAllPupilOfEducation;
                    countAllRealPupilOfSchool += countRealPupilOfEducation;
                    countAllGoodOfSchool += countAllGoodPupil;
                    countAllFairOfSchool += countAllFairPupil;
                    if (countRealPupilOfEducation > 0) percentAllGoodPupil = 100 * 1.0 * countAllGoodPupil / (1.0 * countRealPupilOfEducation);
                    if (countRealPupilOfEducation > 0) percentAllFairPupil = 100 * 1.0 * countAllFairPupil / (1.0 * countRealPupilOfEducation);

                    sheet_ethnic.CopyPasteSameRowHeigh(rangeTC, startRow);

                    sheet_ethnic.SetCellValue(startRow, 2, "TC");
                    sheet_ethnic.SetCellValue(startRow, 3, "=SUM(C" + startSum.ToString() + ":C" + (startRow - 1).ToString() + ")");
                    sheet_ethnic.SetCellValue(startRow, 4, "=SUM(D" + startSum.ToString() + ":D" + (startRow - 1).ToString() + ")");
                    sheet_ethnic.SetCellValue(startRow, 5, "=SUM(E" + startSum.ToString() + ":E" + (startRow - 1).ToString() + ")");
                    sheet_ethnic.SetCellValue(startRow, 6, "=IF(D" + startRow + ">0" + "," + "ROUND(E" + startRow + "/" + "D" + startRow + "*100,2),0)");
                    sheet_ethnic.SetCellValue(startRow, 7, "=SUM(G" + startSum.ToString() + ":G" + (startRow - 1).ToString() + ")");
                    sheet_ethnic.SetCellValue(startRow, 8, "=IF(D" + startRow + ">0" + "," + "ROUND(G" + startRow + "/" + "D" + startRow + "*100,2),0)");


                    if (listFirstCP.Count > 0)
                    {
                        sheet_ethnic.MergeColumn(1, startGroupRow, startGroupRow + listFirstCP.Count);
                        sheet_ethnic.GetRange(startGroupRow, 1, startGroupRow + listFirstCP.Count, 1).SetBorder(VTBorderStyle.Solid, VTBorderWeight.Medium, VTBorderIndex.EdgeLeft);
                    }

                    sheet_ethnic.GetRange(startGroupRow, 1, startRow, 8).SetBorder(VTBorderStyle.Solid, VTBorderWeight.Medium, VTBorderIndex.Around);
                    startRow++;
                    startGroupRow = startGroupRow + listFirstCP.Count + 1;
                }

                sheet_ethnic.CopyPaste(rangeTT, startRow, 1);

                double percentAllGoodOfSchoolFemale = 0;
                if (countAllRealPupilOfSchool > 0) percentAllGoodOfSchoolFemale = 100 * 1.0 * countAllGoodOfSchool / (1.0 * countAllRealPupilOfSchool);
                double percentAllFairOfSchoolFemale = 0;
                if (countAllRealPupilOfSchool > 0) percentAllFairOfSchoolFemale = 100 * 1.0 * countAllFairOfSchool / (1.0 * countAllRealPupilOfSchool);
                sheet_ethnic.SetCellValue(startRow, 1, "T.Trường");
                sheet_ethnic.SetCellValue(startRow, 2, "");
                sheet_ethnic.SetCellValue(startRow, 3, "=SUM(C" + startmin.ToString() + ":C" + (startRow - 1).ToString() + ")/2");
                sheet_ethnic.SetCellValue(startRow, 4, "=SUM(D" + startmin.ToString() + ":D" + (startRow - 1).ToString() + ")/2");
                sheet_ethnic.SetCellValue(startRow, 5, "=SUM(E" + startmin.ToString() + ":E" + (startRow - 1).ToString() + ")/2");
                sheet_ethnic.SetCellValue(startRow, 6, "=IF(D" + startRow + ">0" + "," + "ROUND(E" + startRow + "/" + "D" + startRow + "*100,2),0)");//Math.Round(percentAllGoodOfSchool, 2, MidpointRounding.AwayFromZero));
                sheet_ethnic.SetCellValue(startRow, 7, "=SUM(G" + startmin.ToString() + ":G" + (startRow - 1).ToString() + ")/2");
                sheet_ethnic.SetCellValue(startRow, 8, "=IF(D" + startRow + ">0" + "," + "ROUND(G" + startRow + "/" + "D" + startRow + "*100,2),0)");
                sheet_ethnic.FitAllColumnsOnOnePage = true;
                sheet_ethnic.Name = "HS_Nu_DanToc";
            }
            #endregion

            firstSheet.Delete();
            return oBook.ToStream();
        }
        #endregion


    }
}

