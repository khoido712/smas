﻿/**
 * The Viettel License
 *
 * Copyright 2012 Viettel Telecom. All rights reserved.
 * VIETTEL PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */

/** 
* @author  
* @version $Revision: $
*/

using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;
using SMAS.Models.Models;
using SMAS.Business.BusinessObject;
using SMAS.Business.Common;
using System.Transactions;
using SMAS.VTUtils.Log;

namespace SMAS.Business.Business
{
    public partial class SummedEvaluationBusiness
    {

        #region Search
        public IQueryable<SummedEvaluationBO> Search(IDictionary<string,object> dic)
        {
            int academicYearID = Utils.GetInt(dic, "AcademicYearID");
            int schoolID = Utils.GetInt(dic, "SchoolID");
            int partition = UtilsBusiness.GetPartionId(schoolID);
            int semester = Utils.GetInt(dic, "Semester");
            int evaluationID = Utils.GetInt(dic, "EvaluationID");
            int classID = Utils.GetInt(dic, "ClassID");
            int subjectID = Utils.GetInt(dic, "SubjectID");
            int educationLevelID = Utils.GetInt(dic, "EducationLevelID");


            IQueryable<SummedEvaluationBO> query = from se in SummedEvaluationRepository.All
                                                   join pp in PupilProfileRepository.All on se.PupilID equals pp.PupilProfileID
                                                   join cp in ClassProfileRepository.All.Where(o => o.AcademicYearID == academicYearID && o.SchoolID == schoolID) on se.ClassID equals cp.ClassProfileID
                                                   where se.AcademicYearID == academicYearID && se.LastDigitSchoolID == partition && se.SchoolID == schoolID
                                                   && cp.IsActive == true
                                                   select new SummedEvaluationBO
                                                   {
                                                       EndingEvaluation=se.EndingEvaluation,
                                                       Genre=pp.Genre,
                                                       EthnicID=pp.EthnicID,
                                                       IsCombinedClass=cp.IsCombinedClass,
                                                       IsDisabled=pp.IsDisabled,
                                                       SemesterID=se.SemesterID,
                                                       EvaluationID=se.EvaluationID,
                                                       EvaluationCriteriaID=se.EvaluationCriteriaID,
                                                       ClassID=se.ClassID,
                                                       PupilID=se.PupilID,
                                                       EducationLevelID=se.EducationLevelID,
                                                       PeriodicEndingMark=se.PeriodicEndingMark,
                                                       EndingComment = se.EndingComments
                                                   };
            if (classID > 0)
            {
                query = query.Where(p => p.ClassID == classID);
            }
            if (subjectID > 0)
            {
                query = query.Where(p => p.EvaluationCriteriaID == subjectID);
            }
            if (evaluationID != 0)
            {
                query = query.Where(o => o.EvaluationID == evaluationID);
            }
            if (educationLevelID > 0)
            {
                query = query.Where(p => p.EducationLevelID == educationLevelID);
            }
            if (semester != 0)
            {
                query = query.Where(o => o.SemesterID == semester);
            }
            return query;
        }

        #endregion

        public void InsertOrUpdate(IDictionary<string, object> dic, List<SummedEvaluation> listInsertOrUpdate,List<ActionAuditDataBO> lstActionAuditData,ref ActionAuditDataBO objActionAuditBO)
        {
            try
            {
                this.context.Configuration.AutoDetectChangesEnabled = false;
                this.context.Configuration.ValidateOnSaveEnabled = false;
                int partitionId = Utils.GetInt(dic["PartitionId"]);
                int schoolId = Utils.GetInt(dic["SchoolId"]);
                int academicYearId = Utils.GetInt(dic["AcademicYearId"]);

                AcademicYear objAcadimicYear = AcademicYearBusiness.Find(academicYearId);
                bool isMoveHistory = UtilsBusiness.IsMoveHistory(objAcadimicYear);
                //Neu nam hoc dang thuc hien da chuyen di du lieu lich su thi thuc hien them moi cap nhat du lieu vao lich su
                if (isMoveHistory)
                {
                    SummedEvaluationHistoryBusiness.InsertOrUpdate(schoolId, dic, listInsertOrUpdate, lstActionAuditData, ref objActionAuditBO);
                    return;
                }

                int classId = Utils.GetInt(dic["ClassId"]);
                int educationLevelId = Utils.GetInt(dic["EducationLevelId"]);
                int semesterId = Utils.GetInt(dic["SemesterId"]);
                int evaluationId = Utils.GetInt(dic["EvaluationId"]);
                int evaluationCriteriaID = Utils.GetInt(dic["EvaluationCriteriaID"]);
                bool isRetest = Utils.GetBool(dic["IsRetest"]);
                int monthId = Utils.GetInt(dic["MonthId"]);
                IDictionary<string, object> objDic = new Dictionary<string, object>()
            {
                {"SchoolID",schoolId},
                {"AcademicYearID",academicYearId},
                {"ClassID",classId}
            };
                LockMarkPrimary objLockMarkPrimary = LockMarkPrimaryBusiness.SearchLockedMark(objDic).ToList().FirstOrDefault();
                string LockMarkTitle = objLockMarkPrimary != null ? objLockMarkPrimary.LockPrimary : "";
                //Thuc hien cap nhat du lieu hien tai
                List<SummedEvaluation> evaluationCommentsList = SummedEvaluationBusiness.All
                        .Where(p => p.LastDigitSchoolID == partitionId
                                 && p.ClassID == classId
                                 && p.AcademicYearID == academicYearId
                                 && p.SemesterID == semesterId
                                 && p.EvaluationID == evaluationId
                                 && (p.EvaluationCriteriaID == evaluationCriteriaID || evaluationCriteriaID == 0)
                                 ).ToList();

                List<SummedEvaluation> insertList = new List<SummedEvaluation>();
                List<SummedEvaluation> updateList = new List<SummedEvaluation>();
                SummedEvaluation checkInlist = null;
                SummedEvaluation objSE;
                string ck = (semesterId == GlobalConstants.SEMESTER_OF_YEAR_FIRST) ? "CK1" : "CK2";
                bool lockCK = UtilsBusiness.CheckLockMarkBySemester(semesterId, LockMarkTitle, ck, "");
                #region tao du lieu ghi log
                StringBuilder objectIDSummed = new StringBuilder();
                StringBuilder descriptionSummed = new StringBuilder();
                StringBuilder paramsSummed = new StringBuilder();
                StringBuilder oldObjectSummed = new StringBuilder();
                StringBuilder newObjectSummed = new StringBuilder();
                StringBuilder userFuntionsSummed = new StringBuilder();
                StringBuilder userActionsSummed = new StringBuilder();
                StringBuilder userDescriptionsSummed = new StringBuilder();

                StringBuilder objectIDStrtmp = new StringBuilder();
                StringBuilder descriptionStrtmp = new StringBuilder();
                StringBuilder paramsStrtmp = new StringBuilder();
                StringBuilder oldObjectStrtmp = new StringBuilder();
                StringBuilder newObjectStrtmp = new StringBuilder();
                StringBuilder userFuntionsStrtmp = new StringBuilder();
                StringBuilder userActionsStrtmp = new StringBuilder();
                StringBuilder userDescriptionsStrtmp = new StringBuilder();
                ActionAuditDataBO objAc = null;
                bool isChange = false;
                #endregion
                for (int i = 0; i < listInsertOrUpdate.Count; i++)
                {
                    isChange = false;
                    objSE = listInsertOrUpdate[i];
                    checkInlist = evaluationCommentsList.Where(p => p.PupilID == objSE.PupilID).FirstOrDefault();
                    objAc = lstActionAuditData.Where(p => p.PupilID == objSE.PupilID && p.ClassID == objSE.ClassID && p.EvaluationID == objSE.EvaluationID
                                                     && p.SubjectID == objSE.EvaluationCriteriaID).FirstOrDefault();
                    if (objAc != null)
                    {
                        objectIDStrtmp.Append(objAc.ObjID);
                        descriptionStrtmp.Append(objAc.Description);
                        paramsStrtmp.Append(objAc.Parameter);
                        oldObjectStrtmp.Append(objAc.OldData);
                        newObjectStrtmp.Append(objAc.NewData);
                        userFuntionsStrtmp.Append(objAc.UserFunction);
                        userActionsStrtmp.Append(objAc.UserAction);
                        userDescriptionsStrtmp.Append(objAc.UserDescription);
                    }
                    if (checkInlist == null)
                    {
                        objSE.SummedEvaluationID = SummedEvaluationBusiness.GetNextSeq<long>();
                        if (monthId == GlobalConstants.MonthID_11)
                        {
                            if (!string.IsNullOrEmpty(objSE.EndingComments))
                            {
                                newObjectStrtmp.Append("Nhận xét CK: ").Append(objSE.EndingComments).Append("; ");
                                oldObjectStrtmp.Append("Nhận xét CK: ").Append("; ");
                                isChange = true;
                            }
                        }

                        if (objSE.PeriodicEndingMark.HasValue)
                        {
                            newObjectStrtmp.Append("KTĐK CK: ").Append(objSE.PeriodicEndingMark).Append("; ");
                            oldObjectStrtmp.Append("KTĐK CK: ").Append("; ");
                            isChange = true;
                        }

                        if (!string.IsNullOrEmpty(objSE.EndingEvaluation))
                        {
                            newObjectStrtmp.Append("Đánh giá: ").Append(objSE.EndingEvaluation).Append("; ");
                            oldObjectStrtmp.Append("Đánh giá: ").Append("; ");
                            isChange = true;
                        }

                        if (isChange)
                        {
                            insertList.Add(objSE);
                        }
                    }
                    else
                    {
                        if (!lockCK)
                        {
                            if (monthId == GlobalConstants.MonthID_11)
                            {
                                if (!string.IsNullOrEmpty(checkInlist.EndingComments))
                                {
                                    if (!checkInlist.EndingComments.Equals(objSE.EndingComments))
                                    {
                                        oldObjectStrtmp.Append("Nhận xét CK: ").Append(checkInlist.EndingComments).Append("; ");
                                        newObjectStrtmp.Append("Nhận xét CK: ").Append(objSE.EndingComments).Append("; ");
                                        isChange = true;
                                        checkInlist.EndingComments = objSE.EndingComments;
                                    }
                                }
                                else if (!string.IsNullOrEmpty(objSE.EndingComments))
                                {
                                    oldObjectStrtmp.Append("Nhận xét CK: ").Append(checkInlist.EndingComments).Append("; ");
                                    newObjectStrtmp.Append("Nhận xét CK: ").Append(objSE.EndingComments).Append("; ");
                                    isChange = true;
                                    checkInlist.EndingComments = objSE.EndingComments;
                                }
                            }
                            //update thong tin
                            if (checkInlist.PeriodicEndingMark.HasValue)
                            {
                                if (checkInlist.PeriodicEndingMark != objSE.PeriodicEndingMark)
                                {
                                    oldObjectStrtmp.Append("KTĐK CK: ").Append(checkInlist.PeriodicEndingMark).Append("; ");
                                    newObjectStrtmp.Append("KTĐK CK: ").Append(objSE.PeriodicEndingMark).Append("; ");
                                    isChange = true;
                                    checkInlist.PeriodicEndingMark = objSE.PeriodicEndingMark;
                                }
                            }
                            else if (objSE.PeriodicEndingMark.HasValue)
                            {
                                oldObjectStrtmp.Append("KTĐK CK: ").Append(checkInlist.PeriodicEndingMark).Append("; ");
                                newObjectStrtmp.Append("KTĐK CK: ").Append(objSE.PeriodicEndingMark).Append("; ");
                                isChange = true;
                                checkInlist.PeriodicEndingMark = objSE.PeriodicEndingMark;
                            }

                            if (!string.IsNullOrEmpty(checkInlist.EndingEvaluation))
                            {
                                if (!checkInlist.EndingEvaluation.Equals(objSE.EndingEvaluation))
                                {
                                    oldObjectStrtmp.Append("Đánh giá: ").Append(checkInlist.EndingEvaluation).Append("; ");
                                    newObjectStrtmp.Append("Đánh giá: ").Append(objSE.EndingEvaluation).Append("; ");
                                    isChange = true;
                                    checkInlist.EndingEvaluation = objSE.EndingEvaluation;
                                }
                            }
                            else if (!string.IsNullOrEmpty(objSE.EndingEvaluation))
                            {
                                oldObjectStrtmp.Append("Đánh giá: ").Append(checkInlist.EndingEvaluation).Append("; ");
                                newObjectStrtmp.Append("Đánh giá: ").Append(objSE.EndingEvaluation).Append("; ");
                                isChange = true;
                                checkInlist.EndingEvaluation = objSE.EndingEvaluation;
                            }
                        }
                        //Neu hoc sinh co diem thi lai thi cap nhat diem thi lai
                        if (isRetest)
                        {
                            checkInlist.RetestMark = objSE.RetestMark;
                            checkInlist.EvaluationReTraining = objSE.EvaluationReTraining;
                        }
                        if (isChange)
                        {
                            checkInlist.UpdateTime = DateTime.Now;
                            updateList.Add(checkInlist);
                        }
                    }
                    
                    if (isChange)
                    {
                        string tmpOld = string.Empty;
                        string tmpNew = string.Empty;
                        tmpOld = oldObjectStrtmp.Length > 0 ? oldObjectStrtmp.ToString().Substring(0, oldObjectStrtmp.Length - 2) : "";
                        oldObjectSummed.Append("(Giá trị trước khi sửa): ").Append(tmpOld);
                        tmpNew = newObjectStrtmp.Length > 0 ? newObjectStrtmp.ToString().Substring(0, newObjectStrtmp.Length - 2) : "";
                        newObjectSummed.Append("(Giá trị sau khi sửa): ").Append(tmpNew);
                        userDescriptionsStrtmp.Append("Giá trị cũ {").Append(tmpOld).Append("}. ");
                        userDescriptionsStrtmp.Append("Giá trị mới {").Append(tmpNew).Append("}");

                        objectIDSummed.Append(objectIDStrtmp).Append(SMAS.Business.Common.GlobalConstants.WILD_LOG);
                        paramsSummed.Append(paramsStrtmp).Append(SMAS.Business.Common.GlobalConstants.WILD_LOG);
                        descriptionSummed.Append(descriptionStrtmp).Append(SMAS.Business.Common.GlobalConstants.WILD_LOG);
                        userFuntionsSummed.Append(userFuntionsStrtmp).Append(SMAS.Business.Common.GlobalConstants.WILD_LOG);
                        userActionsSummed.Append(userActionsStrtmp).Append(SMAS.Business.Common.GlobalConstants.WILD_LOG);
                        oldObjectSummed.Append(SMAS.Business.Common.GlobalConstants.WILD_LOG);
                        newObjectSummed.Append(SMAS.Business.Common.GlobalConstants.WILD_LOG);
                        userDescriptionsSummed.Append(userDescriptionsStrtmp).Append(SMAS.Business.Common.GlobalConstants.WILD_LOG);

                    }
                    else if (objAc != null && objAc.isInsertLog)
                    {
                        objectIDSummed.Append(objAc.ObjID).Append(SMAS.Business.Common.GlobalConstants.WILD_LOG);
                        paramsSummed.Append(objAc.Parameter).Append(SMAS.Business.Common.GlobalConstants.WILD_LOG);
                        descriptionSummed.Append(objAc.Description).Append(SMAS.Business.Common.GlobalConstants.WILD_LOG);
                        userFuntionsSummed.Append(objAc.UserFunction).Append(SMAS.Business.Common.GlobalConstants.WILD_LOG);
                        userActionsSummed.Append(objAc.UserAction).Append(SMAS.Business.Common.GlobalConstants.WILD_LOG);
                        string tmpOld = string.Empty;
                        string tmpNew = string.Empty;
                        tmpOld = objAc.OldData.Length > 0 ? objAc.OldData.ToString().Substring(0, objAc.OldData.Length - 2) : "";
                        oldObjectSummed.Append("(Giá trị trước khi sửa): ").Append(tmpOld);
                        tmpNew = objAc.NewData.Length > 0 ? objAc.NewData.ToString().Substring(0, objAc.NewData.Length - 2) : "";
                        newObjectSummed.Append("(Giá trị sau khi sửa): ").Append(tmpNew);
                        userDescriptionsStrtmp.Append("Giá trị cũ {").Append(tmpOld).Append("}. ");
                        userDescriptionsStrtmp.Append("Giá trị mới {").Append(tmpNew).Append("}");
                        oldObjectSummed.Append(SMAS.Business.Common.GlobalConstants.WILD_LOG);
                        newObjectSummed.Append(SMAS.Business.Common.GlobalConstants.WILD_LOG);
                        userDescriptionsSummed.Append(userDescriptionsStrtmp).Append(SMAS.Business.Common.GlobalConstants.WILD_LOG);
                    }

                    objectIDStrtmp = new StringBuilder();
                    descriptionStrtmp = new StringBuilder();
                    paramsStrtmp = new StringBuilder();
                    oldObjectStrtmp = new StringBuilder();
                    newObjectStrtmp = new StringBuilder();
                    userFuntionsStrtmp = new StringBuilder();
                    userActionsStrtmp = new StringBuilder();
                    userDescriptionsStrtmp = new StringBuilder();
                }

                objActionAuditBO.ObjID = objectIDSummed;
                objActionAuditBO.Parameter = paramsSummed;
                objActionAuditBO.UserFunction = userFuntionsSummed;
                objActionAuditBO.UserAction = userActionsSummed;
                objActionAuditBO.UserDescription = userDescriptionsSummed;
                objActionAuditBO.Description = descriptionSummed;
                objActionAuditBO.OldData = oldObjectSummed;
                objActionAuditBO.NewData = newObjectSummed;

                //Kiem tra nam hoc dang thuc hien da chuyen du lieu lich su
                IDictionary<string, object> columnMap = ColumnMappings();
                if (insertList != null && insertList.Count > 0)
                {
                    for (int i = 0; i < insertList.Count; i++)
                    {
                        this.Insert(insertList[i]);
                    }
                }
                if (updateList != null && updateList.Count > 0)
                {
                    for (int i = 0; i < updateList.Count; i++)
                    {
                        this.Update(updateList[i]);
                    }
                }
                SummedEndingEvaluationBusiness.Save();
            }
            catch (Exception ex)
            {                
                LogExtensions.ErrorExt(logger, DateTime.Now, "InsertOrUpdate", "null",ex);

            }
            finally
            {
                this.context.Configuration.AutoDetectChangesEnabled = true;
                this.context.Configuration.ValidateOnSaveEnabled = true;
            }

        }


        public void InsertOrUpdatebyClass(IDictionary<string, object> dic, List<SummedEvaluation> listInsertOrUpdate)
        {
            try
            {
                this.context.Configuration.AutoDetectChangesEnabled = false;
                this.context.Configuration.ValidateOnSaveEnabled = false;
                int partitionId = Utils.GetInt(dic["PartitionId"]);
                int schoolId = Utils.GetInt(dic["SchoolId"]);
                int academicYearId = Utils.GetInt(dic["AcademicYearId"]);

                AcademicYear objAcadimicYear = AcademicYearBusiness.Find(academicYearId);
                bool isMoveHistory = UtilsBusiness.IsMoveHistory(objAcadimicYear);
                //Neu nam hoc dang thuc hien da chuyen di du lieu lich su thi thuc hien them moi cap nhat du lieu vao lich su
                if (isMoveHistory)
                {
                    SummedEvaluationHistoryBusiness.InsertOrUpdatebyClass(dic, listInsertOrUpdate);
                    return;
                }

                int classId = Utils.GetInt(dic["ClassId"]);
                int educationLevelId = Utils.GetInt(dic["EducationLevelId"]);
                int semesterId = Utils.GetInt(dic["SemesterId"]);
                bool isRetest = Utils.GetBool(dic["IsRetest"]);
                IDictionary<string, object> objDic = new Dictionary<string, object>()
                {
                    {"SchoolID",schoolId},
                    {"AcademicYearID",academicYearId},
                    {"ClassID",classId}
                };
                LockMarkPrimary objLockMarkPrimary = LockMarkPrimaryBusiness.SearchLockedMark(objDic).ToList().FirstOrDefault();
                string LockMarkTitle = objLockMarkPrimary != null ? objLockMarkPrimary.LockPrimary : "";
                //Thuc hien cap nhat du lieu hien tai
                List<SummedEvaluation> evaluationCommentsList = SummedEvaluationBusiness.All
                        .Where(p => p.LastDigitSchoolID == partitionId

                                 && p.ClassID == classId
                                 && p.AcademicYearID == academicYearId
                                 && p.SemesterID == semesterId
                                 ).ToList();

                List<SummedEvaluation> insertList = new List<SummedEvaluation>();
                List<SummedEvaluation> updateList = new List<SummedEvaluation>();
                SummedEvaluation checkInlist = null;
                SummedEvaluation objSE;
                string ck = (semesterId == GlobalConstants.SEMESTER_OF_YEAR_FIRST) ? "CK1" : "CK2";
                bool lockCK = UtilsBusiness.CheckLockMarkBySemester(semesterId, LockMarkTitle, ck, "");
                for (int i = 0; i < listInsertOrUpdate.Count; i++)
                {
                    objSE = listInsertOrUpdate[i];
                    checkInlist = evaluationCommentsList.Where(p => p.PupilID == objSE.PupilID
                                                                && p.EvaluationID == objSE.EvaluationID
                                                                && p.EvaluationCriteriaID == objSE.EvaluationCriteriaID).FirstOrDefault();
                    if (checkInlist == null)
                    {
                        objSE.SummedEvaluationID = GetNextSeq<long>();
                        if (!lockCK)
                        {
                            objSE.CreateTime = DateTime.Now;
                            insertList.Add(objSE);    
                        }
                    }
                    else
                    {
                        if (!lockCK)//kiem tra neu co thay doi moi luu
                        {
                            //update thong tin
                            checkInlist.PeriodicEndingMark = objSE.PeriodicEndingMark;
                            checkInlist.EndingComments = objSE.EndingComments;
                            checkInlist.EndingEvaluation = objSE.EndingEvaluation;
                            checkInlist.UpdateTime = DateTime.Now;
                        }
                        //Neu hoc sinh co diem thi lai thi cap nhat diem thi lai
                        if (isRetest)
                        {
                            checkInlist.RetestMark = objSE.RetestMark;
                            checkInlist.EvaluationReTraining = objSE.EvaluationReTraining;
                        }
                        //Khong cap nhat diem thi lai 
                        updateList.Add(checkInlist);
                    }
                }

                //Kiem tra nam hoc dang thuc hien da chuyen du lieu lich su
                IDictionary<string, object> columnMap = ColumnMappings();
                if (insertList != null && insertList.Count > 0)
                {
                    //BulkInsert(insertList, columnMap);
                    for (int i = 0; i < insertList.Count; i++)
                    {
                        this.Insert(insertList[i]);
                    }

                }
                if (updateList != null && updateList.Count > 0)
                {
                    for (int i = 0; i < updateList.Count; i++)
                    {
                        this.Update(updateList[i]);
                    }
                }
                this.Save();
            }
            catch (Exception ex)
            {

                LogExtensions.ErrorExt(logger, DateTime.Now, "InsertOrUpdatebyClass", "", ex);
            }
            finally
            {
                this.context.Configuration.AutoDetectChangesEnabled = true;
                this.context.Configuration.ValidateOnSaveEnabled = true;
            }
        }

        /// <summary>
        /// Mapping column tu entity sang table
        /// </summary>
        /// <returns></returns>
        public IDictionary<string, object> ColumnMappings()
        {
            Dictionary<string, object> columnMap = new Dictionary<string, object>();
            columnMap.Add("SummedEvaluationID", "SUMMED_EVALUATION_ID");
            columnMap.Add("PupilID", "PUPIL_ID");
            columnMap.Add("ClassID", "CLASS_ID");
            columnMap.Add("EducationLevelID", "EDUCATION_LEVEL_ID");
            columnMap.Add("AcademicYearID", "ACADEMIC_YEAR_ID");
            columnMap.Add("LastDigitSchoolID", "LAST_DIGIT_SCHOOL_ID");
            columnMap.Add("SchoolID", "SCHOOL_ID");
            columnMap.Add("SemesterID", "SEMESTER_ID");
            columnMap.Add("EvaluationCriteriaID", "EVALUATION_CRITERIA_ID");
            columnMap.Add("EvaluationID", "EVALUATION_ID");
            columnMap.Add("PeriodicEndingMark", "PERIODIC_ENDING_MARK");
            columnMap.Add("EndingEvaluation", "ENDING_EVALUATION");
            columnMap.Add("RetestMark", "RETEST_MARK");
            columnMap.Add("EvaluationReTraining", "EVALUATION_RETRAINING");
            columnMap.Add("CreateTime", "CREATE_TIME");
            columnMap.Add("UpdateTime", "UPDATE_TIME");
            columnMap.Add("EndingComments", "ENDING_COMMENTS");

            return columnMap;
        }

        public List<SummedEvaluationBO> GetSummedEvaluationByClass(IDictionary<string, object> dic)
        {
            int ClassID = Utils.GetInt(dic, "ClassID");
            int SchoolID = Utils.GetInt(dic, "SchoolID");
            int AcademicYearID = Utils.GetInt(dic, "AcademicYearID");
            int SemesterID = Utils.GetInt(dic, "SemesterID");
            int Year = Utils.GetInt(dic, "Year");
            int EvaluationID = Utils.GetInt(dic, "EvaluationID"); 
            int LastDigit = UtilsBusiness.GetPartionId(SchoolID);
            List<SummedEvaluationBO> lstSummedEvaluation = new List<SummedEvaluationBO>();
            AcademicYear objAcademicYear = AcademicYearBusiness.Find(AcademicYearID);
            bool isMovedHistory = UtilsBusiness.IsMoveHistory(objAcademicYear);
            if (isMovedHistory)
            {
                lstSummedEvaluation = (from s in SummedEvaluationHistoryBusiness.All
                                       where s.SchoolID == SchoolID
                                       && s.AcademicYearID == AcademicYearID
                                       && s.ClassID == ClassID
                                       && s.SemesterID == SemesterID
                                       && s.LastDigitSchoolID == LastDigit
                                       && (s.EvaluationID == EvaluationID || EvaluationID == 0)
                                       select new SummedEvaluationBO
                                       {
                                           PupilID = s.PupilID,
                                           ClassID = s.ClassID,
                                           Mark = s.PeriodicEndingMark,
                                           EvaluationCriteriaID = s.EvaluationCriteriaID,
                                           EndingEvaluation = s.EndingEvaluation
                                       }).ToList();
            }
            else
            {
                lstSummedEvaluation = (from s in SummedEvaluationBusiness.All
                                       where s.SchoolID == SchoolID
                                       && s.AcademicYearID == AcademicYearID
                                       && s.ClassID == ClassID
                                       && s.SemesterID == SemesterID
                                       && s.LastDigitSchoolID == LastDigit
                                       && (s.EvaluationID == EvaluationID || EvaluationID == 0)
                                       select new SummedEvaluationBO
                                       {
                                           PupilID = s.PupilID,
                                           ClassID = s.ClassID,
                                           Mark = s.PeriodicEndingMark,
                                           EvaluationCriteriaID = s.EvaluationCriteriaID,
                                           EndingEvaluation = s.EndingEvaluation
                                       }).ToList();
            }
            
            return lstSummedEvaluation;
        }

        public void ImportFromInputMark(List<Object> insertList, List<Object> updateList)
        {
            try
            {
                this.context.Configuration.AutoDetectChangesEnabled = false;
                this.context.Configuration.ValidateOnSaveEnabled = false;

                for (int i = 0; i < insertList.Count; i++)
                {
                    SummedEvaluation entity = (SummedEvaluation)insertList[i];
                    SummedEvaluationBusiness.Insert(entity);
                }
                for (int i = 0; i < updateList.Count; i++)
                {
                    SummedEvaluation entity = (SummedEvaluation)updateList[i];
                    SummedEvaluationBusiness.Update(entity);
                }

                SummedEvaluationBusiness.Save();
            }
            finally
            {
                this.context.Configuration.AutoDetectChangesEnabled = true;
                this.context.Configuration.ValidateOnSaveEnabled = true;
            }
        }


        public IQueryable<SummedEvaluation> getListSummedEvaluationByListSubject(IDictionary<string, object> dic)
        {
            int schoolId = Utils.GetInt(dic["SchoolId"]);
            int partitionId = UtilsBusiness.GetPartionId(schoolId);
            int academicYearId = Utils.GetInt(dic["AcademicYearId"]);
            int semesterId = Utils.GetInt(dic["SemesterId"]);
            List<int> listSubjectId = Utils.GetIntList(dic["listSubjectId"]);
            int evaluationId = Utils.GetInt(dic["EvaluationId"]);
            int educationLevelId = Utils.GetInt(dic["EduationLevelId"]);
            int classId = Utils.GetInt(dic["ClassId"]);
            List<int> lstPupilId = Utils.GetIntList(dic,"lstPupilID");

            IQueryable<SummedEvaluation> iQuery = SummedEvaluationBusiness.All;
            if (schoolId > 0)
            {
                iQuery = iQuery.Where(p => p.SchoolID == schoolId);
            }
            if (partitionId > 0)
            {
                iQuery = iQuery.Where(p => p.LastDigitSchoolID == partitionId);
            }
            if (academicYearId > 0)
            {
                iQuery = iQuery.Where(p => p.AcademicYearID == academicYearId);
            }
            if (lstPupilId.Count > 0)
            {
                iQuery = iQuery.Where(p => lstPupilId.Contains(p.PupilID));
            }
            if (semesterId > 0)
            {
                iQuery = iQuery.Where(p => p.SemesterID == semesterId);
            }
            if (evaluationId > 0)
            {
                iQuery = iQuery.Where(p => p.EvaluationID == evaluationId);
            }
            if (educationLevelId > 0)
            {
                iQuery = iQuery.Where(p => p.EducationLevelID == educationLevelId);
            }
            if (classId > 0)
            {
                iQuery = iQuery.Where(p => p.ClassID == classId);
            }
            if (listSubjectId != null && listSubjectId.Count > 0)
            {
                iQuery = iQuery.Where(p => listSubjectId.Contains(p.EvaluationCriteriaID));
            }

            return iQuery;
        }


        public List<string> GetListComment(IDictionary<string, object> dic)
        {
            int SchoolID = Utils.GetInt(dic, "SchoolID");
            int AcademicYearID = Utils.GetInt(dic, "AcademicYearID");
            int EvaluationID = Utils.GetInt(dic, "EvaluationID");            
            int ClassID = Utils.GetInt(dic, "ClassID");
            int SemesterID = Utils.GetInt(dic, "SemesterID");
            List<string> lstComment = new List<string>();
            int partition = UtilsBusiness.GetPartionId(SchoolID);

            IQueryable<SummedEvaluation> lstEvaluationComments = (from e in SummedEvaluationBusiness.All.AsNoTracking()
                                                                    where e.SchoolID == SchoolID
                                                                    && e.AcademicYearID == AcademicYearID
                                                                    && e.ClassID == ClassID
                                                                    && e.EvaluationID == EvaluationID                                                                    
                                                                    && (e.SemesterID == SemesterID || SemesterID == 0)
                                                                    && e.LastDigitSchoolID == partition
                                                                    select e);

            lstComment = lstEvaluationComments.Where(p => !string.IsNullOrEmpty(p.EndingComments)).Select(p => p.EndingComments).ToList();            
            return lstComment.ToList();
        }
    }
}
