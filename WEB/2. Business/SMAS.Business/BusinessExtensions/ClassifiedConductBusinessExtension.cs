using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SMAS.Business.BusinessObject;
using SMAS.Business.Business;
using SMAS.Business.Common;
using SMAS.Models.Models;
using System.IO;
using SMAS.VTUtils.Excel.Export;
using SMAS.DAL.Repository;

namespace SMAS.Business.Business
{
    public partial class ClassifiedConductBusiness
    {
        #region Lấy mảng băm cho các tham số đầu vào
        /// <summary>
        /// minhh - 26/11/2012
        ///Lấy mảng băm cho các tham số đầu vào 
        /// </summary>
        /// <param name="entity"></param>
        /// <returns></returns>
        public string GetHashKey(ClassifiedConduct entity)
        {
            IDictionary<string, object> dic = new Dictionary<string, object> {
                {"Semester", entity.Semester},
                {"AcademicYearID", entity.AcademicYearID},
                {"AppliedLevel", entity.AppliedLevel}
            };
            return ReportUtils.GetHashKey(dic);
        }
        #endregion

        #region Lưu lại thông tin
        /// <summary>
        /// minhh - 26/11/2012
        /// Lưu lại thông tin 
        /// </summary>
        /// <param name="entity"></param>
        /// <param name="data"></param>
        /// <returns></returns>
        public ProcessedReport InsertClassifiedConduct(ClassifiedConduct entity, Stream data)
        {

            string reportCode = SystemParamsInFile.REPORT_THONG_KE_XEP_LOAI_HANH_KIEM;

            ReportDefinition reportDef = ReportDefinitionBusiness.GetByCode(reportCode);
            ProcessedReport pr = new ProcessedReport();
            pr.ReportCode = reportCode;
            pr.ProcessedDate = DateTime.Now;
            pr.InputParameterHashKey = GetHashKey(entity);
            pr.ReportData = ReportUtils.Compress(data);

            //Tạo tên file
            string outputNamePattern = reportDef.OutputNamePattern;
            string semester = ReportUtils.ConvertSemesterForReportName(entity.Semester);

            outputNamePattern = outputNamePattern.Replace("[SchoolLevel]", ReportUtils.ConvertAppliedLevelForReportName(entity.AppliedLevel));
            outputNamePattern = outputNamePattern.Replace("[Semester]", semester);
            pr.ReportName = ReportUtils.RemoveSpecialCharacters(outputNamePattern) + "." + reportDef.OutputFormat;

            IDictionary<string, object> dic = new Dictionary<string, object> {
                {"Semester", entity.Semester},
                {"AcademicYearID", entity.AcademicYearID}
            };
            ProcessedReportParameterRepository.Insert(dic, pr);
            ProcessedReportRepository.Insert(pr);
            ProcessedReportRepository.Save();
            return pr;
        }
        #endregion

        #region Lấy bảng thống kê xếp hạng hạnh kiểm được cập nhật mới nhất
        /// <summary>
        /// <author>minhh</author>
        /// Lấy bảng thống kê xếp hạng hạnh kiểm được cập nhật mới nhất
        /// </summary>
        /// <param name="entity"></param>
        /// <returns></returns>
        public ProcessedReport GetClassifiedConduct(ClassifiedConduct entity)
        {
            string reportCode = SystemParamsInFile.REPORT_THONG_KE_XEP_LOAI_HANH_KIEM;
            string inputParameterHashKey = GetHashKey(entity);
            return ProcessedReportBusiness.GetProcessedReport(reportCode, inputParameterHashKey);
        }
        #endregion

        #region Lưu lại thông tin xếp hạng hạnh kiểm
        /// <summary>
        /// <author>minhh</author>
        /// Lưu lại thông tin xếp hạng hạnh kiểm
        /// </summary>
        /// <param name="entity"></param>
        /// <returns></returns>
        public Stream CreateClassifiedConduct(ClassifiedConduct entity)
        {
            string reportCode = SystemParamsInFile.REPORT_THONG_KE_XEP_LOAI_HANH_KIEM;
            ReportDefinition reportDef = ReportDefinitionBusiness.GetByCode(reportCode);
            string templatePath = SystemParamsInFile.TEMPLATE_FOLDER + reportDef.TemplateDirectory + "/" + reportDef.TemplateName;
            IVTWorkbook oBook = VTExport.OpenWorkbook(templatePath);

            //Lấy sheet 
            IVTWorksheet firstSheet = oBook.GetSheet(1);
            //fill dữ liệu vào sheet thông tin chung
            SchoolProfile school = SchoolProfileBusiness.Find(entity.SchoolID);
            string supervisingDeptName = UtilsBusiness.GetSupervisingDeptName(school.SchoolProfileID, entity.AppliedLevel).ToUpper();
            string schoolName = school.SchoolName.ToUpper();
            string provinceName = (school.District != null ? school.District.DistrictName : "");
            string dateTime = "ngày " + DateTime.Now.Date.Day + " tháng " + DateTime.Now.Month + " năm " + DateTime.Now.Year;
            string provinceAndDate = provinceName + ", " + dateTime;
            AcademicYear academicYear = AcademicYearBusiness.Find(entity.AcademicYearID);

            string semester = "";
            if (entity.Semester == SystemParamsInFile.SEMESTER_OF_YEAR_FIRST) semester = "Học kỳ I";
            if (entity.Semester == SystemParamsInFile.SEMESTER_OF_YEAR_SECOND) semester = "Học kỳ II";
           
            string semesterAndAcademic = semester.ToUpper() + " - NĂM HỌC: " + academicYear.DisplayTitle;
            if (entity.Semester == SystemParamsInFile.SEMESTER_OF_YEAR_ALL)
            {
                semesterAndAcademic =  "NĂM HỌC: " + academicYear.DisplayTitle;
            }
           

            List<EducationLevel> lstEducationLevel = EducationLevelBusiness.GetByGrade(entity.AppliedLevel).ToList();
            
            IVTRange range = firstSheet.GetRange("A13", "O13");
            IVTRange rangeEducation = firstSheet.GetRange("A12", "O12");
            IVTRange rangeBoldFont = firstSheet.GetRange("G2", "J2");

            Dictionary<string, object> dic = new Dictionary<string, object> 
            {
                {"AcademicYearID", entity.AcademicYearID}, 
                {"AppliedLevel", entity.AppliedLevel},
                {"Year", academicYear.Year}
            };

            IQueryable<VPupilRanking> listQRanking = VPupilRankingBusiness.SearchBySchool(entity.SchoolID, dic);
            IQueryable<VPupilRanking> lstRanking_Temp = null;
            if (entity.Semester <= SystemParamsInFile.SEMESTER_OF_YEAR_SECOND)
            {
                lstRanking_Temp = listQRanking.Where(o => o.Semester == entity.Semester);
            }
            if (entity.Semester == SystemParamsInFile.SEMESTER_OF_YEAR_ALL)
            {
                listQRanking = listQRanking.Where(o => o.Semester >= SystemParamsInFile.SEMESTER_OF_YEAR_ALL);
                lstRanking_Temp = listQRanking.Where(u => u.Semester == listQRanking.Where(v => v.PupilID == u.PupilID && v.ClassID == u.ClassID).Max(v => v.Semester));
            }
            //Si so 
            AcademicYear Aca = AcademicYearBusiness.Find(entity.AcademicYearID);
            Dictionary<string, object> dicPupilRanking = new Dictionary<string, object> 
            {
                {"AcademicYearID", entity.AcademicYearID},
                {"SchoolID", entity.SchoolID},
                {"AppliedLevel", entity.AppliedLevel}
            };

            IQueryable<PupilOfClass> lstQPoc = PupilOfClassBusiness.SearchBySchool(entity.SchoolID, dicPupilRanking);
            IQueryable<PupilOfClassBO> lstPoc = from p in lstQPoc.AddCriteriaSemester(Aca, entity.Semester)
                                                join q in PupilProfileBusiness.All on p.PupilID equals q.PupilProfileID
                                                join r in ClassProfileBusiness.All on p.ClassID equals r.ClassProfileID
                                                where !r.IsVnenClass.HasValue || (r.IsVnenClass.HasValue && r.IsVnenClass.Value == false)
                                                && r.IsActive.Value
                                                select new PupilOfClassBO
                                                {
                                                    PupilID = p.PupilID,
                                                    ClassID = p.ClassID,
                                                    EducationLevelID = r.EducationLevelID,
                                                    Genre = q.Genre,
                                                    EthnicID = q.EthnicID
                                                };


            //List<PupilOfClassBO> lstQPoc = PupilOfClassBusiness.GetPupilOfClassInSemester(entity.AcademicYearID, entity.SchoolID, null, entity.Semester, null, entity.AppliedLevel);
            //Si so thuc te
            var lstRanking = (from pr in lstRanking_Temp
                              join q in PupilProfileBusiness.All on pr.PupilID equals q.PupilProfileID
                              join r in ClassProfileBusiness.All on pr.ClassID equals r.ClassProfileID
                              where lstPoc.Any(o => o.PupilID == pr.PupilID && o.ClassID == pr.ClassID)
                              && r.IsActive.Value
                              select new 
                              {   PupilID = pr.PupilID, 
                                  ClassID = pr.ClassID,
                                  ConductLevelID = pr.ConductLevelID,
                                  Genre = q.Genre,
                                  EducationLevelID = r.EducationLevelID, 
                                  EthnicID = q.EthnicID 
                              }).ToList();
            // Lay danh sach hoc sinh theo lop
            //var lstCountCurentPupilByClass = lstQPoc.AddCriteriaSemester(Aca, entity.Semester).ToList();
            //var lstCountAllPupilByClass = this.PupilOfClassBusiness.SearchBySchool(entity.SchoolID, new Dictionary<string, object> {
            //    {"AcademicYearID", entity.AcademicYearID},
            //    {"SchoolID", entity.SchoolID},
            //    {"AppliedLevel", entity.AppliedLevel},
            //}).AddCriteriaSemester(Aca,entity.Semester).ToList();
             //ID dan toc kinh, nuoc ngoai
            Ethnic Ethnic_Kinh = EthnicBusiness.All.Where(o => o.EthnicCode.ToLower().Equals(SystemParamsInFile.ETHNIC_CODE_KINH.ToLower()) && o.IsActive == true).FirstOrDefault();
            Ethnic Ethnic_ForeignPeople = EthnicBusiness.All.Where(o => o.EthnicCode.ToLower().Equals(SystemParamsInFile.ETHNIC_CODE_NN.ToLower()) && o.IsActive == true).FirstOrDefault();
            int? EthnicID_Kinh = 0;
            int? EthnicID_ForeignPeople = 0;
            if (Ethnic_Kinh != null)
            {
                EthnicID_Kinh = Ethnic_Kinh.EthnicID;
            }
            if (Ethnic_ForeignPeople != null)
            {
                EthnicID_ForeignPeople = Ethnic_ForeignPeople.EthnicID;
            }
            var listCountCurentPupilByClass = lstPoc.GroupBy(o => new { o.EducationLevelID, o.ClassID, o.Genre, o.EthnicID }).Select(o => new {EducationLevelID = o.Key.EducationLevelID,  ClassID = o.Key.ClassID, Genre = o.Key.Genre, EthnicID = o.Key.EthnicID, TotalPupil = o.Count() }).ToList();
            var listCountCurentPupilByClass_Female = listCountCurentPupilByClass.Where(o => o.Genre == SystemParamsInFile.GENRE_FEMALE).ToList();
            var listCountCurentPupilByClass_Ethnic = listCountCurentPupilByClass.Where(o => o.EthnicID.HasValue && o.EthnicID != EthnicID_Kinh && o.EthnicID != EthnicID_ForeignPeople).ToList();
            var listCountCurentPupilByClass_FemaleEthnic = listCountCurentPupilByClass.Where(o => o.Genre == SystemParamsInFile.GENRE_FEMALE && o.EthnicID.HasValue && o.EthnicID != EthnicID_Kinh && o.EthnicID != EthnicID_ForeignPeople).ToList();
            var lstRanking_Ethnic = lstRanking.Where(o => o.EthnicID.HasValue && o.EthnicID != EthnicID_Kinh && o.EthnicID != EthnicID_ForeignPeople).ToList();
            var lstRanking_Female = lstRanking.Where(o => o.Genre == SystemParamsInFile.GENRE_FEMALE).ToList();
            var lstRanking_FemaleEthnic = lstRanking.Where(o => o.Genre == SystemParamsInFile.GENRE_FEMALE && o.EthnicID.HasValue && o.EthnicID != EthnicID_Kinh && o.EthnicID != EthnicID_ForeignPeople).ToList();
            Dictionary<string, object> dicClass = new Dictionary<string, object> 
                                                        {
                                                            {"AcademicYearID", entity.AcademicYearID}, 
                                                            {"SchoolID", entity.SchoolID} ,
                                                            {"AppliedLevel", entity.AppliedLevel},
                                                            {"IsVNEN",true}
                                                        };
            List<ClassProfileTempBO> listAllClass = this.ClassProfileBusiness.SearchBySchool(entity.SchoolID, dicClass)
              .Select(o => new ClassProfileTempBO
              {
                  ClassProfileID = o.ClassProfileID,
                  EducationLevelID = o.EducationLevelID,
                  OrderNumber = o.OrderNumber,
                  SchoolID = o.SchoolID,
                  AcademicYearID = o.AcademicYearID,
                  DisplayName = o.DisplayName,
                  EmployeeID = o.HeadTeacherID.HasValue ? o.HeadTeacherID.Value : 0,
                  EmployeeName = o.Employee != null ? o.Employee.FullName : string.Empty
              })
              .ToList();
            #region Thống kê xếp loại hạnh kiểm 
            IVTWorksheet sheet = oBook.CopySheetToLast(firstSheet);
            sheet.SetCellValue("A2", supervisingDeptName);
            sheet.SetCellValue("A3", schoolName);
            sheet.SetCellValue("F4", provinceAndDate);
            sheet.Name = "TKXLHK";
            sheet.SetCellValue("A7", semesterAndAcademic);
            int startRow = 13;
            int index = 1;
            int indexOfEducationRow = 12;
            int startsc = startRow - 1;
            
            //Duyet theo tung khoi
            foreach (EducationLevel EducationLevel in lstEducationLevel)
            {
                int startmin = startRow;
                //Danh sach lop thuoc khoi
                int EducationLevelID = EducationLevel.EducationLevelID;
                List<ClassProfileTempBO> listFirstCP = listAllClass.Where(o => o.EducationLevelID == EducationLevelID).OrderBy(o => o.OrderNumber.HasValue?o.OrderNumber:0).ThenBy(o => o.DisplayName).ToList();
                var listCountCurentPupilByClass_Edu = listCountCurentPupilByClass.Where(o => o.EducationLevelID == EducationLevelID).ToList();
                var lstRanking_Edu = lstRanking.Where(o => o.EducationLevelID == EducationLevelID).ToList();
                //Duyet trong tung lop
                for (int i = 0; i < listFirstCP.Count; i++)
                {
                    //Copy row style
                    if (startRow + i > 13)
                    {
                        sheet.CopyPasteSameRowHeigh(range, startRow);
                    }
                    int classID = listFirstCP[i].ClassProfileID;
                    //STT
                    sheet.SetCellValue(startRow, 1, index + "." + (i + 1));

                    //Ten lop
                    sheet.SetCellValue(startRow, 2, listFirstCP[i].DisplayName);

                    //Ten giao vien chu nhiem
                    //string headTeacher = listFirstCP[i].Employee.FullName;
                    sheet.SetCellValue(startRow, 3, listFirstCP[i].EmployeeName);

                    //Si so thuc te                                                           
                    var objCountPupil = listCountCurentPupilByClass_Edu.Where(o => o.ClassID == classID);
                    int countPOC = objCountPupil != null ? objCountPupil.Sum(o => o.TotalPupil) : 0;
                    sheet.SetCellValue(startRow, 4, countPOC);

                    //Dien hanh kiem tot
                    int countGoodOfClass = 0;
                    List<PupilRanking> listRankingOfClass = new List<PupilRanking>();
                    if (entity.AppliedLevel == SystemParamsInFile.APPLIED_LEVEL_TERTIARY)
                        countGoodOfClass = lstRanking_Edu.Count(o => o.ClassID == classID && o.ConductLevelID == SystemParamsInFile.CONDUCT_TYPE_GOOD_TERTIARY);
                    else
                        countGoodOfClass = lstRanking_Edu.Count(o => o.ClassID == classID && o.ConductLevelID == SystemParamsInFile.CONDUCT_TYPE_GOOD_SECONDARY);
                    sheet.SetCellValue(startRow, 5, countGoodOfClass);
                    sheet.SetCellValue(startRow, 6, "=IF(D"+startRow +">0,ROUND(E"+startRow+"/D"+startRow +"*100,2),0)");

                    //Dien hanh kiem kha
                    int countFairOfClass = 0;
                    List<PupilRanking> listRankingOfClassFair = new List<PupilRanking>();
                    if (entity.AppliedLevel == SystemParamsInFile.APPLIED_LEVEL_TERTIARY)
                        countFairOfClass = lstRanking_Edu.Count(o => o.ClassID == classID && o.ConductLevelID == SystemParamsInFile.CONDUCT_TYPE_FAIR_TERTIARY);
                    else
                        countFairOfClass = lstRanking_Edu.Count(o => o.ClassID == classID && o.ConductLevelID == SystemParamsInFile.CONDUCT_TYPE_FAIR_SECONDARY);
                    sheet.SetCellValue(startRow, 7, countFairOfClass);
                    sheet.SetCellValue(startRow, 8, "=IF(D" + startRow + ">0,ROUND(G" + startRow + "/D" + startRow + "*100,2),0)");

                    //Dien hanh kiem TB
                    int countNormalOfClass = 0;
                    if (entity.AppliedLevel == SystemParamsInFile.APPLIED_LEVEL_TERTIARY)
                        countNormalOfClass = lstRanking_Edu.Count(o => o.ClassID == classID && o.ConductLevelID == SystemParamsInFile.CONDUCT_TYPE_NORMAL_TERTIARY);
                    else
                        countNormalOfClass = lstRanking_Edu.Count(o => o.ClassID == classID && o.ConductLevelID == SystemParamsInFile.CONDUCT_TYPE_NORMAL_SECONDARY);
                    sheet.SetCellValue(startRow, 9, countNormalOfClass);
                    sheet.SetCellValue(startRow, 10, "=IF(D" + startRow + ">0,ROUND(I" + startRow + "/D" + startRow + "*100,2),0)");

                    //Dien hanh kiem Yeu
                    int countWeekOfClass = 0;
                    List<PupilRanking> listRankingOfClassWeek = new List<PupilRanking>();
                    if (entity.AppliedLevel == SystemParamsInFile.APPLIED_LEVEL_TERTIARY)
                        countWeekOfClass = lstRanking_Edu.Count(o => o.ClassID == classID && o.ConductLevelID == SystemParamsInFile.CONDUCT_TYPE_WEAK_TERTIARY);
                    else
                        countWeekOfClass = lstRanking_Edu.Count(o => o.ClassID == classID && o.ConductLevelID == SystemParamsInFile.CONDUCT_TYPE_WEAK_SECONDARY);
                    sheet.SetCellValue(startRow, 11, countWeekOfClass);
                    sheet.SetCellValue(startRow, 12, "=IF(D" + startRow + ">0,ROUND(K" + startRow + "/D" + startRow + "*100,2),0)");

                    //TB tro len
                    int aboveWeek = countGoodOfClass + countFairOfClass + countNormalOfClass;
                    sheet.SetCellValue(startRow, 13, "=SUM(E" + startRow + ",G" + startRow + ",I" + startRow + ")");
                    sheet.SetCellValue(startRow, 14, "=IF(D" + startRow + ">0,ROUND(M" + startRow + "/D" + startRow + "*100,2),0)");

                    startRow++;
                }
               
                sheet.GetRange(indexOfEducationRow, 1, indexOfEducationRow, 14).SetBorder(VTBorderStyle.Solid, VTBorderWeight.Medium, VTBorderIndex.Around);
                sheet.SetCellValue("A" + indexOfEducationRow, index);
                sheet.SetCellValue("B" + indexOfEducationRow, EducationLevel.Resolution);
                if (listFirstCP.Count > 0)
                {
                    sheet.SetCellValue("D" + indexOfEducationRow, "=SUM(D" + startmin.ToString() + ":D" + (startRow - 1).ToString() + ")");
                    sheet.SetCellValue("E" + indexOfEducationRow, "=SUM(E" + startmin.ToString() + ":E" + (startRow - 1).ToString() + ")");
                    sheet.SetCellValue("F" + indexOfEducationRow, "=IF(D" + indexOfEducationRow + ">0" + "," + "ROUND(E" + indexOfEducationRow + "/" + "D" + indexOfEducationRow + "*100,2),0)");//Math.Round(countRealPupilOfEducation > 0 ? 100.0 * countAllGoodPupil / countRealPupilOfEducation : 0.0, 2, MidpointRounding.AwayFromZero));
                    sheet.SetCellValue("G" + indexOfEducationRow, "=SUM(G" + startmin.ToString() + ":G" + (startRow - 1).ToString() + ")");
                    sheet.SetCellValue("H" + indexOfEducationRow, "=IF(D" + indexOfEducationRow + ">0" + "," + "ROUND(G" + indexOfEducationRow + "/" + "D" + indexOfEducationRow + "*100,2),0)");
                    sheet.SetCellValue("I" + indexOfEducationRow, "=SUM(I" + startmin.ToString() + ":I" + (startRow - 1).ToString() + ")");
                    sheet.SetCellValue("J" + indexOfEducationRow, "=IF(D" + indexOfEducationRow + ">0" + "," + "ROUND(I" + indexOfEducationRow + "/" + "D" + indexOfEducationRow + "*100,2),0)");
                    sheet.SetCellValue("K" + indexOfEducationRow, "=SUM(K" + startmin.ToString() + ":K" + (startRow - 1).ToString() + ")");
                    sheet.SetCellValue("L" + indexOfEducationRow, "=IF(D" + indexOfEducationRow + ">0" + "," + "ROUND(K" + indexOfEducationRow + "/" + "D" + indexOfEducationRow + "*100,2),0)");

                    sheet.SetFormulaValue("M" + indexOfEducationRow, "=SUM(E" + indexOfEducationRow + ",G" + indexOfEducationRow + ",I" + indexOfEducationRow + ")");

                    sheet.SetCellValue("N" + indexOfEducationRow, "=IF(D" + indexOfEducationRow + ">0" + "," + "ROUND(M" + indexOfEducationRow + "/" + "D" + indexOfEducationRow + "*100,2),0)");//Math.Round(countRealPupilOfEducation > 0 ? 100.0 * countAllAboveWeekPupil / countRealPupilOfEducation : 0.0, 2, MidpointRounding.AwayFromZero));
                }
                else
                {
                    sheet.SetCellValue("D" + indexOfEducationRow, 0);
                    sheet.SetCellValue("E" + indexOfEducationRow, 0);
                    sheet.SetCellValue("F" + indexOfEducationRow, 0);//Math.Round(countRealPupilOfEducation > 0 ? 100.0 * countAllGoodPupil / countRealPupilOfEducation : 0.0, 2, MidpointRounding.AwayFromZero));
                    sheet.SetCellValue("G" + indexOfEducationRow, 0);
                    sheet.SetCellValue("H" + indexOfEducationRow, 0);
                    sheet.SetCellValue("I" + indexOfEducationRow, 0);
                    sheet.SetCellValue("J" + indexOfEducationRow, 0);
                    sheet.SetCellValue("K" + indexOfEducationRow, 0);
                    sheet.SetCellValue("L" + indexOfEducationRow, 0);

                    sheet.SetFormulaValue("M" + indexOfEducationRow, 0);

                    sheet.SetCellValue("N" + indexOfEducationRow, 0);//Math.Round(countRealPupilOfEducation > 0 ? 100.0 * countAllAboveWeekPupil / countRealPupilOfEducation : 0.0, 2, MidpointRounding.AwayFromZero));
                }
                sheet.SetColumnWidth('B', sheet.GetColumnWidth('A') * 2.0);
                sheet.CopyPasteSameRowHeigh(rangeEducation, startRow);
                indexOfEducationRow += listFirstCP.Count + 1;

                index++;
                startRow++;
            }
            sheet.DeleteRow(startRow - 1);

            sheet.SetCellValue("D11", "=SUM(D" + startsc.ToString() + ":D" + (startRow - 2).ToString() + ")/2");
            sheet.SetCellValue("E11", "=SUM(E" + startsc.ToString() + ":E" + (startRow - 2).ToString() + ")/2");
            sheet.SetCellValue("F11", "=IF(D11>0" + "," + "ROUND(E11/" + "D11*100,2),0)");//Math.Round(percentAllGoodOfSchool, 2, MidpointRounding.AwayFromZero));
            sheet.SetCellValue("G11", "=SUM(G" + startsc.ToString() + ":G" + (startRow - 2).ToString() + ")/2");
            sheet.SetCellValue("H11", "=IF(D11>0" + "," + "ROUND(G11/" + "D11*100,2),0)");
            sheet.SetCellValue("I11", "=SUM(I" + startsc.ToString() + ":I" + (startRow - 2).ToString() + ")/2");
            sheet.SetCellValue("J11", "=IF(D11>0" + "," + "ROUND(I11/" + "D11*100,2),0)");
            sheet.SetCellValue("K11", "=SUM(K" + startsc.ToString() + ":K" + (startRow - 2).ToString() + ")/2");
            sheet.SetCellValue("L11", "=IF(D11>0" + "," + "ROUND(K11/" + "D11*100,2),0)");
            sheet.SetCellValue("M11", "=SUM(E11,G11,I11)");
            sheet.SetCellValue("N11", "=IF(D11>0" + "," + "ROUND(M11/" + "D11*100,2),0)");
            sheet.CopyPasteSameSize(rangeBoldFont, "K" + startRow);
            sheet.SetCellValue("K" + startRow, "Người lập báo cáo");
            sheet.GetRange(9, 1, startRow - 2, 14).SetBorder(VTBorderStyle.Solid, VTBorderWeight.Medium, VTBorderIndex.Around);
            sheet.FitAllColumnsOnOnePage = true;
            #endregion

            if (entity.FemaleChecked)
            {
                #region Thống kê xếp loại hạnh kiểm học sinh nữ
                IVTWorksheet sheet_Female = oBook.CopySheetToLast(firstSheet);
                sheet_Female.SetCellValue("A2", supervisingDeptName);
                sheet_Female.SetCellValue("A3", schoolName);
                sheet_Female.SetCellValue("F4", provinceAndDate);
                sheet_Female.Name = "HS_Nu";
                sheet_Female.SetCellValue("A6", "THỐNG KÊ XẾP LOẠI HẠNH KIỂM HỌC SINH NỮ");
                sheet_Female.SetCellValue("A7", semesterAndAcademic);
                startRow = 13;
                index = 1;
                indexOfEducationRow = 12;
                startsc = startRow - 1;

                //Duyet theo tung khoi
                foreach (EducationLevel EducationLevel in lstEducationLevel)
                {
                    int startmin = startRow;
                    //Danh sach lop thuoc khoi
                    int EducationLevelID = EducationLevel.EducationLevelID;
                    List<ClassProfileTempBO> listFirstCP = listAllClass.Where(o => o.EducationLevelID == EducationLevelID).OrderBy(o => o.OrderNumber.HasValue ? o.OrderNumber : 0).ThenBy(o => o.DisplayName).ToList();
                    var listCountCurentPupilByClass_Edu = listCountCurentPupilByClass_Female.Where(o => o.EducationLevelID == EducationLevelID).ToList();
                    var lstRanking_Edu = lstRanking_Female.Where(o => o.EducationLevelID == EducationLevelID).ToList();
                    //Duyet trong tung lop
                    for (int i = 0; i < listFirstCP.Count; i++)
                    {
                        //Copy row style
                        if (startRow + i > 13)
                        {
                            sheet_Female.CopyPasteSameRowHeigh(range, startRow);
                        }
                        int classID = listFirstCP[i].ClassProfileID;
                        //STT
                        sheet_Female.SetCellValue(startRow, 1, index + "." + (i + 1));

                        //Ten lop
                        sheet_Female.SetCellValue(startRow, 2, listFirstCP[i].DisplayName);

                        //Ten giao vien chu nhiem
                        //string headTeacher = listFirstCP[i].Employee.FullName;
                        sheet_Female.SetCellValue(startRow, 3, listFirstCP[i].EmployeeName);

                        //Si so thuc te                                                           
                        var objCountPupil = listCountCurentPupilByClass_Edu.Where(o => o.ClassID == classID);
                        int countPOC = objCountPupil != null ? objCountPupil.Sum(o => o.TotalPupil) : 0;
                        sheet_Female.SetCellValue(startRow, 4, countPOC);

                        //Dien hanh kiem tot
                        int countGoodOfClass = 0;
                        List<PupilRanking> listRankingOfClass = new List<PupilRanking>();
                        if (entity.AppliedLevel == SystemParamsInFile.APPLIED_LEVEL_TERTIARY)
                            countGoodOfClass = lstRanking_Edu.Count(o => o.ClassID == classID && o.ConductLevelID == SystemParamsInFile.CONDUCT_TYPE_GOOD_TERTIARY);
                        else
                            countGoodOfClass = lstRanking_Edu.Count(o => o.ClassID == classID && o.ConductLevelID == SystemParamsInFile.CONDUCT_TYPE_GOOD_SECONDARY);
                        sheet_Female.SetCellValue(startRow, 5, countGoodOfClass);
                        sheet_Female.SetCellValue(startRow, 6, "=IF(D" + startRow + ">0,ROUND(E" + startRow + "/D" + startRow + "*100,2),0)");


                        //Dien hanh kiem kha
                        int countFairOfClass = 0;
                        List<PupilRanking> listRankingOfClassFair = new List<PupilRanking>();
                        if (entity.AppliedLevel == SystemParamsInFile.APPLIED_LEVEL_TERTIARY)
                            countFairOfClass = lstRanking_Edu.Count(o => o.ClassID == classID && o.ConductLevelID == SystemParamsInFile.CONDUCT_TYPE_FAIR_TERTIARY);
                        else
                            countFairOfClass = lstRanking_Edu.Count(o => o.ClassID == classID && o.ConductLevelID == SystemParamsInFile.CONDUCT_TYPE_FAIR_SECONDARY);
                        sheet_Female.SetCellValue(startRow, 7, countFairOfClass);
                        sheet_Female.SetCellValue(startRow, 8, "=IF(D" + startRow + ">0,ROUND(G" + startRow + "/D" + startRow + "*100,2),0)");


                        //Dien hanh kiem TB
                        int countNormalOfClass = 0;
                        if (entity.AppliedLevel == SystemParamsInFile.APPLIED_LEVEL_TERTIARY)
                            countNormalOfClass = lstRanking_Edu.Count(o => o.ClassID == classID && o.ConductLevelID == SystemParamsInFile.CONDUCT_TYPE_NORMAL_TERTIARY);
                        else
                            countNormalOfClass = lstRanking_Edu.Count(o => o.ClassID == classID && o.ConductLevelID == SystemParamsInFile.CONDUCT_TYPE_NORMAL_SECONDARY);
                        sheet_Female.SetCellValue(startRow, 9, countNormalOfClass);
                        sheet_Female.SetCellValue(startRow, 10, "=IF(D" + startRow + ">0,ROUND(I" + startRow + "/D" + startRow + "*100,2),0)");


                        //Dien hanh kiem Yeu
                        int countWeekOfClass = 0;
                        List<PupilRanking> listRankingOfClassWeek = new List<PupilRanking>();
                        if (entity.AppliedLevel == SystemParamsInFile.APPLIED_LEVEL_TERTIARY)
                            countWeekOfClass = lstRanking_Edu.Count(o => o.ClassID == classID && o.ConductLevelID == SystemParamsInFile.CONDUCT_TYPE_WEAK_TERTIARY);
                        else
                            countWeekOfClass = lstRanking_Edu.Count(o => o.ClassID == classID && o.ConductLevelID == SystemParamsInFile.CONDUCT_TYPE_WEAK_SECONDARY);
                        sheet_Female.SetCellValue(startRow, 11, countWeekOfClass);
                        sheet_Female.SetCellValue(startRow, 12, "=IF(D" + startRow + ">0,ROUND(K" + startRow + "/D" + startRow + "*100,2),0)");


                        //TB tro len
                        int aboveWeek = countGoodOfClass + countFairOfClass + countNormalOfClass;
                        sheet_Female.SetCellValue(startRow, 13, "=SUM(E" + startRow + ",G" + startRow + ",I" + startRow + ")");
                        sheet_Female.SetCellValue(startRow, 14, "=IF(D" + startRow + ">0,ROUND(M" + startRow + "/D" + startRow + "*100,2),0)");

                        startRow++;
                    }
                    sheet_Female.GetRange(indexOfEducationRow, 1, indexOfEducationRow, 14).SetBorder(VTBorderStyle.Solid, VTBorderWeight.Medium, VTBorderIndex.Around);
                    sheet_Female.SetCellValue("A" + indexOfEducationRow, index);
                    sheet_Female.SetCellValue("B" + indexOfEducationRow, EducationLevel.Resolution);
                    if (listFirstCP.Count > 0)
                    {
                        sheet_Female.SetCellValue("D" + indexOfEducationRow, "=SUM(D" + startmin.ToString() + ":D" + (startRow - 1).ToString() + ")");
                        sheet_Female.SetCellValue("E" + indexOfEducationRow, "=SUM(E" + startmin.ToString() + ":E" + (startRow - 1).ToString() + ")");
                        sheet_Female.SetCellValue("F" + indexOfEducationRow, "=IF(D" + indexOfEducationRow + ">0" + "," + "ROUND(E" + indexOfEducationRow + "/" + "D" + indexOfEducationRow + "*100,2),0)");//Math.Round(countRealPupilOfEducation > 0 ? 100.0 * countAllGoodPupil / countRealPupilOfEducation : 0.0, 2, MidpointRounding.AwayFromZero));
                        sheet_Female.SetCellValue("G" + indexOfEducationRow, "=SUM(G" + startmin.ToString() + ":G" + (startRow - 1).ToString() + ")");
                        sheet_Female.SetCellValue("H" + indexOfEducationRow, "=IF(D" + indexOfEducationRow + ">0" + "," + "ROUND(G" + indexOfEducationRow + "/" + "D" + indexOfEducationRow + "*100,2),0)");
                        sheet_Female.SetCellValue("I" + indexOfEducationRow, "=SUM(I" + startmin.ToString() + ":I" + (startRow - 1).ToString() + ")");
                        sheet_Female.SetCellValue("J" + indexOfEducationRow, "=IF(D" + indexOfEducationRow + ">0" + "," + "ROUND(I" + indexOfEducationRow + "/" + "D" + indexOfEducationRow + "*100,2),0)");
                        sheet_Female.SetCellValue("K" + indexOfEducationRow, "=SUM(K" + startmin.ToString() + ":K" + (startRow - 1).ToString() + ")");
                        sheet_Female.SetCellValue("L" + indexOfEducationRow, "=IF(D" + indexOfEducationRow + ">0" + "," + "ROUND(K" + indexOfEducationRow + "/" + "D" + indexOfEducationRow + "*100,2),0)");
                        sheet_Female.SetFormulaValue("M" + indexOfEducationRow, "=SUM(E" + indexOfEducationRow + ",G" + indexOfEducationRow + ",I" + indexOfEducationRow + ")");
                        sheet_Female.SetCellValue("N" + indexOfEducationRow, "=IF(D" + indexOfEducationRow + ">0" + "," + "ROUND(M" + indexOfEducationRow + "/" + "D" + indexOfEducationRow + "*100,2),0)");//Math.Round(countRealPupilOfEducation > 0 ? 100.0 * countAllAboveWeekPupil / countRealPupilOfEducation : 0.0, 2, MidpointRounding.AwayFromZero));
                    }
                    else
                    {
                        sheet_Female.SetCellValue("D" + indexOfEducationRow, 0);
                        sheet_Female.SetCellValue("E" + indexOfEducationRow, 0);
                        sheet_Female.SetCellValue("F" + indexOfEducationRow, 0);//Math.Round(countRealPupilOfEducation > 0 ? 100.0 * countAllGoodPupil / countRealPupilOfEducation : 0.0, 2, MidpointRounding.AwayFromZero));
                        sheet_Female.SetCellValue("G" + indexOfEducationRow, 0);
                        sheet_Female.SetCellValue("H" + indexOfEducationRow, 0);
                        sheet_Female.SetCellValue("I" + indexOfEducationRow, 0);
                        sheet_Female.SetCellValue("J" + indexOfEducationRow, 0);
                        sheet_Female.SetCellValue("K" + indexOfEducationRow, 0);
                        sheet_Female.SetCellValue("L" + indexOfEducationRow, 0);
                        sheet_Female.SetFormulaValue("M" + indexOfEducationRow, 0);
                        sheet_Female.SetCellValue("N" + indexOfEducationRow, 0);//Math.Round(countRealPupilOfEducation > 0 ? 100.0 * countAllAboveWeekPupil / countRealPupilOfEducation : 0.0, 2, MidpointRounding.AwayFromZero));
                    }
                    sheet_Female.SetColumnWidth('B', sheet_Female.GetColumnWidth('A') * 2.0);
                    sheet_Female.CopyPasteSameRowHeigh(rangeEducation, startRow);
                    indexOfEducationRow += listFirstCP.Count + 1;

                    index++;
                    startRow++;
                }
                sheet_Female.DeleteRow(startRow - 1);

                sheet_Female.SetCellValue("D11", "=SUM(D" + startsc.ToString() + ":D" + (startRow - 2).ToString() + ")/2");
                sheet_Female.SetCellValue("E11", "=SUM(E" + startsc.ToString() + ":E" + (startRow - 2).ToString() + ")/2");
                sheet_Female.SetCellValue("F11", "=IF(D11>0" + "," + "ROUND(E11/" + "D11*100,2),0)");//Math.Round(percentAllGoodOfSchool, 2, MidpointRounding.AwayFromZero));
                sheet_Female.SetCellValue("G11", "=SUM(G" + startsc.ToString() + ":G" + (startRow - 2).ToString() + ")/2");
                sheet_Female.SetCellValue("H11", "=IF(D11>0" + "," + "ROUND(G11/" + "D11*100,2),0)");
                sheet_Female.SetCellValue("I11", "=SUM(I" + startsc.ToString() + ":I" + (startRow - 2).ToString() + ")/2");
                sheet_Female.SetCellValue("J11", "=IF(D11>0" + "," + "ROUND(I11/" + "D11*100,2),0)");
                sheet_Female.SetCellValue("K11", "=SUM(K" + startsc.ToString() + ":K" + (startRow - 2).ToString() + ")/2");
                sheet_Female.SetCellValue("L11", "=IF(D11>0" + "," + "ROUND(K11/" + "D11*100,2),0)");
                sheet_Female.SetCellValue("M11", "=SUM(E11,G11,I11)");
                sheet_Female.SetCellValue("N11", "=IF(D11>0" + "," + "ROUND(M11/" + "D11*100,2),0)");
                sheet_Female.CopyPasteSameSize(rangeBoldFont, "K" + startRow);
                sheet_Female.SetCellValue("K" + startRow, "Người lập báo cáo");
                sheet_Female.GetRange(9, 1, startRow - 2, 14).SetBorder(VTBorderStyle.Solid, VTBorderWeight.Medium, VTBorderIndex.Around);
                sheet_Female.FitAllColumnsOnOnePage = true;
                //sheet.Delete();
                #endregion
            }
            if (entity.EthnicChecked)
            {
                #region Thống kê xếp loại hạnh kiểm học sinh dân tộc
                IVTWorksheet sheet_Ethnic = oBook.CopySheetToLast(firstSheet);
                sheet_Ethnic.SetCellValue("A2", supervisingDeptName);
                sheet_Ethnic.SetCellValue("A3", schoolName);
                sheet_Ethnic.SetCellValue("F4", provinceAndDate);
                sheet_Ethnic.Name = "HS_DT";
                sheet_Ethnic.SetCellValue("A6", "THỐNG KÊ XẾP LOẠI HẠNH KIỂM HỌC SINH DÂN TỘC");
                sheet_Ethnic.SetCellValue("A7", semesterAndAcademic);
                startRow = 13;
                index = 1;
                indexOfEducationRow = 12;
                startsc = startRow - 1;

                //Duyet theo tung khoi
                foreach (EducationLevel EducationLevel in lstEducationLevel)
                {
                    int startmin = startRow;
                    //Danh sach lop thuoc khoi
                    int EducationLevelID = EducationLevel.EducationLevelID;
                    List<ClassProfileTempBO> listFirstCP = listAllClass.Where(o => o.EducationLevelID == EducationLevelID).OrderBy(o => o.OrderNumber.HasValue ? o.OrderNumber : 0).ThenBy(o => o.DisplayName).ToList();
                    var listCountCurentPupilByClass_Edu = listCountCurentPupilByClass_Ethnic.Where(o => o.EducationLevelID == EducationLevelID).ToList();
                    var lstRanking_Edu = lstRanking_Ethnic.Where(o => o.EducationLevelID == EducationLevelID).ToList();
                    //Duyet trong tung lop
                    for (int i = 0; i < listFirstCP.Count; i++)
                    {
                        //Copy row style
                        if (startRow + i > 13)
                        {
                            sheet_Ethnic.CopyPasteSameRowHeigh(range, startRow);
                        }
                        int classID = listFirstCP[i].ClassProfileID;
                        //STT
                        sheet_Ethnic.SetCellValue(startRow, 1, index + "." + (i + 1));

                        //Ten lop
                        sheet_Ethnic.SetCellValue(startRow, 2, listFirstCP[i].DisplayName);

                        //Ten giao vien chu nhiem
                        //string headTeacher = listFirstCP[i].Employee.FullName;
                        sheet_Ethnic.SetCellValue(startRow, 3, listFirstCP[i].EmployeeName);

                        //Si so thuc te                                                           
                        var objCountPupil = listCountCurentPupilByClass_Edu.Where(o => o.ClassID == classID);
                        int countPOC = objCountPupil != null ? objCountPupil.Sum(o => o.TotalPupil) : 0;
                        sheet_Ethnic.SetCellValue(startRow, 4, countPOC);

                        //Dien hanh kiem tot
                        int countGoodOfClass = 0;
                        List<PupilRanking> listRankingOfClass = new List<PupilRanking>();
                        if (entity.AppliedLevel == SystemParamsInFile.APPLIED_LEVEL_TERTIARY)
                            countGoodOfClass = lstRanking_Edu.Count(o => o.ClassID == classID && o.ConductLevelID == SystemParamsInFile.CONDUCT_TYPE_GOOD_TERTIARY);
                        else
                            countGoodOfClass = lstRanking_Edu.Count(o => o.ClassID == classID && o.ConductLevelID == SystemParamsInFile.CONDUCT_TYPE_GOOD_SECONDARY);
                        sheet_Ethnic.SetCellValue(startRow, 5, countGoodOfClass);
                        sheet_Ethnic.SetCellValue(startRow, 6, "=IF(D" + startRow + ">0,ROUND(E" + startRow + "/D" + startRow + "*100,2),0)");

                        //Dien hanh kiem kha
                        int countFairOfClass = 0;
                        List<PupilRanking> listRankingOfClassFair = new List<PupilRanking>();
                        if (entity.AppliedLevel == SystemParamsInFile.APPLIED_LEVEL_TERTIARY)
                            countFairOfClass = lstRanking_Edu.Count(o => o.ClassID == classID && o.ConductLevelID == SystemParamsInFile.CONDUCT_TYPE_FAIR_TERTIARY);
                        else
                            countFairOfClass = lstRanking_Edu.Count(o => o.ClassID == classID && o.ConductLevelID == SystemParamsInFile.CONDUCT_TYPE_FAIR_SECONDARY);
                        sheet_Ethnic.SetCellValue(startRow, 7, countFairOfClass);
                        sheet_Ethnic.SetCellValue(startRow, 8, "=IF(D" + startRow + ">0,ROUND(G" + startRow + "/D" + startRow + "*100,2),0)");


                        //Dien hanh kiem TB
                        int countNormalOfClass = 0;
                        if (entity.AppliedLevel == SystemParamsInFile.APPLIED_LEVEL_TERTIARY)
                            countNormalOfClass = lstRanking_Edu.Count(o => o.ClassID == classID && o.ConductLevelID == SystemParamsInFile.CONDUCT_TYPE_NORMAL_TERTIARY);
                        else
                            countNormalOfClass = lstRanking_Edu.Count(o => o.ClassID == classID && o.ConductLevelID == SystemParamsInFile.CONDUCT_TYPE_NORMAL_SECONDARY);
                        sheet_Ethnic.SetCellValue(startRow, 9, countNormalOfClass);
                        sheet_Ethnic.SetCellValue(startRow, 10, "=IF(D" + startRow + ">0,ROUND(I" + startRow + "/D" + startRow + "*100,2),0)");


                        //Dien hanh kiem Yeu
                        int countWeekOfClass = 0;
                        List<PupilRanking> listRankingOfClassWeek = new List<PupilRanking>();
                        if (entity.AppliedLevel == SystemParamsInFile.APPLIED_LEVEL_TERTIARY)
                            countWeekOfClass = lstRanking_Edu.Count(o => o.ClassID == classID && o.ConductLevelID == SystemParamsInFile.CONDUCT_TYPE_WEAK_TERTIARY);
                        else
                            countWeekOfClass = lstRanking_Edu.Count(o => o.ClassID == classID && o.ConductLevelID == SystemParamsInFile.CONDUCT_TYPE_WEAK_SECONDARY);
                        sheet_Ethnic.SetCellValue(startRow, 11, countWeekOfClass);
                        sheet_Ethnic.SetCellValue(startRow, 12, "=IF(D" + startRow + ">0,ROUND(K" + startRow + "/D" + startRow + "*100,2),0)");


                        //TB tro len
                        int aboveWeek = countGoodOfClass + countFairOfClass + countNormalOfClass;
                        sheet_Ethnic.SetCellValue(startRow, 13, "=SUM(E" + startRow + ",G" + startRow + ",I" + startRow + ")");
                        sheet_Ethnic.SetCellValue(startRow, 14, "=IF(D" + startRow + ">0,ROUND(M" + startRow + "/D" + startRow + "*100,2),0)");

                        startRow++;
                    }
                    sheet_Ethnic.GetRange(indexOfEducationRow, 1, indexOfEducationRow, 14).SetBorder(VTBorderStyle.Solid, VTBorderWeight.Medium, VTBorderIndex.Around);
                    sheet_Ethnic.SetCellValue("A" + indexOfEducationRow, index);
                    sheet_Ethnic.SetCellValue("B" + indexOfEducationRow, EducationLevel.Resolution);
                    if (listFirstCP.Count > 0)
                    {
                        sheet_Ethnic.SetCellValue("D" + indexOfEducationRow, "=SUM(D" + startmin.ToString() + ":D" + (startRow - 1).ToString() + ")");
                        sheet_Ethnic.SetCellValue("E" + indexOfEducationRow, "=SUM(E" + startmin.ToString() + ":E" + (startRow - 1).ToString() + ")");
                        sheet_Ethnic.SetCellValue("F" + indexOfEducationRow, "=IF(D" + indexOfEducationRow + ">0" + "," + "ROUND(E" + indexOfEducationRow + "/" + "D" + indexOfEducationRow + "*100,2),0)");//Math.Round(countRealPupilOfEducation > 0 ? 100.0 * countAllGoodPupil / countRealPupilOfEducation : 0.0, 2, MidpointRounding.AwayFromZero));
                        sheet_Ethnic.SetCellValue("G" + indexOfEducationRow, "=SUM(G" + startmin.ToString() + ":G" + (startRow - 1).ToString() + ")");
                        sheet_Ethnic.SetCellValue("H" + indexOfEducationRow, "=IF(D" + indexOfEducationRow + ">0" + "," + "ROUND(G" + indexOfEducationRow + "/" + "D" + indexOfEducationRow + "*100,2),0)");
                        sheet_Ethnic.SetCellValue("I" + indexOfEducationRow, "=SUM(I" + startmin.ToString() + ":I" + (startRow - 1).ToString() + ")");
                        sheet_Ethnic.SetCellValue("J" + indexOfEducationRow, "=IF(D" + indexOfEducationRow + ">0" + "," + "ROUND(I" + indexOfEducationRow + "/" + "D" + indexOfEducationRow + "*100,2),0)");
                        sheet_Ethnic.SetCellValue("K" + indexOfEducationRow, "=SUM(K" + startmin.ToString() + ":K" + (startRow - 1).ToString() + ")");
                        sheet_Ethnic.SetCellValue("L" + indexOfEducationRow, "=IF(D" + indexOfEducationRow + ">0" + "," + "ROUND(K" + indexOfEducationRow + "/" + "D" + indexOfEducationRow + "*100,2),0)");
                        sheet_Ethnic.SetFormulaValue("M" + indexOfEducationRow, "=SUM(E" + indexOfEducationRow + ",G" + indexOfEducationRow + ",I" + indexOfEducationRow + ")");
                        sheet_Ethnic.SetCellValue("N" + indexOfEducationRow, "=IF(D" + indexOfEducationRow + ">0" + "," + "ROUND(M" + indexOfEducationRow + "/" + "D" + indexOfEducationRow + "*100,2),0)");//Math.Round(countRealPupilOfEducation > 0 ? 100.0 * countAllAboveWeekPupil / countRealPupilOfEducation : 0.0, 2, MidpointRounding.AwayFromZero));
                    }
                    else
                    {
                        sheet_Ethnic.SetCellValue("D" + indexOfEducationRow, 0);
                        sheet_Ethnic.SetCellValue("E" + indexOfEducationRow, 0);
                        sheet_Ethnic.SetCellValue("F" + indexOfEducationRow, 0);//Math.Round(countRealPupilOfEducation > 0 ? 100.0 * countAllGoodPupil / countRealPupilOfEducation : 0.0, 2, MidpointRounding.AwayFromZero));
                        sheet_Ethnic.SetCellValue("G" + indexOfEducationRow, 0);
                        sheet_Ethnic.SetCellValue("H" + indexOfEducationRow, 0);
                        sheet_Ethnic.SetCellValue("I" + indexOfEducationRow, 0);
                        sheet_Ethnic.SetCellValue("J" + indexOfEducationRow, 0);
                        sheet_Ethnic.SetCellValue("K" + indexOfEducationRow, 0);
                        sheet_Ethnic.SetCellValue("L" + indexOfEducationRow, 0);
                        sheet_Ethnic.SetFormulaValue("M" + indexOfEducationRow, 0);
                        sheet_Ethnic.SetCellValue("N" + indexOfEducationRow, 0);//Math.Round(countRealPupilOfEducation > 0 ? 100.0 * countAllAboveWeekPupil / countRealPupilOfEducation : 0.0, 2, MidpointRounding.AwayFromZero));
                    }
                    sheet_Ethnic.SetColumnWidth('B', sheet_Ethnic.GetColumnWidth('A') * 2.0);
                    sheet_Ethnic.CopyPasteSameRowHeigh(rangeEducation, startRow);
                    indexOfEducationRow += listFirstCP.Count + 1;

                    index++;
                    startRow++;
                }
                sheet_Ethnic.DeleteRow(startRow - 1);

                sheet_Ethnic.SetCellValue("D11", "=SUM(D" + startsc.ToString() + ":D" + (startRow - 2).ToString() + ")/2");
                sheet_Ethnic.SetCellValue("E11", "=SUM(E" + startsc.ToString() + ":E" + (startRow - 2).ToString() + ")/2");
                sheet_Ethnic.SetCellValue("F11", "=IF(D11>0" + "," + "ROUND(E11/" + "D11*100,2),0)");//Math.Round(percentAllGoodOfSchool, 2, MidpointRounding.AwayFromZero));
                sheet_Ethnic.SetCellValue("G11", "=SUM(G" + startsc.ToString() + ":G" + (startRow - 2).ToString() + ")/2");
                sheet_Ethnic.SetCellValue("H11", "=IF(D11>0" + "," + "ROUND(G11/" + "D11*100,2),0)");
                sheet_Ethnic.SetCellValue("I11", "=SUM(I" + startsc.ToString() + ":I" + (startRow - 2).ToString() + ")/2");
                sheet_Ethnic.SetCellValue("J11", "=IF(D11>0" + "," + "ROUND(I11/" + "D11*100,2),0)");
                sheet_Ethnic.SetCellValue("K11", "=SUM(K" + startsc.ToString() + ":K" + (startRow - 2).ToString() + ")/2");
                sheet_Ethnic.SetCellValue("L11", "=IF(D11>0" + "," + "ROUND(K11/" + "D11*100,2),0)");
                sheet_Ethnic.SetCellValue("M11", "=SUM(E11,G11,I11)");
                sheet_Ethnic.SetCellValue("N11", "=IF(D11>0" + "," + "ROUND(M11/" + "D11*100,2),0)");
                sheet_Ethnic.CopyPasteSameSize(rangeBoldFont, "K" + startRow);
                sheet_Ethnic.SetCellValue("K" + startRow, "Người lập báo cáo");
                sheet_Ethnic.GetRange(9, 1, startRow - 2, 14).SetBorder(VTBorderStyle.Solid, VTBorderWeight.Medium, VTBorderIndex.Around);
                sheet_Ethnic.FitAllColumnsOnOnePage = true;
               // sheet.Delete();
                #endregion
            }
            if (entity.FemaleEthnicChecked)
            {
                #region Thống kê xếp loại hạnh kiểm học sinh nữ dân tộc
                IVTWorksheet sheet_FemaleEthnic = oBook.CopySheetToLast(firstSheet);
                sheet_FemaleEthnic.SetCellValue("A2", supervisingDeptName);
                sheet_FemaleEthnic.SetCellValue("A3", schoolName);
                sheet_FemaleEthnic.SetCellValue("F4", provinceAndDate);
                sheet_FemaleEthnic.Name = "HS_NU_DT";
                sheet_FemaleEthnic.SetCellValue("A6", "THỐNG KÊ XẾP LOẠI HẠNH KIỂM HỌC SINH NỮ DÂN TỘC");
                sheet_FemaleEthnic.SetCellValue("A7", semesterAndAcademic);
                startRow = 13;
                index = 1;
                indexOfEducationRow = 12;
                startsc = startRow - 1;

                //Duyet theo tung khoi
                foreach (EducationLevel EducationLevel in lstEducationLevel)
                {
                    int startmin = startRow;
                    //Danh sach lop thuoc khoi
                    int EducationLevelID = EducationLevel.EducationLevelID;
                    List<ClassProfileTempBO> listFirstCP = listAllClass.Where(o => o.EducationLevelID == EducationLevelID).OrderBy(o => o.OrderNumber.HasValue ? o.OrderNumber : 0).ThenBy(o => o.DisplayName).ToList();
                    var listCountCurentPupilByClass_Edu = listCountCurentPupilByClass_FemaleEthnic.Where(o => o.EducationLevelID == EducationLevelID).ToList();
                    var lstRanking_Edu = lstRanking_FemaleEthnic.Where(o => o.EducationLevelID == EducationLevelID).ToList();
                    //Duyet trong tung lop
                    for (int i = 0; i < listFirstCP.Count; i++)
                    {
                        //Copy row style
                        if (startRow + i > 13)
                        {
                            sheet_FemaleEthnic.CopyPasteSameRowHeigh(range, startRow);
                        }
                        int classID = listFirstCP[i].ClassProfileID;
                        //STT
                        sheet_FemaleEthnic.SetCellValue(startRow, 1, index + "." + (i + 1));

                        //Ten lop
                        sheet_FemaleEthnic.SetCellValue(startRow, 2, listFirstCP[i].DisplayName);

                        //Ten giao vien chu nhiem
                        //string headTeacher = listFirstCP[i].Employee.FullName;
                        sheet_FemaleEthnic.SetCellValue(startRow, 3, listFirstCP[i].EmployeeName);

                        //Si so thuc te                                                           
                        var objCountPupil = listCountCurentPupilByClass_Edu.Where(o => o.ClassID == classID);
                        int countPOC = objCountPupil != null ? objCountPupil.Sum(o => o.TotalPupil) : 0;
                        sheet_FemaleEthnic.SetCellValue(startRow, 4, countPOC);

                        //Dien hanh kiem tot
                        int countGoodOfClass = 0;
                        List<PupilRanking> listRankingOfClass = new List<PupilRanking>();
                        if (entity.AppliedLevel == SystemParamsInFile.APPLIED_LEVEL_TERTIARY)
                            countGoodOfClass = lstRanking_Edu.Count(o => o.ClassID == classID && o.ConductLevelID == SystemParamsInFile.CONDUCT_TYPE_GOOD_TERTIARY);
                        else
                            countGoodOfClass = lstRanking_Edu.Count(o => o.ClassID == classID && o.ConductLevelID == SystemParamsInFile.CONDUCT_TYPE_GOOD_SECONDARY);
                        sheet_FemaleEthnic.SetCellValue(startRow, 5, countGoodOfClass);
                        sheet_FemaleEthnic.SetCellValue(startRow, 6, "=IF(D" + startRow + ">0,ROUND(E" + startRow + "/D" + startRow + "*100,2),0)");

                        //Dien hanh kiem kha
                        int countFairOfClass = 0;
                        List<PupilRanking> listRankingOfClassFair = new List<PupilRanking>();
                        if (entity.AppliedLevel == SystemParamsInFile.APPLIED_LEVEL_TERTIARY)
                            countFairOfClass = lstRanking_Edu.Count(o => o.ClassID == classID && o.ConductLevelID == SystemParamsInFile.CONDUCT_TYPE_FAIR_TERTIARY);
                        else
                            countFairOfClass = lstRanking_Edu.Count(o => o.ClassID == classID && o.ConductLevelID == SystemParamsInFile.CONDUCT_TYPE_FAIR_SECONDARY);
                        sheet_FemaleEthnic.SetCellValue(startRow, 7, countFairOfClass);
                        sheet_FemaleEthnic.SetCellValue(startRow, 8, "=IF(D" + startRow + ">0,ROUND(G" + startRow + "/D" + startRow + "*100,2),0)");


                        //Dien hanh kiem TB
                        int countNormalOfClass = 0;
                        if (entity.AppliedLevel == SystemParamsInFile.APPLIED_LEVEL_TERTIARY)
                            countNormalOfClass = lstRanking_Edu.Count(o => o.ClassID == classID && o.ConductLevelID == SystemParamsInFile.CONDUCT_TYPE_NORMAL_TERTIARY);
                        else
                            countNormalOfClass = lstRanking_Edu.Count(o => o.ClassID == classID && o.ConductLevelID == SystemParamsInFile.CONDUCT_TYPE_NORMAL_SECONDARY);
                        sheet_FemaleEthnic.SetCellValue(startRow, 9, countNormalOfClass);
                        sheet_FemaleEthnic.SetCellValue(startRow, 10, "=IF(D" + startRow + ">0,ROUND(I" + startRow + "/D" + startRow + "*100,2),0)");


                        //Dien hanh kiem Yeu
                        int countWeekOfClass = 0;
                        List<PupilRanking> listRankingOfClassWeek = new List<PupilRanking>();
                        if (entity.AppliedLevel == SystemParamsInFile.APPLIED_LEVEL_TERTIARY)
                            countWeekOfClass = lstRanking_Edu.Count(o => o.ClassID == classID && o.ConductLevelID == SystemParamsInFile.CONDUCT_TYPE_WEAK_TERTIARY);
                        else
                            countWeekOfClass = lstRanking_Edu.Count(o => o.ClassID == classID && o.ConductLevelID == SystemParamsInFile.CONDUCT_TYPE_WEAK_SECONDARY);
                        sheet_FemaleEthnic.SetCellValue(startRow, 11, countWeekOfClass);
                        sheet_FemaleEthnic.SetCellValue(startRow, 12, "=IF(D" + startRow + ">0,ROUND(K" + startRow + "/D" + startRow + "*100,2),0)");


                        //TB tro len
                        int aboveWeek = countGoodOfClass + countFairOfClass + countNormalOfClass;
                        sheet_FemaleEthnic.SetCellValue(startRow, 13, "=SUM(E" + startRow + ",G" + startRow + ",I" + startRow + ")");
                        sheet_FemaleEthnic.SetCellValue(startRow, 14, "=IF(D" + startRow + ">0,ROUND(M" + startRow + "/D" + startRow + "*100,2),0)");

                        startRow++;
                    }
                    sheet_FemaleEthnic.GetRange(indexOfEducationRow, 1, indexOfEducationRow, 14).SetBorder(VTBorderStyle.Solid, VTBorderWeight.Medium, VTBorderIndex.Around);
                    sheet_FemaleEthnic.SetCellValue("A" + indexOfEducationRow, index);
                    sheet_FemaleEthnic.SetCellValue("B" + indexOfEducationRow, EducationLevel.Resolution);
                    if (listFirstCP.Count > 0)
                    {
                        sheet_FemaleEthnic.SetCellValue("D" + indexOfEducationRow, "=SUM(D" + startmin.ToString() + ":D" + (startRow - 1).ToString() + ")");
                        sheet_FemaleEthnic.SetCellValue("E" + indexOfEducationRow, "=SUM(E" + startmin.ToString() + ":E" + (startRow - 1).ToString() + ")");
                        sheet_FemaleEthnic.SetCellValue("F" + indexOfEducationRow, "=IF(D" + indexOfEducationRow + ">0" + "," + "ROUND(E" + indexOfEducationRow + "/" + "D" + indexOfEducationRow + "*100,2),0)");//Math.Round(countRealPupilOfEducation > 0 ? 100.0 * countAllGoodPupil / countRealPupilOfEducation : 0.0, 2, MidpointRounding.AwayFromZero));
                        sheet_FemaleEthnic.SetCellValue("G" + indexOfEducationRow, "=SUM(G" + startmin.ToString() + ":G" + (startRow - 1).ToString() + ")");
                        sheet_FemaleEthnic.SetCellValue("H" + indexOfEducationRow, "=IF(D" + indexOfEducationRow + ">0" + "," + "ROUND(G" + indexOfEducationRow + "/" + "D" + indexOfEducationRow + "*100,2),0)");
                        sheet_FemaleEthnic.SetCellValue("I" + indexOfEducationRow, "=SUM(I" + startmin.ToString() + ":I" + (startRow - 1).ToString() + ")");
                        sheet_FemaleEthnic.SetCellValue("J" + indexOfEducationRow, "=IF(D" + indexOfEducationRow + ">0" + "," + "ROUND(I" + indexOfEducationRow + "/" + "D" + indexOfEducationRow + "*100,2),0)");
                        sheet_FemaleEthnic.SetCellValue("K" + indexOfEducationRow, "=SUM(K" + startmin.ToString() + ":K" + (startRow - 1).ToString() + ")");
                        sheet_FemaleEthnic.SetCellValue("L" + indexOfEducationRow, "=IF(D" + indexOfEducationRow + ">0" + "," + "ROUND(K" + indexOfEducationRow + "/" + "D" + indexOfEducationRow + "*100,2),0)");
                        sheet_FemaleEthnic.SetFormulaValue("M" + indexOfEducationRow, "=SUM(E" + indexOfEducationRow + ",G" + indexOfEducationRow + ",I" + indexOfEducationRow + ")");
                        sheet_FemaleEthnic.SetCellValue("N" + indexOfEducationRow, "=IF(D" + indexOfEducationRow + ">0" + "," + "ROUND(M" + indexOfEducationRow + "/" + "D" + indexOfEducationRow + "*100,2),0)");//Math.Round(countRealPupilOfEducation > 0 ? 100.0 * countAllAboveWeekPupil / countRealPupilOfEducation : 0.0, 2, MidpointRounding.AwayFromZero));
                    }
                    else
                    {
                        sheet_FemaleEthnic.SetCellValue("D" + indexOfEducationRow, 0);
                        sheet_FemaleEthnic.SetCellValue("E" + indexOfEducationRow, 0);
                        sheet_FemaleEthnic.SetCellValue("F" + indexOfEducationRow, 0);//Math.Round(countRealPupilOfEducation > 0 ? 100.0 * countAllGoodPupil / countRealPupilOfEducation : 0.0, 2, MidpointRounding.AwayFromZero));
                        sheet_FemaleEthnic.SetCellValue("G" + indexOfEducationRow, 0);
                        sheet_FemaleEthnic.SetCellValue("H" + indexOfEducationRow, 0);
                        sheet_FemaleEthnic.SetCellValue("I" + indexOfEducationRow, 0);
                        sheet_FemaleEthnic.SetCellValue("J" + indexOfEducationRow, 0);
                        sheet_FemaleEthnic.SetCellValue("K" + indexOfEducationRow, 0);
                        sheet_FemaleEthnic.SetCellValue("L" + indexOfEducationRow, 0);
                        sheet_FemaleEthnic.SetFormulaValue("M" + indexOfEducationRow, 0);
                        sheet_FemaleEthnic.SetCellValue("N" + indexOfEducationRow, 0);//Math.Round(countRealPupilOfEducation > 0 ? 100.0 * countAllAboveWeekPupil / countRealPupilOfEducation : 0.0, 2, MidpointRounding.AwayFromZero));
                    }
                    sheet_FemaleEthnic.SetColumnWidth('B', sheet_FemaleEthnic.GetColumnWidth('A') * 2.0);
                    sheet_FemaleEthnic.CopyPasteSameRowHeigh(rangeEducation, startRow);
                    indexOfEducationRow += listFirstCP.Count + 1;

                    index++;
                    startRow++;
                }
                sheet_FemaleEthnic.DeleteRow(startRow - 1);

                sheet_FemaleEthnic.SetCellValue("D11", "=SUM(D" + startsc.ToString() + ":D" + (startRow - 2).ToString() + ")/2");
                sheet_FemaleEthnic.SetCellValue("E11", "=SUM(E" + startsc.ToString() + ":E" + (startRow - 2).ToString() + ")/2");
                sheet_FemaleEthnic.SetCellValue("F11", "=IF(D11>0" + "," + "ROUND(E11/" + "D11*100,2),0)");//Math.Round(percentAllGoodOfSchool, 2, MidpointRounding.AwayFromZero));
                sheet_FemaleEthnic.SetCellValue("G11", "=SUM(G" + startsc.ToString() + ":G" + (startRow - 2).ToString() + ")/2");
                sheet_FemaleEthnic.SetCellValue("H11", "=IF(D11>0" + "," + "ROUND(G11/" + "D11*100,2),0)");
                sheet_FemaleEthnic.SetCellValue("I11", "=SUM(I" + startsc.ToString() + ":I" + (startRow - 2).ToString() + ")/2");
                sheet_FemaleEthnic.SetCellValue("J11", "=IF(D11>0" + "," + "ROUND(I11/" + "D11*100,2),0)");
                sheet_FemaleEthnic.SetCellValue("K11", "=SUM(K" + startsc.ToString() + ":K" + (startRow - 2).ToString() + ")/2");
                sheet_FemaleEthnic.SetCellValue("L11", "=IF(D11>0" + "," + "ROUND(K11/" + "D11*100,2),0)");
                sheet_FemaleEthnic.SetCellValue("M11", "=SUM(E11,G11,I11)");
                sheet_FemaleEthnic.SetCellValue("N11", "=IF(D11>0" + "," + "ROUND(M11/" + "D11*100,2),0)");
                sheet_FemaleEthnic.CopyPasteSameSize(rangeBoldFont, "K" + startRow);
                sheet_FemaleEthnic.SetCellValue("K" + startRow, "Người lập báo cáo");
                sheet_FemaleEthnic.GetRange(9, 1, startRow - 2, 14).SetBorder(VTBorderStyle.Solid, VTBorderWeight.Medium, VTBorderIndex.Around);
                sheet_FemaleEthnic.FitAllColumnsOnOnePage = true;
                // sheet.Delete();
                #endregion
            }
           
            firstSheet.Delete();
            return oBook.ToStream();
        }
        #endregion
    }
}
