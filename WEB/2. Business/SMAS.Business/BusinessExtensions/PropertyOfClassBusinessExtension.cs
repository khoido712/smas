/**
 * The Viettel License
 *
 * Copyright 2012 Viettel Telecom. All rights reserved.
 * VIETTEL PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */

/** 
* @author  
* @version $Revision: $
*/

using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Linq.Expressions;
using System.Web;
using log4net;

using SMAS.DAL.Repository;
using SMAS.DAL.IRepository;
using SMAS.Business.IBusiness;
using SMAS.Models.Models;
using SMAS.Business.Common;

namespace SMAS.Business.Business
{
    public partial class PropertyOfClassBusiness
    {

        #region Them moi
        public void Insert(List<PropertyOfClass> ListPropertyOfClass)
        {
            if (ListPropertyOfClass == null)
            {
                throw new BusinessException("PropertyOfClass_Err_EmptyList");
            }

            #region Kiem tra cac dieu kien dau vao va lay du lieu tu DB de kiem tra mon ngoai ngu
            foreach (PropertyOfClass PropertyOfClass in ListPropertyOfClass)
            {
                // Kiem tra lop va tinh chat da co trong CSDL
                int ClassID = PropertyOfClass.ClassID;
                ClassProfileBusiness.CheckAvailable(ClassID, "ClassProfile_Label_AllTitle", false);
                 ClassPropertyCatBusiness.CheckAvailable(PropertyOfClass.ClassPropertyCatID, "ClassPropertyCat_Label_AllTitle", false);
            }
            #endregion

            // Du lieu hop le, thuc hien them moi vao CSDL
            foreach (PropertyOfClass PropertyOfClass in ListPropertyOfClass)
            {
                this.Insert(PropertyOfClass);
            }
        }
        #endregion

        #region Cap nhat
        public void Update(List<PropertyOfClass> ListPropertyOfClass)
        {
            if (ListPropertyOfClass == null)
            {
                throw new BusinessException("PropertyOfClass_Err_EmptyList");
            }

            #region Kiem tra cac dieu kien dau vao va lay du lieu tu DB de kiem tra mon ngoai ngu
            foreach (PropertyOfClass PropertyOfClass in ListPropertyOfClass)
            {
                // Kiem tra lop va tinh chat da co trong CSDL
                int ClassID = PropertyOfClass.ClassID;
                ClassProfileBusiness.CheckAvailable(ClassID, "ClassProfile_Label_AllTitle", false);
                 ClassPropertyCatBusiness.CheckAvailable(PropertyOfClass.ClassPropertyCatID, "ClassPropertyCat_Label_AllTitle", false);
               CheckAvailable(PropertyOfClass.PropertyOfClassID, "PropertyOfClass_Label_AllTitle");
            }
            #endregion

            // Du lieu hop le, thuc hien them moi vao CSDL
            foreach (PropertyOfClass PropertyOfClass in ListPropertyOfClass)
            {
                this.Update(PropertyOfClass);
            }
        }
        #endregion

        #region Tim kiem tinh chat lop hoc hoc
        /// <summary>
        /// Tim kiem tinh chat lop hoc
        /// </summary>
        /// <param name="SearchInfo"></param>
        /// <returns></returns>
        public IQueryable<PropertyOfClass> Search(IDictionary<string, object> SearchInfo)
        {
            int EducationLevelID = Utils.GetInt(SearchInfo, "EducationLevelID");
            int ClassID = Utils.GetInt(SearchInfo, "ClassID");
            int SchoolID = Utils.GetInt(SearchInfo, "SchoolID");
            int ClassPropertyCatID = Utils.GetInt(SearchInfo, "ClassPropertyCatID");
            IQueryable<PropertyOfClass> lsPropertyOfClass = this.PropertyOfClassRepository.All;

            if (ClassID > 0)
            {
                lsPropertyOfClass = from poc in lsPropertyOfClass
                                    join cp in ClassProfileBusiness.All on poc.ClassID equals cp.ClassProfileID
                                    where poc.ClassID == ClassID && (!cp.IsActive.HasValue || (cp.IsActive.HasValue && cp.IsActive.Value))
                                    select poc;
            }
            if (EducationLevelID != 0)
            {
                lsPropertyOfClass = lsPropertyOfClass.Where(x => x.ClassProfile.EducationLevelID == EducationLevelID);
            }
            if (SchoolID != 0)
            {
                lsPropertyOfClass = lsPropertyOfClass.Where(x => x.ClassProfile.SchoolID == SchoolID);
            }
            if (ClassPropertyCatID != 0)
            {
                lsPropertyOfClass = lsPropertyOfClass.Where(x => x.ClassPropertyCatID == ClassPropertyCatID);
            }
            return lsPropertyOfClass;
        }
        #endregion

        #region Xoa du lieu
        /// <summary>
        /// Xoa du lieu
        /// </summary>
        /// <param name="ListPropertyOfClass"></param>
        public void Delete(List<PropertyOfClass> ListPropertyOfClass, int AcademicYearID )
        {
            foreach (PropertyOfClass PropertyOfClass in ListPropertyOfClass)
            {
                // Kiem tra da ton tai trong CSDL
               CheckAvailable(PropertyOfClass.PropertyOfClassID, "PropertyOfClass_Label_AllTitle", false);
                // Kiem tra thong tin nam hoc
                ClassProfile ClassProfile = ClassProfileRepository.Find(PropertyOfClass.ClassID);
                if (ClassProfile == null || ClassProfile.AcademicYearID != AcademicYearID)
                {
                    throw new BusinessException("ClassSubject_Label_AcademicYear_Err");
                }

            }
            // Khong co loi thi thuc hien xoa toan bo du lieu trong danh sach nay
            base.DeleteAll(ListPropertyOfClass);
        }
        #endregion

        #region Tim kiem tinh chat lop hoc hoc theo truong
        /// <summary>
        /// Tim kiem mon hoc cua lop theo truong
        /// </summary>
        /// <param name="SchoolID"></param>
        /// <param name="SearchInfo"></param>
        /// <returns></returns>
        public IQueryable<PropertyOfClass> SearchBySchool(int SchoolID, IDictionary<string, object> SearchInfo = null)
        {
            if (SchoolID == 0)
            {
                return null;
            }
            if (SearchInfo == null)
            {
                SearchInfo = new Dictionary<string, object>();
            }
            SearchInfo["SchoolID"]= SchoolID;
            return this.Search(SearchInfo);
        }
        #endregion

        #region Tim kiem tinh chat lop hoc hoc theo lop
        /// <summary>
        /// Tim kiem mon hoc cua lop theo lop
        /// </summary>
        /// <param name="ClassID"></param>
        /// <param name="SearchInfo"></param>
        /// <returns></returns>
        public IQueryable<PropertyOfClass> SearchByClass(int ClassID, IDictionary<string, object> SearchInfo = null)
        {
            if (ClassID == 0)
            {
                return null;
            }
            if (SearchInfo == null)
            {
                SearchInfo = new Dictionary<string, object>();
            }
            SearchInfo.Add("ClassID", ClassID);
            return this.Search(SearchInfo);
        }
        #endregion

    }
}