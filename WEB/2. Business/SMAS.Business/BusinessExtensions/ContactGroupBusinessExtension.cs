﻿/**
 * The Viettel License
 *
 * Copyright 2012 Viettel Telecom. All rights reserved.
 * VIETTEL PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */

/** 
* @author  
* @version $Revision: $
*/

using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Linq.Expressions;
using System.Web;
using log4net;

using SMAS.DAL.Repository;
using SMAS.DAL.IRepository;
using SMAS.Business.IBusiness;
using SMAS.Models.Models;
using SMAS.Business.Common;
using SMAS.Business.BusinessObject;
using System.Transactions;
using System.Text;

namespace SMAS.Business.Business
{
    public partial class ContactGroupBusiness
    {
        /// <summary>
        /// search SMS_TEACHER_CONTACT_GROUP
        /// </summary>
        public IQueryable<SMS_TEACHER_CONTACT_GROUP> Search(Dictionary<string, object> dic)
        {
            int schoolID = Utils.GetInt(dic, "schoolID");
            Guid? guid = Utils.GetGuid(dic, "guid");
            int employeeID = Utils.GetInt(dic, "employeeID");
            bool? isLock = Utils.GetNullableBool(dic, "isLock");
            bool isPrincipal = Utils.GetBool(dic, "isPrincipal", false);
            bool? isGetDefault = Utils.GetNullableBool(dic, "isGetDefault");
            int contactGroupID = Utils.GetInt(dic, "contactGroupID");

            //check default group
            if (schoolID != 0 && guid != null)
            {
                CheckDefaultGroup(schoolID, guid.Value);
            }

            IQueryable<SMS_TEACHER_CONTACT_GROUP> lstContactGroup = from ct in this.ContactGroupBusiness.All select ct;

            if (employeeID > 0)
            {
                lstContactGroup = (from g in this.ContactGroupBusiness.All
                                   join e in this.TeacherContactBusiness.All on g.CONTACT_GROUP_ID equals e.CONTACT_GROUP_ID
                                   where e.TEACHER_ID == employeeID && e.IS_ACTIVE == true
                                   select g);
            }

            if (schoolID != 0)
            {
                lstContactGroup = lstContactGroup.Where(p => p.SCHOOL_ID == schoolID).OrderByDescending(a => a.IS_DEFAULT).ThenBy(a => a.CONTACT_GROUP_NAME);
            }

            if (isGetDefault.HasValue)
            {
                lstContactGroup = lstContactGroup.Where(p => p.IS_DEFAULT == isGetDefault.Value);
            }

            if (contactGroupID != 0)
            {
                lstContactGroup = lstContactGroup.Where(p => p.CONTACT_GROUP_ID == contactGroupID);
            }

            if (isLock.HasValue)
            {
                if (!isLock.Value)
                    lstContactGroup = lstContactGroup.Where(a => a.IS_LOCK == null || a.IS_LOCK == isLock.Value);
                else
                    lstContactGroup = lstContactGroup.Where(a => a.IS_LOCK == isLock.Value);
            }

            if (isPrincipal)
            {
                lstContactGroup = this.ContactGroupBusiness.All.Where(a => a.SCHOOL_ID == schoolID && a.IS_DEFAULT == true)
                    .Union(lstContactGroup);
            }
            else
            {
                if (isGetDefault == false)
                {
                    lstContactGroup = lstContactGroup.Where(p => p.IS_DEFAULT == null || p.IS_DEFAULT == false);
                }
            }

            return lstContactGroup;
        }

        /// <summary>
        /// Check default group if empty create default group
        /// </summary>
        /// <auth>Anhvd modifier</auth>
        /// <date>12/07/2013</date>
        /// <param name="schoolID"></param>
        public void CheckDefaultGroup(int schoolID, Guid guid)
        {
            //VTODO 
            string para = string.Format("schoolID={0}", schoolID);

            try
            {
                if (this.ContactGroupBusiness.All.Where(p => p.SCHOOL_ID == schoolID && p.IS_DEFAULT).Count() > 0)
            {
                return;
            }
            else
            {
                //tao default contact cho giao vien
                    SMS_TEACHER_CONTACT_GROUP objContactGroup = new SMS_TEACHER_CONTACT_GROUP();
                    objContactGroup.CONTACT_GROUP_NAME = GlobalConstantsEdu.COMMON_CONTACT_GROUP_ALL_NAME;
                    objContactGroup.SMS_CODE = GlobalConstantsEdu.COMMON_CONTACT_GROUP_ALL_SMSCODE;
                    objContactGroup.IS_LOCK = false;
                    objContactGroup.SCHOOL_ID = schoolID;
                    objContactGroup.IS_DEFAULT = true;
                    objContactGroup.CREATE_USER_ID = guid;
                    objContactGroup.CREATED_DATE = DateTime.Now;// dung default value sql (chua thuc hien vi do loi fw)
                    this.ContactGroupBusiness.Insert(objContactGroup);
                    this.Save();
                }
            }
            catch (Exception ex)
            {
                //VTODO LogExtensions.ErrorExt(logger, DateTime.Now, para, ex);
            }
        }

        public bool CheckTeacherInGroup(int contactGroupID, int teacherID)
        {
            var obj = (from tc in TeacherContactBusiness.All
                       join cg in ContactGroupBusiness.All on tc.CONTACT_GROUP_ID equals cg.CONTACT_GROUP_ID
                       where tc.CONTACT_GROUP_ID == contactGroupID
                       && tc.TEACHER_ID == teacherID
                       && cg.IS_LOCK == false
                       && tc.IS_ACTIVE == true
                       select tc).FirstOrDefault();
            bool IsCheck = obj != null;
            return IsCheck;
        }

        /// <summary>
        /// Insert or update data
        /// </summary>
        /// <param name="dic"></param>
        /// <returns></returns>
        public SMSEduResultBO InsertOrUpdateGroup(Dictionary<string, object> dic)
        {
            //VTODO 
            string para = string.Format("schoolID={0}", Utils.GetInt("schoolID"));
            SMSEduResultBO objResultBO = new SMSEduResultBO();
            StringBuilder sbLogResult = new StringBuilder();
            objResultBO.Value = 0;//danh dau la chua co dong bo du lieu tu to bo mon de khong load lai trang
                int schoolID = Utils.GetInt(dic, "schoolID");
            int academicYearID = Utils.GetInt(dic, "academicYearID");
                int contactGroupID = Utils.GetInt(dic, "contactGroupID");
                string name = Utils.GetString(dic, "name");
                string smsCode = Utils.GetString(dic, "smsCode");
                bool isLock = Utils.GetBool(dic, "isLock");
                bool isDefault = Utils.GetBool(dic, "isDefault");
                string arrTeacherInsert = Utils.GetString(dic, "arrTeacherInsert");
                string arrTeacherDelete = Utils.GetString(dic, "arrTeacherDelete");
                Guid? guid = Utils.GetGuid(dic, "guid");
                List<int> lstTeacherInsert = arrTeacherInsert.Split(new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries).Select(p => int.Parse(p)).ToList();
                List<int> lstTeacherDelete = arrTeacherDelete.Split(new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries).Select(p => int.Parse(p)).ToList();
            List<TeacherBO> lstTeacherInSchool = EmployeeBusiness.GetTeachersOfSchool(schoolID, academicYearID);
            List<int> teacherIDInSchoolS = lstTeacherInSchool.Select(p => p.TeacherID).ToList();
            // check quyen truoc khi insert du lieu
            if (lstTeacherInsert.Where(p => !teacherIDInSchoolS.Contains(p)).Count() > 0 || lstTeacherDelete.Where(p => !teacherIDInSchoolS.Contains(p)).Count() > 0)
            {
                objResultBO.isError = true;
                objResultBO.ErrorMsg = "Common_Error_InternalError";
                return objResultBO;
            }

                if (contactGroupID != 0) //update
                {
                bool isInsertNewTeacher = false;
                SMS_TEACHER_CONTACT_GROUP objContactGroup = this.Find(contactGroupID);
                //neu xoa het giao vien trong nhom lien lac thi khong duoc phep xoa, chi xet truong hop khong nhom phai toan truong
                if (objContactGroup.IS_DEFAULT == false)
                {
                    int countTeacherInGroup = (from tc in this.TeacherContactBusiness.All
                                               where tc.CONTACT_GROUP_ID == contactGroupID
                                               && !lstTeacherDelete.Contains(tc.TEACHER_ID)
                                               && tc.IS_ACTIVE == true
                                               select tc).Count();
                    isInsertNewTeacher = lstTeacherInsert != null && lstTeacherInsert.Count > 0;
                    if (countTeacherInGroup == 0 && isInsertNewTeacher == false)
                    {
                        objResultBO.isError = true;
                        objResultBO.ErrorMsg = "ContactGroup_Confirm_NotEmpty";
                        return objResultBO;
                    }
                }

                objContactGroup.CONTACT_GROUP_NAME = name;
                objContactGroup.IS_LOCK = isLock;
                objContactGroup.SMS_CODE = smsCode;
                sbLogResult.Append("update SMS_TEACHER_CONTACT_GROUP").Append(contactGroupID.ToString());
                objResultBO.JsonResult = objContactGroup.CONTACT_GROUP_ID.ToString();
                    //insert or update employeeContact
                if (isInsertNewTeacher)
                    {
                        //list kiem tra insert or update
                    List<SMS_TEACHER_CONTACT> lstTeacherContactUpdate = (from ec in this.TeacherContactBusiness.All
                                                                         where ec.CONTACT_GROUP_ID == contactGroupID
                                                                          select ec).ToList();

                    SMS_TEACHER_CONTACT objTeacherContact = null;
                    SMS_TEACHER_CONTACT subObjTeacherContact = null;
                        //update
                    if (lstTeacherContactUpdate != null && lstTeacherContactUpdate.Count > 0)
                        {
                            int teacherID = 0;
                            for (int i = 0, size = lstTeacherInsert.Count; i < size; i++)
                            {
                                teacherID = lstTeacherInsert[i];
                            objTeacherContact = null;
                                //update
                            for (int j = lstTeacherContactUpdate.Count - 1; j >= 0; j--)
                                {
                                subObjTeacherContact = lstTeacherContactUpdate[j];
                                if (subObjTeacherContact.TEACHER_ID == teacherID)
                                    {
                                    objTeacherContact = subObjTeacherContact;
                                    objTeacherContact.IS_ACTIVE = true;
                                    sbLogResult.Append("update TeacherContact").Append(teacherID.ToString());
                                        break;
                                    }
                                }
                                //insert
                            if (objTeacherContact == null)
                                {
                                objTeacherContact = new SMS_TEACHER_CONTACT();
                                objTeacherContact.TEACHER_ID = teacherID;
                                objTeacherContact.CONTACT_GROUP_ID = contactGroupID;
                                objTeacherContact.CREATED_DATE = DateTime.Now;
                                objTeacherContact.IS_ACTIVE = true;
                                this.TeacherContactBusiness.Insert(objTeacherContact);
                                sbLogResult.Append("Insert TeacherContact").Append(teacherID.ToString());
                                }
                            }
                        }
                        else //insert 
                        {
                            for (int i = 0, size = lstTeacherInsert.Count; i < size; i++)
                            {
                            objTeacherContact = new SMS_TEACHER_CONTACT();
                            objTeacherContact.TEACHER_ID = lstTeacherInsert[i];
                            objTeacherContact.CONTACT_GROUP_ID = contactGroupID;
                            objTeacherContact.CREATED_DATE = DateTime.Now;
                            objTeacherContact.IS_ACTIVE = true;
                            this.TeacherContactBusiness.Insert(objTeacherContact);
                            sbLogResult.Append("Insert TeacherContact").Append(lstTeacherInsert[i].ToString());
                            }
                        }
                    }

                    if (lstTeacherDelete != null && lstTeacherDelete.Count > 0)//delete
                    {
                    List<SMS_TEACHER_CONTACT> lstTeacherContactDelete = this.TeacherContactBusiness.All.Where(p => lstTeacherDelete.Contains(p.TEACHER_ID) && p.CONTACT_GROUP_ID == contactGroupID).ToList();
                    if (lstTeacherContactDelete != null && lstTeacherContactDelete.Count > 0)
                        {
                        for (int i = lstTeacherContactDelete.Count - 1; i >= 0; i--)
                            {
                            lstTeacherContactDelete[i].IS_ACTIVE = false;
                            sbLogResult.Append("Delete TeacherContact").Append(lstTeacherContactDelete[i].TEACHER_ID.ToString());
                            }
                        }
                    }
                this.Save();
                }
                else //insert moi
                {
                if (lstTeacherInsert != null && lstTeacherInsert.Count > 0)
                {
                    SMS_TEACHER_CONTACT_GROUP objContactGroup = this.ContactGroupBusiness.All.Where(p => p.SMS_CODE == smsCode && p.SCHOOL_ID == schoolID).FirstOrDefault();
                    if (objContactGroup != null)
                    {
                        objResultBO.isError = true;
                        objResultBO.ErrorMsg = "SCSContactGroup_Label_ExistsGroupName";
                        return objResultBO;
                    }
                    //lien ket voi nhom lien lac da ton tai
                    objContactGroup = new SMS_TEACHER_CONTACT_GROUP();
                    objContactGroup.CONTACT_GROUP_NAME = name;
                    objContactGroup.SMS_CODE = smsCode;
                    objContactGroup.IS_LOCK = false;
                    objContactGroup.SCHOOL_ID = schoolID;
                    objContactGroup.IS_DEFAULT = false;
                    objContactGroup.CREATE_USER_ID = guid.Value;
                    objContactGroup.CREATED_DATE = DateTime.Now;
                    this.Insert(objContactGroup);
                    this.Save();

                    //insert teacherContact
                    SMS_TEACHER_CONTACT objTeacherContact = null;
                    for (int i = 0, size = lstTeacherInsert.Count; i < size; i++)
                    {
                        objTeacherContact = new SMS_TEACHER_CONTACT();
                        objTeacherContact.TEACHER_ID = lstTeacherInsert[i];
                        objTeacherContact.CONTACT_GROUP_ID = objContactGroup.CONTACT_GROUP_ID;
                        objTeacherContact.CREATED_DATE = DateTime.Now;
                        objTeacherContact.IS_ACTIVE = true;
                        this.TeacherContactBusiness.Insert(objTeacherContact);
                        sbLogResult.Append("Insert TeacherContact").Append(lstTeacherInsert[i].ToString());
                    }
                    this.Save();
                    objResultBO.JsonResult = objContactGroup.CONTACT_GROUP_ID.ToString();
                }
                else
                {
                    objResultBO.isError = true;
                    objResultBO.ErrorMsg = "ContactGroup_Confirm_NotEmpty";
                    return objResultBO;
                }
            }

            return objResultBO;
        }

        /// <summary>
        /// Check user send SMS
        /// </summary>
        /// <param name="schoolID"></param>
        /// <param name="teacherID"></param>
        /// <param name="contactGroupID"></param>
        /// <returns></returns>
        public bool CheckUserSendSMSInContactGroup(int schoolID, int teacherID, int contactGroupID)
        {
            int count = (from tc in this.TeacherContactBusiness.All
                         join cg in this.ContactGroupBusiness.All on tc.CONTACT_GROUP_ID equals cg.CONTACT_GROUP_ID
                         where tc.TEACHER_ID == teacherID
                         && cg.SCHOOL_ID == schoolID
                         && tc.CONTACT_GROUP_ID == contactGroupID
                         && tc.IS_ACTIVE == true
                         && (cg.IS_LOCK == false)
                         select tc
                         ).Count();
            if (count > 0)
                    {
                return true;
            }
            else
            {
                return false;
            }
        }

        /// <summary>
        /// Viethd4: Ham tao nhom mac dinh 
        /// </summary>
        /// <param name="schoolID"></param>
        /// <param name="guid"></param>
        public SMSEduResultBO CreateDefaultContactGroups(int schoolID, Guid guid)
                        {
            SMSEduResultBO rs = new SMSEduResultBO();

            //Ban giam hieu
            this.ContactGroupBusiness.Insert(CreateDefaultContactGroupObj(schoolID, guid, GlobalConstantsEdu.COMMON_CONTACT_GROUP_BGH));

            //Chi bo
            this.ContactGroupBusiness.Insert(CreateDefaultContactGroupObj(schoolID, guid, GlobalConstantsEdu.COMMON_CONTACT_GROUP_CB));

            //Cong doan truong
            this.ContactGroupBusiness.Insert(CreateDefaultContactGroupObj(schoolID, guid, GlobalConstantsEdu.COMMON_CONTACT_GROUP_CDT));

            //Doan thanh nien
            this.ContactGroupBusiness.Insert(CreateDefaultContactGroupObj(schoolID, guid, GlobalConstantsEdu.COMMON_CONTACT_GROUP_DTN));

            //Giao vien chu nhiem
            this.ContactGroupBusiness.Insert(CreateDefaultContactGroupObj(schoolID, guid, GlobalConstantsEdu.COMMON_CONTACT_GROUP_GVCN));

            //Giao vien bo mon
            this.ContactGroupBusiness.Insert(CreateDefaultContactGroupObj(schoolID, guid, GlobalConstantsEdu.COMMON_CONTACT_GROUP_GVBM));

            //Can bo nam
            this.ContactGroupBusiness.Insert(CreateDefaultContactGroupObj(schoolID, guid, GlobalConstantsEdu.COMMON_CONTACT_GROUP_CBNAM));

            //Can bo nu
            this.ContactGroupBusiness.Insert(CreateDefaultContactGroupObj(schoolID, guid, GlobalConstantsEdu.COMMON_CONTACT_GROUP_CBNU));

            //Dung SDT Viettel
            this.ContactGroupBusiness.Insert(CreateDefaultContactGroupObj(schoolID, guid, GlobalConstantsEdu.COMMON_CONTACT_GROUP_VIETTEL));

            this.Save();

            rs.isError = false;

            return rs;
                        }

        private SMS_TEACHER_CONTACT_GROUP CreateDefaultContactGroupObj(int schoolID, Guid guid, string name)
        {
            SMS_TEACHER_CONTACT_GROUP obj = new SMS_TEACHER_CONTACT_GROUP();
            obj.CONTACT_GROUP_NAME = name;
            obj.IS_LOCK = false;
            obj.SCHOOL_ID = schoolID;
            obj.IS_DEFAULT = false;
            obj.CREATE_USER_ID = guid;
            obj.CREATED_DATE = DateTime.Now;// dung default value sql (chua thuc hien vi do loi fw)
            obj.IS_ALLOW_MEMBER_SEND = true;
            obj.IS_ALLOW_SCHOOL_BOARD_SEND = true;

            return obj;
                    }

        public SMSEduResultBO SaveContactGroup(Dictionary<string, object> dic)
        {
            SMSEduResultBO objResultBO = new SMSEduResultBO();
            StringBuilder sbLogResult = new StringBuilder();

            int schoolID = Utils.GetInt(dic, "schoolID");
            int academicYearID = Utils.GetInt(dic, "academicYearID");
            int contactGroupID = Utils.GetInt(dic, "contactGroupID");
            int employeeID = Utils.GetInt(dic, "employeeID");
            string name = Utils.GetString(dic, "name");
            bool isLock = Utils.GetBool(dic, "isLock");
            bool isDefault = Utils.GetBool(dic, "isDefault");
            bool isAllowMemberSend = Utils.GetBool(dic, "isAllowMemberSend");
            bool isAllowSchoolBoardSend = Utils.GetBool(dic, "isAllowSchoolBoardSend");
            string assignTeachersID = Utils.GetString(dic, "assignTeachersID");
            List<int> lstNewTeacherInGroupId = Utils.GetIntList(dic, "lstTeacherInGroupId");
            Guid? guid = Utils.GetGuid(dic, "guid");

            if (isDefault)
            {
                SMS_TEACHER_CONTACT_GROUP cg = this.Find(contactGroupID);
                cg.IS_LOCK = isLock;
                this.Save();
                }
            else
            {
                //Kiem tra ten
                if (String.IsNullOrEmpty(name))
                {
                    objResultBO.isError = true;
                    objResultBO.ErrorMsg = "Tên nhóm không được để trống";
                    return objResultBO;
            }

                if (this.ContactGroupBusiness.All.Where(o => o.SCHOOL_ID == schoolID && o.CONTACT_GROUP_NAME == name && o.CONTACT_GROUP_ID != contactGroupID).Count() > 0)
            {
                objResultBO.isError = true;
                    objResultBO.ErrorMsg = "Tên nhóm không được trùng với nhóm liên lạc khác";
                    return objResultBO;
                }

                List<TeacherBO> lstTeacherInSchool = EmployeeBusiness.GetTeachersOfSchool(schoolID, academicYearID);
                List<int> teacherIDInSchoolS = lstTeacherInSchool.Select(p => p.TeacherID).ToList();

                //update
                SMS_TEACHER_CONTACT_GROUP cg;
                if (contactGroupID != 0)
                {
                    cg = this.Find(contactGroupID);

                    cg.CONTACT_GROUP_NAME = name;
                    cg.IS_LOCK = isLock;
                    cg.IS_DEFAULT = isDefault;
                    cg.IS_ALLOW_MEMBER_SEND = isAllowMemberSend;
                    cg.IS_ALLOW_SCHOOL_BOARD_SEND = isAllowSchoolBoardSend;
                    cg.ASSIGN_TEACHERS_ID = assignTeachersID;

                    //Tim cac giao vien them moi vao nhom
                    Dictionary<string, object> tmpDic = new Dictionary<string, object>();
                    dic["schoolID"] = schoolID;
                    dic["contactGroupID"] = contactGroupID;
                    dic["teacherID"] = employeeID;
                    dic["isIgnoringTeacher"] = false;
                    List<TeacherBO> lstTeacherInGroup = this.TeacherContactBusiness.GetListTeacherInGroup(dic);
                    if (lstTeacherInGroup == null) lstTeacherInGroup = new List<TeacherBO>();
                    List<int> lstTeacherInGroupID = lstTeacherInGroup.Select(o => o.TeacherID).ToList();

                    List<int> lstInsertId = lstNewTeacherInGroupId.Where(o => !lstTeacherInGroupID.Contains(o)).ToList();
                    List<int> lstDeleteId = lstTeacherInGroupID.Where(o => !lstNewTeacherInGroupId.Contains(o)).ToList();

                    //Insert giao vien vao nhom
                    List<TeacherBO> lstInsertTeacher = lstTeacherInSchool.Where(o => lstInsertId.Contains(o.TeacherID)).ToList();
                    for (int i = 0; i < lstInsertTeacher.Count; i++)
                    {
                        TeacherBO teacher = lstInsertTeacher[i];
                        SMS_TEACHER_CONTACT tc = new SMS_TEACHER_CONTACT();
                        tc.CONTACT_GROUP_ID = contactGroupID;
                        tc.CREATED_DATE = DateTime.Now;
                        tc.IS_ACTIVE = true;
                        tc.TEACHER_ID = teacher.TeacherID;
                        tc.SCHOOL_ID = schoolID;

                        this.TeacherContactBusiness.Insert(tc);
                    }
                    //Xoa cac giao vien bi loai
                    List<SMS_TEACHER_CONTACT> lstDeleteContact = this.TeacherContactBusiness.All.Where(o => o.CONTACT_GROUP_ID == contactGroupID
                        && lstDeleteId.Contains(o.TEACHER_ID)).ToList();

                    this.TeacherContactBusiness.DeleteAll(lstDeleteContact);
                }
                //insert
                else
                {
                    cg = new SMS_TEACHER_CONTACT_GROUP();
                    cg.CREATED_DATE = DateTime.Now;
                    cg.CREATE_USER_ID = guid.Value;
                    cg.IS_ALLOW_MEMBER_SEND = isAllowMemberSend;
                    cg.IS_ALLOW_SCHOOL_BOARD_SEND = isAllowSchoolBoardSend;
                    cg.ASSIGN_TEACHERS_ID = assignTeachersID;
                    cg.IS_DEFAULT = false;
                    cg.IS_LOCK = isLock;
                    cg.CONTACT_GROUP_NAME = name;
                    cg.SCHOOL_ID = schoolID;

                    this.ContactGroupBusiness.Insert(cg);

                    //Insert giao vien vao nhom
                    List<TeacherBO> lstInsertTeacher = lstTeacherInSchool.Where(o => lstNewTeacherInGroupId.Contains(o.TeacherID)).ToList();
                    for (int i = 0; i < lstInsertTeacher.Count; i++)
                    {
                        TeacherBO teacher = lstInsertTeacher[i];
                        SMS_TEACHER_CONTACT tc = new SMS_TEACHER_CONTACT();
                        tc.CONTACT_GROUP_ID = contactGroupID;
                        tc.CREATED_DATE = DateTime.Now;
                        tc.IS_ACTIVE = true;
                        tc.TEACHER_ID = teacher.TeacherID;
                        tc.SCHOOL_ID = schoolID;

                        this.TeacherContactBusiness.Insert(tc);
                    }


            }

                this.Save();
                objResultBO.Value = cg.CONTACT_GROUP_ID;
            }

            objResultBO.isError = false;
            return objResultBO;
        }

        public SMSEduResultBO DeleteContactGroup(int contactGroupId, int schoolId)
        {
            SMSEduResultBO result = new SMSEduResultBO();

            //kiem tra da co tin nhan chua
            int count = this.SMSHistoryBusiness.All.Where(o => o.CONTACT_GROUP_ID == contactGroupId && o.SCHOOL_ID == schoolId).Count();
            if (count > 0)
            {
                result.isError = true;
                result.ErrorMsg = "Contact_Group_Not_Delete";
                return result;
            }

            //Xoa danh sach lien lac trong nhom
            List<SMS_TEACHER_CONTACT> lstTeacherContact = this.TeacherContactBusiness.All.Where(o => o.CONTACT_GROUP_ID == contactGroupId).ToList();
            this.TeacherContactBusiness.DeleteAll(lstTeacherContact);

            //Xoa nhom
            SMS_TEACHER_CONTACT_GROUP cg = this.Find(contactGroupId);
            this.ContactGroupBusiness.Delete(contactGroupId);

            this.Save();

            result.isError = false;


            return result;

        }
    }
}