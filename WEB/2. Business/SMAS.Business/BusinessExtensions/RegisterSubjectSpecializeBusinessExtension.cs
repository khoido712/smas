﻿using SMAS.Business.BusinessObject;
using System;
using System.Collections.Generic;
using System.Linq;

using SMAS.DAL.Repository;
using SMAS.Models.Models;
using SMAS.Business.Common;
using System.Transactions;
using System.IO;
using SMAS.VTUtils.Excel.Export;
using SMAS.VTUtils.Log;

namespace SMAS.Business.Business
{
    public partial class RegisterSubjectSpecializeBusiness
    {
        public List<RegisterSubjectSpecializeBO> GetlistSubjectByClass(IDictionary<string, object> dic)
        {
            int schoolID = Utils.GetInt(dic, "SchoolID");
            int academicYearID = Utils.GetInt(dic, "AcademicYearID");
            int classID = Utils.GetInt(dic, "ClassID");
            int semesterID = Utils.GetInt(dic, "SemesterID");
            int subjectID = Utils.GetInt(dic, "SubjectID");
            int subjectTypeID = Utils.GetInt(dic, "SubjectTypeID");
            int Year = Utils.GetInt(dic, "YearID");
            IQueryable<RegisterSubjectSpecialize> IQueRegisterSubject = RegisterSubjectSpecializeRepository.All;

            if (schoolID > 0)
            {
                IQueRegisterSubject = IQueRegisterSubject.Where(p => p.SchoolID == schoolID);
            }

            if (academicYearID > 0)
            {
                IQueRegisterSubject = IQueRegisterSubject.Where(p => p.AcademicYearID == academicYearID);
            }

            if (classID > 0)
            {
                IQueRegisterSubject = IQueRegisterSubject.Where(p => p.ClassID == classID);
            }

            if (semesterID > 0)
            {
                IQueRegisterSubject = IQueRegisterSubject.Where(p => (semesterID == GlobalConstants.SEMESTER_OF_YEAR_ALL && (p.SemesterID == GlobalConstants.SEMESTER_OF_YEAR_FIRST || p.SemesterID == GlobalConstants.SEMESTER_OF_YEAR_SECOND)) 
                    || p.SemesterID == semesterID);
            }

            if (subjectID > 0)
            {
                IQueRegisterSubject = IQueRegisterSubject.Where(p => p.SubjectID == subjectID);
            }

            if (subjectTypeID > 0)
            {
                IQueRegisterSubject = IQueRegisterSubject.Where(p => p.SubjectID == subjectTypeID);
            }

            if (Year > 0)
            {
                IQueRegisterSubject = IQueRegisterSubject.Where(p => p.LastDigitYearID == Year);
            }

            List<RegisterSubjectSpecializeBO> lstRegisterBO = (from r in IQueRegisterSubject
                                                               select new RegisterSubjectSpecializeBO
                                                               {
                                                                   RegisterSubjectSpecializeID = r.RegisterSubjectSpecializeID,
                                                                   PupilID = r.PupilID,
                                                                   ClassID = r.ClassID,
                                                                   SubjectID = r.SubjectID,
                                                                   SubjectTypeID = r.SubjectTypeID,
                                                                   SemesterID = r.SemesterID
                                                               }).ToList();
            return lstRegisterBO;
        }

        public void InsertOrUpdate(IDictionary<string, object> dic, List<RegisterSubjectSpecialize> lstRegisterSubjectSpecializeON,
                                                                    List<RegisterSubjectSpecialize> lstRegisterSubjectSpecializeOFF)
        {
            int schoolID = Utils.GetInt(dic, "SchoolID");
            int classID = Utils.GetInt(dic, "ClassID");
            int academicYearID = Utils.GetInt(dic, "AcademicYearID");

            List<RegisterSubjectSpecialize> lstRegisterSubjectSpecializeDB = (from regis in RegisterSubjectSpecializeBusiness.All
                                                                              where regis.SchoolID == schoolID
                                                                              && regis.AcademicYearID == academicYearID
                                                                              && regis.ClassID == classID
                                                                              select regis).ToList();
            List<RegisterSubjectSpecialize> lstInsert = new List<RegisterSubjectSpecialize>();
            List<RegisterSubjectSpecialize> lstDelete = new List<RegisterSubjectSpecialize>();

            RegisterSubjectSpecialize objRegisSubjectSpecialize = null;
            RegisterSubjectSpecialize checklist = null;
            //RegisterSubjectSpecialize checkSemester = null;

            for (int i = 0; i < lstRegisterSubjectSpecializeON.Count; i++)
            {
                objRegisSubjectSpecialize = lstRegisterSubjectSpecializeON[i];

                checklist = lstRegisterSubjectSpecializeDB.Where(p => p.PupilID == objRegisSubjectSpecialize.PupilID
                    && p.SubjectID == objRegisSubjectSpecialize.SubjectID
                    && p.SemesterID == objRegisSubjectSpecialize.SemesterID
                    && p.SubjectTypeID == objRegisSubjectSpecialize.SubjectTypeID).FirstOrDefault();
                if (checklist == null)
                {
                    objRegisSubjectSpecialize.RegisterSubjectSpecializeID = GetNextSeq<long>(GlobalConstants.REGISTER_SUBJECT_SPECIALIZE_SEQ);
                    lstInsert.Add(objRegisSubjectSpecialize);
                }
            }

            //IDictionary<string, object> columnMap = ColumnMappings();
            if (lstInsert != null && lstInsert.Count > 0)
            {
                foreach (var item in lstInsert)
                {
                    this.Insert(item);
                }
                this.Save();
                //BulkInsert(lstInsert, columnMap);
            }

            if (lstRegisterSubjectSpecializeOFF != null && lstRegisterSubjectSpecializeOFF.Count > 0)
            {
                for (int i = 0; i < lstRegisterSubjectSpecializeOFF.Count; i++)
                {
                    objRegisSubjectSpecialize = lstRegisterSubjectSpecializeOFF[i];
                    checklist = lstRegisterSubjectSpecializeDB.Where(p => p.PupilID == objRegisSubjectSpecialize.PupilID
                        && p.SubjectID == objRegisSubjectSpecialize.SubjectID
                        && p.SemesterID == objRegisSubjectSpecialize.SemesterID
                        && p.SubjectTypeID == objRegisSubjectSpecialize.SubjectTypeID).FirstOrDefault();
                    if (checklist != null)
                    {
                        //objRegisSubjectSpecialize.RegisterSubjectSpecializeID = checklist.RegisterSubjectSpecializeID;
                        lstDelete.Add(checklist);
                    }
                }
            }

            if (lstDelete != null && lstDelete.Count > 0)
            {
                DeleteList(lstDelete);
            }
        }

        private IDictionary<string, object> ColumnMappings()
        {
            Dictionary<string, object> columnMap = new Dictionary<string, object>();
            
            columnMap.Add("AcademicYearID", "ACADEMIC_YEAR_ID");
            columnMap.Add("ClassID", "CLASS_ID");
            columnMap.Add("CreateTime", "CREATE_TIME");
            columnMap.Add("LastDigitYearID", "LAST_DIGIT_YEAR_ID");
            columnMap.Add("PupilID", "PUPIL_ID");
            columnMap.Add("RegisterSubjectSpecializeID", "REGISTER_SUBJECT_SPECIALIZE_ID");
            columnMap.Add("SchoolID", "SCHOOL_ID");
            columnMap.Add("SemesterID", "SEMESTER_ID");
            columnMap.Add("SubjectID", "SUBJECT_ID");
            columnMap.Add("SubjectTypeID", "SUBJECT_TYPE_ID");            
            columnMap.Add("UpdateTime", "UPDATE_TIME");
            return columnMap;
        }

        public void DeleteRegisterByListID(List<long> lstID)
        {
            for (int i = 0; i < lstID.Count; i++)
            {
                this.Delete(lstID[i]);
            }
            this.Save();
        }

        public void RegisterForClass(List<RegisterSubjectSpecializeBO> listSubject, List<PupilOfClassBO> listPupil, List<ExemptedSubject> listPupilEx, IDictionary<string, object> search)
        {
            int academicYearID = Utils.GetInt(search, "AcademicYearID");
            int classID = Utils.GetInt(search, "ClassID");
            int educationLevelID = Utils.GetInt(search, "EducationLevelID");
            int schoolID = Utils.GetInt(search, "SchoolID");
            int appliedLevel = Utils.GetInt(search, "AppliedLevel");
            AcademicYear objAcademicYear = AcademicYearBusiness.Find(academicYearID);
            int lastdigitYearId = UtilsBusiness.GetPartionId(schoolID);

            // Lay danh sach dang ki hien tai cua lop
            List<RegisterSubjectSpecialize> listRegisterSubject = RegisterSubjectSpecializeBusiness.Search(search).ToList();

            IEnumerable<RegisterSubjectSpecialize> checkInsert = null;
            IEnumerable<RegisterSubjectSpecialize> checkUpdate = null;
            RegisterSubjectSpecializeBO temp = null;
            List<RegisterSubjectSpecializeBO> listNewSubject = new List<RegisterSubjectSpecializeBO>();
            List<RegisterSubjectSpecialize> listDeleteRegister = new List<RegisterSubjectSpecialize>();
            for (int i = 0; i < listSubject.Count; i++)
            {
                temp = listSubject[i];
                checkInsert = listRegisterSubject.Where(o => o.SubjectID == temp.SubjectID && o.SubjectTypeID == temp.SubjectTypeID);
                checkUpdate = listRegisterSubject.Where(o => o.SubjectID == temp.SubjectID && o.SubjectTypeID != temp.SubjectTypeID);

                if ((checkInsert.Count() == 0 && checkUpdate.Count() == 0))
                {
                    if (temp.SubjectTypeID == 0 && temp.CheckOnChange == 1)
                    {
                        listNewSubject.Add(temp);
                    }
                    if (temp.SubjectTypeID > 0)
                    {
                        listNewSubject.Add(temp);
                    }
                }

                if (checkInsert.Count() != 0 && temp.SubjectTypeID == 0 && temp.CheckOnChange == 1)
                {
                    listNewSubject.Add(temp);
                    listDeleteRegister.AddRange(checkInsert);
                }

                if (checkUpdate.Count() != 0)
                {
                    if (temp.SubjectTypeID == 0 && temp.CheckOnChange == 1)
                    {
                        listNewSubject.Add(temp);
                        listDeleteRegister.AddRange(checkUpdate);
                    }
                    if (temp.SubjectTypeID > 0)
                    {
                        listNewSubject.Add(temp);
                        listDeleteRegister.AddRange(checkUpdate);
                    }
                }

                listRegisterSubject = listRegisterSubject.Where(o => o.SubjectID != temp.SubjectID).ToList();
            }

            // Add nhung dang ki cua mon khong co trong cac mon duoc check (bi huy) vao danh sach delete
            listDeleteRegister.AddRange(listRegisterSubject.AsEnumerable());

            // Tao danh sach dang ki can insert
            List<RegisterSubjectSpecialize> listRegister = new List<RegisterSubjectSpecialize>();
            RegisterSubjectSpecialize registerSemester = null;
            RegisterSubjectSpecializeBO subjectBO = null;
            ExemptedSubject ex = null;
            PupilOfClassBO pupil = null;

            for (int i = 0; i < listNewSubject.Count; i++)
            {
                subjectBO = listNewSubject[i];
                for (int j = 0; j < listPupil.Count; j++)
                {
                    pupil = listPupil[j];
                    // Kiem tra co mien giam
                    int testExI = 0;
                    int testExII = 0;
                    for (int k = 0; k < listPupilEx.Count; k++)
                    {
                        ex = listPupilEx[k];
                        if (ex.PupilID == pupil.PupilID && ex.SubjectID == subjectBO.SubjectID)
                        {
                            if (ex.FirstSemesterExemptType != null)
                            {
                                testExI = 1;
                            }
                            if (ex.SecondSemesterExemptType != null)
                            {
                                testExII = 1;
                            }
                            break;
                        }
                    }

                    if (subjectBO.SubjectTypeID == 0)
                    {
                        if (testExI == 0 && testExII == 0)
                        {
                            // Khoi tao dang ki
                            registerSemester = new RegisterSubjectSpecialize();

                            registerSemester.AcademicYearID = academicYearID;
                            registerSemester.SchoolID = schoolID;
                            registerSemester.ClassID = classID;
                            registerSemester.LastDigitYearID = objAcademicYear.Year;
                            registerSemester.CreateTime = DateTime.Now;
                            registerSemester.UpdateTime = null;
                            registerSemester.SubjectID = listNewSubject[i].SubjectID;
                            registerSemester.SubjectTypeID = listNewSubject[i].SubjectTypeID;
                            registerSemester.PupilID = listPupil[j].PupilID;

                            long registerID = RegisterSubjectSpecializeBusiness.GetNextSeq<long>(GlobalConstants.REGISTER_SUBJECT_SPECIALIZE_SEQ);
                            registerSemester.RegisterSubjectSpecializeID = registerID;
                            registerSemester.SemesterID = 0;
                            listRegister.Add(registerSemester);
                        }
                    }
                    else
                    {
                        // Add hoc ki I
                        if (testExI == 0)
                        {
                            if (double.Parse(subjectBO.SectionPerWeekFirstSemester) > 0)
                            {
                                // Khoi tao dang ki
                                registerSemester = new RegisterSubjectSpecialize();

                                registerSemester.AcademicYearID = academicYearID;
                                registerSemester.SchoolID = schoolID;
                                registerSemester.ClassID = classID;
                                registerSemester.LastDigitYearID = objAcademicYear.Year;
                                registerSemester.CreateTime = DateTime.Now;

                                registerSemester.SubjectID = listNewSubject[i].SubjectID;
                                registerSemester.SubjectTypeID = listNewSubject[i].SubjectTypeID;
                                registerSemester.PupilID = listPupil[j].PupilID;

                                long registerID = RegisterSubjectSpecializeBusiness.GetNextSeq<long>(GlobalConstants.REGISTER_SUBJECT_SPECIALIZE_SEQ);
                                registerSemester.RegisterSubjectSpecializeID = registerID;
                                registerSemester.SemesterID = 1;
                                listRegister.Add(registerSemester);
                            }
                        }

                        // Add hoc ki II
                        if (testExII == 0)
                        {
                            if (double.Parse(subjectBO.SectionPerWeekSecondSemester) > 0)
                            {
                                // Khoi tao dang ki
                                registerSemester = new RegisterSubjectSpecialize();

                                registerSemester.AcademicYearID = academicYearID;
                                registerSemester.SchoolID = schoolID;
                                registerSemester.ClassID = classID;
                                registerSemester.LastDigitYearID = objAcademicYear.Year;
                                registerSemester.CreateTime = DateTime.Now;
                                registerSemester.UpdateTime = DateTime.Now;

                                registerSemester.SubjectID = listNewSubject[i].SubjectID;
                                registerSemester.SubjectTypeID = listNewSubject[i].SubjectTypeID;
                                registerSemester.PupilID = listPupil[j].PupilID;

                                long registerID = RegisterSubjectSpecializeBusiness.GetNextSeq<long>(GlobalConstants.REGISTER_SUBJECT_SPECIALIZE_SEQ);
                                registerSemester.RegisterSubjectSpecializeID = registerID;
                                registerSemester.SemesterID = 2;
                                listRegister.Add(registerSemester);
                            }
                        }
                    }
                }
            }

            // Thay doi du lieu
            if (listDeleteRegister.Count > 0)
            {
                this.DeleteList(listDeleteRegister);
            }

            if (listRegister.Count > 0)
            {
                this.InsertList(listRegister);
            }
        }

        public void DeleteList(List<RegisterSubjectSpecialize> listRegisterCancel)
        {
            try
            {
                if (listRegisterCancel == null || listRegisterCancel.Count == 0)
                {
                    return;
                }

                
                this.DeleteAll(listRegisterCancel);
                this.Save();
            }
            catch (Exception ex)
            {
                
                LogExtensions.ErrorExt(logger, DateTime.Now, "DeleteList", "null", ex);
            }
            
        }

        public IQueryable<RegisterSubjectSpecialize> Search(IDictionary<string, object> dic)
        {
            int schoolID = Utils.GetInt(dic, "SchoolID");
            int academicYearID = Utils.GetInt(dic, "AcademicYearID");
            int pupilID = Utils.GetInt(dic, "PupilID");
            int classID = Utils.GetInt(dic, "ClassID");
            int subjectID = Utils.GetInt(dic, "SubjectID");
            int subjectTypeID = Utils.GetInt(dic, "SubjectTypeID");
            int semesterID = Utils.GetInt(dic, "SemesterID");
            int year = Utils.GetInt(dic, "YearID");

            IQueryable<RegisterSubjectSpecialize> query = RegisterSubjectSpecializeBusiness.All;

            if (schoolID != 0)
            {
                query = query.Where(o => o.SchoolID == schoolID);
            }
            if (academicYearID != 0)
            {
                query = query.Where(o => o.AcademicYearID == academicYearID);
            }
            if (pupilID != 0)
            {
                query = query.Where(o => o.PupilID == pupilID);
            }
            if (classID != 0)
            {
                query = query.Where(o => o.ClassID == classID);
            }
            if (subjectID != 0)
            {
                query = query.Where(o => o.SubjectID == subjectID);
            }
            if (subjectTypeID != 0)
            {
                query = query.Where(o => o.SubjectTypeID == subjectTypeID);
            }
            if (semesterID != 0)
            {
                query = query.Where(o => o.SemesterID == semesterID);
            }
            if (year != 0)
            {
                query = query.Where(o => o.LastDigitYearID == year);
            }

            return query;
        }

        public void InsertList(List<RegisterSubjectSpecialize> listRegister)
        {
            try
            {
                if (listRegister == null || listRegister.Count == 0)
                {
                    return;
                }
                context.Configuration.AutoDetectChangesEnabled = false;
                context.Configuration.ValidateOnSaveEnabled = false;
                /*foreach (var item in listRegister)
                {
                    RegisterSubjectSpecializeBusiness.Insert(item);
                }*/

                //RegisterSubjectSpecializeBusiness.Save();
                IDictionary<string, object> columnMappings = ColumnMappings();
                this.BulkInsert(listRegister, columnMappings);
            }
            catch (Exception ex)
            {
                
                LogExtensions.ErrorExt(logger, DateTime.Now, "InsertList", "null", ex);
            }
            finally
            {
                context.Configuration.AutoDetectChangesEnabled = true;
                context.Configuration.ValidateOnSaveEnabled = true;
            }
        }
    }
}
