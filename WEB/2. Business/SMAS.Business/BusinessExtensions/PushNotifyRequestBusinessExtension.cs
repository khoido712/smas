﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Linq.Expressions;
using System.Web;
using log4net;

using SMAS.DAL.Repository;
using SMAS.DAL.IRepository;
using SMAS.Business.IBusiness;
using SMAS.Models.Models;
using SMAS.Business.Common;
using SMAS.Business.BusinessObject;
using System.Transactions;

namespace SMAS.Business.Business
{
    public partial class PushNotifyRequestBusiness
    {
        /// <summary>
        /// listPush chi cho HS or GV, isPupil: 1HS, 0 GV, FucntionName = ID chuc nang dinh nghia trong constant
        /// </summary>
        /// <param name="listPush"></param>
        /// <param name="schoolID"></param>
        /// <param name="isPupil"></param>
        /// <param name="FunctionName"></param>
        public void AddPushRequest(List<PushNotificationBO> listPush, int schoolID, int FunctionName)
        {
            if (listPush != null && listPush.Count > 0)
            {
                List<PushNotifyRequest> listPushRequest = PushNotifyRequestBusiness.All
                    .Where(p => p.SchoolID == schoolID && p.FunctionName == FunctionName && p.Status == 0).ToList();

                List<PushNotifyRequest> listPushRequestInsert = new List<PushNotifyRequest>();
                PushNotifyRequest pushRequestinsert = null;
                string shortContent = string.Empty;
                int contactId = 0;
                PushNotifyRequest pushRequestObj = null;
                int IsPupil = 0;
                for (int i = 0; i < listPush.Count; i++)
                {
                    contactId = listPush[i].ContactID;
                    shortContent = listPush[i].ShortContent;
                    IsPupil = listPush[i].IsPupil;
                    pushRequestObj = listPushRequest.Where(p => p.ContactID == contactId && p.IsPupil == IsPupil && p.ShortContent == shortContent).FirstOrDefault();
                    if (pushRequestObj == null)
                    {
                        //insert vao pushrequest
                        pushRequestinsert = new PushNotifyRequest();
                        pushRequestinsert.SchoolID = schoolID;
                        pushRequestinsert.ContactID = contactId;
                        pushRequestinsert.Content = listPush[i].Content;
                        pushRequestinsert.ShortContent = shortContent;
                        pushRequestinsert.IsPupil = IsPupil;
                        pushRequestinsert.Status = 0;
                        pushRequestinsert.FunctionName = FunctionName;
                        pushRequestinsert.CreateTime = DateTime.Now;
                        listPushRequestInsert.Add(pushRequestinsert);
                    }
                }

                if (listPushRequestInsert != null && listPushRequestInsert.Count > 0)
                {
                    for (int i = 0; i < listPushRequestInsert.Count; i++)
                    {
                        PushNotifyRequestBusiness.Insert(listPushRequestInsert[i]);
                    }
                    PushNotifyRequestBusiness.Save();
                }
            }
        }

    }
}
