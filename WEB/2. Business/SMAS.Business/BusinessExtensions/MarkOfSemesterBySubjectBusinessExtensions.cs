﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SMAS.Business.BusinessObject;
using SMAS.Business.Business;
using SMAS.Business.Common;
using SMAS.Models.Models;
using System.IO;
using SMAS.VTUtils.Excel.Export;
using SMAS.DAL.Repository;

namespace SMAS.Business.Business
{
    public partial class MarkOfSemesterBySubjectBusiness
    {

        #region Lấy mảng băm cho các tham số đầu vào của thống kê điểm thi học kỳ theo môn
        /// <summary>
        /// Lấy mảng băm cho các tham số đầu vào của thống kê điểm thi học kỳ theo môn
        /// </summary>
        /// <param name="entity"></param>
        /// <returns>tungnd</returns>
        public string GetHashKey(MarkOfSemesterBySubjectBO entity)
        {
            IDictionary<string, object> dic = new Dictionary<string, object> {
                {"AcademicYearID", entity.AcademicYearID},
                {"Semester ", entity.Semester},
                {"IsCommenting ", entity.IsCommenting},
                {"SubjectID ", entity.SubjectID},
                {"AppliedLevel ", entity.AppliedLevel},
                {"StatisticLevelReportID", entity.StatisticLevelReportID}
            };
            return ReportUtils.GetHashKey(dic);
        }
        #endregion


        #region Lấy thống kê điểm thi học kỳ theo môn tính điểm được cập nhật mới nhất
        /// <summary>
        /// Lấy thống kê điểm thi học kỳ theo môn tính điểm được cập nhật mới nhất
        /// </summary>
        /// <param name="entity"></param>
        /// <returns>tungnd</returns>
        public ProcessedReport GetMarkOfSemesterBySubject(MarkOfSemesterBySubjectBO entity)
        {

            string reportCode = SystemParamsInFile.REPORT_Tk_DIEMTHI_HK_THEOMON_TINHDIEM;
            string inputParameterHashKey = GetHashKey(entity);
            return ProcessedReportBusiness.GetProcessedReport(reportCode, inputParameterHashKey);
        }
        #endregion

        #region Lưu lại thông tin thống kê điểm thi học kỳ theo môn
        /// <summary>
        /// Lưu lại thông tin thống kê điểm thi học kỳ theo môn
        /// </summary>
        /// <param name="entity"></param>
        /// <returns>tungnd</returns>
        public ProcessedReport InsertMarkOfSemesterBySubject(MarkOfSemesterBySubjectBO entity, Stream data)
        {

            string reportCode = SystemParamsInFile.REPORT_Tk_DIEMTHI_HK_THEOMON_TINHDIEM;

            ReportDefinition reportDef = ReportDefinitionBusiness.GetByCode(reportCode);
            ProcessedReport pr = new ProcessedReport();
            pr.ReportCode = reportCode;
            pr.ProcessedDate = DateTime.Now;
            pr.InputParameterHashKey = GetHashKey(entity);
            pr.ReportData = ReportUtils.Compress(data);


            //Tạo tên file HS_[THCS/THPT]_ThongKeDiemThi_[Học kỳ]_[Môn học]
            string schoolLevel = ReportUtils.ConvertAppliedLevelForReportName(entity.AppliedLevel);
            string outputNamePattern = reportDef.OutputNamePattern;
            string semester = ReportUtils.ConvertSemesterForReportName(entity.Semester);
            string Subject = this.SubjectCatBusiness.All.Where(o => o.SubjectCatID == entity.SubjectID).FirstOrDefault().DisplayName;

            outputNamePattern = outputNamePattern.Replace("[Semester]", ReportUtils.StripVNSign(semester));
            outputNamePattern = outputNamePattern.Replace("[Subject]", ReportUtils.StripVNSign(Subject));
            outputNamePattern = outputNamePattern.Replace("PTTH", schoolLevel);
            pr.ReportName = ReportUtils.RemoveSpecialCharacters(outputNamePattern) + "." + reportDef.OutputFormat;

            IDictionary<string, object> dic = new Dictionary<string, object> {
                {"AcademicYearID", entity.AcademicYearID},
                {"Semester", entity.Semester},
                {"IsCommenting", entity.IsCommenting},
                {"SubjectID", entity.SubjectID},
                {"StatisticLevelReportID", entity.StatisticLevelReportID}
            };
            ProcessedReportParameterRepository.Insert(dic, pr);
            ProcessedReportRepository.Insert(pr);
            ProcessedReportRepository.Save();
            return pr;
        }
        #endregion

        #region Tạo file báo cáo thông tin bảng điểm học sinh theo kỳ - Môn tính điểm
        /// <summary>
        /// Tạo file báo cáo thông tin bảng điểm học sinh theo kỳ
        /// </summary>
        /// <param name="entity"></param>
        /// <returns>tungnd</returns>
        public Stream CreateMarkOfSemesterBySubject(MarkOfSemesterBySubjectBO entity)
        {
            string reportCode = SystemParamsInFile.REPORT_Tk_DIEMTHI_HK_THEOMON_TINHDIEM;
            ReportDefinition reportDef = ReportDefinitionBusiness.GetByCode(reportCode);

            SchoolProfile school = this.SchoolProfileBusiness.Find(entity.SchoolID);
            int partitionId = UtilsBusiness.GetPartionId(entity.SchoolID, 100);
            string SchoolName = school.SchoolName.ToUpper();

            AcademicYear Aca = this.AcademicYearBusiness.Find(entity.AcademicYearID);
            string AcademicYear = Aca.DisplayTitle;

            string Suppervising = UtilsBusiness.GetSupervisingDeptName(school.SchoolProfileID, entity.AppliedLevel).ToUpper();
            string Subject = this.SubjectCatBusiness.Find(entity.SubjectID).DisplayName.ToUpper();
            string SemesterName = "";

            if (entity.Semester == SystemParamsInFile.SEMESTER_OF_YEAR_FIRST)
                SemesterName = "HỌC KỲ I";
            if (entity.Semester == SystemParamsInFile.SEMESTER_OF_YEAR_SECOND)
                SemesterName = "HỌC KỲ II";
            if (entity.Semester == SystemParamsInFile.SEMESTER_OF_YEAR_ALL)
                SemesterName = "CẢ NĂM";

            string templatePath = SystemParamsInFile.TEMPLATE_FOLDER + reportDef.TemplateDirectory + "/" + reportDef.TemplateName;
            IVTWorkbook oBook = VTExport.OpenWorkbook(templatePath);

            //Lấy sheet template

            IVTWorksheet firstSheet = oBook.GetSheet(1);

            //Tạo sheet
            IVTWorksheet sheet = oBook.CopySheetToLast(firstSheet, "Q10");

            // Lấy danh sách khối học
            List<EducationLevel> lstEducationLevel = this.EducationLevelBusiness.GetByGrade(entity.AppliedLevel).ToList();
            //Lấy ra mức thống kê
            int StatisticLevelReportID = entity.StatisticLevelReportID;
            List<StatisticLevelConfig> lstStatisticLevelConfig = StatisticLevelConfigBusiness.Search(new Dictionary<string, object>() { { "StatisticLevelReportID", StatisticLevelReportID } }).OrderBy(o => o.OrderNumber).ToList();
            int numberConfig = lstStatisticLevelConfig.Count();
            IDictionary<string, object> Dic_ClassProfile = new Dictionary<string, object> {
                                                                    {"AcademicYearID", entity.AcademicYearID},
                                                                    {"SchoolID", entity.SchoolID},
                                                                    {"SubjectID", entity.SubjectID}
                                                                };
            var lstClassAllEdu = (from p in this.ClassSubjectBusiness.SearchBySchool(entity.SchoolID, Dic_ClassProfile)
                                  join q in ClassProfileBusiness.All on p.ClassID equals q.ClassProfileID
                                  where q.IsActive.Value
                                  && q.AcademicYearID == entity.AcademicYearID
                                  select new
                                  {
                                      ClassID = q.ClassProfileID,
                                      EducationLevelID = q.EducationLevelID,
                                      OrderNumber = q.OrderNumber,
                                      DisplayName = q.DisplayName,
                                      SubjectID = p.SubjectID
                                  }).ToList();
            lstClassAllEdu = lstClassAllEdu.OrderBy(o => o.OrderNumber.HasValue ? o.OrderNumber : 0).ThenBy(o => o.DisplayName).ToList();
            // Lấy danh sách điểm MarkRecord
            int MarkTypeID = this.MarkTypeBusiness.All.Where(o => o.AppliedLevel == entity.AppliedLevel && o.Title == "HK").SingleOrDefault().MarkTypeID;

            IDictionary<string, object> Dic_MarkRecord = new Dictionary<string, object> {
                                                                {"AcademicYearID", entity.AcademicYearID},
                                                                {"SubjectID", entity.SubjectID},
                                                                {"Semester", entity.Semester},
                                                                {"MarkTypeID", MarkTypeID},
                                                                {"Year", Aca.Year}
                                                            };

            Dictionary<string, object> dicPupilRanking = new Dictionary<string, object>
            {
                {"AcademicYearID", entity.AcademicYearID},
                {"SchoolID", entity.SchoolID},
                {"Semester", entity.Semester},
                {"AppliedLevel", entity.AppliedLevel}
            };

            //lay danh sach hoc sinh dang ky mon chuyen mon tu chon
            IDictionary<string, object> dicRegis = new Dictionary<string, object>()
            {
                {"SchoolID",entity.SchoolID},
                {"AcademicYearID",entity.AcademicYearID},
                {"SubjectID",entity.SubjectID},
                {"SemesterID",entity.Semester},
                {"YearID",Aca.Year}
            };
            //List<RegisterSubjectSpecializeBO> lstRegis = RegisterSubjectSpecializeBusiness.GetlistSubjectByClass(dicRegis);

            IQueryable<PupilOfClassBO> lstQPoc = from p in PupilOfClassBusiness.SearchBySchool(entity.SchoolID, dicPupilRanking).AddCriteriaSemester(Aca, entity.Semester)
                                                 join cs in ClassSubjectBusiness.All on p.ClassID equals cs.ClassID
                                                 join cp in ClassProfileBusiness.All.Where(s => s.AcademicYearID == entity.AcademicYearID && s.IsActive == true) on cs.ClassID equals cp.ClassProfileID
                                                 join q in PupilProfileBusiness.All on p.PupilID equals q.PupilProfileID
                                                 where cs.SubjectID == entity.SubjectID
                                                 && cs.Last2digitNumberSchool == partitionId
                                                 && p.ClassID == cp.ClassProfileID
                                                 select new PupilOfClassBO
                                                 {
                                                     PupilID = p.PupilID,
                                                     Genre = q.Genre,
                                                     EthnicID = q.EthnicID,
                                                     ClassID = p.ClassID,
                                                     SubjectID = cs.SubjectID,
                                                     AppliedType = cs.AppliedType,
                                                     IsSpecialize = cs.IsSpecializedSubject,
                                                     IsVNEN = cs.IsSubjectVNEN,
                                                     EducationLevelID = p.ClassProfile.EducationLevelID
                                                 };

            //Danh sach hoc sinh mien giam
            List<ExemptedSubject> lstExempSubject = ExemptedSubjectBusiness.GetExmpSubjectbyAcademicYear(entity.AcademicYearID, entity.Semester, entity.SubjectID, 0);

            
            List<PupilOfClassBO> lstPOC = this.GetPupilOfClassInSpecialize(lstQPoc, dicRegis);
            List<PupilOfClassBO> lstPOCNotVnen = this.GetPupilOfClassInSpecialize(lstQPoc.Where(o => o.IsVNEN == null || o.IsVNEN == false), dicRegis);
            List<PupilOfClassBO> lstPOCVnen = this.GetPupilOfClassInSpecialize(lstQPoc.Where(o => o.IsVNEN == true), dicRegis);


            //Khong lay cac hoc sinh mien giam
            if (lstExempSubject != null && lstExempSubject.Count > 0)
            {
                lstPOC = lstPOC.Where(p => !lstExempSubject.Exists(s => s.PupilID == p.PupilID && p.ClassID == s.ClassID && s.SubjectID == p.SubjectID)).ToList();
            }
            //Chi lay cac diem thi cua hoc sinh dang hoc

            var tmp = (from u in VMarkRecordBusiness.SearchBySchool(entity.SchoolID, Dic_MarkRecord)
                       join p in PupilProfileBusiness.All on u.PupilID equals p.PupilProfileID
                       join c in ClassProfileBusiness.All on u.ClassID equals c.ClassProfileID
                       where u.SubjectID == entity.SubjectID
                       && c.IsActive.Value
                       && c.AcademicYearID == entity.AcademicYearID
                       select new VMarkRecordBO
                       {
                           PupilID = u.PupilID,
                           Mark = u.Mark,
                           EducationLevelID = c.EducationLevelID,
                           ClassID = u.ClassID,
                           Genre = p.Genre,
                           EthnicID = p.EthnicID,
                           SubjectID = u.SubjectID
                       });
            var iqMark = (from m in tmp.ToList()
                          join l in lstPOCNotVnen on new { m.ClassID, m.PupilID, m.SubjectID } equals new { l.ClassID, l.PupilID, l.SubjectID }
                          select m);

            
            List<VMarkRecordBO> lstMarkRecord = iqMark.ToList();

            //Khong lay cac hoc sinh mien giam
            if (lstExempSubject != null && lstExempSubject.Count > 0)
            {
                lstMarkRecord = lstMarkRecord.Where(p => !lstExempSubject.Exists(s => s.PupilID == p.PupilID && p.ClassID == s.ClassID && s.SubjectID == p.SubjectID)).ToList();
            }
            List<VMarkRecordBO> lstMarRecordFemale = iqMark.Where(o => o.Genre == SystemParamsInFile.GENRE_FEMALE).ToList();
            //Học sinh dân tộc 
            //Dân tộc kinh
            Ethnic Ethnic_Kinh = EthnicBusiness.All.Where(o => o.EthnicCode.ToLower().Equals(SystemParamsInFile.ETHNIC_CODE_KINH.ToLower()) && o.IsActive == true).FirstOrDefault();
            Ethnic Ethnic_ForeignPeople = EthnicBusiness.All.Where(o => o.EthnicCode.ToLower().Equals(SystemParamsInFile.ETHNIC_CODE_NN.ToLower()) && o.IsActive == true).FirstOrDefault();
            int Ethnic_IDKinh = 0;
            if (Ethnic_Kinh != null)
            {
                Ethnic_IDKinh = Ethnic_Kinh.EthnicID;
            }
            int Ethnic_IDNN = 0;
            if (Ethnic_ForeignPeople != null)
            {
                Ethnic_IDNN = Ethnic_ForeignPeople.EthnicID;
            }
            var lstMarkRecordEthnic = iqMark.Where(o => o.EthnicID.HasValue && (o.EthnicID != Ethnic_IDKinh && o.EthnicID != Ethnic_IDNN)).ToList();
            var lstMarkRecordFemaleEthnic = iqMark.Where(o => o.Genre == SystemParamsInFile.GENRE_FEMALE && o.EthnicID.HasValue && (o.EthnicID != Ethnic_IDKinh && o.EthnicID != Ethnic_IDNN)).ToList();


            //Lay diem cuoi ky cac hoc sinh hoc mon hoc VNEN
            int partition = UtilsBusiness.GetPartionId(entity.SchoolID);
            var tmpTnbs = from tnbs in TeacherNoteBookSemesterBusiness.All
                          join cp in ClassProfileBusiness.All.Where(o => o.AcademicYearID == entity.AcademicYearID && o.SchoolID == entity.SchoolID && o.IsActive == true)
                             on tnbs.ClassID equals cp.ClassProfileID
                          join pp in PupilProfileBusiness.All on tnbs.PupilID equals pp.PupilProfileID
                          where tnbs.AcademicYearID == entity.AcademicYearID
                          && tnbs.PartitionID == partition
                          && tnbs.SchoolID == entity.SchoolID
                          && tnbs.SemesterID == entity.Semester
                          && tnbs.SubjectID == entity.SubjectID
                          select new
                          {
                              EducationLevelID = cp.EducationLevelID,
                              SubjectID = tnbs.SubjectID,
                              Judgement = tnbs.PERIODIC_SCORE_END_JUDGLE,
                              Mark = tnbs.PERIODIC_SCORE_END,
                              Genre = pp.Genre,
                              EthnicID = pp.EthnicID,
                              ClassID = tnbs.ClassID,
                              PupilID = tnbs.PupilID
                          };

            var iqTnbs = (from j in tmpTnbs.ToList()
                          join l in lstPOCVnen on new { j.ClassID, j.PupilID, j.SubjectID } equals new { l.ClassID, l.PupilID, l.SubjectID }
                          select j);

            var lstTnbsAll = iqTnbs.ToList();
            //Khong lay cac hoc sinh mien giam
            if (lstExempSubject != null && lstExempSubject.Count > 0)
            {
                lstTnbsAll = lstTnbsAll.Where(p => !lstExempSubject.Exists(s => s.PupilID == p.PupilID && p.ClassID == s.ClassID && s.SubjectID == p.SubjectID)).ToList();
            }
            var lstTnbs_Female = lstTnbsAll.Where(o => o.Genre == SystemParamsInFile.GENRE_FEMALE).ToList();
            var lstTnbs_Ethnic = lstTnbsAll.Where(o => o.EthnicID.HasValue && o.EthnicID != Ethnic_IDNN && o.EthnicID != Ethnic_IDKinh).ToList();
            var lstTnbs_FemaleEthnic = lstTnbsAll.Where(o => o.Genre == SystemParamsInFile.GENRE_FEMALE && o.EthnicID.HasValue && o.EthnicID != Ethnic_IDNN && o.EthnicID != Ethnic_IDKinh).ToList();

            // Lay danh sach hoc sinh theo lop
            var listCountPupilByClass = lstPOC.GroupBy(o => new { o.ClassID, o.SubjectID }).Select(o => new { ClassID = o.Key.ClassID, SubjectID = o.Key.SubjectID, TotalPupil = o.Count() }).ToList();
            var listCountPupilByClass_Female = lstPOC.Where(o => o.Genre == SystemParamsInFile.GENRE_FEMALE).GroupBy(o => new { o.ClassID, o.SubjectID }).Select(o => new { ClassID = o.Key.ClassID, SubjectID = o.Key.SubjectID, TotalPupil = o.Count() });
            var listCountPupilByClass_Ethnic = lstPOC.Where(o => o.EthnicID.HasValue && o.EthnicID != Ethnic_IDKinh && o.EthnicID != Ethnic_IDNN).GroupBy(o => new { o.ClassID, o.SubjectID }).Select(o => new { ClassID = o.Key.ClassID, SubjectID = o.Key.SubjectID, TotalPupil = o.Count() }).ToList();
            var listCountPupilByClass_FemaleEthnic = lstPOC.Where(o => o.Genre == SystemParamsInFile.GENRE_FEMALE && o.EthnicID.HasValue && o.EthnicID != Ethnic_IDKinh && o.EthnicID != Ethnic_IDNN).GroupBy(o => new { o.ClassID, o.SubjectID }).Select(o => new { ClassID = o.Key.ClassID, SubjectID = o.Key.SubjectID, TotalPupil = o.Count() }).ToList();
            #region Tạo Sheet điểm thi học kỳ theo môn
            int startrow = 11;
            int lastColumn = numberConfig * 2 + 3;
            IVTRange range = firstSheet.GetRange(11, 1, 11, lastColumn);
            IVTRange rangeClass = firstSheet.GetRange(13, 1, 13, lastColumn);
            IVTRange rangeLastClass = firstSheet.GetRange(14, 1, 14, lastColumn);
            IVTRange range_level = firstSheet.GetRange("D9", "E10");
            int n = 0;
            for (int i = 0; i < numberConfig; i++)
            {
                sheet.CopyPasteSameSize(range_level, 9, 4 + n);
                sheet.SetCellValue(9, 4 + n, lstStatisticLevelConfig[i].Title);
                n += 2;
            }
            //Fill dữ liệu chung
            sheet.Name = "DiemThiHK";
            sheet.SetCellValue("A2", Suppervising);
            sheet.SetCellValue("A3", SchoolName);
            sheet.SetCellValue("A6", "THỐNG KÊ ĐIỂM THI HỌC KỲ");
            sheet.SetCellValue("A7", "MÔN " + Subject + " - " + SemesterName + " - NĂM HỌC: " + AcademicYear);
            sheet.SetCellValue("H4", (school.District != null ? school.District.DistrictName + ", n" : "N") + "gày " + DateTime.Now.Day + " tháng " + DateTime.Now.Month + " năm " + DateTime.Now.Year);


            for (int i = 0; i < lstEducationLevel.Count; i++)
            {
                int totalPupil = 0;
                int startrowSum = startrow;
                int startrowClass = startrow + 1;
                // Lấy danh sách các lớp trong từng khối
                var lstClass = lstClassAllEdu.Where(o => o.EducationLevelID == lstEducationLevel[i].EducationLevelID).OrderBy(o => o.OrderNumber.HasValue ? o.OrderNumber : 0).ThenBy(o => o.DisplayName).ToList();
                var lstMarkRecord_Edu = lstMarkRecord.Where(o => o.EducationLevelID == lstEducationLevel[i].EducationLevelID && o.SubjectID == entity.SubjectID).ToList();
                var lstTnbs_Edu = lstTnbsAll.Where(o => o.EducationLevelID == lstEducationLevel[i].EducationLevelID && o.SubjectID == entity.SubjectID).ToList();

                for (int j = 0; j < lstClass.Count; j++)
                {
                    //Khởi tạo lại n = 0
                    n = 0;
                    int classID = lstClass[j].ClassID;
                    var listMarkBySubjectEdu = lstMarkRecord_Edu.Where(o => o.ClassID == classID).Select(o => o.Mark).ToList();
                    var listTnbsBySubjectEdu = lstTnbs_Edu.Where(o => o.ClassID == classID).Select(o => o.Mark).ToList();
                    //sỹ số học sinh trong lớp là số lượng học sinh ở trạng thái đang học hoặc đã tốt nghiệp
                    var objCountPupil = listCountPupilByClass.Where(o => o.ClassID == classID && o.SubjectID == entity.SubjectID).FirstOrDefault();
                    int SumPupil = objCountPupil != null ? objCountPupil.TotalPupil : 0;
                    totalPupil += SumPupil;

                    // fill vào excel
                    if (j == lstClass.Count - 1)
                        sheet.CopyPasteSameRowHeigh(rangeLastClass, startrowClass);
                    else
                        sheet.CopyPasteSameRowHeigh(rangeClass, startrowClass);
                    sheet.SetCellValue(startrowClass, 1, j + 1);
                    sheet.SetCellValue(startrowClass, 2, lstClass[j].DisplayName);
                    sheet.SetCellValue(startrowClass, 3, SumPupil);
                    for (int m = 0; m < numberConfig; m++)
                    {
                        StatisticLevelConfig slc = lstStatisticLevelConfig[m];
                        if (slc.SignMin == SystemParamsInFile.Sign_Compare.GREATER_OR_EQUAL_SIGN)
                        {
                            if (slc.SignMax == SystemParamsInFile.Sign_Compare.SMALLER_OR_EQUAL_SIGN)
                            {
                                int SL = listMarkBySubjectEdu.Where(o => o >= slc.MinValue && o <= slc.MaxValue).Count()
                                    + listTnbsBySubjectEdu.Where(o => o >= slc.MinValue && o <= slc.MaxValue).Count();
                                sheet.SetCellValue(startrowClass, 4 + n, SL);//E
                                sheet.SetCellValue(startrowClass, 5 + n, "=IF(C" + startrowClass + ">0" + "," + "ROUND(" + UtilsBusiness.GetExcelColumnName(4 + n) + startrowClass + "/" + "C" + startrowClass + "*100,2),0)");
                            }
                            else if (slc.SignMax == SystemParamsInFile.Sign_Compare.SMALLER_SIGN)
                            {
                                int SL = listMarkBySubjectEdu.Where(o => o >= slc.MinValue && o < slc.MaxValue).Count()
                                    + listTnbsBySubjectEdu.Where(o => o >= slc.MinValue && o < slc.MaxValue).Count();
                                sheet.SetCellValue(startrowClass, 4 + n, SL);//E
                                sheet.SetCellValue(startrowClass, 5 + n, "=IF(C" + startrowClass + ">0" + "," + "ROUND(" + UtilsBusiness.GetExcelColumnName(4 + n) + startrowClass + "/" + "C" + startrowClass + "*100,2),0)");
                            }
                            else if (slc.SignMax == SystemParamsInFile.Sign_Compare.EQUAL_SIGN)
                            {
                                int SL = listMarkBySubjectEdu.Where(o => o >= slc.MinValue && o == slc.MaxValue).Count()
                                    + listTnbsBySubjectEdu.Where(o => o >= slc.MinValue && o == slc.MaxValue).Count();
                                sheet.SetCellValue(startrowClass, 4 + n, SL);//E
                                sheet.SetCellValue(startrowClass, 5 + n, "=IF(C" + startrowClass + ">0" + "," + "ROUND(" + UtilsBusiness.GetExcelColumnName(4 + n) + startrowClass + "/" + "C" + startrowClass + "*100,2),0)");
                            }
                            else
                            {
                                int SL = listMarkBySubjectEdu.Where(o => o >= slc.MinValue).Count()
                                    + listTnbsBySubjectEdu.Where(o => o >= slc.MinValue).Count();
                                sheet.SetCellValue(startrowClass, 4 + n, SL);//E
                                sheet.SetCellValue(startrowClass, 5 + n, "=IF(C" + startrowClass + ">0" + "," + "ROUND(" + UtilsBusiness.GetExcelColumnName(4 + n) + startrowClass + "/" + "C" + startrowClass + "*100,2),0)");
                            }
                        }
                        else if (slc.SignMin == SystemParamsInFile.Sign_Compare.GREATEER_THAN_SIGN)
                        {
                            if (slc.SignMax == SystemParamsInFile.Sign_Compare.SMALLER_OR_EQUAL_SIGN)
                            {
                                int SL = listMarkBySubjectEdu.Where(o => o > slc.MinValue && o <= slc.MaxValue).Count()
                                    + listTnbsBySubjectEdu.Where(o => o > slc.MinValue && o <= slc.MaxValue).Count();
                                sheet.SetCellValue(startrowClass, 4 + n, SL);//E
                                sheet.SetCellValue(startrowClass, 5 + n, "=IF(C" + startrowClass + ">0" + "," + "ROUND(" + UtilsBusiness.GetExcelColumnName(4 + n) + startrowClass + "/" + "C" + startrowClass + "*100,2),0)");
                            }
                            else if (slc.SignMax == SystemParamsInFile.Sign_Compare.SMALLER_SIGN)
                            {
                                int SL = listMarkBySubjectEdu.Where(o => o > slc.MinValue && o < slc.MaxValue).Count()
                                    + listTnbsBySubjectEdu.Where(o => o > slc.MinValue && o < slc.MaxValue).Count();
                                sheet.SetCellValue(startrowClass, 4 + n, SL);//E
                                sheet.SetCellValue(startrowClass, 5 + n, "=IF(C" + startrowClass + ">0" + "," + "ROUND(" + UtilsBusiness.GetExcelColumnName(4 + n) + startrowClass + "/" + "C" + startrowClass + "*100,2),0)");
                            }
                            else if (slc.SignMax == SystemParamsInFile.Sign_Compare.EQUAL_SIGN)
                            {
                                int SL = listMarkBySubjectEdu.Where(o => o > slc.MinValue && o == slc.MaxValue).Count()
                                    + listTnbsBySubjectEdu.Where(o => o > slc.MinValue && o == slc.MaxValue).Count();
                                sheet.SetCellValue(startrowClass, 4 + n, SL);//E
                                sheet.SetCellValue(startrowClass, 5 + n, "=IF(C" + startrowClass + ">0" + "," + "ROUND(" + UtilsBusiness.GetExcelColumnName(4 + n) + startrowClass + "/" + "C" + startrowClass + "*100,2),0)");
                            }
                            else
                            {
                                int SL = listMarkBySubjectEdu.Where(o => o > slc.MinValue).Count()
                                    + listTnbsBySubjectEdu.Where(o => o > slc.MinValue).Count();
                                sheet.SetCellValue(startrowClass, 4 + n, SL);//E
                                sheet.SetCellValue(startrowClass, 5 + n, "=IF(C" + startrowClass + ">0" + "," + "ROUND(" + UtilsBusiness.GetExcelColumnName(4 + n) + startrowClass + "/" + "C" + startrowClass + "*100,2),0)");
                            }
                        }
                        else
                        {
                            if (slc.SignMax == SystemParamsInFile.Sign_Compare.SMALLER_OR_EQUAL_SIGN)
                            {
                                int SL = listMarkBySubjectEdu.Where(o => o <= slc.MaxValue).Count()
                                    + listTnbsBySubjectEdu.Where(o => o <= slc.MaxValue).Count();
                                sheet.SetCellValue(startrowClass, 4 + n, SL);//E
                                sheet.SetCellValue(startrowClass, 5 + n, "=IF(C" + startrowClass + ">0" + "," + "ROUND(" + UtilsBusiness.GetExcelColumnName(4 + n) + startrowClass + "/" + "C" + startrowClass + "*100,2),0)");
                            }
                            else if (slc.SignMax == SystemParamsInFile.Sign_Compare.SMALLER_SIGN)
                            {
                                int SL = listMarkBySubjectEdu.Where(o => o < slc.MaxValue).Count()
                                    + listTnbsBySubjectEdu.Where(o => o < slc.MaxValue).Count();
                                sheet.SetCellValue(startrowClass, 4 + n, SL);//E
                                sheet.SetCellValue(startrowClass, 5 + n, "=IF(C" + startrowClass + ">0" + "," + "ROUND(" + UtilsBusiness.GetExcelColumnName(4 + n) + startrowClass + "/" + "C" + startrowClass + "*100,2),0)");
                            }
                            else if (slc.SignMax == SystemParamsInFile.Sign_Compare.EQUAL_SIGN)
                            {
                                int SL = listMarkBySubjectEdu.Where(o => o == slc.MaxValue).Count()
                                    + listTnbsBySubjectEdu.Where(o => o == slc.MaxValue).Count();
                                sheet.SetCellValue(startrowClass, 4 + n, SL);//E
                                sheet.SetCellValue(startrowClass, 5 + n, "=IF(C" + startrowClass + ">0" + "," + "ROUND(" + UtilsBusiness.GetExcelColumnName(4 + n) + startrowClass + "/" + "C" + startrowClass + "*100,2),0)");
                            }
                        }
                        n += 2;
                    }
                    startrowClass++;
                    startrow++;
                }
                sheet.CopyPasteSameRowHeigh(range, startrowSum);
                sheet.SetCellValue(startrowSum, 1, i + 1);
                sheet.SetCellValue(startrowSum, 2, lstEducationLevel[i].Resolution);
                n = 0;
                if (lstClass.Count > 0)
                {
                    sheet.SetCellValue(startrowSum, 3, "=SUM(C" + (startrowSum + 1).ToString() + ":C" + startrow.ToString() + ")");
                    for (int m = 0; m < numberConfig; m++)
                    {
                        sheet.SetCellValue(startrowSum, 4 + n, "=SUM(" + UtilsBusiness.GetExcelColumnName(4 + n) + (startrowSum + 1).ToString() + ":" + UtilsBusiness.GetExcelColumnName(4 + n) + startrow.ToString() + ")");
                        sheet.SetCellValue(startrowSum, 5 + n, "=IF(C" + startrowSum + ">0" + "," + "ROUND(" + UtilsBusiness.GetExcelColumnName(4 + n) + startrowSum + "/" + "C" + startrowSum + "*100,2),0)");
                        n += 2;
                    }
                }
                else
                {
                    sheet.SetCellValue(startrowSum, 3, 0);
                    for (int m = 0; m < numberConfig; m++)
                    {
                        sheet.SetCellValue(startrowSum, 4 + n, 0);
                        sheet.SetCellValue(startrowSum, 5 + n, 0);
                        n += 2;
                    }
                }
                startrow++;
            }
            if (entity.AppliedLevel == SystemParamsInFile.APPLIED_LEVEL_SECONDARY)
            {
                sheet.GetRange(9, lastColumn, 14 + lstClassAllEdu.Count(), lastColumn).SetBorder(VTBorderStyle.Solid, VTBorderWeight.Medium, VTBorderIndex.EdgeRight);
            }
            else
            {
                sheet.GetRange(9, lastColumn, 13 + lstClassAllEdu.Count(), lastColumn).SetBorder(VTBorderStyle.Solid, VTBorderWeight.Medium, VTBorderIndex.EdgeRight);
            }
            sheet.SetCellValue(UtilsBusiness.GetExcelColumnName(1) + startrow, "(*): Tổng số học sinh của lớp không bao gồm các học sinh được miễn giảm môn tương ứng");
            sheet.SetFontName("Times New Roman", 0);
            sheet.FitAllColumnsOnOnePage = true;
            #endregion


            if (entity.FemaleChecked)
            {
                #region Tạo Sheet điểm thi học kỳ học sinh nữ
                IVTWorksheet sheet_Female = oBook.CopySheetToLast(firstSheet, "Q10");
                n = 0;
                for (int i = 0; i < numberConfig; i++)
                {
                    sheet_Female.CopyPasteSameSize(range_level, 9, 4 + n);
                    sheet_Female.SetCellValue(9, 4 + n, lstStatisticLevelConfig[i].Title);
                    n += 2;
                }
                sheet_Female.Name = "HS_Nu";
                sheet_Female.SetCellValue("A6", "THỐNG KÊ ĐIỂM THI HỌC KỲ HỌC SINH NỮ");
                sheet_Female.SetCellValue("A2", Suppervising);
                sheet_Female.SetCellValue("A3", SchoolName);
                sheet_Female.SetCellValue("A7", "MÔN " + Subject + " - " + SemesterName + " - NĂM HỌC: " + AcademicYear);
                sheet_Female.SetCellValue("H4", (school.District != null ? school.District.DistrictName + ", n" : "N") + "gày " + DateTime.Now.Day + " tháng " + DateTime.Now.Month + " năm " + DateTime.Now.Year);
                startrow = 11;
                for (int i = 0; i < lstEducationLevel.Count; i++)
                {
                    int startrowSum = startrow;
                    int startrowClass = startrow + 1;
                    // Lấy danh sách các lớp trong từng khối
                    int EducationLevelID = lstEducationLevel[i].EducationLevelID;
                    var lstClass = lstClassAllEdu.Where(o => o.EducationLevelID == EducationLevelID).OrderBy(o => o.OrderNumber.HasValue ? o.OrderNumber : 0).ThenBy(o => o.DisplayName).ToList();
                    var lstMarkRecord_Edu = lstMarRecordFemale.Where(o => o.EducationLevelID == EducationLevelID).ToList();
                    var lstTnbs_Edu = lstTnbs_Female.Where(o => o.EducationLevelID == lstEducationLevel[i].EducationLevelID && o.SubjectID == entity.SubjectID).ToList();
                    for (int j = 0; j < lstClass.Count; j++)
                    {
                        //Khởi tạo lại n = 0
                        n = 0;
                        int classID = lstClass[j].ClassID;
                        var listMarkBySubjectEdu = lstMarkRecord_Edu.Where(o => o.ClassID == classID).Select(o => o.Mark).ToList();
                        var listTnbsBySubjectEdu = lstTnbs_Edu.Where(o => o.ClassID == classID).Select(o => o.Mark).ToList();
                        //sỹ số học sinh trong lớp là số lượng học sinh ở trạng thái đang học hoặc đã tốt nghiệp
                        var objCountPupil = listCountPupilByClass_Female.Where(o => o.ClassID == classID).FirstOrDefault();
                        int SumPupil = objCountPupil != null ? objCountPupil.TotalPupil : 0;

                        // fill vào excel
                        if (j == lstClass.Count - 1)
                            sheet_Female.CopyPasteSameRowHeigh(rangeLastClass, startrowClass);
                        else
                            sheet_Female.CopyPasteSameRowHeigh(rangeClass, startrowClass);

                        sheet_Female.SetCellValue(startrowClass, 1, j + 1);
                        sheet_Female.SetCellValue(startrowClass, 2, lstClass[j].DisplayName);
                        sheet_Female.SetCellValue(startrowClass, 3, SumPupil);
                        for (int m = 0; m < numberConfig; m++)
                        {
                            StatisticLevelConfig slc = lstStatisticLevelConfig[m];
                            if (slc.SignMin == SystemParamsInFile.Sign_Compare.GREATER_OR_EQUAL_SIGN)
                            {
                                if (slc.SignMax == SystemParamsInFile.Sign_Compare.SMALLER_OR_EQUAL_SIGN)
                                {
                                    int SL = listMarkBySubjectEdu.Where(o => o >= slc.MinValue && o <= slc.MaxValue).Count()
                                        + listTnbsBySubjectEdu.Where(o => o >= slc.MinValue && o <= slc.MaxValue).Count();
                                    sheet_Female.SetCellValue(startrowClass, 4 + n, SL);//E
                                    sheet_Female.SetCellValue(startrowClass, 5 + n, "=IF(C" + startrowClass + ">0" + "," + "ROUND(" + UtilsBusiness.GetExcelColumnName(4 + n) + startrowClass + "/" + "C" + startrowClass + "*100,2),0)");
                                }
                                else if (slc.SignMax == SystemParamsInFile.Sign_Compare.SMALLER_SIGN)
                                {
                                    int SL = listMarkBySubjectEdu.Where(o => o >= slc.MinValue && o < slc.MaxValue).Count()
                                        + listTnbsBySubjectEdu.Where(o => o >= slc.MinValue && o < slc.MaxValue).Count();
                                    sheet_Female.SetCellValue(startrowClass, 4 + n, SL);//E
                                    sheet_Female.SetCellValue(startrowClass, 5 + n, "=IF(C" + startrowClass + ">0" + "," + "ROUND(" + UtilsBusiness.GetExcelColumnName(4 + n) + startrowClass + "/" + "C" + startrowClass + "*100,2),0)");
                                }
                                else if (slc.SignMax == SystemParamsInFile.Sign_Compare.EQUAL_SIGN)
                                {
                                    int SL = listMarkBySubjectEdu.Where(o => o >= slc.MinValue && o == slc.MaxValue).Count()
                                        + listTnbsBySubjectEdu.Where(o => o >= slc.MinValue && o == slc.MaxValue).Count();

                                    sheet_Female.SetCellValue(startrowClass, 4 + n, SL);//E
                                    sheet_Female.SetCellValue(startrowClass, 5 + n, "=IF(C" + startrowClass + ">0" + "," + "ROUND(" + UtilsBusiness.GetExcelColumnName(4 + n) + startrowClass + "/" + "C" + startrowClass + "*100,2),0)");
                                }
                                else
                                {
                                    int SL = listMarkBySubjectEdu.Where(o => o >= slc.MinValue).Count()
                                        + listTnbsBySubjectEdu.Where(o => o >= slc.MinValue).Count();
                                    sheet_Female.SetCellValue(startrowClass, 4 + n, SL);//E
                                    sheet_Female.SetCellValue(startrowClass, 5 + n, "=IF(C" + startrowClass + ">0" + "," + "ROUND(" + UtilsBusiness.GetExcelColumnName(4 + n) + startrowClass + "/" + "C" + startrowClass + "*100,2),0)");
                                }

                            }
                            else if (slc.SignMin == SystemParamsInFile.Sign_Compare.GREATEER_THAN_SIGN)
                            {
                                if (slc.SignMax == SystemParamsInFile.Sign_Compare.SMALLER_OR_EQUAL_SIGN)
                                {
                                    int SL = listMarkBySubjectEdu.Where(o => o > slc.MinValue && o <= slc.MaxValue).Count()
                                        + listTnbsBySubjectEdu.Where(o => o > slc.MinValue && o <= slc.MaxValue).Count();
                                    sheet_Female.SetCellValue(startrowClass, 4 + n, SL);//E
                                    sheet_Female.SetCellValue(startrowClass, 5 + n, "=IF(C" + startrowClass + ">0" + "," + "ROUND(" + UtilsBusiness.GetExcelColumnName(4 + n) + startrowClass + "/" + "C" + startrowClass + "*100,2),0)");
                                }
                                else if (slc.SignMax == SystemParamsInFile.Sign_Compare.SMALLER_SIGN)
                                {
                                    int SL = listMarkBySubjectEdu.Where(o => o > slc.MinValue && o < slc.MaxValue).Count()
                                        + listTnbsBySubjectEdu.Where(o => o > slc.MinValue && o < slc.MaxValue).Count();
                                    sheet_Female.SetCellValue(startrowClass, 4 + n, SL);//E
                                    sheet_Female.SetCellValue(startrowClass, 5 + n, "=IF(C" + startrowClass + ">0" + "," + "ROUND(" + UtilsBusiness.GetExcelColumnName(4 + n) + startrowClass + "/" + "C" + startrowClass + "*100,2),0)");
                                }
                                else if (slc.SignMax == SystemParamsInFile.Sign_Compare.EQUAL_SIGN)
                                {
                                    int SL = listMarkBySubjectEdu.Where(o => o > slc.MinValue && o == slc.MaxValue).Count()
                                        + listTnbsBySubjectEdu.Where(o => o > slc.MinValue && o == slc.MaxValue).Count();
                                    sheet_Female.SetCellValue(startrowClass, 4 + n, SL);//E
                                    sheet_Female.SetCellValue(startrowClass, 5 + n, "=IF(C" + startrowClass + ">0" + "," + "ROUND(" + UtilsBusiness.GetExcelColumnName(4 + n) + startrowClass + "/" + "C" + startrowClass + "*100,2),0)");
                                }
                                else
                                {
                                    int SL = listMarkBySubjectEdu.Where(o => o > slc.MinValue).Count()
                                        + listTnbsBySubjectEdu.Where(o => o > slc.MinValue).Count();
                                    sheet_Female.SetCellValue(startrowClass, 4 + n, SL);//E
                                    sheet_Female.SetCellValue(startrowClass, 5 + n, "=IF(C" + startrowClass + ">0" + "," + "ROUND(" + UtilsBusiness.GetExcelColumnName(4 + n) + startrowClass + "/" + "C" + startrowClass + "*100,2),0)");
                                }

                            }
                            else
                            {
                                if (slc.SignMax == SystemParamsInFile.Sign_Compare.SMALLER_OR_EQUAL_SIGN)
                                {
                                    int SL = listMarkBySubjectEdu.Where(o => o <= slc.MaxValue).Count()
                                        + listTnbsBySubjectEdu.Where(o => o <= slc.MaxValue).Count();
                                    sheet_Female.SetCellValue(startrowClass, 4 + n, SL);//E
                                    sheet_Female.SetCellValue(startrowClass, 5 + n, "=IF(C" + startrowClass + ">0" + "," + "ROUND(" + UtilsBusiness.GetExcelColumnName(4 + n) + startrowClass + "/" + "C" + startrowClass + "*100,2),0)");
                                }
                                else if (slc.SignMax == SystemParamsInFile.Sign_Compare.SMALLER_SIGN)
                                {
                                    int SL = listMarkBySubjectEdu.Where(o => o < slc.MaxValue).Count()
                                        + listTnbsBySubjectEdu.Where(o => o < slc.MaxValue).Count();
                                    sheet_Female.SetCellValue(startrowClass, 4 + n, SL);//E
                                    sheet_Female.SetCellValue(startrowClass, 5 + n, "=IF(C" + startrowClass + ">0" + "," + "ROUND(" + UtilsBusiness.GetExcelColumnName(4 + n) + startrowClass + "/" + "C" + startrowClass + "*100,2),0)");
                                }
                                else if (slc.SignMax == SystemParamsInFile.Sign_Compare.EQUAL_SIGN)
                                {
                                    int SL = listMarkBySubjectEdu.Where(o => o == slc.MaxValue).Count()
                                        + listTnbsBySubjectEdu.Where(o => o == slc.MaxValue).Count();
                                    sheet_Female.SetCellValue(startrowClass, 4 + n, SL);//E
                                    sheet_Female.SetCellValue(startrowClass, 5 + n, "=IF(C" + startrowClass + ">0" + "," + "ROUND(" + UtilsBusiness.GetExcelColumnName(4 + n) + startrowClass + "/" + "C" + startrowClass + "*100,2),0)");
                                }

                            }
                            n += 2;
                        }
                        startrowClass++;
                        startrow++;
                    }
                    sheet_Female.CopyPasteSameRowHeigh(range, startrowSum);
                    sheet_Female.SetCellValue(startrowSum, 1, i + 1);
                    sheet_Female.SetCellValue(startrowSum, 2, lstEducationLevel[i].Resolution);
                    n = 0;
                    if (lstClass.Count > 0)
                    {
                        sheet_Female.SetCellValue(startrowSum, 3, "=SUM(C" + (startrowSum + 1).ToString() + ":C" + startrow.ToString() + ")");
                        for (int m = 0; m < numberConfig; m++)
                        {
                            sheet_Female.SetCellValue(startrowSum, 4 + n, "=SUM(" + UtilsBusiness.GetExcelColumnName(4 + n) + (startrowSum + 1).ToString() + ":" + UtilsBusiness.GetExcelColumnName(4 + n) + startrow.ToString() + ")");
                            sheet_Female.SetCellValue(startrowSum, 5 + n, "=IF(C" + startrowSum + ">0" + "," + "ROUND(" + UtilsBusiness.GetExcelColumnName(4 + n) + startrowSum + "/" + "C" + startrowSum + "*100,2),0)");
                            n += 2;
                        }
                    }
                    else
                    {
                        sheet_Female.SetCellValue(startrowSum, 3, 0);
                        for (int m = 0; m < numberConfig; m++)
                        {
                            sheet_Female.SetCellValue(startrowSum, 4 + n, 0);
                            sheet_Female.SetCellValue(startrowSum, 5 + n, 0);
                            n += 2;
                        }
                    }
                    startrow++;
                }
                if (entity.AppliedLevel == SystemParamsInFile.APPLIED_LEVEL_SECONDARY)
                {
                    sheet_Female.GetRange(9, lastColumn, 14 + lstClassAllEdu.Count(), lastColumn).SetBorder(VTBorderStyle.Solid, VTBorderWeight.Medium, VTBorderIndex.EdgeRight);
                }
                else
                {
                    sheet_Female.GetRange(9, lastColumn, 13 + lstClassAllEdu.Count(), lastColumn).SetBorder(VTBorderStyle.Solid, VTBorderWeight.Medium, VTBorderIndex.EdgeRight);
                }
                sheet_Female.FitAllColumnsOnOnePage = true;
                sheet_Female.SetCellValue(UtilsBusiness.GetExcelColumnName(1) + startrow, "(*): Tổng số học sinh của lớp không bao gồm các học sinh được miễn giảm môn tương ứng");
                sheet_Female.SetFontName("Times New Roman", 0);
                #endregion
            }
            if (entity.EthnicChecked)
            {
                #region Tạo sheet điểm thi học kỳ học sinh dân tộc
                IVTWorksheet sheet_Ethnic = oBook.CopySheetToLast(firstSheet, "Q10");
                //KHởi tạo lại dòng bắt đầu
                n = 0;
                for (int i = 0; i < numberConfig; i++)
                {
                    sheet_Ethnic.CopyPasteSameSize(range_level, 9, 4 + n);
                    sheet_Ethnic.SetCellValue(9, 4 + n, lstStatisticLevelConfig[i].Title);
                    n += 2;
                }
                startrow = 11;
                sheet_Ethnic.Name = "HS_DT";
                sheet_Ethnic.SetCellValue("A6", "THỐNG KÊ ĐIỂM THI HỌC KỲ HỌC SINH DÂN TỘC");
                sheet_Ethnic.SetCellValue("A2", Suppervising);
                sheet_Ethnic.SetCellValue("A3", SchoolName);
                sheet_Ethnic.SetCellValue("A7", "MÔN " + Subject + " - " + SemesterName + " - NĂM HỌC: " + AcademicYear);
                sheet_Ethnic.SetCellValue("H4", (school.District != null ? school.District.DistrictName + ", n" : "N") + "gày " + DateTime.Now.Day + " tháng " + DateTime.Now.Month + " năm " + DateTime.Now.Year);
                for (int i = 0; i < lstEducationLevel.Count; i++)
                {
                    int startrowSum = startrow;
                    int startrowClass = startrow + 1;
                    // Lấy danh sách các lớp trong từng khối
                    int EducationLevelID = lstEducationLevel[i].EducationLevelID;
                    var lstClass = lstClassAllEdu.Where(o => o.EducationLevelID == EducationLevelID).OrderBy(o => o.OrderNumber.HasValue ? o.OrderNumber : 0).ThenBy(o => o.DisplayName).ToList();
                    var lstMarkRecord_Edu = lstMarkRecordEthnic.Where(o => o.EducationLevelID == EducationLevelID).ToList();
                    var lstTnbs_Edu = lstTnbs_Ethnic.Where(o => o.EducationLevelID == lstEducationLevel[i].EducationLevelID && o.SubjectID == entity.SubjectID).ToList();
                    for (int j = 0; j < lstClass.Count; j++)
                    {
                        //Khởi tạo lại n = 0
                        n = 0;
                        int classID = lstClass[j].ClassID;
                        var listMarkBySubjectEdu = lstMarkRecord_Edu.Where(o => o.ClassID == classID).Select(o => o.Mark).ToList();
                        var listTnbsBySubjectEdu = lstTnbs_Edu.Where(o => o.ClassID == classID).Select(o => o.Mark).ToList();
                        //sỹ số học sinh trong lớp là số lượng học sinh ở trạng thái đang học hoặc đã tốt nghiệp
                        var objCountPupil = listCountPupilByClass_Ethnic.Where(o => o.ClassID == classID).FirstOrDefault();
                        int SumPupil = objCountPupil != null ? objCountPupil.TotalPupil : 0;

                        // fill vào excel
                        if (j == lstClass.Count - 1)
                            sheet_Ethnic.CopyPasteSameRowHeigh(rangeLastClass, startrowClass);
                        else
                            sheet_Ethnic.CopyPasteSameRowHeigh(rangeClass, startrowClass);
                        sheet_Ethnic.SetCellValue(startrowClass, 1, j + 1);
                        sheet_Ethnic.SetCellValue(startrowClass, 2, lstClass[j].DisplayName);
                        sheet_Ethnic.SetCellValue(startrowClass, 3, SumPupil);
                        for (int m = 0; m < numberConfig; m++)
                        {
                            StatisticLevelConfig slc = lstStatisticLevelConfig[m];
                            if (slc.SignMin == SystemParamsInFile.Sign_Compare.GREATER_OR_EQUAL_SIGN)
                            {
                                if (slc.SignMax == SystemParamsInFile.Sign_Compare.SMALLER_OR_EQUAL_SIGN)
                                {
                                    int SL = listMarkBySubjectEdu.Where(o => o >= slc.MinValue && o <= slc.MaxValue).Count()
                                        + listTnbsBySubjectEdu.Where(o => o >= slc.MinValue && o <= slc.MaxValue).Count();
                                    sheet_Ethnic.SetCellValue(startrowClass, 4 + n, SL);//E
                                    sheet_Ethnic.SetCellValue(startrowClass, 5 + n, "=IF(C" + startrowClass + ">0" + "," + "ROUND(" + UtilsBusiness.GetExcelColumnName(4 + n) + startrowClass + "/" + "C" + startrowClass + "*100,2),0)");
                                }
                                else if (slc.SignMax == SystemParamsInFile.Sign_Compare.SMALLER_SIGN)
                                {
                                    int SL = listMarkBySubjectEdu.Where(o => o >= slc.MinValue && o < slc.MaxValue).Count()
                                        + listTnbsBySubjectEdu.Where(o => o >= slc.MinValue && o < slc.MaxValue).Count();
                                    sheet_Ethnic.SetCellValue(startrowClass, 4 + n, SL);//E
                                    sheet_Ethnic.SetCellValue(startrowClass, 5 + n, "=IF(C" + startrowClass + ">0" + "," + "ROUND(" + UtilsBusiness.GetExcelColumnName(4 + n) + startrowClass + "/" + "C" + startrowClass + "*100,2),0)");
                                }
                                else if (slc.SignMax == SystemParamsInFile.Sign_Compare.EQUAL_SIGN)
                                {
                                    int SL = listMarkBySubjectEdu.Where(o => o >= slc.MinValue && o == slc.MaxValue).Count()
                                        + listTnbsBySubjectEdu.Where(o => o >= slc.MinValue && o == slc.MaxValue).Count();
                                    sheet_Ethnic.SetCellValue(startrowClass, 4 + n, SL);//E
                                    sheet_Ethnic.SetCellValue(startrowClass, 5 + n, "=IF(C" + startrowClass + ">0" + "," + "ROUND(" + UtilsBusiness.GetExcelColumnName(4 + n) + startrowClass + "/" + "C" + startrowClass + "*100,2),0)");
                                }
                                else
                                {
                                    int SL = listMarkBySubjectEdu.Where(o => o >= slc.MinValue).Count()
                                        + listTnbsBySubjectEdu.Where(o => o >= slc.MinValue).Count();
                                    sheet_Ethnic.SetCellValue(startrowClass, 4 + n, SL);//E
                                    sheet_Ethnic.SetCellValue(startrowClass, 5 + n, "=IF(C" + startrowClass + ">0" + "," + "ROUND(" + UtilsBusiness.GetExcelColumnName(4 + n) + startrowClass + "/" + "C" + startrowClass + "*100,2),0)");
                                }

                            }
                            else if (slc.SignMin == SystemParamsInFile.Sign_Compare.GREATEER_THAN_SIGN)
                            {
                                if (slc.SignMax == SystemParamsInFile.Sign_Compare.SMALLER_OR_EQUAL_SIGN)
                                {
                                    int SL = listMarkBySubjectEdu.Where(o => o > slc.MinValue && o <= slc.MaxValue).Count()
                                        + listTnbsBySubjectEdu.Where(o => o > slc.MinValue && o <= slc.MaxValue).Count();
                                    sheet_Ethnic.SetCellValue(startrowClass, 4 + n, SL);//E
                                    sheet_Ethnic.SetCellValue(startrowClass, 5 + n, "=IF(C" + startrowClass + ">0" + "," + "ROUND(" + UtilsBusiness.GetExcelColumnName(4 + n) + startrowClass + "/" + "C" + startrowClass + "*100,2),0)");
                                }
                                else if (slc.SignMax == SystemParamsInFile.Sign_Compare.SMALLER_SIGN)
                                {
                                    int SL = listMarkBySubjectEdu.Where(o => o > slc.MinValue && o < slc.MaxValue).Count()
                                        + listTnbsBySubjectEdu.Where(o => o > slc.MinValue && o < slc.MaxValue).Count();
                                    sheet_Ethnic.SetCellValue(startrowClass, 4 + n, SL);//E
                                    sheet_Ethnic.SetCellValue(startrowClass, 5 + n, "=IF(C" + startrowClass + ">0" + "," + "ROUND(" + UtilsBusiness.GetExcelColumnName(4 + n) + startrowClass + "/" + "C" + startrowClass + "*100,2),0)");
                                }
                                else if (slc.SignMax == SystemParamsInFile.Sign_Compare.EQUAL_SIGN)
                                {
                                    int SL = listMarkBySubjectEdu.Where(o => o > slc.MinValue && o == slc.MaxValue).Count()
                                        + listTnbsBySubjectEdu.Where(o => o > slc.MinValue && o == slc.MaxValue).Count();
                                    sheet_Ethnic.SetCellValue(startrowClass, 4 + n, SL);//E
                                    sheet_Ethnic.SetCellValue(startrowClass, 5 + n, "=IF(C" + startrowClass + ">0" + "," + "ROUND(" + UtilsBusiness.GetExcelColumnName(4 + n) + startrowClass + "/" + "C" + startrowClass + "*100,2),0)");
                                }
                                else
                                {
                                    int SL = listMarkBySubjectEdu.Where(o => o > slc.MinValue).Count()
                                        + listTnbsBySubjectEdu.Where(o => o > slc.MinValue).Count();
                                    sheet_Ethnic.SetCellValue(startrowClass, 4 + n, SL);//E
                                    sheet_Ethnic.SetCellValue(startrowClass, 5 + n, "=IF(C" + startrowClass + ">0" + "," + "ROUND(" + UtilsBusiness.GetExcelColumnName(4 + n) + startrowClass + "/" + "C" + startrowClass + "*100,2),0)");
                                }

                            }
                            else
                            {
                                if (slc.SignMax == SystemParamsInFile.Sign_Compare.SMALLER_OR_EQUAL_SIGN)
                                {
                                    int SL = listMarkBySubjectEdu.Where(o => o <= slc.MaxValue).Count()
                                        + listTnbsBySubjectEdu.Where(o => o <= slc.MaxValue).Count();
                                    sheet_Ethnic.SetCellValue(startrowClass, 4 + n, SL);//E
                                    sheet_Ethnic.SetCellValue(startrowClass, 5 + n, "=IF(C" + startrowClass + ">0" + "," + "ROUND(" + UtilsBusiness.GetExcelColumnName(4 + n) + startrowClass + "/" + "C" + startrowClass + "*100,2),0)");
                                }
                                else if (slc.SignMax == SystemParamsInFile.Sign_Compare.SMALLER_SIGN)
                                {
                                    int SL = listMarkBySubjectEdu.Where(o => o < slc.MaxValue).Count()
                                        + listTnbsBySubjectEdu.Where(o => o < slc.MaxValue).Count();
                                    sheet_Ethnic.SetCellValue(startrowClass, 4 + n, SL);//E
                                    sheet_Ethnic.SetCellValue(startrowClass, 5 + n, "=IF(C" + startrowClass + ">0" + "," + "ROUND(" + UtilsBusiness.GetExcelColumnName(4 + n) + startrowClass + "/" + "C" + startrowClass + "*100,2),0)");
                                }
                                else if (slc.SignMax == SystemParamsInFile.Sign_Compare.EQUAL_SIGN)
                                {
                                    int SL = listMarkBySubjectEdu.Where(o => o == slc.MaxValue).Count()
                                        + listTnbsBySubjectEdu.Where(o => o == slc.MaxValue).Count();
                                    sheet_Ethnic.SetCellValue(startrowClass, 4 + n, SL);//E
                                    sheet_Ethnic.SetCellValue(startrowClass, 5 + n, "=IF(C" + startrowClass + ">0" + "," + "ROUND(" + UtilsBusiness.GetExcelColumnName(4 + n) + startrowClass + "/" + "C" + startrowClass + "*100,2),0)");
                                }

                            }
                            n += 2;
                        }
                        startrowClass++;
                        startrow++;
                    }
                    sheet_Ethnic.CopyPasteSameRowHeigh(range, startrowSum);
                    sheet_Ethnic.SetCellValue(startrowSum, 1, i + 1);
                    sheet_Ethnic.SetCellValue(startrowSum, 2, lstEducationLevel[i].Resolution);
                    n = 0;
                    if (lstClass.Count > 0)
                    {
                        sheet_Ethnic.SetCellValue(startrowSum, 3, "=SUM(C" + (startrowSum + 1).ToString() + ":C" + startrow.ToString() + ")");
                        for (int m = 0; m < numberConfig; m++)
                        {
                            sheet_Ethnic.SetCellValue(startrowSum, 4 + n, "=SUM(" + UtilsBusiness.GetExcelColumnName(4 + n) + (startrowSum + 1).ToString() + ":" + UtilsBusiness.GetExcelColumnName(4 + n) + startrow.ToString() + ")");
                            sheet_Ethnic.SetCellValue(startrowSum, 5 + n, "=IF(C" + startrowSum + ">0" + "," + "ROUND(" + UtilsBusiness.GetExcelColumnName(4 + n) + startrowSum + "/" + "C" + startrowSum + "*100,2),0)");
                            n += 2;
                        }
                    }
                    else
                    {
                        sheet_Ethnic.SetCellValue(startrowSum, 3, 0);
                        for (int m = 0; m < numberConfig; m++)
                        {
                            sheet_Ethnic.SetCellValue(startrowSum, 4 + n, 0);
                            sheet_Ethnic.SetCellValue(startrowSum, 5 + n, 0);
                            n += 2;
                        }
                    }
                    startrow++;
                }
                if (entity.AppliedLevel == SystemParamsInFile.APPLIED_LEVEL_SECONDARY)
                {
                    sheet_Ethnic.GetRange(9, lastColumn, 14 + lstClassAllEdu.Count(), lastColumn).SetBorder(VTBorderStyle.Solid, VTBorderWeight.Medium, VTBorderIndex.EdgeRight);
                }
                else
                {
                    sheet_Ethnic.GetRange(9, lastColumn, 13 + lstClassAllEdu.Count(), lastColumn).SetBorder(VTBorderStyle.Solid, VTBorderWeight.Medium, VTBorderIndex.EdgeRight);
                }
                sheet_Ethnic.FitAllColumnsOnOnePage = true;
                sheet_Ethnic.SetCellValue(UtilsBusiness.GetExcelColumnName(1) + startrow, "(*): Tổng số học sinh của lớp không bao gồm các học sinh được miễn giảm môn tương ứng");
                sheet_Ethnic.SetFontName("Times New Roman", 0);
                #endregion
            }

            if (entity.FemaleEthnicChecked)
            {
                #region Tạo sheet điểm thi học kỳ học sinh nu dân tộc
                IVTWorksheet sheet_FemaleEthnic = oBook.CopySheetToLast(firstSheet, "Q10");
                //KHởi tạo lại dòng bắt đầu
                n = 0;
                for (int i = 0; i < numberConfig; i++)
                {
                    sheet_FemaleEthnic.CopyPasteSameSize(range_level, 9, 4 + n);
                    sheet_FemaleEthnic.SetCellValue(9, 4 + n, lstStatisticLevelConfig[i].Title);
                    n += 2;
                }
                startrow = 11;
                sheet_FemaleEthnic.Name = "HS_NU_DT";
                sheet_FemaleEthnic.SetCellValue("A6", "THỐNG KÊ ĐIỂM THI HỌC KỲ HỌC SINH NỮ DÂN TỘC");
                sheet_FemaleEthnic.SetCellValue("A2", Suppervising);
                sheet_FemaleEthnic.SetCellValue("A3", SchoolName);
                sheet_FemaleEthnic.SetCellValue("A7", "MÔN " + Subject + " - " + SemesterName + " - NĂM HỌC: " + AcademicYear);
                sheet_FemaleEthnic.SetCellValue("H4", (school.District != null ? school.District.DistrictName + ", n" : "N") + "gày " + DateTime.Now.Day + " tháng " + DateTime.Now.Month + " năm " + DateTime.Now.Year);
                for (int i = 0; i < lstEducationLevel.Count; i++)
                {
                    int startrowSum = startrow;
                    int startrowClass = startrow + 1;
                    // Lấy danh sách các lớp trong từng khối
                    int EducationLevelID = lstEducationLevel[i].EducationLevelID;
                    var lstClass = lstClassAllEdu.Where(o => o.EducationLevelID == EducationLevelID).OrderBy(o => o.OrderNumber.HasValue ? o.OrderNumber : 0).ThenBy(o => o.DisplayName).ToList();
                    var lstMarkRecord_Edu = lstMarkRecordFemaleEthnic.Where(o => o.EducationLevelID == EducationLevelID).ToList();
                    var lstTnbs_Edu = lstTnbs_FemaleEthnic.Where(o => o.EducationLevelID == lstEducationLevel[i].EducationLevelID && o.SubjectID == entity.SubjectID).ToList();
                    for (int j = 0; j < lstClass.Count; j++)
                    {
                        //Khởi tạo lại n = 0
                        n = 0;
                        int classID = lstClass[j].ClassID;
                        var listMarkBySubjectEdu = lstMarkRecord_Edu.Where(o => o.ClassID == classID).Select(o => o.Mark).ToList();
                        var listTnbsBySubjectEdu = lstTnbs_Edu.Where(o => o.ClassID == classID).Select(o => o.Mark).ToList();
                        //sỹ số học sinh trong lớp là số lượng học sinh ở trạng thái đang học hoặc đã tốt nghiệp
                        var objCountPupil = listCountPupilByClass_FemaleEthnic.Where(o => o.ClassID == classID).FirstOrDefault();
                        int SumPupil = objCountPupil != null ? objCountPupil.TotalPupil : 0;

                        // fill vào excel
                        if (j == lstClass.Count - 1)
                            sheet_FemaleEthnic.CopyPasteSameRowHeigh(rangeLastClass, startrowClass);
                        else
                            sheet_FemaleEthnic.CopyPasteSameRowHeigh(rangeClass, startrowClass);
                        sheet_FemaleEthnic.SetCellValue(startrowClass, 1, j + 1);
                        sheet_FemaleEthnic.SetCellValue(startrowClass, 2, lstClass[j].DisplayName);
                        sheet_FemaleEthnic.SetCellValue(startrowClass, 3, SumPupil);
                        for (int m = 0; m < numberConfig; m++)
                        {
                            StatisticLevelConfig slc = lstStatisticLevelConfig[m];
                            if (slc.SignMin == SystemParamsInFile.Sign_Compare.GREATER_OR_EQUAL_SIGN)
                            {
                                if (slc.SignMax == SystemParamsInFile.Sign_Compare.SMALLER_OR_EQUAL_SIGN)
                                {
                                    int SL = listMarkBySubjectEdu.Where(o => o >= slc.MinValue && o <= slc.MaxValue).Count()
                                        + listTnbsBySubjectEdu.Where(o => o >= slc.MinValue && o <= slc.MaxValue).Count();
                                    sheet_FemaleEthnic.SetCellValue(startrowClass, 4 + n, SL);//E
                                    sheet_FemaleEthnic.SetCellValue(startrowClass, 5 + n, "=IF(C" + startrowClass + ">0" + "," + "ROUND(" + UtilsBusiness.GetExcelColumnName(4 + n) + startrowClass + "/" + "C" + startrowClass + "*100,2),0)");
                                }
                                else if (slc.SignMax == SystemParamsInFile.Sign_Compare.SMALLER_SIGN)
                                {
                                    int SL = listMarkBySubjectEdu.Where(o => o >= slc.MinValue && o < slc.MaxValue).Count()
                                        + listTnbsBySubjectEdu.Where(o => o >= slc.MinValue && o < slc.MaxValue).Count();
                                    sheet_FemaleEthnic.SetCellValue(startrowClass, 4 + n, SL);//E
                                    sheet_FemaleEthnic.SetCellValue(startrowClass, 5 + n, "=IF(C" + startrowClass + ">0" + "," + "ROUND(" + UtilsBusiness.GetExcelColumnName(4 + n) + startrowClass + "/" + "C" + startrowClass + "*100,2),0)");
                                }
                                else if (slc.SignMax == SystemParamsInFile.Sign_Compare.EQUAL_SIGN)
                                {
                                    int SL = listMarkBySubjectEdu.Where(o => o >= slc.MinValue && o == slc.MaxValue).Count()
                                        + listTnbsBySubjectEdu.Where(o => o >= slc.MinValue && o == slc.MaxValue).Count();
                                    sheet_FemaleEthnic.SetCellValue(startrowClass, 4 + n, SL);//E
                                    sheet_FemaleEthnic.SetCellValue(startrowClass, 5 + n, "=IF(C" + startrowClass + ">0" + "," + "ROUND(" + UtilsBusiness.GetExcelColumnName(4 + n) + startrowClass + "/" + "C" + startrowClass + "*100,2),0)");
                                }
                                else
                                {
                                    int SL = listMarkBySubjectEdu.Where(o => o >= slc.MinValue).Count()
                                        + listTnbsBySubjectEdu.Where(o => o >= slc.MinValue).Count();
                                    sheet_FemaleEthnic.SetCellValue(startrowClass, 4 + n, SL);//E
                                    sheet_FemaleEthnic.SetCellValue(startrowClass, 5 + n, "=IF(C" + startrowClass + ">0" + "," + "ROUND(" + UtilsBusiness.GetExcelColumnName(4 + n) + startrowClass + "/" + "C" + startrowClass + "*100,2),0)");
                                }

                            }
                            else if (slc.SignMin == SystemParamsInFile.Sign_Compare.GREATEER_THAN_SIGN)
                            {
                                if (slc.SignMax == SystemParamsInFile.Sign_Compare.SMALLER_OR_EQUAL_SIGN)
                                {
                                    int SL = listMarkBySubjectEdu.Where(o => o > slc.MinValue && o <= slc.MaxValue).Count()
                                        + listTnbsBySubjectEdu.Where(o => o > slc.MinValue && o <= slc.MaxValue).Count();
                                    sheet_FemaleEthnic.SetCellValue(startrowClass, 4 + n, SL);//E
                                    sheet_FemaleEthnic.SetCellValue(startrowClass, 5 + n, "=IF(C" + startrowClass + ">0" + "," + "ROUND(" + UtilsBusiness.GetExcelColumnName(4 + n) + startrowClass + "/" + "C" + startrowClass + "*100,2),0)");
                                }
                                else if (slc.SignMax == SystemParamsInFile.Sign_Compare.SMALLER_SIGN)
                                {
                                    int SL = listMarkBySubjectEdu.Where(o => o > slc.MinValue && o < slc.MaxValue).Count()
                                        + listTnbsBySubjectEdu.Where(o => o > slc.MinValue && o < slc.MaxValue).Count();
                                    sheet_FemaleEthnic.SetCellValue(startrowClass, 4 + n, SL);//E
                                    sheet_FemaleEthnic.SetCellValue(startrowClass, 5 + n, "=IF(C" + startrowClass + ">0" + "," + "ROUND(" + UtilsBusiness.GetExcelColumnName(4 + n) + startrowClass + "/" + "C" + startrowClass + "*100,2),0)");
                                }
                                else if (slc.SignMax == SystemParamsInFile.Sign_Compare.EQUAL_SIGN)
                                {
                                    int SL = listMarkBySubjectEdu.Where(o => o > slc.MinValue && o == slc.MaxValue).Count()
                                        + listTnbsBySubjectEdu.Where(o => o > slc.MinValue && o == slc.MaxValue).Count();
                                    sheet_FemaleEthnic.SetCellValue(startrowClass, 4 + n, SL);//E
                                    sheet_FemaleEthnic.SetCellValue(startrowClass, 5 + n, "=IF(C" + startrowClass + ">0" + "," + "ROUND(" + UtilsBusiness.GetExcelColumnName(4 + n) + startrowClass + "/" + "C" + startrowClass + "*100,2),0)");
                                }
                                else
                                {
                                    int SL = listMarkBySubjectEdu.Where(o => o > slc.MinValue).Count()
                                        + listTnbsBySubjectEdu.Where(o => o > slc.MinValue).Count();
                                    sheet_FemaleEthnic.SetCellValue(startrowClass, 4 + n, SL);//E
                                    sheet_FemaleEthnic.SetCellValue(startrowClass, 5 + n, "=IF(C" + startrowClass + ">0" + "," + "ROUND(" + UtilsBusiness.GetExcelColumnName(4 + n) + startrowClass + "/" + "C" + startrowClass + "*100,2),0)");
                                }

                            }
                            else
                            {
                                if (slc.SignMax == SystemParamsInFile.Sign_Compare.SMALLER_OR_EQUAL_SIGN)
                                {
                                    int SL = listMarkBySubjectEdu.Where(o => o <= slc.MaxValue).Count()
                                        + listTnbsBySubjectEdu.Where(o => o <= slc.MaxValue).Count();
                                    sheet_FemaleEthnic.SetCellValue(startrowClass, 4 + n, SL);//E
                                    sheet_FemaleEthnic.SetCellValue(startrowClass, 5 + n, "=IF(C" + startrowClass + ">0" + "," + "ROUND(" + UtilsBusiness.GetExcelColumnName(4 + n) + startrowClass + "/" + "C" + startrowClass + "*100,2),0)");
                                }
                                else if (slc.SignMax == SystemParamsInFile.Sign_Compare.SMALLER_SIGN)
                                {
                                    int SL = listMarkBySubjectEdu.Where(o => o < slc.MaxValue).Count()
                                        + listTnbsBySubjectEdu.Where(o => o < slc.MaxValue).Count();
                                    sheet_FemaleEthnic.SetCellValue(startrowClass, 4 + n, SL);//E
                                    sheet_FemaleEthnic.SetCellValue(startrowClass, 5 + n, "=IF(C" + startrowClass + ">0" + "," + "ROUND(" + UtilsBusiness.GetExcelColumnName(4 + n) + startrowClass + "/" + "C" + startrowClass + "*100,2),0)");
                                }
                                else if (slc.SignMax == SystemParamsInFile.Sign_Compare.EQUAL_SIGN)
                                {
                                    int SL = listMarkBySubjectEdu.Where(o => o == slc.MaxValue).Count()
                                        + listTnbsBySubjectEdu.Where(o => o == slc.MaxValue).Count();
                                    sheet_FemaleEthnic.SetCellValue(startrowClass, 4 + n, SL);//E
                                    sheet_FemaleEthnic.SetCellValue(startrowClass, 5 + n, "=IF(C" + startrowClass + ">0" + "," + "ROUND(" + UtilsBusiness.GetExcelColumnName(4 + n) + startrowClass + "/" + "C" + startrowClass + "*100,2),0)");
                                }

                            }
                            n += 2;
                        }
                        startrowClass++;
                        startrow++;
                    }
                    sheet_FemaleEthnic.CopyPasteSameRowHeigh(range, startrowSum);
                    sheet_FemaleEthnic.SetCellValue(startrowSum, 1, i + 1);
                    sheet_FemaleEthnic.SetCellValue(startrowSum, 2, lstEducationLevel[i].Resolution);
                    n = 0;
                    if (lstClass.Count > 0)
                    {
                        sheet_FemaleEthnic.SetCellValue(startrowSum, 3, "=SUM(C" + (startrowSum + 1).ToString() + ":C" + startrow.ToString() + ")");
                        for (int m = 0; m < numberConfig; m++)
                        {
                            sheet_FemaleEthnic.SetCellValue(startrowSum, 4 + n, "=SUM(" + UtilsBusiness.GetExcelColumnName(4 + n) + (startrowSum + 1).ToString() + ":" + UtilsBusiness.GetExcelColumnName(4 + n) + startrow.ToString() + ")");
                            sheet_FemaleEthnic.SetCellValue(startrowSum, 5 + n, "=IF(C" + startrowSum + ">0" + "," + "ROUND(" + UtilsBusiness.GetExcelColumnName(4 + n) + startrowSum + "/" + "C" + startrowSum + "*100,2),0)");
                            n += 2;
                        }
                    }
                    else
                    {
                        sheet_FemaleEthnic.SetCellValue(startrowSum, 3, 0);
                        for (int m = 0; m < numberConfig; m++)
                        {
                            sheet_FemaleEthnic.SetCellValue(startrowSum, 4 + n, 0);
                            sheet_FemaleEthnic.SetCellValue(startrowSum, 5 + n, 0);
                            n += 2;
                        }
                    }
                    startrow++;
                }
                if (entity.AppliedLevel == SystemParamsInFile.APPLIED_LEVEL_SECONDARY)
                {
                    sheet_FemaleEthnic.GetRange(9, lastColumn, 14 + lstClassAllEdu.Count(), lastColumn).SetBorder(VTBorderStyle.Solid, VTBorderWeight.Medium, VTBorderIndex.EdgeRight);
                }
                else
                {
                    sheet_FemaleEthnic.GetRange(9, lastColumn, 13 + lstClassAllEdu.Count(), lastColumn).SetBorder(VTBorderStyle.Solid, VTBorderWeight.Medium, VTBorderIndex.EdgeRight);
                }
                sheet_FemaleEthnic.FitAllColumnsOnOnePage = true;
                sheet_FemaleEthnic.SetCellValue(UtilsBusiness.GetExcelColumnName(1) + startrow, "(*): Tổng số học sinh của lớp không bao gồm các học sinh được miễn giảm môn tương ứng");
                sheet_FemaleEthnic.SetFontName("Times New Roman", 0);
                #endregion
            }
            //Xoá sheet template
            firstSheet.Delete();
            return oBook.ToStream();
        }
        #endregion

        #region Tạo file báo cáo thông tin bảng điểm học sinh theo kỳ - Môn nhận xét
        /// <summary>
        /// Tạo file báo cáo thông tin bảng điểm học sinh theo kỳ - mon nhan xet
        /// </summary>
        /// <param name="entity"></param>
        /// <returns>tungnd</returns>
        public Stream CreateJudgeOfSemesterBySubject(MarkOfSemesterBySubjectBO entity)
        {
            string reportCode = SystemParamsInFile.REPORT_TK_DIEMTHI_HK_THEOMON_NHANXET;
            ReportDefinition reportDef = ReportDefinitionBusiness.GetByCode(reportCode);
            SchoolProfile school = this.SchoolProfileBusiness.Find(entity.SchoolID);
            string SchoolName = school.SchoolName.ToUpper();
            string AcademicYear = this.AcademicYearBusiness.Find(entity.AcademicYearID).DisplayTitle;
            string Suppervising = this.SchoolProfileBusiness.Find(entity.SchoolID).SupervisingDept.SupervisingDeptName.ToUpper();
            string Subject = this.SubjectCatBusiness.Find(entity.SubjectID).DisplayName.ToUpper();
            string SemesterName = "";

            if (entity.Semester == SystemParamsInFile.SEMESTER_OF_YEAR_FIRST)
                SemesterName = "HỌC KỲ I";
            if (entity.Semester == SystemParamsInFile.SEMESTER_OF_YEAR_SECOND)
                SemesterName = "HỌC KỲ II";
            if (entity.Semester == SystemParamsInFile.SEMESTER_OF_YEAR_ALL)
                SemesterName = "CẢ NĂM";

            string templatePath = SystemParamsInFile.TEMPLATE_FOLDER + reportDef.TemplateDirectory + "/" + reportDef.TemplateName;
            IVTWorkbook oBook = VTExport.OpenWorkbook(templatePath);

            // Lấy danh sách khối học

            List<EducationLevel> lstEducationLevel = this.EducationLevelBusiness.GetByGrade(entity.AppliedLevel).ToList();
            AcademicYear Aca = AcademicYearBusiness.Find(entity.AcademicYearID);
            // Lấy danh sách điểm MarkRecord
            int MarkTypeID = this.MarkTypeBusiness.All.Where(o => o.AppliedLevel == entity.AppliedLevel && o.Title == "HK").SingleOrDefault().MarkTypeID;

            IDictionary<string, object> Dic_MarkRecord = new Dictionary<string, object> {
                                                                {"AcademicYearID", entity.AcademicYearID},
                                                                {"SubjectID", entity.SubjectID},
                                                                {"Semester", entity.Semester},
                                                                {"MarkTypeID", MarkTypeID}
                                                            };

            IQueryable<JudgeRecord> lstQJudge = this.JudgeRecordBusiness
                                                            .SearchBySchool(entity.SchoolID, Dic_MarkRecord);

            Dictionary<string, object> dicPupilRanking = new Dictionary<string, object>
            {
                {"AcademicYearID", entity.AcademicYearID},
                {"SchoolID", entity.SchoolID},
                {"Semester", entity.Semester},
                {"AppliedLevel", entity.AppliedLevel}
            };
            IQueryable<PupilOfClassBO> lstQPoc = from p in PupilOfClassBusiness.SearchBySchool(entity.SchoolID, dicPupilRanking).AddCriteriaSemester(Aca, entity.Semester)
                                                 join q in PupilProfileBusiness.All on p.PupilID equals q.PupilProfileID
                                                 select new PupilOfClassBO
                                                 {
                                                     PupilID = p.PupilID,
                                                     ClassID = p.ClassID,
                                                     Genre = q.Genre,
                                                     EthnicID = q.EthnicID
                                                 };
            //Học sinh dân tộc 
            //Dân tộc kinh
            Ethnic Ethnic_Kinh = EthnicBusiness.All.Where(o => o.EthnicName.ToLower().Contains("kinh") && o.IsActive == true).FirstOrDefault();
            int Ethnic_IDKinh = 0;
            if (Ethnic_Kinh != null)
            {
                Ethnic_IDKinh = Ethnic_Kinh.EthnicID;
            }
            Ethnic Ethnic_NN = EthnicBusiness.All.Where(o => o.EthnicName.ToLower().Contains("nước ngoài") && o.IsActive == true).FirstOrDefault();
            int Ethnic_IDNN = 0;
            if (Ethnic_NN != null)
            {
                Ethnic_IDNN = Ethnic_NN.EthnicID;
            }
            var listCountPupilByClass = lstQPoc.GroupBy(o => o.ClassID).Select(o => new { ClassID = o.Key, TotalPupil = o.Count() }).ToList();
            var listCountPupilByClass_Female = lstQPoc.Where(o => o.Genre == SystemParamsInFile.GENRE_FEMALE).GroupBy(o => o.ClassID).Select(o => new { ClassID = o.Key, TotalPupil = o.Count() });
            var listCountPupilByClass_Ethnic = lstQPoc.Where(o => o.EthnicID.HasValue && o.EthnicID != Ethnic_IDKinh && o.EthnicID != Ethnic_IDNN).GroupBy(o => o.ClassID).Select(o => new { ClassID = o.Key, TotalPupil = o.Count() }).ToList();

            //Chi lay cac diem thi cua hoc sinh dang hoc trong ky
            var lstJudgeRecordBySubject = (from u in lstQJudge
                                           join q in PupilProfileBusiness.All on u.PupilID equals q.PupilProfileID
                                           join r in ClassProfileBusiness.All on u.ClassID equals r.ClassProfileID
                                           where lstQPoc.Any(o => o.PupilID == u.PupilID && o.ClassID == u.ClassID)
                                           && r.IsActive.Value
                                           select new
                                           {
                                               PupilID = u.PupilID,
                                               ClassID = u.ClassID,
                                               Judgement = u.Judgement,
                                               Genre = q.Genre,
                                               EthnicID = q.EthnicID,
                                               EducationLevelID = r.EducationLevelID
                                           }).ToList();
            var lstJudgeRecordBySubject_Female = lstJudgeRecordBySubject.Where(o => o.Genre == SystemParamsInFile.GENRE_FEMALE).ToList();

            var lstJudgeRecordBySubject_Ethnic = lstJudgeRecordBySubject.Where(o => o.EthnicID.HasValue && o.EthnicID != Ethnic_IDKinh && o.EthnicID != Ethnic_IDNN).ToList();


            int startrow = 14;
            int schooltotalPupil = 0;
            int schooltotalPupilD = 0;
            int schooltotalPupilCD = 0;
            IDictionary<string, object> Dic_ClassProfile = new Dictionary<string, object> {
                                                                    {"AcademicYearID", entity.AcademicYearID},
                                                                    {"SchoolID", entity.SchoolID},
                                                                    {"SubjectID", entity.SubjectID}
                                                                };
            var lstClassAllEdu = (from p in this.ClassSubjectBusiness.SearchBySchool(entity.SchoolID, Dic_ClassProfile)
                                  join q in ClassProfileBusiness.All on p.ClassID equals q.ClassProfileID
                                  where q.IsActive.Value
                                  select new
                                  {
                                      ClassID = q.ClassProfileID,
                                      EducationLevelID = q.EducationLevelID,
                                      OrderNumber = q.OrderNumber,
                                      DisplayName = q.DisplayName,
                                      SubjectID = p.SubjectID
                                  }).ToList();
            lstClassAllEdu = lstClassAllEdu.OrderBy(o => o.OrderNumber.HasValue ? o.OrderNumber : 0).ThenBy(o => o.DisplayName).ToList();

            //Lấy sheet template
            IVTWorksheet firstSheet = oBook.GetSheet(1);
            IVTWorksheet secondSheet = oBook.GetSheet(2);
            IVTWorksheet threeSheet = oBook.GetSheet(3);
            IVTRange range = firstSheet.GetRange("A11", "M11");
            IVTRange rangeClass = firstSheet.GetRange("A15", "M15");
            IVTRange rangeLastClass = firstSheet.GetRange("A16", "M16");
            IVTRange range_Female = secondSheet.GetRange("A11", "M11");
            IVTRange rangeClass_Female = secondSheet.GetRange("A15", "M15");
            IVTRange rangeLastClass_Female = secondSheet.GetRange("A16", "M16");
            IVTRange range_Ethnic = threeSheet.GetRange("A11", "M11");
            IVTRange rangeClass_Ethnic = threeSheet.GetRange("A15", "M15");
            IVTRange rangeLastClass_Ethnic = threeSheet.GetRange("A16", "M16");

            //Tạo sheet
            IVTWorksheet sheet = oBook.CopySheetToLast(firstSheet);
            IVTWorksheet sheet_Female = oBook.CopySheetToLast(secondSheet);
            IVTWorksheet sheet_Ethnic = oBook.CopySheetToLast(threeSheet);
            #region Tạo sheet điểm thi học kỳ môn nhận xét.
            //Fill dữ liệu chung
            sheet.Name = "DiemThiHK";
            sheet.SetCellValue("A2", Suppervising);
            sheet.SetCellValue("A3", SchoolName);
            sheet.SetCellValue("A7", "THỐNG KÊ ĐIỂM THI HỌC KỲ");
            sheet.SetCellValue("A8", "MÔN " + Subject.ToUpper() + " - " + SemesterName + " - NĂM HỌC: " + AcademicYear);
            sheet.SetCellValue("G5", (school.Province != null ? school.Province.ProvinceName + ", n" : "N") + "gày " + DateTime.Now.Day + " tháng " + DateTime.Now.Month + " năm " + DateTime.Now.Year);
            for (int i = 0; i < lstEducationLevel.Count; i++)
            {
                int sumD = 0;
                int sumCD = 0;
                int totalPupil = 0;
                int startrowSum = startrow;
                var lstClass = lstClassAllEdu.Where(o => o.EducationLevelID == lstEducationLevel[i].EducationLevelID).OrderBy(o => o.OrderNumber.HasValue ? o.OrderNumber : 0).ThenBy(o => o.DisplayName).ToList();
                var lstJudgeRecordBySubject_Edu = lstJudgeRecordBySubject.Where(o => o.EducationLevelID == lstEducationLevel[i].EducationLevelID).ToList();
                for (int j = 0; j < lstClass.Count; j++)
                {
                    int classID = lstClass[j].ClassID;
                    var lstJudgeOfClass = lstJudgeRecordBySubject_Edu.Where(u => u.ClassID == classID);
                    // Số lượng học sinh có điểm thi đạt
                    int HSD = lstJudgeOfClass.Where(o => o.Judgement.Equals(SystemParamsInFile.JUDGE_MARK_D)).Count();
                    sumD += HSD;

                    // Những học sinh có điểm thi chưa đạt
                    int HSCD = lstJudgeOfClass.Where(o => o.Judgement.Equals(SystemParamsInFile.JUDGE_MARK_CD)).Count();
                    sumCD += HSCD;

                    //sỹ số học sinh trong lớp là số lượng học sinh ở trạng thái đang học hoặc đã tốt nghiệp
                    var objCountPupil = listCountPupilByClass.Where(o => o.ClassID == classID).FirstOrDefault();
                    int SumPupil = objCountPupil != null ? objCountPupil.TotalPupil : 0;
                    totalPupil += SumPupil;
                    // fill vào excel
                    int startrowClass = startrow + 1;
                    if (j == lstClass.Count - 1)
                        sheet.CopyPasteSameRowHeigh(rangeLastClass, startrowClass);
                    else
                        sheet.CopyPasteSameRowHeigh(rangeClass, startrowClass);

                    sheet.SetCellValue(startrowClass, 1, j + 1);
                    sheet.SetCellValue(startrowClass, 2, lstClass[j].DisplayName);
                    sheet.SetCellValue(startrowClass, 3, SumPupil);
                    sheet.SetCellValue(startrowClass, 4, HSD);
                    sheet.SetCellValue(startrowClass, 6, HSCD);
                    startrow++;
                }
                schooltotalPupil += totalPupil;
                schooltotalPupilD += sumD;
                schooltotalPupilCD += sumCD;
                // Row thong ke theo khoi
                sheet.CopyPasteSameRowHeigh(range, startrowSum);
                sheet.SetCellValue(startrowSum, 1, i + 1);
                sheet.SetCellValue(startrowSum, 2, lstEducationLevel[i].Resolution);
                sheet.SetCellValue(startrowSum, 3, totalPupil);
                sheet.SetCellValue(startrowSum, 4, sumD);
                sheet.SetCellValue(startrowSum, 6, sumCD);

                startrow++;
            }
            // fill vào excel dữ liệu row tổng của trường
            sheet.SetCellValue(12, 1, "Toàn trường");
            sheet.SetCellValue(12, 3, schooltotalPupil);
            sheet.SetCellValue(12, 4, schooltotalPupilD);
            sheet.SetCellValue(12, 6, schooltotalPupilCD);
            //Xoá sheet template
            firstSheet.Delete();
            #endregion
            #region Tạo báo cáo điểm thi học kỳ học sinh nữ
            //Fill dữ liệu chung
            sheet_Female.Name = "HS_Nu";
            sheet_Female.SetCellValue("A2", Suppervising);
            sheet_Female.SetCellValue("A3", SchoolName);
            sheet_Female.SetCellValue("A7", "THỐNG KÊ ĐIỂM THI HỌC KỲ HỌC SINH NỮ");
            sheet_Female.SetCellValue("A8", "MÔN " + Subject.ToUpper() + " - " + SemesterName + " - NĂM HỌC: " + AcademicYear);
            sheet_Female.SetCellValue("G5", (school.Province != null ? school.Province.ProvinceName + ", n" : "N") + "gày " + DateTime.Now.Day + " tháng " + DateTime.Now.Month + " năm " + DateTime.Now.Year);
            int startrow_Female = 14;
            for (int i = 0; i < lstEducationLevel.Count; i++)
            {
                int sumD = 0;
                int sumCD = 0;
                int totalPupil = 0;
                int startrowSum = startrow_Female;
                var lstClass = lstClassAllEdu.Where(o => o.EducationLevelID == lstEducationLevel[i].EducationLevelID).OrderBy(o => o.OrderNumber.HasValue ? o.OrderNumber : 0).ThenBy(o => o.DisplayName).ToList();
                var lstJudgeRecordBySubject_Edu = lstJudgeRecordBySubject_Female.Where(o => o.EducationLevelID == lstEducationLevel[i].EducationLevelID).ToList();
                for (int j = 0; j < lstClass.Count; j++)
                {
                    int classID = lstClass[j].ClassID;
                    var lstJudgeOfClass = lstJudgeRecordBySubject_Edu.Where(u => u.ClassID == classID);
                    // Số lượng học sinh có điểm thi đạt
                    int HSD = lstJudgeOfClass.Where(o => o.Judgement.Equals(SystemParamsInFile.JUDGE_MARK_D)).Count();
                    sumD += HSD;

                    // Những học sinh có điểm thi chưa đạt
                    int HSCD = lstJudgeOfClass.Where(o => o.Judgement.Equals(SystemParamsInFile.JUDGE_MARK_CD)).Count();
                    sumCD += HSCD;

                    //sỹ số học sinh trong lớp là số lượng học sinh ở trạng thái đang học hoặc đã tốt nghiệp
                    var objCountPupil = listCountPupilByClass_Female.Where(o => o.ClassID == classID).FirstOrDefault();
                    int SumPupil = objCountPupil != null ? objCountPupil.TotalPupil : 0;
                    totalPupil += SumPupil;
                    // fill vào excel
                    int startrowClass = startrow_Female + 1;
                    if (j == lstClass.Count - 1)
                        sheet_Female.CopyPasteSameRowHeigh(rangeLastClass_Female, startrowClass);
                    else
                        sheet_Female.CopyPasteSameRowHeigh(rangeClass_Female, startrowClass);

                    sheet_Female.SetCellValue(startrowClass, 1, j + 1);
                    sheet_Female.SetCellValue(startrowClass, 2, lstClass[j].DisplayName);
                    sheet_Female.SetCellValue(startrowClass, 3, SumPupil);
                    sheet_Female.SetCellValue(startrowClass, 4, HSD);
                    sheet_Female.SetCellValue(startrowClass, 6, HSCD);
                    startrow_Female++;
                }
                schooltotalPupil += totalPupil;
                schooltotalPupilD += sumD;
                schooltotalPupilCD += sumCD;
                // Row thong ke theo khoi
                sheet_Female.CopyPasteSameRowHeigh(range_Female, startrowSum);
                sheet_Female.SetCellValue(startrowSum, 1, i + 1);
                sheet_Female.SetCellValue(startrowSum, 2, lstEducationLevel[i].Resolution);
                sheet_Female.SetCellValue(startrowSum, 3, totalPupil);
                sheet_Female.SetCellValue(startrowSum, 4, sumD);
                sheet_Female.SetCellValue(startrowSum, 6, sumCD);

                startrow_Female++;
            }
            // fill vào excel dữ liệu row tổng của trường
            sheet_Female.SetCellValue(12, 1, "Toàn trường");
            sheet_Female.SetCellValue(12, 3, schooltotalPupil);
            sheet_Female.SetCellValue(12, 4, schooltotalPupilD);
            sheet_Female.SetCellValue(12, 6, schooltotalPupilCD);
            //Xoá sheet template
            secondSheet.Delete();
            #endregion 
            #region Tạo báo cáo điểm thi học kỳ học sinh dân tộc
            //Fill dữ liệu chung
            sheet_Ethnic.Name = "HS_DT";
            sheet_Ethnic.SetCellValue("A2", Suppervising);
            sheet_Ethnic.SetCellValue("A3", SchoolName);
            sheet_Ethnic.SetCellValue("A7", "THỐNG KÊ ĐIỂM THI HỌC KỲ HỌC SINH DÂN TỘC");
            sheet_Ethnic.SetCellValue("A8", "MÔN " + Subject.ToUpper() + " - " + SemesterName + " - NĂM HỌC: " + AcademicYear);
            sheet_Ethnic.SetCellValue("G5", (school.Province != null ? school.Province.ProvinceName + ", n" : "N") + "gày " + DateTime.Now.Day + " tháng " + DateTime.Now.Month + " năm " + DateTime.Now.Year);
            int startrow_Ethnic = 14;
            for (int i = 0; i < lstEducationLevel.Count; i++)
            {
                int sumD = 0;
                int sumCD = 0;
                int totalPupil = 0;
                int startrowSum = startrow_Ethnic;
                var lstClass = lstClassAllEdu.Where(o => o.EducationLevelID == lstEducationLevel[i].EducationLevelID).OrderBy(o => o.OrderNumber.HasValue ? o.OrderNumber : 0).ThenBy(o => o.DisplayName).ToList();
                var lstJudgeRecordBySubject_Edu = lstJudgeRecordBySubject_Ethnic.Where(o => o.EducationLevelID == lstEducationLevel[i].EducationLevelID).ToList();
                for (int j = 0; j < lstClass.Count; j++)
                {
                    int classID = lstClass[j].ClassID;
                    var lstJudgeOfClass = lstJudgeRecordBySubject_Edu.Where(u => u.ClassID == classID);
                    // Số lượng học sinh có điểm thi đạt
                    int HSD = lstJudgeOfClass.Where(o => o.Judgement.Equals(SystemParamsInFile.JUDGE_MARK_D)).Count();
                    sumD += HSD;

                    // Những học sinh có điểm thi chưa đạt
                    int HSCD = lstJudgeOfClass.Where(o => o.Judgement.Equals(SystemParamsInFile.JUDGE_MARK_CD)).Count();
                    sumCD += HSCD;

                    //sỹ số học sinh trong lớp là số lượng học sinh ở trạng thái đang học hoặc đã tốt nghiệp
                    var objCountPupil = listCountPupilByClass_Ethnic.Where(o => o.ClassID == classID).FirstOrDefault();
                    int SumPupil = objCountPupil != null ? objCountPupil.TotalPupil : 0;
                    totalPupil += SumPupil;
                    // fill vào excel
                    int startrowClass = startrow_Ethnic + 1;
                    if (j == lstClass.Count - 1)
                        sheet_Ethnic.CopyPasteSameRowHeigh(rangeLastClass_Ethnic, startrowClass);
                    else
                        sheet_Ethnic.CopyPasteSameRowHeigh(rangeClass_Ethnic, startrowClass);

                    sheet_Ethnic.SetCellValue(startrowClass, 1, j + 1);
                    sheet_Ethnic.SetCellValue(startrowClass, 2, lstClass[j].DisplayName);
                    sheet_Ethnic.SetCellValue(startrowClass, 3, SumPupil);
                    sheet_Ethnic.SetCellValue(startrowClass, 4, HSD);
                    sheet_Ethnic.SetCellValue(startrowClass, 6, HSCD);
                    startrow_Ethnic++;
                }
                schooltotalPupil += totalPupil;
                schooltotalPupilD += sumD;
                schooltotalPupilCD += sumCD;
                // Row thong ke theo khoi
                sheet_Ethnic.CopyPasteSameRowHeigh(range_Ethnic, startrowSum);
                sheet_Ethnic.SetCellValue(startrowSum, 1, i + 1);
                sheet_Ethnic.SetCellValue(startrowSum, 2, lstEducationLevel[i].Resolution);
                sheet_Ethnic.SetCellValue(startrowSum, 3, totalPupil);
                sheet_Ethnic.SetCellValue(startrowSum, 4, sumD);
                sheet_Ethnic.SetCellValue(startrowSum, 6, sumCD);

                startrow_Ethnic++;
            }
            // fill vào excel dữ liệu row tổng của trường
            sheet_Ethnic.SetCellValue(12, 1, "Toàn trường");
            sheet_Ethnic.SetCellValue(12, 3, schooltotalPupil);
            sheet_Ethnic.SetCellValue(12, 4, schooltotalPupilD);
            sheet_Ethnic.SetCellValue(12, 6, schooltotalPupilCD);
            //Xoá sheet template
            threeSheet.Delete();
            #endregion 
            return oBook.ToStream();
        }
        #endregion
    }
}
