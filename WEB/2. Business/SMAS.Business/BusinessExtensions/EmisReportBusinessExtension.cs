using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SMAS.Models.Models;
using SMAS.Business.IBusiness;
using log4net;
using SMAS.DAL.IRepository;
using SMAS.DAL.Repository;
using SMAS.Business.Common;
using System.Data.Objects;
using System.IO;
using SMAS.VTUtils.Excel.Export;
using System.Data.Entity.Infrastructure;

namespace SMAS.Business.Business
{
    /// <summary>
    /// Bao cao EMIS
    /// </summary>
    /// <author>AuNH</author>
    /// <date>12/21/2012</date>
    public partial class EmisReportBusiness : GenericBussiness<ProcessedCellData>, IEmisReportBusiness
    {
        IProcessedReportRepository ProcessedReportRepository;
        IProcessedCellDataRepository ProcessedCellDataRepository;

        /// <summary>
        /// Initializes a new instance of the <see cref="EmisReportBusiness" /> class.
        /// </summary>
        /// <param name="logger">The logger.</param>
        /// <param name="context">The context.</param>
        /// <author>AuNH</author>
        /// <date>12/21/2012</date>
        public EmisReportBusiness(ILog logger, SMASEntities context = null) : base(logger)
        {
            if (context == null) { context = new SMASEntities(); } this.context = context;
            this.repository = new ProcessedCellDataRepository(this.context);
            this.ProcessedReportRepository = new ProcessedReportRepository(this.context);
            this.ProcessedCellDataRepository = new ProcessedCellDataRepository(this.context);
        }


        /// <summary>
        /// GetHashKeyForReportSchoolInfo
        /// </summary>
        /// <param name="Entity">The entity.</param>
        /// <returns>
        /// String
        /// </returns>
        /// <date>12/21/2012</date>
        /// <author>AuNH</author>
        /// <date>12/21/2012</date>
        public string GetHashKeyForReportSchoolInfo(IDictionary<string, object> Entity)
        {
            return ReportUtils.GetHashKey(Entity);
        }

        /// <summary>
        /// GetStatusSentToSupervisor
        /// </summary>
        /// <param name="ReportCode">The report code.</param>
        /// <param name="InputParameterHashKey">The input parameter hash key.</param>
        /// <returns>
        /// Byte
        /// </returns>
        /// <date>12/21/2012</date>
        /// <author>AuNH</author>
        /// <date>12/21/2012</date>
        public int GetStatusSentToSupervisor(string ReportCode, string InputParameterHashKey)
        {
            ProcessedReport ProcessedReport = ProcessedReportBusiness.GetProcessedReport(ReportCode, InputParameterHashKey);
            if (ProcessedReport == null || ProcessedReport.SentToSupervisor == null)
            {
                return 0;
            }
            return ProcessedReport.SentToSupervisor.Value ? (int)1 : (int)0;
        }

        /// <summary>
        /// InsertProcessedReportSchoolInfo
        /// </summary>
        /// <param name="ReportCode">The report code.</param>
        /// <param name="SchoolID">The school ID.</param>
        /// <param name="AcademicYearID">The academic year ID.</param>
        /// <param name="ProcName">Name of the proc.</param>
        /// <param name="ProcParams">The proc params.</param>
        /// <returns>
        /// ProcessedReport
        /// </returns>
        /// <date>12/21/2012</date>
        /// <author>AuNH</author>
        /// <date>12/21/2012</date>
        public ProcessedReport InsertProcessedReportSchoolInfo(string ReportCode, int SchoolID, int AcademicYearID, string ProcName, IDictionary<string, object> ProcParams)
        {
            IDictionary<string, object> dic = new Dictionary<string, object>();
            dic["SchoolID"] = SchoolID;
            dic["AcademicYearID"] = AcademicYearID;

            string InputParameterHashKey = GetHashKeyForReportSchoolInfo(dic);

            ProcessedReport ProcessedReport = new ProcessedReport();
            ProcessedReport.ReportCode = ReportCode;
            ProcessedReport.ProcessedDate = DateTime.Now;
            ProcessedReport.InputParameterHashKey = InputParameterHashKey;

            //Tạo tên file
            ReportDefinition reportDef = ReportDefinitionBusiness.GetByCode(ProcessedReport.ReportCode);
            string outputNamePattern = reportDef.OutputNamePattern;
            ProcessedReport.ReportName = ReportUtils.RemoveSpecialCharacters(outputNamePattern) + "." + reportDef.OutputFormat;

            ProcessedReportRepository.Insert(ProcessedReport);
            ProcessedReportRepository.Save();

            //call store procedure
            ExecuteStoreProcedure(ProcName, ProcParams);
            return ProcessedReport;
        }


        /// <summary>
        /// CreatedEmisReport
        /// </summary>
        /// <param name="Entity">The entity.</param>
        /// <param name="SchoolID">The school ID.</param>
        /// <param name="AcademicYearID">The academic year ID.</param>
        /// <param name="workBookData">The work book data.</param>
        /// <returns>
        /// Stream
        /// </returns>
        /// <date>12/21/2012</date>
        /// <author>AuNH</author>
        /// <date>12/21/2012</date>
        public Stream CreatedEmisReport(ProcessedReport Entity, int SchoolID, int AcademicYearID, WorkBookData workBookData)
        {
            ReportDefinition reportDef = ReportDefinitionBusiness.GetByCode(Entity.ReportCode);
            string templatePath = ReportUtils.GetTemplatePath(reportDef);
            IVTWorkbook oBook = VTExport.OpenWorkbook(templatePath);

            string filename = Path.GetFileName(templatePath);
            string tempFilename = filename.Replace(".", "_Template.");
            string tempPath = templatePath.Replace(filename, tempFilename);

            IVTWorkbook tempBook = VTExport.OpenWorkbook(tempPath);

            IQueryable<ProcessedCellData> query = ProcessedCellDataRepository.All.Where(o => o.ProcessedReportID == Entity.ProcessedReportID);

            List<ProcessedCellData> ListData = query.ToList();

            // TODO: AuNH UnComment all comments below here when application completely switch to oracle database

            int num = tempBook.GetSheets().Count;
            IVTWorksheet Sheet = null;
            IVTWorksheet tSheet = null;
            SheetData SheetData = null;
            //List<ProcessedCellData> ListDataBySheet = null;
            for (int i = 1; i <= num; i++)
            {
                Sheet = oBook.GetSheet(i);
                tSheet = tempBook.GetSheet(i);

                SheetData = workBookData.GetSheet(Sheet.Name);

                //ListDataBySheet = ListData.Where(o => o.SheetName == Sheet.Name);


                if (SheetData == null)
                {
                    //foreach (ProcessedCellData data in ListDataBySheet)
                    //{
                    //    workBookData.Data[data.CellCode] = data.CellValue;
                    //}
                    tSheet.FillVariableValue(workBookData.Data, Sheet);
                }
                else
                {
                    //foreach (ProcessedCellData data in ListDataBySheet)
                    //{
                    //    SheetData.Data[data.CellCode] = data.CellValue;
                    //}
                    tSheet.FillVariableValue(SheetData.Data, Sheet);
                }
            }

            Stream excel = oBook.ToStream();

            Entity.ReportData = ReportUtils.Compress(excel);
            ProcessedReportBusiness.Update(Entity);
            excel.Position = 0;

            return excel;
        }


        /// <summary>
        /// Thuc hien goi mot thu tuc trong CSDL
        /// </summary>
        /// <param name="ProcName">Name of the procedure.</param>
        /// <param name="Params">The params of procedure.</param>
        /// <returns>
        /// Int32
        /// </returns>
        /// <author>AuNH</author>
        /// <date>12/21/2012</date>
        private int ExecuteStoreProcedure(string ProcName, IDictionary<string, object> Params)
        {
            ObjectParameter[] oParams = new ObjectParameter[Params.Count];
            int i = 0;
            foreach (string key in Params.Keys)
            {
                oParams[i++] = new ObjectParameter(key, Params[key]);
            }

            return ((IObjectContextAdapter)this.context).ObjectContext.ExecuteFunction(ProcName, oParams);
        }
    }
}
