/**
 * The Viettel License
 *
 * Copyright 2012 Viettel Telecom. All rights reserved.
 * VIETTEL PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */

/** 
* @author  
* @version $Revision: $
*/

using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Linq.Expressions;
using System.Web;
using log4net;

using SMAS.DAL.Repository;
using SMAS.DAL.IRepository;
using SMAS.Business.IBusiness;
using SMAS.Models.Models;
using SMAS.Business.Common;

namespace SMAS.Business.Business
{
    public partial class SalaryLevelBusiness
    {

        /// <summary>
        /// Tìm kiếm bậc lương
        /// </summary>
        /// <author>hath</author>
        /// <date>4/9/2012</date>
        /// <param name="Resolution">Diễn giải bậc lương</param>
        /// <param name="Description">Mô tả</param>
        /// <param name="SubLevel">Phân bậc</param>
        /// <param name="ScaleID">Thang bậc ngạch lương</param>
        /// <param name="IsActive">Biến kích hoạt</param>
        /// <returns>IQueryable<SalaryLevel></returns>
        public IQueryable<SalaryLevel> Search(IDictionary<string, object> dic)
        {
            string Resolution = Utils.GetString(dic, "Resolution");
            string Description = Utils.GetString(dic, "Description");
            int SubLevel = (int)Utils.GetInt(dic, "SubLevel");
            int ScaleID = Utils.GetShort(dic, "ScaleID");
            bool? IsActive = Utils.GetIsActive(dic, "IsActive");

            IQueryable<SalaryLevel> lsSalaryLevel = SalaryLevelRepository.All;


            if (IsActive.HasValue)
                lsSalaryLevel = lsSalaryLevel.Where(sal => sal.IsActive == IsActive);
            if (Resolution.Trim().Length != 0)
                lsSalaryLevel = lsSalaryLevel.Where(sal => sal.Resolution.Contains(Resolution.ToLower()));
            if (Description.Trim().Length != 0)
                lsSalaryLevel = lsSalaryLevel.Where(sal => sal.Description.Contains(Description.ToLower()));
            if (SubLevel != 0)
                lsSalaryLevel = lsSalaryLevel.Where(sal => sal.SubLevel == SubLevel);
            if (ScaleID != 0)
                lsSalaryLevel = lsSalaryLevel.Where(sal => sal.ScaleID == ScaleID);

            return lsSalaryLevel;

        }

        /// <summary>
        /// Xóa bậc lương
        /// </summary>
        /// <author>hath</author>
        /// <date>4/9/2012</date>
        /// <param name="SalaryLevelID">ID bậc lương</param>
        public void Delete(int SalaryLevelID)
        {
            //Bạn chưa chọn bậc lương cần xóa hoặc bậc lương bạn chọn đã bị xóa khỏi hệ thống
            this.CheckAvailable((int)SalaryLevelID, "SalaryLevel_Label_SalaryLevelID", true);

            //Không thể xóa bậc lương đang sử dụng
            this.CheckConstraints(GlobalConstants.LIST_SCHEMA, "SalaryLevel", SalaryLevelID, "SalaryLevel_Label_SalaryLevelID");
            base.Delete(SalaryLevelID, true);
        }

        /// <summary>
        /// Thêm  bậc lương
        /// </summary>
        /// <author>hath</author>
        /// <date>4/9/2012</date>
        /// <param name="salaryLevel">Đối tượng bậc lương</param>
        /// <returns>Đối tượng bậc lương</returns>
        public override SalaryLevel Insert(SalaryLevel salaryLevel)
        {
            //Ngạch lương và bậc lương đã tồn tại - Kiểm tra ScaleID / SubLevel với IsActive=TRUE
            this.CheckDuplicateCouple(salaryLevel.ScaleID.ToString(), salaryLevel.SubLevel.ToString(), GlobalConstants.LIST_SCHEMA, "SalaryLevel", "ScaleID", "SubLevel", true, (int)salaryLevel.SalaryLevelID, "Salary_ScaleID");
            //Độ dài ghi chú không được vượt quá 400 
            Utils.ValidateMaxLength(salaryLevel.Description, 400, "Description");
            //Bậc lương phải là số nguyên dương trong khoảng 1-12 – Kiểm tra SubLevel
            Utils.ValidateRange((int)salaryLevel.SubLevel, 1, 12, "SalaryLevel_Label_SubLevel");
            //Không tồn tại ngạch lương hoặc đã bị xóa - Kiểm tra ScaleID - EmployeeScaleID trong bảng EmployeeScale với IsActive =1
            new EmployeeScaleBusiness(null).CheckAvailable(salaryLevel.ScaleID, "SalaryLevel_Label_ScaleID", true);
            //Hệ số lương phải lơn hơn 0 – Kiểm tra Coefficient
            if (salaryLevel.Coefficient != null && (int)salaryLevel.Coefficient<0)
            {
                throw new BusinessException("SalaryLevel_Label_FailedSalaryLevel");
            }
            //Utils.ValidateCompareNumber(0, (int)salaryLevel.Coefficient, "SalaryLevel_Label_Coefficient", "");

            //Insert
            return base.Insert(salaryLevel);

        }
        /// <summary>
        /// Sửa bậc lương
        /// </summary>
        /// <author>hath</author>
        /// <date>4/9/2012</date>
        /// <param name="salaryLevel">Đối tượng bậc lương</param>
        /// <returns>Đối tượng bậc lương</returns>
        public override SalaryLevel Update(SalaryLevel salaryLevel)
        {
            //Bạn chưa chọn bậc lương cần sửa hoặc bậc lương bạn chọn đã bị xóa khỏi hệ thống
            this.CheckAvailable((int)salaryLevel.SalaryLevelID, "SalaryLevel_Label_SalaryLevelID", true);
            //Ngạch lương và bậc lương đã tồn tại - Kiểm tra ScaleID / SubLevel với IsActive=TRUE
            this.CheckDuplicateCouple(salaryLevel.ScaleID.ToString(), salaryLevel.SubLevel.ToString(), GlobalConstants.LIST_SCHEMA, "SalaryLevel", "ScaleID", "SubLevel", true, (int)salaryLevel.SalaryLevelID, "Salary_ScaleID");
            //Độ dài ghi chú không được vượt quá 400 
            Utils.ValidateMaxLength(salaryLevel.Description, 400, "Description");
            //Bậc lương phải là số nguyên dương trong khoảng 1-12 – Kiểm tra SubLevel
            Utils.ValidateRange((int)salaryLevel.SubLevel, 1, 12, "SalaryLevel_Label_SubLevel");
            //Không tồn tại ngạch lương hoặc đã bị xóa - Kiểm tra ScaleID - EmployeeScaleID trong bảng EmployeeScale với IsActive =1
            new EmployeeScaleBusiness(null).CheckAvailable(salaryLevel.ScaleID, "SalaryLevel_Label_ScaleID", true);
            //Hệ số lương phải lơn hơn 0 – Kiểm tra Coefficient
            if (salaryLevel.Coefficient != null && (int)salaryLevel.Coefficient < 0)
            {
                throw new BusinessException("SalaryLevel_Label_FailedSalaryLevel");
            }
            //Utils.ValidateCompareNumber((double)salaryLevel.Coefficient, 0.0, "SalaryLevel_Label_Coefficient", "");

            return base.Update(salaryLevel);
        }
    }
}
