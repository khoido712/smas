﻿/**
 * The Viettel License
 *
 * Copyright 2012 Viettel Telecom. All rights reserved.
 * VIETTEL PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */

/** 
* @author  
* @version $Revision: $
*/

using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Linq.Expressions;
using System.Web;
using log4net;

using SMAS.DAL.Repository;
using SMAS.DAL.IRepository;
using SMAS.Business.IBusiness;
using SMAS.Models.Models;
using SMAS.Business.Common;
using System.Transactions;
using SMAS.Business.BusinessObject;
using SMAS.Business.Common.Extension;

namespace SMAS.Business.Business
{
    public partial class SupervisingDeptBusiness
    {
        public void ValidateEntity(SupervisingDept entity)
        {
            // Validate du lieu
            Utils.ValidateRequire(entity.SupervisingDeptCode, "SupervisingDept_Label_SupervisingDeptCode");
            Utils.ValidateMaxLength(entity.SupervisingDeptCode, 20, "SupervisingDept_Label_SupervisingDeptCode");
            this.CheckDuplicateCouple(entity.SupervisingDeptCode, entity.ProvinceID.ToString(), "School", "SupervisingDept",
               "SupervisingDeptCode", "ProvinceID", true, entity.SupervisingDeptID, "SupervisingDept_Label_SupervisingDeptCode");
            Utils.ValidateRequire(entity.SupervisingDeptName, "SupervisingDept_Label_SupervisingDeptName");
            Utils.ValidateMaxLength(entity.SupervisingDeptName, 100, "SupervisingDept_Label_SupervisingDeptName");
            this.CheckDuplicateCouple(entity.SupervisingDeptName, entity.ProvinceID.ToString(), "School", "SupervisingDept",
               "SupervisingDeptCode", "ProvinceID", true, entity.SupervisingDeptID, "SupervisingDept_Label_SupervisingDeptName");
            new ProvinceBusiness(null).CheckAvailable(entity.ProvinceID, "SupervisingDept_Label_Province");
            if (entity.HierachyLevel != SystemParamsInFile.EDUCATION_HIERACHY_LEVEL_PROVINCE_OFFICE_DEPARTMENT)
            {
                new DistrictBusiness(null).CheckAvailable(entity.DistrictID, "SupervisingDept_Label_District");
            }

            SupervisingDept super = this.Find(entity.ParentID);

            if (super != null && super.ParentID != null)
            {
                if (super.ProvinceID != entity.ProvinceID)
                {
                    throw new BusinessException("SupervisingDept_Validate_WrongParentID");
                }
            }

            Utils.ValidateMaxLength(entity.Telephone, 15, "SupervisingDept_Label_Telephone");

            Utils.ValidateMaxLength(entity.Address, 400, "SupervisingDept_Label_Address");

            new UserAccountBusiness(null).CheckAvailable(entity.AdminID, "SupervisingDept_Label_Username", false);

        }

        public override SupervisingDept Insert(SupervisingDept entityToInsert)
        {
            ValidateEntity(entityToInsert);

            // Insert vao CSDL
            return base.Insert(entityToInsert);
        }


        public override SupervisingDept Update(SupervisingDept entity)
        {
            ValidateEntity(entity);

            // Update vao CSDL
            return base.Update(entity);
        }

        public void Delete(int id, int UserAccountID)
        {
            this.CheckAvailable(id, "SupervisingDept_Label_SupervisingDeptCode");
            //var check = this.repository.CheckConstraintsWithExceptTable(GlobalConstants.SCHOOL_SCHEMA, "SupervisingDept", ",", id);
            //if (check)
            //{
                //if ton tai id trong bang khac
            //    throw new BusinessException("SupervisingDept_Validate_UsingSupervisingDept");
            //}
            IQueryable<SupervisingDept> lsSuper = SupervisingDeptBusiness.All.Where(o => o.IsActive == true && o.ParentID == id);
            if (lsSuper.Count() > 0)
            {
                throw new BusinessException("SupervisingDept_Validate_UsingSupervisingDept");
            }

            SupervisingDept supervisingDept = this.Find(id);
            SupervisingDept sp = GetSupervisingDeptOfUser(UserAccountID);

            if (supervisingDept.UserAccount != null)
            {
                //disabled tai khoan nguoi dung
                var UserID = supervisingDept.UserAccount.UserAccountID;

                UserAccountBusiness.DeleteAccount(UserID);
            }
            if (sp != null && !supervisingDept.TraversalPath.Contains(sp.TraversalPath))
            {
                List<object> Params = new List<object>();
                throw new BusinessException("SupervisingDept_No_Delete_Permission", Params);
            }
            // Update vao CSDL
            base.Delete(id, true);
        }

        public void DeleteSub(int parentID)
        {
            SupervisingDept super = SupervisingDeptBusiness.Find(parentID);
            string TraversalPath = super.TraversalPath + super.SupervisingDeptID + "\\";
            IQueryable<SupervisingDept> lsSupervisingDept = SupervisingDeptBusiness.All.Where(o => o.TraversalPath.Contains(TraversalPath));
            if (lsSupervisingDept.Count() > 0)
            {
                List<SupervisingDept> lst = lsSupervisingDept.ToList();
                foreach (SupervisingDept sup in lst)
                {
                    IQueryable<Employee> lsEmployee = EmployeeBusiness.All.Where(o => o.SupervisingDeptID == sup.SupervisingDeptID);
                    if (lsEmployee.Count() > 0)
                    {
                        List<Employee> lstEmployee = lsEmployee.ToList();
                        foreach (Employee em in lstEmployee)
                        {
                            em.IsActive = false;
                            EmployeeBusiness.Update(em);

                        }
                    }
                    sup.IsActive = false;
                    this.Update(sup);

                }
                EmployeeBusiness.Save();
                this.Save();
            }
        }

        public IQueryable<SupervisingDept> ListChildDept(int? deptId)
        {
            return deptId == null ? this.All.Where(o => (o.ParentID == null && o.IsActive == true)) :
                                    this.All.Where(o => (o.ParentID == deptId && o.IsActive == true));
        }

        public IQueryable<SupervisingDept> Search(IDictionary<string, object> SearchInfo)
        {
            // Danh sach cac tham so tim kiem
            int SupervisingDeptID = Utils.GetInt(SearchInfo, "SupervisingDeptID");
            string SupervisingDeptCode = Utils.GetString(SearchInfo, "SupervisingDeptCode");
            string SupervisingDeptName = Utils.GetString(SearchInfo, "SupervisingDeptName");
            List<int> ListHierachyLevel = Utils.GetIntList(SearchInfo, "ListHierachyLevel");
            int ProvinceID = Utils.GetInt(SearchInfo, "ProvinceID");
            int DistrictID = Utils.GetInt(SearchInfo, "DistrictID");
            string TraversalPath = Utils.GetString(SearchInfo, "TraversalPath");
            bool? IsActive = Utils.GetIsActive(SearchInfo, "IsActive");
            int AdminID = Utils.GetInt(SearchInfo, "AdminID");
            int ParentID = Utils.GetInt(SearchInfo, "ParentID");
            int HierachyLevel = Utils.GetInt(SearchInfo, "HierachyLevel");

            // Truy van du lieu
            IQueryable<SupervisingDept> res = this.All.Include("UserAccount.aspnet_Users");

            if (SupervisingDeptID != 0)
            {
                res = res.Where(o => o.SupervisingDeptID == SupervisingDeptID);
            }
            if (AdminID != 0)
            {
                res = res.Where(o => o.AdminID == AdminID);
            }
            if (SupervisingDeptCode != null && SupervisingDeptCode.Trim() != "")
            {
                res = res.Where(o => o.SupervisingDeptCode.ToLower().Contains(SupervisingDeptCode.Trim().ToLower()));
            }

            if (SupervisingDeptName != null && SupervisingDeptName.Trim() != "")
            {
                res = res.Where(o => o.SupervisingDeptName.ToLower().Contains(SupervisingDeptName.Trim().ToLower()));
            }

            if (ListHierachyLevel.Count > 0)
            {
                res = res.Where(o => ListHierachyLevel.Contains(o.HierachyLevel));
            }

            if (HierachyLevel != 0)
            {
                res = res.Where(o => o.HierachyLevel == HierachyLevel);
            }

            if (ProvinceID != 0)
            {
                res = res.Where(o => o.ProvinceID == ProvinceID);
            }

            if (DistrictID != 0)
            {
                res = res.Where(o => o.DistrictID == DistrictID);
            }

            if (TraversalPath != "")
            {
                res = res.Where(o => o.TraversalPath.Contains(TraversalPath.Trim()));
            }
            if (IsActive != null)
            {
                res = res.Where(o => o.IsActive == IsActive);
            }
            if (ParentID != 0)
            {
                res = res.Where(o => o.ParentID == ParentID);
            }
            return res;
        }

        public void InsertSupervisingDept(SupervisingDept supervisingDept, string userName, int UserAccountID, int RoleID, bool isActive,string password = "")
        {
            this.CheckDuplicateCouple(supervisingDept.SupervisingDeptCode, supervisingDept.ProvinceID.ToString(), "School", "SupervisingDept",
              "SupervisingDeptCode", "ProvinceID", true, supervisingDept.SupervisingDeptID, "SupervisingDept_Label_SupervisingDeptCode");
            this.CheckDuplicateCouple(supervisingDept.SupervisingDeptName, supervisingDept.ProvinceID.ToString(), "School", "SupervisingDept",
               "SupervisingDeptCode", "ProvinceID", true, supervisingDept.SupervisingDeptID, "SupervisingDept_Label_SupervisingDeptName");

            ValidateEntity(supervisingDept);
            if (supervisingDept.HierachyLevel != 4)
            {
                // Tao tai khoan admin truoc
                int? UserID = UserAccountBusiness.CreateAdminSupervisingDept(userName, UserAccountID, RoleID, isActive,password);
                if (isActive) { UserAccountBusiness.ActiveAccount(UserID.Value); }
                else { UserAccountBusiness.DeActiveAccount(UserID.Value); }
                // Luu vao CSDL
                supervisingDept.AdminID = UserID;
            }
            this.Insert(supervisingDept);

        }

        public void UpdateSupervisingDept(SupervisingDept supervisingDept, bool isActive, bool isForgetPassword, int RoleID = 0)
        {
            if (isActive)
            {
                UserAccountBusiness.ActiveAccount(supervisingDept.AdminID.Value);
            }
            else
            {
                UserAccountBusiness.DeActiveAccount(supervisingDept.AdminID.Value);
            }

            //// Lay lai mat khau
            //if (isForgetPassword)
            //{
            //    UserAccountBusiness.ResetPassword(supervisingDept.AdminID.Value);
            //}

            //cap nhat lai role id cho account
            if (RoleID != 0)
            {
                IDictionary<string, object> dic = new Dictionary<string, object>();

                IQueryable<UserRole> listRoles = UserRoleBusiness.All.Where(o => o.UserID == supervisingDept.AdminID.Value);
                if (listRoles != null && listRoles.Count() > 0)
                {
                    UserRole UserRoleObj = listRoles.FirstOrDefault();
                    UserRoleObj.RoleID = RoleID;

                    UserRoleBusiness.Update(UserRoleObj);
                    UserRoleBusiness.Save();
                }

            }

            //SupervisingDept sp = GetSupervisingDeptOfUser(UserAccountID);
            //if (sp == null || !supervisingDept.TraversalPath.Contains(sp.TraversalPath))
            //{
            //    List<object> Params = new List<object>();
            //    throw new BusinessException("SupervisingDept_No_Delete_Permission", Params);
            //}

            // QuangNN2 - 13/05/2015 - Cap nhat thuoc tinh Synchronize = -2
            //UserAccountBusiness.UpdateSynchronize(supervisingDept.AdminID.Value, GlobalConstants.STATUS_UPDATE_INFO);

            // Luu vao CSDL
            this.Update(supervisingDept);
        }

        public SupervisingDept GetSupervisingDeptOfUser(int UserAccountID)
        {
            bool isAdmin = UserAccountBusiness.IsAdmin(UserAccountID);
            if (isAdmin)
            {
                return this.All.Where(o => o.AdminID == UserAccountID).FirstOrDefault();
            }
            else
            {
                Employee employee = UserAccountBusiness.MapFromUserAccountToEmployee(UserAccountID);
                return employee.SupervisingDept;
            }
        }

        public List<SupervisingDept> GetManagedUnitDirectly(int UserAccountID)
        {
            SupervisingDept sd = GetSupervisingDeptOfUser(UserAccountID);
            if (sd.HierachyLevel == SystemParamsInFile.EDUCATION_HIERACHY_LEVEL_PROVINCE_OFFICE || sd.HierachyLevel == SystemParamsInFile.EDUCATION_HIERACHY_LEVEL_PROVINCE_OFFICE_DEPARTMENT)
            {
                IDictionary<string, object> SearchInfo = new Dictionary<string, object>();
                SearchInfo["ProvinceID"] = sd.ProvinceID;
                return Search(SearchInfo).ToList();
            }
            else if (sd.HierachyLevel == SystemParamsInFile.EDUCATION_HIERACHY_LEVEL_DISTRICT_OFFICE || sd.HierachyLevel == SystemParamsInFile.EDUCATION_HIERACHY_LEVEL_DISTRICT_OFFICE_DEPARTMENT)
            {
                List<SupervisingDept> list = new List<SupervisingDept>();
                list.Add(sd);
                return list;
            }
            return new List<SupervisingDept>();
        }

        /// <summary>
        /// Lay len danh sach phong ban truc thuoc
        /// Hungnd8
        /// </summary>
        /// <param name="SupervisingDeptID"></param>
        /// <returns></returns>
        public IQueryable<SupervisingDept> GetFollwerSupervising(int SupervisingDeptID)
        {
            var query = this.All.Where(s => s.ParentID == SupervisingDeptID
                && (s.HierachyLevel == SystemParamsInFile.EDUCATION_HIERACHY_LEVEL_PROVINCE_OFFICE_DEPARTMENT || // Cap phong ban thuoc so
                s.HierachyLevel == SystemParamsInFile.EDUCATION_HIERACHY_LEVEL_DISTRICT_OFFICE_DEPARTMENT) // Cap phong ban thuoc phong
                && s.IsActive == true);
            return query;
        }

        /// <summary>
        /// Quanglm1 - 19/02/2014
        /// </summary>
        /// <param name="dic"></param>
        /// <returns></returns>
        public List<SubjectCatBO> SearchSubject(IDictionary<string, object> dic)
        {
            int Year = Utils.GetInt(dic, "Year");
            int EducationLevelID = Utils.GetInt(dic, "EducationLevelID");
            int SupervisingDeptID = Utils.GetInt(dic, "SupervisingDeptID");
            int ProvinceID = Utils.GetInt(dic, "ProvinceID");
            int? AppliedLevel = Utils.GetNullableInt(dic, "AppliedLevel");
            int SubjectID = Utils.GetInt(dic, "SubjectID");
            if (AppliedLevel == null)
            {
                AppliedLevel = 0;
            }
            

            // Neu khong truyen thi tim tat ca
            // Neu truyen 0 - Mon tinh diem
            // Neu truyen 1 - Mon nhan xet
            int? IsCommenting = Utils.GetNullableInt(dic, "IsCommenting");


            IQueryable<SubjectCatBO> query = (from ps in ProvinceSubjectBusiness.All
                                              join sc in SubjectCatBusiness.All on ps.SubjectID equals sc.SubjectCatID
                                              join ayp in AcademicYearOfProvinceBusiness.All on ps.AcademicYearOfProvinceID equals ayp.AcademicYearOfProvinceID
                                              where ps.ProvinceID == ProvinceID
                                                    && (ps.EducationLevelID == EducationLevelID || EducationLevelID == 0)
                                                    && ayp.Year == Year
                                                    && (ps.SubjectID == SubjectID || SubjectID == 0)
                                                    && (ps.AppliedLevel == AppliedLevel || AppliedLevel == 0)
                                                    && sc.IsActive == true
                                                    && (IsCommenting.HasValue ? IsCommenting.Value == 0 ?
                                                    (ps.IsCommenting == SystemParamsInFile.ISCOMMENTING_TYPE_MARK || ps.IsCommenting == SystemParamsInFile.ISCOMMENTING_TYPE_MARK_JUDGE)
                                                    : ps.IsCommenting == SystemParamsInFile.ISCOMMENTING_TYPE_JUDGE : true)
                                              orderby sc.OrderInSubject
                                              select new SubjectCatBO
                                              {
                                                  SubjectCatID = sc.SubjectCatID,
                                                  SubjectName = sc.DisplayName,
                                                  Abbreviation = sc.Abbreviation,
                                                  DisplayName = sc.DisplayName,
                                                  IsCommenting = sc.IsCommenting,
                                                  OrderInSubject = sc.OrderInSubject
                                              });
            // Neu ton tai mon hoc duoc khai bao o so thi lay thong tin
            if (query.Count() > 0)
            {
                return query.Distinct().OrderBy(o => o.OrderInSubject).ThenBy(o => o.DisplayName).ToList();
            }
            // Neu chua ton tai mon hoc o so thi lay hop cua tat ca mon hoc o truong
            string traversalPath = "/" + SupervisingDeptID + "/";
            // Lay tat ca don vi con hoac chinh no
            SupervisingDept su = SupervisingDeptBusiness.Find(SupervisingDeptID);
            if (su != null && (su.HierachyLevel == SystemParamsInFile.EDUCATION_HIERACHY_LEVEL_DISTRICT_OFFICE_DEPARTMENT || su.HierachyLevel == SystemParamsInFile.EDUCATION_HIERACHY_LEVEL_PROVINCE_OFFICE_DEPARTMENT))
            {
                SupervisingDeptID = su.ParentID.Value;
            }
            IQueryable<SupervisingDept> listSupervisingDept = SupervisingDeptBusiness.AllNoTracking
                .Where(o => o.IsActive && (o.ParentID == SupervisingDeptID || o.SupervisingDeptID == SupervisingDeptID));

            // Lay mon hoc la hop cua tat ca cac truong truc thuoc don vi
            query = from sc in SchoolSubjectBusiness.All
                    join sp in SchoolProfileBusiness.All on sc.SchoolID equals sp.SchoolProfileID
                    join s in SubjectCatBusiness.All on sc.SubjectID equals s.SubjectCatID
                    join e in EducationLevelBusiness.All on sc.EducationLevelID equals e.EducationLevelID
                    where listSupervisingDept.Any(o => o.SupervisingDeptID == sp.SupervisingDeptID) && sc.AcademicYear.Year == Year
                    && (sc.EducationLevelID == EducationLevelID || EducationLevelID == 0) && s.IsActive
                    && (s.AppliedLevel == AppliedLevel || AppliedLevel == 0)
                    && (e.Grade == AppliedLevel || AppliedLevel == 0)
                    && (sc.SubjectID == SubjectID || SubjectID == 0)
                    && (IsCommenting.HasValue ? IsCommenting.Value == 0 ?
                                                   (sc.IsCommenting == SystemParamsInFile.ISCOMMENTING_TYPE_MARK || sc.IsCommenting == SystemParamsInFile.ISCOMMENTING_TYPE_MARK_JUDGE)
                                                   : sc.IsCommenting == SystemParamsInFile.ISCOMMENTING_TYPE_JUDGE : true)                
                    select new SubjectCatBO
                    {
                        SubjectCatID = s.SubjectCatID,
                        DisplayName = s.DisplayName,
                        Abbreviation = s.Abbreviation,
                        SubjectName = s.DisplayName,
                        IsCommenting = sc.IsCommenting,
                        OrderInSubject = s.OrderInSubject
                    };

            return query.Distinct().OrderBy(o => o.OrderInSubject).ThenBy(o => o.DisplayName).ToList();
        }

        /// <summary>
        /// Lay thong tin cap truc thuoc don vi
        /// </summary>
        /// <param name="SuperVisingDeptID"></param>
        /// <returns></returns>
        public List<SupervisingDept> GetListSuperVisingDeptByID(int SupervisingDeptID)
        {

            SupervisingDept supervisingDept = SupervisingDeptBusiness.Find(SupervisingDeptID);
            if (supervisingDept == null)
            {
                return new List<SupervisingDept>();
            }
            // Neu don vi dau vao khong phai phong so thi tra lai danh sach rong
            if (supervisingDept.HierachyLevel != SystemParamsInFile.EDUCATION_HIERACHY_LEVEL_PROVINCE_OFFICE
                && supervisingDept.HierachyLevel != SystemParamsInFile.EDUCATION_HIERACHY_LEVEL_PROVINCE_OFFICE_DEPARTMENT
                && supervisingDept.HierachyLevel != SystemParamsInFile.EDUCATION_HIERACHY_LEVEL_DISTRICT_OFFICE
                && supervisingDept.HierachyLevel != SystemParamsInFile.EDUCATION_HIERACHY_LEVEL_DISTRICT_OFFICE)
            {
                return new List<SupervisingDept>();
            }
            // ID don vi thuc te se lay danh sach don vi con truc thuoc
            int trueSupervisingDeptID = 0;
            if (supervisingDept.HierachyLevel == SystemParamsInFile.EDUCATION_HIERACHY_LEVEL_PROVINCE_OFFICE
                || supervisingDept.HierachyLevel == SystemParamsInFile.EDUCATION_HIERACHY_LEVEL_DISTRICT_OFFICE)
            {
                trueSupervisingDeptID = SupervisingDeptID;
            }
            else
            {
                // Lay tat ca cac ID cha de lay danh sach don vi truc thuoc cua ID cha
                string traversalPath = supervisingDept.TraversalPath;
                string[] parentIds = traversalPath.Split('\\');
                foreach (string parentId in parentIds)
                {
                    if (!string.IsNullOrWhiteSpace(parentId))
                    {
                        SupervisingDept st = SupervisingDeptBusiness.Find(int.Parse(parentId));
                        if (st.HierachyLevel == SystemParamsInFile.EDUCATION_HIERACHY_LEVEL_PROVINCE_OFFICE
                            || st.HierachyLevel == SystemParamsInFile.EDUCATION_HIERACHY_LEVEL_DISTRICT_OFFICE)
                        {
                            trueSupervisingDeptID = st.SupervisingDeptID;
                            break;
                        }
                    }
                }
            }
            // Neu van khong co ID cap phong hoac so tuong ung thi tra lai danh sach rong
            List<SupervisingDept> listSupervisingDept = new List<SupervisingDept>(); ;
            if (trueSupervisingDeptID > 0)
            {
                Dictionary<string, object> dic = new Dictionary<string, object>();
                dic["IsActive"] = true;
                dic["ParentID"] = trueSupervisingDeptID;
                listSupervisingDept = SupervisingDeptBusiness.Search(dic).OrderBy(o => o.SupervisingDeptName).ToList();
            }

            return listSupervisingDept;
        }

        /// <summary>
        /// Tìm kiếm tât cả nhân viên thuộc đơn vị kể cả nhân viên của đơn vị con.
        /// </summary>
        /// <author date="2014/02/20">HaiVT</author>
        /// <param name="CurrentEmployeeID">dk where khac voi currentEmployeeID</param>
        /// <param name="SearchContent">noi dung tim kiem</param>
        /// <param name="SupervisingDeptID">id phong so</param>
        /// <returns></returns>
        public List<int> GetEmployeeBySuperVisingDeptNoPaging(int CurrentEmployeeID, string SearchContent, int SupervisingDeptID)
        {
            Dictionary<string, object> dic = new Dictionary<string, object>();
            dic["TraversalPath"] = "\\" + SupervisingDeptID + "\\";
            IQueryable<Employee> iqEmployee = EmployeeBusiness.SearchEmployee(SupervisingDeptID, dic)
                .Where(o => o.EmployeeID != CurrentEmployeeID && !string.IsNullOrEmpty(o.Mobile.Trim()));
            if (!string.IsNullOrWhiteSpace(SearchContent))
            {
                iqEmployee = iqEmployee.Where(o => o.EmployeeCode.ToUpper().Contains(SearchContent.Trim().ToUpper())
                    || o.FullName.ToUpper().Contains(SearchContent.Trim().ToUpper()));
            }

            //gan validationCode thanh cong
            //gan gia tri thuoc tinh dinh kem 
            return iqEmployee.Select(p => p.EmployeeID).ToList();

        }

        /// <summary>
        /// Lay danh sach can bo theo dieu kien dau vao - Co phan trang tung phan
        /// </summary>
        /// <param name="CurrentEmployeeID"></param>
        /// <param name="SearchContent"></param>
        /// <param name="SuperisingDeptID"></param>
        /// <param name="PageNum"></param>
        /// <param name="PageSize"></param>
        /// <returns></returns>
        public List<EmployeeBO> GetListEmployeeBySuperVisingDept(int CurrentEmployeeID, string SearchContent, int SupervisingDeptID)
        {

            Dictionary<string, object> dic = new Dictionary<string, object>();
            dic["TraversalPath"] = "\\" + SupervisingDeptID + "\\";
            IQueryable<Employee> iqEmployee = EmployeeBusiness.SearchEmployee(SupervisingDeptID, dic)
                .Where(o => o.EmployeeID != CurrentEmployeeID && !string.IsNullOrEmpty(o.Mobile.Trim()));
            if (!string.IsNullOrWhiteSpace(SearchContent))
            {
                iqEmployee = iqEmployee.Where(o => o.EmployeeCode.ToLower().Contains(SearchContent.Trim().ToLower())
                    || o.FullName.ToLower().Contains(SearchContent.Trim().ToLower()));
            }
            List<EmployeeBO> listEmployee = iqEmployee
                .Select(o => new EmployeeBO
                {
                    EmployeeID = o.EmployeeID,
                    EmployeeCode = o.EmployeeCode,
                    FullName = o.FullName,
                    Mobile = o.Mobile,
                    SupervisingDeptName = o.SupervisingDept.SupervisingDeptName,
                    SupervisingDeptID = o.SupervisingDeptID
                }).OrderBy(o => o.FullName).ToList();

            return listEmployee;
        }

        public List<EmployeeSMSBO> GetEmployeeBySuperVisingDept(int provinceID, int ParentID, int currentEmployeeID, string searchContent, int supervisingDeptID)
        {
            List<EmployeeBO> lst = this.GetListEmployeeBySuperVisingDept(currentEmployeeID, searchContent, supervisingDeptID);
            List<EmployeeSMSBO> list = null;
            if (lst != null)
            {
                list = (from l in lst
                        select new EmployeeSMSBO
                        {
                            EmployeeID = l.EmployeeID,
                            FullName = l.FullName,
                            EmployeeCode = l.EmployeeCode,
                            Mobile = l.Mobile,
                            SuperVisingDeptName = l.SupervisingDeptName,
                            SupervisingDeptID = l.SupervisingDeptID.GetValueOrDefault()
                        }).ToList();
            }
            var objPromotion = this.PromotionBusiness.GetPromotionProgramByProvinceId(provinceID, supervisingDeptID);
            Dictionary<string, object> dicSearch = new Dictionary<string, object>()
                {
                    {"Type",GlobalConstantsEdu.COMMON_HISTORY_RECEIVER_EMPLOYEE_ID},
                    {"SupervisingDeptID",ParentID},
                    {"dateTimeNow",DateTime.Now}
                };
            List<SMS_RECEIVED_RECORD> lstReceivedRecord = ReceivedRecordBusiness.GetListReceiRecord(dicSearch);
            SMS_RECEIVED_RECORD objReceivedRecord;
            int fromOfInternalSMS = objPromotion != null ? objPromotion.FromOfInternalSMS : 0;
            int fromOfExternalSMS = objPromotion != null ? objPromotion.FromOfExternalSMS : 0;
            foreach (var objEmployeeBO in list)
            {
                objReceivedRecord = lstReceivedRecord.FirstOrDefault(e => e.EMPLOYEE_ID == objEmployeeBO.EmployeeID);
                objEmployeeBO.isFreeSMS = objPromotion != null ? objPromotion.IsFreeSMS : null;
                objEmployeeBO.isFreeExSMS = objPromotion != null ? objPromotion.IsFreeExSMS : null;
                if (objReceivedRecord != null)
                {
                    int totalInternalSMS = objReceivedRecord.TOTAL_INTERNAL_SMS;
                    int totalExternalSMS = objReceivedRecord.TOTAL_EXTERNAL_SMS;
                    objEmployeeBO.TotalInternalSMS = totalInternalSMS;
                    objEmployeeBO.TotalExternalSMS = totalExternalSMS;
                    objEmployeeBO.isNumberVT = false;
                    if (objEmployeeBO.Mobile.CheckMobileNumberVT())
                    {
                        objEmployeeBO.TotalSMSPromotion = (totalInternalSMS + totalExternalSMS >= fromOfInternalSMS) ? 0 : fromOfInternalSMS - totalExternalSMS - totalInternalSMS;
                        objEmployeeBO.isNumberVT = true;
                    }
                    else if ((totalInternalSMS + totalExternalSMS >= fromOfInternalSMS))// Thue bao ngoai mang
                    {
                        objEmployeeBO.TotalSMSPromotion = 0;
                    }
                    else if (totalExternalSMS >= fromOfExternalSMS)//if K>=N
                    {
                        objEmployeeBO.TotalSMSPromotion = 0;
                    }
                    else //Min(M- V-K, N- K)
                    {
                        objEmployeeBO.TotalSMSPromotion = Math.Min(fromOfInternalSMS - totalExternalSMS - totalInternalSMS, fromOfExternalSMS - totalExternalSMS);
                    }
                }
                else
                {
                    objEmployeeBO.TotalExternalSMS = 0;
                    objEmployeeBO.TotalInternalSMS = 0;
                    if (objEmployeeBO.Mobile.CheckMobileNumberVT())
                    {
                        objEmployeeBO.TotalSMSPromotion = fromOfInternalSMS;
                        objEmployeeBO.isNumberVT = true;
                    }
                    else
                    {
                        objEmployeeBO.TotalSMSPromotion = fromOfExternalSMS;
                        objEmployeeBO.isNumberVT = false;
                    }
                }
            }
            return list;
        }

        public List<EmployeeBO> GetEmployeeBySuperVisingDeptID(int SupervisingDeptID, bool SupervisingRole, bool SupervisingSubRole)
        {
            try
            {
                string traversalPath = string.Empty;
                SupervisingDept objSuperVisingDept = SupervisingDeptBusiness.Find(SupervisingDeptID);
                traversalPath = objSuperVisingDept.TraversalPath + SupervisingDeptID + "\\";
                //IQueryable<Employee> iqEmployee = EmployeeBusiness.SearchEmployee(SupervisingDeptID, dic).Where(p=>p.Mobile != null);
                List<EmployeeBO> listEmployee = (from e in EmployeeBusiness.All
                                                              join s in SupervisingDeptBusiness.All on e.SupervisingDeptID equals s.SupervisingDeptID
                                                              where e.IsActive
                                                              && (s.TraversalPath.Contains(traversalPath) || e.SupervisingDeptID == SupervisingDeptID)
                                                              && ((SupervisingRole && (s.HierachyLevel == 3 || s.HierachyLevel == 4))
                                                                    || (SupervisingSubRole && (s.HierachyLevel == 5 || s.HierachyLevel == 6)))
                                                              && e.Mobile != null
                                                              orderby e.FullName
                                                 select new EmployeeBO
                                                              {
                                                                  EmployeeID = e.EmployeeID,
                                                                  FullName = e.FullName
                                                              }).ToList();

                return listEmployee;
            }
            catch (Exception ex)
            {
                logger.Error(ex.Message, ex);
                return new List<EmployeeBO>();
            }
        }

        /// <summary>
        /// Lay thong tin cap truc thuoc don vi
        /// </summary>
        /// <param name="SuperVisingDeptID"></param>
        /// <returns></returns>
        public List<SupervisingDeptBO> GetListSuperVisingDeptBO(int SupervisingDeptID)
        {
            try
            {
                SupervisingDept supervisingDept = SupervisingDeptBusiness.Find(SupervisingDeptID);
                if (supervisingDept == null)
                {
                    return new List<SupervisingDeptBO>();
                }
                // Neu don vi dau vao khong phai phong so thi tra lai danh sach rong
                if (supervisingDept.HierachyLevel != SystemParamsInFile.EDUCATION_HIERACHY_LEVEL_PROVINCE_OFFICE
                    && supervisingDept.HierachyLevel != SystemParamsInFile.EDUCATION_HIERACHY_LEVEL_PROVINCE_OFFICE_DEPARTMENT
                    && supervisingDept.HierachyLevel != SystemParamsInFile.EDUCATION_HIERACHY_LEVEL_DISTRICT_OFFICE
                    && supervisingDept.HierachyLevel != SystemParamsInFile.EDUCATION_HIERACHY_LEVEL_DISTRICT_OFFICE)
                {
                    return new List<SupervisingDeptBO>();
                }
                // ID don vi thuc te se lay danh sach don vi con truc thuoc
                int trueSupervisingDeptID = 0;
                if (supervisingDept.HierachyLevel == SystemParamsInFile.EDUCATION_HIERACHY_LEVEL_PROVINCE_OFFICE
                    || supervisingDept.HierachyLevel == SystemParamsInFile.EDUCATION_HIERACHY_LEVEL_DISTRICT_OFFICE)
                {
                    trueSupervisingDeptID = SupervisingDeptID;
                }
                else
                {
                    // Lay tat ca cac ID cha de lay danh sach don vi truc thuoc cua ID cha
                    string traversalPath = supervisingDept.TraversalPath;
                    string[] parentIds = traversalPath.Split('\\');
                    foreach (string parentId in parentIds)
                    {
                        if (!string.IsNullOrWhiteSpace(parentId))
                        {
                            SupervisingDept st = SupervisingDeptBusiness.Find(int.Parse(parentId));
                            if (st.HierachyLevel == SystemParamsInFile.EDUCATION_HIERACHY_LEVEL_PROVINCE_OFFICE
                                || st.HierachyLevel == SystemParamsInFile.EDUCATION_HIERACHY_LEVEL_DISTRICT_OFFICE)
                            {
                                trueSupervisingDeptID = st.SupervisingDeptID;
                                break;
                            }
                        }
                    }
                }
                // Neu van khong co ID cap phong hoac so tuong ung thi tra lai danh sach rong
                List<SupervisingDeptBO> listSupervisingDept = null;
                if (trueSupervisingDeptID > 0)
                {
                    Dictionary<string, object> dic = new Dictionary<string, object>();
                    dic["IsActive"] = true;
                    dic["ParentID"] = trueSupervisingDeptID;
                    listSupervisingDept = SupervisingDeptBusiness.Search(dic)
                        .Select(o => new SupervisingDeptBO
                        {
                            SupervisingDeptID = o.SupervisingDeptID,
                            SupervisingDeptName = o.SupervisingDeptName,
                            ParentID = o.ParentID.HasValue ? o.ParentID.Value : 0,
                            HierachyLevel = (short)o.HierachyLevel,
                            Traversal_Path = o.TraversalPath
                        }).OrderBy(o => o.SupervisingDeptName).ToList();
                }
                return listSupervisingDept;
            }
            catch (Exception ex)
            {
                logger.Error(ex.Message, ex);
                return new List<SupervisingDeptBO>();
            }
        }
    }
}