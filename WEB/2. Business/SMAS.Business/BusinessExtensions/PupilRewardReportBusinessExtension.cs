﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SMAS.Business.BusinessObject;
using SMAS.Business.Common;
using SMAS.Models.Models;
using System.IO;
using SMAS.VTUtils.Excel.Export;
using System.Drawing;

namespace SMAS.Business.Business
{
    public partial class PupilRewardReportBusiness
    {
        #region private member variable

        #endregion

        #region Tạo mảng băm cho các tham số đầu vào của danh sách khen thưởng cấp 1
        public string GetHashKeyForPriamry(PupilRewardReportBO entity)
        {
            IDictionary<string, object> dic = new Dictionary<string, object> {
                {"AcademicYearID", entity.AcademicYearID},
                {"EducationLevelID ", entity.EducationLevelID},
                {"SchoolID", entity.SchoolID},
                {"Semester", entity.Semester},
                {"AppliedLevel", entity.AppliedLevel}
            };
            return ReportUtils.GetHashKey(dic);
        }
        #endregion

        #region Lấy danh sách khen thưởng cấp 1 được cập nhật mới nhất
        public ProcessedReport GetPupilRewardReportForPrimary(PupilRewardReportBO entity)
        {
            string reportCode = SystemParamsInFile.REPORT_DS_HS_KHEN_THUONG_CAP1;
            string inputParameterHashKey = GetHashKeyForPriamry(entity);
            return ProcessedReportBusiness.GetProcessedReport(reportCode, inputParameterHashKey);
        }
        public ProcessedReport GetPupilRewardReportForPrimaryTT30(PupilRewardReportBO entity)
        {
            string reportCode = SystemParamsInFile.REPORT_DS_HS_KHEN_THUONG_TT30_CAP1;
            string inputParameterHashKey = GetHashKeyForPriamry(entity);
            return ProcessedReportBusiness.GetProcessedReport(reportCode, inputParameterHashKey);
        }

        #endregion

        #region Lưu báo cáo danh sách khen thưởng cấp 1 vào CSDL
        public ProcessedReport InsertPupilRewardPrimaryReport(PupilRewardReportBO entity, Stream data)
        {
            string reportCode = SystemParamsInFile.REPORT_DS_HS_KHEN_THUONG_CAP1;
            ReportDefinition reportDef = ReportDefinitionBusiness.GetByCode(reportCode);
            ProcessedReport pr = new ProcessedReport();
            pr.ReportCode = reportCode;
            pr.ProcessedDate = DateTime.Now;
            pr.InputParameterHashKey = GetHashKeyForPriamry(entity);
            pr.ReportData = ReportUtils.Compress(data);

            EducationLevel eduLevel = EducationLevelBusiness.Find(entity.EducationLevelID);

            //Tạo tên file
            string outputNamePattern = reportDef.OutputNamePattern;
            string semester = ReportUtils.ConvertSemesterForReportName(entity.Semester.Value);
            string schoolLevel = entity.AppliedLevel.HasValue ? ReportUtils.ConvertAppliedLevelForReportName(entity.AppliedLevel.Value) : "";
            string educationLevel = eduLevel != null ? eduLevel.Resolution : "Tất cả";
            outputNamePattern = outputNamePattern.Replace("[SchoolLevel]", schoolLevel).Replace("[EducationLevel/Class]", educationLevel).Replace("[Semester]", semester);

            pr.ReportName = ReportUtils.RemoveSpecialCharacters(outputNamePattern) + "." + reportDef.OutputFormat;

            IDictionary<string, object> dic = new Dictionary<string, object> {
                {"AcademicYearID", entity.AcademicYearID},
                {"EducationLevelID ", entity.EducationLevelID},
                {"SchoolID", entity.SchoolID},
                {"Semester", entity.Semester}
            };

            ProcessedReportParameterRepository.Insert(dic, pr);
            ProcessedReportRepository.Insert(pr);
            ProcessedReportRepository.Save();
            return pr;
        }

        public ProcessedReport InsertPupilRewardPrimaryReportTT30(PupilRewardReportBO entity, Stream data)
        {
            string reportCode = SystemParamsInFile.REPORT_DS_HS_KHEN_THUONG_TT30_CAP1;
            ReportDefinition reportDef = ReportDefinitionBusiness.GetByCode(reportCode);
            ProcessedReport pr = new ProcessedReport();
            pr.ReportCode = reportCode;
            pr.ProcessedDate = DateTime.Now;
            pr.InputParameterHashKey = GetHashKeyForPriamry(entity);
            pr.ReportData = ReportUtils.Compress(data);

            EducationLevel eduLevel = EducationLevelBusiness.Find(entity.EducationLevelID);

            //Tạo tên file
            string outputNamePattern = reportDef.OutputNamePattern;
            string semester = ReportUtils.ConvertSemesterForReportName(entity.Semester.Value);
            string schoolLevel = entity.AppliedLevel.HasValue ? ReportUtils.ConvertAppliedLevelForReportName(entity.AppliedLevel.Value) : "";
            string educationLevel = eduLevel != null ? eduLevel.Resolution : "Tất Cả";
            outputNamePattern = outputNamePattern.Replace("[SchoolLevel]", schoolLevel).Replace("[EducationLevel_Class]", educationLevel).Replace("[Semester]", semester);

            pr.ReportName = ReportUtils.RemoveSpecialCharacters(outputNamePattern) + "." + reportDef.OutputFormat;

            IDictionary<string, object> dic = new Dictionary<string, object> {
                {"AcademicYearID", entity.AcademicYearID},
                {"EducationLevelID ", entity.EducationLevelID},
                {"SchoolID", entity.SchoolID},
                {"Semester", entity.Semester}
            };

            ProcessedReportParameterRepository.Insert(dic, pr);
            ProcessedReportRepository.Insert(pr);
            ProcessedReportRepository.Save();
            return pr;
        }
        #endregion

        #region Tạo file báo cáo danh sách khen thưởng cấp 1
        /// <summary>
        /// Tạo file báo cáo danh sách khen thưởng cấp 1
        /// </summary>
        /// <param name="entity"></param>
        /// <returns></returns>
        public Stream CreatePupilRewardPrimaryReport(PupilRewardReportBO entity)
        {
            AcademicYear Aca = AcademicYearBusiness.Find(entity.AcademicYearID);
            string reportCode = SystemParamsInFile.REPORT_DS_HS_KHEN_THUONG_CAP1;
            ReportDefinition reportDef = ReportDefinitionBusiness.GetByCode(reportCode);
            string templatePath = ReportUtils.GetTemplatePath(reportDef);
            IVTWorkbook oBook = VTExport.OpenWorkbook(templatePath);

            //Lấy sheet template
            IVTWorksheet firstSheet = oBook.GetSheet(1);

            //Lấy template dòng đầu tiên trong một nhóm lớp
            IVTRange topRange = firstSheet.GetRange("A7", "G7");
            //Lấy template cho các dòng ở giữa trong một nhóm lớp
            IVTRange middleRange = firstSheet.GetRange("A8", "G8");
            //Lấy template cho các dòng ở cuối trong một nhóm lớp
            IVTRange bottomRange = firstSheet.GetRange("A9", "G9");
            //Lấy template cho các dòng bị bôi đỏ
            IVTRange RedRange = firstSheet.GetRange("A11", "G11");
            IVTRange RedRangetop = firstSheet.GetRange("A10", "G10");
            IVTRange RedRangebottom = firstSheet.GetRange("A13", "G13");
            AcademicYear academicYear = AcademicYearRepository.Find(entity.AcademicYearID);
            SchoolProfile school = SchoolProfileRepository.Find(entity.SchoolID);

            //Fill dữ liệu chung
            string schoolName = school.SchoolName.ToUpper();

            firstSheet.SetCellValue("A2", schoolName);

            //Lấy danh sách các khối
            List<EducationLevel> lstEducationLevel = new List<EducationLevel>();
            if (entity.EducationLevelID != 0)
            {
                lstEducationLevel.Add(EducationLevelRepository.Find(entity.EducationLevelID));
            }
            else
            {
                lstEducationLevel = EducationLevelBusiness.GetByGrade(SystemParamsInFile.APPLIED_LEVEL_PRIMARY).ToList();
            }

            //Lấy danh sách thông tin học sinh khen thưởng cấp 1
            Dictionary<string, object> dicPupilRanking = new Dictionary<string, object> 
            {
                {"AcademicYearID", entity.AcademicYearID}, 
                {"EducationLevelID", entity.EducationLevelID},
                {"SchoolID", entity.SchoolID},
                {"Semester", entity.Semester},
                {"AppliedLevel", entity.AppliedLevel}
            };
            IQueryable<PupilOfClass> listPupil = PupilOfClassBusiness.SearchBySchool(entity.SchoolID.Value, dicPupilRanking).Where(o => o.Status != SystemParamsInFile.PUPIL_STATUS_MOVED_TO_OTHER_CLASS).GetPupil(Aca, entity.Semester);
            IQueryable<VPupilRanking> iQPupilRanking = VPupilRankingBusiness.SearchBySchool(entity.SchoolID.Value, dicPupilRanking);
            IQueryable<PupilEmulation> iQPupilEmulation = PupilEmulationBusiness.SearchBySchool(entity.SchoolID.Value, dicPupilRanking);
            //List<PupilOfClassBO> listPupil = PupilOfClassBusiness.GetPupilOfClassInSemester(entity.AcademicYearID.Value, entity.SchoolID.Value, entity.EducationLevelID, entity.Semester.Value);


            // Lay tat ca trang thai
            List<PupilRankingBO> lstPupilRankingAllStatus =
                (from pc in PupilOfClassBusiness.SearchBySchool(entity.SchoolID.Value, dicPupilRanking)
                 join pe in iQPupilEmulation on new { pc.SchoolID, pc.AcademicYearID, pc.ClassID, pc.PupilID } equals
                                                                      new { pe.SchoolID, pe.AcademicYearID, pe.ClassID, pe.PupilID }
                 join pr in iQPupilRanking on new { pe.SchoolID, pe.AcademicYearID, pe.ClassID, pe.Semester, pe.PupilID } equals
                                                                      new { pr.SchoolID, pr.AcademicYearID, pr.ClassID, pr.Semester, pr.PupilID }
                 join cap in CapacityLevelBusiness.All
                 on pr.CapacityLevelID equals cap.CapacityLevelID
                 join con in ConductLevelBusiness.All
                 on pr.ConductLevelID equals con.ConductLevelID
                 select new PupilRankingBO
                 {
                     PupilRankingID = pr.PupilRankingID,
                     ClassID = pe.ClassID,
                     ClassOrderNumber = pc.ClassProfile.OrderNumber,
                     ClassName = pc.ClassProfile.DisplayName,
                     PupilID = pe.PupilID,
                     Name = pc.PupilProfile.Name,
                     FullName = pc.PupilProfile.FullName,
                     AverageMark = pr.AverageMark,
                     CapacityLevelID = pr.CapacityLevelID,
                     ConductLevelID = pr.ConductLevelID,
                     Semester = pe.Semester,
                     PupilEmulationID = pe.PupilEmulationID,
                     CapacityLevel = cap.CapacityLevel1,
                     ConductLevelName = con.Resolution,
                     HonourAchivementTypeResolution = pe.HonourAchivementType.Resolution,
                     OrderInClass = pc.OrderInClass,
                     EducationLevelID = pc.ClassProfile.EducationLevelID,
                     Status = pc.Status
                 }).ToList();

            List<PupilRankingBO> lstPupilRanking = (from u in lstPupilRankingAllStatus
                                                    where listPupil.Any(o => o.PupilID == u.PupilID && o.ClassID == u.ClassID)
                                                    select u).ToList();


            int firstRow = 7;
            string colOrder = "A";
            string colClass = "B";
            string colPupil = "C";
            string colCapacityLevel = "D";
            string colConductLevel = "E";
            string colPupilRanking = "F";

            //Tạo và fill dữ liệu vào các sheet tương ứng các khối
            foreach (EducationLevel educationLevel in lstEducationLevel)
            {
                //Tạo sheet
                IVTWorksheet sheet = oBook.CopySheetToLast(firstSheet, "G6");
                sheet.Name = educationLevel.Resolution;

                //Fill dữ liệu tiêu đề
                string title = "DANH SÁCH KHEN THƯỞNG " + educationLevel.Resolution.ToUpper()
                    + " NĂM HỌC " + academicYear.Year + " - " + (academicYear.Year + 1);
                sheet.SetCellValue("A4", title);

                int index = 0;

                //Lấy danh sách lớp có thông tin khen thưởng
                List<int> lstClass = (from s in lstPupilRanking
                                      where s.EducationLevelID == educationLevel.EducationLevelID
                                      orderby s.ClassOrderNumber, s.ClassName
                                      select s.ClassID.GetValueOrDefault()).Distinct().ToList();
                List<PupilRankingBO> lstPE = new List<PupilRankingBO>();
                List<PupilRankingBO> lstPETmp = null;
                // lay lai danh sach can fill sap xep theo lop tranh tinh trang lap du lieu
                foreach (int cp in lstClass)
                {
                    //Lấy thông tin học sinh trong lớp
                    lstPETmp = (from s in lstPupilRanking
                                where s.ClassID == cp
                                select s).OrderBy(x => x.OrderInClass).ThenBy(o => o.Name).ThenBy(x => x.FullName).ToList();
                    lstPE.AddRange(lstPETmp);
                }
                for (int i = 0; i < lstPE.Count; i++)
                {
                    int currentRow = firstRow + index;
                    index++;

                    PupilRankingBO pe = lstPE[i];

                    #region
                    //Copy template
                    if (i == lstPE.Count - 1)
                    {
                        if (pe.Status != SystemParamsInFile.PUPIL_STATUS_STUDYING && pe.Status != SystemParamsInFile.PUPIL_STATUS_GRADUATED)
                        {
                            sheet.CopyPasteSameRowHeigh(RedRangebottom, currentRow);
                        }
                        else
                        {

                            sheet.CopyPasteSameRowHeigh(bottomRange, currentRow);
                        }

                    }
                    else
                    {
                        if (i == 0)
                        {
                    #endregion
                            if (pe.Status != SystemParamsInFile.PUPIL_STATUS_STUDYING && pe.Status != SystemParamsInFile.PUPIL_STATUS_GRADUATED)
                            {
                                sheet.CopyPasteSameRowHeigh(RedRangetop, currentRow);
                            }
                            else
                            {

                                sheet.CopyPasteSameRowHeigh(middleRange, currentRow);
                            }
                            #region
                        }
                        else
                        {
                            if (pe.Status != SystemParamsInFile.PUPIL_STATUS_STUDYING && pe.Status != SystemParamsInFile.PUPIL_STATUS_GRADUATED)
                            {
                                sheet.CopyPasteSameRowHeigh(RedRange, currentRow);

                            }
                            else
                            {

                                sheet.CopyPasteSameRowHeigh(middleRange, currentRow);
                            }
                        }
                    }

                            #endregion

                    //if (index % 5 == 0)
                    //    {
                    //    sheet.CopyPasteSameRowHeigh(bottomRange, currentRow);
                    //    }
                    //    else
                    //    {
                    //        sheet.CopyPasteSameRowHeigh(middleRange, currentRow);
                    //    }

                    //Fill dữ liệu
                    if (pe.Status != SystemParamsInFile.PUPIL_STATUS_STUDYING && pe.Status != SystemParamsInFile.PUPIL_STATUS_GRADUATED)
                    {
                        sheet.SetCellValue(colOrder + currentRow, index);
                        sheet.SetCellValue(colClass + currentRow, pe.ClassName);
                        sheet.SetCellValue(colPupil + currentRow, pe.FullName);
                    }
                    else
                    {
                        sheet.SetCellValue(colOrder + currentRow, index);
                        sheet.SetCellValue(colClass + currentRow, pe.ClassName);
                        sheet.SetCellValue(colPupil + currentRow, pe.FullName);
                        sheet.SetCellValue(colCapacityLevel + currentRow, UtilsBusiness.ConvertCapacity(pe.CapacityLevel));
                        sheet.SetCellValue(colConductLevel + currentRow, UtilsBusiness.ConvertCapacity(pe.ConductLevelName));
                        sheet.SetCellValue(colPupilRanking + currentRow, UtilsBusiness.ConvertCapacity(pe.HonourAchivementTypeResolution));
                    }


                }

                //}
                if (lstPE.Count > 0)
                {
                    sheet.GetRange((firstRow + lstPE.Count - 1), 1, (firstRow + lstPE.Count - 1), 7).SetBorder(VTBorderStyle.Solid, VTBorderWeight.Medium, VTBorderIndex.EdgeBottom);
                    sheet.GetRange((firstRow + lstPE.Count - 1), 1, (firstRow + lstPE.Count - 1), 7).SetBorder(VTBorderStyle.Solid, VTBorderWeight.Thin, VTBorderIndex.EdgeRight);
                    sheet.GetRange((firstRow + lstPE.Count - 1), 1, (firstRow + lstPE.Count - 1), 7).SetBorder(VTBorderStyle.Solid, VTBorderWeight.Thin, VTBorderIndex.EdgeLeft);
                }


                sheet.FitAllColumnsOnOnePage = true;
            }

            //Xoá sheet template
            firstSheet.Delete();

            return oBook.ToStream();
        }


        public Stream CreatePupilRewardPrimaryReportTT30(PupilRewardReportBO objPupilRewardReport)
        {
            AcademicYear Aca = AcademicYearBusiness.Find(objPupilRewardReport.AcademicYearID);
            string reportCode = SystemParamsInFile.REPORT_DS_HS_KHEN_THUONG_TT30_CAP1;
            ReportDefinition reportDef = ReportDefinitionBusiness.GetByCode(reportCode);
            string templatePath = ReportUtils.GetTemplatePath(reportDef);
            IVTWorkbook oBook = VTExport.OpenWorkbook(templatePath);
            IVTWorksheet sheet = null;

            int semester = objPupilRewardReport.Semester.HasValue ? objPupilRewardReport.Semester.Value : -1;
            //Lấy sheet template
            IVTWorksheet firstSheet = oBook.GetSheet(1);

            AcademicYear academicYear = AcademicYearRepository.Find(objPupilRewardReport.AcademicYearID);
            SchoolProfile school = SchoolProfileRepository.Find(objPupilRewardReport.SchoolID);
            int partitionReward = UtilsBusiness.GetPartionId(school.SchoolProfileID, 20);
            //Lấy danh sách các khối
            List<EducationLevel> lstEducationLevel = new List<EducationLevel>();
            if (objPupilRewardReport.EducationLevelID != 0)
            {
                lstEducationLevel.Add(EducationLevelRepository.Find(objPupilRewardReport.EducationLevelID));
            }
            else
            {
                lstEducationLevel = EducationLevelBusiness.GetByGrade(SystemParamsInFile.APPLIED_LEVEL_PRIMARY).ToList();
            }
            int startRow = 7;
            //Danh sach hoc sinh tat ca cac lop trong truong
            Dictionary<string, object> dicPupilRanking = new Dictionary<string, object> 
            {
                {"AcademicYearID", academicYear.AcademicYearID},
                {"SchoolID", school.SchoolProfileID},
                {"AppliedLevel",  SystemParamsInFile.APPLIED_LEVEL_PRIMARY},
                {"EducationLevelID",objPupilRewardReport.EducationLevelID},
                {"ClassID",objPupilRewardReport.ClassID}
            };
            IQueryable<PupilOfClass> listPupil_Temp = PupilOfClassBusiness.SearchBySchool(school.SchoolProfileID, dicPupilRanking).AddCriteriaSemesterNoCheckStatus(academicYear, semester);
            List<PupilOfClassBO> listPupil = (from p in listPupil_Temp
                                              join q in ClassProfileBusiness.All on p.ClassID equals q.ClassProfileID
                                              join r in PupilProfileBusiness.All on p.PupilID equals r.PupilProfileID
                                              where q.IsActive.Value
                                              select new PupilOfClassBO
                                              {
                                                  PupilID = p.PupilID,
                                                  ClassID = p.ClassID,
                                                  ClassName = q.DisplayName,
                                                  Status = p.Status,
                                                  Genre = r.Genre,
                                                  EthnicID = r.EthnicID,
                                                  PupilFullName = r.FullName,
                                                  EducationLevelID = q.EducationLevelID,
                                                  Name = r.Name,
                                                  OrderInClass = p.OrderInClass,//order cua lop
                                                  OrderPupil = q.OrderNumber// order cua hoc sinh trong lop
                                              }).ToList();

            //lấy danh sách khen thưởng của tất cả học sinh trong trường
            //for qua danh sach hoc sinh de lauy thong tin co hoc sinh khen thuong
            List<RewardCommentFinal> listRewardComments = RewardCommentFinalBusiness.All.Where(p => p.LastDigitSchoolID == partitionReward && p.SchoolID == school.SchoolProfileID && p.SemesterID == semester && (p.ClassID == objPupilRewardReport.ClassID || !objPupilRewardReport.ClassID.HasValue || objPupilRewardReport.ClassID == 0)).ToList();
            List<RewardCommentFinalBO> lstRewardCommentFinalBO = new List<RewardCommentFinalBO>();
            PupilOfClassBO pupilOfClassObj = null;
            RewardCommentFinalBO rewardObj = null;
            RewardCommentFinal rewardFinalOBj = null;
            for (int i = 0; i < listPupil.Count; i++)
            {
                rewardObj = new RewardCommentFinalBO();
                pupilOfClassObj = listPupil[i];
                rewardFinalOBj = listRewardComments.Where(p => p.PupilID == pupilOfClassObj.PupilID && p.ClassID == pupilOfClassObj.ClassID).FirstOrDefault();
                if (rewardFinalOBj != null && !string.IsNullOrEmpty(rewardFinalOBj.RewardID))
                {
                    rewardObj.ClassID = pupilOfClassObj.ClassID;
                    rewardObj.ClassName = pupilOfClassObj.ClassName;
                    rewardObj.EducationLevelID = pupilOfClassObj.EducationLevelID;
                    rewardObj.PupilID = pupilOfClassObj.PupilID;
                    rewardObj.FullName = pupilOfClassObj.PupilFullName;
                    rewardObj.ShortName = pupilOfClassObj.Name;
                    rewardObj.OrderInClass = pupilOfClassObj.OrderInClass;
                    rewardObj.OrderClass = pupilOfClassObj.OrderPupil;
                    rewardObj.PupilStatus = pupilOfClassObj.Status;
                    rewardObj.RewardID = rewardFinalOBj.RewardID;
                }
                lstRewardCommentFinalBO.Add(rewardObj);
            }

            //lấy danh sách hình thức khen thưởng của trường
            List<RewardFinal> lstRewardFinal = RewardFinalBusiness.All.Where(r => r.SchoolID == school.SchoolProfileID).ToList();
            IDictionary<int, string> dicRewardFinal = new Dictionary<int, string>();
            for (int i = 0; i < lstRewardFinal.Count; i++)
            {
                dicRewardFinal.Add(lstRewardFinal[i].RewardFinalID, lstRewardFinal[i].RewardMode);
            }

            List<RewardCommentFinalBO> lstData = new List<RewardCommentFinalBO>();
            RewardCommentFinalBO objData = null;
            EducationLevel objEducationLevel = null;
            List<string> lstRewardID = new List<string>();
            string rewardMode = string.Empty;
            for (int i = 0; i < lstEducationLevel.Count; i++)
            {
                startRow = 7;
                objEducationLevel = lstEducationLevel[i];
                lstRewardID = new List<string>();
                rewardMode = string.Empty;
                //Tạo sheet
                sheet = oBook.CopySheetToLast(oBook.GetSheet(1));
                sheet.Name = objEducationLevel.Resolution;
                //Fill dữ liệu chung
                string schoolName = school.SchoolName.ToUpper();

                sheet.SetCellValue("A2", schoolName);
                //Fill dữ liệu tiêu đề
                sheet.SetCellValue("A2", Aca.SchoolProfile.SchoolName.ToUpper());

                string hk = "";
                if (semester == GlobalConstants.SEMESTER_OF_YEAR_FIRST)
                {
                    hk = "I";
                }
                else
                {
                    hk = "II";
                }

                string title = "DANH SÁCH KHEN THƯỞNG " + objEducationLevel.Resolution.ToUpper() + ", HỌC KỲ " + hk
                    + " NĂM HỌC " + academicYear.Year + " - " + (academicYear.Year + 1);
                sheet.SetCellValue("A4", title);
                //fill dữ liệu
                lstData = lstRewardCommentFinalBO.Where(p => p.EducationLevelID == objEducationLevel.EducationLevelID).OrderBy(p => p.OrderClass).ThenBy(p => p.ClassName).ThenBy(p => p.OrderInClass).ThenBy(p => SMAS.Business.Common.Utils.SortABC(p.ShortName + " " + p.FullName)).ToList();
                for (int j = 0; j < lstData.Count; j++)
                {
                    objData = lstData[j];
                    rewardMode = string.Empty;
                    lstRewardID = new List<string>();
                    sheet.SetCellValue(startRow, 1, j + 1);//STT
                    sheet.GetRange(startRow, 1, startRow, 1).SetBorder(VTBorderStyle.Solid, VTBorderWeight.Thin, VTBorderIndex.EdgeRight);
                    sheet.GetRange(startRow, 1, startRow, 1).SetBorder(VTBorderStyle.Solid, VTBorderWeight.Thin, VTBorderIndex.EdgeLeft);

                    sheet.SetCellValue(startRow, 2, objData.ClassName);//Lop
                    sheet.GetRange(startRow, 2, startRow, 2).SetBorder(VTBorderStyle.Solid, VTBorderWeight.Thin, VTBorderIndex.EdgeRight);
                    sheet.GetRange(startRow, 2, startRow, 2).SetBorder(VTBorderStyle.Solid, VTBorderWeight.Thin, VTBorderIndex.EdgeLeft);

                    sheet.SetCellValue(startRow, 3, objData.FullName);//Ho ten
                    sheet.GetRange(startRow, 3, startRow, 3).SetBorder(VTBorderStyle.Solid, VTBorderWeight.Thin, VTBorderIndex.EdgeRight);
                    sheet.GetRange(startRow, 3, startRow, 3).SetBorder(VTBorderStyle.Solid, VTBorderWeight.Thin, VTBorderIndex.EdgeLeft);

                    if (!string.IsNullOrEmpty(objData.RewardID))
                    {
                        lstRewardID = objData.RewardID.Split(new string[] { "," }, StringSplitOptions.RemoveEmptyEntries).ToList();
                    }

                    for (int k = 0; k < lstRewardID.Count; k++)
                    {
                        rewardMode += dicRewardFinal[int.Parse(lstRewardID[k])] + ";";
                    }
                    sheet.SetCellValue(startRow, 4, rewardMode);//hinh thuc khen thuong
                    sheet.GetRange(startRow, 4, startRow, 4).WrapText();
                    sheet.GetRange(startRow, 4, startRow, 4).SetBorder(VTBorderStyle.Solid, VTBorderWeight.Thin, VTBorderIndex.EdgeRight);
                    sheet.GetRange(startRow, 4, startRow, 4).SetBorder(VTBorderStyle.Solid, VTBorderWeight.Thin, VTBorderIndex.EdgeLeft);

                    sheet.GetRange(startRow, 1, startRow, 5).SetBorder(VTBorderStyle.Dotted, VTBorderWeight.Thin, VTBorderIndex.EdgeBottom);
                    sheet.GetRange(startRow, 5, startRow, 5).SetBorder(VTBorderStyle.Solid, VTBorderWeight.Thin, VTBorderIndex.EdgeRight);
                    if ((j + 1) == lstData.Count)
                    {
                        sheet.GetRange(startRow, 1, startRow, 5).SetBorder(VTBorderStyle.Solid, VTBorderWeight.Thin, VTBorderIndex.EdgeBottom);
                    }

                    if (objData.PupilStatus != GlobalConstants.PUPIL_STATUS_STUDYING)
                    {
                        sheet.GetRange(startRow, 1, startRow, 5).SetFontStyle(false, System.Drawing.Color.Red, false, null, true, false);
                    }

                    startRow++;
                }
                // sheet.ProtectSheet();
            }
            oBook.GetSheet(1).Delete();
            return oBook.ToStream();
        }

        #endregion


        #region Lấy mảng băm cho các tham số đầu vào của danh sách khen thưởng
        public string GetHashKey(PupilRewardReportBO entity)
        {
            IDictionary<string, object> dic = new Dictionary<string, object> {
                {"AcademicYearID", entity.AcademicYearID},
                {"EducationLevelID ", entity.EducationLevelID},
                {"SchoolID", entity.SchoolID},
                {"Semester", entity.Semester}, 
                {"AppliedLevel", entity.AppliedLevel},
                {"ClassID",entity.ClassID}
            };
            return ReportUtils.GetHashKey(dic);
        }
        #endregion

        #region Lưu lại thông tin danh sách khen thưởng
        public ProcessedReport InsertPupilReward(PupilRewardReportBO entity, Stream data)
        {
            string reportCode = SystemParamsInFile.REPORT_DS_HS_KHEN_THUONG;
            ReportDefinition reportDef = ReportDefinitionBusiness.GetByCode(reportCode);
            ProcessedReport pr = new ProcessedReport();
            pr.ReportCode = reportCode;
            pr.ProcessedDate = DateTime.Now;
            pr.InputParameterHashKey = GetHashKey(entity);
            pr.ReportData = ReportUtils.Compress(data);

            //Tạo tên file
            string outputNamePattern = reportDef.OutputNamePattern;
            string semester = ReportUtils.ConvertSemesterForReportName(entity.Semester.Value);
            string educationLevel = "";
            EducationLevel edu = EducationLevelBusiness.Find(entity.EducationLevelID);
            if (edu == null)
            {
                educationLevel = "";
            }
            else
            {
                educationLevel = edu.Resolution;
            }
            string appliedLevel = ReportUtils.ConvertAppliedLevelForReportName(entity.AppliedLevel.Value);
            outputNamePattern = outputNamePattern.Replace("[SchoolLevel]", appliedLevel);
            outputNamePattern = outputNamePattern.Replace("[EducationLevel]", educationLevel);
            outputNamePattern = outputNamePattern.Replace("[Semester]", semester);
            outputNamePattern = outputNamePattern.Replace("__", "_");
            pr.ReportName = ReportUtils.RemoveSpecialCharacters(outputNamePattern) + "." + reportDef.OutputFormat;

            IDictionary<string, object> dic = new Dictionary<string, object> {
                {"AcademicYearID", entity.AcademicYearID},
                {"EducationLevelID ", entity.EducationLevelID},
                {"SchoolID", entity.SchoolID},
                {"Semester", entity.Semester}
            };
            ProcessedReportParameterRepository.Insert(dic, pr);
            ProcessedReportRepository.Insert(pr);
            ProcessedReportRepository.Save();
            return pr;
        }
        #endregion

        #region Lấy danh sách khen thưởng được cập nhật mới nhất
        public ProcessedReport GetPupilReward(PupilRewardReportBO entity)
        {
            string reportCode = SystemParamsInFile.REPORT_DS_HS_KHEN_THUONG;
            string inputParameterHashKey = GetHashKey(entity);
            return ProcessedReportBusiness.GetProcessedReport(reportCode, inputParameterHashKey);
        }
        #endregion

        #region Lưu lại thông tin danh sách khen thưởng
        /// <summary>
        /// Lưu lại thông tin danh sách khen thưởng
        /// </summary>
        /// <param name="entity"></param>
        /// <returns></returns>
        public Stream CreatePupilReward(PupilRewardReportBO entity)
        {
            string reportCode = SystemParamsInFile.REPORT_DS_HS_KHEN_THUONG;
            ReportDefinition reportDef = ReportDefinitionBusiness.GetByCode(reportCode);
            string templatePath = ReportUtils.GetTemplatePath(reportDef);
            IVTWorkbook oBook = VTExport.OpenWorkbook(templatePath);

            //Lấy sheet template
            IVTWorksheet firstSheet = oBook.GetSheet(1);

            //Lấy template dòng đầu tiên trong một nhóm lớp
            IVTRange topRange = firstSheet.GetRange("A7", "G7");
            //Lấy template cho các dòng ở giữa trong một nhóm lớp
            IVTRange middleRange = firstSheet.GetRange("A8", "G8");
            //Lấy template cho các dòng ở cuối trong một nhóm lớp
            IVTRange bottomRange = firstSheet.GetRange("A9", "G9");

            AcademicYear academicYear = AcademicYearRepository.Find(entity.AcademicYearID);
            SchoolProfile school = SchoolProfileRepository.Find(entity.SchoolID);

            //Fill dữ liệu chung
            string schoolName = school.SchoolName.ToUpper();

            firstSheet.SetCellValue("A2", schoolName);

            //Lấy danh sách các khối
            List<EducationLevel> lstEducationLevel = new List<EducationLevel>();
            if (entity.EducationLevelID != 0)
            {
                lstEducationLevel.Add(EducationLevelRepository.Find(entity.EducationLevelID));
            }
            else
            {
                lstEducationLevel = EducationLevelBusiness.GetByGrade(entity.AppliedLevel.Value).ToList();
            }

            //Lấy danh sách thông tin học sinh khen thưởng
            Dictionary<string, object> dicPupilRanking = new Dictionary<string, object> 
            {
                {"AcademicYearID", entity.AcademicYearID},
                {"SchoolID", entity.SchoolID},
                {"EducationLevelID",entity.EducationLevelID},
                {"AppliedLevel", entity.AppliedLevel},
                {"ClassID",entity.ClassID}
            };
            IQueryable<PupilOfClass> iqueryPOCAll = PupilOfClassBusiness.SearchBySchool(entity.SchoolID.Value, dicPupilRanking);
            IQueryable<PupilOfClass> iqueryPupil = iqueryPOCAll.Where(o => o.AssignedDate == iqueryPOCAll.Where(u => u.PupilID == o.PupilID && u.ClassID == o.ClassID).Select(u => u.AssignedDate).Max());

            List<int> lstPupilID = iqueryPupil.Select(p => p.PupilID).Distinct().ToList();
            dicPupilRanking.Add("lstPupilID", lstPupilID);
            IQueryable<VPupilRanking> iQPupilRanking = VPupilRankingBusiness.SearchBySchool(entity.SchoolID.Value, dicPupilRanking);
            IQueryable<PupilEmulation> iQPupilEmulation = PupilEmulationBusiness.SearchBySchool(entity.SchoolID.Value, dicPupilRanking);

            if (entity.chkChecked)
            {
                //loc ra nhung hoc sinh dat danh hieu HSG/HSTT ca nam
                List<int> lsttmp = iQPupilEmulation.Where(p => p.Semester == GlobalConstants.SEMESTER_OF_YEAR_ALL).Select(p => p.PupilID).Distinct().ToList();
                iQPupilRanking = iQPupilRanking.Where(p => p.Semester == entity.Semester && !lsttmp.Contains(p.PupilID));
                iQPupilEmulation = iQPupilEmulation.Where(p => p.Semester == entity.Semester && !lsttmp.Contains(p.PupilID));
            }
            else
            {
                iQPupilRanking = iQPupilRanking.Where(p => p.Semester == entity.Semester);
                iQPupilEmulation = iQPupilEmulation.Where(p => p.Semester == entity.Semester);
            }

            List<PupilRankingBO> lstPupilRanking =
                (from pc in iqueryPupil
                 join pe in iQPupilEmulation on new { pc.SchoolID, pc.AcademicYearID, pc.ClassID, pc.PupilID } equals
                                                                      new { pe.SchoolID, pe.AcademicYearID, pe.ClassID, pe.PupilID }
                 join pr in iQPupilRanking on new { pe.SchoolID, pe.AcademicYearID, pe.ClassID, pe.Semester, pe.PupilID } equals
                                                                      new { pr.SchoolID, pr.AcademicYearID, pr.ClassID, pr.Semester, pr.PupilID }
                 join cap in CapacityLevelBusiness.All
                 on pr.CapacityLevelID equals cap.CapacityLevelID
                 join con in ConductLevelBusiness.All on pr.ConductLevelID equals con.ConductLevelID into lcd
                 from cd in lcd.DefaultIfEmpty()
                 select new PupilRankingBO
                 {
                     PupilRankingID = pr.PupilRankingID,
                     ClassID = pe.ClassID,
                     ClassName = pc.ClassProfile.DisplayName,
                     PupilID = pe.PupilID,
                     FullName = pc.PupilProfile.FullName,
                     AverageMark = pr.AverageMark,
                     CapacityLevelID = pr.CapacityLevelID,
                     ConductLevelID = pr.ConductLevelID,
                     Semester = pe.Semester,
                     PupilEmulationID = pe.PupilEmulationID,
                     CapacityLevel = cap.CapacityLevel1,
                     ConductLevelName = cd.Resolution,
                     HonourAchivementTypeResolution = pe.HonourAchivementType.Resolution,
                     OrderInClass = pc.OrderInClass,
                     EducationLevelID = pc.ClassProfile.EducationLevelID,
                     ClassOrderNumber = pc.ClassProfile.OrderNumber
                 }).ToList();

            List<PupilOfClass> listPupilAll = iqueryPOCAll.ToList();
            List<PupilOfClass> listPupil = iqueryPupil.ToList();
            int firstRow = 7;
            string colOrder = "A";
            string colClass = "B";
            string colPupil = "C";
            string colPupilRanking = "D";
            string colCapacityLevel = "E";
            string colConductLevel = "F";
            string colHonourAchivementType = "G";
            //Tạo và fill dữ liệu vào các sheet tương ứng các khối
            foreach (EducationLevel educationLevel in lstEducationLevel)
            {
                //Tạo sheet
                IVTWorksheet sheet = oBook.CopySheetToLast(firstSheet, "H6");
                sheet.Name = educationLevel.Resolution;

                //Fill dữ liệu tiêu đề
                string title;
                if (entity.Semester == GlobalConstants.SEMESTER_OF_YEAR_FIRST)
                {
                    title = "DANH SÁCH KHEN THƯỞNG " + educationLevel.Resolution.ToUpper()
                   + " HỌC KỲ I NĂM HỌC " + academicYear.Year + " - " + (academicYear.Year + 1);
                    sheet.SetCellValue("A4", title);
                }
                else if (entity.Semester == GlobalConstants.SEMESTER_OF_YEAR_SECOND)
                {
                    title = "DANH SÁCH KHEN THƯỞNG " + educationLevel.Resolution.ToUpper()
                   + " HỌC KỲ II NĂM HỌC " + academicYear.Year + " - " + (academicYear.Year + 1);
                    sheet.SetCellValue("A4", title);
                }
                else
                {
                    title = "DANH SÁCH KHEN THƯỞNG " + educationLevel.Resolution.ToUpper()
                   + " NĂM HỌC " + academicYear.Year + " - " + (academicYear.Year + 1);
                    sheet.SetCellValue("A4", title);
                }

                IVTRange range = firstSheet.GetRange("A7", "H7");
                int index = 0;

                //Lấy danh sách lớp có thông tin khen thưởng
                List<int?> listClassByRole = ClassProfileBusiness.getClassByAccountRole(entity.AcademicYearID.Value, entity.SchoolID.Value, educationLevel.EducationLevelID, null,
                                                                        entity.UserAccountID, entity.IsSuperVisingDept, entity.IsSubSuperVisingDept).ToList().Select(o => (int?)o.ClassProfileID).ToList();
                List<int> lstClass = (from s in lstPupilRanking
                                      where s.EducationLevelID == educationLevel.EducationLevelID
                                      && listClassByRole.Contains(s.ClassID)
                                      orderby s.ClassOrderNumber.HasValue ? s.ClassOrderNumber : 0, s.ClassName
                                      select s.ClassID.GetValueOrDefault()).Distinct().ToList();

                if (entity.ClassID != 0 && entity.ClassID != null)
                {
                    lstClass = lstClass.Where(o => o == entity.ClassID).ToList();
                }

                List<PupilRankingBO> lstPE = new List<PupilRankingBO>();
                List<PupilRankingBO> lstPEtmp = null;
                //Fill dữ liệu học sinh cho từng lớp
                foreach (int cp in lstClass)
                {
                    //Lấy thông tin học sinh trong lớp
                    lstPEtmp = (from s in lstPupilRanking
                                where s.ClassID == cp
                                select s).OrderBy(x => x.OrderInClass).ThenBy(x => x.FullName).ToList();
                    lstPE.AddRange(lstPEtmp);
                }
                for (int i = 0; i < lstPE.Count; i++)
                {
                    int cp = lstPE[i].ClassID.Value;
                    int currentRow = firstRow + index;
                    index++;
                    //Copy row style
                    sheet.CopyPasteSameRowHeigh(range, currentRow);
                    if (index % 5 == 0)
                    {
                        sheet.CopyPaste(firstSheet.GetRange("A9", "H9"), currentRow, 1, true);
                    }
                    if (index == lstPE.Count)
                    {
                        sheet.CopyPaste(firstSheet.GetRange("A9", "H9"), currentRow, 1, true);
                    }
                    PupilRankingBO pe = lstPE[i];
                    PupilOfClass poc = listPupilAll.Where(o => o.ClassID == cp && o.PupilID == pe.PupilID).FirstOrDefault();
                    if (poc != null)
                    {
                        pe.Status = poc.Status;
                        if (entity.Semester == SystemParamsInFile.SEMESTER_OF_YEAR_FIRST)
                        {
                            //Fill dữ liệu    
                            if (pe.Status == SystemParamsInFile.PUPIL_STATUS_STUDYING || pe.Status == SystemParamsInFile.PUPIL_STATUS_GRADUATED)
                            {
                                sheet.SetCellValue(colOrder + currentRow, index);

                                sheet.SetCellValue(colClass + currentRow, pe.ClassName);
                                sheet.SetCellValue(colPupil + currentRow, pe.FullName);

                                sheet.SetCellValue(colPupilRanking + currentRow, pe.AverageMark);
                                sheet.SetCellValue(colCapacityLevel + currentRow, UtilsBusiness.ConvertCapacity(pe.CapacityLevel));
                                sheet.SetCellValue(colConductLevel + currentRow, UtilsBusiness.ConvertCapacity(pe.ConductLevelName));
                                sheet.SetCellValue(colHonourAchivementType + currentRow, UtilsBusiness.ConvertCapacity(pe.HonourAchivementTypeResolution));
                            }
                            else
                            {
                                if (index % 5 == 0 || index == lstPE.Count())
                                {
                                    sheet.CopyPaste(firstSheet.GetRange("A11", "H11"), currentRow, 1, true);
                                }
                                else
                                {
                                    sheet.CopyPaste(firstSheet.GetRange("A8", "H8"), currentRow, 1, true);
                                }
                                if (poc.EndDate >= academicYear.FirstSemesterEndDate)
                                {
                                    sheet.SetCellValue(colOrder + currentRow, index);
                                    sheet.SetCellValue(colClass + currentRow, pe.ClassName);
                                    sheet.SetCellValue(colPupil + currentRow, pe.FullName);
                                    sheet.SetCellValue(colPupilRanking + currentRow, pe.AverageMark);
                                    sheet.SetCellValue(colCapacityLevel + currentRow, UtilsBusiness.ConvertCapacity(pe.CapacityLevel));
                                    sheet.SetCellValue(colConductLevel + currentRow, UtilsBusiness.ConvertCapacity(pe.ConductLevelName));
                                    sheet.SetCellValue(colHonourAchivementType + currentRow, UtilsBusiness.ConvertCapacity(pe.HonourAchivementTypeResolution));
                                }
                                else
                                {
                                    sheet.SetCellValue(colOrder + currentRow, index);
                                    sheet.SetCellValue(colClass + currentRow, pe.ClassName);
                                    sheet.SetCellValue(colPupil + currentRow, pe.FullName);
                                }
                            }
                        }
                        if (entity.Semester == SystemParamsInFile.SEMESTER_OF_YEAR_SECOND || entity.Semester == SystemParamsInFile.SEMESTER_OF_YEAR_ALL)
                        {
                            if (pe.Status == SystemParamsInFile.PUPIL_STATUS_STUDYING || pe.Status == SystemParamsInFile.PUPIL_STATUS_GRADUATED)
                            {
                                sheet.SetCellValue(colOrder + currentRow, index);

                                sheet.SetCellValue(colClass + currentRow, pe.ClassName);
                                sheet.SetCellValue(colPupil + currentRow, pe.FullName);

                                sheet.SetCellValue(colPupilRanking + currentRow, pe.AverageMark);
                                sheet.SetCellValue(colCapacityLevel + currentRow, UtilsBusiness.ConvertCapacity(pe.CapacityLevel));
                                sheet.SetCellValue(colConductLevel + currentRow, UtilsBusiness.ConvertCapacity(pe.ConductLevelName));
                                sheet.SetCellValue(colHonourAchivementType + currentRow, UtilsBusiness.ConvertCapacity(pe.HonourAchivementTypeResolution));
                            }
                            else
                            {
                                if (index % 5 == 0 || index == lstPE.Count())
                                {
                                    sheet.CopyPaste(firstSheet.GetRange("A11", "H11"), currentRow, 1, true);
                                }
                                else
                                {
                                    sheet.CopyPaste(firstSheet.GetRange("A8", "H8"), currentRow, 1, true);
                                }
                                if (poc.EndDate >= academicYear.SecondSemesterEndDate)
                                {
                                    sheet.SetCellValue(colOrder + currentRow, index);
                                    sheet.SetCellValue(colClass + currentRow, pe.ClassName);
                                    sheet.SetCellValue(colPupil + currentRow, pe.FullName);
                                    sheet.SetCellValue(colPupilRanking + currentRow, pe.AverageMark);
                                    sheet.SetCellValue(colCapacityLevel + currentRow, UtilsBusiness.ConvertCapacity(pe.CapacityLevel));
                                    sheet.SetCellValue(colConductLevel + currentRow, UtilsBusiness.ConvertCapacity(pe.ConductLevelName));
                                    sheet.SetCellValue(colHonourAchivementType + currentRow, UtilsBusiness.ConvertCapacity(pe.HonourAchivementTypeResolution));
                                }
                                else
                                {
                                    sheet.SetCellValue(colOrder + currentRow, index);
                                    sheet.SetCellValue(colClass + currentRow, pe.ClassName);
                                    sheet.SetCellValue(colPupil + currentRow, pe.FullName);
                                }
                            }
                        }
                    }
                }
                sheet.FitAllColumnsOnOnePage = true;
            }
            //Xoá sheet template
            firstSheet.Delete();
            return oBook.ToStream();
        }
        #endregion
    }
}
