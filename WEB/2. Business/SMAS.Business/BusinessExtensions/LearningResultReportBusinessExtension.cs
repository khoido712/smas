﻿/**
 * The Viettel License
 *
 * Copyright 2012 Viettel Telecom. All rights reserved.
 * VIETTEL PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */

/** 
* @author  
* @version $Revision: $
*/

using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Linq.Expressions;
using System.Web;
using log4net;

using SMAS.DAL.Repository;
using SMAS.DAL.IRepository;
using SMAS.Business.IBusiness;
using SMAS.Models.Models;
using SMAS.Business.Common;
using SMAS.Business.BusinessObject;
using System.Transactions;
using System.IO;
using SMAS.VTUtils.Excel.Export;
using System.Configuration;

namespace SMAS.Business.Business
{
    public partial class LearningResultReportBusiness
    {
        private int firstColumn = 1;
        private int lastColumn = 24;
        private int firstRow = 2;
        public string GetHashKey(IDictionary<string, object> dic)
        {
            return ReportUtils.GetHashKey(dic);
        }

        #region Phieu lien lac tieu hoc
        public ProcessedReport GetLearningResultCertificate(IDictionary<string, object> dic)
        {
            string reportCode = SystemParamsInFile.REPORT_LEARNING_RESULT_CERTIFICATE;
            string inputParameterHashKey = GetHashKey(dic);
            return ProcessedReportBusiness.GetProcessedReport(reportCode, inputParameterHashKey);
        }

        public Stream CreateLearningResultCertificate(IDictionary<string, object> dic)
        {
            int academicYearId = Utils.GetInt(dic["AcademicYearID"]);
            int schoolId = Utils.GetInt(dic["SchoolID"]);
            int partition = UtilsBusiness.GetPartionId(schoolId);
            int appliedLevel = Utils.GetInt(dic["AppliedLevel"]);
            int educationLevel = Utils.GetInt(dic["EducationLevelID"]);
            int classId = Utils.GetInt(dic["ClassID"]);
            string PupilCode = Utils.GetString(dic["PupilCode"]);
            string PupilName = Utils.GetString(dic["PupilName"]);
            int semester = Utils.GetInt(dic["Semester"]);
            List<int> lstPocId = null;
            if (dic.ContainsKey("ListPupilOfClassID"))
            {
                lstPocId = (List<int>)dic["ListPupilOfClassID"];
            }


            //Lấy đường dẫn báo cáo
            string reportCode = SystemParamsInFile.REPORT_LEARNING_RESULT_CERTIFICATE;
            ReportDefinition rp = ReportDefinitionBusiness.GetByCode(reportCode);
            string templateName = rp.TemplateName;
            string templatePath = SystemParamsInFile.TEMPLATE_FOLDER + rp.TemplateDirectory + "/" + templateName;

            //Khởi tạo
            IVTWorkbook oBook = VTExport.OpenWorkbook(templatePath);

            //Lấy ra sheet 
            IVTWorksheet sheet1 = oBook.GetSheet(1);
            IVTWorksheet sheet2 = oBook.GetSheet(2);

            //Fill thong tin chung
            AcademicYear ay = AcademicYearBusiness.Find(academicYearId);
            SchoolProfile school = SchoolProfileBusiness.Find(schoolId);

            string schoolName = school.SchoolName.ToUpper();
            string supervisingDeptName = UtilsBusiness.GetSupervisingDeptName(school.SchoolProfileID, appliedLevel).ToUpper();
            string provinceName = (school.District != null ? school.District.DistrictName : "");
            string strAcademicYear = "Năm học " + ay.DisplayTitle;
            if (semester == GlobalConstants.SEMESTER_OF_YEAR_FIRST)
            {
                strAcademicYear = "Học kỳ I - " + strAcademicYear;
            }
            else if (semester == GlobalConstants.SEMESTER_OF_YEAR_SECOND)
            {
                strAcademicYear = "Học kỳ II - " + strAcademicYear;
            }
            DateTime date = DateTime.Now;
            string day = "Ngày " + date.Day.ToString() + " tháng " + date.Month.ToString() + " năm " + date.Year.ToString();
            string headMaster = school.TrainingType.Resolution == "GDTX" ? "GIÁM ĐỐC": "HIỆU TRƯỞNG";
            sheet1.SetCellValue("A1", supervisingDeptName);
            sheet1.SetCellValue("A2", schoolName);
            sheet1.SetCellValue("G5", strAcademicYear);
            sheet1.SetCellValue("J10", schoolName);
            sheet1.SetCellValue("U10", provinceName);
            sheet1.SetCellValue("J22", day);
            sheet1.SetCellValue("J23", headMaster);
            sheet1.SetCellValue("J27", school.HeadMasterName);
            

            //Lay range mau cho 1 hoc sinh
            IVTRange templateHeaderRange = sheet1.GetRange(1, firstColumn, 10, lastColumn);
            IVTRange templateContentRange = sheet1.GetRange(12, firstColumn, 27, lastColumn);

            //Lay danh sach hoc sinh
            IDictionary<string, object> tmpDic = new Dictionary<string, object>();
            tmpDic["EducationLevelID"] = educationLevel;
            tmpDic["ClassID"] = classId;
            tmpDic["SchoolID"] = schoolId;
            tmpDic["AcademicYearID"] = academicYearId;
            tmpDic["AppliedLevel"] = appliedLevel;
            tmpDic["PupilCode"] = PupilCode;
            tmpDic["FullName"] = PupilName;
            List<PupilOfClass> lstPoc = PupilOfClassBusiness.Search(dic)
                .Where(o => lstPocId.Contains(o.PupilOfClassID))
                .OrderBy(o => o.ClassProfile.OrderNumber.HasValue ? o.ClassProfile.OrderNumber : 0)
                .ThenBy(o => o.ClassProfile.DisplayName)
                .ThenBy(u => u.OrderInClass)
                .ThenBy(o => o.PupilProfile.Name)
                .ThenBy(o => o.PupilProfile.FullName).ToList();
            List<int> lstPupilID = lstPoc.Select(p => p.PupilID).Distinct().ToList();

            //Lay danh sach lop
            List<ClassProfile> lstClass = lstPoc.Select(o => o.ClassProfile).ToList();

            List<int> lstHeadTeacherId = lstClass.Select(o => o.HeadTeacherID.HasValue ? o.HeadTeacherID.Value : 0).ToList();

            List<Employee> lstHeadTeacher = EmployeeBusiness.All.Where(o => o.SchoolID == schoolId && lstHeadTeacherId.Contains(o.EmployeeID)).ToList();

            //Danh sach mon hoc
            tmpDic = new Dictionary<string, object>();
            tmpDic["SchoolID"] = schoolId;
            tmpDic["AcademicYearID"] = academicYearId;
            tmpDic["EducationLevelID"] = educationLevel;
            tmpDic["Semester"] = semester;

            List<ClassSubject> lstClassSubjectEdu = ClassSubjectBusiness.SearchBySchool(schoolId, tmpDic).OrderBy(o => o.IsCommenting).ThenBy(o => o.SubjectCat.OrderInSubject).ThenBy(o => o.SubjectCat.DisplayName).ToList();
            
            //Lay thong tin diem TBM
            tmpDic = new Dictionary<string, object>();
            tmpDic["SchoolID"] = schoolId;
            tmpDic["AcademicYearID"] = academicYearId;
            tmpDic["AppliedLevel"] = appliedLevel;
            tmpDic["EducationLevelID"] = educationLevel;
            tmpDic["Semester"] = semester;

            bool isMovedHistory = UtilsBusiness.IsMoveHistory(ay);
            List<SummedUpRecordBO> lstSummedUpRecord;
            if (isMovedHistory)
            {
                lstSummedUpRecord = SummedUpRecordHistoryBusiness.SearchBySchool(schoolId, tmpDic)
                    .Where(o => o.PeriodID == null && lstPupilID.Contains(o.PupilID))
                    .Select(o => new SummedUpRecordBO()
                    {
                        ClassID = o.ClassID,
                        PupilID = o.PupilID,
                        SubjectID = o.SubjectID,
                        SummedUpMark = o.SummedUpMark,
                        JudgementResult = o.JudgementResult,
                        Semester = o.Semester
                    }).ToList();

            }
            else
            {
                lstSummedUpRecord = SummedUpRecordBusiness.SearchBySchool(schoolId, tmpDic)
                    .Where(o => o.PeriodID == null && lstPupilID.Contains(o.PupilID))
                    .Select(o => new SummedUpRecordBO()
                    {
                        ClassID = o.ClassID,
                        PupilID = o.PupilID,
                        SubjectID = o.SubjectID,
                        SummedUpMark = o.SummedUpMark,
                        JudgementResult = o.JudgementResult,
                        Semester = o.Semester
                    }).ToList();
            }

            //hoc luc, hanh kiem...
            tmpDic = new Dictionary<string, object>();
            tmpDic["AcademicYearID"] = academicYearId;
            tmpDic["SchoolID"] = schoolId;
            tmpDic["AppliedLevel"] = appliedLevel;
            tmpDic["EducationLevelID"] = educationLevel;
            tmpDic["Semester"] = semester;

            List<PupilRankingBO> lstPupilRanking = (from vpr in VPupilRankingBusiness.SearchBySchool(schoolId, tmpDic)
                                                    join co in ConductLevelBusiness.All on vpr.ConductLevelID equals co.ConductLevelID
                                                    into lco from lc in lco.DefaultIfEmpty()
                                                    join ca in CapacityLevelBusiness.All on vpr.CapacityLevelID equals ca.CapacityLevelID
                                                    into lca from lca1 in lca.DefaultIfEmpty()
                                                    where lstPupilID.Contains(vpr.PupilID)
                                                    select new PupilRankingBO
                                                    {
                                                        PupilID = vpr.PupilID,
                                                        ClassID = vpr.ClassID,
                                                        AverageMark = vpr.AverageMark,
                                                        ConductLevelName = lc.Resolution,
                                                        CapacityLevel = lca1.CapacityLevel1
                                                    }).ToList();

            //Lay danh hieu
            tmpDic = new Dictionary<string, object>();
            tmpDic["SchoolID"] = schoolId;
            tmpDic["AcademicYearID"] = academicYearId;
            tmpDic["Semester"] = semester;
            List<PupilEmulation> lstPe = PupilEmulationBusiness.SearchBySchool(schoolId, tmpDic).ToList();

            //Lay thong tin mon chuyen
            tmpDic = new Dictionary<string, object>();
            tmpDic["SchoolID"] = schoolId;
            tmpDic["AcademicYearID"] = academicYearId;

            List<RegisterSubjectSpecialize> lstRss = RegisterSubjectSpecializeBusiness.Search(tmpDic)
                .Where(o => o.SubjectTypeID == 0).ToList();


            //Fill du lieu
            int pocStartRow = firstRow;
            List<ClassSubject> lstClassSubjectPupil;
            List<SummedUpRecordBO> lstSummedUpRecordBOPupil;
            PupilRankingBO pupilRanking;
            PupilEmulation pupilEmulation;
            RegisterSubjectSpecialize rss;
            Employee headTeacher;
            for (int i = 0; i < lstPoc.Count; i++)
            {
                PupilOfClass poc = lstPoc[i];
                //Lay danh sach mon hoc
                lstClassSubjectPupil = lstClassSubjectEdu.Where(o => o.ClassID == poc.ClassID).ToList();
                //Lay danh sach diem TBM
                lstSummedUpRecordBOPupil = lstSummedUpRecord.Where(o => o.ClassID == poc.ClassID && o.PupilID == poc.PupilID).ToList();
                //Lay PupilRanking
                pupilRanking = lstPupilRanking.Where(o => o.ClassID == poc.ClassID && o.PupilID == poc.PupilID).FirstOrDefault();
                //Lay danh hieu
                pupilEmulation = lstPe.Where(o => o.ClassID == poc.ClassID && o.PupilID == poc.PupilID).FirstOrDefault();
                //Lay mon chuyen
                rss=lstRss.Where(o=>o.PupilID == poc.PupilID && o.ClassID==poc.ClassID).FirstOrDefault();
                //Lay gvcn
                headTeacher = lstHeadTeacher.Where(o => o.EmployeeID == poc.ClassProfile.HeadTeacherID).FirstOrDefault();

                //Fill data for header
                FillHeader(pocStartRow, sheet2, templateHeaderRange, poc, lstClassSubjectPupil, rss);
                //Fill ket qua hoc tap
                FillLearningResult(pocStartRow, sheet2, templateContentRange, lstClassSubjectPupil, lstSummedUpRecordBOPupil, pupilRanking, pupilEmulation, headTeacher);

                pocStartRow += 33;
                sheet2.SetBreakPage(pocStartRow);
            }

            sheet2.SetPrintArea("$A2:$X" + (pocStartRow - 1));
            sheet2.FitAllColumnsOnOnePage = true;


            ////Xoa sheet dau tien

            sheet1.Delete();

            return oBook.ToStream();
        }

        private void FillHeader(int pocStartRow, IVTWorksheet sheet, IVTRange template, PupilOfClass poc, List<ClassSubject> lstClassSubject, RegisterSubjectSpecialize rss)
        {
            sheet.CopyPasteSameSize(template, pocStartRow, 1);
            VTVector vtStart = new VTVector(pocStartRow, firstColumn);
            VTVector vtName = new VTVector(6, 13);
            VTVector vtBirthDay = new VTVector(7, 10);
            VTVector vtBirthPlace = new VTVector(7, 18);
            VTVector vtClassName = new VTVector(8, 10);
            VTVector vtClassStage = new VTVector(8, 15);
            VTVector vtSpecialSubject = new VTVector(8, 21);

            sheet.SetCellValue(vtStart + vtName, poc.PupilProfile.FullName);
            sheet.SetCellValue(vtStart + vtBirthDay, poc.PupilProfile.BirthDate.ToString("dd/MM/yyyy"));
            sheet.SetCellValue(vtStart + vtBirthPlace, poc.PupilProfile.BirthPlace);
            sheet.SetCellValue(vtStart + vtClassName, poc.ClassProfile.DisplayName);
            sheet.SetCellValue(vtStart + vtClassStage, poc.ClassProfile.SubCommittee != null ? poc.ClassProfile.SubCommittee.Description : String.Empty);

            if (rss != null)
            {
                ClassSubject cs = lstClassSubject.Where(o => o.SubjectID == rss.SubjectID).FirstOrDefault();
                if (cs != null)
                {
                    sheet.SetCellValue(vtStart + vtSpecialSubject, cs.SubjectCat.DisplayName);
                }
            }

        }

        private void FillLearningResult(int pocStartRow, IVTWorksheet sheet, IVTRange template, List<ClassSubject> lstClassSubject, List<SummedUpRecordBO> lstSummedUpRecord,
            PupilRankingBO pupilRanking, PupilEmulation pe, Employee headTeacher)
        {

            //Lay ra vi tri bat dau fill danh sach mon
            int startRow = pocStartRow + 11;
            sheet.CopyPasteSameSize(template, startRow, 1);

            //Fill diem cac mon
            int curRow = startRow + 1;
            for (int i = 0; i < lstClassSubject.Count; i++)
            {
                ClassSubject cs = lstClassSubject[i];
                //STT
                sheet.SetCellValue(curRow, 1, i + 1);

                //ten mon
                sheet.SetCellValue(curRow, 2, cs.SubjectCat.DisplayName);

                SummedUpRecordBO su = lstSummedUpRecord.Where(o => o.SubjectID == cs.SubjectID).FirstOrDefault();
                if (su != null)
                {
                    if (cs.IsCommenting == GlobalConstants.ISCOMMENTING_TYPE_JUDGE)
                    {
                        sheet.SetCellValue(curRow, 6, su.JudgementResult);
                    }
                    else
                    {
                        sheet.SetCellValue(curRow, 6, su.SummedUpMark != null ? ReportUtils.ConvertMarkForV(su.SummedUpMark.Value) : string.Empty);
                    }
                }
                curRow++;
            }

            //Fill danh hieu, hoc luc...
            VTVector vtStart = new VTVector(startRow, firstColumn);
            VTVector vtAverageMark = new VTVector(0, 18);
            VTVector vtConduct = new VTVector(1, 18);
            VTVector vtCapacity = new VTVector(2, 18);
            VTVector vtHonor = new VTVector(3, 18);
            if(pupilRanking!=null)
            {
                sheet.SetCellValue(vtStart + vtAverageMark, pupilRanking.AverageMark != null ? ReportUtils.ConvertMarkForV(pupilRanking.AverageMark.Value) : string.Empty);
                sheet.SetCellValue(vtStart + vtConduct, pupilRanking.ConductLevelName);
                sheet.SetCellValue(vtStart + vtCapacity, pupilRanking.CapacityLevel);
                
            }
            if (pe != null)
            {
                sheet.SetCellValue(vtStart + vtHonor, pe.HonourAchivementType.Resolution);
            }
            //Fill ten GVCN
            VTVector vtHeadTeacher = new VTVector(9, 9);
            if (headTeacher != null)
            {
                sheet.SetCellValue(vtStart + vtHeadTeacher, headTeacher.FullName);
            }
        }

        public ProcessedReport InsertLearningResultCertificate(IDictionary<string, object> dic, Stream data)
        {
            int appliedLevel = Utils.GetInt(dic["AppliedLevel"]);

            string reportCode = SystemParamsInFile.REPORT_LEARNING_RESULT_CERTIFICATE;

            ProcessedReport pr = new ProcessedReport();

            ReportDefinition reportDef = ReportDefinitionBusiness.GetByCode(reportCode);
            pr.ReportCode = reportCode;
            pr.ProcessedDate = DateTime.Now;
            pr.InputParameterHashKey = GetHashKey(dic);
            pr.ReportData = ReportUtils.Compress(data);

            //Tạo tên file
            string outputNamePattern = reportDef.OutputNamePattern;

            string strAppliedLevel = ReportUtils.ConvertAppliedLevelForReportName(appliedLevel);

            outputNamePattern = outputNamePattern.Replace("[AppliedLevel]", ReportUtils.StripVNSign(strAppliedLevel));

            pr.ReportName = ReportUtils.RemoveSpecialCharacters(outputNamePattern) + "." + reportDef.OutputFormat;

            ProcessedReportParameterRepository.Insert(dic, pr);
            ProcessedReportRepository.Insert(pr);
            ProcessedReportRepository.Save();
            return pr;
        }
        #endregion
    }
}
