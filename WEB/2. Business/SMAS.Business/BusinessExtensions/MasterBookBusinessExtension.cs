﻿/**
 * The Viettel License
 *
 * Copyright 2012 Viettel Telecom. All rights reserved.
 * VIETTEL PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */

/**
* @author
* @version $Revision: $
*/

using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using SMAS.Business.BusinessObject;
using SMAS.Business.Common;
using SMAS.DAL.Repository;
using SMAS.Models.Models;
using SMAS.VTUtils.Excel.Export;
using Ionic.Zip;
using SMAS.VTUtils.Utils;

namespace SMAS.Business.Business
{
    /// <summary>
    /// sổ gọi tên và ghi điểm
    /// </summary>
    /// <author>
    /// DungNT77
    /// </author>
    /// <remarks>
    /// 21/11/2012   2:17 PM
    /// </remarks>
    public partial class MasterBookBusiness
    {
        private List<int> ListPolicyTargetID = new List<int>() { 25, 37, 38, 42, 49, 50, 51, 52, 26, 43, 44, 45, 56, 54 };
        private double AreaMarkSubject = 70;

        /// <summary>
        /// Lấy mảng băm cho các tham số đầu vào của in sổ gọi tên và ghi điểm
        /// </summary>
        /// <param name="entity">The entity.</param>
        /// <returns></returns>
        /// <author>
        /// DungNT77
        /// </author>
        /// <remarks>
        /// 21/11/2012   2:17 PM
        /// </remarks>
        public string GetHashKey(MasterBook entity)
        {
            return ReportUtils.GetHashKey(new Dictionary<string, object>()
            {
                {"Semester",entity.Semester},
                {"ClassID",entity.ClassID} ,
                {"SchoolID",entity.SchoolID},
                {"AcademicYearID",entity.AcademicYearID},
                {"AppliedLevel",entity.AppliedLevel}
            });
        }

        public string GetHashKey(IDictionary<string, object> dic)
        {
            return ReportUtils.GetHashKey(dic);
        }

        /// <summary>
        /// Lưu lại thông tin sổ gọi tên và ghi điểm
        /// </summary>
        /// <param name="entity">The entity.</param>
        /// <param name="Data">The data.</param>
        /// <returns></returns>
        /// <author>
        /// DungNT77
        /// </author>
        /// <remarks>
        /// 21/11/2012   2:17 PM
        /// </remarks>
        public ProcessedReport InsertMasterBook(MasterBook entity, Stream Data)
        {
            try
            {
                ReportDefinitionBusiness.SetAutoDetectChangesEnabled(false);
                bool Compatible = ClassProfileRepository.ExistsRow(GlobalConstants.SCHOOL_SCHEMA, "ClassProfile",
                    new Dictionary<string, object>() { { "ClassProfileID", entity.ClassID }, { "AcademicYearID", entity.AcademicYearID } }, null);


                if (!Compatible)
                {
                    throw new BusinessException("Common_Validate_NotCompatible");
                }

                string reportCode = SystemParamsInFile.REPORT_INSOGOITENVAGHIDIEM;
                ReportDefinition reportDef = ReportDefinitionBusiness.GetByCode(reportCode);
                //Khởi tạo đối tượng ProcessedReport:
                ProcessedReport pr = new ProcessedReport();
                pr.ReportCode = SystemParamsInFile.REPORT_INSOGOITENVAGHIDIEM;
                pr.ProcessedDate = DateTime.Now;
                pr.InputParameterHashKey = GetHashKey(entity);
                pr.ReportData = ReportUtils.Compress(Data);

                //Tạo tên file
                string outputNamePattern = reportDef.OutputNamePattern;
                string semester = ReportUtils.ConvertSemesterForReportName(entity.Semester);
                ClassProfile cp = ClassProfileRepository.Find(entity.ClassID);
                string ClassName = cp.DisplayName;
                string schoolLevel = ReportUtils.ConvertAppliedLevelForReportName(entity.AppliedLevel);

                outputNamePattern = outputNamePattern.Replace("[Semester]", semester);
                outputNamePattern = outputNamePattern.Replace("[Class]", ClassName);
                outputNamePattern = outputNamePattern.Replace("[SchoolLevel]", schoolLevel);

                pr.ReportName = ReportUtils.RemoveSpecialCharacters(outputNamePattern) + "." + reportDef.OutputFormat;

                IDictionary<string, object> dic = new Dictionary<string, object> {
                {"ClassID", entity.ClassID},
                {"Semester", entity.Semester}
                ,{"SchoolID",entity.SchoolID},
                {"AcademicYearID",entity.AcademicYearID},
                 {"AppliedLevel",entity.AppliedLevel}
            };
                ProcessedReportParameterRepository.Insert(dic, pr);
                ProcessedReportRepository.Insert(pr);
                ProcessedReportRepository.Save();
                return pr;
            }
            finally
            {
                ReportDefinitionBusiness.SetAutoDetectChangesEnabled(true);
            }
        }

        /// <summary>
        /// Lấy sổ gọi tên và ghi điểm được cập nhật mới nhất
        /// </summary>
        /// <param name="entity">The entity.</param>
        /// <returns></returns>
        /// <author>
        /// DungNT77
        /// </author>
        /// <remarks>
        /// 21/11/2012   2:17 PM
        /// </remarks>
        public ProcessedReport GetMasterBook(MasterBook entity)
        {
            string reportCode = SystemParamsInFile.REPORT_INSOGOITENVAGHIDIEM;
            string inputParameterHashKey = GetHashKey(entity);
            return ProcessedReportBusiness.GetProcessedReport(reportCode, inputParameterHashKey);
        }

        /// <summary>
        /// Lưu lại thông tin sổ gọi tên và ghi điểm
        /// </summary>
        /// <param name="entity">The entity.</param>
        /// <param name="suffixName"></param>
        /// <returns></returns>
        /// <author>
        /// DungNT77
        /// </author>
        /// <remarks>
        /// 21/11/2012   2:18 PM
        /// </remarks>
        public Stream CreateMasterBook(MasterBook entity, string suffixName = null)
        {
            bool Compatible = ClassProfileRepository.ExistsRow(GlobalConstants.SCHOOL_SCHEMA, "ClassProfile",
                    new Dictionary<string, object>()
                {
                    {"ClassProfileID",entity.ClassID},
                    {"AcademicYearID",entity.AcademicYearID}
                }, null);
            if (!Compatible)
            {
                throw new BusinessException("Common_Validate_NotCompatible");
            }
            return CreateMasterBookAppliedLevelTertiary(entity, suffixName);
        }

        /// <summary>
        /// Xuất excel cho cấp 3
        /// </summary>
        /// <param name="entity"></param>
        /// <returns></returns>
        private Stream CreateMasterBookAppliedLevelTertiary(MasterBook entity, string suffixName = null)
        {
            List<VTDataPageNumber> lstDataPageNumber = new List<VTDataPageNumber>();
            VTDataPageNumber objPageNumber = null;
            int positionSheet = 3;
            int startNumber = 2;

            int partitionId = UtilsBusiness.GetPartionId(entity.SchoolID);
            List<int> lstSubjectExportID = !string.IsNullOrEmpty(entity.arrSubjectID) ? entity.arrSubjectID.Split(new string[] { "," }, StringSplitOptions.RemoveEmptyEntries).Select(p => int.Parse(p)).ToList() : new List<int>();
            #region lay cac sheet tu template

            string reportCode = SystemParamsInFile.REPORT_INSOGOITENVAGHIDIEM;
            ReportDefinition reportDef = ReportDefinitionBusiness.GetByCode(reportCode);
            string templatePath = SystemParamsInFile.TEMPLATE_FOLDER + reportDef.TemplateDirectory + "/" + "SoGoiTenVaGhiDiem.xls";//reportDef.TemplateName;
            //if (!string.IsNullOrWhiteSpace(suffixName))
            //{
            //    // Nếu là hcm thì lấy template khác
            //    templatePath = SystemParamsInFile.TEMPLATE_FOLDER + reportDef.TemplateDirectory + "/" + string.Format("{0}-{1}{2}", Path.GetFileNameWithoutExtension(reportDef.TemplateName), suffixName, Path.GetExtension(reportDef.TemplateName));
            //}

            //Xoa 2 sheet bia cua cap hoc khac dang chon
            IVTWorkbook oBook = VTExport.OpenWorkbook(templatePath);
            if (entity.AppliedLevel == GlobalConstants.APPLIED_LEVEL_SECONDARY)
            {
                oBook.GetSheet("Bia1_THPT").Delete();
                oBook.GetSheet("Bia2_THPT").Delete();
            }
            else if (entity.AppliedLevel == GlobalConstants.APPLIED_LEVEL_TERTIARY)
            {
                oBook.GetSheet("Bia1").Delete();
                oBook.GetSheet("Bia2").Delete();
            }
            //lấy các sheet ra:
            IVTWorksheet sheetBia1 = oBook.GetSheet(1);

            IVTWorksheet sheetBia2 = oBook.GetSheet(2);
            if (entity.AppliedLevel == GlobalConstants.APPLIED_LEVEL_TERTIARY)
            {
                sheetBia1.Name = "Bia1";
                sheetBia2.Name = "Bia2";
            }
            IVTWorksheet sheetSoYeuLyLich = oBook.GetSheet(3);
            IVTWorksheet sheetDiemDanh = oBook.GetSheet(5);

            IVTWorksheet sheetBiaGhiDiemHK1 = oBook.GetSheet(6);
            IVTWorksheet sheetDiemHK1 = oBook.GetSheet(7);
            IVTWorksheet sheetDiemHK1NoBorder = oBook.GetSheet(8);
            IVTWorksheet sheetDiemTCHK1 = oBook.GetSheet(9);
            IVTWorksheet sheetBiaGhiDiemHK2 = oBook.GetSheet(10);
            IVTWorksheet sheetDiemHK2 = oBook.GetSheet(11);
            IVTWorksheet sheetDiemHK2NoBorder = oBook.GetSheet(12);
            IVTWorksheet sheetDiemTCHK2 = oBook.GetSheet(13);
            IVTWorksheet sheetBiaGhiDiemCN = oBook.GetSheet(14);
            IVTWorksheet sheetTCCaNam = oBook.GetSheet(15);
            IVTWorksheet sheetNhanXetCuaHT1 = oBook.GetSheet(16);
            IVTWorksheet sheetNhanXetCuaHT2 = oBook.GetSheet(17);
            IVTWorksheet sheetGiaoVienBM = oBook.GetSheet(18);
            decimal totalHeigth = ((decimal)60 * (decimal)17.5);
            double rowHeigth = 0;
            #endregion lay cac sheet tu template
            #region set du lieu sheet Bia
            SchoolProfile sp = SchoolProfileBusiness.Find(entity.SchoolID);
            SupervisingDept supervisingDept = sp.SupervisingDept;
            bool isSoHCM = false;
            if (supervisingDept.Province.ProvinceID == GlobalConstants.ProvinceID_HCM)
            {
                isSoHCM = true;
            }
            List<int> lstSectionKeyID = new List<int>();
            ClassProfile cp = ClassProfileBusiness.Find(entity.ClassID);
            lstSectionKeyID = UtilsBusiness.GetListSectionID(cp.SeperateKey.HasValue ? cp.SeperateKey.Value : 0);

            //string FullAppliedLevel = ReportUtils.ConvertAppliedLevelToFullReportName(entity.AppliedLevel);
            string SchoolName = sp.SchoolName.Trim();
            string startSchoolName = SchoolName.Length >= 6 ? SchoolName.Substring(0, 6).ToUpper() : SchoolName.ToUpper();
            if (startSchoolName.Contains("TRƯỜNG"))
            {
                SchoolName = SchoolName.Remove(0, 6).Trim().ToUpper();
            }
            string district = "";
            string province = "";

            if (sp.DistrictID.HasValue)
            {
                district = DistrictBusiness.Find(sp.DistrictID).DistrictName;
            }
            if (sp.ProvinceID.HasValue)
            {
                province = ProvinceBusiness.Find(sp.ProvinceID).ProvinceName;
            }


            string ClassName = cp.DisplayName;
            string SubCommittee = "";

            //EducationLevel(ClassProfile.EducationLevelID).Grade = APPLIED_LEVEL_TERTIARY
            EducationLevel el = EducationLevelBusiness.Find(entity.EducationLevelID);
            if (cp.SubCommitteeID.HasValue && el.Grade == SystemParamsInFile.APPLIED_LEVEL_TERTIARY)
            {
                SubCommittee = SubCommitteeBusiness.Find(cp.SubCommitteeID).Resolution;
            }
            AcademicYear ay = AcademicYearBusiness.Find(entity.AcademicYearID);
            string AcademicYear = ay.Year + "-" + (ay.Year + 1);

            string HeadTeacherName = "";
            if (cp.HeadTeacherID.HasValue)
            {
                HeadTeacherName = EmployeeBusiness.Find(cp.HeadTeacherID.Value).FullName;
            }
            string HeadMasterName = sp.HeadMasterName;

            //Dien lai 1 so tieu de ap dung cho cac truong HCM

            if (isSoHCM)
            {
                sheetSoYeuLyLich.SetCellValue("M4", "Các chú ý về hoàn cảnh gia đình, sức khỏe hoặc các thay đổi về nơi ở");
                sheetDiemTCHK1.SetCellValue("V3", "Kết quả xếp loại và DH");
                sheetDiemTCHK1.SetCellValue("X4", "DH");
                sheetDiemTCHK2.SetCellValue("V3", "Kết quả xếp loại và DH");
                sheetDiemTCHK2.SetCellValue("X4", "DH");
                sheetTCCaNam.SetCellValue("AT3", "TS buổi nghỉ học");
            }
            #endregion

            #region Fill sheet Bia 1


            SupervisingDept objParent = SupervisingDeptBusiness.All.FirstOrDefault(s => s.SupervisingDeptID == supervisingDept.ParentID.Value);
            if (entity.AppliedLevel == GlobalConstants.APPLIED_LEVEL_TERTIARY) //THPT
            {
                if (isSoHCM)
                {
                    sheetBia1.SetCellValue("A3", "SỞ GIÁO DỤC VÀ ĐÀO TẠO");
                    sheetBia1.SetCellValue("A4", "THÀNH PHỐ HỒ CHÍ MINH");
                }
                sheetBia1.SetCellValue("A46", "TRƯỜNG " + SchoolName.ToUpper());
                if (string.IsNullOrEmpty(SubCommittee))
                {
                    SubCommittee = "Cơ bản";
                }
                string classAndSubcommitee = string.Format("LỚP: {0}    BAN: {1}", ClassName, SubCommittee);
                sheetBia1.SetCellValue("A47", classAndSubcommitee.ToUpper());
                sheetBia1.SetCellValue("A60", "NĂM HỌC: " + AcademicYear);
            }
            else //THCS
            {
                if (isSoHCM)
                {
                    sheetBia1.SetCellValue("A3", "SỞ GIÁO DỤC VÀ ĐÀO TẠO");
                    sheetBia1.SetCellValue("A4", "THÀNH PHỐ HỒ CHÍ MINH");
                }
                sheetBia1.SetCellValue("A46", "TRƯỜNG " + SchoolName.ToUpper());
                sheetBia1.SetCellValue("A47", string.Format("Lớp : {0}", ClassName).ToUpper());
                sheetBia1.SetCellValue("A60", "NĂM HỌC: " + AcademicYear);

            }

            #endregion

            #region fill du lieu sheet bia 2
            if (entity.AppliedLevel == GlobalConstants.APPLIED_LEVEL_TERTIARY)
            {
                if (!isSoHCM)
                {
                    sheetBia2.SetCellValue("A4", supervisingDept.SupervisingDeptName.ToUpper());
                }
                else //SoGDHCM
                {
                    sheetBia2.SetCellValue("A4", "SỞ GIÁO DỤC VÀ ĐÀO TẠO");
                    sheetBia2.SetCellValue("A5", "THÀNH PHỐ HỒ CHÍ MINH");
                }
                sheetBia2.SetCellValue("A38", "TRƯỜNG " + SchoolName.ToUpper());
                sheetBia2.SetCellValue("K46", district);
                sheetBia2.SetCellValue("Y46", province);
                sheetBia2.SetCellValue("D48", ClassName);
                sheetBia2.SetCellValue("M48", SubCommittee);
                sheetBia2.SetCellValue("W48", ay.Year);
                sheetBia2.SetCellValue("Z48", (ay.Year + 1));
                sheetBia2.SetCellValue("A60", HeadTeacherName);
                sheetBia2.SetCellValue("P60", HeadMasterName);
            }
            else //THCS
            {
                if (!isSoHCM)
                {
                    if (supervisingDept.ParentID.HasValue && supervisingDept.ParentID != 1)
                    {
                        sheetBia2.SetCellValue("A4", objParent.SupervisingDeptName.ToUpper());
                        sheetBia2.SetCellValue("A5", supervisingDept.SupervisingDeptName.ToUpper());
                    }
                    else
                    {
                        sheetBia2.SetCellValue("A4", supervisingDept.SupervisingDeptName.ToUpper());
                    }

                }
                else //SoGDHCM
                {
                    sheetBia2.SetCellValue("A3", "SỞ GIÁO DỤC VÀ ĐÀO TẠO");
                    sheetBia2.SetCellValue("A4", "THÀNH PHỐ HỒ CHÍ MINH");
                }

                sheetBia2.SetCellValue("A38", "TRƯỜNG " + SchoolName.ToUpper());

                string communeName = "";
                if (sp.CommuneID.HasValue)
                {
                    Commune objCommune = CommuneBusiness.Find(sp.CommuneID.Value);
                    communeName = (objCommune != null) ? objCommune.CommuneName : "";
                }
                sheetBia2.SetCellValue("J46", communeName);
                sheetBia2.SetCellValue("Y46", district);
                sheetBia2.SetCellValue("P47", province);

                sheetBia2.SetCellValue("J48", ClassName);
                //sheetBia.SetCellValue("P47", SubCommittee);
                //sheetBia.SetCellValue("S48", AcademicYear);
                sheetBia2.SetCellValue("S48", ay.Year);
                sheetBia2.SetCellValue("V48", (ay.Year + 1));
                sheetBia2.SetCellValue("A60", HeadTeacherName);
                sheetBia2.SetCellValue("P60", HeadMasterName);
            }



            #endregion fill du lieu sheet bia

            #region fill so yeu ly lich hoc sinh
            //Lay cau hinh hien thi hoc sinh
            bool isNotShowPupil = ay.IsShowPupil.HasValue && ay.IsShowPupil.Value;

            IQueryable<PupilOfClass> lstPoc = PupilOfClassBusiness.SearchBySchool(entity.SchoolID, new Dictionary<string, object>()
            {
                {"ClassID", entity.ClassID},
                {"AcademicYearID", entity.AcademicYearID},
                {"CheckWithClass", "checkWithClass"}
            }).AddPupilStatus(isNotShowPupil);

            IQueryable<PupilProfileBO> lsPoC = from p in lstPoc
                                               join q in PupilProfileBusiness.All on p.PupilID equals q.PupilProfileID
                                               select new PupilProfileBO
                                               {
                                                   PupilProfileID = p.PupilID,
                                                   PupilCode = q.PupilCode,
                                                   FullName = q.FullName,
                                                   EthnicID = q.EthnicID,
                                                   CommuneName = q.Commune.CommuneName,
                                                   DistrictName = q.District.DistrictName,
                                                   VillageName = q.Village.VillageName,
                                                   EthnicName = q.Ethnic.EthnicName,
                                                   ProvinceName = q.Province.ProvinceName,
                                                   BirthDate = q.BirthDate,
                                                   BirthPlace = q.BirthPlace,
                                                   PolicyTargetName = q.PolicyTarget.Resolution,
                                                   PolicyTargetID = q.PolicyTargetID,
                                                   PermanentResidentalAddress = q.PermanentResidentalAddress,
                                                   TempResidentalAddress = q.TempResidentalAddress,
                                                   FatherFullName = q.FatherFullName,
                                                   MotherFullName = q.MotherFullName,
                                                   SponsorFullName = q.SponsorFullName,
                                                   FatherJob = q.FatherJob,
                                                   MotherJob = q.MotherJob,
                                                   SponsorJob = q.SponsorJob,
                                                   Genre = q.Genre,
                                                   Name = q.Name,
                                                   OrderInClass = p.OrderInClass,
                                                   ProfileStatus = p.Status,
                                                   AssignedDate = p.AssignedDate,
                                                   EndDate = p.EndDate
                                               };

            List<PupilProfileBO> lsPP = new List<PupilProfileBO>();
            if (entity.chkAllPupil)
            {
                if (lsPoC.Count() > 60)
                {
                    lsPP = lsPoC.ToList().OrderBy(o => o.OrderInClass).ThenBy(o => o.Name).ThenBy(p => p.FullName).Take(60).ToList();
                }
                else
                {
                    lsPP = lsPoC.ToList().OrderBy(o => o.OrderInClass).ThenBy(o => o.Name).ThenBy(p => p.FullName).ToList();
                }
            }
            else
            {
                lsPP = lsPoC.ToList().OrderBy(o => o.OrderInClass).ThenBy(o => o.Name).ThenBy(p => p.FullName).Take(entity.PupilNumber).ToList();
            }
            List<int> lstPupilID = lsPP.Select(p => p.PupilProfileID).Distinct().ToList();
            PupilProfileBO objPP = null;
            int countStudying = lsPoC.Where(o => o.ProfileStatus == SystemParamsInFile.PUPIL_STATUS_STUDYING || o.ProfileStatus == SystemParamsInFile.PUPIL_STATUS_GRADUATED).Count();
        
            List<ParticularPupil>  lstParticularPupilDB = ParticularPupilBusiness.SearchBySchool(entity.SchoolID, new Dictionary<string, object>() { 
               {"SchoolID", entity.SchoolID},
               {"AcademicYearID", entity.AcademicYearID},
               {"EducationLevelID", entity.EducationLevelID},
               {"ClassID", entity.ClassID},
               {"lstPupilID", lstPupilID}
            }).ToList();
            ParticularPupilCharacteristic particularPupilCharacteristic = null;

            //Điền thông tin vào sheet
            int countPP = lsPP.Count();
            int startRow = 5;
            int rowStandar = 0;
            if (entity.chkAllPupil)
            {
                if (countPP < 20)
                {
                    rowStandar = 20;
                }
                else
                {
                    rowStandar = countPP;
                }

            }
            else
            {
                rowStandar = entity.PupilNumber;

            }
            rowHeigth = (double)Math.Round((decimal)totalHeigth / (decimal)rowStandar, 2);
            int target = 0;
            for (int i = 0; i < lsPP.Count; i++)
            {
                objPP = lsPP[i];
                sheetSoYeuLyLich.SetCellValue("A" + startRow, i + 1); //stt
                sheetSoYeuLyLich.SetCellValue("B" + startRow, objPP.FullName);
                sheetSoYeuLyLich.SetCellValue("C" + startRow, String.Format("{0:dd/MM/yyyy}", objPP.BirthDate));
                sheetSoYeuLyLich.SetCellValue("D" + startRow, objPP.BirthPlace);
                sheetSoYeuLyLich.SetCellValue("E" + startRow, SMASConvert.ConvertGenre(objPP.Genre));
                sheetSoYeuLyLich.SetCellValue("F" + startRow, objPP.EthnicName);

                target = ListPolicyTargetID.Where(x => x == objPP.PolicyTargetID).FirstOrDefault();
                sheetSoYeuLyLich.SetCellValue("G" + startRow, target > 0 ? objPP.PolicyTargetName : "");

                //fill cho o hien tai
                string address = string.Empty;
                if (entity.rdo1)
                {
                    if (entity.chkPRAddress && entity.chkTRAddress)
                    {
                        address = (!string.IsNullOrEmpty(objPP.TempResidentalAddress)) ? objPP.TempResidentalAddress : objPP.PermanentResidentalAddress;
                    }
                    else if (entity.chkPRAddress && !entity.chkTRAddress)
                    {
                        address = objPP.PermanentResidentalAddress;
                    }
                    else if (!entity.chkPRAddress && entity.chkTRAddress)
                    {
                        address = objPP.TempResidentalAddress;
                    }
                }
                else
                {
                    List<string> lstAdd = new List<string>();
                    if (entity.chkVillageID && !string.IsNullOrEmpty(objPP.VillageName))
                    {
                        lstAdd.Add(objPP.VillageName);
                    }
                    if (entity.chkCommuneID && !string.IsNullOrEmpty(objPP.CommuneName))
                    {
                        lstAdd.Add(objPP.CommuneName);
                    }
                    if (entity.chkDistrictID && !string.IsNullOrEmpty(objPP.DistrictName))
                    {
                        lstAdd.Add(objPP.DistrictName);
                    }
                    if (entity.chkProvinceID && !string.IsNullOrEmpty(objPP.ProvinceName))
                    {
                        lstAdd.Add(objPP.ProvinceName);
                    }
                    address = (lstAdd.Count > 0) ? string.Join(", ", lstAdd) : "";
                }
                sheetSoYeuLyLich.SetCellValue("H" + startRow, address);
                sheetSoYeuLyLich.SetCellValue("J" + startRow, i + 1);

                string fatherName = objPP.FatherFullName;
                string motherName = objPP.MotherFullName;
                string sponsorName = objPP.SponsorFullName;
                string fatherOrSponsor = "";
                string motherOrSponsor = "";
                if (!string.IsNullOrEmpty(fatherName))
                {

                    if (!string.IsNullOrEmpty(objPP.FatherJob))
                    {
                        fatherOrSponsor = string.Format("{0} - {1}", fatherName, objPP.FatherJob);
                    }
                    else
                    {
                        fatherOrSponsor = fatherName;
                    }
                }
                else if (!string.IsNullOrEmpty(sponsorName))
                {

                    if (!string.IsNullOrEmpty(objPP.SponsorJob))
                    {
                        fatherOrSponsor = string.Format("{0} - {1}", sponsorName, objPP.SponsorJob);
                    }
                    else
                    {
                        fatherOrSponsor = sponsorName;
                    }
                }

                if (!string.IsNullOrEmpty(motherName))
                {
                    if (!string.IsNullOrEmpty(objPP.MotherJob))
                    {
                        motherOrSponsor = string.Format("{0} - {1}", motherName, objPP.MotherJob);
                    }
                    else
                    {
                        motherOrSponsor = motherName;
                    }
                }
                else if (!string.IsNullOrEmpty(sponsorName))
                {
                    //motherOrSponsor = sponsorName + "-Nghề nghiệp: " + objPP.SponsorJob;
                    if (!string.IsNullOrEmpty(objPP.SponsorJob))
                    {
                        motherOrSponsor = string.Format("{0} - {1}", sponsorName, objPP.SponsorJob);
                    }
                    else
                    {
                        motherOrSponsor = sponsorName;
                    }
                }
                sheetSoYeuLyLich.SetCellValue("K" + startRow, fatherOrSponsor);
                sheetSoYeuLyLich.SetCellValue("L" + startRow, motherOrSponsor);

                var lstParticularPupil = lstParticularPupilDB.Where(x => x.PupilID == objPP.PupilProfileID).FirstOrDefault();
                if (lstParticularPupil != null)
                {
                    particularPupilCharacteristic = lstParticularPupil.ParticularPupilCharacteristics.OrderByDescending(u => u.RealUpdatedDate).FirstOrDefault();
                    sheetSoYeuLyLich.SetCellValue("M" + startRow, particularPupilCharacteristic != null ? particularPupilCharacteristic.FamilyCharacteristic : "");
                }
                
                if (objPP.ProfileStatus != GlobalConstants.PUPIL_STATUS_STUDYING && objPP.ProfileStatus != GlobalConstants.PUPIL_STATUS_GRADUATED)
                {
                    sheetSoYeuLyLich.GetRange(startRow, 1, startRow, 13).SetFontStyle(false, null, false, null, true, false);
                }
           
                sheetSoYeuLyLich.SetRowHeight(startRow, rowHeigth);
                startRow++;
            }

            objPageNumber = new VTDataPageNumber();
            objPageNumber.SheetIndex = positionSheet;
            objPageNumber.IsHeaderOrFooter = true;
            objPageNumber.AlignPageNumber = 2;
            objPageNumber.StartPageNumberOfSheet = startNumber;
            lstDataPageNumber.Add(objPageNumber);
            positionSheet += 1;
            startNumber += 2;
            this.HidenRow(sheetSoYeuLyLich, (5 + rowStandar), 65);
            #endregion fill so yeu ly lich hoc sinh

            #region fill du lieu sheet diem danh
            //Diem danh
            IVTWorksheet sheetDiemDanhTemplate = oBook.GetSheet(4);
            totalHeigth = ((decimal)60 * (decimal)16.5);
            rowHeigth = (double)Math.Round((decimal)totalHeigth / (decimal)rowStandar, 2);
            startRow = 5;
            this.SetRowHeight(sheetDiemDanhTemplate, startRow, startRow + rowStandar, rowHeigth);
            this.HidenRow(sheetDiemDanhTemplate, startRow + rowStandar, 65);

            IVTRange temDiemDanh = sheetDiemDanhTemplate.GetRange("A1", "AK71");
            IVTRange rangeStudyingDiemdanh = sheetDiemDanhTemplate.GetRange("A5", "AK5");
            IVTRange rangeNotStudyingDiemdanh = sheetDiemDanhTemplate.GetRange("A6", "AK6");

            //Thay doi tieu de so ngay nghi neu la truong o HCM

            if (isSoHCM)
            {
                sheetDiemDanhTemplate.SetCellValue("AI3", "Tổng số buổi nghỉ");
            }
            var query = PupilAbsenceBusiness
                .SearchBySchool(entity.SchoolID, new Dictionary<string, object>()
                {
                        {"ClassID",entity.ClassID},
                        {"AcademicYearID",entity.AcademicYearID},
                        {"EducationLevelID",entity.EducationLevelID}
                })
            .Where(p => lstSectionKeyID.Contains(p.Section));// Chi lay cac du lieu diem danh trong cac buoi hoc chinh

            List<PupilAbsence> iqPA = query.ToList(); // Danh sach diem danh trong buoi hoc chinh

            int FirstSemesterStartmonth = ay.FirstSemesterStartDate.Value.Month;
            int FirstSemesterStartyear = ay.FirstSemesterStartDate.Value.Year;
            //neu ki 2 thi chi hien thi diem danh ki 2
            if (entity.Semester == SystemParamsInFile.SEMESTER_OF_YEAR_SECOND)
            {
                FirstSemesterStartmonth = ay.SecondSemesterStartDate.Value.Month;
                FirstSemesterStartyear = ay.SecondSemesterStartDate.Value.Year;
            }
            int SecondSemesterEndDatemonth = ay.SecondSemesterEndDate.Value.Month;
            int SecondSemesterEndDateyear = ay.SecondSemesterEndDate.Value.Year;
            //neu ki 1 thi chi hien thi diem danh ki 1
            if (entity.Semester == SystemParamsInFile.SEMESTER_OF_YEAR_FIRST)
            {
                SecondSemesterEndDatemonth = ay.FirstSemesterEndDate.Value.Month;
                SecondSemesterEndDateyear = ay.FirstSemesterEndDate.Value.Year;
            }
            int pupilcount = lsPoC.Count();
            sheetDiemDanhTemplate.SetCellValue("AI2", pupilcount);
            //copy
            int totalRow = 71;
            if (SecondSemesterEndDateyear > FirstSemesterStartyear)
            {
                SecondSemesterEndDatemonth += (SecondSemesterEndDateyear - FirstSemesterStartyear) * 12;
            }
            int countP = 0;
            int countK = 0;
            string result = string.Empty;
            for (int i = 0; i < SecondSemesterEndDatemonth - FirstSemesterStartmonth + 1; i++)
            {
                sheetDiemDanh.CopyPasteSameSize(temDiemDanh, totalRow * i + 1, 1);
                //Bo sung sheet cuoi cung vao khi so trang diem danh la chan de dam bao in 2 mat
                /*if (i == SecondSemesterEndDatemonth - FirstSemesterStartmonth && (SecondSemesterEndDatemonth - FirstSemesterStartmonth + 1) % 2 == 0)
                {
                    int row = totalRow * (SecondSemesterEndDatemonth - FirstSemesterStartmonth + 1);
                    sheetDiemDanh.CopyPasteSameSize(temDiemDanh, row + 1, 1);
                }*/
                int nextFirstRow = totalRow * i + 1;
                sheetDiemDanh.SetBreakPage(nextFirstRow);
                int currentMonth = FirstSemesterStartmonth + i;
                int currentYear = FirstSemesterStartyear;
                if (currentMonth > 12)
                {
                    currentYear += 1;
                    currentMonth -= 12;
                }
                string monthYear = "Tháng {0} năm {1}";
                string strMY = string.Format(monthYear, new string[] { currentMonth.ToString(), currentYear.ToString() });
                sheetDiemDanh.SetCellValue("A" + (totalRow * i + 2), strMY);
                //truyen dictionary du lieu vao template truoc khi copy
                //fill du lieu hoc sinh
                //Điền thông tin vào sheet
                //${Rows[].get(FullName)},${Rows[].get(i)},${Rows[].get(Total)},${Rows[].get(P)},${Rows[].get(K)},${Date1}
                DateTime firstDayofMonth = new DateTime(currentYear, currentMonth, 1);
                DateTime lastDayOfMonth = new DateTime(currentYear, currentMonth, DateTime.DaysInMonth(currentYear, currentMonth));
                List<PupilAbsence> lsPAthisMonth = iqPA.Where(o => o.AbsentDate >= firstDayofMonth && o.AbsentDate <= lastDayOfMonth).ToList();
                List<PupilAbsence> lstPAtmp = new List<PupilAbsence>();
                List<string> lstDataTime = lsPAthisMonth.Select(p => p.AbsentDate.ToString()).Distinct().ToList();
                Dictionary<string, object> dic = new Dictionary<string, object>();
                List<object> insideDic = new List<object>();

                for (int j = 0; j < lsPP.Count(); j++)
                {
                    //int column = j + 7;
                    //sheetDiemDanhTemplate.SetCellValue("B" + column, lsPP[j].FullName);
                    if (lsPP[j].ProfileStatus == SystemParamsInFile.PUPIL_STATUS_STUDYING || lsPP[j].ProfileStatus == SystemParamsInFile.PUPIL_STATUS_GRADUATED)
                    {

                        sheetDiemDanh.CopyPasteSameColumnWidth(rangeStudyingDiemdanh, 4 + nextFirstRow + j, 1, true);
                    }
                    else
                    {
                        sheetDiemDanh.CopyPasteSameColumnWidth(rangeNotStudyingDiemdanh, 4 + nextFirstRow + j, 1, true);
                    }
                    Dictionary<string, object> innerDic = new Dictionary<string, object>();
                    innerDic.Add("FullName", lsPP[j].FullName);
                    int pupilID = lsPP[j].PupilProfileID;
                    innerDic.Add("Order", j + 1);
                    innerDic.Add("HeadTeacherName", HeadTeacherName);
                    PupilProfileBO pp = lsPP[j];
                    List<PupilAbsence> lsThisPupilAbsence = lsPAthisMonth.Where(o => o.PupilID == pupilID).ToList();

                    for (int k = 0; k < lstDataTime.Count; k++)
                    {
                        DateTime absencetime = Convert.ToDateTime(lstDataTime[k]);
                        lstPAtmp = lsThisPupilAbsence.Where(o => o.AbsentDate == absencetime).ToList();
                        countP = lstPAtmp.Where(p => p.IsAccepted.HasValue && p.IsAccepted.Value).Count();
                        countK = lstPAtmp.Where(p => p.IsAccepted.HasValue && !p.IsAccepted.Value).Count();
                        if (countP > 0 && countK > 0)
                        {
                            result = countP + "P" + countK + "K";
                        }
                        else if (countP > 0 && countK == 0)
                        {
                            if (countP > 1)
                            {
                                result = countP + "P";
                            }
                            else
                            {
                                result = "P";
                            }
                        }
                        else if (countP == 0 && countK > 0)
                        {
                            if (countK > 1)
                            {
                                result = countK + "K";
                            }
                            else
                            {
                                result = "K";
                            }
                        }
                        else
                        {
                            result = "";
                        }
                        if (absencetime >= ay.FirstSemesterStartDate && absencetime <= ay.FirstSemesterEndDate)
                        {
                            if (pp.ProfileStatus == SystemParamsInFile.PUPIL_STATUS_STUDYING
                                                || pp.ProfileStatus == SystemParamsInFile.PUPIL_STATUS_GRADUATED)
                            {
                                innerDic[absencetime.Day.ToString()] = result;
                            }
                            else
                            {
                                if (absencetime >= ay.FirstSemesterStartDate && absencetime <= ay.FirstSemesterEndDate)
                                {
                                    innerDic[absencetime.Day.ToString()] = result;
                                }
                            }
                        }
                        if (absencetime >= ay.SecondSemesterStartDate && absencetime <= ay.SecondSemesterEndDate)
                        {
                            if (pp.ProfileStatus == SystemParamsInFile.PUPIL_STATUS_STUDYING
                                                || pp.ProfileStatus == SystemParamsInFile.PUPIL_STATUS_GRADUATED)
                            {
                                innerDic[absencetime.Day.ToString()] = result;
                            }
                            else
                            {
                                if (absencetime >= ay.SecondSemesterStartDate && absencetime <= ay.SecondSemesterEndDate)
                                {
                                    innerDic[absencetime.Day.ToString()] = result;
                                }
                            }
                        }

                    }

                    //Xét học kỳ, trạng thái
                    if (pp.ProfileStatus == SystemParamsInFile.PUPIL_STATUS_STUDYING
                                            || pp.ProfileStatus == SystemParamsInFile.PUPIL_STATUS_GRADUATED)
                    {
                        innerDic.Add("TS", lsThisPupilAbsence.Count());
                        innerDic.Add("P", lsThisPupilAbsence.Where(o => o.IsAccepted == true).Count());
                        innerDic.Add("K", lsThisPupilAbsence.Where(o => o.IsAccepted == false).Count());
                    }
                    else
                    {
                        innerDic.Add("TS", lsThisPupilAbsence.Where(o => o.AbsentDate >= ay.FirstSemesterStartDate && o.AbsentDate <= ay.FirstSemesterEndDate).Count());
                        innerDic.Add("P", lsThisPupilAbsence.Where(o => o.IsAccepted == true && o.AbsentDate >= ay.FirstSemesterStartDate && o.AbsentDate <= ay.FirstSemesterEndDate).Count());
                        innerDic.Add("K", lsThisPupilAbsence.Where(o => o.IsAccepted == false && o.AbsentDate >= ay.FirstSemesterStartDate && o.AbsentDate <= ay.FirstSemesterEndDate).Count());
                    }
                    //
                    insideDic.Add(innerDic);
                }

                //dien thong tin ngay trong tuan
                dic.Add("Rows", insideDic);
                Dictionary<string, object> dateDic = new Dictionary<string, object>();
                for (int day = 1; day <= DateTime.DaysInMonth(currentYear, currentMonth); day++)
                {
                    Dictionary<string, object> tempDic = new Dictionary<string, object>();
                    DateTime thisDate = new DateTime(currentYear, currentMonth, day);
                    dateDic[day.ToString()] = GetDateInWeekString(thisDate);
                }
                dic["Date"] = dateDic;

                //copy vao template
                sheetDiemDanh.FillVariableValue(dic);
                int rowHead = totalRow * (i + 1);
                sheetDiemDanh.MergeRow(rowHead, 29, 36);
                IVTRange rangHead = sheetDiemDanh.GetRange(rowHead, 29, rowHead, 36);
                //rangHead.SetFontStyle(true, null, false, null, false, false);
                rangHead.SetHAlign(VTHAlign.xlHAlignCenter);
                sheetDiemDanh.SetCellValue(rowHead, 29, HeadTeacherName);
                //dien ten giao vien CN
            }
            //Bo sung sheet cuoi cung vao khi so trang diem danh la chan de dam bao in 2 mat
            if ((SecondSemesterEndDatemonth - FirstSemesterStartmonth + 1) % 2 == 0)
            {
                int row = totalRow * (SecondSemesterEndDatemonth - FirstSemesterStartmonth + 1);
                sheetDiemDanh.SetCellValue(row + 2, 1, "  ");
            }
            sheetDiemDanh.PageSize = VTXPageSize.VTxlPaperA3;
           
            objPageNumber = new VTDataPageNumber();
            objPageNumber.SheetIndex = positionSheet;
            objPageNumber.IsHeaderOrFooter = true;
            objPageNumber.AlignPageNumber = 2;
            objPageNumber.StartPageNumberOfSheet = startNumber;
            lstDataPageNumber.Add(objPageNumber);
            positionSheet += 1;
            startNumber += (SecondSemesterEndDatemonth - FirstSemesterStartmonth + 1);
            #endregion fill du lieu sheet diem danh
           
            #region lay thong tin diem
            Dictionary<string, object> searchClass = new Dictionary<string, object>();
            searchClass["ClassID"] = entity.ClassID;
            searchClass["AcademicYearID"] = entity.AcademicYearID;
            searchClass["SchoolID"] = entity.SchoolID;

            //Danh sach mon hoc cua lop
            List<ClassSubjectBO> lstTotalSubject = GetSubjectInClass(entity, lstSubjectExportID);
            List<int> lstSubjectID = lstTotalSubject.Select(p => p.SubjectID).Distinct().ToList();
            List<int> lsAppliedTypeMonTuChon = new List<int>() { SystemParamsInFile.APPLIED_TYPE_VOLUNTARY, SystemParamsInFile.APPLIED_TYPE_VOLUNTARY_PLUS_MARK, SystemParamsInFile.APPLIED_TYPE_VOLUNTARY_PRIORITY };

            List<MarkRecordBO> iqMarkRecord = (from p in VMarkRecordBusiness.SearchBySchool(entity.SchoolID, searchClass)
                                               join q in MarkTypeBusiness.All on p.MarkTypeID equals q.MarkTypeID
                                               where lstPupilID.Contains(p.PupilID)
                                               && lstSubjectID.Contains(p.SubjectID)
                                               select new MarkRecordBO
                                               {
                                                   MarkRecordID = p.MarkRecordID,
                                                   Mark = p.Mark,
                                                   PupilID = p.PupilID,
                                                   SubjectID = p.SubjectID,
                                                   Semester = p.Semester,
                                                   Title = q.Title,
                                                   OrderNumber = p.OrderNumber
                                               }).ToList();
            List<JudgeRecordBO> iqJudgeRecord = (from p in VJudgeRecordBusiness.SearchBySchool(entity.SchoolID, searchClass)
                                                 join q in MarkTypeBusiness.All on p.MarkTypeID equals q.MarkTypeID
                                                 where lstPupilID.Contains(p.PupilID)
                                                    && lstSubjectID.Contains(p.SubjectID)
                                                 select new JudgeRecordBO
                                                 {
                                                     JudgeRecordID = p.JudgeRecordID,
                                                     PupilID = p.PupilID,
                                                     SubjectID = p.SubjectID,
                                                     Title = q.Title,
                                                     Semester = p.Semester,
                                                     Judgement = p.Judgement,
                                                     OrderNumber = p.OrderNumber
                                                 }).ToList();
            List<SummedUpRecordBO> iqSummedUpRecord = VSummedUpRecordBusiness.SearchBySchool(entity.SchoolID, searchClass)
                .Where(p => lstPupilID.Contains(p.PupilID) && lstSubjectID.Contains(p.SubjectID))
                .Select(u => new SummedUpRecordBO
                {
                    SummedUpRecordID = u.SummedUpRecordID,
                    SummedUpMark = u.SummedUpMark,
                    JudgementResult = u.JudgementResult,
                    ReTestMark = u.ReTestMark,
                    ReTestJudgement = u.ReTestJudgement,
                    PupilID = u.PupilID,
                    Semester = u.Semester,
                    SubjectID = u.SubjectID,
                    PeriodID = u.PeriodID
                }).Where(o => o.PeriodID == null).ToList();
            List<PupilRankingBO> iqPupilRanking = (from p in VPupilRankingBusiness.SearchBySchool(entity.SchoolID, searchClass)
                                                   join q in CapacityLevelBusiness.All on p.CapacityLevelID equals q.CapacityLevelID into g1
                                                   from j1 in g1.DefaultIfEmpty()
                                                   join r in ConductLevelBusiness.All on p.ConductLevelID equals r.ConductLevelID into g2
                                                   from j2 in g2.DefaultIfEmpty()
                                                   join s in StudyingJudgementBusiness.All on p.StudyingJudgementID equals s.StudyingJudgementID into g3
                                                   from j3 in g3.DefaultIfEmpty()
                                                   where lstPupilID.Contains(p.PupilID)
                                                   select new PupilRankingBO
                                                   {
                                                       PupilRankingID = p.PupilRankingID,
                                                       EducationLevelID = p.EducationLevelID,
                                                       PupilID = p.PupilID,
                                                       Semester = p.Semester,
                                                       CapacityLevelID = p.CapacityLevelID,
                                                       CapacityLevel = j1.CapacityLevel1,
                                                       ConductLevelName = j2.Resolution,
                                                       AverageMark = p.AverageMark,
                                                       PeriodID = p.PeriodID,
                                                       StudyingJudgementID = p.StudyingJudgementID,
                                                       StudyingJudgementResolution = j3.Resolution,
                                                   }).Where(o => o.PeriodID == null).ToList();
            List<PupilEmulationBO> iqPupilEmulation = PupilEmulationBusiness.SearchBySchool(entity.SchoolID, searchClass)
                .Where(p => lstPupilID.Contains(p.PupilID))
                .Select(u => new PupilEmulationBO
                {
                    PupilEmulationID = u.PupilEmulationID,
                    PupilID = u.PupilID,
                    HonourAchivementTypeID = u.HonourAchivementTypeID,
                    HonourAchivementTypeResolution = u.HonourAchivementType.Resolution,
                    Semester = u.Semester
                }).ToList();
            #endregion

            #region fill diem
            int countSubject = lstTotalSubject.Count;
            totalRow = 69;
            startRow = 6;
            int startCol = 0;
            ClassSubjectBO objCS = null;

            int counttmp = 0;
            if (countSubject % 5 == 0)
            {
                counttmp = countSubject / 5;
            }
            else
            {
                counttmp = (countSubject / 5) + 1;
            }

            //int startcopy = 0;
            //int startcopytmp = 0;
            List<ClassSubjectBO> lstCStmp = new List<ClassSubjectBO>();
            //diem mon tinh diem
            List<MarkRecordBO> lsMR = iqMarkRecord.ToList();
            //diem mon nhan xet
            List<JudgeRecordBO> lsJR = iqJudgeRecord.ToList();
            //diem tbm
            List<SummedUpRecordBO> lsSUR = iqSummedUpRecord.ToList();
            //Điểm TBCM, HL, HK, TD
            List<PupilRankingBO> lsPR = iqPupilRanking.ToList();
            //diem tk TD: PupilEmulationBusiness.SearchBySchool(SchoolID, Dictionary)
            List<PupilEmulationBO> lsPE = iqPupilEmulation.ToList();
            List<TeachingAssignmentBO> lstTeachingBo = GetTeachingAssinment(entity);
            //TeachingAssignmentBO objTeachingBO = null;
            Dictionary<string, object> dicSheetDiem = new Dictionary<string, object>();

           /* if (entity.Semester == SystemParamsInFile.SEMESTER_OF_YEAR_FIRST || entity.Semester == GlobalConstants.SEMESTER_OF_YEAR_ALL)//HKI
            {
                if (entity.isBorderMark)
                {
                    FillMark(oBook, sheetDiemHK1, entity, lstSubjectExportID, lstTotalSubject, lstPupilID, lsPP, iqMarkRecord, iqJudgeRecord, iqSummedUpRecord,
                             lstTeachingBo, rowStandar, HeadTeacherName, isSoHCM, GlobalConstants.SEMESTER_OF_YEAR_FIRST, ref positionSheet, ref startNumber, ref lstDataPageNumber);
                }
                else
                {
                    FillMark(sheetDiemHK1NoBorder, entity, lstSubjectExportID, lstTotalSubject, lstPupilID, lsPP, iqMarkRecord, iqJudgeRecord, iqSummedUpRecord,
                             lstTeachingBo, rowStandar, HeadTeacherName, isSoHCM, GlobalConstants.SEMESTER_OF_YEAR_FIRST);
                }
            }

            if (entity.Semester == SystemParamsInFile.SEMESTER_OF_YEAR_SECOND || entity.Semester == GlobalConstants.SEMESTER_OF_YEAR_ALL)//HKII
            {
                if (entity.isBorderMark)
                {
                    FillMark(oBook, sheetDiemHK2, entity, lstSubjectExportID, lstTotalSubject, lstPupilID, lsPP, iqMarkRecord, iqJudgeRecord, iqSummedUpRecord,
                             lstTeachingBo, rowStandar, HeadTeacherName, isSoHCM, GlobalConstants.SEMESTER_OF_YEAR_SECOND, ref positionSheet, ref startNumber, ref lstDataPageNumber);
                }
                else
                {
                    FillMark(sheetDiemHK2NoBorder, entity, lstSubjectExportID, lstTotalSubject, lstPupilID, lsPP, iqMarkRecord, iqJudgeRecord, iqSummedUpRecord,
                             lstTeachingBo, rowStandar, HeadTeacherName, isSoHCM, GlobalConstants.SEMESTER_OF_YEAR_SECOND);
                }
            }*/
            #endregion

            decimal TotalcolWidth = ((decimal)22 * (decimal)4.15);
            double colWidth = (double)Math.Round((decimal)TotalcolWidth / (decimal)(lstTotalSubject.Count + 4), 2);

            #region fill Diem tong ket hoc ky
            if ((SecondSemesterEndDatemonth - FirstSemesterStartmonth + 1) % 2 == 0)
            {
                startNumber++;
            }

            dicSheetDiem = new Dictionary<string, object>();
            SummedUpRecordBO objSUR = null;
            if (entity.Semester == GlobalConstants.SEMESTER_OF_YEAR_FIRST || entity.Semester == GlobalConstants.SEMESTER_OF_YEAR_ALL)
            {
                // Phần bìa ghi điểm                  
                objPageNumber = new VTDataPageNumber();
                objPageNumber.SheetIndex = positionSheet;
                objPageNumber.IsHeaderOrFooter = true;
                objPageNumber.AlignPageNumber = 2;
                objPageNumber.StartPageNumberOfSheet = startNumber;
                lstDataPageNumber.Add(objPageNumber);
                positionSheet += 1;

                // chuyển hàm FillMark xuống này để đảm bảo đánh số trang đúng thứ tự khi chọn cả năm
                if (entity.isBorderMark)
                {
                    FillMark(oBook, sheetDiemHK1, entity, lstSubjectExportID, lstTotalSubject, lstPupilID, lsPP, iqMarkRecord, iqJudgeRecord, iqSummedUpRecord,
                             lstTeachingBo, rowStandar, HeadTeacherName, isSoHCM, GlobalConstants.SEMESTER_OF_YEAR_FIRST, ref positionSheet, ref startNumber, ref lstDataPageNumber);
                }
                else
                {
                    FillMark(sheetDiemHK1NoBorder, entity, lstSubjectExportID, lstTotalSubject, lstPupilID, lsPP, iqMarkRecord, iqJudgeRecord, iqSummedUpRecord,
                             lstTeachingBo, rowStandar, HeadTeacherName, isSoHCM, GlobalConstants.SEMESTER_OF_YEAR_FIRST, ref positionSheet, ref startNumber, ref lstDataPageNumber);
                }

                #region //set do rong cot            
                for (int i = 3; i < 25; i++)
                {
                    sheetDiemTCHK1.SetColumnWidth(i, colWidth);
                }
                string strTitle = "Trong trang này có.......điểm được sửa chữa, trong đó môn : ";
                for (int i = 0; i < lstTotalSubject.Count; i++)
                {
                    objCS = lstTotalSubject[i];
                    strTitle += objCS.DisplayName + "......điểm" + (i < lstTotalSubject.Count - 1 ? ", " : "");
                }
                totalHeigth = ((decimal)60 * (decimal)16.5);
                rowHeigth = (double)Math.Round((decimal)totalHeigth / (decimal)rowStandar, 2);
                this.SetRowHeight(sheetDiemTCHK1, startRow, startRow + rowStandar, rowHeigth);
                this.HidenRow(sheetDiemTCHK1, startRow + rowStandar, 65);
                startCol = 3;
                startRow = 5;
                List<ClassSubjectBO> lstSubjecttmp = lstTotalSubject.Where(p => p.SectionPerWeekFirstSemester > 0).ToList();
                for (int i = 0; i < lsPP.Count; i++)
                {
                    objPP = lsPP[i];
                    sheetDiemTCHK1.SetCellValue(startRow, 1, i + 1);
                    sheetDiemTCHK1.SetCellValue(startRow, 2, objPP.FullName);
                    sheetDiemTCHK1.SetRowHeight(startRow, rowHeigth);
                    startCol = 3;
                    for (int j = 0; j < lstSubjecttmp.Count; j++)
                    {
                        objCS = lstSubjecttmp[j];
                        sheetDiemTCHK1.SetColumnWidth(startCol, colWidth);
                        sheetDiemTCHK1.SetCellValue(3, startCol, (objCS.AppliedType.HasValue && lsAppliedTypeMonTuChon.Contains(objCS.AppliedType.Value)) ? objCS.DisplayName + "(TC)" : objCS.DisplayName);
                        objSUR = lsSUR.Where(o => o.PupilID == objPP.PupilProfileID && o.Semester == GlobalConstants.SEMESTER_OF_YEAR_FIRST).Where(o => o.SubjectID == objCS.SubjectID).FirstOrDefault();
                        if (objCS.IsCommenting == 0)
                        {
                            string diemTBM = objSUR != null && objSUR.SummedUpMark.HasValue ? ReportUtils.ConvertMarkForV(objSUR.SummedUpMark.Value) : "";
                            sheetDiemTCHK1.SetCellValue(startRow, startCol, diemTBM);
                        }
                        else
                        {
                            string diemTBM = objSUR != null ? objSUR.JudgementResult : "";
                            sheetDiemTCHK1.SetCellValue(startRow, startCol, diemTBM);
                        }
                        startCol++;
                    }
                    PupilRankingBO pupilRanking = lsPR.Where(o => o.PupilID == objPP.PupilProfileID && o.Semester == GlobalConstants.SEMESTER_OF_YEAR_FIRST).FirstOrDefault();
                    int startColRanking = this.ColumnNumber("U");
                    if (pupilRanking != null)
                    {
                        string diemTBCM = pupilRanking.AverageMark.HasValue ? ReportUtils.ConvertMarkForV(pupilRanking.AverageMark.Value) : string.Empty;
                        sheetDiemTCHK1.SetCellValue(startRow, startColRanking, diemTBCM);
                        sheetDiemTCHK1.SetCellValue(startRow, startColRanking + 1, pupilRanking.CapacityLevel != null ? UtilsBusiness.ConvertCapacity(pupilRanking.CapacityLevel) : "");
                        sheetDiemTCHK1.SetCellValue(startRow, startColRanking + 2, pupilRanking.ConductLevelName != null ? UtilsBusiness.ConvertCapacity(pupilRanking.ConductLevelName) : "");
                    }
                    PupilEmulationBO pe = lsPE.Where(o => o.PupilID == objPP.PupilProfileID && o.Semester == GlobalConstants.SEMESTER_OF_YEAR_FIRST).FirstOrDefault();
                    sheetDiemTCHK1.SetCellValue(startRow, startColRanking + 3, pe != null ? UtilsBusiness.ConvertCapacity(pe.HonourAchivementTypeResolution) : "");
                    if (objPP.ProfileStatus != GlobalConstants.PUPIL_STATUS_STUDYING && objPP.ProfileStatus != GlobalConstants.PUPIL_STATUS_GRADUATED)
                    {
                        sheetDiemTCHK1.GetRange(startRow, 1, startRow, startColRanking + 2).SetFontStyle(false, null, false, null, true, false);
                    }
                    startRow++;
                }
                sheetDiemTCHK1.SetCellValue("A65", strTitle);
                dicSheetDiem.Add("HeadTeacherName", HeadTeacherName);
                sheetDiemTCHK1.FillVariableValue(dicSheetDiem);
                //an cot thua
                for (int i = startCol; i < 21; i++)
                {
                    sheetDiemTCHK1.HideColumn(i);
                }
                this.HidenRow(sheetDiemTCHK1, 5 + rowStandar, 65);
                sheetDiemTCHK2.PrintArea = "$A$1:$X$68";
                objPageNumber = new VTDataPageNumber();
                objPageNumber.SheetIndex = positionSheet;
                objPageNumber.IsHeaderOrFooter = true;
                objPageNumber.AlignPageNumber = 2;
                objPageNumber.StartPageNumberOfSheet = startNumber;
                lstDataPageNumber.Add(objPageNumber);
                positionSheet += 1;
                startNumber += 1;
                #endregion
            }
            if (entity.Semester == GlobalConstants.SEMESTER_OF_YEAR_SECOND || entity.Semester == GlobalConstants.SEMESTER_OF_YEAR_ALL)
            {
                // Phần bìa ghi điểm    
                objPageNumber = new VTDataPageNumber();
                objPageNumber.SheetIndex = positionSheet;
                objPageNumber.IsHeaderOrFooter = true;
                objPageNumber.AlignPageNumber = 2;
                objPageNumber.StartPageNumberOfSheet = startNumber;
                lstDataPageNumber.Add(objPageNumber);
                positionSheet += 1;

                // chuyển hàm FillMark xuống này để đảm bảo đánh số trang đúng thứ tự khi chọn cả năm
                if (entity.isBorderMark)
                {
                    FillMark(oBook, sheetDiemHK2, entity, lstSubjectExportID, lstTotalSubject, lstPupilID, lsPP, iqMarkRecord, iqJudgeRecord, iqSummedUpRecord,
                             lstTeachingBo, rowStandar, HeadTeacherName, isSoHCM, GlobalConstants.SEMESTER_OF_YEAR_SECOND, ref positionSheet, ref startNumber, ref lstDataPageNumber);
                }
                else
                {
                    FillMark(sheetDiemHK2NoBorder, entity, lstSubjectExportID, lstTotalSubject, lstPupilID, lsPP, iqMarkRecord, iqJudgeRecord, iqSummedUpRecord,
                             lstTeachingBo, rowStandar, HeadTeacherName, isSoHCM, GlobalConstants.SEMESTER_OF_YEAR_SECOND, ref positionSheet, ref startNumber, ref lstDataPageNumber);
                }

                #region//set do rong cot
                for (int i = 3; i < 25; i++)
                {
                    sheetDiemTCHK2.SetColumnWidth(i, colWidth);
                }
                string strTitle = "Trong trang này có.......điểm được sửa chữa, trong đó môn : ";
                for (int i = 0; i < lstTotalSubject.Count; i++)
                {
                    objCS = lstTotalSubject[i];
                    strTitle += objCS.DisplayName + "......điểm" + (i < lstTotalSubject.Count - 1 ? ", " : "");
                }
                totalHeigth = ((decimal)60 * (decimal)16.5);
                rowHeigth = (double)Math.Round((decimal)totalHeigth / (decimal)rowStandar, 2);
                this.SetRowHeight(sheetDiemTCHK2, startRow, startRow + rowStandar, rowHeigth);
                this.HidenRow(sheetDiemTCHK2, startRow + rowStandar, 65);
                startCol = 3;
                startRow = 5;
                List<ClassSubjectBO> lstSubjecttmp = lstTotalSubject.Where(p => p.SectionPerWeekSecondSemester > 0).ToList();
                for (int i = 0; i < lsPP.Count; i++)
                {
                    objPP = lsPP[i];
                    sheetDiemTCHK2.SetCellValue(startRow, 1, i + 1);
                    sheetDiemTCHK2.SetCellValue(startRow, 2, objPP.FullName);
                    sheetDiemTCHK2.SetRowHeight(startRow, rowHeigth);
                    startCol = 3;
                    for (int j = 0; j < lstSubjecttmp.Count; j++)
                    {
                        objCS = lstSubjecttmp[j];
                        sheetDiemTCHK2.SetCellValue(3, startCol, (objCS.AppliedType.HasValue && lsAppliedTypeMonTuChon.Contains(objCS.AppliedType.Value)) ? objCS.DisplayName + "(TC)" : objCS.DisplayName);

                        objSUR = lsSUR.Where(o => o.PupilID == objPP.PupilProfileID && o.Semester == GlobalConstants.SEMESTER_OF_YEAR_SECOND).Where(o => o.SubjectID == objCS.SubjectID).FirstOrDefault();
                        if (objCS.IsCommenting == 0)
                        {
                            string diemTBM = objSUR != null && objSUR.SummedUpMark.HasValue ? ReportUtils.ConvertMarkForV(objSUR.SummedUpMark.Value) : "";
                            sheetDiemTCHK2.SetCellValue(startRow, startCol, diemTBM);
                        }
                        else
                        {
                            string diemTBM = objSUR != null ? objSUR.JudgementResult : "";
                            sheetDiemTCHK2.SetCellValue(startRow, startCol, diemTBM);
                        }
                        startCol++;
                    }
                    PupilRankingBO pupilRanking = lsPR.Where(o => o.PupilID == objPP.PupilProfileID && o.Semester == GlobalConstants.SEMESTER_OF_YEAR_SECOND).FirstOrDefault();
                    startCol = this.ColumnNumber("U");
                    if (pupilRanking != null)
                    {
                        string diemTBCM = pupilRanking.AverageMark.HasValue ? ReportUtils.ConvertMarkForV(pupilRanking.AverageMark.Value) : string.Empty;
                        sheetDiemTCHK2.SetCellValue(startRow, startCol, diemTBCM);
                        sheetDiemTCHK2.SetCellValue(startRow, startCol + 1, pupilRanking.CapacityLevel != null ? UtilsBusiness.ConvertCapacity(pupilRanking.CapacityLevel) : "");
                        sheetDiemTCHK2.SetCellValue(startRow, startCol + 2, pupilRanking.ConductLevelName != null ? UtilsBusiness.ConvertCapacity(pupilRanking.ConductLevelName) : "");
                    }
                    PupilEmulationBO pe = lsPE.Where(o => o.PupilID == objPP.PupilProfileID && o.Semester == GlobalConstants.SEMESTER_OF_YEAR_SECOND).FirstOrDefault();
                    sheetDiemTCHK2.SetCellValue(startRow, startCol + 3, pe != null ? UtilsBusiness.ConvertCapacity(pe.HonourAchivementTypeResolution) : "");
                    if (objPP.ProfileStatus != GlobalConstants.PUPIL_STATUS_STUDYING && objPP.ProfileStatus != GlobalConstants.PUPIL_STATUS_GRADUATED)
                    {
                        sheetDiemTCHK2.GetRange(startRow, 1, startRow, startCol + 2).SetFontStyle(false, null, false, null, true, false);
                    }
                    startRow++;
                }
                sheetDiemTCHK2.SetCellValue("A65", strTitle);
                //IVTRange rangTitleHeadTeacher = sheetDiemTCHK2.GetRange("T65", "X66");
                //rangTitleHeadTeacher.Merge();
                //sheetDiemTCHK2.SetCellValue("T65", "Ký xác nhận của giáo viên chủ nhiệm");
                dicSheetDiem = new Dictionary<string, object>();
                dicSheetDiem.Add("HeadTeacherName", HeadTeacherName);
                sheetDiemTCHK2.FillVariableValue(dicSheetDiem);
                //an cot thua
                for (int i = (3 + lstSubjecttmp.Count); i < 21; i++)
                {
                    sheetDiemTCHK2.HideColumn(i);
                }
                this.HidenRow(sheetDiemTCHK2, 5 + rowStandar, 65);

                sheetDiemTCHK2.PrintArea = "$A$1:$X$68";
                objPageNumber = new VTDataPageNumber();
                objPageNumber.SheetIndex = positionSheet;
                objPageNumber.IsHeaderOrFooter = true;
                objPageNumber.AlignPageNumber = 2;
                objPageNumber.StartPageNumberOfSheet = startNumber;
                lstDataPageNumber.Add(objPageNumber);
                positionSheet += 1;
                startNumber += 1;
                #endregion
            }
         
            #endregion

            #region fill tong ket ca nam
            if (entity.Semester == GlobalConstants.SEMESTER_OF_YEAR_ALL)
            {
                // bìa cuối năm
                objPageNumber = new VTDataPageNumber();
                objPageNumber.SheetIndex = positionSheet;
                objPageNumber.IsHeaderOrFooter = true;
                objPageNumber.AlignPageNumber = 2;
                objPageNumber.StartPageNumberOfSheet = startNumber;
                lstDataPageNumber.Add(objPageNumber);
                positionSheet += 1;
                startNumber += 1;

                Dictionary<int, bool> dicLearingII = new Dictionary<int, bool>();
                Dictionary<int, bool> dicLearingI = new Dictionary<int, bool>();
                lstTotalSubject.ForEach(u =>
                {
                    dicLearingII.Add(u.SubjectID, u.SectionPerWeekSecondSemester.HasValue && u.SectionPerWeekSecondSemester.Value > 0);
                    dicLearingI.Add(u.SubjectID, u.SectionPerWeekFirstSemester.HasValue && u.SectionPerWeekFirstSemester.Value > 0);
                });
                //diem tbm
                List<SummedUpRecordBO> lsSUR_Retest = iqSummedUpRecord.Where(o => o.Semester == SystemParamsInFile.SEMESTER_OF_YEAR_RETEST).ToList();
                List<PupilRankingBO> lsPR_Retest = iqPupilRanking.Where(o => o.Semester > SystemParamsInFile.SEMESTER_OF_YEAR_ALL).ToList();
                List<SummedUpRecordBO> lsSURSemesterI = iqSummedUpRecord.Where(o => o.Semester == SystemParamsInFile.SEMESTER_OF_YEAR_FIRST).ToList();

                //Lấy thông tin điểm danh
                List<PupilAbsence> lstPAR = iqPA.Where(o => o.AbsentDate >= ay.FirstSemesterStartDate && o.AbsentDate <= ay.SecondSemesterEndDate).ToList();

                //diem thi lai
                //Điểm kiểm tra lại, nếu nhiều hơn 3 môn thì dien sang sheeet diem thi lai
                IDictionary<string, object> dicPupilRetest = new Dictionary<string, object>();
                dicPupilRetest["AcademicYearID"] = entity.AcademicYearID;
                dicPupilRetest["ClassID"] = entity.ClassID;
                IQueryable<PupilRetestRegistration> iqPupilRegistration = PupilRetestRegistrationBusiness.SearchBySchool(entity.SchoolID, dicPupilRetest);
                List<PupilRetestRegistration> lstPupilRetestRegistration = iqPupilRegistration.ToList();

                List<ClassSubjectBO> lstSubjectRetest = (from p in iqPupilRegistration
                                                         join r in ClassSubjectBusiness.All on new { p.SubjectID, p.ClassID } equals new { r.SubjectID, r.ClassID }
                                                         join q in SubjectCatBusiness.All on p.SubjectID equals q.SubjectCatID
                                                         where r.Last2digitNumberSchool == partitionId
                                                         orderby q.OrderInSubject
                                                         select new ClassSubjectBO
                                                         {
                                                             SubjectID = q.SubjectCatID,
                                                             DisplayName = q.DisplayName,
                                                             IsCommenting = r.IsCommenting,
                                                             OrderInSubject = q.OrderInSubject
                                                         }).Distinct().ToList();
                lstSubjectRetest = lstSubjectRetest.OrderBy(o => o.OrderInSubject).ToList();
                List<int> lsScRetest = lstSubjectRetest.Select(o => o.SubjectID).ToList();
                //Lay thong tin tot nghiep
                Dictionary<string, object> dicGra = new Dictionary<string, object>();
                dicGra["ClassID"] = entity.ClassID;
                dicGra["AcademicYearID"] = entity.AcademicYearID;
                dicGra["SchoolID"] = entity.SchoolID;
                List<PupilGraduation> lstPgAll = PupilGraduationBusiness.SearchBySchool(entity.SchoolID, dicGra).ToList();
                startRow = 6;
                startCol = 3;
                int countSubjectRetest = lstSubjectRetest.Count > 0 ? lstSubjectRetest.Count : 3;
                TotalcolWidth = ((decimal)28 * (decimal)3);
                colWidth = (double)Math.Round((decimal)TotalcolWidth / (decimal)(lstTotalSubject.Count + countSubjectRetest), 2);
                #region fill ten mon hoc

                //set do rong cac cot
                for (int i = 3; i < 29; i++)
                {
                    sheetTCCaNam.SetColumnWidth(i, colWidth);
                }

                string strTitleEdit = string.Empty;
                strTitleEdit += "Trong trang này có .........điểm được sửa chữa, trong đó môn: ";

                for (int i = 0; i < lstTotalSubject.Count; i++)
                {
                    objCS = lstTotalSubject[i];
                    strTitleEdit += objCS.DisplayName + "......điểm" + (i < lstTotalSubject.Count - 1 ? ", " : "");
                    sheetTCCaNam.SetCellValue(4, startCol, (objCS.AppliedType.HasValue && lsAppliedTypeMonTuChon.Contains(objCS.AppliedType.Value)) ? objCS.DisplayName + "(TC)" : objCS.DisplayName);
                    startCol++;
                }
                //an cot neu < 18 mon
                for (int i = startCol; i < 21; i++)
                {
                    sheetTCCaNam.HideColumn(i);
                }
                startCol = this.ColumnNumber("U");
                sheetTCCaNam.SetColumnWidth(startCol, colWidth);
                startCol++;
                //an cot môn thi lại
                for (int i = (startCol + countSubjectRetest); i < startCol + 8; i++)
                {
                    sheetTCCaNam.HideColumn(i);
                }
                for (int i = 0; i < lstSubjectRetest.Count; i++)
                {
                    objCS = lstSubjectRetest[i];
                    sheetTCCaNam.SetCellValue(5, startCol, objCS.DisplayName);
                    startCol++;
                }
                startCol = this.ColumnNumber("AD");
                sheetTCCaNam.SetColumnWidth(startCol, colWidth);
                #endregion

                int PupilID = 0;
                startCol = 3;
                int startColHLHK = 0;
                string UpClass = string.Empty;
                string NotUpClass = string.Empty;
                string CapRetest = string.Empty;
                string ConductRetest = string.Empty;
                string UpClassRetest = string.Empty;
                string EmulationName = string.Empty;
                int countLL = 0;
                int countLLSTL = 0;
                int countOLL = 0;
                Dictionary<string, object> innerDic = new Dictionary<string, object>();
                innerDic.Add("TotalPupil", countPP);
                int ColEnd = this.ColumnNumber("AZ");
                int startColTBMRetest = 0;
                int startColTBM = 0;
                for (int i = 0; i < lsPP.Count; i++)
                {
                    objPP = lsPP[i];
                    PupilID = objPP.PupilProfileID;
                    sheetTCCaNam.SetCellValue(startRow, 1, (i + 1));
                    sheetTCCaNam.SetCellValue(startRow, 2, objPP.FullName);

                    UpClass = NotUpClass = UpClassRetest = CapRetest = ConductRetest = EmulationName = string.Empty;

                    startColHLHK = this.ColumnNumber("AR");
                    startColTBM = this.ColumnNumber("U");
                    startColTBMRetest = this.ColumnNumber("AD");
                    startCol = 3;
                    bool showRetest = (lstPupilRetestRegistration.Where(o => o.PupilID == PupilID && lsScRetest.Contains(o.SubjectID)).Count() > 0
                                       && lsPR_Retest.Where(p => p.PupilID == PupilID).Count() > 0) ? true : false;
                    if (objPP.ProfileStatus != GlobalConstants.PUPIL_STATUS_STUDYING && objPP.ProfileStatus != GlobalConstants.PUPIL_STATUS_GRADUATED)
                    {
                        sheetTCCaNam.GetRange(startRow, 1, startRow, ColEnd).SetFontStyle(false, null, false, null, true, false);
                    }
                    for (int j = 0; j < lstTotalSubject.Count; j++)
                    {
                        objCS = lstTotalSubject[j];
                        SummedUpRecordBO summed = !dicLearingII[objCS.SubjectID]
                                                    ? lsSURSemesterI.Where(u => u.PupilID == PupilID && u.SubjectID == objCS.SubjectID).FirstOrDefault()
                                                    : (lsSUR.Where(u => u.PupilID == PupilID && u.SubjectID == objCS.SubjectID && u.Semester == SystemParamsInFile.SEMESTER_OF_YEAR_ALL).FirstOrDefault());
                        if (objCS.IsCommenting == 0)
                        {
                            decimal? lsDiemTBM = summed != null ? summed.SummedUpMark : null;
                            string diemTBM = lsDiemTBM != null ? ReportUtils.ConvertMarkForV_SGTGD(lsDiemTBM.Value, isSoHCM) : string.Empty;
                            sheetTCCaNam.SetCellValue(startRow, startCol, diemTBM);
                        }
                        else
                        {
                            string diemTBM = summed != null ? summed.JudgementResult : "";
                            sheetTCCaNam.SetCellValue(startRow, startCol, diemTBM);
                        }
                        startCol++;
                    }
                    PupilRankingBO pupilRanking = lsPR.Where(o => o.PupilID == PupilID && o.Semester == GlobalConstants.SEMESTER_OF_YEAR_ALL).FirstOrDefault();
                    int totalAbsence = lstPAR.Where(o => o.PupilID == PupilID).Count();
                    string diemTBCM = "";
                    string diemTBCM_Retest = "";
                    if (pupilRanking != null)
                    {
                        diemTBCM = pupilRanking.AverageMark != null ? ReportUtils.ConvertMarkForV_SGTGD(pupilRanking.AverageMark.Value, isSoHCM) : string.Empty;
                        sheetTCCaNam.SetCellValue(startRow, startColTBM, diemTBCM);
                        sheetTCCaNam.SetCellValue(startRow, startColHLHK, pupilRanking.CapacityLevel != null ? UtilsBusiness.ConvertCapacity(pupilRanking.CapacityLevel) : "");
                        sheetTCCaNam.SetCellValue(startRow, startColHLHK + 1, pupilRanking.ConductLevelName != null ? UtilsBusiness.ConvertCapacity(pupilRanking.ConductLevelName) : "");
                    }
                    sheetTCCaNam.SetCellValue(startRow, startColHLHK + 2, totalAbsence);

                    PupilEmulationBO pe = lsPE.Where(o => o.PupilID == PupilID && o.Semester == GlobalConstants.SEMESTER_OF_YEAR_ALL).FirstOrDefault();
                    if (pe != null && pe.HonourAchivementTypeResolution != null)
                    {
                        EmulationName = UtilsBusiness.ConvertCapacity(pe.HonourAchivementTypeResolution);
                    }
                    PupilGraduation pg = lstPgAll.FirstOrDefault(o => o.PupilID == PupilID);
                    #region cap 2
                    if (entity.AppliedLevel == GlobalConstants.APPLIED_LEVEL_SECONDARY)
                    {
                        if (pupilRanking != null)
                        {
                            if (pg != null && pg.Status.HasValue && pg.Status == SystemParamsInFile.GRADUATION_STATUS_APPROVE_SUFFICIENT)
                            {
                                UpClass = "Đủ điều kiện tốt nghiệp";
                                countLL++;

                            }
                            else if (pupilRanking.StudyingJudgementID != null && pupilRanking.StudyingJudgementID != 0)
                            {
                                if (pupilRanking.StudyingJudgementID == SystemParamsInFile.STUDYING_JUDGEMENT_UPCLASS || pupilRanking.StudyingJudgementID == SystemParamsInFile.STUDYING_JUDGEMENT_GRADUATION)
                                {
                                    if (pupilRanking.EducationLevelID == GlobalConstants.EDUCATION_LEVEL_ID9)
                                    {
                                        UpClass = "Đủ ĐK xét duyệt TN";
                                    }
                                    else
                                    {
                                        UpClass = UtilsBusiness.ConvertCapacity(pupilRanking.StudyingJudgementResolution);
                                    }
                                    countLL++;
                                }
                                else if (pupilRanking.StudyingJudgementID == SystemParamsInFile.STUDYING_JUDGEMENT_STAYCLASS)
                                {
                                    countOLL++;
                                    NotUpClass = UtilsBusiness.ConvertCapacity(pupilRanking.StudyingJudgementResolution);
                                }
                                else if (pupilRanking.StudyingJudgementID == SystemParamsInFile.STUDYING_JUDGEMENT_RETEST)
                                {
                                    NotUpClass = "Thi lại";
                                }
                                else if (pupilRanking.StudyingJudgementID == SystemParamsInFile.STUDYING_JUDGEMENT_BACKTRAINING)
                                {
                                    NotUpClass = "Rèn luyện lại";
                                }
                            }
                            PupilRankingBO pupilRankingRetest = lsPR_Retest.Where(o => o.PupilID == PupilID).FirstOrDefault();
                            if (pupilRankingRetest != null)
                            {
                                diemTBCM_Retest = pupilRankingRetest.AverageMark != null ? ReportUtils.ConvertMarkForV_SGTGD(pupilRankingRetest.AverageMark.Value, isSoHCM) : string.Empty;
                                CapRetest = pupilRankingRetest.CapacityLevel;
                                ConductRetest = pupilRankingRetest.ConductLevelName;
                                if (pupilRankingRetest.StudyingJudgementID == SystemParamsInFile.STUDYING_JUDGEMENT_UPCLASS || pupilRankingRetest.StudyingJudgementID == SystemParamsInFile.STUDYING_JUDGEMENT_GRADUATION)
                                {
                                    if (pupilRankingRetest.EducationLevelID == GlobalConstants.EDUCATION_LEVEL_ID9)
                                    {
                                        UpClassRetest = "Đủ ĐK xét duyệt TN";
                                    }
                                    else
                                    {
                                        UpClassRetest = pupilRankingRetest.StudyingJudgementResolution;
                                    }
                                    countLL++;
                                    countLLSTL++;
                                }
                                else if (pupilRankingRetest.StudyingJudgementID == SystemParamsInFile.STUDYING_JUDGEMENT_STAYCLASS)
                                {
                                    if (pupilRankingRetest.EducationLevelID != GlobalConstants.EDUCATION_LEVEL_ID9)
                                    {
                                        UpClassRetest = pupilRankingRetest.StudyingJudgementResolution;
                                    }
                                    countOLL++;
                                }
                            }
                            if (showRetest)
                            {
                                startColTBM++;
                                for (int k = 0; k < lstSubjectRetest.Count(); k++)
                                {
                                    ClassSubjectBO thisSubject = lstSubjectRetest[k];
                                    if (thisSubject.IsCommenting == SystemParamsInFile.ISCOMMENTING_TYPE_JUDGE)
                                    {
                                        List<string> lsDiemTBM = lsSUR_Retest.Where(o => o.PupilID == PupilID && o.SubjectID == thisSubject.SubjectID).Select(o => o.ReTestJudgement).ToList();
                                        string diemTBM = string.Join(" ", lsDiemTBM.ToArray());
                                        sheetTCCaNam.SetCellValue(startRow, startColTBM, diemTBM);
                                    }
                                    else
                                    {
                                        decimal? DiemTBM = lsSUR_Retest.Where(o => o.PupilID == PupilID).Where(o => o.SubjectID == thisSubject.SubjectID).Select(o => o.ReTestMark).FirstOrDefault();
                                        string diemTBM = "";
                                        if (DiemTBM != null)
                                        {
                                            diemTBM = ReportUtils.ConvertMarkForV_SGTGD(DiemTBM.Value, isSoHCM);
                                        }
                                        sheetTCCaNam.SetCellValue(startRow, startColTBM, diemTBM);
                                    }
                                    startColTBM++;
                                }
                                PupilRankingBO objRankingRetest = lsPR.FirstOrDefault(o => o.PupilID == PupilID && o.Semester == GlobalConstants.SEMESTER_OF_YEAR_BACKTRAINING);
                                //string TBCMRetest = objRankingRetest != null && objRankingRetest.AverageMark.HasValue ? ReportUtils.ConvertMarkForV_SGTGD(objRankingRetest.AverageMark.Value, isSoHCM) : string.Empty;
                                sheetTCCaNam.SetCellValue(startRow, startColTBMRetest, diemTBCM_Retest);
                            }
                        }
                        sheetTCCaNam.SetCellValue(startRow, startColHLHK + 3, UpClass);
                        sheetTCCaNam.SetCellValue(startRow, startColHLHK + 4, NotUpClass);
                        sheetTCCaNam.SetCellValue(startRow, startColHLHK + 5, CapRetest);
                        sheetTCCaNam.SetCellValue(startRow, startColHLHK + 6, ConductRetest);
                        sheetTCCaNam.SetCellValue(startRow, startColHLHK + 7, UpClassRetest);
                        sheetTCCaNam.SetCellValue(startRow, startColHLHK + 8, EmulationName);
                        startRow++;
                    }
                    #endregion
                    #region cap 3
                    else
                    {
                        if (pupilRanking != null)
                        {
                            //viethd31: neu co thong tin tot nghiep thi lay thong tin tot nghiep
                            if (pg != null && pg.Status.HasValue && pg.Status == SystemParamsInFile.GRADUATION_STATUS_EXAMINATION_SUFFICIENT)
                            {

                                UpClass = "Đủ ĐK dự thi TN";
                                countLL++;
                            }
                            else if (pupilRanking.StudyingJudgementID != null && pupilRanking.StudyingJudgementID != 0)
                            {
                                if (pupilRanking.StudyingJudgementID == SystemParamsInFile.STUDYING_JUDGEMENT_UPCLASS || pupilRanking.StudyingJudgementID == SystemParamsInFile.STUDYING_JUDGEMENT_GRADUATION)
                                {

                                    if (pupilRanking.EducationLevelID == GlobalConstants.EDUCATION_LEVEL_ID12)
                                    {
                                        UpClass = "Đủ ĐK dự thi TN";
                                    }
                                    else
                                    {
                                        UpClass = pupilRanking.StudyingJudgementResolution != null ? pupilRanking.StudyingJudgementResolution : "";
                                    }
                                    countLL++;
                                }
                                else if (pupilRanking.StudyingJudgementID == SystemParamsInFile.STUDYING_JUDGEMENT_STAYCLASS)
                                {
                                    countOLL++;
                                    NotUpClass = pupilRanking.StudyingJudgementResolution;
                                }
                                else
                                {

                                    if (pupilRanking.EducationLevelID != GlobalConstants.EDUCATION_LEVEL_ID12)
                                    {
                                        pupilRanking.StudyingJudgementResolution = pupilRanking.StudyingJudgementResolution != null ? pupilRanking.StudyingJudgementResolution : "";
                                        NotUpClass = pupilRanking.StudyingJudgementResolution;
                                    }
                                }
                            }
                            PupilRankingBO pupilRankingRetest = lsPR_Retest.Where(o => o.PupilID == PupilID).FirstOrDefault();
                            if (pupilRankingRetest != null)
                            {
                                diemTBCM_Retest = pupilRankingRetest.AverageMark != null ? ReportUtils.ConvertMarkForV_SGTGD(pupilRankingRetest.AverageMark.Value, isSoHCM) : string.Empty;
                                CapRetest = pupilRankingRetest.CapacityLevel;
                                ConductRetest = pupilRankingRetest.ConductLevelName;
                                if (pupilRanking.StudyingJudgementID == SystemParamsInFile.STUDYING_JUDGEMENT_RETEST)
                                {
                                    if (pupilRanking.EducationLevelID != GlobalConstants.EDUCATION_LEVEL_ID12)
                                    {
                                        CapRetest = pupilRankingRetest.CapacityLevel != null ? UtilsBusiness.ConvertCapacity(pupilRankingRetest.CapacityLevel) : "";
                                    }
                                }
                                if (pupilRanking.StudyingJudgementID == SystemParamsInFile.STUDYING_JUDGEMENT_BACKTRAINING)
                                {
                                    if (pupilRanking.EducationLevelID != GlobalConstants.EDUCATION_LEVEL_ID12)
                                    {
                                        ConductRetest = pupilRankingRetest.ConductLevelName != null ? UtilsBusiness.ConvertCapacity(pupilRankingRetest.ConductLevelName) : "";
                                    }
                                }

                                if (pupilRankingRetest.StudyingJudgementID > 0)
                                {
                                    if (pupilRankingRetest.StudyingJudgementID == SystemParamsInFile.STUDYING_JUDGEMENT_UPCLASS || pupilRankingRetest.StudyingJudgementID == SystemParamsInFile.STUDYING_JUDGEMENT_GRADUATION)
                                    {
                                        if (pupilRankingRetest.EducationLevelID == 12)
                                        {
                                            UpClassRetest = "Đủ ĐK dự thi TN";
                                        }
                                        else
                                        {
                                            UpClassRetest = pupilRankingRetest.StudyingJudgementResolution;
                                        }
                                        countLL++;
                                        countLLSTL++;
                                    }
                                    else if (pupilRankingRetest.StudyingJudgementID == SystemParamsInFile.STUDYING_JUDGEMENT_STAYCLASS)
                                    {

                                        if (pupilRankingRetest.EducationLevelID != 12)
                                        {
                                            UpClassRetest = pupilRankingRetest.StudyingJudgementResolution;
                                        }
                                        countOLL++;
                                    }
                                }
                            }
                            if (showRetest)
                            {
                                startColTBM++;
                                for (int k = 0; k < lstSubjectRetest.Count(); k++)
                                {
                                    ClassSubjectBO thisSubject = lstSubjectRetest[k];
                                    if (thisSubject.IsCommenting == SystemParamsInFile.ISCOMMENTING_TYPE_JUDGE)
                                    {
                                        List<string> lsDiemTBM = lsSUR_Retest.Where(o => o.PupilID == PupilID && o.SubjectID == thisSubject.SubjectID).Select(o => o.ReTestJudgement).ToList();
                                        string diemTBM = string.Join(" ", lsDiemTBM.ToArray());
                                        sheetTCCaNam.SetCellValue(startRow, startColTBM, diemTBM);
                                    }
                                    else
                                    {
                                        decimal? DiemTBM = lsSUR_Retest.Where(o => o.PupilID == PupilID).Where(o => o.SubjectID == thisSubject.SubjectID).Select(o => o.ReTestMark).FirstOrDefault();
                                        string diemTBM = "";
                                        if (DiemTBM != null)
                                        {
                                            diemTBM = ReportUtils.ConvertMarkForV_SGTGD(DiemTBM.Value, isSoHCM);
                                        }
                                        sheetTCCaNam.SetCellValue(startRow, startColTBM, diemTBM);
                                    }
                                    startColTBM++;
                                }
                                PupilRankingBO objRankingRetest = lsPR.Where(o => o.PupilID == PupilID && o.Semester == GlobalConstants.SEMESTER_OF_YEAR_BACKTRAINING).FirstOrDefault();
                                //string TBCMRetest = objRankingRetest != null && objRankingRetest.AverageMark.HasValue ? ReportUtils.ConvertMarkForV_SGTGD(objRankingRetest.AverageMark.Value, isSoHCM) : string.Empty;
                                sheetTCCaNam.SetCellValue(startRow, startColTBMRetest, diemTBCM_Retest);
                            }
                        }
                        sheetTCCaNam.SetCellValue(startRow, startColHLHK + 3, UpClass);
                        sheetTCCaNam.SetCellValue(startRow, startColHLHK + 4, NotUpClass);
                        sheetTCCaNam.SetCellValue(startRow, startColHLHK + 5, CapRetest);
                        sheetTCCaNam.SetCellValue(startRow, startColHLHK + 6, ConductRetest);
                        sheetTCCaNam.SetCellValue(startRow, startColHLHK + 7, UpClassRetest);
                        sheetTCCaNam.SetCellValue(startRow, startColHLHK + 8, EmulationName);
                        startRow++;
                    }
                    #endregion
                }
                totalHeigth = ((decimal)60 * (decimal)16.5);
                rowHeigth = (double)Math.Round((decimal)totalHeigth / (decimal)rowStandar, 2);
                this.SetRowHeight(sheetTCCaNam, 6, 6 + rowStandar, rowHeigth);
                this.HidenRow(sheetTCCaNam, 6 + rowStandar, 66);
                innerDic.Add("CountUpClass", countLL);
                innerDic.Add("CountNotUpClass", countOLL);
                innerDic.Add("CountUpClassRetest", countLLSTL);
                innerDic.Add("HeadMasterName", HeadMasterName);
                innerDic.Add("HeadTeacherName", HeadTeacherName);
                innerDic.Add("HeadTitleEdit", strTitleEdit);
                sheetTCCaNam.FillVariableValue(innerDic);
             
                objPageNumber = new VTDataPageNumber();
                objPageNumber.SheetIndex = positionSheet;
                objPageNumber.IsHeaderOrFooter = true;
                objPageNumber.AlignPageNumber = 2;
                objPageNumber.StartPageNumberOfSheet = startNumber;
                lstDataPageNumber.Add(objPageNumber);
                positionSheet += 1;
                startNumber += 2;
            }
            #endregion
          
            #region fill sheet nhan xet hieu truong
            List<UsingMarkbookComment> lstUsingMarkbookComment = UsingMarkbookCommentBusiness.All.Where(o => o.AcademicYearID == entity.AcademicYearID
                && o.ClassID == entity.ClassID).ToList();
            string comment = string.Empty;
            UsingMarkbookComment umc;

            #region // HK1
            if (entity.Semester == SystemParamsInFile.SEMESTER_OF_YEAR_FIRST || entity.Semester == GlobalConstants.SEMESTER_OF_YEAR_ALL)
            {
                //thang 8
                umc = lstUsingMarkbookComment.FirstOrDefault(o => o.Month == 8);
                comment = umc != null ? umc.ReviewComment : string.Empty;
                sheetNhanXetCuaHT1.SetCellValue("B4", comment);
                //thang 9
                umc = lstUsingMarkbookComment.FirstOrDefault(o => o.Month == 9);
                comment = umc != null ? umc.ReviewComment : string.Empty;
                sheetNhanXetCuaHT1.SetCellValue("B13", comment);
                //thang 10
                umc = lstUsingMarkbookComment.FirstOrDefault(o => o.Month == 10);
                comment = umc != null ? umc.ReviewComment : string.Empty;
                sheetNhanXetCuaHT1.SetCellValue("B22", comment);
                //thang 11
                umc = lstUsingMarkbookComment.FirstOrDefault(o => o.Month == 11);
                comment = umc != null ? umc.ReviewComment : string.Empty;
                sheetNhanXetCuaHT1.SetCellValue("B31", comment);
                //thang 12
                umc = lstUsingMarkbookComment.FirstOrDefault(o => o.Month == 12);
                comment = umc != null ? umc.ReviewComment : string.Empty;
                sheetNhanXetCuaHT1.SetCellValue("B40", comment);

                objPageNumber = new VTDataPageNumber();
                objPageNumber.SheetIndex = positionSheet;
                objPageNumber.IsHeaderOrFooter = true;
                objPageNumber.AlignPageNumber = 2;
                objPageNumber.StartPageNumberOfSheet = startNumber;
                lstDataPageNumber.Add(objPageNumber);
                positionSheet += 1;
                startNumber += 1;
            }        
            #endregion

            #region // HK2
            if (entity.Semester == SystemParamsInFile.SEMESTER_OF_YEAR_SECOND || entity.Semester == GlobalConstants.SEMESTER_OF_YEAR_ALL)//HKII
            {
                //thang 1
                umc = lstUsingMarkbookComment.FirstOrDefault(o => o.Month == 1);
                comment = umc != null ? umc.ReviewComment : string.Empty;
                sheetNhanXetCuaHT2.SetCellValue("B4", comment);
                //thang 2
                umc = lstUsingMarkbookComment.FirstOrDefault(o => o.Month == 2);
                comment = umc != null ? umc.ReviewComment : string.Empty;
                sheetNhanXetCuaHT2.SetCellValue("B13", comment);
                //thang 3
                umc = lstUsingMarkbookComment.FirstOrDefault(o => o.Month == 3);
                comment = umc != null ? umc.ReviewComment : string.Empty;
                sheetNhanXetCuaHT2.SetCellValue("B22", comment);
                //thang 4
                umc = lstUsingMarkbookComment.FirstOrDefault(o => o.Month == 4);
                comment = umc != null ? umc.ReviewComment : string.Empty;
                sheetNhanXetCuaHT2.SetCellValue("B31", comment);
                //thang 5
                umc = lstUsingMarkbookComment.FirstOrDefault(o => o.Month == 5);
                comment = umc != null ? umc.ReviewComment : string.Empty;
                sheetNhanXetCuaHT2.SetCellValue("B40", comment);

                objPageNumber = new VTDataPageNumber();
                objPageNumber.SheetIndex = positionSheet;
                objPageNumber.IsHeaderOrFooter = true;
                objPageNumber.AlignPageNumber = 2;
                objPageNumber.StartPageNumberOfSheet = startNumber;
                lstDataPageNumber.Add(objPageNumber);
                positionSheet += 1;
                startNumber += 1;
            }           
            #endregion        
            #endregion

            #region // Giao vien bo mon
            FillSheetTeachingAssignment(sheetGiaoVienBM, entity, lstTeachingBo);
            objPageNumber = new VTDataPageNumber();
            objPageNumber.SheetIndex = positionSheet;
            objPageNumber.IsHeaderOrFooter = true;
            objPageNumber.AlignPageNumber = 2;
            objPageNumber.StartPageNumberOfSheet = startNumber;
            lstDataPageNumber.Add(objPageNumber);
            #endregion

            #region //Xoa sheet du
            if (entity.Semester == GlobalConstants.SEMESTER_OF_YEAR_FIRST)
            {
                if (entity.isBorderMark)
                {
                    sheetDiemHK1NoBorder.Delete();
                }
                else
                {
                    sheetDiemHK1NoBorder.Name = "DiemHKI";                 
                }
                sheetDiemHK2NoBorder.Delete();
                sheetBiaGhiDiemHK2.Delete();
                sheetBiaGhiDiemCN.Delete();
                sheetDiemTCHK2.Delete();
                sheetNhanXetCuaHT2.Delete();
                sheetTCCaNam.Delete();
                sheetNhanXetCuaHT1.Name = "NhanXetCuaHT";
            }
            else if (entity.Semester == GlobalConstants.SEMESTER_OF_YEAR_SECOND)
            {
                if (entity.isBorderMark)
                {
                    sheetDiemHK2NoBorder.Delete();
                }
                else
                {                   
                    sheetDiemHK2NoBorder.Name = "DiemHKII";
                }
                sheetBiaGhiDiemHK1.Delete();
                sheetBiaGhiDiemCN.Delete();             
                sheetDiemHK1NoBorder.Delete();
                sheetDiemTCHK1.Delete();
                sheetTCCaNam.Delete();
                sheetNhanXetCuaHT1.Delete();
                sheetNhanXetCuaHT2.Name = "NhanXetCuaHT";
            }
            else
            {
                if (entity.isBorderMark)
                {
                    sheetDiemHK1NoBorder.Delete();
                    sheetDiemHK2NoBorder.Delete();
                }
                else
                {                   
                    sheetDiemHK1NoBorder.Name = "DiemHKI";
                    sheetDiemHK2NoBorder.Name = "DiemHKII";
                }
            }

            sheetDiemHK1.Delete();
            sheetDiemHK2.Delete();
            sheetDiemDanhTemplate.Delete();
            #endregion
            if (entity.fillPageNumber)
            {
                return oBook.FillPageNumber(lstDataPageNumber);
            }

            return oBook.ToStream();
        }

        private void FillMark(IVTWorkbook oBook, IVTWorksheet sheetDiemHK, MasterBook entity, List<int> lstSubjectExportID, List<ClassSubjectBO> lstTotalSubject, List<int> lstPupilID, List<PupilProfileBO> lsPP,
               List<MarkRecordBO> lstMarkRecord, List<JudgeRecordBO> lstJudgeRecord, List<SummedUpRecordBO> lstSummedUpRecord, List<TeachingAssignmentBO> lstTeachingBo,
               int rowStandar, string HeadTeacherName, bool isSoHCM, int semesterId, ref int positionSheet, ref int startNumber, ref List<VTDataPageNumber> lstDataPageNumber)
        {
            //Danh sach mon hoc cua lop
            List<int> lstSubjectID = lstTotalSubject.Select(p => p.SubjectID).Distinct().ToList();
            List<int> lsAppliedTypeMonTuChon = new List<int>() { SystemParamsInFile.APPLIED_TYPE_VOLUNTARY, SystemParamsInFile.APPLIED_TYPE_VOLUNTARY_PLUS_MARK, SystemParamsInFile.APPLIED_TYPE_VOLUNTARY_PRIORITY };

            int countSubject = lstTotalSubject.Count;
            int countMaxMark_M = 0;   // Tong so cot diem lon nhat cua diem M
            int countMaxMark_V = 0;   // Tong so cot diem lon nhat cua diem V
            int countMaxMark_V2 = 0;  // Tong so cot diem lon nhat cua diem (V)
            int startRow = 6;
            int startCol = 0;
            ClassSubjectBO objCS = null;
            PupilProfileBO objPP = null;
            decimal totalHeigth = ((decimal)60 * (decimal)17.5);
            double rowHeigth = 0;
            //Tao vung template
            IVTRange RangeDiemHK = entity.isBorderMark ? sheetDiemHK.GetRange("A2", "CZ69") : sheetDiemHK.GetRange("A2", "CZ69");

            int counttmp = 0;
            if (countSubject % 5 == 0)
            {
                counttmp = countSubject / 5;
            }
            else
            {
                counttmp = (countSubject / 5) + 1;
            }

            List<ClassSubjectBO> lstCStmp = new List<ClassSubjectBO>();
            //diem mon tinh diem
            List<MarkRecordBO> lsMR = lstMarkRecord.ToList();
            //diem mon nhan xet
            List<JudgeRecordBO> lsJR = lstJudgeRecord.ToList();
            //diem tbm
            List<SummedUpRecordBO> lsSUR = lstSummedUpRecord.ToList();

            Dictionary<string, object> dicSheetDiem = new Dictionary<string, object>();
            List<MarkRecordBO> mrOfThisPupil = null;
            List<JudgeRecordBO> jrOfThisPupil = null;
            List<decimal?> lsdiemMieng = new List<decimal?>();
            List<decimal?> lsdiem15ph = new List<decimal?>();
            List<decimal?> lsdiem1t = new List<decimal?>();
            List<decimal?> lsdiemhk = new List<decimal?>();
            List<string> strlsdiemMieng = new List<string>();
            List<string> strlsdiem15ph = new List<string>();
            List<string> strlsdiem1t = new List<string>();
            List<string> strlsdiemhk = new List<string>();

            string diemMieng = string.Empty;
            string diem15 = string.Empty;
            string diem1Tiet = string.Empty;
            string diemHK = string.Empty;
            int colM = 0;
            int colP = 0;
            int colV = 0;
            totalHeigth = ((decimal)60 * (decimal)16.5);
            rowHeigth = (double)Math.Round((decimal)totalHeigth / (decimal)rowStandar, 2);
            startRow = 6;

            this.SetRowHeight(sheetDiemHK, startRow, startRow + rowStandar, rowHeigth);
            this.HidenRow(sheetDiemHK, startRow + rowStandar, 66);
            if (entity.isBorderMark)
            {
                this.HidenColAndSetColWidth(sheetDiemHK, entity.InterviewMark, entity.WritingMark, entity.TwiceCoeffiecientMark);
            }

            List<int> lstIndexColumn = new List<int>(); // vị trí show các cột
            int totalSubjectInSheet = 0;
            VTDataPageNumber objPageNumber = null;
            startNumber++;
            for (int i = 0; i < counttmp; i++)
            {          
                objPageNumber = new VTDataPageNumber();
                objPageNumber.SheetIndex = positionSheet;
                objPageNumber.IsHeaderOrFooter = true;
                objPageNumber.AlignPageNumber = 2;
                objPageNumber.StartPageNumberOfSheet = startNumber;
                lstDataPageNumber.Add(objPageNumber);
                positionSheet += 1;
                startNumber += 2;

                IVTWorksheet sheetFill = oBook.CopySheetToBefore(sheetDiemHK, sheetDiemHK);
                sheetFill.Name = string.Format("Diem_HK{0} ({1})", (semesterId == 1? "I" : "II"), (i + 1));
                sheetFill.CopyPasteSameSize(RangeDiemHK, "A2");
                this.SetRowHeight(sheetFill, startRow, startRow + rowStandar, rowHeigth);

                lstCStmp = lstTotalSubject.Skip((i * 5)).Take(5).ToList();
                int coltmp = 0;
                dicSheetDiem = new Dictionary<string, object>();
                sheetFill.SetCellValue("A66", "Trong trang này có .........điểm được sửa chữa, trong đó môn: ${SubjectName1}.......điểm, ${SubjectName2}......điểm.");
                sheetFill.SetCellValue("AR66", "Trong trang này có .........điểm được sửa chữa, trong đó môn:  ${SubjectName3}.......điểm,  ${SubjectName4}......điểm,  ${SubjectName5}......điểm.");
                totalSubjectInSheet = lstCStmp.Count();
                #region // Fill mon             
                for (int j = 0; j < lstCStmp.Count; j++)
                {
                    #region
                    string confirmHeadTeacherTitle = "Xác nhận điểm của GV ${SubjectName" + (j + 1) + "}";
                    string confirmHeadTeacherName = "${HeadTeacherName" + (j + 1) + "}";
                    objCS = lstCStmp[j];

                    if (j < 2)
                    {
                        startCol = 3 + 20 * j;
                        if (entity.isGVBM)
                        {
                            sheetFill.SetCellValue(67, startCol, confirmHeadTeacherTitle);
                            sheetFill.SetCellValue(69, startCol, confirmHeadTeacherName);

                            dicSheetDiem.Add("HeadTeacherName" + (j + 1), this.GetHeadTeacherName(lstTeachingBo, HeadTeacherName, semesterId, objCS.SubjectID, entity.isGVBM));
                        }
                        else
                        {
                            int col1 = 3 + 20 * (1);
                            if (j == 0)
                            {
                                sheetFill.SetCellValue(67, col1, "Ký xác nhận của GVCN");
                                sheetFill.SetCellValue(69, col1, HeadTeacherName);
                            }
                        }

                    }
                    else
                    {
                        startCol = 5 + 20 * j;
                        if (entity.isGVBM)
                        {
                            sheetFill.SetCellValue(67, j == 2 ? startCol - 1 : startCol, confirmHeadTeacherTitle);
                            sheetFill.SetCellValue(69, j == 2 ? startCol - 1 : startCol, confirmHeadTeacherName);

                            dicSheetDiem.Add("HeadTeacherName" + (j + 1), this.GetHeadTeacherName(lstTeachingBo, HeadTeacherName, semesterId, objCS.SubjectID, entity.isGVBM));
                        }
                        else//giao vien CN
                        {
                            int col1 = 5 + 20 * (4);
                            if (j == 2)
                            {
                                sheetFill.SetCellValue(67, col1, "Ký xác nhận của GVCN");
                                sheetFill.SetCellValue(69, col1, HeadTeacherName);
                            }
                        }
                    }
                    dicSheetDiem.Add("SubjectName" + (j + 1), objCS.SubjectName);
                    sheetFill.SetCellValue(3, startCol, (objCS.AppliedType.HasValue && lsAppliedTypeMonTuChon.Contains(objCS.AppliedType.Value)) ? objCS.DisplayName + "(TC)" : objCS.DisplayName);
                    #endregion

                    #region // fill hoc sinh
                    int temp = 0;
                    for (int k = 0; k < lsPP.Count; k++)
                    {
                        #region // Fill thong tin, get data diem
                        objPP = lsPP[k];
                        coltmp = startCol;
                        sheetFill.SetCellValue((6 + k), 1, (k + 1));
                        sheetFill.SetCellValue((6 + k), 2, objPP.FullName);
                        colM = colP = colV = 0;
                        if (objCS.IsCommenting == 0)
                        {
                            mrOfThisPupil = lsMR.Where(o => o.PupilID == objPP.PupilProfileID && o.Semester == semesterId).ToList();
                            lsdiemMieng = mrOfThisPupil.Where(o => o.Title == GlobalConstants.MARK_TYPE_M)
                                                                .Where(o => o.SubjectID == objCS.SubjectID)
                                                                .OrderBy(o => o.OrderNumber)
                                                                .Select(o => o.Mark).ToList();
                            lsdiem15ph = mrOfThisPupil.Where(o => o.Title == GlobalConstants.MARK_TYPE_P)
                                                                    .Where(o => o.SubjectID == objCS.SubjectID)
                                                                    .OrderBy(o => o.OrderNumber)
                                                                    .Select(o => o.Mark).ToList();
                            lsdiem1t = mrOfThisPupil.Where(o => o.Title == GlobalConstants.MARK_TYPE_V)
                                                                    .Where(o => o.SubjectID == objCS.SubjectID)
                                                                    .OrderBy(o => o.OrderNumber)
                                                                    .Select(o => o.Mark).ToList();
                            lsdiemhk = mrOfThisPupil.Where(o => o.Title == GlobalConstants.MARK_TYPE_HK)
                                                                    .Where(o => o.SubjectID == objCS.SubjectID)
                                                                    .Select(o => o.Mark).ToList();
                            strlsdiemMieng = ReportUtils.ConvertListMarkForM(lsdiemMieng, false);
                            strlsdiem15ph = ReportUtils.ConvertListMarkForV(lsdiem15ph, false);
                            strlsdiem1t = ReportUtils.ConvertListMarkForV(lsdiem1t, false);
                            strlsdiemhk = ReportUtils.ConvertListMarkForV(lsdiemhk, false);
                        }
                        else
                        {
                            jrOfThisPupil = lsJR.Where(o => o.PupilID == objPP.PupilProfileID && o.Semester == semesterId).ToList();
                            strlsdiemMieng = jrOfThisPupil.Where(o => o.Title == GlobalConstants.MARK_TYPE_M)
                                                                .Where(o => o.SubjectID == objCS.SubjectID)
                                                                .OrderBy(o => o.OrderNumber)
                                                                .Select(o => o.Judgement).ToList();
                            strlsdiem15ph = jrOfThisPupil.Where(o => o.Title == GlobalConstants.MARK_TYPE_P)
                                                                    .Where(o => o.SubjectID == objCS.SubjectID)
                                                                    .OrderBy(o => o.OrderNumber)
                                                                    .Select(o => o.Judgement).ToList();
                            strlsdiem1t = jrOfThisPupil.Where(o => o.Title == GlobalConstants.MARK_TYPE_V)
                                                                    .Where(o => o.SubjectID == objCS.SubjectID)
                                                                    .OrderBy(o => o.OrderNumber)
                                                                    .Select(o => o.Judgement).ToList();
                            strlsdiemhk = jrOfThisPupil.Where(o => o.Title == GlobalConstants.MARK_TYPE_HK)
                                                                    .Where(o => o.SubjectID == objCS.SubjectID)
                                                                    .Select(o => o.Judgement).ToList();
                        }
                        #endregion

                        diemHK = string.Join(" ", strlsdiemhk.ToArray());

                        SummedUpRecordBO objSummed = lsSUR.FirstOrDefault(o => o.PupilID == objPP.PupilProfileID && o.SubjectID == objCS.SubjectID && o.Semester == semesterId);
                        string diemTBM = "";
                        if (objCS.IsCommenting == GlobalConstants.ISCOMMENTING_TYPE_MARK)
                        {
                            diemTBM = (objSummed != null && objSummed.SummedUpMark.HasValue) ? ReportUtils.ConvertMarkForV(objSummed.SummedUpMark.Value) : string.Empty;
                        }
                        else
                        {
                            diemTBM = (objSummed != null) ? objSummed.JudgementResult : "";
                        }

                        #region // Fill diem
                        if (entity.isBorderMark)//Ke o
                        {                         
                            colM = startCol;
                            temp = 0;
                            for (int m = 0; m < strlsdiemMieng.Count; m++)
                            {
                                sheetFill.SetCellValue((6 + k), colM, strlsdiemMieng[m]);
                                colM++;

                                temp++;
                                if (temp > countMaxMark_M)
                                    countMaxMark_M = temp;
                            }
                            colP = startCol + 5;
                            temp = 0;
                            for (int m = 0; m < strlsdiem15ph.Count; m++)
                            {
                                sheetFill.SetCellValue((6 + k), colP, strlsdiem15ph[m]);
                                colP++;

                                temp++;
                                if (temp > countMaxMark_V)
                                    countMaxMark_V = temp;
                            }
                            colV = startCol + 10;
                            temp = 0;

                            for (int m = 0; m < strlsdiem1t.Count; m++)
                            {
                                sheetFill.SetCellValue((6 + k), colV, strlsdiem1t[m]);
                                colV++;

                                temp++;
                                if (temp > countMaxMark_V2)
                                    countMaxMark_V2 = temp;
                            }
                            sheetFill.SetCellValue((6 + k), startCol + 18, diemHK);
                            sheetFill.SetCellValue((6 + k), startCol + 19, diemTBM);
                        }                       
                        #endregion

                        if (objPP.ProfileStatus != GlobalConstants.PUPIL_STATUS_STUDYING && objPP.ProfileStatus != GlobalConstants.PUPIL_STATUS_GRADUATED)
                        {
                            sheetFill.GetRange((6 + k), 1, (6 + k), startCol + 19).SetFontStyle(false, null, false, null, true, false);
                        }
                        //startcopytmp++;
                    }
                    #endregion               

                    if (entity.isBorderMark)
                    {
                        if(j < 2) // 0 - 1
                        {
                            int indexColumn = ((j < 1 ? 3 : 23)  + countMaxMark_M);
                            this.SetIndexColumn(ref lstIndexColumn, (j < 1 ? 3 : 23), indexColumn);

                            indexColumn = (j < 1 ? 8 : 28) + (countMaxMark_V > 2 ? countMaxMark_V : 2);
                            this.SetIndexColumn(ref lstIndexColumn, (j < 1 ? 8 : 28), indexColumn);

                            indexColumn = (j < 1 ? 13 : 33) + (countMaxMark_V2 > 3 ? countMaxMark_V2 : 3);
                            this.SetIndexColumn(ref lstIndexColumn, (j < 1 ? 13 : 33), indexColumn);
                        }
                        else // 2 - 3 - 4
                        {
                            if (j < 4)
                            {
                                int indexColumn = ((j < 3 ? 45 : 65) + countMaxMark_M);
                                this.SetIndexColumn(ref lstIndexColumn, (j < 3 ? 45 : 65), indexColumn);

                                indexColumn = (j < 3 ? 50 : 70) + (countMaxMark_V > 2 ? countMaxMark_V : 2);
                                this.SetIndexColumn(ref lstIndexColumn, (j < 3 ? 50 : 70), indexColumn);

                                indexColumn = (j < 3 ? 55 : 75) + (countMaxMark_V2 > 3 ? countMaxMark_V2 : 3);
                                this.SetIndexColumn(ref lstIndexColumn, (j < 3 ? 55 : 75), indexColumn);
                            }
                            else
                            {
                                int indexColumn = (85 + countMaxMark_M);
                                this.SetIndexColumn(ref lstIndexColumn, 85, indexColumn);

                                indexColumn = 90 + (countMaxMark_V > 2 ? countMaxMark_V : 2);
                                this.SetIndexColumn(ref lstIndexColumn, 90, indexColumn);

                                indexColumn = 95 + (countMaxMark_V2 > 3 ? countMaxMark_V2 : 3);
                                this.SetIndexColumn(ref lstIndexColumn, 95, indexColumn);
                            }
                        }
                     
                        countMaxMark_M = 0;
                        countMaxMark_V = 0;
                        countMaxMark_V2 = 0;
                    }
                }
                #endregion

                if (totalSubjectInSheet < 5)
                {
                    for (int j = totalSubjectInSheet; j < 5; j++)
                    {
                        if (j < 2) // 0 - 1
                        {
                            int indexColumn = ((j < 1 ? 3 : 23));
                            this.SetIndexColumn(ref lstIndexColumn, (j < 1 ? 3 : 23), indexColumn);

                            indexColumn = (j < 1 ? 8 : 28) + 2;
                            this.SetIndexColumn(ref lstIndexColumn, (j < 1 ? 8 : 28), indexColumn);

                            indexColumn = (j < 1 ? 13 : 33) + 3;
                            this.SetIndexColumn(ref lstIndexColumn, (j < 1 ? 13 : 33), indexColumn);
                        }
                        else // 2 - 3 - 4
                        {
                            if (j < 4)
                            {
                                int indexColumn = ((j < 3 ? 45 : 65) + 0);
                                this.SetIndexColumn(ref lstIndexColumn, (j < 3 ? 45 : 65), indexColumn);

                                indexColumn = (j < 3 ? 50 : 70) + 2;
                                this.SetIndexColumn(ref lstIndexColumn, (j < 3 ? 50 : 70), indexColumn);

                                indexColumn = (j < 3 ? 55 : 75) + 3;
                                this.SetIndexColumn(ref lstIndexColumn, (j < 3 ? 55 : 75), indexColumn);
                            }
                            else
                            {
                                int indexColumn = (85 + 0);
                                this.SetIndexColumn(ref lstIndexColumn, 85, indexColumn);

                                indexColumn = 90 + 2;
                                this.SetIndexColumn(ref lstIndexColumn, 90, indexColumn);

                                indexColumn = 95 + 3;
                                this.SetIndexColumn(ref lstIndexColumn, 95, indexColumn);
                            }
                        }
                    }
                }

                if (entity.isBorderMark)
                {
                    sheetFill = SetAutoColumnWidth(sheetFill, lstIndexColumn);
                    lstIndexColumn = new List<int>();
                }
               
                sheetFill.FillVariableValue(dicSheetDiem);
                //sheetFill.FitSheetOnTwoPage = true;
                sheetFill.PrintArea = "$A$1:$CZ$69";
                sheetFill.SetBreakPageV(43);
            }
        }

        private void SetIndexColumn(ref List<int> lstIndexColumn, int startColumn, int endColumn)
        {
            if (startColumn == endColumn)
            {
                lstIndexColumn.Add(startColumn);
            }

            for (int m = startColumn; m < endColumn; m++)
            {
                lstIndexColumn.Add(m);
            }
        }

        private IVTWorksheet SetAutoColumnWidth(IVTWorksheet sheetFill, List<int> lstIndexColumn)
        {
            var lstTemp1 = lstIndexColumn.Where(x => x <= 40).ToList(); // khu vuc 2 mon
            var lstTemp2 = lstIndexColumn.Where(x => x >= 45).ToList(); // khu vuc 3 mon

            double columnWidthMark = 0;
            double columnWidthTBM = 0;
            this.GetColumnWidthByTotalCloumns(lstTemp1.Count(), 1, ref columnWidthMark, ref columnWidthTBM); 

            int value = 0;
            if (lstTemp1.Count() > 0)
            {
                for (int i = 3; i <= 42; i++)
                {
                    if (i == 21 || i == 22 || i == 41 || i == 42)
                    {
                        sheetFill.SetColumnWidth(i, columnWidthTBM);
                        continue;
                    }

                    value = lstTemp1.Where(x => x == i).FirstOrDefault();
                    sheetFill.SetColumnWidth(i, (value > 0 ? columnWidthMark : 0));
                    value = 0;
                }
            }

            columnWidthMark = 0;
            columnWidthTBM = 0;
            this.GetColumnWidthByTotalCloumns(lstTemp2.Count(), 2, ref columnWidthMark, ref columnWidthTBM);          
            if (lstTemp2.Count() > 0)
            {
                for (int i = 45; i <= 104; i++)
                {
                    if (i == 63 || i == 64 || i == 83 || i == 84 || i == 103 || i == 104)
                    {
                        sheetFill.SetColumnWidth(i, columnWidthTBM);
                        continue;                       
                    }
                       
                    value = lstTemp2.Where(x => x == i).FirstOrDefault();
                    sheetFill.SetColumnWidth(i, (value > 0 ? columnWidthMark : 0));
                    value = 0;
                }   
            }

            return sheetFill;
        }

        private void GetColumnWidthByTotalCloumns(int totalColumns, int area, ref double columnWidthMark, ref double columnWidthTBM)
        {
            if (area == 1)
            {
                switch (totalColumns)
                {
                    case 12: columnWidthMark = 5.71; columnWidthTBM = 5.71; break;
                    case 13: columnWidthMark = 5.29; columnWidthTBM = 5.29; break;
                    case 14: columnWidthMark = 5; columnWidthTBM = 5; break;
                    case 15: columnWidthMark = 4.71; columnWidthTBM = 4.71; break;
                    case 16: columnWidthMark = 4.43; columnWidthTBM = 4.43; break;
                    case 17: columnWidthMark = 4.14; columnWidthTBM = 4.14; break;
                    case 18: columnWidthMark = 3.86; columnWidthTBM = 4; break;
                    case 19: columnWidthMark = 3.71; columnWidthTBM = 3.86; break;
                    case 20: columnWidthMark = 3.57; columnWidthTBM = 3.57; break;
                    case 21: columnWidthMark = 3.43; columnWidthTBM = 3.43; break;
                    case 22: columnWidthMark = 3.14; columnWidthTBM = 3.57; break;
                    case 23: columnWidthMark = 3; columnWidthTBM = 3.57; break;
                    case 24: columnWidthMark = 2.86; columnWidthTBM = 3.57; break;
                    case 25: columnWidthMark = 2.71; columnWidthTBM = 3.57; break;
                    case 26: columnWidthMark = 2.57; columnWidthTBM = 3.57; break;
                    case 27: columnWidthMark = 2.43; columnWidthTBM = 3.57; break;
                    case 28: columnWidthMark = 2.29; columnWidthTBM = 3.57; break;
                    case 29: columnWidthMark = 2.29; columnWidthTBM = 3.57; break;
                    case 30: columnWidthMark = 2.14; columnWidthTBM = 3.86; break;
                    case 31: columnWidthMark = 2.14; columnWidthTBM = 3.43; break;
                    case 32: columnWidthMark = 2; columnWidthTBM = 3.43; break;
                    case 33: columnWidthMark = 2; columnWidthTBM = 3; break;
                    case 34: columnWidthMark = 1.86; columnWidthTBM = 3.29; break;
                    case 35: columnWidthMark = 1.86; columnWidthTBM = 3; break;
                    default: columnWidthMark = 1.71; columnWidthTBM = 3.71; break; // case: 36
                }
            }
            else
            {
                switch (totalColumns)
                {
                    case 18: columnWidthMark = 4.43; columnWidthTBM = 4.57; break;
                    case 19: columnWidthMark = 4.29; columnWidthTBM = 4.29; break;
                    case 20: columnWidthMark = 4; columnWidthTBM = 4; break;
                    case 21: columnWidthMark = 3.86; columnWidthTBM = 4; break;
                    case 22: columnWidthMark = 3.71; columnWidthTBM = 3.71; break;
                    case 23: columnWidthMark = 3.57; columnWidthTBM = 3.57; break;
                    case 24: columnWidthMark = 3.43; columnWidthTBM = 3.43; break;
                    case 25: columnWidthMark = 3.29; columnWidthTBM = 3.29; break;
                    case 26: columnWidthMark = 3.14; columnWidthTBM = 3.14; break;
                    case 27: columnWidthMark = 3; columnWidthTBM = 3.29; break;
                    case 28: columnWidthMark = 3; columnWidthTBM = 3; break;
                    case 29: columnWidthMark = 2.86; columnWidthTBM = 2.86; break;
                    case 30: columnWidthMark = 2.71; columnWidthTBM = 3; break;
                    case 31: columnWidthMark = 2.43; columnWidthTBM = 3.57; break;
                    case 32: columnWidthMark = 2.43; columnWidthTBM = 3.14; break;
                    case 33: columnWidthMark = 2.29; columnWidthTBM = 3.43; break;
                    case 34: columnWidthMark = 2.29; columnWidthTBM = 3; break;
                    case 35: columnWidthMark = 2.14; columnWidthTBM = 3.43; break;
                    case 36: columnWidthMark = 2.14; columnWidthTBM = 3; break;
                    case 37: columnWidthMark = 2; columnWidthTBM = 3.43; break;
                    case 38: columnWidthMark = 2; columnWidthTBM = 3; break;
                    case 39: columnWidthMark = 1.86; columnWidthTBM = 3.43; break;
                    case 40: columnWidthMark = 1.86; columnWidthTBM = 3.14; break;
                    case 41: columnWidthMark = 1.71; columnWidthTBM = 3.57; break;
                    case 42: columnWidthMark = 1.71; columnWidthTBM = 3.43; break;
                    case 43: columnWidthMark = 1.71; columnWidthTBM = 3; break;
                    case 44: columnWidthMark = 1.57; columnWidthTBM = 3.43; break;
                    case 45: columnWidthMark = 1.57; columnWidthTBM = 3.14; break;
                    case 46: columnWidthMark = 1.57; columnWidthTBM = 2.86; break;
                    case 47: columnWidthMark = 1.43; columnWidthTBM = 3.43; break;
                    case 48: columnWidthMark = 1.43; columnWidthTBM = 3.14; break;
                    case 49: columnWidthMark = 1.43; columnWidthTBM = 2.86; break;
                    case 50: columnWidthMark = 1.29; columnWidthTBM = 3.43; break;
                    case 51: columnWidthMark = 1.29; columnWidthTBM = 3.14; break;
                    case 52: columnWidthMark = 1.29; columnWidthTBM = 3; break;
                    case 53: columnWidthMark = 1.14; columnWidthTBM = 3.86; break;
                    default: columnWidthMark = 1.14; columnWidthTBM = 3.43; break; // case: 54
                }
            }  
        }

        private void FillMark(IVTWorksheet sheetDiemHK, MasterBook entity, List<int> lstSubjectExportID, List<ClassSubjectBO> lstTotalSubject, List<int> lstPupilID, List<PupilProfileBO> lsPP,
                List<MarkRecordBO> lstMarkRecord, List<JudgeRecordBO> lstJudgeRecord, List<SummedUpRecordBO> lstSummedUpRecord, List<TeachingAssignmentBO> lstTeachingBo,
                int rowStandar, string HeadTeacherName, bool isSoHCM, int semesterId, ref int positionSheet, ref int startNumber, ref List<VTDataPageNumber> lstDataPageNumber)
        {
            //Danh sach mon hoc cua lop
            List<int> lstSubjectID = lstTotalSubject.Select(p => p.SubjectID).Distinct().ToList();
            List<int> lsAppliedTypeMonTuChon = new List<int>() { SystemParamsInFile.APPLIED_TYPE_VOLUNTARY, SystemParamsInFile.APPLIED_TYPE_VOLUNTARY_PLUS_MARK, SystemParamsInFile.APPLIED_TYPE_VOLUNTARY_PRIORITY };

            int countSubject = lstTotalSubject.Count;
            int totalRow = 69;
            int startRow = 6;
            int startCol = 0;
            ClassSubjectBO objCS = null;
            PupilProfileBO objPP = null;
            decimal totalHeigth = ((decimal)60 * (decimal)17.5);
            double rowHeigth = 0;
            //Tao vung template
            IVTRange RangeDiemHK = entity.isBorderMark ? sheetDiemHK.GetRange("A2", "CZ69") : sheetDiemHK.GetRange("A2", "CZ69");
            //IVTRange RangeDiemHKINoBorder = sheetDiemHK1NoBorder.GetRange("A2", "CZ69");
            //IVTRange RangeDiemHKII = sheetDiemHK2.GetRange("A2", "CZ69");
            //IVTRange RangeDiemHKIINoBorder = sheetDiemHK2NoBorder.GetRange("A2", "CZ69");
            int counttmp = 0;
            if (countSubject % 5 == 0)
            {
                counttmp = countSubject / 5;
            }
            else
            {
                counttmp = (countSubject / 5) + 1;
            }

            int startcopy = 0;
            int startcopytmp = 0;
            List<ClassSubjectBO> lstCStmp = new List<ClassSubjectBO>();

            //diem mon tinh diem
            List<MarkRecordBO> lsMR = lstMarkRecord.ToList();

            //diem mon nhan xet
            List<JudgeRecordBO> lsJR = lstJudgeRecord.ToList();


            //diem tbm
            List<SummedUpRecordBO> lsSUR = lstSummedUpRecord.ToList();

            Dictionary<string, object> dicSheetDiem = new Dictionary<string, object>();
            List<MarkRecordBO> mrOfThisPupil = null;
            List<JudgeRecordBO> jrOfThisPupil = null;
            List<decimal?> lsdiemMieng = new List<decimal?>();
            List<decimal?> lsdiem15ph = new List<decimal?>();
            List<decimal?> lsdiem1t = new List<decimal?>();
            List<decimal?> lsdiemhk = new List<decimal?>();
            List<string> strlsdiemMieng = new List<string>();
            List<string> strlsdiem15ph = new List<string>();
            List<string> strlsdiem1t = new List<string>();
            List<string> strlsdiemhk = new List<string>();


            string diemMieng = string.Empty;
            string diem15 = string.Empty;
            string diem1Tiet = string.Empty;
            string diemHK = string.Empty;
            int colM = 0;
            int colP = 0;
            int colV = 0;
            totalHeigth = ((decimal)60 * (decimal)16.5);
            rowHeigth = (double)Math.Round((decimal)totalHeigth / (decimal)rowStandar, 2);
            startRow = 6;

            this.SetRowHeight(sheetDiemHK, startRow, startRow + rowStandar, rowHeigth);
            this.HidenRow(sheetDiemHK, startRow + rowStandar, 66);
            if (entity.isBorderMark)
            {
                this.HidenColAndSetColWidth(sheetDiemHK, entity.InterviewMark, entity.WritingMark, entity.TwiceCoeffiecientMark);
            }
            //tao vung fill diem
            for (int i = 0; i < counttmp; i++)
            {
                startcopy = i * totalRow + 1;
                if (i > 0)
                {
                    sheetDiemHK.CopyPasteSameSize(RangeDiemHK, "A" + startcopy);
                    sheetDiemHK.SetBreakPage(startcopy);
                }
            }
            for (int i = 0; i < counttmp; i++)
            {
                startcopy = i * totalRow + 1;
                lstCStmp = lstTotalSubject.Skip((i * 5)).Take(5).ToList();
                int coltmp = 0;
                dicSheetDiem = new Dictionary<string, object>();
                sheetDiemHK.SetCellValue("A" + (startcopy + (i == 0 ? 65 : 64)), "Trong trang này có .........điểm được sửa chữa, trong đó môn: ${SubjectName1}.......điểm, ${SubjectName2}......điểm.");
                sheetDiemHK.SetCellValue("AR" + (startcopy + (i == 0 ? 65 : 64)), "Trong trang này có .........điểm được sửa chữa, trong đó môn:  ${SubjectName3}.......điểm,  ${SubjectName4}......điểm,  ${SubjectName5}......điểm.");
                for (int j = 0; j < lstCStmp.Count; j++)
                {
                    string confirmHeadTeacherTitle = "Xác nhận điểm của GV ${SubjectName" + (j + 1) + "}";
                    string confirmHeadTeacherName = "${HeadTeacherName" + (j + 1) + "}";
                    objCS = lstCStmp[j];

                    if (j < 2)
                    {
                        startCol = 3 + 20 * j;
                        if (entity.isGVBM)
                        {
                            sheetDiemHK.SetCellValue(startcopy + (i == 0 ? 66 : 65), startCol, confirmHeadTeacherTitle);
                            sheetDiemHK.SetCellValue(startcopy + (i == 0 ? 68 : 67), startCol, confirmHeadTeacherName);

                            dicSheetDiem.Add("HeadTeacherName" + (j + 1), this.GetHeadTeacherName(lstTeachingBo, HeadTeacherName, semesterId, objCS.SubjectID, entity.isGVBM));
                        }
                        else
                        {
                            int col1 = 3 + 20 * (1);
                            if (j == 0)
                            {
                                sheetDiemHK.SetCellValue(startcopy + (i == 0 ? 66 : 65), col1, "Ký xác nhận của GVCN");
                                sheetDiemHK.SetCellValue(startcopy + (i == 0 ? 68 : 67), col1, HeadTeacherName);
                            }
                        }

                    }
                    else
                    {
                        startCol = 5 + 20 * j;
                        if (entity.isGVBM)
                        {
                            sheetDiemHK.SetCellValue(startcopy + (i == 0 ? 66 : 65), j == 2 ? startCol - 1 : startCol, confirmHeadTeacherTitle);
                            sheetDiemHK.SetCellValue(startcopy + (i == 0 ? 68 : 67), j == 2 ? startCol - 1 : startCol, confirmHeadTeacherName);
                            //dicSheetDiem.Add("SubjectName" + (j + 1), objCS.SubjectName);
                            dicSheetDiem.Add("HeadTeacherName" + (j + 1), this.GetHeadTeacherName(lstTeachingBo, HeadTeacherName, semesterId, objCS.SubjectID, entity.isGVBM));
                        }
                        else//giao vien CN
                        {
                            int col1 = 5 + 20 * (4);
                            if (j == 2)
                            {
                                sheetDiemHK.SetCellValue(startcopy + (i == 0 ? 66 : 65), col1, "Ký xác nhận của GVCN");
                                sheetDiemHK.SetCellValue(startcopy + (i == 0 ? 68 : 67), col1, HeadTeacherName);
                            }
                        }
                    }
                    dicSheetDiem.Add("SubjectName" + (j + 1), objCS.SubjectName);
                    startcopytmp = startcopy;

                    sheetDiemHK.SetCellValue((startcopytmp + (i == 0 ? 2 : 1)), startCol, (objCS.AppliedType.HasValue && lsAppliedTypeMonTuChon.Contains(objCS.AppliedType.Value)) ? objCS.DisplayName + "(TC)" : objCS.DisplayName);
                    for (int k = 0; k < lsPP.Count; k++)
                    {
                        objPP = lsPP[k];
                        coltmp = startCol;
                        sheetDiemHK.SetCellValue((startcopytmp + (i == 0 ? 5 : 4)), 1, (k + 1));
                        sheetDiemHK.SetCellValue((startcopytmp + (i == 0 ? 5 : 4)), 2, objPP.FullName);
                        colM = colP = colV = 0;
                        if (objCS.IsCommenting == 0)
                        {
                            mrOfThisPupil = lsMR.Where(o => o.PupilID == objPP.PupilProfileID && o.Semester == semesterId).ToList();
                            lsdiemMieng = mrOfThisPupil.Where(o => o.Title == GlobalConstants.MARK_TYPE_M)
                                                                .Where(o => o.SubjectID == objCS.SubjectID)
                                                                .OrderBy(o => o.OrderNumber)
                                                                .Select(o => o.Mark).ToList();
                            lsdiem15ph = mrOfThisPupil.Where(o => o.Title == GlobalConstants.MARK_TYPE_P)
                                                                    .Where(o => o.SubjectID == objCS.SubjectID)
                                                                    .OrderBy(o => o.OrderNumber)
                                                                    .Select(o => o.Mark).ToList();
                            lsdiem1t = mrOfThisPupil.Where(o => o.Title == GlobalConstants.MARK_TYPE_V)
                                                                    .Where(o => o.SubjectID == objCS.SubjectID)
                                                                    .OrderBy(o => o.OrderNumber)
                                                                    .Select(o => o.Mark).ToList();
                            lsdiemhk = mrOfThisPupil.Where(o => o.Title == GlobalConstants.MARK_TYPE_HK)
                                                                    .Where(o => o.SubjectID == objCS.SubjectID)
                                                                    .Select(o => o.Mark).ToList();
                            strlsdiemMieng = ReportUtils.ConvertListMarkForM(lsdiemMieng, false);
                            strlsdiem15ph = ReportUtils.ConvertListMarkForV(lsdiem15ph, false);
                            strlsdiem1t = ReportUtils.ConvertListMarkForV(lsdiem1t, false);
                            strlsdiemhk = ReportUtils.ConvertListMarkForV(lsdiemhk, false);
                        }
                        else
                        {
                            jrOfThisPupil = lsJR.Where(o => o.PupilID == objPP.PupilProfileID && o.Semester == semesterId).ToList();
                            strlsdiemMieng = jrOfThisPupil.Where(o => o.Title == GlobalConstants.MARK_TYPE_M)
                                                                .Where(o => o.SubjectID == objCS.SubjectID)
                                                                .OrderBy(o => o.OrderNumber)
                                                                .Select(o => o.Judgement).ToList();
                            strlsdiem15ph = jrOfThisPupil.Where(o => o.Title == GlobalConstants.MARK_TYPE_P)
                                                                    .Where(o => o.SubjectID == objCS.SubjectID)
                                                                    .OrderBy(o => o.OrderNumber)
                                                                    .Select(o => o.Judgement).ToList();
                            strlsdiem1t = jrOfThisPupil.Where(o => o.Title == GlobalConstants.MARK_TYPE_V)
                                                                    .Where(o => o.SubjectID == objCS.SubjectID)
                                                                    .OrderBy(o => o.OrderNumber)
                                                                    .Select(o => o.Judgement).ToList();
                            strlsdiemhk = jrOfThisPupil.Where(o => o.Title == GlobalConstants.MARK_TYPE_HK)
                                                                    .Where(o => o.SubjectID == objCS.SubjectID)
                                                                    .Select(o => o.Judgement).ToList();
                        }

                        diemHK = string.Join(" ", strlsdiemhk.ToArray());

                        SummedUpRecordBO objSummed = lsSUR.FirstOrDefault(o => o.PupilID == objPP.PupilProfileID && o.SubjectID == objCS.SubjectID && o.Semester == semesterId);
                        string diemTBM = "";
                        if (objCS.IsCommenting == GlobalConstants.ISCOMMENTING_TYPE_MARK)
                        {
                            diemTBM = (objSummed != null && objSummed.SummedUpMark.HasValue) ? ReportUtils.ConvertMarkForV(objSummed.SummedUpMark.Value) : string.Empty;
                        }
                        else
                        {
                            diemTBM = (objSummed != null) ? objSummed.JudgementResult : "";
                        }

                        int tmp = i == 0 ? 5 : 4;
                        if (entity.isBorderMark)//Ke o
                        {
                            colM = startCol;
                            for (int m = 0; m < strlsdiemMieng.Count; m++)
                            {
                                sheetDiemHK.SetCellValue(startcopytmp + tmp, colM, strlsdiemMieng[m]);
                                colM++;
                            }
                            colP = startCol + 5;
                            for (int m = 0; m < strlsdiem15ph.Count; m++)
                            {
                                sheetDiemHK.SetCellValue(startcopytmp + tmp, colP, strlsdiem15ph[m]);
                                colP++;
                            }
                            colV = startCol + 10;
                            for (int m = 0; m < strlsdiem1t.Count; m++)
                            {
                                sheetDiemHK.SetCellValue(startcopytmp + tmp, colV, strlsdiem1t[m]);
                                colV++;
                            }
                            sheetDiemHK.SetCellValue(startcopytmp + tmp, startCol + 18, diemHK);
                            sheetDiemHK.SetCellValue(startcopytmp + tmp, startCol + 19, diemTBM);
                        }
                        else //No boder
                        {
                            diemMieng = string.Join(" ", strlsdiemMieng.ToArray());
                            diem15 = string.Join(" ", strlsdiem15ph.ToArray());
                            diem1Tiet = string.Join(" ", strlsdiem1t.ToArray());
                            diemHK = string.Join(" ", strlsdiemhk.ToArray());

                            sheetDiemHK.GetRange(startcopytmp + tmp, coltmp, startcopytmp + tmp, coltmp + 4).MergeLeft();
                            sheetDiemHK.SetCellValue(startcopytmp + tmp, coltmp, diemMieng);
                            coltmp += 5;
                            sheetDiemHK.GetRange(startcopytmp + tmp, coltmp, startcopytmp + tmp, coltmp + 4).MergeLeft();
                            sheetDiemHK.SetCellValue(startcopytmp + tmp, coltmp, diem15);
                            coltmp += 5;
                            sheetDiemHK.GetRange(startcopytmp + tmp, coltmp, startcopytmp + tmp, coltmp + 7).MergeLeft();
                            sheetDiemHK.SetCellValue(startcopytmp + tmp, coltmp, diem1Tiet);
                            coltmp += 8;
                            sheetDiemHK.SetCellValue(startcopytmp + tmp, coltmp, diemHK);
                            coltmp += 1;
                            sheetDiemHK.SetCellValue(startcopytmp + tmp, coltmp, diemTBM);
                        }

                        if (objPP.ProfileStatus != GlobalConstants.PUPIL_STATUS_STUDYING && objPP.ProfileStatus != GlobalConstants.PUPIL_STATUS_GRADUATED)
                        {
                            sheetDiemHK.GetRange(startcopytmp + tmp, 1, startcopytmp + tmp, startCol + 19).SetFontStyle(false, null, false, null, true, false);
                        }
                        startcopytmp++;
                    }
                    //fill diem
                }
                sheetDiemHK.FillVariableValue(dicSheetDiem);
            }
            startNumber++;
            VTDataPageNumber objPageNumber = new VTDataPageNumber();
            objPageNumber.SheetIndex = positionSheet;
            objPageNumber.IsHeaderOrFooter = true;
            objPageNumber.AlignPageNumber = 2;
            objPageNumber.StartPageNumberOfSheet = startNumber;
            lstDataPageNumber.Add(objPageNumber);
            positionSheet += 1;
            startNumber += counttmp * 2;
        }

        #region So ghi diem giao vien
        public ProcessedReport GetReportMarkInpurBook(IDictionary<string, object> dic)
        {
            string reportCode = SystemParamsInFile.REPORT_MARKINPUTBOOK;
            string inputParameterHashKey = GetHashKey(dic);
            return ProcessedReportBusiness.GetProcessedReport(reportCode, inputParameterHashKey);
        }

        public Stream CreateReportMarkInpurBook(IDictionary<string, object> dic, int numOfRows, bool isNumRowPupilNum, bool idg, int fitIn)
        {
            int academicYearID = Utils.GetInt(dic["AcademicYearID"]);
            int schoolID = Utils.GetInt(dic["SchoolID"]);
            int appliedLevel = Utils.GetInt(dic["AppliedLevel"]);
            int semester = Utils.GetInt(dic["Semester"]);
            int schoolFacultyID = Utils.GetInt(dic["SchoolFacultyID"]);
            int teacherID = Utils.GetInt(dic["TeacherID"]);
            Dictionary<string, object> tmpDic;
            SchoolProfile sp = SchoolProfileBusiness.Find(schoolID);
            AcademicYear aca = AcademicYearBusiness.Find(academicYearID);
            string supervisingDeptName = string.Empty;
            supervisingDeptName = sp.SupervisingDept.HierachyLevel == GlobalConstants.EDUCATION_HIERACHY_LEVEL_PROVINCE_OFFICE ? sp.SupervisingDept.SupervisingDeptName
                : sp.SupervisingDept.SupervisingDept2.SupervisingDeptName;

            //Lay khai bao so con diem HK1
            tmpDic = new Dictionary<string, object>();
            tmpDic["AcademicYearID"] = academicYearID;
            tmpDic["SchoolID"] = schoolID;
            tmpDic["Semester"] = GlobalConstants.SEMESTER_OF_YEAR_FIRST;
            SemeterDeclaration SemeterDeclaration1 = SemeterDeclarationBusiness.Search(tmpDic).Where(u => u.AppliedDate <= DateTime.Now).OrderByDescending(u => u.AppliedDate).FirstOrDefault();
            if (SemeterDeclaration1 == null) throw new BusinessException("Validate_School_NotMark");

            int soCotDiemMieng1 = SemeterDeclaration1.InterviewMark;
            int soCotDiem15Phut1 = SemeterDeclaration1.WritingMark;
            int soCotDiemKiemTra1 = SemeterDeclaration1.TwiceCoeffiecientMark;

            //Lay khai bao so con diem HK2
            tmpDic = new Dictionary<string, object>();
            tmpDic["AcademicYearID"] = academicYearID;
            tmpDic["SchoolID"] = schoolID;
            tmpDic["Semester"] = GlobalConstants.SEMESTER_OF_YEAR_SECOND;
            SemeterDeclaration SemeterDeclaration2 = SemeterDeclarationBusiness.Search(tmpDic).Where(u => u.AppliedDate <= DateTime.Now).OrderByDescending(u => u.AppliedDate).FirstOrDefault();
            if (SemeterDeclaration1 == null) throw new BusinessException("Validate_School_NotMark");

            int soCotDiemMieng2 = SemeterDeclaration2.InterviewMark;
            int soCotDiem15Phut2 = SemeterDeclaration2.WritingMark;
            int soCotDiemKiemTra2 = SemeterDeclaration2.TwiceCoeffiecientMark;

            //Lay danh sach giao vien trong truong
            tmpDic = new Dictionary<string, object>();
            tmpDic["CurrentSchoolID"] = schoolID;
            tmpDic["FacultyID"] = schoolFacultyID;
            tmpDic["EmployeeID"] = teacherID;
            List<Employee> listEmp = EmployeeBusiness.Search(tmpDic).ToList();

            //Lay danh sach phan cong giang day
            tmpDic = new Dictionary<string, object>();
            tmpDic["AcademicYearID"] = academicYearID;
            tmpDic["AppliedLevel"] = appliedLevel;
            tmpDic["SchoolID"] = schoolID;
            tmpDic["FacultyID"] = schoolFacultyID;
            if (semester != GlobalConstants.SEMESTER_OF_YEAR_ALL)
            {
                tmpDic["Semester"] = semester;
            }
            tmpDic["TeacherID"] = teacherID;
            tmpDic["RemoveVNEN"] = true;

            List<TeachingAssignmentBO> lstTa = (from ta in TeachingAssignmentBusiness.SearchBySchool(schoolID, tmpDic).ToList()
                                                where ta.ClassProfile.IsActive == true
                                                group ta by new
                                                {
                                                    ta.ClassID,
                                                    ta.SubjectID,
                                                    ta.TeacherID,
                                                    ta.SubjectCat.OrderInSubject,
                                                    ta.ClassProfile.OrderNumber,
                                                    ta.ClassProfile.EducationLevelID,
                                                    ta.ClassProfile.DisplayName,
                                                    SubjectName = ta.SubjectCat.DisplayName
                                                } into g
                                                select new TeachingAssignmentBO
                                                {
                                                    ClassID = g.Key.ClassID,
                                                    SubjectID = g.Key.SubjectID,
                                                    TeacherID = g.Key.TeacherID,
                                                    OrderInSubject = g.Key.OrderInSubject,
                                                    ClassOrderNumber = g.Key.OrderNumber,
                                                    EducationLevelID = g.Key.EducationLevelID,
                                                    ClassName = g.Key.DisplayName,
                                                    SubjectName = g.Key.SubjectName,
                                                    TeachSemester1 = g.Any(o => o.Semester == GlobalConstants.SEMESTER_OF_YEAR_FIRST),
                                                    TeachSemester2 = g.Any(o => o.Semester == GlobalConstants.SEMESTER_OF_YEAR_SECOND)
                                                }).Distinct().ToList();

            List<int> lstTEacherId = lstTa.Select(o => o.TeacherID.GetValueOrDefault()).ToList();
            if (teacherID == 0)
            {
                listEmp = listEmp.Where(o => lstTEacherId.Contains(o.EmployeeID)).ToList();
            }

            //danh sach hoc sinh
            bool showPupil = aca.IsShowPupil.HasValue && aca.IsShowPupil.Value;
            IQueryable<PupilOfClass> lstPoc = PupilOfClassBusiness.SearchBySchool(schoolID, new Dictionary<string, object>()
            {
                {"AcademicYearID", academicYearID},
                {"CheckWithClass", "checkWithClass"}
            }).AddPupilStatus(showPupil);
            IQueryable<PupilProfileBO> lsPoC = (from p in lstPoc
                                                join q in PupilProfileBusiness.All on p.PupilID equals q.PupilProfileID
                                                where q.IsActive
                                                select new PupilProfileBO
                                                {
                                                    PupilProfileID = p.PupilID,
                                                    PupilCode = q.PupilCode,
                                                    FullName = q.FullName,
                                                    EthnicID = q.EthnicID,
                                                    CommuneName = q.Commune.CommuneName,
                                                    DistrictName = q.District.DistrictName,
                                                    VillageName = q.Village.VillageName,
                                                    EthnicName = q.Ethnic.EthnicName,
                                                    ProvinceName = q.Province.ProvinceName,
                                                    BirthDate = q.BirthDate,
                                                    BirthPlace = q.BirthPlace,
                                                    PolicyTargetName = q.PolicyTarget.Resolution,
                                                    PermanentResidentalAddress = q.PermanentResidentalAddress,
                                                    TempResidentalAddress = q.TempResidentalAddress,
                                                    FatherFullName = q.FatherFullName,
                                                    MotherFullName = q.MotherFullName,
                                                    SponsorFullName = q.SponsorFullName,
                                                    FatherJob = q.FatherJob,
                                                    MotherJob = q.MotherJob,
                                                    SponsorJob = q.SponsorJob,
                                                    Genre = q.Genre,
                                                    Name = q.Name,
                                                    OrderInClass = p.OrderInClass,
                                                    ProfileStatus = p.Status,
                                                    AssignedDate = p.AssignedDate,
                                                    EndDate = p.EndDate,
                                                    CurrentClassID = p.ClassID
                                                });


            //Lay danh sach diem
            tmpDic = new Dictionary<string, object>
            {
                {"AcademicYearID", academicYearID},
                {"SchoolID", schoolID},
                {"Year", aca.Year}
            };
            List<MarkRecordBO> lstMarkRecord = (from p in VMarkRecordBusiness.SearchBySchool(schoolID, tmpDic)
                                                join q in MarkTypeBusiness.All on p.MarkTypeID equals q.MarkTypeID
                                                select new MarkRecordBO
                                                {
                                                    PupilID = p.PupilID,
                                                    ClassID = p.ClassID,
                                                    SubjectID = p.SubjectID,
                                                    Title = q.Title,
                                                    Mark = p.Mark,
                                                    Coefficient = q.Coefficient,
                                                    Semester = p.Semester,
                                                    OrderNumber = p.OrderNumber
                                                }).ToList();

            tmpDic = new Dictionary<string, object>
            {
                {"AcademicYearID", academicYearID},
                {"SchoolID", schoolID},
                 {"Year", aca.Year}
            };
            List<JudgeRecordBO> lstJudgeRecord = (from p in VJudgeRecordBusiness.SearchBySchool(schoolID, tmpDic)
                                                  join q in MarkTypeBusiness.All on p.MarkTypeID equals q.MarkTypeID
                                                  select new JudgeRecordBO
                                                  {
                                                      PupilID = p.PupilID,
                                                      ClassID = p.ClassID,
                                                      SubjectID = p.SubjectID,
                                                      Title = q.Title,
                                                      Coefficient = q.Coefficient,
                                                      Judgement = p.Judgement,
                                                      Semester = p.Semester,
                                                      OrderNumber = p.OrderNumber
                                                  })
                .ToList();

            //Lấy danh sách thông tin điểm tổng kết
            tmpDic = new Dictionary<string, object>
            {
                {"AcademicYearID", academicYearID},
                {"SchoolID", schoolID}
            };
            List<SummedUpRecordBO> lstSummedUpRecord = VSummedUpRecordBusiness.SearchBySchool(schoolID, tmpDic)
                .Select(o => new SummedUpRecordBO
                {
                    PupilID = o.PupilID,
                    ClassID = o.ClassID,
                    PeriodID = o.PeriodID,
                    SubjectID = o.SubjectID,
                    JudgementResult = o.JudgementResult,
                    SummedUpMark = o.SummedUpMark,
                    Semester = o.Semester
                })
                .ToList();

            //Lấy danh sách các môn học cho các lớp
            tmpDic = new Dictionary<string, object>
            {
                {"AcademicYearID", academicYearID},
                {"AppliedLevel", appliedLevel}
            };
            IQueryable<ClassSubject> lstSubjectTemp = ClassSubjectBusiness.SearchBySchool(schoolID, tmpDic);
            List<ClassSubjectBO> lstSubjectAll = (from p in lstSubjectTemp
                                                  join s in SubjectCatBusiness.All on p.SubjectID equals s.SubjectCatID
                                                  select new ClassSubjectBO
                                                  {
                                                      SubjectID = p.SubjectID,
                                                      ClassID = p.ClassID,
                                                      SubjectName = s.SubjectName,
                                                      DisplayName = s.DisplayName,
                                                      OrderInSubject = s.OrderInSubject,
                                                      IsCommenting = p.IsCommenting,
                                                      SubjectIDCrease = p.SubjectIDIncrease != null ? p.SubjectIDIncrease.Value : 0
                                                  }).ToList();

            List<IVTWorkbook> lstStream = new List<IVTWorkbook>();

            //Lấy đường dẫn báo cáo
            string reportCode = SystemParamsInFile.REPORT_MARKINPUTBOOK;
            ReportDefinition rp = ReportDefinitionBusiness.GetByCode(reportCode);
            string templatePath = SystemParamsInFile.TEMPLATE_FOLDER + rp.TemplateDirectory + "/" + rp.TemplateName;

            int endColMSem1 = 7;
            int endColPSem1 = 12;
            int endColVSem1 = 20;
            int startMarkColSem1 = 3;
            int endMarkColSem1 = 20;

            int endColMSem2 = 31;
            int endColPSem2 = 36;
            int endColVSem2 = 44;
            int startMarkColSem2 = 27;
            int endMarkColSem2 = 44;

            List<string> lstFileName = new List<string>();
            ReportDefinition reportDef = ReportDefinitionBusiness.GetByCode(reportCode);

            string outputNamePattern = reportDef.OutputNamePattern;
            string strAppliedLevel = ReportUtils.ConvertAppliedLevelForReportName(appliedLevel);
            outputNamePattern = outputNamePattern.Replace("[AppliedLevel]", ReportUtils.StripVNSign(strAppliedLevel));

            if (semester == GlobalConstants.SEMESTER_OF_YEAR_FIRST)
            {
                outputNamePattern = outputNamePattern.Replace("[Semester]", "HK1");
            }
            else if (semester == GlobalConstants.SEMESTER_OF_YEAR_SECOND)
            {
                outputNamePattern = outputNamePattern.Replace("[Semester]", "HK2");
            }
            else
            {
                outputNamePattern = outputNamePattern.Replace("[Semester]", "CN");
            }

            //outputNamePattern = ReportUtils.RemoveSpecialCharacters(outputNamePattern) + ".xls";

            for (int i = 0; i < listEmp.Count; i++)
            {
                Employee emp = listEmp[i];
                string name = emp.FullName.Replace(" ", string.Empty);
                string code = emp.EmployeeCode;
                string fileName = outputNamePattern.Replace("[Teacher]", ReportUtils.StripVNSign(name + "_" + code));

                fileName = ReportUtils.RemoveSpecialCharacters(fileName) + ".xls";
                lstFileName.Add(fileName);

                //Khởi tạo
                IVTWorkbook oBook = VTExport.OpenWorkbook(templatePath);
                lstStream.Add(oBook);

                //Fill du lieu vao bao cao
                IVTWorksheet oSheet1 = oBook.GetSheet(1);
                IVTWorksheet oSheet2 = oBook.GetSheet(2);
                IVTWorksheet oSheet3 = oBook.GetSheet(3);
                IVTWorksheet oSheet4 = oBook.GetSheet(4);

                #region sheet 1
                oSheet1.SetCellValue("A3", supervisingDeptName.ToUpper());

                //Ho ten
                oSheet1.SetCellValue("D27", string.Format("Họ và tên giáo viên: {0}", emp.FullName));

                //Mon hoc
                List<string> lstSubject = lstTa.Where(o => o.TeacherID == emp.EmployeeID).Select(o => o.SubjectName).Distinct().ToList();
                oSheet1.SetCellValue("D29", string.Format("Môn học: {0}", string.Join(", ", lstSubject)));

                //Ten truong
                oSheet1.SetCellValue("D31", string.Format("Trường: {0}", sp.SchoolName.Replace("Trường", string.Empty).Replace("trường", string.Empty)));

                //Nam hoc
                oSheet1.SetCellValue("A45", string.Format("NĂM HỌC {0}", aca.DisplayTitle));

                #endregion

                #region sheet 2
                oSheet2.SetCellValue("A3", supervisingDeptName.ToUpper());

                //Ho ten
                oSheet2.SetCellValue("D27", string.Format("Họ và tên giáo viên: {0}", emp.FullName));

                //Mon hoc
                oSheet2.SetCellValue("D29", string.Format("Môn học: {0}", string.Join(", ", lstSubject)));

                //Lop day
                List<string> lstClassName = lstTa.Where(o => o.TeacherID == emp.EmployeeID).OrderBy(o => o.OrderInSubject)
                    .ThenBy(o => o.EducationLevelID).ThenBy(o => o.ClassOrderNumber).ThenBy(o => o.ClassName).Select(o => o.ClassName).Distinct().ToList();

                oSheet2.SetCellValue("I31", string.Join(", ", lstClassName));

                //Ten truong
                oSheet2.SetCellValue("D33", string.Format("Trường: {0}", sp.SchoolName.Replace("Trường", string.Empty).Replace("trường", string.Empty)));

                //Nam hoc
                oSheet2.SetCellValue("A44", string.Format("NĂM HỌC {0}", aca.DisplayTitle));
                #endregion

                #region sheet mon-lop
                List<TeachingAssignmentBO> lstTaOfTeacher = lstTa.Where(o => o.TeacherID == emp.EmployeeID).OrderBy(o => o.OrderInSubject)
                    .ThenBy(o => o.EducationLevelID).ThenBy(o => o.ClassOrderNumber).ThenBy(o => o.ClassName).ToList();

                for (int j = 0; j < lstTaOfTeacher.Count; j++)
                {
                    TeachingAssignmentBO ta = lstTaOfTeacher[j];
                    int classId = ta.ClassID.Value;
                    int subjectId = ta.SubjectID.GetValueOrDefault();
                    ClassSubjectBO cs = lstSubjectAll.FirstOrDefault(o => o.ClassID == classId && o.SubjectID == subjectId);
                    if (cs == null)
                    {
                        continue;
                    }
                    int finalNumRow;
                    int finalFitIn;
                    //danh sach hoc sinh cua lop
                    List<PupilProfileBO> lstPupilOfClass = lsPoC.Where(o => o.CurrentClassID == ta.ClassID).ToList();

                    if (isNumRowPupilNum)
                    {
                        if (lstPupilOfClass.Count() > 60)
                        {
                            lstPupilOfClass = lstPupilOfClass.ToList().OrderBy(o => o.OrderInClass).ThenBy(o => o.Name).ThenBy(p => p.FullName).Take(60).ToList();
                            finalNumRow = 60;
                        }
                        else
                        {
                            lstPupilOfClass = lstPupilOfClass.ToList().OrderBy(o => o.OrderInClass).ThenBy(o => o.Name).ThenBy(p => p.FullName).ToList();
                            if (lstPupilOfClass.Count() < 20)
                            {
                                finalNumRow = 20;
                            }
                            else
                            {
                                finalNumRow = lstPupilOfClass.Count;
                            }
                        }
                    }
                    else
                    {
                        lstPupilOfClass = lstPupilOfClass.ToList().OrderBy(o => o.OrderInClass).ThenBy(o => o.Name).ThenBy(p => p.FullName).Take(numOfRows).ToList();
                        finalNumRow = numOfRows;
                    }

                    if (fitIn == 0)
                    {
                        finalFitIn = finalNumRow > 35 ? 2 : 1;
                    }
                    else
                    {
                        finalFitIn = fitIn;
                    }

                    IVTWorksheet sheet;
                    if (cs.IsCommenting == SystemParamsInFile.ISCOMMENTING_TYPE_MARK ||
                               cs.IsCommenting == SystemParamsInFile.ISCOMMENTING_TYPE_MARK_JUDGE)
                    {
                        sheet = oBook.CopySheetToBeforeLast(oSheet3, "AV100");
                    }
                    else
                    {
                        sheet = oBook.CopySheetToBeforeLast(oSheet4, "AV100");
                    }

                    sheet.Worksheet.Cells[3, 3].Characters[1, "Điểm HS I".Length].Font.Bold = true;
                    sheet.Worksheet.Cells[3, 13].Characters[1, "Điểm HS II".Length].Font.Bold = true;
                    sheet.Worksheet.Cells[3, 21].Characters[1, "ĐKThk".Length + 1].Font.Bold = true;
                    sheet.Worksheet.Cells[3, 22].Characters[1, "TBm".Length].Font.Bold = true;
                    sheet.Worksheet.Cells[3, 27].Characters[1, "Điểm HS I".Length].Font.Bold = true;
                    sheet.Worksheet.Cells[3, 37].Characters[1, "Điểm HS II".Length].Font.Bold = true;
                    sheet.Worksheet.Cells[3, 45].Characters[1, "ĐKThk".Length + 1].Font.Bold = true;
                    sheet.Worksheet.Cells[3, 46].Characters[1, "TBm".Length].Font.Bold = true;
                    sheet.Worksheet.Cells[3, 47].Font.Bold = true;
                    sheet.Worksheet.Cells[3, 47].Characters[7, 10].Font.Bold = false;
                    sheet.Worksheet.Cells[65, 1].Characters[1, "Số học sinh đạt HKI".Length].Font.Bold = true;
                    sheet.Worksheet.Cells[65, 25].Characters[1, "Số học sinh đạt HKII".Length].Font.Bold = true;
                    sheet.Worksheet.Cells[65, 37].Characters[1, "Số học sinh đạt CN".Length].Font.Bold = true;
                    #region HK1
                    if (semester == GlobalConstants.SEMESTER_OF_YEAR_FIRST || semester == GlobalConstants.SEMESTER_OF_YEAR_ALL)
                    {
                        //Lấy danh sách thông tin điểm môn tính điếm trong lớp
                        var listMR = (from s in lstMarkRecord
                                      where s.ClassID == classId
                                      && s.SubjectID == subjectId
                                      && s.Semester == GlobalConstants.SEMESTER_OF_YEAR_FIRST
                                      select s).ToList();

                        //Lấy danh sách thông tin điểm môn nhận xét trong lớp
                        var listJR = (from s in lstJudgeRecord
                                      where s.ClassID == classId
                                      && s.SubjectID == subjectId
                                      && s.Semester == GlobalConstants.SEMESTER_OF_YEAR_FIRST
                                      select s).ToList();

                        //Lấy danh sách thông tin điểm tổng kết trong lớp
                        var listSR = (from s in lstSummedUpRecord
                                      where s.ClassID == classId
                                      && s.SubjectID == subjectId
                                      && s.Semester == GlobalConstants.SEMESTER_OF_YEAR_FIRST
                                      select s).ToList();

                        sheet.SetCellValue("A2", string.Format("LỚP: {0}", ta.ClassName.ToUpper()));
                        sheet.SetCellValue("C2", string.Format("MÔN: {0}", ta.SubjectName.ToUpper()));

                        //An cot diem dua theo cau hinh
                        int hideColNum;
                        if (soCotDiemMieng1 < 5)
                        {
                            hideColNum = 5 - soCotDiemMieng1;
                            for (int k = 0; k < hideColNum; k++)
                            {
                                sheet.HideColumn(endColMSem1 - k);
                            }
                        }

                        if (soCotDiem15Phut1 < 5)
                        {
                            hideColNum = 5 - soCotDiem15Phut1;
                            for (int k = 0; k < hideColNum; k++)
                            {
                                sheet.HideColumn(endColPSem1 - k);
                            }
                        }

                        if (soCotDiemKiemTra1 < 8)
                        {
                            hideColNum = 8 - soCotDiemKiemTra1;
                            for (int k = 0; k < hideColNum; k++)
                            {
                                sheet.HideColumn(endColVSem1 - k);
                            }
                        }

                        //tinh lai do rong
                        int totalCol = soCotDiem15Phut1 + soCotDiemKiemTra1 + soCotDiemMieng1;
                        int totalWidth = 36;
                        double colWidth = (double)totalWidth / (double)totalCol;
                        for (int k = startMarkColSem1; k <= endMarkColSem1; k++)
                        {
                            sheet.SetColumnWidth(k, colWidth);
                        }

                        //fill du lieu hoc sinh
                        int startRow = 5;
                        int curRow = startRow;
                        int startCol = 1;
                        int curCol = startCol;

                        List<SummedUpRecordBO> lstSRForStatistic = new List<SummedUpRecordBO>();
                        for (int k = 0; k < lstPupilOfClass.Count; k++)
                        {
                            PupilProfileBO pp = lstPupilOfClass[k];

                            //STT
                            sheet.SetCellValue(curRow, curCol, k + 1);
                            curCol++;

                            //Ho ten
                            sheet.SetCellValue(curRow, curCol, pp.FullName);
                            curCol++;

                            //Fill dữ liệu điểm hệ số 1 và hệ số 2
                            if (cs.IsCommenting == SystemParamsInFile.ISCOMMENTING_TYPE_MARK ||
                                cs.IsCommenting == SystemParamsInFile.ISCOMMENTING_TYPE_MARK_JUDGE)
                            {
                                //Fill dữ liệu điểm miệng
                                List<MarkRecordBO> lstM = (from s in listMR
                                                           where s.PupilID == pp.PupilProfileID
                                                           && s.Title == SystemParamsInFile.MARK_TYPE_M
                                                           select s).OrderBy(x => x.OrderNumber).ToList();

                                List<string> lM = new List<string>();
                                for (int l = 1; l <= soCotDiemMieng1; l++)
                                {
                                    MarkRecordBO mr = lstM.FirstOrDefault(o => o.OrderNumber == l);
                                    lM.Add(mr != null && mr.Mark.HasValue ? ReportUtils.ConvertMarkForM(mr.Mark.Value) : null);
                                }

                                VTVector startCellM = new VTVector(curRow, curCol);
                                sheet.FillDataHorizon(lM, startCellM, soCotDiemMieng1);
                                curCol = curCol + 5;

                                //Lấy dữ liệu điểm 15 phút
                                List<MarkRecordBO> lstP = (from s in listMR
                                                           where s.PupilID == pp.PupilProfileID
                                                           && s.Title == SystemParamsInFile.MARK_TYPE_P
                                                           select s).OrderBy(x => x.OrderNumber).ToList();

                                List<string> lP = new List<string>();
                                for (int l = 1; l <= soCotDiem15Phut1; l++)
                                {
                                    MarkRecordBO mr = lstP.FirstOrDefault(o => o.OrderNumber == l);
                                    lP.Add(mr != null && mr.Mark.HasValue ? ReportUtils.ConvertMarkForP(mr.Mark.Value) : null);
                                }

                                VTVector startCellP = new VTVector(curRow, curCol);
                                sheet.FillDataHorizon(lP, startCellP, soCotDiem15Phut1);
                                curCol = curCol + 5;

                                //Fill diem 1 tiet
                                List<MarkRecordBO> lstV = (from s in listMR
                                                           where s.PupilID == pp.PupilProfileID
                                                           && s.Title == SystemParamsInFile.MARK_TYPE_V
                                                           select s).OrderBy(x => x.OrderNumber).ToList();

                                List<string> lV = new List<string>();
                                for (int l = 1; l <= soCotDiemKiemTra1; l++)
                                {
                                    MarkRecordBO mr = lstV.FirstOrDefault(o => o.OrderNumber == l);
                                    lV.Add(mr != null && mr.Mark.HasValue ? ReportUtils.ConvertMarkForV(mr.Mark.Value) : null);
                                }

                                VTVector startCellV = new VTVector(curRow, curCol);
                                sheet.FillDataHorizon(lV, startCellV, soCotDiemKiemTra1);
                                curCol = curCol + 8;

                                //Lấy dữ liệu điểm kiểm tra học kỳ
                                List<MarkRecordBO> lstHK = (from s in listMR
                                                            where s.PupilID == pp.PupilProfileID
                                                            && s.Title == SystemParamsInFile.MARK_TYPE_HK
                                                            select s).OrderBy(x => x.OrderNumber).ToList();
                                if (lstHK != null && lstHK.Count > 0)
                                {
                                    sheet.SetCellValue(curRow, curCol, ReportUtils.ConvertMarkForV(lstHK[0].Mark.Value));
                                }
                                curCol++;

                                //Fill TBM
                                SummedUpRecordBO se = listSR.Where(o => o.PupilID == pp.PupilProfileID).FirstOrDefault();
                                if (se != null)
                                {
                                    sheet.SetCellValue(curRow, curCol, se.SummedUpMark != null ? ReportUtils.ConvertMarkForV(se.SummedUpMark.Value) : string.Empty);
                                    if (pp.ProfileStatus == GlobalConstants.PUPIL_STATUS_STUDYING
                                        || pp.ProfileStatus == GlobalConstants.PUPIL_STATUS_GRADUATED)
                                    {
                                        lstSRForStatistic.Add(se);
                                    }
                                }
                            }
                            else
                            {
                                //Fill dữ liệu điểm miệng
                                List<JudgeRecordBO> lstM = (from s in listJR
                                                            where s.PupilID == pp.PupilProfileID
                                                            && s.Title == SystemParamsInFile.MARK_TYPE_M
                                                            select s).OrderBy(x => x.OrderNumber).ToList();

                                List<string> lM = new List<string>();
                                for (int l = 1; l <= soCotDiemMieng1; l++)
                                {
                                    JudgeRecordBO mr = lstM.FirstOrDefault(o => o.OrderNumber == l);
                                    lM.Add(mr != null ? mr.Judgement : null);
                                }

                                VTVector startCellM = new VTVector(curRow, curCol);
                                sheet.FillDataHorizon(lM, startCellM, soCotDiemMieng1);
                                curCol = curCol + 5;

                                //Lấy dữ liệu điểm 15 phút
                                List<JudgeRecordBO> lstP = (from s in listJR
                                                            where s.PupilID == pp.PupilProfileID
                                                            && s.Title == SystemParamsInFile.MARK_TYPE_P
                                                            select s).OrderBy(x => x.OrderNumber).ToList();

                                List<string> lP = new List<string>();
                                for (int l = 1; l <= soCotDiem15Phut1; l++)
                                {
                                    JudgeRecordBO mr = lstP.FirstOrDefault(o => o.OrderNumber == l);
                                    lP.Add(mr != null ? mr.Judgement : null);
                                }

                                VTVector startCellP = new VTVector(curRow, curCol);
                                sheet.FillDataHorizon(lP, startCellP, soCotDiem15Phut1);
                                curCol = curCol + 5;

                                //Fill diem 1 tiet
                                List<JudgeRecordBO> lstV = (from s in listJR
                                                            where s.PupilID == pp.PupilProfileID
                                                            && s.Title == SystemParamsInFile.MARK_TYPE_V
                                                            select s).OrderBy(x => x.OrderNumber).ToList();

                                List<string> lV = new List<string>();
                                for (int l = 1; l <= soCotDiemKiemTra1; l++)
                                {
                                    JudgeRecordBO mr = lstV.FirstOrDefault(o => o.OrderNumber == l);
                                    lV.Add(mr != null ? mr.Judgement : null);
                                }

                                VTVector startCellV = new VTVector(curRow, curCol);
                                sheet.FillDataHorizon(lV, startCellV, soCotDiemKiemTra1);
                                curCol = curCol + 8;

                                //Lấy dữ liệu điểm kiểm tra học kỳ
                                List<JudgeRecordBO> lstHK = (from s in listJR
                                                             where s.PupilID == pp.PupilProfileID
                                                             && s.Title == SystemParamsInFile.MARK_TYPE_HK
                                                             select s).OrderBy(x => x.OrderNumber).ToList();
                                if (lstHK != null && lstHK.Count > 0)
                                {
                                    sheet.SetCellValue(curRow, curCol, lstHK[0].Judgement);
                                }
                                curCol++;

                                //Fill TBM
                                SummedUpRecordBO se = listSR.Where(o => o.PupilID == pp.PupilProfileID).FirstOrDefault();
                                if (se != null)
                                {
                                    sheet.SetCellValue(curRow, curCol, se.JudgementResult);
                                    if (pp.ProfileStatus == GlobalConstants.PUPIL_STATUS_STUDYING
                                        || pp.ProfileStatus == GlobalConstants.PUPIL_STATUS_GRADUATED)
                                    {
                                        lstSRForStatistic.Add(se);
                                    }
                                }
                            }

                            if (pp.ProfileStatus != GlobalConstants.PUPIL_STATUS_STUDYING && pp.ProfileStatus != GlobalConstants.PUPIL_STATUS_GRADUATED)
                            {
                                sheet.GetRange(curRow, startCol, curRow, curCol).SetFontStyle(false, System.Drawing.Color.Red, false, null, true, false);
                            }

                            curCol = startCol;
                            curRow++;
                        }

                        //Fill phan thong ke
                        if (cs.IsCommenting == SystemParamsInFile.ISCOMMENTING_TYPE_MARK ||
                                cs.IsCommenting == SystemParamsInFile.ISCOMMENTING_TYPE_MARK_JUDGE)
                        {
                            int pupilNum = lstPupilOfClass.Where(o => o.ProfileStatus == GlobalConstants.PUPIL_STATUS_GRADUATED
                                || o.ProfileStatus == GlobalConstants.PUPIL_STATUS_STUDYING).Count();
                            if (pupilNum > 0)
                            {
                                int numGioi = lstSRForStatistic.Where(o => o.SummedUpMark.HasValue && o.SummedUpMark.Value >= 8).Count();
                                int numKha = lstSRForStatistic.Where(o => o.SummedUpMark.HasValue && o.SummedUpMark.Value < 8 && o.SummedUpMark.Value >= (decimal)6.5).Count();
                                int numTB = lstSRForStatistic.Where(o => o.SummedUpMark.HasValue && o.SummedUpMark.Value < (decimal)6.5 && o.SummedUpMark.Value >= 5).Count();
                                int numYeu = lstSRForStatistic.Where(o => o.SummedUpMark.HasValue && o.SummedUpMark.Value < 5 && o.SummedUpMark.Value >= (decimal)3.5).Count();
                                int numKem = lstSRForStatistic.Where(o => o.SummedUpMark.HasValue && o.SummedUpMark.Value < (decimal)3.5 && o.SummedUpMark.Value >= 0).Count();


                                string strGioi = string.Format("- Giỏi: {0} - {1}%", numGioi, Math.Round(((decimal)numGioi / (decimal)pupilNum) * 100, 1).ToString("0.#").Replace(",", "."));
                                string strKha = string.Format("- Khá: {0} - {1}%", numKha, Math.Round(((decimal)numKha / (decimal)pupilNum) * 100, 1).ToString("0.#").Replace(",", "."));
                                string strTB = string.Format("- Trung bình: {0} - {1}%", numTB, Math.Round(((decimal)numTB / (decimal)pupilNum) * 100, 1).ToString("0.#").Replace(",", "."));
                                string strYeu = string.Format("- Yếu: {0} - {1}%", numYeu, Math.Round(((decimal)numYeu / (decimal)pupilNum) * 100, 1).ToString("0.#").Replace(",", "."));
                                string strKem = string.Format("- Kém: {0} - {1}%", numKem, Math.Round(((decimal)numKem / (decimal)pupilNum) * 100, 1).ToString("0.#").Replace(",", "."));

                                sheet.SetCellValue("C66", strGioi);
                                sheet.SetCellValue("C67", strKha);
                                sheet.SetCellValue("C68", strTB);
                                sheet.SetCellValue("M66", strYeu);
                                sheet.SetCellValue("M67", strKem);
                            }
                        }
                        else
                        {
                            int pupilNum = lstPupilOfClass.Where(o => o.ProfileStatus == GlobalConstants.PUPIL_STATUS_GRADUATED
                                || o.ProfileStatus == GlobalConstants.PUPIL_STATUS_STUDYING).Count();
                            if (pupilNum > 0)
                            {
                                int numD = lstSRForStatistic.Where(o => o.JudgementResult == GlobalConstants.PASS).Count();
                                int numCD = lstSRForStatistic.Where(o => o.JudgementResult == GlobalConstants.NOPASS).Count();


                                string strD = string.Format("- Đạt: {0} - {1}%", numD, Math.Round(((decimal)numD / (decimal)pupilNum) * 100, 1).ToString("0.#").Replace(",", "."));
                                string strCD = string.Format("- Chưa đạt: {0} - {1}%", numCD, Math.Round(((decimal)numCD / (decimal)pupilNum) * 100, 1).ToString("0.#").Replace(",", "."));

                                sheet.SetCellValue("C66", strD);
                                sheet.SetCellValue("C67", strCD);
                            }
                        }
                    }


                    #endregion

                    #region HK2
                    if (semester == GlobalConstants.SEMESTER_OF_YEAR_SECOND || semester == GlobalConstants.SEMESTER_OF_YEAR_ALL)
                    {
                        //Lấy danh sách thông tin điểm môn tính điếm trong lớp
                        var listMR = (from s in lstMarkRecord
                                      where s.ClassID == classId
                                      && s.SubjectID == subjectId
                                      && s.Semester == GlobalConstants.SEMESTER_OF_YEAR_SECOND
                                      select s).ToList();

                        //Lấy danh sách thông tin điểm môn nhận xét trong lớp
                        var listJR = (from s in lstJudgeRecord
                                      where s.ClassID == classId
                                      && s.SubjectID == subjectId
                                      && s.Semester == GlobalConstants.SEMESTER_OF_YEAR_SECOND
                                      select s).ToList();

                        //Lấy danh sách thông tin điểm tổng kết trong lớp
                        var listSR = (from s in lstSummedUpRecord
                                      where s.ClassID == classId
                                      && s.SubjectID == subjectId
                                      && s.Semester == GlobalConstants.SEMESTER_OF_YEAR_SECOND
                                      select s).ToList();

                        //Lấy danh sách thông tin điểm tổng kết ca nam trong lớp
                        var listSRAll = (from s in lstSummedUpRecord
                                         where s.ClassID == classId
                                         && s.SubjectID == subjectId
                                         && s.Semester == GlobalConstants.SEMESTER_OF_YEAR_ALL
                                         select s).ToList();

                        sheet.SetCellValue("Y2", string.Format("LỚP: {0}", ta.ClassName.ToUpper()));
                        sheet.SetCellValue("AA2", string.Format("MÔN: {0}", ta.SubjectName.ToUpper()));

                        //An cot diem dua theo cau hinh
                        int hideColNum;
                        if (soCotDiemMieng2 < 5)
                        {
                            hideColNum = 5 - soCotDiemMieng2;
                            for (int k = 0; k < hideColNum; k++)
                            {
                                sheet.HideColumn(endColMSem2 - k);
                            }
                        }

                        if (soCotDiem15Phut2 < 5)
                        {
                            hideColNum = 5 - soCotDiem15Phut2;
                            for (int k = 0; k < hideColNum; k++)
                            {
                                sheet.HideColumn(endColPSem2 - k);
                            }
                        }

                        if (soCotDiemKiemTra2 < 8)
                        {
                            hideColNum = 8 - soCotDiemKiemTra2;
                            for (int k = 0; k < hideColNum; k++)
                            {
                                sheet.HideColumn(endColVSem2 - k);
                            }
                        }

                        //tinh lai do rong
                        int totalCol = soCotDiem15Phut2 + soCotDiemKiemTra2 + soCotDiemMieng2;
                        int totalWidth = 36;
                        double colWidth = (double)totalWidth / (double)totalCol;
                        for (int k = startMarkColSem2; k <= endMarkColSem2; k++)
                        {
                            sheet.SetColumnWidth(k, colWidth);
                        }

                        //fill du lieu hoc sinh
                        int startRow = 5;
                        int curRow = startRow;
                        int startCol = 25;
                        int curCol = startCol;

                        List<SummedUpRecordBO> lstSRForStatistic = new List<SummedUpRecordBO>();
                        List<SummedUpRecordBO> lstSRForStatisticAll = new List<SummedUpRecordBO>();
                        for (int k = 0; k < lstPupilOfClass.Count; k++)
                        {
                            PupilProfileBO pp = lstPupilOfClass[k];

                            //STT
                            sheet.SetCellValue(curRow, curCol, k + 1);
                            curCol++;

                            //Ho ten
                            sheet.SetCellValue(curRow, curCol, pp.FullName);
                            curCol++;

                            //Fill dữ liệu điểm hệ số 1 và hệ số 2
                            if (cs.IsCommenting == SystemParamsInFile.ISCOMMENTING_TYPE_MARK ||
                                cs.IsCommenting == SystemParamsInFile.ISCOMMENTING_TYPE_MARK_JUDGE)
                            {
                                //Fill dữ liệu điểm miệng
                                List<MarkRecordBO> lstM = (from s in listMR
                                                           where s.PupilID == pp.PupilProfileID
                                                           && s.Title == SystemParamsInFile.MARK_TYPE_M
                                                           select s).OrderBy(x => x.OrderNumber).ToList();

                                List<string> lM = new List<string>();
                                for (int l = 1; l <= soCotDiemMieng2; l++)
                                {
                                    MarkRecordBO mr = lstM.FirstOrDefault(o => o.OrderNumber == l);
                                    lM.Add(mr != null && mr.Mark.HasValue ? ReportUtils.ConvertMarkForM(mr.Mark.Value) : null);
                                }

                                VTVector startCellM = new VTVector(curRow, curCol);
                                sheet.FillDataHorizon(lM, startCellM, soCotDiemMieng2);
                                curCol = curCol + 5;

                                //Lấy dữ liệu điểm 15 phút
                                List<MarkRecordBO> lstP = (from s in listMR
                                                           where s.PupilID == pp.PupilProfileID
                                                           && s.Title == SystemParamsInFile.MARK_TYPE_P
                                                           select s).OrderBy(x => x.OrderNumber).ToList();

                                List<string> lP = new List<string>();
                                for (int l = 1; l <= soCotDiem15Phut2; l++)
                                {
                                    MarkRecordBO mr = lstP.FirstOrDefault(o => o.OrderNumber == l);
                                    lP.Add(mr != null && mr.Mark.HasValue ? ReportUtils.ConvertMarkForP(mr.Mark.Value) : null);
                                }

                                VTVector startCellP = new VTVector(curRow, curCol);
                                sheet.FillDataHorizon(lP, startCellP, soCotDiem15Phut2);
                                curCol = curCol + 5;

                                //Fill diem 1 tiet
                                List<MarkRecordBO> lstV = (from s in listMR
                                                           where s.PupilID == pp.PupilProfileID
                                                           && s.Title == SystemParamsInFile.MARK_TYPE_V
                                                           select s).OrderBy(x => x.OrderNumber).ToList();

                                List<string> lV = new List<string>();
                                for (int l = 1; l <= soCotDiemKiemTra2; l++)
                                {
                                    MarkRecordBO mr = lstV.FirstOrDefault(o => o.OrderNumber == l);
                                    lV.Add(mr != null && mr.Mark.HasValue ? ReportUtils.ConvertMarkForV(mr.Mark.Value) : null);
                                }

                                VTVector startCellV = new VTVector(curRow, curCol);
                                sheet.FillDataHorizon(lV, startCellV, soCotDiemKiemTra2);
                                curCol = curCol + 8;

                                //Lấy dữ liệu điểm kiểm tra học kỳ
                                List<MarkRecordBO> lstHK = (from s in listMR
                                                            where s.PupilID == pp.PupilProfileID
                                                            && s.Title == SystemParamsInFile.MARK_TYPE_HK
                                                            select s).OrderBy(x => x.OrderNumber).ToList();
                                if (lstHK != null && lstHK.Count > 0)
                                {
                                    sheet.SetCellValue(curRow, curCol, ReportUtils.ConvertMarkForV(lstHK[0].Mark.Value));
                                }
                                curCol++;

                                //Fill TBM
                                SummedUpRecordBO se = listSR.Where(o => o.PupilID == pp.PupilProfileID).FirstOrDefault();
                                if (se != null)
                                {
                                    sheet.SetCellValue(curRow, curCol, se.SummedUpMark != null ? ReportUtils.ConvertMarkForV(se.SummedUpMark.Value) : string.Empty);
                                    if (pp.ProfileStatus == GlobalConstants.PUPIL_STATUS_STUDYING
                                        || pp.ProfileStatus == GlobalConstants.PUPIL_STATUS_GRADUATED)
                                    {
                                        lstSRForStatistic.Add(se);
                                    }
                                }
                                curCol++;

                                //Fill TBM ca nam
                                SummedUpRecordBO seAll = listSRAll.Where(o => o.PupilID == pp.PupilProfileID).FirstOrDefault();
                                if (seAll != null)
                                {
                                    sheet.SetCellValue(curRow, curCol, seAll.SummedUpMark != null ? ReportUtils.ConvertMarkForV(seAll.SummedUpMark.Value) : string.Empty);

                                    if (pp.ProfileStatus == GlobalConstants.PUPIL_STATUS_STUDYING
                                        || pp.ProfileStatus == GlobalConstants.PUPIL_STATUS_GRADUATED)
                                    {
                                        lstSRForStatisticAll.Add(seAll);
                                    }
                                }

                            }
                            else
                            {
                                //Fill dữ liệu điểm miệng
                                List<JudgeRecordBO> lstM = (from s in listJR
                                                            where s.PupilID == pp.PupilProfileID
                                                            && s.Title == SystemParamsInFile.MARK_TYPE_M
                                                            select s).OrderBy(x => x.OrderNumber).ToList();

                                List<string> lM = new List<string>();
                                for (int l = 1; l <= soCotDiemMieng2; l++)
                                {
                                    JudgeRecordBO mr = lstM.FirstOrDefault(o => o.OrderNumber == l);
                                    lM.Add(mr != null ? mr.Judgement : null);
                                }

                                VTVector startCellM = new VTVector(curRow, curCol);
                                sheet.FillDataHorizon(lM, startCellM, soCotDiemMieng2);
                                curCol = curCol + 5;

                                //Lấy dữ liệu điểm 15 phút
                                List<JudgeRecordBO> lstP = (from s in listJR
                                                            where s.PupilID == pp.PupilProfileID
                                                            && s.Title == SystemParamsInFile.MARK_TYPE_P
                                                            select s).OrderBy(x => x.OrderNumber).ToList();

                                List<string> lP = new List<string>();
                                for (int l = 1; l <= soCotDiem15Phut2; l++)
                                {
                                    JudgeRecordBO mr = lstP.FirstOrDefault(o => o.OrderNumber == l);
                                    lP.Add(mr != null ? mr.Judgement : null);
                                }

                                VTVector startCellP = new VTVector(curRow, curCol);
                                sheet.FillDataHorizon(lP, startCellP, soCotDiem15Phut2);
                                curCol = curCol + 5;

                                //Fill diem 1 tiet
                                List<JudgeRecordBO> lstV = (from s in listJR
                                                            where s.PupilID == pp.PupilProfileID
                                                            && s.Title == SystemParamsInFile.MARK_TYPE_V
                                                            select s).OrderBy(x => x.OrderNumber).ToList();

                                List<string> lV = new List<string>();
                                for (int l = 1; l <= soCotDiemKiemTra2; l++)
                                {
                                    JudgeRecordBO mr = lstV.FirstOrDefault(o => o.OrderNumber == l);
                                    lV.Add(mr != null ? mr.Judgement : null);
                                }

                                VTVector startCellV = new VTVector(curRow, curCol);
                                sheet.FillDataHorizon(lV, startCellV, soCotDiemKiemTra2);
                                curCol = curCol + 8;

                                //Lấy dữ liệu điểm kiểm tra học kỳ
                                List<JudgeRecordBO> lstHK = (from s in listJR
                                                             where s.PupilID == pp.PupilProfileID
                                                             && s.Title == SystemParamsInFile.MARK_TYPE_HK
                                                             select s).OrderBy(x => x.OrderNumber).ToList();
                                if (lstHK != null && lstHK.Count > 0)
                                {
                                    sheet.SetCellValue(curRow, curCol, lstHK[0].Judgement);
                                }
                                curCol++;

                                //Fill TBM
                                SummedUpRecordBO se = listSR.Where(o => o.PupilID == pp.PupilProfileID).FirstOrDefault();
                                if (se != null)
                                {
                                    sheet.SetCellValue(curRow, curCol, se.JudgementResult);
                                    if (pp.ProfileStatus == GlobalConstants.PUPIL_STATUS_STUDYING
                                        || pp.ProfileStatus == GlobalConstants.PUPIL_STATUS_GRADUATED)
                                    {
                                        lstSRForStatistic.Add(se);
                                    }
                                }
                                curCol++;

                                //Fill TBM ca nam
                                SummedUpRecordBO seAll = listSRAll.Where(o => o.PupilID == pp.PupilProfileID).FirstOrDefault();
                                if (seAll != null)
                                {
                                    sheet.SetCellValue(curRow, curCol, seAll.JudgementResult);
                                    if (pp.ProfileStatus == GlobalConstants.PUPIL_STATUS_STUDYING
                                        || pp.ProfileStatus == GlobalConstants.PUPIL_STATUS_GRADUATED)
                                    {
                                        lstSRForStatisticAll.Add(seAll);
                                    }
                                }
                            }

                            if (pp.ProfileStatus != GlobalConstants.PUPIL_STATUS_STUDYING && pp.ProfileStatus != GlobalConstants.PUPIL_STATUS_GRADUATED)
                            {
                                sheet.GetRange(curRow, startCol, curRow, curCol).SetFontStyle(false, System.Drawing.Color.Red, false, null, true, false);
                            }

                            curCol = startCol;
                            curRow++;
                        }

                        //Fill phan thong ke
                        if (cs.IsCommenting == SystemParamsInFile.ISCOMMENTING_TYPE_MARK ||
                                cs.IsCommenting == SystemParamsInFile.ISCOMMENTING_TYPE_MARK_JUDGE)
                        {
                            int pupilNum = lstPupilOfClass.Where(o => o.ProfileStatus == GlobalConstants.PUPIL_STATUS_GRADUATED
                                || o.ProfileStatus == GlobalConstants.PUPIL_STATUS_STUDYING).Count();

                            if (pupilNum > 0)
                            {
                                int numGioi = lstSRForStatistic.Where(o => o.SummedUpMark.HasValue && o.SummedUpMark.Value >= 8).Count();
                                int numKha = lstSRForStatistic.Where(o => o.SummedUpMark.HasValue && o.SummedUpMark.Value < 8 && o.SummedUpMark.Value >= (decimal)6.5).Count();
                                int numTB = lstSRForStatistic.Where(o => o.SummedUpMark.HasValue && o.SummedUpMark.Value < (decimal)6.5 && o.SummedUpMark.Value >= 5).Count();
                                int numYeu = lstSRForStatistic.Where(o => o.SummedUpMark.HasValue && o.SummedUpMark.Value < 5 && o.SummedUpMark.Value >= (decimal)3.5).Count();
                                int numKem = lstSRForStatistic.Where(o => o.SummedUpMark.HasValue && o.SummedUpMark.Value < (decimal)3.5 && o.SummedUpMark.Value >= 0).Count();

                                string strGioi = string.Format("- Giỏi: {0} - {1}%", numGioi, Math.Round(((decimal)numGioi / (decimal)pupilNum) * 100, 1).ToString("0.#").Replace(",", "."));
                                string strKha = string.Format("- Khá: {0} - {1}%", numKha, Math.Round(((decimal)numKha / (decimal)pupilNum) * 100, 1).ToString("0.#").Replace(",", "."));
                                string strTB = string.Format("- Trung bình: {0} - {1}%", numTB, Math.Round(((decimal)numTB / (decimal)pupilNum) * 100, 1).ToString("0.#").Replace(",", "."));
                                string strYeu = string.Format("- Yếu: {0} - {1}%", numYeu, Math.Round(((decimal)numYeu / (decimal)pupilNum) * 100, 1).ToString("0.#").Replace(",", "."));
                                string strKem = string.Format("- Kém: {0} - {1}%", numKem, Math.Round(((decimal)numKem / (decimal)pupilNum) * 100, 1).ToString("0.#").Replace(",", "."));

                                sheet.SetCellValue("Z66", strGioi);
                                sheet.SetCellValue("Z67", strKha);
                                sheet.SetCellValue("Z68", strTB);
                                sheet.SetCellValue("AC66", strYeu);
                                sheet.SetCellValue("AC67", strKem);

                                int numGioiAll = lstSRForStatisticAll.Where(o => o.SummedUpMark.HasValue && o.SummedUpMark.Value >= 8).Count();
                                int numKhaAll = lstSRForStatisticAll.Where(o => o.SummedUpMark.HasValue && o.SummedUpMark.Value < 8 && o.SummedUpMark.Value >= (decimal)6.5).Count();
                                int numTBAll = lstSRForStatisticAll.Where(o => o.SummedUpMark.HasValue && o.SummedUpMark.Value < (decimal)6.5 && o.SummedUpMark.Value >= 5).Count();
                                int numYeuAll = lstSRForStatisticAll.Where(o => o.SummedUpMark.HasValue && o.SummedUpMark.Value < 5 && o.SummedUpMark.Value >= (decimal)3.5).Count();
                                int numKemAll = lstSRForStatisticAll.Where(o => o.SummedUpMark.HasValue && o.SummedUpMark.Value < (decimal)3.5 && o.SummedUpMark.Value >= 0).Count();

                                string strGioiAll = string.Format("- Giỏi: {0} - {1}%", numGioiAll, Math.Round(((decimal)numGioiAll / (decimal)pupilNum) * 100, 1).ToString("0.#").Replace(",", "."));
                                string strKhaAll = string.Format("- Khá: {0} - {1}%", numKhaAll, Math.Round(((decimal)numKhaAll / (decimal)pupilNum) * 100, 1).ToString("0.#").Replace(",", "."));
                                string strTBAll = string.Format("- Trung bình: {0} - {1}%", numTBAll, Math.Round(((decimal)numTBAll / (decimal)pupilNum) * 100, 1).ToString("0.#").Replace(",", "."));
                                string strYeuAll = string.Format("- Yếu: {0} - {1}%", numYeuAll, Math.Round(((decimal)numYeuAll / (decimal)pupilNum) * 100, 1).ToString("0.#").Replace(",", "."));
                                string strKemAll = string.Format("- Kém: {0} - {1}%", numKemAll, Math.Round(((decimal)numKemAll / (decimal)pupilNum) * 100, 1).ToString("0.#").Replace(",", "."));

                                sheet.SetCellValue("AL66", strGioiAll);
                                sheet.SetCellValue("AL67", strKhaAll);
                                sheet.SetCellValue("AL68", strTBAll);
                                sheet.SetCellValue("AT66", strYeuAll);
                                sheet.SetCellValue("AT67", strKemAll);
                            }
                        }
                        else
                        {
                            int pupilNum = lstPupilOfClass.Where(o => o.ProfileStatus == GlobalConstants.PUPIL_STATUS_GRADUATED
                                || o.ProfileStatus == GlobalConstants.PUPIL_STATUS_STUDYING).Count();

                            if (pupilNum > 0)
                            {
                                int numD = lstSRForStatistic.Where(o => o.JudgementResult == GlobalConstants.PASS).Count();
                                int numCD = lstSRForStatistic.Where(o => o.JudgementResult == GlobalConstants.NOPASS).Count();

                                string strD = string.Format("- Đạt: {0} - {1}%", numD, Math.Round(((decimal)numD / (decimal)pupilNum) * 100, 1).ToString("0.#").Replace(",", "."));
                                string strCD = string.Format("- Chưa đạt: {0} - {1}%", numCD, Math.Round(((decimal)numCD / (decimal)pupilNum) * 100, 1).ToString("0.#").Replace(",", "."));

                                sheet.SetCellValue("Z66", strD);
                                sheet.SetCellValue("Z67", strCD);

                                int numDAll = lstSRForStatisticAll.Where(o => o.JudgementResult == GlobalConstants.PASS).Count();
                                int numCDAll = lstSRForStatisticAll.Where(o => o.JudgementResult == GlobalConstants.NOPASS).Count();

                                string strDAll = string.Format("- Đạt: {0} - {1}%", numDAll, Math.Round(((decimal)numDAll / (decimal)pupilNum) * 100, 1).ToString("0.#").Replace(",", "."));
                                string strCDAll = string.Format("- Chưa đạt: {0} - {1}%", numCDAll, Math.Round(((decimal)numCDAll / (decimal)pupilNum) * 100, 1).ToString("0.#").Replace(",", "."));

                                sheet.SetCellValue("AL66", strDAll);
                                sheet.SetCellValue("AL67", strCDAll);
                            }
                        }
                    }


                    #endregion

                    //Xu ly xoa dong dua theo so dong du lieu
                    int beginRow = 5;
                    int endRow = 64;
                    int removedRowNum = 60 - finalNumRow;
                    for (int k = 0; k < removedRowNum; k++)
                    {
                        sheet.DeleteRow(endRow - k);
                    }

                    //xu ly dat do cao cua dong
                    double standard1PageTotalHeight = ((double)21.75) * 27;
                    double standard2PageTotalHeight = ((double)21.75) * 60;
                    double rowHeight;

                    if (finalFitIn == 1)
                    {
                        rowHeight = standard1PageTotalHeight / (double)finalNumRow;
                        //sheet.FitSheetOnOnePageH = true;
                    }
                    else
                    {
                        rowHeight = standard2PageTotalHeight / (double)finalNumRow;
                        // sheet.FitSheetOnTwoPageH = true;
                    }

                    for (int k = beginRow; k < beginRow + finalNumRow; k++)
                    {
                        sheet.SetRowHeight(k, rowHeight);
                    }

                    //Xu ly neu chon 1 HK thi xoa ky con lai
                    if (semester == GlobalConstants.SEMESTER_OF_YEAR_FIRST || !ta.TeachSemester2)
                    {
                        sheet.SetBreakPageV(24);

                        for (int k = 48; k >= 24; k--)
                        {
                            sheet.DeleteColumn(k);
                        }
                    }

                    if (semester == GlobalConstants.SEMESTER_OF_YEAR_SECOND || !ta.TeachSemester1)
                    {
                        for (int k = 24; k >= 1; k--)
                        {
                            sheet.DeleteColumn(k);
                        }
                    }
                    else
                    {
                        sheet.SetBreakPageV(24);
                    }

                    sheet.Name = string.Format("{0} – {1}", ReportUtils.StripVNSign(ta.SubjectName), ReportUtils.StripVNSign(ta.ClassName));
                }
                #endregion

                oSheet3.Delete();
                oSheet4.Delete();
            }


            if (teacherID != 0)
            {
                return lstStream[0].ToStream();
            }
            else
            {
                using (ZipFile zip = new ZipFile())
                {
                    for (int i = 0; i < lstStream.Count; i++)
                    {
                        zip.AddEntry(lstFileName[i], lstStream[i].ToStream());
                    }

                    MemoryStream output = new MemoryStream();

                    zip.Save(output);
                    output.Seek(0, SeekOrigin.Begin);
                    return output;

                }
            }
        }
        /// <summary>
        /// Luu lai thong tin
        /// </summary>
        /// <param name="entity"></param>
        /// <param name="data"></param>
        /// <returns></returns>
        public ProcessedReport InsertReportMarkInpurBook(IDictionary<string, object> dic, Stream data)
        {
            int teacherID = Utils.GetInt(dic["TeacherID"]);
            int appliedLevel = Utils.GetInt(dic["AppliedLevel"]);
            int semester = Utils.GetInt(dic["Semester"]);

            string reportCode = SystemParamsInFile.REPORT_MARKINPUTBOOK;

            ProcessedReport pr = new ProcessedReport();

            ReportDefinition reportDef = ReportDefinitionBusiness.GetByCode(reportCode);
            pr.ReportCode = reportCode;
            pr.ProcessedDate = DateTime.Now;
            pr.InputParameterHashKey = GetHashKey(dic);
            pr.ReportData = ReportUtils.Compress(data);

            //Tạo tên file
            string outputNamePattern = reportDef.OutputNamePattern;
            string strAppliedLevel = ReportUtils.ConvertAppliedLevelForReportName(appliedLevel);
            outputNamePattern = outputNamePattern.Replace("[AppliedLevel]", ReportUtils.StripVNSign(strAppliedLevel));
            if (teacherID != 0)
            {
                Employee e = EmployeeBusiness.Find(teacherID);
                string name = e.FullName.Replace(" ", string.Empty);
                string code = e.EmployeeCode;
                outputNamePattern = outputNamePattern.Replace("[Teacher]", ReportUtils.StripVNSign(name + "_" + code));
            }
            else
            {
                outputNamePattern = outputNamePattern.Replace("[Teacher]", "TatCa");
            }

            if (semester == GlobalConstants.SEMESTER_OF_YEAR_FIRST)
            {
                outputNamePattern = outputNamePattern.Replace("[Semester]", "HK1");
            }
            else if (semester == GlobalConstants.SEMESTER_OF_YEAR_SECOND)
            {
                outputNamePattern = outputNamePattern.Replace("[Semester]", "HK2");
            }
            else
            {
                outputNamePattern = outputNamePattern.Replace("[Semester]", "CN");
            }

            string outputFormat = teacherID > 0 ? "xls" : "zip";
            pr.ReportName = ReportUtils.RemoveSpecialCharacters(outputNamePattern) + "." + outputFormat;

            ProcessedReportParameterRepository.Insert(dic, pr);
            ProcessedReportRepository.Insert(pr);
            ProcessedReportRepository.Save();
            ProcessedReportParameterRepository.Save();
            return pr;
        }
        #endregion

        /// <summary>
        /// Sap xep mon hoc theo thu tu Môn bắt buộc (Tính điểm > nhận xét) > ngoại ngữ 2 > tự chọn > nghề PT.
        /// </summary>
        /// <param name="entity"></param>
        /// <param name="lstSubjectExportID"></param>
        /// <returns></returns>
        private List<ClassSubjectBO> GetSubjectInClass(MasterBook entity, List<int> lstSubjectExportID)
        {
            Dictionary<string, object> searchClass = new Dictionary<string, object>();
            searchClass["ClassID"] = entity.ClassID;
            searchClass["AcademicYearID"] = entity.AcademicYearID;
            searchClass["SchoolID"] = entity.SchoolID;
            IQueryable<ClassSubjectBO> iquery = (from p in ClassSubjectBusiness.SearchBySchool(entity.SchoolID, searchClass)
                                                 join q in SubjectCatBusiness.All on p.SubjectID equals q.SubjectCatID
                                                 where q.AppliedLevel == entity.AppliedLevel
                                                 && q.IsActive
                                                 select new ClassSubjectBO
                                                 {
                                                     ClassID = p.ClassID,
                                                     DisplayName = q.DisplayName,
                                                     OrderInSubject = q.OrderInSubject,
                                                     SubjectID = q.SubjectCatID,
                                                     SubjectName = q.SubjectName,
                                                     IsCommenting = p.IsCommenting,
                                                     Abbreviation = q.Abbreviation,
                                                     SectionPerWeekFirstSemester = p.SectionPerWeekFirstSemester,
                                                     SectionPerWeekSecondSemester = p.SectionPerWeekSecondSemester,
                                                     AppliedType = p.AppliedType,
                                                     IsForeignLanguage = q.IsForeignLanguage
                                                 });

            if (entity.Semester == SystemParamsInFile.SEMESTER_OF_YEAR_FIRST)
            {
                iquery = iquery.Where(o => o.SectionPerWeekFirstSemester > 0);
            }
            else if (entity.Semester == SystemParamsInFile.SEMESTER_OF_YEAR_SECOND)
            {
                iquery = iquery.Where(o => o.SectionPerWeekSecondSemester > 0);
            }
            else
            {
                iquery = iquery.Where(o => o.SectionPerWeekFirstSemester > 0 || o.SectionPerWeekSecondSemester > 0);
            }

            if (!entity.chkAllSubject && lstSubjectExportID != null && lstSubjectExportID.Count > 0)
            {
                iquery = iquery.Where(p => lstSubjectExportID.Contains(p.SubjectID));
            }

            //Sap xep mon hoc theo thu tu: Môn bắt buộc (Tính điểm > nhận xét) > ngoại ngữ 2 > tự chọn > nghề PT.
            List<ClassSubjectBO> lsAllSubject = iquery.ToList();
            List<ClassSubjectBO> lstTotalSubject = new List<ClassSubjectBO>();
            //Danh sach mon hoc bat buoc
            List<ClassSubjectBO> lstMonBatBuoc = lsAllSubject.Where(s => s.AppliedType == SystemParamsInFile.APPLIED_TYPE_MANDATORY).OrderBy(s => s.OrderInSubject).ThenBy(s => s.IsCommenting).ToList();
            if (lstMonBatBuoc != null && lstMonBatBuoc.Count > 0)
            {
                lstTotalSubject.AddRange(lstMonBatBuoc);
            }


            List<int> lsAppliedTypeMonTuChon = new List<int>() { SystemParamsInFile.APPLIED_TYPE_VOLUNTARY, SystemParamsInFile.APPLIED_TYPE_VOLUNTARY_PLUS_MARK, SystemParamsInFile.APPLIED_TYPE_VOLUNTARY_PRIORITY };

            //Lay các môn ngoai ngu khac mon bat buoc va mon nghe
            List<ClassSubjectBO> lsMonNN2 = lsAllSubject.Where(o => o.IsForeignLanguage == true && o.AppliedType != SystemParamsInFile.APPLIED_TYPE_MANDATORY && o.AppliedType != SystemParamsInFile.APPLIED_TYPE_APPRENTICESHIP).ToList();

            //Lay danh sach mon hoc tu chon bo ra mon NN2
            List<ClassSubjectBO> lsMonTC = new List<ClassSubjectBO>();

            if (lsMonNN2 != null && lsMonNN2.Count > 0)
            {
                lstTotalSubject.AddRange(lsMonNN2);
                lsMonTC = lsAllSubject.Except(lsMonNN2).Where(p => lsAppliedTypeMonTuChon.Contains(p.AppliedType.Value)).OrderBy(s => s.OrderInSubject).ToList();
            }
            else
            {
                lsMonTC = lsAllSubject.Where(p => lsAppliedTypeMonTuChon.Contains(p.AppliedType.Value)).OrderBy(s => s.OrderInSubject).ToList();
            }

            if (lsMonTC != null && lsMonTC.Count > 0)
            {
                lstTotalSubject.AddRange(lsMonTC);
            }


            List<ClassSubjectBO> lsMonNghePT = lsAllSubject.Where(p => p.AppliedType.Value == SystemParamsInFile.APPLIED_TYPE_APPRENTICESHIP).ToList();
            if (lsMonNghePT != null && lsMonNghePT.Count > 0)
            {
                lstTotalSubject.AddRange(lsMonNghePT);
            }


            return lstTotalSubject;
        }
        private void SetRowHeight(IVTWorksheet sheet, int FromRow, int ToRow, double rowHeight)
        {
            for (int i = FromRow; i < ToRow; i++)
            {
                sheet.SetRowHeight(i, rowHeight);
            }
        }
        private void HidenRow(IVTWorksheet sheet, int FromRow, int ToRow)
        {
            for (int i = FromRow; i < ToRow; i++)
            {
                sheet.SetRowHeight(i, 0);
            }
        }
        private void HidenColAndSetColWidth(IVTWorksheet sheet, int CountMarkM, int CountMarkP, int CountMarkV)
        {
            decimal totalWidth = 0;
            double colWidth = 0;
            int startCol = 0;
            if (CountMarkM < 5 || CountMarkP < 5 || CountMarkV < 8)
            {
                for (int i = 0; i < 6; i++)
                {

                    if (i < 2)
                    {
                        startCol = 3 + 20 * i;
                        totalWidth = (decimal)35.21;
                        colWidth = (double)Math.Round((decimal)totalWidth / (decimal)(CountMarkM + CountMarkP + CountMarkV), 2);
                    }
                    else
                    {
                        startCol = 5 + 20 * i;
                        totalWidth = (decimal)26;
                        colWidth = (double)Math.Round((decimal)totalWidth / (decimal)(CountMarkM + CountMarkP + CountMarkV), 2);
                    }
                    //set do rong
                    for (int j = startCol; j < (startCol + 18); j++)
                    {
                        sheet.SetColumnWidth(j, colWidth);
                    }
                    //An cot
                    //Mieng
                    for (int j = (startCol + CountMarkM); j < (startCol + 5); j++)
                    {
                        sheet.HideColumn(j);
                    }
                    //15p
                    for (int j = (startCol + 5 + CountMarkP); j < (startCol + 10); j++)
                    {
                        sheet.HideColumn(j);
                    }
                    //1 tiet
                    for (int j = (startCol + 10 + CountMarkV); j < (startCol + 18); j++)
                    {
                        sheet.HideColumn(j);
                    }
                }
            }
        }
        private string GetHeadTeacherName(List<TeachingAssignmentBO> lstTeachingBo, string HeadTeacherName, int SemesterID, int SubjectID, bool isHeadTeacher)
        {
            string strResult = string.Empty;
            if (isHeadTeacher)
            {
                TeachingAssignmentBO objTA = lstTeachingBo.Where(p => p.SubjectID == SubjectID && p.Semester == SemesterID).FirstOrDefault();
                strResult = objTA != null ? objTA.TeacherName : "";
            }
            else
            {
                strResult = HeadTeacherName;
            }
            return strResult;
        }
        /// <summary>
        /// Chiendd: 30.11.2015, Bo sung sheet thong tin giao vien bo mon cho cap 2, 3
        /// </summary>
        /// <param name="sheetGiaovienBM"></param>
        /// <param name="entity"></param>
        private void FillSheetTeachingAssignment(IVTWorksheet sheetGiaovienBM, MasterBook entity, List<TeachingAssignmentBO> lstTeachingBo)
        {
            Dictionary<string, object> dicClassSubject = new Dictionary<string, object>();
            dicClassSubject.Add("SchoolID", entity.SchoolID);
            IQueryable<ClassSubject> iqClassSubject_bm = ClassSubjectBusiness.SearchByClass(entity.ClassID, dicClassSubject);

            List<ClassSubjectBO> lstClassSubject1 = (from p in iqClassSubject_bm
                                                     join q in SubjectCatBusiness.All on p.SubjectID equals q.SubjectCatID
                                                     where (p.SectionPerWeekFirstSemester > 0 || p.SectionPerWeekSecondSemester > 0)
                                                     orderby q.OrderInSubject ascending
                                                     select new ClassSubjectBO
                                                     {
                                                         ClassID = p.ClassID,
                                                         DisplayName = q.DisplayName,
                                                         OrderInSubject = q.OrderInSubject,
                                                         SubjectID = q.SubjectCatID,
                                                         SubjectName = q.SubjectName
                                                     }).ToList();


            if (lstClassSubject1 != null && lstClassSubject1.Count > 0)
            {
                ClassSubjectBO objClassSubect1;
                TeachingAssignmentBO objTeachingAssignmentHK1;
                TeachingAssignmentBO objTeachingAssignmentHK2;
                List<int> lstTeacherId = new List<int>();
                int startRow_GVBM = 6;
                int row1 = 0;
                int row2 = 0;
                int row3 = 0;
                int rowExten = 0;
                IVTRange rangeEnd = sheetGiaovienBM.GetRange("C6", "L8");

                string teacherName = "";
                #region Dien thong tin giao vien BM


                for (int i = 0; i < lstClassSubject1.Count; i++)
                {
                    objClassSubect1 = lstClassSubject1[i];
                    row1 = startRow_GVBM + row2 + 3 * i;
                    row3 = row1 + 2;
                    if (i > 0)
                    {
                        sheetGiaovienBM.CopyAndInsertARow(rangeEnd, row1, true);
                    }
                    lstTeacherId = lstTeachingBo.Where(t => t.SubjectID == objClassSubect1.SubjectID)
                                    .Select(t => t.TeacherID.Value)
                                    .Distinct()
                                    .ToList();

                    rowExten = 0;
                    //Dien thong tin giao vien day, ho ky day
                    if (lstTeacherId != null && lstTeacherId.Count > 0)
                    {

                        for (int j = 0; j < lstTeacherId.Count; j++)
                        {
                            if (j >= 3)
                            {
                                rowExten++;
                                sheetGiaovienBM.CopyAndInsertARow(row1, row3 + rowExten, true);
                            }
                            objTeachingAssignmentHK1 = lstTeachingBo.FirstOrDefault(t => t.TeacherID == lstTeacherId[j] && t.SubjectID == objClassSubect1.SubjectID && t.Semester == GlobalConstants.SEMESTER_OF_YEAR_FIRST);
                            objTeachingAssignmentHK2 = lstTeachingBo.FirstOrDefault(t => t.TeacherID == lstTeacherId[j] && t.SubjectID == objClassSubect1.SubjectID && t.Semester == GlobalConstants.SEMESTER_OF_YEAR_SECOND);
                            //Dien thong tin giao vien
                            if (objTeachingAssignmentHK1 != null)
                            {
                                teacherName = objTeachingAssignmentHK1.TeacherName;
                            }
                            else if (objTeachingAssignmentHK2 != null)
                            {
                                teacherName = objTeachingAssignmentHK2.TeacherName;
                            }
                            else
                            {
                                teacherName = "";
                            }
                            sheetGiaovienBM.SetCellValue(row1 + j, 3, teacherName);
                            //Dien hoc ky I
                            sheetGiaovienBM.SetCellValue(row1 + j, 10, objTeachingAssignmentHK1 != null && objTeachingAssignmentHK1.Semester > 0 ? "X" : "");
                            //Dien hoc ky II
                            sheetGiaovienBM.SetCellValue(row1 + j, 11, objTeachingAssignmentHK2 != null && objTeachingAssignmentHK2.Semester > 0 ? "X" : "");
                        }
                    }
                    row2 += rowExten;
                    row3 += rowExten;

                    sheetGiaovienBM.GetRange(row1, 1, row3, 12).SetBorder(VTBorderStyle.Solid, VTBorderIndex.EdgeBottom);
                    sheetGiaovienBM.GetRange(row1, 1, row3, 1).Merge();
                    //Dien STT
                    sheetGiaovienBM.SetCellValue(row1, 1, i + 1);
                    sheetGiaovienBM.GetRange(row1, 1, row3, 1).SetHAlign(VTHAlign.xlHAlignCenter);
                    sheetGiaovienBM.GetRange(row1, 1, row3, 1).SetHAlignVer(NativeExcel.XlVAlign.xlVAlignCenter);
                    sheetGiaovienBM.GetRange(row1, 1, row3, 1).SetFontStyle(true, null, false, 12, false, false);
                    sheetGiaovienBM.GetRange(row1, 1, row3, 1).SetBorder(VTBorderStyle.Solid, VTBorderIndex.All);
                    //Dien ten mon hoc
                    sheetGiaovienBM.GetRange(row1, 2, row3, 2).SetBorder(VTBorderStyle.Solid, VTBorderIndex.All);
                    sheetGiaovienBM.GetRange(row1, 2, row3, 2).Merge();
                    sheetGiaovienBM.GetRange(row1, 2, row3, 2).WrapText();
                    sheetGiaovienBM.GetRange(row1, 2, row3, 2).SetHAlign(VTHAlign.xlHAlignCenter);
                    sheetGiaovienBM.GetRange(row1, 2, row3, 2).SetHAlignVer(NativeExcel.XlVAlign.xlVAlignCenter);
                    sheetGiaovienBM.GetRange(row1, 2, row3, 2).SetFontStyle(true, null, false, 12, false, false);
                    sheetGiaovienBM.SetCellValue(row1, 2, objClassSubect1.SubjectName.ToUpper());

                }
                #endregion

                if (row3 % 3 != 0 && row2 > 0)
                {
                    sheetGiaovienBM.DeleteRow(row3);
                }
            }
        }

        /// <summary>
        /// Lay danh sach phan cong giang day cua giao vien
        /// </summary>
        /// <param name="entity"></param>
        /// <returns></returns>
        private List<TeachingAssignmentBO> GetTeachingAssinment(MasterBook entity)
        {
            Dictionary<string, object> dicTeaching = new Dictionary<string, object>();
            dicTeaching.Add("ClassID", entity.ClassID);
            dicTeaching.Add("AcademicYearID", entity.AcademicYearID);
            IQueryable<TeachingAssignment> iqTeachingAssignmen = TeachingAssignmentBusiness.SearchBySchool(entity.SchoolID, dicTeaching);

            Dictionary<string, object> dicEmployee = new Dictionary<string, object>();
            dicEmployee.Add("SchoolID", entity.SchoolID);
            dicEmployee.Add("CurrentEmployeeStatus", GlobalConstants.EMPLOYMENT_STATUS_WORKING);
            IQueryable<Employee> IqEmployee = EmployeeBusiness.Search(dicEmployee);
            List<TeachingAssignmentBO> lstTeachingBo = (from tc in iqTeachingAssignmen
                                                        join e in IqEmployee on tc.TeacherID equals e.EmployeeID
                                                        select new TeachingAssignmentBO
                                                        {
                                                            EmployeeCode = e.EmployeeCode,
                                                            EmploymentStatus = e.EmploymentStatus,
                                                            Semester = tc.Semester,
                                                            TeacherID = e.EmployeeID,
                                                            TeacherName = e.FullName,
                                                            SubjectID = tc.SubjectID
                                                        }).ToList();
            return lstTeachingBo;
        }


        /// <summary>
        /// Fill giao vien bo mon cho cac sheet diem. Ap dung cho HCM
        /// </summary>
        /// <param name="sheet"></param>
        /// <param name="row"></param>
        /// <param name="column"></param>
        /// <param name="subjectId"></param>
        /// <param name="semmesterId"></param>
        /// <param name="suffixName"></param>
        /// <param name="lstTeachingBo"></param>
        /// <param name="i"></param>
        private void FillTeachingSubject(IVTWorksheet sheet, string cell, int subjectId, int semmesterId, string suffixName, List<TeachingAssignmentBO> lstTeachingBo, int i = 0)
        {
            //Chi dien khi fill hoc sinh dau tien
            //if (i > 0)
            //{
            //    return;
            //}
            ///Neu khong phai HCM thì khong dien
            if (string.IsNullOrEmpty(suffixName))
            {
                return;
            }
            TeachingAssignmentBO objTeaching = lstTeachingBo.FirstOrDefault(t => t.Semester == semmesterId && t.SubjectID == subjectId);
            sheet.SetCellValue(cell, objTeaching != null ? objTeaching.TeacherName : "");

        }

        /// <summary>
        /// Dien thong tin giao vien chu nhiem
        /// </summary>
        /// <param name="sheet"></param>
        /// <param name="cell"></param>
        /// <param name="headTeacher"></param>
        private void FillHeadTeacher(IVTWorksheet sheet, string cell, string headTeacher)
        {
            sheet.SetCellValue(cell, headTeacher);
        }
        private void FillSubjectReTest(IVTWorksheet sheetMarkReTest, List<ClassSubjectBO> lstSubject, List<PupilProfileBO> lstPupilReset, List<SummedUpRecordBO> lsSumupReTest, bool isSoHCM)
        {
            if (lstPupilReset == null || lstPupilReset.Count == 0)
            {
                return;
            }
            ClassSubjectBO objSubject;
            int startColumnSubject = 3;
            PupilProfileBO objPupilFile;
            SummedUpRecordBO objMark;
            int startRow = 6;
            for (int i = 0; i < lstSubject.Count; i++)
            {
                //Dien ten mon hoc thi lai
                objSubject = lstSubject[i];
                startRow = 6;
                sheetMarkReTest.SetCellValue(4, startColumnSubject, objSubject.DisplayName);
                for (int j = 0; j < lstPupilReset.Count; j++)
                {
                    objPupilFile = lstPupilReset[j];
                    if (i == 0)
                    {
                        sheetMarkReTest.SetCellValue(startRow, 2, objPupilFile.FullName);
                    }
                    //Dien diem mon hoc
                    objMark = lsSumupReTest.FirstOrDefault(m => m.PupilID == objPupilFile.PupilProfileID && m.SubjectID == objSubject.SubjectID);
                    //Neu la mon tinh diem
                    if (objSubject.IsCommenting == GlobalConstants.ISCOMMENTING_TYPE_MARK)
                    {
                        if (objMark != null && objMark.ReTestMark != null)
                        {
                            sheetMarkReTest.SetCellValue(startRow, startColumnSubject, ReportUtils.ConvertMarkForV_SGTGD(objMark.ReTestMark.Value, isSoHCM));
                        }
                    }
                    else
                    {
                        sheetMarkReTest.SetCellValue(startRow, startColumnSubject, objMark != null ? objMark.ReTestJudgement : "");
                    }
                    startRow++;
                }
                startColumnSubject++;
            }
        }

        public string SetDisplaySubjectNX(string displayName)
        {
            string output = "";
            switch (displayName.ToUpper())
            {
                case "TOÁN":
                    output = "C4";
                    break;
                case "VẬT LÍ":
                    output = "D4";
                    break;
                case "HÓA HỌC":
                    output = "E4";
                    break;
                case "SINH HỌC":
                    output = "F4";
                    break;
                case "TIN HỌC":
                    output = "G4";
                    break;
                case "NGỮ VĂN":
                    output = "H4";
                    break;
                case "LỊCH SỬ":
                    output = "I4";
                    break;
                case "ĐỊA LÍ":
                    output = "J4";
                    break;
                case "GDCD":
                    output = "L4";
                    break;
                case "THỂ DỤC":
                    output = "N4";
                    break;
                case "GDQP - AN":
                    output = "O4";
                    break;
                default:
                    break;
            }
            return output;
        }

        public string SetDisplaySubjectNX12(string displayName)
        {
            string output = "";
            switch (displayName.ToUpper())
            {
                case "TOÁN":
                    output = "C3";
                    break;
                case "VẬT LÍ":
                    output = "D3";
                    break;
                case "HÓA HỌC":
                    output = "E3";
                    break;
                case "SINH HỌC":
                    output = "F3";
                    break;
                case "TIN HỌC":
                    output = "G3";
                    break;
                case "NGỮ VĂN":
                    output = "H3";
                    break;
                case "LỊCH SỬ":
                    output = "I3";
                    break;
                case "ĐỊA LÍ":
                    output = "J3";
                    break;
                case "GDCD":
                    output = "L3";
                    break;
                case "THỂ DỤC":
                    output = "N3";
                    break;
                case "GDQP - AN":
                    output = "O3";
                    break;
                default:
                    break;
            }

            return output;
        }

        /// <summary>
        /// Hàm lấy ra danh sach cac buổi học của lớp: buoi sang tuong ung voi 1, chieu tuong ung voi 2, toi tuong ung voi 3
        /// Trong khai bao lớp học thi: 1 - buổi sáng, 2 - buổi chiều, 4 - buổi tối
        /// </summary>
        /// <param name="cp">Class profile</param>
        /// <returns>Danh sach cac buoi hoc cua hoc sinh tuong ung voi chuc nang diem danh</returns>
        public List<int> GetSectionsOfClass(ClassProfile cp)
        {
            List<int> sectionOfClass = new List<int>(5);
            if (cp.Section.HasValue)
            {
                if ((cp.Section.Value & 1) == 1)
                {
                    sectionOfClass.Add(1);
                }
                if ((cp.Section.Value & 2) == 2)
                {
                    sectionOfClass.Add(2);
                }
                if ((cp.Section.Value & 4) == 4)
                {
                    sectionOfClass.Add(3);
                }
            }
            return sectionOfClass;
        }

        #region get Date in week to string

        public string GetDateInWeekString(DateTime date)
        {
            string day = "";
            switch (date.DayOfWeek)
            {
                case DayOfWeek.Monday:
                    day = "T2";
                    break;

                case DayOfWeek.Tuesday:
                    day = "T3";
                    break;

                case DayOfWeek.Wednesday:
                    day = "T4";
                    break;

                case DayOfWeek.Thursday:
                    day = "T5";
                    break;

                case DayOfWeek.Friday:
                    day = "T6";
                    break;

                case DayOfWeek.Saturday:
                    day = "T7";
                    break;

                case DayOfWeek.Sunday:
                    day = "CN";
                    break;

                default:
                    break;
            }
            return day;
        }

        #endregion get Date in week to string
        public int ColumnNumber(string colAdress)
        {
            int[] digits = new int[colAdress.Length];
            for (int i = 0; i < colAdress.Length; ++i)
            {
                digits[i] = Convert.ToInt32(colAdress[i]) - 64;
            }
            int mul = 1; int res = 0;
            for (int pos = digits.Length - 1; pos >= 0; --pos)
            {
                res += digits[pos] * mul;
                mul *= 26;
            }
            return res;
        }
    }
}
