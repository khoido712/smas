﻿/**
 * The Viettel License
 *
 * Copyright 2012 Viettel Telecom. All rights reserved.
 * VIETTEL PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */

/** 
* @author AuNH
* @version $Revision: $
*/

using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Linq.Expressions;
using System.Web;
using log4net;

using SMAS.DAL.Repository;
using SMAS.DAL.IRepository;
using SMAS.Business.IBusiness;
using SMAS.Models.Models;
using SMAS.Business.Common;
using SMAS.Business.BusinessExtensions;

namespace SMAS.Business.Business
{
    public partial class HonourAchivementBusiness
    {
        #region aunh
        private const int HONOUR_ACHIVEMENT_TYPE_EMPLOYEE = 1;
        private const int HONOUR_ACHIVEMENT_TYPE_FACULTY = 3;
        private const int HONOUR_ACHIVEMENT_DESCRIPTION_MAXLENGTH = 500;
        //private void Validate(HonourAchivement HonourAchivement)
        //{
        //    ValidationMetadata.ValidateObject(HonourAchivement);

        //    // Kiểm tra cặp AcademicYearID, SchoolID có hợp lệ không
        //    IDictionary<string, object> SearchInfo = new Dictionary<string, object>();
        //    SearchInfo["AcademicYearID"] = HonourAchivement.AcademicYearID;
        //    SearchInfo["SchoolID"] = HonourAchivement.SchoolID;
        //    bool isAcademicYearSchoolCompatible = this.repository.ExistsRow(GlobalConstants.SCHOOL_SCHEMA, "AcademicYear", SearchInfo);
        //    if (isAcademicYearSchoolCompatible)
        //    {
        //        throw new BusinessException("Validate_NotCompatible");
        //    }
            
                
        //        //AcademicYearID: PK(AcademicYear)
        //    AcademicYearBusiness.CheckAvailable(HonourAchivement.AcademicYearID, "HonourAchivement_Label_AcademicYearID");

        //        //SchoolID: PK(SchoolProfile)
        //    SchoolProfileBusiness.CheckAvailable(HonourAchivement.SchoolID, "HonourAchivement_Label_SchoolID");

        //    //Description: maxlength(500)
        //    Utils.ValidateMaxLength(HonourAchivement.Description, HONOUR_ACHIVEMENT_DESCRIPTION_MAXLENGTH, "HonourAchivement_Label_Description");

        //    //HonourAchivementTypeID: PK(HonourAchivementType)

        //    HonourAchivementTypeBusiness.CheckAvailable(HonourAchivement.HonourAchivementTypeID, "HonourAchivementType_Label_HonourAchivementTypeID");
        
        //}

        //public HonourAchivement Insert(HonourAchivement HonourAchivement)
        //{
        //    Validate(HonourAchivement);
        //    base.Insert(HonourAchivement);
        //    return HonourAchivement;
        //}

        //public HonourAchivement Update(HonourAchivement HonourAchivement)
        //{
        //    Validate(HonourAchivement);
        //    base.Update(HonourAchivement);
        //    return HonourAchivement;
        //}

        //public void Delete(int Id)
        //{
        //    base.Delete(Id);
        //}

        //public IQueryable<HonourAchivementBO> Search(IDictionary<string, object> dic, int Type)
        //{
        //    int SchoolID = Utils.GetInt(dic, "SchoolID");
        //    int HonourAchivementTypeID = Utils.GetInt(dic, "HonourAchivementTypeID");
        //    int ObjectID = Utils.GetInt(dic, "ObjectID", -1);
        //    int AcademicYearID = Utils.GetInt(dic, "AcademicYearID");
        //    int SchoolFacultyID = Utils.GetInt(dic, "SchoolFacultyID");
        //    IQueryable<HonourAchivementBO> Query = null;
        //    if (Type == HONOUR_ACHIVEMENT_TYPE_FACULTY)
        //    {
        //        Query = from ha in HonourAchivementRepository.All
        //                join sf in SchoolFacultyRepository.All on ha.ObjectID equals sf.SchoolFacultyID
        //                //join ep in EmployeeRepository.All on ha.ObjectID equals ep.EmployeeID
        //                select new HonourAchivementBO
        //                {
        //                    HonourAchivementID = ha.HonourAchivementID,
        //                    AcademicYearID = ha.AcademicYearID,
        //                    AchivedDate = ha.AchivedDate,
        //                    SchoolID = ha.SchoolID,
        //                    HonourAchivementTypeID = ha.HonourAchivementTypeID,
        //                    Description = ha.Description,
        //                    FacultyName = sf.FacultyName,
        //                    ObjectID = ha.ObjectID,
        //                    Resolution = ha.HonourAchivementType.Resolution,
        //                    FacultyID = sf.SchoolFacultyID
        //                }
        //                        ;


        //    }
        //    if (Type == HONOUR_ACHIVEMENT_TYPE_TEACHER)
        //    {
        //        Query = from ha in HonourAchivementRepository.All
        //                //join sf in SchoolFacultyRepository.All on ha.ObjectID equals sf.SchoolFacultyID
        //                join ep in EmployeeRepository.All on ha.ObjectID equals ep.EmployeeID
        //                select new HonourAchivementBO
        //                {
        //                    HonourAchivementID = ha.HonourAchivementID,
        //                    AcademicYearID = ha.AcademicYearID,
        //                    AchivedDate = ha.AchivedDate,
        //                    SchoolID = ha.SchoolID,
        //                    HonourAchivementTypeID = ha.HonourAchivementTypeID,
        //                    Description = ha.Description,
        //                    FacultyName = ep.SchoolFaculty.FacultyName,
        //                    FacultyID = ep.SchoolFacultyID,
        //                    ObjectID = ha.ObjectID,
        //                    Resolution = ha.HonourAchivementType.Resolution,
        //                    EmployeeCode = ep.EmployeeCode,
        //                    FullName = ep.FullName
        //                }
        //                            ;

        //    }

        //    if (SchoolFacultyID != 0)
        //    {
        //        Query = Query.Where(o => o.FacultyID == SchoolFacultyID);
        //    }
        //    if (SchoolID > 0)
        //    {
        //        Query = Query.Where(o => o.SchoolID == SchoolID);
        //    }

        //    if (HonourAchivementTypeID > 0)
        //    {
        //        Query = Query.Where(o => o.HonourAchivementTypeID == HonourAchivementTypeID);
        //    }

        //    if (ObjectID > -1)
        //    {
        //        Query = Query.Where(o => o.ObjectID == ObjectID);
        //    }

        //    if (AcademicYearID > 0)
        //    {
        //        Query = Query.Where(o => o.AcademicYearID == AcademicYearID);
        //    }




        //    return Query;
        //}

        #endregion
       

        #region dungnt
        /// <summary>
        /// Hàm check validate dữ liệu
        /// </summary>
        /// <param name="HonourAchivement"></param>
        public void Validate(HonourAchivement HonourAchivement, int type)
        {
            ValidationMetadata.ValidateObject(HonourAchivement);

            // Kiểm tra cặp AcademicYearID, SchoolID có hợp lệ không
            IDictionary<string, object> SearchInfo = new Dictionary<string, object>();
            SearchInfo["AcademicYearID"] = HonourAchivement.AcademicYearID;
            SearchInfo["SchoolID"] = HonourAchivement.SchoolID;
            bool isAcademicYearSchoolCompatible = this.repository.ExistsRow(GlobalConstants.SCHOOL_SCHEMA, "AcademicYear", SearchInfo);
            if (!isAcademicYearSchoolCompatible)
            {
                throw new BusinessException("Validate_NotCompatible");
            }


            //AcademicYearID: PK(AcademicYear)
            AcademicYearBusiness.CheckAvailable(HonourAchivement.AcademicYearID, "HonourAchivement_Label_AcademicYearID",false);

            //SchoolID: PK(SchoolProfile)
            SchoolProfileBusiness.CheckAvailable(HonourAchivement.SchoolID, "HonourAchivement_Label_SchoolID", false);

            //Description: maxlength(500)
            Utils.ValidateMaxLength(HonourAchivement.Description, HONOUR_ACHIVEMENT_DESCRIPTION_MAXLENGTH, "HonourAchivement_Label_Description");

            //HonourAchivementTypeID: PK(HonourAchivementType)

            HonourAchivementTypeBusiness.CheckAvailable(HonourAchivement.HonourAchivementTypeID, "HonourAchivementType_Label_HonourAchivementTypeID", false);
            
            if (!AcademicYearBusiness.IsCurrentYear(HonourAchivement.AcademicYearID) && type == 2)
            {
                throw new BusinessException("Common_IsCurrentYear_Err");
            }

        }

        /// <summary>
        /// Tìm kiếm Danh hiệu thi đua cán bộ / tập thể
        /// </summary>
        /// <param name="dic"></param>
        /// <returns></returns>
        public IQueryable<HonourAchivement> Search(IDictionary<string, object> dic)
        {
            int SchoolID = Utils.GetInt(dic, "SchoolID");
            int AcademicYearID = Utils.GetInt(dic, "AcademicYearID");
            int HonourAchivementTypeID = Utils.GetInt(dic, "HonourAchivementTypeID");
            int EmployeeID = Utils.GetInt(dic, "EmployeeID");

            int FacultyID = Utils.GetInt(dic, "FacultyID");
            int Type = Utils.GetInt(dic, "Type");

            IQueryable<HonourAchivement> Query = HonourAchivementRepository.All;

            if (FacultyID != 0)
            {
               Query = Query.Where(o => o.FacultyID == FacultyID);
            }
            if (SchoolID > 0)
            {
                Query = Query.Where(o => o.SchoolID == SchoolID);
            }

            if (EmployeeID > 0)
            {
                Query = Query.Where(o => o.EmployeeID == EmployeeID);
            }

            if (Type > 0)
            {
                Query = Query.Where(o => o.Type == Type);
            }

            if (HonourAchivementTypeID > 0)
            {
                Query = Query.Where(o => o.HonourAchivementTypeID == HonourAchivementTypeID);
            }

          

            if (AcademicYearID > 0)
            {
                Query = Query.Where(o => o.AcademicYearID == AcademicYearID);
            }
            return Query;
        }

        /// <summary>
        /// Thêm mới Danh hiệu thi đua cán bộ
        /// </summary>
        /// <param name="HonourAchivement"></param>
        /// <returns></returns>
        public HonourAchivement InsertHonourAchivementForEmployee(HonourAchivement HonourAchivement)
        {
            //EmployeeID: PK(Employee)
            EmployeeBusiness.CheckAvailable(HonourAchivement.EmployeeID, "HonourAchivement_Label_EmployeeID");

            //EmployeeID, SchoolID: not compatiable(Employee)
            IDictionary<string, object> SearchInfo = new Dictionary<string, object>();
            SearchInfo["EmployeeID"] = HonourAchivement.EmployeeID;
            SearchInfo["SchoolID"] = HonourAchivement.SchoolID;
            bool EmployeeCompatible = EmployeeRepository.ExistsRow(GlobalConstants.SCHOOL_SCHEMA, "Employee", SearchInfo);
            if (!EmployeeCompatible)
            {
                throw new BusinessException("Validate_NotCompatible");
            }

            //HonourAchivementType(HonourAchivementTypeID).TYPE phải nhận giá trị HONOUR_ACHIVEMENT_TYPE_EMPLOYEE (1)
            HonourAchivementType tempHonourAchivementType = HonourAchivementTypeRepository.Find(HonourAchivement.HonourAchivementTypeID);
            if (tempHonourAchivementType.Type != HONOUR_ACHIVEMENT_TYPE_EMPLOYEE)
            {
                throw new BusinessException("HonourAchivementType_Label_HonourAchivementTypeID");
            }
            int type = 1;
            Validate(HonourAchivement, type);
            //AchivedDate = DateTime.Now
            HonourAchivement.Type = HONOUR_ACHIVEMENT_TYPE_EMPLOYEE;
            HonourAchivement.AchivedDate = DateTime.Now;
            return base.Insert(HonourAchivement);
        }

        /// <summary>
        /// Cập nhật Danh hiệu thi đua cán bộ 
        /// </summary>
        /// <param name="HonourAchivement"></param>
        /// <returns></returns>
        public HonourAchivement UpdateHonourAchivementForEmployee(HonourAchivement HonourAchivement)
        {
            //EmployeeID: PK(Employee)
            EmployeeBusiness.CheckAvailable(HonourAchivement.EmployeeID, "HonourAchivement_Label_EmployeeID");

            //EmployeeID, SchoolID: not compatiable(Employee)
            IDictionary<string, object> SearchInfo = new Dictionary<string, object>();
            SearchInfo["EmployeeID"] = HonourAchivement.EmployeeID;
            SearchInfo["SchoolID"] = HonourAchivement.SchoolID;
            bool EmployeeCompatible = EmployeeRepository.ExistsRow(GlobalConstants.SCHOOL_SCHEMA, "Employee", SearchInfo);
            if (!EmployeeCompatible)
            {
                throw new BusinessException("Validate_NotCompatible");
            }

            //HonourAchivementType(HonourAchivementTypeID).TYPE phải nhận giá trị HONOUR_ACHIVEMENT_TYPE_EMPLOYEE (1)
            HonourAchivementType tempHonourAchivementType = HonourAchivementTypeRepository.Find(HonourAchivement.HonourAchivementTypeID);
            if (tempHonourAchivementType.Type != HONOUR_ACHIVEMENT_TYPE_EMPLOYEE)
            {
                throw new BusinessException("HonourAchivementType_Label_HonourAchivementTypeID");
            }

            //HonourAchivementID: PK(HonourAchivement)
            this.CheckAvailable(HonourAchivement.HonourAchivementID, "HonourAchivement_Label_HonourAchivementID", false);

            //HonourAchivementID, SchoolID: not compatiable(HonourAchivement)
            {
                bool HonourAchivementCompatible = HonourAchivementRepository.ExistsRow(GlobalConstants.SCHOOL_SCHEMA, "HonourAchivement", new Dictionary<string, object>()
                {
                    {"HonourAchivementID",HonourAchivement.HonourAchivementID},
                    {"SchoolID",HonourAchivement.SchoolID}
                });
                if (!HonourAchivementCompatible)
                {
                    throw new BusinessException("Validate_NotCompatible");
                }
            }

            //HonourAchivementID, AcademicYearID: not compatiable(HonourAchivement)
            {
                bool HonourAchivementCompatible = HonourAchivementRepository.ExistsRow(GlobalConstants.SCHOOL_SCHEMA, "HonourAchivement", new Dictionary<string, object>()
                {
                    {"HonourAchivementID",HonourAchivement.HonourAchivementID},
                    {"AcademicYearID",HonourAchivement.AcademicYearID}
                });
                if (!HonourAchivementCompatible)
                {
                    throw new BusinessException("Validate_NotCompatible");
                }
            }

            //HonourAchivement (HonourAchivementID). TYPE (lấy trong CSDL) 
            //chỉ được nhận giá trị HONOUR_ACHIVEMENT_TYPE_EMPLOYEE

            if (HonourAchivement.Type != HONOUR_ACHIVEMENT_TYPE_EMPLOYEE)
            {
                throw new BusinessException("HonourAchivement_Label_Type");
            }
            int type = 1;
            Validate(HonourAchivement, type);
            HonourAchivement.Type = HONOUR_ACHIVEMENT_TYPE_EMPLOYEE;
            HonourAchivement.AchivedDate = DateTime.Now;
            return base.Update(HonourAchivement);
        }

        /// <summary>
        /// Xóa Danh hiệu thi đua cán bộ 
        /// </summary>
        /// <param name="SchoolID"></param>
        /// <param name="HonourAchivementForEmployeeID"></param>
        public void DeleteHonourAchivementForEmployee(int SchoolID, int HonourAchivementForEmployeeID)
        {
            HonourAchivement HonourAchivement = HonourAchivementRepository.Find(HonourAchivementForEmployeeID);

            //HonourAchivementID: PK(HonourAchivement)
            this.CheckAvailable(HonourAchivementForEmployeeID, "HonourAchivement_Label_HonourAchivementID", false);

            //HonourAchivementID, SchoolID: not compatiable(HonourAchivement)
            {
                bool HonourAchivementCompatible = HonourAchivementRepository.ExistsRow(GlobalConstants.SCHOOL_SCHEMA, "HonourAchivement", new Dictionary<string, object>()
                {
                    {"HonourAchivementID",HonourAchivement.HonourAchivementID},
                    {"SchoolID",HonourAchivement.SchoolID}
                });
                if (!HonourAchivementCompatible)
                {
                    throw new BusinessException("Validate_NotCompatible");
                }
            }

            //HonourAchivement (HonourAchivementID). TYPE (lấy trong CSDL) 
            //chỉ được nhận giá trị HONOUR_ACHIVEMENT_TYPE_EMPLOYEE 

            if (HonourAchivement.Type != HONOUR_ACHIVEMENT_TYPE_EMPLOYEE)
            {
                throw new BusinessException("HonourAchivement_Label_Type");
            }
            base.Delete(HonourAchivementForEmployeeID,false);

        }


        /// <summary>
        /// Tìm kiếm Danh hiệu thi đua cán bộ
        /// </summary>
        /// <param name="SchoolID"></param>
        /// <param name="dic"></param>
        /// <returns></returns>
        public IQueryable<HonourAchivement> SearchHonourAchivementForEmployee(int SchoolID, IDictionary<string, object> dic=null)
        {
            if (SchoolID == 0)
            {
                return null;
            }
            if (dic == null)
            {
                dic = new Dictionary<string, object>();
            }
            dic["SchoolID"] = SchoolID;
            dic["Type"] = HONOUR_ACHIVEMENT_TYPE_EMPLOYEE;
            return Search(dic);
        }

        /// <summary>
        /// Thêm mới Danh hiệu thi đua tổ bộ môn
        /// </summary>
        /// <param name="HonourAchivement"></param>
        /// <returns></returns>
        public HonourAchivement InsertHonourAchivementForFaculty(HonourAchivement HonourAchivement)
        {
            //FacultyID: PK(SchoolFacultyID)
            SchoolFacultyBusiness.CheckAvailable(HonourAchivement.FacultyID, "HonourAchivement_Label_FacultyID");

            //FacultyID, SchoolID: not compatiable(SchoolFacultyID)
            IDictionary<string, object> SearchInfo = new Dictionary<string, object>();
            SearchInfo["SchoolFacultyID"] = HonourAchivement.FacultyID;
            SearchInfo["SchoolID"] = HonourAchivement.SchoolID;
            bool SchoolFacultyCompatible = SchoolFacultyRepository.ExistsRow(GlobalConstants.SCHOOL_SCHEMA, "SchoolFaculty", SearchInfo);
            if (!SchoolFacultyCompatible)
            {
                throw new BusinessException("Validate_NotCompatible");
            }

            //HonourAchivementType(HonourAchivementTypeID).TYPE phải nhận giá trị HONOUR_ACHIVEMENT_TYPE_FACULTY (3)
            HonourAchivementType tempHonourAchivementType = HonourAchivementTypeRepository.Find(HonourAchivement.HonourAchivementTypeID);
            if (tempHonourAchivementType.Type != HONOUR_ACHIVEMENT_TYPE_FACULTY)
            {
                throw new BusinessException("HonourAchivementType_Label_HonourAchivementTypeID");
            }
            int type = 2;
            Validate(HonourAchivement, type);
            //AchivedDate = DateTime.Now
            HonourAchivement.Type = HONOUR_ACHIVEMENT_TYPE_FACULTY;
            HonourAchivement.AchivedDate = DateTime.Now;
            return base.Insert(HonourAchivement);
        }

        /// <summary>
        /// Cập nhật Danh hiệu thi đua tổ bộ môn 
        /// </summary>
        /// <param name="HonourAchivement"></param>
        /// <returns></returns>
        public HonourAchivement UpdateHonourAchivementForFaculty(HonourAchivement HonourAchivement)
        {
            //FacultyID: PK(SchoolFacultyID)
            SchoolFacultyBusiness.CheckAvailable(HonourAchivement.FacultyID, "HonourAchivement_Label_FacultyID");

            //FacultyID, SchoolID: not compatiable(SchoolFacultyID)
            IDictionary<string, object> SearchInfo = new Dictionary<string, object>();
            SearchInfo["SchoolFacultyID"] = HonourAchivement.FacultyID;
            SearchInfo["SchoolID"] = HonourAchivement.SchoolID;
            bool SchoolFacultyCompatible = SchoolFacultyRepository.ExistsRow(GlobalConstants.SCHOOL_SCHEMA, "SchoolFaculty", SearchInfo);
            if (!SchoolFacultyCompatible)
            {
                throw new BusinessException("Validate_NotCompatible");
            }

            //HonourAchivementType(HonourAchivementTypeID).TYPE phải nhận giá trị HONOUR_ACHIVEMENT_TYPE_FACULTY (3)
            HonourAchivementType tempHonourAchivementType = HonourAchivementTypeRepository.Find(HonourAchivement.HonourAchivementTypeID);
            if (tempHonourAchivementType.Type != HONOUR_ACHIVEMENT_TYPE_FACULTY)
            {
                throw new BusinessException("HonourAchivementType_Label_HonourAchivementTypeID");
            }

            //HonourAchivementID: PK(HonourAchivement)
            this.CheckAvailable(HonourAchivement.HonourAchivementID, "HonourAchivement_Label_HonourAchivementID", false);

            //HonourAchivementID, SchoolID: not compatiable(HonourAchivement)
            {
                bool HonourAchivementCompatible = HonourAchivementRepository.ExistsRow(GlobalConstants.SCHOOL_SCHEMA, "HonourAchivement", new Dictionary<string, object>()
                {
                    {"HonourAchivementID",HonourAchivement.HonourAchivementID},
                    {"SchoolID",HonourAchivement.SchoolID}
                });
                if (!HonourAchivementCompatible)
                {
                    throw new BusinessException("Validate_NotCompatible");
                }
            }

            //HonourAchivementID, AcademicYearID: not compatiable(HonourAchivement)
            {
                bool HonourAchivementCompatible = HonourAchivementRepository.ExistsRow(GlobalConstants.SCHOOL_SCHEMA, "HonourAchivement", new Dictionary<string, object>()
                {
                    {"HonourAchivementID",HonourAchivement.HonourAchivementID},
                    {"AcademicYearID",HonourAchivement.AcademicYearID}
                });
                if (!HonourAchivementCompatible)
                {
                    throw new BusinessException("Validate_NotCompatible");
                }
            }

            //HonourAchivement (HonourAchivementID). TYPE (lấy trong CSDL) 
            //chỉ được nhận giá trị HONOUR_ACHIVEMENT_TYPE_FACULTY 

            if (HonourAchivement.Type != HONOUR_ACHIVEMENT_TYPE_FACULTY)
            {
                throw new BusinessException("HonourAchivement_Label_Type");
            }
            int type = 2;
            Validate(HonourAchivement, type);
            HonourAchivement.Type = HONOUR_ACHIVEMENT_TYPE_FACULTY;
            HonourAchivement.AchivedDate = DateTime.Now;
            return base.Update(HonourAchivement);
        }

        /// <summary>
        /// Xóa Danh hiệu thi đua tổ bộ môn 
        /// </summary>
        /// <param name="SchoolID"></param>
        /// <param name="HonourAchivementForEmployeeID"></param>
        public void DeleteHonourAchivementForFaculty(int SchoolID, int HonourAchivementForEmployeeID)
        {
            HonourAchivement HonourAchivement = HonourAchivementRepository.Find(HonourAchivementForEmployeeID);

            //HonourAchivementID: PK(HonourAchivement)
            this.CheckAvailable(HonourAchivementForEmployeeID, "HonourAchivement_Label_HonourAchivementID", false);

            //HonourAchivementID, SchoolID: not compatiable(HonourAchivement)
            {
                bool HonourAchivementCompatible = HonourAchivementRepository.ExistsRow(GlobalConstants.SCHOOL_SCHEMA, "HonourAchivement", new Dictionary<string, object>()
                {
                    {"HonourAchivementID",HonourAchivement.HonourAchivementID},
                    {"SchoolID",HonourAchivement.SchoolID}
                });
                if (!HonourAchivementCompatible)
                {
                    throw new BusinessException("Validate_NotCompatible");
                }
            }


            //AcademicYearBusiness.IsCurrentYear(AcademicYearID) = FALSE => Năm học không phải năm hiện tại
            if (!AcademicYearBusiness.IsCurrentYear(HonourAchivement.AcademicYearID))
            {
                throw new BusinessException("HonourAchivement_Label_AcademicYearID");
            }

            //HonourAchivement (HonourAchivementID). TYPE (lấy trong CSDL) 
            //chỉ được nhận giá trị HONOUR_ACHIVEMENT_TYPE_FACULTY  

            if (HonourAchivement.Type != HONOUR_ACHIVEMENT_TYPE_FACULTY)
            {
                throw new BusinessException("HonourAchivement_Label_Type");
            }
            base.Delete(HonourAchivementForEmployeeID, false);
        }

        /// <summary>
        /// Tìm kiếm Danh hiệu thi đua tổ bộ môn
        /// </summary>
        /// <param name="SchoolID"></param>
        /// <param name="dic"></param>
        /// <returns></returns>
        public IQueryable<HonourAchivement> SearchHonourAchivementForFaculty(int SchoolID, IDictionary<string, object> dic=null)
        {
            if (SchoolID == 0)
            {
                return null;
            }
            if (dic == null)
            {
                dic = new Dictionary<string, object>();
            }
            dic["SchoolID"] = SchoolID;
            dic["Type"] = HONOUR_ACHIVEMENT_TYPE_FACULTY;
            return Search(dic);
        }

        #endregion
    }
}