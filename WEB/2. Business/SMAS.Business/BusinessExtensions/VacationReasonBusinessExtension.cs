﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Linq.Expressions;
using System.Web;
using log4net;

using SMAS.DAL.Repository;
using SMAS.DAL.IRepository;
using SMAS.Business.IBusiness;
using SMAS.Models.Models;
using System.Web.Mvc;
using SMAS.Business.Common;
using SMAS.Business.BusinessObject;
using System.Data.Objects;

namespace SMAS.Business.Business
{
    public partial class VacationReasonBusiness
    {
        public List<VacationReason> GetAll()
        {
            List<VacationReason> listReason = VacationReasonBusiness.All.Where(p=>p.IsActive).OrderBy(p=>p.VacationReasonID).ToList();
            return listReason;
        }
    }
}
