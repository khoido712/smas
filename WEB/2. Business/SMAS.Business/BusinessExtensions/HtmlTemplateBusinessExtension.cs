﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Linq.Expressions;
using System.Web;
using log4net;

using SMAS.DAL.Repository;
using SMAS.DAL.IRepository;
using SMAS.Business.IBusiness;
using SMAS.Models.Models;
using SMAS.Business.Common;
using SMAS.Business.BusinessObject;
using System.Transactions;

namespace SMAS.Business.Business
{
    public partial class HtmlTemplateBusiness
    {
        public HtmlTemplate GetHtmlTemplate(int htmlContentCodeId)
        {
            HtmlTemplate objResult = HtmlTemplateBusiness.All.AsNoTracking().Where(p => p.HtmlContentCodeID == htmlContentCodeId).FirstOrDefault();
            return objResult;
        }
    }
}
