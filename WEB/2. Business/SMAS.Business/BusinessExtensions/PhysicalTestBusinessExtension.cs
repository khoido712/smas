﻿/**
 * The Viettel License
 *
 * Copyright 2012 Viettel Telecom. All rights reserved.
 * VIETTEL PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */

/** 
* @author  
* @version $Revision: $
*/

using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Linq.Expressions;
using System.Web;
using log4net;
using SMAS.Business.Business;
using SMAS.DAL.Repository;
using SMAS.DAL.IRepository;
using SMAS.Business.IBusiness;
using SMAS.Models.Models;
using SMAS.Business.Common;
using SMAS.Business.BusinessObject;
using System.Text;
using SMAS.VTUtils.Log;

namespace SMAS.Business.Business
{
    public partial class PhysicalTestBusiness
    {
        #region Search
        /// <summary>
        /// Search
        /// </summary>
        /// <param name="SearchInfo">The search info.</param>
        /// <returns>
        /// IQueryable{PhysicalTest}
        /// </returns>
        /// <author>hath</author>
        /// <date>12/25/2012</date>
        ///       

        private IQueryable<PhysicalTest> Search(IDictionary<string, object> SearchInfo)
        {
            IQueryable<PhysicalTest> lstPhysicalTest = PhysicalTestRepository.All;
            int MonitoringBookID = Utils.GetInt(SearchInfo, "MonitoringBookID");
            decimal Height = Utils.GetDecimal(SearchInfo, "Height", -1);
            decimal Weight = Utils.GetDecimal(SearchInfo, "Weight", -1);
            decimal Breast = Utils.GetDecimal(SearchInfo, "Breast", -1);
            DateTime? Date = Utils.GetDateTime(SearchInfo, "Date");
            int PhysicalClassification = Utils.GetInt(SearchInfo, "PhysicalClassification");
            bool IsActive = Utils.GetBool(SearchInfo, "IsActive");
            int SchoolID = Utils.GetInt(SearchInfo, "SchoolID");
            int AcademicYearID = Utils.GetInt(SearchInfo, "AcademicYearID");
            int PupilID = Utils.GetInt(SearchInfo, "PupilID");
            int ClassID = Utils.GetInt(SearchInfo, "ClassID");
            int HealthPeriodID = Utils.GetInt(SearchInfo, "HealthPeriodID");
            string UnitName = Utils.GetString(SearchInfo, "UnitName");
            DateTime? MonitoringDate = Utils.GetDateTime(SearchInfo, "MonitoringDate");
            int EducationLevelID = Utils.GetInt(SearchInfo, "EducationLevelID");
            int Nutrition = Utils.GetInt(SearchInfo, "Nutrition");
            decimal HeightMin = Utils.GetDecimal(SearchInfo, "HeightMin");
            decimal HeightMax = Utils.GetDecimal(SearchInfo, "HeightMax");
            decimal WeightMin = Utils.GetDecimal(SearchInfo, "WeightMin");
            decimal WeightMax = Utils.GetDecimal(SearchInfo, "WeightMax");
            int EducationGrade = Utils.GetInt(SearchInfo, "EducationGrade");
            string FullName = Utils.GetString(SearchInfo, "FullName");
            if (EducationGrade != 0)
            {
                lstPhysicalTest = from ps in PhysicalTestRepository.All
                                  join mb in MonitoringBookBusiness.All on ps.MonitoringBookID equals mb.MonitoringBookID
                                  join ed in EducationLevelBusiness.All on mb.EducationLevelID equals ed.EducationLevelID
                                  where ed.Grade == EducationGrade
                                  select ps;
            }
            if (SchoolID != 0)
            {
                lstPhysicalTest = lstPhysicalTest.Where(o => o.MonitoringBook.SchoolID == SchoolID);
            }
            if (AcademicYearID != 0)
            {
                lstPhysicalTest = lstPhysicalTest.Where(o => o.MonitoringBook.AcademicYearID == AcademicYearID);
            }
            if (PupilID != 0)
            {
                lstPhysicalTest = lstPhysicalTest.Where(o => o.MonitoringBook.PupilID == PupilID);
            }
            if (ClassID != 0)
            {
                lstPhysicalTest = lstPhysicalTest.Where(o => o.MonitoringBook.ClassID == ClassID);
            }
            if (MonitoringBookID != 0)
            {
                lstPhysicalTest = lstPhysicalTest.Where(o => o.MonitoringBookID == MonitoringBookID);
            }
            if (Height != -1)
            {
                lstPhysicalTest = lstPhysicalTest.Where(o => o.Height == Height);
            }
            if (Weight != -1)
            {
                lstPhysicalTest = lstPhysicalTest.Where(o => o.Weight == Weight);
            }
            if (Breast != -1)
            {
                lstPhysicalTest = lstPhysicalTest.Where(o => o.Breast == Breast);
            }
            if (Date.HasValue)
            {
                lstPhysicalTest = lstPhysicalTest.Where(o => o.Date == Date);
            }
            if (PhysicalClassification != 0)
            {
                lstPhysicalTest = lstPhysicalTest.Where(o => o.PhysicalClassification == PhysicalClassification);
            }
            if (IsActive != true)
            {
                lstPhysicalTest = lstPhysicalTest.Where(o => o.Date == Date);
            }
            if (HealthPeriodID != 0)
            {
                lstPhysicalTest = lstPhysicalTest.Where(o => o.MonitoringBook.HealthPeriodID == HealthPeriodID);
            }
            if (EducationLevelID != 0)
            {
                lstPhysicalTest = lstPhysicalTest.Where(o => o.MonitoringBook.EducationLevelID == EducationLevelID);
            }
            if (Nutrition != 0)
            {
                lstPhysicalTest = lstPhysicalTest.Where(o => o.Nutrition == Nutrition);
            }
            if (HeightMin != 0)
            {
                lstPhysicalTest = lstPhysicalTest.Where(o => o.Height >= HeightMin);
            }
            if (HeightMax != 0)
            {
                lstPhysicalTest = lstPhysicalTest.Where(o => o.Height <= HeightMax);
            }
            if (WeightMin != 0)
            {
                lstPhysicalTest = lstPhysicalTest.Where(o => o.Weight >= WeightMin);
            }
            if (WeightMax != 0)
            {
                lstPhysicalTest = lstPhysicalTest.Where(o => o.Weight <= WeightMax);
            }
            if (!string.IsNullOrEmpty(FullName))
            {
                // lstPhysicalTest = lstPhysicalTest.ToList().Where(o => o.MonitoringBook.PupilProfile.FullName.ToLower().replaceUnicode().Contains(FullName.ToLower().replaceUnicode())).ToList();
                lstPhysicalTest = lstPhysicalTest.Where(o => o.MonitoringBook.PupilProfile.FullName.ToLower().Contains(FullName.ToLower()));
            }
            return lstPhysicalTest;
        }
        /// <summary>
        /// SearchBySchool
        /// </summary>
        /// <param name="SchoolID">The school ID.</param>
        /// <param name="SearchInfo">The search info.</param>
        /// <returns>
        /// IQueryable{PhysicalTest}
        /// </returns>
        /// <author>hath</author>
        /// <date>12/25/2012</date>
        public IQueryable<PhysicalTest> SearchBySchool(int SchoolID, IDictionary<string, object> SearchInfo)
        {
            if (SchoolID != 0)
            {
                SearchInfo["SchoolID"] = SchoolID;
            }
            return Search(SearchInfo);
        }

        /// <summary>
        /// Tìm kiếm thông tin khám sức khỏe theo học sinh – Thể lực
        /// author: trangdd
        /// </summary>
        /// <param name="AcademicYearID"></param>
        /// <param name="SchoolID"></param>
        /// <param name="PupilID"></param>
        /// <param name="ClassID"></param>
        /// <returns></returns>
        public IQueryable<PhysicalTestBO> SearchByPupil(int AcademicYearID, int SchoolID, int PupilID, int ClassID)
        {
            //B1: Khởi tạo IQueryable<MonitoringBook> lstMonitoringBook bằng cách gọi hàm MonitoringBookBusiness.SearchByPupil(SchoolID, AcademicYearID, PupilID, ClassID)
            IQueryable<MonitoringBook> lstMonitoringBook = MonitoringBookBusiness.SearchByPupil(SchoolID, AcademicYearID, PupilID, ClassID);
            //B2: Thực hiện việc tìm kiếm lstMonitoringBook mb left DentalTest dt on mb.MonitoringBookID = dt.MonitoringBookID. Kêt quả thu được IQueryable<DentalTestBO>
            var query = from mb in lstMonitoringBook
                        join dt in PhysicalTestRepository.All.Where(o => o.IsActive == true) on mb.MonitoringBookID equals dt.MonitoringBookID
                        //into g1
                        //from j2 in g1.DefaultIfEmpty()
                        select new PhysicalTestBO
                        {
                            PhysicalTestID = dt.PhysicalTestID,
                            MonitoringBookID = mb.MonitoringBookID,
                            Date = mb.MonitoringDate,
                            Height = dt.Height,
                            Weight = dt.Weight,
                            Breast = dt.Breast,
                            PhysicalClassification = dt.PhysicalClassification,
                            Nutrition = dt.Nutrition,
                            CreateDate = dt.CreatedDate,
                            PupilProFileID = mb.PupilID,
                            PupilProFile = mb.PupilProfile,
                            ClassProFileID = mb.ClassID,
                            ClassProFile = mb.ClassProfile
                        };
            return query;
        }
        #endregion

        #region GetListPhysicalTest
        /// <summary>
        /// GetListPhysicalTest
        /// </summary>
        /// <param name="SchoolID">The school ID.</param>
        /// <param name="AcademicYearID">The academic year ID.</param>
        /// <param name="EducationLevelID">The education level ID.</param>
        /// <param name="ClassID">The class ID.</param>
        /// <param name="FullName">The full name.</param>
        /// <param name="HealthPeriodID">The health period ID.</param>
        /// <param name="PhysicalClassification">The physical classification.</param>
        /// <param name="Nutrition">The nutrition.</param>
        /// <param name="HeightMin">The height min.</param>
        /// <param name="HeightMax">The height max.</param>
        /// <param name="WeightMin">The weight min.</param>
        /// <param name="WeightMax">The weight max.</param>
        /// <returns>
        /// List{PhysicalTest}
        /// </returns>
        /// <author>hath</author>
        /// <date>12/25/2012</date>
        public List<PhysicalTest> GetListPhysicalTest(int SchoolID, int AcademicYearID, int EducationLevelID, int ClassID, string FullName, int HealthPeriodID
            , int PhysicalClassification, int Nutrition, decimal HeightMin, decimal HeightMax, decimal WeightMin, decimal WeightMax, int EducationGrade, int PageSize, int PageNum, ref int toltalRecord, int SemesterId)
        {
            //Gọi hàm PhysicalTestBusiness.SearchBySchool(SchoolID, Dictionary) với:
            //+ Dictionary[“AcademicYearID”] = AcademicYearID
            //+ Dictionary[“EducationLevelID”] = EducationLevelID
            //+ Dictionary[“ClassID”] = ClassID
            //+ Dictionary[“FullName”] = FullName
            //+ Dictioanry[“PhysicalClassification”] = PhysicalClassification
            //+ Dictionary[“Nutrition”] = Nutrition
            //=> kết quả trả về lst
            //Select trong lst với điều kiện:
            //HeightMin <= lst[i].Height <= HeightMax
            //WeightMin <= lst[i].Weight <= WeightMax
            //=> kết quả trả về lstPhysicalTest

            IDictionary<string, object> Dictionary = new Dictionary<string, object>();
            Dictionary["AcademicYearID"] = AcademicYearID;
            Dictionary["EducationLevelID"] = EducationLevelID;
            Dictionary["ClassID"] = ClassID;
            Dictionary["FullName"] = FullName;
            Dictionary["PhysicalClassification"] = PhysicalClassification;
            Dictionary["Nutrition"] = Nutrition;
            Dictionary["HeightMin"] = HeightMin;
            Dictionary["HeightMax"] = HeightMax;
            Dictionary["WeightMin"] = WeightMin;
            Dictionary["WeightMax"] = WeightMax;
            Dictionary["HealthPeriodID"] = HealthPeriodID;
            Dictionary["EducationGrade"] = EducationGrade;

            //IQueryable<PhysicalTest> lstPhysicalTest = this.SearchBySchool(SchoolID, Dictionary);
            AcademicYear academicYearObj = AcademicYearBusiness.Find(AcademicYearID);
            IDictionary<string, object> dic = new Dictionary<string, object>();
            dic["SchoolID"] = SchoolID;
            dic["AppliedLevel"] = EducationGrade;
            dic["AcademicYearID"] = AcademicYearID;
            dic["ClassID"] = ClassID;
            IQueryable<PupilOfClass> ls = UtilsBusiness.AddCriteriaSemester(PupilOfClassBusiness.SearchBySchool(SchoolID, dic), academicYearObj, SemesterId);
            dic["EducationLevelID"] = EducationLevelID;
            dic["HealthPeriodID"] = HealthPeriodID;
            IQueryable<MonitoringBook> LstMB = MonitoringBookBusiness.Search(dic);
            IQueryable<PhysicalTest> lstPhysicalTest = (from a in this.SearchBySchool(SchoolID, Dictionary)
                                                        join c in MonitoringBookBusiness.All on a.MonitoringBookID equals c.MonitoringBookID
                                                        join poc in ls on new { c.PupilID, c.ClassID } equals new { poc.PupilID, poc.ClassID }
                                                        select a);

            toltalRecord = lstPhysicalTest.Count();
            List<PhysicalTest> lst = new List<PhysicalTest>();
            if (toltalRecord > 0)
            {
                lst = lstPhysicalTest.OrderBy(p => p.MonitoringBook.ClassProfile.EducationLevelID).ThenBy(p=>p.MonitoringBook.ClassProfile.DisplayName).ThenBy(p=>p.MonitoringBook.PupilProfile.Name).ThenBy(p=>p.MonitoringBook.PupilProfile.FullName).Skip((PageNum - 1) * PageSize).Take(PageSize).ToList();
            }
            return lst;
        }
        #endregion


        #region GetListPhysicalTest Export
        /// <summary>
        /// GetListPhysicalTest
        /// </summary>
        /// <param name="SchoolID">The school ID.</param>
        /// <param name="AcademicYearID">The academic year ID.</param>
        /// <param name="EducationLevelID">The education level ID.</param>
        /// <param name="ClassID">The class ID.</param>
        /// <param name="FullName">The full name.</param>
        /// <param name="HealthPeriodID">The health period ID.</param>
        /// <param name="PhysicalClassification">The physical classification.</param>
        /// <param name="Nutrition">The nutrition.</param>
        /// <param name="HeightMin">The height min.</param>
        /// <param name="HeightMax">The height max.</param>
        /// <param name="WeightMin">The weight min.</param>
        /// <param name="WeightMax">The weight max.</param>
        /// <returns>
        /// List{PhysicalTest}
        /// </returns>
        /// <author>hath</author>
        /// <date>12/25/2012</date>
        public List<PhysicalTest> GetListPhysicalTestExport(int SchoolID, int AcademicYearID, int EducationLevelID, int ClassID, string FullName, int HealthPeriodID
            , int PhysicalClassification, int Nutrition, decimal HeightMin, decimal HeightMax, decimal WeightMin, decimal WeightMax, int EducationGrade, int SemesterId)
        {
            IDictionary<string, object> Dictionary = new Dictionary<string, object>();
            Dictionary["AcademicYearID"] = AcademicYearID;
            Dictionary["EducationLevelID"] = EducationLevelID;
            Dictionary["ClassID"] = ClassID;
            Dictionary["FullName"] = FullName;
            Dictionary["PhysicalClassification"] = PhysicalClassification;
            Dictionary["Nutrition"] = Nutrition;
            Dictionary["HeightMin"] = HeightMin;
            Dictionary["HeightMax"] = HeightMax;
            Dictionary["WeightMin"] = WeightMin;
            Dictionary["WeightMax"] = WeightMax;
            Dictionary["HealthPeriodID"] = HealthPeriodID;
            Dictionary["EducationGrade"] = EducationGrade;

            //IQueryable<PhysicalTest> lstPhysicalTest = this.SearchBySchool(SchoolID, Dictionary);

            AcademicYear academicYearObj = AcademicYearBusiness.Find(AcademicYearID);
            IDictionary<string, object> dic = new Dictionary<string, object>();
            dic["SchoolID"] = SchoolID;
            dic["AppliedLevel"] = EducationGrade;
            dic["AcademicYearID"] = AcademicYearID;
            dic["ClassID"] = ClassID;
            IQueryable<PupilOfClass> ls = UtilsBusiness.AddCriteriaSemester(PupilOfClassBusiness.SearchBySchool(SchoolID, dic), academicYearObj, SemesterId);
            dic["EducationLevelID"] = EducationLevelID;
            dic["HealthPeriodID"] = HealthPeriodID;
            IQueryable<PhysicalTest> lstPhysicalTest = from a in this.SearchBySchool(SchoolID, Dictionary)
                                                       join c in MonitoringBookBusiness.Search(dic) on a.MonitoringBookID equals c.MonitoringBookID
                                                       join poc in ls on new { c.PupilID, c.ClassID } equals new { poc.PupilID, poc.ClassID }
                                                       select a;

            List<PhysicalTest> lst = new List<PhysicalTest>();
            if (lstPhysicalTest.Count() > 0)
            {
                lst = lstPhysicalTest.OrderBy(p => p.MonitoringBook.ClassProfile.EducationLevelID).ThenBy(p => p.MonitoringBook.ClassProfile.DisplayName).ThenBy(p => p.MonitoringBook.PupilProfile.Name).ThenBy(p => p.MonitoringBook.PupilProfile.FullName).ToList();
            }
            return lst;
        }
        #endregion

        /// <summary>
        /// Thêm mới thông tin khám sức khỏe – thể lực
        /// author: trangdd
        /// </summary>
        public void Insert(PhysicalTest PhysicalTest, MonitoringBook MonitoringBook)
        {
            ValidationMetadata.ValidateObject(PhysicalTest);
            IDictionary<string, object> SearchInfo = new Dictionary<string, object>();
            SearchInfo["PupilID"] = MonitoringBook.PupilID;
            SearchInfo["HealthPeriodID"] = MonitoringBook.HealthPeriodID;
            SearchInfo["AcademicYearID"] = MonitoringBook.AcademicYearID;
            List<MonitoringBook> lstMonitoringBook = MonitoringBookBusiness.SearchBySchool(MonitoringBook.SchoolID, SearchInfo).ToList();
            if (lstMonitoringBook.Count == 0)
            {
                if (null == MonitoringBook.MonitoringDate)
                {
                    throw new BusinessException("MonitoringBook_Label_MonitoringDateRepuired");
                }
                PhysicalTest.MonitoringBookID = MonitoringBook.MonitoringBookID;
                base.Insert(PhysicalTest);
                //Thực hiện insert dữ liệu vào bảng MonitoringBook bằng cách gọi hàm 
                MonitoringBookBusiness.Insert(MonitoringBook);
            }
            else
            {
                //Thực hiện Insert dữ liệu vào bảng PhysicalTest với MonitoringBookID = lstMonitoringBook.MonitoringBookID
                PhysicalTest.MonitoringBookID = lstMonitoringBook.FirstOrDefault().MonitoringBookID;
                PhysicalTest.Date = DateTime.Now;//lstMonitoringBook.FirstOrDefault().MonitoringDate;

                //entity.MonitoringBookID: FK(MonitoringBook), duplicate                                
                bool MonitoringBookDiplicate = this.repository.ExistsRow(GlobalConstants.HEALTH_SCHEMA, "PhysicalTest",
                 new Dictionary<string, object>()
                {
                    {"MonitoringBookID",PhysicalTest.MonitoringBookID},
                    {"IsActive",true}
                }, new Dictionary<string, object>()
                {
                    //{"MonitoringBookID",entity.MonitoringBookID},
                });
                if (MonitoringBookDiplicate)
                {
                    throw new BusinessException("MonitoringBook_Label_CheckDuplicateMonitoringBookID");
                }
                base.Insert(PhysicalTest);
            }
        }
        /// <summary>
        /// Cập nhật thông tin khám sức khỏe – Thể lực
        /// author: trangdd
        /// </summary>
        public void Update(PhysicalTest PhysicalTest, MonitoringBook MonitoringBook)
        {
            ValidationMetadata.ValidateObject(PhysicalTest);
            //Thực hiện update dữ liệu vào bảng MonitoringBook bằng cách gọi hàm 
            MonitoringBookBusiness.Update(MonitoringBook);
            base.Update(PhysicalTest);
        }
        /// <summary>
        /// Xoa thông tin khám sức khỏe – Thể lực
        /// author: trangdd
        /// </summary>
        public void Delete(int PhysicalTestID, int SchoolID)
        {
            //PhysicalTestID: PK(PhysicalTest)          
            new PhysicalTestBusiness(null).CheckAvailable((int)PhysicalTestID, "PhysicalTest_Label_PhysicalTestID", false);

            int MonitoringBookID = this.Find(PhysicalTestID).MonitoringBookID;
            //MonitoringBookID, SchoolID: not compatible(MonitoringBook)
            bool SchoolCompatible = new MonitoringBookRepository(this.context).ExistsRow(GlobalConstants.HEALTH_SCHEMA, "MonitoringBook",
                  new Dictionary<string, object>()
                {
                    {"MonitoringBookID",MonitoringBookID},
                    {"SchoolID",SchoolID}
                }, null);
            if (!SchoolCompatible)
            {
                throw new BusinessException("Common_Validate_NotCompatible");
            }
            this.Delete(PhysicalTestID, true);
        }

        /// <summary>
        /// InsertOrUpdate PhysicalTest - for children
        /// </summary>
        /// <auth>HaiVT</auth>
        /// <date>24/04/2013</date>
        /// <param name="lstMonthlyGrowthBO"></param>
        /// <param name="dic"></param>
        /// <returns></returns>
        public ResultBO InsertOrUpdatePhysicalTest(List<MonthlyGrowthBO> lstMonthlyGrowthBO, IDictionary<string, object> dic)
        {
            ResultBO objResultBO = new ResultBO();
            try
            {
                int classID = Utils.GetInt(dic, "classID");
                DateTime? monthSelect = Utils.GetDateTime(dic, "monthSelect");
                int schoolID = Utils.GetInt(dic, "schoolID");
                int academicYearID = Utils.GetInt(dic, "academicYearID");
                int educationLevelID = Utils.GetInt(dic, "educationLevelID");
                StringBuilder sbLogResult = new StringBuilder();
                StringBuilder sbJson = new StringBuilder();

                //lst dung de MonitoringBook update
                List<MonitoringBook> lstMonitoringBookUpdate = MonitoringBookBusiness.All.Where(p => p.ClassID == classID
                                                                && p.SchoolID == schoolID && p.AcademicYearID == academicYearID
                                                                && p.MonitoringType == GlobalConstants.MONITORINGBOOK_TYPE_MONTH
                                                                && p.MonitoringDate.HasValue ? p.MonitoringDate.Value.Month == monthSelect.Value.Month : true
                                                                 && p.MonitoringDate.HasValue ? p.MonitoringDate.Value.Year == monthSelect.Value.Year : true).ToList();

                List<int> lstMonitoringID = new List<int>();
                for (int i = lstMonitoringBookUpdate.Count - 1; i >= 0; i--)
                {
                    lstMonitoringID.Add(lstMonitoringBookUpdate[i].MonitoringBookID);
                }
                //lst PhysicalTest dung de update
                List<PhysicalTest> lstPhysicalTest = PhysicalTestBusiness.All.Where(p => lstMonitoringID.Contains(p.MonitoringBookID)).ToList();

                //lay list classificationlevelHealth de tinh nutrition
                List<ClassificationLevelHealth> lstClassificationLevelHealth = ClassificationLevelHealthBusiness.All.Where(p => p.TypeConfigID == GlobalConstants.TypeConfig_Weight).ToList();

                ClassificationBO objClassificationBO = new ClassificationBO();
                objClassificationBO.MaxLevel = lstClassificationLevelHealth.Max(p => p.Level);
                objClassificationBO.MinLevel = lstClassificationLevelHealth.Min(p => p.Level);

                for (int i = lstClassificationLevelHealth.Count - 1; i >= 0; i--)
                {
                    if (lstClassificationLevelHealth[i].Level == objClassificationBO.MaxLevel.Value)
                    {
                        objClassificationBO.MaxClassificationLevelID = lstClassificationLevelHealth[i].ClassificationLevelHealthID;
                    }
                }

                for (int i = lstClassificationLevelHealth.Count - 1; i >= 0; i--)
                {
                    if (lstClassificationLevelHealth[i].Level == objClassificationBO.MinLevel.Value)
                    {
                        objClassificationBO.MinClassificationLevelID = lstClassificationLevelHealth[i].ClassificationLevelHealthID;
                    }
                }

                //lay ClassificationCriterialID de tinh nutrition
                objClassificationBO.MaleClassificationCriterialID = ClassificationCriteriaBusiness.All.Where(p => p.Genre == GlobalConstants.ClassificationCriterialGenre_MALE
                                                                                                && p.TypeConfigID == GlobalConstants.TypeConfig_Weight)
                                                                                        .OrderBy(p => p.EffectDate)
                                                                                        .Select(p => p.ClassificationCriteriaID).FirstOrDefault();
                objClassificationBO.FemaleClassificationCriterialID = ClassificationCriteriaBusiness.All.Where(p => p.Genre == GlobalConstants.ClassificationCriterialGenre_FEMALE
                                                                                                && p.TypeConfigID == GlobalConstants.TypeConfig_Weight)
                                                                                        .OrderBy(p => p.EffectDate)
                                                                                        .Select(p => p.ClassificationCriteriaID).FirstOrDefault();

                MonthlyGrowthBO objMonthlyGrowthBO = null;
                MonitoringBook objMonitoringBook = null;
                PhysicalTest objPhysicalTest = null;
                DateTime dateTimeNow = DateTime.Now;
                for (int i = lstMonthlyGrowthBO.Count - 1; i >= 0; i--)
                {
                    objMonthlyGrowthBO = lstMonthlyGrowthBO[i];

                    //set gioi tinh va thang cho object classification de kiem tra dinh duong
                    objClassificationBO.Genre = objMonthlyGrowthBO.Genre;
                    objClassificationBO.Month = ((dateTimeNow.Year - objMonthlyGrowthBO.BirthDate.Year) * 12) + dateTimeNow.Month - objMonthlyGrowthBO.BirthDate.Month;
                    objClassificationBO.Weight = objMonthlyGrowthBO.Weight;

                    //kiem tra monitoring book insert or update
                    objMonitoringBook = null;
                    //update monitoringBook
                    if (lstMonitoringBookUpdate != null && lstMonitoringBookUpdate.Count > 0)
                    {
                        for (int j = lstMonitoringBookUpdate.Count - 1; j >= 0; j--)
                        {
                            if (lstMonitoringBookUpdate[j].PupilID == objMonthlyGrowthBO.PupilID)
                            {
                                objMonitoringBook = lstMonitoringBookUpdate[j];
                                objMonitoringBook.Solution = objMonthlyGrowthBO.Solution;
                                objMonitoringBook.EducationLevelID = educationLevelID;
                                //kiem tra physical test insert or update
                                if (lstPhysicalTest.Count > 0)
                                {
                                    objPhysicalTest = null;
                                    //update physicalTest
                                    for (int k = lstPhysicalTest.Count - 1; k >= 0; k--)
                                    {
                                        if (lstPhysicalTest[k].MonitoringBookID == objMonitoringBook.MonitoringBookID)
                                        {
                                            if (objMonthlyGrowthBO.Height.HasValue && objMonthlyGrowthBO.Weight.HasValue)
                                            {
                                                objPhysicalTest = lstPhysicalTest[k];
                                                objPhysicalTest.Height = objMonthlyGrowthBO.Height.Value;
                                                objPhysicalTest.Weight = objMonthlyGrowthBO.Weight.Value;
                                                objPhysicalTest.IsActive = true;
                                                objPhysicalTest.ModifiedDate = dateTimeNow;

                                                //neu thang tuoi lon hon 60 thi khong xac dinh dinh duong
                                                if (objClassificationBO.Month < 0 || objClassificationBO.Month > 60)
                                                {
                                                    objPhysicalTest.Nutrition = GlobalConstants.PHYSICALTEST_UNDENTIFY_NUTRITION;
                                                    objPhysicalTest.PhysicalClassification = GlobalConstants.PHYSICALTEST_UNDENTIFY_NUTRITION;
                                                }
                                                else
                                                {
                                                    objPhysicalTest.Nutrition = CheckNutrition(objClassificationBO);
                                                    switch (objPhysicalTest.Nutrition)
                                                    {
                                                        case GlobalConstants.PHYSICALTEST_MAL_NUTRITION:
                                                            {
                                                                objPhysicalTest.PhysicalClassification = SystemParamsInFile.PHYSICAL_CLASSIFICATION_POOR;
                                                                break;
                                                            }
                                                        case GlobalConstants.PHYSICALTEST_NORMAL_WEIGHT:
                                                            {
                                                                objPhysicalTest.PhysicalClassification = SystemParamsInFile.PHYSICAL_CLASSIFICATION_EXCELLENT;
                                                                break;
                                                            }
                                                        case GlobalConstants.PHYSICALTEST_OVER_WEIGHT:
                                                            {
                                                                objPhysicalTest.PhysicalClassification = SystemParamsInFile.PHYSICAL_CLASSIFICATION_POOR;
                                                                break;
                                                            }
                                                        default:
                                                            break;
                                                    }
                                                }
                                            }
                                            else
                                            {
                                                objPhysicalTest = lstPhysicalTest[k];
                                                objPhysicalTest.IsActive = false;
                                            }

                                            sbLogResult.Append("Update PT ").Append(objPhysicalTest.PhysicalTestID.ToString());
                                        }
                                    }
                                    //insert physicalTest
                                    if (objPhysicalTest == null)
                                    {
                                        if (objMonthlyGrowthBO.Weight.HasValue && objMonthlyGrowthBO.Height.HasValue)
                                        {
                                            objPhysicalTest = new PhysicalTest();
                                            objPhysicalTest.MonitoringBookID = objMonitoringBook.MonitoringBookID;
                                            objPhysicalTest.Date = monthSelect.Value;
                                            objPhysicalTest.Height = objMonthlyGrowthBO.Height.Value;
                                            objPhysicalTest.Weight = objMonthlyGrowthBO.Weight.Value;
                                            objPhysicalTest.Breast = 0;//children khong tinh vong nguc
                                            objPhysicalTest.CreatedDate = dateTimeNow;
                                            objPhysicalTest.IsActive = true;

                                            //neu thang tuoi lon hon 60 thi khong xac dinh dinh duong
                                            if (objClassificationBO.Month < 0 || objClassificationBO.Month > 60)
                                            {
                                                objPhysicalTest.Nutrition = GlobalConstants.PHYSICALTEST_UNDENTIFY_NUTRITION;
                                                objPhysicalTest.PhysicalClassification = GlobalConstants.PHYSICALTEST_UNDENTIFY_NUTRITION;
                                            }
                                            else
                                            {
                                                objPhysicalTest.Nutrition = CheckNutrition(objClassificationBO);
                                                switch (objPhysicalTest.Nutrition)
                                                {
                                                    case GlobalConstants.PHYSICALTEST_MAL_NUTRITION:
                                                        {
                                                            objPhysicalTest.PhysicalClassification = SystemParamsInFile.PHYSICAL_CLASSIFICATION_POOR;
                                                            break;
                                                        }
                                                    case GlobalConstants.PHYSICALTEST_NORMAL_WEIGHT:
                                                        {
                                                            objPhysicalTest.PhysicalClassification = SystemParamsInFile.PHYSICAL_CLASSIFICATION_EXCELLENT;
                                                            break;
                                                        }
                                                    case GlobalConstants.PHYSICALTEST_OVER_WEIGHT:
                                                        {
                                                            objPhysicalTest.PhysicalClassification = SystemParamsInFile.PHYSICAL_CLASSIFICATION_POOR;
                                                            break;
                                                        }
                                                    default:
                                                        break;
                                                }
                                            }
                                            PhysicalTestBusiness.Insert(objPhysicalTest);
                                            sbLogResult.Append("Insert PT ID mb ").Append(objPhysicalTest.MonitoringBookID.ToString());
                                        }
                                    }
                                }
                                else
                                {
                                    //insert physicalTest
                                    if (objMonthlyGrowthBO.Height.HasValue && objMonthlyGrowthBO.Weight.HasValue)
                                    {
                                        objPhysicalTest = new PhysicalTest();
                                        objPhysicalTest.MonitoringBookID = objMonitoringBook.MonitoringBookID;
                                        objPhysicalTest.Date = monthSelect.Value;
                                        objPhysicalTest.Height = objMonthlyGrowthBO.Height.Value;
                                        objPhysicalTest.Weight = objMonthlyGrowthBO.Weight.Value;
                                        objPhysicalTest.Breast = 0; //children khong tinh vong nguc
                                        objPhysicalTest.CreatedDate = dateTimeNow;
                                        objPhysicalTest.IsActive = true;

                                        //neu thang tuoi lon hon 60 thi khong xac dinh dinh duong
                                        if (objClassificationBO.Month < 0 || objClassificationBO.Month > 60)
                                        {
                                            objPhysicalTest.Nutrition = GlobalConstants.PHYSICALTEST_UNDENTIFY_NUTRITION;
                                            objPhysicalTest.PhysicalClassification = GlobalConstants.PHYSICALTEST_UNDENTIFY_NUTRITION;
                                        }
                                        else
                                        {
                                            objPhysicalTest.Nutrition = CheckNutrition(objClassificationBO);
                                            switch (objPhysicalTest.Nutrition)
                                            {
                                                case GlobalConstants.PHYSICALTEST_MAL_NUTRITION:
                                                    {
                                                        objPhysicalTest.PhysicalClassification = SystemParamsInFile.PHYSICAL_CLASSIFICATION_POOR;
                                                        break;
                                                    }
                                                case GlobalConstants.PHYSICALTEST_NORMAL_WEIGHT:
                                                    {
                                                        objPhysicalTest.PhysicalClassification = SystemParamsInFile.PHYSICAL_CLASSIFICATION_EXCELLENT;
                                                        break;
                                                    }
                                                case GlobalConstants.PHYSICALTEST_OVER_WEIGHT:
                                                    {
                                                        objPhysicalTest.PhysicalClassification = SystemParamsInFile.PHYSICAL_CLASSIFICATION_POOR;
                                                        break;
                                                    }
                                                default:
                                                    break;
                                            }
                                        }

                                        PhysicalTestBusiness.Insert(objPhysicalTest);
                                        sbLogResult.Append("Insert PT ID mb ").Append(objPhysicalTest.MonitoringBookID.ToString());
                                    }
                                }
                            }
                        }
                    }
                    //insert monitoring book
                    if (objMonitoringBook == null)
                    {
                        if (objMonthlyGrowthBO.Weight.HasValue && objMonthlyGrowthBO.Height.HasValue)
                        {
                            //insert monitoringBook
                            objMonitoringBook = new MonitoringBook();
                            objMonitoringBook.AcademicYearID = academicYearID;
                            objMonitoringBook.SchoolID = schoolID;
                            objMonitoringBook.ClassID = classID;
                            objMonitoringBook.PupilID = objMonthlyGrowthBO.PupilID;
                            objMonitoringBook.MonitoringDate = monthSelect.Value;
                            objMonitoringBook.Solution = objMonthlyGrowthBO.Solution;
                            objMonitoringBook.MonitoringType = GlobalConstants.MONITORINGBOOK_TYPE_MONTH;
                            objMonitoringBook.CreateDate = dateTimeNow;
                            objMonitoringBook.EducationLevelID = educationLevelID;

                            MonitoringBookBusiness.Insert(objMonitoringBook);

                            sbLogResult.Append("Insert MB ").Append(objMonitoringBook.MonitoringBookID.ToString());
                            //insert physicalTest
                            objPhysicalTest = new PhysicalTest();
                            objPhysicalTest.MonitoringBook = objMonitoringBook;
                            objPhysicalTest.Date = monthSelect.Value;
                            objPhysicalTest.Height = objMonthlyGrowthBO.Height.Value;
                            objPhysicalTest.Weight = objMonthlyGrowthBO.Weight.Value;
                            objPhysicalTest.Breast = 0; //children khong tinh vong nguc
                            objPhysicalTest.CreatedDate = dateTimeNow;
                            objPhysicalTest.IsActive = true;
                            //neu thang tuoi lon hon 60 thi khong xac dinh dinh duong
                            if (objClassificationBO.Month < 0 || objClassificationBO.Month > 60)
                            {
                                objPhysicalTest.Nutrition = GlobalConstants.PHYSICALTEST_UNDENTIFY_NUTRITION;
                                objPhysicalTest.PhysicalClassification = GlobalConstants.PHYSICALTEST_UNDENTIFY_NUTRITION;
                            }
                            else
                            {
                                objPhysicalTest.Nutrition = CheckNutrition(objClassificationBO);
                                switch (objPhysicalTest.Nutrition)
                                {
                                    case GlobalConstants.PHYSICALTEST_MAL_NUTRITION:
                                        {
                                            objPhysicalTest.PhysicalClassification = SystemParamsInFile.PHYSICAL_CLASSIFICATION_POOR;
                                            break;
                                        }
                                    case GlobalConstants.PHYSICALTEST_NORMAL_WEIGHT:
                                        {
                                            objPhysicalTest.PhysicalClassification = SystemParamsInFile.PHYSICAL_CLASSIFICATION_EXCELLENT;
                                            break;
                                        }
                                    case GlobalConstants.PHYSICALTEST_OVER_WEIGHT:
                                        {
                                            objPhysicalTest.PhysicalClassification = SystemParamsInFile.PHYSICAL_CLASSIFICATION_POOR;
                                            break;
                                        }
                                    default:
                                        break;
                                }
                            }

                            PhysicalTestBusiness.Insert(objPhysicalTest);
                            sbLogResult.Append("Insert PT ID mb").Append(objPhysicalTest.MonitoringBookID.ToString());
                        }
                    }

                    if (objMonthlyGrowthBO.Weight.HasValue && objMonthlyGrowthBO.Height.HasValue)
                    {
                        sbJson.Append("{\"PupilID\":").Append(objMonthlyGrowthBO.PupilID.ToString()).Append(",")
                            .Append("\"Nutrition\":").Append(objPhysicalTest.Nutrition).Append("},");
                    }
                    else
                    {
                        sbJson.Append("{\"PupilID\":").Append(objMonthlyGrowthBO.PupilID.ToString()).Append(",")
                                                .Append("\"Nutrition\":").Append("0").Append("},");
                    }
                }

                MonitoringBookBusiness.Save();

                objResultBO.LogResult = sbLogResult.ToString();
                objResultBO.Result = "Common_Label_Save";
                objResultBO.JsonResult = sbJson.ToString();

                return objResultBO;
            }
            catch (Exception ex)
            {
                
                LogExtensions.ErrorExt(logger, DateTime.Now, "InsertOrUpdatePhysicalTest", "null", ex);
                objResultBO.isError = true;
                objResultBO.ErrorMsg = "SCSMonthlyGrowth_Message_Error";
                return objResultBO;
            }
        }

        /// <summary>
        /// check Nutrition children
        /// </summary>
        /// <auth>HaiVT</auth>
        /// <date>24/04/2013</date>
        /// <param name="objClassificationBO"></param>
        /// <returns></returns>
        public int CheckNutrition(ClassificationBO objClassificationBO)
        {
            //be trai
            if (objClassificationBO.Genre == SystemParamsInFile.GENRE_MALE)
            {
                //kiem tra beo phi
                decimal? indexValue = ClassificationCriteriaDetailBusiness.All.Where(p => p.Month == objClassificationBO.Month
                                                                                                && p.ClassificationCriteriaID == objClassificationBO.MaleClassificationCriterialID
                                                                                                && p.ClassificationLevelHealthID == objClassificationBO.MaxClassificationLevelID
                                                                                    ).Select(p => p.IndexValue).FirstOrDefault();
                if (indexValue.HasValue && objClassificationBO.Weight > indexValue.Value)
                {
                    return GlobalConstants.PHYSICALTEST_OVER_WEIGHT;
                }
                else
                {
                    //kiem tra suy dinh duong
                    indexValue = ClassificationCriteriaDetailBusiness.All.Where(p => p.Month == objClassificationBO.Month
                                                                                                && p.ClassificationCriteriaID == objClassificationBO.MaleClassificationCriterialID
                                                                                                && p.ClassificationLevelHealthID == objClassificationBO.MinClassificationLevelID
                                                                                    ).Select(p => p.IndexValue).FirstOrDefault();
                    if (indexValue.HasValue && objClassificationBO.Weight < indexValue.Value)
                    {
                        return GlobalConstants.PHYSICALTEST_MAL_NUTRITION;
                    }
                }
                //binh thuong
                return GlobalConstants.PHYSICALTEST_NORMAL_WEIGHT;
            }
            else//be gai
            {
                //kiem tra beo phi
                decimal? indexValue = ClassificationCriteriaDetailBusiness.All.Where(p => p.Month == objClassificationBO.Month
                                                                                                && p.ClassificationCriteriaID == objClassificationBO.FemaleClassificationCriterialID
                                                                                                && p.ClassificationLevelHealthID == objClassificationBO.MaxClassificationLevelID
                                                                                    ).Select(p => p.IndexValue).FirstOrDefault();
                if (indexValue.HasValue && objClassificationBO.Weight > indexValue.Value)
                {
                    return GlobalConstants.PHYSICALTEST_OVER_WEIGHT;
                }
                else
                {
                    //kiem tra suy dinh duong
                    indexValue = ClassificationCriteriaDetailBusiness.All.Where(p => p.Month == objClassificationBO.Month
                                                                                                && p.ClassificationCriteriaID == objClassificationBO.FemaleClassificationCriterialID
                                                                                                && p.ClassificationLevelHealthID == objClassificationBO.MinClassificationLevelID
                                                                                    ).Select(p => p.IndexValue).FirstOrDefault();
                    if (indexValue.HasValue && objClassificationBO.Weight < indexValue.Value)
                    {
                        return GlobalConstants.PHYSICALTEST_MAL_NUTRITION;
                    }
                }
                //binh thuong
                return GlobalConstants.PHYSICALTEST_NORMAL_WEIGHT;
            }
        }

        // Thuyen - 05/10/2017
        public List<PhysicalTest> ListPhysicalTestByMonitoringBookId(List<int> lstMoniBookID)
        {
            return PhysicalTestBusiness.All.Where(x => lstMoniBookID.Contains(x.MonitoringBookID)).ToList();
        }
    }
}