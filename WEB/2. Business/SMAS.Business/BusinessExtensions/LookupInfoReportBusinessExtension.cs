﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SMAS.Business.BusinessObject;
using SMAS.Business.Business;
using SMAS.Business.Common;
using SMAS.Models.Models;
using System.IO;
using SMAS.VTUtils.Excel.Export;
using SMAS.DAL.Repository;
using System.Data;
using Oracle.DataAccess.Client;
using System.Threading.Tasks;
using SMAS.VTUtils.Excel.ExportXML;

namespace SMAS.Business.Business
{
    public partial class LookupInfoReportBusiness
    {
        #region Xuất excel cho cán bộ
        public int startCol = 1;
        const int rowHeaderStart = 8;
        const int rowHeaderEnd = 9;
        public Stream ExportLookupInfo(Dictionary<String, object> dic, out string FileName)
        {
            int HierachyLevel = Utils.GetInt(dic, "HierachyLevel");
            if (dic["form1[ProvinceID]"] == "")
            {
                dic["form1[ProvinceID]"] = "0";
            }
            if (dic["form1[EducationGrade]"] == "")
            {
                dic["form1[EducationGrade]"] = "0";
            }
            if (dic["form1[District]"] == "")
            {
                dic["form1[District]"] = "0";
            }
            if (dic["form1[SupervisingDeptID1]"] == "")
            {
                dic["form1[SupervisingDeptID1]"] = "0";
            }
            if (dic["form1[School]"] == "")
            {
                dic["form1[School]"] = "0";
            }
            if (dic["form1[Sex]"] == "")
            {
                dic["form1[Sex]"] = "-1";
            }
            if (dic["form1[EmploymentStatus]"] == "")
            {
                dic["form1[EmploymentStatus]"] = "0";
            }
            if (dic["form1[SchoolFaculty]"] == "")
            {
                dic["form1[SchoolFaculty]"] = "0";
            }
            if (dic["form1[WorkType]"] == "")
            {
                dic["form1[WorkType]"] = "0";
            }
            if (dic["form1[ContractType]"] == "")
            {
                dic["form1[ContractType]"] = "0";
            }
            int ProvinceID = Utils.GetInt(dic, "form1[ProvinceID]");
            int AppliedLevel = Utils.GetInt(dic, "form1[EducationGrade]");
            int DistrictID = Utils.GetInt(dic, "form1[District]");
            int SupervisingDeptID = Utils.GetInt(dic, "form1[SupervisingDeptID1]");
            int SchoolID = Utils.GetInt(dic, "form1[School]");
            string EmployeeCode = Utils.GetString(dic, "form1[EmployeeCode]");
            string FullName = Utils.GetString(dic, "form1[FullName]");
            bool isSub = Utils.GetBool(dic, "IsSub");
            DateTime? BirthDate;// = Utils.GetDateTime(convert(dic, "form1[Birthdate]"));
            DateTime temp;
            if (dic.ContainsKey("form1[Birthdate]"))
            {
                if (DateTime.TryParse(dic["form1[Birthdate]"].ToString(), out temp))
                {
                    BirthDate = temp;
                }
                else
                {
                    BirthDate = null;
                }
            }
            else
            {
                BirthDate = null;
            }
            int SchoolFacultyID = Utils.GetInt(dic, "form1[SchoolFaculty]");
            int EmploymentStatus = Utils.GetInt(dic, "form1[EmploymentStatus]");
            int WorkTypeID = Utils.GetInt(dic, "form1[WorkType]");
            int ContractTypeID = Utils.GetInt(dic, "form1[ContractType]");
            int Genre = Utils.GetInt(dic, "form1[Sex]");

            string reportCode = "";
            reportCode = SystemParamsInFile.REPORT_DANHSACHCANBO;
            ReportDefinition rp = ReportDefinitionBusiness.GetByCode(reportCode);
            string templatePath = SystemParamsInFile.TEMPLATE_FOLDER + rp.TemplateDirectory + "/" + rp.TemplateName;
            IVTWorkbook oBook = VTExport.OpenWorkbook(templatePath);
            IVTWorksheet firstSheet = oBook.GetSheet(1);
            IVTWorksheet sheet = oBook.GetSheet(2);
            IVTRange range = firstSheet.GetRange("A1", "J7");
            sheet.CopyPasteSameSize(range, "A1");
            int SupervisingDeptID1 = Utils.GetInt(convert(dic, "form1[SupervisingDeptID1]"));
            SupervisingDept sp = SupervisingDeptBusiness.Find(SupervisingDeptID1);
            Province province = ProvinceBusiness.Find(ProvinceID);

            sheet.SetCellValue("A2", sp.SupervisingDeptName);
            string date = province.ProvinceName + ", ngày " + DateTime.Now.Day + " tháng " + DateTime.Now.Month + " năm " + DateTime.Now.Year;
            sheet.SetCellValue("F4", date);
            //Tìm File Name trong CSDL
            string outputNamePattern = rp.OutputNamePattern;
            if (HierachyLevel == SystemParamsInFile.EDUCATION_HIERACHY_LEVEL_PROVINCE_OFFICE)
            {
                outputNamePattern = outputNamePattern.Replace("[LevelAccount]", ReportUtils.StripVNSign("SGD"));
            }
            if (HierachyLevel == SystemParamsInFile.EDUCATION_HIERACHY_LEVEL_DISTRICT_OFFICE)
            {
                outputNamePattern = outputNamePattern.Replace("[LevelAccount]", ReportUtils.StripVNSign("PGD"));
            }
            FileName = ReportUtils.RemoveSpecialCharacters(outputNamePattern) + "." + rp.OutputFormat;

            #region Kiểm tra các trường được check
            bool chkFullName = CheckSelected("form1[chkFullName]", dic);
            bool chkEmployeeCode = CheckSelected("form1[chkEmployeeCode]", dic);
            bool chkBirthDate = CheckSelected("form1[chkBirthDate]", dic);
            bool chkSex = CheckSelected("form1[chkSex]", dic);
            bool chkEthnic = CheckSelected("form1[chkEthnic]", dic);
            bool chkReligion = CheckSelected("form1[chkReligion]", dic);
            bool chkIdentifyNumber = CheckSelected("form1[chkIdentifyNumber]", dic);
            bool chkIdentifyIssuedDate = CheckSelected("form1[chkIdentifyIssuedDate]", dic);
            bool chkIdentifyIssuedPlace = CheckSelected("form1[chkIdentifyIssuedPlace]", dic);
            bool chkMobile = CheckSelected("form1[chkMobile]", dic);
            bool chkEmail = CheckSelected("form1[chkEmail]", dic);
            bool chkHomeTown = CheckSelected("form1[chkHomeTown]", dic);
            bool chkPermanentResidentalAddress = CheckSelected("form1[chkPermanentResidentalAddress]", dic);
            bool chkYouthLeageMember = CheckSelected("form1[chkYouthLeageMember]", dic);
            bool chkYouthLeagueJoinedDate = CheckSelected("form1[chkYouthLeagueJoinedDate]", dic);
            bool chkCommunistPartyMember = CheckSelected("form1[chkCommunistPartyMember]", dic);
            bool chkCommunistPartyJoinedDate = CheckSelected("form1[chkCommunistPartyJoinedDate]", dic);
            bool chkFatherFullName = CheckSelected("form1[chkFatherFullName]", dic);
            bool chkFatherBirthDate = CheckSelected("form1[chkFatherBirthDate]", dic);
            bool chkMotherFullName = CheckSelected("form1[chkMotherFullName]", dic);
            bool chkMotherBirthDate = CheckSelected("form1[chkMotherBirthDate]", dic);
            bool chkSpouseFullName = CheckSelected("form1[chkSpouseFullName]", dic);
            bool chkSpouseBirthDate = CheckSelected("form1[chkSpouseBirthDate]", dic);
            bool chkGraduationLevel = CheckSelected("form1[chkGraduationLevel]", dic);
            bool chkQualificationLevel = CheckSelected("form1[chkQualificationLevel]", dic);
            bool chkForeignLanguageGrade = CheckSelected("form1[chkForeignLanguageGrade]", dic);
            bool chkITQualificationLevel = CheckSelected("form1[chkITQualificationLevel]", dic);
            bool chkPoliticalGrade = CheckSelected("form1[chkPoliticalGrade]", dic);
            bool chkSpecialityCat = CheckSelected("form1[chkSpecialityCat]", dic);
            bool chkContractType = CheckSelected("form1[chkContractType]", dic);
            bool chkWorkType = CheckSelected("form1[chkWorkType]", dic);
            bool chkSchoolFaculty = CheckSelected("form1[chkSchoolFaculty]", dic);
            bool chkSchool = CheckSelected("form1[chkSchool]", dic);
            bool chkSTT = true;
            #endregion
            #region Copy Header của file Excel
            int P_FullName = 0, P_EmployeeCode = 0, P_BirthDate = 0, P_Sex = 0, P_Ethnic = 0, P_Religion = 0, P_IdentifyNumber = 0, P_IdentifyIssuedDate = 0, P_IdentifyIssuedPlace = 0, P_Mobile = 0, P_Email = 0, P_HomeTown = 0, P_PermanentResidentalAddress = 0, P_YouthLeageMember = 0, P_YouthLeagueJoinedDate = 0, P_CommunistPartyMember = 0, P_CommunistPartyJoinedDate = 0, P_FatherFullName = 0, P_FatherBirthDate = 0, P_MotherFullName = 0, P_MotherBirthDate = 0, P_SpouseFullName = 0, P_SpouseBirthDate = 0, P_GraduationLevel = 0, P_QualificationLevel = 0, P_ForeignLanguageGrade = 0, P_ITQualificationLevel = 0, P_PoliticalGrade = 0, P_SpecialityCat = 0, P_ContractType = 0, P_WorkType = 0, P_SchoolFaculty = 0, P_School = 0;

            if (chkSTT)
            {
                copyCol(firstSheet, sheet, 1, 1);
            }
            if (chkFullName)
            {
                P_FullName = startCol;
                copyCol(firstSheet, sheet, 2, startCol);
            }
            if (chkEmployeeCode)
            {
                P_EmployeeCode = startCol;
                copyCol(firstSheet, sheet, 3, startCol);
            }
            if (chkBirthDate)
            {
                P_BirthDate = startCol;
                copyCol(firstSheet, sheet, 4, startCol);
            }
            if (chkSex)
            {
                P_Sex = startCol;
                copyCol(firstSheet, sheet, 5, startCol);
            }
            if (chkEthnic)
            {
                P_Ethnic = startCol;
                copyCol(firstSheet, sheet, 6, startCol);
            }
            if (chkReligion)
            {
                P_Religion = startCol;
                copyCol(firstSheet, sheet, 7, startCol);
            }
            if (chkIdentifyNumber)
            {
                P_IdentifyNumber = startCol;
                copyCol(firstSheet, sheet, 8, startCol);
            }
            if (chkIdentifyIssuedDate)
            {
                P_IdentifyIssuedDate = startCol;
                copyCol(firstSheet, sheet, 9, startCol);
            }
            if (chkIdentifyIssuedPlace)
            {
                P_IdentifyIssuedPlace = startCol;
                copyCol(firstSheet, sheet, 10, startCol);
            }
            if (chkMobile)
            {
                P_Mobile = startCol;
                copyCol(firstSheet, sheet, 11, startCol);
            }
            if (chkEmail)
            {
                P_Email = startCol;
                copyCol(firstSheet, sheet, 12, startCol);
            }
            if (chkHomeTown)
            {
                P_HomeTown = startCol;
                copyCol(firstSheet, sheet, 13, startCol);
            }
            if (chkPermanentResidentalAddress)
            {
                P_PermanentResidentalAddress = startCol;
                copyCol(firstSheet, sheet, 14, startCol);
            }
            if (chkYouthLeageMember)
            {
                P_YouthLeageMember = startCol;
                copyCol(firstSheet, sheet, 15, startCol);
            }
            if (chkYouthLeagueJoinedDate)
            {
                P_YouthLeagueJoinedDate = startCol;
                copyCol(firstSheet, sheet, 16, startCol);
            }
            if (chkCommunistPartyMember)
            {
                P_CommunistPartyMember = startCol;
                copyCol(firstSheet, sheet, 17, startCol);
            }
            if (chkCommunistPartyJoinedDate)
            {
                P_CommunistPartyJoinedDate = startCol;
                copyCol(firstSheet, sheet, 18, startCol);
            }

            if (chkFatherFullName)
            {
                P_FatherFullName = startCol;
                copyCol(firstSheet, sheet, 19, startCol);
            }

            if (chkFatherBirthDate)
            {
                P_FatherBirthDate = startCol;
                copyCol(firstSheet, sheet, 20, startCol);
            }
            if (chkMotherFullName)
            {
                P_MotherFullName = startCol;
                copyCol(firstSheet, sheet, 21, startCol);
            }
            if (chkMotherBirthDate)
            {
                P_MotherBirthDate = startCol;
                copyCol(firstSheet, sheet, 22, startCol);
            }
            if (chkSpouseFullName)
            {
                P_SpouseFullName = startCol;
                copyCol(firstSheet, sheet, 23, startCol);
            }
            if (chkSpouseBirthDate)
            {
                P_SpouseBirthDate = startCol;
                copyCol(firstSheet, sheet, 24, startCol);
            }
            if (chkGraduationLevel)
            {
                P_GraduationLevel = startCol;
                copyCol(firstSheet, sheet, 25, startCol);
            }
            if (chkQualificationLevel)
            {
                P_QualificationLevel = startCol;
                copyCol(firstSheet, sheet, 26, startCol);
            }
            if (chkForeignLanguageGrade)
            {
                P_ForeignLanguageGrade = startCol;
                copyCol(firstSheet, sheet, 27, startCol);
            }
            if (chkITQualificationLevel)
            {
                P_ITQualificationLevel = startCol;
                copyCol(firstSheet, sheet, 28, startCol);
            }
            if (chkPoliticalGrade)
            {
                P_PoliticalGrade = startCol;
                copyCol(firstSheet, sheet, 29, startCol);
            }
            if (chkSpecialityCat)
            {
                P_SpecialityCat = startCol;
                copyCol(firstSheet, sheet, 30, startCol);
            }
            if (chkContractType)
            {
                P_ContractType = startCol;
                copyCol(firstSheet, sheet, 31, startCol);
            }
            if (chkWorkType)
            {
                P_WorkType = startCol;
                copyCol(firstSheet, sheet, 32, startCol);
            }
            if (chkSchoolFaculty)
            {
                P_SchoolFaculty = startCol;
                copyCol(firstSheet, sheet, 33, startCol);
            }
            if (chkSchool)
            {
                P_School = startCol;
                copyCol(firstSheet, sheet, 34, startCol);
            }

            IVTRange temRange = sheet.GetRange("A9", "AH9");

            #endregion
            #region fill dữ liệu lên sheet Excel

            Dictionary<string, object> search = new Dictionary<string, object>();
            search["ProvinceID"] = ProvinceID;
            search["DistrictID"] = DistrictID;
            search["SupervisingDeptID"] = SupervisingDeptID;
            search["SchoolID"] = SchoolID;
            search["EmployeeCode"] = EmployeeCode;
            search["FullName"] = FullName;
            search["Genre"] = Genre;
            search["EmployeeStatus"] = EmploymentStatus;
            search["ContractTypeID"] = ContractTypeID;
            search["HierachyLevel"] = HierachyLevel;
            search["WorkTypeID"] = WorkTypeID;
            search["SchoolFacultyID"] = SchoolFacultyID;
            search["BirthDate"] = BirthDate;
            search["AppliedLevel"] = AppliedLevel;
            search["IsSub"] = isSub;
            int? district = Utils.GetInt(convert(dic, "form1[District]"));

            IQueryable<EmployeeBO> lstEmployeeBO1 = EmployeeBusiness.SearchTeacherBySupervisingDept(search)
                .Where(e => e.SchoolProfile.IsActive)
                .Select(e => new EmployeeBO
                {
                    EmployeeID = e.EmployeeID,
                    FullName = e.FullName,
                    EmployeeCode = e.EmployeeCode,
                    BirthDate = e.BirthDate,
                    Genre = e.Genre,
                    SchoolFacultyID = e.SchoolFacultyID,
                    WorkTypeID = e.WorkTypeID,
                    ContractID = e.ContractID,
                    Mobile = e.Mobile,
                    SchoolID = e.SchoolID,
                    IsActive = e.IsActive,
                    EmployeeType = e.EmployeeType,
                    IdentifyNumber = e.IdentityNumber,
                    IdentityIssuedDate = e.IdentityIssuedDate,
                    IdentityIssuedPlace = e.IdentityIssuedPlace,
                    HomeTown = e.HomeTown,
                    PermanentResidentalAddress = e.PermanentResidentalAddress,
                    IsYouthLeageMember = e.IsYouthLeageMember,
                    YouthLeagueJoinedDate = e.YouthLeagueJoinedDate,
                    IsCommunistPartyMember = e.IsCommunistPartyMember,
                    CommunistPartyJoinedDate = e.CommunistPartyJoinedDate,
                    FatherFullName = e.FatherFullName,
                    FatherBirthDate = e.FatherBirthDate,
                    MotherFullName = e.MotherFullName,
                    MotherBirthDate = e.MotherBirthDate,
                    SpouseFullName = e.SpouseFullName,
                    SpouseBirthDate = e.SpouseBirthDate,
                    TrainingLevelResolution = e.TrainingLevel.Resolution,
                    QualificationResolution = e.QualificationLevel.Resolution,
                    ForeignResolution = e.ForeignLanguageGrade.Resolution,
                    ITResolution = e.ITQualificationLevel.Resolution,
                    GraduationLevelName = e.TrainingLevel.Resolution,
                    PoliticalGradeResolution = e.PoliticalGrade.Resolution,
                    SpecialityCatResolution = e.SpecialityCat.Resolution,
                    ContractTypeResolution = e.ContractType.Resolution,
                    WorkTypeResolution = e.WorkType.Resolution,
                    SchoolFacultyName = e.SchoolFaculty.FacultyName,
                    SchoolName = e.SchoolProfile.SchoolName,
                    Email = e.Email,
                    Name = e.Name,
                    ReligionName = e.Religion.Resolution,
                    EthnicName = e.Ethnic.EthnicName,
                    ContractTypeID = e.ContractTypeID
                });
            List<EmployeeBO> lstEmployee = lstEmployeeBO1.ToList();
            lstEmployee = lstEmployee.OrderBy(o => o.SchoolName).ThenBy(o => o.Name).ToList();
            int startRow = 9;
            int startSTT = 1;
            foreach (var item in lstEmployee)
            {
                sheet.CopyPasteSameSize(temRange, startRow, 1);

                bool chkItem = true;//CheckSelected(nameCheckbox, dic);
                if (chkItem)
                {
                    if (chkSTT)
                    {
                        sheet.SetCellValue(startRow, 1, startSTT);
                    }

                    if (chkFullName)
                    {
                        sheet.SetCellValue(startRow, P_FullName, item.FullName);
                    }
                    if (chkEmployeeCode)
                    {
                        sheet.SetCellValue(startRow, P_EmployeeCode, item.EmployeeCode);
                    }
                    if (chkBirthDate)
                    {
                        sheet.SetCellValue(startRow, P_BirthDate, item.BirthDate != null ? item.BirthDate.Value.ToShortDateString() : "");
                    }
                    string GenreName = item.Genre ? SystemParamsInFile.REPORT_LOOKUPINFO_MALE : SystemParamsInFile.REPORT_LOOKUPINFO_FEMALE;
                    if (chkSex)
                    {
                        sheet.SetCellValue(startRow, P_Sex, GenreName);
                    }
                    if (chkEthnic)
                    {
                        sheet.SetCellValue(startRow, P_Ethnic, item.EthnicName);
                    }
                    if (chkReligion)
                    {
                        sheet.SetCellValue(startRow, P_Religion, item.ReligionName);
                    }
                    if (chkIdentifyNumber)
                    {
                        sheet.SetCellValue(startRow, P_IdentifyNumber, item.IdentifyNumber);
                    }
                    if (chkIdentifyIssuedDate)
                    {
                        sheet.SetCellValue(startRow, P_IdentifyIssuedDate, item.IdentityIssuedDate != null ? item.IdentityIssuedDate.Value.ToShortDateString() : "");
                    }
                    if (chkIdentifyIssuedPlace)
                    {
                        sheet.SetCellValue(startRow, P_IdentifyIssuedPlace, item.IdentityIssuedPlace);
                    }

                    if (chkMobile)
                    {
                        sheet.SetCellValue(startRow, P_Mobile, item.Mobile);
                    }
                    if (chkEmail)
                    {
                        sheet.SetCellValue(startRow, P_Email, item.Email);
                    }

                    if (chkHomeTown)
                    {
                        sheet.SetCellValue(startRow, P_HomeTown, item.HomeTown);
                    }

                    if (chkPermanentResidentalAddress)
                    {
                        sheet.SetCellValue(startRow, P_PermanentResidentalAddress, item.PermanentResidentalAddress);
                    }
                    string IsYouthLeageMemberName = (item.IsYouthLeageMember.HasValue && item.IsYouthLeageMember.Value) ? SystemParamsInFile.REPORT_LOOKUPINFO_ISYOUTHLEAGEMEMBER : SystemParamsInFile.REPORT_LOOKUPINFO_NO;
                    if (chkYouthLeageMember)
                    {
                        sheet.SetCellValue(startRow, P_YouthLeageMember, IsYouthLeageMemberName);
                    }

                    if (chkYouthLeagueJoinedDate)
                    {
                        sheet.SetCellValue(startRow, P_YouthLeagueJoinedDate, item.YouthLeagueJoinedDate != null ? item.YouthLeagueJoinedDate.Value.ToShortDateString() : "");
                    }
                    bool IsCommunistPartyMember = item.IsCommunistPartyMember.HasValue ? item.IsCommunistPartyMember.Value : false;
                    string IsCommunistPartyMemberName = IsCommunistPartyMember ? SystemParamsInFile.REPORT_LOOKUPINFO_ISCOMMUNISTPARTYMEMBER : SystemParamsInFile.REPORT_LOOKUPINFO_NO;
                    if (chkCommunistPartyMember)
                    {
                        sheet.SetCellValue(startRow, P_CommunistPartyMember, IsCommunistPartyMemberName);
                    }
                    string CommunistPartyJoinedDate = item.CommunistPartyJoinedDate != null ? item.CommunistPartyJoinedDate.Value.ToShortDateString() : "";
                    if (chkCommunistPartyJoinedDate)
                    {
                        sheet.SetCellValue(startRow, P_CommunistPartyJoinedDate, CommunistPartyJoinedDate);
                    }
                    if (chkFatherFullName)
                    {
                        sheet.SetCellValue(startRow, P_FatherFullName, item.FatherFullName);
                    }
                    string FatherBirthDate = item.FatherBirthDate != null ? item.FatherBirthDate.Value.Year.ToString() : "";
                    if (chkFatherBirthDate)
                    {
                        sheet.SetCellValue(startRow, P_FatherBirthDate, FatherBirthDate);
                    }
                    if (chkMotherFullName)
                    {
                        sheet.SetCellValue(startRow, P_MotherFullName, item.MotherFullName);
                    }
                    string MotherBirthDate = item.MotherBirthDate != null ? item.MotherBirthDate.Value.Year.ToString() : "";
                    if (chkMotherBirthDate)
                    {
                        sheet.SetCellValue(startRow, P_MotherBirthDate, MotherBirthDate);
                    }
                    if (chkSpouseFullName)
                    {
                        sheet.SetCellValue(startRow, P_SpouseFullName, item.SpouseFullName);
                    }
                    string SpouseBirthDate = item.SpouseBirthDate != null ? item.SpouseBirthDate.Value.Year.ToString() : "";
                    if (chkSpouseBirthDate)
                    {
                        sheet.SetCellValue(startRow, P_SpouseBirthDate, SpouseBirthDate);
                    }
                    if (chkGraduationLevel)
                    {
                        sheet.SetCellValue(startRow, P_GraduationLevel, item.GraduationLevelName);
                    }
                    if (chkQualificationLevel)
                    {
                        sheet.SetCellValue(startRow, P_QualificationLevel, item.QualificationResolution);
                    }

                    if (chkForeignLanguageGrade)
                    {
                        sheet.SetCellValue(startRow, P_ForeignLanguageGrade, item.ForeignResolution);
                    }

                    if (chkITQualificationLevel)
                    {
                        sheet.SetCellValue(startRow, P_ITQualificationLevel, item.ITResolution);
                    }

                    if (chkPoliticalGrade)
                    {
                        sheet.SetCellValue(startRow, P_PoliticalGrade, item.PoliticalGradeResolution);
                    }

                    if (chkSpecialityCat)
                    {
                        sheet.SetCellValue(startRow, P_SpecialityCat, item.SpecialityCatResolution);
                    }

                    if (chkContractType)
                    {
                        sheet.SetCellValue(startRow, P_ContractType, item.ContractTypeResolution);
                    }
                    if (chkWorkType)
                    {
                        sheet.SetCellValue(startRow, P_WorkType, item.WorkTypeResolution);
                    }

                    if (chkSchoolFaculty)
                    {
                        sheet.SetCellValue(startRow, P_SchoolFaculty, item.SchoolFacultyName);
                    }
                    if (chkSchool)
                    {
                        sheet.SetCellValue(startRow, P_School, item.SchoolName);
                    }

                    //Hết 1 bản ghi, tăng lên dòng tiếp theo
                    startRow++;
                    startSTT++;
                }

            }

            firstSheet.Delete();
            sheet.Name = "Danh sach can bo";
            #endregion
            return oBook.ToStream();
        }

        public void copyCol(IVTWorksheet sheetOld, IVTWorksheet sheet, int col, int newPostioncol)
        {

            IVTRange range = sheetOld.GetRange(rowHeaderStart, col, rowHeaderEnd, col);

            sheet.CopyPasteSameRowHeigh(range, rowHeaderStart, newPostioncol);

            startCol++;
        }
        public bool CheckSelected(String key, IDictionary<string, object> dic)
        {
            if (!dic.ContainsKey(key))
            {
                return false;
            }
            else
            {
                if (dic[key].ToString().Equals("on") || dic[key].ToString().Equals("") || dic[key].ToString().Equals("True"))
                {
                    return true;
                }

            }
            return false;
        }
        public object convert(IDictionary<string, object> dic, string key)
        {
            if (!dic.ContainsKey(key))
            {
                return null;
            }
            if (dic[key] == null || dic[key] == "")
                return null;
            else
                return dic[key];
        }
        #endregion
        #region Xuất excel cho học sinh
        public Stream ExportLookupInfoPupil(Dictionary<String, object> dic, out string FileName)
        {
            int HierachyLevel = Utils.GetInt(dic, "HierachyLevel");
            if (dic["form1[ProvinceID]"] == "")
            {
                dic["form1[ProvinceID]"] = "0";
            }
            if (dic["form1[EducationGrade]"] == "")
            {
                dic["form1[EducationGrade]"] = "0";
            }
            if (dic["form1[District]"] == "")
            {
                dic["form1[District]"] = "0";
            }
            if (dic["form1[SupervisingDeptID1]"] == "")
            {
                dic["form1[SupervisingDeptID1]"] = "0";
            }
            if (dic["form1[School]"] == "")
            {
                dic["form1[School]"] = "0";
            }
            if (dic["form1[Sex]"] == "")
            {
                dic["form1[Sex]"] = "-1";
            }
            if (dic["form1[Status]"] == "")
            {
                dic["form1[Status]"] = "0";
            }
            if (dic["form1[AcademicYear]"] == "")
            {
                dic["form1[AcademicYear]"] = "0";
            }
            if (dic["form1[EducationLevel]"] == "")
            {
                dic["form1[EducationLevel]"] = "0";
            }
            if (dic["form1[Class]"] == "")
            {
                dic["form1[Class]"] = "0";
            }
            int ProvinceID = Utils.GetInt(dic, "form1[ProvinceID]");
            int AppliedLevel = Utils.GetInt(dic, "form1[EducationGrade]");
            int DistrictID = Utils.GetInt(dic, "form1[District]");
            int SupervisingDeptID = Utils.GetInt(dic, "form1[SupervisingDeptID1]");
            int SchoolID = Utils.GetInt(dic, "form1[School]");
            string PupilCode = Utils.GetString(dic, "form1[PupilCode]");
            string FullName = Utils.GetString(dic, "form1[FullName]");
            int Genre = Utils.GetInt(dic, "form1[Sex]");
            int Status = Utils.GetInt(dic, "form1[Status]");
            int Year = Utils.GetInt(dic, "form1[AcademicYear]");
            int EducationLevelID = Utils.GetInt(dic, "form1[EducationLevel]");
            int ClassID = Utils.GetInt(dic, "form1[Class]");
            bool isSub = Utils.GetBool(dic, "IsSub");

            string reportCode = "";
            reportCode = SystemParamsInFile.REPORT_DANHSACHHS;
            ReportDefinition rp = ReportDefinitionBusiness.GetByCode(reportCode);
            string templatePath = SystemParamsInFile.TEMPLATE_FOLDER + rp.TemplateDirectory + "/" + rp.TemplateName;
            IVTWorkbook oBook = VTExport.OpenWorkbook(templatePath);
            IVTWorksheet firstSheet = oBook.GetSheet(1);
            IVTWorksheet sheet = oBook.GetSheet(2);
            IVTRange range = firstSheet.GetRange("A1", "J7");
            sheet.CopyPasteSameSize(range, "A1");
            int SupervisingDeptID1 = Utils.GetInt(convert(dic, "form1[SupervisingDeptID1]"));
            SupervisingDept sp = SupervisingDeptBusiness.Find(SupervisingDeptID1);
            Province province = ProvinceBusiness.Find(ProvinceID);

            sheet.SetCellValue("A2", sp.SupervisingDeptName);
            string date = province.ProvinceName + ", ngày " + DateTime.Now.Day + " tháng " + DateTime.Now.Month + " năm " + DateTime.Now.Year;
            sheet.SetCellValue("F4", date);
            //File Name
            string outputNamePattern = rp.OutputNamePattern;
            if (HierachyLevel == SystemParamsInFile.EDUCATION_HIERACHY_LEVEL_PROVINCE_OFFICE)
            {
                outputNamePattern = outputNamePattern.Replace("[LevelAccount]", ReportUtils.StripVNSign("SGD"));
            }
            if (HierachyLevel == SystemParamsInFile.EDUCATION_HIERACHY_LEVEL_DISTRICT_OFFICE)
            {
                outputNamePattern = outputNamePattern.Replace("[LevelAccount]", ReportUtils.StripVNSign("PGD"));
            }
            FileName = ReportUtils.RemoveSpecialCharacters(outputNamePattern) + "." + rp.OutputFormat;

            /////////////////
            #region Kiểm tra các trường được check
            bool chkPFullName = CheckSelected("form1[chkPFullName]", dic);
            bool chkPPupilCode = CheckSelected("form1[chkPPupilCode]", dic);
            bool chkPClass = CheckSelected("form1[chkPClass]", dic);
            bool chkPEducationLevel = CheckSelected("form1[chkPEducationLevel]", dic);


            bool chkPBirthDate = CheckSelected("form1[chkPBirthDate]", dic);
            bool chkPBirthPlace = CheckSelected("form1[chkPBirthPlace]", dic);

            bool chkPSex = CheckSelected("form1[chkPSex]", dic);
            bool chkPStatus = CheckSelected("form1[chkPStatus]", dic);
            bool chkPEthnic = CheckSelected("form1[chkPEthnic]", dic);
            bool chkPReligion = CheckSelected("form1[chkPReligion]", dic);
            bool chkPHomeTown = CheckSelected("form1[chkPHomeTown]", dic);
            bool chkPPermanentResidentalAddress = CheckSelected("form1[chkPPermanentResidentalAddress]", dic);
            bool chkPFamilyType = CheckSelected("form1[chkPFamilyType]", dic);
            bool chkPPolicyTarget = CheckSelected("form1[chkPPolicyTarget]", dic);
            bool chkPFatherFullName = CheckSelected("form1[chkPFatherFullName]", dic);
            bool chkPFatherBirthDate = CheckSelected("form1[chkPFatherBirthDate]", dic);
            bool chkPFatherMobile = CheckSelected("form1[chkPFatherMobile]", dic);
            bool chkPMotherFullName = CheckSelected("form1[chkPMotherFullName]", dic);
            bool chkPMotherBirthDate = CheckSelected("form1[chkPMotherBirthDate]", dic);
            bool chkPMotherMobile = CheckSelected("form1[chkPMotherMobile]", dic);
            bool chkPSchool = CheckSelected("form1[chkPSchool]", dic);
            bool chkPProvince = CheckSelected("form1[chkPProvince]", dic);
            bool chkPDistrict = CheckSelected("form1[chkPDistrict]", dic);
            bool chkPCommune = CheckSelected("form1[chkPCommune]", dic);
            bool chkPFatherJob = CheckSelected("form1[chkPFatherJob]", dic);
            bool chkPMotherJob = CheckSelected("form1[chkPMotherJob]", dic);
            bool chkPSponsorFullName = CheckSelected("form1[chkPSponsorFullName]", dic);
            bool chkPSponsorBirthDate = CheckSelected("form1[chkPSponsorBirthDate]", dic);
            bool chkPSponsorJob = CheckSelected("form1[chkPSponsorJob]", dic);
            bool chkPSponsorMobile = CheckSelected("form1[chkPSponsorMobile]", dic);



            bool chkSTT = true;
            #endregion
            #region Copy Header của file Excel
            int P_PFullName = 0, P_PPupilCode = 0, P_PClass = 0, P_PBirthDate = 0, P_PSex = 0, P_PStatus = 0, P_PEthnic = 0, P_PReligion = 0, P_PHomeTown = 0,
                P_PPermanentResidentalAddress = 0, P_PFamilyType = 0, P_PPolicyTarget = 0, P_PFatherFullName = 0, P_PFatherBirthDate = 0, P_PFatherMobile = 0, P_PMotherFullName = 0,
                P_PMotherBirthDate = 0, P_PMotherMobile = 0, P_PSchool = 0, P_PProvince = 0,
                P_PEducationLevel = 0, P_PBirthPlace = 0, P_PDistrict = 0, P_PCommune = 0, P_PFatherJob = 0, P_PMotherJob = 0, P_PSponsorFullName = 0, P_PSponsorBirthDate = 0, P_PSponsorJob = 0, P_PSponsorMobile = 0;
            if (chkSTT)
            {
                copyCol(firstSheet, sheet, 1, 1);
            }
            if (chkPFullName)
            {
                P_PFullName = startCol;
                copyCol(firstSheet, sheet, 2, startCol);
            }
            if (chkPPupilCode)
            {
                P_PPupilCode = startCol;
                copyCol(firstSheet, sheet, 3, startCol);
            }

            if (chkPBirthDate)
            {
                P_PBirthDate = startCol;
                copyCol(firstSheet, sheet, 4, startCol);
            }
            if (chkPSex)
            {
                P_PSex = startCol;
                copyCol(firstSheet, sheet, 5, startCol);
            }

            if (chkPEducationLevel)
            {
                P_PEducationLevel = startCol;
                copyCol(firstSheet, sheet, 6, startCol);
            }
            if (chkPClass)
            {
                P_PClass = startCol;
                copyCol(firstSheet, sheet, 7, startCol);
            }
            if (chkPStatus)
            {
                P_PStatus = startCol;
                copyCol(firstSheet, sheet, 8, startCol);
            }
            if (chkPEthnic)
            {
                P_PEthnic = startCol;
                copyCol(firstSheet, sheet, 9, startCol);
            }
            if (chkPReligion)
            {
                P_PReligion = startCol;
                copyCol(firstSheet, sheet, 10, startCol);
            }

            if (chkPBirthPlace)
            {
                P_PBirthPlace = startCol;
                copyCol(firstSheet, sheet, 11, startCol);
            }


            if (chkPHomeTown)
            {
                P_PHomeTown = startCol;
                copyCol(firstSheet, sheet, 12, startCol);
            }
            if (chkPPermanentResidentalAddress)
            {
                P_PPermanentResidentalAddress = startCol;
                copyCol(firstSheet, sheet, 13, startCol);
            }
            if (chkPProvince)
            {
                P_PProvince = startCol;
                copyCol(firstSheet, sheet, 14, startCol);
            }
            if (chkPDistrict)
            {
                P_PDistrict = startCol;
                copyCol(firstSheet, sheet, 15, startCol);
            }
            if (chkPCommune)
            {
                P_PCommune = startCol;
                copyCol(firstSheet, sheet, 16, startCol);
            }

            if (chkPFamilyType)
            {
                P_PFamilyType = startCol;
                copyCol(firstSheet, sheet, 17, startCol);
            }
            if (chkPPolicyTarget)
            {
                P_PPolicyTarget = startCol;
                copyCol(firstSheet, sheet, 18, startCol);
            }
            if (chkPFatherFullName)
            {
                P_PFatherFullName = startCol;
                copyCol(firstSheet, sheet, 19, startCol);
            }
            if (chkPFatherBirthDate)
            {
                P_PFatherBirthDate = startCol;
                copyCol(firstSheet, sheet, 20, startCol);
            }

            if (chkPFatherJob)
            {
                P_PFatherJob = startCol;
                copyCol(firstSheet, sheet, 21, startCol);
            }
            if (chkPFatherMobile)
            {
                P_PFatherMobile = startCol;
                copyCol(firstSheet, sheet, 22, startCol);
            }
            if (chkPMotherFullName)
            {
                P_PMotherFullName = startCol;
                copyCol(firstSheet, sheet, 23, startCol);
            }
            if (chkPMotherBirthDate)
            {
                P_PMotherBirthDate = startCol;
                copyCol(firstSheet, sheet, 24, startCol);
            }
            if (chkPMotherJob)
            {
                P_PMotherJob = startCol;
                copyCol(firstSheet, sheet, 25, startCol);
            }
            if (chkPMotherMobile)
            {
                P_PMotherMobile = startCol;
                copyCol(firstSheet, sheet, 26, startCol);
            }

            if (chkPSponsorFullName)
            {
                P_PSponsorFullName = startCol;
                copyCol(firstSheet, sheet, 27, startCol);
            }
            if (chkPSponsorBirthDate)
            {
                P_PSponsorBirthDate = startCol;
                copyCol(firstSheet, sheet, 28, startCol);
            }
            if (chkPSponsorJob)
            {
                P_PSponsorJob = startCol;
                copyCol(firstSheet, sheet, 29, startCol);
            }
            if (chkPSponsorMobile)
            {
                P_PSponsorMobile = startCol;
                copyCol(firstSheet, sheet, 30, startCol);
            }

            if (chkPSchool)
            {
                P_PSchool = startCol;
                copyCol(firstSheet, sheet, 31, startCol);
            }

            IVTRange temRange = sheet.GetRange("A9", "AE9");
            #endregion
            #region fill dữ liệu lên sheet Excel
            Dictionary<string, object> search = new Dictionary<string, object>();
            search["ProvinceID"] = ProvinceID;
            search["AppliedLevel"] = AppliedLevel;
            search["DistrictID"] = DistrictID;
            search["SupervisingDeptID"] = SupervisingDeptID;
            search["SchoolID"] = SchoolID;
            search["PupilCode"] = PupilCode;
            search["FullName"] = FullName;
            search["Genre"] = Genre;
            search["Status"] = Status;
            search["Year"] = Year;
            search["EducationLevelID"] = EducationLevelID;
            search["ClassID"] = ClassID;
            search["HierachyLevel"] = HierachyLevel;
            search["IsSub"] = isSub;
            IQueryable<PupilProfileBO> lstPupilProfileBO = PupilProfileBusiness.SearchPupilBySupervisingDept((Dictionary<string, object>)search);
            List<PupilProfileBO> lstResult = lstPupilProfileBO.ToList();
            lstResult = lstResult.OrderBy(o => o.SchoolName).ThenBy(o => o.EducationLevelID).ThenBy(o => o.ClassOrderNumber).ThenBy(o => o.ClassName).ThenBy(o => o.OrderInClass).ThenBy(o => o.Name).ThenBy(o => o.FullName).ToList();
            int startRow = 9;
            int startSTT = 1;
            foreach (var item in lstResult)
            {
                sheet.CopyPasteSameSize(temRange, startRow, 1);
                bool chkItem = true;//CheckSelected(nameCheckbox, dic);
                if (chkItem)
                {
                    if (chkSTT)
                    {
                        sheet.SetCellValue(startRow, 1, startSTT);
                    }
                    if (chkPFullName)
                    {
                        sheet.SetCellValue(startRow, P_PFullName, item.FullName);
                    }
                    if (chkPPupilCode)
                    {
                        sheet.SetCellValue(startRow, P_PPupilCode, item.PupilCode);
                    }

                    if (chkPClass)
                    {
                        sheet.SetCellValue(startRow, P_PClass, item.ClassName);
                    }
                    string BirthDate = item.BirthDate != null ? item.BirthDate.ToShortDateString() : "";
                    if (chkPBirthDate)
                    {
                        sheet.SetCellValue(startRow, P_PBirthDate, BirthDate);
                    }
                    int Sex = item.Genre;
                    string GenreName = "";
                    if (Sex == 1)
                    {
                        GenreName = SystemParamsInFile.REPORT_LOOKUPINFO_MALE;
                    }
                    else if (Sex == 0)
                    {
                        GenreName = SystemParamsInFile.REPORT_LOOKUPINFO_FEMALE;
                    }
                    if (chkPSex)
                    {
                        sheet.SetCellValue(startRow, P_PSex, GenreName);
                    }
                    string StatusName = "";
                    switch (item.ProfileStatus)
                    {
                        case SystemParamsInFile.PUPIL_STATUS_STUDYING:
                            StatusName = SystemParamsInFile.REPORT_LOOKUPINFO_STUDYING;
                            break;

                        case SystemParamsInFile.PUPIL_STATUS_GRADUATED:
                            StatusName = SystemParamsInFile.REPORT_LOOKUPINFO_GRADUATED;
                            break;

                        case SystemParamsInFile.PUPIL_STATUS_MOVED_TO_OTHER_SCHOOL:
                            StatusName = SystemParamsInFile.REPORT_LOOKUPINFO_MOVED_TO_OTHER_SCHOOL;
                            break;

                        case SystemParamsInFile.PUPIL_STATUS_LEAVED_OFF:
                            StatusName = SystemParamsInFile.REPORT_LOOKUPINFO_LEAVED_OFF;
                            break;

                        case SystemParamsInFile.PUPIL_STATUS_MOVED_TO_OTHER_CLASS:
                            StatusName = SystemParamsInFile.REPORT_LOOKUPINFO_MOVED_TO_OTHER_CLASS;
                            break;
                    }
                    if (chkPStatus)
                    {
                        sheet.SetCellValue(startRow, P_PStatus, StatusName);
                    }
                    if (chkPEthnic)
                    {
                        sheet.SetCellValue(startRow, P_PEthnic, item.EthnicName);
                    }
                    if (chkPReligion)
                    {
                        sheet.SetCellValue(startRow, P_PReligion, item.ReligionName);
                    }

                    if (chkPHomeTown)
                    {
                        sheet.SetCellValue(startRow, P_PHomeTown, item.HomeTown);
                    }

                    if (chkPPermanentResidentalAddress)
                    {
                        sheet.SetCellValue(startRow, P_PPermanentResidentalAddress, item.PermanentResidentalAddress);
                    }

                    if (chkPFamilyType)
                    {
                        sheet.SetCellValue(startRow, P_PFamilyType, item.FamilyName);
                    }

                    if (chkPPolicyTarget)
                    {
                        sheet.SetCellValue(startRow, P_PPolicyTarget, item.PolicyTargetName);
                    }

                    if (chkPFatherFullName)
                    {
                        sheet.SetCellValue(startRow, P_PFatherFullName, item.FatherFullName);
                    }
                    if (chkPFatherBirthDate && item.FatherBirthDate != null)
                    {
                        sheet.SetCellValue(startRow, P_PFatherBirthDate, item.FatherBirthDate.Value.Year.ToString());
                    }

                    if (chkPFatherMobile)
                    {
                        sheet.SetCellValue(startRow, P_PFatherMobile, item.FatherMobile);
                    }

                    if (chkPMotherFullName)
                    {
                        sheet.SetCellValue(startRow, P_PMotherFullName, item.MotherFullName);
                    }

                    if (chkPMotherBirthDate && item.MotherBirthDate != null)
                    {
                        sheet.SetCellValue(startRow, P_PMotherBirthDate, item.MotherBirthDate.Value.Year.ToString());
                    }

                    if (chkPMotherMobile)
                    {
                        sheet.SetCellValue(startRow, P_PMotherMobile, item.MotherMobile);
                    }

                    if (chkPSchool)
                    {
                        sheet.SetCellValue(startRow, P_PSchool, item.SchoolName);
                    }

                    if (chkPProvince)
                    {
                        sheet.SetCellValue(startRow, P_PProvince, item.ProvinceName);
                    }

                    if (chkPEducationLevel)
                    {
                        sheet.SetCellValue(startRow, P_PEducationLevel, item.EducationLevel);
                    }
                    if (chkPBirthPlace)
                    {
                        sheet.SetCellValue(startRow, P_PBirthPlace, item.BirthPlace);
                    }
                    if (chkPDistrict)
                    {
                        sheet.SetCellValue(startRow, P_PDistrict, item.DistrictName);
                    }
                    if (chkPCommune)
                    {
                        sheet.SetCellValue(startRow, P_PCommune, item.CommuneName);
                    }
                    if (chkPFatherJob)
                    {
                        sheet.SetCellValue(startRow, P_PFatherJob, item.FatherJob);
                    }
                    if (chkPMotherJob)
                    {
                        sheet.SetCellValue(startRow, P_PMotherJob, item.MotherJob);
                    }
                    if (chkPSponsorFullName)
                    {
                        sheet.SetCellValue(startRow, P_PSponsorFullName, item.SponsorFullName);
                    }
                    if (chkPSponsorBirthDate && item.SponsorBirthDate != null)
                    {
                        sheet.SetCellValue(startRow, P_PSponsorBirthDate, item.SponsorBirthDate.Value.Year.ToString());
                    }
                    if (chkPSponsorJob)
                    {
                        sheet.SetCellValue(startRow, P_PSponsorJob, item.SponsorJob);
                    }
                    if (chkPSponsorMobile)
                    {
                        sheet.SetCellValue(startRow, P_PSponsorMobile, item.SponsorMobile);
                    }

                    //Hết 1 bản ghi, tăng lên dòng tiếp theo
                    startRow++;
                    startSTT++;
                }

            }
            #endregion
            //FileName = "";
            firstSheet.Delete();
            sheet.Name = "Danh sach HS";
            return oBook.ToStream();
        }
        #endregion

        #region Xuất excel cho học sinh
        public Stream ExportLookupInfoPupilBySuper(Dictionary<String, object> dic, out string FileName)
        {
            int HierachyLevel = Utils.GetInt(dic, "HierachyLevel");

            if (dic["frm[EducationGrade]"] == null)
            {
                dic["frm[EducationGrade]"] = "0";
            }
            if (dic["frm[InfoType]"] == null)
            {
                dic["frm[InfoType]"] = "0";
            }
            if (dic["frm[AcademicYear]"] == null)
            {
                dic["frm[AcademicYear]"] = "0";
            }
            if (dic["frm[District]"] == null)
            {
                dic["frm[District]"] = "0";
            }
            if (dic["frm[School]"] == null)
            {
                dic["frm[School]"] = "0";
            }
            if (dic["frm[EducationLevel]"] == null)
            {
                dic["frm[EducationLevel]"] = "0";
            }
            if (dic["frm[Class]"] == null)
            {
                dic["frm[Class]"] = "0";
            }
            if (dic["frm[Status]"] == null)
            {
                dic["frm[Status]"] = "0";
            }
            if (dic["frm[Sex]"] == null)
            {
                dic["frm[Sex]"] = "-1";
            }
            if (dic["frm[ProvinceID]"] == null)
            {
                dic["frm[ProvinceID]"] = "0";
            }
            if (dic["frm[SupervisingDeptID1]"] == "")
            {
                dic["frm[SupervisingDeptID1]"] = "0";
            }
            if (dic["frm[ValTemplateId]"] == "")
            {
                dic["frm[ValTemplateId]"] = "0";
            }
            if (dic["frm[ValStartRow]"] == "")
            {
                dic["frm[ValStartRow]"] = "1";
            }

            int AppliedLevel = Utils.GetInt(dic, "frm[EducationGrade]");
            int InfoType = Utils.GetInt(dic, "frm[InfoType]");
            int SemesterID = 0;
            if (InfoType == 2 && AppliedLevel <= 3)
            {

                SemesterID = Utils.GetInt(dic, "frm[SemesterID]", 0);
            }

            int TypeMarkID = 1;
            if (InfoType == 2 && AppliedLevel <= SystemParamsInFile.REPORT_EDUCATION_GRADE_TERTIARY)
            {

                TypeMarkID = Utils.GetInt(dic, "frm[TypeMarkID]", 1);
            }
            int Year = Utils.GetInt(dic, "frm[AcademicYear]");
            int DistrictID = Utils.GetInt(dic, "frm[District]");
            int SchoolID = Utils.GetInt(dic, "frm[School]");
            int EducationLevelID = Utils.GetInt(dic, "frm[EducationLevel]");
            int ClassID = Utils.GetInt(dic, "frm[Class]");
            int Status = Utils.GetInt(dic, "frm[Status]");
            string PupilCode = Utils.GetString(dic, "frm[PupilCode]");
            string FullName = Utils.GetString(dic, "frm[FullName]");
            int Genre = Utils.GetInt(dic, "frm[Sex]", -1);
            int ProvinceID = Utils.GetInt(dic, "frm[ProvinceID]");
            int SupervisingDeptID = Utils.GetInt(dic, "frm[SupervisingDeptID1]");
            int TemplateID = Utils.GetInt(dic, "frm[ValTemplateId]");
            int startRow = Utils.GetInt(dic, "frm[ValStartRow]");
            if (startRow <= 0)
                startRow = 1;
            bool isSub = Utils.GetBool(dic, "IsSub");

            string reportCode = "";
            reportCode = SystemParamsInFile.REPORT_DANHSACHHS;
            ReportDefinition rp = ReportDefinitionBusiness.GetByCode(reportCode);
            string templatePath = SystemParamsInFile.TEMPLATE_FOLDER + rp.TemplateDirectory + "/DanhSachHS.xlsx";// +rp.TemplateName;
            IXVTWorkbook oBook = XVTExportExcel.OpenWorkbook(templatePath);
            IXVTWorksheet sheetFill = oBook.GetSheet(1);
            //IVTWorkbook oBook = VTExport.OpenWorkbook(templatePath);
            //IVTWorksheet sheetFill = oBook.GetSheet(1);

            //File Name
            string outputNamePattern = rp.OutputNamePattern;
            if (HierachyLevel == SystemParamsInFile.EDUCATION_HIERACHY_LEVEL_PROVINCE_OFFICE)
            {
                outputNamePattern = outputNamePattern.Replace("[LevelAccount]", ReportUtils.StripVNSign("SGD"));
            }
            if (HierachyLevel == SystemParamsInFile.EDUCATION_HIERACHY_LEVEL_DISTRICT_OFFICE)
            {
                outputNamePattern = outputNamePattern.Replace("[LevelAccount]", ReportUtils.StripVNSign("PGD"));
            }
            FileName = ReportUtils.RemoveSpecialCharacters(outputNamePattern) + "." + rp.OutputFormat;

            /////////////////      
            //bool chkPFullName = CheckSelected("form1[chkPFullName]", dic);

            #region // Get data
            ProfileTemplate profileTemplate = ProfileTemplateBusiness.Find(TemplateID);
            if (profileTemplate == null)
            {
                return null;
            }
            //startRow += 1;

            List<EmployeeColumnTemplateBO> lstTemplate = new List<EmployeeColumnTemplateBO>();
            if (profileTemplate != null && !string.IsNullOrEmpty(profileTemplate.ColDescriptionIds))
            {
                lstTemplate = Newtonsoft.Json.JsonConvert.DeserializeObject<List<EmployeeColumnTemplateBO>>(profileTemplate.ColDescriptionIds);
            }

            if (lstTemplate.Count() == 0)
            {
                sheetFill.SheetName = "Danh sach HS";
                return oBook.ToStream();
            }

            List<int> lstReportType = new List<int>();
            List<ColumnDescription> lstColumnDescriptionTotal = new List<ColumnDescription>();
            // học sinh/trẻ và loại thông tin hồ sơ
            if ((AppliedLevel <= SystemParamsInFile.REPORT_EDUCATION_GRADE_TERTIARY && InfoType == 1)
                || (AppliedLevel > SystemParamsInFile.REPORT_EDUCATION_GRADE_TERTIARY && InfoType == 1))
            {
                #region
                if (AppliedLevel <= SystemParamsInFile.REPORT_EDUCATION_GRADE_TERTIARY)
                {
                    lstReportType.Add(GlobalConstants.REPORT_TYPE_OF_GENERAL_5);
                    lstReportType.Add(GlobalConstants.REPORT_TYPE_OF_GENERAL_6);
                    lstReportType.Add(GlobalConstants.REPORT_TYPE_OF_GENERAL_7);
                }
                else
                {
                    lstReportType.Add(GlobalConstants.REPORT_TYPE_OF_GENERAL_8);
                    lstReportType.Add(GlobalConstants.REPORT_TYPE_OF_GENERAL_9);
                    lstReportType.Add(GlobalConstants.REPORT_TYPE_OF_GENERAL_10);
                    lstReportType.Add(GlobalConstants.REPORT_TYPE_OF_GENERAL_17);
                }
                #endregion
            } // Học sinh và loại thông tin môn học, năng lực phẩm chất, kết quả học tập, rèn luyện
            else if (AppliedLevel <= SystemParamsInFile.REPORT_EDUCATION_GRADE_TERTIARY && InfoType == 2)
            {
                #region
                lstReportType.Add(GlobalConstants.REPORT_TYPE_OF_GENERAL_5);
                // Cấp 1

                switch (AppliedLevel)
                {
                    case SystemParamsInFile.REPORT_EDUCATION_GRADE_PRIMARY:
                        lstReportType.Add(GlobalConstants.REPORT_TYPE_OF_GENERAL_11);
                        lstReportType.Add(GlobalConstants.REPORT_TYPE_OF_GENERAL_15);
                        break;
                    case SystemParamsInFile.REPORT_EDUCATION_GRADE_SECONDARY:
                        lstReportType.Add(GlobalConstants.REPORT_TYPE_OF_GENERAL_12);
                        lstReportType.Add(GlobalConstants.REPORT_TYPE_OF_GENERAL_14);
                        break;
                    case SystemParamsInFile.REPORT_EDUCATION_GRADE_TERTIARY:
                        lstReportType.Add(GlobalConstants.REPORT_TYPE_OF_GENERAL_13);
                        lstReportType.Add(GlobalConstants.REPORT_TYPE_OF_GENERAL_14);
                        break;

                }
                #endregion
            }
            // Trẻ và loại thông tin kết quả sức khỏe
            else if (AppliedLevel > SystemParamsInFile.REPORT_EDUCATION_GRADE_TERTIARY && InfoType == 2)
            {
                lstReportType.Add(GlobalConstants.REPORT_TYPE_OF_GENERAL_8);
                lstReportType.Add(GlobalConstants.REPORT_TYPE_OF_GENERAL_16);
            }

            IDictionary<string, object> dicCol = new Dictionary<string, object>();
            dicCol["listReportType"] = lstReportType;
            lstColumnDescriptionTotal = ColumnDescriptionBusiness.Search(dicCol).ToList();

            if (AppliedLevel == SystemParamsInFile.REPORT_EDUCATION_GRADE_PRIMARY && InfoType == 2 && SemesterID != 4)
            {
                lstColumnDescriptionTotal = lstColumnDescriptionTotal.Where(x => (x.ColumnName != "UP_CLASS" && x.ColumnName != "ENDING_EVALUATION")).ToList();
            }

            List<ColumnDescriptionBO> lstColumn = (from temp in lstTemplate
                                                   join cd in lstColumnDescriptionTotal on temp.ColDesId equals cd.ColumnDescriptionId
                                                   select new ColumnDescriptionBO
                                                   {
                                                       CellName = temp.ColExcel,
                                                       ColumnDescriptionId = cd.ColumnDescriptionId,
                                                       ColumnIndex = new VTVector(temp.ColExcel + "1").Y,
                                                       ColumnName = cd.ColumnName,
                                                       ColumnSize = cd.ColumnSize,
                                                       Resolution = temp.ExcelName,
                                                       ColumnOrder = cd.ColumnOrder,
                                                       ColumnType = cd.ColumnType,
                                                       Description = cd.Description,
                                                       RequiredColumn = cd.RequiredColumn,
                                                       TableName = cd.TableName,
                                                       ReportType = cd.ReportType,
                                                       OrderID = cd.OrderID
                                                   }).ToList();
            lstColumn = lstColumn.OrderBy(x => x.OrderID).OrderBy(c => c.ColumnOrder).ToList();

            IQueryable<AcademicYearBO> iqSchool = from ac in AcademicYearBusiness.AllNoTracking
                                                  join sp in SchoolProfileBusiness.AllNoTracking on ac.SchoolID equals sp.SchoolProfileID
                                                  where sp.ProvinceID == ProvinceID
                                                        && ac.Year == Year
                                                        && sp.IsActive == true
                                                        && ac.IsActive == true
                                                  select new AcademicYearBO
                                                  {
                                                      AcademicYearID = ac.AcademicYearID,
                                                      SchoolID = ac.SchoolID,
                                                      Year = ac.Year,
                                                      School = sp
                                                  };
            if (DistrictID > 0)
            {
                iqSchool = iqSchool.Where(sp => sp.School.DistrictID == DistrictID);
            }

            if (SchoolID > 0)
            {
                iqSchool = iqSchool.Where(x => x.SchoolID == SchoolID);
            }
            List<int> lstEducationGrade = UtilsBusiness.GetEducationGrade(AppliedLevel);
            iqSchool = iqSchool.Where(sp => lstEducationGrade.Contains(sp.School.EducationGrade));
            List<AcademicYearBO> lstSchool = iqSchool.ToList();

            #region // Lấy thông tin từ store

            // thông tin chung của học sinh, trẻ
            DataTable dtPupilProfile = new DataTable();
            // Thông tin môn học cấp 2,3 (hoặc môn học và HĐGD cấp 1)
            DataTable dtMarkSubjectInfo = new DataTable();
            DataTable dtPupilJudge = new DataTable();
            DataTable dtPupilMarkVNEN = new DataTable();
            // Thông tin năng lực phẩm chất cấp 1 (kết quả cuối năm cấp 1)
            DataTable dtCapQuaInfo = new DataTable();
            // Thông tin kết quả học tập - rèn luyện 
            DataTable dtLearnResultInfo = new DataTable();
            // Thông tin kết quả sức khỏe cấp MN
            DataTable dtHealthResultInfo = new DataTable();
            #endregion

            int SchoolIDtmp = 0;
            int AcademicYearIDtmp = 0;
            int numTask = 10;
            Task[] arrTask = new Task[numTask];
            for (int z = 0; z < numTask; z++)
            {
                int currentIndex = z;
                arrTask[z] = Task.Factory.StartNew(() =>
                {
                    List<AcademicYearBO> lstSchoolRunTask = lstSchool.Where(p => p.SchoolID % numTask == currentIndex).ToList();
                    if (lstSchoolRunTask != null && lstSchoolRunTask.Count > 0)
                    {
                        for (int s = lstSchoolRunTask.Count - 1; s >= 0; s--)
                        {
                            #region // Lấy thông tin từ store
                            SchoolIDtmp = lstSchoolRunTask[s].SchoolID;
                            AcademicYearIDtmp = lstSchoolRunTask[s].AcademicYearID;

                            #region // thông tin chung của học sinh, trẻ
                            DataTable dtPupilProfileStore = GetGeneralPupilProfile(SchoolIDtmp, AcademicYearIDtmp, Year, AppliedLevel, ProvinceID, DistrictID, Status,
                                PupilCode, FullName, Genre, EducationLevelID, ClassID);

                            if (dtPupilProfileStore.Rows.Count > 0)
                            {
                                try
                                {
                                    dtPupilProfile.Merge(dtPupilProfileStore);
                                }
                                catch (Exception ex)
                                {
                                    logger.Error("dtPupilProfile " + ex.Message, ex);
                                }

                            }

                            /*if (dtPupilProfileStore.Columns.Count > 0 && dtPupilProfile.Columns.Count == 0 && dtPupilProfile.Rows.Count == 0)
                            {
                                DataColumn[] columnNames = dtPupilProfileStore.Columns.Cast<DataColumn>().ToArray();
                                foreach (var item in columnNames)
                                {
                                    if (!dtPupilProfile.Columns.Equals(item.ColumnName))
                                    {
                                        dtPupilProfile.Columns.Add(item.ColumnName);
                                    }
                                }
                            }

                            for (int j = 0; j < dtPupilProfileStore.Rows.Count; j++)
                            {
                                dtPupilProfile.ImportRow(dtPupilProfileStore.Rows[j]);
                            }*/

                            #endregion

                            // Thông tin năng lực phẩm chất cấp 1 (kết quả cuối năm cấp 1)
                            DataTable dtCapQuaInfoStore = new DataTable();
                            // Thông tin kết quả học tập - rèn luyện 
                            DataTable dtLearnResultInfoStore = new DataTable();
                            // Thông tin kết quả sức khỏe cấp MN
                            DataTable dtHealthResultInfoStore = new DataTable();

                            // Học sinh và loại thông tin môn học, năng lực phẩm chất, kết quả học tập, rèn luyện
                            if (AppliedLevel <= SystemParamsInFile.REPORT_EDUCATION_GRADE_TERTIARY && InfoType == 2)
                            {
                                // Thông tin môn học cấp 2,3 (hoặc môn học và HĐGD cấp 1)
                                DataTable dtMarkSubjectInfoStore = new DataTable();

                                #region


                                switch (AppliedLevel)
                                {
                                    case SystemParamsInFile.REPORT_EDUCATION_GRADE_PRIMARY:
                                        #region // get data
                                        // Môn học và HĐGD 
                                        int temptSemester = SemesterID <= 2 ? 1 : 2;
                                        dtMarkSubjectInfoStore = GetInfoResultOfPupilPrimary(1, SchoolIDtmp, AcademicYearIDtmp, Year, AppliedLevel,
                                            ProvinceID, DistrictID, Status, PupilCode, FullName, Genre, EducationLevelID, ClassID, temptSemester, SemesterID);
                                        if (dtMarkSubjectInfoStore.Rows.Count > 0)
                                        {
                                            try
                                            {
                                                dtMarkSubjectInfo.Merge(dtMarkSubjectInfoStore);
                                            }
                                            catch (Exception ex)
                                            {
                                                logger.Error("dtMarkSubjectInfo " + ex.Message, ex);
                                            }

                                        }

                                        // Năng lực, phẩm chất, kết quả cuối kỳ 
                                        dtCapQuaInfoStore = GetInfoResultOfPupilPrimary(2, SchoolIDtmp, AcademicYearIDtmp, Year, AppliedLevel,
                                            ProvinceID, DistrictID, Status, PupilCode, FullName, Genre, EducationLevelID, ClassID, temptSemester, SemesterID);

                                        if (dtCapQuaInfoStore.Rows.Count > 0)
                                        {
                                            try
                                            {
                                                dtCapQuaInfo.Merge(dtCapQuaInfoStore);
                                            }
                                            catch (Exception ex)
                                            {
                                                logger.Error("dtCapQuaInfo " + ex.Message, ex);
                                            }

                                        }
                                        #endregion
                                        break;
                                    case SystemParamsInFile.REPORT_EDUCATION_GRADE_SECONDARY:
                                    case SystemParamsInFile.REPORT_EDUCATION_GRADE_TERTIARY:
                                        #region // Get data
                                        // Môn học
                                        dtMarkSubjectInfoStore = GetMarkPupil23(SchoolIDtmp, AcademicYearIDtmp, Year, AppliedLevel, ProvinceID, DistrictID,
                                            Status, PupilCode, FullName, Genre, EducationLevelID, ClassID, SemesterID, TypeMarkID);

                                        if (dtMarkSubjectInfoStore.Rows.Count > 0)
                                        {
                                            try
                                            {
                                                dtMarkSubjectInfo.Merge(dtMarkSubjectInfoStore);
                                            }
                                            catch (Exception ex)
                                            {

                                                logger.Error("dtMarkSubjectInfo " + ex.Message, ex);
                                            }

                                        }

                                        // Kết quả học tập -rèn luyện
                                        dtLearnResultInfoStore = GetLearnResult23(SchoolIDtmp, AcademicYearIDtmp, Year, AppliedLevel, ProvinceID, DistrictID,
                                            PupilCode, FullName, Genre, EducationLevelID, ClassID, SemesterID, Status);

                                        if (dtLearnResultInfoStore.Rows.Count > 0)
                                        {
                                            try
                                            {
                                                dtLearnResultInfo.Merge(dtLearnResultInfoStore);
                                            }
                                            catch (Exception ex)
                                            {

                                                logger.Error("dtLearnResultInfo " + ex.Message, ex);
                                            }
                                        }

                                        if (TypeMarkID == 2)
                                        {
                                            DataTable dtPupilJudgeStore = GetJudgementPupil(SchoolIDtmp, AcademicYearIDtmp, Year, AppliedLevel, ProvinceID, DistrictID,
                                                Status, PupilCode, FullName, Genre, EducationLevelID, ClassID, (SemesterID == 3 ? 2 : SemesterID));

                                            if (dtPupilJudgeStore.Rows.Count > 0)
                                            {
                                                try
                                                {
                                                    dtPupilJudge.Merge(dtPupilJudgeStore);
                                                }
                                                catch (Exception ex)
                                                {

                                                    logger.Error("dtPupilJudge " + ex.Message, ex);
                                                }
                                                
                                            }
                                        }

                                        DataTable dtPupilMarkVNENStore = GetMarkVNENPupil(SchoolIDtmp, AcademicYearIDtmp, Year, AppliedLevel, ProvinceID, DistrictID,
                                            Status, PupilCode, FullName, Genre, EducationLevelID, ClassID, SemesterID);


                                        if (dtPupilMarkVNENStore.Rows.Count > 0)
                                        {
                                            try
                                            {
                                                dtPupilMarkVNEN.Merge(dtPupilMarkVNENStore);
                                            }
                                            catch (Exception ex)
                                            {

                                                logger.Error("dtPupilMarkVNEN " + ex.Message, ex);
                                            }
                                            
                                        }
                                        #endregion
                                        break;
                                }
                                #endregion
                            }
                            // Trẻ và loại thông tin kết quả sức khỏe
                            else if (AppliedLevel > SystemParamsInFile.REPORT_EDUCATION_GRADE_TERTIARY && InfoType == 2)
                            {
                                dtHealthResultInfoStore = GetHealthOfPupil(SchoolIDtmp, AcademicYearIDtmp, Year, AppliedLevel, ProvinceID, DistrictID, Status, PupilCode, FullName, Genre, EducationLevelID, ClassID);

                                if (dtHealthResultInfoStore.Rows.Count > 0)
                                {
                                    try
                                    {
                                        dtHealthResultInfo.Merge(dtHealthResultInfoStore);
                                    }
                                    catch (Exception ex)
                                    {

                                        logger.Error("dtHealthResultInfo " + ex.Message, ex);
                                    }
                                    
                                }
                            }
                            #endregion
                        }
                    }
                });
            }
            Task.WaitAll(arrTask);
            #endregion

            #region // fill data
            int rawBorder = startRow;
            int row = startRow;
            int col = 1;
            for (int i = 0; i < lstColumn.Count; i++)
            {
                ColumnDescriptionBO column = lstColumn[i];
                sheetFill.SetCellValue((column.CellName + row), column.Resolution);

                sheetFill.SetColumnWidth((column.CellName), Convert.ToDouble(column.ColumnSize));
                sheetFill.GetRange((column.CellName + row)).SetFontStyle(true, false, 12, false, false);
                sheetFill.GetRange((column.CellName + row)).FillColor(System.Drawing.Color.Gray);
                sheetFill.GetRange((column.CellName + row)).SetBorder(XLVTBorderStyleValues.Thin, XVTBorderIndex.OutsideBorder);
                col++;
            }
            int maxColumn = (col - 1);
            row++;

            int stt = 1;
            string val = "";
            string tempValue = "";
            int _startCol = 0;
            for (int j = 0; j < dtPupilProfile.Rows.Count; j++)
            {
                DataRow dr = dtPupilProfile.Rows[j];
                int pupilID = Convert.ToInt32(dr["PUPIL_ID"]);
                int classID = Convert.ToInt32(dr["CLASS_ID"]);
                //int isVNEN = Convert.ToInt32(dr["IS_VNEN_CLASS"]);
                _startCol = 1;
                for (int i = 0; i < lstColumn.Count; i++)
                {
                    ColumnDescriptionBO column = lstColumn[i];

                    if (column.ReportType == GlobalConstants.REPORT_TYPE_OF_GENERAL_11)
                    {
                        #region  // Môn học và hoạt động cấp 1
                        if (dtMarkSubjectInfo.Rows.Count > 0)
                        {
                            if (!string.IsNullOrEmpty(column.Description))
                            {
                                int subjectID = Int32.Parse(column.Description);

                                if (subjectID == 0 && column.ColumnName.Trim().ToUpper().Equals("NGOẠI NGỮ"))
                                {
                                    // môn tiếng anh
                                    subjectID = 20;
                                }

                                string keySearch = string.Format("PUPIL_ID = '{0}' AND CLASS_ID = '{1}' AND SUBJECT_ID = '{2}'", pupilID, classID, subjectID);
                                DataRow objData = dtMarkSubjectInfo.Select(keySearch).FirstOrDefault();

                                if (objData != null)
                                {
                                    tempValue = objData[column.ColumnName].ToString();
                                }
                            }
                        }
                        #endregion
                    }
                    else if (column.ReportType == GlobalConstants.REPORT_TYPE_OF_GENERAL_15)
                    {
                        #region  // Năng lực, phẩm chất, kết quả cuối kỳ cấp 1
                        string keySearch = "";
                        int idColumnDes = Int32.Parse(column.Description);
                        if (dtCapQuaInfo.Rows.Count > 0)
                        {
                            if (idColumnDes >= 1 && idColumnDes <= 7)
                            {

                                keySearch = string.Format("PUPIL_ID = '{0}' AND CLASS_ID = '{1}' AND SUBJECT_ID = '{2}'", pupilID, classID, idColumnDes);
                                DataRow objData = dtCapQuaInfo.Select(keySearch).FirstOrDefault();

                                if (objData != null)
                                {
                                    if (idColumnDes <= 3)
                                    {
                                        tempValue = (objData != null) ? objData["CAPACITY_EVALUATION"].ToString() : "";
                                    }
                                    else
                                    {
                                        tempValue = (objData != null) ? objData["QUALITY_EVALUATION"].ToString() : "";
                                    }
                                }

                            }
                            else
                            {
                                keySearch = string.Format("PUPIL_ID = '{0}' AND CLASS_ID = '{1}'", pupilID, classID);
                                DataRow objData = dtCapQuaInfo.Select(keySearch).FirstOrDefault();

                                if (objData != null)
                                {
                                    tempValue = objData[column.ColumnName].ToString();
                                    if (SemesterID != 4 && (column.ColumnName.Equals("UP_CLASS") || column.ColumnName.Equals("ENDING_EVALUATION")))
                                    {
                                        tempValue = "";
                                    }
                                }
                            }
                        }

                        #endregion
                    }
                    else if (column.ReportType == GlobalConstants.REPORT_TYPE_OF_GENERAL_12
                        || column.ReportType == GlobalConstants.REPORT_TYPE_OF_GENERAL_13)
                    {
                        #region  // Môn học cấp 2,3
                        if (!string.IsNullOrEmpty(column.Description))
                        {
                            int subjectID = Int32.Parse(column.Description);
                            if (AppliedLevel == 3)
                            {
                                // set môn ngoại ngữ => môn tiếng anh
                                if (column.Resolution.Trim().ToUpper().Equals("NGOẠI NGỮ"))
                                {
                                    subjectID = 33;
                                }
                                // set môn ngoại ngữ 2 => môn ngoại ngữ
                                if (column.Resolution.Trim().ToUpper().Equals("NGOẠI NGỮ 2"))
                                {
                                    subjectID = 29;
                                }
                            }

                            string keySearch = string.Format("PUPIL_ID = '{0}' AND CLASS_ID = '{1}' AND SUBJECT_ID = '{2}'", pupilID, classID, subjectID);
                            if (dtMarkSubjectInfo.Rows.Count > 0)
                            {
                                DataRow objData = dtMarkSubjectInfo.Select(keySearch).FirstOrDefault();

                                if (objData != null)
                                {
                                    if (objData["IS_COMMENTING"].ToString().Length > 0)
                                    {
                                        if (objData["IS_COMMENTING"].ToString().ToUpper() == "TRUE"
                                            || objData["IS_COMMENTING"].ToString().ToUpper() == "1")
                                        {
                                            tempValue = (objData != null) ? objData["JUDGEMENT_RESULT"].ToString() : "";
                                        }
                                        else
                                        {
                                            tempValue = (objData != null) ? objData["SUMMED_UP_MARK"].ToString() : "";
                                            if (!string.IsNullOrEmpty(tempValue))
                                            {
                                                double valueFortmat = Double.Parse(tempValue);
                                                tempValue = valueFortmat.ToString("#.##").Replace(",", ".");
                                            }
                                        }
                                    }
                                }
                            }

                            if (dtPupilJudge.Rows.Count > 0)
                            {
                                DataRow drPupil = dtPupilJudge.Select(keySearch).FirstOrDefault();
                                if (drPupil != null)
                                {
                                    tempValue = (drPupil != null) ? drPupil["MARK"].ToString() : "";
                                }
                            }

                            if (dtPupilMarkVNEN.Rows.Count > 0)
                            {
                                DataRow drPupil = dtPupilMarkVNEN.Select(keySearch).FirstOrDefault();
                                if (drPupil != null)
                                {
                                    if (TypeMarkID == 1)
                                    {
                                        tempValue = (drPupil != null) ? drPupil["SUMMED_UP_MARK"].ToString() : "";
                                    }
                                    else
                                    {
                                        tempValue = drPupil != null ? drPupil["MARK"].ToString() : "";
                                    }
                                }
                            }
                        }
                        #endregion
                    }
                    else if (column.ReportType == GlobalConstants.REPORT_TYPE_OF_GENERAL_14)
                    {
                        #region  // Kết quả học tập cấp 2,3
                        if (dtLearnResultInfo.Rows.Count > 0)
                        {
                            string keySearch = string.Format("PUPIL_ID = '{0}' AND CLASS_ID = '{1}'", pupilID, classID);
                            DataRow objData = dtLearnResultInfo.Select(keySearch).FirstOrDefault();
                            if (objData != null)
                            {
                                tempValue = objData[column.ColumnName].ToString();
                            }
                        }
                        #endregion
                    }
                    else if (column.ReportType == GlobalConstants.REPORT_TYPE_OF_GENERAL_16)
                    {
                        #region  // Kết quả sức khỏe cấp MN
                        if (dtHealthResultInfo.Rows.Count > 0)
                        {
                            string keySearch = string.Format("PUPIL_ID = '{0}' AND CLASS_ID = '{1}'", pupilID, classID);
                            DataRow objData = dtHealthResultInfo.Select(keySearch).FirstOrDefault();
                            if (objData != null)
                            {
                                tempValue = objData[column.ColumnName].ToString();
                            }
                        }
                        #endregion
                    }
                    else if ((column.ReportType == GlobalConstants.REPORT_TYPE_OF_GENERAL_5
                        || column.ReportType == GlobalConstants.REPORT_TYPE_OF_GENERAL_6
                        || column.ReportType == GlobalConstants.REPORT_TYPE_OF_GENERAL_7)
                        || (column.ReportType == GlobalConstants.REPORT_TYPE_OF_GENERAL_8
                            || column.ReportType == GlobalConstants.REPORT_TYPE_OF_GENERAL_9
                            || column.ReportType == GlobalConstants.REPORT_TYPE_OF_GENERAL_10
                            || column.ReportType == GlobalConstants.REPORT_TYPE_OF_GENERAL_17))
                    {
                        #region  // Thông tin chung, cá nhân, gia đình
                        if (dtPupilProfile.Columns[column.ColumnName] == null)
                        {
                            val = "";
                            tempValue = "";
                            _startCol++;
                            sheetFill.GetRange((column.CellName + row)).SetBorder(XLVTBorderStyleValues.Thin, XVTBorderIndex.OutsideBorder);
                            continue;
                        }

                        if (column.ColumnName == "ORDER_ID")
                        {
                            tempValue = stt.ToString();
                        }
                        else
                        {
                            tempValue = dr[column.ColumnName].ToString();
                        }
                        #endregion
                    }

                    if (column.ColumnType == GlobalConstants.COLUMN_TYPE_DATE)
                    {
                        if (!string.IsNullOrEmpty(tempValue))
                        {
                            try
                            {
                                val = SMAS.Business.Common.Utils.ConvertDateVN(tempValue).ToString("dd/MM/yyyy");
                                // val = DateTime.Parse(tempValue, System.Globalization.CultureInfo.InvariantCulture).ToString("dd/MM/yyyy");
                            }
                            catch (Exception ex)
                            {
                                logger.Error("Loi dinh dang ngay/thang/nam" + tempValue, ex);
                                val = tempValue;
                            }
                        }
                    }
                    else if (column.ColumnType.ToUpper() == GlobalConstants.COLUMN_TYPE_X)
                    {
                        val = !string.IsNullOrEmpty(tempValue) ? "x" : "";
                    }
                    else
                    {

                        if (column.ColumnName.Equals("FATHER_BIRTH_DATE") || column.ColumnName.Equals("MOTHER_BIRTH_DATE") || column.ColumnName.Equals("SPONSOR_BIRTH_DATE"))
                        {
                            if (!string.IsNullOrEmpty(tempValue))
                            {
                                val = tempValue.Substring(6, 4);
                            }
                        }
                        else
                        {
                            val = tempValue;
                        }
                    }

                    sheetFill.SetCellValue((column.CellName + row), (!string.IsNullOrEmpty(val) ? ("'" + val) : ""));
                    //sheetFill.GetRange(row, _startCol, row, _startCol).SetHAlign(6);
                    sheetFill.GetRange((column.CellName + row)).SetHAlign(6);
                    sheetFill.GetRange((column.CellName + row)).SetBorder(XLVTBorderStyleValues.Thin, XVTBorderIndex.OutsideBorder);
                    val = "";
                    tempValue = "";
                    _startCol++;
                }
                stt++;
                row++;
            }
            #endregion
            //sheetFill.GetRange(rawBorder, 1, (dtPupilProfile.Rows.Count + rawBorder), maxColumn).SetBorder(XLVTBorderStyleValues.Thin, XVTBorderIndex.OutsideBorder);
            //sheetFill.GetRange(rawBorder, 1, (dtPupilProfile.Rows.Count + rawBorder), maxColumn).SetBorder(XLVTBorderStyleValues.Thin, XVTBorderIndex.TopBorder);
            //sheetFill.GetRange(rawBorder, 1, (dtPupilProfile.Rows.Count + rawBorder), maxColumn).SetBorder(XLVTBorderStyleValues.Thin, XVTBorderIndex.BottomBorder);
            //sheetFill.GetRange(rawBorder, 1, (dtPupilProfile.Rows.Count + rawBorder), maxColumn).SetBorder(XLVTBorderStyleValues.Thin, XVTBorderIndex.LeftBorder);
            //sheetFill.GetRange(rawBorder, 1, (dtPupilProfile.Rows.Count + rawBorder), maxColumn).SetBorder(XLVTBorderStyleValues.Thin, XVTBorderIndex.RightBorder);
            //sheetFill.SetFontName("Times New Roman", 12);

            sheetFill.SheetName = "Danh sach HS";
            return oBook.ToStream();
        }

        // Hồ sơ học sinh, trẻ
        private DataTable GetGeneralPupilProfile(int schoolId, int AcademicYearID, int year, int appliedLevelId,
            int provinceId, int districtId, int statusId, string pupilCode, string fullName,
            int genre, int educationLevelId, int classId)
        {
            string connectionString = System.Web.Configuration.WebConfigurationManager.ConnectionStrings["OraAspNetConnectionString"].ToString();
            DataTable dt = new DataTable();
            using (OracleConnection conn = new OracleConnection(connectionString))
            {
                try
                {
                    if (conn.State != ConnectionState.Open)
                    {
                        conn.Open();
                    }

                    OracleCommand cmd = new OracleCommand
                    {
                        CommandType = CommandType.StoredProcedure,
                        CommandText = "SP_GET_PUPIL_BY_SCHOOL",
                        Connection = conn
                    };

                    cmd.Parameters.Add("P_SCHOOL_ID", OracleDbType.Int32).Value = schoolId;
                    cmd.Parameters.Add("P_ACADEMIC_YEAR_ID", OracleDbType.Int32).Value = AcademicYearID;
                    cmd.Parameters.Add("P_YEAR", OracleDbType.Int32).Value = year;
                    cmd.Parameters.Add("P_APPLIED_LEVEL_ID", OracleDbType.Int32).Value = appliedLevelId;
                    cmd.Parameters.Add("P_PROVINCE_ID", OracleDbType.Int32).Value = provinceId;
                    cmd.Parameters.Add("P_DISTRICT_ID", OracleDbType.Int32).Value = districtId;
                    cmd.Parameters.Add("P_STATUS_ID", OracleDbType.Int32).Value = statusId;
                    cmd.Parameters.Add("P_PUPIL_CODE", OracleDbType.NVarchar2).Value = pupilCode;
                    cmd.Parameters.Add("P_FULL_NAME", OracleDbType.NVarchar2).Value = fullName;
                    cmd.Parameters.Add("P_GENRE", OracleDbType.Int32).Value = genre;
                    cmd.Parameters.Add("P_EDUCATION_LEVEL_ID", OracleDbType.Int32).Value = educationLevelId;
                    cmd.Parameters.Add("P_CLASS_ID", OracleDbType.Int32).Value = classId;
                    cmd.Parameters.Add("P_CUSOR", OracleDbType.RefCursor, ParameterDirection.Output);

                    cmd.ExecuteNonQuery();
                    OracleDataAdapter da = new OracleDataAdapter(cmd);
                    da.Fill(dt);

                }
                catch (Exception ex)
                {
                    logger.Error(ex.Message, ex);
                }
                finally
                {
                    if (conn != null)
                    {
                        conn.Close();
                    }
                }
            }
            return dt;
        }

        public DataTable GetHealthOfPupil(int SchoolID, int AcademicYearID, int Year, int AppliedlevelID, int ProvinceID, int DistrictID, int StatusID, string PupilCode, string FullName, int Genre, int EducationLevelID, int ClassID)
        {
            string connectionString = System.Web.Configuration.WebConfigurationManager.ConnectionStrings["OraAspNetConnectionString"].ToString();
            DataTable dt = new DataTable();
            using (OracleConnection conn = new OracleConnection(connectionString))
            {

                try
                {
                    if (conn.State != ConnectionState.Open)
                    {
                        conn.Open();
                    }

                    OracleCommand cmd = new OracleCommand
                    {
                        CommandType = CommandType.StoredProcedure,
                        CommandText = "SP_GET_HEALTH_PUPIL_BY_SCHOOL",
                        Connection = conn
                    };
                    cmd.Parameters.Add("P_SCHOOL_ID", OracleDbType.Int32).Value = SchoolID;
                    cmd.Parameters.Add("P_ACADEMIC_YEAR_ID", OracleDbType.Int32).Value = AcademicYearID;
                    cmd.Parameters.Add("P_YEAR", OracleDbType.Int32).Value = Year;
                    cmd.Parameters.Add("P_APPLIED_LEVEL_ID", OracleDbType.Int32).Value = AppliedlevelID;
                    cmd.Parameters.Add("P_PROVINCE_ID", OracleDbType.Int32).Value = ProvinceID;
                    cmd.Parameters.Add("P_DISTRICT_ID", OracleDbType.Int32).Value = DistrictID;
                    cmd.Parameters.Add("P_STATUS_ID", OracleDbType.Int32).Value = StatusID;
                    cmd.Parameters.Add("P_PUPIL_CODE", OracleDbType.Varchar2).Value = PupilCode;
                    cmd.Parameters.Add("P_FULL_NAME", OracleDbType.Varchar2).Value = FullName;
                    cmd.Parameters.Add("P_GENRE", OracleDbType.Int32).Value = Genre;
                    cmd.Parameters.Add("P_EDUCATION_LEVEL_ID", OracleDbType.Int32).Value = EducationLevelID;
                    cmd.Parameters.Add("P_CLASS_ID", OracleDbType.Int32).Value = ClassID;
                    cmd.Parameters.Add("P_CUSOR", OracleDbType.RefCursor, ParameterDirection.Output);

                    cmd.ExecuteNonQuery();
                    OracleDataAdapter da = new OracleDataAdapter(cmd);
                    da.Fill(dt);

                }
                catch (Exception ex)
                {
                    logger.Error(ex.Message, ex);
                }
                finally
                {
                    if (conn != null)
                    {
                        conn.Close();
                    }
                }
            }
            return dt;
        }

        /// <summary>
        /// Thông tin môn học và HĐGD cấp 1 => typeStore = 1,
        /// Hoặc thông tin năng lực phẩm chất - kết quả cuối năm cấp 1 => typeStore =2,
        /// </summary>
        /// <param name="typeStore"></param>
        /// <param name="schoolId"></param>
        /// <param name="year"></param>
        /// <param name="appliedLevelId"></param>
        /// <param name="provinceId"></param>
        /// <param name="districtId"></param>
        /// <param name="statusId"></param>
        /// <param name="pupilCode"></param>
        /// <param name="fullName"></param>
        /// <param name="genre"></param>
        /// <param name="educationLevelId"></param>
        /// <param name="classId"></param>
        /// <param name="semesterId"></param>
        /// <returns></returns>
        private DataTable GetInfoResultOfPupilPrimary(int typeStore, int schoolId, int academicYearID, int year, int appliedLevelId,
            int provinceId, int districtId, int statusId, string pupilCode, string fullName,
            int genre, int educationLevelId, int classId, int semesterId, int semesterSelected)
        {
            string connectionString = System.Web.Configuration.WebConfigurationManager.ConnectionStrings["OraAspNetConnectionString"].ToString();
            DataTable dt = new DataTable();
            using (OracleConnection conn = new OracleConnection(connectionString))
            {
                try
                {
                    if (conn.State != ConnectionState.Open)
                    {
                        conn.Open();
                    }

                    string store = "";

                    if (typeStore == 1)
                    {
                        store = "SP_GET_INFO_MARK_SUBJECT_PUPIL";
                    }
                    else
                    {
                        store = "SP_GET_CAPACITY_QUALITY_PUPIL";
                    }

                    OracleCommand cmd = new OracleCommand
                    {
                        CommandType = CommandType.StoredProcedure,
                        CommandText = store,
                        Connection = conn
                    };

                    cmd.Parameters.Add("P_SCHOOL_ID", OracleDbType.Int32).Value = schoolId;
                    cmd.Parameters.Add("P_PARTTION_ID", OracleDbType.Int32).Value = (schoolId % 100);
                    cmd.Parameters.Add("P_ACADEMIC_YEAR_ID", OracleDbType.Int32).Value = academicYearID;
                    cmd.Parameters.Add("P_YEAR", OracleDbType.Int32).Value = year;
                    cmd.Parameters.Add("P_APPLIED_LEVEL_ID", OracleDbType.Int32).Value = appliedLevelId;
                    cmd.Parameters.Add("P_PROVINCE_ID", OracleDbType.Int32).Value = provinceId;
                    cmd.Parameters.Add("P_DISTRICT_ID", OracleDbType.Int32).Value = districtId;
                    cmd.Parameters.Add("P_STATUS_ID", OracleDbType.Int32).Value = statusId;
                    cmd.Parameters.Add("P_PUPIL_CODE", OracleDbType.NVarchar2).Value = pupilCode;
                    cmd.Parameters.Add("P_FULL_NAME", OracleDbType.NVarchar2).Value = fullName;
                    cmd.Parameters.Add("P_GENRE", OracleDbType.Int32).Value = genre;
                    cmd.Parameters.Add("P_EDUCATION_LEVEL_ID", OracleDbType.Int32).Value = educationLevelId;
                    cmd.Parameters.Add("P_CLASS_ID", OracleDbType.Int32).Value = classId;
                    cmd.Parameters.Add("P_SEMESTER_ID", OracleDbType.Int32).Value = semesterId;
                    cmd.Parameters.Add("P_SEMESTER_SELECTED", OracleDbType.Int32).Value = semesterSelected;
                    cmd.Parameters.Add("P_CUSOR", OracleDbType.RefCursor, ParameterDirection.Output);

                    cmd.ExecuteNonQuery();
                    OracleDataAdapter da = new OracleDataAdapter(cmd);
                    da.Fill(dt);

                }
                catch (Exception ex)
                {
                    logger.Error(ex.Message, ex);
                }
                finally
                {
                    if (conn != null)
                    {
                        conn.Close();
                    }
                }
            }
            return dt;
        }

        // Thông tin môn học cấp 2, 3
        public DataTable GetMarkPupil23(int SchoolID, int AcademicYearID, int Year, int AppliedlevelID, int ProvinceID, int DistrictID, int StatusID,
            string PupilCode, string FullName, int? Genre, int EducationLevelID, int ClassID, int SemesterID, int ReportTypeID)
        {
            string connectionString = System.Web.Configuration.WebConfigurationManager.ConnectionStrings["OraAspNetConnectionString"].ToString();
            DataTable dt = new DataTable();
            using (OracleConnection conn = new OracleConnection(connectionString))
            {
                try
                {
                    if (conn.State != ConnectionState.Open)
                    {
                        conn.Open();
                    }

                    OracleCommand cmd = new OracleCommand
                    {
                        CommandType = CommandType.StoredProcedure,
                        CommandText = "SP_GET_MARK_PUPIL_BY_SCHOOL",
                        Connection = conn
                    };
                    cmd.Parameters.Add("P_SCHOOL_ID", OracleDbType.Int32).Value = SchoolID;
                    cmd.Parameters.Add("P_ACADEMIC_YEAR_ID", OracleDbType.Int32).Value = AcademicYearID;
                    cmd.Parameters.Add("P_YEAR", OracleDbType.Int32).Value = Year;
                    cmd.Parameters.Add("P_APPLIED_LEVEL_ID", OracleDbType.Int32).Value = AppliedlevelID;
                    cmd.Parameters.Add("P_PROVINCE_ID", OracleDbType.Int32).Value = ProvinceID;
                    cmd.Parameters.Add("P_DISTRICT_ID", OracleDbType.Int32).Value = DistrictID;
                    cmd.Parameters.Add("P_STATUS_ID", OracleDbType.Int32).Value = StatusID;
                    cmd.Parameters.Add("P_PUPIL_CODE", OracleDbType.Varchar2).Value = PupilCode;
                    cmd.Parameters.Add("P_FULL_NAME", OracleDbType.Varchar2).Value = FullName;
                    cmd.Parameters.Add("P_GENRE", OracleDbType.Int32).Value = Genre;
                    cmd.Parameters.Add("P_EDUCATION_LEVEL_ID", OracleDbType.Int32).Value = EducationLevelID;
                    cmd.Parameters.Add("P_CLASS_ID", OracleDbType.Int32).Value = ClassID;
                    cmd.Parameters.Add("P_SEMESTER_ID", OracleDbType.Int32).Value = SemesterID;
                    cmd.Parameters.Add("P_REPORT_TYPE_ID", OracleDbType.Int32).Value = ReportTypeID;
                    cmd.Parameters.Add("P_CUSOR", OracleDbType.RefCursor, ParameterDirection.Output);

                    cmd.ExecuteNonQuery();
                    OracleDataAdapter da = new OracleDataAdapter(cmd);
                    da.Fill(dt);

                }
                catch (Exception ex)
                {
                    logger.Error(ex.Message, ex);
                }
                finally
                {
                    if (conn != null)
                    {
                        conn.Close();
                    }
                }
            }
            return dt;
        }

        // Thông tin kết quả học tập, rèn luyện cấp 2, 3
        private DataTable GetLearnResult23(int SchoolID, int AcademicYearID, int Year, int AppliedlevelID, int ProvinceID, int DistrictID,
            string PupilCode, string FullName, int Genre, int EducationLevelID, int ClassID, int SemesterID, int StatusID)
        {
            string connectionString = System.Web.Configuration.WebConfigurationManager.ConnectionStrings["OraAspNetConnectionString"].ToString();
            DataTable dt = new DataTable();
            using (OracleConnection conn = new OracleConnection(connectionString))
            {

                try
                {
                    if (conn.State != ConnectionState.Open)
                    {
                        conn.Open();
                    }

                    OracleCommand cmd = new OracleCommand
                    {
                        CommandType = CommandType.StoredProcedure,
                        CommandText = "SP_GET_STUDY_RESULT",
                        Connection = conn
                    };
                    cmd.Parameters.Add("P_SCHOOL_ID", OracleDbType.Int32).Value = SchoolID;
                    cmd.Parameters.Add("P_ACADEMIC_YEAR_ID", OracleDbType.Int32).Value = AcademicYearID;
                    cmd.Parameters.Add("P_YEAR", OracleDbType.Int32).Value = Year;
                    cmd.Parameters.Add("P_APPLIED_LEVEL_ID", OracleDbType.Int32).Value = AppliedlevelID;
                    cmd.Parameters.Add("P_PROVINCE_ID", OracleDbType.Int32).Value = ProvinceID;
                    cmd.Parameters.Add("P_DISTRICT_ID", OracleDbType.Int32).Value = DistrictID;
                    cmd.Parameters.Add("P_PUPIL_CODE", OracleDbType.Varchar2).Value = PupilCode;
                    cmd.Parameters.Add("P_FULL_NAME", OracleDbType.Varchar2).Value = FullName;
                    cmd.Parameters.Add("P_GENRE", OracleDbType.Int32).Value = Genre;
                    cmd.Parameters.Add("P_EDUCATION_LEVEL_ID", OracleDbType.Int32).Value = EducationLevelID;
                    cmd.Parameters.Add("P_CLASS_ID", OracleDbType.Int32).Value = ClassID;
                    cmd.Parameters.Add("P_SEMESTER_ID", OracleDbType.Int32).Value = SemesterID;
                    cmd.Parameters.Add("P_STATUS_ID", OracleDbType.Int32).Value = StatusID;
                    cmd.Parameters.Add("P_CUSOR", OracleDbType.RefCursor, ParameterDirection.Output);

                    cmd.ExecuteNonQuery();
                    OracleDataAdapter da = new OracleDataAdapter(cmd);
                    da.Fill(dt);

                }
                catch (Exception ex)
                {
                    logger.Error(ex.Message, ex);
                }
                finally
                {
                    if (conn != null)
                    {
                        conn.Close();
                    }
                }
            }
            return dt;
        }

        // Thông tin kết quả sức khỏe cấp MN
        private DataTable GetHealthResult(int SchoolID, int AcademicYearID, int Year, int AppliedlevelID, int ProvinceID, int DistrictID,
            string PupilCode, string FullName, int Genre, int EducationLevelID, int ClassID, int StatusID)
        {
            string connectionString = System.Web.Configuration.WebConfigurationManager.ConnectionStrings["OraAspNetConnectionString"].ToString();
            DataTable dt = new DataTable();
            using (OracleConnection conn = new OracleConnection(connectionString))
            {
                try
                {
                    if (conn.State != ConnectionState.Open)
                    {
                        conn.Open();
                    }

                    OracleCommand cmd = new OracleCommand
                    {
                        CommandType = CommandType.StoredProcedure,
                        CommandText = "SP_GET_HEALTH_PUPIL_BY_SCHOOL",
                        Connection = conn
                    };
                    cmd.Parameters.Add("P_SCHOOL_ID", OracleDbType.Int32).Value = SchoolID;
                    cmd.Parameters.Add("P_ACADEMIC_YEAR_ID", OracleDbType.Int32).Value = AcademicYearID;
                    cmd.Parameters.Add("P_YEAR", OracleDbType.Int32).Value = Year;
                    cmd.Parameters.Add("P_APPLIED_LEVEL_ID", OracleDbType.Int32).Value = AppliedlevelID;
                    cmd.Parameters.Add("P_PROVINCE_ID", OracleDbType.Int32).Value = ProvinceID;
                    cmd.Parameters.Add("P_DISTRICT_ID", OracleDbType.Int32).Value = DistrictID;
                    cmd.Parameters.Add("P_PUPIL_CODE", OracleDbType.Varchar2).Value = PupilCode;
                    cmd.Parameters.Add("P_FULL_NAME", OracleDbType.Varchar2).Value = FullName;
                    cmd.Parameters.Add("P_GENRE", OracleDbType.Int32).Value = Genre;
                    cmd.Parameters.Add("P_EDUCATION_LEVEL_ID", OracleDbType.Int32).Value = EducationLevelID;
                    cmd.Parameters.Add("P_CLASS_ID", OracleDbType.Int32).Value = ClassID;
                    cmd.Parameters.Add("P_STATUS_ID", OracleDbType.Int32).Value = StatusID;
                    cmd.Parameters.Add("P_CUSOR", OracleDbType.RefCursor, ParameterDirection.Output);

                    cmd.ExecuteNonQuery();
                    OracleDataAdapter da = new OracleDataAdapter(cmd);
                    da.Fill(dt);

                }
                catch (Exception ex)
                {
                    logger.Error(ex.Message, ex);
                }
                finally
                {
                    if (conn != null)
                    {
                        conn.Close();
                    }
                }
            }
            return dt;
        }

        private DataTable GetMarkVNENPupil(int SchoolID, int AcademicYearID, int Year, int AppliedlevelID, int ProvinceID, int DistrictID,
            int StatusID, string PupilCode, string FullName, int? Genre, int EducationLevelID, int ClassID, int SemesterID)
        {
            string connectionString = System.Web.Configuration.WebConfigurationManager.ConnectionStrings["OraAspNetConnectionString"].ToString();
            DataTable dt = new DataTable();
            using (OracleConnection conn = new OracleConnection(connectionString))
            {

                try
                {
                    if (conn.State != ConnectionState.Open)
                    {
                        conn.Open();
                    }

                    OracleCommand cmd = new OracleCommand
                    {
                        CommandType = CommandType.StoredProcedure,
                        CommandText = "SP_GET_MARK_VNEN_PUPIL",
                        Connection = conn
                    };
                    cmd.Parameters.Add("P_SCHOOL_ID", OracleDbType.Int32).Value = SchoolID;
                    cmd.Parameters.Add("P_ACADEMIC_YEAR_ID", OracleDbType.Int32).Value = AcademicYearID;
                    cmd.Parameters.Add("P_YEAR", OracleDbType.Int32).Value = Year;
                    cmd.Parameters.Add("P_APPLIED_LEVEL_ID", OracleDbType.Int32).Value = AppliedlevelID;
                    cmd.Parameters.Add("P_PROVINCE_ID", OracleDbType.Int32).Value = ProvinceID;
                    cmd.Parameters.Add("P_DISTRICT_ID", OracleDbType.Int32).Value = DistrictID;
                    cmd.Parameters.Add("P_STATUS_ID", OracleDbType.Int32).Value = StatusID;
                    cmd.Parameters.Add("P_PUPIL_CODE", OracleDbType.Varchar2).Value = PupilCode;
                    cmd.Parameters.Add("P_FULL_NAME", OracleDbType.Varchar2).Value = FullName;
                    cmd.Parameters.Add("P_GENRE", OracleDbType.Int32).Value = Genre;
                    cmd.Parameters.Add("P_EDUCATION_LEVEL_ID", OracleDbType.Int32).Value = EducationLevelID;
                    cmd.Parameters.Add("P_CLASS_ID", OracleDbType.Int32).Value = ClassID;
                    cmd.Parameters.Add("P_SEMESTER_ID", OracleDbType.Int32).Value = SemesterID;
                    cmd.Parameters.Add("P_CUSOR", OracleDbType.RefCursor, ParameterDirection.Output);

                    cmd.ExecuteNonQuery();
                    OracleDataAdapter da = new OracleDataAdapter(cmd);
                    da.Fill(dt);

                }
                catch (Exception ex)
                {
                    logger.Error(ex.Message, ex);
                }
                finally
                {
                    if (conn != null)
                    {
                        conn.Close();
                    }
                }
            }
            return dt;
        }

        private DataTable GetJudgementPupil(int SchoolID, int AcademicYearID, int Year, int AppliedlevelID, int ProvinceID, int DistrictID, int StatusID, string PupilCode, string FullName, int? Genre, int EducationLevelID, int ClassID, int SemesterID)
        {
            string connectionString = System.Web.Configuration.WebConfigurationManager.ConnectionStrings["OraAspNetConnectionString"].ToString();

            DataTable dt = new DataTable();
            using (OracleConnection conn = new OracleConnection(connectionString))
            {

                try
                {
                    if (conn.State != ConnectionState.Open)
                    {
                        conn.Open();
                    }

                    OracleCommand cmd = new OracleCommand
                    {
                        CommandType = CommandType.StoredProcedure,
                        CommandText = "SP_GET_JUDGE_PUPIL_BY_SCHOOL",
                        Connection = conn
                    };
                    cmd.Parameters.Add("P_SCHOOL_ID", OracleDbType.Int32).Value = SchoolID;
                    cmd.Parameters.Add("P_ACADEMIC_YEAR_ID", OracleDbType.Int32).Value = AcademicYearID;
                    cmd.Parameters.Add("P_YEAR", OracleDbType.Int32).Value = Year;
                    cmd.Parameters.Add("P_APPLIED_LEVEL_ID", OracleDbType.Int32).Value = AppliedlevelID;
                    cmd.Parameters.Add("P_PROVINCE_ID", OracleDbType.Int32).Value = ProvinceID;
                    cmd.Parameters.Add("P_DISTRICT_ID", OracleDbType.Int32).Value = DistrictID;
                    cmd.Parameters.Add("P_STATUS_ID", OracleDbType.Int32).Value = StatusID;
                    cmd.Parameters.Add("P_PUPIL_CODE", OracleDbType.Varchar2).Value = PupilCode;
                    cmd.Parameters.Add("P_FULL_NAME", OracleDbType.Varchar2).Value = FullName;
                    cmd.Parameters.Add("P_GENRE", OracleDbType.Int32).Value = Genre;
                    cmd.Parameters.Add("P_EDUCATION_LEVEL_ID", OracleDbType.Int32).Value = EducationLevelID;
                    cmd.Parameters.Add("P_CLASS_ID", OracleDbType.Int32).Value = ClassID;
                    cmd.Parameters.Add("P_SEMESTER_ID", OracleDbType.Int32).Value = SemesterID;
                    cmd.Parameters.Add("P_CUSOR", OracleDbType.RefCursor, ParameterDirection.Output);

                    cmd.ExecuteNonQuery();
                    OracleDataAdapter da = new OracleDataAdapter(cmd);
                    da.Fill(dt);

                }
                catch (Exception ex)
                {
                    logger.Error(ex.Message, ex);
                }
                finally
                {
                    if (conn != null)
                    {
                        conn.Close();
                    }
                }
            }
            return dt;
        }

        #endregion

        #region Xuất excel cho lớp
        public Stream ExportLookupInfoClassBySuper(Dictionary<String, object> dic, out string FileName)
        {
            int HierachyLevel = Utils.GetInt(dic, "HierachyLevel");
            int AppliedLevel = Utils.GetInt(dic, "frm[EducationGrade]");
            int Year = Utils.GetInt(dic, "frm[AcademicYear]");
            int DistrictID = Utils.GetInt(dic, "frm[District]");
            int SchoolID = Utils.GetInt(dic, "frm[School]");
            int EducationLevelID = Utils.GetInt(dic, "frm[EducationLevel]");
            int ProvinceID = Utils.GetInt(dic, "frm[ProvinceID]");
            //int SupervisingDeptID = Utils.GetInt(dic, "SuperVisingDeptIDCus");
            int TemplateID = Utils.GetInt(dic, "frm[ProfileTemplateId]");
            int startRow = Utils.GetInt(dic, "frm[RowId]");
            if (startRow <= 0)
                startRow = 1;
            bool isSub = Utils.GetBool(dic, "IsSub");

            string reportCode = "";
            reportCode = SystemParamsInFile.REPORT_DANHSACHLOP;
            ReportDefinition rp = ReportDefinitionBusiness.GetByCode(reportCode);
            string templatePath = SystemParamsInFile.TEMPLATE_FOLDER + rp.TemplateDirectory + "/DanhSachLop.xls";// +rp.TemplateName;
            IVTWorkbook oBook = VTExport.OpenWorkbook(templatePath);
            IVTWorksheet sheetFill = oBook.GetSheet(1);

            //File Name
            string outputNamePattern = rp.OutputNamePattern;
            if (HierachyLevel == SystemParamsInFile.EDUCATION_HIERACHY_LEVEL_PROVINCE_OFFICE)
            {
                outputNamePattern = outputNamePattern.Replace("[LevelAccount]", ReportUtils.StripVNSign("SGD"));
            }
            if (HierachyLevel == SystemParamsInFile.EDUCATION_HIERACHY_LEVEL_DISTRICT_OFFICE)
            {
                outputNamePattern = outputNamePattern.Replace("[LevelAccount]", ReportUtils.StripVNSign("PGD"));
            }
            FileName = ReportUtils.RemoveSpecialCharacters(outputNamePattern) + "." + rp.OutputFormat;

            #region // Get data
            ProfileTemplate profileTemplate = ProfileTemplateBusiness.Find(TemplateID);
            if (profileTemplate == null)
            {
                return null;
            }
            startRow += 1;

            List<EmployeeColumnTemplateBO> lstTemplate = new List<EmployeeColumnTemplateBO>();
            if (profileTemplate != null && !string.IsNullOrEmpty(profileTemplate.ColDescriptionIds))
            {
                lstTemplate = Newtonsoft.Json.JsonConvert.DeserializeObject<List<EmployeeColumnTemplateBO>>(profileTemplate.ColDescriptionIds);
            }

            string strApplied = AppliedLevel.ToString();
            if (SchoolID > 0 && (AppliedLevel == 2 || AppliedLevel == 3))
            {
                SchoolProfile objSchool = SchoolProfileBusiness.Find(SchoolID);
                if (objSchool != null && objSchool.TrainingTypeID == SystemParamsInFile.TRAINING_TYPE_GDTX)
                {
                    strApplied = "6";
                }
            }

            List<ColumnDescription> lstColumnDescriptionTotal = new List<ColumnDescription>();
            IDictionary<string, object> dicCol = new Dictionary<string, object>();
            List<int> lstReportType = new List<int>() { GlobalConstants.REPORT_TYPE_OF_GENERAL_19, GlobalConstants.REPORT_TYPE_OF_GENERAL_20 };
            lstColumnDescriptionTotal = ColumnDescriptionBusiness.Search(dicCol).Where(x => x.ReportType.HasValue && lstReportType.Contains(x.ReportType.Value))
                                                                                .Where(x => !string.IsNullOrEmpty(x.AppliedLevelID) && x.AppliedLevelID.Contains(strApplied)).ToList();

            List<ColumnDescriptionBO> lstColumn = (from temp in lstTemplate
                                                   join cd in lstColumnDescriptionTotal on temp.ColDesId equals cd.ColumnDescriptionId
                                                   select new ColumnDescriptionBO
                                                   {
                                                       CellName = temp.ColExcel,
                                                       ColumnDescriptionId = cd.ColumnDescriptionId,
                                                       ColumnIndex = new VTVector(temp.ColExcel + "1").Y,
                                                       ColumnName = cd.ColumnName,
                                                       ColumnSize = cd.ColumnSize,
                                                       Resolution = temp.ExcelName,
                                                       ColumnOrder = cd.ColumnOrder,
                                                       ColumnType = cd.ColumnType,
                                                       Description = cd.Description,
                                                       RequiredColumn = cd.RequiredColumn,
                                                       TableName = cd.TableName,
                                                       ReportType = cd.ReportType,
                                                       OrderID = cd.OrderID
                                                   }).ToList();
            lstColumn = lstColumn.OrderBy(x => x.OrderID).OrderBy(c => c.ColumnOrder).ToList();

            DataTable dtClassProfile = GetGeneralClassProfile(1, SchoolID, Year, AppliedLevel, ProvinceID, DistrictID, EducationLevelID);
            DataTable dtPropertyOfClass = GetGeneralClassProfile(2, SchoolID, Year, AppliedLevel, ProvinceID, DistrictID, EducationLevelID);
            #endregion


            #region // fill data

            int rawBorder = startRow;
            int startColumn = lstColumn.Count() != 0 ? lstColumn.First().ColumnIndex : 1;
            int maxColumn = lstColumn.Count();
            int maxCell = lstColumn.Count() != 0 ? lstColumn.Last().ColumnIndex : 1;
            int row = startRow;
            for (int i = 0; i < lstColumn.Count; i++)
            {
                ColumnDescriptionBO column = lstColumn[i];
                sheetFill.SetCellValue(row, column.ColumnIndex, column.Resolution);
                sheetFill.SetColumnWidth(column.ColumnIndex, Convert.ToDouble(column.ColumnSize));
            }
            sheetFill.GetRange(row, startColumn, row, maxCell).SetFontStyle(true, System.Drawing.Color.Black, false, 12, false, false);

            sheetFill.GetRange(row, startColumn, row, maxCell).FillColor(System.Drawing.Color.Gray);
            sheetFill.GetRange(row, startColumn, row, maxCell).SetHAlign(VTHAlign.xlHAlignCenter);
            row++;

            int stt = 1;
            string val = "";
            string tempValue = "";
            int _startCol = 0;
            List<SubjectCat> lstSubjectCat = this.SubjectCatBusiness.All.Where(x => x.IsActive).ToList();
            for (int j = 0; j < dtClassProfile.Rows.Count; j++)
            {
                DataRow dr = dtClassProfile.Rows[j];
                int classID = Convert.ToInt32(dr["CLASS_PROFILE_ID"]);
                _startCol = 1;
                for (int i = 0; i < lstColumn.Count; i++)
                {
                    ColumnDescriptionBO column = lstColumn[i];

                    #region

                    if (column.ColumnName.Contains("CLASS_PROPERTY_CAT"))
                    {
                        string keySearch = String.Format("CLASS_PROFILE_ID = '{0}'", classID);

                        DataRow[] lstDataRow = dtPropertyOfClass.Select(keySearch);

                        for (int h = 0; h < lstDataRow.Count(); h++)
                        {
                            switch (column.ColumnName)
                            {
                                case "CLASS_PROPERTY_CAT_1":
                                    var objVal1 = lstDataRow[h].ItemArray[1].ToString();
                                    if (!string.IsNullOrEmpty(objVal1))
                                    {
                                        val = objVal1;
                                    }
                                    break;
                                case "CLASS_PROPERTY_CAT_2":
                                    var objVal2 = lstDataRow[h].ItemArray[2].ToString();
                                    if (!string.IsNullOrEmpty(objVal2))
                                    {
                                        val = objVal2;
                                    }
                                    break;
                                case "CLASS_PROPERTY_CAT_3":
                                    var objVal3 = lstDataRow[h].ItemArray[3].ToString();
                                    if (!string.IsNullOrEmpty(objVal3))
                                    {
                                        val = objVal3;
                                    }
                                    break;
                                case "CLASS_PROPERTY_CAT_4":
                                    var objVal4 = lstDataRow[h].ItemArray[4].ToString();
                                    if (!string.IsNullOrEmpty(objVal4))
                                    {
                                        val = objVal4;
                                    }
                                    break;
                                case "CLASS_PROPERTY_CAT_5":
                                    var objVal5 = lstDataRow[h].ItemArray[5].ToString();
                                    if (!string.IsNullOrEmpty(objVal5))
                                    {
                                        val = objVal5;
                                    }
                                    break;
                                case "CLASS_PROPERTY_CAT_6":
                                    var objVal6 = lstDataRow[h].ItemArray[6].ToString();
                                    if (!string.IsNullOrEmpty(objVal6))
                                    {
                                        val = objVal6;
                                    }
                                    break;
                                case "CLASS_PROPERTY_CAT_7":
                                    var objVal7 = lstDataRow[h].ItemArray[7].ToString();
                                    if (!string.IsNullOrEmpty(objVal7))
                                    {
                                        val = objVal7;
                                    }
                                    break;
                                case "CLASS_PROPERTY_CAT_8":
                                    var objVal8 = lstDataRow[h].ItemArray[8].ToString();
                                    if (!string.IsNullOrEmpty(objVal8))
                                    {
                                        val = objVal8;
                                    }
                                    break;
                                case "CLASS_PROPERTY_CAT_9":
                                    var objVal9 = lstDataRow[h].ItemArray[9].ToString();
                                    if (!string.IsNullOrEmpty(objVal9))
                                    {
                                        val = objVal9;
                                    }
                                    break;
                                case "CLASS_PROPERTY_CAT_10":
                                    var objVal10 = lstDataRow[h].ItemArray[10].ToString();
                                    if (!string.IsNullOrEmpty(objVal10))
                                    {
                                        val = objVal10;
                                    }
                                    break;
                                case "CLASS_PROPERTY_CAT_11":
                                    var objVal11 = lstDataRow[h].ItemArray[11].ToString();
                                    if (!string.IsNullOrEmpty(objVal11))
                                    {
                                        val = objVal11;
                                    }
                                    break;
                                case "CLASS_PROPERTY_CAT_12":
                                    var objVal12 = lstDataRow[h].ItemArray[12].ToString();
                                    if (!string.IsNullOrEmpty(objVal12))
                                    {
                                        val = objVal12;
                                    }
                                    break;
                                case "CLASS_PROPERTY_CAT_13":
                                    var objVal13 = lstDataRow[h].ItemArray[13].ToString();
                                    if (!string.IsNullOrEmpty(objVal13))
                                    {
                                        val = objVal13;
                                    }
                                    break;
                                case "CLASS_PROPERTY_CAT_14":
                                    var objVal14 = lstDataRow[h].ItemArray[14].ToString();
                                    if (!string.IsNullOrEmpty(objVal14))
                                    {
                                        val = objVal14;
                                    }
                                    break;
                                case "CLASS_PROPERTY_CAT_15":
                                    var objVal15 = lstDataRow[h].ItemArray[15].ToString();
                                    if (!string.IsNullOrEmpty(objVal15))
                                    {
                                        val = objVal15;
                                    }
                                    break;
                                case "CLASS_PROPERTY_CAT_16":
                                    var objVal16 = lstDataRow[h].ItemArray[16].ToString();
                                    if (!string.IsNullOrEmpty(objVal16))
                                    {
                                        val = objVal16;
                                    }
                                    break;
                                case "CLASS_PROPERTY_CAT_17":
                                    var objVal17 = lstDataRow[h].ItemArray[17].ToString();
                                    if (!string.IsNullOrEmpty(objVal17))
                                    {
                                        val = objVal17;
                                    }
                                    break;
                                case "CLASS_PROPERTY_CAT_18":
                                    var objVal18 = lstDataRow[h].ItemArray[18].ToString();
                                    if (!string.IsNullOrEmpty(objVal18))
                                    {
                                        val = objVal18;
                                    }
                                    break;
                                case "CLASS_PROPERTY_CAT_19":
                                    var objVal19 = lstDataRow[h].ItemArray[19].ToString();
                                    if (!string.IsNullOrEmpty(objVal19))
                                    {
                                        val = objVal19;
                                    }
                                    break;
                                case "CLASS_PROPERTY_CAT_20":
                                    var objVal20 = lstDataRow[h].ItemArray[20].ToString();
                                    if (!string.IsNullOrEmpty(objVal20))
                                    {
                                        val = objVal20;
                                    }
                                    break;
                            }
                        }

                        sheetFill.SetCellValue((column.CellName + row), (!string.IsNullOrEmpty(val) ? val : ""));
                        sheetFill.GetRange(row, _startCol, row, _startCol).SetHAlign(VTHAlign.xlHAlignLeft);
                        val = "";
                        tempValue = "";
                        _startCol++;
                        continue;
                    }

                    if (dtClassProfile.Columns[column.ColumnName] == null)
                    {
                        val = "";
                        tempValue = "";
                        _startCol++;
                        continue;
                    }

                    if (column.ColumnName == "ORDER_ID")
                    {
                        tempValue = stt.ToString();
                    }
                    else
                    {
                        tempValue = dr[column.ColumnName].ToString();
                    }
                    #endregion

                    if (column.ColumnName == "FIRST_FOREIGN_LANGUAGE")
                    {
                        if (!string.IsNullOrEmpty(dr[column.ColumnName].ToString()))
                        {
                            string firstForeignLanguage = "";
                            int firstForeignLanguageId = Int32.Parse(dr[column.ColumnName].ToString());
                            var objLanguage1 = lstSubjectCat.Where(x => x.SubjectCatID == firstForeignLanguageId).FirstOrDefault();
                            if (objLanguage1 != null)
                            {
                                firstForeignLanguage = objLanguage1.SubjectName;
                            }
                            tempValue = firstForeignLanguage;
                        }
                    }

                    if (column.ColumnName == "SECOND_FOREIGN_LANGUAGE")
                    {
                        if (!string.IsNullOrEmpty(dr[column.ColumnName].ToString()))
                        {
                            string secondForeignLanguage = "";
                            int secondForeignLanguageId = Int32.Parse(dr[column.ColumnName].ToString());
                            var objLanguage2 = lstSubjectCat.Where(x => x.SubjectCatID == secondForeignLanguageId).FirstOrDefault();
                            if(objLanguage2 != null)
                            {
                                secondForeignLanguage = objLanguage2.SubjectName;
                            }
                            tempValue = secondForeignLanguage;
                        }

                    }

                    if (column.ColumnType == GlobalConstants.COLUMN_TYPE_DATE)
                    {
                        if (!string.IsNullOrEmpty(tempValue))
                        {
                            val = Convert.ToDateTime(tempValue).ToString("dd/MM/yyyy");
                        }
                    }
                    else if (column.ColumnType.ToUpper() == GlobalConstants.COLUMN_TYPE_X)
                    {
                        val = tempValue == "x" ? "x" : "";
                    }
                    else
                    {
                        val = tempValue;
                    }

                    sheetFill.SetCellValue((column.CellName + row), (!string.IsNullOrEmpty(val) ? val : ""));
                    sheetFill.GetRange(row, _startCol, row, _startCol).SetHAlign(VTHAlign.xlHAlignLeft);
                    val = "";
                    tempValue = "";
                    _startCol++;
                }
                stt++;
                row++;
            }
            #endregion

            sheetFill.GetRange(rawBorder, startColumn, (dtClassProfile.Rows.Count + rawBorder), maxCell).SetBorder(VTBorderStyle.Solid, VTBorderWeight.Thin, VTBorderIndex.All);
            sheetFill.SetFontName("Times New Roman", 12);

            sheetFill.Name = "Danh sach lop";
            return oBook.ToStream(0);
        }

        private DataTable GetGeneralClassProfile(int typeStore, int schoolId, int year, int appliedLevelId, int provinceId, int districtId, int educationLevelId)
        {
            string connectionString = System.Web.Configuration.WebConfigurationManager.ConnectionStrings["OraAspNetConnectionString"].ToString();
            DataTable dt = new DataTable();
            using (OracleConnection conn = new OracleConnection(connectionString))
            {
                try
                {
                    if (conn.State != ConnectionState.Open)
                    {
                        conn.Open();
                    }

                    OracleCommand cmd = new OracleCommand
                    {
                        CommandType = CommandType.StoredProcedure,
                        CommandText = typeStore == 1 ? "SP_GET_CLASS_PROFILE" : "SP_GET_PROPERTY_CLASS_PROFILE",
                        Connection = conn
                    };

                    cmd.Parameters.Add("P_SCHOOL_ID", OracleDbType.Int32).Value = schoolId;
                    cmd.Parameters.Add("P_YEAR", OracleDbType.Int32).Value = year;
                    cmd.Parameters.Add("P_APPLIED_LEVEL_ID", OracleDbType.Int32).Value = appliedLevelId;
                    cmd.Parameters.Add("P_PROVINCE_ID", OracleDbType.Int32).Value = provinceId;
                    cmd.Parameters.Add("P_DISTRICT_ID", OracleDbType.Int32).Value = districtId;
                    cmd.Parameters.Add("P_EDUCATION_LEVEL_ID", OracleDbType.Int32).Value = educationLevelId;
                    cmd.Parameters.Add("P_CUSOR", OracleDbType.RefCursor, ParameterDirection.Output);

                    cmd.ExecuteNonQuery();
                    OracleDataAdapter da = new OracleDataAdapter(cmd);
                    da.Fill(dt);

                }
                catch (Exception ex)
                {
                    logger.Error(ex.Message, ex);
                }
                finally
                {
                    if (conn != null)
                    {
                        conn.Close();
                    }
                }
            }
            return dt;
        }

        #endregion
    }
}
