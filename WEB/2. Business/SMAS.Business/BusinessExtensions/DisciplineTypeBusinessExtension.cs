/**
 * The Viettel License
 *
 * Copyright 2012 Viettel Telecom. All rights reserved.
 * VIETTEL PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */

/** 
* @author  
* @version $Revision: $
*/

using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Linq.Expressions;
using System.Web;
using log4net;

using SMAS.DAL.Repository;
using SMAS.DAL.IRepository;
using SMAS.Business.IBusiness;
using SMAS.Models.Models;
using SMAS.Business.Common;

namespace SMAS.Business.Business
{ 
    public partial class DisciplineTypeBusiness
    {
        /// <summary>
        /// Insert vao CSDL
        /// </summary>
        /// <param name="DisciplineType"></param>
        /// <returns></returns>
        public override DisciplineType Insert(DisciplineType DisciplineType)
        {
            // Kiem tra truong bat buoc
            Utils.ValidateRequire(DisciplineType.Resolution, "DisciplineType_Label_Resolution");
            // Kiem tra truong Resolution maxlength khong duoc vuot qua 100 ky tu
            Utils.ValidateMaxLength(DisciplineType.Resolution, 100, "DisciplineType_Label_Resolution");
            // Kiem tra thong tin truong hoc da co trong CSDL
            SchoolProfileBusiness.CheckAvailable(DisciplineType.SchoolID, "SchoolProfile_Label_SchoolID", true);
            // Kiem tra trung ten va truong hoc
            string strSchoolID = DisciplineType.SchoolID == null ? null : DisciplineType.SchoolID.ToString();
            //this.CheckDuplicateCouple(DisciplineType.Resolution, strSchoolID, GlobalConstants.LIST_SCHEMA, "DisciplineType", "Resolution", "SchoolID", true, null, "DisciplineType_Label_Resolution");
            Dictionary<string, object> SearchInfo = new Dictionary<string, object>();
            SearchInfo["SchoolID"] = DisciplineType.SchoolID;
            SearchInfo["Resolution"] = DisciplineType.Resolution;
            SearchInfo["IsActive"] = true;


            IDictionary<string, object> ExceptInfo = new Dictionary<string, object>();
            ExceptInfo["DisciplineTypeID"] = DisciplineType.DisciplineTypeID;

            bool duplicate = this.repository.ExistsRow(GlobalConstants.LIST_SCHEMA, "DisciplineType", SearchInfo, ExceptInfo);
            if (duplicate)
            {
                List<object> Params = new List<object>();
                Params.Add("DisciplineType_Label_Resolution");
                throw new BusinessException("Common_Validate_Duplicate", Params);
            }
            // Them vao CSDL            
            return base.Insert(DisciplineType);
        }

        /// <summary>
        /// Cap nhat vao CSDL
        /// </summary>
        /// <param name="DisciplineType"></param>
        /// <returns></returns>
        public override DisciplineType Update(DisciplineType DisciplineType)
        {
            // Kiem tra truong bat buoc
            Utils.ValidateRequire(DisciplineType.Resolution, "DisciplineType_Label_Resolution");
            // Kiem tra truong Resolution maxlength khong duoc vuot qua 100 ky tu
            Utils.ValidateMaxLength(DisciplineType.Resolution, 100, "DisciplineType_Label_Resolution");
            // Kiem tra trung ten va truong hoc
            // Kiem tra thong tin truong hoc da co trong CSDL
            SchoolProfileBusiness.CheckAvailable(DisciplineType.SchoolID, "SchoolProfile_Label_SchoolID", true);
            // Kiem tra trung ten va truong hoc
            string strSchoolID = DisciplineType.SchoolID == null ? null : DisciplineType.SchoolID.ToString();
            //this.CheckDuplicateCouple(DisciplineType.Resolution, strSchoolID, GlobalConstants.LIST_SCHEMA, "DisciplineType", "Resolution", "SchoolID", true, null, "DisciplineType_Label_Resolution");
            Dictionary<string, object> SearchInfo = new Dictionary<string, object>();
            SearchInfo["SchoolID"] = DisciplineType.SchoolID;
            SearchInfo["Resolution"] = DisciplineType.Resolution;
            SearchInfo["IsActive"] = true;


            IDictionary<string, object> ExceptInfo = new Dictionary<string, object>();
            ExceptInfo["DisciplineTypeID"] = DisciplineType.DisciplineTypeID;

            bool duplicate = this.repository.ExistsRow(GlobalConstants.LIST_SCHEMA, "DisciplineType", SearchInfo, ExceptInfo);
            if (duplicate)
            {
                List<object> Params = new List<object>();
                Params.Add("DisciplineType_Label_Resolution");
                throw new BusinessException("Common_Validate_Duplicate", Params);
            }
            // Kiem tra ID da ton tai trong he thong
            DisciplineTypeBusiness.CheckAvailable(DisciplineType.DisciplineTypeID, "DisciplineType_Label_Title", true);
            // Cap nhat vao CSDL  
            return base.Update(DisciplineType);
        }

        /// <summary>
        /// Xoa hinh thuc ky luat
        /// </summary>
        /// <param name="DisciplineTypeID"></param>
        public void Delete(int DisciplineTypeID)
        {
            // Kiem tra da ton tai trong  CSDL
            this.CheckAvailable(DisciplineTypeID, "DisciplineType_Label_Title", true);
            this.CheckConstraints(GlobalConstants.LIST_SCHEMA, "DisciplineType", DisciplineTypeID, "DisciplineType_Label_FailedDelete");
            // Xoa bang cach thiet lap IsActive = 0;
            this.Delete(DisciplineTypeID, true);
        }

        /// <summary>
        /// Tim kiem hinh thuc ky luat
        /// </summary>
        /// <param name="dicParam"></param>
        /// <returns></returns>
        public IQueryable<DisciplineType> Search(IDictionary<string, object> dicParam)
        {
            string Resolution = Utils.GetString(dicParam, "Resolution");
            int SchoolID = Utils.GetInt(dicParam, "SchoolID");
            bool? IsActive = Utils.GetNullableBool(dicParam, "IsActive");
            IQueryable<DisciplineType> listDisciplineType = this.DisciplineTypeRepository.All;
            if (!Resolution.Equals(string.Empty))
            {
                listDisciplineType = listDisciplineType.Where(x => x.Resolution.ToLower().Contains(Resolution.ToLower()));
            }
            if (IsActive.HasValue)
            {
                listDisciplineType = listDisciplineType.Where(x => x.IsActive == true);
            }
            if (SchoolID != 0)
            {
                listDisciplineType = listDisciplineType.Where(x => x.SchoolID == SchoolID || x.SchoolID == null);
            }
            //int count = listDisciplineType != null ? listDisciplineType.Count() : 0;
            //if (count == 0)
            //{
            //    return null;
            //}
            return listDisciplineType;

        }

        /// <summary>
        /// Tim kiem theo thong tin truong hoc
        /// </summary>
        /// <param name="SchoolID"></param>
        /// <param name="SearchInfo"></param>
        /// <returns></returns>
        public IQueryable<DisciplineType> SearchBySchool(int SchoolID, IDictionary<string, object> SearchInfo)
        {
            if (SchoolID == 0)
            {
                return null;
            }
            SearchInfo["SchoolID"] = SchoolID;
            return this.Search(SearchInfo);
        }
        
    }
}