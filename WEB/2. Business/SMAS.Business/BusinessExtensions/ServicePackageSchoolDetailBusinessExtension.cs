﻿/**
 * The Viettel License
 *
 * Copyright 2012 Viettel Telecom. All rights reserved.
 * VIETTEL PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */

/** 
* @author  
* @version $Revision: $
*/

using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Linq.Expressions;
using System.Web;
using log4net;

using SMAS.DAL.Repository;
using SMAS.DAL.IRepository;
using SMAS.Business.IBusiness;
using SMAS.Models.Models;
using SMAS.Business.Common;
using SMAS.Business.BusinessObject;
using System.Transactions;

namespace SMAS.Business.Business
{
    public partial class ServicePackageSchoolDetailBusiness
    {
        public IQueryable<SMS_SERVICE_PACKAGE_UNIT> Search(Dictionary<string, object> dic)
        {
            int servicePackageID = Utils.GetInt(dic, "ServicePackageID");
            int schoolID = Utils.GetInt(dic, "SchoolID");
            List<int> lstSchoolIDCheck = Utils.GetIntList(dic, "LstSchoolID");
            IQueryable<SMS_SERVICE_PACKAGE_UNIT> schoolDetail = this.All;

            if (servicePackageID != 0)
            {
                schoolDetail = schoolDetail.Where(p => p.SERVICE_PACKAGE_ID == servicePackageID);
            }

            if (schoolID > 0)
            {
                schoolDetail = schoolDetail.Where(p => p.UNIT_ID == schoolID && p.PACKAGE_UNIT_TYPE == GlobalConstantsEdu.SERVICE_PACKAGE_APPLY_TYPE_SCHOOL);
            }

            if (lstSchoolIDCheck.Count > 0)
            {
                schoolDetail = schoolDetail.Where(p => lstSchoolIDCheck.Contains(p.UNIT_ID) && p.PACKAGE_UNIT_TYPE == GlobalConstantsEdu.SERVICE_PACKAGE_APPLY_TYPE_SCHOOL);
            }
            return schoolDetail;
        }

        public int CountSchoolApply(int ServicePackageID, int type)
        {
            int total = 0;
            SMS_SERVICE_PACKAGE sp = this.ServicePackageBusiness.Find(ServicePackageID);
            if (sp != null)
            {
                if (type == 1)
                {
                    total = this.All.Where(o => o.SERVICE_PACKAGE_ID == ServicePackageID && o.PACKAGE_UNIT_TYPE == GlobalConstantsEdu.SERVICE_PACKAGE_APPLY_TYPE_SCHOOL).Count();
                }
                else
                {
                    total = this.All.Where(o => o.SERVICE_PACKAGE_ID == ServicePackageID && o.PACKAGE_UNIT_TYPE == GlobalConstantsEdu.SERVICE_PACKAGE_APPLY_TYPE_PROVINCE).Count();
                }
            }

            return total;
        }

        /// <summary>
        /// Áp dụng theo điều kiện tìm kiếm
        /// </summary>
        /// <param name="dic"></param>
        public void Apply(Dictionary<string, object> dic)
        {
            int ServicePackageID = Utils.GetInt(dic, "ServicePackageID");
            List<int> lstSchoolID = SchoolProfileBusiness.GetListSchoolID(dic);
            List<int> lstSchoolIDApplied = this.Search(dic).Select(o => o.UNIT_ID).ToList();
            List<int> lstSchoolIDApply = lstSchoolID.Except(lstSchoolIDApplied).ToList();

            //Thuc hien xoa het thong tin ap dung cho tinh
            List<SMS_SERVICE_PACKAGE_UNIT> lstAppliedProvince = this.All.Where(o => o.PACKAGE_UNIT_TYPE == GlobalConstantsEdu.SERVICE_PACKAGE_APPLY_TYPE_PROVINCE
                                                                                && o.SERVICE_PACKAGE_ID == ServicePackageID).ToList();
            this.DeleteAll(lstAppliedProvince);

            // Anhvd9 20160314 - Bổ sung thông tin
            List<int> lstSchoolIDExist = this.SchoolProfileBusiness.All.Where(o => lstSchoolIDApply.Contains(o.SchoolProfileID)).Select(o => o.SchoolProfileID).ToList();
            List<int> lstSchoolIDIns = lstSchoolIDApply.Except(lstSchoolIDExist).ToList();
            if (lstSchoolIDIns.Count > 0)
            {
                int total = 0;
                dic["LstSchoolID"] = lstSchoolIDIns;
                dic["page"] = 1;
                dic["numRecord"] = lstSchoolIDApply.Count;
                SchoolProfileBusiness.GetListSchool(dic, ref total);
            }
            // End 20160314

            SMS_SERVICE_PACKAGE_UNIT entity = null;
            for (int i = lstSchoolIDApply.Count - 1; i >= 0; i--)
            {
                entity = new SMS_SERVICE_PACKAGE_UNIT();
                entity.PACKAGE_UNIT_TYPE = GlobalConstantsEdu.SERVICE_PACKAGE_APPLY_TYPE_SCHOOL;
                entity.UNIT_ID = lstSchoolIDApply[i];
                entity.SERVICE_PACKAGE_ID = ServicePackageID;
                this.Insert(entity);
            }
            this.Save();
        }

        /// <summary>
        /// Áp dụng theo danh sách id
        /// </summary>
        /// <param name="ServicePackageID"></param>
        /// <param name="lstSchoolIDAdd"></param>
        /// <param name="lstSchoolIDRemove"></param>
        public void Apply(int ServicePackageID, List<int> lstSchoolIDAdd, List<int> lstSchoolIDRemove)
        {
            SMS_SERVICE_PACKAGE sp = this.ServicePackageBusiness.Find(ServicePackageID);

            //Thuc hien xoa het thong tin ap dung cho tinh
            List<SMS_SERVICE_PACKAGE_UNIT> lstAppliedProvince = this.All.Where(o => o.PACKAGE_UNIT_TYPE == GlobalConstantsEdu.SERVICE_PACKAGE_APPLY_TYPE_PROVINCE
                                                                                && o.SERVICE_PACKAGE_ID == ServicePackageID).ToList();
            this.ServicePackageSchoolDetailBusiness.DeleteAll(lstAppliedProvince);

            IQueryable<SMS_SERVICE_PACKAGE_UNIT> iq = this.All.Where(o => o.SERVICE_PACKAGE_ID == ServicePackageID && o.PACKAGE_UNIT_TYPE == GlobalConstantsEdu.SERVICE_PACKAGE_APPLY_TYPE_SCHOOL);
            if (lstSchoolIDAdd != null && lstSchoolIDAdd.Count > 0)
            {
                List<int> lstSchoolIDApplied = iq.Select(o => o.UNIT_ID).ToList();
                List<int> lstSchoolIDApply = lstSchoolIDAdd.Except(lstSchoolIDApplied).ToList();
                SMS_SERVICE_PACKAGE_UNIT entity = null;
                for (int i = lstSchoolIDApply.Count - 1; i >= 0; i--)
                {
                    entity = new SMS_SERVICE_PACKAGE_UNIT();
                    entity.UNIT_ID = lstSchoolIDApply[i];
                    entity.PACKAGE_UNIT_TYPE = GlobalConstantsEdu.SERVICE_PACKAGE_APPLY_TYPE_SCHOOL;
                    entity.SERVICE_PACKAGE_ID = ServicePackageID;
                    this.Insert(entity);
                }
            }

            if (lstSchoolIDRemove != null && lstSchoolIDRemove.Count > 0)
            {
                List<SMS_SERVICE_PACKAGE_UNIT> lstSchoolDetail = iq.Where(o => lstSchoolIDRemove.Contains(o.UNIT_ID)).ToList();
                this.DeleteAll(lstSchoolDetail);
            }

            this.Save();
        }
    }
}
