/**
 * The Viettel License
 *
 * Copyright 2012 Viettel Telecom. All rights reserved.
 * VIETTEL PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */

/** 
* @author  
* @version $Revision: $
*/

using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Linq.Expressions;
using System.Web;
using log4net;

using SMAS.DAL.Repository;
using SMAS.DAL.IRepository;
using SMAS.Business.IBusiness;
using SMAS.Models.Models;
using SMAS.Business.Common;

namespace SMAS.Business.Business
{
    public partial class ExemptedObjectTypeBusiness
    {

        /// <summary>
        /// Tim kiem doi tuong mien giam
        /// </summary>
        /// <param name="dicParam"></param>
        /// <returns></returns>
        public IQueryable<ExemptedObjectType> Search(IDictionary<string, object> dicParam)
        {
            string Resolution = Utils.GetString(dicParam, "Resolution");
            bool? IsActive = Utils.GetIsActive(dicParam, "IsActive");
            IQueryable<ExemptedObjectType> listExemptedObjectType = this.ExemptedObjectTypeRepository.All;
            if (!Resolution.Equals(string.Empty))
            {
                listExemptedObjectType = listExemptedObjectType.Where(x => x.Resolution.ToLower().Contains(Resolution.ToLower()));
            }
            if (IsActive.HasValue)
            {
                listExemptedObjectType = listExemptedObjectType.Where(x => x.IsActive == IsActive);
            }
            return listExemptedObjectType;

        }
        public override ExemptedObjectType Update(ExemptedObjectType entity)
        {
            this.CheckAvailable(entity.ExemptedObjectTypeID, "ExemptedObjectType_Label_ExemptedObjectType");
            this.CheckDuplicate(entity.Resolution, GlobalConstants.LIST_SCHEMA, "ExemptedObjectType", "Resolution", true, entity.ExemptedObjectTypeID, "ExemptedObjectType_Label_Resolution");
            return base.Update(entity);
        }
        public override ExemptedObjectType Insert(ExemptedObjectType entity)
        {

            this.CheckDuplicate(entity.Resolution, GlobalConstants.LIST_SCHEMA, "ExemptedObjectType", "Resolution", true, entity.ExemptedObjectTypeID, "ExemptedObjectType_Label_Resolution");
            return base.Insert(entity);
        }
        public void Delete(int id)
        {
            this.CheckAvailable(id, "ExemptedObjectType_Label_ExemptedObjectType");
            this.CheckConstraints("ExemptedObjectType_Label_ExemptedObjectType", "ExemptedObjectType", id, "ExemptedObjectType_Label_ExemptedObjectType");
            base.Delete(id, true);
        }
    }
}