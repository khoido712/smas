/**
 * The Viettel License
 *
 * Copyright 2012 Viettel Telecom. All rights reserved.
 * VIETTEL PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */

/** 
* @author  
* @version $Revision: $
*/

using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Linq.Expressions;
using System.Web;
using log4net;

using SMAS.DAL.Repository;
using SMAS.DAL.IRepository;
using SMAS.Business.IBusiness;
using SMAS.Models.Models;
using SMAS.Business.Common;
using SMAS.Business.BusinessObject;
using System.Transactions;

namespace SMAS.Business.Business
{
    public partial class EmployeeEvaluationBusiness
    {
        public void InsertOrUpdateEmployeeEvaluation(List<EmployeeEvaluation> lstTeacherGrading, IDictionary<string, object> dic)
        {
            try
            {
                EmployeeEvaluationBusiness.SetAutoDetectChangesEnabled(false);
                int SchoolID = Utils.GetInt(dic, "SchoolID");
                int AcademicYearID = Utils.GetInt(dic, "AcademicYearID");
                List<int> lstTeacherID = lstTeacherGrading.Select(p => p.EmployeeId).Distinct().ToList();
                List<int> lstEvaluationField = lstTeacherGrading.Select(p => p.EvaluaionFieldId).Distinct().ToList();
                //lay danh sach danh gia giao vien
                List<EmployeeEvaluation> lstTeacherGradingDB = (from tg in EmployeeEvaluationBusiness.All
                                                                where tg.SchoolId == SchoolID
                                                                && tg.AcademicYearId == AcademicYearID
                                                                && lstTeacherID.Contains(tg.EmployeeId)
                                                                && lstEvaluationField.Contains(tg.EvaluaionFieldId)
                                                                select tg).ToList();
                EmployeeEvaluationBusiness.DeleteAll(lstTeacherGradingDB);
                lstTeacherGrading = lstTeacherGrading.Where(p => p.EvaluationLevelId > 0 || !string.IsNullOrEmpty(p.EvaluationText)).ToList();
                for (int i = 0; i < lstTeacherGrading.Count; i++)
                {
                    EmployeeEvaluationBusiness.Insert(lstTeacherGrading[i]);
                }
                EmployeeEvaluationBusiness.Save();
            }
            catch (Exception ex)
            {
                logger.Error(ex.Message, ex);
            }
            finally
            {
                EmployeeEvaluationBusiness.SetAutoDetectChangesEnabled(true);
            }
        }

        public List<EmployeeEvaluationBO> GetListEmployeeEvaluation(IDictionary<string,object> dic)
        {
            int SchoolID = Utils.GetInt(dic, "SchoolID");
            int AcademicYearID = Utils.GetInt(dic, "AcademicYearID");
            int EmployeeID = Utils.GetInt(dic, "EmployeeID");
            List<EmployeeEvaluationBO> lstResult = new List<EmployeeEvaluationBO>();
            lstResult = (from ee in EmployeeEvaluationBusiness.All
                         join ef in EvaluationFieldBusiness.All on ee.EvaluaionFieldId equals ef.EvaluationFieldId
                         join el in EvaluationLevelBusiness.All on ee.EvaluationLevelId equals el.EvaluationLevelId into l
                         from l1 in l.DefaultIfEmpty()
                         where ee.SchoolId == SchoolID
                         && ee.AcademicYearId == AcademicYearID
                         && (ee.EmployeeId == EmployeeID || EmployeeID == 0)
                         select new EmployeeEvaluationBO
                         {
                             EmployeeID = ee.EmployeeId,
                             EvaluationFieldID = ee.EvaluaionFieldId,
                             EvaluationFieldName = ef.Resolution,
                             EvaluationLevelName = l1.Resolution,
                             FieldType = ef.FieldType,
                             EvaluationText = ee.EvaluationText
                         }).ToList();
            return lstResult;
        }

        public List<EmployeeEvaluationBO> GetListEmployeeEvaluationBySupervising(IDictionary<string, object> dic)
        {
            List<int> lstEmployeeID = Utils.GetIntList(dic, "LstEmployeeID");
            List<int> lstSchoolID = Utils.GetIntList(dic, "LstSchoolID");
            int Year = Utils.GetInt(dic, "Year");
            List<EmployeeEvaluationBO> lstResult = new List<EmployeeEvaluationBO>();
            lstResult = (from ee in EmployeeEvaluationBusiness.All
                         join ef in EvaluationFieldBusiness.All on ee.EvaluaionFieldId equals ef.EvaluationFieldId
                         join el in EvaluationLevelBusiness.All on ee.EvaluationLevelId equals el.EvaluationLevelId into l
                         from l1 in l.DefaultIfEmpty()
                         join ay in AcademicYearBusiness.All.Where(x=>x.IsActive == true) on ee.SchoolId equals ay.SchoolID
                         where lstSchoolID.Contains(ay.SchoolID)
                         && ay.Year == Year
                         && lstEmployeeID.Contains(ee.EmployeeId)
                         select new EmployeeEvaluationBO
                         {
                             EmployeeID = ee.EmployeeId,
                             EvaluationFieldID = ee.EvaluaionFieldId,
                             EvaluationFieldName = ef.Resolution,
                             EvaluationLevelName = l1.Resolution,
                             FieldType = ef.FieldType,
                             EvaluationText = ee.EvaluationText,
                             SchoolID = ee.SchoolId
                         }).ToList();
            return lstResult;
        }

        public void DeleteEmployeeEvaluation(IDictionary<string, object> dic)
        {
            int SchoolID = Utils.GetInt(dic, "SchoolID");
            int AcademicYearID = Utils.GetInt(dic, "AcademicYearID");
            List<int> lstEmployeeID = Utils.GetIntList(dic, "lstEmployeeID");
            List<EmployeeEvaluation> lstDelete = EmployeeEvaluationBusiness.All.Where(p => p.SchoolId == SchoolID && p.AcademicYearId == AcademicYearID
                                                && lstEmployeeID.Contains(p.EmployeeId)).ToList();
            EmployeeEvaluationBusiness.DeleteAll(lstDelete);
            EmployeeEvaluationBusiness.Save();
        }
    }
}
