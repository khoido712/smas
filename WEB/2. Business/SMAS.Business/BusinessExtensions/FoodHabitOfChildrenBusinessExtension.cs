/**
 * The Viettel License
 *
 * Copyright 2012 Viettel Telecom. All rights reserved.
 * VIETTEL PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */

/** 
* @author  
* @version $Revision: $
*/

using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Linq.Expressions;
using System.Web;
using log4net;

using SMAS.DAL.Repository;
using SMAS.DAL.IRepository;
using SMAS.Business.IBusiness;
using SMAS.Models.Models;
using SMAS.Business.Common;

namespace SMAS.Business.Business
{ 
    public partial class FoodHabitOfChildrenBusiness
    {
        #region Search


        /// <summary>       
        /// </summary>
        /// <author>trangdd</author>
        /// <date>06/11/2012</date>    
        /// <returns>IQueryable<Ethnic></returns>
        public IQueryable<FoodHabitOfChildren> Search(IDictionary<string, object> dic)
        {
            int HabitType = Utils.GetInt(dic, "HabitType");
            int PupilID = Utils.GetInt(dic, "PupilID");
            int TypeOfDishID = Utils.GetInt(dic, "TypeOfDishID");
            IQueryable<FoodHabitOfChildren> Query = FoodHabitOfChildrenRepository.All;

            if (PupilID != 0)
            {
                Query = Query.Where(cfh => cfh.PupilID == PupilID);
            }
            if (HabitType != 0)
            {
                Query = Query.Where(cfh => cfh.HabitType == HabitType);
            }
            if (TypeOfDishID != 0)
            {
                Query = Query.Where(cfh => cfh.TypeOfDishID == TypeOfDishID);
            }
            return Query;

        }
        #endregion  
        
    }
}