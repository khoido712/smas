using System;
using System.Collections.Generic;
using System.Linq;
using SMAS.DAL.Repository;
using SMAS.Models.Models;
using SMAS.Business.Common;
using System.Transactions;
using System.IO;
using SMAS.VTUtils.Excel.Export;
using SMAS.Business.BusinessObject;

namespace SMAS.Business.Business
{
    public partial class RewardFinalBusiness
    {
        #region private member variable
        private const int REWARDFINALMODE_MAX_LENGTH = 300;   //do dai lon nhat truong khen thuong
        private const int NOTE_MAX_LENGTH = 500; //do dai lon nhat truong ghi chu
        #endregion
        
        #region insert
        public override RewardFinal Insert(RewardFinal insertRewardFinal)
        {
            //Hinh thuc khen thuong rong
            Utils.ValidateRequire(insertRewardFinal.RewardMode, "RewardFinal_Control_Resolution");
            Utils.ValidateMaxLength(insertRewardFinal.RewardMode, REWARDFINALMODE_MAX_LENGTH, "RewardFinal_Control_Resolution");
            Utils.ValidateMaxLength(insertRewardFinal.Note, NOTE_MAX_LENGTH, "RewardFinal_Label_Note");

            //kiem tra ton tai - theo cap 
            Dictionary<string, object> SearchInfo = new Dictionary<string, object>();
            SearchInfo["SchoolID"] = insertRewardFinal.SchoolID;
            SearchInfo["RewardMode"] = insertRewardFinal.RewardMode.ToUpper();

            IDictionary<string, object> ExceptInfo = new Dictionary<string, object>();
            ExceptInfo["RewardFinalID"] = insertRewardFinal.RewardFinalID;

            bool duplicate = RewardFinalBusiness.All
                .Where(p => p.SchoolID == insertRewardFinal.SchoolID 
                    && insertRewardFinal.RewardMode.ToUpper()
                    .Equals(p.RewardMode.ToUpper())).Count() > 0; //this.repository.ExistsRow("SMAS3", "RewardFinal", SearchInfo, ExceptInfo);
            if (duplicate)
            {
                //List<object> Params = new List<object>();
                //Params.Add("RewardFinal_Control_Resolution");
                throw new BusinessException("Reward_Validate_Exist_NameOfRewardFinal");
            }
            SchoolProfileBusiness.CheckAvailable(insertRewardFinal.SchoolID, "UserInfo_SchoolID");
            return base.Insert(insertRewardFinal);
        }
        #endregion

        #region update
        public override RewardFinal Update(RewardFinal updateRewardFinal)
        {
            //check avai
            new RewardFinalBusiness(null).CheckAvailable(updateRewardFinal.RewardFinalID, "RewardFinal_Label_RewardFinalID");
            ValidationMetadata.ValidateObject(updateRewardFinal);
            //Hinh thuc khen thuong rong
            Utils.ValidateRequire(updateRewardFinal.RewardMode, "RewardFinal_Control_Resolution");
            Utils.ValidateMaxLength(updateRewardFinal.RewardMode, REWARDFINALMODE_MAX_LENGTH, "RewardFinal_Control_Resolution");
            Utils.ValidateMaxLength(updateRewardFinal.Note, NOTE_MAX_LENGTH, "RewardFinal_Label_Note");
            //kiem tra ton tai - theo cap - can lam lai
            //this.CheckDuplicate(updateRewardFinal.RewardMode, GlobalConstants.LIST_SCHEMA, "RewardFinal", "RewardFinalMode", updateExperienceType.ExperienceTypeID, "RewardFinal_Control_Resolution");
            Dictionary<string, object> SearchInfo = new Dictionary<string, object>();
            SearchInfo["SchoolID"] = updateRewardFinal.SchoolID;
            SearchInfo["RewardMode"] = updateRewardFinal.RewardMode;

            IDictionary<string, object> ExceptInfo = new Dictionary<string, object>();
            ExceptInfo["RewardFinalID"] = updateRewardFinal.RewardFinalID;
            //xet trung ten
            if (this.RewardFinalRepository.ExistsRow(GlobalConstants.LIST_SCHEMA, "RewardFinal", SearchInfo, ExceptInfo))
            {
                throw new BusinessException("Reward_Validate_Exist_NameOfRewardFinal");
            }

            //bool duplicate = RewardFinalBusiness.All
            //    .Where(p => p.SchoolID == updateRewardFinal.SchoolID
            //        && updateRewardFinal.RewardMode.ToUpper()
            //        .Equals(p.RewardMode.ToUpper())).Count() > 0; // this.repository.ExistsRow(GlobalConstants.LIST_SCHEMA, "RewardFinal", SearchInfo, ExceptInfo);
            //if (duplicate)
            //{
            //    //List<object> Params = new List<object>();
            //    //Params.Add("RewardFinal_Control_Resolution");
            //    throw new BusinessException("Reward_Validate_Exist_NameOfRewardFinal");
            //}
            //check 
            SchoolProfileBusiness.CheckAvailable(updateRewardFinal.SchoolID, "UserInfo_SchoolID");
            //Loi pham quy che thi khong thuoc truong truyen vao
            //tu RewardFinalID ->vao csdl lay RewardFinal tuong ung roi 
            //so sanh voi updateRewardFinal.SchoolID
            //.................
            return base.Update(updateRewardFinal);
        }
        #endregion

        #region Search
        public IQueryable<RewardFinal> Search(IDictionary<string, object> dic)
        {
            string rewardFinalMode = Utils.GetString(dic, "RewardFinal");
            string note = Utils.GetString(dic, "Note");
            int SchoolID = Utils.GetInt(dic, "SchoolID");
            IQueryable<RewardFinal> lsRewardFinal = this.RewardFinalRepository.All;

            if (SchoolID != 0)
            {
                lsRewardFinal = lsRewardFinal.Where(em => (em.SchoolID == SchoolID));
            }

            if (!string.IsNullOrEmpty(rewardFinalMode.Trim()))
            {
                lsRewardFinal = lsRewardFinal.Where(em => (em.RewardMode.ToUpper().Contains(rewardFinalMode.ToUpper())));
            }

            
            return lsRewardFinal;
        }
        #endregion
    }
}
