﻿/**
 * The Viettel License
 *
 * Copyright 2012 Viettel Telecom. All rights reserved.
 * VIETTEL PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */

/** 
* @author  
* @version $Revision: $
*/

using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Linq.Expressions;
using System.Web;
using SMAS.Business.Common;
using SMAS.Models.Models;
using SMAS.VTUtils.Log;

namespace SMAS.Business.Business
{
    public partial class VPupilRankingBusiness
    {
        /// <summary>
        /// Quanglm 
        /// Tim kiem theo truong
        /// </summary>
        /// <param name="SchoolID"></param>
        /// <param name="dic"></param>
        /// <returns></returns>
        public IQueryable<VPupilRanking> SearchBySchool(int SchoolID, IDictionary<string, object> dic)
        {
            if (SchoolID == 0)
                return null;
            dic["SchoolID"] = SchoolID;
            return this.Search(dic);
        }


        public void UpdateRank(int schoolId, bool isMovedHistory, IDictionary<string, object> dic)
        {
            string paramList = "";
            try
            {
                //SetAutoDetectChangesEnabled(false);

                int partitionId = UtilsBusiness.GetPartionId(schoolId);
                int ClassID = Utils.GetInt(dic, "ClassID");
                int Semester = Utils.GetByte(dic, "Semester");
                int? PeriodID = Utils.GetNullInt(dic, "PeriodID");
                int AcademicYearID = Utils.GetInt(dic, "AcademicYearID");
                List<int> lstPupilid = Utils.GetIntList(dic, "lstPupilId");

                paramList = string.Format("Xep hang hoc sinh: SchoolID={0},AcademicYearID={1}, ClassID={2},Semester={3}, periodId={4}", schoolId, AcademicYearID, ClassID, Semester, PeriodID);

                AcademicYear objAcademicYear = AcademicYearBusiness.Find(AcademicYearID);
                IQueryable<PupilOfClass> iqPoc = PupilOfClassBusiness.SearchBySchool(schoolId, dic);
                if (isMovedHistory)
                {
                    IQueryable<PupilRankingHistory> IqPrHistory = PupilRankingHistoryBusiness.SearchBySchool(schoolId, dic);
                    IQueryable<PupilRankingHistory> iqHisUpdate = from pr in IqPrHistory
                                                                  join pc in iqPoc on pr.PupilID equals pc.PupilID
                                                                  select pr;
                    List<PupilRankingHistory> lstUpdate = new List<PupilRankingHistory>();
                    if (lstPupilid != null && lstPupilid.Count > 0)
                    {
                        lstUpdate = iqHisUpdate.Where(p => lstPupilid.Contains(p.PupilID)).ToList();
                    }
                    PupilRankingHistoryBusiness.Rank(objAcademicYear.RankingCriteria, lstUpdate, false);
                    PupilRankingHistory objPupilRankingHistory = null;
                    for (int i = 0; i < lstUpdate.Count; i++)
                    {
                        objPupilRankingHistory = lstUpdate[i];
                        objPupilRankingHistory.SynchronizeID = -1;
                        objPupilRankingHistory.MSourcedb = GlobalConstants.PUPIL_RANKIG_AUTO;
                        PupilRankingHistoryBusiness.Update(objPupilRankingHistory);
                    }
                    PupilRankingHistoryBusiness.Save();
                }
                else
                {
                    IQueryable<PupilRanking> IqPr = PupilRankingBusiness.SearchBySchool(schoolId, dic);
                    IQueryable<PupilRanking> iqUpdate = from pr in IqPr
                                                        join pc in iqPoc on pr.PupilID equals pc.PupilID
                                                        select pr;
                    List<PupilRanking> lstUpdate = new List<PupilRanking>();
                    if (lstPupilid != null && lstPupilid.Count > 0)
                    {
                        lstUpdate = iqUpdate.Where(p => lstPupilid.Contains(p.PupilID)).ToList();
                    }

                    PupilRankingBusiness.Rank(objAcademicYear.RankingCriteria, lstUpdate, false);
                    PupilRanking objPupilRanking = null;
                    for (int i = 0; i < lstUpdate.Count; i++)
                    {
                        objPupilRanking = lstUpdate[i];
                        objPupilRanking.SynchronizeID = -1;
                        objPupilRanking.MSourcedb = GlobalConstants.PUPIL_RANKIG_AUTO;
                        PupilRankingBusiness.Update(objPupilRanking);
                    }
                    PupilRankingBusiness.Save();
                }


            }
            finally
            {
                //SetAutoDetectChangesEnabled(true);
                LogExtensions.InfoExt(logger, LogExtensions.LOG_TYPE_INFO, DateTime.Now, paramList, "", "");
            }
        }


        public void UpdateRankPeriod(Dictionary<int, bool> dicPupilNotConductRank, List<PupilRanking> lstPupilRanking, AcademicYear academicYear)
        {
            try
            {
                SetAutoDetectChangesEnabled(false);
                if (UtilsBusiness.IsMoveHistory(academicYear))
                {
                    List<PupilRankingHistory> lstHistory = PupilRankingHistoryBusiness.ConvertListToHistory(lstPupilRanking);
                    // Thuc hien luu vao db
                    foreach (PupilRankingHistory item in lstHistory)
                    {
                        if (dicPupilNotConductRank.ContainsKey(item.PupilID) && dicPupilNotConductRank[item.PupilID])
                        {
                            item.ConductLevelID = null;
                        }
                        if (item.PupilRankingID == 0)
                        {
                            PupilRankingHistoryBusiness.Insert(item);
                        }
                        else
                        {
                            if (!item.ConductLevelID.HasValue && !item.CapacityLevelID.HasValue && item.Rank.HasValue)
                            {
                                item.Rank = null;
                            }
                            PupilRankingHistoryBusiness.Update(item);
                        }
                    }
                    PupilRankingHistoryBusiness.Save();
                }
                else
                {
                    // Thuc hien luu vao db
                    foreach (PupilRanking item in lstPupilRanking)
                    {
                        if (dicPupilNotConductRank.ContainsKey(item.PupilID) && dicPupilNotConductRank[item.PupilID])
                        {
                            item.ConductLevelID = null;
                        }
                        if (item.PupilRankingID == 0)
                        {
                            PupilRankingBusiness.Insert(item);
                        }
                        else
                        {
                            if (!item.ConductLevelID.HasValue && !item.CapacityLevelID.HasValue && item.Rank.HasValue)
                            {
                                item.Rank = null;
                            }
                            PupilRankingBusiness.Update(item);
                        }
                    }
                    PupilRankingHistoryBusiness.Save();

                }

            }
            catch (Exception ex)
            {

                throw ex;
            }
            finally
            {
                SetAutoDetectChangesEnabled(true);
            }
        }

        /// <summary>
        /// Quanglm
        /// Tim kiem chi tiet
        /// </summary>
        /// <param name="dic"></param>
        /// <returns></returns>
        private IQueryable<VPupilRanking> Search(IDictionary<string, object> dic)
        {
            int SchoolID = Utils.GetInt(dic, "SchoolID");
            int partitionId = UtilsBusiness.GetPartionId(SchoolID);
            IQueryable<VPupilRanking> lsPupilRanking = this.VPupilRankingBusiness.AllNoTracking.Where(c => c.Last2digitNumberSchool == partitionId);
            int StudyingJudgementID = Utils.GetInt(dic, "StudyingJudgementID");
            int PupilRankingID = Utils.GetInt(dic, "PupilRankingID");
            int PupilID = Utils.GetInt(dic, "PupilID");
            int ClassID = Utils.GetInt(dic, "ClassID");
            List<int> lstPupilID = Utils.GetIntList(dic, "lstPupilID");
            int AcademicYearID = Utils.GetInt(dic, "AcademicYearID");
            int AppliedLevel = Utils.GetByte(dic, "AppliedLevel");
            int EducationLevelID = Utils.GetInt(dic, "EducationLevelID");
            int Semester = Utils.GetByte(dic, "Semester");
            int? PeriodID = Utils.GetNullInt(dic, "PeriodID");
            decimal? AverageMark = Utils.GetNullableDecimal(dic, "AverageMark");
            int? TotalAbsentDaysWithPermission = Utils.GetNullableByte(dic, "TotalAbsentDaysWithPermission");
            int? TotalAbsentDaysWithoutPermission = Utils.GetNullableByte(dic, "TotalAbsentDaysWithoutPermission");
            int CapacityLevelID = Utils.GetInt(dic, "CapacityLevelID");
            int ConductLevelID = Utils.GetInt(dic, "ConductLevelID");
            int? Rank = Utils.GetNullableInt(dic, "Rank");
            DateTime? RankingDay = Utils.GetDateTime(dic, "RankingDay");
            bool? IsCategory = Utils.GetNullableBool(dic, "IsCategory");
            string checkWithClassMovement = Utils.GetString(dic, "checkWithClassMovement");

            if (PupilRankingID != 0)
                lsPupilRanking = lsPupilRanking.Where(em => (em.PupilRankingID == PupilRankingID));
            if (PupilID != 0)
                lsPupilRanking = lsPupilRanking.Where(em => (em.PupilID == PupilID));
            if (lstPupilID.Count > 0)
                lsPupilRanking = lsPupilRanking.Where(em => lstPupilID.Contains(em.PupilID));
            if (ClassID != 0)
                lsPupilRanking = lsPupilRanking.Where(em => (em.ClassID == ClassID));
            if (AcademicYearID != 0)
            {
                lsPupilRanking = lsPupilRanking.Where(em => (em.AcademicYearID == AcademicYearID));

                var aca = AcademicYearBusiness.Find(AcademicYearID);
                if (aca == null) throw new BusinessException("Năm học không tồn tại");
                int createAcaYear = aca.Year;
                lsPupilRanking = lsPupilRanking.Where(o => (o.CreatedAcademicYear == createAcaYear));
                //lsPupilRanking = lsPupilRanking.Where(o => (o.Last2digitNumberSchool == aca.SchoolID % 100));

            }
            if (AppliedLevel != 0)
            {
                IQueryable<ClassProfile> IqClassProfile = ClassProfileBusiness.All.Where(c => c.IsActive == true);
                lsPupilRanking = lsPupilRanking.Where(o => IqClassProfile.Any(c => c.ClassProfileID == o.ClassID && c.EducationLevel.Grade == AppliedLevel));
            }
            if (EducationLevelID != 0)
            {
                IQueryable<ClassProfile> IqClassProfile = ClassProfileBusiness.All.Where(c => c.IsActive == true);
                lsPupilRanking = lsPupilRanking.Where(o => IqClassProfile.Any(c => c.ClassProfileID == o.ClassID && c.EducationLevelID == EducationLevelID));
            }

            if (Semester != 0)
            {
                // fix loi 0014575	: voi bao cao cuoi nam thi lay ca thong tin sau thi lai va ren luyen lai luon
                if (Semester == SystemParamsInFile.SEMESTER_OF_YEAR_ALL)
                {
                    lsPupilRanking = lsPupilRanking.Where(em => (em.Semester >= Semester));
                }
                else
                {
                    lsPupilRanking = lsPupilRanking.Where(em => (em.Semester == Semester));
                }
            }
            if (checkWithClassMovement.Length == 0)
            {
                if (PeriodID.HasValue)
                {
                    if (PeriodID != 0)
                    {
                        lsPupilRanking = lsPupilRanking.Where(em => (em.PeriodID == PeriodID));
                    }
                    else
                    {
                        lsPupilRanking = lsPupilRanking.Where(em => !em.PeriodID.HasValue);
                    }
                }
                else
                {
                    lsPupilRanking = lsPupilRanking.Where(em => !em.PeriodID.HasValue);
                }
            }
            if (AverageMark != null)
            {
                lsPupilRanking = lsPupilRanking.Where(em => (em.AverageMark == AverageMark));
            }
            if (TotalAbsentDaysWithPermission != null)
            {
                lsPupilRanking = lsPupilRanking.Where(em => (em.TotalAbsentDaysWithPermission == TotalAbsentDaysWithPermission));
            }
            if (TotalAbsentDaysWithoutPermission != null)
            {
                lsPupilRanking = lsPupilRanking.Where(em => (em.TotalAbsentDaysWithoutPermission == TotalAbsentDaysWithoutPermission));
            }
            if (CapacityLevelID != 0)
            {
                lsPupilRanking = lsPupilRanking.Where(em => (em.CapacityLevelID == CapacityLevelID));
            }
            if (ConductLevelID != 0)
            {
                lsPupilRanking = lsPupilRanking.Where(em => (em.ConductLevelID == ConductLevelID));
            }
            if (Rank != null)
            {
                lsPupilRanking = lsPupilRanking.Where(em => (em.Rank == Rank));
            }
            if (RankingDay != null)
            {
                lsPupilRanking = lsPupilRanking.Where(em => (em.RankingDate == RankingDay));
            }
            if (IsCategory.HasValue)
            {
                lsPupilRanking = lsPupilRanking.Where(em => (em.IsCategory == IsCategory.Value));
            }
            if (StudyingJudgementID != 0)
            {
                lsPupilRanking = lsPupilRanking.Where(o => o.StudyingJudgementID == StudyingJudgementID);
            }


            return lsPupilRanking;

        }
    }
}