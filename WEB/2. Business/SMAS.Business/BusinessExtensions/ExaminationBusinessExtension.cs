﻿/**
 * The Viettel License
 *
 * Copyright 2012 Viettel Telecom. All rights reserved.
 * VIETTEL PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */

/**
* @author dungnt
* @version $Revision: $
*/

using System;
using System.Collections.Generic;
using System.Linq;
using SMAS.Business.Common;
using SMAS.DAL.Repository;
using SMAS.Models.Models;
using SMAS.Business.BusinessObject;
using System.Text;

namespace SMAS.Business.Business
{
    /// <summary>
    /// <Author>dungnt</Author>
    /// <DateTime>06/11/2012</DateTime>
    /// </summary>
    public partial class ExaminationBusiness
    {
        #region variable

        public List<int> lsAppliedLevel = new List<int>() { 1, 2, 3 };
        public List<int> lsUsingSeparateList = new List<int>() { 0, 1 };

        #endregion variable

        #region Validate

        /// <summary>
        /// Validate đối tượng trước khi insert/update
        /// </summary>
        /// <Author>dungnt</Author>
        /// <DateTime>06/11/2012</DateTime>
        /// <param name="exam"></param>
        private void Validate(Examination exam)
        {
            ValidationMetadata.ValidateObject(exam);

            DateTime examFromDate = new DateTime(exam.FromDate.Value.Year, exam.FromDate.Value.Month, exam.FromDate.Value.Day, 00, 00, 00);
            DateTime examEndDate = new DateTime(exam.ToDate.Value.Year, exam.ToDate.Value.Month, exam.ToDate.Value.Day, 23, 59, 59);

            //AcademicYearID: PK(AcademicYear)
            {
                bool Exist = AcademicYearRepository.ExistsRow(GlobalConstants.SCHOOL_SCHEMA, "AcademicYear",
                        new Dictionary<string, object>()
                {
                    {"AcademicYearID",exam.AcademicYearID}
                }, null);
                if (!Exist)
                {
                    throw new BusinessException("Common_Validate_NotAvailable");
                }
            }

            //SchoolID: PK(SchoolProfile)
            {
                bool Exist = SchoolProfileRepository.ExistsRow(GlobalConstants.SCHOOL_SCHEMA, "SchoolProfile",
                        new Dictionary<string, object>()
                {
                    {"SchoolProfileID",exam.SchoolID}
                }, null);
                if (!Exist)
                {
                    throw new BusinessException("Common_Validate_NotAvailable");
                }
            }

            if (!lsAppliedLevel.Contains(exam.AppliedLevel))
            {
                throw new BusinessException("Examinatin_Label_ErrorAppliedLevelNotInRange");
            }

            //AcademicYearID, SchoolID: not compatible (AcademicYear)
            if (exam.SchoolID.HasValue)
            {
                bool Compatible = AcademicYearRepository.ExistsRow(GlobalConstants.SCHOOL_SCHEMA, "AcademicYear",
                        new Dictionary<string, object>()
                {
                    {"AcademicYearID",exam.AcademicYearID},
                    {"SchoolID",exam.SchoolID.Value}
                }, null);
                if (!Compatible)
                {
                    throw new BusinessException("Common_Validate_NotCompatible");
                }
            }

            //->validate Object
            //Title: require, maxlength(200)
            //Location: maxlength(200)
            // FromDate: require

            //UsingSeparateList: in (0,1)
            if (!lsUsingSeparateList.Contains(exam.UsingSeparateList))
            {
                throw new BusinessException("Examinatin_Label_ErrorUsingSeparateListNotInRange");
            }

            //CandidateFromMultipleLevel
            if (!lsUsingSeparateList.Contains(exam.CandidateFromMultipleLevel))
            {
                throw new BusinessException("Examinatin_Label_ErrorUsingCandidateFromMultipleLevelNotInRange");
            }

            //MarkImportType
            if (!lsAppliedLevel.Contains(exam.MarkImportType))
            {
                throw new BusinessException("Examinatin_Label_ErrorMarkImportTypeNotInRange");
            }

            //FromDate <= ToDate
            if (exam.ToDate.HasValue)
            {
                Utils.ValidateAfterDate(exam.ToDate.Value, exam.FromDate.Value, "Examination_Label_ToDate", "Examination_Label_FromDate");
            }

            //Title, AcademicYearID: duplicate (Examination)
            {
                bool Exist = this.repository.ExistsRow(GlobalConstants.EXAM_SCHEMA, "Examination",
                        new Dictionary<string, object>()
                {
                    {"Title",exam.Title}
                    , {"AcademicYearID",exam.AcademicYearID}
                    , {"AppliedLevel",exam.AppliedLevel}
                    , {"IsActive",true}
                }, new Dictionary<string, object>()
                {
                    {"ExaminationID",exam.ExaminationID}
                    
                });
                if (Exist)
                {
                    List<object> Params = new List<object>();
                    Params.Add("Examination_Label_Title");
                    Params.Add("Examination_Label_AcademicYear");
                    throw new BusinessException("Common_Validate_Duplicate", Params);
                }
            }

            if (exam.FromDate.HasValue && exam.ToDate.HasValue)
            {
                // Thời gian thi phải sau ngày hiện tại
                //DateTime exFromDate = new DateTime(exam.FromDate.Value.Year, exam.FromDate.Value.Month, exam.FromDate.Value.Day, 00, 00, 00);
                //DateTime exEndDate = new DateTime(exam.ToDate.Value.Year, exam.ToDate.Value.Month, exam.ToDate.Value.Day, 23, 59, 57);
                //if (exFromDate < DateTime.Now.AddDays(-1))
                //{
                //    throw new BusinessException("Examination_Label_ErrorDate_", new List<object>() { });
                //}
                //Đã tồn tại kỳ thi nằm trong khoảng thời gian này. Kiểm tra FromDate và ToDate nằm trong khoảng khai báo của một kỳ thi
                IQueryable<Examination> lsExamination = this.repository.All.Where(o => o.AcademicYearID == exam.AcademicYearID)
                  .Where(o => o.AppliedLevel == exam.AppliedLevel)
                  .Where(o => o.SchoolID == exam.SchoolID)
                  .Where(o => o.IsActive == true);
                if(exam.ExaminationID!=0)
                {
                    lsExamination = lsExamination.Where(o => o.ExaminationID != exam.ExaminationID);
                }

                if (lsExamination.Count() > 0)
                {
                    List<Examination> list = lsExamination.ToList();
                    foreach (var item in list)
                    {
                        if (item.FromDate.HasValue && item.ToDate.HasValue)
                        {
                            //kiem tra neu da ton tai ky thi trong khoang thoi gian nay thi throw exception
                            var checkFromDate = new DateTime(item.FromDate.Value.Year, item.FromDate.Value.Month, item.FromDate.Value.Day, 0, 0, 0);
                            var checkToDate = new DateTime(item.ToDate.Value.Year, item.ToDate.Value.Month, item.ToDate.Value.Day, 23, 59, 59);
                            if (CompareDateTimeRange(examFromDate, examEndDate, checkFromDate, checkToDate))
                            {
                                throw new BusinessException("Examination_Label_DateConflict", new List<object>() { });
                            }
                        }
                    }
                }
            }

            //Thời gian thi không được ngoài khoảng thời gian năm học
            AcademicYear ay = AcademicYearRepository.Find(exam.AcademicYearID);
            if (exam.FromDate.HasValue && exam.ToDate.HasValue)
            {
                var acaFromDate = new DateTime(ay.FirstSemesterStartDate.Value.Year, ay.FirstSemesterStartDate.Value.Month, ay.FirstSemesterStartDate.Value.Day, 0, 0, 0);
                var acaToDate = new DateTime(ay.SecondSemesterEndDate.Value.Year, ay.SecondSemesterEndDate.Value.Month, ay.SecondSemesterEndDate.Value.Day, 23, 59, 59);
                if (!IsDateTimeInsideADateTimeRange(examFromDate, examEndDate, acaFromDate, acaToDate))
                {
                    throw new BusinessException("Examination_Label_OutsideOfAcademicYear", new List<object>() { });
                }
            }

            //AcademicYearBusiness.IsCurrentYear(AcademicYearID) = FALSE => Năm học không phải năm hiện tại Examination_Label_NotCurrentAcademicYear
            if (!AcademicYearBusiness.IsCurrentYear(exam.AcademicYearID))
            {
                throw new BusinessException("Examination_Label_NotCurrentAcademicYear", new List<object>() { });
            }

            //Neu ki thi da lap lich thi thi kiem tra thoi gian thi phai chua thoi gian trong lich thi
            //Chi kiem tra khi sua ki thi exam.ExaminationID != 0
            if(exam.ExaminationID != 0)
            {

                List<ExaminationSubject> lstExaminationSubject = ExaminationSubjectBusiness.SearchBySchool(exam.SchoolID.Value, new Dictionary<string, object> 
                                                                        {
                                                                            {"ExaminationID", exam.ExaminationID}
                                                                        }).ToList();
                foreach (ExaminationSubject examSubject in lstExaminationSubject)
                {
                    if (examSubject.StartTime.HasValue && (examSubject.StartTime.Value.Date < examFromDate || examSubject.StartTime.Value.Date > examEndDate))
                    {
                        throw new BusinessException("Examination_Label_ConflictExaminationSubject", new List<object>() { });
                    }
                }
            }

        }

        #endregion Validate

        #region insert

        /// <summary>
        /// Thêm mới thông tin kỳ thi
        /// <Author>dungnt</Author>
        /// <DateTime>06/11/2012</DateTime>
        /// </summary>
        /// <param name="insertExamination"></param>
        /// <returns></returns>
        public override Examination Insert(Examination insertExamination)
        {
            Validate(insertExamination);
            insertExamination.CurrentStage = SystemParamsInFile.EXAMINATION_STAGE_CREATED;
            insertExamination.IsActive = true;
            AcademicYear ay = AcademicYearRepository.Find(insertExamination.AcademicYearID);
            insertExamination.Year = ay.Year;
            return base.Insert(insertExamination);
        }

        #endregion insert

        #region update

        /// <summary>
        /// Cập nhật thông tin kỳ thi
        /// </summary>
        /// <Author>dungnt</Author>
        /// <DateTime>06/11/2012</DateTime>
        /// <param name="insertExamination"></param>
        /// <returns></returns>
        public override Examination Update(Examination insertExamination)
        {
            //ExaminationID: PK(Examination)
            {
                bool Exist = this.repository.ExistsRow(GlobalConstants.EXAM_SCHEMA, "Examination",
                        new Dictionary<string, object>()
                {
                    {"ExaminationID",insertExamination.ExaminationID}
                    ,{"IsActive",true}
                }, null);
                if (!Exist)
                {
                    throw new BusinessException("Common_Validate_NotAvailable");
                }
            }

            //ExaminationID, SchoolID: not compatible (ExaminationID)
            if (insertExamination.SchoolID.HasValue)
            {
                bool Compatible = this.repository.ExistsRow(GlobalConstants.EXAM_SCHEMA, "Examination",
                        new Dictionary<string, object>()
                {
                    {"ExaminationID",insertExamination.ExaminationID},
                    {"SchoolID",insertExamination.SchoolID.Value}
                }, null);
                if (!Compatible)
                {
                    throw new BusinessException("Common_Validate_NotCompatible");
                }
            }

            //ExaminationID, AppliedLevel: not compatible (ExaminationID)
            {
                bool Compatible = this.repository.ExistsRow(GlobalConstants.EXAM_SCHEMA, "Examination",
                        new Dictionary<string, object>()
                {
                    {"ExaminationID",insertExamination.ExaminationID},
                    {"AppliedLevel",insertExamination.AppliedLevel}
                }, null);
                if (!Compatible)
                {
                    throw new BusinessException("Common_Validate_NotCompatible");
                }
            }

            //Examination(ExaminationID). CurrentStage (lấy trong CSDL) chỉ được nhận giá trị < EXAMINATION_STAGE_MARK_COMPLETED (5)
            if (insertExamination.CurrentStage > SystemParamsInFile.EXAMINATION_STAGE_MARK_COMPLETED)
            {
                List<object> Params = new List<object>();
                Params.Add("Examination_Label_CurrentStage");
                Params.Add(SystemParamsInFile.EXAMINATION_STAGE_MARK_COMPLETED.ToString());
                throw new BusinessException("Common_Validate_CompareNumberDecimalNotEqual", Params);
            }

            //Nếu UsingSeparateList != 0 và HasSeparatedCandidateInSubject(ExaminationID) thì đưa ra thông báo:
            //“Kỳ thi đã có danh sách thí sinh từng môn thi khác nhau” -Examination_Label_HasSeparatedCandidateInSubjectError
            if (insertExamination.UsingSeparateList != 0 && HasSeparatedCandidateInSubject(insertExamination.ExaminationID))
            {
                throw new BusinessException("Examination_Label_HasSeparatedCandidateInSubjectError");
            }

            //Nếu CandidateFromMultipleLevel = 0 và HasCandidateFromMultipleLevel(ExaminationID) thì đưa ra thông báo:
            //“Kỳ thi đã có môn thi lấy thí sinh từ khối thi khác” -Examination_Label_HasCandidateFromMultipleLevelError
            if (insertExamination.CandidateFromMultipleLevel == 0 && HasCandidateFromMultipleLevel(insertExamination.ExaminationID))
            {
                throw new BusinessException("Examination_Label_HasCandidateFromMultipleLevelError");
            }
            Validate(insertExamination);

            //Lấy đối tượng Examination theo ExaminationID rồi map các thông tin tương ứng từ Entity (Ngoại trừ CurrentStage, CreatedDate, IsActive)

            return base.Update(insertExamination);
        }

        #endregion update

        #region delete

        /// <summary>
        /// Xoá thông tin kỳ thi
        /// </summary>
        /// <Author>dungnt</Author>
        /// <DateTime>06/11/2012</DateTime>
        /// <param name="SchoolID"></param>
        /// <param name="AppliedLevel"></param>
        /// <param name="ExaminationID"></param>
        public void Delete(int SchoolID, int AppliedLevel, int ExaminationID)
        {
            //ExaminationID: PK(Examination)
            {
                bool Exist = this.repository.ExistsRow(GlobalConstants.EXAM_SCHEMA, "Examination",
                        new Dictionary<string, object>()
                {
                    {"ExaminationID",ExaminationID}
                      ,{"IsActive",true}
                }, null);
                if (!Exist)
                {
                    throw new BusinessException("Common_Validate_NotAvailable");
                }
            }

            //ExaminationID, SchoolID: not compatible (ExaminationID)
            {
                bool Compatible = this.repository.ExistsRow(GlobalConstants.EXAM_SCHEMA, "Examination",
                        new Dictionary<string, object>()
                {
                    {"ExaminationID",ExaminationID},
                    {"SchoolID",SchoolID}
                }, null);
                if (!Compatible)
                {
                    throw new BusinessException("Common_Validate_NotCompatible");
                }
            }

            //ExaminationID, AppliedLevel: not compatible (ExaminationID)
            {
                bool Compatible = this.repository.ExistsRow(GlobalConstants.EXAM_SCHEMA, "Examination",
                        new Dictionary<string, object>()
                {
                    {"ExaminationID",ExaminationID},
                    {"AppliedLevel",AppliedLevel}
                }, null);
                if (!Compatible)
                {
                    throw new BusinessException("Common_Validate_NotCompatible");
                }
            }

            //Examination(ExaminationID). CurrentStage (lấy trong CSDL) chỉ được nhận giá trị EXAMINATION_STAGE_CREATED
            Examination exam = this.repository.Find(ExaminationID);
            if (exam.CurrentStage != SystemParamsInFile.EXAMINATION_STAGE_CREATED)
            {
                List<object> Params = new List<object>();
                Params.Add("Examination_Label_CurrentStage");
                Params.Add(SystemParamsInFile.EXAMINATION_STAGE_CREATED.ToString());
                throw new BusinessException("Common_Validate_Examination_CurrentStageDelete", Params);
            }

            //AcademicYearBusiness.IsCurrentYear(AcademicYearID) = FALSE => Năm học không phải năm hiện tại Examination_Label_NotCurrentAcademicYear
            if (!AcademicYearBusiness.IsCurrentYear(exam.AcademicYearID))
            {
                throw new BusinessException("Examination_Label_NotCurrentAcademicYear", new List<object>() { });
            }

            base.Delete(ExaminationID, true);
        }

        #endregion delete

        #region Search

        /// <summary>
        /// Tìm kiếm thông tin kỳ thi
        /// </summary>
        /// <Author>dungnt</Author>
        /// <DateTime>06/11/2012</DateTime>
        /// <param name="SearchInfo"></param>
        /// <returns></returns>
        private IQueryable<Examination> Search(IDictionary<string, object> SearchInfo)
        {
            int DetachableHeadMappingID = Utils.GetInt(SearchInfo, "DetachableHeadMappingID");
            int AcademicYearID = Utils.GetInt(SearchInfo, "AcademicYearID");
            int SchoolID = Utils.GetInt(SearchInfo, "SchoolID");
            int Year = Utils.GetInt(SearchInfo, "Year");

            string Title = Utils.GetString(SearchInfo, "Title");
            int EducationLevelID = Utils.GetInt(SearchInfo, "EducationLevelID");
            int AppliedLevel = Utils.GetInt(SearchInfo, "AppliedLevel");
            string Location = Utils.GetString(SearchInfo, "Location");

            string Description = Utils.GetString(SearchInfo, "Description");
            string CurrentStage = Utils.GetString(SearchInfo, "CurrentStage", "-1");
            int UsingSeparateList = Utils.GetInt(SearchInfo, "UsingSeparateList", -1);
            int CandidateFromMultipleLevel = Utils.GetInt(SearchInfo, "CandidateFromMultipleLevel", -1);

            int MarkImportType = Utils.GetInt(SearchInfo, "MarkImportType", -1);
            DateTime? FromDate = Utils.GetDateTime(SearchInfo, "FromDate");
            DateTime? ToDate = Utils.GetDateTime(SearchInfo, "ToDate");
            DateTime? CreatedDate = Utils.GetDateTime(SearchInfo, "CreatedDate");

            bool IsActive = Utils.GetBool(SearchInfo, "IsActive");
            DateTime? ModifiedDate = Utils.GetDateTime(SearchInfo, "ModifiedDate");

            //Tìm kiếm danh sách trạng thái kì thi
            List<int> Status = Utils.GetIntListSpecial(SearchInfo, "Status");
            //Tìm kiếm trạng thái '<='
            int StatusLower = Utils.GetInt(SearchInfo, "StatusLower");
            int StatusAbove = Utils.GetInt(SearchInfo, "StatusAbove");
            IQueryable<Examination> lsExamination = this.repository.All;

            if (DetachableHeadMappingID != 0)
            {
                //lsExamination = lsExamination.Where(o=>o.);
            }

            if (AcademicYearID != 0)
            {
                lsExamination = lsExamination.Where(o => o.AcademicYearID == AcademicYearID);
            }

            if (SchoolID != 0)
            {
                lsExamination = lsExamination.Where(o => o.SchoolID == SchoolID);
            }

            if (Year != 0)
            {
                lsExamination = lsExamination.Where(o => o.Year == Year);
            }

            if (Title.Trim().Length > 0)
            {
                lsExamination = lsExamination.Where(o => o.Title.ToUpper().Contains(Title.ToUpper()));
            }

            if (EducationLevelID != 0)
            {
                // lsExamination = lsExamination.Where(o=>o.);
            }

            if (AppliedLevel != 0)
            {
                lsExamination = lsExamination.Where(o => o.AppliedLevel == AppliedLevel);
            }

            if (Location.Trim().Length > 0)
            {
                lsExamination = lsExamination.Where(o => o.Location.ToUpper().Contains(Location.ToUpper()));
            }

            if (Description.Trim().Length > 0)
            {
                lsExamination = lsExamination.Where(o => o.Description.ToUpper().Contains(Description.ToUpper()));
            }

            if (CurrentStage.Trim().Length > 0)
            {
                //neu la so thi tim theo so
                int iCurrentStage = 0;
                if (int.TryParse(CurrentStage.Trim(), out iCurrentStage))
                {
                    if(iCurrentStage!=-1)
                    {
                        lsExamination = lsExamination.Where(o => o.CurrentStage == iCurrentStage);
                    }
                }
                else
                {
                    //tach thanh array
                    var lsCS = CurrentStage.Split(new string[] { "," }, StringSplitOptions.RemoveEmptyEntries);
                    if (lsCS.Count() > 0)
                    {
                        lsExamination = lsExamination.Where(o => lsCS.Contains(o.CurrentStage.ToString()));
                    }
                }
            }

            if (UsingSeparateList != -1)
            {
                lsExamination = lsExamination.Where(o => o.UsingSeparateList == UsingSeparateList);
            }

            if (CandidateFromMultipleLevel != -1)
            {
                lsExamination = lsExamination.Where(o => o.CandidateFromMultipleLevel == CandidateFromMultipleLevel);
            }

            if (MarkImportType != -1)
            {
                lsExamination = lsExamination.Where(o => o.MarkImportType == MarkImportType);
            }

            if (FromDate.HasValue)
            {
                lsExamination = lsExamination.Where(o => o.FromDate.Value.CompareTo(FromDate) == 0);
            }

            if (ToDate.HasValue)
            {
                lsExamination = lsExamination.Where(o => o.ToDate.Value.CompareTo(ToDate) == 0);
            }
            if (CreatedDate.HasValue)
            {
                lsExamination = lsExamination.Where(o => o.CreatedDate.Value.CompareTo(CreatedDate) == 0);
            }
            if (ModifiedDate.HasValue)
            {
                lsExamination = lsExamination.Where(o => o.ModifiedDate.Value.CompareTo(ModifiedDate) == 0);
            }
            if (Status.Count > 0)
            {
                lsExamination = lsExamination.Where(o => Status.Contains(o.CurrentStage));
            }
            if (StatusLower != 0)
            {
                lsExamination = lsExamination.Where(o => o.CurrentStage <= StatusLower);
            }
            if (StatusAbove != 0)
            {
                lsExamination = lsExamination.Where(o => o.CurrentStage >= StatusAbove);
            }



            lsExamination = lsExamination.Where(o => o.IsActive == IsActive);

            return lsExamination;
        }

        /// <summary>
        /// Tìm kiếm thông tin kỳ thi
        /// </summary>
        /// <Author>dungnt</Author>
        /// <DateTime>06/11/2012</DateTime>
        /// <param name="SchoolID"></param>
        /// <param name="SearchInfo"></param>
        /// <returns></returns>
        public IQueryable<Examination> SearchBySchool(int SchoolID, IDictionary<string, object> SearchInfo = null)
        {
            if (SchoolID == 0)
            {
                return null;
            }
            if (SearchInfo == null)
            {
                SearchInfo = new Dictionary<string, object>();
            }
            SearchInfo["SchoolID"] = SchoolID;
            return Search(SearchInfo);
        }

        #endregion Search

        #region Additonal

        /// <summary>
        /// Kiểm tra kỳ thi đã lập danh sách môn thi và lịch thi
        /// </summary>
        /// <Author>dungnt</Author>
        /// <DateTime>06/11/2012</DateTime>
        /// <param name="ExaminationID"></param>
        /// <returns></returns>
        public bool HasExaminationSubject(int ExaminationID)
        {
            //ExaminationID: PK(Examination)
            {
                bool Exist = this.repository.ExistsRow(GlobalConstants.EXAM_SCHEMA, "Examination",
                        new Dictionary<string, object>()
                {
                    {"ExaminationID",ExaminationID}
                }, null);
                if (!Exist)
                {
                    throw new BusinessException("Common_Validate_NotAvailable");
                }
            }

            //Kiểm tra có tồn tại ExaminationID trong bảng ExaminationSubject không. Nếu có trả về TRUÊ, nếu không trả về FALSE
            bool ExistID = ExaminationSubjectRepository.ExistsRow(GlobalConstants.EXAM_SCHEMA, "ExaminationSubject",
                        new Dictionary<string, object>()
                {
                    {"ExaminationID",ExaminationID}
                }, null);

            return ExistID;
        }

        /// <summary>
        /// Kiểm tra kỳ thi đã có lập danh sách thí sinh chưa
        /// </summary>
        /// <Author>dungnt</Author>
        /// <DateTime>06/11/2012</DateTime>
        /// <param name="ExaminationID"></param>
        /// <returns></returns>
        public bool HasCandidate(int ExaminationID)
        {
            //ExaminationID: PK(Examination)
            {
                bool Exist = this.repository.ExistsRow(GlobalConstants.EXAM_SCHEMA, "Examination",
                        new Dictionary<string, object>()
                {
                    {"ExaminationID",ExaminationID}
                }, null);
                if (!Exist)
                {
                    throw new BusinessException("Common_Validate_NotAvailable");
                }
            }

            //Kiểm tra có tồn tại ExaminationID trong bảng Candidate không. Nếu có trả về TRUÊ, nếu không trả về FALSE
            bool ExistID = CandidateRepository.ExistsRow(GlobalConstants.EXAM_SCHEMA, "Candidate",
                        new Dictionary<string, object>()
                {
                    {"ExaminationID",ExaminationID}
                }, null);

            return ExistID;
        }

        /// <summary>
        /// Kiểm tra kỳ thi đã có thí sinh trong môn thi khác nhau
        /// </summary>
        /// <Author>dungnt</Author>
        /// <DateTime>06/11/2012</DateTime>
        /// <param name="ExaminationID"></param>
        /// <returns></returns>
        public bool HasSeparatedCandidateInSubject(int ExaminationID)
        {
            //ExaminationID: PK(Examination)
            {
                bool Exist = this.repository.ExistsRow(GlobalConstants.EXAM_SCHEMA, "Examination",
                        new Dictionary<string, object>()
                {
                    {"ExaminationID",ExaminationID}
                }, null);
                if (!Exist)
                {
                    throw new BusinessException("Common_Validate_NotAvailable");
                }
            }
            // Kiểm tra có thí sinh nào tồn tại ở môn thi A nhưng không tồn tại ở môn thi B
            //- Select 1 from Candiadte c where exists (select 1 from ExaminationSubject es where es.ExaminationSubjectID != c.SubjectID
            //and es.ExaminationID =   c.ExaminationID
            //and es.EducationLevelID =   c.EducationLevelID
            //and not exists (select 1 from Candiadte cd where cd.PupilID = c.PupilID and cd.SubjectID = es.ExaminationSubjectID))
            //Nếu có dữ liệu thì trả về TRUE, nếu không thì trả về FALSE.
          //  Examination exam = ExaminationRepository.Find(ExaminationID);
            
            //ExaminationID -> lsExaminationSubject
            Examination exam = ExaminationBusiness.Find(ExaminationID);
            IQueryable<ExaminationSubject> lsExamSubject = ExaminationSubjectBusiness.SearchBySchool(exam.SchoolID.Value, new Dictionary<string, object>()
            {
                {"ExaminationID",ExaminationID}
            });
            if (lsExamSubject != null && lsExamSubject.Count() > 0)
            {
                List<int> lstEducationLevelID = lsExamSubject.Select(o => o.EducationLevelID).Distinct().ToList();
                foreach (int educationLevelID in lstEducationLevelID)
                {
                    var lsExaminationSubject = lsExamSubject.Where(o => o.EducationLevelID == educationLevelID).Select(o => o.ExaminationSubjectID).ToList();
                    //tương ứng mỗi examinationsubjectID -> List<PupilID>
                    List<List<int>> allListPupilID = new List<List<int>>();
                    foreach (var examinationSubjectID in lsExaminationSubject)
                    {
                        List<int> lsPupilIDPerSubject = CandidateBusiness.All.Where(o => o.ExaminationID == ExaminationID)
                            .Where(o => o.SubjectID == examinationSubjectID).Select(o => o.PupilID).ToList();

                        allListPupilID.Add(lsPupilIDPerSubject);
                    }

                    //sau khi đã có danh sách học sinh tại các môn thì bắt đầu so sánh từng list
                    for (int i = 0; i < allListPupilID.Count(); i++)
                    {
                        for (int j = i + 1; j < allListPupilID.Count(); j++)
                        {
                            bool isDiff = allListPupilID[i].SequenceEqual(allListPupilID[j]);
                            if (!isDiff)
                            {
                                return true;
                            }
                        }
                    }
                }
            }
            return false;
        }

        /// <summary>
        /// Kiểm tra kỳ thi tồn tại môn thi có thi sinh khác cấp
        /// </summary>
        /// <Author>dungnt</Author>
        /// <DateTime>06/11/2012</DateTime>
        /// <param name="ExaminationID"></param>
        /// <returns></returns>
        public bool HasCandidateFromMultipleLevel(int ExaminationID)
        {
            //ExaminationID: PK(Examination)
            {
                bool Exist = this.repository.ExistsRow(GlobalConstants.EXAM_SCHEMA, "Examination",
                        new Dictionary<string, object>()
                {
                    {"ExaminationID",ExaminationID}
                }, null);
                if (!Exist)
                {
                    throw new BusinessException("Common_Validate_NotAvailable");
                }
            }
            Examination exam = ExaminationBusiness.Find(ExaminationID);

            //Lấy danh sách khối đã được khai báo môn thi của kỳ thi
            IQueryable<ExaminationSubject> lsExamSubject = ExaminationSubjectBusiness.SearchBySchool(exam.SchoolID.Value, new Dictionary<string, object>()
            {
                {"ExaminationID",ExaminationID}
            });
            if(lsExamSubject != null && lsExamSubject.Count() > 0)
            {
                List<int> lstEducationLevelID = lsExamSubject.Select(o => o.EducationLevelID).Distinct().ToList();
                foreach (int educationLevelID in lstEducationLevelID)
                {
                    var lstEduLevelIDCandidate = CandidateBusiness.All.Where(o => o.ExaminationID == ExaminationID && o.EducationLevelID == educationLevelID).Select(o => o.ClassProfile.EducationLevelID).Distinct().ToList();
                    if (lstEduLevelIDCandidate != null && (lstEduLevelIDCandidate.Count > 1 || !lstEduLevelIDCandidate.Contains(educationLevelID)))
                    {
                        return true;
                    }
          
                }
            }
            else return false;
            return false;
        }

        /// <summary>
        /// Thiết lập lại trạng thái kỳ thi
        /// </summary>
        /// <Author>dungnt</Author>
        /// <DateTime>06/11/2012</DateTime>
        /// <param name="SchoolID"></param>
        /// <param name="AppliedLevel"></param>
        /// <param name="ExaminationID"></param>
        /// <param name="CurrentStage"></param>
        /// <returns></returns>
        public Examination SetStage(int SchoolID, int AppliedLevel, int ExaminationID, int CurrentStage)
        {
            //CurrentStage: range(0-6)
            Utils.ValidateRange(CurrentStage, SystemParamsInFile.EXAMINATION_STAGE_CREATED, SystemParamsInFile.EXAMINATION_STAGE_FINISHED, "Examination_Label_CurrentStage");

            //SchoolID: PK(SchoolProfile)
            {
                bool Exist = SchoolProfileRepository.ExistsRow(GlobalConstants.SCHOOL_SCHEMA, "SchoolProfile",
                        new Dictionary<string, object>()
                {
                    {"SchoolProfileID",SchoolID}
                }, null);
                if (!Exist)
                {
                    throw new BusinessException("Common_Validate_NotAvailable");
                }
            }

            //ExaminationID, SchoolID: not compatible (ExaminationID)
            {
                bool Compatible = this.repository.ExistsRow(GlobalConstants.EXAM_SCHEMA, "Examination",
                        new Dictionary<string, object>()
                {
                    {"ExaminationID",ExaminationID},
                    {"SchoolID",SchoolID}
                }, null);
                if (!Compatible)
                {
                    throw new BusinessException("Common_Validate_NotCompatible");
                }
            }

            //ExaminationID, AppliedLevel: not compatible (ExaminationID)
            {
                bool Compatible = this.repository.ExistsRow(GlobalConstants.EXAM_SCHEMA, "Examination",
                        new Dictionary<string, object>()
                {
                    {"ExaminationID",ExaminationID},
                    {"AppliedLevel",AppliedLevel}
                }, null);
                if (!Compatible)
                {
                    throw new BusinessException("Common_Validate_NotCompatible");
                }
            }

            Examination exam = this.repository.Find(ExaminationID);

            //AcademicYearBusiness.IsCurrentYear(AcademicYearID) = FALSE => Năm học không phải năm hiện tại Examination_Label_NotCurrentAcademicYear
            if (!AcademicYearBusiness.IsCurrentYear(exam.AcademicYearID))
            {
                throw new BusinessException("Examination_Label_NotCurrentAcademicYear", new List<object>() { });
            }

            //Thực hiện cập nhật lại trường CurrentStage  vào bảng Examination theo ExaminationID
            exam.CurrentStage = (int)CurrentStage;
            return base.Update(exam);
        }

        #endregion Additonal

        #region date time comparation

        /// <summary>
        /// hàm so sánh 1 khoảng thời gian A có dính líu đến khoảng thời gian B hay không
        /// </summary>
        /// <param name="sourceStart">Bắt đầu thời gian A</param>
        /// <param name="sourceEnd">Kết thúc thời gian A</param>
        /// <param name="destinationStart">Bắt đầu thời gian B</param>
        /// <param name="destinationEnd">Kết thúc thời gian B</param>
        /// <returns>true nếu dính líu và false nếu không</returns>
        public bool CompareDateTimeRange(DateTime sourceStart, DateTime sourceEnd, DateTime destinationStart, DateTime destinationEnd)
        {
            //Có các trường hợp dính líu sau:
            //            |--- Date 1 ---|
            //                 | --- Date 2 --- |

            //                | --- Date 1 --- |
            //         | --- Date 2 ---- |

            //         | -------- Date 1 -------- |
            //               | --- Date 2 --- |

            //               | --- Date 1 --- |
            //         | -------- Date 2 -------- |

            if (sourceStart == sourceEnd || destinationStart == destinationEnd)
                return false; // No actual date range

            if (sourceStart == destinationStart || sourceEnd == destinationEnd)
                return true; // If any set is the same time, then by default there must be some overlap.

            if (sourceStart < destinationStart)
            {
                if (sourceEnd > destinationStart && sourceEnd < destinationEnd)
                    return true; // Condition 1

                if (sourceEnd > destinationEnd)
                    return true; // Condition 3
            }
            else
            {
                if (destinationEnd > sourceStart && destinationEnd < sourceEnd)
                    return true; // Condition 2

                if (destinationEnd > sourceEnd)
                    return true; // Condition 4
            }

            return false;
        }

        /// <summary>
        /// hàm so sánh 1 khoảng thời gian A có nằm trong khoảng thời gian B hay không
        /// </summary>
        /// <param name="sourceStart">Bắt đầu thời gian A</param>
        /// <param name="sourceEnd">Kết thúc thời gian A</param>
        /// <param name="destinationStart">Bắt đầu thời gian B</param>
        /// <param name="destinationEnd">Kết thúc thời gian B</param>
        /// <returns>true nếu dính líu và false nếu không</returns>
        public bool IsDateTimeInsideADateTimeRange(DateTime sourceStart, DateTime sourceEnd, DateTime destinationStart, DateTime destinationEnd)
        {
            //Có các trường hợp dính líu sau:
            //            |--- Date 1 ---|
            //                 | --- Date 2 --- |

            //                | --- Date 1 --- |
            //         | --- Date 2 ---- |

            //         | -------- Date 1 -------- |
            //               | --- Date 2 --- |

            //               | --- Date 1 --- |         --------->Đây là trường hợp ra true
            //         | -------- Date 2 -------- |

            if (sourceStart == sourceEnd || destinationStart == destinationEnd)
                return true; // No actual date range

            if (sourceStart == destinationStart || sourceEnd == destinationEnd)
                return true; // If any set is the same time, then by default there must be some overlap.

            if (sourceStart < destinationStart)
            {
                if (sourceEnd > destinationStart && sourceEnd < destinationEnd)
                    return false; // Condition 1

                if (sourceEnd > destinationEnd)
                    return false; // Condition 3
            }
            else
            {
                if (destinationEnd > sourceStart && destinationEnd < sourceEnd)
                    return false; // Condition 2

                if (destinationEnd > sourceEnd)
                    return true; // Condition 4
            }

            return false;
        }

        #endregion date time comparation
        public List<ExamSubjectBO> getExamScheduleInfo(List<int> pupilIDList, int schoolID, int academicYearID, List<int> lstClassID, int examID)
        {
            try
            {
                List<PupilOfClassBO> pupilOfClassList = (from poc in PupilOfClassBusiness.All
                                                         where poc.AcademicYearID == academicYearID
                                                         && poc.SchoolID == schoolID
                                                         && lstClassID.Contains(poc.ClassID)
                                                         && pupilIDList.Contains(poc.PupilID)
                                                         select new PupilOfClassBO 
                                                         {
                                                             PupilID = poc.PupilID,
                                                             PupilFullName = poc.PupilProfile.FullName
                                                         }).ToList();

                List<ExamSubjectBO> listResult = new List<ExamSubjectBO>();
                ExamSubjectBO obj;
                if (pupilOfClassList != null && pupilOfClassList.Count > 0)
                {
                    int partitionId = UtilsBusiness.GetPartionId(schoolID);
                    PupilOfClassBO pupilOfClassBO = null;
                    int pupilID = 0;
                    StringBuilder pupilContent = null;

                    List<ExamPupilBO> iqExamPupil = (from ex in ExaminationsBusiness.All
                                                           join eg in ExamGroupBusiness.All on ex.ExaminationsID equals eg.ExaminationsID
                                                           join ep in ExamPupilBusiness.All on new { ex.ExaminationsID, eg.ExamGroupID } equals new { ep.ExaminationsID, ep.ExamGroupID }
                                                           join es in ExamSubjectBusiness.All on new { ex.ExaminationsID, eg.ExamGroupID } equals new { es.ExaminationsID, es.ExamGroupID }
                                                           join sc in SubjectCatBusiness.All on es.SubjectID equals sc.SubjectCatID
                                                           where ex.SchoolID == schoolID && ep.LastDigitSchoolID == partitionId
                                                           && ex.AcademicYearID == academicYearID
                                                           && pupilIDList.Contains(ep.PupilID)
                                                           && ex.ExaminationsID == examID
                                                           orderby sc.OrderInSubject
                                                           select new ExamPupilBO
                                                           {
                                                               ExaminationsID = ex.ExaminationsID,
                                                               ExaminationName = ex.ExaminationsName,
                                                               PupilID = ep.PupilID,
                                                               ExamGroupID = eg.ExamGroupID,
                                                               SubjectID = es.SubjectID,
                                                               SubjectName = sc.SubjectName,
                                                               SchedulesExam = es.SchedulesExam
                                                           }).ToList();
                    List<ExamPupilBO> lstGroupEachPupil = null;
                    ExamPupilBO objExamDetailBO = null;
                    for (int i = 0; i < pupilOfClassList.Count; i++)
                    {
                        pupilOfClassBO = pupilOfClassList[i];
                        pupilID = pupilOfClassBO.PupilID;
                        obj = new ExamSubjectBO();
                        obj.PupilID = pupilID;
                        obj.PupilName = pupilOfClassBO.PupilFullName;

                        pupilContent = new StringBuilder();
                        bool isFirst = true;
                        lstGroupEachPupil = iqExamPupil.Where(p => p.PupilID == pupilID).ToList();
                        for (int j = 0; j < lstGroupEachPupil.Count; j++)
                        {
                            objExamDetailBO = lstGroupEachPupil[j];
                            if (isFirst)
                            {
                                isFirst = false;
                                obj.ExaminationsID = objExamDetailBO.ExaminationsID;
                                obj.ExaminationsName = objExamDetailBO.ExaminationName;
                            }
                            if (!string.IsNullOrEmpty(objExamDetailBO.SchedulesExam))
                                pupilContent.Append(objExamDetailBO.SubjectName).Append(" ").Append(objExamDetailBO.SchedulesExam).Append("; ");
                        }

                        if (pupilContent != null && pupilContent.Length > 0)
                        {
                            if (pupilContent.ToString().EndsWith("; ")) pupilContent = pupilContent.Remove(pupilContent.Length - 2, 2);
                            obj.ScheduleContent = pupilContent.ToString();

                        }

                        listResult.Add(obj);
                    }
                }
                return listResult;
            }
            catch (Exception ex)
            {
                logger.Error(ex.Message, ex);
                return new List<ExamSubjectBO>();
            }
        }
        public List<ExamMarkPupilBO> getExamResultInfo(List<long> pupilIDList, int schoolID, int academicYearID, List<int> lstClassID, int examID)
        {
            try
            {
                List<PupilOfClassBO> pupilOfClassList = (from poc in PupilOfClassBusiness.All
                                                         where poc.AcademicYearID == academicYearID
                                                         && poc.SchoolID == schoolID
                                                         && lstClassID.Contains(poc.ClassID)
                                                         && pupilIDList.Contains(poc.PupilID)
                                                         select new PupilOfClassBO
                                                         {
                                                             PupilID = poc.PupilID,
                                                             PupilFullName = poc.PupilProfile.FullName
                                                         }).ToList();

                List<ExamMarkPupilBO> listResult = new List<ExamMarkPupilBO>();

                if (pupilOfClassList != null && pupilOfClassList.Count > 0)
                {
                    int partitionId = UtilsBusiness.GetPartionId(schoolID);
                    ExamMarkPupilBO obj = null;
                    PupilOfClassBO pupilOfClassBO = null;
                    int pupilID = 0;
                    StringBuilder pupilContent = null;

                    List<ExamInputMarkBO> lstExamMarkInput = (from eim in ExamInputMarkBusiness.All
                                                              join sc in SubjectCatBusiness.All on eim.SubjectID equals sc.SubjectCatID
                                                              where eim.SchoolID == schoolID && eim.LastDigitSchoolID == partitionId
                                                              && eim.AcademicYearID == academicYearID
                                                              && lstClassID.Contains(eim.ClassID) && pupilIDList.Contains(eim.PupilID)
                                                              && eim.ExaminationsID == examID
                                                              orderby sc.OrderInSubject
                                                              select new ExamInputMarkBO
                                                              {
                                                                  PupilID = eim.PupilID,
                                                                  SubjectID = eim.SubjectID,
                                                                  SubjectName = sc.SubjectName,
                                                                  ClassID = eim.ClassID,
                                                                  ExamMark = eim.ExamMark,
                                                                  ActualMark = eim.ActualMark,
                                                                  ExamJudgeMark = eim.ExamJudgeMark
                                                              }).ToList();
                    Examinations exam = ExaminationsBusiness.Find(examID);
                    List<ExamInputMarkBO> lstMarkEachPupil = null;
                    ExamInputMarkBO objExamInputMarkBO = null;
                    for (int i = 0; i < pupilOfClassList.Count; i++)
                    {
                        pupilOfClassBO = pupilOfClassList[i];
                        pupilID = pupilOfClassBO.PupilID;
                        obj = new ExamMarkPupilBO();
                        obj.PupilID = pupilID;
                        obj.PupilName = pupilOfClassBO.PupilFullName;
                        if (exam != null)
                        {
                            obj.ExaminationID = examID;
                            obj.ExaminationName = exam.ExaminationsName;
                        }
                        pupilContent = new StringBuilder();

                        lstMarkEachPupil = lstExamMarkInput.Where(p => p.PupilID == pupilID).ToList();
                        bool isFirst = true;
                        for (int j = 0; j < lstMarkEachPupil.Count; j++)
                        {
                            objExamInputMarkBO = lstMarkEachPupil[j];
                            if (objExamInputMarkBO.ActualMark.HasValue || !string.IsNullOrEmpty(objExamInputMarkBO.ExamJudgeMark))
                            {
                                if (isFirst) isFirst = false;
                                else pupilContent.Append("; ");
                                pupilContent.Append(objExamInputMarkBO.SubjectName).Append(" ");
                                if (objExamInputMarkBO.ActualMark.HasValue) pupilContent.Append(objExamInputMarkBO.ActualMark.Value.ToString("#0.#"));
                                else if (!string.IsNullOrEmpty(objExamInputMarkBO.ExamJudgeMark)) pupilContent.Append(objExamInputMarkBO.ExamJudgeMark);
                            }
                        }
                        if (pupilContent != null && pupilContent.Length > 0) obj.ExamResult = pupilContent.ToString();
                        listResult.Add(obj);
                    }
                }
                return listResult;
            }
            catch (Exception ex)
            {
                logger.Error(ex.Message, ex);
                return new List<ExamMarkPupilBO>();
            }
        }
        public List<ExaminationsBO> GetExamOfSchool(IDictionary<string, object> dic)
        {
            int schoolID = Utils.GetInt(dic, "SchoolID");
            int academicYearID = Utils.GetInt(dic, "AcademicYearID");
            int semester = Utils.GetInt(dic, "SemesterID");
            long examID = Utils.GetInt(dic, "ExaminationID");
            int appliedLevel = Utils.GetInt(dic, "AppliedLevelID");
            List<ExaminationsBO> lstExaminations = new List<ExaminationsBO>();
            try
            {
                List<ExaminationsBO> lstExam = (from ex in ExaminationsBusiness.All
                                                where (examID == 0 || ex.ExaminationsID == examID)
                                                && ex.SchoolID == schoolID && ex.AcademicYearID == academicYearID
                                                && ex.AppliedLevel == appliedLevel
                                                && ex.SemesterID == semester
                                                orderby ex.CreateTime descending
                                                select new ExaminationsBO()
                                                {
                                                    ExaminationsID = ex.ExaminationsID,
                                                    ExaminationsName = ex.ExaminationsName,
                                                    SchoolID = ex.SchoolID,
                                                    AcademicYearID = ex.AcademicYearID,
                                                    Semester = ex.SemesterID,
                                                    MarkInput = ex.MarkInput.HasValue ? ex.MarkInput.Value : false,
                                                    MarkClosing = ex.MarkClosing.HasValue ? ex.MarkClosing.Value : false
                                                }).ToList();

                return lstExam;
            }
            catch (Exception ex)
            {
                logger.Error(ex.Message, ex);
                return new List<ExaminationsBO>();
            }
        }
    }
}
