﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Linq.Expressions;
using System.Web;
using log4net;

using SMAS.DAL.Repository;
using SMAS.DAL.IRepository;
using SMAS.Business.IBusiness;
using SMAS.Models.Models;
using System.IO;
using SMAS.Business.Common;
using SMAS.VTUtils.Excel.Export;

namespace SMAS.Business.Business
{
    public class ReportExamBusiness : GenericBussiness<ProcessedReport>, IReportExamBusiness
    {
        private ISubjectCatRepository SubjectCatRepository;
        private IExaminationRoomRepository ExaminationRoomRepository;
        private IExaminationSubjectRepository ExaminationSubjectRepository;
        private IExaminationRepository ExaminationRepository;
        //private IReportDefinitionBusiness _ReportDefinitionBusiness;


        public ReportExamBusiness(ILog logger, SMASEntities context = null)
            : base(logger)
        {
            if (context == null) { context = new SMASEntities(); } this.context = context;

            this.SubjectCatRepository = new SubjectCatRepository(context);
            this.ExaminationRoomRepository = new ExaminationRoomRepository(context);
            this.ExaminationSubjectRepository = new ExaminationSubjectRepository(context);
            this.ExaminationRepository = new ExaminationRepository(context);
        }


        /// <summary>
        /// Author: Tamhm1
        /// DAte: 19/12/2012
        /// In danh sách thí sinh
        /// </summary>
        /// <param name="dic"></param>
        /// <returns></returns>
        public Stream CreateReportList(IDictionary<string, object> dic)
        {
            //reportName GV_[SchoolLevel]_BaoCaoSoLuongHSTheoGV_[Semester]
            int schoolID = Utils.GetInt(dic, "SchoolID");
            int academicYearID = Utils.GetInt(dic, "AcademicYearID");
            int appliedLevel = Utils.GetInt(dic, "AppliedLevel");
            int examinationID = Utils.GetInt(dic, "ExaminationID");
            int educationLevelID = Utils.GetInt(dic, "EducationLevelID");
            int examinationSubjectID = Utils.GetInt(dic, "ExaminationSubjectID");
            int examinationRoomID = Utils.GetInt(dic, "ExaminationRoomID");
            if (examinationSubjectID == 0)
                throw new BusinessException("ReportExam_Validate_Require");

            string reportCode = SystemParamsInFile.REPORT_DANH_SACH_THI_SINH;
            ReportDefinition reportDef = ReportDefinitionBusiness.GetByCode(reportCode);
            string templatePath = SystemParamsInFile.TEMPLATE_FOLDER + reportDef.TemplateDirectory + "/" + reportDef.TemplateName;
            IVTWorkbook oBook = VTExport.OpenWorkbook(templatePath);
            int firstRow = 9;

            //Lấy sheet template
            IVTWorksheet firstSheet = oBook.GetSheet(1);
            //Tạo template chuẩn

            //tempSheet.CopyPasteSameSize(firstSheet.GetRange(firstRow, 1, firstRow + 6, lastCol), "A" + firstRow);

            //Fill dữ liệu chung cho template

            SchoolProfile school = SchoolProfileBusiness.Find(schoolID);
            AcademicYear academicYear = AcademicYearBusiness.Find(academicYearID);
            Province province = school.Province;
            string schoolName = school.SchoolName.ToUpper();
            string provinceName = province.ProvinceName.ToUpper();
            string SupervisingDeptName = school.SupervisingDept.SupervisingDeptName.ToUpper();
            string academicTitle = academicYear.Year + " - " + (academicYear.Year + 1);
            Examination exam = ExaminationRepository.Find(examinationID);
            string examinationName = exam.Title;
            //string academicYearTitle = academicYear.DisplayTitle;

            //Tạo các dòng chuẩn theo template
            IVTRange topRow = firstSheet.GetRange("A10", "F10");
            IVTRange middleRow = firstSheet.GetRange("A11", "F11");
            IVTRange bottomRow = firstSheet.GetRange("A12", "F12");
            //KHởi tạo một list để add các phòng thi vào.
            List<ExaminationRoom> lstExam;
            if (examinationRoomID == 0) // Chon tat cac cac phong thi
            {
                IDictionary<string, object> dicExamRoom = new Dictionary<string, object>();
                dicExamRoom["ExaminationSubjectID"] = examinationSubjectID;
                dicExamRoom["ExaminationID"] = examinationID;
                dicExamRoom["EducationLevelID"] = educationLevelID;
                dicExamRoom["AppliedLevel"] = appliedLevel;
                dicExamRoom["AcademicYearID"] = academicYearID;
                lstExam = ExaminationRoomBusiness.SearchBySchool(schoolID, dicExamRoom).OrderBy(r=>r.RoomTitle).ToList();
            }
            else // Chon 1 phong thi
            {
                lstExam = new List<ExaminationRoom>();
                lstExam.Add(ExaminationRoomRepository.Find(examinationRoomID));
            }
            //Lấy ra danh sách thí sinh
            SubjectCat subjectCat = ExaminationSubjectBusiness.Find(examinationSubjectID).SubjectCat;
            foreach (ExaminationRoom er in lstExam)
            {
                IVTWorksheet sheet = oBook.CopySheetToLast(firstSheet, "F" + (firstRow));
                IDictionary<string, object> dicCandidate = new Dictionary<string, object>();
                dicCandidate["ExaminationID"] = examinationID;
                dicCandidate["EducationLevelID"] = educationLevelID;
                dicCandidate["RoomID"] = er.ExaminationRoomID;
                dicCandidate["AcademicYearID"] = academicYearID;
                dicCandidate["AppliedLevel"] = appliedLevel;
                IQueryable<Candidate> qCandidate = CandidateBusiness.SearchBySchool(schoolID, dicCandidate);
                var lstCandidate = (from c in qCandidate
                                    join p in PupilProfileBusiness.All on c.PupilID equals p.PupilProfileID
                                    join cp in ClassProfileBusiness.All on c.ClassID equals cp.ClassProfileID
                                    where cp.IsActive.Value
                                    select new
                                    {
                                        c.NamedListNumber,
                                        c.NamedListCode,
                                        p.BirthDate,
                                        p.PupilCode,
                                        p.FullName,
                                        p.Name,
                                        ClassName = cp.DisplayName
                                    }).ToList().OrderBy(o => o.NamedListNumber).ThenBy(o => o.Name).ThenBy(o => o.FullName).ToList();
                string subjectName = "";
                string roomName = "";
                roomName = "Phòng thi: " + er.RoomTitle;
                sheet.SetCellValue("A7", roomName);
                if (examinationSubjectID != 0 && exam.UsingSeparateList == 1)
                {
                    subjectName = " - Môn thi: " + subjectCat.DisplayName;
                }
                Dictionary<string, object> dicGeneralInfo = new Dictionary<string, object> 
                {
                    {"SchoolName", schoolName}, 
                    {"SupervisingDeptName", SupervisingDeptName},                 
                    {"ExaminationName", examinationName},
                    {"AcademicTitle", academicTitle},
                    {"SubjectName", subjectName},
                    {"RoomName", roomName}
                };
                int order = 1;
                int currentRow;
                List<object> list = new List<object>();
                foreach (var candidate in lstCandidate)
                {
                    currentRow = firstRow + order;
                    if (order == 1 && lstCandidate.Count() == 1)
                    {
                        sheet.CopyPasteSameRowHeigh(bottomRow, currentRow);
                    }
                    else if (order == 1)
                    {
                        sheet.CopyPasteSameRowHeigh(topRow, currentRow);
                    }
                    else if (order == lstCandidate.Count())
                    {
                        sheet.CopyPasteSameRowHeigh(bottomRow, currentRow);
                    }
                    else
                    {
                        sheet.CopyPasteSameRowHeigh(middleRow, currentRow);
                    }

                    list.Add(new Dictionary<string, object> 
                    {
                        {"Order", order++}, 
                        {"NamedListCode", candidate.NamedListCode},
                        {"FullName", candidate.FullName},
                        {"BirthDate", candidate.BirthDate.ToString("dd/MM/yyyy")},
                        {"ClassName", candidate.ClassName},
                       
                    });
                }
                dicGeneralInfo.Add("list", list);
                sheet.FillVariableValue(dicGeneralInfo);
                sheet.Name = er.RoomTitle;
            }
            firstSheet.Delete();

            return oBook.ToStream();
        }
        /// <summary>
        /// Danh sách phân công giám thị
        /// Author: Tamhm1
        /// Date: 19/12/2012
        /// </summary>
        /// <param name="dic"></param>
        /// <returns></returns>
        public Stream CreateAssignReport(IDictionary<string, object> dic)
        {

            //reportName GV_[SchoolLevel]_BaoCaoSoLuongHSTheoGV_[Semester]
            int schoolID = Utils.GetInt(dic, "SchoolID");
            int academicYearID = Utils.GetInt(dic, "AcademicYearID");
            int appliedLevel = Utils.GetInt(dic, "AppliedLevel");
            int examinationID = Utils.GetInt(dic, "ExaminationID");
            int educationLevelID = Utils.GetInt(dic, "EducationLevelID");

            string reportCode = SystemParamsInFile.REPORT_DS_PHAN_CONG_GIAM_THI;
            ReportDefinition reportDef = ReportDefinitionBusiness.GetByCode(reportCode);
            string templatePath = SystemParamsInFile.TEMPLATE_FOLDER + reportDef.TemplateDirectory + "/" + reportDef.TemplateName;
            IVTWorkbook oBook = VTExport.OpenWorkbook(templatePath);
            int firstRow = 10;

            //Lấy sheet template
            IVTWorksheet firstSheet = oBook.GetSheet(1);
            //Tạo template chuẩn


            //tempSheet.CopyPasteSameSize(firstSheet.GetRange(firstRow, 1, firstRow + 6, lastCol), "A" + firstRow);

            //Fill dữ liệu chung cho template

            SchoolProfile school = SchoolProfileBusiness.Find(schoolID);
            AcademicYear academicYear = AcademicYearBusiness.Find(academicYearID);
            Province province = school.Province;
            string schoolName = school.SchoolName.ToUpper();
            string provinceName = province.ProvinceName;
            string SupervisingDeptName = school.SupervisingDept.SupervisingDeptName.ToUpper();
            string academicTitle = academicYear.Year + " - " + (academicYear.Year + 1);
            string examinationName = ExaminationRepository.Find(examinationID).Title.ToUpper();
            string educationLevelName = EducationLevelBusiness.Find(educationLevelID).Resolution.ToUpper();
            DateTime ReportDate = DateTime.Now;

            //string academicYearTitle = academicYear.DisplayTitle;

            //Tạo các dòng chuẩn theo template
            IVTRange topRow = firstSheet.GetRange("A11", "G11");
            IVTRange middleRow = firstSheet.GetRange("A12", "G12");
            IVTRange bottomRow = firstSheet.GetRange("A13", "G13");
            IVTRange mergeRow = firstSheet.GetRange("A11", "A12");
            //KHởi tạo một list môn học
            List<ExaminationSubject> lstExaminationSubject = new List<ExaminationSubject>();
            lstExaminationSubject = ExaminationSubjectBusiness.SearchBySchool(schoolID, dic)
                .OrderBy(o => o.SubjectCat.OrderInSubject).ThenBy(o => o.SubjectCat.DisplayName)
                .ToList();

            foreach (ExaminationSubject es in lstExaminationSubject)
            {
                string time;
                DateTime startTime = ExaminationSubjectBusiness.Find(es.ExaminationSubjectID).StartTime.GetValueOrDefault();
                if (startTime.Minute != 0)
                    time = "(" + startTime.Hour + "h" + startTime.Minute + ", " + startTime.Day + "/" + startTime.Month + "/" + startTime.Year + ")";
                else
                    time = "(" + startTime.Hour + "h" + ", " + startTime.Day + "/" + startTime.Month + "/" + startTime.Year + ")";
                Dictionary<string, object> dicGeneralInfo = new Dictionary<string, object> 
                {
                    {"SchoolName", schoolName}, 
                    {"ProvinceName", provinceName},
                    {"SupervisingDeptName", SupervisingDeptName},                 
                    {"ExaminationName", examinationName},
                    {"AcademicYearTitle", academicTitle},
                    {"EducationLevelName", educationLevelName},
                    {"ReportDate", ReportDate},
                    {"ExaminationDate",time},
                    {"SubjectName", ExaminationSubjectRepository.Find(es.ExaminationSubjectID).SubjectCat.DisplayName.ToUpper()},                 
                };
                //Lấy ra danh sách thí sinh

                IVTWorksheet sheet = oBook.CopySheetToLast(firstSheet, "G" + (firstRow));
                List<ExaminationRoom> lstExaminationRoom = new List<ExaminationRoom>();

                //Mỗi môn học lấy ra danh sách các phòng thi dựa vào kỳ thi, khối học, môn thi
                IDictionary<string, object> dicRoom = new Dictionary<string, object>();
                dicRoom["ExaminationID"] = examinationID;
                dicRoom["EducationLevelID"] = educationLevelID;
                dicRoom["ExaminationSubjectID"] = es.ExaminationSubjectID;
                dicRoom["AcademicYearID"] = academicYearID;
                dicRoom["AppliedLevel"] = appliedLevel;

                lstExaminationRoom = ExaminationRoomBusiness.SearchBySchool(schoolID, dicRoom).ToList();
                int order = 1;
                int currentRow;
                List<object> list = new List<object>();
                int merge = 1;

                int firstMerge = firstRow;
                int lastMerge = firstRow;
                foreach (ExaminationRoom examinationRoom in lstExaminationRoom)
                {
                    //Đối với mỗi phòng thi thì lấy ra danh sách giám thị
                    List<InvigilatorAssignment> lstInvigilatorAssignment = new List<InvigilatorAssignment>();
                    IDictionary<string, object> dicInvi = new Dictionary<string, object>();
                    dicInvi["ExaminationID"] = examinationID;
                    dicInvi["EducationLevelID"] = educationLevelID;
                    dicInvi["AcademicYearID"] = academicYearID;
                    dicInvi["AppliedLevel"] = appliedLevel;
                    dicInvi["ExaminationSubjectID"] = es.ExaminationSubjectID;
                    dicInvi["RoomID"] = examinationRoom.ExaminationRoomID;
                    lstInvigilatorAssignment = InvigilatorAssignmentBusiness.SearchBySchool(schoolID, dicInvi).ToList();
                    lstInvigilatorAssignment = lstInvigilatorAssignment.OrderBy(o => o.Invigilator.Employee.FullName).ToList();
                    int row = 1;
                    if (lstInvigilatorAssignment.Count() != 0)
                    {
                        lastMerge += lstInvigilatorAssignment.Count();
                        firstMerge = lastMerge - lstInvigilatorAssignment.Count() + 1;
                        foreach (InvigilatorAssignment invigilatorAssignment in lstInvigilatorAssignment)
                        {

                            currentRow = firstRow + order;
                            if (row == 1 && lstInvigilatorAssignment.Count() == 1)
                            {
                                sheet.CopyPasteSameRowHeigh(bottomRow, currentRow);
                            }
                            else if (row == 1)
                            {
                                sheet.CopyPasteSameRowHeigh(topRow, currentRow);
                            }
                            else if (row == lstInvigilatorAssignment.Count())
                            {
                                sheet.CopyPasteSameRowHeigh(bottomRow, currentRow);

                            }
                            else
                            {
                                sheet.CopyPasteSameRowHeigh(middleRow, currentRow);
                            }
                            row++;

                            Employee employee = invigilatorAssignment.Invigilator.Employee;
                            list.Add(new Dictionary<string, object> 
                    {
                       
                        {"EmployeeCode", employee.EmployeeCode},
                        {"EmployeeName", employee.FullName},
                        {"BirthDate", employee.BirthDate},
                        {"FacultyName", employee.SchoolFaculty != null ? employee.SchoolFaculty.FacultyName : string.Empty},
                        {"PhoneNumber", employee.Mobile}
                       
                    });
                            order++;
                        }
                        sheet.GetRange("A" + firstMerge, "A" + lastMerge).Merge();
                        sheet.SetCellValue("A" + firstMerge, merge);

                        sheet.GetRange("B" + firstMerge, "B" + lastMerge).Merge();
                        sheet.SetCellValue("B" + firstMerge, examinationRoom.RoomTitle);
                        merge++;
                    }



                }
                dicGeneralInfo.Add("list", list);
                sheet.FillVariableValue(dicGeneralInfo);
                sheet.Name = SubjectCatBusiness.Find(es.SubjectID).DisplayName;
            }
            firstSheet.Delete();
            return oBook.ToStream();
        }

        /// <summary>
        /// Danh sách thí sinh theo phòng thi
        /// Author: Tamhm1
        /// Date: 19/12/2012
        /// </summary>
        /// <param name="dic"></param>
        /// <returns></returns>
        public Stream CreateRollReport(IDictionary<string, object> dic)
        {

            //reportName GV_[SchoolLevel]_BaoCaoSoLuongHSTheoGV_[Semester]
            int schoolID = Utils.GetInt(dic, "SchoolID");
            int academicYearID = Utils.GetInt(dic, "AcademicYearID");
            int appliedLevel = Utils.GetInt(dic, "AppliedLevel");
            int examinationID = Utils.GetInt(dic, "ExaminationID");
            int educationLevelID = Utils.GetInt(dic, "EducationLevelID");
            int examinationSubjectID = Utils.GetInt(dic, "ExaminationSubjectID");
            int examinationRoomID = Utils.GetInt(dic, "ExaminationRoomID");
            if (examinationSubjectID == 0)
                throw new BusinessException("ReportExam_Validate_Require");

            string reportCode = SystemParamsInFile.REPORT_DANH_SACH_THI_SINH_DIEM_DANH;
            ReportDefinition reportDef = ReportDefinitionBusiness.GetByCode(reportCode);
            string templatePath = SystemParamsInFile.TEMPLATE_FOLDER + reportDef.TemplateDirectory + "/" + reportDef.TemplateName;
            IVTWorkbook oBook = VTExport.OpenWorkbook(templatePath);
            int firstRow = 10;

            //Lấy sheet template
            IVTWorksheet firstSheet = oBook.GetSheet(1);
            //Tạo template chuẩn


            //tempSheet.CopyPasteSameSize(firstSheet.GetRange(firstRow, 1, firstRow + 6, lastCol), "A" + firstRow);

            //Fill dữ liệu chung cho template

            SchoolProfile school = SchoolProfileBusiness.Find(schoolID);
            AcademicYear academicYear = AcademicYearBusiness.Find(academicYearID);
            Province province = school.Province;
            string schoolName = school.SchoolName.ToUpper();
            string provinceName = province.ProvinceName;
            string SupervisingDeptName = school.SupervisingDept.SupervisingDeptName.ToUpper();
            string academicTitle = academicYear.Year + " - " + (academicYear.Year + 1);
            string examinationName = ExaminationRepository.Find(examinationID).Title.ToUpper();
            DateTime ReportDate = DateTime.Now;

            //string academicYearTitle = academicYear.DisplayTitle;

            //Tạo các dòng chuẩn theo template
            IVTRange topRow = firstSheet.GetRange("A11", "G11");
            IVTRange middleRow = firstSheet.GetRange("A12", "G12");
            IVTRange bottomRow = firstSheet.GetRange("A13", "G13");
            //KHởi tạo một list để add các phòng thi vào.
            List<ExaminationRoom> lstExam = new List<ExaminationRoom>();
            if (examinationRoomID == 0)
            {
                IDictionary<string, object> dicExamRoom = new Dictionary<string, object>();
                dicExamRoom["ExaminationSubjectID"] = examinationSubjectID;
                dicExamRoom["ExaminationID"] = examinationID;
                dicExamRoom["EducationLevelID"] = educationLevelID;
                dicExamRoom["AppliedLevel"] = appliedLevel;
                dicExamRoom["AcademicYearID"] = academicYearID;
                lstExam = ExaminationRoomBusiness.SearchBySchool(schoolID, dicExamRoom).OrderBy(e=>e.RoomTitle).ToList();
            }
            else
            {
                lstExam.Add(ExaminationRoomRepository.Find(examinationRoomID));
            }
            //Lấy ra danh sách thí sinh
            foreach (ExaminationRoom er in lstExam)
            {
                IVTWorksheet sheet = oBook.CopySheetToLast(firstSheet, "G" + (firstRow));
                DateTime startTime = ExaminationSubjectBusiness.Find(examinationSubjectID).StartTime.GetValueOrDefault();
                string time;
                if (startTime.Minute != 0)
                    time = "(" + startTime.Hour + "h" + startTime.Minute + ", " + startTime.Day + "/" + startTime.Month + "/" + startTime.Year + ")";
                else
                    time = "(" + startTime.Hour + "h" + ", " + startTime.Day + "/" + startTime.Month + "/" + startTime.Year + ")";
                Dictionary<string, object> dicGeneralInfo = new Dictionary<string, object> 
                {
                    {"SchoolName", schoolName}, 
                    {"ProvinceName", provinceName},
                    {"SupervisingDeptName", SupervisingDeptName},                 
                    {"ExaminationName", examinationName},
                    {"AcademicYearTitle", academicTitle},
                    {"RoomTitle", er.RoomTitle.ToUpper()},
                    {"ReportDate", ReportDate},
                    {"ExaminationDate",time},
                    {"SubjectName", ExaminationSubjectRepository.Find(examinationSubjectID).SubjectCat.DisplayName.ToUpper()},                 
                };

                IDictionary<string, object> dicCandidate = new Dictionary<string, object>();
                dicCandidate["ExaminationID"] = examinationID;
                dicCandidate["EducationLevelID"] = educationLevelID;
                dicCandidate["RoomID"] = er.ExaminationRoomID;
                dicCandidate["AcademicYearID"] = academicYearID;
                dicCandidate["AppliedLevel"] = appliedLevel;

                IQueryable<Candidate> qCandidate = CandidateBusiness.SearchBySchool(schoolID, dicCandidate);
                var lstCandidate = (from c in qCandidate
                                    join p in PupilProfileBusiness.All on c.PupilID equals p.PupilProfileID
                                    join cp in ClassProfileBusiness.All on c.ClassID equals cp.ClassProfileID
                                    where cp.IsActive.Value
                                    select new
                                    {
                                        c.HasReason,
                                        c.NamedListNumber,
                                        c.NamedListCode,
                                        p.BirthDate,
                                        p.PupilCode,
                                        p.FullName,
                                        p.Name,
                                        ClassName = cp.DisplayName
                                    }).ToList().OrderBy(o => o.NamedListNumber).ThenBy(o => o.Name).ThenBy(o => o.FullName).ToList();
                int order = 1;
                int currentRow;
                List<object> list = new List<object>();
                foreach (var candidate in lstCandidate)
                {
                    currentRow = firstRow + order;
                    if (order == 1 && lstCandidate.Count() == 1)
                    {
                        sheet.CopyPasteSameRowHeigh(bottomRow, currentRow);
                    }
                    else if (order == 1)
                    {
                        sheet.CopyPasteSameRowHeigh(topRow, currentRow);
                    }
                    else if (order == lstCandidate.Count() || order % 5 == 0)
                    {
                        sheet.CopyPasteSameRowHeigh(bottomRow, currentRow);
                    }
                    else
                    {
                        sheet.CopyPasteSameRowHeigh(middleRow, currentRow);
                    }
                    string absenceExam = "";
                    if (candidate.HasReason != null)
                    {
                        absenceExam = "X";
                    }
                    list.Add(new Dictionary<string, object> 
                    {
                        {"Order", order++}, 
                        {"NamedListCode", candidate.NamedListCode},
                        {"FullName", candidate.FullName},
                        {"BirthDate", candidate.BirthDate},
                        {"ClassName", candidate.ClassName},
                        {"ExaminationAbsence", absenceExam}
                       
                    });
                }
                dicGeneralInfo.Add("list", list);
                sheet.FillVariableValue(dicGeneralInfo);
                sheet.Name = er.RoomTitle;
            }
            firstSheet.Delete();

            return oBook.ToStream();
        }



        /// <summary>
        /// Author: Tamhm1
        /// Date: 19/12/2012
        /// Báo cáo File nhập điểm
        /// </summary>
        /// <param name="dic"></param>
        /// <returns></returns>
        public Stream CreateInputMarkFile(IDictionary<string, object> dic)
        {

            //reportName GV_[SchoolLevel]_BaoCaoSoLuongHSTheoGV_[Semester]
            int schoolID = Utils.GetInt(dic, "SchoolID");
            int academicYearID = Utils.GetInt(dic, "AcademicYearID");
            int appliedLevel = Utils.GetInt(dic, "AppliedLevel");
            int examinationID = Utils.GetInt(dic, "ExaminationID");
            int educationLevelID = Utils.GetInt(dic, "EducationLevelID");
            int examinationSubjectID = Utils.GetInt(dic, "ExaminationSubjectID");
            int detachableHeadBagID = Utils.GetInt(dic, "DetachableHeadBagID");
            //int examinationRoomID = Utils.GetInt(dic, "ExaminationRoomID");
            if (examinationSubjectID == 0)
                throw new BusinessException("ReportExam_Validate_Require");
            //Các thông tin chung
            SchoolProfile school = SchoolProfileBusiness.Find(schoolID);
            AcademicYear academicYear = AcademicYearBusiness.Find(academicYearID);
            Province province = school.Province;
            string schoolName = school.SchoolName.ToUpper();
            string provinceName = province.ProvinceName;
            string SupervisingDeptName = school.SupervisingDept.SupervisingDeptName.ToUpper();
            string academicTitle = academicYear.Year + " - " + (academicYear.Year + 1);
            string examinationName = ExaminationRepository.Find(examinationID).Title.ToUpper();
            DateTime ReportDate = DateTime.Now;
            string examinationSubjectName = ExaminationSubjectBusiness.Find(examinationSubjectID).SubjectCat.DisplayName;

            //Kiểm tra loại báo cáo theo mã học sinh hoặc là theo số báo danh hoặc là theo số phách.
            int type = ExaminationBusiness.Find(examinationID).MarkImportType;
            if (type == SystemParamsInFile.MARK_IMPORT_TYPE_PUPILCODE)
            {
                string reportCode = SystemParamsInFile.REPORT_DANH_SACH_THI_SINH_MA_HS;
                ReportDefinition reportDef = ReportDefinitionBusiness.GetByCode(reportCode);
                string templatePath = SystemParamsInFile.TEMPLATE_FOLDER + reportDef.TemplateDirectory + "/" + reportDef.TemplateName;
                IVTWorkbook oBook = VTExport.OpenWorkbook(templatePath);
                int firstRow = 9;
                IVTWorksheet firstSheet = oBook.GetSheet(1);
                //Tạo các dòng template mẫu
                IVTRange topRow = firstSheet.GetRange("A10", "E10");
                IVTRange middleRow = firstSheet.GetRange("A11", "E11");
                IVTRange bottomRow = firstSheet.GetRange("A12", "E12");
                //Lấy ra danh sách phòng thi
                List<ExaminationRoom> lstExaminationRoom = new List<ExaminationRoom>();
                IDictionary<string, object> dicExamRoom = new Dictionary<string, object>();
                dicExamRoom["ExaminationSubjectID"] = examinationSubjectID;
                dicExamRoom["ExaminationID"] = examinationID;
                dicExamRoom["EducationLevelID"] = educationLevelID;
                dicExamRoom["AppliedLevel"] = appliedLevel;
                dicExamRoom["AcademicYearID"] = academicYearID;
                lstExaminationRoom = ExaminationRoomBusiness.SearchBySchool(schoolID, dicExamRoom).ToList();
                foreach (ExaminationRoom examinationRoom in lstExaminationRoom)
                {
                    IVTWorksheet sheet = oBook.CopySheetToLast(firstSheet, "E" + (firstRow));
                    IDictionary<string, object> dicCandidate = new Dictionary<string, object>();
                    dicCandidate["ExaminationID"] = examinationID;
                    dicCandidate["EducationLevelID"] = educationLevelID;
                    dicCandidate["RoomID"] = examinationRoom.ExaminationRoomID;
                    dicCandidate["AcademicYearID"] = academicYearID;
                    dicCandidate["AppliedLevel"] = appliedLevel;

                    IQueryable<Candidate> qCandidate = CandidateBusiness.SearchBySchool(schoolID, dicCandidate);
                    var lstCandidate = (from c in qCandidate
                                        join p in PupilProfileBusiness.All on c.PupilID equals p.PupilProfileID
                                        select new
                                        {
                                            c.NamedListNumber,
                                            c.NamedListCode,
                                            p.BirthDate,
                                            p.PupilCode,
                                            p.FullName,
                                            p.Name
                                        }).ToList().OrderBy(o => o.NamedListNumber).ThenBy(o => o.Name).ThenBy(o => o.FullName).ToList();
                    Dictionary<string, object> dicGeneralInfo = new Dictionary<string, object> 
                        {
                            {"SchoolName", schoolName}, 
                            {"ProvinceName", provinceName},
                            {"SupervisingDeptName", SupervisingDeptName},                 
                            {"ExaminationName", examinationName},
                            {"AcademicYearTitle", academicTitle},
                            {"ExaminationRoomName", examinationRoom.RoomTitle.ToUpper()},
                            {"ReportDate", ReportDate},                   
                            {"ExaminationSubjectName", ExaminationSubjectRepository.Find(examinationSubjectID).SubjectCat.DisplayName.ToUpper()},                 
                        };
                    int order = 1;
                    int currentRow;
                    List<object> list = new List<object>();
                    foreach (var candidate in lstCandidate)
                    {
                        currentRow = firstRow + order;
                        if (order == 1 && lstCandidate.Count() == 1)
                        {
                            sheet.CopyPasteSameRowHeigh(bottomRow, currentRow);
                        }
                        else if (order == 1)
                        {
                            sheet.CopyPasteSameRowHeigh(topRow, currentRow);
                        }
                        else if (order == lstCandidate.Count() || order % 5 == 0)
                        {
                            sheet.CopyPasteSameRowHeigh(bottomRow, currentRow);
                        }
                        else
                        {
                            sheet.CopyPasteSameRowHeigh(middleRow, currentRow);
                        }

                        list.Add(new Dictionary<string, object> 
                                    {
                                        {"Order", order++}, 
                                        {"PupilCode", candidate.PupilCode},
                                        {"FullName", candidate.FullName},                    
                                    });
                    }
                    dicGeneralInfo.Add("list", list);
                    sheet.FillVariableValue(dicGeneralInfo);
                    sheet.Name = examinationRoom.RoomTitle;
                }
                firstSheet.Delete();
                return oBook.ToStream();
            }
            else if (type == SystemParamsInFile.MARK_IMPORT_TYPE_ORDER_NUMBER)
            {
                string reportCode = SystemParamsInFile.REPORT_DANH_SACH_THI_SINH_SBD;
                ReportDefinition reportDef = ReportDefinitionBusiness.GetByCode(reportCode);
                string templatePath = SystemParamsInFile.TEMPLATE_FOLDER + reportDef.TemplateDirectory + "/" + reportDef.TemplateName;
                IVTWorkbook oBook = VTExport.OpenWorkbook(templatePath);
                int firstRow = 9;
                IVTWorksheet firstSheet = oBook.GetSheet(1);
                //Tạo các dòng template mẫu
                IVTRange topRow = firstSheet.GetRange("A10", "E10");
                IVTRange bottomRow = firstSheet.GetRange("A11", "E11");

                //Lấy ra danh sách phòng thi
                List<ExaminationRoom> lstExaminationRoom = new List<ExaminationRoom>();
                IDictionary<string, object> dicExamRoom = new Dictionary<string, object>();
                dicExamRoom["ExaminationSubjectID"] = examinationSubjectID;
                dicExamRoom["ExaminationID"] = examinationID;
                dicExamRoom["EducationLevelID"] = educationLevelID;
                dicExamRoom["AppliedLevel"] = appliedLevel;
                dicExamRoom["AcademicYearID"] = academicYearID;
                lstExaminationRoom = ExaminationRoomBusiness.SearchBySchool(schoolID, dicExamRoom).ToList();
                foreach (ExaminationRoom examinationRoom in lstExaminationRoom)
                {
                    IVTWorksheet sheet = oBook.CopySheetToLast(firstSheet, "E" + (firstRow));
                    List<Candidate> lstCandidate = new List<Candidate>();
                    IDictionary<string, object> dicCandidate = new Dictionary<string, object>();
                    dicCandidate["ExaminationID"] = examinationID;
                    dicCandidate["EducationLevelID"] = educationLevelID;
                    dicCandidate["RoomID"] = examinationRoom.ExaminationRoomID;
                    dicCandidate["AcademicYearID"] = academicYearID;
                    dicCandidate["AppliedLevel"] = appliedLevel;

                    lstCandidate = CandidateBusiness.SearchBySchool(schoolID, dicCandidate).OrderBy(o => o.NamedListNumber).ToList();
                    Dictionary<string, object> dicGeneralInfo = new Dictionary<string, object> 
                        {
                            {"SchoolName", schoolName}, 
                            {"ProvinceName", provinceName},
                            {"SupervisingDeptName", SupervisingDeptName},                 
                            {"ExaminationName", examinationName},
                            {"AcademicYearTitle", academicTitle},
                            {"ExaminationRoomName", examinationRoom.RoomTitle.ToUpper()},
                            {"ReportDate", ReportDate},                   
                            {"ExaminationSubjectName", ExaminationSubjectRepository.Find(examinationSubjectID).SubjectCat.DisplayName.ToUpper()},                 
                        };
                    int order = 1;
                    int currentRow;
                    List<object> list = new List<object>();
                    foreach (Candidate candidate in lstCandidate)
                    {
                        currentRow = firstRow + order;

                        if (order == lstCandidate.Count() || order % 5 == 0)
                        {
                            sheet.CopyPasteSameRowHeigh(bottomRow, currentRow);
                        }
                        else
                        {
                            sheet.CopyPasteSameRowHeigh(topRow, currentRow);
                        }

                        list.Add(new Dictionary<string, object> 
                                    {
                                        {"Order", order++}, 
                                        {"NamedListCode", candidate.NamedListCode}                                        
                                    });
                    }
                    dicGeneralInfo.Add("list", list);
                    sheet.FillVariableValue(dicGeneralInfo);
                    sheet.Name = examinationRoom.RoomTitle;
                }
                firstSheet.Delete();
                return oBook.ToStream();
            }
            else if (type == SystemParamsInFile.MARK_IMPORT_TYPE_DETACHABLE_HEAD)
            {
                string reportCode = SystemParamsInFile.REPORT_DANH_SACH_THI_SINH_THEO_SO_PHACH;
                ReportDefinition reportDef = ReportDefinitionBusiness.GetByCode(reportCode);
                string templatePath = SystemParamsInFile.TEMPLATE_FOLDER + reportDef.TemplateDirectory + "/" + reportDef.TemplateName;
                IVTWorkbook oBook = VTExport.OpenWorkbook(templatePath);
                int firstRow = 9;
                IVTWorksheet firstSheet = oBook.GetSheet(1);
                //Tạo các dòng template mẫu
                IVTRange topRow = firstSheet.GetRange("A10", "D10");
                IVTRange bottomRow = firstSheet.GetRange("A11", "D11");

                List<DetachableHeadMapping> lstDetachableHeadMapping = new List<DetachableHeadMapping>();
                IDictionary<string, object> dicDetach = new Dictionary<string, object>();
                dicDetach["ExaminationID"] = examinationID;
                dicDetach["EducationLevelID"] = educationLevelID;
                dicDetach["ExaminationSubjectID"] = examinationSubjectID;
                dicDetach["AcademicYearID"] = academicYearID;
                dicDetach["AppliedLevel"] = appliedLevel;

                //Lấy danh sách túi phách
                IDictionary<string, object> dicHeadBag = new Dictionary<string, object>();
                dicHeadBag["ExaminationSubjectID"] = examinationSubjectID;
                dicHeadBag["EducationLevelID"] = educationLevelID;
                dicHeadBag["ExaminationID"] = examinationID;
                dicHeadBag["AppliedLevel"] = appliedLevel;
                dicHeadBag["AcademicYearID"] = academicYearID;
                List<DetachableHeadBag> lstDetachableHeadBag = new List<DetachableHeadBag>();
                if (detachableHeadBagID == 0)
                {
                    lstDetachableHeadBag = DetachableHeadBagBusiness.SearchBySchool(schoolID, dicHeadBag).ToList();
                }
                else
                {
                    lstDetachableHeadBag.Add(DetachableHeadBagBusiness.Find(detachableHeadBagID));
                }
                //lstDetachableHeadBag = DetachableHeadBagBusiness.SearchBySchool(schoolID, dicHeadBag).ToList();
                foreach (DetachableHeadBag detachableHeadBag in lstDetachableHeadBag)
                {
                    IVTWorksheet sheet = oBook.CopySheetToLast(firstSheet, "E" + (firstRow));

                    Dictionary<string, object> dicGeneralInfo = new Dictionary<string, object> 
                        {
                            {"SchoolName", schoolName}, 
                            {"ProvinceName", provinceName},
                            {"SupervisingDeptName", SupervisingDeptName},                 
                            {"ExaminationName", examinationName},
                            {"AcademicYearTitle", academicTitle},
                            {"DetachableHeadBag", detachableHeadBag.BagTitle},
                            {"ReportDate", ReportDate},                   
                            {"ExaminationSubjectName", ExaminationSubjectRepository.Find(examinationSubjectID).SubjectCat.DisplayName.ToUpper()},                 
                        };
                    lstDetachableHeadMapping = DetachableHeadMappingBusiness.SearchBySchool(schoolID, dicDetach).ToList();
                    lstDetachableHeadMapping = lstDetachableHeadMapping.Where(o => o.DetachableHeadBagID == detachableHeadBag.DetachableHeadBagID).OrderBy(u => u.DetachableHeadNumber).ToList();
                    int order = 1;
                    int currentRow;
                    List<object> list = new List<object>();
                    foreach (DetachableHeadMapping detachableHeadMapping in lstDetachableHeadMapping)
                    {
                        currentRow = firstRow + order;

                        if (order == lstDetachableHeadMapping.Count())
                        {
                            sheet.CopyPasteSameRowHeigh(bottomRow, currentRow);
                        }
                        else
                        {
                            sheet.CopyPasteSameRowHeigh(topRow, currentRow);
                        }

                        list.Add(new Dictionary<string, object> 
                                    {
                                        {"Order", order++}, 
                                        {"SP", detachableHeadMapping.DetachableHeadNumber},
                                        {"Description", detachableHeadMapping.Description}            
                                    });
                    }
                    dicGeneralInfo.Add("list", list);
                    sheet.FillVariableValue(dicGeneralInfo);
                    sheet.Name = detachableHeadBag.BagTitle;
                }
                firstSheet.Delete();
                return oBook.ToStream();

            }
            return null;

        }

        /// <summary>
        /// hath8
        /// 12/13/2013
        /// File nhap diem tat ca cac mon
        /// </summary>
        /// <param name="dic"></param>
        /// <returns></returns>
        public Stream CreateInputMarkFileBySBD(IDictionary<string, object> dic)
        {

            //reportName GV_[SchoolLevel]_BaoCaoSoLuongHSTheoGV_[Semester]
            int schoolID = Utils.GetInt(dic, "SchoolID");
            int academicYearID = Utils.GetInt(dic, "AcademicYearID");
            int appliedLevel = Utils.GetInt(dic, "AppliedLevel");
            int examinationID = Utils.GetInt(dic, "ExaminationID");
            int educationLevelID = Utils.GetInt(dic, "EducationLevelID");
            int examinationSubjectID = Utils.GetInt(dic, "ExaminationSubjectID");
            int detachableHeadBagID = Utils.GetInt(dic, "DetachableHeadBagID");
            //int examinationRoomID = Utils.GetInt(dic, "ExaminationRoomID");

            //Lay du lieu
            Examination Exam = ExaminationBusiness.Find(examinationID);
            SchoolProfile sp = SchoolProfileBusiness.Find(schoolID);
            AcademicYear ac = AcademicYearBusiness.Find(academicYearID);
            SupervisingDept sd = SupervisingDeptBusiness.Find(sp.SupervisingDeptID);
            Province pro = ProvinceBusiness.Find(sp.ProvinceID);
            //Gan du lieu vao dictionary de fill vao excel
            Dictionary<string, object> dicSBD = new Dictionary<string, object>();
            dicSBD["SupervisingDept"] = sd.SupervisingDeptName;
            dicSBD["SchoolName"] = sp.SchoolName;
            dicSBD["Examination"] = Exam.Title;
            dicSBD["AcademicYear"] = ac.DisplayTitle;
            dicSBD["ProvinceName"] = pro.ProvinceName;
            dicSBD["Day"] = DateTime.Now.Day;
            dicSBD["Month"] = DateTime.Now.Month;
            dicSBD["Year"] = DateTime.Now.Year;

            string reportCode = SystemParamsInFile.REPORT_DANH_SACH_THI_SINH_DIEM_DANH_SBD;
            ReportDefinition reportDef = ReportDefinitionBusiness.GetByCode(reportCode);
            string templatePath = SystemParamsInFile.TEMPLATE_FOLDER + reportDef.TemplateDirectory + "/" + reportDef.TemplateName;
            IVTWorkbook oBook = VTExport.OpenWorkbook(templatePath);
            //Lấy sheet template
            IVTWorksheet firstSheet = oBook.GetSheet(1);
            //Tao template chuan

            //KHởi tạo một list để add các phòng thi vào.
            List<string> lstExam = new List<string>();
            IDictionary<string, object> dicExamRoom = new Dictionary<string, object>();
            dicExamRoom["ExaminationSubjectID"] = examinationSubjectID;
            dicExamRoom["ExaminationID"] = examinationID;
            dicExamRoom["EducationLevelID"] = educationLevelID;
            dicExamRoom["AppliedLevel"] = appliedLevel;
            dicExamRoom["AcademicYearID"] = academicYearID;
            lstExam = ExaminationRoomBusiness.SearchBySchool(schoolID, dicExamRoom).Select(o => o.RoomTitle).ToList().OrderBy(o => o).Distinct().ToList();

            //Lấy ra danh sách thí sinh
            IDictionary<string, object> dicCandidate = new Dictionary<string, object>();
            dicCandidate["ExaminationID"] = examinationID;
            dicCandidate["EducationLevelID"] = educationLevelID;
            //dicCandidate["RoomID"] = er.ExaminationRoomID;
            dicCandidate["AcademicYearID"] = academicYearID;
            dicCandidate["AppliedLevel"] = appliedLevel;

            IQueryable<Candidate> qCandidate = CandidateBusiness.SearchBySchool(schoolID, dicCandidate);
            var lstCandidate = (from c in qCandidate
                                join p in PupilProfileBusiness.All on c.PupilID equals p.PupilProfileID
                                join cp in ClassProfileBusiness.All on c.ClassID equals cp.ClassProfileID
                                where cp.IsActive.Value
                                select new
                                {                 
                                    c.ExaminationRoom.RoomTitle,
                                    c.NamedListCode,
                                    c.NamedListNumber,

                                }).ToList().Distinct().OrderBy(o => o.NamedListNumber).ToList();

            //Lấy danh sách môn thi theo phòng thi
            List<ExaminationSubject> lstExamSubject = ExaminationSubjectBusiness.SearchBySchool(schoolID, dicExamRoom).Distinct().OrderBy(o => o.SubjectCat.OrderInSubject).ToList();
            

           
            
            //Lay range
            IVTRange RanSubject = firstSheet.GetRange("C9", "C10");
            IVTRange RangeTop = firstSheet.GetRange("A11", "B11");
            IVTRange RangeMid = firstSheet.GetRange("A12", "B12");
            IVTRange RangeBot = firstSheet.GetRange("A13", "B13");
            IVTRange RangeNote = firstSheet.GetRange("D9", "D10");
            IVTRange RangeTopNote = firstSheet.GetRange("D11", "D11");
            IVTRange RangeMidNote = firstSheet.GetRange("D12", "D12");
            IVTRange RangeBotNote = firstSheet.GetRange("D13", "D13");
            IVTRange RangeTopSub = firstSheet.GetRange("C11", "C11");
            IVTRange RangeMidSub = firstSheet.GetRange("C12", "C12");
            IVTRange RangeBotSub = firstSheet.GetRange("C13", "C13");
            foreach (string RoomTitle in lstExam)
            {
                

                //Fill Tieu de
                IVTWorksheet sheet = oBook.CopySheetToLast(firstSheet, "K8");
                sheet.SetCellValue("A7", "Phòng thi : " + RoomTitle);
                //STT va SBD
                sheet.CopyPasteSameSize(firstSheet.GetRange("A9", "B10"), "A9");
                //Diem thi va mon thi
                //Khai bao cac bien dong
                int startrow = 11;
                int startcolsub = 3;
                foreach(ExaminationSubject es in lstExamSubject)
                {
                    
                    sheet.CopyPasteSameSize(RanSubject, 9, startcolsub);
                    sheet.SetCellValue(10, startcolsub, es.SubjectCat.SubjectName);
                    sheet.GetRange(10, startcolsub, 10, startcolsub).IsLock = true;
                    startcolsub++;
                }
                //merge dong diem thi
                sheet.GetRange(9, 3, 9, startcolsub-1).Merge();
                //Fill ghi chu
                sheet.CopyPasteSameSize(RangeNote, 9, startcolsub);
                //Fill danh sach hoc sinh
                //STT
                int i = 1;
                var listtemp = lstCandidate.Where(o => o.RoomTitle == RoomTitle).OrderBy(o=>o.RoomTitle).ToList();
                foreach (var item in listtemp)
                {
                    //Fill style diem thi
                    int startcolsubject = 3;
                    foreach (ExaminationSubject es in lstExamSubject)
                    {
                        if (i == 1 && i < listtemp.Count)
                        {
                            sheet.CopyPaste(RangeTopSub, startrow, startcolsubject, true);
                        }
                        if (i > 1 && i < listtemp.Count)
                        {
                            sheet.CopyPaste(RangeMidSub, startrow, startcolsubject, true);
                        }
                        if (i == listtemp.Count || listtemp.Count == 0)
                        {
                            sheet.CopyPaste(RangeBotSub, startrow, startcolsubject, true);
                        }
                        startcolsubject++;
                    }
                    if (i == 1 && i < listtemp.Count)
                    {
                        sheet.CopyPaste(RangeTop, startrow, 1, true);
                        sheet.CopyPaste(RangeTopNote, startrow, startcolsubject, true);
                    }
                    if (i > 1 && i < listtemp.Count)
                    {
                        sheet.CopyPaste(RangeMid, startrow, 1, true);
                        sheet.CopyPaste(RangeMidNote, startrow, startcolsubject, true);
                    }
                    if (i == listtemp.Count || listtemp.Count == 0)
                    {
                        sheet.CopyPaste(RangeBot, startrow, 1, true);
                        sheet.CopyPaste(RangeBotNote, startrow, startcolsubject, true);
                    }
                    //STT
                    sheet.SetCellValue(startrow, 1, i);
                    //SBD
                    sheet.SetCellValue(startrow, 2, item.NamedListCode);
                    
                    startrow++;
                    i++;
                }
                sheet.GetRange(11, 2, startrow, 2).IsLock = true;
                sheet.FillVariableValue(dicSBD);
                sheet.Name = RoomTitle;
                sheet.FitAllColumnsOnOnePage = true;
                sheet.ProtectSheet();
                
            }
            

            firstSheet.Delete();
            return oBook.ToStream();
        }
        /// <summary>
        /// Báo cáo học sinh vi phạm
        /// Author: Tamhm1
        /// Date: 19/12/2012
        /// </summary>
        /// <param name="dic"></param>
        /// <returns></returns>
        public Stream CreateViolatedCandidateReport(IDictionary<string, object> dic)
        {
            int examinationID = Utils.GetInt(dic, "ExaminationID");
            int educationLevelID = Utils.GetInt(dic, "EducationLevelID");
            int examinationSubjectID = Utils.GetInt(dic, "ExaminationSubjectID");
            int appliedLevel = Utils.GetInt(dic, "AppliedLevel");
            int academicYearID = Utils.GetInt(dic, "AcademicYearID");
            int schoolID = Utils.GetInt(dic, "SchoolID");
            //KHởi tạo dicViolate
            IDictionary<string, object> dicViolate = new Dictionary<string, object>();
            dicViolate["ExaminationID"] = examinationID;
            dicViolate["EducationLevelID"] = educationLevelID;
            dicViolate["AppliedLevel"] = appliedLevel;
            dicViolate["AcademicYearID"] = academicYearID;
            dicViolate["ExaminationSubjectID"] = examinationSubjectID;
            IQueryable<ViolatedCandidate> qViolatedCandidate = ViolatedCandidateBusiness.SearchBySchool(schoolID, dicViolate);

            //Danh sách thí sinh vi phạm
            var lstViolatedCandidate = (from v in qViolatedCandidate
                                        join p in PupilProfileBusiness.All on v.PupilID equals p.PupilProfileID
                                        select new
                                        {
                                            NameListCode = v.NameListCode,
                                            EduResolution = v.EducationLevel.Resolution,
                                            RoomTitle = v.ExaminationRoom.RoomTitle,
                                            NamedListNumber = v.Candidate.NamedListNumber,
                                            ViolationDetail = v.ViolationDetail,
                                            FullName = p.FullName,
                                            BirthDate = p.BirthDate,
                                            Genre = p.Genre,
                                            SubjectName = v.ExaminationSubject.SubjectCat.DisplayName,
                                            Name = p.Name,
                                            ExamViolationTypeName = v.ExamViolationType.Resolution
                                        }).ToList().OrderBy(o => o.NamedListNumber).ThenBy(o => o.Name).ThenBy(o => o.FullName).ToList();

            //Các thông tin chung
            SchoolProfile school = SchoolProfileBusiness.Find(schoolID);
            AcademicYear academicYear = AcademicYearBusiness.Find(academicYearID);
            Province province = school.Province;
            string schoolName = school.SchoolName.ToUpper();
            string provinceName = province.ProvinceName;
            string SupervisingDeptName = school.SupervisingDept.SupervisingDeptName.ToUpper();
            string academicYearTitle = academicYear.Year + " - " + (academicYear.Year + 1);
            string examinationName = ExaminationRepository.Find(examinationID).Title.ToUpper();
            //DateTime ReportDate = DateTime.Now;
            string date = string.Format("{0}, ngày {1} tháng {2} năm {3}"
                , provinceName
                , DateTime.Now.Day < 10 ? "0"+ DateTime.Now.Day.ToString() : DateTime.Now.Day.ToString()
                , DateTime.Now.Month < 10 ? "0" + DateTime.Now.Month.ToString() : DateTime.Now.Month.ToString()
                , DateTime.Now.Year);

            //Fill các thông tin chung cho template
            Dictionary<string, object> dicGeneralInfo = new Dictionary<string, object> 
                        {
                            {"SchoolName", schoolName}, 
                            {"ProvinceName", provinceName},
                            {"SupervisingDept", SupervisingDeptName},                 
                            {"ExaminationName", examinationName},
                            {"AcademicYear", academicYearTitle},                        
                            {"ProvinceDate", date}                                                            
                        };
            //Tạo Sheet
            string reportCode = SystemParamsInFile.REPORT_THI_SINH_VPQC;
            ReportDefinition reportDef = ReportDefinitionBusiness.GetByCode(reportCode);
            string templatePath = SystemParamsInFile.TEMPLATE_FOLDER + reportDef.TemplateDirectory + "/" + reportDef.TemplateName;
            IVTWorkbook oBook = VTExport.OpenWorkbook(templatePath);
            int firstRow = 10;
            IVTWorksheet firstSheet = oBook.GetSheet(1);
            IVTWorksheet sheet = oBook.CopySheetToLast(firstSheet, "J" + (firstRow));
            //Tạo các dòng template mẫu
            IVTRange topRow = firstSheet.GetRange("A11", "J11");
            IVTRange bottomRow = firstSheet.GetRange("A13", "J13");
            IVTRange middleRow = firstSheet.GetRange("A12", "J12");
            int order = 1;
            int currentRow;
            //KHởi tạo một list
            List<object> list = new List<object>();
            foreach (var violatedCandidate in lstViolatedCandidate)
            {
                currentRow = firstRow + order;
                if (order == 1 && lstViolatedCandidate.Count() == 1)
                {
                    sheet.CopyPasteSameRowHeigh(bottomRow, currentRow);
                }
                else if (order == 1)
                {
                    sheet.CopyPasteSameRowHeigh(topRow, currentRow);
                }
                else if (order == lstViolatedCandidate.Count() || order % 5 == 0)
                {
                    sheet.CopyPasteSameRowHeigh(bottomRow, currentRow);
                }
                else
                {
                    sheet.CopyPasteSameRowHeigh(middleRow, currentRow);
                }
                list.Add(new Dictionary<string, object> 
                                    {
                                        {"Index", order++}, 
                                        {"NameListCode", violatedCandidate.NameListCode},
                                        {"FullName", violatedCandidate.FullName},
                                        {"BirthDate", violatedCandidate.BirthDate.ToString("dd/MM/yyyy")},
                                        {"GenreTitle", SMASConvert.ConvertGenre(violatedCandidate.Genre)},
                                        {"EducationLevelResolution", violatedCandidate.EduResolution},
                                        {"SubjectName", violatedCandidate.SubjectName },
                                        {"RoomTitle", violatedCandidate.RoomTitle},
                                        {"ViolationDetail", violatedCandidate.ViolationDetail},
                                        {"ExamViolationTypeResolution", violatedCandidate.ExamViolationTypeName}
                                    });
            }
            dicGeneralInfo.Add("list", list);
            sheet.Name = "TSViPham";
            sheet.FillVariableValue(dicGeneralInfo);
            firstSheet.Delete();
            return oBook.ToStream();
        }

        /// <summary>
        /// Báo cáo giáo viên vi phạm quy chế thi
        /// Author: Tamhm1
        /// Date: 19/12/2012
        /// </summary>
        /// <param name="dic"></param>
        /// <returns></returns>
        public Stream ViolatedInvilgilatorReport(IDictionary<string, object> dic)
        {
            int examinationID = Utils.GetInt(dic, "ExaminationID");
            int educationLevelID = Utils.GetInt(dic, "EducationLevelID");
            int examinationSubjectID = Utils.GetInt(dic, "ExaminationSubjectID");
            int appliedLevel = Utils.GetInt(dic, "AppliedLevel");
            int academicYearID = Utils.GetInt(dic, "AcademicYearID");
            int schoolID = Utils.GetInt(dic, "SchoolID");
            //Danh sách thí sinh vi phạm
            //KHởi tạo dicViolate
            IDictionary<string, object> dicViolate = new Dictionary<string, object>();
            dicViolate["ExaminationID"] = examinationID;
            dicViolate["EducationLevelID"] = educationLevelID;
            dicViolate["AppliedLevel"] = appliedLevel;
            dicViolate["AcademicYearID"] = academicYearID;
            dicViolate["ExaminationSubjectID"] = examinationSubjectID;
            IQueryable<ViolatedInvigilator> qViolatedInvigilator = ViolatedInvigilatorBusiness.SearchBySchool(schoolID, dicViolate);

            //Danh sách thí sinh vi phạm
            var lstViolatedInvigilator = (from v in qViolatedInvigilator
                                          join e in EmployeeBusiness.All on v.TeacherID equals e.EmployeeID
                                          select new
                                          {
                                              EduResolution = v.EducationLevel.Resolution,
                                              RoomTitle = v.ExaminationRoom.RoomTitle,
                                              ViolationDetail = v.ViolationDetail,
                                              FullName = e.FullName,
                                              BirthDate = e.BirthDate,
                                              Genre = e.Genre,
                                              Name = e.Name,
                                              DisciplinedForm = v.DisciplinedForm,
                                              SubjectName = v.ExaminationSubject.SubjectCat.DisplayName
                                          }).OrderBy(o => o.Name).ThenBy(o => o.FullName).ToList();
            //Các thông tin chung
            SchoolProfile school = SchoolProfileBusiness.Find(schoolID);
            AcademicYear academicYear = AcademicYearBusiness.Find(academicYearID);
            Province province = school.Province;
            string schoolName = school.SchoolName.ToUpper();
            string provinceName = province.ProvinceName;
            string SupervisingDeptName = school.SupervisingDept.SupervisingDeptName.ToUpper();
            string academicYearTitle = academicYear.Year + " - " + (academicYear.Year + 1);
            string examinationName = ExaminationRepository.Find(examinationID).Title.ToUpper();
            
            //string date = provinceName + " ,Ngày " + DateTime.Now.ToString("dd/MM/yyyy");
            string date = string.Format("{0}, ngày {1} tháng {2} năm {3}"
                , provinceName
                , DateTime.Now.Day < 10 ? "0" + DateTime.Now.Day.ToString() : DateTime.Now.Day.ToString()
                , DateTime.Now.Month < 10 ? "0" + DateTime.Now.Month.ToString() : DateTime.Now.Month.ToString()
                , DateTime.Now.Year);

            //string examinationSubjectName = ExaminationSubjectBusiness.Find(examinationSubjectID).SubjectCat.DisplayName;
            //Fill các thông tin chung cho template
            Dictionary<string, object> dicGeneralInfo = new Dictionary<string, object> 
                        {
                            {"SchoolName", schoolName}, 
                            {"ProvinceName", provinceName},
                            {"SupervisingDeptName", SupervisingDeptName},                 
                            {"ExaminationName", examinationName},
                            {"AcademicYearTitle", academicYearTitle},                        
                            {"ProvinceDate", date}                                                            
                        };
            //Tạo Sheet
            string reportCode = SystemParamsInFile.REPORT_GIAM_THI_VPQC;
            ReportDefinition reportDef = ReportDefinitionBusiness.GetByCode(reportCode);
            string templatePath = SystemParamsInFile.TEMPLATE_FOLDER + reportDef.TemplateDirectory + "/" + reportDef.TemplateName;
            IVTWorkbook oBook = VTExport.OpenWorkbook(templatePath);
            
            IVTWorksheet firstSheet = oBook.GetSheet(1);
            
            int firstRow = 11;
            int order = 0;
            int currentRow;
            IVTWorksheet sheet = oBook.CopySheetToLast(firstSheet, null);
            //KHởi tạo một list
            List<object> list = new List<object>();
            foreach (var violatedInvigilator in lstViolatedInvigilator)
            {
                currentRow = firstRow + order;
                // Dinh dang border
                sheet.GetRange("A" + currentRow, "I" + currentRow).SetBorder(VTBorderStyle.Solid, VTBorderWeight.Medium, VTBorderIndex.EdgeLeft); // Canh ben trai
                sheet.GetRange("A" + currentRow, "I" + currentRow).SetBorder(VTBorderStyle.Solid, VTBorderWeight.Medium, VTBorderIndex.EdgeRight); // Canh ben phai
                sheet.GetRange("A" + currentRow, "I" + currentRow).SetBorder(VTBorderStyle.Dashed, VTBorderWeight.Thin, VTBorderIndex.InsideAll); // Canh ben trong
                // Canh phia duoi
                if ((order + 1) % 5 == 0 || (order + 1) == lstViolatedInvigilator.Count) // Neu hoc sinh o vi tri chia het cho 5 hoac cuoi cung thi dien net dam
                {
                    sheet.GetRange("A" + currentRow, "I" + currentRow).SetBorder(VTBorderStyle.Solid, VTBorderWeight.Medium, VTBorderIndex.EdgeBottom);
                }
                else // Neu hoc sinh o vi tri chia het cho 5 hoac cuoi cung thi dien net dut
                {
                    sheet.GetRange("A" + currentRow, "I" + currentRow).SetBorder(VTBorderStyle.Dashed, VTBorderWeight.Thin, VTBorderIndex.EdgeBottom);
                }
                list.Add(new Dictionary<string, object> 
                                    {
                                        {"Index", order + 1},
                                        {"FullName", violatedInvigilator.FullName},
                                        {"BirthDate", violatedInvigilator.BirthDate.HasValue ? violatedInvigilator.BirthDate.Value.ToString("dd/MM/yyyy") : ""},
                                        {"Genre", violatedInvigilator.Genre == true? "Nam":"Nữ"},
                                        {"EducationLevelName", violatedInvigilator.EduResolution},
                                        {"SubjectName", violatedInvigilator.SubjectName },
                                        {"ExaminationRoomName", violatedInvigilator.RoomTitle},
                                        {"ViolateDetail", violatedInvigilator.ViolationDetail},
                                        {"Process", violatedInvigilator.DisciplinedForm}
                                    });
                order++;
            }
            dicGeneralInfo.Add("list", list);

            sheet.FillVariableValue(dicGeneralInfo);
            firstSheet.Delete();
            sheet.Name = "GiamThiVPQC";
            return oBook.ToStream();
        }
    }

}

