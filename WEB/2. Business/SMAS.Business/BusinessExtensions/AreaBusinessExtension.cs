/**
 * The Viettel License
 *
 * Copyright 2012 Viettel Telecom. All rights reserved.
 * VIETTEL areaPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */

/** 
* @author  
* @version $Revision: $
*/

using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Linq.Expressions;
using System.Web;
using log4net;

using SMAS.DAL.Repository;
using SMAS.DAL.IRepository;
using SMAS.Business.IBusiness;
using SMAS.Models.Models;
using SMAS.Business.Common;

namespace SMAS.Business.Business
{
    /// <summary>
    /// 
    /// </summary>
    public partial class AreaBusiness
    {
        #region private member variable
        private const int RESOLUTION_MAX_LENGTH = 50;   //Độ dài trường diễn giải
        private const int DESCRIPTION_MAX_LENGTH = 400; //Độ dài ghi chú
        private const int AREACODE_MAX_LENGTH = 10; //Độ dài mã khu vực
        private const int AREANAME_MAX_LENGTH = 50; //Độ dài tên  KHU VỰC
        #endregion

       
        #region base method
        /// <summary>
        /// Tìm kiếm khu vực
        /// </summary>
        /// <author>hath</author>
        /// <date>4/9/2012</date>
        /// <param name="area">object area.</param>
        /// <param name="AreaName"> Tên khu vực</param>
        /// <param name="AreaCode">Mã khu vực</param>
        /// <param name="Description">Mô tả khu vực</param>
        /// <param name="IsActive">Biến kiểm tra có active hay ko</param>
        /// <returns> IQueryable<Area></returns>
        public IQueryable<Area> Search(IDictionary<string, object> dic)
        {
            string AreaName = Utils.GetString(dic, "AreaName"); 
            string AreaCode = Utils.GetString(dic, "AreaCode"); 
            string Description = Utils.GetString(dic, "Description"); 
            bool? IsActive = Utils.GetIsActive(dic, "IsActive"); 
            IQueryable<Area> lsArea = AreaRepository.All; 

            if (IsActive.HasValue)
                lsArea = lsArea.Where(area => area.IsActive == IsActive);
            if (AreaName.Trim().Length != 0)
                lsArea = lsArea.Where(area => area.AreaName.Contains(AreaName.ToLower()));            
            if (AreaCode.Trim().Length != 0)
                lsArea = lsArea.Where(area => area.AreaCode.ToLower().Contains(AreaCode.ToLower()));
            //if (Description.Trim().Length != 0)
            //    lsArea = lsArea.Where(area => area.Description.Contains(Description.ToLower()));

            return lsArea;

        }
        /// <summary>
        /// Thêm khu vực
        /// </summary>
        /// <author>hath</author>
        /// <date>4/9/2012</date>
        /// <param name="area">object area.</param>
        /// <returns>objiect area</returns>
        public override Area Insert(Area area)
        {
            //Mã khu vực không được để trống 
            Utils.ValidateRequire(area.AreaCode, "Area_Label_AreaCode");
            //Độ dài mã khu vực không được vượt quá 10 
            Utils.ValidateMaxLength(area.AreaCode, AREACODE_MAX_LENGTH, "Area_Label_AreaCode");
            //Mã khu vực đã tồn tại 
            new AreaBusiness(null).CheckDuplicate(area.AreaCode, GlobalConstants.LIST_SCHEMA, "Area", "AreaCode", true, area.AreaID, "Area_Label_AreaCode");
            //Tên khu vực không được để trống 
            Utils.ValidateRequire(area.AreaName, "Area_Label_AreaName");
            //Độ dài trường tên khu vực không được vượt quá 50 
            Utils.ValidateMaxLength(area.AreaName, AREANAME_MAX_LENGTH, "Area_Label_AreaName");
            //Tên khu vực đã tồn tại 
            new AreaBusiness(null).CheckDuplicate(area.AreaName, GlobalConstants.LIST_SCHEMA, "Area", "AreaName", true, area.AreaID, "Area_Label_AreaName");
            //Độ dài ghi chú không được vượt quá 400 
            Utils.ValidateMaxLength(area.Description, DESCRIPTION_MAX_LENGTH, "Description");


            //Insert
            base.Insert(area);
            return area;

        }
        /// <summary>
        /// Sửa khu vực
        /// </summary>
        /// <author>hath</author>
        /// <date>4/9/2012</date>
        /// <param name="area">object area.</param>
        /// <returns>objiect area</returns>
        public override Area Update(Area area)
        {
            //Bạn chưa chọn khu vực cần sửa hoặc khu vực bạn chọn đã bị xóa khỏi hệ thống
            new AreaBusiness(null).CheckAvailable((int)area.AreaID, "Area_Label_AreaID", true);
            //Mã khu vực không được để trống 
            Utils.ValidateRequire(area.AreaCode, "Area_Label_AreaCode");
            //Độ dài mã khu vực không được vượt quá 10 
            Utils.ValidateMaxLength(area.AreaCode, AREACODE_MAX_LENGTH, "Area_Label_AreaCode");
            //Mã khu vực đã tồn tại 
            new AreaBusiness(null).CheckDuplicate(area.AreaCode, GlobalConstants.LIST_SCHEMA, "Area", "AreaCode", true, area.AreaID, "Area_Label_AreaCode");
            //Tên khu vực không được để trống 
            Utils.ValidateRequire(area.AreaName, "Area_Label_AreaName");
            //Độ dài trường tên khu vực không được vượt quá 50 
            Utils.ValidateMaxLength(area.AreaName, AREANAME_MAX_LENGTH, "Area_Label_AreaName");
            //Tên khu vực đã tồn tại 
            new AreaBusiness(null).CheckDuplicate(area.AreaName, GlobalConstants.LIST_SCHEMA, "Area", "AreaName", true, area.AreaID, "Area_Label_AreaName");
            //Độ dài ghi chú không được vượt quá 400 
            Utils.ValidateMaxLength(area.Description, DESCRIPTION_MAX_LENGTH, "Description");

            base.Update(area);
            return area;

        }
        /// <summary>
        /// Xóa khu vực
        /// </summary>
        /// <author>hath</author>
        /// <date>4/9/2012</date>
        /// <param name="AreaID"></param>
        public void Delete(int AreaID)
        {
            //Bạn chưa chọn khu vực cần xóa hoặc khu vực bạn chọn đã bị xóa khỏi hệ thống
            new AreaBusiness(null).CheckAvailable((int)AreaID, "Area_Label_AreaID", true);

            //Không thể xóa khu vực đang sử dụng
            new AreaBusiness(null).CheckConstraints(GlobalConstants.LIST_SCHEMA, "Area", AreaID, "Area_Label_AreaID");

            base.Delete(AreaID, true);
        }
        #endregion
    }
}
