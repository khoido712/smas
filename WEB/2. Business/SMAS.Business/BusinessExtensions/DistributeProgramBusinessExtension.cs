/**
 * The Viettel License
 *
 * Copyright 2012 Viettel Telecom. All rights reserved.
 * VIETTEL PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */

/** 
* @author  
* @version $Revision: $
*/

using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Linq.Expressions;
using System.Web;
using log4net;

using SMAS.DAL.Repository;
using SMAS.DAL.IRepository;
using SMAS.Business.IBusiness;
using SMAS.Models.Models;
using SMAS.Business.Common;
using SMAS.Business.BusinessObject;

namespace SMAS.Business.Business
{ 
    public partial class DistributeProgramBusiness
    {
        public IQueryable<DistributeProgram> Search(IDictionary<string,object>dic)
        {
            int AcademicYearID = Utils.GetInt(dic, "AcademicYearID");
            int SchoolID = Utils.GetInt(dic, "SchoolID");
            int SubjectID = Utils.GetInt(dic, "SubjectID");
            int EducationLevelID = Utils.GetInt(dic,"EducationLevelID");
            int SchoolWeekID = Utils.GetInt(dic, "SchoolWeekID");
            int DistributeProgramOrder = Utils.GetInt(dic, "DistributeProgramOrder");
            int PartitionID = UtilsBusiness.GetPartionId(SchoolID);
            List<int> lstSchoolWeekID = Utils.GetIntList(dic, "lstSchoolWeekID");

            

            IQueryable<DistributeProgram> query = DistributeProgramBusiness.All;
            if (AcademicYearID != 0)
            {
                query = query.Where(p => p.AcademicYearID == AcademicYearID);
            }
            if (SchoolID != 0)
            {
                query = query.Where(p => p.SchoolID == SchoolID && p.Last2DigitNumberSchool == PartitionID);
            }
            if (SubjectID != 0)
            {
                query = query.Where(p => p.SubjectID == SubjectID);
            }
            if (EducationLevelID != 0)
            {
                query = query.Where(p => p.EducationLevelID == EducationLevelID);
            }
            if (SchoolWeekID != 0)
            {
                query = query.Where(p => p.SchoolWeekID == SchoolWeekID);
            }
            else if (lstSchoolWeekID.Count > 0)
            {
                query = query.Where(p => lstSchoolWeekID.Contains(p.SchoolWeekID));
            }

            if (DistributeProgramOrder != 0)
            {
                query = query.Where(p => p.DistributeProgramOrder == DistributeProgramOrder);
            }
            return query;
        }
        public List<DistributeProgramBO> getListDistributeProgram(IDictionary<string, object> SearchInfo)
        {
            int SchoolWeekID = Utils.GetInt(SearchInfo, "SchoolWeekID");
            int SubjectID = Utils.GetInt(SearchInfo, "SubjectID");
            int EducationLevelID = Utils.GetInt(SearchInfo, "EducationLevelID");
            int SchoolID = Utils.GetInt(SearchInfo, "SchoolID");
            int AcademicYearID = Utils.GetInt(SearchInfo, "AcademicYearID");
            int ClassID = Utils.GetInt(SearchInfo, "ClassID");
            int? AssignSubjectID = Utils.GetNullableInt(SearchInfo, "AssignSubjectID");

            var dpQuery = DistributeProgramBusiness.All.Where(
                x => x.EducationLevelID == EducationLevelID
                    //&& x.SubjectID == SubjectID
                    && x.SchoolID == SchoolID
                    && x.AcademicYearID == AcademicYearID
                    //&& x.ClassID == ClassID
                    && x.SchoolWeekID > 0
                //&& ((SchoolWeekID == 0 && x.SchoolWeekID > 0) || x.SchoolWeekID == SchoolWeekID)
                );

            if (ClassID > 0)
            {
                dpQuery = dpQuery.Where(x => x.ClassID == ClassID);
            }

            if (SubjectID > 0)
            {
                dpQuery = dpQuery.Where(x => x.SubjectID == SubjectID);
            }

            if (SchoolWeekID > 0)
            {
                dpQuery = dpQuery.Where(x => x.SchoolWeekID == SchoolWeekID);
            }

            if (AssignSubjectID.HasValue)
            {
                dpQuery = dpQuery.Where(x => x.AssignSubjectID == AssignSubjectID);
            }
            else
            {
                dpQuery = dpQuery.Where(x => x.AssignSubjectID.HasValue == false);
            }

            List<DistributeProgramBO> lsDistributeProgram = (from dp in dpQuery
                                                             join sw in SchoolWeekBusiness.All on dp.SchoolWeekID equals sw.SchoolWeekID
                                                             select new DistributeProgramBO
                                                             {
                                                                 DistributeProgramID = dp.DistributeProgramID,
                                                                 DistributeProgramOrder = dp.DistributeProgramOrder,
                                                                 EducationLevelID = dp.EducationLevelID,
                                                                 SubjectID = dp.SubjectID,
                                                                 LessonName = dp.LessonName,
                                                                 SchoolWeekID = dp.SchoolWeekID,
                                                                 ClassID = dp.ClassID,
                                                                 AssignSubjectID = dp.AssignSubjectID,
                                                                 Note = dp.Note,
                                                                 FromDate = sw.FromDate,
                                                                 ToDate = sw.ToDate,
                                                                 OrderWeek = sw.OrderWeek,
                                                                 AdcademicYearID = dp.AcademicYearID,
                                                                 schoolID = dp.SchoolID
                                                             })
                                                             .OrderBy(x=>x.OrderWeek).ThenBy(x=>x.DistributeProgramOrder)
                                                             .ToList();


                return lsDistributeProgram;
        }
    }
}
