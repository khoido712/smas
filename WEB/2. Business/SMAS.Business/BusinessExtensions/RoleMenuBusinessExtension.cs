/**
 * The Viettel License
 *
 * Copyright 2012 Viettel Telecom. All rights reserved.
 * VIETTEL PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */

/** 
* @author  
* @version $Revision: $
*/

using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Linq.Expressions;
using System.Web;
using log4net;

using SMAS.DAL.Repository;
using SMAS.DAL.IRepository;
using SMAS.Business.IBusiness;
using SMAS.Models.Models;
using SMAS.Business.Common;
namespace SMAS.Business.Business
{
    public partial class RoleMenuBusiness
    {
        IRoleMenuRepository RoleMenuRepository;
        public RoleMenuBusiness(ILog logger, SMASEntities context = null)
            : base(logger)
        {
            if (context == null) { context = new SMASEntities(); } this.context = context;
            this.RoleMenuRepository = new RoleMenuRepository(context);
            repository = RoleMenuRepository;
        }

        public void DeleteRoleOfMenu(int menuId)
        {
            if (RoleMenuRepository.All.Where(rm => rm.MenuID == menuId).Count() != 0)
            {
                foreach (var item in RoleMenuRepository.All.Where(rm => rm.MenuID == menuId).ToList())
                {
                    RoleMenuRepository.Delete(item.RoleMenuID);
                }
                RoleMenuRepository.Save();
            }
        }


        public bool AssignMenuForRoles(int menuID, List<int> listRoleID, int roleID)
        {
            if (menuID == 0 || listRoleID == null || roleID == 0)
            {
                return false;
            }
            foreach (int id in listRoleID)
            {
                RoleMenu rm = new RoleMenu { MenuID = menuID, RoleID = id };
                RoleMenuRepository.Insert(rm);
            }
            return true;
        }

        public IQueryable<Menu> GetMenuOfRole(int RoleID, bool mnuVisible = true)
        {
            if (RoleID == 0) return null;
            IQueryable<RoleMenu> roleMenus = RoleMenuRepository.All.Where(rm => rm.RoleID == RoleID);
            if (roleMenus == null) return null;
            if (mnuVisible)
            {
                roleMenus = roleMenus.Where(o => o.Menu.IsActive && o.Menu.IsVisible);
            }
            else
            {
                roleMenus = roleMenus.Where(o => o.Menu.IsActive);
            }
            return roleMenus.Select(o => o.Menu).OrderBy(m => m.OrderNumber);
        }

        public IQueryable<Role> GetRoleOfMenu(int menuID)
        {
            if (menuID == 0) return null;
            IQueryable<RoleMenu> roleMenus = RoleMenuRepository.All.Where(rm => rm.MenuID == menuID);
            if (roleMenus == null) return null;
            roleMenus = roleMenus.Where(o => o.Role.IsActive);
            return roleMenus.Select(o => o.Role);
        }

    }
}