﻿/**
 * The Viettel License
 *
 * Copyright 2012 Viettel Telecom. All rights reserved.
 * VIETTEL PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */

/** 
* @author  
* @version $Revision: $
*/

using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Linq.Expressions;
using System.Web;
using log4net;
using SMAS.Business.Common;
using SMAS.DAL.Repository;
using SMAS.DAL.IRepository;
using SMAS.Business.IBusiness;
using SMAS.Models.Models;
using SMAS.Business.BusinessObject;
using System.IO;
using SMAS.VTUtils.Excel.Export;

namespace SMAS.Business.Business
{ 
    public partial class ExamPupilViolateBusiness
    {
        public IQueryable<ExamPupilViolate> Search(IDictionary<string, object> search)
        {
            long examPupilViolateID = Utils.GetLong(search, "ExamPupilViolateID");
            long examinationsID = Utils.GetLong(search, "ExaminationsID");
            long examGroupID = Utils.GetLong(search, "ExamGroupID");
            int subjectID = Utils.GetInt(search, "SubjectID");
            long examRoomID = Utils.GetLong(search, "ExamRoomID");
            long examPupilID = Utils.GetLong(search, "ExamPupilID");
            int examViolationTypeID = Utils.GetInt(search, "ExamViolateTypeID");

            IQueryable<ExamPupilViolate> query = this.All;

            if (examPupilViolateID != 0)
            {
                query = query.Where(o => o.EamPupilViolateID == examPupilViolateID);
            }
            if (examinationsID != 0)
            {
                query = query.Where(o => o.ExaminationsID == examinationsID);
            }
            if (examGroupID != 0)
            {
                query = query.Where(o => o.ExamGroupID == examGroupID);
            }
            if (subjectID != 0)
            {
                query = query.Where(o => o.SubjectID == subjectID);
            }
            if (examRoomID != 0)
            {
                query = query.Where(o => o.ExamRoomID == examRoomID);
            }
            if (examPupilID != 0)
            {
                query = query.Where(o => o.ExamPupilID == examPupilID);
            }
            if (examViolationTypeID != 0)
            {
                query = query.Where(o => o.ExamViolationTypeID == examViolationTypeID);
            }

            return query;
        }

        public IQueryable<ExamViolationType> SearchViolationType(IDictionary<string, object> search)
        {
            int schoolID = Utils.GetInt(search, "SchoolID");
            bool? isActive = Utils.GetBool(search, "IsActive");
            bool? isAppliedForCandidate = Utils.GetBool(search, "AppliedForCandidate");

            IQueryable<ExamViolationType> query = ExamViolationTypeBusiness.All;
            if (isActive.HasValue)
            {
                query = query.Where(o => o.IsActive == true);
            }
            if (isAppliedForCandidate.HasValue)
            {
                query = query.Where(o => o.AppliedForCandidate == true);
            }
            if (schoolID != 0)
            {
                query = query.Where(o => o.SchoolID == schoolID);
            }

            return query.OrderBy(p=>p.Resolution);
        }

        public List<ExamPupilViolateBO> GetListExamPupil(IDictionary<string, object> search)
        {
            int academicYearID = Utils.GetInt(search, "AcademicYearID");
            int schoolID = Utils.GetInt(search, "SchoolID");
            int lastDigitSchoolID = UtilsBusiness.GetPartionId(schoolID);
            int subjectID = Utils.GetInt(search, "SubjectID");
            long examinationsID = Utils.GetLong(search, "ExaminationsID");
            long examGroupID = Utils.GetLong(search, "ExamGroupID");
            long examRoomID = Utils.GetLong(search, "ExamRoomID");

            IQueryable<ExamPupilViolateBO> query = (from ep in ExamPupilRepository.All.Where(o => o.SchoolID == schoolID && o.LastDigitSchoolID == lastDigitSchoolID)
                                                    join pp in PupilProfileRepository.All on ep.PupilID equals pp.PupilProfileID
                                                    join poc in PupilOfClassRepository.All on pp.PupilProfileID equals poc.PupilID
                                                    where pp.IsActive == true && ep.SchoolID == schoolID && poc.AcademicYearID == academicYearID && poc.Status == GlobalConstants.PUPIL_STATUS_STUDYING
                                                    select new ExamPupilViolateBO
                                                    {
                                                        ExaminationsID = ep.ExaminationsID,
                                                        ExamPupilID = ep.ExamPupilID,
                                                        ExamGroupID = ep.ExamGroupID,
                                                        ExamRoomID = ep.ExamRoomID,
                                                        PupilCode = pp.PupilCode,
                                                        Name = pp.Name,
                                                        FullName = pp.FullName,
                                                        EthnicCode = pp.Ethnic.EthnicCode,
                                                        SBD = ep.ExamineeNumber,

                                                    });

            if (examinationsID != 0)
            {
                query = query.Where(o => o.ExaminationsID == examinationsID);
            }
            if (examGroupID != 0)
            {
                query = query.Where(o => o.ExamGroupID == examGroupID);
            }
            if (examRoomID != 0)
            {
                query = query.Where(o => o.ExamRoomID == examRoomID);
            }

            /*
            // Loại bỏ những thí sinh đã có trong danh sách vi phạm
            List<ExamPupilViolateBO> listPupil = query.ToList();
            List<long> listPupilViolate = this.Search(search).Select(o => o.ExamPupilID).ToList();
            for (int i = 0; i < listPupilViolate.Count; i++)
            {
                listPupil = listPupil.Where(o => o.ExamPupilID != listPupilViolate[i]).ToList();
            }
            */

            // Loại bỏ những thí sinh vắng thi (Vắng thi thì sẽ không vi phạm)
            List<ExamPupilViolateBO> listPupil = query.ToList();
            List<long> listPupilAbsence = ExamPupilAbsenceBusiness.Search(search).Select(o => o.ExamPupilID).ToList();
            for (int i = 0; i < listPupilAbsence.Count; i++)
            {
                listPupil = listPupil.Where(o => o.ExamPupilID != listPupilAbsence[i]).ToList();
            }

            return listPupil;
        }

        public List<ExamPupilViolateBO> GetListExamPupilViolate(IDictionary<string, object> search)
        {
            int academicYearID = Utils.GetInt(search, "AcademicYearID");
            int schoolID = Utils.GetInt(search, "SchoolID");
            int partition = UtilsBusiness.GetPartionId(schoolID);
            int educationLevelID = Utils.GetInt(search, "EducationLevelID");
            long examinationsID = Utils.GetLong(search, "ExaminationsID");
            long examGroupID = Utils.GetLong(search, "ExamGroupID");
            int subjectID = Utils.GetInt(search, "SubjectID");
            long examRoomID = Utils.GetLong(search, "ExamRoomID");
            string fullName = Utils.GetString(search, "FullName").ToLower();
            long? examPupilViolateID = Utils.GetInt(search, "ExamPupilViolateID");

            IQueryable<ExamPupilViolateBO> query = (from epv in ExamPupilViolateRepository.All.Where(o => o.ExaminationsID == examinationsID)
                                                    join eg in ExamGroupRepository.All.Where(o => o.ExaminationsID == examinationsID) on epv.ExamGroupID equals eg.ExamGroupID
                                                    join ep in ExamPupilRepository.All.AsNoTracking().Where(o => o.LastDigitSchoolID == partition && o.ExaminationsID == examinationsID) on epv.ExamPupilID equals ep.ExamPupilID
                                                    join er in ExamRoomRepository.All.Where(o => o.ExaminationsID == examinationsID) on ep.ExamRoomID equals er.ExamRoomID
                                                    join pp in PupilProfileRepository.All on ep.PupilID equals pp.PupilProfileID
                                                    join poc in PupilOfClassRepository.All on pp.PupilProfileID equals poc.PupilID
                                                    join sc in SubjectCatRepository.All on epv.SubjectID equals sc.SubjectCatID
                                                    join ev in ExamViolationTypeRepository.All.Where(o => o.SchoolID == schoolID) on epv.ExamViolationTypeID equals ev.ExamViolationTypeID
                                                    where ev.AppliedForCandidate.Value == true && ev.IsActive == true
                                                    && poc.Status == GlobalConstants.PUPIL_STATUS_STUDYING && poc.AcademicYearID == academicYearID
                                                    && pp.IsActive
                                                    select new ExamPupilViolateBO
                                                    {
                                                        ExamPupilViolateID = epv.EamPupilViolateID,
                                                        ExaminationsID = epv.ExaminationsID,
                                                        ExamRoomID = er.ExamRoomID,
                                                        ExamPupilID = ep.ExamPupilID,
                                                        ExamGroupID = epv.ExamGroupID,
                                                        SubjectID = epv.SubjectID,
                                                        ContentViolated = epv.ContentViolate,
                                                        PenalizedMark = ev.PenalizedMark,
                                                        Resolution = ev.Resolution,
                                                        ExamViolatetionTypeID = epv.ExamViolationTypeID,
                                                        PupilCode = pp.PupilCode,
                                                        Name = pp.Name,
                                                        FullName = pp.FullName,
                                                        EthnicCode = pp.Ethnic.EthnicCode,
                                                        SubjectName = sc.SubjectName,
                                                        OrderInSubject = sc.OrderInSubject,
                                                        ExamRoomCode = er.ExamRoomCode,
                                                        ExamGroupName = eg.ExamGroupName,
                                                        ExamViolatetionTypeName = ev.Resolution,
                                                        SBD = ep.ExamineeNumber,
                                                        BirthDay = pp.BirthDate,
                                                        Genre = pp.Genre,
                                                        ClassName = poc.ClassProfile.DisplayName
                                                    });

          
            if (examGroupID != 0)
            {
                query = query.Where(o => o.ExamGroupID == examGroupID);
            }
            if (subjectID != 0)
            {
                query = query.Where(o => o.SubjectID == subjectID);
            }
            if (examRoomID != 0)
            {
                query = query.Where(o => o.ExamRoomID == examRoomID);
            }
            if (fullName != string.Empty)
            {
                query = query.Where(o => o.FullName.ToLower().Contains(fullName));
            }
            if (examPupilViolateID.HasValue && examPupilViolateID.Value > 0)
            {
                query = query.Where(o => o.ExamPupilViolateID == examPupilViolateID);
            }

            return query.ToList();
        }

        public void InsertListExamPupilViolate(IDictionary<string, object> insertInfo, long[] ExamPupilID,
            long[] CheckExamPupilID, string[] ContentViolate, int[] ViolateTypeID)
        {
            long examinationsID = Utils.GetLong(insertInfo, "ExaminationsID");
            long examGroupID = Utils.GetLong(insertInfo, "ExamGroupID");
            int subjectID = Utils.GetInt(insertInfo, "SubjectID");
            long examRoomID = Utils.GetLong(insertInfo, "ExamRoomID");

            IDictionary<string, object> columnMapping = ColumnMappings();
            /*List<ExamPupilViolate> listInsert = new List<ExamPupilViolate>();*/
            ExamPupilViolate insert = null;
            int countPupil = ExamPupilID.Count();
            long tempID = 0;
            for (int i = 0; i < countPupil; i++)
            {
                tempID = ExamPupilID[i];
                if (CheckExamPupilID != null && CheckExamPupilID.Contains(tempID))
                {
                    insert = new ExamPupilViolate();
                    insert.EamPupilViolateID = ExamPupilViolateBusiness.GetNextSeq<long>();
                    insert.ExaminationsID = examinationsID;
                    insert.ExamGroupID = examGroupID;
                    insert.ExamRoomID = examRoomID;
                    insert.SubjectID = subjectID;
                    insert.ExamPupilID = tempID;
                    insert.ContentViolate = ContentViolate[i];
                    insert.ExamViolationTypeID = ViolateTypeID[i];
                    insert.CreateTime = DateTime.Now;
                    /*listInsert.Add(insert);*/
                    this.Insert(insert);
                }
            }

            /*for (int i = 0; i < listInsert.Count; i++)
            {
                this.Insert(listInsert[i]);
            }*/

            this.Save();
        }

        public void DeleteExamPupilViolate(List<long> listExamPupilViolateID)
        {
            if (listExamPupilViolateID == null)
            {
                return;
            }
            for (int i = 0; i < listExamPupilViolateID.Count; i++)
            {
                this.Delete(listExamPupilViolateID[i]);
            }
            this.Save();
        }

        public void UpdateExamPupilViolate(long examPupilViolateID, string contentViolate, long examViolationType)
        {
            ExamPupilViolate violate = this.Find(examPupilViolateID);
            if (violate != null)
            {
                violate.ContentViolate = contentViolate;
                violate.ExamViolationTypeID = examViolationType;
                violate.UpdateTime = DateTime.Now;
                this.Update(violate);
            }
            this.Save();
        }

        private IDictionary<string, object> ColumnMappings()
        {
            Dictionary<string, object> columnMap = new Dictionary<string, object>();
            columnMap.Add("EamPupilViolateID", "EXAM_PUPIL_VIOLATE_ID");
            columnMap.Add("ExamRoomID", "EXAM_ROOM_ID");
            columnMap.Add("ExaminationsID", "EXAMINATIONS_ID");
            columnMap.Add("ExamGroupID", "EXAM_GROUP_ID");
            columnMap.Add("SubjectID", "SUBJECT_ID");
            columnMap.Add("ExamPupilID", "EXAM_PUPIL_ID");
            columnMap.Add("ContentViolate", "CONTENT_VIOLATE");
            columnMap.Add("ExamViolationTypeID", "EXAM_VIOLATION_TYPE_ID");
            columnMap.Add("CreateTime", "CREATE_TIME");
            columnMap.Add("UpdateTime", "UPDATE_TIME");
            return columnMap;
        }

        #region Report
        /// <summary>
        ///Lay mang bam cho cac tham so dau vao
        /// </summary>
        /// <param name="entity"></param>
        /// <returns></returns>
        public string GetHashKey(IDictionary<string, object> dic)
        {
            return ReportUtils.GetHashKey(dic);
        }


        public ProcessedReport GetExamPupilViolateReport(IDictionary<string, object> dic)
        {
            string reportCode = SystemParamsInFile.REPORT_THI_SINH_VI_PHAM;
            string inputParameterHashKey = GetHashKey(dic);
            return ProcessedReportBusiness.GetProcessedReport(reportCode, inputParameterHashKey);
        }

        public Stream CreateExamPupilViolateReport(IDictionary<string, object> dic)
        {
            int academicYearID = Utils.GetInt(dic["AcademicYearID"]);
            int schoolID = Utils.GetInt(dic["SchoolID"]);
            long examinationsID = Utils.GetLong(dic["ExaminationsID"]);
            int appliedLevelID = Utils.GetInt(dic["AppliedLevelID"]);

            //Lấy đường dẫn báo cáo
            string reportCode = SystemParamsInFile.REPORT_THI_SINH_VI_PHAM;
            ReportDefinition rp = ReportDefinitionBusiness.GetByCode(reportCode);
            string templatePath = SystemParamsInFile.TEMPLATE_FOLDER + rp.TemplateDirectory + "/" + rp.TemplateName;
            //Khởi tạo
            IVTWorkbook oBook = VTExport.OpenWorkbook(templatePath);

            //Fill du lieu vao bao cao
            //Lấy ra sheet đầu tiên
            IVTWorksheet oSheet = oBook.GetSheet(1);

            //Điền các thông tin tiêu đề báo cáo
            AcademicYear academicYear = AcademicYearBusiness.Find(academicYearID);
            SchoolProfile school = SchoolProfileBusiness.Find(schoolID);
            Examinations exam = ExaminationsBusiness.Find(examinationsID);

            string schoolName = school.SchoolName.ToUpper();
            string supervisingDeptName = UtilsBusiness.GetSupervisingDeptName(school.SchoolProfileID, appliedLevelID).ToUpper();
            string provinceName = (school.District != null ? school.District.DistrictName : "");
            DateTime date = DateTime.Now;
            string day = "Ngày " + date.Day.ToString() + " tháng " + date.Month.ToString() + " năm " + date.Year.ToString();
            string provinceAndDate = provinceName + ", " + day;
            string examinationsName ="Kỳ Thi: " + exam.ExaminationsName;
            string strAcademicYear = "Năm Học: " + academicYear.DisplayTitle;

            //Dien du lieu vao phan tieu de
            oSheet.SetCellValue("A2", supervisingDeptName);
            oSheet.SetCellValue("A3", schoolName);
            oSheet.SetCellValue("D7", examinationsName);
            oSheet.SetCellValue("D8", strAcademicYear);
            oSheet.SetCellValue("H4", provinceAndDate);


            //Fill du lieu
            List<ExamPupilViolateBO> listResult = this.ExamPupilViolateBusiness.GetListExamPupilViolate(dic).ToList()
                                                    .OrderBy(o => SMAS.Business.Common.Utils.SortABC(o.FullName.getOrderingName(o.EthnicCode)))
                                                    .ToList();

            int startRow = 11;
            int lastRow = startRow + listResult.Count - 1;
            int curRow = startRow;
            int startColumn = 1;
            int lastColumn = 11;
            int curColumn = startColumn;

            for (int i = 0; i < listResult.Count; i++)
            {
                ExamPupilViolateBO obj = listResult[i];
                //STT
                oSheet.SetCellValue(curRow, curColumn, i + 1);
                curColumn++;
                //SBD
                oSheet.SetCellValue(curRow, curColumn, obj.SBD);
                curColumn++;
                //Ho va ten
                oSheet.SetCellValue(curRow, curColumn, obj.FullName);
                curColumn++;
                //Ngay sinh
                oSheet.SetCellValue(curRow, curColumn, obj.BirthDay);
                curColumn++;
                //Gioi tinh
                oSheet.SetCellValue(curRow, curColumn, obj.GenreName);
                curColumn++;
                //Lớp
                oSheet.SetCellValue(curRow, curColumn, obj.ClassName);
                curColumn++;
                //Nhom thi
                oSheet.SetCellValue(curRow, curColumn, obj.ExamGroupName);
                curColumn++;
                //Mon thi
                oSheet.SetCellValue(curRow, curColumn, obj.SubjectName);
                curColumn++;
                //Phong thi
                oSheet.SetCellValue(curRow, curColumn, obj.ExamRoomCode);
                curColumn++;
                //Noi dung vi pham
                oSheet.SetCellValue(curRow, curColumn, obj.ContentViolated);
                curColumn++;
                //Hinh thuc xu ly
                oSheet.SetCellValue(curRow, curColumn, obj.ExamViolatetionTypeName);
                curColumn++;


                //Ve khung cho dong
                IVTRange range = oSheet.GetRange(curRow, startColumn, curRow, lastColumn);

                VTBorderStyle style;
                VTBorderWeight weight;
                if ((curRow - startRow + 1) % 5 != 0)
                {
                    style = VTBorderStyle.Dotted;
                    weight = VTBorderWeight.Thin;
                }
                else
                {
                    style = VTBorderStyle.Solid;
                    weight = VTBorderWeight.Medium;
                }
                range.SetBorder(style, weight, VTBorderIndex.EdgeBottom);

                curRow++;
                curColumn = startColumn;
            }

            IVTRange globalRange = oSheet.GetRange(startRow, startColumn, lastRow, lastColumn);
            globalRange.SetBorder(VTBorderStyle.Solid, VTBorderWeight.Medium, VTBorderIndex.Around);
            globalRange.SetBorder(VTBorderStyle.Solid, VTBorderWeight.Thin, VTBorderIndex.InsideVertical);

            oSheet.Orientation = VTXPageOrientation.VTxlPortrait;
            oSheet.FitAllColumnsOnOnePage = true;

            return oBook.ToStream();
        }
        /// <summary>
        /// Luu lai thong tin
        /// </summary>
        /// <param name="entity"></param>
        /// <param name="data"></param>
        /// <returns></returns>
        public ProcessedReport InsertExamPupilViolateReport(IDictionary<string, object> dic, Stream data)
        {
            string reportCode = SystemParamsInFile.REPORT_THI_SINH_VI_PHAM;

            long examinationsID = Utils.GetLong(dic["ExaminationsID"]);
            long examGroupID = Utils.GetLong(dic["ExamGroupID"]);
            long subjectID = Utils.GetLong(dic["SubjectID"]);
            long examRoomID = Utils.GetLong(dic["ExamRoomID"]);

            Examinations exam = ExaminationsBusiness.Find(examinationsID);
            ExamGroup examGroup = ExamGroupBusiness.Find(examGroupID);
            SubjectCat subject = SubjectCatBusiness.Find(subjectID);
            ExamRoom examRoom = ExamRoomBusiness.Find(examRoomID);

            ProcessedReport pr = new ProcessedReport();

            ReportDefinition reportDef = ReportDefinitionBusiness.GetByCode(reportCode);
            pr.ReportCode = reportCode;
            pr.ProcessedDate = DateTime.Now;
            pr.InputParameterHashKey = GetHashKey(dic);
            pr.ReportData = ReportUtils.Compress(data);

            //Tạo tên file
            string outputNamePattern = reportDef.OutputNamePattern;
            outputNamePattern = outputNamePattern.Replace("[Examinations]", ReportUtils.StripVNSign(exam.ExaminationsName));
            if (examGroup != null)
            {
                outputNamePattern = outputNamePattern + "_" + ReportUtils.StripVNSign(examGroup.ExamGroupName);
            }
            if(subject!=null)
            {
                outputNamePattern = outputNamePattern + "_" + ReportUtils.StripVNSign(subject.DisplayName);
            }
            if (examRoom != null)
            {
                outputNamePattern = outputNamePattern + "_" + ReportUtils.StripVNSign(examRoom.ExamRoomCode);
            }

            pr.ReportName = ReportUtils.RemoveSpecialCharacters(outputNamePattern) + "." + reportDef.OutputFormat;

            ProcessedReportParameterRepository.Insert(dic, pr);
            ProcessedReportRepository.Insert(pr);
            ProcessedReportRepository.Save();
            ProcessedReportParameterRepository.Save();
            return pr;
        }
        #endregion 
    }
}