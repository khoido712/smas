﻿/**
 * The Viettel License
 *
 * Copyright 2012 Viettel Telecom. All rights reserved.
 * VIETTEL PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */

/** 
* @author  
* @version $Revision: $
*/

using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Linq.Expressions;
using System.Web;
using log4net;

using SMAS.DAL.Repository;
using SMAS.DAL.IRepository;
using SMAS.Business.IBusiness;
using SMAS.Models.Models;
using SMAS.Business.Common;
using SMAS.Business.BusinessObject;
using System.Transactions;

namespace SMAS.Business.Business
{
    public partial class PromotionDetailBusiness
    {
        public IQueryable<SMS_PROMOTION_DETAIL> Search(Dictionary<string, object> dic)
        {
            int PromotionID = Utils.GetInt(dic, "PromotionID");
            int ProvinceID = Utils.GetInt(dic, "ProvinceID");
            int UnitID = Utils.GetInt(dic, "UnitID");
            IQueryable<SMS_PROMOTION_DETAIL> iquery = this.All;
            if (PromotionID != 0)
            {
                iquery = iquery.Where(p => p.PROMOTION_ID == PromotionID);
            }
            if (ProvinceID != 0)
            {
                iquery = iquery.Where(p => p.PROVINCE_ID == ProvinceID);
            }
            if (UnitID != 0)
            {
                iquery = iquery.Where(p => p.UNIT_ID == UnitID);
            }
            return iquery;
        }

        public List<SMS_PROMOTION_DETAIL> GetlistPromotionDetail(Dictionary<string, object> dic)
        {
            int PromotionID = Utils.GetInt(dic, "PromotionID");
            int ProvinceID = Utils.GetInt(dic, "ProvinceID");
            int UnitID = Utils.GetInt(dic, "UnitID");
            IQueryable<SMS_PROMOTION_DETAIL> iquery = this.All;
            if (PromotionID != 0)
            {
                iquery = iquery.Where(p => p.PROMOTION_ID == PromotionID);
            }
            if (ProvinceID != 0)
            {
                iquery = iquery.Where(p => p.PROVINCE_ID == ProvinceID);
            }
            if (UnitID != 0)
            {
                iquery = iquery.Where(p => p.UNIT_ID == UnitID);
            }
            return iquery.ToList();
        }

        public string InsertPromotionDetail(List<SMS_PROMOTION_DETAIL> lstPromotionDetail, Dictionary<string, object> dic)
        {
            List<int> lstUnitID = lstPromotionDetail.Select(p => p.UNIT_ID).Distinct().ToList();
            List<int> lstPromotionID = lstPromotionDetail.Select(p => p.PROMOTION_ID).Distinct().ToList();
            long promotionID = Utils.GetInt(dic, "PromotionID");
            List<SMS_PROMOTION_DETAIL> lstPromotionDetailDB = this.PromotionDetailBusiness.All.Where(p => lstUnitID.Contains(p.UNIT_ID)).ToList();
            List<SMS_PROMOTION> lstPromotion = this.PromotionBusiness.All.ToList();
            SMS_PROMOTION objPromotion = null;
            SMS_PROMOTION objPromotionInsertDetail = null;
            SMS_PROMOTION_DETAIL objInsert = null;
            List<SMS_PROMOTION_DETAIL> lsttmp = null;
            string Message = string.Empty;
            SMS_PROMOTION_DETAIL objtmp = null;
            for (int i = 0; i < lstPromotionDetail.Count; i++)
            {
                objInsert = lstPromotionDetail[i];
                objPromotion = lstPromotion.Where(p => p.PROMOTION_ID == objInsert.PROMOTION_ID).FirstOrDefault();
                lsttmp = lstPromotionDetailDB.Where(p => p.UNIT_ID == objInsert.UNIT_ID).ToList();
                if (lsttmp.Count > 0)
                {
                    for (int j = 0; j < lsttmp.Count; j++)
                    {
                        objtmp = lsttmp[j];
                        if (objInsert.PROMOTION_ID != objtmp.PROMOTION_ID)
                        {
                            objPromotionInsertDetail = lstPromotion.Where(p => p.PROMOTION_ID == objtmp.PROMOTION_ID).FirstOrDefault();
                            if (objPromotionInsertDetail != null
                                && ((objPromotionInsertDetail.END_DATE.HasValue && objPromotion.START_DATE < objPromotionInsertDetail.END_DATE.Value)
                                || !objPromotionInsertDetail.END_DATE.HasValue))
                            {
                                Message = "Thời gian áp dụng không được trùng với một chương trình khuyến mại khác đã tồn tại";
                                return Message;
                            }
                        }
                    }
                }
                this.Insert(objInsert);
            }
            this.Save();
            return Message;
        }
        public void DeletePromotionDetail(List<long> lstPromotionDetailID)
        {
            List<SMS_PROMOTION_DETAIL> lstRemove = this.All.Where(p => lstPromotionDetailID.Contains(p.SMS_PROMOTION_DETAIL_ID)).ToList();
            for (int i = 0; i < lstRemove.Count; i++)
            {
                this.Delete(lstRemove[i].SMS_PROMOTION_DETAIL_ID);
            }
            this.Save();
        }
    }
}
