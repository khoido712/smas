/**
 * The Viettel License
 *
 * Copyright 2012 Viettel Telecom. All rights reserved.
 * VIETTEL PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */

/**
* @author tungnd
* @version $Revision: $
*/

using System;
using System.Collections.Generic;
using System.Linq;
using SMAS.Business.BusinessObject;
using SMAS.Business.Common;
using SMAS.Business.SearchForm;
using SMAS.Models.Models;
using System.Text;
using SMAS.VTUtils.Excel.ContextExt;
using Newtonsoft.Json;
using SMAS.DAL.Repository;
using System.Data.Objects;
using SMAS.VTUtils.Log;

namespace SMAS.Business.Business
{
    public partial class PupilProfileBusiness
    {
        #region Validate

        public void Validate(PupilProfile pupilprofile)
        {
            ValidationMetadata.ValidateObject(pupilprofile);

            //Năm học không phải là năm hiện tại
            if (!AcademicYearBusiness.IsCurrentYear(pupilprofile.CurrentAcademicYearID))
                throw new BusinessException("Common_Validate_IsCurrentYear");
            int PupilID = pupilprofile.PupilProfileID;
            //Mã học sinh đã tồn tại 

            if (pupilprofile.ClassProfile != null && EducationLevelBusiness.Find(pupilprofile.ClassProfile.EducationLevelID).Grade >= 4)
            {
                if (PupilProfileBusiness.All.Any(u => u.IsActive && u.PupilCode == pupilprofile.PupilCode && u.CurrentSchoolID == pupilprofile.CurrentSchoolID && u.PupilProfileID != pupilprofile.PupilProfileID))
                    throw new BusinessException("Common_Validate_Duplicate", "PupilProfileChildren_Label_PupilCode");
            }
            else
            {
                if (PupilProfileBusiness.All.Any(u => u.IsActive && u.PupilCode == pupilprofile.PupilCode && u.CurrentSchoolID == pupilprofile.CurrentSchoolID && u.PupilProfileID != pupilprofile.PupilProfileID))
                    throw new BusinessException("Common_Validate_Duplicate", "PupilProfile_Label_PupilCode");
            }
            //new PupilProfileBusiness(null).CheckDuplicate(pupilprofile.PupilCode, GlobalConstants.PUPIL_SCHEMA, "PupilProfile", "PupilCode", true, pupilprofile.PupilProfileID, "PupilProfile_Label_PupilCode");

            //  CurrentClassID và CurrentAcademicYearID not compatible trong ClassProfile
            IDictionary<string, object> SearchInfo = new Dictionary<string, object>();
            SearchInfo["ClassProfileID"] = pupilprofile.CurrentClassID;
            SearchInfo["AcademicYearID"] = pupilprofile.CurrentAcademicYearID;

            bool SearchInfoExist = new ClassProfileRepository(this.context).ExistsRow(GlobalConstants.SCHOOL_SCHEMA, "ClassProfile", SearchInfo, null);

            if (!SearchInfoExist)
            {
                throw new BusinessException("Common_Validate_NotCompatible");
            }

            //  CurrentSchoolID và CurrentAcademicYearID not compatible trong AcademicYear
            IDictionary<string, object> SearchInfoAcademic = new Dictionary<string, object>();
            SearchInfoAcademic["SchoolID"] = pupilprofile.CurrentSchoolID;
            SearchInfoAcademic["AcademicYearID"] = pupilprofile.CurrentAcademicYearID;

            bool SearchInfoAcademicExist = new AcademicYearRepository(this.context).ExistsRow(GlobalConstants.SCHOOL_SCHEMA, "AcademicYear", SearchInfoAcademic, null);

            if (!SearchInfoAcademicExist)
            {
                throw new BusinessException("Common_Validate_NotCompatible");
            }

            if (pupilprofile.DistrictID.HasValue)
            {
                //  DistrictID và ProvinceID not compatible trong District
                IDictionary<string, object> SearchInfoDistrict = new Dictionary<string, object>();
                SearchInfoDistrict["DistrictID"] = pupilprofile.DistrictID;
                SearchInfoDistrict["ProvinceID"] = pupilprofile.ProvinceID;

                bool SearchInfoDistrictExist = new AcademicYearRepository(this.context).ExistsRow(GlobalConstants.LIST_SCHEMA, "District", SearchInfoDistrict, null);

                if (!SearchInfoDistrictExist)
                {
                    throw new BusinessException("Common_Validate_NotCompatible");
                }
            }

            if (pupilprofile.CommuneID.HasValue && pupilprofile.DistrictID.HasValue)
            {
                //  CommuneID và DistrictID not compatible trong Commune
                IDictionary<string, object> SearchInfoCommune = new Dictionary<string, object>();
                SearchInfoCommune["CommuneID"] = pupilprofile.CommuneID;
                SearchInfoCommune["DistrictID"] = pupilprofile.DistrictID;

                bool SearchInfoCommuneExist = new AcademicYearRepository(this.context).ExistsRow(GlobalConstants.LIST_SCHEMA, "Commune", SearchInfoCommune, null);

                if (!SearchInfoCommuneExist)
                {
                    throw new BusinessException("Common_Validate_NotCompatible");
                }
            }
            //Validate Email
            Utils.ValidateEmail(pupilprofile.FatherEmail, "PupilProfile_Label_FatherEmail");
            Utils.ValidateEmail(pupilprofile.MotherEmail, "PupilProfile_Label_MotherEmail");
            Utils.ValidateEmail(pupilprofile.SponsorEmail, "PupilProfile_Label_SponsorEmail");
            //Số con thứ không được lơn hơn tổng số con
            if (pupilprofile.ChildOrder > pupilprofile.NumberOfChild)
            {
                throw new BusinessException("Common_Validate_ChildOrder_NumberOfChild");
            }
            //BirthDate < DateTime.Now            
            //Utils.ValidatePastDateNow(pupilprofile.EnrolmentDate, "PupilProfile_Label_EnrolmentDate");
            Utils.ValidatePastDateNow(pupilprofile.BirthDate, "PupilProfile_Label_BirthDate");
            Utils.ValidatePastYearNow(pupilprofile.FatherBirthDate, "PupilProfile_Label_FatherBirthdateName");
            Utils.ValidatePastYearNow(pupilprofile.MotherBirthDate, "PupilProfile_Label_MotherBirthdateName");
            Utils.ValidatePastYearNow(pupilprofile.SponsorBirthDate, "PupilProfile_Label_SponsorBirthDateName");

            Utils.ValidateBeforeDate(pupilprofile.BirthDate, new DateTime(1900, 1, 1), "PupilProfile_Label_BirthDate", "Common_Min_BirthYear");
            Utils.ValidateBeforeDate(pupilprofile.EnrolmentDate, new DateTime(1900, 1, 1), "PupilProfile_Label_EnrolmentDate", "Common_Min_BirthYear");
            Utils.ValidateBeforeDate(pupilprofile.YouthLeagueJoinedDate, pupilprofile.YoungPioneerJoinedDate, "PupilProfile_Label_YouthLeagueJoinedDate", "PupilProfile_Label_YoungPioneerJoinedDate");
            Utils.ValidateBeforeDate(pupilprofile.CommunistPartyJoinedDate, pupilprofile.YouthLeagueJoinedDate, "PupilProfile_Label_CommunistPartyJoinedDate", "PupilProfile_Label_YouthLeagueJoinedDate");
            //Kiểm tra thêm điều kiện AssignDate phải nhỏ hơn ngày kết thúc năm học hiện tại
            AcademicYear academicYear = AcademicYearBusiness.Find(pupilprofile.CurrentAcademicYearID);
            if (pupilprofile.EnrolmentDate > academicYear.SecondSemesterEndDate)
            {
                throw new BusinessException("PupilProfile_Validate_Enrollment");
            }
        }

        #endregion Validate

        #region Insert children

        /// <summary>
        /// Thêm mới thông tin trẻ em
        /// </summary>
        /// <author>trangdd</author>
        /// <date>13/11/2012</date>
        /// <param name="ethnic">Đối tượng PupilProfile</param>
        /// <returns>Đối tượng PupilProfile</returns>
        public void InsertChildren(int UserID, PupilProfile InsertPupilProfile, List<HabitOfChildren> lstHabitOfChildren, List<FoodHabitOfChildren> lstFoodHabitOfChildren, int EnrollmentType)
        {
            //Thực hiện insert từng phần tử trong lstHabitOfChildren vào bảng HabitOfChildren
            foreach (var HabitOfChildren in lstHabitOfChildren)
            {
                //HabitOfChildren.PupilProfile = InsertPupilProfile;
                HabitOfChildren.PupilID = InsertPupilProfile.PupilProfileID;
                HabitOfChildrenBusiness.Insert(HabitOfChildren);
            }

            //Thực hiện insert từng phần tử trong lstFoodHabitOfChildren vào bảng FoodHabitOfChildren
            foreach (var FoodHabitOfChildren in lstFoodHabitOfChildren)
            {
                //FoodHabitOfChildren.PupilProfile = InsertPupilProfile;
                FoodHabitOfChildren.PupilID = InsertPupilProfile.PupilProfileID;
                FoodHabitOfChildrenBusiness.Insert(FoodHabitOfChildren);
            }
            this.InsertPupilProfile(UserID, InsertPupilProfile, EnrollmentType);
        }

        #endregion Insert children

        #region Update children

        /// <summary>
        /// Cập nhật thông tin trẻ em
        /// </summary>
        /// <author>trangdd</author>
        /// <date>13/11/2012</date>
        /// <param name="ethnic">Đối tượng PupilProfile</param>
        /// <returns>Đối tượng PupilProfile</returns>
        public void UpdateChildren(int UserID, PupilProfile PupilProfile, List<HabitOfChildren> lstHabitOfChildren, List<FoodHabitOfChildren> lstFoodHabitOfChildren, int OldClassID, int EnrollmentType)
        {
            //Nếu có các thông tin thì không cho sửa lớp
            int PupilID = PupilProfile.PupilProfileID;
            int ClassID = PupilProfile.CurrentClassID;
            if (ClassID != OldClassID)
            {
                bool PupilAbsenseConstraint = PupilAbsenceBusiness.All.Any(o => o.PupilID == PupilID && o.ClassID == OldClassID);
                if (PupilAbsenseConstraint)
                {
                    throw new BusinessException("ImportPupilChildren_Label_PupilAbsenceExist");
                }
                bool DevelopmentOfChildrenConstraint = DevelopmentOfChildrenBusiness.All.Any(o => o.PupilID == PupilID && o.ClassID == OldClassID);
                if (DevelopmentOfChildrenConstraint)
                {
                    throw new BusinessException("ImportPupilChildren_Label_DevelopmentOfChildren_Exist");
                }
                bool GoodTicketChildreConstraint = GoodChildrenTicketBusiness.All.Any(o => o.PupilID == PupilID && o.ClassID == OldClassID);
                if (GoodTicketChildreConstraint)
                {
                    throw new BusinessException("ImportPupilChildren_Label_GoodTicketChildren_Exist");
                }
            }
            //Lấy PupilID của đối tượng vừa insert được cập nhật vào lstHabitOfChildren.
            //Tìm kiếm trong bảng HabitOfChildren theo PupilID. Xóa các bản ghi tìm được
            IDictionary<string, object> SearchInfo = new Dictionary<string, object>();
            SearchInfo["PupilID"] = PupilProfile.PupilProfileID;
            List<HabitOfChildren> listHabitOfChildren = HabitOfChildrenBusiness.Search(SearchInfo).ToList();
            HabitOfChildrenBusiness.DeleteAll(listHabitOfChildren);

            //Thực hiện insert từng phần tử trong lstHabitOfChildren vào bảng HabitOfChildren
            foreach (var HabitOfChildren in lstHabitOfChildren)
            {
                HabitOfChildren.PupilID = PupilProfile.PupilProfileID;
                HabitOfChildrenBusiness.Insert(HabitOfChildren);
            }

            //Lấy PupilID của đối tượng vừa insert được cập nhật vào lstFoodHabitOfChildren.
            //Tìm kiếm trong bảng FoodHabitOfChildren theo PupilID. Xóa các bản ghi tìm được
            IDictionary<string, object> FoodHabitSearchInfo = new Dictionary<string, object>();
            FoodHabitSearchInfo["PupilID"] = PupilProfile.PupilProfileID;
            List<FoodHabitOfChildren> listFoodHabitOfChildren = FoodHabitOfChildrenBusiness.Search(SearchInfo).ToList();

            FoodHabitOfChildrenBusiness.DeleteAll(listFoodHabitOfChildren);
            //Thực hiện insert từng phần tử trong lstFoodHabitOfChildren vào bảng FoodHabitOfChildren
            foreach (var FoodHabitOfChildren in lstFoodHabitOfChildren)
            {
                FoodHabitOfChildren.PupilID = PupilProfile.PupilProfileID;

                FoodHabitOfChildrenBusiness.Insert(FoodHabitOfChildren);
            }
            PupilProfile.IsActive = true;
            //this.Update(PupilProfile);
            this.UpdatePupilProfile(UserID, PupilProfile, EnrollmentType, OldClassID);
        }

        #endregion Update children

        #region InsertPupilProfile

        /// <summary>
        /// Thêm mới thông tin học sinh
        /// </summary>
        /// <author>tungnd</author>
        /// <date>9/10/2012</date>
        /// <param name="ethnic">Đối tượng PupilProfile</param>
        /// <returns>Đối tượng PupilProfile</returns>
        public void InsertPupilProfile(int UserID, PupilProfile InsertPupilProfile, int EnrolmentType, bool isImport = false)
        {
            // bắt dữ liệu truyền vào
            if (isImport == false)
            {
                Validate(InsertPupilProfile);
            }
            if (UtilsBusiness.HasHeadTeacherPermission(UserID, InsertPupilProfile.CurrentClassID))
            {
                InsertPupilProfile.IsActive = true;

                // insert dữ liệu vào bảng PupilOfSchool
                PupilOfSchool pos = new PupilOfSchool();
                pos.PupilID = InsertPupilProfile.PupilProfileID;
                pos.SchoolID = InsertPupilProfile.CurrentSchoolID;
                pos.EnrolmentDate = InsertPupilProfile.EnrolmentDate;
                pos.EnrolmentType = (int)EnrolmentType;
                InsertPupilProfile.PupilOfSchools.Add(pos);

                // insert dữ liệu vào bảng PupilOfClass
                AcademicYear AcademicYear = AcademicYearBusiness.Find(InsertPupilProfile.CurrentAcademicYearID);
                PupilOfClass lastPoc = PupilOfClassBusiness.All.Where(o => o.ClassID == InsertPupilProfile.CurrentClassID
                    && o.PupilProfile.IsActive && o.OrderInClass.HasValue).OrderByDescending(o => o.OrderInClass).FirstOrDefault();

                PupilOfClass poc = new PupilOfClass();
                poc.PupilID = InsertPupilProfile.PupilProfileID;
                poc.ClassID = InsertPupilProfile.CurrentClassID;
                poc.SchoolID = InsertPupilProfile.CurrentSchoolID;
                poc.AcademicYearID = InsertPupilProfile.CurrentAcademicYearID;
                poc.Year = AcademicYear.Year;
                poc.AssignedDate = InsertPupilProfile.EnrolmentDate;
                poc.Status = GlobalConstants.PUPIL_STATUS_STUDYING;
                poc.OrderInClass = lastPoc != null ? (lastPoc.OrderInClass + 1) : 1;

                InsertPupilProfile.PupilOfClasses.Add(poc);

                InsertPupilProfile.CreatedDate = DateTime.Now;
                base.Insert(InsertPupilProfile);
            }
        }

        #endregion InsertPupilProfile

        #region ImportPupilProfile

        //Import học sinh
        //lstPupilProfile, lstEnrollmentType, lstOrder tuong ung voi nhau
        public void ImportPupilProfile(int UserID, List<PupilProfile> lstPupilProfile, List<int> lstEnrollmentType, List<int> lstOrder, List<int> lstClassID)
        {
            context.Configuration.AutoDetectChangesEnabled = false;
            context.Configuration.ValidateOnSaveEnabled = false;
            //Check quyen voi UserID
            //Validate
            foreach (int ClassID in lstClassID)
            {
                if (!UtilsBusiness.HasHeadTeacherPermission(UserID, ClassID))
                {
                    throw new BusinessException("Common_Error_HasHadTeacherPermission");
                }
            }
            if (lstPupilProfile.Count == 0)
            {
                return;
            }
            PupilProfile pp = lstPupilProfile[0];
            AcademicYear aca = AcademicYearBusiness.Find(pp.CurrentAcademicYearID);

            #region sua lai luong luu dung BulkInsert

            List<string> lstPupilCode = lstPupilProfile.Select(p => p.PupilCode).Distinct().ToList();
            PupilProfileBusiness.BulkInsert(lstPupilProfile, PupilProfileMapping(), "PupilProfileID", "Image");

            List<PupilProfile> lstPupilDB = (from pf in PupilProfileBusiness.All
                                             where pf.CurrentAcademicYearID == pp.CurrentAcademicYearID
                                             && pf.CurrentSchoolID == pp.CurrentSchoolID
                                             && lstPupilCode.Contains(pf.PupilCode)
                                             && lstClassID.Contains(pf.CurrentClassID)
                                             && pf.IsActive
                                             select pf).ToList();
            PupilProfile objPFDB = null;

            #endregion

            //Tao doi tuong luu bang PupilOfSchool va PupilOfClass
            List<PupilOfSchool> lstPOS = new List<PupilOfSchool>();
            PupilOfSchool objPOS = null;
            List<PupilOfClass> lstPOC = new List<PupilOfClass>();
            PupilOfClass objPOC = null;
            string pupilCode = string.Empty;
            for (int i = 0; i < lstPupilCode.Count; i++)
            {
                objPOS = new PupilOfSchool();
                objPOC = new PupilOfClass();
                pupilCode = lstPupilCode[i];
                objPFDB = lstPupilDB.Where(p => p.PupilCode == pupilCode).FirstOrDefault();
                objPOS.PupilID = objPFDB.PupilProfileID;
                objPOS.SchoolID = objPFDB.CurrentSchoolID;
                objPOS.EnrolmentDate = objPFDB.EnrolmentDate;
                objPOS.EnrolmentType = lstEnrollmentType[i];
                lstPOS.Add(objPOS);

                objPOC.PupilID = objPFDB.PupilProfileID;
                objPOC.ClassID = objPFDB.CurrentClassID;
                objPOC.SchoolID = objPFDB.CurrentSchoolID;
                objPOC.AcademicYearID = objPFDB.CurrentAcademicYearID;
                objPOC.Year = aca.Year;
                objPOC.AssignedDate = objPFDB.EnrolmentDate;
                objPOC.Status = GlobalConstants.PUPIL_STATUS_STUDYING;
                objPOC.OrderInClass = lstOrder[i];
                lstPOC.Add(objPOC);
            }

            PupilOfSchoolBusiness.BulkInsert(lstPOS, this.PupilOfSchoolMapping(), "PupilOfSchoolID");
            PupilOfClassBusiness.BulkInsert(lstPOC, this.PupilOfClassMapping(), "PupilOfClassID");

            //ValidateAll(lstPupilProfile);
            //foreach (PupilProfile InsertPupilProfile in lstPupilProfile)
            //{

            //    InsertPupilProfile.IsActive = true;
            //    // insert dữ liệu vào bảng PupilOfSchool
            //    PupilOfSchool pos = new PupilOfSchool();
            //    pos.PupilID = InsertPupilProfile.PupilProfileID;
            //    pos.SchoolID = InsertPupilProfile.CurrentSchoolID;
            //    pos.EnrolmentDate = InsertPupilProfile.EnrolmentDate;
            //    pos.EnrolmentType = lstEnrollmentType[i];
            //    InsertPupilProfile.PupilOfSchools.Add(pos);
            //    //this.PupilOfSchoolBusiness.Insert(pos);
            //    // insert dữ liệu vào bảng PupilOfClass
            //    //AcademicYear AcademicYear = AcademicYearBusiness.Find(InsertPupilProfile.CurrentAcademicYearID);

            //    PupilOfClass poc = new PupilOfClass();
            //    poc.PupilID = InsertPupilProfile.PupilProfileID;
            //    poc.ClassID = InsertPupilProfile.CurrentClassID;
            //    poc.SchoolID = InsertPupilProfile.CurrentSchoolID;
            //    poc.AcademicYearID = InsertPupilProfile.CurrentAcademicYearID;
            //    poc.Year = aca.Year;
            //    poc.AssignedDate = InsertPupilProfile.EnrolmentDate;
            //    poc.Status = GlobalConstants.PUPIL_STATUS_STUDYING;
            //    poc.OrderInClass = lstOrder[i];
            //    //this.PupilOfClassBusiness.Insert(poc);
            //    InsertPupilProfile.PupilOfClasses.Add(poc);
            //    InsertPupilProfile.CreatedDate = DateTime.Now;
            //    base.Insert(InsertPupilProfile);

            //    i++;
            //}
            context.Configuration.AutoDetectChangesEnabled = true;
            context.Configuration.ValidateOnSaveEnabled = true;
        }

        #endregion ImportPupilProfile

        #region UpdateImportPupilProfile


        public void UpdateImportPupilProfile(int UserID, List<PupilProfile> lstUpdatePupilProfile, List<int> lstEnrolmentType,
            List<int> lstOrderUpdate, List<int> lstOldClassID, List<int> lstClassID)
        {
            try
            {
                SetAutoDetectChangesEnabled(false);
                int AcademicYearID = lstUpdatePupilProfile.FirstOrDefault().CurrentAcademicYearID;
                AcademicYear objAy = AcademicYearBusiness.Find(AcademicYearID);
                List<int> lstPupilID = lstUpdatePupilProfile.Select(o => o.PupilProfileID).ToList();
                List<PupilOfSchool> lstPupilOfSchool = new List<PupilOfSchool>();
                List<PupilOfClass> lstPupilOfClass = new List<PupilOfClass>();
                ValidateAll(lstUpdatePupilProfile, true);
                int SemesterID = 0;
                DateTime datetimeNow = DateTime.Now.Date;
                if (objAy.FirstSemesterStartDate <= datetimeNow && datetimeNow <= objAy.FirstSemesterEndDate)
                {
                    SemesterID = GlobalConstants.SEMESTER_OF_YEAR_FIRST;
                }
                else if (objAy.SecondSemesterStartDate <= datetimeNow && datetimeNow <= objAy.SecondSemesterEndDate)
                {
                    SemesterID = GlobalConstants.SEMESTER_OF_YEAR_SECOND;
                }
                int NewClassID = 0;
                int OldClassID = 0;
                bool isCalSMS = false;
                // Lay danh sach nam hoc ra truoc
                List<int> listAcademicYearID = lstUpdatePupilProfile.Select(o => o.CurrentAcademicYearID).Distinct().ToList();
                List<AcademicYear> listAcademicYear = new List<AcademicYear>();

                for (int i = 0; i < lstUpdatePupilProfile.Count; i++)// (PupilProfile UpdatePupilProfile in lstUpdatePupilProfile)
                {
                    PupilProfile UpdatePupilProfile = lstUpdatePupilProfile[i];
                    if (i == 0)
                    {
                        Dictionary<string, object> dic = new Dictionary<string, object>();
                        dic["AcademicYearID"] = UpdatePupilProfile.CurrentAcademicYearID;
                        //academicYear = AcademicYearBusiness.Find(UpdatePupilProfile.CurrentAcademicYearID);
                        lstPupilOfSchool = PupilOfSchoolBusiness.SearchBySchool(UpdatePupilProfile.CurrentSchoolID, new Dictionary<string, object>()).Where(o => lstPupilID.Contains(o.PupilID)).ToList();
                        lstPupilOfClass = PupilOfClassBusiness.SearchBySchool(UpdatePupilProfile.CurrentSchoolID, dic).Where(o => lstPupilID.Contains(o.PupilID)).ToList();
                        listAcademicYear = AcademicYearBusiness.SearchBySchool(UpdatePupilProfile.CurrentSchoolID).Where(o => listAcademicYearID.Contains(o.AcademicYearID)).ToList();
                    }
                    UpdatePupilProfile.IsActive = true;
                    PupilOfSchool pos = lstPupilOfSchool.FirstOrDefault(o => o.PupilID == UpdatePupilProfile.PupilProfileID);
                    if (pos == null)
                    {
                        pos = new PupilOfSchool();
                    }
                    pos.PupilID = UpdatePupilProfile.PupilProfileID;
                    pos.SchoolID = UpdatePupilProfile.CurrentSchoolID;
                    pos.EnrolmentDate = UpdatePupilProfile.EnrolmentDate;
                    pos.EnrolmentType = lstEnrolmentType[i];
                    NewClassID = UpdatePupilProfile.CurrentClassID;
                    if (pos.PupilOfSchoolID == 0)
                    {
                        PupilOfSchoolBusiness.Insert(pos);
                    }
                    else
                    {
                        PupilOfSchoolBusiness.Update(pos);
                    }

                    // insert dữ liệu vào bảng PupilOfClass
                    AcademicYear AcademicYear = listAcademicYear.FirstOrDefault(o => o.AcademicYearID == UpdatePupilProfile.CurrentAcademicYearID);

                    //PupilOfClass poc = lstPupilOfClass.Where(o => o.PupilID == UpdatePupilProfile.PupilProfileID && o.Status == GlobalConstants.PUPIL_STATUS_STUDYING).FirstOrDefault();
                    List<PupilOfClass> lstpoc = lstPupilOfClass.Where(o => o.PupilID == UpdatePupilProfile.PupilProfileID).ToList();
                    PupilOfClass poc = lstpoc.FirstOrDefault(o => o.Status == SystemParamsInFile.PUPIL_STATUS_STUDYING);
                    if (poc == null)
                    {
                        poc = new PupilOfClass();
                    }
                    OldClassID = poc.ClassID;
                    poc.PupilID = UpdatePupilProfile.PupilProfileID;
                    poc.ClassID = UpdatePupilProfile.CurrentClassID;
                    poc.SchoolID = UpdatePupilProfile.CurrentSchoolID;
                    poc.AcademicYearID = UpdatePupilProfile.CurrentAcademicYearID;
                    poc.Year = AcademicYear.Year;
                    if (lstpoc.Count == 1 || lstpoc.Count == 0)
                    {
                        poc.AssignedDate = UpdatePupilProfile.EnrolmentDate;
                    }
                    else
                    {
                        PupilOfClass pocAssignMin = lstpoc.OrderBy(o => o.AssignedDate).FirstOrDefault();
                        if (pocAssignMin != null)
                        {
                            poc.AssignedDate = UpdatePupilProfile.EnrolmentDate;
                            //this.PupilOfClassBusiness.Update(pocAssignMin);
                        }
                    }

                    poc.Status = UpdatePupilProfile.ProfileStatus;
                    poc.OrderInClass = lstOrderUpdate[i];
                    if (poc.PupilOfClassID == 0)
                    {
                        this.PupilOfClassBusiness.Insert(poc);
                    }
                    else
                    {
                        this.PupilOfClassBusiness.Update(poc);
                        if (NewClassID != OldClassID)
                        {
                            //cap nhat lai ClassID trong bang SMSParentContract
                            SMSParentContractBusiness.UpdateClassIDToSMSParentContract(NewClassID, OldClassID, SemesterID, UpdatePupilProfile.CurrentAcademicYearID, UpdatePupilProfile.CurrentSchoolID, UpdatePupilProfile.PupilProfileID);
                            lstClassID.Add(NewClassID);
                            lstClassID.Add(OldClassID);
                            isCalSMS = true;
                        }
                    }

                    PupilProfileBusiness.Update(UpdatePupilProfile);
                }
                PupilProfileBusiness.SaveDetect();
                lstClassID = lstClassID.Distinct().ToList();
                if (isCalSMS)
                {
                    //Tinh lai quy tin cho lop
                    SMSParentContractBusiness.GetListSMSParentContractClass(objAy.SchoolID, objAy.AcademicYearID, SemesterID, lstClassID);
                }
            }
            finally
            {
                SetAutoDetectChangesEnabled(true);
            }
        }

        #endregion UpdateImportPupilProfile

        #region UpdatePupilProfile

        /// <summary>
        /// Cập nhật thông tin học sinh
        /// </summary>
        /// <author>tungnd</author>
        /// <date>9/10/2012</date>
        /// <param name="ethnic">Đối tượng PupilProfile</param>
        /// <returns>Đối tượng PupilProfile</returns>
        public void UpdatePupilProfile(int UserID, PupilProfile UpdatePupilProfile, int EnrolmentType, int OldClassID, bool isImport = false)
        {

            // bắt dữ liệu truyền vào
            if (isImport == false)
            {
                Validate(UpdatePupilProfile);

            }
            int PupilID = UpdatePupilProfile.PupilProfileID;
            int ClassID = UpdatePupilProfile.CurrentClassID;
            int SchoolID = UpdatePupilProfile.CurrentSchoolID;
            int modSchoolID = SchoolID % 100;
            int AcademicYearID = UpdatePupilProfile.CurrentAcademicYearID;
            AcademicYear objAy = AcademicYearBusiness.Find(AcademicYearID);
            int year = objAy.Year;
            int EducationLevelID = 0;
            int SemesterID = 0;
            DateTime datetimeNow = DateTime.Now.Date;
            if (objAy.FirstSemesterStartDate <= datetimeNow && datetimeNow <= objAy.FirstSemesterEndDate)
            {
                SemesterID = GlobalConstants.SEMESTER_OF_YEAR_FIRST;
            }
            else if (objAy.SecondSemesterStartDate <= datetimeNow && datetimeNow <= objAy.SecondSemesterEndDate)
            {
                SemesterID = GlobalConstants.SEMESTER_OF_YEAR_SECOND;
            }
            if (UpdatePupilProfile.ClassProfile != null && EducationLevelBusiness.Find(UpdatePupilProfile.ClassProfile.EducationLevelID).Grade < 4)
            {
                EducationLevelID = UpdatePupilProfile.ClassProfile.EducationLevelID;
            }
            if (ClassID != OldClassID && EducationLevelID > 0)
            {
                bool MarkRecordConstraint = MarkRecordBusiness.All.Any(o => o.PupilID == PupilID && o.ClassID == OldClassID && o.Last2digitNumberSchool == modSchoolID && o.CreatedAcademicYear == year);
                if (MarkRecordConstraint)
                {
                    throw new BusinessException("PupilProfile_Validate_Using_MarkRecord");
                }
                bool JudgeRecordConstraint = JudgeRecordBusiness.All.Any(o => o.PupilID == PupilID && o.ClassID == OldClassID && o.Last2digitNumberSchool == modSchoolID && o.CreatedAcademicYear == year);
                if (JudgeRecordConstraint)
                {
                    throw new BusinessException("PupilProfile_Validate_Using_MarkRecord");
                }
                bool PupilRankingConstraint = PupilRankingBusiness.All.Any(o => o.PupilID == PupilID && o.ClassID == OldClassID && o.Last2digitNumberSchool == modSchoolID && o.CreatedAcademicYear == year);
                if (PupilRankingConstraint)
                {
                    throw new BusinessException("PupilProfile_Validate_Using_PupilRanking");
                }
                bool SummedUpRecordConstraint = SummedUpRecordBusiness.All.Any(o => o.PupilID == PupilID && o.ClassID == OldClassID && o.Last2digitNumberSchool == modSchoolID && o.CreatedAcademicYear == year);
                if (SummedUpRecordConstraint)
                {
                    throw new BusinessException("PupilProfile_Validate_Using_SummedUpRecord");
                }
                bool PupilAbsenseConstraint = PupilAbsenceBusiness.All.Any(o => o.PupilID == PupilID && o.ClassID == OldClassID);
                if (PupilAbsenseConstraint)
                {
                    throw new BusinessException("PupilProfile_Validate_Using_PupilAbsence");
                }
                bool PupilFaultConstraint = PupilFaultBusiness.All.Any(o => o.PupilID == PupilID && o.ClassID == OldClassID);
                if (PupilFaultConstraint)
                {
                    throw new BusinessException("PupilProfile_Validate_Using_PupilFault");
                }
            }

            if (UtilsBusiness.HasHeadTeacherPermission(UserID, UpdatePupilProfile.CurrentClassID))
            {
                UpdatePupilProfile.IsActive = true;

                //Tạo PupilOfSchool pof  = PupilOfSchoolBusiness.SearchBySchool(UserInfo.SchoolID, Dictionary)
                //với Dictionary[“AcademicYearID”] = entity.CurrentAcademicYearID,
                //Dictionary[“PupilID”] = entity.PupilProfileID

                PupilOfSchool pos = PupilOfSchoolBusiness.SearchBySchool(UpdatePupilProfile.CurrentSchoolID,
                    new Dictionary<string, object>()
                    {
                        {"SchoolID",UpdatePupilProfile.CurrentSchoolID},
                        {"PupilID",UpdatePupilProfile.PupilProfileID}
                    }).FirstOrDefault();
                pos.PupilID = UpdatePupilProfile.PupilProfileID;
                pos.SchoolID = UpdatePupilProfile.CurrentSchoolID;
                pos.EnrolmentDate = UpdatePupilProfile.EnrolmentDate;
                pos.EnrolmentType = (int)EnrolmentType;

                this.PupilOfSchoolBusiness.Update(pos);

                // insert dữ liệu vào bảng PupilOfClass
                AcademicYear AcademicYear = AcademicYearBusiness.Find(UpdatePupilProfile.CurrentAcademicYearID);

                List<PupilOfClass> lstpoc = PupilOfClassBusiness.SearchBySchool(UpdatePupilProfile.CurrentSchoolID,
                  new Dictionary<string, object>()
                    {
                        {"AcademicYearID",UpdatePupilProfile.CurrentAcademicYearID},
                        {"PupilID",UpdatePupilProfile.PupilProfileID}
                    }).ToList();
                PupilOfClass poc = lstpoc.Where(o => o.Status == SystemParamsInFile.PUPIL_STATUS_STUDYING).FirstOrDefault();

                poc.PupilID = UpdatePupilProfile.PupilProfileID;
                poc.ClassID = UpdatePupilProfile.CurrentClassID;
                poc.SchoolID = UpdatePupilProfile.CurrentSchoolID;
                poc.AcademicYearID = UpdatePupilProfile.CurrentAcademicYearID;
                poc.Year = AcademicYear.Year;
                //Neu hoc sinh dang hoc trong lop va truoc do chua co qua trinh chuyen lop doi voi lop do thi cap nhat lai ngay vao truong bang ngay vao lop
                if (lstpoc.Count == 1)
                {
                    poc.AssignedDate = UpdatePupilProfile.EnrolmentDate;
                }
                else
                {
                    var lstpoc_temp = lstpoc.OrderBy(o => o.AssignedDate);
                    PupilOfClass pocAssignMax = lstpoc_temp.LastOrDefault();
                    if (pocAssignMax != null && UpdatePupilProfile.EnrolmentDate >= pocAssignMax.AssignedDate)
                    {
                        //Ngày vào trường không thể lớn hơn ngày vào lớp đang học
                        throw new BusinessException("PupilProfile_Validate_AssignDate");

                    }
                    else
                    {
                        PupilOfClass pocAssignMin = lstpoc_temp.FirstOrDefault();

                        if (pocAssignMin != null)
                        {
                            pocAssignMin.AssignedDate = UpdatePupilProfile.EnrolmentDate;
                            this.PupilOfClassBusiness.Update(pocAssignMin);
                        }
                    }
                }
                poc.Status = UpdatePupilProfile.ProfileStatus;
                this.PupilOfClassBusiness.Update(poc);

                base.Update(UpdatePupilProfile);
            }
            //base.SaveDetect();
            PupilProfileBusiness.Save();
            if (ClassID != OldClassID)
            {
                //Cap nhat lai lop moi trong bang SMS_PARENT_CONTRACT
                SMSParentContractBusiness.UpdateClassIDToSMSParentContract(ClassID, OldClassID, SemesterID, AcademicYearID, SchoolID, UpdatePupilProfile.PupilProfileID);
                SMSParentContractBusiness.Save();
                SMSParentContractInClassDetailBusiness.Save();
                //Tinh lai quy tin cho lop
                List<int> lstClassID = new List<int>()
                {
                    ClassID,
                    OldClassID
                };
                SMSParentContractBusiness.GetListSMSParentContractClass(SchoolID, AcademicYearID, SemesterID, lstClassID);
            }
        }

        #endregion UpdatePupilProfile

        #region DeletePupilProfile

        /// <summary>
        /// Xoá thông tin học sinh
        /// </summary>
        /// <author>tungnd</author>
        /// <date>9/10/2012</date>
        /// <param name="ethnic">Đối tượng PupilProfile</param>
        public void DeletePupilProfile(int UserID, int PupilID, int SchoolID, int AcademicYearID, UserInfoBO userInfo)
        {
            if (PupilID == 0) return;

            string description = "Hồ sơ học sinh, HS:{0}, mã: {1}, lớp: {2} ";
            RESTORE_DATA objRes = new RESTORE_DATA
            {
                ACADEMIC_YEAR_ID = AcademicYearID,
                SCHOOL_ID = SchoolID,
                DELETED_DATE = DateTime.Now,
                DELETED_FULLNAME = userInfo.FullName,
                DELETED_USER = userInfo.UserName,
                RESTORED_DATE = DateTime.MinValue,
                RESTORED_STATUS = 0,
                RESTORE_DATA_ID = Guid.NewGuid(),
                RESTORE_DATA_TYPE_ID = RestoreDataConstant.RESTORE_DATA_TYPE_PUPIL,
                //SHORT_DESCRIPTION = description
            };


            #region AnhVD9 20140410 _ Delete from reference tables
            /* Khong thuc hien xoa du lieu cac bang lien quan nữa
            short IsOK = 0;
            bool result = this.context.DELETE_PUPIL_IN_REF_TABLE(PupilID.ToString(), GlobalConstants.SEMICOLON_DELEMITER, SchoolID, AcademicYearID, out IsOK);
            if (!result || IsOK != 1)
            {
                throw new BusinessException("Common_Label_DeleteFailureMessage");
            }
            */
            #endregion End Delete from reference tables

            // pupil_of_class
            List<PupilOfClass> pupilOfClass = PupilOfClassBusiness.All.Where(p => p.PupilID == PupilID).ToList();
            if (pupilOfClass != null)
            {
                // Xóa trong năm học hiện tại
                List<PupilOfClass> poc_currentYear = pupilOfClass.Where(o => o.AcademicYearID == AcademicYearID).ToList();
                PupilOfClassBusiness.DeletePupilOfClass(poc_currentYear);

                int classId = poc_currentYear.FirstOrDefault().ClassID;
                ClassProfile classProfile = ClassProfileBusiness.Find(classId);

                // Lay thong tin PupilProfile
                PupilProfile pupilprofile = PupilProfileRepository.Find(PupilID);

                objRes.SHORT_DESCRIPTION = string.Format(description, pupilprofile.FullName, pupilprofile.PupilCode, classProfile.DisplayName);

                if (pupilOfClass.Count == 1)
                {
                    // kiểm tra xem dữ liệu định xóa có tồn tại không
                    bool checkDelete = new PupilProfileRepository(this.context).ExistsRow(GlobalConstants.PUPIL_SCHEMA, "PupilProfile",
                    new Dictionary<string, object>()
                {
                    {"PupilProfileID",PupilID},
                                                {"CurrentSchoolID", SchoolID},
                                                {"IsActive", true}
                }, null);
                    if (!checkDelete)
                    {
                        throw new BusinessException("Common_Delete_Exception");
                    }

                    if (UtilsBusiness.HasHeadTeacherPermission(UserID, pupilprofile.CurrentClassID))
                        pupilprofile.IsActive = false;
                }

                pupilOfClass = pupilOfClass.Except(poc_currentYear).Where(p => p.Status == SystemParamsInFile.PUPIL_STATUS_STUDYING).ToList();
                // AnhVD9 - Cập nhật lại Current_Class và Current_AcademicYear cho HS
                pupilOfClass = PupilOfClassBusiness.All.Where(p => p.PupilID == PupilID && p.SchoolID == SchoolID &&
                                    p.Status == SystemParamsInFile.PUPIL_STATUS_STUDYING).ToList();

                PupilOfClass pupilOfClassRecentest = pupilOfClass.FirstOrDefault(p => p.Year == pupilOfClass.Max(o => o.Year));
                if (pupilOfClassRecentest != null)
                {
                    pupilprofile.CurrentClassID = pupilOfClassRecentest.ClassID;
                    pupilprofile.CurrentAcademicYearID = pupilOfClassRecentest.AcademicYearID;
                }

                pupilprofile.ModifiedDate = DateTime.Now;
                base.Update(pupilprofile);
                PupilProfileBusiness.Save();
                //Luu thong tin phuc hoi du lieu
                List<PupilProfile> lstRestorePP = new List<PupilProfile>();
                lstRestorePP.Add(pupilprofile);

                List<RESTORE_DATA_DETAIL> lstRestoreDetail = SavePupilInRestoreData(poc_currentYear, lstRestorePP, objRes);

                if (lstRestoreDetail != null && lstRestoreDetail.Count > 0)
                {
                    RestoreDataBusiness.Insert(objRes);
                    RestoreDataBusiness.Save();
                    RestoreDataDetailBusiness.BulkInsert(lstRestoreDetail, ColumnMapping.Instance.RestoreDataDetail(), "RESTORE_DATA_DETAIL_ID");
                }
                // End 20141003
            }
        }

        public void DeletePupilProfileChildren(int UserID, int PupilID, int SchoolID, int AcademicYearID)
        {
            if (PupilID == 0)
                return;

            #region AnhVD9 20150103 - Thay đổi luồng xóa
            //// kiểm tra học sinh đã có dữ liệu liên quan
            //// PupilID using PupilAbsence
            //bool PupilAbsenseConstraint = PupilAbsenceBusiness.All.Any(o => o.PupilID == PupilID && o.AcademicYearID == AcademicYearID);
            //if (PupilAbsenseConstraint)
            //{
            //    throw new BusinessException("PupilProfileChildren_Validate_Using");
            //}

            //// PupilID using GoodChildrenTicket
            //bool goodChildrenTicketConstraint = GoodChildrenTicketBusiness.All.Any(o => o.PupilID == PupilID && o.AcademicYearID == AcademicYearID);
            //if (goodChildrenTicketConstraint)
            //{
            //    throw new BusinessException("PupilProfileChildren_Validate_Using");
            //}

            //// PupilID using DevelopmentOfChildren
            //bool DevelopmentOfChildrenConstraint = DevelopmentOfChildrenBusiness.All.Any(o => o.PupilID == PupilID && o.AcademicYearID == AcademicYearID);
            //if (DevelopmentOfChildrenConstraint)
            //{
            //    throw new BusinessException("PupilProfileChildren_Validate_Using");
            //}

            //bool CheckActivityOfPupil = ActivityOfPupilBusiness.All.Any(o => o.PupilID == PupilID && o.AcademicYearID == AcademicYearID);
            //if (CheckActivityOfPupil)
            //{
            //    throw new BusinessException("PupilProfileChildren_Validate_Using");
            //}
            //bool CheckAddMenuForChildren = AddMenuForChildrenBusiness.All.Any(o => o.PupilID == PupilID && o.AcademicYearID == AcademicYearID);
            //if (CheckAddMenuForChildren)
            //{
            //    throw new BusinessException("PupilProfileChildren_Validate_Using");
            //}
            //bool CheckNotEatingChildren = NotEatingChildrenBusiness.All.Any(o => o.PupilID == PupilID && o.AcademicYearID == AcademicYearID);
            //if (CheckNotEatingChildren)
            //{
            //    throw new BusinessException("PupilProfileChildren_Validate_Using");
            //}
            //List<HabitOfChildren> CheckHabitOfChildrenList = (from a in HabitOfChildrenBusiness.All
            //                                                  join b in PupilOfClassBusiness.All on a.PupilID equals b.PupilID
            //                                                  where a.PupilID == PupilID && b.AcademicYearID == AcademicYearID && b.SchoolID == SchoolID
            //                                                  select a).ToList();
            //if (CheckHabitOfChildrenList.Count > 0)
            //{
            //    throw new BusinessException("PupilProfileChildren_Validate_Using");
            //}

            //List<FoodHabitOfChildren> FoodHabitOfChildrenList = (from a in FoodHabitOfChildrenBusiness.All
            //                                                     join b in PupilOfClassBusiness.All on a.PupilID equals b.PupilID
            //                                                     where a.PupilID == PupilID && b.AcademicYearID == AcademicYearID && b.SchoolID == SchoolID
            //                                                     select a).ToList();
            //if (FoodHabitOfChildrenList.Count > 0)
            //{
            //    throw new BusinessException("PupilProfileChildren_Validate_Using");
            //}



            //// Kiem tra hoc sinh co trong pupil_of_class khong.Neu co thi xoa khong thi thoi
            //List<PupilOfClass> pupilOfClass = PupilOfClassBusiness.All.Where(p => p.PupilID == PupilID && p.AcademicYearID == AcademicYearID).ToList();
            //if (pupilOfClass != null)
            //{
            //    PupilOfClassBusiness.DeletePupilOfClass(pupilOfClass);
            //}
            //else
            //{               
            //    // kiểm tra xem dữ liệu định xóa có tồn tại không
            //    bool checkDelete = new PupilProfileRepository(this.context).ExistsRow(GlobalConstants.PUPIL_SCHEMA, "PupilProfile",
            //    new Dictionary<string, object>()
            //    {
            //        {"PupilProfileID",PupilID},
            //        {"CurrentSchoolID",SchoolID},{"IsActive",true}
            //    }, null);
            //    if (!checkDelete)
            //    {
            //        throw new BusinessException("Common_Delete_Exception");
            //    }

            //    //  PupilID và CurrentAcademicYearID not compatible trong PupilProfile
            //    PupilProfile pupilprofile = PupilProfileRepository.Find(PupilID);

            //    IDictionary<string, object> SearchInfo = new Dictionary<string, object>();
            //    SearchInfo["PupilProfileID"] = PupilID;
            //    SearchInfo["CurrentAcademicYearID"] = pupilprofile.CurrentAcademicYearID;
            //    SearchInfo["IsActive"] = true;
            //    bool SearchInfoExist = new PupilProfileRepository(this.context).ExistsRow(GlobalConstants.PUPIL_SCHEMA, "PupilProfile", SearchInfo, null);

            //    if (!SearchInfoExist)
            //    {
            //        throw new BusinessException("Common_Validate_NotCompatible");
            //    }

            //    if (UtilsBusiness.HasHeadTeacherPermission(UserID, pupilprofile.CurrentClassID))
            //    {
            //        base.Delete(PupilID, true);
            //    }
            //}
            #endregion

            #region AnhVD9 20150203 _ Delete from reference tables
            short IsOK = 0;
            bool result = this.context.DELETE_PUPIL_IN_REF_TABLE(PupilID.ToString(), GlobalConstants.SEMICOLON_DELEMITER, SchoolID, AcademicYearID, out IsOK);
            if (!result || IsOK != 1)
            {
                throw new BusinessException("Common_Label_DeleteFailureMessage");
            }
            #endregion End Delete from reference tables

            // pupil_of_class
            List<PupilOfClass> pupilOfClass = PupilOfClassBusiness.All.Where(p => p.PupilID == PupilID).ToList();
            if (pupilOfClass != null)
            {
                // Xóa trong năm học hiện tại
                List<PupilOfClass> poc_currentYear = pupilOfClass.Where(o => o.AcademicYearID == AcademicYearID).ToList();
                PupilOfClassBusiness.DeletePupilOfClass(poc_currentYear);

                // Lay thong tin PupilProfile
                PupilProfile pupilprofile = PupilProfileRepository.Find(PupilID);
                if (pupilOfClass.Count == 1)
                {
                    // kiểm tra xem dữ liệu định xóa có tồn tại không
                    bool checkDelete = new PupilProfileRepository(this.context).ExistsRow(GlobalConstants.PUPIL_SCHEMA, "PupilProfile",
                    new Dictionary<string, object>()
                {
                    {"PupilProfileID",PupilID},
                        {"CurrentSchoolID", SchoolID},
                        {"IsActive", true}
                }, null);
                    if (!checkDelete)
                    {
                        throw new BusinessException("Common_Delete_Exception");
                    }

                    if (UtilsBusiness.HasHeadTeacherPermission(UserID, pupilprofile.CurrentClassID))
                        pupilprofile.IsActive = false;
                }

                pupilOfClass = pupilOfClass.Except(poc_currentYear).Where(p => p.Status == SystemParamsInFile.PUPIL_STATUS_STUDYING).ToList();
                // AnhVD9 - Cập nhật lại Current_Class và Current_AcademicYear cho HS
                //pupilOfClass = PupilOfClassBusiness.All.Where(p => p.PupilID == PupilID && p.SchoolID == SchoolID &&
                //                    p.Status == SystemParamsInFile.PUPIL_STATUS_STUDYING).ToList();

                PupilOfClass pupilOfClassRecentest = pupilOfClass.FirstOrDefault(p => p.Year == pupilOfClass.Max(o => o.Year));
                if (pupilOfClassRecentest != null)
                {
                    pupilprofile.CurrentClassID = pupilOfClassRecentest.ClassID;
                    pupilprofile.CurrentAcademicYearID = pupilOfClassRecentest.AcademicYearID;
                }

                pupilprofile.ModifiedDate = DateTime.Now;
                base.Update(pupilprofile);
                // End 20150203
            }
        }


        /// <summary>
        /// Xoá thông tin học sinh
        /// </summary>
        /// <author>tungnd</author>
        /// <date>9/10/2012</date>
        /// <param name="ethnic">Đối tượng PupilProfile</param>

        public List<RESTORE_DATA_DETAIL> DeleteListPupilProfile(int UserID, List<int> ListPupilID, int SchoolID, int AcademicYearID, RESTORE_DATA objRestore)
        {
            try
            {
                SetAutoDetectChangesEnabled(false);

                if (ListPupilID == null || ListPupilID.Count == 0)
                {
                    return new List<RESTORE_DATA_DETAIL>();
                }

                // Danh sách HS ở tất cả các năm học
                List<PupilOfClass> LstPOC = PupilOfClassBusiness.All.Where(p => ListPupilID.Contains(p.PupilID)).ToList();
                //Danh sách hoc sinh có trong pupil_of_class của năm học đang cần xóa
                List<PupilOfClass> LstPOCInCurrent = LstPOC.Where(p => ListPupilID.Contains(p.PupilID) && p.AcademicYearID == AcademicYearID).ToList();
                //Danh sach ID học sinh có trong pupil_of_class của năm học đang cần xóa
                List<int> lstPupilIDOfClassCurrent = LstPOCInCurrent.Select(p => p.PupilID).ToList();
                // Danh sách ID của học sinh không có trong năm học cần xóa (ở các năm học khác vẫn còn thì sẽ cập nhật lại current_class_id và current_academic_year_id 
                // không xóa các HS này
                List<PupilOfClass> LstPOCInOtherYear = LstPOC.Except(LstPOCInCurrent).ToList();
                List<int> LstPupilIDUpdateNotDeactive = LstPOC.Except(LstPOCInCurrent).Select(o => o.PupilID).ToList();
                // Danh sách sẽ tiến hành xóa
                List<int> LstPupilIDDeactive = ListPupilID.Where(p => !LstPupilIDUpdateNotDeactive.Contains(p)).ToList();

                //Xóa học sinh có trong pupil_of_class
                if (LstPOCInCurrent.Count > 0)
                {
                    PupilOfClassBusiness.DeleteAll(LstPOCInCurrent);
                    //PupilOfClassBusiness.DeletePupilOfClass(LstPOCInCurrent);
                }

                List<PupilProfile> listPupilProfile = (from pp2 in PupilProfileBusiness.All
                                                       join poc in PupilOfClassBusiness.All.Where(p => p.AcademicYearID == AcademicYearID
                                                                                                     && p.SchoolID == SchoolID
                                                                                                     && ListPupilID.Contains(p.PupilID)) on pp2.PupilProfileID equals poc.PupilID
                                                       select pp2).ToList();

                // Kiểm tra quyền xóa của GV trên lớp 
                List<int> listClassID = listPupilProfile.Select(o => o.CurrentClassID).Distinct().ToList();
                foreach (int classID in listClassID)
                {
                    if (!UtilsBusiness.HasHeadTeacherPermission(UserID, classID))
                    {
                        throw new BusinessException("Common_Label_PemissionTeacher", new List<object>() { ClassProfileBusiness.Find(classID).DisplayName });
                    }
                }

                // Tiến hành deactive các HS chỉ có 1 record trong năm học hiện tại
                // Cập nhật lại trạng thái cho các HS còn có chi tiết ở các năm học khác
                PupilProfile pp;
                List<PupilOfClass> LstPocDetail = new List<PupilOfClass>();
                PupilOfClass pupilOfClassMax;
                for (int i = listPupilProfile.Count - 1; i >= 0; i--)
                {
                    pp = listPupilProfile[i];
                    // AnhVD9 - Cập nhật lại năm học và lớp hiện tại
                    if (LstPupilIDUpdateNotDeactive.Contains(pp.PupilProfileID))
                    {
                        LstPocDetail = LstPOCInOtherYear.Where(p => p.PupilID == pp.PupilProfileID && p.SchoolID == SchoolID &&
                                            p.Status == SystemParamsInFile.PUPIL_STATUS_STUDYING).ToList();
                        pupilOfClassMax = LstPocDetail.FirstOrDefault(p => p.Year == LstPocDetail.Max(o => o.Year));
                        if (pupilOfClassMax != null)
                        {
                            pp.CurrentClassID = pupilOfClassMax.ClassID;
                            pp.CurrentAcademicYearID = pupilOfClassMax.AcademicYearID;
                            pp.ModifiedDate = DateTime.Now;
                        }
                    }
                    else if (LstPupilIDDeactive.Contains(pp.PupilProfileID))
                    {
                        pp.ModifiedDate = DateTime.Now;
                        pp.IsActive = false;
                    }
                    //PupilProfileBusiness.Update(pp);
                }
                PupilProfileBusiness.Save();
                List<RESTORE_DATA_DETAIL> lstRestoreDetail = SavePupilInRestoreData(LstPOCInCurrent, listPupilProfile, objRestore);
                return lstRestoreDetail;
            }
            finally
            {
                SetAutoDetectChangesEnabled(true);
            }
        }

        public void DeleteListPupilProfileChildren(int UserID, List<int> ListPupilID, int SchoolID, int AcademicYearID, RESTORE_DATA objRestore)
        {
            if (ListPupilID == null || ListPupilID.Count == 0)
            {
                return;
            }

            #region AnhVD9 20150203 Comment - Sửa luồng xóa HS
            //// kiểm tra học sinh đã có dữ liệu liên quan
            //// PupilID using PupilAbsence
            //bool PupilAbsenseConstraint = PupilAbsenceBusiness.All.Any(o => ListPupilID.Contains(o.PupilID) && o.AcademicYearID == AcademicYearID);
            //if (PupilAbsenseConstraint)
            //{
            //    throw new BusinessException("PupilProfileChildren_Validate_Using");
            //}

            //// PupilID using GoodChildrenTicket
            //bool goodChildrenTicketConstraint = GoodChildrenTicketBusiness.All.Any(o => ListPupilID.Contains(o.PupilID) && o.AcademicYearID == AcademicYearID);
            //if (goodChildrenTicketConstraint)
            //{
            //    throw new BusinessException("PupilProfileChildren_Validate_Using");
            //}

            //// PupilID using DevelopmentOfChildren
            //bool DevelopmentOfChildrenConstraint = DevelopmentOfChildrenBusiness.All.Any(o => ListPupilID.Contains(o.PupilID) && o.AcademicYearID == AcademicYearID);
            //if (DevelopmentOfChildrenConstraint)
            //{
            //    throw new BusinessException("PupilProfileChildren_Validate_Using");
            //}

            //bool CheckActivityOfPupil = ActivityOfPupilBusiness.All.Any(o => ListPupilID.Contains(o.PupilID) && o.AcademicYearID == AcademicYearID);
            //if (CheckActivityOfPupil)
            //{
            //    throw new BusinessException("PupilProfileChildren_Validate_Using");
            //}
            //bool CheckAddMenuForChildren = AddMenuForChildrenBusiness.All.Any(o => ListPupilID.Contains(o.PupilID) && o.AcademicYearID == AcademicYearID);
            //if (CheckAddMenuForChildren)
            //{
            //    throw new BusinessException("PupilProfileChildren_Validate_Using");
            //}
            //bool CheckNotEatingChildren = NotEatingChildrenBusiness.All.Any(o => ListPupilID.Contains(o.PupilID) && o.AcademicYearID == AcademicYearID);
            //if (CheckNotEatingChildren)
            //{
            //    throw new BusinessException("PupilProfileChildren_Validate_Using");
            //}
            //List<HabitOfChildren> CheckHabitOfChildrenList = (from a in HabitOfChildrenBusiness.All
            //                                                  join b in PupilOfClassBusiness.All on a.PupilID equals b.PupilID
            //                                                  where ListPupilID.Contains(a.PupilID) && b.AcademicYearID == AcademicYearID && b.SchoolID == SchoolID
            //                                                  select a).ToList();
            //if (CheckHabitOfChildrenList.Count > 0)
            //{
            //    throw new BusinessException("PupilProfileChildren_Validate_Using");
            //}

            //List<FoodHabitOfChildren> FoodHabitOfChildrenList = (from a in FoodHabitOfChildrenBusiness.All
            //                                                     join b in PupilOfClassBusiness.All on a.PupilID equals b.PupilID
            //                                                     where ListPupilID.Contains(a.PupilID) && b.AcademicYearID == AcademicYearID && b.SchoolID == SchoolID
            //                                                     select a).ToList();
            //if (FoodHabitOfChildrenList.Count > 0)
            //{
            //    throw new BusinessException("PupilProfileChildren_Validate_Using");
            //}

            ////kiem tra hoc sinh co trong pupil_of_class o nam hoc hien tai khong? neu co thi xoa trong pupil_of_class di, neu khong co thi xoa han hoc sinh trong pupil_profile
            ////Danh sách hoc sinh có trong pupil_of_class của năm học hiện tại
            //List<PupilOfClass> PupilOfClassList = PupilOfClassBusiness.All.Where(p => ListPupilID.Contains(p.PupilID) && p.AcademicYearID == AcademicYearID).ToList();
            ////Danh sach ID học sinh có trong pupil_of_class của năm học hiện tại
            //List<int> PupilIDOfClassList = PupilOfClassList.Select(p => p.PupilID).ToList();
            //// Danh sachs ID của học sinh không có trong pupil_of_class của năm học hiện tại
            //List<int> PupilIDNotOfClassList = ListPupilID.Where(p => !PupilIDOfClassList.Contains(p)).ToList();

            ////Xóa học sinh có trong pupil_of_class
            //if (PupilOfClassList.Count > 0)
            //{
            //    PupilOfClassBusiness.DeletePupilOfClass(PupilOfClassList);
            //}

            ////Xóa học sinh không có trong pupil_of_class
            //if (PupilIDNotOfClassList.Count > 0)
            //{
            //    // kiểm tra xem dữ liệu định xóa có tồn tại không
            //    List<PupilProfile> listPupilProfile = PupilProfileBusiness.All.Where(o => o.CurrentSchoolID == SchoolID
            //        && ListPupilID.Contains(o.PupilProfileID)).ToList();
            //    bool checkDelete = listPupilProfile.Select(o => o.PupilProfileID).Distinct().Count() == ListPupilID.Count ? true : false;
            //    if (!checkDelete)
            //    {
            //        throw new BusinessException("Common_Delete_Exception");
            //    }

            //    bool ExistNotACtiveOrCurrentYear = listPupilProfile.Any(o => !o.IsActive || o.CurrentAcademicYearID != AcademicYearID);

            //    if (ExistNotACtiveOrCurrentYear)
            //    {
            //        throw new BusinessException("Common_Validate_NotCompatible");
            //    }
            //    List<int> listClassID = listPupilProfile.Select(o => o.CurrentClassID).Distinct().ToList();
            //    foreach (int classID in listClassID)
            //    {
            //        if (!UtilsBusiness.HasHeadTeacherPermission(UserID, classID))
            //        {
            //            throw new BusinessException("Common_Label_PemissionTeacher", new List<object>() { ClassProfileBusiness.Find(classID).DisplayName });
            //        }
            //    }
            //    foreach (PupilProfile pp in listPupilProfile)
            //    {
            //        pp.ModifiedDate = DateTime.Now;
            //        pp.IsActive = false;
            //        base.Update(pp);
            //    }
            //}
            #endregion

            #region AnhVD9 20150203 _ Delete from reference tables
            StringBuilder LstStrPID = new StringBuilder();
            for (int i = ListPupilID.Count - 1; i >= 0; i--)
            {
                LstStrPID.Append(ListPupilID[i]);
                if (i > 0) LstStrPID.Append(GlobalConstants.SEMICOLON_DELEMITER);
            }
            short IsOK = 0;
            bool result = this.context.DELETE_PUPIL_IN_REF_TABLE(LstStrPID.ToString(), GlobalConstants.SEMICOLON_DELEMITER, SchoolID, AcademicYearID, out IsOK);
            if (!result || IsOK != 1)
            {
                throw new BusinessException("Common_Label_DeleteFailureMessage");
            }
            #endregion End Delete from reference tables

            // Danh sách HS ở tất cả các năm học
            List<PupilOfClass> LstPOC = PupilOfClassBusiness.All.Where(p => ListPupilID.Contains(p.PupilID)).ToList();
            //Danh sách hoc sinh có trong pupil_of_class của năm học đang cần xóa
            List<PupilOfClass> LstPOCInCurrent = PupilOfClassBusiness.All.Where(p => ListPupilID.Contains(p.PupilID) && p.AcademicYearID == AcademicYearID).ToList();
            //Danh sach ID học sinh có trong pupil_of_class của năm học đang cần xóa
            List<int> lstPupilIDOfClassCurrent = LstPOCInCurrent.Select(p => p.PupilID).ToList();
            // Danh sách ID của học sinh không có trong năm học cần xóa (ở các năm học khác vẫn còn thì sẽ cập nhật lại current_class_id và current_academic_year_id 
            // không xóa các HS này
            List<PupilOfClass> LstPOCInOtherYear = LstPOC.Except(LstPOCInCurrent).ToList();
            List<int> LstPupilIDUpdateNotDeactive = LstPOC.Except(LstPOCInCurrent).Select(o => o.PupilID).ToList();
            // Danh sách sẽ tiến hành xóa
            List<int> LstPupilIDDeactive = ListPupilID.Where(p => !LstPupilIDUpdateNotDeactive.Contains(p)).ToList();

            //Xóa học sinh có trong pupil_of_class
            if (lstPupilIDOfClassCurrent.Count > 0)
            {
                PupilOfClassBusiness.DeletePupilOfClass(LstPOCInCurrent);
            }

            List<PupilProfile> listPupilProfile = PupilProfileBusiness.All.Where(o => o.CurrentSchoolID == SchoolID && ListPupilID.Contains(o.PupilProfileID)).ToList();

            // Kiểm tra quyền xóa của GV trên lớp 
            List<int> listClassID = listPupilProfile.Select(o => o.CurrentClassID).Distinct().ToList();
            foreach (int classID in listClassID)
            {
                if (!UtilsBusiness.HasHeadTeacherPermission(UserID, classID))
                {
                    throw new BusinessException("Common_Label_PemissionTeacher", new List<object>() { ClassProfileBusiness.Find(classID).DisplayName });
                }
            }

            // Tiến hành deactive các HS chỉ có 1 record trong năm học hiện tại
            // Cập nhật lại trạng thái cho các HS còn có chi tiết ở các năm học khác
            PupilProfile pp;
            List<PupilOfClass> LstPocDetail = new List<PupilOfClass>();
            PupilOfClass pupilOfClassMax;
            for (int i = listPupilProfile.Count - 1; i >= 0; i--)
            {
                pp = listPupilProfile[i];
                // AnhVD9 - Cập nhật lại năm học và lớp hiện tại
                if (LstPupilIDUpdateNotDeactive.Contains(pp.PupilProfileID))
                {
                    LstPocDetail = LstPOCInOtherYear.Where(p => p.PupilID == pp.PupilProfileID && p.SchoolID == SchoolID &&
                                        p.Status == SystemParamsInFile.PUPIL_STATUS_STUDYING).ToList();
                    pupilOfClassMax = LstPocDetail.FirstOrDefault(p => p.Year == LstPocDetail.Max(o => o.Year));
                    if (pupilOfClassMax != null)
                    {
                        pp.CurrentClassID = pupilOfClassMax.ClassID;
                        pp.CurrentAcademicYearID = pupilOfClassMax.AcademicYearID;
                        pp.ModifiedDate = DateTime.Now;
                    }
                }
                else if (LstPupilIDDeactive.Contains(pp.PupilProfileID))
                {
                    pp.ModifiedDate = DateTime.Now;
                    pp.IsActive = false;
                }
                base.Update(pp);
            }
        }

        #endregion DeletePupilProfile

        #region Search
        /// <summary>
        /// Tìm kiếm thông tin học sinh
        /// </summary>
        /// <author>tungnd</author>
        /// <date>9/10/2012</date>
        /// <param name="ethnic">Đối tượng PupilProfile</param>
        /// <returns>Đối tượng PupilProfile</returns>
        public IQueryable<PupilProfileBO> Search(IDictionary<string, object> dic)
        {
            int CurrentClassID = 0;
            int PupilProfileID = Utils.GetInt(dic, "PupilProfileID");
            //int CurrentClassID = Utils.GetInt(dic, "CurrentClassID");
            int CurrentSchoolID = Utils.GetInt(dic, "CurrentSchoolID");
            int CurrentAcademicYearID = Utils.GetInt(dic, "CurrentAcademicYearID");
            //chiendd1: bo sung them AcademicYearID do nhieu chuc nang dang truyen AcademicYearID khong phai CurrentAcademicYearID
            int AcademicYearID = Utils.GetInt(dic, "AcademicYearID");
            int AreaID = Utils.GetInt(dic, "AreaID");
            int ProvinceID = Utils.GetInt(dic, "ProvinceID");
            int DistrictID = Utils.GetInt(dic, "DistrictID");
            int CommuneID = Utils.GetInt(dic, "CommuneID");
            int EthnicID = Utils.GetInt(dic, "EthnicID");
            int ReligionID = Utils.GetInt(dic, "ReligionID");
            int PolicyTargetID = Utils.GetInt(dic, "PolicyTargetID");
            int FamilyTypeID = Utils.GetInt(dic, "FamilyTypeID");
            int PriorityTypeID = Utils.GetInt(dic, "PriorityTypeID");
            int PreviousGraduationLevelID = Utils.GetInt(dic, "PreviousGraduationLevelID");
            string PupilCode = Utils.GetString(dic, "PupilCode");
            string FullName = Utils.GetString(dic, "FullName");
            int Genre = Utils.GetInt(dic, "Genre", -1);
            DateTime? BirthDate = Utils.GetDateTime(dic, "BirthDate");
            string BirthPlace = Utils.GetString(dic, "BirthPlace");
            string HomeTown = Utils.GetString(dic, "HomeTown");
            string Telephone = Utils.GetString(dic, "Telephone");
            string Mobile = Utils.GetString(dic, "Mobile");
            string Email = Utils.GetString(dic, "Email");
            string TempResidentalAddress = Utils.GetString(dic, "TempResidentalAddress");
            string PermanentResidentalAddress = Utils.GetString(dic, "PermanentResidentalAddress");
            Boolean IsDisabled = Utils.GetBool(dic, "IsDisabled ");
            int BloodType = Utils.GetInt(dic, "BloodType");
            double EnrolmentMark = Utils.GetDouble(dic, "EnrolmentMark ");
            int ForeignLanguageTraining = Utils.GetInt(dic, "ForeignLanguageTraining");
            Boolean IsYoungPioneerMember = Utils.GetBool(dic, "IsYoungPioneerMember");
            DateTime? YoungPioneerJoinedDate = Utils.GetDateTime(dic, "YoungPioneerJoinedDate");
            string YoungPioneerJoinedPlace = Utils.GetString(dic, "YoungPioneerJoinedPlace");
            Boolean IsYouthLeageMember = Utils.GetBool(dic, "IsYouthLeageMember ");
            DateTime? YouthLeagueJoinedDate = Utils.GetDateTime(dic, "YouthLeagueJoinedDate");
            string YouthLeagueJoinedPlace = Utils.GetString(dic, "YouthLeagueJoinedPlace");
            Boolean IsCommunistPartyMember = Utils.GetBool(dic, "IsCommunistPartyMember ");
            DateTime? CommunistPartyJoinedDate = Utils.GetDateTime(dic, "CommunistPartyJoinedDate");
            string CommunistPartyJoinedPlace = Utils.GetString(dic, "CommunistPartyJoinedPlace");
            string FatherFullName = Utils.GetString(dic, "FatherFullName");
            DateTime? FatherBirthDate = Utils.GetDateTime(dic, "FatherBirthDate");
            string FatherJob = Utils.GetString(dic, "FatherJob");
            string MotherFullName = Utils.GetString(dic, "MotherFullName");
            string FatherMobile = Utils.GetString(dic, "FatherMobile");
            DateTime? MotherBirthDate = Utils.GetDateTime(dic, "MotherBirthDate");
            string MotherJob = Utils.GetString(dic, "MotherJob");
            string MotherMobile = Utils.GetString(dic, "MotherMobile");
            string SponsorFullName = Utils.GetString(dic, "SponsorFullName");
            DateTime? SponsorBirthDate = Utils.GetDateTime(dic, "SponsorBirthDate");
            string SponsorJob = Utils.GetString(dic, "SponsorJob");
            int ProfileStatus = Utils.GetInt(dic, "ProfileStatus");
            Boolean IsActive = Utils.GetBool(dic, "IsActive ");
            DateTime? ModifiedDate = Utils.GetDateTime(dic, "ModifiedDate");
            int AppliedLevel = Utils.GetInt(dic, "AppliedLevel");
            int EducationLevelID = Utils.GetInt(dic, "EducationLevelID");
            List<int> ListClassID = Utils.GetIntList(dic, "ListClassID");
            List<int> ListNullableClassID = Utils.GetNullableList<int>(dic, "ListNullableClassID");
            string strSearch = Utils.GetString(dic, "strSearch");

            string strCurrentClassID = string.Empty;
            string strCombineClassID = string.Empty;
            if (EducationLevelID > 0 && EducationLevelID == SystemParamsInFile.Combine_Class_ID)
            {
                strCombineClassID = Utils.GetString(dic, "CurrentClassID");
            }
            else if (EducationLevelID > 0 && EducationLevelID != SystemParamsInFile.Combine_Class_ID)
            {
                CurrentClassID = Utils.GetInt(dic, "CurrentClassID");
                strCurrentClassID = Utils.GetString(dic, "StrCurrentClassID");
            }
            else if (EducationLevelID <= 0)
            {
                CurrentClassID = Utils.GetInt(dic, "CurrentClassID");
            }


            // bắt đầu tìm kiếm
            IQueryable<PupilOfClass> iqPupilOfClass = PupilOfClassBusiness.AllNoTracking;
            IQueryable<ClassProfile> iqClass = ClassProfileBusiness.AllNoTracking.Where(cp => (!cp.IsActive.HasValue || cp.IsActive == true));
            IQueryable<PupilProfile> iqPupil = PupilProfileBusiness.AllNoTracking;
            if (CurrentAcademicYearID > 0)
            {
                iqPupilOfClass = iqPupilOfClass.Where(pc => pc.AcademicYearID == CurrentAcademicYearID);
                iqClass = iqClass.Where(cp => cp.AcademicYearID == CurrentAcademicYearID);
            }

            if (AcademicYearID > 0)
            {
                iqPupilOfClass = iqPupilOfClass.Where(pc => pc.AcademicYearID == AcademicYearID);
                iqClass = iqClass.Where(cp => cp.AcademicYearID == AcademicYearID);
            }
            if (CurrentClassID > 0)
            {
                iqPupilOfClass = iqPupilOfClass.Where(pc => pc.ClassID == CurrentClassID);
                iqClass = iqClass.Where(cp => cp.ClassProfileID == CurrentClassID);
            }

            if (EducationLevelID == SystemParamsInFile.Combine_Class_ID)
            {
                iqClass = iqClass.Where(cp => cp.IsCombinedClass == true);
                if (!string.IsNullOrEmpty(strCombineClassID))
                {
                    iqClass = iqClass.Where(cp => cp.CombinedClassCode == strCombineClassID);
                }
                List<int> _lstClassID = iqClass.Select(x => x.ClassProfileID).Distinct().ToList();
                iqPupilOfClass = iqPupilOfClass.Where(pc => _lstClassID.Contains(pc.ClassID));
            }
            else if (EducationLevelID != SystemParamsInFile.Combine_Class_ID)
            {                                 
                if (!string.IsNullOrEmpty(strCurrentClassID) && strCurrentClassID == "ValueIsNullOrEmpty")
                {
                    iqClass = iqClass.Where(cp => cp.IsCombinedClass == false);
                    List<int> _lstClassID = iqClass.Select(x => x.ClassProfileID).Distinct().ToList();
                    iqPupilOfClass = iqPupilOfClass.Where(pc => _lstClassID.Contains(pc.ClassID));
                }             
            }

            if (ProfileStatus != 0)
            {
                iqPupilOfClass = iqPupilOfClass.Where(pc => pc.Status == ProfileStatus);
            }

            // Nếu đã có khối thì không cần phải tìm theo cả danh sách khối nữa
            if (EducationLevelID != 0 && EducationLevelID != SystemParamsInFile.Combine_Class_ID)
            {
                iqClass = iqClass.Where(pp => (pp.EducationLevelID == EducationLevelID));
            }
            else if (EducationLevelID != SystemParamsInFile.Combine_Class_ID)
            {
                IQueryable<EducationLevel> ls = EducationLevelBusiness.All.Where(o => (o.Grade == AppliedLevel) && o.IsActive);

                if (AppliedLevel != 0)
                {
                    iqClass = iqClass.Where(o => ls.Any(u => u.EducationLevelID == o.EducationLevelID));
                }
            }

            if (AppliedLevel != 0)
            {
                iqClass = iqClass.Where(x => x.EducationLevel.Grade == AppliedLevel);
            }

            if (ListClassID != null && ListClassID.Count > 0)
            {
                iqClass = iqClass.Where(pp => ListClassID.Contains(pp.ClassProfileID));
            }

            if (ListNullableClassID != null)
            {
                iqClass = iqClass.Where(pp => ListNullableClassID.Contains(pp.ClassProfileID));
            }


            if (PupilProfileID != 0)
            {
                iqPupil = iqPupil.Where(pp => (pp.PupilProfileID == PupilProfileID));
            }

            /*if (CurrentSchoolID != 0)
            {
                iqPupil = iqPupil.Where(pp => (pp.CurrentSchoolID == CurrentSchoolID));
            }
            if (CurrentAcademicYearID != 0)
            {
                iqPupil = iqPupil.Where(pp => (pp.CurrentAcademicYearID == CurrentAcademicYearID));
            }*/
            if (AreaID != 0)
            {
                iqPupil = iqPupil.Where(pp => (pp.AreaID == AreaID));
            }
            if (ProvinceID != 0)
            {
                iqPupil = iqPupil.Where(pp => (pp.ProvinceID == ProvinceID));
            }
            if (DistrictID != 0)
            {
                iqPupil = iqPupil.Where(pp => (pp.DistrictID == DistrictID));
            }
            if (CommuneID != 0)
            {
                iqPupil = iqPupil.Where(pp => (pp.CommuneID == CommuneID));
            }
            if (EthnicID != 0)
            {
                iqPupil = iqPupil.Where(pp => (pp.EthnicID == EthnicID));
            }
            if (ReligionID != 0)
            {
                iqPupil = iqPupil.Where(pp => (pp.ReligionID == ReligionID));
            }
            if (PolicyTargetID != 0)
            {
                iqPupil = iqPupil.Where(pp => (pp.PolicyTargetID == PolicyTargetID));
            }
            if (FamilyTypeID != 0)
            {
                iqPupil = iqPupil.Where(pp => (pp.FamilyTypeID == FamilyTypeID));
            }
            if (PriorityTypeID != 0)
            {
                iqPupil = iqPupil.Where(pp => (pp.PriorityTypeID == PriorityTypeID));
            }
            if (PreviousGraduationLevelID != 0)
            {
                iqPupil = iqPupil.Where(pp => (pp.PreviousGraduationLevelID == PreviousGraduationLevelID));
            }
            if (PupilCode.Trim().Length != 0)
            {
                iqPupil = iqPupil.Where(pp => (pp.PupilCode.ToUpper().Contains(PupilCode.ToUpper())));
            }
            if (FullName.Trim().Length != 0)
            {
                iqPupil = iqPupil.Where(pp => (pp.FullName.ToUpper().Contains(FullName.ToUpper())));
            }
            if (Genre != -1)
            {
                iqPupil = iqPupil.Where(pp => (pp.Genre == Genre));
            }
            if (BirthDate.HasValue)
            {
                iqPupil = iqPupil.Where(pp => (pp.BirthDate == BirthDate));
            }
            if (BirthPlace.Trim().Length != 0)
            {
                iqPupil = iqPupil.Where(pp => (pp.BirthPlace.ToUpper().Contains(BirthPlace.ToUpper())));
            }
            if (HomeTown.Trim().Length != 0)
            {
                iqPupil = iqPupil.Where(pp => (pp.HomeTown.ToUpper().Contains(HomeTown.ToUpper())));
            }
            if (Telephone.Trim().Length != 0)
            {
                iqPupil = iqPupil.Where(pp => (pp.Telephone.ToUpper().Contains(Telephone.ToUpper())));
            }
            if (Mobile.Trim().Length != 0)
            {
                iqPupil = iqPupil.Where(pp => (pp.Mobile.ToUpper().Contains(Mobile.ToUpper())));
            }
            if (Email.Trim().Length != 0)
            {
                iqPupil = iqPupil.Where(pp => (pp.Email.ToUpper().Contains(Email.ToUpper())));
            }
            if (TempResidentalAddress.Trim().Length != 0)
            {
                iqPupil = iqPupil.Where(pp => (pp.TempResidentalAddress.ToUpper().Contains(TempResidentalAddress.ToUpper())));
            }
            if (PermanentResidentalAddress.Trim().Length != 0)
            {
                iqPupil = iqPupil.Where(pp => (pp.PermanentResidentalAddress.ToUpper().Contains(PermanentResidentalAddress.ToUpper())));
            }

            //if (IsDisabled != null)
            //{
            //    query = query.Where(pp => (pp.IsDisabled == IsDisabled));

            //}
            if (BloodType != 0)
            {
                iqPupil = iqPupil.Where(pp => (pp.BloodType == BloodType));
            }
            if (EnrolmentMark != 0)
            {
                iqPupil = iqPupil.Where(pp => (pp.EnrolmentMark == (decimal)EnrolmentMark));
            }

            if (ForeignLanguageTraining > 0)
            {
                iqPupil = iqPupil.Where(pp => (pp.ForeignLanguageTraining == ForeignLanguageTraining));
            }

            //if (IsYoungPioneerMember != null)
            //{
            //    query = query.Where(pp => (pp.IsYoungPioneerMember == IsYoungPioneerMember));

            //}
            if (YoungPioneerJoinedDate.HasValue)
            {
                iqPupil = iqPupil.Where(pp => (pp.YoungPioneerJoinedDate == YoungPioneerJoinedDate));
            }
            if (YoungPioneerJoinedPlace.Trim().Length != 0)
            {
                iqPupil = iqPupil.Where(pp => (pp.YoungPioneerJoinedPlace.ToUpper().Contains(YoungPioneerJoinedPlace.ToUpper())));
            }

            //if (IsCommunistPartyMember != null)
            //{
            //    query = query.Where(pp => (pp.IsCommunistPartyMember == IsCommunistPartyMember));

            //}
            if (CommunistPartyJoinedDate.HasValue)
            {
                iqPupil = iqPupil.Where(pp => (pp.CommunistPartyJoinedDate == CommunistPartyJoinedDate));
            }
            if (CommunistPartyJoinedPlace.Trim().Length != 0)
            {
                iqPupil = iqPupil.Where(pp => (pp.CommunistPartyJoinedPlace.ToUpper().Contains(CommunistPartyJoinedPlace.ToUpper())));
            }

            //if ( IsYouthLeageMember!= null)
            //{
            //    query = query.Where(pp => (pp.IsYouthLeageMember == IsYouthLeageMember));
            //}
            if (YoungPioneerJoinedDate.HasValue)
            {
                iqPupil = iqPupil.Where(pp => (pp.YoungPioneerJoinedDate == YoungPioneerJoinedDate));
            }
            if (YoungPioneerJoinedPlace.Trim().Length != 0)
            {
                iqPupil = iqPupil.Where(pp => (pp.YoungPioneerJoinedPlace.ToUpper().Contains(YoungPioneerJoinedPlace.ToUpper())));
            }
            if (FatherFullName.Trim().Length != 0)
            {
                iqPupil = iqPupil.Where(pp => (pp.FatherFullName.ToUpper().Contains(FatherFullName.ToUpper())));
            }
            if (FatherBirthDate.HasValue)
            {
                iqPupil = iqPupil.Where(pp => (pp.FatherBirthDate == FatherBirthDate));
            }
            if (FatherJob.Trim().Length != 0)
            {
                iqPupil = iqPupil.Where(pp => (pp.FatherJob.ToUpper().Contains(FatherJob.ToUpper())));
            }
            if (FatherMobile.Trim().Length != 0)
            {
                iqPupil = iqPupil.Where(pp => (pp.FatherMobile.ToUpper().Contains(FatherMobile.ToUpper())));
            }
            if (MotherFullName.Trim().Length != 0)
            {
                iqPupil = iqPupil.Where(pp => (pp.MotherFullName.ToUpper().Contains(MotherFullName.ToUpper())));
            }
            if (MotherBirthDate.HasValue)
            {
                iqPupil = iqPupil.Where(pp => (pp.MotherBirthDate == MotherBirthDate));
            }
            if (MotherJob.Trim().Length != 0)
            {
                iqPupil = iqPupil.Where(pp => (pp.MotherJob.ToUpper().Contains(MotherJob.ToUpper())));
            }
            if (MotherMobile.Trim().Length != 0)
            {
                iqPupil = iqPupil.Where(pp => (pp.MotherMobile.ToUpper().Contains(MotherMobile.ToUpper())));
            }
            if (SponsorFullName.Trim().Length != 0)
            {
                iqPupil = iqPupil.Where(pp => (pp.SponsorFullName.ToUpper().Contains(SponsorFullName.ToUpper())));
            }
            if (SponsorBirthDate.HasValue)
            {
                iqPupil = iqPupil.Where(pp => (pp.SponsorBirthDate == SponsorBirthDate));
            }
            if (SponsorJob.Trim().Length != 0)
            {
                iqPupil = iqPupil.Where(pp => (pp.SponsorJob.ToUpper().Contains(SponsorJob.ToUpper())));
            }
            if (IsActive != true)
            {
                iqPupil = iqPupil.Where(pp => (pp.IsActive == IsActive));
            }
            if (ModifiedDate.HasValue)
            {
                iqPupil = iqPupil.Where(pp => (pp.ModifiedDate == ModifiedDate));
            }
            /*if (ProfileStatus != 0)
            {
                iqPupil = iqPupil.Where(poc => (poc.ProfileStatus == ProfileStatus));
            }*/
            if (!string.IsNullOrEmpty(strSearch))
            {
                iqPupil = iqPupil.Where(p => p.FullName.ToUpper().Contains(strSearch.ToUpper()) || p.PupilCode.Contains(strSearch));
            }


            var query = from poc in iqPupilOfClass
                        join pos in PupilOfSchoolRepository.All on new { poc.PupilID, poc.SchoolID } equals new { pos.PupilID, pos.SchoolID }
                        join pp in iqPupil on poc.PupilID equals pp.PupilProfileID
                        join cp in iqClass on poc.ClassID equals cp.ClassProfileID
                        where pp.IsActive && cp.IsActive == true && poc.AssignedDate == (from iqPoc in iqPupilOfClass
                                                                                         where iqPoc.ClassID == poc.ClassID
                                                                                             && iqPoc.PupilID == poc.PupilID
                                                                                         select iqPoc.AssignedDate).Max()
                        select new PupilProfileBO
                        {
                            CombineClassCode = cp.CombinedClassCode,
                            PupilProfileID = pp.PupilProfileID,
                            CurrentClassID = poc.ClassID,
                            CurrentClassName = cp.DisplayName,
                            CurrentSchoolID = poc.SchoolID,
                            CurrentAcademicYearID = poc.AcademicYearID,
                            AreaID = pp.AreaID,
                            AreaName = pp.Area.AreaName,
                            ProvinceID = pp.ProvinceID,
                            ProvinceName = pp.Province.ProvinceName,
                            DistrictID = pp.DistrictID,
                            CommuneID = pp.CommuneID,
                            EthnicID = pp.EthnicID,
                            ReligionID = pp.ReligionID,
                            PolicyTargetID = pp.PolicyTargetID,
                            FamilyTypeID = pp.FamilyTypeID,
                            PriorityTypeID = pp.PriorityTypeID,
                            PreviousGraduationLevelID = pp.PreviousGraduationLevelID,
                            PupilCode = pp.PupilCode,
                            FullName = pp.FullName,
                            Genre = pp.Genre,
                            BirthDate = pp.BirthDate,
                            BirthPlace = pp.BirthPlace,
                            HomeTown = pp.HomeTown,
                            HomePlace = pp.HomePlace,
                            Telephone = pp.Telephone,
                            Mobile = pp.Mobile,
                            Email = pp.Email,
                            IdentifyNumber = pp.IdentifyNumber,
                            TempResidentalAddress = pp.TempResidentalAddress,
                            PermanentResidentalAddress = pp.PermanentResidentalAddress,
                            IsDisabled = pp.IsDisabled,
                            BloodType = pp.BloodType,
                            EnrolmentMark = pp.EnrolmentMark,
                            ForeignLanguageTraining = pp.ForeignLanguageTraining,
                            IsYoungPioneerMember = pp.IsYoungPioneerMember,
                            YoungPioneerJoinedDate = pp.YoungPioneerJoinedDate,
                            YoungPioneerJoinedPlace = pp.YoungPioneerJoinedPlace,
                            IsYouthLeageMember = pp.IsYouthLeageMember,
                            YouthLeagueJoinedDate = pp.YouthLeagueJoinedDate,
                            YouthLeagueJoinedPlace = pp.YouthLeagueJoinedPlace,
                            IsCommunistPartyMember = pp.IsCommunistPartyMember,
                            CommunistPartyJoinedDate = pp.CommunistPartyJoinedDate,
                            CommunistPartyJoinedPlace = pp.CommunistPartyJoinedPlace,
                            FatherFullName = pp.FatherFullName,
                            FatherBirthDate = pp.FatherBirthDate,
                            FatherJob = pp.FatherJob,
                            MotherFullName = pp.MotherFullName,
                            FatherMobile = pp.FatherMobile,
                            IsSwimming = pp.IsSwimming,
                            MotherBirthDate = pp.MotherBirthDate,
                            MotherJob = pp.MotherJob,
                            MotherMobile = pp.MotherMobile,
                            SponsorFullName = pp.SponsorFullName,
                            SponsorBirthDate = pp.SponsorBirthDate,
                            SponsorJob = pp.SponsorJob,
                            ProfileStatus = poc.Status,
                            IsActive = pp.IsActive,
                            ModifiedDate = pp.ModifiedDate,
                            EducationLevelID = cp.EducationLevelID,
                            IsResidentIn = pp.IsResidentIn,
                            DisabledTypeID = pp.DisabledTypeID,
                            DisabledSeverity = pp.DisabledSeverity,
                            EnrolmentDate = pos.EnrolmentDate.HasValue ? pos.EnrolmentDate.Value : new DateTime(),
                            SponsorMobile = pp.SponsorMobile,
                            CreatedDate = pp.CreatedDate,
                            EnrolmentType = pos.EnrolmentType,
                            OrderInClass = poc.OrderInClass,
                            Name = pp.Name,
                            WeightAtBirth = pp.WeightAtBirth,
                            HeightAtBirth = pp.HeightAtBirth,
                            FatherEmail = pp.FatherEmail,
                            MotherEmail = pp.MotherEmail,
                            SponsorEmail = pp.SponsorEmail,
                            ChildOrder = pp.ChildOrder,
                            NumberOfChild = pp.NumberOfChild,
                            EatingHabit = pp.EatingHabit,
                            ReactionTrainingHabit = pp.ReactionTrainingHabit,
                            AttitudeInStrangeScene = pp.AttitudeInStrangeScene,
                            OtherHabit = pp.OtherHabit,
                            FavoriteToy = pp.FavoriteToy,
                            EvaluationConfigID = pp.EvaluationConfigID,
                            ClassName = cp.DisplayName,
                            EthnicName = pp.Ethnic.EthnicName,
                            EthnicCode = pp.Ethnic.EthnicCode,
                            PolicyTargetName = pp.PolicyTarget.Resolution,
                            ReligionName = pp.Religion.Resolution,
                            ReligionCode = pp.Religion.Code,
                            SchoolName = pp.SchoolProfile.SchoolName,
                            DistrictName = pp.District.DistrictName,
                            CommuneName = pp.Commune.CommuneName,
                            PolicyRegimeID = pp.PolicyRegimeID,
                            PriorityTypeName = pp.PriorityType.Resolution,
                            FamilyName = pp.FamilyType.Resolution,
                            DisabledTypeName = pp.DisabledType.Resolution,
                            IsSupportForLearning = pp.IsSupportForLearning.HasValue ? pp.IsSupportForLearning.Value : false,
                            AssignedDate = poc.AssignedDate,
                            IsSponsorSMS = pp.IsSponsorSMS,
                            IsFatherSMS = pp.IsFatherSMS,
                            IsMotherSMS = pp.IsMotherSMS,
                            Image = pp.Image,
                            LanguageCertificate = pp.ForeignLanguageGrade.Resolution,
                            ItCertificate = pp.ITQualificationLevel.Resolution,
                            PupilLearningType = pp.PupilLearningType,
                            SubCommitteeID = cp.SubCommitteeID,
                            SubCommitteeName = cp.SubCommittee.Resolution,
                            LanguageCertificateID = pp.LanguageCertificateID,
                            ItCertificateID = pp.ItCertificateID,

                            CommnuneCode = pp.Commune.CommuneCode,
                            ProvinceCode = pp.Province.ProvinceCode,
                            DistrictCode = pp.District.DistrictCode,

                            PolicyTargetCode = pp.PolicyTarget.Code,

                            VillageID = pp.VillageID,
                            VillageName = pp.Village.VillageName,
                            VillageCode = pp.Village.VillageCode,
                            StorageNumber = pp.StorageNumber,
                            ClassOrderNumber = cp.OrderNumber,
                            FavouriteTypeOfDish = pp.FavouriteTypeOfDish,
                            HateTypeOfDish = pp.HateTypeOfDish,
                            SupportingPolicy = pp.SupportingPolicy,
                            IsReceiveRiceSubsidy = pp.IsReceiveRiceSubsidy,
                            IsResettlementTarget = pp.IsResettlementTarget,
                            ClassType = pp.ClassType,
                            IsUsedMoetProgram = pp.UsedMoetProgram,
                            IsMinorityFather = pp.MinorityFather,
                            IsMinorityMother = pp.MinorityMother,
                            MinorityFather = pp.MinorityFather,
                            MinorityMother = pp.MinorityMother,
                            UsedMoetProgram = pp.UsedMoetProgram,
                            OtherEthnicID = pp.OtherEthnicID
                        };


            return query;
        }

        #endregion Search

        #region SearchBySchool

        /// <summary>
        /// Lấy ra thông tin từ mã trường
        /// </summary>
        /// <author>tungnd</author>
        /// <date>9/10/2012</date>
        /// <param name="ethnic">Đối tượng PupilProfile</param>
        /// <returns>Đối tượng PupilProfile</returns>

        public IQueryable<PupilProfileBO> SearchBySchool(int SchoolID, IDictionary<string, object> SearchInfo)
        {
            if (SchoolID == 0)
            {
                return null;
            }
            else
            {
                SearchInfo["CurrentSchoolID"] = SchoolID;

                return Search(SearchInfo);
            }
        }

        // <author>phuongh1</author>
        /// <date>07/08/2013</date>
        public IQueryable<PupilProfile> SearchBySchool(int SchoolID, PupilProfileSearchForm SearchForm = null)
        {
            if (SchoolID == 0)
            {
                return null;
            }
            else
            {
                if (SearchForm == null) SearchForm = new PupilProfileSearchForm();
                SearchForm.CurrentSchoolID = SchoolID;

                return SearchForm.Search(PupilProfileRepository.All);
            }
        }

        #endregion SearchBySchool

        #region get list policy target
        /// <summary>
        /// Lấy ra danh sách học sinh thuộc diện chính sách
        /// Author: Tamhm1
        /// Date: 21/11/2012
        /// </summary>
        /// <param name="AcademicYearID"></param>
        /// <param name="SchoolID"></param>
        /// <returns></returns>
        public IQueryable<PupilProfile> GetListPupilPolicyTarget(int AcademicYearID, int SchoolID)
        {
            if (AcademicYearID == 0) return null;
            if (SchoolID == 0) return null;
            IQueryable<PupilProfile> listPupilProfile = from p in PupilProfileRepository.All

                                                        where p.CurrentAcademicYearID == AcademicYearID
                                                        && p.CurrentSchoolID == SchoolID
                                                        && p.IsActive == true
                                                        && (p.PolicyTargetID != 0 || p.PriorityTypeID != 0)

                                                        select p;
            return listPupilProfile;
        }

        /// <summary>
        /// Tìm kiếm học sinh trên giao diện Phòng/Sở
        /// Author: DungVA
        /// Date: 17/12/2012
        /// </summary>
        /// <param name="ProvinceID"></param>
        /// <param name="DistrictID"></param>
        /// <param name="SupervisingDeptID"></param>
        /// <param name="SchoolID"></param>
        /// <param name="PupilCode"></param>
        /// <param name="FullName"></param>
        /// <param name="Genre"></param>
        /// <param name="Status"></param>
        /// <param name="Year"></param>
        /// <param name="EducationLevelID"></param>
        /// <param name="ClassID"></param>
        /// <param name="HierachyLevel"></param>
        /// <returns> Đối tượng PupilProfileBO</returns>
        public IQueryable<PupilProfileBO> SearchPupilBySupervisingDept(Dictionary<string, object> dic)
        {
            int ProvinceID = Utils.GetInt(dic, "ProvinceID");
            int AppliedLevel = Utils.GetInt(dic, "AppliedLevel");
            int DistrictID = Utils.GetInt(dic, "DistrictID");
            int SupervisingDeptID = Utils.GetInt(dic, "SupervisingDeptID");
            int SchoolID = Utils.GetInt(dic, "SchoolID");
            string PupilCode = Utils.GetString(dic, "PupilCode");
            string FullName = Utils.GetString(dic, "FullName");
            int Genre = Utils.GetInt(dic, "Genre");
            int Status = Utils.GetInt(dic, "Status");
            int Year = Utils.GetInt(dic, "Year");
            int EducationLevelID = Utils.GetInt(dic, "EducationLevelID");
            int ClassID = Utils.GetInt(dic, "ClassID");
            int HierachyLevel = Utils.GetInt(dic, "HierachyLevel");
            bool isSub = Utils.GetBool(dic, "IsSub");
            //Nếu HierachyLevel = 3, ProvinceID = 0 thì trả về null
            if ((HierachyLevel == GlobalConstants.EDUCATION_HIERACHY_LEVEL_PROVINCE_OFFICE) && (ProvinceID == 0))
            {
                return null;
            }

            //Nếu HierachyLevel = 5, SupervisingDeptID = 0 thì trả về null
            if ((HierachyLevel == GlobalConstants.EDUCATION_HIERACHY_LEVEL_DISTRICT_OFFICE) && (SupervisingDeptID == 0))
            {
                return null;
            }
            //Neu HierachyLevel = 3 thi tim kiem theo ProvinceID, ko tim theo SupervisingDeptID
            if (HierachyLevel == GlobalConstants.EDUCATION_HIERACHY_LEVEL_PROVINCE_OFFICE)
            {
                SupervisingDeptID = 0;
            }
            var query = from poc in PupilOfClassRepository.All
                        join cp in ClassProfileRepository.All.Where(o => o.IsActive == true && o.EducationLevel.Grade == AppliedLevel || AppliedLevel == 0) on poc.ClassID equals cp.ClassProfileID
                        join sp in SchoolProfileRepository.All.Where(o => o.ProvinceID == ProvinceID) on poc.SchoolID equals sp.SchoolProfileID
                        join pp in PupilProfileRepository.All on poc.PupilID equals pp.PupilProfileID

                        select new PupilProfileBO
                        {
                            PupilProfileID = pp.PupilProfileID,
                            CurrentClassID = poc.ClassID,
                            CurrentClassName = cp.DisplayName,
                            CurrentSchoolID = poc.SchoolID,
                            CurrentAcademicYearID = poc.AcademicYearID,
                            AreaID = sp.AreaID,
                            AreaName = sp.Area.AreaName,
                            ProvinceID = sp.ProvinceID.Value,
                            ProvinceName = sp.Province.ProvinceName,
                            DistrictID = sp.DistrictID,
                            CommuneID = sp.CommuneID,
                            EthnicID = pp.EthnicID,
                            ReligionID = pp.ReligionID,
                            PolicyTargetID = pp.PolicyTargetID,
                            FamilyTypeID = pp.FamilyTypeID,
                            PriorityTypeID = pp.PriorityTypeID,
                            PreviousGraduationLevelID = pp.PreviousGraduationLevelID,
                            PupilCode = pp.PupilCode,
                            FullName = pp.FullName,
                            Genre = pp.Genre,
                            BirthDate = pp.BirthDate,
                            BirthPlace = pp.BirthPlace,
                            HomeTown = pp.HomeTown,
                            Telephone = pp.Telephone,
                            Mobile = pp.Mobile,
                            Email = pp.Email,
                            TempResidentalAddress = pp.TempResidentalAddress,
                            PermanentResidentalAddress = pp.PermanentResidentalAddress,
                            IsDisabled = pp.IsDisabled,
                            BloodType = pp.BloodType,
                            EnrolmentMark = pp.EnrolmentMark,
                            ForeignLanguageTraining = pp.ForeignLanguageTraining,
                            IsYoungPioneerMember = pp.IsYoungPioneerMember,
                            YoungPioneerJoinedDate = pp.YoungPioneerJoinedDate,
                            YoungPioneerJoinedPlace = pp.YoungPioneerJoinedPlace,
                            IsYouthLeageMember = pp.IsYouthLeageMember,
                            YouthLeagueJoinedDate = pp.YouthLeagueJoinedDate,
                            YouthLeagueJoinedPlace = pp.YouthLeagueJoinedPlace,
                            IsCommunistPartyMember = pp.IsCommunistPartyMember,
                            CommunistPartyJoinedDate = pp.CommunistPartyJoinedDate,
                            CommunistPartyJoinedPlace = pp.CommunistPartyJoinedPlace,
                            FatherFullName = pp.FatherFullName,
                            FatherBirthDate = pp.FatherBirthDate,
                            FatherJob = pp.FatherJob,
                            MotherFullName = pp.MotherFullName,
                            FatherMobile = pp.FatherMobile,
                            MotherBirthDate = pp.MotherBirthDate,
                            MotherJob = pp.MotherJob,
                            MotherMobile = pp.MotherMobile,
                            SponsorFullName = pp.SponsorFullName,
                            SponsorBirthDate = pp.SponsorBirthDate,
                            SponsorJob = pp.SponsorJob,
                            ProfileStatus = poc.Status,
                            IsActive = pp.IsActive,
                            ModifiedDate = pp.ModifiedDate,
                            EducationLevelID = cp.EducationLevelID,
                            IsResidentIn = pp.IsResidentIn,
                            DisabledTypeID = pp.DisabledTypeID,
                            DisabledSeverity = pp.DisabledSeverity,
                            EnrolmentDate = pp.EnrolmentDate,
                            SponsorMobile = pp.SponsorMobile,
                            CreatedDate = pp.CreatedDate,
                            OrderInClass = poc.OrderInClass,
                            Name = pp.Name,
                            WeightAtBirth = pp.WeightAtBirth,
                            HeightAtBirth = pp.HeightAtBirth,
                            FatherEmail = pp.FatherEmail,
                            MotherEmail = pp.MotherEmail,
                            SponsorEmail = pp.SponsorEmail,
                            ChildOrder = pp.ChildOrder,
                            NumberOfChild = pp.NumberOfChild,
                            EatingHabit = pp.EatingHabit,
                            ReactionTrainingHabit = pp.ReactionTrainingHabit,
                            AttitudeInStrangeScene = pp.AttitudeInStrangeScene,
                            OtherHabit = pp.OtherHabit,
                            FavoriteToy = pp.FavoriteToy,
                            EvaluationConfigID = pp.EvaluationConfigID,
                            ClassName = cp.DisplayName,
                            EthnicName = pp.Ethnic.EthnicName,
                            PolicyTargetName = pp.PolicyTarget.Resolution,
                            ReligionName = pp.Religion.Resolution,
                            SchoolName = sp.SchoolName,
                            DistrictName = pp.District.DistrictName,
                            CommuneName = pp.Commune.CommuneName,
                            PolicyRegimeID = pp.PolicyRegimeID,
                            PolicyRegimeName = pp.PolicyRegimeID.HasValue ? pp.PolicyRegime.Resolution : "",
                            PriorityTypeName = pp.PriorityType.Resolution,
                            FamilyName = pp.FamilyType.Resolution,
                            DisabledTypeName = pp.DisabledType.Resolution,
                            IsSupportForLearning = pp.IsSupportForLearning.HasValue ? pp.IsSupportForLearning.Value : false,
                            AssignedDate = poc.AssignedDate,
                            SupervisingDeptID = sp.SupervisingDeptID,
                            Year = poc.Year,
                            EducationLevel = cp.EducationLevel.Resolution,
                            IsSponsorSMS = pp.IsSponsorSMS,
                            IsFatherSMS = pp.IsFatherSMS,
                            IsMotherSMS = pp.IsMotherSMS,
                            ClassOrderNumber = cp.OrderNumber,
                            ParentID = sp.SupervisingDept.ParentID,
                            IsMinorityMother = pp.MinorityMother,
                            IsMinorityFather = pp.MinorityFather,
                            UsedMoetProgram = pp.UsedMoetProgram,
                            VillageName = pp.VillageID.HasValue ? pp.Village.VillageName : "",
                            
                        };
            //if (ProvinceID != 0)
            //{
            //    query = query.Where(o => (o.ProvinceID == ProvinceID));
            //}
            if (DistrictID != 0 && isSub == true)
            {
                query = query.Where(o => (o.DistrictID == DistrictID));
            }
            SupervisingDept su = SupervisingDeptBusiness.Find(SupervisingDeptID);
            if (su != null && (su.HierachyLevel == SystemParamsInFile.EDUCATION_HIERACHY_LEVEL_PROVINCE_OFFICE_DEPARTMENT || su.HierachyLevel == SystemParamsInFile.EDUCATION_HIERACHY_LEVEL_DISTRICT_OFFICE_DEPARTMENT))
            {
                SupervisingDeptID = su.ParentID.Value;
            }
            if (SupervisingDeptID != 0)
            {
                query = query.Where(o => (o.SupervisingDeptID == SupervisingDeptID || o.ParentID == SupervisingDeptID));
            }
            if (SchoolID != 0)
            {
                query = query.Where(o => (o.CurrentSchoolID == SchoolID));
            }
            if (Year != 0)
            {
                query = query.Where(o => (o.Year == Year));
            }
            if (EducationLevelID != 0)
            {
                query = query.Where(o => (o.EducationLevelID == EducationLevelID));
            }
            if (ClassID != 0)
            {
                query = query.Where(o => (o.CurrentClassID == ClassID));
            }
            if (PupilCode != null && PupilCode != "")
            {
                query = query.Where(o => (o.PupilCode.Contains(PupilCode)));
            }
            if (FullName != null && FullName != "")
            {
                query = query.Where(o => (o.FullName.Contains(FullName)));
            }
            if (Genre != -1)
            {
                query = query.Where(o => (o.Genre == Genre));
            }
            query = query.Where(o => o.IsActive == true);

            var returnValue = query.Where(u => u.AssignedDate == query.Where(v => v.PupilProfileID == u.PupilProfileID && v.CurrentClassID == u.CurrentClassID).Max(v => v.AssignedDate));

            if (Status != 0)
            {
                returnValue = returnValue.Where(poc => (poc.ProfileStatus == Status));
            }
            return returnValue;
        }

        public IQueryable<PupilProfileBO> SearchPupilBySupervisingDept2(Dictionary<string, object> dic)
        {
            int ProvinceID = Utils.GetInt(dic, "ProvinceID");
            int AppliedLevel = Utils.GetInt(dic, "AppliedLevel");
            int DistrictID = Utils.GetInt(dic, "DistrictID");
            int SupervisingDeptID = Utils.GetInt(dic, "SupervisingDeptID");
            int SchoolID = Utils.GetInt(dic, "SchoolID");
            string PupilCode = Utils.GetString(dic, "PupilCode");
            string FullName = Utils.GetString(dic, "FullName");
            int Genre = Utils.GetInt(dic, "Genre");
            int Status = Utils.GetInt(dic, "Status");
            int Year = Utils.GetInt(dic, "Year");
            int EducationLevelID = Utils.GetInt(dic, "EducationLevelID");
            int ClassID = Utils.GetInt(dic, "ClassID");
            int HierachyLevel = Utils.GetInt(dic, "HierachyLevel");
            bool isSub = Utils.GetBool(dic, "IsSub");
            //Nếu HierachyLevel = 3, ProvinceID = 0 thì trả về null
            if ((HierachyLevel == GlobalConstants.EDUCATION_HIERACHY_LEVEL_PROVINCE_OFFICE) && (ProvinceID == 0))
            {
                return null;
            }

            //Nếu HierachyLevel = 5, SupervisingDeptID = 0 thì trả về null
            if ((HierachyLevel == GlobalConstants.EDUCATION_HIERACHY_LEVEL_DISTRICT_OFFICE) && (SupervisingDeptID == 0))
            {
                return null;
            }
            //Neu HierachyLevel = 3 thi tim kiem theo ProvinceID, ko tim theo SupervisingDeptID
            if (HierachyLevel == GlobalConstants.EDUCATION_HIERACHY_LEVEL_PROVINCE_OFFICE)
            {
                SupervisingDeptID = 0;
            }
            var query = from poc in PupilOfClassRepository.All
                        join cp in ClassProfileRepository.All.Where(o => o.IsActive == true && o.EducationLevel.Grade == AppliedLevel || AppliedLevel == 0) on poc.ClassID equals cp.ClassProfileID
                        join sp in SchoolProfileRepository.All.Where(o => o.ProvinceID == ProvinceID) on poc.SchoolID equals sp.SchoolProfileID
                        join pp in PupilProfileRepository.All on poc.PupilID equals pp.PupilProfileID
                        join pos in PupilOfSchoolRepository.All on pp.PupilProfileID equals pos.PupilID
                        select new PupilProfileBO
                        {
                            PupilProfileID = pp.PupilProfileID,
                            PupilCode = pp.PupilCode,
                            FullName = pp.FullName,
                            CurrentSchoolID = poc.SchoolID,
                            SchoolName = sp.SchoolName,
                            SyncCodeSchool = sp.SyncCode,
                            EducationLevelID = cp.EducationLevelID,
                            EducationLevel = cp.EducationLevel.Resolution,
                            CurrentClassID = poc.ClassID,
                            CurrentClassName = cp.DisplayName,
                            ClassName = cp.DisplayName,
                            BirthDate = pp.BirthDate,
                            Genre = pp.Genre,
                            ProfileStatus = poc.Status,
                            StorageNumber = pp.StorageNumber,
                            EthnicID = pp.EthnicID,
                            EthnicName = pp.Ethnic.EthnicName,
                            OtherEthnicID = pp.OtherEthnicID,
                            ProvinceID = sp.ProvinceID.Value,
                            ProvinceName = sp.Province.ProvinceName,
                            DistrictID = sp.DistrictID,
                            DistrictName = pp.District.DistrictName,
                            CommuneID = sp.CommuneID,
                            CommuneName = pp.Commune.CommuneName,
                            BirthPlace = pp.BirthPlace,
                            VillageID = pp.VillageID,
                            VillageName = pp.Village.VillageName,
                            HomeTown = pp.HomeTown,
                            TempResidentalAddress = pp.TempResidentalAddress,
                            PermanentResidentalAddress = pp.PermanentResidentalAddress,
                            EnrolmentType = pos.EnrolmentType,
                            EnrolmentDate = pp.EnrolmentDate,
                            PreviousGraduationLevelID = pp.PreviousGraduationLevelID,
                            ClassType = pp.ClassType,
                            LanguageCertificate = pp.LanguageCertificate,
                            LanguageCertificateID = pp.LanguageCertificateID,
                            ItCertificate = pp.ItCertificate,
                            ItCertificateID = pp.ItCertificateID,
                            ForeignLanguageTraining = pp.ForeignLanguageTraining,

                            Telephone = pp.Telephone,
                            Mobile = pp.Mobile,
                            AreaID = sp.AreaID,
                            AreaName = sp.Area.AreaName,
                            IsDisabled = pp.IsDisabled,
                            DisabledTypeID = pp.DisabledTypeID,
                            DisabledTypeName = pp.DisabledType.Resolution,
                            DisabledSeverity = pp.DisabledSeverity,
                            PriorityTypeID = pp.PriorityTypeID,
                            PriorityTypeName = pp.PriorityType.Resolution,
                            PolicyTargetID = pp.PolicyTargetID,
                            PolicyTargetName = pp.PolicyTarget.Resolution,
                            PolicyRegimeID = pp.PolicyRegimeID,
                            PolicyRegimeName = pp.PolicyRegime.Resolution,
                            _IsSupportForLearning = pp.IsSupportForLearning,
                            IsReceiveRiceSubsidy = pp.IsReceiveRiceSubsidy,
                            IsResettlementTarget = pp.IsResettlementTarget,
                            IdentifyNumber = pp.IdentifyNumber,
                            BloodType = pp.BloodType,
                            WeightAtBirth = pp.WeightAtBirth,
                            HeightAtBirth = pp.HeightAtBirth,
                            ChildOrder = pp.ChildOrder,
                            IsSwimming = pp.IsSwimming,
                            UsedMoetProgram = pp.UsedMoetProgram,
                            IsUsedMoetProgram = pp.UsedMoetProgram,
                            IsYoungPioneerMember = pp.IsYoungPioneerMember,
                            YoungPioneerJoinedDate = pp.YoungPioneerJoinedDate,
                            YoungPioneerJoinedPlace = pp.YoungPioneerJoinedPlace,
                            IsYouthLeageMember = pp.IsYouthLeageMember,
                            YouthLeagueJoinedDate = pp.YouthLeagueJoinedDate,
                            YouthLeagueJoinedPlace = pp.YouthLeagueJoinedPlace,
                            IsCommunistPartyMember = pp.IsCommunistPartyMember,
                            CommunistPartyJoinedDate = pp.CommunistPartyJoinedDate,
                            CommunistPartyJoinedPlace = pp.CommunistPartyJoinedPlace,
                            IsResidentIn = pp.IsResidentIn,


                            FamilyTypeID = pp.FamilyTypeID,
                            FamilyName = pp.FamilyType.Resolution,
                            MinorityFather = pp.MinorityFather,
                            MinorityMother = pp.MinorityMother,
                            IsMinorityMother = pp.MinorityMother,
                            IsMinorityFather = pp.MinorityFather,
                            FatherFullName = pp.FatherFullName,
                            FatherBirthDate = pp.FatherBirthDate,
                            FatherJob = pp.FatherJob,
                            FatherMobile = pp.FatherMobile,
                            FatherEmail = pp.FatherEmail,
                            MotherFullName = pp.MotherFullName,
                            MotherBirthDate = pp.MotherBirthDate,
                            MotherJob = pp.MotherJob,
                            MotherMobile = pp.MotherMobile,
                            MotherEmail = pp.MotherEmail,
                            SponsorFullName = pp.SponsorFullName,
                            SponsorBirthDate = pp.SponsorBirthDate,
                            SponsorJob = pp.SponsorJob,
                            SponsorMobile = pp.SponsorMobile,
                            SponsorEmail = pp.SponsorEmail,

                            EatingHabit = pp.EatingHabit,                            
                            OtherHabit = pp.OtherHabit,
                            CurrentAcademicYearID = poc.AcademicYearID,                        
                            ReligionID = pp.ReligionID,
                            ReligionName = pp.Religion.Resolution,
                            Email = pp.Email,
                            EnrolmentMark = pp.EnrolmentMark,                          
                            IsActive = pp.IsActive,
                            ModifiedDate = pp.ModifiedDate,                          
                            CreatedDate = pp.CreatedDate,
                            OrderInClass = poc.OrderInClass,
                            Name = pp.Name,                            
                            NumberOfChild = pp.NumberOfChild, 
                            ReactionTrainingHabit = pp.ReactionTrainingHabit,
                            AttitudeInStrangeScene = pp.AttitudeInStrangeScene,                          
                            FavoriteToy = pp.FavoriteToy,
                            EvaluationConfigID = pp.EvaluationConfigID,
                            AssignedDate = poc.AssignedDate,
                            SupervisingDeptID = sp.SupervisingDeptID,
                            Year = poc.Year,                         
                            IsSponsorSMS = pp.IsSponsorSMS,
                            IsFatherSMS = pp.IsFatherSMS,
                            IsMotherSMS = pp.IsMotherSMS,
                            ClassOrderNumber = cp.OrderNumber,
                            ParentID = sp.SupervisingDept.ParentID,                        
                            HomePlace= pp.HomePlace,
                            FavouriteTypeOfDish = pp.FavouriteTypeOfDish,
                            HateTypeOfDish = pp.HateTypeOfDish,
                            ApprenticeshipGroupID = cp.ApprenticeshipGroupID
                        };
            //if (ProvinceID != 0)
            //{
            //    query = query.Where(o => (o.ProvinceID == ProvinceID));
            //}
            if (DistrictID != 0 && isSub == true)
            {
                query = query.Where(o => (o.DistrictID == DistrictID));
            }
            SupervisingDept su = SupervisingDeptBusiness.Find(SupervisingDeptID);
            if (su != null && (su.HierachyLevel == SystemParamsInFile.EDUCATION_HIERACHY_LEVEL_PROVINCE_OFFICE_DEPARTMENT || su.HierachyLevel == SystemParamsInFile.EDUCATION_HIERACHY_LEVEL_DISTRICT_OFFICE_DEPARTMENT))
            {
                SupervisingDeptID = su.ParentID.Value;
            }
            if (SupervisingDeptID != 0)
            {
                query = query.Where(o => (o.SupervisingDeptID == SupervisingDeptID || o.ParentID == SupervisingDeptID));
            }
            if (SchoolID != 0)
            {
                query = query.Where(o => (o.CurrentSchoolID == SchoolID));
            }
            if (Year != 0)
            {
                query = query.Where(o => (o.Year == Year));
            }
            if (EducationLevelID != 0)
            {
                query = query.Where(o => (o.EducationLevelID == EducationLevelID));
            }
            if (ClassID != 0)
            {
                query = query.Where(o => (o.CurrentClassID == ClassID));
            }
            if (PupilCode != null && PupilCode != "")
            {
                query = query.Where(o => (o.PupilCode.Contains(PupilCode)));
            }
            if (FullName != null && FullName != "")
            {
                query = query.Where(o => (o.FullName.Contains(FullName)));
            }
            if (Genre != -1)
            {
                query = query.Where(o => (o.Genre == Genre));
            }
            query = query.Where(o => o.IsActive == true);

            var returnValue = query.Where(u => u.AssignedDate == query.Where(v => v.PupilProfileID == u.PupilProfileID && v.CurrentClassID == u.CurrentClassID).Max(v => v.AssignedDate));

            if (Status != 0)
            {
                returnValue = returnValue.Where(poc => (poc.ProfileStatus == Status));
            }
            return returnValue;
        }
        #endregion

        #region Count for Report/Statistics
        /// <summary>
        /// Dem so hoc sinh dang hoc hoac da tot nghiep trong truong
        /// </summary>
        /// <param name="AcademicYearID"></param>
        /// <param name="SchoolID"></param>
        /// <returns></returns>
        public List<PupilProfile> CountPupilOfClass(int AcademicYearID, int SchoolID)
        {
            IDictionary<string, object> dicPupil = new Dictionary<string, object>();
            dicPupil["AcademicYearID"] = AcademicYearID;
            dicPupil["SchoolID"] = SchoolID;

            dicPupil["Check"] = "check";
            List<PupilProfile> lstPupilOfClass = PupilOfClassBusiness.SearchBySchool(SchoolID, dicPupil).Where(o => o.Status == SystemParamsInFile.PUPIL_STATUS_STUDYING || o.Status == SystemParamsInFile.PUPIL_STATUS_GRADUATED).Select(o => o.PupilProfile).ToList();
            return lstPupilOfClass;
        }

        /// <summary>
        /// Dem so hoc sinh NAM dang hoc hoac da tot nghiep
        /// </summary>
        /// <param name="AcademicYearID"></param>
        /// <param name="SchoolID"></param>
        /// <returns></returns>
        public List<PupilProfile> CountPupilMaleOfClass(int AcademicYearID, int SchoolID)
        {
            IDictionary<string, object> dicPupil = new Dictionary<string, object>();
            dicPupil["AcademicYearID"] = AcademicYearID;
            dicPupil["SchoolID"] = SchoolID;

            dicPupil["Check"] = "check";
            List<PupilProfile> lstPupilOfClass = PupilOfClassBusiness.SearchBySchool(SchoolID, dicPupil).Where(o => (o.Status == SystemParamsInFile.PUPIL_STATUS_STUDYING || o.Status == SystemParamsInFile.PUPIL_STATUS_GRADUATED) && o.PupilProfile.Genre == SystemParamsInFile.GENRE_MALE).Select(o => o.PupilProfile).ToList();
            return lstPupilOfClass;
        }

        /// <summary>
        /// Dem so hoc sinh NU dang hoc hoac da tot nghiep
        /// </summary>
        /// <param name="AcademicYearID"></param>
        /// <param name="SchoolID"></param>
        /// <returns></returns>
        public List<PupilProfile> CountPupilFemaleOfClass(int AcademicYearID, int SchoolID)
        {
            IDictionary<string, object> dicPupil = new Dictionary<string, object>();
            dicPupil["AcademicYearID"] = AcademicYearID;
            dicPupil["SchoolID"] = SchoolID;

            dicPupil["Check"] = "check";
            List<PupilProfile> lstPupilOfClass = PupilOfClassBusiness.SearchBySchool(SchoolID, dicPupil).Where(o => (o.Status == SystemParamsInFile.PUPIL_STATUS_STUDYING || o.Status == SystemParamsInFile.PUPIL_STATUS_GRADUATED) && o.PupilProfile.Genre == SystemParamsInFile.GENRE_FEMALE).Select(o => o.PupilProfile).ToList();
            return lstPupilOfClass;
        }
        #endregion

        #region ValidateAll
        //dung trong chuc nang import danh sach hoc sinh
        //Cac phan tu trong list co cung AcademicYearID, SchoolID nen validate 1 phan tu.
        //Neu insert thi chi co Province nen khong can validate
        private void ValidateAll(List<PupilProfile> lstPupilprofile, bool isUpdate = false)
        {
            //Lay phan tu dau tien
            PupilProfile pp = lstPupilprofile[0];
            //  CurrentSchoolID và CurrentAcademicYearID not compatible trong AcademicYear
            IDictionary<string, object> SearchInfoAcademic = new Dictionary<string, object>();
            SearchInfoAcademic["SchoolID"] = pp.CurrentSchoolID;
            SearchInfoAcademic["AcademicYearID"] = pp.CurrentAcademicYearID;

            bool SearchInfoAcademicExist = new AcademicYearRepository(this.context).ExistsRow(GlobalConstants.SCHOOL_SCHEMA, "AcademicYear", SearchInfoAcademic, null);

            if (!SearchInfoAcademicExist)
            {
                throw new BusinessException("Common_Validate_NotCompatible");
            }
            Dictionary<string, object> dic = new Dictionary<string, object>();
            dic["AcademicYearID"] = pp.CurrentAcademicYearID;

            List<int> lstClassIDInAcademicYear = ClassProfileBusiness.SearchBySchool(pp.CurrentSchoolID, dic).Select(o => o.ClassProfileID).ToList();
            List<PupilProfile> checkClass = lstPupilprofile.Where(o => !lstClassIDInAcademicYear.Contains(o.CurrentClassID)).ToList();
            if (checkClass != null && checkClass.Count() > 0)
            {
                throw new BusinessException("Common_Validate_NotCompatible");
            }
            List<string> lstPupilCode = lstPupilprofile.Select(o => o.PupilCode).ToList();
            var checkCode = PupilProfileBusiness.SearchBySchool(pp.CurrentSchoolID, new Dictionary<string, object>()).Where(o => lstPupilCode.Contains(o.PupilCode));
            if (checkCode != null && checkCode.Count() > 0 && isUpdate == false)
            {
                List<object> listParams = new List<object>();
                listParams.Add("PupilProfile_Label_PupilCode");
                throw new BusinessException("Common_Validate_Duplicate", listParams);
            }
            AcademicYear academicYear = AcademicYearBusiness.Find(pp.CurrentAcademicYearID);
            //  DistrictID và ProvinceID not compatible trong District
            ////if (isUpdate)
            ////{
            ////    var dicProvince = lstPupilprofile.Where(o => o.DistrictID != null).Distinct().Select(o => new { ProvinceID = o.ProvinceID, DistrictID = o.DistrictID });
            ////    IDictionary<string, object> SearchInfoDistrict = new Dictionary<string, object>();
            ////}


            //if (isUpdate)
            //{
            //    //  DistrictID và ProvinceID not compatible trong District
            //    IDictionary<string, object> SearchInfoDistrict = new Dictionary<string, object>();
            //    SearchInfoDistrict["DistrictID"] = pupilprofile.DistrictID;
            //    SearchInfoDistrict["ProvinceID"] = pupilprofile.ProvinceID;

            //    bool SearchInfoDistrictExist = new AcademicYearRepository(this.context).ExistsRow(GlobalConstants.LIST_SCHEMA, "District", SearchInfoDistrict, null);

            //    if (!SearchInfoDistrictExist)
            //    {
            //        throw new BusinessException("Common_Validate_NotCompatible");
            //    }
            //}

            foreach (PupilProfile pupilprofile in lstPupilprofile)
            {
                ValidationMetadata.ValidateObject(pupilprofile);

                //Validate Email
                //Utils.ValidateEmail(pupilprofile.FatherEmail, "PupilProfile_Label_FatherEmail");
                if (!Utils.ValidateEmail(pupilprofile.FatherEmail))
                {
                    throw new BusinessException("Email cha không đúng định dạng");
                }
                //Utils.ValidateEmail(pupilprofile.MotherEmail, "PupilProfile_Label_MotherEmail");
                if (!Utils.ValidateEmail(pupilprofile.MotherEmail))
                {
                    throw new BusinessException("Email mẹ không đúng định dạng");
                }
                //Utils.ValidateEmail(pupilprofile.SponsorEmail, "PupilProfile_Label_SponsorEmail");
                if (!Utils.ValidateEmail(pupilprofile.MotherEmail))
                {
                    throw new BusinessException("Email người bảo hộ không đúng định dạng");
                }

                //Số con thứ không được lơn hơn tổng số con
                if (pupilprofile.ChildOrder > pupilprofile.NumberOfChild)
                {
                    throw new BusinessException("Common_Validate_ChildOrder_NumberOfChild");
                }
                //BirthDate < DateTime.Now            
                //Utils.ValidatePastDateNow(pupilprofile.BirthDate, "PupilProfile_Label_BirthDate");
                if (Utils.CompareDateWithDateNow(pupilprofile.BirthDate))
                {
                    throw new BusinessException("Ngày sinh không được lớn hơn ngày hiện tại");
                }
                //Utils.ValidatePastYearNow(pupilprofile.FatherBirthDate, "PupilProfile_Label_FatherBirthdateName");
                if (Utils.CompareYearWithYearNow(pupilprofile.FatherBirthDate))
                {
                    throw new BusinessException("Năm sinh của cha không được lớn hơn năm hiện tại");
                }
                //Utils.ValidatePastYearNow(pupilprofile.MotherBirthDate, "PupilProfile_Label_MotherBirthdateName");
                if (Utils.CompareYearWithYearNow(pupilprofile.MotherBirthDate))
                {
                    throw new BusinessException("Năm sinh của mẹ không được lớn hơn năm hiện tại");
                }
                //Utils.ValidatePastYearNow(pupilprofile.SponsorBirthDate, "PupilProfile_Label_SponsorBirthDateName");
                if (Utils.CompareYearWithYearNow(pupilprofile.SponsorBirthDate))
                {
                    throw new BusinessException("Năm sinh người bảo hộ không được lớn hơn năm hiện tại");
                }

                //Utils.ValidateBeforeDate(pupilprofile.BirthDate, new DateTime(1900, 1, 1), "PupilProfile_Label_BirthDate", "Common_Min_BirthYear");
                if (!Utils.CompareTwoDate(pupilprofile.BirthDate, new DateTime(1900, 1, 1)))
                {
                    throw new BusinessException("Ngáy sinh phải lớn hơn 1900");
                }
                //Utils.ValidateBeforeDate(pupilprofile.EnrolmentDate, new DateTime(1900, 1, 1), "PupilProfile_Label_EnrolmentDate", "Common_Min_BirthYear");
                if (!Utils.CompareTwoDate(pupilprofile.BirthDate, new DateTime(1900, 1, 1)))
                {
                    throw new BusinessException("Ngày vào trường phải lớn hơn 1900");
                }
                //Utils.ValidateBeforeDate(pupilprofile.YouthLeagueJoinedDate, pupilprofile.YoungPioneerJoinedDate, "PupilProfile_Label_YouthLeagueJoinedDate", "PupilProfile_Label_YoungPioneerJoinedDate");
                if (!Utils.CompareTwoDate(pupilprofile.YouthLeagueJoinedDate, pupilprofile.YoungPioneerJoinedDate))
                {
                    throw new BusinessException("Ngày vào đoàn phải lớn hơn Ngày vào Đội");
                }
                //Utils.ValidateBeforeDate(pupilprofile.CommunistPartyJoinedDate, pupilprofile.YouthLeagueJoinedDate, "PupilProfile_Label_CommunistPartyJoinedDate", "PupilProfile_Label_YouthLeagueJoinedDate");
                if (!Utils.CompareTwoDate(pupilprofile.CommunistPartyJoinedDate, pupilprofile.YouthLeagueJoinedDate))
                {
                    throw new BusinessException("Ngày vào Đảng phải lớn hơn Ngày vào đoàn");
                }
                if (pupilprofile.EnrolmentDate > academicYear.SecondSemesterEndDate)
                {
                    throw new BusinessException("PupilProfile_Validate_Enrollment");
                }
            }
        }
        #endregion ValidateAll

        #region Parent method
        /// <summary>
        /// get list pupil of parent
        /// </summary>
        /// <auth>HaiVT</auth>
        /// <date>05/06/2013</date>       
        /// <returns></returns>
        public IQueryable<SelectPupilBO> GetSelectPupilByParentAccount(IDictionary<string, object> dic)
        {
            string mobile = Utils.GetString(dic, "mobile");
            IQueryable<SelectPupilBO> lstSelectPupilBO = from pp in PupilProfileBusiness.All
                                                         join sp in SchoolProfileBusiness.All on pp.CurrentSchoolID equals sp.SchoolProfileID
                                                         where
                                                         ((pp.FatherMobile.Equals(mobile) && pp.IsFatherSMS == true)
                                                         || (pp.MotherMobile.Equals(mobile) && pp.IsMotherSMS == true)
                                                         || (pp.SponsorMobile.Equals(mobile) && pp.IsSponsorSMS == true))
                                                         && pp.IsActive == true
                                                         select new SelectPupilBO
                                                         {
                                                             PupilID = pp.PupilProfileID,
                                                             FullName = pp.FullName,
                                                             BirthDate = pp.BirthDate,
                                                             Genre = pp.Genre,
                                                             SchoolID = sp.SchoolProfileID,
                                                             SchoolName = sp.SchoolName,
                                                         };

            return lstSelectPupilBO;
        }

        /// <summary>
        /// Tìm kiếm thông tin học sinh cho SMS Parents
        /// </summary>
        /// <author>tungnd</author>
        /// <date>9/10/2012</date>
        /// <param name="ethnic">Đối tượng PupilProfile</param>
        /// <returns>Đối tượng PupilProfile</returns>
        public IQueryable<PupilProfileBO> SearchForSMSParents(IDictionary<string, object> dic)
        {
            int CurrentClassID = Utils.GetInt(dic, "CurrentClassID");
            int CurrentSchoolID = Utils.GetInt(dic, "CurrentSchoolID");
            int CurrentAcademicYearID = Utils.GetInt(dic, "CurrentAcademicYearID");
            string PupilCode = Utils.GetString(dic, "PupilCode");
            string FullName = Utils.GetString(dic, "FullName");
            int AppliedLevel = Utils.GetInt(dic, "AppliedLevel");
            int EducationLevelID = Utils.GetInt(dic, "EducationLevelID");

            // bắt đầu tìm kiếm
            var query = from pp in PupilProfileRepository.All
                        join cp in ClassProfileRepository.All on pp.CurrentClassID equals cp.ClassProfileID
                        join sp in SchoolProfileRepository.All on pp.CurrentSchoolID equals sp.SchoolProfileID
                        where
                        cp.IsActive == true
                        select new PupilProfileBO
                        {
                            PupilProfileID = pp.PupilProfileID,
                            CurrentClassID = pp.CurrentClassID,
                            CurrentClassName = cp.DisplayName,
                            CurrentSchoolID = pp.CurrentSchoolID,
                            CurrentAcademicYearID = pp.CurrentAcademicYearID,
                            PupilCode = pp.PupilCode,
                            FullName = pp.FullName,
                            Genre = pp.Genre,
                            BirthDate = pp.BirthDate,
                            FatherFullName = pp.FatherFullName,
                            MotherFullName = pp.MotherFullName,
                            FatherMobile = pp.FatherMobile,
                            MotherMobile = pp.MotherMobile,
                            IsActive = pp.IsActive,
                            EducationLevelID = cp.EducationLevelID,
                            ClassName = pp.ClassProfile.DisplayName,
                            SchoolName = pp.SchoolProfile.SchoolName,
                            IsSponsorSMS = pp.IsSponsorSMS,
                            IsFatherSMS = pp.IsFatherSMS,
                            IsMotherSMS = pp.IsMotherSMS
                        };
            query = query.Where(o => (o.IsActive == true));

            IQueryable<EducationLevel> ls = EducationLevelBusiness.All.Where(o => (o.Grade == AppliedLevel));
            List<int> lsEducationLevelID = ls.Select(o => o.EducationLevelID).ToList();

            if (AppliedLevel != 0)
            {
                query = query.Where(o => lsEducationLevelID.Contains(o.EducationLevelID));
            }
            if (EducationLevelID != 0)
            {
                query = query.Where(pp => (pp.EducationLevelID == EducationLevelID));
            }
            if (CurrentClassID != 0)
            {
                query = query.Where(pp => (pp.CurrentClassID == CurrentClassID));
            }
            if (CurrentSchoolID != 0)
            {
                query = query.Where(pp => (pp.CurrentSchoolID == CurrentSchoolID));
            }
            if (CurrentAcademicYearID != 0)
            {
                query = query.Where(pp => (pp.CurrentAcademicYearID == CurrentAcademicYearID));
            }
            if (PupilCode.Trim().Length != 0)
            {
                query = query.Where(pp => (pp.PupilCode.ToUpper().Contains(PupilCode.ToUpper())));
            }
            if (FullName.Trim().Length != 0)
            {
                query = query.Where(pp => (pp.FullName.ToUpper().Contains(FullName.ToUpper())));
            }

            return query;
        }
        #endregion

        #region check conduct ranking pupil
        public bool IsNotConductRankingPupil(int pupilID, int classID)
        {
            PupilProfile pupilProfile = this.Find(pupilID);
            ClassProfile classProfile = ClassProfileBusiness.Find(classID);
            SchoolProfile schoolProfile = classProfile.SchoolProfile;
            bool isGDTX = schoolProfile.TrainingTypeID.HasValue && schoolProfile.TrainingType.Resolution == "GDTX";
            return IsNotConductRankingPupil(isGDTX, classProfile.EducationLevel.Grade, pupilProfile.PupilLearningType, pupilProfile.BirthDate);
        }

        public bool IsNotConductRankingPupil(bool isGDTX, int appliedLevel, int? learingType, DateTime? birthDate)
        {
            if (!isGDTX || (!learingType.HasValue && !birthDate.HasValue)) return false;

            return learingType == SystemParamsInFile.PUPIL_LEARNING_TYPE_SELF_LEARNING
                    || (learingType == SystemParamsInFile.PUPIL_LEARNING_TYPE_WORKING_AND_LEARNING
                    && ((appliedLevel == SystemParamsInFile.APPLIED_LEVEL_SECONDARY && birthDate.HasValue && DateTime.Now.AddYears(-20) >= birthDate.Value)
                    || (appliedLevel == SystemParamsInFile.APPLIED_LEVEL_TERTIARY && birthDate.HasValue && DateTime.Now.AddYears(-25) >= birthDate.Value)));
        }
        public bool IsNotConductRankingPupilImport(bool isGDTX, int appliedLevel, int? learingType, DateTime? birthDate)
        {
            if (!isGDTX || (!learingType.HasValue && !birthDate.HasValue)) return false;

            return learingType == SystemParamsInFile.PUPIL_LEARNING_TYPE_SELF_LEARNING
                    || (learingType == SystemParamsInFile.PUPIL_LEARNING_TYPE_WORKING_AND_LEARNING
                    && ((appliedLevel == SystemParamsInFile.APPLIED_LEVEL_SECONDARY && birthDate.HasValue && DateTime.Now.AddYears(-20) >= birthDate.Value)
                    || (appliedLevel == SystemParamsInFile.APPLIED_LEVEL_TERTIARY && birthDate.HasValue && DateTime.Now.AddYears(-25) >= birthDate.Value)));
        }
        #endregion

        #region Dung cho SMS_EDU
        public List<int> GetListPupilID(int SchoolID, int AcademicYearID, int EducationLevelID, int ClassID, int GradeID = 0)
        {
            try
            {
                if (ClassID == 0)
                {
                    IQueryable<PupilOfClass> iqPOC = from poc in PupilOfClassBusiness.All
                                                     where poc.SchoolID == SchoolID
                                                        && poc.AcademicYearID == AcademicYearID
                                                        && poc.PupilProfile.IsActive
                                                        && (poc.Status == GlobalConstants.PUPIL_STATUS_STUDYING || poc.Status == GlobalConstants.PUPIL_STATUS_GRADUATED)
                                                     select poc;
                    if (GradeID > 0) iqPOC = iqPOC.Where(o => o.ClassProfile.EducationLevel.Grade == GradeID);
                    if (EducationLevelID > 0) iqPOC = iqPOC.Where(o => o.ClassProfile.EducationLevelID == EducationLevelID);
                    return iqPOC.Select(o => o.PupilID).ToList();
                }
                else
                {
                    return (from poc in PupilOfClassBusiness.All
                            where poc.SchoolID == SchoolID
                            && poc.AcademicYearID == AcademicYearID
                            && poc.PupilProfile.IsActive
                            && poc.ClassID == ClassID
                            && (poc.Status == GlobalConstants.PUPIL_STATUS_STUDYING || poc.Status == GlobalConstants.PUPIL_STATUS_GRADUATED)
                            select poc.PupilID).ToList();


                }
            }
            catch (Exception ex)
            {
                logger.Error(ex.Message, ex);
                return null;
            }
        }

        public IQueryable<PupilProfileBO> GetListPupilProfile(IDictionary<string,object> dic)
        {
            try
            {
                int SchoolID = Utils.GetInt(dic, "SchoolID");
                int AcademicYearID = Utils.GetInt(dic, "AcademicYearID");
                List<int> lstClassID = Utils.GetIntList(dic, "lstClassID");
                List<int> lstPupilID = Utils.GetIntList(dic, "lstPupilID");
                int EducationLevelID = Utils.GetInt(dic, "EducationLevelID");
                int AppliedLevelID = Utils.GetInt(dic, "Appliedlevel");
                IQueryable<PupilProfileBO> iqPupil = (from poc in PupilOfClassBusiness.All
                                                join pf in PupilProfileBusiness.All on poc.PupilID equals pf.PupilProfileID
                                                join cp in ClassProfileBusiness.All on poc.ClassID equals cp.ClassProfileID
                                                where poc.SchoolID == SchoolID && poc.AcademicYearID == AcademicYearID && poc.PupilProfile.IsActive
                                                && (poc.Status == GlobalConstants.PUPIL_STATUS_STUDYING || poc.Status == GlobalConstants.PUPIL_STATUS_GRADUATED)
                                                //&& lstClassID.Contains(poc.ClassID)
                                                orderby poc.ClassProfile.EducationLevelID, poc.ClassProfile.OrderNumber, poc.ClassProfile.DisplayName, poc.OrderInClass, poc.PupilProfile.Name, poc.PupilProfile.FullName
                                                select new PupilProfileBO
                                                  {
                                                      PupilProfileID = poc.PupilID,
                                                      CurrentClassID = poc.ClassID,
                                                      FullName = pf.FullName,
                                                      PupilCode = pf.PupilCode,
                                                      ProfileStatus = poc.Status,
                                                      ClassName = cp.DisplayName,
                                                      EducationLevelID = cp.EducationLevelID,
                                                      Grade = cp.EducationLevel.Grade
                                                  });
                if (lstClassID.Count > 0)
                {
                    iqPupil = iqPupil.Where(p => lstClassID.Contains(p.CurrentClassID));
                }
                if (lstPupilID.Count > 0)
                {
                    iqPupil = iqPupil.Where(p => lstPupilID.Contains(p.PupilProfileID));
                }
                if (EducationLevelID > 0)
                {
                    iqPupil = iqPupil.Where(p => p.EducationLevelID == EducationLevelID);
                }
                if (AppliedLevelID > 0)
                {
                    iqPupil = iqPupil.Where(p => p.Grade == AppliedLevelID);
                }
                return iqPupil;
            }
            catch (Exception ex)
            {
                logger.Error(ex.Message, ex);
                return null;
            }
        }

        /// <summary>
        /// Lay danh sach hoc sinh tu danh sach Ma hoc sinh cho truoc
        /// </summary>
        /// <param name="pupilIDList"></param>
        /// <param name="SchoolID"></param>
        /// <param name="AcademicYearID"></param>
        /// <param name="ClassID"></param>
        /// <param name="EducationLevelID"></param>
        /// <param name="PupilCode"></param>
        /// <param name="Pupilname"></param>
        /// <param name="grade"></param>
        /// <param name="teacherID"></param>
        /// <returns></returns>
        public List<PupilOfClassBO> GetPupilsByIDsPaging(List<int> pupilIDList, int SchoolID, int AcademicYearID, int ClassID, int? EducationLevelID, string PupilCode, string Pupilname, int grade, int currentPage, int pageSize,ref int totalRecord, int teacherID)
        {
            try
            {
                IDictionary<string, object> dic = new Dictionary<string, object>();
                dic["SchoolID"] = SchoolID;
                dic["AcademicYearID"] = AcademicYearID;
                dic["EducationLevelID"] = EducationLevelID;
                dic["AppliedLevel"] = grade;
                dic["Check"] = "check";
                dic["ClassID"] = ClassID;
                dic["FullName"] = Pupilname;
                dic["PupilCode"] = PupilCode;

                if (teacherID > 0)
                {
                    List<ClassProfile> listClassID = this.GetListClassByHeadTeacher(AcademicYearID, SchoolID, teacherID);
                    if (listClassID != null && listClassID.Count > 0)
                    {
                        dic["ListClassID"] = listClassID;
                    }
                }
                IQueryable<PupilOfClassBO> iqPupil = (from poc in PupilOfClassBusiness.Search(dic)
                                                          join pp in PupilProfileBusiness.All on poc.PupilID equals pp.PupilProfileID
                                                          join cp in ClassProfileBusiness.All on poc.ClassID equals cp.ClassProfileID
                                                          join ed in EducationLevelBusiness.All on cp.EducationLevelID equals ed.EducationLevelID
                                                          join sp in SchoolProfileBusiness.All on poc.SchoolID equals sp.SchoolProfileID
                                                          where sp.SchoolProfileID == SchoolID
                                                          && pupilIDList.Contains(poc.PupilID)
                                                          && poc.PupilID == pp.PupilProfileID
                                                          && poc.Status != GlobalConstants.PUPIL_STATUS_MOVED_TO_OTHER_CLASS
                                                    select new PupilOfClassBO
                                                          {
                                                              PupilID = poc.PupilID,
                                                              PupilCode = pp.PupilCode,
                                                              AcademicYearID = poc.AcademicYearID,
                                                              ClassID = poc.ClassID,
                                                              PupilFullName = pp.FullName,
                                                              ClassName = cp.DisplayName,
                                                              SchoolName = sp.SchoolName,
                                                              EducationLevelID = cp.EducationLevelID,
                                                              Name = pp.Name,
                                                              FatherFullName = pp.FatherFullName,
                                                              MotherFullName = pp.MotherFullName,
                                                              SponserFullName = pp.SponsorFullName,
                                                              FatherMobile = pp.FatherMobile,
                                                              MotherMobile = pp.MotherMobile,
                                                              SponserMobile = pp.SponsorMobile,
                                                              Genre = pp.Genre,
                                                              Mobile = pp.Mobile,
                                                              OrderInClass = poc.OrderInClass,
                                                              Birthday = pp.BirthDate,
                                                              EducationLevelName = ed.Resolution,
                                                              IsRegisterContract = poc.IsRegisterContract,
                                                              Status = poc.Status,
                                                              OrderPupil = cp.OrderNumber.HasValue ? cp.OrderNumber.Value : 0,
                                                              PupilOfClassID = poc.PupilOfClassID
                                                          }).OrderBy(p => p.EducationLevelID)
                                                          .ThenBy(p => p.OrderPupil)
                                                          .ThenBy(p => p.ClassName)
                                                          .ThenBy(p => p.OrderInClass)
                                                          .ThenBy(p => p.Name);
                totalRecord = iqPupil.Count();
                if (ClassID <= 0)
                {
                    iqPupil = iqPupil.Skip((currentPage - 1) * pageSize).Take(pageSize);
                }
                return iqPupil.ToList();
            }
            catch (Exception ex)
            {
                logger.Error(ex.Message, ex);
                return new List<PupilOfClassBO>();
            }
        }
        public List<PupilOfClassBO> GetPupilsbySchoolIDPaging(int SchoolID, int AcademicYearID, int ClassID, string PupilCode, string Pupilname, int? EducationLevelID, int grade, int Page, int NumPage, bool isRegisContract, ref int totalRecord, int teacherID)
        {
            try
            {
                IDictionary<string, object> dic = new Dictionary<string, object>();
                dic["SchoolID"] = SchoolID;
                dic["AcademicYearID"] = AcademicYearID;
                dic["EducationLevelID"] = EducationLevelID;
                dic["AppliedLevel"] = grade;
                dic["Check"] = "check";
                dic["ClassID"] = ClassID;
                dic["PupilCode"] = PupilCode;
                dic["FullName"] = Pupilname;
                if (teacherID > 0)
                {
                    List<ClassProfile> listClassID = this.GetListClassByHeadTeacher(AcademicYearID, SchoolID, teacherID);
                    if (listClassID != null && listClassID.Count > 0)
                    {
                        dic["ListClassID"] = listClassID;
                    }
                    //viethd31: Fix loi khi GV khong dc phan cong lop nao lai tim ra toan bo HS cac lop
                    else
                    {
                        dic["ListClassID"] = new List<int> { -1 };
                    }
                }

                IQueryable<PupilOfClassBO> iqPupils = (from poc in PupilOfClassBusiness.Search(dic)
                                                       join pp in PupilProfileBusiness.All on poc.PupilID equals pp.PupilProfileID
                                                       join cp in ClassProfileBusiness.All on poc.ClassID equals cp.ClassProfileID
                                                       join ed in EducationLevelBusiness.All on cp.EducationLevelID equals ed.EducationLevelID
                                                       join sp in SchoolProfileBusiness.All on poc.SchoolID equals sp.SchoolProfileID // review : schoolprofile
                                                       where ((!isRegisContract && (poc.IsRegisterContract == null || poc.IsRegisterContract == false)) || isRegisContract == true)
                                                                       && poc.Status != GlobalConstants.PUPIL_STATUS_MOVED_TO_OTHER_CLASS
                                                       select new PupilOfClassBO
                                                                 {
                                                                     PupilID = poc.PupilID,
                                                                     PupilCode = pp.PupilCode,
                                                                     AcademicYearID = poc.AcademicYearID,
                                                                     ClassID = poc.ClassID,
                                                                     PupilFullName = pp.FullName,
                                                                     ClassName = cp.DisplayName,
                                                                     SchoolName = sp.SchoolName,
                                                                     EducationLevelID = cp.EducationLevelID,
                                                                     Name = pp.Name,
                                                                     FatherFullName = pp.FatherFullName,
                                                                     MotherFullName = pp.MotherFullName,
                                                                     SponserFullName = pp.SponsorFullName,
                                                                     FatherMobile = pp.FatherMobile,
                                                                     MotherMobile = pp.MotherMobile,
                                                                     SponserMobile = pp.SponsorMobile,
                                                                     Genre = pp.Genre,
                                                                     Mobile = pp.Mobile,
                                                                     OrderInClass = poc.OrderInClass,
                                                                     Birthday = pp.BirthDate,
                                                                     EducationLevelName = ed.Resolution,
                                                                     IsRegisterContract = poc.IsRegisterContract,
                                                                     Status = poc.Status,
                                                                     OrderPupil = cp.OrderNumber.HasValue ? cp.OrderNumber.Value : 0,
                                                                     PupilOfClassID = poc.PupilOfClassID
                                                                 }).OrderBy(p => p.EducationLevelID)
                                                                    .ThenBy(p => p.OrderPupil)
                                                                    .ThenBy(p => p.ClassName)
                                                                    .ThenBy(p => p.OrderInClass)
                                                                    .ThenBy(p => p.Name);

                totalRecord = iqPupils.Count();
                if (ClassID <= 0)
                {
                    iqPupils = iqPupils.Skip((Page - 1) * NumPage).Take(NumPage);
                }
                return iqPupils.ToList();
            }
            catch (Exception ex)
            {
                logger.Error(ex.Message, ex);
                return new List<PupilOfClassBO>();
            }
        }
        private List<ClassProfile> GetListClassByHeadTeacher(int AcademicYearID, int SchoolID, int TeacherID)
        {
            List<ClassProfile> listResult = ClassProfileBusiness.SearchBySchool(SchoolID, new Dictionary<string, object>(){
                {"AcademicYearID", AcademicYearID},
                {"TeacherByRoleID", TeacherID},
                {"Type", SystemParamsInFile.SUPERVISING_PERMISSION_HEAD_TEACHER}
                }).ToList();
            return listResult;
        }
        #endregion

        public IQueryable<PupilOfClassBO> getListPupilByListPupilID(List<int> listPupilId, int academicYearId)
        {
            IQueryable<PupilOfClassBO> listPupilprofile = (from pp in PupilProfileBusiness.All
                                                           join poc in PupilOfClassBusiness.All on pp.PupilProfileID equals poc.PupilID
                                                           join cp in ClassProfileBusiness.All on poc.ClassID equals cp.ClassProfileID
                                                           where poc.AcademicYearID == academicYearId
                                                           && listPupilId.Contains(poc.PupilID)
                                                           && poc.Status == SystemParamsInFile.PUPIL_STATUS_STUDYING
                                                           && cp.IsActive.Value
                                                           select new PupilOfClassBO
                                                           {
                                                               PupilCode = pp.PupilCode,
                                                               PupilID = pp.PupilProfileID,
                                                               PupilFullName = pp.FullName,
                                                               Name = pp.Name,
                                                               Genre = pp.Genre,
                                                               Birthday = pp.BirthDate,
                                                               ClassID = cp.ClassProfileID,
                                                               ClassName = cp.DisplayName
                                                           });
            return listPupilprofile;
        }

        public IQueryable<PupilOfClassBO> GetListPupilByListPupilIDForContract(List<int> pupilIDList, int SchoolID, int AcademicYearID, int ClassID, int? EducationLevelID, string PupilCode, string Pupilname, int grade, int teacherID)
        {
            
                IDictionary<string, object> dic = new Dictionary<string, object>();
                dic["SchoolID"] = SchoolID;
                dic["AcademicYearID"] = AcademicYearID;
                dic["EducationLevelID"] = EducationLevelID;
                dic["AppliedLevel"] = grade;
                dic["Check"] = "check";
                dic["ClassID"] = ClassID;
                dic["FullName"] = Pupilname;
                dic["PupilCode"] = PupilCode;
                if (teacherID > 0)
                {
                    List<int> listClassID = this.GetListClassByHeadTeacher(AcademicYearID, SchoolID, teacherID).Select(p => p.ClassProfileID).ToList();
                    if (listClassID != null && listClassID.Count > 0)
                    {
                        dic["ListClassID"] = listClassID;
                    }
                }
                return (from poc in PupilOfClassBusiness.Search(dic)
                                                          join pp in PupilProfileBusiness.All on poc.PupilID equals pp.PupilProfileID
                                                          join cp in ClassProfileBusiness.All.Where(cp => cp.AcademicYearID == AcademicYearID) on poc.ClassID equals cp.ClassProfileID
                                                          join ed in EducationLevelBusiness.All on cp.EducationLevelID equals ed.EducationLevelID
                                                          join sp in SchoolProfileBusiness.All.Where(sp => sp.SchoolProfileID == SchoolID) on poc.SchoolID equals sp.SchoolProfileID
                                                          where sp.SchoolProfileID == SchoolID
                                                          && pupilIDList.Contains(poc.PupilID)
                                                          && poc.PupilID == pp.PupilProfileID
                                                          && poc.Status != GlobalConstants.PUPIL_STATUS_MOVED_TO_OTHER_CLASS
                                                          select new PupilOfClassBO
                                                          {
                                                              PupilID = poc.PupilID,
                                                              PupilCode = pp.PupilCode,
                                                              AcademicYearID = poc.AcademicYearID,
                                                              ClassID = poc.ClassID,
                                                              PupilFullName = pp.FullName,
                                                              ClassName = cp.DisplayName,
                                                              SchoolName = sp.SchoolName,
                                                              EducationLevelID = cp.EducationLevelID,
                                                              Name = pp.Name,
                                                              FatherFullName = pp.FatherFullName,
                                                              MotherFullName = pp.MotherFullName,
                                                              SponserFullName = pp.SponsorFullName,
                                                              FatherMobile = pp.FatherMobile,
                                                              MotherMobile = pp.MotherMobile,
                                                              SponserMobile = pp.SponsorMobile,
                                                              Genre = pp.Genre,
                                                              Mobile = pp.Mobile,
                                                              OrderInClass = poc.OrderInClass,
                                                              Birthday = pp.BirthDate,
                                                              EducationLevelName = ed.Resolution,
                                                              IsRegisterContract = poc.IsRegisterContract,
                                                              OrderPupil = cp.OrderNumber.HasValue ? cp.OrderNumber.Value : 0,
                                                              Status = poc.Status,
                                                              PupilOfClassID = poc.PupilOfClassID
                                                          }).OrderBy(p => p.EducationLevelID).ThenBy(p => p.OrderPupil).ThenBy(p => p.ClassName).ThenBy(p => p.OrderInClass).ThenBy(p => p.Name);
        }

        #region column Mapping
        private IDictionary<string, object> PupilProfileMapping()
        {
            Dictionary<string, object> columnMap = new Dictionary<string, object>();
            columnMap.Add("PupilProfileID", "PUPIL_PROFILE_ID");
            columnMap.Add("CurrentClassID", "CURRENT_CLASS_ID");
            columnMap.Add("CurrentSchoolID", "CURRENT_SCHOOL_ID");
            columnMap.Add("CurrentAcademicYearID", "CURRENT_ACADEMIC_YEAR_ID");
            columnMap.Add("AreaID", "AREA_ID");
            columnMap.Add("ProvinceID", "PROVINCE_ID");
            columnMap.Add("DistrictID", "DISTRICT_ID");
            columnMap.Add("CommuneID", "COMMUNE_ID");
            columnMap.Add("EthnicID", "ETHNIC_ID");
            columnMap.Add("ReligionID", "RELIGION_ID");
            columnMap.Add("PolicyTargetID", "POLICY_TARGET_ID");
            columnMap.Add("FamilyTypeID", "FAMILY_TYPE_ID");
            columnMap.Add("PriorityTypeID", "PRIORITY_TYPE_ID");
            columnMap.Add("PolicyRegimeID", "POLICY_REGIME_ID");
            columnMap.Add("PreviousGraduationLevelID", "PREVIOUS_GRADUATION_LEVEL_ID");
            columnMap.Add("PupilCode", "PUPIL_CODE");
            columnMap.Add("Name", "NAME");
            columnMap.Add("FullName", "FULL_NAME");
            columnMap.Add("Genre", "GENRE");
            columnMap.Add("BirthDate", "BIRTH_DATE");
            columnMap.Add("BirthPlace", "BIRTH_PLACE");
            columnMap.Add("HomeTown", "HOME_TOWN");
            columnMap.Add("Telephone", "TELEPHONE");
            columnMap.Add("Mobile", "MOBILE");
            columnMap.Add("Email", "EMAIL");
            columnMap.Add("TempResidentalAddress", "TEMP_RESIDENTAL_ADDRESS");
            columnMap.Add("PermanentResidentalAddress", "PERMANENT_RESIDENTAL_ADDRESS");
            columnMap.Add("IsResidentIn", "IS_RESIDENT_IN");
            columnMap.Add("IsDisabled", "IS_DISABLED");
            columnMap.Add("DisabledTypeID", "DISABLED_TYPE_ID");
            columnMap.Add("DisabledSeverity", "DISABLED_SEVERITY");
            columnMap.Add("BloodType", "BLOOD_TYPE");
            columnMap.Add("EnrolmentMark", "ENROLMENT_MARK");
            columnMap.Add("EnrolmentDate", "ENROLMENT_DATE");
            columnMap.Add("ForeignLanguageTraining", "FOREIGN_LANGUAGE_TRAINING");
            columnMap.Add("IsYoungPioneerMember", "IS_YOUNG_PIONEER_MEMBER");
            columnMap.Add("YoungPioneerJoinedDate", "YOUNG_PIONEER_JOINED_DATE");
            columnMap.Add("YoungPioneerJoinedPlace", "YOUNG_PIONEER_JOINED_PLACE");
            columnMap.Add("IsYouthLeageMember", "IS_YOUTH_LEAGE_MEMBER");
            columnMap.Add("YouthLeagueJoinedDate", "YOUTH_LEAGUE_JOINED_DATE");
            columnMap.Add("YouthLeagueJoinedPlace", "YOUTH_LEAGUE_JOINED_PLACE");
            columnMap.Add("IsCommunistPartyMember", "IS_COMMUNIST_PARTY_MEMBER");
            columnMap.Add("CommunistPartyJoinedDate", "COMMUNIST_PARTY_JOINED_DATE");
            columnMap.Add("CommunistPartyJoinedPlace", "COMMUNIST_PARTY_JOINED_PLACE");
            columnMap.Add("FatherFullName", "FATHER_FULL_NAME");
            columnMap.Add("FatherBirthDate", "FATHER_BIRTH_DATE");
            columnMap.Add("FatherJob", "FATHER_JOB");
            columnMap.Add("FatherMobile", "FATHER_MOBILE");
            columnMap.Add("MotherFullName", "MOTHER_FULL_NAME");
            columnMap.Add("MotherBirthDate", "MOTHER_BIRTH_DATE");
            columnMap.Add("MotherJob", "MOTHER_JOB");
            columnMap.Add("MotherMobile", "MOTHER_MOBILE");
            columnMap.Add("SponsorFullName", "SPONSOR_FULL_NAME");
            columnMap.Add("SponsorBirthDate", "SPONSOR_BIRTH_DATE");
            columnMap.Add("SponsorJob", "SPONSOR_JOB");
            columnMap.Add("SponsorMobile", "SPONSOR_MOBILE");
            columnMap.Add("ProfileStatus", "PROFILE_STATUS");
            columnMap.Add("CreatedDate", "CREATED_DATE");
            columnMap.Add("IsActive", "IS_ACTIVE");
            columnMap.Add("ModifiedDate", "MODIFIED_DATE");
            columnMap.Add("HeightAtBirth", "HEIGHT_AT_BIRTH");
            columnMap.Add("FatherEmail", "FATHER_EMAIL");
            columnMap.Add("MotherEmail", "MOTHER_EMAIL");
            columnMap.Add("SponsorEmail", "SPONSOR_EMAIL");
            columnMap.Add("ChildOrder", "CHILD_ORDER");
            columnMap.Add("NumberOfChild", "NUMBER_OF_CHILD");
            columnMap.Add("EatingHabit", "EATING_HABIT");
            columnMap.Add("ReactionTrainingHabit", "REACTION_TRAINING_HABIT");
            columnMap.Add("AttitudeInStrangeScene", "ATTITUDE_IN_STRANGE_SCENE");
            columnMap.Add("OtherHabit", "OTHER_HABIT");
            columnMap.Add("FavoriteToy", "FAVORITE_TOY");
            columnMap.Add("EvaluationConfigID", "EVALUATION_CONFIG_ID");
            columnMap.Add("WeightAtBirth", "WEIGHT_AT_BIRTH");
            columnMap.Add("SupportingPolicy", "SUPPORTING_POLICY");
            columnMap.Add("IsReceiveRiceSubsidy", "IS_RECEIVE_RICE_SUBSIDY");
            columnMap.Add("IsResettlementTarget", "IS_RESETTLEMENT_TARGET");
            columnMap.Add("IsSupportForLearning", "IS_SUPPORT_FOR_LEARNING");
            columnMap.Add("ClassType", "CLASS_TYPE");
            columnMap.Add("M_ProvinceID", "M_PROVINCE_ID");
            columnMap.Add("M_OldID", "M_OLD_ID");
            columnMap.Add("IsFatherSMS", "IS_FATHER_SMS");
            columnMap.Add("IsMotherSMS", "IS_MOTHER_SMS");
            columnMap.Add("IsSponsorSMS", "IS_SPONSOR_SMS");
            columnMap.Add("Image", "IMAGE");
            columnMap.Add("MSourcedb", "M_SOURCEDB");
            columnMap.Add("PupilLearningType", "PUPIL_LEARNING_TYPE");
            columnMap.Add("LanguageCertificateID", "LANGUAGE_CERTIFICATE_ID");
            columnMap.Add("ItCertificateID", "IT_CERTIFICATE_ID");
            columnMap.Add("ItCertificate", "IT_CERTIFICATE");
            columnMap.Add("LanguageCertificate", "LANGUAGE_CERTIFICATE");
            columnMap.Add("Last2digitNumberSchool", "LAST_2DIGIT_NUMBER_SCHOOL");
            columnMap.Add("SynchronizeID", "SYNCHRONIZE_ID");
            columnMap.Add("VillageID", "VILLAGE_ID");
            columnMap.Add("FavouriteTypeOfDish", "FAVOURITE_TYPE_OF_DISH");
            columnMap.Add("HateTypeOfDish", "HATE_TYPE_OF_DISH");
            columnMap.Add("IdentifyNumber", "IDENTIFY_NUMBER");
            columnMap.Add("HomePlace", "HOME_PLACE");
            columnMap.Add("StorageNumber", "STORAGE_NUMBER");
            columnMap.Add("IsSwimming", "IS_SWIMMING");
            columnMap.Add("MinorityFather", "MINORITY_FATHER");
            columnMap.Add("MinorityMother", "MINORITY_MOTHER");
            columnMap.Add("UsedMoetProgram", "USED_MOET_PROGRAM");
            columnMap.Add("OtherEthnicID", "OTHER_ETHNIC_ID");

            return columnMap;
        }
        private IDictionary<string, object> PupilOfSchoolMapping()
        {
            Dictionary<string, object> columnMap = new Dictionary<string, object>();
            columnMap.Add("PupilOfSchoolID", "PUPIL_OF_SCHOOL_ID");
            columnMap.Add("SchoolID", "SCHOOL_ID");
            columnMap.Add("PupilID", "PUPIL_ID");
            columnMap.Add("EnrolmentDate", "ENROLMENT_DATE");
            columnMap.Add("EnrolmentType", "ENROLMENT_TYPE");
            columnMap.Add("M_Year", "M_YEAR");
            columnMap.Add("M_ProvinceID", "M_PROVINCE_ID");
            columnMap.Add("M_OldID", "M_OLD_ID");
            columnMap.Add("MSourcedb", "M_SOURCEDB");
            columnMap.Add("EndDate", "END_DATE");
            columnMap.Add("SynchronizeID", "SYNCHRONIZE_ID");
            return columnMap;
        }
        private IDictionary<string, object> PupilOfClassMapping()
        {
            Dictionary<string, object> columnMap = new Dictionary<string, object>();
            columnMap.Add("PupilOfClassID", "PUPIL_OF_CLASS_ID");
            columnMap.Add("PupilID", "PUPIL_ID");
            columnMap.Add("ClassID", "CLASS_ID");
            columnMap.Add("SchoolID", "SCHOOL_ID");
            columnMap.Add("AcademicYearID", "ACADEMIC_YEAR_ID");
            columnMap.Add("Year", "YEAR");
            columnMap.Add("AssignedDate", "ASSIGNED_DATE");
            columnMap.Add("OrderInClass", "ORDER_IN_CLASS");
            columnMap.Add("Description", "DESCRIPTION");
            columnMap.Add("NoConductEstimation", "NO_CONDUCT_ESTIMATION");
            columnMap.Add("Status", "STATUS");
            columnMap.Add("M_ProvinceID", "M_PROVINCE_ID");
            columnMap.Add("M_OldID", "M_OLD_ID");
            columnMap.Add("EndDate", "END_DATE");
            columnMap.Add("MSourcedb", "M_SOURCEDB");
            columnMap.Add("Last2digitNumberSchool", "LAST_2DIGIT_NUMBER_SCHOOL");
            columnMap.Add("CreatedAcademicYear", "CREATED_ACADEMIC_YEAR");
            columnMap.Add("SynchronizeID", "SYNCHRONIZE_ID");
            columnMap.Add("IsRegisterContract", "IS_REGISTER_CONTRACT");
            return columnMap;
        }
        #endregion

        public void ImportFromOtherSystem(int UserID, List<PupilProfile> lstPupilProfile, List<OrderInsertBO> lstOrder, List<int> lstClassID)
        {
            context.Configuration.AutoDetectChangesEnabled = false;
            context.Configuration.ValidateOnSaveEnabled = false;
            //Check quyen voi UserID
            //Validate
            foreach (int ClassID in lstClassID)
            {
                if (!UtilsBusiness.HasHeadTeacherPermission(UserID, ClassID))
                {
                    throw new BusinessException("Common_Error_HasHadTeacherPermission");
                }
            }
            if (lstPupilProfile.Count == 0)
            {
                return;
            }
            PupilProfile pp = lstPupilProfile[0];
            AcademicYear aca = AcademicYearBusiness.Find(pp.CurrentAcademicYearID);
            OrderInsertBO objOrder = null;

            List<string> lstPupilCode = lstPupilProfile.Select(p => p.PupilCode).Distinct().ToList();
            PupilProfileBusiness.BulkInsert(lstPupilProfile, PupilProfileMapping(), "PupilProfileID", "Image");

            List<PupilProfile> lstPupilDB = (from pf in PupilProfileBusiness.All
                                             where pf.CurrentAcademicYearID == pp.CurrentAcademicYearID
                                             && pf.CurrentSchoolID == pp.CurrentSchoolID
                                             && lstPupilCode.Contains(pf.PupilCode)
                                             && lstClassID.Contains(pf.CurrentClassID)
                                             && pf.IsActive
                                             select pf).ToList();
            PupilProfile objPFDB = null;

            //Tao doi tuong luu bang PupilOfSchool va PupilOfClass
            List<PupilOfSchool> lstPOS = new List<PupilOfSchool>();
            PupilOfSchool objPOS = null;
            List<PupilOfClass> lstPOC = new List<PupilOfClass>();
            PupilOfClass objPOC = null;
            string pupilCode = string.Empty;
            for (int i = 0; i < lstPupilCode.Count; i++)
            {
                objPOS = new PupilOfSchool();
                objPOC = new PupilOfClass();
                pupilCode = lstPupilCode[i];
                objPFDB = lstPupilDB.Where(p => p.PupilCode == pupilCode).FirstOrDefault();
                objPOS.PupilID = objPFDB.PupilProfileID;
                objPOS.SchoolID = objPFDB.CurrentSchoolID;
                objPOS.EnrolmentDate = objPFDB.EnrolmentDate;
                lstPOS.Add(objPOS);

                objPOC.PupilID = objPFDB.PupilProfileID;
                objPOC.ClassID = objPFDB.CurrentClassID;
                objPOC.SchoolID = objPFDB.CurrentSchoolID;
                objPOC.AcademicYearID = objPFDB.CurrentAcademicYearID;
                objPOC.Year = aca.Year;
                objPOC.AssignedDate = objPFDB.EnrolmentDate;
                objPOC.Status = GlobalConstants.PUPIL_STATUS_STUDYING;
                objOrder = lstOrder.Where(p => p.PupilCode == pupilCode).FirstOrDefault();
                objPOC.OrderInClass = objOrder != null ? objOrder.OrderNumber : 0;
                lstPOC.Add(objPOC);
            }

            PupilOfSchoolBusiness.BulkInsert(lstPOS, this.PupilOfSchoolMapping(), "PupilOfSchoolID");
            PupilOfClassBusiness.BulkInsert(lstPOC, this.PupilOfClassMapping(), "PupilOfClassID");

            context.Configuration.AutoDetectChangesEnabled = true;
            context.Configuration.ValidateOnSaveEnabled = true;
        }

        public List<PupilProfile> GetPupilWithPaging(int page, int size, int academicYearId, int schoolId, List<int> educationLevels, int? classId, string code,
            string fullname, List<int> notInPupilIds, List<int> forceInPupilIds, List<int> searchInPupilIds, out int total)
        {
            if (page <= 0)
            {
                page = 1;
            }
            //Lay cac hojc sinh co trang thai dang hoc hoac da tot nghiep
            IQueryable<PupilOfClass> iqPoc;
            if (forceInPupilIds != null && forceInPupilIds.Any())
            {
                iqPoc = PupilOfClassBusiness.AllNoTracking.Where(poc => (poc.Status == GlobalConstants.PUPIL_STATUS_STUDYING || poc.Status == GlobalConstants.PUPIL_STATUS_GRADUATED || forceInPupilIds.Contains(poc.PupilID)));
            }
            else
            {
                iqPoc = PupilOfClassBusiness.AllNoTracking.Where(poc => (poc.Status == GlobalConstants.PUPIL_STATUS_STUDYING || poc.Status == GlobalConstants.PUPIL_STATUS_GRADUATED));
            }
            IQueryable<ClassProfile> iqClass = ClassProfileBusiness.AllNoTracking.Where(c => c.IsActive == true);
            IQueryable<PupilProfile> iqpupil = PupilProfileBusiness.AllNoTracking.Where(c => c.IsActive == true);
            if (academicYearId > 0)
            {
                iqPoc = iqPoc.Where(p => p.AcademicYearID == academicYearId);
                iqClass = iqClass.Where(p => p.AcademicYearID == academicYearId);
            }
            if (classId > 0)
            {
                iqPoc = iqPoc.Where(p => p.ClassID == classId);
                iqClass = iqClass.Where(p => p.ClassProfileID == classId);
            }

            if (educationLevels != null && educationLevels.Any())
            {
                iqClass = iqClass.Where(p => educationLevels.Contains(p.EducationLevelID));
            }

            if (!string.IsNullOrEmpty(code))
            {
                iqpupil = iqpupil.Where(p => p.PupilCode.ToLower().Equals(code.ToLower().Trim()));
            }

            if (!string.IsNullOrEmpty(fullname))
            {
                iqpupil = iqpupil.Where(p => p.FullName.ToLower().Contains(fullname.ToLower().Trim()));
            }
            if (notInPupilIds != null && notInPupilIds.Any())
            {
                iqpupil = iqpupil.Where(p => !notInPupilIds.Contains(p.PupilProfileID));
            }
            if (searchInPupilIds != null)
            {
                iqpupil = iqpupil.Where(p => searchInPupilIds.Contains(p.PupilProfileID));
            }

            IQueryable<PupilProfile> iqValue = from pp in iqpupil
                                               join poc in iqPoc on pp.PupilProfileID equals poc.PupilID
                                               join cp in iqClass on poc.ClassID equals cp.ClassProfileID
                                               orderby cp.EducationLevelID, cp.OrderNumber, cp.DisplayName, poc.OrderInClass, pp.Name
                                               select pp;
            total = iqValue.Count();
            //iqValue= iqValue.OrderBy(o => o.CurrentClassID).ThenBy(o => o.FullName);
            List<PupilProfile> list = iqValue.Skip((page - 1) * size).Take(size).ToList();
            return list;

            //iqValue = iqValue.OrderBy(pp=>pp.CurrentClassID).ThenBy(pp=>o);
            /*const string paging = @"
                                SELECT * FROM
                                (
                                    SELECT a.*, rownum r__
                                    FROM
                                    (
                                        {0}
                                    ) a
                                    WHERE rownum < :take
                                )
                                WHERE r__ >= :skip";
            string sql = @"
                            FROM PUPIL_OF_CLASS poc, PUPIL_PROFILE pp, CLASS_PROFILE cp
                            WHERE pp.PUPIL_PROFILE_ID = poc.PUPIL_ID
                              AND cp.CLASS_PROFILE_ID = poc.CLASS_ID
                              AND pp.IS_ACTIVE = 1
                              AND poc.ACADEMIC_YEAR_ID = :academicYearId
                              AND cp.SCHOOL_ID = :schoolId
                            ";
            var param = new Dictionary<string, object>
            {
                {"academicYearId", academicYearId}, 
                {"schoolId", schoolId}
            };
            if (educationLevel != null && educationLevel > 0)
            {
                sql += " AND cp.EDUCATION_LEVEL_ID = :educationLevel ";
                param.Add("educationLevel", educationLevel);
            }
            if (classId != null && classId > 0)
            {
                sql += " AND cp.CLASS_PROFILE_ID = :classId ";
                param.Add("classId", classId);
            }
            if (!string.IsNullOrWhiteSpace(code))
            {
                sql += " AND lower(pp.PUPIL_CODE) like '%' || lower(:code) || '%' ";
                param.Add("code", code);
            }
            if (!string.IsNullOrWhiteSpace(fullname))
            {
                sql += " AND lower(pp.FULL_NAME) like '%' || lower(:fullname) || '%' ";
                param.Add("fullname", fullname);
            }
            total = context.CountFromSql("SELECT COUNT(*) " + sql, param);
            sql += "  ORDER BY pp.PUPIL_PROFILE_ID ASC ";
            param.Add("take", (page * size) + 1 );
            param.Add("skip", ((page-1) * size) + 1);
            List<PupilProfile> list = context.GenericFromSql<PupilProfile>(string.Format(paging, "SELECT pp.* " + sql), param);
            return list;*/
        }

        public List<IDictionary<string, object>> GetStatisticForPupilLeavingOffReport(int schoolId, int yearId)
        {
            List<IDictionary<string, object>> res = new List<IDictionary<string, object>>();
            string query1 = @"
                        select
	                            ---Tieu hoc
                                   --Tong
                                    sum (case when cp.education_level_id IN (1,2,3,4,5) then 1 else 0 end)   C13,
                                   --hoc sinh nu
                                    sum (case when cp.education_level_id IN (1,2,3,4,5) and PP.GENRE = 0 then 1 else 0 end)  D13,
                                   -- hoc sinh la dan toc
                                    sum (case when cp.education_level_id IN (1,2,3,4,5) and PP.ETHNIC_ID not in (1,79) then 1 else 0 end)  E13,
	                            ---THCS
                                   --Tong
                                    sum (case when cp.education_level_id IN (6,7,8,9) then 1 else 0 end)   C14,
                                   --hoc sinh nu
                                    sum (case when cp.education_level_id IN (6,7,8,9) and PP.GENRE = 0 then 1 else 0 end)  D14,
                                   -- hoc sinh la dan toc
                                    sum (case when cp.education_level_id IN (6,7,8,9) and PP.ETHNIC_ID not in (1,79) then 1 else 0 end)  E14,
	                            ---THPT
                                   --Tong
                                    sum (case when cp.education_level_id IN (10,11,12) then 1 else 0 end)   C15,
                                   --hoc sinh nu
                                    sum (case when cp.education_level_id IN (10,11,12) and PP.GENRE = 0 then 1 else 0 end)  D15,
                                   -- hoc sinh la dan toc
                                    sum (case when cp.education_level_id IN (10,11,12) and PP.ETHNIC_ID not in (1,79) then 1 else 0 end)  E15        
                            from pupil_of_class poc
                                join class_profile cp on poc.class_id = cp.class_profile_id
                                join pupil_profile pp on poc.pupil_id = pp.pupil_profile_id
                            where
                              cp.school_id = :SCHOOL_ID
                              and    poc.academic_year_id = :YEAR_ID
                              and  pp.is_active = 1
                        ";
            Dictionary<string, object> queryParams1 = new Dictionary<string, object>
                    {
                        {"SCHOOL_ID", schoolId},
                        {"YEAR_ID", yearId}
                    };

            res = context.CollectionFromSql(query1, queryParams1).ToList();

            string query2 = @"
                        select  	
	----Hoc sinh Tieu hoc
		---Tong so
        sum (case when plo.education_level_id in (1,2,3,4,5) then 1 else 0 end)  F13,
        --hoc sinh nu
        sum (case when plo.education_level_id in (1,2,3,4,5) and PP.GENRE = 0 then 1 else 0 end)  G13,
        -- hoc sinh la dan toc
        sum (case when plo.education_level_id in (1,2,3,4,5) and PP.ETHNIC_ID not in (1,79) then 1 else 0 end) H13,
		--hoan canh gia dinh kho khan
		sum (case when plo.education_level_id in (1,2,3,4,5) and plo.LEAVING_REASON_ID = 1 then 1 else 0 end) J13,
		-- Hoc luc yeu kem
		sum (case when plo.education_level_id in (1,2,3,4,5) and plo.LEAVING_REASON_ID = 4 then 1 else 0 end) K13,
		-- Xa truong, di lai kho khan
		sum (case when plo.education_level_id in (1,2,3,4,5) and plo.LEAVING_REASON_ID = 2 then 1 else 0 end) L13,
		-- Thien tai, dich benh
		sum (case when plo.education_level_id in (1,2,3,4,5) and plo.LEAVING_REASON_ID = 3 then 1 else 0 end)  M13,
		-- Do ky thi
		sum (case when plo.education_level_id in (1,2,3,4,5) and plo.LEAVING_REASON_ID = 12 then 1 else 0 end)  N13,
		-- Nguyen nhan khac
		sum (case when plo.education_level_id in (1,2,3,4,5) and plo.LEAVING_REASON_ID not in(1,2,3,4,12)  then 1 else 0 end) O13,
		
     ----Hoc sinh THCS
		---Tong so
        sum (case when plo.education_level_id in (6,7,8,9) then 1 else 0 end)  F14,
        --hoc sinh nu
        sum (case when plo.education_level_id in (6,7,8,9) and PP.GENRE = 0 then 1 else 0 end)  G14,
        -- hoc sinh la dan toc
        sum (case when plo.education_level_id in (6,7,8,9) and PP.ETHNIC_ID not in (1,79) then 1 else 0 end) H14,
		--hoan canh gia dinh kho khan
		sum (case when plo.education_level_id in (6,7,8,9) and plo.LEAVING_REASON_ID = 1 then 1 else 0 end) J14,
		-- Hoc luc yeu kem
		sum (case when plo.education_level_id in (6,7,8,9) and plo.LEAVING_REASON_ID = 4 then 1 else 0 end) K14,
		-- Xa truong, di lai kho khan
		sum (case when plo.education_level_id in (6,7,8,9) and plo.LEAVING_REASON_ID = 2 then 1 else 0 end) L14,
		-- Thien tai, dich benh
		sum (case when plo.education_level_id in (6,7,8,9) and plo.LEAVING_REASON_ID = 3 then 1 else 0 end)  M14,
		-- Do ky thi
		sum (case when plo.education_level_id in (6,7,8,9) and plo.LEAVING_REASON_ID = 12 then 1 else 0 end)  N14,
		-- Nguyen nhan khac
		sum (case when plo.education_level_id in (6,7,8,9) and plo.LEAVING_REASON_ID  not in(1,2,3,4,12) then 1 else 0 end)  O14,
		
	----Hoc sinh THPT
		---Tong so
        sum (case when plo.education_level_id in (10,11,12)then 1 else 0 end)  F15,
        --hoc sinh nu
        sum (case when plo.education_level_id in (10,11,12)and PP.GENRE = 0 then 1 else 0 end)  G15,
        -- hoc sinh la dan toc
        sum (case when plo.education_level_id in (10,11,12)and PP.ETHNIC_ID not in (1,79) then 1 else 0 end) H15,
		--hoan canh gia dinh kho khan
		sum (case when plo.education_level_id in (10,11,12)and plo.LEAVING_REASON_ID = 1 then 1 else 0 end) J15,
		-- Hoc luc yeu kem
		sum (case when plo.education_level_id in (10,11,12)and plo.LEAVING_REASON_ID = 4 then 1 else 0 end) K15,
		-- Xa truong, di lai kho khan
		sum (case when plo.education_level_id in (10,11,12)and plo.LEAVING_REASON_ID = 2 then 1 else 0 end) L15,
		-- Thien tai, dich benh
		sum (case when plo.education_level_id in (10,11,12)and plo.LEAVING_REASON_ID = 3 then 1 else 0 end)  M15,
		-- Do ky thi
		sum (case when plo.education_level_id in (10,11,12)and plo.LEAVING_REASON_ID = 12 then 1 else 0 end)  N15,
		-- Nguyen nhan khac
		sum (case when plo.education_level_id in (10,11,12)and plo.LEAVING_REASON_ID  not in(1,2,3,4,12) then 1 else 0 end)  O15       
from PUPIL_LEAVING_OFF plo
            join pupil_profile pp ON plo.pupil_id = pp.pupil_profile_id
where plo.SCHOOL_ID = :SCHOOL_ID
      AND plo.LEAVING_REASON_ID!=92
      and plo.ACADEMIC_YEAR_ID = (Select academic_year_id from ACADEMIC_YEAR ay
																where ay.SCHOOL_ID = :SCHOOL_ID2
																		and ay.YEAR = (Select Year from ACADEMIC_YEAR 
																									where ACADEMIC_YEAR_ID= :YEAR_ID))
                        ";
            Dictionary<string, object> queryParams2 = new Dictionary<string, object>
                    {
                        {"SCHOOL_ID", schoolId},
                        {"SCHOOL_ID2", schoolId},
                        {"YEAR_ID", yearId}
                    };

            var res2 = context.CollectionFromSql(query2, queryParams2).ToList();

            if (res2 != null && res2.Count > 0)
            {
                foreach (var item in res2)
                {
                    res.Add(item);
                }
            }

            return res;
        }

        private List<RESTORE_DATA_DETAIL> SavePupilInRestoreData(List<PupilOfClass> lstPupilOfClass, List<PupilProfile> lstPupilProfile, RESTORE_DATA objRes)
        {
            if (lstPupilOfClass == null || lstPupilOfClass.Count == 0)
            {
                return new List<RESTORE_DATA_DETAIL>();
            }
            List<RESTORE_DATA_DETAIL> lstRestoreDetail = new List<RESTORE_DATA_DETAIL>();
            RESTORE_DATA_DETAIL objRestoreDetail;
            PupilOfClass poc;
            PupilProfile objPupil;
            RestorePupilOfClassBO objUndoPupilPoc;
            RestorePupilProfileBO objUndoPupil;
            int partitionId = UtilsBusiness.GetPartionId(objRes.SCHOOL_ID);

            for (int i = 0; i < lstPupilOfClass.Count; i++)
            {
                poc = lstPupilOfClass[i];
                if (poc == null)
                {
                    continue;
                }
                objPupil = lstPupilProfile.FirstOrDefault(s => s.PupilProfileID == poc.PupilID);
                if (objPupil != null)
                {
                    objUndoPupil = new RestorePupilProfileBO
                    {
                        PulpilCode = objPupil.PupilCode,
                        PupilId = objPupil.PupilProfileID,
                        ReplacePupilCode = "",
                        SqlUndo = RestoreDataConstant.SQL_UNDO_PUPIL_PROFILE
                    };
                }
                else
                {
                    objUndoPupil = new RestorePupilProfileBO();
                }


                objUndoPupilPoc = new RestorePupilOfClassBO
                {
                    AcademicYearID = poc.AcademicYearID,
                    AssignedDate = poc.AssignedDate,
                    ClassID = poc.ClassID,
                    CreatedAcademicYear = poc.CreatedAcademicYear,
                    Description = poc.Description,
                    EndDate = poc.EndDate,
                    IsRegisterContract = poc.IsRegisterContract,
                    Last2digitNumberSchool = poc.Last2digitNumberSchool,
                    MSourcedb = poc.MSourcedb,
                    M_OldID = poc.M_OldID,
                    M_ProvinceID = poc.M_ProvinceID,
                    NoConductEstimation = poc.NoConductEstimation,
                    OrderInClass = poc.OrderInClass,
                    PupilID = poc.PupilID,
                    PupilOfClassID = poc.PupilOfClassID,
                    SchoolID = poc.SchoolID,
                    Status = poc.Status,
                    SynchronizeID = poc.SynchronizeID,
                    Year = poc.Year
                };
                objRestoreDetail = new RESTORE_DATA_DETAIL()
                {
                    ACADEMIC_YEAR_ID = objRes.ACADEMIC_YEAR_ID,
                    CREATED_DATE = DateTime.Now,
                    END_DATE = null,
                    IS_VALIDATE = 1,
                    LAST_2DIGIT_NUMBER_SCHOOL = partitionId,
                    ORDER_ID = 1,
                    RESTORE_DATA_ID = objRes.RESTORE_DATA_ID,
                    RESTORE_DATA_TYPE_ID = objRes.RESTORE_DATA_TYPE_ID,
                    SCHOOL_ID = objRes.SCHOOL_ID,
                    SQL_DELETE = JsonConvert.SerializeObject(objUndoPupilPoc),
                    SQL_UNDO = JsonConvert.SerializeObject(objUndoPupil),
                    TABLE_NAME = RestoreDataConstant.TABLE_PUPIL_PROFILE
                };
                lstRestoreDetail.Add(objRestoreDetail);

            }

            return lstRestoreDetail;
        }

        /// <summary>
        /// Lay danh sach hoc sinh tu danh sach Ma hoc sinh cho truoc
        /// </summary>
        /// <param name="pupilIDList"></param>
        /// <param name="SchoolID"></param>
        /// <param name="AcademicYearID"></param>
        /// <param name="ClassID"></param>
        /// <param name="EducationLevelID"></param>
        /// <param name="PupilCode"></param>
        /// <param name="Pupilname"></param>
        /// <param name="grade"></param>
        /// <param name="teacherID"></param>
        /// <returns></returns>
        public List<PupilProfileBO> GetPupilsByIDsPaging(List<int> pupilIDList, int SchoolID, int AcademicYearID, int ClassID, int? EducationLevelID, string PupilCode, string Pupilname, int grade, int currentPage, int pageSize, int teacherID, ref int total)
        {
          
            IDictionary<string, object> dic = new Dictionary<string, object>();
            dic["SchoolID"] = SchoolID;
            dic["AcademicYearID"] = AcademicYearID;
            dic["EducationLevelID"] = EducationLevelID;
            dic["AppliedLevel"] = grade;
            dic["Check"] = "check";
            dic["ClassID"] = ClassID;
            dic["FullName"] = Pupilname;
            dic["PupilCode"] = PupilCode;

            if (teacherID > 0)
            {
                List<int> listClassID = this.GetListClassByHeadTeacher(AcademicYearID, SchoolID, teacherID).Select(p => p.ClassProfileID).ToList();
                if (listClassID != null && listClassID.Count > 0)
                {
                    dic["ListClassID"] = listClassID;
                }
            }
            IQueryable<PupilProfileBO> iqPupil = (from poc in PupilOfClassBusiness.Search(dic)
                                                        join pp in PupilProfileBusiness.All on poc.PupilID equals pp.PupilProfileID
                                                        join cp in ClassProfileBusiness.All on poc.ClassID equals cp.ClassProfileID
                                                        join ed in EducationLevelBusiness.All on cp.EducationLevelID equals ed.EducationLevelID
                                                        join sp in SchoolProfileBusiness.All on poc.SchoolID equals sp.SchoolProfileID
                                                        where sp.SchoolProfileID == SchoolID
                                                        && pupilIDList.Contains(poc.PupilID)
                                                        && poc.PupilID == pp.PupilProfileID
                                                        && poc.Status != GlobalConstants.PUPIL_STATUS_MOVED_TO_OTHER_CLASS
                                                        select new PupilProfileBO
                                                        {
                                                            PupilProfileID = poc.PupilID,
                                                            PupilCode = pp.PupilCode,
                                                            CurrentAcademicYearID = poc.AcademicYearID,
                                                            CurrentClassID = poc.ClassID,
                                                            FullName = pp.FullName,
                                                            ClassName = cp.DisplayName,
                                                            SchoolName = sp.SchoolName,
                                                            EducationLevelID = cp.EducationLevelID,
                                                            Name = pp.Name,
                                                            FatherFullName = pp.FatherFullName,
                                                            MotherFullName = pp.MotherFullName,
                                                            SponsorFullName = pp.SponsorFullName,
                                                            FatherMobile = pp.FatherMobile,
                                                            MotherMobile = pp.MotherMobile,
                                                            SponsorMobile = pp.SponsorMobile,
                                                            Genre = pp.Genre,
                                                            Mobile = pp.Mobile,
                                                            OrderInClass = poc.OrderInClass,
                                                            BirthDate = pp.BirthDate,
                                                            EducationLevel = ed.Resolution,
                                                            IsRegisterContract = poc.IsRegisterContract,
                                                            ProfileStatus = poc.Status,
                                                            ClassOrderNumber = cp.OrderNumber.HasValue ? cp.OrderNumber.Value : 0,
                                                            PupilOfClassID = poc.PupilOfClassID
                                                        }).OrderBy(p => p.EducationLevelID)
                                                        .ThenBy(p => p.ClassOrderNumber)
                                                        .ThenBy(p => p.ClassName)
                                                        .ThenBy(p => p.OrderInClass)
                                                        .ThenBy(p => p.Name);

            total = iqPupil.Count();
            if (ClassID <= 0)
            {
                iqPupil = iqPupil.Skip((currentPage - 1) * pageSize).Take(pageSize);
            }



            return iqPupil.ToList();
           
        }

        #region AnHVD9 - Lấy danh sách học sinh (có phân trang) theo trường (format dạng chuỗi để giảm kích thước, KO dùng ToList để tránh cao tải)
        /// <summary>
        /// lay danh sach hoc sinh theo truong (co phan trang), ket qua tra ve theo dang noi chuoi
        /// </summary>
        /// <param name="SchoolID"></param>
        /// <param name="AcademicYearID"></param>
        /// <param name="ClassID"></param>
        /// <param name="PupilCode"></param>
        /// <param name="Pupilname"></param>
        /// <param name="EducationLevelID"></param>
        /// <param name="grade"></param>
        /// <param name="Page"></param>
        /// <param name="NumPage"></param>
        /// <param name="isRegisContract"></param>
        /// <param name="teacherID"></param>
        /// <returns></returns>
        public List<PupilProfileBO> GetPupilsbySchoolIDPaging(int SchoolID, int AcademicYearID, int ClassID, string PupilCode, string Pupilname, int? EducationLevelID, int grade, int Page, int NumPage, bool isRegisContract, int teacherID, ref int total)
        {
            IDictionary<string, object> dic = new Dictionary<string, object>();
            dic["SchoolID"] = SchoolID;
            dic["AcademicYearID"] = AcademicYearID;
            dic["EducationLevelID"] = EducationLevelID;
            dic["AppliedLevel"] = grade;
            dic["Check"] = "check";
            dic["ClassID"] = ClassID;
            dic["PupilCode"] = PupilCode;
            dic["FullName"] = Pupilname;
            if (teacherID > 0)
            {
                List<int> listClassID = this.GetListClassByHeadTeacher(AcademicYearID, SchoolID, teacherID).Select(p => p.ClassProfileID).ToList();
                if (listClassID != null && listClassID.Count > 0)
                {
                    dic["ListClassID"] = listClassID;
                }
                //viethd31: Fix loi khi GV khong dc phan cong lop nao lai tim ra toan bo HS cac lop
                else
                {
                    dic["ListClassID"] = new List<int> { -1 };
                }
            }

            IQueryable<PupilProfileBO> iqPupils = (from poc in PupilOfClassBusiness.Search(dic)
                                                       join pp in PupilProfileBusiness.All on poc.PupilID equals pp.PupilProfileID
                                                       join cp in ClassProfileBusiness.All on poc.ClassID equals cp.ClassProfileID
                                                       join ed in EducationLevelBusiness.All on cp.EducationLevelID equals ed.EducationLevelID
                                                       join sp in SchoolProfileBusiness.All on poc.SchoolID equals sp.SchoolProfileID // review : schoolprofile
                                                       where ((!isRegisContract && (poc.IsRegisterContract == null || poc.IsRegisterContract == false)) || isRegisContract == true)
                                                                       && poc.Status != GlobalConstants.PUPIL_STATUS_MOVED_TO_OTHER_CLASS
                                                    select new PupilProfileBO
                                                       {
                                                           PupilProfileID = poc.PupilID,
                                                           PupilCode = pp.PupilCode,
                                                           CurrentAcademicYearID = poc.AcademicYearID,
                                                           CurrentClassID = poc.ClassID,
                                                           FullName = pp.FullName,
                                                           ClassName = cp.DisplayName,
                                                           SchoolName = sp.SchoolName,
                                                           EducationLevelID = cp.EducationLevelID,
                                                           Name = pp.Name,
                                                           FatherFullName = pp.FatherFullName,
                                                           MotherFullName = pp.MotherFullName,
                                                           SponsorFullName = pp.SponsorFullName,
                                                           FatherMobile = pp.FatherMobile,
                                                           MotherMobile = pp.MotherMobile,
                                                           SponsorMobile = pp.SponsorMobile,
                                                           Genre = pp.Genre,
                                                           Mobile = pp.Mobile,
                                                           OrderInClass = poc.OrderInClass,
                                                           BirthDate = pp.BirthDate,
                                                           EducationLevel = ed.Resolution,
                                                           IsRegisterContract = poc.IsRegisterContract,
                                                           ProfileStatus = poc.Status,
                                                           ClassOrderNumber = cp.OrderNumber.HasValue ? cp.OrderNumber.Value : 0,
                                                           PupilOfClassID = poc.PupilOfClassID
                                                       }).OrderBy(p => p.EducationLevelID)
                                                                .ThenBy(p => p.ClassOrderNumber)
                                                                .ThenBy(p => p.ClassName)
                                                                .ThenBy(p => p.OrderInClass)
                                                                .ThenBy(p => p.Name);

            total = iqPupils.Count();
            if (ClassID <= 0)
            {
                iqPupils = iqPupils.Skip((Page - 1) * NumPage).Take(NumPage);
            }

            return iqPupils.ToList();

        }
        #endregion

        public List<PupilProfileBO> GetListPupilByListPupilID(List<int> pupilIDList, int SchoolID, int AcademicYearID, int ClassID, int? EducationLevelID, string PupilCode, string Pupilname, int grade, int teacherID)
        {

            IDictionary<string, object> dic = new Dictionary<string, object>();
            dic["SchoolID"] = SchoolID;
            dic["AcademicYearID"] = AcademicYearID;
            dic["EducationLevelID"] = EducationLevelID;
            dic["AppliedLevel"] = grade;
            dic["Check"] = "check";
            dic["ClassID"] = ClassID;
            dic["FullName"] = Pupilname;
            dic["PupilCode"] = PupilCode;
            if (teacherID > 0)
            {
                List<int> listClassID = this.GetListClassByHeadTeacher(AcademicYearID, SchoolID, teacherID).Select(p => p.ClassProfileID).ToList();
                if (listClassID != null && listClassID.Count > 0)
                {
                    dic["ListClassID"] = listClassID;
                }
            }
            return (from poc in PupilOfClassBusiness.Search(dic)
                                                      join pp in PupilProfileBusiness.All on poc.PupilID equals pp.PupilProfileID
                                                      join cp in ClassProfileBusiness.All.Where(cp => cp.AcademicYearID == AcademicYearID) on poc.ClassID equals cp.ClassProfileID
                                                      join ed in EducationLevelBusiness.All on cp.EducationLevelID equals ed.EducationLevelID
                                                      join sp in SchoolProfileBusiness.All.Where(sp => sp.SchoolProfileID == SchoolID) on poc.SchoolID equals sp.SchoolProfileID
                                                      where sp.SchoolProfileID == SchoolID
                                                      && pupilIDList.Contains(poc.PupilID)
                                                      && poc.PupilID == pp.PupilProfileID
                                                      && poc.Status != GlobalConstants.PUPIL_STATUS_MOVED_TO_OTHER_CLASS
                                                    select new PupilProfileBO
                                                      {
                                                          PupilProfileID = poc.PupilID,
                                                          PupilCode = pp.PupilCode,
                                                          CurrentAcademicYearID = poc.AcademicYearID,
                                                          CurrentClassID = poc.ClassID,
                                                          FullName = pp.FullName,
                                                          ClassName = cp.DisplayName,
                                                          SchoolName = sp.SchoolName,
                                                          EducationLevelID = cp.EducationLevelID,
                                                          Name = pp.Name,
                                                          FatherFullName = pp.FatherFullName,
                                                          MotherFullName = pp.MotherFullName,
                                                          SponsorFullName = pp.SponsorFullName,
                                                          FatherMobile = pp.FatherMobile,
                                                          MotherMobile = pp.MotherMobile,
                                                          SponsorMobile = pp.SponsorMobile,
                                                          Genre = pp.Genre,
                                                          Mobile = pp.Mobile,
                                                          OrderInClass = poc.OrderInClass,
                                                          BirthDate = pp.BirthDate,
                                                          EducationLevel = ed.Resolution,
                                                          IsRegisterContract = poc.IsRegisterContract,
                                                          ClassOrderNumber = cp.OrderNumber.HasValue ? cp.OrderNumber.Value : 0,
                                                          ProfileStatus = poc.Status,
                                                          PupilOfClassID = poc.PupilOfClassID
                                                      }).OrderBy(p => p.EducationLevelID).ThenBy(p => p.ClassOrderNumber).ThenBy(p => p.ClassName).ThenBy(p => p.OrderInClass).ThenBy(p => p.Name).ToList();


        }

        /// <summary>
        /// Lay danh sach hoc sinh theo cac thong tin
        /// 
        /// </summary>
        /// <param name="SchoolID"></param>
        /// <param name="AcademicYearID"></param>
        /// <param name="ClassID"></param>
        /// <param name="PupilCode"></param>
        /// <param name="Pupilname"></param>
        /// <param name="EducationLevelID"></param>
        /// <param name="grade"></param>
        /// <param name="teacherID"></param>
        /// <returns>Danh sách dưới dạng chuỗi để giảm kích thước dữ liệu cho response</returns>
        public List<PupilProfileBO> GetPupilsformatString(int SchoolID, int AcademicYearID, int ClassID, string PupilCode, string Pupilname, int? EducationLevelID, int grade, int teacherID)
        {

            IDictionary<string, object> dic = new Dictionary<string, object>();
            dic["SchoolID"] = SchoolID;
            dic["AcademicYearID"] = AcademicYearID;
            dic["EducationLevelID"] = EducationLevelID;
            dic["AppliedLevel"] = grade;
            dic["Check"] = "check";
            dic["ClassID"] = ClassID;
            dic["PupilCode"] = PupilCode;
            dic["FullName"] = Pupilname;
            if (teacherID > 0)
            {
                List<int> listClassID = this.GetListClassByHeadTeacher(AcademicYearID, SchoolID, teacherID).Select(p => p.ClassProfileID).ToList();
                if (listClassID != null && listClassID.Count > 0)
                {
                    dic["ListClassID"] = listClassID;
                }
            }
            IQueryable<PupilProfileBO> iqPupilDetail = (from poc in PupilOfClassBusiness.Search(dic)
                                                        join pp in PupilProfileBusiness.All on poc.PupilID equals pp.PupilProfileID
                                                        join cp in ClassProfileBusiness.All on poc.ClassID equals cp.ClassProfileID
                                                        join ed in EducationLevelBusiness.All on cp.EducationLevelID equals ed.EducationLevelID
                                                        join sp in SchoolProfileBusiness.All on poc.SchoolID equals sp.SchoolProfileID
                                                        where poc.Status != GlobalConstants.PUPIL_STATUS_MOVED_TO_OTHER_CLASS
                                                        select new PupilProfileBO
                                                        {
                                                            PupilProfileID = poc.PupilID,
                                                            PupilCode = pp.PupilCode,
                                                            CurrentAcademicYearID = poc.AcademicYearID,
                                                            CurrentClassID = poc.ClassID,
                                                            FullName = pp.FullName,
                                                            ClassName = cp.DisplayName,
                                                            SchoolName = sp.SchoolName,
                                                            EducationLevelID = cp.EducationLevelID,
                                                            Name = pp.Name,
                                                            FatherFullName = pp.FatherFullName,
                                                            MotherFullName = pp.MotherFullName,
                                                            SponsorFullName = pp.SponsorFullName,
                                                            FatherMobile = pp.FatherMobile,
                                                            MotherMobile = pp.MotherMobile,
                                                            SponsorMobile = pp.SponsorMobile,
                                                            Genre = pp.Genre,
                                                            Mobile = pp.Mobile,
                                                            OrderInClass = poc.OrderInClass,
                                                            BirthDate = pp.BirthDate,
                                                            EducationLevel = ed.Resolution,
                                                            IsRegisterContract = poc.IsRegisterContract,
                                                            ClassOrderNumber = cp.OrderNumber.HasValue ? cp.OrderNumber.Value : 0,
                                                            ProfileStatus = poc.Status,
                                                            PupilOfClassID = poc.PupilOfClassID
                                                        }).OrderBy(p => p.EducationLevelID)
                                                               .ThenBy(p => p.ClassOrderNumber)
                                                               .ThenBy(p => p.ClassName)
                                                               .ThenBy(p => p.OrderInClass)
                                                               .ThenBy(p => p.Name);
            return iqPupilDetail.ToList();
        }

        public List<PupilProfileBO> GetListPupilByClassID(int SchoolID, int AcademicYearID, int? ClassID)
        {

            IDictionary<string, object> dic = new Dictionary<string, object>();
            dic["SchoolID"] = SchoolID;
            dic["AcademicYearID"] = AcademicYearID;
            dic["ClassID"] = ClassID;
            dic["Check"] = "check";

            return (from poc in PupilOfClassBusiness.Search(dic)
                    join pp in PupilProfileBusiness.All on poc.PupilID equals pp.PupilProfileID
                    join cp in ClassProfileBusiness.All.Where(c => c.AcademicYearID == AcademicYearID) on poc.ClassID equals cp.ClassProfileID
                    join ed in EducationLevelBusiness.All on cp.EducationLevelID equals ed.EducationLevelID
                    join sp in SchoolProfileBusiness.All.Where(sp => sp.SchoolProfileID == SchoolID) on poc.SchoolID equals sp.SchoolProfileID
                    where (poc.Status == GlobalConstants.PUPIL_STATUS_STUDYING)
                    && cp.IsActive == true
                    select new PupilProfileBO
                  {
                      PupilProfileID = poc.PupilID,
                      PupilCode = pp.PupilCode,
                      CurrentAcademicYearID = poc.AcademicYearID,
                      CurrentClassID = poc.ClassID,
                      FullName = pp.FullName,
                      ClassName = cp.DisplayName,
                      SchoolName = sp.SchoolName,
                      EducationLevelID = cp.EducationLevelID,
                      Name = pp.Name,
                      FatherFullName = pp.FatherFullName,
                      MotherFullName = pp.MotherFullName,
                      SponsorFullName = pp.SponsorFullName,
                      FatherMobile = pp.FatherMobile,
                      MotherMobile = pp.MotherMobile,
                      SponsorMobile = pp.SponsorMobile,
                      Genre = pp.Genre,
                      Mobile = pp.Mobile,
                      OrderInClass = poc.OrderInClass,
                      BirthDate = pp.BirthDate,
                      EducationLevel = ed.Resolution
                  }).ToList();

        }

        /// <summary>
        /// lay danh sach hoc sinh theo danh sach id va nam hoc
        /// </summary>
        /// <param name="PupilIDs"></param>
        /// <param name="Year"></param>
        /// <returns></returns>
        public List<PupilProfileBO> GetPupilsByYear(List<int> PupilIDs, int Year)
        {
            try
            {
                //Khoi tao Business
                return (from p in PupilOfClassBusiness.All
                        where p.AcademicYear.Year == Year
                        && PupilIDs.Contains(p.PupilID)
                        && p.Status == GlobalConstants.PUPIL_STATUS_STUDYING
                        select new PupilProfileBO
                        {
                            CurrentClassID = p.ClassID,
                            PupilProfileID = p.PupilID,
                            CurrentAcademicYearID = p.AcademicYearID,
                            FullName = p.PupilProfile.FullName,
                            PupilOfClassID = p.PupilOfClassID
                        }).ToList();
            }
            catch
            {
                return null;
            }
        }

        /// <summary>
        /// lay thong tin hoc sinh theo dieu kien
        /// </summary>
        /// <param name="PupilProfileID"></param>
        /// <param name="Year"></param>
        /// <returns></returns>
        public PupilProfileBO GetPupilProfileByYear(int PupilProfileID, int Year)
        {
            return (from p in PupilOfClassBusiness.All
                    where p.AcademicYear.Year == Year
                   && p.PupilID == PupilProfileID
                   && p.Status == GlobalConstants.PUPIL_STATUS_STUDYING
                    select new PupilProfileBO
                                                    {
                                                        CurrentClassID = p.ClassID,
                                                        PupilProfileID = p.PupilID,
                                                        CurrentAcademicYearID = p.AcademicYearID,
                                                        FullName = p.PupilProfile.FullName,
                                                        PupilOfClassID = p.PupilOfClassID
                                                    }).FirstOrDefault();

        }

        #region Cap nhat danh sach dang ky HD
        /// <summary>
        ///  Anhnph - Cap nhat hoc sinh co dang ky HD
        /// </summary>
        /// <modifer data="3/2/2015">AnhVD9</modifer>
        /// <param name="lstPupilUpdate"></param>
        /// <param name="SchoolID"></param>
        /// <param name="AcademicYearID"></param>
        /// <param name="UserID"></param>
        /// <returns></returns>
        public bool UpdateRegisterContractPupil(List<int> lstPupilOfClassIDUpdate)
        {
            try
            {
                if (lstPupilOfClassIDUpdate == null || lstPupilOfClassIDUpdate.Count == 0) return true;

                List<PupilOfClass> LstPOC = this.PupilOfClassBusiness.All.Where(o => lstPupilOfClassIDUpdate.Contains(o.PupilOfClassID) && o.Status == GlobalConstants.PUPIL_STATUS_STUDYING).ToList();
                for (int i = LstPOC.Count - 1; i >= 0; i--)
                {
                    LstPOC[i].IsRegisterContract = true;
                }
                this.Save();
               
            }
            catch (Exception ex)
            {
                LogExtensions.ErrorExt(logger, DateTime.Now, "UpdateRegisterContractPupil", "", ex);
                return false;
            }
            return true;
        }
        #endregion

        public List<PupilProfileBO> GetListPupilSendSMS(List<int> pupilProfileList, int academicYearID)
        {
            try
            {
                List<PupilProfileBO> listPupil = new List<PupilProfileBO>();
                DateTime dateTimeLast = DateTime.Now.Date;
                List<int> pupilProfileIDListTemp = new List<int>();
                listPupil = (from pp in PupilProfileBusiness.All
                             join poc in PupilOfClassBusiness.All on pp.PupilProfileID equals poc.PupilID
                             join cp in ClassProfileBusiness.All on poc.ClassID equals cp.ClassProfileID
                             join hts in HeadTeacherSubstitutionBusiness.All on
                                new { ClassID = cp.ClassProfileID } equals new { ClassID = hts.ClassID } into hts_l
                             from hts_j in hts_l.Where(p => p.AssignedDate <= dateTimeLast && dateTimeLast <= p.EndDate && p.AcademicYearID == academicYearID).DefaultIfEmpty()
                             where pp.IsActive
                             && pupilProfileList.Contains(pp.PupilProfileID)
                             && poc.AcademicYearID == academicYearID
                             && cp.AcademicYearID == academicYearID
                             && (poc.Status == GlobalConstants.PUPIL_STATUS_STUDYING || poc.Status == GlobalConstants.PUPIL_STATUS_GRADUATED)
                             select new PupilProfileBO
                             {
                                 PupilProfileID = pp.PupilProfileID,
                                 FullName = pp.FullName,
                                 HeadTeacherID = hts_j.SubstituedHeadTeacher > 0 ? hts_j.SubstituedHeadTeacher : cp.HeadTeacherID.HasValue ? cp.HeadTeacherID.Value : 0
                             }).ToList();
                return listPupil;
            }
            catch (Exception ex)
            {
                logger.Error(ex.Message, ex);
                return new List<PupilProfileBO>();
            }
        }

        public List<PupilProfileBO> GetListPupilformatString(int SchoolID, int AcademicYearID, int EducationLevelID, int ClassID, int GradeID = 0)
        {
            try
            {
                IQueryable<PupilProfileBO> iqPupil = null;

                // Tách làm 2 nhánh để trong trường hợp chỉ có mỗi lớp không phải join vào bảng CLASS
                if (ClassID == 0)
                {
                    IQueryable<PupilOfClass> iqPOC = from poc in PupilOfClassBusiness.All
                                                     where poc.SchoolID == SchoolID && poc.AcademicYearID == AcademicYearID && poc.PupilProfile.IsActive
                                                     && (poc.Status == GlobalConstants.PUPIL_STATUS_STUDYING || poc.Status == GlobalConstants.PUPIL_STATUS_GRADUATED)
                                                     orderby poc.ClassProfile.EducationLevelID, poc.ClassProfile.OrderNumber, poc.ClassProfile.DisplayName, poc.OrderInClass, poc.PupilProfile.Name, poc.PupilProfile.FullName
                                                     select poc;
                    // tránh where điều kiện @constant = value
                    if (GradeID > 0)
                    {
                        iqPOC = iqPOC.Where(o => o.ClassProfile.EducationLevel.Grade == GradeID);
                    }

                    if (EducationLevelID > 0)
                    {
                        iqPOC = iqPOC.Where(o => o.ClassProfile.EducationLevelID == EducationLevelID);
                    }

                    iqPupil = (from poc in iqPOC
                               select new PupilProfileBO
                               {
                                   PupilProfileID = poc.PupilID,
                                   CurrentClassID = poc.ClassID,
                                   FullName = poc.PupilProfile.FullName,
                                   PupilCode = poc.PupilProfile.PupilCode,
                                   ProfileStatus = poc.Status,
                                   CurrentClassName = poc.ClassProfile.DisplayName
                               });
                }
                else
                {
                    if (ClassID > 0)
                    {
                        iqPupil = (from poc in PupilOfClassBusiness.All
                                   where poc.SchoolID == SchoolID && poc.AcademicYearID == AcademicYearID && poc.PupilProfile.IsActive && poc.ClassID == ClassID
                                   && (poc.Status == GlobalConstants.PUPIL_STATUS_STUDYING || poc.Status == GlobalConstants.PUPIL_STATUS_GRADUATED)
                                   orderby poc.OrderInClass, poc.PupilProfile.Name, poc.PupilProfile.FullName
                                   select new PupilProfileBO
                                   {
                                       PupilProfileID = poc.PupilID,
                                       CurrentClassID = poc.ClassID,
                                       FullName = poc.PupilProfile.FullName,
                                       PupilCode = poc.PupilProfile.PupilCode,
                                       ProfileStatus = poc.Status,
                                       CurrentClassName = poc.ClassProfile.DisplayName
                                   });

                    }
                }

                return iqPupil.ToList();
            }
            catch
            {
                return new List<PupilProfileBO>();
            }
        }

        public List<PupilMarkBO> GetListMarkRecordSemester(int SchoolID, int ClassID, int AcademicYearID, int Semester)
        {
            List<PupilMarkBO> listMarkOfSemesterResponse = new List<PupilMarkBO>();
           
            //Validate semester
            if (Semester != GlobalConstants.SEMESTER_OF_YEAR_FIRST && Semester != GlobalConstants.SEMESTER_OF_YEAR_SECOND
                && Semester != GlobalConstants.SEMESTER_OF_YEAR_ALL)
            {

            }
            else
            {
                //Khoi tao Business
                AcademicYear objAcademicYear = (from a in AcademicYearBusiness.All
                                                where a.SchoolID == SchoolID
                                                && a.AcademicYearID == AcademicYearID
                                                select a).FirstOrDefault();
                DateTime FromDateHKI = DateTime.Now;
                DateTime ToDateHKI = DateTime.Now;
                DateTime FromDateHKII = DateTime.Now;
                DateTime ToDateHKII = DateTime.Now;
                if (objAcademicYear != null)
                {
                    FromDateHKI = objAcademicYear.FirstSemesterStartDate.Value.Date;
                    ToDateHKI = objAcademicYear.FirstSemesterEndDate.Value.Date.AddHours(23).AddMinutes(59).AddSeconds(59);
                    FromDateHKII = objAcademicYear.SecondSemesterStartDate.Value.Date;
                    ToDateHKII = objAcademicYear.SecondSemesterEndDate.Value.Date.AddHours(23).AddMinutes(59).AddSeconds(59);
                }
                //Cap hoc
                int grade = ClassProfileBusiness.Find(ClassID).EducationLevel.Grade;
                List<ClassSubjectBO> listClassSubject = ClassSubjectBusiness.SearchByClass(ClassID, new Dictionary<string, object>() { { "Semester", Semester } }).Select(o => new ClassSubjectBO
                {
                    SubjectID = o.SubjectID,
                    DisplayName = o.SubjectCat.DisplayName,
                    IsCommenting = o.IsCommenting,
                    OrderInSubject = o.SubjectCat.OrderInSubject
                }).OrderBy(o => o.IsCommenting).ThenBy(o => o.OrderInSubject).ThenBy(o => o.DisplayName).ToList();
                //lay diem danh theo trong 1 thang cua hoc sinh
                //chiendd1: bo sung parttition
                int partitionId = SchoolID % 100;
                IQueryable<PupilAbsence> lstPupilAbsent = (from p in PupilAbsenceBusiness.All
                                                            where p.ClassID == ClassID
                                                            && p.SchoolID == SchoolID
                                                            && p.AcademicYearID == AcademicYearID
                                                            && p.Last2digitNumberSchool == partitionId
                                                            && ((grade > 1 && ((Semester == 1 && p.AbsentDate >= FromDateHKI && p.AbsentDate <= ToDateHKI)
                                                                                || (Semester == 2 && p.AbsentDate >= FromDateHKII && p.AbsentDate <= ToDateHKII)
                                                                                || (Semester == 3 && ((p.AbsentDate >= FromDateHKI && p.AbsentDate <= ToDateHKI)
                                                                                                || (p.AbsentDate >= FromDateHKII && p.AbsentDate <= ToDateHKII)))))
                                                            || (grade == 1 && ((Semester == 1 && p.AbsentDate >= FromDateHKI && p.AbsentDate <= ToDateHKI)
                                                                            || (Semester == 2 && ((p.AbsentDate >= FromDateHKI && p.AbsentDate <= ToDateHKI)
                                                                                            || (p.AbsentDate >= FromDateHKII && p.AbsentDate <= ToDateHKII))))))
                                                            select p);

                //lay diem chi tiet trung binh cua cac mon
                List<SummedUpRecordBO> listSummedUpRecord = SummedUpRecordBusiness.GetSummedUpRecord(AcademicYearID, SchoolID, Semester, ClassID, null).ToList();
                //lay diem trung binh cua tat ca cac mon theo dot + lay hoc luc, hanh kiem, xep hang theo dot
                List<PupilOfClassRanking> listPupilRanking = PupilRankingBusiness.GetPupilRankingOfClass(AcademicYearID, SchoolID, Semester, ClassID, null).ToList();
                //lay danh hieu thi dua
                List<PupilEmulationBO> listPupilEmulation = PupilEmulationBusiness.SearchBySchool(SchoolID, new Dictionary<string, object>() {
            { "AcademicYearID", AcademicYearID } ,
            { "ClassID", ClassID },
            { "Semester", Semester }
            }).Select(o => new PupilEmulationBO
            {
                PupilID = o.PupilID,
                HonourAchivementTypeResolution = o.HonourAchivementType.Resolution
            }).ToList();
                    
                // Lay thong tin cho tung hoc sinh
                // Thong tin diem theo hoc sinh
                List<SummedUpRecordBO> listPupilSum = new List<SummedUpRecordBO>();
                // Thong tin khen thuong
                PupilEmulationBO pupilEmulationBO = new PupilEmulationBO();
                //Object mapping vao list
                PupilMarkBO objMarkOfSemester = new PupilMarkBO();
                //object mapping thong tin diem trung binh mon (dung de tao content)
                SummedUpRecordBO objSummedUpRecordBO = new SummedUpRecordBO();
                int countAbsentP = 0;
                int countAbsentK = 0;
                foreach (PupilOfClassRanking pupilRank in listPupilRanking)
                {
                    int pupilID = pupilRank.PupilID;
                    // Khoi tao item
                    objMarkOfSemester = new PupilMarkBO();
                    // Lay len thong tin diem cua hoc sinh
                    listPupilSum = listSummedUpRecord.Where(o => o.PupilID == pupilID).ToList();
                    // Lay len thon tin khen thuong
                    pupilEmulationBO = listPupilEmulation.Find(o => o.PupilID == pupilID);
                    // Lay thong tin hoc luc hanh kiem
                    string pupilInfo = "";
                    int countSubject = listClassSubject.Count;
                    countAbsentP = lstPupilAbsent.Where(p => p.PupilID == pupilID && p.IsAccepted == true).Count();
                    countAbsentK = lstPupilAbsent.Where(p => p.PupilID == pupilID && p.IsAccepted == false).Count();
                    // Thong diem diem theo mon
                    foreach (ClassSubjectBO cs in listClassSubject)
                    {
                        objSummedUpRecordBO = listPupilSum.Find(o => o.SubjectID == cs.SubjectID);
                        if (objSummedUpRecordBO != null)
                        {
                            if (!objSummedUpRecordBO.SummedUpMark.HasValue && string.IsNullOrEmpty(objSummedUpRecordBO.JudgementResult))
                            {
                                continue;
                            }
                            pupilInfo += cs.DisplayName + ": " + (cs.IsCommenting.Value == SystemParamsInFile.ISCOMMENTING_TYPE_JUDGE ? objSummedUpRecordBO.JudgementResult + "\n" : (objSummedUpRecordBO.SummedUpMark.HasValue ? (grade > 1 ? objSummedUpRecordBO.SummedUpMark.Value.ToString("0.0") : objSummedUpRecordBO.SummedUpMark.Value.ToString("0.#")) : string.Empty)) + (grade > 1 && objSummedUpRecordBO.IsCommenting == SystemParamsInFile.ISCOMMENTING_TYPE_MARK ? "\n" : "");
                            if (grade == 1 && objSummedUpRecordBO.SummedUpMark.HasValue)
                            {
                                decimal SummedUpMark = objSummedUpRecordBO.SummedUpMark.Value;
                                if (SummedUpMark > GlobalConstants.EXCELLENT_MARK)
                                {
                                    pupilInfo += GlobalConstants.LEVEL_EXPERT + "\n";
                                }
                                else if (SummedUpMark > GlobalConstants.GOOD_MARK_PRIMARY)
                                {
                                    pupilInfo += GlobalConstants.LEVEL_GOOD + "\n";
                                }
                                else if (SummedUpMark >= GlobalConstants.NORMAL_MARK)
                                {
                                    pupilInfo += GlobalConstants.LEVEL_NORMAL + "\n";
                                }
                                else
                                {
                                    pupilInfo += GlobalConstants.LEVEL_WEAK + "\n";
                                }
                            }
                        }
                    }
                    if (pupilInfo.EndsWith("\n"))
                    {
                        pupilInfo = pupilInfo.Substring(0, pupilInfo.Length - 1);
                    }
                    // Neu la cap 1 thi bo sung them thong tin nghi hoc
                    if (grade == 1)
                    {
                        if (!string.IsNullOrEmpty(pupilRank.ConductLevel))
                        {
                            pupilInfo += "\nHạnh kiểm: " + pupilRank.ConductLevel;
                        }

                        if (!string.IsNullOrEmpty(pupilRank.CapacityLevel))
                        {
                            pupilInfo += "\nXếp loại giáo dục: " + pupilRank.CapacityLevel;
                        }

                        if (pupilEmulationBO != null && !string.IsNullOrEmpty(pupilEmulationBO.HonourAchivementTypeResolution))
                        {
                            pupilInfo += "\nDanh hiệu: " + pupilEmulationBO.HonourAchivementTypeResolution;
                        }

                        if (countAbsentP > 0)
                        {
                            pupilInfo += "\nNghỉ có phép: " + countAbsentP;
                        }

                        if (countAbsentK > 0)
                        {
                            pupilInfo += "\nNghỉ không phép: " + countAbsentK;
                        }
                    }
                    else
                    {
                        if (pupilRank.AverageMark.HasValue && pupilRank.AverageMark.Value > 0)
                        {
                            pupilInfo += "\nTrung bình các môn: " + pupilRank.AverageMark.Value.ToString("0.0");
                        }

                        if (!string.IsNullOrEmpty(pupilRank.CapacityLevel))
                        {
                            pupilInfo += "\nHọc lực: " + pupilRank.CapacityLevel;
                        }

                        if (!string.IsNullOrEmpty(pupilRank.ConductLevel))
                        {
                            pupilInfo += "\nHạnh kiểm: " + pupilRank.ConductLevel;
                        }

                        if (pupilRank.Rank.HasValue && pupilRank.Rank.Value > 0)
                        {
                            pupilInfo += "\nXếp hạng: " + pupilRank.Rank.ToString();
                        }

                        if (pupilEmulationBO != null && !string.IsNullOrEmpty(pupilEmulationBO.HonourAchivementTypeResolution))
                        {
                            pupilInfo += "\nDanh hiệu thi đua: " + pupilEmulationBO.HonourAchivementTypeResolution;
                        }

                        if (countAbsentP > 0)
                        {
                            pupilInfo += "\nNghỉ có phép: " + countAbsentP;
                        }

                        if (countAbsentK > 0)
                        {
                            pupilInfo += "\nNghỉ không phép: " + countAbsentK;
                        }

                    }
                    // Thong tin diem TBM
                    // gan thong tin chi tiet object de add
                    objMarkOfSemester.Content = pupilInfo;
                    objMarkOfSemester.PupilProfileID = pupilID;
                    // Gan vao list de tra ve
                    listMarkOfSemesterResponse.Add(objMarkOfSemester);
                }
 
            }
          
            return listMarkOfSemesterResponse;
        }

        public List<PupilMarkBO> GetMarkRecordByClass(int SchoolID, int ClassID, int EducationLevel,
             int AcademicYearID, DateTime DateStart, DateTime DateEnd, bool CheckAbsent, int Semester)
        {
            List<PupilMarkBO> objResultResponse = new List<PupilMarkBO>();

            //Kiem tra neu EducationLevel <=0 hoac EducationLevel >= 13
            if (EducationLevel <= 0 || EducationLevel >= 13)
            {
                return new List<PupilMarkBO>();
            }
            //Khoi tao Business
            // Kiem tra truong hoac nam hoc khong ton tai
            if (SchoolProfileBusiness.Find(SchoolID) == null)
            {
                //gan validationCode thanh cong
                return new List<PupilMarkBO>();
            }
            if (AcademicYearBusiness.Find(AcademicYearID) == null)
            {
                //gan validationCode thanh cong
                return new List<PupilMarkBO>();
            }

            //lay len diem chi tiet cac mon theo datetime
            IDictionary<string, object> dic = new Dictionary<string, object>();
            dic["SchoolID"] = SchoolID;
            dic["ClassID"] = ClassID;
            dic["AcademicYearID"] = AcademicYearID;
            dic["Semester"] = Semester;
            //Lay len diem cua lop
            IQueryable<MarkRecordWSBO> IQMarkRecord = MarkRecordBusiness.GetMardRecordOfAllSubjectInClass(dic);
            //Lay diem nhan xet cua lop
            IQueryable<JudgeRecordWSBO> IQJudgeRecord = JudgeRecordBusiness.GetAllJudgeRecordOfClass(dic);

            //Loc thoi gian cham diem mon tinh diem
            //Lay lai thoi gian bat dau va ket thuc cho chinh xac
            DateTime fromDate = new DateTime(DateStart.Year, DateStart.Month, DateStart.Day);
            DateTime toDate = new DateTime(DateEnd.Year, DateEnd.Month, DateEnd.Day).AddDays(1);
            bool isPrimary = EducationLevel >= 1 && EducationLevel <= 5;
            var IQFilterMarkRecordTmp = (from m in IQMarkRecord
                                         where m.MarkedDate >= fromDate
                                         && m.MarkedDate < toDate
                                         select new
                                         {
                                             m.PupilID,
                                             m.SubjectName,
                                             m.SubjectOrderNumber,
                                             m.MarkOrderNumber,
                                             m.Mark,
                                             m.Title,
                                             m.MarkTypeID,
                                             m.MarkedDate,
                                             m.MarkredDatePrimary
                                         }).ToList();

            var IQFilterMarkRecord = (from m in IQFilterMarkRecordTmp
                                      where (isPrimary && !m.Title.Contains("VGK") && !m.Title.Contains("ĐGK")
                                                       && !m.Title.Contains("VCK") && !m.Title.Contains("ĐCK")
                                                       && !m.Title.Contains("VCN") && !m.Title.Contains("ĐCN"))
                                      || (isPrimary == false)
                                      select new
                                      {
                                          m.PupilID,
                                          m.SubjectName,
                                          m.SubjectOrderNumber,
                                          m.MarkOrderNumber,
                                          Mark = m.Mark.HasValue ? m.Mark.Value.ToString("0.#") : string.Empty,
                                          Title = !isPrimary && m.Title.Contains("M") ? "M" : !isPrimary && m.Title.Contains("P") ? "15P" : !isPrimary && m.Title.Contains("V")
                                                    ? "1T" : !isPrimary && m.Title.Contains("HK") ? "KTHK" : isPrimary && m.Title.Contains("ĐTX") && m.MarkredDatePrimary.HasValue
                                                    ? m.Title + (SetMonthWithPrimary(m.MarkredDatePrimary.Value.Month))
                                                    : isPrimary && m.Title.Contains("GK") ? "GK" : isPrimary && (m.Title.Contains("CK") || m.Title.Contains("CN")) ? "CK" : m.Title + " ",
                                          m.MarkTypeID
                                      }).ToList();

            //Loc thoi gian cham diem mon nhan xet
            var LsFilterJudgeRecordTmp = (from j in IQJudgeRecord
                                          where j.MarkedDate >= fromDate
                                          && j.MarkedDate < toDate
                                          select new
                                          {
                                              j.PupilID,
                                              j.SubjectID,
                                              j.SubjectName,
                                              j.SubjectOrderNumber,
                                              j.MarkOrderNumber,
                                              Judgement = j.Judgement == null ? string.Empty : j.Judgement,
                                              j.MarkTypeID,
                                              Title = j.Title,
                                              EducationLevelID = j.EducationLevelID
                                          }).ToList();

            var LsFilterJudgeRecord = (from j in LsFilterJudgeRecordTmp
                                       select new
                                       {
                                           j.PupilID,
                                           j.SubjectID,
                                           j.SubjectName,
                                           j.SubjectOrderNumber,
                                           j.MarkOrderNumber,
                                           Judgement = j.Judgement == null ? string.Empty : j.Judgement,
                                           j.MarkTypeID,
                                           Title = j.Title.Contains("P") ? "15P" : j.Title.Contains("V") ? "1T" : j.Title.Contains("M") ? "M" : isPrimary ? "NX" + (Semester == 2 ? (j.EducationLevelID > 2 ? j.MarkOrderNumber + 5 : j.MarkOrderNumber + 4) : j.MarkOrderNumber) : (!isPrimary && j.Title.Contains("HK") ? "KTHK" : j.Title),
                                       }).ToList();
            //Loc nhung hoc sinh co diem mon tin diem phat sinh
            var IQMarkRecordGroup = (from p in IQFilterMarkRecord
                                     orderby p.MarkTypeID, p.SubjectOrderNumber, p.SubjectName, p.MarkOrderNumber
                                     group p by new
                                     {
                                         p.PupilID,
                                         p.SubjectName,
                                         p.SubjectOrderNumber,
                                         p.Title
                                     } into g
                                     select new
                                     {
                                         PupilID = g.Key.PupilID.Value,
                                         SubjectName = g.Key.SubjectName,
                                         SubjectOrderNumber = g.Key.SubjectOrderNumber,
                                         OrderPrimaryID = SetOrderPrimary(isPrimary, g.Key.Title),
                                         MarkContent = (isPrimary && g.Key.Title.Contains("ĐTX")) ? (g.Key.Title.Replace("ĐTX", "T") + ": " + g.Select(x => x.Mark).Aggregate((a, b) => (a + " " + b)))
                                                : isPrimary && (g.Key.Title.Contains("GK") || g.Key.Title.Contains("CK")) ? (g.Key.Title + ": " + g.Select(x => x.Mark).Aggregate((a, b) => (a + ", " + b))) : (g.Key.Title + ": " + g.Select(x => x.Mark).Aggregate((a, b) => (a + " " + b)))
                                     }).ToList();
            //Nhom diem cua cac mon tinh diem lai
            var IQMarkRecordResponse = (from mg in IQMarkRecordGroup
                                        orderby mg.SubjectOrderNumber, mg.SubjectName, mg.OrderPrimaryID
                                        group mg
                                        by new
                                        {
                                            mg.PupilID,
                                            mg.SubjectName
                                        }
                                            into g
                                            select new PupilMarkBO
                                            {
                                                PupilProfileID = g.Key.PupilID,
                                                Content = "-" + g.Key.SubjectName + "\n" + g.Select(x => x.MarkContent).Aggregate((a, b) => (a + "\n" + b)) + "\n"
                                            }).ToList();

            // neu la cap 1 thi lay diem kiem tra hoc ky mon nhan xet trong summed_up_record                
            //var judgeRecordPrimary = (
            //                          from poc in PupilOfClassBusiness.All
            //                          join sur in SummedUpRecordBusiness.All on poc.PupilID equals sur.PupilID
            //                          where sur.ClassID == ClassID
            //                          && sur.AcademicYearID == AcademicYearID
            //                          && sur.Semester == Semester
            //                          && poc.ClassID == ClassID
            //                          && poc.AcademicYearID == AcademicYearID
            //                          && poc.Status == GlobalConstants.PUPIL_STATUS_STUDYING
            //                          && sur.JudgementResult != null
            //                          && isPrimary
            //                          select new
            //                          {
            //                              sur.PupilID,
            //                              sur.SubjectID,
            //                              sur.JudgementResult
            //                          }).ToList();

            //isPrimary = isPrimary && judgeRecordPrimary != null && judgeRecordPrimary.Count > 0;

            //Loc nhung hoc sinh co diem  mon nhan xet phat sinh
            var IQMarkJudgeGroup = (from p in LsFilterJudgeRecord
                                    orderby p.MarkTypeID, p.MarkOrderNumber
                                    group p by new
                                    {
                                        p.PupilID,
                                        p.SubjectName,
                                        p.SubjectID,
                                        p.SubjectOrderNumber,
                                        Title = p.Title
                                    } into g
                                    select new
                                    {
                                        PupilID = g.Key.PupilID.Value,
                                        SubjectName = g.Key.SubjectName,
                                        SubjectID = g.Key.SubjectID,
                                        SubjectOrderNumber = g.Key.SubjectOrderNumber,
                                        JudgementContent = g.Key.Title
                                            + ": " + g.Select(x => (x.Judgement.ToString())).Aggregate((a, b) => (a + " " + b))
                                        //((g.Key.Title.Contains("NX")
                                        //  && isPrimary
                                        //  )
                                        //? g.Select(x => (x.Judgement.ToString())).Aggregate((a, b) => (a + " " + b))
                                        //+ (g.Key.Title == LsFilterJudgeRecord.Where(p => p.PupilID == g.Key.PupilID && p.SubjectID == g.Key.SubjectID && p.Title.Contains("NX")).OrderByDescending(p => (Convert.ToInt32(p.Title.Replace("NX", "")))).Select(p => p.Title).FirstOrDefault()
                                        //        && judgeRecordPrimary.Where(p => p.SubjectID == g.Key.SubjectID && p.PupilID == g.Key.PupilID).ToList() != null
                                        //        && judgeRecordPrimary.Where(p => p.SubjectID == g.Key.SubjectID && p.PupilID == g.Key.PupilID).Count() > 0
                                        //        ? ("\nHK: " + judgeRecordPrimary.Where(p => p.SubjectID == g.Key.SubjectID && p.PupilID == g.Key.PupilID).Select(x => (x.JudgementResult.ToString())).Aggregate((a, b) => (a + " " + b)))
                                        //        : string.Empty)
                                        //: g.Select(x => (x.Judgement.ToString())).Aggregate((a, b) => (a + " " + b)))
                                    }).ToList();

            //Nhom diem cua cac mon nhan xet lai
            var IQMarkJudgeResponse = (from mg in IQMarkJudgeGroup
                                       orderby mg.SubjectOrderNumber, mg.SubjectName
                                       group mg
                                        by new
                                        {
                                            mg.PupilID,
                                            mg.SubjectName,
                                            mg.SubjectID
                                        }
                                           into g
                                           select new PupilMarkBO
                                           {
                                               PupilProfileID = g.Key.PupilID,
                                               Content = "-" + g.Key.SubjectName + "\n"
                                                        + g.Select(x => x.JudgementContent).Aggregate((a, b) => (a + "\n" + b)) + "\n"
                                           }).ToList();
            // loc hoc sinh trung nhau
            IQMarkJudgeResponse = (from p in IQMarkJudgeResponse
                                   group p by new { p.PupilProfileID }
                                       into g
                                       select new PupilMarkBO
                                       {
                                           PupilProfileID = g.Key.PupilProfileID,
                                           Content = g.Where(p => p.PupilProfileID == g.Key.PupilProfileID).Select(p => p.Content).Aggregate((a, b) => (a + b))
                                       }).ToList();

            IQMarkRecordResponse = (from p in IQMarkRecordResponse
                                    group p by new { p.PupilProfileID }
                                        into g
                                        select new PupilMarkBO
                                        {
                                            PupilProfileID = g.Key.PupilProfileID,
                                            Content = g.Where(p => p.PupilProfileID == g.Key.PupilProfileID).Select(p => p.Content).Aggregate((a, b) => (a + b))
                                        }).ToList();

            // Bo sung them mon
            foreach (var item in IQMarkJudgeResponse)
            {
                var t = IQMarkRecordResponse.FirstOrDefault(o => o.PupilProfileID == item.PupilProfileID);
                if (t != null)
                {
                    t.Content += item.Content;
                    t.Content = t.Content.Substring(0, t.Content.Length - 1);
                }
                else
                {
                    IQMarkRecordResponse.Add(item);
                }
            }
            //Filter nhung hoc sinh co content moi hien thi
            //if (EducationLevel == WCFConstant.SCHOOL_PRIMARY_LEVEL)// Neu la truong tieu hoc
            //{
            //    //gan gia tri thuoc tinh dinh kem 
            //    objResultResponse.LstMarkRecordDefault = IQMarkRecordResponse;
            //}
            //else
            //{
            if (CheckAbsent)
            {
                DateStart = DateTime.Now.Date.AddMonths(-1).AddDays(1);
                DateEnd = DateTime.Now.Date.AddHours(23).AddMinutes(59).AddSeconds(59);
                //lay len danh sach hoc sinh nghi hoc
                List<PupilAbsence> IQPupilAbsence = PupilAbsenceBusiness.SearchBySchool(SchoolID, dic).Where(p => p.AbsentDate >= DateStart && p.AbsentDate <= DateEnd).ToList();

                //Filter de Count so ngay nghi khong phep cua tung hoc sinh trong lop
                var IQPupilAbsencePCount = (from p in IQPupilAbsence
                                            where p.IsAccepted == true
                                            group p by new
                                            {
                                                p.PupilID
                                            } into g
                                            select new
                                            {
                                                PupilProfileID = g.Key.PupilID,
                                                Absent = g.Count() > 0 ? "\n-Nghỉ có phép: " + g.Count().ToString() : string.Empty
                                            }).ToList();
                //Filter de Count so ngay nghi co phep cua tung hoc sinh trong lop
                var IQPupilAbsenceKCount = (from p in IQPupilAbsence
                                            where p.IsAccepted == false
                                            group p by new
                                            {
                                                p.PupilID
                                            } into g
                                            select new
                                            {
                                                PupilProfileID = g.Key.PupilID,
                                                Absent = g.Count() > 0 ? "\n-Nghỉ không phép: " + g.Count().ToString() : string.Empty
                                            }).ToList();

                //Loc nhung hoc sinh co diem phat sinh
                List<PupilMarkBO> IQFilterMarkRecordResponse = (from mr in IQMarkRecordResponse
                                                                join fa in IQPupilAbsencePCount on mr.PupilProfileID equals fa.PupilProfileID into jP
                                                                from g1 in jP.DefaultIfEmpty()
                                                                join pa in IQPupilAbsenceKCount on mr.PupilProfileID equals pa.PupilProfileID into jK
                                                                from g2 in jK.DefaultIfEmpty()
                                                                select new PupilMarkBO
                                                                      {
                                                                          PupilProfileID = mr.PupilProfileID,
                                                                          Content = (mr.Content.EndsWith("\n") ? mr.Content.Substring(0, mr.Content.Length - 1) : mr.Content) +
                                                                          (g1 == null ? string.Empty : g1.Absent) +
                                                                          (g2 == null ? string.Empty : g2.Absent)
                                                                      }).Distinct().ToList();

                //gan gia tri thuoc tinh dinh kem 
                objResultResponse = IQFilterMarkRecordResponse;
            }
            else
            {
                //gan gia tri thuoc tinh dinh kem 
                objResultResponse = IQMarkRecordResponse;
            }
            //}

            //gan validationCode thanh cong


            return objResultResponse;
        }

        public List<PupilMarkBO> GetListMarkOfPeriod(int SchoolID, int ClassID, int AcademicYearID, int PeriodID)
        {
            if (AcademicYearID == 0)
            {
                return new List<PupilMarkBO>(); 

            }
            else if (PeriodID == 0)
            {
                return new List<PupilMarkBO>(); 
            }
            else
            {
                //Khoi tao Business
                //Khai bao bien
                int Semester = 0;//Hoc ky
                //Lay thong tin ten mon hoc cua lop
                IQueryable<ClassSubject> lstSubjectClass = ClassSubjectBusiness.SearchByClass(ClassID);
                List<SubjectCat> lstSubject = lstSubjectClass.Select(a => a.SubjectCat).Distinct().ToList();
                //lay thong tin dot nam trong hoc ky nao va nam hoc nao
                PeriodDeclaration objPeriodDeclaration = PeriodDeclarationBusiness.Find(PeriodID);
                Semester = objPeriodDeclaration.Semester.Value;
                AcademicYearID = objPeriodDeclaration.AcademicYearID;
                //Lay len danh sach hoc sinh cua lop
                List<PupilOfClassBO> IQPupilOfClass = PupilOfClassBusiness.GetPupilInClass(ClassID).ToList();
                //lay diem chi tiet trung binh cua cac mon
                List<SummedUpRecordBO> IQSummedUpRecord = (from s in SummedUpRecordBusiness.GetSummedUpRecord(AcademicYearID, SchoolID, Semester, ClassID, PeriodID)
                                                           join sc in SubjectCatBusiness.All on s.SubjectID equals sc.SubjectCatID
                                                           orderby sc.OrderInSubject
                                                           select s).ToList();
                //lay diem trung binh cua tat ca cac mon theo dot + lay hoc luc, hanh kiem, xep hang theo dot
                List<PupilOfClassRanking> IQPupilRanking = PupilRankingBusiness.GetPupilRankingOfClass(AcademicYearID, SchoolID, Semester, ClassID, PeriodID).ToList();
                //Fill vao Content
                //Loc nhung hoc sinh co diem  mon nhan xet phat sinh
                var lstMarkJudgeGroup = (from m in IQSummedUpRecord
                                         where m.JudgementResult != null
                                         orderby m.OrderInClass
                                         group m by m.PupilID into g
                                         select new
                                         {
                                             PupilProfileID = g.Key,
                                             JudgeContent = (g.Select(x => (x.SubjectName + ": " + x.JudgementResult.ToString() + " "))).Aggregate((a, b) => (a + "\n" + b))
                                         }).ToList();
                var lstMarkRecordPeriodGroup = (from m in IQSummedUpRecord
                                                where m.SummedUpMark != null
                                                orderby m.OrderInClass
                                                group m by m.PupilID into g
                                                select new
                                                {
                                                    PupilProfileID = g.Key,
                                                    MarkContent = (g.Select(x => (x.SubjectName + ": " + (x.SummedUpMark.HasValue ? x.SummedUpMark.Value.ToString("0.0") : x.SummedUpMark.ToString()) + " "))).Aggregate((a, b) => (a + "\n" + b))
                                                }).ToList();
                //Fill Diem thanh phan 
                List<PupilMarkBO> IQMarkOfPeriodResponse = (from p in IQPupilOfClass
                                                                    join j in lstMarkJudgeGroup on p.PupilID equals j.PupilProfileID into j1
                                                                    from g1 in j1.DefaultIfEmpty()
                                                                    join m in lstMarkRecordPeriodGroup on p.PupilID equals m.PupilProfileID into m1
                                                                    from g2 in m1.DefaultIfEmpty()
                                                                    select new PupilMarkBO
                                                                    {
                                                                        PupilProfileID = p.PupilID,
                                                                        Content = (g2 == null ? "" : g2.MarkContent) + (g2 != null && g2.MarkContent != "" ? "\n" : "") + (g1 == null ? "" : g1.JudgeContent)
                                                                    }).Distinct().ToList();
                //Fill hoc luc, hanh kiem, xep hang
                List<PupilMarkBO> IQFilterMarkOfPeriodResponse = (from mr in IQMarkOfPeriodResponse
                                                                          join pr in IQPupilRanking on mr.PupilProfileID equals pr.PupilID into g1
                                                                          from g2 in g1.DefaultIfEmpty()
                                                                          where mr.Content != ""
                                                                          select new PupilMarkBO
                                                                          {
                                                                              PupilProfileID = mr.PupilProfileID,
                                                                              Content = mr.Content +
                                                                                  //neu khong ton tai pupilranking
                                                                              (g2 == null ?
                                                                              string.Empty
                                                                              :
                                                                                  //Neu ton tai pupilranking                                                                                  
                                                                              (g2.AverageMark == null ? "" : "\nTrung bình các môn: " + (g2.AverageMark.HasValue ? g2.AverageMark.Value.ToString("0.0") : string.Empty)) +
                                                                              (g2.CapacityLevel == null ? "" : "\nHọc lực:" + g2.CapacityLevel) +
                                                                              (g2.ConductLevel == null ? "" : "\nHạnh kiểm: " + g2.ConductLevel) +
                                                                              (g2.Rank == null ? "" : "\nXếp hạng: " + g2.Rank.ToString()))
                                                                          }).ToList();
                return IQFilterMarkOfPeriodResponse;
            }
           
        }

        public List<PupilMarkBO> GetListTraningInfoByTime(int SchoolID, int ClassID, int AcademicYearID, DateTime startTime, DateTime endTime)
        {
            List<PupilMarkBO> listPupilTraining = new List<PupilMarkBO>();
            #region Thông tin điểm danh/ vi pham
            //lay len thong tin diem danh cua lop
            IDictionary<string, object> dicAbsent = new Dictionary<string, object>();
            dicAbsent["ClassID"] = ClassID;
            dicAbsent["AcademicYearID"] = AcademicYearID;
            // thong tin diem danh cua hoc sinh
            List<PupilAbsence> lstAbsence = PupilAbsenceBusiness.SearchBySchool(SchoolID, dicAbsent)
                .Where(p => EntityFunctions.TruncateTime(p.AbsentDate) >= EntityFunctions.TruncateTime(startTime)
                    && EntityFunctions.TruncateTime(p.AbsentDate) <= EntityFunctions.TruncateTime(endTime)).ToList();

            // thong tin vi pham cua hoc sinh
            var lstFault = (from f in PupilFaultBusiness.AllNoTracking
                            join lf in FaultCriteriaBusiness.AllNoTracking on f.FaultID equals lf.FaultCriteriaID
                            join fg in FaultGroupBusiness.AllNoTracking on lf.GroupID equals fg.FaultGroupID
                            where f.SchoolID == SchoolID
                            && f.AcademicYearID == AcademicYearID
                            && EntityFunctions.TruncateTime(f.ViolatedDate) >= EntityFunctions.TruncateTime(startTime)
                            && EntityFunctions.TruncateTime(f.ViolatedDate) <= EntityFunctions.TruncateTime(endTime)
                            select new 
                            {
                                PupilID = f.PupilID,
                                FaultID = f.FaultID,
                                NumberOfFault = f.NumberOfFault,
                                Resolution = lf.Resolution,
                                Note = f.Note
                            }).ToList();

            List<int> pupilIds = new List<int>();
            if (lstAbsence != null && lstAbsence.Count > 0)
            {
                pupilIds = lstAbsence.Select(p => p.PupilID).Distinct().ToList();
            }

            if (lstFault != null && lstFault.Count > 0)
            {
                pupilIds.AddRange(lstFault.Select(p => p.PupilID).Distinct().ToList());
                pupilIds = pupilIds.Distinct().ToList();
            }

            List<PupilAbsence> itemAbsences = null;

            //Tao content cho object
            int pupilID = 0;
            string content = string.Empty;
            int cp = 0, kp = 0;
            PupilMarkBO itemResponse = null;

            for (int i = pupilIds.Count - 1; i >= 0; i--)
            {
                content = string.Empty;
                itemResponse = new PupilMarkBO();
                pupilID = pupilIds[i];
                // diem danh
                itemAbsences = lstAbsence.Where(o => o.PupilID == pupilID).ToList();
                if (itemAbsences != null && itemAbsences.Count > 0)
                {
                    cp = itemAbsences.Where(p => p.IsAccepted == true).Count();
                    kp = itemAbsences.Where(p => p.IsAccepted != true).Count();
                    if (cp > 0)
                    {
                        content += string.Format(GlobalConstantsEdu.FORMAT_0_1, string.Format("Nghỉ có phép: {0}", cp), Environment.NewLine);
                    }

                    if (kp > 0)
                    {
                        content += string.Format(GlobalConstantsEdu.FORMAT_0_1, string.Format("Nghỉ không phép: {0}", kp), Environment.NewLine);
                    }
                }

                //Vi pham
                var itemFaults = lstFault.Where(a => a.PupilID == pupilID).GroupBy(l => new { l.FaultID, l.Resolution })
                        .Select(g => new
                        {
                            FaultID = g.Key.FaultID,
                            Resolution = g.Key.Resolution,
                            Note = string.Join(", ", g.Where(o => !string.IsNullOrWhiteSpace(o.Note))
                                .Select(x => x.Note).Distinct()),
                            Number = g.Sum(x => x.NumberOfFault)
                        }).ToList();


                if (itemFaults != null && itemFaults.Count > 0)
                {
                    for (int ik = 0, iksize = itemFaults.Count; ik < iksize; ik++)
                    {
                        var itemFaulst = itemFaults[ik];
                        if (itemFaulst.Number > 1)
                        {
                            content += string.Format("{0}{1} ({2}), ", string.IsNullOrEmpty(itemFaulst.Resolution) ? string.Empty : itemFaulst.Resolution, string.IsNullOrEmpty(itemFaulst.Note) ? string.Empty : " " + itemFaulst.Note, itemFaulst.Number);
                        }
                        else
                        {
                            content += string.Format("{0}{1}, ", string.IsNullOrEmpty(itemFaulst.Resolution) ? string.Empty : itemFaulst.Resolution, string.IsNullOrEmpty(itemFaulst.Note) ? string.Empty : " " + itemFaulst.Note);
                        }


                    }

                    // cat bo dau phay
                    content = content.Substring(0, content.Length - 2);
                }

                itemResponse.PupilProfileID = pupilID;
                itemResponse.Content = content;
                listPupilTraining.Add(itemResponse);
            }
            #endregion

            return listPupilTraining;
        }

        public List<EvaluationCommentsBO> GetCommentsVNEN(List<long> listPupilID, int schoolID, int academicYearID, int classID, int semesterID, int monthID)
        {
            AcademicYear academicYear = AcademicYearBusiness.Find(academicYearID);
            int partitionId = UtilsBusiness.GetPartionId(schoolID);
            List<PupilOfClassBO> pupilOfClassList = (from poc in PupilOfClassBusiness.GetPupilInClass(classID)
                                                     where listPupilID.Contains(poc.PupilID)
                                                     select poc).ToList();

            List<EvaluationCommentsBO> listResult = new List<EvaluationCommentsBO>();

            if (pupilOfClassList != null && pupilOfClassList.Count > 0)
            {
                EvaluationCommentsBO obj = null;
                PupilOfClassBO pupilOfClassBO = null;
                int pupilID = 0;
                StringBuilder monthComments = null, pupilContent = null;
                string MonthStr = string.Empty;

                if (monthID > 0 && monthID <= GlobalConstants.MonthID_12)
                {
                    int Year = academicYear.Year;
                    if (monthID <= 8 && semesterID == GlobalConstants.SEMESTER_OF_YEAR_SECOND) Year = academicYear.Year + 1;
                    #region Danh gia theo thang
                    MonthStr = string.Format("{0}{1}", Year, monthID);

                    int v_monthID = 0;
                    if (Int32.TryParse(MonthStr, out v_monthID))
                    {
                        List<TeacherNoteBookMonthBO> listNotesMonth = (from note in TeacherNoteBookMonthBusiness.All
                                                                       join sc in SubjectCatBusiness.All on note.SubjectID equals sc.SubjectCatID
                                                                       where note.SchoolID == schoolID && note.PartitionID == partitionId
                                                                           && note.AcademicYearID == academicYearID && note.ClassID == classID
                                                                           && note.MonthID == v_monthID
                                                                            && listPupilID.Contains(note.PupilID)
                                                                       orderby sc.OrderInSubject
                                                                       select new TeacherNoteBookMonthBO
                                                                       {
                                                                           PupilID = note.PupilID,
                                                                           SubjectID = note.SubjectID,
                                                                           SubjectName = sc.SubjectName,
                                                                           MonthID = note.MonthID,
                                                                           CommentCQ = note.CommentCQ,
                                                                           CommentSubject = note.CommentSubject
                                                                       }).ToList();
                        TeacherNoteBookMonthBO noteSubjectMonthBO = null;
                        for (int i = 0; i < pupilOfClassList.Count; i++)
                        {
                            pupilOfClassBO = pupilOfClassList[i];
                            pupilID = pupilOfClassBO.PupilID;
                            obj = new EvaluationCommentsBO();
                            obj.PupilID = pupilID;
                            obj.FullName = pupilOfClassBO.PupilFullName;
                            monthComments = new StringBuilder();

                            List<TeacherNoteBookMonthBO> listCommentsBypupilID = listNotesMonth.Where(p => p.PupilID == pupilID).ToList();
                            for (int j = 0; j < listCommentsBypupilID.Count; j++)
                            {
                                noteSubjectMonthBO = listCommentsBypupilID[j];
                                if (!string.IsNullOrEmpty(noteSubjectMonthBO.CommentSubject) || !string.IsNullOrEmpty(noteSubjectMonthBO.CommentCQ))
                                {
                                    monthComments.Append(j + 1).Append("/").Append(noteSubjectMonthBO.SubjectName).Append(":\r\n");
                                    if (!string.IsNullOrEmpty(noteSubjectMonthBO.CommentSubject)) monthComments.AppendLine(noteSubjectMonthBO.CommentSubject);
                                    if (!string.IsNullOrEmpty(noteSubjectMonthBO.CommentCQ)) monthComments.AppendLine(noteSubjectMonthBO.CommentCQ);
                                }

                            }
                            if (monthComments != null && monthComments.Length > 0)
                            {
                                obj.EvaluationMonth = monthComments.ToString();
                            }
                            listResult.Add(obj);
                        }
                    }
                    #endregion
                }
                else if (monthID == GlobalConstants.MonthID_EndSemester1 || monthID == GlobalConstants.MonthID_EndSemester2)
                {
                    #region Nhan xet cuoi ky
                    List<ReviewBookPupil> listReviewPupil = (from rv in ReviewBookPupilBusiness.All
                                                             where rv.SchoolID == schoolID && rv.PartitionID == partitionId
                                                                && rv.AcademicYearID == academicYearID && rv.ClassID == classID
                                                                && rv.SemesterID == semesterID
                                                             && listPupilID.Contains(rv.PupilID)
                                                             select rv).ToList();
                    ReviewBookPupil reviewPupil = null;
                    for (int i = 0; i < pupilOfClassList.Count; i++)
                    {
                        monthComments = new StringBuilder();
                        pupilOfClassBO = pupilOfClassList[i];
                        pupilID = pupilOfClassBO.PupilID;
                        obj = new EvaluationCommentsBO();
                        obj.PupilID = pupilID;
                        obj.FullName = pupilOfClassBO.PupilFullName;

                        // lay thong tin nhan xet (So tay hoc sinh)
                        reviewPupil = listReviewPupil.FirstOrDefault(p => p.PupilID == pupilID);
                        if (reviewPupil != null)
                        {
                            if (!string.IsNullOrEmpty(reviewPupil.CQComment)) monthComments.AppendLine(string.Format("Biểu hiện nổi bật: {0}", reviewPupil.CQComment));
                            if (!string.IsNullOrEmpty(reviewPupil.CommentAdd)) monthComments.AppendLine(string.Format("Điều cần khắc phục giúp đỡ, rèn luyện thêm: {0}", reviewPupil.CommentAdd));
                        }


                        if (monthComments != null && monthComments.Length > 0)
                        {
                            obj.EvaluationMonth = monthComments.ToString();
                        }
                        listResult.Add(obj);
                    }
                    #endregion
                }
                else
                {
                    #region Ket qua hoc ky
                    List<SubjectCatBO> lsClassSubject = new List<SubjectCatBO>();
                    if (classID > 0)
                    {
                        lsClassSubject = (from sb in ClassSubjectBusiness.All
                                          join sub in SubjectCatBusiness.All on sb.SubjectID equals sub.SubjectCatID
                                          orderby sb.IsSubjectVNEN descending, sb.IsCommenting, sub.OrderInSubject
                                          where sb.ClassID == classID
                                          && sub.IsActive == true
                                          && ((semesterID == GlobalConstants.SEMESTER_OF_YEAR_FIRST && sb.SectionPerWeekFirstSemester > 0)
                                          || (semesterID == GlobalConstants.SEMESTER_OF_YEAR_SECOND && sb.SectionPerWeekSecondSemester > 0))
                                          select new SubjectCatBO
                                          {
                                              SubjectCatID = sb.SubjectID,
                                              SubjectName = sub.SubjectName,
                                              IsCommenting = sb.IsCommenting,
                                              IsSubjectVNEN = sb.IsSubjectVNEN
                                          }).ToList();
                    }

                    // Danh sach danh gia So tay GV
                    List<TeacherNoteBookSemester> listNotesSem = (from tbs in TeacherNoteBookSemesterBusiness.All
                                                                  where tbs.SchoolID == schoolID && tbs.PartitionID == partitionId
                                                                   && tbs.AcademicYearID == academicYearID
                                                                   && tbs.ClassID == classID
                                                                   && tbs.SemesterID == semesterID
                                                                   && listPupilID.Contains(tbs.PupilID)
                                                                  select tbs).ToList();
                    // Thong tin theo TT58
                    List<SummedUpRecord> listSummedUpRecord = (from su in SummedUpRecordBusiness.All
                                                               where su.SchoolID == schoolID && su.Last2digitNumberSchool == partitionId
                                                                && su.AcademicYearID == academicYearID
                                                                && su.ClassID == classID
                                                                && ((semesterID == GlobalConstants.SEMESTER_OF_YEAR_SECOND && su.Semester == GlobalConstants.SEMESTER_OF_YEAR_ALL)
                                                                 || (semesterID == GlobalConstants.SEMESTER_OF_YEAR_FIRST && su.Semester == semesterID))
                                                                && su.PeriodID == null
                                                                && listPupilID.Contains(su.PupilID)
                                                               select su).ToList();
                    // Danh sach danh gia So tay HS
                    List<ReviewBookPupil> listReviewPupil = (from rv in ReviewBookPupilBusiness.All
                                                             where rv.SchoolID == schoolID && rv.PartitionID == partitionId
                                                                && rv.AcademicYearID == academicYearID && rv.ClassID == classID
                                                                && rv.SemesterID == semesterID
                                                             && listPupilID.Contains(rv.PupilID)
                                                             select rv).ToList();
                    // Danh sach khen thuong
                    //List<RewardCommentFinal> listRewardCommentsFinal = (from rf in RewardCommentFinalBusiness.All
                    //                                                       where rf.SchoolID == schoolID && rf.LastDigitSchoolID == partitionId
                    //                                                       && rf.AcademicYearID == academicYearID
                    //                                                       && rf.ClassID == classID
                    //                                                       && rf.SemesterID == semesterID
                    //                                                       && pupilIDList.Contains(rf.PupilID)
                    //                                                       select rf).ToList();
                    List<UpdateReward> listRewardComments = (from ur in UpdateRewardBusiness.All
                                                             where ur.SchoolID == schoolID && ur.PartitionID == partitionId
                                                                        && ur.AcademicYearID == academicYearID
                                                                        && ur.ClassID == classID
                                                                        && ur.SemesterID == semesterID
                                                                        && listPupilID.Contains(ur.PupilID)
                                                             select ur).ToList();
                    List<TeacherNoteBookSemester> lstNotesPerPupil = null;
                    TeacherNoteBookSemester objNotesPupil = null;
                    SubjectCatBO objSubjectBO = null;
                    SummedUpRecord objSummedUpRecord = null;
                    ReviewBookPupil objReviewPupil = null;
                    for (int i = 0; i < pupilOfClassList.Count; i++)
                    {
                        pupilOfClassBO = pupilOfClassList[i];
                        pupilID = pupilOfClassBO.PupilID;
                        obj = new EvaluationCommentsBO();
                        obj.PupilID = pupilID;
                        obj.FullName = pupilOfClassBO.PupilFullName;
                        monthComments = new StringBuilder();
                        // Danh gia mon hoc
                        lstNotesPerPupil = listNotesSem.Where(p => p.PupilID == pupilID).ToList();
                        for (int j = 0; j < lsClassSubject.Count; j++)
                        {
                            objSubjectBO = lsClassSubject[j];
                            pupilContent = new StringBuilder();

                            if (objSubjectBO.IsSubjectVNEN == true)
                            {
                                objNotesPupil = lstNotesPerPupil.FirstOrDefault(o => o.SubjectID == objSubjectBO.SubjectCatID);
                                if (objNotesPupil != null)
                                {
                                    if (objNotesPupil.Rate == GlobalConstants.REVIEW_TYPE_FINISH) pupilContent.Append(GlobalConstants.SHORTFINISH).Append(", ");
                                    else if (objNotesPupil.Rate == GlobalConstants.REVIEW_TYPE_NOT_FINISH) pupilContent.Append(GlobalConstants.SHORTNOTFINISH).Append(", ");

                                    if (objSubjectBO.IsCommenting == GlobalConstants.SUBJECTCAT_ISCOMMENTING_MARK && objNotesPupil.AVERAGE_MARK.HasValue) pupilContent.Append(objNotesPupil.AVERAGE_MARK.Value.ToString("#.#")).Append("; ");
                                    else if (objSubjectBO.IsCommenting == GlobalConstants.SUBJECTCAT_ISCOMMENTING_COMMENT && !String.IsNullOrEmpty(objNotesPupil.AVERAGE_MARK_JUDGE)) pupilContent.Append(objNotesPupil.AVERAGE_MARK_JUDGE).Append("; ");
                                }
                            }
                            else
                            {
                                objSummedUpRecord = listSummedUpRecord.FirstOrDefault(o => o.PupilID == pupilID && o.SubjectID == objSubjectBO.SubjectCatID);
                                if (objSummedUpRecord != null)
                                {
                                    if (objSubjectBO.IsCommenting == GlobalConstants.SUBJECTCAT_ISCOMMENTING_MARK && objSummedUpRecord.SummedUpMark.HasValue) pupilContent.Append(objSummedUpRecord.SummedUpMark.Value.ToString("#.#")).Append("; ");
                                    else if (objSubjectBO.IsCommenting == GlobalConstants.SUBJECTCAT_ISCOMMENTING_COMMENT && !String.IsNullOrEmpty(objSummedUpRecord.JudgementResult)) pupilContent.Append(objSummedUpRecord.JudgementResult).Append("; ");
                                }
                            }

                            if (pupilContent.Length > 0) monthComments.Append(objSubjectBO.SubjectName).Append(": ").Append(pupilContent.ToString());
                        }

                        if (monthComments.Length > 0) monthComments.Append("\r\n");

                        // Nang luc, pham chat
                        objReviewPupil = listReviewPupil.FirstOrDefault(o => o.PupilID == pupilID);
                        if (objReviewPupil != null)
                        {
                            string strNL = string.Empty;

                            if (objReviewPupil.CapacityRate == 1)
                            {
                                strNL = "T";
                            }
                            else if (objReviewPupil.CapacityRate == 2)
                            {
                                strNL = "Đ";
                            }
                            else if (objReviewPupil.CapacityRate == 3)
                            {
                                strNL = "C";
                            }

                            if (!string.IsNullOrEmpty(strNL))
                            {
                                monthComments.AppendLine("Năng lực: " + strNL);
                            }

                            string strPC = string.Empty;

                            if (objReviewPupil.QualityRate == 1)
                            {
                                strPC = "T";
                            }
                            else if (objReviewPupil.QualityRate == 2)
                            {
                                strPC = "Đ";
                            }
                            else if (objReviewPupil.QualityRate == 3)
                            {
                                strPC = "C";
                            }

                            if (!string.IsNullOrEmpty(strPC))
                            {
                                monthComments.AppendLine("Phẩm chất: " + strPC);
                            }

                        }

                        // Khen thuong
                        string strRewardID = listRewardComments.Where(p => p.PupilID == pupilID).Select(p => p.Rewards).FirstOrDefault();
                        if (!string.IsNullOrEmpty(strRewardID))
                        {
                            List<int> listInt = strRewardID.Split(new[] { "," }, StringSplitOptions.RemoveEmptyEntries).Select(p => int.Parse(p)).ToList();
                            List<RewardFinal> rewardFinalList = RewardFinalBusiness.All.Where(p => p.SchoolID == schoolID).ToList();
                            string rewardMode = string.Empty;
                            pupilContent = new StringBuilder();
                            for (int g = 0; g < listInt.Count; g++)
                            {
                                rewardMode = rewardFinalList.Where(p => p.RewardFinalID == listInt[g]).Select(p => p.RewardMode).FirstOrDefault();
                                if (!string.IsNullOrWhiteSpace(rewardMode))
                                {
                                    pupilContent.Append(rewardMode);
                                    if (g != listInt.Count - 1)
                                    {
                                        pupilContent.Append("; ");
                                    }
                                }
                            }
                            if (pupilContent.Length > 0) monthComments.Append("Khen thưởng: ").AppendLine(pupilContent.ToString());
                        }

                        // Danh gia cuoi nam
                        if (semesterID == GlobalConstants.SEMESTER_OF_YEAR_SECOND)
                        {
                            if (objReviewPupil != null)
                            {
                                if (objReviewPupil.RateAndYear == GlobalConstants.REVIEW_TYPE_FINISH)
                                {
                                    monthComments.AppendLine("Được lên lớp");
                                }
                                else if (objReviewPupil.RateAndYear == GlobalConstants.REVIEW_TYPE_NOT_FINISH)
                                {
                                    if (!objReviewPupil.RateAdd.HasValue) monthComments.AppendLine("Chưa hoàn thành chương trình lớp học");
                                    else if (objReviewPupil.RateAdd == GlobalConstants.REVIEW_TYPE_FINISH) monthComments.AppendLine("Được lên lớp");
                                    else if (objReviewPupil.RateAdd == GlobalConstants.REVIEW_TYPE_NOT_FINISH) monthComments.AppendLine("Ở lại lớp");
                                }
                            }
                        }

                        if (monthComments != null && monthComments.Length > 0)
                        {
                            obj.EvaluationMonth = monthComments.ToString();
                        }
                        listResult.Add(obj);
                    }
                    #endregion
                }
            }

            return listResult;
        }

        public List<RatedCommentPupilBO> GetListRatedCommentPupil(List<long> pupilIDList, int schoolID, int academicYearID, int classID, int type, int semesterID)
        {
            IDictionary<string, object> dic;

            //list hoc sinh trong lop
            List<PupilOfClassBO> lstPupilOfClass = (from poc in PupilOfClassBusiness.GetPupilInClass(classID)
                                                    where pupilIDList.Contains(poc.PupilID)
                                                    select poc)
                                                     .OrderBy(c => c.OrderInClass)
                                                     .ThenBy(c => c.Name)
                                                     .ThenBy(c => c.PupilFullName).ToList();

            //list mon hoc
            List<SubjectCatBO> lstClassSubject = new List<SubjectCatBO>();
            if (classID > 0)
            {
                lstClassSubject = (from sb in ClassSubjectBusiness.All
                                   join sub in SubjectCatBusiness.All on sb.SubjectID equals sub.SubjectCatID
                                   orderby sb.IsCommenting, sub.OrderInSubject
                                   where sb.ClassID == classID
                                   && sub.IsActive == true
                                   && ((semesterID == GlobalConstants.SEMESTER_OF_YEAR_FIRST && sb.SectionPerWeekFirstSemester > 0)
                                   || (semesterID == GlobalConstants.SEMESTER_OF_YEAR_SECOND && sb.SectionPerWeekSecondSemester > 0))
                                   select new SubjectCatBO
                                   {
                                       SubjectCatID = sb.SubjectID,
                                       SubjectName = sub.SubjectName,
                                       IsCommenting = sb.IsCommenting
                                   }
                                 ).ToList();
            }

            //Khen thuong 
            dic = new Dictionary<string, object>()
                {
                    {"SchoolID",schoolID},
                    {"AcademicYearID",academicYearID},
                    {"lstClassID",new List<int>{classID}},
                    {"Semester", GlobalConstants.SEMESTER_OF_EVALUATION_RESULT}
                };
            List<SummedEndingEvaluationBO> lstSEE = SummedEndingEvaluationBusiness.Search(dic).ToList();

            dic = new Dictionary<string, object>()
                {
                    {"SchoolID",schoolID},
                    {"AcademicYearID",academicYearID},
                    {"lstClassID",new List<int>{classID}},
                    {"SemesterID", semesterID},
                    {"lstPupilID", pupilIDList.ConvertAll(i => (int)i)},

                };

            List<EvaluationReward> lstEvaluationReward = EvaluationRewardBusiness.Search(dic).ToList();

            //List loai nang luc va pham chat
            List<EvaluationCriteria> lstEc = EvaluationCriteriaBusiness.All.ToList();
            List<EvaluationCriteria> lstEcNL = lstEc.Where(o => o.TypeID == 2).OrderBy(o => o.EvaluationCriteriaID).ToList();
            List<EvaluationCriteria> lstEcPC = lstEc.Where(o => o.TypeID == 3).OrderBy(o => o.EvaluationCriteriaID).ToList();

            List<RatedCommentPupilBO> lstData = new List<RatedCommentPupilBO>();
            RatedCommentPupilBO objData;

            dic = new Dictionary<string, object>();
            dic["SchoolID"] = schoolID;
            dic["AcademicYearID"] = academicYearID;
            dic["ClassID"] = classID;
            dic["SemesterID"] = semesterID;

            AcademicYear objAy = AcademicYearBusiness.Find(academicYearID);

            List<RatedCommentPupilBO> lstRatedCommentAll;
            if (UtilsBusiness.IsMoveHistory(objAy))
            {
                lstRatedCommentAll = RatedCommentPupilHistoryBusiness.Search(dic)
                    .Select(o => new RatedCommentPupilBO
                    {
                        CapacityEndingEvaluation = o.CapacityEndingEvaluation,
                        CapacityMiddleEvaluation = o.CapacityMiddleEvaluation,
                        Comment = o.Comment,
                        EndingEvaluation = o.EndingEvaluation,
                        EvaluationID = o.EvaluationID,
                        MiddleEvaluation = o.MiddleEvaluation,
                        PeriodicEndingJudgement = o.PeriodicEndingJudgement,
                        PeriodicEndingMark = o.PeriodicEndingMark,
                        PeriodicMiddleJudgement = o.PeriodicMiddleJudgement,
                        PeriodicMiddleMark = o.PeriodicMiddleMark,
                        PupilID = o.PupilID,
                        QualityEndingEvaluation = o.QualityEndingEvaluation,
                        QualityMiddleEvaluation = o.QualityMiddleEvaluation,
                        RetestMark = o.RetestMark,
                        SemesterID = o.SemesterID,
                        SubjectID = o.SubjectID

                    }).ToList();
            }
            else
            {
                lstRatedCommentAll = RatedCommentPupilBusiness.Search(dic)
                    .Select(o => new RatedCommentPupilBO
                    {
                        CapacityEndingEvaluation = o.CapacityEndingEvaluation,
                        CapacityMiddleEvaluation = o.CapacityMiddleEvaluation,
                        Comment = o.Comment,
                        EndingEvaluation = o.EndingEvaluation,
                        EvaluationID = o.EvaluationID,
                        MiddleEvaluation = o.MiddleEvaluation,
                        PeriodicEndingJudgement = o.PeriodicEndingJudgement,
                        PeriodicEndingMark = o.PeriodicEndingMark,
                        PeriodicMiddleJudgement = o.PeriodicMiddleJudgement,
                        PeriodicMiddleMark = o.PeriodicMiddleMark,
                        PupilID = o.PupilID,
                        QualityEndingEvaluation = o.QualityEndingEvaluation,
                        QualityMiddleEvaluation = o.QualityMiddleEvaluation,
                        RetestMark = o.RetestMark,
                        SemesterID = o.SemesterID,
                        SubjectID = o.SubjectID

                    }).ToList();
            }

            for (int i = 0; i < lstPupilOfClass.Count; i++)
            {
                PupilOfClassBO poc = lstPupilOfClass[i];
                objData = new RatedCommentPupilBO();
                objData.PupilID = poc.PupilID;

                List<RatedCommentPupilBO> lstRatedCommentPupil = lstRatedCommentAll.Where(o => o.PupilID == poc.PupilID).ToList();
                StringBuilder sb = new StringBuilder();

                string tempNL;
                string tempPC;
                string tempKT;
                //Tong hop ket qua
                switch (type)
                {
                    //Ket qua giua ky
                    case 1:
                        //Mon hoc & HDGD
                        for (int j = 0; j < lstClassSubject.Count; j++)
                        {
                            SubjectCatBO subject = lstClassSubject[j];

                            RatedCommentPupilBO ratedCommentSubject = lstRatedCommentPupil
                                .Where(o => o.EvaluationID == 1 && o.SubjectID == subject.SubjectCatID).FirstOrDefault();

                            if (ratedCommentSubject != null)
                            {
                                string evaluation = ratedCommentSubject.MiddleEvaluation == 1 ? "T"
                                    : (ratedCommentSubject.MiddleEvaluation == 2 ? "H" :
                                    (ratedCommentSubject.MiddleEvaluation == 3 ? "C" : string.Empty));

                                string mark;

                                if (subject.IsCommenting.HasValue && subject.IsCommenting == 1)
                                {
                                    mark = ratedCommentSubject.PeriodicMiddleJudgement;
                                }
                                else
                                {
                                    mark = ratedCommentSubject.PeriodicMiddleMark.HasValue ?
                                        ratedCommentSubject.PeriodicMiddleMark.Value.ToString() : string.Empty;
                                }

                                if (!string.IsNullOrEmpty(evaluation) || !string.IsNullOrEmpty(mark))
                                {
                                    sb.Append(subject.SubjectName);
                                    sb.Append(": ");
                                    if (!string.IsNullOrEmpty(evaluation))
                                    {
                                        sb.Append(evaluation);
                                    }

                                    if (!string.IsNullOrEmpty(evaluation) && !string.IsNullOrEmpty(mark))
                                    {
                                        sb.Append(",");
                                    }

                                    if (!string.IsNullOrEmpty(mark))
                                    {
                                        sb.Append(mark);
                                    }

                                    sb.Append(";");
                                }
                            }
                        }
                        sb.Append("\n");

                        //Nang luc
                        tempNL = string.Empty;
                        for (int j = 0; j < lstEcNL.Count; j++)
                        {
                            EvaluationCriteria ec = lstEcNL[j];

                            RatedCommentPupilBO ratedCommentSubject = lstRatedCommentPupil
                                .Where(o => o.EvaluationID == 2 && o.SubjectID == ec.EvaluationCriteriaID).FirstOrDefault();

                            if (ratedCommentSubject != null)
                            {
                                string evaluation = ratedCommentSubject.CapacityMiddleEvaluation == 1 ? "T"
                                    : (ratedCommentSubject.CapacityMiddleEvaluation == 2 ? "Đ" :
                                    (ratedCommentSubject.CapacityMiddleEvaluation == 3 ? "C" : string.Empty));

                                if (!string.IsNullOrEmpty(evaluation))
                                {
                                    tempNL += evaluation;
                                    tempNL += ";";
                                }
                            }
                        }
                        if (!string.IsNullOrWhiteSpace(tempNL))
                        {
                            sb.Append("Năng lực: ");
                            sb.Append(tempNL);
                            sb.Append("\n");
                        }

                        //Pham chat
                        tempPC = string.Empty;
                        for (int j = 0; j < lstEcPC.Count; j++)
                        {
                            EvaluationCriteria ec = lstEcPC[j];

                            RatedCommentPupilBO ratedCommentSubject = lstRatedCommentPupil
                                .Where(o => o.EvaluationID == 3 && o.SubjectID == ec.EvaluationCriteriaID).FirstOrDefault();

                            if (ratedCommentSubject != null)
                            {
                                string evaluation = ratedCommentSubject.QualityMiddleEvaluation == 1 ? "T"
                                    : (ratedCommentSubject.QualityMiddleEvaluation == 2 ? "Đ" :
                                    (ratedCommentSubject.QualityMiddleEvaluation == 3 ? "C" : string.Empty));

                                if (!string.IsNullOrEmpty(evaluation))
                                {
                                    tempPC += evaluation;
                                    tempPC += ";";
                                }
                            }
                        }

                        if (!string.IsNullOrWhiteSpace(tempPC))
                        {
                            sb.Append("Phẩm chất: ");
                            sb.Append(tempPC);
                            sb.Append("\n");
                        }

                        break;
                    //Ket qua cuoi ky
                    case 2:
                        //Mon hoc & HDGD
                        for (int j = 0; j < lstClassSubject.Count; j++)
                        {
                            SubjectCatBO subject = lstClassSubject[j];

                            RatedCommentPupilBO ratedCommentSubject = lstRatedCommentPupil
                                .Where(o => o.EvaluationID == 1 && o.SubjectID == subject.SubjectCatID).FirstOrDefault();

                            if (ratedCommentSubject != null)
                            {
                                string evaluation = ratedCommentSubject.EndingEvaluation == 1 ? "T"
                                    : (ratedCommentSubject.EndingEvaluation == 2 ? "H" :
                                    (ratedCommentSubject.EndingEvaluation == 3 ? "C" : string.Empty));

                                string mark;

                                if (subject.IsCommenting.HasValue && subject.IsCommenting == 1)
                                {
                                    mark = ratedCommentSubject.PeriodicEndingJudgement;
                                }
                                else
                                {
                                    mark = ratedCommentSubject.PeriodicEndingMark.HasValue ?
                                        ratedCommentSubject.PeriodicEndingMark.Value.ToString() : string.Empty;
                                }

                                if (!string.IsNullOrEmpty(evaluation) || !string.IsNullOrEmpty(mark))
                                {
                                    sb.Append(subject.SubjectName);
                                    sb.Append(": ");
                                    if (!string.IsNullOrEmpty(evaluation))
                                    {
                                        sb.Append(evaluation);
                                    }

                                    if (!string.IsNullOrEmpty(evaluation) && !string.IsNullOrEmpty(mark))
                                    {
                                        sb.Append(",");
                                    }

                                    if (!string.IsNullOrEmpty(mark))
                                    {
                                        sb.Append(mark);
                                    }

                                    sb.Append(";");
                                }
                            }
                        }
                        sb.Append("\n");

                        //Nang luc

                        tempNL = string.Empty;
                        for (int j = 0; j < lstEcNL.Count; j++)
                        {
                            EvaluationCriteria ec = lstEcNL[j];

                            RatedCommentPupilBO ratedCommentSubject = lstRatedCommentPupil
                                .Where(o => o.EvaluationID == 2 && o.SubjectID == ec.EvaluationCriteriaID).FirstOrDefault();

                            if (ratedCommentSubject != null)
                            {
                                string evaluation = ratedCommentSubject.CapacityEndingEvaluation == 1 ? "T"
                                    : (ratedCommentSubject.CapacityEndingEvaluation == 2 ? "Đ" :
                                    (ratedCommentSubject.CapacityEndingEvaluation == 3 ? "C" : string.Empty));

                                if (!string.IsNullOrEmpty(evaluation))
                                {
                                    tempNL += evaluation;
                                    tempNL += ";";
                                }
                            }
                        }

                        if (!string.IsNullOrWhiteSpace(tempNL))
                        {
                            sb.Append("Năng lực: ");
                            sb.Append(tempNL);
                            sb.Append("\n");
                        }

                        //Pham chat

                        tempPC = string.Empty;
                        for (int j = 0; j < lstEcPC.Count; j++)
                        {
                            EvaluationCriteria ec = lstEcPC[j];

                            RatedCommentPupilBO ratedCommentSubject = lstRatedCommentPupil
                                .Where(o => o.EvaluationID == 3 && o.SubjectID == ec.EvaluationCriteriaID).FirstOrDefault();

                            if (ratedCommentSubject != null)
                            {
                                string evaluation = ratedCommentSubject.QualityEndingEvaluation == 1 ? "T"
                                    : (ratedCommentSubject.QualityEndingEvaluation == 2 ? "Đ" :
                                    (ratedCommentSubject.QualityEndingEvaluation == 3 ? "C" : string.Empty));

                                if (!string.IsNullOrEmpty(evaluation))
                                {
                                    tempPC += evaluation;
                                    tempPC += ";";
                                }
                            }
                        }

                        if (!string.IsNullOrWhiteSpace(tempPC))
                        {
                            sb.Append("Phẩm chất: ");
                            sb.Append(tempPC);
                            sb.Append("\n");
                        }

                        //Khen thuong

                        tempKT = string.Empty;
                        SummedEndingEvaluationBO see = lstSEE.Where(o => o.PupilID == poc.PupilID && semesterID == GlobalConstants.SEMESTER_OF_YEAR_SECOND && o.RewardID != null && o.RewardID != 0).FirstOrDefault();
                        if (see != null)
                        {
                            tempKT += "Học sinh xuất sắc";
                            tempKT += ";";
                        }

                        List<EvaluationReward> lstEvaluationRewardPupil = lstEvaluationReward.Where(o => o.PupilID == poc.PupilID).ToList();

                        for (int j = 0; j < lstEvaluationRewardPupil.Count; j++)
                        {
                            EvaluationReward er = lstEvaluationRewardPupil[j];
                            string rewardName = er.RewardID == 1 || er.RewardID == 2 ? er.Content : string.Empty;
                            if (!string.IsNullOrEmpty(rewardName))
                            {
                                tempKT += rewardName;
                                tempKT += ";";
                            }
                        }

                        if (!string.IsNullOrWhiteSpace(tempKT))
                        {
                            sb.Append("Khen thưởng: ");
                            sb.Append(tempKT);
                            sb.Append("\n");
                        }

                        if (semesterID == 2)
                        {
                            SummedEndingEvaluationBO objSee = lstSEE.Where(o => o.PupilID == poc.PupilID).FirstOrDefault();

                            if (objSee != null)
                            {

                                string evaluation = string.Empty;
                                if (objSee.EndingEvaluation == "HT")
                                {
                                    evaluation = "Được lên lớp";
                                }
                                else if (objSee.EndingEvaluation == "CHT" && objSee.RateAdd != 0 && objSee.RateAdd != 1)
                                {
                                    evaluation = "Chưa hoàn thành chương trình lớp học";
                                }
                                else if (objSee.EndingEvaluation == "CHT" && objSee.RateAdd == 1)
                                {
                                    evaluation = "Được lên lớp";
                                }
                                else if (objSee.EndingEvaluation == "CHT" && objSee.RateAdd == 0)
                                {
                                    evaluation = "Ở lại lớp";
                                }

                                sb.Append(evaluation);
                            }
                        }

                        break;
                    //Nhat xet cuoi ky
                    case 3:
                        //Mon hoc & HDGD
                        for (int j = 0; j < lstClassSubject.Count; j++)
                        {
                            SubjectCatBO subject = lstClassSubject[j];

                            RatedCommentPupilBO ratedCommentSubject = lstRatedCommentPupil
                                .Where(o => o.EvaluationID == 1 && o.SubjectID == subject.SubjectCatID).FirstOrDefault();

                            if (ratedCommentSubject != null)
                            {
                                string comment = ratedCommentSubject.Comment;

                                if (!string.IsNullOrEmpty(comment))
                                {
                                    sb.Append(subject.SubjectName);
                                    sb.Append(": ");
                                    sb.Append(comment);
                                    sb.Append(";");
                                }
                            }
                        }
                        sb.Append("\n");

                        //Nang luc

                        tempNL = string.Empty;

                        RatedCommentPupilBO objRatedComment = lstRatedCommentPupil
                            .Where(o => o.EvaluationID == 2).FirstOrDefault();

                        if (objRatedComment != null)
                        {
                            string comment = objRatedComment.Comment;

                            if (!string.IsNullOrEmpty(comment))
                            {
                                tempNL += comment;
                                tempNL += ";";
                            }
                        }

                        if (!string.IsNullOrWhiteSpace(tempNL))
                        {
                            sb.Append("Năng lực: ");
                            sb.Append(tempNL);
                            sb.Append("\n");
                        }

                        //Pham chat
                        tempPC = string.Empty;

                        objRatedComment = lstRatedCommentPupil
                            .Where(o => o.EvaluationID == 3).FirstOrDefault();

                        if (objRatedComment != null)
                        {
                            string comment = objRatedComment.Comment;

                            if (!string.IsNullOrEmpty(comment))
                            {
                                tempPC += comment;
                                tempPC += ";";
                            }
                        }


                        if (!string.IsNullOrWhiteSpace(tempPC))
                        {
                            sb.Append("Phẩm chất: ");
                            sb.Append(tempPC);
                        }
                        break;
                }

                objData.SMSContent = sb.ToString().Trim();
                lstData.Add(objData);
            }

            return lstData;

        }

        public List<ExamMarkSemesterBO> GetListExamMarkSemester(List<int> lstPupilID, int SchoolID, int ClassID, int AcademicYearID, int Semester)
        {
            List<ExamMarkSemesterBO> lstResult = new List<ExamMarkSemesterBO>();
            if (Semester == GlobalConstants.SEMESTER_OF_YEAR_ALL)
            {
                return new List<ExamMarkSemesterBO>();
            }
            else
            {
                int grade = ClassProfileBusiness.Find(ClassID).EducationLevel.Grade;
                #region Danh sach mon hoc cua lop
                List<ClassSubjectBO> listClassSubject = ClassSubjectBusiness.SearchByClass(ClassID, new Dictionary<string, object>() { { "Semester", Semester } }).Select(o => new ClassSubjectBO
                {
                    SubjectID = o.SubjectID,
                    DisplayName = o.SubjectCat.DisplayName,
                    IsCommenting = o.IsCommenting,
                    OrderInSubject = o.SubjectCat.OrderInSubject
                }).OrderBy(p => p.IsCommenting.HasValue ? p.IsCommenting.Value : 0).ThenBy(o => o.OrderInSubject).ThenBy(o => o.DisplayName).ToList();
                #endregion

                List<ExamMarkSemesterBO> LstExamMarkSemseter = new List<ExamMarkSemesterBO>();
                ExamMarkSemesterBO examMarkSemesterObj = null;
                int PupilID = 0;
                StringBuilder Content = null;
                ClassSubjectBO classSubjectObj = null;
                if (grade != GlobalConstants.PRIMARY_EDUCATION_SCHOOL)
                {
                    #region Lay diem cap 2,3
                    List<int> listMarkSubjectID = listClassSubject.Where(p => p.IsCommenting == GlobalConstants.ISCOMMENTING_TYPE_MARK).Select(p => p.SubjectID).ToList();
                    List<int> listJudgeSubjectID = listClassSubject.Where(p => p.IsCommenting == GlobalConstants.ISCOMMENTING_TYPE_JUDGE).Select(p => p.SubjectID).ToList();

                    Dictionary<string, object> dic = new Dictionary<string, object>();
                    dic["AcademicYearID"] = AcademicYearID;
                    dic["ClassID"] = ClassID;
                    dic["Semester"] = Semester;
                    dic["Title"] = GlobalConstants.MARK_TYPE_HK;
                    dic["ListPupilID"] = lstPupilID;

                    //Lay diem thi theo mon tinh diem
                    List<VMarkRecord> lstMark = VMarkRecordBusiness.SearchBySchool(SchoolID, dic).Where(m => listMarkSubjectID.Contains(m.SubjectID)).ToList();
                    //Lay diem mon nhan xet
                    List<VJudgeRecord> lstJudge = VJudgeRecordBusiness.SearchBySchool(SchoolID, dic).Where(m => listJudgeSubjectID.Contains(m.SubjectID)).ToList();

                    List<VMarkRecord> lstMarkRecordWithPupil = null;
                    List<VJudgeRecord> lstJudgeRecordWithPupil = null;
                    List<VMarkRecord> lstMarkRecordWithSubject = null;
                    List<VJudgeRecord> lstJudgeRecordWithSubject = null;

                    for (int k = 0, num = lstPupilID.Count; k < num; k++)
                    {
                        PupilID = lstPupilID[k];
                        examMarkSemesterObj = new ExamMarkSemesterBO();
                        Content = new StringBuilder();
                        examMarkSemesterObj.PupilID = PupilID;
                        lstMarkRecordWithPupil = lstMark.Where(p => p.PupilID == PupilID).ToList();
                        lstJudgeRecordWithPupil = lstJudge.Where(p => p.PupilID == PupilID).ToList();
                        for (int i = 0; i < listClassSubject.Count; i++)
                        {
                            classSubjectObj = listClassSubject[i];

                            lstMarkRecordWithSubject = lstMarkRecordWithPupil.Where(p => p.SubjectID == classSubjectObj.SubjectID).ToList();
                            if (lstMarkRecordWithSubject != null && lstMarkRecordWithSubject.Count > 0)
                            {
                                foreach (var item in lstMarkRecordWithSubject)
                                    Content.Append(classSubjectObj.DisplayName).Append(":").Append(string.Format("{0:0.#}", item.Mark)).Append("\n");
                            }

                            lstJudgeRecordWithSubject = lstJudgeRecordWithPupil.Where(p => p.SubjectID == classSubjectObj.SubjectID).ToList();
                            if (lstJudgeRecordWithSubject != null && lstJudgeRecordWithSubject.Count > 0)
                            {
                                foreach (var item in lstJudgeRecordWithSubject)
                                    Content.Append(classSubjectObj.DisplayName).Append(":").Append(item.Judgement).Append("\n");
                            }
                        }
                        if (Content.Length > 0) examMarkSemesterObj.MarkContent = Content.ToString();
                        LstExamMarkSemseter.Add(examMarkSemesterObj);
                    }

                    lstResult = LstExamMarkSemseter;
                    #endregion
                }
                else
                {
                    #region lay diem cap 1
                    // chi lay nhung mon tinh diem
                    SummedEvaluation summedEvaluationObj = null;
                    int partition = UtilsBusiness.GetPartionId(SchoolID);
                    listClassSubject = listClassSubject.Where(p => p.IsCommenting == GlobalConstants.ISCOMMENTING_TYPE_MARK).ToList();
                    List<int> listSubjectID = listClassSubject.Select(p => p.SubjectID).ToList();

                    List<SummedEvaluation> lstSummedEvaluation = (from m in SummedEvaluationBusiness.All
                                                                  where m.LastDigitSchoolID == partition
                                                                   && m.AcademicYearID == AcademicYearID
                                                                   && m.SchoolID == SchoolID
                                                                   && m.ClassID == ClassID
                                                                   && m.SemesterID == Semester
                                                                   && listSubjectID.Contains(m.EvaluationCriteriaID)
                                                                   && lstPupilID.Contains(m.PupilID)
                                                                  select m).ToList();
                    List<SummedEvaluation> lstSummedEvaluationPupil = null;
                    List<SummedEvaluation> lstSummedEvaluationSub = null;
                    for (int k = 0, num = lstPupilID.Count; k < num; k++)
                    {
                        PupilID = lstPupilID[k];
                        lstSummedEvaluationPupil = lstSummedEvaluation.Where(p => p.PupilID == PupilID).ToList();
                        examMarkSemesterObj = new ExamMarkSemesterBO();
                        Content = new StringBuilder();
                        examMarkSemesterObj.PupilID = PupilID;

                        for (int i = 0; i < listClassSubject.Count; i++)
                        {
                            classSubjectObj = listClassSubject[i];
                            if (lstSummedEvaluationPupil != null && lstSummedEvaluationPupil.Count > 0)
                            {
                                lstSummedEvaluationSub = lstSummedEvaluationPupil.Where(p => p.EvaluationCriteriaID == classSubjectObj.SubjectID).ToList();
                                if (lstSummedEvaluationSub != null && lstSummedEvaluationSub.Count > 0)
                                {
                                    foreach (var item in lstSummedEvaluationSub)
                                        Content.Append(classSubjectObj.DisplayName).Append(":").Append(string.Format("{0:0.#}", summedEvaluationObj.PeriodicEndingMark)).Append("\n");
                                }
                            }
                        }
                        if (Content.Length > 0) examMarkSemesterObj.MarkContent = Content.ToString();
                        LstExamMarkSemseter.Add(examMarkSemesterObj);
                    }
                    lstResult = LstExamMarkSemseter;

                    #endregion
                }

                return lstResult;
            }
        }

        public List<ExamSubjectBO> getExamScheduleInfo(List<long> pupilIDList, int schoolID, int academicYearID, int classID, int semester, int examID)
        {
            List<PupilOfClassBO> pupilOfClassList = (from poc in PupilOfClassBusiness.GetPupilInClass(classID)
                                                     where pupilIDList.Contains(poc.PupilID)
                                                     select poc).ToList();

            List<ExamSubjectBO> listResult = new List<ExamSubjectBO>();
            ExamSubjectBO obj;
            if (pupilOfClassList != null && pupilOfClassList.Count > 0)
            {
                int partitionId = UtilsBusiness.GetPartionId(schoolID);
                PupilOfClassBO pupilOfClassBO = null;
                int pupilID = 0;
                StringBuilder pupilContent = null;

                IQueryable<ExamPupilBO> iqExamPupil = (from ex in ExaminationsBusiness.All
                                                       join eg in ExamGroupBusiness.All on ex.ExaminationsID equals eg.ExaminationsID
                                                       join ep in ExamPupilBusiness.All on new { ex.ExaminationsID, eg.ExamGroupID } equals new { ep.ExaminationsID, ep.ExamGroupID }
                                                       join es in ExamSubjectBusiness.All on new { ex.ExaminationsID, eg.ExamGroupID } equals new { es.ExaminationsID, es.ExamGroupID }
                                                       join sc in SubjectCatBusiness.All on es.SubjectID equals sc.SubjectCatID
                                                       where ex.SchoolID == schoolID && ep.LastDigitSchoolID == partitionId
                                                       && ex.AcademicYearID == academicYearID
                                                       && pupilIDList.Contains(ep.PupilID)
                                                       && ex.ExaminationsID == examID
                                                       orderby sc.OrderInSubject
                                                       select new ExamPupilBO
                                                       {
                                                           ExaminationsID = ex.ExaminationsID,
                                                           ExaminationName = ex.ExaminationsName,
                                                           PupilID = ep.PupilID,
                                                           ExamGroupID = eg.ExamGroupID,
                                                           SubjectID = es.SubjectID,
                                                           SubjectName = sc.SubjectName,
                                                           SchedulesExam = es.SchedulesExam
                                                       });
                List<ExamPupilBO> lstGroupEachPupil = null;
                ExamPupilBO objExamDetailBO = null;
                for (int i = 0; i < pupilOfClassList.Count; i++)
                {
                    pupilOfClassBO = pupilOfClassList[i];
                    pupilID = pupilOfClassBO.PupilID;
                    obj = new ExamSubjectBO();
                    obj.PupilID = pupilID;
                    obj.PupilName = pupilOfClassBO.PupilFullName;

                    pupilContent = new StringBuilder();
                    bool isFirst = true;
                    lstGroupEachPupil = iqExamPupil.Where(p => p.PupilID == pupilID).ToList();
                    for (int j = 0; j < lstGroupEachPupil.Count; j++)
                    {
                        objExamDetailBO = lstGroupEachPupil[j];
                        if (isFirst)
                        {
                            isFirst = false;
                            obj.ExaminationsID = objExamDetailBO.ExaminationsID;
                            obj.ExaminationsName = objExamDetailBO.ExaminationName;
                        }
                        if (!string.IsNullOrEmpty(objExamDetailBO.SchedulesExam))
                            pupilContent.Append(objExamDetailBO.SubjectName).Append(" ").Append(objExamDetailBO.SchedulesExam).Append("; ");
                    }

                    if (pupilContent != null && pupilContent.Length > 0)
                    {
                        if (pupilContent.ToString().EndsWith("; ")) pupilContent = pupilContent.Remove(pupilContent.Length - 2, 2);
                        obj.ScheduleContent = pupilContent.ToString();

                    }

                    listResult.Add(obj);
                }
            }


            return listResult;
        }

        public List<ExamMarkPupilBO> getExamResultInfo(List<long> pupilIDList, int schoolID, int academicYearID, int classID, int semester, int examID)
        {
            List<PupilOfClassBO> pupilOfClassList = (from poc in PupilOfClassBusiness.GetPupilInClass(classID)
                                                     where pupilIDList.Contains(poc.PupilID)
                                                     select poc).ToList();

            List<ExamMarkPupilBO> listResult = new List<ExamMarkPupilBO>();

            if (pupilOfClassList != null && pupilOfClassList.Count > 0)
            {
                int partitionId = UtilsBusiness.GetPartionId(schoolID);
                ExamMarkPupilBO obj = null;
                PupilOfClassBO pupilOfClassBO = null;
                int pupilID = 0;
                StringBuilder pupilContent = null;

                IQueryable<ExamInputMarkBO> lstExamMarkInput = (from eim in ExamInputMarkBusiness.All
                                                                join sc in SubjectCatBusiness.All on eim.SubjectID equals sc.SubjectCatID
                                                                where eim.SchoolID == schoolID && eim.LastDigitSchoolID == partitionId
                                                                && eim.AcademicYearID == academicYearID
                                                                && eim.ClassID == classID && pupilIDList.Contains(eim.PupilID)
                                                                && eim.ExaminationsID == examID
                                                                orderby sc.OrderInSubject
                                                                select new ExamInputMarkBO
                                                                {
                                                                    PupilID = eim.PupilID,
                                                                    SubjectID = eim.SubjectID,
                                                                    SubjectName = sc.SubjectName,
                                                                    ClassID = eim.ClassID,
                                                                    ExamMark = eim.ExamMark,
                                                                    ActualMark = eim.ActualMark,
                                                                    ExamJudgeMark = eim.ExamJudgeMark
                                                                });
                Examinations exam = ExaminationsBusiness.Find(examID);
                List<ExamInputMarkBO> lstMarkEachPupil = null;
                ExamInputMarkBO objExamInputMarkBO = null;
                for (int i = 0; i < pupilOfClassList.Count; i++)
                {
                    pupilOfClassBO = pupilOfClassList[i];
                    pupilID = pupilOfClassBO.PupilID;
                    obj = new ExamMarkPupilBO();
                    obj.PupilID = pupilID;
                    obj.PupilName = pupilOfClassBO.PupilFullName;
                    if (exam != null)
                    {
                        obj.ExaminationID = examID;
                        obj.ExaminationName = exam.ExaminationsName;
                    }
                    pupilContent = new StringBuilder();

                    lstMarkEachPupil = lstExamMarkInput.Where(p => p.PupilID == pupilID).ToList();
                    bool isFirst = true;
                    for (int j = 0; j < lstMarkEachPupil.Count; j++)
                    {
                        objExamInputMarkBO = lstMarkEachPupil[j];
                        if (objExamInputMarkBO.ActualMark.HasValue || !string.IsNullOrEmpty(objExamInputMarkBO.ExamJudgeMark))
                        {
                            if (isFirst) isFirst = false;
                            else pupilContent.Append("; ");
                            pupilContent.Append(objExamInputMarkBO.SubjectName).Append(" ");
                            if (objExamInputMarkBO.ActualMark.HasValue) pupilContent.Append(objExamInputMarkBO.ActualMark.Value.ToString("#0.#"));
                            else if (!string.IsNullOrEmpty(objExamInputMarkBO.ExamJudgeMark)) pupilContent.Append(objExamInputMarkBO.ExamJudgeMark);
                        }
                    }
                    if (pupilContent != null && pupilContent.Length > 0) obj.ExamResult = pupilContent.ToString();
                    listResult.Add(obj);
                }
            }

            return listResult;
        }

        public string GetListCalendarOfPupil(int AcademicYearID, int ClassID, int Semester)
        {
            var listCalendar = CalendarBusiness.All.Where(o => o.AcademicYearID == AcademicYearID
                    && o.ClassID == ClassID
                    && o.Semester == Semester && o.IsActive)
                    .Select(o => new
                    {
                        o.SubjectCat.DisplayName,
                        o.Section,
                        o.DayOfWeek,
                        o.SubjectOrder
                    }).ToList();
            string calendarSchedule = "";
            // Các thứ lớp có học
            List<int> listDayOfWeek = listCalendar.Select(o => o.DayOfWeek).Distinct().OrderBy(d => d).ToList();
            int count = listDayOfWeek.Count;
            for (int i = 0; i < count; i++)
            {
                string calendarByDay = string.Empty;
                // Thứ
                int d = listDayOfWeek[i];
                var listByDay = listCalendar.Where(o => o.DayOfWeek == d).ToList();
                var listBySection = listByDay.Select(o => o.Section).Distinct().OrderBy(s => s).ToList();
                calendarSchedule += "T" + d + "-";
                foreach (int s in listBySection)
                {
                    // Buổi
                    string sectionName = this.GetSection(s);
                    calendarByDay += sectionName + ": ";
                    // Môn học
                    var listSubjectName = listByDay.Where(o => o.Section == s).OrderBy(o => o.SubjectOrder);
                    string subjectName = string.Empty;
                    int countSubject = 0;
                    int sumSubjectName = listSubjectName.Count();
                    int index = 0;
                    foreach (var item in listSubjectName)
                    {
                        index++;
                        if (!item.DisplayName.Equals(subjectName))
                        {
                            if (!string.IsNullOrEmpty(subjectName))
                            {
                                if (countSubject > 1)
                                {
                                    calendarByDay += string.Format("{0}({1}), ", subjectName, countSubject);
                                }
                                else
                                {
                                    calendarByDay += string.Format("{0}, ", subjectName, countSubject);
                                }
                            }
                            subjectName = item.DisplayName;
                            countSubject = 0;
                        }

                        if (subjectName == item.DisplayName)
                        {
                            countSubject++;
                        }

                        if (sumSubjectName == index)
                        {
                            if (countSubject > 1)
                            {
                                calendarByDay += string.Format("{0}({1})", subjectName, countSubject);
                            }
                            else
                            {
                                calendarByDay += string.Format("{0}", subjectName);
                            }
                        }
                    }

                    // Thêm vào dấu phân cách
                    calendarByDay += "; ";
                }

                calendarSchedule += calendarByDay;
                if (i != count - 1)
                {
                    // Xuống dòng với mỗi thứ
                    calendarSchedule = calendarSchedule.Substring(0, calendarSchedule.Length - 2);
                    calendarSchedule += "\n";
                }
            }
            calendarSchedule = calendarSchedule.Substring(0, calendarSchedule.Length - 2);
            return calendarSchedule;
        }

        private int SetMonthWithPrimary(int month)
        {
            int Item = 0;
            if (month == 1)
            {
                Item = 5;
            }
            else if (month == 2)
            {
                Item = 6;
            }
            else if (month == 3)
            {
                Item = 7;
            }
            else if (month == 4)
            {
                Item = 8;
            }
            else if (month == 5)
            {
                Item = 9;
            }
            else if (month == 9)
            {
                Item = 1;
            }
            else if (month == 10)
            {
                Item = 2;
            }
            else if (month == 11)
            {
                Item = 3;
            }
            else if (month == 12)
            {
                Item = 4;
            }
            return Item;
        }

        private int SetOrderPrimary(bool isPrimary, string title)
        {
            int orderID = 0;
            if (isPrimary)
            {
                if (title.Contains("ĐTX1") || title.Contains("ĐTX6"))
                {
                    orderID = 1;
                }

                if (title.Contains("ĐTX2") || title.Contains("ĐTX7"))
                {
                    orderID = 2;
                }

                if (title.Contains("ĐTX3"))
                {
                    orderID = 3;
                }

                if (title.Contains("GK"))
                {
                    orderID = 4;
                }

                if (title.Contains("ĐTX4") || title.Contains("ĐTX8"))
                {
                    orderID = 5;
                }

                if (title.Contains("ĐTX5") || title.Contains("ĐTX9"))
                {
                    orderID = 5;
                }

                if (title.Contains("CK"))
                {
                    orderID = 6;
                }
            }

            return orderID;
        }

        private string GetSection(int Section)
        {
            if (Section == 1) return GlobalConstants.SECTION_MONING;
            if (Section == 2) return GlobalConstants.SECTION_AFTERNOON;
            if (Section == 3) return GlobalConstants.SECTION_EVENING;
            return string.Empty;
        }
    }
}
