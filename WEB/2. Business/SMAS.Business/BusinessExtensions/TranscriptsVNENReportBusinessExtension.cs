﻿/**
 * The Viettel License
 *
 * Copyright 2012 Viettel Telecom. All rights reserved.
 * VIETTEL PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */

/** 
* @author  
* @version $Revision: $
*/

using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Linq.Expressions;
using System.Web;
using log4net;

using SMAS.DAL.Repository;
using SMAS.DAL.IRepository;
using SMAS.Business.IBusiness;
using SMAS.Models.Models;
using SMAS.Business.Common;
using System.IO;
using SMAS.Business.BusinessObject;
using SMAS.VTUtils;
using SMAS.VTUtils.Excel.Export;
using System.Text;

namespace SMAS.Business.Business
{
    public partial class TranscriptsVNENReportBusiness
    {
        public ReportDefinition GetReportDefinition(string reportCode)
        {
            return ReportDefinitionBusiness.GetByCode(reportCode);
        }
        public string GetHashKeyTranscriptsVNENReport(TranscriptsVNENReportBO entity)
        {
            IDictionary<string, object> dic = new Dictionary<string, object> {
                {"AcademicYearID", entity.AcademicYearID},
                {"ClassID", entity.ClassID},
                {"SchoolID", entity.SchoolID},
                {"PupilID",entity.PupilID}
            };
            return ReportUtils.GetHashKey(dic);
        }
        public ProcessedReport GetProcessedReport(TranscriptsVNENReportBO entity,bool isZip = false)
        {
            string reportCode = isZip ? SystemParamsInFile.REPORT_TRANSCRIPTS_VNEN_ZIPFILE : SystemParamsInFile.REPORT_TRANSCRIPTS_VNEN;
            string inputParameterHashKey = this.GetHashKeyTranscriptsVNENReport(entity);
            return ProcessedReportBusiness.GetProcessedReport(reportCode, inputParameterHashKey);
        }
        public ProcessedReport InsertTranscriptsVNENReport(TranscriptsVNENReportBO entity, Stream data, bool isZip = false)
        {
            string reportCode = isZip ? SystemParamsInFile.REPORT_TRANSCRIPTS_VNEN_ZIPFILE : SystemParamsInFile.REPORT_TRANSCRIPTS_VNEN;
            ReportDefinition reportDef = ReportDefinitionBusiness.GetByCode(reportCode);
            ProcessedReport pr = new ProcessedReport();
            pr.ReportCode = reportCode;
            pr.ProcessedDate = DateTime.Now;
            pr.InputParameterHashKey = this.GetHashKeyTranscriptsVNENReport(entity);
            pr.ReportData = isZip ? ((MemoryStream)data).ToArray() : ReportUtils.Compress(data);

            //Tạo tên file
            string outputNamePattern = reportDef.OutputNamePattern;
            
            if (!isZip)
            {
                string PupilName = Utils.StripVNSignAndSpace(PupilProfileBusiness.Find(entity.PupilID).FullName);
                outputNamePattern = outputNamePattern.Replace("[PupilName]", PupilName);    
            }
            pr.ReportName = ReportUtils.RemoveSpecialCharacters(outputNamePattern) + "." + reportDef.OutputFormat;

            IDictionary<string, object> dic = new Dictionary<string, object> {
                {"AcademicYearID", entity.AcademicYearID},
                {"ClassID", entity.ClassID},
                {"SchoolID", entity.SchoolID},
                {"PupilID", entity.PupilID}
            };
            ProcessedReportParameterRepository.Insert(dic, pr);
            ProcessedReportRepository.Insert(pr);
            ProcessedReportRepository.Save();
            return pr;
        }
        public Stream GetTranscriptsVNENReport(TranscriptsVNENReportBO Entity)
        {
            ReportDefinition reportDef = ReportDefinitionBusiness.GetByCode(SystemParamsInFile.REPORT_TRANSCRIPTS_VNEN);
            string templatePath = SystemParamsInFile.TEMPLATE_FOLDER + reportDef.TemplateDirectory + "/" + reportDef.TemplateName;
            IVTWorkbook oBook = VTExport.OpenWorkbook(templatePath);
            PupilOfClassBO objPOC = PupilOfClassBusiness.GetPupilInClassTranscripReport(Entity.ClassID).Where(p => p.PupilID == Entity.PupilID && p.Status == SystemParamsInFile.PUPIL_STATUS_STUDYING)
                                                                            .OrderBy(p => p.OrderInClass).ThenBy(p => p.Name).ThenBy(p => p.PupilFullName).FirstOrDefault();

            List<SMAS.VTUtils.Utils.VTDataImageValidation> lsDataImage = new List<VTUtils.Utils.VTDataImageValidation>();
            List<SMAS.VTUtils.Utils.VTDataPageNumber> lstDataPageNumber = new List<VTUtils.Utils.VTDataPageNumber>();

            if (objPOC != null && objPOC.Image != null && objPOC.Image.Length > 0)
            {
                lsDataImage.Add(new SMAS.VTUtils.Utils.VTDataImageValidation
                {
                    SheetIndex = 3,
                    UpperLeftRow = 5,
                    UpperLeftColumn = 1,
                    LowerRightColumn = 2,
                    LowerRightRow = 10,
                    TypeFormat = 3,
                    ByteImage = objPOC.Image
                });
            }
        
            this.SetDataToFileExcel(oBook, Entity, objPOC, Entity.fillPaging, ref lstDataPageNumber);

            if (!Entity.fillPaging)
                lstDataPageNumber = new List<VTUtils.Utils.VTDataPageNumber>();

            return oBook.InsertImage(lsDataImage, lstDataPageNumber);
        }
        public void DownloadZipFile(TranscriptsVNENReportBO entity, out string folderSaveFile)
        {
            #region lay danh sach hoc sinh cua lop
            List<PupilOfClassBO> lstPOC = new List<PupilOfClassBO>();
            lstPOC = PupilOfClassBusiness.GetPupilInClassTranscripReport(entity.ClassID).Where(p => p.Status == SystemParamsInFile.PUPIL_STATUS_STUDYING)
                                                                            .OrderBy(p => p.OrderInClass).ThenBy(p => p.Name).ThenBy(p => p.PupilFullName).ToList();
            PupilOfClassBO objPOC = null;
            int pupilID = 0;
            string fileName = "{0}_HS_THCS_Hocba_{1}.xls";
            string tmp = string.Empty;
            string folder = Path.Combine(AppDomain.CurrentDomain.BaseDirectory + SystemParamsInFile.UPLOAD_FILE + "\\" + Guid.NewGuid().ToString());
            Directory.CreateDirectory(folder);

            List<SMAS.VTUtils.Utils.VTDataImageValidation> lsDataImage = new List<VTUtils.Utils.VTDataImageValidation>();
            List<SMAS.VTUtils.Utils.VTDataPageNumber> lstDataPageNumber = new List<VTUtils.Utils.VTDataPageNumber>();
         
            for (int i = 0; i < lstPOC.Count; i++)
            {
                objPOC = lstPOC[i];
                pupilID = objPOC.PupilID;

                if (objPOC != null && objPOC.Image != null && objPOC.Image.Length > 0)
                {
                    lsDataImage.Add(new SMAS.VTUtils.Utils.VTDataImageValidation
                    {
                        SheetIndex = 3,
                        UpperLeftRow = 5,
                        UpperLeftColumn = 1,
                        LowerRightColumn = 2,
                        LowerRightRow = 10,
                        TypeFormat = 3,
                        ByteImage = objPOC.Image
                    });
                }

                ReportDefinition reportDef = ReportDefinitionBusiness.GetByCode(SystemParamsInFile.REPORT_TRANSCRIPTS_VNEN);
                string templatePath = SystemParamsInFile.TEMPLATE_FOLDER + reportDef.TemplateDirectory + "/" + reportDef.TemplateName;
                IVTWorkbook oBook = VTExport.OpenWorkbook(templatePath);
                entity.PupilID = pupilID;
                this.SetDataToFileExcel(oBook, entity, objPOC, entity.fillPaging, ref lstDataPageNumber);

                if (!entity.fillPaging)
                    lstDataPageNumber = new List<VTUtils.Utils.VTDataPageNumber>();

                oBook.CreateZipFile(string.Format(fileName, i + 1, Utils.StripVNSignAndSpace(objPOC.PupilFullName)), folder, lsDataImage, lstDataPageNumber);

                lsDataImage = new List<VTUtils.Utils.VTDataImageValidation>();
                lstDataPageNumber = new List<VTUtils.Utils.VTDataPageNumber>();               
            }
            folderSaveFile = folder;
            #endregion

        }
        private void SetDataToFileExcel(IVTWorkbook oBook, TranscriptsVNENReportBO entity, PupilOfClassBO objPOC, bool fillPaging, ref List<SMAS.VTUtils.Utils.VTDataPageNumber> lstDataPageNumber)
        {
            #region lay danh sach cac sheet
            //sheet bia
            IVTWorksheet sheetBia = oBook.GetSheet(1);
            //sheet huong dan
            IVTWorksheet sheet2 = oBook.GetSheet(2);
            //sheet trang 3
            IVTWorksheet sheetTrang3 = oBook.GetSheet(3);
            //sheet trang 4
            IVTWorksheet sheetTrang4 = oBook.GetSheet(4);
            //sheet trang 5
            IVTWorksheet sheetTrang5 = oBook.GetSheet(5);
            //sheet trang 6
            IVTWorksheet sheettmp6 = oBook.GetSheet(6);
            //sheet trang cuoi
            IVTWorksheet sheettmpcuoi = oBook.GetSheet(7);
            #endregion

            int sheetPosition = 3;
            int startPageNumberOfSheet = 1;

            SchoolProfile objSP = SchoolProfileBusiness.Find(entity.SchoolID);
            AcademicYear objAcademicYear = AcademicYearBusiness.Find(entity.AcademicYearID);
            #region lay thong tin sheet 4
            var lstLearningProcess = (from poc in PupilOfClassBusiness.All
                                      join ay in AcademicYearBusiness.All on poc.AcademicYearID equals ay.AcademicYearID
                                      join cp in ClassProfileBusiness.All on poc.ClassID equals cp.ClassProfileID
                                      join sp in SchoolProfileBusiness.All on poc.SchoolID equals sp.SchoolProfileID
                                      where poc.SchoolID == entity.SchoolID
                                      && poc.PupilID == entity.PupilID
                                      && poc.Status == SystemParamsInFile.PUPIL_STATUS_STUDYING
                                      && cp.IsActive.Value
                                      select new
                                      {
                                          PupilID = poc.PupilID,
                                          YearName = ay.DisplayTitle,
                                          ClassName = cp.DisplayName,
                                          AcademicYearID = ay.AcademicYearID,
                                          SchoolID = ay.SchoolID,
                                          ClassID = cp.ClassProfileID,
                                          IsVNEN = cp.IsVnenClass,
                                          AdressName = (sp.SchoolName + ", " + sp.District.DistrictName + ", " + sp.Province.ProvinceName)
                                      }).ToList();
            #endregion
            #region lay du lieu sheet 5
            IDictionary<string, object> dicTeacherNoteBook = new Dictionary<string, object>()
            {
                {"SchoolID",entity.SchoolID},
                //{"AcademicYearID",entity.AcademicYearID},
                {"PupilID",entity.PupilID},
                //{"ClassID",entity.ClassID},
                {"MonthID",16}//danh gia cuoi ky hoc ky II
            };
            List<TeacherNoteBookMonthBO> lstTeacherNoteBookMonth = TeacherNoteBookMonthBusiness.GetListTeacherNoteBookMonth(dicTeacherNoteBook).ToList();
            List<TeacherNoteBookSemesterBO> lstTeacherNoteBookSemester = (from t in TeacherNoteBookSemesterBusiness.All
                                                                          join cs in ClassSubjectBusiness.All on new { t.ClassID, t.SubjectID } equals new { cs.ClassID, cs.SubjectID }
                                                                          join sc in SubjectCatBusiness.All on t.SubjectID equals sc.SubjectCatID
                                                                          where t.SchoolID == entity.SchoolID
                                                                          //&& t.AcademicYearID == entity.AcademicYearID
                                                                          && t.PupilID == entity.PupilID
                                                                          //&& t.ClassID == entity.ClassID
                                                                          && (t.SemesterID == SystemParamsInFile.SEMESTER_OF_YEAR_SECOND
                                                                          || t.SemesterID == SystemParamsInFile.SEMESTER_OF_YEAR_ALL)
                                                                          && t.PartitionID == (t.SchoolID % 100)
                                                                          select new TeacherNoteBookSemesterBO
                                                                          {
                                                                              AcademicYearID = t.AcademicYearID,
                                                                              ClassID = t.ClassID,
                                                                              PupilID = t.PupilID,
                                                                              SubjectName = sc.SubjectName,
                                                                              isCommenting = cs.IsCommenting,
                                                                              PERIODIC_SCORE_END = t.PERIODIC_SCORE_END,
                                                                              PERIODIC_SCORE_END_JUDGLE = t.PERIODIC_SCORE_END_JUDGLE,
                                                                              SubjectID = t.SubjectID,
                                                                              SemesterID = t.SemesterID,
                                                                              AVERAGE_MARK = t.AVERAGE_MARK,
                                                                              AVERAGE_MARK_JUDGE = t.AVERAGE_MARK_JUDGE
                                                                          }).ToList();

            List<int> lstClassID = lstLearningProcess.Where(p => p.IsVNEN.HasValue && p.IsVNEN.Value).Select(p => p.ClassID).ToList();

            List<ClassSubjectBO> lstClassSubject = (from cs in ClassSubjectBusiness.All
                                                    join cp in ClassProfileBusiness.All on cs.ClassID equals cp.ClassProfileID
                                                    join sc in SubjectCatBusiness.All on cs.SubjectID equals sc.SubjectCatID
                                                    where cp.SchoolID == entity.SchoolID
                                                    && lstClassID.Contains(cs.ClassID)
                                                    && cp.IsActive.Value
                                                    select new ClassSubjectBO
                                                    {
                                                        AcademicYearID = cp.AcademicYearID,
                                                        ClassID = cs.ClassID,
                                                        SubjectID = cs.SubjectID,
                                                        SubjectName = sc.DisplayName,
                                                        IsCommenting = cs.IsCommenting,
                                                        AppliedType = cs.AppliedType,
                                                        OrderInSubject = sc.OrderInSubject,
                                                        IsVNEN = cs.IsSubjectVNEN
                                                    }).OrderBy(p => p.OrderInSubject).ThenBy(p => p.SubjectName).ToList();
            IDictionary<string,object> dicSummed = new Dictionary<string,object>()
            {
                {"AcademicYearID",entity.AcademicYearID},
                {"ClassID",entity.ClassID},
                {"PupilID",entity.PupilID},
                {"Semester",SystemParamsInFile.SEMESTER_OF_YEAR_ALL}
            };
            List<SummedUpRecord> lstSummedUpRecord = SummedUpRecordBusiness.SearchBySchool(entity.SchoolID, dicSummed).ToList();;
            SummedUpRecord objSummedUpRecord = null;

            ClassSubjectBO objCS = null;
            TeacherNoteBookMonthBO objTeacherNoteMonthBO = null;
            TeacherNoteBookSemester objTeacherNoteSemesterBO = null;
            TeacherNoteBookSemester objTeacherNoteSemesterBOYear = null;
            #endregion
            #region lay du lieu sheet 6
            List<RewardFinal> lstRewardFinal = (from r in RewardFinalBusiness.All
                                                join ay in AcademicYearBusiness.All on r.SchoolID equals ay.SchoolID
                                                where r.SchoolID == entity.SchoolID
                                                && ay.AcademicYearID == entity.AcademicYearID
                                                select r).ToList();
            IDictionary<int, string> dicRewardFinal = new Dictionary<int, string>();
            lstRewardFinal.ForEach(p =>
            {
                dicRewardFinal[p.RewardFinalID] = p.RewardMode;
            });

            IDictionary<string, object> dicUpdateReward = new Dictionary<string, object>()
            {
                {"SchoolID",entity.SchoolID},
                {"AcademicYearID",entity.AcademicYearID},
                {"ClassID",entity.ClassID},
                {"PupilID",entity.PupilID}
            };
            List<UpdateReward> lstUpdateReward = UpdateRewardBusiness.Search(dicUpdateReward).OrderBy(p=>p.SemesterID).ToList();
            UpdateReward objUpdateReward = null;
            #endregion
            #region fill dữ liệu
            Dictionary<string, object> dicVariable = new Dictionary<string, object>();
            string schoolName = this.CutSchoolName(objSP.SchoolName);
            dicVariable["SchoolName"] = schoolName;
            dicVariable["DistrictName"] = objSP.District.DistrictName;
            dicVariable["ProvinceName"] = objSP.Province.ProvinceName;
            int PupilID = entity.PupilID;
            int startRow = 5;
            string subjectName = string.Empty;
            string strReward = string.Empty;
            List<int> lstRewardMod = new List<int>();
            #region fill sheet bia
            dicVariable["FullName"] = objPOC.PupilFullName;
            dicVariable["StorageNumber"] = !string.IsNullOrEmpty(objPOC.StorageNumber) ? objPOC.StorageNumber : "……………….:";
            sheetBia.FillVariableValue(dicVariable);
            #endregion
            #region fill sheet 3
            dicVariable = new Dictionary<string, object>()
            {
                {"FullName",objPOC.PupilFullName},
                {"Birthday",objPOC.Birthday.Date.Day + " tháng " + objPOC.Birthday.Date.Month + " năm " + objPOC.Birthday.Date.Year},
                {"GenreName",objPOC.Genre == 1 ? "Nam" : "Nữ"},
                {"BirthPlace",objPOC.BirthPlace},
                {"ResidentalAddress",!string.IsNullOrEmpty(objPOC.ResidentalAddress) ? objPOC.ResidentalAddress : objPOC.PermanentResidentalAddress},
                {"EthnicName",objPOC.EthnicName},
                {"FatherFullName",objPOC.FatherFullName},
                {"FatherJobName",objPOC.FatherJobName},
                {"MotherFullName",objPOC.MotherFullName},
                {"MotherJobName",objPOC.MotherJobName},
                {"SponserFullName",objPOC.SponserFullName},
                {"SponserJobName",objPOC.SponserJobName},
                {"DateTimeNow",objSP.Province.ProvinceName.ToUpper() + ", ngày " + objPOC.EnrolmentDate.Day + " tháng " + objPOC.EnrolmentDate.Month + " năm " + objPOC.EnrolmentDate.Year},
                {"HeadMasterName",objSP.HeadMasterName}               
            };
            sheetTrang3.FillVariableValue(dicVariable);

            lstDataPageNumber.Add(new SMAS.VTUtils.Utils.VTDataPageNumber()
            {
                SheetIndex = sheetPosition,
                IsHeaderOrFooter = true,
                AlignPageNumber = 2,
                StartPageNumberOfSheet = startPageNumberOfSheet,
            });

            #endregion
            #region fill sheet 4
            for (int j = 0; j < lstLearningProcess.Count; j++)
            {
                var objtmp = lstLearningProcess[j];
                if (objtmp != null)
                {
                    sheetTrang4.SetCellValue(startRow, 1, objtmp.YearName);
                    sheetTrang4.SetCellValue(startRow, 3, objtmp.ClassName);
                    sheetTrang4.SetCellValue(startRow, 4, objtmp.AdressName);
                    startRow++;
                }
            }

            sheetPosition++;
            startPageNumberOfSheet++;
            lstDataPageNumber.Add(new SMAS.VTUtils.Utils.VTDataPageNumber()
            {
                SheetIndex = sheetPosition,
                IsHeaderOrFooter = true,
                AlignPageNumber = 2,
                StartPageNumberOfSheet = startPageNumberOfSheet,
            });
            #endregion
            IVTWorksheet sheetTMP = null;
            List<ClassSubjectBO> lstCSTmp = null;
            int IntSheet = 5;
            #region fill sheet 5
            var lsttmp = lstLearningProcess.Where(p => p.IsVNEN.HasValue && p.IsVNEN.Value).ToList();
            double areaDefaultSetHeightSubject = 680;
            for (int i = 0; i < lsttmp.Count; i++)
            {
                var objLearningProcess = lsttmp[i];
                sheetTMP = oBook.CopySheetToLast(sheetTrang5);
                sheetTMP.SetCellValue("D2", objPOC.PupilFullName);
                sheetTMP.SetCellValue("I2", objLearningProcess.ClassName);
                sheetTMP.SetCellValue("J2", "Năm học: " + objLearningProcess.YearName);

                AcademicYear tmpAca = AcademicYearBusiness.Find(objLearningProcess.AcademicYearID);

                if (tmpAca.Year >= 2016)
                {
                    sheetTMP.SetCellValue("J4", "Điểm trung bình môn cả năm");
                }

                startRow = 5;
                lstCSTmp = lstClassSubject.Where(p => p.AcademicYearID == objLearningProcess.AcademicYearID && p.ClassID == objLearningProcess.ClassID).Distinct().ToList();
                for (int j = 0; j < lstCSTmp.Count; j++)
                {
                    #region
                    objCS = lstCSTmp[j];
                    subjectName = objCS.SubjectName;
                    sheetTMP.SetCellValue(startRow, 1, j + 1);
                    sheetTMP.SetCellValue(startRow, 2, subjectName);
                    if (objCS.IsVNEN.HasValue && objCS.IsVNEN.Value)
                    {
                        objTeacherNoteMonthBO = lstTeacherNoteBookMonth.Where(p => p.AcademicYearID == objLearningProcess.AcademicYearID && p.ClassID == objLearningProcess.ClassID
                                                                                  && p.SubjectID == objCS.SubjectID).FirstOrDefault();
                        if (objTeacherNoteMonthBO != null)
                        {
                            sheetTMP.SetCellValue(startRow, 4, objTeacherNoteMonthBO.CommentCQ);
                        }

                        objTeacherNoteSemesterBO = lstTeacherNoteBookSemester.Where(p => p.AcademicYearID == objLearningProcess.AcademicYearID && p.ClassID == objLearningProcess.ClassID 
                                                                                    && p.SubjectID == objCS.SubjectID && p.SemesterID == SystemParamsInFile.SEMESTER_OF_YEAR_SECOND).FirstOrDefault();

                        objTeacherNoteSemesterBOYear = lstTeacherNoteBookSemester.Where(p => p.AcademicYearID == objLearningProcess.AcademicYearID && p.ClassID == objLearningProcess.ClassID
                                                                                   && p.SubjectID == objCS.SubjectID && p.SemesterID == SystemParamsInFile.SEMESTER_OF_YEAR_ALL).FirstOrDefault();

                        if (tmpAca.Year >= 2016)
                        {
                            if (objTeacherNoteSemesterBOYear != null)
                            {
                                if (objCS.IsCommenting == 0)
                                {
                                    sheetTMP.SetCellValue(startRow, 10, objTeacherNoteSemesterBOYear.AVERAGE_MARK);
                                }
                                else
                                {
                                    sheetTMP.SetCellValue(startRow, 10, objTeacherNoteSemesterBOYear.AVERAGE_MARK_JUDGE);
                                }
                            }
                        }
                        else
                        {

                            if (objTeacherNoteSemesterBO != null)
                            {
                                if (objCS.IsCommenting == 0)
                                {
                                    sheetTMP.SetCellValue(startRow, 10, objTeacherNoteSemesterBO.PERIODIC_SCORE_END);
                                }
                                else
                                {
                                    sheetTMP.SetCellValue(startRow, 10, objTeacherNoteSemesterBO.PERIODIC_SCORE_END_JUDGLE);
                                }
                            }
                        }
                        //sheetTMP.SetRowHeight(startRow, 46);
                    }
                    else
                    {
                        objSummedUpRecord = lstSummedUpRecord.Where(p => p.AcademicYearID == objLearningProcess.AcademicYearID && p.ClassID == objLearningProcess.ClassID
                                                                    && p.SubjectID == objCS.SubjectID).FirstOrDefault();
                        if (objSummedUpRecord != null)
                        {
                            if (objCS.IsCommenting == 0)
                            {
                                sheetTMP.SetCellValue(startRow, 10, objSummedUpRecord.SummedUpMark);
                            }
                            else
                            {
                                sheetTMP.SetCellValue(startRow, 10, objSummedUpRecord.JudgementResult);
                            }
                        }
                        //sheetTMP.SetRowHeight(startRow, 46);
                    }
                    #endregion

                    sheetTMP.GetRange(startRow, 2, startRow, 3).Merge();
                    sheetTMP.GetRange(startRow, 2, startRow, 3).WrapText();
                    sheetTMP.GetRange(startRow, 4, startRow, 9).Merge();
                    sheetTMP.GetRange(startRow, 4, startRow, 9).SetHAlign(VTHAlign.xlHAlignLeft);              
                    sheetTMP.GetRange(startRow, 4, startRow, 9).WrapText();
                    sheetTMP.GetRange(startRow, 10, startRow, 11).Merge();
                    sheetTMP.GetRange(startRow, 12, startRow, 13).Merge();
                    startRow++;
                }

                double rowHeight = areaDefaultSetHeightSubject / (startRow - 5);
                for (int k = 5; k < startRow; k++)
                {
                    sheetTMP.SetRowHeight(k, rowHeight);
                }             
                sheetTMP.Name = "Trang " + (IntSheet + i);
                sheetTMP.SetFontName("Times New Roman", 12);
                //ke border
                sheetTMP.GetRange(5, 1, lstCSTmp.Count + 4, 13).SetBorder(VTBorderStyle.Solid, VTBorderIndex.All);

                sheetPosition++;
                startPageNumberOfSheet++;
                lstDataPageNumber.Add(new SMAS.VTUtils.Utils.VTDataPageNumber()
                {
                    SheetIndex = sheetPosition,
                    IsHeaderOrFooter = true,
                    AlignPageNumber = 2,
                    StartPageNumberOfSheet = startPageNumberOfSheet,
                });
            }
            
            sheetTrang5.Delete();
            #endregion
            #region fill sheet 6
            IVTWorksheet sheetTrang6 = oBook.CopySheetToLast(sheettmp6);
            if (lstUpdateReward.Count > 0)
            {
                for (int i = 0; i < lstUpdateReward.Count; i++)
                {
                    objUpdateReward = lstUpdateReward[i];
                    lstRewardMod = objUpdateReward.Rewards.Split(new string[] { "," }, StringSplitOptions.RemoveEmptyEntries).Select(p => Int32.Parse(p)).ToList();
                    for (int k = 0; k < lstRewardMod.Count; k++)
                    {
                        strReward += dicRewardFinal[lstRewardMod[k]] + ((k < lstRewardMod.Count - 1) ? ", " : "");
                    }
                    strReward += " - " + (objUpdateReward.SemesterID == SystemParamsInFile.SEMESTER_OF_YEAR_FIRST ? "Học kỳ 1"
                                                                                : objUpdateReward.SemesterID > SystemParamsInFile.SEMESTER_OF_YEAR_FIRST ? "Cả năm"
                                                                                : "") + "\r\n";
                }               
            }
           
            dicVariable = new Dictionary<string, object>()
            {
                {"SchoolName",schoolName},
                {"DistrictName",objSP.District != null ? objSP.District.DistrictName : ""},
                {"ProvinceName",objSP.Province.ProvinceName},
                {"RewardName",strReward},
                {"HeadMasterName",objSP.HeadMasterName},             
            };
            sheetTrang6.FillVariableValue(dicVariable);
            sheetTrang6.Name = "Trang " + (IntSheet + lsttmp.Count);

            sheetPosition++;
            startPageNumberOfSheet++;
            lstDataPageNumber.Add(new SMAS.VTUtils.Utils.VTDataPageNumber()
            {
                SheetIndex = sheetPosition,
                IsHeaderOrFooter = true,
                AlignPageNumber = 2,
                StartPageNumberOfSheet = startPageNumberOfSheet,
            });
            #endregion
            IVTWorksheet sheetTrangCuoi = oBook.CopySheetToLast(sheettmpcuoi);
            sheetPosition++;
            startPageNumberOfSheet++;
            lstDataPageNumber.Add(new SMAS.VTUtils.Utils.VTDataPageNumber()
            {
                SheetIndex = sheetPosition,
                IsHeaderOrFooter = true,
                AlignPageNumber = 2,
                StartPageNumberOfSheet = startPageNumberOfSheet,
            });

            sheetTrangCuoi.Name = "Trang " + (IntSheet + lsttmp.Count + 1);
            sheettmp6.Delete();
            sheettmpcuoi.Delete();

            sheetBia.FitAllColumnsOnOnePage = true;
            sheet2.FitAllColumnsOnOnePage = true;
            sheetBia.FitAllRowsOnOnePage = true;
            sheet2.FitAllRowsOnOnePage = true;
            sheetTrang3.FitAllColumnsOnOnePage = true;
            sheetTrang4.FitAllColumnsOnOnePage = true;
            sheetTrang5.FitAllColumnsOnOnePage = true;
            sheetTrang6.FitAllColumnsOnOnePage = true;
            sheetTrangCuoi.FitAllColumnsOnOnePage = true;
            sheetTrangCuoi.FitAllRowsOnOnePage = true;
            #endregion
        }
        private string CutSchoolName(string schoolName)
        {
            string result = string.Empty;
            string[] arrName = schoolName.Split(' ');
            if (arrName[0].ToUpper().Contains("TRƯỜNG"))
            {
                for (int i = 1; i < arrName.Length; i++)
                {
                    result = result + arrName[i] + " ";
                }
            }
            else
            {
                result = schoolName;
            }

            return result.Trim();
        }

        private string CutClassName(string className)
        {
            string result = string.Empty;
            string[] arrName = className.Split(' ');
            if (arrName[0].ToUpper().Contains("LỚP"))
            {
                for (int i = 1; i < arrName.Length; i++)
                {
                    result = result + arrName[i] + " ";
                }
            }
            else
            {
                result = className;
            }

            return result.Trim();
        }
    }
}