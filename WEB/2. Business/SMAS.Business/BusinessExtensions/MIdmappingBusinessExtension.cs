﻿/**
 * The Viettel License
 *
 * Copyright 2012 Viettel Telecom. All rights reserved.
 * VIETTEL PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */

/** 
* @author  
* @version $Revision: $
*/

using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Linq.Expressions;
using System.Web;
using log4net;
using SMAS.DAL.Repository;
using SMAS.DAL.IRepository;
using SMAS.Business.IBusiness;
using SMAS.Models.Models;
using SMAS.Business.Common;

namespace SMAS.Business.Business
{
    public partial class MIdmappingBusiness
    {
        #region Search
        /// <summary>
        /// Tìm kiếm 
        /// </summary>
        /// <param name="NewID">ID Smas 3</param>
        /// <param name="NewTableName"> Table SMAS3</param>
        /// <param name="OldGuid">ID if Guid</param>
        /// <param name="OldID">ID Smas 2</param>
        /// <param name="OldString"></param>
        /// <param name="OldTableName">Table Smas 2</param>
        /// <param name="Source"></param>
        /// <param name="SynchronizeID"></param>
        /// <returns> IQueryable<MIdmapping></returns>
        public IQueryable<MIdmapping> Search(IDictionary<string, object> dic)
        {
            int NewID = Utils.GetInt(dic, "NewID");
            string NewTableName = Utils.GetString(dic, "NewTableName");
            Guid? OldGuid = Utils.GetGuid(dic, "OldGuid");
            int OldID = Utils.GetInt(dic, "OldID");
            string OldString = Utils.GetString(dic, "OldString");
            string OldTableName = Utils.GetString(dic, "OldTableName");
            string Source = Utils.GetString(dic, "Source");
            int? SynchronizeID = Utils.GetInt(dic, "SynchronizeID");
            // bắt đầu tìm kiếm
            var query = MIdmappingRepository.All;

            if (NewID != 0)
            {
                query = query.Where(m => (m.NewID == NewID));
            }
            if (NewTableName.Trim().Length != 0)
            {
                query = query.Where(m => (m.NewTableName.ToUpper().Contains(NewTableName.ToUpper())));

            }
            if (OldGuid != null)
            {
                query = query.Where(m => (m.OldGuid == OldGuid));
            }
            if (OldID != 0)
            {
                query = query.Where(m => (m.OldID == OldID));

            }
            if (OldString.Trim().Length != 0)
            {
                query = query.Where(m => (m.OldString.ToUpper().Contains(OldString.ToUpper())));

            }
            if (OldTableName.Trim().Length != 0)
            {
                query = query.Where(m => (m.OldTableName.ToUpper().Contains(OldTableName.ToUpper())));

            }
            if (Source.Trim().Length != 0)
            {
                query = query.Where(m => (m.Source.ToUpper().Contains(Source.ToUpper())));
            }
            if (SynchronizeID.HasValue && SynchronizeID != 0)
            {
                query = query.Where(m => (m.SynchronizeID == SynchronizeID));
            }

            return query;

        }
        #endregion
    }
}