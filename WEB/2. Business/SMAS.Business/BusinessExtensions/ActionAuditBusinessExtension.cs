﻿/**
 * The Viettel License
 *
 * Copyright 2012 Viettel Telecom. All rights reserved.
 * VIETTEL PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */

/** 
* @author  
* @version $Revision: $
*/

using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Linq.Expressions;
using System.Web;
using log4net;

using SMAS.DAL.Repository;
using SMAS.DAL.IRepository;
using SMAS.Business.IBusiness;
using SMAS.Models.Models;
using System.Web.Mvc;
using SMAS.Business.Common;
using SMAS.Business.BusinessObject;
using System.Data.Objects;

namespace SMAS.Business.Business
{
    public partial class ActionAuditBusiness
    {
        public void AuditAction(ControllerContext ControllerContext, DateTime BeginAuditTime, DateTime EndAuditTime, int objectId, string OldObjectJSonValue, string NewObjectJSonValue, string Description)
        {
            //UserID không được null
            //TODO:hath8
            //Tên  Controller không được null
            Utils.ValidateNull(ControllerContext, "AuditAction_Label_ControllerContext");
            //Tên action không được null

            //Tên Controller không vượt quá 100 kí tự
            Utils.ValidateMaxLength(ControllerContext, 100, "AuditAction_Label_ControllerContext");
            //Tên Action không vượt quá 100 kí tự

            //Từ ControllerContext lấy ra các thông tin:
            //-	Tên controller
            //-	Tên action
            //-	IP, Referer và User-Agent của máy thực hiện
            //Từ Session lấy ra các thông tin: 
            //-	UserAccountID, RoleID, GroupID
            //Validate các trường required (UserID, Controller, Action) trong bảng ActionAudit
            //Validate max length các trường (Controller, Action, Parameter, BeginAuditTime, EndAuditTime, IP, Referer, User-Agent, oldObjectValue, newObjectValue, Description) trong bảng ActionAudit
            //Insert bản ghi mới vào bảng ActionAudit

            //Lay thong tin action cua truog
            //AdminID la UserAccCount cua truong
        }
        public List<ActionHistoryObject> ListActionBySchool(int SchoolID, int AdminID, DateTime FromDate, DateTime ToDate, string Username, string Action, int PageNum, int PageSize, ref int Total)
        {
            int partitionID = UtilsBusiness.GetPartionId(SchoolID);
            IQueryable<ActionHistoryObject> ActionAuditIquery = null;
            List<ActionHistoryObject> ActionAuditList = new List<ActionHistoryObject>();
            List<int> EmployeeIDList = null;
            if (SchoolID > 0)
            {
                //danh sach Id cua giao vien co ID UserAccount
                EmployeeIDList = (from e in EmployeeBusiness.All
                                  join ua in UserAccountBusiness.All on e.EmployeeID equals ua.EmployeeID
                                  where e.SchoolID == SchoolID
                                  && e.IsActive == true
                                  select ua.UserAccountID).ToList();

                if (EmployeeIDList.Count > 0 || AdminID > 0)
                {
                    if (Action == null)
                    {
                        Action = "-1";
                    }
                    if (Username == null)
                    {
                        Username = "-1";
                    }
                    ActionAuditIquery = from aa in ActionAuditBusiness.All
                                        join ua in UserAccountBusiness.All on aa.UserID equals ua.UserAccountID
                                        join oas in aspnet_UsersBusiness.All on ua.GUID equals oas.UserId
                                        where aa.LastDigitNumberSchool == partitionID
                                        && (EntityFunctions.TruncateTime(aa.BeginAuditTime) >= EntityFunctions.TruncateTime(FromDate.Date) && EntityFunctions.TruncateTime(aa.BeginAuditTime) <= EntityFunctions.TruncateTime(ToDate.Date))
                                        && (oas.UserName.ToLower().Contains(Username.ToLower()) || Username.Equals("-1"))
                                        && (aa.userAction.Contains(Action) || Action.Equals("-1"))
                                        && (EmployeeIDList.Contains(aa.UserID) || aa.UserID == AdminID)
                                        && !String.IsNullOrEmpty(aa.userFunction.Trim())
                                        select new ActionHistoryObject
                                        {
                                            ActionAuditID = aa.ActionAuditID,
                                            ActionDate = aa.BeginAuditTime.Value,
                                            UserName = oas.UserName,
                                            Name = ua.Employee.FullName,
                                            FunctionName = aa.userFunction,
                                            Action = ((GlobalConstants.ACTION_VIEW).Equals(aa.userAction) ? "Xem" : ((GlobalConstants.ACTION_ADD).Equals(aa.userAction) ? "Thêm" : ((GlobalConstants.ACTION_UPDATE).Equals(aa.userAction) ? "Sửa" : ((GlobalConstants.ACTION_DELETE).Equals(aa.userAction) ? "Xóa" : ((GlobalConstants.ACTION_EXPORT).Equals(aa.userAction) ? "Xuất excel" : ((GlobalConstants.ACTION_IMPORT).Equals(aa.userAction) ? "Import" : "")))))),
                                            Description = aa.userDescription,
                                            IP = aa.IP
                                        };
                    Total = ActionAuditIquery.Count();
                    ActionAuditList = ActionAuditIquery.OrderByDescending(p => p.ActionDate).Skip((PageNum - 1) * PageSize).Take(PageSize).ToList();
                }
            }
            return ActionAuditList;
        }

        //AdminID la userAccount cua phong so
        public List<ActionHistoryObject> ListActionBySuperVisingDeptID(int SuperVisingDeptID, int AdminID, DateTime FromDate, DateTime ToDate, string Username, string Action, int PageNum, int PageSize, ref int Total)
        {
            IQueryable<ActionHistoryObject> ActionAuditIquery = null;
            List<int> EmployeeIDList = null;
            List<ActionHistoryObject> ActionAuditList = new List<ActionHistoryObject>();
            if (Action == null)
            {
                Action = "-1";
            }
            if (Username == null)
            {
                Username = "-1";
            }
            if (SuperVisingDeptID > 0)
            {
                string traversalPath = string.Empty;
                SupervisingDept objSuperVisingDept = SupervisingDeptBusiness.Find(SuperVisingDeptID);
                traversalPath = objSuperVisingDept.TraversalPath + SuperVisingDeptID + "\\";

                //danh sach Id nhan vien thuoc phong so
                EmployeeIDList = (from e in EmployeeBusiness.All
                                  join s in SupervisingDeptBusiness.All on e.SupervisingDeptID equals s.SupervisingDeptID
                                  join ua in UserAccountBusiness.All on e.EmployeeID equals ua.EmployeeID
                                  where e.IsActive == true
                                  //&& (s.TraversalPath.Contains(traversalPath) || e.SupervisingDeptID == SuperVisingDeptID)
                                  && (e.SupervisingDeptID == SuperVisingDeptID)
                                  select ua.UserAccountID).ToList();

                if (EmployeeIDList.Count > 0 || AdminID > 0)
                {
                    ActionAuditIquery = from aa in ActionAuditBusiness.All
                                        join ua in UserAccountBusiness.All on aa.UserID equals ua.UserAccountID
                                        join oas in aspnet_UsersBusiness.All on ua.GUID equals oas.UserId
                                        where (EntityFunctions.TruncateTime(aa.BeginAuditTime) >= FromDate.Date && EntityFunctions.TruncateTime(aa.BeginAuditTime) <= ToDate.Date)
                                        && (oas.UserName.ToLower().Contains(Username.ToLower()) || Username.Equals("-1"))
                                        && (aa.userAction.Contains(Action) || Action.Equals("-1"))
                                        && (EmployeeIDList.Contains(aa.UserID) || aa.UserID == AdminID)
                                        && !String.IsNullOrEmpty(aa.userFunction.Trim())
                                        select new ActionHistoryObject
                                        {
                                            ActionAuditID = aa.ActionAuditID,
                                            ActionDate = aa.BeginAuditTime.Value,
                                            UserName = oas.UserName,
                                            Name =  ua.Employee.FullName,
                                            FunctionName = aa.userFunction,
                                            Action = ((GlobalConstants.ACTION_VIEW).Equals(aa.userAction) ? "Xem" : ((GlobalConstants.ACTION_ADD).Equals(aa.userAction) ? "Thêm" : ((GlobalConstants.ACTION_UPDATE).Equals(aa.userAction) ? "Sửa" : ((GlobalConstants.ACTION_DELETE).Equals(aa.userAction) ? "Xóa" : ((GlobalConstants.ACTION_EXPORT).Equals(aa.userAction) ? "Xuất excel" : ((GlobalConstants.ACTION_IMPORT).Equals(aa.userAction) ? "Import" : "")))))),
                                            Description = aa.userDescription,
                                            IP = aa.IP
                                        };
                    Total = ActionAuditIquery.Count();
                    ActionAuditList = ActionAuditIquery.OrderByDescending(p => p.ActionDate).Skip((PageNum - 1) * PageSize).Take(PageSize).ToList();
                }
            }
            return ActionAuditList;
        }

        public List<ActionHistoryObject> ListActionByAdmin(int AdminID, DateTime FromDate, DateTime ToDate, string Action, int PageNum, int PageSize, ref int Total)
        {
            if (Action == null)
            {
                Action = "-1";
            }

            List<ActionHistoryObject> ActionAuditList = new List<ActionHistoryObject>();
            IQueryable<ActionHistoryObject> ActionAuditIquery = from aa in ActionAuditBusiness.All
                                                                where aa.UserID == AdminID
                                                                && (EntityFunctions.TruncateTime(aa.BeginAuditTime) >= FromDate.Date && EntityFunctions.TruncateTime(aa.BeginAuditTime) <= ToDate.Date)
                                                                && (aa.userAction.Contains(Action) || Action.Equals("-1"))
                                                                && !String.IsNullOrEmpty(aa.userFunction.Trim())
                                                                select new ActionHistoryObject
                                                                 {
                                                                     ActionAuditID = aa.ActionAuditID,
                                                                     ActionDate = aa.BeginAuditTime.Value,
                                                                     UserName = "admin",
                                                                     Name = "admin",
                                                                     FunctionName = aa.userFunction,
                                                                     Action = ((GlobalConstants.ACTION_VIEW).Equals(aa.userAction) ? "Xem" : ((GlobalConstants.ACTION_ADD).Equals(aa.userAction) ? "Thêm" : ((GlobalConstants.ACTION_UPDATE).Equals(aa.userAction) ? "Sửa" : ((GlobalConstants.ACTION_DELETE).Equals(aa.userAction) ? "Xóa" : ((GlobalConstants.ACTION_EXPORT).Equals(aa.userAction) ? "Xuất excel" : ((GlobalConstants.ACTION_IMPORT).Equals(aa.userAction) ? "Import" : "")))))),
                                                                     Description = aa.userDescription,
                                                                     IP = aa.IP
                                                                 };
            Total = ActionAuditIquery.Count();
            ActionAuditList = ActionAuditIquery.OrderByDescending(p => p.ActionDate).Skip((PageNum - 1) * PageSize).Take(PageSize).ToList();
            return ActionAuditList;
        }

        public int GetAdminID(int SchoolID)
        {
            int AdminID = 0;
            SchoolProfile schoolProfile = SchoolProfileBusiness.All.Where(p => p.SchoolProfileID == SchoolID).FirstOrDefault();
            if (schoolProfile != null)
            {
                AdminID = schoolProfile.AdminID.Value;
            }
            return AdminID;
        }

        public int GetAdminIDBySuperVisingDept(int SupervisingDeptID)
        {
            int AdminID = 0;
            SupervisingDept supervisingDept = SupervisingDeptBusiness.All.Where(p => p.SupervisingDeptID == SupervisingDeptID).FirstOrDefault();
            if (supervisingDept != null)
            {
                AdminID = supervisingDept.AdminID.Value;
            }
            return AdminID;
        }

        public List<ActionHistoryObject> ListActionBySchoolExport(int SchoolID, int AdminID, DateTime FromDate, DateTime ToDate, string Username, string Action)
        {
            int partitionID = UtilsBusiness.GetPartionId(SchoolID);
            IQueryable<ActionHistoryObject> ActionAuditIquery = null;
            List<int> EmployeeIDList = null;
            if (SchoolID > 0)
            {
                //danh sach Id cua giao vien co ID UserAccount
                EmployeeIDList = (from e in EmployeeBusiness.All
                                  join ua in UserAccountBusiness.All on e.EmployeeID equals ua.EmployeeID
                                  where e.SchoolID == SchoolID
                                  && e.IsActive == true
                                  select ua.UserAccountID).ToList();

                if (EmployeeIDList.Count > 0 || AdminID > 0)
                {
                    if (Action == null)
                    {
                        Action = "-1";
                    }
                    if (Username == null)
                    {
                        Username = "-1";
                    }
                    ActionAuditIquery = from aa in ActionAuditBusiness.All
                                       join ua in UserAccountBusiness.All on aa.UserID equals ua.UserAccountID
                                       join oas in aspnet_UsersBusiness.All on ua.GUID equals oas.UserId
                                       where aa.LastDigitNumberSchool == partitionID
                                       && (EntityFunctions.TruncateTime(aa.BeginAuditTime) >= EntityFunctions.TruncateTime(FromDate.Date) && EntityFunctions.TruncateTime(aa.BeginAuditTime) <= EntityFunctions.TruncateTime(ToDate.Date))
                                       && (oas.UserName.ToLower().Contains(Username.ToLower()) || Username.Equals("-1"))
                                       && (aa.userAction.Contains(Action) || Action.Equals("-1"))
                                       && (EmployeeIDList.Contains(aa.UserID) || aa.UserID == AdminID)
                                       && !String.IsNullOrEmpty(aa.userFunction.Trim())
                                       orderby aa.BeginAuditTime descending
                                       select new ActionHistoryObject
                                       {
                                           ActionAuditID = aa.ActionAuditID,
                                           ActionDate = aa.BeginAuditTime.Value,
                                           UserName = oas.UserName,
                                           Name = ua.Employee.FullName,
                                           FunctionName = aa.userFunction,
                                           Action = ((GlobalConstants.ACTION_VIEW).Equals(aa.userAction) ? "Xem" : ((GlobalConstants.ACTION_ADD).Equals(aa.userAction) ? "Thêm" : ((GlobalConstants.ACTION_UPDATE).Equals(aa.userAction) ? "Sửa" : ((GlobalConstants.ACTION_DELETE).Equals(aa.userAction) ? "Xóa" : ((GlobalConstants.ACTION_EXPORT).Equals(aa.userAction) ? "Xuất excel" : ((GlobalConstants.ACTION_IMPORT).Equals(aa.userAction) ? "Import" : "")))))),
                                           Description = aa.userDescription,
                                           IP = aa.IP
                                       };
                }
            }
            return ActionAuditIquery.ToList();
        }

        public List<ActionHistoryObject> ListActionByAdminExport(int AdminID, DateTime FromDate, DateTime ToDate, string Action)
        {
            if (Action == null)
            {
                Action = "-1";
            }
            List<ActionHistoryObject> ActionAuditList = (from aa in ActionAuditBusiness.All
                                                         where aa.UserID == AdminID
                                                         && (EntityFunctions.TruncateTime(aa.BeginAuditTime) >= EntityFunctions.TruncateTime(FromDate.Date) && EntityFunctions.TruncateTime(aa.BeginAuditTime) <= EntityFunctions.TruncateTime(ToDate.Date))
                                                         && (aa.userAction.Contains(Action) || Action.Equals("-1"))
                                                         && !String.IsNullOrEmpty(aa.userFunction.Trim())
                                                         orderby aa.BeginAuditTime descending
                                                         select new ActionHistoryObject
                                                         {
                                                             ActionAuditID = aa.ActionAuditID,
                                                             ActionDate = aa.BeginAuditTime.Value,
                                                             UserName = "admin",
                                                             Name = "admin",
                                                             FunctionName = aa.userFunction,
                                                             Action = ((GlobalConstants.ACTION_VIEW).Equals(aa.userAction) ? "Xem" : ((GlobalConstants.ACTION_ADD).Equals(aa.userAction) ? "Thêm" : ((GlobalConstants.ACTION_UPDATE).Equals(aa.userAction) ? "Sửa" : ((GlobalConstants.ACTION_DELETE).Equals(aa.userAction) ? "Xóa" : ((GlobalConstants.ACTION_EXPORT).Equals(aa.userAction) ? "Xuất excel" : ((GlobalConstants.ACTION_IMPORT).Equals(aa.userAction) ? "Import" : "")))))),
                                                             Description = aa.userDescription,
                                                             IP = aa.IP
                                                         }).ToList();
            return ActionAuditList;
        }

        public List<ActionHistoryObject> ListActionBySuperVisingDeptIDExport(int SuperVisingDeptID, int AdminID, DateTime FromDate, DateTime ToDate, string Username, string Action)
        {
            List<int> EmployeeIDList = null;
            List<ActionHistoryObject> ActionAuditList = new List<ActionHistoryObject>();
            if (Action == null)
            {
                Action = "-1";
            }
            if (Username == null)
            {
                Username = "-1";
            }
            if (SuperVisingDeptID > 0)
            {
                string traversalPath = string.Empty;
                SupervisingDept objSuperVisingDept = SupervisingDeptBusiness.Find(SuperVisingDeptID);
                traversalPath = objSuperVisingDept.TraversalPath + SuperVisingDeptID + "\\";

                //danh sach Id nhan vien thuoc phong so
                EmployeeIDList = (from e in EmployeeBusiness.All
                                  join s in SupervisingDeptBusiness.All on e.SupervisingDeptID equals s.SupervisingDeptID
                                  join ua in UserAccountBusiness.All on e.EmployeeID equals ua.EmployeeID
                                  where e.IsActive == true
                                  //&& (s.TraversalPath.Contains(traversalPath) || e.SupervisingDeptID == SuperVisingDeptID)
                                  && (e.SupervisingDeptID == SuperVisingDeptID)
                                  select ua.UserAccountID).ToList();

                if (EmployeeIDList.Count > 0 || AdminID > 0)
                {
                    ActionAuditList = (from aa in ActionAuditBusiness.All
                                       join ua in UserAccountBusiness.All on aa.UserID equals ua.UserAccountID
                                       join oas in aspnet_UsersBusiness.All on ua.GUID equals oas.UserId
                                       where (EntityFunctions.TruncateTime(aa.BeginAuditTime) >= FromDate.Date && EntityFunctions.TruncateTime(aa.BeginAuditTime) <= ToDate.Date)
                                       && (oas.UserName.ToLower().Contains(Username.ToLower()) || Username.Equals("-1"))
                                       && (aa.userAction.Contains(Action) || Action.Equals("-1"))
                                       && (EmployeeIDList.Contains(aa.UserID) || aa.UserID == AdminID)
                                       && !String.IsNullOrEmpty(aa.userFunction.Trim())
                                       orderby aa.BeginAuditTime descending
                                       select new ActionHistoryObject
                                       {
                                           ActionAuditID = aa.ActionAuditID,
                                           ActionDate = aa.BeginAuditTime.Value,
                                           UserName = oas.UserName,
                                           Name = ua.Employee.FullName,
                                           FunctionName = aa.userFunction,
                                           Action = ((GlobalConstants.ACTION_VIEW).Equals(aa.userAction) ? "Xem" : ((GlobalConstants.ACTION_ADD).Equals(aa.userAction) ? "Thêm" : ((GlobalConstants.ACTION_UPDATE).Equals(aa.userAction) ? "Sửa" : ((GlobalConstants.ACTION_DELETE).Equals(aa.userAction) ? "Xóa" : ((GlobalConstants.ACTION_EXPORT).Equals(aa.userAction) ? "Xuất excel" : ((GlobalConstants.ACTION_IMPORT).Equals(aa.userAction) ? "Import" : "")))))),
                                           Description = aa.userDescription,
                                           IP = aa.IP
                                       }).ToList();
                }
            }
            return ActionAuditList;
        }
        public int EducationGrade(int schoolID)
        {
            int education = 0;
            SchoolProfile schoolProfile = SchoolProfileBusiness.All.Where(p => p.SchoolProfileID == schoolID).FirstOrDefault();
            if (schoolProfile != null)
            {
                education = schoolProfile.EducationGrade;
            }
            return education;

        }
    }
}
