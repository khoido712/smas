﻿
/**
 * The Viettel License
 *
 * Copyright 2012 Viettel Telecom. All rights reserved.
 * VIETTEL PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */

/** 
* @author  
* @version $Revision: $
*/

using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Linq.Expressions;
using System.Web;
using log4net;

using SMAS.DAL.Repository;
using SMAS.DAL.IRepository;
using SMAS.Business.IBusiness;
using SMAS.Models.Models;
using SMAS.Business.Common;

namespace SMAS.Business.Business
{

    public partial class VillageBusiness
    {

        #region private member variable
        private const int RESOLUTION_MAX_LENGTH = 50;   //Độ dài trường diễn giải
        private const int DESCRIPTION_MAX_LENGTH = 400; //Độ dài ghi chú
        private const int VILLAGECODE_MAX_LENGTH = 10; //Độ dài mã XÃ PHƯỜNG
        private const int VILLAGENAME_MAX_LENGTH = 50; //Độ dài tên Thôn/xóm
        private const int SHORTNAME_MAX_LENGTH = 10; // Độ dài tên ngắn
        private const int VALUE_MIN = 1;//Giá trị min
        private const int VALUE_MAX = 3;//Giá trị max
        #endregion


        #region Search


        /// <summary>
        /// Tìm kiếm Thôn/Xóm
        /// </summary>
        /// <author>hath</author>
        /// <date>4/9/2012</date>
        /// <param name="VillageName">Tên Thôn/xóm</param>
        /// <param name="VillageCode">Mã Thôn/xóm</param>
        /// <param name="ProvinceID">ID tỉnh/ thành phố</param>
        /// <param name="DistrictID">ID quận/huyện</param>
        /// <param name="Description">Mô tả</param>
        /// <param name="IsActive">Biến Kích hoạt</param>
        /// <returns>IQueryable<Village></returns>
        public IQueryable<Village> Search(IDictionary<string, object> dic)
        {
            string villageName = Utils.GetString(dic, "VillageName");
            string villageCode = Utils.GetString(dic, "VillageCode");
            int provinceID = Utils.GetInt(dic, "ProvinceID");
            int districtID = Utils.GetInt(dic, "DistrictID");
            int villageID = Utils.GetInt(dic, "CommuneID");
            string description = Utils.GetString(dic, "Description");
            bool? isActive = Utils.GetIsActive(dic, "IsActive");
            IQueryable<Village> lsVillage = VillageRepository.All;


            if (isActive.HasValue)
                lsVillage = lsVillage.Where(pro => pro.IsActive == isActive && pro.Commune.IsActive == true);
            if (villageName.Trim().Length != 0)
                lsVillage = lsVillage.Where(pro => pro.VillageName.Trim().ToLower().Equals(villageName.Trim().ToLower()) );
            if (villageCode.Trim().Length != 0)
                lsVillage = lsVillage.Where(pro => pro.VillageCode.Trim().ToLower().Equals(villageCode.Trim().ToLower()));
            if (description.Trim().Length != 0)
                lsVillage = lsVillage.Where(pro => pro.Description.Contains(description.ToLower()));
            if (provinceID != 0)
                lsVillage =
                    lsVillage.Where(
                        pro =>
                        pro.Commune.District.ProvinceID == provinceID && pro.Commune.IsActive == true &&
                        pro.Commune.District.IsActive == true);
            if (districtID != 0)
                lsVillage = lsVillage.Where(pro => pro.Commune.DistrictID == districtID);

            if (villageID != 0)
                lsVillage = lsVillage.Where(p => p.CommuneID == villageID);

            return lsVillage;

        }
        #endregion

        #region Insert


        /// <summary>
        /// Thêm Thôn/Xóm
        /// </summary>
        /// <author>hath</author>
        /// <date>4/9/2012</date>
        /// <param name="village">object village.</param>
        /// <returns>object village</returns>
        public override Village Insert(Village village)
        {            
            //Độ dài mã Thôn/Xóm không được vượt quá 10 
            Utils.ValidateMaxLength(village.VillageCode, VILLAGECODE_MAX_LENGTH, "Village_Label_VillageCode");
            //Tên Thôn/Xóm không được để trống 
            Utils.ValidateRequire(village.VillageName, "Village_Label_VillageName");
            //Độ dài trường tên Thôn/Xóm không được vượt quá 50 
            Utils.ValidateMaxLength(village.VillageName, VILLAGENAME_MAX_LENGTH, "Village_Label_VillageName");
            //Không tồn tại Xã/Phường hoặc đã bị xóa - Kiểm tra CommuneID trong bảng Commune với IsActive =1            
            new CommuneBusiness(null).CheckAvailable(village.CommuneID, "Village_CommuneID", true);
            //Độ dài ghi chú không được vượt quá 400 
            Utils.ValidateMaxLength(village.Description, DESCRIPTION_MAX_LENGTH, "Description");
            //Insert
            base.Insert(village);
            return village;

        }
        #endregion

        #region Update

        /// <summary>
        /// Sửa Thôn/Xóm
        /// </summary>
        /// <author>hath</author>
        /// <date>4/9/2012</date>
        /// <param name="village">object village.</param>
        /// <returns>object village</returns>
        public override Village Update(Village village)
        {
            //Bạn chưa chọn Thôn/Xóm cần sửa hoặc Thôn/Xóm bạn chọn đã bị xóa khỏi hệ thống
            new VillageBusiness(null).CheckAvailable((int)village.VillageID, "Village_Label_VillageID", true);
            //Độ dài mã Thôn/Xóm không được vượt quá 10 
            Utils.ValidateMaxLength(village.VillageCode, VILLAGECODE_MAX_LENGTH, "Village_Label_VillageCode");
            //Tên Thôn/Xóm không được để trống 
            Utils.ValidateRequire(village.VillageName, "Village_Label_VillageName");
            //Độ dài trường tên Thôn/Xóm không được vượt quá 50 
            Utils.ValidateMaxLength(village.VillageName, VILLAGENAME_MAX_LENGTH, "Village_Label_VillageName");
            //Không tồn tại Xã/Phường hoặc đã bị xóa - Kiểm tra CommuneID trong bảng Commune với IsActive =1            
            new CommuneBusiness(null).CheckAvailable(village.CommuneID, "Village_CommuneID", true);
            //Độ dài ghi chú không được vượt quá 400 
            Utils.ValidateMaxLength(village.Description, DESCRIPTION_MAX_LENGTH, "Description");


            base.Update(village);
            return village;
        }
        #endregion

        #region Delete

        /// <summary>
        /// Sửa Thôn/Xóm
        /// </summary>
        /// <author>hath</author>
        /// <date>4/9/2012</date>
        /// <param name="VillageID">ID Thôn/Xóm</param>
        public void Delete(int VillageID)
        {
            //Bạn chưa chọn Thôn/Xóm cần xóa hoặc Thôn/Xóm bạn chọn đã bị xóa khỏi hệ thống
            new VillageBusiness(null).CheckAvailable(VillageID, "Village_Label_VillageID", true);
            //da su dung hay chua
            this.CheckConstraints(GlobalConstants.LIST_SCHEMA, "Village", VillageID, "Village_Label_VillageIDFailed");
            ////Không thể xóa Thôn/Xóm đang sử dụng
            //new VillageBusiness(null).CheckConstraints(GlobalConstants.LIST_SCHEMA, "Village", VillageID, "Village_Label_VillageID");
            base.Delete(VillageID, true);
        }
        #endregion
    }
}