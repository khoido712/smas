﻿

﻿/**
 * The Viettel License
 *
 * Copyright 2012 Viettel Telecom. All rights reserved.
 * VIETTEL PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */

/** 
* @author  
* @version $Revision: $
*/

using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Linq.Expressions;
using System.Web;
using log4net;

using SMAS.DAL.Repository;
using SMAS.DAL.IRepository;
using SMAS.Business.IBusiness;
using SMAS.Models.Models;
using SMAS.Business.Common;
using SMAS.Business.BusinessObject;
using System.Web.Mvc;
using SMAS.VTUtils.Excel.Export;
using System.IO;

namespace SMAS.Business.Business
{
    /// <summary>
    /// Theo dõi sức khỏe
    /// <author>DungVA</author>
    /// <date>21/12/2012</date>
    /// </summary>
    public partial class MonitoringBookBusiness
    {
        #region old
        #region Validate
        public void CheckMonitoringBookIDAvailable(int MonitoringBookID)
        {
            bool AvailableMonitoringBookIDCompatible = this.repository.ExistsRow(GlobalConstants.HEALTH_SCHEMA, "MonitoringBook",
                   new Dictionary<string, object>()
                {
                    {"MonitoringBookID",MonitoringBookID}
                    
                }, null);
            if (!AvailableMonitoringBookIDCompatible)
            {
                throw new BusinessException("Common_Validate_NotAvailable");
            }
        }
        #endregion

        #region Insert
        /// <summary>
        /// Thêm mới thông tin sổ theo dõi sức khỏe
        /// <author>DungVA</author>
        /// <date>21/12/2012</date>
        /// </summary>
        /// <param name="monitoringBook">Đối tượng Insert</param>
        /// <returns></returns>
        public void Insert(MonitoringBook monitoringBook)
        {
            // Ngày theo dõi phải nhỏ hơn ngày hiện
            Utils.ValidatePastDate(monitoringBook.MonitoringDate, "DentalTest_Label_MonitoringBookDate");
            //Check Require, max length
            ValidationMetadata.ValidateObject(monitoringBook);
            //Check not compatible
            bool schoolAcademicYearCompatible = this.repository.ExistsRow(GlobalConstants.SCHOOL_SCHEMA, "AcademicYear",
                   new Dictionary<string, object>()
                {
                    {"SchoolID",monitoringBook.SchoolID},
                    {"AcademicYearID",monitoringBook.AcademicYearID}
                    //{"IsActive",true}
                }, null);
            if (!schoolAcademicYearCompatible)
            {
                throw new BusinessException("Common_Validate_NotCompatible");
            }

            bool classSchoolCompatible = this.repository.ExistsRow(GlobalConstants.SCHOOL_SCHEMA, "ClassProfile",
                   new Dictionary<string, object>()
                {
                    {"ClassProfileID",monitoringBook.ClassID},
                    {"SchoolID",monitoringBook.SchoolID}
                    //{"IsActive",true}
                }, null);
            if (!classSchoolCompatible)
            {
                throw new BusinessException("Common_Validate_NotCompatible");
            }

            //bool pupilClassCompatible = this.repository.ExistsRow(GlobalConstants.PUPIL_SCHEMA, "PupilProfile",
            //       new Dictionary<string, object>()
            //    {
            //        {"PupilProfileID",monitoringBook.PupilID},
            //        {"CurrentClassID",monitoringBook.ClassID}

            //    }, null);
            //if (!pupilClassCompatible)
            //{
            //    throw new BusinessException("Common_Validate_NotCompatible");
            //}

            // Ngày theo dõi phải lớn hơn ngày bắt đầu kỳ học mới
            AcademicYear ay = AcademicYearBusiness.Find(monitoringBook.AcademicYearID);

            if (monitoringBook.MonitoringDate.HasValue)
            {
                if (monitoringBook.MonitoringDate.Value.Date < ay.FirstSemesterStartDate.Value.Date)
                {
                    //Common_Validate_NotBeforeDate : {0} phải lớn hơn {1}	
                    throw new BusinessException("Common_Validate_NotBeforeDate", new List<object> { monitoringBook.MonitoringDate.Value.Date, ay.FirstSemesterStartDate.Value.Date });
                }

                if (monitoringBook.MonitoringDate.Value.Date > ay.SecondSemesterEndDate.Value.Date)
                {
                    //Common_Validate_NotBeforeDate : {0} phải lớn hơn {1}	
                    throw new BusinessException("Common_Validate_NotBeforeDate", new List<object> { ay.SecondSemesterEndDate.Value.Date, monitoringBook.MonitoringDate.Value.Date });
                }
            }

            if (monitoringBook.HealthPeriodID.HasValue)
            {
                bool pupilAcadYearCompatible = this.repository.ExistsRow(GlobalConstants.HEALTH_SCHEMA, "MonitoringBook",
                       new Dictionary<string, object>()
                {
                    {"PupilID",monitoringBook.PupilID},
                    {"HealthPeriodID",monitoringBook.HealthPeriodID},
                    {"AcademicYearID",monitoringBook.AcademicYearID}
                }, null);
                if (pupilAcadYearCompatible)
                {
                    throw new BusinessException("Common_Validate_Duplicate", new List<object>() { "HealthTest_Label_HealthPeriod" });
                }
            }
            else
            {
                //HaiVT 22/05/2013 - check phai them type neu khong se gap truong hop kham theo ngay hoac kham theo thang trung 1 ngay se sinh loi
                bool pupilMonitoringDateCompatible = this.repository.ExistsRow(GlobalConstants.HEALTH_SCHEMA, "MonitoringBook",
                       new Dictionary<string, object>()
                {
                    {"PupilID",monitoringBook.PupilID},
                    {"MonitoringDate",monitoringBook.MonitoringDate},
                    {"MonitoringType",monitoringBook.MonitoringType},
                }, null);
                if (pupilMonitoringDateCompatible)
                {
                    throw new BusinessException("Common_Validate_Duplicate", new List<object>() { "MonitoringBook_Label_MonitoringDate" });
                }
            }
            //this.CheckDuplicate(monitoringBook.PupilID.ToString(), GlobalConstants.HEALTH_SCHEMA, "MonitoringBook", "PupilID", false, 0, "MonitoringBook_Label_CheckDuplicatePupilID");
            //this.CheckDuplicate(monitoringBook.HealthPeriodID.ToString(), GlobalConstants.HEALTH_SCHEMA, "MonitoringBook", "HealthPeriodID", false, 0, "MonitoringBook_Label_CheckDuplicateHealthPeriodID");
            //this.CheckDuplicate(monitoringBook.AcademicYearID.ToString(), GlobalConstants.HEALTH_SCHEMA, "MonitoringBook", "AcademicYearID", false, 0, "MonitoringBook_Label_CheckDuplicateAcademicYearID");
            //this.CheckDuplicate(monitoringBook.MonitoringDate.ToString(), GlobalConstants.HEALTH_SCHEMA, "MonitoringBook", "MonitoringDate", false, 0, "MonitoringBook_Label_CheckDuplicateMonitoringDate");
            base.Insert(monitoringBook);
        }
        #endregion

        #region Insert
        /// <summary>
        /// Thêm mới thông tin sổ theo dõi sức khỏe
        /// <author>Vunx</author>
        /// <date>07/06/2013</date>
        /// </summary>
        /// <param name="monitoringBook">Đối tượng Insert</param>
        /// <returns></returns>
        public void InsertHealthTest(MonitoringBook MonitoringBook, PhysicalTest PhysicalTest, EyeTest EyeTest, ENTTest EntTest, SpineTest SpineTest, DentalTest DentalTest, OverallTest OverallTest)
        {
            ValidationMetadata.ValidateObject(PhysicalTest);
            IDictionary<string, object> SearchInfo = new Dictionary<string, object>();
            SearchInfo["PupilID"] = MonitoringBook.PupilID;
            SearchInfo["HealthPeriodID"] = MonitoringBook.HealthPeriodID;
            SearchInfo["AcademicYearID"] = MonitoringBook.AcademicYearID;
            List<MonitoringBook> lstMonitoringBook = MonitoringBookBusiness.SearchBySchool(MonitoringBook.SchoolID, SearchInfo).ToList();
            if (lstMonitoringBook.Count == 0)
            {
                if (null == MonitoringBook.MonitoringDate)
                {
                    throw new BusinessException("MonitoringBook_Label_MonitoringDateRepuired");
                }
                //Insert Phycical Test
                PhysicalTest.MonitoringBookID = MonitoringBook.MonitoringBookID;
                PhysicalTestBusiness.Insert(PhysicalTest);
                //Insert EyeTest
                EyeTest.MonitoringBookID = MonitoringBook.MonitoringBookID;
                EyeTestBusiness.Insert(EyeTest);
                //Insert EntTest
                EntTest.MonitoringBookID = MonitoringBook.MonitoringBookID;
                ENTTestBusiness.Insert(EntTest);
                //Insert Spine Test
                SpineTest.MonitoringBookID = MonitoringBook.MonitoringBookID;
                SpineTestBusiness.Insert(SpineTest);
                //Insert Dental Test
                DentalTest.MonitoringBookID = MonitoringBook.MonitoringBookID;
                DentalTestBusiness.Insert(DentalTest);
                //Insert Overall Test
                OverallTest.MonitoringBookID = MonitoringBook.MonitoringBookID;
                OverallTestBusiness.Insert(OverallTest);

                //Thực hiện insert dữ liệu vào bảng MonitoringBook bằng cách gọi hàm 
                MonitoringBookBusiness.Insert(MonitoringBook);
            }
            else
            {
                throw new BusinessException("MonitoringBook_Label_CheckDuplicateMonitoringBookID");
            }
        }
        #endregion

        #region Update
        /// <summary>
        /// Cập nhật thông tin sổ theo dõi sức khỏe
        /// <author>DungVA</author>
        /// <date>24/12/2012</date>
        /// </summary>
        /// <param name="monitoringBook">Đối tượng Update</param>
        /// <returns></returns>
        public void Update(MonitoringBook monitoringBook)
        {
            // Check Available MonitoringBookID
            CheckMonitoringBookIDAvailable(monitoringBook.MonitoringBookID);
            //Check Require, max length
            ValidationMetadata.ValidateObject(monitoringBook);
            //Check not compatible
            bool schoolAcademicYearCompatible = this.repository.ExistsRow(GlobalConstants.SCHOOL_SCHEMA, "AcademicYear",
                   new Dictionary<string, object>()
                {
                    {"SchoolID",monitoringBook.SchoolID},
                    {"AcademicYearID",monitoringBook.AcademicYearID}              
                }, null);
            if (!schoolAcademicYearCompatible)
            {
                throw new BusinessException("Common_Validate_NotCompatible");
            }
            bool classSchoolCompatible = this.repository.ExistsRow(GlobalConstants.SCHOOL_SCHEMA, "ClassProfile",
                   new Dictionary<string, object>()
                {
                    {"ClassProfileID",monitoringBook.ClassID},
                    {"SchoolID",monitoringBook.SchoolID}            
                }, null);
            if (!classSchoolCompatible)
            {
                throw new BusinessException("Common_Validate_NotCompatible");
            }

            bool pupilClassCompatible = this.repository.ExistsRow(GlobalConstants.PUPIL_SCHEMA, "PupilOfClass",
                   new Dictionary<string, object>()
                {
                  {"PupilID",monitoringBook.PupilID},
                    {"ClassID",monitoringBook.ClassID}
                }, null);
            if (!pupilClassCompatible)
            {
                throw new BusinessException("Common_Validate_NotCompatible");
            }
            // Ngày theo dõi phải nhỏ hơn ngày hiện
            if (monitoringBook.MonitoringDate > DateTime.Now)
            {
                throw new BusinessException("MonitoringBook_Label_CheckMonitoringDate");
            }
            // Ngày theo dõi phải lớn hơn ngày bắt đầu kỳ học mới
            AcademicYear ay = AcademicYearBusiness.Find(monitoringBook.AcademicYearID);

            //Utils.ValidateBeforeDate(monitoringBook.MonitoringDate, ay.FirstSemesterStartDate, "MonitoringBook_Label_MonitoringDate", "MonitoringBook_Label_FirstSemesterStartDate");
            //// Ngày theo dõi phải nhỏ hơn ngày kết thúc kỳ học cuối
            //Utils.ValidateAfterDate(monitoringBook.MonitoringDate, ay.SecondSemesterEndDate, "MonitoringBook_Label_MonitoringDate", "MonitoringBook_Label_FirstSemesterStartDate");
            //// Check Duplicate
            //this.CheckDuplicate(monitoringBook.PupilID.ToString(), GlobalConstants.HEALTH_SCHEMA, "MonitoringBook", "PupilID", false, 0, "MonitoringBook_Label_CheckDuplicatePupilID");
            //this.CheckDuplicate(monitoringBook.HealthPeriodID.ToString(), GlobalConstants.HEALTH_SCHEMA, "MonitoringBook", "HealthPeriodID", false, 0, "MonitoringBook_Label_CheckDuplicateHealthPeriodID");
            //this.CheckDuplicate(monitoringBook.AcademicYearID.ToString(), GlobalConstants.HEALTH_SCHEMA, "MonitoringBook", "AcademicYearID", false, 0, "MonitoringBook_Label_CheckDuplicateAcademicYearID");
            //this.CheckDuplicate(monitoringBook.MonitoringDate.ToString(), GlobalConstants.HEALTH_SCHEMA, "MonitoringBook", "MonitoringDate", false, 0, "MonitoringBook_Label_CheckDuplicateMonitoringDate");

            if (monitoringBook.MonitoringDate.HasValue)
            {
                if (monitoringBook.MonitoringDate.Value.Date < ay.FirstSemesterStartDate.Value.Date)
                {
                    //Common_Validate_NotBeforeDate : {0} phải lớn hơn {1}	
                    throw new BusinessException("Common_Validate_NotBeforeDate", new List<object> { monitoringBook.MonitoringDate.Value.Date, ay.FirstSemesterStartDate.Value.Date });
                }

                if (monitoringBook.MonitoringDate.Value.Date > ay.SecondSemesterEndDate.Value.Date)
                {
                    //Common_Validate_NotBeforeDate : {0} phải lớn hơn {1}	
                    throw new BusinessException("Common_Validate_NotBeforeDate", new List<object> { ay.SecondSemesterEndDate.Value.Date, monitoringBook.MonitoringDate.Value.Date });
                }
            }

            base.Update(monitoringBook);
        }
        #endregion

        #region Delete
        /// <summary>
        /// Xóa thông tin sổ theo dõi sức khỏe
        /// <author>DungVA</author>
        /// <date>25/12/2012</date>
        /// </summary>
        /// <param name="MonitoringBookID">MonitoringBookID</param>
        /// <param name="SchoolID">SchoolID</param>
        /// <returns></returns>
        public void Delete(int MonitoringBookID, int SchoolID)
        {
            // Check Available MonitoringBookID
            CheckMonitoringBookIDAvailable(MonitoringBookID);

            // Check Using
            this.CheckConstraints(GlobalConstants.HEALTH_SCHEMA, "MonitoringBook", MonitoringBookID, "Supplier_Label_SupplierID");

            //Check not compatible
            bool SchoolIDMonitoringBookIDCompatible = this.repository.ExistsRow(GlobalConstants.HEALTH_SCHEMA, "MonitoringBook",
                   new Dictionary<string, object>()
                {
                    {"MonitoringBookID",MonitoringBookID},
                    {"SchoolID",SchoolID},
                    {"IsActive",true}
                }, null);
            if (!SchoolIDMonitoringBookIDCompatible)
            {
                throw new BusinessException("Common_Validate_NotCompatible");
            }


            base.Delete(MonitoringBookID, true);
        }
        #endregion

        #region Search
        /// <summary>
        /// Tìm kiếm thông tin sổ theo dõi sức khỏe
        /// <author>DungVA</author>
        /// <date>25/12/2012</date>
        /// </summary>
        /// <param name="dic">dic</param>
        /// <returns>MonitoringBook</returns>
        public IQueryable<MonitoringBook> Search(IDictionary<string, object> dic)
        {
            int MonitoringBookID = Utils.GetInt(dic, "MonitoringBookID");//: default = 0; 0 => Tìm kiếm all
            int AcademicYearID = Utils.GetInt(dic, "AcademicYearID");//: default = 0; 0 => Tìm kiếm All
            int SchoolID = Utils.GetInt(dic, "SchoolID");//: default = 0; 0 => tìm kiếm all
            int ClassID = Utils.GetInt(dic, "ClassID");//: default = 0; 0 => tìm kiếm All
            int PupilID = Utils.GetInt(dic, "PupilID");//: default = 0; 0 => Tìm kiếm All
            DateTime? MonitoringDate = Utils.GetDateTime(dic, "MonitoringDate");//: default = NULL; NULL => Tìm kiếm All
            string Symptoms = Utils.GetString(dic, "Symptoms");//: default = “”; “” => tìm kiếm All
            string Solution = Utils.GetString(dic, "Solution");//: default = “” ; “” => Tìm kiếm All
            string UnitName = Utils.GetString(dic, "UnitName");//: default = “”; “” => Tìm kiếm All
            int MonitoringType = Utils.GetInt(dic, "MonitoringType");//: Default = 0; 0 => Tìm kiếm All
            int HealthPeriodID = Utils.GetInt(dic, "HealthPeriodID");//: default = 0; 0 => Tìm kiếm All
            string FullName = Utils.GetString(dic, "FullName");//: default = “”; “” => Tìm kiếm All
            DateTime? FromDate = Utils.GetDateTime(dic, "FromMonitoringDate");
            DateTime? ToDate = Utils.GetDateTime(dic, "ToMonitoringDate");
            int EducationLevelID = Utils.GetInt(dic, "EducationLevelID");//: Default = 0; 0 => Tìm kiếm All
            int MonitoringMonth = Utils.GetInt(dic, "MonitoringMonth");//: Tháng tạo du lieu
            int AppliedLevel = Utils.GetInt(dic, "AppliedLevel");
            IQueryable<MonitoringBook> listMonitoringBook = this.MonitoringBookRepository.All;
            IQueryable<PupilProfile> listPupilprofile = this.PupilProfileRepository.All;
            if (FromDate != null)
            {
                listMonitoringBook = listMonitoringBook.Where(x => x.MonitoringDate >= FromDate);
            }
            if (ToDate != null)
            {
                listMonitoringBook = listMonitoringBook.Where(x => x.MonitoringDate <= ToDate);
            }
            if (!FullName.Equals(string.Empty))
            {
                listMonitoringBook = from a in listMonitoringBook
                                     join b in listPupilprofile on a.PupilID equals b.PupilProfileID
                                     where b.FullName.ToLower().Contains(FullName.ToLower())
                                     select a;
            }
            if (MonitoringBookID != 0)
            {
                listMonitoringBook = listMonitoringBook.Where(x => x.MonitoringBookID == MonitoringBookID);
            }
            if (AcademicYearID != 0)
            {
                listMonitoringBook = listMonitoringBook.Where(x => x.AcademicYearID == AcademicYearID);
            }
            if (SchoolID != 0)
            {
                listMonitoringBook = listMonitoringBook.Where(x => x.SchoolID == SchoolID);
            }
            if (ClassID != 0)
            {
                listMonitoringBook = listMonitoringBook.Where(x => x.ClassID == ClassID);
            }
            if (PupilID != 0)
            {
                listMonitoringBook = listMonitoringBook.Where(x => x.PupilID == PupilID);
            }
            /*if (MonitoringDate.HasValue)
            {
                listMonitoringBook = listMonitoringBook.Where(x => x.MonitoringDate.Day == MonitoringDate.Value.Day &&
                    x.MonitoringDate.Month == MonitoringDate.Value.Month && x.MonitoringDate.Year == MonitoringDate.Value.Year);
            }
            if (MonitoringMonth != 0)
            {
                listMonitoringBook = listMonitoringBook.Where(a => a.MonitoringDate.Month == MonitoringMonth);
            }*/
            if (!Symptoms.Equals(string.Empty))
            {
                listMonitoringBook = listMonitoringBook.Where(x => x.Symptoms.ToLower().Contains(Symptoms.ToLower()));
            }
            if (!Solution.Equals(string.Empty))
            {
                listMonitoringBook = listMonitoringBook.Where(x => x.Solution.ToLower().Contains(Solution.ToLower()));
            }
            if (!UnitName.Equals(string.Empty))
            {
                listMonitoringBook = listMonitoringBook.Where(x => x.UnitName.ToLower().Contains(UnitName.ToLower()));
            }
            if (MonitoringType != 0)
            {
                listMonitoringBook = listMonitoringBook.Where(x => x.MonitoringType == MonitoringType);
            }
            if (HealthPeriodID != 0)
            {
                listMonitoringBook = listMonitoringBook.Where(x => x.HealthPeriodID == HealthPeriodID);
            }
            if (EducationLevelID != 0)
            {
                listMonitoringBook = listMonitoringBook.Where(x => x.EducationLevelID == EducationLevelID);
            }
            else
            {
                if (AppliedLevel == 1)
                {
                    listMonitoringBook = listMonitoringBook.Where(x => x.EducationLevelID == 1 || x.EducationLevelID == 2 || x.EducationLevelID == 3 || x.EducationLevelID == 4 || x.EducationLevelID == 5);
                }
                else if (AppliedLevel == 2)
                {
                    listMonitoringBook = listMonitoringBook.Where(x => x.EducationLevelID == 6 || x.EducationLevelID == 7 || x.EducationLevelID == 8 || x.EducationLevelID == 9);
                }
                else if (AppliedLevel == 3)
                {
                    listMonitoringBook = listMonitoringBook.Where(x => x.EducationLevelID == 10 || x.EducationLevelID == 11 || x.EducationLevelID == 12);
                }
                else if (AppliedLevel == 4)
                {
                    listMonitoringBook = listMonitoringBook.Where(x => x.EducationLevelID == 13 || x.EducationLevelID == 14 || x.EducationLevelID == 15);
                }
                else if (AppliedLevel == 5)
                {
                    listMonitoringBook = listMonitoringBook.Where(x => x.EducationLevelID == 16 || x.EducationLevelID == 17 || x.EducationLevelID == 18);
                }
            }
            return listMonitoringBook;
        }
        /// <summary>
        /// Tìm kiếm thông tin sổ theo dõi sức khỏe theo học sinh
        /// <author>DungVA</author>
        /// <date>25/12/2012</date>
        /// </summary>
        /// <param name="SchoolID">SchoolID</param>
        /// <param name="AcademicYearID">AcademicYearID = 0 - lay thong tin tat ca cac nam hoc</param>
        /// <param name="PupilID">PupilID != 0 - lay thong tin cua 1 hoc sinh, = 0 - lay thong tin tat ca cac hoc sinh trong lop</param>
        /// <param name="ClassID">ClassID</param>
        /// <returns>MonitoringBook</returns>
        public IQueryable<MonitoringBook> SearchByPupil(int SchoolID, int AcademicYearID, int PupilID, int ClassID)
        {
            // baolvt start 23/04/2014
            if (AcademicYearID == 0)// lấy thông tin tất cả các năm học
            {
                if (PupilID != 0)//
                {
                    IQueryable<MonitoringBook> lstMonistoringBookTemp = MonitoringBookRepository.All.Where(o => o.PupilID == PupilID);
                    return lstMonistoringBookTemp;
                }
                else
                {
                    //IQueryable<MonitoringBook> lstMonistoringBookTemp = MonitoringBookRepository.All.Where(o => o.ClassID == ClassID);
                    IQueryable<MonitoringBook> lstMonistoringBookTemp = from a in MonitoringBookRepository.All
                                                                        join b in PupilOfClassRepository.All on a.PupilID equals b.PupilID
                                                                        where b.ClassID == ClassID
                                                                        select a;
                    return lstMonistoringBookTemp;
                }
            }
            // baolvt end

            DateTime? date = null;
            IDictionary<string, object> dictionary = new Dictionary<string, object>()
                {
                    {"PupilID",PupilID},
                    {"AcademicYearID",AcademicYearID}
                };
            IQueryable<PupilOfClass> lstPupilOfClass = PupilOfClassBusiness.SearchBySchool(SchoolID, dictionary);
            List<PupilOfClass> lstPOC = lstPupilOfClass.ToList();
            for (int i = 0; i < lstPOC.Count; i++)
            {
                if (lstPOC[i].Status != SystemParamsInFile.PUPIL_STATUS_MOVED_TO_OTHER_SCHOOL || lstPOC[i].Status != SystemParamsInFile.PUPIL_STATUS_MOVED_TO_OTHER_CLASS)
                {
                    DateTime? currentDate = DateTime.Now;
                    DateTime? academicDate = AcademicYearBusiness.Find(AcademicYearID).SecondSemesterEndDate;
                    if (currentDate > academicDate)
                    {
                        date = currentDate;
                    }
                    else
                    {
                        date = academicDate;
                    }
                }
                else
                {
                    IDictionary<string, object> dic = new Dictionary<string, object>()
                    {
                        {"PupilID",PupilID}
                    };
                    SchoolMovement schoolMovement = SchoolMovementBusiness.SearchBySchool(SchoolID, dic).OrderByDescending(o => o.MovedDate).FirstOrDefault();
                    DateTime? currentDate = DateTime.Now;
                    DateTime? academicDate = AcademicYearBusiness.Find(AcademicYearID).SecondSemesterEndDate;
                    DateTime? moveDate = schoolMovement.MovedDate;
                    if (currentDate >= academicDate && currentDate >= moveDate)
                    {
                        date = currentDate;
                    }
                    else if (academicDate >= currentDate && academicDate >= moveDate)
                    {
                        date = academicDate;
                    }
                    else date = moveDate;
                }

            }
            IQueryable<MonitoringBook> lstMonistoringBook = MonitoringBookRepository.All.Where(o => o.MonitoringDate <= date && o.PupilID == PupilID);
            return lstMonistoringBook;
        }
        /// <summary>
        /// Tìm kiếm thông tin sổ theo dõi sức khỏe theo trường
        /// <author>DungVA</author>
        /// <date>25/12/2012</date>
        /// </summary>
        /// <param name="SchoolID">SchoolID</param>
        /// <param name="dic">dic</param>
        /// <returns>MonitoringBook</returns>
        public IQueryable<MonitoringBook> SearchBySchool(int SchoolID, IDictionary<string, object> dic)
        {
            if (SchoolID == 0)
            {
                return null;
            }
            else
            {
                if (dic == null)
                {
                    dic = new Dictionary<string, object>();
                }
                dic["SchoolID"] = SchoolID;
                return Search(dic);
            }

        }
        #endregion

        #region GetList
        /// <summary>
        /// Tìm kiếm thông tin sổ theo dõi sức khỏe
        /// <author>DungVA</author>
        /// <date>25/12/2012</date>
        /// </summary>
        /// <param name="dic">dic</param>
        /// <returns>MonitoringBook</returns>
        /// Sửa trangdd
        public IQueryable<MonitoringBookSearch> GetListPupil(IDictionary<string, object> dic)
        {
            int AppliedLevel = Utils.GetInt(dic, "AppliedLevel");
            int EducationLevelID = Utils.GetInt(dic, "EducationLevelID");
            int min;
            int max;
            if (AppliedLevel == GlobalConstants.APPLIED_LEVEL_PRIMARY)
            {
                min = 1;
                max = 5;
            }
            else if (AppliedLevel == GlobalConstants.APPLIED_LEVEL_SECONDARY)
            {
                min = 6;
                max = 9;
            }
            else if (AppliedLevel == GlobalConstants.APPLIED_LEVEL_TERTIARY)
            {
                min = 10;
                max = 12;
            }
            else if (AppliedLevel == GlobalConstants.APPLIED_LEVEL_CRECHE)
            {
                min = 13;
                max = 15;
            }
            else if (AppliedLevel == GlobalConstants.APPLIED_LEVEL_KINDER_GARTEN)
            {
                min = 16;
                max = 18;
            }
            else
            {
                min = int.MaxValue;
                max = int.MinValue;
            }

            int AcademicYearID = Utils.GetInt(dic, "AcademicYearID");
            int ClassID = Utils.GetInt(dic, "ClassID");
            int HealthPeriodID = Utils.GetInt(dic, "HealthPeriodID");
            int SchoolID = Utils.GetInt(dic, "SchoolID");
            string FullName = Utils.GetString(dic, "FullName");
            string LowerFullName;
            if (FullName != null) LowerFullName = FullName.ToLower();
            else LowerFullName = null;
            int HealthStatus = Utils.GetInt(dic, "HealthStatus");
            bool? IsDredging = dic["IsDredging"] == null ? false : (dic["IsDredging"].ToString().Contains("true") ? true : false);

            if (HealthStatus == SystemParamsInFile.HEALTH_STATUS_ALL)
            {
                var query = from poc in PupilOfClassRepository.All
                            join mb in MonitoringBookRepository.All.Where(o => o.HealthPeriodID == HealthPeriodID
                                                                                && o.AcademicYearID == AcademicYearID)
                                on poc.PupilID equals mb.PupilID into PupilOfClassMonitoringBook
                            from pmb in PupilOfClassMonitoringBook.DefaultIfEmpty()
                            where (poc.ClassProfile.EducationLevelID >= min && poc.ClassProfile.EducationLevelID <= max)
                                && (poc.ClassProfile.EducationLevelID == EducationLevelID || EducationLevelID == 0)
                                && (poc.SchoolID == SchoolID)
                                && (poc.AcademicYearID == AcademicYearID)
                                && (poc.ClassID == ClassID || ClassID == 0)
                                && (poc.PupilProfile.FullName.ToLower().Contains(LowerFullName))
                                && (poc.PupilProfile.IsActive == true)
                                && (poc.Status != SystemParamsInFile.PUPIL_STATUS_MOVED_TO_OTHER_CLASS)
                            select new MonitoringBookSearch
                            {
                                EducationLevelID = poc.ClassProfile.EducationLevelID,
                                AcademicYearID = poc.AcademicYearID,
                                SchoolID = poc.SchoolID,
                                PupilID = poc.PupilID,
                                PupilCode = poc.PupilProfile.PupilCode,
                                FullName = poc.PupilProfile.FullName,
                                BrithDate = poc.PupilProfile.BirthDate,
                                Sex = poc.PupilProfile.Genre,
                                ClassID = poc.ClassID,
                                ClassName = poc.ClassProfile.DisplayName,
                                HealthPeriodID = pmb.HealthPeriodID,
                                MonitoringDate = pmb.MonitoringDate,
                                MonitoringBookID = pmb.MonitoringBookID,
                                IsAction = pmb.MonitoringBookID != null ? true : false,
                                PupilShortName = poc.PupilProfile.Name
                            };
                return query;
            }
            else if (HealthStatus == SystemParamsInFile.HEALTH_STATUS_DK)
            {
                if (IsDredging == true)
                {
                    var query =
                        //from 
                                from ot in OverallTestRepository.All.Where(o => (o.IsDredging == IsDredging))
                                join mb in MonitoringBookRepository.All on ot.MonitoringBookID equals mb.MonitoringBookID into MonitoringBookOverallTest
                                from mbo in MonitoringBookOverallTest.DefaultIfEmpty()
                                join poc in PupilOfClassRepository.All on mbo.PupilID equals poc.PupilID
                                where (poc.ClassProfile.EducationLevelID >= min && poc.ClassProfile.EducationLevelID <= max)
                                && (poc.ClassProfile.EducationLevelID == EducationLevelID || EducationLevelID == 0)
                                && (poc.SchoolID == SchoolID)
                                && (poc.AcademicYearID == AcademicYearID)
                                && (poc.ClassID == ClassID || ClassID == 0)
                                && (poc.PupilProfile.FullName.ToLower().Contains(LowerFullName))
                                && (mbo.HealthPeriodID == HealthPeriodID)
                                && (poc.PupilProfile.IsActive == true)
                                && (poc.Status != SystemParamsInFile.PUPIL_STATUS_MOVED_TO_OTHER_CLASS)
                                select new MonitoringBookSearch
                                {
                                    MonitoringBookID = mbo.MonitoringBookID,
                                    EducationLevelID = poc.ClassProfile.EducationLevelID,
                                    AcademicYearID = poc.AcademicYearID,
                                    SchoolID = poc.SchoolID,
                                    PupilID = poc.PupilID,
                                    PupilCode = poc.PupilProfile.PupilCode,
                                    FullName = poc.PupilProfile.FullName,
                                    BrithDate = poc.PupilProfile.BirthDate,
                                    Sex = poc.PupilProfile.Genre,
                                    ClassID = poc.ClassID,
                                    ClassName = poc.ClassProfile.DisplayName,
                                    HealthPeriodID = mbo.HealthPeriodID,
                                    MonitoringDate = mbo.MonitoringDate,
                                    IsAction = true,
                                    PupilShortName = poc.PupilProfile.Name
                                };
                    return query;
                }
                else
                {
                    var query =
                                from mbo in MonitoringBookRepository.All
                                join poc in PupilOfClassRepository.All on mbo.PupilID equals poc.PupilID
                                where (poc.ClassProfile.EducationLevelID >= min && poc.ClassProfile.EducationLevelID <= max)
                                && (poc.ClassProfile.EducationLevelID == EducationLevelID || EducationLevelID == 0)
                                && (poc.SchoolID == SchoolID)
                                && (poc.AcademicYearID == AcademicYearID)
                                && (mbo.AcademicYearID == AcademicYearID)
                                && (poc.ClassID == ClassID || ClassID == 0)
                                && (poc.PupilProfile.FullName.ToLower().Contains(LowerFullName))
                                && (mbo.HealthPeriodID == HealthPeriodID)
                                && (poc.PupilProfile.IsActive == true)
                                && (poc.Status != SystemParamsInFile.PUPIL_STATUS_MOVED_TO_OTHER_CLASS)
                                select new MonitoringBookSearch
                                {
                                    MonitoringBookID = mbo.MonitoringBookID,
                                    EducationLevelID = poc.ClassProfile.EducationLevelID,
                                    AcademicYearID = poc.AcademicYearID,
                                    SchoolID = poc.SchoolID,
                                    PupilID = poc.PupilID,
                                    PupilCode = poc.PupilProfile.PupilCode,
                                    FullName = poc.PupilProfile.FullName,
                                    BrithDate = poc.PupilProfile.BirthDate,
                                    Sex = poc.PupilProfile.Genre,
                                    ClassID = poc.ClassID,
                                    ClassName = poc.ClassProfile.DisplayName,
                                    HealthPeriodID = mbo.HealthPeriodID,
                                    MonitoringDate = mbo.MonitoringDate,
                                    IsAction = true,
                                    PupilShortName = poc.PupilProfile.Name
                                };
                    return query;
                }

            }
            else
            {
                List<int> lstPupilDK = (from poc in PupilOfClassRepository.All
                                        join mb in MonitoringBookRepository.All on poc.PupilID equals mb.PupilID
                                        where (poc.ClassProfile.EducationLevelID == EducationLevelID || EducationLevelID == 0)
                                        && (poc.SchoolID == SchoolID)
                                        && (poc.AcademicYearID == AcademicYearID)
                                        && (mb.AcademicYearID == AcademicYearID)
                                        && (poc.ClassID == ClassID || ClassID == 0)
                                        && (poc.PupilProfile.FullName.ToLower().Contains(LowerFullName))
                                        && (mb.HealthPeriodID == HealthPeriodID)
                                        && (poc.PupilProfile.IsActive == true)
                                        && (poc.Status != SystemParamsInFile.PUPIL_STATUS_MOVED_TO_OTHER_CLASS)
                                        select mb.PupilID).Distinct().ToList();

                var query2 = from poc in PupilOfClassRepository.All
                             join mb in MonitoringBookRepository.All.Where(o => (o.HealthPeriodID == HealthPeriodID && o.AcademicYearID == AcademicYearID)) on poc.PupilID equals mb.PupilID into PupilOfClassMonitoringBook
                             from pmb in PupilOfClassMonitoringBook.DefaultIfEmpty()
                             where (poc.ClassProfile.EducationLevelID >= min && poc.ClassProfile.EducationLevelID <= max)
                             && (poc.ClassProfile.EducationLevelID == EducationLevelID || EducationLevelID == 0)
                             && (poc.SchoolID == SchoolID)
                             && (poc.AcademicYearID == AcademicYearID)
                             && (poc.ClassID == ClassID || ClassID == 0)
                             && (poc.PupilProfile.FullName.ToLower().Contains(LowerFullName))
                             && (poc.PupilProfile.IsActive == true)
                             && (poc.Status != SystemParamsInFile.PUPIL_STATUS_MOVED_TO_OTHER_CLASS)
                             select new MonitoringBookSearch
                             {
                                 EducationLevelID = poc.ClassProfile.EducationLevelID,
                                 AcademicYearID = poc.AcademicYearID,
                                 SchoolID = poc.SchoolID,
                                 PupilID = poc.PupilID,
                                 PupilCode = poc.PupilProfile.PupilCode,
                                 FullName = poc.PupilProfile.FullName,
                                 BrithDate = poc.PupilProfile.BirthDate,
                                 Sex = poc.PupilProfile.Genre,
                                 ClassID = poc.ClassID,
                                 ClassName = poc.ClassProfile.DisplayName,
                                 HealthPeriodID = pmb.HealthPeriodID,
                                 MonitoringDate = pmb.MonitoringDate,
                                 MonitoringBookID = pmb.MonitoringBookID,
                                 IsAction = false,
                                 PupilShortName = poc.PupilProfile.Name
                             };

                //lst2 = query2;
                //// Lấy ra các phần tử thuộc list2 theo PupilID
                //var listPupilIDFromL2 = lst2.Select(o => o.PupilID);
                //// Lấy ra các phần tử thuộc list 1 mà không thuộc list2 theo PupilID
                //lst3 = lst1.Where(o => !listPupilIDFromL2.Contains(o.PupilID));
                var Query = query2.Where(o => !lstPupilDK.Contains(o.PupilID));
                return Query;
            }
        }

        #endregion

        #region Update IsSMS is TRUE
        /// <summary>
        /// Anhvd
        /// </summary>
        /// <param name="dic"></param>
        /// <param name="pupilIds"></param>
        /// <returns></returns>
        public bool EnableIsSMS(IDictionary<string, object> dic, List<int> pupilIds)
        {
            IQueryable<MonitoringBook> lstMonitoring = this.Search(dic);
            for (int i = 0; i < pupilIds.Count; i++)
            {
                int pupilId = pupilIds[i];
                IQueryable<MonitoringBook> lstMonitorPupil = lstMonitoring.Where(o => o.PupilID == pupilId);
                foreach (var item in lstMonitorPupil)
                {
                    item.IsSMS = true;
                    this.Update(item);
                }
            }
            this.Save();
            return true;
        }
        #endregion

        #region get list by weight, height of pupil of class
        /// <summary>
        /// get list mothlyGrowth
        /// </summary>
        /// <auth>HaiVT</auth>
        /// <date>25/04/2013</date>
        /// <param name="dic"></param>
        /// <returns></returns>
        public IQueryable<MonthlyGrowthBO> GetListMonthlyGrowth(IDictionary<string, object> dic)
        {
            int classID = Utils.GetInt(dic, "classID");
            int academicYearID = Utils.GetInt(dic, "AcademicYearID");
            DateTime? monthSelect = Utils.GetDateTime(dic, "monthSelect");
            IQueryable<MonthlyGrowthBO> lstMonthlyGrowthBO = from pp in PupilProfileBusiness.All
                                                             join mb in MonitoringBookBusiness.All.Where(p => p.MonitoringType == GlobalConstants.MONITORINGBOOK_TYPE_MONTH
                                                                                                        && p.MonitoringDate.HasValue ? p.MonitoringDate.Value.Month == monthSelect.Value.Month : true
                                                                                                        && p.MonitoringDate.HasValue ? p.MonitoringDate.Value.Year == monthSelect.Value.Year : true)
                                                                       on pp.PupilProfileID equals mb.PupilID into monitoring
                                                             from mbl in monitoring.DefaultIfEmpty()
                                                             join pt in PhysicalTestBusiness.All.Where(p => p.Date.Month == monthSelect.Value.Month
                                                                            && p.Date.Year == monthSelect.Value.Year && p.IsActive == true)
                                                                            on mbl.MonitoringBookID equals pt.MonitoringBookID into physicalTest
                                                             from ptl in physicalTest.DefaultIfEmpty()
                                                             join poc in PupilOfClassBusiness.All on pp.PupilProfileID equals poc.PupilID
                                                             orderby poc.OrderInClass, pp.FullName
                                                             where poc.ClassID == classID && poc.AcademicYearID == academicYearID
                                                             select new MonthlyGrowthBO()
                                                             {
                                                                 MonitoringBookID = mbl.MonitoringBookID,
                                                                 PupilID = pp.PupilProfileID,
                                                                 BirthDate = pp.BirthDate,
                                                                 Genre = pp.Genre,
                                                                 Image = pp.Image,
                                                                 FullName = pp.FullName,
                                                                 Height = ptl.Height,
                                                                 Weight = ptl.Weight,
                                                                 Status = poc.Status,
                                                                 Nutrition = ptl.Nutrition,
                                                                 Solution = mbl.Solution
                                                             };
            return lstMonthlyGrowthBO;
        }
        #endregion
        /// <summary>
        /// phan he phhs - get list mothlyGrowth of pupil
        /// </summary>
        /// <modifier>HaiVT</modifier>
        /// <date>22/06/2013</date>
        /// <param name="dic"></param>
        /// <returns></returns>
        public IQueryable<MonthlyGrowthBO> GetListMonthlyGrowthPHHS(IDictionary<string, object> dic)
        {
            int classID = Utils.GetInt(dic, "classID");
            int academicYearID = Utils.GetInt(dic, "AcademicYearID");
            int pupilID = Utils.GetInt(dic, "pupilID");
            IQueryable<MonthlyGrowthBO> lstMonthlyGrowthBO = from mb in MonitoringBookBusiness.All.Where(p => p.MonitoringType == GlobalConstants.MONITORINGBOOK_TYPE_MONTH)
                                                             join pt in PhysicalTestBusiness.All.Where(p => p.IsActive == true) on mb.MonitoringBookID equals pt.MonitoringBookID into physicalTest
                                                             from ptl in physicalTest.DefaultIfEmpty()
                                                             join poc in PupilOfClassBusiness.All on mb.PupilID equals poc.PupilID
                                                             where poc.ClassID == classID && poc.AcademicYearID == academicYearID && mb.PupilID == pupilID
                                                             select new MonthlyGrowthBO()
                                                             {
                                                                 Height = ptl.Height,
                                                                 Weight = ptl.Weight,
                                                                 Nutrition = ptl.Nutrition,
                                                                 Solution = mb.Solution,
                                                                 MonitoringDate = mb.MonitoringDate
                                                             };
            return lstMonthlyGrowthBO;
        }

        #region get all monitoringBook of Pupil by pupilID
        /// <summary>
        /// get all monitoringBook by pupilID
        /// </summary>
        /// <auth>HaiVT</auth>
        /// <date>25/04/2013</date>
        /// <param name="dic"></param>
        /// <returns></returns>
        public IQueryable<MonthlyGrowthBO> GetAllMonitoringBookByPupilID(IDictionary<string, object> dic)
        {
            int pupilID = Utils.GetInt(dic, "pupilID");
            DateTime? birthday = Utils.GetDateTime(dic, "birthday");
            IQueryable<MonthlyGrowthBO> lstMonthlyGrowthBO = from mb in MonitoringBookBusiness.All
                                                             join pt in PhysicalTestBusiness.All on mb.MonitoringBookID equals pt.MonitoringBookID
                                                             join ot in OverallTestBusiness.All on mb.MonitoringBookID equals ot.MonitoringBookID into overallTest
                                                             from otl in overallTest.DefaultIfEmpty()
                                                             orderby pt.Date.Month
                                                             where mb.PupilID == pupilID && mb.MonitoringType == GlobalConstants.MONITORINGBOOK_TYPE_MONTH
                                                                && pt.IsActive == true
                                                             select new MonthlyGrowthBO()
                                                             {
                                                                 Height = pt.Height,
                                                                 Weight = pt.Weight,
                                                                 HistoryYourSelf = otl.HistoryYourSelf,
                                                                 Month = ((pt.Date.Year - birthday.Value.Year) * 12) + pt.Date.Month - birthday.Value.Month
                                                             };

            return lstMonthlyGrowthBO;

        }

        public IQueryable<MonthlyGrowthBO> GetAllMonitoringBookHistoryYourSelfByPupilID(IDictionary<string, object> dic)
        {
            int pupilID = Utils.GetInt(dic, "pupilID");
            DateTime? birthday = Utils.GetDateTime(dic, "birthday");
            IQueryable<MonthlyGrowthBO> lstMonthlyGrowthBO = from mb in MonitoringBookBusiness.All
                                                             //join pt in PhysicalTestBusiness.All on mb.MonitoringBookID equals pt.MonitoringBookID
                                                             join ot in OverallTestBusiness.All on mb.MonitoringBookID equals ot.MonitoringBookID into overallTest
                                                             from otl in overallTest.DefaultIfEmpty()
                                                             orderby mb.MonitoringDate
                                                             where mb.PupilID == pupilID && mb.MonitoringType == GlobalConstants.MONITORINGBOOK_TYPE_PERIOD
                                                                && otl.IsActive
                                                             select new MonthlyGrowthBO()
                                                             {
                                                                 //Height = pt.Height,
                                                                 //Weight = pt.Weight,
                                                                 HistoryYourSelf = otl.HistoryYourSelf,
                                                                 //Month = ((pt.Date.Year - birthday.Value.Year) * 12) + pt.Date.Month - birthday.Value.Month
                                                             };

            return lstMonthlyGrowthBO;

        }

        /// <summary>
        /// Load By ClassID
        /// </summary>
        /// <auth>Hai</auth>
        /// <date>25/04/2013</date>
        /// <param name="dic"></param>
        /// <returns></returns>
        public IQueryable<MonthlyGrowthBO> GetAllMonitoringBookByClassID(IDictionary<string, object> dic)
        {
            int classID = 0;
            int schoolID = 0;
            int academicYearID = 0;
            if (dic.Keys.Contains("ClassID"))
            {
                classID = Utils.GetInt(dic, "ClassID");
            }
            if (dic.Keys.Contains("SchoolID"))
            {
                schoolID = Utils.GetInt(dic, "SchoolID");
            }
            if (dic.Keys.Contains("AcademicYearID"))
            {
                academicYearID = Utils.GetInt(dic, "AcademicYearID");
            }
            IQueryable<MonthlyGrowthBO> lstMonthlyGrowthBO = from mb in MonitoringBookBusiness.All
                                                             join pt in PhysicalTestBusiness.All on mb.MonitoringBookID equals pt.MonitoringBookID
                                                             join ot in OverallTestBusiness.All on mb.MonitoringBookID equals ot.MonitoringBookID into overallTest
                                                             join pf in PupilProfileBusiness.All on mb.PupilID equals pf.PupilProfileID
                                                             join cls in PupilOfClassBusiness.All on pf.PupilProfileID equals cls.PupilID
                                                             from otl in overallTest.DefaultIfEmpty()
                                                             orderby pt.Date.Month
                                                             where mb.MonitoringType == GlobalConstants.MONITORINGBOOK_TYPE_MONTH
                                                             && pt.IsActive == true
                                                             && pf.IsActive == true
                                                             && (cls.ClassID == classID || classID == 0)
                                                             && (cls.SchoolID == schoolID || schoolID == 0)
                                                             && (cls.Status == GlobalConstants.PUPIL_STATUS_STUDYING)
                                                             && (cls.AcademicYearID == academicYearID || academicYearID == 0)
                                                             && (mb.AcademicYearID == academicYearID || academicYearID == 0)
                                                             select new MonthlyGrowthBO()
                                                             {
                                                                 Height = pt.Height,
                                                                 Weight = pt.Weight,
                                                                 HistoryYourSelf = otl.HistoryYourSelf,
                                                                 Month = ((pt.Date.Year - pf.BirthDate.Year) * 12) + pt.Date.Month - pf.BirthDate.Month,
                                                                 PupilID = pf.PupilProfileID,
                                                                 FullName = pf.FullName,
                                                                 Genre = pf.Genre
                                                             };

            return lstMonthlyGrowthBO;

        }
        #endregion

        /// <summary>
        /// Vunx 4 - 26/06/2013 - Get infor to set in form create healthtest 
        /// </summary>
        /// <param name="dic"></param>
        /// <param name="isActive"></param>
        /// <param name="maxDateClass"></param>
        /// <param name="maxDateSchool"></param>
        public void GetCreateInfo(IDictionary<string, object> dic, ref bool isActive, ref DateTime? maxDateClass, ref DateTime? maxDateSchool, ref string unitName)
        {
            int AcademicYearID = Utils.GetInt(dic, "AcademicYearID");
            int SchoolID = Utils.GetInt(dic, "SchoolID");
            int ClassID = Utils.GetInt(dic, "ClassID");
            int PupilID = Utils.GetInt(dic, "PupilID");

            IDictionary<string, object> dictionary = new Dictionary<string, object>();
            dictionary["AcademicYearID"] = AcademicYearID;
            dictionary["SchoolID"] = SchoolID;
            IQueryable<MonitoringBook> listSchool = Search(dictionary);
            if (listSchool.Count() != 0)
            {
                maxDateSchool = listSchool.Max(x => x.MonitoringDate);
                unitName = listSchool.FirstOrDefault().UnitName;
                IQueryable<MonitoringBook> listClass = listSchool.Where(x => x.ClassID == ClassID);
                if (listClass.Count() != 0)
                {
                    maxDateClass = listClass.Max(x => x.MonitoringDate);
                    unitName = listClass.FirstOrDefault().UnitName;
                }
                MonitoringBook lstPupil = listSchool.Where(x => x.PupilID == PupilID).FirstOrDefault();
                if (lstPupil != null)
                {
                    isActive = true;
                }
            }
        }
        #endregion

        #region //new
        #region  ValidatePhysicalInsert
        public void ValidatePhysicalInsert(FormCollection frm, Dictionary<string, object> dicInsert, int LogChangeID)
        {
            int schoolID = Utils.GetInt(dicInsert, "SchoolID");
            int academicYearID = Utils.GetInt(dicInsert, "AcademicYearID");
            int healthPeriodID = Utils.GetInt(dicInsert, "HealthPeriodID");
            int classID = Utils.GetInt(dicInsert, "ClassID");
            int EducationLevelID = ClassProfileBusiness.GetEducationLevelIDByClassID(classID);

            List<PupilOfClassBO> lstPOCDB = PupilOfClassBusiness.GetPupilInClass(classID).Distinct().ToList();
            List<PupilOfClassBO> lstPOC = new List<PupilOfClassBO>();
            lstPOC = lstPOCDB.OrderBy(x => x.PupilID).ToList();
            PupilOfClassBO objPOC = null;

            List<MonitoringBookBO> lstMoni = new List<MonitoringBookBO>();
            MonitoringBookBO objMoni = null;
            List<PhysicalTestBO> lstInput = new List<PhysicalTestBO>();
            PhysicalTestBO objInput = null;
            List<int> lstPupilID = new List<int>();
            int pupilID = 0;
            //DateTime dateNow = DateTime.Now;

            for (int i = 0; i < lstPOC.Count; i++)
            {
                objPOC = lstPOC[i];
                pupilID = objPOC.PupilID;
                if (!"on".Equals(frm["chk_" + pupilID]) || (objPOC.Status != GlobalConstants.PUPIL_STATUS_STUDYING))
                {
                    continue;
                }
                lstPupilID.Add(pupilID);

                objMoni = new MonitoringBookBO();
                objMoni.AcademicYearID = academicYearID;
                objMoni.SchoolID = schoolID;
                objMoni.ClassID = objPOC.ClassID;
                objMoni.PupilID = pupilID;
                objMoni.BirthDate = objPOC.Birthday;
                objMoni.Genre = objPOC.Genre.Value;

                objMoni.MonitoringType = 2; // theo dõi theo đợt
                objMoni.CreateDate = DateTime.Now;
                objMoni.HealthPeriodID = healthPeriodID;
                objMoni.EducationLevelID = EducationLevelID;

                objInput = new PhysicalTestBO();
                objInput.PupilProFileID = pupilID;

                objInput.Height = !string.IsNullOrWhiteSpace(frm["txtHeight_" + pupilID]) ? Utils.ToDecimal(frm["txtHeight_" + pupilID]) : null;
                objInput.Weight = !string.IsNullOrWhiteSpace(frm["txtWeight_" + pupilID]) ? Utils.ToDecimal(frm["txtWeight_" + pupilID]) : null;
                objInput.Breast = !string.IsNullOrWhiteSpace(frm["txtBreast_" + pupilID]) ? Utils.ToDecimal(frm["txtBreast_" + pupilID]) : null;

                objInput.PhysicalClassification = !string.IsNullOrEmpty(frm["txtPhysicalClassification_" + pupilID]) ? Int32.Parse(frm["txtPhysicalClassification_" + pupilID]) : 0;

                if (objInput.PhysicalClassification == 0 || objInput.PhysicalClassification > 5)
                {
                    objInput.PhysicalClassification = null;
                }

                objInput.Date = DateTime.Now;
                objInput.CreateDate = DateTime.Now;
                objInput.ModifineDate = DateTime.Now;
                objInput.LogID = LogChangeID;
                objInput.IsActive = true;

                lstMoni.Add(objMoni);
                lstInput.Add(objInput);
            }

            dicInsert.Add("EducationLevelID", EducationLevelID);
            dicInsert.Add("lstPupilID", lstPupilID);

            List<EyeTestBO> lstEyeTest = new List<EyeTestBO>();
            List<ENTTestBO> lstEntTest = new List<ENTTestBO>();
            List<SpineTestBO> lstSpineTest = new List<SpineTestBO>();
            List<DentalTestBO> lstDentalTest = new List<DentalTestBO>();
            List<OverallTestBO> lstOverallTest = new List<OverallTestBO>();

            this.InsertOrUpdateValueHealthTest(lstMoni,
                lstInput, lstEyeTest, lstEntTest, lstSpineTest, lstDentalTest, lstOverallTest, dicInsert);
        }
        #endregion

        #region  ValidateEyeInsert
        public void ValidateEyeInsert(FormCollection frm, Dictionary<string, object> dicInsert, int LogChangeID)
        {
            int schoolID = Utils.GetInt(dicInsert, "SchoolID");
            int academicYearID = Utils.GetInt(dicInsert, "AcademicYearID");
            int healthPeriodID = Utils.GetInt(dicInsert, "HealthPeriodID");
            int classID = Utils.GetInt(dicInsert, "ClassID");
            int EducationLevelID = ClassProfileBusiness.GetEducationLevelIDByClassID(classID);

            List<PupilOfClassBO> lstPOCDB = PupilOfClassBusiness.GetPupilInClass(classID).Distinct().ToList();
            List<PupilOfClassBO> lstPOC = new List<PupilOfClassBO>();
            lstPOC = lstPOCDB.OrderBy(x => x.PupilID).ToList();
            PupilOfClassBO objPOC = null;

            List<MonitoringBookBO> lstMoni = new List<MonitoringBookBO>();
            MonitoringBookBO objMoni = null;
            List<EyeTestBO> lstInput = new List<EyeTestBO>();
            EyeTestBO objInput = null;
            List<int> lstPupilID = new List<int>();
            int pupilID = 0;

            for (int i = 0; i < lstPOC.Count; i++)
            {
                objPOC = lstPOC[i];
                pupilID = objPOC.PupilID;
                if (!"on".Equals(frm["chk_" + pupilID]) || objPOC.Status != GlobalConstants.PUPIL_STATUS_STUDYING)
                {
                    continue;
                }
                lstPupilID.Add(pupilID);

                objMoni = new MonitoringBookBO();
                objMoni.AcademicYearID = academicYearID;
                objMoni.SchoolID = schoolID;
                objMoni.ClassID = objPOC.ClassID;
                objMoni.PupilID = pupilID;
                objMoni.BirthDate = objPOC.Birthday;
                objMoni.Genre = objPOC.Genre.Value;
                objMoni.MonitoringDate = DateTime.Now;
                objMoni.MonitoringType = 2; // theo dõi theo đợt
                objMoni.CreateDate = DateTime.Now;
                objMoni.HealthPeriodID = healthPeriodID;
                objMoni.EducationLevelID = EducationLevelID;

                objInput = new EyeTestBO();
                objInput.PupilProFileID = pupilID;
                string rEye = !string.IsNullOrEmpty(frm["txtNoRightEye_" + pupilID]) ? frm["txtNoRightEye_" + pupilID] : string.Empty;
                if (!string.IsNullOrEmpty(rEye))
                {
                    objInput.REye = Int32.Parse(frm["txtNoRightEye_" + pupilID]);
                }

                string lEye = !string.IsNullOrEmpty(frm["txtNoLeftEye_" + pupilID]) ? frm["txtNoLeftEye_" + pupilID] : string.Empty;
                if (!string.IsNullOrEmpty(lEye))
                {
                    objInput.LEye = Int32.Parse(frm["txtNoLeftEye_" + pupilID]);
                }

                string rIsEye = !string.IsNullOrEmpty(frm["txtRightIsEye_" + pupilID]) ? frm["txtRightIsEye_" + pupilID] : string.Empty;
                if (!string.IsNullOrEmpty(rIsEye))
                {
                    objInput.RIsEye = Int32.Parse(frm["txtRightIsEye_" + pupilID]);
                }

                string lIsEye = !string.IsNullOrEmpty(frm["txtLeftIsEye_" + pupilID]) ? frm["txtLeftIsEye_" + pupilID] : string.Empty;
                if (!string.IsNullOrEmpty(lIsEye))
                {
                    objInput.LIsEye = Int32.Parse(frm["txtLeftIsEye_" + pupilID]);
                }

                objInput.IsTreatment = false;
                if (!string.IsNullOrEmpty(lIsEye) || !string.IsNullOrEmpty(rIsEye))
                    objInput.IsTreatment = true; // có kính

                objInput.RMyopic = !string.IsNullOrEmpty(frm["txtRMyopic_" + pupilID]) ? Utils.ToDecimal(frm["txtRMyopic_" + pupilID]) : null;
                objInput.LMyopic = !string.IsNullOrEmpty(frm["txtLMyopic_" + pupilID]) ? Utils.ToDecimal(frm["txtLMyopic_" + pupilID]) : null;
                if (objInput.RMyopic.HasValue || objInput.LMyopic.HasValue)
                    objInput.IsMyopic = true;
                if (!objInput.RMyopic.HasValue && !objInput.LMyopic.HasValue)
                    objInput.IsMyopic = false;

                objInput.RFarsightedness = !string.IsNullOrEmpty(frm["txtRFarsightedness_" + pupilID]) ? Utils.ToDecimal(frm["txtRFarsightedness_" + pupilID]) : null;
                objInput.LFarsightedness = !string.IsNullOrEmpty(frm["txtLFarsightedness_" + pupilID]) ? Utils.ToDecimal(frm["txtLFarsightedness_" + pupilID]) : null;
                objInput.IsFarsightedness = false;
                if (objInput.RFarsightedness.HasValue || objInput.LFarsightedness.HasValue)
                    objInput.IsFarsightedness = true;

                objInput.RAstigmatism = !string.IsNullOrEmpty(frm["txtRAstigmatism_" + pupilID]) ? Utils.ToDecimal(frm["txtRAstigmatism_" + pupilID]) : null;
                objInput.LAstigmatism = !string.IsNullOrEmpty(frm["txtLAstigmatism_" + pupilID]) ? Utils.ToDecimal(frm["txtLAstigmatism_" + pupilID]) : null;
                objInput.IsAstigmatism = false;
                if (objInput.LAstigmatism.HasValue || objInput.RAstigmatism.HasValue)
                    objInput.IsAstigmatism = true;

                objInput.Other = !string.IsNullOrEmpty(frm["txtOther_" + pupilID]) ? frm["txtOther_" + pupilID] : string.Empty;
                objInput.CreateDate = DateTime.Now;
                objInput.ModifiedDate = DateTime.Now;
                objInput.LogID = LogChangeID;
                objInput.IsActive = true;

                lstMoni.Add(objMoni);
                lstInput.Add(objInput);
            }

            dicInsert.Add("EducationLevelID", EducationLevelID);
            dicInsert.Add("lstPupilID", lstPupilID);

            List<PhysicalTestBO> lstPhysicalTest = new List<PhysicalTestBO>();
            List<ENTTestBO> lstEntTest = new List<ENTTestBO>();
            List<SpineTestBO> lstSpineTest = new List<SpineTestBO>();
            List<DentalTestBO> lstDentalTest = new List<DentalTestBO>();
            List<OverallTestBO> lstOverallTest = new List<OverallTestBO>();

            this.InsertOrUpdateValueHealthTest(lstMoni,
                lstPhysicalTest, lstInput, lstEntTest, lstSpineTest, lstDentalTest, lstOverallTest, dicInsert);
        }
        #endregion

        #region  ValidateEntInsert
        public void ValidateEntInsert(FormCollection frm, Dictionary<string, object> dicInsert, int LogChangeID)
        {
            int schoolID = Utils.GetInt(dicInsert, "SchoolID");
            int academicYearID = Utils.GetInt(dicInsert, "AcademicYearID");
            int healthPeriodID = Utils.GetInt(dicInsert, "HealthPeriodID");
            int classID = Utils.GetInt(dicInsert, "ClassID");
            int EducationLevelID = ClassProfileBusiness.GetEducationLevelIDByClassID(classID);

            List<PupilOfClassBO> lstPOCDB = PupilOfClassBusiness.GetPupilInClass(classID).Distinct().ToList();
            List<PupilOfClassBO> lstPOC = new List<PupilOfClassBO>();
            lstPOC = lstPOCDB.OrderBy(x => x.PupilID).ToList();
            PupilOfClassBO objPOC = null;

            List<MonitoringBookBO> lstMoni = new List<MonitoringBookBO>();
            MonitoringBookBO objMoni = null;
            List<ENTTestBO> lstInput = new List<ENTTestBO>();
            ENTTestBO objInput = null;
            List<int> lstPupilID = new List<int>();
            int pupilID = 0;

            for (int i = 0; i < lstPOC.Count; i++)
            {
                objPOC = lstPOC[i];
                pupilID = objPOC.PupilID;
                if (!"on".Equals(frm["chk_" + pupilID]) || objPOC.Status != GlobalConstants.PUPIL_STATUS_STUDYING)
                {
                    continue;
                }
                lstPupilID.Add(pupilID);

                objMoni = new MonitoringBookBO();
                objMoni.AcademicYearID = academicYearID;
                objMoni.SchoolID = schoolID;
                objMoni.ClassID = objPOC.ClassID;
                objMoni.PupilID = pupilID;
                objMoni.BirthDate = objPOC.Birthday;
                objMoni.Genre = objPOC.Genre.Value;
                objMoni.MonitoringDate = DateTime.Now;

                objMoni.MonitoringType = 2; // theo dõi theo đợt
                objMoni.CreateDate = DateTime.Now;
                objMoni.HealthPeriodID = healthPeriodID;
                objMoni.EducationLevelID = EducationLevelID;

                objInput = new ENTTestBO();
                objInput.PupilProFileID = pupilID;
                string lEnt = !string.IsNullOrEmpty(frm["txtLCapacityHearing_" + pupilID]) ? frm["txtLCapacityHearing_" + pupilID] : string.Empty;
                if (!string.IsNullOrEmpty(lEnt))
                {
                    objInput.LCapacityHearing = Int32.Parse(lEnt);
                }
                string rEnt = !string.IsNullOrEmpty(frm["txtRCapacityHearing_" + pupilID]) ? frm["txtRCapacityHearing_" + pupilID] : string.Empty;
                if (!string.IsNullOrEmpty(rEnt))
                {
                    objInput.RCapacityHearing = Int32.Parse(rEnt);
                }
                objInput.IsDeaf = !string.IsNullOrEmpty(frm["txtIsDeaf_" + pupilID]) ? true : false;
                objInput.IsSick = !string.IsNullOrEmpty(frm["txtIsSick_" + pupilID]) ? true : false;
                if (objInput.IsSick == true)
                {
                    objInput.Description = !string.IsNullOrEmpty(frm["txtDescription_" + pupilID]) ? frm["txtDescription_" + pupilID] : string.Empty;
                }
                else
                    objInput.Description = string.Empty;

                objInput.CreatedDate = DateTime.Now;
                objInput.IsActive = true;
                objInput.ModifiedDate = DateTime.Now;
                objInput.LogID = LogChangeID;

                lstMoni.Add(objMoni);
                lstInput.Add(objInput);
            }

            dicInsert.Add("EducationLevelID", EducationLevelID);
            dicInsert.Add("lstPupilID", lstPupilID);

            List<PhysicalTestBO> lstPhysicalTest = new List<PhysicalTestBO>();
            List<EyeTestBO> lstEyeTest = new List<EyeTestBO>();
            List<SpineTestBO> lstSpineTest = new List<SpineTestBO>();
            List<DentalTestBO> lstDentalTest = new List<DentalTestBO>();
            List<OverallTestBO> lstOverallTest = new List<OverallTestBO>();

            this.InsertOrUpdateValueHealthTest(lstMoni,
                lstPhysicalTest, lstEyeTest, lstInput, lstSpineTest, lstDentalTest, lstOverallTest, dicInsert);
        }
        #endregion

        #region  ValidateSpineInsert
        public void ValidateSpineInsert(FormCollection frm, Dictionary<string, object> dicInsert, int LogChangeID)
        {
            int schoolID = Utils.GetInt(dicInsert, "SchoolID");
            int academicYearID = Utils.GetInt(dicInsert, "AcademicYearID");
            int healthPeriodID = Utils.GetInt(dicInsert, "HealthPeriodID");
            int classID = Utils.GetInt(dicInsert, "ClassID");
            int EducationLevelID = ClassProfileBusiness.GetEducationLevelIDByClassID(classID);

            List<PupilOfClassBO> lstPOCDB = PupilOfClassBusiness.GetPupilInClass(classID).Distinct().ToList();
            List<PupilOfClassBO> lstPOC = new List<PupilOfClassBO>();
            lstPOC = lstPOCDB.OrderBy(x => x.PupilID).ToList();
            PupilOfClassBO objPOC = null;

            List<MonitoringBookBO> lstMoni = new List<MonitoringBookBO>();
            MonitoringBookBO objMoni = null;
            List<SpineTestBO> lstInput = new List<SpineTestBO>();
            SpineTestBO objInput = null;

            List<MonitoringBook> lstMoniCheckExists = (from mn in MonitoringBookBusiness.All
                                                       join oa in SpineTestBusiness.All on mn.MonitoringBookID equals oa.MonitoringBookID
                                                       where mn.SchoolID == schoolID &&
                                                             mn.AcademicYearID == academicYearID &&
                                                             mn.HealthPeriodID == healthPeriodID &&
                                                             mn.ClassID == classID
                                                       select mn).ToList();
            List<int> lstPupilIDExists = lstMoniCheckExists.Select(x => x.PupilID).Distinct().ToList();

            List<int> lstPupilID = new List<int>();
            int pupilID = 0;

            for (int i = 0; i < lstPOC.Count; i++)
            {
                objPOC = lstPOC[i];
                pupilID = objPOC.PupilID;
                if (!"on".Equals(frm["chk_" + pupilID]) || objPOC.Status != GlobalConstants.PUPIL_STATUS_STUDYING)
                {
                    continue;
                }
                lstPupilID.Add(pupilID);

                objMoni = new MonitoringBookBO();
                objMoni.AcademicYearID = academicYearID;
                objMoni.SchoolID = schoolID;
                objMoni.ClassID = objPOC.ClassID;
                objMoni.PupilID = pupilID;                
                objMoni.MonitoringType = 2;
                objMoni.CreateDate = DateTime.Now;
                objMoni.HealthPeriodID = healthPeriodID;
                objMoni.EducationLevelID = EducationLevelID;

                objInput = new SpineTestBO();
                objInput.MonitoringBookID = 1;
                objInput.PupilProFileID = pupilID;

                string a = frm["txtInput_" + pupilID].ToString();

                if (frm["txtInput_" + pupilID].ToString() == "x" || frm["txtInput_" + pupilID].ToString() == "X")
                {
                    objInput.IsDeformity = true;
                }
                else
                {
                    objInput.IsDeformity = false;
                }
                objInput.ExaminationStraight = !string.IsNullOrWhiteSpace(frm["txtStraight_" + pupilID]) ? frm["txtStraight_" + pupilID] : "";
                objInput.ExaminationItalic = !string.IsNullOrWhiteSpace(frm["txtItalic_" + pupilID]) ? frm["txtItalic_" + pupilID] : "";
                objInput.Other = !string.IsNullOrEmpty(frm["txtOther_" + pupilID]) ? frm["txtOther_" + pupilID] : "";
                objInput.CreateDate = DateTime.Now;
                objInput.ModifineDate = DateTime.Now;
                objInput.LogID = LogChangeID;
                objInput.IsActive = true;

                if (lstPupilIDExists.Where(x => x == pupilID).Count() == 0)
                    if (string.IsNullOrEmpty(objInput.ExaminationItalic)
                        && string.IsNullOrEmpty(objInput.ExaminationStraight)
                        && string.IsNullOrEmpty(objInput.Other)
                        && objInput.IsDeformity == false)
                        continue;

                lstMoni.Add(objMoni);
                lstInput.Add(objInput);
            }

            dicInsert.Add("EducationLevelID", EducationLevelID);
            dicInsert.Add("lstPupilID", lstPupilID);

            List<EyeTestBO> lstEyeTest = new List<EyeTestBO>();
            List<ENTTestBO> lstEntTest = new List<ENTTestBO>();
            List<PhysicalTestBO> lstPhysicalTest = new List<PhysicalTestBO>();
            List<DentalTestBO> lstDentalTest = new List<DentalTestBO>();
            List<OverallTestBO> lstOverallTest = new List<OverallTestBO>();

            this.InsertOrUpdateValueHealthTest(lstMoni, lstPhysicalTest, lstEyeTest, lstEntTest, lstInput, lstDentalTest, lstOverallTest, dicInsert);
        }
        #endregion

        #region  ValidateDentalInsert
        public void ValidateDentalInsert(FormCollection frm, Dictionary<string, object> dicInsert, int LogChangeID)
        {
            int schoolID = Utils.GetInt(dicInsert, "SchoolID");
            int academicYearID = Utils.GetInt(dicInsert, "AcademicYearID");
            int healthPeriodID = Utils.GetInt(dicInsert, "HealthPeriodID");
            int classID = Utils.GetInt(dicInsert, "ClassID");
            int EducationLevelID = ClassProfileBusiness.GetEducationLevelIDByClassID(classID);

            List<PupilOfClassBO> lstPOCDB = PupilOfClassBusiness.GetPupilInClass(classID).Distinct().ToList();
            List<PupilOfClassBO> lstPOC = new List<PupilOfClassBO>();
            lstPOC = lstPOCDB.OrderBy(x => x.PupilID).ToList();

            int PupilIDOne = !string.IsNullOrEmpty(frm["PupilIDOne"]) ? int.Parse(frm["PupilIDOne"]) : 0;
            if (PupilIDOne != 0)
            {
                lstPOC = lstPOC.Where(x => x.PupilID == PupilIDOne).ToList();
            }
            PupilOfClassBO objPOC = null;

            List<MonitoringBookBO> lstMoni = new List<MonitoringBookBO>();
            MonitoringBookBO objMoni = null;
            List<DentalTestBO> lstInput = new List<DentalTestBO>();
            DentalTestBO objInput = null;

            List<MonitoringBook> lstMoniCheckExists = (from mn in MonitoringBookBusiness.All
                                                       join oa in DentalTestBusiness.All on mn.MonitoringBookID equals oa.MonitoringBookID
                                                       where mn.SchoolID == schoolID &&
                                                             mn.AcademicYearID ==academicYearID &&
                                                             mn.HealthPeriodID == healthPeriodID &&
                                                             mn.ClassID == classID
                                                       select mn).ToList();
            List<int> lstPupilIDExists = lstMoniCheckExists.Select(x => x.PupilID).Distinct().ToList();

            List<int> lstPupilID = new List<int>();
            int pupilID = 0;

            for (int i = 0; i < lstPOC.Count; i++)
            {
                objPOC = lstPOC[i];
                pupilID = objPOC.PupilID;
                if (!"on".Equals(frm["chk_" + pupilID]) || objPOC.Status != GlobalConstants.PUPIL_STATUS_STUDYING)
                {
                    continue;
                }
                lstPupilID.Add(pupilID);

                objMoni = new MonitoringBookBO();
                objMoni.AcademicYearID = academicYearID;
                objMoni.SchoolID = schoolID;
                objMoni.ClassID = objPOC.ClassID;
                objMoni.PupilID = pupilID;                             
                objMoni.MonitoringType = 2;
                objMoni.CreateDate = DateTime.Now;
                objMoni.HealthPeriodID = healthPeriodID;
                objMoni.EducationLevelID = EducationLevelID;

                objInput = new DentalTestBO();
                objInput.MonitoringBookID = 1;
                objInput.PupilProFileID = pupilID;

                if (frm["txtScraptTeeth_" + pupilID].ToString() == "x" || frm["txtScraptTeeth_" + pupilID].ToString() == "X")
                {
                    objInput.IsScraptTeethCustom = true;
                }
                else
                {
                    objInput.IsScraptTeethCustom = false;
                }

                if (frm["txtTreatment_" + pupilID].ToString() == "x" || frm["txtTreatment_" + pupilID].ToString() == "X")
                {
                    objInput.IsTreatmentCustom = true;
                }
                else
                {
                    objInput.IsTreatmentCustom = false;
                }

                if (!string.IsNullOrWhiteSpace(frm["txtTeethExtracted_" + pupilID]))
                {
                    objInput.NumTeethExtracted = Int32.Parse(frm["txtTeethExtracted_" + pupilID]);
                }

                if (!string.IsNullOrWhiteSpace(frm["txtPreDentalFilling_" + pupilID]))
                {
                    objInput.NumPreventiveDentalFilling = Int32.Parse(frm["txtPreDentalFilling_" + pupilID]);
                }

                if (!string.IsNullOrWhiteSpace(frm["txtDentalFillingHidden_" + pupilID]))
                {
                    objInput.NumDentalFillingCustom = Int32.Parse(frm["txtDentalFillingHidden_" + pupilID]);
                }

                objInput.Other = frm["txtOther_" + pupilID];
                objInput.CreateDate = DateTime.Now;
                objInput.ModifineDate = DateTime.Now;
                objInput.LogID = LogChangeID;
                objInput.IsActive = true;
                objInput.NumDentalFillingOne = !string.IsNullOrEmpty(frm["txtNumDentalFillingOne_" + pupilID]) ? frm["txtNumDentalFillingOne_" + pupilID] : string.Empty;
                objInput.NumDentalFillingTwo = !string.IsNullOrEmpty(frm["txtNumDentalFillingTwo_" + pupilID]) ? frm["txtNumDentalFillingTwo_" + pupilID] : string.Empty;
                objInput.NumDentalFillingThree = !string.IsNullOrEmpty(frm["txtNumDentalFillingThree_" + pupilID]) ? frm["txtNumDentalFillingThree_" + pupilID] : string.Empty;
                objInput.NumDentalFillingFour = !string.IsNullOrEmpty(frm["txtNumDentalFillingFour_" + pupilID]) ? frm["txtNumDentalFillingFour_" + pupilID] : string.Empty;

                if (lstPupilIDExists.Where(x => x == pupilID).Count() == 0)
                    if (objInput.NumDentalFillingCustom.HasValue == null &&
                        string.IsNullOrEmpty(objInput.NumDentalFillingOne) &&
                        string.IsNullOrEmpty(objInput.NumDentalFillingTwo) &&
                        string.IsNullOrEmpty(objInput.NumDentalFillingThree) &&
                        string.IsNullOrEmpty(objInput.NumDentalFillingFour) &&
                        string.IsNullOrEmpty(objInput.Other) &&
                        objInput.NumTeethExtracted == null &&
                        objInput.NumPreventiveDentalFilling == null &&
                        objInput.IsTreatmentCustom == false &&
                        objInput.IsScraptTeethCustom == false)
                        continue;

                lstInput.Add(objInput);
                lstMoni.Add(objMoni);
            }

            dicInsert.Add("EducationLevelID", EducationLevelID);
            dicInsert.Add("lstPupilID", lstPupilID);

            List<EyeTestBO> lstEyeTest = new List<EyeTestBO>();
            List<ENTTestBO> lstEntTest = new List<ENTTestBO>();
            List<PhysicalTestBO> lstPhysicalTest = new List<PhysicalTestBO>();
            List<SpineTestBO> lstSpineTest = new List<SpineTestBO>();
            List<OverallTestBO> lstOverallTest = new List<OverallTestBO>();

            this.InsertOrUpdateValueHealthTest(lstMoni, lstPhysicalTest, lstEyeTest, lstEntTest, lstSpineTest, lstInput, lstOverallTest, dicInsert);
        }
        #endregion

        #region  ValidateOverAllInsert
        public void ValidateOverAllInsert(FormCollection frm, Dictionary<string, object> dicInsert, int LogChangeID)
        {
            int schoolID = Utils.GetInt(dicInsert, "SchoolID");
            int academicYearID = Utils.GetInt(dicInsert, "AcademicYearID");
            int healthPeriodID = Utils.GetInt(dicInsert, "HealthPeriodID");
            int classID = Utils.GetInt(dicInsert, "ClassID");
            int EducationLevelID = ClassProfileBusiness.GetEducationLevelIDByClassID(classID);

            List<PupilOfClassBO> lstPOCDB = PupilOfClassBusiness.GetPupilInClass(classID).Distinct().ToList();
            List<PupilOfClassBO> lstPOC = new List<PupilOfClassBO>();
            lstPOC = lstPOCDB.OrderBy(x => x.PupilID).ToList();

            int PupilIDOne = !string.IsNullOrEmpty(frm["PupilIDOne"]) ? int.Parse(frm["PupilIDOne"]) : 0;
            if (PupilIDOne != 0)
            {
                lstPOC = lstPOC.Where(x => x.PupilID == PupilIDOne).ToList();
            }
            PupilOfClassBO objPOC = null;

            List<MonitoringBookBO> lstMoni = new List<MonitoringBookBO>();
            MonitoringBookBO objMoni = null;
            List<OverallTestBO> lstInput = new List<OverallTestBO>();
            OverallTestBO objInput = null;

            List<MonitoringBook> lstMoniCheckExists = (from mn in MonitoringBookBusiness.All
                                                       join oa in OverallTestBusiness.All on mn.MonitoringBookID equals oa.MonitoringBookID
                                                       where mn.SchoolID == schoolID &&
                                                             mn.AcademicYearID == academicYearID &&
                                                             mn.HealthPeriodID == healthPeriodID &&
                                                             mn.ClassID == classID
                                                       select mn).ToList();
            List<int> lstPupilIDExists = lstMoniCheckExists.Select(x => x.PupilID).Distinct().ToList();

            List<int> lstPupilID = new List<int>();
            int pupilID = 0;
            AcademicYear academicYear = AcademicYearBusiness.Find(academicYearID);
            for (int i = 0; i < lstPOC.Count; i++)
            {
                objPOC = lstPOC[i];
                pupilID = objPOC.PupilID;
                if (!"on".Equals(frm["chk_" + pupilID]) || objPOC.Status != GlobalConstants.PUPIL_STATUS_STUDYING)
                {
                    continue;
                }
                lstPupilID.Add(pupilID);

                objMoni = new MonitoringBookBO();
                objMoni.AcademicYearID = academicYearID;
                objMoni.SchoolID = schoolID;
                objMoni.ClassID = objPOC.ClassID;
                objMoni.PupilID = pupilID;
                objMoni.BirthDate = objPOC.Birthday;
                objMoni.Genre = objPOC.Genre.Value;

                if (!string.IsNullOrEmpty(frm["txtDate_" + pupilID]))
                {
                    if (Utils.IsDateTime(frm["txtDate_" + pupilID]))
                    {
                        DateTime inputDate = Convert.ToDateTime(frm["txtDate_" + pupilID]);
                        double dTotalDays = (DateTime.Now - inputDate).TotalDays;
                        if (dTotalDays >= 0)
                        {
                            if (inputDate.Date >= academicYear.FirstSemesterStartDate.Value.Date
                                && inputDate.Date <= academicYear.SecondSemesterEndDate.Value.Date)
                                objMoni.MonitoringDate = inputDate;
                        }
                    }
                }
                objMoni.UnitName = !string.IsNullOrEmpty(frm["txtUnitName_" + pupilID]) ? frm["txtUnitName_" + pupilID] : string.Empty;
                objMoni.MonitoringType = 2; // theo dõi theo đợt
                objMoni.CreateDate = DateTime.Now;
                objMoni.HealthPeriodID = healthPeriodID;
                objMoni.EducationLevelID = EducationLevelID;

                objInput = new OverallTestBO();
                objInput.MonitoringBookID = 1;
                objInput.PupilProFileID = pupilID;
                objInput.HistoryYourSelf = !string.IsNullOrEmpty(frm["txtHistoryYourSelf_" + pupilID]) ? frm["txtHistoryYourSelf_" + pupilID] : string.Empty;
                if (!string.IsNullOrWhiteSpace(frm["txtOverallInternal_" + pupilID]))
                {
                    objInput.OverallInternal = Int32.Parse(frm["txtOverallInternal_" + pupilID]);
                }
                else
                {
                    objInput.OverallInternal = 0;
                }
                objInput.DescriptionOverallInternal = !string.IsNullOrEmpty(frm["txtDescriptionOverallInternal_" + pupilID]) ? frm["txtDescriptionOverallInternal_" + pupilID] : string.Empty;

                if (!string.IsNullOrWhiteSpace(frm["txtOverallForeign_" + pupilID]))
                {
                    objInput.OverallForeign = Int32.Parse(frm["txtOverallForeign_" + pupilID]);
                }
                else
                {
                    objInput.OverallForeign = 0;
                }
                objInput.DescriptionOverallForeign = !string.IsNullOrEmpty(frm["txtDescriptionOverallForeign_" + pupilID]) ? frm["txtDescriptionOverallForeign_" + pupilID] : string.Empty;

                if (!string.IsNullOrWhiteSpace(frm["txtSkinDiseases_" + pupilID]))
                {
                    objInput.SkinDiseases = Int32.Parse(frm["txtSkinDiseases_" + pupilID]);
                }
                else
                {
                    objInput.SkinDiseases = 0;
                }
                objInput.DescriptionSkinDiseases = !string.IsNullOrEmpty(frm["txtDescriptionSkinDiseases_" + pupilID]) ? frm["txtDescriptionSkinDiseases_" + pupilID] : string.Empty;
                objInput.Other = !string.IsNullOrEmpty(frm["txtOther_" + pupilID]) ? frm["txtOther_" + pupilID] : string.Empty;

                if (frm["txtIsHeartDiseases_" + pupilID].ToString() == "true")
                {
                    objInput.IsHeartDiseases = true;
                }
                else
                {
                    objInput.IsHeartDiseases = false;
                }

                if (frm["txtIsBirthDefect_" + pupilID].ToString() == "true")
                {
                    objInput.IsBirthDefect = true;
                }
                else
                {
                    objInput.IsBirthDefect = false;
                }
                objInput.Doctor = !string.IsNullOrEmpty(frm["txtDoctor_" + pupilID]) ? frm["txtDoctor_" + pupilID] : "";
                objInput.RequireOfDoctor = !string.IsNullOrEmpty(frm["txtRequireOfDoctor_" + pupilID]) ? frm["txtRequireOfDoctor_" + pupilID] : "";

                if (!string.IsNullOrEmpty(frm["txtEvaluationHealth_" + pupilID]))
                {
                    if (frm["txtEvaluationHealth_" + pupilID].ToString() == "a" || frm["txtEvaluationHealth_" + pupilID].ToString() == "A")
                        objInput.EvaluationHealth = 1;
                    else if (frm["txtEvaluationHealth_" + pupilID].ToString() == "b" || frm["txtEvaluationHealth_" + pupilID].ToString() == "B")
                        objInput.EvaluationHealth = 2;
                    else if (frm["txtEvaluationHealth_" + pupilID].ToString() == "c" || frm["txtEvaluationHealth_" + pupilID].ToString() == "C")
                        objInput.EvaluationHealth = 3;
                    else
                        objInput.EvaluationHealth = 0;
                }
                else
                    objInput.EvaluationHealth = 0;

                objInput.IsDredging = false;
                objInput.CreateDate = DateTime.Now;
                objInput.ModifineDate = DateTime.Now;
                objInput.LogID = LogChangeID;
                objInput.IsActive = true;

                if (lstPupilIDExists.Where(x => x == pupilID).Count() == 0)
                {
                    if (string.IsNullOrEmpty(objInput.HistoryYourSelf) &&
                          string.IsNullOrEmpty(objInput.Other) &&
                          string.IsNullOrEmpty(objInput.UnitName) &&
                          string.IsNullOrEmpty(objInput.RequireOfDoctor) &&
                           string.IsNullOrEmpty(objInput.Doctor) &&
                           objInput.OverallInternal == 0 &&
                           objInput.OverallForeign == 0 &&
                           objInput.SkinDiseases == 0 &&
                           objInput.IsHeartDiseases == false &&
                           objInput.IsBirthDefect == false &&
                           objInput.EvaluationHealth == 0 &&
                           !objMoni.MonitoringDate.HasValue)
                        continue;
                }

                lstMoni.Add(objMoni);
                lstInput.Add(objInput);
            }

            dicInsert.Add("EducationLevelID", EducationLevelID);
            dicInsert.Add("lstPupilID", lstPupilID);

            List<EyeTestBO> lstEyeTest = new List<EyeTestBO>();
            List<ENTTestBO> lstEntTest = new List<ENTTestBO>();
            List<PhysicalTestBO> lstPhysicalTest = new List<PhysicalTestBO>();
            List<SpineTestBO> lstSpineTest = new List<SpineTestBO>();
            List<DentalTestBO> lstDentalTest = new List<DentalTestBO>();

            this.InsertOrUpdateValueHealthTest(lstMoni, lstPhysicalTest, lstEyeTest, lstEntTest, lstSpineTest, lstDentalTest, lstInput, dicInsert);
        }
        #endregion

        #region InsertOrUpdateValueHealthTest
        /// <summary>
        /// Thêm mới thông tin sổ theo dõi sức khỏe
        /// <author>Thuyen</author>
        /// <date>05/09/2017</date>
        /// </summary>
        /// <param name="monitoringBook">Đối tượng Insert</param>
        /// <returns></returns>
        public void InsertOrUpdateValueHealthTest(
            List<MonitoringBookBO> lstMonitoringBook,
            List<PhysicalTestBO> lstPhysicalTest,
            List<EyeTestBO> lstEyeTest,
            List<ENTTestBO> lstEntTest,
            List<SpineTestBO> lstSpineTest,
            List<DentalTestBO> lstDentalTest,
            List<OverallTestBO> lstOverallTest,
            IDictionary<string, object> SearchInfo)
        {
            List<int> lstPupilID = Utils.GetIntList(SearchInfo, "lstPupilID");
            int educationId = Utils.GetInt(SearchInfo, "EducationLevelID");
            List<MonitoringBook> lstMonitoringBookDB = MonitoringBookBusiness.Search(SearchInfo)
                .Where(x => lstPupilID.Contains(x.PupilID) && x.EducationLevelID == educationId).ToList();

            List<int> lstMoniteringBookID = lstMonitoringBookDB.Select(p => p.MonitoringBookID).Distinct().ToList();

            //Insert Phycical Test
            #region Insert Physical
            if (lstPhysicalTest.Count > 0)
            {
                #region
                IQueryable<ClassificationCriteria> lstClassificationCriteria = ClassificationCriteriaBusiness.All;
                IQueryable<ClassificationCriteriaDetail> lstClassificationCriteriaDetail = ClassificationCriteriaDetailBusiness.All;


                var lstPhysicalDB = PhysicalTestBusiness.All.Where(x => lstMoniteringBookID.Contains(x.MonitoringBookID)).ToList();
                PhysicalTest objPhysical = null;
                MonitoringBook objMoni = null;
                int nutrition = 0;
                List<ValueNutrition> lstNutri = new List<ValueNutrition>();
                foreach (var item in lstMonitoringBook)
                {
                    var objInput = lstPhysicalTest.Where(x => x.PupilProFileID == item.PupilID).FirstOrDefault();
                    var testMoniBD = lstMonitoringBookDB.Where(x => x.PupilID == item.PupilID).FirstOrDefault();

                    if (testMoniBD != null)
                    {
                        if (testMoniBD.MonitoringDate.HasValue && objInput.Weight.HasValue && objInput.Height.HasValue)
                        {
                            decimal defaultForMin, defaultForMax;
                            decimal minThreshold, maxThreshold;
                            decimal index;
                            int typeConfigID;
                            int month = caculateNumberOfMonthBetweenTowDate(item.BirthDate, testMoniBD.MonitoringDate.Value);
                            if (month <= 60)
                            {
                                typeConfigID = 5; // ID cua tieu chi can nang
                                defaultForMin = 2m;
                                defaultForMax = 9m;
                                index = objInput.Weight.Value;
                            }
                            else
                            {
                                typeConfigID = 7; // ID cua tieu chi BMI
                                defaultForMin = 18m;
                                defaultForMax = 24.9m;
                                index = Math.Round(objInput.Weight.Value / ((objInput.Height.Value * 0.01m) * (objInput.Height.Value * 0.01m)), 2);
                            }
                            int ClassificationCriteriaID = 0;
                            bool Genre = item.Genre == 1 ? true : false;
                            var objClassificationCriteria = lstClassificationCriteria.Where(x => x.TypeConfigID == typeConfigID && x.Genre == Genre).ToList();
                            if (objClassificationCriteria.Count() > 0)
                            {
                                ClassificationCriteriaID = objClassificationCriteria.FirstOrDefault().ClassificationCriteriaID;
                            }
                            else
                                ClassificationCriteriaID = -1;

                            var queryByMonth = lstClassificationCriteriaDetail.Where(x => x.Month == month && x.ClassificationCriteriaID == ClassificationCriteriaID)
                                .Select(x => new { x.IndexValue, x.Month, x.ClassificationCriteria.EffectDate, x.ClassificationLevelHealth.Level }).ToList();

                            var minValue = queryByMonth.Where(p => p.Level == -3).OrderByDescending(o => o.EffectDate).FirstOrDefault();
                            minThreshold = (minValue == null || !minValue.IndexValue.HasValue) ? defaultForMin : minValue.IndexValue.Value;

                            var maxValue = queryByMonth.Where(p => p.Level == 3).OrderByDescending(o => o.EffectDate).FirstOrDefault();
                            maxThreshold = (maxValue == null || !maxValue.IndexValue.HasValue) ? defaultForMax : maxValue.IndexValue.Value;

                            nutrition = index < minThreshold ? SystemParamsInFile.NUTRITION_TYPE_MALNUTRITION : index <= maxThreshold ? SystemParamsInFile.NUTRITION_TYPE_NORMALWEIGHT : SystemParamsInFile.NUTRITION_TYPE_OVERWEIGHT;
                        }
                    }
                    else
                    {// insert
                        objMoni = new MonitoringBook()
                        {
                            AcademicYearID = item.AcademicYearID,
                            SchoolID = item.SchoolID,
                            ClassID = item.ClassID,
                            PupilID = item.PupilID,
                            Symptoms = item.Symptoms,
                            Solution = item.Solution,
                            MonitoringType = item.MonitoringType,
                            HealthPeriodID = item.HealthPeriodID,
                            CreateDate = item.CreateDate,
                            EducationLevelID = item.EducationLevelID,
                        };
                        this.MonitoringBookRepository.Insert(objMoni);
                    }
                    lstNutri.Add(new ValueNutrition { PupilProFileID = item.PupilID, Nutrition = nutrition });
                    nutrition = 0;
                }
                this.MonitoringBookRepository.Save();

                List<MonitoringBook> lstMonitoringBookIDAfterSave = MonitoringBookBusiness.Search(SearchInfo)
                .Where(x => lstPupilID.Contains(x.PupilID) && x.EducationLevelID == educationId).ToList();

                foreach (var item in lstPhysicalTest)
                {
                    int monitoringBookId = lstMonitoringBookIDAfterSave.Where(x => x.PupilID == item.PupilProFileID).FirstOrDefault().MonitoringBookID;
                    var resultPhysicalDB = lstPhysicalDB.Where(x => x.MonitoringBookID == monitoringBookId).FirstOrDefault();
                    int nutri = lstNutri.Where(x => x.PupilProFileID == item.PupilProFileID).FirstOrDefault().Nutrition;

                    if (!item.Height.HasValue && !item.Weight.HasValue && !item.Breast.HasValue
                        && !item.PhysicalClassification.HasValue)
                    {
                        if (resultPhysicalDB != null)
                        {
                            this.PhysicalTestBusiness.Delete(resultPhysicalDB.PhysicalTestID);
                        }
                        else
                            continue;
                    }
                    else
                    {
                        if (resultPhysicalDB != null)
                        {
                            // cập nhật thể lực                              
                            resultPhysicalDB.Height = item.Height;
                            resultPhysicalDB.Weight = item.Weight;
                            resultPhysicalDB.Breast = item.Breast;
                            resultPhysicalDB.PhysicalClassification = item.PhysicalClassification;
                            resultPhysicalDB.ModifiedDate = item.ModifineDate;
                            resultPhysicalDB.Nutrition = nutri;
                            resultPhysicalDB.LogID = item.LogID;
                            this.PhysicalTestBusiness.Update(resultPhysicalDB);
                        }
                        else
                        {
                            objPhysical = new PhysicalTest()
                            {
                                MonitoringBookID = monitoringBookId,
                                Date = item.Date.Value,
                                Height = item.Height,
                                Weight = item.Weight,
                                Breast = item.Breast,
                                PhysicalClassification = item.PhysicalClassification,
                                CreatedDate = item.CreateDate,
                                IsActive = item.IsActive,
                                ModifiedDate = item.ModifineDate,
                                Nutrition = nutri,
                                LogID = item.LogID
                            };
                            this.PhysicalTestBusiness.Insert(objPhysical);
                        }
                    }
                }
                this.PhysicalTestBusiness.Save();
                #endregion

            }
            #endregion

            #region Insert Spin
            else if (lstSpineTest.Count > 0)
            {
                var lstSpinDB = SpineTestBusiness.All.Where(x => lstMoniteringBookID.Contains(x.MonitoringBookID)).ToList();
                SpineTest objSpin = null;
                MonitoringBook objMoni = null;
                foreach (var item in lstMonitoringBook)
                {
                    var testMoniBD = lstMonitoringBookDB.Where(x => x.PupilID == item.PupilID).FirstOrDefault();
                    var objInput = lstSpineTest.Where(x => x.PupilProFileID == item.PupilID).FirstOrDefault();
                    if (testMoniBD != null)
                    {
                        var testPhyDB = lstSpinDB.Where(x => x.MonitoringBookID == testMoniBD.MonitoringBookID).FirstOrDefault();
                        if (testPhyDB != null)
                        {
                            // cập nhật thể lực     
                            if (string.IsNullOrEmpty(objInput.ExaminationStraight)
                                && string.IsNullOrEmpty(objInput.ExaminationItalic)
                                && string.IsNullOrEmpty(objInput.Other)
                                && objInput.IsDeformity == false)
                            {
                                this.SpineTestBusiness.Delete(testPhyDB.SpineTestID);
                            }
                            else
                            {
                                testPhyDB.IsDeformityOfTheSpine = objInput.IsDeformity;
                                testPhyDB.ExaminationStraight = objInput.ExaminationStraight;
                                testPhyDB.ExaminationItalic = objInput.ExaminationItalic;
                                testPhyDB.Other = objInput.Other;
                                testPhyDB.ModifiedDate = objInput.ModifineDate;
                                this.SpineTestBusiness.Update(testPhyDB);
                            }
                        }
                        else
                        { // insert thể lực
                            objSpin = new SpineTest()
                            {
                                MonitoringBookID = testMoniBD.MonitoringBookID,
                                IsDeformityOfTheSpine = objInput.IsDeformity,
                                ExaminationStraight = objInput.ExaminationStraight,
                                ExaminationItalic = objInput.ExaminationItalic,
                                Other = objInput.Other,
                                CreatedDate = objInput.CreateDate,
                                IsActive = objInput.IsActive,
                                ModifiedDate = objInput.ModifineDate,
                                LogID = objInput.LogID
                            };
                            this.SpineTestBusiness.Insert(objSpin);
                        }
                    }
                    else // insert
                    {
                        int moniteringBookID = MonitoringBookBusiness.GetNextSeq<int>();
                        objMoni = new MonitoringBook()
                        {
                            AcademicYearID = item.AcademicYearID,
                            MonitoringBookID = moniteringBookID,
                            SchoolID = item.SchoolID,
                            ClassID = item.ClassID,
                            PupilID = item.PupilID,
                            MonitoringDate = item.MonitoringDate,
                            Symptoms = item.Symptoms,
                            Solution = item.Solution,
                            UnitName = item.UnitName,
                            MonitoringType = item.MonitoringType,
                            HealthPeriodID = item.HealthPeriodID,
                            CreateDate = item.CreateDate,
                            EducationLevelID = item.EducationLevelID,
                        };
                        this.MonitoringBookRepository.Insert(objMoni);

                        objSpin = new SpineTest()
                        {
                            MonitoringBookID = objMoni.MonitoringBookID,
                            IsDeformityOfTheSpine = objInput.IsDeformity,
                            ExaminationStraight = objInput.ExaminationStraight,
                            ExaminationItalic = objInput.ExaminationItalic,
                            Other = objInput.Other,
                            CreatedDate = objInput.CreateDate,
                            IsActive = objInput.IsActive,
                            ModifiedDate = objInput.ModifineDate,
                            LogID = objInput.LogID
                        };
                        this.SpineTestBusiness.Insert(objSpin);
                    }
                }

                MonitoringBookBusiness.Save();
                SpineTestBusiness.Save();
            }
            #endregion

            #region Insert Dental
            else if (lstDentalTest.Count > 0)
            {
                var lstDentalDB = DentalTestBusiness.All.Where(x => lstMoniteringBookID.Contains(x.MonitoringBookID)).ToList();
                DentalTest objDental = null;
                MonitoringBook objMoni = null;
                foreach (var item in lstMonitoringBook)
                {
                    var testMoniBD = lstMonitoringBookDB.Where(x => x.PupilID == item.PupilID).FirstOrDefault();
                    var objInput = lstDentalTest.Where(x => x.PupilProFileID == item.PupilID).FirstOrDefault();
                    if (testMoniBD != null)
                    {
                        var testPhyDB = lstDentalDB.Where(x => x.MonitoringBookID == testMoniBD.MonitoringBookID).FirstOrDefault();
                        if (testPhyDB != null)
                        {

                            if ((objInput.NumDentalFillingCustom == 0 || objInput.NumDentalFillingCustom == null) &&
                                string.IsNullOrEmpty(objInput.NumDentalFillingOne) &&
                                string.IsNullOrEmpty(objInput.NumDentalFillingTwo) &&
                                string.IsNullOrEmpty(objInput.NumDentalFillingThree)  &&
                                string.IsNullOrEmpty(objInput.NumDentalFillingFour)&&
                                string.IsNullOrEmpty(objInput.Other) &&
                                objInput.NumTeethExtracted == null &&
                                objInput.NumPreventiveDentalFilling == null &&
                                objInput.IsTreatmentCustom == false &&
                                objInput.IsScraptTeethCustom == false)
                            {
                                this.DentalTestBusiness.Delete(testPhyDB.DentalTestID);
                            }
                            else
                            {
                                // cập nhật thể lực                              
                                testPhyDB.IsScraptTeeth = objInput.IsScraptTeethCustom;
                                testPhyDB.IsTreatment = objInput.IsTreatmentCustom;
                                testPhyDB.NumTeethExtracted = objInput.NumTeethExtracted;
                                testPhyDB.NumPreventiveDentalFilling = objInput.NumPreventiveDentalFilling;
                                testPhyDB.NumDentalFilling = objInput.NumDentalFillingCustom;
                                testPhyDB.Other = objInput.Other;
                                testPhyDB.ModifiedDate = objInput.ModifineDate;
                                testPhyDB.NumDentalFillingOne = objInput.NumDentalFillingOne;
                                testPhyDB.NumDentalFillingTwo = objInput.NumDentalFillingTwo;
                                testPhyDB.NumDentalFillingThree = objInput.NumDentalFillingThree;
                                testPhyDB.NumDentalFillingFour = objInput.NumDentalFillingFour;
                                this.DentalTestBusiness.Update(testPhyDB);
                            }
                        }
                        else
                        { // insert thể lực
                            objDental = new DentalTest()
                            {
                                MonitoringBookID = testMoniBD.MonitoringBookID,
                                IsScraptTeeth = objInput.IsScraptTeethCustom,
                                IsTreatment = objInput.IsTreatmentCustom,
                                NumTeethExtracted = objInput.NumTeethExtracted,
                                NumPreventiveDentalFilling = objInput.NumPreventiveDentalFilling,
                                NumDentalFilling = objInput.NumDentalFillingCustom,
                                NumDentalFillingOne = objInput.NumDentalFillingOne,
                                NumDentalFillingTwo = objInput.NumDentalFillingTwo,
                                NumDentalFillingThree = objInput.NumDentalFillingThree,
                                NumDentalFillingFour = objInput.NumDentalFillingFour,
                                Other = objInput.Other,
                                CreatedDate = objInput.CreateDate,
                                IsActive = true,
                                ModifiedDate = objInput.ModifineDate,
                                LogID = objInput.LogID
                            };
                            this.DentalTestBusiness.Insert(objDental);
                        }
                    }
                    else // insert
                    {
                        int moniteringBookID = MonitoringBookBusiness.GetNextSeq<int>();
                        objMoni = new MonitoringBook()
                        {
                            AcademicYearID = item.AcademicYearID,
                            MonitoringBookID = moniteringBookID,
                            SchoolID = item.SchoolID,
                            ClassID = item.ClassID,
                            PupilID = item.PupilID,
                            MonitoringDate = item.MonitoringDate,
                            Symptoms = item.Symptoms,
                            Solution = item.Solution,
                            UnitName = item.UnitName,
                            MonitoringType = item.MonitoringType,
                            HealthPeriodID = item.HealthPeriodID,
                            CreateDate = item.CreateDate,
                            EducationLevelID = item.EducationLevelID,
                        };
                        this.MonitoringBookRepository.Insert(objMoni);

                        objDental = new DentalTest()
                        {
                            MonitoringBookID = objMoni.MonitoringBookID,
                            IsScraptTeeth = objInput.IsScraptTeethCustom,
                            IsTreatment = objInput.IsTreatmentCustom,
                            NumTeethExtracted = objInput.NumTeethExtracted,
                            NumPreventiveDentalFilling = objInput.NumPreventiveDentalFilling,
                            NumDentalFilling = objInput.NumDentalFillingCustom,
                            NumDentalFillingOne = objInput.NumDentalFillingOne,
                            NumDentalFillingTwo = objInput.NumDentalFillingTwo,
                            NumDentalFillingThree = objInput.NumDentalFillingThree,
                            NumDentalFillingFour = objInput.NumDentalFillingFour,
                            Other = objInput.Other,
                            CreatedDate = objInput.CreateDate,
                            IsActive = true,
                            ModifiedDate = objInput.ModifineDate,
                            LogID = objInput.LogID
                        };
                        this.DentalTestBusiness.Insert(objDental);
                    }
                }

                MonitoringBookBusiness.Save();
                DentalTestBusiness.Save();
            }
            #endregion

            #region Insert OverAll
            else if (lstOverallTest.Count > 0)
            {
                IQueryable<ClassificationCriteria> lstClassificationCriteria = ClassificationCriteriaBusiness.All;
                IQueryable<ClassificationCriteriaDetail> lstClassificationCriteriaDetail = ClassificationCriteriaDetailBusiness.All;

                var lstPhysicalDB = PhysicalTestBusiness.All.Where(x => lstMoniteringBookID.Contains(x.MonitoringBookID)).ToList();
                var lstOverAllDB = OverallTestBusiness.All.Where(x => lstMoniteringBookID.Contains(x.MonitoringBookID)).ToList();
                OverallTest objOverAll = null;
                MonitoringBook objMoni = null;
                int nutrition = 0;
                foreach (var item in lstMonitoringBook)
                {
                    var testMoniBD = lstMonitoringBookDB.Where(x => x.PupilID == item.PupilID).FirstOrDefault();
                    var objInput = lstOverallTest.Where(x => x.PupilProFileID == item.PupilID).FirstOrDefault();
                    if (testMoniBD != null)
                    {
                        var testPhyDB = lstPhysicalDB.Where(x => x.MonitoringBookID == testMoniBD.MonitoringBookID).FirstOrDefault();
                        var testOverAllDB = lstOverAllDB.Where(x => x.MonitoringBookID == testMoniBD.MonitoringBookID).FirstOrDefault();
                        // cập nhật monitoring
                        testMoniBD.UnitName = item.UnitName;
                        testMoniBD.MonitoringDate = item.MonitoringDate;

                        if (testPhyDB != null)
                        {
                            if (testPhyDB.Weight.HasValue && testPhyDB.Height.HasValue && item.MonitoringDate.HasValue)
                            {
                                decimal defaultForMin, defaultForMax;
                                decimal minThreshold, maxThreshold;
                                decimal index;
                                int typeConfigID;
                                int month = caculateNumberOfMonthBetweenTowDate(item.BirthDate, item.MonitoringDate.Value);
                                if (month <= 60)
                                {
                                    typeConfigID = 5; // ID cua tieu chi can nang
                                    defaultForMin = 2m;
                                    defaultForMax = 9m;
                                    index = testPhyDB.Weight.Value;
                                }
                                else
                                {
                                    typeConfigID = 7; // ID cua tieu chi BMI
                                    defaultForMin = 18m;
                                    defaultForMax = 24.9m;
                                    index = Math.Round(testPhyDB.Weight.Value / ((testPhyDB.Height.Value * 0.01m) * (testPhyDB.Height.Value * 0.01m)), 2);
                                }
                                int ClassificationCriteriaID = 0;
                                bool Genre = item.Genre == 1 ? true : false;
                                var objClassificationCriteria = lstClassificationCriteria.Where(x => x.TypeConfigID == typeConfigID && x.Genre == Genre).ToList();
                                if (objClassificationCriteria.Count() > 0)
                                {
                                    ClassificationCriteriaID = objClassificationCriteria.FirstOrDefault().ClassificationCriteriaID;
                                }
                                else
                                    ClassificationCriteriaID = -1;

                                var queryByMonth = lstClassificationCriteriaDetail.Where(x => x.Month == month && x.ClassificationCriteriaID == ClassificationCriteriaID)
                                    .Select(x => new { x.IndexValue, x.Month, x.ClassificationCriteria.EffectDate, x.ClassificationLevelHealth.Level }).ToList();

                                var minValue = queryByMonth.Where(p => p.Level == -3).OrderByDescending(o => o.EffectDate).FirstOrDefault();
                                minThreshold = (minValue == null || !minValue.IndexValue.HasValue) ? defaultForMin : minValue.IndexValue.Value;

                                var maxValue = queryByMonth.Where(p => p.Level == 3).OrderByDescending(o => o.EffectDate).FirstOrDefault();
                                maxThreshold = (maxValue == null || !maxValue.IndexValue.HasValue) ? defaultForMax : maxValue.IndexValue.Value;

                                nutrition = index < minThreshold ? SystemParamsInFile.NUTRITION_TYPE_MALNUTRITION : index <= maxThreshold ? SystemParamsInFile.NUTRITION_TYPE_NORMALWEIGHT : SystemParamsInFile.NUTRITION_TYPE_OVERWEIGHT;
                            }
                            testPhyDB.Nutrition = nutrition;
                            this.PhysicalTestBusiness.Update(testPhyDB);
                            nutrition = 0;
                        }
                        this.MonitoringBookRepository.Update(testMoniBD);

                        if (testOverAllDB != null)
                        {
                            if (string.IsNullOrEmpty(objInput.HistoryYourSelf) &&
                                objInput.OverallInternal == 0 &&
                                objInput.OverallForeign == 0 &&
                                objInput.SkinDiseases == 0 &&
                                objInput.IsHeartDiseases == false &&
                               string.IsNullOrEmpty(objInput.Other) &&
                                objInput.IsBirthDefect == false &&
                                objInput.EvaluationHealth == 0 &&
                                string.IsNullOrEmpty(objInput.RequireOfDoctor) &&
                                string.IsNullOrEmpty(objInput.Doctor))
                            {
                                this.OverallTestBusiness.Delete(testOverAllDB.OverallTestID);
                            }
                            else
                            {
                                // cập nhật                            
                                testOverAllDB.HistoryYourSelf = objInput.HistoryYourSelf;
                                testOverAllDB.OverallInternal = objInput.OverallInternal.HasValue ? objInput.OverallInternal.Value : 0;
                                testOverAllDB.DescriptionOverallInternall = objInput.DescriptionOverallInternal;
                                testOverAllDB.OverallForeign = objInput.OverallForeign.HasValue ? objInput.OverallForeign.Value : 0;
                                testOverAllDB.DescriptionForeign = objInput.DescriptionOverallForeign;
                                testOverAllDB.Other = objInput.Other;
                                testOverAllDB.ModifiedDate = objInput.ModifineDate;
                                testOverAllDB.SkinDiseases = objInput.SkinDiseases.HasValue ? objInput.SkinDiseases.Value : 0;
                                testOverAllDB.DescriptionSkinDiseases = objInput.DescriptionSkinDiseases;
                                testOverAllDB.Doctor = objInput.Doctor;
                                testOverAllDB.EvaluationHealth = objInput.EvaluationHealth.HasValue ? objInput.EvaluationHealth.Value : 0;
                                testOverAllDB.LogID = objInput.LogID;
                                testOverAllDB.IsBirthDefect = objInput.IsBirthDefect.HasValue ? objInput.IsBirthDefect.Value : false;
                                testOverAllDB.IsHeartDiseases = objInput.IsHeartDiseases.HasValue ? objInput.IsHeartDiseases.Value : false;
                                testOverAllDB.IsDredging = objInput.IsDredging.HasValue ? objInput.IsDredging.Value : false;
                                testOverAllDB.RequireOfDoctor = objInput.RequireOfDoctor;
                                this.OverallTestBusiness.Update(testOverAllDB);
                            }
                        }
                        else
                        { // insert
                            objOverAll = new OverallTest()
                            {
                                MonitoringBookID = testMoniBD.MonitoringBookID,
                                HistoryYourSelf = objInput.HistoryYourSelf,
                                OverallInternal = objInput.OverallInternal.HasValue ? objInput.OverallInternal.Value : 0,
                                DescriptionOverallInternall = objInput.DescriptionOverallInternal,
                                OverallForeign = objInput.OverallForeign.HasValue ? objInput.OverallForeign.Value : 0,
                                DescriptionForeign = objInput.DescriptionOverallForeign,
                                Other = objInput.Other,
                                ModifiedDate = objInput.ModifineDate,
                                CreatedDate = objInput.CreateDate,
                                SkinDiseases = objInput.SkinDiseases.HasValue ? objInput.SkinDiseases.Value : 0,
                                DescriptionSkinDiseases = objInput.DescriptionSkinDiseases,
                                Doctor = objInput.Doctor,
                                EvaluationHealth = objInput.EvaluationHealth.HasValue ? objInput.EvaluationHealth.Value : 0,
                                LogID = objInput.LogID,
                                IsBirthDefect = objInput.IsBirthDefect.HasValue ? objInput.IsBirthDefect.Value : false,
                                IsHeartDiseases = objInput.IsHeartDiseases.HasValue ? objInput.IsHeartDiseases.Value : false,
                                IsDredging = objInput.IsDredging.HasValue ? objInput.IsDredging.Value : false,
                                IsActive = true,
                                RequireOfDoctor = objInput.RequireOfDoctor,
                            };
                            this.OverallTestBusiness.Insert(objOverAll);
                        }
                    }
                    else // insert
                    {
                        int moniteringBookID = MonitoringBookBusiness.GetNextSeq<int>();
                        objMoni = new MonitoringBook()
                        {
                            AcademicYearID = item.AcademicYearID,
                            MonitoringBookID = moniteringBookID,
                            SchoolID = item.SchoolID,
                            ClassID = item.ClassID,
                            PupilID = item.PupilID,
                            MonitoringDate = item.MonitoringDate,
                            Symptoms = item.Symptoms,
                            Solution = item.Solution,
                            UnitName = item.UnitName,
                            MonitoringType = item.MonitoringType,
                            HealthPeriodID = item.HealthPeriodID,
                            CreateDate = item.CreateDate,
                            EducationLevelID = item.EducationLevelID,
                        };
                        this.MonitoringBookRepository.Insert(objMoni);

                        objOverAll = new OverallTest()
                        {
                            MonitoringBookID = objMoni.MonitoringBookID,
                            HistoryYourSelf = objInput.HistoryYourSelf,
                            OverallInternal = objInput.OverallInternal.HasValue ? objInput.OverallInternal.Value : 0,
                            DescriptionOverallInternall = objInput.DescriptionOverallInternal,
                            OverallForeign = objInput.OverallForeign.HasValue ? objInput.OverallForeign.Value : 0,
                            DescriptionForeign = objInput.DescriptionOverallForeign,
                            Other = objInput.Other,
                            ModifiedDate = objInput.ModifineDate,
                            CreatedDate = objInput.CreateDate,
                            SkinDiseases = objInput.SkinDiseases.HasValue ? objInput.SkinDiseases.Value : 0,
                            DescriptionSkinDiseases = objInput.DescriptionSkinDiseases,
                            Doctor = objInput.Doctor,
                            EvaluationHealth = objInput.EvaluationHealth.HasValue ? objInput.EvaluationHealth.Value : 0,
                            LogID = objInput.LogID,
                            IsBirthDefect = objInput.IsBirthDefect.HasValue ? objInput.IsBirthDefect.Value : false,
                            IsHeartDiseases = objInput.IsHeartDiseases.HasValue ? objInput.IsHeartDiseases.Value : false,
                            IsDredging = objInput.IsDredging.HasValue ? objInput.IsDredging.Value : false,
                            IsActive = true,
                            RequireOfDoctor = objInput.RequireOfDoctor,
                        };
                        this.OverallTestBusiness.Insert(objOverAll);
                    }
                }

                MonitoringBookBusiness.Save();
                PhysicalTestBusiness.Save();
                OverallTestBusiness.Save();
            }
            #endregion

            //Insert EyeTest     
            else if (lstEyeTest.Count > 0)
            {
                #region
                var lstEyeTestDB = EyeTestBusiness.All.Where(x => lstMoniteringBookID.Contains(x.MonitoringBookID)).ToList();
                EyeTest objEye = null;
                MonitoringBook objMoni = null;

                foreach (var item in lstMonitoringBook)
                {
                    var testMoniBD = lstMonitoringBookDB.Where(x => x.PupilID == item.PupilID).FirstOrDefault();
                    if (testMoniBD == null)
                    {
                        objMoni = new MonitoringBook()
                        {
                            AcademicYearID = item.AcademicYearID,
                            SchoolID = item.SchoolID,
                            ClassID = item.ClassID,
                            PupilID = item.PupilID,
                            MonitoringDate = item.MonitoringDate,
                            Symptoms = item.Symptoms,
                            Solution = item.Solution,
                            UnitName = item.UnitName,
                            MonitoringType = item.MonitoringType,
                            HealthPeriodID = item.HealthPeriodID,
                            CreateDate = item.CreateDate,
                            EducationLevelID = item.EducationLevelID,
                        };
                        this.MonitoringBookRepository.Insert(objMoni);
                    }
                }
                this.MonitoringBookRepository.Save();

                List<MonitoringBook> lstMonitoringBookIDAfterSave = MonitoringBookBusiness.Search(SearchInfo)
              .Where(x => lstPupilID.Contains(x.PupilID) && x.EducationLevelID == educationId).ToList();

                foreach (var item in lstEyeTest)
                {
                    int monitoringBookId = lstMonitoringBookIDAfterSave.Where(x => x.PupilID == item.PupilProFileID).FirstOrDefault().MonitoringBookID;
                    var resultEyeDB = lstEyeTestDB.Where(x => x.MonitoringBookID == monitoringBookId).FirstOrDefault();

                    if (!item.LEye.HasValue && !item.REye.HasValue
                        && !item.LIsEye.HasValue && !item.RIsEye.HasValue
                        && !item.RMyopic.HasValue && !item.LMyopic.HasValue
                        && !item.LAstigmatism.HasValue && !item.RAstigmatism.HasValue
                        && !item.LFarsightedness.HasValue && !item.RFarsightedness.HasValue && string.IsNullOrEmpty(item.Other))
                    {
                        if (resultEyeDB != null)
                        {
                            this.EyeTestBusiness.Delete(resultEyeDB.EyeTestID);
                        }
                        else
                            continue;
                    }
                    else
                    {
                        if (resultEyeDB != null)
                        {
                            resultEyeDB.REye = item.REye;
                            resultEyeDB.LEye = item.LEye;
                            resultEyeDB.RIsEye = item.RIsEye;
                            resultEyeDB.LIsEye = item.LIsEye;
                            resultEyeDB.RMyopic = item.RMyopic;
                            resultEyeDB.LMyopic = item.LMyopic;
                            resultEyeDB.RFarsightedness = item.RFarsightedness;
                            resultEyeDB.LFarsightedness = item.LFarsightedness;
                            resultEyeDB.RAstigmatism = item.RAstigmatism;
                            resultEyeDB.LAstigmatism = item.LAstigmatism;
                            resultEyeDB.Other = item.Other;
                            resultEyeDB.ModifiedDate = item.ModifiedDate;
                            resultEyeDB.LogID = item.LogID;
                            resultEyeDB.IsAstigmatism = item.IsAstigmatism;
                            resultEyeDB.IsFarsightedness = item.IsFarsightedness;
                            resultEyeDB.IsMyopic = item.IsMyopic;
                            this.EyeTestBusiness.Update(resultEyeDB);
                        }
                        else
                        {
                            objEye = new EyeTest()
                            {
                                MonitoringBookID = monitoringBookId,
                                REye = item.REye,
                                LEye = item.LEye,
                                RIsEye = item.RIsEye,
                                LIsEye = item.LIsEye,
                                RMyopic = item.RMyopic,
                                LMyopic = item.LMyopic,
                                RFarsightedness = item.RFarsightedness,
                                LFarsightedness = item.LFarsightedness,
                                RAstigmatism = item.RAstigmatism,
                                LAstigmatism = item.LAstigmatism,
                                Other = item.Other,
                                IsTreatment = item.IsTreatment.Value,
                                CreatedDate = item.CreateDate,
                                IsActive = item.IsActive,
                                ModifiedDate = item.ModifiedDate,
                                LogID = item.LogID,
                                IsAstigmatism = item.IsAstigmatism,
                                IsFarsightedness = item.IsFarsightedness,
                                IsMyopic = item.IsMyopic,
                            };
                            this.EyeTestBusiness.Insert(objEye);
                        }
                    }
                }
                this.EyeTestBusiness.Save();

                #endregion
            }//Insert EntTest
            else if (lstEntTest.Count > 0)
            {
                #region
                var lstEntTestDB = ENTTestBusiness.All.Where(x => lstMoniteringBookID.Contains(x.MonitoringBookID)).ToList();
                ENTTest objEnt = null;
                MonitoringBook objMoni = null;

                foreach (var item in lstMonitoringBook)
                {
                    var testMoniBD = lstMonitoringBookDB.Where(x => x.PupilID == item.PupilID).FirstOrDefault();
                    if (testMoniBD == null)
                    {
                        objMoni = new MonitoringBook()
                        {
                            AcademicYearID = item.AcademicYearID,
                            SchoolID = item.SchoolID,
                            ClassID = item.ClassID,
                            PupilID = item.PupilID,
                            MonitoringDate = item.MonitoringDate,
                            Symptoms = item.Symptoms,
                            Solution = item.Solution,
                            UnitName = item.UnitName,
                            MonitoringType = item.MonitoringType,
                            HealthPeriodID = item.HealthPeriodID,
                            CreateDate = item.CreateDate,
                            EducationLevelID = item.EducationLevelID,
                        };
                        this.MonitoringBookRepository.Insert(objMoni);
                    }
                }
                this.MonitoringBookRepository.Save();
                List<MonitoringBook> lstMonitoringBookIDAfterSave = MonitoringBookBusiness.Search(SearchInfo)
              .Where(x => lstPupilID.Contains(x.PupilID) && x.EducationLevelID == educationId).ToList();

                foreach (var item in lstEntTest)
                {
                    int monitoringBookId = lstMonitoringBookIDAfterSave.Where(x => x.PupilID == item.PupilProFileID).FirstOrDefault().MonitoringBookID;
                    var resultEntDB = lstEntTestDB.Where(x => x.MonitoringBookID == monitoringBookId).FirstOrDefault();

                    if (!item.RCapacityHearing.HasValue && !item.LCapacityHearing.HasValue
                        && item.IsDeaf.Value == false && item.IsSick.Value == false && string.IsNullOrEmpty(item.Description))
                    {
                        if (resultEntDB != null)
                        {
                            this.ENTTestBusiness.Delete(resultEntDB.ENTTestID);
                        }
                        else
                            continue;
                    }
                    else
                    {
                        if (resultEntDB != null)
                        {
                            resultEntDB.LCapacityHearing = item.LCapacityHearing;
                            resultEntDB.RCapacityHearing = item.RCapacityHearing;
                            resultEntDB.IsSick = item.IsSick.Value;
                            resultEntDB.IsDeaf = item.IsDeaf.Value;
                            resultEntDB.Description = item.Description;
                            resultEntDB.ModifiedDate = item.ModifiedDate;
                            resultEntDB.LogID = item.LogID;
                            this.ENTTestBusiness.Update(resultEntDB);
                        }
                        else
                        {
                            objEnt = new ENTTest()
                            {
                                MonitoringBookID = monitoringBookId,
                                LCapacityHearing = item.LCapacityHearing,
                                RCapacityHearing = item.RCapacityHearing,
                                IsSick = item.IsSick,
                                IsDeaf = item.IsDeaf,
                                Description = item.Description,
                                ModifiedDate = item.ModifiedDate,
                                CreatedDate = item.CreatedDate,
                                IsActive = item.IsActive,
                                LogID = item.LogID,
                            };
                            this.ENTTestBusiness.Insert(objEnt);
                        }
                    }
                }
                this.ENTTestBusiness.Save();
                #endregion
            }
        }
        #endregion

        private int caculateNumberOfMonthBetweenTowDate(DateTime fromDate, DateTime? toDate)
        {
            int result = (toDate.Value.Year - fromDate.Year) * 12 + toDate.Value.Month - fromDate.Month + (toDate.Value.Day >= fromDate.Day ? 0 : -1);
            return result;
        }

        #region InsertOrUpdateValuePhysical
        /// <summary>
        /// Thêm mới thông tin sổ theo dõi sức khỏe
        /// <author>Thuyen</author>
        /// <date>05/09/2017</date>
        /// </summary>
        /// <param name="monitoringBook">Đối tượng Insert</param>
        /// <returns></returns>
        public void InsertOrUpdateValuePhysical(
            List<MonitoringBookBO> lstMonitoringBook,
            List<PhysicalTestBO> lstPhysicalTest,
            IDictionary<string, object> SearchInfo)
        {
            List<int> lstPupilID = Utils.GetIntList(SearchInfo, "lstPupilIDOfPhysical");
            int educationId = Utils.GetInt(SearchInfo, "EducationLevelID");
            List<MonitoringBook> lstMonitoringBookDB = MonitoringBookBusiness.Search(SearchInfo)
                .Where(x => x.EducationLevelID == educationId).ToList();
            List<int> lstMoniteringBookID = lstMonitoringBookDB.Select(p => p.MonitoringBookID).Distinct().ToList();

            List<MonitoringBook> lstMonitoringBookFlowPupilID = new List<MonitoringBook>();
            foreach (var item in lstPupilID)
            {
                var objMoniBook = lstMonitoringBookDB.Where(x => x.PupilID == item).ToList();
                if (objMoniBook.Count > 0)
                {
                    foreach (var itemObj in objMoniBook)
                    {
                        lstMonitoringBookFlowPupilID.Add(itemObj);
                    }
                    
                }
            }

            //Insert Phycical Test
            #region Insert Physical
            IQueryable<ClassificationCriteria> lstClassificationCriteria = ClassificationCriteriaBusiness.All;
            IQueryable<ClassificationCriteriaDetail> lstClassificationCriteriaDetail = ClassificationCriteriaDetailBusiness.All;

            var lstPhysicalDB = PhysicalTestBusiness.All.Where(x => lstMoniteringBookID.Contains(x.MonitoringBookID)).ToList();
            PhysicalTest objPhysical = null;
            MonitoringBook objMoni = null;
            int nutrition = 0;
            List<ValueNutrition> lstNutri = new List<ValueNutrition>();
            foreach (var item in lstMonitoringBook)
            {
                var objInput = lstPhysicalTest.Where(x => x.PupilProFileID == item.PupilID).FirstOrDefault();
                var testMoniBD = lstMonitoringBookFlowPupilID.Where(x => x.PupilID == item.PupilID 
                                                            && x.ClassID == item.ClassID).FirstOrDefault();
                if (testMoniBD != null)
                {
                    if (testMoniBD.MonitoringDate.HasValue && objInput.Weight.HasValue && objInput.Height.HasValue)
                    {
                        decimal defaultForMin, defaultForMax;
                        decimal minThreshold, maxThreshold;
                        decimal index;
                        int typeConfigID;
                        int month = caculateNumberOfMonthBetweenTowDate(item.BirthDate, testMoniBD.MonitoringDate.Value);
                        if (month <= 60)
                        {
                            typeConfigID = 5; // ID cua tieu chi can nang
                            defaultForMin = 2m;
                            defaultForMax = 9m;
                            index = objInput.Weight.Value;
                        }
                        else
                        {
                            typeConfigID = 7; // ID cua tieu chi BMI
                            defaultForMin = 18m;
                            defaultForMax = 24.9m;
                            index = Math.Round(objInput.Weight.Value / ((objInput.Height.Value * 0.01m) * (objInput.Height.Value * 0.01m)), 2);
                        }
                        int ClassificationCriteriaID = 0;
                        bool Genre = item.Genre == 1 ? true : false;
                        var objClassificationCriteria = lstClassificationCriteria.Where(x => x.TypeConfigID == typeConfigID && x.Genre == Genre).ToList();
                        if (objClassificationCriteria.Count() > 0)
                        {
                            ClassificationCriteriaID = objClassificationCriteria.FirstOrDefault().ClassificationCriteriaID;
                        }
                        else
                            ClassificationCriteriaID = -1;

                        var queryByMonth = lstClassificationCriteriaDetail.Where(x => x.Month == month && x.ClassificationCriteriaID == ClassificationCriteriaID)
                            .Select(x => new { x.IndexValue, x.Month, x.ClassificationCriteria.EffectDate, x.ClassificationLevelHealth.Level }).ToList();

                        var minValue = queryByMonth.Where(p => p.Level == -3).OrderByDescending(o => o.EffectDate).FirstOrDefault();
                        minThreshold = (minValue == null || !minValue.IndexValue.HasValue) ? defaultForMin : minValue.IndexValue.Value;

                        var maxValue = queryByMonth.Where(p => p.Level == 3).OrderByDescending(o => o.EffectDate).FirstOrDefault();
                        maxThreshold = (maxValue == null || !maxValue.IndexValue.HasValue) ? defaultForMax : maxValue.IndexValue.Value;

                        nutrition = index < minThreshold ? SystemParamsInFile.NUTRITION_TYPE_MALNUTRITION : index <= maxThreshold ? SystemParamsInFile.NUTRITION_TYPE_NORMALWEIGHT : SystemParamsInFile.NUTRITION_TYPE_OVERWEIGHT;
                    }
                }
                else
                {// insert
                    objMoni = new MonitoringBook()
                    {
                        AcademicYearID = item.AcademicYearID,
                        SchoolID = item.SchoolID,
                        ClassID = item.ClassID,
                        PupilID = item.PupilID,
                        MonitoringDate = item.MonitoringDate,
                        Symptoms = item.Symptoms,
                        Solution = item.Solution,
                        UnitName = item.UnitName,
                        MonitoringType = item.MonitoringType,
                        HealthPeriodID = item.HealthPeriodID,
                        CreateDate = item.CreateDate,
                        EducationLevelID = item.EducationLevelID,
                    };
                    this.MonitoringBookRepository.Insert(objMoni);
                }
                lstNutri.Add(new ValueNutrition { PupilProFileID = item.PupilID, Nutrition = nutrition });
                nutrition = 0;
            }
            this.MonitoringBookRepository.Save();

            List<MonitoringBook> lstMonitoringBookIDAfterSave = MonitoringBookBusiness.Search(SearchInfo)
            .Where(x => lstPupilID.Contains(x.PupilID) && x.EducationLevelID == educationId).ToList();

            foreach (var item in lstPhysicalTest)
            {
                int monitoringBookId = lstMonitoringBookIDAfterSave.Where(x => x.PupilID == item.PupilProFileID
                                                                    && x.ClassID == item.ClassProFileID)
                                                                    .FirstOrDefault().MonitoringBookID;
                int nutri = lstNutri.Where(x => x.PupilProFileID == item.PupilProFileID).FirstOrDefault().Nutrition;
                var resultPhysicalDB = lstPhysicalDB.Where(x => x.MonitoringBookID == monitoringBookId).FirstOrDefault();

                if (!item.Height.HasValue && !item.Weight.HasValue && !item.Breast.HasValue
                        && !item.PhysicalClassification.HasValue)
                {
                    if (resultPhysicalDB != null)
                    {
                        this.PhysicalTestBusiness.Delete(resultPhysicalDB.PhysicalTestID);
                    }
                    else
                        continue;
                }
                else
                {
                    if (resultPhysicalDB != null)
                    {
                        // cập nhật thể lực                              
                        resultPhysicalDB.Height = item.Height;
                        resultPhysicalDB.Weight = item.Weight;
                        resultPhysicalDB.Breast = item.Breast;
                        resultPhysicalDB.PhysicalClassification = item.PhysicalClassification;
                        resultPhysicalDB.ModifiedDate = item.ModifineDate;
                        resultPhysicalDB.Nutrition = nutri;
                        resultPhysicalDB.LogID = item.LogID;
                        this.PhysicalTestBusiness.Update(resultPhysicalDB);
                    }
                    else
                    {
                        objPhysical = new PhysicalTest()
                        {
                            MonitoringBookID = monitoringBookId,
                            Date = item.Date.Value,
                            Height = item.Height,
                            Weight = item.Weight,
                            Breast = item.Breast,
                            PhysicalClassification = item.PhysicalClassification,
                            CreatedDate = item.CreateDate,
                            IsActive = item.IsActive,
                            ModifiedDate = item.ModifineDate,
                            Nutrition = nutri,
                            LogID = item.LogID
                        };
                        this.PhysicalTestBusiness.Insert(objPhysical);
                    }
                }
            }
            this.PhysicalTestBusiness.Save();
            #endregion
        }
        #endregion

        #region InsertOrUpdateValueEye
        /// <summary>
        /// Thêm mới thông tin sổ theo dõi sức khỏe
        /// <author>Thuyen</author>
        /// <date>05/09/2017</date>
        /// </summary>
        /// <param name="monitoringBook">Đối tượng Insert</param>
        /// <returns></returns>
        public void InsertOrUpdateValueEye(
            List<MonitoringBookBO> lstMonitoringBook,
            List<EyeTestBO> lstEyeTest,
            IDictionary<string, object> SearchInfo)
        {
            List<int> lstPupilID = Utils.GetIntList(SearchInfo, "lstPupilIDOfEye");
            int educationId = Utils.GetInt(SearchInfo, "EducationLevelID");
            List<MonitoringBook> lstMonitoringBookDB = MonitoringBookBusiness.Search(SearchInfo)
                .Where(x => x.EducationLevelID == educationId).ToList();
            List<int> lstMoniteringBookID = lstMonitoringBookDB.Select(p => p.MonitoringBookID).Distinct().ToList();

            List<MonitoringBook> lstMonitoringBookFlowPupilID = new List<MonitoringBook>();
            foreach (var item in lstPupilID)
            {
                var objMoniBook = lstMonitoringBookDB.Where(x => x.PupilID == item).ToList();
                if (objMoniBook.Count > 0)
                {
                    foreach (var itemObj in objMoniBook)
                    {
                        lstMonitoringBookFlowPupilID.Add(itemObj);
                    }

                }
            }

            #region
            var lstEyeTestDB = EyeTestBusiness.All.Where(x => lstMoniteringBookID.Contains(x.MonitoringBookID)).ToList();
            EyeTest objEye = null;
            MonitoringBook objMoni = null;

            foreach (var item in lstMonitoringBook)
            {
                var testMoniBD = lstMonitoringBookFlowPupilID.Where(x => x.PupilID == item.PupilID && x.ClassID == item.ClassID).FirstOrDefault();
                if (testMoniBD == null)
                {
                    objMoni = new MonitoringBook()
                    {
                        AcademicYearID = item.AcademicYearID,
                        SchoolID = item.SchoolID,
                        ClassID = item.ClassID,
                        PupilID = item.PupilID,
                        MonitoringDate = item.MonitoringDate,
                        Symptoms = item.Symptoms,
                        Solution = item.Solution,
                        UnitName = item.UnitName,
                        MonitoringType = item.MonitoringType,
                        HealthPeriodID = item.HealthPeriodID,
                        CreateDate = item.CreateDate,
                        EducationLevelID = item.EducationLevelID,
                    };
                    this.MonitoringBookRepository.Insert(objMoni);
                }
            }
            this.MonitoringBookRepository.Save();

            List<MonitoringBook> lstMonitoringBookIDAfterSave = MonitoringBookBusiness.Search(SearchInfo)
          .Where(x => lstPupilID.Contains(x.PupilID) && x.EducationLevelID == educationId).ToList();

            foreach (var item in lstEyeTest)
            {
                int monitoringBookId = lstMonitoringBookIDAfterSave.Where(x => x.PupilID == item.PupilProFileID
                                                                            && x.ClassID == item.ClassID)
                                                                            .FirstOrDefault().MonitoringBookID;
                var resultEyeDB = lstEyeTestDB.Where(x => x.MonitoringBookID == monitoringBookId).FirstOrDefault();
                if (!item.LEye.HasValue && !item.REye.HasValue
                        && !item.LIsEye.HasValue && !item.RIsEye.HasValue
                        && !item.RMyopic.HasValue && !item.LMyopic.HasValue
                        && !item.LAstigmatism.HasValue && !item.RAstigmatism.HasValue
                        && !item.LFarsightedness.HasValue && !item.RFarsightedness.HasValue && string.IsNullOrEmpty(item.Other))
                {
                    if (resultEyeDB != null)
                    {
                        this.EyeTestBusiness.Delete(resultEyeDB.EyeTestID);
                    }
                    else
                        continue;
                }
                else
                {
                    if (resultEyeDB != null)
                    {
                        resultEyeDB.REye = item.REye;
                        resultEyeDB.LEye = item.LEye;
                        resultEyeDB.RIsEye = item.RIsEye;
                        resultEyeDB.LIsEye = item.LIsEye;
                        resultEyeDB.RMyopic = item.RMyopic;
                        resultEyeDB.LMyopic = item.LMyopic;
                        resultEyeDB.RFarsightedness = item.RFarsightedness;
                        resultEyeDB.LFarsightedness = item.LFarsightedness;
                        resultEyeDB.RAstigmatism = item.RAstigmatism;
                        resultEyeDB.LAstigmatism = item.LAstigmatism;
                        resultEyeDB.Other = item.Other;
                        resultEyeDB.ModifiedDate = item.ModifiedDate;
                        resultEyeDB.LogID = item.LogID;
                        resultEyeDB.IsTreatment = item.IsTreatment.Value;
                        resultEyeDB.IsAstigmatism = item.IsAstigmatism;
                        resultEyeDB.IsFarsightedness = item.IsFarsightedness;
                        resultEyeDB.IsMyopic = item.IsMyopic;
                        this.EyeTestBusiness.Update(resultEyeDB);
                    }
                    else
                    {
                        objEye = new EyeTest()
                        {
                            MonitoringBookID = monitoringBookId,
                            REye = item.REye,
                            LEye = item.LEye,
                            RIsEye = item.RIsEye,
                            LIsEye = item.LIsEye,
                            RMyopic = item.RMyopic,
                            LMyopic = item.LMyopic,
                            RFarsightedness = item.RFarsightedness,
                            LFarsightedness = item.LFarsightedness,
                            RAstigmatism = item.RAstigmatism,
                            LAstigmatism = item.LAstigmatism,
                            Other = item.Other,
                            IsTreatment = item.IsTreatment.Value,
                            CreatedDate = item.CreateDate,
                            IsActive = item.IsActive,
                            ModifiedDate = item.ModifiedDate,
                            LogID = item.LogID,
                            IsAstigmatism = item.IsAstigmatism,
                            IsFarsightedness = item.IsFarsightedness,
                            IsMyopic = item.IsMyopic,
                        };
                        this.EyeTestBusiness.Insert(objEye);
                    }
                }
            }
            this.EyeTestBusiness.Save();
            #endregion

        }
        #endregion

        #region InsertOrUpdateValueENT
        /// <summary>
        /// Thêm mới thông tin sổ theo dõi sức khỏe
        /// <author>Thuyen</author>
        /// <date>05/09/2017</date>
        /// </summary>
        /// <param name="monitoringBook">Đối tượng Insert</param>
        /// <returns></returns>
        public void InsertOrUpdateValueENT(
            List<MonitoringBookBO> lstMonitoringBook,
            List<ENTTestBO> lstEntTest,
            IDictionary<string, object> SearchInfo)
        {
            List<int> lstPupilID = Utils.GetIntList(SearchInfo, "lstPupilIDOfENT");
            int educationId = Utils.GetInt(SearchInfo, "EducationLevelID");
            List<MonitoringBook> lstMonitoringBookDB = MonitoringBookBusiness.Search(SearchInfo)
                .Where(x => x.EducationLevelID == educationId).ToList();

            List<int> lstMoniteringBookID = lstMonitoringBookDB.Select(p => p.MonitoringBookID).Distinct().ToList();

            List<MonitoringBook> lstMonitoringBookFlowPupilID = new List<MonitoringBook>();
            foreach (var item in lstPupilID)
            {
                var objMoniBook = lstMonitoringBookDB.Where(x => x.PupilID == item).ToList();
                if (objMoniBook.Count > 0)
                {
                    foreach (var itemObj in objMoniBook)
                    {
                        lstMonitoringBookFlowPupilID.Add(itemObj);
                    }

                }
            }

            #region
            var lstEntTestDB = ENTTestBusiness.All.Where(x => lstMoniteringBookID.Contains(x.MonitoringBookID)).ToList();
            ENTTest objEnt = null;
            MonitoringBook objMoni = null;

            foreach (var item in lstMonitoringBook)
            {
                var testMoniBD = lstMonitoringBookFlowPupilID.Where(x => x.PupilID == item.PupilID && x.ClassID== item.ClassID).FirstOrDefault();
                if (testMoniBD == null)
                {
                    objMoni = new MonitoringBook()
                    {
                        AcademicYearID = item.AcademicYearID,
                        SchoolID = item.SchoolID,
                        ClassID = item.ClassID,
                        PupilID = item.PupilID,
                        MonitoringDate = item.MonitoringDate,
                        Symptoms = item.Symptoms,
                        Solution = item.Solution,
                        UnitName = item.UnitName,
                        MonitoringType = item.MonitoringType,
                        HealthPeriodID = item.HealthPeriodID,
                        CreateDate = item.CreateDate,
                        EducationLevelID = item.EducationLevelID,
                    };
                    this.MonitoringBookRepository.Insert(objMoni);
                }
            }
            this.MonitoringBookRepository.Save();
            List<MonitoringBook> lstMonitoringBookIDAfterSave = MonitoringBookBusiness.Search(SearchInfo)
                .Where(x => lstPupilID.Contains(x.PupilID) && x.EducationLevelID == educationId).ToList();

            foreach (var item in lstEntTest)
            {
                int monitoringBookId = lstMonitoringBookIDAfterSave.Where(x => x.PupilID == item.PupilProFileID && x.ClassID == item.ClassID).FirstOrDefault().MonitoringBookID;
                var resultEntDB = lstEntTestDB.Where(x => x.MonitoringBookID == monitoringBookId).FirstOrDefault();
                if (!item.RCapacityHearing.HasValue && !item.LCapacityHearing.HasValue
                        && item.IsDeaf.Value == false && item.IsSick.Value == false && string.IsNullOrEmpty(item.Description))
                {
                    if (resultEntDB != null)
                    {
                        this.ENTTestBusiness.Delete(resultEntDB.ENTTestID);
                    }
                    else
                        continue;
                }
                else
                {
                    if (resultEntDB != null)
                    {
                        resultEntDB.LCapacityHearing = item.LCapacityHearing;
                        resultEntDB.RCapacityHearing = item.RCapacityHearing;
                        resultEntDB.IsSick = item.IsSick.Value;
                        resultEntDB.IsDeaf = item.IsDeaf.Value;
                        resultEntDB.Description = item.Description;
                        resultEntDB.ModifiedDate = item.ModifiedDate;
                        resultEntDB.LogID = item.LogID;
                        this.ENTTestBusiness.Update(resultEntDB);
                    }
                    else
                    {
                        objEnt = new ENTTest()
                        {
                            MonitoringBookID = monitoringBookId,
                            LCapacityHearing = item.LCapacityHearing,
                            RCapacityHearing = item.RCapacityHearing,
                            IsSick = item.IsSick,
                            IsDeaf = item.IsDeaf,
                            Description = item.Description,
                            ModifiedDate = item.ModifiedDate,
                            CreatedDate = item.CreatedDate,
                            IsActive = item.IsActive,
                            LogID = item.LogID,
                        };
                        this.ENTTestBusiness.Insert(objEnt);
                    }
                }
            }
            this.ENTTestBusiness.Save();
            #endregion
        }
        #endregion

        #region InsertOrUpdateValueDental
        /// <summary>
        /// Thêm mới thông tin sổ theo dõi sức khỏe
        /// <author>Thuyen</author>
        /// <date>05/09/2017</date>
        /// </summary>
        /// <param name="monitoringBook">Đối tượng Insert</param>
        /// <returns></returns>
        public void InsertOrUpdateValueDental(
            List<MonitoringBookBO> lstMonitoringBook,
            List<DentalTestBO> lstDentalTest,
            IDictionary<string, object> SearchInfo)
        {
            List<int> lstPupilID = Utils.GetIntList(SearchInfo, "lstPupilIDOfDental");
            int educationId = Utils.GetInt(SearchInfo, "EducationLevelID");
            List<MonitoringBook> lstMonitoringBookDB = MonitoringBookBusiness.Search(SearchInfo)
                .Where(x => x.EducationLevelID == educationId).ToList();

            List<int> lstMoniteringBookID = lstMonitoringBookDB.Select(p => p.MonitoringBookID).Distinct().ToList();

            List<MonitoringBook> lstMonitoringBookFlowPupilID = new List<MonitoringBook>();
            foreach (var item in lstPupilID)
            {
                var objMoniBook = lstMonitoringBookDB.Where(x => x.PupilID == item).ToList();
                if (objMoniBook.Count > 0)
                {
                    foreach (var itemObj in objMoniBook)
                {
                        lstMonitoringBookFlowPupilID.Add(itemObj);
                    }

                }
            }

            #region Insert Dental
            var lstDentalDB = DentalTestBusiness.All.Where(x => lstMoniteringBookID.Contains(x.MonitoringBookID)).ToList();
            DentalTest objDental = null;
            MonitoringBook objMoni = null;
            foreach (var item in lstMonitoringBook)
            {
                var testMoniBD = lstMonitoringBookFlowPupilID.Where(x => x.PupilID == item.PupilID && x.ClassID == item.ClassID).FirstOrDefault();
                var objInput = lstDentalTest.Where(x => x.PupilProFileID == item.PupilID && x.ClassID == item.ClassID).FirstOrDefault();
                if (testMoniBD != null)
                {
                    var testPhyDB = lstDentalDB.Where(x => x.MonitoringBookID == testMoniBD.MonitoringBookID).FirstOrDefault();

                    if (testPhyDB != null)
                    {
                        if ((objInput.NumDentalFillingCustom == 0 || objInput.NumDentalFillingCustom == null) &&
                            string.IsNullOrEmpty(objInput.NumDentalFillingOne) &&
                            string.IsNullOrEmpty(objInput.NumDentalFillingTwo) &&
                            string.IsNullOrEmpty(objInput.NumDentalFillingThree) &&
                            string.IsNullOrEmpty(objInput.NumDentalFillingFour) &&
                            string.IsNullOrEmpty(objInput.Other) &&
                            objInput.NumTeethExtracted == null &&
                            objInput.NumPreventiveDentalFilling == null &&
                            objInput.IsTreatmentCustom == false &&
                            objInput.IsScraptTeethCustom == false
                        )
                        {
                            this.DentalTestBusiness.Delete(testPhyDB.DentalTestID);
                        }
                        else
                        {
                            // cập nhật thể lực                              
                            testPhyDB.IsScraptTeeth = objInput.IsScraptTeethCustom;
                            testPhyDB.IsTreatment = objInput.IsTreatmentCustom;
                            testPhyDB.NumTeethExtracted = objInput.NumTeethExtracted;
                            testPhyDB.NumPreventiveDentalFilling = objInput.NumPreventiveDentalFilling;
                            testPhyDB.NumDentalFilling = objInput.NumDentalFillingCustom;
                            testPhyDB.Other = objInput.Other;
                            testPhyDB.ModifiedDate = objInput.ModifineDate;
                            testPhyDB.NumDentalFillingOne = objInput.NumDentalFillingOne;
                            testPhyDB.NumDentalFillingTwo = objInput.NumDentalFillingTwo;
                            testPhyDB.NumDentalFillingThree = objInput.NumDentalFillingThree;
                            testPhyDB.NumDentalFillingFour = objInput.NumDentalFillingFour;
                            testPhyDB.ModifiedDate = objInput.ModifiedDate;
                            this.DentalTestBusiness.Update(testPhyDB);
                        }
                    }
                    else
                    { // insert thể lực
                        objDental = new DentalTest()
                        {
                            MonitoringBookID = testMoniBD.MonitoringBookID,
                            IsScraptTeeth = objInput.IsScraptTeethCustom,
                            IsTreatment = objInput.IsTreatmentCustom,
                            NumTeethExtracted = objInput.NumTeethExtracted,
                            NumPreventiveDentalFilling = objInput.NumPreventiveDentalFilling,
                            NumDentalFilling = objInput.NumDentalFillingCustom,
                            NumDentalFillingOne = objInput.NumDentalFillingOne,
                            NumDentalFillingTwo = objInput.NumDentalFillingTwo,
                            NumDentalFillingThree = objInput.NumDentalFillingThree,
                            NumDentalFillingFour = objInput.NumDentalFillingFour,
                            Other = objInput.Other,
                            CreatedDate = objInput.CreateDate,
                            IsActive = true,
                            ModifiedDate = objInput.ModifineDate,
                            LogID = objInput.LogID
                        };
                        this.DentalTestBusiness.Insert(objDental);
                    }
                }
                else // insert
                {
                    int moniteringBookID = MonitoringBookBusiness.GetNextSeq<int>();
                    objMoni = new MonitoringBook()
                    {
                        AcademicYearID = item.AcademicYearID,
                        MonitoringBookID = moniteringBookID,
                        SchoolID = item.SchoolID,
                        ClassID = item.ClassID,
                        PupilID = item.PupilID,
                        MonitoringDate = item.MonitoringDate,
                        Symptoms = item.Symptoms,
                        Solution = item.Solution,
                        UnitName = item.UnitName,
                        MonitoringType = item.MonitoringType,
                        HealthPeriodID = item.HealthPeriodID,
                        CreateDate = item.CreateDate,
                        EducationLevelID = item.EducationLevelID,
                    };
                    this.MonitoringBookRepository.Insert(objMoni);

                    objDental = new DentalTest()
                    {
                        MonitoringBookID = objMoni.MonitoringBookID,
                        IsScraptTeeth = objInput.IsScraptTeethCustom,
                        IsTreatment = objInput.IsTreatmentCustom,
                        NumTeethExtracted = objInput.NumTeethExtracted,
                        NumPreventiveDentalFilling = objInput.NumPreventiveDentalFilling,
                        NumDentalFilling = objInput.NumDentalFillingCustom,
                        NumDentalFillingOne = objInput.NumDentalFillingOne,
                        NumDentalFillingTwo = objInput.NumDentalFillingTwo,
                        NumDentalFillingThree = objInput.NumDentalFillingThree,
                        NumDentalFillingFour = objInput.NumDentalFillingFour,
                        Other = objInput.Other,
                        CreatedDate = objInput.CreateDate,
                        IsActive = true,
                        ModifiedDate = objInput.ModifineDate,
                        LogID = objInput.LogID
                    };
                    this.DentalTestBusiness.Insert(objDental);
                }
            }
            this.MonitoringBookBusiness.Save();
            this.DentalTestBusiness.Save();
            #endregion
        }
        #endregion

        #region InsertOrUpdateValueSpine
        public void InsertOrUpdateValueSpine(
            List<MonitoringBookBO> lstMonitoringBook,
            List<SpineTestBO> lstSpineTest,
            IDictionary<string, object> SearchInfo)
        {
            List<int> lstPupilID = Utils.GetIntList(SearchInfo, "lstPupilIDOfSpine");
            int educationId = Utils.GetInt(SearchInfo, "EducationLevelID");
            List<MonitoringBook> lstMonitoringBookDB = MonitoringBookBusiness.Search(SearchInfo)
                .Where(x => x.EducationLevelID == educationId).ToList();

            List<int> lstMoniteringBookID = lstMonitoringBookDB.Select(p => p.MonitoringBookID).Distinct().ToList();

            List<MonitoringBook> lstMonitoringBookFlowPupilID = new List<MonitoringBook>();
            foreach (var item in lstPupilID)
            {
                var objMoniBook = lstMonitoringBookDB.Where(x => x.PupilID == item).ToList();
                if (objMoniBook.Count > 0)
                {
                    foreach (var itemObj in objMoniBook)
                {
                        lstMonitoringBookFlowPupilID.Add(itemObj);
                    }
                }
            }

            #region
            var lstSpineTestDB = SpineTestBusiness.All.Where(x => lstMoniteringBookID.Contains(x.MonitoringBookID)).ToList();
            SpineTest objSpine = null;
            MonitoringBook objMoni = null;

            foreach (var item in lstMonitoringBook)
            {
                var testMoniBD = lstMonitoringBookFlowPupilID.Where(x => x.PupilID == item.PupilID && x.ClassID == item.ClassID).FirstOrDefault();
                if (testMoniBD == null)
                {
                    objMoni = new MonitoringBook()
                    {
                        AcademicYearID = item.AcademicYearID,
                        SchoolID = item.SchoolID,
                        ClassID = item.ClassID,
                        PupilID = item.PupilID,
                        MonitoringDate = item.MonitoringDate,
                        Symptoms = item.Symptoms,
                        Solution = item.Solution,
                        UnitName = item.UnitName,
                        MonitoringType = item.MonitoringType,
                        HealthPeriodID = item.HealthPeriodID,
                        CreateDate = item.CreateDate,
                        EducationLevelID = item.EducationLevelID,
                    };
                    this.MonitoringBookRepository.Insert(objMoni);
                }
            }
            this.MonitoringBookRepository.Save();
            List<MonitoringBook> lstMonitoringBookIDAfterSave = MonitoringBookBusiness.Search(SearchInfo)
                .Where(x => lstPupilID.Contains(x.PupilID) && x.EducationLevelID == educationId).ToList();

            foreach (var item in lstSpineTest)
            {
                int monitoringBookId = lstMonitoringBookIDAfterSave.Where(x => x.PupilID == item.PupilProFileID && x.ClassID == item.ClassID).FirstOrDefault().MonitoringBookID;
                var resultSpineDB = lstSpineTestDB.Where(x => x.MonitoringBookID == monitoringBookId).FirstOrDefault();

                if (string.IsNullOrEmpty(item.ExaminationStraight)
                                && string.IsNullOrEmpty(item.ExaminationItalic)
                                && string.IsNullOrEmpty(item.Other)
                                && item.IsDeformity == false)
                {
                    if (resultSpineDB != null)
                    {
                        this.SpineTestBusiness.Delete(resultSpineDB.SpineTestID);
                    }
                    else
                        continue;
                }
                else
                {
                    if (resultSpineDB != null)
                    {
                        resultSpineDB.IsDeformityOfTheSpine = item.IsDeformity;
                        resultSpineDB.ExaminationStraight = item.ExaminationStraight;
                        resultSpineDB.ExaminationItalic = item.ExaminationItalic;
                        resultSpineDB.Other = item.Other;
                        resultSpineDB.ModifiedDate = item.ModifineDate;
                        this.SpineTestBusiness.Update(resultSpineDB);
                    }
                    else
                    {
                        objSpine = new SpineTest()
                    {
                            MonitoringBookID = monitoringBookId,
                            IsDeformityOfTheSpine = item.IsDeformity,
                            ExaminationStraight = item.ExaminationStraight,
                            ExaminationItalic = item.ExaminationItalic,
                            Other = item.Other,
                            CreatedDate = item.CreateDate,
                            IsActive = item.IsActive,
                            ModifiedDate = item.ModifineDate,
                            LogID = item.LogID
                    };
                        this.SpineTestBusiness.Insert(objSpine);
                    }
                }
            }
            this.SpineTestBusiness.Save();
            #endregion
        }
        #endregion

        #region InsertOrUpdateValueOverAll
        /// <summary>
        /// Thêm mới thông tin sổ theo dõi sức khỏe
        /// <author>Thuyen</author>
        /// <date>05/09/2017</date>
        /// </summary>
        /// <param name="monitoringBook">Đối tượng Insert</param>
        /// <returns></returns>
        public void InsertOrUpdateValueOverAll(
            List<MonitoringBookBO> lstMonitoringBook,
            List<OverallTestBO> lstOverallTest,
            IDictionary<string, object> SearchInfo)
        {
            List<int> lstPupilID = Utils.GetIntList(SearchInfo, "lstPupilIDofOverAll");
            int educationId = Utils.GetInt(SearchInfo, "EducationLevelID");
            List<MonitoringBook> lstMonitoringBookDB = MonitoringBookBusiness.Search(SearchInfo)
                .Where(x => x.EducationLevelID == educationId).ToList();

            List<int> lstMoniteringBookID = lstMonitoringBookDB.Select(p => p.MonitoringBookID).Distinct().ToList();

            List<MonitoringBook> lstMonitoringBookFlowPupilID = new List<MonitoringBook>();
            foreach (var item in lstPupilID)
            {
                var objMoniBook = lstMonitoringBookDB.Where(x => x.PupilID == item).ToList();
                if (objMoniBook.Count > 0)
                {
                    foreach (var itemObj in objMoniBook)
                    {
                        lstMonitoringBookFlowPupilID.Add(itemObj);
                    }

                }
            }

            #region Insert OverAll
            IQueryable<ClassificationCriteria> lstClassificationCriteria = ClassificationCriteriaBusiness.All;
            IQueryable<ClassificationCriteriaDetail> lstClassificationCriteriaDetail = ClassificationCriteriaDetailBusiness.All;

            var lstPhysicalDB = PhysicalTestBusiness.All.Where(x => lstMoniteringBookID.Contains(x.MonitoringBookID)).ToList();
            var lstOverAllDB = OverallTestBusiness.All.Where(x => lstMoniteringBookID.Contains(x.MonitoringBookID)).ToList();
            OverallTest objOverAll = null;
            MonitoringBook objMoni = null;
            int nutrition = 0;
            foreach (var item in lstMonitoringBook)
            {
                var testMoniBD = lstMonitoringBookFlowPupilID.Where(x => x.PupilID == item.PupilID && x.ClassID == item.ClassID).FirstOrDefault();
                var objInput = lstOverallTest.Where(x => x.PupilProFileID == item.PupilID && x.ClassID == item.ClassID).FirstOrDefault();
                if (testMoniBD != null)
                {
                    var testPhyDB = lstPhysicalDB.Where(x => x.MonitoringBookID == testMoniBD.MonitoringBookID).FirstOrDefault();
                    var testOverAllDB = lstOverAllDB.Where(x => x.MonitoringBookID == testMoniBD.MonitoringBookID).FirstOrDefault();

                    // cập nhật
                    testMoniBD.UnitName = item.UnitName;
                    testMoniBD.MonitoringDate = item.MonitoringDate;
                    this.MonitoringBookRepository.Update(testMoniBD);

                    if (testPhyDB != null)
                    {
                        if (testPhyDB.Weight.HasValue && testPhyDB.Height.HasValue && item.MonitoringDate.HasValue)
                        {
                            decimal defaultForMin, defaultForMax;
                            decimal minThreshold, maxThreshold;
                            decimal index;
                            int typeConfigID;
                            int month = caculateNumberOfMonthBetweenTowDate(item.BirthDate, item.MonitoringDate.Value);
                            if (month <= 60)
                            {
                                typeConfigID = 5; // ID cua tieu chi can nang
                                defaultForMin = 2m;
                                defaultForMax = 9m;
                                index = testPhyDB.Weight.Value;
                            }
                            else
                            {
                                typeConfigID = 7; // ID cua tieu chi BMI
                                defaultForMin = 18m;
                                defaultForMax = 24.9m;
                                index = Math.Round(testPhyDB.Weight.Value / ((testPhyDB.Height.Value * 0.01m) * (testPhyDB.Height.Value * 0.01m)), 2);
                            }
                            int ClassificationCriteriaID = 0;
                            bool Genre = item.Genre == 1 ? true : false;
                            var objClassificationCriteria = lstClassificationCriteria.Where(x => x.TypeConfigID == typeConfigID && x.Genre == Genre).ToList();
                            if (objClassificationCriteria.Count() > 0)
                            {
                                ClassificationCriteriaID = objClassificationCriteria.FirstOrDefault().ClassificationCriteriaID;
                            }
                            else
                                ClassificationCriteriaID = -1;

                            var queryByMonth = lstClassificationCriteriaDetail.Where(x => x.Month == month && x.ClassificationCriteriaID == ClassificationCriteriaID)
                                .Select(x => new { x.IndexValue, x.Month, x.ClassificationCriteria.EffectDate, x.ClassificationLevelHealth.Level }).ToList();

                            var minValue = queryByMonth.Where(p => p.Level == -3).OrderByDescending(o => o.EffectDate).FirstOrDefault();
                            minThreshold = (minValue == null || !minValue.IndexValue.HasValue) ? defaultForMin : minValue.IndexValue.Value;

                            var maxValue = queryByMonth.Where(p => p.Level == 3).OrderByDescending(o => o.EffectDate).FirstOrDefault();
                            maxThreshold = (maxValue == null || !maxValue.IndexValue.HasValue) ? defaultForMax : maxValue.IndexValue.Value;

                            nutrition = index < minThreshold ? SystemParamsInFile.NUTRITION_TYPE_MALNUTRITION : index <= maxThreshold ? SystemParamsInFile.NUTRITION_TYPE_NORMALWEIGHT : SystemParamsInFile.NUTRITION_TYPE_OVERWEIGHT;
                        }
                        testPhyDB.Nutrition = nutrition;
                        this.PhysicalTestBusiness.Update(testPhyDB);
                        nutrition = 0;
                    }
                    this.MonitoringBookRepository.Update(testMoniBD);

                    if (testOverAllDB != null)
                    {
                        if (string.IsNullOrEmpty(objInput.HistoryYourSelf) &&
                            (objInput.OverallInternal == 0 || objInput.OverallInternal == 2) &&
                            (objInput.OverallForeign == 0 || objInput.OverallForeign == 2) &&
                            (objInput.SkinDiseases == 0 || objInput.SkinDiseases == 2) &&
                            objInput.IsHeartDiseases == false &&
                            string.IsNullOrEmpty(objInput.Other) &&
                            objInput.IsBirthDefect == false &&
                            objInput.EvaluationHealth == 0 &&
                            string.IsNullOrEmpty(objInput.Doctor) &&
                            string.IsNullOrEmpty(objInput.RequireOfDoctor))
                        {
                            this.OverallTestBusiness.Delete(testOverAllDB.OverallTestID);
                        }
                        else
                        {
                            // cập nhật                            
                            testOverAllDB.HistoryYourSelf = objInput.HistoryYourSelf;
                            testOverAllDB.OverallInternal = objInput.OverallInternal.HasValue ? objInput.OverallInternal.Value : 0;
                            testOverAllDB.DescriptionOverallInternall = objInput.DescriptionOverallInternal;
                            testOverAllDB.OverallForeign = objInput.OverallForeign.HasValue ? objInput.OverallForeign.Value : 0;
                            testOverAllDB.DescriptionForeign = objInput.DescriptionOverallForeign;
                            testOverAllDB.Other = objInput.Other;
                            testOverAllDB.ModifiedDate = objInput.ModifineDate;
                            testOverAllDB.SkinDiseases = objInput.SkinDiseases.HasValue ? objInput.SkinDiseases.Value : 0;
                            testOverAllDB.DescriptionSkinDiseases = objInput.DescriptionSkinDiseases;
                            testOverAllDB.Doctor = objInput.Doctor;
                            testOverAllDB.EvaluationHealth = objInput.EvaluationHealth.HasValue ? objInput.EvaluationHealth.Value : 0;
                            testOverAllDB.LogID = objInput.LogID;
                            testOverAllDB.IsBirthDefect = objInput.IsBirthDefect.HasValue ? objInput.IsBirthDefect.Value : false;
                            testOverAllDB.IsHeartDiseases = objInput.IsHeartDiseases.HasValue ? objInput.IsHeartDiseases.Value : false;
                            testOverAllDB.IsDredging = objInput.IsDredging.HasValue ? objInput.IsDredging.Value : false;
                            testOverAllDB.RequireOfDoctor = objInput.RequireOfDoctor;
                            this.OverallTestBusiness.Update(testOverAllDB);
                        }
                    }
                    else
                    { // insert thể lực
                        objOverAll = new OverallTest()
                        {
                            MonitoringBookID = testMoniBD.MonitoringBookID,
                            HistoryYourSelf = objInput.HistoryYourSelf,
                            OverallInternal = objInput.OverallInternal.HasValue ? objInput.OverallInternal.Value : 0,
                            DescriptionOverallInternall = objInput.DescriptionOverallInternal,
                            OverallForeign = objInput.OverallForeign.HasValue ? objInput.OverallForeign.Value : 0,
                            DescriptionForeign = objInput.DescriptionOverallForeign,
                            Other = objInput.Other,
                            ModifiedDate = objInput.ModifineDate,
                            CreatedDate = objInput.CreateDate,
                            SkinDiseases = objInput.SkinDiseases.HasValue ? objInput.SkinDiseases.Value : 0,
                            DescriptionSkinDiseases = objInput.DescriptionSkinDiseases,
                            Doctor = objInput.Doctor,
                            EvaluationHealth = objInput.EvaluationHealth.HasValue ? objInput.EvaluationHealth.Value : 0,
                            LogID = objInput.LogID,
                            IsBirthDefect = objInput.IsBirthDefect.HasValue ? objInput.IsBirthDefect.Value : false,
                            IsHeartDiseases = objInput.IsHeartDiseases.HasValue ? objInput.IsHeartDiseases.Value : false,
                            IsDredging = objInput.IsDredging.HasValue ? objInput.IsDredging.Value : false,
                            IsActive = true,
                            RequireOfDoctor = objInput.RequireOfDoctor,
                        };
                        this.OverallTestBusiness.Insert(objOverAll);
                    }
                }
                else // insert
                {
                    int moniteringBookID = MonitoringBookBusiness.GetNextSeq<int>();
                    objMoni = new MonitoringBook()
                    {
                        AcademicYearID = item.AcademicYearID,
                        MonitoringBookID = moniteringBookID,
                        SchoolID = item.SchoolID,
                        ClassID = item.ClassID,
                        PupilID = item.PupilID,
                        MonitoringDate = item.MonitoringDate,
                        Symptoms = item.Symptoms,
                        Solution = item.Solution,
                        UnitName = item.UnitName,
                        MonitoringType = item.MonitoringType,
                        HealthPeriodID = item.HealthPeriodID,
                        CreateDate = item.CreateDate,
                        EducationLevelID = item.EducationLevelID,
                    };
                    this.MonitoringBookRepository.Insert(objMoni);

                    objOverAll = new OverallTest()
                    {
                        MonitoringBookID = objMoni.MonitoringBookID,
                        HistoryYourSelf = objInput.HistoryYourSelf,
                        OverallInternal = objInput.OverallInternal.HasValue ? objInput.OverallInternal.Value : 0,
                        DescriptionOverallInternall = objInput.DescriptionOverallInternal,
                        OverallForeign = objInput.OverallForeign.HasValue ? objInput.OverallForeign.Value : 0,
                        DescriptionForeign = objInput.DescriptionOverallForeign,
                        Other = objInput.Other,
                        ModifiedDate = objInput.ModifineDate,
                        CreatedDate = objInput.CreateDate,
                        SkinDiseases = objInput.SkinDiseases.HasValue ? objInput.SkinDiseases.Value : 0,
                        DescriptionSkinDiseases = objInput.DescriptionSkinDiseases,
                        Doctor = objInput.Doctor,
                        EvaluationHealth = objInput.EvaluationHealth.HasValue ? objInput.EvaluationHealth.Value : 0,
                        LogID = objInput.LogID,
                        IsBirthDefect = objInput.IsBirthDefect.HasValue ? objInput.IsBirthDefect.Value : false,
                        IsHeartDiseases = objInput.IsHeartDiseases.HasValue ? objInput.IsHeartDiseases.Value : false,
                        IsDredging = objInput.IsDredging.HasValue ? objInput.IsDredging.Value : false,
                        IsActive = true,
                        RequireOfDoctor = objInput.RequireOfDoctor,
                    };
                    this.OverallTestBusiness.Insert(objOverAll);
                }
            }
            this.MonitoringBookBusiness.Save();
            PhysicalTestBusiness.Save();
            this.OverallTestBusiness.Save();
            #endregion
        }
        #endregion

        #region Delete
        public void DeletePhysical(IDictionary<string, object> dic)
        {
            int ClassID = Utils.GetInt(dic, "ClassID");
            List<int> lstPupilID = Utils.GetIntList(dic, "lstPupilID");

            List<MonitoringBook> lstMonitoringBookDB = MonitoringBookBusiness.Search(dic).ToList();
            List<int> lstMoniteringBookID = lstMonitoringBookDB.Select(p => p.MonitoringBookID).Distinct().ToList();
            var lstPhysicalDB = PhysicalTestBusiness.All.Where(x => lstMoniteringBookID.Contains(x.MonitoringBookID)).ToList();
            List<PupilOfClassBO> lstPOC = PupilOfClassBusiness.GetPupilInClass(ClassID)
                                                                .Where(p => lstPupilID.Contains(p.PupilID))
                                                                .Distinct().ToList();
            foreach (var item in lstMonitoringBookDB)
            {
                var objPOC = lstPOC.Where(p => p.PupilID == item.PupilID).FirstOrDefault();
                if (objPOC != null)
                {
                    var monitoringBookID = item.MonitoringBookID;
                    var resultPhy = lstPhysicalDB.Where(x => x.MonitoringBookID == monitoringBookID).FirstOrDefault();
                    if (resultPhy != null)
                        PhysicalTestBusiness.Delete(resultPhy.PhysicalTestID);
                }
            }
            base.Save();
        }

        public void DeleteSpin(IDictionary<string, object> dic)
        {
            int ClassID = Utils.GetInt(dic, "ClassID");
            List<int> lstPupilID = Utils.GetIntList(dic, "lstPupilID");

            List<MonitoringBook> lstMonitoringBookDB = MonitoringBookBusiness.Search(dic).ToList();
            List<int> lstMoniteringBookID = lstMonitoringBookDB.Select(p => p.MonitoringBookID).Distinct().ToList();
            var lstSpinDB = SpineTestBusiness.All.Where(x => lstMoniteringBookID.Contains(x.MonitoringBookID)).ToList();
            List<PupilOfClassBO> lstPOC = PupilOfClassBusiness.GetPupilInClass(ClassID)
                                                                .Where(p => lstPupilID.Contains(p.PupilID))
                                                                .Distinct().ToList();
            foreach (var item in lstMonitoringBookDB)
            {
                var objPOC = lstPOC.Where(p => p.PupilID == item.PupilID).FirstOrDefault();
                if (objPOC != null)
                {
                    var monitoringBookID = item.MonitoringBookID;
                    var resultPhy = lstSpinDB.Where(x => x.MonitoringBookID == monitoringBookID).FirstOrDefault();
                    if (resultPhy != null)
                        SpineTestBusiness.Delete(resultPhy.SpineTestID);
                }
            }
            base.Save();
        }

        public void DeleteDental(IDictionary<string, object> dic)
        {
            int ClassID = Utils.GetInt(dic, "ClassID");
            List<int> lstPupilID = Utils.GetIntList(dic, "lstPupilID");

            List<MonitoringBook> lstMonitoringBookDB = MonitoringBookBusiness.Search(dic).ToList();
            List<int> lstMoniteringBookID = lstMonitoringBookDB.Select(p => p.MonitoringBookID).Distinct().ToList();
            var lstDentalDB = DentalTestBusiness.All.Where(x => lstMoniteringBookID.Contains(x.MonitoringBookID)).ToList();
            List<PupilOfClassBO> lstPOC = PupilOfClassBusiness.GetPupilInClass(ClassID)
                                                                .Where(p => lstPupilID.Contains(p.PupilID))
                                                                .Distinct().ToList();
            foreach (var item in lstMonitoringBookDB)
            {
                var objPOC = lstPOC.Where(p => p.PupilID == item.PupilID).FirstOrDefault();
                if (objPOC != null)
                {
                    var monitoringBookID = item.MonitoringBookID;
                    var resultPhy = lstDentalDB.Where(x => x.MonitoringBookID == monitoringBookID).FirstOrDefault();
                    if (resultPhy != null)
                        DentalTestBusiness.Delete(resultPhy.DentalTestID);
                }
            }
            base.Save();
        }

        public void DeleteOverAll(IDictionary<string, object> dic)
        {
            int ClassID = Utils.GetInt(dic, "ClassID");
            List<int> lstPupilID = Utils.GetIntList(dic, "lstPupilID");

            List<MonitoringBook> lstMonitoringBookDB = MonitoringBookBusiness.Search(dic).ToList();
            List<int> lstMoniteringBookID = lstMonitoringBookDB.Select(p => p.MonitoringBookID).Distinct().ToList();
            var lstOverAllDB = OverallTestBusiness.All.Where(x => lstMoniteringBookID.Contains(x.MonitoringBookID)).ToList();
            List<PupilOfClassBO> lstPOC = PupilOfClassBusiness.GetPupilInClass(ClassID)
                                                                .Where(p => lstPupilID.Contains(p.PupilID))
                                                                .Distinct().ToList();
            var lstPhysicalDB = PhysicalTestBusiness.All.Where(x => lstMoniteringBookID.Contains(x.MonitoringBookID)).ToList();

            foreach (var item in lstMonitoringBookDB)
            {
                var objPOC = lstPOC.Where(p => p.PupilID == item.PupilID).FirstOrDefault();
                if (objPOC != null)
                {
                    var resultMoni = lstMonitoringBookDB.Where(x => x.MonitoringBookID == item.MonitoringBookID).FirstOrDefault();
                    if (resultMoni != null)
                    {
                        resultMoni.MonitoringDate = null;
                        resultMoni.UnitName = null;
                        MonitoringBookRepository.Update(resultMoni);
                    }

                    var resultPhy = lstOverAllDB.Where(x => x.MonitoringBookID == item.MonitoringBookID).FirstOrDefault();
                    if (resultPhy != null)
                        OverallTestBusiness.Delete(resultPhy.OverallTestID);

                    var objPhy = lstPhysicalDB.Where(x => x.MonitoringBookID == item.MonitoringBookID).FirstOrDefault();
                    if (objPhy != null)
                    {
                        objPhy.Nutrition = 0;
                        PhysicalTestBusiness.Update(objPhy);
                    }
                }
            }
            base.Save();
        }

        public void DeleteEye(IDictionary<string, object> dic)
        {
            int ClassID = Utils.GetInt(dic, "ClassID");
            List<int> lstPupilID = Utils.GetIntList(dic, "lstPupilID");

            List<MonitoringBook> lstMonitoringBookDB = MonitoringBookBusiness.Search(dic).ToList();
            List<int> lstMoniteringBookID = lstMonitoringBookDB.Select(p => p.MonitoringBookID).Distinct().ToList();
            var lstEyeDB = EyeTestBusiness.All.Where(x => lstMoniteringBookID.Contains(x.MonitoringBookID)).ToList();
            List<PupilOfClassBO> lstPOC = PupilOfClassBusiness.GetPupilInClass(ClassID)
                                                                .Where(p => lstPupilID.Contains(p.PupilID))
                                                                .Distinct().ToList();
            foreach (var item in lstMonitoringBookDB)
            {
                var objPOC = lstPOC.Where(p => p.PupilID == item.PupilID).FirstOrDefault();
                if (objPOC != null)
                {
                    var monitoringBookID = item.MonitoringBookID;
                    //this.Delete(monitoringBookID);
                    var resultEye = lstEyeDB.Where(x => x.MonitoringBookID == monitoringBookID).FirstOrDefault();
                    if (resultEye != null)
                        EyeTestBusiness.Delete(resultEye.EyeTestID);
                }
            }
            base.Save();
        }

        public void DeleteEnt(IDictionary<string, object> dic)
        {
            int ClassID = Utils.GetInt(dic, "ClassID");
            List<int> lstPupilID = Utils.GetIntList(dic, "lstPupilID");

            List<MonitoringBook> lstMonitoringBookDB = MonitoringBookBusiness.Search(dic).ToList();
            List<int> lstMoniteringBookID = lstMonitoringBookDB.Select(p => p.MonitoringBookID).Distinct().ToList();
            var lstEntDB = ENTTestBusiness.All.Where(x => lstMoniteringBookID.Contains(x.MonitoringBookID)).ToList();
            List<PupilOfClassBO> lstPOC = PupilOfClassBusiness.GetPupilInClass(ClassID)
                                                                .Where(p => lstPupilID.Contains(p.PupilID))
                                                                .Distinct().ToList();
            foreach (var item in lstMonitoringBookDB)
            {
                var objPOC = lstPOC.Where(p => p.PupilID == item.PupilID).FirstOrDefault();
                if (objPOC != null)
                {
                    var monitoringBookID = item.MonitoringBookID;
                    //this.Delete(monitoringBookID);
                    var resultEnt = lstEntDB.Where(x => x.MonitoringBookID == monitoringBookID).FirstOrDefault();
                    if (resultEnt != null)
                        ENTTestBusiness.Delete(resultEnt.ENTTestID);
                }
            }
            base.Save();
        }
        #endregion
        #endregion
    }
}
