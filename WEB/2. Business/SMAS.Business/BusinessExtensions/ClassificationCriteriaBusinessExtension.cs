﻿/**
 * The Viettel License
 *
 * Copyright 2012 Viettel Telecom. All rights reserved.
 * VIETTEL PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */

/** 
* @author  
* @version $Revision: $
*/

using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Linq.Expressions;
using System.Web;
using log4net;

using SMAS.DAL.Repository;
using SMAS.DAL.IRepository;
using SMAS.Business.IBusiness;
using SMAS.Models.Models;
using SMAS.Business.Common;
using System.Data.Objects;
using System.Threading.Tasks;

namespace SMAS.Business.Business
{
    public partial class ClassificationCriteriaBusiness
    {

        #region Insert
        private void Validation(ClassificationCriteria ClassificationCriteria)
        {
            //TypeConfigID: FK(TypeConfig)
            TypeConfigBusiness.CheckAvailable(ClassificationCriteria.TypeConfigID, "ClassificationCriteria_Label_TypeConfig", true);
            //TypeConfigID, EffectDate, Genre: Duplicate
            bool Classificationduplicate = new ClassificationCriteriaRepository(this.context).ExistsRow(GlobalConstants.LIST_SCHEMA, "ClassificationCriteria",
                new Dictionary<string, object>()
                {
                    {"TypeConfigID",ClassificationCriteria.TypeConfigID},
                    {"EffectDate",ClassificationCriteria.EffectDate},
                    {"Genre",ClassificationCriteria.Genre}
                },
                  new Dictionary<string, object>()
                 {
                    {"ClassificationCriteriaID",ClassificationCriteria.ClassificationCriteriaID},                    
                });

            if (Classificationduplicate)
            {
                string typeConfigName = TypeConfigBusiness.Find(ClassificationCriteria.TypeConfigID).TypeConfigName;
                string genreText= ClassificationCriteria.Genre ? "Common_Label_Boys" : "Common_Label_Girls";
                throw new BusinessException("ClassificationCriteria_Label_NotDuplicate", typeConfigName, genreText, ClassificationCriteria.EffectDate.ToString("dd/MM/yyyy"));
            }
        }

        public void Insert(DateTime EffectDate, int GenreID)
        {
            #region Insert ClassificationCriteria
            Dictionary<string, object> Dic = new Dictionary<string, object>();
            List<TypeConfig> lstTypeConfig = TypeConfigBusiness.Search(Dic).ToList();
            List<ClassificationCriteria> lstClassification = new List<ClassificationCriteria>();
            TypeConfig itemType = null;
            ClassificationCriteria item = null;
            bool Genre = GenreID == 1 ? true : false;
            for (int i = 0, size = lstTypeConfig.Count; i < size; i++)
            {
                itemType = lstTypeConfig[i];
                item = new ClassificationCriteria();
                item.EffectDate = EffectDate;
                item.Genre = Genre;
                item.TypeConfigID = itemType.TypeConfigID;
                item.CreateDate = DateTime.Now;
                Validation(item);
                lstClassification.Add(item);
                base.Insert(item);
            }
            base.Save();
            #endregion

            #region Insert ClassificationCriteriaDetail
            Task[] arrTask = new Task[lstClassification.Count];
            for (int k = lstClassification.Count - 1; k >= 0; k--)
            {
                ClassificationCriteria itemC = lstClassification[k];
                TypeConfig objTypeConfig = lstTypeConfig[k];
                arrTask[k] = Task.Factory.StartNew(() =>
                {
                    SMASEntities contextTask = new SMASEntities();
                    contextTask.Configuration.AutoDetectChangesEnabled = false;
                    contextTask.Configuration.ValidateOnSaveEnabled = false;
                    int classificationCriteriaID = itemC.ClassificationCriteriaID;
                    List<ClassificationCriteria> lst = contextTask.ClassificationCriteria.AsNoTracking().Where(p=>p.Genre == Genre && p.TypeConfigID == objTypeConfig.TypeConfigID).OrderBy(p=>p.ClassificationCriteriaID).ToList();
                    int ClassificationID = (lst != null && lst.Count > 0) ? lst.Select(p => p.ClassificationCriteriaID).FirstOrDefault() : 0;
                    List<ClassificationCriteriaDetail> lstClassificationCriteriaDetail = contextTask.ClassificationCriteriaDetail.AsNoTracking().Where(o => o.ClassificationCriteriaID == ClassificationID).ToList();
                    if (lst.Count > 0 && lstClassificationCriteriaDetail.Count > 0)
                    {
                        // split task
                        int numTask = 10;
                        Task[] taskInsert = new Task[numTask];
                        for (int zt = 0; zt < numTask; zt++)
                        {
                            List<ClassificationCriteriaDetail> lstClassificationInsert = lstClassificationCriteriaDetail.Where(p => p.ClassificationCriteriaDetailID % numTask == zt).ToList();
                            taskInsert[zt] = Task.Factory.StartNew(() =>
                            {
                                SMASEntities contextInsert = new SMASEntities();
                                ClassificationCriteriaDetail itemDetail = null;
                                ClassificationCriteriaDetail itemDetailInsert = null;
                                contextInsert.Configuration.AutoDetectChangesEnabled = false;
                                contextInsert.Configuration.ValidateOnSaveEnabled = false;
                                for (int i = lstClassificationInsert.Count - 1; i >= 0; i--)
                                {
                                    itemDetail = lstClassificationInsert[i];
                                    itemDetailInsert = new ClassificationCriteriaDetail();
                                    itemDetailInsert.ClassificationLevelHealthID = itemDetail.ClassificationLevelHealthID;
                                    itemDetailInsert.Month = itemDetail.Month;
                                    itemDetailInsert.IndexValue = itemDetail.IndexValue;
                                    itemDetailInsert.ClassificationCriteriaID = classificationCriteriaID;
                                    contextInsert.ClassificationCriteriaDetail.Add(itemDetailInsert);
                                }

                                contextInsert.Configuration.AutoDetectChangesEnabled = true;
                                contextInsert.Configuration.ValidateOnSaveEnabled = true;
                                contextInsert.SaveChanges();
                            });
                        }

                        // wait all
                        Task.WaitAll(taskInsert);
                    }
                    else
                    {
                        List<ClassificationLevelHealth> lstLevelHealth = contextTask.ClassificationLevelHealth.AsNoTracking().Where(p => p.IsActive == true).ToList();
                        var clh = lstLevelHealth.Where(o => o.TypeConfigID == itemC.TypeConfigID).ToList();
                        //split task
                        Task[] taskInsert = new Task[clh.Count];
                        for (int ik = clh.Count - 1; ik >= 0; ik--)
                        {
                            ClassificationLevelHealth itemclh = clh[ik];
                            if (itemC.TypeConfig.TypeConfigName.ToUpper().Equals("CÂN NẶNG") || itemC.TypeConfig.TypeConfigName.ToUpper().Equals("CHIỀU CAO"))
                            {
                                taskInsert[ik] = Task.Factory.StartNew(() =>
                                {
                                    SMASEntities contextInsert = new SMASEntities();
                                    contextInsert.Configuration.ValidateOnSaveEnabled = false;
                                    contextInsert.Configuration.AutoDetectChangesEnabled = false;
                                    ClassificationCriteriaDetail ccd = null;
                                    for (int i = 0; i <= 60; i++)
                                    {
                                        ccd = new ClassificationCriteriaDetail();
                                        ccd.ClassificationLevelHealthID = itemclh.ClassificationLevelHealthID;
                                        ccd.Month = i;
                                        ccd.ClassificationCriteriaID = classificationCriteriaID;
                                        contextInsert.ClassificationCriteriaDetail.Add(ccd);
                                    }

                                    contextInsert.Configuration.ValidateOnSaveEnabled = true;
                                    contextInsert.Configuration.AutoDetectChangesEnabled = true;
                                    contextInsert.SaveChanges();
                                });
                            }

                            if (itemC.TypeConfig.TypeConfigName.ToUpper().Equals("BMI"))
                            {
                                taskInsert[ik] = Task.Factory.StartNew(() =>
                                {
                                    SMASEntities contextInsert = new SMASEntities();
                                    contextInsert.Configuration.ValidateOnSaveEnabled = false;
                                    contextInsert.Configuration.AutoDetectChangesEnabled = false;
                                    ClassificationCriteriaDetail ccd = null;
                                    for (int i = 61; i <= 228; i++)
                                    {
                                        ccd = new ClassificationCriteriaDetail();
                                        ccd.ClassificationLevelHealthID = itemclh.ClassificationLevelHealthID;
                                        ccd.Month = i;
                                        ccd.ClassificationCriteriaID = classificationCriteriaID;
                                        contextInsert.ClassificationCriteriaDetail.Add(ccd);
                                    }

                                    contextInsert.Configuration.ValidateOnSaveEnabled = true;
                                    contextInsert.Configuration.AutoDetectChangesEnabled = true;
                                    contextInsert.SaveChanges();
                                });
                            }
                        }

                        // wait all
                        Task.WaitAll(taskInsert);
                    }
                });
            }

            // wait all
            Task.WaitAll(arrTask);
            #endregion
        }
        #endregion
        #region Delete

        public void Delete(int ClassificationCriteriaID)
        {

            List<ClassificationCriteriaDetail> lstClassificationCriteriaDetail = ClassificationCriteriaDetailBusiness.All.Where(o => o.ClassificationCriteriaID == ClassificationCriteriaID).ToList();
            if (lstClassificationCriteriaDetail.Count() > 0)
            {
                //foreach (var item in lstClassificationCriteriaDetail)
                //{
                //    ClassificationCriteriaDetailBusiness.Delete(item.ClassificationCriteriaDetailID);
                //}
                ClassificationCriteriaDetailBusiness.DeleteAll(lstClassificationCriteriaDetail);
            }


            base.Delete(ClassificationCriteriaID);
        }


        #endregion
        #region Search
        public IQueryable<ClassificationCriteria> Search(IDictionary<string, object> SearchInfo)
        {
            int ClassificationCriteriaID = Utils.GetInt(SearchInfo, "ClassificationCriteriaID");
            //bool Genre = Utils.GetBool(SearchInfo, "Genre");
            int GenreID = Utils.GetInt(SearchInfo, "GenreID");
            int TypeConfigID = Utils.GetInt(SearchInfo, "TypeConfigID");
            DateTime? EffectDate = Utils.GetDateTime(SearchInfo, "EffectDate");

            IQueryable<ClassificationCriteria> lst = ClassificationCriteriaRepository.All;

            if (ClassificationCriteriaID != 0)
            {
                lst = lst.Where(o => o.ClassificationCriteriaID == ClassificationCriteriaID);
            }
            if (GenreID >= 0)
            {
                lst = lst.Where(o => o.Genre == (GenreID == 1 ? true : false));
            }
            
            if (TypeConfigID != 0)
            {
                lst = lst.Where(o => o.TypeConfigID == TypeConfigID);
            }

            if (EffectDate != null)
            {
                lst = lst.Where(o => EntityFunctions.TruncateTime(o.EffectDate) == EntityFunctions.TruncateTime(EffectDate));
            }

            return lst;
        }

        #endregion
    }
}