/**
 * The Viettel License
 *
 * Copyright 2012 Viettel Telecom. All rights reserved.
 * VIETTEL PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */

/** 
* @author  
* @version $Revision: $
*/

using System;
using log4net;
using System.Web;
using System.Data;
using System.Linq;
using System.Data.Entity;
using SMAS.Models.Models;
using SMAS.DAL.Repository;
using SMAS.DAL.IRepository;
using SMAS.Business.Common;
using System.Linq.Expressions;
using SMAS.Business.IBusiness;
using System.Collections.Generic;
using SMAS.Business.BusinessObject;
using SMAS.VTUtils.Log;

namespace SMAS.Business.Business
{
    public partial class ExamSupervisoryBusiness
    {
        public IQueryable<ExamSupervisory> Search(IDictionary<string, object> search)
        {
            long examinationsID = Utils.GetLong(search, "ExaminationsID");
            int academicYearID = Utils.GetInt(search, "AcademicYearID");
            int teacherID = Utils.GetInt(search, "TeacherID");
            long examSupervisoryID = Utils.GetLong(search, "ExamSupervisoryID");
            int employmentStatus = Utils.GetInt(search, "EmploymentStatus");

            IQueryable<ExamSupervisory> query = this.All;
            if (examinationsID != 0)
            {
                query = query.Where(o => o.ExaminationsID == examinationsID);
            }
            if (academicYearID != 0)
            {
                query = query.Where(o => o.AcademicYearID == academicYearID);
            }
            if (teacherID != 0)
            {
                query = query.Where(o => o.TeacherID == teacherID);
            }
            if (examSupervisoryID != 0)
            {
                query = query.Where(o => o.ExamSupervisoryID == examSupervisoryID);
            }
            // Lay nhung giam thi co trang thai dang lam viec
            if (employmentStatus != 0)
            {
                query = from es in query
                        join e in EmployeeRepository.All on es.TeacherID equals e.EmployeeID
                        where e.EmploymentStatus == employmentStatus
                        select es;
            }

            return query;
        }

        public IQueryable<ExamSupervisoryBO> GetExamSupervisoryOfExaminations(int academicYearID, int schoolID, long examinationsID)
        {
            IQueryable<ExamSupervisoryBO> query = from es in this.repository.All
                                                  join ee in this.EmployeeRepository.All.Where(o => o.SchoolID == schoolID) on es.TeacherID equals ee.EmployeeID
                                                  join sf in this.SchoolFacultyRepository.All.Where(o => o.SchoolID == schoolID) on ee.SchoolFacultyID equals sf.SchoolFacultyID
                                                  join en in this.EthnicRepository.All on ee.EthnicID equals en.EthnicID into des
                                                  from x in des.DefaultIfEmpty()
                                                  where es.AcademicYearID == academicYearID
                                                  && es.ExaminationsID == examinationsID
                                                  && ee.IsActive == true
                                                  select new ExamSupervisoryBO
                                                  {
                                                      ExaminationsID = es.AcademicYearID,
                                                      ExamSupervisoryID = es.ExamSupervisoryID,
                                                      TeacherID = es.TeacherID,
                                                      ExamGroupID = es.ExamGroupID,
                                                      TeacherName = ee.FullName,
                                                      EmployeeCode = ee.EmployeeCode,
                                                      SchoolFacultyName = sf.FacultyName,
                                                      EthnicCode = x != null ? x.EthnicCode : null,
                                                  };
            return query;
        }

        public IQueryable<ExamSupervisoryBO> GetExamSupervisoryOfExaminationsCustom(int academicYearID, int schoolID, long examinationsID, long examGroup)
        {
            IQueryable<ExamSupervisoryBO> query = from es in this.repository.All
                                                  join ee in this.EmployeeRepository.All.Where(o => o.SchoolID == schoolID) on es.TeacherID equals ee.EmployeeID
                                                  join sf in this.SchoolFacultyRepository.All.Where(o => o.SchoolID == schoolID) on ee.SchoolFacultyID equals sf.SchoolFacultyID
                                                  join en in this.EthnicRepository.All on ee.EthnicID equals en.EthnicID into des
                                                  from x in des.DefaultIfEmpty()
                                                  where es.AcademicYearID == academicYearID
                                                  && es.ExaminationsID == examinationsID
                                                  && ee.IsActive == true
                                                  && (es.ExamGroupID == examGroup || es.ExamGroupID == 0 )
                                                  select new ExamSupervisoryBO
                                                  {
                                                      ExaminationsID = es.AcademicYearID,
                                                      ExamSupervisoryID = es.ExamSupervisoryID,
                                                      TeacherID = es.TeacherID,
                                                      ExamGroupID = es.ExamGroupID,
                                                      TeacherName = ee.FullName,
                                                      EmployeeCode = ee.EmployeeCode,
                                                      SchoolFacultyName = sf.FacultyName,
                                                      EthnicCode = x != null ? x.EthnicCode : null,
                                                  };
            return query;
        }

        public List<ExamSupervisoryBO> GetListExamSupervisory(IDictionary<string, object> search)
        {
            long examinationsID = Utils.GetLong(search, "ExaminationsID");
            string employeeFullName = Utils.GetString(search, "EmployeeFullName").ToLower();
            int academicYearID = Utils.GetInt(search, "AcademicYearID");

            IQueryable<ExamSupervisoryBO> query = (from es in ExamSupervisoryRepository.All
                                                   join e in EmployeeRepository.All on es.TeacherID equals e.EmployeeID
                                                   join sf in SchoolFacultyRepository.All on e.SchoolFacultyID equals sf.SchoolFacultyID
                                                   where es.AcademicYearID == academicYearID && e.IsActive == true
                                                   select new ExamSupervisoryBO
                                                   {
                                                       ExamSupervisoryID = es.ExamSupervisoryID,
                                                       ExaminationsID = es.ExaminationsID,
                                                       ExamGroupID = es.ExamGroupID,
                                                       AcademicYearID = es.AcademicYearID,
                                                       TeacherID = es.TeacherID,
                                                       TeacherName = e.Name,
                                                       CreateTime = es.CreateTime,
                                                       UpdateTime = es.UpdateTime,
                                                       EmployeeCode = e.EmployeeCode,
                                                       FullName = e.FullName,
                                                       Birthday = e.BirthDate,
                                                       Genre = e.Genre,
                                                       FacultyID = sf.SchoolFacultyID,
                                                       FacultyName = sf.FacultyName,
                                                       Telephone = e.Mobile

                                                   }).Distinct();
            if (examinationsID != 0)
            {
                query = query.Where(o => o.ExaminationsID == examinationsID);
            }
            if (employeeFullName != string.Empty)
            {
                query = query.Where(o => o.FullName.ToLower().Contains(employeeFullName));
            }

            List<ExamSupervisoryBO> listResult = query.ToList();
            ExamSupervisoryBO temp = null;
            ExamSupervisoryAssignment checkAssign = null;
            ExamSupervisoryViolate checkViolate = null;
            for (int i = 0; i < listResult.Count; i++)
            {
                temp = listResult[i];
                search["ExamSupervisoryID"] = temp.ExamSupervisoryID;
                checkAssign = ExamSupervisoryAssignmentBusiness.Search(search).FirstOrDefault();
                checkViolate = ExamSupervisoryViolateBusiness.SearchViolate(search).FirstOrDefault();
                if (checkAssign == null && checkViolate == null)
                {
                    listResult[i].IsDelete = true;
                }
                else
                {
                    listResult[i].IsDelete = false;
                }
            }

            return listResult;
        }

        public List<ExamSupervisoryBO> GetListExamSupervisoryDev(IDictionary<string, object> search)
        {
            long examinationsID = Utils.GetLong(search, "ExaminationsID");
            string employeeFullName = Utils.GetString(search, "EmployeeFullName").ToLower();
            int academicYearID = Utils.GetInt(search, "AcademicYearID");
            int examGroupID = Utils.GetLong(search, "ExamGroupID");

            IQueryable<ExamSupervisoryBO> query = (from es in ExamSupervisoryRepository.All
                                                   join e in EmployeeRepository.All on es.TeacherID equals e.EmployeeID
                                                   join sf in SchoolFacultyRepository.All on e.SchoolFacultyID equals sf.SchoolFacultyID
                                                   where es.AcademicYearID == academicYearID && e.IsActive == true
                                                   select new ExamSupervisoryBO
                                                   {
                                                       ExamSupervisoryID = es.ExamSupervisoryID,
                                                       ExaminationsID = es.ExaminationsID,
                                                       ExamGroupID = es.ExamGroupID,
                                                       AcademicYearID = es.AcademicYearID,
                                                       TeacherID = es.TeacherID,
                                                       TeacherName = e.Name,
                                                       CreateTime = es.CreateTime,
                                                       UpdateTime = es.UpdateTime,
                                                       EmployeeCode = e.EmployeeCode,
                                                       FullName = e.FullName,
                                                       Birthday = e.BirthDate,
                                                       Genre = e.Genre,
                                                       FacultyID = sf.SchoolFacultyID,
                                                       FacultyName = sf.FacultyName,
                                                       Telephone = e.Mobile

                                                   }).Distinct();
            if (examinationsID != 0)
            {
                query = query.Where(o => o.ExaminationsID == examinationsID);
            }
            if (examGroupID != 0)
            {
                query = query.Where(o => o.ExamGroupID == examGroupID);
            }
            if (employeeFullName != string.Empty)
            {
                query = query.Where(o => o.FullName.ToLower().Contains(employeeFullName));
            }

            List<ExamSupervisoryBO> listResult = query.ToList();
            ExamSupervisoryBO temp = null;
            List<ExamSupervisoryAssignment> lstcheckAssign = ExamSupervisoryAssignmentBusiness.Search(search).ToList();
            ExamSupervisoryAssignment checkAssign = null;
            List<ExamSupervisoryViolate> lstcheckViolate = ExamSupervisoryViolateBusiness.SearchViolate(search).ToList();
            ExamSupervisoryViolate checkViolate = null;
            List<ExamGroup> lstExamGroup = ExamGroupBusiness.Search(search).ToList();
            ExamGroup objExamGroup = null;
            for (int i = 0; i < listResult.Count; i++)
            {
                temp = listResult[i];
                //search["ExamSupervisoryID"] = temp.ExamSupervisoryID;
                checkAssign = lstcheckAssign.Where(x => x.ExamSupervisoryID == temp.ExamSupervisoryID).FirstOrDefault();
                checkViolate = lstcheckViolate.Where(x => x.ExamSupervisoryID == temp.ExamSupervisoryID).FirstOrDefault();
                objExamGroup = lstExamGroup.Where(x => x.ExamGroupID == temp.ExamGroupID).FirstOrDefault();

                if (objExamGroup != null)
                    temp.ExamGroupName = objExamGroup.ExamGroupName;

                if (checkAssign == null && checkViolate == null)
                {
                    listResult[i].IsDelete = true;
                }
                else
                {
                    listResult[i].IsDelete = false;
                }
            }

            return listResult;
        }

        public List<ExamSupervisoryBO> GetListEmployee(IDictionary<string, object> search)
        {
            int schoolID = Utils.GetInt(search, "SchoolID");
            string searchInfoText = Utils.GetString(search, "SearchInfoText").ToLower();
            int academicYearID = Utils.GetInt(search, "AcademicYearID");
            long examinationsID = Utils.GetLong(search, "ExaminationsID");
            int status = Utils.GetInt(search, "Status");
            int appliLevelID = Utils.GetInt(search, "AppliLevelID");

            List<ExamSupervisoryBO> query = (from e in EmployeeBusiness.All
                                             join sf in SchoolFacultyBusiness.All on e.SchoolFacultyID equals sf.SchoolFacultyID
                                             where e.SchoolID == schoolID && e.IsActive == true && e.EmploymentStatus == GlobalConstants.EMPLOYMENT_STATUS_WORKING
                                             && ((e.EmployeeCode.ToLower().Contains(searchInfoText)) || (e.FullName.ToLower().Contains(searchInfoText)) || (sf.FacultyName.ToLower().Contains(searchInfoText)))
                                             select new ExamSupervisoryBO
                                             {
                                                 TeacherID = e.EmployeeID,                                                 
                                                 EmployeeCode = e.EmployeeCode,
                                                 TeacherName = e.Name,
                                                 FullName = e.FullName,
                                                 Birthday = e.BirthDate,
                                                 Genre = e.Genre,
                                                 Telephone = e.Mobile,
                                                 FacultyID = sf.SchoolFacultyID,
                                                 FacultyName = sf.FacultyName,
                                                 SchoolID = e.SchoolID,
                                                 AppliedLevel = e.AppliedLevel
                                             }).Distinct().ToList();


            List<int> list = null;
            if (examinationsID != 0)
            {
                list = (from es in ExamSupervisoryBusiness.All
                        where es.AcademicYearID == academicYearID && es.ExaminationsID == examinationsID
                        select es.TeacherID).ToList();
            }
            else
            {
                list = (from es in ExamSupervisoryBusiness.All
                        where es.AcademicYearID == academicYearID
                        select es.TeacherID).ToList();
            }


            if (status == 0)
            {
                query = query.ToList();
            }
            else if (status == 1)
            {
                query = query.Where(o => list.Contains(o.TeacherID)).ToList();
            }
            else
            {
                query = query.Where(o => !list.Contains(o.TeacherID)).ToList();
            }
            List<ExamSupervisoryBO> listResult = query.ToList();           
            if (appliLevelID != 0)
            {
                ExamSupervisoryBO temp = null;
                ExamSupervisory checkAssign = null;
                List<ExamSupervisory> lstSup = (from e in ExaminationsBusiness.All.Where(x => x.AcademicYearID == academicYearID
                                                                                                && x.SchoolID == schoolID && x.AppliedLevel == appliLevelID && x.ExaminationsID == examinationsID)
                                                join esa in ExamSupervisoryAssignmentBusiness.All.Where(x => x.AcademicYearID == academicYearID && x.ExaminationsID == examinationsID) on e.ExaminationsID equals esa.ExaminationsID
                                                join es in ExamSupervisoryBusiness.All.Where(x => x.AcademicYearID == academicYearID && x.ExaminationsID == examinationsID) on esa.ExamSupervisoryID equals es.ExamSupervisoryID
                                                     select es).ToList();

                for (int i = 0; i < listResult.Count; i++)
                {
                    temp = listResult[i];
                    checkAssign = lstSup.Where(x => x.TeacherID == temp.TeacherID).FirstOrDefault();
                    //objExamGroup = lstExamGroup.Where(x => x.ExamGroupID == temp.ExamGroupID).FirstOrDefault();
                    //if (objExamGroup != null)
                    //    temp.ExamGroupName = objExamGroup.ExamGroupName;

                    if (checkAssign == null)
                    {
                        listResult[i].IsDelete = false;
                    }
                    else
                    {
                        listResult[i].IsDelete = true;
                    }
                }                 
            }
            
            return listResult;
        }

        public void InsertListExamSupervisory(List<ExamSupervisory> listExamSupervisory)
        {
            try
            {
                if (listExamSupervisory == null || listExamSupervisory.Count == 0)
                {
                    return;
                }
                context.Configuration.AutoDetectChangesEnabled = false;
                context.Configuration.ValidateOnSaveEnabled = false;
                for (int i = 0; i < listExamSupervisory.Count; i++)
                {
                    ExamSupervisoryBusiness.Insert(listExamSupervisory[i]);
                }
                ExamSupervisoryBusiness.Save();
            }
            catch (Exception ex)
            {
                
                LogExtensions.ErrorExt(logger, DateTime.Now, "InsertListExamSupervisory", "null", ex);
            }
            finally
            {
                context.Configuration.AutoDetectChangesEnabled = true;
                context.Configuration.ValidateOnSaveEnabled = true;
            }
        }

        public void UpdateListExamSupervisory(List<ExamSupervisory> listExamSupervisory)
        {
            try
            {
                if (listExamSupervisory == null || listExamSupervisory.Count == 0)
                {
                    return;
                }
                context.Configuration.AutoDetectChangesEnabled = false;
                context.Configuration.ValidateOnSaveEnabled = false;
                for (int i = 0; i < listExamSupervisory.Count; i++)
                {
                    ExamSupervisoryBusiness.Update(listExamSupervisory[i]);
                }
                ExamSupervisoryBusiness.Save();
            }
            catch (Exception ex)
            {
                
                LogExtensions.ErrorExt(logger, DateTime.Now, "UpdateListExamSupervisory", "null", ex);
            }
            finally
            {
                context.Configuration.AutoDetectChangesEnabled = true;
                context.Configuration.ValidateOnSaveEnabled = true;
            }
        }

        public void DeleteExamSupervisory(List<long> listExamSupervisoryID)
        {
            try
            {
                context.Configuration.AutoDetectChangesEnabled = false;
                context.Configuration.ValidateOnSaveEnabled = false;
                if (listExamSupervisoryID == null)
                {
                    return;
                }
                List<ExamSupervisory> lstDelete = ExamSupervisoryBusiness.All.Where(e => listExamSupervisoryID.Contains(e.ExamSupervisoryID)).ToList();
                if (lstDelete == null || lstDelete.Count == 0)
                {
                    return;
                }
                /*for (int i = 0; i < listExamSupervisoryID.Count; i++)
                {
                    this.Delete(listExamSupervisoryID[i]);
                }*/
                ExamSupervisoryBusiness.DeleteAll(lstDelete);
                ExamSupervisoryBusiness.Save();
            }
            catch (Exception ex)
            {
                
                LogExtensions.ErrorExt(logger, DateTime.Now, "DeleteExamSupervisory", "null", ex);
            }
            finally
            {
                context.Configuration.AutoDetectChangesEnabled = true;
                context.Configuration.ValidateOnSaveEnabled = true;
            }
        }

    }
}