﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SMAS.Models.Models;

namespace SMAS.Business.Business
{
    public partial class MonthCommentsBusiness
    {
        public List<MonthComments> getMonthCommentsList()
        {
            List<MonthComments> list = this.MonthCommentsRepository.All.OrderBy(p=>p.MonthID).ToList();
            return list;
        }

    }
}
