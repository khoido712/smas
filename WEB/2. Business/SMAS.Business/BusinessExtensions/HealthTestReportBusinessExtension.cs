using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SMAS.Business.Common;
using SMAS.Business.BusinessObject;
using SMAS.Models.Models;
using System.IO;
using SMAS.VTUtils.Excel.Export;

namespace SMAS.Business.Business
{
    public partial class HealthTestReportBusiness
    {
        #region GetHashKeyForSchool
        /// <summary>
        /// Lấy mảng băm cho các tham số đầu vào của thống kê báo cáo kết quả khám sức khoẻ cấp trường
        /// </summary>
        /// <param name="entity"></param>
        /// <returns>tungnd</returns>
        public string GetHashKeyForSchool(ExcelHealthTestReportBO entity, int ReportType)
        {
            IDictionary<string, object> dic = new Dictionary<string, object>();
            if (ReportType == 1)
            {
                dic["ReportCode"] = SystemParamsInFile.REPORT_KETQUA_KHAM_SUCKHOE_TRUONG;
            }
            if (ReportType == 2)
            {
                dic["ReportCode"] = SystemParamsInFile.REPORT_KETQUA_KHAM_MAT;
            }

            dic["HeathPeriod"] = entity.HealthPeriod;
            dic["AcademicYearID"] = entity.AcademicYearID;
            dic["EducationLevelID"] = entity.EducationLevelID;
            dic["SchoolID"] = entity.SchoolID;
            return ReportUtils.GetHashKey(dic);
        }
        #endregion


        #region GetHealthTestReportForSchool
        /// <summary>
        /// Lấy báo cáo thống kê kết quả khám sức khoẻ được cập nhật mới nhất
        /// </summary>
        /// <param name="entity"></param>
        /// <returns>tungnd</returns>
        public ProcessedReport GetHealthTestReportForSchool(ExcelHealthTestReportBO entity, int ReportType)
        {
            string reportCode = "";
            if (ReportType == 1)
            {
                reportCode = SystemParamsInFile.REPORT_KETQUA_KHAM_SUCKHOE_TRUONG;
            }
            if (ReportType == 2)
            {
                reportCode = SystemParamsInFile.REPORT_KETQUA_KHAM_MAT;
            }

            string inputParameterHashKey = GetHashKeyForSchool(entity, ReportType);
            return ProcessedReportBusiness.GetProcessedReport(reportCode, inputParameterHashKey);
        }
        #endregion


        #region InsertHealthTestReportForSchool
        /// <summary>
        /// Lưu lại thông tin thống kê kết quả khám sức khoẻ
        /// </summary>
        /// <param name="entity"></param>
        /// <returns>tungnd</returns>
        public ProcessedReport InsertHealthTestReportForSchool(ExcelHealthTestReportBO entity, Stream data, int ReportType)
        {
            string reportCode = "";
            if (ReportType == 1)
            {
                reportCode = SystemParamsInFile.REPORT_KETQUA_KHAM_SUCKHOE_TRUONG;
            }
            if (ReportType == 2)
            {
                reportCode = SystemParamsInFile.REPORT_KETQUA_KHAM_MAT;
            }


            ReportDefinition reportDef = ReportDefinitionBusiness.GetByCode(reportCode);
            ProcessedReport pr = new ProcessedReport();
            pr.ReportCode = reportCode;
            pr.ProcessedDate = DateTime.Now;
            pr.InputParameterHashKey = GetHashKeyForSchool(entity, ReportType);
            pr.ReportData = ReportUtils.Compress(data);


            //Tạo tên file HS_[SchoolLevel]_KetQuaKhamMat ,HS_KetQuaKhamSucKhoeTruong_[School]
            string outputNamePattern = reportDef.OutputNamePattern;
            string schoolLevel = ReportUtils.ConvertAppliedLevelForReportName(entity.AppliedLevel);
            string SchoolName = this.SchoolProfileBusiness.Find(entity.SchoolID).SchoolName;

            if (ReportType == 2)//HS_[SchoolLevel]_KetQuaKhamMat
            {
                //outputNamePattern = outputNamePattern.Replace("[SchoolLevel]", ReportUtils.StripVNSign(schoolLevel));
                if (entity.AppliedLevel == 1)
                {
                    outputNamePattern = outputNamePattern.Replace("[SchoolLevel]", "TH");
                }
                else if (entity.AppliedLevel == 2)
                {
                    outputNamePattern = outputNamePattern.Replace("[SchoolLevel]", "THCS");
                }
                else if (entity.AppliedLevel == 3)
                {
                    outputNamePattern = outputNamePattern.Replace("[SchoolLevel]", "THPT");
                }
                else if (entity.AppliedLevel == 4)
                {
                    outputNamePattern = outputNamePattern.Replace("[SchoolLevel]", "NT");
                }
                else if (entity.AppliedLevel == 5)
                {
                    outputNamePattern = outputNamePattern.Replace("[SchoolLevel]", "MG");
                }
                if (entity.EducationLevelID == 0)
                {
                    outputNamePattern = outputNamePattern.Replace("KetQuaKhamMat", "KetQuaKhamMat_Dot_" + entity.HealthPeriod.ToString());
                }
                else
                {
                    if (entity.EducationLevelID >= 1 && entity.EducationLevelID <= 12)
                    {
                        outputNamePattern = outputNamePattern.Replace("KetQuaKhamMat", "KetQuaKhamMat_Khoi_" + entity.EducationLevelID + "_Dot_" + entity.HealthPeriod.ToString());
                    }
                    else
                    {
                        if (entity.EducationLevelID == 13)
                        {
                            outputNamePattern = outputNamePattern.Replace("KetQuaKhamMat", "KetQuaKhamMat_Khoi_3_12_thang_Dot_" + entity.HealthPeriod.ToString());
                        }
                        else if (entity.EducationLevelID == 14)
                        {
                            outputNamePattern = outputNamePattern.Replace("KetQuaKhamMat", "KetQuaKhamMat_Khoi_13_24_thang_Dot_" + entity.HealthPeriod.ToString());
                        }
                        else if (entity.EducationLevelID == 15)
                        {
                            outputNamePattern = outputNamePattern.Replace("KetQuaKhamMat", "KetQuaKhamMat_Khoi_25_36_thang_Dot_" + entity.HealthPeriod.ToString());
                        }
                        else if (entity.EducationLevelID == 16)
                        {
                            outputNamePattern = outputNamePattern.Replace("KetQuaKhamMat", "KetQuaKhamMat_Khoi_3_4_tuoi_Dot_" + entity.HealthPeriod.ToString());
                        }
                        else if (entity.EducationLevelID == 17)
                        {
                            outputNamePattern = outputNamePattern.Replace("KetQuaKhamMat", "KetQuaKhamMat_Khoi_4_5_tuoi_Dot_" + entity.HealthPeriod.ToString());
                        }
                        else if (entity.EducationLevelID == 18)
                        {
                            outputNamePattern = outputNamePattern.Replace("KetQuaKhamMat", "KetQuaKhamMat_Khoi_5_6_tuoi_Dot_" + entity.HealthPeriod.ToString());
                        }
                    }
                }
            }
            else if (ReportType == 1)//HS_KetQuaKhamSucKhoeTruong_[School]
            {
                //outputNamePattern = outputNamePattern.Replace("[School]", ReportUtils.StripVNSign(SchoolName));
                if (entity.AppliedLevel == 1)
                {
                    outputNamePattern = outputNamePattern.Replace("HS_", "HS_TH_");
                }
                else if (entity.AppliedLevel == 2)
                {
                    outputNamePattern = outputNamePattern.Replace("HS_", "HS_THCS_");
                }
                else if (entity.AppliedLevel == 3)
                {
                    outputNamePattern = outputNamePattern.Replace("HS_", "HS_THPT_");
                }
                else if (entity.AppliedLevel == 4)
                {
                    outputNamePattern = outputNamePattern.Replace("HS_", "HS_NT_");
                }
                else if (entity.AppliedLevel == 5)
                {
                    outputNamePattern = outputNamePattern.Replace("HS_", "HS_MG_");
                }
                if (entity.EducationLevelID == 0)
                {
                    outputNamePattern = outputNamePattern.Replace("Truong_[School]", "_Dot_" + entity.HealthPeriod.ToString());
                }
                else
                {
                    if (entity.EducationLevelID >= 1 && entity.EducationLevelID <= 12)
                    {
                        outputNamePattern = outputNamePattern.Replace("Truong_[School]", "_Khoi_" + entity.EducationLevelID + "_Dot_" + entity.HealthPeriod.ToString());
                    }
                    else
                    {
                        if (entity.EducationLevelID == 13)
                        {
                            outputNamePattern = outputNamePattern.Replace("Truong_[School]", "_3_12_thang_Dot_" + entity.HealthPeriod.ToString());
                        }
                        else if (entity.EducationLevelID == 14)
                        {
                            outputNamePattern = outputNamePattern.Replace("Truong_[School]", "_13_24_thang_Dot_" + entity.HealthPeriod.ToString());
                        }
                        else if (entity.EducationLevelID == 15)
                        {
                            outputNamePattern = outputNamePattern.Replace("Truong_[School]", "_25_36_thang_Dot_" + entity.HealthPeriod.ToString());
                        }
                        else if (entity.EducationLevelID == 16)
                        {
                            outputNamePattern = outputNamePattern.Replace("Truong_[School]", "_3_4_tuoi_Dot_" + entity.HealthPeriod.ToString());
                        }
                        else if (entity.EducationLevelID == 17)
                        {
                            outputNamePattern = outputNamePattern.Replace("Truong_[School]", "_4_5_tuoi_Dot_" + entity.HealthPeriod.ToString());
                        }
                        else if (entity.EducationLevelID == 18)
                        {
                            outputNamePattern = outputNamePattern.Replace("Truong_[School]", "_5_6_tuoi_Dot_" + entity.HealthPeriod.ToString());
                        }
                    }
                }
            }
            pr.ReportName = ReportUtils.RemoveSpecialCharacters(outputNamePattern) + "." + reportDef.OutputFormat;

            IDictionary<string, object> dic = new Dictionary<string, object> {
                {"SchoolID", entity.SchoolID},
                {"HealthPeriod", entity.HealthPeriod},
                {"EducationLevelID ", entity.EducationLevelID},
                {"AcademicYearID", entity.AcademicYearID}
            };
            ProcessedReportParameterRepository.Insert(dic, pr);
            ProcessedReportRepository.Insert(pr);
            ProcessedReportRepository.Save();
            return pr;
        }
        #endregion


        #region CreateHealthTestReportForSchool
        /// <summary>
        /// Tạo file excel thống kê kết quả khám sức khoẻ cấp trường
        /// </summary>
        /// <param name="entity"></param>
        /// <returns>tungnd</returns>
        public Stream CreateHealthTestReportForSchool(ExcelHealthTestReportBO entity, int ReportType)
        {
            string reportCode = "";
            string PeriodExam = "";

            if (ReportType == 1)
            {
                reportCode = SystemParamsInFile.REPORT_KETQUA_KHAM_SUCKHOE_TRUONG;
            }
            if (ReportType == 2)
            {
                reportCode = SystemParamsInFile.REPORT_KETQUA_KHAM_MAT;
            }
            if (entity.HealthPeriod == 1)
            {
                PeriodExam = "Đợt I";
            }
            else if (entity.HealthPeriod == 2)
            {
                PeriodExam = "Đợt II";
            }

            ReportDefinition reportDef = ReportDefinitionBusiness.GetByCode(reportCode);
            SchoolProfile school = SchoolProfileBusiness.Find(entity.SchoolID);
            string AcademicYear = this.AcademicYearBusiness.All.Where(o => o.AcademicYearID == entity.AcademicYearID).FirstOrDefault().DisplayTitle;
            string Suppervising = UtilsBusiness.GetSupervisingDeptName(school.SchoolProfileID, entity.AppliedLevel).ToUpper();
            string Province = (school.District != null ? school.District.DistrictName : "");
            string SchoolName = school.SchoolName;

            string templatePath = SystemParamsInFile.TEMPLATE_FOLDER + reportDef.TemplateDirectory + "/" + reportDef.TemplateName;
            IVTWorkbook oBook = VTExport.OpenWorkbook(templatePath);

            //Lấy sheet template
            IVTWorksheet firstSheet = oBook.GetSheet(1);

            Dictionary<string, object> dic = new Dictionary<string, object>();
            dic["EducationLevelID"] = entity.EducationLevelID;
            dic["AcademicYearID"] = entity.AcademicYearID;
            var lstClass = ClassProfileBusiness.SearchBySchool(entity.SchoolID, dic).ToList();

            List<EducationLevel> lstEducationLevel = new List<EducationLevel>();
            if (entity.EducationLevelID == 0)
            {
                lstEducationLevel = this.EducationLevelBusiness.All.Where(o => o.Grade == entity.AppliedLevel).ToList();
            }
            else
            {
                lstEducationLevel = this.EducationLevelBusiness.All.Where(o => o.EducationLevelID == entity.EducationLevelID).ToList();
            }
            #region get du lieu
            var lstEyeTest = from poc in this.PupilOfClassBusiness.All
                             join mb in MonitoringBookRepository.All on poc.PupilID equals mb.PupilID
                             join et in EyeTestBusiness.All on mb.MonitoringBookID equals et.MonitoringBookID
                             where  mb.SchoolID == entity.SchoolID
                                    && mb.AcademicYearID == entity.AcademicYearID
                                    && mb.HealthPeriodID == entity.HealthPeriod
                                    && poc.AcademicYearID == mb.AcademicYearID
                                    && (poc.Status == SystemParamsInFile.PUPIL_STATUS_STUDYING || poc.Status == SystemParamsInFile.PUPIL_STATUS_GRADUATED) 
                                    && et.IsActive == true
                             select new { poc, mb, et };

            var lstPupilK = from poc in this.PupilOfClassBusiness.All
                            join mb in MonitoringBookRepository.All on poc.PupilID equals mb.PupilID
                            where  mb.SchoolID == entity.SchoolID
                                   && mb.AcademicYearID == entity.AcademicYearID
                                   && mb.HealthPeriodID == entity.HealthPeriod
                                   && poc.AcademicYearID == mb.AcademicYearID
                                   && (poc.Status == SystemParamsInFile.PUPIL_STATUS_STUDYING || poc.Status == SystemParamsInFile.PUPIL_STATUS_GRADUATED) 
                            select new { poc, mb };


            var lstDentalTest = from poc in this.PupilOfClassBusiness.All
                                join mb in MonitoringBookRepository.All on poc.PupilID equals mb.PupilID
                                join dt in DentalTestBusiness.All on mb.MonitoringBookID equals dt.MonitoringBookID
                                where  mb.SchoolID == entity.SchoolID
                                       && mb.AcademicYearID == entity.AcademicYearID
                                       && mb.HealthPeriodID == entity.HealthPeriod
                                       && poc.AcademicYearID == mb.AcademicYearID
                                       && (poc.Status == SystemParamsInFile.PUPIL_STATUS_STUDYING || poc.Status == SystemParamsInFile.PUPIL_STATUS_GRADUATED) 
                                       && dt.IsActive == true
                                select new { poc, mb, dt };

            var lstENTTest = from poc in this.PupilOfClassBusiness.All
                             join mb in MonitoringBookRepository.All on poc.PupilID equals mb.PupilID
                             join ent in ENTTestBusiness.All on mb.MonitoringBookID equals ent.MonitoringBookID
                             where  mb.SchoolID == entity.SchoolID
                                    && mb.AcademicYearID == entity.AcademicYearID
                                    && mb.HealthPeriodID == entity.HealthPeriod
                                    && poc.AcademicYearID == mb.AcademicYearID
                                    && (poc.Status == SystemParamsInFile.PUPIL_STATUS_STUDYING || poc.Status == SystemParamsInFile.PUPIL_STATUS_GRADUATED)
                                    && ent.IsSick == true
                                    && ent.IsActive == true
                             select new { poc, mb, ent };


            var lstSpineTest = from poc in this.PupilOfClassBusiness.All
                               join mb in MonitoringBookRepository.All on poc.PupilID equals mb.PupilID
                               join st in SpineTestBusiness.All on mb.MonitoringBookID equals st.MonitoringBookID
                               where  mb.SchoolID == entity.SchoolID
                                      && mb.AcademicYearID == entity.AcademicYearID
                                      && mb.HealthPeriodID == entity.HealthPeriod
                                      && poc.AcademicYearID == mb.AcademicYearID
                                      && (poc.Status == SystemParamsInFile.PUPIL_STATUS_STUDYING || poc.Status == SystemParamsInFile.PUPIL_STATUS_GRADUATED) 
                                      && st.IsDeformityOfTheSpine == true
                                      && st.IsActive == true
                               select new { poc, mb, st };

            var lstOverallTest = from poc in this.PupilOfClassBusiness.All
                                 join mb in MonitoringBookRepository.All on poc.PupilID equals mb.PupilID
                                 join ot in OverallTestBusiness.All on mb.MonitoringBookID equals ot.MonitoringBookID
                                 where  mb.SchoolID == entity.SchoolID
                                        && mb.AcademicYearID == entity.AcademicYearID
                                        && mb.HealthPeriodID == entity.HealthPeriod 
                                        && (poc.Status == SystemParamsInFile.PUPIL_STATUS_STUDYING || poc.Status == SystemParamsInFile.PUPIL_STATUS_GRADUATED)
                                        && poc.AcademicYearID == mb.AcademicYearID
                                        && ot.IsActive == true
                                 select new { poc, mb, ot };


            var lstPhysicalTest = from poc in this.PupilOfClassBusiness.All
                                  join mb in MonitoringBookRepository.All on poc.PupilID equals mb.PupilID
                                  join pt in PhysicalTestBusiness.All on mb.MonitoringBookID equals pt.MonitoringBookID
                                  where mb.SchoolID == entity.SchoolID
                                        && mb.AcademicYearID == entity.AcademicYearID 
                                        && mb.HealthPeriodID == entity.HealthPeriod  
                                        && (poc.Status == SystemParamsInFile.PUPIL_STATUS_STUDYING || poc.Status == SystemParamsInFile.PUPIL_STATUS_GRADUATED)  
                                        && pt.IsActive == true
                                  select new { poc, mb, pt };
            #endregion
            #region ket qua kham suc khoe
            if (ReportType == 1)
            {
                //Tạo sheet
                IVTWorksheet sheetType1 = oBook.CopySheetToLast(firstSheet);
                IVTRange rangeType1 = firstSheet.GetRange("A14", "AG14");
                IVTRange rangeTotalType1 = firstSheet.GetRange("A15", "AG15");
                IVTRange rangeFirstType1 = firstSheet.GetRange("A13", "AG13");

                sheetType1.Name = "KETQUAKHAMSUCKHOE";
                sheetType1.SetCellValue("A2", Suppervising);
                sheetType1.SetCellValue("A3", SchoolName);
                sheetType1.SetCellValue("A8", "BÁO CÁO KẾT QUẢ KHÁM SỨC KHỎE - NĂM HỌC " + AcademicYear + " - " + PeriodExam.ToUpper());
                sheetType1.SetCellValue("U4", Province + ", ngày " + DateTime.Now.Day + " tháng " + DateTime.Now.Month + " năm " + DateTime.Now.Year);

                int startrow1 = 13;
                int startrowClass1 = 0;

                int SchoolsumPupil = 0;
                int SchoolsumPupilK = 0;
                int SchoolPupilEyeTest = 0;
                int SchoolPupilEyeK = 0;
                int SchoolPupilOrther = 0;
                int SchoolPupilDentaTest = 0;
                int SchoolPupilDentaTestOther = 0;
                int SchoolPupilTMH = 0;
                int SchoolPupilCS = 0;
                int SchoolPupilBP = 0;
                int SchoolPupilSDD = 0;
                int SchoolPupilDL = 0;
                int SchoolPupilNK = 0;
                int SchoolPupilNGK = 0;
                int SchoolPupilT = 0;
                int SchoolPupilDTBS = 0;
                int j = 0;
                List<ClassProfile> lstClassTemp = new List<ClassProfile>();
                foreach (var itemEdu in lstEducationLevel)
                {
                    lstClassTemp = lstClass.Where(o => o.EducationLevelID == itemEdu.EducationLevelID).ToList();
                    //lstClass = lstClass.Where(o => o.EducationLevelID == itemEdu.EducationLevelID).ToList();
                    sheetType1.CopyPasteSameRowHeigh(rangeFirstType1, startrow1);
                    sheetType1.SetCellValue(startrow1, 2, itemEdu.Resolution);
                    startrow1++;

                    startrowClass1 = startrow1;
                    foreach (var itemClass in lstClassTemp)
                    {
                        j++;
                        int classID = itemClass.ClassProfileID;
                        // Tong so hoc sinh dang hoc hoac da tot nghiep trong lop
                        int sumPupil = this.PupilOfClassBusiness.All.Where(o => o.SchoolID == entity.SchoolID && o.ClassID == classID 
                            && o.AcademicYearID == entity.AcademicYearID 
                            && (o.Status == SystemParamsInFile.PUPIL_STATUS_STUDYING || o.Status == SystemParamsInFile.PUPIL_STATUS_GRADUATED)).Count();
                        SchoolsumPupil += sumPupil; // Tinh tong so hoc sinh trong truong
                        int sumPupilK = lstPupilK.Where(o => o.poc.ClassID == classID).Count();
                        SchoolsumPupilK += sumPupilK;
                        // So hoc sinh bi can thi
                        int PupilEyeTest = lstEyeTest.Where(o => o.poc.ClassID == classID && o.et.IsMyopic == true).Count();
                        SchoolPupilEyeTest += PupilEyeTest;
                        // So hoc sinh bi tat khuc xa
                        int PupilEyeK = lstEyeTest.Where(o => o.poc.ClassID == classID && (o.et.IsAstigmatism == true || o.et.IsFarsightedness == true)).Count();
                        SchoolPupilEyeK += PupilEyeK;
                        // So hoc sinh bi benh mat khac
                        int PupilOrther = lstEyeTest.Where(o => o.poc.ClassID == classID && o.et.Other != null).Count();
                        SchoolPupilOrther += PupilOrther;
                        int PupilDentaTest = lstDentalTest.Where(o => o.poc.ClassID == classID && o.dt.NumDentalFilling > 0).Count();
                        SchoolPupilDentaTest += PupilDentaTest;
                        int PupilDentaTestOther = lstDentalTest.Where(o => o.poc.ClassID == classID && o.dt.Other != null).Count();
                        SchoolPupilDentaTestOther += PupilDentaTestOther;
                        int PupilTMH = lstENTTest.Where(o => o.poc.ClassID == classID).Count();
                        SchoolPupilTMH += PupilTMH;
                        int PupilCS = lstSpineTest.Where(o => o.poc.ClassID == classID).Count();
                        SchoolPupilCS += PupilCS;
                        int PupilDL = lstOverallTest.Where(o => o.poc.ClassID == classID && o.ot.SkinDiseases == 1).Count();
                        SchoolPupilDL += PupilDL;
                        int PupilNK = lstOverallTest.Where(o => o.poc.ClassID == classID && o.ot.OverallInternal  == 1).Count();
                        SchoolPupilNK += PupilNK;
                        int PupilNGK = lstOverallTest.Where(o => o.poc.ClassID == classID && o.ot.OverallForeign  == 1).Count();
                        SchoolPupilNGK += PupilNGK;
                        int PupilT = lstOverallTest.Where(o => o.poc.ClassID == classID && o.ot.IsHeartDiseases == true).Count();
                        SchoolPupilT += PupilT;
                        int PupilDTBS = lstOverallTest.Where(o => o.poc.ClassID == classID && o.ot.IsBirthDefect == true).Count();
                        SchoolPupilDTBS += PupilDTBS;
                        int PupilBP = lstPhysicalTest.Where(o => o.poc.ClassID == classID && o.pt.Nutrition == 2).Count();
                        SchoolPupilBP += PupilBP;
                        int PupilSDD = lstPhysicalTest.Where(o => o.poc.ClassID == classID && o.pt.Nutrition == 1).Count();
                        SchoolPupilSDD += PupilSDD;
                        // fill giữ liệu từng lớp
                        sheetType1.CopyPasteSameRowHeigh(rangeType1, startrow1);
                        sheetType1.SetCellValue(startrow1, 1, j);
                        sheetType1.SetCellValue(startrow1, 2, itemClass.DisplayName);
                        sheetType1.SetCellValue(startrow1, 3, sumPupil);
                        sheetType1.SetCellValue(startrow1, 4, sumPupilK);
                        sheetType1.SetFormulaValue(startrow1, 5, "=ROUND(IF(C"+ startrow1 +" > 0,D" + startrow1 + "/C" + startrow1 + "*100),1)");
                        sheetType1.SetCellValue(startrow1, 6, PupilEyeTest);
                        sheetType1.SetFormulaValue(startrow1, 7, "=ROUND(IF(C" + startrow1 + " > 0,F" + startrow1 + "/C" + startrow1 + "*100),1)");
                        sheetType1.SetCellValue(startrow1, 8, PupilEyeK);
                        sheetType1.SetFormulaValue(startrow1, 9, "=ROUND(IF(C" + startrow1 + " > 0,H" + startrow1 + "/C" + startrow1 + "*100),1)");
                        sheetType1.SetCellValue(startrow1, 10, PupilOrther);
                        sheetType1.SetFormulaValue(startrow1, 11, "=ROUND(IF(C" + startrow1 + " > 0,J" + startrow1 + "/C" + startrow1 + "*100),1)");
                        sheetType1.SetCellValue(startrow1, 12, PupilDentaTest);
                        sheetType1.SetFormulaValue(startrow1, 13, "=ROUND(IF(C" + startrow1 + " > 0,L" + startrow1 + "/C" + startrow1 + "*100),1)");
                        sheetType1.SetCellValue(startrow1, 14, PupilDentaTestOther);
                        sheetType1.SetFormulaValue(startrow1, 15, "=ROUND(IF(C" + startrow1 + " > 0,N" + startrow1 + "/C" + startrow1 + "*100),1)");
                        sheetType1.SetCellValue(startrow1, 16, PupilTMH);
                        sheetType1.SetFormulaValue(startrow1, 17, "=ROUND(IF(C" + startrow1 + " > 0,P" + startrow1 + "/C" + startrow1 + "*100),1)");
                        sheetType1.SetCellValue(startrow1, 18, PupilCS);
                        sheetType1.SetFormulaValue(startrow1, 19, "=ROUND(IF(C" + startrow1 + " > 0,R" + startrow1 + "/C" + startrow1 + "*100),1)");
                        sheetType1.SetCellValue(startrow1, 20, PupilDL);
                        sheetType1.SetFormulaValue(startrow1, 21, "=ROUND(IF(C" + startrow1 + " > 0,T" + startrow1 + "/C" + startrow1 + "*100),1)");
                        sheetType1.SetCellValue(startrow1, 22, PupilNK);
                        sheetType1.SetFormulaValue(startrow1, 23, "=ROUND(IF(C" + startrow1 + " > 0,V" + startrow1 + "/C" + startrow1 + "*100),1)");
                        sheetType1.SetCellValue(startrow1, 24, PupilNGK);
                        sheetType1.SetFormulaValue(startrow1, 25, "=ROUND(IF(C" + startrow1 + " > 0,X" + startrow1 + "/C" + startrow1 + "*100),1)");
                        sheetType1.SetCellValue(startrow1, 26, PupilT);
                        sheetType1.SetFormulaValue(startrow1, 27, "=ROUND(IF(C" + startrow1 + " > 0,Z" + startrow1 + "/C" + startrow1 + "*100),1)");
                        sheetType1.SetCellValue(startrow1, 28, PupilDTBS);
                        sheetType1.SetFormulaValue(startrow1, 29, "=ROUND(IF(C" + startrow1 + " > 0,AB" + startrow1 + "/C" + startrow1 + "*100),1)");
                        sheetType1.SetCellValue(startrow1, 30, PupilBP);
                        sheetType1.SetFormulaValue(startrow1, 31, "=ROUND(IF(C" + startrow1 + " > 0,AD" + startrow1 + "/C" + startrow1 + "*100),1)");
                        sheetType1.SetCellValue(startrow1, 32, PupilSDD);
                        sheetType1.SetFormulaValue(startrow1, 33, "=ROUND(IF(C" + startrow1 + " > 0,AF" + startrow1 + "/C" + startrow1 + "*100),1)");
                        startrow1++;

                    }

                    sheetType1.CopyPasteSameRowHeigh(rangeTotalType1, startrow1);
                    sheetType1.SetCellValue(startrow1, 2, "Cộng " + itemEdu.Resolution);
                    sheetType1.SetCellValue(startrow1, 3, "=SUM(C" + startrowClass1 + ":C" + (startrow1 - 1) + ")");
                    sheetType1.SetCellValue(startrow1, 4, "=SUM(D" + startrowClass1 + ":D" + (startrow1 - 1) + ")");
                    sheetType1.SetFormulaValue(startrow1, 5, "=ROUND(IF(C"+ startrow1 +">0,D" + startrow1 + "/C" + startrow1 + "*100),1)");
                    sheetType1.SetCellValue(startrow1, 6, "=SUM(F" + startrowClass1 + ":F" + (startrow1 - 1) + ")");
                    sheetType1.SetFormulaValue(startrow1, 7, "=ROUND(IF(C" + startrow1 + ">0,F" + startrow1 + "/C" + startrow1 + "*100),1)");
                    sheetType1.SetCellValue(startrow1, 8, "=SUM(H" + startrowClass1 + ":H" + (startrow1 - 1) + ")");
                    sheetType1.SetFormulaValue(startrow1, 9, "=ROUND(IF(C" + startrow1 + ">0,H" + startrow1 + "/C" + startrow1 + "*100),1)");
                    sheetType1.SetCellValue(startrow1, 10, "=SUM(J" + startrowClass1 + ":J" + (startrow1 - 1) + ")");
                    sheetType1.SetFormulaValue(startrow1, 11, "=ROUND(IF(C" + startrow1 + ">0,J" + startrow1 + "/C" + startrow1 + "*100),1)");
                    sheetType1.SetCellValue(startrow1, 12, "=SUM(L" + startrowClass1 + ":L" + (startrow1 - 1) + ")");
                    sheetType1.SetFormulaValue(startrow1, 13, "=ROUND(IF(C" + startrow1 + ">0,L" + startrow1 + "/C" + startrow1 + "*100),1)");
                    sheetType1.SetCellValue(startrow1, 14, "=SUM(N" + startrowClass1 + ":N" + (startrow1 - 1) + ")");
                    sheetType1.SetFormulaValue(startrow1, 15, "=ROUND(IF(C" + startrow1 + ">0,N" + startrow1 + "/C" + startrow1 + "*100),1)");
                    sheetType1.SetCellValue(startrow1, 16, "=SUM(P" + startrowClass1 + ":P" + (startrow1 - 1) + ")");
                    sheetType1.SetFormulaValue(startrow1, 17, "=ROUND(IF(C" + startrow1 + ">0,P" + startrow1 + "/C" + startrow1 + "*100),1)");
                    sheetType1.SetCellValue(startrow1, 18, "=SUM(R" + startrowClass1 + ":R" + (startrow1 - 1) + ")");
                    sheetType1.SetFormulaValue(startrow1, 19, "=ROUND(IF(C" + startrow1 + ">0,R" + startrow1 + "/C" + startrow1 + "*100),1)");
                    sheetType1.SetCellValue(startrow1, 20, "=SUM(T" + startrowClass1 + ":T" + (startrow1 - 1) + ")");
                    sheetType1.SetFormulaValue(startrow1, 21, "=ROUND(IF(C" + startrow1 + ">0,T" + startrow1 + "/C" + startrow1 + "*100),1)");
                    sheetType1.SetCellValue(startrow1, 22, "=SUM(V" + startrowClass1 + ":V" + (startrow1 - 1) + ")");
                    sheetType1.SetFormulaValue(startrow1, 23, "=ROUND(IF(C" + startrow1 + ">0,V" + startrow1 + "/C" + startrow1 + "*100),1)");
                    sheetType1.SetCellValue(startrow1, 24, "=SUM(X" + startrowClass1 + ":X" + (startrow1 - 1) + ")");
                    sheetType1.SetFormulaValue(startrow1, 25, "=ROUND(IF(C" + startrow1 + ">0,X" + startrow1 + "/C" + startrow1 + "*100),1)");
                    sheetType1.SetCellValue(startrow1, 26, "=SUM(Z" + startrowClass1 + ":Z" + (startrow1 - 1) + ")");
                    sheetType1.SetFormulaValue(startrow1, 27, "=ROUND(IF(C" + startrow1 + ">0,Z" + startrow1 + "/C" + startrow1 + "*100),1)");
                    sheetType1.SetCellValue(startrow1, 28, "=SUM(AB" + startrowClass1 + ":AB" + (startrow1 - 1) + ")");
                    sheetType1.SetFormulaValue(startrow1, 29, "=ROUND(IF(C" + startrow1 + ">0,AB" + startrow1 + "/C" + startrow1 + "*100),1)");
                    sheetType1.SetCellValue(startrow1, 30, "=SUM(AD" + startrowClass1 + ":AD" + (startrow1 - 1) + ")");
                    sheetType1.SetFormulaValue(startrow1, 31, "=ROUND(IF(C" + startrow1 + ">0,AD" + startrow1 + "/C" + startrow1 + "*100),1)");
                    sheetType1.SetCellValue(startrow1, 32, "=SUM(AF" + startrowClass1 + ":AF" + (startrow1 - 1) + ")");
                    sheetType1.SetFormulaValue(startrow1, 33, "=ROUND(IF(C" + startrow1 + ">0,AF" + startrow1 + "/C" + startrow1 + "*100),1)");
                    startrow1++;
                }

                sheetType1.CopyPasteSameRowHeigh(rangeTotalType1, startrow1);
                sheetType1.SetCellValue(startrow1, 2, "Tổng cộng");
                sheetType1.SetCellValue(startrow1, 3, SchoolsumPupil);
                sheetType1.SetCellValue(startrow1, 4, SchoolsumPupilK);
                sheetType1.SetFormulaValue(startrow1, 5, "=ROUND(IF(C" + startrow1 + " > 0,D" + startrow1 + "/C" + startrow1 + "*100),1)");
                sheetType1.SetCellValue(startrow1, 6, SchoolPupilEyeTest);
                sheetType1.SetFormulaValue(startrow1, 7, "=ROUND(IF(C" + startrow1 + " > 0,F" + startrow1 + "/C" + startrow1 + "*100),1)");
                sheetType1.SetCellValue(startrow1, 8, SchoolPupilEyeK);
                sheetType1.SetFormulaValue(startrow1, 9, "=ROUND(IF(C" + startrow1 + " > 0,H" + startrow1 + "/C" + startrow1 + "*100),1)");
                sheetType1.SetCellValue(startrow1, 10, SchoolPupilOrther);
                sheetType1.SetFormulaValue(startrow1, 11, "=ROUND(IF(C" + startrow1 + " > 0,J" + startrow1 + "/C" + startrow1 + "*100),1)");
                sheetType1.SetCellValue(startrow1, 12, SchoolPupilDentaTest);
                sheetType1.SetFormulaValue(startrow1, 13, "=ROUND(IF(C" + startrow1 + " > 0,L" + startrow1 + "/C" + startrow1 + "*100),1)");
                sheetType1.SetCellValue(startrow1, 14, SchoolPupilDentaTestOther);
                sheetType1.SetFormulaValue(startrow1, 15, "=ROUND(IF(C" + startrow1 + " > 0,N" + startrow1 + "/C" + startrow1 + "*100),1)");
                sheetType1.SetCellValue(startrow1, 16, SchoolPupilTMH);
                sheetType1.SetFormulaValue(startrow1, 17, "=ROUND(IF(C" + startrow1 + " > 0,P" + startrow1 + "/C" + startrow1 + "*100),1)");
                sheetType1.SetCellValue(startrow1, 18, SchoolPupilCS);
                sheetType1.SetFormulaValue(startrow1, 19, "=ROUND(IF(C" + startrow1 + " > 0,R" + startrow1 + "/C" + startrow1 + "*100),1)");
                sheetType1.SetCellValue(startrow1, 20, SchoolPupilDL);
                sheetType1.SetFormulaValue(startrow1, 21, "=ROUND(IF(C" + startrow1 + " > 0,T" + startrow1 + "/C" + startrow1 + "*100),1)");
                sheetType1.SetCellValue(startrow1, 22, SchoolPupilNK);
                sheetType1.SetFormulaValue(startrow1, 23, "=ROUND(IF(C" + startrow1 + " > 0,V" + startrow1 + "/C" + startrow1 + "*100),1)");
                sheetType1.SetCellValue(startrow1, 24, SchoolPupilNGK);
                sheetType1.SetFormulaValue(startrow1, 25, "=ROUND(IF(C" + startrow1 + " > 0,X" + startrow1 + "/C" + startrow1 + "*100),1)");
                sheetType1.SetCellValue(startrow1, 26, SchoolPupilT);
                sheetType1.SetFormulaValue(startrow1, 27, "=ROUND(IF(C" + startrow1 + " > 0,Z" + startrow1 + "/C" + startrow1 + "*100),1)");
                sheetType1.SetCellValue(startrow1, 28, SchoolPupilDTBS);
                sheetType1.SetFormulaValue(startrow1, 29, "=ROUND(IF(C" + startrow1 + " > 0,AB" + startrow1 + "/C" + startrow1 + "*100),1)");
                sheetType1.SetCellValue(startrow1, 30, SchoolPupilBP);
                sheetType1.SetFormulaValue(startrow1, 31, "=ROUND(IF(C" + startrow1 + " > 0,AD" + startrow1 + "/C" + startrow1 + "*100),1)");
                sheetType1.SetCellValue(startrow1, 32, SchoolPupilSDD);
                sheetType1.SetFormulaValue(startrow1, 33, "=ROUND(IF(C" + startrow1 + " > 0,AF" + startrow1 + "/C" + startrow1 + "*100),1)");
            }
            #endregion
            #region ket qua kham mat
            else if (ReportType == 2)
            {
                //Tạo sheet
                IVTWorksheet sheet = oBook.CopySheetToLast(firstSheet);
                IVTRange range = firstSheet.GetRange("A14", "P14");
                IVTRange rangeTotal = firstSheet.GetRange("A15", "P15");
                IVTRange rangeFirst = firstSheet.GetRange("A13", "P13");

                sheet.Name = "KETQUAKHAMMAT";
                sheet.SetCellValue("A2", Suppervising);
                sheet.SetCellValue("A3", SchoolName);
                sheet.SetCellValue("A8", "BÁO CÁO KẾT QUẢ KHÁM MẮT HỌC SINH - NĂM HỌC " + AcademicYear + " - " + PeriodExam.ToUpper());
                sheet.SetCellValue("M4", Province + ", ngày " + DateTime.Now.Day + " tháng " + DateTime.Now.Month + " năm " + DateTime.Now.Year);

                int startrow = 13;
                int startrowClass = 0;

                int i = 0;
                int SchoolSumPupil = 0;
                int SchoolpupilEye = 0;
                int SchoolpupilEyeNot = 0;
                int SchoolpupilEyeHave = 0;
                int SchoolpupilEyeHave8 = 0;
                int SchoolpupilOrther = 0;
                List<ClassProfile> lstClassTemp = new List<ClassProfile>();
                foreach (var itemEdu in lstEducationLevel)
                {
                    lstClassTemp = lstClass.Where(o => o.EducationLevelID == itemEdu.EducationLevelID).ToList();
                    //lstClass = lstClass.Where(o => o.EducationLevelID == itemEdu.EducationLevelID).ToList();
                    sheet.CopyPasteSameRowHeigh(rangeFirst, startrow);
                    sheet.SetCellValue(startrow, 2, itemEdu.Resolution);
                    startrow++;

                    startrowClass = startrow;
                    foreach (var itemClass in lstClassTemp)
                    {
                        i++;
                        int classID = itemClass.ClassProfileID;
                        int sumPupil = this.PupilOfClassBusiness.All.Where(o => o.SchoolID == entity.SchoolID && o.ClassID == classID && o.AcademicYearID == entity.AcademicYearID && o.Status == SystemParamsInFile.PUPIL_STATUS_STUDYING).ToList().Count();
                        var lstEyeTest1 = lstEyeTest.Where(o => o.mb.ClassID == classID);
                        SchoolSumPupil += sumPupil;
                        int pupilEye = lstEyeTest1.Count();
                        SchoolpupilEye += pupilEye;
                        int pupilEyeNot = lstEyeTest1.Where(o => o.et.IsTreatment == false && (o.et.REye < 8 || o.et.LEye < 8)).Count();
                        SchoolpupilEyeNot += pupilEyeNot;
                        int pupilEyeHave = lstEyeTest1.Where(o => o.et.IsTreatment == true).Count();
                        SchoolpupilEyeHave += pupilEyeHave;
                        int pupilEyeHave8 = lstEyeTest1.Where(o => o.et.IsTreatment == true && (o.et.REye < 8 || o.et.LEye < 8)).Count();
                        SchoolpupilEyeHave8 += pupilEyeHave8;
                        int pupilOrther = lstEyeTest1.Where(o => o.et.Other != null).Count();
                        SchoolpupilOrther += pupilOrther;
                        // fill giữ liệu từng lớp
                        sheet.CopyPasteSameRowHeigh(range, startrow);
                        sheet.SetCellValue(startrow, 1, i);
                        sheet.SetCellValue(startrow, 2, itemClass.DisplayName);
                        sheet.SetCellValue(startrow, 3, sumPupil);
                        sheet.SetCellValue(startrow, 4, pupilEye);
                        sheet.SetFormulaValue(startrow, 5, "=ROUND(IF(C"+ startrow +">0,D" + startrow + "/C" + startrow + "*100),1)");
                        sheet.SetCellValue(startrow, 6, pupilEyeNot);
                        sheet.SetFormulaValue(startrow, 7, "=ROUND(IF(C" + startrow + ">0,F" + startrow + "/C" + startrow + "*100),1)");
                        sheet.SetCellValue(startrow, 8, pupilEyeHave);
                        sheet.SetFormulaValue(startrow, 9, "=ROUND(IF(C" + startrow + ">0,H" + startrow + "/C" + startrow + "*100),1)");
                        sheet.SetCellValue(startrow, 10, pupilEyeHave8);
                        sheet.SetFormulaValue(startrow, 11, "=ROUND(IF(C" + startrow + ">0,J" + startrow + "/C" + startrow + "*100),1)");
                        sheet.SetCellValue(startrow, 12, pupilOrther);
                        sheet.SetFormulaValue(startrow, 13, "=ROUND(IF(C" + startrow + ">0,L" + startrow + "/C" + startrow + "*100),1)");
                        sheet.SetCellValue(startrow, 14, "=D" + startrow);
                        sheet.SetFormulaValue(startrow, 15, "=ROUND(IF(C" + startrow + ">0,N" + startrow + "/C" + startrow + "*100),1)");
                        startrow++;
                    }
                    sheet.CopyPasteSameRowHeigh(rangeTotal, startrow);
                    sheet.SetCellValue(startrow, 2, "Cộng " + itemEdu.Resolution);
                    sheet.SetCellValue(startrow, 3, "=SUM(C" + startrowClass + ":C" + (startrow - 1) + ")");
                    sheet.SetCellValue(startrow, 4, "=SUM(D" + startrowClass + ":D" + (startrow - 1) + ")");
                    sheet.SetCellValue(startrow, 5, "=ROUND(IF(C" + startrow + ">0,D" + startrow + "/C" + startrow + "*100),1)");
                    sheet.SetCellValue(startrow, 6, "=SUM(F" + startrowClass + ":F" + (startrow - 1) + ")");
                    sheet.SetCellValue(startrow, 7, "=ROUND(IF(C" + startrow + ">0,F" + startrow + "/C" + startrow + "*100),1)");
                    sheet.SetCellValue(startrow, 8, "=SUM(H" + startrowClass + ":H" + (startrow - 1) + ")");
                    sheet.SetCellValue(startrow, 9, "=ROUND(IF(C" + startrow + ">0,H" + startrow + "/C" + startrow + "*100),1)");
                    sheet.SetCellValue(startrow, 10, "=SUM(J" + startrowClass + ":J" + (startrow - 1) + ")");
                    sheet.SetCellValue(startrow, 11, "=ROUND(IF(C" + startrow + ">0,J" + startrow + "/C" + startrow + "*100),1)");
                    sheet.SetCellValue(startrow, 12, "=SUM(L" + startrowClass + ":L" + (startrow - 1) + ")");
                    sheet.SetCellValue(startrow, 13, "=ROUND(IF(C" + startrow + ">0,L" + startrow + "/C" + startrow + "*100),1)");
                    sheet.SetCellValue(startrow, 14, "=SUM(N" + startrowClass + ":N" + (startrow - 1) + ")");
                    sheet.SetCellValue(startrow, 15, "=ROUND(IF(C" + startrow + ">0,N" + startrow + "/C" + startrow + "*100),1)");
                    startrow++;
                }

                sheet.CopyPasteSameRowHeigh(rangeTotal, startrow);
                sheet.SetCellValue(startrow, 2, "Tổng cộng");
                sheet.SetCellValue(startrow, 3, SchoolSumPupil);
                sheet.SetCellValue(startrow, 4, SchoolpupilEye);
                sheet.SetFormulaValue(startrow, 5, "=ROUND(IF(C"+ startrow +"> 0,D" + startrow + "/C" + startrow + "*100),1)");
                sheet.SetCellValue(startrow, 6, SchoolpupilEyeNot);
                sheet.SetFormulaValue(startrow, 7, "=ROUND(IF(C" + startrow + "> 0,F" + startrow + "/C" + startrow + "*100),1)");
                sheet.SetCellValue(startrow, 8, SchoolpupilEyeHave);
                sheet.SetFormulaValue(startrow, 9, "=ROUND(IF(C" + startrow + "> 0,H" + startrow + "/C" + startrow + "*100),1)");
                sheet.SetCellValue(startrow, 10, SchoolpupilEyeHave8);
                sheet.SetFormulaValue(startrow, 11, "=ROUND(IF(C" + startrow + "> 0,J" + startrow + "/C" + startrow + "*100),1)");
                sheet.SetCellValue(startrow, 12, SchoolpupilOrther);
                sheet.SetFormulaValue(startrow, 13, "=ROUND(IF(C" + startrow + "> 0,L" + startrow + "/C" + startrow + "*100),1)");
                sheet.SetCellValue(startrow, 14, "=D" + startrow);
                sheet.SetFormulaValue(startrow, 15, "=ROUND(IF(C" + startrow + "> 0,N" + startrow + "/C" + startrow + "*100),1)");
                sheet.FitToPage = true;
                sheet.Orientation = VTXPageOrientation.VTxlLandscape;
            }
            #endregion
            //Xoá sheet template
            firstSheet.Delete();

            return oBook.ToStream();

        }

        #endregion


        #region GetHashKeyPGD
        /// <summary>
        /// Lấy mảng băm cho các tham số đầu vào của thống kê báo cáo kết quả khám sức khoẻ cấp phòng
        /// </summary>
        /// <param name="entity"></param>
        /// <returns>tungnd</returns>
        public string GetHashKeyPGD(ExcelHealthTestReportPGDBO entity)
        {
            IDictionary<string, object> dic = new Dictionary<string, object>();
            dic["ReportCode"] = SystemParamsInFile.REPORT_KETQUA_KHAM_SUCKHOE_PGD;
            dic["HeathPeriod"] = entity.HealthPeriod;
            dic["AcademicYearID"] = this.AcademicYearBusiness.All.Where(o => o.Year == entity.Year && o.IsActive == true).FirstOrDefault().AcademicYearID;
            dic["AppliedLevel"] = entity.AppliedLevel;
            dic["DistrictID"] = entity.DistrictID;
            dic["HealthPeriod"] = entity.HealthPeriod;
            dic["SupervisingDeptID"] = entity.SupervisingDeptID;
            dic["Year"] = entity.Year;
            return ReportUtils.GetHashKey(dic);
        }
        #endregion

        #region GetHealthTestReportPGD
        /// <summary>
        /// Lấy báo cáo thống kê kết quả khám sức khoẻ được cập nhật mới nhất
        /// </summary>
        /// <param name="entity"></param>
        /// <returns>tungnd</returns>
        public ProcessedReport GetHealthTestReportPGD(ExcelHealthTestReportPGDBO entity)
        {
            string reportCode = SystemParamsInFile.REPORT_KETQUA_KHAM_SUCKHOE_PGD;
            string inputParameterHashKey = GetHashKeyPGD(entity);
            return ProcessedReportBusiness.GetProcessedReport(reportCode, inputParameterHashKey);
        }
        #endregion

        #region InsertHealthTestReportPGD
        /// <summary>
        /// Lưu lại thông tin thống kê kết quả khám sức khoẻ
        /// </summary>
        /// <param name="entity"></param>
        /// <returns>tungnd</returns>
        public ProcessedReport InsertHealthTestReportPGD(ExcelHealthTestReportPGDBO entity, Stream data)
        {
            string reportCode = SystemParamsInFile.REPORT_KETQUA_KHAM_SUCKHOE_PGD;
            ReportDefinition reportDef = ReportDefinitionBusiness.GetByCode(reportCode);
            ProcessedReport pr = new ProcessedReport();
            pr.ReportCode = reportCode;
            pr.ProcessedDate = DateTime.Now;
            pr.InputParameterHashKey = GetHashKeyPGD(entity);
            pr.ReportData = ReportUtils.Compress(data);

            //Tạo tên file [PGD]_KetQuaKhamSucKhoePGD
            //string outputNamePattern = reportDef.OutputNamePattern;
            string outputNamePattern;
            SupervisingDept obj = this.SupervisingDeptBusiness.Find(entity.SupervisingDeptID);
            if (obj.HierachyLevel == SystemParamsInFile.EDUCATION_HIERACHY_LEVEL_DISTRICT_OFFICE || obj.HierachyLevel == SystemParamsInFile.EDUCATION_HIERACHY_LEVEL_DISTRICT_OFFICE_DEPARTMENT)
            {
                outputNamePattern = "PGD_[LEVEL]_KetQuaKhamSucKhoe_[PERIOD]";
            }
            else if (obj.HierachyLevel == SystemParamsInFile.EDUCATION_HIERACHY_LEVEL_PROVINCE_OFFICE || obj.HierachyLevel == SystemParamsInFile.EDUCATION_HIERACHY_LEVEL_PROVINCE_OFFICE_DEPARTMENT)
            {
                outputNamePattern = "SGD_[LEVEL]_KetQuaKhamSucKhoe_[PERIOD]";
            }
            else
            {
                outputNamePattern = "[LEVEL]_KetQuaKhamSucKhoe_[PERIOD]";
            }
            
            
            if (entity.AppliedLevel == 1)
            {
                outputNamePattern = outputNamePattern.Replace("[LEVEL]", "TH");
            }
            else if (entity.AppliedLevel == 2)
            {
                outputNamePattern = outputNamePattern.Replace("[LEVEL]", "THCS");
            }
            else if (entity.AppliedLevel == 3)
            {
                outputNamePattern = outputNamePattern.Replace("[LEVEL]", "THPT");
            }
            else if (entity.AppliedLevel == 4)
            {
                outputNamePattern = outputNamePattern.Replace("[LEVEL]", "NT");
            }
            else if (entity.AppliedLevel == 5)
            {
                outputNamePattern = outputNamePattern.Replace("[LEVEL]", "MG");
            }
            else
            {
                outputNamePattern = outputNamePattern.Replace("_[LEVEL]", "");
            }
            outputNamePattern = outputNamePattern.Replace("[PERIOD]", "Dot_" + entity.HealthPeriod.ToString());
            string schoolLevel = ReportUtils.ConvertAppliedLevelForReportName(entity.AppliedLevel);
            //pr.ReportName = ReportUtils.RemoveSpecialCharacters(outputNamePattern) + "." + reportDef.OutputFormat;
            pr.ReportName = outputNamePattern + "." + reportDef.OutputFormat;

            IDictionary<string, object> dic = new Dictionary<string, object> {
                {"Year", entity.Year},
                {"HealthPeriod", entity.HealthPeriod},
                {"SupervisingDeptID", entity.SupervisingDeptID},
                {"DistrictID", entity.DistrictID},
                {"AppliedLevel ", entity.AppliedLevel},
                {"AcademicYearID", this.AcademicYearBusiness.All.Where(o=> o.Year == entity.Year).FirstOrDefault().AcademicYearID}
            };
            ProcessedReportParameterRepository.Insert(dic, pr);
            ProcessedReportRepository.Insert(pr);
            ProcessedReportRepository.Save();
            return pr;
        }
        #endregion


        #region CreateHealthTestReportPGD
        /// <summary>
        /// Tạo file excel thống kê kết quả khám sức khoẻ cấp trường
        /// </summary>
        /// <param name="entity"></param>
        /// <returns>tungnd</returns>
        public Stream CreateHealthTestReportPGD(ExcelHealthTestReportPGDBO entity)
        {
            string PeriodExam = "";
            string reportCode = SystemParamsInFile.REPORT_KETQUA_KHAM_SUCKHOE_PGD;

            if (entity.HealthPeriod == 1)
            {
                PeriodExam = "Đợt I";
            }

            if (entity.HealthPeriod == 2)
            {
                PeriodExam = "Đợt II";
            }

            ReportDefinition reportDef = ReportDefinitionBusiness.GetByCode(reportCode);
            string AcademicYear = this.AcademicYearBusiness.All.Where(o => o.Year == entity.Year).FirstOrDefault().DisplayTitle;
            SupervisingDept obj = this.SupervisingDeptBusiness.Find(entity.SupervisingDeptID);

            string Suppervising = obj.SupervisingDeptName;
            string Province = obj.Province.ProvinceName;
            int ParentID = obj.ParentID.Value;
            string ParentName = this.SupervisingDeptBusiness.Find(ParentID).SupervisingDeptName.ToUpper();
            string templatePath = SystemParamsInFile.TEMPLATE_FOLDER + reportDef.TemplateDirectory + "/" + reportDef.TemplateName;
            IVTWorkbook oBook = VTExport.OpenWorkbook(templatePath);

            //Lấy sheet template
            IVTWorksheet firstSheet = oBook.GetSheet(1);

            //Tạo sheet
            IVTWorksheet sheet = oBook.GetSheet(2);
            sheet.CopyPasteSameSize(firstSheet.GetRange("A1", "AG12"), 1, 1);
            // IVTWorksheet sheet = oBook.CopySheetToLast(firstSheet);
            IVTRange range = firstSheet.GetRange("A14", "AG14");
            IVTRange rangeTotal = firstSheet.GetRange("A15", "AG15");
            IVTRange rangeFirst = firstSheet.GetRange("A13", "AG13");
            DateTime datetimeNow = DateTime.Now.Date;
            int SupID = entity.SupervisingDeptID;
            if (obj.HierachyLevel == SystemParamsInFile.EDUCATION_HIERACHY_LEVEL_DISTRICT_OFFICE || obj.HierachyLevel == SystemParamsInFile.EDUCATION_HIERACHY_LEVEL_DISTRICT_OFFICE_DEPARTMENT)
            {
            sheet.Name = "KETQUAKHAMSUCKHOEPGD";
            }
            else if (obj.HierachyLevel == SystemParamsInFile.EDUCATION_HIERACHY_LEVEL_PROVINCE_OFFICE || obj.HierachyLevel == SystemParamsInFile.EDUCATION_HIERACHY_LEVEL_PROVINCE_OFFICE_DEPARTMENT)
            {
                sheet.Name = "KETQUAKHAMSUCKHOESGD";
            }
            else
            {
                sheet.Name = "KETQUAKHAMSUCKHOE";
            }
            if (obj.HierachyLevel == SystemParamsInFile.EDUCATION_HIERACHY_LEVEL_DISTRICT_OFFICE_DEPARTMENT || obj.HierachyLevel == SystemParamsInFile.EDUCATION_HIERACHY_LEVEL_PROVINCE_OFFICE_DEPARTMENT)
            {
                SupID = obj.ParentID.Value;
            }
            sheet.SetCellValue("A2", ParentName);
            sheet.SetCellValue("A3", Suppervising);
            sheet.SetCellValue("A8", "KẾT QUẢ KHÁM SỨC KHỎE HỌC SINH - NĂM HỌC " + AcademicYear + " - " + PeriodExam.ToUpper());
            sheet.SetCellValue("R4", Province + ", ngày " + datetimeNow.Day + " tháng " + datetimeNow.Month + " năm " + datetimeNow.Year);


            Dictionary<string, object> Dictionary = new Dictionary<string, object>();
            Dictionary["SupervisingDeptID"] = SupID;
            Dictionary["DistrictID"] = entity.DistrictID;
            if (entity.AppliedLevel != 0)
            {
                if (entity.AppliedLevel == 4)
                {
                    Dictionary["AppliedLevel"] = SystemParamsInFile.APPLIED_LEVEL_CRECHE;
                }
                else
                {
                    Dictionary["AppliedLevel"] = entity.AppliedLevel;
                }
            }

            int grade = entity.AppliedLevel > 0 ? Utils.GradeToBinary(entity.AppliedLevel) : 0;

            List<SchoolProfile> lstSchool = this.SchoolProfileBusiness.Search(Dictionary).OrderBy(s => s.EducationGrade).ToList();//bỏ ra trường cấp 3

            var listPupil = from poc in this.PupilOfClassBusiness.All
                            join ay in this.AcademicYearBusiness.All on poc.AcademicYearID equals ay.AcademicYearID
                            where ay.Year == entity.Year
                                && (poc.Status == SystemParamsInFile.PUPIL_STATUS_STUDYING || poc.Status == SystemParamsInFile.PUPIL_STATUS_GRADUATED)
                            select poc;

            var listPupilK = from mb in MonitoringBookBusiness.All
                             join poc in this.PupilOfClassBusiness.All on mb.PupilID equals poc.PupilID
                             join ay in this.AcademicYearBusiness.All on mb.AcademicYearID equals ay.AcademicYearID
                             where ay.Year == entity.Year
                                   && mb.HealthPeriodID == entity.HealthPeriod
                                   && poc.AcademicYearID == ay.AcademicYearID
                                   && (poc.Status == SystemParamsInFile.PUPIL_STATUS_STUDYING || poc.Status == SystemParamsInFile.PUPIL_STATUS_GRADUATED)
                             select new { poc, mb, ay };


            var lstEyeTest = from mb in MonitoringBookBusiness.All
                             join poc in this.PupilOfClassBusiness.All on mb.PupilID equals poc.PupilID
                             join et in EyeTestBusiness.All on mb.MonitoringBookID equals et.MonitoringBookID
                             join ay in this.AcademicYearBusiness.All on mb.AcademicYearID equals ay.AcademicYearID
                             where ay.Year == entity.Year
                                && mb.HealthPeriodID == entity.HealthPeriod
                                && poc.AcademicYearID == mb.AcademicYearID
                                && (poc.Status == SystemParamsInFile.PUPIL_STATUS_STUDYING || poc.Status == SystemParamsInFile.PUPIL_STATUS_GRADUATED)
                                && et.IsActive == true
                             select new { poc, mb, ay, et };

            var lstDentalTest = from mb in MonitoringBookBusiness.All
                                join poc in this.PupilOfClassBusiness.All on mb.PupilID equals poc.PupilID
                                join dt in DentalTestBusiness.All on mb.MonitoringBookID equals dt.MonitoringBookID
                                join ay in this.AcademicYearBusiness.All on mb.AcademicYearID equals ay.AcademicYearID
                                where ay.Year == entity.Year
                                    && mb.HealthPeriodID == entity.HealthPeriod                            
                                    && poc.AcademicYearID == mb.AcademicYearID
                                    && (poc.Status == SystemParamsInFile.PUPIL_STATUS_STUDYING || poc.Status == SystemParamsInFile.PUPIL_STATUS_GRADUATED)   
                                    && dt.IsActive == true
                                select new { poc, mb, ay, dt };


            var lstENTTest = from mb in MonitoringBookBusiness.All
                             join poc in this.PupilOfClassBusiness.All on mb.PupilID equals poc.PupilID
                             join ent in ENTTestBusiness.All on mb.MonitoringBookID equals ent.MonitoringBookID
                             join ay in this.AcademicYearBusiness.All on mb.AcademicYearID equals ay.AcademicYearID
                             where ay.Year == entity.Year
                                && mb.HealthPeriodID == entity.HealthPeriod    
                                && poc.AcademicYearID == mb.AcademicYearID
                                && (poc.Status == SystemParamsInFile.PUPIL_STATUS_STUDYING || poc.Status == SystemParamsInFile.PUPIL_STATUS_GRADUATED) 
                                && ent.IsSick == true
                                && ent.IsActive == true
                             select new { poc, mb, ay, ent };

            var lstSpineTest = from mb in MonitoringBookBusiness.All
                               join poc in this.PupilOfClassBusiness.All on mb.PupilID equals poc.PupilID
                               join st in SpineTestBusiness.All on mb.MonitoringBookID equals st.MonitoringBookID
                               join ay in this.AcademicYearBusiness.All on mb.AcademicYearID equals ay.AcademicYearID
                               where ay.Year == entity.Year
                                    && mb.HealthPeriodID == entity.HealthPeriod
                                    && poc.AcademicYearID == mb.AcademicYearID
                                    && (poc.Status == SystemParamsInFile.PUPIL_STATUS_STUDYING || poc.Status == SystemParamsInFile.PUPIL_STATUS_GRADUATED) 
                                    && st.IsDeformityOfTheSpine == true
                                    && st.IsActive == true
                               select new { poc, mb, ay, st };

            var lstOverallTest = from mb in MonitoringBookBusiness.All
                                 join poc in this.PupilOfClassBusiness.All on mb.PupilID equals poc.PupilID
                                 join ot in OverallTestBusiness.All on mb.MonitoringBookID equals ot.MonitoringBookID
                                 join ay in this.AcademicYearBusiness.All on mb.AcademicYearID equals ay.AcademicYearID
                                 where ay.Year == entity.Year
                                    && mb.HealthPeriodID == entity.HealthPeriod
                                    && poc.AcademicYearID == mb.AcademicYearID
                                    && (poc.Status == SystemParamsInFile.PUPIL_STATUS_STUDYING || poc.Status == SystemParamsInFile.PUPIL_STATUS_GRADUATED)   
                                    && ot.IsActive == true
                                 select new { poc, mb, ay, ot };

            var lstPhysicalTest = from mb in MonitoringBookBusiness.All
                                  join poc in this.PupilOfClassBusiness.All on mb.PupilID equals poc.PupilID
                                  join pt in PhysicalTestBusiness.All on mb.MonitoringBookID equals pt.MonitoringBookID
                                  join ay in this.AcademicYearBusiness.All on mb.AcademicYearID equals ay.AcademicYearID
                                  where (ay.Year == entity.Year)
                                        && (mb.HealthPeriodID == entity.HealthPeriod)       
                                        && poc.AcademicYearID == mb.AcademicYearID
                                        && (poc.Status == SystemParamsInFile.PUPIL_STATUS_STUDYING || poc.Status == SystemParamsInFile.PUPIL_STATUS_GRADUATED) 
                                        && (pt.IsActive == true)
                                  select new { poc, mb, ay, pt };


            var lstSchoolGrade = lstSchool.GroupBy(o => o.EducationGrade).ToList();
            string AppName = "";
            int startrow = 13;
            int i = 0;
            int PGDsumPupil = 0;
            int PGDsumPupilK = 0;
            int PGDPupilC = 0;
            int PGDPupilKX = 0;
            int PGDPupilEyeOrther = 0;
            int PGDPupilDenta = 0;
            int PGDPupilDentaOrther = 0;
            int PGDPupilTMH = 0;
            int PGDPupilCS = 0;
            int PGDPupilDL = 0;
            int PGDPupilNK = 0;
            int PGDPupilNGK = 0;
            int PGDPupilT = 0;
            int PGDPupilDTBS = 0;
            int PGDPupilBP = 0;
            int PGDPupilSDD = 0;
            int StartrowSchool = 0;

            foreach (var item in lstSchoolGrade)
            {
                if (item.Key == 1)
                {
                    AppName = "Tiểu học";
                }
                else if (item.Key == 2)
                {
                    AppName = "THCS";
                }
                else if (item.Key == 3)
                {
                    AppName = "PTCS";
                }
                else if (item.Key == 8)
                {
                    AppName = "Nhà trẻ";
                }
                else if (item.Key == 16)
                {
                    AppName = "Mẫu giáo";
                }
                else if (item.Key == 24)
                {
                    AppName = "Mầm non";
                }
                else if (item.Key == 4)
                {
                    AppName = "THPT"; // Trung hoc pho thong
                }
                else if (item.Key == 6)
                {
                    AppName = "PTTH"; // Pho thong trung hoc
                }
                else if (item.Key == 7)
                {
                    AppName = "PT"; // Pho thong
                }
                else if (item.Key == 31)
                {
                    AppName = "Mầm non + Phổ thông";
                }

                sheet.CopyPasteSameRowHeigh(rangeFirst, startrow);
                sheet.SetCellValue(startrow, 2, AppName);
                startrow++;
                StartrowSchool = startrow;
                var lstSchoolGroup = lstSchool.Where(o => o.EducationGrade == item.FirstOrDefault().EducationGrade ).ToList();
                foreach (var itemSchool in lstSchoolGroup)
                {

                    i++;
                    int sumPupil = listPupil.Where(o => o.SchoolID == itemSchool.SchoolProfileID).Count();
                    PGDsumPupil += sumPupil;
                    int sumPupilK = listPupilK.Where(o => o.poc.SchoolID == itemSchool.SchoolProfileID).Count();
                    PGDsumPupilK += sumPupilK;

                    ///////////////////////////////////////////////////////////////////////////////////////////
                    //// So hoc sinh bi can thi
                    //int PupilEyeTest = lstEyeTest.Where(o => o.poc.ClassID == classID && o.et.IsMyopic == true).Count();
                    //SchoolPupilEyeTest += PupilEyeTest;
                    //// So hoc sinh bi tat khuc xa
                    //int PupilEyeK = lstEyeTest.Where(o => o.poc.ClassID == classID && (o.et.IsAstigmatism == true || o.et.IsFarsightedness == true)).Count();
                    //SchoolPupilEyeK += PupilEyeK;
                    //// So hoc sinh bi benh mat khac
                    //int PupilOrther = lstEyeTest.Where(o => o.poc.ClassID == classID && o.et.Other != null).Count();
                    ///////////////////////////////////////////////////////////////////////////////////////////

                    // So hoc sinh can thi
                    int PupilC = lstEyeTest.Where(o => o.poc.SchoolID == itemSchool.SchoolProfileID && o.et.IsMyopic == true).Count();
                    PGDPupilC += PupilC;
                    // So hoc sinh bi tat khuc xa
                    int PupilKX = lstEyeTest.Where(o => o.poc.SchoolID == itemSchool.SchoolProfileID && (o.et.IsAstigmatism == true || o.et.IsFarsightedness == true)).Count();
                    PGDPupilKX += PupilKX;
                    // So học sinh bi benh mat khac
                    int PupilEyeOrther = lstEyeTest.Where(o => o.poc.SchoolID == itemSchool.SchoolProfileID && o.et.Other != null).Count();
                    PGDPupilEyeOrther += PupilEyeOrther;
                    // So hoc sinh bi benh rang mieng
                    int PupilDenta = lstDentalTest.Where(o => o.poc.SchoolID == itemSchool.SchoolProfileID && o.dt.NumDentalFilling > 0).Count();
                    PGDPupilDenta += PupilDenta;
                    // So hoc sinh bi banh rang mieng khac
                    int PupilDentaOrther = lstDentalTest.Where(o => o.poc.SchoolID == itemSchool.SchoolProfileID && o.dt.Other != null).Count();
                    PGDPupilDentaOrther += PupilDentaOrther;
                    int PupilTMH = lstENTTest.Where(o => o.poc.SchoolID == itemSchool.SchoolProfileID).Count();
                    PGDPupilTMH += PupilTMH;
                    int PupilCS = lstSpineTest.Where(o => o.poc.SchoolID == itemSchool.SchoolProfileID).Count();
                    PGDPupilCS += PupilCS;
                    int PupilDL = lstOverallTest.Where(o => o.poc.SchoolID == itemSchool.SchoolProfileID && o.ot.SkinDiseases == 1).Count();
                    PGDPupilDL += PupilDL;
                    int PupilNK = lstOverallTest.Where(o => o.poc.SchoolID == itemSchool.SchoolProfileID && o.ot.OverallInternal == 1).Count();
                    PGDPupilNK += PupilNK;
                    int PupilNGK = lstOverallTest.Where(o => o.poc.SchoolID == itemSchool.SchoolProfileID && o.ot.OverallForeign == 1).Count();
                    PGDPupilNGK += PupilNGK;
                    int PupilT = lstOverallTest.Where(o => o.poc.SchoolID == itemSchool.SchoolProfileID && o.ot.IsHeartDiseases == true).Count();
                    PGDPupilT += PupilT;
                    int PupilDTBS = lstOverallTest.Where(o => o.poc.SchoolID == itemSchool.SchoolProfileID && o.ot.IsBirthDefect == true).Count();
                    PGDPupilDTBS += PupilDTBS;
                    int PupilBP = lstPhysicalTest.Where(o => o.poc.SchoolID == itemSchool.SchoolProfileID && o.pt.Nutrition == 2).Count();
                    PGDPupilBP += PupilBP;
                    int PupilSDD = lstPhysicalTest.Where(o => o.poc.SchoolID == itemSchool.SchoolProfileID && o.pt.Nutrition == 1).Count();
                    PGDPupilSDD += PupilSDD;
                    // fill giữ liệu vào excel
                    sheet.CopyPasteSameRowHeigh(range, startrow);
                    sheet.SetCellValue(startrow, 1, i);
                    sheet.SetCellValue(startrow, 2, itemSchool.SchoolName);
                    sheet.SetCellValue(startrow, 3, sumPupil);
                    sheet.SetCellValue(startrow, 4, sumPupilK);
                    sheet.SetFormulaValue(startrow, 5, "=IF(C"+ startrow +" > 0; D" + startrow + "/C" + startrow + "*100" + ";0)");
                    sheet.SetCellValue(startrow, 6, PupilC);
                    sheet.SetCellValue(startrow, 7, "=IF(C"+ startrow + " > 0; F" + startrow + "/C" + startrow + "*100" + ";0)");
                    sheet.SetCellValue(startrow, 8, PupilKX);
                    sheet.SetCellValue(startrow, 9, "=IF(C"+ startrow + " > 0; H" + startrow + "/C" + startrow + "*100" + ";0)");
                    sheet.SetCellValue(startrow, 10, PupilEyeOrther);
                    sheet.SetCellValue(startrow, 11, "=IF(C"+ startrow + " > 0; J" + startrow + "/C" + startrow + "*100" + ";0)");
                    sheet.SetCellValue(startrow, 12, PupilDenta);
                    sheet.SetCellValue(startrow, 13, "=IF(C"+ startrow + " > 0; L" + startrow + "/C" + startrow + "*100" + ";0)");
                    sheet.SetCellValue(startrow, 14, PupilDentaOrther);
                    sheet.SetCellValue(startrow, 15, "=IF(C"+ startrow + " > 0; N" + startrow + "/C" + startrow + "*100" + ";0)");
                    sheet.SetCellValue(startrow, 16, PupilTMH);
                    sheet.SetCellValue(startrow, 17, "=IF(C"+ startrow + " > 0; P" + startrow + "/C" + startrow + "*100" + ";0)");
                    sheet.SetCellValue(startrow, 18, PupilCS);
                    sheet.SetCellValue(startrow, 19, "=IF(C"+ startrow + " > 0; R" + startrow + "/C" + startrow + "*100" + ";0)");
                    sheet.SetCellValue(startrow, 20, PupilDL);
                    sheet.SetCellValue(startrow, 21, "=IF(C"+ startrow + " > 0; T" + startrow + "/C" + startrow + "*100" + ";0)");
                    sheet.SetCellValue(startrow, 22, PupilNK);
                    sheet.SetCellValue(startrow, 23, "=IF(C"+ startrow + " > 0; V" + startrow + "/C" + startrow + "*100" + ";0)");
                    sheet.SetCellValue(startrow, 24, PupilNGK);
                    sheet.SetCellValue(startrow, 25, "=IF(C"+ startrow + " > 0; X" + startrow + "/C" + startrow + "*100" + ";0)");
                    sheet.SetCellValue(startrow, 26, PupilT);
                    sheet.SetCellValue(startrow, 27, "=IF(C"+ startrow + " > 0; Z" + startrow + "/C" + startrow + "*100" + ";0)");
                    sheet.SetCellValue(startrow, 28, PupilDTBS);
                    sheet.SetCellValue(startrow, 29, "=IF(C"+ startrow + " > 0; AB" + startrow + "/C" + startrow + "*100" + ";0)");
                    sheet.SetCellValue(startrow, 30, PupilBP);
                    sheet.SetCellValue(startrow, 31, "=IF(C"+ startrow + " > 0; AD" + startrow + "/C" + startrow + "*100" + ";0)");
                    sheet.SetCellValue(startrow, 32, PupilSDD);
                    sheet.SetCellValue(startrow, 33, "=IF(C"+ startrow + " > 0; AF" + startrow + "/C" + startrow + "*100" + ";0)");
                    startrow++;

                }

                sheet.CopyPasteSameRowHeigh(rangeTotal, startrow);
                sheet.SetCellValue(startrow, 2, "Cộng " + AppName);
                sheet.SetCellValue(startrow, 3, "=SUM(C" + StartrowSchool + ":C" + (startrow - 1) + ")");
                sheet.SetCellValue(startrow, 4, "=SUM(D" + StartrowSchool + ":D" + (startrow - 1) + ")");
                sheet.SetCellValue(startrow, 5, "=D" + startrow + "/C" + startrow + "*100");
                sheet.SetCellValue(startrow, 6, "=SUM(F" + StartrowSchool + ":F" + (startrow - 1) + ")");
                sheet.SetCellValue(startrow, 7, "=F" + startrow + "/C" + startrow + "*100");
                sheet.SetCellValue(startrow, 8, "=SUM(H" + StartrowSchool + ":H" + (startrow - 1) + ")");
                sheet.SetCellValue(startrow, 9, "=H" + startrow + "/C" + startrow + "*100");
                sheet.SetCellValue(startrow, 10, "=SUM(J" + StartrowSchool + ":J" + (startrow - 1) + ")");
                sheet.SetCellValue(startrow, 11, "=J" + startrow + "/C" + startrow + "*100");
                sheet.SetCellValue(startrow, 12, "=SUM(L" + StartrowSchool + ":L" + (startrow - 1) + ")");
                sheet.SetCellValue(startrow, 13, "=L" + startrow + "/C" + startrow + "*100");
                sheet.SetCellValue(startrow, 14, "=SUM(N" + StartrowSchool + ":N" + (startrow - 1) + ")");
                sheet.SetCellValue(startrow, 15, "=N" + startrow + "/C" + startrow + "*100");
                sheet.SetCellValue(startrow, 16, "=SUM(P" + StartrowSchool + ":P" + (startrow - 1) + ")");
                sheet.SetCellValue(startrow, 17, "=P" + startrow + "/C" + startrow + "*100");
                sheet.SetCellValue(startrow, 18, "=SUM(R" + StartrowSchool + ":R" + (startrow - 1) + ")");
                sheet.SetCellValue(startrow, 19, "=R" + startrow + "/C" + startrow + "*100");
                sheet.SetCellValue(startrow, 20, "=SUM(T" + StartrowSchool + ":T" + (startrow - 1) + ")");
                sheet.SetCellValue(startrow, 21, "=T" + startrow + "/C" + startrow + "*100");
                sheet.SetCellValue(startrow, 22, "=SUM(V" + StartrowSchool + ":V" + (startrow - 1) + ")");
                sheet.SetCellValue(startrow, 23, "=V" + startrow + "/C" + startrow + "*100");
                sheet.SetCellValue(startrow, 24, "=SUM(X" + StartrowSchool + ":X" + (startrow - 1) + ")");
                sheet.SetCellValue(startrow, 25, "=X" + startrow + "/C" + startrow + "*100");
                sheet.SetCellValue(startrow, 26, "=SUM(Z" + StartrowSchool + ":Z" + (startrow - 1) + ")");
                sheet.SetCellValue(startrow, 27, "=Z" + startrow + "/C" + startrow + "*100");
                sheet.SetCellValue(startrow, 28, "=SUM(AB" + StartrowSchool + ":AB" + (startrow - 1) + ")");
                sheet.SetCellValue(startrow, 29, "=AB" + startrow + "/C" + startrow + "*100");
                sheet.SetCellValue(startrow, 30, "=SUM(AD" + StartrowSchool + ":AD" + (startrow - 1) + ")");
                sheet.SetCellValue(startrow, 31, "=AD" + startrow + "/C" + startrow + "*100");
                sheet.SetCellValue(startrow, 32, "=SUM(AF" + StartrowSchool + ":AF" + (startrow - 1) + ")");
                sheet.SetCellValue(startrow, 33, "=AF" + startrow + "/C" + startrow + "*100");
                startrow++;


            }


            sheet.CopyPasteSameRowHeigh(rangeTotal, startrow);
            sheet.SetCellValue(startrow, 2, "Tổng cộng");
            sheet.SetCellValue(startrow, 3, PGDsumPupil);
            sheet.SetCellValue(startrow, 4, PGDsumPupilK);
            sheet.SetCellValue(startrow, 5, "=D" + startrow + "/C" + startrow + "*100");
            sheet.SetCellValue(startrow, 6, PGDPupilC);
            sheet.SetCellValue(startrow, 7, "=F" + startrow + "/C" + startrow + "*100");
            sheet.SetCellValue(startrow, 8, PGDPupilKX);
            sheet.SetCellValue(startrow, 9, "=H" + startrow + "/C" + startrow + "*100");
            sheet.SetCellValue(startrow, 10, PGDPupilEyeOrther);
            sheet.SetCellValue(startrow, 11, "=J" + startrow + "/C" + startrow + "*100");
            sheet.SetCellValue(startrow, 12, PGDPupilDenta);
            sheet.SetCellValue(startrow, 13, "=L" + startrow + "/C" + startrow + "*100");
            sheet.SetCellValue(startrow, 14, PGDPupilDentaOrther);
            sheet.SetCellValue(startrow, 15, "=N" + startrow + "/C" + startrow + "*100");
            sheet.SetCellValue(startrow, 16, PGDPupilTMH);
            sheet.SetCellValue(startrow, 17, "=P" + startrow + "/C" + startrow + "*100");
            sheet.SetCellValue(startrow, 18, PGDPupilCS);
            sheet.SetCellValue(startrow, 19, "=R" + startrow + "/C" + startrow + "*100");
            sheet.SetCellValue(startrow, 20, PGDPupilDL);
            sheet.SetCellValue(startrow, 21, "=T" + startrow + "/C" + startrow + "*100");
            sheet.SetCellValue(startrow, 22, PGDPupilNK);
            sheet.SetCellValue(startrow, 23, "=V" + startrow + "/C" + startrow + "*100");
            sheet.SetCellValue(startrow, 24, PGDPupilNGK);
            sheet.SetCellValue(startrow, 25, "=X" + startrow + "/C" + startrow + "*100");
            sheet.SetCellValue(startrow, 26, PGDPupilT);
            sheet.SetCellValue(startrow, 27, "=Z" + startrow + "/C" + startrow + "*100");
            sheet.SetCellValue(startrow, 28, PGDPupilDTBS);
            sheet.SetCellValue(startrow, 29, "=AB" + startrow + "/C" + startrow + "*100");
            sheet.SetCellValue(startrow, 30, PGDPupilBP);
            sheet.SetCellValue(startrow, 31, "=AD" + startrow + "/C" + startrow + "*100");
            sheet.SetCellValue(startrow, 32, PGDPupilSDD);
            sheet.SetCellValue(startrow, 33, "=AF" + startrow + "/C" + startrow + "*100");


            //Xoá sheet template
            firstSheet.Delete();

            return oBook.ToStream();


        }

        #endregion

    }
}
