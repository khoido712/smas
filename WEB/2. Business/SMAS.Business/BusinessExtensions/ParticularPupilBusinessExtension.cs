/**
 * The Viettel License
 *
 * Copyright 2012 Viettel Telecom. All rights reserved.
 * VIETTEL PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */

/** 
* @author  trangdd
* @version $Revision: $
*/

using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Linq.Expressions;
using System.Web;
using log4net;

using SMAS.DAL.Repository;
using SMAS.DAL.IRepository;
using SMAS.Business.IBusiness;
using SMAS.Models.Models;
using SMAS.Business.Common;
using SMAS.Business.BusinessObject;

namespace SMAS.Business.Business
{
    public partial class ParticularPupilBusiness
    {
        #region Validate
        /// <summary>     
        /// </summary>
        /// <author>trangdd</author>
        /// <date>15/10/2012</date>      
        private void Validate(ParticularPupil ParticularPupil)
        {
            ValidationMetadata.ValidateObject(ParticularPupil);

            if (ParticularPupil.RealUpdatedDate > DateTime.Now)
                throw new BusinessException("ParticularPupil_Label_RealUpdateDateError");

            //If(SchoolID != null): SchoolID: FK(SchoolProfile)
            SchoolProfileBusiness.CheckAvailable(ParticularPupil.SchoolID, "ParticularPupil_Label_SchoolID", true);

            //ClassID (ClassProfileID), EducationLevelID: not compatible(ClassProfile)
            bool ClassCompatible = new ClassProfileRepository(this.context).ExistsRow(GlobalConstants.SCHOOL_SCHEMA, "ClassProfile",
                   new Dictionary<string, object>()
                {
                    {"ClassProfileID",ParticularPupil.ClassID},
                    {"EducationLevelID",ParticularPupil.EducationLevelID}
                }, null);

            if (!ClassCompatible)
            {
                throw new BusinessException("Common_Validate_NotCompatible");
            }
            //PupilID, ClassID (CurrentClassID): not compatible(PupilProfile)
            /*bool PupilCompatible = new PupilProfileRepository(this.context).ExistsRow(GlobalConstants.PUPIL_SCHEMA, "PupilProfile",
                  new Dictionary<string, object>()
                {
                    {"CurrentClassID",ParticularPupil.ClassID},
                    {"PupilProfileID",ParticularPupil.PupilID}
                }, null);
            if (!PupilCompatible)
            {
                throw new BusinessException("Common_Validate_NotCompatible");
            }*/
            //ClassID, AcademicYearID: not compatible(ClassProfile)
            bool AcademicYearCompatible = new ClassProfileRepository(this.context).ExistsRow(GlobalConstants.SCHOOL_SCHEMA, "ClassProfile",
                  new Dictionary<string, object>()
                {
                    {"ClassProfileID",ParticularPupil.ClassID},
                    {"AcademicYearID",ParticularPupil.AcademicYearID}
                }, null);
            if (!AcademicYearCompatible)
            {
                throw new BusinessException("Common_Validate_NotCompatible");
            }
            //SchoolID, AcademicYearID: not compatible(AcademicYear)
            bool SchoolCompatible = new AcademicYearRepository(this.context).ExistsRow(GlobalConstants.SCHOOL_SCHEMA, "AcademicYear",
                new Dictionary<string, object>()
                {
                    {"SchoolID",ParticularPupil.SchoolID},
                    {"AcademicYearID",ParticularPupil.AcademicYearID}
                }, null);
            if (!SchoolCompatible)
            {
                throw new BusinessException("Common_Validate_NotCompatible");
            }

            PupilOfClass poc = PupilOfClassBusiness.All.Where(o => o.PupilID == ParticularPupil.PupilID
                                                                    && o.ClassID == ParticularPupil.ClassID
                                                                    && o.SchoolID == ParticularPupil.SchoolID
                                                                    && o.AcademicYearID == ParticularPupil.AcademicYearID)
                                                        .OrderByDescending(o => o.AssignedDate).FirstOrDefault();
            if (poc == null || poc.Status != GlobalConstants.PUPIL_STATUS_STUDYING)
            {
                throw new BusinessException("Common_Validate_Pupil_Status_Studying");
            }

            if (ParticularPupil.RealUpdatedDate > DateTime.Now)
            {
                throw new BusinessException("ParticularPupil_Label_ExceptionDate");
            }

            bool Exist = repository.ExistsRow(GlobalConstants.PUPIL_SCHEMA, "ParticularPupil",
                                                new Dictionary<string, object>()
                                                {
                                                    {"PupilID",ParticularPupil.PupilID}
                                                    ,{"ClassID",ParticularPupil.ClassID}
                                                    ,{"EducationLevelID",ParticularPupil.EducationLevelID}
                                                    ,{"SchoolID",ParticularPupil.SchoolID}
                                                    ,{"AcademicYearID",ParticularPupil.AcademicYearID}
                                                }, new Dictionary<string, object>() { { "ParticularPupilID", ParticularPupil.ParticularPupilID } });
            if (Exist)
            {
                throw new BusinessException("ParticularPupil_Label_ExceptionPupil");
            }
        }

        #endregion
        #region InsertParticularPupil
        /// <summary>
        /// Thêm mới thông tin học sinh cá biệt
        /// </summary>
        /// <author>trangdd</author>
        /// <date>15/10/2012</date>      

        public void InsertParticularPupil(int UserID, ParticularPupil ParticularPupil)
        {
            Validate(ParticularPupil);            
            if (UtilsBusiness.HasHeadTeacherPermission(UserID, ParticularPupil.ClassID))
                this.Insert(ParticularPupil);
        }
        #endregion

        #region UpdateParticularPupil
        /// <summary>
        /// Sửa thông tin học sinh cá biệt
        /// </summary>
        /// <author>trangdd</author>
        /// <date>15/10/2012</date>      

        public void UpdateParticularPupil(int UserID, ParticularPupil ParticularPupil)
        {
            Validate(ParticularPupil);
            if (UtilsBusiness.HasHeadTeacherPermission(UserID, ParticularPupil.ClassID))
                this.Update(ParticularPupil);
        }
        #endregion

        #region DeleteParticularPupil
        /// <summary>
        /// Xoá thông tin học sinh cá biệt
        /// </summary>
        /// <author>trangdd</author>
        /// <date>15/10/2012</date>      

        public void DeleteParticularPupil(int UserID, int ParticularPupilID, int SchoolID)
        {
            //ParticularPupilID: PK(ParticularPupilID)            
            new ParticularPupilBusiness(null).CheckAvailable((int)ParticularPupilID, "ParticularPupil_Label_ParticularPupilID", false);

            ParticularPupil ParticularPupil = ParticularPupilRepository.Find(ParticularPupilID);
            //PupilID, SchoolID: not compatible(PupilProfile)
            bool PupilCompatible = new PupilProfileRepository(this.context).ExistsRow(GlobalConstants.PUPIL_SCHEMA, "PupilProfile",
                  new Dictionary<string, object>()
                {
                    {"PupilProfileID",ParticularPupil.PupilID},
                    {"CurrentSchoolID",SchoolID}
                }, null);
            if (!PupilCompatible)
            {
                throw new BusinessException("Common_Validate_NotCompatible");
            }
            //AcademicYearBusiness.IsCurrentYear(AcademicYearID) = FALSE => Năm học không phải năm hiện tại            
            if (!AcademicYearBusiness.IsCurrentYear(ParticularPupil.AcademicYearID))
            {
                throw new BusinessException("Common_Validate_IsCurrentYear");
            }
            //PupilOfClass(PupilID).Status phải nhận các giá trị PUPIL_STATUS_STUDYING        
            IQueryable<PupilOfClass> ListPupilOfClass = PupilOfClassBusiness.All.Where(o => (o.PupilID == ParticularPupil.PupilID && o.ClassID == ParticularPupil.ClassID));

            PupilOfClass PupilOfClass = ListPupilOfClass.OrderByDescending(o => o.AssignedDate).FirstOrDefault();

            if (PupilOfClass.Status != GlobalConstants.PUPIL_STATUS_STUDYING)
            {
                throw new BusinessException("Common_Validate_Pupil_Status_Studying");
            }

            if (UtilsBusiness.HasHeadTeacherPermission(UserID, ParticularPupil.ClassID))
            {
                //this.ParticularPupilCharacteristicBusiness.Delete(ParticularPupilID, false);
                List<ParticularPupilTreatment> lstparticularPupilTreatment = new List<ParticularPupilTreatment>();
                Dictionary<string, object> searchInfo = new Dictionary<string, object>();
                searchInfo["ParticularPupilID"] = ParticularPupilID;
                searchInfo["PupilID"] = ParticularPupil.PupilID;
                lstparticularPupilTreatment = ParticularPupilTreatmentBusiness.SearchParticularPupilTreatment(searchInfo).ToList();
                foreach (ParticularPupilTreatment p in lstparticularPupilTreatment)
                {
                    this.ParticularPupilTreatmentBusiness.DeleteParticularPupilTreatment(UserID, p.ParticularPupilTreatmentID, SchoolID);
                }
                List<ParticularPupilCharacteristic> lstParticularPupilCharacteristic = new List<ParticularPupilCharacteristic>();
                Dictionary<string, object> searchParticularCha = new Dictionary<string, object>();
                searchParticularCha["ParticularPupilID"] = ParticularPupilID;
                lstParticularPupilCharacteristic = ParticularPupilCharacteristicBusiness.SearchByParticularPupil(ParticularPupilID, searchParticularCha).ToList();
                foreach (ParticularPupilCharacteristic ppc in lstParticularPupilCharacteristic)
                {
                    ParticularPupilCharacteristicBusiness.Delete(ppc.ParticularPupilCharacteristicID);
                }
                //particularPupilTreatment = ParticularPupilTreatmentBusiness.SearchParticularPupilTreatment(
                //this.ParticularPupilTreatmentBusiness.DeleteParticularPupilTreatment(UserID, ParticularPupilID, SchoolID);
                this.Delete(ParticularPupilID);
            }
        }
        #endregion

        #region Search
        private IQueryable<ParticularPupil> Search(IDictionary<string, object> SearchInfo)
        {
            int ParticularPupilID = Utils.GetInt(SearchInfo, "ParticularPupilID");
            int PupilID = Utils.GetInt(SearchInfo, "PupilID");
            int ClassID = Utils.GetInt(SearchInfo, "ClassID");
            int EducationLevelID = Utils.GetShort(SearchInfo, "EducationLevelID");
            int SchoolID = Utils.GetInt(SearchInfo, "SchoolID");
            int AcademicYearID = Utils.GetInt(SearchInfo, "AcademicYearID");
            DateTime? RealUpdatedDate = Utils.GetDateTime(SearchInfo, "RealUpdatedDate");
            string PupilCode = Utils.GetString(SearchInfo, "PupilCode");
            string FullName = Utils.GetString(SearchInfo, "FullName");
            int AppliedLevel = Utils.GetInt(SearchInfo, "AppliedLevel");
            List<int> lstPupilID = Utils.GetIntList(SearchInfo, "lstPupilID");

            IQueryable<ParticularPupil> Query = ParticularPupilRepository.All;
            if (ClassID != 0)
            {
                Query = from pp in Query
                        join cp in ClassProfileBusiness.All on pp.ClassID equals cp.ClassProfileID
                        where (!cp.IsActive.HasValue || (cp.IsActive.HasValue && cp.IsActive.Value))
                        && pp.ClassID == ClassID
                        select pp;
            }
            if (ParticularPupilID != 0)
            {
                Query = Query.Where(o => o.ParticularPupilID == ParticularPupilID);
            }
            if (PupilID != 0)
            {
                Query = Query.Where(o => o.PupilID == PupilID);
            }

            if (EducationLevelID != 0)
            {
                Query = Query.Where(o => o.EducationLevelID == EducationLevelID);
            }
            if (SchoolID != 0)
            {
                Query = Query.Where(o => o.SchoolID == SchoolID);
            }
            if (AcademicYearID != 0)
            {
                Query = Query.Where(o => o.AcademicYearID == AcademicYearID);
            }
            if (RealUpdatedDate.HasValue)
            {
                Query = Query.Where(o => o.UpdatedDate == RealUpdatedDate);
            }
            //if (Description.Trim().Length != 0)
            //{
            //    Query = Query.Where(o => o.Description.Contains(Description.ToLower()));
            //}
            if (PupilCode.Trim().Length != 0)
            {
                Query = Query.Where(o => o.PupilProfile.PupilCode.Contains(PupilCode.ToLower()));
            }
            if (FullName.Trim().Length != 0)
            {
                Query = Query.Where(o => o.PupilProfile.FullName.Contains(FullName.ToLower()));
            }
            if (AppliedLevel != 0)
            {
                Query = Query.Where(o => o.EducationLevel.Grade == AppliedLevel);
            }
            if (lstPupilID.Count() > 0)
            {
                Query = Query.Where(x => lstPupilID.Contains(x.PupilID));           
            }

            return Query;
        }
        #endregion

        #region Search by School
        public IQueryable<ParticularPupil> SearchBySchool(int SchoolID, IDictionary<string, object> dic)
        {
            if (SchoolID == 0)
            {
                return null;
            }
            dic["SchoolID"] = SchoolID;
            return this.Search(dic);
        }
        #endregion

    }
}
