﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SMAS.Models.Models;
using SMAS.Business.Common;
using SMAS.DAL.Repository;
using SMAS.DAL.IRepository;
using System.IO;
using SMAS.VTUtils.Excel.Export;
using SMAS.Business.BusinessObject;

namespace SMAS.Business.Business
{
    public partial class GraduationApprovalBusiness
    {
        #region InsertData
        public string CreateOrUpdateGraduationApproval(GraduationApproval GraduationApprovalItem)
        {
            string status = string.Empty;
            if (GraduationApprovalItem == null)
            {
                status = "Graduation_Stubmit_Error";
            }
            //this.context.Configuration.AutoDetectChangesEnabled = false;
            //this.context.Configuration.ValidateOnSaveEnabled = false;
            GraduationApprovalBusiness.SetAutoDetectChangesEnabled(false);
            var objApproval = GraduationApprovalBusiness.All.Where(x => x.Year == GraduationApprovalItem.Year
                                                                        && x.AcademicYearID == GraduationApprovalItem.AcademicYearID
                                                                        && x.SchoolID == GraduationApprovalItem.SchoolID)
                                                                        .ToList();

            ValidationMetadata.ValidateObject(GraduationApprovalItem);
            //Update
            if (objApproval != null && objApproval.Count > 0)
            {
                objApproval[0].Status = GraduationApprovalItem.Status;
                objApproval[0].ModifiedDate = DateTime.Now;
                base.Update(objApproval[0]);
                //Cancel
                if (objApproval[0].Status == SystemParamsInFile.GRADUCATION_STATUS_CANCEL || objApproval[0].Status == -1)
                    status = "Graduation_Cancel_Success";
                else //Update row data => Sent Approval
                    status = "Graduation_Stubmit_Success";
            }
            else
            {
                //Create
                GraduationApprovalItem.CreatedDate = DateTime.Now;
                base.Insert(GraduationApprovalItem);
                status = "Graduation_Stubmit_Success";
            }
            //this.context.Configuration.AutoDetectChangesEnabled = true;
            //this.context.Configuration.ValidateOnSaveEnabled = true;
            base.Save();
            GraduationApprovalBusiness.SetAutoDetectChangesEnabled(true);
            return status;
        }
        #endregion
       

        public void SaveBySubSuperVising(List<GraduationApproval> lstData, Dictionary<string, object> dic)
        {
            int year = Utils.GetInt(dic, "Year");
            bool isCancel = Utils.GetBool(dic, "IsCancel");
            if (lstData.Count > 0)
            {
                List<int> lstSchoolID = lstData.Select(X => X.SchoolID).ToList();
                List<GraduationApproval> lstDB = GraduationApprovalBusiness.All.Where(x => x.Year == year && lstSchoolID.Contains(x.SchoolID)).ToList();
                GraduationApproval objGA = null;
                foreach (var item in lstData)
                {
                    objGA = lstDB.Where(x => x.SchoolID == item.SchoolID && x.AcademicYearID == item.AcademicYearID && x.Year == item.Year).FirstOrDefault();
                    if (objGA != null && isCancel == false) // Xét duyệt
                    {
                        if (objGA.Status != 2)
                        {
                            objGA.ModifiedDate = DateTime.Now;
                        }
                        objGA.Status = 2;
                        objGA.UserApprove = item.UserApprove;                        
                        objGA.Note = item.Note;
                    }
                    else if (objGA != null && isCancel == true)
                    {
                        if (objGA.Status != 3)
                        {
                            objGA.ModifiedDate = DateTime.Now;
                        }
                        objGA.Status = 3;
                        objGA.UserApprove = item.UserApprove;                      
                        objGA.Note = item.Note;
                    }
                }
                this.Save();
            }

        }

        public Stream ExportApproveGraduationProfile(IVTWorkbook oBook, List<PupilGraduationBO> listApprovePupil, List<PupilGraduationBO> listApprovePupilPriority,
                                                        SchoolProfile objSP, AcademicYear acaYear, Dictionary<string, object> dic)
        {
            IVTWorksheet oSheetBia = oBook.GetSheet(1);
            IVTWorksheet oSheetMau1 = oBook.GetSheet(2);
            IVTWorksheet oSheetMau2 = oBook.GetSheet(3);
            IVTWorksheet oSheetMau5 = oBook.GetSheet(4);
            IVTWorksheet oSheetRang1 = oBook.GetSheet(5);
            IVTWorksheet oSheetRang2 = oBook.GetSheet(6);
            int classID = Utils.GetInt(dic, "ClassID");
            var totalGraduation = listApprovePupil.Count();
            var totalGraduationApproved = listApprovePupil.Where(x => x.GraduationGrade != null && x.GraduationGrade > 0).Count();
            var totalGraduationFemale = listApprovePupil.Where(x => x.Genre == SystemParamsInFile.GENRE_FEMALE).Count();
            var totalGraduationApproveFemale = listApprovePupil.Where(x => x.Genre == SystemParamsInFile.GENRE_FEMALE && x.GraduationGrade != null && x.GraduationGrade > 0).Count();
            var totalGraduationEnthic = listApprovePupil.Where(x => x.EthnicID != null && x.EthnicID != SystemParamsInFile.ETHNIC_ID_KINH).Count();
            var totalGraduationApproveEnthic = listApprovePupil.Where(x => x.EthnicID != null 
                && x.EthnicID != SystemParamsInFile.ETHNIC_ID_KINH
                && x.EthnicID != SystemParamsInFile.ETHNIC_ID_NN
                && x.GraduationGrade != null && x.GraduationGrade > 0).Count();
            var totalExcelent = 0;
            var totalGood = 0;
            var totalNormal = 0;
            var totalGraduationPriority = listApprovePupilPriority.Count();
            var totalGraduationApprovedPriority = listApprovePupilPriority.Where(x => x.GraduationGrade != null && x.GraduationGrade > 0).Count();
            var totalGraduationFemalePriority = listApprovePupilPriority.Where(x => x.Genre == SystemParamsInFile.GENRE_FEMALE).Count();
            var totalGraduationApproveFemalePriority = listApprovePupilPriority.Where(x => x.Genre == SystemParamsInFile.GENRE_FEMALE && x.GraduationGrade != null && x.GraduationGrade > 0).Count();
            var totalGraduationEnthicPriority = listApprovePupilPriority.Where(x => x.EthnicID != null 
                && x.EthnicID != SystemParamsInFile.ETHNIC_ID_KINH
                && x.EthnicID != SystemParamsInFile.ETHNIC_ID_NN
                ).Count();
            var totalGraduationApproveEnthicPriority = listApprovePupilPriority.Where(x => x.EthnicID != null 
                && x.EthnicID != SystemParamsInFile.ETHNIC_ID_KINH
                && x.EthnicID != SystemParamsInFile.ETHNIC_ID_NN
                && x.GraduationGrade != null && x.GraduationGrade > 0).Count();
            var totalExcelentPriority = 0;
            var totalGoodPriority = 0;
            var totalNormalPriority = 0;
            //var objGA = listApprovePupil.FirstOrDefault();
            
            string SubName = "";
            var schoolProfile = SchoolProfileBusiness.Find(objSP.SchoolProfileID);
            var subperlst = SupervisingDeptBusiness.All.Where(x => x.HierachyLevel == 5 && x.SupervisingDeptID == schoolProfile.SupervisingDeptID && x.DistrictID == schoolProfile.DistrictID).FirstOrDefault();
            if (subperlst != null)
            {
                SubName = subperlst.SupervisingDeptName;
            }
            else
            {
                var SubSuperVising = SupervisingDeptBusiness.All.Where(x => x.ParentID == schoolProfile.SupervisingDeptID && x.DistrictID == schoolProfile.DistrictID).FirstOrDefault();
                if (SubSuperVising != null)
                {
                    SubName = SubSuperVising.SupervisingDeptName;
                }
            }

            #region // Fill sheet bia
            string superVis = string.Format("{0} - {1}", SubName.ToUpper(), objSP.Province.ProvinceName.ToUpper());
            oSheetBia.SetCellValue("A3", superVis);
            oSheetBia.SetCellValue("A5", string.Format("NĂM HỌC {0}", acaYear.DisplayTitle));
            oSheetBia.SetCellValue("A12", objSP.SchoolName.ToLower().Contains("trường") ? objSP.SchoolName.Replace("Trường", string.Empty) : objSP.SchoolName);
            #endregion

            #region // fill shett mau 1

            IVTRange rangMau1 = oSheetRang1.GetRange("A2", "M11");

            int startRow = 10;
            PupilGraduationBO objBO = null;
            int index = 0;
            oSheetMau1.SetCellValue(2, 1, SubName.ToUpper());
            oSheetMau1.SetCellValue(3, 1, "HỘI ĐỒNG XÉT CÔNG NHẬN TỐT NGHIỆP " + objSP == null ? "" : objSP.SchoolName.ToUpper());
            for (int i = 0; i < listApprovePupil.Count; i++)
            {
                index++;
                objBO = listApprovePupil[i];
                ComboObjectGraduation gg = objBO.GraduationGrade.HasValue ? GraduationGrade().Where(v => v.key == objBO.GraduationGrade.Value.ToString()).SingleOrDefault() : null;
                totalExcelent = gg != null && gg.key == SystemParamsInFile.GRADUATION_GRADE_EXCELLENT.ToString() ? totalExcelent + 1 : totalExcelent;
                totalGood = gg != null && gg.key == SystemParamsInFile.GRADUATION_GRADE_GOOD.ToString() ? totalGood + 1 : totalGood;
                totalNormal = gg != null && gg.key == SystemParamsInFile.GRADUATION_GRADE_FAIR.ToString() ? totalNormal + 1 : totalNormal;
                oSheetMau1.SetCellValue("A" + startRow, index);
                oSheetMau1.SetCellValue("B" + startRow, objBO.FullName);
                oSheetMau1.SetCellValue("C" + startRow, objBO.BirthDate.Day);
                oSheetMau1.SetCellValue("D" + startRow, objBO.BirthDate.Month);
                oSheetMau1.SetCellValue("E" + startRow, objBO.BirthDate.Year);
                oSheetMau1.SetCellValue("G" + startRow, objBO.BirthPlace);
                oSheetMau1.SetCellValue("H" + startRow, objBO.Genre == SystemParamsInFile.GENRE_MALE ? "Nam" : "Nữ");
                oSheetMau1.SetCellValue("I" + startRow, objBO.EthnicName);
                oSheetMau1.SetCellValue("J" + startRow, objBO.CapacityLevel);
                oSheetMau1.SetCellValue("K" + startRow, objBO.ConductLevel);
                oSheetMau1.SetCellValue("L" + startRow, string.Empty);
                oSheetMau1.SetCellValue("M" + startRow, gg != null ? gg.value : string.Empty);
                //oSheetMau1.SetCellValue("N" + startRow, objBO.Notification); 
                startRow++;
            }
            oSheetRang1.SetCellValue("B3", string.Format("Số học sinh được công nhận tốt nghiệp: {0}", totalExcelent + totalGood + totalNormal));
            oSheetRang1.SetCellValue("B4", string.Format("Bằng chữ: {0}", totalGraduation != null ? Utils.ConvertNumberToText(totalExcelent + totalGood + totalNormal) : string.Empty));
            oSheetRang1.SetCellValue("B7", string.Format("Dân tộc TN: {0} Nữ TN: {1}", totalGraduationApproveEnthic, totalGraduationApproveFemale));
            oSheetRang1.SetCellValue("B8", string.Format("Xếp loại Giỏi: {0} Khá: {1} TB: {2}", totalExcelent, totalGood, totalNormal));
            oSheetRang1.SetCellValue("B9", string.Format("{0}, ngày {1} tháng {2} năm {3}", objSP.District.DistrictName, 
                                                                DateTime.Now.Day,  DateTime.Now.Month, DateTime.Now.Year));
            oSheetRang1.SetCellValue("J9", string.Format("{0}, ngày {1} tháng {2} năm {3}", objSP.District.DistrictName,
                                                                DateTime.Now.Day, DateTime.Now.Month, DateTime.Now.Year));
            oSheetMau1.GetRange(10, 1, startRow - 1, 14).SetBorder(VTBorderStyle.Solid, VTBorderWeight.Thin, VTBorderIndex.All);
            oSheetMau1.GetRange(10, 1, startRow - 1, 1).SetHAlign(VTHAlign.xlHAlignRight);
            // 
            oSheetMau1.CopyPaste(rangMau1, startRow, 1);
            #endregion

            #region // fill shett mau 2
            IVTRange rangMau2 = oSheetRang2.GetRange("A2", "M11");
            int startRow2 = 11;
            PupilGraduationBO objBO2 = null;
            int index2 = 0;
            oSheetMau2.SetCellValue(2, 1, SubName.ToUpper());
            oSheetMau2.SetCellValue(3, 1, "HỘI ĐỒNG XÉT CÔNG NHẬN TỐT NGHIỆP " + objSP == null ? "" : objSP.SchoolName.ToUpper());
            for (int i = 0; i < listApprovePupilPriority.Count; i++)
            {
                index2++;
                objBO2 = listApprovePupilPriority[i];
                ComboObjectGraduation gg = objBO2.GraduationGrade.HasValue ? GraduationGrade().Where(v => v.key == objBO2.GraduationGrade.Value.ToString()).SingleOrDefault() : null;
                totalExcelentPriority = gg != null && gg.key == SystemParamsInFile.GRADUATION_GRADE_EXCELLENT.ToString() ? totalExcelentPriority + 1 : totalExcelentPriority;
                totalGoodPriority = gg != null && gg.key == SystemParamsInFile.GRADUATION_GRADE_GOOD.ToString() ? totalGoodPriority + 1 : totalGoodPriority;
                totalNormalPriority = gg != null && gg.key == SystemParamsInFile.GRADUATION_GRADE_FAIR.ToString() ? totalNormalPriority + 1 : totalNormalPriority;
                oSheetMau2.SetCellValue("A" + startRow2, index2);
                oSheetMau2.SetCellValue("B" + startRow2, objBO2.FullName);
                oSheetMau2.SetCellValue("C" + startRow2, objBO2.BirthDate.Day);
                oSheetMau2.SetCellValue("D" + startRow2, objBO2.BirthDate.Month);
                oSheetMau2.SetCellValue("E" + startRow2, objBO2.BirthDate.Year);
                oSheetMau2.SetCellValue("G" + startRow2, objBO2.BirthPlace);
                oSheetMau2.SetCellValue("H" + startRow2, objBO2.Genre == SystemParamsInFile.GENRE_MALE ? "Nam" : "Nữ");
                oSheetMau2.SetCellValue("I" + startRow2, objBO2.EthnicName);
                oSheetMau2.SetCellValue("J" + startRow2, objBO2.CapacityLevel);
                oSheetMau2.SetCellValue("K" + startRow2, objBO2.ConductLevel);
                oSheetMau2.SetCellValue("L" + startRow2, objBO2.PriorityReason);
                oSheetMau2.SetCellValue("M" + startRow2, gg != null ? gg.value : string.Empty);
                //oSheetMau2.SetCellValue("N" + startRow, objBO2.Notification); 
                startRow2++;
            }
            oSheetRang2.SetCellValue("B3", string.Format("Số học sinh được công nhận tốt nghiệp: {0}", totalExcelentPriority + totalGoodPriority + totalNormalPriority));
            oSheetRang2.SetCellValue("B4", string.Format("Bằng chữ: {0}", totalGraduationPriority != null ? Utils.ConvertNumberToText(totalExcelentPriority + totalGoodPriority + totalNormalPriority) : string.Empty));
            oSheetRang2.SetCellValue("B7", string.Format("Dân tộc TN: {0} Nữ TN: {1}", totalGraduationApproveEnthicPriority, totalGraduationApproveFemalePriority));
            oSheetRang2.SetCellValue("B8", string.Format("Xếp loại Giỏi: {0} Khá: {1} TB: {2}", totalExcelentPriority, totalGoodPriority, totalNormalPriority));
            oSheetRang2.SetCellValue("B9", string.Format("{0}, ngày {1} tháng {2} năm {3}", objSP.District.DistrictName,
                                                                DateTime.Now.Day, DateTime.Now.Month, DateTime.Now.Year));
            oSheetRang2.SetCellValue("J9", string.Format("{0}, ngày {1} tháng {2} năm {3}", objSP.District.DistrictName,
                                                                DateTime.Now.Day, DateTime.Now.Month, DateTime.Now.Year));
            // 
            oSheetMau2.GetRange(11, 1, startRow2 - 1, 14).SetBorder(VTBorderStyle.Solid, VTBorderWeight.Thin, VTBorderIndex.All);
            oSheetMau2.GetRange(11, 1, startRow2 - 1, 1).SetHAlign(VTHAlign.xlHAlignRight);
            oSheetMau2.CopyPaste(rangMau2, startRow2, 1);
            #endregion

            #region mau 5
            Dictionary<string, object> SearchInfo = new Dictionary<string, object> { 
                    { "ProvinceID", objSP.ProvinceID },
                    { "DistrictID", objSP.DistrictID},
                    {"Year", acaYear.Year},
                    {"SchoolID", objSP.SchoolProfileID}                   
                };

            var lstData = GetDataForExportExcel(SearchInfo);
            if (classID != 0) 
            {
                lstData.Where(x => x.classID == classID);
            }

            startRow = 9;
            int stt = 1;
            IVTWorksheet sheet = oBook.GetSheet(7);
            IVTRange rang1 = sheet.GetRange("A3", "B4");
            IVTRange rang3 = sheet.GetRange("U3", "Z4"); 
            foreach (var item in lstData)
            {
                sheet.SetCellValue(startRow, 1, stt);
                sheet.SetCellValue(startRow, 2, item.schoolName);
                sheet.SetCellValue(startRow, 3, item.expectedPupilSum);
                sheet.SetCellValue(startRow, 4, item.expectedFemalePupilSum);
                sheet.SetCellValue(startRow, 5, "=IF(D" + startRow + "=0,0,ROUND(D" + startRow + "*100/C" + startRow + ",2))");
                sheet.SetCellValue(startRow, 6, item.expectedRegionPupilSum);
                sheet.SetCellValue(startRow, 7, "=IF(F" + startRow + "=0,0,ROUND(F" + startRow + "*100/C" + startRow + ",2))");
                sheet.SetCellValue(startRow, 8, 0);
                sheet.SetCellValue(startRow, 9, 0);
                sheet.SetCellValue(startRow, 10, item.achievementPupilSum);
                sheet.SetCellValue(startRow, 11, "=IF(J" + startRow + "=0,0,ROUND(J" + startRow + "*100/C" + startRow + ",2))");
                sheet.SetCellValue(startRow, 12, item.achievementFemalePupilSum);
                sheet.SetCellValue(startRow, 13, "=IF(L" + startRow + "=0,0,ROUND(L" + startRow + "*100/C" + startRow + ",2))");
                sheet.SetCellValue(startRow, 14, item.achievementRegionPupilSum);
                sheet.SetCellValue(startRow, 15, "=IF(N" + startRow + "=0,0,ROUND(N" + startRow + "*100/C" + startRow + ",2))");
                sheet.SetCellValue(startRow, 16, item.goodGraduation);
                sheet.SetCellValue(startRow, 17, "=IF(P" + startRow + "=0,0,ROUND(P" + startRow + "*100/C" + startRow + ",2))");
                sheet.SetCellValue(startRow, 18, item.normalGraduation);
                sheet.SetCellValue(startRow, 19, "=IF(R" + startRow + "=0,0,ROUND(R" + startRow + "*100/C" + startRow + ",2))");
                sheet.SetCellValue(startRow, 20, item.note);
                oSheetMau5.SetRowHeight(startRow, 25);
                startRow++;
                stt++;
            }          

            sheet.GetRange(9, 1, startRow - 1, 20).SetBorder(VTBorderStyle.Solid, VTBorderWeight.Thin, VTBorderIndex.All);
            sheet.GetRange(9, 20, startRow - 1, 20).WrapText();
            sheet.GetRange(9, 1, startRow - 1, 1).SetHAlign(VTHAlign.xlHAlignCenter);
            sheet.GetRange(9, 1, startRow - 1, 1).SetHAlignVer(NativeExcel.XlVAlign.xlVAlignCenter);
            sheet.GetRange(9, 3, startRow - 1, 20).SetHAlign(VTHAlign.xlHAlignCenter);
            sheet.GetRange(9, 3, startRow - 1, 20).SetHAlignVer(NativeExcel.XlVAlign.xlVAlignCenter);
            sheet.SetFontName("Times New Roman", 10);           
            IVTRange rang2 = sheet.GetRange("A9", "T" + startRow + 1);
            oSheetMau5.SetCellValue(2, 1, SubName.ToUpper());
            oSheetMau5.SetCellValue(3, 1, objSP == null ? "" : objSP.SchoolName.ToUpper());
            oSheetMau5.CopyPaste(rang2, 9, 1);
            oSheetMau5.CopyPaste(rang1, startRow + 2, 1);
            oSheetMau5.CopyPaste(rang3, startRow + 2, 15);
            oSheetMau5.SetCellValue("O" + (startRow + 1), string.Format("{0}, ngày {1} tháng {2} năm {3}", objSP.District.DistrictName,
                                                                DateTime.Now.Day, DateTime.Now.Month, DateTime.Now.Year));
            
            #endregion

            oSheetRang1.Delete();
            oSheetRang2.Delete();
            sheet.Delete();

            return oBook.ToStream();
        }

        public IQueryable<GraduationApprovalViewModel> GetDataForExportExcel(Dictionary<string, object> dic)
        {

            int schoolID = Utils.GetInt(dic, "SchoolID");
            int year = Utils.GetInt(dic, "Year");
            int status = Utils.GetInt(dic, "Status");
            int provinceID = Utils.GetInt(dic, "ProvinceID");
            int districtID = Utils.GetInt(dic, "DistrictID");

            IDictionary<string, object> SearchInfo = new Dictionary<string, object> { 
                    { "ProvinceID", provinceID },
                    { "DistrictID", districtID}
                };
            IQueryable<SchoolProfile> lstSP = SchoolProfileBusiness.Search(SearchInfo).Where(x => x.EducationGrade == SystemParamsInFile.SCHOOL_EDUCATION_GRADE_SECONDARY ||
                                                                                    x.EducationGrade == SystemParamsInFile.SCHOOL_EDUCATION_GRADE_PRIMARY_SECONDARY ||
                                                                                    x.EducationGrade == SystemParamsInFile.SCHOOL_EDUCATION_GRADE_SECONDARY_TERTIARY ||
                                                                                    x.EducationGrade == SystemParamsInFile.SCHOOL_EDUCATION_GRADE_ALL ||
                                                                                    x.EducationGrade == SystemParamsInFile.SCHOOL_EDUCATION_GRADE_BCIS);

            if (schoolID != 0)
            {
                lstSP = lstSP.Where(x => x.SchoolProfileID == schoolID);
            }

            IQueryable<GraduationApprovalViewModel> model = (from sp in lstSP.Where(x => x.IsActive == true)
                                                             join ac in AcademicYearBusiness.All.Where(x => x.IsActive == true && x.Year == year) on sp.SchoolProfileID equals ac.SchoolID
                                                             join pos in PupilOfSchoolBusiness.All on sp.SchoolProfileID equals pos.SchoolID
                                                             join poc in PupilOfClassBusiness.All.Where(x => (x.Status == 1 || x.Status == 2) && x.ClassProfile.IsActive == true && x.PupilProfile.IsActive == true)
                                                                      on new { pos.PupilID, pos.SchoolID, ac.AcademicYearID } equals new { poc.PupilID, poc.SchoolID, poc.AcademicYearID }
                                                             join pg in PupilGraduationBusiness.All.Where(x => x.Status == 1)
                                                                    on new { p1 = sp.SchoolProfileID, p2 = ac.AcademicYearID, p3 = poc.PupilID, p4 = poc.ClassID } equals new { p1 = pg.SchoolID, p2 = pg.AcademicYearID, p3 = pg.PupilID, p4 = pg.ClassID } into ps
                                                             from pg in ps.DefaultIfEmpty()
                                                             //join ga in GraduationApprovalBusiness.All
                                                             //       on new { p1 = sp.SchoolProfileID, p2 = ac.AcademicYearID, p3 = ac.Year } equals new { p1 = ga.SchoolID, p2 = ga.AcademicYearID, p3 = ga.Year } into ap
                                                             //from ga in ap.DefaultIfEmpty()
                                                             group new { sp, pg, poc, ac } by sp.SchoolProfileID into result
                                                             select new GraduationApprovalViewModel
                                                             {
                                                                 schoolID = result.Key,
                                                                 academicYearID = result.FirstOrDefault().ac.AcademicYearID,
                                                                 year = result.FirstOrDefault().ac.Year,
                                                                 classID = result.Select(x=>x.poc.ClassID).FirstOrDefault(),
                                                                 schoolName = result.FirstOrDefault().sp.SchoolName,
                                                                 expectedPupilSum = result.Where(x => x.poc.ClassProfile.EducationLevelID == 9).Count(),
                                                                 expectedFemalePupilSum = result.Where(x => x.poc.PupilProfile.Genre == 0 && x.poc.ClassProfile.EducationLevelID == 9).Count(),
                                                                 expectedRegionPupilSum = result.Where(x => x.poc.PupilProfile.EthnicID != 1 && x.poc.PupilProfile.EthnicID != 79 && x.poc.ClassProfile.EducationLevelID == 9).Count(),
                                                                 achievementPupilSum = result.Where(x => x.pg.SchoolID != null && x.poc.ClassProfile.EducationLevelID == 9).Count(),
                                                                 achievementFemalePupilSum = result.Where(x => x.pg.SchoolID != null && x.poc.ClassProfile.EducationLevelID == 9 && x.poc.PupilProfile.Genre == 0).Count(),
                                                                 achievementRegionPupilSum = result.Where(x => x.pg.SchoolID != null && x.poc.ClassProfile.EducationLevelID == 9 && x.poc.PupilProfile.EthnicID != 1 && x.poc.PupilProfile.EthnicID != 79).Count(),
                                                                 goodGraduation = result.Where(x => x.pg.SchoolID != null && x.poc.ClassProfile.EducationLevelID == 9 && x.pg.GraduationGrade == 1).Count(),
                                                                 normalGraduation = result.Where(x => x.pg.SchoolID != null && x.poc.ClassProfile.EducationLevelID == 9 && x.pg.GraduationGrade == 2).Count(),
                                                             }).OrderBy(o => o.schoolName.ToLower());

            model = (from sp in model
                     join ga in GraduationApprovalBusiness.All
                         on new { p1 = sp.schoolID, p2 = sp.academicYearID, p3 = sp.year } equals new { p1 = ga.SchoolID, p2 = ga.AcademicYearID, p3 = ga.Year } into ap
                     from ga in ap.DefaultIfEmpty()
                     select new
                     {
                         sp = sp,
                         ga = ga,
                     }).ToList().Select(o => new GraduationApprovalViewModel
                     {
                         schoolID = o.sp.schoolID,
                         academicYearID = o.sp.academicYearID,
                         classID = o.sp.classID,
                         schoolName = o.sp.schoolName,
                         expectedPupilSum = o.sp.expectedPupilSum,
                         expectedFemalePupilSum = o.sp.expectedFemalePupilSum,
                         expectedRegionPupilSum = o.sp.expectedRegionPupilSum,
                         achievementPupilSum = o.sp.achievementPupilSum,
                         achievementFemalePupilSum = o.sp.achievementFemalePupilSum,
                         achievementRegionPupilSum = o.sp.achievementRegionPupilSum,
                         goodGraduation = o.sp.goodGraduation,
                         normalGraduation = o.sp.normalGraduation,
                         status = o.ga != null ? o.ga.Status : -1,
                         userApprovalID = o.ga != null ? o.ga.UserApprove : -1,
                         date = o.ga != null ? o.ga.ModifiedDate : new DateTime(),
                         graduationApprovalID = o.ga != null ? o.ga.GraduationApprovalID : -1,
                         year = o.ga != null ? o.ga.Year : -1,
                     }).AsQueryable();

            if (status != 0)
            {
                switch (status)
                {
                    case 1: model = model.Where(x => x.status == -1);
                        break;
                    case 2: model = model.Where(x => x.status == 2);
                        break;
                    case 3: model = model.Where(x => x.status == 1);
                        break;
                    case 4: model = model.Where(x => x.status == 3);
                        break;
                }
            }

            return model;
        }

        public List<ComboObjectGraduation> GraduationGrade()
        {
            List<ComboObjectGraduation> listGraduationGrade = new List<ComboObjectGraduation>();
            listGraduationGrade.Add(new ComboObjectGraduation(SystemParamsInFile.GRADUATION_GRADE_EXCELLENT.ToString(), "Giỏi"));
            listGraduationGrade.Add(new ComboObjectGraduation(SystemParamsInFile.GRADUATION_GRADE_GOOD.ToString(), "Khá"));
            //listGraduationGrade.Add(new ComboObject(SystemParamsInFile.GRADUATION_GRADE_ABOVE_FAIR.ToString(), Res.Get("Graduation_Grade_Above_Fair")));
            listGraduationGrade.Add(new ComboObjectGraduation(SystemParamsInFile.GRADUATION_GRADE_FAIR.ToString(), "Trung bình"));
            return listGraduationGrade;
        }

    }

    public class GraduationApprovalViewModel
    {
        public int graduationApprovalID { get; set; }
        public int schoolID { get; set; }
        public string schoolName { get; set; }
        public int academicYearID { get; set; }
        public int expectedPupilSum { get; set; }
        public int expectedFemalePupilSum { get; set; }
        public int expectedRegionPupilSum { get; set; }
        public int expectedFreePupilSum { get; set; }
        public int achievementPupilSum { get; set; }
        public int achievementFemalePupilSum { get; set; }
        public int achievementRegionPupilSum { get; set; }
        public int goodGraduation { get; set; }
        public int normalGraduation { get; set; }
        public string note { get; set; }
        public string graduationApproStatus { get; set; }
        public int year { get; set; }
        public int status { get; set; }
        public int? userApprovalID { get; set; }
        public DateTime date { get; set; }
        public int? classID { get; set; }
    }

    public class ComboObjectGraduation
    {
        public string key { get; set; }
        public string value { get; set; }

        public ComboObjectGraduation(string key, string value)
        {
            this.key = key;
            this.value = value;
        }
        public ComboObjectGraduation()
        {
        }
    }

}
//Extension
