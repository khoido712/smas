﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Linq.Expressions;
using System.Web;
using log4net;
using SMAS.DAL.Repository;
using SMAS.DAL.IRepository;
using SMAS.Business.IBusiness;
using SMAS.Models.Models;
using SMAS.Business.Common;
using SMAS.Business.BusinessObject;
using System.Transactions;
using System.Text;
using System.IO;
using SMAS.VTUtils.Excel.Export;
using System.Threading.Tasks;
using Oracle.DataAccess.Client;
using SMAS.VTUtils.Log;
using System.Web.Mvc;
using SMAS.Business.BusinessExtensions;

namespace SMAS.Business.Business
{
    public partial class GrowthEvaluationBusiness
    {
        #region RatedComment

        #region Search
        public IQueryable<GrowthEvaluation> Search(IDictionary<string, object> dic)
        {
            int SchoolID = Utils.GetInt(dic, "SchoolID");
            int AcademicYearID = Utils.GetInt(dic, "AcademicYearID");
            int ClassID = Utils.GetInt(dic, "ClassID");
            int DeclareEvaluationGroupID = Utils.GetInt(dic, "EvaluationDevGroID");
            int DeclareEvaluationIndexID = Utils.GetInt(dic, "DeclareEvaluationIndexID");
            int PupilID = Utils.GetInt(dic, "PupilID");

            List<int> lstClassID = Utils.GetIntList(dic, "lstClassID");
            List<int> lstDeclareEvaluationGroupID = Utils.GetIntList(dic, "lstDeclareEvaluationGroupID");
            List<int> lstPupilID = Utils.GetIntList(dic, "lstPupilID");

            IQueryable<GrowthEvaluation> iqQuery = GrowthEvaluationBusiness.All;

            if (SchoolID != 0)
            {
                int PartionID = UtilsBusiness.GetPartionId(SchoolID);
                iqQuery = iqQuery.Where(p => p.SchoolID == SchoolID && p.LastDigitSchoolID == PartionID);
            }
            if (AcademicYearID != 0)
            {
                iqQuery = iqQuery.Where(p => p.AcademicYearID == AcademicYearID);
            }
            if (ClassID != 0)
            {
                iqQuery = iqQuery.Where(p => p.ClassID == ClassID);
            }
            if (DeclareEvaluationGroupID != 0)
            {
                iqQuery = iqQuery.Where(p => p.DeclareEvaluationGroupID == DeclareEvaluationGroupID);
            }
            if (DeclareEvaluationIndexID != 0)
            {
                iqQuery = iqQuery.Where(p => p.DeclareEvaluationIndexID == DeclareEvaluationIndexID);
            }
            if (PupilID != 0)
            {
                iqQuery = iqQuery.Where(p => p.PupilID == PupilID);
            }
            if (lstClassID.Count > 0)
            {
                iqQuery = iqQuery.Where(p => lstClassID.Contains(p.ClassID));
            }
            if (lstDeclareEvaluationGroupID.Count > 0)
            {
                iqQuery = iqQuery.Where(p => lstDeclareEvaluationGroupID.Contains(p.DeclareEvaluationGroupID));
            }
            if (lstPupilID.Count > 0)
            {
                iqQuery = iqQuery.Where(p => lstPupilID.Contains(p.PupilID));
            }
            return iqQuery;
        }
        #endregion
        public GrowthEvaluation GetObjectGrowEvaluation(int SchoolID, int AcademicYearID, int ClassID, int PupilID)
        {
            if (SchoolID != 0 && AcademicYearID != 0 && ClassID != 0 && PupilID != 0)
            {
                var result = GrowthEvaluationRepository.All.FirstOrDefault(x => x.SchoolID == SchoolID
                                                                            && x.AcademicYearID == AcademicYearID
                                                                            && x.ClassID == ClassID
                                                                            && x.PupilID == PupilID);
                if (result != null)
                    return result;
                return null;
            }

            return null;
        }
        #endregion

        public void InsertOrUpdateGrowthEvaluation(List<GrowthEvaluationBO> lstGrowthEvaluationBO, Dictionary<string, object> dic)
        {
            List<GrowthEvaluation> lstGrowthEvaluationDB = this.Search(dic).ToList();
            GrowthEvaluation objDB = null;
            GrowthEvaluation objInsertGE = null;
            GrowthEvaluationBO objGEBO = null;
            for (int i = 0; i < lstGrowthEvaluationBO.Count; i++)
            {
                objGEBO = lstGrowthEvaluationBO[i];
                objDB = lstGrowthEvaluationDB.Where(p => p.PupilID == objGEBO.PupilID && p.DeclareEvaluationIndexID == objGEBO.DeclareEvaluationIndexID).FirstOrDefault();
                if (objDB != null)
                {
                    // xóa khi import excel
                    if (objGEBO.EvaluationType == 0)
                    {
                        this.Delete(objDB.GrowthEvaluationID);
                        continue;
                    }

                    // cập nhật khi lưu
                    objDB.EvaluationType = objGEBO.EvaluationType;
                    objDB.ModifiedDate = DateTime.Now;
                    objDB.LogID = objGEBO.LogID;
                    this.Update(objDB);
                }
                else
                {
                    if (objGEBO.EvaluationType == 0)
                        continue;

                    objInsertGE = new GrowthEvaluation();
                    objInsertGE.SchoolID = objGEBO.SchoolID;
                    objInsertGE.AcademicYearID = objGEBO.AcademicYearID;
                    objInsertGE.ClassID = objGEBO.ClassID;
                    objInsertGE.PupilID = objGEBO.PupilID;
                    objInsertGE.DeclareEvaluationGroupID = objGEBO.DeclareEvaluationGroupID;
                    objInsertGE.DeclareEvaluationIndexID = objGEBO.DeclareEvaluationIndexID;
                    objInsertGE.EvaluationType = objGEBO.EvaluationType;
                    objInsertGE.LogID = objGEBO.LogID;
                    objInsertGE.LastDigitSchoolID = objGEBO.LastDigitSchoolID;
                    objInsertGE.CreateDate = DateTime.Now;
                    objInsertGE.ModifiedDate = DateTime.Now;
                    this.Insert(objInsertGE);
                }
            }
            this.Save();
        }

        public void DeleteCommentPupil(IDictionary<string, object> dic)
        {
            int SchoolID = Utils.GetInt(dic, "SchoolID");
            int AcademicYearID = Utils.GetInt(dic, "AcademicYearID");
            int ClassID = Utils.GetInt(dic, "ClassID");
            int DeclareEvaluationGroupID = Utils.GetInt(dic, "DeclareEvaluationGroupID");
            int PartionID = UtilsBusiness.GetPartionId(SchoolID);
            List<int> lstPupilID = Utils.GetIntList(dic, "lstPupilID");

            List<GrowthEvaluation> lstGrowthEvaluationDB = new List<GrowthEvaluation>();
            List<PupilOfClassBO> lstPOC = PupilOfClassBusiness.GetPupilInClass(ClassID)
                                                                .Where(p => lstPupilID.Contains(p.PupilID))
                                                                .Distinct().ToList();
            PupilOfClassBO objPOC = null;

            lstGrowthEvaluationDB = GrowthEvaluationBusiness.All
                .Where(x=>x.SchoolID == SchoolID
                    && x.AcademicYearID == AcademicYearID 
                    && x.LastDigitSchoolID == PartionID 
                    && x.ClassID == ClassID 
                    && x.DeclareEvaluationGroupID == DeclareEvaluationGroupID
                    && lstPupilID.Contains(x.PupilID)).ToList();

            foreach (var item in lstGrowthEvaluationDB)
            {
                objPOC = lstPOC.Where(p => p.PupilID == item.PupilID).FirstOrDefault();
                if (objPOC != null)
                {
                    this.Delete(item.GrowthEvaluationID);
                    objPOC = null;
                }
            }

            this.Save();
        }
    }
}
