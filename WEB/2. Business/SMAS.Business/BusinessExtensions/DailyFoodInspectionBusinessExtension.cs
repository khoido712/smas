﻿/**
 * The Viettel License
 *
 * Copyright 2012 Viettel Telecom. All rights reserved.
 * VIETTEL PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */

/** 
* @author  
* @version $Revision: $
*/

using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Linq.Expressions;
using System.Web;
using log4net;

using SMAS.DAL.Repository;
using SMAS.DAL.IRepository;
using SMAS.Business.IBusiness;
using SMAS.Models.Models;
using SMAS.Business.Common;

namespace SMAS.Business.Business
{
    public partial class DailyFoodInspectionBusiness
    {
        /// <summary>
        /// 
        /// </summary>
        /// <param name="lstDailyFootInspection"></param>
        /// <returns></returns>
        public void Insert(List<DailyFoodInspection> lstDailyFootInspection)
        {
            /*
             Tìm kiếm trong bảng DailyFoodInspection dfi
            Với điều kiện dfi.DishInspectionID = entity.DishInspectionID
            Xoá các bản ghi trả về.
             */
            foreach (var dailyFootInspection in lstDailyFootInspection)
            {
                var listDailyFoodInspection = DailyFoodInspectionRepository.All.Where(o => o.DishInspectionID == dailyFootInspection.DishInspectionID);
                if (listDailyFoodInspection.Count() > 0)
                {
                    DailyFoodInspectionRepository.DeleteAll(listDailyFoodInspection.ToList());
                }
            }
            //FoodID, FoodGroupID: not compatible(FoodCat)
            //foreach (var dailyFootInspection in lstDailyFootInspection)
            //{
            //    bool FoodCatCompatible = new FoodCatRepository(this.context).ExistsRow(GlobalConstants.LIST_SCHEMA, "FoodCat",
            //           new Dictionary<string, object>()
            //    {
            //        {"FoodID",dailyFootInspection.FoodID},
            //        {"FoodGroupID",dailyFootInspection.FoodGroupID}
            //    }, null);
            //    if (!FoodCatCompatible)
            //    {
            //        throw new BusinessException("Common_Validate_NotCompatible");
            //    }
            //}
            /*
             Tạo đối tượng DailyFoodInspection dfi
            dfi.weight = entity.weight
            dfi.PricePerOne = entity.PricePerOne
            dfi.DishInspectionID = entity.DishInspectionID
            dfi.FoodGroupID = entity.FoodGroupID
            dfi.FoodID = entity.FoodID
            Thực hiện insert vào bảng DailyFoodInspection
             */
            foreach (var dailyFoodInspection in lstDailyFootInspection)
            {
                DailyFoodInspection entity = new DailyFoodInspection();

                entity.Weight = dailyFoodInspection.Weight;
                entity.PricePerOnce = dailyFoodInspection.PricePerOnce;
                entity.DishInspectionID = dailyFoodInspection.DishInspectionID;
                entity.FoodGroupID = dailyFoodInspection.FoodGroupID;
                entity.FoodID = dailyFoodInspection.FoodID;
                DailyFoodInspectionRepository.Insert(entity);
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="SchoolID"></param>
        /// <param name="SearchInfo"></param>
        /// <returns></returns>
        public IQueryable<DailyFoodInspection> SearchBySchool(int SchoolID, IDictionary<string, object> SearchInfo)
        {
            if (SchoolID <= 0)
                return null;
            else
            {
                SearchInfo["SchoolID"] = SchoolID;
                return Search(SearchInfo);
            }
        }

        private IQueryable<DailyFoodInspection> Search(IDictionary<string, object> SearchInfo)
        {
            int SchoolID = Utils.GetInt(SearchInfo, "SchoolID");
            int DishInspectionID = Utils.GetInt(SearchInfo, "DishInspectionID");
            int FoodGroupID = Utils.GetInt(SearchInfo, "FoodGroupID");
            int FoodID = Utils.GetInt(SearchInfo, "FoodID");

            var query = from dfi in DailyFoodInspectionRepository.All
                        join di in DishInspectionRepository.All on dfi.DishInspectionID equals di.DishInspectionID
                        where dfi.DishInspectionID == DishInspectionID
                        select new { dfi, di };
            if (SchoolID > 0)
            {
                query = query.Where(o => o.di.SchoolID == SchoolID);
            }
            if (FoodGroupID > 0)
            {
                query = query.Where(o => o.dfi.FoodGroupID == FoodGroupID);
            }
            if (FoodID > 0)
            {
                query = query.Where(o => o.dfi.FoodID == FoodID);
            }
            return query.Select(o => o.dfi);
        }
    }
}