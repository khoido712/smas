﻿/**
 * The Viettel License
 *
 * Copyright 2012 Viettel Telecom. All rights reserved.
 * VIETTEL PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */

/** 
* @author  
* @version $Revision: $
*/

using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Linq.Expressions;
using System.Web;
using log4net;

using SMAS.DAL.Repository;
using SMAS.DAL.IRepository;
using SMAS.Business.IBusiness;
using SMAS.Models.Models;
using SMAS.Business.Common;
using SMAS.Business.BusinessObject;

namespace SMAS.Business.Business
{
    public partial class OverallTestBusiness
    {
        /// <summary>
        /// Thêm mới thông tin khám sức khỏe – Tổng quát
        /// author: namdv3
        /// </summary>
        /// <param name="entity"></param>
        /// <param name="entity1"></param>
        public void Insert(OverallTest overallTest, MonitoringBook monitoringBook)
        {
            ValidationMetadata.ValidateObject(overallTest);
            ValidationMetadata.ValidateObject(monitoringBook);

            int PupilID = monitoringBook.PupilID;
            int? HealthPeriodID = monitoringBook.HealthPeriodID;
            int SchoolID = monitoringBook.SchoolID;
            int AcademicYearID = monitoringBook.AcademicYearID;

            IDictionary<string, object> dic = new Dictionary<string, object>();
            dic["PupilID"] = PupilID;
            dic["HealthPeriodID"] = HealthPeriodID;
            dic["AcademicYearID"] = AcademicYearID;
            List<MonitoringBook> lstMonitoringBook = MonitoringBookBusiness.SearchBySchool(SchoolID, dic).ToList();
            if (lstMonitoringBook.Count == 0)
            {
                monitoringBook.OverallTests.Add(overallTest);
                monitoringBook.CreateDate = DateTime.Now;
                MonitoringBookBusiness.Insert(monitoringBook);
            }
            else
            {
                int monitoringBookID = lstMonitoringBook.FirstOrDefault().MonitoringBookID;
                if (!this.All.Any(u => u.IsActive == true && u.MonitoringBookID == monitoringBookID))
                {
                    overallTest.MonitoringBookID = monitoringBookID;
                    this.Insert(overallTest);
                }
                else
                {
                    throw new BusinessException("HealthTest_Label_OverallTestExisted");
                }
            }
        }

        /// <summary>
        /// Cập nhật thông tin khám sức khỏe – Tổng quát
        /// author: namdv3
        /// </summary>
        /// <param name="entity"></param>
        /// <param name="entity1"></param>
        public void Update(OverallTest overallTest, MonitoringBook monitoringBook)
        {
            ValidationMetadata.ValidateObject(overallTest);
            ValidationMetadata.ValidateObject(monitoringBook);

            MonitoringBook mb = MonitoringBookBusiness.Find(overallTest.MonitoringBookID);
            if (mb.OverallTests.Any(u => u.MonitoringBookID == overallTest.MonitoringBookID && u.OverallTestID != overallTest.OverallTestID))
            {
                throw new BusinessException("HealthTest_Label_OverallTestExisted");
            }
            else
            {
                MonitoringBookBusiness.Update(monitoringBook);
                this.Update(overallTest);
            }
        }

        /// <summary>
        /// Xóa thông tin khám sức khỏe – Tổng quát
        /// </summary>
        /// <param name="DentalTestID"></param>
        /// <param name="SchoolID"></param>
        public void Delete(int DentalTestID, int SchoolID)
        {
            //OverallTestID  => MonitoringBookID
            //Todo: namdv3 vs hieund9 validation data before delete
            OverallTest overallTest = this.Find(DentalTestID);
            if (overallTest != null)
            {
                //Thực hiện chuyển  IsActive=false, ModifiedDate=Sysdate với bản ghi tương ứng trong bảng OverallTest
                overallTest.IsActive = false;
                overallTest.ModifiedDate = DateTime.Now;
                this.Update(overallTest);
            }
            else
            {
                //Nếu không tồn tại bản ghi tương ứng DentalTestID thì return luôn
                return;
            }
        }

        /// <summary>
        /// Tìm kiếm thông tin khám sức khỏe –  Tổng quát
        /// author: namdv3
        /// </summary>
        /// <param name="SearchInfo"></param>
        /// <returns></returns>
        private IQueryable<OverallTest> Search(IDictionary<string, object> SearchInfo)
        {
            int OverallTestID = Utils.GetInt(SearchInfo, "OverallTestID");
            int OverallInternal = Utils.GetInt(SearchInfo, "OverallInternal");
            string HistoryYourSelf = Utils.GetString(SearchInfo, "HistoryYourSelf");
            string DescriptionOverallInternall = Utils.GetString(SearchInfo, "DescriptionOverallInternall");
            int OverallForeign = Utils.GetInt(SearchInfo, "OverallForeign");
            string DescriptionForeign = Utils.GetString(SearchInfo, "DescriptionForeign");
            int SkinDiseases = Utils.GetInt(SearchInfo, "SkinDiseases");
            string DescriptionSkinDiseases = Utils.GetString(SearchInfo, "DescriptionSkinDiseases");
            bool IsHeartDiseases = Utils.GetBool(SearchInfo, "IsHeartDiseases");
            bool IsBirthDefect = Utils.GetBool(SearchInfo, "IsBirthDefect");
            int EvaluationHealth = Utils.GetInt(SearchInfo, "EvaluationHealth");
            string RequireOfDoctor = Utils.GetString(SearchInfo, "RequireOfDoctor");
            string Doctor = Utils.GetString(SearchInfo, "Doctor");
            bool IsDredging = Utils.GetBool(SearchInfo, "IsDredging");
            bool IsActive = Utils.GetBool(SearchInfo, "IsActive");
            int MonitoringBookID = Utils.GetInt(SearchInfo, "MonitoringBookID");
            int SchoolID = Utils.GetInt(SearchInfo, "SchoolID");
            int AcademicYearID = Utils.GetInt(SearchInfo, "AcademicYearID");
            int PupilID = Utils.GetInt(SearchInfo, "PupilID");
            int ClassID = Utils.GetInt(SearchInfo, "ClassID");
            int HealthPeriodID = Utils.GetInt(SearchInfo, "HealthPeriodID");
            string UnitName = Utils.GetString(SearchInfo, "UnitName");
            DateTime? MonitoringDate = Utils.GetDateTime(SearchInfo, "MonitoringDate");
            string FullName = Utils.GetString(SearchInfo, "FullName");
            int EducationLevelID = Utils.GetInt(SearchInfo,"EducationLevelID");
            int Grade = Utils.GetInt(SearchInfo, "EducationGrade");

            /*Thực hiện tìm kiếm trong bảng OverallTest ot join MonitoringBook mb (Theo MonitoringBookID) 
             * join PupilProfile pp (theo PupilID) theo các thông tin trong SearchInfo:*/
            var query = from ot in OverallTestRepository.All
                        join mb in MonitoringBookRepository.All on ot.MonitoringBookID equals mb.MonitoringBookID
                        join pp in PupilProfileRepository.All on mb.PupilID equals pp.PupilProfileID
                        select new { ot, mb, pp };
            if (Grade > 0)
            {
                query = from q in query
                        join e in EducationLevelBusiness.All on q.mb.EducationLevelID equals e.EducationLevelID
                        where e.Grade == Grade
                        select q;
            }
            if (EducationLevelID != 0)
            {
                query = query.Where(p=>p.mb.EducationLevelID == EducationLevelID);
            }
            if (OverallTestID > 0)
            {
                query = query.Where(o => o.ot.OverallTestID == OverallTestID);
            }
            if (HistoryYourSelf != "")
            {
                query = query.Where(o => o.ot.HistoryYourSelf.Contains(HistoryYourSelf));
            }
            if (OverallInternal > 0)
            {
                query = query.Where(o => o.ot.OverallInternal == OverallInternal);
            }
            if (DescriptionOverallInternall != "")
            {
                query = query.Where(o => o.ot.DescriptionOverallInternall.Contains(DescriptionOverallInternall));
            }
            if (OverallForeign > 0)
            {
                query = query.Where(o => o.ot.OverallForeign == OverallForeign);
            }
            if (DescriptionForeign != "")
            {
                query = query.Where(o => o.ot.DescriptionForeign.Contains(DescriptionForeign));
            }
            if (SkinDiseases > 0 && SkinDiseases != 2)
            {
                query = query.Where(o => o.ot.SkinDiseases == SkinDiseases);
            }
            if (DescriptionSkinDiseases != "")
            {
                query = query.Where(o => o.ot.DescriptionSkinDiseases.Contains(DescriptionSkinDiseases));
            }
            if (IsHeartDiseases)
            {
                query = query.Where(o => o.ot.IsHeartDiseases == IsHeartDiseases);
            }
            if (IsBirthDefect)
            {
                query = query.Where(o => o.ot.IsBirthDefect == IsBirthDefect);
            }

            if (EvaluationHealth > 0)
            {
                query = query.Where(o => o.ot.EvaluationHealth == EvaluationHealth);
            }
            if (RequireOfDoctor != "")
            {
                query = query.Where(o => o.ot.RequireOfDoctor.Contains(RequireOfDoctor));
            }
            if (Doctor != "")
            {
                query = query.Where(o => o.ot.Doctor.Contains(Doctor));
            }
            if (IsDredging)
            {
                query = query.Where(o => o.ot.IsDredging == IsDredging);
            }
            //if (IsActive)
            //{
            //    query = query.Where(o => o.ot.IsActive == true);
            //}
            if (MonitoringBookID > 0)
            {
                query = query.Where(o => o.ot.MonitoringBookID == MonitoringBookID);
            }
            if (SchoolID > 0)
            {
                query = query.Where(o => o.mb.SchoolID == SchoolID);
            }
            if (AcademicYearID > 0)
            {
                query = query.Where(o => o.mb.AcademicYearID == AcademicYearID);
            }
            if (PupilID > 0)
            {
                query = query.Where(o => o.mb.PupilID == PupilID);
            }
            if (ClassID > 0)
            {
                query = query.Where(o => o.mb.ClassID == ClassID);
            }
            if (HealthPeriodID > 0)
            {
                query = query.Where(o => o.mb.HealthPeriodID == HealthPeriodID);
            }
            if (UnitName != "")
            {
                query = query.Where(o => o.mb.UnitName.Contains(UnitName));
            }
            if (MonitoringDate.HasValue)
            {
                query = query.Where(o => o.mb.MonitoringDate == MonitoringDate);
            }
            if (FullName != "")
            {
                query = query.Where(o => o.pp.FullName.ToLower().Contains(FullName.ToLower()));
            }
            return query.Select(o => o.ot);
        }

        /// <summary>
        /// Tìm kiếm thông tin khám sức khỏe –  Tổng quát
        /// author: namdv3
        /// </summary>
        /// <param name="SchoolID"></param>
        /// <param name="SearchInfo"></param>
        /// <returns></returns>
        public IQueryable<OverallTest> SearchBySchool(int SchoolID, IDictionary<string, object> SearchInfo)
        {
            //Nếu  SchoolID = 0 -> trả về NULL
            if (SchoolID == 0)
            {
                return null;
            }
            else
            {
                SearchInfo["SchoolID"] = SchoolID;
                return Search(SearchInfo);
            }
        }

        /// <summary>
        /// Tìm kiếm thông tin khám sức khỏe theo học sinh – Tổng quát
        /// author: namdv3
        /// </summary>
        /// <param name="AcademicYearID">Năm học</param>
        /// <param name="SchoolID">Trường học</param>
        /// <param name="PupilID">Học sinh</param>
        /// <param name="ClassID">Lớp học</param>
        /// <returns></returns>
        public IQueryable<OverallTestBO> SearchByPupil(int AcademicYearID, int SchoolID, int PupilID, int ClassID)
        {
            //B1: Khởi tạo IQueryable<MonitoringBook> lstMonitoringBook bằng cách gọi hàm MonitoringBookBusiness.SearchByPupil(SchoolID, AcademicYearID, PupilID, ClassID)
            IQueryable<MonitoringBook> lstMonitoringBook = MonitoringBookBusiness.SearchByPupil(SchoolID, AcademicYearID, PupilID, ClassID);
            //B2: Thực hiện việc tìm kiếm lstMonitoringBook mb left DentalTest dt on mb.MonitoringBookID = dt.MonitoringBookID. Kêt quả thu được IQueryable<DentalTestBO>
            var query = from mb in lstMonitoringBook
                        join dt in OverallTestRepository.All.Where(u => u.IsActive == true) on mb.MonitoringBookID equals dt.MonitoringBookID
                        //into g1
                        //from j2 in g1.DefaultIfEmpty()
                        select new OverallTestBO
                        {
                            OverallTestID = dt.OverallTestID,
                            MonitoringBookID = mb.MonitoringBookID,
                            MonitoringDate = mb.MonitoringDate,
                            HistoryYourSelf = dt.HistoryYourSelf,
                            OverallInternal = dt.OverallInternal,
                            DescriptionOverallInternal = dt.DescriptionOverallInternall,
                            OverallForeign = dt.OverallForeign,
                            DescriptionOverallForeign = dt.DescriptionForeign,
                            SkinDiseases = dt.SkinDiseases,
                            DescriptionSkinDiseases = dt.DescriptionSkinDiseases,
                            IsHeartDiseases = dt.IsHeartDiseases,
                            IsBirthDefect = dt.IsBirthDefect,
                            Other = dt.Other,
                            EvaluationHealth = dt.EvaluationHealth,
                            RequireOfDoctor = dt.RequireOfDoctor,
                            Doctor = dt.Doctor,
                            IsDredging = dt.IsDredging,
                            CreatedDate = dt.CreatedDate,
                            IsActive = dt.IsActive,
                            ModifiedDate = dt.ModifiedDate,
                            ClassID = mb.ClassID,
                            HealthPeriodID = mb.HealthPeriodID,
                            PupilID = mb.PupilID,
                            UnitName = mb.UnitName
                        };
            return query;
        }

        // Thuyen - 05/10/2017
        public List<OverallTest> ListOverallTestByMonitoringBookId(List<int> lstMoniBookID)
        {
            return OverallTestBusiness.All.Where(x => lstMoniBookID.Contains(x.MonitoringBookID)).ToList();
        }
    }
}