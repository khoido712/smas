﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SMAS.Business.BusinessObject;
using SMAS.Business.Common;
using SMAS.Models.Models;
using System.IO;
using SMAS.VTUtils.Excel.Export;
using SMAS.Business.Business;
using SMAS.DAL.Repository;
using System.Drawing;

namespace SMAS.Business.Business
{
    public partial class PupilRetestBusiness
    {
        #region Lấy mảng băm cho các tham số đầu vào của danh sách thi lại
        public string GetHashKey(PupilRetestBO entity)
        {
            IDictionary<string, object> dic = new Dictionary<string, object> {
                {"AcademicYearID", entity.AcademicYearID},          
                {"Semester", entity.Semester},
                {"EducationLevelID", entity.EducationLevelID},          
                {"ClassID", entity.ClassID},
                {"SchoolID", entity.SchoolID},         
                {"AppliedLevel", entity.AppliedLevel}
            };
            return ReportUtils.GetHashKey(dic);
        }
        #endregion

        #region Lưu lại thông tin sổ gọi tên và ghi điểm
        public ProcessedReport InsertPupilRetest(PupilRetestBO entity, Stream data)
        {
            string reportCode = SystemParamsInFile.REPORT_DS_HS_THI_LAI;
            ReportDefinition reportDef = ReportDefinitionBusiness.GetByCode(reportCode);
            ProcessedReport pr = new ProcessedReport();
            pr.ReportCode = reportCode;
            pr.ProcessedDate = DateTime.Now;
            pr.InputParameterHashKey = GetHashKey(entity);
            pr.ReportData = ReportUtils.Compress(data);

            //Tạo tên file
            string outputNamePattern = reportDef.OutputNamePattern;
            string semester = ReportUtils.ConvertSemesterForReportName(entity.Semester);
            string schoolLevel = "";
            if (entity.AppliedLevel == SystemParamsInFile.APPLIED_LEVEL_PRIMARY)
            {
                schoolLevel = "TH";
            }
            if (entity.AppliedLevel == SystemParamsInFile.APPLIED_LEVEL_SECONDARY)
            {
                schoolLevel = "THCS";
            }
            if (entity.AppliedLevel == SystemParamsInFile.APPLIED_LEVEL_TERTIARY)
            {
                schoolLevel = "THPT";
            }
            string resolution = EducationLevelBusiness.Find(entity.EducationLevelID) != null ? EducationLevelBusiness.Find(entity.EducationLevelID).Resolution : string.Empty;
            string className = ClassProfileBusiness.Find(entity.ClassID) != null ? ClassProfileBusiness.Find(entity.ClassID).DisplayName : string.Empty;

            outputNamePattern = outputNamePattern.Replace("[Semester]", semester);
            outputNamePattern = outputNamePattern.Replace("[SchoolLevel]", schoolLevel);
            if (entity.ClassID != 0)
            {
                outputNamePattern = outputNamePattern.Replace("[EducationLevel/Class]", className);
            }
            else
            {
                outputNamePattern = outputNamePattern.Replace("[EducationLevel/Class]", resolution);
            }
            

            pr.ReportName = ReportUtils.RemoveSpecialCharacters(outputNamePattern) + "." + reportDef.OutputFormat;

            IDictionary<string, object> dic = new Dictionary<string, object> {
                {"AcademicYearID", entity.AcademicYearID},                                
                {"Semester", entity.Semester},                
                {"StudyingJudgementID", SystemParamsInFile.STUDYING_JUDGEMENT_RETEST},
                {"EducationLevelID", entity.EducationLevelID},          
                {"ClassID",entity.ClassID},
                {"SchoolID", entity.SchoolID},              
            };
            ProcessedReportParameterRepository.Insert(dic, pr);
            ProcessedReportRepository.Insert(pr);
            ProcessedReportRepository.Save();
            return pr;
        }
        #endregion

        #region Lấy danh sách học sinh thi lại được cập nhật mới nhất
        public ProcessedReport GetPupilRetest(PupilRetestBO entity)
        {
            string reportCode = SystemParamsInFile.REPORT_DS_HS_THI_LAI;
            string inputParameterHashKey = GetHashKey(entity);
            return ProcessedReportBusiness.GetProcessedReport(reportCode, inputParameterHashKey);
        }
        #endregion

        #region Lưu lại thông tin danh sách học sinh thi lại
        /// <summary>
        /// Lưu lại thông tin danh sách học sinh thi lại
        /// </summary>
        /// <param name="entity"></param>
        /// <returns></returns>
        public Stream CreatePupilRetest(PupilRetestBO entity)
        {
            string reportCode = SystemParamsInFile.REPORT_DS_HS_THI_LAI;
            ReportDefinition reportDef = ReportDefinitionBusiness.GetByCode(reportCode);
            string templatePath = ReportUtils.GetTemplatePath(reportDef);

            IVTWorkbook oBook = VTExport.OpenWorkbook(templatePath);

            //Lấy sheet template
            IVTWorksheet firstSheet = oBook.GetSheet(1);

            //Lấy template dòng đầu tiên trong một nhóm lớp
            IVTRange topRange = firstSheet.GetRange("A9", "V9");

            //Lấy template cho các dòng ở giữa trong một nhóm lớp
            IVTRange middleRange = firstSheet.GetRange("A10", "V10");

            //Lấy template cho các dòng ở cuối trong một nhóm lớp
            IVTRange bottomRange = firstSheet.GetRange("A11", "V11");

            AcademicYear academicYear = AcademicYearRepository.Find(entity.AcademicYearID);
            SchoolProfile school = SchoolProfileRepository.Find(entity.SchoolID);

            //Fill dữ liệu chung
            string schoolName = school.SchoolName.ToUpper();
            string SupervisingDeptName = UtilsBusiness.GetSupervisingDeptName(school.SchoolProfileID, entity.AppliedLevel);
            firstSheet.SetCellValue("A2", SupervisingDeptName);
            firstSheet.SetCellValue("A3", schoolName);

            //Lấy danh sách các khối
            List<EducationLevel> lstEducationLevel = new List<EducationLevel>();
            if (entity.EducationLevelID != 0)
            {
                lstEducationLevel.Add(EducationLevelRepository.Find(entity.EducationLevelID));
            }
            else
            {
                lstEducationLevel = EducationLevelBusiness.GetByGrade(entity.AppliedLevel).ToList();
            }

            if (entity.EducationLevelID != 0)
            {
                lstEducationLevel = lstEducationLevel.Where(o => o.EducationLevelID == entity.EducationLevelID).ToList();
            }
            //Lấy danh sách thông tin học sinh thi lại
            Dictionary<string, object> dicPupilRetest = new Dictionary<string, object> 
            {
                {"AcademicYearID", entity.AcademicYearID},                                
                {"Semester", entity.Semester},                
                {"StudyingJudgementID", SystemParamsInFile.STUDYING_JUDGEMENT_RETEST},
                {"EducationLevelID", entity.EducationLevelID},          
                {"SchoolID", entity.SchoolID},     
                {"AppliedLevel", entity.AppliedLevel},          
            };
            List<PupilRankingBO> lstPupilRetest = PupilRankingBusiness.GetListPupilRetest(dicPupilRetest).ToList();

            if (entity.EducationLevelID != 0)
            {
                lstPupilRetest = lstPupilRetest.Where(o => o.EducationLevelID == entity.EducationLevelID).ToList();
            }
            if (entity.ClassID != 0)
            {
                lstPupilRetest = lstPupilRetest.Where(o => o.ClassID == entity.ClassID).ToList();
            }

            Dictionary<string, object> dicSubject = new Dictionary<string, object> 
                    {
                        {"AcademicYearID", entity.AcademicYearID},                                
                        {"SchoolID", entity.SchoolID},    
                        {"AppliedLevel", entity.AppliedLevel},    
                    };
            //List<ClassSubject> LstClassSubject = ClassSubjectBusiness.SearchByAcademicYear(entity.AcademicYearID, dicSubject).Distinct().OrderBy(o => o.SubjectCat.DisplayName).ToList();
            List<ClassSubject> LstClassSubjectAll = ClassSubjectBusiness.SearchBySchool(entity.SchoolID, dicSubject).ToList();


            //Tạo và fill dữ liệu vào các sheet tương ứng các khối
            foreach (EducationLevel educationLevel in lstEducationLevel)
            {
                if (educationLevel.EducationLevelID != SystemParamsInFile.EDUCATION_LEVEL_NINTH && educationLevel.EducationLevelID != SystemParamsInFile.EDUCATION_LEVEL_TWELFTH)
                {
                    //Tạo sheet
                    IVTWorksheet sheet = oBook.CopySheetToLast(firstSheet, "Z1000");
                    sheet.Name = educationLevel.Resolution;

                    //Fill dữ liệu tiêu đề
                    string title = "DANH SÁCH HỌC SINH THI LẠI " + educationLevel.Resolution.ToUpper()
                        + " NĂM HỌC " + academicYear.Year + " - " + (academicYear.Year + 1);
                    sheet.SetCellValue("A5", title);

                    int index = 0;
                    //Lấy danh sách môn học

                    List<SubjectCat> LstClassSubject = LstClassSubjectAll.Where(u => u.ClassProfile.EducationLevelID == educationLevel.EducationLevelID)
                                                            .Select(e => e.SubjectCat)
                                                            .Distinct().OrderBy(o => o.OrderInSubject).ToList();



                    //Sua HeaderTeamplate
                    int headerStartCol = 4;
                    int headerRowStart = 8;
                    //Lấy range môn học
                    IVTRange RangeSubject = firstSheet.GetRange(7, 4, 8, 4);
                    foreach (var item in LstClassSubject)
                    {
                        sheet.CopyPaste(RangeSubject, 7, headerStartCol);
                        sheet.SetCellValue(headerRowStart, headerStartCol, item.DisplayName);

                        headerStartCol++;
                    }

                    //Cac cot con lai
                    int colTBCM = headerStartCol;
                    int headerRow = 7;
                    //sheet.SetCellValue(headerRow, colTBCM, "TBcm");

                    int colXLHL = colTBCM + 1;
                    //sheet.SetCellValue(headerRow, colXLHL, "XLHL");

                    int colXLHLK = colXLHL + 1;
                    //sheet.SetCellValue(headerRow, colXLHLK, "colXLHLK");

                    int colMonThiLai = colXLHLK + 1;
                    //sheet.SetCellValue(headerRow, colCP, "CP");

                    int colMobile = colMonThiLai + 1;
                    //sheet.SetCellValue(headerRow, colKP, "KP");

                    int colMonDangKy = colMobile + 1;
                    //sheet.SetCellValue(headerRow, colDanhHieu, "Danh hiệu");

                    int colXepHang = colMonDangKy + 1;
                    //sheet.SetCellValue(headerRow, colXepHang, "Xếp hạng");

                    //di chuyen vi tri header chxhvn
                    IVTRange columnHead = firstSheet.GetRange("O20", "W22");
                    sheet.CopyPasteSameSize(columnHead, "O2");

                    columnHead = firstSheet.GetRange("O2", "W4");
                    sheet.CopyPasteSameSize(columnHead, 2, colXepHang - 7);
                    //sheet.SetCellValue(4, colXepHang - 7, Province + ", Ngày " + DateTime.Now.Day.ToString() + " Tháng " + DateTime.Now.Month.ToString() + " Năm " + DateTime.Now.Year.ToString());
                    //Merger col Title file
                    sheet.MergeRow(2, 1, 5);
                    sheet.MergeRow(3, 1, 5);
                    sheet.MergeRow(5, 1, colMonDangKy);

                    //merger col title DIEM TB CAC MON HOC
                    columnHead = sheet.GetRange(7, 4, 7, colTBCM - 1);
                    columnHead.Merge();

                    //Copy col header tu cot tbcm
                    columnHead = firstSheet.GetRange("Q7", "W8");
                    sheet.CopyPasteSameSize(columnHead, headerRow, colTBCM);

                    IVTRange emptyRange = firstSheet.GetRange("A18", "M24");
                    sheet.CopyPasteSameSize(emptyRange, headerRow, colXepHang + 1);

                    //Lấy thông tin học sinh trong lớp
                    List<PupilRankingBO> lstPupilRankingBO = (from s in lstPupilRetest
                                                              where s.EducationLevelID == educationLevel.EducationLevelID
                                                              select s).OrderBy(o => o.ClassOrderNumber.HasValue ? o.ClassOrderNumber : 0).ThenBy(x => x.ClassName).ThenBy(x => x.OrderInClass).ThenBy(x => x.FullName).ToList();

                    List<int> lstPE = (from s in lstPupilRankingBO
                                       select s.PupilID.GetValueOrDefault()).Distinct().ToList();

                    int startRow = 9;
                    int countsub = LstClassSubject.Count();
                    int col = countsub + 9;
                    IVTRange range = firstSheet.GetRange(startRow, 1, startRow, col);
                    IVTRange range1 = firstSheet.GetRange(startRow, 20, startRow, 20);
                    IVTRange range2 = firstSheet.GetRange(startRow, 22, startRow, 22);
                    IVTRange rangeTemp = firstSheet.GetRange("R11", "V12");
                    sheet.SetCellValue("D7", "ĐIỂM TRUNG BÌNH CÁC MÔN HỌC");

                    for (int i = 0; i < lstPE.Count; i++)
                    {
                        PupilRankingBO PupilRanking = lstPupilRetest.Where(o => o.PupilID == lstPE[i]).FirstOrDefault();
                        int pupilID = PupilRanking.PupilID.Value;
                        // AnhVD 20140717 - DS HS thi lai phai co it nhat 1 mon co diem TB < 5
                        // Neu hoc luc KEM thi o lai lop va ko nam trong danh sach thi lai
                        //if (!lstPupilRankingBO.Where(o => o.PupilID == pupilID).Any(o => o.IsCommenting.HasValue &&  
                        //    (o.IsCommenting == GlobalConstants.ISCOMMENTING_TYPE_MARK && o.SummedUpMark < 5) || (o.IsCommenting == GlobalConstants.ISCOMMENTING_TYPE_JUDGE && o.JudgementResult == GlobalConstants.NOPASS))
                        //    || PupilRanking.CapacityLevelID != GlobalConstants.CAPACITY_TYPE_WEAK)
                        //{
                        //    continue;
                        //}
                        // End - AnhVD 20140717
                        //if (i > 0)
                        //{
                        //    sheet.CopyAndInsertARow(range, startRow);
                        //}
                        //sheet.CopyPaste(range1, startRow, countsub + 7);
                        //sheet.CopyPaste(range2, startRow, countsub + 9);
                        //sheet.CopyPaste(range, startRow, 1, true);
                        //Copy row style
                        //sheet.CopyPasteSameRowHeigh(range, startRow);sheet.CopyPasteSameSize(range, startRow, 1);
                        sheet.CopyPasteSameSize(range, startRow, 1);
                        //Cot diem dau tien     
                        headerStartCol = 4;
                       
                        //STT
                        sheet.SetCellValue(startRow, 1, ++index);
                        //Tên học sinh
                        sheet.SetCellValue(startRow, 2, PupilRanking.FullName);
                        sheet.SetCellValue(startRow, 3, PupilRanking.ClassName);

                        //diem tong ket cua tung mn hoc
                        foreach (SubjectCat SubjectCat in LstClassSubject)
                        {
                            IEnumerable<PupilRankingBO> lstSummedUpRecordTemp = lstPupilRetest.Where(o => o.PupilID == pupilID && o.SubjectID == SubjectCat.SubjectCatID);
                            if (lstSummedUpRecordTemp != null && lstSummedUpRecordTemp.Count() > 0)
                            {
                                PupilRankingBO PupilRankingBO = lstSummedUpRecordTemp.FirstOrDefault();
                                if (PupilRankingBO != null)
                                {
                                    int status = PupilRankingBO.IsCommenting.Value;
                                    if (status == GlobalConstants.ISCOMMENTING_TYPE_MARK || status == GlobalConstants.ISCOMMENTING_TYPE_MARK_JUDGE)
                                    {
                                        if (PupilRankingBO.SummedUpMark.HasValue)
                                        {
                                            sheet.SetCellValue(startRow, headerStartCol, PupilRankingBO.SummedUpMark.Value);
                                            if (PupilRankingBO.IsCommenting == GlobalConstants.ISCOMMENTING_TYPE_MARK && PupilRankingBO.SummedUpMark < 5)
                                            {
                                                sheet.GetRange(startRow, headerStartCol, startRow, headerStartCol).SetFontColour(Color.Red);
                                            }
                                        }
                                    }
                                    if (status == GlobalConstants.ISCOMMENTING_TYPE_JUDGE)
                                    {
                                        sheet.SetCellValue(startRow, headerStartCol, PupilRankingBO.JudgementResult);
                                        if (PupilRankingBO.IsCommenting == GlobalConstants.ISCOMMENTING_TYPE_JUDGE && PupilRankingBO.JudgementResult == "CĐ")
                                        {
                                            sheet.GetRange(startRow, headerStartCol, startRow, headerStartCol).SetFontColour(Color.Red);
                                        }
                                    }
                                }
                            }
                            headerStartCol = headerStartCol + 1;
                        }
                        //dien thong tin cho 6 cot con lai

                        //cot Tbcm
                        IEnumerable<PupilRankingBO> listPupilRankingTemp = lstPupilRetest.Where(o => o.PupilID == pupilID).OrderBy(o => o.OrderInSubject);
                        if (listPupilRankingTemp != null && listPupilRankingTemp.Count() > 0)
                        {
                            string MonThiLai = "";
                            string Dangki = "";
                            foreach (var item in listPupilRankingTemp)
                            {
                                if (Dangki == "")
                                {
                                    Dangki = item.SubjectNameRetest;
                                }
                                else
                                {
                                    if (item.SubjectNameRetest != null)
                                    {
                                        if (!Dangki.Contains(item.SubjectNameRetest))
                                        {
                                            Dangki = Dangki + "," + item.SubjectNameRetest;
                                        }
                                    }
                                }
                                int status = item.IsCommenting.Value;
                                if (status == GlobalConstants.ISCOMMENTING_TYPE_MARK)
                                {
                                    if (item.SummedUpMark.HasValue)
                                    {
                                        if (item.SummedUpMark.Value < 5)
                                        {
                                            if (MonThiLai == "")
                                            {
                                                MonThiLai = item.SubjectName;
                                            }
                                            else
                                            {
                                                if (!MonThiLai.Contains(item.SubjectName))
                                                {
                                                    MonThiLai = MonThiLai + ", " + item.SubjectName;
                                                }
                                            }
                                        }
                                    }
                                }
                                if (status == GlobalConstants.ISCOMMENTING_TYPE_JUDGE)
                                {
                                    if (item.JudgementResult != null)
                                    {
                                        if (item.JudgementResult == "CĐ")
                                        {
                                            if (MonThiLai == "")
                                            {
                                                MonThiLai = item.SubjectName;
                                            }
                                            else
                                            {
                                                if (!MonThiLai.Contains(item.SubjectName))
                                                {
                                                    MonThiLai = MonThiLai + ", " + item.SubjectName;
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                            PupilRankingBO PupilRankingBO = listPupilRankingTemp.FirstOrDefault();
                            if (PupilRankingBO.AverageMark != null)
                            {
                                sheet.SetCellValue(startRow, colTBCM, PupilRankingBO.AverageMark.Value);
                            }
                            //cot hanh kiem
                            sheet.SetCellValue(startRow, colXLHLK, UtilsBusiness.ConvertCapacity(PupilRankingBO.ConductLevel));
                            //cot hoc luc                       
                            sheet.SetCellValue(startRow, colXLHL, UtilsBusiness.ConvertCapacity(PupilRankingBO.CapacityLevel));
                            //cot mon thi lai              
                            sheet.SetCellValue(startRow, colMonThiLai, MonThiLai);
                            //cot mobile                       
                            sheet.SetCellValue(startRow, colMobile, PupilRankingBO.Mobile);
                            //cot mon dang ky thi lai                       
                            sheet.SetCellValue(startRow, colMonDangKy, Dangki);

                        }

                        sheet.GetRange(startRow, 1, startRow, colMonDangKy).SetBorder(VTBorderStyle.Solid, VTBorderWeight.Thin, VTBorderIndex.All);
                        startRow = startRow + 1;  //Lop                    
                    }
                    //set do cao cho row header

                    sheet.SetRowHeight(8, 45);
                    sheet.SetColumnWidth(col, 12);
                    sheet.SetColumnWidth(col - 1, 10);
                    sheet.SetColumnWidth(col - 2, 12);
                    //IVTRange rangeDel = sheet.GetRange("AE36", "AE38");
                    //sheet.CopyPasteSameSize(rangeDel, "AF36");

                    sheet.SetCellValue(1, 1, "");
                    SheetData sheetdata = new SheetData(firstSheet.Name);
                    sheetdata.Data["ProvinceName"] = (school.District != null ? school.District.DistrictName : "");
                    sheetdata.Data["day"] = DateTime.Now.Day;
                    sheetdata.Data["month"] = DateTime.Now.Month;
                    sheetdata.Data["year"] = DateTime.Now.Year;
                    firstSheet.FillVariableValue(sheetdata.Data);
                    sheet.CopyPasteSameSize(rangeTemp, startRow + 1, 18);

                    int rowEnd = 9 + lstPE.Count + 1;
                    int colEnd = 9 + LstClassSubject.Count - 3;
                    string titleDateTime = (school.District != null ? school.District.DistrictName : "") + ", ngày " + DateTime.Now.Day
                        + " tháng " + DateTime.Now.Month + " năm " + DateTime.Now.Year;
                    string namePrin = "HIỆU TRƯỞNG";
                    sheet.SetCellValue(rowEnd, colEnd, titleDateTime);
                    sheet.SetCellValue(rowEnd + 1, colEnd, namePrin);
                    sheet.GetRange(rowEnd, colEnd, rowEnd, colEnd + 3).Merge();
                    sheet.GetRange(rowEnd+1, colEnd, rowEnd+1, colEnd + 3).Merge();

                    IVTRange footerRange = sheet.GetRange(rowEnd, colEnd, rowEnd + 1, colEnd + 2);
                    footerRange.SetHAlign(VTHAlign.xlHAlignCenter);
                 
  
                    sheet.FitAllColumnsOnOnePage = true;
                }


            }

            //Xoá sheet template
            firstSheet.Delete();

            return oBook.ToStream();
        }

        #endregion
    }
}
