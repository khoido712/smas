﻿/**
 * The Viettel License
 *
 * Copyright 2012 Viettel Telecom. All rights reserved.
 * VIETTEL PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */

/** 
* @author  
* @version $Revision: $
*/

using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Linq.Expressions;
using System.Web;
using log4net;

using SMAS.DAL.Repository;
using SMAS.DAL.IRepository;
using SMAS.Business.IBusiness;
using SMAS.Models.Models;
using System.Transactions;
using SMAS.Business.Common;
using SMAS.Business.BusinessObject;

namespace SMAS.Business.Business
{
    public partial class EatingGroupBusiness
    {
        private void Validation(EatingGroup Entity, List<int> lstClass)
        {
            ValidationMetadata.ValidateObject(Entity);

            //SchoolID: FK(SchoolProfile)
            new SchoolProfileBusiness(null).CheckAvailable(Entity.SchoolID, "SchoolProfile_Label_SchoolProfile");
            //            MonthFrom < MonthTo
            if (Entity.MonthFrom >= Entity.MonthTo)
            {
                throw new BusinessException("EatingGroup_Validate_Month");
            }
            //ClassID in lstClass: FK(ClassProfile)
            foreach (int classID in lstClass)
            {
                new ClassProfileBusiness(null).CheckAvailable(classID, "ClassProfile_Label_ClassProfile");
            }
            //SchoolID, AcademicYearID: Not compatible(AcademicYear)
            bool AcademicYearCompatible = new AcademicYearRepository(this.context).ExistsRow(GlobalConstants.SCHOOL_SCHEMA, "AcademicYear",
                 new Dictionary<string, object>()
                {
                    {"SchoolID",Entity.SchoolID},
                    {"AcademicYearID",Entity.AcademicYearID}
                }, null);
            if (!AcademicYearCompatible)
            {
                throw new BusinessException("Common_Validate_NotCompatible");
            }
            //AcademicYearBusiness.IsCurrentYear(entity.AcademicYearID) = FALSE => Năm học không phải năm hiện tại
            AcademicYear academicYear = AcademicYearRepository.Find(Entity.AcademicYearID);
            if (!AcademicYearBusiness.IsCurrentYear(Entity.AcademicYearID))
            {
                throw new BusinessException("EatingGroup_Validate_IsNotCurrrentYear");
            }
        }

        public void Insert(EatingGroup Entity, List<int> lstClass)
        {
            Validation(Entity, lstClass);
            this.CheckDuplicate(Entity.EatingGroupName, GlobalConstants.LIST_SCHEMA, "EatingGroup", "EatingGroupName", true, 0, "EatingGroup_Label_EatingGroupName");
            using (TransactionScope scope = new TransactionScope())
            {
                Entity.IsActive = true;
                base.Insert(Entity);
                base.Save();
                //Với mỗi phần tử trong lstClass.
                //Thực hiện Insert dữ liệu vào bảng EatingGroupClass
                //với EatingGroupID = Entity.EatingGroupID
                //ClassID = lstClass[i].ClassID

                if (lstClass != null && lstClass.Count > 0)
                {
                    int classID;
                    for (int i = 0,tempClass = lstClass.Count; i < tempClass; i++)
                    {
                        classID = lstClass[i];
                    //}
                    //foreach (int classID in lstClass)
                    //{
                        EatingGroupClass EatingGroupClass = new EatingGroupClass();
                        EatingGroupClass.EatingGroupID = Entity.EatingGroupID;
                        EatingGroupClass.ClassID = classID;
                        EatingGroupClassRepository.Insert(EatingGroupClass);
                    }
                }
                scope.Complete();
            }

        }

        public void Update(EatingGroup Entity, List<int> lstClass)
        {
            Validation(Entity, lstClass);
            //EatingGroupID: PK(EatingGroup)
            //this.CheckAvailable(Entity.EatingGroupID, "EatingGroup_Label_EatingGroup");
            //SchooID,EatingGroupID: not compatible(EatingGroup)
            bool EatingGroupCompatible = new EatingGroupRepository(this.context).ExistsRow(GlobalConstants.LIST_SCHEMA, "EatingGroup",
                 new Dictionary<string, object>()
                {
                    {"SchoolID",Entity.SchoolID},
                    {"EatingGroupID",Entity.EatingGroupID}
                }, null);
            if (!EatingGroupCompatible)
            {
                throw new BusinessException("Common_Validate_NotCompatible");
            }


            bool checkEatingGroupNamExist = new EatingGroupRepository(this.context).CheckDuplicate(Entity.EatingGroupName, GlobalConstants.LIST_SCHEMA, "EatingGroup", "EatingGroupName", true, Entity.EatingGroupID);
//CheckDuplicate(Entity.EatingGroupName, GlobalConstants.LIST_SCHEMA, "EatingGroup", "EatingGroupName", true, 0, "EatingGroup_Label_EatingGroupName");    }, null);

            if (checkEatingGroupNamExist)
            {
                throw new BusinessException("Common_Validate_Duplicate", new List<object> {"EatingGroup_Label_EatingGroupName" });
            }
            using (TransactionScope scope = new TransactionScope())
            {
                base.Update(Entity);
                base.Save();
                var lstEatingGroupClass = EatingGroupClassRepository.All.Where(o => o.EatingGroupID == Entity.EatingGroupID).ToList();
                if (lstEatingGroupClass.Count() > 0)
                {
                    EatingGroupClass item = null;
                    for (int i = 0,tempEatingGroupClass = lstEatingGroupClass.Count; i < tempEatingGroupClass; i++)
                    {
                        item = lstEatingGroupClass[i];
                    //}
                    //foreach (EatingGroupClass item in lstEatingGroupClass)
                    //{
                        EatingGroupClassRepository.Delete(item.EatingGroupClassID);
                    }
                }
                if (lstClass != null && lstClass.Count > 0)
                {
                    int item;
                    for (int i = 0,tempClass = lstClass.Count; i < tempClass; i++)
                    {
                        item = lstClass[i];
                    //}
                    //foreach (var item in lstClass)
                    //{
                        EatingGroupClass EatingGroupClass = new EatingGroupClass();
                        EatingGroupClass.EatingGroupID = Entity.EatingGroupID;
                        EatingGroupClass.ClassID = item;
                        //EatingGroupClassRepository.Insert(EatingGroupClass);
                        EatingGroupClassRepository.Insert(EatingGroupClass);
                    }
                }
                scope.Complete();
            }

        }

        public void Delete(int EatingGroupID, int SchoolID)
        {
            this.CheckAvailable(EatingGroupID, "EatingGroup_Label_EatingGroup");
            //EatingGroupID: Using
            bool checkDelete = new EatingGroupRepository(this.context).ExistsRow(GlobalConstants.LIST_SCHEMA, "EatingGroup",
            new Dictionary<string, object>()
                {
                    {"EatingGroupID",EatingGroupID}
                }, null);
            if (!checkDelete)
            {
                throw new BusinessException("Common_Delete_Exception");
            }
            bool checkUsing = new EatingGroupClassRepository(this.context).ExistsRow(GlobalConstants.LIST_SCHEMA, "NutritionalNorm",
                new Dictionary<string, object>()
                {
                    {"EatingGroupID",EatingGroupID},
                    {"IsActive", true}
                }, null);
            if (checkUsing)
            {
                throw new BusinessException("Common_Validate_Using", new List<object> { "EatingGroup_Label_AllTitle" });
            }
            EatingGroup Entity = this.Find(EatingGroupID);
            bool EatingGroupCompatible = new EatingGroupRepository(this.context).ExistsRow(GlobalConstants.LIST_SCHEMA, "EatingGroup",
                new Dictionary<string, object>()
                {
                    {"SchoolID",Entity.SchoolID},
                    {"EatingGroupID",Entity.EatingGroupID}
                }, null);
            if (!EatingGroupCompatible)
            {
                throw new BusinessException("Common_Validate_NotCompatible");
            }
            AcademicYear academicYear = AcademicYearRepository.Find(Entity.AcademicYearID);
            if (!AcademicYearBusiness.IsCurrentYear(Entity.AcademicYearID))
            {
                throw new BusinessException("EatingGroup_Validate_IsNotCurrrentYear");
            }

            var EatingGroup = this.All.Where(o => o.EatingGroupID == EatingGroupID).FirstOrDefault();
            if (EatingGroup != null)
            {
                EatingGroup.IsActive = false;
                EatingGroup.ModifiedDate = DateTime.Now;
                this.Update(EatingGroup);
            }

        }

        public IQueryable<EatingGroup> SearchEatingGroup(IDictionary<string, object> dic)
        {
            string EatingGroupName = Utils.GetString(dic, "EatingGroupName");
            int SchoolID = Utils.GetInt(dic, "SchoolID");
            int AcademicYearID = Utils.GetInt(dic, "AcademicYearID");        
            bool? IsActive = Utils.GetIsActive(dic, "IsActive");
            //if (SchoolID == 0)
            //    return null;
            IQueryable<EatingGroup> Query = EatingGroupRepository.All;
            if (IsActive.HasValue)
            {
                Query = Query.Where(cfh => cfh.IsActive == IsActive);
            }
            if (EatingGroupName.Trim().Length != 0)
            {
                Query = Query.Where(cfh => cfh.EatingGroupName.Contains(EatingGroupName));
            }
            if (SchoolID != 0)
            {
                Query = Query.Where(cfh => cfh.SchoolID == SchoolID);
            }
            if (AcademicYearID != 0)
            {
                Query = Query.Where(cfh => cfh.AcademicYearID == AcademicYearID);
            }
            return Query.OrderBy(p=>p.EatingGroupName);
        }

        private IQueryable<EatingGroupBO> Search(IDictionary<string, object> SearchInfo)
        {

            var query = from eg in this.All
                        join egc in EatingGroupClassRepository.All on eg.EatingGroupID equals egc.EatingGroupID into eat
                        from ett in eat.DefaultIfEmpty()
                        select new EatingGroupBO
                        {
                            EatingGroupID = eg.EatingGroupID,
                            AcademicYearID = eg.AcademicYearID,
                            EatingGroupName = eg.EatingGroupName,
                            MonthFrom = eg.MonthFrom,
                            MonthTo = eg.MonthTo,
                            SchoolID = eg.SchoolID,
                            IsActive = eg.IsActive,
                            ClassID = ett.ClassID,
                            ClassName = ett.ClassProfile.DisplayName,
                            EatingGroupClassID = ett.EatingGroupClassID
                        };
            int SchoolID = Utils.GetInt(SearchInfo, "SchoolID");
            int AcademicYearID = Utils.GetInt(SearchInfo, "AcademicYearID");
            string EatingGroupName = Utils.GetString(SearchInfo, "EatingGroupName");
            int MonthFrom = Utils.GetInt(SearchInfo, "MonthFrom");
            int MonthTo = Utils.GetInt(SearchInfo, "MonthTo");
            int EatingGroupID = Utils.GetInt(SearchInfo, "EatingGroupID");
            bool? IsActive = Utils.GetIsActive(SearchInfo, "IsActive");

            if (SchoolID > 0)
                query = query.Where(o => o.SchoolID == SchoolID);
            if (AcademicYearID > 0)
                query = query.Where(o => o.AcademicYearID == AcademicYearID);
            if (!string.IsNullOrWhiteSpace(EatingGroupName))
                query = query.Where(o => o.EatingGroupName == EatingGroupName
                    || o.EatingGroupName.Contains(EatingGroupName));
            if (EatingGroupID > 0)
                query = query.Where(o => o.EatingGroupID == EatingGroupID);
            if (MonthFrom > 0)
                query = query.Where(o => o.MonthFrom == MonthFrom);
            if (MonthTo > 0)
                query = query.Where(o => o.MonthTo == MonthTo);
            if (IsActive.HasValue)
                query = query.Where(o => o.IsActive == IsActive.Value);
            return query;
        }

        public IQueryable<EatingGroupBO> SearchBySchool(int SchoolID, IDictionary<string, object> SearchInfo)
        {
            if (SchoolID == 0)
                return null;
            SearchInfo["SchoolID"] = SchoolID;
            return Search(SearchInfo);
        }
    }
}