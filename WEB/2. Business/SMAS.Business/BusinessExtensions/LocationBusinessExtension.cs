﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SMAS.Models.Models;
using SMAS.DAL.Repository;
using SMAS.Business.Common;

namespace SMAS.Business.Business
{
    public partial class LocationBusiness
    {
        public IQueryable<Location> Search(IDictionary<string, object> dic)
        {
            int LocationID = Utils.GetInt(dic, "LocationID");
            string Name = Utils.GetString(dic, "Name");
            DateTime? CreatedDate = Utils.GetDateTime(dic, "CreatedDate");
            IQueryable<Location> iqLocation = LocationRepository.All;

            if (LocationID != 0)
            {
                iqLocation = iqLocation.Where(o => (o.LocationID == LocationID));
            }
            if (Name.Trim().Length >= 0)
            {
                iqLocation = iqLocation.Where(o => (o.Name == Name));
            }
            if (CreatedDate != null)
            {
                iqLocation = iqLocation.Where(o => (o.CreatedDate == CreatedDate));
            }
            return iqLocation;
        }
    }
}
