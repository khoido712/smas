/**
 * The Viettel License
 *
 * Copyright 2012 Viettel Telecom. All rights reserved.
 * VIETTEL PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */

/** 
* @author  
* @version $Revision: $
*/

using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Linq.Expressions;
using System.Web;
using log4net;

using SMAS.DAL.Repository;
using SMAS.DAL.IRepository;
using SMAS.Business.IBusiness;
using SMAS.Models.Models;
using SMAS.Business.Common;

namespace SMAS.Business.Business
{
    public partial class AcademicYearOfProvinceBusiness
    {
        #region Validate
        public void Validate(AcademicYearOfProvince entity)
        {
            ValidationMetadata.ValidateObject(entity);
            //FK(Province)
            //!( FirstSemesterStartDate < FirstSemesterEndDate < SecondSemesterStartDate < SecondSemesterEndDate)

            if (entity.FirstSemesterStartDate >= entity.FirstSemesterEndDate || entity.FirstSemesterEndDate >= entity.SecondSemesterStartDate || entity.SecondSemesterStartDate >= entity.SecondSemesterEndDate)
            {
                throw new BusinessException("AcademicYearOfProvince_Validate_TimeSemester");
            }

            //Year, ProvinceID: duplicate
            //FirstSemesterStartDate.Year != Year
            //FirstSemesterEndDate.Year, SecondSemesterStartDate.Year, SecondSemesterEndDate.Year hơn Year quá 1

            bool employeeExist = new AcademicYearOfProvinceRepository(this.context).ExistsRow(GlobalConstants.SCHOOL_SCHEMA, "AcademicYearOfProvince", new Dictionary<string, object>() { { "Year", entity.Year }, { "ProvinceID", entity.ProvinceID }, { "IsActive", true } }, null);

            if (employeeExist)
            {
                throw new BusinessException("AcademicYearOfProvince_Validate_DuplicateYear");
            }

            if (entity.FirstSemesterStartDate.Value.Year != entity.Year)
            {
                throw new BusinessException("AcademicYearOfProvince_Validate_TimeSemester");
            }

            if (entity.FirstSemesterEndDate.Value.Year != entity.SecondSemesterStartDate.Value.Year || entity.FirstSemesterEndDate.Value.Year != entity.SecondSemesterEndDate.Value.Year)
            {
                throw new BusinessException("AcademicYearOfProvince_Validate_TimeSemester");
            }

            if (entity.FirstSemesterEndDate.Value.Year - entity.Year != 1)
            {
                throw new BusinessException("AcademicYearOfProvince_Validate_TimeSemester");
            }
        }
        #endregion

        #region Insert
        public override AcademicYearOfProvince Insert(AcademicYearOfProvince entity)
        {
            entity.Year = (int)entity.FirstSemesterStartDate.Value.Year;
            entity.DisplayTitle = entity.Year + " - " + (entity.Year + 1).ToString();
            Validate(entity);
            //Nếu entity.IsInheritData = true
            //Thực hiện kế thừa môn học
            if (entity.IsInheritData == true)
            {
                IQueryable<AcademicYearOfProvince> listAcademicYearOfProvince = this.SearchByProvince(entity.ProvinceID, new Dictionary<string, object>() { }).Where(u => u.Year < entity.Year).OrderByDescending(o => o.Year);

                if (listAcademicYearOfProvince != null)
                {
                    //ProvinceSubjectBusiness.SearchByProvince(entity.Province, Dic)
                    //với Dic[“AcademicYearOfProvinceID”] = aca.AcademicYearOfProvinceID
                    //Lấy danh sách ProvinceSubject  tìm được. Tạo bản ghi mới thay thế aca.AcademicYearOfProvinceID = entity.AcademicYearOfProvince rồi insert vào bảng ProvinceSubject

                    IDictionary<string, object> dic = new Dictionary<string, object>();
                    dic["AcademicYearOfProvinceID"] = listAcademicYearOfProvince.FirstOrDefault().AcademicYearOfProvinceID;

                    IQueryable<ProvinceSubject> lsProvinceSubject = ProvinceSubjectBusiness.SearchByProvince(entity.ProvinceID, dic);

                    if (lsProvinceSubject == null)
                    {
                        return null;
                    }
                    List<ProvinceSubject> listProvinceSubject = lsProvinceSubject.ToList();
                    foreach (ProvinceSubject ProvinceSubject in listProvinceSubject)
                    {
                        ProvinceSubject ps = new ProvinceSubject();
                        ps.AppliedLevel = ProvinceSubject.AppliedLevel;
                        ps.AppliedType = ProvinceSubject.AppliedType;
                        ps.Coefficient = ProvinceSubject.Coefficient;
                        ps.EducationLevelID = ProvinceSubject.EducationLevelID;
                        ps.IsCommenting = ProvinceSubject.IsCommenting;
                        ps.IsRegularEducation = ProvinceSubject.IsRegularEducation;
                        ps.ProvinceID = ProvinceSubject.ProvinceID;
                        ps.SectionPerWeekFirstSemester = ProvinceSubject.SectionPerWeekFirstSemester;
                        ps.SectionPerWeekSecondSemester = ProvinceSubject.SectionPerWeekSecondSemester;
                        ps.SubjectID = ProvinceSubject.SubjectID;
                        entity.ProvinceSubjects.Add(ps);

                        //entity.ProvinceSubjects.Add(ps);
                    }
                }

            }
            base.Insert(entity);
            this.Save();
            return entity;
        }
        #endregion

        #region Update
        /// <summary>
        /// Update
        /// </summary>
        /// <param name="entity">The entity.</param>
        ///<author>Nam ta</author>
        ///<Date>1/11/2013</Date>
        /// <exception cref="BusinessException">AcademicYearOfProvince_Validate_ProvinceDuplicate</exception>
        public void Update(AcademicYearOfProvince entity)
        {
            entity.DisplayTitle = entity.Year + "-" + (entity.Year + 1).ToString();
            AcademicYearOfProvince AcademicYearOfProvince = this.Find(entity.AcademicYearOfProvinceID);

            if (entity.ProvinceID != AcademicYearOfProvince.ProvinceID)
            {
                throw new BusinessException("AcademicYearOfProvince_Validate_ProvinceDuplicate");
            }

            base.Update(entity);


        }
        #endregion

        #region Delete
        /// <summary>
        /// Delete
        /// </summary>
        /// <param name="AcademicYearOfProvinceID">The academic year of province ID.</param>
        /// <param name="ProvinceID">The province ID.</param>
        ///<author>Nam ta</author>
        ///<Date>1/11/2013</Date>
        /// <exception cref="BusinessException">Common_Validate_NotCompatible</exception>
        public void Delete(int AcademicYearOfProvinceID, int ProvinceID)
        {
            this.CheckAvailable(AcademicYearOfProvinceID, "AcademicYearOfProvince_Validate_AcademicYearOfProvinceID");
            this.CheckConstraints(GlobalConstants.SCHOOL_SCHEMA, "AcademicYearOfProvince", AcademicYearOfProvinceID, "AcademicYearOfProvince_Validate_AcademicYearOfProvinceID");

            bool AcademicYearOfProvinceCompatible = new AcademicYearOfProvinceRepository(this.context).ExistsRow(GlobalConstants.SCHOOL_SCHEMA, "AcademicYearOfProvince",
                    new Dictionary<string, object>()
                {
                    {"AcademicYearOfProvinceID",AcademicYearOfProvinceID},
                    {"ProvinceID",ProvinceID}
                }, null);
            if (!AcademicYearOfProvinceCompatible)
            {
                throw new BusinessException("Common_Validate_NotCompatible");
            }

            AcademicYearOfProvince entity = this.Find(AcademicYearOfProvinceID);
            ValidationMetadata.ValidateObject(entity);
            entity.IsActive = false;

            this.Update(entity);

        }
        #endregion

        #region Search
        public IQueryable<AcademicYearOfProvince> Search(IDictionary<string, object> SearchInfo)
        {
            int ProvinceID = Utils.GetInt(SearchInfo, "ProvinceID");
            bool IsActive = Utils.GetBool(SearchInfo, "IsActive", true);
            int Year = Utils.GetInt(SearchInfo, "Year");

            IQueryable<AcademicYearOfProvince> querry = this.All;

            if (ProvinceID != 0)
            {
                querry = querry.Where(o => o.ProvinceID == ProvinceID);
            }

            if (Year != 0)
            {
                querry = querry.Where(o => o.Year == Year);
            }

            querry = querry.Where(o => o.IsActive == IsActive);

            return querry;

        }
        #endregion

        #region SearchByProvince
        public IQueryable<AcademicYearOfProvince> SearchByProvince(int ProvinceID, IDictionary<string, object> SearchInfo)
        {
            if (ProvinceID == 0)
                return null;
            else
            {
                SearchInfo["ProvinceID"] = ProvinceID;
            }
            return this.Search(SearchInfo);
        }
        #endregion

        /// <summary>
        /// Lay thong tin nam hoc hien tai
        /// </summary>
        /// <param name="ProvinceID"></param>
        /// <returns></returns>
        public int GetCurrentYear(int ProvinceID, string StandardAcFromDate = null)
        {
            DateTime curDate = DateTime.Now.Date;
            // Lay danh sach nam hoc hien tai cua so
            IQueryable<AcademicYearOfProvince> listAcaProvince = this.SearchByProvince(ProvinceID, new Dictionary<string, object>());
            // Neu da co nam hoc tren so thi chi xet doi voi danh sach nay
            if (listAcaProvince.Count() > 0)
            {
                AcademicYearOfProvince curAcaProvince = listAcaProvince.Where(o => o.FirstSemesterStartDate <= curDate && o.SecondSemesterEndDate >= curDate).FirstOrDefault();
                // Neu ton tai nam hoc trong khoang thoi gian hien tai
                if (curAcaProvince != null)
                {

                    return curAcaProvince.Year.Value;
                }
                return 0;
            }
            // Neu khong co nam hoc duoc khai bao tren so thi xet thoi khoang thoi gian hien tai thuoc nam hoc nao
            // theo khai bao chuan nam hoc
            if (StandardAcFromDate != null)
            {
                try
                {
                    // Ghep ngay chuan voi nam hien tai
                    // Neu nhu ngay hien tai lon hon ngay nay co nghia nam hoc hien tai la nam hoc nay
                    DateTime fromDate = DateTime.Parse(StandardAcFromDate + curDate.Year);
                    if (curDate >= fromDate)
                    {
                        return curDate.Year;
                    }
                    // Neu nguoc lai thi la nam hoc truong
                    return curDate.Year - 1;
                }
                catch
                {
                    // Neu truyen dau vao dinh dang ngay/thang/ thi tra ve 0
                    return 0;
                }
            }
            return 0;
        }


        public int GetCurrentYearAndSemester(int ProvinceID, out int semester, string StandardAcFromDate = null)
        {
            DateTime curDate = DateTime.Now.Date;
            // Lay danh sach nam hoc hien tai cua so
            IQueryable<AcademicYearOfProvince> listAcaProvince = this.SearchByProvince(ProvinceID, new Dictionary<string, object>());
            // Neu da co nam hoc tren so thi chi xet doi voi danh sach nay
            semester = SystemParamsInFile.SEMESTER_OF_YEAR_FIRST;
            if (listAcaProvince.Count() > 0)
            {
                AcademicYearOfProvince curAcaProvince = listAcaProvince.Where(o => o.FirstSemesterStartDate <= curDate && o.SecondSemesterEndDate >= curDate).FirstOrDefault();
                // Neu ton tai nam hoc trong khoang thoi gian hien tai
                if (curAcaProvince != null)
                {
                    if (curAcaProvince.FirstSemesterStartDate <= curDate && curDate <= curAcaProvince.FirstSemesterEndDate)
                    {
                        semester = SystemParamsInFile.SEMESTER_OF_YEAR_FIRST;
                    }
                    else
                    {
                        semester = SystemParamsInFile.SEMESTER_OF_YEAR_SECOND;
                    }
                    return curAcaProvince.Year.Value;
                }
                return 0;
            }
            // Neu khong co nam hoc duoc khai bao tren so thi xet thoi khoang thoi gian hien tai thuoc nam hoc nao
            // theo khai bao chuan nam hoc
            if (StandardAcFromDate != null)
            {
                try
                {
                    // Ghep ngay chuan voi nam hien tai
                    // Neu nhu ngay hien tai lon hon ngay nay co nghia nam hoc hien tai la nam hoc nay
                    DateTime fromDate = DateTime.Parse(StandardAcFromDate + curDate.Year);
                    if (curDate >= fromDate)
                    {
                        return curDate.Year;
                    }
                    // Neu nguoc lai thi la nam hoc truong
                    return curDate.Year - 1;
                }
                catch
                {
                    // Neu truyen dau vao dinh dang ngay/thang/ thi tra ve 0
                    return 0;
                }
            }
            return 0;
        }
    }
}
