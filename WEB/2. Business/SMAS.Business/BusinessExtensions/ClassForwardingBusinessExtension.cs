﻿/**
 * The Viettel License
 *
 * Copyright 2012 Viettel Telecom. All rights reserved.
 * VIETTEL PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */

/**
* @author dungnt
* @version $Revision: $
*/

using System;
using System.Collections.Generic;
using System.Linq;
using SMAS.Business.BusinessObject;
using SMAS.Business.Common;
using SMAS.DAL.Repository;
using SMAS.Models.Models;
using SMAS.VTUtils.Log;

namespace SMAS.Business.Business
{
    /// <summary>
    /// k?t chuy?n d? li?u
    /// <author>dungnt</author>
    /// <date>05/09/2012</date>
    /// </summary>
    public partial class ClassForwardingBusiness
    {
        public const int MAX_EDUCATIONLEVEL_PRIMARY = 5;
        #region private member variable

        private const int LEN_LOP = 1;      //cac trang thai
        private const int O_LAI_LOP = 2;

        #endregion private member variable

        #region insert

        /// <summary>
        /// Thêm m?i k?t chuy?n d? li?u
        /// <author>dungnt</author>
        /// <date>05/09/2012</date>
        /// </summary>
        /// <param name="insertClassForwarding">Ð?i tu?ng Insert</param>
        /// <returns></returns>
        public override ClassForwarding Insert(ClassForwarding insertClassForwarding)
        {
            //Nam h?c không t?n t?i ho?c dã b? xóa. Ki?m tra AcademicYearID
            AcademicYearBusiness.CheckAvailable(insertClassForwarding.AcademicYearID, "AcademicYear_Label_AcademicYearID", false);

            //Tru?ng h?c không t?n t?i ho?c dã b? xóa. Ki?m tra SchoolID
            SchoolProfileBusiness.CheckAvailable(insertClassForwarding.SchoolID, "SchoolProfile_Label_SchoolProfileID", false);

            //L?p cu không t?n t?i ho?c dã b? xóa. Ki?m tra ClosedClassID
            if (insertClassForwarding.ClosedClassID != 0)
            {
                ClassProfileBusiness.CheckAvailable(insertClassForwarding.ClosedClassID, "ClassProfile_Label_ClassProfileID", false);
            }

            //L?p chuy?n lên không t?n t?i ho?c dã b? xóa. Ki?m tra ForwardedClassID
            if (insertClassForwarding.ForwardedClassID != 0)
            {
                ClassProfileBusiness.CheckAvailable(insertClassForwarding.ForwardedClassID, "ClassProfile_Label_ClassProfileID", false);

            }

            //L?p luu ban không t?n t?i ho?c dã b? xóa. Ki?m tra RepeatedClassID
            //if(insertClassForwarding.RepeatedClassID!=0)
            //{
            //    ClassProfileBusiness.CheckAvailable(insertClassForwarding.RepeatedClassID, "ClassProfile_Label_ClassProfileID", false);

            //}

            IQueryable<AcademicYear> lsAcademicYear = AcademicYearRepository.All;

            lsAcademicYear = lsAcademicYear.Where(em => ((em.SchoolID == insertClassForwarding.SchoolID) && (em.AcademicYearID == insertClassForwarding.AcademicYearID)));
            AcademicYear extractly = lsAcademicYear.FirstOrDefault();

            //Ngày chuy?n không thu?c h?c kì 1 c?a nam h?c m?i. Ki?m tra ForwardedDate
            //Utils.ValidateAfterDate(extractly.FirstSemesterEndDate.Value, insertClassForwarding.ForwardedDate.Value,
            //    "AcademicYear_Label_FirstSemesterEndDate", "ClassForwarding_Label_ForwardedDate");

            //Utils.ValidateAfterDate(insertClassForwarding.ForwardedDate, extractly.FirstSemesterStartDate.Value,
            //   "ClassForwarding_Label_ForwardedDate", "AcademicYear_Label_FirstSemesterStartDate");

            //insert
            return base.Insert(insertClassForwarding);
        }

        #endregion insert

        #region update

        /// <summary>
        /// Update k?t chuy?n d? li?u
        /// <author>dungnt</author>
        /// <date>05/09/2012</date>
        /// </summary>
        /// <param name="insertClassForwarding">Ð?i tu?ng Update</param>
        /// <returns></returns>
        public override ClassForwarding Update(ClassForwarding insertClassForwarding)
        {
            //Nam h?c không t?n t?i ho?c dã b? xóa. Ki?m tra AcademicYearID
            AcademicYearBusiness.CheckAvailable(insertClassForwarding.AcademicYearID, "AcademicYear_Label_AcademicYearID", false);

            //Tru?ng h?c không t?n t?i ho?c dã b? xóa. Ki?m tra SchoolID
            SchoolProfileBusiness.CheckAvailable(insertClassForwarding.SchoolID, "SchoolProfile_Label_SchoolProfileID", false);

            //L?p cu không t?n t?i ho?c dã b? xóa. Ki?m tra ClosedClassID
            ClassProfileBusiness.CheckAvailable(insertClassForwarding.ClosedClassID, "ClassProfile_Label_ClassProfileID", false);

            //L?p chuy?n lên không t?n t?i ho?c dã b? xóa. Ki?m tra ForwardedClassID
            ClassProfileBusiness.CheckAvailable(insertClassForwarding.ForwardedClassID, "ClassProfile_Label_ClassProfileID", false);

            //L?p luu ban không t?n t?i ho?c dã b? xóa. Ki?m tra RepeatedClassID
            //ClassProfileBusiness.CheckAvailable(insertClassForwarding.RepeatedClassID, "ClassProfile_Label_ClassProfileID", false);

            IQueryable<AcademicYear> lsAcademicYear = AcademicYearRepository.All;

            lsAcademicYear = lsAcademicYear.Where(em => ((em.SchoolID == insertClassForwarding.SchoolID) && (em.AcademicYearID == insertClassForwarding.AcademicYearID)));
            AcademicYear extractly = lsAcademicYear.FirstOrDefault();

            ////Ngày chuy?n không thu?c h?c kì 1 c?a nam h?c m?i. Ki?m tra ForwardedDate
            //Utils.ValidateAfterDate(extractly.FirstSemesterEndDate.Value, insertClassForwarding.ForwardedDate.Value,
            //     "AcademicYear_Label_FirstSemesterEndDate", "ClassForwarding_Label_ForwardedDate");

            /*Utils.ValidateAfterDate(insertClassForwarding.ForwardedDate, extractly.FirstSemesterStartDate.Value,
               "ClassForwarding_Label_ForwardedDate", "AcademicYear_Label_FirstSemesterStartDate");*/

            //check id
            CheckAvailable(insertClassForwarding.ClassForwardingID, "ClassForwarding_Label_ClassForwardingID", false);

            return base.Update(insertClassForwarding);
        }

        #endregion update

        #region Delete

        /// <summary>
        /// Xóa k?t chuy?n d? li?u
        /// <author>dungnt</author>
        /// <date>05/09/2012</date>
        /// </summary>
        /// <param name="ClassForwardingId">ID d?i tu?ng c?n xóa</param>
        public void Delete(int ClassForwardingId, int AcademicYearID)
        {
            //Ð?i tu?ng không t?n t?i ho?c dã b? xóa
            CheckAvailable(ClassForwardingId, "ClassForwarding_Label_ClassForwardingID", false);

            // Kiem tra nam hoc
            ClassForwarding ClassForwarding = Find(ClassForwardingId);
            if (ClassForwarding.AcademicYearID != AcademicYearID)
            {
                throw new BusinessException("ClassForwarding_Label_School_Err");
            }

            //da su dung hay chua
            //   this.CheckConstraints(GlobalConstants.LIST_SCHEMA, "ClassForwarding", ClassForwardingId, "ClassForwarding_Label_PriorityID");

            base.Delete(ClassForwardingId);
        }

        #endregion Delete

        #region Search

        /// <summary>
        ///Tìm ki?m thông tin K?t chuy?n d? li?u
        /// <author>dungnt</author>
        /// <date>05/09/2012</date>
        /// </summary>
        /// <param name="dic">tham so tim kiem</param>
        /// <returns>danh sach tim duoc</returns>
        public IQueryable<ClassForwarding> Search(IDictionary<string, object> dic)
        {
            IQueryable<ClassForwarding> lsClassForwarding = this.ClassForwardingRepository.All;

            int AcademicYearID = Utils.GetInt(dic, "AcademicYearID");
            int SchoolID = Utils.GetInt(dic, "SchoolID");
            int ClosedClassID = Utils.GetInt(dic, "ClosedClassID");
            int ForwardedClassID = Utils.GetInt(dic, "ForwardedClassID");
            int RepeatedClassID = Utils.GetInt(dic, "RepeatedClassID");

            if (ClosedClassID != 0)
            {
                lsClassForwarding = from cf in lsClassForwarding
                                    join cp in ClassProfileBusiness.All on cf.ClosedClassID equals cp.ClassProfileID
                                    where (!cp.IsActive.HasValue || (cp.IsActive.HasValue && cp.IsActive.Value))
                                    && cf.ClosedClassID == ClosedClassID
                                    select cf;
            }
            if (ForwardedClassID != 0)
            {
                lsClassForwarding = from cf in lsClassForwarding
                                    join cp in ClassProfileBusiness.All on cf.ForwardedClassID equals cp.ClassProfileID
                                    where (!cp.IsActive.HasValue || (cp.IsActive.HasValue && cp.IsActive.Value))
                                    && cf.ForwardedClassID == ForwardedClassID
                                    select cf;
            }
            if (AcademicYearID != 0)
            {
                lsClassForwarding = lsClassForwarding.Where(em => (em.AcademicYearID == AcademicYearID));
            }
            if (SchoolID != 0)
            {
                lsClassForwarding = lsClassForwarding.Where(em => (em.SchoolID == SchoolID));
            }
            //if (RepeatedClassID != 0)
            //{
            //    lsClassForwarding = lsClassForwarding.Where(em => (em.RepeatedClassID == RepeatedClassID));
            //}
            return lsClassForwarding;
        }

        /// <summary>
        /// Tìm ki?m thông tin K?t chuy?n d? li?u theo id nam h?c
        /// <author>dungnt</author>
        /// <date>05/09/2012</date>
        /// </summary>
        /// <param name="AcademicYearID"></param>
        /// <param name="dic"></param>
        /// <returns></returns>
        public IQueryable<ClassForwarding> SearchByAcademicYear(int AcademicYearID, IDictionary<string, object> dic)
        {
            if (AcademicYearID == 0)
            {
                return null;
            }
            else
            {
                dic["AcademicYearID"] = AcademicYearID;
                return Search(dic);
            }
        }

        #endregion Search

        #region transfer data

        #region Nghiep vu ket chuyen cu
        /// <summary>
        /// K?t chuy?n d? li?u
        /// <author>dungnt</author>
        /// <date>05/09/2012</date>
        /// </summary>
        /// <param name="lstClass">Danh sách l?p c?n k?t chuy?n</param>
        /// <param name="lstUpClass">Danh sách l?p chuy?n lên</param>
        /// <param name="lstDownClass">Danh sách l?p luu ban</param>
        /// <param name="OldAcademicYearID">ID nam h?c cu</param>
        /// <param name="CurrentAcademicYearID">ID nam h?c hi?n t?i</param>
        /// <param name="EducationLevelID">	Kh?i h?c</param>
        /// <param name="SchoolID"></param>
        /// Hàm không dùng n?a do có nghi?p v? m?i
        public void TransferData(List<int> lstClass, List<int> lstUpClass, List<int> lstDownClass, int OldAcademicYearID,
           int CurrentAcademicYearID, int EducationLevelID, int SchoolID, int UserID)
        {
            List<PupilOfClassRanking> lstPupilRanking = new List<PupilOfClassRanking>();

            //Trong lstClass có l?p không thu?c qu?n lý c?a tru?ng có SchoolID tuong ?ng
            IQueryable<ClassProfile> lsClassProfile = this.ClassProfileRepository.All.Where(c => c.IsActive == true);
            for (int i = 0; i < lstClass.Count(); i++)
            {
                int temp = lstClass[i];     //tranh loi LINQ to Entities does not recognize the method 'Int32 get_Item(Int32)' method
                IQueryable<ClassProfile> lsSingleClass = lsClassProfile.Where(em => ((em.SchoolID == SchoolID) && (em.ClassProfileID == temp)));
                if (lsSingleClass == null || lsSingleClass.Count() == 0)
                {
                    throw new BusinessException("SchoolProfile_Label_SchoolID");
                }

                //Ch?y t?ng ID trong lstClass, g?i hàm PupilRankingBusiness.TransferData v?i các tham s?
                IDictionary<string, object> dic = new Dictionary<string, object>();
                dic["ClassID"] = lstClass[i];
                dic["AcademicYearID"] = OldAcademicYearID;
                dic["EducationLevelID"] = EducationLevelID;
                dic["Period"] = null;
                dic["Semester"] = null;
                dic["ProfileStatus"] = 1;
                lstPupilRanking = new PupilRankingBusiness(null).TransferData(dic);

                //            K?t qu? thu du?c danh sách h?c sinh trong l?p v?i k?t qu? c?a h?c sinh dó nam h?c cu lstPupilRanking
                //N?u có h?c sinh có tr?ng thái khác lên l?p ho?c ? l?i l?p (Ki?m tra StudyingJudgementID)
                //            thì thông báo không th? k?t chuy?n do l?p chua x?p h?ng l?i cho h?c sinh
                foreach (var item in lstPupilRanking)
                {
                    if (item.StudyingJudgementID != LEN_LOP || item.StudyingJudgementID != O_LAI_LOP)
                    {
                        throw new BusinessException("ClassForwarding_Label_TransferError");
                    }
                }

                //Tìm ki?m xem h?c sinh này dã du?c k?t chuy?n chua b?ng cách g?i hàm
                //PupilOfClassBusiness.SearchPupilOfClass theo các tham s?
                IDictionary<string, object> dic2 = new Dictionary<string, object>();
                dic2["PupilID"] = null;
                dic2["AcademicYearID"] = CurrentAcademicYearID;
                dic2["SchoolID"] = SchoolID;
                dic2["Status"] = 1;
                IQueryable<PupilOfClass> lstPupilOfClass = new PupilOfClassBusiness(null).SearchBySchool(SchoolID, dic2);

                //N?u tìm du?c danh sách h?c sinh dã k?t chuy?n lstPupilOfClass thì
                // xóa b?ng cách g?i hàm PupilOfClassBusiness.DeletePupilOfClass(lstPupilOfClass)
                if (lstPupilOfClass.Count() > 0)
                {
                    new PupilOfClassBusiness(null).DeletePupilOfClass(lstPupilOfClass.ToList());
                }

                //Th?c hi?n k?t chuy?n lên l?p
                //Th?c hi?n ki?m tra t?ng h?c sinh trong lstPupilRanking l?y ra b?n ghi có StudyingJudgementID = lên l?p
                //C?p nh?t các giá tr? m?i cho các b?n ghi dó:

                foreach (var singlePupilRanking in lstPupilRanking)
                {
                    if (singlePupilRanking.StudyingJudgementID == LEN_LOP)
                    {
                        PupilOfClass singlePupil = new PupilOfClass();
                        singlePupil.ClassID = lstUpClass[i];
                        singlePupil.AcademicYearID = CurrentAcademicYearID;
                        singlePupil.SchoolID = SchoolID;
                        singlePupil.Year = (int?)AcademicYearRepository.Find(CurrentAcademicYearID).Year;
                        singlePupil.AssignedDate = DateTime.Now;
                        singlePupil.Status = 1;

                        new PupilOfClassBusiness(null).InsertPupilOfClass(UserID, singlePupil);
                    }
                    else if (singlePupilRanking.StudyingJudgementID == O_LAI_LOP)
                    {
                        PupilOfClass singlePupil = new PupilOfClass();
                        singlePupil.ClassID = lstDownClass[i];
                        singlePupil.AcademicYearID = CurrentAcademicYearID;
                        singlePupil.SchoolID = SchoolID;
                        singlePupil.Year = (int?)AcademicYearRepository.Find(CurrentAcademicYearID).Year;
                        singlePupil.AssignedDate = DateTime.Now;
                        singlePupil.Status = 1;

                        new PupilOfClassBusiness(null).InsertPupilOfClass(UserID, singlePupil);
                    }
                }

                ClassForwarding insertClassForwarding = new ClassForwarding();
                insertClassForwarding.AcademicYearID = CurrentAcademicYearID;
                insertClassForwarding.SchoolID = SchoolID;
                insertClassForwarding.ClosedClassID = lstClass[i];
                insertClassForwarding.ForwardedClassID = lstUpClass[i];
                //insertClassForwarding.RepeatedClassID = lstDownClass[i];

                insertClassForwarding.TotalForwardedPupil = (int?)lstPupilRanking.Where(o => (o.StudyingJudgementID == LEN_LOP)).Count();
                insertClassForwarding.TotalRepeatedPupil = (int?)lstPupilRanking.Where(o => (o.StudyingJudgementID == O_LAI_LOP)).Count();

                insertClassForwarding.ForwardedDate = DateTime.Now;

                //G?i hàm ClassForwardingBusiness.Insert(ClassForwarding)
                Insert(insertClassForwarding);
            }
        }
        #endregion


        /// <summary>
        /// Ketn chuyen hoc sinh len lop
        /// </summary>
        /// <param name="dicClassFw"></param>
        /// <param name="AcademicYearID"></param>
        /// <param name="SchoolID"></param>
        /// <param name="EducationLevelID"></param>
        public void TransferData(Dictionary<int, int> dicClassFw, bool TransferAllPupilInSelectedClass, int AcademicYearID, int SchoolID, int EducationLevelID)
        {
            try
            {

                SetAutoDetectChangesEnabled(false);
                // Lay nam hoc hien tai
                AcademicYear currentAcademicYear = AcademicYearBusiness.Find(AcademicYearID);
                // Lay ID cua nam hoc truoc
                int previousAcademicYearID = this.AcademicYearBusiness.All.Where(ay => ay.SchoolID == SchoolID && ay.Year == currentAcademicYear.Year - 1 && ay.IsActive == true).Select(p => p.AcademicYearID).FirstOrDefault(); // previousAcademicYear.AcademicYearID;// 20723;// 
                                                                                                                                                                                                                                  // Danh sach ID cac lop ket chuyen hoc sinh di
                List<int> lstOldClassID = dicClassFw.Select(o => o.Key).ToList();
                // Danh sach ID cac lop ket chuyen hoc sinh den
                List<int> lstNewClassID = dicClassFw.Select(o => o.Value).ToList();
                //Danh sach hoc sinh o trang thai dang hoc trong danh sach lop ket chuyen
                Dictionary<string, object> paras = new Dictionary<string, object>();
                paras["AcademicYearID"] = previousAcademicYearID;
                paras["Status"] = new List<int> { { SystemParamsInFile.PUPIL_STATUS_STUDYING } };
                paras["Check"] = "Checked";
                //lấy danh sách học sinh đã được kết chuyển 
                List<int> lstPupilRepeated = this.PupilRepeatedBusiness.All.Where(p => p.EducationLevelID == EducationLevelID
                                                                        && p.SchoolID == SchoolID && p.AcademicYearID == AcademicYearID
                                                                        ).Select(p => p.PupilID).ToList();

                // Danh sach cac hoc sinh dang hoc trong cac lop ket chuyen hoc sinh di
                var getListPupilOfOldClass = from poc in PupilOfClassBusiness.SearchBySchool(SchoolID, paras)
                                             where lstOldClassID.Contains(poc.ClassID)
                                                && poc.Status == GlobalConstants.PUPIL_STATUS_STUDYING
                                             select poc;
                List<PupilOfClass> lstPupilOfOldClass = getListPupilOfOldClass.ToList();
                // Danh sach hoc sinh duoc len lop trong danh sach lop ket chuyen
                List<PupilForwardingBO> lstPupilUpClass;
                if (EducationLevelID > 12) // Neu la mau giao thi tât ca hoc sinh deu thuoc dien len lop
                {
                    lstPupilUpClass = lstPupilOfOldClass.Select(o => new PupilForwardingBO() { PupilID = o.PupilID, ClassID = o.ClassID }).ToList();
                }
                else
                {
                    if (!TransferAllPupilInSelectedClass)
                    {
                        lstPupilUpClass = PupilRankingBusiness.GetPupilUpClass(EducationLevelID, previousAcademicYearID, lstOldClassID);
                    }
                    else // Neu nguoi dung lua chon ket chuyen tat ca cac hoc sinh trong lop duoc chon thi tat ca cac hoc sinh se thuoc dien len lop
                    {
                        lstPupilUpClass = lstPupilOfOldClass.Select(o => new PupilForwardingBO() { PupilID = o.PupilID, ClassID = o.ClassID }).ToList();
                    }
                }

                // loc ra cac hoc sinh da duoc ket chuyen luu ban 
                lstPupilUpClass = (from lst in lstPupilUpClass
                                   where !lstPupilRepeated.Contains(lst.PupilID)
                                   select lst).ToList();

                // Danh sach hoc sinh se duoc ket chuyen len lop
                var queryListPupilInPreviousClass = from pp in this.PupilProfileBusiness.All
                                                    join oldpoc in this.PupilOfClassBusiness.All on pp.PupilProfileID equals oldpoc.PupilID
                                                    where lstOldClassID.Contains(oldpoc.ClassID) && oldpoc.AcademicYearID == previousAcademicYearID && oldpoc.SchoolID == SchoolID && oldpoc.ClassProfile.EducationLevelID == EducationLevelID // Dieu kien chon lop nam hoc truoc
                                                        && oldpoc.Status == SystemParamsInFile.PUPIL_STATUS_STUDYING // Chon hoc sinh dang hoc trong nam hoc truoc
                                                    select new
                                                    {
                                                        PupilProfile = pp, // Hoc sinh
                                                        PupilOfClassInPreviousAcademicYear = oldpoc, // Lop nam hoc truoc
                                                        PupilOfClassInCurrentAcademicYear = pp.PupilOfClasses.Where(newpoc => newpoc.AcademicYearID == AcademicYearID && newpoc.SchoolID == SchoolID && newpoc.ClassProfile.EducationLevelID == EducationLevelID + 1) // Lop nam hoc hien tai (dang le goi FirstOrDefault() o day nhung bi loi nen se goi sau)
                                                    };
                var lstPupilInPreviousClass = queryListPupilInPreviousClass.ToList();
                // Danh sach hoc sinh se duoc ket chuyen len lop
                var queryListPupilWillBeTransfered = from pt in lstPupilInPreviousClass
                                                     join pu in lstPupilUpClass
                                                      on new { PupilProfileID = pt.PupilProfile.PupilProfileID, PreviousClassID = pt.PupilOfClassInPreviousAcademicYear.ClassID }
                                                      equals new { PupilProfileID = pu.PupilID, PreviousClassID = pu.ClassID }
                                                     select pt;
                var lstPupilWillBeTransfered = queryListPupilWillBeTransfered
                    .Select(p => new
                    {
                        PupilProfile = p.PupilProfile,
                        PupilOfClassInPreviousAcademicYear = p.PupilOfClassInPreviousAcademicYear,
                        PupilOfClassInCurrentAcademicYear = p.PupilOfClassInCurrentAcademicYear.FirstOrDefault()
                    }).Distinct().ToList();

                int partitionId = UtilsBusiness.GetPartionId(SchoolID, 100);
                foreach (int beingVisitedClass in lstOldClassID) // Duyet qua danh sach cac lop co hoc sinh ket chuyen di
                {
                    int newClassID = dicClassFw[beingVisitedClass]; // Lop ket chuyen den tuong ung voi lop chuyen di
                                                                    // Danh sach hoc sinh len lop trong lop ket chuyen di dang xet
                    var lstPupilWillBeTransferedOfBeingVisitedClass = lstPupilWillBeTransfered.Where(pt => pt.PupilOfClassInPreviousAcademicYear.ClassID == beingVisitedClass);
                    foreach (var PupilWillBeTransfered in lstPupilWillBeTransferedOfBeingVisitedClass) // Duyet qua danh sach cac hoc sinh se duoc ket chuyen lop dang xet
                    {
                        // Kiem tra xem hoc sinh da co trong nam hoc hien tai chua, neu co roi thi update, neu chua thi insert
                        PupilOfClass pupilAddedIntoNewClass = PupilWillBeTransfered.PupilOfClassInCurrentAcademicYear;
                        bool insertPupilIntoNewClass;
                        if (pupilAddedIntoNewClass == null)
                        {
                            pupilAddedIntoNewClass = new PupilOfClass();
                            insertPupilIntoNewClass = true;
                        }
                        else
                        {
                            insertPupilIntoNewClass = false;
                        }
                        pupilAddedIntoNewClass.Last2digitNumberSchool = partitionId;
                        pupilAddedIntoNewClass.ClassID = newClassID;
                        pupilAddedIntoNewClass.AcademicYearID = AcademicYearID;
                        pupilAddedIntoNewClass.SchoolID = SchoolID;
                        pupilAddedIntoNewClass.Year = currentAcademicYear.Year;
                        pupilAddedIntoNewClass.AssignedDate = currentAcademicYear.FirstSemesterStartDate;
                        pupilAddedIntoNewClass.Status = SystemParamsInFile.PUPIL_STATUS_STUDYING;
                        pupilAddedIntoNewClass.Description = PupilWillBeTransfered.PupilOfClassInPreviousAcademicYear.Description; // Copy thuoc tinh cua nam hoc truoc
                        pupilAddedIntoNewClass.NoConductEstimation = PupilWillBeTransfered.PupilOfClassInPreviousAcademicYear.NoConductEstimation; // Copy thuoc tinh cua nam hoc truoc
                        pupilAddedIntoNewClass.OrderInClass = PupilWillBeTransfered.PupilOfClassInPreviousAcademicYear.OrderInClass; // Copy thuoc tinh cua nam hoc truoc
                        pupilAddedIntoNewClass.PupilID = PupilWillBeTransfered.PupilOfClassInPreviousAcademicYear.PupilID; // Copy thuoc tinh cua nam hoc truoc
                        if (insertPupilIntoNewClass)
                        {
                            this.PupilOfClassBusiness.Insert(pupilAddedIntoNewClass);
                        }
                        else
                        {
                            this.PupilOfClassBusiness.Update(pupilAddedIntoNewClass);
                        }
                        // Chinh lai CurrentClassID va CurrentAcademicYearID cho dung.
                        PupilProfile pupilProfile = PupilWillBeTransfered.PupilProfile;
                        pupilProfile.CurrentClassID = newClassID;
                        pupilProfile.CurrentAcademicYearID = AcademicYearID;
                        this.PupilProfileBusiness.Update(pupilProfile);
                    }
                    // Tao ban ghi luu lai thong tin ve lop da ket chuyen va so luong hoc sinh da ket chuyen
                    ClassForwarding cf = new ClassForwarding();
                    cf.AcademicYearID = AcademicYearID;
                    cf.SchoolID = SchoolID;
                    cf.ClosedClassID = beingVisitedClass;
                    cf.ForwardedClassID = newClassID;
                    cf.TotalForwardedPupil = lstPupilWillBeTransferedOfBeingVisitedClass.Count();
                    cf.ForwardedDate = DateTime.Now;
                    base.Insert(cf);
                }

                this.SaveDetect();
            }
            finally
            {
                SetAutoDetectChangesEnabled(true);
            }
        }
        #endregion transfer data


        /// <summary>
        /// GetTransferData
        /// </summary>
        /// <param name="EducationLevelID">The education level ID.</param>
        /// <param name="SchoolID">The school ID.</param>
        /// <param name="AcademicYearID">The academic year ID.</param>
        /// <returns></returns>
        /// <author>
        /// dungnt77
        /// </author>
        /// <remarks>
        /// 04/01/2013   3:21 PM
        /// </remarks>
        public List<ClassForwardingBO> GetTransferData(int EducationLevelID, int SchoolID, int AcademicYearID)
        {
            AcademicYear currentAY = AcademicYearBusiness.Find(AcademicYearID);
            AcademicYear oldAY = AcademicYearBusiness.SearchBySchool(SchoolID, new Dictionary<string, object>() { { "Year", currentAY.Year - 1 } }).FirstOrDefault();

            if (oldAY == null)
            {
                throw new BusinessException("ClassForwarding_Label_ErrorNoOldAcademicYear");
                //oldAY = currentAY;
            }

            IQueryable<ClassProfile> iqOldClass = ClassProfileBusiness.SearchBySchool(SchoolID, new Dictionary<string, object>()
            {
            {"AcademicYearID",oldAY.AcademicYearID},
            {"EducationLevelID",EducationLevelID}
            });
            List<ClassProfile> lstOldClass = iqOldClass.ToList();
            List<int> listOldClassID = lstOldClass.OrderBy(c => c.OrderNumber).ThenBy(c => c.DisplayName).Select(o => o.ClassProfileID).ToList();
            List<ClassForwardingBO> lsResult = new List<ClassForwardingBO>();
            // Danh sach hoc sinh len lop
            int? createdAcaYear = oldAY.Year;
            List<PupilTransferBO> listPupilUp = new List<PupilTransferBO>();
            List<int> listPupilUpClassID = new List<int>();
            List<int> listPupilUpPupilID = new List<int>();
            //anhnph_sửa luống kết chuyển cấp 1
            //nếu là cấp 2&3 giữ nguyên luồng cũ
            //nếu cấp 1: xét đánh giá HKII/CN hoặc đánh gái bổ sung là HT thì mới tính là lên lớp
            // Danh sach hoc sinh thuoc dien len lop
            IDictionary<string, object> dicSearch = new Dictionary<string, object>()
            {
                {"AcademicYearID",oldAY.AcademicYearID},
                {"Status",SystemParamsInFile.PUPIL_STATUS_STUDYING}
            };
            if (EducationLevelID > MAX_EDUCATIONLEVEL_PRIMARY)
            {
                listPupilUp = (from prr in VPupilRankingBusiness.All
                               join poc in PupilOfClassBusiness.SearchBySchool(SchoolID, dicSearch) on new { prr.PupilID, prr.ClassID } equals new { poc.PupilID, poc.ClassID }
                               join cp in ClassProfileBusiness.All on poc.ClassID equals cp.ClassProfileID
                               where prr.Last2digitNumberSchool == SchoolID % 100
                               && prr.CreatedAcademicYear == createdAcaYear
                               && prr.SchoolID == SchoolID
                               && prr.AcademicYearID == oldAY.AcademicYearID
                               && prr.EducationLevelID == EducationLevelID
                               && prr.Semester >= SystemParamsInFile.SEMESTER_OF_YEAR_SECOND
                               && prr.StudyingJudgementID == SystemParamsInFile.STUDYING_JUDGEMENT_UPCLASS
                               && listOldClassID.Contains(prr.ClassID)
                               && (cp.IsVnenClass == null || cp.IsVnenClass == false)
                               && cp.IsActive.Value
                               select new PupilTransferBO()
                               {
                                   PupilID = prr.PupilID,
                                   ClassID = prr.ClassID
                               }).ToList();

                //viethd4: Xet them cac lop VNEN
                List<PupilTransferBO> listPupilUpVNEN = (from poc in PupilOfClassBusiness.SearchBySchool(SchoolID, dicSearch)
                                                         join rb in ReviewBookPupilBusiness.All on new { poc.PupilID, poc.ClassID } equals new { rb.PupilID, rb.ClassID }
                                                         join cp in ClassProfileBusiness.All on poc.ClassID equals cp.ClassProfileID
                                                         where rb.SchoolID == SchoolID
                                                         && rb.AcademicYearID == oldAY.AcademicYearID
                                                         && rb.SemesterID == SystemParamsInFile.SEMESTER_OF_YEAR_SECOND
                                                         && (rb.RateAndYear == 1 || rb.RateAdd == 1)
                                                         && listOldClassID.Contains(rb.ClassID)
                                                         && (cp.IsVnenClass == true)
                                                         && cp.IsActive.Value
                                                         select new PupilTransferBO()
                                                         {
                                                             PupilID = rb.PupilID,
                                                             ClassID = rb.ClassID
                                                         }).ToList();

                listPupilUp = listPupilUp.Union(listPupilUpVNEN).ToList();
            }
            else
            {
                listPupilUp = (from sde in SummedEndingEvaluationBusiness.All
                               join poc in PupilOfClassBusiness.SearchBySchool(SchoolID, dicSearch) on sde.PupilID equals poc.PupilID
                               where sde.LastDigitSchoolID == SchoolID % 100
                               && sde.SchoolID == SchoolID
                               && sde.AcademicYearID == oldAY.AcademicYearID
                               && ((sde.EndingEvaluation != null && "HT".Equals(sde.EndingEvaluation))
                                         || (sde.EndingEvaluation != null && "CHT".Equals(sde.EndingEvaluation)
                                         && sde.RateAdd != null && sde.RateAdd == SystemParamsInFile.SUMMEDENDING_RATE_ADD_HT))
                               && listOldClassID.Contains(sde.ClassID)
                               && sde.SemesterID >= SystemParamsInFile.SEMESTER_OF_YEAR_SECOND
                               select new PupilTransferBO()
                               {
                                   PupilID = sde.PupilID,
                                   ClassID = sde.ClassID,
                               }).ToList();
            }

            listPupilUpClassID = listPupilUp != null ? listPupilUp.Select(o => o.ClassID).ToList() : listPupilUpClassID;
            listPupilUpPupilID = listPupilUp != null ? listPupilUp.Select(o => o.PupilID).ToList() : listPupilUpPupilID;
            // Lay tat ca thong tin ket chuyen
            List<ClassForwarding> listClassFw = this.Search(new Dictionary<string, object>()
               {
                    {"SchoolID",SchoolID},
                    {"AcademicYearID",AcademicYearID},
               }).Where(o => listOldClassID.Contains(o.ClosedClassID)).ToList();

            // Danh sach hoc sinh lop
            var listGroupPoc = PupilOfClassBusiness.SearchBySchool(SchoolID, new Dictionary<string, object>()
                {
                {"AcademicYearID",oldAY.AcademicYearID},
                {"Status",SystemParamsInFile.PUPIL_STATUS_STUDYING}
                }).GroupBy(o => o.ClassID).Select(o => new { ClassID = o.Key, TotalPupil = o.Count() }).ToList();

            var listGroupPocUp = listPupilUp
                .GroupBy(o => o.ClassID).Select(o => new { ClassID = o.Key, TotalPupil = o.Count() }).ToList();
            ClassForwardingBO cfbo = null;
            ClassProfile item = null;
            for (int i = 0; i < lstOldClass.Count; i++)
            {
                item = lstOldClass[i];
                int classID = item.ClassProfileID;
                cfbo = new ClassForwardingBO();
                cfbo.OldClassID = classID;
                cfbo.OldClassName = item.DisplayName;
                var pupilInClass = listGroupPoc.Where(o => o.ClassID == classID).FirstOrDefault();
                if (pupilInClass != null)
                {
                    cfbo.TotalPupil = pupilInClass.TotalPupil;
                }

                var pupilUpInClass = listGroupPocUp.Where(o => o.ClassID == classID).FirstOrDefault();
                if (pupilUpInClass != null)
                {
                    cfbo.TotalMove = pupilUpInClass.TotalPupil;
                }

                var ClassForward = listClassFw.Where(o => o.ClosedClassID == item.ClassProfileID).FirstOrDefault();

                if (ClassForward != null)
                {
                    cfbo.NewClassID = ClassForward.ForwardedClassID;
                    cfbo.TotalMoved = ClassForward.TotalForwardedPupil;
                }
                else
                {
                    cfbo.NewClassID = 0;
                    cfbo.TotalMoved = 0;
                }
                lsResult.Add(cfbo);
            }
            return lsResult;
        }

        /// <summary>
        /// CancelTransferData
        /// </summary>
        /// <param name="lstClassForwarding">The LST class forwarding.</param>
        /// <param name="AcademicYearID">The academic year ID.</param>
        /// <param name="SchoolID">The school ID.</param>
        /// <author>
        /// dungnt77
        /// </author>
        /// <remarks>
        /// 04/01/2013   3:52 PM
        /// </remarks>
        public void CancelTransferData(List<int> lstOldClass, int AcademicYearID, int SchoolID, int EducationLevelID)
        {
            int currentYear = AcademicYearBusiness.All.Where(p => p.AcademicYearID == AcademicYearID).Select(p => p.Year).FirstOrDefault();
            // Ma nam hoc truoc
            int previousAcademicYearID = this.AcademicYearBusiness.All
                .Where(ay => ay.SchoolID == SchoolID && ay.Year == currentYear - 1 && ay.IsActive == true)
                .Select(p => p.AcademicYearID).FirstOrDefault();
            Dictionary<string, object> paras = new Dictionary<string, object>();
            paras["AcademicYearID"] = previousAcademicYearID;
            paras["Status"] = new List<int> { { SystemParamsInFile.PUPIL_STATUS_STUDYING } };
            paras["Check"] = "Checked";
            // Danh sach hoc sinh da duoc ket chuyen len lop
            var queryLstPupilUntransfer = from pp in this.PupilProfileBusiness.All
                                          join oldpoc in this.PupilOfClassRepository.All on pp.PupilProfileID equals oldpoc.PupilID
                                          join newpoc in this.PupilOfClassRepository.All on pp.PupilProfileID equals newpoc.PupilID
                                          where oldpoc.AcademicYearID == previousAcademicYearID && oldpoc.SchoolID == SchoolID && oldpoc.ClassProfile.EducationLevelID == EducationLevelID && lstOldClass.Contains(oldpoc.ClassID) // Dieu kien chon lop nam hoc truoc
                                              && oldpoc.Status == SystemParamsInFile.PUPIL_STATUS_STUDYING  // Chon hoc sinh dang hoc trong lop hoc nam truoc
                                              && newpoc.AcademicYearID == AcademicYearID && newpoc.SchoolID == SchoolID && newpoc.ClassProfile.EducationLevelID == EducationLevelID + 1 // Dieu kien chon lop nam hoc hien tai
                                          select new
                                          {
                                              PupilProfile = pp, // Hoc sinh
                                              PupilOfClassInPreviousAcademicYear = oldpoc, // Lop nam hoc truoc
                                              PupilOfClassInCurrentAcademicYear = newpoc // Lop nam hoc hien tai
                                          };
            var lstPupilUntransfer = queryLstPupilUntransfer.ToList();
            // Xet tung lop duoc lua chon de huy ket chuyen
            foreach (int oldClassID in lstOldClass)
            {
                // Danh sach hoc sinh thuoc lop dang xet da ket chuyen len lop
                var lstPupilUntransferOfVisitedClass = lstPupilUntransfer.Where(p => p.PupilOfClassInPreviousAcademicYear.ClassID == oldClassID);
                // Chinh lai CurrentClassID va CurrentAcademicYearID cua hoc sinh ve nam hoc cu
                foreach (var p in lstPupilUntransferOfVisitedClass)
                {
                    PupilProfile pp = p.PupilProfile;
                    pp.CurrentClassID = oldClassID;
                    pp.CurrentAcademicYearID = previousAcademicYearID;
                    this.PupilProfileBusiness.Update(pp);
                }
                // Xoa cac ban ghi PupilOfClass trong nam hoc hien tai
                var lstPupilOfClassInCurrentAcademicYearOfVisitedClass = lstPupilUntransferOfVisitedClass.Select(a => a.PupilOfClassInCurrentAcademicYear).ToList();
                PupilOfClassBusiness.DeleteAll(lstPupilOfClassInCurrentAcademicYearOfVisitedClass);
            }
            // Xoa ban luu trong bang ket chuyen
            var lstClassForwardingDelete = this.ClassForwardingBusiness.All.Where(cf => lstOldClass.Contains(cf.ClosedClassID)).ToList();
            this.ClassForwardingBusiness.DeleteAll(lstClassForwardingDelete);
        }

        /// <summary>
        /// anhnph1_20150708_lấy ra danh sách học sinh lên lớp của lớp được chọn
        /// </summary>
        /// <param name="SchoolID"></param>
        /// <param name="AcademicYearID"></param>
        /// <param name="EducationLevelID"></param>
        /// <param name="ClassID"></param>
        /// <returns></returns>
        public List<PupilForwardBO> GetPupilForward(int SchoolID, int AcademicYearID, int EducationLevelID, int ClassID, bool TransferAllPupilInSelectedClass = false)
        {
            var currentYear = AcademicYearBusiness.All.Where(o => o.AcademicYearID == AcademicYearID).First();
            var oldAY = AcademicYearBusiness.SearchBySchool(SchoolID, new Dictionary<string, object>() { { "Year", currentYear.Year - 1 } })
                .Select(o => new { o.AcademicYearID, o.Year })
                .FirstOrDefault();
            int partitionId = UtilsBusiness.GetPartionId(SchoolID);
            if (oldAY == null)
            {
                throw new BusinessException("ClassForwarding_Label_ErrorNoOldAcademicYear");
                //oldAY = currentAY;
            }

            List<int> lstOldClass = new List<int>();
            if (ClassID == 0)
            {
                lstOldClass = ClassProfileBusiness.SearchBySchool(SchoolID, new Dictionary<string, object>()
                {
                    {"AcademicYearID",oldAY.AcademicYearID}
                    ,{"EducationLevelID",EducationLevelID}
                }).Select(o => o.ClassProfileID).ToList();
            }
            else
            {
                lstOldClass.Add(ClassID);
            }

            //Xử lý lấy ra danh sách học sinh lên lớp
            // Neu la mau giao thi tât ca hoc sinh deu thuoc dien len lop
            //Nếu cấp 2 & 3: Học sinh lên lớp khi  thuộc diện cả năm của học sinh lên lớp hoặc thuộc diện sau thi lại lên lớp
            //Nếu cấp 1: Đánh giá cuối kỳ II/CN HT hoặc đánh giá bổ sung HT
            IQueryable<PupilTransferBO> lsPupilRanking = null;
            // Danh sach hoc sinh thuoc dien len lop
            IDictionary<string, object> dicSearch = new Dictionary<string, object>()
                {
                    {"AcademicYearID",oldAY.AcademicYearID},
                    {"Status",SystemParamsInFile.PUPIL_STATUS_STUDYING},
                    {"ClassID",ClassID},
                    {"EducationLevelID",EducationLevelID}
                };
            int? createdAcaYear = oldAY.Year;
            if (EducationLevelID > 12) // Neu la mau giao thi tât ca hoc sinh deu thuoc dien len lop
            {
                lsPupilRanking = (from poc in PupilOfClassBusiness.SearchBySchool(SchoolID, dicSearch)
                                  where lstOldClass.Contains(poc.ClassID)
                                    && poc.Status == GlobalConstants.PUPIL_STATUS_STUDYING
                                  select new PupilTransferBO()
                                  {
                                      PupilID = poc.PupilID,
                                      ClassID = poc.ClassID
                                  });
            }
            else if (EducationLevelID > MAX_EDUCATIONLEVEL_PRIMARY)
            {

                if (!TransferAllPupilInSelectedClass)
                {
                    lsPupilRanking = (from poc in PupilOfClassBusiness.SearchBySchool(SchoolID, dicSearch)
                                      join prr in VPupilRankingBusiness.All on new { poc.ClassID, poc.PupilID } equals new { prr.ClassID, prr.PupilID }
                                      join cp in ClassProfileBusiness.All on poc.ClassID equals cp.ClassProfileID
                                      where prr.Last2digitNumberSchool == partitionId
                                      && prr.CreatedAcademicYear == createdAcaYear
                                      && prr.SchoolID == SchoolID
                                      && prr.AcademicYearID == oldAY.AcademicYearID
                                      && prr.StudyingJudgementID == SystemParamsInFile.STUDYING_JUDGEMENT_UPCLASS
                                      && prr.PeriodID == null
                                      && prr.Semester >= SystemParamsInFile.SEMESTER_OF_YEAR_ALL
                                      && lstOldClass.Contains(prr.ClassID)
                                      && (cp.IsVnenClass == null || cp.IsVnenClass == false)
                                      && cp.IsActive.Value
                                      select new PupilTransferBO()
                                      {
                                          PupilID = prr.PupilID,
                                          ClassID = prr.ClassID
                                      });

                    //viethd4: Xet them cac lop VNEN
                    IQueryable<PupilTransferBO> lstPupilRankingVNEN = (from poc in PupilOfClassBusiness.SearchBySchool(SchoolID, dicSearch)
                                                                       join rb in ReviewBookPupilBusiness.All on new { poc.PupilID, poc.ClassID } equals new { rb.PupilID, rb.ClassID }
                                                                       join cp in ClassProfileBusiness.All on poc.ClassID equals cp.ClassProfileID
                                                                       where rb.SchoolID == SchoolID
                                                                       && rb.AcademicYearID == oldAY.AcademicYearID
                                                                       && (rb.RateAndYear == 1 || rb.RateAdd == 1)
                                                                       && rb.SemesterID == SystemParamsInFile.SEMESTER_OF_YEAR_SECOND
                                                                       && lstOldClass.Contains(rb.ClassID)
                                                                       && (cp.IsVnenClass == true)
                                                                       && cp.IsActive.Value
                                                                       select new PupilTransferBO()
                                                                       {
                                                                           PupilID = rb.PupilID,
                                                                           ClassID = rb.ClassID
                                                                       });

                    lsPupilRanking = lsPupilRanking.Union(lstPupilRankingVNEN);
                }
                else
                {
                    lsPupilRanking = (from poc in PupilOfClassBusiness.SearchBySchool(SchoolID, dicSearch)
                                      select new PupilTransferBO()
                                      {
                                          PupilID = poc.PupilID,
                                          ClassID = poc.ClassID
                                      });
                }

            }
            else
            {
                if (!TransferAllPupilInSelectedClass)
                {
                    lsPupilRanking = (from poc in PupilOfClassBusiness.SearchBySchool(SchoolID, dicSearch)
                                      join sde in SummedEndingEvaluationBusiness.All on new { poc.PupilID, poc.ClassID } equals new { sde.PupilID, sde.ClassID }
                                      where sde.LastDigitSchoolID == partitionId
                                      && sde.SchoolID == SchoolID
                                      && sde.AcademicYearID == oldAY.AcademicYearID
                                      && sde.SemesterID >= SystemParamsInFile.SEMESTER_OF_YEAR_SECOND
                                      && ((sde.EndingEvaluation != null && "HT".Equals(sde.EndingEvaluation))
                                            || (sde.EndingEvaluation != null && "CHT".Equals(sde.EndingEvaluation)
                                            && sde.RateAdd != null && sde.RateAdd == SystemParamsInFile.SUMMEDENDING_RATE_ADD_HT))
                                      && lstOldClass.Contains(sde.ClassID)
                                      select new PupilTransferBO()
                                      {
                                          PupilID = sde.PupilID,
                                          ClassID = sde.ClassID,
                                      });
                }
                else
                {
                    lsPupilRanking = (from poc in PupilOfClassBusiness.SearchBySchool(SchoolID, dicSearch)
                                      select new PupilTransferBO()
                                      {
                                          PupilID = poc.PupilID,
                                          ClassID = poc.ClassID
                                      });
                }
            }

            var lsPupilRanking2 = lsPupilRanking.ToList();

            List<int> lstPupilID = lsPupilRanking2.Select(o => o.PupilID).Distinct().ToList();
            List<int> lstClassID = lsPupilRanking2.Select(o => o.ClassID).Distinct().ToList();

            // Danh sach hoc sinh lop nam hoc cu
            var allPupilForward = PupilOfClassBusiness.SearchBySchool(SchoolID, new Dictionary<string, object>()
                {
                {"AcademicYearID",oldAY.AcademicYearID},
                {"Status",SystemParamsInFile.PUPIL_STATUS_STUDYING}
                });
            if (ClassID != 0)
            {
                allPupilForward = allPupilForward.Where(o => o.ClassID == ClassID);
            }
            else
            {
                allPupilForward = allPupilForward.Where(o => lstClassID.Contains(o.ClassID));
            }


            //Lấy ra danh sách học sinh đã được kết chuyển thành công
            var allPupilIsTranForward = PupilOfClassBusiness.SearchBySchool(SchoolID, new Dictionary<string, object>()
                {
                    {"AcademicYearID",currentYear.AcademicYearID},
                    {"Status",SystemParamsInFile.PUPIL_STATUS_STUDYING},
                    //{"ClassID",ClassID},
                    {"EducationLevelID",EducationLevelID + 1}
                }).ToList();

            List<PupilForwardBO> query = (from poc in allPupilForward
                                          join pp in PupilProfileBusiness.All on poc.PupilID equals pp.PupilProfileID
                                          where lstPupilID.Contains(poc.PupilID)
                                          select new PupilForwardBO
                                          {
                                              AcademicYearID = AcademicYearID,
                                              BirthDate = pp.BirthDate,
                                              EducationLevelID = EducationLevelID,
                                              FullName = pp.FullName,
                                              Genre = pp.Genre,
                                              GenreName = (pp.Genre == SystemParamsInFile.GENRE_MALE) ? "Nam" : "Nữ",
                                              OldClassID = poc.ClassID,
                                              OldClassName = poc.ClassProfile.DisplayName,
                                              PupilCode = pp.PupilCode,
                                              PupilID = poc.PupilID,
                                              SchoolID = SchoolID,
                                              Year = oldAY.Year,
                                              Name = pp.Name,
                                              OrderInClass = poc.OrderInClass,
                                              OrderNumber = poc.ClassProfile.OrderNumber
                                          }).ToList();
            query = query.OrderBy(p => p.OrderNumber)
                        .ThenBy(p => p.OldClassName)
                        .ThenBy(p => p.OrderInClass)
                        .ThenBy(p => (Utils.SortABC(p.Name + " " + p.FullName))).ToList();

            foreach (var item in query)
            {
                // Fix lỗi ATTT
                //PupilOfClass pocCurrentTrans = allPupilIsTranForward.Where(p => p.PupilID == item.PupilID && p.SchoolID == p.SchoolID
                //    && p.AcademicYearID == currentYear.AcademicYearID).FirstOrDefault();
                PupilOfClass pocCurrentTrans = allPupilIsTranForward.FirstOrDefault(p => p.PupilID == item.PupilID
                    && p.SchoolID == item.SchoolID //p.SchoolID  
                    && p.AcademicYearID == currentYear.AcademicYearID
                );
                if (pocCurrentTrans != null)
                {
                    item.ForwardClassID = pocCurrentTrans.ClassID;
                }
            }
            return query;
        }

        /// <summary>
        /// anhnph_20150709_ket chuyen len lop cho hoc sinh
        /// </summary>
        /// <param name="lstPupilForwardBO"></param>
        /// <param name="AcademicYearID"></param>
        /// <param name="SchoolID"></param>
        /// <param name="EducationLevelID"></param>
        public void ForwardPupil(List<PupilForwardBO> lstPupilForwardBO, int AcademicYearID, int SchoolID, int EducationLevelID)
        {
            try
            {
                context.Configuration.AutoDetectChangesEnabled = false;
                context.Configuration.ValidateOnSaveEnabled = false;
                AcademicYear currentAcademicYear = AcademicYearBusiness.Find(AcademicYearID);
                int currentYear = AcademicYearBusiness.All.Where(o => o.AcademicYearID == AcademicYearID).Select(o => o.Year).First();
                int? oldAYID = AcademicYearBusiness.SearchBySchool(SchoolID, new Dictionary<string, object>() { { "Year", currentYear - 1 } })
                    .Select(o => o.AcademicYearID).FirstOrDefault();

                List<int> lstPupilProfileID = lstPupilForwardBO.Select(o => o.PupilID).Distinct().ToList();
                List<PupilProfile> lstPupilProfile = PupilProfileBusiness.All.Where(o => lstPupilProfileID.Contains(o.PupilProfileID) && o.IsActive == true && o.CurrentSchoolID == SchoolID).ToList();
                Dictionary<string, object> dic = new Dictionary<string, object>();
                dic["AcademicYearID"] = oldAYID;
                PupilForwardBO pfBO = null;
                List<PupilOfClass> lstPupilOfClass = this.PupilOfClassBusiness.SearchBySchool(SchoolID)
                                                     .Where(p => p.AcademicYearID == AcademicYearID
                                                     && lstPupilProfileID.Contains(p.PupilID)).ToList();
                int countPupil = 0;
                int countIsTranferPupil = 0; //dung de xac dinh so hoc sinh cua cac lop da duoc ket chuyen
                //bien luu co khi lop cu va lop chuyen khac
                PupilForwardBO flagPFBO = lstPupilForwardBO[0];
                ClassForwarding check_cf = null;
                List<ClassForwarding> lstClassForwardingCheck = ClassForwardingBusiness.All.Where(p => p.SchoolID == SchoolID
                                                                                        && p.AcademicYearID == AcademicYearID).ToList();
                for (int i = 0; i < lstPupilForwardBO.Count; i++)
                {
                    pfBO = lstPupilForwardBO[i];

                    //neu hox sinh có oldclass va forwardclass khac thi 
                    //add class forward
                    //gan lai flagPFBO                
                    //cap nhat lai bien dem hoc sinh 
                    if ((flagPFBO.OldClassID != pfBO.OldClassID && flagPFBO.ForwardClassID != pfBO.ForwardClassID) ||
                        (flagPFBO.OldClassID != pfBO.OldClassID && flagPFBO.ForwardClassID == pfBO.ForwardClassID))
                    {
                        // Tao ban ghi luu lai thong tin ve lop da ket chuyen va so luong hoc sinh da ket chuyen duyet truoc do                 
                        if (check_cf != null)
                        {
                            ClassForwardingBusiness.Update(check_cf);
                        }
                        else
                        {
                            ClassForwarding cf = new ClassForwarding();
                            cf.AcademicYearID = AcademicYearID;
                            cf.SchoolID = SchoolID;
                            cf.ClosedClassID = flagPFBO.OldClassID;
                            cf.ForwardedClassID = flagPFBO.ForwardClassID;
                            cf.TotalForwardedPupil = countPupil;
                            cf.ForwardedDate = DateTime.Now;
                            base.Insert(cf);
                        }
                        flagPFBO = lstPupilForwardBO[i];
                        countIsTranferPupil += countPupil;
                        countPupil = 0;
                        i--;
                    }
                    else
                    {
                        //Insert vao PupilOfClass
                        PupilOfClass poc2 = lstPupilOfClass.FirstOrDefault(poc => poc.ClassID == pfBO.ForwardClassID
                                                                                 && poc.PupilID == pfBO.PupilID);
                        bool insertPupilIntoNewClass;
                        if (poc2 == null)
                        {
                            poc2 = new PupilOfClass();
                            insertPupilIntoNewClass = true;
                        }
                        else
                        {
                            insertPupilIntoNewClass = false;
                        }
                        poc2.ClassID = pfBO.ForwardClassID;
                        poc2.AcademicYearID = AcademicYearID;
                        poc2.SchoolID = SchoolID;
                        poc2.Year = currentAcademicYear.Year;
                        poc2.AssignedDate = currentAcademicYear.FirstSemesterStartDate;
                        poc2.Status = SystemParamsInFile.PUPIL_STATUS_STUDYING;
                        poc2.PupilID = pfBO.PupilID;
                        poc2.OrderInClass = pfBO.OrderInClass;
                        if (insertPupilIntoNewClass)
                        {
                            this.PupilOfClassBusiness.Insert(poc2);
                        }
                        else
                        {
                            this.PupilOfClassBusiness.Update(poc2);
                        }
                        //PupilProfile
                        PupilProfile pp = lstPupilProfile.Where(o => o.PupilProfileID == pfBO.PupilID).FirstOrDefault();
                        pp.CurrentClassID = pfBO.ForwardClassID;
                        pp.CurrentAcademicYearID = AcademicYearID;
                        PupilProfileBusiness.Update(pp);

                        countPupil++;

                        check_cf = lstClassForwardingCheck.FirstOrDefault(p => p.ClosedClassID == flagPFBO.OldClassID);
                        if (check_cf != null) // co du lieu luu trong class forwrad
                        {
                            check_cf.TotalForwardedPupil = check_cf.TotalForwardedPupil + 1;
                        }
                        else
                        {
                            // truong hop tat ca hoc sinh cua mot lop cung chuyen wa lop moi
                            if (countPupil == lstPupilForwardBO.Count - countIsTranferPupil)
                            {
                                // Tao ban ghi luu lai thong tin ve lop da ket chuyen va so luong hoc sinh da ket chuyen
                                ClassForwarding cf = new ClassForwarding();
                                cf.AcademicYearID = AcademicYearID;
                                cf.SchoolID = SchoolID;
                                cf.ClosedClassID = flagPFBO.OldClassID;
                                cf.ForwardedClassID = flagPFBO.ForwardClassID;
                                cf.TotalForwardedPupil = countPupil;
                                cf.ForwardedDate = DateTime.Now;
                                base.Insert(cf);
                            }
                        }
                    }
                }
                if (check_cf != null)
                {
                    ClassForwardingBusiness.Update(check_cf);
                }

                this.Save();
            }
            catch (Exception ex)
            {
                
                string ParamList = string.Format("AcademicYearID={0}, SchoolID={1}, EducationLevelID={2}", AcademicYearID, SchoolID, EducationLevelID);
                LogExtensions.ErrorExt(logger, DateTime.Now, "ForwardPupil", ParamList, ex);
            }
            finally
            {
                context.Configuration.AutoDetectChangesEnabled = true;
                context.Configuration.ValidateOnSaveEnabled = true;
            }
        }

        /// <summary>
        /// anhnph_20150710_Huy ket chuyen len lop cho hoc sinh
        /// </summary>
        /// <param name="lstPupilForwardBO"></param>
        /// <param name="AcademicYearID"></param>
        /// <param name="SchoolID"></param>
        /// <param name="EducationLevelID"></param>
        public void CancelForwardPupil(List<PupilForwardBO> lstPupilForwardBO, int AcademicYearID, int SchoolID, int EducationLevelID)
        {
            try
            {
                context.Configuration.AutoDetectChangesEnabled = false;
                context.Configuration.ValidateOnSaveEnabled = false;
                //context.Configuration.LazyLoadingEnabled = false;
                //context.Configuration.ProxyCreationEnabled = false;
                int currentYear = AcademicYearBusiness.All.FirstOrDefault(c => c.AcademicYearID == AcademicYearID).Year;
                // Ma nam hoc truoc
                int previousAcademicYearID = this.AcademicYearBusiness.All
                    .Where(ay => ay.SchoolID == SchoolID && ay.Year == currentYear - 1)
                    .Select(p => p.AcademicYearID).FirstOrDefault();
                Dictionary<string, object> paras = new Dictionary<string, object>();
                paras["AcademicYearID"] = previousAcademicYearID;
                paras["Status"] = new List<int> { { SystemParamsInFile.PUPIL_STATUS_STUDYING } };
                paras["Check"] = "Checked";

                //danh sach cac oldclass trong lstPupilForwardBo
                List<int> lstOldClass = lstPupilForwardBO.Select(p => p.OldClassID).Distinct().ToList();
                List<int> lstPupilSelect = lstPupilForwardBO.Select(p => p.PupilID).ToList();
                // Danh sach hoc sinh da duoc ket chuyen len lop
                var queryLstPupilUntransfer = from pp in this.PupilProfileBusiness.All
                                              join oldpoc in this.PupilOfClassBusiness.All on pp.PupilProfileID equals oldpoc.PupilID
                                              join newpoc in this.PupilOfClassBusiness.All on pp.PupilProfileID equals newpoc.PupilID
                                              where oldpoc.AcademicYearID == previousAcademicYearID && oldpoc.SchoolID == SchoolID && oldpoc.ClassProfile.EducationLevelID == EducationLevelID
                                              && lstOldClass.Contains(oldpoc.ClassID) // Dieu kien chon lop nam hoc truoc
                                                && oldpoc.Status == SystemParamsInFile.PUPIL_STATUS_STUDYING  // Chon hoc sinh dang hoc trong lop hoc nam truoc
                                                && newpoc.AcademicYearID == AcademicYearID && newpoc.SchoolID == SchoolID && newpoc.ClassProfile.EducationLevelID == EducationLevelID + 1 // Dieu kien chon lop nam hoc hien tai
                                                && lstPupilSelect.Contains(pp.PupilProfileID)
                                              select new PupilTransferedBO
                                              {
                                                  PupilProfile = pp, // Hoc sinh
                                                  PupilOfClassInPreviousAcademicYear = oldpoc, // Lop nam hoc truoc
                                                  PupilOfClassInCurrentAcademicYear = newpoc // Lop nam hoc hien tai
                                              };

                var lstPupilUntransfer = queryLstPupilUntransfer.ToList();

                int countPupil = 0;
                List<ClassForwarding> lsttempDelete = new List<ClassForwarding>();
                List<ClassForwarding> lstClassForwardingUpdate = new List<ClassForwarding>();
                PupilProfile pupilProfile;
                List<ClassForwarding> lstClassForwarding = this.All.Where(cf => lstOldClass.Contains(cf.ClosedClassID)).ToList();
                List<PupilProfile> lstPupilProfile = new List<PupilProfile>();
                List<PupilOfClass> lstPupilOfClass = new List<PupilOfClass>();
                //List<PupilProfile> lstPupilProfileUpdate;
                foreach (int oldClassID in lstOldClass) //duyet danh oldclass
                {
                    var lstPupilUntransferOfClass = lstPupilUntransfer.Where(p => p.PupilOfClassInPreviousAcademicYear.ClassID == oldClassID);

                    // Chinh lai CurrentClassID va CurrentAcademicYearID cua hoc sinh ve nam hoc cu
                    foreach (PupilTransferedBO p in lstPupilUntransferOfClass)
                    {

                        //Utils.Clone(p.PupilProfile,pupilProfile);
                        pupilProfile = p.PupilProfile;
                        pupilProfile.CurrentClassID = oldClassID;
                        pupilProfile.CurrentAcademicYearID = previousAcademicYearID;
                        //PupilProfileBusiness.Update(pupilProfile);
                        lstPupilProfile.Add(pupilProfile);
                        countPupil++;
                    }
                    // Xoa cac ban ghi PupilOfClass trong nam hoc hien tai
                    var lstPupilOfClassInCurrentAcademicYearOfVisitedClass = lstPupilUntransferOfClass.Select(a => a.PupilOfClassInCurrentAcademicYear).ToList();
                    PupilOfClassBusiness.DeleteAll(lstPupilOfClassInCurrentAcademicYearOfVisitedClass);


                    var ClassForwardingDelete = lstClassForwarding.Where(p => p.ClosedClassID == oldClassID).FirstOrDefault();
                    if (ClassForwardingDelete != null)
                    {
                        if (countPupil == ClassForwardingDelete.TotalForwardedPupil.Value)
                        {
                            lsttempDelete.Add(ClassForwardingDelete);
                        }
                        else
                        {
                            ClassForwardingDelete.TotalForwardedPupil = ClassForwardingDelete.TotalForwardedPupil - countPupil;
                            //this.Update(ClassForwardingDelete);
                            lstClassForwardingUpdate.Add(ClassForwardingDelete);
                        }
                    }
                    countPupil = 0;
                }
                if (lstPupilProfile != null && lstPupilProfile.Count > 0)
                {

                    for (int i = 0; i < lstPupilProfile.Count; i++)
                    {
                        PupilProfileBusiness.UpdateExistsObjectStateManager(lstPupilProfile[i]);
                    }

                }
                // Xoa ban luu trong bang ket chuyen     
                if (lsttempDelete != null && lsttempDelete.Count > 0)
                {
                    this.ClassForwardingBusiness.DeleteAll(lsttempDelete);
                }
                if (lstClassForwardingUpdate != null)
                {
                    for (int i = 0; i < lstClassForwardingUpdate.Count; i++)
                    {
                        ClassForwardingBusiness.Update(lstClassForwardingUpdate[i]);
                    }
                }
                this.Save();
            }
            catch (Exception ex)
            {
                
                string ParamList = string.Format("AcademicYearID={0}, SchoolID={1}, EducationLevelID={2}", AcademicYearID, SchoolID, EducationLevelID);
                LogExtensions.ErrorExt(logger, DateTime.Now, "CancelForwardPupil", ParamList,ex);
            }
            finally
            {
                context.Configuration.AutoDetectChangesEnabled = true;
                context.Configuration.ValidateOnSaveEnabled = true;
            }
        }
    }
}
