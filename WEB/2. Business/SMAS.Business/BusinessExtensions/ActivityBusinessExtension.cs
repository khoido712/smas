/**
 * The Viettel License
 *
 * Copyright 2012 Viettel Telecom. All rights reserved.
 * VIETTEL PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */

/** 
* @author  
* @version $Revision: $
*/

using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Linq.Expressions;
using System.Web;
using log4net;

using SMAS.DAL.Repository;
using SMAS.DAL.IRepository;
using SMAS.Business.IBusiness;
using SMAS.Models.Models;
using SMAS.Business.BusinessObject;
using SMAS.Business.Common;
using System.Data.Objects;

namespace SMAS.Business.Business
{
    public partial class ActivityBusiness
    {
        public IQueryable<Activity> SearchByClass(int ClassID, int SchoolID, int AcademicYearID, DateTime ActivityDate)
        {
            var query = from apc in ActivityPlanClassRepository.All
                        join ap in ActivityPlanRepository.All on apc.ActivityPlanID equals ap.ActivityPlanID
                        where apc.ClassID == ClassID
                                && ap.SchoolID == SchoolID
                                && ap.AcademicYearID == AcademicYearID
                                && ap.FromDate <= ActivityDate
                                && ap.FromDate <= ap.ToDate
                                && ap.IsActive == true
                        select apc.ActivityPlanID;
            List<int> lstActivityPlan = new List<int>();
            lstActivityPlan = query.ToList();
            IQueryable<Activity> ac = this.ActivityRepository.All;
            ac = ac.Where(x => lstActivityPlan.Contains(x.ActivityPlanID));
            return ac;
        }
        public List<ActivityPlanToSMS> GetActivityToSMS(IDictionary<string, object> dic)
        {
            int SchoolID = Utils.GetInt(dic, "SchoolID");
            int AcademicYearID = Utils.GetInt(dic, "AcademicYearID");
            DateTime? ActivatedDate = Utils.GetDateTime(dic, "ActivatedDate");
            //bo sung CurrentAcademicYearID
            dic["CurrentAcademicYearID"] = AcademicYearID;
            List<ActivityPlanToSMS> ab = new List<ActivityPlanToSMS>();

            List<PupilProfileBO> lstPupil = PupilProfileBusiness.SearchBySchool(SchoolID, dic).ToList();
            for (int i = 0; i < lstPupil.Count; i++)
            {
                List<Activity> lstActivity = new List<Activity>();
                string ContentActivity = "";
                string strMorning = "";
                string strEvening = "";

                lstActivity = SearchByClass(lstPupil[i].CurrentClassID, SchoolID, AcademicYearID, ActivatedDate.Value).ToList();
                for (int j = 0; j < lstActivity.Count; j++)
                {
                    if (lstActivity[j].Section == SystemParamsInFile.SECTION_OF_DAY_MORNING)
                    {
                        strMorning += lstActivity[j].ActivityName;
                    }
                    if (lstActivity[j].Section == SystemParamsInFile.SECTION_OF_DAY_EVENING)
                    {
                        strEvening += lstActivity[j].ActivityName;
                    }
                    ContentActivity = "Kế hoạch hoạt động ngày" + ActivatedDate.ToString() + "của trẻ:" + "Buổi sáng:" + strMorning + "Buổi chiều:" + strEvening;
                }
                ActivityPlanToSMS apt = new ActivityPlanToSMS();
                apt.PupilID = lstPupil[i].PupilProfileID;
                apt.ClassID = lstPupil[i].CurrentClassID;
                apt.ContentActivity = ContentActivity;
                ab.Add(apt);

            }

            return ab;
        }
        /// <summary>
        /// minhh added
        /// 14/11/12
        /// </summary>
        /// <param name="dic"></param>
        /// <returns></returns>
        public IQueryable<Activity> Search(IDictionary<string, object> dic)
        {
            int ActivityID = Utils.GetInt(dic, "ActivityID");
            string ActivityName = Utils.GetString(dic, "ActivityName");
            System.DateTime? FromTime = Utils.GetDateTime(dic, "FromTime");
            System.DateTime? ToTime = Utils.GetDateTime(dic, "ToTime");
            int ActivityTypeID = Utils.GetInt(dic, "ActivityTypeID");
            int ActivityPlanID = Utils.GetInt(dic, "ActivityPlanID");
            List<int> LstActivityPlanID = Utils.GetIntList(dic, "LstActivityPlanID");
            int DayOfWeek = Utils.GetByte(dic, "DayOfWeek");
            int Section = Utils.GetByte(dic, "Section");
            System.DateTime? ActivityDate = Utils.GetDateTime(dic, "ActivityDate");
            IQueryable<Activity> Query = ActivityBusiness.All;
            if (ActivityTypeID != 0)
            {
                Query = Query.Where(evd => evd.ActivityTypeID == ActivityTypeID);
            }
            if (ActivityID != 0)
            {
                Query = Query.Where(evd => evd.ActivityID == ActivityID);
            }
            if (FromTime.HasValue)
            {
                Query = Query.Where(evd => evd.FromTime.Hour == FromTime.Value.Hour && evd.FromTime.Minute == FromTime.Value.Minute);
            }
            if (ToTime.HasValue)
            {
                Query = Query.Where(evd => evd.ToTime.Hour == ToTime.Value.Hour && evd.ToTime.Minute == ToTime.Value.Minute);
            }
            if (ActivityDate.HasValue)
            {
                /*Query = Query.Where(evd => evd.ActivityDate.Value.Day == ActivityDate.Value.Day &&
                    evd.ActivityDate.Value.Month == ActivityDate.Value.Month && evd.ActivityDate.Value.Year == ActivityDate.Value.Year);*/
                Query = Query.Where(avd => EntityFunctions.TruncateTime(avd.ActivityDate.Value) == EntityFunctions.TruncateTime(ActivityDate.Value));
            }

            if (ActivityPlanID != 0)
            {
                Query = Query.Where(evd => evd.ActivityPlanID == ActivityPlanID);
            }

            // Truong hop la danh sach cac hoat dong 
            if (LstActivityPlanID != null && LstActivityPlanID.Count > 0)
            {
                Query = Query.Where(e => LstActivityPlanID.Contains(e.ActivityPlanID));
            }

            if (DayOfWeek != 0)
            {
                Query = Query.Where(evd => evd.DayOfWeek == DayOfWeek);
            }
            if (Section != 0)
            {
                Query = Query.Where(evd => evd.Section == Section);
            }
            if (ActivityName.Trim().Length != 0) Query = Query.Where(o => o.ActivityName.Contains(ActivityName.ToLower()));

            return Query;
        }
        /// <summary>
        /// Thêm mới hoạt động trong ngày của lớp
        /// <author>minhh - 20/11/2012</author>
        /// </summary>
        /// <param name="lsActivity"></param>
        public void Insert(List<Activity> lsActivity)
        {
            Activity obj = null;
            foreach (var item in lsActivity)
            {
                obj = new Activity();
                ValidationMetadata.ValidateObject(item);
                if (item.FromTime > item.ToTime)
                {
                    throw new BusinessException("Activity_Validate_DOW");
                }
                obj = item;
                base.Insert(obj);
            }
            this.Save();
        }
        public void Delete(List<int> lsActivityID)
        {
            if (lsActivityID == null || lsActivityID.Count <= 0) return;
            for (int i = 0; i < lsActivityID.Count; i++)
            {
                this.Delete(lsActivityID[i]);
            }
            this.Save();
        }
        #region Sao chép kế hoạch hoạt động
        public void InsertCoppy(List<Activity> lsActivity, IDictionary<string, object> dic)
        {
            Activity obj = null;
            List<Activity> lstAc = new List<Activity>();

            int AcademicYearID = Utils.GetInt(dic, "AcademicYearID");
            int SchoolID = Utils.GetInt(dic, "SchoolID");
            List<int> LstClassID = Utils.GetIntList(dic, "lstClassID");
            DateTime? fromDate = Utils.GetDateTime(dic, "FromDate");
            DateTime? toDate = Utils.GetDateTime(dic, "ToDate");

            int activityPlanID = lsActivity[0].ActivityPlanID;

            IQueryable<Activity> lstAct = this.ActivityRepository.All;
            IQueryable<ActivityPlanClass> lstAPC = ActivityPlanClassRepository.All.Where(x => LstClassID.Contains(x.ClassID));
            IQueryable<ActivityPlan> lstAP = ActivityPlanRepository.All.Where(x => x.SchoolID == SchoolID && x.AcademicYearID == AcademicYearID
                                                                                && x.IsActive == true && x.FromDate == fromDate.Value);

            var lstActivityPlanID = from apc in lstAPC
                                    join ap in lstAP on apc.ActivityPlanID equals ap.ActivityPlanID
                                    where LstClassID.Contains(apc.ClassID)
                                    && ap.FromDate == fromDate.Value
                                    && ap.ActivityPlanID != activityPlanID
                                    select apc.ActivityPlanID;

            List<int> lstActivityID = new List<int>();
            if (lstActivityPlanID.Count() > 0)
            {
                // Xoa ke hoach cua lop
                List<int> lstIntActivityPlanID = lstActivityPlanID.ToList();
                var lstAPCByActivityPlanID = lstAPC.Where(x => lstIntActivityPlanID.Contains(x.ActivityPlanID)).ToList();

                IQueryable<ActivityOfPupil> lstAOP = ActivityOfPupilBusiness.All.Where(x => x.SchoolID == SchoolID && x.AcademicYearID == AcademicYearID
                                                                                && LstClassID.Contains(x.ClassID));
                lstActivityID = lstAOP.Where(x => x.ActivityOfClassID.HasValue && x.ActivityOfClassID > 0 && !string.IsNullOrEmpty(x.CommentOfTeacher))
                                                .Select(x => x.ActivityOfClassID.Value).ToList();
                List<int> lstActivityPlanId = lstAct.Where(x => lstActivityID.Contains(x.ActivityID)).Select(x => x.ActivityPlanID).ToList();
                var lstActivityPlan = lstAP.Where(x => lstActivityPlanId.Contains(x.ActivityPlanID));

                if (lstActivityPlan.Count() <= 0)
                {
                    ActivityPlanClassRepository.DeleteAll(lstAPCByActivityPlanID);
                    ActivityPlanClassRepository.Save();
                }
                else // khong cho sao chep khi tuan da duoc su dung
                {
                    lstActivityPlanId = lstActivityPlan.Select(x => x.ActivityPlanID).ToList();
                    var lstActivityByActivityPlan = lstAct.Where(x => lstActivityPlanId.Contains(x.ActivityPlanID));
                    if (lstActivityByActivityPlan.Count() <= 0)
                    {
                        ActivityPlanClassRepository.DeleteAll(lstAPCByActivityPlanID);
                        ActivityPlanClassRepository.Save();
                    }
                    else
                        return;
                }
            }

            foreach (var ClassID in LstClassID)
            {
                var query = from apc in lstAPC
                            join ap in lstAP on apc.ActivityPlanID equals ap.ActivityPlanID
                            where apc.ClassID == ClassID
                            select apc.ActivityPlanID;

                List<int> lstActivityPlan = query.ToList();
                var tempAct = lstAct.Where(x => lstActivityPlan.Contains(x.ActivityPlanID));

                for (var i = 0; i < lsActivity.Count; i++)
                {
                    var x = lsActivity[i];
                    obj = tempAct.Where(t => EntityFunctions.TruncateTime(t.ActivityDate.Value) == EntityFunctions.TruncateTime(x.ActivityDate.Value)).FirstOrDefault();
                    if (obj != null)
                    {
                        // xoa hoat dong cua lop
                        this.Delete(obj.ActivityID);
                        this.Save();
                    }
                }
            }

            // Ghi de
            foreach (var item in lsActivity)
            {
                obj = new Activity();
                ValidationMetadata.ValidateObject(item);
                if (item.FromTime > item.ToTime)
                {
                    throw new BusinessException("Activity_Validate_DOW");
                }
                obj = item;
                base.Insert(obj);
            }
            this.Save();
        }
        #endregion
        ////
        //public IQueryable<Activity> SeachByShool(int SchoolID, int AcademicYearID)
        //{
        //    var query = from apc in ActivityPlanClassRepository.All
        //                join ap in ActivityPlanRepository.All on apc.ActivityPlanID equals ap.ActivityPlanID
        //                where
        //                        ap.SchoolID == SchoolID
        //                        && ap.AcademicYearID == AcademicYearID
        //                        && ap.IsActive == true
        //                select apc.ActivityPlanID;
        //    List<int> lstActivityPlan = new List<int>();
        //    lstActivityPlan = query.ToList();
        //    IQueryable<Activity> ac = this.ActivityRepository.All;
        //    ac = ac.Where(x => lstActivityPlan.Contains(x.ActivityPlanID));
        //    return ac;
        //}
    }
}
