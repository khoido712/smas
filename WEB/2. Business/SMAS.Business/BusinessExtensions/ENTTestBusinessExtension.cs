﻿/**
 * The Viettel License
 *
 * Copyright 2012 Viettel Telecom. All rights reserved.
 * VIETTEL PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */

/** 
* @author  
* @version $Revision: $
*/

using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Linq.Expressions;
using System.Web;
using log4net;

using SMAS.DAL.Repository;
using SMAS.DAL.IRepository;
using SMAS.Business.IBusiness;
using SMAS.Models.Models;
using SMAS.Business.Common;
using SMAS.Business.BusinessObject;

namespace SMAS.Business.Business
{
    public partial class ENTTestBusiness
    {
        #region validate
        /// <summary>
        /// validate
        /// </summary>
        /// <param name="entity">The entity.</param>
        ///<author>Nam ta</author>
        ///<Date>12/25/2012</Date>
        /// <exception cref="SMAS.Business.Common.BusinessException">ENTTest_Validate_RcapacityHearing</exception>
        private void validate(ENTTest entity)
        {

            ValidationMetadata.ValidateObject(entity);

            //RcapacityHearing: Kiểu int,  <= 9999
            //LcapacityHearing: Kiểu int, <= 9999

            if (entity.RCapacityHearing > 9999)
            {
                throw new BusinessException("ENTTest_Validate_RcapacityHearing");
            }

            if (entity.LCapacityHearing > 9999)
            {
                throw new BusinessException("ENTTest_Validate_LCapacityHearing");
            }
        }
        #endregion

        /// <summary>
        /// Insert
        /// </summary>
        /// <param name="entity">The entity.</param>
        /// <param name="entity1">The entity1.</param>
        ///<author>Nam ta</author>
        ///<Date>12/25/2012</Date>
        public void Insert(ENTTest entity, MonitoringBook entity1)
        {
            //Thực hiện kiểm tra với PupilID = entity1.PupilID, HealthPeriodID = entity1.HealthPeriodID, SchoolID = entity1.SchoolID, AcademicYearID = entity1.AcademicYearID
            validate(entity);
            int PupilID = entity1.PupilID;
            int? HealthPeriodID = entity1.HealthPeriodID;
            int SchoolID = entity1.SchoolID;
            int AcademicYearID = entity1.AcademicYearID;

            IDictionary<string, object> dic = new Dictionary<string, object>();
            dic["PupilID"] = PupilID;
            dic["HealthPeriodID"] = HealthPeriodID;
            dic["AcademicYearID"] = AcademicYearID;


            IQueryable<MonitoringBook> lstMonitoringBook = MonitoringBookBusiness.SearchBySchool(SchoolID, dic);

            if (lstMonitoringBook == null || lstMonitoringBook.Count() == 0)
            {
                MonitoringBookBusiness.Insert(entity1);
                MonitoringBookBusiness.Save();
                entity.MonitoringBookID = entity1.MonitoringBookID;
                this.Insert(entity);
                this.Save();

            }
            else if (lstMonitoringBook.Count() > 0)
            {
                entity.MonitoringBookID = lstMonitoringBook.FirstOrDefault().MonitoringBookID;
                bool MonitoringBookDiplicate = this.repository.ExistsRow(GlobalConstants.HEALTH_SCHEMA, "ENTTEST",
                   new Dictionary<string, object>()
                {
                    {"MonitoringBookID",entity.MonitoringBookID},
                    {"IsActive",true}
                }, new Dictionary<string, object>()
                {
                    //{"MonitoringBookID",entity.MonitoringBookID},
                });
                if (MonitoringBookDiplicate)
                {
                    throw new BusinessException("MonitoringBook_Label_CheckDuplicateMonitoringBookID");
                }
                dic["HealthPeriodID"] = HealthPeriodID;
                dic["AcademicYearID"] = AcademicYearID;
                this.Insert(entity);
                this.Save();
            }

        }


        /// <summary>
        /// Update
        /// </summary>
        /// <param name="entity">The entity.</param>
        /// <param name="entity1">The entity1.</param>
        ///<author>Nam ta</author>
        ///<Date>12/25/2012</Date>
        public void Update(ENTTest entity, MonitoringBook entity1)
        {
            validate(entity);
            //Thực hiện kiểm tra với PupilID = entity1.PupilID, HealthPeriodID = entity1.HealthPeriodID, SchoolID = entity1.SchoolID, AcademicYearID = entity1.AcademicYearID

            int PupilID = entity1.PupilID;
            int HealthPeriodID = entity1.HealthPeriodID.Value;
            int SchoolID = entity1.SchoolID;
            int AcademicYearID = entity1.AcademicYearID;

            bool MonitoringBookDiplicate = this.repository.ExistsRow(GlobalConstants.HEALTH_SCHEMA, "ENTTEST",
                   new Dictionary<string, object>()
                {
                    {"MonitoringBookID",entity.MonitoringBookID}
                }, new Dictionary<string, object>()
                {
                    {"MonitoringBookID",entity.MonitoringBookID},
                });
            if (MonitoringBookDiplicate)
            {
                throw new BusinessException("MonitoringBook_Label_CheckDuplicateMonitoringBookID");
            }
            MonitoringBookBusiness.Update(entity1);
            MonitoringBookBusiness.Save();
            entity.MonitoringBookID = entity1.MonitoringBookID;
            this.Update(entity);
            this.Save();


        }

        /// <summary>
        /// Delete
        /// </summary>
        /// <param name="ENTTestID">The ENT test ID.</param>
        /// <param name="SchoolID">The school ID.</param>
        ///<author>Nam ta</author>
        ///<Date>12/26/2012</Date>
        /// <exception cref="BusinessException">Common_Validate_NotCompatible</exception>
        public void Delete(int ENTTestID, int SchoolID)
        {
            this.CheckConstraints("ENTTest", GlobalConstants.HEALTH_SCHEMA, ENTTestID, "ENTTest_Validate_ENTTestID");

            ENTTest entity = this.Find(ENTTestID);
            int MonitoringBookID = entity.MonitoringBookID;
            MonitoringBook entity1 = MonitoringBookBusiness.Find(MonitoringBookID);
            IDictionary<string, object> SearchInfo = new Dictionary<string, object>();
            SearchInfo["MonitoringBookID"] = entity.MonitoringBookID;
            SearchInfo["SchoolID"] = entity1.SchoolID;

            bool compatible = new ClassProfileRepository(this.context).ExistsRow(GlobalConstants.HEALTH_SCHEMA, "MonitoringBook", SearchInfo);

            if (!compatible)
            {
                throw new BusinessException("Common_Validate_NotCompatible");
            }

            entity.ModifiedDate = DateTime.Now;
            entity.IsActive = false;

            this.Update(entity);
            this.Save();

        }


        /// <summary>
        /// Search
        /// </summary>
        /// <param name="SearchInfo">The search info.</param>
        /// <returns>
        /// IQueryable{ENTTest}
        /// </returns>
        ///<author>Nam ta</author>
        ///<Date>12/26/2012</Date>
        private IQueryable<ENTTest> Search(IDictionary<string, object> SearchInfo)
        {
            int ENTTestID = Utils.GetInt(SearchInfo, "ENTTestID");
            int RcapacityHearing = Utils.GetInt(SearchInfo, "RCapacityHearing");
            int LcapacityHearing = Utils.GetInt(SearchInfo, "LCapacityHearing");
            bool? IsDeaf = Utils.GetBool(SearchInfo, "IsDeaf");
            bool? IsSick = Utils.GetBool(SearchInfo, "IsSick");
            string Description = Utils.GetString(SearchInfo, "Description");
            bool IsActive = Utils.GetBool(SearchInfo, "IsActive", true);
            int MonitoringBookID = Utils.GetInt(SearchInfo, "MonitoringBookID");
            int SchoolID = Utils.GetInt(SearchInfo, "SchoolID");
            int AcademicYearID = Utils.GetInt(SearchInfo, "AcademicYearID");
            int PupilID = Utils.GetInt(SearchInfo, "PupilID");
            int ClassID = Utils.GetInt(SearchInfo, "ClassID");
            int HealthPeriodID = Utils.GetInt(SearchInfo, "HealthPeriodID");
            string UnitName = Utils.GetString(SearchInfo, "UnitName");
            DateTime? MonitoringDate = Utils.GetDateTime(SearchInfo, "MonitoringDate");
            string FullName = Utils.GetString(SearchInfo, "FullName", "");
            int EducationLevelID = Utils.GetInt(SearchInfo, "EducationLevelID");
            int Grade = Utils.GetInt(SearchInfo, "EducationGrade");
            //bảng ENTTest et  join MonitoringBook mb on et.MonitoringBookID = mb.MonitoringBookID join PupilProfile pp (theo PupilID

            IQueryable<ENTTest> querry = from mb in MonitoringBookBusiness.All
                                         join et in this.All on mb.MonitoringBookID equals et.MonitoringBookID
                                         select et;

            //IQueryable<ENTTest> querry = MonitoringBookBusiness.All.Where(u => u.PupilID == PupilID && (u.PupilProfile.FullName == "" || u.PupilProfile.FullName.Contains(FullName))).SelectMany(u => u.ENTTests);
            if (Grade > 0)
            {
                querry = from ps in this.All
                         join mb in MonitoringBookBusiness.All on ps.MonitoringBookID equals mb.MonitoringBookID
                         join ed in EducationLevelBusiness.All on mb.EducationLevelID equals ed.EducationLevelID
                         where ed.Grade == Grade
                         select ps;
            }
            if (ENTTestID != 0)
            {
                querry = querry.Where(o => o.ENTTestID == ENTTestID);
            }
            if (RcapacityHearing != 0)
            {
                querry = querry.Where(o => o.RCapacityHearing == RcapacityHearing);
            }
            if (LcapacityHearing != 0)
            {
                querry = querry.Where(o => o.LCapacityHearing == LcapacityHearing);
            }
            if (IsDeaf == true)
            {
                querry = querry.Where(o => o.IsDeaf == IsDeaf);
            }
            if (IsSick == true)
            {
                querry = querry.Where(o => o.IsSick == IsSick);
            }
            if (Description != "")
            {
                querry = querry.Where(o => o.Description == Description);
            }
            if (MonitoringBookID != 0)
            {
                querry = querry.Where(o => o.MonitoringBookID == MonitoringBookID);
            }
            if (SchoolID != 0)
            {
                querry = querry.Where(o => o.MonitoringBook.SchoolID == SchoolID);
            }
            if (AcademicYearID != 0)
            {
                querry = querry.Where(o => o.MonitoringBook.AcademicYearID == AcademicYearID);
            }
            if (PupilID != 0)
            {
                querry = querry.Where(o => o.MonitoringBook.PupilID == PupilID);
            }
            if (ClassID != 0)
            {
                querry = querry.Where(o => o.MonitoringBook.ClassID == ClassID);
            }
            if (HealthPeriodID != 0)
            {
                querry = querry.Where(o => o.MonitoringBook.HealthPeriodID == HealthPeriodID);
            }
            if (UnitName != "")
            {
                querry = querry.Where(o => o.MonitoringBook.UnitName == UnitName);
            }
            if (MonitoringDate != null)
            {
                querry = querry.Where(o => o.MonitoringBook.MonitoringDate == MonitoringDate);
            }
            if (EducationLevelID != 0)
            {
                querry = querry.Where(o => o.MonitoringBook.EducationLevelID == EducationLevelID);
            }
            if (FullName != "")
            {
                querry = querry.Where(o => o.MonitoringBook.PupilProfile.FullName.ToLower().Contains(FullName.ToLower()));
            }
            if (PupilID != 0)
            {
                querry = querry.Where(o => o.MonitoringBook.PupilID == PupilID);
            }
            //querry = querry.Where(o => o.IsActive == IsActive);
            return querry;

        }

        /// <summary>
        /// SearchBySchool
        /// </summary>
        /// <param name="SchoolID">The school ID.</param>
        /// <param name="SearchInfo">The search info.</param>
        /// <returns>
        /// IQueryable{ENTTest}
        /// </returns>
        ///<author>Nam ta</author>
        ///<Date>12/26/2012</Date>
        public IQueryable<ENTTest> SearchBySchool(int SchoolID, IDictionary<string, object> SearchInfo)
        {
            if (SchoolID == 0)
            {
                return null;
            }
            else
            {
                SearchInfo["SchoolID"] = SchoolID;
                return this.Search(SearchInfo);
            }
        }

        /// <summary>
        /// SearchByPupil
        /// </summary>
        /// <param name="AcademicYearID">The academic year ID.</param>
        /// <param name="SchoolID">The school ID.</param>
        /// <param name="PupilID">The pupil ID.</param>
        /// <param name="ClassID">The class ID.</param>
        /// <returns>
        /// IQueryable{ENTTestBO}
        /// </returns>
        ///<author>Nam ta</author>
        ///<Date>12/26/2012</Date>
        public IQueryable<ENTTestBO> SearchByPupil(int AcademicYearID, int SchoolID, int PupilID, int ClassID)
        {
            //B1: Khởi tạo IQueryable<MonitoringBook> lstMonitoringBook bằng cách gọi hàm MonitoringBookBusiness.SearchByPupil(SchoolID, AcademicYearID, PupilID, ClassID)
            IQueryable<MonitoringBook> lstMonitoringBook = MonitoringBookBusiness.SearchByPupil(SchoolID, AcademicYearID, PupilID, ClassID);
            //B2: Thực hiện việc tìm kiếm lstMonitoringBook mb left DentalTest dt on mb.MonitoringBookID = dt.MonitoringBookID. Kêt quả thu được IQueryable<DentalTestBO>
            var query = from mb in lstMonitoringBook
                        join dt in this.All.Where(o => o.IsActive == true) on mb.MonitoringBookID equals dt.MonitoringBookID
                        select new ENTTestBO
                        {
                            MonitoringBookID = mb.MonitoringBookID,
                            MonitoringBookDate = mb.MonitoringDate,
                            ENTTestID = dt.ENTTestID,
                            RCapacityHearing = dt.RCapacityHearing,
                            LCapacityHearing = dt.LCapacityHearing,
                            IsDeaf = dt.IsDeaf,
                            IsSick = dt.IsSick,
                            Description = dt.Description,
                            HealthPeriodID = mb.HealthPeriodID,
                            UnitName = mb.UnitName,
                            CreatedDate = dt.CreatedDate,
                        };
            return query;
        }

        // Thuyen - 05/10/2017
        public List<ENTTest> ListENTTestByMonitoringBookId(List<int> lstMoniBookID)
        {
            return ENTTestBusiness.All.Where(x => lstMoniBookID.Contains(x.MonitoringBookID)).ToList();
        }
    }
}