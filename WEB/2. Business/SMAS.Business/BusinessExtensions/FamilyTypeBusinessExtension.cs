/**
 * The Viettel License
 *
 * Copyright 2012 Viettel Telecom. All rights reserved.
 * VIETTEL PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */

/** 
* @author  
* @version $Revision: $
*/

using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Linq.Expressions;
using System.Web;
using log4net;

using SMAS.DAL.Repository;
using SMAS.DAL.IRepository;
using SMAS.Business.IBusiness;
using SMAS.Models.Models;
using SMAS.Business.Common;

namespace SMAS.Business.Business
{
    public partial class FamilyTypeBusiness
    {

        /// <summary>
        /// Insert vao CSDL
        /// </summary>
        /// <param name="FamilyType"></param>
        /// <returns></returns>
        public override FamilyType Insert(FamilyType FamilyType)
        {
            // Kiem tra truong bat buoc
            Utils.ValidateRequire(FamilyType.Resolution, "FamilyType_Label_Resolution");
            // Kiem tra truong Resolution maxlength khong duoc vuot qua 50 ky tu
            Utils.ValidateMaxLength(FamilyType.Resolution, 50, "FamilyType_Label_Resolution");
            // Kiem tra trung ten 
            FamilyTypeBusiness.CheckDuplicate(FamilyType.Resolution, GlobalConstants.LIST_SCHEMA, "FamilyType", "Resolution", true, FamilyType.FamilyTypeID, "FamilyType_Label_Resolution");
           //this.CheckDuplicate(FamilyType.Resolution, GlobalConstants.LIST_SCHEMA, "FamilyType", "Resolution", true, null, "FamilyType_Label_Resolution");
            // Kiem tra truong mo ta khong duoc vuot qua 400 ky tu
            Utils.ValidateMaxLength(FamilyType.Description, 400, "FamilyType_Label_Description");
            // Them vao CSDL            
            return base.Insert(FamilyType);
        }

        /// <summary>
        /// Cap nhat vao CSDL
        /// </summary>
        /// <param name="FamilyType"></param>
        /// <returns></returns>
        public override FamilyType Update(FamilyType FamilyType)
        {
            // Kiem tra truong bat buoc
            Utils.ValidateRequire(FamilyType.Resolution, "FamilyType_Label_Resolution");
            // Kiem tra truong Resolution maxlength khong duoc vuot qua 50 ky tu
            Utils.ValidateMaxLength(FamilyType.Resolution, 50, "FamilyType_Label_Resolution");
            // Kiem tra trung ten 
            this.CheckDuplicate(FamilyType.Resolution, GlobalConstants.LIST_SCHEMA, "FamilyType", "Resolution", true, FamilyType.FamilyTypeID, "FamilyType_Label_Resolution");
            // Kiem tra truong mo ta khong duoc vuot qua 400 ky tu
            Utils.ValidateMaxLength(FamilyType.Description, 400, "FamilyType_Label_Description");
            // Kiem tra ten thanh phan gia dinh da co trong DB
            //this.CheckAvailable(FamilyType.FamilyTypeID, "FamilyType_Label_Resolution", false);
            new FamilyTypeBusiness(null).CheckAvailable(FamilyType.FamilyTypeID, "FamilyType_Label_Resolution", false);
            // Cap nhat vao CSDL  
            return base.Update(FamilyType);
        }

        /// <summary>
        /// Xoa thanh phan gia dinh
        /// </summary>
        /// <param name="FamilyTypeID"></param>
        public void Delete(int FamilyTypeID)
        {
            // Kiem tra da ton tai trong  CSDL
            this.CheckAvailable(FamilyTypeID, "FamilyType_Label_Title", true);
            // Check Using
            this.CheckConstraints(GlobalConstants.LIST_SCHEMA, "FamilyType", FamilyTypeID, "FamilyType_Label_TitleFailed");
            // Xoa bang cach thiet lap IsActive = 0;
            this.Delete(FamilyTypeID, true);
        }

        /// <summary>
        /// Tim kiem thanh phan gia dinh
        /// </summary>
        /// <param name="dicParam"></param>
        /// <returns></returns>
        public IQueryable<FamilyType> Search(IDictionary<string, object> dicParam)
        {
            string Resolution = Utils.GetString(dicParam, "Resolution");
            string Description = Utils.GetString(dicParam, "Description");
            bool? IsActive = Utils.GetNullableBool(dicParam, "IsActive");
            IQueryable<FamilyType> listFamilyType = this.FamilyTypeRepository.All.OrderBy(o => o.Resolution);
            if (!Resolution.Equals(string.Empty))
            {
                listFamilyType = listFamilyType.Where(x => x.Resolution.ToLower().Contains(Resolution.ToLower()));
            }
            if (!Description.Equals(string.Empty))
            {
                listFamilyType = listFamilyType.Where(x => x.Description.ToLower().Contains(Description.ToLower()));
            }
            if (IsActive.HasValue)
            {
                listFamilyType = listFamilyType.Where(x => x.IsActive == true);
            }
            
            return listFamilyType;

        }

    }
}