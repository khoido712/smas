/**
 * The Viettel License
 *
 * Copyright 2012 Viettel Telecom. All rights reserved.
 * VIETTEL PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */

/** 
* @author  
* @version $Revision: $
*/

using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Linq.Expressions;
using System.Web;
using log4net;

using SMAS.DAL.Repository;
using SMAS.DAL.IRepository;
using SMAS.Business.IBusiness;
using SMAS.Models.Models;
using SMAS.Business.Common;

namespace SMAS.Business.Business
{ 
    public partial class SkillOfPupilBusiness
    {
        public IQueryable<SkillOfPupil> SearchBySchool(int? SchoolID, IDictionary<string, object> dic)
        {
            if (SchoolID == 0)
            {
                return null;
            }
            if (dic == null)
            {
                dic = new Dictionary<string, object>();
            }
            dic["SchoolID"] = SchoolID;
            return this.Search(dic);
        }

        private IQueryable<SkillOfPupil> Search(IDictionary<string, object> dic)
        {
            int SkillOfPupilID = Utils.GetInt(dic, "SkillOfPupilID");
            string CommentOfTeacher = Utils.GetString(dic, "CommentOfTeacher");
            int SchoolID = Utils.GetInt(dic, "SchoolID");
            int? ClassID = Utils.GetInt(dic, "ClassID");
            int AcademicYearID = Utils.GetInt(dic, "AcademicYearID");
            int PupilID = Utils.GetInt(dic, "PupilID");
            int EducationLevelID = Utils.GetInt(dic, "EducationLevelID");
            short AppliedLevel = Utils.GetShort(dic, "AppliedLevel");
            System.DateTime? ModifiedDate = Utils.GetDateTime(dic, "ModifiedDate");
            System.DateTime? CreateDate = Utils.GetDateTime(dic, "CreateDate");
            int? SkillOfClassID = Utils.GetInt(dic, "SkillOfClassID");
            IQueryable<SkillOfPupil> Query = SkillOfPupilRepository.All;
            if (EducationLevelID != 0)
            {
                if (ClassID == null)
                {
                    if (AppliedLevel != 0)
                    {
                        Query = Query.Where(o => o.ClassProfile.EducationLevel.Grade == AppliedLevel && o.ClassProfile.EducationLevelID == EducationLevelID);
                    }
                    else
                    {
                        Query = Query.Where(o => o.ClassProfile.EducationLevelID == EducationLevelID);
                    }

                }
                else if (ClassID != null)
                {
                    Query = Query.Where(o => o.ClassProfile.EducationLevelID == EducationLevelID && o.ClassID == ClassID);
                }
            }
          
            if (SchoolID != 0)
            {
                Query = Query.Where(evd => evd.SchoolID == SchoolID);
            }
            if (AcademicYearID != 0)
            {
                Query = Query.Where(evd => evd.AcademicYearID == AcademicYearID);
            }
            if (PupilID != 0)
            {
                Query = Query.Where(evd => evd.PupilID == PupilID);
            }

            if (SkillOfPupilID != 0)
            {
                Query = Query.Where(evd => evd.SkillOfPupilID == SkillOfPupilID);
            }

            if (SkillOfClassID != 0)
            {
                Query = Query.Where(evd => evd.SkillOfClassID == SkillOfClassID);
            }

            if (ModifiedDate != null || CreateDate != null)
            {

                Query = (Query.Where(evd => evd.ModifiedDate.HasValue && evd.ModifiedDate.Value.Year == ModifiedDate.Value.Year &&
                    evd.ModifiedDate.Value.Month == ModifiedDate.Value.Month && evd.ModifiedDate.Value.Day == ModifiedDate.Value.Day))
                    .Union(Query.Where(evd => evd.CreatedDate.Value.Year == CreateDate.Value.Year &&
                    evd.CreatedDate.Value.Month == CreateDate.Value.Month && evd.CreatedDate.Value.Day == CreateDate.Value.Day));
            }

            if (CommentOfTeacher.Trim().Length != 0) Query = Query.Where(o => o.CommentOfTeacher.Contains(CommentOfTeacher.ToLower()));

            return Query;
        }
        
    }
}