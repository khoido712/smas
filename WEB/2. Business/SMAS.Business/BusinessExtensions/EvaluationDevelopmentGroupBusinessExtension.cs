﻿/**
 * The Viettel License
 *
 * Copyright 2012 Viettel Telecom. All rights reserved.
 * VIETTEL PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */

/** 
* @author  
* @version $Revision: $
*/

using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Linq.Expressions;
using System.Web;
using log4net;

using SMAS.DAL.Repository;
using SMAS.DAL.IRepository;
using SMAS.Business.IBusiness;
using SMAS.Models.Models;
using SMAS.Business.Common;

namespace SMAS.Business.Business
{ 
    public partial class EvaluationDevelopmentGroupBusiness
    {

        #region Search


        /// <summary>
       
        /// </summary>
        /// <author>trangdd</author>
        /// <date>06/11/2012</date>    
        /// <returns>IQueryable<Ethnic></returns>
        public IQueryable<EvaluationDevelopmentGroup> Search(IDictionary<string, object> dic)
        {
            int AppliedLevel = Utils.GetInt(dic, "AppliedLevel");           
            bool? IsActive = Utils.GetIsActive(dic, "IsActive");
            IQueryable<EvaluationDevelopmentGroup> Query = EvaluationDevelopmentGroupRepository.All;
            if (IsActive.HasValue)
            {
                Query = Query.Where(evd => evd.IsActive == IsActive);
            }
            if (AppliedLevel != 0)
            {
                Query = Query.Where(evd => evd.AppliedLevel == AppliedLevel);
            }          
            return Query;
        }
        #endregion

        public IQueryable<EvaluationDevelopmentGroup> SearchEval(IDictionary<string, object> dic)
        {
            int evalId = Utils.GetInt(dic, "EvaluationDevGroID");
            int educationLevel = Utils.GetInt(dic, "EducationLevelID");
            int status = Utils.GetInt(dic, "StatusID");
            int provinceId = Utils.GetInt(dic, "ProvinceID");
            string evaluaName = Utils.GetString(dic, "EvaluaName");
            string evaluaCode = Utils.GetString(dic, "EvaluaCode");

            IQueryable<EvaluationDevelopmentGroup> Query = EvaluationDevelopmentGroupRepository.All;
            if (status != 0)
            {
                if (status == 1)
                    Query = Query.Where(evd => evd.IsActive == true);
                if (status == 2)
                    Query = Query.Where(evd => evd.IsActive == false);
            }

            if (evalId != 0)
            {
                Query = Query.Where(evd => evd.EvaluationDevelopmentGroupID == evalId);
            }

            if (educationLevel != 0)
            {
                Query = Query.Where(evd => evd.EducationLevelID == educationLevel);
            }
            if (provinceId != 0)
            {
                Query = Query.Where(evd => evd.ProvinceID == provinceId);
            }
            if (!string.IsNullOrEmpty(evaluaName))
            {
                Query = Query.Where(evd => evd.EvaluationDevelopmentGroupName.Contains(evaluaName));
            }
            if (!string.IsNullOrEmpty(evaluaCode))
            {
                Query = Query.Where(evd => evd.EvaluationDGCode.ToUpper().Contains(evaluaCode.ToUpper()));
            }

            return Query;

        }

        public void InsertEval(List<EvaluationDevelopmentGroup> lstInput)
        {
            foreach (var item in lstInput)
            {
                this.Insert(item);
            }
            this.Save();
        }

        public void UpdateVal(EvaluationDevelopmentGroup objInpt) {
            IDictionary<string, object> SearchInfo = new Dictionary<string, object>();
            SearchInfo["EvaluationDevGroID"] = objInpt.EvaluationDevelopmentGroupID;
            EvaluationDevelopmentGroup objDB = SearchEval(SearchInfo).FirstOrDefault();

            if (objDB != null)
            {
                objDB.EvaluationDevelopmentGroupName = objInpt.EvaluationDevelopmentGroupName;
                objDB.EvaluationDGCode = objInpt.EvaluationDGCode;
                objDB.Description = objInpt.Description;
                objDB.ProvinceID = objInpt.ProvinceID;
                objDB.IsActive = objInpt.IsActive;
                objDB.ModifiedDate = objInpt.ModifiedDate;

                this.Update(objDB);
                this.Save();
            }
        }

        public void UpdateOrderID(List<EvaluationDevelopmentGroup> lstInput, List<EvaluationDevelopmentGroup> lstDB)
        {
            foreach (var item in lstInput)
            {
                var result = lstDB.Where(x => x.EvaluationDevelopmentGroupID == item.EvaluationDevelopmentGroupID).FirstOrDefault();
                if (result != null)
                {
                    result.OrderID = item.OrderID;
                    result.ModifiedDate = item.ModifiedDate;
                    this.Update(result);
                }
            }
            this.Save();
        }

        public IQueryable<EvaluationDevelopmentGroup> GetListByAppliedLevelAndProvince(int AppliedLevel, int ProvinceID)
        {
            IQueryable<EvaluationDevelopmentGroup> query = this.EvaluationDevelopmentGroupBusiness.All.Where(x=>x.IsActive == true);

            if (AppliedLevel != 0)
                query = query.Where(x => x.AppliedLevel == AppliedLevel);

            if (ProvinceID != 0)
            {
                var check = query.Where(x => x.ProvinceID == ProvinceID).FirstOrDefault();
                if (check != null)
                    query = query.Where(x => x.ProvinceID == ProvinceID);
                else
                    query = query.Where(x => x.ProvinceID == null || x.ProvinceID == -1);
            }
            return query;
        
        }

        public void UpdateLock(int ProvinceID, bool IsLocked)
        {
            IDictionary<string, object> dic = new Dictionary<string, object>();
            List<EvaluationDevelopmentGroup> lstEvalua =  this.SearchEval(dic).Where(x=>x.IsActive).ToList();

            if (ProvinceID == -1)// Toan quoc
            {
                lstEvalua = lstEvalua.Where(x => x.ProvinceID.HasValue == false || x.ProvinceID == ProvinceID).ToList();
            }
            else
            {
                lstEvalua = lstEvalua.Where(x => x.ProvinceID == ProvinceID).ToList();
            }

            for (int i = 0; i < lstEvalua.Count(); i++)
            {
                var objEvaluation = lstEvalua[i];
                objEvaluation.IsLocked = IsLocked ? false : true;
                this.Update(objEvaluation);
            }
            this.Save();
        }
    }
}