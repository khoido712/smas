﻿/**
 * The Viettel License
 *
 * Copyright 2012 Viettel Telecom. All rights reserved.
 * VIETTEL PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */

/** 
* @author  
* @version $Revision: $
*/

using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Linq.Expressions;
using System.Web;
using log4net;

using SMAS.DAL.Repository;
using SMAS.DAL.IRepository;
using SMAS.Business.IBusiness;
using SMAS.Models.Models;
using SMAS.Business.BusinessObject;
using SMAS.Business.Common;
using System.IO;
using SMAS.VTUtils.Excel.Export;


namespace SMAS.Business.Business
{
    public partial class StatisticOfTertiaryBusiness
    {
        /// <summary>
        /// namdv3
        /// </summary>
        /// <param name="SearchInfo"></param>
        /// <returns></returns>
        private IQueryable<StatisticOfTertiary> Search(IDictionary<string, object> SearchInfo)
        {
            /*
             Thực hiện tìm kiếm trong bảng StatisticOfTertiary theo các thông tin trong SearchInfo:
            - SchoolID: default = 0; 0 => Tìm kiếm all
            - AcademicYearID: default = 0; 0 => Tìm kiếm All
            - Year: default = 0; 0 => Tìm kiếm All, !=0 tìm kiếm theo AcademicYear.Year = Year
             */
            int SchoolID = Utils.GetInt(SearchInfo, "SchoolID");
            int AcademicYearID = Utils.GetInt(SearchInfo, "AcademicYearID");
            int Year = Utils.GetInt(SearchInfo, "Year");
            IQueryable<StatisticOfTertiary> query = StatisticOfTertiaryRepository.All;
            if (SchoolID > 0)
            {
                query = query.Where(o => o.SchoolID == SchoolID);
            }
            if (AcademicYearID > 0)
            {
                query = query.Where(o => o.AcademicYearID == AcademicYearID);
            }
            if (Year > 0)
            {
                query = query.Where(o => o.AcademicYear.Year == Year);
            }
            return query;
        }


        /// <summary>
        /// Tìm kiếm
        /// author: namdv3
        /// </summary>
        /// <param name="ProvinceID"></param>
        /// <param name="Year"></param>
        /// <returns></returns>
        public IQueryable<StatisticOfTertiaryBO> GetListSchoolReported(int ProvinceID, int Year, int DistrictID, int? Status)
        {
            /*
             Danh sách trường: iqSchool = SchoolProfileBusiness.Search(dic)
            dic[“ProvinceID”] =ProvinceID
            dic[“AppliedLevel”] = APPLIED_LEVEL_TERTIARY
             */
            IDictionary<string, object> dic = new Dictionary<string, object>();
            dic["ProvinceID"] = ProvinceID;
            dic["DistrictID"] = DistrictID;
            dic["AppliedLevel"] = SystemParamsInFile.APPLIED_LEVEL_TERTIARY;
            IQueryable<SchoolProfile> iqSchool = SchoolProfileBusiness.Search(dic);

            /*
             iqStatisticOfTertiary = StatisticOfTertiaryBusiness.Search(dic)
             dic[“Year”] = Year
             */
            IDictionary<string, object> dictionary = new Dictionary<string, object>();
            dictionary["Year"] = Year;
            IQueryable<StatisticOfTertiary> iqStatisticOfTertiary = this.Search(dictionary);
            IQueryable<StatisticOfTertiaryBO> query = from s in iqSchool
                                                      join sot in iqStatisticOfTertiary on s.SchoolProfileID equals sot.SchoolID into g1
                                                      from j2 in g1.DefaultIfEmpty()
                                                      select new StatisticOfTertiaryBO
                                                      {
                                                          SchoolID = s.SchoolProfileID,
                                                          Year = Year,
                                                          SchoolName = s.SchoolName,
                                                          TrainingType = s.TrainingTypeID,
                                                          TrainingTypeResolution = s.TrainingType.Resolution,
                                                          DistrictID = s.DistrictID,
                                                          DistrictName = s.District.DistrictName,
                                                          StatisticOfTertiaryID = j2.StatisticOfTertiaryID,
                                                          SentDate = j2.SentDate,
                                                          StatusResolution = j2.SentDate.HasValue ? "Đã báo cáo" : "Chưa báo cáo",
                                                          EnableDetail = j2.SentDate.HasValue ? true : false
                                                      };
            if (!Status.HasValue)
            {
                return query;
            }
            else
            {
                if (Status.Value == 1)
                {
                    query = query.Where(o => o.SentDate == null);
                }
                else if (Status.Value == 2)
                {
                    query = query.Where(o => o.SentDate != null);
                }
            }
            return query;
        }

        /// <summary>
        /// SearchBySchool
        /// </summary>
        /// <param name="SchoolID">The school ID.</param>
        /// <param name="SearchInfo">The search info.</param>
        /// <returns></returns>
        /// <author>
        /// dungnt77
        /// </author>
        /// <remarks>
        /// 17/01/2013   9:52 AM
        /// </remarks>
        public IQueryable<StatisticOfTertiary> SearchBySchool(int SchoolID, IDictionary<string, object> SearchInfo = null)
        {
            if (SchoolID == 0)
                return null;
            if (SearchInfo == null)
            {
                SearchInfo = new Dictionary<string, object>() { };
            }

            SearchInfo["SchoolID"] = SchoolID;
            return Search(SearchInfo);
        }

        /// <summary>
        /// Tạo file excel báo cáo của trường
        /// author: namdv3
        /// </summary>
        /// <param name="AcademicYearID"></param>
        /// <param name="reportName"></param>
        /// <returns></returns>
        public Stream CreateSchoolReport(int AcademicYearID, out string reportName)
        {
            //Tạo file excel từ template SMAS 3.1\05.DESIGN\Draft\Templates \HS\ Mau_2_THPT_Mau truong.xls
            ReportDefinition reportDefinition = ReportDefinitionBusiness.GetByCode(SystemParamsInFile.HS_THPT_BAOCAOSOLIEU_DN);

            string template = ReportUtils.GetTemplatePath(reportDefinition);
            reportName = reportDefinition.OutputNamePattern;
            IVTWorkbook oBook = VTExport.OpenWorkbook(template);
            IVTWorksheet dataSheet = oBook.GetSheet(1);
            IVTWorksheet resultSheet = oBook.GetSheet(4);
            IVTWorksheet tempSheet = oBook.GetSheet(2);
            IVTRange dataRow = tempSheet.GetRange("H32", "I35");
            IVTRange BanCoBan_Range = tempSheet.GetRange("A32", "B35");
            IVTRange BanCoBan_TrongDo_Range = tempSheet.GetRange("C33", "D35");
            IVTRange BanCoBan_SubCom_Range = tempSheet.GetRange("C34", "D35");
            //Từ AcademicYearID => SchoolID
            int SchoolID = AcademicYearBusiness.Find(AcademicYearID).SchoolID;
            IDictionary<string, object> dic = new Dictionary<string, object>();
            dic["AcademicYearID"] = AcademicYearID;
            //StatisticOfTertiary = StatisticOfTertiaryBusiness.SearchBySchool(SchoolID, dic)
            StatisticOfTertiary statisticOfTertiary = this.SearchBySchool(SchoolID, dic).FirstOrDefault();
            if(statisticOfTertiary==null)
            {
                statisticOfTertiary = new StatisticOfTertiary();
            }

            int? A10 =  statisticOfTertiary.PupilFromSecondary;
            int? B10 = statisticOfTertiary.PupilOfOther;
            int? C10 = A10 + B10;
            int? A15 = statisticOfTertiary.ComputerRoom;
            int? B15 = statisticOfTertiary.NumberOfComputer;
            int? C15 = statisticOfTertiary.NumberOfComputerInternetConnected;
            bool? D15 = statisticOfTertiary.HasWebsite;
            bool? E15 = statisticOfTertiary.HasDatabase;
            int? F15 = statisticOfTertiary.LearningRoom;

            resultSheet.SetCellValue("A10", A10);
            resultSheet.SetCellValue("B10", B10);
            resultSheet.SetCellValue("C10", C10);
            resultSheet.SetCellValue("A15", A15);
            resultSheet.SetCellValue("B15", B15);
            resultSheet.SetCellValue("C15", C15);
            resultSheet.SetCellValue("D15", (D15.HasValue && D15.Value) ? "Có" : "Không");
            resultSheet.SetCellValue("E15", (E15.HasValue && E15.Value) ? "Có" : "Không");
            resultSheet.SetCellValue("F15", F15);

            //lấy danh sách khối học
            IQueryable<EducationLevel> listEducationLevel = EducationLevelBusiness.GetByGrade(SystemParamsInFile.APPLIED_LEVEL_TERTIARY);
            //iqStatisticSubCommitteeOfTertiary = StatisticSubCommitteeOfTertiaryBusiness.Search(dic)
            IQueryable<StatisticSubcommitteeOfTertiary> iqStatisticSubCommitteeOfTertiary = StatisticSubcommitteeOfTertiaryBusiness.Search(dic);
            //iqStatisticSubjectOfTertiary = StatisticSubjectOfTertiaryBusiness.Search(dic)
            IQueryable<StatisticSubjectOfTertiary> iqStatisticSubjectOfTertiary = StatisticSubjectOfTertiaryBusiness.Search(dic);
            //Lấy danh sách các ban: lstSubCommittee =  SubCommitteeBusiness.Search().
            IQueryable<SubCommittee> lstSubCommittee = SubCommitteeBusiness.Search(dic);
            /*
                Lấy từng ban trong lstSubCommittee:
                Tính số lớp, số học sinh tương ứng với mỗi ban:
                TotalClass = Sum(iqStatisticSubCommitteeOfTertiary.TotalClass where SubCommitteeID = SubCommitteeID)
                TotalPupil = Sum(iqStatisticSubCommitteeOfTertiary.TotalPupil where SubCommitteeID = SubCommitteeID)
             */
            List<SubCommittee> banChuyen = lstSubCommittee.Where(o => o.Resolution.ToLower().Contains("cơ bản") == false).ToList();
            List<SubCommittee> banCoban = lstSubCommittee.Where(o => o.Resolution.ToLower().Contains("cơ bản") == true).ToList();
            #region tạo khung
            int TotalCol = 0;
            int CountSubCommittee = lstSubCommittee.Count();
            if (lstSubCommittee != null && lstSubCommittee.Count() > 0)
            {
                
                int subCommitteeIndex = 0;
                if (banChuyen != null && banChuyen.Count > 0)
                {
                    foreach (var subCommittee in banChuyen)
                    {
                        resultSheet.CopyPaste(dataRow, 18, subCommitteeIndex * 2 + 4, false);
                        resultSheet.SetCellValue(18, subCommitteeIndex * 2 + 4, subCommittee.Resolution);
                        resultSheet.GetRange(18, subCommitteeIndex * 2 + 4, 19, subCommitteeIndex * 2 + 5).Merge();
                        subCommitteeIndex++;
                    }
                }
                resultSheet.CopyPaste(BanCoBan_Range, 18, banChuyen.Count * 2 + 4, false);
                int subCommitteeCoBanIndex = 0;
                if (banCoban != null && banCoban.Count > 0)
                {
                    foreach (var subCommittee in banCoban)
                    {
                        resultSheet.CopyPaste(tempSheet.GetRange("E30", "F30"), 18, banChuyen.Count * 2 + 4 + 2 + subCommitteeCoBanIndex * 2, false);
                        resultSheet.CopyPaste(BanCoBan_TrongDo_Range, 19, banChuyen.Count * 2 + 4 + 2 + subCommitteeCoBanIndex * 2, false);

                        resultSheet.SetCellValue(20, banChuyen.Count * 2 + 4 + 2 + subCommitteeCoBanIndex * 2, subCommittee.Resolution);
                        subCommitteeCoBanIndex++;
                    }
                    //tao khung cho "1-2 Môn NC" va "Không học NC"
                    resultSheet.CopyPaste(tempSheet.GetRange("E30", "F30"), 18, banChuyen.Count * 2 + 4 + 2 + subCommitteeCoBanIndex * 2, false);
                    resultSheet.CopyPaste(BanCoBan_TrongDo_Range, 19, banChuyen.Count * 2 + 4 + 2 + subCommitteeCoBanIndex * 2, false);
                    resultSheet.SetCellValue(20, banChuyen.Count * 2 + 4 + 2 + subCommitteeCoBanIndex * 2, "1-2 Môn NC");
                    resultSheet.CopyPaste(tempSheet.GetRange("E30", "F30"), 18, banChuyen.Count * 2 + 4 + 2 + (subCommitteeCoBanIndex + 1) * 2, false);
                    resultSheet.CopyPaste(BanCoBan_TrongDo_Range, 19, banChuyen.Count * 2 + 4 + 2 + (subCommitteeCoBanIndex + 1) * 2, false);
                    resultSheet.SetCellValue(20, banChuyen.Count * 2 + 4 + 2 + (subCommitteeCoBanIndex + 1) * 2, "Không học NC");
                }

                TotalCol = CountSubCommittee * 2 + 4 + 2 + 3;
                //MergeRow BAN CƠ BẢN
                resultSheet.GetRange(18, banChuyen.Count * 2 + 4, 18, CountSubCommittee * 2 + 9).Merge();
                // MergeRow Trong đó
                resultSheet.GetRange(19, banChuyen.Count * 2 + 4 + 2, 19, CountSubCommittee * 2 + 9).Merge();
            }
            IVTRange Row_Rank = dataSheet.GetRange(22, 1, 22, TotalCol);
            #endregion
            int CountEducationLevel = 0;
            int row_index = 22;
            if (listEducationLevel != null && listEducationLevel.Count() > 0)
            {
                CountEducationLevel = listEducationLevel.Count();
                foreach (var educationLevel in listEducationLevel)
                {
                    //coppy Row
                    resultSheet.CopyPaste(Row_Rank, row_index, 1, true);
                    #region fill dữ liệu cho từng khối học vào file excel
                    int colIndex = 1;
                    //khối
                    resultSheet.SetCellValue(row_index, colIndex++, educationLevel.Resolution);
                    //tổng số lớp
                    //string totalClassFormula_item = "=";
                    resultSheet.SetCellValue(row_index, colIndex++, 0);
                    //tổng số học sinh
                    //string totalPupilFormula_item = "=";
                    resultSheet.SetCellValue(row_index, colIndex++, 0);
                    //Các ban chuyên
                    if (banChuyen != null && banChuyen.Count > 0)
                    {
                        int TotalClass = 0;
                        int TotalPupil = 0;
                        foreach (var subCommittee in banChuyen)
                        {
                            StatisticSubcommitteeOfTertiary ssot = iqStatisticSubCommitteeOfTertiary.Where(o => o.SubCommitteeID == subCommittee.SubCommitteeID
                                                                    && o.EducationLevelID == educationLevel.EducationLevelID).FirstOrDefault();
                            if (ssot != null)
                            {
                                if (ssot.TotalClass.HasValue)
                                {
                                    TotalClass += ssot.TotalClass.Value;
                                }
                                if (ssot.TotalPupil.HasValue)
                                {
                                    TotalPupil += ssot.TotalPupil.Value;
                                }
                            }
                            resultSheet.CopyPaste(tempSheet.GetRange("C36", "D36"), row_index, colIndex, false);
                            //string Col_Class_Char = VTVector.ColumnIntToString(colIndex);
                            //totalClassFormula_item += Col_Class_Char + row_index.ToString() + "+";
                            //số lớp
                            resultSheet.SetCellValue(row_index, colIndex++, TotalClass);
                            //string Col_Pupil_Char = VTVector.ColumnIntToString(colIndex);
                            //totalPupilFormula_item += Col_Class_Char + row_index.ToString() + "+";
                            //số học sinh
                            resultSheet.SetCellValue(row_index, colIndex++, TotalPupil);
                        }
                    }
                    //totalClassFormula_item += VTVector.ColumnIntToString(banChuyen.Count * 2 + 4) + row_index.ToString();
                    //totalPupilFormula_item += VTVector.ColumnIntToString(banChuyen.Count * 2 + 5) + row_index.ToString();
                    //resultSheet.SetFormulaValue(row_index, 2, totalClassFormula_item);
                    //resultSheet.SetFormulaValue(row_index, 3, totalPupilFormula_item);
                    resultSheet.CopyPaste(tempSheet.GetRange("C37", "D37"), row_index, colIndex, false);
                    //số lớp
                    resultSheet.SetCellValue(row_index, colIndex++, 0);
                    //số học sinh
                    resultSheet.SetCellValue(row_index, colIndex++, 0);
                    //Các ban trong ban cơ bản
                    if (banCoban != null && banCoban.Count > 0)
                    {
                        int TotalClass = 0;
                        int TotalPupil = 0;
                        if (lstSubCommittee != null && lstSubCommittee.Count() > 0)
                        {
                            foreach (var subCommittee in banCoban)
                            {
                                StatisticSubcommitteeOfTertiary ssot = iqStatisticSubCommitteeOfTertiary.Where(o => o.SubCommitteeID == subCommittee.SubCommitteeID && o.EducationLevelID == educationLevel.EducationLevelID).FirstOrDefault();
                                if (ssot != null)
                                {
                                    if (ssot.TotalClass.HasValue)
                                    {
                                        TotalClass += ssot.TotalClass.Value;
                                    }
                                    if (ssot.TotalPupil.HasValue)
                                    {
                                        TotalPupil += ssot.TotalPupil.Value;
                                    }
                                }
                                resultSheet.CopyPaste(tempSheet.GetRange("C36", "D36"), row_index, colIndex, false);
                                //số lớp
                                resultSheet.SetCellValue(row_index, colIndex++, TotalClass);
                                //số học sinh
                                resultSheet.SetCellValue(row_index, colIndex++, TotalPupil);
                            }
                        }
                    }
                    //Các ban có tên mở đầu bằng “Ban cơ bản” và ban “1-2 môn NC”, “Không học NC” hợp lại thành ban cơ bản
                    /*
                        Bổ sung ban “1-2 môn NC”
                        TotalClass = Sum(iqStatisticSubCommitteeOfTertiary.TotalClass where SpecialType = 1)
                        TotalPupil = Sum(iqStatisticSubCommitteeOfTertiary.TotalPupil where SpecialType = 1)
                        Bổ sung ban “Không học NC”
                        TotalClass = Sum(iqStatisticSubCommitteeOfTertiary.TotalClass where SpecialType = 2)
                        TotalPupil = Sum(iqStatisticSubCommitteeOfTertiary.TotalPupil where SpecialType = 2)
                     */
                    int? TotalClass12NC = null;
                    int? TotalPupil12NC = null;
                    int? TotalClass0NC = null;
                    int? TotalPupil0NC = null;
                    StatisticSubcommitteeOfTertiary nc = iqStatisticSubCommitteeOfTertiary.Where(o => o.SpecicalType == 1 && o.EducationLevelID == educationLevel.EducationLevelID).FirstOrDefault();
                    StatisticSubcommitteeOfTertiary NoNc = iqStatisticSubCommitteeOfTertiary.Where(o => o.SpecicalType == 2 && o.EducationLevelID == educationLevel.EducationLevelID).FirstOrDefault();
                    if (nc != null)
                    {
                        TotalClass12NC = nc.TotalClass;
                        TotalPupil12NC = nc.TotalPupil;
                    }
                    if (NoNc != null)
                    {
                        TotalClass0NC = NoNc.TotalClass;
                        TotalPupil0NC = NoNc.TotalPupil;
                    }
                    resultSheet.CopyPaste(tempSheet.GetRange("C36", "D36"), row_index, colIndex, false);
                    //ban “1-2 môn NC”
                    //số lớp
                    resultSheet.SetCellValue(row_index, colIndex++, TotalClass12NC);
                    //số học sinh
                    resultSheet.SetCellValue(row_index, colIndex++, TotalPupil12NC);
                    //Ban Không học môn NC
                    resultSheet.CopyPaste(tempSheet.GetRange("C36", "D36"), row_index, colIndex, false);
                    //số lớp
                    resultSheet.SetCellValue(row_index, colIndex++, TotalClass0NC);
                    //số học sinh
                    resultSheet.SetCellValue(row_index, colIndex++, TotalPupil0NC);
                    #endregion
                    row_index++;
                }

                int _row_index = 22;
                foreach (var educationLevel in listEducationLevel)
                {
                    int colIndex = 4;
                    string totalClassFormula_item = "=";
                    string totalPupilFormula_item = "=";
                    foreach (var subCommittee in banChuyen)
                    {
                        string Col_Class_Char = VTVector.ColumnIntToString(colIndex++);
                        totalClassFormula_item += Col_Class_Char + _row_index.ToString() + "+";
                        string Col_Pupil_Char = VTVector.ColumnIntToString(colIndex++);
                        totalPupilFormula_item += Col_Pupil_Char + _row_index.ToString() + "+";
                    }
                    totalClassFormula_item += VTVector.ColumnIntToString(banChuyen.Count * 2 + 4) + _row_index.ToString();
                    totalPupilFormula_item += VTVector.ColumnIntToString(banChuyen.Count * 2 + 5) + _row_index.ToString();
                    resultSheet.SetFormulaValue(_row_index, 2, totalClassFormula_item);
                    resultSheet.SetFormulaValue(_row_index, 3, totalPupilFormula_item);
                    //set formula cho ban co ban

                    int col_coban = banChuyen.Count * 2 + 6;
                    string total_Coban_ClassFormula_item = "=";
                    string total_Coban_PupilFormula_item = "=";
                    foreach (var subCommittee in banCoban)
                    {
                        string Col_Class_Char = VTVector.ColumnIntToString(col_coban++);
                        total_Coban_ClassFormula_item += Col_Class_Char + _row_index.ToString() + "+";
                        string Col_Pupil_Char = VTVector.ColumnIntToString(col_coban++);
                        total_Coban_PupilFormula_item += Col_Pupil_Char + _row_index.ToString() + "+";
                    }
                    total_Coban_ClassFormula_item += VTVector.ColumnIntToString((banChuyen.Count + banCoban.Count) * 2 + 6) + _row_index.ToString() + "+";
                    total_Coban_ClassFormula_item += VTVector.ColumnIntToString((banChuyen.Count + banCoban.Count) * 2 + 8) + _row_index.ToString();
                    total_Coban_PupilFormula_item += VTVector.ColumnIntToString((banChuyen.Count + banCoban.Count) * 2 + 7) + _row_index.ToString() + "+";
                    total_Coban_PupilFormula_item += VTVector.ColumnIntToString((banChuyen.Count + banCoban.Count) * 2 + 9) + _row_index.ToString();
                    resultSheet.SetFormulaValue(_row_index, banChuyen.Count * 2 + 4, total_Coban_ClassFormula_item);
                    resultSheet.SetFormulaValue(_row_index, banChuyen.Count * 2 + 5, total_Coban_PupilFormula_item);

                    _row_index++;



                }

               

                #region Dòng tổng
                //coppy Row
                resultSheet.CopyPaste(Row_Rank, row_index, 1, true);
                //khối
                resultSheet.SetCellValue(row_index, 1, "TỔNG");
                string row = "";
                int start_col = 4;
                //Tổng số lớp =SUM(B22:B24)
                string totalClassFormula = "=SUM(B22:B" + (22 + CountEducationLevel - 1).ToString() + ")";
                resultSheet.SetFormulaValue(22 + CountEducationLevel, 2, totalClassFormula);
                //Tổng số học sinh =SUM(C22:C24)
                string totalPupilFormula = "=SUM(C22:C" + (22 + CountEducationLevel - 1).ToString() + ")";
                resultSheet.SetFormulaValue(22 + CountEducationLevel, 3, totalPupilFormula);

                if (banChuyen != null && banChuyen.Count > 0)
                {
                    if (lstSubCommittee != null && lstSubCommittee.Count() > 0)
                    {
                        for (var i = 0; i < banChuyen.Count * 2; i++)
                        {
                            row = VTVector.ColumnIntToString(start_col);
                            string totalFormula_item = string.Format("=SUM({0}22:{0}{1})", row, (22 + CountEducationLevel - 1).ToString());
                            resultSheet.CopyPaste(tempSheet.GetRange("C37", "D37"), 22 + CountEducationLevel, start_col, false);
                            resultSheet.SetFormulaValue(22 + CountEducationLevel, start_col, totalFormula_item);
                            start_col++;
                        }
                    }
                }
                resultSheet.CopyPaste(tempSheet.GetRange("C37", "D37"), 22 + CountEducationLevel, start_col, false);
                //formula số lớp vs số học sinh của ban cơ bản
                row = VTVector.ColumnIntToString(start_col);
                string totalClassFormula_CoBan = string.Format("=SUM({0}22:{0}{1})", row, (22 + CountEducationLevel - 1).ToString());
                resultSheet.SetFormulaValue(22 + CountEducationLevel, start_col, totalClassFormula_CoBan);
                start_col++;
                row = VTVector.ColumnIntToString(start_col);
                string totalPupilFormula_CoBan = string.Format("=SUM({0}22:{0}{1})", row, (22 + CountEducationLevel - 1).ToString());
                resultSheet.SetFormulaValue(22 + CountEducationLevel, start_col, totalPupilFormula_CoBan);
                start_col++;
                if (banCoban != null && banCoban.Count > 0)
                {
                    if (lstSubCommittee != null && lstSubCommittee.Count() > 0)
                    {
                        for (var i = 0; i < banCoban.Count * 2; i++)
                        {
                            row = VTVector.ColumnIntToString(start_col);
                            string totalFormula_item = string.Format("=SUM({0}22:{0}{1})", row, (22 + CountEducationLevel - 1).ToString());
                            resultSheet.CopyPaste(tempSheet.GetRange("C37", "D37"), 22 + CountEducationLevel, start_col, false);
                            resultSheet.SetFormulaValue(22 + CountEducationLevel, start_col, totalFormula_item);
                            start_col++;
                        }
                    }
                }
                //formula 1-2 môn NC
                resultSheet.CopyPaste(tempSheet.GetRange("C37", "D37"), 22 + CountEducationLevel, start_col, false);
                row = VTVector.ColumnIntToString(start_col);
                string totalClassFormula_12NC = string.Format("=SUM({0}22:{0}{1})", row, (22 + CountEducationLevel - 1).ToString());
                resultSheet.SetFormulaValue(22 + CountEducationLevel, start_col, totalClassFormula_12NC);
                start_col++;
                row = VTVector.ColumnIntToString(start_col);
                string totalPupilFormula_12NC = string.Format("=SUM({0}22:{0}{1})", row, (22 + CountEducationLevel - 1).ToString());
                resultSheet.SetFormulaValue(22 + CountEducationLevel, start_col, totalPupilFormula_12NC);
                start_col++;
                //formula Không học NC
                resultSheet.CopyPaste(tempSheet.GetRange("C37", "D37"), 22 + CountEducationLevel, start_col, false);
                row = VTVector.ColumnIntToString(start_col);
                string totalClassFormula_0NC = string.Format("=SUM({0}22:{0}{1})", row, (22 + CountEducationLevel - 1).ToString());
                resultSheet.SetFormulaValue(22 + CountEducationLevel, start_col, totalClassFormula_0NC);
                start_col++;
                row = VTVector.ColumnIntToString(start_col);
                string totalPupilFormula_0NC = string.Format("=SUM({0}22:{0}{1})", row, (22 + CountEducationLevel - 1).ToString());
                resultSheet.SetFormulaValue(22 + CountEducationLevel, start_col, totalPupilFormula_0NC);

                #endregion


            }



            #region TẠO BẢNG CƠ CẤU GIÁO VIÊN THEO MÔN HỌC CẤP THPT NĂM HỌC
            //coppy excel from tempSheet to dataSheet
            IVTRange CoCauGiaoVien_Range = tempSheet.GetRange("A10", "D13");
            resultSheet.CopyPaste(CoCauGiaoVien_Range, row_index + 3, 1, false);
            //Lấy danh sách môn học: lstSubject = SchoolSubjectBusiness.SearchBySchool(UserInfo.SchoolID, dic)
            //                                      .Select(o => o.SubjectCat).Distinct()
            List<SubjectCat> lstSubject = SchoolSubjectBusiness.SearchBySchool(SchoolID, dic).Select(o => o.SubjectCat).Distinct().ToList();

            //trong biên chế
            resultSheet.SetCellValue(row_index + 6, 2, statisticOfTertiary.NumOfStaff);
            //Với mỗi môn học trong lstSubject:
            int colIndex_CoCauGV = 4;
            if (lstSubject != null && lstSubject.Count > 0)
            {
                foreach (var subject in lstSubject)
                {
                    //Tên môn học
                    resultSheet.CopyPaste(tempSheet.GetRange("D11", "D13"), row_index + 4, colIndex_CoCauGV);
                    resultSheet.SetCellValue(row_index + 5, colIndex_CoCauGV, subject.DisplayName);
                    int? TotalTeacher = 0;
                    if (iqStatisticSubjectOfTertiary.Where(o => o.SubjectID == subject.SubjectCatID).Count() > 0)
                    {
                        TotalTeacher = iqStatisticSubjectOfTertiary.Where(o => o.SubjectID == subject.SubjectCatID).FirstOrDefault().TotalTeacher;
                    }
                    resultSheet.SetCellValue(row_index + 6, colIndex_CoCauGV++, TotalTeacher);
                }
                resultSheet.CopyPaste(tempSheet.GetRange("D11", "D13"), row_index + 4, colIndex_CoCauGV);
                resultSheet.SetCellValue(row_index + 5, colIndex_CoCauGV, "Môn khác dạy GDCD");
                int? TotalTeacher_MKD = 0;
                if (iqStatisticSubjectOfTertiary.Where(o => o.SpecicalType == 1).Count() > 0)
                {
                    TotalTeacher_MKD = iqStatisticSubjectOfTertiary.Where(o => o.SpecicalType == 1).FirstOrDefault().TotalTeacher;
                }
                resultSheet.SetCellValue(row_index + 6, colIndex_CoCauGV, TotalTeacher_MKD);
                resultSheet.GetRange(29, 4, 29, 4 + lstSubject.Count).Merge();
            }
            IVTRange TrinhDo_Range = tempSheet.GetRange("A16", "C18");
            resultSheet.CopyPaste(TrinhDo_Range, row_index + 4, colIndex_CoCauGV + 1, false);
            resultSheet.SetCellValue(row_index + 6, colIndex_CoCauGV + 1, statisticOfTertiary.NumOfStandard);
            resultSheet.SetCellValue(row_index + 6, colIndex_CoCauGV + 2, statisticOfTertiary.NumOfOverStandard);
            resultSheet.SetCellValue(row_index + 6, colIndex_CoCauGV + 3, statisticOfTertiary.NumOfUnderStandard);
            //set formula cho TSGV
            string start_Char = VTVector.ColumnIntToString(colIndex_CoCauGV + 1);
            string end_Char = VTVector.ColumnIntToString(colIndex_CoCauGV + 3);
            string TSGV_Formula = string.Format("=SUM({0}{1}:{2}{1})", start_Char, (row_index + 6).ToString(), end_Char, (row_index + 6).ToString());
            resultSheet.SetFormulaValue(row_index + 6, 1, TSGV_Formula);
            #endregion

             
            //Người báo cáo
            IVTRange NguoiBaoCao_Range = tempSheet.GetRange("R33","V33");

            if ((CountSubCommittee + 5) > (lstSubject.Count + 6))
            {
                resultSheet.CopyPaste(oBook.GetSheet(3).GetRange("R33", "V33"), 33, CountSubCommittee);
            }
            else
            {
                resultSheet.CopyPaste(oBook.GetSheet(3).GetRange("R33", "V33"), 33, lstSubject.Count + 2);
            }
            //dataSheet.CopyPaste(NguoiBaoCao_Range, row_index + 8, 18, false);
            reportName = reportName + ".xls";
            Dictionary<string, object> dicVarable = new Dictionary<string, object>();
            SchoolProfile school = SchoolProfileBusiness.Find(SchoolID);
            AcademicYear acaYear = AcademicYearBusiness.Find(AcademicYearID);
            dicVarable["SupervisingDept"] = school.SupervisingDept.SupervisingDeptName;
            dicVarable["SchoolName"] = school.SchoolName;
            dicVarable["AcademicYear"] = acaYear.Year + " - " + (acaYear.Year + 1);
            resultSheet.FillVariableValue(dicVarable);
            oBook.GetSheet(1).Delete();
            oBook.GetSheet(1).Delete();
            oBook.GetSheet(1).Delete();
            return oBook.ToStream();
        }

        /// <summary>
        /// Tạo báo cáo tổng hợp của Sở
        /// author: namdv3
        /// </summary>
        /// <param name="ProvinceID"></param>
        /// <param name="Year"></param>
        /// <param name="reportName"></param>
        /// <returns></returns>
        public Stream CreateProvinceReport(int SupervisingDeptID, int ProvinceID, int Year, out string reportName)
        {
            //Tạo file excel từ template SMAS 3.1\05.DESIGN\Draft\Templates \HS\ Mau_2_THPT_Mau truong.xls
            ReportDefinition reportDefinition = ReportDefinitionBusiness.GetByCode(SystemParamsInFile.MAU_2_THPT_SO);
            string template = ReportUtils.GetTemplatePath(reportDefinition);
            reportName = reportDefinition.OutputNamePattern;
            IVTWorkbook oBook = VTExport.OpenWorkbook(template);
            IVTWorksheet dataSheet = oBook.GetSheet(1);
            IVTWorksheet tempSheet = oBook.GetSheet(2);
            IVTRange dataRow = tempSheet.GetRange("F16", "G20");
            IVTRange BanCoBan_Range = tempSheet.GetRange("F1", "G4");
            IVTRange BanCoBan_TrongDo_Range = tempSheet.GetRange("H1", "I4");
            IVTRange ColHasColor = tempSheet.GetRange("G6", "G6");
            IVTRange ColNoColor = tempSheet.GetRange("I6", "I6");
            IQueryable<EducationLevel> listEducationLevel = EducationLevelBusiness.GetByGrade(SystemParamsInFile.APPLIED_LEVEL_TERTIARY);
            //dic[“ProvinceID”] =ProvinceID
            //dic[“AppliedLevel”] = APPLIED_LEVEL_TERTIARY
            //Danh sách trường: iqSchool = SchoolProfileBusiness.Search(dic)

            IDictionary<string, object> dic = new Dictionary<string, object>();
            dic["ProvinceID"] = ProvinceID;
            dic["AppliedLevel"] = SystemParamsInFile.APPLIED_LEVEL_TERTIARY;
            IQueryable<SchoolProfile> iqSchool = SchoolProfileBusiness.Search(dic);

            IDictionary<string, object> dictionary = new Dictionary<string, object>();
            dictionary["Year"] = Year;
            //iqStatisticOfTertiary = StatisticOfTertiaryBusiness.Search(dic)
            IQueryable<StatisticOfTertiary> iqStatisticOfTertiary = this.Search(dic);
            //iqStatisticSubCommitteeOfTertiary = StatisticSubCommitteeOfTertiaryBusiness.Search(dic)
            IQueryable<StatisticSubcommitteeOfTertiary> iqStatisticSubCommitteeOfTertiary = StatisticSubcommitteeOfTertiaryBusiness.Search(dic);
            // iqStatisticSubjectOfTertiary = StatisticSubjectOfTertiaryBusiness.Search(dic)
            IQueryable<StatisticSubjectOfTertiary> iqStatisticSubjectOfTertiary = StatisticSubjectOfTertiaryBusiness.Search(dic);
            /*  Tạo đối tượng StatisticOfTertiary sot
                var iqStatistic =  iqSchool join iqStatisticOfTertiary theo SchoolID 
                sot.PupilFromSecondary = Sum(iqStatistic.PupilFromSecondary)
                sot.PupilOfOther = Sum(iqStatistic.PupilOfOther)*/
            StatisticOfTertiary sot = new StatisticOfTertiary();
            var iqStatistic = from iqs in iqSchool
                              join iqst in iqStatisticOfTertiary on iqs.SchoolProfileID equals iqst.SchoolID
                              select iqst;
            sot.PupilFromSecondary = iqStatistic.Sum(o => o.PupilFromSecondary);
            sot.PupilOfOther = iqStatistic.Sum(o => o.PupilOfOther);
            sot.NumOfUnderStandard = iqStatistic.Sum(o => o.NumOfUnderStandard);
            sot.NumOfStandard = iqStatistic.Sum(o => o.NumOfStandard);
            sot.NumOfStaff = iqStatistic.Sum(o => o.NumOfStaff);
            sot.NumOfOverStandard = iqStatistic.Sum(o => o.NumOfOverStandard);
            sot.NumberOfComputerInternetConnected = iqStatistic.Sum(o => o.NumberOfComputerInternetConnected);
            sot.NumberOfComputer = iqStatistic.Sum(o => o.NumberOfComputer);
            sot.LearningRoom = iqStatistic.Sum(o => o.LearningRoom);

            #region Fill du lieu vao Nguon Tuyen Sinh Lop 10 va Co So Vat Chat Thiet bi tin hoc
            int? A10 = sot.PupilFromSecondary;
            int? B10 = sot.PupilOfOther;
            int? C10 = A10 + B10;
            int? A15 = sot.ComputerRoom;
            int? B15 = sot.NumberOfComputer;
            int? C15 = sot.NumberOfComputerInternetConnected;
            int? F15 = sot.LearningRoom;

            dataSheet.SetCellValue("A10", A10);
            dataSheet.SetCellValue("B10", B10);
            dataSheet.SetCellValue("C10", C10);
            dataSheet.SetCellValue("A15", A15);
            dataSheet.SetCellValue("B15", B15);
            dataSheet.SetCellValue("C15", C15);
            dataSheet.SetCellValue("F15", F15);
            #endregion

            //Lấy danh sách các ban: lstSubCommittee =  SubCommitteeBusiness.Search().
            List<SubCommittee> lstSubCommittee = SubCommitteeBusiness.Search(new Dictionary<string, object>()).ToList();
            List<SubCommittee> banChuyen = lstSubCommittee.Where(o => o.Resolution.ToLower().Contains("cơ bản") == false).ToList();
            List<SubCommittee> banCoban = lstSubCommittee.Where(o => o.Resolution.ToLower().Contains("cơ bản") == true).ToList();


            #region tạo khung
            int TotalCol = 0;
            if (lstSubCommittee != null && lstSubCommittee.Count() > 0)
            {
                int CountSubCommittee = lstSubCommittee.Count();
                int subCommitteeIndex = 0;
                if (banChuyen != null && banChuyen.Count > 0)
                {
                    foreach (var subCommittee in banChuyen)
                    {
                        dataSheet.CopyPaste(dataRow, 18, subCommitteeIndex * 2 + 4, false);
                        dataSheet.SetCellValue(18, subCommitteeIndex * 2 + 4, subCommittee.Resolution);
                        dataSheet.GetRange(18, subCommitteeIndex * 2 + 4, 19, subCommitteeIndex * 2 + 5).Merge();
                        subCommitteeIndex++;
                    }
                }
                dataSheet.CopyPaste(BanCoBan_Range, 18, banChuyen.Count * 2 + 4, false);

                int subCommitteeCoBanIndex = 0;
                if (banCoban != null && banCoban.Count > 0)
                {
                    foreach (var subCommittee in banCoban)
                    {
                        dataSheet.CopyPaste(BanCoBan_TrongDo_Range, 18, banChuyen.Count * 2 + 4 + 2 + subCommitteeCoBanIndex * 2, false);
                        dataSheet.SetCellValue(20, banChuyen.Count * 2 + 4 + 2 + subCommitteeCoBanIndex * 2, subCommittee.Resolution);
                        subCommitteeCoBanIndex++;
                    }
                    //tao khung cho "1-2 Môn NC" va "Không học NC"
                    dataSheet.CopyPaste(BanCoBan_TrongDo_Range, 18, banChuyen.Count * 2 + 4 + 2 + subCommitteeCoBanIndex * 2, false);
                    dataSheet.SetCellValue(20, banChuyen.Count * 2 + 4 + 2 + subCommitteeCoBanIndex++ * 2, "1-2 Môn NC");
                    dataSheet.CopyPaste(BanCoBan_TrongDo_Range, 18, banChuyen.Count * 2 + 4 + 2 + subCommitteeCoBanIndex * 2, false);
                    dataSheet.SetCellValue(20, banChuyen.Count * 2 + 4 + 2 + subCommitteeCoBanIndex * 2, "Không học NC");
                }

                TotalCol = CountSubCommittee * 2 + 4 + 2 + 3;
                //MergeRow BAN CƠ BẢN
                dataSheet.MergeRow(18, banChuyen.Count * 2 + 4, TotalCol);
                //MergeRow Trong đó
                dataSheet.MergeRow(19, banChuyen.Count * 2 + 4 + 2, TotalCol);
                dataSheet.SetCellValue(19, banChuyen.Count * 2 + 4 + 2, "Trong đó");
            }
            IVTRange Row_Rank = tempSheet.GetRange(22, 1, 22, TotalCol);
            #endregion

            //iqStatisticSubCommittee = iqSchool join iqStatisticSubCommitteeOfTertiary theo SchoolID
            var iqStatisticSubCommittee = from iqs in iqSchool
                                          join iqsst in iqStatisticSubCommitteeOfTertiary on iqs.SchoolProfileID equals iqsst.SchoolID
                                          select new { iqs, iqsst };

            int CountEducationLevel = 0;
            int row_index = 22;
            int _row_index = 22;
            if (listEducationLevel != null && listEducationLevel.Count() > 0)
            {
                CountEducationLevel = listEducationLevel.Count();
                #region set value
                foreach (var educationLevel in listEducationLevel)
                {
                    //coppy Row
                    dataSheet.CopyPaste(Row_Rank, row_index, 1, true);
                    #region fill dữ liệu cho từng khối học vào file excel
                    int colIndex = 1;
                    //khối
                    dataSheet.CopyPaste(ColNoColor, row_index, colIndex, false);
                    dataSheet.SetCellValue(row_index, colIndex++, educationLevel.Resolution);
                    //tổng số lớp
                    dataSheet.CopyPaste(ColNoColor, row_index, colIndex++, false);
                    //tổng số học sinh
                    dataSheet.CopyPaste(ColNoColor, row_index, colIndex++, false);
                    //Các ban chuyên
                    if (banChuyen != null && banChuyen.Count > 0)
                    {
                        int TotalClass = 0;
                        int TotalPupil = 0;
                        foreach (var subCommittee in banChuyen)
                        {
                            StatisticSubcommitteeOfTertiary ssot = iqStatisticSubCommittee.Where(o => o.iqsst.SubCommitteeID == subCommittee.SubCommitteeID
                                                                    && o.iqsst.EducationLevelID == educationLevel.EducationLevelID).Select(o => o.iqsst).FirstOrDefault();
                            if (ssot != null)
                            {
                                if (ssot.TotalClass.HasValue)
                                {
                                    TotalClass += ssot.TotalClass.Value;
                                }
                                if (ssot.TotalPupil.HasValue)
                                {
                                    TotalPupil += ssot.TotalPupil.Value;
                                }
                            }
                            //số lớp
                            dataSheet.CopyPaste(ColNoColor, row_index, colIndex, false);
                            dataSheet.SetCellValue(row_index, colIndex++, TotalClass);
                            //số học sinh
                            dataSheet.CopyPaste(ColNoColor, row_index, colIndex, false);
                            dataSheet.SetCellValue(row_index, colIndex++, TotalPupil);
                        }
                    }
                    //số lớp
                    colIndex++;
                    //số học sinh
                    colIndex++;
                    //Các ban trong ban cơ bản
                    if (banCoban != null && banCoban.Count > 0)
                    {
                        int TotalClass = 0;
                        int TotalPupil = 0;
                        if (lstSubCommittee != null && lstSubCommittee.Count() > 0)
                        {
                            foreach (var subCommittee in banCoban)
                            {
                                StatisticSubcommitteeOfTertiary ssot = iqStatisticSubCommittee.Where(o => o.iqsst.SubCommitteeID == subCommittee.SubCommitteeID
                                                                        && o.iqsst.EducationLevelID == educationLevel.EducationLevelID).Select(o => o.iqsst).FirstOrDefault();
                                if (ssot != null)
                                {
                                    if (ssot.TotalClass.HasValue)
                                    {
                                        TotalClass += ssot.TotalClass.Value;
                                    }
                                    if (ssot.TotalPupil.HasValue)
                                    {
                                        TotalPupil += ssot.TotalPupil.Value;
                                    }
                                }
                                //số lớp
                                dataSheet.CopyPaste(ColNoColor, row_index, colIndex, false);
                                dataSheet.SetCellValue(row_index, colIndex++, TotalClass);
                                //số học sinh
                                dataSheet.CopyPaste(ColNoColor, row_index, colIndex, false);
                                dataSheet.SetCellValue(row_index, colIndex++, TotalPupil);
                            }
                        }
                    }
                    //Các ban có tên mở đầu bằng “Ban cơ bản” và ban “1-2 môn NC”, “Không học NC” hợp lại thành ban cơ bản
                    /*
                        Bổ sung ban “1-2 môn NC”
                        TotalClass = Sum(iqStatisticSubCommittee.TotalClass where SpecialType = 1)
                        TotalPupil = Sum(iqStatisticSubCommittee.TotalPupil where SpecialType = 1)
                        Bổ sung ban “Không học NC”
                        TotalClass = Sum(iqStatisticSubCommittee.TotalClass where SpecialType = 2)
                        TotalPupil = Sum(iqStatisticSubCommittee.TotalPupil where SpecialType = 2)
                     */
                    StatisticSubcommitteeOfTertiary nc = iqStatisticSubCommittee.Where(o => o.iqsst.SpecicalType == 1 && o.iqsst.EducationLevelID == educationLevel.EducationLevelID).Select(o => o.iqsst).FirstOrDefault();
                    StatisticSubcommitteeOfTertiary NoNc = iqStatisticSubCommittee.Where(o => o.iqsst.SpecicalType == 2 && o.iqsst.EducationLevelID == educationLevel.EducationLevelID).Select(o => o.iqsst).FirstOrDefault();
                    int? TotalClass12NC = nc != null ? nc.TotalClass : 0;
                    int? TotalPupil12NC = nc != null ? nc.TotalPupil : 0;
                    int? TotalClass0NC = NoNc != null ? nc.TotalClass : 0;
                    int? TotalPupil0NC = NoNc != null ? nc.TotalClass : 0;
                    //ban “1-2 môn NC”
                    //số lớp
                    dataSheet.CopyPaste(ColNoColor, row_index, colIndex, false);
                    dataSheet.SetCellValue(row_index, colIndex++, TotalClass12NC);
                    //số học sinh
                    dataSheet.CopyPaste(ColNoColor, row_index, colIndex, false);
                    dataSheet.SetCellValue(row_index, colIndex++, TotalPupil12NC);
                    //Ban Không học môn NC
                    //số lớp
                    dataSheet.CopyPaste(ColNoColor, row_index, colIndex, false);
                    dataSheet.SetCellValue(row_index, colIndex++, TotalClass0NC);
                    dataSheet.CopyPaste(ColNoColor, row_index, colIndex, false);
                    //số học sinh
                    dataSheet.SetCellValue(row_index, colIndex++, TotalPupil0NC);
                    #endregion
                    row_index++;
                }
                #endregion
                #region set formula
                foreach (var educationLevel in listEducationLevel)
                {
                    int colIndex = 4;
                    string totalClassFormula_item = "=";
                    string totalPupilFormula_item = "=";
                    foreach (var subCommittee in banChuyen)
                    {
                        string Col_Class_Char = VTVector.ColumnIntToString(colIndex++);
                        totalClassFormula_item += Col_Class_Char + _row_index.ToString() + "+";
                        string Col_Pupil_Char = VTVector.ColumnIntToString(colIndex++);
                        totalPupilFormula_item += Col_Pupil_Char + _row_index.ToString() + "+";
                    }
                    totalClassFormula_item += VTVector.ColumnIntToString(banChuyen.Count * 2 + 4) + _row_index.ToString();
                    totalPupilFormula_item += VTVector.ColumnIntToString(banChuyen.Count * 2 + 5) + _row_index.ToString();
                    dataSheet.CopyPaste(ColHasColor, _row_index, 2, false);
                    dataSheet.CopyPaste(ColHasColor, _row_index, 3, false);
                    dataSheet.SetFormulaValue(_row_index, 2, totalClassFormula_item);
                    dataSheet.SetFormulaValue(_row_index, 3, totalPupilFormula_item);
                    //set formula cho ban co ban

                    int col_coban = banChuyen.Count * 2 + 6;
                    string total_Coban_ClassFormula_item = "=";
                    string total_Coban_PupilFormula_item = "=";
                    foreach (var subCommittee in banCoban)
                    {
                        string Col_Class_Char = VTVector.ColumnIntToString(col_coban++);
                        total_Coban_ClassFormula_item += Col_Class_Char + _row_index.ToString() + "+";
                        string Col_Pupil_Char = VTVector.ColumnIntToString(col_coban++);
                        total_Coban_PupilFormula_item += Col_Pupil_Char + _row_index.ToString() + "+";
                    }
                    total_Coban_ClassFormula_item += VTVector.ColumnIntToString((banChuyen.Count + banCoban.Count) * 2 + 6) + _row_index.ToString() + "+";
                    total_Coban_ClassFormula_item += VTVector.ColumnIntToString((banChuyen.Count + banCoban.Count) * 2 + 8) + _row_index.ToString();
                    total_Coban_PupilFormula_item += VTVector.ColumnIntToString((banChuyen.Count + banCoban.Count) * 2 + 7) + _row_index.ToString() + "+";
                    total_Coban_PupilFormula_item += VTVector.ColumnIntToString((banChuyen.Count + banCoban.Count) * 2 + 9) + _row_index.ToString();
                    dataSheet.CopyPaste(ColHasColor, _row_index, banChuyen.Count * 2 + 4, false);
                    dataSheet.CopyPaste(ColHasColor, _row_index, banChuyen.Count * 2 + 5, false);
                    dataSheet.SetFormulaValue(_row_index, banChuyen.Count * 2 + 4, total_Coban_ClassFormula_item);
                    dataSheet.SetFormulaValue(_row_index, banChuyen.Count * 2 + 5, total_Coban_PupilFormula_item);

                    _row_index++;
                }
                #endregion

                #region Dòng tổng
                int row_total = 22 + CountEducationLevel;
                //coppy Row
                dataSheet.CopyPaste(Row_Rank, row_index, 1, true);
                //khối
                dataSheet.CopyPaste(ColNoColor, row_total, 1, false);
                dataSheet.SetCellValue(row_index, 1, "TỔNG");
                //Tổng số lớp =SUM(B22:B24)
                dataSheet.CopyPaste(ColHasColor, row_total, 2, false);
                string totalClassFormula = "=SUM(B22:B" + (22 + CountEducationLevel - 1).ToString() + ")";
                dataSheet.SetFormulaValue(row_total, 2, totalClassFormula);
                //Tổng số học sinh =SUM(C22:C24)
                dataSheet.CopyPaste(ColHasColor, row_total, 3, false);
                string totalPupilFormula = "=SUM(C22:C" + (22 + CountEducationLevel - 1).ToString() + ")";
                dataSheet.SetFormulaValue(row_total, 3, totalPupilFormula);
                string row = "";
                int start_row = 4;
                if (banChuyen != null && banChuyen.Count > 0)
                {
                    for (var i = 0; i < banChuyen.Count * 2; i++)
                    {
                        row = VTVector.ColumnIntToString(start_row);
                        dataSheet.CopyPaste(ColHasColor, row_total, start_row, false);
                        string totalFormula_item = string.Format("=SUM({0}22:{0}{1})", row, (22 + CountEducationLevel - 1).ToString());
                        dataSheet.SetFormulaValue(row_total, start_row, totalFormula_item);
                        start_row++;
                    }
                }
                //formula số lớp vs số học sinh của ban cơ bản
                row = VTVector.ColumnIntToString(start_row);
                dataSheet.CopyPaste(ColHasColor, row_total, start_row, false);
                string totalClassFormula_CoBan = string.Format("=SUM({0}22:{0}{1})", row, (22 + CountEducationLevel - 1).ToString());
                dataSheet.SetFormulaValue(row_total, start_row, totalClassFormula_CoBan);
                start_row++;
                row = VTVector.ColumnIntToString(start_row);
                dataSheet.CopyPaste(ColHasColor, row_total, start_row, false);
                string totalPupilFormula_CoBan = string.Format("=SUM({0}22:{0}{1})", row, (22 + CountEducationLevel - 1).ToString());
                dataSheet.SetFormulaValue(row_total, start_row, totalPupilFormula_CoBan);
                start_row++;
                if (banCoban != null && banCoban.Count > 0)
                {
                    for (var i = 0; i < banCoban.Count * 2; i++)
                    {
                        row = VTVector.ColumnIntToString(start_row);
                        dataSheet.CopyPaste(ColHasColor, row_total, start_row, false);
                        string totalFormula_item = string.Format("=SUM({0}22:{0}{1})", row, (22 + CountEducationLevel - 1).ToString());
                        dataSheet.SetFormulaValue(row_total, start_row, totalFormula_item);
                        start_row++;
                    }
                }
                //formula 1-2 môn NC
                row = VTVector.ColumnIntToString(start_row);
                dataSheet.CopyPaste(ColHasColor, row_total, start_row, false);
                string totalClassFormula_12NC = string.Format("=SUM({0}22:{0}{1})", row, (22 + CountEducationLevel - 1).ToString());
                dataSheet.SetFormulaValue(row_total, start_row, totalClassFormula_12NC);
                start_row++;
                row = VTVector.ColumnIntToString(start_row);
                dataSheet.CopyPaste(ColHasColor, row_total, start_row, false);
                string totalPupilFormula_12NC = string.Format("=SUM({0}22:{0}{1})", row, (22 + CountEducationLevel - 1).ToString());
                dataSheet.SetFormulaValue(row_total, start_row, totalPupilFormula_12NC);
                start_row++;
                //formula Không học NC
                row = VTVector.ColumnIntToString(start_row);
                dataSheet.CopyPaste(ColHasColor, row_total, start_row, false);
                string totalClassFormula_0NC = string.Format("=SUM({0}22:{0}{1})", row, (22 + CountEducationLevel - 1).ToString());
                dataSheet.SetFormulaValue(row_total, start_row, totalClassFormula_0NC);
                start_row++;
                row = VTVector.ColumnIntToString(start_row);
                dataSheet.CopyPaste(ColHasColor, row_total, start_row, false);
                string totalPupilFormula_0NC = string.Format("=SUM({0}22:{0}{1})", row, (22 + CountEducationLevel - 1).ToString());
                dataSheet.SetFormulaValue(row_total, start_row, totalPupilFormula_0NC);
                #endregion
            }

            #region TẠO BẢNG CƠ CẤU GIÁO VIÊN THEO MÔN HỌC CẤP THPT NĂM HỌC
            //coppy excel from tempSheet to dataSheet
            IVTRange CoCauGiaoVien_Range = tempSheet.GetRange("A10", "D13");
            dataSheet.CopyPaste(CoCauGiaoVien_Range, row_index + 3, 1, false);
            //Lấy danh sách môn học: lstSubject = ProvinceSubjectBusiness.SearchByProvince(ProvinceID, dic)
            IDictionary<string, object> dicSubject = new Dictionary<string, object>();
            dicSubject["Year"] = Year;
            List<SubjectCat> lstSubject = ProvinceSubjectBusiness.SearchByProvince(ProvinceID, dicSubject).Select(o => o.SubjectCat).Distinct().ToList();

            //trong biên chế
            dataSheet.SetCellValue(row_index + 6, 2, sot.NumOfStaff);
            //Với mỗi môn học trong lstSubject:
            int colIndex_CoCauGV = 4;
            if (lstSubject != null && lstSubject.Count > 0)
            {
                foreach (var subject in lstSubject)
                {
                    dataSheet.CopyPaste(tempSheet.GetRange("D11", "D13"), row_index + 4, colIndex_CoCauGV, false);
                    //Tên môn học
                    dataSheet.SetCellValue(row_index + 5, colIndex_CoCauGV, subject.DisplayName);
                    StatisticSubjectOfTertiary statisticSubjectOfTertiary = iqStatisticSubjectOfTertiary.Where(o => o.SubjectID == subject.SubjectCatID).FirstOrDefault();
                    int? TotalTeacher = statisticSubjectOfTertiary != null ? statisticSubjectOfTertiary.TotalTeacher : null;
                    dataSheet.SetCellValue(row_index + 6, colIndex_CoCauGV++, TotalTeacher);
                }
                dataSheet.CopyPaste(tempSheet.GetRange("D11", "D13"), row_index + 4, colIndex_CoCauGV, false);
                dataSheet.SetCellValue(row_index + 5, colIndex_CoCauGV, "Môn khác dạy GDCD");
                StatisticSubjectOfTertiary statisticSubjectOfTertiary_MKD = iqStatisticSubjectOfTertiary.Where(o => o.SpecicalType == 1).FirstOrDefault();
                int? TotalTeacher_MKD = statisticSubjectOfTertiary_MKD != null ? statisticSubjectOfTertiary_MKD.TotalTeacher : null;
                dataSheet.SetCellValue(row_index + 6, colIndex_CoCauGV, TotalTeacher_MKD);
            }
            IVTRange TrinhDo_Range = tempSheet.GetRange("J16", "L18");
            dataSheet.CopyPaste(TrinhDo_Range, row_index + 4, colIndex_CoCauGV + 1, false);
            dataSheet.SetCellValue(row_index + 6, colIndex_CoCauGV + 1, sot.NumOfStandard);
            dataSheet.SetCellValue(row_index + 6, colIndex_CoCauGV + 2, sot.NumOfOverStandard);
            dataSheet.SetCellValue(row_index + 6, colIndex_CoCauGV + 3, sot.NumOfUnderStandard);
            //set formula cho TSGV
            string start_Char = VTVector.ColumnIntToString(colIndex_CoCauGV + 1);
            string end_Char = VTVector.ColumnIntToString(colIndex_CoCauGV + 3);
            string TSGV_Formula = string.Format("=SUM({0}{1}:{2}{1})", start_Char, (row_index + 6).ToString(), end_Char, (row_index + 6).ToString());
            dataSheet.SetFormulaValue(row_index + 6, 1, TSGV_Formula);

            dataSheet.MergeRow(row_index + 4, 4, lstSubject.Count + 4);
            dataSheet.SetCellValue(row_index + 4, 4, "Cơ cấu giáo viên theo môn học");

            #endregion
            //Người báo cáo
            IVTRange NguoiBaoCao_Range = tempSheet.GetRange("H24", "H24");
            dataSheet.CopyPaste(NguoiBaoCao_Range, row_index + 8, TotalCol > lstSubject.Count ? TotalCol - 4 : lstSubject.Count - 4, false);

            dataSheet.MergeRow(5, 1, TotalCol > lstSubject.Count ? TotalCol : lstSubject.Count);

            Dictionary<string, object> dicVarable = new Dictionary<string, object>();
            SupervisingDept supervisingDept = SupervisingDeptBusiness.Find(SupervisingDeptID);
            dicVarable["SupervisingDept"] = supervisingDept.SupervisingDeptName;
            dicVarable["AcademicYear"] = Year + " - " + (Year + 1).ToString();
            dicVarable["Year"] = Year;
            dataSheet.FillVariableValue(dicVarable);

            oBook.GetSheet(2).Delete();
            oBook.GetSheet(2).Delete();
            reportName = reportName + ".xls";
            return oBook.ToStream();
        }


        /// <summary>
        /// InsertOrUpdate
        /// </summary>
        /// <param name="SchoolID">The school ID.</param>
        /// <param name="AcademicYearID">The academic year ID.</param>
        /// <param name="sot">The sot.</param>
        /// <param name="lstStatisticSubCommittee">The LST statistic sub committee.</param>
        /// <param name="lstStatisticSubject">The LST statistic subject.</param>
        ///<author>hath</author>
        /// <date>1/18/2013</date>
        public void InsertOrUpdate(int SchoolID, int AcademicYearID, StatisticOfTertiary sot, List<StatisticSubcommitteeOfTertiary> lstStatisticSubCommittee, List<StatisticSubjectOfTertiary> lstStatisticSubject)
        {
            //Cập nhật SchoolID, AcademicYearID vào các đối tượng sot.
            //Tìm kiếm trong bảng StatisticOfTertiary theo SchoolID, AcademicYearID
            //Nếu tìm thấy thì cập nhật các giá trị theo đối tượng sot rồi Update.
            //Nếu không thì Insert đối tượng sot vào bảng StatisticOfTertiary
            //Gọi hàm StatisticSubCommitteeOfTertiaryBusiness.InsertOrUpdate(SchoolID, AcademicYearID, lstStatisticSubCommittee)
            //Gọi hàm StatisticSubjectOfTertiaryBusiness.InsertOrUpdate(SchoolID, AcademicYearID, lstStatisticSubject)
            sot.SchoolID = SchoolID;
            sot.AcademicYearID = AcademicYearID;
            IQueryable<StatisticOfTertiary> lsStatisticOfTertiary = StatisticOfTertiaryRepository.All.Where(o => o.SchoolID == sot.SchoolID && o.AcademicYearID == sot.AcademicYearID);
            List<StatisticOfTertiary> lstStatisticOfTertiary = new List<StatisticOfTertiary>();
            if (lsStatisticOfTertiary.Count() > 0)
            {
                lstStatisticOfTertiary = lsStatisticOfTertiary.ToList();
                foreach (StatisticOfTertiary soty in lstStatisticOfTertiary)
                {
                    soty.ComputerRoom = sot.ComputerRoom;
                    soty.HasDatabase = sot.HasDatabase;
                    soty.HasWebsite = sot.HasWebsite;
                    soty.LearningRoom = sot.LearningRoom;
                    soty.NumberOfComputer = sot.NumberOfComputer;
                    soty.NumberOfComputerInternetConnected = sot.NumberOfComputerInternetConnected;
                    soty.NumOfOverStandard = sot.NumOfOverStandard;
                    soty.NumOfStaff = sot.NumOfStaff;
                    soty.NumOfStandard = sot.NumOfStandard;
                    soty.NumOfUnderStandard = sot.NumOfUnderStandard;
                    soty.PupilFromSecondary = sot.PupilFromSecondary;
                    soty.PupilOfOther = sot.PupilOfOther;
                    soty.SentDate = sot.SentDate;
                    base.Update(soty);
                }
            }
            else
            {
                base.Insert(sot);
            }
            StatisticSubcommitteeOfTertiaryBusiness.InsertOrUpdate(SchoolID, AcademicYearID, lstStatisticSubCommittee);
            StatisticSubjectOfTertiaryBusiness.InsertOrUpdate(SchoolID, AcademicYearID, lstStatisticSubject);
            StatisticSubcommitteeOfTertiaryBusiness.Save();
            StatisticSubjectOfTertiaryBusiness.Save();
        }
    }
}