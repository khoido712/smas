﻿/**
 * The Viettel License
 *
 * Copyright 2012 Viettel Telecom. All rights reserved.
 * VIETTEL PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */

/** 
* @author  
* @version $Revision: $
*/

using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Linq.Expressions;
using System.Web;
using log4net;

using SMAS.DAL.Repository;
using SMAS.DAL.IRepository;
using SMAS.Business.IBusiness;
using SMAS.Models.Models;
using SMAS.Business.Common;

namespace SMAS.Business.Business
{
    public partial class LockTrainingBusiness
    {
        /// <summary>
        /// Tim kiem khoa diem danh, vi pham theo truong hoc
        /// </summary>
        /// <param name="SchoolID"></param>
        /// <param name="dic"></param>
        /// <returns></returns>
        public IQueryable<LockTraining> SearchBySchool(int SchoolID, IDictionary<string, object> dic = null)
        {
            if (dic == null)
            {
                dic = new Dictionary<string, object>();
            }
            dic["SchoolID"] = SchoolID;
            return this.Search(dic);
        }

        /// <summary>
        /// Tim kiem khoa diem danh, vi pham theo lop hoc
        /// </summary>
        /// <param name="ClassID"></param>
        /// <param name="dic"></param>
        /// <returns></returns>
        public IQueryable<LockTraining> SearchByClass(int ClassID, IDictionary<string, object> dic = null)
        {
            if (dic == null)
            {
                dic = new Dictionary<string, object>();
            }
            dic["ClassID"] = ClassID;
            return this.Search(dic);
        }

        private IQueryable<LockTraining> Search(IDictionary<string, object> dic = null)
        {
            IQueryable<LockTraining> iqLockTraining = this.repository.All;
            int SchoolID = Utils.GetInt(dic, "SchoolID");
            int AcademicYearID = Utils.GetInt(dic, "AcademicYearID");
            int EducationLevelID = Utils.GetInt(dic, "EducationLevelID");
            int ClassID = Utils.GetInt(dic, "ClassID");
            bool? IsViolation = Utils.GetNullableBool(dic, "IsViolation");
            bool? IsAbsence = Utils.GetNullableBool(dic, "IsAbsence");
            if (ClassID > 0)
            {
                iqLockTraining = from lt in iqLockTraining
                                 join cp in ClassProfileBusiness.All on lt.ClassID equals cp.ClassProfileID
                                 where (!cp.IsActive.HasValue || (cp.IsActive.HasValue && cp.IsActive.Value))
                                 && lt.ClassID == ClassID
                                 select lt;//iqLockTraining.Where(o => o.ClassID == ClassID);
            }
            if (SchoolID > 0)
            {
                iqLockTraining = iqLockTraining.Where(o => o.SchoolID == SchoolID);
            }
            if (AcademicYearID > 0)
            {
                iqLockTraining = iqLockTraining.Where(o => o.AcademicYearID == AcademicYearID);
            }
            if (EducationLevelID > 0)
            {
                iqLockTraining = iqLockTraining.Where(o => o.ClassProfile.EducationLevelID == EducationLevelID);
            }
            if (IsViolation.HasValue)
            {
                iqLockTraining = iqLockTraining.Where(o => o.TrainingType == GlobalConstants.LOCK_TRAINING_VIOLATION || o.TrainingType == GlobalConstants.LOCK_TRAINING_ABSENCE_VIOLATION);
            }
            if (IsAbsence.HasValue)
            {
                iqLockTraining = iqLockTraining.Where(o => o.TrainingType == GlobalConstants.LOCK_TRAINING_ABSENCE || o.TrainingType == GlobalConstants.LOCK_TRAINING_ABSENCE_VIOLATION);
            }
            return iqLockTraining;
        }
        
        /// <summary>
        /// Quanglm - Cap nhat vao CSDL
        /// </summary>
        /// <param name="SchoolID"></param>
        /// <param name="AcademicYearID"></param>
        /// <param name="listLockTraining"></param>
        public void UpdateOrInSert(int SchoolID, int AcademicYearID, List<LockTraining> listLockTraining)
        {
            #region Kiem tra du lieu dau vao
            //Kiem tra thong tin truong va nam hoc trong danh sach phai thuoc truong va nam
            if (listLockTraining.Any(o => o.SchoolID != SchoolID || o.AcademicYearID != AcademicYearID))
            {
                throw new BusinessException("Common_Validate_NotCompatible");
            }
            // Neu lop hoc không thuoc truong va nam hoc
            List<int> listClassID = listLockTraining.Select(o => o.ClassID).Distinct().ToList();
            int countClass = ClassProfileBusiness.SearchBySchool(SchoolID, new Dictionary<string, object>(){
                {"AcademicYearID", AcademicYearID}
            }).Where(o => listClassID.Contains(o.ClassProfileID)).Count();
            if (countClass != listClassID.Count)
            {
                throw new BusinessException("Common_Validate_NotCompatible");
            }
            // Luu du lieu
            foreach (LockTraining item in listLockTraining)
            {
                if (item.LockTrainingID != 0)
                {
                    this.Update(item);
                }
                else
                {
                    this.Insert(item);
                }
            }
            #endregion
        }
    }
}
