﻿/**
 * The Viettel License
 *
 * Copyright 2012 Viettel Telecom. All rights reserved.
 * VIETTEL PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */

/** 
* @author  
* @version $Revision: $
*/

using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Linq.Expressions;
using System.Web;
using log4net;

using SMAS.DAL.Repository;
using SMAS.DAL.IRepository;
using SMAS.Business.IBusiness;
using SMAS.Models.Models;
using SMAS.Business.Common;
using SMAS.Business.BusinessObject;

namespace SMAS.Business.Business
{
    public partial class DentalTestBusiness
    {
        private const int NUMDENTALFILLING_MIN = 0;
        private const int NUMDENTALFILLING_MAX = 99;
        /// <summary>
        /// Thêm mới thông tin khám sức khỏe – Răng hàm mặt
        /// author: namdv3
        /// </summary>
        /// <param name="entity"></param>
        /// <param name="entity1"></param>
        public void Insert(DentalTest entity, MonitoringBook entity1)
        {
            #region validation before insert
            ValidationMetadata.ValidateObject(entity);
            //0 =< NumDentalFilling < 99
            if (entity.NumDentalFilling < NUMDENTALFILLING_MIN || entity.NumDentalFilling >= NUMDENTALFILLING_MAX)
            {
                throw new BusinessException("DentalTest_Validation_NumDentalFilling_MinMax");
            }
            //if(NumPreventiveDentalFilling != null) 0 =< NumPreventiveDentalFilling < = 99
            if (entity.NumPreventiveDentalFilling.HasValue)
            {
                if (entity.NumPreventiveDentalFilling < NUMDENTALFILLING_MIN || entity.NumPreventiveDentalFilling > NUMDENTALFILLING_MAX)
                {
                    throw new BusinessException("DentalTest_Validation_NumPreventiveDentalFilling_MinMax");
                }
            }
            //if(NumTeethExtracted != null) 0 =< NumTeethExtracted <= 99
            if (entity.NumTeethExtracted.HasValue)
            {
                if (entity.NumTeethExtracted < NUMDENTALFILLING_MIN || entity.NumTeethExtracted > NUMDENTALFILLING_MAX)
                {
                    throw new BusinessException("DentalTest_Validation_NumTeethExtracted_MinMax");
                }
            }
            #endregion

            #region business insert
            /*Thực hiện kiểm tra với PupilID = entity1.PupilID, HealthPeriodID = entity1.HealthPeriodID,
             * SchoolID = entity1.SchoolID, AcademicYearID = entity1.AcademicYearID*/
            int PupilID = entity1.PupilID;
            int? HealthPeriodID = entity1.HealthPeriodID;
            int SchoolID = entity1.SchoolID;
            int AcademicYearID = entity1.AcademicYearID;

            /* Kiểm tra sự tồn tại bằng cách gọi hàm MonitoringBookBusiness.SearchBySchool(SchoolID, Dictionary) 
             * với Dictionary[“PupilID”] = PupilID, Dictionary[“HealthPeriodID”] = HealthPeriodID,
             * Dictionary[“AcademicYearID”] = AcademicYearID. Kết quả thu được lstMonitoringBook.
             */
            IDictionary<string, object> dic = new Dictionary<string, object>();
            dic["PupilID"] = PupilID;
            dic["HealthPeriodID"] = HealthPeriodID;
            dic["AcademicYearID"] = AcademicYearID;
            IQueryable<MonitoringBook> lstMonitoringBook = MonitoringBookBusiness.SearchBySchool(SchoolID, dic);
            //Nếu lstMonitoringBook = null:
            if (lstMonitoringBook == null || lstMonitoringBook.Count() <= 0)
            {
                //B1. Thực hiện insert dữ liệu vào bảng MonitoringBook bằng cách gọi hàm MonitoringBookBusiness.Insert(entity1)
                MonitoringBookBusiness.Insert(entity1);
                //B2. Thực hiện Insert dữ liệu vào bảng DentalTest với bổ sung MonitoringBookID = entity1.MonitoringBookID
                entity.MonitoringBookID = entity1.MonitoringBookID;
                this.Insert(entity);
            }
            //Nếu lstMonitoringBook != null
            else
            {
                //B1. Thực hiện Insert dữ liệu vào bảng DentalTest với MonitoringBookID = lstMonitoringBook.MonitoringBookID.
                entity.MonitoringBookID = lstMonitoringBook.FirstOrDefault().MonitoringBookID;
                DentalTest dentalTest = this.All.Where(o => o.MonitoringBookID == entity.MonitoringBookID && o.IsActive == true).FirstOrDefault();
                bool exitsDentalTest = dentalTest != null ? true : false;
                if (!exitsDentalTest)
                {
                    this.Insert(entity);
                }
                else
                {
                    throw new BusinessException("DentalTest_Validation_ExitsDentalTest");
                }
            }
            #endregion
        }

        /// <summary>
        /// Cập nhật thông tin khám sức khỏe – Răng hàm mặt
        /// author: namdv3
        /// </summary>
        /// <param name="entity"></param>
        /// <param name="entity1"></param>
        public void Update(DentalTest entity, MonitoringBook entity1)
        {
            #region validation before insert
            ValidationMetadata.ValidateObject(entity);
            //0 =< NumDentalFilling < 99
            if (entity.NumDentalFilling < NUMDENTALFILLING_MIN || entity.NumDentalFilling >= NUMDENTALFILLING_MAX)
            {
                throw new BusinessException("DentalTest_Validation_NumDentalFilling_MinMax");
            }
            //if(NumPreventiveDentalFilling != null) 0 =< NumPreventiveDentalFilling < = 99
            if (entity.NumPreventiveDentalFilling.HasValue)
            {
                if (entity.NumPreventiveDentalFilling < NUMDENTALFILLING_MIN || entity.NumPreventiveDentalFilling > NUMDENTALFILLING_MAX)
                {
                    throw new BusinessException("DentalTest_Validation_NumPreventiveDentalFilling_MinMax");
                }
            }
            //if(NumTeethExtracted != null) 0 =< NumTeethExtracted <= 99
            if (entity.NumTeethExtracted.HasValue)
            {
                if (entity.NumTeethExtracted < NUMDENTALFILLING_MIN || entity.NumTeethExtracted > NUMDENTALFILLING_MAX)
                {
                    throw new BusinessException("DentalTest_Validation_NumTeethExtracted_MinMax");
                }
            }
            #endregion
            //Thực hiện Update dữ liệu vào bảng MonitoringBook bằng cách gọi hàm MonitoringBookBusiness.Update(entity1)
            MonitoringBookBusiness.Update(entity1);
            // Thực hiện Update dữ liệu vào bảng DentalTest 
            this.Update(entity);
        }

        /// <summary>
        /// Xóa thông tin khám sức khỏe – Răng hàm mặt
        /// </summary>
        /// <param name="DentalTestID"></param>
        /// <param name="SchoolID"></param>
        public void Delete(int DentalTestID, int SchoolID)
        {
            #region validation before delete
            //DentalTestID => MonitoringBookID
            int MonitoringBookID = this.Find(DentalTestID).MonitoringBookID;
            //MonitoringBookID, SchoolID: not compatible(MonitoringBook)
            IDictionary<string, object> SearchInfo = new Dictionary<string, object>();
            SearchInfo["MonitoringBookID"] = MonitoringBookID;
            SearchInfo["SchoolID"] = SchoolID;
            bool compatible = new MonitoringBookRepository(this.context).ExistsRow(GlobalConstants.HEALTH_SCHEMA, "MonitoringBook", SearchInfo);
            if (!compatible)
            {
                throw new BusinessException("Common_Validate_NotCompatible");
            }
            #endregion

            DentalTest dentalTest = this.Find(DentalTestID);
            if (dentalTest != null)
            {
                //Thực hiện chuyển  IsActive=false, ModifiedDate=Sysdate với bản ghi tương ứng trong bảng DentalTest
                dentalTest.IsActive = false;
                dentalTest.ModifiedDate = DateTime.Now;
                this.Update(dentalTest);
            }
            else
            {
                //Nếu không tồn tại bản ghi tương ứng DentalTestID thì return luôn
                return;
            }
        }
        /// <summary>
        /// Tìm kiếm thông tin khám sức khỏe –  Răng hàm mặt
        /// author: namdv3
        /// </summary>
        /// <param name="SearchInfo"></param>
        /// <returns></returns>
        private IQueryable<DentalTest> Search(IDictionary<string, object> SearchInfo)
        {
            int DentalTestID = Utils.GetInt(SearchInfo, "DentalTestID");
            int NumTeethExtracted = Utils.GetInt(SearchInfo, "NumTeethExtracted", -1);
            string DescriptionTeethExtracted = Utils.GetString(SearchInfo, "DescriptionTeethExtracted");
            int NumDentalFilling = Utils.GetInt(SearchInfo, "NumDentalFilling", -1);
            //string DescriptionTeethExtracted = Utils.GetString(SearchInfo, "DescriptionTeethExtracted");
            int NumPreventiveDentalFilling = Utils.GetInt(SearchInfo, "NumPreventiveDentalFilling", -1);
            string DescriptionPreventiveDentalFilling = Utils.GetString(SearchInfo, "DescriptionPreventiveDentalFilling");
            bool IsScraptTeeth = Utils.GetBool(SearchInfo, "IsScraptTeeth");
            bool IsTreatment = Utils.GetBool(SearchInfo, "IsTreatment");
            bool ToothDecay = Utils.GetBool(SearchInfo, "ToothDecay");
            bool IsActive = true;
            int MonitoringBookID = Utils.GetInt(SearchInfo, "MonitoringBookID");
            int SchoolID = Utils.GetInt(SearchInfo, "SchoolID");
            int AcademicYearID = Utils.GetInt(SearchInfo, "AcademicYearID");
            int PupilID = Utils.GetInt(SearchInfo, "PupilID");
            int ClassID = Utils.GetInt(SearchInfo, "ClassID");
            int HealthPeriodID = Utils.GetInt(SearchInfo, "HealthPeriodID");
            string UnitName = Utils.GetString(SearchInfo, "UnitName");
            DateTime? MonitoringDate = Utils.GetDateTime(SearchInfo, "MonitoringDate");
            string FullName = Utils.GetString(SearchInfo, "FullName");
            int Grade = Utils.GetInt(SearchInfo, "EducationGrade");
            bool Other = Utils.GetBool(SearchInfo, "Other");
            int EducationLevelID = Utils.GetInt(SearchInfo, "EducationLevelID");

            /*Thực hiện tìm kiếm trong bảng
             * DentalTest dt  join MonitoringBook mb (Theo MonitoringBookID)
             * join PupilProfile pp (theo PupilID) 
             * theo các thông tin trong SearchInfo:*/
            var query = DentalTestRepository.All;
            if (Grade != 0)
            {
                query = from ps in DentalTestRepository.All
                        join mb in MonitoringBookBusiness.All on ps.MonitoringBookID equals mb.MonitoringBookID
                        join ed in EducationLevelBusiness.All on mb.EducationLevelID equals ed.EducationLevelID
                        where ed.Grade == Grade
                        select ps;
            }
            if (EducationLevelID > 0)
            {
                query = query.Where(p => p.MonitoringBook.EducationLevelID == EducationLevelID);
            }
            if (Other == true)
            {
                query = query.Where(o =>(!string.IsNullOrEmpty(o.Other)));
            }
            if (ToothDecay == true)
            {
                query = query.Where(o => o.NumDentalFilling > 0);
            }
            if (DentalTestID > 0)
            {
                query = query.Where(o => o.DentalTestID == DentalTestID);
            }
            if (NumTeethExtracted != -1)
            {
                query = query.Where(o => o.NumTeethExtracted == NumTeethExtracted);
            }
            if (DescriptionTeethExtracted != "")
            {
                query = query.Where(o => o.DescriptionTeethExtracted.Contains(DescriptionTeethExtracted));
            }
            if (NumDentalFilling != -1)
            {
                query = query.Where(o => o.NumDentalFilling == NumDentalFilling);
            }
            if (NumPreventiveDentalFilling != -1)
            {
                query = query.Where(o => o.NumPreventiveDentalFilling == NumPreventiveDentalFilling);
            }
            if (DescriptionPreventiveDentalFilling != "")
            {
                query = query.Where(o => o.DescriptionPreventiveDentalFilling.Contains(DescriptionPreventiveDentalFilling));
            }
            if (IsScraptTeeth)
            {
                query = query.Where(o => o.IsScraptTeeth == IsScraptTeeth);
            }
            if (IsTreatment)
            {
                query = query.Where(o => o.IsTreatment == IsTreatment);
            }

            //if (IsActive)
            //{
            //    query = query.Where(o => o.IsActive == IsActive);
            //}
            if (MonitoringBookID > 0)
            {
                query = query.Where(o => o.MonitoringBookID == MonitoringBookID);
            }
            if (SchoolID > 0)
            {
                query = query.Where(o => o.MonitoringBook.SchoolID == SchoolID);
            }
            if (AcademicYearID > 0)
            {
                query = query.Where(o => o.MonitoringBook.AcademicYearID == AcademicYearID);
            }
            if (PupilID > 0)
            {
                query = query.Where(o => o.MonitoringBook.PupilID == PupilID);
            }
            if (ClassID > 0)
            {
                query = query.Where(o => o.MonitoringBook.ClassID == ClassID);
            }
            if (HealthPeriodID > 0)
            {
                query = query.Where(o => o.MonitoringBook.HealthPeriodID == HealthPeriodID);
            }
            if (UnitName != "")
            {
                query = query.Where(o => o.MonitoringBook.UnitName.Contains(UnitName));
            }
            if (MonitoringDate.HasValue)
            {
                query = query.Where(o => o.MonitoringBook.MonitoringDate == MonitoringDate);
            }
            if (FullName != "")
            {
                query = query.Where(o => o.MonitoringBook.PupilProfile.FullName.ToLower().Contains(FullName.ToLower()));
            }
         
            return query;
        }
        /// <summary>
        /// Tìm kiếm thông tin khám sức khỏe –  Răng hàm mặt
        /// author: namdv3
        /// </summary>
        /// <param name="SchoolID"></param>
        /// <param name="SearchInfo"></param>
        /// <returns></returns>
        public IQueryable<DentalTest> SearchBySchool(int SchoolID, IDictionary<string, object> SearchInfo)
        {
            //Nếu  SchoolID = 0 -> trả về NULL
            if (SchoolID == 0)
            {
                return null;
            }
            else
            {
                SearchInfo["SchoolID"] = SchoolID;
                return Search(SearchInfo);
            }
        }

        /// <summary>
        /// Tìm kiếm thông tin khám sức khỏe theo học sinh – Răng hàm mặt
        /// author: namdv3
        /// </summary>
        /// <param name="AcademicYearID">Năm học</param>
        /// <param name="SchoolID">Trường học</param>
        /// <param name="PupilID">Học sinh</param>
        /// <param name="ClassID">Lớp học</param>
        /// <returns></returns>
        public IQueryable<DentalTestBO> SearchByPupil(int AcademicYearID, int SchoolID, int PupilID, int ClassID)
        {
            //B1: Khởi tạo IQueryable<MonitoringBook> lstMonitoringBook bằng cách gọi hàm MonitoringBookBusiness.SearchByPupil(SchoolID, AcademicYearID, PupilID, ClassID)
            IQueryable<MonitoringBook> lstMonitoringBook = MonitoringBookBusiness.SearchByPupil(SchoolID, AcademicYearID, PupilID, ClassID);
            //B2: Thực hiện việc tìm kiếm lstMonitoringBook mb left DentalTest dt on mb.MonitoringBookID = dt.MonitoringBookID. Kêt quả thu được IQueryable<DentalTestBO>
            var query = from mb in lstMonitoringBook
                        join dt in DentalTestRepository.All.Where(o => o.IsActive == true) on mb.MonitoringBookID equals dt.MonitoringBookID
                        //into g1
                        //from j2 in g1.DefaultIfEmpty()
                        select new DentalTestBO
                        {
                            MonitoringBookID = mb.MonitoringBookID,
                            MonitoringBookDate = mb.MonitoringDate,
                            DentalTestID = dt.DentalTestID,
                            NumTeethExtracted = dt.NumTeethExtracted,
                            DescriptionTeethExtracted = dt.DescriptionTeethExtracted,
                            NumDentalFilling = dt.NumDentalFilling,
                            DescriptionDentalFilling = dt.DescriptionDentalFilling,
                            NumPreventiveDentalFilling = dt.NumPreventiveDentalFilling,
                            DescriptionPreventiveDentalFilling = dt.DescriptionPreventiveDentalFilling,
                            IsScraptTeeth = dt.IsScraptTeeth,
                            IsTreatment = dt.IsTreatment,
                            Other = dt.Other,
                            CreatedDate = dt.CreatedDate,
                            IsActive = dt.IsActive,
                            ModifiedDate = dt.ModifiedDate
                        };
            return query;
        }

        // Thuyen - 05/10/2017
        public List<DentalTest> ListDentalTestByMonitoringBookId(List<int> lstMoniBookID)
        {
            return DentalTestBusiness.All.Where(x => lstMoniBookID.Contains(x.MonitoringBookID)).ToList();
        }
    }
}