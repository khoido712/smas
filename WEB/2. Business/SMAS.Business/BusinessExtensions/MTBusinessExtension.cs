/**
 * The Viettel License
 *
 * Copyright 2012 Viettel Telecom. All rights reserved.
 * VIETTEL PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */

/** 
* @author  
* @version $Revision: $
*/

using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Linq.Expressions;
using System.Web;
using log4net;

using SMAS.DAL.Repository;
using SMAS.DAL.IRepository;
using SMAS.Business.IBusiness;
using SMAS.Models.Models;
using SMAS.Business.Common;
using SMAS.Business.BusinessObject;
using System.Transactions;
using SMAS.Business.Common.Extension;

namespace SMAS.Business.Business
{ 
    public partial class MTBusiness
    {  
        private const string FORMAT_0_1 = "{0}{1}";

        private const string SMAS_TEXT1 = "SMAS ";

        private const string SMAS_TEXT2 = "SMAS: ";

        #region gui sms cho nguoi dung; ham gui khong insert vao bang communication chi gui tren dien thoai
        /// <summary>
        /// gui sms cho nguoi dung; ham gui khong insert vao bang communication chi gui tren dien thoai
        /// </summary>
        /// <param name="dic"></param>
        public void SendSMS(Dictionary<string, object> dic)
        {
            string Mobile = Utils.GetString(dic, "Mobile");
            string Content = Utils.GetString(dic, "Content");
            int? ReceiveType = Utils.GetNullableInt(dic,"ReceiveType");
            int SchoolID = Utils.GetInt(dic, "SchoolID");
            int? Year = Utils.GetNullableInt(dic, "Year");
            int partitionSMSHistory = UtilsBusiness.GetPartionId(SchoolID);
            // check brand Name
            if (Content.ToUpper().StartsWith(SMAS_TEXT2))
                Content = string.Format(FORMAT_0_1, SMAS_TEXT2, Content.Substring(SMAS_TEXT2.Length, Content.Length - SMAS_TEXT2.Length));
            else if (Content.ToUpper().StartsWith(SMAS_TEXT1))
                Content = string.Format(FORMAT_0_1, SMAS_TEXT1, Content.Substring(SMAS_TEXT1.Length, Content.Length - SMAS_TEXT1.Length));
            else Content = string.Format(FORMAT_0_1, SMAS_TEXT1, Content);

            int TypeID = GlobalConstantsEdu.COMMON_HISTORYSMS_TO_REGISTER_ACCOUNT_SPARENT;
            if (dic.ContainsKey("TypeID")) TypeID = Utils.GetInt(dic, "TypeID");

            Content = Content.StripVNSign();
            DateTime dateTimeNow = DateTime.Now;
            Guid rawId = Guid.NewGuid();
            SMS_HISTORY historySMS = new SMS_HISTORY();
            historySMS.MOBILE = Mobile;
            historySMS.IS_INTERNAL = historySMS.MOBILE.CheckMobileNumberVT();
            historySMS.RECEIVER_NAME = " ";
            historySMS.SMS_TYPE_ID = TypeID;
            if (ReceiveType.HasValue) historySMS.RECEIVE_TYPE = ReceiveType.Value;
            else historySMS.RECEIVE_TYPE = GlobalConstantsEdu.COMMON_HISTORY_RECEIVER_PARENT_ID;

            historySMS.CREATE_DATE = dateTimeNow;
            historySMS.SCHOOL_ID = SchoolID;
            historySMS.PARTITION_ID = partitionSMSHistory;
            if (Year.HasValue) historySMS.YEAR = Year.Value;
            else historySMS.YEAR = dateTimeNow.Year;
            historySMS.SMS_CONTENT = Content;
            historySMS.SHORT_CONTENT = Content;
            historySMS.SENDER_NAME = " ";
            historySMS.SMS_CNT = Content.countNumSMS();
            historySMS.IS_SMS = true;
            historySMS.SCHOOL_USERNAME = " ";
            historySMS.HISTORY_RAW_ID = rawId;
            this.SMSHistoryBusiness.Insert(historySMS);

            if (Mobile.CheckMobileNumber())
            {
                SMS_MT objMT = new SMS_MT();
                objMT.USER_ID = Mobile;
                objMT.SMS_CONTENT = Content;
                objMT.TIME_SEND_REQUEST = dateTimeNow;
                objMT.TIME_SENT = dateTimeNow;
                objMT.HISTORY_RAW_ID = rawId;
                objMT.REQUEST_ID = Guid.NewGuid();
                this.Insert(objMT);
            }

            this.Save();
        }
        #endregion

    }
}