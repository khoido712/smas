﻿/**
 * The Viettel License
 *
 * Copyright 2012 Viettel Telecom. All rights reserved.
 * VIETTEL PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */

/** 
* @author  
* @version $Revision: $
*/

using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Linq.Expressions;
using System.Web;
using log4net;

using SMAS.DAL.Repository;
using SMAS.DAL.IRepository;
using SMAS.Business.IBusiness;
using SMAS.Models.Models;
using SMAS.Business.BusinessObject;
using SMAS.Business.Common;
using System.IO;
using SMAS.VTUtils.Excel.Export;

namespace SMAS.Business.Business
{
    public partial class ConductStatisticBusiness
    {
        #region Cu
        public void SendAll(List<ConductStatisticsBO> ListData)
        {
            ConductStatistic objConduct = null;
            foreach (ConductStatisticsBO cs in ListData)
            {
                objConduct = new ConductStatistic();
                objConduct.ProcessedDate = cs.ProcessedDate;
                objConduct.ReportCode = cs.ReportCode;
                objConduct.SchoolID = cs.SchoolID;
                objConduct.AcademicYearID = cs.AcademicYearID;
                objConduct.Year = cs.Year;
                objConduct.Semester = cs.Semester;
                objConduct.EducationLevelID = cs.EducationLevelID;
                objConduct.SubCommitteeID = cs.SubCommitteeID;
                objConduct.SentToSupervisor = true;
                objConduct.SentDate = DateTime.Now;
                objConduct.ConductLevel01 = cs.ConductLevel01;
                objConduct.ConductLevel02 = cs.ConductLevel02;
                objConduct.ConductLevel03 = cs.ConductLevel03;
                objConduct.ConductLevel04 = cs.ConductLevel04;
                objConduct.PupilTotal = cs.PupilTotal;
                objConduct.AppliedLevel = cs.AppliedLevel;
                Insert(cs);
            }
        }

        /// <summary>
        /// Tìm kiếm thống kê hạnh kiểm cấp 2, 3 với account Sở: Thống kê theo khối
        /// Tamhm1
        /// 03/12/2012
        /// </summary>
        /// <param name="SearchInfo"></param>
        /// <returns></returns>
        public List<ConductStatisticsOfProvinceGroupByLevel23BO> SearchByProvinceGroupByLevel23(IDictionary<string, object> SearchInfo)
        {
            int? districtID = Utils.GetInt(SearchInfo, "DistrictID");
            int? provinceID = Utils.GetInt(SearchInfo, "ProvinceID");
            string reportCode = "";
            int? year = Utils.GetInt(SearchInfo, "Year");
            int? semester = Utils.GetInt(SearchInfo, "Semester");
            bool sentToSupervisor = Utils.GetBool(SearchInfo, "SentToSupervisor");
            int? educationLevelID = Utils.GetInt(SearchInfo, "EducationLevelID");
            int appliedLevel = Utils.GetInt(SearchInfo, "AppliedLevel");
            int? supervisingDeptID = Utils.GetInt(SearchInfo, "SupervisingDeptID");
            if (supervisingDeptID.HasValue && supervisingDeptID > 0)
            {
                SupervisingDept su = SupervisingDeptBusiness.Find(supervisingDeptID);
                if (su.HierachyLevel == SystemParamsInFile.EDUCATION_HIERACHY_LEVEL_DISTRICT_OFFICE_DEPARTMENT || su.HierachyLevel == SystemParamsInFile.EDUCATION_HIERACHY_LEVEL_PROVINCE_OFFICE_DEPARTMENT)
                {
                    supervisingDeptID = su.ParentID.Value;
                }
            }
            if (appliedLevel == 2)
            {
                reportCode = SystemParamsInFile.THONGKEHANHKIEMCAP2;
            }
            else if (appliedLevel == 3)
            {
                reportCode = SystemParamsInFile.THONGKEHANHKIEMTHEOBANCAP3;
            }

            int grade = Utils.GradeToBinary(appliedLevel);
            List<ConductStatistic> lstConductStatistic = new List<ConductStatistic>();
            List<EducationLevel> lstEducationLevel = EducationLevelBusiness.GetByGrade(SystemParamsInFile.APPLIED_LEVEL_SECONDARY).ToList();
            IQueryable<ConductStatistic> lstConductStatisticReposity = ConductStatisticRepository.All.Where(p => p.ReportCode == reportCode && p.Year == year && p.Semester == semester && p.SentToSupervisor == true);
            lstConductStatistic = (from c in lstConductStatisticReposity
                                   join s in SchoolProfileRepository.All on c.SchoolID equals s.SchoolProfileID
                                   join sd in SupervisingDeptBusiness.All on s.SupervisingDeptID equals sd.SupervisingDeptID
                                   where (sd.SupervisingDeptID == supervisingDeptID || sd.ParentID == supervisingDeptID) && s.IsActive == true && (s.EducationGrade & grade) != 0
                                   && (c.ProcessedDate == (from c1 in lstConductStatisticReposity where c.SchoolID == c1.SchoolID && c.EducationLevelID == c1.EducationLevelID select c1.ProcessedDate).Max())
                                   select c).ToList<ConductStatistic>();

            //lstConductStatistic = (from c in ConductStatisticRepository.All.ToList()
            //                       join s in SchoolProfileRepository.All.ToList() on c.SchoolID equals s.SchoolProfileID
            //                       where (s.DistrictID == districtID || districtID == 0)
            //                       && s.ProvinceID == provinceID
            //                       && c.ReportCode == reportCode
            //                       && c.Year == year
            //                       && c.Semester == semester
            //                       && c.SentToSupervisor == sentToSupervisor
            //                       && (s.EducationGrade & grade) != 0
            //                       && (c.SentDate == (from c1 in ConductStatisticRepository.All.ToList() where c.SchoolID == c1.SchoolID select c1.SentDate).Max())
            //                       select c).ToList<ConductStatistic>();

            List<ConductStatisticsOfProvinceGroupByLevel23BO> lstConductStatisticsOfProvince23;
            var query = from j in
                            (from el in lstEducationLevel
                             join c in lstConductStatistic on el.EducationLevelID equals c.EducationLevelID into g
                             from jn in g.DefaultIfEmpty()
                             select new
                             {
                                 EducationLevelID = el.EducationLevelID,
                                 EducationLevelName = el.Resolution,
                                 ConductLevel01 = jn == null ? 0 : jn.ConductLevel01,
                                 ConductLevel02 = jn == null ? 0 : jn.ConductLevel02,
                                 ConductLevel03 = jn == null ? 0 : jn.ConductLevel03,
                                 ConductLevel04 = jn == null ? 0 : jn.ConductLevel04,
                                 PupilTotal = jn == null ? 0 : jn.PupilTotal
                             })
                        group j by new { j.EducationLevelID, j.EducationLevelName } into g2
                        select new ConductStatisticsOfProvinceGroupByLevel23BO
                        {
                            EducationLevelID = g2.Key.EducationLevelID,
                            EducationLevelName = g2.Key.EducationLevelName,
                            TotalGood = g2.Sum(u => u.ConductLevel01),
                            TotalFair = g2.Sum(u => u.ConductLevel02),
                            TotalNormal = g2.Sum(u => u.ConductLevel03),
                            TotalWeak = g2.Sum(u => u.ConductLevel04),
                            TotalPupil = g2.Sum(u => u.PupilTotal),
                            TotalAboveAverage = g2.Sum(u => u.ConductLevel01) + g2.Sum(u => u.ConductLevel02) + g2.Sum(u => u.ConductLevel03),
                            PercentGood = Math.Round(g2.Sum(u => u.PupilTotal) == 0 ? 0 : (100 * (double)g2.Sum(u => u.ConductLevel01) / (double)(g2.Sum(u => u.PupilTotal))), 2),
                            PercentFair = Math.Round(g2.Sum(u => u.PupilTotal) == 0 ? 0 : (100 * (double)g2.Sum(u => u.ConductLevel02) / (double)(g2.Sum(u => u.PupilTotal))), 2),
                            PercentNormal = Math.Round(g2.Sum(u => u.PupilTotal) == 0 ? 0 : (100 * (double)g2.Sum(u => u.ConductLevel03) / (double)(g2.Sum(u => u.PupilTotal))), 2),
                            PercentWeak = Math.Round(g2.Sum(u => u.PupilTotal) == 0 ? 0 : (100 * (double)g2.Sum(u => u.ConductLevel04) / (double)(g2.Sum(u => u.PupilTotal))), 2),
                            PercentAboveAverage = Math.Round(g2.Sum(u => u.PupilTotal) == 0 ? 0 : (100 * (double)(g2.Sum(u => u.ConductLevel01) + g2.Sum(u => u.ConductLevel02) + g2.Sum(u => u.ConductLevel03))) / (double)(g2.Sum(u => u.PupilTotal)), 2)
                        };
            lstConductStatisticsOfProvince23 = query.ToList();

            return lstConductStatisticsOfProvince23;
        }

        /// <summary>
        /// Thống kê hạnh kiểm theo phòng - Thống kê theo quận huyện
        /// Tamhm1
        /// DAte: 04/12/2012
        /// </summary>
        /// <param name="SearchInfo"></param>
        /// <returns></returns>
        public List<ConductStatisticsOfProvinceGroupByDistrict23BO> SearchByProvinceGroupByDistrict23(IDictionary<string, object> SearchInfo)
        {
            int? districtID = Utils.GetInt(SearchInfo, "DistrictID");
            int? provinceID = Utils.GetInt(SearchInfo, "ProvinceID");
            int? year = Utils.GetInt(SearchInfo, "Year");
            int? semester = Utils.GetInt(SearchInfo, "Semester");
            bool sentToSupervisor = Utils.GetBool(SearchInfo, "SentToSupervisor");
            int? educationLevelID = Utils.GetInt(SearchInfo, "EducationLevelID");
            int appliedLevel = Utils.GetInt(SearchInfo, "AppliedLevel");
            int supervisingDeptID = Utils.GetInt(SearchInfo, "SupervisingDeptID");
            bool IsSubSuperVisingDeptRole = Utils.GetBool(SearchInfo, "IsSubSuperVisingDeptRole");
            bool IsSuperVisingDeptRole = Utils.GetBool(SearchInfo, "IsSuperVisingDeptRole");
            int grade = Utils.GradeToBinary(appliedLevel);
            SupervisingDept su = SupervisingDeptBusiness.Find(supervisingDeptID);
            if (su.HierachyLevel == SystemParamsInFile.EDUCATION_HIERACHY_LEVEL_DISTRICT_OFFICE_DEPARTMENT || su.HierachyLevel == SystemParamsInFile.EDUCATION_HIERACHY_LEVEL_PROVINCE_OFFICE_DEPARTMENT)
            {
                supervisingDeptID = su.ParentID.Value;
            }
            string reportCode = "";

            if (appliedLevel == 3)
            {
                reportCode = SystemParamsInFile.THONGKEHANHKIEMTHEOBANCAP3;
            }
            else if (appliedLevel == 2)
            {
                reportCode = SystemParamsInFile.THONGKEHANHKIEMCAP2;
            }

            List<ConductStatistic> lstConductStatistic = new List<ConductStatistic>();
            //List<District> lstDistrict = DistrictRepository.All.Where(d => d.ProvinceID == provinceID && d.IsActive == true).ToList();
            var lstDistrict = (from p in DistrictBusiness.All
                               join q in ProvinceBusiness.All on p.ProvinceID equals q.ProvinceID
                               join s in SupervisingDeptBusiness.All on q.ProvinceID equals s.ProvinceID
                               where p.IsActive == true && (s.SupervisingDeptID == supervisingDeptID || s.ParentID == supervisingDeptID)
                               select new
                               {
                                   DistrictID = p.DistrictID,
                                   DistrictName = p.DistrictName,
                                   ProvinceID = q.ProvinceID
                               }).Distinct().ToList();
            lstDistrict = lstDistrict.OrderBy(o => o.DistrictName).ToList();

            IQueryable<ConductStatistic> lstConductStatisticReposity 
                = ConductStatisticRepository.All.Where(p => p.ReportCode == reportCode && p.Year == year 
                    && p.Semester == semester && p.SentToSupervisor == true 
                    && p.AppliedLevel == appliedLevel);
            if (appliedLevel == 3)
            {
                lstConductStatistic = ( from sch in SchoolProfileRepository.All
                                        join ct in lstConductStatisticReposity on sch.SchoolProfileID equals ct.SchoolID
                                        join sd in SupervisingDeptBusiness.All on sch.SupervisingDeptID equals sd.SupervisingDeptID
                                        where (sd.SupervisingDeptID == supervisingDeptID || sd.ParentID == supervisingDeptID) 
                                              && sch.IsActive == true && (sch.EducationGrade & grade)!=0
                                              && (ct.ProcessedDate == (from c1 in lstConductStatisticReposity 
                                                                      where c1.SchoolID == ct.SchoolID && c1.EducationLevelID == ct.EducationLevelID && ct.SubCommitteeID == c1.SubCommitteeID
                                                                      select c1.ProcessedDate).Max())
                                        select ct)
                                        .ToList<ConductStatistic>();
            }
            else
            {
                lstConductStatistic = (from c in lstConductStatisticReposity
                                       join s in SchoolProfileRepository.All on c.SchoolID equals s.SchoolProfileID
                                       join sd in SupervisingDeptBusiness.All on s.SupervisingDeptID equals sd.SupervisingDeptID
                                       where (sd.SupervisingDeptID == supervisingDeptID || sd.ParentID == supervisingDeptID) && s.IsActive == true && (s.EducationGrade & grade) != 0
                                       && (c.ProcessedDate == (from c1 in lstConductStatisticReposity where c.SchoolID == c1.SchoolID && c.EducationLevelID == c1.EducationLevelID select c1.ProcessedDate).Max())
                                       select c).ToList<ConductStatistic>();
            }
            //List<District> lstDistrict = DistrictRepository.All.Where(d => d.ProvinceID == provinceID && d.IsActive == true).ToList();
            //List<ConductStatistic> lstConductStatistic = new List<ConductStatistic>();

            //lstConductStatistic = (from c in ConductStatisticRepository.All.ToList()
            //                       join s in SchoolProfileRepository.All.ToList() on c.SchoolID equals s.SchoolProfileID
            //                       where 
            //                          s.ProvinceID == provinceID
            //                       && c.ReportCode == reportCode
            //                       && c.Year == year
            //                       && c.Semester == semester
            //                       && c.SentToSupervisor == sentToSupervisor
            //                       && (c.SentDate == (from c1 in ConductStatisticRepository.All.ToList() where c.SchoolID == c1.SchoolID select c1.SentDate).Max())
            //                       select c).ToList<ConductStatistic>();

            List<ConductStatisticsOfProvinceGroupByDistrict23BO> lstConductStatisticsOfProvince23;
            var query = from j in
                            (from s in lstDistrict
                             join c in lstConductStatistic on s.DistrictID equals c.SchoolProfile.DistrictID into g
                             from jn in g.DefaultIfEmpty()
                             select new
                             {
                                 DistrictID = s.DistrictID,
                                 DistrictName = s.DistrictName,
                                 ConductLevel01 = jn == null ? 0 : jn.ConductLevel01,
                                 ConductLevel02 = jn == null ? 0 : jn.ConductLevel02,
                                 ConductLevel03 = jn == null ? 0 : jn.ConductLevel03,
                                 ConductLevel04 = jn == null ? 0 : jn.ConductLevel04,
                                 PupilTotal = jn == null ? 0 : jn.PupilTotal
                             })
                        group j by new { j.DistrictID, j.DistrictName } into g2
                        select new ConductStatisticsOfProvinceGroupByDistrict23BO
                        {
                            DistrictID = g2.Key.DistrictID,
                            DistrictName = g2.Key.DistrictName,
                            TotalGood = g2.Sum(u => u.ConductLevel01),
                            TotalFair = g2.Sum(u => u.ConductLevel02),
                            TotalNormal = g2.Sum(u => u.ConductLevel03),
                            TotalWeak = g2.Sum(u => u.ConductLevel04),
                            TotalPupil = g2.Sum(u => u.PupilTotal),
                            TotalAboveAverage = g2.Sum(u => u.ConductLevel01) + g2.Sum(u => u.ConductLevel02) + g2.Sum(u => u.ConductLevel03),
                            PercentGood = Math.Round(g2.Sum(u => u.PupilTotal) == 0 ? 0 : (100 * (double)g2.Sum(u => u.ConductLevel01) / (double)(g2.Sum(u => u.PupilTotal))), 2),
                            PercentFair = Math.Round(g2.Sum(u => u.PupilTotal) == 0 ? 0 : (100 * (double)g2.Sum(u => u.ConductLevel02) / (double)(g2.Sum(u => u.PupilTotal))), 2),
                            PercentNormal = Math.Round(g2.Sum(u => u.PupilTotal) == 0 ? 0 : (100 * (double)g2.Sum(u => u.ConductLevel03) / (double)(g2.Sum(u => u.PupilTotal))), 2),
                            PercentWeak = Math.Round(g2.Sum(u => u.PupilTotal) == 0 ? 0 : (100 * (double)g2.Sum(u => u.ConductLevel04) / (double)(g2.Sum(u => u.PupilTotal))), 2),
                            PercentAboveAverage = Math.Round(g2.Sum(u => u.PupilTotal) == 0 ? 0 : (100 * (double)(g2.Sum(u => u.ConductLevel01) + g2.Sum(u => u.ConductLevel02) + g2.Sum(u => u.ConductLevel03))) / (double)(g2.Sum(u => u.PupilTotal)), 2)
                        };
            lstConductStatisticsOfProvince23 = query.ToList();
            return lstConductStatisticsOfProvince23;
        }
        /// <summary>
        /// Tìm kiếm thống kê hạnh kiểm cấp 2, 3 - Phòng: Thống kê theo khối
        /// Author: Tamhm1
        /// Date: 03/12/2012
        /// </summary>
        /// <param name="SearchInfo"></param>
        /// <returns></returns>
        public List<ConductStatisticsOfProvinceGroupByLevel23BO> SearchBySupervisingDeptGroupByLevel23(IDictionary<string, object> SearchInfo)
        {
            int? educationLevelID = Utils.GetInt(SearchInfo, "EducationLevelID");
            string reportCode = Utils.GetString(SearchInfo, "ReportCode");
            int? year = Utils.GetInt(SearchInfo, "Year");
            int? semester = Utils.GetInt(SearchInfo, "Semester");
            bool sentToSupervisor = Utils.GetBool(SearchInfo, "SentToSupervisor");
            int? trainningTypeID = Utils.GetInt(SearchInfo, "TrainingTypeID");
            int? provinceID = Utils.GetInt(SearchInfo, "ProvinceID");
            int? districtID = Utils.GetInt(SearchInfo, "DistrictID");
            int? supervisingDeptID = Utils.GetInt(SearchInfo, "SupervisingDeptID");
            int appliedLevel = Utils.GetInt(SearchInfo, "AppliedLevel");
            if (supervisingDeptID.HasValue && supervisingDeptID > 0)
            {
                SupervisingDept su = SupervisingDeptBusiness.Find(supervisingDeptID);
                if (su.HierachyLevel == SystemParamsInFile.EDUCATION_HIERACHY_LEVEL_PROVINCE_OFFICE_DEPARTMENT || su.HierachyLevel == SystemParamsInFile.EDUCATION_HIERACHY_LEVEL_DISTRICT_OFFICE_DEPARTMENT)
                {
                    supervisingDeptID = su.ParentID.Value;
                }
            }
            if (appliedLevel == 2)
            {
                reportCode = SystemParamsInFile.THONGKEHANHKIEMCAP2;
            }
            else if (appliedLevel == 3)
            {
                reportCode = SystemParamsInFile.THONGKEHANHKIEMTHEOBANCAP3;
            }
            int grade = Utils.GradeToBinary(appliedLevel);
            List<ConductStatistic> lstConductStatistic = new List<ConductStatistic>();

            IQueryable<ConductStatistic> lstConductStatisticReposity = ConductStatisticRepository.All.Where(p => p.ReportCode == reportCode && p.Year == year && p.Semester == semester && p.SentToSupervisor == true && p.AppliedLevel == appliedLevel);
            lstConductStatistic = (from c in lstConductStatisticReposity
                                   join s in SchoolProfileRepository.All on c.SchoolID equals s.SchoolProfileID
                                   join sd in SupervisingDeptBusiness.All on s.SupervisingDeptID equals sd.SupervisingDeptID
                                   where (sd.SupervisingDeptID == supervisingDeptID || sd.ParentID == supervisingDeptID) && s.IsActive == true && (s.EducationGrade & grade)!=0
                                   && (c.ProcessedDate == (from c1 in lstConductStatisticReposity where c.SchoolID == c1.SchoolID && c.EducationLevelID == c1.EducationLevelID select c1.ProcessedDate).Max())
                                   select c).ToList<ConductStatistic>();

            //lstConductStatistic = (from c in ConductStatisticRepository.All.ToList()
            //                       join s in SchoolProfileRepository.All.ToList() on c.SchoolID equals s.SchoolProfileID
            //                       where s.DistrictID == districtID
            //                       && c.ReportCode == reportCode
            //                       && c.Year == year
            //                       && c.Semester == semester
            //                       && c.SentToSupervisor == sentToSupervisor
            //                       && (c.SentDate == (from c1 in ConductStatisticRepository.All.ToList() where c.SchoolID == c1.SchoolID select c1.SentDate).Max())
            //                       select c).ToList<ConductStatistic>();

            List<ConductStatisticsOfProvinceGroupByLevel23BO> lstConductStatisticsOfSchool23;
            List<EducationLevel> lstEducationLevel = EducationLevelBusiness.GetByGrade(SystemParamsInFile.APPLIED_LEVEL_SECONDARY).ToList();
            var query = from j in
                            (from el in lstEducationLevel
                             join c in lstConductStatistic on el.EducationLevelID equals c.EducationLevelID into g
                             from jn in g.DefaultIfEmpty()
                             select new
                             {
                                 EducationLevelID = el.EducationLevelID,
                                 EducationLevelName = el.Resolution,
                                 ConductLevel01 = jn == null ? 0 : jn.ConductLevel01,
                                 ConductLevel02 = jn == null ? 0 : jn.ConductLevel02,
                                 ConductLevel03 = jn == null ? 0 : jn.ConductLevel03,
                                 ConductLevel04 = jn == null ? 0 : jn.ConductLevel04,
                                 PupilTotal = jn == null ? 0 : jn.PupilTotal
                             })
                        group j by new { j.EducationLevelID, j.EducationLevelName } into g2
                        select new ConductStatisticsOfProvinceGroupByLevel23BO
                        {
                            EducationLevelID = g2.Key.EducationLevelID,
                            EducationLevelName = g2.Key.EducationLevelName,
                            TotalGood = g2.Sum(u => u.ConductLevel01),
                            TotalFair = g2.Sum(u => u.ConductLevel02),
                            TotalNormal = g2.Sum(u => u.ConductLevel03),
                            TotalWeak = g2.Sum(u => u.ConductLevel04),
                            TotalPupil = g2.Sum(u => u.PupilTotal),
                            TotalAboveAverage = g2.Sum(u => u.ConductLevel01) + g2.Sum(u => u.ConductLevel02) + g2.Sum(u => u.ConductLevel03),
                            PercentGood = Math.Round(g2.Sum(u => u.PupilTotal) == 0 ? 0 : (100 * (double)g2.Sum(u => u.ConductLevel01) / (double)(g2.Sum(u => u.PupilTotal))), 2),
                            PercentFair = Math.Round(g2.Sum(u => u.PupilTotal) == 0 ? 0 : (100 * (double)g2.Sum(u => u.ConductLevel02) / (double)(g2.Sum(u => u.PupilTotal))), 2),
                            PercentNormal = Math.Round(g2.Sum(u => u.PupilTotal) == 0 ? 0 : (100 * (double)g2.Sum(u => u.ConductLevel03) / (double)(g2.Sum(u => u.PupilTotal))), 2),
                            PercentWeak = Math.Round(g2.Sum(u => u.PupilTotal) == 0 ? 0 : (100 * (double)g2.Sum(u => u.ConductLevel04) / (double)(g2.Sum(u => u.PupilTotal))), 2),
                            PercentAboveAverage = Math.Round(g2.Sum(u => u.PupilTotal) == 0 ? 0 : (100 * (double)(g2.Sum(u => u.ConductLevel01) + g2.Sum(u => u.ConductLevel02) + g2.Sum(u => u.ConductLevel03))) / (double)(g2.Sum(u => u.PupilTotal)), 2)
                        };
            lstConductStatisticsOfSchool23 = query.ToList();
            return lstConductStatisticsOfSchool23;
        }
        /// <summary>
        /// Thong ke hanh kiem theo sở: Thống kê theo khối
        /// Author: Tamhm1
        /// Date: 04/12/2012
        /// </summary>
        /// <param name="SearchInfo"></param>
        /// <param name="inputParameterHashKey"></param>
        /// <returns></returns>
        public List<ConductStatisticsOfProvinceGroupByLevel23BO> CreateSGDConductStatisticsByLevelSecondary(IDictionary<string, object> SearchInfo, string inputParameterHashKey, out int FileID)
        {
            System.Threading.Thread.CurrentThread.CurrentCulture = System.Globalization.CultureInfo.CreateSpecificCulture("en-US");
            int? year = Utils.GetInt(SearchInfo, "Year");
            int semester = Utils.GetInt(SearchInfo, "Semester");
            int? educationLevelID = Utils.GetInt(SearchInfo, "EducationLevelID");
            string reportCode = SystemParamsInFile.REPORT_THONG_KE_HANH_KIEM_THEO_KHOI_CAP2;
            bool sentToSupervisor = Utils.GetBool(SearchInfo, "SentToSupervisor");
            int? provinceID = Utils.GetInt(SearchInfo, "ProvinceID");
            int? districtID = Utils.GetInt(SearchInfo, "DistrictID");
            int? supervisingDeptID = Utils.GetInt(SearchInfo, "SupervisingDeptID");
            SearchInfo["AppliedLevel"] = SystemParamsInFile.APPLIED_LEVEL_SECONDARY;
            List<ConductStatisticsOfProvinceGroupByLevel23BO> lstConductStatisticOfProvinceByLevelSecondary = new List<ConductStatisticsOfProvinceGroupByLevel23BO>();
            lstConductStatisticOfProvinceByLevelSecondary = SearchByProvinceGroupByLevel23(SearchInfo);
            FileID = 0;
            if (lstConductStatisticOfProvinceByLevelSecondary.Count() == 0) return null;
            else
            {
                ReportDefinition reportDef = ReportDefinitionBusiness.GetByCode(reportCode);

                Stream data = null;

                string templatePath = SystemParamsInFile.TEMPLATE_FOLDER + reportDef.TemplateDirectory + "/" + reportDef.TemplateName;
                IVTWorkbook oBook = VTExport.OpenWorkbook(templatePath);
                //Lấy sheet template
                IVTWorksheet firstSheet = oBook.GetSheet(1);

                //Tạo dữ liệu chung

                string supervisingDeptName = SupervisingDeptRepository.Find(supervisingDeptID).SupervisingDeptName.ToUpper();
                string convertSemester = SMASConvert.ConvertSemester(semester);
                DateTime reportDate = DateTime.Now;
                string provinceName = ProvinceRepository.Find(provinceID).ProvinceName;


                int firstRow = 11;
                int beginRow = firstRow + 1;
                int endRow = 0;
                IVTWorksheet sheet = oBook.CopySheetToLast(firstSheet, "L" + firstRow);
                sheet.Name = "ThongKeHanhKiemTheoKhoi";
                //Danh sách học sinh chuyển lớp

                //Template Dòng đầu tiên
                IVTRange topRow = firstSheet.GetRange("A12", "L12");
                //Template dòng giữa
                IVTRange middleRow = firstSheet.GetRange("A13", "L13");
                //Template dòng cuối
                IVTRange lastRow = firstSheet.GetRange("A15", "L15");

                int currentRow = 0;
                string academicYearTitle = year + " - " + (year + 1);
                List<object> list = new List<object>();

                Dictionary<string, object> dic = new Dictionary<string, object> 
                {
                    {"SupervisingDeptName", supervisingDeptName}, 
                    {"Semester", convertSemester},
                    {"ProvinceName", provinceName},                  
                    {"ReportDate", reportDate},
                    {"AcademicYearTitle", academicYearTitle}
                };
                int row = 1;
                foreach (ConductStatisticsOfProvinceGroupByLevel23BO conductStatis in lstConductStatisticOfProvinceByLevelSecondary)
                {
                    currentRow = firstRow + row;
                    if (conductStatis == null)
                    {

                    }
                    if (row == 1)
                    {
                        sheet.CopyPasteSameRowHeigh(topRow, currentRow);
                    }

                    else
                    {
                        sheet.CopyPasteSameRowHeigh(middleRow, currentRow);
                    }

                    list.Add(new Dictionary<string, object> 
                    {                      
                        {"EducationLevelName", conductStatis.EducationLevelName},
                        {"TotalPupil", conductStatis.TotalPupil},
                        {"TotalGood", conductStatis.TotalGood},
                        {"TotalFair",conductStatis.TotalFair},
                        {"TotalNormal", conductStatis.TotalNormal},
                        {"TotalWeak", conductStatis.TotalWeak}                                                          
                    });
                    row++;
                }
                dic.Add("list", list);
                sheet.FillVariableValue(dic);
                endRow = currentRow + 1;
                sheet.CopyPasteSameRowHeigh(lastRow, endRow);

                string alphabetTS = VTVector.ColumnIntToString(2);
                string fomularTS = "=SUM(#" + (beginRow) + ":" + "#" + (endRow - 1) + ")";
                sheet.SetFormulaValue(endRow, 2, fomularTS.Replace("#", alphabetTS));

                for (int i = 0; i < 7; i += 2)
                {
                    string alphabet = VTVector.ColumnIntToString(i + 3);
                    string fomular = "=SUM(#" + (beginRow) + ":" + "#" + (endRow - 1) + ")";
                    sheet.SetFormulaValue(endRow, i + 3, fomular.Replace("#", alphabet));
                }

                //Fill dữ liệu

                firstSheet.Delete();
                data = oBook.ToStream();

                ProcessedReport pr = new ProcessedReport();
                pr.ReportCode = reportCode;
                pr.ProcessedDate = DateTime.Now;
                pr.InputParameterHashKey = inputParameterHashKey;
                pr.SentToSupervisor = true;
                pr.ReportData = ReportUtils.Compress(data);
                //Tao ten file
                string outputNamePattern = reportDef.OutputNamePattern;
                string hk = ReportUtils.ConvertSemesterForReportName(semester);
                //string khoi = EducationLevelBusiness.Find(educationLevelID).Resolution;

                outputNamePattern = outputNamePattern.Replace("[Semester]", hk);
                //outputNamePattern = outputNamePattern.Replace("[EducationLevel]", khoi);
                pr.ReportName = ReportUtils.RemoveSpecialCharacters(outputNamePattern) + "." + reportDef.OutputFormat;

                IDictionary<string, object> dic1 = new Dictionary<string, object> {
                {"Year", year},              
                {"Semester", semester},
                {"EducationLevelID", educationLevelID},
                {"ProvinceID",provinceID},
                {"DistrictID", districtID},
                {"SupervisingDeptID", supervisingDeptID}
            };
                ProcessedReportRepository.Insert(pr);
                ProcessedReportParameterRepository.Insert(dic1, pr);
                ProcessedReportRepository.Save();
                FileID = pr.ProcessedReportID;

                return lstConductStatisticOfProvinceByLevelSecondary;
            }
        }

        /// <summary>
        /// Thong ke hanh kiem theo phong: Thống kê theo khối
        /// Author: Tamhm1
        /// Date: 04/12/2012
        /// </summary>
        /// <param name="SearchInfo"></param>
        /// <param name="inputParameterHashKey"></param>
        /// <returns></returns>
        public List<ConductStatisticsOfProvinceGroupByLevel23BO> CreatePGDConductStatisticsByLevelSecondary(IDictionary<string, object> SearchInfo, string inputParameterHashKey, out int FileID)
        {
            System.Threading.Thread.CurrentThread.CurrentCulture = System.Globalization.CultureInfo.CreateSpecificCulture("en-US");
            int? year = Utils.GetInt(SearchInfo, "Year");
            int semester = Utils.GetInt(SearchInfo, "Semester");
            int? educationLevelID = Utils.GetInt(SearchInfo, "EducationLevelID");
            int? trainingTypeID = Utils.GetInt(SearchInfo, "TrainingTypeID ");
            string reportCode = SystemParamsInFile.REPORT_THONG_KE_HANH_KIEM_THEO_KHOI_CAP2;
            bool sentToSupervisor = Utils.GetBool(SearchInfo, "SentToSupervisor");
            int? provinceID = Utils.GetInt(SearchInfo, "ProvinceID");
            int? districtID = Utils.GetInt(SearchInfo, "DistrictID");
            int? supervisingDeptID = Utils.GetInt(SearchInfo, "SupervisingDeptID");
            SearchInfo["AppliedLevel"] = SystemParamsInFile.APPLIED_LEVEL_SECONDARY;
            List<ConductStatisticsOfProvinceGroupByLevel23BO> lstConductStatisticOfSchoolByLevelSecondary = new List<ConductStatisticsOfProvinceGroupByLevel23BO>();
            lstConductStatisticOfSchoolByLevelSecondary = SearchBySupervisingDeptGroupByLevel23(SearchInfo);
            FileID = 0;
            if (lstConductStatisticOfSchoolByLevelSecondary.Count() == 0) return null;
            else
            {
                ReportDefinition reportDef = ReportDefinitionBusiness.GetByCode(reportCode);

                Stream data = null;

                string templatePath = SystemParamsInFile.TEMPLATE_FOLDER + reportDef.TemplateDirectory + "/" + reportDef.TemplateName;
                IVTWorkbook oBook = VTExport.OpenWorkbook(templatePath);
                //Lấy sheet template
                IVTWorksheet firstSheet = oBook.GetSheet(1);

                //Tạo dữ liệu chung

                string supervisingDeptName = SupervisingDeptRepository.Find(supervisingDeptID).SupervisingDeptName.ToUpper();
                string convertSemester = SMASConvert.ConvertSemester(semester).ToUpper();
                DateTime reportDate = DateTime.Now;
                string provinceName = ProvinceRepository.Find(provinceID).ProvinceName;


                int firstRow = 11;
                int beginRow = firstRow + 1;
                int endRow = 0;
                IVTWorksheet sheet = oBook.CopySheetToLast(firstSheet, "L" + firstRow);
                sheet.Name = "ThongKeHanhKiemTheoKhoi";

                //Template Dòng đầu tiên
                IVTRange topRow = firstSheet.GetRange("A12", "L12");
                //Template dòng giữa
                IVTRange middleRow = firstSheet.GetRange("A13", "L13");
                //Template dòng cuối
                IVTRange lastRow = firstSheet.GetRange("A15", "L15");

                int currentRow = 0;
                string academicYearTitle = year + " - " + (year + 1);
                List<object> list = new List<object>();

                Dictionary<string, object> dic = new Dictionary<string, object> 
                {
                    {"SupervisingDeptName", supervisingDeptName}, 
                    {"Semester", convertSemester},
                    {"ProvinceName", provinceName},                  
                    {"ReportDate", reportDate},
                    {"AcademicYearTitle", academicYearTitle}
                };
                int row = 1;
                foreach (ConductStatisticsOfProvinceGroupByLevel23BO conductStatis in lstConductStatisticOfSchoolByLevelSecondary)
                {
                    currentRow = firstRow + row;

                    if (row == 1)
                    {
                        sheet.CopyPasteSameRowHeigh(topRow, currentRow);
                    }

                    else
                    {
                        sheet.CopyPasteSameRowHeigh(middleRow, currentRow);
                    }

                    list.Add(new Dictionary<string, object> 
                    {                      
                        {"EducationLevelName", conductStatis.EducationLevelName},
                        {"TotalPupil", conductStatis.TotalPupil},
                        {"TotalGood", conductStatis.TotalGood},
                        {"TotalFair",conductStatis.TotalFair},
                        {"TotalNormal", conductStatis.TotalNormal},
                        {"TotalWeak", conductStatis.TotalWeak}                                                          
                    });
                    row++;
                }
                dic.Add("list", list);
                sheet.FillVariableValue(dic);
                endRow = currentRow + 1;
                sheet.CopyPasteSameRowHeigh(lastRow, endRow);

                string alphabetTS = VTVector.ColumnIntToString(2);
                string fomularTS = "=SUM(#" + (beginRow) + ":" + "#" + (endRow - 1) + ")";
                sheet.SetFormulaValue(endRow, 2, fomularTS.Replace("#", alphabetTS));

                for (int i = 0; i < 7; i += 2)
                {
                    string alphabet = VTVector.ColumnIntToString(i + 3);
                    string fomular = "=SUM(#" + (beginRow) + ":" + "#" + (endRow - 1) + ")";
                    sheet.SetFormulaValue(endRow, i + 3, fomular.Replace("#", alphabet));
                }
                firstSheet.Delete();
                data = oBook.ToStream();

                ProcessedReport pr = new ProcessedReport();
                pr.ReportCode = reportCode;
                pr.ProcessedDate = DateTime.Now;
                pr.InputParameterHashKey = inputParameterHashKey;
                pr.SentToSupervisor = true;
                pr.ReportData = ReportUtils.Compress(data);

                //Tao ten file
                string outputNamePattern = reportDef.OutputNamePattern;
                string hk = ReportUtils.ConvertSemesterForReportName(semester);
                outputNamePattern = outputNamePattern.Replace("[Semester]", hk);
                outputNamePattern = outputNamePattern.Replace("SGD", "PGD");
                pr.ReportName = ReportUtils.RemoveSpecialCharacters(outputNamePattern) + "." + reportDef.OutputFormat;

                IDictionary<string, object> dic1 = new Dictionary<string, object> {
                {"Year", year},              
                {"Semester", semester},
                {"EducationLevelID", educationLevelID},
                {"TrainingTypeID", trainingTypeID},
                {"ProvinceID",provinceID},
                {"DistrictID", districtID},
                {"SupervisingDeptID", supervisingDeptID}
            };
                ProcessedReportRepository.Insert(pr);
                ProcessedReportParameterRepository.Insert(dic1, pr);
                ProcessedReportRepository.Save();
                FileID = pr.ProcessedReportID;
                return lstConductStatisticOfSchoolByLevelSecondary;
            }
        }

        public List<ConductStatisticsOfProvinceGroupByDistrict23BO> CreateSGDConductStatisticsByDistrictSecondary(IDictionary<string, object> SearchInfo, string inputParameterHashKey, out int FileID)
        {
            System.Threading.Thread.CurrentThread.CurrentCulture = System.Globalization.CultureInfo.CreateSpecificCulture("en-US");
            int? year = Utils.GetInt(SearchInfo, "Year");
            int semester = Utils.GetInt(SearchInfo, "Semester");
            int? educationLevelID = Utils.GetInt(SearchInfo, "EducationLevelID");
            string reportCode = SystemParamsInFile.REPORT_THONG_KE_HANH_KIEM_THEO_QUAN_HUYEN_CAP2;
            bool sentToSupervisor = Utils.GetBool(SearchInfo, "SentToSupervisor");
            int? provinceID = Utils.GetInt(SearchInfo, "ProvinceID");
            int? districtID = Utils.GetInt(SearchInfo, "DistrictID");
            int? supervisingDeptID = Utils.GetInt(SearchInfo, "SupervisingDeptID");
            SearchInfo["AppliedLevel"] = SystemParamsInFile.APPLIED_LEVEL_SECONDARY;
            List<ConductStatisticsOfProvinceGroupByDistrict23BO> lstConductStatisticOfProvinceByDistrictSecondary = new List<ConductStatisticsOfProvinceGroupByDistrict23BO>();
            lstConductStatisticOfProvinceByDistrictSecondary = SearchByProvinceGroupByDistrict23(SearchInfo);
            FileID = 0;
            if (lstConductStatisticOfProvinceByDistrictSecondary.Count() == 0) return null;
            else
            {
                ReportDefinition reportDef = ReportDefinitionBusiness.GetByCode(reportCode);

                Stream data = null;

                string templatePath = SystemParamsInFile.TEMPLATE_FOLDER + reportDef.TemplateDirectory + "/" + reportDef.TemplateName;
                IVTWorkbook oBook = VTExport.OpenWorkbook(templatePath);
                //Lấy sheet template
                IVTWorksheet firstSheet = oBook.GetSheet(1);

                //Tạo dữ liệu chung

                string supervisingDeptName = SupervisingDeptRepository.Find(supervisingDeptID).SupervisingDeptName.ToUpper();
                string convertSemester = SMASConvert.ConvertSemester(semester);
                DateTime reportDate = DateTime.Now;
                string provinceName = ProvinceRepository.Find(provinceID).ProvinceName;
                string academicYearTitle = year + " - " + (year + 1);

                int firstRow = 11;
                int beginRow = firstRow + 1;
                int endRow = 0;
                IVTWorksheet sheet = oBook.CopySheetToLast(firstSheet, "M" + firstRow);
                sheet.Name = "ThongKeHanhKiemTheoQuanHuyen";
                //Danh sách học sinh chuyển lớp

                //Template Dòng đầu tiên
                IVTRange topRow = firstSheet.GetRange("A12", "M12");
                //Template dòng giữa
                IVTRange middleRow = firstSheet.GetRange("A13", "M13");
                //Template dòng cuối
                IVTRange bottomRow = firstSheet.GetRange("A14", "M14");
                //Merge row
                IVTRange lastRow = firstSheet.GetRange("A15", "M15");
                //Template để mergeRow người lập báo cáo

                int currentRow = 0;

                List<object> list = new List<object>();
                int order = 1;
                Dictionary<string, object> dic = new Dictionary<string, object> 
                {
                    {"SupervisingDeptName", supervisingDeptName}, 
                    {"Semester", convertSemester},
                    {"ProvinceName", provinceName},                  
                    {"ReportDate", reportDate},
                    {"AcademicYearTitle", academicYearTitle}
                };
                foreach (ConductStatisticsOfProvinceGroupByDistrict23BO conductStatis in lstConductStatisticOfProvinceByDistrictSecondary)
                {
                    currentRow = firstRow + order;

                    if (order == 1)
                    {
                        sheet.CopyPasteSameRowHeigh(topRow, currentRow);
                    }
                    else if (order % 5 == 0 || order == lstConductStatisticOfProvinceByDistrictSecondary.Count())
                    {
                        sheet.CopyPasteSameRowHeigh(bottomRow, currentRow);
                    }

                    else
                    {
                        sheet.CopyPasteSameRowHeigh(middleRow, currentRow);
                    }

                    list.Add(new Dictionary<string, object> 
                    {
                        {"Order", order++}, 
                        {"DistrictName", conductStatis.DistrictName},
                        {"TotalPupil", conductStatis.TotalPupil},
                        {"TotalGood", conductStatis.TotalGood},
                        {"TotalFair",conductStatis.TotalFair},
                        {"TotalNormal", conductStatis.TotalNormal},
                        {"TotalWeak", conductStatis.TotalWeak}                                  
                    });
                }
                dic.Add("list", list);
                sheet.FillVariableValue(dic);
                endRow = currentRow + 1;
                sheet.CopyPasteSameRowHeigh(lastRow, endRow);
                for (int i = 1; i < 10; i += 2)
                {
                    string alphabet = VTVector.ColumnIntToString(i + 3);
                    string fomular = "=SUM(#" + (beginRow) + ":" + "#" + (endRow - 1) + ")";
                    sheet.SetFormulaValue(endRow, i + 3, fomular.Replace("#", alphabet));
                }
                string fomularSum = "=SUM(C" + (beginRow) + ":" + "C" + (endRow - 1) + ")";
                sheet.SetFormulaValue(endRow, 3, fomularSum);
                //Fill dữ liệu

                firstSheet.Delete();
                data = oBook.ToStream();

                ProcessedReport pr = new ProcessedReport();
                pr.ReportCode = reportCode;
                pr.ProcessedDate = DateTime.Now;
                pr.InputParameterHashKey = inputParameterHashKey;
                pr.SentToSupervisor = true;
                pr.ReportData = ReportUtils.Compress(data);
                //Tao ten file
                string outputNamePattern = reportDef.OutputNamePattern;
                string hk = ReportUtils.ConvertSemesterForReportName(semester);
                string khoi = "";
                if (educationLevelID != 0)
                {
                    khoi = EducationLevelBusiness.Find(educationLevelID).Resolution;
                }

                outputNamePattern = outputNamePattern.Replace("[Semester]", hk);
                outputNamePattern = outputNamePattern.Replace("[EducationLevel]", khoi);
                pr.ReportName = ReportUtils.RemoveSpecialCharacters(outputNamePattern) + "." + reportDef.OutputFormat;

                IDictionary<string, object> dic1 = new Dictionary<string, object> {
                {"Year", year},              
                {"Semester", semester},
                {"EducationLevelID", educationLevelID},
                {"ProvinceID",provinceID},
                {"DistrictID", districtID},
                {"SupervisingDeptID", supervisingDeptID}
            };

                ProcessedReportRepository.Insert(pr);
                ProcessedReportParameterRepository.Insert(dic1, pr);
                ProcessedReportRepository.Save();
                FileID = pr.ProcessedReportID;
                return lstConductStatisticOfProvinceByDistrictSecondary;
            }
        }

        public List<ConductStatisticsOfProvince23BO> SearchByProvince23(IDictionary<string, object> SearchInfo)
        {
            int Year = Utils.GetInt(SearchInfo, "Year");
            int Semester = Utils.GetInt(SearchInfo, "Semester");
            int EducationLevelID = Utils.GetInt(SearchInfo, "EducationLevelID");
            int TrainingTypeID = Utils.GetInt(SearchInfo, "TrainingTypeID");
            string ReportCode = Utils.GetString(SearchInfo, "ReportCode");
            bool SentToSupervisor = Utils.GetBool(SearchInfo, "SentToSupervisor");
            int ProvinceID = Utils.GetInt(SearchInfo, "ProvinceID");
            int DistrictID = Utils.GetInt(SearchInfo, "DistrictID");
            int SubCommitteeID = Utils.GetInt(SearchInfo, "SubCommitteeID");
            int AppliedLevel = Utils.GetInt(SearchInfo, "AppliedLevel");
            int SupervisingDeptID = Utils.GetInt(SearchInfo, "SupervisingDeptID");
            SupervisingDept supervisingDept = SupervisingDeptBusiness.Find(SupervisingDeptID);
            if (supervisingDept.HierachyLevel == SystemParamsInFile.EDUCATION_HIERACHY_LEVEL_PROVINCE_OFFICE_DEPARTMENT || supervisingDept.HierachyLevel == SystemParamsInFile.EDUCATION_HIERACHY_LEVEL_DISTRICT_OFFICE_DEPARTMENT)
            {
                SupervisingDeptID = supervisingDept.ParentID.Value;
            }
            int grade = Utils.GradeToBinary(AppliedLevel);

            // Thong tin hanh kiem truong gui len
            var query = ConductStatisticRepository.All.Where(o => o.ReportCode == ReportCode && o.SentToSupervisor == SentToSupervisor);

            if (Year > 0)
            {
                query = query.Where(u => u.Year == Year);
            }
            if (Semester > 0)
            {
                query = query.Where(u => u.Semester == Semester);
            }
            if (EducationLevelID > 0)
            {
                query = query.Where(u => u.EducationLevelID == EducationLevelID);
            }

            if (SubCommitteeID > 0)
            {
                query = query.Where(u => u.SubCommitteeID == SubCommitteeID);
            }
            // Lay them thong tin cua ban hoc
            // Neu cap 2 thi truong nay la null nen ban chat la van lay ngay gui gan nhat theo truong
            // Neu cap 3 thi mot so truong hop phai tong hop du lieu tu nhieu ban do vay phai lay them de SUM
            var lstConductStatistics = query.Where(o => o.SentDate == query
                .Where(u => u.SchoolID == o.SchoolID && (u.SubCommitteeID.HasValue ? u.SubCommitteeID : 0) == (o.SubCommitteeID.HasValue ? o.SubCommitteeID : 0))
                .Select(u => u.SentDate).Max());

            List<ConductStatisticsOfProvince23BO> lstConductStatisticsOfProvince23BO =
                (from s in SchoolProfileRepository.All
                 join p in SupervisingDeptBusiness.All on s.SupervisingDeptID equals p.SupervisingDeptID
                 join q in lstConductStatistics on s.SchoolProfileID equals q.SchoolID into g
                 from c in g.DefaultIfEmpty()
                 where s.IsActive == true && (p.SupervisingDeptID == SupervisingDeptID || p.ParentID == SupervisingDeptID)
                    && (ProvinceID > 0 ? s.ProvinceID == ProvinceID : true)
                    && (DistrictID > 0 ? s.DistrictID == DistrictID : true)
                    && (s.EducationGrade & grade) != 0
                    && (TrainingTypeID > 0 ? s.TrainingTypeID == TrainingTypeID : true)
                 group c by new
                 {
                     c.SchoolID,
                     s.SchoolName
                 } into gr
                 select new ConductStatisticsOfProvince23BO
                 {
                     SchoolName = gr.Key.SchoolName,
                     SchoolID = gr.Key.SchoolID,
                     SentDate = gr.Max(o => o.SentDate),
                     TotalGood = gr.Sum(c => c.ConductLevel01.HasValue ? c.ConductLevel01.Value : 0),
                     TotalFair = gr.Sum(c => c.ConductLevel02.HasValue ? c.ConductLevel02.Value : 0),
                     TotalNormal = gr.Sum(c => c.ConductLevel03.HasValue ? c.ConductLevel03.Value : 0),
                     TotalWeak = gr.Sum(c => c.ConductLevel04.HasValue ? c.ConductLevel04.Value : 0),
                     TotalPupil = gr.Sum(c => c.PupilTotal.HasValue ? c.PupilTotal.Value : 0)
                 }).OrderBy(o => o.SchoolName).ToList();

            if (lstConductStatisticsOfProvince23BO != null && lstConductStatisticsOfProvince23BO.Count > 0)
            {
                foreach (var item in lstConductStatisticsOfProvince23BO)
                {
                    item.TotalAboveAverage = (item.TotalFair.HasValue ? item.TotalFair.Value : 0)
                        + (item.TotalGood.HasValue ? item.TotalGood.Value : 0)
                        + (item.TotalNormal.HasValue ? item.TotalNormal.Value : 0);
                    item.PercentAboveAverage = item.TotalPupil.HasValue && item.TotalPupil.Value > 0 && item.TotalAboveAverage.HasValue
                        ? Math.Round((double)item.TotalAboveAverage * 100 / (double)item.TotalPupil, 2, MidpointRounding.AwayFromZero) : 0;
                    item.PercentGood = item.TotalPupil.HasValue && item.TotalPupil.Value > 0 && item.TotalGood.HasValue
                        ? Math.Round((double)item.TotalGood * 100 / (double)item.TotalPupil, 2, MidpointRounding.AwayFromZero) : 0;
                    item.PercentFair = item.TotalPupil.HasValue && item.TotalPupil.Value > 0 && item.TotalFair.HasValue
                        ? Math.Round((double)item.TotalFair * 100 / (double)item.TotalPupil, 2, MidpointRounding.AwayFromZero) : 0;
                    item.PercentNormal = item.TotalPupil.HasValue && item.TotalPupil.Value > 0 && item.TotalNormal.HasValue
                        ? Math.Round((double)item.TotalNormal * 100 / (double)item.TotalPupil, 2, MidpointRounding.AwayFromZero) : 0;
                    item.PercentWeak = item.TotalPupil.HasValue && item.TotalPupil.Value > 0 && item.TotalWeak.HasValue
                        ? Math.Round((double)item.TotalWeak * 100 / (double)item.TotalPupil, 2, MidpointRounding.AwayFromZero) : 0;
                }
            }
            lstConductStatisticsOfProvince23BO = lstConductStatisticsOfProvince23BO.OrderBy(o => o.SchoolName).ToList();
            return lstConductStatisticsOfProvince23BO;
        }

        /// <summary>
        /// author: namdv3
        /// </summary>
        /// <param name="SearchInfo"></param>
        /// <param name="InputParameterHashKey"></param>
        /// <param name="FileID"></param>
        /// <returns></returns>
        public List<ConductStatisticsOfProvince23BO> CreateSGDConductStatisticsTertiary(IDictionary<string, object> SearchInfo, string InputParameterHashKey, out int FileID)
        {
            int Year = Utils.GetInt(SearchInfo, "Year");
            int Semester = Utils.GetInt(SearchInfo, "Semester");
            int EducationLevelID = Utils.GetInt(SearchInfo, "EducationLevelID");
            int TrainingTypeID = Utils.GetInt(SearchInfo, "TrainingTypeID");
            string ReportCode = Utils.GetString(SearchInfo, "ReportCode");
            bool SentToSupervisor = Utils.GetBool(SearchInfo, "SentToSupervisor");
            int ProvinceID = Utils.GetInt(SearchInfo, "ProvinceID");
            int DistrictID = Utils.GetInt(SearchInfo, "DistrictID");
            int SubCommitteeID = Utils.GetInt(SearchInfo, "SubCommitteeID");
            int SupervisingDeptID = Utils.GetInt(SearchInfo, "SupervisingDeptID");
            SearchInfo["AppliedLevel"] = SystemParamsInFile.APPLIED_LEVEL_TERTIARY;
            var lstConductStatisticOfProvinceTertiary = this.SearchByProvince23(SearchInfo);
            lstConductStatisticOfProvinceTertiary = lstConductStatisticOfProvinceTertiary.OrderBy(o => o.SchoolName).ToList();
            if (lstConductStatisticOfProvinceTertiary != null && lstConductStatisticOfProvinceTertiary.Count > 0)
            {
                var rp = ReportDefinitionBusiness.GetByCode(SystemParamsInFile.REPORT_SGD_THPT_THONGKEHANHKIEM);
                string templatePath = ReportUtils.GetTemplatePath(rp);
                IVTWorkbook oBook = VTExport.OpenWorkbook(templatePath);

                //Lấy sheet template
                IVTWorksheet templateSheet = oBook.GetSheet(1);
                IVTWorksheet sheet = oBook.CopySheetToLast(templateSheet, "N14");
                sheet.Name = "THPT_TKHK";
                //Tao khung cho report
                IVTRange sumTempRow = templateSheet.GetRange("A19", "N19");
                int sumRowIndex = 0;
                if (lstConductStatisticOfProvinceTertiary.Count > 5)
                {
                    IVTRange topTempRow = templateSheet.GetRange("A16", "N16");
                    IVTRange midTempRow = templateSheet.GetRange("A17", "N17");
                    IVTRange botTempRow = templateSheet.GetRange("A18", "N18");
                    for (int i = 0; i < lstConductStatisticOfProvinceTertiary.Count - 5; i++)
                    {
                        if (i % 5 == 0)
                            sheet.CopyPaste(topTempRow, i + 15, 1);
                        else
                            if (i % 5 == 4)
                                sheet.CopyPaste(botTempRow, i + 15, 1);
                            else
                                sheet.CopyPaste(midTempRow, i + 15, 1);
                    }
                    sumRowIndex = lstConductStatisticOfProvinceTertiary.Count + 10;
                    sheet.CopyPaste(sumTempRow, sumRowIndex, 1);
                }
                else
                {
                    sumRowIndex = lstConductStatisticOfProvinceTertiary.Count + 10;
                    sheet.CopyPaste(sumTempRow, sumRowIndex, 1);
                    for (int j = sumRowIndex + 10; j >= sumRowIndex + 1; j--)
                    {
                        sheet.DeleteRow(j);
                    }
                }

                //templateSheet.MergeRow(sumRowIndex,1,3);
                //templateSheet.GetRange(sumRowIndex, 1, sumRowIndex, 3).Value = "Tổng cộng";
                // templateSheet.GetRange(sumRowIndex, 1, sumRowIndex, 14).SetBorder(VTBorderStyle.Solid, VTBorderWeight.Medium, VTBorderIndex.All);
                sheet.SetFormulaValue(string.Format("{0}{1}", "D", sumRowIndex), string.Format("=SUM({0}:{1})", "D10", string.Format("{0}{1}", "D", sumRowIndex - 1)));
                for (char colChar = 'E'; colChar <= 'M'; colChar = (char)(colChar + 2))
                    sheet.SetFormulaValue(string.Format("{0}{1}", colChar, sumRowIndex), string.Format("=SUM({0}:{1})", string.Format("{0}{1}", colChar, "10"), string.Format("{0}{1}", colChar, sumRowIndex - 1)));
                // lay du lieu vao list
                //Tạo dictionary dữ liệu
                Dictionary<string, object> dic = new Dictionary<string, object> 
                {
                    {"SupervisingDeptName",SupervisingDeptID>0? SupervisingDeptBusiness.Find(SupervisingDeptID).SupervisingDeptName.ToUpper():"TATCA"},
                    {"SupervisingDeptID", SupervisingDeptID},
                    {"Semester", Semester},
                    {"Year", Year},
                    {"TrainingType",TrainingTypeID>0? TrainingTypeBusiness.Find(TrainingTypeID).Resolution:"TATCA"},
                    {"EducationLevel",EducationLevelID>0? EducationLevelBusiness.Find(EducationLevelID).Resolution:"TATCA"}
                };
                //fill du lieu vao excel
                List<object> list = new List<object>();
                int order = 1;
                foreach (var bo in lstConductStatisticOfProvinceTertiary)
                {
                    Dictionary<string, object> dic_list = new Dictionary<string, object>();
                    dic_list.Add("Order", order++);
                    dic_list.Add("SchoolName", bo.SchoolName);
                    dic_list.Add("SentDate", bo.SentDate.HasValue ? bo.SentDate.Value.ToString("dd/MM/yyyy") : string.Empty);
                    dic_list.Add("TotalPupil", bo.TotalPupil);
                    dic_list.Add("TotalGood", bo.TotalGood);
                    dic_list.Add("TotalFair", bo.TotalFair);
                    dic_list.Add("TotalNormal", bo.TotalNormal);
                    dic_list.Add("TotalWeak", bo.TotalWeak);
                    dic_list.Add("TotalAboveAverage", bo.TotalAboveAverage);
                    if (bo.PercentGood.HasValue)
                        dic_list.Add("PercentGood", bo.PercentGood.Value);
                    else dic_list.Add("PercentGood", "");
                    if (bo.PercentFair.HasValue)
                        dic_list.Add("PercentFair", bo.PercentFair.Value);
                    else dic_list.Add("PercentFair", "");
                    if (bo.PercentNormal.HasValue)
                        dic_list.Add("PercentNormal", bo.PercentNormal.Value);
                    else dic_list.Add("PercentNormal", "");
                    if (bo.PercentWeak.HasValue)
                        dic_list.Add("PercentWeak", bo.PercentWeak.Value);
                    else dic_list.Add("PercentWeak", "");
                    if (bo.PercentAboveAverage.HasValue)
                        dic_list.Add("PercentAboveAverage", bo.PercentAboveAverage.Value);
                    else dic_list.Add("PercentAboveAverage", "");
                    list.Add(dic_list);
                }

                var date = DateTime.Now;
                string trainingTypeName = TrainingTypeID > 0 ? TrainingTypeBusiness.Find(TrainingTypeID).Resolution : "[Tất cả]";
                string subCommitteeName = SubCommitteeID > 0 ? SubCommitteeBusiness.Find(SubCommitteeID).Resolution : "[Tất cả]";
                string TrainingType_Semester_SubCommittee_Year_Format = "Hệ {0} - {1} - Ban {2} - {3} - Năm {4}";
                string TrainingType = string.Format(TrainingType_Semester_SubCommittee_Year_Format, trainingTypeName, EducationLevelID > 0 ? EducationLevelBusiness.Find(EducationLevelID).Resolution : "Khối [Tất cả]", SubCommitteeID > 0 ? subCommitteeName.Replace("Ban ", "") : "[Tất cả]", Semester > 0 ? ReportUtils.ConvertSemesterForDisplayTitle(Semester) : "Học kì [Tất cả]", Year > 0 ? Year + " - " + (Year + 1) : "[Tất cả]");
                Dictionary<string, object> dicParam = new Dictionary<string, object> 
                {
                    {"TrainingType_Semester", TrainingType},
                    {"SupervisingDeptName", SupervisingDeptBusiness.Find(SupervisingDeptID).SupervisingDeptName.ToUpper()},
                    {"ProvinceVsDate", string.Format("{0}, ngày {1} tháng {2} năm {3}", ProvinceBusiness.Find(ProvinceID).ProvinceName, date.Day, date.Month, date.Year)},
                    {"list", list}
                };
                //Fill dữ liệu
                sheet.FillVariableValue(dicParam);
                sheet.FitAllColumnsOnOnePage = true;
                //templateSheet.DeleteRow(12);
                //templateSheet.DeleteRow(13);
                templateSheet.Delete();
                var Data = oBook.ToStream();

                //SGD_THPT_ThongKeHK_[Khối]_[Học kỳ].xls
                //Tạo tên file
                string outputNamePattern = rp.OutputNamePattern;
                string hk = Semester > 0 ? ReportUtils.ConvertSemesterForReportName(Semester) : "TATCA";
                string khoi = EducationLevelID > 0 ? EducationLevelBusiness.Find(EducationLevelID).Resolution : "TATCA";
                outputNamePattern = outputNamePattern.Replace("_[District]", DistrictID > 0 ? "_" + DistrictBusiness.Find(DistrictID).DistrictName : string.Empty);
                outputNamePattern = outputNamePattern.Replace("_[TrainingType]", (TrainingTypeID > 0 ? "_" + trainingTypeName : string.Empty));
                outputNamePattern = outputNamePattern.Replace("_[SubCommittee]", (SubCommitteeID > 0 ? "_" + subCommitteeName : string.Empty));
                outputNamePattern = outputNamePattern.Replace("[Semester]", ReportUtils.StripVNSign(hk));
                outputNamePattern = outputNamePattern.Replace("_[EducationLevel]", EducationLevelID > 0 ? "_" + khoi : string.Empty);

                ProcessedReport processedReport = new ProcessedReport();
                processedReport.ReportCode = SystemParamsInFile.REPORT_SGD_THPT_THONGKEHANHKIEM;
                processedReport.InputParameterHashKey = InputParameterHashKey;
                processedReport.SentToSupervisor = true;
                processedReport.ProcessedDate = DateTime.Now;
                processedReport.ReportData = ReportUtils.Compress(Data);
                processedReport.ReportName = ReportUtils.RemoveSpecialCharacters(outputNamePattern) + "." + rp.OutputFormat;

                foreach (var item in dic)
                {
                    ProcessedReportParameter reportParams = new ProcessedReportParameter();
                    reportParams.ProcessedReport = processedReport;
                    reportParams.ParameterName = item.Key;
                    reportParams.ParameterValue = item.Value != null ? item.Value.ToString() : string.Empty;
                    processedReport.ProcessedReportParameters.Add(reportParams);
                }
                ProcessedReportBusiness.Insert(processedReport);
                ProcessedReportBusiness.Save();
                FileID = processedReport.ProcessedReportID;
                return lstConductStatisticOfProvinceTertiary;
            }
            else
            {
                FileID = 0;
                return new List<ConductStatisticsOfProvince23BO>();
            }
        }

        /// <summary>
        /// minhh
        /// 4/12/2012
        /// </summary>
        /// <param name="SearchInfo"></param>
        /// <param name="InputParameterHashkey"></param>
        /// <returns></returns>
        public List<ConductStatisticsOfProvince23BO> CreateSGDConductStatisticsSecondary(IDictionary<string, object> SearchInfo, string InputParameterHashkey, out int FileID)
        {
            System.Threading.Thread.CurrentThread.CurrentCulture = System.Globalization.CultureInfo.CreateSpecificCulture("en-US");
            int? districtID = Utils.GetInt(SearchInfo, "DistrictID");
            int? provinceID = Utils.GetInt(SearchInfo, "ProvinceID");
            string reportCode = Utils.GetString(SearchInfo, "ReportCode");
            string reportCodeToSearch = SystemParamsInFile.REPORT_THONG_KE_HANH_KIEM_SO;
            int? year = Utils.GetInt(SearchInfo, "Year");
            int semester = Utils.GetInt(SearchInfo, "Semester");
            bool? sentToSupervisor = Utils.GetNullableBool(SearchInfo, "SentToSupervisor");
            int? educationLevelID = Utils.GetInt(SearchInfo, "EducationLevelID");
            int? trainningTypeID = Utils.GetInt(SearchInfo, "TrainingTypeID");
            int? SupervisingDeptID = Utils.GetInt(SearchInfo, "SupervisingDeptID");

            FileID = 0;

            List<ConductStatisticsOfProvince23BO> lstCapacityStatisticOfProvinceSecondary = this.SearchByProvince23(SearchInfo);
            lstCapacityStatisticOfProvinceSecondary = lstCapacityStatisticOfProvinceSecondary.OrderBy(o => o.SchoolName).ToList();
            if (lstCapacityStatisticOfProvinceSecondary != null)
            {

                ReportDefinition reportDef = ReportDefinitionBusiness.GetByCode(reportCodeToSearch);
                string templatePath = SystemParamsInFile.TEMPLATE_FOLDER + reportDef.TemplateDirectory + "/" + reportDef.TemplateName;
                IVTWorkbook oBook = VTExport.OpenWorkbook(templatePath);

                //Lấy sheet 
                IVTWorksheet tempSheet = oBook.GetSheet(1);
                IVTWorksheet sheet = oBook.CopySheetToLast(tempSheet, "N10");
                sheet.Name = "THCS_TKHK";

                //fill dữ liệu vào sheet thông tin chung
                SupervisingDept supervisingDept = SupervisingDeptBusiness.Find(SupervisingDeptID);

                string supervisingDeptName = SupervisingDeptBusiness.Find(SupervisingDeptID).SupervisingDeptName.ToUpper();
                string districtName = districtID > 0 ? DistrictBusiness.Find(districtID).DistrictName : "[Tất cả]";
                string provinceName = supervisingDept.Province.ProvinceName;
                string trainingName = trainningTypeID > 0 ? TrainingTypeBusiness.Find(trainningTypeID).Resolution : "[Tất cả]";
                string educationName = educationLevelID > 0 ? EducationLevelBusiness.Find(educationLevelID).Resolution : "Khối [Tất cả]";
                string dateTime = "ngày " + DateTime.Now.Date.Day + " tháng " + DateTime.Now.Month + " năm " + DateTime.Now.Year;
                string provinceAndDate = provinceName + ", " + dateTime;
                string semesterName = semester > 0 ? ReportUtils.ConvertSemesterForReportName(semester) : string.Empty;
                string semesterAndAcademic = districtName + " - Hệ " + trainingName + " - " + educationName + " - " + ReportUtils.ConvertSemesterForDisplayTitle(semester) + " - Năm học " + year + " - " + (year + 1);
                sheet.SetCellValue("A2", supervisingDeptName);

                sheet.SetCellValue("G4", provinceAndDate);
                sheet.SetCellValue("B7", semesterAndAcademic);

                int startRow = 10;

                IVTRange range = tempSheet.GetRange("A10", "N10");
                IVTRange lastRange = tempSheet.GetRange("A198", "N198");
                IVTRange rangeTC = tempSheet.GetRange("A200", "N200");

                for (int i = 0; i < lstCapacityStatisticOfProvinceSecondary.Count; i++)
                {
                    ConductStatisticsOfProvince23BO conductBO = lstCapacityStatisticOfProvinceSecondary[i];
                    conductBO.TotalPupil = conductBO.TotalPupil.HasValue ? conductBO.TotalPupil.Value : 0;
                    conductBO.TotalFair = conductBO.TotalFair.HasValue ? conductBO.TotalFair.Value : 0;
                    conductBO.TotalGood = conductBO.TotalGood.HasValue ? conductBO.TotalGood.Value : 0;
                    conductBO.TotalNormal = conductBO.TotalNormal.HasValue ? conductBO.TotalNormal.Value : 0;
                    conductBO.TotalWeak = conductBO.TotalWeak.HasValue ? conductBO.TotalWeak.Value : 0;
                    conductBO.TotalAboveAverage = conductBO.TotalAboveAverage.HasValue ? conductBO.TotalAboveAverage.Value : 0;

                    if (i == lstCapacityStatisticOfProvinceSecondary.Count - 1)
                    {
                        sheet.CopyPasteSameRowHeigh(lastRange, startRow);
                    }
                    else
                    {
                        //Copy row style
                        if (startRow + i > 10)
                        {
                            if ((i + 1) % 5 == 0)
                            {
                                sheet.CopyPasteSameRowHeigh(lastRange, startRow);
                            }
                            else
                                sheet.CopyPasteSameRowHeigh(range, startRow);
                        }
                    }
                    //STT
                    sheet.SetCellValue(startRow, 1, i + 1);

                    //Ten truong
                    sheet.SetCellValue(startRow, 2, conductBO.SchoolName);

                    //Ngay gui
                    sheet.SetCellValue(startRow, 3, conductBO.SentDate.HasValue ? conductBO.SentDate.Value.ToString("dd/MM/yyyy") : string.Empty);

                    //Tong so HS
                    sheet.SetCellValue(startRow, 4, conductBO.TotalPupil);

                    //Tot
                    sheet.SetCellValue(startRow, 5, conductBO.TotalGood);
                    //sheet.SetCellValue(startRow, 6, Math.Round(conductBO.PercentGood.Value, 2, MidpointRounding.AwayFromZero));

                    //Kha
                    sheet.SetCellValue(startRow, 7, conductBO.TotalFair);
                    //sheet.SetCellValue(startRow, 8, Math.Round(conductBO.PercentFair.Value, 2, MidpointRounding.AwayFromZero));

                    //TB
                    sheet.SetCellValue(startRow, 9, conductBO.TotalNormal);
                    //sheet.SetCellValue(startRow, 10, Math.Round(conductBO.PercentNormal.Value, 2, MidpointRounding.AwayFromZero));

                    //Yeu
                    sheet.SetCellValue(startRow, 11, conductBO.TotalWeak);
                    //sheet.SetCellValue(startRow, 12, Math.Round(conductBO.PercentWeak.Value, 2, MidpointRounding.AwayFromZero));

                    //TB tro len
                    sheet.SetCellValue(startRow, 13, conductBO.TotalAboveAverage);
                    //sheet.SetCellValue(startRow, 14, Math.Round(conductBO.PercentAboveAverage.Value, 2, MidpointRounding.AwayFromZero));

                    startRow++;
                }

                sheet.CopyPasteSameRowHeigh(rangeTC, startRow);
                sheet.SetCellValue(startRow, 1, "Tổng cộng");
                sheet.MergeRow(startRow, 1, 3);
                // Thiet lap cong thuc
                string alphabetTS = VTVector.ColumnIntToString(4);
                string fomularTS = "=SUM(#" + (10) + ":" + "#" + (startRow - 1) + ")";
                sheet.SetFormulaValue(startRow, 4, fomularTS.Replace("#", alphabetTS));

                for (int i = 1; i <= 9; i += 2)
                {
                    string alphabet = VTVector.ColumnIntToString(i + 4);
                    string fomular = "=SUM(#" + (10) + ":" + "#" + (startRow - 1) + ")";
                    sheet.SetFormulaValue(startRow, i + 4, fomular.Replace("#", alphabet));
                }
                tempSheet.Delete();

                sentToSupervisor = true;
                //Tạo tên file
                string outputNamePattern = reportDef.OutputNamePattern;
                string khoi = educationLevelID > 0 ? EducationLevelBusiness.Find(educationLevelID).Resolution : "TatCa";
                outputNamePattern = outputNamePattern.Replace("_[District]", districtID.HasValue && districtID.Value > 0 ? "_" + DistrictBusiness.Find(districtID).DistrictName : string.Empty);
                outputNamePattern = outputNamePattern.Replace("_[TrainingType]", (trainningTypeID.HasValue && trainningTypeID.Value > 0 ? "_" + TrainingTypeBusiness.Find(trainningTypeID).Resolution : string.Empty));
                outputNamePattern = outputNamePattern.Replace("[Semester]", semesterName);
                outputNamePattern = outputNamePattern.Replace("_[EducationLevel]", educationLevelID.HasValue && educationLevelID.Value > 0 ? "_" + educationName : string.Empty);

                IDictionary<string, object> dic = new Dictionary<string, object> {
                {"Semester", semester},
                {"Year", year},
                {"EducationLevelID", educationLevelID},
                {"TrainingTypeID", trainningTypeID},
                {"DistrictID ", districtID = 0},
                {"ProvinceID", provinceID},
                {"SupervisingDeptID", SupervisingDeptID},
                {"SentToSupervisor", true}
            };
                ProcessedReport pr = InsertSGDConductStatisticsSecondary(reportCodeToSearch, InputParameterHashkey, outputNamePattern, dic, oBook.ToStream());
                FileID = pr.ProcessedReportID;
            }

            return lstCapacityStatisticOfProvinceSecondary;
        }

        private ProcessedReport InsertSGDConductStatisticsSecondary(string reportCode, string inputHashKey, string outputNamePattern, IDictionary<string, object> dic, Stream data)
        {
            ReportDefinition reportDef = ReportDefinitionBusiness.GetByCode(reportCode);
            ProcessedReport pr = new ProcessedReport();
            pr.ReportCode = reportCode;
            pr.ProcessedDate = DateTime.Now;
            pr.InputParameterHashKey = inputHashKey;
            pr.ReportData = ReportUtils.Compress(data);
            pr.ReportName = ReportUtils.RemoveSpecialCharacters(outputNamePattern) + "." + reportDef.OutputFormat;
            ProcessedReportParameterRepository.Insert(dic, pr);
            ProcessedReportRepository.Insert(pr);
            ProcessedReportRepository.Save();
            return pr;
        }

        /// <summary>
        /// minhh
        /// 4/12/2012
        /// </summary>
        /// <param name="SearchInfo"></param>
        /// <returns></returns>
        public List<ConductStatisticsOfProvince23BO> SearchBySupervisingDept23(IDictionary<string, object> SearchInfo)
        {
            int Year = Utils.GetInt(SearchInfo, "Year");
            int Semester = Utils.GetInt(SearchInfo, "Semester");
            int EducationLevelID = Utils.GetInt(SearchInfo, "EducationLevelID");
            int TrainingTypeID = Utils.GetInt(SearchInfo, "TrainingTypeID");
            string ReportCode = Utils.GetString(SearchInfo, "ReportCode");
            bool SentToSupervisor = Utils.GetBool(SearchInfo, "SentToSupervisor");
            int ProvinceID = Utils.GetInt(SearchInfo, "ProvinceID");
            int DistrictID = Utils.GetInt(SearchInfo, "DistrictID");
            int SubCommitteeID = Utils.GetInt(SearchInfo, "SubCommitteeID");
            int supervisingDeptID = Utils.GetInt(SearchInfo, "SupervisingDeptID");
            int? AppliedLevel = Utils.GetInt(SearchInfo, "AppliedLevel");
            int grade = Utils.GradeToBinary(AppliedLevel.Value);
            SupervisingDept su = SupervisingDeptBusiness.Find(supervisingDeptID);
            if (su != null && (su.HierachyLevel == SystemParamsInFile.EDUCATION_HIERACHY_LEVEL_DISTRICT_OFFICE_DEPARTMENT || su.HierachyLevel == SystemParamsInFile.EDUCATION_HIERACHY_LEVEL_PROVINCE_OFFICE_DEPARTMENT))
            {
                supervisingDeptID = su.ParentID.Value;
            }
            // Thong tin hanh kiem truong gui len
            var query = ConductStatisticRepository.All.Where(o => o.ReportCode == ReportCode && o.SentToSupervisor == SentToSupervisor);

            if (Year > 0)
            {
                query = query.Where(u => u.Year == Year);
            }
            if (Semester > 0)
            {
                query = query.Where(u => u.Semester == Semester);
            }
            if (EducationLevelID > 0)
            {
                query = query.Where(u => u.EducationLevelID == EducationLevelID);
            }

            if (SubCommitteeID > 0)
            {
                query = query.Where(u => u.SubCommitteeID == SubCommitteeID);
            }
            var lstConductStatistics = query.Where(o => o.SentDate == query.Where(u => u.SchoolID == o.SchoolID).Select(u => u.SentDate).Max());

            List<ConductStatisticsOfProvince23BO> lstConductStatisticsOfProvince23BO =
                (from s in SchoolProfileRepository.All
                 join sd in SupervisingDeptBusiness.All on s.SupervisingDeptID equals sd.SupervisingDeptID
                 join q in lstConductStatistics on s.SchoolProfileID equals q.SchoolID into g
                 from c in g.DefaultIfEmpty()
                 where s.IsActive == true
                    && (ProvinceID > 0 ? s.ProvinceID == ProvinceID : true)
                    && (supervisingDeptID > 0 ? (s.SupervisingDeptID == supervisingDeptID || sd.ParentID == supervisingDeptID) : true)
                    && (s.EducationGrade & grade) != 0
                    && (TrainingTypeID > 0 ? s.TrainingTypeID == TrainingTypeID : true)
                 select new ConductStatisticsOfProvince23BO
                 {
                     SchoolName = s.SchoolName,
                     SchoolID = s.SchoolProfileID,
                     SentDate = c.SentDate.Value,
                     TotalGood = c.ConductLevel01,
                     TotalFair = c.ConductLevel02,
                     TotalNormal = c.ConductLevel03,
                     TotalWeak = c.ConductLevel04,
                     TotalPupil = c.PupilTotal,
                 }).OrderBy(o => o.SchoolName).ToList();
            if (lstConductStatisticsOfProvince23BO != null && lstConductStatisticsOfProvince23BO.Count > 0)
            {
                foreach (var item in lstConductStatisticsOfProvince23BO)
                {
                    item.TotalAboveAverage = (item.TotalFair.HasValue ? item.TotalFair.Value : 0)
                        + (item.TotalGood.HasValue ? item.TotalGood.Value : 0)
                        + (item.TotalNormal.HasValue ? item.TotalNormal.Value : 0);
                    item.PercentAboveAverage = item.TotalPupil.HasValue && item.TotalPupil.Value > 0 && item.TotalAboveAverage.HasValue
                        ? Math.Round((double)item.TotalAboveAverage * 100 / (double)item.TotalPupil, 2, MidpointRounding.AwayFromZero) : 0;
                    item.PercentGood = item.TotalPupil.HasValue && item.TotalPupil.Value > 0 && item.TotalGood.HasValue
                        ? Math.Round((double)item.TotalGood * 100 / (double)item.TotalPupil, 2, MidpointRounding.AwayFromZero) : 0;
                    item.PercentFair = item.TotalPupil.HasValue && item.TotalPupil.Value > 0 && item.TotalFair.HasValue
                        ? Math.Round((double)item.TotalFair * 100 / (double)item.TotalPupil, 2, MidpointRounding.AwayFromZero) : 0;
                    item.PercentNormal = item.TotalPupil.HasValue && item.TotalPupil.Value > 0 && item.TotalNormal.HasValue
                        ? Math.Round((double)item.TotalNormal * 100 / (double)item.TotalPupil, 2, MidpointRounding.AwayFromZero) : 0;
                    item.PercentWeak = item.TotalPupil.HasValue && item.TotalPupil.Value > 0 && item.TotalWeak.HasValue
                        ? Math.Round((double)item.TotalWeak * 100 / (double)item.TotalPupil, 2, MidpointRounding.AwayFromZero) : 0;
                }
            }
            lstConductStatisticsOfProvince23BO = lstConductStatisticsOfProvince23BO.OrderBy(o => o.SchoolName).ToList();
            return lstConductStatisticsOfProvince23BO;
        }

        /// <summary>
        /// minhh
        /// 4/12/2012
        /// </summary>
        /// <param name="SearchInfo"></param>
        /// <param name="InputParameterHashkey"></param>
        /// <returns></returns>
        public List<ConductStatisticsOfProvince23BO> CreatePGDConductStatisticsSecondary(IDictionary<string, object> SearchInfo, string InputParameterHashkey, out int FileID)
        {
            int? districtID = Utils.GetInt(SearchInfo, "DistrictID");
            int? provinceID = Utils.GetInt(SearchInfo, "ProvinceID");
            string reportCode = Utils.GetString(SearchInfo, "ReportCode");
            string reportCodeToSearch = SystemParamsInFile.REPORT_THONG_KE_HANH_KIEM_PHONG;
            int? year = Utils.GetInt(SearchInfo, "Year");
            int semester = Utils.GetInt(SearchInfo, "Semester");
            bool? sentToSupervisor = Utils.GetNullableBool(SearchInfo, "SentToSupervisor");
            int? educationLevelID = Utils.GetInt(SearchInfo, "EducationLevelID");
            int? trainningTypeID = Utils.GetInt(SearchInfo, "TrainingTypeID");
            int? SupervisingDeptID = Utils.GetInt(SearchInfo, "SupervisingDeptID");
            int? AcademicYearID = Utils.GetInt(SearchInfo, "AcademicYearID");
            SearchInfo["AppliedLevel"] = SystemParamsInFile.APPLIED_LEVEL_SECONDARY;
            FileID = 0;
            // List<CapacityStatisticsOfProvince23BO> query = this.SearchByProvince23(SearchInfo);
            List<ConductStatisticsOfProvince23BO> lstCapacityStatisticOfProvinceSecondary = this.SearchBySupervisingDept23(SearchInfo);
            lstCapacityStatisticOfProvinceSecondary = lstCapacityStatisticOfProvinceSecondary.OrderBy(o => o.SchoolName).ToList();
            if (lstCapacityStatisticOfProvinceSecondary != null)
            {

                ReportDefinition reportDef = ReportDefinitionBusiness.GetByCode(reportCodeToSearch);
                string templatePath = SystemParamsInFile.TEMPLATE_FOLDER + reportDef.TemplateDirectory + "/" + reportDef.TemplateName;
                IVTWorkbook oBook = VTExport.OpenWorkbook(templatePath);

                //Lấy sheet 
                IVTWorksheet tempSheet = oBook.GetSheet(1);
                IVTWorksheet sheet = oBook.CopySheetToLast(tempSheet, "N10");
                sheet.Name = "THCS_TKHK";

                //fill dữ liệu vào sheet thông tin chung
                SupervisingDept supervisingDept = SupervisingDeptBusiness.Find(SupervisingDeptID);

                string supervisingDeptName = SupervisingDeptBusiness.Find(SupervisingDeptID).SupervisingDeptName.ToUpper();
                string districtName = supervisingDept.District.DistrictName;
                string provinceName = supervisingDept.Province.ProvinceName;
                string trainingName = trainningTypeID > 0 ? TrainingTypeBusiness.Find(trainningTypeID).Resolution : "[Tất cả]";
                string educationName = educationLevelID > 0 ? EducationLevelBusiness.Find(educationLevelID).Resolution : "Khối [Tất cả]";
                string dateTime = "ngày " + DateTime.Now.Date.Day + " tháng " + DateTime.Now.Month + " năm " + DateTime.Now.Year;
                string provinceAndDate = provinceName + ", " + dateTime;
                string semesterName = semester > 0 ? ReportUtils.ConvertSemesterForReportName(semester) : string.Empty;
                string semesterAndAcademic = districtName + " - Hệ " + trainingName + " - " + educationName + " - " + ReportUtils.ConvertSemesterForDisplayTitle(semester) + " - Năm học " + year + " - " + (year + 1);
                sheet.SetCellValue("A2", supervisingDeptName);

                sheet.SetCellValue("G4", provinceAndDate);
                sheet.SetCellValue("B7", semesterAndAcademic);

                int startRow = 10;

                IVTRange range = tempSheet.GetRange("A10", "N10");
                IVTRange lastRange = tempSheet.GetRange("A198", "N198");
                IVTRange rangeTC = tempSheet.GetRange("A200", "N200");

                for (int i = 0; i < lstCapacityStatisticOfProvinceSecondary.Count; i++)
                {
                    ConductStatisticsOfProvince23BO conductBO = lstCapacityStatisticOfProvinceSecondary[i];
                    conductBO.TotalPupil = conductBO.TotalPupil.HasValue ? conductBO.TotalPupil.Value : 0;
                    conductBO.TotalFair = conductBO.TotalFair.HasValue ? conductBO.TotalFair.Value : 0;
                    conductBO.TotalGood = conductBO.TotalGood.HasValue ? conductBO.TotalGood.Value : 0;
                    conductBO.TotalNormal = conductBO.TotalNormal.HasValue ? conductBO.TotalNormal.Value : 0;
                    conductBO.TotalWeak = conductBO.TotalWeak.HasValue ? conductBO.TotalWeak.Value : 0;
                    conductBO.TotalAboveAverage = conductBO.TotalAboveAverage.HasValue ? conductBO.TotalAboveAverage.Value : 0;
                    
                    if (i == lstCapacityStatisticOfProvinceSecondary.Count - 1)
                    {
                        sheet.CopyPasteSameRowHeigh(lastRange, startRow);
                    }
                    else
                    {
                        //Copy row style
                        if (startRow + i > 10)
                        {
                            if ((i + 1) % 5 == 0)
                            {
                                sheet.CopyPasteSameRowHeigh(lastRange, startRow);
                            }
                            else
                                sheet.CopyPasteSameRowHeigh(range, startRow);
                        }
                    }
                    //STT
                    sheet.SetCellValue(startRow, 1, i + 1);

                    //Ten truong
                    sheet.SetCellValue(startRow, 2, conductBO.SchoolName);

                    //Ngay gui
                    sheet.SetCellValue(startRow, 3, conductBO.SentDate.HasValue ? conductBO.SentDate.Value.ToString("dd/MM/yyyy") : string.Empty);

                    //Tong so HS
                    sheet.SetCellValue(startRow, 4, conductBO.TotalPupil);

                    //Tot
                    sheet.SetCellValue(startRow, 5, conductBO.TotalGood);
                    //sheet.SetCellValue(startRow, 6, Math.Round(conductBO.PercentGood.Value, 2, MidpointRounding.AwayFromZero));

                    //Kha
                    sheet.SetCellValue(startRow, 7, conductBO.TotalFair);
                    //sheet.SetCellValue(startRow, 8, Math.Round(conductBO.PercentFair.Value, 2, MidpointRounding.AwayFromZero));

                    //TB
                    sheet.SetCellValue(startRow, 9, conductBO.TotalNormal);
                    //sheet.SetCellValue(startRow, 10, Math.Round(conductBO.PercentNormal.Value, 2, MidpointRounding.AwayFromZero));

                    //Yeu
                    sheet.SetCellValue(startRow, 11, conductBO.TotalWeak);
                    //sheet.SetCellValue(startRow, 12, Math.Round(conductBO.PercentWeak.Value, 2, MidpointRounding.AwayFromZero));

                    //TB tro len
                    sheet.SetCellValue(startRow, 13, conductBO.TotalAboveAverage);
                    //sheet.SetCellValue(startRow, 14, Math.Round(conductBO.PercentAboveAverage.Value, 2, MidpointRounding.AwayFromZero));

                    startRow++;
                }
                sheet.CopyPasteSameRowHeigh(rangeTC, startRow);
                sheet.SetCellValue(startRow, 1, "Tổng cộng");
                sheet.MergeRow(startRow, 1, 3);
                // Thiet lap cong thuc
                string alphabetTS = VTVector.ColumnIntToString(4);
                string fomularTS = "=SUM(#" + (10) + ":" + "#" + (startRow - 1) + ")";
                sheet.SetFormulaValue(startRow, 4, fomularTS.Replace("#", alphabetTS));

                for (int i = 1; i <= 9; i += 2)
                {
                    string alphabet = VTVector.ColumnIntToString(i + 4);
                    string fomular = "=SUM(#" + (10) + ":" + "#" + (startRow - 1) + ")";
                    sheet.SetFormulaValue(startRow, i + 4, fomular.Replace("#", alphabet));
                }
                tempSheet.Delete();

                sentToSupervisor = true;
                //Tạo tên file
                string outputNamePattern = reportDef.OutputNamePattern;
                string khoi = educationLevelID > 0 ? EducationLevelBusiness.Find(educationLevelID).Resolution : "TatCa";
                
                outputNamePattern = outputNamePattern.Replace("_[TrainingType]", (trainningTypeID.HasValue && trainningTypeID.Value > 0 ? "_" + TrainingTypeBusiness.Find(trainningTypeID).Resolution : string.Empty));
                outputNamePattern = outputNamePattern.Replace("[Semester]", semesterName);
                outputNamePattern = outputNamePattern.Replace("_[EducationLevel]", educationLevelID.HasValue && educationLevelID.Value > 0 ? "_" + educationName : string.Empty);
                outputNamePattern = outputNamePattern.Replace("SGD", "PGD");

                IDictionary<string, object> dic = new Dictionary<string, object> {
                {"Semester", semester},
                {"Year", year},
                {"EducationLevelID", educationLevelID},
                {"TrainingTypeID", trainningTypeID},
                {"DistrictID ", districtID },
                {"ProvinceID", provinceID},
                {"SentToSupervisor", true},
                 {"SupervisingDeptID", SupervisingDeptID}
            };
                ProcessedReport pr = InsertSGDConductStatisticsSecondary(reportCodeToSearch, InputParameterHashkey, outputNamePattern, dic, oBook.ToStream());
                FileID = pr.ProcessedReportID;
            }
            return lstCapacityStatisticOfProvinceSecondary;
        }

        /// <summary>
        /// SearchByProvinceGroupBySubCommittee23
        /// </summary>
        /// <param name="SearchInfo">The search info.</param>
        /// <returns>
        /// IQueryable{ConductStatisticsOfProvinceGroupBySubCommittee23}
        /// </returns>
        ///<author>Nam ta</author>
        ///<Date>12/5/2012</Date>
        ///

        public IQueryable<ConductStatisticsOfProvinceGroupBySubCommittee23> SearchByProvinceGroupBySubCommittee23(IDictionary<string, object> SearchInfo)
        {
            int? Year = Utils.GetInt(SearchInfo, "Year");
            int? Semester = Utils.GetInt(SearchInfo, "Semester");
            int? EducationLevelID = Utils.GetInt(SearchInfo, "EducationLevelID");
            int? TrainingTypeID = Utils.GetInt(SearchInfo, "TrainingTypeID");
            int? SubjectID = Utils.GetInt(SearchInfo, "SubjectID");
            bool? SentToSupervisor = Utils.GetBool(SearchInfo, "SentToSupervisor");
            int? ProvinceID = Utils.GetInt(SearchInfo, "ProvinceID");
            int? DistrictID = Utils.GetInt(SearchInfo, "DistrictID");
            int AppliedLevel = Utils.GetInt(SearchInfo, "AppliedLevel");
            int? SubCommitteeID = Utils.GetInt(SearchInfo, "SubCommitteeID");
            int? supervisingDeptID = Utils.GetInt(SearchInfo, "SupervisingDeptID");
            bool IsSubSuperVisingDeptRole = Utils.GetBool(SearchInfo, "IsSubSuperVisingDeptRole");
            bool IsSuperVisingDeptRole = Utils.GetBool(SearchInfo, "IsSuperVisingDeptRole");
            SupervisingDept su = SupervisingDeptBusiness.Find(supervisingDeptID);
            if (supervisingDeptID.HasValue && supervisingDeptID > 0)
            {
                if (su.HierachyLevel == SystemParamsInFile.EDUCATION_HIERACHY_LEVEL_PROVINCE_OFFICE_DEPARTMENT || su.HierachyLevel == SystemParamsInFile.EDUCATION_HIERACHY_LEVEL_DISTRICT_OFFICE_DEPARTMENT)
                {
                    supervisingDeptID = su.ParentID.Value;
                }
            }
            string ReportCode = "";
            if (AppliedLevel == 2)
            {
                ReportCode = SystemParamsInFile.THONGKEHANHKIEMTHEOBANCAP2;
            }
            else if (AppliedLevel == 3)
            {
                ReportCode = SystemParamsInFile.THONGKEHANHKIEMTHEOBANCAP3;
            }
            int grade = Utils.GradeToBinary(AppliedLevel);


            IQueryable<ConductStatistic> iqConductStatistic = this.All.Where(o => o.Semester == Semester && o.ReportCode == ReportCode && o.AppliedLevel == AppliedLevel && o.Year == Year);
            var query = from cs in iqConductStatistic
                        join s in SchoolProfileRepository.All on cs.SchoolID equals s.SchoolProfileID
                        join sd in SupervisingDeptBusiness.All on s.SupervisingDeptID equals sd.SupervisingDeptID
                        where (sd.SupervisingDeptID == supervisingDeptID || sd.ParentID == supervisingDeptID) && s.IsActive == true && (s.EducationGrade & grade) != 0
                        where s.IsActive == true
                        && (cs.SentDate == (from p in iqConductStatistic where cs.SchoolID == p.SchoolID && cs.EducationLevelID == p.EducationLevelID && cs.SubCommitteeID == p.SubCommitteeID select p.SentDate).Max())
                        select cs;

            //var lstConductStatistics = from Cs in this.All
            //                            join sp in listSchoolProfile on Cs.SchoolID equals sp.SchoolProfileID
            //                            select Cs;

            //if (ReportCode != null)
            //    lstConductStatistics = lstConductStatistics.Where(o => o.ReportCode == ReportCode);
            //if (Year.HasValue && Year != 0)
            //    lstConductStatistics = lstConductStatistics.Where(o => o.Year == Year.Value);
            //if (Semester.HasValue && Semester != 0)
            //    lstConductStatistics = lstConductStatistics.Where(o => o.Semester == Semester.Value);
            //if (SentToSupervisor.HasValue)
            //    lstConductStatistics = lstConductStatistics.Where(o => o.SentToSupervisor == SentToSupervisor.Value);
            //if (EducationLevelID.HasValue && EducationLevelID != 0)
            //    lstConductStatistics = lstConductStatistics.Where(o => o.EducationLevelID == EducationLevelID.Value);
            //if (SubCommitteeID.HasValue && EducationLevelID != 0)
            //    lstConductStatistics = lstConductStatistics.Where(o => o.SubCommitteeID == SubCommitteeID.Value);
            //if (TrainingTypeID.HasValue && EducationLevelID != 0)
            //    lstConductStatistics = lstConductStatistics.Where(o => o.SchoolProfile.TrainingTypeID == TrainingTypeID.Value);

            //ConductStatisticsOfProvinceGroupBySubCommittee23 item = new ConductStatisticsOfProvinceGroupBySubCommittee23();

            //List<ConductStatisticsOfProvinceGroupBySubCommittee23> lstConductStatisticOfProvince23 = new List<ConductStatisticsOfProvinceGroupBySubCommittee23>();
            //var query = from lsch in listSchoolProfile
            //            join lcss in lstConductStatistics on lsch.SchoolProfileID equals lcss.SchoolID
            //            select lcss;
            var lstConductStatisticOfSchool23 = query.GroupBy(u => new { u.EducationLevelID, u.SubCommitteeID, SubCommitteeTitle = u.SubCommittee.Resolution, EducationLevelName = u.EducationLevel.Resolution }).Select(u => new ConductStatisticsOfProvinceGroupBySubCommittee23
            {
                EducationLevel = u.Key.EducationLevelID.Value,
                SubCommittee = u.Key.SubCommitteeID.Value,
                SubCommitteeTitle = u.Key.SubCommitteeTitle,
                EducationLevelName = u.Key.EducationLevelName,
                TotalExcellent = u.Sum(v => v.ConductLevel01),
                TotalGood = u.Sum(v => v.ConductLevel02),
                TotalNormal = u.Sum(v => v.ConductLevel03),
                TotalWeak = u.Sum(v => v.ConductLevel04),
                TotalPupil = u.Sum(v => v.PupilTotal),
            }).OrderBy(u => u.EducationLevel).ThenBy(u => u.SubCommitteeTitle).ToList();

            if (lstConductStatisticOfSchool23 != null && lstConductStatisticOfSchool23.Count > 0)
            {
                foreach (var obj in lstConductStatisticOfSchool23)
                {
                    //obj.TotalPupil = obj.TotalExcellent + obj.TotalGood + obj.TotalNormal + obj.TotalWeak;
                    obj.PercentExcellent = Math.Round((((double)obj.TotalExcellent / (double)obj.TotalPupil) * 100), 2);
                    obj.PercentGood = Math.Round(((double)obj.TotalGood / (double)obj.TotalPupil) * 100, 2);
                    obj.PercentNormal = Math.Round(((double)obj.TotalNormal / (double)obj.TotalPupil) * 100, 2);
                    obj.PercentWeak = Math.Round(((double)obj.TotalWeak / (double)obj.TotalPupil) * 100, 2);
                    obj.TotalAboveAverage = obj.TotalExcellent + obj.TotalGood + obj.TotalNormal;
                    obj.PercentAboveAverage = Math.Round(((double)obj.TotalAboveAverage / (double)obj.TotalPupil) * 100, 2);

                }
            }

            return lstConductStatisticOfSchool23.AsQueryable();
        }

        /// <summary>
        /// CreateSGDConductStatisticsBySubCommitteeTertiary
        /// </summary>
        /// <param name="SearchInfo">The search info.</param>
        /// <param name="InputParameterHashKey">The input parameter hash key.</param>
        /// <returns>
        /// List{ConductStatisticsOfProvinceGroupBySubCommittee23}
        /// </returns>
        ///<author>Nam ta </author>
        ///<Date>12/5/2012</Date>
        public List<ConductStatisticsOfProvinceGroupBySubCommittee23> CreateSGDConductStatisticsBySubCommitteeTertiary(IDictionary<string, object> SearchInfo, String InputParameterHashKey, out int ProcessedReportID)
        {
            System.Threading.Thread.CurrentThread.CurrentCulture = System.Globalization.CultureInfo.CreateSpecificCulture("en-US");
            int? Year = Utils.GetInt(SearchInfo, "Year");
            int? Semester = Utils.GetInt(SearchInfo, "Semester");
            int? EducationLevelID = Utils.GetInt(SearchInfo, "EducationLevelID");
            int? TrainingTypeID = Utils.GetInt(SearchInfo, "TrainingTypeID");
            int? SubjectID = Utils.GetInt(SearchInfo, "SubjectID");
            string ReportCode = Utils.GetString(SearchInfo, "ReportCode");
            bool? SentToSupervisor = Utils.GetBool(SearchInfo, "SentToSupervisor");
            int? ProvinceID = Utils.GetInt(SearchInfo, "ProvinceID");
            int? DistrictID = Utils.GetInt(SearchInfo, "DistrictID");
            int? SubCommitteeID = Utils.GetInt(SearchInfo, "SubCommitteeID");
            int? SupervisingDeptID = Utils.GetInt(SearchInfo, "SupervisingDeptID");
            bool IsSubSuperVisingDeptRole = Utils.GetBool(SearchInfo, "IsSubSuperVisingDeptRole");
            bool IsSuperVisingDeptRole = Utils.GetBool(SearchInfo, "IsSuperVisingDeptRole");
            SearchInfo["AppliedLevel"] = SystemParamsInFile.APPLIED_LEVEL_TERTIARY;
            var lstConductStatisticOfProvinceBySubCommitteeTertiary = this.SearchByProvinceGroupBySubCommittee23(SearchInfo);
            ProcessedReportID = 0;
            if (lstConductStatisticOfProvinceBySubCommitteeTertiary != null && lstConductStatisticOfProvinceBySubCommitteeTertiary.Count() > 0)
            {

                string reportCode = SystemParamsInFile.REPORT_SGD_THPT_THONGKEHANHKIEMTHEOBAN;

                ReportDefinition reportDef = ReportDefinitionBusiness.GetByCode(reportCode);

                string templatePath = SystemParamsInFile.TEMPLATE_FOLDER + reportDef.TemplateDirectory + "/" + reportDef.TemplateName;
                IVTWorkbook oBook = VTExport.OpenWorkbook(templatePath);

                //L?y sheet template
                IVTWorksheet firstSheet = oBook.GetSheet(1);
                IVTWorksheet sheet = oBook.CopySheetToLast(firstSheet);

                SupervisingDept SupervisingDept = SupervisingDeptBusiness.Find(SupervisingDeptID);
                //tieu de ten so giao duc

                sheet.SetCellValue("A2", SupervisingDept.SupervisingDeptName.ToUpper());
                string semeterString = "";
                //tieu de nam hoc
                if (Semester == GlobalConstants.SEMESTER_OF_YEAR_FIRST)
                {
                    semeterString = "Học Kỳ I";
                }
                else if (Semester == GlobalConstants.SEMESTER_OF_YEAR_SECOND)
                {
                    semeterString = "Học Kỳ II";
                }
                else
                {
                    semeterString = "Cả năm";
                }
                sheet.SetCellValue("A7", semeterString + " Năm học " + Year.ToString() + " - " + (Year + 1).ToString());

                //Tieu de th?i gian
                if (ProvinceID.HasValue)
                {
                    Province Province = ProvinceBusiness.Find(ProvinceID);
                    sheet.SetCellValue("I4", Province.ProvinceName + ", ngày " + DateTime.Now.Day + " tháng " + DateTime.Now.Month + " năm " + DateTime.Now.Year);
                }

                //IEnumerable<int> GroupEducationLevel = lstConductStatisticOfProvinceBySubCommitteeTertiary.Select(u => u.EducationLevel.Value);

                int rowStart = 11;

                IVTRange RowCopy = firstSheet.GetRange("A12", "O12");
                IVTRange RowSumEducationLevel = firstSheet.GetRange("A14", "O14");
                IVTRange RowSumProvince = firstSheet.GetRange("A16", "O16");
                //xoa  dong template
                for (int i = 11; i < 17; i++)
                {
                    sheet.DeleteRow(i);
                }
                int rowMerger = rowStart;
                int beginRow = 11;
                int firstEdu = 11;
                List<EducationLevel> lstEdu = EducationLevelBusiness.All.Where(o => o.Grade == SystemParamsInFile.APPLIED_LEVEL_TERTIARY && o.IsActive == true).ToList();
                foreach (var edu in lstEdu)
                {
                    IQueryable<ConductStatisticsOfProvinceGroupBySubCommittee23> lst = lstConductStatisticOfProvinceBySubCommitteeTertiary.Where(o => o.EducationLevel == edu.EducationLevelID);
                    foreach (var item in lst)
                    {
                        sheet.CopyPasteSameSize(RowCopy, rowStart, 1);
                        sheet.SetCellValue(rowStart, 2, item.SubCommitteeTitle);
                        sheet.SetCellValue(rowStart, 3, item.TotalPupil.Value);
                        sheet.SetCellValue(rowStart, 4, item.TotalExcellent.Value);
                        sheet.SetCellValue(rowStart, 6, item.TotalGood.Value);
                        sheet.SetCellValue(rowStart, 8, item.TotalNormal.Value);
                        sheet.SetCellValue(rowStart, 10, item.TotalWeak.Value);
                        sheet.SetCellValue(rowStart, 12, item.TotalAboveAverage.Value);
                        rowStart++;
                    }
                    int count = lst.Count();
                    sheet.CopyPasteSameSize(RowSumEducationLevel, rowStart, 1);
                    IVTRange RowMerger = sheet.GetRange(rowMerger, 1, rowStart, 1);
                    RowMerger.Merge();
                    sheet.SetCellValue(rowMerger, 1, edu.Resolution);
                    rowMerger = rowStart + 1;
                    int endrow = rowMerger - 2;
                    sheet.SetCellValue(rowStart, 2, "Toàn khối " + edu.Resolution);

                    string alphabetTS = VTVector.ColumnIntToString(3);
                    string fomularTS = "=SUM(#" + (firstEdu) + ":" + "#" + (endrow) + ")";
                    sheet.SetFormulaValue(rowStart, 3, fomularTS.Replace("#", alphabetTS));

                    for (int i = 1; i <= 9; i += 2)
                    {
                        string alphabet = VTVector.ColumnIntToString(i + 3);
                        string fomular = "=SUM(#" + (firstEdu) + ":" + "#" + (endrow) + ")";
                        sheet.SetFormulaValue(rowStart, i + 3, fomular.Replace("#", alphabet));
                    }
                    firstEdu = firstEdu + count + 1;
                    rowStart++;

                }
                sheet.CopyPasteSameSize(RowSumProvince, rowStart, 1);
                //Cột tổng số toàn bộ các khối
                string alphabetTST = VTVector.ColumnIntToString(3);
                string fomularTST = "=SUM(#" + (beginRow) + ":" + "#" + (rowStart - 1) + ")/2";
                sheet.SetFormulaValue(rowStart, 3, fomularTST.Replace("#", alphabetTST));
                for (int i = 1; i <= 9; i += 2)
                {
                    string alphabet = VTVector.ColumnIntToString(i + 3);
                    string fomular = "=SUM(#" + (beginRow) + ":" + "#" + (rowStart - 1) + ")/2";
                    sheet.SetFormulaValue(rowStart, i + 3, fomular.Replace("#", alphabet));
                }

                sheet.Name = "TKHKTheoBan";


                //ket thuc file chuyen sang luu vao csdl
                //-	ReportCode = IDictionary[“ReportCode”]
                //-	ProcessedDate = Ngày hi?n t?i
                //-	InputParameterHashKey = InputParameterHashKey
                //-	ReportData : ReportUtils.Compress(Data)
                //-	SentToSupervisor = 1
                //Map tuong ?ng vào d?i tu?ng ProcessedReport r?i g?i hàm ProcessedReportBusiness.Insert(ProcessedReport)

                //Xoa sheet dau
                firstSheet.Delete();

                CapacityConductReport ConductConductReport = new CapacityConductReport();
                ConductConductReport.DistrictID = DistrictID.Value;
                ConductConductReport.EducationLevelID = EducationLevelID.Value;
                ConductConductReport.ProvinceID = ProvinceID.Value;
                ConductConductReport.Semester = Semester.Value;
                ConductConductReport.SubcommitteeID = SubCommitteeID.Value;
                ConductConductReport.SupervisingDeptID = SupervisingDeptID.Value;
                ConductConductReport.TrainingTypeID = TrainingTypeID.Value;
                ConductConductReport.Year = Year.Value;


                ProcessedReport ProcessedReport = new ProcessedReport();
                ProcessedReport.ReportCode = reportCode;
                ProcessedReport.ProcessedDate = DateTime.Now;
                ProcessedReport.InputParameterHashKey = CapacityStatisticBusiness.GetHashKeyForConductCapacityStatistics(ConductConductReport);
                ProcessedReport.ReportData = ReportUtils.Compress(oBook.ToStream());
                ProcessedReport.SentToSupervisor = true;

                //T?o tên file HS_[SchoolLevel]_BangDiem_[Class]_[Semester]_[Subject]
                string outputNamePattern = reportDef.OutputNamePattern;
                string semester = ReportUtils.ConvertSemesterForReportName(Semester.Value);
                outputNamePattern = outputNamePattern.Replace("[Semester]", ReportUtils.StripVNSign(semester));
                ProcessedReport.ReportName = ReportUtils.RemoveSpecialCharacters(outputNamePattern) + "." + reportDef.OutputFormat;

                IDictionary<string, object> dic = new Dictionary<string, object> {
                {"Year", Year},
                {"Semester", Semester},
                {"EducationLevelID ", EducationLevelID},
                {"TrainingTypeID", TrainingTypeID},
                {"SubcommitteeID", SubCommitteeID},
                {"SubjectID", SubjectID},
                {"ProvinceID", ProvinceID},
                {"DistrictID", DistrictID},
                {"SupervisingDeptID", SupervisingDeptID}
            };
                ProcessedReportParameterRepository.Insert(dic, ProcessedReport);
                ProcessedReportRepository.Insert(ProcessedReport);
                ProcessedReportRepository.Save();

                //luu report vao csdl

                ProcessedReportID = ProcessedReport.ProcessedReportID;
                //luu vao bang ProcessedReportParameter 

                return lstConductStatisticOfProvinceBySubCommitteeTertiary.ToList();
            }
            else return null;


        }

        /// <summary>
        /// <author>PhuongH</author>
        /// <date>7/12/2012</date>
        /// </summary>
        /// <param name="dic"></param>
        /// <returns></returns>

        public List<ConductStatisticsOfProvinceGroupByDistrict23BO> CreateSGDConductStatisticsByDistrictTertiary(IDictionary<string, object> dic, out int FileID, string inputHash)
        {
            System.Threading.Thread.CurrentThread.CurrentCulture = System.Globalization.CultureInfo.CreateSpecificCulture("en-US");
            string ReportCode = Utils.GetString(dic, "ReportCode");
            int Year = Utils.GetInt(dic, "Year");
            int Semester = Utils.GetInt(dic, "Semester");
            int EducationLevelID = Utils.GetInt(dic, "EducationLevelID");
            int TrainingTypeID = Utils.GetInt(dic, "TrainingTypeID");
            int SubcommitteeID = Utils.GetInt(dic, "SubcommitteeID");
            int SubjectID = Utils.GetInt(dic, "SubjectID");
            int ProvinceID = Utils.GetInt(dic, "ProvinceID");
            int DistrictID = Utils.GetInt(dic, "DistrictID");
            int SupervisingDeptID = Utils.GetInt(dic, "SupervisingDeptID");
            int AppliedLevel = Utils.GetInt(dic, "SupervisingDeptID");
            bool IsSubSuperVisingDeptRole = Utils.GetBool(dic, "IsSubSuperVisingDeptRole");
            bool IsSuperVisingDeptRole = Utils.GetBool(dic, "IsSuperVisingDeptRole");

            List<ConductStatisticsOfProvinceGroupByDistrict23BO> listConductStatistic = this.SearchByProvinceGroupByDistrict23(dic).ToList();

            if (listConductStatistic == null)
            {
                FileID = 0;
                return null;
            }
            ReportDefinition reportDef = ReportDefinitionBusiness.GetByCode(SystemParamsInFile.REPORT_SGD_THPT_THONGKEHANHKIEMTHEOQUANHUYEN);
            string templatePath = ReportUtils.GetTemplatePath(reportDef);
            Stream data = null;
            IVTWorkbook oBook = VTExport.OpenWorkbook(templatePath);
            //Lấy sheet template
            IVTWorksheet firstSheet = oBook.GetSheet(1);

            //Tạo dữ liệu chung
            SupervisingDept supervisingDept = SupervisingDeptBusiness.Find(SupervisingDeptID);
            string supervisingDeptName = supervisingDept.SupervisingDeptName.ToUpper();
            string convertSemester = SMASConvert.ConvertSemester(Semester);
            DateTime reportDate = DateTime.Now;
            string provinceName = supervisingDept.Province.ProvinceName;
            string academicYearTitle = Year + " - " + (Year + 1);

            int firstRow = 11;
            int beginRow = firstRow + 1;
            int endRow = 0;
            IVTWorksheet sheet = oBook.CopySheetToLast(firstSheet, "M" + firstRow);
            sheet.Name = "ThongKeHanhKiemTheoQuanHuyen";
            //Danh sách học sinh chuyển lớp

            //Template Dòng đầu tiên
            IVTRange topRow = firstSheet.GetRange("A12", "M12");
            //Template dòng giữa
            IVTRange middleRow = firstSheet.GetRange("A13", "M13");
            //Template dòng cuối
            IVTRange bottomRow = firstSheet.GetRange("A14", "M14");
            //Merge row
            IVTRange lastRow = firstSheet.GetRange("A15", "M15");
            //Template để mergeRow người lập báo cáo

            int currentRow = 0;

            List<object> list = new List<object>();
            int order = 1;
            Dictionary<string, object> dic2 = new Dictionary<string, object> 
                {
                    {"SupervisingDeptName", supervisingDeptName}, 
                    {"Semester", convertSemester},
                    {"ProvinceName", provinceName},                  
                    {"ReportDate", reportDate},
                    {"AcademicYearTitle", academicYearTitle}
                };
            foreach (ConductStatisticsOfProvinceGroupByDistrict23BO conductStatis in listConductStatistic)
            {
                currentRow = firstRow + order;

                if (order == 1)
                {
                    sheet.CopyPasteSameRowHeigh(topRow, currentRow);
                }
                else if (order % 5 == 0 || order == listConductStatistic.Count())
                {
                    sheet.CopyPasteSameRowHeigh(bottomRow, currentRow);
                }

                else
                {
                    sheet.CopyPasteSameRowHeigh(middleRow, currentRow);
                }

                list.Add(new Dictionary<string, object> {
                    { "Index", order++},
                    {  "DistrictName", conductStatis.DistrictName },
                    {  "TotalPupil", conductStatis.TotalPupil},
                    {  "TotalGood", conductStatis.TotalGood },
                    {  "TotalFair", conductStatis.TotalFair },
                    {  "TotalNormal", conductStatis.TotalNormal },
                    {  "TotalWeak", conductStatis.TotalWeak },
                    {  "PercentGood", conductStatis.PercentGood },
                    {  "PercentNormal", conductStatis.PercentNormal },
                    {  "PercentWeak", conductStatis.PercentWeak },
                    {  "PercentFair", conductStatis.PercentFair },
                    {  "TotalAboveAverage", conductStatis.TotalAboveAverage },
                    {  "PercentAboveAverage" , conductStatis.PercentAboveAverage}
                });
            }
            dic2.Add("list", list);
            sheet.FillVariableValue(dic2);
            endRow = currentRow + 1;
            sheet.CopyPasteSameRowHeigh(lastRow, endRow);
            for (int i = 1; i < 10; i += 2)
            {
                string alphabet = VTVector.ColumnIntToString(i + 3);
                string fomular = "=SUM(#" + (beginRow) + ":" + "#" + (endRow - 1) + ")";
                sheet.SetFormulaValue(endRow, i + 3, fomular.Replace("#", alphabet));
            }
            string fomularSum = "=SUM(C" + (beginRow) + ":" + "C" + (endRow - 1) + ")";
            sheet.SetFormulaValue(endRow, 3, fomularSum);
            //Fill dữ liệu
            firstSheet.Delete();
            data = oBook.ToStream();

            ProcessedReport processedReport = new ProcessedReport();
            processedReport.ReportCode = SystemParamsInFile.REPORT_SGD_THPT_THONGKEHANHKIEMTHEOQUANHUYEN;
            string sSemester = Semester == 1 ? "HKI" : Semester == 2 ? "HKII" : "CN";
            processedReport.ReportName = SystemParamsInFile.REPORT_SGD_THPT_THONGKEHANHKIEMTHEOQUANHUYEN + "_" + sSemester + "." + reportDef.OutputFormat;
            processedReport.ProcessedDate = DateTime.Now;
            processedReport.InputParameterHashKey = inputHash;
            processedReport.ReportData = ReportUtils.Compress(data);
            processedReport.SentToSupervisor = true;
            ProcessedReportParameterRepository.Insert(dic, processedReport);
            ProcessedReportRepository.Insert(processedReport);
            ProcessedReportRepository.Save();
            FileID = processedReport.ProcessedReportID;
            return listConductStatistic;
        }



        public IQueryable<ConductStatistic> Search(IDictionary<string, object> SearchInfo)
        {

            int SchoolID = Utils.GetInt(SearchInfo, "SchoolID");
            int AcademicYearID = Utils.GetInt(SearchInfo, "AcademicYearID");
            int Year = Utils.GetInt(SearchInfo, "Year");
            string ReportCode = Utils.GetString(SearchInfo, "ReportCode");
            int Semester = Utils.GetInt(SearchInfo, "Semester");
            int EducationLevelID = Utils.GetInt(SearchInfo, "EducationLevelID");
            int SubCommitteeID = Utils.GetInt(SearchInfo, "SubCommitteeID");
            int AppliedLevel = Utils.GetInt(SearchInfo, "AppliedLevel");
            bool SentToSupervisor = Utils.GetBool(SearchInfo, "SentToSupervisor");

            IQueryable<ConductStatistic> lstConductStatistic = ConductStatisticBusiness.All;

            if (SchoolID != 0)
            {
                lstConductStatistic = lstConductStatistic.Where(o => o.SchoolID == SchoolID);
            }
            if (AcademicYearID != 0)
            {
                lstConductStatistic = lstConductStatistic.Where(o => o.AcademicYearID == AcademicYearID);
            }
            if (Year != 0)
            {
                lstConductStatistic = lstConductStatistic.Where(o => o.Year == Year);
            }
            if (ReportCode.Length != 0)
            {
                lstConductStatistic = lstConductStatistic.Where(o => o.ReportCode.ToLower().Contains(ReportCode.ToLower()));
            }
            if (Semester != 0)
            {
                lstConductStatistic = lstConductStatistic.Where(o => o.Semester == Semester);
            }
            if (EducationLevelID != 0)
            {
                lstConductStatistic = lstConductStatistic.Where(o => o.EducationLevelID == EducationLevelID);
            }
            if (SubCommitteeID != 0)
            {
                lstConductStatistic = lstConductStatistic.Where(o => o.SubCommitteeID == SubCommitteeID);
            }
            if (AppliedLevel != 0)
            {
                lstConductStatistic = lstConductStatistic.Where(o => o.AppliedLevel == AppliedLevel);
            }
            if (SentToSupervisor != false)
            {
                lstConductStatistic = lstConductStatistic.Where(o => o.SentToSupervisor == SentToSupervisor);
            }

            IQueryable<ConductStatistic> lstConductStatistic1 = (from o in lstConductStatistic
                                                                 where o.SentDate == (from c in lstConductStatistic
                                                                                      where o.SchoolID == c.SchoolID && (o.Year == c.Year || Year == 0) && o.EducationLevelID == c.EducationLevelID && (o.SubCommitteeID == c.SubCommitteeID || SubCommitteeID == 0)
                                                                                        && o.Semester == c.Semester && o.ReportCode == c.ReportCode && o.AppliedLevel == c.AppliedLevel
                                                                                      select c.SentDate).Max()
                                                                 select o);

            return lstConductStatistic1;

        }


        #endregion

        #region Moi
        public List<ConductStatisticsBO> GetListConductAllSchool(string reportCode, int yearId, int semester, int educationLevelId, int provinceID, int supervisingDeptId, int appliedLevelId
            , bool IsSuperVisingDeptRole, int districtId)
        {

            IQueryable<ConductStatisticsBO> listConduct = (from a in ConductStatisticBusiness.All.AsNoTracking()
                                                           where a.SupervisingDeptID == supervisingDeptId
                                                           && a.ReportCode.Equals(reportCode)
                                                           && a.Year == yearId
                                                           && a.Semester == semester
                                                           && a.AppliedLevel == appliedLevelId
                                                           && ((educationLevelId > 0 && a.EducationLevelID == educationLevelId) || educationLevelId == 0)
                                                           select new ConductStatisticsBO
                                                             {
                                                                 PupilTotal = a.PupilTotal,
                                                                 ConductLevel01 = a.ConductLevel01,
                                                                 ConductLevel02 = a.ConductLevel02,
                                                                 ConductLevel03 = a.ConductLevel03,
                                                                 ConductLevel04 = a.ConductLevel04,
                                                                 OnAverage = a.OnAverage,
                                                                 SchoolName = a.SchoolProfile.SchoolName,
                                                                 SchoolID = a.SchoolID,
                                                                 DistrictName = a.SchoolProfile.District.DistrictName,
                                                                 DistrictID = a.DistrictID,
                                                                 Semester = a.Semester,
                                                                 EducationLevelID = a.EducationLevelID,
                                                                 IsEthnic = a.IsEthnic,
                                                                 IsFemale = a.IsFemale
                                                             }).OrderBy(p => p.DistrictName).ThenBy(p => p.SchoolName);

            return listConduct.ToList();
        }

        public List<ConductStatisticsBO> GetListConductAllEdu(string reportCode, int yearId, int semester, int educationLevelId, int provinceID, int supervisingDeptId, int appliedLevelId
            , bool IsSuperVisingDeptRole, int districtId)
        {
            List<ConductStatisticsBO> listResult = new List<ConductStatisticsBO>();
            List<ConductStatisticsBO> listDataAllSchool = this.GetListConductAllSchool(reportCode, yearId, semester, 0, provinceID, supervisingDeptId, appliedLevelId, IsSuperVisingDeptRole, districtId);
            if (listDataAllSchool != null && listDataAllSchool.Count > 0)
            {
                listResult = listDataAllSchool.GroupBy(n => new { n.EducationLevelID, n.IsFemale, n.IsEthnic }).
                                     Select(group =>
                                         new ConductStatisticsBO
                                         {
                                             EducationLevelID = group.Key.EducationLevelID,
                                             IsFemale = group.Key.IsFemale,
                                             IsEthnic = group.Key.IsEthnic,
                                             PupilTotal = group.Sum(p => p.PupilTotal),
                                             ConductLevel01 = group.Sum(p => p.ConductLevel01),
                                             ConductLevel02 = group.Sum(p => p.ConductLevel02),
                                             ConductLevel03 = group.Sum(p => p.ConductLevel03),
                                             ConductLevel04 = group.Sum(p => p.ConductLevel04),
                                             OnAverage = group.Sum(p => p.OnAverage)
                                         }).ToList();
            }
            return listResult;
        }

        public List<ConductStatisticsBO> GetListConductDistrict(string reportCode, int yearId, int semester, int educationLevelId, int provinceID, int supervisingDeptId, int appliedLevelId
            , bool IsSuperVisingDeptRole, int districtId)
        {
            List<ConductStatisticsBO> listResult = new List<ConductStatisticsBO>();
            List<ConductStatisticsBO> listDataAllSchool = this.GetListConductAllSchool(reportCode, yearId, semester, 0, provinceID, supervisingDeptId, appliedLevelId, IsSuperVisingDeptRole,districtId);
            if (listDataAllSchool != null && listDataAllSchool.Count > 0)
            {
                listResult = listDataAllSchool.GroupBy(p => new { p.DistrictID, p.DistrictName, p.IsFemale, p.IsEthnic }).
                    Select(group => new ConductStatisticsBO
                    {
                        DistrictID = group.Key.DistrictID,
                        DistrictName = group.Key.DistrictName,
                        IsFemale = group.Key.IsFemale,
                        IsEthnic = group.Key.IsEthnic,
                        PupilTotal = group.Sum(p => p.PupilTotal),
                        ConductLevel01 = group.Sum(p => p.ConductLevel01),
                        ConductLevel02 = group.Sum(p => p.ConductLevel02),
                        ConductLevel03 = group.Sum(p => p.ConductLevel03),
                        ConductLevel04 = group.Sum(p => p.ConductLevel04),
                        OnAverage = group.Sum(p => p.OnAverage)
                    }).OrderBy(p => p.DistrictName).ToList();
            }

            return listResult;
        }

       



        #endregion

    }
}
