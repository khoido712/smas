﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SMAS.Business.Business;
using SMAS.Models.Models;
using SMAS.VTUtils.Excel.Export;
using SMAS.Business.BusinessObject;
using System.IO;
using SMAS.Business.Common;
using SMAS.DAL.IRepository;
using log4net;
using SMAS.DAL.Repository;
using SMAS.Business.IBusiness;
using System.Text.RegularExpressions;
using SMAS.VTUtils.HtmlExport;
using SMAS.VTUtils.Log;

namespace SMAS.Business.Business
{
    /// <summary>
    /// 
    /// </summary>
    /// <author>AuNH</author>
    /// <date>11/29/2012</date>
    public partial class InputStatisticsBusiness
    {
        public string GroupByDistrictStatistics(int UserAccountID, int SupervisingDeptID, int DistrictID, int Year, DateTime ReportDate)
        {
            ReportDefinition reportDef = ReportDefinitionBusiness.GetByCode("TinhHinhNhapLieuTaiCacPhongGD");
            string templatePath = ReportUtils.GetTemplatePath(reportDef);
            templatePath = templatePath.Replace(".xls", ".html");

            VTHtmlExport HtmlExport = new VTHtmlExport(templatePath);

            List<ReportData> ListData = GroupByDistrictStatisticsGetData(UserAccountID, SupervisingDeptID, DistrictID, Year, ReportDate);

            int index = 1;
            Dictionary<string, object> TempData = new Dictionary<string, object>();
            foreach (ReportData ReportData in ListData)
            {
                int i = 0;
                TempData["STT"] = index;
                TempData["DistrictName"] = ReportData.DistrictName;
                foreach (InputStatistics InputStatistics in ReportData.ListInputStatistics)
                {
                    TempData["InputStatistics"] = InputStatistics;

                    if (i == 0)
                    {
                        HtmlExport.AppendToPart("Main", HtmlExport.FillVariableDataForPart("Row1", TempData, false));
                    }
                    else
                    {
                        HtmlExport.AppendToPart("Main", HtmlExport.FillVariableDataForPart("Row2", TempData, false));
                    }
                    i++;
                }
                index++;
            }

            return HtmlExport.CreateHtmlOutput(new Dictionary<string, object>());
        }

        public ProcessedReport ExportGroupByDistrictStatistics(int UserAccountID, int SupervisingDeptID, int DistrictID, int Year, DateTime ReportDate)
        {
            ReportDefinition reportDef = ReportDefinitionBusiness.GetByCode("TinhHinhNhapLieuTaiCacPhongGD");
            string templatePath = ReportUtils.GetTemplatePath(reportDef);
            List<ReportData> ListData = GroupByDistrictStatisticsGetData(UserAccountID, SupervisingDeptID, DistrictID, Year, ReportDate);

            SupervisingDept Dept = SupervisingDeptBusiness.Find(SupervisingDeptID);

            Stream data = WriteToExcel(templatePath, ListData, Dept.Province.ProvinceName, Year + " - " + (Year + 1));

            return InsertProcessedReport("TinhHinhNhapLieuTaiCacPhongGD", UserAccountID, SupervisingDeptID, DistrictID, Year, ReportDate, data);
        }

        public string SchoolStatistics(int UserAccountID, int SupervisingDeptID, int DistrictID, int Year, DateTime ReportDate)
        {
            ReportDefinition reportDef = ReportDefinitionBusiness.GetByCode("TinhHinhNhapLieuTheoQuanTaiTruong");
            string templatePath = ReportUtils.GetTemplatePath(reportDef);
            templatePath = templatePath.Replace(".xls", ".html");

            VTHtmlExport HtmlExport = new VTHtmlExport(templatePath);

            List<ReportData> ListData = SchoolStatisticsGetData(UserAccountID, SupervisingDeptID, DistrictID, Year, ReportDate);

            int index = 1;
            Dictionary<string, object> TempData = new Dictionary<string, object>();
            foreach (ReportData ReportData in ListData)
            {
                int i = 0;
                TempData["STT"] = index;
                TempData["District"] = ReportData;
                try
                {
                    foreach (InputStatistics InputStatistics in ReportData.ListInputStatistics)
                    {
                        TempData["InputStatistics"] = InputStatistics;

                        if (i == 0)
                        {
                            HtmlExport.AppendToPart("Main", HtmlExport.FillVariableDataForPart("Row1", TempData, false));
                        }
                        else
                        {
                            HtmlExport.AppendToPart("Main", HtmlExport.FillVariableDataForPart("Row2", TempData, false));
                        }
                        i++;
                    }
                }
                catch (Exception ex)
                {
                    
                    String ParamList = string.Format("UserAccountID={0}, SupervisingDeptID={1}, DistrictID={2}, Year={3}, ReportDate={4}", UserAccountID, SupervisingDeptID, DistrictID, Year, ReportDate);
                    LogExtensions.ErrorExt(logger, DateTime.Now, "SchoolStatistics",ParamList, ex);
                }
                index++;
            }

            return HtmlExport.CreateHtmlOutput(new Dictionary<string, object>());
        }

        public ProcessedReport ExportSchoolStatistics(int UserAccountID, int SupervisingDeptID, int DistrictID, int Year, DateTime ReportDate)
        {
            ReportDefinition reportDef = ReportDefinitionBusiness.GetByCode("TinhHinhNhapLieuTheoQuanTaiTruong");
            string templatePath = ReportUtils.GetTemplatePath(reportDef);
            List<ReportData> ListData = SchoolStatisticsGetData(UserAccountID, SupervisingDeptID, DistrictID, Year, ReportDate);

            SupervisingDept Dept = SupervisingDeptBusiness.Find(SupervisingDeptID);

            Stream data = WriteToExcel(templatePath, ListData, Dept.Province.ProvinceName, Year + " - " + (Year + 1));

            return InsertProcessedReport("TinhHinhNhapLieuTheoQuanTaiTruong", UserAccountID, SupervisingDeptID, DistrictID, Year, ReportDate, data);
        }

        // ================================== private method ==========================================

        private Stream WriteToExcel(string templatePath, List<ReportData> ListData, string ProvinceName, string AcademicYear)
        {
            IVTWorkbook oBook = VTExport.OpenWorkbook(templatePath);

            IVTWorksheet Template = oBook.GetSheet(1);
            IVTWorksheet Sheet = oBook.CopySheetToLast(oBook.GetSheet(1));

            int index = 1;
            int CurrentRow = 8;
            Dictionary<string, object> TempData = new Dictionary<string, object>();
            foreach (ReportData ReportData in ListData)
            {

                TempData["STT"] = index;
                TempData["DistrictName"] = ReportData.DistrictName;
                foreach (InputStatistics InputStatistics in ReportData.ListInputStatistics)
                {
                    TempData["InputStatistics"] = InputStatistics;

                    Sheet.CopyAndInsertARow(7, CurrentRow);
                    Sheet.GetRange(CurrentRow, 1, CurrentRow, 100).FillVariableValue(TempData);
                    CurrentRow++;
                }
                Sheet.GetRange(CurrentRow - 1, 1, CurrentRow - ReportData.ListInputStatistics.Count, 1).Merge();
                Sheet.GetRange(CurrentRow - 1, 2, CurrentRow - ReportData.ListInputStatistics.Count, 2).Merge();
                Sheet.GetRange(CurrentRow - 1, 2, CurrentRow - ReportData.ListInputStatistics.Count, 2).SetHAlign(VTHAlign.xlHAlignLeft);
                index++;
            }

            Sheet.DeleteRow(7);

            TempData["Formula"] = "=";
            TempData["SumRowNum"] = CurrentRow - 2;
            Sheet.GetRange(CurrentRow - 1, 1, CurrentRow - 1, 100).FillVariableValue(TempData);

            TempData["ProvinceName"] = ProvinceName;
            TempData["AcademicYear"] = AcademicYear;
            Sheet.GetRange("A1", "Q4").FillVariableValue(TempData);

            oBook.GetSheet(1).Delete();

            return oBook.ToStream();
        }

        private List<ReportData> GroupByDistrictStatisticsGetData(int UserAccountID, int SupervisingDeptID, int DistrictID, int Year, DateTime ReportDate)
        {
            SupervisingDept Dept = SupervisingDeptBusiness.Find(SupervisingDeptID);

            // Danh sach quan huyen
            List<District> ListDistrict = DistrictBusiness.All.Where(o => o.ProvinceID == Dept.ProvinceID && o.IsActive == true && (DistrictID == 0 || o.DistrictID == DistrictID)).ToList();

            // Danh sach so luong truong theo quan huyen va theo cap
            List<InputStatistics> ListSchoolCount = SchoolProfileBusiness.All.Where(o => o.ProvinceID == Dept.ProvinceID && o.IsActive == true && (DistrictID == 0 || o.DistrictID == DistrictID))
                .GroupBy(o => new { o.DistrictID, o.EducationGrade }).Select(o => new InputStatistics
                {
                    DistrictID = o.Key.DistrictID,
                    GradeID = o.Key.EducationGrade,
                    NumberOfSchool = o.Count()
                }).ToList();

            // Danh sach so luong truong co du lieu theo quan huyen va theo cap
            List<InputStatistics> ListSchoolHasDataCount = AcademicYearBusiness.All.Where(o => o.IsActive==true && o.SchoolProfile.ProvinceID == Dept.ProvinceID && o.SchoolProfile.IsActive == true && (DistrictID == 0 || o.SchoolProfile.DistrictID == DistrictID))
                .GroupBy(o => new { o.SchoolProfile.DistrictID, o.SchoolProfile.EducationGrade }).Select(o => new InputStatistics
                {
                    DistrictID = o.Key.DistrictID,
                    GradeID = o.Key.EducationGrade,
                    NumberOfSchoolHasData = o.Select(p => p.SchoolID).Distinct().Count()
                }).ToList();

            // Danh sach so luong hoc sinh theo quan huyen va cap
            List<InputStatistics> ListPupilCount = PupilOfClassBusiness.All.Where(o => o.SchoolProfile.ProvinceID == Dept.ProvinceID && o.SchoolProfile.IsActive == true && (DistrictID == 0 || o.SchoolProfile.DistrictID == DistrictID))
                .GroupBy(o => new { o.SchoolProfile.DistrictID, o.SchoolProfile.EducationGrade }).Select(o => new InputStatistics
                {
                    DistrictID = o.Key.DistrictID,
                    GradeID = o.Key.EducationGrade,
                    NumberOfPupil = o.Count()
                }).ToList();

            // Danh sach so luong giao vien theo quan huyen va cap
            List<InputStatistics> ListTeacherCount = EmployeeHistoryStatusBusiness.All
                .Where(o =>
                    o.SchoolProfile.ProvinceID == Dept.ProvinceID &&
                    o.SchoolProfile.IsActive == true && (DistrictID == 0 || o.SchoolProfile.DistrictID == DistrictID) &&
                    o.FromDate.Year <= Year && Year <= o.ToDate.Value.Year &&
                    o.EmployeeStatus == SystemParamsInFile.EMPLOYMENT_STATUS_WORKING
                )
                .GroupBy(o => new { o.SchoolProfile.DistrictID, o.SchoolProfile.EducationGrade }).Select(o => new InputStatistics
                {
                    DistrictID = o.Key.DistrictID,
                    GradeID = o.Key.EducationGrade,
                    NumberOfTeacher = o.Count()
                }).ToList();

            // Danh sach so luong lop theo quan huyen va cap
            List<InputStatistics> ListClassCount = ClassProfileBusiness.All
                .Where(o =>
                    o.SchoolProfile.ProvinceID == Dept.ProvinceID &&
                    o.SchoolProfile.IsActive == true && (DistrictID == 0 || o.SchoolProfile.DistrictID == DistrictID) &&
                    o.AcademicYear.Year == Year
                    && o.IsActive==true
                )
                .GroupBy(o => new { o.SchoolProfile.DistrictID, o.SchoolProfile.EducationGrade }).Select(o => new InputStatistics
                {
                    DistrictID = o.Key.DistrictID,
                    GradeID = o.Key.EducationGrade,
                    NumberOfClass = o.Count()
                }).ToList();

            // Danh sach so luong lop da khai bao mon hoc theo quan huyen va cap
            List<InputStatistics> ListClassHasSubjectCount = ClassSubjectBusiness.All
                .Where(o =>
                    o.ClassProfile.SchoolProfile.ProvinceID == Dept.ProvinceID &&
                    o.ClassProfile.SchoolProfile.IsActive == true && (DistrictID == 0 || o.ClassProfile.SchoolProfile.DistrictID == DistrictID) &&
                    o.ClassProfile.AcademicYear.Year == Year
                )
                .GroupBy(o => new { o.ClassProfile.SchoolProfile.DistrictID, o.ClassProfile.SchoolProfile.EducationGrade }).Select(o => new InputStatistics
                {
                    DistrictID = o.Key.DistrictID,
                    GradeID = o.Key.EducationGrade,
                    NumberOfClassHasSubject = o.Select(p => p.ClassID).Distinct().Count()
                }).ToList();

            // Danh sach so luong lop da phan cong giang day HKI theo quan huyen va cap
            List<InputStatistics> ListClassHasAssignForFirstSemesterCount = TeachingAssignmentBusiness.All
                .Where(o =>
                    o.ClassProfile.SchoolProfile.ProvinceID == Dept.ProvinceID &&
                    o.ClassProfile.SchoolProfile.IsActive == true && (DistrictID == 0 || o.SchoolProfile.DistrictID == DistrictID) &&
                    o.ClassProfile.AcademicYear.Year == Year &&
                    (o.Semester == SystemParamsInFile.SEMESTER_OF_YEAR_FIRST || o.Semester == null)
                )
                .GroupBy(o => new { o.ClassProfile.SchoolProfile.DistrictID, o.ClassProfile.SchoolProfile.EducationGrade }).Select(o => new InputStatistics
                {
                    DistrictID = o.Key.DistrictID,
                    GradeID = o.Key.EducationGrade,
                    NumberOfClassHasAssignForFirstSemester = o.Select(p => p.ClassID).Distinct().Count()
                }).ToList();

            // Danh sach so luong lop da nhap diem HKI theo quan huyen va cap
            List<InputStatistics> ListClassHasMarkForFirstSemesterCount = MarkRecordBusiness.All
                .Where(o =>
                    o.ClassProfile.SchoolProfile.ProvinceID == Dept.ProvinceID &&
                    o.ClassProfile.SchoolProfile.IsActive == true && (DistrictID == 0 || o.SchoolProfile.DistrictID == DistrictID) &&
                    o.ClassProfile.AcademicYear.Year == Year &&
                    (o.Semester == SystemParamsInFile.SEMESTER_OF_YEAR_FIRST || o.Semester == null)
                )
                .GroupBy(o => new { o.ClassProfile.SchoolProfile.DistrictID, o.ClassProfile.SchoolProfile.EducationGrade }).Select(o => new InputStatistics
                {
                    DistrictID = o.Key.DistrictID,
                    GradeID = o.Key.EducationGrade,
                    NumberOfClassHasMarkForFirstSemester = o.Select(p => p.ClassID).Distinct().Count()
                }).ToList();

            // Danh sach so luong lop da nhap hanh kiem HKI theo quan huyen va cap
            List<InputStatistics> ListClassHasConductForFirstSemesterCount = PupilRankingBusiness.All
                .Where(o =>
                    o.ClassProfile.SchoolProfile.ProvinceID == Dept.ProvinceID &&
                    o.ClassProfile.SchoolProfile.IsActive == true && (DistrictID == 0 || o.SchoolProfile.DistrictID == DistrictID) &&
                    o.ClassProfile.AcademicYear.Year == Year &&
                    o.Semester == SystemParamsInFile.SEMESTER_OF_YEAR_FIRST &&
                    o.ConductLevelID != null
                )
                .GroupBy(o => new { o.ClassProfile.SchoolProfile.DistrictID, o.ClassProfile.SchoolProfile.EducationGrade }).Select(o => new InputStatistics
                {
                    DistrictID = o.Key.DistrictID,
                    GradeID = o.Key.EducationGrade,
                    NumberOfClassHasConductForFirstSemester = o.Select(p => p.ClassID).Distinct().Count()
                }).ToList();

            // Danh sach so luong lop da tong ket diem HKI theo quan huyen va cap
            List<InputStatistics> ListClassHasSumMarkForFirstSemesterCount = PupilRankingBusiness.All
                .Where(o =>
                    o.ClassProfile.SchoolProfile.ProvinceID == Dept.ProvinceID &&
                    o.ClassProfile.SchoolProfile.IsActive == true && (DistrictID == 0 || o.SchoolProfile.DistrictID == DistrictID) &&
                    o.ClassProfile.AcademicYear.Year == Year &&
                    o.Semester == SystemParamsInFile.SEMESTER_OF_YEAR_FIRST &&
                    o.AverageMark != null
                )
                .GroupBy(o => new { o.ClassProfile.SchoolProfile.DistrictID, o.ClassProfile.SchoolProfile.EducationGrade }).Select(o => new InputStatistics
                {
                    DistrictID = o.Key.DistrictID,
                    GradeID = o.Key.EducationGrade,
                    NumberOfClassHasSumMarkForFirstSemester = o.Select(p => p.ClassID).Distinct().Count()
                }).ToList();




            // Danh sach so luong lop da phan cong giang day HKII theo quan huyen va cap
            List<InputStatistics> ListClassHasAssignForSecondSemesterCount = TeachingAssignmentBusiness.All
                .Where(o =>
                    o.ClassProfile.SchoolProfile.ProvinceID == Dept.ProvinceID &&
                    o.ClassProfile.SchoolProfile.IsActive == true && (DistrictID == 0 || o.SchoolProfile.DistrictID == DistrictID) &&
                    o.ClassProfile.AcademicYear.Year == Year &&
                    (o.Semester == SystemParamsInFile.SEMESTER_OF_YEAR_SECOND || o.Semester == null)
                )
                .GroupBy(o => new { o.ClassProfile.SchoolProfile.DistrictID, o.ClassProfile.SchoolProfile.EducationGrade }).Select(o => new InputStatistics
                {
                    DistrictID = o.Key.DistrictID,
                    GradeID = o.Key.EducationGrade,
                    NumberOfClassHasAssignForSecondSemester = o.Select(p => p.ClassID).Distinct().Count()
                }).ToList();

            // Danh sach so luong lop da nhap diem HKII theo quan huyen va cap
            List<InputStatistics> ListClassHasMarkForSecondSemesterCount = MarkRecordBusiness.All
                .Where(o =>
                    o.ClassProfile.SchoolProfile.ProvinceID == Dept.ProvinceID &&
                    o.ClassProfile.SchoolProfile.IsActive == true && (DistrictID == 0 || o.SchoolProfile.DistrictID == DistrictID) &&
                    o.ClassProfile.AcademicYear.Year == Year &&
                    (o.Semester == SystemParamsInFile.SEMESTER_OF_YEAR_SECOND || o.Semester == null)
                )
                .GroupBy(o => new { o.ClassProfile.SchoolProfile.DistrictID, o.ClassProfile.SchoolProfile.EducationGrade }).Select(o => new InputStatistics
                {
                    DistrictID = o.Key.DistrictID,
                    GradeID = o.Key.EducationGrade,
                    NumberOfClassHasMarkForSecondSemester = o.Select(p => p.ClassID).Distinct().Count()
                }).ToList();

            // Danh sach so luong lop da nhap hanh kiem HKII theo quan huyen va cap
            List<InputStatistics> ListClassHasConductForSecondSemesterCount = PupilRankingBusiness.All
                .Where(o =>
                    o.ClassProfile.SchoolProfile.ProvinceID == Dept.ProvinceID &&
                    o.ClassProfile.SchoolProfile.IsActive == true && (DistrictID == 0 || o.SchoolProfile.DistrictID == DistrictID) &&
                    o.ClassProfile.AcademicYear.Year == Year &&
                    o.Semester == SystemParamsInFile.SEMESTER_OF_YEAR_SECOND &&
                    o.ConductLevelID != null
                )
                .GroupBy(o => new { o.ClassProfile.SchoolProfile.DistrictID, o.ClassProfile.SchoolProfile.EducationGrade }).Select(o => new InputStatistics
                {
                    DistrictID = o.Key.DistrictID,
                    GradeID = o.Key.EducationGrade,
                    NumberOfClassHasConductForSecondSemester = o.Select(p => p.ClassID).Distinct().Count()
                }).ToList();

            // Danh sach so luong lop da tong ket diem HKII theo quan huyen va cap
            List<InputStatistics> ListClassHasSumMarkForSecondSemesterCount = PupilRankingBusiness.All
                .Where(o =>
                    o.ClassProfile.SchoolProfile.ProvinceID == Dept.ProvinceID &&
                    o.ClassProfile.SchoolProfile.IsActive == true && (DistrictID == 0 || o.SchoolProfile.DistrictID == DistrictID) &&
                    o.ClassProfile.AcademicYear.Year == Year &&
                    o.Semester == SystemParamsInFile.SEMESTER_OF_YEAR_SECOND &&
                    o.AverageMark != null
                )
                .GroupBy(o => new { o.ClassProfile.SchoolProfile.DistrictID, o.ClassProfile.SchoolProfile.EducationGrade }).Select(o => new InputStatistics
                {
                    DistrictID = o.Key.DistrictID,
                    GradeID = o.Key.EducationGrade,
                    NumberOfClassHasSumMarkForSecondSemester = o.Select(p => p.ClassID).Distinct().Count()
                }).ToList();



            // Danh sach so luong user giao vien da cap theo quan huyen va cap
            List<InputStatistics> ListUserCount = UserAccountBusiness.All
                .Where(o =>
                    o.Employee.SchoolProfile.ProvinceID == Dept.ProvinceID &&
                    o.Employee.SchoolProfile.IsActive == true && (DistrictID == 0 || o.Employee.SchoolProfile.DistrictID == DistrictID) &&
                    o.CreatedDate <= ReportDate &&
                    o.IsActive == true
                )
                .GroupBy(o => new { o.Employee.SchoolProfile.DistrictID, o.Employee.SchoolProfile.EducationGrade }).Select(o => new InputStatistics
                {
                    DistrictID = o.Key.DistrictID,
                    GradeID = o.Key.EducationGrade,
                    NumberOfUser = o.Count()
                }).ToList();

            // Danh sach so luong user giao vien da login theo quan huyen va cap
            List<InputStatistics> ListLoggedUserCount = UserAccountBusiness.All
                .Where(o =>
                    o.Employee.SchoolProfile.ProvinceID == Dept.ProvinceID &&
                    o.Employee.SchoolProfile.IsActive == true && (DistrictID == 0 || o.Employee.SchoolProfile.DistrictID == DistrictID) &&
                    o.CreatedDate <= ReportDate &&
                    o.FirstLoginDate <= ReportDate &&
                    o.IsActive == true
                )
                .GroupBy(o => new { o.Employee.SchoolProfile.DistrictID, o.Employee.SchoolProfile.EducationGrade }).Select(o => new InputStatistics
                {
                    DistrictID = o.Key.DistrictID,
                    GradeID = o.Key.EducationGrade,
                    NumberOfLoggedUser = o.Count()
                }).ToList();



            // Danh sach so luong hoc sinh da nhap sdt ca nhan theo quan huyen va cap
            List<InputStatistics> ListPupilHasMobileCount = PupilOfClassBusiness.All
                .Where(o =>
                    o.SchoolProfile.ProvinceID == Dept.ProvinceID &&
                    o.SchoolProfile.IsActive == true && (DistrictID == 0 || o.SchoolProfile.DistrictID == DistrictID) &&
                    (o.Status == SystemParamsInFile.PUPIL_STATUS_STUDYING || o.Status == SystemParamsInFile.PUPIL_STATUS_GRADUATED) &&
                    o.PupilProfile.Mobile != null
                )
                .GroupBy(o => new { o.SchoolProfile.DistrictID, o.SchoolProfile.EducationGrade }).Select(o => new InputStatistics
                {
                    DistrictID = o.Key.DistrictID,
                    GradeID = o.Key.EducationGrade,
                    NumberOfPupilHasMobile = o.Count()
                }).ToList();


            // Danh sach so luong hoc sinh da nhap sdt cua cha theo quan huyen va cap
            List<InputStatistics> ListPupilHasFatherMobileCount = PupilOfClassBusiness.All
                .Where(o =>
                    o.SchoolProfile.ProvinceID == Dept.ProvinceID &&
                    o.SchoolProfile.IsActive == true && (DistrictID == 0 || o.SchoolProfile.DistrictID == DistrictID) &&
                    (o.Status == SystemParamsInFile.PUPIL_STATUS_STUDYING || o.Status == SystemParamsInFile.PUPIL_STATUS_GRADUATED) &&
                    o.PupilProfile.FatherMobile != null
                )
                .GroupBy(o => new { o.SchoolProfile.DistrictID, o.SchoolProfile.EducationGrade }).Select(o => new InputStatistics
                {
                    DistrictID = o.Key.DistrictID,
                    GradeID = o.Key.EducationGrade,
                    NumberOfPupilHasFatherMobile = o.Count()
                }).ToList();

            // Danh sach so luong hoc sinh da nhap sdt cua me theo quan huyen va cap
            List<InputStatistics> ListPupilHasMotherMobileCount = PupilOfClassBusiness.All
                .Where(o =>
                    o.SchoolProfile.ProvinceID == Dept.ProvinceID &&
                    o.SchoolProfile.IsActive == true && (DistrictID == 0 || o.SchoolProfile.DistrictID == DistrictID) &&
                    (o.Status == SystemParamsInFile.PUPIL_STATUS_STUDYING || o.Status == SystemParamsInFile.PUPIL_STATUS_GRADUATED) &&
                    o.PupilProfile.MotherMobile != null
                )
                .GroupBy(o => new { o.SchoolProfile.DistrictID, o.SchoolProfile.EducationGrade }).Select(o => new InputStatistics
                {
                    DistrictID = o.Key.DistrictID,
                    GradeID = o.Key.EducationGrade,
                    NumberOfPupilHasMotherMobile = o.Count()
                }).ToList();


            List<InputStatistics> ListResult = new List<InputStatistics>();
            List<ReportData> ListData = new List<ReportData>();
            InputStatistics obj = null;
            decimal? tmp = 0;
            foreach (District district in ListDistrict)
            {
                ReportData ReportData = new ReportData();
                ReportData.DistrictName = district.DistrictName;
                List<InputStatistics> lstSchool = ListSchoolCount.Where(p => p.DistrictID == district.DistrictID).ToList();
                List<InputStatistics> lstSchoolHasData = ListSchoolHasDataCount.Where(p => p.DistrictID == district.DistrictID).ToList();
                List<InputStatistics> lstPupil = ListPupilCount.Where(p => p.DistrictID == district.DistrictID).ToList();
                List<InputStatistics> lstTeacher = ListTeacherCount.Where(p => p.DistrictID == district.DistrictID).ToList();
                List<InputStatistics> lstClass = ListClassCount.Where(p => p.DistrictID == district.DistrictID).ToList();
                List<InputStatistics> lstClassHasSubject = ListClassHasSubjectCount.Where(p => p.DistrictID == district.DistrictID).ToList();
                List<InputStatistics> lstClassHasAssignForFirstSemester = ListClassHasAssignForFirstSemesterCount.Where(p => p.DistrictID == district.DistrictID).ToList();
                List<InputStatistics> lstClassHasMarkForFirstSemester = ListClassHasMarkForFirstSemesterCount.Where(p => p.DistrictID == district.DistrictID).ToList();
                List<InputStatistics> lstClassHasConductForFirstSemester = ListClassHasConductForFirstSemesterCount.Where(p => p.DistrictID == district.DistrictID).ToList();
                List<InputStatistics> lstClassHasSumMarkForFirstSemester = ListClassHasSumMarkForFirstSemesterCount.Where(p => p.DistrictID == district.DistrictID).ToList();
                List<InputStatistics> lstClassHasAssignForSecondSemester = ListClassHasAssignForSecondSemesterCount.Where(p => p.DistrictID == district.DistrictID).ToList();
                List<InputStatistics> lstClassHasMarkForSecondSemester = ListClassHasMarkForSecondSemesterCount.Where(p => p.DistrictID == district.DistrictID).ToList();
                List<InputStatistics> lstClassHasConductForSecondSemester = ListClassHasConductForSecondSemesterCount.Where(p => p.DistrictID == district.DistrictID).ToList();
                List<InputStatistics> lstClassHasSumMarkForSecondSemester = ListClassHasSumMarkForSecondSemesterCount.Where(p => p.DistrictID == district.DistrictID).ToList();
                List<InputStatistics> lstPupilHasMobile = ListPupilHasMobileCount.Where(p => p.DistrictID == district.DistrictID).ToList();
                List<InputStatistics> lstPupilHasFatherMobile = ListPupilHasFatherMobileCount.Where(p => p.DistrictID == district.DistrictID).ToList();
                List<InputStatistics> lstPupilHasMotherMobile = ListPupilHasMotherMobileCount.Where(p => p.DistrictID == district.DistrictID).ToList();
                for (int i = 1; i <= 3; i++)
                {
                    obj = new InputStatistics();
                    obj.DistrictID = district.DistrictID;
                    obj.DistrictName = district.DistrictName;
                    obj.GradeID = Utils.GradeToBinary(i);

                    // So luong truong
                    tmp = lstSchool.Where(o => (o.GradeID & obj.GradeID) != 0).Sum(o => o.NumberOfSchool);
                    obj.NumberOfSchool = tmp == null ? 0 : (int)tmp.Value;

                    // So luong truong da nhap du lieu
                    tmp = lstSchoolHasData.Where(o => (o.GradeID & obj.GradeID) != 0).Sum(o => o.NumberOfSchoolHasData);
                    obj.NumberOfSchoolHasData = tmp == null ? 0 : (int)tmp.Value;

                    // So hoc sinh
                    tmp = lstPupil.Where(o => (o.GradeID & obj.GradeID) != 0).Sum(o => o.NumberOfPupil);
                    obj.NumberOfPupil = tmp == null ? 0 : (int)tmp.Value;

                    // So giao vien
                    tmp = lstTeacher.Where(o => (o.GradeID & obj.GradeID) != 0).Sum(o => o.NumberOfTeacher);
                    obj.NumberOfTeacher = tmp == null ? 0 : (int)tmp.Value;

                    // So lop
                    tmp = lstClass.Where(o => (o.GradeID & obj.GradeID) != 0).Sum(o => o.NumberOfClass);
                    obj.NumberOfClass = tmp == null ? 0 : (int)tmp.Value;

                    // So lop da khai bao mon hoc
                    tmp = lstClassHasSubject.Where(o =>  (o.GradeID & obj.GradeID) != 0).Sum(o => o.NumberOfClassHasSubject);
                    obj.NumberOfClassHasSubject = tmp == null ? 0 : (int)tmp.Value;

                    // So lop da phan cong giang day HKI
                    tmp = lstClassHasAssignForFirstSemester.Where(o => (o.GradeID & obj.GradeID) != 0).Sum(o => o.NumberOfClassHasAssignForFirstSemester);
                    obj.NumberOfClassHasAssignForFirstSemester = tmp == null ? 0 : (int)tmp.Value;

                    // So lop da nhap diem HKI
                    tmp = lstClassHasMarkForFirstSemester.Where(o =>(o.GradeID & obj.GradeID) != 0).Sum(o => o.NumberOfClassHasMarkForFirstSemester);
                    obj.NumberOfClassHasMarkForFirstSemester = tmp == null ? 0 : (int)tmp.Value;

                    // So lop da nhap hanh kiem HKI
                    tmp = lstClassHasConductForFirstSemester.Where(o => (o.GradeID & obj.GradeID) != 0).Sum(o => o.NumberOfClassHasConductForFirstSemester);
                    obj.NumberOfClassHasConductForFirstSemester = tmp == null ? 0 : (int)tmp.Value;

                    // So lop da tong ket HKI
                    tmp = lstClassHasSumMarkForFirstSemester.Where(o =>(o.GradeID & obj.GradeID) != 0).Sum(o => o.NumberOfClassHasSumMarkForFirstSemester);
                    obj.NumberOfClassHasSumMarkForFirstSemester = tmp == null ? 0 : (int)tmp.Value;


                    // So lop da phan cong giang day HKII
                    tmp = lstClassHasAssignForSecondSemester.Where(o => (o.GradeID & obj.GradeID) != 0).Sum(o => o.NumberOfClassHasAssignForSecondSemester);
                    obj.NumberOfClassHasAssignForSecondSemester = tmp == null ? 0 : (int)tmp.Value;

                    // So lop da nhap diem HKII
                    tmp = lstClassHasMarkForSecondSemester.Where(o => (o.GradeID & obj.GradeID) != 0).Sum(o => o.NumberOfClassHasMarkForSecondSemester);
                    obj.NumberOfClassHasMarkForSecondSemester = tmp == null ? 0 : (int)tmp.Value;

                    // So lop da nhap hanh kiem HKII
                    tmp = lstClassHasConductForSecondSemester.Where(o => (o.GradeID & obj.GradeID) != 0).Sum(o => o.NumberOfClassHasConductForSecondSemester);
                    obj.NumberOfClassHasConductForSecondSemester = tmp == null ? 0 : (int)tmp.Value;

                    // So lop da tong ket HKII
                    tmp = lstClassHasSumMarkForSecondSemester.Where(o => (o.GradeID & obj.GradeID) != 0).Sum(o => o.NumberOfClassHasSumMarkForSecondSemester);
                    obj.NumberOfClassHasSumMarkForSecondSemester = tmp == null ? 0 : (int)tmp.Value;

                    // So lop da nhap sdt ca nhan
                    tmp = ListPupilHasMobileCount.Where(o =>(o.GradeID & obj.GradeID) != 0).Sum(o => o.NumberOfPupilHasMobile);
                    obj.NumberOfPupilHasMobile = tmp == null ? 0 : (int)tmp.Value;

                    // So lop da nhap sdt ca nhan
                    tmp = lstPupilHasFatherMobile.Where(o =>(o.GradeID & obj.GradeID) != 0).Sum(o => o.NumberOfPupilHasFatherMobile);
                    obj.NumberOfPupilHasFatherMobile = tmp == null ? 0 : (int)tmp.Value;

                    // So lop da nhap sdt ca nhan
                    tmp = lstPupilHasMotherMobile.Where(o => (o.GradeID & obj.GradeID) != 0).Sum(o => o.NumberOfPupilHasMotherMobile);
                    obj.NumberOfPupilHasMotherMobile = tmp == null ? 0 : (int)tmp.Value;

                    obj.GradeID = i;
                    ReportData.ListInputStatistics.Add(obj);
                }

                ListData.Add(ReportData);
            }

            return ListData;
        }

        private List<ReportData> SchoolStatisticsGetData(int UserAccountID, int SupervisingDeptID, int DistrictID, int Year, DateTime ReportDate)
        {
            SupervisingDept Dept = SupervisingDeptBusiness.Find(SupervisingDeptID);

            // Danh sach quan huyen
            List<District> ListDistrict = DistrictBusiness.All.Where(o => o.ProvinceID == Dept.ProvinceID && o.IsActive == true && (DistrictID == 0 || o.DistrictID == DistrictID) && (DistrictID == 0 || o.DistrictID == DistrictID)).ToList();

            // Danh sach so luong truong theo quan huyen va cap
            List<InputStatistics> ListSchoolCount = SchoolProfileBusiness.All.Where(o => o.ProvinceID == Dept.ProvinceID && o.IsActive == true && (DistrictID == 0 || o.DistrictID == DistrictID))
                .Select(o => new InputStatistics
                {
                    DistrictID = o.DistrictID,
                    SchoolID = o.SchoolProfileID,
                    SchoolName = o.SchoolName,
                    GradeID = o.EducationGrade
                }).ToList();

            // Danh sach so luong hoc sinh theo quan huyen va cap
            List<InputStatistics> ListPupilCount = PupilOfClassBusiness.All.Where(o => o.SchoolProfile.ProvinceID == Dept.ProvinceID && o.SchoolProfile.IsActive == true && (DistrictID == 0 || o.SchoolProfile.DistrictID == DistrictID))
                .GroupBy(o => new { o.SchoolProfile.DistrictID, o.SchoolProfile.SchoolProfileID }).Select(o => new InputStatistics
                {
                    DistrictID = o.Key.DistrictID,
                    SchoolID = o.Key.SchoolProfileID,
                    NumberOfPupil = o.Count()
                }).ToList();

            // Danh sach so luong giao vien theo quan huyen va cap
            List<InputStatistics> ListTeacherCount = EmployeeHistoryStatusBusiness.All
                .Where(o =>
                    o.SchoolProfile.ProvinceID == Dept.ProvinceID &&
                    o.SchoolProfile.IsActive == true && (DistrictID == 0 || o.SchoolProfile.DistrictID == DistrictID) &&
                    o.FromDate.Year <= Year && Year <= o.ToDate.Value.Year &&
                    o.EmployeeStatus == SystemParamsInFile.EMPLOYMENT_STATUS_WORKING
                )
                .GroupBy(o => new { o.SchoolProfile.DistrictID, o.SchoolProfile.SchoolProfileID }).Select(o => new InputStatistics
                {
                    DistrictID = o.Key.DistrictID,
                    SchoolID = o.Key.SchoolProfileID,
                    NumberOfTeacher = o.Count()
                }).ToList();

            // Danh sach so luong lop theo quan huyen va cap
            List<InputStatistics> ListClassCount = ClassProfileBusiness.All
                .Where(o =>
                    o.SchoolProfile.ProvinceID == Dept.ProvinceID &&
                    o.SchoolProfile.IsActive == true && (DistrictID == 0 || o.SchoolProfile.DistrictID == DistrictID) &&
                    o.AcademicYear.Year == Year
                    && o.IsActive==true
                )
                .GroupBy(o => new { o.SchoolProfile.DistrictID, o.SchoolProfile.SchoolProfileID }).Select(o => new InputStatistics
                {
                    DistrictID = o.Key.DistrictID,
                    SchoolID = o.Key.SchoolProfileID,
                    NumberOfClass = o.Count()
                }).ToList();

            // Danh sach so luong lop da khai bao mon hoc theo quan huyen va cap
            List<InputStatistics> ListClassHasSubjectCount = ClassSubjectBusiness.All
                .Where(o =>
                    o.ClassProfile.SchoolProfile.ProvinceID == Dept.ProvinceID &&
                    o.ClassProfile.SchoolProfile.IsActive == true && (DistrictID == 0 || o.ClassProfile.SchoolProfile.DistrictID == DistrictID) &&
                    o.ClassProfile.AcademicYear.Year == Year
                )
                .GroupBy(o => new { o.ClassProfile.SchoolProfile.DistrictID, o.ClassProfile.SchoolProfile.SchoolProfileID }).Select(o => new InputStatistics
                {
                    DistrictID = o.Key.DistrictID,
                    SchoolID = o.Key.SchoolProfileID,
                    NumberOfClassHasSubject = o.Select(p => p.ClassID).Distinct().Count()
                }).ToList();

            // Danh sach so luong lop da phan cong giang day HKI theo quan huyen va cap
            List<InputStatistics> ListClassHasAssignForFirstSemesterCount = TeachingAssignmentBusiness.All
                .Where(o =>
                    o.ClassProfile.SchoolProfile.ProvinceID == Dept.ProvinceID &&
                    o.ClassProfile.SchoolProfile.IsActive == true && (DistrictID == 0 || o.SchoolProfile.DistrictID == DistrictID) &&
                    o.ClassProfile.AcademicYear.Year == Year &&
                    (o.Semester == SystemParamsInFile.SEMESTER_OF_YEAR_FIRST || o.Semester == null)
                )
                .GroupBy(o => new { o.ClassProfile.SchoolProfile.DistrictID, o.ClassProfile.SchoolProfile.SchoolProfileID }).Select(o => new InputStatistics
                {
                    DistrictID = o.Key.DistrictID,
                    SchoolID = o.Key.SchoolProfileID,
                    NumberOfClassHasAssignForFirstSemester = o.Select(p => p.ClassID).Distinct().Count()
                }).ToList();

            // Danh sach so luong lop da nhap diem HKI theo quan huyen va cap
            List<InputStatistics> ListClassHasMarkForFirstSemesterCount = MarkRecordBusiness.All
                .Where(o =>
                    o.ClassProfile.SchoolProfile.ProvinceID == Dept.ProvinceID &&
                    o.ClassProfile.SchoolProfile.IsActive == true && (DistrictID == 0 || o.SchoolProfile.DistrictID == DistrictID) &&
                    o.ClassProfile.AcademicYear.Year == Year &&
                    (o.Semester == SystemParamsInFile.SEMESTER_OF_YEAR_FIRST || o.Semester == null)
                )
                .GroupBy(o => new { o.ClassProfile.SchoolProfile.DistrictID, o.ClassProfile.SchoolProfile.SchoolProfileID }).Select(o => new InputStatistics
                {
                    DistrictID = o.Key.DistrictID,
                    SchoolID = o.Key.SchoolProfileID,
                    NumberOfClassHasMarkForFirstSemester = o.Select(p => p.ClassID).Distinct().Count()
                }).ToList();

            // Danh sach so luong lop da nhap hanh kiem HKI theo quan huyen va cap
            List<InputStatistics> ListClassHasConductForFirstSemesterCount = PupilRankingBusiness.All
                .Where(o =>
                    o.ClassProfile.SchoolProfile.ProvinceID == Dept.ProvinceID &&
                    o.ClassProfile.SchoolProfile.IsActive == true && (DistrictID == 0 || o.SchoolProfile.DistrictID == DistrictID) &&
                    o.ClassProfile.AcademicYear.Year == Year &&
                    o.Semester == SystemParamsInFile.SEMESTER_OF_YEAR_FIRST &&
                    o.ConductLevelID != null
                )
                .GroupBy(o => new { o.ClassProfile.SchoolProfile.DistrictID, o.ClassProfile.SchoolProfile.SchoolProfileID }).Select(o => new InputStatistics
                {
                    DistrictID = o.Key.DistrictID,
                    SchoolID = o.Key.SchoolProfileID,
                    NumberOfClassHasConductForFirstSemester = o.Select(p => p.ClassID).Distinct().Count()
                }).ToList();

            // Danh sach so luong lop da tong ket diem HKI theo quan huyen va cap
            List<InputStatistics> ListClassHasSumMarkForFirstSemesterCount = PupilRankingBusiness.All
                .Where(o =>
                    o.ClassProfile.SchoolProfile.ProvinceID == Dept.ProvinceID &&
                    o.ClassProfile.SchoolProfile.IsActive == true && (DistrictID == 0 || o.SchoolProfile.DistrictID == DistrictID) &&
                    o.ClassProfile.AcademicYear.Year == Year &&
                    o.Semester == SystemParamsInFile.SEMESTER_OF_YEAR_FIRST &&
                    o.AverageMark != null
                )
                .GroupBy(o => new { o.ClassProfile.SchoolProfile.DistrictID, o.ClassProfile.SchoolProfile.SchoolProfileID }).Select(o => new InputStatistics
                {
                    DistrictID = o.Key.DistrictID,
                    SchoolID = o.Key.SchoolProfileID,
                    NumberOfClassHasSumMarkForFirstSemester = o.Select(p => p.ClassID).Distinct().Count()
                }).ToList();




            // Danh sach so luong lop da phan cong giang day HKII theo quan huyen va cap
            List<InputStatistics> ListClassHasAssignForSecondSemesterCount = TeachingAssignmentBusiness.All
                .Where(o =>
                    o.ClassProfile.SchoolProfile.ProvinceID == Dept.ProvinceID &&
                    o.ClassProfile.SchoolProfile.IsActive == true && (DistrictID == 0 || o.SchoolProfile.DistrictID == DistrictID) &&
                    o.ClassProfile.AcademicYear.Year == Year &&
                    (o.Semester == SystemParamsInFile.SEMESTER_OF_YEAR_SECOND || o.Semester == null)
                )
                .GroupBy(o => new { o.ClassProfile.SchoolProfile.DistrictID, o.ClassProfile.SchoolProfile.SchoolProfileID }).Select(o => new InputStatistics
                {
                    DistrictID = o.Key.DistrictID,
                    SchoolID = o.Key.SchoolProfileID,
                    NumberOfClassHasAssignForSecondSemester = o.Select(p => p.ClassID).Distinct().Count()
                }).ToList();

            // Danh sach so luong lop da nhap diem HKII theo quan huyen va cap
            List<InputStatistics> ListClassHasMarkForSecondSemesterCount = MarkRecordBusiness.All
                .Where(o =>
                    o.ClassProfile.SchoolProfile.ProvinceID == Dept.ProvinceID &&
                    o.ClassProfile.SchoolProfile.IsActive == true && (DistrictID == 0 || o.SchoolProfile.DistrictID == DistrictID) &&
                    o.ClassProfile.AcademicYear.Year == Year &&
                    (o.Semester == SystemParamsInFile.SEMESTER_OF_YEAR_SECOND || o.Semester == null)
                )
                .GroupBy(o => new { o.ClassProfile.SchoolProfile.DistrictID, o.ClassProfile.SchoolProfile.SchoolProfileID }).Select(o => new InputStatistics
                {
                    DistrictID = o.Key.DistrictID,
                    SchoolID = o.Key.SchoolProfileID,
                    NumberOfClassHasMarkForSecondSemester = o.Select(p => p.ClassID).Distinct().Count()
                }).ToList();

            // Danh sach so luong lop da nhap hanh kiem HKII theo quan huyen va cap
            List<InputStatistics> ListClassHasConductForSecondSemesterCount = PupilRankingBusiness.All
                .Where(o =>
                    o.ClassProfile.SchoolProfile.ProvinceID == Dept.ProvinceID &&
                    o.ClassProfile.SchoolProfile.IsActive == true && (DistrictID == 0 || o.SchoolProfile.DistrictID == DistrictID) &&
                    o.ClassProfile.AcademicYear.Year == Year &&
                    o.Semester == SystemParamsInFile.SEMESTER_OF_YEAR_SECOND &&
                    o.ConductLevelID != null
                )
                .GroupBy(o => new { o.ClassProfile.SchoolProfile.DistrictID, o.ClassProfile.SchoolProfile.SchoolProfileID }).Select(o => new InputStatistics
                {
                    DistrictID = o.Key.DistrictID,
                    SchoolID = o.Key.SchoolProfileID,
                    NumberOfClassHasConductForSecondSemester = o.Select(p => p.ClassID).Distinct().Count()
                }).ToList();

            // Danh sach so luong lop da tong ket diem HKII theo quan huyen va cap
            List<InputStatistics> ListClassHasSumMarkForSecondSemesterCount = PupilRankingBusiness.All
                .Where(o =>
                    o.ClassProfile.SchoolProfile.ProvinceID == Dept.ProvinceID &&
                    o.ClassProfile.SchoolProfile.IsActive == true && (DistrictID == 0 || o.SchoolProfile.DistrictID == DistrictID) &&
                    o.ClassProfile.AcademicYear.Year == Year &&
                    o.Semester == SystemParamsInFile.SEMESTER_OF_YEAR_SECOND &&
                    o.AverageMark != null
                )
                .GroupBy(o => new { o.ClassProfile.SchoolProfile.DistrictID, o.ClassProfile.SchoolProfile.SchoolProfileID }).Select(o => new InputStatistics
                {
                    DistrictID = o.Key.DistrictID,
                    SchoolID = o.Key.SchoolProfileID,
                    NumberOfClassHasSumMarkForSecondSemester = o.Select(p => p.ClassID).Distinct().Count()
                }).ToList();



            // Danh sach so luong user giao vien da cap theo quan huyen va cap
            List<InputStatistics> ListUserCount = UserAccountBusiness.All
                .Where(o =>
                    o.Employee.SchoolProfile.ProvinceID == Dept.ProvinceID &&
                    o.Employee.SchoolProfile.IsActive == true && (DistrictID == 0 || o.Employee.SchoolProfile.DistrictID == DistrictID) &&
                    o.CreatedDate <= ReportDate &&
                    o.IsActive == true
                )
                .GroupBy(o => new { o.Employee.SchoolProfile.DistrictID, o.Employee.SchoolProfile.SchoolProfileID }).Select(o => new InputStatistics
                {
                    DistrictID = o.Key.DistrictID,
                    SchoolID = o.Key.SchoolProfileID,
                    NumberOfUser = o.Count()
                }).ToList();

            // Danh sach so luong user giao vien da login theo quan huyen va cap
            List<InputStatistics> ListLoggedUserCount = UserAccountBusiness.All
                .Where(o =>
                    o.Employee.SchoolProfile.ProvinceID == Dept.ProvinceID &&
                    o.Employee.SchoolProfile.IsActive == true && (DistrictID == 0 || o.Employee.SchoolProfile.DistrictID == DistrictID) &&
                    o.CreatedDate <= ReportDate &&
                    o.FirstLoginDate <= ReportDate &&
                    o.IsActive == true
                )
                .GroupBy(o => new { o.Employee.SchoolProfile.DistrictID, o.Employee.SchoolProfile.SchoolProfileID }).Select(o => new InputStatistics
                {
                    DistrictID = o.Key.DistrictID,
                    SchoolID = o.Key.SchoolProfileID,
                    NumberOfLoggedUser = o.Count()
                }).ToList();



            // Danh sach so luong hoc sinh da nhap sdt ca nhan theo quan huyen va cap
            List<InputStatistics> ListPupilHasMobileCount = PupilOfClassBusiness.All
                .Where(o =>
                    o.SchoolProfile.ProvinceID == Dept.ProvinceID &&
                    o.SchoolProfile.IsActive == true && (DistrictID == 0 || o.SchoolProfile.DistrictID == DistrictID) &&
                    (o.Status == SystemParamsInFile.PUPIL_STATUS_STUDYING || o.Status == SystemParamsInFile.PUPIL_STATUS_GRADUATED) &&
                    o.PupilProfile.Mobile != null
                )
                .GroupBy(o => new { o.SchoolProfile.DistrictID, o.SchoolProfile.SchoolProfileID }).Select(o => new InputStatistics
                {
                    DistrictID = o.Key.DistrictID,
                    SchoolID = o.Key.SchoolProfileID,
                    NumberOfPupilHasMobile = o.Count()
                }).ToList();


            // Danh sach so luong hoc sinh da nhap sdt cua cha theo quan huyen va cap
            List<InputStatistics> ListPupilHasFatherMobileCount = PupilOfClassBusiness.All
                .Where(o =>
                    o.SchoolProfile.ProvinceID == Dept.ProvinceID &&
                    o.SchoolProfile.IsActive == true && (DistrictID == 0 || o.SchoolProfile.DistrictID == DistrictID) &&
                    (o.Status == SystemParamsInFile.PUPIL_STATUS_STUDYING || o.Status == SystemParamsInFile.PUPIL_STATUS_GRADUATED) &&
                    o.PupilProfile.FatherMobile != null
                )
                .GroupBy(o => new { o.SchoolProfile.DistrictID, o.SchoolProfile.SchoolProfileID }).Select(o => new InputStatistics
                {
                    DistrictID = o.Key.DistrictID,
                    SchoolID = o.Key.SchoolProfileID,
                    NumberOfPupilHasFatherMobile = o.Count()
                }).ToList();

            // Danh sach so luong hoc sinh da nhap sdt cua me theo quan huyen va cap
            List<InputStatistics> ListPupilHasMotherMobileCount = PupilOfClassBusiness.All
                .Where(o =>
                    o.SchoolProfile.ProvinceID == Dept.ProvinceID &&
                    o.SchoolProfile.IsActive == true && (DistrictID == 0 || o.SchoolProfile.DistrictID == DistrictID) &&
                    (o.Status == SystemParamsInFile.PUPIL_STATUS_STUDYING || o.Status == SystemParamsInFile.PUPIL_STATUS_GRADUATED) &&
                    o.PupilProfile.MotherMobile != null
                )
                .GroupBy(o => new { o.SchoolProfile.DistrictID, o.SchoolProfile.SchoolProfileID }).Select(o => new InputStatistics
                {
                    DistrictID = o.Key.DistrictID,
                    SchoolID = o.Key.SchoolProfileID,
                    NumberOfPupilHasMotherMobile = o.Count()
                }).ToList();


            List<InputStatistics> ListResult = new List<InputStatistics>();
            List<ReportData> ListData = new List<ReportData>();
            InputStatistics obj = null;
            decimal? tmp = 0;
            foreach (District district in ListDistrict)
            {
                ReportData ReportData = new ReportData();
                ReportData.DistrictName = district.DistrictName;
                //tim ra list truong chi thuoc quan do
                //for tren list do
                List<InputStatistics> lstSchoolProfile = ListSchoolCount.Where(p => p.DistrictID == district.DistrictID).ToList();
                List<InputStatistics> lstPupliprofile = ListPupilCount.Where(p => p.DistrictID == district.DistrictID).ToList();
                List<InputStatistics> lstTeacher = ListTeacherCount.Where(p => p.DistrictID == district.DistrictID).ToList();
                List<InputStatistics> lstClass = ListClassCount.Where(p => p.DistrictID == district.DistrictID).ToList();
                List<InputStatistics> lstClassHasSubject = ListClassHasSubjectCount.Where(p => p.DistrictID == district.DistrictID).ToList();
                List<InputStatistics> lstClassHasAssignForFirstSemester = ListClassHasAssignForFirstSemesterCount.Where(p => p.DistrictID == district.DistrictID).ToList();
                List<InputStatistics> lstClassHasMarkForFirstSemester = ListClassHasMarkForFirstSemesterCount.Where(p => p.DistrictID == district.DistrictID).ToList();
                List<InputStatistics> lstClassHasConductForFirstSemester = ListClassHasConductForFirstSemesterCount.Where(p => p.DistrictID == district.DistrictID).ToList();
                List<InputStatistics> lstClassHasSumMarkForFirstSemester = ListClassHasSumMarkForFirstSemesterCount.Where(p => p.DistrictID == district.DistrictID).ToList();
                List<InputStatistics> lstClassHasAssignForSecondSemester = ListClassHasAssignForSecondSemesterCount.Where(p => p.DistrictID == district.DistrictID).ToList();
                List<InputStatistics> lstClassHasMarkForSecondSemester = ListClassHasMarkForSecondSemesterCount.Where(p => p.DistrictID == district.DistrictID).ToList();
                List<InputStatistics> lstClassHasConductForSecondSemester = ListClassHasConductForSecondSemesterCount.Where(p => p.DistrictID == district.DistrictID).ToList();
                List<InputStatistics> lstClassHasSumMarkForSecondSemester = ListClassHasSumMarkForSecondSemesterCount.Where(p => p.DistrictID == district.DistrictID).ToList();
                List<InputStatistics> lstPupilHasMobile = ListPupilHasMobileCount.Where(p => p.DistrictID == district.DistrictID).ToList();
                List<InputStatistics> lstPupilHasFatherMobile = ListPupilHasFatherMobileCount.Where(p => p.DistrictID == district.DistrictID).ToList();
                List<InputStatistics> lstPupilHasMotherMobile = ListPupilHasMotherMobileCount.Where(p => p.DistrictID == district.DistrictID).ToList();
                foreach (InputStatistics school in lstSchoolProfile)
                {
                    obj = new InputStatistics();
                    obj.DistrictID = district.DistrictID;
                    obj.DistrictName = district.DistrictName;
                    obj.SchoolName = school.SchoolName;

                    // So luong truong         
                    tmp = lstSchoolProfile.Where(o => o.SchoolID == school.SchoolID).Sum(o => o.NumberOfSchool);
                    obj.NumberOfSchool = tmp == null ? 0 : (int)tmp.Value;

                    // So hoc sinh                  
                    tmp = lstPupliprofile.Where(o => o.SchoolID == school.SchoolID).Sum(o => o.NumberOfPupil);
                    obj.NumberOfPupil = tmp == null ? 0 : (int)tmp.Value;

                    // So giao vien
                    tmp = lstTeacher.Where(o => o.SchoolID == school.SchoolID).Sum(o => o.NumberOfTeacher);
                    obj.NumberOfTeacher = tmp == null ? 0 : (int)tmp.Value;

                    // So lop
                    tmp = lstClass.Where(o => o.SchoolID == school.SchoolID).Sum(o => o.NumberOfClass);
                    obj.NumberOfClass = tmp == null ? 0 : (int)tmp.Value;

                    // So lop da khai bao mon hoc
                    tmp = lstClassHasSubject.Where(o => o.SchoolID == school.SchoolID).Sum(o => o.NumberOfClassHasSubject);
                    obj.NumberOfClassHasSubject = tmp == null ? 0 : (int)tmp.Value;

                    // So lop da phan cong giang day HKI
                    tmp = lstClassHasAssignForFirstSemester.Where(o => o.SchoolID == school.SchoolID).Sum(o => o.NumberOfClassHasAssignForFirstSemester);
                    obj.NumberOfClassHasAssignForFirstSemester = tmp == null ? 0 : (int)tmp.Value;

                    // So lop da nhap diem HKI
                    tmp = lstClassHasMarkForFirstSemester.Where(o => o.SchoolID == school.SchoolID).Sum(o => o.NumberOfClassHasMarkForFirstSemester);
                    obj.NumberOfClassHasMarkForFirstSemester = tmp == null ? 0 : (int)tmp.Value;

                    // So lop da nhap hanh kiem HKI
                    tmp = lstClassHasConductForFirstSemester.Where(o => o.SchoolID == school.SchoolID).Sum(o => o.NumberOfClassHasConductForFirstSemester);
                    obj.NumberOfClassHasConductForFirstSemester = tmp == null ? 0 : (int)tmp.Value;

                    // So lop da tong ket HKI
                    tmp = lstClassHasSumMarkForFirstSemester.Where(o => o.SchoolID == school.SchoolID).Sum(o => o.NumberOfClassHasSumMarkForFirstSemester);
                    obj.NumberOfClassHasSumMarkForFirstSemester = tmp == null ? 0 : (int)tmp.Value;


                    // So lop da phan cong giang day HKII
                    tmp = lstClassHasAssignForSecondSemester.Where(o => o.SchoolID == school.SchoolID).Sum(o => o.NumberOfClassHasAssignForSecondSemester);
                    obj.NumberOfClassHasAssignForSecondSemester = tmp == null ? 0 : (int)tmp.Value;

                    // So lop da nhap diem HKII
                    tmp = lstClassHasMarkForSecondSemester.Where(o => o.SchoolID == school.SchoolID).Sum(o => o.NumberOfClassHasMarkForSecondSemester);
                    obj.NumberOfClassHasMarkForSecondSemester = tmp == null ? 0 : (int)tmp.Value;

                    // So lop da nhap hanh kiem HKII
                    tmp = lstClassHasConductForSecondSemester.Where(o => o.SchoolID == school.SchoolID).Sum(o => o.NumberOfClassHasConductForSecondSemester);
                    obj.NumberOfClassHasConductForSecondSemester = tmp == null ? 0 : (int)tmp.Value;

                    // So lop da tong ket HKII
                    tmp = lstClassHasSumMarkForSecondSemester.Where(o => o.SchoolID == school.SchoolID).Sum(o => o.NumberOfClassHasSumMarkForSecondSemester);
                    obj.NumberOfClassHasSumMarkForSecondSemester = tmp == null ? 0 : (int)tmp.Value;

                    // So lop da nhap sdt ca nhan
                    tmp = lstPupilHasMobile.Where(o => o.SchoolID == school.SchoolID).Sum(o => o.NumberOfPupilHasMobile);
                    obj.NumberOfPupilHasMobile = tmp == null ? 0 : (int)tmp.Value;

                    // So lop da nhap sdt ca nhan
                    tmp = lstPupilHasFatherMobile.Where(o => o.SchoolID == school.SchoolID).Sum(o => o.NumberOfPupilHasFatherMobile);
                    obj.NumberOfPupilHasFatherMobile = tmp == null ? 0 : (int)tmp.Value;

                    // So lop da nhap sdt ca nhan
                    tmp = lstPupilHasMotherMobile.Where(o => o.SchoolID == school.SchoolID).Sum(o => o.NumberOfPupilHasMotherMobile);
                    obj.NumberOfPupilHasMotherMobile = tmp == null ? 0 : (int)tmp.Value;

                    obj.GradeID = school.GradeID;
                    obj.ListGrade = string.Join(", ", Utils.GetListGradeFromEducationGrade(school.GradeID));
                    ReportData.ListInputStatistics.Add(obj);
                }

                ListData.Add(ReportData);
            }

            return ListData;
        }


        private ProcessedReport InsertProcessedReport(string ReportCode, int UserAccountID, int SupervisingDeptID, int DistrictID, int Year, DateTime ReportDate, Stream Data)
        {
            ProcessedReport ProcessedReport = new ProcessedReport();

            ProcessedReport.ReportCode = ReportCode;
            ProcessedReport.ProcessedDate = DateTime.Now;
            ProcessedReport.InputParameterHashKey = ReportUtils.GetHashKey(new Dictionary<string, object>{
                {"UserAccountID", UserAccountID},
                {"SupervisingDeptID", SupervisingDeptID},
                {"DistrictID", DistrictID},
                {"Year", Year},
                {"ReportDate", ReportDate}
            });
            ProcessedReport.ReportData = ReportUtils.Compress(Data);

            //Tạo tên file
            ReportDefinition reportDef = ReportDefinitionBusiness.GetByCode(ProcessedReport.ReportCode);
            string outputNamePattern = reportDef.OutputNamePattern;
            string DeptName = SupervisingDeptBusiness.Find(SupervisingDeptID).Province.ProvinceName;


            outputNamePattern = outputNamePattern.Replace("[PGD/SGD]", ReportUtils.StripVNSign(DeptName));
            ProcessedReport.ReportName = ReportUtils.RemoveSpecialCharacters(outputNamePattern) + "." + reportDef.OutputFormat;

            IDictionary<string, object> dic = new Dictionary<string, object>{
                {"UserAccountID", UserAccountID},
                {"SupervisingDeptID", SupervisingDeptID},
                {"DistrictID", DistrictID},
                {"Year", Year},
                {"ReportDate", ReportDate}
            };

            ProcessedReportParameterRepository.Insert(dic, ProcessedReport);
            ProcessedReportRepository.Insert(ProcessedReport);

            ProcessedReportParameterRepository.Save();
            ProcessedReportRepository.Save();

            return ProcessedReport;
        }


        private class ReportData
        {
            public string DistrictName { get; set; }
            public List<InputStatistics> ListInputStatistics { get; set; }

            public ReportData()
            {
                ListInputStatistics = new List<InputStatistics>();
            }
        }
    }
}
