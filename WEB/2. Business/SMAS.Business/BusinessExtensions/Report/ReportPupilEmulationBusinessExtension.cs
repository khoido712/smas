﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Linq.Expressions;
using System.Web;
using log4net;

using SMAS.DAL.Repository;
using SMAS.DAL.IRepository;
using SMAS.Business.IBusiness;
using SMAS.Models.Models;
using SMAS.Business.BusinessObject;
using SMAS.Business.Common;
using System.IO;
using SMAS.VTUtils.Excel.Export;

namespace SMAS.Business.Business
{
    public class ReportPupilEmulationBusiness : GenericBussiness<ProcessedReport>, IReportPupilEmulationBusiness
    {
        IProcessedReportParameterRepository ProcessedReportParameterRepository;
        IProcessedReportRepository ProcessedReportRepository;
        IEducationLevelRepository EducationLevelRepository;
        IAcademicYearRepository AcademicYearRepository;
        IPupilEmulationRepository PupilEmulationRepository;
        IPupilProfileRepository PupilProfileRepository;
        ISummedUpRecordRepository SummedUpRecordRepository;

        public ReportPupilEmulationBusiness(ILog logger, SMASEntities context = null)
            : base(logger)
        {
            if (context == null) { context = new SMASEntities(); } this.context = context;
            this.ProcessedReportParameterRepository = new ProcessedReportParameterRepository(context);
            this.ProcessedReportRepository = new ProcessedReportRepository(context);
            this.EducationLevelRepository = new EducationLevelRepository(context);
            this.AcademicYearRepository = new AcademicYearRepository(context);
            this.PupilEmulationRepository = new PupilEmulationRepository(context);
            this.PupilProfileRepository = new PupilProfileRepository(context);
            this.SummedUpRecordRepository = new SummedUpRecordRepository(context);
        }

        #region Tạo mảng băm cho các tham số đầu vào cho báo cáo
        public string GetHashKeyPrimary(ReportPupilEmulationSearchBO entity)
        {
            IDictionary<string, object> dic = new Dictionary<string, object> {
                {"AcademicYearID", entity.AcademicYearID},
                {"SchoolID", entity.SchoolID},
                {"Semester", entity.Semester}
            };
            return ReportUtils.GetHashKey(dic);
        }
        #endregion

        #region Lấy danh sách thống kê danh hiệu thi đua cấp 1 được cập nhật mới nhất
        public ProcessedReport GetReportPupilEmulationPrimary(ReportPupilEmulationSearchBO entity)
        {
            string reportCode = SystemParamsInFile.REPORT_DS_THONG_KE_XEP_DANH_HIEU_THI_DUA_CAP1;
            string inputParameterHashKey = GetHashKeyPrimary(entity);
            return ProcessedReportBusiness.GetProcessedReport(reportCode, inputParameterHashKey);
        }
        #endregion

        #region Lưu báo cáo danh sách thống kê danh hiệu thi đua cấp 1 vào CSDL
        public ProcessedReport InsertReportPupilEmulationPrimary(ReportPupilEmulationSearchBO entity, Stream data)
        {
            string reportCode = SystemParamsInFile.REPORT_DS_THONG_KE_XEP_DANH_HIEU_THI_DUA_CAP1;
            ReportDefinition reportDef = ReportDefinitionBusiness.GetByCode(reportCode);
            ProcessedReport pr = new ProcessedReport();
            pr.ReportCode = reportCode;
            pr.ProcessedDate = DateTime.Now;
            pr.InputParameterHashKey = GetHashKeyPrimary(entity);
            pr.ReportData = ReportUtils.Compress(data);

            //Tạo tên file
            string outputNamePattern = reportDef.OutputNamePattern;
            string semester = ReportUtils.ConvertSemesterForReportName(entity.Semester);

            outputNamePattern = outputNamePattern.Replace("[Semester]", semester);
            pr.ReportName = ReportUtils.RemoveSpecialCharacters(outputNamePattern) + "." + reportDef.OutputFormat;

            IDictionary<string, object> dic = new Dictionary<string, object> {
                {"AcademicYearID", entity.AcademicYearID},
                {"SchoolID", entity.SchoolID},
                {"Semester", entity.Semester}
            };
            ProcessedReportParameterRepository.Insert(dic, pr);
            ProcessedReportRepository.Insert(pr);
            ProcessedReportRepository.Save();
            return pr;
        }
        #endregion

        #region Tạo file báo cáo thống kê danh hiệu thi đua cấp 1
        public Stream CreateReportPupilEmulationPrimary(ReportPupilEmulationSearchBO entity)
        {
            string reportCode = SystemParamsInFile.REPORT_DS_THONG_KE_XEP_DANH_HIEU_THI_DUA_CAP1;
            ReportDefinition reportDef = ReportDefinitionBusiness.GetByCode(reportCode);
            string templatePath = ReportUtils.GetTemplatePath(reportDef);
            IVTWorkbook oBook = VTExport.OpenWorkbook(templatePath);

            int firstRow = 7;
            int defaultNumbeOfColumn = 2;
            int firstDynamicCol = VTVector.dic['E'];
            int defaultLastCol = VTVector.dic['H'];
            //Lấy sheet template
            IVTWorksheet firstSheet = oBook.GetSheet(1);
            IVTRange subjectTemp = firstSheet.GetRange(firstRow, firstDynamicCol, firstRow + 6, firstDynamicCol + 1);

            //Tạo template chuẩn
            IVTWorksheet tempSheet = oBook.CopySheetToLast(firstSheet, "H" + (firstRow - 1));
            tempSheet.CopyPasteSameSize(firstSheet.GetRange(firstRow, 1, firstRow + 6, firstDynamicCol - 1), "A" + firstRow);
            tempSheet.CopyPaste(firstSheet.GetRange(firstRow + 8, 1, firstRow + 8, 100), firstRow + 8, 1);

            //Lấy danh sách các cột động
            List<HonourAchivementType> listSC = HonourAchivementTypeBusiness.Search(new Dictionary<string, object>
            {
                {"Type", SystemParamsInFile.HONOUR_ACHIVEMENT_TYPE_PUPIL}
            }).OrderBy(x => x.HonourAchivementTypeID).ToList<HonourAchivementType>();


            //Tạo các cột động
            Dictionary<int, int> dicStatisticsConfigToColumn = new Dictionary<int, int>();
            for (int i = 0; i < listSC.Count; i++)
            {
                HonourAchivementType sc = listSC[i];
                tempSheet.CopyPasteSameSize(subjectTemp, firstRow, firstDynamicCol + i * 2);
                tempSheet.SetCellValue(firstRow, firstDynamicCol + i * 2, sc.Resolution);
                dicStatisticsConfigToColumn.Add(sc.HonourAchivementTypeID, firstDynamicCol + i * 2);
            }

            int lastCol = defaultLastCol + (listSC.Count - defaultNumbeOfColumn) * 2;

            //Chỉnh lại dòng khác
            if (listSC.Count > defaultNumbeOfColumn)
            {
                tempSheet.MergeRow(2, VTVector.dic['E'], lastCol);
                tempSheet.MergeRow(3, VTVector.dic['E'], lastCol);
                tempSheet.MergeRow(4, VTVector.dic['A'], lastCol);
                tempSheet.MergeRow(5, VTVector.dic['A'], lastCol);

                double colWidth = firstSheet.GetColumnWidth(firstDynamicCol);
                double resizeWidth = colWidth * defaultNumbeOfColumn / listSC.Count;
                for (int i = firstDynamicCol; i <= lastCol; i++)
                {
                    tempSheet.SetColumnWidth(i, resizeWidth);
                }
            }

            //Tính các công thức 
            string formulaPercent = "=IF(cellNumberOfPupil=0;0;ROUND(cellTotal/cellNumberOfPupil;4)*100)";

            for (int i = 0; i < listSC.Count; i++)
            {
                HonourAchivementType sc = listSC[i];
                tempSheet.SetCellValue(firstRow + 2, dicStatisticsConfigToColumn[sc.HonourAchivementTypeID], "${list[].Get(" + sc.HonourAchivementTypeID + ")}");
            }

            for (int rowIndex = firstRow + 2; rowIndex < firstRow + 7; rowIndex++)
            {
                for (int i = firstDynamicCol; i <= lastCol; i += 2)
                {
                    string cellNumberOfPupil = "D" + rowIndex;
                    string cellTotal = new VTVector(rowIndex, i).ToString();
                    tempSheet.SetFormulaValue(rowIndex, i + 1, formulaPercent.
                        Replace("cellTotal", cellTotal).Replace("cellNumberOfPupil", cellNumberOfPupil));
                }
            }

            //Fill dữ liệu chung cho template
            AcademicYear academicYear = AcademicYearBusiness.Find(entity.AcademicYearID);
            SchoolProfile school = SchoolProfileBusiness.Find(entity.SchoolID);
            Province province = school.Province;

            string schoolName = school.SchoolName.ToUpper();
            string supervisingDeptName = school.SupervisingDept.SupervisingDeptName.ToUpper();
            string academicYearTitle = academicYear.DisplayTitle;
            string provinceName = province.ProvinceName;
            DateTime reportDate = DateTime.Now;
            string semester = SMASConvert.ConvertSemester(entity.Semester).ToUpper();

            Dictionary<string, object> dicGeneralInfo = new Dictionary<string, object> 
                {
                    {"SchoolName", schoolName}, 
                    {"SupervisingDeptName", supervisingDeptName},
                    {"AcademicYearTitle", academicYearTitle},
                    {"ProvinceName", provinceName},
                    {"ReportDate", reportDate},
                    {"Semester", semester}
                };

            tempSheet.GetRange(1, 1, firstRow - 1, defaultLastCol).FillVariableValue(dicGeneralInfo);

            //Tạo sheet Fill dữ liệu
            int lastColumn = Math.Max(lastCol, defaultLastCol);
            IVTWorksheet dataSheet = oBook.CopySheetToLast(tempSheet,
                new VTVector(firstRow + 1, lastColumn).ToString());
            dataSheet.Name = "DHTD";
            //Tạo vùng cho từng khối
            IVTRange summarizeRange = tempSheet.GetRow(firstRow + 5);
            IVTRange topRange = tempSheet.GetRow(firstRow + 2);
            IVTRange midRange = tempSheet.GetRow(firstRow + 3);
            IVTRange lastRange = tempSheet.GetRow(firstRow + 4);

            //fill dữ liệu cho các khối
            List<EducationLevel> listEducationLevel = EducationLevelBusiness.GetByGrade(
                SystemParamsInFile.APPLIED_LEVEL_PRIMARY).OrderBy(x => x.EducationLevelID).ToList();

            List<PupilEmulation> listPupilEmulationAll = PupilEmulationBusiness.SearchBySchool(entity.SchoolID, new Dictionary<string, object>
            {
                {"AcademicYearID", entity.AcademicYearID},
                {"AppliedLevel", SystemParamsInFile.APPLIED_LEVEL_PRIMARY},
                {"Semester", entity.Semester}
            }).ToList<PupilEmulation>();

            //Danh sách các lớp
            List<ClassProfile> listClass = ClassProfileBusiness.SearchBySchool(entity.SchoolID, new Dictionary<string, object>
            {
                {"AcademicYearID", entity.AcademicYearID},
                {"AppliedLevel", SystemParamsInFile.APPLIED_LEVEL_PRIMARY}
            }).ToList<ClassProfile>();

            //Lấy danh sách học sinh
            IQueryable<PupilOfClass> listPupil = PupilOfClassBusiness.SearchBySchool(entity.SchoolID, new Dictionary<string, object> 
            {
                {"AcademicYearID", entity.AcademicYearID}, 
                {"AppliedLevel", SystemParamsInFile.APPLIED_LEVEL_PRIMARY},
                //{"Check", "Check"},
                //{"Status", new List<int>{SystemParamsInFile.PUPIL_STATUS_STUDYING, SystemParamsInFile.PUPIL_STATUS_GRADUATED}}
            });
            //Si so thuc te
            IQueryable<PupilOfClass> listpupilTT = listPupil.AddCriteriaSemester(academicYear,entity.Semester);
            // Lay hoc sinh thuoc lop dang hoc hoac da tot nghiep
            List<PupilEmulation> listPupilEmulation = (from u in listPupilEmulationAll
                                                       join v in listpupilTT on new { u.ClassID, u.PupilID } equals new { v.ClassID, v.PupilID }
                                                       select u).ToList();

            List<int> listBeginRow = new List<int>();
            int beginRow = firstRow + 2;
            int indexEducation = 0;
            int numRow = firstRow + 2;
            foreach (EducationLevel educationLevel in listEducationLevel)
            {
                int endRow = beginRow + 1;
                indexEducation++;
                
                List<ClassProfile> listCP = (from lc in listClass
                                             where lc.EducationLevelID == educationLevel.EducationLevelID
                                             orderby lc.DisplayName
                                             select lc).ToList<ClassProfile>();

                List<object> listData = new List<object>();
                if (listCP == null || listCP.Count == 0)
                {
                    dataSheet.CopyPasteSameSize(summarizeRange, beginRow, 1);
                    foreach (HonourAchivementType sc in listSC)
                    {
                        dataSheet.SetCellValue(beginRow, dicStatisticsConfigToColumn[sc.HonourAchivementTypeID], 0);
                    }
                    dataSheet.SetCellValue("C" + beginRow, 0);
                    dataSheet.SetCellValue("D" + beginRow, 0);
                    endRow = beginRow;
                }
                else
                {
                    //Fill dữ liệu dòng tổng
                    endRow = beginRow + listCP.Count();

                    //Tạo khung dữ liệu danh sách các lớp
                    for (int i = beginRow; i < endRow; i++)
                    {
                        IVTRange copyRange;
                        if (i == beginRow)
                        {
                            copyRange = topRange;
                        }
                        else if (i == endRow - 1)
                        {
                            copyRange = lastRange;
                        }
                        else
                        {
                            copyRange = midRange;
                        }
                        dataSheet.CopyPasteSameSize(copyRange, "A" + i);
                    }
                    dataSheet.CopyPasteSameSize(summarizeRange, endRow, 1);
                    dataSheet.MergeColumn('A', beginRow, endRow);
                    //string fomula = "=SUM(#" + beginRow + ":" + "#" + (endRow-1) + ")";
                    //foreach (HonourAchivementType sc in listSC)
                    //{
                    //    int col = dicStatisticsConfigToColumn[sc.HonourAchivementTypeID];
                    //    dataSheet.SetFormulaValue(endRow,
                    //        col, fomula.Replace("#", VTVector.ColumnIntToString(col)));
                    //}
                    //dataSheet.SetFormulaValue("C" + endRow, fomula.Replace("#", "C"));
                    //dataSheet.SetFormulaValue("D" + endRow, fomula.Replace("#", "D"));

                    //Tạo dữ liệu cần fill
                    int indexClass = 0;
                    List<int> listStatus = new List<int> { SystemParamsInFile.PUPIL_STATUS_STUDYING, SystemParamsInFile.PUPIL_STATUS_GRADUATED };
                    foreach (ClassProfile cp in listCP)
                    {
                        indexClass++;
                        Dictionary<string, object> dicData = new Dictionary<string, object>();
                        dicData["ClassName"] = cp.DisplayName;

                        int numberOfPupil = (from mr in listPupil
                                             where mr.ClassID == cp.ClassProfileID
                                             select mr.PupilOfClassID).Count();
                        dicData["NumberOfPupil"] = numberOfPupil;

                        int realNumberOfPupil = (from mr in listpupilTT
                                                 where mr.ClassID == cp.ClassProfileID
                                                 && listStatus.Contains(mr.Status)
                                                 select mr.PupilOfClassID).Count();
                        dicData["RealNumberOfPupil"] = realNumberOfPupil;

                        foreach (HonourAchivementType sc in listSC)
                        {
                            int numberOf = (from mr in listPupilEmulation
                                            where mr.ClassID == cp.ClassProfileID
                                            && mr.HonourAchivementTypeID == sc.HonourAchivementTypeID
                                            select mr.PupilEmulationID).Count();
                            dicData[sc.HonourAchivementTypeID.ToString()] = numberOf;
                        }
                        listData.Add(dicData);
                    }
                    listBeginRow.Add(endRow);
                }

                //Fill dữ liệu
                dataSheet.GetRange(beginRow, 1, endRow, lastColumn).FillVariableValue(new Dictionary<string, object>
                    {
                        {"list",listData},
                        {"EducationLevel",educationLevel.Resolution}
                    });
                beginRow = endRow + 1;

                dataSheet.SetFormulaValue("C" + (beginRow - 1), "=SUM(C" + (numRow).ToString() + ":C" + (endRow - 1).ToString() + ")");
                dataSheet.SetFormulaValue("D" + (beginRow - 1), "=SUM(D" + (numRow).ToString() + ":D" + (endRow - 1).ToString() + ")");
                dataSheet.SetFormulaValue("E" + (beginRow - 1), "=SUM(E" + (numRow).ToString() + ":E" + (endRow - 1).ToString() + ")");
                dataSheet.SetFormulaValue("G" + (beginRow - 1), "=SUM(G" + (numRow).ToString() + ":G" + (endRow - 1).ToString() + ")");
                numRow = numRow + listCP.Count() + 1;
                //Tính vị trí tiếp theo              
            }

            //Insert row tổng
            dataSheet.CopyPasteSameSize(tempSheet.GetRow(firstRow + 6), beginRow, 1);
            //string formulaSum = "=SUM(#" + string.Join(";#", listBeginRow.ToArray<int>()) + ")";

            //foreach (HonourAchivementType sc in listSC)
            //{
            //    int col = dicStatisticsConfigToColumn[sc.HonourAchivementTypeID];
            //    dataSheet.SetFormulaValue(beginRow,
            //        col, formulaSum.Replace("#", VTVector.ColumnIntToString(col)));
            //}
            dataSheet.SetFormulaValue("C" + beginRow, "=SUM(C" + (firstRow + 2).ToString() + ":C" + (listBeginRow.LastOrDefault()).ToString() + ")/2");
            dataSheet.SetFormulaValue("D" + beginRow, "=SUM(D" + (firstRow + 2).ToString() + ":D" + (listBeginRow.LastOrDefault()).ToString() + ")/2");
            dataSheet.SetFormulaValue("E" + beginRow, "=SUM(E" + (firstRow + 2).ToString() + ":E" + (listBeginRow.LastOrDefault()).ToString() + ")/2");
            dataSheet.SetFormulaValue("G" + beginRow, "=SUM(G" + (firstRow + 2).ToString() + ":G" + (listBeginRow.LastOrDefault()).ToString() + ")/2");


            dataSheet.CopyPaste(tempSheet.GetRow(firstRow + 8), beginRow + 1, 1);
            dataSheet.FitAllColumnsOnOnePage = true;
            //Xoá sheet template
            firstSheet.Delete();
            tempSheet.Delete();

            return oBook.ToStream();
        }
        #endregion


    }
}
