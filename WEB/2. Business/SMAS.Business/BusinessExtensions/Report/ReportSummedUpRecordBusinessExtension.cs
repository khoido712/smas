﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Linq.Expressions;
using System.Web;
using log4net;

using SMAS.DAL.Repository;
using SMAS.DAL.IRepository;
using SMAS.Business.IBusiness;
using SMAS.Models.Models;
using SMAS.Business.BusinessObject;
using SMAS.Business.Common;
using System.IO;
using SMAS.VTUtils.Excel.Export;

namespace SMAS.Business.Business
{
    public class ReportSummedUpRecordBusiness : GenericBussiness<ProcessedReport>, IReportSummedUpRecordBusiness
    {

        IProcessedReportParameterRepository ProcessedReportParameterRepository;
        IProcessedReportRepository ProcessedReportRepository;
        IEducationLevelRepository EducationLevelRepository;
        IAcademicYearRepository AcademicYearRepository;
        ISummedUpRecordRepository SummedUpRecordRepository;
        IPupilProfileRepository PupilProfileRepository;
        IClassSubjectRepository ClassSubjectRepository;

        public ReportSummedUpRecordBusiness(ILog logger, SMASEntities context = null)
            : base(logger)
        {
            if (context == null) { context = new SMASEntities(); } this.context = context;
            this.ProcessedReportParameterRepository = new ProcessedReportParameterRepository(context);
            this.ProcessedReportRepository = new ProcessedReportRepository(context);
            this.EducationLevelRepository = new EducationLevelRepository(context);
            this.AcademicYearRepository = new AcademicYearRepository(context);
            this.SummedUpRecordRepository = new SummedUpRecordRepository(context);
            this.PupilProfileRepository = new PupilProfileRepository(context);
            this.ClassSubjectRepository = new ClassSubjectRepository(context);
        }

        #region Tạo mảng băm cho các tham số đầu vào cho báo cáo
        public string GetHashKeyPrimary(ReportSummedUpRecordSearchBO entity)
        {
            IDictionary<string, object> dic = new Dictionary<string, object> {
                {"AcademicYearID", entity.AcademicYearID},
                {"EducationLevelID ", entity.EducationLevelID},
                {"SchoolID", entity.SchoolID},
                {"Semester", entity.Semester}
            };
            return ReportUtils.GetHashKey(dic);
        }
        #endregion

        #region Lấy danh sách thống kê xếp loại chi tiết môn cấp 1 được cập nhật mới nhất
        public ProcessedReport GetReportSummedUpRecordGroupByClassPrimary(ReportSummedUpRecordSearchBO entity)
        {
            string reportCode = SystemParamsInFile.REPORT_DS_XEP_LOAI_CHI_TIET_CAP1;
            string inputParameterHashKey = GetHashKeyPrimary(entity);
            return ProcessedReportBusiness.GetProcessedReport(reportCode, inputParameterHashKey);
        }
        #endregion

        #region Lưu báo cáo danh sách điểm học kỳ cấp 1 vào CSDL
        public ProcessedReport InsertReportSummedUpRecordGroupByClassPrimary(ReportSummedUpRecordSearchBO entity, Stream data)
        {
            string reportCode = SystemParamsInFile.REPORT_DS_XEP_LOAI_CHI_TIET_CAP1;
            ReportDefinition reportDef = ReportDefinitionBusiness.GetByCode(reportCode);
            ProcessedReport pr = new ProcessedReport();
            pr.ReportCode = reportCode;
            pr.ProcessedDate = DateTime.Now;
            pr.InputParameterHashKey = GetHashKeyPrimary(entity);
            pr.ReportData = ReportUtils.Compress(data);

            //Tạo tên file
            string outputNamePattern = reportDef.OutputNamePattern;
            string semester = ReportUtils.ConvertSemesterForReportName(entity.Semester);
            outputNamePattern = outputNamePattern.Replace("[Semester]", semester);
            string educationLevelOrClass = "";
            if (entity.EducationLevelID != 0)
            {
                educationLevelOrClass = EducationLevelRepository.Find(entity.EducationLevelID).Resolution;
                outputNamePattern = outputNamePattern.Replace("[EducationLevel/Class]", ReportUtils.StripVNSign(educationLevelOrClass));
            }
            else
            {
                outputNamePattern = outputNamePattern.Replace("[EducationLevel/Class]_", "");
            }
            
            pr.ReportName = ReportUtils.RemoveSpecialCharacters(outputNamePattern) + "." + reportDef.OutputFormat;

            IDictionary<string, object> dic = new Dictionary<string, object> {
                {"AcademicYearID", entity.AcademicYearID},
                {"EducationLevelID ", entity.EducationLevelID},
                {"SchoolID", entity.SchoolID},
                {"Semester", entity.Semester}
            };
            ProcessedReportParameterRepository.Insert(dic, pr);
            ProcessedReportRepository.Insert(pr);
            ProcessedReportRepository.Save();
            return pr;
        }
        #endregion

        #region Tạo file báo cáo thống kê xếp loại chi tiết môn cấp 1
        public Stream CreateReportSummedUpRecordGroupByClassPrimary(ReportSummedUpRecordSearchBO entity)
        {
            List<int> listStatus = new List<int> { SystemParamsInFile.PUPIL_STATUS_STUDYING, SystemParamsInFile.PUPIL_STATUS_GRADUATED };
            string reportCode = SystemParamsInFile.REPORT_DS_XEP_LOAI_CHI_TIET_CAP1;
            ReportDefinition reportDef = ReportDefinitionBusiness.GetByCode(reportCode);
            string templatePath = ReportUtils.GetTemplatePath(reportDef);
            IVTWorkbook oBook = VTExport.OpenWorkbook(templatePath);

            int firstRow = 9;
            int defaultNumbeOfColumn = 7;
            int firstDynamicCol = VTVector.dic['E'];
            int defaultLastCol = VTVector.dic['R'];
            //Lấy sheet template
            IVTWorksheet firstSheet = oBook.GetSheet(1);
            IVTRange subjectTemp = firstSheet.GetRange(firstRow, firstDynamicCol, firstRow + 6, firstDynamicCol + 1);

            //Tạo template chuẩn
            IVTWorksheet tempSheet = oBook.CopySheetToLast(firstSheet, "R" + (firstRow - 1));
            tempSheet.CopyPasteSameSize(firstSheet.GetRange(firstRow, 1, firstRow + 6, firstDynamicCol - 1), "A" + firstRow);
            tempSheet.CopyPaste(firstSheet.GetRange(firstRow + 8, 1, firstRow + 8, 100), firstRow + 8, 1);

            //Lấy danh sách các cột động
            List<StatisticsConfig> listSC = StatisticsConfigBusiness.GetList(
                SystemParamsInFile.APPLIED_LEVEL_PRIMARY,
                SystemParamsInFile.StatisticsConfig.REPORT_TYPE_HOC_LUC_MON,
                SystemParamsInFile.StatisticsConfig.MARK_TYPE_ALL,
                SystemParamsInFile.StatisticsConfig.SUBJECT_TYPE_ALL);


            //Tạo các cột động
            Dictionary<StatisticsConfig, int> dicStatisticsConfigToColumn = new Dictionary<StatisticsConfig, int>();
            for (int i = 0; i < listSC.Count; i++)
            {
                StatisticsConfig sc = listSC[i];
                tempSheet.CopyPasteSameSize(subjectTemp, firstRow, firstDynamicCol + i * 2);
                tempSheet.SetCellValue(firstRow, firstDynamicCol + i * 2, sc.DisplayName);
                dicStatisticsConfigToColumn.Add(sc, firstDynamicCol + i * 2);
            }


            int lastCol = defaultLastCol + (listSC.Count - defaultNumbeOfColumn) * 2;

            //Chỉnh lại dòng khác
            if (listSC.Count > defaultNumbeOfColumn)
            {
                tempSheet.MergeRow(2, VTVector.dic['K'], lastCol);
                tempSheet.MergeRow(3, VTVector.dic['K'], lastCol);
                tempSheet.MergeRow(4, VTVector.dic['K'], lastCol);
                tempSheet.MergeRow(6, VTVector.dic['A'], lastCol);
                tempSheet.MergeRow(7, VTVector.dic['A'], lastCol);

                double colWidth = firstSheet.GetColumnWidth(firstDynamicCol);
                double resizeWidth = colWidth * defaultNumbeOfColumn / listSC.Count;
                for (int i = firstDynamicCol; i <= lastCol; i++)
                {
                    tempSheet.SetColumnWidth(i, resizeWidth);
                }
            }

            //Tính các công thức 
            string formulaPercent = "=IF(cellNumberOfPupil=0;0;ROUND(cellTotal/cellNumberOfPupil;4)*100)";

            for (int i = 0; i < listSC.Count; i++)
            {
                StatisticsConfig sc = listSC[i];
                tempSheet.SetCellValue(firstRow + 2, dicStatisticsConfigToColumn[sc], "${list[].Get(" + sc.StatisticsConfigID + ")}");
            }

            for (int rowIndex = firstRow + 2; rowIndex < firstRow + 6; rowIndex++)
            {
                for (int i = firstDynamicCol; i <= lastCol; i += 2)
                {
                    string cellNumberOfPupil = "D" + rowIndex;
                    string cellTotal = new VTVector(rowIndex, i).ToString();
                    tempSheet.SetFormulaValue(rowIndex, i + 1, formulaPercent.
                        Replace("cellTotal", cellTotal).Replace("cellNumberOfPupil", cellNumberOfPupil));
                }
            }

            //Fill dữ liệu chung cho template
            AcademicYear academicYear = AcademicYearBusiness.Find(entity.AcademicYearID);
            SchoolProfile school = SchoolProfileBusiness.Find(entity.SchoolID);
            Province province = school.Province;

            string schoolName = school.SchoolName.ToUpper();
            string supervisingDeptName = school.SupervisingDept.SupervisingDeptName.ToUpper();
            string academicYearTitle = academicYear.DisplayTitle;
            string provinceName = province.ProvinceName;
            DateTime reportDate = DateTime.Now;
            string semester = SMASConvert.ConvertSemester(entity.Semester);

            Dictionary<string, object> dicGeneralInfo = new Dictionary<string, object> 
                {
                    {"SchoolName", schoolName}, 
                    {"SupervisingDeptName", supervisingDeptName},
                    {"AcademicYearTitle", academicYearTitle},
                    {"ProvinceName", provinceName},
                    {"ReportDate", reportDate},
                    {"Semester", semester}
                };

            //Tính dòng cuôi cùng
            int lastColumn = Math.Max(lastCol, defaultLastCol);

            //Lấy danh sách các khối
            List<EducationLevel> listEducationLevel = new List<EducationLevel>();
            if (entity.EducationLevelID == 0)
            {
                listEducationLevel = EducationLevelBusiness.GetByGrade(
                SystemParamsInFile.APPLIED_LEVEL_PRIMARY).OrderBy(x => x.EducationLevelID).ToList();
            }
            else
            {
                listEducationLevel.Add(EducationLevelBusiness.Find(entity.EducationLevelID));
            }

            //Lấy danh sách các lớp
            List<ClassProfile> listClass = ClassProfileBusiness.SearchBySchool(entity.SchoolID, new Dictionary<string, object>
            {
                {"AcademicYearID", entity.AcademicYearID},
                {"AppliedLevel", SystemParamsInFile.APPLIED_LEVEL_PRIMARY}
            }).ToList<ClassProfile>();

            //Lấy danh sách học sinh
            AcademicYear Aca = AcademicYearBusiness.Find(entity.AcademicYearID);
            Dictionary<string, object> dicPupilRanking = new Dictionary<string, object> 
            {
                {"AcademicYearID", entity.AcademicYearID},
                {"SchoolID", entity.SchoolID},
               // {"Check", "Check"},
               //{"Semester",entity.Semester},
                {"AppliedLevel",  SystemParamsInFile.APPLIED_LEVEL_PRIMARY}
            };
            IQueryable<PupilOfClass> listPupil = PupilOfClassBusiness.SearchBySchool(entity.SchoolID, dicPupilRanking).GetPupil(academicYear,entity.Semester);

            //Si so thuc te.lay ca nhung hoc sinh hoc o Hk1 va chuyen o HK2
            List<PupilOfClass> listPupilTT = new List<PupilOfClass>();
            listPupilTT = listPupil.AddCriteriaSemester(academicYear, entity.Semester).ToList();
            //listPupilTT = listPupilTT.Where(o => listStatus.Contains(o.Status)).ToList();
            //Lấy danh sách môn học
            List<ClassSubject> listClassSubject = ClassSubjectBusiness.SearchBySchool(entity.SchoolID, new Dictionary<string, object> 
            {
                {"AcademicYearID", entity.AcademicYearID}, 
                {"AppliedLevel", SystemParamsInFile.APPLIED_LEVEL_PRIMARY}
            }).ToList();

            //Lấy danh sách học lực môn
            List<VSummedUpRecord> listSummedUpRecord = VSummedUpRecordBusiness.SearchBySchool(entity.SchoolID, new Dictionary<string, object>
            {
                {"AcademicYearID", entity.AcademicYearID},
                {"AppliedLevel", SystemParamsInFile.APPLIED_LEVEL_PRIMARY},
                //{"Semester", entity.Semester}
            }).ToList();            

            //Tạo vùng cho từng khối
            IVTRange summarizeRange = tempSheet.GetRange(firstRow + 5, 1, firstRow + 5, lastColumn);
            IVTRange topRange = tempSheet.GetRange(firstRow + 2, 1, firstRow + 2, lastColumn);
            IVTRange midRange = tempSheet.GetRange(firstRow + 3, 1, firstRow + 3, lastColumn);
            IVTRange lastRange = tempSheet.GetRange(firstRow + 4, 1, firstRow + 4, lastColumn);

            //Fill dữ liệu cho các sheet
            foreach (EducationLevel educationLevel in listEducationLevel)
            {
                //Tạo sheet
                IVTWorksheet dataSheet = oBook.CopySheetToLast(tempSheet,
               new VTVector(firstRow + 2, lastColumn).ToString());
                dataSheet.Name = educationLevel.Resolution;

                //Fill dữ liệu chung
                dicGeneralInfo["EducationLevel"] = educationLevel.Resolution.ToUpper();
                dataSheet.GetRange(1, 1, firstRow - 1, lastColumn).FillVariableValue(dicGeneralInfo);

                //Lấy danh sách môn học
                List<SubjectCat> lstSubject = (from lc in listClassSubject
                                               where lc.ClassProfile.EducationLevelID == educationLevel.EducationLevelID
                                               orderby lc.SubjectCat.OrderInSubject
                                               select lc.SubjectCat).Distinct().ToList<SubjectCat>();

                //Lấy danh sách lớp học
                List<ClassProfile> listCP = listClass
                    .Where(cls=>cls.EducationLevelID == educationLevel.EducationLevelID)
                    .OrderBy(cls=>cls.OrderNumber)
                    .ThenBy(cls=>cls.DisplayName)
                    .ToList<ClassProfile>();

                //Tính sĩ số cho từng lớp
                Dictionary<int, object> dicNoPupil = new Dictionary<int, object>();
                Dictionary<int, object> dicRealNoPupil = new Dictionary<int, object>();
               
                foreach (ClassProfile cp in listCP)
                {
                    int numberOfPupil = (from mr in listPupil
                                         where mr.ClassID == cp.ClassProfileID
                                         select mr.PupilOfClassID).Count();
                    int realNumberOfPupil = (from mr in listPupilTT
                                             where mr.ClassID == cp.ClassProfileID
                                             select mr.PupilOfClassID).Count();
                    dicNoPupil[cp.ClassProfileID] = numberOfPupil;
                    dicRealNoPupil[cp.ClassProfileID] = realNumberOfPupil;
                }

                if (listCP.Count > 0 && lstSubject.Count > 0)
                {
                    //Fill dữ liệu cho từng môn học
                    int beginRow = firstRow + 2;
                    if (lstSubject.Count > 0)
                    {
                        foreach (SubjectCat sc in lstSubject)
                        {
                            //Tạo khung dữ liệu
                            int endRow = beginRow + listCP.Count;
                            for (int i = beginRow; i < endRow; i++)
                            {
                                IVTRange copyRange;
                                if (i == beginRow)
                                {
                                    copyRange = topRange;
                                }
                                else if (i == endRow - 1)
                                {
                                    copyRange = lastRange;
                                }
                                else
                                {
                                    copyRange = midRange;
                                }
                                dataSheet.CopyPasteSameSize(copyRange, "A" + i);
                            }
                            dataSheet.CopyPasteSameSize(summarizeRange, endRow, 1);

                            //Merge cell môn học
                            dataSheet.MergeColumn('A', beginRow, endRow);

                            //Fill dữ liệu dòng tổng
                            string fomula = "=SUM(#" + beginRow + ":" + "#" + (endRow - 1) + ")";
                            foreach (StatisticsConfig scg in listSC)
                            {
                                int col = dicStatisticsConfigToColumn[scg];
                                dataSheet.SetFormulaValue(endRow,
                                    col, fomula.Replace("#", VTVector.ColumnIntToString(col)));
                            }
                            dataSheet.SetFormulaValue("C" + endRow, fomula.Replace("#", "C"));
                            dataSheet.SetFormulaValue("D" + endRow, fomula.Replace("#", "D"));

                            //Tạo dữ liệu cần fill
                            List<object> listData = new List<object>();
                            foreach (ClassProfile cp in listCP)
                            {
                                Dictionary<string, object> dicData = new Dictionary<string, object>();
                                dicData["ClassName"] = cp.DisplayName;
                                dicData["NumberOfPupil"] = dicNoPupil[cp.ClassProfileID];
                                dicData["RealNumberOfPupil"] = dicRealNoPupil[cp.ClassProfileID];
                                List<PupilOfClass> listPupilClass = listPupilTT.Where(o => o.ClassID == cp.ClassProfileID).ToList();
                                List<VSummedUpRecord> listSummedUpRecordClass = listSummedUpRecord.Where(o => o.ClassID == cp.ClassProfileID && o.SubjectID == sc.SubjectCatID).ToList();
                                List<VSummedUpRecord> lstSummedUpRecordBySemester = new List<VSummedUpRecord>();
                                if (entity.Semester == SystemParamsInFile.SEMESTER_OF_YEAR_FIRST)
                                {

                                    lstSummedUpRecordBySemester = listSummedUpRecordClass.Where(o => o.Semester == entity.Semester).ToList();
                                }
                                else
                                {
                                    //Lấy theo max semester và max date
                                    List<VSummedUpRecord> listSummedUpMax = listSummedUpRecordClass.Where(o => o.Semester >= SystemParamsInFile.SEMESTER_OF_YEAR_SECOND).ToList();
                                    lstSummedUpRecordBySemester = listSummedUpMax.Where(u => u.Semester == listSummedUpMax.Where(v => v.PupilID == u.PupilID && v.ClassID == u.ClassID).Max(v => v.Semester)).ToList();
                                }
                                foreach (StatisticsConfig scg in listSC)
                                {
                                    int numberOf;
                                    if (scg.SubjectType == SystemParamsInFile.StatisticsConfig.SUBJECT_TYPE_MARK)
                                    {
                                        numberOf = (from mr in lstSummedUpRecordBySemester
                                                    where listPupilClass.Any(o => o.PupilID == mr.PupilID)
                                                    && ((mr.ReTestMark == null && mr.SummedUpMark < scg.MaxValue) || (mr.ReTestMark != null && mr.ReTestMark < scg.MaxValue))
                                                    && ((mr.ReTestMark == null && mr.SummedUpMark >= scg.MinValue) || (mr.ReTestMark != null && mr.ReTestMark >= scg.MinValue))
                                                    select mr.SummedUpRecordID).Count();

                                    }
                                    else
                                    {
                                        numberOf = (from mr in lstSummedUpRecordBySemester
                                                    where listPupilClass.Any(o => o.PupilID == mr.PupilID)
                                                    && ((mr.ReTestJudgement == null && mr.JudgementResult == scg.EqualValue) || (mr.JudgementResult != null && mr.ReTestJudgement == scg.EqualValue))
                                                    select mr.SummedUpRecordID).Count();
                                    }
                                    dicData[scg.StatisticsConfigID.ToString()] = numberOf;

                                }
                                listData.Add(dicData);
                            }

                            //Fill dữ liệu
                            dataSheet.GetRange(beginRow, 1, endRow, lastColumn).FillVariableValue(new Dictionary<string, object>
                        {
                            {"list",listData},
                            {"SubjectName",sc.DisplayName}
                        });

                            //Tính vị trí tiếp theo
                            beginRow = endRow + 1;
                        }
                    }
                    
                    dataSheet.CopyPaste(tempSheet.GetRange(firstRow + 8, 1, firstRow + 8, 100), beginRow + 1, 1);
                }
                else
                {
                    dataSheet.DeleteRow(firstRow + 2);
                }
                
                dataSheet.FitAllColumnsOnOnePage = true;
            }
   
            //Xoá sheet template
            firstSheet.Delete();
            tempSheet.Delete();

            return oBook.ToStream();
        }
        #endregion

        #region Lấy danh sách thống kê xếp loại học lực môn cấp 1 được cập nhật mới nhất
        public ProcessedReport GetReportSummedUpRecordGroupBySubjectPrimary(ReportSummedUpRecordSearchBO entity)
        {
            string reportCode = SystemParamsInFile.REPORT_DS_THONG_KE_XEP_LOAI_HOC_LUC_MON_CAP1;
            string inputParameterHashKey = GetHashKeyPrimary(entity);
            return ProcessedReportBusiness.GetProcessedReport(reportCode, inputParameterHashKey);
        }
        #endregion

        #region Lưu báo cáo danh sách  xếp loại học lực môn cấp 1 vào CSDL
        public ProcessedReport InsertReportSummedUpRecordGroupBySubjectPrimary(ReportSummedUpRecordSearchBO entity, Stream data)
        {
            string reportCode = SystemParamsInFile.REPORT_DS_THONG_KE_XEP_LOAI_HOC_LUC_MON_CAP1;
            ReportDefinition reportDef = ReportDefinitionBusiness.GetByCode(reportCode);
            ProcessedReport pr = new ProcessedReport();
            pr.ReportCode = reportCode;
            pr.ProcessedDate = DateTime.Now;
            pr.InputParameterHashKey = GetHashKeyPrimary(entity);
            pr.ReportData = ReportUtils.Compress(data);

            //Tạo tên file
            string outputNamePattern = reportDef.OutputNamePattern;
            string semester = ReportUtils.ConvertSemesterForReportName(entity.Semester);

            outputNamePattern = outputNamePattern.Replace("[Semester]", semester);
            pr.ReportName = ReportUtils.RemoveSpecialCharacters(outputNamePattern) + "." + reportDef.OutputFormat;

            IDictionary<string, object> dic = new Dictionary<string, object> {
                {"AcademicYearID", entity.AcademicYearID},
                {"EducationLevelID ", entity.EducationLevelID},
                {"SchoolID", entity.SchoolID},
                {"Semester", entity.Semester}
            };
            ProcessedReportParameterRepository.Insert(dic, pr);
            ProcessedReportRepository.Insert(pr);
            ProcessedReportRepository.Save();
            return pr;
        }
        #endregion

        #region Tạo file báo cáo thống kê xếp loại học lực môn cấp 1
        public Stream CreateReportSummedUpRecordGroupBySubjectPrimary(ReportSummedUpRecordSearchBO entity)
        {
            string reportCode = SystemParamsInFile.REPORT_DS_THONG_KE_XEP_LOAI_HOC_LUC_MON_CAP1;
            ReportDefinition reportDef = ReportDefinitionBusiness.GetByCode(reportCode);
            string templatePath = ReportUtils.GetTemplatePath(reportDef);
            IVTWorkbook oBook = VTExport.OpenWorkbook(templatePath);

            int firstRow = 9;
            int defaultNumbeOfColumn = 7;
            int firstDynamicCol = VTVector.dic['D'];
            int defaultLastCol = VTVector.dic['Q'];
            //Lấy sheet template
            IVTWorksheet firstSheet = oBook.GetSheet(1);
            IVTRange subjectTemp = firstSheet.GetRange(firstRow, firstDynamicCol, firstRow + 6, firstDynamicCol + 1);

            //Tạo template chuẩn
            IVTWorksheet tempSheet = oBook.CopySheetToLast(firstSheet, "Q" + (firstRow - 1));
            tempSheet.CopyPasteSameSize(firstSheet.GetRange(firstRow, 1, firstRow + 6, firstDynamicCol - 1), "A" + firstRow);
            tempSheet.CopyPaste(firstSheet.GetRange(firstRow + 8, 1, firstRow + 8, 100), firstRow + 8, 1);

            //Lấy danh sách các cột động
            List<StatisticsConfig> listSC = StatisticsConfigBusiness.GetList(
                SystemParamsInFile.APPLIED_LEVEL_PRIMARY,
                SystemParamsInFile.StatisticsConfig.REPORT_TYPE_HOC_LUC_MON,
                SystemParamsInFile.StatisticsConfig.MARK_TYPE_ALL,
                SystemParamsInFile.StatisticsConfig.SUBJECT_TYPE_ALL);


            //Tạo các cột động
            Dictionary<StatisticsConfig, int> dicStatisticsConfigToColumn = new Dictionary<StatisticsConfig, int>();
            for (int i = 0; i < listSC.Count; i++)
            {
                StatisticsConfig sc = listSC[i];
                tempSheet.CopyPasteSameSize(subjectTemp, firstRow, firstDynamicCol + i * 2);
                tempSheet.SetCellValue(firstRow, firstDynamicCol + i * 2, sc.DisplayName);
                dicStatisticsConfigToColumn.Add(sc, firstDynamicCol + i * 2);
            }


            int lastCol = defaultLastCol + (listSC.Count - defaultNumbeOfColumn) * 2;

            //Chỉnh lại dòng khác
            if (listSC.Count > defaultNumbeOfColumn)
            {
                tempSheet.MergeRow(2, VTVector.dic['I'], lastCol);
                tempSheet.MergeRow(3, VTVector.dic['I'], lastCol);
                tempSheet.MergeRow(4, VTVector.dic['I'], lastCol);
                tempSheet.MergeRow(6, VTVector.dic['A'], lastCol);
                tempSheet.MergeRow(7, VTVector.dic['A'], lastCol);

                double colWidth = firstSheet.GetColumnWidth(firstDynamicCol);
                double resizeWidth = colWidth * defaultNumbeOfColumn / listSC.Count;
                for (int i = firstDynamicCol; i <= lastCol; i++)
                {
                    tempSheet.SetColumnWidth(i, resizeWidth);
                }
            }

            //Tính các công thức 
            string formulaPercent = "=IF(cellNumberOfPupil=0;0;ROUND(cellTotal/cellNumberOfPupil;4)*100)";

            for (int i = 0; i < listSC.Count; i++)
            {
                StatisticsConfig sc = listSC[i];
                tempSheet.SetCellValue(firstRow + 2, dicStatisticsConfigToColumn[sc], "${list[].Get(" + sc.StatisticsConfigID + ")}");
            }

            for (int rowIndex = firstRow + 2; rowIndex < firstRow + 5; rowIndex++)
            {
                for (int i = firstDynamicCol; i <= lastCol; i += 2)
                {
                    string cellNumberOfPupil = "C" + rowIndex;
                    string cellTotal = new VTVector(rowIndex, i).ToString();
                    tempSheet.SetFormulaValue(rowIndex, i + 1, formulaPercent.
                        Replace("cellTotal", cellTotal).Replace("cellNumberOfPupil", cellNumberOfPupil));
                }
            }

            //Fill dữ liệu chung cho template
            AcademicYear academicYear = AcademicYearBusiness.Find(entity.AcademicYearID);
            SchoolProfile school = SchoolProfileBusiness.Find(entity.SchoolID);
            Province province = school.Province;

            string schoolName = school.SchoolName.ToUpper();
            string supervisingDeptName = school.SupervisingDept.SupervisingDeptName.ToUpper();
            string academicYearTitle = academicYear.DisplayTitle;
            string provinceName = province.ProvinceName;
            DateTime reportDate = DateTime.Now;
            string semester = SMASConvert.ConvertSemesterSpecial2(entity.Semester).ToUpper();

            Dictionary<string, object> dicGeneralInfo = new Dictionary<string, object> 
                {
                    {"SchoolName", schoolName}, 
                    {"SupervisingDeptName", supervisingDeptName},
                    {"AcademicYearTitle", academicYearTitle},
                    {"ProvinceName", provinceName},
                    {"ReportDate", reportDate},
                    {"Semester", semester}
                };
            if(entity.Semester == SystemParamsInFile.SEMESTER_OF_YEAR_SECOND)
            {
                semester = "HỌC KỲ II";
            }
            string str = semester + " NĂM HỌC " + academicYearTitle;
            //Tính dòng cuôi cùng
            int lastColumn = Math.Max(lastCol, defaultLastCol);

            //Lấy danh sách học sinh / môn học
            AcademicYear Aca = AcademicYearBusiness.Find(entity.AcademicYearID);
            Dictionary<string, object> dicPupilRanking = new Dictionary<string, object> 
            {
                {"AcademicYearID", entity.AcademicYearID}, 
                {"AppliedLevel", SystemParamsInFile.APPLIED_LEVEL_PRIMARY},                
            };
            IQueryable<PupilOfClass> listPupil = PupilOfClassBusiness.SearchBySchool(entity.SchoolID, dicPupilRanking).AddCriteriaSemester(academicYear,entity.Semester);
            IQueryable<PupilOfClassBO> lstPoc = from p in listPupil
                                                join q in PupilProfileBusiness.All on p.PupilID equals q.PupilProfileID
                                                select new PupilOfClassBO
                                                {
                                                    PupilID = p.PupilID, 
                                                    ClassID = p.ClassID,
                                                    Genre = q.Genre,
                                                    EthnicID = q.EthnicID
                                                };

            IQueryable<ClassSubject> listClassSubjectSchool = ClassSubjectBusiness.SearchBySchool(entity.SchoolID, new Dictionary<string, object> 
            {
                {"AcademicYearID", entity.AcademicYearID}, 
                {"AppliedLevel", SystemParamsInFile.APPLIED_LEVEL_PRIMARY},
                {"Semester", entity.Semester}
            });
            //Dân tộc
            Ethnic Ethnic_Kinh = EthnicBusiness.All.Where(o => o.EthnicName.ToLower().Contains("kinh")).FirstOrDefault();
            Ethnic Ethnic_ForeignPeople = EthnicBusiness.All.Where(o => o.EthnicName.ToLower().Contains("nước ngoài")).FirstOrDefault();
            int? EthnicID_Kinh = 0;
            int? EthnicID_ForeignPeople = 0;
            if (Ethnic_Kinh != null)
            {
                EthnicID_Kinh = Ethnic_Kinh.EthnicID;
            }
            if (Ethnic_ForeignPeople != null)
            {
                EthnicID_ForeignPeople = Ethnic_ForeignPeople.EthnicID;
            }
            var listRSU_Temp = from lp in lstPoc
                                                   join cs in listClassSubjectSchool on lp.ClassID equals cs.ClassID
                                                   select new 
                                                   {
                                                        PupilID = lp.PupilID,
                                                        ClassID = lp.ClassID,
                                                        Genre = lp.Genre,
                                                        EthnicID = lp.EthnicID,
                                                        SubjectID = cs.SubjectID
                                                   };
            var listRSU = (from p in listRSU_Temp
                                                   group p by p.SubjectID into g
                                                   select new {
                                                       SubjectID = g.Key,
                                                       NumberOfPupil = g.Count()                                                      
                                                   }).ToList();
            var listRSU_Female = (from p in listRSU_Temp.Where(o => o.Genre == SystemParamsInFile.GENRE_FEMALE)
                                                    group p by p.SubjectID into g
                                                    select new 
                                                    {
                                                        SubjectID = g.Key,
                                                        NumberOfPupil = g.Count()
                                                    }).ToList();
            List<ReportSummedUpRecordBO> listRSU_Ethnic = (from p in listRSU_Temp.Where(o => o.EthnicID.HasValue && o.EthnicID != EthnicID_Kinh && o.EthnicID != EthnicID_ForeignPeople)
                                                           group p by p.SubjectID into g
                                                           select new ReportSummedUpRecordBO
                                                           {
                                                               SubjectID = g.Key,
                                                               NumberOfPupil = g.Count()
                                                           }).ToList();

            Dictionary<int, int> dicSubjectAndPupil = new Dictionary<int, int>();
            Dictionary<int, int> dicSubjectAndPupil_Female = new Dictionary<int, int>();
            Dictionary<int, int> dicSubjectAndPupil_Ethnic = new Dictionary<int, int>();
            foreach (var bo in listRSU)
            {
                dicSubjectAndPupil.Add(bo.SubjectID, bo.NumberOfPupil);
            }
            foreach (var bo in listRSU_Female)
            {
                dicSubjectAndPupil_Female.Add(bo.SubjectID, bo.NumberOfPupil);
            }
            foreach (ReportSummedUpRecordBO bo in listRSU_Ethnic)
            {
                dicSubjectAndPupil_Ethnic.Add(bo.SubjectID, bo.NumberOfPupil);
            }

            //Lấy danh sách môn học
            List<ClassSubject> listClassSubject = listClassSubjectSchool.ToList();

            //Lấy danh sách học lực môn
            IQueryable<VSummedUpRecord> listSummedUpRecordAll = VSummedUpRecordBusiness.SearchBySchool(entity.SchoolID, new Dictionary<string, object>
            {
                {"AcademicYearID", entity.AcademicYearID},
                {"AppliedLevel", SystemParamsInFile.APPLIED_LEVEL_PRIMARY},
                //{"Semester", entity.Semester}
            }).Where(o => !o.PeriodID.HasValue);

            IQueryable<VSummedUpRecord> listSummedUpRecordClass = (from u in listSummedUpRecordAll
                                                       join v in listPupil on new { u.ClassID, u.PupilID } equals new { v.ClassID, v.PupilID }
                                                       select u);

            IQueryable<SummedUpRecordBO> listSummedUpRecord = from u in listSummedUpRecordClass
                                                               join r in  PupilProfileBusiness.All on u.PupilID equals r.PupilProfileID
                                                               join v in listClassSubjectSchool on new { u.ClassID, u.SubjectID } equals new { v.ClassID, v.SubjectID }
                                                               select new SummedUpRecordBO
                                                               {
                                                                   SummedUpRecordID = u.SummedUpRecordID,
                                                                   PupilID = u.PupilID,
                                                                   ClassID = u.ClassID,
                                                                   Genre = r.Genre,
                                                                   EthnicID = r.EthnicID, 
                                                                   SubjectID = u.SubjectID,
                                                                   Semester = u.Semester,
                                                                   SummedUpMark = u.SummedUpMark,
                                                                   ReTestMark = u.ReTestMark,
                                                                   JudgementResult = u.JudgementResult,
                                                                   ReTestJudgement = u.ReTestJudgement
                                                               };
            List<SummedUpRecordBO> lstSummedUpRecord;
            List<SummedUpRecordBO> lstSummedUpRecord_Female = new List<SummedUpRecordBO>();
            List<SummedUpRecordBO> lstSummedUpRecord_Ethnic = new List<SummedUpRecordBO>();
            if (entity.Semester == SystemParamsInFile.SEMESTER_OF_YEAR_FIRST)
            {
                lstSummedUpRecord = listSummedUpRecord.Where(o => o.Semester == entity.Semester).ToList();
            }
            else
            {
                listSummedUpRecord = listSummedUpRecord.Where(o => o.Semester >= entity.Semester);
                lstSummedUpRecord = listSummedUpRecord.Where(u => u.Semester == listSummedUpRecord.Where(v => v.PupilID == u.PupilID && v.SubjectID == u.SubjectID).Max(v => v.Semester)).ToList();
            }
            lstSummedUpRecord_Female = lstSummedUpRecord.Where(o => o.Genre.HasValue && o.Genre == SystemParamsInFile.GENRE_FEMALE).ToList();
            lstSummedUpRecord_Ethnic = lstSummedUpRecord.Where(o => o.EthnicID.HasValue && o.EthnicID != EthnicID_Kinh && o.EthnicID != EthnicID_ForeignPeople).ToList();

            //Tạo vùng cho từng khối
            IVTRange topRange = tempSheet.GetRange(firstRow + 2, 1, firstRow + 2, lastColumn);
            IVTRange midRange = tempSheet.GetRange(firstRow + 3, 1, firstRow + 3, lastColumn);
            IVTRange lastRange = tempSheet.GetRange(firstRow + 4, 1, firstRow + 4, lastColumn);

            #region Tạo sheet thống kê xếp loại học lực môn
            //Tạo sheet
            IVTWorksheet dataSheet = oBook.CopySheetToLast(tempSheet,
           new VTVector(firstRow + 2, lastColumn).ToString());

            //Fill dữ liệu chung
            dataSheet.GetRange(1, 1, firstRow - 1, lastColumn).FillVariableValue(dicGeneralInfo);
            dataSheet.SetCellValue("A6", "TỔNG HỢP XẾP LOẠI HỌC LỰC MÔN");
            dataSheet.SetCellValue("A7", str);
            //Lấy danh sách môn học theo thứ tự
            List<SubjectCat> lstSubject = (from lc in listClassSubjectSchool
                                           select lc.SubjectCat).Distinct()
                                           .OrderBy(o => o.OrderInSubject)
                                           .ThenBy(o => o.DisplayName)
                                           .ToList<SubjectCat>();

            //Tạo khung fill dữ liệu
            int beginRow = firstRow + 2;
            int endRow = beginRow + lstSubject.Count - 1;
            for (int i = beginRow; i <= endRow; i++)
            {
                IVTRange copyRange;
                if (i == beginRow)
                {
                    copyRange = topRange;
                }
                else if (i == endRow)
                {
                    copyRange = lastRange;
                }
                else
                {
                    copyRange = midRange;
                }
                dataSheet.CopyPasteSameSize(copyRange, "A" + i);
            }

            //Tạo dữ liệu cần fill
            List<object> listData = new List<object>();
            int index = 0;
            foreach (SubjectCat sc in lstSubject)
            {
                index++;
                Dictionary<string, object> dicData = new Dictionary<string, object>();
                dicData["Order"] = index;
                dicData["SubjectName"] = sc.DisplayName;
                dicData["NumberOfPupil"] = dicSubjectAndPupil.ContainsKey(sc.SubjectCatID) ? dicSubjectAndPupil[sc.SubjectCatID] : 0;
                foreach (StatisticsConfig scg in listSC)
                {
                    int numberOf;
                    if (scg.SubjectType == SystemParamsInFile.StatisticsConfig.SUBJECT_TYPE_MARK)
                    {
                        numberOf = (from mr in lstSummedUpRecord
                                    where mr.SubjectID == sc.SubjectCatID
                                    && ((mr.ReTestMark == null && mr.SummedUpMark < scg.MaxValue) || (mr.ReTestMark != null && mr.ReTestMark < scg.MaxValue))
                                    && ((mr.ReTestMark == null && mr.SummedUpMark >= scg.MinValue) || (mr.ReTestMark != null && mr.ReTestMark >= scg.MinValue))
                                    select mr.SummedUpRecordID).Count();
                    }
                    else
                    {
                        numberOf = (from mr in lstSummedUpRecord
                                    where mr.SubjectID == sc.SubjectCatID
                                    && ((mr.ReTestJudgement == null && mr.JudgementResult == scg.EqualValue) || (mr.ReTestJudgement != null && mr.ReTestJudgement == scg.EqualValue))
                                    select mr.SummedUpRecordID).Count();
                    }
                    dicData[scg.StatisticsConfigID.ToString()] = numberOf;

                }
                listData.Add(dicData);
            }

            //Fill dữ liệu
            dataSheet.GetRange(beginRow, 1, endRow, lastColumn).FillVariableValue(new Dictionary<string, object>
                        {
                            {"list",listData}
                        });


            dataSheet.CopyPaste(tempSheet.GetRange(firstRow + 8, 1, firstRow + 8, 100), endRow + 2, 1);
            dataSheet.Name = "XLHLM";
            dataSheet.FitAllColumnsOnOnePage = true;
            #endregion
            #region Tạo Sheet thống kê xếp loại học lực môn học sinh nữ
            IVTWorksheet dataSheet_Female = oBook.CopySheetToLast(tempSheet,
          new VTVector(firstRow + 2, lastColumn).ToString());

            //Fill dữ liệu chung
            dataSheet_Female.GetRange(1, 1, firstRow - 1, lastColumn).FillVariableValue(dicGeneralInfo);
            dataSheet_Female.SetCellValue("A6", "TỔNG HỢP XẾP LOẠI HỌC LỰC MÔN HỌC SINH NỮ");
            dataSheet_Female.SetCellValue("A7", str);

            //Tạo khung fill dữ liệu
            beginRow = firstRow + 2;
            endRow = beginRow + lstSubject.Count - 1;
            for (int i = beginRow; i <= endRow; i++)
            {
                IVTRange copyRange;
                if (i == beginRow)
                {
                    copyRange = topRange;
                }
                else if (i == endRow)
                {
                    copyRange = lastRange;
                }
                else
                {
                    copyRange = midRange;
                }
                dataSheet_Female.CopyPasteSameSize(copyRange, "A" + i);
            }

            //Tạo dữ liệu cần fill
            listData = new List<object>();
            index = 0;
            foreach (SubjectCat sc in lstSubject)
            {
                index++;
                Dictionary<string, object> dicData = new Dictionary<string, object>();
                dicData["Order"] = index;
                dicData["SubjectName"] = sc.DisplayName;
                dicData["NumberOfPupil"] = dicSubjectAndPupil_Female.ContainsKey(sc.SubjectCatID) ? dicSubjectAndPupil_Female[sc.SubjectCatID] : 0;
                foreach (StatisticsConfig scg in listSC)
                {
                    int numberOf;
                    if (scg.SubjectType == SystemParamsInFile.StatisticsConfig.SUBJECT_TYPE_MARK)
                    {
                        numberOf = (from mr in lstSummedUpRecord_Female
                                    where mr.SubjectID == sc.SubjectCatID
                                    && ((mr.ReTestMark == null && mr.SummedUpMark < scg.MaxValue) || (mr.ReTestMark != null && mr.ReTestMark < scg.MaxValue))
                                    && ((mr.ReTestMark == null && mr.SummedUpMark >= scg.MinValue) || (mr.ReTestMark != null && mr.ReTestMark >= scg.MinValue))
                                    select mr.SummedUpRecordID).Count();
                    }
                    else
                    {
                        numberOf = (from mr in lstSummedUpRecord_Female
                                    where mr.SubjectID == sc.SubjectCatID
                                    && ((mr.ReTestJudgement == null && mr.JudgementResult == scg.EqualValue) || (mr.ReTestJudgement != null && mr.ReTestJudgement == scg.EqualValue))
                                    select mr.SummedUpRecordID).Count();
                    }
                    dicData[scg.StatisticsConfigID.ToString()] = numberOf;

                }
                listData.Add(dicData);
            }

            //Fill dữ liệu
            dataSheet_Female.GetRange(beginRow, 1, endRow, lastColumn).FillVariableValue(new Dictionary<string, object>
                        {
                            {"list",listData}
                        });


            dataSheet_Female.CopyPaste(tempSheet.GetRange(firstRow + 8, 1, firstRow + 8, 100), endRow + 2, 1);
            dataSheet_Female.Name = "HS_Nu";
            dataSheet_Female.FitAllColumnsOnOnePage = true;
            #endregion 
            #region Tạo sheet thống kê xếp loại học lực môn học sinh dân tộc
            IVTWorksheet dataSheet_Ethnic = oBook.CopySheetToLast(tempSheet,
         new VTVector(firstRow + 2, lastColumn).ToString());

            //Fill dữ liệu chung
            dataSheet_Ethnic.GetRange(1, 1, firstRow - 1, lastColumn).FillVariableValue(dicGeneralInfo);
            dataSheet_Ethnic.SetCellValue("A6", "TỔNG HỢP XẾP LOẠI HỌC LỰC MÔN HỌC SINH DÂN TỘC");
            dataSheet_Ethnic.SetCellValue("A7", str);

            //Tạo khung fill dữ liệu
            beginRow = firstRow + 2;
            endRow = beginRow + lstSubject.Count - 1;
            for (int i = beginRow; i <= endRow; i++)
            {
                IVTRange copyRange;
                if (i == beginRow)
                {
                    copyRange = topRange;
                }
                else if (i == endRow)
                {
                    copyRange = lastRange;
                }
                else
                {
                    copyRange = midRange;
                }
                dataSheet_Ethnic.CopyPasteSameSize(copyRange, "A" + i);
            }

            //Tạo dữ liệu cần fill
            listData = new List<object>();
            index = 0;
            foreach (SubjectCat sc in lstSubject)
            {
                index++;
                Dictionary<string, object> dicData = new Dictionary<string, object>();
                dicData["Order"] = index;
                dicData["SubjectName"] = sc.DisplayName;
                dicData["NumberOfPupil"] = dicSubjectAndPupil_Ethnic.ContainsKey(sc.SubjectCatID) ? dicSubjectAndPupil_Ethnic[sc.SubjectCatID] : 0;
                foreach (StatisticsConfig scg in listSC)
                {
                    int numberOf;
                    if (scg.SubjectType == SystemParamsInFile.StatisticsConfig.SUBJECT_TYPE_MARK)
                    {
                        numberOf = (from mr in lstSummedUpRecord_Ethnic
                                    where mr.SubjectID == sc.SubjectCatID
                                    && ((mr.ReTestMark == null && mr.SummedUpMark < scg.MaxValue) || (mr.ReTestMark != null && mr.ReTestMark < scg.MaxValue))
                                    && ((mr.ReTestMark == null && mr.SummedUpMark >= scg.MinValue) || (mr.ReTestMark != null && mr.ReTestMark >= scg.MinValue))
                                    select mr.SummedUpRecordID).Count();
                    }
                    else
                    {
                        numberOf = (from mr in lstSummedUpRecord_Ethnic
                                    where mr.SubjectID == sc.SubjectCatID
                                    && ((mr.ReTestJudgement == null && mr.JudgementResult == scg.EqualValue) || (mr.ReTestJudgement != null && mr.ReTestJudgement == scg.EqualValue))
                                    select mr.SummedUpRecordID).Count();
                    }
                    dicData[scg.StatisticsConfigID.ToString()] = numberOf;

                }
                listData.Add(dicData);
            }

            //Fill dữ liệu
            dataSheet_Ethnic.GetRange(beginRow, 1, endRow, lastColumn).FillVariableValue(new Dictionary<string, object>
                        {
                            {"list",listData}
                        });


            dataSheet_Ethnic.CopyPaste(tempSheet.GetRange(firstRow + 8, 1, firstRow + 8, 100), endRow + 2, 1);
            dataSheet_Ethnic.Name = "HS_DT";
            dataSheet_Ethnic.FitAllColumnsOnOnePage = true;
            #endregion
            //Xoá sheet template
            firstSheet.Delete();
            tempSheet.Delete();

            return oBook.ToStream();
        }
        #endregion

        #region Lấy danh sách thống kê xếp loại học lực môn theo khối cấp 1 được cập nhật mới nhất
        public ProcessedReport GetReportSummedUpRecordGroupByEducationLevelPrimary(ReportSummedUpRecordSearchBO entity)
        {
            string reportCode = SystemParamsInFile.REPORT_DS_THONG_KE_XEP_LOAI_HOC_LUC_MON_THEO_KHOI_CAP1;
            string inputParameterHashKey = GetHashKeyPrimary(entity);
            return ProcessedReportBusiness.GetProcessedReport(reportCode, inputParameterHashKey);
        }
        #endregion

        #region Lưu báo cáo xếp loại học lực môn theo khối cấp 1 vào CSDL
        public ProcessedReport InsertReportSummedUpRecordGroupByEducationLevelPrimary(ReportSummedUpRecordSearchBO entity, Stream data)
        {
            string reportCode = SystemParamsInFile.REPORT_DS_THONG_KE_XEP_LOAI_HOC_LUC_MON_THEO_KHOI_CAP1;
            ReportDefinition reportDef = ReportDefinitionBusiness.GetByCode(reportCode);
            ProcessedReport pr = new ProcessedReport();
            pr.ReportCode = reportCode;
            pr.ProcessedDate = DateTime.Now;
            pr.InputParameterHashKey = GetHashKeyPrimary(entity);
            pr.ReportData = ReportUtils.Compress(data);

            //Tạo tên file
            string outputNamePattern = reportDef.OutputNamePattern;
            string semester = ReportUtils.ConvertSemesterForReportName(entity.Semester);

            outputNamePattern = outputNamePattern.Replace("[Semester]", semester);
            pr.ReportName = ReportUtils.RemoveSpecialCharacters(outputNamePattern) + "." + reportDef.OutputFormat;

            IDictionary<string, object> dic = new Dictionary<string, object> {
                {"AcademicYearID", entity.AcademicYearID},
                {"EducationLevelID ", entity.EducationLevelID},
                {"SchoolID", entity.SchoolID},
                {"Semester", entity.Semester}
            };
            ProcessedReportParameterRepository.Insert(dic, pr);
            ProcessedReportRepository.Insert(pr);
            ProcessedReportRepository.Save();
            return pr;
        }
        #endregion

        #region Tạo file báo cáo xếp loại học lực môn theo khối cấp 1
        public Stream CreateReportSummedUpRecordGroupByEducationLevelPrimary(ReportSummedUpRecordSearchBO entity)
        {
            string reportCode = SystemParamsInFile.REPORT_DS_THONG_KE_XEP_LOAI_HOC_LUC_MON_THEO_KHOI_CAP1;
            ReportDefinition reportDef = ReportDefinitionBusiness.GetByCode(reportCode);
            string templatePath = ReportUtils.GetTemplatePath(reportDef);
            IVTWorkbook oBook = VTExport.OpenWorkbook(templatePath);

            int firstRow = 9;
            int defaultNumbeOfColumn = 7;
            int firstDynamicCol = VTVector.dic['D'];
            int defaultLastCol = VTVector.dic['Q'];
            //Lấy sheet template
            IVTWorksheet firstSheet = oBook.GetSheet(1);
            IVTRange subjectTemp = firstSheet.GetRange(firstRow, firstDynamicCol, firstRow + 6, firstDynamicCol + 1);

            //Tạo template chuẩn
            IVTWorksheet tempSheet = oBook.CopySheetToLast(firstSheet, "D" + (firstRow - 1));
            tempSheet.CopyPasteSameSize(firstSheet.GetRange(1, 1, 4,24), 1,1);
            tempSheet.CopyPasteSameSize(firstSheet.GetRange(firstRow, 1, firstRow + 6, firstDynamicCol - 1), "A" + firstRow);
            tempSheet.CopyPaste(firstSheet.GetRange(firstRow + 8, 1, firstRow + 8, 100), firstRow + 8, 1);

            //Lấy danh sách các cột động
            List<StatisticsConfig> listSC = StatisticsConfigBusiness.GetList(
                SystemParamsInFile.APPLIED_LEVEL_PRIMARY,
                SystemParamsInFile.StatisticsConfig.REPORT_TYPE_HOC_LUC_MON,
                SystemParamsInFile.StatisticsConfig.MARK_TYPE_ALL,
                SystemParamsInFile.StatisticsConfig.SUBJECT_TYPE_ALL);


            //Tạo các cột động
            Dictionary<StatisticsConfig, int> dicStatisticsConfigToColumn = new Dictionary<StatisticsConfig, int>();
            for (int i = 0; i < listSC.Count; i++)
            {
                StatisticsConfig sc = listSC[i];
                tempSheet.CopyPasteSameSize(subjectTemp, firstRow, firstDynamicCol + i * 2);
                tempSheet.SetCellValue(firstRow, firstDynamicCol + i * 2, sc.DisplayName);
                dicStatisticsConfigToColumn.Add(sc, firstDynamicCol + i * 2);
            }


            int lastCol = defaultLastCol + (listSC.Count - defaultNumbeOfColumn) * 2;

            //Chỉnh lại dòng khác
            if (listSC.Count > defaultNumbeOfColumn)
            {
                tempSheet.MergeRow(2, VTVector.dic['I'], lastCol);
                tempSheet.MergeRow(3, VTVector.dic['I'], lastCol);
                tempSheet.MergeRow(4, VTVector.dic['I'], lastCol);
                tempSheet.MergeRow(6, VTVector.dic['A'], lastCol);
                tempSheet.MergeRow(7, VTVector.dic['A'], lastCol);

                double colWidth = firstSheet.GetColumnWidth(firstDynamicCol);
                double resizeWidth = colWidth * defaultNumbeOfColumn / listSC.Count;
                for (int i = firstDynamicCol; i <= lastCol; i++)
                {
                    tempSheet.SetColumnWidth(i, resizeWidth);
                }
            }

            //Tính các công thức 
            string formulaPercent = "=IF(cellNumberOfPupil=0;0;ROUND(cellTotal/cellNumberOfPupil;4)*100)";

            for (int i = 0; i < listSC.Count; i++)
            {
                StatisticsConfig sc = listSC[i];
                tempSheet.SetCellValue(firstRow + 2, dicStatisticsConfigToColumn[sc], "${list[].Get(" + sc.StatisticsConfigID + ")}");
            }

            for (int rowIndex = firstRow + 2; rowIndex < firstRow + 6; rowIndex++)
            {
                for (int i = firstDynamicCol; i <= lastCol; i += 2)
                {
                    string cellNumberOfPupil = "C" + rowIndex;
                    string cellTotal = new VTVector(rowIndex, i).ToString();
                    tempSheet.SetFormulaValue(rowIndex, i + 1, formulaPercent.
                        Replace("cellTotal", cellTotal).Replace("cellNumberOfPupil", cellNumberOfPupil));
                }
            }

            //Fill dữ liệu chung cho template
            AcademicYear academicYear = AcademicYearBusiness.Find(entity.AcademicYearID);
            SchoolProfile school = SchoolProfileBusiness.Find(entity.SchoolID);
            Province province = school.Province;

            string schoolName = school.SchoolName.ToUpper();
            string supervisingDeptName = school.SupervisingDept.SupervisingDeptName.ToUpper();
            string academicYearTitle = academicYear.DisplayTitle;
            string provinceName = province.ProvinceName;
            DateTime reportDate = DateTime.Now;
            string semester = SMASConvert.ConvertSemesterSpecial(entity.Semester).ToUpper();
            if (entity.Semester == SystemParamsInFile.SEMESTER_OF_YEAR_SECOND)
            {
                semester = "HỌC KỲ II";
            }
            string str = semester + " NĂM HỌC " + academicYearTitle;
            Dictionary<string, object> dicGeneralInfo = new Dictionary<string, object> 
                {
                    {"SchoolName", schoolName}, 
                    {"SupervisingDeptName", supervisingDeptName},
                    {"AcademicYearTitle", academicYearTitle},
                    {"ProvinceName", provinceName},
                    {"ReportDate", reportDate},
                    {"Semester", semester.ToUpper()}
                };

            //Tính dòng cuôi cùng
            int lastColumn = Math.Max(lastCol, defaultLastCol);

            //Lấy danh sách các khối
            List<EducationLevel> listEducationLevel = EducationLevelBusiness.GetByGrade(
                SystemParamsInFile.APPLIED_LEVEL_PRIMARY).OrderBy(x => x.EducationLevelID).ToList();

            //Lấy danh sách học sinh / môn học
            AcademicYear Aca = AcademicYearBusiness.Find(entity.AcademicYearID);
            Dictionary<string, object> dicPupilRanking = new Dictionary<string, object> 
            {
                {"AcademicYearID", entity.AcademicYearID},
                {"SchoolID", entity.SchoolID},
                {"AppliedLevel", entity.AppliedLevel}
            };
            IQueryable<PupilOfClassBO> listPupil = from p in PupilOfClassBusiness.SearchBySchool(entity.SchoolID, dicPupilRanking).AddCriteriaSemester(academicYear, entity.Semester)
                                                   join q in ClassProfileBusiness.All on p.ClassID equals q.ClassProfileID
                                                   join r in PupilProfileBusiness.All on p.PupilID equals r.PupilProfileID
                                                   where q.IsActive.Value
                                                   select new PupilOfClassBO
                                                   {
                                                       PupilID = p.PupilID,
                                                       ClassID = p.ClassID,
                                                       EducationLevelID = q.EducationLevelID,
                                                       Genre = r.Genre,
                                                       EthnicID = r.EthnicID
                                                   };
            
            IDictionary<string,object> dicCS = new Dictionary<string,object>(){
                {"AcademicYearID", entity.AcademicYearID},
                {"Semester", entity.Semester},
                {"AppliedLevel", entity.AppliedLevel}
            };
            IQueryable<ClassSubject> listClassSubject = ClassSubjectBusiness.SearchBySchool(entity.SchoolID, dicCS);
            //Dân tộc
            Ethnic Ethnic_Kinh = EthnicBusiness.All.Where(o => o.EthnicCode.ToLower().Equals(SystemParamsInFile.ETHNIC_CODE_KINH.ToLower()) && o.IsActive == true).FirstOrDefault();
            Ethnic Ethnic_ForeignPeople = EthnicBusiness.All.Where(o => o.EthnicCode.ToLower().Equals(SystemParamsInFile.ETHNIC_CODE_NN.ToLower()) && o.IsActive == true).FirstOrDefault();
            int? EthnicID_Kinh = 0;
            int? EthnicID_ForeignPeople = 0;
            if (Ethnic_Kinh != null)
            {
                EthnicID_Kinh = Ethnic_Kinh.EthnicID;
            }
            if (Ethnic_ForeignPeople != null)
            {
                EthnicID_ForeignPeople = Ethnic_ForeignPeople.EthnicID;
            }
            List<ReportSummedUpRecordBO> listRSU = (from lp in listPupil
                                                    join cs in listClassSubject
                                                        on  lp.ClassID equals cs.ClassID
                                                    join cp in ClassProfileBusiness.All on lp.ClassID equals cp.ClassProfileID
                                                    where cp.IsActive.Value
                                                    group cs by new { cs.SubjectID, cp.EducationLevelID } into gg
                                                    select new ReportSummedUpRecordBO()
                                                    {
                                                        NumberOfPupil = gg.Count(),
                                                        SubjectID = gg.Key.SubjectID,
                                                        EducationLevelID = gg.Key.EducationLevelID
                                                    }).ToList<ReportSummedUpRecordBO>();
            List<ReportSummedUpRecordBO> listRSU_Female = (from lp in listPupil.Where(o => o.Genre.HasValue && o.Genre == SystemParamsInFile.GENRE_FEMALE)
                                                    join cs in listClassSubject
                                                        on lp.ClassID equals cs.ClassID
                                                    join cp in ClassProfileBusiness.All on lp.ClassID equals cp.ClassProfileID
                                                    where cp.IsActive.Value
                                                    group cs by new { cs.SubjectID, cp.EducationLevelID } into gg
                                                    select new ReportSummedUpRecordBO()
                                                    {
                                                        NumberOfPupil = gg.Count(),
                                                        SubjectID = gg.Key.SubjectID,
                                                        EducationLevelID = gg.Key.EducationLevelID
                                                    }).ToList<ReportSummedUpRecordBO>();
            List<ReportSummedUpRecordBO> listRSU_Ethnic = (from lp in listPupil.Where(o => o.EthnicID.HasValue && o.EthnicID != EthnicID_ForeignPeople && o.EthnicID != EthnicID_Kinh)
                                                           join cs in listClassSubject
                                                               on lp.ClassID equals cs.ClassID
                                                           join cp in ClassProfileBusiness.All on lp.ClassID equals cp.ClassProfileID
                                                           where cp.IsActive.Value
                                                           group cs by new { cs.SubjectID, cp.EducationLevelID } into gg
                                                           select new ReportSummedUpRecordBO()
                                                           {
                                                               NumberOfPupil = gg.Count(),
                                                               SubjectID = gg.Key.SubjectID,
                                                               EducationLevelID = gg.Key.EducationLevelID
                                                           }).ToList<ReportSummedUpRecordBO>();

            Dictionary<string, int> dicSubjectAndPupil = new Dictionary<string, int>();
            Dictionary<string, int> dicSubjectAndPupil_Female = new Dictionary<string, int>();
            Dictionary<string, int> dicSubjectAndPupil_Ethnic = new Dictionary<string, int>();
            foreach (ReportSummedUpRecordBO bo in listRSU)
            {
                dicSubjectAndPupil.Add(bo.SubjectID + "_" + bo.EducationLevelID, bo.NumberOfPupil);
            }
            foreach (ReportSummedUpRecordBO bo in listRSU_Female)
            {
                dicSubjectAndPupil_Female.Add(bo.SubjectID + "_" + bo.EducationLevelID, bo.NumberOfPupil);
            }
            foreach (ReportSummedUpRecordBO bo in listRSU_Ethnic)
            {
                dicSubjectAndPupil_Ethnic.Add(bo.SubjectID + "_" + bo.EducationLevelID, bo.NumberOfPupil);
            }
            //Lấy danh sách học lực môn
            IQueryable<VSummedUpRecord> listSummedUpRecord_Temp = VSummedUpRecordBusiness.SearchBySchool(entity.SchoolID, new Dictionary<string, object>
            {
                {"AcademicYearID", entity.AcademicYearID},
                {"AppliedLevel", SystemParamsInFile.APPLIED_LEVEL_PRIMARY},
            });
            IQueryable<SummedUpRecordBO> listSummedUpRecord = from ls in listSummedUpRecord_Temp
                                                              join poc in listPupil
                                                              on new { ls.ClassID, ls.PupilID } equals new { poc.ClassID, poc.PupilID }
                                                              join cs in listClassSubject on new { ls.ClassID, ls.SubjectID } equals new { cs.ClassID, cs.SubjectID }
                                                              join cp in ClassProfileBusiness.All on ls.ClassID equals cp.ClassProfileID
                                                              join pp in  PupilProfileBusiness.All on ls.PupilID equals pp.PupilProfileID
                                                              where cp.IsActive.Value
                                                              select new SummedUpRecordBO
                                                              {
                                                                  SummedUpRecordID = ls.SummedUpRecordID,
                                                                  PupilID = ls.PupilID, 
                                                                  EducationLevelID = cp.EducationLevelID,
                                                                  ClassID = ls.ClassID,
                                                                  Genre = pp.Genre,
                                                                  EthnicID = pp.EthnicID,
                                                                  Semester = ls.Semester,
                                                                  SubjectID = ls.SubjectID,
                                                                  SummedUpMark = ls.SummedUpMark,
                                                                  ReTestMark = ls.ReTestMark,
                                                                  JudgementResult = ls.JudgementResult, 
                                                                  ReTestJudgement = ls.ReTestJudgement
                                                              };
            List<SummedUpRecordBO> lstSummedUpRecord;
            List<SummedUpRecordBO> lstSummedUpRecord_Female = new List<SummedUpRecordBO>();
            List<SummedUpRecordBO> lstSummedUpRecord_Ethnic = new List<SummedUpRecordBO>();
            if (entity.Semester == SystemParamsInFile.SEMESTER_OF_YEAR_FIRST)
            {
                lstSummedUpRecord = listSummedUpRecord.Where(u => u.Semester == entity.Semester).ToList();
            }
            else
            {
                listSummedUpRecord = listSummedUpRecord.Where(u => u.Semester >= entity.Semester);
                lstSummedUpRecord = listSummedUpRecord.Where(u => u.Semester == listSummedUpRecord.Where(v => v.PupilID == u.PupilID && v.ClassID == u.ClassID && v.SubjectID == u.SubjectID).Max(v => v.Semester)).ToList();
            }
            lstSummedUpRecord_Female = lstSummedUpRecord.Where(o => o.Genre.HasValue && o.Genre == SystemParamsInFile.GENRE_FEMALE).ToList();
            lstSummedUpRecord_Ethnic = lstSummedUpRecord.Where(o => o.EthnicID.HasValue && o.EthnicID != EthnicID_ForeignPeople && o.EthnicID != EthnicID_Kinh).ToList();
            //Loai di nhung hoc sinh chuyen lop trong truong
            //List<PupilOfClass> listPoc = listPupil.ToList();
            
            //Tạo vùng cho từng khối
            IVTRange summarizeRange = tempSheet.GetRange(firstRow + 5, 1, firstRow + 5, lastColumn);
            IVTRange topRange = tempSheet.GetRange(firstRow + 2, 1, firstRow + 2, lastColumn);
            IVTRange midRange = tempSheet.GetRange(firstRow + 3, 1, firstRow + 3, lastColumn);
            IVTRange lastRange = tempSheet.GetRange(firstRow + 4, 1, firstRow + 4, lastColumn);
            #region Tạo sheet thống kê xếp loại môn học theo khối
            //Fill dữ liệu cho các sheet
            //Tạo sheet
            IVTWorksheet dataSheet = oBook.CopySheetToLast(tempSheet,
           new VTVector(firstRow + 2, lastColumn).ToString());

            //Fill dữ liệu chung
            dataSheet.GetRange(1, 1, firstRow - 1, lastColumn).FillVariableValue(dicGeneralInfo);
            dataSheet.SetCellValue("A6", "THỐNG KÊ XẾP LOẠI HỌC LỰC MÔN THEO KHỐI");
            dataSheet.SetCellValue("A7", str);
            dataSheet.Name = "XLMH";
            //Lấy danh sách môn học
            List<SubjectCatBO> lstSubject = (from lc in listClassSubject
                                             join sc in SubjectCatBusiness.All on lc.SubjectID equals sc.SubjectCatID
                                             orderby sc.OrderInSubject
                                             select new SubjectCatBO { 
                                             SubjectCatID = lc.SubjectID,
                                             OrderInSubject = sc.OrderInSubject,
                                             DisplayName = sc.DisplayName
                                             }).Distinct().ToList<SubjectCatBO>();
            lstSubject = lstSubject.OrderBy(o => o.OrderInSubject).ThenBy(o => o.DisplayName).ToList();
            //Fill dữ liệu cho từng môn học
            int beginRow = firstRow + 2;
            List<SummedUpRecordBO> lstSummedUpRecordBySubject = new List<SummedUpRecordBO>();
            List<SummedUpRecordBO> lstSummedUpRecord_Edu = new List<SummedUpRecordBO>();
            foreach (SubjectCatBO sc in lstSubject)
            {
                //Tạo khung dữ liệu
                int endRow = beginRow + listEducationLevel.Count;
                for (int i = beginRow; i < endRow; i++)
                {
                    IVTRange copyRange;
                    if (i == beginRow)
                    {
                        copyRange = topRange;
                    }
                    else if (i == endRow - 1)
                    {
                        copyRange = lastRange;
                    }
                    else
                    {
                        copyRange = midRange;
                    }
                    dataSheet.CopyPasteSameSize(copyRange, "A" + i);
                }
                dataSheet.CopyPasteSameSize(summarizeRange, endRow, 1);

                //Merge cell môn học
                dataSheet.MergeColumn('A', beginRow, endRow);

                //Fill dữ liệu dòng tổng
                string fomula = "=SUM(#" + beginRow + ":" + "#" + (endRow - 1) + ")";
                foreach (StatisticsConfig scg in listSC)
                {
                    int col = dicStatisticsConfigToColumn[scg];
                    dataSheet.SetFormulaValue(endRow,
                        col, fomula.Replace("#", VTVector.ColumnIntToString(col)));
                }
                dataSheet.SetFormulaValue("C" + endRow, fomula.Replace("#", "C"));

                //Tạo dữ liệu cần fill
                List<object> listData = new List<object>();
                lstSummedUpRecordBySubject = lstSummedUpRecord.Where(o => o.SubjectID == sc.SubjectCatID).ToList();
               
                foreach (EducationLevel el in listEducationLevel)
                {
                    Dictionary<string, object> dicData = new Dictionary<string, object>();
                    dicData["EducationLevel"] = el.Resolution;
                    string key = sc.SubjectCatID + "_" + el.EducationLevelID;
                    dicData["NumberOfPupil"] = dicSubjectAndPupil.ContainsKey(key)? dicSubjectAndPupil[key] : 0;
                    lstSummedUpRecord_Edu = lstSummedUpRecordBySubject.Where(o => o.EducationLevelID == el.EducationLevelID).ToList();
                    foreach (StatisticsConfig scg in listSC)
                    {
                        int numberOf;
                        if (scg.SubjectType != SystemParamsInFile.StatisticsConfig.SUBJECT_TYPE_JUDGE)
                        {
                            numberOf = (from mr in lstSummedUpRecord_Edu                                      
                                        where ((mr.ReTestMark == null && mr.SummedUpMark < scg.MaxValue) || (mr.ReTestMark != null && mr.ReTestMark < scg.MaxValue))
                                        && ((mr.ReTestMark == null && mr.SummedUpMark >= scg.MinValue) ||(mr.ReTestMark != null && mr.ReTestMark >= scg.MinValue))
                                        select mr.SummedUpRecordID).Count();
                        }
                        else
                        {
                            numberOf = (from mr in lstSummedUpRecord_Edu
                                        where ((mr.ReTestJudgement == null && mr.JudgementResult != null && mr.JudgementResult.ToUpper().Equals(scg.EqualValue.ToUpper())) ||(mr.ReTestJudgement != null && mr.ReTestJudgement.ToUpper().Equals(scg.EqualValue.ToUpper())))
                                        select mr.SummedUpRecordID).Count();
                        }
                        dicData[scg.StatisticsConfigID.ToString()] = numberOf;

                    }
                    listData.Add(dicData);
                }

                //Fill dữ liệu
                dataSheet.GetRange(beginRow, 1, endRow, lastColumn).FillVariableValue(new Dictionary<string, object>
                        {
                            {"list",listData},
                            {"SubjectName",sc.DisplayName}
                        });

                //Tính vị trí tiếp theo
                beginRow = endRow + 1;
            }
            dataSheet.CopyPaste(tempSheet.GetRange(firstRow + 8, 1, firstRow + 8, 100), beginRow + 1, 1);
            dataSheet.FitAllColumnsOnOnePage = true;
            #endregion
            #region Thống kê xếp loại môn học theo khối học sinh nữ
            IVTWorksheet dataSheet_Female = oBook.CopySheetToLast(tempSheet,
new VTVector(firstRow + 2, lastColumn).ToString());

            //Fill dữ liệu chung
            dataSheet_Female.GetRange(1, 1, firstRow - 1, lastColumn).FillVariableValue(dicGeneralInfo);
            dataSheet_Female.SetCellValue("A6", "THỐNG KÊ XẾP LOẠI HỌC LỰC MÔN THEO KHỐI HỌC SINH NỮ");
            dataSheet_Female.SetCellValue("A7", str);
            dataSheet_Female.Name = "HS_Nu";
            beginRow = firstRow + 2;
            lstSummedUpRecordBySubject = new List<SummedUpRecordBO>();
            lstSummedUpRecord_Edu = new List<SummedUpRecordBO>();
            foreach (SubjectCatBO sc in lstSubject)
            {
                //Tạo khung dữ liệu
                int endRow = beginRow + listEducationLevel.Count;
                for (int i = beginRow; i < endRow; i++)
                {
                    IVTRange copyRange;
                    if (i == beginRow)
                    {
                        copyRange = topRange;
                    }
                    else if (i == endRow - 1)
                    {
                        copyRange = lastRange;
                    }
                    else
                    {
                        copyRange = midRange;
                    }
                    dataSheet_Female.CopyPasteSameSize(copyRange, "A" + i);
                }
                dataSheet_Female.CopyPasteSameSize(summarizeRange, endRow, 1);

                //Merge cell môn học
                dataSheet_Female.MergeColumn('A', beginRow, endRow);

                //Fill dữ liệu dòng tổng
                string fomula = "=SUM(#" + beginRow + ":" + "#" + (endRow - 1) + ")";
                foreach (StatisticsConfig scg in listSC)
                {
                    int col = dicStatisticsConfigToColumn[scg];
                    dataSheet_Female.SetFormulaValue(endRow,
                        col, fomula.Replace("#", VTVector.ColumnIntToString(col)));
                }
                dataSheet_Female.SetFormulaValue("C" + endRow, fomula.Replace("#", "C"));

                //Tạo dữ liệu cần fill
                List<object> listData = new List<object>();
                lstSummedUpRecordBySubject = lstSummedUpRecord_Female.Where(o => o.SubjectID == sc.SubjectCatID).ToList();

                foreach (EducationLevel el in listEducationLevel)
                {
                    Dictionary<string, object> dicData = new Dictionary<string, object>();
                    dicData["EducationLevel"] = el.Resolution;
                    string key = sc.SubjectCatID + "_" + el.EducationLevelID;
                    dicData["NumberOfPupil"] = dicSubjectAndPupil_Female.ContainsKey(key) ? dicSubjectAndPupil_Female[key] : 0;
                    lstSummedUpRecord_Edu = lstSummedUpRecordBySubject.Where(o => o.EducationLevelID == el.EducationLevelID).ToList();
                    foreach (StatisticsConfig scg in listSC)
                    {
                        int numberOf;
                        if (scg.SubjectType != SystemParamsInFile.StatisticsConfig.SUBJECT_TYPE_JUDGE)
                        {
                            numberOf = (from mr in lstSummedUpRecord_Edu
                                        where ((mr.ReTestMark == null && mr.SummedUpMark < scg.MaxValue) || (mr.ReTestMark != null && mr.ReTestMark < scg.MaxValue))
                                        && ((mr.ReTestMark == null && mr.SummedUpMark >= scg.MinValue) || (mr.ReTestMark != null && mr.ReTestMark >= scg.MinValue))
                                        select mr.SummedUpRecordID).Count();
                        }
                        else
                        {
                            numberOf = (from mr in lstSummedUpRecord_Edu
                                        where ((mr.ReTestJudgement == null && mr.JudgementResult != null && mr.JudgementResult.ToUpper().Equals(scg.EqualValue.ToUpper())) || (mr.ReTestJudgement != null && mr.ReTestJudgement.ToUpper().Equals(scg.EqualValue.ToUpper())))
                                        select mr.SummedUpRecordID).Count();
                        }
                        dicData[scg.StatisticsConfigID.ToString()] = numberOf;

                    }
                    listData.Add(dicData);
                }

                //Fill dữ liệu
                dataSheet_Female.GetRange(beginRow, 1, endRow, lastColumn).FillVariableValue(new Dictionary<string, object>
                        {
                            {"list",listData},
                            {"SubjectName",sc.DisplayName}
                        });

                //Tính vị trí tiếp theo
                beginRow = endRow + 1;
            }
            dataSheet_Female.CopyPaste(tempSheet.GetRange(firstRow + 8, 1, firstRow + 8, 100), beginRow + 1, 1);
            dataSheet_Female.FitAllColumnsOnOnePage = true;
            #endregion 
            #region Thống kê xếp loại môn học theo khối học sinh dân tộc
            IVTWorksheet dataSheet_Ethnic = oBook.CopySheetToLast(tempSheet,
new VTVector(firstRow + 2, lastColumn).ToString());

            //Fill dữ liệu chung
            dataSheet_Ethnic.GetRange(1, 1, firstRow - 1, lastColumn).FillVariableValue(dicGeneralInfo);
            dataSheet_Ethnic.SetCellValue("A6", "THỐNG KÊ XẾP LOẠI HỌC LỰC MÔN THEO KHỐI HỌC SINH DÂN TỘC");
            dataSheet_Ethnic.SetCellValue("A7", str);
            dataSheet_Ethnic.Name = "HS_DT";
            beginRow = firstRow + 2;
            lstSummedUpRecordBySubject = new List<SummedUpRecordBO>();
            lstSummedUpRecord_Edu = new List<SummedUpRecordBO>();
            foreach (SubjectCatBO sc in lstSubject)
            {
                //Tạo khung dữ liệu
                int endRow = beginRow + listEducationLevel.Count;
                for (int i = beginRow; i < endRow; i++)
                {
                    IVTRange copyRange;
                    if (i == beginRow)
                    {
                        copyRange = topRange;
                    }
                    else if (i == endRow - 1)
                    {
                        copyRange = lastRange;
                    }
                    else
                    {
                        copyRange = midRange;
                    }
                    dataSheet_Ethnic.CopyPasteSameSize(copyRange, "A" + i);
                }
                dataSheet_Ethnic.CopyPasteSameSize(summarizeRange, endRow, 1);

                //Merge cell môn học
                dataSheet_Ethnic.MergeColumn('A', beginRow, endRow);

                //Fill dữ liệu dòng tổng
                string fomula = "=SUM(#" + beginRow + ":" + "#" + (endRow - 1) + ")";
                foreach (StatisticsConfig scg in listSC)
                {
                    int col = dicStatisticsConfigToColumn[scg];
                    dataSheet_Ethnic.SetFormulaValue(endRow,
                        col, fomula.Replace("#", VTVector.ColumnIntToString(col)));
                }
                dataSheet_Ethnic.SetFormulaValue("C" + endRow, fomula.Replace("#", "C"));

                //Tạo dữ liệu cần fill
                List<object> listData = new List<object>();
                lstSummedUpRecordBySubject = lstSummedUpRecord_Ethnic.Where(o => o.SubjectID == sc.SubjectCatID).ToList();

                foreach (EducationLevel el in listEducationLevel)
                {
                    Dictionary<string, object> dicData = new Dictionary<string, object>();
                    dicData["EducationLevel"] = el.Resolution;
                    string key = sc.SubjectCatID + "_" + el.EducationLevelID;
                    dicData["NumberOfPupil"] = dicSubjectAndPupil_Ethnic.ContainsKey(key) ? dicSubjectAndPupil_Ethnic[key] : 0;
                    lstSummedUpRecord_Edu = lstSummedUpRecordBySubject.Where(o => o.EducationLevelID == el.EducationLevelID).ToList();
                    foreach (StatisticsConfig scg in listSC)
                    {
                        int numberOf;
                        if (scg.SubjectType != SystemParamsInFile.StatisticsConfig.SUBJECT_TYPE_JUDGE)
                        {
                            numberOf = (from mr in lstSummedUpRecord_Edu
                                        where ((mr.ReTestMark == null && mr.SummedUpMark < scg.MaxValue) || (mr.ReTestMark != null && mr.ReTestMark < scg.MaxValue))
                                        && ((mr.ReTestMark == null && mr.SummedUpMark >= scg.MinValue) || (mr.ReTestMark != null && mr.ReTestMark >= scg.MinValue))
                                        select mr.SummedUpRecordID).Count();
                        }
                        else
                        {
                            numberOf = (from mr in lstSummedUpRecord_Edu
                                        where ((mr.ReTestJudgement == null && mr.JudgementResult != null && mr.JudgementResult.ToUpper().Equals(scg.EqualValue.ToUpper())) || (mr.ReTestJudgement != null && mr.ReTestJudgement.ToUpper().Equals(scg.EqualValue.ToUpper())))
                                        select mr.SummedUpRecordID).Count();
                        }
                        dicData[scg.StatisticsConfigID.ToString()] = numberOf;

                    }
                    listData.Add(dicData);
                }

                //Fill dữ liệu
                dataSheet_Ethnic.GetRange(beginRow, 1, endRow, lastColumn).FillVariableValue(new Dictionary<string, object>
                        {
                            {"list",listData},
                            {"SubjectName",sc.DisplayName}
                        });

                //Tính vị trí tiếp theo
                beginRow = endRow + 1;
            }
            dataSheet_Ethnic.CopyPaste(tempSheet.GetRange(firstRow + 8, 1, firstRow + 8, 100), beginRow + 1, 1);
            dataSheet_Ethnic.FitAllColumnsOnOnePage = true;
            #endregion
            //Xoá sheet template
            firstSheet.Delete();
            tempSheet.Delete();

            return oBook.ToStream();
        }
        #endregion

    }
}
