using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Linq.Expressions;
using System.Web;
using log4net;

using SMAS.DAL.Repository;
using SMAS.DAL.IRepository;
using SMAS.Business.IBusiness;
using SMAS.Models.Models;
using SMAS.Business.BusinessObject;
using SMAS.Business.Common;
using System.IO;
using SMAS.VTUtils.Excel.Export;

namespace SMAS.Business.Business
{
    public class ClassifiedConductByPeriodBusiness : GenericBussiness<ProcessedReport>, IClassifiedConductByPeriodBusiness
    {
        IProcessedReportParameterRepository ProcessedReportParameterRepository;
        IProcessedReportRepository ProcessedReportRepository;
        IEducationLevelRepository EducationLevelRepository;
        IAcademicYearRepository AcademicYearRepository;
        IMarkRecordRepository MarkRecordRepository;
        IPupilProfileRepository PupilProfileRepository;
        ISummedUpRecordRepository SummedUpRecordRepository;

        public ClassifiedConductByPeriodBusiness(ILog logger, SMASEntities context = null)
            : base(logger)
        {
            if (context == null) { context = new SMASEntities(); } this.context = context;
            this.ProcessedReportParameterRepository = new ProcessedReportParameterRepository(context);
            this.ProcessedReportRepository = new ProcessedReportRepository(context);
            this.EducationLevelRepository = new EducationLevelRepository(context);
            this.AcademicYearRepository = new AcademicYearRepository(context);
            this.MarkRecordRepository = new MarkRecordRepository(context);
            this.PupilProfileRepository = new PupilProfileRepository(context);
            this.SummedUpRecordRepository = new SummedUpRecordRepository(context);
        }

        #region Tạo mảng băm cho các tham số đầu vào cho báo cáo
        public string GetHashKey(ClassifiedConductByPeriod entity)
        {
            IDictionary<string, object> dic = new Dictionary<string, object> {
                {"AcademicYearID", entity.AcademicYearID},
                {"PeriodID ", entity.PeriodID},
                {"SchoolID ", entity.SchoolID},
                {"AppliedLevel ", entity.AppliedLevel}
            };
            return ReportUtils.GetHashKey(dic);
        }
        #endregion

        #region Lấy danh sách điểm học kỳ cấp 1 được cập nhật mới nhất
        public ProcessedReport GetClassifiedConductByPeriod(ClassifiedConductByPeriod entity)
        {
            string reportCode = SystemParamsInFile.REPORT_THONG_KE_XLHK_THEO_DOT;
            string inputParameterHashKey = GetHashKey(entity);
            return ProcessedReportBusiness.GetProcessedReport(reportCode, inputParameterHashKey);
        }
        #endregion

        #region Lưu báo cáo danh sách điểm học kỳ cấp 1 vào CSDL
        public ProcessedReport InsertClassifiedConductByPeriod(ClassifiedConductByPeriod entity, Stream data)
        {
            string reportCode = SystemParamsInFile.REPORT_THONG_KE_XLHK_THEO_DOT;
            ReportDefinition reportDef = ReportDefinitionBusiness.GetByCode(reportCode);
            ProcessedReport pr = new ProcessedReport();
            pr.ReportCode = reportCode;
            pr.ProcessedDate = DateTime.Now;
            pr.InputParameterHashKey = GetHashKey(entity);
            pr.ReportData = ReportUtils.Compress(data);

            //Tạo tên file HS_PTTH_ThongKeXLHK_[Semester]_[Period]
            string outputNamePattern = reportDef.OutputNamePattern;
            PeriodDeclaration period = PeriodDeclarationBusiness.Find(entity.PeriodID);
            string periodName = period.Resolution;
            string semester = ReportUtils.ConvertSemesterForReportName(period.Semester.Value);
            string appliedLevel = ReportUtils.ConvertAppliedLevelForReportName(entity.AppliedLevel);

            outputNamePattern = outputNamePattern.Replace("[Period]", periodName);
            outputNamePattern = outputNamePattern.Replace("[Semester]", semester);
            outputNamePattern = outputNamePattern.Replace("[SchoolType]", appliedLevel);
            pr.ReportName = ReportUtils.RemoveSpecialCharacters(outputNamePattern) + "." + reportDef.OutputFormat;

            IDictionary<string, object> dic = new Dictionary<string, object> {
                {"AcademicYearID", entity.AcademicYearID},
                {"PeriodID ", entity.PeriodID},
                {"SchoolID ", entity.SchoolID},
                {"AppliedLevel ", entity.AppliedLevel}
            };
            ProcessedReportParameterRepository.Insert(dic, pr);
            ProcessedReportRepository.Insert(pr);
            ProcessedReportRepository.Save();
            return pr;
        }
        #endregion

        #region Tạo file báo cáo danh sách điểm thi học kỳ cấp 1
        public Stream CreateClassifiedConductByPeriod(ClassifiedConductByPeriod entity)
        {
            string reportCode = SystemParamsInFile.REPORT_THONG_KE_XLHK_THEO_DOT;
            ReportDefinition reportDef = ReportDefinitionBusiness.GetByCode(reportCode);
            string templatePath = ReportUtils.GetTemplatePath(reportDef);
            IVTWorkbook oBook = VTExport.OpenWorkbook(templatePath);

            int firstRow = 9;
            int defaultNumbeOfColumn = 4;
            int firstDynamicCol = VTVector.dic['F'];
            int defaultLastCol = VTVector.dic['O'];
            //Lấy sheet template
            IVTWorksheet firstSheet = oBook.GetSheet(1);
            IVTRange subjectTemp = firstSheet.GetRange(firstRow, firstDynamicCol, firstRow + 6, firstDynamicCol + 1);

            //Tạo template chuẩn
            IVTWorksheet tempSheet = oBook.CopySheetToLast(firstSheet, "O" + (firstRow - 1));
            tempSheet.CopyPasteSameSize(firstSheet.GetRange(firstRow, 1, firstRow + 6, firstDynamicCol - 1), "A" + firstRow);
            tempSheet.CopyPaste(firstSheet.GetRange(firstRow + 8, 1, firstRow + 8, 100), firstRow + 8, 1);

            //Lấy danh sách các cột động
            List<StatisticsConfig> listSC = StatisticsConfigBusiness.GetList(
                entity.AppliedLevel,
                SystemParamsInFile.StatisticsConfig.REPORT_TYPE_HANH_KIEM);


            //Tạo các cột động
            Dictionary<StatisticsConfig, int> dicStatisticsConfigToColumn = new Dictionary<StatisticsConfig, int>();
            for (int i = 0; i < listSC.Count; i++)
            {
                StatisticsConfig sc = listSC[i];
                tempSheet.CopyPasteSameSize(subjectTemp, firstRow, firstDynamicCol + i * 2);
                tempSheet.SetCellValue(firstRow, firstDynamicCol + i * 2, sc.DisplayName);
                dicStatisticsConfigToColumn.Add(sc, firstDynamicCol + i * 2);
            }

            //Copy cột TB trở lên
            tempSheet.CopyPasteSameSize(firstSheet.GetRange(firstRow, firstDynamicCol + 2 * defaultNumbeOfColumn,
                firstRow + 6, firstDynamicCol + 2 * defaultNumbeOfColumn + 1), firstRow, firstDynamicCol + listSC.Count * 2);

            int lastCol = defaultLastCol + (listSC.Count - defaultNumbeOfColumn) * 2;

            //Chỉnh lại dòng khác
            if (listSC.Count > defaultNumbeOfColumn)
            {
                tempSheet.MergeRow(2, VTVector.dic['G'], lastCol);
                tempSheet.MergeRow(3, VTVector.dic['G'], lastCol);
                tempSheet.MergeRow(6, VTVector.dic['A'], lastCol);
                tempSheet.MergeRow(7, VTVector.dic['A'], lastCol);

                double colWidth = firstSheet.GetColumnWidth(firstDynamicCol);
                double resizeWidth = colWidth * defaultNumbeOfColumn / listSC.Count;
                for (int i = firstDynamicCol; i < lastCol; i++)
                {
                    tempSheet.SetColumnWidth(i, resizeWidth);
                }
            }

            //Tính các công thức 
            string formulaPercent = "=IF(cellNumberOfPupil=0;0;ROUND(cellTotal/cellNumberOfPupil;4)*100)";

            var lstAboveAve = (from sc in listSC
                               where
                                   sc.MarkType == SystemParamsInFile.StatisticsConfig.MARK_TYPE_ABOVE_AVERAGE
                               select VTVector.ColumnIntToString(dicStatisticsConfigToColumn[sc])).ToArray();
            string formulaAverageTotal = "=SUM(" + string.Join("#;", lstAboveAve) + "#)";
            for (int i = 0; i < listSC.Count; i++)
            {
                StatisticsConfig sc = listSC[i];
                tempSheet.SetCellValue(firstRow + 4, dicStatisticsConfigToColumn[sc], "${list[].Get(" + sc.StatisticsConfigID + ")}");
            }

            for (int rowIndex = firstRow + 2; rowIndex < firstRow + 7; rowIndex++)
            {
                for (int i = firstDynamicCol; i < lastCol; i += 2)
                {
                    string cellNumberOfPupil = "E" + rowIndex;
                    string cellTotal = new VTVector(rowIndex, i).ToString();
                    tempSheet.SetFormulaValue(rowIndex, i + 1, formulaPercent.
                        Replace("cellTotal", cellTotal).Replace("cellNumberOfPupil", cellNumberOfPupil));
                }

                tempSheet.SetFormulaValue(rowIndex, lastCol - 1, formulaAverageTotal.Replace("#", rowIndex.ToString()));
            }

            //Fill dữ liệu chung cho template
            AcademicYear academicYear = AcademicYearBusiness.Find(entity.AcademicYearID);
            SchoolProfile school = SchoolProfileBusiness.Find(entity.SchoolID);
            Province province = school.Province;

            string schoolName = school.SchoolName.ToUpper();
            string supervisingDeptName = UtilsBusiness.GetSupervisingDeptName(school.SchoolProfileID, entity.AppliedLevel).ToUpper();
            string academicYearTitle = academicYear.DisplayTitle;
            string provinceName = (school.District != null ? school.District.DistrictName : "");
            DateTime reportDate = DateTime.Now;
            PeriodDeclaration period = PeriodDeclarationBusiness.Find(entity.PeriodID);
            if (period == null)
            {
                throw new BusinessException("Chưa chọn đợt");
            }
            string periodName = period.Resolution;
            string semester = SMASConvert.ConvertSemester(period.Semester.Value).ToUpper();

            Dictionary<string, object> dicGeneralInfo = new Dictionary<string, object> 
                {
                    {"SchoolName", schoolName}, 
                    {"SupervisingDeptName", supervisingDeptName},
                    {"AcademicYearTitle", academicYearTitle},
                    {"Period", periodName},
                    {"ProvinceName", provinceName},
                    {"ReportDate", reportDate},
                    {"Semester", semester}
                };

            tempSheet.GetRange(1, 1, firstRow - 1, defaultLastCol).FillVariableValue(dicGeneralInfo);

            //Tạo sheet Fill dữ liệu
            int lastColumn = Math.Max(lastCol, defaultLastCol);
            IVTWorksheet dataSheet = oBook.CopySheetToLast(tempSheet,
                new VTVector(firstRow + 2, lastColumn).ToString());

            //Tạo vùng cho từng khối
            IVTRange summarizeRange = tempSheet.GetRange(firstRow + 3, 1, firstRow + 3, lastColumn);
            IVTRange topRange = tempSheet.GetRange(firstRow + 4, 1, firstRow + 4, lastColumn);
            IVTRange midRange = tempSheet.GetRange(firstRow + 5, 1, firstRow + 5, lastColumn);
            IVTRange lastRange = tempSheet.GetRange(firstRow + 6, 1, firstRow + 6, lastColumn);



            #region Danh sach lop, si so
            // Danh sach khoi
            List<EducationLevel> lstEducationLevel = EducationLevelBusiness.GetByGrade(entity.AppliedLevel).ToList();


            //Danh sach lop hoc
            IQueryable<ClassProfile> lsClass = ClassProfileBusiness.SearchBySchool(entity.SchoolID, new Dictionary<string, object> {
                                                {"AppliedLevel", entity.AppliedLevel},
                                                {"AcademicYearID", entity.AcademicYearID},
                                                {"IsVNEN",true}});

            IQueryable<PupilOfClass> lsPupilOfClassSemester1 = PupilOfClassBusiness.SearchBySchool(entity.SchoolID, new Dictionary<string, object> {
                                                {"AppliedLevel", entity.AppliedLevel},
                                                {"AcademicYearID", entity.AcademicYearID},
                                                {"FemaleID",entity.FemaleID},
                                                {"EthnicID",entity.EthnicID}
                                                }).AddCriteriaSemester(AcademicYearBusiness.Find(entity.AcademicYearID), period.Semester.Value);
            IQueryable<PupilOfClass> lsPupilOfClassSemester = lsPupilOfClassSemester1.Where(o => o.AssignedDate == lsPupilOfClassSemester1.Where(u => u.PupilID == o.PupilID && u.ClassID == o.ClassID).Select(u => u.AssignedDate).Max());

            /*
            //lay danh sach hoc sinh dang ky mon chuyen mon tu chon
            IDictionary<string, object> dic = new Dictionary<string, object>()
            {
                {"SchoolID",entity.SchoolID},
                {"AcademicYearID",entity.AcademicYearID},
                //{"SubjectID",Entity.SubjectID},
                {"SemesterID",entity.SemesterID},
                {"YearID",AcademicYearBusiness.Find(entity.AcademicYearID).Year}
            };
            
            List<RegisterSubjectSpecializeBO> lstRegis = RegisterSubjectSpecializeBusiness.GetlistSubjectByClass(dic);
            List<int> lstPupilID = lstRegis.Select(p => p.PupilID).ToList();

            lsPupilOfClassSemester = lsPupilOfClassSemester.Where(p => lstPupilID.Contains(p.PupilID));*/
            IDictionary<string, object> dicCATP = new Dictionary<string, object>()
                {
                    {"SchoolID",entity.SchoolID},
                    {"AcademicYearID",entity.AcademicYearID},
                    {"Semester",entity.SemesterID},
                    {"Type",SystemParamsInFile.GET_NUMPUPIL_IN_CLASS_TYPE0}
                };
            IQueryable<ClassInfoBO> iqClassInfoBO = PupilOfClassBusiness.GetClassAndTotalPupil(lsClass, dicCATP);

            List<ClassInfoBO> ListClass = iqClassInfoBO.ToList();
            #endregion

            //fill dữ liệu cho các khối
            IQueryable<VPupilRanking> iqPupilRanking = VPupilRankingBusiness.SearchBySchool(entity.SchoolID, new Dictionary<string, object> {
                                                    {"AppliedLevel", entity.AppliedLevel},
                                                    {"AcademicYearID", entity.AcademicYearID},
                                                    {"PeriodID", entity.PeriodID}
                                                }).Where(u => lsPupilOfClassSemester.Any(o => o.PupilID == u.PupilID && o.ClassID == u.ClassID))
                                                .Where(o => o.ConductLevelID != null);

            var listMarkGroup = from s in listSC
                                join m in iqPupilRanking on 1 equals 1
                                where s.EqualValue == m.ConductLevelID.ToString()
                                select new
                                {
                                    s.StatisticsConfigID,
                                    s.MinValue,
                                    s.MaxValue,
                                    s.MarkType,
                                    s.MappedColumn,
                                    m.SchoolID,
                                    m.EducationLevelID,
                                    m.Semester,
                                    m.PeriodID,
                                    m.ClassID
                                };

            var listMarkGroupCount = (from g in listMarkGroup
                                      group g by new
                                      {
                                          g.SchoolID,
                                          g.EducationLevelID,
                                          g.StatisticsConfigID,
                                          g.MinValue,
                                          g.MaxValue,
                                          g.MarkType,
                                          g.MappedColumn,
                                          g.Semester,
                                          g.PeriodID,
                                          g.ClassID
                                      } into c
                                      select new
                                      {
                                          c.Key.SchoolID,
                                          c.Key.EducationLevelID,
                                          c.Key.StatisticsConfigID,
                                          c.Key.MinValue,
                                          c.Key.MaxValue,
                                          c.Key.MarkType,
                                          c.Key.MappedColumn,
                                          c.Key.Semester,
                                          c.Key.PeriodID,
                                          c.Key.ClassID,
                                          CountMark = c.Count()
                                      }
                                     ).ToList();




            IDictionary<string, object> dicConductRankByPeriod = new Dictionary<string, object>()
            {
                {"AcademicYearID", entity.AcademicYearID},
                {"PeriodID", entity.PeriodID}
            };
            List<VPupilRanking> lstConductRankByPeriod = VPupilRankingBusiness.SearchBySchool(entity.SchoolID, dicConductRankByPeriod).ToList();
            List<int> listBeginRow = new List<int>();
            int beginRow = firstRow + 3;
            int indexEducation = 0;
            foreach (EducationLevel educationLevel in lstEducationLevel)
            {
                listBeginRow.Add(beginRow);
                int endRow = beginRow + 1;
                indexEducation++;
                dataSheet.CopyPasteSameSize(summarizeRange, beginRow, 1);
                dataSheet.SetCellValue("A" + beginRow, indexEducation);
                dataSheet.SetCellValue("B" + beginRow, educationLevel.Resolution);

                List<ClassInfoBO> listCP = ListClass.Where(o => o.EducationLevelID == educationLevel.EducationLevelID)
                .OrderBy(o => o.ClassOrderNumber.HasValue ? o.ClassOrderNumber : 0).ThenBy(o => o.ClassName).ToList();

                if (listCP == null || listCP.Count == 0)
                {
                    foreach (StatisticsConfig sc in listSC)
                    {
                        dataSheet.SetCellValue(beginRow, dicStatisticsConfigToColumn[sc], 0);
                    }
                    dataSheet.SetCellValue("D" + beginRow, 0);
                    dataSheet.SetCellValue("E" + beginRow, 0);
                    endRow = beginRow;
                }
                else
                {
                    //Fill dữ liệu dòng tổng
                    endRow = beginRow + listCP.Count();
                    string fomula = "=SUM(#" + (beginRow + 1) + ":" + "#" + endRow + ")";
                    foreach (StatisticsConfig sc in listSC)
                    {
                        int col = dicStatisticsConfigToColumn[sc];
                        dataSheet.SetFormulaValue(beginRow,
                            col, fomula.Replace("#", VTVector.ColumnIntToString(col)));
                    }
                    dataSheet.SetFormulaValue("D" + beginRow, fomula.Replace("#", "D"));
                    dataSheet.SetFormulaValue("E" + beginRow, fomula.Replace("#", "E"));

                    //Tạo khung dữ liệu danh sách các lớp
                    for (int i = beginRow + 1; i <= endRow; i++)
                    {
                        IVTRange copyRange;
                        if (i == (beginRow + 1))
                        {
                            copyRange = topRange;
                        }
                        else if (i == endRow)
                        {
                            copyRange = lastRange;
                        }
                        else
                        {
                            copyRange = midRange;
                        }
                        dataSheet.CopyPasteSameSize(copyRange, "A" + i);
                    }

                    //Tạo dữ liệu cần fill
                    List<object> listData = new List<object>();
                    int indexClass = 0;
                    foreach (ClassInfoBO cp in listCP)
                    {
                        indexClass++;
                        Dictionary<string, object> dicData = new Dictionary<string, object>();
                        dicData["Order"] = indexClass;
                        dicData["ClassName"] = cp.ClassName;
                        dicData["HeadTeacherName"] = cp.TeacherName;
                        dicData["NumberOfPupil"] = cp.NumOfPupil;
                        dicData["ActualNumberOfPupil"] = cp.ActualNumOfPupil;
                        foreach (StatisticsConfig sc in listSC)
                        {
                            //HieuND : Chi diem hoc sinh dang hoc
                            int ConductLevelID = int.Parse(sc.EqualValue);
                            var pupilConductCount = listMarkGroupCount.Where(o => o.ClassID == cp.ClassID && o.StatisticsConfigID == sc.StatisticsConfigID).FirstOrDefault();
                            dicData[sc.StatisticsConfigID.ToString()] = pupilConductCount != null ? pupilConductCount.CountMark : 0;
                        }
                        listData.Add(dicData);
                    }

                    //Fill dữ liệu
                    dataSheet.GetRange(beginRow, 1, endRow, lastColumn).FillVariableValue(new Dictionary<string, object>
                    {
                        {"list",listData}
                    });
                }

                //Tính vị trí tiếp theo
                beginRow = endRow + 1;
            }

            string formulaSum = "=SUM(#" + string.Join(";#", listBeginRow.ToArray<int>()) + ")";

            foreach (StatisticsConfig sc in listSC)
            {
                int col = dicStatisticsConfigToColumn[sc];
                dataSheet.SetFormulaValue(firstRow + 2,
                    col, formulaSum.Replace("#", VTVector.ColumnIntToString(col)));
            }
            dataSheet.SetFormulaValue("D" + (firstRow + 2), formulaSum.Replace("#", "D"));
            dataSheet.SetFormulaValue("E" + (firstRow + 2), formulaSum.Replace("#", "E"));

            dataSheet.CopyPaste(tempSheet.GetRange(firstRow + 8, 1, firstRow + 8, 100), beginRow + 1, 1);

            dataSheet.GetRange(9, 1, beginRow - 1, lastCol).SetBorder(VTBorderStyle.Solid, VTBorderWeight.Medium, VTBorderIndex.Around);

            //Xoá sheet template
            firstSheet.Delete();
            tempSheet.Delete();

            return oBook.ToStream();
        }
        #endregion
    }
}
