﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Linq.Expressions;
using System.Web;
using log4net;

using SMAS.DAL.Repository;
using SMAS.DAL.IRepository;
using SMAS.Business.IBusiness;
using SMAS.Models.Models;
using SMAS.Business.BusinessObject;
using SMAS.Business.Common;
using System.IO;
using SMAS.VTUtils.Excel.Export;

namespace SMAS.Business.Business
{
    public class ReportJudgeRecordBusiness : GenericBussiness<ProcessedReport>, IReportJudgeRecordBusiness
    {

        IProcessedReportParameterRepository ProcessedReportParameterRepository;
        IProcessedReportRepository ProcessedReportRepository;
        IEducationLevelRepository EducationLevelRepository;
        IAcademicYearRepository AcademicYearRepository;
        IJudgeRecordRepository JudgeRecordRepository;
        IPupilProfileRepository PupilProfileRepository;
        ISummedUpRecordRepository SummedUpRecordRepository;

        public ReportJudgeRecordBusiness(ILog logger, SMASEntities context = null)
            : base(logger)
        {
            if (context == null) { context = new SMASEntities(); } this.context = context;
            this.ProcessedReportParameterRepository = new ProcessedReportParameterRepository(context);
            this.ProcessedReportRepository = new ProcessedReportRepository(context);
            this.EducationLevelRepository = new EducationLevelRepository(context);
            this.AcademicYearRepository = new AcademicYearRepository(context);
            this.JudgeRecordRepository = new JudgeRecordRepository(context);
            this.PupilProfileRepository = new PupilProfileRepository(context);
            this.SummedUpRecordRepository = new SummedUpRecordRepository(context);
        }

        #region Tạo mảng băm cho các tham số đầu vào cho báo cáo
        public string GetHashKeyPrimary(ReportJudgeRecordSearchBO entity)
        {
            IDictionary<string, object> dic = new Dictionary<string, object> {
                {"AcademicYearID", entity.AcademicYearID},
                {"EducationLevelID ", entity.EducationLevelID},
                {"SchoolID", entity.SchoolID},
                {"Semester", entity.Semester},
                {"SubjectID", entity.SubjectID}
            };
            return ReportUtils.GetHashKey(dic);
        }
        #endregion

        #region Lấy danh sách điểm học kỳ cấp 1 được cập nhật mới nhất
        public ProcessedReport GetReportJudgeRecordPrimary(ReportJudgeRecordSearchBO entity)
        {
            string reportCode = SystemParamsInFile.REPORT_DS_HS_DIEM_THI_MON_NHAN_XET_CAP1;
            string inputParameterHashKey = GetHashKeyPrimary(entity);
            return ProcessedReportBusiness.GetProcessedReport(reportCode, inputParameterHashKey);
        }
        #endregion

        #region Lưu báo cáo danh sách điểm học kỳ cấp 1 vào CSDL
        public ProcessedReport InsertReportJudgeRecordPrimary(ReportJudgeRecordSearchBO entity, Stream data)
        {
            string reportCode = SystemParamsInFile.REPORT_DS_HS_DIEM_THI_MON_NHAN_XET_CAP1;
            ReportDefinition reportDef = ReportDefinitionBusiness.GetByCode(reportCode);
            ProcessedReport pr = new ProcessedReport();
            pr.ReportCode = reportCode;
            pr.ProcessedDate = DateTime.Now;
            pr.InputParameterHashKey = GetHashKeyPrimary(entity);
            pr.ReportData = ReportUtils.Compress(data);

            //Tạo tên file
            string outputNamePattern = reportDef.OutputNamePattern;
            string subject = SubjectCatBusiness.Find(entity.SubjectID).DisplayName;
            string semester = ReportUtils.ConvertSemesterForReportName(entity.Semester);

            outputNamePattern = outputNamePattern.Replace("[Subject]", subject);
            outputNamePattern = outputNamePattern.Replace("[Semester]", semester);
            pr.ReportName = ReportUtils.RemoveSpecialCharacters(outputNamePattern) + "." + reportDef.OutputFormat;

            IDictionary<string, object> dic = new Dictionary<string, object> {
                {"AcademicYearID", entity.AcademicYearID},
                {"EducationLevelID ", entity.EducationLevelID},
                {"SchoolID", entity.SchoolID},
                {"Semester", entity.Semester},
                {"SubjectID", entity.SubjectID}
            };
            ProcessedReportParameterRepository.Insert(dic, pr);
            ProcessedReportRepository.Insert(pr);
            ProcessedReportRepository.Save();
            return pr;
        }
        #endregion

        #region Tạo file báo cáo danh sách điểm thi học kỳ cấp 1
        public Stream CreateReportJudgeRecordPrimary(ReportJudgeRecordSearchBO entity)
        {
            SubjectCatBusiness.CheckAvailable(entity.SubjectID, "LearningOutcomePrimaryReport_Label_Subject");
            string reportCode = SystemParamsInFile.REPORT_DS_HS_DIEM_THI_MON_NHAN_XET_CAP1;
            ReportDefinition reportDef = ReportDefinitionBusiness.GetByCode(reportCode);
            string templatePath = ReportUtils.GetTemplatePath(reportDef);
            IVTWorkbook oBook = VTExport.OpenWorkbook(templatePath);

            int firstRow = 9;
            int firstDynamicCol = VTVector.dic['D'];
            //Lấy sheet template
            IVTWorksheet firstSheet = oBook.GetSheet(1);
            IVTRange subjectTemp = firstSheet.GetRange("D" + firstRow, "E" + (firstRow + 6));

            //Tạo template chuẩn
            IVTWorksheet tempSheet = oBook.CopySheetToLast(firstSheet, "I" + (firstRow - 1));
            tempSheet.CopyPasteSameSize(firstSheet.GetRange("A" + firstRow, "C" + (firstRow + 6)), "A" + firstRow);
            tempSheet.CopyPaste(firstSheet.GetRange(firstRow + 8, 1, firstRow + 8, 100), firstRow + 8, 1);

            //Lấy danh sách các cột động
            List<StatisticsConfig> listSC = StatisticsConfigBusiness.GetList(
                SystemParamsInFile.APPLIED_LEVEL_PRIMARY,
                SystemParamsInFile.StatisticsConfig.REPORT_TYPE_DIEM_THI_HOC_KY,
                SystemParamsInFile.StatisticsConfig.MARK_TYPE_ALL,
                SystemParamsInFile.StatisticsConfig.SUBJECT_TYPE_JUDGE);


            //Tạo các cột động
            Dictionary<StatisticsConfig, int> dicStatisticsConfigToColumn = new Dictionary<StatisticsConfig, int>();
            for (int i = 0; i < listSC.Count; i++)
            {
                StatisticsConfig sc = listSC[i];
                tempSheet.CopyPasteSameSize(subjectTemp, firstRow, firstDynamicCol + i * 2);
                tempSheet.SetCellValue(firstRow, firstDynamicCol + i * 2, sc.DisplayName);
                dicStatisticsConfigToColumn.Add(sc, firstDynamicCol + i * 2);
            }

            int lastCol = VTVector.dic['I'] + (listSC.Count - 3) * 2;

            //Chỉnh lại dòng khác
            if (listSC.Count > 2)
            {
                tempSheet.MergeRow(6, VTVector.dic['A'], lastCol);
                tempSheet.MergeRow(7, VTVector.dic['A'], lastCol);
            }

            if (listSC.Count > 3)
            {
                tempSheet.MergeRow(2, VTVector.dic['F'], lastCol);
                tempSheet.MergeRow(3, VTVector.dic['F'], lastCol);

                double colWidth = firstSheet.GetColumnWidth('D');
                double resizeWidth = colWidth * 3 / listSC.Count;
                for (int i = VTVector.dic['D']; i < lastCol; i++)
                {
                    tempSheet.SetColumnWidth(i, resizeWidth);
                }
            }

            //Tính các công thức 
            string formulaPercent = "=IF(cellNumberOfPupil=0;0;ROUND(cellTotal/cellNumberOfPupil;4)*100)";

            for (int i = 0; i < listSC.Count; i++)
            {
                StatisticsConfig sc = listSC[i];
                tempSheet.SetCellValue(firstRow + 4, dicStatisticsConfigToColumn[sc], "${list[].Get(" + sc.StatisticsConfigID + ")}");
            }

            for (int rowIndex = firstRow + 2; rowIndex < firstRow + 7; rowIndex++)
            {
                for (int i = VTVector.dic['D']; i < lastCol; i += 2)
                {
                    string cellNumberOfPupil = "C" + rowIndex;
                    string cellTotal = new VTVector(rowIndex, i).ToString();
                    tempSheet.SetFormulaValue(rowIndex, i + 1, formulaPercent.
                        Replace("cellTotal", cellTotal).Replace("cellNumberOfPupil", cellNumberOfPupil));
                }
            }

            //Fill dữ liệu chung cho template
            AcademicYear academicYear = AcademicYearBusiness.Find(entity.AcademicYearID);
            SchoolProfile school = SchoolProfileBusiness.Find(entity.SchoolID);
            SubjectCat subject = SubjectCatBusiness.Find(entity.SubjectID);
            Province province = school.Province;

            string schoolName = school.SchoolName.ToUpper();
            string supervisingDeptName = school.SupervisingDept.SupervisingDeptName.ToUpper();
            int year = academicYear.Year;
            int nextYear = year + 1;
            string subjectName = subject.DisplayName.ToUpper();
            string provinceName = province.ProvinceName;
            DateTime reportDate = DateTime.Now;
            string semester = SMASConvert.ConvertSemester(entity.Semester).ToUpper();

            Dictionary<string, object> dicGeneralInfo = new Dictionary<string, object> 
                {
                    {"SchoolName", schoolName}, 
                    {"SupervisingDeptName", supervisingDeptName},
                    {"Year", year},
                    {"NextYear", nextYear},
                    {"SubjectName", subjectName},
                    {"ProvinceName", provinceName},
                    {"ReportDate", reportDate},
                    {"Semester", semester}
                };

            tempSheet.GetRange("A1", "I" + (firstRow - 1)).FillVariableValue(dicGeneralInfo);

            //Tạo sheet Fill dữ liệu
            int lastColumn = Math.Max(lastCol, VTVector.dic['I']);
            IVTWorksheet dataSheet = oBook.CopySheetToLast(tempSheet,
                new VTVector(firstRow + 2, lastColumn).ToString());

            //Tạo vùng cho từng khối
            IVTRange summarizeRange = tempSheet.GetRange(firstRow + 3, 1, firstRow + 3, lastColumn);
            IVTRange topRange = tempSheet.GetRange(firstRow + 4, 1, firstRow + 4, lastColumn);
            IVTRange midRange = tempSheet.GetRange(firstRow + 5, 1, firstRow + 5, lastColumn);
            IVTRange lastRange = tempSheet.GetRange(firstRow + 6, 1, firstRow + 6, lastColumn);

            //fill dữ liệu cho các khối
            List<EducationLevel> listEducationLevel = EducationLevelBusiness.GetByGrade(
                SystemParamsInFile.APPLIED_LEVEL_PRIMARY).OrderBy(x => x.EducationLevelID).ToList();

            IQueryable<VJudgeRecord> listJudgeRecordAll = VJudgeRecordBusiness.SearchBySchool(entity.SchoolID, new Dictionary<string, object>
            {
                {"AcademicYearID", entity.AcademicYearID},
                {"AppliedLevel", SystemParamsInFile.APPLIED_LEVEL_PRIMARY},
                {"Semester", entity.Semester},
                {"JudgeTitle", entity.Semester == SystemParamsInFile.SEMESTER_OF_YEAR_FIRST? 
                    SystemParamsInFile.MARK_TYPE_CK1 : SystemParamsInFile.MARK_TYPE_CK2},
                    {"Year", academicYear.Year}
            });

            List<ClassProfile> listClass = ClassProfileBusiness.SearchBySchool(entity.SchoolID, new Dictionary<string, object>
            {
                {"AcademicYearID", entity.AcademicYearID},
                {"AppliedLevel", SystemParamsInFile.APPLIED_LEVEL_PRIMARY}
            }).ToList<ClassProfile>();

            //Lấy danh sách học sinh
            IQueryable<PupilOfClass> listPupil = PupilOfClassBusiness.SearchBySchool(entity.SchoolID, new Dictionary<string, object> 
            {
                {"AcademicYearID", entity.AcademicYearID}, 
                {"AppliedLevel", SystemParamsInFile.APPLIED_LEVEL_PRIMARY},
                //{"Status", new List<int>{SystemParamsInFile.PUPIL_STATUS_STUDYING, SystemParamsInFile.PUPIL_STATUS_GRADUATED}},
                //{"Check", "Check"}
            }).AddCriteriaSemester(academicYear, entity.Semester);
            IQueryable<PupilOfClassBO> lstPoc = from p in listPupil
                                          join q in PupilProfileBusiness.All on p.PupilID equals q.PupilProfileID
                                          select new PupilOfClassBO{
                                              PupilID = p.PupilID,
                                              ClassID = p.ClassID,
                                              Genre = q.Genre, 
                                              EthnicID = q.EthnicID
                                          };
            //Dân tộc
            Ethnic Ethnic_Kinh = EthnicBusiness.All.Where(o => o.EthnicCode.ToLower().Equals(SystemParamsInFile.ETHNIC_CODE_KINH.ToLower()) && o.IsActive == true).FirstOrDefault();
            Ethnic Ethnic_ForeignPeople = EthnicBusiness.All.Where(o => o.EthnicCode.ToLower().Equals(SystemParamsInFile.ETHNIC_CODE_NN.ToLower()) && o.IsActive == true).FirstOrDefault();
            int? EthnicID_Kinh = 0;
            int? EthnicID_ForeignPeople = 0;
            if (Ethnic_Kinh != null)
            {
                EthnicID_Kinh = Ethnic_Kinh.EthnicID;
            }
            if (Ethnic_ForeignPeople != null)
            {
                EthnicID_ForeignPeople = Ethnic_ForeignPeople.EthnicID;
            }
            var lstPupil = lstPoc.GroupBy(o => o.ClassID).Select(o => new {ClassID = o.Key, TotalPupil = o.Count() }).ToList();
            var lstPupil_Famale = lstPoc.Where(o => o.Genre == SystemParamsInFile.GENRE_FEMALE).GroupBy(o => o.ClassID).Select(o => new { ClassID = o.Key, TotalPupil = o.Count() }).ToList();
            var lstPupil_Ethnic = lstPoc.Where(o => o.EthnicID.HasValue && o.EthnicID != EthnicID_ForeignPeople && o.EthnicID != EthnicID_Kinh).GroupBy(o => o.ClassID).Select(o => new { ClassID = o.Key, TotalPupil = o.Count() }).ToList();
            // Lay danh sach mon hoc cua lop theo nam hoc
            IQueryable<ClassSubject> listClassSubject = ClassSubjectBusiness.SearchBySchool(entity.SchoolID, new Dictionary<string, object> 
            {
                {"AcademicYearID", entity.AcademicYearID}, 
                {"SubjectID", entity.SubjectID},
                {"AppliedLevel", SystemParamsInFile.APPLIED_LEVEL_PRIMARY}
            });
            // Lay hoc sinh thuoc lop dang hoc hoac da tot nghiep
            List<JudgeRecordBO> listJudgeRecord = (from u in listJudgeRecordAll
                                                   join v in ClassProfileBusiness.All on u.ClassID equals v.ClassProfileID
                                                   join r in PupilProfileBusiness.All on u.PupilID equals r.PupilProfileID
                                                   where lstPoc.Any(o => o.PupilID == u.PupilID && o.ClassID == u.ClassID) &&
                                                  listClassSubject.Any(o => o.ClassID == u.ClassID && o.SubjectID == u.SubjectID)
                                                  && v.IsActive.Value
                                                   select new JudgeRecordBO
                                                   {
                                                       PupilID = u.PupilID,
                                                       ClassID = u.ClassID,
                                                       EducationLevelID = v.EducationLevelID,
                                                       Genre = r.Genre,
                                                       EthnicID = r.EthnicID,
                                                       Judgement = u.Judgement
                                                   }).ToList();
            List<JudgeRecordBO> listJudgeRecord_Female = listJudgeRecord.Where(o => o.Genre == SystemParamsInFile.GENRE_FEMALE).ToList();
            List<JudgeRecordBO> listJudgeRecord_Ethnic = listJudgeRecord.Where(o => o.EthnicID.HasValue && o.EthnicID != EthnicID_Kinh && o.EthnicID != EthnicID_ForeignPeople).ToList();
            List<int> listBeginRow = new List<int>();
            int beginRow = firstRow + 3;
            int indexEducation = 0;
            List<JudgeRecordBO> listJudgeRecord_Edu = new List<JudgeRecordBO>();
            foreach (EducationLevel educationLevel in listEducationLevel)
            {
                listBeginRow.Add(beginRow);
                int endRow = beginRow + 1;
                indexEducation++;
                dataSheet.CopyPasteSameSize(summarizeRange, beginRow, 1);
                dataSheet.SetCellValue("A" + beginRow, indexEducation);
                dataSheet.SetCellValue("B" + beginRow, educationLevel.Resolution);
                List<ClassProfile> listCP = listClass.Where(o => o.EducationLevelID == educationLevel.EducationLevelID).ToList();
                listCP = listCP.OrderBy(o => o.OrderNumber.HasValue ? o.OrderNumber : 0).ThenBy(o => o.DisplayName).ToList();
                if (listCP == null || listCP.Count == 0)
                {
                    foreach (StatisticsConfig sc in listSC)
                    {
                        dataSheet.SetCellValue(beginRow, dicStatisticsConfigToColumn[sc], 0);
                    }
                    dataSheet.SetCellValue("C" + beginRow, 0);
                    endRow = beginRow;
                }
                else
                {
                    //Fill dữ liệu dòng tổng
                    endRow = beginRow + listCP.Count();
                    listJudgeRecord_Edu = listJudgeRecord.Where(o => o.EducationLevelID == educationLevel.EducationLevelID).ToList();
                    string fomula = "=SUM(#" + (beginRow + 1) + ":" + "#" + endRow + ")";
                    foreach (StatisticsConfig sc in listSC)
                    {
                        int col = dicStatisticsConfigToColumn[sc];
                        dataSheet.SetFormulaValue(beginRow,
                            col, fomula.Replace("#", VTVector.ColumnIntToString(col)));
                    }
                    dataSheet.SetFormulaValue("C" + beginRow, fomula.Replace("#", "C"));

                    //Tạo khung dữ liệu danh sách các lớp
                    for (int i = beginRow + 1; i <= endRow; i++)
                    {
                        IVTRange copyRange;
                        if (i == beginRow + 1)
                        {
                            copyRange = topRange;
                        }
                        else if (i == endRow)
                        {
                            copyRange = lastRange;
                        }
                        else
                        {
                            copyRange = midRange;
                        }
                        dataSheet.CopyPasteSameSize(copyRange, "A" + i);
                    }

                    //Tạo dữ liệu cần fill
                    List<object> listData = new List<object>();
                    int indexClass = 0;
                    foreach (ClassProfile cp in listCP)
                    {
                        indexClass++;
                        Dictionary<string, object> dicData = new Dictionary<string, object>();
                        dicData["Order"] = indexClass;
                        dicData["ClassName"] = cp.DisplayName;
                        int numberOfPupil = lstPupil.Where(o => o.ClassID == cp.ClassProfileID).Sum(o => o.TotalPupil);
                        dicData["NumberOfPupil"] = numberOfPupil;
                        foreach (StatisticsConfig sc in listSC)
                        {
                            int numberOf = listJudgeRecord_Edu.Where(o => o.ClassID == cp.ClassProfileID && o.Judgement.ToUpper().Equals(sc.EqualValue.ToUpper())).Count();
                            dicData[sc.StatisticsConfigID.ToString()] = numberOf;
                        }
                        listData.Add(dicData);
                    }

                    //Fill dữ liệu
                    dataSheet.GetRange(beginRow, 1, endRow, lastColumn).FillVariableValue(new Dictionary<string, object>
                    {
                        {"list",listData}
                    });
                }

                //Tính vị trí tiếp theo
                beginRow = endRow + 1;
            }

            string formulaSum = "=SUM(";
            for (int i = 0; i < listBeginRow.Count; i++)
            {
                formulaSum += "#" + listBeginRow[i];
                if (i == listBeginRow.Count - 1)
                {
                    formulaSum += ")";
                }
                else
                {
                    formulaSum += ";";
                }
            }

            foreach (StatisticsConfig sc in listSC)
            {
                int col = dicStatisticsConfigToColumn[sc];
                dataSheet.SetFormulaValue(firstRow + 2,
                    col, formulaSum.Replace("#", VTVector.ColumnIntToString(col)));
            }
            dataSheet.SetFormulaValue("C" + (firstRow + 2), formulaSum.Replace("#", "C"));

            dataSheet.CopyPaste(tempSheet.GetRange(firstRow + 8, 1, firstRow + 8, 100), beginRow + 1, 1);

            //Xoá sheet template
            firstSheet.Delete();
            tempSheet.Delete();

            return oBook.ToStream();
        }
        #endregion


    }
}
