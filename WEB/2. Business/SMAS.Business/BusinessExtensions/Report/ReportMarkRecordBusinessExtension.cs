﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Linq.Expressions;
using System.Web;
using log4net;

using SMAS.DAL.Repository;
using SMAS.DAL.IRepository;
using SMAS.Business.IBusiness;
using SMAS.Models.Models;
using SMAS.Business.BusinessObject;
using SMAS.Business.Common;
using System.IO;
using SMAS.VTUtils.Excel.Export;

namespace SMAS.Business.Business
{
    public class ReportMarkRecordBusiness : GenericBussiness<ProcessedReport>, IReportMarkRecordBusiness
    {

        IProcessedReportParameterRepository ProcessedReportParameterRepository;
        IProcessedReportRepository ProcessedReportRepository;
        IEducationLevelRepository EducationLevelRepository;
        IAcademicYearRepository AcademicYearRepository;
        IMarkRecordRepository MarkRecordRepository;
        IPupilProfileRepository PupilProfileRepository;
        ISummedUpRecordRepository SummedUpRecordRepository;

        public ReportMarkRecordBusiness(ILog logger, SMASEntities context = null)
            : base(logger)
        {
            if (context == null) { context = new SMASEntities(); } this.context = context;
            this.ProcessedReportParameterRepository = new ProcessedReportParameterRepository(context);
            this.ProcessedReportRepository = new ProcessedReportRepository(context);
            this.EducationLevelRepository = new EducationLevelRepository(context);
            this.AcademicYearRepository = new AcademicYearRepository(context);
            this.MarkRecordRepository = new MarkRecordRepository(context);
            this.PupilProfileRepository = new PupilProfileRepository(context);
            this.SummedUpRecordRepository = new SummedUpRecordRepository(context);
        }

        #region Tạo mảng băm cho các tham số đầu vào cho báo cáo
        public string GetHashKeyPrimary(ReportMarkRecordSearchBO entity)
        {
            IDictionary<string, object> dic = new Dictionary<string, object> {
                {"AcademicYearID", entity.AcademicYearID},
                {"EducationLevelID ", entity.EducationLevelID},
                {"SchoolID", entity.SchoolID},
                {"Semester", entity.Semester},
                {"SubjectID", entity.SubjectID},
                {"FemaleID",entity.IsCheckedFemale ? 1 : 0},
                {"EthnicID",entity.IsCheckedEthnic ? 1 : 0}
            };
            return ReportUtils.GetHashKey(dic);
        }
        #endregion

        #region Lấy danh sách điểm học kỳ cấp 1 được cập nhật mới nhất
        public ProcessedReport GetReportMarkRecordPrimary(ReportMarkRecordSearchBO entity)
        {
            string reportCode = SystemParamsInFile.REPORT_DS_HS_DIEM_THI_MON_TINH_DIEM_CAP1;
            string inputParameterHashKey = GetHashKeyPrimary(entity);
            return ProcessedReportBusiness.GetProcessedReport(reportCode, inputParameterHashKey);
        }
        #endregion

        #region Lưu báo cáo danh sách điểm học kỳ cấp 1 vào CSDL
        public ProcessedReport InsertReportMarkRecordPrimary(ReportMarkRecordSearchBO entity, Stream data)
        {
            string reportCode = SystemParamsInFile.REPORT_DS_HS_DIEM_THI_MON_TINH_DIEM_CAP1;
            ReportDefinition reportDef = ReportDefinitionBusiness.GetByCode(reportCode);
            ProcessedReport pr = new ProcessedReport();
            pr.ReportCode = reportCode;
            pr.ProcessedDate = DateTime.Now;
            pr.InputParameterHashKey = GetHashKeyPrimary(entity);
            pr.ReportData = ReportUtils.Compress(data);

            //Tạo tên file
            string outputNamePattern = reportDef.OutputNamePattern;
            string subject = SubjectCatBusiness.Find(entity.SubjectID).DisplayName;
            string semester = ReportUtils.ConvertSemesterForReportName(entity.Semester);

            outputNamePattern = outputNamePattern.Replace("[Subject]", subject);
            outputNamePattern = outputNamePattern.Replace("[Semester]", semester);
            pr.ReportName = ReportUtils.RemoveSpecialCharacters(outputNamePattern) + "." + reportDef.OutputFormat;

            IDictionary<string, object> dic = new Dictionary<string, object> {
                {"AcademicYearID", entity.AcademicYearID},
                {"EducationLevelID ", entity.EducationLevelID},
                {"SchoolID", entity.SchoolID},
                {"Semester", entity.Semester},
                {"SubjectID", entity.SubjectID},
                {"FemaleID",entity.IsCheckedFemale ? 1 : 0},
                {"EthnicID",entity.IsCheckedEthnic ? 1 : 0}
            };
            ProcessedReportParameterRepository.Insert(dic, pr);
            ProcessedReportRepository.Insert(pr);
            ProcessedReportRepository.Save();
            return pr;
        }
        #endregion

        #region Tạo file báo cáo danh sách điểm thi học kỳ cấp 1
        public Stream CreateReportMarkRecordPrimary(ReportMarkRecordSearchBO entity)
        {
            SubjectCatBusiness.CheckAvailable(entity.SubjectID, "LearningOutcomePrimaryReport_Label_Subject");
            string reportCode = SystemParamsInFile.REPORT_DS_HS_DIEM_THI_MON_TINH_DIEM_CAP1;
            ReportDefinition reportDef = ReportDefinitionBusiness.GetByCode(reportCode);
            string templatePath = ReportUtils.GetTemplatePath(reportDef);
            IVTWorkbook oBook = VTExport.OpenWorkbook(templatePath);

            int firstRow = 9;
            int defaultNumbeOfColumn = 4;
            int firstDynamicCol = VTVector.dic['D'];
            int defaultLastCol = VTVector.dic['M'];
            //Lấy sheet template
            IVTWorksheet firstSheet = oBook.GetSheet(1);
            IVTRange subjectTemp = firstSheet.GetRange(firstRow, firstDynamicCol, firstRow + 6, firstDynamicCol + 1);

            //Tạo template chuẩn
            IVTWorksheet tempSheet = oBook.CopySheetToLast(firstSheet, "M" + (firstRow - 1));
            tempSheet.CopyPasteSameSize(firstSheet.GetRange(firstRow, 1, firstRow + 6, firstDynamicCol - 1), "A" + firstRow);
            tempSheet.CopyPaste(firstSheet.GetRange(firstRow + 8, 1, firstRow + 8, 100), firstRow + 8, 1);

            //Lấy danh sách các cột động
            List<StatisticsConfig> listSC = StatisticsConfigBusiness.GetList(
                SystemParamsInFile.APPLIED_LEVEL_PRIMARY,
                SystemParamsInFile.StatisticsConfig.REPORT_TYPE_DIEM_THI_HOC_KY,
                SystemParamsInFile.StatisticsConfig.MARK_TYPE_ALL,
                SystemParamsInFile.StatisticsConfig.SUBJECT_TYPE_MARK);


            //Tạo các cột động
            Dictionary<StatisticsConfig, int> dicStatisticsConfigToColumn = new Dictionary<StatisticsConfig, int>();
            for (int i = 0; i < listSC.Count; i++)
            {
                StatisticsConfig sc = listSC[i];
                tempSheet.CopyPasteSameSize(subjectTemp, firstRow, firstDynamicCol + i * 2);
                tempSheet.SetCellValue(firstRow, firstDynamicCol + i * 2, sc.DisplayName);
                dicStatisticsConfigToColumn.Add(sc, firstDynamicCol + i * 2);
            }

            //Copy cột TB trở lên
            tempSheet.CopyPasteSameSize(firstSheet.GetRange(firstRow, firstDynamicCol + 2 * defaultNumbeOfColumn,
                firstRow + 6, firstDynamicCol + 2 * defaultNumbeOfColumn + 1), firstRow, firstDynamicCol + listSC.Count * 2);

            int lastCol = defaultLastCol + (listSC.Count - defaultNumbeOfColumn) * 2;

            //Chỉnh lại dòng khác
            if (listSC.Count > defaultNumbeOfColumn)
            {
                tempSheet.MergeRow(2, VTVector.dic['G'], lastCol);
                tempSheet.MergeRow(3, VTVector.dic['G'], lastCol);
                tempSheet.MergeRow(6, VTVector.dic['A'], lastCol);
                tempSheet.MergeRow(7, VTVector.dic['A'], lastCol);

                double colWidth = firstSheet.GetColumnWidth(firstDynamicCol);
                double resizeWidth = colWidth * defaultNumbeOfColumn / listSC.Count;
                for (int i = firstDynamicCol; i <= lastCol; i++)
                {
                    tempSheet.SetColumnWidth(i, resizeWidth);
                }
            }

            //Tính các công thức 
            string formulaPercent = "=IF(cellNumberOfPupil=0;0;ROUND(cellTotal/cellNumberOfPupil;4)*100)";

            var lstAboveAve = (from sc in listSC
                               where
                                   sc.MarkType == SystemParamsInFile.StatisticsConfig.MARK_TYPE_ABOVE_AVERAGE
                               select VTVector.ColumnIntToString(dicStatisticsConfigToColumn[sc])).ToArray();
            string formulaAverageTotal = "=SUM(" + string.Join("#;", lstAboveAve) + "#)";
            for (int i = 0; i < listSC.Count; i++)
            {
                StatisticsConfig sc = listSC[i];
                tempSheet.SetCellValue(firstRow + 4, dicStatisticsConfigToColumn[sc], "${list[].Get(" + sc.StatisticsConfigID + ")}");
            }

            for (int rowIndex = firstRow + 2; rowIndex < firstRow + 7; rowIndex++)
            {
                for (int i = firstDynamicCol; i <= lastCol; i += 2)
                {
                    string cellNumberOfPupil = "C" + rowIndex;
                    string cellTotal = new VTVector(rowIndex, i).ToString();
                    tempSheet.SetFormulaValue(rowIndex, i + 1, formulaPercent.
                        Replace("cellTotal", cellTotal).Replace("cellNumberOfPupil", cellNumberOfPupil));
                }

                tempSheet.SetFormulaValue(rowIndex, lastCol - 1, formulaAverageTotal.Replace("#", rowIndex.ToString()));
            }

            //Fill dữ liệu chung cho template
            AcademicYear academicYear = AcademicYearBusiness.Find(entity.AcademicYearID);
            SchoolProfile school = SchoolProfileBusiness.Find(entity.SchoolID);
            SubjectCat subject = SubjectCatBusiness.Find(entity.SubjectID);
            Province province = school.Province;

            string schoolName = school.SchoolName.ToUpper();
            string supervisingDeptName = school.SupervisingDept.SupervisingDeptName.ToUpper();
            string academicYearTitle = academicYear.DisplayTitle;
            string subjectName = subject.DisplayName.ToUpper();
            string provinceName = province.ProvinceName;
            DateTime reportDate = DateTime.Now;
            string semester = SMASConvert.ConvertSemester(entity.Semester).ToUpper();

            Dictionary<string, object> dicGeneralInfo = new Dictionary<string, object> 
                {
                    {"SchoolName", schoolName}, 
                    {"SupervisingDeptName", supervisingDeptName},
                    {"ProvinceName", provinceName},
                    {"ReportDate", reportDate}
                };

            tempSheet.GetRange(1, 1, firstRow - 1, defaultLastCol).FillVariableValue(dicGeneralInfo);

            //fill dữ liệu cho các khối
            List<EducationLevel> listEducationLevel = EducationLevelBusiness.GetByGrade(
                SystemParamsInFile.APPLIED_LEVEL_PRIMARY).OrderBy(x => x.EducationLevelID).ToList();

            IQueryable<VMarkRecord> listMarkRecordAll = VMarkRecordBusiness.SearchBySchool(entity.SchoolID, new Dictionary<string, object>
            {
                {"AcademicYearID", entity.AcademicYearID},
                {"AppliedLevel", SystemParamsInFile.APPLIED_LEVEL_PRIMARY},
                {"Semester", entity.Semester},
                {"SubjectID", entity.SubjectID},
                {"MarkTitle", entity.Semester == SystemParamsInFile.SEMESTER_OF_YEAR_FIRST? 
                    SystemParamsInFile.MARK_TYPE_CK1 : SystemParamsInFile.MARK_TYPE_CK2},
                {"Year", academicYear.Year}

            });
            List<ClassProfile> listClass = ClassProfileBusiness.SearchBySchool(entity.SchoolID, new Dictionary<string, object>
            {
                {"AcademicYearID", entity.AcademicYearID},
                {"AppliedLevel", SystemParamsInFile.APPLIED_LEVEL_PRIMARY}
            }).ToList();

            //Lấy danh sách học sinh
            Dictionary<string, object> dicPupilRanking = new Dictionary<string, object> 
            {
                {"AcademicYearID", entity.AcademicYearID},
                {"SchoolID", entity.SchoolID},
                //{"Semester", entity.Semester},
                {"AppliedLevel",  SystemParamsInFile.APPLIED_LEVEL_PRIMARY}
            };

            IQueryable<PupilOfClass> listPupil_Temp  = PupilOfClassBusiness.SearchBySchool(entity.SchoolID, dicPupilRanking).AddCriteriaSemester(academicYear,entity.Semester);
            List<PupilOfClassBO> listPupil = (from p in listPupil_Temp
                                             join q in ClassProfileBusiness.All on p.ClassID equals q.ClassProfileID
                                             join r in PupilProfileBusiness.All on p.PupilID equals r.PupilProfileID
                                             where q.IsActive.Value
                                             select new PupilOfClassBO{
                                                 PupilID = p.PupilID,
                                                 ClassID = p.ClassID, 
                                                 EducationLevelID = q.EducationLevelID,
                                                 Genre = r.Genre,
                                                 EthnicID = r.EthnicID
                                             }).ToList();
            
             //Dân tộc
                Ethnic Ethnic_Kinh = EthnicBusiness.All.Where(o => o.EthnicCode.ToLower().Equals(SystemParamsInFile.ETHNIC_CODE_KINH.ToLower()) && o.IsActive == true).FirstOrDefault();
                Ethnic Ethnic_ForeignPeople = EthnicBusiness.All.Where(o => o.EthnicCode.ToLower().Equals(SystemParamsInFile.ETHNIC_CODE_NN.ToLower()) && o.IsActive == true).FirstOrDefault();
                int? EthnicID_Kinh = 0;
                int? EthnicID_ForeignPeople = 0;
                if (Ethnic_Kinh != null)
                {
                    EthnicID_Kinh = Ethnic_Kinh.EthnicID;
                }
                if (Ethnic_ForeignPeople != null)
                {
                    EthnicID_ForeignPeople = Ethnic_ForeignPeople.EthnicID;
                }
                var lstPupil = listPupil.GroupBy(o => o.ClassID).Select(o => new { ClassID = o.Key, TotalPupil = o.Count() }).ToList();
               var lstPupil_Female = listPupil.Where(o => o.Genre == SystemParamsInFile.GENRE_FEMALE).GroupBy(o => o.ClassID).Select(o => new {ClassID = o.Key, TotalPupil = o.Count()}).ToList();
               var lstPupil_Ethnic = listPupil.Where(o => o.EthnicID.HasValue && o.EthnicID != EthnicID_ForeignPeople && o.EthnicID != EthnicID_Kinh).GroupBy(o => o.ClassID).Select(o => new {ClassID = o.Key, TotalPupil = o.Count()}).ToList();

            // Lay danh sach mon hoc cua lop theo nam hoc
            IQueryable<ClassSubject> listClassSubject = ClassSubjectBusiness.SearchBySchool(entity.SchoolID, new Dictionary<string, object> 
            {
                {"AcademicYearID", entity.AcademicYearID}, 
                {"SubjectID", entity.SubjectID},
                {"AppliedLevel", SystemParamsInFile.APPLIED_LEVEL_PRIMARY}
            });

            // Lay hoc sinh thuoc lop dang hoc hoac da tot nghiep
            List<MarkRecordBO> listMarkRecord = (from u in listMarkRecordAll
                                                 join v in PupilProfileBusiness.All on u.PupilID equals v.PupilProfileID
                                                 join r in ClassProfileBusiness.All on u.ClassID equals r.ClassProfileID
                                                 where
                                           listPupil_Temp.Any(o => o.ClassID == u.ClassID && o.PupilID == u.PupilID)
                                           && listClassSubject.Any(o => o.ClassID == u.ClassID && o.SubjectID == u.SubjectID)
                                           && r.IsActive.Value
                                                 select new MarkRecordBO
                                                 {
                                                     MarkRecordID = u.MarkRecordID,
                                                     PupilID = u.PupilID,
                                                     ClassID = u.ClassID,
                                                     EducationLevelID = r.EducationLevelID,
                                                     Genre = v.Genre,
                                                     EthnicID = v.EthnicID,
                                                     Mark = u.Mark
                                                 }).ToList();
            List<MarkRecordBO> listMarkRecord_Female = listMarkRecord.Where(o => o.Genre == SystemParamsInFile.GENRE_FEMALE).ToList();
            List<MarkRecordBO> listMarkRecord_Ethnic = listMarkRecord.Where(o => o.EthnicID.HasValue && o.EthnicID != EthnicID_ForeignPeople && o.EthnicID != EthnicID_Kinh).ToList();

            #region Thống kê điểm thi học kỳ theo môn
            List<int> listBeginRow = new List<int>();
            int beginRow = firstRow + 3;
            int indexEducation = 0;
            int lastRow = 0;
            List<MarkRecordBO> listMarkRecord_Edu = new List<MarkRecordBO>();
            //Tạo sheet Fill dữ liệu
            int lastColumn = Math.Max(lastCol, defaultLastCol);
            IVTWorksheet dataSheet = oBook.CopySheetToLast(tempSheet,
                new VTVector(firstRow + 2, lastColumn).ToString());
            dataSheet.Name = "Thi HK";
            string str = "MÔN " + subjectName + " - " + semester.ToUpper() + " - NĂM HỌC " + academicYearTitle;
            if (entity.Semester == SystemParamsInFile.SEMESTER_OF_YEAR_SECOND)
            {
                str = "MÔN " + subject.DisplayName.ToUpper() + " - HỌC KỲ II " + " - NĂM HỌC " + academicYearTitle;
            }
            dataSheet.SetCellValue("A7", str);
            dataSheet.SetCellValue("A6", "THỐNG KÊ ĐIỂM THI HỌC KỲ");
            //Tạo vùng cho từng khối
            IVTRange summarizeRange = tempSheet.GetRange(firstRow + 3, 1, firstRow + 3, lastColumn);
            IVTRange topRange = tempSheet.GetRange(firstRow + 4, 1, firstRow + 4, lastColumn);
            IVTRange midRange = tempSheet.GetRange(firstRow + 5, 1, firstRow + 5, lastColumn);
            IVTRange lastRange = tempSheet.GetRange(firstRow + 6, 1, firstRow + 6, lastColumn);
            foreach (EducationLevel educationLevel in listEducationLevel)
            {
                listBeginRow.Add(beginRow);
                int endRow = beginRow + 1;
                indexEducation++;
                dataSheet.CopyPasteSameSize(summarizeRange, beginRow, 1);
                dataSheet.SetCellValue("A" + beginRow, indexEducation);
                dataSheet.SetCellValue("B" + beginRow, educationLevel.Resolution);

                List<ClassProfile> listCP = listClass.Where(o => o.EducationLevelID == educationLevel.EducationLevelID).OrderBy(o => o.OrderNumber.HasValue ? o.OrderNumber : 0).ThenBy(o => o.DisplayName).ToList();
                if (listCP == null || listCP.Count() == 0)
                {
                    foreach (StatisticsConfig sc in listSC)
                    {
                        dataSheet.SetCellValue(beginRow, dicStatisticsConfigToColumn[sc], 0);
                    }
                    dataSheet.SetCellValue("C" + beginRow, 0);
                    endRow = beginRow;
                }
                else
                {
                    //Fill dữ liệu dòng tổng
                    endRow = beginRow + listCP.Count();
                    string fomula = "=SUM(#" + (beginRow + 1) + ":" + "#" + endRow + ")";
                    foreach (StatisticsConfig sc in listSC)
                    {
                        int col = dicStatisticsConfigToColumn[sc];
                        dataSheet.SetFormulaValue(beginRow,
                            col, fomula.Replace("#", VTVector.ColumnIntToString(col)));
                    }
                    dataSheet.SetFormulaValue("C" + beginRow, fomula.Replace("#", "C"));

                    //Tạo khung dữ liệu danh sách các lớp
                    for (int i = beginRow + 1; i <= endRow; i++)
                    {
                        IVTRange copyRange;
                        if (i == beginRow + 1)
                        {
                            copyRange = topRange;
                        }
                        else if (i == endRow)
                        {
                            copyRange = lastRange;
                        }
                        else
                        {
                            copyRange = midRange;
                        }
                        dataSheet.CopyPasteSameSize(copyRange, "A" + i);
                    }

                    //Tạo dữ liệu cần fill
                    List<object> listData = new List<object>();
                    int indexClass = 0;
                    List<ClassProfile> lstClass = listCP.ToList();
                    listMarkRecord_Edu = listMarkRecord.Where(o => o.EducationLevelID == educationLevel.EducationLevelID).ToList();
                    foreach (ClassProfile cp in lstClass)
                    {
                        indexClass++;
                        Dictionary<string, object> dicData = new Dictionary<string, object>();
                        dicData["Order"] = indexClass;
                        dicData["ClassName"] = cp.DisplayName;
                        int numberOfPupil = lstPupil.Where(o => o.ClassID == cp.ClassProfileID).Sum(o => o.TotalPupil);
                        dicData["NumberOfPupil"] = numberOfPupil;

                        foreach (StatisticsConfig sc in listSC)
                        {
                            int numberOf = (from mr in listMarkRecord_Edu
                                            where mr.ClassID == cp.ClassProfileID
                                            && mr.Mark < sc.MaxValue
                                            && mr.Mark >= sc.MinValue
                                            select mr.MarkRecordID).Count();
                            dicData[sc.StatisticsConfigID.ToString()] = numberOf;
                        }
                        listData.Add(dicData);
                    }

                    //Fill dữ liệu
                    dataSheet.GetRange(beginRow, 1, endRow, lastColumn).FillVariableValue(new Dictionary<string, object>
                    {
                        {"list",listData}
                    });
                }

                //Tính vị trí tiếp theo
                beginRow = endRow + 1;
                lastRow = endRow;
            }

            string formulaSum = "=SUM(#" + string.Join(";#", listBeginRow.ToArray<int>()) + ")";

            foreach (StatisticsConfig sc in listSC)
            {
                int col = dicStatisticsConfigToColumn[sc];
                dataSheet.SetFormulaValue(firstRow + 2,
                    col, formulaSum.Replace("#", VTVector.ColumnIntToString(col)));
            }
            dataSheet.SetFormulaValue("C" + (firstRow + 2), formulaSum.Replace("#", "C"));

            dataSheet.CopyPaste(tempSheet.GetRange(firstRow + 8, 1, firstRow + 8, 100), beginRow + 1, 1);
            
            dataSheet.CopyPaste(tempSheet.GetRange(15, 1, 15, 13), lastRow, 1,true);
            dataSheet.FitAllColumnsOnOnePage = true;
         
            #endregion
            #region Thống kê điểm thi học kỳ theo môn học sinh nữ
            listBeginRow = new List<int>();
            beginRow = firstRow + 3;
            indexEducation = 0;
            lastRow = 0;
            listMarkRecord_Edu = new List<MarkRecordBO>();
            //Tạo sheet Fill dữ liệu
            lastColumn = Math.Max(lastCol, defaultLastCol);
            IVTWorksheet dataSheet_Female = oBook.CopySheetToLast(tempSheet,
                new VTVector(firstRow + 2, lastColumn).ToString());
            dataSheet_Female.Name = "HS_Nu";
            dataSheet_Female.SetCellValue("A6", "THỐNG KÊ ĐIỂM THI HỌC KỲ HỌC SINH NỮ");
            dataSheet_Female.SetCellValue("A7", str);

            //Tạo vùng cho từng khối
            foreach (EducationLevel educationLevel in listEducationLevel)
            {
                listBeginRow.Add(beginRow);
                int endRow = beginRow + 1;
                indexEducation++;
                dataSheet_Female.CopyPasteSameSize(summarizeRange, beginRow, 1);
                dataSheet_Female.SetCellValue("A" + beginRow, indexEducation);
                dataSheet_Female.SetCellValue("B" + beginRow, educationLevel.Resolution);

                List<ClassProfile> listCP = listClass.Where(o => o.EducationLevelID == educationLevel.EducationLevelID).OrderBy(o => o.OrderNumber.HasValue ? o.OrderNumber : 0).ThenBy(o => o.DisplayName).ToList();
                if (listCP == null || listCP.Count() == 0)
                {
                    foreach (StatisticsConfig sc in listSC)
                    {
                        dataSheet_Female.SetCellValue(beginRow, dicStatisticsConfigToColumn[sc], 0);
                    }
                    dataSheet_Female.SetCellValue("C" + beginRow, 0);
                    endRow = beginRow;
                }
                else
                {
                    //Fill dữ liệu dòng tổng
                    endRow = beginRow + listCP.Count();
                    string fomula = "=SUM(#" + (beginRow + 1) + ":" + "#" + endRow + ")";
                    foreach (StatisticsConfig sc in listSC)
                    {
                        int col = dicStatisticsConfigToColumn[sc];
                        dataSheet_Female.SetFormulaValue(beginRow,
                            col, fomula.Replace("#", VTVector.ColumnIntToString(col)));
                    }
                    dataSheet_Female.SetFormulaValue("C" + beginRow, fomula.Replace("#", "C"));

                    //Tạo khung dữ liệu danh sách các lớp
                    for (int i = beginRow + 1; i <= endRow; i++)
                    {
                        IVTRange copyRange;
                        if (i == beginRow + 1)
                        {
                            copyRange = topRange;
                        }
                        else if (i == endRow)
                        {
                            copyRange = lastRange;
                        }
                        else
                        {
                            copyRange = midRange;
                        }
                        dataSheet_Female.CopyPasteSameSize(copyRange, "A" + i);
                    }

                    //Tạo dữ liệu cần fill
                    List<object> listData = new List<object>();
                    int indexClass = 0;
                    List<ClassProfile> lstClass = listCP.ToList();
                    listMarkRecord_Edu = listMarkRecord_Female.Where(o => o.EducationLevelID == educationLevel.EducationLevelID).ToList();
                    foreach (ClassProfile cp in lstClass)
                    {
                        indexClass++;
                        Dictionary<string, object> dicData = new Dictionary<string, object>();
                        dicData["Order"] = indexClass;
                        dicData["ClassName"] = cp.DisplayName;
                        int numberOfPupil = lstPupil_Female.Where(o => o.ClassID == cp.ClassProfileID).Sum(o => o.TotalPupil);
                        dicData["NumberOfPupil"] = numberOfPupil;

                        foreach (StatisticsConfig sc in listSC)
                        {
                            int numberOf = (from mr in listMarkRecord_Edu
                                            where mr.ClassID == cp.ClassProfileID
                                            && mr.Mark < sc.MaxValue
                                            && mr.Mark >= sc.MinValue
                                            select mr.MarkRecordID).Count();
                            dicData[sc.StatisticsConfigID.ToString()] = numberOf;
                        }
                        listData.Add(dicData);
                    }

                    //Fill dữ liệu
                    dataSheet_Female.GetRange(beginRow, 1, endRow, lastColumn).FillVariableValue(new Dictionary<string, object>
                    {
                        {"list",listData}
                    });
                }

                //Tính vị trí tiếp theo
                beginRow = endRow + 1;
                lastRow = endRow;
            }

            formulaSum = "=SUM(#" + string.Join(";#", listBeginRow.ToArray<int>()) + ")";

            foreach (StatisticsConfig sc in listSC)
            {
                int col = dicStatisticsConfigToColumn[sc];
                dataSheet_Female.SetFormulaValue(firstRow + 2,
                    col, formulaSum.Replace("#", VTVector.ColumnIntToString(col)));
            }
            dataSheet_Female.SetFormulaValue("C" + (firstRow + 2), formulaSum.Replace("#", "C"));

            dataSheet_Female.CopyPaste(tempSheet.GetRange(firstRow + 8, 1, firstRow + 8, 100), beginRow + 1, 1);

            dataSheet_Female.CopyPaste(tempSheet.GetRange(15, 1, 15, 13), lastRow, 1, true);
            dataSheet_Female.FitAllColumnsOnOnePage = true;
            #endregion
            #region Thống kê điểm thi học kỳ theo môn học sinh dân tộc
            listBeginRow = new List<int>();
            beginRow = firstRow + 3;
            indexEducation = 0;
            lastRow = 0;
            listMarkRecord_Edu = new List<MarkRecordBO>();
            //Tạo sheet Fill dữ liệu
            lastColumn = Math.Max(lastCol, defaultLastCol);
            IVTWorksheet dataSheet_Ethnic = oBook.CopySheetToLast(tempSheet,
                new VTVector(firstRow + 2, lastColumn).ToString());
            dataSheet_Ethnic.Name = "HS_DT";
            dataSheet_Ethnic.SetCellValue("A6", "THỐNG KÊ ĐIỂM THI HỌC KỲ HỌC SINH DÂN TỘC");
            dataSheet_Ethnic.SetCellValue("A7", str);

            //Tạo vùng cho từng khối
            foreach (EducationLevel educationLevel in listEducationLevel)
            {
                listBeginRow.Add(beginRow);
                int endRow = beginRow + 1;
                indexEducation++;
                dataSheet_Ethnic.CopyPasteSameSize(summarizeRange, beginRow, 1);
                dataSheet_Ethnic.SetCellValue("A" + beginRow, indexEducation);
                dataSheet_Ethnic.SetCellValue("B" + beginRow, educationLevel.Resolution);

                List<ClassProfile> listCP = listClass.Where(o => o.EducationLevelID == educationLevel.EducationLevelID).OrderBy(o => o.OrderNumber.HasValue ? o.OrderNumber : 0).ThenBy(o => o.DisplayName).ToList();
                if (listCP == null || listCP.Count() == 0)
                {
                    foreach (StatisticsConfig sc in listSC)
                    {
                        dataSheet_Ethnic.SetCellValue(beginRow, dicStatisticsConfigToColumn[sc], 0);
                    }
                    dataSheet_Ethnic.SetCellValue("C" + beginRow, 0);
                    endRow = beginRow;
                }
                else
                {
                    //Fill dữ liệu dòng tổng
                    endRow = beginRow + listCP.Count();
                    string fomula = "=SUM(#" + (beginRow + 1) + ":" + "#" + endRow + ")";
                    foreach (StatisticsConfig sc in listSC)
                    {
                        int col = dicStatisticsConfigToColumn[sc];
                        dataSheet_Ethnic.SetFormulaValue(beginRow,
                            col, fomula.Replace("#", VTVector.ColumnIntToString(col)));
                    }
                    dataSheet_Ethnic.SetFormulaValue("C" + beginRow, fomula.Replace("#", "C"));

                    //Tạo khung dữ liệu danh sách các lớp
                    for (int i = beginRow + 1; i <= endRow; i++)
                    {
                        IVTRange copyRange;
                        if (i == beginRow + 1)
                        {
                            copyRange = topRange;
                        }
                        else if (i == endRow)
                        {
                            copyRange = lastRange;
                        }
                        else
                        {
                            copyRange = midRange;
                        }
                        dataSheet_Ethnic.CopyPasteSameSize(copyRange, "A" + i);
                    }

                    //Tạo dữ liệu cần fill
                    List<object> listData = new List<object>();
                    int indexClass = 0;
                    List<ClassProfile> lstClass = listCP.ToList();
                    listMarkRecord_Edu = listMarkRecord_Ethnic.Where(o => o.EducationLevelID == educationLevel.EducationLevelID).ToList();
                    foreach (ClassProfile cp in lstClass)
                    {
                        indexClass++;
                        Dictionary<string, object> dicData = new Dictionary<string, object>();
                        dicData["Order"] = indexClass;
                        dicData["ClassName"] = cp.DisplayName;

                        int numberOfPupil = lstPupil_Ethnic.Where(o => o.ClassID == cp.ClassProfileID).Sum(o => o.TotalPupil);
                        dicData["NumberOfPupil"] = numberOfPupil;

                        foreach (StatisticsConfig sc in listSC)
                        {
                            int numberOf = (from mr in listMarkRecord_Edu
                                            where mr.ClassID == cp.ClassProfileID
                                            && mr.Mark < sc.MaxValue
                                            && mr.Mark >= sc.MinValue
                                            select mr.MarkRecordID).Count();
                            dicData[sc.StatisticsConfigID.ToString()] = numberOf;
                        }
                        listData.Add(dicData);
                    }

                    //Fill dữ liệu
                    dataSheet_Ethnic.GetRange(beginRow, 1, endRow, lastColumn).FillVariableValue(new Dictionary<string, object>
                    {
                        {"list",listData}
                    });
                }

                //Tính vị trí tiếp theo
                beginRow = endRow + 1;
                lastRow = endRow;
            }

            formulaSum = "=SUM(#" + string.Join(";#", listBeginRow.ToArray<int>()) + ")";

            foreach (StatisticsConfig sc in listSC)
            {
                int col = dicStatisticsConfigToColumn[sc];
                dataSheet_Ethnic.SetFormulaValue(firstRow + 2,
                    col, formulaSum.Replace("#", VTVector.ColumnIntToString(col)));
            }
            dataSheet_Ethnic.SetFormulaValue("C" + (firstRow + 2), formulaSum.Replace("#", "C"));

            dataSheet_Ethnic.CopyPaste(tempSheet.GetRange(firstRow + 8, 1, firstRow + 8, 100), beginRow + 1, 1);

            dataSheet_Ethnic.CopyPaste(tempSheet.GetRange(15, 1, 15, 13), lastRow, 1, true);
            dataSheet_Ethnic.FitAllColumnsOnOnePage = true;
            #endregion
            //Xoá sheet template
            firstSheet.Delete();
            tempSheet.Delete();
            return oBook.ToStream();
        }

        public Stream CreateReportMarkRecordPrimaryTT30(ReportMarkRecordSearchBO entity, bool isMoveHistory)
        {
            SubjectCatBusiness.CheckAvailable(entity.SubjectID, "LearningOutcomePrimaryReport_Label_Subject");
            string reportCode = SystemParamsInFile.REPORT_DS_HS_DIEM_THI_MON_TINH_DIEM_CAP1;
            ReportDefinition reportDef = ReportDefinitionBusiness.GetByCode(reportCode);
            string templatePath = ReportUtils.GetTemplatePath(reportDef);
            IVTWorkbook oBook = VTExport.OpenWorkbook(templatePath);

            int firstRow = 9;
            int defaultNumbeOfColumn = 4;
            int firstDynamicCol = VTVector.dic['D'];
            int defaultLastCol = VTVector.dic['M'];
            //Lấy sheet template
            IVTWorksheet firstSheet = oBook.GetSheet(1);
            IVTRange subjectTemp = firstSheet.GetRange(firstRow, firstDynamicCol, firstRow + 6, firstDynamicCol + 1);

            //Tạo template chuẩn
            IVTWorksheet tempSheet = oBook.CopySheetToLast(firstSheet, "M" + (firstRow - 1));
            tempSheet.CopyPasteSameSize(firstSheet.GetRange(firstRow, 1, firstRow + 6, firstDynamicCol - 1), "A" + firstRow);
            tempSheet.CopyPaste(firstSheet.GetRange(firstRow + 8, 1, firstRow + 8, 100), firstRow + 8, 1);

            //Lấy danh sách các cột động
            List<StatisticsConfig> listSC = StatisticsConfigBusiness.GetList(
                SystemParamsInFile.APPLIED_LEVEL_PRIMARY,
                SystemParamsInFile.StatisticsConfig.REPORT_TYPE_DIEM_THI_HOC_KY,
                SystemParamsInFile.StatisticsConfig.MARK_TYPE_ALL,
                SystemParamsInFile.StatisticsConfig.SUBJECT_TYPE_MARK);


            //Tạo các cột động
            Dictionary<StatisticsConfig, int> dicStatisticsConfigToColumn = new Dictionary<StatisticsConfig, int>();
            for (int i = 0; i < listSC.Count; i++)
            {
                StatisticsConfig sc = listSC[i];
                tempSheet.CopyPasteSameSize(subjectTemp, firstRow, firstDynamicCol + i * 2);
                tempSheet.SetCellValue(firstRow, firstDynamicCol + i * 2, sc.DisplayName);
                dicStatisticsConfigToColumn.Add(sc, firstDynamicCol + i * 2);
            }

            //Copy cột TB trở lên
            tempSheet.CopyPasteSameSize(firstSheet.GetRange(firstRow, firstDynamicCol + 2 * defaultNumbeOfColumn,
                firstRow + 6, firstDynamicCol + 2 * defaultNumbeOfColumn + 1), firstRow, firstDynamicCol + listSC.Count * 2);

            int lastCol = defaultLastCol + (listSC.Count - defaultNumbeOfColumn) * 2;

            //Chỉnh lại dòng khác
            if (listSC.Count > defaultNumbeOfColumn)
            {
                tempSheet.MergeRow(2, VTVector.dic['G'], lastCol);
                tempSheet.MergeRow(3, VTVector.dic['G'], lastCol);
                tempSheet.MergeRow(6, VTVector.dic['A'], lastCol);
                tempSheet.MergeRow(7, VTVector.dic['A'], lastCol);

                double colWidth = firstSheet.GetColumnWidth(firstDynamicCol);
                double resizeWidth = colWidth * defaultNumbeOfColumn / listSC.Count;
                for (int i = firstDynamicCol; i <= lastCol; i++)
                {
                    tempSheet.SetColumnWidth(i, resizeWidth);
                }
            }

            //Tính các công thức 
            string formulaPercent = "=IF(cellNumberOfPupil=0;0;ROUND(cellTotal/cellNumberOfPupil;4)*100)";

            var lstAboveAve = (from sc in listSC
                               where
                                   sc.MarkType == SystemParamsInFile.StatisticsConfig.MARK_TYPE_ABOVE_AVERAGE
                               select VTVector.ColumnIntToString(dicStatisticsConfigToColumn[sc])).ToArray();
            string formulaAverageTotal = "=SUM(" + string.Join("#;", lstAboveAve) + "#)";
            for (int i = 0; i < listSC.Count; i++)
            {
                StatisticsConfig sc = listSC[i];
                tempSheet.SetCellValue(firstRow + 4, dicStatisticsConfigToColumn[sc], "${list[].Get(" + sc.StatisticsConfigID + ")}");
            }

            for (int rowIndex = firstRow + 2; rowIndex < firstRow + 7; rowIndex++)
            {
                for (int i = firstDynamicCol; i <= lastCol; i += 2)
                {
                    string cellNumberOfPupil = "C" + rowIndex;
                    string cellTotal = new VTVector(rowIndex, i).ToString();
                    tempSheet.SetFormulaValue(rowIndex, i + 1, formulaPercent.
                        Replace("cellTotal", cellTotal).Replace("cellNumberOfPupil", cellNumberOfPupil));
                }

                tempSheet.SetFormulaValue(rowIndex, lastCol - 1, formulaAverageTotal.Replace("#", rowIndex.ToString()));
            }

            //Fill dữ liệu chung cho template
            AcademicYear academicYear = AcademicYearBusiness.Find(entity.AcademicYearID);
            SchoolProfile school = SchoolProfileBusiness.Find(entity.SchoolID);
            SubjectCat subject = SubjectCatBusiness.Find(entity.SubjectID);
            Province province = school.Province;

            string schoolName = school.SchoolName.ToUpper();
            string supervisingDeptName = UtilsBusiness.GetSupervisingDeptName(school.SchoolProfileID, entity.AppliedLevel).ToUpper();
            string academicYearTitle = academicYear.DisplayTitle;
            string subjectName = subject.DisplayName.ToUpper();
            string provinceName = (school.District != null ? school.District.DistrictName : "");
            DateTime reportDate = DateTime.Now;
            string semester = SMASConvert.ConvertSemester(entity.Semester).ToUpper();
            int partionID = UtilsBusiness.GetPartionId(entity.SchoolID);

            Dictionary<string, object> dicGeneralInfo = new Dictionary<string, object> 
                {
                    {"SchoolName", schoolName}, 
                    {"SupervisingDeptName", supervisingDeptName},
                    {"ProvinceName", provinceName},
                    {"ReportDate", reportDate}
                };

            tempSheet.GetRange(1, 1, firstRow - 1, defaultLastCol).FillVariableValue(dicGeneralInfo);

            //fill dữ liệu cho các khối
            List<EducationLevel> listEducationLevel = EducationLevelBusiness.GetByGrade(
                SystemParamsInFile.APPLIED_LEVEL_PRIMARY).OrderBy(x => x.EducationLevelID).ToList();

            List<SummedEvaluationBO> listSummedEvaluation = new List<SummedEvaluationBO>();
            if (isMoveHistory)
            {
                listSummedEvaluation = (from sm in SummedEvaluationHistoryBusiness.All
                                        where sm.LastDigitSchoolID == partionID
                                        && sm.AcademicYearID == entity.AcademicYearID
                                        && sm.SchoolID == entity.SchoolID
                                        && sm.SemesterID == entity.Semester
                                        && sm.EvaluationCriteriaID == entity.SubjectID
                                        select new SummedEvaluationBO
                                        {
                                            SummedEvaluationID = sm.SummedEvaluationID,
                                            PupilID = sm.PupilID,
                                            Mark = sm.PeriodicEndingMark,
                                            SubjectID = sm.EvaluationCriteriaID,
                                            ClassID = sm.ClassID
                                        }).ToList();

            }
            else
            {
                listSummedEvaluation = (from sm in SummedEvaluationBusiness.All
                                        where sm.LastDigitSchoolID == partionID
                                        && sm.AcademicYearID == entity.AcademicYearID
                                        && sm.SchoolID == entity.SchoolID
                                        && sm.SemesterID == entity.Semester
                                        && sm.EvaluationCriteriaID == entity.SubjectID
                                        select new SummedEvaluationBO
                                        {
                                            SummedEvaluationID = sm.SummedEvaluationID,
                                            PupilID = sm.PupilID,
                                            Mark = sm.PeriodicEndingMark,
                                            SubjectID = sm.EvaluationCriteriaID,
                                            ClassID = sm.ClassID
                                        }).ToList();
            }
            List<ClassProfile> listClass = ClassProfileBusiness.SearchBySchool(entity.SchoolID, new Dictionary<string, object>
            {
                {"AcademicYearID", entity.AcademicYearID},
                {"AppliedLevel", SystemParamsInFile.APPLIED_LEVEL_PRIMARY}
            }).ToList();

            //Lấy danh sách học sinh
            Dictionary<string, object> dicPupilRanking = new Dictionary<string, object> 
            {
                {"AcademicYearID", entity.AcademicYearID},
                {"SchoolID", entity.SchoolID},
                //{"Semester", entity.Semester},
                {"AppliedLevel",  SystemParamsInFile.APPLIED_LEVEL_PRIMARY}
            };

            IQueryable<PupilOfClass> listPupil_Temp = PupilOfClassBusiness.SearchBySchool(entity.SchoolID, dicPupilRanking).AddCriteriaSemester(academicYear, entity.Semester);
            List<PupilOfClassBO> listPupil = (from p in listPupil_Temp
                                              join q in ClassProfileBusiness.All on p.ClassID equals q.ClassProfileID
                                              join r in PupilProfileBusiness.All on p.PupilID equals r.PupilProfileID
                                              where q.IsActive.Value
                                              select new PupilOfClassBO
                                              {
                                                  PupilID = p.PupilID,
                                                  ClassID = p.ClassID,
                                                  EducationLevelID = q.EducationLevelID,
                                                  Genre = r.Genre,
                                                  EthnicID = r.EthnicID
                                              }).ToList();

            //Dân tộc
            Ethnic Ethnic_Kinh = EthnicBusiness.All.Where(o => o.EthnicCode.ToLower().Equals(SystemParamsInFile.ETHNIC_CODE_KINH.ToLower()) && o.IsActive == true).FirstOrDefault();
            Ethnic Ethnic_ForeignPeople = EthnicBusiness.All.Where(o => o.EthnicCode.ToLower().Equals(SystemParamsInFile.ETHNIC_CODE_NN.ToLower()) && o.IsActive == true).FirstOrDefault();
            int? EthnicID_Kinh = 0;
            int? EthnicID_ForeignPeople = 0;
            if (Ethnic_Kinh != null)
            {
                EthnicID_Kinh = Ethnic_Kinh.EthnicID;
            }
            if (Ethnic_ForeignPeople != null)
            {
                EthnicID_ForeignPeople = Ethnic_ForeignPeople.EthnicID;
            }
            var lstPupil = listPupil.GroupBy(o => o.ClassID).Select(o => new { ClassID = o.Key, TotalPupil = o.Count() }).ToList();
            var lstPupil_Female = listPupil.Where(o => o.Genre == SystemParamsInFile.GENRE_FEMALE).GroupBy(o => o.ClassID).Select(o => new { ClassID = o.Key, TotalPupil = o.Count() }).ToList();
            var lstPupil_Ethnic = listPupil.Where(o => o.EthnicID.HasValue && o.EthnicID != EthnicID_ForeignPeople && o.EthnicID != EthnicID_Kinh).GroupBy(o => o.ClassID).Select(o => new { ClassID = o.Key, TotalPupil = o.Count() }).ToList();
            var lstPupil_FemaleEthnic = listPupil.Where(o => o.Genre == SystemParamsInFile.GENRE_FEMALE && o.EthnicID.HasValue && o.EthnicID != EthnicID_ForeignPeople && o.EthnicID != EthnicID_Kinh).GroupBy(o => o.ClassID).Select(o => new { ClassID = o.Key, TotalPupil = o.Count() }).ToList();

            // Lay danh sach mon hoc cua lop theo nam hoc
            IQueryable<ClassSubject> listClassSubject = ClassSubjectBusiness.SearchBySchool(entity.SchoolID, new Dictionary<string, object> 
            {
                {"AcademicYearID", entity.AcademicYearID}, 
                {"SubjectID", entity.SubjectID},
                {"AppliedLevel", SystemParamsInFile.APPLIED_LEVEL_PRIMARY}
            });

            List<SummedEvaluationBO> listSummedEvaluationBO = (from u in listSummedEvaluation
                                                               join lp in listPupil on u.PupilID equals lp.PupilID
                                                               where lp.ClassID == u.ClassID
                                                               select new SummedEvaluationBO
                                                               {
                                                                   SummedEvaluationID = u.SummedEvaluationID,
                                                                   PupilID = u.PupilID,
                                                                   ClassID = u.ClassID,
                                                                   EducationLevelID = lp.EducationLevelID,
                                                                   Genre = lp.Genre,
                                                                   EthnicID = lp.EthnicID,
                                                                   Mark = u.Mark
                                                               }).ToList();

            List<SummedEvaluationBO> listMarkRecord_Female = listSummedEvaluationBO.Where(o => o.Genre == SystemParamsInFile.GENRE_FEMALE).ToList();
            List<SummedEvaluationBO> listMarkRecord_Ethnic = listSummedEvaluationBO.Where(o => o.EthnicID.HasValue && o.EthnicID != EthnicID_ForeignPeople && o.EthnicID != EthnicID_Kinh).ToList();
            List<SummedEvaluationBO> listMarkRecord_FemaleEthnic = listSummedEvaluationBO.Where(o => o.Genre == SystemParamsInFile.GENRE_FEMALE && o.EthnicID.HasValue && o.EthnicID != EthnicID_ForeignPeople && o.EthnicID != EthnicID_Kinh).ToList();
            List<int> listBeginRow = new List<int>();
            int beginRow = firstRow + 3;
            int indexEducation = 0;
            int lastRow = 0;
            List<SummedEvaluationBO> listMarkRecord_Edu = new List<SummedEvaluationBO>();
            //Tạo sheet Fill dữ liệu
            int lastColumn = Math.Max(lastCol, defaultLastCol);
            IVTWorksheet dataSheet = oBook.CopySheetToLast(tempSheet,
                new VTVector(firstRow + 2, lastColumn).ToString());
            //Tạo vùng cho từng khối
            IVTRange summarizeRange = tempSheet.GetRange(firstRow + 3, 1, firstRow + 3, lastColumn);
            IVTRange topRange = tempSheet.GetRange(firstRow + 4, 1, firstRow + 4, lastColumn);
            IVTRange midRange = tempSheet.GetRange(firstRow + 5, 1, firstRow + 5, lastColumn);
            IVTRange lastRange = tempSheet.GetRange(firstRow + 6, 1, firstRow + 6, lastColumn);
            string str = string.Empty;
            string formulaSum = string.Empty;

            if ((entity.IsCheckedFemale && entity.IsCheckedEthnic && entity.IsCheckedFemaleEthnic)
                || (!entity.IsCheckedFemale && !entity.IsCheckedEthnic && !entity.IsCheckedFemaleEthnic))
            {
                #region Thống kê điểm thi học kỳ theo môn
                dataSheet.Name = "Tatca";
                str = "MÔN " + subjectName + " - " + semester.ToUpper() + " - NĂM HỌC " + academicYearTitle;
                if (entity.Semester == SystemParamsInFile.SEMESTER_OF_YEAR_SECOND)
                {
                    str = "MÔN " + subject.DisplayName.ToUpper() + " - HỌC KỲ II " + " - NĂM HỌC " + academicYearTitle;
                }
                dataSheet.SetCellValue("A7", str);
                dataSheet.SetCellValue("A6", "THỐNG KÊ ĐIỂM THI CUỐI HỌC KỲ");
                foreach (EducationLevel educationLevel in listEducationLevel)
                {
                    listBeginRow.Add(beginRow);
                    int endRow = beginRow + 1;
                    indexEducation++;
                    dataSheet.CopyPasteSameSize(summarizeRange, beginRow, 1);
                    dataSheet.SetCellValue("A" + beginRow, indexEducation);
                    dataSheet.SetCellValue("B" + beginRow, educationLevel.Resolution);

                    List<ClassProfile> listCP = listClass.Where(o => o.EducationLevelID == educationLevel.EducationLevelID).OrderBy(o => o.OrderNumber.HasValue ? o.OrderNumber : 0).ThenBy(o => o.DisplayName).ToList();
                    if (listCP == null || listCP.Count() == 0)
                    {
                        foreach (StatisticsConfig sc in listSC)
                        {
                            dataSheet.SetCellValue(beginRow, dicStatisticsConfigToColumn[sc], 0);
                        }
                        dataSheet.SetCellValue("C" + beginRow, 0);
                        endRow = beginRow;
                    }
                    else
                    {
                        //Fill dữ liệu dòng tổng
                        endRow = beginRow + listCP.Count();
                        string fomula = "=SUM(#" + (beginRow + 1) + ":" + "#" + endRow + ")";
                        foreach (StatisticsConfig sc in listSC)
                        {
                            int col = dicStatisticsConfigToColumn[sc];
                            dataSheet.SetFormulaValue(beginRow,
                                col, fomula.Replace("#", VTVector.ColumnIntToString(col)));
                        }
                        dataSheet.SetFormulaValue("C" + beginRow, fomula.Replace("#", "C"));

                        //Tạo khung dữ liệu danh sách các lớp
                        for (int i = beginRow + 1; i <= endRow; i++)
                        {
                            IVTRange copyRange;
                            if (i == beginRow + 1)
                            {
                                copyRange = topRange;
                            }
                            else if (i == endRow)
                            {
                                copyRange = lastRange;
                            }
                            else
                            {
                                copyRange = midRange;
                            }
                            dataSheet.CopyPasteSameSize(copyRange, "A" + i);
                        }

                        //Tạo dữ liệu cần fill
                        List<object> listData = new List<object>();
                        int indexClass = 0;
                        List<ClassProfile> lstClass = listCP.ToList();
                        listMarkRecord_Edu = listSummedEvaluationBO.Where(o => o.EducationLevelID == educationLevel.EducationLevelID).ToList();
                        foreach (ClassProfile cp in lstClass)
                        {
                            indexClass++;
                            Dictionary<string, object> dicData = new Dictionary<string, object>();
                            dicData["Order"] = indexClass;
                            dicData["ClassName"] = cp.DisplayName;
                            int numberOfPupil = lstPupil.Where(o => o.ClassID == cp.ClassProfileID).Sum(o => o.TotalPupil);
                            dicData["NumberOfPupil"] = numberOfPupil;

                            foreach (StatisticsConfig sc in listSC)
                            {
                                int numberOf = (from mr in listMarkRecord_Edu
                                                where mr.ClassID == cp.ClassProfileID
                                                && mr.Mark < sc.MaxValue
                                                && mr.Mark >= sc.MinValue
                                                select mr.SummedEvaluationID).Count();
                                dicData[sc.StatisticsConfigID.ToString()] = numberOf;
                            }
                            listData.Add(dicData);
                        }

                        //Fill dữ liệu
                        dataSheet.GetRange(beginRow, 1, endRow, lastColumn).FillVariableValue(new Dictionary<string, object>
                    {
                        {"list",listData}
                    });
                    }

                    //Tính vị trí tiếp theo
                    beginRow = endRow + 1;
                    lastRow = endRow;
                }

                formulaSum = "=SUM(#" + string.Join(";#", listBeginRow.ToArray<int>()) + ")";

                foreach (StatisticsConfig sc in listSC)
                {
                    int col = dicStatisticsConfigToColumn[sc];
                    dataSheet.SetFormulaValue(firstRow + 2,
                        col, formulaSum.Replace("#", VTVector.ColumnIntToString(col)));
                }
                dataSheet.SetFormulaValue("C" + (firstRow + 2), formulaSum.Replace("#", "C"));

                dataSheet.CopyPaste(tempSheet.GetRange(firstRow + 8, 1, firstRow + 8, 100), beginRow + 1, 1);

                dataSheet.CopyPaste(tempSheet.GetRange(15, 1, 15, 13), lastRow, 1, true);
                dataSheet.FitAllColumnsOnOnePage = true;

                #endregion
            }
            if (entity.IsCheckedFemale)
            {
                #region Thống kê điểm thi học kỳ theo môn học sinh nữ
                listBeginRow = new List<int>();
                beginRow = firstRow + 3;
                indexEducation = 0;
                lastRow = 0;
                listMarkRecord_Edu = new List<SummedEvaluationBO>();
                //Tạo sheet Fill dữ liệu
                lastColumn = Math.Max(lastCol, defaultLastCol);
                IVTWorksheet dataSheet_Female = oBook.CopySheetToLast(tempSheet,
                    new VTVector(firstRow + 2, lastColumn).ToString());
                dataSheet_Female.Name = "HS_Nu";
                dataSheet_Female.SetCellValue("A6", "THỐNG KÊ ĐIỂM THI CUỐI HỌC KỲ HỌC SINH NỮ");
                dataSheet_Female.SetCellValue("A7", str);

                //Tạo vùng cho từng khối
                foreach (EducationLevel educationLevel in listEducationLevel)
                {
                    listBeginRow.Add(beginRow);
                    int endRow = beginRow + 1;
                    indexEducation++;
                    dataSheet_Female.CopyPasteSameSize(summarizeRange, beginRow, 1);
                    dataSheet_Female.SetCellValue("A" + beginRow, indexEducation);
                    dataSheet_Female.SetCellValue("B" + beginRow, educationLevel.Resolution);

                    List<ClassProfile> listCP = listClass.Where(o => o.EducationLevelID == educationLevel.EducationLevelID).OrderBy(o => o.OrderNumber.HasValue ? o.OrderNumber : 0).ThenBy(o => o.DisplayName).ToList();
                    if (listCP == null || listCP.Count() == 0)
                    {
                        foreach (StatisticsConfig sc in listSC)
                        {
                            dataSheet_Female.SetCellValue(beginRow, dicStatisticsConfigToColumn[sc], 0);
                        }
                        dataSheet_Female.SetCellValue("C" + beginRow, 0);
                        endRow = beginRow;
                    }
                    else
                    {
                        //Fill dữ liệu dòng tổng
                        endRow = beginRow + listCP.Count();
                        string fomula = "=SUM(#" + (beginRow + 1) + ":" + "#" + endRow + ")";
                        foreach (StatisticsConfig sc in listSC)
                        {
                            int col = dicStatisticsConfigToColumn[sc];
                            dataSheet_Female.SetFormulaValue(beginRow,
                                col, fomula.Replace("#", VTVector.ColumnIntToString(col)));
                        }
                        dataSheet_Female.SetFormulaValue("C" + beginRow, fomula.Replace("#", "C"));

                        //Tạo khung dữ liệu danh sách các lớp
                        for (int i = beginRow + 1; i <= endRow; i++)
                        {
                            IVTRange copyRange;
                            if (i == beginRow + 1)
                            {
                                copyRange = topRange;
                            }
                            else if (i == endRow)
                            {
                                copyRange = lastRange;
                            }
                            else
                            {
                                copyRange = midRange;
                            }
                            dataSheet_Female.CopyPasteSameSize(copyRange, "A" + i);
                        }

                        //Tạo dữ liệu cần fill
                        List<object> listData = new List<object>();
                        int indexClass = 0;
                        List<ClassProfile> lstClass = listCP.ToList();
                        listMarkRecord_Edu = listMarkRecord_Female.Where(o => o.EducationLevelID == educationLevel.EducationLevelID).ToList();
                        foreach (ClassProfile cp in lstClass)
                        {
                            indexClass++;
                            Dictionary<string, object> dicData = new Dictionary<string, object>();
                            dicData["Order"] = indexClass;
                            dicData["ClassName"] = cp.DisplayName;
                            int numberOfPupil = lstPupil_Female.Where(o => o.ClassID == cp.ClassProfileID).Sum(o => o.TotalPupil);
                            dicData["NumberOfPupil"] = numberOfPupil;

                            foreach (StatisticsConfig sc in listSC)
                            {
                                int numberOf = (from mr in listMarkRecord_Edu
                                                where mr.ClassID == cp.ClassProfileID
                                                && mr.Mark < sc.MaxValue
                                                && mr.Mark >= sc.MinValue
                                                select mr.SummedEvaluationID).Count();
                                dicData[sc.StatisticsConfigID.ToString()] = numberOf;
                            }
                            listData.Add(dicData);
                        }

                        //Fill dữ liệu
                        dataSheet_Female.GetRange(beginRow, 1, endRow, lastColumn).FillVariableValue(new Dictionary<string, object>
                    {
                        {"list",listData}
                    });
                    }

                    //Tính vị trí tiếp theo
                    beginRow = endRow + 1;
                    lastRow = endRow;
                }

                formulaSum = "=SUM(#" + string.Join(";#", listBeginRow.ToArray<int>()) + ")";

                foreach (StatisticsConfig sc in listSC)
                {
                    int col = dicStatisticsConfigToColumn[sc];
                    dataSheet_Female.SetFormulaValue(firstRow + 2,
                        col, formulaSum.Replace("#", VTVector.ColumnIntToString(col)));
                }
                dataSheet_Female.SetFormulaValue("C" + (firstRow + 2), formulaSum.Replace("#", "C"));

                dataSheet_Female.CopyPaste(tempSheet.GetRange(firstRow + 8, 1, firstRow + 8, 100), beginRow + 1, 1);

                dataSheet_Female.CopyPaste(tempSheet.GetRange(15, 1, 15, 13), lastRow, 1, true);
                dataSheet_Female.FitAllColumnsOnOnePage = true;
                #endregion
            }
            if (entity.IsCheckedEthnic)
            {
                #region Thống kê điểm thi học kỳ theo môn học sinh dân tộc
                listBeginRow = new List<int>();
                beginRow = firstRow + 3;
                indexEducation = 0;
                lastRow = 0;
                listMarkRecord_Edu = new List<SummedEvaluationBO>();
                //Tạo sheet Fill dữ liệu
                lastColumn = Math.Max(lastCol, defaultLastCol);
                IVTWorksheet dataSheet_Ethnic = oBook.CopySheetToLast(tempSheet,
                    new VTVector(firstRow + 2, lastColumn).ToString());
                dataSheet_Ethnic.Name = "HS_DT";
                dataSheet_Ethnic.SetCellValue("A6", "THỐNG KÊ ĐIỂM THI CUỐI HỌC KỲ HỌC SINH DÂN TỘC");
                dataSheet_Ethnic.SetCellValue("A7", str);

                //Tạo vùng cho từng khối
                foreach (EducationLevel educationLevel in listEducationLevel)
                {
                    listBeginRow.Add(beginRow);
                    int endRow = beginRow + 1;
                    indexEducation++;
                    dataSheet_Ethnic.CopyPasteSameSize(summarizeRange, beginRow, 1);
                    dataSheet_Ethnic.SetCellValue("A" + beginRow, indexEducation);
                    dataSheet_Ethnic.SetCellValue("B" + beginRow, educationLevel.Resolution);

                    List<ClassProfile> listCP = listClass.Where(o => o.EducationLevelID == educationLevel.EducationLevelID).OrderBy(o => o.OrderNumber.HasValue ? o.OrderNumber : 0).ThenBy(o => o.DisplayName).ToList();
                    if (listCP == null || listCP.Count() == 0)
                    {
                        foreach (StatisticsConfig sc in listSC)
                        {
                            dataSheet_Ethnic.SetCellValue(beginRow, dicStatisticsConfigToColumn[sc], 0);
                        }
                        dataSheet_Ethnic.SetCellValue("C" + beginRow, 0);
                        endRow = beginRow;
                    }
                    else
                    {
                        //Fill dữ liệu dòng tổng
                        endRow = beginRow + listCP.Count();
                        string fomula = "=SUM(#" + (beginRow + 1) + ":" + "#" + endRow + ")";
                        foreach (StatisticsConfig sc in listSC)
                        {
                            int col = dicStatisticsConfigToColumn[sc];
                            dataSheet_Ethnic.SetFormulaValue(beginRow,
                                col, fomula.Replace("#", VTVector.ColumnIntToString(col)));
                        }
                        dataSheet_Ethnic.SetFormulaValue("C" + beginRow, fomula.Replace("#", "C"));

                        //Tạo khung dữ liệu danh sách các lớp
                        for (int i = beginRow + 1; i <= endRow; i++)
                        {
                            IVTRange copyRange;
                            if (i == beginRow + 1)
                            {
                                copyRange = topRange;
                            }
                            else if (i == endRow)
                            {
                                copyRange = lastRange;
                            }
                            else
                            {
                                copyRange = midRange;
                            }
                            dataSheet_Ethnic.CopyPasteSameSize(copyRange, "A" + i);
                        }

                        //Tạo dữ liệu cần fill
                        List<object> listData = new List<object>();
                        int indexClass = 0;
                        List<ClassProfile> lstClass = listCP.ToList();
                        listMarkRecord_Edu = listMarkRecord_Ethnic.Where(o => o.EducationLevelID == educationLevel.EducationLevelID).ToList();
                        foreach (ClassProfile cp in lstClass)
                        {
                            indexClass++;
                            Dictionary<string, object> dicData = new Dictionary<string, object>();
                            dicData["Order"] = indexClass;
                            dicData["ClassName"] = cp.DisplayName;

                            int numberOfPupil = lstPupil_Ethnic.Where(o => o.ClassID == cp.ClassProfileID).Sum(o => o.TotalPupil);
                            dicData["NumberOfPupil"] = numberOfPupil;

                            foreach (StatisticsConfig sc in listSC)
                            {
                                int numberOf = (from mr in listMarkRecord_Edu
                                                where mr.ClassID == cp.ClassProfileID
                                                && mr.Mark < sc.MaxValue
                                                && mr.Mark >= sc.MinValue
                                                select mr.SummedEvaluationID).Count();
                                dicData[sc.StatisticsConfigID.ToString()] = numberOf;
                            }
                            listData.Add(dicData);
                        }

                        //Fill dữ liệu
                        dataSheet_Ethnic.GetRange(beginRow, 1, endRow, lastColumn).FillVariableValue(new Dictionary<string, object>
                    {
                        {"list",listData}
                    });
                    }

                    //Tính vị trí tiếp theo
                    beginRow = endRow + 1;
                    lastRow = endRow;
                }

                formulaSum = "=SUM(#" + string.Join(";#", listBeginRow.ToArray<int>()) + ")";

                foreach (StatisticsConfig sc in listSC)
                {
                    int col = dicStatisticsConfigToColumn[sc];
                    dataSheet_Ethnic.SetFormulaValue(firstRow + 2,
                        col, formulaSum.Replace("#", VTVector.ColumnIntToString(col)));
                }
                dataSheet_Ethnic.SetFormulaValue("C" + (firstRow + 2), formulaSum.Replace("#", "C"));

                dataSheet_Ethnic.CopyPaste(tempSheet.GetRange(firstRow + 8, 1, firstRow + 8, 100), beginRow + 1, 1);

                dataSheet_Ethnic.CopyPaste(tempSheet.GetRange(15, 1, 15, 13), lastRow, 1, true);
                dataSheet_Ethnic.FitAllColumnsOnOnePage = true;
                #endregion
            }
            if (entity.IsCheckedFemale && entity.IsCheckedEthnic)
            {
                #region Thống kê điểm thi học kỳ theo môn học sinh nữ dân tộc
                listBeginRow = new List<int>();
                beginRow = firstRow + 3;
                indexEducation = 0;
                lastRow = 0;
                listMarkRecord_Edu = new List<SummedEvaluationBO>();
                //Tạo sheet Fill dữ liệu
                lastColumn = Math.Max(lastCol, defaultLastCol);
                IVTWorksheet dataSheet_FemaleEthnic = oBook.CopySheetToLast(tempSheet,
                    new VTVector(firstRow + 2, lastColumn).ToString());
                dataSheet_FemaleEthnic.Name = "HS_NU_DT";
                dataSheet_FemaleEthnic.SetCellValue("A6", "THỐNG KÊ ĐIỂM THI CUỐI HỌC KỲ HỌC SINH NỮ DÂN TỘC");
                dataSheet_FemaleEthnic.SetCellValue("A7", str);

                //Tạo vùng cho từng khối
                foreach (EducationLevel educationLevel in listEducationLevel)
                {
                    listBeginRow.Add(beginRow);
                    int endRow = beginRow + 1;
                    indexEducation++;
                    dataSheet_FemaleEthnic.CopyPasteSameSize(summarizeRange, beginRow, 1);
                    dataSheet_FemaleEthnic.SetCellValue("A" + beginRow, indexEducation);
                    dataSheet_FemaleEthnic.SetCellValue("B" + beginRow, educationLevel.Resolution);

                    List<ClassProfile> listCP = listClass.Where(o => o.EducationLevelID == educationLevel.EducationLevelID).OrderBy(o => o.OrderNumber.HasValue ? o.OrderNumber : 0).ThenBy(o => o.DisplayName).ToList();
                    if (listCP == null || listCP.Count() == 0)
                    {
                        foreach (StatisticsConfig sc in listSC)
                        {
                            dataSheet_FemaleEthnic.SetCellValue(beginRow, dicStatisticsConfigToColumn[sc], 0);
                        }
                        dataSheet_FemaleEthnic.SetCellValue("C" + beginRow, 0);
                        endRow = beginRow;
                    }
                    else
                    {
                        //Fill dữ liệu dòng tổng
                        endRow = beginRow + listCP.Count();
                        string fomula = "=SUM(#" + (beginRow + 1) + ":" + "#" + endRow + ")";
                        foreach (StatisticsConfig sc in listSC)
                        {
                            int col = dicStatisticsConfigToColumn[sc];
                            dataSheet_FemaleEthnic.SetFormulaValue(beginRow,
                                col, fomula.Replace("#", VTVector.ColumnIntToString(col)));
                        }
                        dataSheet_FemaleEthnic.SetFormulaValue("C" + beginRow, fomula.Replace("#", "C"));

                        //Tạo khung dữ liệu danh sách các lớp
                        for (int i = beginRow + 1; i <= endRow; i++)
                        {
                            IVTRange copyRange;
                            if (i == beginRow + 1)
                            {
                                copyRange = topRange;
                            }
                            else if (i == endRow)
                            {
                                copyRange = lastRange;
                            }
                            else
                            {
                                copyRange = midRange;
                            }
                            dataSheet_FemaleEthnic.CopyPasteSameSize(copyRange, "A" + i);
                        }

                        //Tạo dữ liệu cần fill
                        List<object> listData = new List<object>();
                        int indexClass = 0;
                        List<ClassProfile> lstClass = listCP.ToList();
                        listMarkRecord_Edu = listMarkRecord_FemaleEthnic.Where(o => o.EducationLevelID == educationLevel.EducationLevelID).ToList();
                        foreach (ClassProfile cp in lstClass)
                        {
                            indexClass++;
                            Dictionary<string, object> dicData = new Dictionary<string, object>();
                            dicData["Order"] = indexClass;
                            dicData["ClassName"] = cp.DisplayName;

                            int numberOfPupil = lstPupil_FemaleEthnic.Where(o => o.ClassID == cp.ClassProfileID).Sum(o => o.TotalPupil);
                            dicData["NumberOfPupil"] = numberOfPupil;

                            foreach (StatisticsConfig sc in listSC)
                            {
                                int numberOf = (from mr in listMarkRecord_Edu
                                                where mr.ClassID == cp.ClassProfileID
                                                && mr.Mark < sc.MaxValue
                                                && mr.Mark >= sc.MinValue
                                                select mr.SummedEvaluationID).Count();
                                dicData[sc.StatisticsConfigID.ToString()] = numberOf;
                            }
                            listData.Add(dicData);
                        }

                        //Fill dữ liệu
                        dataSheet_FemaleEthnic.GetRange(beginRow, 1, endRow, lastColumn).FillVariableValue(new Dictionary<string, object>
                    {
                        {"list",listData}
                    });
                    }

                    //Tính vị trí tiếp theo
                    beginRow = endRow + 1;
                    lastRow = endRow;
                }

                formulaSum = "=SUM(#" + string.Join(";#", listBeginRow.ToArray<int>()) + ")";

                foreach (StatisticsConfig sc in listSC)
                {
                    int col = dicStatisticsConfigToColumn[sc];
                    dataSheet_FemaleEthnic.SetFormulaValue(firstRow + 2,
                        col, formulaSum.Replace("#", VTVector.ColumnIntToString(col)));
                }
                dataSheet_FemaleEthnic.SetFormulaValue("C" + (firstRow + 2), formulaSum.Replace("#", "C"));

                dataSheet_FemaleEthnic.CopyPaste(tempSheet.GetRange(firstRow + 8, 1, firstRow + 8, 100), beginRow + 1, 1);

                dataSheet_FemaleEthnic.CopyPaste(tempSheet.GetRange(15, 1, 15, 13), lastRow, 1, true);
                dataSheet_FemaleEthnic.FitAllColumnsOnOnePage = true;
                #endregion
            }
            //Xoá sheet template
            firstSheet.Delete();
            tempSheet.Delete();
            return oBook.ToStream();
        }
        #endregion


    }
}
