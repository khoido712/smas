﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Linq.Expressions;
using System.Web;
using log4net;

using SMAS.DAL.Repository;
using SMAS.DAL.IRepository;
using SMAS.Business.IBusiness;
using SMAS.Models.Models;
using SMAS.Business.BusinessObject;
using SMAS.Business.Common;
using System.IO;
using SMAS.VTUtils.Excel.Export;

namespace SMAS.Business.Business
{
    /// <summary>
    /// Bao cao tong hop cac mon theo dot
    /// </summary>
    /// <author>Quanglm</author>
    /// <date>11/29/2012</date>
    public class SynthesisClassifiedSubjectByPeriodBusiness : GenericBussiness<ProcessedReport>, ISynthesisClassifiedSubjectByPeriodBusiness
    {
        IProcessedReportParameterRepository ProcessedReportParameterRepository;
        IProcessedReportRepository ProcessedReportRepository;
        IClassSubjectRepository ClassSubjectRepository;
        IPupilOfClassRepository PupilOfClassRepository;

        public SynthesisClassifiedSubjectByPeriodBusiness(ILog logger, SMASEntities context = null)
            : base(logger)
        {
            if (context == null) { context = new SMASEntities(); }
            this.context = context;
            this.ProcessedReportParameterRepository = new ProcessedReportParameterRepository(context);
            this.ProcessedReportRepository = new ProcessedReportRepository(context);
            this.ClassSubjectRepository = new ClassSubjectRepository(context);
            this.PupilOfClassRepository = new PupilOfClassRepository(context);
        }

        #region Tạo mảng băm cho các tham số đầu vào cho báo cáo
        public string GetHashKey(SynthesisClassifiedSubjectByPeriod entity)
        {
            IDictionary<string, object> dic = new Dictionary<string, object> {
                {"AcademicYearID", entity.AcademicYearID},
                {"PeriodID ", entity.PeriodID},
                {"SchoolID ", entity.SchoolID},
                {"AppliedLevel ", entity.AppliedLevel}
            };
            return ReportUtils.GetHashKey(dic);
        }
        #endregion

        #region Lấy danh sách tổng hợp xếp loại các môn được cập nhật mới nhất
        /// <summary>
        /// GetSynthesisClassifiedSubjectByPeriod
        /// </summary>
        /// <param name="entity">The entity.</param>
        /// <returns>
        /// ProcessedReport
        /// </returns>
        /// <author>Quanglm</author>
        /// <date>11/29/2012</date>
        public ProcessedReport GetSynthesisClassifiedSubjectByPeriod(SynthesisClassifiedSubjectByPeriod entity)
        {
            string reportCode = SystemParamsInFile.REPORT_THONGKE_TONGHOP_XEPLOAI_MON_THEODOT;
            string inputParameterHashKey = GetHashKey(entity);
            return ProcessedReportBusiness.GetProcessedReport(reportCode, inputParameterHashKey);
        }
        #endregion

        #region Lưu báo cáo danh sách tổng hợp xếp loại các môn vào CSDL
        /// <summary>
        /// InsertSynthesisClassifiedSubjectByPeriod
        /// </summary>
        /// <param name="entity">The entity.</param>
        /// <param name="data">The data.</param>
        /// <returns>
        /// ProcessedReport
        /// </returns>
        /// <author>Quanglm</author>
        /// <date>11/29/2012</date>
        public ProcessedReport InsertSynthesisClassifiedSubjectByPeriod(SynthesisClassifiedSubjectByPeriod entity, Stream data)
        {
            string reportCode = SystemParamsInFile.REPORT_THONGKE_TONGHOP_XEPLOAI_MON_THEODOT;
            ReportDefinition reportDef = ReportDefinitionBusiness.GetByCode(reportCode);
            ProcessedReport pr = new ProcessedReport();
            pr.ReportCode = reportCode;
            pr.ProcessedDate = DateTime.Now;
            pr.InputParameterHashKey = GetHashKey(entity);
            pr.ReportData = ReportUtils.Compress(data);

            //Tạo tên file HS_PTTH_ThongKeXLHK_[Semester]_[Period]
            string outputNamePattern = reportDef.OutputNamePattern;
            PeriodDeclaration period = PeriodDeclarationBusiness.Find(entity.PeriodID);
            string periodName = period.Resolution;
            string semester = ReportUtils.ConvertSemesterForReportName(period.Semester.Value);
            string appliedLevel = ReportUtils.ConvertAppliedLevelForReportName(entity.AppliedLevel);

            outputNamePattern = outputNamePattern.Replace("[Period]", periodName);
            outputNamePattern = outputNamePattern.Replace("[Semester]", semester);
            outputNamePattern = outputNamePattern.Replace("[SchoolType]", appliedLevel);
            pr.ReportName = ReportUtils.RemoveSpecialCharacters(outputNamePattern) + "." + reportDef.OutputFormat;

            IDictionary<string, object> dic = new Dictionary<string, object> {
                {"AcademicYearID", entity.AcademicYearID},
                {"PeriodID ", entity.PeriodID},
                {"SchoolID ", entity.SchoolID},
                {"AppliedLevel ", entity.AppliedLevel}
            };
            ProcessedReportParameterRepository.Insert(dic, pr);
            ProcessedReportRepository.Insert(pr);
            ProcessedReportRepository.Save();
            return pr;
        }
        #endregion

        #region Tạo file báo cáo tổng hợp xếp loại các môn học
        /// <summary>
        /// CreateSynthesisClassifiedSubjectByPeriod
        /// </summary>
        /// <param name="entity">The entity.</param>
        /// <returns>
        /// Stream
        /// </returns>
        /// <author>Quanglm</author>
        /// <date>11/29/2012</date>
        public Stream CreateSynthesisClassifiedSubjectByPeriod(SynthesisClassifiedSubjectByPeriod entity)
        {
            string reportCode = SystemParamsInFile.REPORT_THONGKE_TONGHOP_XEPLOAI_MON_THEODOT;
            ReportDefinition reportDef = ReportDefinitionBusiness.GetByCode(reportCode);
            string templatePath = ReportUtils.GetTemplatePath(reportDef);
            IVTWorkbook oBook = VTExport.OpenWorkbook(templatePath);

            int firstRow = 9;
            int defaultNumbeOfColumn = 7;
            int lenCopTemp = 5;
            int firstDynamicCol = VTVector.dic['D'];
            int defaultLastCol = VTVector.dic['Q'];
            //Lấy sheet template
            IVTWorksheet firstSheet = oBook.GetSheet(1);
            IVTRange dynamicTemp = firstSheet.GetRange(firstRow, firstDynamicCol, firstRow + lenCopTemp, firstDynamicCol + 1);

            //Tạo template chuẩn
            IVTWorksheet tempSheet = oBook.CopySheetToLast(firstSheet, "Q" + (firstRow - 1));
            tempSheet.CopyPasteSameSize(firstSheet.GetRange(firstRow, 1, firstRow + lenCopTemp, firstDynamicCol - 1), "A" + firstRow);
            tempSheet.CopyPaste(firstSheet.GetRange(firstRow + 6, 1, firstRow + 6, 100), firstRow + 8, 1);

            //Lấy danh sách các cột động
            //Viethd4: Fix sua lay muc thong ke theo bang moi
            List<StatisticLevelConfig> listSC;
            //List<StatisticsConfig> listSC = StatisticsConfigBusiness.GetList(
            //    entity.AppliedLevel,
            //    SystemParamsInFile.StatisticsConfig.REPORT_TYPE_HOC_LUC_MON);
            int StatisticLevelReportID = entity.StatisticLevelReportID;
            listSC = StatisticLevelConfigBusiness.Search(new Dictionary<string, object>() { { "StatisticLevelReportID", StatisticLevelReportID } }).OrderBy(o => o.OrderNumber).ToList();
            listSC.AddRange(new List<StatisticLevelConfig>() {
                    new StatisticLevelConfig{
                        Title="Đ",
                        StatisticLevelConfigID=-1
                    },
                    new StatisticLevelConfig
                    {
                        Title="CĐ",
                        StatisticLevelConfigID=-2
                    }
                });


            //Tạo các cột động
            Dictionary<int, int> dicStatisticsConfigToColumn = new Dictionary<int, int>();
            for (int i = 0; i < listSC.Count; i++)
            {
                StatisticLevelConfig sc = listSC[i];
                tempSheet.CopyPasteSameSize(dynamicTemp, firstRow, firstDynamicCol + i * 2);
                tempSheet.SetCellValue(firstRow, firstDynamicCol + i * 2, sc.Title);
                dicStatisticsConfigToColumn.Add(sc.StatisticLevelConfigID, firstDynamicCol + i * 2);
            }
            int lastCol = defaultLastCol + (listSC.Count - defaultNumbeOfColumn) * 2;

            //Chỉnh lại dòng khác
            if (listSC.Count > defaultNumbeOfColumn)
            {
                tempSheet.MergeRow(2, VTVector.dic['I'], lastCol);
                tempSheet.MergeRow(3, VTVector.dic['I'], lastCol);
                tempSheet.MergeRow(6, VTVector.dic['A'], lastCol);
                tempSheet.MergeRow(7, VTVector.dic['A'], lastCol);

                double colWidth = firstSheet.GetColumnWidth(firstDynamicCol);
                double resizeWidth = colWidth * defaultNumbeOfColumn / listSC.Count;
                for (int i = firstDynamicCol; i < lastCol; i++)
                {
                    tempSheet.SetColumnWidth(i, resizeWidth);
                }
            }

            //Tính các công thức 
            string formulaPercent = "=IF(cellNumberOfPupil=0;0;ROUND(cellTotal/cellNumberOfPupil;4)*100)";

            for (int i = 0; i < listSC.Count; i++)
            {
                StatisticLevelConfig sc = listSC[i];
                tempSheet.SetCellValue(firstRow + 2, dicStatisticsConfigToColumn[sc.StatisticLevelConfigID], "${list[].Get(" + sc.StatisticLevelConfigID + ")}");
            }

            for (int rowIndex = firstRow + 2; rowIndex < firstRow + 7; rowIndex++)
            {
                for (int i = firstDynamicCol; i < lastCol; i += 2)
                {
                    string cellNumberOfPupil = "C" + rowIndex;
                    string cellTotal = new VTVector(rowIndex, i).ToString();
                    tempSheet.SetFormulaValue(rowIndex, i + 1, formulaPercent.
                        Replace("cellTotal", cellTotal).Replace("cellNumberOfPupil", cellNumberOfPupil));
                }
            }

            //Fill dữ liệu chung cho template
            AcademicYear academicYear = AcademicYearBusiness.Find(entity.AcademicYearID);
            SchoolProfile school = SchoolProfileBusiness.Find(entity.SchoolID);
            Province province = school.Province;

            string schoolName = school.SchoolName.ToUpper();
            string supervisingDeptName = UtilsBusiness.GetSupervisingDeptName(school.SchoolProfileID, entity.AppliedLevel).ToUpper();
            string academicYearTitle = academicYear.DisplayTitle;
            string provinceName = (school.District != null ? school.District.DistrictName : "");

            DateTime reportDate = DateTime.Now;
            PeriodDeclaration period = PeriodDeclarationBusiness.Find(entity.PeriodID);
            if (period == null)
            {
                throw new BusinessException("Chưa chọn đợt");
            }
            string periodName = period.Resolution;
            string semester = SMASConvert.ConvertSemester(period.Semester.Value).ToUpper();

            Dictionary<string, object> dicGeneralInfo = new Dictionary<string, object>
                {
                    {"SchoolName", schoolName},
                    {"SupervisingDeptName", supervisingDeptName},
                    {"AcademicYearTitle", academicYearTitle},
                    {"Period", periodName},
                    {"ProvinceName", provinceName},
                    {"ReportDate", reportDate},
                    {"Semester", semester},
                    //{"PupilName",PupilName}
                };

            tempSheet.GetRange(1, 1, firstRow - 1, defaultLastCol).FillVariableValue(dicGeneralInfo);

            //Tạo sheet Fill dữ liệu
            int lastColumn = Math.Max(lastCol, defaultLastCol);
            IVTWorksheet dataSheet = oBook.CopySheetToLast(tempSheet, new VTVector(firstRow + 2, lastColumn).ToString());


            //Tạo vùng dữ liệu
            #region Danh sach lop, si so
            // Danh sach khoi
            List<EducationLevel> lstEducationLevel = EducationLevelBusiness.GetByGrade(entity.AppliedLevel).ToList();


            //Danh sach lop hoc
            IQueryable<ClassProfile> lsClass = ClassProfileBusiness.SearchBySchool(entity.SchoolID, new Dictionary<string, object> {
                                                {"AppliedLevel", entity.AppliedLevel},
                                                {"AcademicYearID", entity.AcademicYearID}}).OrderBy(o => o.DisplayName);

            IQueryable<PupilOfClass> lsPupilOfClassSemester1 = PupilOfClassBusiness.SearchBySchool(entity.SchoolID, new Dictionary<string, object> {
                                                {"AppliedLevel", entity.AppliedLevel},
                                                {"AcademicYearID", entity.AcademicYearID}
                                                }).AddCriteriaSemester(AcademicYearBusiness.Find(entity.AcademicYearID), period.Semester.Value);
            IQueryable<PupilOfClass> lsPupilOfClassSemester = lsPupilOfClassSemester1.Where(o => o.AssignedDate == lsPupilOfClassSemester1.Where(u => u.PupilID == o.PupilID && u.ClassID == o.ClassID).Select(u => u.AssignedDate).Max());

            IQueryable<PupilOfClassBO> tmpPoc;
            if (entity.SemesterID == GlobalConstants.SEMESTER_OF_YEAR_FIRST)
            {
                tmpPoc = (from poc in lsPupilOfClassSemester
                          join cs in ClassSubjectBusiness.All on poc.ClassID equals cs.ClassID
                          where (cs.IsSubjectVNEN == null || cs.IsSubjectVNEN == false)
                          && cs.SectionPerWeekFirstSemester.HasValue && cs.SectionPerWeekFirstSemester > 0
                          select new PupilOfClassBO
                          {
                              PupilID = poc.PupilID,
                              ClassID = poc.ClassID,
                              SubjectID = cs.SubjectID,
                              AppliedType = cs.AppliedType,
                              IsSpecialize = cs.IsSpecializedSubject,
                              Genre = poc.PupilProfile.Genre,
                              EthnicID = poc.PupilProfile.EthnicID
                          });
            }
            else if (entity.SemesterID == GlobalConstants.SEMESTER_OF_YEAR_SECOND)
            {
                tmpPoc = (from poc in lsPupilOfClassSemester
                          join cs in ClassSubjectBusiness.All on poc.ClassID equals cs.ClassID
                          where (cs.IsSubjectVNEN == null || cs.IsSubjectVNEN == false)
                          && cs.SectionPerWeekSecondSemester.HasValue && cs.SectionPerWeekSecondSemester > 0
                          select new PupilOfClassBO
                          {
                              PupilID = poc.PupilID,
                              ClassID = poc.ClassID,
                              SubjectID = cs.SubjectID,
                              AppliedType = cs.AppliedType,
                              IsSpecialize = cs.IsSpecializedSubject,
                              Genre = poc.PupilProfile.Genre,
                              EthnicID = poc.PupilProfile.EthnicID
                          });
            }
            else
            {
                tmpPoc = (from poc in lsPupilOfClassSemester
                          join cs in ClassSubjectBusiness.All on poc.ClassID equals cs.ClassID
                          where (cs.IsSubjectVNEN == null || cs.IsSubjectVNEN == false)
                          select new PupilOfClassBO
                          {
                              PupilID = poc.PupilID,
                              ClassID = poc.ClassID,
                              SubjectID = cs.SubjectID,
                              AppliedType = cs.AppliedType,
                              IsSpecialize = cs.IsSpecializedSubject,
                              Genre = poc.PupilProfile.Genre,
                              EthnicID = poc.PupilProfile.EthnicID
                          });
            }

            //lay danh sach hoc sinh dang ky mon chuyen mon tu chon
            IDictionary<string, object> dic = new Dictionary<string, object>()
            {
                {"SchoolID",entity.SchoolID},
                {"AcademicYearID",entity.AcademicYearID},
                //{"SubjectID",entity.SubjectID},
                {"SemesterID",entity.SemesterID},
                {"YearID",AcademicYearBusiness.Find(entity.AcademicYearID).Year}
            };

            List<PupilOfClassBO> lstPOC = this.GetPupilOfClassInSpecialize(tmpPoc, dic);

            //Danh sach hoc sinh mien giam
            List<ExemptedSubject> lstExempSubject = ExemptedSubjectBusiness.GetExmpSubjectbyAcademicYear(entity.AcademicYearID, entity.SemesterID, 0, 0);

            //Khong lay cac hoc sinh mien giam
            if (lstExempSubject != null && lstExempSubject.Count > 0)
            {
                lstPOC = lstPOC.Where(p => !lstExempSubject.Exists(s => s.PupilID == p.PupilID && p.ClassID == s.ClassID && s.SubjectID == p.SubjectID)).ToList();

            }



            Ethnic Ethnic_Kinh = EthnicBusiness.All.Where(o => o.EthnicCode.ToLower().Equals(SystemParamsInFile.ETHNIC_CODE_KINH.ToLower()) && o.IsActive == true).FirstOrDefault();
            Ethnic Ethnic_ForeignPeople = EthnicBusiness.All.Where(o => o.EthnicCode.ToLower().Equals(SystemParamsInFile.ETHNIC_CODE_NN.ToLower()) && o.IsActive == true).FirstOrDefault();

            List<PupilOfClassBO> lstPOCFemale = lstPOC.Where(pp => pp.Genre == GlobalConstants.GENRE_FEMALE).ToList();
            List<PupilOfClassBO> lstPOCEthnic = lstPOC.Where(pp => pp.EthnicID.HasValue && pp.EthnicID != Ethnic_Kinh.EthnicID && pp.EthnicID != Ethnic_ForeignPeople.EthnicID).ToList();
            List<PupilOfClassBO> lstPOCFemaleEthnic = lstPOC.Where(pp => pp.Genre == GlobalConstants.GENRE_FEMALE
                && pp.EthnicID.HasValue && pp.EthnicID != Ethnic_Kinh.EthnicID && pp.EthnicID != Ethnic_ForeignPeople.EthnicID).ToList();

            dic.Add("Type", SystemParamsInFile.GET_NUMPUPIL_IN_CLASS_TYPE1);
            //dic.Add("EthnicID", entity.EthnicID);
            //dic.Add("FemaleID", entity.FemaleID);
            List<ClassInfoBO> ListClass = PupilOfClassBusiness.GetClassAndTotalPupilNotRegister(lsClass, dic).ToList();

            List<ClassInfoBO> ListClassFemale = new List<ClassInfoBO>();
            if (entity.FemaleID > 0)
            {
                dic["FemaleID"] = 1;
                dic["EthnicID"] = 0;
                ListClassFemale = PupilOfClassBusiness.GetClassAndTotalPupilNotRegister(lsClass, dic).ToList();
            }

            List<ClassInfoBO> ListClassEthnic = new List<ClassInfoBO>();
            if (entity.EthnicID > 0)
            {
                dic["FemaleID"] = 0;
                dic["EthnicID"] = 1;
                ListClassEthnic = PupilOfClassBusiness.GetClassAndTotalPupilNotRegister(lsClass, dic).ToList();
            }

            List<ClassInfoBO> ListClassFemaleEthnic = new List<ClassInfoBO>();
            if (entity.FemaleEthnicID > 0)
            {
                dic["FemaleID"] = 1;
                dic["EthnicID"] = 1;
                ListClassFemaleEthnic = PupilOfClassBusiness.GetClassAndTotalPupilNotRegister(lsClass, dic).ToList();
            }
            #endregion

            IDictionary<string, object> dicSubject = new Dictionary<string, object>()
            {
                {"AcademicYearID", entity.AcademicYearID},
                {"AppliedLevel", entity.AppliedLevel}
            };
            // Danh sách môn học theo trường
            List<SubjectCatBO> listSchoolSubject = SchoolSubjectBusiness.SearchBySchool(entity.SchoolID, dicSubject)
                .Select(o => new SubjectCatBO
                {
                    SubjectCatID = o.SubjectID,
                    DisplayName = o.SubjectCat.DisplayName,
                    IsCommenting = o.SubjectCat.IsCommenting,
                    //IsCommenting = o.IsCommenting,
                    OrderInSubject = o.SubjectCat.OrderInSubject
                }).Distinct()
                .OrderBy(o => o.OrderInSubject)
                .ThenBy(o => o.DisplayName)
                .ToList();
            // Danh sách điểm tổng kết của trường theo đợt và năm học
            IDictionary<string, object> dicSummedUpRecord = new Dictionary<string, object>()
            {
                {"AcademicYearID", entity.AcademicYearID},
                {"PeriodID", entity.PeriodID},
                {"AppliedLevel", entity.AppliedLevel}
            };
            IQueryable<VSummedUpRecord> tmpSummedUp = VSummedUpRecordBusiness.SearchBySchool(entity.SchoolID, dicSummedUpRecord);
            List<VSummedUpRecord> lstSummedUp = tmpSummedUp.ToList();
            //Khong lay cac hoc sinh mien giam
            if (lstExempSubject != null && lstExempSubject.Count > 0)
            {
                lstSummedUp = lstSummedUp.Where(p => !lstExempSubject.Exists(s => s.PupilID == p.PupilID && p.ClassID == s.ClassID && s.SubjectID == p.SubjectID)).ToList();
            }

            // Danh sach mon hoc cua toan truong theo cap
            List<ClassSubject> listClassSubjectAll = ClassSubjectBusiness.SearchBySchool(entity.SchoolID,
                new Dictionary<string, object>() {
                { "AppliedLevel", entity.AppliedLevel },
                { "AcademicYearID", entity.AcademicYearID },
                { "Semester", entity.SemesterID },
                }).ToList();

            //Danh sach mon hoc
            List<SubjectCat> lstSubjectCatAll = SubjectCatBusiness.All.Where(o => o.AppliedLevel == entity.AppliedLevel && o.IsActive == true).ToList();


          
            FillData(tempSheet, dataSheet, lastColumn, lstEducationLevel, ListClass, lstPOC, listSchoolSubject, lstSummedUp, listSC, listClassSubjectAll,
                "TOAN_TRUONG", lstSubjectCatAll);

            if (entity.FemaleID > 0)
            {
                dataSheet = oBook.CopySheetToLast(tempSheet, new VTVector(firstRow + 2, lastColumn).ToString());
                dataSheet.SetCellValue("A6", "THỐNG KÊ XẾP LOẠI MÔN HỌC TOÀN TRƯỜNG HỌC SINH NỮ");
                FillData(tempSheet, dataSheet, lastColumn, lstEducationLevel, ListClassFemale, lstPOCFemale, listSchoolSubject, lstSummedUp, listSC, listClassSubjectAll,
               "HS_Nu", lstSubjectCatAll);
            }

            if (entity.EthnicID > 0)
            {
                dataSheet = oBook.CopySheetToLast(tempSheet, new VTVector(firstRow + 2, lastColumn).ToString());
                dataSheet.SetCellValue("A6", "THỐNG KÊ XẾP LOẠI MÔN HỌC TOÀN TRƯỜNG HỌC SINH DÂN TỘC");
                FillData(tempSheet, dataSheet, lastColumn, lstEducationLevel, ListClassEthnic, lstPOCEthnic, listSchoolSubject, lstSummedUp, listSC, listClassSubjectAll,
               "HS_DT", lstSubjectCatAll);
            }

            if (entity.FemaleEthnicID > 0)
            {
                dataSheet = oBook.CopySheetToLast(tempSheet, new VTVector(firstRow + 2, lastColumn).ToString());
                dataSheet.SetCellValue("A6", "THỐNG KÊ XẾP LOẠI MÔN HỌC TOÀN TRƯỜNG HỌC SINH NỮ DÂN TỘC");
                FillData(tempSheet, dataSheet, lastColumn, lstEducationLevel, ListClassFemaleEthnic, lstPOCFemaleEthnic, listSchoolSubject, lstSummedUp, listSC, listClassSubjectAll,
               "HS_Nu_DT", lstSubjectCatAll);
            }

            //Xoá sheet template
            firstSheet.Delete();
            tempSheet.Delete();

            return oBook.ToStream();
        }

        private void FillData(IVTWorksheet tempSheet, IVTWorksheet dataSheet, int lastColumn,
            List<EducationLevel> lstEducationLevel, List<ClassInfoBO> ListClass, List<PupilOfClassBO> lstPOC,
            List<SubjectCatBO> listSchoolSubject, List<VSummedUpRecord> tmpSummedUp, List<StatisticLevelConfig> listSC,
            List<ClassSubject> listClassSubjectAll, string sheetName, List<SubjectCat> lstSubjectCatAll)
        {
            int firstRow = 9;

            //Tạo vùng dữ liệu           
            IVTRange topRange = tempSheet.GetRange(firstRow + 2, 1, firstRow + 2, lastColumn);
            IVTRange midRange = tempSheet.GetRange(firstRow + 3, 1, firstRow + 3, lastColumn);
            IVTRange lastRange = tempSheet.GetRange(firstRow + 4, 1, firstRow + 4, lastColumn);



            

            var iqSummedUpRecord = (from u in tmpSummedUp
                                    join l in lstPOC on new { u.ClassID, u.PupilID, u.SubjectID } equals new { l.ClassID, l.PupilID, l.SubjectID }
                                    select u);

            var listMarkGroup = from s in listSC.ToList()
                                join m in iqSummedUpRecord on 1 equals 1
                                where (s.SignMin == SystemParamsInFile.Sign_Compare.GREATER_OR_EQUAL_SIGN
                                    && m.SummedUpMark >= s.MinValue && ((s.SignMax == SystemParamsInFile.Sign_Compare.SMALLER_OR_EQUAL_SIGN && m.SummedUpMark <= s.MaxValue)
                                                                        || (s.SignMax == SystemParamsInFile.Sign_Compare.SMALLER_SIGN && m.SummedUpMark < s.MaxValue)
                                                                        || (s.SignMax == SystemParamsInFile.Sign_Compare.EQUAL_SIGN && m.SummedUpMark == s.MaxValue)
                                                                        || (s.SignMax == null)))
                                || (s.SignMin == SystemParamsInFile.Sign_Compare.GREATEER_THAN_SIGN
                                    && m.SummedUpMark > s.MinValue && ((s.SignMax == SystemParamsInFile.Sign_Compare.SMALLER_OR_EQUAL_SIGN && m.SummedUpMark <= s.MaxValue)
                                                                        || (s.SignMax == SystemParamsInFile.Sign_Compare.SMALLER_SIGN && m.SummedUpMark < s.MaxValue)
                                                                        || (s.SignMax == SystemParamsInFile.Sign_Compare.EQUAL_SIGN && m.SummedUpMark == s.MaxValue)
                                                                        || (s.SignMax == null)))
                                || (s.SignMin == null && ((s.SignMax == SystemParamsInFile.Sign_Compare.SMALLER_OR_EQUAL_SIGN && m.SummedUpMark <= s.MaxValue)
                                                                        || (s.SignMax == SystemParamsInFile.Sign_Compare.SMALLER_SIGN && m.SummedUpMark < s.MaxValue)
                                                                        || (s.SignMax == SystemParamsInFile.Sign_Compare.EQUAL_SIGN && m.SummedUpMark == s.MaxValue)))
                                || (s.MinValue == null && s.MaxValue == null && s.Title == m.JudgementResult)
                                select new
                                {
                                    s.StatisticLevelConfigID,
                                    m.SchoolID,
                                    m.Semester,
                                    m.PeriodID,
                                    m.SubjectID
                                };

            var listMarkGroupCount = (from g in listMarkGroup
                                      group g by new
                                      {
                                          g.SchoolID,
                                          g.StatisticLevelConfigID,
                                          g.Semester,
                                          g.PeriodID,
                                          g.SubjectID
                                      } into c
                                      select new
                                      {
                                          c.Key.SchoolID,
                                          c.Key.StatisticLevelConfigID,
                                          c.Key.Semester,
                                          c.Key.PeriodID,
                                          c.Key.SubjectID,
                                          CountMark = c.Count()
                                      }
                                     ).ToList();


            // fill data
            int beginRow = firstRow + 2;
            int endRow = beginRow + listSchoolSubject.Count - 1;
            //Tạo khung dữ liệu danh sách các lớp
            for (int i = beginRow; i <= endRow; i++)
            {
                IVTRange copyRange;
                if (i == beginRow)
                {
                    copyRange = topRange;
                }
                else if (i == endRow)
                {
                    copyRange = lastRange;
                }
                else
                {
                    copyRange = midRange;
                }
                dataSheet.CopyPasteSameSize(copyRange, "A" + i);
            }
            //Tạo dữ liệu cần fill
            List<object> listData = new List<object>();
            int index = 0;
            foreach (SubjectCatBO item in listSchoolSubject)
            {
                index++;
                // Kiem tra neu chua gan loai mon thi lay tu danh muc mon hoc
                if (!item.IsCommenting.HasValue)
                {
                    item.IsCommenting = lstSubjectCatAll.Where(o => o.SubjectCatID == item.SubjectCatID).First().IsCommenting;
                }
                // Danh sach mon hoc cua lop theo cap
                List<ClassSubject> listClassSubject = listClassSubjectAll
                    .Where(o => o.SubjectID == item.SubjectCatID).ToList();
                List<int> lstClassID = listClassSubject.Select(o => o.ClassID).ToList();

                // Danh sach hoc sinh cua lop theo truong

                var countPupilBySubject = ListClass.Where(o => lstClassID.Contains(o.ClassID) && o.SubjectID == item.SubjectCatID).Sum(o => o.ActualNumOfPupil);
                int countPupil = countPupilBySubject;
                Dictionary<string, object> dicData = new Dictionary<string, object>();
                dicData["SubjectName"] = item.DisplayName;
                dicData["Order"] = index;
                // So luong hoc sinh hoc mon hoc nay trong khoi tuong ung
                dicData["NumberOfPupil"] = countPupil;

                foreach (StatisticLevelConfig sc in listSC)
                {

                    int numberOf = 0;
                    var markCount = listMarkGroupCount.Where(o => o.StatisticLevelConfigID == sc.StatisticLevelConfigID && o.SubjectID == item.SubjectCatID).FirstOrDefault();
                    numberOf = markCount != null ? markCount.CountMark : 0;
                    dicData[sc.StatisticLevelConfigID.ToString()] = numberOf;
                }
                listData.Add(dicData);
            }
            //Fill dữ liệu
            dataSheet.GetRange(beginRow, 1, endRow, lastColumn).FillVariableValue(new Dictionary<string, object>
                    {
                        {"list",listData}
                    });
            dataSheet.CopyPaste(tempSheet.GetRange(firstRow + 8, 1, firstRow + 8, 100), endRow + 2, 1);
            dataSheet.SetCellValue(UtilsBusiness.GetExcelColumnName(1) + (endRow + 1), "(*): Tổng số học sinh của lớp không bao gồm các học sinh được miễn giảm môn tương ứng");
            dataSheet.SetFontName("Times New Roman", 0);
            dataSheet.PageMaginBottom = 0.5;
            dataSheet.PageMaginTop = 0.5;
            dataSheet.FitSheetOnOnePage = true;
            dataSheet.Name = sheetName;
        }
        #endregion
    }
}
