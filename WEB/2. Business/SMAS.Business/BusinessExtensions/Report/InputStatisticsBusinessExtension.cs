﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Linq.Expressions;
using System.Web;
using log4net;

using SMAS.DAL.Repository;
using SMAS.DAL.IRepository;
using SMAS.Business.IBusiness;
using SMAS.Models.Models;
using SMAS.Business.BusinessObject;
using SMAS.Business.Common;
using System.IO;
using SMAS.VTUtils.Excel.Export;
using System.Threading.Tasks;
using System.Data.Objects;
using System.Web.Configuration;
using System.Threading;
using Oracle.DataAccess.Client;
using SMAS.VTUtils.Log;

namespace SMAS.Business.Business
{
    public partial class InputStatisticsBusiness : GenericBussiness<ProcessedReport>, IInputStatisticsBusiness
    {
        private const string NUM_TASK_REPORT = "NumTaskReport";

        /// <summary>
        ///  Num task run, default 30 task
        /// </summary>
        private int NumTask
        {
            get
            {
                if (WebConfigurationManager.AppSettings["NumTaskReport"] != null)
                {
                    return Convert.ToInt32(WebConfigurationManager.AppSettings["NumTaskReport"]);
                }

                return 30;
            }
        }

        IProcessedReportParameterRepository ProcessedReportParameterRepository;
        IProcessedReportRepository ProcessedReportRepository;
        IPupilOfClassRepository PupilOfClassRepository;
        IEmployeeHistoryStatusRepository EmployeeHistoryStatusRepository;
        ISchoolProfileRepository SchoolProfileRepository;
        IClassSubjectRepository ClassSubjectRepository;
        ITeachingAssignmentRepository TeachingAssignmentRepository;
        IMarkRecordRepository MarkRecordRepository;
        IPupilRankingRepository PupilRankingRepository;
        IUserAccountRepository UserAccountRepository;
        IEmployeeRepository EmployeeRepository;

        public InputStatisticsBusiness(ILog logger, SMASEntities context = null)
            : base(logger)
        {
            if (context == null) { context = new SMASEntities(); }
            this.context = context;
            this.ProcessedReportParameterRepository = new ProcessedReportParameterRepository(context);
            this.ProcessedReportRepository = new ProcessedReportRepository(context);
            this.PupilOfClassRepository = new PupilOfClassRepository(context);
            this.EmployeeHistoryStatusRepository = new EmployeeHistoryStatusRepository(context);
            this.SchoolProfileRepository = new SchoolProfileRepository(context);
            this.ClassSubjectRepository = new ClassSubjectRepository(context);
            this.TeachingAssignmentRepository = new TeachingAssignmentRepository(context);
            this.MarkRecordRepository = new MarkRecordRepository(context);
            this.PupilRankingRepository = new PupilRankingRepository(context);
            this.UserAccountRepository = new UserAccountRepository(context);
            this.EmployeeRepository = new EmployeeRepository(context);
        }

        /// <summary>
        /// Lay danh sach chi tiet hien thi tren Grid
        /// </summary>
        /// <param name="UserAccountID">The user account ID.</param>
        /// <param name="SupervisingDeptID">The supervising dept ID.</param>
        /// <param name="DistrictID">The district ID.</param>
        /// <param name="Year">The year.</param>
        /// <param name="ReportDate">The report date.</param>
        /// <param name="AppliedLevel">The applied level.</param>
        /// <returns>
        /// List{InputStatistics}
        /// </returns>
        /// <date>12/4/2012</date>
        /// <author>Quanglm</author>
        /// <date>12/4/2012</date>
        public List<InputStatistics> DetailStatistics(int UserAccountID, int SupervisingDeptID, int DistrictID, int Year, DateTime? ReportDate, int AppliedLevel)
        {
            // Kiem tra du lieu dau vao
            // Bat buoc nhap ngay bao cao
            if (!ReportDate.HasValue)
            {
                Utils.ValidateRequire(string.Empty, "InputStatistics_Label_ReportedDate");
            }

            // SuppervisingDept
            bool deptCompatible = this.ProcessedReportRepository.ExistsRow(GlobalConstants.SCHOOL_SCHEMA, "SupervisingDept",
                new Dictionary<string, object>()
                {
                    { "SupervisingDeptID",SupervisingDeptID }

                }, null);

            if (!deptCompatible)
            {
                throw new BusinessException("Common_Validate_NotCompatible");
            }
            // Kiem tra quyen
            List<SupervisingDept> listSupervisingDept = SupervisingDeptBusiness.GetManagedUnitDirectly(UserAccountID);
            if (listSupervisingDept == null)
            {
                // Neu danh sach rong
                throw new BusinessException("Common_Validate_NotCompatible");
            }
            SupervisingDept sd = listSupervisingDept.Find(o => o.SupervisingDeptID == SupervisingDeptID);
            if (sd == null)
            {
                // SupervisingDeptID truyen vao khong co trong danh sach don vi cua user
                throw new BusinessException("Common_Validate_NotCompatible");
            }
            // Xu ly du lieu
            // Lay danh sach truong hoc theo quan huyen
            int grade = Utils.GradeToBinary(AppliedLevel);
            List<SchoolProfile> listSchool = (from s in SchoolProfileRepository.All
                                              where s.SupervisingDept.TraversalPath.Contains(sd.TraversalPath)
                                              && (DistrictID == 0 || s.DistrictID == DistrictID)
                                              && (s.EducationGrade & grade) != 0
                                              select s).ToList() as List<SchoolProfile>;

            // Lay danh sach so luong hs theo tung truong
            List<int> listStatus = new List<int>(){
                SystemParamsInFile.PUPIL_STATUS_STUDYING, SystemParamsInFile.PUPIL_STATUS_GRADUATED
            };
            var numberOfPupil = (from s in
                                     (from p in PupilOfClassRepository.All
                                      where
                                          p.SchoolProfile.SupervisingDept.TraversalPath.Contains(sd.TraversalPath)
                                          && p.Year == Year
                                          && listStatus.Contains(p.Status)
                                          && p.ClassProfile.EducationLevel.Grade == AppliedLevel
                                      select new { p.PupilID, p.SchoolID }).Distinct()
                                 group s by s.SchoolID into pupilInSchool
                                 select new
                                 {
                                     SchoolID = pupilInSchool.Key,
                                     CountPupil = pupilInSchool.Count()
                                 }).ToList();

            // Lay danh sach so luong giao vien theo truong
            var numberOfTeacher = (from ehs in EmployeeHistoryStatusRepository.All
                                   join sp in SchoolProfileRepository.All on ehs.SchoolID equals sp.SchoolProfileID
                                   where ehs.FromDate.Year <= Year
                                   && (!ehs.ToDate.HasValue || ehs.ToDate.Value.Year >= Year)
                                   && sp.SupervisingDept.TraversalPath.Contains(sd.TraversalPath)
                                   && ehs.EmployeeStatus == SystemParamsInFile.EMPLOYMENT_STATUS_WORKING
                                   && ehs.AppliedLevel == AppliedLevel
                                   group sp by sp.SchoolProfileID into employeeInSchool
                                   select new
                                   {
                                       SchoolID = employeeInSchool.Key,
                                       CountEmployee = employeeInSchool.Count()
                                   }).ToList();

            // Lay danh sach so lop theo truong
            var numberOfClass = (from s in
                                     (from p in PupilOfClassRepository.All
                                      where
                                          p.SchoolProfile.SupervisingDept.TraversalPath.Contains(sd.TraversalPath)
                                          && p.Year == Year
                                          && listStatus.Contains(p.Status)
                                          && p.ClassProfile.EducationLevel.Grade == AppliedLevel
                                      select new { p.SchoolID, p.ClassID }).Distinct()
                                 group s by s.SchoolID into classInSchool
                                 select new
                                 {
                                     SchoolID = classInSchool.Key,
                                     CountClass = classInSchool.Count()
                                 }).ToList();

            // Danh sach so luong lop da khai bao mon hoc
            var numberOfClassHasSubject = (from s in
                                               (from p in PupilOfClassRepository.All
                                                join cs in ClassSubjectRepository.All on p.ClassID equals cs.ClassID
                                                where
                                                    p.SchoolProfile.SupervisingDept.TraversalPath.Contains(sd.TraversalPath)
                                                    && p.Year == Year
                                                    && listStatus.Contains(p.Status)
                                                    && p.ClassProfile.EducationLevel.Grade == AppliedLevel
                                                select new { p.SchoolID, p.ClassID }).Distinct()
                                           group s by s.SchoolID into classInSchool
                                           select new
                                           {
                                               SchoolID = classInSchool.Key,
                                               CountClass = classInSchool.Count()
                                           }).ToList();

            // Cac lop da phan cong giang day trong ky
            // Lay ra list chung sau do se lay ra ky 1 va ly 2
            List<int> listSemester = new List<int>()
            {
                SystemParamsInFile.SEMESTER_OF_YEAR_FIRST,
                SystemParamsInFile.SEMESTER_OF_YEAR_SECOND
            };
            var numberOfClassHasAssignBySemester = (from s in
                                                        (from p in PupilOfClassRepository.All
                                                         join ta in TeachingAssignmentRepository.All on p.ClassID equals ta.ClassID
                                                         where
                                                             p.SchoolProfile.SupervisingDept.TraversalPath.Contains(sd.TraversalPath)
                                                             && p.Year == Year
                                                             && listStatus.Contains(p.Status)
                                                             && p.ClassProfile.EducationLevel.Grade == AppliedLevel
                                                             && listSemester.Contains(ta.Semester.Value)
                                                         select new { p.SchoolID, p.ClassID, ta.Semester }).Distinct()
                                                    group s by new { s.SchoolID, s.Semester } into pupilInSchool
                                                    select new
                                                    {
                                                        SchoolID = pupilInSchool.Key.SchoolID,
                                                        Semester = pupilInSchool.Key.Semester,
                                                        CountClass = pupilInSchool.Count()
                                                    }).ToList();
            // Danh sach so luong lop da nhap diem theo truong theo ky
            // Lay chung va select rieng tung ky sau
            var numberOfClassHasMarkBySemester = (from s in
                                                      (from p in PupilOfClassRepository.All
                                                       join mr in MarkRecordRepository.All on p.ClassID equals mr.ClassID
                                                       where
                                                           p.SchoolProfile.SupervisingDept.TraversalPath.Contains(sd.TraversalPath)
                                                           && p.Year == Year
                                                           && listStatus.Contains(p.Status)
                                                           && p.ClassProfile.EducationLevel.Grade == AppliedLevel
                                                           && listSemester.Contains(mr.Semester.Value)
                                                       select new { p.SchoolID, p.ClassID, mr.Semester }).Distinct()
                                                  group s by new { s.SchoolID, s.Semester } into pupilInSchool
                                                  select new
                                                  {
                                                      SchoolID = pupilInSchool.Key.SchoolID,
                                                      Semester = pupilInSchool.Key.Semester,
                                                      CountClass = pupilInSchool.Count()
                                                  }).ToList();

            // Danh sach so luong lop da xep loai hanh kiem theo truong theo ky
            // Lay chung va select rieng tung ky sau
            var numberOfClassHasConductBySemester = (from s in
                                                         (from p in PupilOfClassRepository.All
                                                          join pr in PupilRankingRepository.All on p.ClassID equals pr.ClassID
                                                          where
                                                              p.SchoolProfile.SupervisingDept.TraversalPath.Contains(sd.TraversalPath)
                                                              && p.Year == Year
                                                              && listStatus.Contains(p.Status)
                                                              && p.ClassProfile.EducationLevel.Grade == AppliedLevel
                                                              && listSemester.Contains(pr.Semester.Value)
                                                              && pr.ConductLevelID.HasValue
                                                          select new { p.SchoolID, p.ClassID, pr.Semester }).Distinct()
                                                     group s by new { s.SchoolID, s.Semester } into pupilInSchool
                                                     select new
                                                     {
                                                         SchoolID = pupilInSchool.Key.SchoolID,
                                                         Semester = pupilInSchool.Key.Semester,
                                                         CountClass = pupilInSchool.Count()
                                                     }).ToList();

            // Danh sach so luong lop da tong ket diem theo ky
            var numberOfClassHasSumMarkBySemester = (from s in
                                                         (from p in PupilOfClassRepository.All
                                                          join pr in PupilRankingRepository.All on p.ClassID equals pr.ClassID
                                                          where
                                                              p.SchoolProfile.SupervisingDept.TraversalPath.Contains(sd.TraversalPath)
                                                              && p.Year == Year
                                                              && listStatus.Contains(p.Status)
                                                              && p.ClassProfile.EducationLevel.Grade == AppliedLevel
                                                              && listSemester.Contains(pr.Semester.Value)
                                                              && pr.AverageMark.HasValue
                                                          select new { p.SchoolID, p.ClassID, pr.Semester }).Distinct()
                                                     group s by new { s.SchoolID, s.Semester } into pupilInSchool
                                                     select new
                                                     {
                                                         SchoolID = pupilInSchool.Key.SchoolID,
                                                         Semester = pupilInSchool.Key.Semester,
                                                         CountClass = pupilInSchool.Count()
                                                     }).ToList();
            // Danh sach so luong user thuoc truong
            var numberOfUser = (from us in
                                    (from e in EmployeeRepository.All
                                     join ua in UserAccountRepository.All on e.EmployeeID equals ua.EmployeeID
                                     where (!ReportDate.HasValue || DateTime.Compare(ua.CreatedDate.Value, ReportDate.Value) <= 0)
                                 && ua.IsActive == true
                                 && e.SchoolProfile.SupervisingDept.TraversalPath.Contains(sd.TraversalPath)
                                     select new { e.SchoolID, ua.UserAccountID }).Distinct()
                                group us by us.SchoolID into userInSchool
                                select new
                                {
                                    SchoolID = userInSchool.Key,
                                    CountUser = userInSchool.Count()
                                }).ToList();

            // Danh sach so luong user da dang nhap
            var numberOfLoggedUser = (from us in
                                          (from e in EmployeeRepository.All
                                           join ua in UserAccountRepository.All on e.EmployeeID equals ua.EmployeeID
                                           where (!ReportDate.HasValue || DateTime.Compare(ua.FirstLoginDate.Value, ReportDate.Value) <= 0)
                                       && ua.IsActive == true
                                       && e.SchoolProfile.SupervisingDept.TraversalPath.Contains(sd.TraversalPath)
                                           select new { e.SchoolID, ua.UserAccountID }).Distinct()
                                      group us by us.SchoolID into userInSchool
                                      select new
                                      {
                                          SchoolID = userInSchool.Key,
                                          CountUser = userInSchool.Count()
                                      }).ToList();

            // Danh sach so luong hoc sinh da nhap so dien thoai
            var numberOfPupilHasMobile = (from s in
                                              (from p in PupilOfClassRepository.All
                                               where
                                                   p.SchoolProfile.SupervisingDept.TraversalPath.Contains(sd.TraversalPath)
                                                   && p.Year == Year
                                                   && listStatus.Contains(p.Status)
                                                   && p.ClassProfile.EducationLevel.Grade == AppliedLevel
                                                   && p.PupilProfile.Mobile != null && p.PupilProfile.Mobile != string.Empty
                                               select new { p.SchoolID, p.PupilID }).Distinct()
                                          group s by s.SchoolID into pupilInSchool
                                          select new
                                          {
                                              SchoolID = pupilInSchool.Key,
                                              CountPupil = pupilInSchool.Count()
                                          }).ToList();
            // Danh sach so luong hoc sinh da nhap so dien thoai cua cha theo truong
            var numberOfPupilHasFatherMobile = (from s in
                                                    (from p in PupilOfClassRepository.All
                                                     where
                                                         p.SchoolProfile.SupervisingDept.TraversalPath.Contains(sd.TraversalPath)
                                                         && p.Year == Year
                                                         && listStatus.Contains(p.Status)
                                                         && p.ClassProfile.EducationLevel.Grade == AppliedLevel
                                                         && p.PupilProfile.FatherMobile != null && p.PupilProfile.FatherMobile != string.Empty
                                                     select new { p.SchoolID, p.PupilID }).Distinct()
                                                group s by s.SchoolID into pupilInSchool
                                                select new
                                                {
                                                    SchoolID = pupilInSchool.Key,
                                                    CountPupil = pupilInSchool.Count()
                                                }).ToList();
            // Danh sach so luong hoc sinh da nhap so dien thoai cua me theo truong
            var numberOfPupilHasMotherMobile = (from s in
                                                    (from p in PupilOfClassRepository.All
                                                     where
                                                         p.SchoolProfile.SupervisingDept.TraversalPath.Contains(sd.TraversalPath)
                                                         && p.Year == Year
                                                         && listStatus.Contains(p.Status)
                                                         && p.ClassProfile.EducationLevel.Grade == AppliedLevel
                                                         && p.PupilProfile.MotherMobile != null && p.PupilProfile.MotherMobile != string.Empty
                                                     select new { p.SchoolID, p.PupilID }).Distinct()
                                                group s by s.SchoolID into pupilInSchool
                                                select new
                                                {
                                                    SchoolID = pupilInSchool.Key,
                                                    CountPupil = pupilInSchool.Count()
                                                }).ToList();

            List<InputStatistics> listInputStatistics = new List<InputStatistics>();
            foreach (SchoolProfile school in listSchool)
            {
                int schoolID = school.SchoolProfileID;
                InputStatistics inputStatistics = new InputStatistics();
                if (school.District != null)
                {
                    inputStatistics.DistrictID = school.District.DistrictID;
                    inputStatistics.DistrictName = school.District.DistrictName;
                }
                inputStatistics.SchoolID = schoolID;
                inputStatistics.SchoolName = school.SchoolName;
                // SL hoc sinh
                var pupilInSchool = numberOfPupil.Where(o => o.SchoolID == schoolID).FirstOrDefault();
                inputStatistics.NumberOfPupil = pupilInSchool != null ? pupilInSchool.CountPupil : 0;
                // SL giao vien
                var teacherInSchool = numberOfTeacher.Where(o => o.SchoolID == schoolID).FirstOrDefault();
                inputStatistics.NumberOfTeacher = teacherInSchool != null ? teacherInSchool.CountEmployee : 0;
                // SL lop hoc
                var classInSchool = numberOfClass.Where(o => o.SchoolID == schoolID).FirstOrDefault();
                inputStatistics.NumberOfClass = classInSchool != null ? classInSchool.CountClass : 0;
                // SL lop hoc da co mon hoc
                var classHasSubjectInSchool = numberOfClassHasSubject.Where(o => o.SchoolID == schoolID).FirstOrDefault();
                inputStatistics.NumberOfClassHasSubject = classHasSubjectInSchool != null ? classHasSubjectInSchool.CountClass : 0;
                // SL lop hoc da duoc phan cong giang day
                // Ky 1
                var classAsignmentFirst = numberOfClassHasAssignBySemester.Where(o => o.SchoolID == schoolID)
                    .Where(o => (o.Semester.HasValue && o.Semester.Value == SystemParamsInFile.SEMESTER_OF_YEAR_FIRST)).FirstOrDefault();
                inputStatistics.NumberOfClassHasAssignForFirstSemester = classAsignmentFirst != null ? classAsignmentFirst.CountClass : 0;
                // Ky 2
                var classAsignmentSecond = numberOfClassHasAssignBySemester.Where(o => o.SchoolID == schoolID)
                    .Where(o => (o.Semester.HasValue && o.Semester.Value == SystemParamsInFile.SEMESTER_OF_YEAR_SECOND)).FirstOrDefault();
                inputStatistics.NumberOfClassHasAssignForSecondSemester = classAsignmentSecond != null ? classAsignmentSecond.CountClass : 0;
                // SL lop da co diem
                // Ky 1
                var classMarkFirst = numberOfClassHasMarkBySemester.Where(o => o.SchoolID == schoolID)
                    .Where(o => (o.Semester.HasValue && o.Semester.Value == SystemParamsInFile.SEMESTER_OF_YEAR_FIRST)).FirstOrDefault();
                inputStatistics.NumberOfClassHasMarkForFirstSemester = classMarkFirst != null ? classMarkFirst.CountClass : 0;
                // Ky 2
                var classMarkSecond = numberOfClassHasMarkBySemester.Where(o => o.SchoolID == schoolID)
                    .Where(o => (o.Semester.HasValue && o.Semester.Value == SystemParamsInFile.SEMESTER_OF_YEAR_SECOND)).FirstOrDefault();
                inputStatistics.NumberOfClassHasMarkForSecondSemester = classMarkSecond != null ? classMarkSecond.CountClass : 0;
                // SL lop da xep hanh kiem
                // Ky 1
                var classConductFirst = numberOfClassHasConductBySemester.Where(o => o.SchoolID == schoolID)
                    .Where(o => (o.Semester.HasValue && o.Semester.Value == SystemParamsInFile.SEMESTER_OF_YEAR_FIRST)).FirstOrDefault();
                inputStatistics.NumberOfClassHasConductForFirstSemester = classConductFirst != null ? classConductFirst.CountClass : 0;
                // Ky 2
                var classConductSecond = numberOfClassHasConductBySemester.Where(o => o.SchoolID == schoolID)
                    .Where(o => (o.Semester.HasValue && o.Semester.Value == SystemParamsInFile.SEMESTER_OF_YEAR_SECOND)).FirstOrDefault();
                inputStatistics.NumberOfClassHasConductForSecondSemester = classConductSecond != null ? classConductSecond.CountClass : 0;
                // SL lop da co tong ket diem
                // Ky 1
                var classSumMarkFirst = numberOfClassHasSumMarkBySemester.Where(o => o.SchoolID == schoolID)
                    .Where(o => (o.Semester.HasValue && o.Semester.Value == SystemParamsInFile.SEMESTER_OF_YEAR_FIRST)).FirstOrDefault();
                inputStatistics.NumberOfClassHasSumMarkForFirstSemester = classSumMarkFirst != null ? classSumMarkFirst.CountClass : 0;
                // Ky 2
                var classSumMarkSecond = numberOfClassHasSumMarkBySemester.Where(o => o.SchoolID == schoolID)
                    .Where(o => (o.Semester.HasValue && o.Semester.Value == SystemParamsInFile.SEMESTER_OF_YEAR_SECOND)).FirstOrDefault();
                inputStatistics.NumberOfClassHasSumMarkForSecondSemester = classSumMarkSecond != null ? classSumMarkSecond.CountClass : 0;
                // SL user trong truong
                var userInSchool = numberOfUser.Where(o => o.SchoolID == schoolID).FirstOrDefault();
                inputStatistics.NumberOfUser = userInSchool != null ? userInSchool.CountUser : 0;
                // SL user da login vao he thong
                var userLoggedInSchool = numberOfLoggedUser.Where(o => o.SchoolID == schoolID).FirstOrDefault();
                inputStatistics.NumberOfLoggedUser = userLoggedInSchool != null ? userLoggedInSchool.CountUser : 0;
                // SL hoc sinh da nhap sdt
                var userMobile = numberOfPupilHasMobile.Where(o => o.SchoolID == schoolID).FirstOrDefault();
                inputStatistics.NumberOfPupilHasMobile = userMobile != null ? userMobile.CountPupil : 0;
                // SL hoc sinh da nhap sdt cho me
                var userMotherMobile = numberOfPupilHasMotherMobile.Where(o => o.SchoolID == schoolID).FirstOrDefault();
                inputStatistics.NumberOfPupilHasMotherMobile = userMotherMobile != null ? userMotherMobile.CountPupil : 0;
                // SL hoc sinh da nhap sdt cho cha
                var userFatherMobile = numberOfPupilHasFatherMobile.Where(o => o.SchoolID == schoolID).FirstOrDefault();
                inputStatistics.NumberOfPupilHasFatherMobile = userFatherMobile != null ? userFatherMobile.CountPupil : 0;
                listInputStatistics.Add(inputStatistics);
            }
            return listInputStatistics;
        }

        /// <summary>
        /// Xuat bao cao chi tiet
        /// </summary>
        /// <param name="UserAccountID">The user account ID.</param>
        /// <param name="SupervisingDeptID">The supervising dept ID.</param>
        /// <param name="DistrictID">The district ID.</param>
        /// <param name="Year">The year.</param>
        /// <param name="ReportDate">The report date.</param>
        /// <param name="AppliedLevel">The applied level.</param>
        /// <returns>
        /// Stream
        /// </returns>
        /// <date>12/4/2012</date>
        /// <author>Quanglm</author>
        /// <date>12/4/2012</date>
        public ProcessedReport ExportDetailStatistics(int UserAccountID, int SupervisingDeptID, int DistrictID,
            int Year, DateTime? ReportDate, int AppliedLevel)
        {
            #region Xuat excel
            string reportCode = SystemParamsInFile.REPORT_THONGKE_CHITIET_TINHHINH_NHAPLIEU;
            ReportDefinition reportDef = ReportDefinitionBusiness.GetByCode(reportCode);
            string templatePath = ReportUtils.GetTemplatePath(reportDef);
            IVTWorkbook oBook = VTExport.OpenWorkbook(templatePath);

            int firstRow = 7;
            int lastColumn = VTVector.dic['U'];
            int firstColumnSum = VTVector.dic['D'];

            List<InputStatistics> listInputStatistics = this.DetailStatistics(UserAccountID, SupervisingDeptID, DistrictID,
            Year, ReportDate, AppliedLevel);

            // Thiet lap template ban dau
            IVTWorksheet firstSheet = oBook.GetSheet(1);

            // Lay cac sheet mau          
            IVTRange topRange = firstSheet.GetRange(firstRow, 1, firstRow, lastColumn);
            IVTRange midRange = firstSheet.GetRange(firstRow + 1, 1, firstRow + 1, lastColumn);
            IVTRange lastRange = firstSheet.GetRange(firstRow + 2, 1, firstRow + 2, lastColumn);
            IVTRange sumRange = firstSheet.GetRange(firstRow + 3, 1, firstRow + 3, lastColumn);

            //Tạo template chuẩn
            IVTWorksheet dataSheet = oBook.CopySheetToLast(firstSheet, "U" + (firstRow - 1));

            // Thong tin phan header
            Dictionary<string, object> dicGeneralInfo = new Dictionary<string, object>() {
                {"SchoolLevel", AppliedLevel},
                {"AcademicYearTitle", Year + " - " + (Year + 1)}
            };

            dataSheet.GetRange(1, 1, firstRow - 1, lastColumn).FillVariableValue(dicGeneralInfo);

            int beginRow = firstRow;
            int endRow = beginRow + listInputStatistics.Count - 1;
            for (int i = beginRow; i <= endRow; i++)
            {
                IVTRange copyRange;
                if (i == beginRow)
                {
                    copyRange = topRange;
                }
                else if (i == endRow)
                {
                    copyRange = lastRange;
                }
                else
                {
                    copyRange = midRange;
                }
                dataSheet.CopyPasteSameSize(copyRange, "A" + i);
            }

            // Fill du lieu
            List<int> listBeginRow = new List<int>();
            List<object> listData = new List<object>();
            int index = 0;
            foreach (InputStatistics inputStatistics in listInputStatistics)
            {
                listBeginRow.Add(beginRow + index);
                index++;
                Dictionary<string, object> dicData = new Dictionary<string, object>();

                dicData["Order"] = index;
                dicData["DistrictName"] = inputStatistics.DistrictName;
                dicData["SchoolName"] = inputStatistics.SchoolName;
                dicData["NumberOfPupil"] = inputStatistics.NumberOfPupil;
                dicData["NumberOfTeacher"] = inputStatistics.NumberOfTeacher;
                dicData["NumberOfClass"] = inputStatistics.NumberOfClass;
                dicData["NumberOfClassHasSubject"] = inputStatistics.NumberOfClassHasSubject;
                dicData["NumberOfClassAsignmentFirstSemester"] = inputStatistics.NumberOfClassHasAssignForFirstSemester;
                dicData["NumberOfClassAsignmentSecondSemester"] = inputStatistics.NumberOfClassHasAssignForSecondSemester;
                dicData["NumberOfClassHasMarkFirstSemester"] = inputStatistics.NumberOfClassHasMarkForFirstSemester;
                dicData["NumberOfClassHasMarkSecondSemester"] = inputStatistics.NumberOfClassHasMarkForSecondSemester;
                dicData["NumberOfClassHasConductFirstSemester"] = inputStatistics.NumberOfClassHasConductForFirstSemester;
                dicData["NumberOfClassHasConductSecondSemester"] = inputStatistics.NumberOfClassHasConductForSecondSemester;
                dicData["NumberOfClassSumMarkFirstSemester"] = inputStatistics.NumberOfClassHasSumMarkForFirstSemester;
                dicData["NumberOfClassSumMarkSecondSemester"] = inputStatistics.NumberOfClassHasSumMarkForSecondSemester;
                dicData["NumberOfUser"] = inputStatistics.NumberOfUser;
                dicData["NumberOfLoggedUser"] = inputStatistics.NumberOfLoggedUser;
                dicData["NumberOfPupilHasMobile"] = inputStatistics.NumberOfPupilHasMobile;
                dicData["NumberOfPupilHasParentMobile"] = inputStatistics.NumberOfPupilHasFatherMobile + inputStatistics.NumberOfPupilHasMotherMobile;
                dicData["NumberOfPupilHasFatherMobile"] = inputStatistics.NumberOfPupilHasFatherMobile;
                dicData["NumberOfPupilHasMotherMobile"] = inputStatistics.NumberOfPupilHasMotherMobile;

                listData.Add(dicData);
            }
            // Fill du lieu
            dataSheet.GetRange(beginRow, 1, endRow, lastColumn).FillVariableValue(new Dictionary<string, object>
                {
                    {"list",listData}
                });

            // Lay thong tin ve cot sum
            dataSheet.CopyPasteSameSize(sumRange, endRow + 1, 1);
            string formulaSum = "=SUM(#" + string.Join(";#", listBeginRow.ToArray<int>()) + ")";
            for (int i = firstColumnSum; i <= lastColumn; i++)
            {
                dataSheet.SetFormulaValue(endRow + 1, i, formulaSum.Replace("#", VTVector.ColumnIntToString(i)));
            }
            dataSheet.GetRange(listData.Count + 7, 1, listData.Count + 7, lastColumn).SetBorder(VTBorderStyle.Solid, VTBorderWeight.Thin, VTBorderIndex.All);
            dataSheet.GetRange(listData.Count + 7, 1, listData.Count + 7, 3).Merge(true);
            dataSheet.GetRange(listData.Count + 7, 1, listData.Count + 7, 3).Value = "Tổng số";
            dataSheet.GetRange(listData.Count + 7, 1, listData.Count + 7, 3).SetFontStyle(true, System.Drawing.Color.Black, false, 11, false, false);

            //Xoá sheet template
            firstSheet.Delete();
            Stream data = oBook.ToStream();
            #endregion

            #region Luu du lieu vao DB
            ProcessedReport pr = new ProcessedReport();
            pr.ReportCode = reportCode;
            pr.ProcessedDate = DateTime.Now;
            IDictionary<string, object> dic = new Dictionary<string, object> {
                 {"UserAccountID", UserAccountID},
                 {"SupervisingDeptID", SupervisingDeptID},
                 {"DistrictID", DistrictID},
                 {"Year", Year},
                 {"ReportDate", ReportDate},
                 {"AppliedLevel", AppliedLevel}
            };
            pr.InputParameterHashKey = ReportUtils.GetHashKey(dic);
            pr.ReportData = ReportUtils.Compress(data);

            //Tạo tên file
            string outputNamePattern = reportDef.OutputNamePattern;
            string titleReport = "All";
            SupervisingDept supervisingDept = SupervisingDeptBusiness.Find(SupervisingDeptID);
            if (supervisingDept.HierachyLevel == SystemParamsInFile.EDUCATION_HIERACHY_LEVEL_DISTRICT_OFFICE)
            {
                titleReport = "PGD";
            }
            else if (supervisingDept.HierachyLevel == SystemParamsInFile.EDUCATION_HIERACHY_LEVEL_PROVINCE_OFFICE)
            {
                titleReport = "SGD";
            }
            outputNamePattern = outputNamePattern.Replace("[PGD/SGD]", titleReport);
            outputNamePattern = outputNamePattern.Replace("[SchoolLevel]", AppliedLevel.ToString());
            pr.ReportName = ReportUtils.RemoveSpecialCharacters(outputNamePattern) + "." + reportDef.OutputFormat;
            ProcessedReportParameterRepository.Insert(dic, pr);
            ProcessedReportRepository.Insert(pr);
            ProcessedReportRepository.Save();
            #endregion
            data.Position = 0;
            return pr;
        }

        #region New Synthesis
        /// <summary>
        /// Tong hop moi
        /// </summary>
        /// <param name="dic"></param>
        /// <returns></returns>
        public IQueryable<ReportRequestBO> SearchSchoolReport(IDictionary<string, object> dic)
        {
            int year = Utils.GetInt(dic, "Year");
            int provinceID = Utils.GetInt(dic, "ProvinceID");
            int districtID = Utils.GetInt(dic, "DistrictID");
            int appliedLevel = Utils.GetInt(dic, "EducationGradeID");
            int semesterID = Utils.GetInt(dic, "SemesterID");
            bool subRole = Utils.GetBool(dic, "IsSubDeptRole", false);
            int supervisingDeptID = Utils.GetInt(dic, "SupervisingDeptID");
            int gradeBinary = appliedLevel > 0 ? Utils.GradeToBinary(appliedLevel) : 0;

            int primaryGrade = 0;
            int secondGrade = 0;

            if (subRole && gradeBinary == 0)
            {
                primaryGrade = Utils.GradeToBinary(SystemParamsInFile.APPLIED_LEVEL_PRIMARY);
                secondGrade = Utils.GradeToBinary(SystemParamsInFile.APPLIED_LEVEL_SECONDARY);
            }

            SupervisingDept superDept = SupervisingDeptBusiness.Find(supervisingDeptID);
            if (superDept.HierachyLevel == SystemParamsInFile.EDUCATION_HIERACHY_LEVEL_DISTRICT_OFFICE_DEPARTMENT || superDept.HierachyLevel == SystemParamsInFile.EDUCATION_HIERACHY_LEVEL_PROVINCE_OFFICE_DEPARTMENT)
            {
                supervisingDeptID = superDept.ParentID.Value;
            }

            var schoolProfile = from sp in SchoolProfileBusiness.AllNoTracking
                                where sp.IsActive == true
                                      && (
                                      // So
                                      (subRole == false && (sp.DistrictID != null && ((districtID > 0 && sp.DistrictID.Value == districtID) || (districtID == 0))) // chi quan tam truong co quan huyen
                                      && sp.ProvinceID == provinceID && (sp.SupervisingDeptID == supervisingDeptID || sp.SupervisingDept.ParentID == supervisingDeptID))
                                      // Phong
                                      || (subRole && (sp.SupervisingDeptID == supervisingDeptID || sp.SupervisingDept.ParentID == supervisingDeptID)))
                                select new
                                {
                                    sp.SchoolProfileID,
                                    sp.SchoolName,
                                    ProvinceID = sp.ProvinceID.Value,
                                    DistrictID = sp.DistrictID.Value,
                                    sp.AdminID,
                                    sp.EducationGrade,
                                    Year = year
                                };

            if (gradeBinary > 0)
            {
                schoolProfile = schoolProfile.Where(o => (o.EducationGrade & gradeBinary) != 0);
            }

            IQueryable<ReportRequestBO> lstReportRequest = from sp in schoolProfile
                                                           join ur in UserRoleBusiness.AllNoTracking on sp.AdminID equals ur.UserID
                                                           join r in RoleBusiness.AllNoTracking on ur.RoleID equals r.RoleID
                                                           join a in AcademicYearBusiness.AllNoTracking.Where(o => o.Year == year && o.IsActive == true) on sp.SchoolProfileID equals a.SchoolID
                                                           where r.RoleID >= 5 && r.RoleID <= 7
                                                           select new ReportRequestBO
                                                           {
                                                               SchoolID = sp.SchoolProfileID,
                                                               ProvinceID = sp.ProvinceID,
                                                               DistrictID = sp.DistrictID,
                                                               SemesterID = semesterID,
                                                               EducationGrade = r.RoleID == 5 ? 3 : r.RoleID == 6 ? 2 : 1,
                                                               AcademicYearID = a.AcademicYearID,
                                                               Year = year,
                                                               CreateTime = DateTime.Now,
                                                               Status = false
                                                           };

            if (appliedLevel > 0)
            {
                lstReportRequest = lstReportRequest.Where(p => p.EducationGrade == appliedLevel);
            }
            else
            {
                if (subRole)
                {
                    lstReportRequest = lstReportRequest.Where(p => p.EducationGrade == SystemParamsInFile.APPLIED_LEVEL_PRIMARY || p.EducationGrade == SystemParamsInFile.APPLIED_LEVEL_SECONDARY);
                }
            }

            /*List<ReportRequestBO> listReport = lstReportRequest.ToList();
            List<int> listSchoolID = listReport.Select(o => o.SchoolID).ToList();
            for (int i = 0; i < listSchoolID.Count; i++)
            {
                int schoolID = listSchoolID[i];
                AcademicYear aca = (from a in AcademicYearBusiness.All.Where(o => o.SchoolID == schoolID && o.Year == year)
                                    select a).FirstOrDefault();
                if (aca != null)
                {
                    listReport[i].AcademicYearID = aca.AcademicYearID;
                }
            }*/

            return lstReportRequest;
        }

        #endregion

        #region Search ReportInputMonitorSituation

        /// <summary>
        /// Tim kiem thong ke tinh hinh nhap lieu
        /// </summary>
        /// <author date="150703">QuangNN2</param>
        /// <param name="total"></param>
        /// <returns></returns>
        public List<ReportInputMonitorBO> SearchInput(IDictionary<string, object> dic, ref int total)
        {
            int provinceID = Utils.GetInt(dic, "ProvinceID");
            int districtID = Utils.GetInt(dic, "DistrictID");
            int appliedLevel = Utils.GetInt(dic, "AppliedLevel");
            int year = Utils.GetInt(dic, "Year");

            int currentPage = Utils.GetInt(dic, "currentPage");
            int pageSize = Utils.GetInt(dic, "pageSize");
            bool subRole = Utils.GetBool(dic, "subRole", false);
            int supervisingDeptID = Utils.GetInt(dic, "supervisingDeptID");

            IQueryable<ReportRequest> lstReport = ReportRequestBusiness.Search(dic);
            IQueryable<ReportRequestBO> queryReportInputStatis = (from rr in lstReport
                                                                  join sp in SchoolProfileBusiness.All on rr.SchoolID equals sp.SchoolProfileID
                                                                  select new ReportRequestBO
                                                                  {
                                                                      ReportRequestID = rr.ReportRequestID,
                                                                      SchoolID = rr.SchoolID,
                                                                      SchoolName = sp.SchoolName,
                                                                      ProvinceID = rr.ProvinceID,
                                                                      DistrictID = rr.DistrictID,
                                                                      SemesterID = rr.SemesterID,
                                                                      AcademicYearID = rr.AcademicYearID,
                                                                      Year = rr.Year,
                                                                      EducationGrade = rr.EducationGrade,
                                                                      CreateTime = rr.CreateTime,
                                                                      UpdateTime = rr.UpdateTime,
                                                                      Status = rr.Status

                                                                  });

            if (pageSize > 0 && currentPage > 0)
            {
                var temp = (from q in queryReportInputStatis
                            group q by new { q.DistrictID, q.SchoolName, q.SchoolID } into g
                            select g);

                total = temp.Count();
                var lstSchoolID = temp.OrderBy(s => s.Key.DistrictID).ThenBy(s => s.Key.SchoolName)
                                    .Skip((currentPage - 1) * pageSize).Take(pageSize)
                                    .Select(s => s.Key.SchoolID)
                                    .ToList();

                // Danh sach cac Truong de chay thong ke
                queryReportInputStatis = queryReportInputStatis.Where(o => lstSchoolID.Contains(o.SchoolID));
            }

            List<ReportInputMonitorBO> listResult = new List<ReportInputMonitorBO>();
            List<ReportRequestBO> lstReportInputStatis = queryReportInputStatis.ToList();
            ReportRequestBO requestBO = null;
            for (int i = 0; i < lstReportInputStatis.Count; i++)
            {
                // Danh sach IndicatorData
                requestBO = lstReportInputStatis[i];

                // Ghep list va return
            }

            return listResult;
        }

        #region Comment Old Code
        /// <summary>
        /// Tim kiem thong ke tinh hinh nhap lieu
        /// </summary>
        /// <author date="140424">HaiVT</author>
        /// <param name="dic"></param>
        /// <returns></returns>
        public List<ReportInputMonitorBO> SearchInputMonitor(IDictionary<string, object> dic, ref int total)
        {
            int provinceID = Utils.GetInt(dic, "ProvinceID");
            int districtID = Utils.GetInt(dic, "DistrictID");
            int year = Utils.GetInt(dic, "Year");
            int AppliedLevel = Utils.GetInt(dic, "AppliedLevel");

            int currentPage = Utils.GetInt(dic, "currentPage");
            int pageSize = Utils.GetInt(dic, "pageSize");
            bool groupByDistrict = Utils.GetBool(dic, "groupByDistrict", false);
            bool subRole = Utils.GetBool(dic, "subRole", false);
            int supervisingDeptID = Utils.GetInt(dic, "supervisingDeptID");

            int gradeBinary = AppliedLevel > 0 ? Utils.GradeToBinary(AppliedLevel) : 0;

            // User Phong thi chi co cap 1 va cap 2
            int primaryGrade = 0;
            int secondGrade = 0;
            if (subRole && gradeBinary == 0)
            {
                primaryGrade = Utils.GradeToBinary(SystemParamsInFile.APPLIED_LEVEL_PRIMARY);
                secondGrade = Utils.GradeToBinary(SystemParamsInFile.APPLIED_LEVEL_SECONDARY);
            }
            SupervisingDept superDept = SupervisingDeptBusiness.Find(supervisingDeptID);
            if (superDept.HierachyLevel == SystemParamsInFile.EDUCATION_HIERACHY_LEVEL_DISTRICT_OFFICE_DEPARTMENT || superDept.HierachyLevel == SystemParamsInFile.EDUCATION_HIERACHY_LEVEL_PROVINCE_OFFICE_DEPARTMENT)
            {
                supervisingDeptID = superDept.ParentID.Value;
            }
            var schoolProfile = from sp in SchoolProfileBusiness.AllNoTracking
                                where
                                    sp.IsActive == true
                                    &&
                                    (
                                        // So
                                        (subRole == false && (sp.DistrictID != null && ((districtID > 0 && sp.DistrictID.Value == districtID) || (districtID == 0))) // chi quan tam truong co quan huyen
                                    && sp.ProvinceID == provinceID && (sp.SupervisingDeptID == supervisingDeptID || sp.SupervisingDept.ParentID == supervisingDeptID))
                                    // Phong
                                    || (subRole && (sp.SupervisingDeptID == supervisingDeptID || sp.SupervisingDept.ParentID == supervisingDeptID))
                                    )
                                /* 
                                && (
                                        (gradeBinary > 0 && (sp.EducationGrade & gradeBinary) != 0) 
                                || (
                                        gradeBinary == 0 
                                        && (
                                            (subRole == false) 
                                            || (
                                                    subRole && ((sp.EducationGrade & primaryGrade) != 0) || ((sp.EducationGrade & secondGrade) != 0)
                                                )
                                            )
                                    )
                                )
                                */
                                select new
                                {
                                    sp.SchoolProfileID,
                                    sp.SchoolName,
                                    DistrictID = sp.DistrictID.Value,
                                    sp.AdminID,
                                    sp.EducationGrade
                                };

            if (gradeBinary > 0)
            {
                schoolProfile = schoolProfile.Where(o => (o.EducationGrade & gradeBinary) != 0);
            }

            if (pageSize > 0 && currentPage > 0)
            {
                if (groupByDistrict) // Thuc hien phan trang khi chon bao cao theo Phong/So
                {
                    List<int> lstDistrictId = (from p in schoolProfile
                                               group p by p.DistrictID into g
                                               select g.Key).OrderBy(p => p).Skip((currentPage - 1) * pageSize).Take(pageSize).ToList();

                    schoolProfile = schoolProfile.Where(p => lstDistrictId.Contains(p.DistrictID));

                    total = (from p in schoolProfile
                             orderby p.DistrictID
                             group p by p.DistrictID into g
                             select g.Key).Count();
                }
            }

            IQueryable<ReportInputMonitorBO> qryReportInputStatis = from sp in schoolProfile
                                                                    join ur in UserRoleBusiness.AllNoTracking on sp.AdminID equals ur.UserID
                                                                    join r in RoleBusiness.AllNoTracking on ur.RoleID equals r.RoleID
                                                                    where r.RoleID >= 5 && r.RoleID <= 7 // Chi lay thong tin cap 1 2 3     
                                                                    select new ReportInputMonitorBO
                                                                    {
                                                                        SchoolID = sp.SchoolProfileID,
                                                                        SchoolName = sp.SchoolName,
                                                                        DistrictID = sp.DistrictID,
                                                                        GradeID = r.RoleID == 5 ? 3 : r.RoleID == 6 ? 2 : 1
                                                                    };

            if (AppliedLevel > 0)
            {
                qryReportInputStatis = qryReportInputStatis.Where(p => p.GradeID == AppliedLevel);
            }
            else
            {
                if (subRole)
                {
                    qryReportInputStatis = qryReportInputStatis.Where(p => p.GradeID == SystemParamsInFile.APPLIED_LEVEL_PRIMARY || p.GradeID == SystemParamsInFile.APPLIED_LEVEL_SECONDARY);
                }
            }

            // TanND11: Fix bug 0015490 - Begin
            if (pageSize > 0 && currentPage > 0)
            {
                if (!groupByDistrict) // Thuc hien phan trang khi chon bao cao theo Truong
                {
                    var temp = (from q in qryReportInputStatis.ToList()
                                group q by new { q.DistrictID, q.SchoolName, q.SchoolID } into g
                                select g);
                    total = temp.Count();
                    var lstSchoolID = temp.OrderBy(s => s.Key.DistrictID).ThenBy(s => s.Key.SchoolName)
                                        .Skip((currentPage - 1) * pageSize).Take(pageSize)
                                        .Select(s => s.Key.SchoolID)
                                        .ToList();

                    // Danh sach cac Truong de chay thong ke
                    qryReportInputStatis = qryReportInputStatis.Where(s => lstSchoolID.Contains(s.SchoolID));
                }
            }
            // TanND11: Fix bug 0015490 - End

            List<ReportInputMonitorBO> lstReportInputStatis = qryReportInputStatis.ToList();
            if (lstReportInputStatis != null && lstReportInputStatis.Count > 0)
            {
                // Danh sach Quan/Huyen
                List<int> districtIDs = lstReportInputStatis.Select(p => p.DistrictID).Distinct().ToList();
                List<District> lstDistrict = DistrictBusiness.AllNoTracking.Where(P => districtIDs.Contains(P.DistrictID)).ToList();

                // Chay Async de Thong ke so lieu
                Task[] arrTask = new Task[NumTask];
                for (int z = 0; z < NumTask; z++)
                {
                    int currentIndex = z;
                    arrTask[z] = Task.Factory.StartNew(() =>
                    {
                        #region run task
                        SMASEntities smasContext = new SMASEntities();
                        List<ReportInputMonitorBO> lstReportRunTask = lstReportInputStatis.Where(p => p.SchoolID % NumTask == currentIndex).ToList();
                        if (lstReportRunTask != null && lstReportRunTask.Count > 0)
                        {
                            List<ReportInputMonitorBO> lstReportInTask = null;
                            ReportInputMonitorBO reportInput = null;
                            int numColumnMark = 0;
                            //DateTime dateTimeNow = DateTime.Now;
                            for (int i = lstReportRunTask.Count - 1; i >= 0; i--)
                            {
                                lstReportInTask = lstReportInputStatis.Where(p => p.SchoolID == lstReportRunTask[i].SchoolID).ToList();
                                for (int k = lstReportInTask.Count - 1; k >= 0; k--)
                                {
                                    reportInput = lstReportInTask[k];
                                    AcademicYear aca = smasContext.AcademicYear.AsNoTracking().Where(o => o.SchoolID == reportInput.SchoolID && o.Year == year).FirstOrDefault();
                                    if (reportInput != null)
                                    {
                                        #region thong ke so lieu cho tung truong theo cap hoc
                                        // Ten quan/huyen
                                        if (reportInput.DistrictID > 0)
                                        {
                                            reportInput.DistrictName = lstDistrict.Where(p => p.DistrictID == reportInput.DistrictID).Select(p => p.DistrictName).FirstOrDefault();
                                        }

                                        // So hoc sinh trong truong
                                        reportInput.TotalPupil = (from poc in smasContext.PupilOfClass.AsNoTracking()
                                                                  join pp in smasContext.PupilProfile.AsNoTracking() on poc.PupilID equals pp.PupilProfileID
                                                                  join cp in smasContext.ClassProfile.AsNoTracking() on poc.ClassID equals cp.ClassProfileID
                                                                  join el in smasContext.EducationLevel.AsNoTracking() on cp.EducationLevelID equals el.EducationLevelID
                                                                  where poc.SchoolID == reportInput.SchoolID
                                                                  && (poc.Status == SystemParamsInFile.PUPIL_STATUS_STUDYING || poc.Status == SystemParamsInFile.PUPIL_STATUS_GRADUATED)
                                                                  && poc.Year == year
                                                                  && pp.IsActive == true
                                                                  //&& EntityFunctions.TruncateTime(poc.AssignedDate) == EntityFunctions.TruncateTime(reportDate)
                                                                  && cp.SchoolID == reportInput.SchoolID
                                                                  && (el.Grade == reportInput.GradeID || el.Grade == 0)
                                                                  group poc by poc.PupilID into g
                                                                  select g.Key).Count();

                                        // So giao vien                                    
                                        if (aca != null)
                                        {
                                            reportInput.TotalTeacher = (from p in smasContext.Employee.AsNoTracking().Where(p => p.SchoolID == reportInput.SchoolID && p.IsActive == true)
                                                                        join q in smasContext.EmployeeHistoryStatus.AsNoTracking().Where(q => q.SchoolID == reportInput.SchoolID) on p.EmployeeID equals q.EmployeeID
                                                                        where (q.ToDate == null || q.ToDate >= aca.FirstSemesterStartDate) && q.FromDate <= aca.SecondSemesterEndDate && q.EmployeeStatus == SystemParamsInFile.EMPLOYMENT_STATUS_WORKING
                                                                        group p by p.EmployeeID into g
                                                                        select g.Key).Count();
                                        }
                                        else
                                        {
                                            reportInput.TotalTeacher = 0;
                                        }



                                        // So lop hoc
                                        reportInput.TotalClass = (from cp in smasContext.ClassProfile.AsNoTracking()
                                                                  join a in smasContext.AcademicYear.AsNoTracking() on cp.AcademicYearID equals a.AcademicYearID
                                                                  join el in smasContext.EducationLevel.AsNoTracking() on cp.EducationLevelID equals el.EducationLevelID
                                                                  where cp.SchoolID == reportInput.SchoolID
                                                                  && a.Year == year
                                                                  && a.SchoolID == reportInput.SchoolID
                                                                  && (el.Grade == reportInput.GradeID || el.Grade == 0)
                                                                  group cp by cp.ClassProfileID into g
                                                                  select g.Key).Count();

                                        // So lop khai bao mon hoc
                                        reportInput.TotalClassSubject = (from cp in smasContext.ClassProfile.AsNoTracking()
                                                                         join a in smasContext.AcademicYear.AsNoTracking() on cp.AcademicYearID equals a.AcademicYearID
                                                                         join cs in smasContext.ClassSubject.AsNoTracking() on cp.ClassProfileID equals cs.ClassID
                                                                         join el in smasContext.EducationLevel.AsNoTracking() on cp.EducationLevelID equals el.EducationLevelID
                                                                         where cp.SchoolID == reportInput.SchoolID && a.Year == year
                                                                         && a.SchoolID == reportInput.SchoolID
                                                                         && (el.Grade == reportInput.GradeID || el.Grade == 0)
                                                                         group cp by cp.ClassProfileID into g
                                                                         select g.Key).Count();

                                        // So lop da phan cong giang day HK1
                                        reportInput.TotalClassAssignment1 = (from ta in smasContext.TeachingAssignment.AsNoTracking()
                                                                             join a in smasContext.AcademicYear.AsNoTracking() on ta.AcademicYearID equals a.AcademicYearID
                                                                             join cp in smasContext.ClassProfile.AsNoTracking() on ta.ClassID equals cp.ClassProfileID
                                                                             join el in smasContext.EducationLevel.AsNoTracking() on cp.EducationLevelID equals el.EducationLevelID
                                                                             where ta.SchoolID == reportInput.SchoolID
                                                                                && a.Year == year
                                                                                && ta.IsActive
                                                                             && a.SchoolID == reportInput.SchoolID
                                                                             && ta.Semester == SystemParamsInFile.SEMESTER_OF_YEAR_FIRST
                                                                             && cp.SchoolID == reportInput.SchoolID
                                                                             && (el.Grade == reportInput.GradeID || el.Grade == 0)
                                                                             group cp by cp.ClassProfileID into g
                                                                             select g.Key).Count();

                                        // So lop da phan cong giang day HK2
                                        reportInput.TotalClassAssignment2 = (from ta in smasContext.TeachingAssignment.AsNoTracking()
                                                                             join a in smasContext.AcademicYear.AsNoTracking() on ta.AcademicYearID equals a.AcademicYearID
                                                                             join cp in smasContext.ClassProfile.AsNoTracking() on ta.ClassID equals cp.ClassProfileID
                                                                             join el in smasContext.EducationLevel.AsNoTracking() on cp.EducationLevelID equals el.EducationLevelID
                                                                             where ta.SchoolID == reportInput.SchoolID && a.Year == year
                                                                             && a.SchoolID == reportInput.SchoolID
                                                                                && ta.IsActive
                                                                             && ta.Semester == SystemParamsInFile.SEMESTER_OF_YEAR_SECOND
                                                                             && cp.SchoolID == reportInput.SchoolID
                                                                             && (el.Grade == reportInput.GradeID || el.Grade == 0)
                                                                             group cp by cp.ClassProfileID into g
                                                                             select g.Key).Count();

                                        #region code moi kiem tra lop da nhap diem chi can ton tai 1 record diem
                                        // HK 1
                                        var markRecord = (from cp in smasContext.ClassProfile.AsNoTracking()
                                                          join m in smasContext.MarkRecord.AsNoTracking() on cp.ClassProfileID equals m.ClassID
                                                          join pp in smasContext.PupilProfile.AsNoTracking() on m.PupilID equals pp.PupilProfileID
                                                          join a in smasContext.AcademicYear.AsNoTracking() on cp.AcademicYearID equals a.AcademicYearID
                                                          join el in smasContext.EducationLevel.AsNoTracking() on cp.EducationLevelID equals el.EducationLevelID
                                                          where
                                                          m.Last2digitNumberSchool == reportInput.SchoolID % 100
                                                          && m.Semester == SystemParamsInFile.SEMESTER_OF_YEAR_FIRST
                                                          && cp.SchoolID == reportInput.SchoolID
                                                          && a.Year == year
                                                          && a.SchoolID == reportInput.SchoolID
                                                          && (el.Grade == reportInput.GradeID || el.Grade == 0)
                                                          && pp.IsActive == true
                                                          group cp by cp.ClassProfileID into g
                                                          select g.Key).ToList();

                                        var judgeRecord = (from cp in smasContext.ClassProfile.AsNoTracking()
                                                           join j in smasContext.JudgeRecord.AsNoTracking() on cp.ClassProfileID equals j.ClassID
                                                           join pp in smasContext.PupilProfile.AsNoTracking() on j.PupilID equals pp.PupilProfileID
                                                           join a in smasContext.AcademicYear.AsNoTracking() on cp.AcademicYearID equals a.AcademicYearID
                                                           join el in smasContext.EducationLevel.AsNoTracking() on cp.EducationLevelID equals el.EducationLevelID
                                                           where
                                                           j.Last2digitNumberSchool == reportInput.SchoolID % 100
                                                           && j.Semester == SystemParamsInFile.SEMESTER_OF_YEAR_FIRST
                                                           && cp.SchoolID == reportInput.SchoolID
                                                           && a.Year == year
                                                           && a.SchoolID == reportInput.SchoolID
                                                           && (el.Grade == reportInput.GradeID || el.Grade == 0)
                                                           && pp.IsActive == true
                                                           group cp by cp.ClassProfileID into g
                                                           select g.Key).ToList();

                                        numColumnMark = (from m in markRecord
                                                         join j in judgeRecord on m equals j
                                                         group m by m into g
                                                         select g.Key).Count();

                                        numColumnMark += markRecord.Where(p => !judgeRecord.Contains(p)).Count();
                                        numColumnMark += judgeRecord.Where(p => !markRecord.Contains(p)).Count();
                                        reportInput.TotalClassMark1 = numColumnMark;

                                        // HK 2
                                        markRecord = (from cp in smasContext.ClassProfile.AsNoTracking()
                                                      join m in smasContext.MarkRecord.AsNoTracking() on cp.ClassProfileID equals m.ClassID
                                                      join pp in smasContext.PupilProfile.AsNoTracking() on m.PupilID equals pp.PupilProfileID
                                                      join a in smasContext.AcademicYear.AsNoTracking() on cp.AcademicYearID equals a.AcademicYearID
                                                      join el in smasContext.EducationLevel.AsNoTracking() on cp.EducationLevelID equals el.EducationLevelID
                                                      where
                                                      m.Last2digitNumberSchool == reportInput.SchoolID % 100
                                                      && m.Semester == SystemParamsInFile.SEMESTER_OF_YEAR_SECOND
                                                      && cp.SchoolID == reportInput.SchoolID
                                                      && a.Year == year
                                                      && a.SchoolID == reportInput.SchoolID
                                                      && (el.Grade == reportInput.GradeID || el.Grade == 0)
                                                      && pp.IsActive == true
                                                      group cp by cp.ClassProfileID into g
                                                      select g.Key).ToList();

                                        judgeRecord = (from cp in smasContext.ClassProfile.AsNoTracking()
                                                       join j in smasContext.JudgeRecord.AsNoTracking() on cp.ClassProfileID equals j.ClassID
                                                       join pp in smasContext.PupilProfile.AsNoTracking() on j.PupilID equals pp.PupilProfileID
                                                       join a in smasContext.AcademicYear.AsNoTracking() on cp.AcademicYearID equals a.AcademicYearID
                                                       join el in smasContext.EducationLevel.AsNoTracking() on cp.EducationLevelID equals el.EducationLevelID
                                                       where
                                                       j.Last2digitNumberSchool == reportInput.SchoolID % 100
                                                       && j.Semester == SystemParamsInFile.SEMESTER_OF_YEAR_SECOND
                                                       && cp.SchoolID == reportInput.SchoolID
                                                       && a.Year == year
                                                       && a.SchoolID == reportInput.SchoolID
                                                       && (el.Grade == reportInput.GradeID || el.Grade == 0)
                                                       && pp.IsActive == true
                                                       group cp by cp.ClassProfileID into g
                                                       select g.Key).ToList();

                                        numColumnMark = (from m in markRecord
                                                         join j in judgeRecord on m equals j
                                                         group m by m into g
                                                         select g.Key).Count();

                                        numColumnMark += markRecord.Where(p => !judgeRecord.Contains(p)).Count();
                                        numColumnMark += judgeRecord.Where(p => !markRecord.Contains(p)).Count();
                                        reportInput.TotalClassMark2 = numColumnMark;
                                        #endregion

                                        // So lop da nhap hanh kiem HK1
                                        reportInput.TotalClassConduct1 = (from pr in smasContext.PupilRanking.AsNoTracking()
                                                                          join pp in smasContext.PupilProfile.AsNoTracking() on pr.PupilID equals pp.PupilProfileID
                                                                          join cp in smasContext.ClassProfile.AsNoTracking() on pr.ClassID equals cp.ClassProfileID
                                                                          join a in smasContext.AcademicYear.AsNoTracking() on pr.AcademicYearID equals a.AcademicYearID
                                                                          join el in smasContext.EducationLevel.AsNoTracking() on cp.EducationLevelID equals el.EducationLevelID
                                                                          where pr.Last2digitNumberSchool == reportInput.SchoolID % 100
                                                                          && pr.Semester == SystemParamsInFile.SEMESTER_OF_YEAR_FIRST
                                                                          && pr.ConductLevelID != null
                                                                          && pp.IsActive == true
                                                                          && cp.SchoolID == reportInput.SchoolID
                                                                          && a.Year == year
                                                                          && a.SchoolID == reportInput.SchoolID
                                                                          && (el.Grade == reportInput.GradeID || el.Grade == 0)
                                                                          && pp.IsActive == true
                                                                          group pr by pr.ClassID into g
                                                                          select new
                                                                          {
                                                                              g.Key,
                                                                              //Num = g.Count()
                                                                          }).Count();

                                        //reportInput.TotalClassConduct1 = (from poc in pupilClass
                                        //                                  join cc in classConduct on poc.Key equals cc.Key
                                        //                                  where poc.Num == cc.Num
                                        //                                  select poc).Count();

                                        // So lop da nhap hanh kiem HK2
                                        reportInput.TotalClassConduct2 = (from pr in smasContext.PupilRanking.AsNoTracking()
                                                                          join pp in smasContext.PupilProfile.AsNoTracking() on pr.PupilID equals pp.PupilProfileID
                                                                          join cp in smasContext.ClassProfile.AsNoTracking() on pr.ClassID equals cp.ClassProfileID
                                                                          join a in smasContext.AcademicYear.AsNoTracking() on pr.AcademicYearID equals a.AcademicYearID
                                                                          join el in smasContext.EducationLevel.AsNoTracking() on cp.EducationLevelID equals el.EducationLevelID
                                                                          where pr.Last2digitNumberSchool == reportInput.SchoolID % 100
                                                                          && pr.Semester == SystemParamsInFile.SEMESTER_OF_YEAR_SECOND
                                                                          && pr.ConductLevelID != null
                                                                          && pp.IsActive == true
                                                                          && cp.SchoolID == reportInput.SchoolID
                                                                          && a.Year == year
                                                                          && a.SchoolID == reportInput.SchoolID
                                                                          && (el.Grade == reportInput.GradeID || el.Grade == 0)
                                                                          && pp.IsActive == true
                                                                          group pr by pr.ClassID into g
                                                                          select new
                                                                          {
                                                                              g.Key,
                                                                              //Num = g.Count()
                                                                          }).Count();

                                        //reportInput.TotalClassConduct2 = (from poc in pupilClass
                                        //                                  join cc in classConduct on poc.Key equals cc.Key
                                        //                                  where poc.Num == cc.Num
                                        //                                  select poc).Count();

                                        // So lop da tong ket HK1
                                        reportInput.TotalClassSummedUp1 = (from pr in smasContext.PupilRanking.AsNoTracking()
                                                                           join pp in smasContext.PupilProfile.AsNoTracking() on pr.PupilID equals pp.PupilProfileID
                                                                           join a in smasContext.AcademicYear.AsNoTracking() on pr.AcademicYearID equals a.AcademicYearID
                                                                           join cp in smasContext.ClassProfile.AsNoTracking() on pr.ClassID equals cp.ClassProfileID
                                                                           join el in smasContext.EducationLevel.AsNoTracking() on cp.EducationLevelID equals el.EducationLevelID
                                                                           where pr.SchoolID == reportInput.SchoolID
                                                                           && pr.Semester == SystemParamsInFile.SEMESTER_OF_YEAR_FIRST
                                                                           && pr.Last2digitNumberSchool == reportInput.SchoolID % 100
                                                                           && pr.AverageMark != null
                                                                           && pp.IsActive == true
                                                                           && a.Year == year
                                                                           && a.SchoolID == reportInput.SchoolID
                                                                           && cp.SchoolID == reportInput.SchoolID
                                                                           && (el.Grade == reportInput.GradeID || el.Grade == 0)
                                                                           group pr by pr.ClassID into g
                                                                           select new
                                                                           {
                                                                               g.Key,
                                                                               //Num = g.Count()
                                                                           }).Count();

                                        //reportInput.TotalClassSummedUp1 = (from poc in pupilClass
                                        //                                   join s in classSummedUp on poc.Key equals s.Key
                                        //                                   where poc.Num == s.Num
                                        //                                   select poc).Count();

                                        // So lop da tong ket HK2
                                        reportInput.TotalClassSummedUp2 = (from pr in smasContext.PupilRanking.AsNoTracking()
                                                                           join pp in smasContext.PupilProfile.AsNoTracking() on pr.PupilID equals pp.PupilProfileID
                                                                           join a in smasContext.AcademicYear.AsNoTracking() on pr.AcademicYearID equals a.AcademicYearID
                                                                           join cp in smasContext.ClassProfile.AsNoTracking() on pr.ClassID equals cp.ClassProfileID
                                                                           join el in smasContext.EducationLevel.AsNoTracking() on cp.EducationLevelID equals el.EducationLevelID
                                                                           where pr.SchoolID == reportInput.SchoolID
                                                                           && pr.Semester == SystemParamsInFile.SEMESTER_OF_YEAR_SECOND
                                                                           && pr.Last2digitNumberSchool == reportInput.SchoolID % 100
                                                                           && pr.AverageMark != null
                                                                           && pp.IsActive == true
                                                                           && a.Year == year
                                                                           && cp.SchoolID == reportInput.SchoolID
                                                                           && (el.Grade == reportInput.GradeID || el.Grade == 0)
                                                                           group pr by pr.ClassID into g
                                                                           select new
                                                                           {
                                                                               g.Key,
                                                                               //Num = g.Count()
                                                                           }).Count();

                                        //reportInput.TotalClassSummedUp2 = (from poc in pupilClass
                                        //                                   join s in classSummedUp on poc.Key equals s.Key
                                        //                                   where poc.Num == s.Num
                                        //                                   select poc).Count();

                                        // So user da cap

                                        reportInput.TotalUser = (from asp in smasContext.aspnet_Users.AsNoTracking()
                                                                 join ua in smasContext.UserAccount.AsNoTracking() on asp.UserId equals ua.GUID
                                                                 join e in smasContext.Employee.AsNoTracking() on ua.EmployeeID equals e.EmployeeID
                                                                 where e.SchoolID == reportInput.SchoolID
                                                                 && e.EmploymentStatus == SystemParamsInFile.EMPLOYMENT_STATUS_WORKING
                                                                 && e.IsActive == true
                                                                 group asp by asp.UserName into g
                                                                 select g.Key).Count();

                                        // So user da cap dang nhap he thong
                                        reportInput.TotalUserLogin = (from asp in smasContext.aspnet_Users.AsNoTracking()
                                                                      join ua in smasContext.UserAccount.AsNoTracking() on asp.UserId equals ua.GUID
                                                                      join e in smasContext.Employee.AsNoTracking() on ua.EmployeeID equals e.EmployeeID
                                                                      where e.SchoolID == reportInput.SchoolID
                                                                      && e.EmploymentStatus == SystemParamsInFile.EMPLOYMENT_STATUS_WORKING
                                                                      && e.IsActive == true
                                                                      //&& e.AppliedLevel == reportInput.GradeID // Quan tam cap hoc
                                                                      && ua.FirstLoginDate != null
                                                                      group asp by asp.UserName into g
                                                                      select g.Key).Count();

                                        // So hoc sinh da nhap SDT ca nhan
                                        reportInput.TotalPupilMobile = (from pp in smasContext.PupilProfile.AsNoTracking()
                                                                        join poc in smasContext.PupilOfClass.AsNoTracking() on pp.PupilProfileID equals poc.PupilID
                                                                        join cp in smasContext.ClassProfile.AsNoTracking() on poc.ClassID equals cp.ClassProfileID
                                                                        join el in smasContext.EducationLevel.AsNoTracking() on cp.EducationLevelID equals el.EducationLevelID
                                                                        where
                                                                        pp.Mobile != null && pp.Mobile.Length > 0
                                                                        && pp.IsActive == true
                                                                        && poc.SchoolID == reportInput.SchoolID
                                                                        && (poc.Status == SystemParamsInFile.PUPIL_STATUS_STUDYING || poc.Status == SystemParamsInFile.PUPIL_STATUS_GRADUATED)
                                                                        && poc.Year == year
                                                                        && cp.SchoolID == reportInput.SchoolID
                                                                        && (el.Grade == reportInput.GradeID || el.Grade == 0)
                                                                        group pp by pp.PupilProfileID into g
                                                                        select g.Key).Count();

                                        // So hoc sinh da nhap SDT phu huynh
                                        reportInput.TotalFamilyMobile = (from pp in smasContext.PupilProfile.AsNoTracking()
                                                                         join poc in smasContext.PupilOfClass.AsNoTracking() on pp.PupilProfileID equals poc.PupilID
                                                                         join cp in smasContext.ClassProfile.AsNoTracking() on poc.ClassID equals cp.ClassProfileID
                                                                         join el in smasContext.EducationLevel.AsNoTracking() on cp.EducationLevelID equals el.EducationLevelID
                                                                         where
                                                                         ((pp.MotherMobile != null && pp.MotherMobile.Length > 0) || (pp.FatherMobile != null && pp.FatherMobile.Length > 0))
                                                                         && pp.IsActive == true
                                                                         && poc.SchoolID == reportInput.SchoolID
                                                                         && (poc.Status == SystemParamsInFile.PUPIL_STATUS_STUDYING || poc.Status == SystemParamsInFile.PUPIL_STATUS_GRADUATED)
                                                                         && poc.Year == year
                                                                         && cp.SchoolID == reportInput.SchoolID
                                                                         && (el.Grade == reportInput.GradeID || el.Grade == 0)
                                                                         group pp by pp.PupilProfileID into g
                                                                         select g.Key).Count();

                                        // So hoc sinh da nhap SDT phu huynh cha
                                        reportInput.TotalFamilyMobileF = (from pp in smasContext.PupilProfile.AsNoTracking()
                                                                          join poc in smasContext.PupilOfClass.AsNoTracking() on pp.PupilProfileID equals poc.PupilID
                                                                          join cp in smasContext.ClassProfile.AsNoTracking() on poc.ClassID equals cp.ClassProfileID
                                                                          join el in smasContext.EducationLevel.AsNoTracking() on cp.EducationLevelID equals el.EducationLevelID
                                                                          where
                                                                          pp.FatherMobile != null && pp.FatherMobile.Length > 0
                                                                          && pp.IsActive == true
                                                                          && poc.SchoolID == reportInput.SchoolID
                                                                          && (poc.Status == SystemParamsInFile.PUPIL_STATUS_STUDYING || poc.Status == SystemParamsInFile.PUPIL_STATUS_GRADUATED)
                                                                          && poc.Year == year
                                                                          && cp.SchoolID == reportInput.SchoolID
                                                                          && (el.Grade == reportInput.GradeID || el.Grade == 0)
                                                                          group pp by pp.PupilProfileID into g
                                                                          select g.Key).Count();

                                        // So hoc sinh da nhap SDT phu huynh me
                                        reportInput.TotalFamilyMobileM = (from pp in smasContext.PupilProfile.AsNoTracking()
                                                                          join poc in smasContext.PupilOfClass.AsNoTracking() on pp.PupilProfileID equals poc.PupilID
                                                                          join cp in smasContext.ClassProfile.AsNoTracking() on poc.ClassID equals cp.ClassProfileID
                                                                          join el in smasContext.EducationLevel.AsNoTracking() on cp.EducationLevelID equals el.EducationLevelID
                                                                          where
                                                                          pp.MotherMobile != null && pp.MotherMobile.Length > 0
                                                                          && pp.IsActive == true
                                                                          && poc.SchoolID == reportInput.SchoolID
                                                                          && (poc.Status == SystemParamsInFile.PUPIL_STATUS_STUDYING || poc.Status == SystemParamsInFile.PUPIL_STATUS_GRADUATED)
                                                                          && poc.Year == year
                                                                          && cp.SchoolID == reportInput.SchoolID
                                                                          && (el.Grade == reportInput.GradeID || el.Grade == 0)
                                                                          group pp by pp.PupilProfileID into g
                                                                          select g.Key).Count();
                                        #endregion
                                    }
                                }
                            }
                        }
                        #endregion
                    });
                }

                Task.WaitAll(arrTask);
            }

            return lstReportInputStatis;
        }
        #endregion
        #endregion

        #region Fill Excel
        /// <summary>
        /// Fill Excel
        /// </summary>
        /// <author date="14/05/05">HaiVT</author>
        /// <param name="dic"></param>
        public ProcessedReport FillExcelReportInputMonitor(IDictionary<string, object> dic)
        {
            #region Load template
            List<ReportInputMonitorBO> lstInput = (List<ReportInputMonitorBO>)dic["lstInput"];
            int totalLstInput = lstInput.Count;
            string reportCode = Utils.GetString(dic, "reportCode");
            int schoolLevel = Utils.GetInt(dic, "AppliedLevel");
            int year = Utils.GetInt(dic, "Year");
            int provinceID = Utils.GetInt(dic, "ProvinceID");
            char lastColumnc = (char)dic["charLastColumn"];
            bool mergeDistrict = Utils.GetBool(dic, "mergeDistrict", false);
            ReportDefinition reportDef = ReportDefinitionBusiness.GetByCode(reportCode);
            string templatePath = ReportUtils.GetTemplatePath(reportDef);
            IVTWorkbook oBook = VTExport.OpenWorkbook(templatePath);
            IVTWorksheet oSheet = oBook.GetSheet(1);
            #endregion

            #region Fill thong tin chung
            Dictionary<string, object> dicExcel = new Dictionary<string, object>();
            dicExcel["SchoolLevel"] = schoolLevel;
            dicExcel["AcademicYearTitle"] = year + " - " + (year + 1);
            dicExcel["ProvinceName"] = ProvinceBusiness.Find(provinceID).ProvinceName;
            int firstRow = 7;
            int lastColumn = VTVector.dic[lastColumnc];
            int firstColumnSum = VTVector.dic['D'];
            oSheet.GetRange(1, 1, firstRow - 1, lastColumn).FillVariableValue(dicExcel);
            #endregion

            #region Fill noi dung
            // Copy cot summary
            IVTRange summaryRange = oSheet.GetRange(8, 1, 8, lastColumn);
            IVTRange copyRange = oSheet.GetRange(totalLstInput + 8, 1, totalLstInput + 8, lastColumn);
            summaryRange.Range.Copy(copyRange.Range, NativeExcel.XlPasteType.xlPasteAll);

            // Xoa summary cu
            summaryRange.Range.Delete();

            // Set cong thuc
            string formulaSum = string.Empty;
            for (int i = firstColumnSum; i <= lastColumn; i++)
            {
                formulaSum = "=SUM(" + VTVector.ColumnIntToString(i) + "7:" + VTVector.ColumnIntToString(i) + (totalLstInput + 6).ToString() + ")";
                oSheet.SetCellValue(totalLstInput + 7, i, formulaSum);
            }

            List<object> lstData = new List<object>();
            Dictionary<string, object> dicData = null;
            ReportInputMonitorBO item = null;
            var mergeCell = new { };
            for (int i = 0, size = totalLstInput; i < size; i++)
            {
                item = lstInput[i];
                dicData = new Dictionary<string, object>();
                dicData["Order"] = i + 1;
                dicData["DistrictName"] = item.DistrictName;
                dicData["GradeName"] = item.GradeName;
                dicData["TotalSchool"] = item.TotalSchool;
                dicData["TotalSchoolInput"] = item.TotalSchoolInput;
                dicData["SchoolName"] = item.SchoolName;
                dicData["NumberOfPupil"] = item.TotalPupil;
                dicData["NumberOfTeacher"] = item.TotalTeacher;
                dicData["NumberOfClass"] = item.TotalClass;
                dicData["NumberOfClassHasSubject"] = item.TotalClassSubject;
                dicData["NumberOfClassAsignmentFirstSemester"] = item.TotalClassAssignment1;
                dicData["NumberOfClassAsignmentSecondSemester"] = item.TotalClassAssignment2;
                dicData["NumberOfClassHasMarkFirstSemester"] = item.TotalClassMark1;
                dicData["NumberOfClassHasMarkSecondSemester"] = item.TotalClassMark2;
                dicData["NumberOfClassHasConductFirstSemester"] = item.TotalClassConduct1;
                dicData["NumberOfClassHasConductSecondSemester"] = item.TotalClassConduct2;
                dicData["NumberOfClassSumMarkFirstSemester"] = item.TotalClassSummedUp1;
                dicData["NumberOfClassSumMarkSecondSemester"] = item.TotalClassSummedUp2;
                dicData["NumberOfUser"] = item.TotalUser;
                dicData["NumberOfLoggedUser"] = item.TotalUserLogin;
                dicData["NumberOfPupilHasMobile"] = item.TotalPupilMobile;
                dicData["NumberOfPupilHasParentMobile"] = item.TotalFamilyMobile;
                dicData["NumberOfPupilHasFatherMobile"] = item.TotalFamilyMobileF;
                dicData["NumberOfPupilHasMotherMobile"] = item.TotalFamilyMobileM;
                lstData.Add(dicData);
            }

            oSheet.GetRange(firstRow, 1, firstRow + totalLstInput - 1, lastColumn).FillVariableValue(new Dictionary<string, object> { { "list", lstData } });
            oSheet.GetRange(5, 1, lstData.Count + 7, lastColumn).SetBorder(VTBorderStyle.Solid, VTBorderWeight.Thin, VTBorderIndex.All);
            oSheet.GetRange(7, 1, lstData.Count + 7, lastColumn).Range.RowHeight = 20;

            // Merge cell
            if (mergeDistrict)
            {
                var mDistrict = (from p in lstInput
                                 group p by p.DistrictID into g
                                 select new
                                 {
                                     g.Key,
                                     Num = g.Count()
                                 }).ToList();
                int firstRowMer = 7;
                for (int i = 0, size = mDistrict.Count; i < size; i++)
                {
                    oSheet.GetRange(firstRowMer, 2, firstRowMer + mDistrict[i].Num - 1, 2).Range.Merge();
                    firstRowMer += mDistrict[i].Num;
                }
            }
            #endregion

            #region Luu vao DB
            Stream data = oBook.ToStream();
            ProcessedReport pr = new ProcessedReport();
            pr.ReportCode = reportCode;
            pr.ProcessedDate = DateTime.Now;
            int userAccountID = Utils.GetInt(dic, "UserAccountID");
            int supervisingDeptID = Utils.GetInt(dic, "supervisingDeptID");
            int districtID = Utils.GetInt(dic, "DistrictID");
            dic = new Dictionary<string, object>
            {
                 {"UserAccountID", userAccountID},
                 {"SupervisingDeptID", supervisingDeptID},
                 {"DistrictID", districtID},
                 {"Year", year},
                 {"AppliedLevel", schoolLevel}
            };

            pr.InputParameterHashKey = ReportUtils.GetHashKey(dic);
            pr.ReportData = ReportUtils.Compress(data);

            //Tạo tên file
            string outputNamePattern = reportDef.OutputNamePattern;
            string titleReport = "All";
            SupervisingDept supervisingDept = SupervisingDeptBusiness.Find(supervisingDeptID);
            if (supervisingDept.HierachyLevel == SystemParamsInFile.EDUCATION_HIERACHY_LEVEL_DISTRICT_OFFICE || supervisingDept.HierachyLevel == SystemParamsInFile.EDUCATION_HIERACHY_LEVEL_DISTRICT_OFFICE_DEPARTMENT)
            {
                titleReport = "PGD";
            }
            else if (supervisingDept.HierachyLevel == SystemParamsInFile.EDUCATION_HIERACHY_LEVEL_PROVINCE_OFFICE || supervisingDept.HierachyLevel == SystemParamsInFile.EDUCATION_HIERACHY_LEVEL_PROVINCE_OFFICE_DEPARTMENT)
            {
                titleReport = "SGD";
            }

            outputNamePattern = outputNamePattern.Replace("[PGD/SGD]", titleReport);
            outputNamePattern = outputNamePattern.Replace("[SchoolLevel]", schoolLevel == 1 ? "TH" : schoolLevel == 2 ? "THCS" : "THPT");
            pr.ReportName = ReportUtils.RemoveSpecialCharacters(outputNamePattern) + "." + reportDef.OutputFormat;
            ProcessedReportParameterRepository.Insert(dic, pr);
            ProcessedReportRepository.Insert(pr);
            ProcessedReportRepository.Save();
            return pr;
            #endregion
        }
        #endregion

        /// <summary>
        /// Tong hop du lieu tinh hinh nhap lieu
        /// </summary>
        /// <param name="SearchInfo"></param>
        public void CreateDataInputSituation(IDictionary<string, object> SearchInfo)
        {
           
            int provinceId = Utils.GetInt(SearchInfo, "ProvinceID");
            int last2digitProvince = UtilsBusiness.GetPartionId(provinceId, 64);
            int districtId = Utils.GetInt(SearchInfo, "DistrictID");
            int superVisingDeptId = Utils.GetInt(SearchInfo, "SuperVisingDeptID"); /*0: so, 1: phong, 2:admin */
            int year = Utils.GetInt(SearchInfo, "Year");
            string ReportCode = Utils.GetString(SearchInfo, "ReportCode");
            int semester = Utils.GetInt(SearchInfo, "Semester");
            int appliedLevel = Utils.GetInt(SearchInfo, "AppliedLevel");
            int markType = Utils.GetInt(SearchInfo, "MarkType");
            int unitId = Utils.GetInt(SearchInfo, "UnitId");
            int ReportType = Utils.GetInt(SearchInfo, "ReportType");
            string sUnitId = unitId.ToString();
            int[] ArrEducationGrade = null;
            if (appliedLevel == GlobalConstants.APPLIED_LEVEL_PRIMARY)
            {
                ArrEducationGrade = new int[7] { 1, 3, 7, 9, 17, 25, 31 };
            }
            else if (appliedLevel == GlobalConstants.APPLIED_LEVEL_SECONDARY)
            {
                ArrEducationGrade = new int[4] { 2, 3, 6, 7 };
            }
            else if (appliedLevel == GlobalConstants.APPLIED_LEVEL_TERTIARY)
            {
                ArrEducationGrade = new int[3] { 4, 6, 7 };
            }
            else if (appliedLevel == GlobalConstants.APPLIED_LEVEL_CRECHE || appliedLevel == GlobalConstants.APPLIED_LEVEL_KINDER_GARTEN)
            {
                ArrEducationGrade = new int[4] { 8, 16, 24, 31 };
            }
            //Lay danh sach truong can tong hop
            IQueryable<AcademicYearBO> iqSchool = from ac in AcademicYearBusiness.AllNoTracking
                                                  join sp in SchoolProfileBusiness.AllNoTracking on ac.SchoolID equals sp.SchoolProfileID
                                                  //join su in SupervisingDeptBusiness.AllNoTracking on sp.SupervisingDeptID equals su.SupervisingDeptID
                                                  where sp.ProvinceID == provinceId
                                                        && ac.Year == year
                                                        && sp.IsActive == true
                                                        && ArrEducationGrade.Contains(sp.EducationGrade)
                                                        && ac.IsActive == true
                                                  //&& (su.SupervisingDeptID == unitId || su.TraversalPath.Contains(sUnitId))
                                                  select new AcademicYearBO
                                                  {
                                                      AcademicYearID = ac.AcademicYearID,
                                                      SchoolID = ac.SchoolID,
                                                      Year = ac.Year,
                                                      School = sp
                                                  };

            if (districtId > 0)
            {
                iqSchool = iqSchool.Where(sp => sp.School.DistrictID == districtId);
            }

            List<AcademicYearBO> lstSchool = iqSchool.ToList();
            if (lstSchool == null || lstSchool.Count == 0)
            {
                return;
            }
            //Xoa du lieu truoc khi tong hop
            IQueryable<MarkStatistic> iQMarkStatisticDelete = from m in MarkStatisticBusiness.AllNoTracking
                                                              where m.Last2DigitNumberProvince == last2digitProvince
                                                              && m.ProvinceID == provinceId
                                                              && m.Year == year
                                                              && m.AppliedLevel == appliedLevel
                                                              && m.Semester == semester
                                                              && m.MarkType == markType
                                                              && m.ReportCode == ReportCode
                                                              && m.SuperVisingDeptID == superVisingDeptId
                                                              select m;
            if (districtId > 0)
            {
                iQMarkStatisticDelete = iQMarkStatisticDelete.Where(m => m.DistrictID == districtId);
            }
            List<MarkStatistic> lstMarkStatistic = iQMarkStatisticDelete.ToList();
            //Xoa du lieu cu
            if (lstMarkStatistic != null && lstMarkStatistic.Count > 0)
            {
                try
                {
                    //context.Configuration.AutoDetectChangesEnabled = false;
                    //context.Configuration.ValidateOnSaveEnabled = false;
                    MarkStatisticBusiness.SetAutoDetectChangesEnabled(false);
                    if (lstMarkStatistic != null && lstMarkStatistic.Count > 0)
                    {
                        MarkStatisticBusiness.DeleteAll(lstMarkStatistic);
                        MarkStatisticBusiness.Save();
                    }
                }
                catch (Exception ex)
                {
                    LogExtensions.ErrorExt(logger, DateTime.Now, "CreateDataInputSituation", "null", ex);
                }
                finally
                {
                    MarkStatisticBusiness.SetAutoDetectChangesEnabled(true);
                }
            }
            int numTask = UtilsBusiness.GetNumTask;
            //Tong hop tinh hinh nhap lieu
            Task[] arrTask = new Task[numTask];
            for (int z = 0; z < numTask; z++)
            {
                int currentIndex = z;
                arrTask[z] = Task.Factory.StartNew(() =>
                {
                    #region run task
                    List<AcademicYearBO> lstReportRunTask = lstSchool.Where(p => p.SchoolID % numTask == currentIndex).ToList();
                    if (lstReportRunTask != null && lstReportRunTask.Count > 0)
                    {
                        for (int j = 0; j < lstReportRunTask.Count; j++)
                        {
                            AcademicYearBO objAcademicYearTask = lstReportRunTask[j];
                            if (objAcademicYearTask == null)
                            {
                                continue;
                            }

                            InsertMarkStatistc(provinceId, objAcademicYearTask.School.DistrictID.Value, superVisingDeptId, objAcademicYearTask.SchoolID, objAcademicYearTask.AcademicYearID, objAcademicYearTask.Year,
                            appliedLevel, semester, markType, ReportCode, ReportType);

                        }
                    }
                    #endregion
                });
            }
            Task.WaitAll(arrTask);
            LogExtensions.InfoExt(logger, LogExtensions.LOG_TYPE_INFO, DateTime.Now, "", "", "");
        }
        public System.IO.Stream ExportInputMonitoringSituation(IDictionary<string, object> SearchInfo)
        {
            int superVisingDeptId = Utils.GetInt(SearchInfo, "SuperVisingDeptID");
            int appliedLevel = Utils.GetInt(SearchInfo, "AppliedLevel");
            string ReportCode = Utils.GetString(SearchInfo, "ReportCode");
            string templatePath = String.Format("{0}/Phong_So/{1}.xls", SystemParamsInFile.TEMPLATE_FOLDER, ReportCode);
            IVTWorkbook oBook = VTExport.OpenWorkbook(templatePath);
            IVTWorksheet sheet = oBook.GetSheet(1);
            List<MarkStatisticBO> lstMark = GetDataMarkStatistic(SearchInfo);
            ExportInputMonitoringSituation(sheet, lstMark, SearchInfo);
            return oBook.ToStream();
        }
        public void CreateDataKinderGartenReport(IDictionary<string,object> dic)
        {
            int AcademicYearID = Utils.GetInt(dic, "AcademicYearID");
            int ProvinceID = Utils.GetInt(dic, "ProvinceID");
            int DistrictID = Utils.GetInt(dic, "DistrictID");
            int Year = Utils.GetInt(dic, "Year");

            List<string> lstReportCode = new List<string>();
            lstReportCode.Add("KinterGarten_Report_M2");
            lstReportCode.Add("KinterGarten_Report_M3");
            lstReportCode.Add("KinterGarten_Report_M4");
            lstReportCode.Add("KinterGarten_Report_M6");
            lstReportCode.Add("KinterGarten_Report_M7");
            int[] ArrEducationGrade = null;
            ArrEducationGrade = new int[4] { 8, 16, 24, 31 };//tat ca cac truong MN,NT,MG
            IQueryable<AcademicYearBO> iqSchool = from ac in AcademicYearBusiness.AllNoTracking
                                                  join sp in SchoolProfileBusiness.AllNoTracking on ac.SchoolID equals sp.SchoolProfileID
                                                  join su in SupervisingDeptBusiness.AllNoTracking on sp.SupervisingDeptID equals su.SupervisingDeptID
                                                  where sp.ProvinceID == ProvinceID
                                                    && ac.Year == Year
                                                    && sp.IsActive == true
                                                    && ac.IsActive == true
                                                    && su.HierachyLevel == SystemParamsInFile.EDUCATION_HIERACHY_LEVEL_DISTRICT_OFFICE
                                                    && ArrEducationGrade.Contains(sp.EducationGrade)
                                                  select new AcademicYearBO
                                                  {
                                                      AcademicYearID = ac.AcademicYearID,
                                                      SchoolID = ac.SchoolID,
                                                      Year = ac.Year,
                                                      School = sp
                                                  };
            if (DistrictID > 0)
            {
                iqSchool = iqSchool.Where(sp => sp.School.DistrictID == DistrictID);
            }

            List<AcademicYearBO> lstSchool = iqSchool.ToList();
            if (lstSchool == null || lstSchool.Count == 0)
            {
                return;
            }

            List<int> lstSchoolID = lstSchool.Select(p => p.SchoolID).Distinct().ToList();

            IQueryable<MarkStatistic> iQMarkStatisticDelete = from m in MarkStatisticBusiness.AllNoTracking
                                                              where m.Last2DigitNumberProvince == ProvinceID % 64
                                                              && m.ProvinceID == ProvinceID
                                                              && m.Year == Year
                                                              && lstReportCode.Contains(m.ReportCode)
                                                              select m;
            List<MarkStatistic> lstMarkStatistic = iQMarkStatisticDelete.ToList();
            //Xoa du lieu cu
            if (lstMarkStatistic != null && lstMarkStatistic.Count > 0)
            {
                try
                {
                    context.Configuration.AutoDetectChangesEnabled = false;
                    context.Configuration.ValidateOnSaveEnabled = false;
                    if (lstMarkStatistic != null && lstMarkStatistic.Count > 0)
                    {
                        MarkStatisticBusiness.DeleteAll(lstMarkStatistic);
                        MarkStatisticBusiness.Save();
                    }
                }
                catch (Exception ex)
                {
                    
                    LogExtensions.ErrorExt(logger, DateTime.Now, "CreateDataKinderGartenReport", "null", ex);
                }
                finally
                {
                    context.Configuration.AutoDetectChangesEnabled = true;
                    context.Configuration.ValidateOnSaveEnabled = true;
                }
            }
            int numTask = 1;//UtilsBusiness.GetNumTask;
            string ReportCode = string.Empty;
            for (int i = 0; i < lstReportCode.Count; i++)
            {
                ReportCode = lstReportCode[i];
                //Tong hop bao cao diem kiem tra cuoi ky
                Task[] arrTask = new Task[numTask];
                for (int z = 0; z < numTask; z++)
                {
                    int currentIndex = z;
                    arrTask[z] = Task.Factory.StartNew(() =>
                    {
                        #region run task
                        List<AcademicYearBO> lstReportRunTask = lstSchool.Where(p => p.SchoolID % numTask == currentIndex).ToList();
                        if (lstReportRunTask != null && lstReportRunTask.Count > 0)
                        {
                            for (int j = 0; j < lstReportRunTask.Count; j++)
                            {
                                AcademicYearBO objAcademicYearTask = lstReportRunTask[j];
                                if (objAcademicYearTask == null)
                                {
                                    continue;
                                }
                                this.InsertKinderGartenReport(ProvinceID, objAcademicYearTask.School.DistrictID.Value,objAcademicYearTask.SchoolID,objAcademicYearTask.School.SupervisingDeptID.Value, objAcademicYearTask.AcademicYearID, Year, ReportCode);
                            }
                        }
                        #endregion
                    });
                }
                Task.WaitAll(arrTask);
            }
        }
        public System.IO.Stream ExportKinderGartenReport(IDictionary<string, object> SearchInfo)
        {
            int ProvinceID = Utils.GetInt(SearchInfo, "ProvinceID");
            int DistrictID = Utils.GetInt(SearchInfo, "DistrictID");
            string AcademicYearID = Utils.GetString(SearchInfo, "AcademicYearID");
            int Year = Utils.GetInt(SearchInfo, "Year");
            string SupervisingDeptName = Utils.GetString(SearchInfo, "SupervisingDeptName");
            List<string> lstReportCode = new List<string>();
            lstReportCode.Add("KinterGarten_Report_M2");
            lstReportCode.Add("KinterGarten_Report_M3");
            lstReportCode.Add("KinterGarten_Report_M4");
            lstReportCode.Add("KinterGarten_Report_M6");
            lstReportCode.Add("KinterGarten_Report_M7");

            string templatePath = String.Format("{0}/Phong_So/{1}.xls", SystemParamsInFile.TEMPLATE_FOLDER, "KinterGarten_Report");
            IVTWorkbook oBook = VTExport.OpenWorkbook(templatePath);
            IVTWorksheet sheetM2 = oBook.GetSheet(1);//Mau 2
            IVTWorksheet sheetM3 = oBook.GetSheet(2);//Mau 3
            IVTWorksheet sheetM4 = oBook.GetSheet(3);//Mau 4
            IVTWorksheet sheetM6 = oBook.GetSheet(4);//Mau 6
            IVTWorksheet sheetM7 = oBook.GetSheet(5);//Mau 7
            int[] ArrEducationGrade = null;
            ArrEducationGrade = new int[4] { 8, 16, 24, 31 };//tat ca cac truong MN,NT,MG
            //lay danh sach truong
            IQueryable<AcademicYearBO> iqSchool = from ac in AcademicYearBusiness.AllNoTracking
                                                  join sp in SchoolProfileBusiness.AllNoTracking on ac.SchoolID equals sp.SchoolProfileID
                                                  join su in SupervisingDeptBusiness.AllNoTracking on sp.SupervisingDeptID equals su.SupervisingDeptID
                                                  where sp.ProvinceID == ProvinceID
                                                    && ac.Year == Year
                                                    && sp.IsActive == true
                                                    && ac.IsActive == true
                                                    && su.HierachyLevel == SystemParamsInFile.EDUCATION_HIERACHY_LEVEL_DISTRICT_OFFICE
                                                    && ArrEducationGrade.Contains(sp.EducationGrade)
                                                  select new AcademicYearBO
                                                  {
                                                      AcademicYearID = ac.AcademicYearID,
                                                      SchoolID = ac.SchoolID,
                                                      SchoolName = sp.SchoolName,
                                                      Year = ac.Year,
                                                      School = sp

                                                  };
            if (DistrictID > 0)
            {
                iqSchool = iqSchool.Where(sp => sp.School.DistrictID == DistrictID);
            }
            List<AcademicYearBO> lstSchool = iqSchool.ToList();
            AcademicYearBO objSchool = null;

            IQueryable<MarkStatisticBO> iqMarkStatistic = from m in MarkStatisticBusiness.AllNoTracking
                                                          join sp in SchoolProfileBusiness.AllNoTracking on m.SchoolID equals sp.SchoolProfileID
                                                          join d in DistrictBusiness.AllNoTracking on sp.DistrictID equals d.DistrictID
                                                          join s in SupervisingDeptBusiness.AllNoTracking on sp.SupervisingDeptID equals s.SupervisingDeptID
                                                          where m.Last2DigitNumberProvince == ProvinceID % 64
                                                           && m.ProvinceID == ProvinceID
                                                           && m.Year == Year
                                                           && lstReportCode.Contains(m.ReportCode)
                                                          select new MarkStatisticBO
                                                           {
                                                               DistrictCode = d.DistrictCode,
                                                               DistrictName = d.DistrictName,
                                                               SupervisingDeptName = s.SupervisingDeptName,
                                                               SuperVisingDeptID = s.SupervisingDeptID,
                                                               EducationGrade = sp.EducationGrade,
                                                               SchoolType = sp.SchoolTypeID,
                                                               DistrictID = d.DistrictID,
                                                               TrainingType = sp.TrainingTypeID,
                                                               MarkLevel00 = m.MarkLevel00.HasValue ? m.MarkLevel00 : 0,
                                                               MarkLevel01 = m.MarkLevel01.HasValue ? m.MarkLevel01 : 0,
                                                               MarkLevel02 = m.MarkLevel02.HasValue ? m.MarkLevel02 : 0,
                                                               MarkLevel03 = m.MarkLevel03.HasValue ? m.MarkLevel03 : 0,
                                                               MarkLevel04 = m.MarkLevel04.HasValue ? m.MarkLevel04 : 0,
                                                               MarkLevel05 = m.MarkLevel05.HasValue ? m.MarkLevel05 : 0,
                                                               MarkLevel06 = m.MarkLevel06.HasValue ? m.MarkLevel06 : 0,
                                                               MarkLevel07 = m.MarkLevel07.HasValue ? m.MarkLevel07 : 0,
                                                               MarkLevel08 = m.MarkLevel08.HasValue ? m.MarkLevel08 : 0,
                                                               MarkLevel09 = m.MarkLevel09.HasValue ? m.MarkLevel09 : 0,
                                                               MarkLevel10 = m.MarkLevel10.HasValue ? m.MarkLevel10 : 0,
                                                               MarkLevel11 = m.MarkLevel11.HasValue ? m.MarkLevel11 : 0,
                                                               MarkLevel12 = m.MarkLevel12.HasValue ? m.MarkLevel12 : 0,
                                                               MarkLevel13 = m.MarkLevel13.HasValue ? m.MarkLevel13 : 0,
                                                               MarkLevel14 = m.MarkLevel14.HasValue ? m.MarkLevel14 : 0,
                                                               MarkLevel15 = m.MarkLevel15.HasValue ? m.MarkLevel15 : 0,
                                                               MarkLevel16 = m.MarkLevel16.HasValue ? m.MarkLevel16 : 0,
                                                               MarkLevel17 = m.MarkLevel17.HasValue ? m.MarkLevel17 : 0,
                                                               MarkLevel18 = m.MarkLevel18.HasValue ? m.MarkLevel18 : 0,
                                                               MarkLevel19 = m.MarkLevel19.HasValue ? m.MarkLevel19 : 0,
                                                               MarkLevel20 = m.MarkLevel20.HasValue ? m.MarkLevel20 : 0,
                                                               MarkLevel21 = m.MarkLevel21.HasValue ? m.MarkLevel21 : 0,
                                                               MarkLevel22 = m.MarkLevel22.HasValue ? m.MarkLevel22 : 0,
                                                               MarkLevel23 = m.MarkLevel23.HasValue ? m.MarkLevel23 : 0,
                                                               MarkLevel24 = m.MarkLevel24.HasValue ? m.MarkLevel24 : 0,
                                                               MarkLevel25 = m.MarkLevel25.HasValue ? m.MarkLevel25 : 0,
                                                               MarkLevel26 = m.MarkLevel26.HasValue ? m.MarkLevel26 : 0,
                                                               MarkLevel27 = m.MarkLevel27.HasValue ? m.MarkLevel27 : 0,
                                                               MarkLevel28 = m.MarkLevel28.HasValue ? m.MarkLevel28 : 0,
                                                               MarkLevel29 = m.MarkLevel29.HasValue ? m.MarkLevel29 : 0,
                                                               MarkLevel30 = m.MarkLevel30.HasValue ? m.MarkLevel30 : 0,
                                                               MarkLevel31 = m.MarkLevel31.HasValue ? m.MarkLevel31 : 0,
                                                               MarkLevel32 = m.MarkLevel32.HasValue ? m.MarkLevel32 : 0,
                                                               MarkLevel33 = m.MarkLevel33.HasValue ? m.MarkLevel33 : 0,
                                                               MarkLevel34 = m.MarkLevel34.HasValue ? m.MarkLevel34 : 0,
                                                               MarkLevel35 = m.MarkLevel35.HasValue ? m.MarkLevel35 : 0,
                                                               MarkLevel36 = m.MarkLevel36.HasValue ? m.MarkLevel36 : 0,
                                                               MarkLevel37 = m.MarkLevel37.HasValue ? m.MarkLevel37 : 0,
                                                               MarkLevel38 = m.MarkLevel38.HasValue ? m.MarkLevel38 : 0,
                                                               MarkLevel39 = m.MarkLevel39.HasValue ? m.MarkLevel39 : 0,
                                                               MarkLevel40 = m.MarkLevel40.HasValue ? m.MarkLevel40 : 0,
                                                               MarkLevel41 = m.MarkLevel41,
                                                               MarkLevel42 = m.MarkLevel42,
                                                               MarkTotal = m.MarkTotal,
                                                               ProcessedDate = m.ProcessedDate,
                                                               PupilTotal = m.PupilTotal,
                                                               ReportCode = m.ReportCode,
                                                               SchoolCode = sp.SchoolCode,
                                                               SchoolID = sp.SchoolProfileID,
                                                               SchoolName = sp.SchoolName,
                                                           };                                           
            if (DistrictID > 0)
            {
                iqMarkStatistic = iqMarkStatistic.Where(m => m.DistrictID == DistrictID);
            }
            List<MarkStatisticBO> lstMark = iqMarkStatistic.ToList();
            List<MarkStatisticBO> lstMarkResult = new List<MarkStatisticBO>();
            List<MarkStatisticBO> lstMarktmp = new List<MarkStatisticBO>();
            MarkStatisticBO objMarkResult = null;
            List<AcademicYearBO> lstSchooltmp = new List<AcademicYearBO>();
            var lstGroupSupervisingDept = lstMark.GroupBy(p => new { p.SuperVisingDeptID, p.SupervisingDeptName }).Select(p => new { p.Key.SuperVisingDeptID, p.Key.SupervisingDeptName })
                .OrderBy(p=>p.SupervisingDeptName).Distinct().ToList();
            #region Fill Sheet Mau 2
            lstMarkResult = lstMark.Where(p => p.ReportCode.Equals("KinterGarten_Report_M2")).OrderBy(p => p.SchoolName).ToList();
            int startRow = 9;
            List<int> lstToRow = new List<int>();
            sheetM2.SetCellValue("A2", SupervisingDeptName.ToUpper());
            sheetM2.SetCellValue("A3", string.Format("BÁO CÁO SỐ LIỆU TRƯỜNG, LỚP ĐẦU NĂM HỌC {0} - {1}", Year, (Year + 1)));
            for (int i = 0; i < lstGroupSupervisingDept.Count; i++)
            {
                var objGroupSupervisingDept = lstGroupSupervisingDept[i];
                sheetM2.GetRange(startRow, 1, startRow, 2).Merge();
                sheetM2.GetRange(startRow, 1, startRow, 29).SetFontStyle(true, null, false, 10, false, false);
                sheetM2.SetCellValue(startRow, 1, objGroupSupervisingDept.SupervisingDeptName);
                lstToRow.Add(startRow);
                lstSchooltmp = lstSchool.Where(p => p.School.SupervisingDeptID == objGroupSupervisingDept.SuperVisingDeptID).ToList();
                this.SetFormulaProvince(sheetM2, startRow, startRow + 1, startRow + lstSchooltmp.Count, 3, 30);
                startRow++;
                for (int j = 0; j < lstSchooltmp.Count; j++)
                {
                    objSchool = lstSchooltmp[j];
                    objMarkResult = lstMarkResult.Where(p => p.SuperVisingDeptID == objGroupSupervisingDept.SuperVisingDeptID && p.SchoolID == objSchool.SchoolID).FirstOrDefault();
                    sheetM2.SetCellValue(startRow, 1, (j + 1));
                    sheetM2.SetCellValue(startRow, 2, objSchool.SchoolName);
                    sheetM2.SetCellValue(startRow, 3, 1);
                    
                    if (objMarkResult != null)
                    {
                        //Trong cong lap
                        sheetM2.SetCellValue(startRow, 4, objMarkResult.EducationGrade == SystemParamsInFile.SCHOOL_EDUCATION_GRADE_PRESCHOOL
                            && objMarkResult.TrainingType == 1 ? 1 : 0);
                        sheetM2.SetCellValue(startRow, 5, objMarkResult.EducationGrade == SystemParamsInFile.SCHOOL_EDUCATION_GRADE_KINDERGARTEN
                            && objMarkResult.TrainingType == 1 ? 1 : 0);
                        sheetM2.SetCellValue(startRow, 6, objMarkResult.MarkLevel03);
                        sheetM2.SetCellValue(startRow, 7, objMarkResult.MarkLevel04);
                        sheetM2.SetCellValue(startRow, 8, objMarkResult.MarkLevel05);
                        sheetM2.SetCellValue(startRow, 9, objMarkResult.MarkLevel06);
                        sheetM2.SetCellValue(startRow, 10, objMarkResult.MarkLevel07);
                        sheetM2.SetCellValue(startRow, 11, objMarkResult.MarkLevel08);
                        sheetM2.SetCellValue(startRow, 12, objMarkResult.MarkLevel09);
                        sheetM2.SetCellValue(startRow, 13, objMarkResult.MarkLevel10);
                        sheetM2.SetCellValue(startRow, 14, objMarkResult.MarkLevel21);
                        sheetM2.SetCellValue(startRow, 15, objMarkResult.MarkLevel22);
                        sheetM2.SetCellValue(startRow, 16, objMarkResult.MarkLevel23);
                        //Ngoai cong lap
                        sheetM2.SetCellValue(startRow, 17, objMarkResult.EducationGrade == SystemParamsInFile.SCHOOL_EDUCATION_GRADE_PRESCHOOL
                            && (objMarkResult.TrainingType == 2 || objMarkResult.TrainingType == 4) ? 1 : 0);
                        sheetM2.SetCellValue(startRow, 18, objMarkResult.EducationGrade == SystemParamsInFile.SCHOOL_EDUCATION_GRADE_KINDERGARTEN
                            && (objMarkResult.TrainingType == 2 || objMarkResult.TrainingType == 4) ? 1 : 0);
                        sheetM2.SetCellValue(startRow, 19, objMarkResult.MarkLevel12);
                        sheetM2.SetCellValue(startRow, 20, objMarkResult.MarkLevel14);
                        sheetM2.SetCellValue(startRow, 21, objMarkResult.MarkLevel15);
                        sheetM2.SetCellValue(startRow, 22, objMarkResult.MarkLevel16);
                        sheetM2.SetCellValue(startRow, 23, objMarkResult.MarkLevel20);
                        sheetM2.SetCellValue(startRow, 24, objMarkResult.MarkLevel17);
                        sheetM2.SetCellValue(startRow, 25, objMarkResult.MarkLevel18);
                        sheetM2.SetCellValue(startRow, 26, objMarkResult.MarkLevel19);
                        sheetM2.SetCellValue(startRow, 27, objMarkResult.MarkLevel24);
                        sheetM2.SetCellValue(startRow, 28, objMarkResult.MarkLevel25);
                        sheetM2.SetCellValue(startRow, 29, objMarkResult.MarkLevel26);
                    }
                    sheetM2.GetRange(startRow, 3, startRow, 29).NumberFormat("0;-0;;@");
                    startRow++;
                }
            }
            sheetM2.GetRange(startRow, 1, startRow, 2).Merge();
            sheetM2.GetRange(startRow, 1, startRow, 29).SetFontStyle(true, null, false, 10, false, false);
            sheetM2.SetCellValue(startRow, 1, "Tổng số");
            this.SetFormulaTotal(sheetM2, startRow, lstToRow, 3, 30);
            sheetM2.GetRange(9, 1, startRow, 29).SetBorder(VTBorderStyle.Solid, VTBorderWeight.Thin, VTBorderIndex.All);
            #endregion
            #region Fill Sheet Mau 3
            lstMarkResult = lstMark.Where(p => p.ReportCode.Equals("KinterGarten_Report_M3")).OrderBy(p => p.SchoolName).ToList();
            sheetM3.SetCellValue("A2", SupervisingDeptName.ToUpper());
            sheetM3.SetCellValue("A3", string.Format("BÁO CÁO THỐNG KÊ TRẺ MẦM NON RA LỚP ĐẦU NĂM HỌC {0} - {1}", Year, (Year + 1)));
            startRow = 8;
            lstToRow = new List<int>();
            for (int i = 0; i < lstGroupSupervisingDept.Count; i++)
            {
                var objGroupSupervisingDept = lstGroupSupervisingDept[i];
                sheetM3.GetRange(startRow, 1, startRow, 2).Merge();
                sheetM3.GetRange(startRow, 1, startRow, 29).SetFontStyle(true, null, false, 10, false, false);
                sheetM3.SetCellValue(startRow, 1, objGroupSupervisingDept.SupervisingDeptName);
                lstToRow.Add(startRow);
                lstSchooltmp = lstSchool.Where(p => p.School.SupervisingDeptID == objGroupSupervisingDept.SuperVisingDeptID).ToList();
                this.SetFormulaProvince(sheetM3, startRow, startRow + 1, startRow + lstSchooltmp.Count, 3, 29);
                startRow++;
                for (int j = 0; j < lstSchooltmp.Count; j++)
                {
                    objSchool = lstSchooltmp[j];
                    objMarkResult = lstMarkResult.Where(p => p.SuperVisingDeptID == objGroupSupervisingDept.SuperVisingDeptID && p.SchoolID == objSchool.SchoolID).FirstOrDefault();
                    sheetM3.SetCellValue(startRow, 1, (j + 1));
                    sheetM3.SetCellValue(startRow, 2, objSchool.SchoolName);
                    if (objMarkResult != null)
                    {
                        //Tong so tre nha tre
                        sheetM3.SetCellValue(startRow, 3, objMarkResult.MarkLevel00);
                        sheetM3.SetCellValue(startRow, 4, objMarkResult.MarkLevel01);
                        sheetM3.SetCellValue(startRow, 5, objMarkResult.MarkLevel02);
                        sheetM3.SetCellValue(startRow, 6, objMarkResult.MarkLevel03);
                        //Tong so tre hoc MG
                        sheetM3.SetCellValue(startRow, 9, objMarkResult.MarkLevel06);
                        sheetM3.SetCellValue(startRow, 10, objMarkResult.MarkLevel07);
                        sheetM3.SetCellValue(startRow, 11, objMarkResult.MarkLevel08);
                        sheetM3.SetCellValue(startRow, 12, objMarkResult.MarkLevel09);
                        //Trong do tre 0-2 tuoi
                        sheetM3.SetCellValue(startRow, 13, objMarkResult.MarkLevel10);
                        sheetM3.SetCellValue(startRow, 14, objMarkResult.MarkLevel11);
                        sheetM3.SetCellValue(startRow, 15, objMarkResult.MarkLevel12);
                        sheetM3.SetCellValue(startRow, 16, objMarkResult.MarkLevel13);
                        //Trong do tre 3 tuoi
                        sheetM3.SetCellValue(startRow, 17, objMarkResult.MarkLevel14);
                        sheetM3.SetCellValue(startRow, 18, objMarkResult.MarkLevel15);
                        sheetM3.SetCellValue(startRow, 19, objMarkResult.MarkLevel16);
                        sheetM3.SetCellValue(startRow, 20, objMarkResult.MarkLevel17);
                        //Trong do tre 4 tuoi
                        sheetM3.SetCellValue(startRow, 21, objMarkResult.MarkLevel18);
                        sheetM3.SetCellValue(startRow, 22, objMarkResult.MarkLevel19);
                        sheetM3.SetCellValue(startRow, 23, objMarkResult.MarkLevel20);
                        sheetM3.SetCellValue(startRow, 24, objMarkResult.MarkLevel21);
                        //Trong do tre 5 tuoi
                        sheetM3.SetCellValue(startRow, 25, objMarkResult.MarkLevel22);
                        sheetM3.SetCellValue(startRow, 26, objMarkResult.MarkLevel23);
                        sheetM3.SetCellValue(startRow, 27, objMarkResult.MarkLevel24);
                        sheetM3.SetCellValue(startRow, 28, objMarkResult.MarkLevel25);
                    }
                    sheetM3.GetRange(startRow, 3, startRow, 28).NumberFormat("0;-0;;@");
                    startRow++;
                }
            }
            sheetM3.GetRange(startRow, 1, startRow, 2).Merge();
            sheetM3.GetRange(startRow, 1, startRow, 28).SetFontStyle(true, null, false, 10, false, false);
            sheetM3.SetCellValue(startRow, 1, "Tổng số");
            this.SetFormulaTotal(sheetM3, startRow, lstToRow, 3, 29);
            sheetM3.GetRange(8, 1, startRow, 28).SetBorder(VTBorderStyle.Solid, VTBorderWeight.Thin, VTBorderIndex.All);
            #endregion
            #region Fill Sheet Mau 4
            lstMarkResult = lstMark.Where(p => p.ReportCode.Equals("KinterGarten_Report_M4")).OrderBy(p => p.SchoolName).ToList();
            sheetM4.SetCellValue("A2", SupervisingDeptName.ToUpper());
            sheetM4.SetCellValue("A3", string.Format("BÁO CÁO TRẺ HỌC HAI BUỔI/NGÀY ĐẦU NĂM HỌC {0} - {1}", Year, (Year + 1)));
            startRow = 9;
            lstToRow = new List<int>();
            for (int i = 0; i < lstGroupSupervisingDept.Count; i++)
            {
                var objGroupSupervisingDept = lstGroupSupervisingDept[i];
                sheetM4.GetRange(startRow, 1, startRow, 2).Merge();
                sheetM4.GetRange(startRow, 1, startRow, 29).SetFontStyle(true, null, false, 10, false, false);
                sheetM4.SetCellValue(startRow, 1, objGroupSupervisingDept.SupervisingDeptName);
                lstSchooltmp = lstSchool.Where(p => p.School.SupervisingDeptID == objGroupSupervisingDept.SuperVisingDeptID).ToList();
                lstToRow.Add(startRow);
                this.SetFormulaProvince(sheetM4, startRow, startRow + 1, startRow + lstSchooltmp.Count, 3, 29);
                startRow++;
                for (int j = 0; j < lstSchooltmp.Count; j++)
                {
                    objSchool = lstSchooltmp[j];
                    objMarkResult = lstMarkResult.Where(p => p.SuperVisingDeptID == objGroupSupervisingDept.SuperVisingDeptID && p.SchoolID == objSchool.SchoolID).FirstOrDefault();
                    sheetM4.SetCellValue(startRow, 1, (j + 1));
                    sheetM4.SetCellValue(startRow, 2, objSchool.SchoolName);
                    if (objMarkResult != null)
                    {
                        //Nhom lop hoc 2 buoi/ngay
                        sheetM4.SetCellValue(startRow, 3, objMarkResult.MarkLevel00);
                        sheetM4.SetCellValue(startRow, 4, objMarkResult.MarkLevel01);
                        sheetM4.SetCellValue(startRow, 5, objMarkResult.MarkLevel02);
                        sheetM4.SetCellValue(startRow, 6, objMarkResult.MarkLevel03);
                        sheetM4.SetCellValue(startRow, 7, objMarkResult.MarkLevel04);
                        sheetM4.SetCellValue(startRow, 8, objMarkResult.MarkLevel05);
                        sheetM4.SetCellValue(startRow, 9, objMarkResult.MarkLevel25);
                        sheetM4.SetCellValue(startRow, 10, objMarkResult.MarkLevel07);
                        sheetM4.SetCellValue(startRow, 11, objMarkResult.MarkLevel08);
                        sheetM4.SetCellValue(startRow, 12, objMarkResult.MarkLevel09);
                        sheetM4.SetCellValue(startRow, 13, objMarkResult.MarkLevel26);
                        sheetM4.SetCellValue(startRow, 14, objMarkResult.MarkLevel27);
                        sheetM4.SetCellValue(startRow, 15, objMarkResult.MarkLevel28);
                        //So tre hoc 2 buoi/ngay
                        sheetM4.SetCellValue(startRow, 16, objMarkResult.MarkLevel13);
                        sheetM4.SetCellValue(startRow, 17, objMarkResult.MarkLevel14);
                        sheetM4.SetCellValue(startRow, 18, objMarkResult.MarkLevel15);
                        sheetM4.SetCellValue(startRow, 19, objMarkResult.MarkLevel16);
                        sheetM4.SetCellValue(startRow, 20, objMarkResult.MarkLevel17);
                        sheetM4.SetCellValue(startRow, 21, objMarkResult.MarkLevel18);
                        //So tre DTTS hoc 2 buoi/ngay
                        sheetM4.SetCellValue(startRow, 22, objMarkResult.MarkLevel19);
                        sheetM4.SetCellValue(startRow, 23, objMarkResult.MarkLevel20);
                        sheetM4.SetCellValue(startRow, 24, objMarkResult.MarkLevel21);
                        sheetM4.SetCellValue(startRow, 25, objMarkResult.MarkLevel22);
                        sheetM4.SetCellValue(startRow, 26, objMarkResult.MarkLevel23);
                        sheetM4.SetCellValue(startRow, 27, objMarkResult.MarkLevel24); 
                        sheetM4.SetCellValue(startRow, 28, objMarkResult.MarkLevel29);
                    }
                    sheetM4.GetRange(startRow, 3, startRow, 27).NumberFormat("0;-0;;@");
                    startRow++;
                }
            }
            sheetM4.GetRange(startRow, 1, startRow, 2).Merge();
            sheetM4.GetRange(startRow, 1, startRow, 29).SetFontStyle(true, null, false, 10, false, false);
            sheetM4.SetCellValue(startRow, 1, "Tổng số");
            this.SetFormulaTotal(sheetM4, startRow, lstToRow, 3, 29);
            sheetM4.GetRange(8, 1, startRow, 28).SetBorder(VTBorderStyle.Solid, VTBorderWeight.Thin, VTBorderIndex.All);
            #endregion
            #region Fill Sheet Mau 6
            lstMarkResult = lstMark.Where(p => p.ReportCode.Equals("KinterGarten_Report_M6")).OrderBy(p => p.SchoolName).ToList();
            sheetM6.SetCellValue("A2", SupervisingDeptName.ToUpper());
            sheetM6.SetCellValue("A3", string.Format("BÁO CÁO SỐ LIỆU KHẢO SÁT TRẺ SUY DINH DƯỠNG, BÉO PHÌ ĐẦU NĂM HỌC {0} - {1}", Year, (Year + 1)));
            startRow = 7;
            lstToRow = new List<int>();
            for (int i = 0; i < lstGroupSupervisingDept.Count; i++)
            {
                var objGroupSupervisingDept = lstGroupSupervisingDept[i];
                sheetM6.GetRange(startRow, 1, startRow, 2).Merge();
                sheetM6.GetRange(startRow, 1, startRow, 32).SetFontStyle(true, null, false, 10, false, false);
                sheetM6.SetCellValue(startRow, 1, objGroupSupervisingDept.SupervisingDeptName);
                lstSchooltmp = lstSchool.Where(p => p.School.SupervisingDeptID == objGroupSupervisingDept.SuperVisingDeptID).ToList();
                lstToRow.Add(startRow);
                this.SetFormulaProvince(sheetM6, startRow, startRow + 1, startRow + lstSchooltmp.Count, 3, 33);
                startRow++;
                for (int j = 0; j < lstSchooltmp.Count; j++)
                {
                    objSchool = lstSchooltmp[j];
                    objMarkResult = lstMarkResult.Where(p => p.SuperVisingDeptID == objGroupSupervisingDept.SuperVisingDeptID && p.SchoolID == objSchool.SchoolID).FirstOrDefault();
                    sheetM6.SetCellValue(startRow, 1, (j + 1));
                    sheetM6.SetCellValue(startRow, 2, objSchool.SchoolName);
                    
                    if (objMarkResult != null)
                    {
                        //TS tre duoc kham SKDK
                        sheetM6.SetCellValue(startRow, 3, objMarkResult.MarkLevel00);
                        sheetM6.SetCellValue(startRow, 4, objMarkResult.MarkLevel01);
                        sheetM6.SetCellValue(startRow, 5, objMarkResult.MarkLevel02);
                        //So tre duoc theo doi bieu do can nang
                        sheetM6.SetCellValue(startRow, 6, objMarkResult.MarkLevel03);
                        sheetM6.SetCellValue(startRow, 7, objMarkResult.MarkLevel04);
                        sheetM6.SetCellValue(startRow, 8, objMarkResult.MarkLevel05);
                        sheetM6.SetCellValue(startRow, 9, objMarkResult.MarkLevel06);
                        //So tre bi SDD the nhe can
                        sheetM6.SetCellValue(startRow, 10, objMarkResult.MarkLevel07);
                        sheetM6.SetCellValue(startRow, 11, objMarkResult.MarkLevel08);
                        sheetM6.SetCellValue(startRow, 12, objMarkResult.MarkLevel09);
                        sheetM6.SetCellValue(startRow, 13, objMarkResult.MarkLevel10);
                        sheetM6.SetCellValue(startRow, 14, objMarkResult.MarkLevel11);
                        sheetM6.SetCellValue(startRow, 15, objMarkResult.MarkLevel12);
                        sheetM6.SetCellValue(startRow, 16, objMarkResult.MarkLevel13);
                        //So tre duoc theo doi bieu do chieu cao
                        sheetM6.SetCellValue(startRow, 17, objMarkResult.MarkLevel14);
                        sheetM6.SetCellValue(startRow, 18, objMarkResult.MarkLevel15);
                        sheetM6.SetCellValue(startRow, 19, objMarkResult.MarkLevel16);
                        sheetM6.SetCellValue(startRow, 20, objMarkResult.MarkLevel17);
                        //Tong so tre SDD thap coi
                        sheetM6.SetCellValue(startRow, 21, objMarkResult.MarkLevel18);
                        sheetM6.SetCellValue(startRow, 22, objMarkResult.MarkLevel19);
                        sheetM6.SetCellValue(startRow, 23, objMarkResult.MarkLevel20);
                        sheetM6.SetCellValue(startRow, 24, objMarkResult.MarkLevel21);
                        sheetM6.SetCellValue(startRow, 25, objMarkResult.MarkLevel22);
                        sheetM6.SetCellValue(startRow, 26, objMarkResult.MarkLevel23);
                        sheetM6.SetCellValue(startRow, 27, objMarkResult.MarkLevel24);
                        //Tre beo phi
                        sheetM6.SetCellValue(startRow, 28, objMarkResult.MarkLevel25);
                        sheetM6.SetCellValue(startRow, 29, objMarkResult.MarkLevel26);
                        sheetM6.SetCellValue(startRow, 30, objMarkResult.MarkLevel27);
                        sheetM6.SetCellValue(startRow, 31, objMarkResult.MarkLevel28);
                        sheetM6.SetCellValue(startRow, 32, objMarkResult.MarkLevel29);
                    }
                    sheetM6.GetRange(startRow, 3, startRow, 32).NumberFormat("0;-0;;@");
                    startRow++;
                }
            }
            sheetM6.GetRange(startRow, 1, startRow, 2).Merge();
            sheetM6.GetRange(startRow, 1, startRow, 32).SetFontStyle(true, null, false, 10, false, false);
            sheetM6.SetCellValue(startRow, 1, "Tổng số");
            this.SetFormulaTotal(sheetM6, startRow, lstToRow, 3, 33);
            sheetM6.GetRange(7, 1, startRow, 32).SetBorder(VTBorderStyle.Solid, VTBorderWeight.Thin, VTBorderIndex.All);
            #endregion
            #region Fill Sheet Mau 7
            lstMarkResult = lstMark.Where(p => p.ReportCode.Equals("KinterGarten_Report_M7")).OrderBy(p => p.SchoolName).ToList();
            sheetM7.SetCellValue("A2", SupervisingDeptName.ToUpper());
            startRow = 9;
            lstToRow = new List<int>();
            for (int i = 0; i < lstGroupSupervisingDept.Count; i++)
            {
                var objGroupSupervisingDept = lstGroupSupervisingDept[i];
                sheetM7.GetRange(startRow, 1, startRow, 2).Merge();
                sheetM7.GetRange(startRow, 1, startRow, 30).SetFontStyle(true, null, false, 10, false, false);
                sheetM7.SetCellValue(startRow, 1, objGroupSupervisingDept.SupervisingDeptName);
                lstSchooltmp = lstSchool.Where(p => p.School.SupervisingDeptID == objGroupSupervisingDept.SuperVisingDeptID).ToList();
                lstToRow.Add(startRow);
                this.SetFormulaProvince(sheetM7, startRow, startRow + 1, startRow + lstSchooltmp.Count, 3, 31);
                startRow++;
                for (int j = 0; j < lstSchooltmp.Count; j++)
                {
                    objSchool = lstSchooltmp[j];
                    objMarkResult = lstMarkResult.Where(p => p.SuperVisingDeptID == objGroupSupervisingDept.SuperVisingDeptID && p.SchoolID == objSchool.SchoolID).FirstOrDefault();
                    sheetM7.SetCellValue(startRow, 1, (j + 1));
                    sheetM7.SetCellValue(startRow, 2, objSchool.SchoolName);
                    if (objMarkResult != null)
                    {
                        //Hieu truong
                        sheetM7.SetCellValue(startRow, 5, objMarkResult.MarkLevel02);
                        sheetM7.SetCellValue(startRow, 6, objMarkResult.MarkLevel03);
                        sheetM7.SetCellValue(startRow, 7, objMarkResult.MarkLevel41);//trinh do VH
                        sheetM7.SetCellValue(startRow, 8, objMarkResult.MarkLevel42);//trinh do CM
                        sheetM7.SetCellValue(startRow, 9, objMarkResult.MarkLevel06);
                        //Pho hieu truong
                        sheetM7.SetCellValue(startRow, 12, objMarkResult.MarkLevel09);
                        sheetM7.SetCellValue(startRow, 13, objMarkResult.MarkLevel10);
                        sheetM7.SetCellValue(startRow, 14, objMarkResult.MarkLevel11);
                        sheetM7.SetCellValue(startRow, 15, objMarkResult.MarkLevel12);
                        sheetM7.SetCellValue(startRow, 16, objMarkResult.MarkLevel13);
                        sheetM7.SetCellValue(startRow, 17, objMarkResult.MarkLevel14);
                        sheetM7.SetCellValue(startRow, 18, objMarkResult.MarkLevel15);
                        sheetM7.SetCellValue(startRow, 19, objMarkResult.MarkLevel16);
                        //Giao vien
                        sheetM7.SetCellValue(startRow, 22, objMarkResult.MarkLevel19);
                        sheetM7.SetCellValue(startRow, 23, objMarkResult.MarkLevel20);
                        sheetM7.SetCellValue(startRow, 24, objMarkResult.MarkLevel21);
                        sheetM7.SetCellValue(startRow, 25, objMarkResult.MarkLevel22);
                        sheetM7.SetCellValue(startRow, 26, objMarkResult.MarkLevel23);
                        sheetM7.SetCellValue(startRow, 27, objMarkResult.MarkLevel24);
                        sheetM7.SetCellValue(startRow, 28, objMarkResult.MarkLevel25);
                        sheetM7.SetCellValue(startRow, 29, objMarkResult.MarkLevel26);
                        sheetM7.SetCellValue(startRow, 30, objMarkResult.MarkLevel27);
                    }
                    sheetM7.GetRange(startRow, 3, startRow, 30).NumberFormat("0;-0;;@");
                    startRow++;
                }
            }
            sheetM7.GetRange(startRow, 1, startRow, 2).Merge();
            sheetM7.GetRange(startRow, 1, startRow, 30).SetFontStyle(true, null, false, 10, false, false);
            sheetM7.SetCellValue(startRow, 1, "Tổng số");
            this.SetFormulaTotal(sheetM7, startRow, lstToRow, 3, 31);
            sheetM7.GetRange(9, 1, startRow, 30).SetBorder(VTBorderStyle.Solid, VTBorderWeight.Thin, VTBorderIndex.All);
            #endregion
            return oBook.ToStream();
        }
        #region Private Method
        /// <summary>
        /// Xuat excel tinh hinh nhap lieu
        /// </summary>
        /// <param name="sheet"></param>
        /// <param name="lstMark"></param>
        /// <param name="SearchInfo"></param>
        /// <returns></returns>
        private void ExportInputMonitoringSituation(IVTWorksheet sheet, List<MarkStatisticBO> lstMark, IDictionary<string, object> SearchInfo)
        {
            int StartRow = 7;
            int appliedLevel = Utils.GetInt(SearchInfo, "AppliedLevel");
            int year = Utils.GetInt(SearchInfo, "Year");
            int semester = Utils.GetInt(SearchInfo, "Semester");
            int provinceId = Utils.GetInt(SearchInfo, "ProvinceID");
            int reportTypeId = Utils.GetInt(SearchInfo, "ReportType");
            Province objProvince = ProvinceBusiness.Find(provinceId);
            string appliedLevelName = "";
            if (appliedLevel == SystemParamsInFile.APPLIED_LEVEL_CRECHE)
            {
                appliedLevelName = "Cấp NT/MG";
            }
            else if (appliedLevel == SystemParamsInFile.APPLIED_LEVEL_KINDER_GARTEN)
            {
                appliedLevelName = "Cấp NT/MG";
            }
            else if (appliedLevel == SystemParamsInFile.APPLIED_LEVEL_PRIMARY)
            {
                appliedLevelName = "Cấp TH";
            }
            else if (appliedLevel == SystemParamsInFile.APPLIED_LEVEL_SECONDARY)
            {
                appliedLevelName = "Cấp THCS";
            }
            else if (appliedLevel == SystemParamsInFile.APPLIED_LEVEL_TERTIARY)
            {
                appliedLevelName = "Cấp THPT";
            }
            string subTitle = string.Empty;
            if (appliedLevel == GlobalConstants.APPLIED_LEVEL_PRIMARY)
            {
                subTitle = String.Format("{0} - {1} - Năm học {2} - {3}", appliedLevelName, reportTypeId == 1 ? "Giữa kỳ I" : reportTypeId == 2 ? "Giữa kỳ II" : reportTypeId == 3 ? "Cuối kỳ I" : "Cuối kỳ II", year, year + 1);
            }
            else
            {
                subTitle = String.Format("{0} - Học kỳ {1} - Năm học {2} - {3}", appliedLevelName, (semester == 1) ? "I" : "II", year, year + 1);
            }
            sheet.SetCellValue(3, 1, subTitle);
            if (lstMark == null || lstMark.Count == 0)
            {
                return;
            }
            int row = StartRow;
            MarkStatisticBO objMark;

            if (appliedLevel == 2 || appliedLevel == 3)
            {
                #region "Dien du lieu vao excel cho bao cao cap 2, 3"
                for (int i = 0; i < lstMark.Count; i++)
                {
                    objMark = lstMark[i];
                    if (i > 0)
                    {
                        sheet.CopyPasteSameRowHeigh(sheet.GetRange("A7", "O7"), row);
                    }
                    sheet.SetCellValue(row, 1, i + 1);
                    sheet.SetCellValue(row, 2, objProvince.ProvinceName);
                    sheet.SetCellValue(row, 3, objMark.DistrictName);
                    sheet.SetCellValue(row, 4, objMark.SchoolName);
                    //Sohs
                    sheet.SetCellValue(row, 5, objMark.MarkLevel00 > 0 ? objMark.MarkLevel00 : 0);
                    //soGV
                    sheet.SetCellValue(row, 6, objMark.MarkLevel01 > 0 ? objMark.MarkLevel01 : 0);
                    //soLop
                    sheet.SetCellValue(row, 7, objMark.MarkLevel02 > 0 ? objMark.MarkLevel02 : 0);
                    //soLopKhaiBaoMonHoc
                    sheet.SetCellValue(row, 8, objMark.MarkLevel03 > 0 ? objMark.MarkLevel03 : 0);
                    //soLopPCGD
                    sheet.SetCellValue(row, 9, objMark.MarkLevel04 > 0 ? objMark.MarkLevel04 : 0);
                    //soLopNhapDiem
                    sheet.SetCellValue(row, 10, objMark.MarkLevel05 > 0 ? objMark.MarkLevel05 : 0);
                    //soLopnhapHK
                    sheet.SetCellValue(row, 11, objMark.MarkLevel06 > 0 ? objMark.MarkLevel06 : 0);
                    //soLopTKD
                    sheet.SetCellValue(row, 12, objMark.MarkLevel07 > 0 ? objMark.MarkLevel07 : 0);
                    //soLopXepLoai
                    sheet.SetCellValue(row, 13, objMark.MarkLevel08 > 0 ? objMark.MarkLevel08 : 0);
                    //soUser
                    sheet.SetCellValue(row, 14, objMark.MarkLevel09 > 0 ? objMark.MarkLevel09 : 0);
                    //soUserLogGin
                    sheet.SetCellValue(row, 15, objMark.MarkLevel10 > 0 ? objMark.MarkLevel10 : 0);
                    row++;
                }
                SetFormulaProvince(sheet, StartRow - 1, StartRow, row - 1, 5, 16);
                #endregion
            }
            else if (appliedLevel == 1)
            {
                #region Dien du lieu cho cap 1
                for (int i = 0; i < lstMark.Count; i++)
                {
                    objMark = lstMark[i];
                    if (i > 0)
                    {
                        sheet.CopyPasteSameRowHeigh(sheet.GetRange("A7", "O7"), row);
                    }
                    sheet.SetCellValue(row, 1, i + 1);
                    sheet.SetCellValue(row, 2, objProvince.ProvinceName);
                    sheet.SetCellValue(row, 3, objMark.DistrictName);
                    sheet.SetCellValue(row, 4, objMark.SchoolName);
                    //Sohs
                    sheet.SetCellValue(row, 5, objMark.MarkLevel00 > 0 ? objMark.MarkLevel00 : 0);
                    //soGV
                    sheet.SetCellValue(row, 6, objMark.MarkLevel01 > 0 ? objMark.MarkLevel01 : 0);
                    //soLop
                    sheet.SetCellValue(row, 7, objMark.MarkLevel02 > 0 ? objMark.MarkLevel02 : 0);
                    //soLopKhaiBaoMonHoc
                    sheet.SetCellValue(row, 8, objMark.MarkLevel03 > 0 ? objMark.MarkLevel03 : 0);
                    //soLopPCGD
                    sheet.SetCellValue(row, 9, objMark.MarkLevel04 > 0 ? objMark.MarkLevel04 : 0);
                    //soHS da nhap danh gia MH
                    sheet.SetCellValue(row, 10, objMark.MarkLevel05 > 0 ? objMark.MarkLevel05 : 0);
                    //soHS da nhap danh gia nang luc
                    sheet.SetCellValue(row, 11, objMark.MarkLevel06 > 0 ? objMark.MarkLevel06 : 0);
                    //soHS da nhap danh gia pham chat
                    sheet.SetCellValue(row, 12, objMark.MarkLevel07 > 0 ? objMark.MarkLevel07 : 0);
                    //soHS da danh gia cuoi ky
                    sheet.SetCellValue(row, 13, objMark.MarkLevel08 > 0 ? objMark.MarkLevel08 : 0);
                    //soUser
                    sheet.SetCellValue(row, 14, objMark.MarkLevel09 > 0 ? objMark.MarkLevel09 : 0);
                    //soUserLogGin
                    sheet.SetCellValue(row, 15, objMark.MarkLevel10 > 0 ? objMark.MarkLevel10 : 0);
                    row++;
                }
                SetFormulaProvince(sheet, StartRow - 1, StartRow, row - 1, 5, 16);
                #endregion
            }
            else
            {
                #region Dien du lieu cho cap NT, MG
                for (int i = 0; i < lstMark.Count; i++)
                {
                    objMark = lstMark[i];
                    if (i > 0)
                    {
                        sheet.CopyPasteSameRowHeigh(sheet.GetRange("A7", "L7"), row);
                    }
                    sheet.SetCellValue(row, 1, i + 1);
                    sheet.SetCellValue(row, 2, objProvince.ProvinceName);
                    sheet.SetCellValue(row, 3, objMark.DistrictName);
                    sheet.SetCellValue(row, 4, objMark.SchoolName);
                    //Sohs
                    sheet.SetCellValue(row, 5, objMark.MarkLevel00 > 0 ? objMark.MarkLevel00 : 0);
                    //soGV
                    sheet.SetCellValue(row, 6, objMark.MarkLevel01 > 0 ? objMark.MarkLevel01 : 0);
                    //soLop
                    sheet.SetCellValue(row, 7, objMark.MarkLevel02 > 0 ? objMark.MarkLevel02 : 0);
                    //soLopPCGD
                    sheet.SetCellValue(row, 8, objMark.MarkLevel04 > 0 ? objMark.MarkLevel04 : 0);
                    //soLopNhapDiem
                    sheet.SetCellValue(row, 9, objMark.MarkLevel05 > 0 ? objMark.MarkLevel05 : 0);
                    //soLopnhapHK
                    sheet.SetCellValue(row, 10, objMark.MarkLevel06 > 0 ? objMark.MarkLevel06 : 0);
                    //soUser
                    sheet.SetCellValue(row, 11, objMark.MarkLevel09 > 0 ? objMark.MarkLevel09 : 0);
                    //soUserLogGin
                    sheet.SetCellValue(row, 12, objMark.MarkLevel10 > 0 ? objMark.MarkLevel10 : 0);
                    row++;
                }
                SetFormulaProvince(sheet, StartRow - 1, StartRow, row - 1, 5, 13);
                #endregion
            }


        }

        /// <summary>
        /// lay du lieu tinh hinh nhap 
        /// </summary>
        /// <param name="SearchInfo"></param>
        /// <returns></returns>
        private List<MarkStatisticBO> GetDataMarkStatistic(IDictionary<string, object> SearchInfo)
        {
            int provinceId = Utils.GetInt(SearchInfo, "ProvinceID");
            int last2digitProvince = UtilsBusiness.GetPartionId(provinceId, 64);
            int districtId = Utils.GetInt(SearchInfo, "DistrictID");
            int superVisingDeptId = Utils.GetInt(SearchInfo, "SuperVisingDeptID"); /*0: so, 1: phong, 2: admin */
            int year = Utils.GetInt(SearchInfo, "Year");
            string ReportCode = Utils.GetString(SearchInfo, "ReportCode");
            int semester = Utils.GetInt(SearchInfo, "Semester");
            int appliedLevel = Utils.GetInt(SearchInfo, "AppliedLevel");
            int markType = Utils.GetInt(SearchInfo, "MarkType");
            int unitId = Utils.GetInt(SearchInfo, "UnitId");
            //string sUnitId = unitId.ToString();
            int[] ArrEducationGrade = null;
            if (appliedLevel == GlobalConstants.APPLIED_LEVEL_PRIMARY)
            {
                ArrEducationGrade = new int[7] { 1, 3, 7, 9, 17, 25, 31 };
            }
            else if (appliedLevel == GlobalConstants.APPLIED_LEVEL_SECONDARY)
            {
                ArrEducationGrade = new int[4] { 2, 3, 6, 7 };
            }
            else if (appliedLevel == GlobalConstants.APPLIED_LEVEL_TERTIARY)
            {
                ArrEducationGrade = new int[3] { 4, 6, 7 };
            }
            else if (appliedLevel == GlobalConstants.APPLIED_LEVEL_CRECHE || appliedLevel == GlobalConstants.APPLIED_LEVEL_KINDER_GARTEN)
            {
                ArrEducationGrade = new int[4] { 8, 16, 24, 31 };
            }
            //Lay du lieu tong hop
            IQueryable<MarkStatistic> lstMarkStatistic = from m in MarkStatisticBusiness.AllNoTracking
                                                         where m.Last2DigitNumberProvince == last2digitProvince
                                                          && m.ProvinceID == provinceId
                                                          && m.Year == year
                                                          && m.AppliedLevel == appliedLevel
                                                          && m.Semester == semester
                                                          && m.MarkType == markType
                                                          && m.ReportCode == ReportCode
                                                          && m.SuperVisingDeptID == superVisingDeptId
                                                         select m;

            //Xoa du lieu truoc khi tong hop
            IQueryable<MarkStatisticBO> iqMarkStatistic = from sp in SchoolProfileBusiness.AllNoTracking
                                                          join d in DistrictBusiness.AllNoTracking on sp.DistrictID equals d.DistrictID
                                                          join ms in lstMarkStatistic on sp.SchoolProfileID equals ms.SchoolID into gs
                                                          from m in gs.DefaultIfEmpty()
                                                          where d.ProvinceID == provinceId
                                                           && ArrEducationGrade.Contains(sp.EducationGrade)
                                                           && sp.IsActive
                                                          select new MarkStatisticBO
                                                          {
                                                              //AcademicYearID = m.AcademicYearID,
                                                              //AppliedLevel = m.AppliedLevel,
                                                              BelowAverage = m.BelowAverage,
                                                              //CriteriaReportID = m.CriteriaReportID,
                                                              DistrictCode = d.DistrictCode,
                                                              DistrictName = d.DistrictName,
                                                              DistrictID = d.DistrictID,
                                                              //EducationLevelID = m.EducationLevelID,
                                                              //Last2DigitNumberProvince = m.Last2DigitNumberProvince,
                                                              MarkLevel00 = m.MarkLevel00,
                                                              MarkLevel01 = m.MarkLevel01,
                                                              MarkLevel02 = m.MarkLevel02,
                                                              MarkLevel03 = m.MarkLevel03,
                                                              MarkLevel04 = m.MarkLevel04,
                                                              MarkLevel05 = m.MarkLevel05,
                                                              MarkLevel06 = m.MarkLevel06,
                                                              MarkLevel07 = m.MarkLevel07,
                                                              MarkLevel08 = m.MarkLevel08,
                                                              MarkLevel09 = m.MarkLevel09,
                                                              MarkLevel10 = m.MarkLevel10,
                                                              MarkLevel11 = m.MarkLevel11,
                                                              MarkLevel12 = m.MarkLevel12,
                                                              MarkLevel13 = m.MarkLevel13,
                                                              MarkLevel14 = m.MarkLevel14,
                                                              MarkLevel15 = m.MarkLevel15,
                                                              MarkLevel16 = m.MarkLevel16,
                                                              MarkLevel17 = m.MarkLevel17,
                                                              MarkLevel18 = m.MarkLevel18,
                                                              MarkLevel19 = m.MarkLevel19,
                                                              MarkLevel20 = m.MarkLevel20,
                                                              MarkLevel21 = m.MarkLevel21,
                                                              MarkLevel22 = m.MarkLevel22,
                                                              MarkLevel23 = m.MarkLevel23,
                                                              MarkLevel24 = m.MarkLevel24,
                                                              MarkLevel25 = m.MarkLevel25,
                                                              MarkLevel26 = m.MarkLevel26,
                                                              MarkLevel27 = m.MarkLevel27,
                                                              MarkLevel28 = m.MarkLevel28,
                                                              MarkLevel29 = m.MarkLevel29,
                                                              MarkLevel30 = m.MarkLevel30,
                                                              MarkLevel31 = m.MarkLevel31,
                                                              MarkLevel32 = m.MarkLevel32,
                                                              MarkLevel33 = m.MarkLevel33,
                                                              MarkLevel34 = m.MarkLevel34,
                                                              MarkLevel35 = m.MarkLevel35,
                                                              MarkLevel36 = m.MarkLevel36,
                                                              MarkLevel37 = m.MarkLevel37,
                                                              MarkLevel38 = m.MarkLevel38,
                                                              MarkLevel39 = m.MarkLevel39,
                                                              MarkLevel40 = m.MarkLevel40,
                                                              MarkTotal = m.MarkTotal,
                                                              //MarkType = m.MarkType,
                                                              OnAverage = m.OnAverage,
                                                              //ProvinceID = m.ProvinceID,
                                                              ProcessedDate = m.ProcessedDate,
                                                              PupilTotal = m.PupilTotal,
                                                              ReportCode = m.ReportCode,
                                                              SchoolCode = sp.SchoolCode,
                                                              SchoolID = sp.SchoolProfileID,
                                                              SchoolName = sp.SchoolName,
                                                              //Semester = m.Semester,
                                                              //SuperVisingDeptID = m.SuperVisingDeptID,
                                                              //Year = m.Year
                                                          };
            if (districtId > 0)
            {
                iqMarkStatistic = iqMarkStatistic.Where(m => m.DistrictID == districtId);
            }
            List<MarkStatisticBO> lstMark = iqMarkStatistic.OrderBy(o => o.DistrictID).ThenBy(o => o.SchoolName).ToList();
            return lstMark;
        }

        /// <summary>
        /// Dien cong thuc tinh tong so cho cot toan tinh
        /// </summary>
        /// <param name="sheet"></param>
        /// <param name="fromRow"></param>
        /// <param name="lsttoRow"></param>
        private void SetFormulaProvince(IVTWorksheet sheet, int rowTotal, int fromRow, int toRow, int fromColumn, int toColumn)
        {

            string sumValue = "";
            for (int i = fromColumn; i < toColumn; i++)
            {
                sumValue = "";
                sumValue = String.Format("=SUM({0}:{1})", new VTVector(fromRow, i).ToString(), new VTVector(toRow, i).ToString());
                sheet.SetFormulaValue(rowTotal, i, sumValue);
            }
        }

        private void SetFormulaTotal(IVTWorksheet sheet, int fromRow, List<int> lsttoRow, int fromColumn, int toColumn)
        {
            VTVector fromCell;
            string sumValue = "";
            int toRow = 0;
            List<string> lstVal;
            for (int i = fromColumn; i < toColumn; i++)
            {
                sumValue = "";
                lstVal = new List<string>();
                for (int row = 0; row < lsttoRow.Count; row++)
                {
                    toRow = lsttoRow[row];
                    fromCell = new VTVector(toRow, i);
                    lstVal.Add(fromCell.ToString());
                }
                if (lstVal != null && lstVal.Count > 0)
                {
                    sumValue = String.Format("=SUM({0})", String.Join(",", lstVal.ToArray()));
                    sheet.SetFormulaValue(fromRow, i, sumValue);

                }
            }
        }

        /// <summary>
        /// Them moi tinh hinh nhap lieu
        /// </summary>
        /// <param name="provinceId"></param>
        /// <param name="districtId"></param>
        /// <param name="supervisingDeptId"></param>
        /// <param name="schoolId"></param>
        /// <param name="academicYearId"></param>
        /// <param name="year"></param>
        /// <param name="appliedLevel"></param>
        /// <param name="semester"></param>
        /// <param name="markType"></param>
        /// <param name="reportCode"></param>
        private void InsertMarkStatistc(int provinceId, int districtId, int supervisingDeptId, int schoolId, int academicYearId, int year, int appliedLevel,
                                        int semester, int markType, string reportCode, int reportType = 0)
        {
            SMASEntities contenxt1 = new SMASEntities();

            using (OracleConnection conn = new OracleConnection(contenxt1.Database.Connection.ConnectionString))
            {
                try
                {
                    #region "Tong hop du lieu diem dinh ky, hoc ky"
                    if (conn != null)
                    {
                        if (conn.State != ConnectionState.Open)
                        {
                            conn.Open();
                        }
                        using (OracleTransaction tran = conn.BeginTransaction())
                        {
                            try
                            {
                                OracleCommand command = new OracleCommand
                                {
                                    CommandType = CommandType.StoredProcedure,
                                    CommandText = "SP_INSERT_INPUT_SITUATION",
                                    Connection = conn,
                                    CommandTimeout = 0
                                };
                                command.Parameters.Add("P_PROVINCE_ID", provinceId);
                                command.Parameters.Add("P_DISTRICT_ID", districtId);
                                command.Parameters.Add("P_SUPER_VISING_DEPT_ID", supervisingDeptId);
                                command.Parameters.Add("P_SCHOOL_ID", schoolId);
                                command.Parameters.Add("P_ACADEMIC_YEAR_ID", academicYearId);
                                command.Parameters.Add("P_YEAR", year);
                                command.Parameters.Add("P_APPLIED_LEVEL", appliedLevel);
                                command.Parameters.Add("P_SEMESTER_ID", semester);
                                command.Parameters.Add("P_REPORT_CODE", reportCode);
                                command.Parameters.Add("P_REPORT_TYPE", reportType);
                                command.Parameters.Add("P_MARK_TYPE_ID", markType);
                                command.ExecuteNonQuery();
                                tran.Commit();
                            }
                            catch (Exception)
                            {
                                tran.Rollback();
                            }
                            finally
                            {
                                tran.Dispose();
                            }
                        }
                    }
                    #endregion
                }
                finally
                {
                    if (conn != null)
                    {
                        conn.Close();
                    }
                }
            }
        }
        private void InsertKinderGartenReport(int provinceId, int districtId,int schoolId,int supervisingDeptID, int academicYearId, int year,string reportCode)
        {
            SMASEntities contenxt1 = new SMASEntities();
            using (OracleConnection conn = new OracleConnection(contenxt1.Database.Connection.ConnectionString))
            {
                try
                {
                    switch (reportCode)
                    {
                        case "KinterGarten_Report_M2":
                        {
                            #region KinterGartenReport M2
                            if (conn != null)
                            {
                                if (conn.State != ConnectionState.Open)
                                {
                                    conn.Open();
                                }
                                using (OracleTransaction tran = conn.BeginTransaction())
                                {
                                    try
                                    {
                                        OracleCommand command = new OracleCommand
                                        {
                                            CommandType = CommandType.StoredProcedure,
                                            CommandText = "SP_GETKINDERGARTEN_REPORT_M2",
                                            Connection = conn,
                                            CommandTimeout = 0
                                        };
                                        command.Parameters.Add("P_PROVINCE_ID", provinceId);
                                        command.Parameters.Add("P_DISTRICT_ID", districtId);
                                        command.Parameters.Add("P_SCHOOL_ID", schoolId);
                                        command.Parameters.Add("P_SUPER_VISING_DEPT_ID", supervisingDeptID);
                                        command.Parameters.Add("P_REPORT_CODE", reportCode);
                                        command.Parameters.Add("P_YEAR_ID", year);
                                        command.Parameters.Add("P_ACADEMIC_YEAR_ID", academicYearId);
                                        command.ExecuteNonQuery();
                                        tran.Commit();
                                    }
                                    catch (Exception)
                                    {
                                        tran.Rollback();
                                    }
                                    finally
                                    {
                                        tran.Dispose();
                                    }
                                }
                            }
                            #endregion
                            break;
                        }
                        case "KinterGarten_Report_M3":
                        {
                            #region KinterGartenReport M3
                            if (conn != null)
                            {
                                if (conn.State != ConnectionState.Open)
                                {
                                    conn.Open();
                                }
                                using (OracleTransaction tran = conn.BeginTransaction())
                                {
                                    try
                                    {
                                        OracleCommand command = new OracleCommand
                                        {
                                            CommandType = CommandType.StoredProcedure,
                                            CommandText = "SP_GETKINDERGARTEN_REPORT_M3",
                                            Connection = conn,
                                            CommandTimeout = 0
                                        };
                                        command.Parameters.Add("P_PROVINCE_ID", provinceId);
                                        command.Parameters.Add("P_DISTRICT_ID", districtId);
                                        command.Parameters.Add("P_SCHOOL_ID", schoolId);
                                        command.Parameters.Add("P_SUPER_VISING_DEPT_ID", supervisingDeptID);
                                        command.Parameters.Add("P_REPORT_CODE", reportCode);
                                        command.Parameters.Add("P_YEAR_ID", year);
                                        command.Parameters.Add("P_ACADEMIC_YEAR_ID", academicYearId);
                                        command.ExecuteNonQuery();
                                        tran.Commit();
                                    }
                                    catch (Exception)
                                    {
                                        tran.Rollback();
                                    }
                                    finally
                                    {
                                        tran.Dispose();
                                    }
                                }
                            }
                            #endregion
                            break;
                        }
                        case "KinterGarten_Report_M4":
                        {
                            #region KinterGartenReport M4
                            if (conn != null)
                            {
                                if (conn.State != ConnectionState.Open)
                                {
                                    conn.Open();
                                }
                                using (OracleTransaction tran = conn.BeginTransaction())
                                {
                                    try
                                    {
                                        OracleCommand command = new OracleCommand
                                        {
                                            CommandType = CommandType.StoredProcedure,
                                            CommandText = "SP_GETKINDERGARTEN_REPORT_M4",
                                            Connection = conn,
                                            CommandTimeout = 0
                                        };
                                        command.Parameters.Add("P_PROVINCE_ID", provinceId);
                                        command.Parameters.Add("P_DISTRICT_ID", districtId);
                                        command.Parameters.Add("P_SCHOOL_ID", schoolId);
                                        command.Parameters.Add("P_SUPER_VISING_DEPT_ID", supervisingDeptID);
                                        command.Parameters.Add("P_REPORT_CODE", reportCode);
                                        command.Parameters.Add("P_YEAR_ID", year);
                                        command.Parameters.Add("P_ACADEMIC_YEAR_ID", academicYearId);
                                        command.ExecuteNonQuery();
                                        tran.Commit();
                                    }
                                    catch (Exception)
                                    {
                                        tran.Rollback();
                                    }
                                    finally
                                    {
                                        tran.Dispose();
                                    }
                                }
                            }
                            #endregion
                            break;
                        }
                        case "KinterGarten_Report_M6":
                        {
                            #region KinterGartenReport M6
                            if (conn != null)
                            {
                                if (conn.State != ConnectionState.Open)
                                {
                                    conn.Open();
                                }
                                using (OracleTransaction tran = conn.BeginTransaction())
                                {
                                    try
                                    {
                                        OracleCommand command = new OracleCommand
                                        {
                                            CommandType = CommandType.StoredProcedure,
                                            CommandText = "SP_GETKINDERGARTEN_REPORT_M6",
                                            Connection = conn,
                                            CommandTimeout = 0
                                        };
                                        command.Parameters.Add("P_PROVINCE_ID", provinceId);
                                        command.Parameters.Add("P_DISTRICT_ID", districtId);
                                        command.Parameters.Add("P_SCHOOL_ID", schoolId);
                                        command.Parameters.Add("P_SUPER_VISING_DEPT_ID", supervisingDeptID);
                                        command.Parameters.Add("P_REPORT_CODE", reportCode);
                                        command.Parameters.Add("P_YEAR_ID", year);
                                        command.Parameters.Add("P_MONTH_ID", int.Parse(year + "09"));
                                        command.Parameters.Add("P_ACADEMIC_YEAR_ID", academicYearId);
                                        command.ExecuteNonQuery();
                                        tran.Commit();
                                    }
                                    catch (Exception)
                                    {
                                        tran.Rollback();
                                    }
                                    finally
                                    {
                                        tran.Dispose();
                                    }
                                }
                            }
                            #endregion
                            break;
                        }
                        case "KinterGarten_Report_M7":
                        {
                            #region KinterGartenReport M7
                            if (conn != null)
                            {
                                if (conn.State != ConnectionState.Open)
                                {
                                    conn.Open();
                                }
                                using (OracleTransaction tran = conn.BeginTransaction())
                                {
                                    try
                                    {
                                        OracleCommand command = new OracleCommand
                                        {
                                            CommandType = CommandType.StoredProcedure,
                                            CommandText = "SP_GETKINDERGARTEN_REPORT_M7",
                                            Connection = conn,
                                            CommandTimeout = 0
                                        };
                                        command.Parameters.Add("P_PROVINCE_ID", provinceId);
                                        command.Parameters.Add("P_DISTRICT_ID", districtId);
                                        command.Parameters.Add("P_SCHOOL_ID", schoolId);
                                        command.Parameters.Add("P_SUPER_VISING_DEPT_ID", supervisingDeptID);
                                        command.Parameters.Add("P_REPORT_CODE", reportCode);
                                        command.Parameters.Add("P_YEAR_ID", year);
                                        command.Parameters.Add("P_ACADEMIC_YEAR_ID", academicYearId);
                                        command.ExecuteNonQuery();
                                        tran.Commit();
                                    }
                                    catch (Exception)
                                    {
                                        tran.Rollback();
                                    }
                                    finally
                                    {
                                        tran.Dispose();
                                    }
                                }
                            }
                            #endregion
                            break;
                        }
                    }
                    
                }
                finally
                {
                    if (conn != null)
                    {
                        conn.Close();
                    }
                }
            }
        }
        #endregion
    }
}
