﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Linq.Expressions;
using System.Web;
using log4net;

using SMAS.DAL.Repository;
using SMAS.DAL.IRepository;
using SMAS.Business.IBusiness;
using SMAS.Models.Models;
using SMAS.Business.BusinessObject;
using SMAS.Business.Common;
using System.IO;
using SMAS.VTUtils.Excel.Export;

namespace SMAS.Business.Business
{
    public class ReportPupilRankingBusiness : GenericBussiness<ProcessedReport>, IReportPupilRankingBusiness
    {
        IProcessedReportParameterRepository ProcessedReportParameterRepository;
        IProcessedReportRepository ProcessedReportRepository;
        IEducationLevelRepository EducationLevelRepository;
        IAcademicYearRepository AcademicYearRepository;
        IPupilRankingRepository PupilRankingRepository;
        IPupilProfileRepository PupilProfileRepository;
        ISummedUpRecordRepository SummedUpRecordRepository;

        public ReportPupilRankingBusiness(ILog logger, SMASEntities context = null)
            : base(logger)
        {
            if (context == null) { context = new SMASEntities(); } this.context = context;
            this.ProcessedReportParameterRepository = new ProcessedReportParameterRepository(context);
            this.ProcessedReportRepository = new ProcessedReportRepository(context);
            this.EducationLevelRepository = new EducationLevelRepository(context);
            this.AcademicYearRepository = new AcademicYearRepository(context);
            this.PupilRankingRepository = new PupilRankingRepository(context);
            this.PupilProfileRepository = new PupilProfileRepository(context);
            this.SummedUpRecordRepository = new SummedUpRecordRepository(context);
        }

        #region Tạo mảng băm cho các tham số đầu vào cho báo cáo
        public string GetHashKeyPrimary(ReportPupilRankingSearchBO entity)
        {
            IDictionary<string, object> dic = new Dictionary<string, object> {
                {"AcademicYearID", entity.AcademicYearID},
                {"SchoolID", entity.SchoolID},
                {"Semester", entity.Semester}
            };
            return ReportUtils.GetHashKey(dic);
        }
        #endregion

        #region Lấy báo cáo thống kê xếp loại đạo đức cấp 1 được cập nhật mới nhất
        public ProcessedReport GetReportCapacityPrimary(ReportPupilRankingSearchBO entity)
        {
            string reportCode = SystemParamsInFile.REPORT_DS_THONG_KE_XEP_LOAI_GIAO_DUC_CAP1;
            string inputParameterHashKey = GetHashKeyPrimary(entity);
            return ProcessedReportBusiness.GetProcessedReport(reportCode, inputParameterHashKey);
        }
        #endregion

        #region Lưu báo cáo thống kê xếp loại đạo đức cấp 1 vào CSDL
        public ProcessedReport InsertReportCapacityPrimary(ReportPupilRankingSearchBO entity, Stream data)
        {
            string reportCode = SystemParamsInFile.REPORT_DS_THONG_KE_XEP_LOAI_GIAO_DUC_CAP1;
            ReportDefinition reportDef = ReportDefinitionBusiness.GetByCode(reportCode);
            ProcessedReport pr = new ProcessedReport();
            pr.ReportCode = reportCode;
            pr.ProcessedDate = DateTime.Now;
            pr.InputParameterHashKey = GetHashKeyPrimary(entity);
            pr.ReportData = ReportUtils.Compress(data);

            //Tạo tên file
            string outputNamePattern = reportDef.OutputNamePattern;
            string semester = ReportUtils.ConvertSemesterForReportName(entity.Semester);

            outputNamePattern = outputNamePattern.Replace("[Semester]", semester);
            pr.ReportName = ReportUtils.RemoveSpecialCharacters(outputNamePattern) + "." + reportDef.OutputFormat;

            IDictionary<string, object> dic = new Dictionary<string, object> {
                {"AcademicYearID", entity.AcademicYearID},
                {"SchoolID", entity.SchoolID},
                {"Semester", entity.Semester}
            };
            ProcessedReportParameterRepository.Insert(dic, pr);
            ProcessedReportRepository.Insert(pr);
            ProcessedReportRepository.Save();
            return pr;
        }
        #endregion

        #region Tạo file báo cáo thống kê xếp loại đạo đức cấp 1
        public Stream CreateReportCapacityPrimary(ReportPupilRankingSearchBO entity)
        {
            string reportCode = SystemParamsInFile.REPORT_DS_THONG_KE_XEP_LOAI_GIAO_DUC_CAP1;
            ReportDefinition reportDef = ReportDefinitionBusiness.GetByCode(reportCode);
            string templatePath = ReportUtils.GetTemplatePath(reportDef);
            IVTWorkbook oBook = VTExport.OpenWorkbook(templatePath);

            int firstRow = 9;
            int defaultNumbeOfColumn = 4;
            int firstDynamicCol = VTVector.dic['E'];
            int defaultLastCol = VTVector.dic['N'];
            //Lấy sheet template
            IVTWorksheet firstSheet = oBook.GetSheet(1);
            IVTRange subjectTemp = firstSheet.GetRange(firstRow, firstDynamicCol, firstRow + 6, firstDynamicCol + 1);

            //Tạo template chuẩn
            IVTWorksheet tempSheet = oBook.CopySheetToLast(firstSheet, "N" + (firstRow - 1));
            tempSheet.CopyPasteSameSize(firstSheet.GetRange(firstRow, 1, firstRow + 6, firstDynamicCol - 1), "A" + firstRow);
            tempSheet.CopyPaste(firstSheet.GetRange(firstRow + 8, 1, firstRow + 8, 100), firstRow + 8, 1);

            //Lấy danh sách các cột động
            List<StatisticsConfig> listSC = StatisticsConfigBusiness.GetList(
                SystemParamsInFile.APPLIED_LEVEL_PRIMARY,
                SystemParamsInFile.StatisticsConfig.REPORT_TYPE_HOC_LUC,
                SystemParamsInFile.StatisticsConfig.MARK_TYPE_ALL);


            //Tạo các cột động
            Dictionary<StatisticsConfig, int> dicStatisticsConfigToColumn = new Dictionary<StatisticsConfig, int>();
            for (int i = 0; i < listSC.Count; i++)
            {
                StatisticsConfig sc = listSC[i];
                tempSheet.CopyPasteSameSize(subjectTemp, firstRow, firstDynamicCol + i * 2);
                tempSheet.SetCellValue(firstRow, firstDynamicCol + i * 2, sc.DisplayName);
                dicStatisticsConfigToColumn.Add(sc, firstDynamicCol + i * 2);
            }

            //Copy cột TB trở lên
            tempSheet.CopyPasteSameSize(firstSheet.GetRange(firstRow, firstDynamicCol + 2 * defaultNumbeOfColumn,
                firstRow + 6, firstDynamicCol + 2 * defaultNumbeOfColumn + 1), firstRow, firstDynamicCol + listSC.Count * 2);

            int lastCol = defaultLastCol + (listSC.Count - defaultNumbeOfColumn) * 2;

            //Chỉnh lại dòng khác
            if (listSC.Count > defaultNumbeOfColumn)
            {
                tempSheet.MergeRow(2, VTVector.dic['G'], lastCol);
                tempSheet.MergeRow(3, VTVector.dic['G'], lastCol);
                tempSheet.MergeRow(6, VTVector.dic['A'], lastCol);
                tempSheet.MergeRow(7, VTVector.dic['A'], lastCol);

                double colWidth = firstSheet.GetColumnWidth(firstDynamicCol);
                double resizeWidth = colWidth * defaultNumbeOfColumn / listSC.Count;
                for (int i = firstDynamicCol; i <= lastCol; i++)
                {
                    tempSheet.SetColumnWidth(i, resizeWidth);
                }
            }

            //Tính các công thức 
            string formulaPercent = "=IF(cellNumberOfPupil=0;0;ROUND(cellTotal/cellNumberOfPupil;4)*100)";

            var lstAboveAve = (from sc in listSC
                               where
                                   sc.MarkType == SystemParamsInFile.StatisticsConfig.MARK_TYPE_ABOVE_AVERAGE
                               select VTVector.ColumnIntToString(dicStatisticsConfigToColumn[sc])).ToArray();
            string formulaAverageTotal = "=SUM(" + string.Join("#;", lstAboveAve) + "#)";
            for (int i = 0; i < listSC.Count; i++)
            {
                StatisticsConfig sc = listSC[i];
                tempSheet.SetCellValue(firstRow + 4, dicStatisticsConfigToColumn[sc], "${list[].Get(" + sc.StatisticsConfigID + ")}");
            }

            for (int rowIndex = firstRow + 2; rowIndex < firstRow + 7; rowIndex++)
            {
                for (int i = firstDynamicCol; i <= lastCol; i += 2)
                {
                    string cellNumberOfPupil = "D" + rowIndex;
                    string cellTotal = new VTVector(rowIndex, i).ToString();
                    tempSheet.SetFormulaValue(rowIndex, i + 1, formulaPercent.
                        Replace("cellTotal", cellTotal).Replace("cellNumberOfPupil", cellNumberOfPupil));
                }

                tempSheet.SetFormulaValue(rowIndex, lastCol - 1, formulaAverageTotal.Replace("#", rowIndex.ToString()));
            }

            //Fill dữ liệu chung cho template
            AcademicYear academicYear = AcademicYearBusiness.Find(entity.AcademicYearID);
            SchoolProfile school = SchoolProfileBusiness.Find(entity.SchoolID);
            Province province = school.Province;

            string schoolName = school.SchoolName.ToUpper();
            string supervisingDeptName = school.SupervisingDept.SupervisingDeptName.ToUpper();
            string academicYearTitle = academicYear.DisplayTitle;
            string provinceName = province.ProvinceName;
            DateTime reportDate = DateTime.Now;
            string semester = SMASConvert.ConvertSemesterSpecial(entity.Semester).ToUpper();
            if (entity.Semester == SystemParamsInFile.SEMESTER_OF_YEAR_SECOND)
            {
                semester = "HỌC KỲ II";
            }
            Dictionary<string, object> dicGeneralInfo = new Dictionary<string, object> 
                {
                    {"SchoolName", schoolName}, 
                    {"SupervisingDeptName", supervisingDeptName},
                    {"AcademicYearTitle", academicYearTitle},
                    {"ProvinceName", provinceName},
                    {"ReportDate", reportDate},
                    {"Semester", semester}
                };

            tempSheet.GetRange(1, 1, firstRow - 1, defaultLastCol).FillVariableValue(dicGeneralInfo);

            //fill dữ liệu cho các khối
            List<EducationLevel> listEducationLevel = EducationLevelBusiness.GetByGrade(
                SystemParamsInFile.APPLIED_LEVEL_PRIMARY).OrderBy(x => x.EducationLevelID).ToList();

            //Lấy danh sách xếp hạng
            IQueryable<VPupilRanking> listPupilRankingAll = VPupilRankingBusiness.SearchBySchool(entity.SchoolID, new Dictionary<string, object>
            {
                {"AcademicYearID", entity.AcademicYearID},
                {"AppliedLevel", SystemParamsInFile.APPLIED_LEVEL_PRIMARY},
                //{"Semester", entity.Semester}
            })
            .Where(o => !o.PeriodID.HasValue);

            //Lấy danh sách lớp
            List<ClassProfileTempBO> listClass = (from p in ClassProfileBusiness.SearchBySchool(entity.SchoolID, new Dictionary<string, object>
            {
                {"AcademicYearID", entity.AcademicYearID},
                {"AppliedLevel", SystemParamsInFile.APPLIED_LEVEL_PRIMARY}
            })
                                                  join q in EmployeeBusiness.All on p.HeadTeacherID equals q.EmployeeID into g
                                                  from j in g.DefaultIfEmpty()
                                                  select new ClassProfileTempBO
                                                  {
                                                      ClassProfileID = p.ClassProfileID,
                                                      EducationLevelID = p.EducationLevelID,
                                                      OrderNumber = p.OrderNumber,
                                                      DisplayName = p.DisplayName,
                                                      EmployeeID = j.EmployeeID,
                                                      EmployeeName = j.FullName

                                                  })
            .ToList<ClassProfileTempBO>();

            //Lấy danh sách học sinh
            AcademicYear Aca = AcademicYearBusiness.Find(entity.AcademicYearID);
            Dictionary<string, object> dicPupilRanking = new Dictionary<string, object> 
            {
                {"AcademicYearID", entity.AcademicYearID},
                {"SchoolID", entity.SchoolID},
                //{"Semester", entity.Semester},
                {"AppliedLevel", entity.AppliedLevel}
            };
            IQueryable<PupilOfClass> listPupil = PupilOfClassBusiness.SearchBySchool(entity.SchoolID, dicPupilRanking).AddCriteriaSemester(academicYear, entity.Semester);
            List<PupilOfClassBO> listPupilTT = new List<PupilOfClassBO>();
            List<PupilOfClassBO> listPupilTT_Female = new List<PupilOfClassBO>();
            List<PupilOfClassBO> listPupilTT_Ethnic = new List<PupilOfClassBO>();
            listPupilTT = (from p in listPupil
                           join q in ClassProfileBusiness.All on p.ClassID equals q.ClassProfileID
                           join r in PupilProfileBusiness.All on p.PupilID equals r.PupilProfileID
                           where q.IsActive.Value
                           select new PupilOfClassBO
                           {
                               PupilID = p.PupilID,
                               ClassID = p.ClassID,
                               EducationLevelID = q.EducationLevelID,
                               Genre = r.Genre,
                               EthnicID = r.EthnicID
                           }).ToList();
            //Dân tộc
            Ethnic Ethnic_Kinh = EthnicBusiness.All.Where(o => o.EthnicCode.ToLower().Equals(SystemParamsInFile.ETHNIC_CODE_KINH.ToLower()) && o.IsActive == true).FirstOrDefault();
            Ethnic Ethnic_ForeignPeople = EthnicBusiness.All.Where(o => o.EthnicCode.ToLower().Equals(SystemParamsInFile.ETHNIC_CODE_NN.ToLower()) && o.IsActive == true).FirstOrDefault();
            int? EthnicID_Kinh = 0;
            int? EthnicID_ForeignPeople = 0;
            if (Ethnic_Kinh != null)
            {
                EthnicID_Kinh = Ethnic_Kinh.EthnicID;
            }
            if (Ethnic_ForeignPeople != null)
            {
                EthnicID_ForeignPeople = Ethnic_ForeignPeople.EthnicID;
            }
            listPupilTT_Female = listPupilTT.Where(o => o.Genre == SystemParamsInFile.GENRE_FEMALE).ToList();
            listPupilTT_Ethnic = listPupilTT.Where(o => o.EthnicID.HasValue && o.EthnicID != EthnicID_ForeignPeople && o.EthnicID != EthnicID_Kinh).ToList();
            // Lay hoc sinh thuoc lop thuc te
            List<PupilRankingBO> listPupilRanking = (from u in listPupilRankingAll
                                                     join v in PupilProfileBusiness.All on u.PupilID equals v.PupilProfileID
                                                     join r in ClassProfileBusiness.All on u.ClassID equals r.ClassProfileID
                                                     where listPupil.Any(o => o.PupilID == u.PupilID && o.ClassID == u.ClassID)
                                                     && r.IsActive.Value
                                                     select new PupilRankingBO
                                                     {
                                                         PupilRankingID = u.PupilRankingID,
                                                         PupilID = u.PupilID,
                                                         ClassID = u.ClassID,
                                                         EducationLevelID = r.EducationLevelID,
                                                         Genre = v.Genre,
                                                         EthnicID = v.EthnicID,
                                                         ConductLevelID = u.ConductLevelID,
                                                         CapacityLevelID = u.CapacityLevelID,
                                                         Semester = u.Semester
                                                     }).ToList();
            List<PupilRankingBO> listPRBySemester = new List<PupilRankingBO>();
            List<PupilRankingBO> listPRBySemester_Female = new List<PupilRankingBO>();
            List<PupilRankingBO> listPRBySemester_Ethnic = new List<PupilRankingBO>();
            if (entity.Semester == SystemParamsInFile.SEMESTER_OF_YEAR_FIRST)
            {
                listPRBySemester = listPupilRanking.Where(o => o.Semester == entity.Semester).ToList();
            }
            else
            {
                listPupilRanking = listPupilRanking.Where(o => o.Semester >= entity.Semester).ToList();
                listPRBySemester = listPupilRanking.Where(o => o.Semester == (listPupilRanking.Where(u => u.ClassID == o.ClassID && u.PupilID == o.PupilID).Max(u => u.Semester))).ToList();
            }
            listPRBySemester_Female = listPRBySemester.Where(o => o.Genre == SystemParamsInFile.GENRE_FEMALE).ToList();

            listPRBySemester_Ethnic = listPRBySemester.Where(o => o.EthnicID.HasValue && o.EthnicID != EthnicID_ForeignPeople && o.EthnicID != EthnicID_Kinh).ToList();

            #region Tạo sheet thống kê xếp loại giáo dục
            //Tạo sheet Fill dữ liệu
            int lastColumn = Math.Max(lastCol, defaultLastCol);
            IVTWorksheet dataSheet = oBook.CopySheetToLast(tempSheet,
                new VTVector(firstRow + 2, lastColumn).ToString());

            //Tạo vùng cho từng khối
            IVTRange summarizeRange = tempSheet.GetRange(firstRow + 3, 1, firstRow + 3, lastColumn);
            IVTRange topRange = tempSheet.GetRange(firstRow + 4, 1, firstRow + 4, lastColumn);
            IVTRange midRange = tempSheet.GetRange(firstRow + 5, 1, firstRow + 5, lastColumn);
            IVTRange lastRange = tempSheet.GetRange(firstRow + 6, 1, firstRow + 6, lastColumn);
            List<int> listBeginRow = new List<int>();
            int beginRow = firstRow + 3;
            int indexEducation = 0;
            dataSheet.SetCellValue("A6", "THỐNG KÊ XẾP LOẠI GIÁO DỤC");
            dataSheet.Name = "XLGD";
            foreach (EducationLevel educationLevel in listEducationLevel)
            {
                listBeginRow.Add(beginRow);
                int endRow = beginRow + 1;
                indexEducation++;
                dataSheet.CopyPasteSameSize(summarizeRange, beginRow, 1);
                dataSheet.SetCellValue("A" + beginRow, indexEducation);
                dataSheet.SetCellValue("B" + beginRow, educationLevel.Resolution);

                List<ClassProfileTempBO> listCP = (from lc in listClass
                                                   where lc.EducationLevelID == educationLevel.EducationLevelID
                                                   select lc).ToList<ClassProfileTempBO>();
                listCP = listCP.OrderBy(o => o.OrderNumber.HasValue ? o.OrderNumber : 0).ThenBy(o => o.DisplayName).ToList();
                if (listCP == null || listCP.Count == 0)
                {
                    foreach (StatisticsConfig sc in listSC)
                    {
                        dataSheet.SetCellValue(beginRow, dicStatisticsConfigToColumn[sc], 0);
                    }
                    dataSheet.SetCellValue("D" + beginRow, 0);
                    endRow = beginRow;
                }
                else
                {
                    //Fill dữ liệu dòng tổng
                    endRow = beginRow + listCP.Count();
                    string fomula = "=SUM(#" + (beginRow + 1) + ":" + "#" + endRow + ")";
                    foreach (StatisticsConfig sc in listSC)
                    {
                        int col = dicStatisticsConfigToColumn[sc];
                        dataSheet.SetFormulaValue(beginRow,
                            col, fomula.Replace("#", VTVector.ColumnIntToString(col)));
                    }
                    dataSheet.SetFormulaValue("D" + beginRow, fomula.Replace("#", "D"));
                    //Tạo khung dữ liệu danh sách các lớp
                    for (int i = beginRow + 1; i <= endRow; i++)
                    {
                        IVTRange copyRange;
                        if (i == beginRow + 1)
                        {
                            copyRange = topRange;
                        }
                        else if (i == endRow)
                        {
                            copyRange = lastRange;
                        }
                        else
                        {
                            copyRange = midRange;
                        }
                        dataSheet.CopyPasteSameSize(copyRange, "A" + i);
                    }

                    //Tạo dữ liệu cần fill
                    List<object> listData = new List<object>();
                    int indexClass = 0;
                    foreach (ClassProfileTempBO cp in listCP)
                    {
                        indexClass++;
                        Dictionary<string, object> dicData = new Dictionary<string, object>();
                        dicData["Order"] = indexEducation + "." + indexClass;
                        dicData["ClassName"] = cp.DisplayName;
                        dicData["HeadTeacherName"] = cp.EmployeeID != null ? cp.EmployeeName : string.Empty;
                        //Si so
                        int numberOfPupil = listPupilTT.Where(o => o.ClassID == cp.ClassProfileID).Count();
                        dicData["NumberOfPupil"] = numberOfPupil;
                        foreach (StatisticsConfig sc in listSC)
                        {
                            int numberOf = (from mr in listPRBySemester
                                            where mr.ClassID == cp.ClassProfileID &&
                                             mr.CapacityLevelID.ToString() == sc.EqualValue
                                            select mr.PupilRankingID).Count();
                            dicData[sc.StatisticsConfigID.ToString()] = numberOf;
                        }
                        listData.Add(dicData);
                    }

                    //Fill dữ liệu
                    dataSheet.GetRange(beginRow, 1, endRow, lastColumn).FillVariableValue(new Dictionary<string, object>
                    {
                        {"list",listData}
                    });
                }

                //Tính vị trí tiếp theo
                beginRow = endRow + 1;
                if (indexEducation == listEducationLevel.Count)
                {
                    dataSheet.CopyPaste(tempSheet.GetRange(15, 1, 15, 15), endRow, 1, true);
                }
            }

            string formulaSum = "=SUM(#" + string.Join(";#", listBeginRow.ToArray<int>()) + ")";

            foreach (StatisticsConfig sc in listSC)
            {
                int col = dicStatisticsConfigToColumn[sc];
                dataSheet.SetFormulaValue(firstRow + 2,
                    col, formulaSum.Replace("#", VTVector.ColumnIntToString(col)));
            }
            dataSheet.SetFormulaValue("D" + (firstRow + 2), formulaSum.Replace("#", "D"));

            dataSheet.CopyPaste(tempSheet.GetRange(firstRow + 8, 1, firstRow + 8, 100), beginRow + 1, 1);


            dataSheet.FitAllColumnsOnOnePage = true;
            #endregion
            #region Thống kê xếp loại giáo dục học sinh nữ
            IVTWorksheet dataSheet_Female = oBook.CopySheetToLast(tempSheet,
                new VTVector(firstRow + 2, lastColumn).ToString());
            dataSheet_Female.Name = "HS_Nu";
            dataSheet_Female.SetCellValue("A6", "THỐNG KÊ XẾP LOẠI GIÁO DỤC HỌC SINH NỮ");
            //Tạo vùng cho từng khối
            listBeginRow = new List<int>();
            beginRow = firstRow + 3;
            indexEducation = 0;
            foreach (EducationLevel educationLevel in listEducationLevel)
            {
                listBeginRow.Add(beginRow);
                int endRow = beginRow + 1;
                indexEducation++;
                dataSheet_Female.CopyPasteSameSize(summarizeRange, beginRow, 1);
                dataSheet_Female.SetCellValue("A" + beginRow, indexEducation);
                dataSheet_Female.SetCellValue("B" + beginRow, educationLevel.Resolution);

                List<ClassProfileTempBO> listCP = (from lc in listClass
                                                   where lc.EducationLevelID == educationLevel.EducationLevelID
                                                   select lc).ToList<ClassProfileTempBO>();
                listCP = listCP.OrderBy(o => o.OrderNumber.HasValue ? o.OrderNumber : 0).ThenBy(o => o.DisplayName).ToList();
                if (listCP == null || listCP.Count == 0)
                {
                    foreach (StatisticsConfig sc in listSC)
                    {
                        dataSheet_Female.SetCellValue(beginRow, dicStatisticsConfigToColumn[sc], 0);
                    }
                    dataSheet_Female.SetCellValue("D" + beginRow, 0);
                    endRow = beginRow;
                }
                else
                {
                    //Fill dữ liệu dòng tổng
                    endRow = beginRow + listCP.Count();
                    string fomula = "=SUM(#" + (beginRow + 1) + ":" + "#" + endRow + ")";
                    foreach (StatisticsConfig sc in listSC)
                    {
                        int col = dicStatisticsConfigToColumn[sc];
                        dataSheet_Female.SetFormulaValue(beginRow,
                            col, fomula.Replace("#", VTVector.ColumnIntToString(col)));
                    }
                    dataSheet_Female.SetFormulaValue("D" + beginRow, fomula.Replace("#", "D"));
                    //Tạo khung dữ liệu danh sách các lớp
                    for (int i = beginRow + 1; i <= endRow; i++)
                    {
                        IVTRange copyRange;
                        if (i == beginRow + 1)
                        {
                            copyRange = topRange;
                        }
                        else if (i == endRow)
                        {
                            copyRange = lastRange;
                        }
                        else
                        {
                            copyRange = midRange;
                        }
                        dataSheet_Female.CopyPasteSameSize(copyRange, "A" + i);
                    }

                    //Tạo dữ liệu cần fill
                    List<object> listData = new List<object>();
                    int indexClass = 0;
                    foreach (ClassProfileTempBO cp in listCP)
                    {
                        indexClass++;
                        Dictionary<string, object> dicData = new Dictionary<string, object>();
                        dicData["Order"] = indexEducation + "." + indexClass;
                        dicData["ClassName"] = cp.DisplayName;
                        dicData["HeadTeacherName"] = cp.EmployeeID != null ? cp.EmployeeName : string.Empty;
                        //Si so
                        int numberOfPupil = listPupilTT_Female.Where(o => o.ClassID == cp.ClassProfileID).Count();
                        dicData["NumberOfPupil"] = numberOfPupil;
                        foreach (StatisticsConfig sc in listSC)
                        {
                            int numberOf = (from mr in listPRBySemester_Female
                                            where mr.ClassID == cp.ClassProfileID &&
                                             mr.CapacityLevelID.ToString() == sc.EqualValue
                                            select mr.PupilRankingID).Count();
                            dicData[sc.StatisticsConfigID.ToString()] = numberOf;
                        }
                        listData.Add(dicData);
                    }

                    //Fill dữ liệu
                    dataSheet_Female.GetRange(beginRow, 1, endRow, lastColumn).FillVariableValue(new Dictionary<string, object>
                    {
                        {"list",listData}
                    });
                }

                //Tính vị trí tiếp theo
                beginRow = endRow + 1;
                if (indexEducation == listEducationLevel.Count)
                {
                    dataSheet_Female.CopyPaste(tempSheet.GetRange(15, 1, 15, 15), endRow, 1, true);
                }
            }

            formulaSum = "=SUM(#" + string.Join(";#", listBeginRow.ToArray<int>()) + ")";

            foreach (StatisticsConfig sc in listSC)
            {
                int col = dicStatisticsConfigToColumn[sc];
                dataSheet_Female.SetFormulaValue(firstRow + 2,
                    col, formulaSum.Replace("#", VTVector.ColumnIntToString(col)));
            }
            dataSheet_Female.SetFormulaValue("D" + (firstRow + 2), formulaSum.Replace("#", "D"));

            dataSheet_Female.CopyPaste(tempSheet.GetRange(firstRow + 8, 1, firstRow + 8, 100), beginRow + 1, 1);
            dataSheet_Female.FitAllColumnsOnOnePage = true;
            #endregion
            #region Thống kê xếp loại giáo dục học sinh dân tộc
            IVTWorksheet dataSheet_Ethnic = oBook.CopySheetToLast(tempSheet,
               new VTVector(firstRow + 2, lastColumn).ToString());
            dataSheet_Ethnic.Name = "HS_DT";
            dataSheet_Ethnic.SetCellValue("A6", "THỐNG KÊ XẾP LOẠI GIÁO DỤC HỌC SINH DÂN TỘC");
            //Tạo vùng cho từng khối
            listBeginRow = new List<int>();
            beginRow = firstRow + 3;
            indexEducation = 0;
            foreach (EducationLevel educationLevel in listEducationLevel)
            {
                listBeginRow.Add(beginRow);
                int endRow = beginRow + 1;
                indexEducation++;
                dataSheet_Ethnic.CopyPasteSameSize(summarizeRange, beginRow, 1);
                dataSheet_Ethnic.SetCellValue("A" + beginRow, indexEducation);
                dataSheet_Ethnic.SetCellValue("B" + beginRow, educationLevel.Resolution);

                List<ClassProfileTempBO> listCP = (from lc in listClass
                                                   where lc.EducationLevelID == educationLevel.EducationLevelID
                                                   select lc).ToList<ClassProfileTempBO>();
                listCP = listCP.OrderBy(o => o.OrderNumber.HasValue ? o.OrderNumber : 0).ThenBy(o => o.DisplayName).ToList();
                if (listCP == null || listCP.Count == 0)
                {
                    foreach (StatisticsConfig sc in listSC)
                    {
                        dataSheet_Ethnic.SetCellValue(beginRow, dicStatisticsConfigToColumn[sc], 0);
                    }
                    dataSheet_Ethnic.SetCellValue("D" + beginRow, 0);
                    endRow = beginRow;
                }
                else
                {
                    //Fill dữ liệu dòng tổng
                    endRow = beginRow + listCP.Count();
                    string fomula = "=SUM(#" + (beginRow + 1) + ":" + "#" + endRow + ")";
                    foreach (StatisticsConfig sc in listSC)
                    {
                        int col = dicStatisticsConfigToColumn[sc];
                        dataSheet_Ethnic.SetFormulaValue(beginRow,
                            col, fomula.Replace("#", VTVector.ColumnIntToString(col)));
                    }
                    dataSheet_Ethnic.SetFormulaValue("D" + beginRow, fomula.Replace("#", "D"));
                    //Tạo khung dữ liệu danh sách các lớp
                    for (int i = beginRow + 1; i <= endRow; i++)
                    {
                        IVTRange copyRange;
                        if (i == beginRow + 1)
                        {
                            copyRange = topRange;
                        }
                        else if (i == endRow)
                        {
                            copyRange = lastRange;
                        }
                        else
                        {
                            copyRange = midRange;
                        }
                        dataSheet_Ethnic.CopyPasteSameSize(copyRange, "A" + i);
                    }

                    //Tạo dữ liệu cần fill
                    List<object> listData = new List<object>();
                    int indexClass = 0;
                    foreach (ClassProfileTempBO cp in listCP)
                    {
                        indexClass++;
                        Dictionary<string, object> dicData = new Dictionary<string, object>();
                        dicData["Order"] = indexEducation + "." + indexClass;
                        dicData["ClassName"] = cp.DisplayName;
                        dicData["HeadTeacherName"] = cp.EmployeeID != null ? cp.EmployeeName : string.Empty;
                        //Si so
                        int numberOfPupil = listPupilTT_Ethnic.Where(o => o.ClassID == cp.ClassProfileID).Count();
                        dicData["NumberOfPupil"] = numberOfPupil;
                        foreach (StatisticsConfig sc in listSC)
                        {
                            int numberOf = (from mr in listPRBySemester_Ethnic
                                            where mr.ClassID == cp.ClassProfileID &&
                                             mr.CapacityLevelID.ToString() == sc.EqualValue
                                            select mr.PupilRankingID).Count();
                            dicData[sc.StatisticsConfigID.ToString()] = numberOf;
                        }
                        listData.Add(dicData);
                    }

                    //Fill dữ liệu
                    dataSheet_Ethnic.GetRange(beginRow, 1, endRow, lastColumn).FillVariableValue(new Dictionary<string, object>
                    {
                        {"list",listData}
                    });
                }

                //Tính vị trí tiếp theo
                beginRow = endRow + 1;
                if (indexEducation == listEducationLevel.Count)
                {
                    dataSheet_Ethnic.CopyPaste(tempSheet.GetRange(15, 1, 15, 15), endRow, 1, true);
                }
            }

            formulaSum = "=SUM(#" + string.Join(";#", listBeginRow.ToArray<int>()) + ")";

            foreach (StatisticsConfig sc in listSC)
            {
                int col = dicStatisticsConfigToColumn[sc];
                dataSheet_Ethnic.SetFormulaValue(firstRow + 2,
                    col, formulaSum.Replace("#", VTVector.ColumnIntToString(col)));
            }
            dataSheet_Ethnic.SetFormulaValue("D" + (firstRow + 2), formulaSum.Replace("#", "D"));

            dataSheet_Ethnic.CopyPaste(tempSheet.GetRange(firstRow + 8, 1, firstRow + 8, 100), beginRow + 1, 1);
            //Xoá sheet template
            firstSheet.Delete();
            tempSheet.Delete();
            dataSheet_Ethnic.FitAllColumnsOnOnePage = true;
            #endregion
            return oBook.ToStream();
        }
        #endregion

        #region Lấy báo cáo thống kê xếp loại hạnh kiểm cấp 1 được cập nhật mới nhất
        public ProcessedReport GetReportConductPrimary(ReportPupilRankingSearchBO entity)
        {
            string reportCode = SystemParamsInFile.REPORT_DS_THONG_KE_XEP_LOAI_HANH_KIEM_CAP1;
            string inputParameterHashKey = GetHashKeyPrimary(entity);
            return ProcessedReportBusiness.GetProcessedReport(reportCode, inputParameterHashKey);
        }
        #endregion

        #region Lưu báo cáo thống kê xếp loại hạnh kiểm cấp 1 vào CSDL
        public ProcessedReport InsertReportConductPrimary(ReportPupilRankingSearchBO entity, Stream data)
        {
            string reportCode = SystemParamsInFile.REPORT_DS_THONG_KE_XEP_LOAI_HANH_KIEM_CAP1;
            ReportDefinition reportDef = ReportDefinitionBusiness.GetByCode(reportCode);
            ProcessedReport pr = new ProcessedReport();
            pr.ReportCode = reportCode;
            pr.ProcessedDate = DateTime.Now;
            pr.InputParameterHashKey = GetHashKeyPrimary(entity);
            pr.ReportData = ReportUtils.Compress(data);

            //Tạo tên file
            string outputNamePattern = reportDef.OutputNamePattern;
            string semester = ReportUtils.ConvertSemesterForReportName(entity.Semester);

            outputNamePattern = outputNamePattern.Replace("[Semester]", semester);
            pr.ReportName = ReportUtils.RemoveSpecialCharacters(outputNamePattern) + "." + reportDef.OutputFormat;

            IDictionary<string, object> dic = new Dictionary<string, object> {
                {"AcademicYearID", entity.AcademicYearID},
                {"SchoolID", entity.SchoolID},
                {"Semester", entity.Semester}
            };
            ProcessedReportParameterRepository.Insert(dic, pr);
            ProcessedReportRepository.Insert(pr);
            ProcessedReportRepository.Save();
            return pr;
        }
        #endregion

        #region Tạo file báo cáo thống kê xếp loại hạnh kiểm cấp 1
        public Stream CreateReportConductPrimary(ReportPupilRankingSearchBO entity)
        {
            string reportCode = SystemParamsInFile.REPORT_DS_THONG_KE_XEP_LOAI_HANH_KIEM_CAP1;
            ReportDefinition reportDef = ReportDefinitionBusiness.GetByCode(reportCode);
            string templatePath = ReportUtils.GetTemplatePath(reportDef);
            IVTWorkbook oBook = VTExport.OpenWorkbook(templatePath);

            int firstRow = 9;
            int defaultNumbeOfColumn = 2;
            int firstDynamicCol = VTVector.dic['E'];
            int defaultLastCol = VTVector.dic['J'];
            //Lấy sheet template
            IVTWorksheet firstSheet = oBook.GetSheet(1);
            IVTRange subjectTemp = firstSheet.GetRange(firstRow, firstDynamicCol, firstRow + 6, firstDynamicCol + 1);

            //Tạo template chuẩn
            IVTWorksheet tempSheet = oBook.CopySheetToLast(firstSheet, "J" + (firstRow - 1));
            tempSheet.CopyPasteSameSize(firstSheet.GetRange(firstRow, 1, firstRow + 6, firstDynamicCol - 1), "A" + firstRow);
            tempSheet.CopyPaste(firstSheet.GetRange(firstRow + 8, 1, firstRow + 8, 100), firstRow + 8, 1);

            //Lấy danh sách các cột động
            List<StatisticsConfig> listSC = StatisticsConfigBusiness.GetList(
                SystemParamsInFile.APPLIED_LEVEL_PRIMARY,
                SystemParamsInFile.StatisticsConfig.REPORT_TYPE_HANH_KIEM,
                SystemParamsInFile.StatisticsConfig.MARK_TYPE_ALL);


            //Tạo các cột động
            Dictionary<StatisticsConfig, int> dicStatisticsConfigToColumn = new Dictionary<StatisticsConfig, int>();
            for (int i = 0; i < listSC.Count; i++)
            {
                StatisticsConfig sc = listSC[i];
                tempSheet.CopyPasteSameSize(subjectTemp, firstRow, firstDynamicCol + i * 2);
                tempSheet.SetCellValue(firstRow, firstDynamicCol + i * 2, sc.DisplayName);
                dicStatisticsConfigToColumn.Add(sc, firstDynamicCol + i * 2);
            }

            //Copy cột Ghi chú
            tempSheet.CopyPasteSameSize(firstSheet.GetRange(firstRow, firstDynamicCol + 2 * defaultNumbeOfColumn,
                firstRow + 6, firstDynamicCol + 2 * defaultNumbeOfColumn + 1), firstRow, firstDynamicCol + listSC.Count * 2);

            int lastCol = defaultLastCol + (listSC.Count - defaultNumbeOfColumn) * 2;

            //Chỉnh lại dòng khác
            if (listSC.Count > defaultNumbeOfColumn)
            {
                tempSheet.MergeRow(2, VTVector.dic['F'], lastCol);
                tempSheet.MergeRow(3, VTVector.dic['F'], lastCol);
                tempSheet.MergeRow(6, VTVector.dic['A'], lastCol);
                tempSheet.MergeRow(7, VTVector.dic['A'], lastCol);

                double colWidth = firstSheet.GetColumnWidth(firstDynamicCol);
                double resizeWidth = colWidth * defaultNumbeOfColumn / listSC.Count;
                for (int i = firstDynamicCol; i <= lastCol; i++)
                {
                    tempSheet.SetColumnWidth(i, resizeWidth);
                }
            }

            //Tính các công thức 
            string formulaPercent = "=IF(cellNumberOfPupil=0;0;ROUND(cellTotal/cellNumberOfPupil;4)*100)";

            var lstAboveAve = (from sc in listSC
                               where
                                   sc.MarkType == SystemParamsInFile.StatisticsConfig.MARK_TYPE_ABOVE_AVERAGE
                               select VTVector.ColumnIntToString(dicStatisticsConfigToColumn[sc])).ToArray();
            string formulaAverageTotal = "=SUM(" + string.Join("#;", lstAboveAve) + "#)";
            for (int i = 0; i < listSC.Count; i++)
            {
                StatisticsConfig sc = listSC[i];
                tempSheet.SetCellValue(firstRow + 3, dicStatisticsConfigToColumn[sc], "${list[].Get(" + sc.StatisticsConfigID + ")}");
            }

            for (int rowIndex = firstRow + 2; rowIndex < firstRow + 7; rowIndex++)
            {
                for (int i = firstDynamicCol; i <= lastCol - 2; i += 2)
                {
                    string cellNumberOfPupil = "D" + rowIndex;
                    string cellTotal = new VTVector(rowIndex, i).ToString();
                    tempSheet.SetFormulaValue(rowIndex, i + 1, formulaPercent.
                        Replace("cellTotal", cellTotal).Replace("cellNumberOfPupil", cellNumberOfPupil));
                }

                //tempSheet.SetFormulaValue(rowIndex, lastCol - 1, formulaAverageTotal.Replace("#", rowIndex.ToString()));
            }

            //Fill dữ liệu chung cho template
            AcademicYear academicYear = AcademicYearBusiness.Find(entity.AcademicYearID);
            SchoolProfile school = SchoolProfileBusiness.Find(entity.SchoolID);
            Province province = school.Province;

            string schoolName = school.SchoolName.ToUpper();
            string supervisingDeptName = school.SupervisingDept.SupervisingDeptName.ToUpper();
            string academicYearTitle = academicYear.DisplayTitle;
            string provinceName = province.ProvinceName;
            DateTime reportDate = DateTime.Now;
            string semester = SMASConvert.ConvertSemester(entity.Semester);

            Dictionary<string, object> dicGeneralInfo = new Dictionary<string, object> 
                {
                    {"SchoolName", schoolName}, 
                    {"SupervisingDeptName", supervisingDeptName},
                    {"AcademicYearTitle", academicYearTitle},
                    {"ProvinceName", provinceName},
                    {"ReportDate", reportDate},
                    {"Semester", semester}
                };

            tempSheet.GetRange(1, 1, firstRow - 1, defaultLastCol).FillVariableValue(dicGeneralInfo);

            //Tạo sheet Fill dữ liệu
            int lastColumn = Math.Max(lastCol, defaultLastCol);
            IVTWorksheet dataSheet = oBook.CopySheetToLast(tempSheet,
                new VTVector(firstRow + 2, lastColumn).ToString());

            //Tạo vùng cho từng khối
            IVTRange summarizeRange = tempSheet.GetRange(firstRow + 2, 1, firstRow + 2, lastColumn);
            IVTRange topRange = tempSheet.GetRange(firstRow + 3, 1, firstRow + 3, lastColumn);
            IVTRange midRange = tempSheet.GetRange(firstRow + 4, 1, firstRow + 4, lastColumn);
            IVTRange lastRange = tempSheet.GetRange(firstRow + 5, 1, firstRow + 5, lastColumn);

            //fill dữ liệu cho các khối
            List<EducationLevel> listEducationLevel = EducationLevelBusiness.GetByGrade(
                SystemParamsInFile.APPLIED_LEVEL_PRIMARY).OrderBy(x => x.EducationLevelID).ToList();

            //Lấy danh sách xếp hạng
            List<VPupilRanking> listPupilRankingAll = VPupilRankingBusiness.SearchBySchool(entity.SchoolID, new Dictionary<string, object>
            {
                {"AcademicYearID", entity.AcademicYearID},
                {"AppliedLevel", SystemParamsInFile.APPLIED_LEVEL_PRIMARY},
                //{"Semester", entity.Semester}
            })
            .Where(o => !o.PeriodID.HasValue)
            .ToList<VPupilRanking>();
            if (entity.Semester == SystemParamsInFile.SEMESTER_OF_YEAR_FIRST)
            {
                listPupilRankingAll = listPupilRankingAll.Where(o => o.Semester == entity.Semester).ToList();
            }
            else
            {
                listPupilRankingAll = listPupilRankingAll.Where(o => o.Semester >= entity.Semester).ToList();
                listPupilRankingAll = listPupilRankingAll.Where(u => u.Semester == listPupilRankingAll.Where(v => v.PupilID == u.PupilID).Max(v => v.Semester)).ToList();
            }
            //Lấy danh sách lớp
            List<ClassProfile> listClass = ClassProfileBusiness.SearchBySchool(entity.SchoolID, new Dictionary<string, object>
            {
                {"AcademicYearID", entity.AcademicYearID},
                {"AppliedLevel", SystemParamsInFile.APPLIED_LEVEL_PRIMARY}
            }).ToList<ClassProfile>();

            //Lấy danh sách học sinh
            IQueryable<PupilOfClass> iqPupil = PupilOfClassBusiness.SearchBySchool(entity.SchoolID, new Dictionary<string, object> 
            {
                {"AcademicYearID", entity.AcademicYearID}, 
                {"AppliedLevel", SystemParamsInFile.APPLIED_LEVEL_PRIMARY},                
            });


            var listCountPupilInClass = (from p in iqPupil.GetPupil(academicYear, entity.Semester)
                                         group p by p.ClassID into g
                                         select new
                                         {
                                             ClassID = g.Key,
                                             CountPupil = g.Count()
                                         }).ToList();
            List<PupilOfClass> listppTT = iqPupil.AddCriteriaSemester(academicYear, entity.Semester).ToList();
            // Danh sach lay so HS thuc te trong lop
            var listCountRealPupilInClass = (from p in listppTT
                                             group p by p.ClassID into g
                                             select new
                                             {
                                                 ClassID = g.Key,
                                                 CountPupil = g.Count()
                                             }).ToList();
            // join de chi lay hoc sinh thuc te trong lop
            List<VPupilRanking> listPupilRanking = (from u in listPupilRankingAll
                                                    join v in listppTT on new { u.ClassID, u.PupilID } equals new { v.ClassID, v.PupilID }
                                                    select u).ToList();

            List<int> listBeginRow = new List<int>();
            int beginRow = firstRow + 2;
            int indexEducation = 0;
            foreach (EducationLevel educationLevel in listEducationLevel)
            {
                listBeginRow.Add(beginRow);
                int endRow = beginRow + 1;
                indexEducation++;
                dataSheet.CopyPasteSameSize(summarizeRange, beginRow, 1);
                dataSheet.SetCellValue("A" + beginRow, indexEducation);
                dataSheet.SetCellValue("B" + beginRow, educationLevel.Resolution);

                // Danh sach lop trong khoihoc dang xet
                List<ClassProfile> listCP = listClass
                    .Where(cls=>cls.EducationLevelID == educationLevel.EducationLevelID)
                    .OrderBy(cls=>cls.OrderNumber)
                    .ThenBy(cls=>cls.DisplayName)
                    .ToList();

                if (listCP == null || listCP.Count == 0)
                {
                    foreach (StatisticsConfig sc in listSC)
                    {
                        dataSheet.SetCellValue(beginRow, dicStatisticsConfigToColumn[sc], 0);
                    }
                    dataSheet.SetCellValue("C" + beginRow, 0);
                    dataSheet.SetCellValue("D" + beginRow, 0);
                    endRow = beginRow;
                }
                else
                {
                    //Fill dữ liệu dòng tổng
                    endRow = beginRow + listCP.Count();
                    string fomula = "=SUM(#" + (beginRow + 1) + ":" + "#" + endRow + ")";
                    foreach (StatisticsConfig sc in listSC)
                    {
                        int col = dicStatisticsConfigToColumn[sc];
                        dataSheet.SetFormulaValue(beginRow,
                            col, fomula.Replace("#", VTVector.ColumnIntToString(col)));
                    }
                    dataSheet.SetFormulaValue("C" + beginRow, fomula.Replace("#", "C"));
                    dataSheet.SetFormulaValue("D" + beginRow, fomula.Replace("#", "D"));
                    //Tạo khung dữ liệu danh sách các lớp
                    for (int i = beginRow + 1; i <= endRow; i++)
                    {
                        IVTRange copyRange;
                        if (i == beginRow + 1)
                        {
                            copyRange = topRange;
                        }
                        else if (i == endRow)
                        {
                            copyRange = lastRange;
                        }
                        else
                        {
                            copyRange = midRange;
                        }
                        dataSheet.CopyPasteSameSize(copyRange, "A" + i);
                    }

                    //Tạo dữ liệu cần fill
                    List<object> listData = new List<object>();
                    int indexClass = 0;
                    List<int> listStatus = new List<int> { SystemParamsInFile.PUPIL_STATUS_STUDYING, SystemParamsInFile.PUPIL_STATUS_GRADUATED };
                    foreach (ClassProfile cp in listCP)
                    {
                        indexClass++;
                        Dictionary<string, object> dicData = new Dictionary<string, object>();
                        dicData["Order"] = indexEducation + "." + indexClass;
                        dicData["ClassName"] = cp.DisplayName;

                        //Si so
                        var objCount = listCountPupilInClass.Where(o => o.ClassID == cp.ClassProfileID).FirstOrDefault();
                        dicData["NumberOfPupil"] = objCount != null ? objCount.CountPupil : 0;

                        //Si so thuc te
                        var objRealCount = listCountRealPupilInClass.Where(o => o.ClassID == cp.ClassProfileID).FirstOrDefault();
                        dicData["RealNumberOfPupil"] = objRealCount != null ? objRealCount.CountPupil : 0;

                        foreach (StatisticsConfig sc in listSC)
                        {
                            int numberOf = (from mr in listPupilRanking
                                            where mr.ClassID == cp.ClassProfileID
                                            && mr.ConductLevelID.ToString() == sc.EqualValue
                                            select mr.PupilRankingID).Count();
                            dicData[sc.StatisticsConfigID.ToString()] = numberOf;
                        }
                        listData.Add(dicData);
                    }

                    //Fill dữ liệu
                    dataSheet.GetRange(beginRow, 1, endRow, lastColumn).FillVariableValue(new Dictionary<string, object>
                    {
                        {"list",listData}
                    });
                }

                //Tính vị trí tiếp theo
                beginRow = endRow + 1;
                if (indexEducation == listEducationLevel.Count)
                {
                    dataSheet.CopyPaste(tempSheet.GetRange(14, 1, 14, 10), endRow, 1, true);
                }
            }

            dataSheet.CopyPaste(tempSheet.GetRange(firstRow + 8, 1, firstRow + 8, 100), beginRow + 1, 1);
            dataSheet.FitAllColumnsOnOnePage = true;
            //Xoá sheet template
            firstSheet.Delete();
            tempSheet.Delete();

            return oBook.ToStream();
        }
        #endregion

        #region Lấy danh sách thống kê kết quả cuối năm cấp 1 được cập nhật mới nhất
        public ProcessedReport GetReportFinalResultPrimary(ReportPupilRankingSearchBO entity)
        {
            string reportCode = SystemParamsInFile.REPORT_DS_THONG_KE_KET_QUA_CUOI_NAM_CAP1;
            string inputParameterHashKey = GetHashKeyPrimary(entity);
            return ProcessedReportBusiness.GetProcessedReport(reportCode, inputParameterHashKey);
        }
        #endregion

        #region Lưu báo cáo danh sách thống kê kết quả cuối năm cấp 1 vào CSDL
        public ProcessedReport InsertReportFinalResultPrimary(ReportPupilRankingSearchBO entity, Stream data)
        {
            string reportCode = SystemParamsInFile.REPORT_DS_THONG_KE_KET_QUA_CUOI_NAM_CAP1;
            ReportDefinition reportDef = ReportDefinitionBusiness.GetByCode(reportCode);
            ProcessedReport pr = new ProcessedReport();
            pr.ReportCode = reportCode;
            pr.ProcessedDate = DateTime.Now;
            pr.InputParameterHashKey = GetHashKeyPrimary(entity);
            pr.ReportData = ReportUtils.Compress(data);

            //Tạo tên file
            string outputNamePattern = reportDef.OutputNamePattern;
            string schoolLevel = ReportUtils.ConvertAppliedLevelForReportName(entity.AppliedLevel);
            outputNamePattern = outputNamePattern.Replace("[SchoolLevel]", schoolLevel);
            pr.ReportName = ReportUtils.RemoveSpecialCharacters(outputNamePattern) + "." + reportDef.OutputFormat;

            IDictionary<string, object> dic = new Dictionary<string, object> {
                {"AcademicYearID", entity.AcademicYearID},
                {"SchoolID", entity.SchoolID}
            };
            ProcessedReportParameterRepository.Insert(dic, pr);
            ProcessedReportRepository.Insert(pr);
            ProcessedReportRepository.Save();
            return pr;
        }
        #endregion

        #region Tạo file báo cáo thống kê kết quả cuối năm cấp 1
        public Stream CreateReportFinalResultPrimary(ReportPupilRankingSearchBO entity)
        {
            string reportCode = SystemParamsInFile.REPORT_DS_THONG_KE_KET_QUA_CUOI_NAM_CAP1;
            ReportDefinition reportDef = ReportDefinitionBusiness.GetByCode(reportCode);
            string templatePath = ReportUtils.GetTemplatePath(reportDef);
            IVTWorkbook oBook = VTExport.OpenWorkbook(templatePath);

            int firstRow = 8;
            int defaultNumbeOfColumn = 4;
            int firstDynamicCol = VTVector.dic['E'];
            int defaultLastCol = VTVector.dic['L'];
            //Lấy sheet template
            IVTWorksheet firstSheet = oBook.GetSheet(1);
            IVTRange subjectTemp = firstSheet.GetRange(firstRow, firstDynamicCol, firstRow + 6, firstDynamicCol + 1);

            //Tạo template chuẩn
            IVTWorksheet tempSheet = oBook.CopySheetToLast(firstSheet, "L" + (firstRow - 1));
            tempSheet.CopyPasteSameSize(firstSheet.GetRange(firstRow, 1, firstRow + 6, firstDynamicCol - 1), "A" + firstRow);
            tempSheet.CopyPaste(firstSheet.GetRange(firstRow + 8, 1, firstRow + 8, 100), firstRow + 8, 1);

            //Lấy danh sách các cột động
            List<StatisticsConfig> listSC = StatisticsConfigBusiness.GetList(SystemParamsInFile.APPLIED_LEVEL_PRIMARY,
                SystemParamsInFile.StatisticsConfig.REPORT_TYPE_KET_QUA_CUOI_NAM);


            //Tạo các cột động
            Dictionary<int, int> dicStatisticsConfigToColumn = new Dictionary<int, int>();
            for (int i = 0; i < listSC.Count; i++)
            {
                StatisticsConfig sc = listSC[i];
                tempSheet.CopyPasteSameSize(subjectTemp, firstRow, firstDynamicCol + i * 2);
                tempSheet.SetCellValue(firstRow, firstDynamicCol + i * 2, sc.DisplayName);
                dicStatisticsConfigToColumn.Add(sc.StatisticsConfigID, firstDynamicCol + i * 2);
            }

            int lastCol = defaultLastCol + (listSC.Count - defaultNumbeOfColumn) * 2;

            //Chỉnh lại dòng khác
            if (listSC.Count > defaultNumbeOfColumn)
            {
                tempSheet.MergeRow(2, VTVector.dic['E'], lastCol);
                tempSheet.MergeRow(3, VTVector.dic['E'], lastCol);
                tempSheet.MergeRow(4, VTVector.dic['A'], lastCol);
                tempSheet.MergeRow(5, VTVector.dic['A'], lastCol);

                double colWidth = firstSheet.GetColumnWidth(firstDynamicCol);
                double resizeWidth = colWidth * defaultNumbeOfColumn / listSC.Count;
                for (int i = firstDynamicCol; i <= lastCol; i++)
                {
                    tempSheet.SetColumnWidth(i, resizeWidth);
                }
            }

            //Tính các công thức 
            string formulaPercent = "=IF(cellNumberOfPupil=0;0;ROUND(cellTotal/cellNumberOfPupil;4)*100)";

            for (int i = 0; i < listSC.Count; i++)
            {
                StatisticsConfig sc = listSC[i];
                tempSheet.SetCellValue(firstRow + 1, dicStatisticsConfigToColumn[sc.StatisticsConfigID], "${list[].Get(" + sc.StatisticsConfigID + ")}");
            }

            for (int rowIndex = firstRow + 1; rowIndex < firstRow + 6; rowIndex++)
            {
                for (int i = firstDynamicCol; i <= lastCol; i += 2)
                {
                    string cellNumberOfPupil = "D" + rowIndex;
                    string cellTotal = new VTVector(rowIndex, i).ToString();
                    tempSheet.SetFormulaValue(rowIndex, i + 1, formulaPercent.
                        Replace("cellTotal", cellTotal).Replace("cellNumberOfPupil", cellNumberOfPupil));
                }
            }

            //Fill dữ liệu chung cho template
            AcademicYear academicYear = AcademicYearBusiness.Find(entity.AcademicYearID);
            SchoolProfile school = SchoolProfileBusiness.Find(entity.SchoolID);
            Province province = school.Province;

            string schoolName = school.SchoolName.ToUpper();
            string supervisingDeptName = school.SupervisingDept.SupervisingDeptName.ToUpper();
            string academicYearTitle = academicYear.DisplayTitle;
            string provinceName = province.ProvinceName;
            DateTime reportDate = DateTime.Now;

            Dictionary<string, object> dicGeneralInfo = new Dictionary<string, object> 
                {
                    {"SchoolName", schoolName}, 
                    {"SupervisingDeptName", supervisingDeptName},
                    {"AcademicYearTitle", academicYearTitle},
                    {"ProvinceName", provinceName},
                    {"ReportDate", reportDate}
                };

            tempSheet.GetRange(1, 1, firstRow - 1, defaultLastCol).FillVariableValue(dicGeneralInfo);

            //Tạo sheet Fill dữ liệu
            int lastColumn = Math.Max(lastCol, defaultLastCol);
            IVTWorksheet dataSheet = oBook.CopySheetToLast(tempSheet,
                new VTVector(firstRow + 1, lastColumn).ToString());

            //Tạo vùng cho từng khối
            IVTRange summarizeRange = tempSheet.GetRow(firstRow + 4);
            IVTRange topRange = tempSheet.GetRow(firstRow + 1);
            IVTRange midRange = tempSheet.GetRow(firstRow + 2);
            IVTRange lastRange = tempSheet.GetRow(firstRow + 3);

            //fill dữ liệu cho các khối
            List<EducationLevel> listEducationLevel = EducationLevelBusiness.GetByGrade(
                SystemParamsInFile.APPLIED_LEVEL_PRIMARY).OrderBy(x => x.EducationLevelID).ToList();

            List<VPupilRanking> listPupilRankingAll = VPupilRankingBusiness.SearchBySchool(entity.SchoolID, new Dictionary<string, object>
            {
                {"AcademicYearID", entity.AcademicYearID},
                {"AppliedLevel", SystemParamsInFile.APPLIED_LEVEL_PRIMARY},
            })
            .Where(o => !o.PeriodID.HasValue)

            .ToList<VPupilRanking>();
            listPupilRankingAll = listPupilRankingAll.Where(u => u.Semester == listPupilRankingAll.Where(v => v.PupilID == u.PupilID).Max(v => v.Semester)).ToList();
            //Danh sách các lớp
            List<ClassProfile> listClass = ClassProfileBusiness.SearchBySchool(entity.SchoolID, new Dictionary<string, object>
            {
                {"AcademicYearID", entity.AcademicYearID},
                {"AppliedLevel", SystemParamsInFile.APPLIED_LEVEL_PRIMARY}
            }).ToList<ClassProfile>();

            //Lấy danh sách học sinh
            IQueryable<PupilOfClass> listPupil = PupilOfClassBusiness.SearchBySchool(entity.SchoolID, new Dictionary<string, object> 
            {
                {"AcademicYearID", entity.AcademicYearID}, 
                {"AppliedLevel", SystemParamsInFile.APPLIED_LEVEL_PRIMARY},
            });
            IQueryable<PupilOfClass> listPupilTT = listPupil.AddCriteriaSemester(academicYear, entity.Semester).Where(o => o.Status == SystemParamsInFile.PUPIL_STATUS_STUDYING || o.Status == SystemParamsInFile.PUPIL_STATUS_GRADUATED);

            // join de chi lay hoc sinh dang hoc hoac tot nghiep
            List<VPupilRanking> listPupilRanking = (from u in listPupilRankingAll
                                                    join v in listPupilTT on new { u.ClassID, u.PupilID } equals new { v.ClassID, v.PupilID }
                                                    select u).ToList();
            List<int> listBeginRow = new List<int>();
            int beginRow = firstRow + 1;
            int indexEducation = 0;
            foreach (EducationLevel educationLevel in listEducationLevel)
            {
                if (educationLevel.EducationLevelID == 5)
                {
                    continue;
                }
                int endRow = beginRow + 1;
                indexEducation++;
                List<ClassProfile> listCP = (from lc in listClass
                                             where lc.EducationLevelID == educationLevel.EducationLevelID
                                             orderby lc.DisplayName
                                             select lc).ToList<ClassProfile>();

                List<object> listData = new List<object>();
                if (listCP == null || listCP.Count == 0)
                {
                    dataSheet.CopyPasteSameSize(summarizeRange, beginRow, 1);
                    foreach (StatisticsConfig sc in listSC)
                    {
                        dataSheet.SetCellValue(beginRow, dicStatisticsConfigToColumn[sc.StatisticsConfigID], 0);
                    }
                    dataSheet.SetCellValue("C" + beginRow, 0);
                    dataSheet.SetCellValue("D" + beginRow, 0);
                    endRow = beginRow;
                }
                else
                {
                    //Fill dữ liệu dòng tổng
                    endRow = beginRow + listCP.Count();

                    //Tạo khung dữ liệu danh sách các lớp
                    for (int i = beginRow; i < endRow; i++)
                    {
                        IVTRange copyRange;
                        if (i == beginRow)
                        {
                            copyRange = topRange;
                        }
                        else if (i == endRow - 1)
                        {
                            copyRange = lastRange;
                        }
                        else
                        {
                            copyRange = midRange;
                        }
                        dataSheet.CopyPasteSameSize(copyRange, "A" + i);
                    }
                    dataSheet.CopyPasteSameSize(summarizeRange, endRow, 1);
                    dataSheet.MergeColumn('A', beginRow, endRow);
                    string fomula = "=SUM(#" + beginRow + ":" + "#" + (endRow - 1) + ")";
                    foreach (StatisticsConfig sc in listSC)
                    {
                        int col = dicStatisticsConfigToColumn[sc.StatisticsConfigID];
                        dataSheet.SetFormulaValue(endRow,
                            col, fomula.Replace("#", VTVector.ColumnIntToString(col)));
                    }
                    dataSheet.SetFormulaValue("C" + endRow, fomula.Replace("#", "C"));
                    dataSheet.SetFormulaValue("D" + endRow, fomula.Replace("#", "D"));

                    //Tạo dữ liệu cần fill
                    int indexClass = 0;
                    List<int> listStatus = new List<int> { SystemParamsInFile.PUPIL_STATUS_STUDYING, SystemParamsInFile.PUPIL_STATUS_GRADUATED };
                    foreach (ClassProfile cp in listCP)
                    {
                        indexClass++;
                        Dictionary<string, object> dicData = new Dictionary<string, object>();
                        dicData["ClassName"] = cp.DisplayName;

                        int numberOfPupil = (from mr in listPupil
                                             where mr.ClassID == cp.ClassProfileID
                                             select mr.PupilOfClassID).Count();
                        dicData["NumberOfPupil"] = numberOfPupil;

                        int realNumberOfPupil = (from mr in listPupilTT
                                                 where mr.ClassID == cp.ClassProfileID
                                                 select mr.PupilOfClassID).Count();
                        dicData["RealNumberOfPupil"] = realNumberOfPupil;


                        foreach (StatisticsConfig sc in listSC)
                        {
                            string[] arr = sc.EqualValue.Split(',');
                            int numberOf = (from mr in listPupilRanking
                                            where mr.ClassID == cp.ClassProfileID
                                            && arr.Contains(mr.StudyingJudgementID.ToString())
                                            select mr.PupilRankingID).Count();
                            dicData[sc.StatisticsConfigID.ToString()] = numberOf;
                        }
                        listData.Add(dicData);
                    }
                    listBeginRow.Add(endRow);
                }

                //Fill dữ liệu
                dataSheet.GetRange(beginRow, 1, endRow, lastColumn).FillVariableValue(new Dictionary<string, object>
                    {
                        {"list",listData},
                        {"EducationLevel",educationLevel.Resolution}
                    });

                //Tính vị trí tiếp theo
                beginRow = endRow + 1;
            }

            //Insert row tổng
            dataSheet.CopyPasteSameSize(tempSheet.GetRow(firstRow + 5), beginRow, 1);
            string formulaSum = "=SUM(#" + string.Join(";#", listBeginRow.ToArray<int>()) + ")";

            foreach (StatisticsConfig sc in listSC)
            {
                int col = dicStatisticsConfigToColumn[sc.StatisticsConfigID];
                dataSheet.SetFormulaValue(beginRow,
                    col, formulaSum.Replace("#", VTVector.ColumnIntToString(col)));
            }
            dataSheet.SetFormulaValue("C" + beginRow, formulaSum.Replace("#", "C"));
            dataSheet.SetFormulaValue("D" + beginRow, formulaSum.Replace("#", "D"));

            dataSheet.CopyPaste(tempSheet.GetRow(firstRow + 8), beginRow + 1, 1);
            dataSheet.Name = "TKKQCN";
            //Xoá sheet template
            firstSheet.Delete();
            tempSheet.Delete();
            dataSheet.FitAllColumnsOnOnePage = true;
            return oBook.ToStream();
        }
        #endregion

        #region Lấy danh sách thống kê kết quả cuối năm cấp 1 được cập nhật mới nhất - Thông tư 30
        public ProcessedReport GetReportFinalResultPrimaryTT30(ReportPupilRankingSearchBO entity)
        {
            string reportCode = SystemParamsInFile.REPORT_DS_THONG_KE_KET_QUA_CUOI_NAM_CAP1_TT30;
            string inputParameterHashKey = GetHashKeyPrimary(entity);
            return ProcessedReportBusiness.GetProcessedReport(reportCode, inputParameterHashKey);
        }
        #endregion

        #region Lưu báo cáo danh sách thống kê kết quả cuối năm cấp 1 vào CSDL - Thông tư 30
        public ProcessedReport InsertReportFinalResultPrimaryTT30(ReportPupilRankingSearchBO entity, Stream data)
        {
            string reportCode = SystemParamsInFile.REPORT_DS_THONG_KE_KET_QUA_CUOI_NAM_CAP1_TT30;
            ReportDefinition reportDef = ReportDefinitionBusiness.GetByCode(reportCode);
            ProcessedReport pr = new ProcessedReport();
            pr.ReportCode = reportCode;
            pr.ProcessedDate = DateTime.Now;
            pr.InputParameterHashKey = GetHashKeyPrimary(entity);
            pr.ReportData = ReportUtils.Compress(data);

            //Tạo tên file
            string outputNamePattern = reportDef.OutputNamePattern;
            string schoolLevel = ReportUtils.ConvertAppliedLevelForReportName(entity.AppliedLevel);
            outputNamePattern = outputNamePattern.Replace("[SchoolLevel]", schoolLevel);
            pr.ReportName = ReportUtils.RemoveSpecialCharacters(outputNamePattern) + "." + reportDef.OutputFormat;

            IDictionary<string, object> dic = new Dictionary<string, object> {
                {"AcademicYearID", entity.AcademicYearID},
                {"SchoolID", entity.SchoolID}
            };
            ProcessedReportParameterRepository.Insert(dic, pr);
            ProcessedReportRepository.Insert(pr);
            ProcessedReportRepository.Save();
            return pr;
        }
        #endregion

        #region Tạo file báo cáo thống kê kết quả cuối năm cấp 1 - Thông tư 30
        public Stream CreateReportFinalResultPrimaryTT30(ReportPupilRankingSearchBO entity)
        {
            string reportCode = SystemParamsInFile.REPORT_DS_THONG_KE_KET_QUA_CUOI_NAM_CAP1_TT30;
            ReportDefinition reportDef = ReportDefinitionBusiness.GetByCode(reportCode);
            string templatePath = ReportUtils.GetTemplatePath(reportDef);
            IVTWorkbook oBook = VTExport.OpenWorkbook(templatePath);

            int firstRow = 8;//DÒNG ĐẦU TIÊN ĐỔ DL 
            int firstDynamicCol = VTVector.dic['E']; //Vùng đổ dl 
            int defaultLastCol = VTVector.dic['J'];

            //Lấy sheet template
            IVTWorksheet firstSheet = oBook.GetSheet(1);//Gọi template _ sheet 1
            IVTRange subjectTemp = firstSheet.GetRange(firstRow, firstDynamicCol, firstRow + 6, firstDynamicCol + 1);

            //Lấy danh sách các cột 
            List<StatisticsConfig> listSC = StatisticsConfigBusiness.GetList(SystemParamsInFile.APPLIED_LEVEL_PRIMARY,
                SystemParamsInFile.StatisticsConfig.REPORT_TYPE_KET_QUA_CUOI_NAM);
            int startRow = 10;

            //Fill dữ liệu chung cho template
            AcademicYear academicYear = AcademicYearBusiness.Find(entity.AcademicYearID);
            SchoolProfile school = SchoolProfileBusiness.Find(entity.SchoolID);
            Province province = school.Province;

            string schoolName = school.SchoolName.ToUpper();
            string supervisingDeptName = UtilsBusiness.GetSupervisingDeptName(school.SchoolProfileID, entity.AppliedLevel).ToUpper();
            string academicYearTitle = academicYear.DisplayTitle;
            string provinceName = (school.District != null ? school.District.DistrictName : "");
            DateTime reportDate = DateTime.Now;
            string PupilName = string.Empty;
            string SheetName = string.Empty;
            int yearReport = academicYear.Year;
            bool isCurrentYear = yearReport >= 2016 ? true : false;
            List<int> lstAdd = new List<int>();
            lstAdd.Add(0);//Tat ca
            if (entity.FemaleID > 0)
            {
                lstAdd.Add(1);//Hoc sinh nu
            }
            if (entity.EthnicID > 0)
            {
                lstAdd.Add(2);//hoc sinh dan toc
            }
            if (entity.FemaleEthnicID > 0)
            {
                lstAdd.Add(3);//hoc sinh nu dan toc
            }  
            Dictionary<string, object> dicGeneralInfo = new Dictionary<string, object> 
                    {
                        {"SchoolName", schoolName}, 
                        {"SupervisingDeptName", supervisingDeptName},
                        {"AcademicYearTitle", academicYearTitle},
                        {"ProvinceName", provinceName},
                        {"ReportDate", reportDate},
                        {"PupilName",PupilName}
                    };

            firstSheet.GetRange(1, 1, firstRow - 1, defaultLastCol).FillVariableValue(dicGeneralInfo, firstSheet);

            //list khối
            List<EducationLevel> listEducationLevel = EducationLevelBusiness.GetByGrade(
                SystemParamsInFile.APPLIED_LEVEL_PRIMARY).OrderBy(x => x.EducationLevelID).ToList();
            //list lớp học
            List<ClassProfile> listClass = ClassProfileBusiness.SearchBySchool(entity.SchoolID, new Dictionary<string, object>
            {
                {"AcademicYearID", entity.AcademicYearID},
                {"AppliedLevel", SystemParamsInFile.APPLIED_LEVEL_PRIMARY}
            }).ToList<ClassProfile>().OrderBy(a => a.OrderNumber).ToList();
            List<int> lstClassID = listClass.Select(p => p.ClassProfileID).Distinct().ToList();
            //Lấy danh sách học sinh
            IDictionary<string,object> dicSearchPupil = new Dictionary<string, object> 
            {
                {"AcademicYearID", entity.AcademicYearID}, 
                {"AppliedLevel", SystemParamsInFile.APPLIED_LEVEL_PRIMARY},
                {"lstClassID",lstClassID}
            };
            List<PupilOfClassBO> listPupil = PupilOfClassBusiness.SearchBySchool(entity.SchoolID,dicSearchPupil).Select(p => new PupilOfClassBO
            {
                PupilID = p.PupilID,
                ClassID = p.ClassID,
                Status = p.Status,
                EthnicID = p.PupilProfile.EthnicID,
                Genre = p.PupilProfile.Genre
            }).ToList();

            //List lớp học theo khối
            List<ClassProfile> lstClassProfileByEdu = new List<ClassProfile>();
            //List học sinh hoàn thành chương trình lớp học
            int countPupilComplete = 0;
            int countPupilNotComplete = 0;
            ClassProfile objClassProfile = null;
            int totalPupil = 0;
            int totalPupilTT = 0;
            List<long> lstPupilID = new List<long>();
            List<SummedEndingEvaluation> lstSummedEndingEvaluation = new List<SummedEndingEvaluation>();
            //List học sinh có Evaluation và semecter = 4 với ID trường và Năm học
            IDictionary<string, object> dic = new Dictionary<string, object>()
            {
                {"SchoolID",entity.SchoolID},
                {"AcademicYearID",entity.AcademicYearID}
            };
            lstSummedEndingEvaluation = SummedEndingEvaluationBusiness.GetSummedEndingEvaluation(dic)
                .Where(p => p.SemesterID == GlobalConstants.SEMESTER_OF_EVALUATION_RESULT 
                    && p.EvaluationID == GlobalConstants.SEMESTER_OF_EVALUATION_RESULT).ToList();
            //Fill Dữ liệu
            string sumTotalPupilSS = "";
            string sumTotalPupilSSTT = "";
            string sumTotalPupilHT = "";
            string sumTotalPupilCHT = "";
            string sumTotalPupilNComplete = "";
            string title = "THỐNG KÊ KẾT QUẢ CUỐI NĂM {0}";

            List<PupilOfClassBO> lstPupiltmp = new List<PupilOfClassBO>();
            List<SummedEndingEvaluation> lstSummedEndingEvaluationtmp = new List<SummedEndingEvaluation>();
            int addNum = 0;
            for (int add = 0; add < lstAdd.Count; add++)
            {
                addNum = lstAdd[add];
                startRow = 10;
                IVTWorksheet sheet = oBook.CopySheetToLast(firstSheet);
                if (addNum == 0)
                {
                    lstPupiltmp = listPupil;
                }
                else if (addNum == 1)
                {
                    lstPupiltmp = listPupil.Where(p => p.Genre == GlobalConstants.GENRE_FEMALE).ToList();
                    sheet.SetCellValue("A5", string.Format(title, "HỌC SINH NỮ"));
                }
                else if (addNum == 2)
                {
                    lstPupiltmp = listPupil.Where(p => p.EthnicID.HasValue && p.EthnicID != SystemParamsInFile.ETHNIC_ID_KINH  
                        && p.EthnicID != SystemParamsInFile.ETHNIC_ID_NN).ToList();
                    sheet.SetCellValue("A5", string.Format(title, "HỌC SINH DÂN TỘC"));
                }
                else if (addNum == 3)
                {
                    lstPupiltmp = listPupil.Where(p => p.EthnicID.HasValue && p.Genre == GlobalConstants.GENRE_FEMALE && p.EthnicID != SystemParamsInFile.ETHNIC_ID_KINH 
                        && p.EthnicID != SystemParamsInFile.ETHNIC_ID_NN).ToList();
                    sheet.SetCellValue("A5", string.Format(title, "HỌC SINH NỮ DÂN TỘC"));
                }
                else
                {
                    sheet.SetCellValue("A5", string.Format(title, ""));
                }
                foreach (EducationLevel education in listEducationLevel)
                {
                    lstClassProfileByEdu = listClass.Where(c => c.EducationLevelID == education.EducationLevelID).ToList();
                    sheet.SetCellValue(startRow, 1, education.Resolution);
                    sheet.GetRange(startRow, 1, startRow + lstClassProfileByEdu.Count, 1).Merge(false);
                    sheet.GetRange(startRow, 1, startRow, 1).SetFontStyle(true, System.Drawing.Color.Black, false, 18, false, false);
                    sheet.GetRange(startRow, 1, startRow + lstClassProfileByEdu.Count, 1).SetBorder(VTBorderStyle.Solid, VTBorderWeight.Medium, VTBorderIndex.EdgeLeft);
                    sheet.GetRange(startRow, 1, startRow, 1).SetBorder(VTBorderStyle.Solid, VTBorderWeight.Thin, VTBorderIndex.EdgeRight);
                    sheet.GetRange(startRow, 2, startRow, 2).SetBorder(VTBorderStyle.Solid, VTBorderWeight.Thin, VTBorderIndex.EdgeRight);
                    sheet.GetRange(startRow, 3, startRow, 3).SetBorder(VTBorderStyle.Solid, VTBorderWeight.Thin, VTBorderIndex.EdgeRight);
                    sheet.GetRange(startRow, 4, startRow, 4).SetBorder(VTBorderStyle.Solid, VTBorderWeight.Medium, VTBorderIndex.EdgeRight);
                    sheet.GetRange(startRow, 5, startRow, 5).SetBorder(VTBorderStyle.Solid, VTBorderWeight.HairLine, VTBorderIndex.EdgeRight);
                    sheet.GetRange(startRow, 6, startRow, 6).SetBorder(VTBorderStyle.Solid, VTBorderWeight.Medium, VTBorderIndex.EdgeRight);
                    sheet.GetRange(startRow, 7, startRow, 7).SetBorder(VTBorderStyle.Solid, VTBorderWeight.HairLine, VTBorderIndex.EdgeRight);
                    sheet.GetRange(startRow, 8, startRow, 8).SetBorder(VTBorderStyle.Solid, VTBorderWeight.Medium, VTBorderIndex.EdgeRight);
                    sheet.GetRange(startRow, 9, startRow, 9).SetBorder(VTBorderStyle.Solid, VTBorderWeight.Thin, VTBorderIndex.EdgeRight);
                    for (int i = 0; i < lstClassProfileByEdu.Count; i++)
                    {
                        sheet.GetRange(startRow, 1, startRow, 1).SetBorder(VTBorderStyle.Solid, VTBorderWeight.Thin, VTBorderIndex.EdgeRight);
                        sheet.GetRange(startRow, 9, startRow, 9).SetBorder(VTBorderStyle.Solid, VTBorderWeight.Thin, VTBorderIndex.EdgeRight);
                        sheet.GetRange(startRow, 1, startRow, 10).SetBorder(VTBorderStyle.Solid, VTBorderWeight.Thin, VTBorderIndex.EdgeRight);
                        objClassProfile = lstClassProfileByEdu[i];
                        lstPupilID = lstPupiltmp.Where(p => p.ClassID == objClassProfile.ClassProfileID && p.Status == GlobalConstants.PUPIL_STATUS_STUDYING).Select(p => (long)p.PupilID).ToList();
                        //Fill Tên lớp
                        sheet.SetCellValue(startRow, 2, objClassProfile.DisplayName);
                        sheet.GetRange(startRow, 2, startRow, 2).SetBorder(VTBorderStyle.Solid, VTBorderWeight.Thin, VTBorderIndex.EdgeRight);
                        //Fill SS
                        totalPupil = lstPupiltmp.Where(p => p.ClassID == objClassProfile.ClassProfileID).Count();
                        sheet.GetRange(startRow + 1, 3, startRow + 1, 3).SetBorder(VTBorderStyle.Solid, VTBorderWeight.Thin, VTBorderIndex.EdgeRight);
                        sheet.SetCellValue(startRow, 3, totalPupil);
                        //Fill SS Thực tế
                        totalPupilTT = totalPupil - lstPupiltmp.Where(p => p.ClassID == objClassProfile.ClassProfileID && p.Status != GlobalConstants.PUPIL_STATUS_STUDYING).Count();
                        sheet.SetCellValue(startRow, 4, totalPupilTT);
                        sheet.GetRange(startRow + 1, 4, startRow + 1, 4).SetBorder(VTBorderStyle.Solid, VTBorderWeight.Medium, VTBorderIndex.EdgeRight);
                        //Fill SL Hoàn thành chương trình lớp học
                        countPupilComplete = lstSummedEndingEvaluation.Where(p => p.ClassID == objClassProfile.ClassProfileID && lstPupilID.Contains(p.PupilID) && "HT".Equals(p.EndingEvaluation)).Count();
                        sheet.SetCellValue(startRow, 5, countPupilComplete);
                        sheet.GetRange(startRow, 5, startRow, 5).SetBorder(VTBorderStyle.Solid, VTBorderWeight.HairLine, VTBorderIndex.EdgeRight);
                        sheet.SetFormulaValue(startRow, 6, "=IF(D" + startRow + "=0,0,ROUND(E" + startRow + "/D" + startRow + ",4)*100)");
                        sheet.GetRange(startRow, 6, startRow, 6).SetBorder(VTBorderStyle.Solid, VTBorderWeight.Medium, VTBorderIndex.EdgeRight);
                        //Fill SL Rèn luyện bổ sung
                        countPupilNotComplete = lstSummedEndingEvaluation.Where(p => p.ClassID == objClassProfile.ClassProfileID && lstPupilID.Contains(p.PupilID) && "CHT".Equals(p.EndingEvaluation)).Count();
                        sheet.SetCellValue(startRow, 7, countPupilNotComplete);
                        sheet.GetRange(startRow, 7, startRow, 7).SetBorder(VTBorderStyle.Solid, VTBorderWeight.HairLine, VTBorderIndex.EdgeRight);
                        sheet.SetFormulaValue(startRow, 8, "=IF(D" + startRow + "=0,0,ROUND(G" + startRow + "/D" + startRow + ",4)*100)");
                        sheet.GetRange(startRow, 8, startRow, 8).SetBorder(VTBorderStyle.Solid, VTBorderWeight.Medium, VTBorderIndex.EdgeRight);
                        //Ở lại khong fill (theo giai phap)
                        if (isCurrentYear)
                        {
                            countPupilNotComplete = lstSummedEndingEvaluation.Where(p => p.ClassID == objClassProfile.ClassProfileID && lstPupilID.Contains(p.PupilID) && "CHT".Equals(p.EndingEvaluation) && p.RateAdd.HasValue && p.RateAdd.Value == 0).Count();
                        }
                        else//TT30
                        {
                            countPupilNotComplete = lstSummedEndingEvaluation.Where(p => p.ClassID == objClassProfile.ClassProfileID && lstPupilID.Contains(p.PupilID) && "CHT".Equals(p.EndingEvaluation) && p.RateAdd.HasValue && p.RateAdd.Value == 2).Count();
                        }
                        sheet.SetCellValue(startRow, 9, countPupilNotComplete);
                        sheet.GetRange(startRow, 9, startRow, 9).SetBorder(VTBorderStyle.Solid, VTBorderWeight.HairLine, VTBorderIndex.EdgeRight);
                        sheet.SetFormulaValue(startRow, 10, "=IF(D" + startRow + "=0,0,ROUND(I" + startRow + "/D" + startRow + ",4)*100)");
                        sheet.GetRange(startRow, 10, startRow, 10).SetBorder(VTBorderStyle.Solid, VTBorderWeight.Medium, VTBorderIndex.EdgeRight);

                        //Fill Tổng cộng
                        sheet.GetRange(startRow + 1, 1, startRow + 1, 10).SetBorder(VTBorderStyle.Solid, VTBorderWeight.Thin, VTBorderIndex.EdgeRight);
                        sheet.GetRange(startRow + 1, 2, startRow + 1, 2).SetBorder(VTBorderStyle.Solid, VTBorderWeight.Thin, VTBorderIndex.EdgeRight);
                        sheet.GetRange(startRow + 1, 3, startRow + 1, 3).SetBorder(VTBorderStyle.Solid, VTBorderWeight.Thin, VTBorderIndex.EdgeRight);
                        sheet.GetRange(startRow + 1, 4, startRow + 1, 4).SetBorder(VTBorderStyle.Solid, VTBorderWeight.Medium, VTBorderIndex.EdgeRight);
                        sheet.GetRange(startRow + 1, 5, startRow + 1, 5).SetBorder(VTBorderStyle.Solid, VTBorderWeight.HairLine, VTBorderIndex.EdgeRight);
                        sheet.GetRange(startRow + 1, 6, startRow + 1, 6).SetBorder(VTBorderStyle.Solid, VTBorderWeight.Medium, VTBorderIndex.EdgeRight);
                        sheet.GetRange(startRow + 1, 7, startRow + 1, 7).SetBorder(VTBorderStyle.Solid, VTBorderWeight.HairLine, VTBorderIndex.EdgeRight);
                        sheet.GetRange(startRow + 1, 8, startRow + 1, 8).SetBorder(VTBorderStyle.Solid, VTBorderWeight.Medium, VTBorderIndex.EdgeRight);
                        if (i + 2 == lstClassProfileByEdu.Count + 1)
                        {
                            sheet.SetCellValue(startRow + 1, 2, "TC");
                            sheet.GetRange(startRow + 1, 10, startRow + 1, 10).SetBorder(VTBorderStyle.Solid, VTBorderWeight.Medium, VTBorderIndex.EdgeRight);
                            sheet.GetRange(startRow + 1, 1, startRow + 1, 1).SetBorder(VTBorderStyle.Solid, VTBorderWeight.Thin, VTBorderIndex.EdgeRight);
                            sheet.GetRange(startRow + 1, 9, startRow + 1, 9).SetBorder(VTBorderStyle.Solid, VTBorderWeight.Thin, VTBorderIndex.EdgeRight);
                            sheet.SetFormulaValue(startRow + 1, 3, "=SUM(C" + (startRow - (lstClassProfileByEdu.Count - 1)) + ":C" + (startRow) + ")");
                            sheet.SetFormulaValue(startRow + 1, 4, "=SUM(D" + (startRow - (lstClassProfileByEdu.Count - 1)) + ":D" + (startRow) + ")");
                            sheet.SetFormulaValue(startRow + 1, 5, "=SUM(E" + (startRow - (lstClassProfileByEdu.Count - 1)) + ":E" + (startRow) + ")");
                            sheet.SetFormulaValue(startRow + 1, 6, "=IF(D" + (startRow + 1) + "=0,0,ROUND(E" + (startRow + 1) + "/D" + (startRow + 1) + ",4)*100)");
                            sheet.SetFormulaValue(startRow + 1, 7, "=SUM(G" + (startRow - (lstClassProfileByEdu.Count - 1)) + ":G" + (startRow) + ")");
                            sheet.SetFormulaValue(startRow + 1, 8, "=IF(D" + (startRow + 1) + "=0,0,ROUND(G" + (startRow + 1) + "/D" + (startRow + 1) + ",4)*100)");
                            sheet.SetFormulaValue(startRow + 1, 9, "=SUM(I" + (startRow - (lstClassProfileByEdu.Count - 1)) + ":I" + (startRow) + ")");
                            sheet.SetFormulaValue(startRow + 1, 10, "=IF(D" + (startRow + 1) + "=0,0,ROUND(I" + (startRow + 1) + "/D" + (startRow + 1) + ",4)*100)");
                            startRow++;
                            sheet.GetRange(startRow, 2, startRow, 5).SetFontStyle(true, System.Drawing.Color.Black, false, 11, false, false);
                            sheet.GetRange(startRow, 1, startRow, 10).SetBorder(VTBorderStyle.Solid, VTBorderWeight.Medium, VTBorderIndex.EdgeBottom);
                            sheet.GetRange(startRow, 7, startRow, 7).SetFontStyle(true, System.Drawing.Color.Black, false, 11, false, false);
                            sheet.GetRange(startRow, 9, startRow, 9).SetFontStyle(true, System.Drawing.Color.Black, false, 11, false, false);
                            sheet.GetRange(startRow, 1, startRow, 10).SetBorder(VTBorderStyle.Solid, VTBorderWeight.Thin, VTBorderIndex.EdgeTop);
                            sheet.GetRange(startRow, 2, startRow, 10).FillColor(System.Drawing.Color.PaleTurquoise);
                        }
                        else
                        {
                            sheet.GetRange(startRow, 1, startRow, 10).SetBorder(VTBorderStyle.Solid, VTBorderWeight.HairLine, VTBorderIndex.EdgeBottom);
                        }
                        startRow++;
                    }
                    sumTotalPupilSS += "C" + (startRow - 1) + ",";
                    sumTotalPupilSSTT += "D" + (startRow - 1) + ",";
                    sumTotalPupilHT += "E" + (startRow - 1) + ",";
                    sumTotalPupilCHT += "G" + (startRow - 1) + ",";
                    sumTotalPupilNComplete += "I" + (startRow - 1) + ",";
                }
                //TỔNG SỐ
                sheet.SetCellValue(startRow, 1, "TỔNG SỐ");
                sheet.SetRowHeight(startRow, 25);
                sheet.GetRange(startRow, 1, startRow, 1).SetFontStyle(true, System.Drawing.Color.Black, false, 18, false, false);
                sheet.GetRange(startRow, 1, startRow, 1).SetBorder(VTBorderStyle.Solid, VTBorderWeight.Medium, VTBorderIndex.EdgeLeft);
                sheet.GetRange(startRow, 1, startRow, 1).SetBorder(VTBorderStyle.Solid, VTBorderWeight.Thin, VTBorderIndex.EdgeRight);
                sheet.GetRange(startRow, 1, startRow, 10).SetBorder(VTBorderStyle.Solid, VTBorderWeight.Medium, VTBorderIndex.EdgeBottom);
                sheet.GetRange(startRow, 1, startRow, 2).SetBorder(VTBorderStyle.Solid, VTBorderWeight.Thin, VTBorderIndex.EdgeRight);
                sheet.GetRange(startRow, 1, startRow, 3).SetBorder(VTBorderStyle.Solid, VTBorderWeight.Thin, VTBorderIndex.EdgeRight);
                sheet.GetRange(startRow, 1, startRow, 4).SetBorder(VTBorderStyle.Solid, VTBorderWeight.Medium, VTBorderIndex.EdgeRight);
                sheet.GetRange(startRow, 1, startRow, 5).SetBorder(VTBorderStyle.Solid, VTBorderWeight.HairLine, VTBorderIndex.EdgeRight);
                sheet.GetRange(startRow, 1, startRow, 6).SetBorder(VTBorderStyle.Solid, VTBorderWeight.Medium, VTBorderIndex.EdgeRight);
                sheet.GetRange(startRow, 1, startRow, 7).SetBorder(VTBorderStyle.Solid, VTBorderWeight.HairLine, VTBorderIndex.EdgeRight);
                sheet.GetRange(startRow, 1, startRow, 8).SetBorder(VTBorderStyle.Solid, VTBorderWeight.Medium, VTBorderIndex.EdgeRight);
                sheet.GetRange(startRow, 9, startRow, 9).SetBorder(VTBorderStyle.Solid, VTBorderWeight.Thin, VTBorderIndex.EdgeRight);
                //sheet.GetRange(startRow, 1, startRow, 9).SetBorder(VTBorderStyle.Solid, VTBorderWeight.Thin, VTBorderIndex.EdgeRight);
                sheet.GetRange(startRow, 1, startRow, 10).SetBorder(VTBorderStyle.Solid, VTBorderWeight.Medium, VTBorderIndex.EdgeRight);
                sheet.GetRange(startRow, 2, startRow, 5).SetFontStyle(true, System.Drawing.Color.Black, false, 11, false, false);
                sheet.GetRange(startRow, 7, startRow, 7).SetFontStyle(true, System.Drawing.Color.Black, false, 11, false, false);
                sheet.GetRange(startRow, 9, startRow, 9).SetFontStyle(true, System.Drawing.Color.Black, false, 11, false, false);


                //Set formullavalue
                sheet.SetFormulaValue(startRow, 3, "=Sum(" + sumTotalPupilSS.Remove(sumTotalPupilSS.Length - 1, 1) + ")");
                sheet.SetFormulaValue(startRow, 4, "=Sum(" + sumTotalPupilSSTT.Remove(sumTotalPupilSSTT.Length - 1, 1) + ")");
                sheet.SetFormulaValue(startRow, 5, "=Sum(" + sumTotalPupilHT.Remove(sumTotalPupilHT.Length - 1, 1) + ")");
                sheet.SetFormulaValue(startRow, 7, "=Sum(" + sumTotalPupilCHT.Remove(sumTotalPupilCHT.Length - 1, 1) + ")");
                sheet.SetFormulaValue(startRow, 9, "=Sum(" + sumTotalPupilNComplete.Remove(sumTotalPupilNComplete.Length - 1, 1) + ")");
                sheet.SetFormulaValue(startRow, 6, "=IF(D" + (startRow) + "=0,0,ROUND(E" + (startRow) + "/D" + (startRow) + ",4)*100)");
                sheet.SetFormulaValue(startRow, 8, "=IF(D" + (startRow) + "=0,0,ROUND(G" + (startRow) + "/D" + (startRow) + ",4)*100)");
                sheet.SetFormulaValue(startRow, 10, "=IF(D" + (startRow) + "=0,0,ROUND(I" + (startRow) + "/D" + (startRow) + ",4)*100)");

                sheet.Name = addNum == 1 ? "HS_Nu" : addNum == 2 ? "HS_DT" : addNum == 3 ? "HS_Nu_DT" : "Tatca";   
            }
            firstSheet.Delete();
            return oBook.ToStream();
        }
        #endregion
    }
}
