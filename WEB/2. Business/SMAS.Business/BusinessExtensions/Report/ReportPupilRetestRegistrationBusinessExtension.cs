﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Linq.Expressions;
using System.Web;
using log4net;

using SMAS.DAL.Repository;
using SMAS.DAL.IRepository;
using SMAS.Business.IBusiness;
using SMAS.Models.Models;
using SMAS.Business.BusinessObject;
using SMAS.Business.Common;
using System.IO;
using SMAS.VTUtils.Excel.Export;

namespace SMAS.Business.Business
{
    public class ReportPupilRetestRegistrationBusiness : GenericBussiness<ProcessedReport>, IReportPupilRetestRegistrationBusiness
    {

        IProcessedReportParameterRepository ProcessedReportParameterRepository;
        IProcessedReportRepository ProcessedReportRepository;
        IEducationLevelRepository EducationLevelRepository;
        IAcademicYearRepository AcademicYearRepository;
        IPupilRetestRegistrationRepository PupilRetestRegistrationRepository;
        IPupilProfileRepository PupilProfileRepository;
        ISummedUpRecordRepository SummedUpRecordRepository;

        public ReportPupilRetestRegistrationBusiness(ILog logger, SMASEntities context = null)
            : base(logger)
        {
            if (context == null) { context = new SMASEntities(); } this.context = context;
            this.ProcessedReportParameterRepository = new ProcessedReportParameterRepository(context);
            this.ProcessedReportRepository = new ProcessedReportRepository(context);
            this.EducationLevelRepository = new EducationLevelRepository(context);
            this.AcademicYearRepository = new AcademicYearRepository(context);
            this.PupilRetestRegistrationRepository = new PupilRetestRegistrationRepository(context);
            this.PupilProfileRepository = new PupilProfileRepository(context);
            this.SummedUpRecordRepository = new SummedUpRecordRepository(context);
        }

        #region Tạo mảng băm cho các tham số đầu vào của danh sách học sinh đăng ký thi lại
        public string GetHashKey(ReportPupilRetestRegistrationSearchBO entity)
        {
            IDictionary<string, object> dic = new Dictionary<string, object> {
                {"AcademicYearID", entity.AcademicYearID},
                {"EducationLevelID ", entity.EducationLevelID},
                {"SchoolID", entity.SchoolID},
                {"Semester", entity.Semester},
                {"IsCommenting", entity.IsCommenting},
                {"SubjectID", entity.SubjectID}
            };
            return ReportUtils.GetHashKey(dic);
        }
        #endregion

        #region Lấy danh sách học sinh đăng ký thi lại cấp 1 được cập nhật mới nhất
        public ProcessedReport GetReportPupilRetestRegistrationPrimary(ReportPupilRetestRegistrationSearchBO entity)
        {
            string reportCode = SystemParamsInFile.REPORT_DS_HS_DANG_KY_THI_LAI_CAP1;
            string inputParameterHashKey = GetHashKey(entity);
            return ProcessedReportBusiness.GetProcessedReport(reportCode, inputParameterHashKey);
        }
        #endregion

        #region Lưu báo cáo danh sách học sinh đăng ký thi lại cấp 1 vào CSDL
        public ProcessedReport InsertReportPupilRetestRegistrationPrimary(ReportPupilRetestRegistrationSearchBO entity, Stream data)
        {
            string reportCode = SystemParamsInFile.REPORT_DS_HS_DANG_KY_THI_LAI_CAP1;
            ReportDefinition reportDef = ReportDefinitionBusiness.GetByCode(reportCode);
            ProcessedReport pr = new ProcessedReport();
            pr.ReportCode = reportCode;
            pr.ProcessedDate = DateTime.Now;
            pr.InputParameterHashKey = GetHashKey(entity);
            pr.ReportData = ReportUtils.Compress(data);

            //Tạo tên file
            string outputNamePattern = reportDef.OutputNamePattern;
            string subject = SubjectCatBusiness.Find(entity.SubjectID).SubjectName;

            outputNamePattern = outputNamePattern.Replace("[Subject]", subject);
            outputNamePattern = outputNamePattern.Replace("[SchoolLevel]", ReportUtils.ConvertAppliedLevelForReportName(entity.AppliedLevel));
            pr.ReportName = ReportUtils.RemoveSpecialCharacters(outputNamePattern) + "." + reportDef.OutputFormat;

            IDictionary<string, object> dic = new Dictionary<string, object> {
                {"AcademicYearID", entity.AcademicYearID},
                {"EducationLevelID ", entity.EducationLevelID},
                {"SchoolID", entity.SchoolID},
                {"Semester", entity.Semester},
                {"IsCommenting", entity.IsCommenting},
                {"SubjectID", entity.SubjectID}
            };
            ProcessedReportParameterRepository.Insert(dic, pr);
            ProcessedReportRepository.Insert(pr);
            ProcessedReportRepository.Save();
            return pr;
        }
        #endregion

        #region Tạo file báo cáo danh sách học sinh đăng ký thi lại
        public Stream CreateReportPupilRetestRegistrationPrimary(ReportPupilRetestRegistrationSearchBO entity)
        {
            SubjectCatBusiness.CheckAvailable(entity.SubjectID, "LearningOutcomePrimaryReport_Label_Subject");
            string reportCode = SystemParamsInFile.REPORT_DS_HS_DANG_KY_THI_LAI_CAP1;
            ReportDefinition reportDef = ReportDefinitionBusiness.GetByCode(reportCode);
            string templatePath = ReportUtils.GetTemplatePath(reportDef);
            IVTWorkbook oBook = VTExport.OpenWorkbook(templatePath);

            //Lấy sheet template
            IVTWorksheet firstSheet = oBook.GetSheet(1);

            //Lấy template dòng cần lặp 
            IVTRange templateRange = firstSheet.GetRange("A12", "H12");
            IVTRange templateLastRange = firstSheet.GetRange("A10", "H10");
            IVTRange templateRangeRed = firstSheet.GetRange("A13", "H13");
            IVTRange templateLastRangeRed = firstSheet.GetRange("A14", "H14");


            AcademicYear academicYear = AcademicYearBusiness.Find(entity.AcademicYearID);
            SchoolProfile school = SchoolProfileBusiness.Find(entity.SchoolID);
            SubjectCat subject = SubjectCatBusiness.Find(entity.SubjectID);

            //Tạo dữ liệu chung
            string schoolName = school.SchoolName.ToUpper();
            string supervisingDeptName = school.SupervisingDept.SupervisingDeptName.ToUpper();
            string title = academicYear.DisplayTitle;
            string subjectName = subject.DisplayName;

            //Lấy danh sách các khối
            List<EducationLevel> lstEducationLevel = new List<EducationLevel>();
            if (entity.EducationLevelID != 0)
            {
                lstEducationLevel.Add(EducationLevelRepository.Find(entity.EducationLevelID));
            }
            else
            {
                lstEducationLevel = EducationLevelBusiness.GetByGrade(SystemParamsInFile.APPLIED_LEVEL_PRIMARY).ToList();
            }

            if (lstEducationLevel != null && lstEducationLevel.Count > 0)
            {
                lstEducationLevel = lstEducationLevel.Where(p => (p.EducationLevelID != SystemParamsInFile.EDUCATION_LEVEL_FIFTH && p.EducationLevelID != SystemParamsInFile.EDUCATION_LEVEL_NINTH && p.EducationLevelID != SystemParamsInFile.EDUCATION_LEVEL_TWELFTH)).ToList();
            }

            //Lấy danh sách thông tin học sinh đăng ký thi lại cấp 1 
            //Dictionary<string, object> dicPupilRanking = new Dictionary<string, object> 
            //{
            //    {"AcademicYearID", entity.AcademicYearID}, 
            //    {"EducationLevelID", entity.EducationLevelID},
            //    {"SchoolID", entity.SchoolID},
            //    {"Semester", entity.Semester},
            //    {"AppliedLevel", SystemParamsInFile.APPLIED_LEVEL_PRIMARY}
            //};

            // Danh sach hoc sinh dang hoc
            //Lấy danh sách học sinh

            AcademicYear Aca = AcademicYearBusiness.Find(entity.AcademicYearID);
            Dictionary<string, object> dicPupilRanking = new Dictionary<string, object> 
            {
                {"AcademicYearID", entity.AcademicYearID},
                {"SchoolID", entity.SchoolID},
                //{"Semester", entity.Semester},
                //{"Check", "Check"},
                {"AppliedLevel",  SystemParamsInFile.APPLIED_LEVEL_PRIMARY}
            };
            IQueryable<PupilOfClass> lstPupil = PupilOfClassBusiness.SearchBySchool(entity.SchoolID, dicPupilRanking).AddCriteriaSemester(Aca, entity.Semester);

            //Dictionary<string, object> dicPupil = new Dictionary<string, object> 
            //{
            //    {"AcademicYearID", entity.AcademicYearID}, 
            //    {"EducationLevelID", entity.EducationLevelID},
            //    {"AppliedLevel", SystemParamsInFile.APPLIED_LEVEL_PRIMARY},
            //    {"Check", "Check"},
            //    {"Status", new List<int>{SystemParamsInFile.PUPIL_STATUS_STUDYING, SystemParamsInFile.PUPIL_STATUS_GRADUATED}}
            //};
            //List<PupilOfClass> lstPupil = PupilOfClassBusiness.SearchBySchool(entity.SchoolID, dicPupil).ToList();
            // Danh sach mon hoc cua lop
            //Lấy danh sách các môn học cho các lớp
            Dictionary<string, object> dicSubject = new Dictionary<string, object> 
            {
                {"AcademicYearID", entity.AcademicYearID}, 
                {"EducationLevelID", entity.EducationLevelID},
                {"SubjectID", entity.SubjectID},
                {"AppliedLevel", SystemParamsInFile.APPLIED_LEVEL_PRIMARY}
            };
            var lstSubject = ClassSubjectBusiness.SearchBySchool(entity.SchoolID, dicSubject)
                .Select(o => o.ClassID)
                .Distinct()
                .ToList();

            List<ReportPupilRetestRegistrationBO> lstPupilRetestRegistrationAllStatus = GetListPupilRetestRegistration(entity.SchoolID,
                entity.AcademicYearID, entity.EducationLevelID, entity.SubjectID, SystemParamsInFile.SEMESTER_OF_YEAR_SECOND,
                SystemParamsInFile.APPLIED_LEVEL_PRIMARY).Distinct().ToList<ReportPupilRetestRegistrationBO>();

            List<ReportPupilRetestRegistrationBO> lstPupilRetestRegistration = (from u in lstPupilRetestRegistrationAllStatus
                                                                                where lstPupil.Any(o => o.PupilID == u.PupilID && o.ClassID == u.ClassID)
                                                                                && lstSubject.Any(o => o == u.ClassID)
                                                                                select u)
                                                                                .ToList();

            int firstRow = 10;

            //Tạo và fill dữ liệu vào các sheet tương ứng các khối
            foreach (EducationLevel educationLevel in lstEducationLevel)
            {
                //Tạo sheet
                IVTWorksheet sheet = oBook.CopySheetToLast(firstSheet, "H9");
                sheet.Name = educationLevel.Resolution;

                //Tạo dictionary dữ liệu
                Dictionary<string, object> dic = new Dictionary<string, object> 
                {
                    {"SchoolName", schoolName}, 
                    {"SupervisingDeptName", supervisingDeptName},
                    {"AcaTitle", title},
                    {"SubjectName", subjectName},
                    {"EducationLevel", "KHỐI " + educationLevel.Resolution.ToUpper().Replace("KHỐI", "")}
                };

                //Lấy danh sách học sinh đăng ký thi lại cấp 1
                List<ReportPupilRetestRegistrationBO> lstPRR = (from s in lstPupilRetestRegistration
                                                                where s.EducationLevelID == educationLevel.EducationLevelID
                                                                //orderby s.Name, s.ClassName
                                                                select s).OrderBy(u => u.CLassOrder)
                                                                                .ThenBy(u => u.ClassName)
                                                                                .ThenBy(p => p.PupilOrder)
                                                                                .ThenBy(p => p.Name)
                                                                                .ThenBy(p => p.PupilName)
                                                                .ToList();

                //Tạo khung cho sheet trước khi fill dữ liệu
                for (int i = 0; i < lstPRR.Count; i++)
                {
                    if (lstPRR[i].status == SystemParamsInFile.PUPIL_STATUS_STUDYING || lstPRR[i].status == SystemParamsInFile.PUPIL_STATUS_GRADUATED)
                    {
                        sheet.CopyPaste(templateRange, firstRow-1 + i,1,true);
                    }
                    else
                    {
                        sheet.CopyPaste(templateRangeRed, firstRow-1 + i, 1,true);
                    }
                        
                    if (i == lstPRR.Count -1)
                    {
                        if (lstPRR[i].status == SystemParamsInFile.PUPIL_STATUS_STUDYING || lstPRR[i].status == SystemParamsInFile.PUPIL_STATUS_GRADUATED)
                        {
                            sheet.CopyPasteSameRowHeigh(templateLastRange, firstRow + lstPRR.Count - 2, true);
                           
                        }
                        else
                        {
                            sheet.CopyPasteSameRowHeigh(templateLastRangeRed, firstRow + lstPRR.Count - 2, true);
                        }
                        
                    }
                }
                
                sheet.DeleteRow(firstRow + lstPRR.Count);
                sheet.DeleteRow(firstRow + lstPRR.Count - 1);

                //Fill dữ liệu
                List<object> list = new List<object>();
                int order = 1;
                foreach (ReportPupilRetestRegistrationBO bo in lstPRR)
                {
                    object mark;
                    if (entity.IsCommenting == SystemParamsInFile.ISCOMMENTING_TYPE_JUDGE)
                    {
                        mark = bo.JudgementResult;
                    }
                    else
                    {
                        mark = bo.SummedUpMark;
                    }
                    if (bo.status == SystemParamsInFile.PUPIL_STATUS_STUDYING)
                    {
                        list.Add(new Dictionary<string, object> 
                        {
                        {"Order", order++}, 
                        {"PupilName", bo.PupilName},
                        {"PupilCode", bo.PupilCode},
                        {"BirthDate", bo.BirthDate.ToString("dd/MM/yyyy")},
                        {"Genre", SMASConvert.ConvertGenre(bo.Genre)},
                        {"ClassName", bo.ClassName},
                        {"Mark", mark}
                        });
                    }
                    else
                    {
                        list.Add(new Dictionary<string, object> 
                        {
                        {"Order", order++}, 
                        {"PupilName", bo.PupilName},
                        {"PupilCode", bo.PupilCode},
                        {"BirthDate", bo.BirthDate.ToString("dd/MM/yyyy")},
                        {"Genre", SMASConvert.ConvertGenre(bo.Genre)},
                        {"ClassName", bo.ClassName},
                        });
                    }
                }
                dic.Add("list", list);

                //Fill dữ liệu
                sheet.FillVariableValue(dic);
                //sheet.FitAllColumnsOnOnePage = true;
            }
            
            //Xoá sheet template
            firstSheet.Delete();

            return oBook.ToStream();
        }
        #endregion

        #region Lấy danh sách học sinh đăng ký thi lại
        public IQueryable<ReportPupilRetestRegistrationBO> GetListPupilRetestRegistration(int SchoolID,
            int AcademicYearID, int EducationLevelID, int SubjectID, int Semester, int AppliedLevel)
        {
            AcademicYear aca = AcademicYearBusiness.Find(AcademicYearID);
            Dictionary<string, object> dic = new Dictionary<string, object> 
            {
                {"AcademicYearID", AcademicYearID}, 
                {"EducationLevelID", EducationLevelID},
                {"SubjectID", SubjectID},
                //{"Semester", Semester},
                {"AppliedLevel", AppliedLevel}
            };
            IQueryable<PupilRetestRegistration> listPR = PupilRetestRegistrationBusiness.SearchBySchool(SchoolID, dic);
            dic["Semester"] = Semester;
            IQueryable<VSummedUpRecord> listSUR = VSummedUpRecordBusiness.SearchBySchool(SchoolID, dic).Where(o => !o.PeriodID.HasValue &&
                (!o.IsOldData.HasValue || !o.IsOldData.Value));
            IQueryable<PupilOfClass> listpoc = PupilOfClassBusiness.SearchBySchool(SchoolID, dic);
            var query = from prr in listPR
                        join sur in listSUR on new { prr.PupilID, prr.ClassID } equals new { sur.PupilID, sur.ClassID }
                        join poc in listpoc on new { prr.PupilID, prr.ClassID } equals new { poc.PupilID, poc.ClassID }
                        where prr.AcademicYearID == AcademicYearID
                            //&& prr.ModifiedDate == null
                        && prr.ClassProfile.EducationLevel.Grade == AppliedLevel
                        && prr.PupilProfile.IsActive
                        select new ReportPupilRetestRegistrationBO
                        {
                            PupilID = prr.PupilID,
                            ClassID = prr.ClassID,
                            BirthDate = prr.PupilProfile.BirthDate,
                            ClassName = prr.ClassProfile.DisplayName,
                            EducationLevelID = prr.ClassProfile.EducationLevelID,
                            Genre = prr.PupilProfile.Genre,
                            JudgementResult = sur.JudgementResult,
                            Name = prr.PupilProfile.Name,
                            PupilCode = prr.PupilProfile.PupilCode,
                            PupilName = prr.PupilProfile.FullName,
                            SummedUpMark = sur.SummedUpMark,
                            status = poc.Status,
                            PupilOrder = (poc.OrderInClass.HasValue) ? poc.OrderInClass.Value : 0,
                            CLassOrder = (prr.ClassProfile.OrderNumber.HasValue) ? prr.ClassProfile.OrderNumber.Value : 0
                        };
            return query;
        }
        #endregion

        #region Lấy báo cáo thống kê học sinh thi lại cấp 1 được cập nhật mới nhất
        public ProcessedReport GetReportPupilRetestPrimary(ReportPupilRetestRegistrationSearchBO entity)
        {
            string reportCode = SystemParamsInFile.REPORT_DS_HS_THI_LAI_CAP1;
            string inputParameterHashKey = GetHashKey(entity);
            return ProcessedReportBusiness.GetProcessedReport(reportCode, inputParameterHashKey);
        }
        #endregion

        #region Lưu báo cáo thống kê học sinh thi lại cấp 1 vào CSDL
        public ProcessedReport InsertReportPupilRetestPrimary(ReportPupilRetestRegistrationSearchBO entity, Stream data)
        {
            string reportCode = SystemParamsInFile.REPORT_DS_HS_THI_LAI_CAP1;
            ReportDefinition reportDef = ReportDefinitionBusiness.GetByCode(reportCode);
            ProcessedReport pr = new ProcessedReport();
            pr.ReportCode = reportCode;
            pr.ProcessedDate = DateTime.Now;
            pr.InputParameterHashKey = GetHashKey(entity);
            pr.ReportData = ReportUtils.Compress(data);

            //Tạo tên file
            string outputNamePattern = reportDef.OutputNamePattern;

            pr.ReportName = ReportUtils.RemoveSpecialCharacters(outputNamePattern) + "." + reportDef.OutputFormat;

            IDictionary<string, object> dic = new Dictionary<string, object> {
                {"AcademicYearID", entity.AcademicYearID},
                {"EducationLevelID ", entity.EducationLevelID},
                {"SchoolID", entity.SchoolID}
            };
            ProcessedReportParameterRepository.Insert(dic, pr);
            ProcessedReportRepository.Insert(pr);
            ProcessedReportRepository.Save();
            return pr;
        }
        #endregion

        #region Tạo file báo cáo thống kê học sinh thi lại cấp 1
        public Stream CreateReportPupilRetestPrimary(ReportPupilRetestRegistrationSearchBO entity)
        {
            string reportCode = SystemParamsInFile.REPORT_DS_HS_THI_LAI_CAP1;
            ReportDefinition reportDef = ReportDefinitionBusiness.GetByCode(reportCode);
            string templatePath = ReportUtils.GetTemplatePath(reportDef);
            IVTWorkbook oBook = VTExport.OpenWorkbook(templatePath);

            int firstRow = 7;
            int defaultNumbeOfColumn = 2;
            int firstDynamicCol = VTVector.dic['D'];
            int defaultLastCol = VTVector.dic['J'];
            //Lấy sheet template
            IVTWorksheet firstSheet = oBook.GetSheet(1);
            IVTRange subjectTemp = firstSheet.GetRange(firstRow, firstDynamicCol, firstRow + 6, firstDynamicCol);
            IVTRange lastTemp = firstSheet.GetRange(firstRow, firstDynamicCol + defaultNumbeOfColumn, firstRow + 20, defaultLastCol);

            AcademicYear academicYear = AcademicYearBusiness.Find(entity.AcademicYearID);
            SchoolProfile school = SchoolProfileBusiness.Find(entity.SchoolID);
            Province province = school.Province;

            //Lấy danh sách các khối
            List<EducationLevel> listEducationLevel = new List<EducationLevel>();
            if (entity.EducationLevelID == 0)
            {
                listEducationLevel = EducationLevelBusiness.GetByGrade(
                SystemParamsInFile.APPLIED_LEVEL_PRIMARY).Where(o => o.IsLastYear != true).OrderBy(x => x.EducationLevelID).ToList();
            }
            else
            {
                listEducationLevel.Add(EducationLevelBusiness.Find(entity.EducationLevelID));
            }

            //Lấy danh sách môn học
            List<ClassSubject> listClassSubject = ClassSubjectBusiness.SearchBySchool(entity.SchoolID, new Dictionary<string, object> 
            {
                {"AcademicYearID", entity.AcademicYearID}, 
                {"AppliedLevel", SystemParamsInFile.APPLIED_LEVEL_PRIMARY}
            }).ToList();

            //Lấy danh sách xếp loại
            List<VPupilRanking> listPupilRanking = VPupilRankingBusiness.SearchBySchool(entity.SchoolID, new Dictionary<string, object>
            {
                {"AcademicYearID", entity.AcademicYearID},
                {"AppliedLevel", SystemParamsInFile.APPLIED_LEVEL_PRIMARY},
                {"Semester", SystemParamsInFile.SEMESTER_OF_YEAR_SECOND}
                
            }).Where(o => o.StudyingJudgementID == SystemParamsInFile.STUDYING_JUDGEMENT_RETEST).ToList<VPupilRanking>();
            //Danh sach hoc sinh thuoc dien thi lai
            List<int> listPupilID = listPupilRanking.Select(o => o.PupilID).Distinct().ToList();

            //Lấy danh sách học sinh
            List<PupilOfClass> listPupil = PupilOfClassBusiness.SearchBySchool(entity.SchoolID, new Dictionary<string, object> 
            {
                {"AcademicYearID", entity.AcademicYearID}, 
                {"AppliedLevel", SystemParamsInFile.APPLIED_LEVEL_PRIMARY},
                {"Check", "Check"},
                {"Status", new List<int>{SystemParamsInFile.PUPIL_STATUS_STUDYING, SystemParamsInFile.PUPIL_STATUS_GRADUATED}}
            }).Where(o => listPupilID.Contains(o.PupilID)).ToList();

            //Lấy danh sách học lực môn, chi lay danh sach bi thi lai
            List<VSummedUpRecord> listSummedUpRecord = VSummedUpRecordBusiness.SearchBySchool(entity.SchoolID, new Dictionary<string, object>
            {
                {"AcademicYearID", entity.AcademicYearID},
                {"AppliedLevel", SystemParamsInFile.APPLIED_LEVEL_PRIMARY},
                {"Semester", SystemParamsInFile.SEMESTER_OF_YEAR_SECOND}
            })
            .Where(o => (o.SummedUpMark.HasValue && o.SummedUpMark.Value < 5) || (o.JudgementResult != null && o.JudgementResult == SystemParamsInFile.JUDGE_MARK_B))
            .ToList<VSummedUpRecord>();



            //Lấy danh sách học sinh đăng ký thi lại 
            List<PupilRetestRegistration> listPupilRetestRegistration = PupilRetestRegistrationBusiness.SearchBySchool(entity.SchoolID, new Dictionary<string, object>
            {
                {"AcademicYearID", entity.AcademicYearID},
                {"AppliedLevel", SystemParamsInFile.APPLIED_LEVEL_PRIMARY}
            }).ToList<PupilRetestRegistration>();

            //Fill dữ liệu cho các sheet
            foreach (EducationLevel educationLevel in listEducationLevel)
            {

                //Lấy danh sách môn học - cột động
                List<SubjectCat> lstSubject = (from lc in listClassSubject
                                               where lc.ClassProfile.EducationLevelID == educationLevel.EducationLevelID
                                               orderby lc.SubjectCat.OrderInSubject, lc.SubjectCat.DisplayName
                                               select lc.SubjectCat).Distinct().ToList<SubjectCat>();

                //Tạo template chuẩn
                IVTWorksheet tempSheet = oBook.CopySheetToLast(firstSheet, "J" + (firstRow - 1));
                tempSheet.CopyPasteSameSize(firstSheet.GetRange(firstRow, 1, firstRow + 6, firstDynamicCol - 1), "A" + firstRow);
                tempSheet.CopyPasteSameColumnWidth(lastTemp, firstRow, firstDynamicCol + lstSubject.Count);

                //Tạo các cột động
                Dictionary<int, int> dicSubjectToColumn = new Dictionary<int, int>();
                for (int i = 0; i < lstSubject.Count; i++)
                {
                    SubjectCat sc = lstSubject[i];
                    tempSheet.CopyPasteSameColumnWidth(subjectTemp, firstRow, firstDynamicCol + i);
                    tempSheet.SetCellValue(firstRow + 1, firstDynamicCol + i, sc.DisplayName);
                    dicSubjectToColumn.Add(sc.SubjectCatID, firstDynamicCol + i);
                }
                if (lstSubject.Count > 1)
                {
                    tempSheet.MergeRow(firstRow, firstDynamicCol, firstDynamicCol + lstSubject.Count - 1);
                }

                int lastCol = defaultLastCol + (lstSubject.Count - defaultNumbeOfColumn);

                //Chỉnh lại dòng khác
                double colWidth = firstSheet.GetColumnWidth(firstDynamicCol);
                if (lstSubject.Count > defaultNumbeOfColumn)
                {
                    tempSheet.MergeRow(2, VTVector.dic['F'], lastCol);
                    tempSheet.MergeRow(3, VTVector.dic['F'], lastCol);
                    tempSheet.MergeRow(5, VTVector.dic['A'], lastCol);
                    if (lstSubject.Count > 13)
                    {
                        double resizeWidth = colWidth * 13 / lstSubject.Count;
                        for (int i = firstDynamicCol; i <= lastCol; i++)
                        {
                            tempSheet.SetColumnWidth(i, resizeWidth);
                        }
                    }
                }

                //Tính các công thức 

                foreach (SubjectCat sc in lstSubject)
                {
                    tempSheet.SetCellValue(firstRow + 2, dicSubjectToColumn[sc.SubjectCatID], "${list[].Get(" + sc.SubjectCatID + ")}");
                }

                //Fill dữ liệu chung cho template
                string schoolName = school.SchoolName.ToUpper();
                string supervisingDeptName = school.SupervisingDept.SupervisingDeptName.ToUpper();
                string academicYearTitle = academicYear.DisplayTitle;
                string provinceName = province.ProvinceName;
                DateTime reportDate = DateTime.Now;
                string educationLevelName = educationLevel.Resolution.ToUpper();

                Dictionary<string, object> dicGeneralInfo = new Dictionary<string, object> 
                {
                    {"SchoolName", schoolName}, 
                    {"SupervisingDeptName", supervisingDeptName},
                    {"AcademicYearTitle", academicYearTitle},
                    {"ProvinceName", provinceName},
                    {"ReportDate", reportDate},
                    {"EducationLevel", educationLevelName}
                };

                //Tính dòng cuôi cùng
                int lastColumn = Math.Max(lastCol, defaultLastCol);

                //Tạo vùng cho từng khối
                IVTRange topRange = tempSheet.GetRow(firstRow + 2);
                IVTRange midRange = tempSheet.GetRow(firstRow + 3);
                IVTRange lastRange = tempSheet.GetRow(firstRow + 4);


                //Tạo sheet
                IVTWorksheet dataSheet = oBook.CopySheetToLast(tempSheet,
               new VTVector(firstRow - 1, lastColumn).ToString());
                dataSheet.Name = educationLevel.Resolution;

                IVTRange lastTempRange = tempSheet.GetRange(firstRow, firstDynamicCol + lstSubject.Count, firstRow + 1, lastColumn);

                dataSheet.CopyPaste(tempSheet.GetRange(firstRow, 1, firstRow + 1, lastColumn), firstRow, 1);
                dataSheet.CopyPaste(lastTempRange, firstRow, firstDynamicCol + lstSubject.Count);

                List<PupilOfClass> listPoC = (from mr in listPupil
                                              where mr.ClassProfile.EducationLevelID == educationLevel.EducationLevelID
                                              && listSummedUpRecord.Any(o => o.ClassID == mr.ClassID && o.PupilID == mr.PupilID)
                                              select mr).OrderBy(x => x.ClassProfile.OrderNumber).ThenBy(c => c.ClassProfile.DisplayName)
                                              .ThenBy(cp => cp.OrderInClass)
                                              .ThenBy(x => x.PupilProfile.Name)
                                              .ThenBy(x => x.PupilProfile.FullName)
                                              .ToList<PupilOfClass>();

                int beginRow = firstRow + 2;
                int endRow = beginRow + listPoC.Count - 1;
                int count = 0;
                //Tạo khung dữ liệu
                for (int i = beginRow; i <= endRow; i++)
                {
                    count++;
                    IVTRange copyRange;
                    if (i == beginRow)
                    {
                        copyRange = topRange;
                    }
                    else if (i == endRow)
                    {
                        copyRange = lastRange;
                    }
                    else
                    {
                        copyRange = midRange;
                    }

                    if (count % 5 == 0)
                    {
                        copyRange = lastRange;
                    }

                    dataSheet.CopyPaste(copyRange, i, 1);
                }

                //Tạo dữ liệu cần fill
                int index = 0;
                List<object> listData = new List<object>();
                foreach (PupilOfClass poC in listPoC)
                {
                    index++;
                    Dictionary<string, object> dicData = new Dictionary<string, object>();
                    dicData["Order"] = index;
                    dicData["PupilName"] = poC.PupilProfile.FullName;
                    dicData["ClassName"] = poC.ClassProfile.DisplayName;
                    dicData["Telephone"] = poC.PupilProfile.Telephone;

                    //Lấy dữ liệu xếp loại
                    List<VPupilRanking> lPR = (from s in listPupilRanking
                                              where s.PupilID == poC.PupilID
                                              && s.ClassID == poC.ClassID
                                              select s).ToList();
                    if (lPR != null && lPR.Count > 0)
                    {
                        VPupilRanking pr = lPR[0];
                        CapacityLevel cap = CapacityLevelBusiness.Find(pr.CapacityLevelID);
                        ConductLevel con = ConductLevelBusiness.Find(pr.ConductLevelID);
                        dicData["CapacityLevel"] = cap == null ? "" : cap.CapacityLevel1;
                        dicData["ConductLevel"] = con == null ? "" : con.Resolution;
                    }
                    else
                    {
                        dicData["CapacityLevel"] = "";
                        dicData["ConductLevel"] = "";
                    }

                    //Lấy điểm môn học
                    List<string> listFailSubject = new List<string>();
                    foreach (SubjectCat sc in lstSubject)
                    {
                        //Lấy điểm môn học
                        List<VSummedUpRecord> lSUR = (from s in listSummedUpRecord
                                                     where s.PupilID == poC.PupilID
                                                     && s.ClassID == poC.ClassID
                                                     && s.SubjectID == sc.SubjectCatID
                                                     select s).ToList();
                        string result = "";
                        if (lSUR != null && lSUR.Count > 0)
                        {
                            VSummedUpRecord sur = lSUR[0];
                            if (sur.SummedUpMark != null)
                            {
                                if (sur.SummedUpMark < 5)
                                {
                                    result = "Y";

                                    listFailSubject.Add(sc.DisplayName);
                                }
                            }
                            else
                            {
                                if (sur.JudgementResult == "B")
                                {
                                    result = sur.JudgementResult;
                                    listFailSubject.Add(sc.DisplayName);
                                }
                            }
                        }
                        dicData[sc.SubjectCatID.ToString()] = result;
                    }
                    dicData["RetestSubject"] = string.Join(", ", listFailSubject);

                    //Lấy danh sách các môn thi lại
                    List<string> lPRR = (from s in listPupilRetestRegistration
                                         where s.PupilID == poC.PupilID
                                         && s.ClassID == poC.ClassID
                                         group s by new { s.SubjectCat.SubjectName, s.SubjectCat.SubjectCatID } into gg
                                         select gg.Key.SubjectName).ToList();
                    dicData["RetestRegistrationSubject"] = string.Join(", ", lPRR);
                    listData.Add(dicData);
                }

                dicGeneralInfo["list"] = listData;

                dataSheet.FillVariableValue(dicGeneralInfo);

                dataSheet.CopyPaste(tempSheet.GetRange(firstRow + 7, 1, firstRow + 8, 100), endRow + 2, 1);
                SheetData sheetdata = new SheetData(dataSheet.Name);
                sheetdata.Data["ProvinceName"] = ProvinceBusiness.Find(SchoolProfileBusiness.Find(entity.SchoolID).ProvinceID).ProvinceName;
                sheetdata.Data["day"] = DateTime.Now.Day;
                sheetdata.Data["month"] = DateTime.Now.Month;
                sheetdata.Data["year"] = DateTime.Now.Year;
                dataSheet.FillVariableValue(sheetdata.Data);
                tempSheet.Delete();
                dataSheet.FitAllColumnsOnOnePage = true;
            }



            //Xoá sheet template
            firstSheet.Delete();


            return oBook.ToStream();
        }
        #endregion
    }
}
