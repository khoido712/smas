/**
 * The Viettel License
 *
 * Copyright 2012 Viettel Telecom. All rights reserved.
 * VIETTEL PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */

/** 
* @author  minhh
* @version $Revision: $
*/

using System;
using System.Collections.Generic;
using System.Linq;

using SMAS.DAL.Repository;
using SMAS.Models.Models;
using SMAS.Business.Common;
using SMAS.Business.SearchForm;
using System.Transactions;
namespace SMAS.Business.Business
{
    /// <summary>
    /// <author>minhh</author>
    /// <date>10/10/2012</date>
    /// </summary>
    public partial class PupilFaultBusiness
    {
        #region validate
        /// <summary>
        /// <author>minhh</author>
        /// Xác thực thông tin vi phạm của học sinh
        /// </summary>
        /// <param name="Entity"></param>
        private void validate(PupilFault Entity)
        {
            //Kiểm tra TotalPenalizedMark (= NumberOfFault * PenalizedMark) không phải dạng decimal(4,1) hoặc bằng 0
            //Số chữ số của decimal
            const int DECIMAL_LENGTH = 5;
            //Số chữ số sau dấu phẩy của decimal
            const int DECIMAL_AFTERCOMMA_LENGTH = 2;
            bool checkDecimal = false;
            ValidationMetadata.ValidateObject(Entity);
            AcademicYear academicYear = AcademicYearRepository.Find(Entity.AcademicYearID);
            if (Entity.PupilID != 0)
            {
                PupilOfClass pupilOfClass = PupilOfClassBusiness.All.Where(o => o.PupilID == Entity.PupilID && o.ClassID == Entity.ClassID).OrderByDescending(o => o.AssignedDate).FirstOrDefault();
                if (pupilOfClass == null || pupilOfClass.Status != SystemParamsInFile.PUPIL_STATUS_STUDYING)
                    throw new BusinessException("PupilFault_Validate_PupilNotWorking");

                //AcademicYear(AcademicYearID). FirstSemesterStartDate  < DisciplinedDate < AcademicYear(AcademicYearID). SecondSemesterEndDate
                if (academicYear.FirstSemesterStartDate > Entity.ViolatedDate || academicYear.SecondSemesterEndDate < Entity.ViolatedDate)
                {
                    throw new BusinessException("PupilFault_Validate_NotAfterDate");
                }
                Utils.ValidatePastDate(Entity.ViolatedDate, "PupilFault_Label_ViolatedDate");
                //Tổng điểm trừ TotalPenalizedMark (= NumberOfFault * PenalizedMark) không phải dạng decimal(4,1) hoặc bằng 0
                if (Entity.TotalPenalizedMark == 0)
                {
                    throw new BusinessException("PupilFault_Validate_NotDecimalOrZero");
                }
                decimal? dec = Entity.TotalPenalizedMark;
                string temp = "";
                if (dec.HasValue)
                {
                    temp = dec.Value.ToString();
                    if (temp.Length <= DECIMAL_LENGTH)
                    {
                        if (temp.LastIndexOf(",") <= DECIMAL_LENGTH - DECIMAL_AFTERCOMMA_LENGTH)
                        {
                            checkDecimal = true;
                        }
                    }
                    if (!checkDecimal)
                    {
                        throw new BusinessException("PupilFault_Validate_NotDecimalOrZero");
                    }
                }
                else
                {
                    throw new BusinessException("PupilFault_Validate_NotExistMark");
                }
            }

        }
        #endregion
        /// <summary>
        /// Tính tổng điểm trừ của lớp
        /// </summary>
        /// <param name="ClassID">id lớp</param>
        /// <returns></returns>
        public decimal SumTotalPenalizedMarkByClass(int ClassID, DateTime start, DateTime end)
        {
            IQueryable<PupilFault> IQueryPupilFault = PupilFaultRepository.All
                .Where(x => x.ClassID == ClassID && x.TotalPenalizedMark.HasValue && x.ViolatedDate >= start && x.ViolatedDate <= end);
            if (IQueryPupilFault.Count() > 0)
            {
                decimal TotalMark = IQueryPupilFault.Sum(x => x.TotalPenalizedMark.Value);
                return TotalMark;
            }
            return 0;
        }

        #region SearchBySchool
        /// <summary>
        /// Tìm kiếm thông tin vi phạm kỉ luật của học sinh theo trường
        /// <author>minhh</author>
        /// </summary>
        /// <param name="SchoolID"></param>
        /// <param name="SearchInfo"></param>
        /// <returns></returns>
        public IQueryable<PupilFault> SearchBySchool(int SchoolID = 0, IDictionary<string, object> SearchInfo = null)
        {
            if (SchoolID == 0) return null;
            if (SearchInfo == null) SearchInfo = new Dictionary<string, object>();
            SearchInfo["SchoolID"] = SchoolID;
            return this.Search(SearchInfo);
        }

        public IQueryable<PupilFault> SearchBySchool(int SchoolID, PupilFaultSearchForm SearchForm)
        {
            if (SchoolID == 0) return null;
            SearchForm.SchoolID = SchoolID;
            return SearchForm.Search(PupilFaultRepository.All);
        }
        #endregion

        #region SearchPupilFault
        /// <summary>
        /// Tìm kiếm thông tin vi phạm kỉ luật của học sinh
        /// <author>minhh</author>
        /// </summary>
        /// <param name="SearchInfo"></param>
        /// <returns></returns>
        private IQueryable<PupilFault> Search(IDictionary<string, object> SearchInfo)
        {
            int SchoolID = Utils.GetInt(SearchInfo, "SchoolID");
            int PupilFaultID = Utils.GetInt(SearchInfo, "PupilFaultID");
            int PupilID = Utils.GetInt(SearchInfo, "PupilID");
            List<int> lstPupilID = Utils.GetIntList(SearchInfo, "lstPupilID");
            int ClassID = Utils.GetInt(SearchInfo, "ClassID");
            int FaultID = Utils.GetInt(SearchInfo, "FaultID");
            int AcademicYearID = Utils.GetInt(SearchInfo, "AcademicYearID");
            int EducationLevelID = Utils.GetInt(SearchInfo, "EducationLevelID");
            DateTime? ViolatedDate = Utils.GetDateTime(SearchInfo, "ViolatedDate");
            int NumberOfFault = Utils.GetInt(SearchInfo, "NumberOfFault");
            int TotalPenalizedMark = Utils.GetInt(SearchInfo, "TotalPenalizedMark");
            string PupilCode = Utils.GetString(SearchInfo, "PupilCode");
            string FullName = Utils.GetString(SearchInfo, "FullName");
            int FaultGroupID = Utils.GetInt(SearchInfo, "FaultGroupID");
            DateTime? FromViolatedDate = Utils.GetDateTime(SearchInfo, "FromViolatedDate");
            DateTime? ToViolatedDate = Utils.GetDateTime(SearchInfo, "ToViolatedDate");
            List<int> lstClassID = Utils.GetIntList(SearchInfo, "ListClassID");
            //khoi tao ket qua tim kiem

            IQueryable<PupilFault> results = this.PupilFaultRepository.All.Where(pf => pf.PupilProfile.IsActive);
            if (ClassID != 0)
            {
                results = from pa in results
                          join cp in ClassProfileBusiness.All on pa.ClassID equals cp.ClassProfileID
                          where (!cp.IsActive.HasValue || (cp.IsActive.HasValue && cp.IsActive.Value))
                          && pa.ClassID == ClassID
                          select pa;
            }
            if (SchoolID != 0) results = results.Where(pf => pf.SchoolID == SchoolID);
            if (PupilFaultID != 0) results = results.Where(pf => pf.PupilFaultID == PupilFaultID);
            if (PupilID != 0) results = results.Where(pf => pf.PupilID == PupilID);
            if (lstPupilID.Count > 0)
            {
                results = results.Where(p => lstPupilID.Contains(p.PupilID));
            }
            //if (ClassID != 0) results = results.Where(pf => pf.ClassID == ClassID);
            if (FaultID != 0) results = results.Where(pf => pf.FaultID == FaultID);
            if (AcademicYearID != 0) results = results.Where(pf => pf.AcademicYearID == AcademicYearID);
            if (EducationLevelID != 0) results = results.Where(pf => pf.EducationLevelID == EducationLevelID);
            if (ViolatedDate.HasValue) results = results.Where(pf => pf.ViolatedDate.Day == ViolatedDate.Value.Day
                && pf.ViolatedDate.Month == ViolatedDate.Value.Month && pf.ViolatedDate.Year == ViolatedDate.Value.Year);
            if (NumberOfFault != 0) results = results.Where(pf => pf.NumberOfFault == NumberOfFault);
            if (TotalPenalizedMark != 0) results = results.Where(pf => pf.TotalPenalizedMark == TotalPenalizedMark);
            if (PupilCode.Trim().Length != 0) results = results.Where(o => o.PupilProfile.PupilCode.ToLower().Contains(PupilCode.ToLower()));
            if (FullName.Trim().Length != 0) results = results.Where(o => o.PupilProfile.FullName.ToLower().Contains(FullName.ToLower()));
            if (FaultGroupID != 0) results = results.Where(pf => pf.FaultCriteria.FaultGroup.FaultGroupID == FaultGroupID);
            if (FromViolatedDate != null) results = results.Where(pf => pf.ViolatedDate >= FromViolatedDate);
            if (ToViolatedDate != null) results = results.Where(pf => pf.ViolatedDate <= ToViolatedDate);
            if (lstClassID != null & lstClassID.Count != 0) results = results.Where(o => lstClassID.Contains(o.ClassID));
            return results;
        }
        #endregion

        #region insert
        /// <summary>
        /// <author>minhh</author>
        /// Thêm mới thông tin vi phạm kỉ luật của học sinh
        /// </summary>
        /// <param name="UserID">id người dùng</param>
        /// <param name="SchoolID">id trường học</param>
        /// <param name="AppliedLevel"></param>
        /// <param name="ClassID">id lớp</param>
        /// <param name="PupilID">id học sinh</param>
        /// <param name="FaultGroupID">id loại nhóm vi phạm //dư thừa</param>
        /// <param name="ViolatedDate">ngày vi phạm</param>
        /// <param name="ListFaultCriteriaID">danh sách lỗi vi phạm</param>
        /// <param name="ListNumberOfFault">danh sách số lượng lỗi</param>
        public void InsertPupilFault(int AcademicYearID, int UserID, int SchoolID, int AppliedLevel, int ClassID, int PupilID, int FaultGroupID, DateTime ViolatedDate, List<int> ListFaultCriteriaID, List<int> ListNumberOfFault)
        {
            ClassProfile classProfile = ClassProfileRepository.Find(ClassID);
            PupilFault pupilFault = new PupilFault();
            int? NbrOfFault;
            int FaultID = 0;
            //Với mỗi FaultID trong ListFaultCriteriaID
            foreach (int item in ListFaultCriteriaID)
            {
                //Thông tin lỗi không tồn tại hoặc đã bị xóa – Kiểm tra FaultID với isActive = 1
                new FaultCriteriaBusiness(null).CheckAvailable(item, "PupilFault_Label_Fault");
            }

            //PupilID, ClassID: not compatible(PupilProfile)
            bool PupilProfileCompatible = new PupilProfileRepository(this.context).ExistsRow(GlobalConstants.PUPIL_SCHEMA, "PupilProfile",
                new Dictionary<string, object>()
                {
                    {"PupilProfileID", PupilID},
                    {"CurrentClassID", ClassID}
                }, null);
            if (!PupilProfileCompatible)
            {
                throw new BusinessException("Common_Validate_NotCompatible");
            }

            //ClassID, AcademicYearID: not compatible(ClassProfile)
            bool ClassProfileCompatible = new ClassProfileRepository(this.context).ExistsRow(GlobalConstants.SCHOOL_SCHEMA, "ClassProfile",
                new Dictionary<string, object>()
                {
                    {"ClassProfileID", classProfile.ClassProfileID},
                    {"AcademicYearID",classProfile.AcademicYearID}
                }, null);
            if (!ClassProfileCompatible)
            {
                throw new BusinessException("Common_Validate_NotCompatible");
            }

            //SchoolID, AcademicYearID: not compatible(AcademicYear)
            bool AcademicYearCompatible = new AcademicYearRepository(this.context).ExistsRow(GlobalConstants.SCHOOL_SCHEMA, "AcademicYear",
                new Dictionary<string, object>()
                {
                    {"SchoolID", SchoolID},
                    {"AcademicYearID",classProfile.AcademicYearID}
                }, null);
            if (!AcademicYearCompatible)
            {
                throw new BusinessException("Common_Validate_NotCompatible");
            }

            //EducationLevelID, AppliedLevel(Grade):not compatible(EducationLevel)
            bool EducationLevelCompatible = new EducationLevelRepository(this.context).ExistsRow(GlobalConstants.LIST_SCHEMA, "EducationLevel",
                new Dictionary<string, object>()
                {
                    {"EducationLevelID", classProfile.EducationLevelID},
                    {"Grade",AppliedLevel}
                }, null);
            if (!EducationLevelCompatible)
            {
                throw new BusinessException("Common_Validate_NotCompatible");
            }

            //If(UtilsBusiness.HasHeadTeacherPermission(UserID, ClassID) = true || UtislBusiness.HasOverseePermission(UserID, ClassID) = true): Thực hiện bước 2
            if (UtilsBusiness.HasHeadTeacherPermission(UserID, ClassID) || UtilsBusiness.HasOverSeePermission(UserID, ClassID))
            {
                List<PupilFault> lstPupilFault = new List<PupilFault>();
                //Thực hiện insert vào bảng PupilFault với mỗi phần tử trong ListFaultCriteria tương ứng với ListNumberOfFault
                for (int i = 0; i < ListFaultCriteriaID.Count; i++)
                {
                    NbrOfFault = (int)ListNumberOfFault[i];
                    FaultID = (int)ListFaultCriteriaID[i];
                    FaultCriteria faultCriteria = FaultCriteriaBusiness.Find(FaultID);
                    PupilFault newPupilFault = new PupilFault();
                    newPupilFault.FaultID = FaultID;
                    newPupilFault.NumberOfFault = NbrOfFault;
                    newPupilFault.ClassID = ClassID;
                    newPupilFault.PupilID = PupilID;
                    newPupilFault.SchoolID = SchoolID;
                    newPupilFault.ViolatedDate = ViolatedDate;
                    newPupilFault.AcademicYearID = AcademicYearID;
                    newPupilFault.EducationLevelID = classProfile.EducationLevelID;
                    newPupilFault.TotalPenalizedMark = NbrOfFault * faultCriteria.PenalizedMark;
                    this.validate(newPupilFault);
                    base.Insert(newPupilFault);

                }
            }

        }
        #endregion

        #region update
        /// <summary>
        /// <author>minhh</author>
        /// Sửa thông tin vi phạm kỉ luật của học sinh
        /// </summary>
        /// <param name="UserID"></param>
        /// <param name="pupilFault"></param>
        public void UpdatePupilFault(int UserID, bool IsAdmin, PupilFault pupilFault)
        {
            this.validate(pupilFault);

            //Thông tin vi phạm kỉ luật không tồn tại hoặc đã bị xóa – Kiểm tra PupilFaultID với isActive = 1
            this.CheckAvailable(pupilFault.PupilFaultID, "PupilFault_Label");

            //PupilID, ClassID: not compatible(PupilProfile)
            bool PupilProfileCompatible = new PupilProfileRepository(this.context).ExistsRow(GlobalConstants.PUPIL_SCHEMA, "PupilProfile",
                new Dictionary<string, object>()
                {
                    {"PupilProfileID", pupilFault.PupilID},
                    {"CurrentClassID", pupilFault.ClassID}
                }, null);
            if (!PupilProfileCompatible)
            {
                throw new BusinessException("Common_Validate_NotCompatible");
            }

            //ClassID, AcademicYearID: not compatible(ClassProfile)
            bool ClassProfileCompatible = new ClassProfileRepository(this.context).ExistsRow(GlobalConstants.SCHOOL_SCHEMA, "ClassProfile",
                new Dictionary<string, object>()
                {
                    {"ClassProfileID", pupilFault.ClassID},
                    {"AcademicYearID",pupilFault.AcademicYearID}
                }, null);
            if (!ClassProfileCompatible)
            {
                throw new BusinessException("Common_Validate_NotCompatible");
            }

            //SchoolID, AcademicYearID: not compatible(AcademicYear)
            bool AcademicYearCompatible = new AcademicYearRepository(this.context).ExistsRow(GlobalConstants.SCHOOL_SCHEMA, "AcademicYear",
                new Dictionary<string, object>()
                {
                    {"SchoolID", pupilFault.SchoolID},
                    {"AcademicYearID",pupilFault.AcademicYearID}
                }, null);
            if (!AcademicYearCompatible)
            {
                throw new BusinessException("Common_Validate_NotCompatible");
            }

            // Kiem tra co bi khoa hay khong
            LockTraining lockTraining = !IsAdmin ? LockTrainingBusiness.SearchByClass(pupilFault.ClassID, new Dictionary<string, object>()
                {
                    {"IsViolation", true}
                }).FirstOrDefault() : null;
            if (lockTraining != null)
            {
                DateTime fromLockDate = lockTraining.FromDate.HasValue ? lockTraining.FromDate.Value : AcademicYearBusiness.Find(pupilFault.AcademicYearID).FirstSemesterStartDate.Value;
                if (lockTraining != null && fromLockDate <= pupilFault.ViolatedDate && lockTraining.ToDate.AddDays(1) > pupilFault.ViolatedDate)
                {
                    List<object> listParam = new List<object>();
                    listParam.Add(fromLockDate.ToString("dd/MM/yyyy"));
                    listParam.Add(lockTraining.ToDate.ToString("dd/MM/yyyy"));
                    throw new BusinessException("LockTraining_Label_ViolationTitle", listParam);
                }
            }

            if (UtilsBusiness.HasHeadTeacherPermission(UserID, pupilFault.ClassID) || UtilsBusiness.HasOverSeePermission(UserID, pupilFault.ClassID))
            {
                base.Update(pupilFault);
            }


        }
        #endregion

        #region delete
        /// <summary>
        /// <author>minhh</author>
        /// <Xóa thông tin vi phạm kỉ luật của học sinh
        /// </summary>
        /// <param name="UserID"></param>
        /// <param name="PupilFaultID"></param>
        /// <param name="SchoolID"></param>
        public void DeletePupilFault(int UserID, bool IsAdmin, int PupilFaultID, int SchoolID)
        {
            this.CheckAvailable(PupilFaultID, "PupilFault_Label");
            //PupilID, SchoolID: not compatible(PupilProfile)
            PupilFault pupilFault = PupilFaultRepository.Find(PupilFaultID);
            this.validate(pupilFault);
            bool PupilProfileCompatible = new PupilProfileRepository(this.context).ExistsRow(GlobalConstants.PUPIL_SCHEMA, "PupilProfile",
               new Dictionary<string, object>()
                {
                    {"PupilProfileID", pupilFault.PupilID},
                    {"CurrentSchoolID", SchoolID}
                }, null);
            if (!PupilProfileCompatible)
            {
                throw new BusinessException("Common_Validate_NotCompatible");
            }

            //AcademicYearBusiness.IsCurrentYear(AcademicYearID) = FALSE => Năm học không phải năm hiện tại
            if (!AcademicYearBusiness.IsCurrentYear(pupilFault.AcademicYearID))
            {
                throw new BusinessException("PupilFault_Validate_IsNotCurrentYear");
            }

            // Kiểm tra trạng thái học sinh vi phạm kỉ luật
            //PupilOfClass(PupilID).Status phải nhận các giá trị PUPIL_STATUS_STUDYING
            if (pupilFault.PupilID != 0)
            {
                List<PupilOfClass> lstPupilOfClass = PupilOfClassBusiness.All.Where(o => o.PupilID == pupilFault.PupilID).Where(o => o.ClassID == pupilFault.ClassID).ToList();
                foreach (PupilOfClass item in lstPupilOfClass)
                {
                    if (item.Status != SystemParamsInFile.PUPIL_STATUS_STUDYING)
                    {
                        throw new BusinessException("PupilFault_Validate_PupilNotWorking");
                    }
                }
            }
            //kiểm tra vi phạm có liên quan tới điểm danh không
            if (pupilFault.FaultCriteria.Absence == "C" || pupilFault.FaultCriteria.Absence == "K")
            {
                throw new BusinessException("PupilFault_Validate_PupilNotAbsence");
            }

            // Kiem tra co bi khoa hay khong
            LockTraining lockTraining = !IsAdmin ? LockTrainingBusiness.SearchByClass(pupilFault.ClassID, new Dictionary<string, object>()
                {
                    {"IsViolation", true}
                }).FirstOrDefault() : null;
            if (lockTraining != null)
            {
                DateTime fromLockDate = lockTraining.FromDate.HasValue ? lockTraining.FromDate.Value : AcademicYearBusiness.Find(pupilFault.AcademicYearID).FirstSemesterStartDate.Value;
                if (lockTraining != null && fromLockDate <= pupilFault.ViolatedDate && lockTraining.ToDate.AddDays(1) > pupilFault.ViolatedDate)
                {
                    List<object> listParam = new List<object>();
                    listParam.Add(fromLockDate.ToString("dd/MM/yyyy"));
                    listParam.Add(lockTraining.ToDate.ToString("dd/MM/yyyy"));
                    throw new BusinessException("LockTraining_Label_ViolationTitle", listParam);
                }
            }

            if (UtilsBusiness.HasHeadTeacherPermission(UserID, pupilFault.ClassID) || UtilsBusiness.HasOverSeePermission(UserID, pupilFault.ClassID))
            {
                base.Delete(PupilFaultID);
            }

        }

        public void DeleteAllFaultInDay(int UserID, bool IsAdmin, int PupilFaultID, int SchoolID)
        {
            using (TransactionScope TransactionScope = new TransactionScope())
            {
                this.CheckAvailable(PupilFaultID, "PupilFault_Label");
                //PupilID, SchoolID: not compatible(PupilProfile)
                PupilFault pupilFault = PupilFaultRepository.Find(PupilFaultID);

                if (pupilFault.PupilProfile.CurrentSchoolID != SchoolID)
                {
                    throw new BusinessException("Common_Validate_NotCompatible");
                }

                //AcademicYearBusiness.IsCurrentYear(AcademicYearID) = FALSE => Năm học không phải năm hiện tại
                if (!AcademicYearBusiness.IsCurrentYear(pupilFault.AcademicYearID))
                {
                    throw new BusinessException("PupilFault_Validate_IsNotCurrentYear");
                }

                // Kiểm tra trạng thái học sinh vi phạm kỉ luật
                //PupilOfClass(PupilID).Status phải nhận các giá trị PUPIL_STATUS_STUDYING
                if (pupilFault.PupilProfile.ProfileStatus != SystemParamsInFile.PUPIL_STATUS_STUDYING)
                {
                    throw new BusinessException("PupilFault_Validate_PupilNotWorking");
                }

                if (!UtilsBusiness.HasHeadTeacherPermission(UserID, pupilFault.ClassID)
                    && !UtilsBusiness.HasOverSeePermission(UserID, pupilFault.ClassID))
                {
                    throw new BusinessException("Common_Validate_User");
                }

                // Kiem tra co bi khoa hay khong
                LockTraining lockTraining = !IsAdmin ? LockTrainingBusiness.SearchByClass(pupilFault.ClassID, new Dictionary<string, object>()
                {
                    {"IsViolation", true}
                }).FirstOrDefault() : null;
                if (lockTraining != null)
                {
                    DateTime fromLockDate = lockTraining.FromDate.HasValue ? lockTraining.FromDate.Value : AcademicYearBusiness.Find(pupilFault.AcademicYearID).FirstSemesterStartDate.Value;
                    if (fromLockDate <= pupilFault.ViolatedDate && lockTraining.ToDate.AddDays(1) > pupilFault.ViolatedDate)
                    {
                        List<object> listParam = new List<object>();
                        listParam.Add(fromLockDate.ToString("dd/MM/yyyy"));
                        listParam.Add(lockTraining.ToDate.ToString("dd/MM/yyyy"));
                        throw new BusinessException("LockTraining_Label_ViolationTitle", listParam);
                    }
                }

                //Xoá dữ liệu
                List<PupilFault> listToDelete = SearchBySchool(SchoolID, new PupilFaultSearchForm()
                {
                    AcademicYearID = pupilFault.AcademicYearID,
                    PupilID = pupilFault.PupilID,
                    ViolatedDate = pupilFault.ViolatedDate
                }).ToList().FindAll(x => string.IsNullOrWhiteSpace(x.FaultCriteria.Absence));
                PupilFaultRepository.DeleteAll(listToDelete);
                context.SaveChanges();
                TransactionScope.Complete();
            }
        }
        #endregion

        #region Update IsSMS is TRUE
        /// <summary>
        /// Anhvd
        /// </summary>
        /// <param name="dic"></param>
        /// <param name="pupilIds"></param>
        /// <returns></returns>
        public bool EnableIsSMS(IDictionary<string, object> dic, List<int> pupilIds)
        {
            IQueryable<PupilFault> lstFault = this.Search(dic);
            for (int i = 0; i < pupilIds.Count; i++)
            {
                int pupilId = pupilIds[i];
                IQueryable<PupilFault> lstFaultPupil = lstFault.Where(o => o.PupilID == pupilId);
                foreach (var item in lstFaultPupil)
                {
                    item.IsSMS = true;
                    this.Update(item);
                }
            }
            this.Save();
            return true;
        }
        #endregion

        //phuongh1 09/08/2012
        public void UpdateForPupil(int UserID, bool IsAdmin,
            int SchoolID, int AcademicYearID, int PupilID,
            DateTime Date, List<PupilFault> ListData)
        {
            using (TransactionScope TransactionScope = new TransactionScope())
            {
                #region Validate
                PupilProfile PupilProfile = PupilProfileBusiness.Find(PupilID);

                if (!UtilsBusiness.HasOverseeingTeacherPermission(UserID, PupilProfile.CurrentClassID)
                    && !UtilsBusiness.HasHeadTeacherPermission(UserID, PupilProfile.CurrentClassID))
                {
                    throw new BusinessException("Common_Validate_User");
                }

                if (PupilProfile.CurrentSchoolID != SchoolID)
                {
                    throw new BusinessException("Common_Validate_NotCompatible");
                }

                /*if (PupilProfile.CurrentAcademicYearID != AcademicYearID)
                {
                    throw new BusinessException("Common_Validate_NotCompatible");
                }*/

                if (PupilProfile.ProfileStatus != GlobalConstants.PUPIL_STATUS_STUDYING)
                {
                    throw new BusinessException("Common_Validate_Pupil_Status_Studying");
                }

                if (!AcademicYearBusiness.IsCurrentYear(AcademicYearID))
                {
                    throw new BusinessException("Common_Validate_IsCurrentYear");
                }

                Utils.ValidatePastDate(Date, "PupilFault_Label_ViolatedDate");

                AcademicYear AcademicYear = AcademicYearBusiness.Find(AcademicYearID);
                if (AcademicYear.FirstSemesterStartDate > Date || Date > AcademicYear.SecondSemesterEndDate)
                {
                    throw new BusinessException("PupilFault_Validate_NotAfterDate");
                }

                // Kiem tra co bi khoa hay khong
                LockTraining lockTraining = !IsAdmin ? LockTrainingBusiness.SearchByClass(PupilProfile.CurrentClassID, new Dictionary<string, object>()
                {
                    {"IsViolation", true}
                }).FirstOrDefault() : null;
                if (lockTraining != null)
                {
                    DateTime fromLockDate = lockTraining.FromDate.HasValue ? lockTraining.FromDate.Value : AcademicYear.FirstSemesterStartDate.Value;
                    if (fromLockDate <= Date && lockTraining.ToDate.AddDays(1) > Date)
                    {
                        List<object> listParam = new List<object>();
                        listParam.Add(fromLockDate.ToString("dd/MM/yyyy"));
                        listParam.Add(lockTraining.ToDate.ToString("dd/MM/yyyy"));
                        throw new BusinessException("LockTraining_Label_ViolationTitle", listParam);
                    }
                }
                // End kiem tra khoa

                List<FaultCriteria> lstFaultCriteria = FaultCriteriaBusiness.SearchBySchool(SchoolID).ToList();
                foreach (PupilFault pf in ListData)
                {
                    if (!lstFaultCriteria.Exists(x => x.FaultCriteriaID == pf.FaultID))
                    {
                        throw new BusinessException("Common_Validate_NotCompatible");
                    }

                    //if (!string.IsNullOrWhiteSpace(lstFaultCriteria.Find(x => x.FaultCriteriaID == pf.FaultID).Absence))
                    //{
                    //    throw new BusinessException("Common_Validate_NotCompatible");
                    //}

                    if (pf.NumberOfFault < 1 || pf.NumberOfFault > 9)
                    {
                        throw new BusinessException("Common_Validate_NotCompatible");
                    }
                }
                #endregion

                //Xoá dữ liệu cũ
                List<PupilFault> listToDelete = SearchBySchool(SchoolID, new PupilFaultSearchForm()
                {
                    AcademicYearID = AcademicYearID,
                    PupilID = PupilID,
                    ViolatedDate = Date
                }).ToList();

                PupilFaultRepository.DeleteAll(listToDelete);

                //Insert dữ liệu mới
                foreach (PupilFault pf in ListData)
                {
                    pf.AcademicYearID = AcademicYearID;
                    pf.ClassID = PupilProfile.CurrentClassID;
                    pf.EducationLevelID = PupilProfile.ClassProfile.EducationLevelID;
                    pf.PupilID = PupilID;
                    pf.SchoolID = SchoolID;
                    pf.ViolatedDate = Date;
                    pf.TotalPenalizedMark = pf.NumberOfFault
                        * lstFaultCriteria.Find(x => x.FaultCriteriaID == pf.FaultID).PenalizedMark;
                    PupilFaultRepository.Insert(pf);
                }

                context.SaveChanges();
                TransactionScope.Complete();
            }
        }

        //SMAS Mobile
        public void UpdateForListPupil(int UserID, int SchoolID, int AcademicYearID,int ClassID, DateTime Date, List<PupilFault> lstPupilFault,int FaultID)
        {
            List<int> lstPupilID = lstPupilFault.Select(p => p.PupilID).Distinct().ToList();
            IDictionary<string, object> dic = new Dictionary<string, object>();
            dic.Add("AcademicYearID", AcademicYearID);
            dic.Add("lstPupilID", lstPupilID);
            dic.Add("ViolatedDate", Date);
            dic.Add("FaultID", FaultID);
            dic.Add("ClassID", ClassID);
            List<PupilFault> listToDelete = SearchBySchool(SchoolID, dic).ToList();
            this.PupilFaultBusiness.DeleteAll(listToDelete);

            PupilFault objInsertPupilFault = null;
            PupilFault objPupilFault = null;
            List<FaultCriteria> lstFaultCriteria = FaultCriteriaBusiness.SearchBySchool(SchoolID).ToList();
            for (int i = 0; i < lstPupilFault.Count; i++)
            {
                objPupilFault = lstPupilFault[i];
                objInsertPupilFault = new PupilFault();
                if (objPupilFault.NumberOfFault > 0)
                {
                    objInsertPupilFault.AcademicYearID = AcademicYearID;
                    objInsertPupilFault.ClassID = objPupilFault.ClassID;
                    objInsertPupilFault.EducationLevelID = objPupilFault.EducationLevelID;
                    objInsertPupilFault.PupilID = objPupilFault.PupilID;
                    objInsertPupilFault.SchoolID = SchoolID;
                    objInsertPupilFault.ViolatedDate = Date;
                    objInsertPupilFault.FaultID = objPupilFault.FaultID;
                    objInsertPupilFault.NumberOfFault = objPupilFault.NumberOfFault;
                    objInsertPupilFault.TotalPenalizedMark = objInsertPupilFault.NumberOfFault
                            * lstFaultCriteria.Find(x => x.FaultCriteriaID == objInsertPupilFault.FaultID).PenalizedMark;
                    PupilFaultBusiness.Insert(objInsertPupilFault);
                }
            }
            PupilFaultBusiness.Save();
        }

    }
}
