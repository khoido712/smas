﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Linq.Expressions;
using System.Web;
using log4net;

using SMAS.DAL.Repository;
using SMAS.DAL.IRepository;
using SMAS.Business.IBusiness;
using SMAS.Models.Models;
using SMAS.Business.BusinessObject;
using SMAS.Business.Common;
using System.IO;
using SMAS.VTUtils.Excel.Export;
using System.Data.Objects.SqlClient;

namespace SMAS.Business.Business
{
    public class ReportExamResultBusiness : GenericBussiness<ProcessedReport>, IReportExamResultBusiness
    {
        //Tạo báo cáo danh sách điểm thi theo lớp
        //PhuongTD
        //20/12/2012
        public Stream CreateReportExamMarkByClass(int SchoolID, IDictionary<string, object> dic)
        {
            int AcademicYearID = Utils.GetInt(dic, "AcademicYearID");
            int AppliedLevel = Utils.GetInt(dic, "AppliedLevel");
            int ExaminationID = Utils.GetInt(dic, "ExaminationID");
            int EducationLevelID = Utils.GetInt(dic, "EducationLevelID");
            int ExaminationSubjectID = Utils.GetInt(dic, "ExaminationSubjectID");
            int ClassID = Utils.GetInt(dic, "ClassID");
            string reportCode = SystemParamsInFile.REPORT_DANH_SACH_DIEM_THI_THEO_LOP;
            ReportDefinition reportDef = ReportDefinitionBusiness.GetByCode(reportCode);
            string templatePath = SystemParamsInFile.TEMPLATE_FOLDER + reportDef.TemplateDirectory + "/" + reportDef.TemplateName;
            IVTWorkbook oBook = VTExport.OpenWorkbook(templatePath);
            IVTWorksheet firstSheet = oBook.GetSheet(1);
            int headerEndRow = 7;
            int headerEndCol = 9; //"I"
            //Tạo template
            IVTWorksheet tempSheet = oBook.CopySheetToLast(firstSheet, VTVector.ColumnIntToString(headerEndCol) + headerEndRow);
            int gridFirstRow = headerEndRow + 1;
            int startDynamicCol = 4;
            Examination exam = ExaminationBusiness.Find(ExaminationID);
            if (exam.UsingSeparateList != 1) //Nếu kì thi có danh sách riêng cho từng môn thì Ko có thêm cột SBD
            {
                startDynamicCol++;
            }
            //Điền dữ liệu chung cho template
            Dictionary<string, object> genaralInfo = new Dictionary<string, object>();
            SchoolProfile school = SchoolProfileBusiness.Find(SchoolID);
            Province province = school.Province;
            string schoolName = school.SchoolName.ToUpper();
            string supervisingDeptName = school.SupervisingDept.SupervisingDeptName.ToUpper();
            string provinceName = province.ProvinceName;
            Examination examination = ExaminationBusiness.Find(ExaminationID);
            string examinationName = examination.Title.ToUpper();
            AcademicYear academicYear = AcademicYearBusiness.Find(AcademicYearID);
            string academicYearTitle = academicYear.DisplayTitle;
            genaralInfo["DeptName"] = supervisingDeptName;
            genaralInfo["SchoolName"] = schoolName;
            genaralInfo["ExaminationName"] = examinationName;
            genaralInfo["AcademicYear"] = academicYearTitle;
            genaralInfo["ProvinceName"] = provinceName;
            genaralInfo["ReportDate"] = DateTime.Now;
            tempSheet.GetRange("A1", VTVector.ColumnIntToString(headerEndCol) + (headerEndRow - 2)).FillVariableValue(genaralInfo);

            //Danh sách môn thi
            List<ExaminationSubjectBO> lstExaminationSubject = new List<ExaminationSubjectBO>();
            if (ExaminationSubjectID == 0)
            {
                Dictionary<string, object> dictionary = new Dictionary<string, object>();
                dictionary["AcademicYearID"] = AcademicYearID;
                dictionary["ExaminationID"] = ExaminationID;
                dictionary["EducationLevelID"] = EducationLevelID;
                IQueryable<ExaminationSubject> qExaminationSubject = ExaminationSubjectBusiness.SearchBySchool(SchoolID, dictionary);
                lstExaminationSubject = (from es in qExaminationSubject
                                         select new ExaminationSubjectBO()
                                         {
                                             ExaminationSubjectID = es.ExaminationSubjectID,
                                             SubjectID = es.SubjectID,
                                             DisplayName = es.SubjectCat.DisplayName,
                                             OrderInSubject = es.SubjectCat.OrderInSubject
                                         }).OrderBy(o => o.OrderInSubject).ThenBy(o => o.DisplayName).ToList();
            }
            else
            {
                ExaminationSubject examinationSubject = ExaminationSubjectBusiness.Find(ExaminationSubjectID);
                lstExaminationSubject.Add(new ExaminationSubjectBO()
                {
                    ExaminationSubjectID = examinationSubject.ExaminationSubjectID,
                    SubjectID = examinationSubject.SubjectID,
                    DisplayName = examinationSubject.SubjectCat.DisplayName
                });
            }
            //Copy các cột tĩnh sang tempSheet
            IVTRange staticGrid = firstSheet.GetRange(gridFirstRow, 1, gridFirstRow + 3, startDynamicCol - 1);
            tempSheet.CopyPasteSameSize(staticGrid, gridFirstRow, 1);
            //Tạo các cột động
            int countSubject = 0;
            IVTRange rangeSubjectName = firstSheet.GetRange(gridFirstRow + 1, startDynamicCol + 1, gridFirstRow + 1, startDynamicCol + 1);
            IVTRange rangeSumSubject = firstSheet.GetRange(gridFirstRow, startDynamicCol + 1, gridFirstRow, startDynamicCol + 1);
            IVTRange rangeSubjectRow = firstSheet.GetRange(gridFirstRow + 2, startDynamicCol + 1, gridFirstRow + 3, startDynamicCol + 1);
            foreach (ExaminationSubjectBO examSubject in lstExaminationSubject)
            {
                int examSubjectID = examSubject.ExaminationSubjectID;
                tempSheet.CopyPaste(rangeSumSubject, gridFirstRow, startDynamicCol + countSubject);
                tempSheet.CopyPaste(rangeSubjectName, gridFirstRow + 1, startDynamicCol + countSubject);
                tempSheet.SetCellValue(gridFirstRow + 1, startDynamicCol + countSubject, examSubject.DisplayName);
                tempSheet.CopyPaste(rangeSubjectRow, gridFirstRow + 2, startDynamicCol + countSubject);
                tempSheet.SetCellValue(gridFirstRow + 2, startDynamicCol + countSubject, "${list[].Get(ExamSubject" + examSubjectID + ")}");
                tempSheet.SetCellValue(gridFirstRow + 3, startDynamicCol + countSubject, "${list[].Get(ExamSubject" + examSubjectID + ")}");
                countSubject++;
            }
            int lastDynamicCol = startDynamicCol + countSubject - 1;


            tempSheet.GetRange(gridFirstRow, startDynamicCol, gridFirstRow, lastDynamicCol).Merge();
            tempSheet.SetCellValue(gridFirstRow, startDynamicCol, "Điểm thi");
            //Điền cột ghi chú
            IVTRange rangeDescription = firstSheet.GetRange("I" + gridFirstRow, "I" + (gridFirstRow + 3));
            tempSheet.CopyPasteSameSize(rangeDescription, VTVector.ColumnIntToString(lastDynamicCol + 1) + gridFirstRow);
            Dictionary<string, object> dic2 = new Dictionary<string, object>();
            dic2["ExaminationID"] = ExaminationID;
            dic2["EducationLevelID"] = EducationLevelID;
            dic2["AcademicYearID"] = AcademicYearID;
            dic2["AppliedLevel"] = AppliedLevel;
            dic2["ExaminationSubjectID"] = ExaminationSubjectID;
            dic2["ClassID"] = ClassID;
            //Dữ liệu điểm
            IEnumerable<CandidateMark> lsCandidateMark = CandidateBusiness.SearchMark(SchoolID, (int)AppliedLevel, dic2);
            //For từng lớp để điền dữ liệu
            //Danh sách lớp
            List<int> lstClassID = new List<int>();
            if (ClassID == 0)
            {
                Dictionary<string, object> dic3 = new Dictionary<string, object>();
                dic3["AcademicYearID"] = AcademicYearID;
                dic3["ExaminationID"] = ExaminationID;
                dic3["EducationLevelID"] = EducationLevelID;
                IQueryable<Candidate> lsCandidate = CandidateBusiness.SearchBySchool(SchoolID, dic3);
                lstClassID = lsCandidate.Select(o => o.ClassID.Value).Distinct().ToList();
            }
            else
            {
                lstClassID.Add(ClassID);
            }
            //Style các dòng
            IVTRange tempRow = tempSheet.GetRange("A" + (gridFirstRow + 2), VTVector.ColumnIntToString(lastDynamicCol + 1) + (gridFirstRow + 2));
            IVTRange tempRowBold = tempSheet.GetRange("A" + (gridFirstRow + 3), VTVector.ColumnIntToString(lastDynamicCol + 1) + (gridFirstRow + 3));
            if (lastDynamicCol < 9)
            {
                lastDynamicCol = 9;
            }
            List<ClassProfile> lstcp = ClassProfileBusiness.All.Where(o => lstClassID.Contains(o.ClassProfileID)).OrderBy(u => u.DisplayName).ToList();
            lstClassID = lstcp.Select(o => o.ClassProfileID).ToList();
            foreach (int idClass in lstClassID)
            {
                ClassProfile cp = ClassProfileBusiness.Find(idClass);
                IVTWorksheet dataSheet = oBook.CopySheetToLast(tempSheet, VTVector.ColumnIntToString(lastDynamicCol + 1) + (gridFirstRow + 1));
                dataSheet.Name = cp.DisplayName;
                Dictionary<string, object> classNameDic = new Dictionary<string, object> { { "ClassName", cp.DisplayName.ToUpper() } };
                dataSheet.GetRange("A6", "I6").FillVariableValue(classNameDic);
                IEnumerable<CandidateMark> lsCandidateMarkByClass = lsCandidateMark.Where(o => o.ClassID == idClass);
                var lsPupilProfile = examination.UsingSeparateList == 0 ? // Cac mon thi co danh sach thi sinh giong nhau
                    lsCandidateMarkByClass
                        .ToList()
                        .OrderBy(p=>SMAS.Business.Common.Utils.SortABC(p.PupilName.getOrderingName(p.EthnicCode)))
                        .Select(o => new
                        {
                            PupilID = o.PupilID,
                            PupilCode = o.PupilCode,
                            Name = o.Name,
                            PupilName = o.PupilName,
                            NamedListCode = o.NamedListCode
                        })
                        .Distinct()
                        .ToList()
                    :
                    lsCandidateMarkByClass
                        .ToList()
                        .OrderBy(p => SMAS.Business.Common.Utils.SortABC(p.PupilName.getOrderingName(p.EthnicCode)))
                        .Select(o => new
                        {
                            PupilID = o.PupilID,
                            PupilCode = o.PupilCode,
                            Name = o.Name,
                            PupilName = o.PupilName,
                            NamedListCode = ""//o.NamedListCode
                        })
                        .Distinct()
                        .ToList();
                List<Object> lstData = new List<object>();
                int countPupil = 0;
                if (exam.UsingSeparateList == 1)
                {
                    foreach (var pupil in lsPupilProfile)
                    {
                        countPupil++;
                        if (countPupil % 5 == 0 || countPupil == lsPupilProfile.Count)
                        {
                            dataSheet.CopyPasteSameSize(tempRowBold, gridFirstRow + 1 + countPupil, 1);
                        }
                        else
                        {
                            dataSheet.CopyPasteSameSize(tempRow, gridFirstRow + 1 + countPupil, 1);
                        }
                        Dictionary<string, object> markData = new Dictionary<string, object>();
                        markData["Order"] = countPupil;
                        markData["PupilCode"] = pupil.PupilCode;
                        markData["FullName"] = pupil.PupilName;
                        markData["NamedListCode"] = pupil.NamedListCode;
                        string description = "";
                        IEnumerable<CandidateMark> marks = lsCandidateMarkByClass.Where(o => o.PupilID == pupil.PupilID);
                        foreach (CandidateMark mark in marks)
                        {
                            markData["ExamSubject" + mark.SubjectID] = mark.RealMark == null ? "" : mark.RealMark.ToString().Replace(".", ","); // Diem mon hoc
                            if (mark.Description != null && mark.Description != "") // Tao mo ta
                            {
                                description += mark.Description;
                            }
                            }
                        if (description.EndsWith(";")) // Bo dau cham hoi cuoi cung
                        {
                            description = description.Substring(0, description.Length - 1);
                        }
                        markData["Description"] = description;
                        lstData.Add(markData);
                    }
                }
                else
                {
                    List<int> listPupilID = lsPupilProfile.Select(o => o.PupilID).Distinct().ToList();
                    foreach (var pupilID in listPupilID)
                    {
                        var pupil = lsPupilProfile.Find(o => o.PupilID == pupilID);
                        countPupil++;
                        if (countPupil % 5 == 0 || countPupil == listPupilID.Count)
                        {
                            dataSheet.CopyPasteSameSize(tempRowBold, gridFirstRow + 1 + countPupil, 1);
                        }
                        else
                        {
                            dataSheet.CopyPasteSameSize(tempRow, gridFirstRow + 1 + countPupil, 1);
                        }
                        Dictionary<string, object> markData = new Dictionary<string, object>();
                        markData["Order"] = countPupil;
                        markData["PupilCode"] = pupil.PupilCode;
                        markData["FullName"] = pupil.PupilName;
                        markData["NamedListCode"] = pupil.NamedListCode;
                        markData["Description"] = "";
                        foreach (var examSubject in lstExaminationSubject)
                        {
                            CandidateMark mark = lsCandidateMarkByClass.Where(o => o.PupilID == pupilID && o.SubjectID == examSubject.ExaminationSubjectID).FirstOrDefault();
                            if (mark != null)
                            {
                                markData["ExamSubject" + mark.SubjectID] = mark.RealMark == null ? "" : mark.RealMark.ToString().Replace(".", ",");
                                markData["Description"] += mark.Description;
                                if (mark.Description != null && mark.Description != "")
                                {
                                    markData["Description"] += ";";
                                }
                            }
                        }
                        lstData.Add(markData);
                    }
                }

                dataSheet.GetRange("A" + gridFirstRow, VTVector.ColumnIntToString(lastDynamicCol + 1) + (gridFirstRow + 2 + countPupil)).FillVariableValue(new Dictionary<string, object> { { "list", lstData } });
            }
            firstSheet.Delete();
            tempSheet.Delete();
            return oBook.ToStream();
        }
        //Danh sách điểm thi theo phòng thi
        //PhuongTD
        //20/12/2012
        /// <summary>
        /// Thống kê kết quả theo phòng thi
        /// </summary>
        /// <modifier>AnhVD9</modifier>
        /// <param name="SchoolID"></param>
        /// <param name="dic"></param>
        /// <returns></returns>
        public Stream CreateReportExamMarkByRoom(int SchoolID, IDictionary<string, object> dic)
        {
            int AcademicYearID = Utils.GetInt(dic, "AcademicYearID");
            int AppliedLevel = Utils.GetInt(dic, "AppliedLevel");
            int ExaminationID = Utils.GetInt(dic, "ExaminationID");
            int EducationLevelID = Utils.GetInt(dic, "EducationLevelID");
            int ExaminationSubjectID = Utils.GetInt(dic, "ExaminationSubjectID");
            int ExaminationRoomID = Utils.GetInt(dic, "ExaminationRoomID");
            string reportCode = SystemParamsInFile.REPORT_DANH_SACH_DIEM_THI_THEO_PHONG_THI;
            ReportDefinition reportDef = ReportDefinitionBusiness.GetByCode(reportCode);
            string templatePath = SystemParamsInFile.TEMPLATE_FOLDER + reportDef.TemplateDirectory + "/" + reportDef.TemplateName;
            IVTWorkbook oBook = VTExport.OpenWorkbook(templatePath);
            IVTWorksheet firstSheet = oBook.GetSheet(1);
            int headerEndRow = 7;
            int headerEndCol = 9; //"I"
            //Tạo template
            IVTWorksheet tempSheet = oBook.CopySheetToLast(firstSheet, VTVector.ColumnIntToString(headerEndCol) + headerEndRow);
            int gridFirstRow = headerEndRow + 1;
            int startDynamicCol = 4;
            Examination exam = ExaminationBusiness.Find(ExaminationID);
            if (exam.UsingSeparateList == 1) //Nếu kì thi có danh sách riêng cho từng môn thì có thêm cột SBD
            {
                startDynamicCol++;
            }
            //Điền dữ liệu chung cho template
            Dictionary<string, object> genaralInfo = new Dictionary<string, object>();
            SchoolProfile school = SchoolProfileBusiness.Find(SchoolID);
            Province province = school.Province;
            string schoolName = school.SchoolName.ToUpper();
            string supervisingDeptName = school.SupervisingDept.SupervisingDeptName.ToUpper();
            string provinceName = province.ProvinceName;
            Examination examination = ExaminationBusiness.Find(ExaminationID);
            string examinationName = examination.Title.ToUpper();
            AcademicYear academicYear = AcademicYearBusiness.Find(AcademicYearID);
            string academicYearTitle = academicYear.DisplayTitle;
            genaralInfo["DeptName"] = supervisingDeptName;
            genaralInfo["SchoolName"] = schoolName;
            genaralInfo["ExaminationName"] = examinationName;
            genaralInfo["AcademicYear"] = academicYearTitle;
            genaralInfo["ProvinceName"] = provinceName;
            genaralInfo["ReportDate"] = DateTime.Now;
            tempSheet.GetRange("A1", VTVector.ColumnIntToString(headerEndCol) + (headerEndRow - 2)).FillVariableValue(genaralInfo);

            //Danh sách môn thi

            List<ExaminationSubjectBO> lstExaminationSubject = new List<ExaminationSubjectBO>();
            if (ExaminationSubjectID == 0) // Chọn tất cả các môn thi
            {
                Dictionary<string, object> dictionary = new Dictionary<string, object>();
                dictionary["AcademicYearID"] = AcademicYearID;
                dictionary["ExaminationID"] = ExaminationID;
                dictionary["EducationLevelID"] = EducationLevelID;
                dictionary["AppliedLevel"] = AppliedLevel;
                IQueryable<ExaminationSubject> qExaminationSubject = ExaminationSubjectBusiness.SearchBySchool(SchoolID, dictionary);
                lstExaminationSubject = (from es in qExaminationSubject
                                         select new ExaminationSubjectBO()
                                         {
                                             ExaminationSubjectID = es.ExaminationSubjectID,
                                             SubjectID = es.SubjectID,
                                             DisplayName = es.SubjectCat.DisplayName,
                                             OrderInSubject = es.SubjectCat.OrderInSubject
                                         }).OrderBy(o => o.OrderInSubject).ThenBy(o => o.DisplayName).ToList();
            }
            else
            {
                ExaminationSubject examinationSubject = ExaminationSubjectBusiness.Find(ExaminationSubjectID);
                lstExaminationSubject.Add(new ExaminationSubjectBO()
                {
                    ExaminationSubjectID = examinationSubject.ExaminationSubjectID,
                    SubjectID = examinationSubject.SubjectID,
                    DisplayName = examinationSubject.SubjectCat.DisplayName
                });
            }
            //Copy các cột tĩnh sang tempSheet
            IVTRange staticGrid = firstSheet.GetRange(gridFirstRow, 1, gridFirstRow + 3, startDynamicCol - 1);
            tempSheet.CopyPasteSameSize(staticGrid, gridFirstRow, 1);
            //Tạo các cột động
            int countSubject = 0;
            IVTRange rangeSubjectName = firstSheet.GetRange(gridFirstRow + 1, startDynamicCol + 1, gridFirstRow + 1, startDynamicCol + 1);
            IVTRange rangeSumSubject = firstSheet.GetRange(gridFirstRow, startDynamicCol + 1, gridFirstRow, startDynamicCol + 1);
            IVTRange rangeSubjectRow = firstSheet.GetRange(gridFirstRow + 2, startDynamicCol + 1, gridFirstRow + 3, startDynamicCol + 1);
            foreach (ExaminationSubjectBO examSubject in lstExaminationSubject)
            {
                int examSubjectID = examSubject.ExaminationSubjectID;
                tempSheet.CopyPaste(rangeSumSubject, gridFirstRow, startDynamicCol + countSubject);
                tempSheet.CopyPaste(rangeSubjectName, gridFirstRow + 1, startDynamicCol + countSubject);
                tempSheet.SetCellValue(gridFirstRow + 1, startDynamicCol + countSubject, examSubject.DisplayName);
                tempSheet.CopyPaste(rangeSubjectRow, gridFirstRow + 2, startDynamicCol + countSubject);
                tempSheet.SetCellValue(gridFirstRow + 2, startDynamicCol + countSubject, "${list[].Get(ExamSubject" + examSubjectID + ")}");
                tempSheet.SetCellValue(gridFirstRow + 3, startDynamicCol + countSubject, "${list[].Get(ExamSubject" + examSubjectID + ")}");
                countSubject++;
            }
            int lastDynamicCol = startDynamicCol + countSubject - 1;


            tempSheet.GetRange(gridFirstRow, startDynamicCol, gridFirstRow, lastDynamicCol).Merge();
            tempSheet.SetCellValue(gridFirstRow, startDynamicCol, "Điểm thi");
            //Điền cột ghi chú
            IVTRange rangeDescription = firstSheet.GetRange("I" + gridFirstRow, "I" + (gridFirstRow + 3));
            tempSheet.CopyPasteSameSize(rangeDescription, VTVector.ColumnIntToString(lastDynamicCol + 1) + gridFirstRow);
            
            //Danh sách phòng thi
            List<int> lstRoomID = new List<int>();
             // Sử dụng cho trường hợp xuất tất cả
            bool IsExportAllRoom = false;
            bool IsExportAllSubject = false;
            List<int> LstRoomIDForAll = new List<int>();
                Dictionary<string, object> dic3 = new Dictionary<string, object>();
                dic3["AcademicYearID"] = AcademicYearID;
                dic3["ExaminationID"] = ExaminationID;
                dic3["EducationLevelID"] = EducationLevelID;
            if (ExaminationRoomID == 0)  // Chọn tất cả phòng thi
            {
                
                if (exam.UsingSeparateList == 0 && ExaminationSubjectID > 0) // Có chọn môn thi
                {
                    dic3["ExaminationSubjectID"] = ExaminationSubjectID;
                }
                else // Không chọn môn thi - măc định tất cả
                {
                    if (lstExaminationSubject.Count > 0)
                    {
                        IsExportAllRoom = true;
                        dic3["ExaminationSubjectID"] = lstExaminationSubject.First().ExaminationSubjectID;
                    }
                }
                IQueryable<ExaminationRoom> lstExaminationRoom = ExaminationRoomBusiness.SearchBySchool(SchoolID, dic3);
                lstRoomID = lstExaminationRoom.Select(o => o.ExaminationRoomID).Distinct().ToList();

                /* AnhVD9 - Hotfix 0016853
                 * Do thiết kế DB mỗi môn có trên nhiều phòng nên trường hợp Xuất Excel với tất cả môn, phòng không lấy được Dữ liệu
                 * Tạm thời chữa cháy:
                 * Lấy danh sách tên phòng và tìm điểm theo tất cả các tên phòng này (ko sử dụng roomID để tìm điểm nữa do như vậy khi xuất sẽ ko có điểm)
                 */
                if (IsExportAllRoom)
                {
                    List<string> LstExamRoomName = lstExaminationRoom.Select(o => o.RoomTitle).ToList();
                    dic3.Remove("ExaminationSubjectID");
                    LstRoomIDForAll = ExaminationRoomBusiness.SearchBySchool(SchoolID, dic3).Where(o => LstExamRoomName.Contains(o.RoomTitle))
                                        .Select(o => o.ExaminationRoomID).ToList();
                }
                // End AnhVd9
            }
            else
            {
                lstRoomID.Add(ExaminationRoomID);
                // AnhVD9 - Chọn phòng thi nhưng Xuất dữ liệu tất cả các môn( Không có ExaminationSubjectID - Mặc định tất cả các môn)
                if (exam.UsingSeparateList == 0 && ExaminationSubjectID == 0)
                {
                    IsExportAllSubject = true;
                    ExaminationRoom er = ExaminationRoomBusiness.Find(ExaminationRoomID);
                    dic3["RoomTitle"] = er.RoomTitle;
                    LstRoomIDForAll = ExaminationRoomBusiness.SearchBySchool(SchoolID, dic3).Select(o => o.ExaminationRoomID).ToList();
                }
                    
            }
            //Style các dòng
            IVTRange tempRow = tempSheet.GetRange("A" + (gridFirstRow + 2), VTVector.ColumnIntToString(lastDynamicCol + 1) + (gridFirstRow + 2));
            IVTRange tempRowBold = tempSheet.GetRange("A" + (gridFirstRow + 3), VTVector.ColumnIntToString(lastDynamicCol + 1) + (gridFirstRow + 3));
            if (lastDynamicCol < 9)
            {
                lastDynamicCol = 9;
            }

            // Dữ liệu điểm
            Dictionary<string, object> dic2 = new Dictionary<string, object>();
            dic2["ExaminationID"] = ExaminationID;
            dic2["EducationLevelID"] = EducationLevelID;
            dic2["AcademicYearID"] = AcademicYearID;
            dic2["AppliedLevel"] = AppliedLevel;
            dic2["ExaminationSubjectID"] = ExaminationSubjectID;           
            IEnumerable<CandidateMark> lsCandidateMark = CandidateBusiness.SearchMark(SchoolID, AppliedLevel, dic2);

            var lstRooms = this.ExaminationRoomBusiness.All.Where(p => lstRoomID.Contains(p.ExaminationRoomID)).OrderBy(p => p.RoomTitle);
            foreach (ExaminationRoom room in lstRooms)
            {
                IVTWorksheet dataSheet = oBook.CopySheetToLast(tempSheet, VTVector.ColumnIntToString(lastDynamicCol + 1) + (gridFirstRow + 1));
                dataSheet.Name = room.RoomTitle;
                Dictionary<string, object> roomNameDic = new Dictionary<string, object> { { "RoomName", room.RoomTitle.ToUpper() } };
                dataSheet.GetRange("A6", "I6").FillVariableValue(roomNameDic);
                var lsCandidateMarkByClass = lsCandidateMark;
                //AnhVD9 - Lấy danh sách các phòng cùng tên để lấy thông tin điểm
                if (IsExportAllRoom || IsExportAllSubject)
                {
                    lsCandidateMarkByClass = lsCandidateMarkByClass.Where(o => o.RoomID.HasValue && LstRoomIDForAll.Contains(o.RoomID.Value));
                }
                else
                {
                    lsCandidateMarkByClass = lsCandidateMarkByClass.Where(o => o.RoomID.HasValue && o.RoomID == room.ExaminationRoomID);
                }

                List<Object> lstData = new List<object>(); ;
                int countPupil = 0;
                if (lsCandidateMarkByClass != null)
                {
                    // AnhVD9 - Thông tin HS chỉ lấy trong Phòng có ID tương ứng 
                    var lsPupilProfile = lsCandidateMarkByClass.Where(o => o.RoomID.HasValue && o.RoomID == room.ExaminationRoomID)
                        .Select(o => new
                        {
                            o.PupilID,
                            o.PupilCode,
                            o.Name,
                            o.PupilName,
                            o.NamedListCode,
                            o.NamedListNumber
                        }).OrderBy(o => o.NamedListNumber).ThenBy(o => o.Name).ThenBy(o => o.PupilName).ToList();

                    foreach (var pupil in lsPupilProfile)
                    {
                        countPupil++;
                        if (countPupil % 5 == 0 || countPupil == lsPupilProfile.Count)
                        {
                            dataSheet.CopyPasteSameSize(tempRowBold, gridFirstRow + 1 + countPupil, 1);
                        }
                        else
                        {
                            dataSheet.CopyPasteSameSize(tempRow, gridFirstRow + 1 + countPupil, 1);
                        }
                        Dictionary<string, object> markData = new Dictionary<string, object>();
                        markData["Order"] = countPupil;
                        markData["PupilCode"] = pupil.PupilCode;
                        markData["FullName"] = pupil.PupilName;
                        if (exam.UsingSeparateList == 1) //Nếu kì thi có danh sách riêng cho từng môn thì có thêm cột SBD
                        {
                            markData["NamedListCode"] = pupil.NamedListCode;
                        }
                        markData["Description"] = "";
                        foreach (var examSubject in lstExaminationSubject)
                        {
                            CandidateMark mark = lsCandidateMarkByClass.FirstOrDefault(o => o.PupilID == pupil.PupilID && o.SubjectID == examSubject.ExaminationSubjectID);
                            if (mark != null)
                            {
                                markData["ExamSubject" + mark.SubjectID] = mark.RealMark == null ? "" : mark.RealMark.ToString().Replace(".", ",");
                                markData["Description"] += mark.Description;
                                if (mark.Description != null && mark.Description != "")
                                {
                                    markData["Description"] += ";";
                                }
                            }
                        }
                        lstData.Add(markData);
                    }
                }
                dataSheet.GetRange("A" + gridFirstRow, VTVector.ColumnIntToString(lastDynamicCol + 1) + (gridFirstRow + 2 + countPupil)).FillVariableValue(new Dictionary<string, object> { { "list", lstData } });
            }
            firstSheet.Delete();
            tempSheet.Delete();
            return oBook.ToStream();
        }
        //Thống kê chất lượng lớp học
        //PhuongTD
        //20/12/2012
        public Stream CreateReportQualityOfClass(int SchoolID, IDictionary<string, object> dic)
        {
            int AcademicYearID = Utils.GetInt(dic, "AcademicYearID");
            int AppliedLevel = Utils.GetInt(dic, "AppliedLevel");
            int ExaminationID = Utils.GetInt(dic, "ExaminationID");
            int EducationLevelID = Utils.GetInt(dic, "EducationLevelID");
            int ExaminationSubjectID = Utils.GetInt(dic, "ExaminationSubjectID");
            string formular = "";
            string formularAbove = "";
            string reportCode = SystemParamsInFile.REPORT_THONG_KE_CHAT_LUONG_CAC_LOP;
            ReportDefinition reportDef = ReportDefinitionBusiness.GetByCode(reportCode);
            string templatePath = SystemParamsInFile.TEMPLATE_FOLDER + reportDef.TemplateDirectory + "/" + reportDef.TemplateName;
            IVTWorkbook oBook = VTExport.OpenWorkbook(templatePath);
            IVTWorksheet markSheet = oBook.GetSheet(1);
            IVTWorksheet judgeSheet = oBook.GetSheet(2);
            //Tạo template cho môn tính điểm
            int markHeaderEndCol = 17;
            int markHeaderEndRow = 8;
            IVTWorksheet tempMarkSheet = oBook.CopySheetToLast(markSheet, VTVector.ColumnIntToString(markHeaderEndCol) + markHeaderEndRow);
            //Lấy dữ liệu cho header
            Dictionary<string, object> genaralInfo = new Dictionary<string, object>();
            SchoolProfile school = SchoolProfileBusiness.Find(SchoolID);
            Province province = school.Province;
            string schoolName = school.SchoolName.ToUpper();
            string supervisingDeptName = school.SupervisingDept.SupervisingDeptName.ToUpper();
            string provinceName = province.ProvinceName;
            Examination examination = ExaminationBusiness.Find(ExaminationID);
            string examinationName = examination.Title.ToUpper();
            AcademicYear academicYear = AcademicYearBusiness.Find(AcademicYearID);
            string academicYearTitle = academicYear.DisplayTitle;
            genaralInfo["DeptName"] = supervisingDeptName;
            genaralInfo["SchoolName"] = schoolName;
            genaralInfo["ExaminationName"] = examinationName;
            genaralInfo["AcademicYear"] = academicYearTitle;
            genaralInfo["ProvinceName"] = provinceName;
            genaralInfo["ReportDate"] = DateTime.Now;
            //Điền dữ liệu vào chung vào Sheet
            tempMarkSheet.GetRange("A1", VTVector.ColumnIntToString(markHeaderEndCol) + (markHeaderEndRow - 2)).FillVariableValue(genaralInfo);
            int markGridFirstRow = 9;
            int markFirstDynamicCol = 6;
            IVTRange staticCol = markSheet.GetRange("A" + markGridFirstRow, VTVector.ColumnIntToString(markFirstDynamicCol - 1) + (markGridFirstRow + 5));
            tempMarkSheet.CopyPasteSameSize(staticCol, markGridFirstRow, 1);
            //Tạo các cột động
            List<StatisticsConfig> listSCMark = StatisticsConfigBusiness.GetList(
                AppliedLevel,
                SystemParamsInFile.StatisticsConfig.REPORT_TYPE_CHAT_LUONG_CAC_LOP,
                SystemParamsInFile.StatisticsConfig.MARK_TYPE_ALL,
                SystemParamsInFile.StatisticsConfig.SUBJECT_TYPE_MARK);
            IVTRange markDynamicCol = markSheet.GetRange(VTVector.ColumnIntToString(markFirstDynamicCol) + markGridFirstRow, VTVector.ColumnIntToString(markFirstDynamicCol + 1) + (markGridFirstRow + 5));
            int countSc = 0;
            foreach (StatisticsConfig sc in listSCMark)
            {
                tempMarkSheet.CopyPasteSameSize(markDynamicCol, markGridFirstRow, markFirstDynamicCol + countSc * 2);
                tempMarkSheet.SetCellValue(markGridFirstRow, markFirstDynamicCol + countSc * 2, sc.DisplayName);
                tempMarkSheet.SetCellValue(markGridFirstRow + 3, markFirstDynamicCol + countSc * 2, "${list[].Get(StatisticConfig" + sc.StatisticsConfigID + ")}");
                countSc++;
            }

            int marklastCol = markFirstDynamicCol + countSc * 2 + 1;
            //Điền cột trên TB
            IVTRange aboveNormal = markSheet.GetRange("P9", "Q14");
            tempMarkSheet.CopyPasteSameSize(aboveNormal, markGridFirstRow, marklastCol - 1);



            //Lấy các range của tempSheet
            IVTRange topMarkRange = tempMarkSheet.GetRange("A" + (markGridFirstRow + 3), VTVector.ColumnIntToString(marklastCol) + (markGridFirstRow + 3));
            IVTRange midMarkRange = tempMarkSheet.GetRange("A" + (markGridFirstRow + 4), VTVector.ColumnIntToString(marklastCol) + (markGridFirstRow + 4));
            IVTRange lastMarkRange = tempMarkSheet.GetRange("A" + (markGridFirstRow + 5), VTVector.ColumnIntToString(marklastCol) + (markGridFirstRow + 5));
            IVTRange sumMarkRange = tempMarkSheet.GetRange("A" + (markGridFirstRow + 2), VTVector.ColumnIntToString(marklastCol) + (markGridFirstRow + 2));

            //Tạo template cho môn nhận xét
            int judgeHeaderEndCol = 9;
            int judgeHeaderEndRow = 8;
            int judgeFirstDynamicCol = 6;
            int judgeGridFirstRow = 9;
            IVTWorksheet tempJudgeSheet = oBook.CopySheetToLast(judgeSheet, VTVector.ColumnIntToString(judgeHeaderEndCol) + judgeHeaderEndRow);
            //Điền dữ liệu vào chung vào Sheet
            tempJudgeSheet.GetRange("A1", VTVector.ColumnIntToString(judgeHeaderEndCol) + (judgeHeaderEndRow - 2)).FillVariableValue(genaralInfo);
            IVTRange staticColJu = judgeSheet.GetRange("A" + markGridFirstRow, VTVector.ColumnIntToString(markFirstDynamicCol - 1) + (markGridFirstRow + 5));
            tempJudgeSheet.CopyPasteSameSize(staticColJu, markGridFirstRow, 1);
            List<StatisticsConfig> listSCJudge = StatisticsConfigBusiness.GetList(
                AppliedLevel,
                SystemParamsInFile.StatisticsConfig.REPORT_TYPE_CHAT_LUONG_CAC_LOP,
                SystemParamsInFile.StatisticsConfig.MARK_TYPE_ALL,
                SystemParamsInFile.StatisticsConfig.SUBJECT_TYPE_JUDGE);
            IVTRange judgeDynamicCol = judgeSheet.GetRange(VTVector.ColumnIntToString(judgeFirstDynamicCol) + markGridFirstRow, VTVector.ColumnIntToString(judgeFirstDynamicCol + 1) + (markGridFirstRow + 5));
            countSc = 0;
            foreach (StatisticsConfig sc in listSCJudge)
            {
                tempJudgeSheet.CopyPasteSameSize(judgeDynamicCol, judgeGridFirstRow, judgeFirstDynamicCol + countSc * 2);
                tempJudgeSheet.SetCellValue(judgeGridFirstRow, judgeFirstDynamicCol + countSc * 2, sc.DisplayName);
                tempJudgeSheet.SetCellValue(judgeGridFirstRow + 3, judgeFirstDynamicCol + countSc * 2, "${list[].Get(StatisticConfig" + sc.StatisticsConfigID + ")}");
                countSc++;
            }
            int judgelastCol = judgeFirstDynamicCol + countSc * 2 + 1;
            //Lấy các range của tempSheet
            IVTRange topJudgeRange = tempJudgeSheet.GetRange("A" + (judgeGridFirstRow + 3), VTVector.ColumnIntToString(judgelastCol) + (judgeGridFirstRow + 3));
            IVTRange midJudgeRange = tempJudgeSheet.GetRange("A" + (judgeGridFirstRow + 4), VTVector.ColumnIntToString(judgelastCol) + (judgeGridFirstRow + 4));
            IVTRange lastJudgeRange = tempJudgeSheet.GetRange("A" + (judgeGridFirstRow + 5), VTVector.ColumnIntToString(judgelastCol) + (judgeGridFirstRow + 5));
            IVTRange sumJudgeRange = tempJudgeSheet.GetRange("A" + (judgeGridFirstRow + 2), VTVector.ColumnIntToString(judgelastCol) + (judgeGridFirstRow + 2));
            IVTRange personReportRange = tempMarkSheet.GetRange("M16", "Q16");


            //Bắt đầu điền dữ liệu
            Dictionary<string, object> dic2 = new Dictionary<string, object>();
            dic2["ExaminationID"] = ExaminationID;
            dic2["EducationLevelID"] = EducationLevelID;
            dic2["AcademicYearID"] = AcademicYearID;
            dic2["AppliedLevel"] = AppliedLevel;
            dic2["ExaminationSubjectID"] = ExaminationSubjectID;
            dic2["IsAbsence"] = false;
            //Dữ liệu điểm
            IEnumerable<CandidateMark> lsCandidateMark = CandidateBusiness.SearchMark(SchoolID, (int)AppliedLevel, dic2);
            //Danh sách môn thi
            List<ExaminationSubjectBO> lstExaminationSubject = new List<ExaminationSubjectBO>();
            List<int> lstSubjectID = new List<int>();
            if (ExaminationSubjectID == 0)
            {
                Dictionary<string, object> dictionary = new Dictionary<string, object>();
                dictionary["AcademicYearID"] = AcademicYearID;
                dictionary["ExaminationID"] = ExaminationID;
                dictionary["EducationLevelID"] = EducationLevelID;
                dictionary["AppliedLevel"] = AppliedLevel;
                IQueryable<ExaminationSubject> qExaminationSubject = ExaminationSubjectBusiness.SearchBySchool(SchoolID, dictionary);
                lstExaminationSubject = (from es in qExaminationSubject
                                         select new ExaminationSubjectBO()
                                         {
                                             ExaminationSubjectID = es.ExaminationSubjectID,
                                             SubjectID = es.SubjectID,
                                             DisplayName = es.SubjectCat.DisplayName,
                                             OrderInSubject = es.SubjectCat.OrderInSubject
                                         }).OrderBy(o => o.OrderInSubject).ThenBy(o => o.DisplayName).ToList();
            }
            else
            {
                ExaminationSubject examinationSubject = ExaminationSubjectBusiness.Find(ExaminationSubjectID);
                lstExaminationSubject.Add(new ExaminationSubjectBO()
                                         {
                                             ExaminationSubjectID = examinationSubject.ExaminationSubjectID,
                                             SubjectID = examinationSubject.SubjectID,
                                             DisplayName = examinationSubject.SubjectCat.DisplayName
                                         });
            }


            //Danh sách lớp
            Dictionary<string, object> dic3 = new Dictionary<string, object>();
            dic3["AcademicYearID"] = AcademicYearID;
            dic3["ExaminationID"] = ExaminationID;
            dic3["EducationLevelID"] = EducationLevelID;
            IQueryable<Candidate> lsCandidate = CandidateBusiness.SearchBySchool(SchoolID, dic3);
            List<ClassProfile> lstClass = lsCandidate.Select(o => o.ClassProfile).Distinct().OrderBy(o => o.OrderNumber.HasValue ? o.OrderNumber : 0)
                .ThenBy(o => o.DisplayName)
                .ToList();
            //kiem tra ki thi co mon thi cho nhieu khoi hay khong
            Examination examinationObj = ExaminationBusiness.All.Where(p => p.ExaminationID == ExaminationID).FirstOrDefault();
            bool checkExamination = false;
            if (examinationObj != null)
            {
                if (examinationObj.CandidateFromMultipleLevel == 1)
                {
                    checkExamination = true;
                }
            }
            //Danh sách học sinh trong lớp
            IQueryable<PupilOfClass> listPupil = null;
            if (checkExamination)
            {
                // khi khai báo môn thi cho nhiều khối
                listPupil = PupilOfClassBusiness.SearchBySchool(SchoolID, new Dictionary<string, object> 
                {
                    {"AcademicYearID", AcademicYearID}, 
                    {"AppliedLevel", AppliedLevel},
                    {"Status", new List<int>{SystemParamsInFile.PUPIL_STATUS_STUDYING, SystemParamsInFile.PUPIL_STATUS_GRADUATED}}
                });
            }
            else
            {
                    listPupil = PupilOfClassBusiness.SearchBySchool(SchoolID, new Dictionary<string, object> 
            {
                {"AcademicYearID", AcademicYearID}, 
                {"AppliedLevel", AppliedLevel},
                {"EducationLevelID", EducationLevelID},
                {"Status", new List<int>{SystemParamsInFile.PUPIL_STATUS_STUDYING, SystemParamsInFile.PUPIL_STATUS_GRADUATED}}
            });
            }
            List<int> listSubjectID = lstExaminationSubject.Select(o => o.SubjectID).Distinct().ToList();
            Dictionary<int, int?> dicSubjectType = ExaminationSubjectBusiness.GetSubjectMarkType(AcademicYearID, listSubjectID, AppliedLevel);
            foreach (ExaminationSubjectBO examSubjectBO in lstExaminationSubject)
            {
                int examSubjectId = examSubjectBO.ExaminationSubjectID;
                int? isCommentingType = dicSubjectType.ContainsKey(examSubjectBO.SubjectID) ? dicSubjectType[examSubjectBO.SubjectID] : new Nullable<int>();
                // Neu khong co thong tin ve kieu mon thi bo qua
                if (!isCommentingType.HasValue)
                {
                    continue;
                }
                if (isCommentingType.Value != SystemParamsInFile.ISCOMMENTING_TYPE_JUDGE) //Nếu là môn tính điểm
                {
                    IVTWorksheet dataSheet = oBook.CopySheetToLast(tempMarkSheet, VTVector.ColumnIntToString(marklastCol) + (markGridFirstRow + 1));

                    dataSheet.Name = examSubjectBO.DisplayName;

                    if (examSubjectBO.DisplayName == "Toán" || examSubjectBO.DisplayName == "Toan")
                    {
                        dataSheet.Name = "Toan ";
                    }

                    dataSheet.CopyPasteSameSize(sumMarkRange, markGridFirstRow + 2, 1);
                    dataSheet.GetRange("A7", "R7").FillVariableValue(new Dictionary<string, object> { { "SubjectName", examSubjectBO.DisplayName.ToUpper() } });
                    int countClass = 0;
                    List<Object> listData = new List<object>();
                    int countAllClass = lstClass.Count;
                    foreach (ClassProfile classProfile in lstClass)
                    {
                        int classID = classProfile.ClassProfileID;
                        Dictionary<string, object> data = new Dictionary<string, object>();
                        countClass++;
                        if (countClass == 1)
                        {
                            dataSheet.CopyPasteSameSize(topMarkRange, markGridFirstRow + 2 + countClass, 1);
                        }
                        else if (countClass == countAllClass)
                        {
                            dataSheet.CopyPasteSameSize(lastMarkRange, markGridFirstRow + 2 + countClass, 1);
                        }
                        else
                        {
                            dataSheet.CopyPasteSameSize(midMarkRange, markGridFirstRow + 2 + countClass, 1);
                        }
                        data["Order"] = countClass;
                        data["ClassName"] = classProfile.DisplayName;
                        Employee emp = EmployeeBusiness.Find(classProfile.HeadTeacherID);
                        data["HeadTeacherName"] = emp != null ? emp.FullName : string.Empty;
                        data["NumberOfPupil"] = (from p in listPupil
                                                 where
                                                     p.ClassID == classID
                                                 select p.PupilID).Count();
                        data["NumberOfTest"] = (from c in lsCandidateMark
                                                where
                                                     c.ClassID == classID
                                                     && c.SubjectID == examSubjectId
                                                select c.CandidateID).Count();
                        countSc = 0;
                        formularAbove = "=SUM(";
                        foreach (StatisticsConfig sc in listSCMark)
                        {
                            var listCandidate = (from c in lsCandidateMark.ToList()
                                                 where c.ClassID == classID
                                                         && c.SubjectID == examSubjectId
                                                 select c);
                            int cnt = 0;

                            foreach (var obj in listCandidate)
                            {
                                if (obj.RealMark != "")
                                {
                                    if (Convert.ToDouble(obj.RealMark.Replace(".", ",")) <= Convert.ToDouble(sc.MaxValue))
                                    {
                                        if (Convert.ToDouble(obj.RealMark.Replace(".", ",")) >= Convert.ToDouble(sc.MinValue))
                                        {
                                            cnt++;
                                        }
                                    }
                                }

                            }

                            data["StatisticConfig" + sc.StatisticsConfigID] = cnt;
                            formular = "=IF(E" + (markGridFirstRow + 2 + countClass) + "=0;0;ROUND(" + VTVector.ColumnIntToString(markFirstDynamicCol + countSc * 2) + (markGridFirstRow + 2 + countClass) + "/E" + (markGridFirstRow + 2 + countClass) + ";4)*100)";
                            dataSheet.SetFormulaValue(markGridFirstRow + 2 + countClass, markFirstDynamicCol + 1 + countSc * 2, formular);
                            if (sc.MarkType == SystemParamsInFile.StatisticsConfig.MARK_TYPE_ABOVE_AVERAGE)
                            {
                                formularAbove += VTVector.ColumnIntToString(markFirstDynamicCol + countSc * 2) + (markGridFirstRow + 2 + countClass) + ";";
                            }
                            countSc++;
                        }
                        formularAbove.Remove(formularAbove.Length - 1);
                        formularAbove += ")";
                        dataSheet.SetFormulaValue(markGridFirstRow + 2 + countClass, marklastCol - 1, formularAbove);
                        formular = "=IF(E" + (markGridFirstRow + 2 + countClass) + "=0;0;ROUND(" + VTVector.ColumnIntToString(marklastCol - 1) + (markGridFirstRow + 2 + countClass) + "/E" + (markGridFirstRow + 2 + countClass) + ";4)*100)";
                        dataSheet.SetFormulaValue(VTVector.ColumnIntToString(marklastCol) + (markGridFirstRow + 2 + countClass), formular);
                        listData.Add(data);
                    }
                    dataSheet.GetRange(markGridFirstRow, 1, markGridFirstRow + countClass + 3, marklastCol).FillVariableValue(new Dictionary<string, object> { { "list", listData } });
                    //Điền cột tổng số
                    formular = "=SUM(D" + (markGridFirstRow + 3) + ":D" + (markGridFirstRow + countAllClass + 2) + ")";
                    dataSheet.SetFormulaValue("D" + (markGridFirstRow + 2), formular);
                    formular = "=SUM(E" + (markGridFirstRow + 3) + ":E" + (markGridFirstRow + countAllClass + 2) + ")";
                    dataSheet.SetFormulaValue("E" + (markGridFirstRow + 2), formular);
                    countSc = 0;
                    formularAbove = "=SUM(";
                    foreach (StatisticsConfig sc in listSCMark)
                    {
                        countSc++;
                        formular = "=SUM(" + VTVector.ColumnIntToString(markFirstDynamicCol + (countSc - 1) * 2) + (markGridFirstRow + 3) + ":" + VTVector.ColumnIntToString(markFirstDynamicCol + (countSc - 1) * 2) + (markGridFirstRow + countAllClass + 2) + ")";
                        dataSheet.SetFormulaValue(VTVector.ColumnIntToString(markFirstDynamicCol + (countSc - 1) * 2) + (markGridFirstRow + 2), formular);
                        formular = "=IF(E" + (markGridFirstRow + 2) + "=0;0;ROUND(" + VTVector.ColumnIntToString(markFirstDynamicCol + (countSc - 1) * 2) + (markGridFirstRow + 2) + "/E" + (markGridFirstRow + 2) + ";4)*100)";
                        dataSheet.SetFormulaValue(VTVector.ColumnIntToString(markFirstDynamicCol + (countSc - 1) * 2 + 1) + (markGridFirstRow + 2), formular);
                        if (sc.MarkType == SystemParamsInFile.StatisticsConfig.MARK_TYPE_ABOVE_AVERAGE)
                        {
                            formularAbove += VTVector.ColumnIntToString(markFirstDynamicCol + (countSc - 1) * 2) + (markGridFirstRow + 2) + ";";
                        }
                    }
                    formularAbove.Remove(formularAbove.Length - 1);
                    formularAbove += ")";
                    dataSheet.SetFormulaValue(markGridFirstRow + 2, marklastCol - 1, formularAbove);
                    formular = "=IF(E" + (markGridFirstRow + 2) + "=0;0;ROUND(" + VTVector.ColumnIntToString(marklastCol - 1) + (markGridFirstRow + 2) + "/E" + (markGridFirstRow + 2) + ";4)*100)";
                    dataSheet.SetFormulaValue(VTVector.ColumnIntToString(marklastCol) + (markGridFirstRow + 2), formular);
                    //Hungnd 17/04/2013 Fix bug 167247
                    dataSheet.SetCellValue("N" + (countAllClass + 14), "Người lập báo cáo");
                    dataSheet.CopyPaste(personReportRange, countAllClass + 14, 13, true);
                }
                else if (isCommentingType.Value == SystemParamsInFile.ISCOMMENTING_TYPE_JUDGE)
                {
                    ExaminationSubject examSubject = ExaminationSubjectBusiness.Find(examSubjectId);
                    IVTWorksheet dataSheet = oBook.CopySheetToLast(tempJudgeSheet, VTVector.ColumnIntToString(judgelastCol) + (judgeGridFirstRow + 1));
                    dataSheet.Name = examSubject.SubjectCat.DisplayName.Replace(":", " "); //Name sheet trog excel không cho phép kí tự :
                    dataSheet.CopyPasteSameSize(sumJudgeRange, judgeGridFirstRow + 2, 1);
                    dataSheet.GetRange("A7", "R7").FillVariableValue(new Dictionary<string, object> { { "SubjectName", examSubject.SubjectCat.DisplayName.ToUpper() } });
                    int countClass = 0;
                    List<Object> listData = new List<object>();
                    int countAllClass = lstClass.Count;
                    foreach (ClassProfile classProfile in lstClass)
                    {
                        int classID = classProfile.ClassProfileID;
                        Dictionary<string, object> data = new Dictionary<string, object>();
                        countClass++;
                        if (countClass == 1)
                        {
                            dataSheet.CopyPasteSameSize(topJudgeRange, judgeGridFirstRow + 2 + countClass, 1);
                        }
                        else if (countClass == countAllClass)
                        {
                            dataSheet.CopyPasteSameSize(lastJudgeRange, judgeGridFirstRow + 2 + countClass, 1);
                        }
                        else
                        {
                            dataSheet.CopyPasteSameSize(midJudgeRange, judgeGridFirstRow + 2 + countClass, 1);
                        }
                        data["Order"] = countClass;
                        data["ClassName"] = classProfile.DisplayName;
                        Employee emp = EmployeeBusiness.Find(classProfile.HeadTeacherID);
                        if (emp != null)
                        {
                            data["HeadTeacherName"] = (!string.IsNullOrEmpty(emp.FullName) ? emp.FullName : string.Empty);
                        }
                        else
                        {
                            data["HeadTeacherName"] = string.Empty;
                        }
                        data["NumberOfPupil"] = (from p in listPupil
                                                 where
                                                     p.ClassID == classID
                                                 select p.PupilID).Count();
                        data["NumberOfTest"] = (from c in lsCandidateMark
                                                where
                                                     c.ClassID == classID
                                                     && c.SubjectID == examSubjectId
                                                select c.CandidateID).Count();
                        countSc = 0;
                        foreach (StatisticsConfig sc in listSCJudge)
                        {
                            int cnt = (from c in lsCandidateMark.ToList()
                                       where c.ClassID == classID
                                               && c.SubjectID == examSubjectId
                                               && (c.RealMark != null && c.RealMark.Trim() == sc.EqualValue)
                                       select c).Count();

                            data["StatisticConfig" + sc.StatisticsConfigID] = cnt;
                            formular = "=IF(E" + (markGridFirstRow + 2 + countClass) + "=0;0;ROUND(" + VTVector.ColumnIntToString(markFirstDynamicCol + countSc * 2) + (markGridFirstRow + 2 + countClass) + "/E" + (markGridFirstRow + 2 + countClass) + ";4)*100)";
                            dataSheet.SetFormulaValue(markGridFirstRow + 2 + countClass, markFirstDynamicCol + 1 + countSc * 2, formular);
                            countSc++;
                        }
                        listData.Add(data);
                    }
                    dataSheet.GetRange(markGridFirstRow, 1, markGridFirstRow + countClass + 3, marklastCol).FillVariableValue(new Dictionary<string, object> { { "list", listData } });

                    //Điền cột tổng số
                    formular = "=SUM(D" + (markGridFirstRow + 3) + ":D" + (markGridFirstRow + countAllClass + 2) + ")";
                    dataSheet.SetFormulaValue("D" + (markGridFirstRow + 2), formular);
                    formular = "=SUM(E" + (markGridFirstRow + 3) + ":E" + (markGridFirstRow + countAllClass + 2) + ")";
                    dataSheet.SetFormulaValue("E" + (markGridFirstRow + 2), formular);
                    countSc = 0;
                    if (isCommentingType != SystemParamsInFile.ISCOMMENTING_TYPE_JUDGE)
                    {
                        foreach (StatisticsConfig sc in listSCMark)
                        {
                            countSc++;
                            formular = "=SUM(" + VTVector.ColumnIntToString(markFirstDynamicCol + (countSc - 1) * 2) + (markGridFirstRow + 3) + ":" + VTVector.ColumnIntToString(markFirstDynamicCol + (countSc - 1) * 2) + (markGridFirstRow + countAllClass + 2) + ")";
                            dataSheet.SetFormulaValue(VTVector.ColumnIntToString(markFirstDynamicCol + (countSc - 1) * 2) + (markGridFirstRow + 2), formular);
                            formular = "=IF(E" + (markGridFirstRow + 2) + "=0;0;ROUND(" + VTVector.ColumnIntToString(markFirstDynamicCol + (countSc - 1) * 2) + (markGridFirstRow + 2) + "/E" + (markGridFirstRow + 2) + ";4)*100)";
                            dataSheet.SetFormulaValue(VTVector.ColumnIntToString(markFirstDynamicCol + (countSc - 1) * 2 + 1) + (markGridFirstRow + 2), formular);

                        }
                    }
                    else if (isCommentingType == SystemParamsInFile.ISCOMMENTING_TYPE_JUDGE)
                    {
                        foreach (StatisticsConfig sc in listSCJudge)
                        {
                            countSc++;
                            formular = "=SUM(" + VTVector.ColumnIntToString(judgeFirstDynamicCol + (countSc - 1) * 2) + (judgeGridFirstRow + 3) + ":" + VTVector.ColumnIntToString(judgeFirstDynamicCol + (countSc - 1) * 2) + (judgeGridFirstRow + countAllClass + 2) + ")";
                            dataSheet.SetFormulaValue(VTVector.ColumnIntToString(judgeFirstDynamicCol + (countSc - 1) * 2) + (judgeGridFirstRow + 2), formular);
                            formular = "=IF(E" + (markGridFirstRow + 2) + "=0;0;ROUND(" + VTVector.ColumnIntToString(judgeFirstDynamicCol + (countSc - 1) * 2) + (judgeGridFirstRow + 2) + "/E" + (judgeGridFirstRow + 2) + ";4)*100)";
                            dataSheet.SetFormulaValue(VTVector.ColumnIntToString(judgeFirstDynamicCol + (countSc - 1) * 2 + 1) + (judgeGridFirstRow + 2), formular);

                        }
                    }

                    //Hungnd 17/04/2013 Fix bug 167247
                    dataSheet.SetCellValue("H" + (countAllClass + 14), "Người lập báo cáo");
                    dataSheet.CopyPaste(personReportRange, countAllClass + 14, 8, true);
                }
            }
            markSheet.Delete();
            judgeSheet.Delete();
            tempMarkSheet.Delete();
            tempJudgeSheet.Delete();
            return oBook.ToStream();
        }
        //Thống kê chất lượng môn thi
        //PhuongTD
        //20/12/2012
        public Stream CreateReportQualityOfSubject(int SchoolID, IDictionary<string, object> dic)
        {
            int AcademicYearID = Utils.GetInt(dic, "AcademicYearID");
            int AppliedLevel = Utils.GetInt(dic, "AppliedLevel");
            int ExaminationID = Utils.GetInt(dic, "ExaminationID");
            int EducationLevelID = Utils.GetInt(dic, "EducationLevelID");
            int ClassID = Utils.GetInt(dic, "ClassID");
            int ExaminationSubjectID = 0;
            string reportCode = SystemParamsInFile.REPORT_THONG_KE_CHAT_LUONG_MON_THI;
            ReportDefinition reportDef = ReportDefinitionBusiness.GetByCode(reportCode);
            string templatePath = SystemParamsInFile.TEMPLATE_FOLDER + reportDef.TemplateDirectory + "/" + reportDef.TemplateName;
            IVTWorkbook oBook = VTExport.OpenWorkbook(templatePath);
            IVTWorksheet firstSheet = oBook.GetSheet(1);
            int headerEndRow = 7;
            int headerEndCol = 17; //"Q"
            //Tạo template
            IVTWorksheet tempSheet = oBook.CopySheetToLast(firstSheet, VTVector.ColumnIntToString(headerEndCol) + headerEndRow);
            int gridFirstRow = headerEndRow + 1;
            int startDynamicCol = 4;
            //Điền dữ liệu chung cho template
            Dictionary<string, object> genaralInfo = new Dictionary<string, object>();
            SchoolProfile school = SchoolProfileBusiness.Find(SchoolID);
            Province province = school.Province;
            string schoolName = school.SchoolName.ToUpper();
            string supervisingDeptName = school.SupervisingDept.SupervisingDeptName.ToUpper();
            string provinceName = province.ProvinceName;
            Examination examination = ExaminationBusiness.Find(ExaminationID);
            string examinationName = examination.Title.ToUpper();
            AcademicYear academicYear = AcademicYearBusiness.Find(AcademicYearID);
            string academicYearTitle = academicYear.DisplayTitle;
            genaralInfo["DeptName"] = supervisingDeptName;
            genaralInfo["SchoolName"] = schoolName;
            genaralInfo["ExaminationName"] = examinationName;
            genaralInfo["AcademicYear"] = academicYearTitle;
            genaralInfo["ProvinceName"] = provinceName;
            genaralInfo["ReportDate"] = DateTime.Now;
            tempSheet.GetRange("A1", VTVector.ColumnIntToString(headerEndCol) + (headerEndRow - 2)).FillVariableValue(genaralInfo);

            //Danh sách môn thi
            List<ExaminationSubjectBO> lstExaminationSubject = new List<ExaminationSubjectBO>();
            if (ExaminationSubjectID == 0)
            {
                Dictionary<string, object> dictionary = new Dictionary<string, object>();
                dictionary["AcademicYearID"] = AcademicYearID;
                dictionary["ExaminationID"] = ExaminationID;
                dictionary["EducationLevelID"] = EducationLevelID;
                dictionary["AppliedLevel"] = AppliedLevel;
                IQueryable<ExaminationSubject> qExaminationSubject = ExaminationSubjectBusiness.SearchBySchool(SchoolID, dictionary);
                lstExaminationSubject = (from es in qExaminationSubject
                                         select new ExaminationSubjectBO()
                                         {
                                             ExaminationSubjectID = es.ExaminationSubjectID,
                                             SubjectID = es.SubjectID,
                                             DisplayName = es.SubjectCat.DisplayName,
                                             OrderInSubject = es.SubjectCat.OrderInSubject
                                         }).OrderBy(o => o.OrderInSubject).ThenBy(o => o.DisplayName).ToList();
            }
            else
            {
                ExaminationSubject examinationSubject = ExaminationSubjectBusiness.Find(ExaminationSubjectID);
                lstExaminationSubject.Add(new ExaminationSubjectBO()
                {
                    ExaminationSubjectID = examinationSubject.ExaminationSubjectID,
                    SubjectID = examinationSubject.SubjectID,
                    DisplayName = examinationSubject.SubjectCat.DisplayName
                });
            }
            //Copy các cột tĩnh sang tempSheet
            IVTRange staticGrid = firstSheet.GetRange(gridFirstRow, 1, gridFirstRow + 3, startDynamicCol - 1);
            tempSheet.CopyPasteSameSize(staticGrid, gridFirstRow, 1);
            //Tạo các cột động
            List<StatisticsConfig> listSC = StatisticsConfigBusiness.GetList(
                AppliedLevel,
                SystemParamsInFile.StatisticsConfig.REPORT_TYPE_CHAT_LUONG_MON_THI,
                SystemParamsInFile.StatisticsConfig.MARK_TYPE_ALL,
                SystemParamsInFile.StatisticsConfig.SUBJECT_TYPE_ALL).OrderBy(o => o.StatisticsConfigID).ToList();
            IVTRange rangeDynamicCol = firstSheet.GetRange(gridFirstRow, startDynamicCol, gridFirstRow + 2, startDynamicCol + 1);
            int countSc = 0;
            foreach (StatisticsConfig sc in listSC)
            {
                tempSheet.CopyPaste(rangeDynamicCol, gridFirstRow, startDynamicCol + countSc * 2);
                tempSheet.SetCellValue(gridFirstRow, startDynamicCol + countSc * 2, sc.DisplayName);
                tempSheet.SetCellValue(gridFirstRow + 2, startDynamicCol + countSc * 2, "${list[].Get(StatisticConfig" + sc.StatisticsConfigID + ")}");
                countSc++;
            }
            int lastDynamicCol = startDynamicCol + countSc * 2 - 1;

            //Dữ liệu điểm
            Dictionary<string, object> dic2 = new Dictionary<string, object>();
            dic2["ExaminationID"] = ExaminationID;
            dic2["EducationLevelID"] = EducationLevelID;
            dic2["AcademicYearID"] = AcademicYearID;
            dic2["AppliedLevel"] = AppliedLevel;
            dic2["IsAbsence"] = false;
            IEnumerable<CandidateMark> lsCandidateMark = CandidateBusiness.SearchMark(SchoolID, (int)AppliedLevel, dic2);
            if (ClassID != 0)
            {
                lsCandidateMark = lsCandidateMark.Where(o => o.ClassID == ClassID);
            }
            //Style các dòng
            IVTRange tempRow = tempSheet.GetRange("A" + (gridFirstRow + 2), VTVector.ColumnIntToString(lastDynamicCol) + (gridFirstRow + 2));
            IVTWorksheet dataSheet = oBook.CopySheetToLast(tempSheet, VTVector.ColumnIntToString(lastDynamicCol) + (gridFirstRow + 1));
            List<Object> lstData = new List<object>();
            int countSubject = 0;
            string formular = "";
            // Lay danh sach mon hoc cua truong
            List<SchoolSubject> listSs = SchoolSubjectBusiness.SearchBySchool(SchoolID, new Dictionary<string, object>(){
            {"AcademicYearID", AcademicYearID},
            {"EducationLevelID", EducationLevelID},
            {"AppliedLevel", AppliedLevel}
            }).ToList();
            foreach (ExaminationSubjectBO examSubject in lstExaminationSubject)
            {
                int examSubjectID = examSubject.ExaminationSubjectID;
                dataSheet.CopyPasteSameSize(tempRow, gridFirstRow + 2 + countSubject, 1);
                countSubject++;
                Dictionary<string, object> markData = new Dictionary<string, object>();
                markData["Order"] = countSubject;
                markData["SubjectName"] = examSubject.DisplayName;
                IEnumerable<CandidateMark> lsCandidateMarkBySubject = lsCandidateMark.Where(o => o.SubjectID == examSubjectID);
                int NumberOfPupil = lsCandidateMarkBySubject.Count();
                markData["NumberOfPupil"] = NumberOfPupil;
                countSc = 0;

                SchoolSubject ss = listSs.Where(o => o.SubjectID == examSubject.SubjectID).FirstOrDefault();

                foreach (StatisticsConfig sc in listSC)
                {
                    if (ss != null && ss.IsCommenting != SystemParamsInFile.ISCOMMENTING_TYPE_JUDGE && sc.SubjectType == SystemParamsInFile.StatisticsConfig.SUBJECT_TYPE_MARK)
                    {
                        int cnt = 0;

                        foreach (var obj in lsCandidateMarkBySubject)
                        {
                            if (!string.IsNullOrEmpty(obj.RealMark) && Convert.ToDouble(obj.RealMark.Replace(".", ",")) <= Convert.ToDouble(sc.MaxValue) && Convert.ToDouble(obj.RealMark.Replace(".", ",")) >= Convert.ToDouble(sc.MinValue))
                                cnt++;
                        }
                        markData["StatisticConfig" + sc.StatisticsConfigID] = cnt;
                    }
                    else
                    {
                        int cnt = (from c in lsCandidateMarkBySubject where c.RealMark != null && c.RealMark.Trim() == sc.EqualValue select c).Count();
                        markData["StatisticConfig" + sc.StatisticsConfigID] = cnt;
                    }
                    formular = "=IF(C" + (gridFirstRow + 1 + countSubject) + "=0;0;ROUND(" + VTVector.ColumnIntToString(startDynamicCol + countSc * 2) + (gridFirstRow + 1 + countSubject) + "/C" + (gridFirstRow + 1 + countSubject) + ";4)*100)";
                    dataSheet.SetFormulaValue(gridFirstRow + 1 + countSubject, startDynamicCol + countSc * 2 + 1, formular);
                    countSc++;
                }
                lstData.Add(markData);
            }
            dataSheet.GetRange("A" + gridFirstRow, VTVector.ColumnIntToString(lastDynamicCol) + (gridFirstRow + 2 + countSubject))
                     .FillVariableValue(new Dictionary<string, object> { { "list", lstData } });
            IVTRange createUser = firstSheet.GetRange("A12", "Q12");
            dataSheet.CopyPasteSameSize(createUser, gridFirstRow + 4 + countSubject, 1);
            firstSheet.Delete();
            tempSheet.Delete();
            return oBook.ToStream();
        }
    }
}
