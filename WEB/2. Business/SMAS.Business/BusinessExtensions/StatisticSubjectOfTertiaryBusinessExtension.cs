﻿/**
 * The Viettel License
 *
 * Copyright 2012 Viettel Telecom. All rights reserved.
 * VIETTEL PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */

/**
* @author
* @version $Revision: $
*/

using System.Collections.Generic;
using System.Linq;
using SMAS.Business.Common;
using SMAS.DAL.Repository;
using SMAS.Models.Models;

namespace SMAS.Business.Business
{
    public partial class StatisticSubjectOfTertiaryBusiness
    {
        /// <summary>
        /// Search
        /// </summary>
        /// <param name="dic">The dic.</param>
        /// <returns></returns>
        /// <author>
        /// dungnt77
        /// </author>
        /// <remarks>
        /// 17/01/2013   10:01 AM
        /// </remarks>
        public IQueryable<StatisticSubjectOfTertiary> Search(IDictionary<string, object> dic)
        {
            int SchoolID = Utils.GetInt(dic, "SchoolID");
            int AcademicYearID = Utils.GetInt(dic, "AcademicYearID");
            int SubjectID = Utils.GetInt(dic, "SubjectID");
            int SpecicalType = Utils.GetInt(dic, "SpecicalType");
            int Year = Utils.GetInt(dic, "Year");
            var lsStatisticSubjectOfTertiary = StatisticSubjectOfTertiaryRepository.All;
            if (SchoolID != 0)
            {
                lsStatisticSubjectOfTertiary = lsStatisticSubjectOfTertiary.Where(o => o.SchoolID == SchoolID);
            }

            if (AcademicYearID != 0)
            {
                lsStatisticSubjectOfTertiary = lsStatisticSubjectOfTertiary.Where(o => o.AcademicYearID == AcademicYearID);
            }

            if (SubjectID != 0)
            {
                lsStatisticSubjectOfTertiary = lsStatisticSubjectOfTertiary.Where(o => o.SubjectID == SubjectID);
            }

            if (SpecicalType != 0)
            {
                lsStatisticSubjectOfTertiary = lsStatisticSubjectOfTertiary.Where(o => o.SpecicalType == SpecicalType);
            }

            if (Year != 0)
            {
                lsStatisticSubjectOfTertiary = lsStatisticSubjectOfTertiary.Where(o => o.AcademicYear.Year == Year);
            }

            return lsStatisticSubjectOfTertiary;
        }

        /// <summary>
        /// SearchBySchool
        /// </summary>
        /// <param name="SchoolID">The school ID.</param>
        /// <param name="dic">The dic.</param>
        /// <returns></returns>
        /// <author>
        /// dungnt77
        /// </author>
        /// <remarks>
        /// 17/01/2013   10:01 AM
        /// </remarks>
        public IQueryable<StatisticSubjectOfTertiary> SearchBySchool(int SchoolID, IDictionary<string, object> dic = null)
        {
            if (SchoolID == 0)
            {
                return null;
            }
            if (dic == null)
            {
                dic = new Dictionary<string, object>() { };
            }

            dic["SchoolID"] = SchoolID;
            return Search(dic);
        }

        /// <summary>
        /// InsertOrUpdate
        /// </summary>
        /// <param name="SchoolID">The school ID.</param>
        /// <param name="AcademicYearID">The academic year ID.</param>
        /// <param name="lstStatisticSubject">The LST statistic subject.</param>
        ///<author>hath</author>
        /// <date>1/18/2013</date>
        public void InsertOrUpdate(int SchoolID, int AcademicYearID, List<StatisticSubjectOfTertiary> lstStatisticSubject)
        { 
            //SubjectID: FK(SubjectCat)
            //TotalTeacher in (0, 10000)
            //    Tìm kiếm trong bảng StatisticSubjectOfTertiary theo SchooID và AcademicYearID. Xoá hết các bản ghi tìm được.
            //    Cập nhật SchoolID, AcademicYearID vào trong lstStatisticSubject
            //    Insert lstStatisticSubject vào bảng StatisticSubjectOfTertiary
            IQueryable<StatisticSubjectOfTertiary> lsStatisticSubjectOfTertiary = StatisticSubjectOfTertiaryRepository.All.Where(o => o.SchoolID == SchoolID && o.AcademicYearID == AcademicYearID);
            if (lsStatisticSubjectOfTertiary.Count() > 0)
            {
                List<StatisticSubjectOfTertiary> lstStatisticSubjectOfTertiary = lsStatisticSubjectOfTertiary.ToList();
                base.DeleteAll(lstStatisticSubjectOfTertiary);
            }
            foreach(StatisticSubjectOfTertiary ssot in lstStatisticSubject)
            {
                if (ssot.SubjectID != 0)
                {
                    SubjectCatBusiness.CheckAvailable(ssot.SubjectID, "SubjectCat_Label_SubjectCatID", true);
                }
                if (ssot.TotalTeacher != null)
                {
                    Utils.ValidateRange(ssot.TotalTeacher.Value, 0, 1000, "StatisticSubjectOfTertiary_Label_TotalTeacher");
                }
                ssot.SchoolID = SchoolID;
                ssot.AcademicYearID = AcademicYearID;
                base.Insert(ssot);
            }

        }
    }
}