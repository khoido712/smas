/**
 * The Viettel License
 *
 * Copyright 2012 Viettel Telecom. All rights reserved.
 * VIETTEL PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */

/** 
* @author  
* @version $Revision: $
*/

using System;
using log4net;
using System.Web;
using System.Data;
using System.Linq;
using System.Data.Entity;
using SMAS.Models.Models;
using SMAS.DAL.Repository;
using SMAS.DAL.IRepository;
using SMAS.Business.Common;
using System.Linq.Expressions;
using SMAS.Business.IBusiness;
using System.Collections.Generic;
using SMAS.Business.BusinessObject;
using SMAS.VTUtils.Log;

namespace SMAS.Business.Business
{
    public partial class ExamInputMarkAssignedBusiness
    {

        public IQueryable<ExamInputMarkAssignedBO> Search(IDictionary<string, object> dic)
        {
            long examinationsID = Utils.GetInt(dic, "ExaminationsID");
            long examGroupID = Utils.GetInt(dic, "ExamGroupID");
            int subjectID = Utils.GetInt(dic, "SubjectID");
            int teacherID = Utils.GetInt(dic, "TeacherID");

            IQueryable<ExamInputMarkAssignedBO> query = from eia in ExamInputMarkAssignedRepository.All
                                                        join ee in EmployeeRepository.All on eia.TeacherID equals ee.EmployeeID
                                                        join sc in SubjectCatRepository.All on eia.SubjectID equals sc.SubjectCatID
                                                        join sf in SchoolFacultyRepository.All on eia.SchooFacultyID equals sf.SchoolFacultyID
                                                        join et in EthnicRepository.All on ee.EthnicID equals et.EthnicID into des
                                                        from x in des.DefaultIfEmpty()
                                                        where ee.IsActive == true
                                                        select new ExamInputMarkAssignedBO
                                                         {
                                                             EmployeeName = ee.FullName,
                                                             EmployeeCode = ee.EmployeeCode,
                                                             ExamGroupID = eia.ExamGroupID,
                                                             ExaminationsID = eia.ExaminationsID,
                                                             ExamInputMarkAssignedID = eia.ExamInputMarkAssignedID,
                                                             SchoolFacultyID = eia.SchooFacultyID,
                                                             SchoolFacultyName = sf.FacultyName,
                                                             SubjectID = eia.SubjectID,
                                                             SubjectName = sc.SubjectName,
                                                             TeacherID = eia.TeacherID,
                                                             EthnicCode = x != null ? x.EthnicCode : null,
                                                             ExamRoomID = eia.ExamRoomID
                                                         };


            if (examinationsID != 0)
            {
                query = query.Where(o => o.ExaminationsID == examinationsID);
            }
            if (examGroupID != 0)
            {
                query = query.Where(o => o.ExamGroupID == examGroupID);
            }
            if (subjectID != 0)
            {
                query = query.Where(o => o.SubjectID == subjectID);
            }
            if (teacherID != 0)
            {
                query = query.Where(o => o.TeacherID == teacherID);
            }

            return query;
        }

        /// <summary>
        /// Insert list su dung Bulk insert
        /// </summary>
        /// <param name="insertList"></param>
        public void InsertList(List<ExamInputMarkAssigned> insertList)
        {
            try
            {
                context.Configuration.AutoDetectChangesEnabled = false;
                context.Configuration.ValidateOnSaveEnabled = false;
                if (insertList == null || insertList.Count == 0)
                {
                    return;
                }
                for (int i = 0; i < insertList.Count; i++)
                {
                    ExamInputMarkAssignedBusiness.Insert(insertList[i]);
                }
                ExamInputMarkAssignedBusiness.Save();
            }
            catch (Exception ex)
            {
                
                LogExtensions.ErrorExt(logger, DateTime.Now, "InsertList", "null", ex);
            }
            finally
            {
                context.Configuration.AutoDetectChangesEnabled = true;
                context.Configuration.ValidateOnSaveEnabled = true;
            }

        }

        /// <summary>
        /// Update list su dung Bulk insert
        /// </summary>
        /// <param name="insertList"></param>
        public void UpdateList(List<ExamInputMarkAssigned> updateList)
        {
            try
            {
                context.Configuration.AutoDetectChangesEnabled = false;
                context.Configuration.ValidateOnSaveEnabled = false;
                if (updateList == null || updateList.Count == 0)
                {
                    return;
                }
                for (int i = 0; i < updateList.Count; i++)
                {
                    ExamInputMarkAssignedBusiness.Update(updateList[i]);
                }
                ExamInputMarkAssignedBusiness.Save();
            }
            catch (Exception ex)
            {
                
                LogExtensions.ErrorExt(logger, DateTime.Now, "UpdateList", "null", ex);
            }
            finally
            {
                context.Configuration.AutoDetectChangesEnabled = true;
                context.Configuration.ValidateOnSaveEnabled = true;
            }

        }

        /// <summary>
        /// Delete list su dung Bulk delete
        /// </summary>
        /// <param name="insertList"></param>
        public void DeleteList(List<ExamInputMarkAssigned> deleteList)
        {
            try
            {
                context.Configuration.AutoDetectChangesEnabled = false;
                context.Configuration.ValidateOnSaveEnabled = false;
                if (deleteList == null || deleteList.Count == 0)
                {
                    return;
                }
                ExamInputMarkAssignedBusiness.DeleteAll(deleteList);
                ExamInputMarkAssignedBusiness.Save();
            }
            catch (Exception ex)
            {
                
                LogExtensions.ErrorExt(logger, DateTime.Now, "DeleteList", "null", ex);
            }
            finally
            {
                context.Configuration.AutoDetectChangesEnabled = true;
                context.Configuration.ValidateOnSaveEnabled = true;
            }
        }
        public IQueryable<ExamInputMarkAssigned> SearchAssigned(IDictionary<string, object> search)
        {
            long examInputMarkAssignedID = Utils.GetLong(search, "ExamInputMarkAssignedID");
            long examinationsID = Utils.GetLong(search, "ExaminationsID");
            long examGroupID = Utils.GetLong(search, "ExamGroupID");
            int subjectID = Utils.GetInt(search, "SubjectID");
            int teacherID = Utils.GetInt(search, "TeacherID");

            IQueryable<ExamInputMarkAssigned> query = this.All;
            if (examinationsID != 0)
            {
                query = query.Where(o => o.ExaminationsID == examinationsID);
            }
            if (examGroupID != 0)
            {
                query = query.Where(o => o.ExamGroupID == examGroupID);
            }
            if (subjectID != 0)
            {
                query = query.Where(o => o.SubjectID == subjectID);
            }
            if (teacherID != 0)
            {
                query = query.Where(o => o.TeacherID == teacherID);
            }
            if (examInputMarkAssignedID != 0)
            {
                query = query.Where(o => o.ExamInputMarkAssignedID == examInputMarkAssignedID);
            }

            return query;
        }
    }
}