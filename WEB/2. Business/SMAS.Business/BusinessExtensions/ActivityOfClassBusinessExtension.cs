/**
 * The Viettel License
 *
 * Copyright 2012 Viettel Telecom. All rights reserved.
 * VIETTEL PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */

/** 
* @author  
* @version $Revision: $
*/

using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Linq.Expressions;
using System.Web;
using log4net;

using SMAS.DAL.Repository;
using SMAS.DAL.IRepository;
using SMAS.Business.IBusiness;
using SMAS.Models.Models;
using SMAS.Business.Common;

namespace SMAS.Business.Business
{ 
    public partial class ActivityOfClassBusiness
    {  
        public IQueryable<ActivityOfClass> Search(IDictionary<string, object> dic)
        {
            int ActivityOfClassID = Utils.GetInt(dic, "ActivityOfClassID");
            string ActivityOfClassName = Utils.GetString(dic, "ActivityOfClassName");
            System.DateTime? FromTime = Utils.GetDateTime(dic, "FromTime");
            System.DateTime? ToTime = Utils.GetDateTime(dic, "ToTime");
            int ActivityTypeID = Utils.GetInt(dic, "ActivityTypeID");
            int ActivityPlanID = Utils.GetInt(dic, "ActivityPlanID");
            List<int> LstActivityPlanID = Utils.GetIntList(dic, "LstActivityPlanID");
            int DayOfWeek = Utils.GetByte(dic, "DayOfWeek");
            int Section = Utils.GetInt(dic, "Section");
            System.DateTime? ActivityDate = Utils.GetDateTime(dic, "ActivityDate");
            IQueryable<ActivityOfClass> Query = ActivityOfClassRepository.All;
            if (ActivityTypeID != 0)
            {
                Query = Query.Where(evd => evd.ActivityTypeID == ActivityTypeID);
            }
            if (ActivityOfClassID != 0)
            {
                Query = Query.Where(evd => evd.ActivityOfClassID == ActivityOfClassID);
            }
            if (FromTime.HasValue)
            {
                Query = Query.Where(evd => evd.FromTime.Hour == FromTime.Value.Hour && evd.FromTime.Minute == FromTime.Value.Minute);
            }
            if (ToTime.HasValue)
            {
                Query = Query.Where(evd => evd.ToTime.Hour == ToTime.Value.Hour && evd.ToTime.Minute == ToTime.Value.Minute);
            }
            if (ActivityPlanID != 0)
            {
                Query = Query.Where(evd => evd.ActivityPlanID == ActivityPlanID);
            }
            // Truong hop la danh sach cac hoat dong 
            if (LstActivityPlanID != null && LstActivityPlanID.Count > 0)
            {
                Query = Query.Where(e => LstActivityPlanID.Contains(e.ActivityPlanID));
            }
            if (DayOfWeek != 0)
            {
                Query = Query.Where(evd => evd.DayOfWeek == DayOfWeek);
            }
            if (Section != 0)
            {
                Query = Query.Where(evd => evd.Section == Section);
            }
            if (ActivityDate != null)
            {
                Query = Query.Where(evd => evd.ActivityDate.Year == ActivityDate.Value.Year &&
                    evd.ActivityDate.Month == ActivityDate.Value.Month && evd.ActivityDate.Day == ActivityDate.Value.Day);
            }
            if (ActivityOfClassName.Trim().Length != 0) Query = Query.Where(o => o.ActivityOfClassName.Contains(ActivityOfClassName.ToLower()));
            return Query;
        }
        public void Insert(List<ActivityOfClass> lsActivity)
        {
            //validate wrong - comment anhvd
            //for (int i = 0; i < lsActivity.Count - 1; i++)
            //{
            //    for (int j = i + 1; j < lsActivity.Count; j++)
            //    {
            //        if (lsActivity[i].DayOfWeek == lsActivity[j].DayOfWeek && (lsActivity[i].FromTime < lsActivity[j].ToTime || lsActivity[j].FromTime < lsActivity[i].ToTime))
            //        {
            //            throw new BusinessException("Activity_Validate_DOW");
            //        }
            //    }
            //}
            foreach (ActivityOfClass item in lsActivity)
            {
                ValidationMetadata.ValidateObject(item);
                if (item.FromTime > item.ToTime)
                {
                    throw new BusinessException("Activity_Validate_DOW");
                }
                base.Insert(item);
            }
        }
        
        public void Delete(List<int> lsActivityOfClassID)
        {
            for (int i = 0; i < lsActivityOfClassID.Count; i++)
            {
                this.Delete(lsActivityOfClassID[i]);
            }
            this.Save();
        }
    }
}