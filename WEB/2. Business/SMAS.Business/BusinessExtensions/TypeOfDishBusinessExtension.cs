﻿/**
 * The Viettel License
 *
 * Copyright 2012 Viettel Telecom. All rights reserved.
 * VIETTEL PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */

/** 
* @author  
* @version $Revision: $
*/

using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Linq.Expressions;
using System.Web;
using log4net;

using SMAS.DAL.Repository;
using SMAS.DAL.IRepository;
using SMAS.Business.IBusiness;
using SMAS.Models.Models;
using SMAS.Business.Common;

namespace SMAS.Business.Business
{
    public partial class TypeOfDishBusiness
    {
        private void Validate(TypeOfDish entity)
        {
            ValidationMetadata.ValidateObject(entity);
        }

        public void Insert(TypeOfDish entity)
        {
            Validate(entity);
            this.CheckDuplicate(entity.TypeOfDishName, GlobalConstants.LIST_SCHEMA, "TypeOfDish", "TypeOfDishName", true, 0, "TypeOfDish_Label_TypeOfDishName");
            base.Insert(entity);
        }

        public void Update(TypeOfDish entity)
        {
            Validate(entity);
            //TypeOfDishID: PK(TypeOfDish)
            this.CheckAvailable(entity.TypeOfDishID, "TypeOfDish_Label_TypeOfDishID");
            //TypeOfDishName: require, maxlength(300), duplicate
            this.CheckDuplicate(entity.TypeOfDishName, GlobalConstants.LIST_SCHEMA, "TypeOfDish", "TypeOfDishName", true, entity.TypeOfDishID, "TypeOfDish_Label_TypeOfDishName");
            base.Update(entity);
        }

        public void Delete(int TypeOfDishID)
        {
            //TypeOfDishID: PK(FoodCat)
            this.CheckAvailable(TypeOfDishID, "TypeOfDish_Label_TypeOfDishID");
            //TypeOfDishID: Using
          bool checkDelete = new TypeOfDishRepository(this.context).ExistsRow(GlobalConstants.LIST_SCHEMA, "TypeOfDish",
          new Dictionary<string, object>()
                {
                    {"TypeOfDishID",TypeOfDishID}
                }, null);
            if (!checkDelete)
            {
                throw new BusinessException("Common_Delete_Exception");
            }
            //  Thực hiện chuyển IsActive=false, ModifiedDate=Sysdate với bản ghi tương ứng trong bảng TypeOfDish
            TypeOfDish tod = this.Find(TypeOfDishID);
            tod.IsActive = false;
            tod.ModifiedDate = DateTime.Now;
            base.Update(tod);
        }

        public IQueryable<TypeOfDish> Search(IDictionary<string, object> dic)
        {
            //Thực hiện tìm kiếm trong bảng TypeOfDish theo các thông tin trong SearchInfo:
            //- TypeOfDishName: default = “”; “” => Tìm kiếm All
            //- Note: default = “”; “” -> Tìm kiếm All
            //- Description: default = ""; ""  -> tìm kiếm all
            //- IsActive: default=TRUE; NULL -> tìm kiếm all
            int TypeOfDishID = Utils.GetInt(dic, "TypeOfDishID");
            string Note = Utils.GetString(dic, "Note");
            string Description = Utils.GetString(dic, "Description");
            string TypeOfDishName = Utils.GetString(dic, "TypeOfDishName");
            bool IsActive = Utils.GetBool(dic, "IsActive");
            IQueryable<TypeOfDish> query = this.TypeOfDishRepository.All;
            if (Note.Trim().Length != 0)
            {
                query = query.Where(o => o.Note.ToLower().Contains(Note.ToLower()));
            }
            if (Description.Trim().Length != 0)
            {
                query = query.Where(o => o.Description.Contains(Description.ToLower()));
            }
            if (TypeOfDishName.Trim().Length != 0)
            {
                query = query.Where(o => o.TypeOfDishName.Contains(TypeOfDishName.ToLower()));
            }
            if (IsActive)
            {
                query = query.Where(o => o.IsActive == IsActive);
            }
            return query;
        }

    }
}