﻿/**
 * The Viettel License
 *
 * Copyright 2012 Viettel Telecom. All rights reserved.
 * VIETTEL PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */

/** 
* @author  NAMTA
* @version $Revision: $
*/

using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Linq.Expressions;
using System.Web;
using log4net;

using SMAS.DAL.Repository;
using SMAS.DAL.IRepository;
using SMAS.Business.IBusiness;
using SMAS.Models.Models;
using SMAS.Business.Common;
using SMAS.Business.BusinessObject;
using System.IO;
using SMAS.VTUtils.Excel.Export;
using System.Text;
using System.Globalization;

namespace SMAS.Business.Business
{
    public partial class PupilRetestRegistrationBusiness
    {
        public const int STUDYING_JUDGEMENT_RETEST = 2;
        public const int SEMESTER_OF_YEAR_SECOND = 2;
        public const int SEMESTER_OF_YEAR_ALL = 3;
        public const string CHARACTER_D = "CĐ";

        #region Validate
        /// <summary>
        /// NAMTA
        /// </summary>
        /// <param name="PupilRetestRegistration"></param>
        private void Validate(PupilRetestRegistration PupilRetestRegistration)
        {
            ValidationMetadata.ValidateObject(PupilRetestRegistration);
            IDictionary<string, object> SearchInfo = new Dictionary<string, object>();
            SearchInfo["ClassProfileID"] = PupilRetestRegistration.ClassID;
            SearchInfo["AcademicYearID"] = PupilRetestRegistration.AcademicYearID;

            bool compatible = new ClassProfileRepository(this.context).ExistsRow(GlobalConstants.SCHOOL_SCHEMA, "ClassProfile", SearchInfo);

            if (!compatible)
            {
                throw new BusinessException("Common_Validate_NotCompatible");
            }

            SearchInfo = new Dictionary<string, object>();
            SearchInfo["SchoolID"] = PupilRetestRegistration.SchoolID;
            SearchInfo["AcademicYearID"] = PupilRetestRegistration.AcademicYearID;

            compatible = new AcademicYearRepository(this.context).ExistsRow(GlobalConstants.SCHOOL_SCHEMA, "AcademicYear", SearchInfo);

            if (!compatible)
            {
                throw new BusinessException("Common_Validate_NotCompatible");
            }

            //SearchInfo = new Dictionary<string, object>();
            //SearchInfo["PupilProfileID"] = PupilRetestRegistration.PupilID;
            //SearchInfo["CurrentClassID"] = PupilRetestRegistration.ClassID;

            //compatible = new AcademicYearRepository(this.context).ExistsRow(GlobalConstants.PUPIL_SCHEMA, "PupilProfile", SearchInfo);

            //if (!compatible)
            //{
            //    throw new BusinessException("Common_Validate_NotCompatible");
            //}

            SearchInfo = new Dictionary<string, object>();
            SearchInfo["SubjectID"] = PupilRetestRegistration.SubjectID;
            SearchInfo["ClassID"] = PupilRetestRegistration.ClassID;

            compatible = new AcademicYearRepository(this.context).ExistsRow(GlobalConstants.SCHOOL_SCHEMA, "ClassSubject", SearchInfo);

            if (!compatible)
            {
                throw new BusinessException("Common_Validate_NotCompatible");
            }

        }
        #endregion

        #region Them moi
        /// <summary>
        /// Them moi
        /// </summary>
        /// <param name="ListPupilRetestRegistration"></param>
        public void InsertPupilRetestRegistration(int SchoolID, int AcademicYearID, List<PupilRetestRegistration> ListPupilRetestRegistration)
        {
            try
            {
                SetAutoDetectChangesEnabled(false);
            List<int> listClassID = ListPupilRetestRegistration.Select(o => o.ClassID).Distinct().ToList();
            IDictionary<string, object> SearchInfo = new Dictionary<string, object>();
            SearchInfo["AcademicYearID"] = AcademicYearID;
            SearchInfo["Check"] = "check";
            List<PupilOfClass> listPupilOfClassInSchool = PupilOfClassBusiness.SearchBySchool(SchoolID, SearchInfo)
                .Where(o => listClassID.Contains(o.ClassID))
                .ToList();

                List<VPupilRanking> ListPupilRankingInSchool = VPupilRankingBusiness.SearchBySchool(SchoolID, new Dictionary<string, object>() { 
                    {"AcademicYearID", AcademicYearID}
                    })
                .Where(o => listClassID.Contains(o.ClassID))
                .ToList();
                VPupilRanking objPupilRanking = null;
            foreach (PupilRetestRegistration PupilRetestRegistration in ListPupilRetestRegistration)
            {
                this.Validate(PupilRetestRegistration);
                List<PupilOfClass> listPupilOfClass = listPupilOfClassInSchool
                    .Where(o => o.ClassID == PupilRetestRegistration.ClassID
                    && o.PupilID == PupilRetestRegistration.PupilID)
                    .ToList();
                    if (listPupilOfClass == null || listPupilOfClass.Count == 0)
                {
                        continue;
                    }
                    //Kiểm tra PupilOfClass nếu Status != 1
                    if (listPupilOfClass[0].Status != 1)
                    {
                        continue;
                    }
                    List<VPupilRanking> ListPupilRanking = ListPupilRankingInSchool
                        .Where(o => o.ClassID == PupilRetestRegistration.ClassID
                        && o.PupilID == PupilRetestRegistration.PupilID
                        && o.Semester == PupilRetestRegistration.Semester)
                        .ToList();

                        //Kiem tra hoc sinh thuoc dien thi lai
                        if (ListPupilRanking != null && ListPupilRanking.Count > 0)
                        {
                        objPupilRanking = ListPupilRanking.FirstOrDefault();
                        if (objPupilRanking != null && objPupilRanking.StudyingJudgementID != null)
                            {
                            int StudyingJudgementID = objPupilRanking.StudyingJudgementID.Value;
                                if (StudyingJudgementID == STUDYING_JUDGEMENT_RETEST)
                                {
                                    this.Insert(PupilRetestRegistration);
                                }
                            }
                        }

                    }
                this.Save();
                }
            finally
            {
                SetAutoDetectChangesEnabled(true);
            }
        }
        #endregion

        #region Update
        public void UpdatePupilRetestRegistration(int SchoolID, int AcademicYearID, List<PupilRetestRegistration> ListPupilRetestRegistration)
        {

            List<int> listClassID = ListPupilRetestRegistration.Select(o => o.ClassID).Distinct().ToList();
            IDictionary<string, object> SearchInfo = new Dictionary<string, object>();
            SearchInfo["AcademicYearID"] = AcademicYearID;
            SearchInfo["Check"] = "check";
            List<PupilOfClass> listPupilOfClassInSchool = PupilOfClassBusiness.SearchBySchool(SchoolID, SearchInfo)
                .Where(o => listClassID.Contains(o.ClassID))
                .ToList();

            List<PupilRanking> ListPupilRankingInSchool = PupilRankingBusiness.SearchBySchool(SchoolID, new Dictionary<string, object>() { 
                    {"AcademicYearID", AcademicYearID}
                    })
                .Where(o => listClassID.Contains(o.ClassID))
                .ToList();
            foreach (PupilRetestRegistration prr in ListPupilRetestRegistration)
            {
                this.Validate(prr);
                Utils.ValidateRequire(prr.PupilRetestRegistrationID, "PupilRetestRegistration_Label_PupilRetestRegistrationID");
                PupilOfClass pupilOfClass = listPupilOfClassInSchool
                                    .Where(o => o.ClassID == prr.ClassID
                                    && o.PupilID == prr.PupilID)
                                    .FirstOrDefault();
                if (pupilOfClass != null && pupilOfClass.Status == SystemParamsInFile.PUPIL_STATUS_STUDYING)
                {
                    List<PupilRanking> ListPupilRanking = ListPupilRankingInSchool
                        .Where(o => o.ClassID == prr.ClassID
                        && o.PupilID == prr.PupilID
                        && o.Semester == prr.Semester)
                        .ToList();

                    PupilRanking checkPupil = null;
                    if (pupilOfClass.ClassProfile.EducationLevel.Grade == SystemParamsInFile.EDUCATION_GRADE_PRIMARY)
                        checkPupil = ListPupilRanking.Where(u => u.Semester >= SystemParamsInFile.SEMESTER_OF_YEAR_SECOND && u.Semester == ListPupilRanking.Where(v => v.PupilID == u.PupilID && v.ClassID == u.ClassID).Max(v => v.Semester)).SingleOrDefault();
                    else
                        checkPupil = ListPupilRanking.Where(u => u.Semester >= SystemParamsInFile.SEMESTER_OF_YEAR_ALL && u.Semester == ListPupilRanking.Where(v => v.PupilID == u.PupilID && v.ClassID == u.ClassID).Max(v => v.Semester)).SingleOrDefault();

                    //Kiem tra hoc sinh thuoc dien thi lai
                    if (checkPupil != null && checkPupil.StudyingJudgementID == SystemParamsInFile.STUDYING_JUDGEMENT_RETEST)
                    {
                        this.Delete(prr.PupilRetestRegistrationID);
                        PupilRetestRegistration obj = new Models.Models.PupilRetestRegistration();
                        obj.AcademicYearID = prr.AcademicYearID;
                        obj.ClassID = prr.ClassID;
                        obj.PupilID = prr.PupilID;
                        obj.SchoolID = prr.SchoolID;
                        obj.Semester = prr.Semester;
                        obj.SubjectID = prr.SubjectID;
                        obj.ModifiedDate = prr.ModifiedDate;
                        obj.RegisteredDate = prr.RegisteredDate;
                        this.Insert(obj);
                    }
                }
            }
        }
        #endregion

        #region delete
        public void DeletePupilRetestRegistration(int PupilRetestRegistrationID, int SchoolID)
        {
            this.CheckAvailable(PupilRetestRegistrationID, "PupilRetestRegistrationID_Label_PupilRetestRegistrationID", true);
            PupilRetestRegistration PupilRetestRegistration = this.Find(PupilRetestRegistrationID);
            if (PupilRetestRegistration != null)
            {
                IDictionary<string, object> SearchInfo = new Dictionary<string, object>();
                SearchInfo["PupilID"] = PupilRetestRegistration.PupilID;
                SearchInfo["SchoolID"] = PupilRetestRegistration.SchoolID;

                bool compatible = new ClassProfileRepository(this.context).ExistsRow(GlobalConstants.PUPIL_SCHEMA, "PupilRetestRegistration", SearchInfo);

                if (!compatible)
                {
                    throw new BusinessException("Common_Validate_NotCompatible");
                }
                //if (!AcademicYearBusiness.IsCurrentYear(PupilRetestRegistration.AcademicYearID))
                //{
                //    throw new BusinessException("PupilRetestRegistration_Label_AcademicYearID");
                //}
                //Thoa man dieu kien thuc hien xoa
                this.Delete(PupilRetestRegistrationID);
            }
        }

        #endregion

        #region SearchPupilRetestRegistration
        public IQueryable<PupilRetestRegistration> SearchPupilRetestRegistration(IDictionary<string, object> dic)
        {
            IQueryable<PupilRetestRegistration> listSearchPupilRetestRegistration = from prr in this.All
                                                                                    join cp in ClassProfileRepository.All on prr.ClassID equals cp.ClassProfileID
                                                                                    where cp.IsActive == true
                                                                                    select prr;

            int PupilRetestRegistrationID = Utils.GetInt(dic, "PupilRetestRegistrationID");
            int PupilID = Utils.GetInt(dic, "PupilID");
            int ClassID = Utils.GetInt(dic, "ClassID");
            int SchoolID = Utils.GetInt(dic, "SchoolID");
            int AcademicYearID = Utils.GetInt(dic, "AcademicYearID");
            int SubjectID = Utils.GetInt(dic, "SubjectID");
            int EduationLevelID = Utils.GetInt(dic, "EduationLevelID");
            DateTime? RegisteredDate = Utils.GetDateTime(dic, "RegisteredDate");
            List<int> ListClassID = Utils.GetIntList(dic, "ListClassID");

            if (PupilRetestRegistrationID != 0)
            {
                listSearchPupilRetestRegistration = listSearchPupilRetestRegistration.Where(o => o.PupilRetestRegistrationID == PupilRetestRegistrationID);
            }

            if (PupilID != 0)
            {
                listSearchPupilRetestRegistration = listSearchPupilRetestRegistration.Where(o => o.PupilID == PupilID);
            }

            if (ClassID != 0)
            {
                listSearchPupilRetestRegistration = listSearchPupilRetestRegistration.Where(o => o.ClassID == ClassID);
            }

            if (ListClassID != null && ListClassID.Count > 0)
            {
                listSearchPupilRetestRegistration = listSearchPupilRetestRegistration.Where(o => ListClassID.Contains(o.ClassID));
            }

            if (SchoolID != 0)
            {
                listSearchPupilRetestRegistration = listSearchPupilRetestRegistration.Where(o => o.SchoolID == SchoolID);
            }

            if (AcademicYearID != 0)
            {
                listSearchPupilRetestRegistration = listSearchPupilRetestRegistration.Where(o => o.AcademicYearID == AcademicYearID);
            }

            if (SubjectID != 0)
            {
                listSearchPupilRetestRegistration = listSearchPupilRetestRegistration.Where(o => o.SubjectID == SubjectID);
            }

            if (EduationLevelID != 0)
            {
                listSearchPupilRetestRegistration = listSearchPupilRetestRegistration.Where(o => o.ClassProfile.EducationLevelID == EduationLevelID);
            }

            if (RegisteredDate != null)
            {
                listSearchPupilRetestRegistration = listSearchPupilRetestRegistration.Where(o => o.RegisteredDate == RegisteredDate);
            }

            return (listSearchPupilRetestRegistration);


        }

        private IQueryable<PupilRetestRegistration> Search(IDictionary<string, object> dic)
        {
            dic = dic == null ? new Dictionary<string, object>() : dic;
            int? PupilRetestRegistrationID = Utils.GetNullableInt(dic, "PupilRetestRegistrationID");
            int? PupilID = Utils.GetNullableInt(dic, "PupilID");
            int? ClassID = Utils.GetNullableInt(dic, "ClassID");
            int? SchoolID = Utils.GetNullableInt(dic, "SchoolID");
            int? AcademicYearID = Utils.GetNullableInt(dic, "AcademicYearID");
            int? SubjectID = Utils.GetNullableShort(dic, "SubjectID");
            DateTime? RegisteredDate = Utils.GetDateTime(dic, "RegisteredDate");
            int? EduationLevelID = Utils.GetNullableInt(dic, "EducationLevelID");
            List<int> ListClassID = Utils.GetIntList(dic, "ListClassID");
            List<int> lstPupilID = Utils.GetIntList(dic, "lstPupilID");
            bool isVNEN = Utils.GetBool(dic, "IsVNEN", false);

            var lstPupilRetestReg = this.PupilRetestRegistrationRepository.All.Where(o => o.PupilProfile.IsActive);

            if (ClassID.HasValue && ClassID.Value != 0)
            {
                lstPupilRetestReg = from prr in lstPupilRetestReg
                                    join cp in ClassProfileBusiness.All on prr.ClassID equals cp.ClassProfileID
                                    where (!cp.IsActive.HasValue || (cp.IsActive.HasValue && cp.IsActive.Value))
                                    && prr.ClassID == ClassID
                                    select prr;
            }
            if (ListClassID.Count > 0)
            {
                lstPupilRetestReg = from prr in lstPupilRetestReg
                                    join cp in ClassProfileBusiness.All on prr.ClassID equals cp.ClassProfileID
                                    where (!cp.IsActive.HasValue || (cp.IsActive.HasValue && cp.IsActive.Value))
                                    && ListClassID.Contains(prr.ClassID)
                                    select prr;
            }
            if (isVNEN)
            {
                lstPupilRetestReg = lstPupilRetestReg.Where(u => (!u.ClassProfile.IsVnenClass.HasValue || (u.ClassProfile.IsVnenClass.HasValue && u.ClassProfile.IsVnenClass.Value == false)));
            }
            if (PupilRetestRegistrationID.HasValue && PupilRetestRegistrationID.Value > 0)
                lstPupilRetestReg = lstPupilRetestReg.Where(u => u.PupilRetestRegistrationID == PupilRetestRegistrationID.Value);

            if (PupilID.HasValue && PupilID.Value > 0)
                lstPupilRetestReg = lstPupilRetestReg.Where(u => u.PupilID == PupilID.Value);
            if (lstPupilID.Count > 0)
            {
                lstPupilRetestReg = lstPupilRetestReg.Where(p => lstPupilID.Contains(p.PupilID));
            }
            if (SchoolID.HasValue && SchoolID.Value > 0)
                lstPupilRetestReg = lstPupilRetestReg.Where(u => u.SchoolID == SchoolID.Value);

            if (AcademicYearID.HasValue && AcademicYearID.Value > 0)
                lstPupilRetestReg = lstPupilRetestReg.Where(u => u.AcademicYearID == AcademicYearID.Value);

            if (SubjectID.HasValue && SubjectID.Value > 0)
                lstPupilRetestReg = lstPupilRetestReg.Where(u => u.SubjectID == SubjectID.Value);

            if (RegisteredDate.HasValue)
                lstPupilRetestReg = lstPupilRetestReg.Where(u => u.RegisteredDate.Value.Date == RegisteredDate.Value.Date);

            if (EduationLevelID.HasValue && EduationLevelID.Value > 0)
                lstPupilRetestReg = lstPupilRetestReg.Where(u => u.ClassProfile.EducationLevelID == EduationLevelID.Value);

            //if (ListClassID.Count > 0)
            //    lstPupilRetestReg = lstPupilRetestReg.Where(u => ListClassID.Contains(u.ClassID));

            return lstPupilRetestReg;
        }

        public IQueryable<PupilRetestRegistration> SearchBySchool(int SchoolID, IDictionary<string, object> dic)
        {
            dic = dic == null ? new Dictionary<string, object>() : dic;
            if (SchoolID <= 0)
                return null;
            else
            {
                dic["SchoolID"] = SchoolID;
                return Search(dic);
            }
        }
        #endregion

        #region SearchSubjectRetest
        public List<int> SearchSubjectRetest(IDictionary<string, object> dic)
        {
            IQueryable<PupilRetestRegistration> listSearchPupilRetestRegistration = from prr in this.All
                                                                                    join cp in ClassProfileRepository.All on prr.ClassID equals cp.ClassProfileID
                                                                                    where cp.IsActive == true
                                                                                    select prr;

            int ClassID = Utils.GetInt(dic, "ClassID");
            int SchoolID = Utils.GetInt(dic, "SchoolID");
            int AcademicYearID = Utils.GetInt(dic, "AcademicYearID");
            int EduationLevelID = Utils.GetInt(dic, "EduationLevelID");
            int? Semester = Utils.GetNullableByte(dic, "Semester");

            if (ClassID != 0)
            {
                listSearchPupilRetestRegistration = listSearchPupilRetestRegistration.Where(o => o.ClassID == ClassID);
            }
            else
            {
                if (EduationLevelID != 0)
                {
                    listSearchPupilRetestRegistration = listSearchPupilRetestRegistration.Where(o => o.ClassProfile.EducationLevelID == EduationLevelID);
                }
            }

            if (SchoolID != 0)
            {
                listSearchPupilRetestRegistration = listSearchPupilRetestRegistration.Where(o => o.SchoolID == SchoolID);
            }

            if (Semester != 0)
            {
                listSearchPupilRetestRegistration = listSearchPupilRetestRegistration.Where(o => o.Semester == Semester);
            }

            return (listSearchPupilRetestRegistration.Select(o => o.SubjectID).ToList());


        }

        #endregion

        #region GetListPupilRetestRegistrationBySubject
        /// <summary>
        /// Danh sách học sinh thi lại theo lớp theo môn 
        /// </summary>
        /// <param name="dic"></param>
        /// <returns>NAMTA</returns>
        public IQueryable<PupilRetestRegistrationSubjectBO> GetListPupilRetestRegistrationBySubject(IDictionary<string, object> dic)
        {
            int SchoolID = Utils.GetInt(dic, "SchoolID");
            int AcademicYearID = Utils.GetInt(dic, "AcademicYearID");
            int EducationLevelID = Utils.GetInt(dic, "EducationLevelID");
            int ClassID = Utils.GetInt(dic, "ClassID");
            int SubjectID = Utils.GetInt(dic, "SubjectID");
            int AppliedLevel = Utils.GetInt(dic, "AppliedLevel");
            List<int> lstSubjectID = Utils.GetIntList(dic, "ListSubjectID");
            List<int> ListClassID = Utils.GetIntList(dic, "ListClassID");
            bool isVNEN = Utils.GetBool(dic, "IsVNEN", false);
            Dictionary<string, object> searchPOC = new Dictionary<string, object>();
            int partitionId = UtilsBusiness.GetPartionId(SchoolID);
            searchPOC["AcademicYearID"] = AcademicYearID;
            searchPOC["EducationLevelID"] = EducationLevelID;
            searchPOC["ClassID"] = ClassID;
            searchPOC["AppliedLevel"] = AppliedLevel;

            if (ClassID > 0)
            {
                searchPOC["Check"] = "Check";
            }
            else
            {
                searchPOC["CheckWithClass"] = "Check";
                searchPOC["ListClassID"] = ListClassID;
            }
            var iqPupilOfClass = PupilOfClassBusiness.SearchBySchool(SchoolID, searchPOC);
            // AnhVD - 20140724 - Kiem tra rang buoc khi cap nhat diem thi lai
            Dictionary<string, object> search = new Dictionary<string, object>();
            search["AcademicYearID"] = AcademicYearID;
            search["EducationLevelID"] = EducationLevelID;
            search["ClassID"] = ClassID;
            search["AppliedLevel"] = AppliedLevel;
            search["StudyingJudgementID"] = SystemParamsInFile.STUDYING_JUDGEMENT_RETEST;

            if (AppliedLevel == SystemParamsInFile.APPLIED_LEVEL_PRIMARY)
            {
                return null;
            }
            else if (AppliedLevel == SystemParamsInFile.APPLIED_LEVEL_SECONDARY || AppliedLevel == SystemParamsInFile.APPLIED_LEVEL_TERTIARY)
            {
                // AnhVD 20140724 - Cap 2-3 sau khi tong ket ca nam, thi lai HK = 3
                //Chiendd1: 04/08/2015 Tuning lai chuc nang
                search["Semester"] = SystemParamsInFile.SEMESTER_OF_YEAR_ALL;
                IQueryable<VPupilRanking> iqPupilRanking = VPupilRankingBusiness.SearchBySchool(SchoolID, search).Where(o => o.PeriodID == null);

                List<PupilRetestRegistration> queryRetestReg;

                IQueryable<PupilRetestRegistration> RetestReg = from prr in this.All.Where(u => u.SchoolID == SchoolID && u.AcademicYearID == AcademicYearID)
                                                                join cp in ClassProfileBusiness.All.Where(c => c.AcademicYearID == AcademicYearID && c.EducationLevelID == EducationLevelID && c.IsActive==true) on prr.ClassID equals cp.ClassProfileID
                                                                join pr in iqPupilRanking on new { prr.SchoolID, prr.AcademicYearID, prr.ClassID, prr.PupilID, prr.Semester }
                                                                            equals new { pr.SchoolID, pr.AcademicYearID, pr.ClassID, pr.PupilID, pr.Semester }
                                                                where (isVNEN == true && (!cp.IsVnenClass.HasValue || (cp.IsVnenClass.HasValue && cp.IsVnenClass.Value == false)))
                                                                select prr;
                if (ClassID > 0)
                {
                    RetestReg = RetestReg.Where(c => c.ClassID == ClassID);
                }
                if (lstSubjectID.Count > 0)
                {
                    RetestReg = RetestReg.Where(c => lstSubjectID.Contains(c.SubjectID));
                }

                if (SubjectID != 0)
                {
                    RetestReg = RetestReg.Where(c => c.SubjectID == SubjectID);
                }
                queryRetestReg = RetestReg.ToList();
                List<int> lstPupilRetestReg = queryRetestReg.Select(c => c.PupilID).ToList();
                List<SummedUpRecordBO> summedUp;
                IQueryable<SummedUpRecordBO> summedAll = (from sur in VSummedUpRecordBusiness.All.Where(u => u.SchoolID == SchoolID
                                                                            && u.AcademicYearID == AcademicYearID
                                                                            && u.Last2digitNumberSchool == partitionId
                                                                            && u.Semester >= SystemParamsInFile.SEMESTER_OF_YEAR_ALL
                                                                            && lstPupilRetestReg.Contains(u.PupilID))
                                                          join cs in ClassSubjectBusiness.All.Where(c => c.Last2digitNumberSchool == partitionId) on new { sur.ClassID, sur.SubjectID } equals new { cs.ClassID, cs.SubjectID }
                                                          select new SummedUpRecordBO
                                                          {
                                                              SummedUpRecordID = sur.SummedUpRecordID,
                                                              ClassID = sur.ClassID,
                                                              SubjectID = sur.SubjectID,
                                                              Semester = sur.Semester,
                                                              PupilID = sur.PupilID,
                                                              SummedUpDate = sur.SummedUpDate,
                                                              SummedUpMark = sur.SummedUpMark,
                                                              JudgementResult = sur.JudgementResult,
                                                              ReTestJudgement = sur.ReTestJudgement,
                                                              CreatedAcademicYear = sur.CreatedAcademicYear,
                                                              IsCommenting = cs.IsCommenting,
                                                              Comment = sur.Comment,
                                                              PeriodID = sur.PeriodID,
                                                              ReTestMark = sur.ReTestMark,
                                                          });
                if (ClassID > 0)
                {
                    summedAll = summedAll.Where(c => c.ClassID == ClassID);
                }
                if (lstSubjectID.Count > 0)
                {
                    summedAll = summedAll.Where(c => lstSubjectID.Contains(c.SubjectID.Value));
                }
                else
                {
                    summedAll = summedAll.Where(c => c.SubjectID == SubjectID);
                }

                summedUp = summedAll.ToList();
                // Tong ket ca nam
                List<SummedUpRecordBO> summedUpSemesterAll = summedUp.Where(u => u.Semester == SystemParamsInFile.SEMESTER_OF_YEAR_ALL).ToList();
                List<SummedUpRecordBO> summedUpMaxSemester = summedUp.Where(u => u.Semester > SystemParamsInFile.SEMESTER_OF_YEAR_ALL).ToList();


                List<PupilRetestRegistrationSubjectBO> lstResult = new List<PupilRetestRegistrationSubjectBO>();
                PupilRetestRegistrationSubjectBO objResult = null;
                SummedUpRecordBO objSummedUprecordAll = null;
                SummedUpRecordBO objSummedUpMaxDate = null;
                PupilRetestRegistration item = null;
                for (int i = 0; i < queryRetestReg.Count; i++)
                {
                    item = queryRetestReg[i];
                    objResult = new PupilRetestRegistrationSubjectBO();
                    objSummedUprecordAll = summedUpSemesterAll.Where(p => p.ClassID == item.ClassID && p.PupilID == item.PupilID && p.SubjectID == item.SubjectID).FirstOrDefault();
                    objSummedUpMaxDate = summedUpMaxSemester.Where(p => p.PupilID == item.PupilID && p.ClassID == item.ClassID && p.SubjectID == item.SubjectID).OrderByDescending(c => c.SummedUpDate).FirstOrDefault();
                    objResult.AcademicYearID = item.AcademicYearID;
                    objResult.ClassID = item.ClassID;
                    objResult.ClassName = item.ClassProfile.DisplayName;
                    objResult.FullName = item.PupilProfile.FullName;
                    objResult.ModifiedDatePRR = item.ModifiedDate;
                    objResult.PupilCode = item.PupilProfile.PupilCode;
                    objResult.PupilID = item.PupilID;
                    objResult.PupilRetestRegistrationID = item.PupilRetestRegistrationID;
                    objResult.SchoolID = item.SchoolID;
                    objResult.Semester = item.Semester;
                    objResult.SubjectID = item.SubjectID;
                    objResult.Name = item.PupilProfile.Name;
                    objResult.isAbsentContest = item.IsAbsentContest;
                    objResult.Reason = item.Reason;
                    if (objSummedUprecordAll != null)
                    {
                        objResult.JudgementResult = objSummedUprecordAll.JudgementResult;
                        objResult.SummedUpMark = objSummedUprecordAll.SummedUpMark;
                        objResult.Year = objSummedUprecordAll.CreatedAcademicYear;
                        objResult.IsCommenting = objSummedUprecordAll.IsCommenting.Value;
                    }
                    if (objSummedUpMaxDate != null)
                    {
                        objResult.Comment = objSummedUpMaxDate.Comment;
                        objResult.PeriodID = objSummedUpMaxDate.PeriodID;
                        objResult.ReTestJudgement = objSummedUpMaxDate.ReTestJudgement;
                        objResult.ReTestMark = objSummedUpMaxDate.ReTestMark;
                        objResult.SummedUpDate = objSummedUpMaxDate.SummedUpDate;
                        objResult.SummedUpRecordID = objSummedUpMaxDate.SummedUpRecordID.Value;
                    }

                    lstResult.Add(objResult);
                }


                var query2 = from su in lstResult
                             join poc in iqPupilOfClass on su.PupilID equals  poc.PupilID
                             where (poc.Status == SystemParamsInFile.PUPIL_STATUS_STUDYING)
                             orderby ClassID > 0 ? poc.OrderInClass : 0, su.Name, su.FullName
                             select su;
                return query2.AsQueryable();
            }
            return null;
        }
        public IQueryable<PupilRetestRegistrationSubjectBO> GetListPupilRetestExportSumManyMark(IDictionary<string, object> dic)
        {
            int SchoolID = Utils.GetInt(dic, "SchoolID");
            int AcademicYearID = Utils.GetInt(dic, "AcademicYearID");
            int EducationLevelID = Utils.GetInt(dic, "EducationLevelID");
            int ClassID = Utils.GetInt(dic, "ClassID");
            int SubjectID = Utils.GetInt(dic, "SubjectID");
            int AppliedLevel = Utils.GetInt(dic, "AppliedLevel");
            List<int> lstSubjectID = Utils.GetIntList(dic, "ListSubjectID");
            List<int> ListClassID = Utils.GetIntList(dic, "ListClassID");
            bool isVNEN = Utils.GetBool(dic, "IsVNEN", false);
            Dictionary<string, object> searchPOC = new Dictionary<string, object>();
            int partitionId = UtilsBusiness.GetPartionId(SchoolID);
            searchPOC["AcademicYearID"] = AcademicYearID;
            searchPOC["EducationLevelID"] = EducationLevelID;
            searchPOC["ClassID"] = ClassID;
            searchPOC["AppliedLevel"] = AppliedLevel;

            if (ClassID > 0)
            {
                searchPOC["Check"] = "Check";
            }
            else
            {
                searchPOC["CheckWithClass"] = "Check";
                searchPOC["ListClassID"] = ListClassID;
            }
            var iqPupilOfClass = PupilOfClassBusiness.SearchBySchool(SchoolID, searchPOC);
            // AnhVD - 20140724 - Kiem tra rang buoc khi cap nhat diem thi lai
            Dictionary<string, object> search = new Dictionary<string, object>();
            search["AcademicYearID"] = AcademicYearID;
            search["EducationLevelID"] = EducationLevelID;
            search["ClassID"] = ClassID;
            search["AppliedLevel"] = AppliedLevel;
            search["StudyingJudgementID"] = SystemParamsInFile.STUDYING_JUDGEMENT_RETEST;

            if (AppliedLevel == SystemParamsInFile.APPLIED_LEVEL_PRIMARY)
            {
                return null;
            }
            else if (AppliedLevel == SystemParamsInFile.APPLIED_LEVEL_SECONDARY || AppliedLevel == SystemParamsInFile.APPLIED_LEVEL_TERTIARY)
            {
                // AnhVD 20140724 - Cap 2-3 sau khi tong ket ca nam, thi lai HK = 3
                //Chiendd1: 04/08/2015 Tuning lai chuc nang
                search["Semester"] = SystemParamsInFile.SEMESTER_OF_YEAR_ALL;
                IQueryable<VPupilRanking> iqPupilRanking = VPupilRankingBusiness.SearchBySchool(SchoolID, search).Where(o => o.PeriodID == null);

                List<PupilRetestRegistrationBO> queryRetestReg;

                IQueryable<PupilRetestRegistrationBO> RetestReg = from prr in this.All.Where(u => u.SchoolID == SchoolID && u.AcademicYearID == AcademicYearID)
                                                                  join cp in ClassProfileBusiness.All.Where(c => c.AcademicYearID == AcademicYearID && c.IsActive == true) on prr.ClassID equals cp.ClassProfileID
                                                                  join pr in iqPupilRanking on new { prr.SchoolID, prr.AcademicYearID, prr.ClassID, prr.PupilID, prr.Semester }
                                                                              equals new { pr.SchoolID, pr.AcademicYearID, pr.ClassID, pr.PupilID, pr.Semester }
                                                                  where (isVNEN == true && (!cp.IsVnenClass.HasValue || (cp.IsVnenClass.HasValue && cp.IsVnenClass.Value == false)))
                                                                  select new PupilRetestRegistrationBO {                                                                     
                                                                    PupilID = prr.PupilID,
                                                                    ClassID = prr.ClassID,
                                                                    SchoolID = prr.SchoolID,
                                                                    AcademicYearID = prr.AcademicYearID,
                                                                    SubjectID = prr.SubjectID,
                                                                    Semester = prr.Semester,
                                                                    RegisteredDate = prr.RegisteredDate,
                                                                    ModifiedDate = prr.ModifiedDate,                                                                                                                                                                             
                                                                     IsAbsentContest = prr.IsAbsentContest,
                                                                    Reason = prr.Reason,
                                                                    EducationLevelID = cp.EducationLevelID,
                                                                    DisplayName = cp.DisplayName,
                                                                    FullName = prr.PupilProfile.FullName,
                                                                    Name = prr.PupilProfile.Name
                                                                  };
                if (EducationLevelID > 0)
                {
                    RetestReg = RetestReg.Where(x => x.EducationLevelID == EducationLevelID);
                }              
                if (ClassID > 0)
                {
                    RetestReg = RetestReg.Where(c => c.ClassID == ClassID );
                }
                if (lstSubjectID.Count > 0)
                {
                    RetestReg = RetestReg.Where(c => lstSubjectID.Contains(c.SubjectID.Value));
                }

                if (SubjectID != 0)
                {
                    RetestReg = RetestReg.Where(c => c.SubjectID == SubjectID);
                }
                queryRetestReg = RetestReg.ToList();
                List<int> lstPupilRetestReg = queryRetestReg.Select(c => c.PupilID).ToList();
                List<SummedUpRecordBO> summedUp;
                IQueryable<SummedUpRecordBO> summedAll = (from sur in VSummedUpRecordBusiness.All.Where(u => u.SchoolID == SchoolID
                                                                            && u.AcademicYearID == AcademicYearID
                                                                            && u.Last2digitNumberSchool == partitionId
                                                                            && u.Semester >= SystemParamsInFile.SEMESTER_OF_YEAR_ALL
                                                                            && lstPupilRetestReg.Contains(u.PupilID))
                                                          join cs in ClassSubjectBusiness.All.Where(c => c.Last2digitNumberSchool == partitionId) on new { sur.ClassID, sur.SubjectID } equals new { cs.ClassID, cs.SubjectID }
                                                          select new SummedUpRecordBO
                                                          {
                                                              SummedUpRecordID = sur.SummedUpRecordID,
                                                              ClassID = sur.ClassID,
                                                              SubjectID = sur.SubjectID,
                                                              Semester = sur.Semester,
                                                              PupilID = sur.PupilID,
                                                              SummedUpDate = sur.SummedUpDate,
                                                              SummedUpMark = sur.SummedUpMark,
                                                              JudgementResult = sur.JudgementResult,
                                                              ReTestJudgement = sur.ReTestJudgement,
                                                              CreatedAcademicYear = sur.CreatedAcademicYear,
                                                              IsCommenting = cs.IsCommenting,
                                                              Comment = sur.Comment,
                                                              PeriodID = sur.PeriodID,
                                                              ReTestMark = sur.ReTestMark,
                                                          });
                if (ClassID > 0)
                {
                    summedAll = summedAll.Where(c => c.ClassID == ClassID);
                }
                if (lstSubjectID.Count > 0)
                {
                    summedAll = summedAll.Where(c => lstSubjectID.Contains(c.SubjectID.Value));
                }
                else
                {
                    summedAll = summedAll.Where(c => c.SubjectID == SubjectID);
                }

                summedUp = summedAll.ToList();
                // Tong ket ca nam
                List<SummedUpRecordBO> summedUpSemesterAll = summedUp.Where(u => u.Semester == SystemParamsInFile.SEMESTER_OF_YEAR_ALL).ToList();
                List<SummedUpRecordBO> summedUpMaxSemester = summedUp.Where(u => u.Semester > SystemParamsInFile.SEMESTER_OF_YEAR_ALL).ToList();


                List<PupilRetestRegistrationSubjectBO> lstResult = new List<PupilRetestRegistrationSubjectBO>();
                PupilRetestRegistrationSubjectBO objResult = null;
                SummedUpRecordBO objSummedUprecordAll = null;
                SummedUpRecordBO objSummedUpMaxDate = null;
                PupilRetestRegistrationBO item = null;
                for (int i = 0; i < queryRetestReg.Count; i++)
                {
                    item = queryRetestReg[i];
                    objResult = new PupilRetestRegistrationSubjectBO();
                    objSummedUprecordAll = summedUpSemesterAll.Where(p => p.ClassID == item.ClassID && p.PupilID == item.PupilID && p.SubjectID == item.SubjectID).FirstOrDefault();
                    objSummedUpMaxDate = summedUpMaxSemester.Where(p => p.PupilID == item.PupilID && p.ClassID == item.ClassID && p.SubjectID == item.SubjectID).OrderByDescending(c => c.SummedUpDate).FirstOrDefault();
                    objResult.AcademicYearID = item.AcademicYearID.Value;
                    objResult.ClassID = item.ClassID.Value;
                    objResult.ClassName = item.DisplayName;
                    objResult.FullName = item.FullName;
                    objResult.ModifiedDatePRR = item.ModifiedDate;
                    objResult.PupilCode = item.PupilCode;
                    objResult.PupilID = item.PupilID;
                    objResult.PupilRetestRegistrationID = item.PupilRetestRegistrationID;
                    objResult.SchoolID = item.SchoolID.Value;
                    objResult.Semester = item.Semester;
                    objResult.SubjectID = item.SubjectID.Value;
                    objResult.Name = item.Name;
                    objResult.isAbsentContest = item.IsAbsentContest;
                    objResult.Reason = item.Reason;
                    if (objSummedUprecordAll != null)
                    {
                        objResult.JudgementResult = objSummedUprecordAll.JudgementResult;
                        objResult.SummedUpMark = objSummedUprecordAll.SummedUpMark;
                        objResult.Year = objSummedUprecordAll.CreatedAcademicYear;
                        objResult.IsCommenting = objSummedUprecordAll.IsCommenting.Value;
                    }
                    if (objSummedUpMaxDate != null)
                    {
                        objResult.Comment = objSummedUpMaxDate.Comment;
                        objResult.PeriodID = objSummedUpMaxDate.PeriodID;
                        objResult.ReTestJudgement = objSummedUpMaxDate.ReTestJudgement;
                        objResult.ReTestMark = objSummedUpMaxDate.ReTestMark;
                        objResult.SummedUpDate = objSummedUpMaxDate.SummedUpDate;
                        objResult.SummedUpRecordID = objSummedUpMaxDate.SummedUpRecordID.Value;
                    }

                    lstResult.Add(objResult);
                }


                var query2 = from su in lstResult
                             join poc in iqPupilOfClass on su.PupilID equals poc.PupilID
                             where (poc.Status == SystemParamsInFile.PUPIL_STATUS_STUDYING)
                             orderby ClassID > 0 ? poc.OrderInClass : 0, su.Name, su.FullName
                             select su;
                return query2.AsQueryable();
            }
            return null;
        }

        #endregion

        #region GetListPupilRetestToCategory
        public IQueryable<PupilRetestToCategoryBO> GetListPupilRetestToCategory(IDictionary<string, object> dic)
        {


            int ClassID = Utils.GetInt(dic, "ClassID");
            int SchoolID = Utils.GetInt(dic, "SchoolID");
            int AcademicYearID = Utils.GetInt(dic, "AcademicYearID");
            int EduationLevelID = Utils.GetInt(dic, "EduationLevelID");
            int? Semester = SEMESTER_OF_YEAR_ALL;

            var querry = from prr in this.All
                         join poc in PupilOfClassRepository.All on prr.PupilID equals poc.PupilID
                         join pr in PupilRankingRepository.All on prr.PupilID equals pr.PupilID
                         join cp in PupilProfileRepository.All on prr.PupilID equals cp.PupilProfileID
                         select new PupilRetestToCategoryBO
                         {
                             Semester = prr.Semester,
                             ClassID = prr.ClassID,
                             ClassName = prr.ClassProfile.DisplayName,
                             SubjectID = prr.SubjectID,
                             EducationLevelID = poc.ClassProfile.EducationLevelID,
                             SchoolID = prr.SchoolID,
                             AcademicYearID = prr.AcademicYearID,
                             PupilID = prr.PupilID,
                             FullName = cp.FullName,
                             PupilCode = cp.PupilCode,
                             BirthDate = cp.BirthDate,
                             Genre = cp.Genre,
                             CapacityLevelID = pr.CapacityLevelID,
                             StudyingJudgementID = pr.StudyingJudgementID,
                             ConductLevelID = pr.ConductLevelID

                         };

            if (ClassID != 0)
            {
                querry = querry.Where(o => o.ClassID == ClassID);
            }
            if (EduationLevelID != 0)
            {
                querry = querry.Where(o => o.EducationLevelID == EduationLevelID);
            }

            if (SchoolID != 0)
            {
                querry = querry.Where(o => o.SchoolID == SchoolID);
            }

            if (Semester != 0)
            {
                querry = querry.Where(o => o.Semester == Semester);
            }

            return (querry);


        }

        #endregion

        #region GetPupilPrimaryRetestToCategory

        public int CountNumberRetest(int PupilID, int SchoolID, int AcademicYearID)
        {
            if (PupilID == 0 || SchoolID == 0 || AcademicYearID == 0)
            {
                return 0;
            }
            else
            {
                IDictionary<string, object> dic = new Dictionary<string, object>();
                dic["PupilID"] = PupilID;
                dic["AcademicYearID"] = AcademicYearID;

                return (this.SearchBySchool(SchoolID, dic).Count());
            }
        }
        #endregion

        #region GetPupilPrimaryRetestToCategory
        public IQueryable<PupilPrimaryRetestToCategoryBO> GetPupilPrimaryRetestToCategory(IDictionary<string, object> dic)
        {


            int ClassID = Utils.GetInt(dic, "ClassID");
            int SchoolID = Utils.GetInt(dic, "SchoolID");
            int AcademicYearID = Utils.GetInt(dic, "AcademicYearID");
            int EduationLevelID = Utils.GetInt(dic, "EduationLevelID");
            int? Semester = SEMESTER_OF_YEAR_SECOND;

            var querry = from prr in this.All
                         join poc in PupilOfClassRepository.All on prr.PupilID equals poc.PupilID
                         join pr in PupilRankingRepository.All on prr.PupilID equals pr.PupilID
                         join cp in PupilProfileRepository.All on prr.PupilID equals cp.PupilProfileID
                         join sur in SummedUpRecordRepository.All on prr.PupilID equals sur.PupilID
                         join mr in MarkRecordRepository.All on prr.PupilID equals mr.PupilID
                         join jr in JudgeRecordRepository.All on prr.PupilID equals jr.PupilID

                         select new PupilPrimaryRetestToCategoryBO
                         {
                             Semester = prr.Semester,
                             ClassID = prr.ClassID,
                             ClassName = prr.ClassProfile.DisplayName,
                             SubjectID = prr.SubjectID,
                             EducationLevelID = poc.ClassProfile.AcademicYearID,
                             SchoolID = prr.SchoolID,
                             AcademicYearID = prr.AcademicYearID,
                             PupilID = prr.PupilID,
                             FullName = cp.FullName,
                             PupilCode = cp.PupilCode,
                             BirthDate = cp.BirthDate,
                             Genre = cp.Genre,
                             StudyingJudgementID = pr.StudyingJudgementID,
                             ConductLevel = pr.ConductLevelID

                         };

            if (ClassID != 0)
            {
                querry = querry.Where(o => o.ClassID == ClassID);
            }
            if (EduationLevelID != 0)
            {
                querry = querry.Where(o => o.EducationLevelID == EduationLevelID);
            }

            if (SchoolID != 0)
            {
                querry = querry.Where(o => o.SchoolID == SchoolID);
            }

            if (Semester != 0)
            {
                querry = querry.Where(o => o.Semester == Semester);
            }

            return (querry);


        }

        #endregion

        #region CheckNumberOfRetest
        public bool CheckNumberOfRetest(int AcademicYearID, int SchoolID, int ClassID, int PupilID)
        {
            IQueryable<SummedUpRecord> listSummedUpRecord = SummedUpRecordRepository.All.Where(sur => sur.PupilID == PupilID && sur.ClassID == ClassID && sur.SchoolID == SchoolID && sur.AcademicYearID == AcademicYearID);
            //int dem = listSummedUpRecord.Count();

            //IQueryable<PupilRetestRegistration> listPupilRetestRegistration = this.All.Where(sur => sur.PupilID == PupilID && sur.SchoolID == SchoolID && sur.AcademicYearID == AcademicYearID).Distinct();
            //int dem1 = listPupilRetestRegistration.Count();
            List<int> lstSubjectID = this.All.Where(sur => sur.PupilID == PupilID && sur.SchoolID == SchoolID && sur.AcademicYearID == AcademicYearID).Select(o => o.SubjectID).Distinct().ToList();
            if (lstSubjectID.Count() == 0) return false;
            foreach (int SubjectID in lstSubjectID)
            {
                Dictionary<string, object> dic = new Dictionary<string, object>();
                dic["PupilID"] = PupilID;
                dic["SubjectID"] = SubjectID;
                dic["AcademicYearID"] = AcademicYearID;
                dic["SchoolID"] = SchoolID;
                dic["ClassID"] = ClassID;
                IQueryable<PupilRetestRegistration> listPRR = this.SearchPupilRetestRegistration(dic).Where(o => o.ModifiedDate != null);
                //Kiểm tra điểm có lớn hơn 5 hoặc là chưa đạt hay không
                SummedUpRecord summedUpRecord = listSummedUpRecord.Where(o => o.SubjectID == SubjectID && o.Semester == SystemParamsInFile.SEMESTER_OF_YEAR_RETEST).FirstOrDefault();
                if ((summedUpRecord == null) || (listPRR.Count() < 3 && summedUpRecord != null && ((summedUpRecord.ReTestMark != null && summedUpRecord.ReTestMark < 5) || (summedUpRecord.ReTestJudgement != null && summedUpRecord.ReTestJudgement.ToString() == "B"))))
                {
                    return (false);
                }
            }
            return true;
        }
        #endregion

        #region CheckNumberOfRetestSecondary
        public bool CheckNumberOfRetestSecondary(int AcademicYearID, int SchoolID, int ClassID, int PupilID)
        {
            //List<int> lstSubjectID = this.All.Where(sur => sur.PupilID == PupilID && sur.SchoolID == SchoolID
            //    && sur.AcademicYearID == AcademicYearID).Select(o => o.SubjectID).Distinct().ToList();
            //if (lstSubjectID.Count() == 0)
            //{ return false; }


            Dictionary<string, object> dic = new Dictionary<string, object>();
            dic["PupilID"] = PupilID;
            dic["AcademicYearID"] = AcademicYearID;
            dic["SchoolID"] = SchoolID;
            dic["ClassID"] = ClassID;
            bool check = this.SearchPupilRetestRegistration(dic).Any(o => o.ModifiedDate == null);
            //bool check = listPRR.Any(o => lstSubjectID.Contains(o.SubjectID));
            return !check;
        }
        #endregion

        public List<PupilRetestToCategory> CategoryPupilAfterRetest(IEnumerable<int> lstClass, int SchoolID, int AcademicYearID)
        {
            var lstPrr = this.PupilRetestRegistrationRepository.All
                                        .Where(u => u.SchoolID == SchoolID
                                                    && u.AcademicYearID == AcademicYearID
                                                    && lstClass.Contains(u.ClassID));
            AcademicYear academicYear = AcademicYearBusiness.Find(AcademicYearID);
            int year = academicYear.Year;
            int modSchoolID = SchoolID % 100;
            var lstPr = lstPrr.Select(u => new { u.SchoolID, u.AcademicYearID, u.ClassID, u.PupilID }).Distinct()
                                .Join(PupilRankingBusiness.All
                                            .Where(u => u.CreatedAcademicYear == year && u.Last2digitNumberSchool == modSchoolID && u.SchoolID == SchoolID && u.AcademicYearID == AcademicYearID && lstClass.Contains(u.ClassID) && u.Semester >= SystemParamsInFile.SEMESTER_OF_YEAR_ALL),
                                                        u => new { u.PupilID, u.ClassID },
                                                        v => new { v.PupilID, v.ClassID },
                                                        (u, v) => v);

            var lstPrMax = lstPr.Where(u => u.Semester == lstPr.Where(v => v.PupilID == u.PupilID && v.ClassID == u.ClassID).Max(v => v.Semester)).ToList();
            var lstPrSemesterAll = lstPr.Where(o => o.Semester == SystemParamsInFile.SEMESTER_OF_YEAR_ALL).ToList();


            var lstSur = lstPrr.Select(u => new { u.SchoolID, u.AcademicYearID, u.ClassID, u.PupilID }).Distinct()
                                .Join(SummedUpRecordBusiness.All
                                            .Where(u => u.SchoolID == SchoolID && u.AcademicYearID == AcademicYearID
                                                        && !u.SubjectCat.IsApprenticeshipSubject
                                                        && lstClass.Contains(u.ClassID)
                                                        && u.Semester >= SystemParamsInFile.SEMESTER_OF_YEAR_ALL),
                                                        u => new { u.PupilID, u.ClassID },
                                                        v => new { v.PupilID, v.ClassID },
                                                        (u, v) => v);
            List<SummedUpRecord> lstSurMax = new List<SummedUpRecord>();
            if (lstSur != null && lstSur.Count() != 0)
            {
                lstSurMax = lstSur.Where(u => u.Semester == lstSur.Where(v => v.PupilID == u.PupilID
                                                                                        && v.ClassID == u.ClassID
                                                                                        && v.SubjectID == u.SubjectID).Max(v => v.Semester)).ToList();
            }
            List<PupilRetestToCategory> lstPrtc = new List<PupilRetestToCategory>();

            var pupils = (from prr in lstPrr.Where(u => u.PupilProfile.IsActive)
                          //AnhVD 20140724 - Join Kiem tra du lieu
                          join pr in VPupilRankingBusiness.All.Where(u => u.Last2digitNumberSchool == modSchoolID && u.CreatedAcademicYear == year && u.SchoolID == SchoolID && u.AcademicYearID == AcademicYearID && lstClass.Contains(u.ClassID) &&
                              u.Semester == SystemParamsInFile.SEMESTER_OF_YEAR_ALL && u.StudyingJudgementID == SystemParamsInFile.STUDYING_JUDGEMENT_RETEST)
                                          on new { prr.SchoolID, prr.AcademicYearID, prr.ClassID, prr.PupilID, prr.Semester } equals new { pr.SchoolID, pr.AcademicYearID, pr.ClassID, pr.PupilID, pr.Semester }
                          select (new
                          {
                              prr.SchoolID,
                              prr.AcademicYearID,
                              prr.ClassID,
                              prr.PupilID,
                              prr.PupilProfile.PupilCode,
                              prr.PupilProfile.FullName,
                              prr.PupilProfile.Name,
                              prr.ClassProfile.DisplayName
                          })).Distinct().ToList();
            var lstCS = this.ClassSubjectBusiness.SearchBySchool(SchoolID, new Dictionary<string, object> { }).Where(u => lstClass.Contains(u.ClassID)).ToList();

            List<ExemptedSubject> lstExemptedI = ExemptedSubjectBusiness.GetListExemptedSubject(lstClass, SystemParamsInFile.SEMESTER_OF_YEAR_FIRST).ToList();
            List<ExemptedSubject> lstExemptedII = ExemptedSubjectBusiness.GetListExemptedSubject(lstClass, SystemParamsInFile.SEMESTER_OF_YEAR_SECOND).ToList();
            PupilRanking prrSemesterAll = new PupilRanking();
            foreach (var pupil in pupils)
            {
                PupilRetestToCategory prtc = new PupilRetestToCategory();
                prtc.PupilID = pupil.PupilID;
                prtc.PupilCode = pupil.PupilCode;
                prtc.FullName = pupil.FullName;
                prtc.ClassID = pupil.ClassID;
                prtc.pr = lstPrMax.SingleOrDefault(u => u.PupilID == pupil.PupilID && u.ClassID == pupil.ClassID);
                //prrSemesterAll = lstPrSemesterAll.Where(u => u.PupilID == pupil.PupilID && u.ClassID == pupil.ClassID).FirstOrDefault();
                //if (prrSemesterAll != null)
                //{
                //    prtc.pr.ConductLevelID = prrSemesterAll.ConductLevelID;
                //    prtc.pr.ConductLevel = prrSemesterAll.ConductLevel;
                //}
                prtc.lstMark = new List<string>();
                prtc.lstPupiRetestRegistration = lstPrr.Where(u => u.PupilID == pupil.PupilID && u.ClassID == pupil.ClassID).ToList();
                prtc.lstSummedUpRecord = lstSurMax.Where(u => u.PupilID == pupil.PupilID && u.ClassID == pupil.ClassID).ToList();
                prtc.Name = pupil.Name;
                prtc.ClassName = pupil.DisplayName;
                for (int j = 0; j < prtc.lstSummedUpRecord.Count; j++)
                {
                    PupilRetestRegistration pupilRetest = prtc.lstPupiRetestRegistration.FirstOrDefault(u => u.PupilID == prtc.lstSummedUpRecord[j].PupilID && u.SubjectID == prtc.lstSummedUpRecord[j].SubjectID);
                    ClassSubject cs = lstCS.SingleOrDefault(u => u.SubjectID == prtc.lstSummedUpRecord[j].SubjectID && u.ClassID == prtc.lstSummedUpRecord[j].ClassID);
                    if (cs == null)
                    {
                        List<object> Params = new List<object>();
                        Params.Add(prtc.lstSummedUpRecord[j].SubjectCat.DisplayName);
                        Params.Add(prtc.lstSummedUpRecord[j].ClassProfile.DisplayName);
                        throw new BusinessException("Common_Validate_NotSubject", Params);
                    }
                    string strMark = string.Empty;
                    if (pupilRetest != null)
                    {
                        //Mon dang ky thi lai
                        if (pupilRetest.IsAbsentContest.HasValue && pupilRetest.IsAbsentContest.Value)
                        {
                            strMark = "";
                        }
                        else
                        {
                            strMark = pupilRetest.ModifiedDate.HasValue
                                                ? (cs.IsCommenting == SystemParamsInFile.ISCOMMENTING_TYPE_JUDGE
                                                        ? prtc.lstSummedUpRecord[j].ReTestJudgement
                                                        : prtc.lstSummedUpRecord[j].ReTestMark.HasValue ? prtc.lstSummedUpRecord[j].ReTestMark.Value.ToString("0.#") : "")
                                                : "CN";
                        }
                    }
                    else
                    {
                        //Mon khong dang ky thi lai
                        if (lstExemptedI.Where(u => u.ClassID == pupil.ClassID).Any(u => u.PupilID == pupil.PupilID && u.SubjectID == cs.SubjectID)
                            || lstExemptedII.Where(u => u.ClassID == pupil.ClassID).Any(u => u.PupilID == pupil.PupilID && u.SubjectID == cs.SubjectID))
                        {
                            //Mon duoc mien giam
                            strMark = "MG";
                        }
                        else
                        {
                            //Mon khong thi lai && khong mien giam
                            strMark = cs.IsCommenting == SystemParamsInFile.ISCOMMENTING_TYPE_JUDGE
                                            ? prtc.lstSummedUpRecord[j].JudgementResult
                                            : prtc.lstSummedUpRecord[j].SummedUpMark.HasValue ? prtc.lstSummedUpRecord[j].SummedUpMark.Value.ToString("0.#") : "";
                        }
                    }
                    prtc.lstMark.Add(strMark);
                }
                lstPrtc.Add(prtc);
            }
            Dictionary<string, object> searchPOC = new Dictionary<string, object>();
            searchPOC["AcademicYearID"] = AcademicYearID;
            if (lstClass.Count() == 1)
            {
                searchPOC["Check"] = "Check";
            }
            else
            {
                searchPOC["CheckWithClass"] = "Check";
            }
            var iqPupilOfClass = PupilOfClassBusiness.SearchBySchool(SchoolID, searchPOC).Where(o => lstClass.Contains(o.ClassID));

            var lstPrtc2 = from su in lstPrtc
                           join poc in iqPupilOfClass on new { su.PupilID, su.ClassID } equals new { poc.PupilID, poc.ClassID }
                           where (poc.Status == SystemParamsInFile.PUPIL_STATUS_STUDYING)
                           orderby lstClass.Count() == 1 ? poc.OrderInClass : 0, su.Name, su.FullName
                           select su;

            return lstPrtc2.ToList();
        }

        /// <summary>
        /// Danh sach chi tiet HS sau thi lai
        /// </summary>
        /// <param name="lstClass"></param>
        /// <param name="SchoolID"></param>
        /// <param name="AcademicYearID"></param>
        /// <returns></returns>
        public List<PupilRetestHistoryToCategory> CategoryPupilAfterRetestHistory(IEnumerable<int> lstClass, int SchoolID, int AcademicYearID)
        {
            // Danh sách HS có đăng ký thi lại
            var lstPrr = this.PupilRetestRegistrationRepository.All
                                        .Where(u => u.SchoolID == SchoolID
                                                    && u.AcademicYearID == AcademicYearID
                                                    && lstClass.Contains(u.ClassID));
            AcademicYear academicYear = AcademicYearBusiness.Find(AcademicYearID);
            int year = academicYear.Year;
            int modSchoolID = SchoolID % 100;

            // Danh sách bao gồm cả xếp loại cả năm + sau thi lại (nếu có)
            var lstPr = lstPrr.Select(u => new { u.SchoolID, u.AcademicYearID, u.ClassID, u.PupilID }).Distinct()
                                .Join(PupilRankingHistoryBusiness.All
                                            .Where(u => u.CreatedAcademicYear == year && u.Last2digitNumberSchool == modSchoolID && u.SchoolID == SchoolID && u.AcademicYearID == AcademicYearID && lstClass.Contains(u.ClassID)
                                                && u.Semester >= SystemParamsInFile.SEMESTER_OF_YEAR_ALL),
                                                        u => new { u.PupilID, u.ClassID },
                                                        v => new { v.PupilID, v.ClassID },
                                                        (u, v) => v);
            // Danh sách xếp loại sau cùng
            var lstPrMax = lstPr.Where(u => u.Semester == lstPr.Where(v => v.PupilID == u.PupilID && v.ClassID == u.ClassID).Max(v => v.Semester)).ToList();
            // Danh sách xếp loại cả năm
            var lstPrSemesterAll = lstPr.Where(o => o.Semester == SystemParamsInFile.SEMESTER_OF_YEAR_ALL).ToList();


            var lstSur = lstPrr.Select(u => new { u.SchoolID, u.AcademicYearID, u.ClassID, u.PupilID }).Distinct()
                                .Join(SummedUpRecordHistoryBusiness.All
                                            .Where(u => u.SchoolID == SchoolID && u.AcademicYearID == AcademicYearID
                                                        && !u.SubjectCat.IsApprenticeshipSubject
                                                        && lstClass.Contains(u.ClassID)
                                                        && u.Semester >= SystemParamsInFile.SEMESTER_OF_YEAR_ALL),
                                                        u => new { u.PupilID, u.ClassID },
                                                        v => new { v.PupilID, v.ClassID },
                                                        (u, v) => v);
            List<SummedUpRecordHistory> lstSurMax = new List<SummedUpRecordHistory>();
            if (lstSur != null && lstSur.Count() != 0)
            {
                lstSurMax = lstSur.Where(u => u.Semester == lstSur.Where(v => v.PupilID == u.PupilID
                                                                                        && v.ClassID == u.ClassID
                                                                                        && v.SubjectID == u.SubjectID).Max(v => v.Semester)).ToList();
            }
            List<PupilRetestHistoryToCategory> lstPrtc = new List<PupilRetestHistoryToCategory>();

            var pupils = (from prr in lstPrr.Where(u => u.PupilProfile.IsActive)
                          //AnhVD 20140724 - Join Kiem tra du lieu
                          join pr in VPupilRankingBusiness.All.Where(u => u.Last2digitNumberSchool == modSchoolID && u.CreatedAcademicYear == year && u.SchoolID == SchoolID && u.AcademicYearID == AcademicYearID && lstClass.Contains(u.ClassID) &&
                              u.Semester == SystemParamsInFile.SEMESTER_OF_YEAR_ALL && u.StudyingJudgementID == SystemParamsInFile.STUDYING_JUDGEMENT_RETEST)
                                          on new { prr.SchoolID, prr.AcademicYearID, prr.ClassID, prr.PupilID, prr.Semester } equals new { pr.SchoolID, pr.AcademicYearID, pr.ClassID, pr.PupilID, pr.Semester }
                          select (new
                          {
                              prr.SchoolID,
                              prr.AcademicYearID,
                              prr.ClassID,
                              prr.PupilID,
                              prr.PupilProfile.PupilCode,
                              prr.PupilProfile.FullName,
                              prr.PupilProfile.Name,
                              prr.ClassProfile.DisplayName
                          })).Distinct().ToList();

            var lstCS = this.ClassSubjectBusiness.SearchBySchool(SchoolID, new Dictionary<string, object> { }).Where(u => lstClass.Contains(u.ClassID)).ToList();

            List<ExemptedSubject> lstExemptedI = ExemptedSubjectBusiness.GetListExemptedSubject(lstClass, SystemParamsInFile.SEMESTER_OF_YEAR_FIRST).ToList();
            List<ExemptedSubject> lstExemptedII = ExemptedSubjectBusiness.GetListExemptedSubject(lstClass, SystemParamsInFile.SEMESTER_OF_YEAR_SECOND).ToList();
            // Hs xếp loại cả năm
            PupilRanking prrSemesterAll = new PupilRanking();

            // Danh sach xep loai ca nam
            foreach (var pupil in pupils)
            {
                PupilRetestHistoryToCategory prtc = new PupilRetestHistoryToCategory();
                prtc.PupilID = pupil.PupilID;
                prtc.PupilCode = pupil.PupilCode;
                prtc.FullName = pupil.FullName;
                prtc.ClassID = pupil.ClassID;
                // Pupil_Ranking sau cung
                prtc.pr = lstPrMax.FirstOrDefault(u => u.PupilID == pupil.PupilID && u.ClassID == pupil.ClassID);
                //prrSemesterAll = lstPrSemesterAll.Where(u => u.PupilID == pupil.PupilID && u.ClassID == pupil.ClassID).FirstOrDefault();
                //if (prrSemesterAll != null)
                //{
                //    prtc.pr.ConductLevelID = prrSemesterAll.ConductLevelID;
                //    prtc.pr.ConductLevel = prrSemesterAll.ConductLevel;
                //}
                prtc.lstMark = new List<string>();
                prtc.lstPupiRetestRegistration = lstPrr.Where(u => u.PupilID == pupil.PupilID && u.ClassID == pupil.ClassID).ToList();
                prtc.lstSummedUpRecordHistory = lstSurMax.Where(u => u.PupilID == pupil.PupilID && u.ClassID == pupil.ClassID).ToList();
                prtc.Name = pupil.Name;
                prtc.ClassName = pupil.DisplayName;
                for (int j = 0; j < prtc.lstSummedUpRecordHistory.Count; j++)
                {
                    PupilRetestRegistration pupilRetest = prtc.lstPupiRetestRegistration.FirstOrDefault(u => u.PupilID == prtc.lstSummedUpRecordHistory[j].PupilID && u.SubjectID == prtc.lstSummedUpRecordHistory[j].SubjectID);
                    ClassSubject cs = lstCS.SingleOrDefault(u => u.SubjectID == prtc.lstSummedUpRecordHistory[j].SubjectID && u.ClassID == prtc.lstSummedUpRecordHistory[j].ClassID);
                    if (cs == null)
                    {
                        List<object> Params = new List<object>();
                        Params.Add(prtc.lstSummedUpRecordHistory[j].SubjectCat.DisplayName);
                        Params.Add(prtc.lstSummedUpRecordHistory[j].ClassProfile.DisplayName);
                        throw new BusinessException("Common_Validate_NotSubject", Params);
                    }
                    string strMark = string.Empty;
                    if (pupilRetest != null)
                    {
                        //Mon dang ky thi lai
                        strMark = pupilRetest.ModifiedDate.HasValue
                                                ? (cs.IsCommenting == SystemParamsInFile.ISCOMMENTING_TYPE_JUDGE
                                                        ? prtc.lstSummedUpRecordHistory[j].ReTestJudgement
                                                        : prtc.lstSummedUpRecordHistory[j].ReTestMark.HasValue ? prtc.lstSummedUpRecordHistory[j].ReTestMark.Value.ToString("0.#") : "")
                                                : "CN";
                    }
                    else
                    {
                        //Mon khong dang ky thi lai
                        if (lstExemptedI.Where(u => u.ClassID == pupil.ClassID).Any(u => u.PupilID == pupil.PupilID && u.SubjectID == cs.SubjectID)
                            || lstExemptedII.Where(u => u.ClassID == pupil.ClassID).Any(u => u.PupilID == pupil.PupilID && u.SubjectID == cs.SubjectID))
                        {
                            //Mon duoc mien giam
                            strMark = "MG";
                        }
                        else
                        {
                            //Mon khong thi lai && khong mien giam
                            strMark = cs.IsCommenting == SystemParamsInFile.ISCOMMENTING_TYPE_JUDGE
                                            ? prtc.lstSummedUpRecordHistory[j].JudgementResult
                                            : prtc.lstSummedUpRecordHistory[j].SummedUpMark.HasValue ? prtc.lstSummedUpRecordHistory[j].SummedUpMark.Value.ToString("0.#") : "";
                        }
                    }
                    prtc.lstMark.Add(strMark);
                }
                lstPrtc.Add(prtc);
            }
            Dictionary<string, object> searchPOC = new Dictionary<string, object>();
            searchPOC["AcademicYearID"] = AcademicYearID;
            if (lstClass.Count() == 1)
            {
                searchPOC["Check"] = "Check";
            }
            else
            {
                searchPOC["CheckWithClass"] = "Check";
            }
            var iqPupilOfClass = PupilOfClassBusiness.SearchBySchool(SchoolID, searchPOC).Where(o => lstClass.Contains(o.ClassID));

            var lstPrtc2 = from su in lstPrtc
                           join poc in iqPupilOfClass on new { su.PupilID, su.ClassID } equals new { poc.PupilID, poc.ClassID }
                           where (poc.Status == SystemParamsInFile.PUPIL_STATUS_STUDYING)
                           orderby lstClass.Count() == 1 ? poc.OrderInClass : 0, su.Name, su.FullName
                           select su;

            return lstPrtc2.ToList();
        }

        //Tamhm1 - 07/01/2014
        //Lay du lieu lich su
        public List<PupilRetestToCategory> CategoryPupilAfterRetest_History(IEnumerable<int> lstClass, int SchoolID, int AcademicYearID, bool? isVNEN = false)
        {

            var lstPrr = (from p in this.PupilRetestRegistrationRepository.All
                          join cp in ClassProfileBusiness.All on p.ClassID equals cp.ClassProfileID
                          where p.SchoolID == SchoolID
                          && p.AcademicYearID == AcademicYearID
                          && lstClass.Contains(p.ClassID)
                          && (isVNEN == true && (!cp.IsVnenClass.HasValue || (cp.IsVnenClass.HasValue && cp.IsVnenClass.Value == false)))
                          && cp.IsActive.Value
                          select p);
            //AnhVD 20140724 - Commment
            //List<PupilRetestRegistration> lstPrrAll = lstPrr.ToList();
            int modSchoolID = SchoolID % 100;
            AcademicYear ac = AcademicYearBusiness.Find(AcademicYearID);

            var lstPr = from p in lstPrr.Select(u => new { u.SchoolID, u.AcademicYearID, u.ClassID, u.PupilID }).Distinct()
                        join q in VPupilRankingBusiness.All.Where(u => u.Last2digitNumberSchool == modSchoolID && u.CreatedAcademicYear == ac.Year && u.SchoolID == SchoolID && u.AcademicYearID == AcademicYearID && lstClass.Contains(u.ClassID) && u.Semester >= SystemParamsInFile.SEMESTER_OF_YEAR_ALL)
                        on new { p.PupilID, p.ClassID } equals new { q.PupilID, q.ClassID }
                        join r in ConductLevelBusiness.All on q.ConductLevelID equals r.ConductLevelID into g1
                        from j1 in g1.DefaultIfEmpty()
                        join s in CapacityLevelBusiness.All on q.CapacityLevelID equals s.CapacityLevelID into g2
                        from j2 in g2.DefaultIfEmpty()
                        select new PupilRankingBO
                        {
                            PupilRankingID = q.PupilRankingID,
                            AverageMark = q.AverageMark,
                            PupilID = q.PupilID,
                            ConductLevelID = q.ConductLevelID,
                            CapacityLevelID = q.CapacityLevelID,
                            ConductLevelName = j1.Resolution,
                            CapacityLevel = j2.CapacityLevel1,
                            ClassID = q.ClassID,
                            SchoolID = q.SchoolID,
                            AcademicYearID = q.AcademicYearID,
                            Semester = q.Semester
                        };

            var lstPrMax = lstPr.Where(u => u.Semester == lstPr.Where(v => v.PupilID == u.PupilID && v.ClassID == u.ClassID).Max(v => v.Semester)).ToList();
            var lstPrSemesterAll = lstPr.Where(o => o.Semester == SystemParamsInFile.SEMESTER_OF_YEAR_ALL).ToList();

            var lstSur = from p in lstPrr.Select(u => new { u.SchoolID, u.AcademicYearID, u.ClassID, u.PupilID }).Distinct()
                         join q in VSummedUpRecordBusiness.All.Where(u => u.SchoolID == SchoolID && u.AcademicYearID == AcademicYearID &&
                             lstClass.Contains(u.ClassID) && u.Semester >= SystemParamsInFile.SEMESTER_OF_YEAR_ALL && u.Last2digitNumberSchool == modSchoolID && u.CreatedAcademicYear == ac.Year) on new { p.PupilID, p.ClassID } equals new { q.PupilID, q.ClassID }
                         join r in SubjectCatBusiness.All on q.SubjectID equals r.SubjectCatID
                         join c in ClassProfileBusiness.All on q.ClassID equals c.ClassProfileID
                         where !r.IsApprenticeshipSubject
                         && c.IsActive.Value
                         select new SummedUpRecordBO
                         {
                             SummedUpRecordID = q.SummedUpRecordID,
                             AcademicYearID = q.AcademicYearID,
                             SchoolID = q.SchoolID,
                             ClassID = q.ClassID,
                             PupilID = q.PupilID,
                             ClassName = c.DisplayName,
                             SubjectID = q.SubjectID,
                             SubjectName = r.DisplayName,
                             SummedUpMark = q.SummedUpMark,
                             ReTestMark = q.ReTestMark,
                             ReTestJudgement = q.ReTestJudgement,
                             JudgementResult = q.JudgementResult,
                             Semester = q.Semester
                         };

            // Danh sách tổng kết điểm cuối cùng
            List<SummedUpRecordBO> lstSurMax = new List<SummedUpRecordBO>();
            if (lstSur != null && lstSur.Count() != 0)
            {
                lstSurMax = lstSur.Where(u => u.Semester == lstSur.Where(v => v.PupilID == u.PupilID
                                                                                        && v.ClassID == u.ClassID
                                                                                        && v.SubjectID == u.SubjectID)
                                    .Max(v => v.Semester)).ToList();
            }
            List<PupilRetestToCategory> lstPrtc = new List<PupilRetestToCategory>();

            var pupils = (from prr in lstPrr.Where(u => u.PupilProfile.IsActive)
                          //AnhVD 20140724 - Join Kiem tra du lieu
                          join pr in VPupilRankingBusiness.All.Where(u => u.Last2digitNumberSchool == modSchoolID && u.CreatedAcademicYear == ac.Year && u.SchoolID == SchoolID && u.AcademicYearID == AcademicYearID && lstClass.Contains(u.ClassID) &&
                              u.Semester == SystemParamsInFile.SEMESTER_OF_YEAR_ALL && u.StudyingJudgementID == SystemParamsInFile.STUDYING_JUDGEMENT_RETEST)
                                          on new { prr.SchoolID, prr.AcademicYearID, prr.ClassID, prr.PupilID, prr.Semester } equals new { pr.SchoolID, pr.AcademicYearID, pr.ClassID, pr.PupilID, pr.Semester }
                          select (new
                          {
                              prr.SchoolID,
                              prr.AcademicYearID,
                              prr.ClassID,
                              prr.PupilID,
                              prr.PupilProfile.PupilCode,
                              prr.PupilProfile.FullName,
                              prr.PupilProfile.Name,
                              prr.ClassProfile.DisplayName
                          })).Distinct().ToList();

            var lstCS = this.ClassSubjectBusiness.SearchBySchool(SchoolID, new Dictionary<string, object> { }).Where(u => lstClass.Contains(u.ClassID)).ToList();

            List<ExemptedSubject> lstExemptedI = ExemptedSubjectBusiness.GetListExemptedSubject(lstClass, SystemParamsInFile.SEMESTER_OF_YEAR_FIRST).ToList();
            List<ExemptedSubject> lstExemptedII = ExemptedSubjectBusiness.GetListExemptedSubject(lstClass, SystemParamsInFile.SEMESTER_OF_YEAR_SECOND).ToList();
            VPupilRanking prrSemesterAll = new VPupilRanking();
            foreach (var pupil in pupils)
            {
                PupilRetestToCategory prtc = new PupilRetestToCategory();
                prtc.PupilID = pupil.PupilID;
                prtc.PupilCode = pupil.PupilCode;
                prtc.FullName = pupil.FullName;
                prtc.ClassID = pupil.ClassID;
                prtc.Vpr = lstPrMax.FirstOrDefault(u => u.PupilID == pupil.PupilID && u.ClassID == pupil.ClassID);
                prtc.lstMark = new List<string>();
                prtc.lstPupiRetestRegistration = lstPrr.Where(u => u.PupilID == pupil.PupilID && u.ClassID == pupil.ClassID).ToList();
                prtc.lstSummedUpRecordBO = lstSurMax.Where(u => u.PupilID == pupil.PupilID && u.ClassID == pupil.ClassID).ToList();
                prtc.Name = pupil.Name;
                prtc.ClassName = pupil.DisplayName;
                for (int j = 0; j < prtc.lstSummedUpRecordBO.Count; j++)
                {
                    PupilRetestRegistration pupilRetest = prtc.lstPupiRetestRegistration.FirstOrDefault(u => u.PupilID == prtc.lstSummedUpRecordBO[j].PupilID && u.SubjectID == prtc.lstSummedUpRecordBO[j].SubjectID);
                    ClassSubject cs = lstCS.SingleOrDefault(u => u.SubjectID == prtc.lstSummedUpRecordBO[j].SubjectID && u.ClassID == prtc.lstSummedUpRecordBO[j].ClassID);
                    if (cs == null)
                    {
                        List<object> Params = new List<object>();
                        Params.Add(prtc.lstSummedUpRecordBO[j].SubjectName);
                        Params.Add(prtc.lstSummedUpRecordBO[j].ClassName);
                        throw new BusinessException("Common_Validate_NotSubject", Params);
                    }
                    string strMark = string.Empty;
                    if (pupilRetest != null)
                    {
                        //Mon dang ky thi lai
                        strMark = (prtc.lstSummedUpRecordBO[j].ReTestMark.HasValue || !string.IsNullOrEmpty(prtc.lstSummedUpRecordBO[j].ReTestJudgement))
                                                ? (cs.IsCommenting == SystemParamsInFile.ISCOMMENTING_TYPE_JUDGE
                                                        ? prtc.lstSummedUpRecordBO[j].ReTestJudgement
                                                        : prtc.lstSummedUpRecordBO[j].ReTestMark.HasValue ? prtc.lstSummedUpRecordBO[j].ReTestMark.Value.ToString("0.#") : "")
                                                : "CN";
                    }
                    else
                    {
                        //Mon khong dang ky thi lai
                        if (lstExemptedI.Where(u => u.ClassID == pupil.ClassID).Any(u => u.PupilID == pupil.PupilID && u.SubjectID == cs.SubjectID)
                            && lstExemptedII.Where(u => u.ClassID == pupil.ClassID).Any(u => u.PupilID == pupil.PupilID && u.SubjectID == cs.SubjectID))
                        {
                            //Mon duoc mien giam
                            strMark = "MG";
                        }
                        else
                        {
                            //Mon khong thi lai && khong mien giam
                            strMark = cs.IsCommenting == SystemParamsInFile.ISCOMMENTING_TYPE_JUDGE
                                            ? prtc.lstSummedUpRecordBO[j].JudgementResult
                                            : prtc.lstSummedUpRecordBO[j].SummedUpMark.HasValue ? prtc.lstSummedUpRecordBO[j].SummedUpMark.Value.ToString("0.#") : "";
                        }
                    }
                    prtc.lstMark.Add(strMark);
                }
                lstPrtc.Add(prtc);
            }
            Dictionary<string, object> searchPOC = new Dictionary<string, object>();
            searchPOC["AcademicYearID"] = AcademicYearID;
            if (lstClass.Count() == 1)
            {
                searchPOC["Check"] = "Check";
            }
            else
            {
                searchPOC["CheckWithClass"] = "Check";
            }
            var iqPupilOfClass = PupilOfClassBusiness.SearchBySchool(SchoolID, searchPOC).Where(o => lstClass.Contains(o.ClassID));

            var lstPrtc2 = from su in lstPrtc
                           join poc in iqPupilOfClass on new { su.PupilID, su.ClassID } equals new { poc.PupilID, poc.ClassID }
                           where (poc.Status == SystemParamsInFile.PUPIL_STATUS_STUDYING)
                           orderby lstClass.Count() == 1 ? poc.OrderInClass : 0, su.Name, su.FullName
                           select su;

            return lstPrtc2.ToList();
        }

        #region Lấy mảng băm cho các tham số đầu vào của danh sách đăng ký thi lại
        public string GetHashKey(PupilRetestRegistrationBO entity)
        {
            IDictionary<string, object> dic = new Dictionary<string, object> {
                {"AcademicYearID", entity.AcademicYearID},
                {"EducationLevelID ", entity.EducationLevelID},
                {"ClassID", entity.ClassID},
                {"SchoolID", entity.SchoolID},
                {"SubjectID", entity.SubjectID},               
                {"Semester", entity.Semester},
                {"AppliedLevel", entity.AppliedLevel}
            };
            return ReportUtils.GetHashKey(dic);
        }
        #endregion

        #region Lấy danh sách khen thưởng được cập nhật mới nhất
        public ProcessedReport GetPupilRetest(PupilRetestRegistrationBO entity)
        {
            string reportCode = SystemParamsInFile.REPORT_DS_HS_DANG_KY_THI_LAI;
            string inputParameterHashKey = GetHashKey(entity);
            return ProcessedReportBusiness.GetProcessedReport(reportCode, inputParameterHashKey);
        }
        #endregion

        #region Lưu lại thông tin danh sách đăng ký thi lai
        public ProcessedReport InsertPupilRetestRegistrationSecondary(PupilRetestRegistrationBO entity, Stream data)
        {
            string reportCode = SystemParamsInFile.REPORT_DS_HS_DANG_KY_THI_LAI;
            ReportDefinition reportDef = ReportDefinitionBusiness.GetByCode(reportCode);
            ProcessedReport pr = new ProcessedReport();
            pr.ReportCode = reportCode;
            pr.ProcessedDate = DateTime.Now;
            pr.InputParameterHashKey = GetHashKey(entity);
            pr.ReportData = ReportUtils.Compress(data);

            //Tạo tên file
            string outputNamePattern = reportDef.OutputNamePattern;
            string appliedLevel = ReportUtils.ConvertAppliedLevelForReportName(entity.AppliedLevel.Value);
            string educationName = entity.EducationLevelID > 0 ? "Khoi" + entity.EducationLevelID : "TatCa";
            ClassProfile Class = ClassProfileBusiness.Find(entity.ClassID);
            string className = Class != null ? Class.DisplayName : String.Empty;
            string EducationOrClass;
            if (!string.IsNullOrEmpty(className))
            {
                EducationOrClass = className;
            }
            else
            {
                EducationOrClass = educationName;
            }
            SubjectCat subject = SubjectCatBusiness.Find(entity.SubjectID.Value);
            string subjectName = "";
            if (subject != null)
            {
                subjectName = subject.DisplayName;
            }
            outputNamePattern = outputNamePattern.Replace("[SchoolLevel]", appliedLevel);
            string semester = ReportUtils.ConvertSemesterForReportName(entity.Semester.Value);
            outputNamePattern = outputNamePattern.Replace("[Subject]", subjectName);
            outputNamePattern = outputNamePattern.Replace("[Semester]", semester);
            outputNamePattern = outputNamePattern.Replace("[EducationOrClass]", EducationOrClass);

            pr.ReportName = ReportUtils.RemoveSpecialCharacters(outputNamePattern) + "." + reportDef.OutputFormat;

            IDictionary<string, object> dic = new Dictionary<string, object> {
                {"AcademicYearID", entity.AcademicYearID},
                {"EducationLevelID ", entity.EducationLevelID},
                {"ClassID", entity.ClassID},
                {"SchoolID", entity.SchoolID},
                {"SubjectID", entity.SubjectID},                
                {"Semester", entity.Semester},
            };
            ProcessedReportParameterRepository.Insert(dic, pr);
            ProcessedReportRepository.Insert(pr);
            ProcessedReportRepository.Save();
            return pr;
        }
        #endregion

        #region Tìm kiếm danh sách học sinh đăng ký thi lại
        public List<PupilRetestRegistrationBO> GetListPupilRetestRegistration(int AcademicYearID, int SubjectID, int Semester, int SchoolID)
        {
            Dictionary<string, object> dic = new Dictionary<string, object>();
            dic = new Dictionary<string, object>();
            dic["AcademicYearID"] = AcademicYearID;
            dic["SubjectID"] = SubjectID;
            List<PupilRetestRegistration> lstPupilRetestRegistration = PupilRetestRegistrationBusiness.SearchBySchool(SchoolID, dic).ToList();
            List<int> lstPupilID = lstPupilRetestRegistration.Select(p => p.PupilID).Distinct().ToList();

            dic = new Dictionary<string, object>();
            dic["AcademicYearID"] = AcademicYearID;
            dic["lstPupilID"] = lstPupilID;
            IQueryable<PupilOfClass> lsPupilOfClassSemester1 = PupilOfClassBusiness.SearchBySchool(SchoolID, dic)
                                                               .AddCriteriaSemester(AcademicYearBusiness.Find(AcademicYearID), SystemParamsInFile.SEMESTER_OF_YEAR_SECOND);
            List<PupilOfClass> lsPupilOfClassSemester = lsPupilOfClassSemester1.Where(o => o.AssignedDate == lsPupilOfClassSemester1.Where(u => u.PupilID == o.PupilID && u.ClassID == o.ClassID).Select(u => u.AssignedDate).Max()).ToList();


            dic = new Dictionary<string, object>();
            dic["AcademicYearID"] = AcademicYearID;
            dic["Semester"] = Semester;
            dic["ListPupilID"] = lstPupilID;
            dic["SubjectID"] = SubjectID;
            List<SummedUpRecordBO> lstSummedUpRecord = VSummedUpRecordBusiness.SearchBySchool(SchoolID, dic).Where(o => o.PeriodID == null)
                                                     .Select(p => new SummedUpRecordBO
                                                     {
                                                         PupilID = p.PupilID,
                                                         SubjectID = p.SubjectID,
                                                         JudgementResult = p.JudgementResult,
                                                         SummedUpMark = p.SummedUpMark
                                                     }).Distinct().ToList();



            List<PupilRetestRegistrationBO> lstResult = (from pc in lsPupilOfClassSemester
                                                         join prr in lstPupilRetestRegistration on pc.PupilID equals prr.PupilID
                                                         join sur in lstSummedUpRecord on prr.PupilID equals sur.PupilID
                                                         join sub in SubjectCatBusiness.All on sur.SubjectID equals sub.SubjectCatID
                                                         where prr.SubjectID == sur.SubjectID
                                                         select new PupilRetestRegistrationBO
                                                         {
                                                             ClassID = pc.ClassID,
                                                             ClassName = pc.ClassProfile.DisplayName,
                                                             ClassOrderNumber = pc.ClassProfile.OrderNumber,
                                                             FullName = pc.PupilProfile.FullName,
                                                             PupilCode = pc.PupilProfile.PupilCode,
                                                             BirthDate = pc.PupilProfile.BirthDate,
                                                             JudgementResult = sur.JudgementResult,
                                                             SummedUpMark = sur.SummedUpMark,
                                                             EducationLevelID = pc.ClassProfile.EducationLevelID,
                                                             OrderInClass = pc.OrderInClass,
                                                             IsCommenting = sub.IsCommenting,
                                                             GenreName = (pc.PupilProfile.Genre == (int)SystemParamsInFile.GENRE_MALE) ? "Nam" : "Nữ",
                                                         }).ToList();
            return lstResult;
        }
        #endregion

        #region Lưu lại thông tin danh sách học sinh đăng ký thi lại
        /// <summary>
        /// Lưu lại thông tin danh sách học sinh thi lại
        /// </summary>
        /// <param name="entity"></param>
        /// <returns></returns>
        public Stream CreatePupilRetestRegistrationSecondary(PupilRetestRegistrationBO entity)
        {
            string reportCode = SystemParamsInFile.REPORT_DS_HS_DANG_KY_THI_LAI;
            ReportDefinition reportDef = ReportDefinitionBusiness.GetByCode(reportCode);
            string templatePath = ReportUtils.GetTemplatePath(reportDef);
            IVTWorkbook oBook = VTExport.OpenWorkbook(templatePath);

            //Lấy sheet template
            IVTWorksheet firstSheet = oBook.GetSheet(1);

            AcademicYear academicYear = AcademicYearRepository.Find(entity.AcademicYearID);
            SchoolProfile school = SchoolProfileRepository.Find(entity.SchoolID);

            //Fill dữ liệu chung
            string schoolName = school.SchoolName.ToUpper();
            string SupervisingDeptName = UtilsBusiness.GetSupervisingDeptName(school.SchoolProfileID, entity.AppliedLevel.Value);
            firstSheet.SetCellValue("A2", SupervisingDeptName);
            firstSheet.SetCellValue("A3", schoolName);

            //Lấy danh sách các khối
            List<EducationLevel> lstEducationLevel = new List<EducationLevel>();
            if (entity.EducationLevelID != 0)
            {
                lstEducationLevel.Add(EducationLevelRepository.Find(entity.EducationLevelID));
            }
            else
            {
                lstEducationLevel = EducationLevelBusiness.GetByGrade(entity.AppliedLevel.Value).Where(o => o.IsLastYear == false).ToList();
            }
            if (entity.EducationLevelID != 0)
            {
                lstEducationLevel = lstEducationLevel.Where(o => o.EducationLevelID == entity.EducationLevelID).ToList();
            }

            if (!entity.SubjectID.HasValue)
            {
                throw new BusinessException("Chưa chọn môn học");
            }
            //Lấy danh sách học sinh đăng ký thi lại            
            List<PupilRetestRegistrationBO> lstPupilRetestRegistration = this.GetListPupilRetestRegistration(entity.AcademicYearID.Value, (int)entity.SubjectID.Value, entity.Semester.Value, entity.SchoolID.Value).ToList();

            if (entity.EducationLevelID != 0)
            {
                lstPupilRetestRegistration = lstPupilRetestRegistration.Where(o => o.EducationLevelID == entity.EducationLevelID).ToList();
            }
            if (entity.ClassID != 0)
            {
                lstPupilRetestRegistration = lstPupilRetestRegistration.Where(o => o.ClassID == entity.ClassID).ToList();
            }

            int firstRow = 9;
            string colOrder = "A";
            string colFullName = "B";
            string colPupilCode = "C";
            string colClass = "D";
            string colBrithDate = "E";
            string colGene = "F";
            string colResult = "G";
            //string colNote = "H";
            //Tạo và fill dữ liệu vào các sheet tương ứng các khối
            foreach (EducationLevel educationLevel in lstEducationLevel)
            {
                //Tạo sheet
                IVTWorksheet sheet = oBook.CopySheetToLast(firstSheet, "H9");
                sheet.Name = educationLevel.Resolution;

                //Fill dữ liệu tiêu đề
                string title = "DANH SÁCH HỌC SINH ĐĂNG KÝ THI LẠI KHỐI " + educationLevel.Resolution.ToUpper().Replace("KHỐI", "")
                    + " NĂM HỌC " + academicYear.DisplayTitle;
                sheet.SetCellValue("A5", title);
                string SubjectName = SubjectCatBusiness.Find(entity.SubjectID).SubjectName;
                sheet.SetCellValue("A6", "Môn: " + SubjectName);
                int index = 0;
                IVTRange range = firstSheet.GetRange("A12", "H12");
                IVTRange lastRange = firstSheet.GetRange("A10", "H10");

                //Lấy danh sách lớp có có học sinh thi lại
                List<int> lstClass = (from s in lstPupilRetestRegistration
                                      where s.EducationLevelID == educationLevel.EducationLevelID
                                      orderby s.ClassOrderNumber.HasValue ? s.ClassOrderNumber : 0, s.ClassName
                                      select s.ClassID.GetValueOrDefault()).Distinct().ToList();

                //Fill dữ liệu học sinh cho từng lớp
                foreach (int cp in lstClass)
                {
                    //Lấy thông tin học sinh trong lớp
                    List<PupilRetestRegistrationBO> lstPE = (from s in lstPupilRetestRegistration
                                                             where s.ClassID == cp
                                                             select s).OrderBy(x => x.OrderInClass).ThenBy(x => x.FullName).ToList();

                    for (int i = 0; i < lstPE.Count; i++)
                    {
                        int currentRow = firstRow + index;
                        index++;
                        //Copy row style
                        sheet.CopyPasteSameSize(range, currentRow, 1);
                        PupilRetestRegistrationBO pe = lstPE[i];

                        //Fill dữ liệu
                        sheet.SetCellValue(colOrder + currentRow, index);
                        sheet.SetCellValue(colFullName + currentRow, pe.FullName);
                        sheet.SetCellValue(colPupilCode + currentRow, pe.PupilCode);
                        sheet.SetCellValue(colClass + currentRow, pe.ClassName);
                        sheet.SetCellValue(colBrithDate + currentRow, pe.BirthDate.HasValue ? pe.BirthDate.Value.ToString("dd/MM/yyyy") : "");
                        sheet.SetCellValue(colGene + currentRow, pe.GenreName);
                        if (pe.IsCommenting == SystemParamsInFile.ISCOMMENTING_TYPE_MARK || pe.IsCommenting == SystemParamsInFile.ISCOMMENTING_TYPE_MARK_JUDGE)
                        {
                            if (pe.SummedUpMark.HasValue)
                            {
                                sheet.SetCellValue(colResult + currentRow, pe.SummedUpMark);
                            }
                            else
                            {
                                if (pe.JudgementResult != null)
                                {
                                    sheet.SetCellValue(colResult + currentRow, pe.JudgementResult);
                                }
                            }
                        }
                        else
                        {
                            sheet.SetCellValue(colResult + currentRow, pe.JudgementResult);
                        }
                    }
                }
                if (index > 0)
                {
                    sheet.CopyPasteSameRowHeigh(lastRange, firstRow + index - 1, true);
                }
                else
                {
                    sheet.DeleteRow(firstRow);
                }
                sheet.ZoomPage = 80;
            }

            //Xoá sheet template
            firstSheet.Delete();

            return oBook.ToStream();
        }

        #endregion
    }
}
