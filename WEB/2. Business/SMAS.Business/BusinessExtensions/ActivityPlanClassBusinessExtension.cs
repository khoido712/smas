/**
 * The Viettel License
 *
 * Copyright 2012 Viettel Telecom. All rights reserved.
 * VIETTEL PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */

/** 
* @author  
* @version $Revision: $
*/

using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Linq.Expressions;
using System.Web;
using log4net;

using SMAS.DAL.Repository;
using SMAS.DAL.IRepository;
using SMAS.Business.IBusiness;
using SMAS.Models.Models;
using SMAS.Business.Common;

namespace SMAS.Business.Business
{ 
    public partial class ActivityPlanClassBusiness
    {
        public IQueryable<ActivityPlanClass> Search(IDictionary<string, object> dic)
        {
            int ActivityPlanClassID = Utils.GetInt(dic, "ActivityPlanClassID");
            int ActivityPlanID = Utils.GetInt(dic, "ActivityPlanID");
            int ClassID = Utils.GetInt(dic, "ClassID");
            IQueryable<ActivityPlanClass> Query = ActivityPlanClassRepository.All;
            if (ActivityPlanClassID != 0)
            {
                Query = Query.Where(evd => evd.ActivityPlanClassID == ActivityPlanClassID);
            }
            if (ActivityPlanID != 0)
            {
                Query = Query.Where(evd => evd.ActivityPlanID == ActivityPlanID);
            }
            if (ClassID != 0)
            {
                Query = Query.Where(evd => evd.ClassID == ClassID);
            }

            return Query;
        }
        
        public IQueryable<ClassProfile> GetListClass(int ActivityPlanID)
        {
           
              IQueryable<ClassProfile> query= from apr in ActivityPlanClassRepository.All
                      where apr.ActivityPlanID == ActivityPlanID
                      select apr.ClassProfile;
              return query;
        }

        public void Insert(List<ActivityPlanClass> lsActivityPlan)
        {
            foreach (ActivityPlanClass item in lsActivityPlan)
            {
                ValidationMetadata.ValidateObject(item);
                base.Insert(item);
            }
            this.Save();
        }

        public void Delete(int ActivityPlanID, List<int> lstClassID)
        {
            IQueryable<ActivityPlanClass> lst = from s in ActivityPlanClassRepository.All 
                                          where s.ActivityPlanID == ActivityPlanID 
                                          select s;
            foreach (ActivityPlanClass item in lst)
            {
                if (lstClassID.Contains(item.ClassID)) {
                    this.Delete(item.ActivityPlanClassID);
                }

            }
            this.Save();
        }
        


    }
}