﻿/**
 * The Viettel License
 *
 * Copyright 2012 Viettel Telecom. All rights reserved.
 * VIETTEL PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */

/** 
* @author  tungnd
* @version $Revision: $
*/

using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Linq.Expressions;
using System.Web;
using log4net;

using SMAS.DAL.Repository;
using SMAS.DAL.IRepository;
using SMAS.Business.IBusiness;
using SMAS.Models.Models;
using SMAS.Business.Common;

namespace SMAS.Business.Business
{
    public partial class PupilOfSchoolBusiness
    {
        private const int PUPIL_STATUS_STUDYING = 1;

        #region Validate

        public void Validate(PupilOfSchool pupilofschool)
        {
            ValidationMetadata.ValidateObject(pupilofschool);

        }

        #endregion

        #region InsertPupilOfSchool
        /// <summary>
        /// Thêm mới thông tin học sinh vào trường
        /// </summary>
        /// <author>tungnd</author>
        /// <date>9/10/2012</date>
        /// <param name="ethnic">Đối tượng PupilOfSchool</param>
        /// <returns>Đối tượng PupilOfSchool</returns>
        public void InsertPupilOfSchool(int UserID, PupilOfSchool PupilOfSchool)
        {
            // tìm kiếm thông tin trong bảng PupilProfile qua PupilID để tìm CurrentClassID
            PupilProfile PupilProfile = PupilProfileBusiness.Find(PupilOfSchool.PupilID);
            int ClassID = PupilProfile.CurrentClassID;
            if (ClassID == 0)
            {
                throw new BusinessException("Common_Validate_Null");
            }
            else
            {
                if (UtilsBusiness.HasHeadTeacherPermission(UserID, ClassID))
                {
                    base.Insert(PupilOfSchool);
                }
            }



        }

        #endregion

        #region UpdatePupilOfSchool

        /// <summary>
        /// Cập nhật thông tin học sinh vào trường
        /// </summary>
        /// <author>tungnd</author>
        /// <date>9/10/2012</date>
        /// <param name="ethnic">Đối tượng PupilOfSchool</param>
        /// <returns>Đối tượng PupilOfSchool</returns>

        public void UpdatePupilOfSchool(int UserID, PupilOfSchool PupilOfSchool)
        {

            // Validate giữ liệu
            Validate(PupilOfSchool);

            // tìm kiếm thông tin thời gian học kỳ 2 trong bảng AcademicYear để xem thời gian chuyển trường có phải học kỳ 2 hay không
            //AcademicYear AcademicYear = AcademicYearBusiness.Find(UpdatePupilOfSchool.AcademicYearID);
            //if (AcademicYear.SecondSemesterStartDate <= UpdatePupilOfSchool.EnrolmentDate && UpdatePupilOfSchool.EnrolmentDate <= AcademicYear.SecondSemesterEndDate)
            //{

            // tìm kiếm thông tin trong bảng PupilProfile qua PupilID để tìm CurrentClassID
            PupilProfile PupilProfile = PupilProfileBusiness.Find(PupilOfSchool.PupilID);
            int ClassID = PupilProfile.CurrentClassID;
            if (ClassID == 0)
            {
                throw new BusinessException("Common_Validate_Null");
            }
            else
            {
                if (UtilsBusiness.HasHeadTeacherPermission(UserID, ClassID))
                {
                    base.Update(PupilOfSchool);
                }
            }
            //   }


        }
        #endregion

        #region DeletePupilOfSchool
        /// <summary>
        /// Xoá thông tin học sinh trong trường
        /// </summary>
        /// <author>tungnd</author>
        /// <date>9/10/2012</date>
        /// <param name="ethnic">Đối tượng PupilOfSchool</param>


        public void DeletePupilOfSchool(int PupilOfSchoolID, int SchoolID)
        {
            // kiểm tra xem dữ liệu định xóa có tồn tại không
            bool checkDelete = new PupilOfSchoolRepository(this.context).ExistsRow(GlobalConstants.PUPIL_SCHEMA, "PupilOfSchool",
           new Dictionary<string, object>()
                {
                    {"PupilOfSchoolID",PupilOfSchoolID},
                    {"SchoolID",SchoolID}
                }, null);
            if (!checkDelete)
            {
                throw new BusinessException("Common_Delete_Exception");
            }

            PupilOfSchool PupilOfSchool = PupilOfSchoolRepository.Find(PupilOfSchoolID);
            //PupilID, SchoolID: not compatible (PupilProfile)
            bool SearchInfo = new PupilProfileRepository(this.context).ExistsRow(GlobalConstants.PUPIL_SCHEMA, "PupilProfile",
                    new Dictionary<string, object>()
                {
                    {"PupilProfileID",PupilOfSchool.PupilID},
                    {"CurrentSchoolID",PupilOfSchool.SchoolID}
                }, null);
            if (!SearchInfo)
            {
                throw new BusinessException("Common_Validate_NotCompatible");
            }

            IQueryable<PupilOfClass> lsPupilOfClass = PupilOfClassBusiness.All.Where(o => (o.PupilID == PupilOfSchool.PupilID));
            PupilOfClass PupilOfClass = lsPupilOfClass.FirstOrDefault();
            if (PupilOfClass.Status != GlobalConstants.PUPIL_STATUS_STUDYING)
            {
                throw new BusinessException("Common_Validate_NotIsCurrentStatus");
            }

            else
            {
                base.Delete(PupilOfSchoolID);

            }

        }

        #endregion

        #region Search
        /// <summary>
        /// Tìm kiếm thông tin học sinh vào trường.
        /// </summary>
        /// <author>tungnd</author>
        /// <date>9/10/2012</date>
        /// <param name="ethnic">Đối tượng PupilOfSchool</param>
        /// <returns>Đối tượng PupilOfSchool</returns>

        private IQueryable<PupilOfSchool> Search(IDictionary<string, object> dic)
        {

            int SchoolID = Utils.GetInt(dic, "SchoolID");

            int PupilID = Utils.GetInt(dic, "PupilID");

            DateTime? EnrolmentDate = Utils.GetDateTime(dic, "EnrolmentDate");
            DateTime? EndDate = Utils.GetDateTime(dic, "EndDate");
            int EnrolmentType = Utils.GetInt(dic, "EnrolmentType");

            IQueryable<PupilOfSchool> lsPupilOfSchool = this.PupilOfSchoolRepository.All;
            // bắt đầu tìm kiếm
            if (SchoolID != 0)
            {
                lsPupilOfSchool = lsPupilOfSchool.Where(pu => (pu.SchoolID == SchoolID));
            }

            if (PupilID != 0)
            {
                lsPupilOfSchool = lsPupilOfSchool.Where(pu => (pu.PupilID == PupilID));
            }

            if (EnrolmentDate != null)
            {
                lsPupilOfSchool = lsPupilOfSchool.Where(pu => (pu.EnrolmentDate == EnrolmentDate));
            }

            if (EnrolmentType != 0)
            {
                lsPupilOfSchool = lsPupilOfSchool.Where(pu => (pu.EnrolmentType == EnrolmentType));
            }

            return lsPupilOfSchool;
        }

        #endregion

        #region SearchBySchool
        /// <summary>
        /// Lấy ra thông tin từ mã trường
        /// </summary>
        /// <author>tungnd</author>
        /// <date>9/10/2012</date>
        /// <param name="ethnic">Đối tượng PupilOfSchool</param>
        /// <returns>Đối tượng PupilOfSchool</returns>

        public IQueryable<PupilOfSchool> SearchBySchool(int SchoolID, IDictionary<string, object> SearchInfo)
        {
            if (SchoolID == 0)
            {
                return null;
            }
            else
            {
                SearchInfo["SchoolID"] = SchoolID;
                return Search(SearchInfo);

            }

        }


        #endregion
    }
}