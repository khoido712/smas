﻿/**
 * The Viettel License
 *
 * Copyright 2012 Viettel Telecom. All rights reserved.
 * VIETTEL PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */

/** 
* @author dungnt
* @version $Revision: $
*/

using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Linq.Expressions;
using System.Web;
using log4net;

using SMAS.DAL.Repository;
using SMAS.DAL.IRepository;
using SMAS.Business.IBusiness;
using SMAS.Models.Models;
using SMAS.Business.Common;

namespace SMAS.Business.Business
{
    /// <summary>
    /// Trình độ quản lý nhà nước
    /// <author>dungnt</author>
    /// <date>05/09/2012</date>
    /// </summary>
    public partial class StateManagementGradeBusiness
    {
        #region private member variable
        private const int RESOLUTION_MAX_LENGTH = 100;   //do dai lon nhat truong ten
        private const int DESCRIPTION_MAX_LENGTH = 400; //do dai lon nhat truong mieu ta
        #endregion



        #region insert
        /// <summary>
        /// Thêm mới Trình độ quản lý nhà nước
        /// <author>dungnt</author>
        /// <date>05/09/2012</date>
        /// </summary>
        /// <param name="insertStateManagementGrade">Đối tượng Insert</param>
        /// <returns>Đối tượng sau khi được insert vào DB thành công</returns>
        public override StateManagementGrade Insert(StateManagementGrade insertStateManagementGrade)
        {


            //resolution rong
            Utils.ValidateRequire(insertStateManagementGrade.Resolution, "StateManagementGrade_Label_Resolution");

            Utils.ValidateMaxLength(insertStateManagementGrade.Resolution, RESOLUTION_MAX_LENGTH, "StateManagementGrade_Label_Resolution");
            Utils.ValidateMaxLength(insertStateManagementGrade.Description, DESCRIPTION_MAX_LENGTH, "StateManagementGrade_Column_Description");

            //kiem tra ton tai - theo cap - can lam lai
            this.CheckDuplicate(insertStateManagementGrade.Resolution, GlobalConstants.LIST_SCHEMA, "StateManagementGrade", "Resolution", true, 0, "StateManagementGrade_Label_Resolution");
            //this.CheckDuplicate(insertStateManagementGrade.SchoolID.ToString(), GlobalConstants.LIST_SCHEMA, "StateManagementGrade", "AppliedLevel", true, 0, "StateManagementGrade_Label_ShoolID");
           // this.CheckDuplicateCouple(insertStateManagementGrade.Resolution, insertStateManagementGrade.SchoolID.ToString(),
           //GlobalConstants.LIST_SCHEMA, "StateManagementGrade", "Resolution", "SchoolID", true, 0, "StateManagementGrade_Label_Resolution");
            //kiem tra range

          

            insertStateManagementGrade.IsActive = true;

          return  base.Insert(insertStateManagementGrade);
        }
        #endregion

        #region update
        /// <summary>
        /// Cập nhật Trình độ quản lý nhà nước
        /// <author>dungnt</author>
        /// <date>05/09/2012</date>
        /// </summary>
        /// <param name="updateStateManagementGrade">Đối tượng Update</param>
        /// <returns>Đối tượng sau khi được Update vào DB thành công</returns>
        public override StateManagementGrade Update(StateManagementGrade updateStateManagementGrade)
        {

            //check avai
            new StateManagementGradeBusiness(null).CheckAvailable(updateStateManagementGrade.StateManagementGradeID, "StateManagementGrade_Label_StateManagementGradeID");

            //resolution rong
            Utils.ValidateRequire(updateStateManagementGrade.Resolution, "StateManagementGrade_Label_Resolution");

            Utils.ValidateMaxLength(updateStateManagementGrade.Resolution, RESOLUTION_MAX_LENGTH, "StateManagementGrade_Label_Resolution");
            Utils.ValidateMaxLength(updateStateManagementGrade.Description, DESCRIPTION_MAX_LENGTH, "StateManagementGrade_Column_Description");

            //kiem tra ton tai - theo cap - can lam lai
             this.CheckDuplicate(updateStateManagementGrade.Resolution, GlobalConstants.LIST_SCHEMA, "StateManagementGrade", "Resolution", true, updateStateManagementGrade.StateManagementGradeID, "StateManagementGrade_Label_Resolution");

            //this.CheckDuplicateCouple(updateStateManagementGrade.Resolution, updateStateManagementGrade.SchoolID.ToString(),
            //    GlobalConstants.LIST_SCHEMA, "StateManagementGrade", "Resolution", "SchoolID", true, updateStateManagementGrade.StateManagementGradeID, "StateManagementGrade_Label_Resolution");

            // this.CheckDuplicate(updateStateManagementGrade.SchoolID.ToString(), GlobalConstants.LIST_SCHEMA, "StateManagementGrade", "AppliedLevel", true, updateStateManagementGrade.StateManagementGradeID, "StateManagementGrade_Label_ShoolID");

           

            //Loi pham quy che thi khong thuoc truong truyen vao
            //tu StateManagementGradeID ->vao csdl lay StateManagementGrade tuong ung roi 
            //so sanh voi updateStateManagementGrade.SchoolID


            //.................



            updateStateManagementGrade.IsActive = true;

          return  base.Update(updateStateManagementGrade);
        }
        #endregion

        #region Delete
        /// <summary>
        /// Xóa Trình độ quản lý nhà nước
        /// <author>dungnt</author>
        /// <date>05/09/2012</date>
        /// </summary>
        /// <param name="StateManagementGradeId">ID Trình độ quản lý nhà nước</param>
        public void Delete(int StateManagementGradeId)
        {
            //check avai
            new StateManagementGradeBusiness(null).CheckAvailable(StateManagementGradeId, "StateManagementGrade_Label_StateManagementGradeID");

            //da su dung hay chua
            this.CheckConstraints(GlobalConstants.LIST_SCHEMA, "StateManagementGrade",StateManagementGradeId, "StateManagementGrade_Label_StateManagementGradeIDFailed");

            base.Delete(StateManagementGradeId,true);


        }
        #endregion

        #region Search
        /// <summary>
        /// Tìm kiếm Trình độ quản lý nhà nước
        /// <author>dungnt</author>
        /// <date>05/09/2012</date>
        /// </summary>
        /// <param name="dic">tham so doi tuong tim kiem</param>
        /// <returns>Danh sách kết quả tìm kiếm</returns>
        public IQueryable<StateManagementGrade> Search(IDictionary<string, object> dic)
        {
            string resolution = Utils.GetString(dic,"Resolution");
            string description = Utils.GetString(dic,"Description");
           
            bool? isActive = Utils.GetIsActive(dic,"IsActive");
            IQueryable<StateManagementGrade> lsStateManagementGrade = this.StateManagementGradeRepository.All.OrderBy(o => o.Resolution);

            

             if (isActive.HasValue)
            {
                lsStateManagementGrade = lsStateManagementGrade.Where(em => (em.IsActive == isActive));
            }


           

            if (resolution.Trim().Length != 0)
            {

                lsStateManagementGrade = lsStateManagementGrade.Where(em => (em.Resolution.ToUpper().Contains(resolution.ToUpper())));
            }
            if (description.Trim().Length != 0)
            {

                lsStateManagementGrade = lsStateManagementGrade.Where(em => (em.Description.ToUpper().Contains(description.ToUpper())));
            }

           
            return lsStateManagementGrade;
        }
        #endregion
        
    }
}