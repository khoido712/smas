/**
 * The Viettel License
 *
 * Copyright 2012 Viettel Telecom. All rights reserved.
 * VIETTEL PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */

/** 
* @author  
* @version $Revision: $
*/

using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Linq.Expressions;
using System.Web;
using log4net;

using SMAS.DAL.Repository;
using SMAS.DAL.IRepository;
using SMAS.Business.IBusiness;
using SMAS.Models.Models;
using SMAS.Business.Common;
using SMAS.Business.BusinessObject;
using System.Transactions;

namespace SMAS.Business.Business
{
    public partial class SMSParentSentDetailBusiness
    {
        public IQueryable<SMS_PARENT_SENT_DETAIL> Search(SMSParentSendDetailSearchBO searchBO)
        {
            IQueryable<SMS_PARENT_SENT_DETAIL> query = this.All;
            if (searchBO.SchoolId > 0)
            {
                int partitionId = UtilsBusiness.GetPartionId(searchBO.SchoolId);
                query = query.Where(c => c.SCHOOL_ID == searchBO.SchoolId && c.PARTITION_ID == partitionId);
            }
            if (searchBO.Semester > 0)
            {
                query = query.Where(c => c.SEMESTER == searchBO.Semester || c.SEMESTER == GlobalConstants.SEMESTER_OF_YEAR_ALL);
            }

            if (searchBO.Year > 0)
            {
                query = query.Where(c => c.YEAR == searchBO.Year);
            }
            if (searchBO.lstSemesterID != null && searchBO.lstSemesterID.Count > 0)
            {
                query = query.Where(p => searchBO.lstSemesterID.Contains(p.SEMESTER));
            }
            if (searchBO.lstSMSParentContractDetailID != null && searchBO.lstSMSParentContractDetailID.Count > 0)
            {
                query = query.Where(p => searchBO.lstSMSParentContractDetailID.Contains(p.SMS_PARENT_CONTRACT_DETAIL_ID));
            }
            return query;
        }
    }
}
