﻿/**
 * The Viettel License
 *
 * Copyright 2012 Viettel Telecom. All rights reserved.
 * VIETTEL PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */

/** 
* @author  
* @version $Revision: $
*/

using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Linq.Expressions;
using System.Web;
using log4net;

using SMAS.DAL.Repository;
using SMAS.DAL.IRepository;
using SMAS.Business.IBusiness;
using SMAS.Models.Models;
using SMAS.Business.Common;

namespace SMAS.Business.Business
{ 
    public partial class TypeOfFoodBusiness
    {

        #region Validate

        public void Validate(TypeOfFood typeoffood)
        {
            ValidationMetadata.ValidateObject(typeoffood);
        }

        #endregion    

        #region Insert
        public TypeOfFood Insert(TypeOfFood typeoffood)
        {
            Validate(typeoffood);
            //Tên loại thực phẩm đã tồn tại 
            new TypeOfFoodBusiness(null).CheckDuplicate(typeoffood.TypeOfFoodName, GlobalConstants.LIST_SCHEMA, "TypeOfFood", "TypeOfFoodName", true, typeoffood.TypeOfFoodID, "TypeOfFood_Label_TypeOfFoodName");
            return base.Insert(typeoffood);

        }
        
        #endregion

        #region Update
        public TypeOfFood Update(TypeOfFood typeoffood)
        {
            Validate(typeoffood);
            //Tên loại thực phẩm đã tồn tại 
            new TypeOfFoodBusiness(null).CheckDuplicate(typeoffood.TypeOfFoodName, GlobalConstants.LIST_SCHEMA, "TypeOfFood", "TypeOfFoodName", true, typeoffood.TypeOfFoodID, "TypeOfFood_Label_TypeOfFoodName");
            return base.Update(typeoffood);

        }

        #endregion

        #region Delete

        public void Delete(int TypeOfFoodID)
        {

            //Bạn chưa chọn loại thực phẩm cần xóa hoặc loại thực phẩm bạn chọn đã bị xóa khỏi hệ thống
            new TypeOfFoodBusiness(null).CheckAvailable((int)TypeOfFoodID, "TypeOfFood_Label", true);

            //Không thể xóa Dân tộc đang sử dụng
            new EthnicBusiness(null).CheckConstraints(GlobalConstants.LIST_SCHEMA, "TypeOfFood", TypeOfFoodID, "TypeOfFood_Label");
            base.Delete(TypeOfFoodID, true);

        }

        #endregion

        #region Search
        public IQueryable<TypeOfFood> Search(IDictionary<string, object> SearchInfo)
        {
            string TypeOfFoodName = Utils.GetString(SearchInfo, "TypeOfFoodName");
            string Description = Utils.GetString(SearchInfo, "Description");
            bool IsActive = Utils.GetBool(SearchInfo, "IsActive");

            IQueryable<TypeOfFood> lstTypeOfFood = this.TypeOfFoodRepository.All;

            if (TypeOfFoodName.Length != 0)
            {
                lstTypeOfFood = lstTypeOfFood.Where(tof => tof.TypeOfFoodName.Contains(TypeOfFoodName));
            }

            if (Description.Length != 0)
            {
                lstTypeOfFood = lstTypeOfFood.Where(tof => tof.Description.Contains(Description));
            }

            if (IsActive != false)
            {
                lstTypeOfFood = lstTypeOfFood.Where(tof => (tof.IsActive == IsActive));
            }

            return lstTypeOfFood;
        
        }

        #endregion

    }
}