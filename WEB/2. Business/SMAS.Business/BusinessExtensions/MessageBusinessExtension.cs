﻿/**
 * The Viettel License
 *
 * Copyright 2012 Viettel Telecom. All rights reserved.
 * VIETTEL PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */

/** 
* @author  
* @version $Revision: $
*/

using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Linq.Expressions;
using System.Web;
using log4net;

using SMAS.DAL.Repository;
using SMAS.DAL.IRepository;
using SMAS.Business.IBusiness;
using SMAS.Models.Models;
using SMAS.Business.Common;

namespace SMAS.Business.Business
{
    public partial class MessageBusiness
    {
        #region Validate
        public void CheckMessageIDAvailable(int MessageID)
        {
            bool AvailableMessageID = this.repository.ExistsRow(GlobalConstants.TRS_SCHEMA, "Message",
                   new Dictionary<string, object>()
                {
                    {"MessageID", MessageID}
                    
                }, null);
            if (!AvailableMessageID)
            {
                throw new BusinessException("Common_Validate_NotAvailable");
            }
        }
        #endregion
        #region Insert
        public Message Insert(Message Message)
        {

            //Check Require, Max Length
            ValidationMetadata.ValidateObject(Message);

            return base.Insert(Message);

        }
        #endregion
        #region Update
        public Message Update(Message Message)
        {
            //Check Require, Max Length
            ValidationMetadata.ValidateObject(Message);

            return base.Update(Message);
        }
        #endregion
        #region Delete
        public bool Delete(int MessageID)
        {
            try
            {
                //Check available
                CheckMessageIDAvailable(MessageID);

                //Check Using
                this.CheckConstraints(GlobalConstants.TRS_SCHEMA, "Message", MessageID, "Message_Label_MessageID");

                base.Delete(MessageID);
                return true;
            }
            catch
            {
                return false;
            }

        }
        #endregion

        #region Search
        /// <summary>
        /// Tìm kiếm Thông báo
        /// </summary>
        /// <author>BaLX</author>
        /// <date>7/12/2012</date>
        /// <param name="ethnic">Đối tượng message</param>
        /// <returns>Đối tượng message</returns>

        public IQueryable<Message> Search(IDictionary<string, object> dic)
        {
            int MessageID = Utils.GetInt(dic, "MessageID");
            string UserNameSend = Utils.GetString(dic, "UserNameSend");
            string UserNameReceive = Utils.GetString(dic, "UserNameReceive");
            string Title = Utils.GetString(dic, "Title");
            string Content = Utils.GetString(dic, "Content");
            DateTime? SendDate = Utils.GetDateTime(dic, "SendDate");
            DateTime? FromDate = Utils.GetDateTime(dic, "FromDate");
            DateTime? ToDate = Utils.GetDateTime(dic, "ToDate");
            string EmailReceive = Utils.GetString(dic, "EmailReceive");
            bool? IsRead = Utils.GetNullableBool(dic, "IsRead");


            // bắt đầu tìm kiếm
            var query = MessageRepository.All;

            if (MessageID != 0)
            {
                query = query.Where(m => (m.MessageID == MessageID));
            }
            if (UserNameSend.Trim().Length != 0)
            {
                query = query.Where(m => (m.UserNameSend.ToUpper().Contains(UserNameSend.ToUpper())));

            }
            if (UserNameReceive.Trim().Length != 0)
            {
                query = query.Where(m => (m.UserNameReceive.ToUpper().Contains(UserNameReceive.ToUpper())));

            }
            if (Title.Trim().Length != 0)
            {
                query = query.Where(m => (m.Title.ToUpper().Contains(Title.ToUpper())));

            }
            if (Content.Trim().Length != 0)
            {
                query = query.Where(m => (m.Content.ToUpper().Contains(Content.ToUpper())));

            }
            if (EmailReceive.Trim().Length != 0)
            {
                query = query.Where(m => (m.EmailReceive.ToUpper().Contains(EmailReceive.ToUpper())));

            }
            if (IsRead.HasValue)
            {
                query = query.Where(m => (m.IsRead == IsRead.Value));
            }
            if (FromDate.HasValue && ToDate.HasValue)
            {
                query = query.Where(m => (m.SendDate >= FromDate && m.SendDate <= ToDate));

            }
            if (FromDate.HasValue && !ToDate.HasValue)
            {
                query = query.Where(m => (m.SendDate >= FromDate));

            }
            if (!FromDate.HasValue && ToDate.HasValue)
            {
                query = query.Where(m => (m.SendDate <= ToDate));

            }

            return query;
        }
        #endregion

    }
}