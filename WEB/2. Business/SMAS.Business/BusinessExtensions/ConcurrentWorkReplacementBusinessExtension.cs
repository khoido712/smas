﻿/**
 * The Viettel License
 *
 * Copyright 2012 Viettel Telecom. All rights reserved.
 * VIETTEL PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */

/** 
* @author  
* @version $Revision: $
*/

using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Linq.Expressions;
using System.Web;
using log4net;

using SMAS.DAL.Repository;
using SMAS.DAL.IRepository;
using SMAS.Business.IBusiness;
using SMAS.Models.Models;
using SMAS.Business.Common;

namespace SMAS.Business.Business
{ 
    public partial class ConcurrentWorkReplacementBusiness
    {
        private void Validate(ConcurrentWorkReplacement Entity)
        {
            ValidationMetadata.ValidateObject(Entity);

            // Kiểm tra tính hợp lệ dữ liệu
            IDictionary<string, object> SearchInfo = new Dictionary<string, object>();
            SearchInfo["AcademicYearID"] = Entity.AcademicYearID;
            SearchInfo["SchoolID"] = Entity.SchoolID;
            bool compatible = this.repository.ExistsRow(GlobalConstants.SCHOOL_SCHEMA, "AcademicYear", SearchInfo);
            if (!compatible)
            {
                throw new BusinessException("Common_Validate_NotCompatible");
            }

            // Kiểm tra tính hợp lệ dữ liệu
            SearchInfo = new Dictionary<string, object>();
            SearchInfo["ConcurrentWorkAssignmentID"] = Entity.ConcurrentWorkAssignmentID;
            SearchInfo["SchoolID"] = Entity.SchoolID;
            compatible = this.repository.ExistsRow(GlobalConstants.SCHOOL_SCHEMA, "ConcurrentWorkAssignment", SearchInfo);
            if (!compatible)
            {
                throw new BusinessException("Common_Validate_NotCompatible");
            }

            // Kiểm tra tính hợp lệ dữ liệu
            SearchInfo = new Dictionary<string, object>();
            SearchInfo["EmployeeID"] = Entity.TeacherID;
            SearchInfo["SchoolID"] = Entity.SchoolID;
            compatible = this.repository.ExistsRow(GlobalConstants.SCHOOL_SCHEMA, "Employee", SearchInfo);
            if (!compatible)
            {
                throw new BusinessException("Common_Validate_NotCompatible");
            }

            // Kiểm tra tính hợp lệ dữ liệu
            SearchInfo = new Dictionary<string, object>();
            SearchInfo["ConcurrentWorkAssignmentID"] = Entity.ConcurrentWorkAssignmentID;
            SearchInfo["TeacherID"] = Entity.TeacherID;
            compatible = this.repository.ExistsRow(GlobalConstants.SCHOOL_SCHEMA, "ConcurrentWorkAssignment", SearchInfo);
            if (!compatible)
            {
                throw new BusinessException("Common_Validate_NotCompatible");
            }

            // Kiểm tra tính hợp lệ dữ liệu
            SearchInfo = new Dictionary<string, object>();
            SearchInfo["EmployeeID"] = Entity.ReplacedTeacherID;
            SearchInfo["SchoolID"] = Entity.SchoolID;
            compatible = this.repository.ExistsRow(GlobalConstants.SCHOOL_SCHEMA, "Employee", SearchInfo);
            if (!compatible)
            {
                throw new BusinessException("Common_Validate_NotCompatible");
            }

            // Kiểm tra có phải năm hiện tại không
            bool isCurrentYear = this.AcademicYearBusiness.IsCurrentYear(Entity.AcademicYearID.Value);
            if (!isCurrentYear)
            {
                throw new BusinessException("Common_Validate_NotCurrentYear");
            }

            // Kiểm tra trạng thái giáo viên phải đang làm việc
            Employee Teacher = Entity.Employee;
            if (Teacher == null)
            {
                Teacher = this.EmployeeBusiness.Find(Entity.TeacherID);
            }
            if (Teacher.EmploymentStatus != SystemParamsInFile.EMPLOYMENT_STATUS_WORKING)
            {
                throw new BusinessException("ConcurrentWorkReplacement_Validate_TeacherNotWorking");
            }

            AcademicYear AcademicYear = Entity.AcademicYear;
            if (AcademicYear == null)
            {
                AcademicYear = this.AcademicYearBusiness.Find(Entity.AcademicYearID);
            }
            if (!(AcademicYear.FirstSemesterStartDate <= Entity.FromDate && Entity.ToDate <= AcademicYear.SecondSemesterEndDate))
            {
                throw new BusinessException("ConcurrentWorkReplacement_Validate_DateNotInAcademicYearTime");
            }

            //Cùng một phân công kiêm nhiệm và cùng một cán bộ làm thay thì khoảng thời gian không được phép chồng lên nhau.
            IDictionary<string, object> SInfo = new Dictionary<string, object>();
            SInfo["ConcurrentWorkAssignmentID"] = Entity.ConcurrentWorkAssignmentID;
            SInfo["ReplacedTeacherID"] = Entity.ReplacedTeacherID;
            SInfo["AcademicYearID"] = Entity.AcademicYearID;
            SInfo["IsActive"] = true;
            List<ConcurrentWorkReplacement> lstConReplace1 = ConcurrentWorkReplacementBusiness.SearchBySchool(Entity.SchoolID.Value, SInfo).Where(o => o.ConcurrentWorkReplacementID != Entity.ConcurrentWorkReplacementID).ToList();
            foreach (ConcurrentWorkReplacement item in lstConReplace1)
            {
                if (Entity.FromDate.HasValue)
                {
                    if (item.FromDate.HasValue && item.ToDate.HasValue)
                    {
                        if (Utils.CompareDateTimeRange(Entity.FromDate.Value, Entity.ToDate, item.FromDate.Value, item.ToDate.Value))
                        {
                            //Công việc làm thay cho cán bộ kiêm nhiệm trong khoảng thời gian này đã tồn tại. Vui lòng cập nhật lại.
                            throw new BusinessException("ConcurrentWorkReplacement_Label_DateConflict2", new List<object>() { });
                        }
                    }
                }
            }

        }
        /// <summary>
        /// Tìm kiếm Làm thay kiêm nhiệm
        /// </summary>
        /// <param name="TeacherID">ID cán bộ</param>
        /// <param name="FullName">Tên đầy đủ</param>
        /// <param name="SchoolFacultyID">ID tổ bộ môn</param>
        /// <param name="ConcurrentWorkAssignmentID">ID công việc kiêm nhiệm</param>
        /// <param name="AcademicYearID">ID năm</param>
        /// <param name="SchoolID">ID trường</param>
        /// <returns>
        /// Danh sách làm thay kiêm nhiệm
        /// </returns>
        /// <author>hath</author>
        /// <date> 9/7/2012</date>
        public IQueryable<ConcurrentWorkReplacement> Search(IDictionary<string, object> dic)
        {
            int TeacherID = Utils.GetInt(dic, "TeacherID");
            string FullName = Utils.GetString(dic, "FullName");
            int SchoolFacultyID = Utils.GetInt(dic, "SchoolFacultyID");
            int ConcurrentWorkAssignmentID = Utils.GetInt(dic, "ConcurrentWorkAssignmentID");
            int AcademicYearID = Utils.GetInt(dic, "AcademicYearID");
            int SchoolID = Utils.GetInt(dic, "SchoolID");
            bool? IsActive = Utils.GetIsActive(dic, "IsActive");
            int EmploymentStatus = Utils.GetInt(dic, "EmploymentStatus");
            int ReplacedTeacherID = Utils.GetInt(dic, "ReplacedTeacherID");

            //IQueryable<ConcurrentWorkReplacement> lsConcurrentWorkReplacement = ConcurrentWorkReplacementRepository.All;
            IQueryable<ConcurrentWorkReplacement> lsConcurrentWorkReplacement = from p in ConcurrentWorkReplacementRepository.All
                                                                                join q in EmployeeHistoryStatusBusiness.All on p.ReplacedTeacherID equals q.EmployeeID
                                                                                where (q.SchoolID == SchoolID || SchoolID == 0) && (q.EmployeeStatus == EmploymentStatus || EmploymentStatus == 0)
                                                                                select p;
            if (TeacherID != 0)
            {
                lsConcurrentWorkReplacement = lsConcurrentWorkReplacement.Where(o => o.TeacherID == TeacherID);
            }

            if (ReplacedTeacherID != 0)
            {
                lsConcurrentWorkReplacement = lsConcurrentWorkReplacement.Where(o => o.ReplacedTeacherID == ReplacedTeacherID);
            }
            if (FullName.Trim().Length != 0)
            {
                lsConcurrentWorkReplacement = lsConcurrentWorkReplacement.Where(o => o.Employee.FullName.Contains(FullName.ToLower()));
            }
            if (SchoolFacultyID != 0)
            {
                lsConcurrentWorkReplacement = lsConcurrentWorkReplacement.Where(o => o.SchoolFacultyID == SchoolFacultyID);
            }
            if (ConcurrentWorkAssignmentID != 0)
            {
                lsConcurrentWorkReplacement = lsConcurrentWorkReplacement.Where(o => o.ConcurrentWorkAssignmentID == ConcurrentWorkAssignmentID);
            }
            if (AcademicYearID != 0)
            {
                lsConcurrentWorkReplacement = lsConcurrentWorkReplacement.Where(o => o.AcademicYearID == AcademicYearID);
            }
            if (SchoolID != 0)
            {
                lsConcurrentWorkReplacement = lsConcurrentWorkReplacement.Where(o => o.SchoolFaculty.SchoolID == SchoolID);
            }

            if (IsActive != null)
            {
                lsConcurrentWorkReplacement = lsConcurrentWorkReplacement.Where(o => o.IsActive == IsActive);
            }
            return lsConcurrentWorkReplacement;
        }

        /// <summary>
        /// Xóa làm thay kiêm nhiệm
        /// </summary>
        /// <param name="ConcurrentWorkReplacementID">Đối tượng ConcurrentWorkReplacementID</param>
        /// <author>hath</author>
        /// <date> 9/7/2012</date>
        public void Delete(int SchoolID, int ConcurrentWorkReplacementID)
        { 
            //Dữ liệu không tồn tại
            this.CheckAvailable(ConcurrentWorkReplacementID, "ConcurrentWorkReplacement_Label_ConcurrentWorkReplacementID");

            // Kiểm tra tính hợp lệ dữ liệu
            IDictionary<string, object> SearchInfo = new Dictionary<string, object>();
            SearchInfo["ConcurrentWorkReplacementID"] = ConcurrentWorkReplacementID;
            SearchInfo["SchoolID"] = SchoolID;
            bool compatible = this.repository.ExistsRow(GlobalConstants.SCHOOL_SCHEMA, "ConcurrentWorkReplacement", SearchInfo);
            if (!compatible)
            {
                throw new BusinessException("Common_Validate_NotCompatible");
            }


            ConcurrentWorkReplacement Entity = this.Find(ConcurrentWorkReplacementID);
            // Kiểm tra có phải năm hiện tại không
            bool isCurrentYear = this.AcademicYearBusiness.IsCurrentYear(Entity.AcademicYearID.Value);
            if (!isCurrentYear)
            {
                throw new BusinessException("Common_Validate_NotCurrentYear");
            }

            base.Delete(ConcurrentWorkReplacementID,true);
        }

        /// <summary>
        /// Sửa Làm thay công việc kiêm nhiệm
        /// </summary>
        /// <param name="concurrentWorkReplacement">Đối tượng ConcurrentWorkReplacement</param>
        /// <returns>
        /// Đối tượng ConcurrentWorkReplacement
        /// </returns>
        /// <author>hath</author>
        /// <date> 9/7/2012</date>
        public override ConcurrentWorkReplacement Update(ConcurrentWorkReplacement concurrentWorkReplacement)
        {
            //ton tai
            this.CheckAvailable(concurrentWorkReplacement.ConcurrentWorkReplacementID, "ConcurrentWorkReplacement_Label_ConcurrentWorkReplacementID", true);

            this.Validate(concurrentWorkReplacement);

            // Kiểm tra tính hợp lệ dữ liệu
            IDictionary<string, object> SearchInfo = new Dictionary<string, object>();
            SearchInfo["ConcurrentWorkReplacementID"] = concurrentWorkReplacement.ConcurrentWorkReplacementID;
            SearchInfo["SchoolID"] = concurrentWorkReplacement.SchoolID;
            bool compatible = this.repository.ExistsRow(GlobalConstants.SCHOOL_SCHEMA, "ConcurrentWorkReplacement", SearchInfo);
            if (!compatible)
            {
                throw new BusinessException("Common_Validate_NotCompatible");
            }


            // Kiểm tra tính hợp lệ dữ liệu
            SearchInfo = new Dictionary<string, object>();
            SearchInfo["ConcurrentWorkReplacementID"] = concurrentWorkReplacement.ConcurrentWorkReplacementID;
            SearchInfo["AcademicYearID"] = concurrentWorkReplacement.AcademicYearID;
            compatible = this.repository.ExistsRow(GlobalConstants.SCHOOL_SCHEMA, "ConcurrentWorkReplacement", SearchInfo);
            if (!compatible)
            {
                throw new BusinessException("Common_Validate_NotCompatible");
            }

            concurrentWorkReplacement.SchoolFacultyID = this.EmployeeBusiness.Find(concurrentWorkReplacement.ReplacedTeacherID).SchoolFacultyID;
            return base.Update(concurrentWorkReplacement);
        }


        /// <summary>
        /// Thêm Làm thay công việc kiêm nhiệm
        /// </summary>
        /// <param name="concurrentWorkReplacement">Đối tượng ConcurrentWorkReplacement</param>
        /// <returns>
        /// Đối tượng ConcurrentWorkReplacement
        /// </returns>
        /// <author>hath</author>
        /// <date> 9/7/2012</date>
        public override ConcurrentWorkReplacement Insert(ConcurrentWorkReplacement concurrentWorkReplacement)
        {

            this.Validate(concurrentWorkReplacement);
            IQueryable<ConcurrentWorkReplacement> lscwr = ConcurrentWorkReplacementBusiness.All.Where(o => o.AcademicYearID == concurrentWorkReplacement.AcademicYearID
                && o.SchoolID == concurrentWorkReplacement.SchoolID
                && o.TeacherID == concurrentWorkReplacement.TeacherID
                && o.ReplacedTeacherID == concurrentWorkReplacement.ReplacedTeacherID);
            if (lscwr.Count() > 0)
            {
                if (lscwr.Where(o => (o.FromDate <= concurrentWorkReplacement.FromDate && o.ToDate >= concurrentWorkReplacement.FromDate)
                    || (o.FromDate <= concurrentWorkReplacement.ToDate && o.ToDate >= concurrentWorkReplacement.ToDate)).Count() > 0)
                {
                    throw new BusinessException("Common_Validate_ContaintDatetime");
                }
            }
            concurrentWorkReplacement.SchoolFacultyID = this.EmployeeBusiness.Find(concurrentWorkReplacement.ReplacedTeacherID).SchoolFacultyID;
            return base.Insert(concurrentWorkReplacement);
        }


        public IQueryable<ConcurrentWorkReplacement> SearchBySchool(int SchoolID, IDictionary<string, object> dic)
        {
            if (SchoolID == 0)
            {
                return null;
            }
            else
            {
                dic["SchoolID"] = SchoolID;
                return this.Search(dic);
            }
        }


        /// <summary>
        /// Xóa phân công việc kiêm nhiệm (chỉ hủy kích hoạt, ko xóa hẳn khỏi DB)
        /// </summary>
        /// <author>AnhVD9 20140924</author>
        /// <param name="SchoolID"></param>
        /// <param name="TeacherID"></param>
        public void Delete(Dictionary<string, object> SearchInfo)
        {
            var LstWork = this.Search(SearchInfo).ToList();
            int Count = 0;
            if (LstWork != null && (Count = LstWork.Count) > 0)
            {
                ConcurrentWorkReplacement entity;
                for (int i = 0; i < Count; i++)
                {
                    entity = LstWork[i];
                    base.Delete(entity.ConcurrentWorkReplacementID, true); // Deactive cả các năm học cũ - Ko xóa
                }
                this.ConcurrentWorkAssignmentBusiness.Save();
            }
        }
    }
}