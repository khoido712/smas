/**
 * The Viettel License
 *
 * Copyright 2012 Viettel Telecom. All rights reserved.
 * VIETTEL PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */

/** 
* @author  
* @version $Revision: $
*/

using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Linq.Expressions;
using System.Web;
using log4net;

using SMAS.DAL.Repository;
using SMAS.DAL.IRepository;
using SMAS.Business.IBusiness;
using SMAS.Models.Models;
using SMAS.Business.Common;
using System.Data.Objects;
using System.Data.Entity.Infrastructure;

namespace SMAS.Business.Business
{
    public partial class InvigilatorAssignmentBusiness
    {
        private class Room
        {
            public int RoomID { get; set; }
            public DateTime Start { get; set; }
            public DateTime End { get; set; }
            public Room() { }
            public Room(int RoomID, DateTime Start, DateTime End)
            {
                this.RoomID = RoomID;
                this.Start = Start;
                this.End = End;
            }
        }

        private class PInvigilator
        {
            public int InvigilatorID;
            public List<Room> ListRoom { get; set; }
            public PInvigilator(int InvigilatorID)
            {
                this.InvigilatorID = InvigilatorID;
                this.ListRoom = new List<Room>();
            }

            public bool BusyIn(DateTime start, DateTime end)
            {
                foreach (Room room in ListRoom)
                {
                    bool c1 = start <= room.Start && room.Start <= end;
                    bool c2 = start <= room.End && room.End <= end;

                    if (c1 || c2)
                    {
                        return true;
                    }
                }
                return false;
            }
        }

        private PInvigilator GetFreeInvigilator(List<PInvigilator> lstTeacher, DateTime start, DateTime end)
        {
            foreach (PInvigilator teacher in lstTeacher)
            {
                if (!teacher.BusyIn(start, end))
                {
                    return teacher;
                }
            }
            return null;
        }

        /// <summary>
        /// AddInvigilatorForRoom
        /// </summary>
        /// <param name="SchoolID">The school ID.</param>
        /// <param name="AppliedLevel">The applied level.</param>
        /// <param name="RoomID">The room ID.</param>
        /// <param name="InvigilatorID">The invigilator ID.</param>
        /// <author>AuNH</author>
        /// <date>11/19/2012</date>
        public void AddInvigilatorForRoom(int SchoolID, int AppliedLevel, int RoomID, int InvigilatorID)
        {
            this.ExaminationRoomBusiness.CheckAvailable(RoomID, "ExaminationRoomID", false);
            this.InvigilatorBusiness.CheckAvailable(InvigilatorID, "InvigilatorID", false);

            ExaminationRoom ExaminationRoom = this.ExaminationRoomBusiness.Find(RoomID);

            // Kiểm tra tính hợp lệ dữ liệu
            IDictionary<string, object> SearchInfo = new Dictionary<string, object>();
            SearchInfo["ExaminationID"] = ExaminationRoom.ExaminationID;
            SearchInfo["SchoolID"] = SchoolID;
            bool compatible = this.repository.ExistsRow(GlobalConstants.EXAM_SCHEMA, "Examination", SearchInfo);
            if (!compatible)
            {
                throw new BusinessException("Common_Validate_NotCompatible");
            }

            SearchInfo = new Dictionary<string, object>();
            SearchInfo["ExaminationID"] = ExaminationRoom.ExaminationID;
            SearchInfo["AppliedLevel"] = AppliedLevel;
            compatible = this.repository.ExistsRow(GlobalConstants.EXAM_SCHEMA, "Examination", SearchInfo);
            if (!compatible)
            {
                throw new BusinessException("Common_Validate_NotCompatible");
            }

            compatible = ExaminationRoom.Examination.CurrentStage < SystemParamsInFile.EXAMINATION_STAGE_HEAD_ATTACHED;
            if (!compatible)
            {
                throw new BusinessException("InvigilatorAssignment_ExamStage_NotValid");
            }

            bool isCurrentYear = this.AcademicYearBusiness.IsCurrentYear(ExaminationRoom.Examination.AcademicYearID);
            if (!isCurrentYear)
            {
                throw new BusinessException("Common_Validate_NotCurrentYear");
            }

            Invigilator Invigilator = this.InvigilatorBusiness.Find(InvigilatorID);
            compatible = ExaminationRoom.ExaminationID == Invigilator.ExaminationID;
            if (!compatible)
            {
                throw new BusinessException("Common_Validate_NotCompatible");
            }
            int CountInvi = InvigilatorAssignmentBusiness.All.Where(o => o.InvigilatorID == InvigilatorID && o.Invigilator.ExaminationID == ExaminationRoom.ExaminationID && o.ExaminationRoom.ExaminationSubjectID == ExaminationRoom.ExaminationSubjectID).Count();
            if (CountInvi > 0)
            {
                throw new BusinessException("Common_Validate_InvigilatorValid");
            }
            //Từ RoomID, InvigilatorID tạo ra đối tượng  InvigilatorAssignmentID tương ứng rồi insert vào bảng InvigilatorAssignment
            InvigilatorAssignment ia = new InvigilatorAssignment();
            ia.RoomID = RoomID;
            ia.InvigilatorID = InvigilatorID;
            ia.AssignedDate = DateTime.Now;

            base.Insert(ia);

        }

        /// <summary>
        /// Search
        /// </summary>
        /// <param name="SearchIno">The search ino.</param>
        /// <returns>
        /// IQueryable{InvigilatorAssignment}
        /// </returns>
        /// <author>hath</author>
        /// <date>11/19/2012</date>
        private IQueryable<InvigilatorAssignment> Search(IDictionary<string, object> SearchIno)
        {
            IQueryable<InvigilatorAssignment> lsInvigilatorAssignment = InvigilatorAssignmentRepository.All;
            //Thực hiện tìm kiếm trong bảng InvigilatorAssignment theo các thông tin trong SearchInfo:
            //InvigilatorAssignmentID: default = 0;0   All
            int InvigilatorAssignmentID = Utils.GetInt(SearchIno, "InvigilatorAssignmentID");
            //InvigilatorID: default =0;0   All
            int InvigilatorID = Utils.GetInt(SearchIno, "InvigilatorID");
            //RoomID: default = 0; 0   All
            int RoomID = Utils.GetInt(SearchIno, "RoomID");
            //AssignedDate: default =null; null   All
            DateTime? AssignedDate = Utils.GetDateTime(SearchIno, "AssignedDate");
            //AcademicYearID: default =0; 0 All
            int AcademicYearID = Utils.GetInt(SearchIno, "AcademicYearID");
            //SchoolID: default =0; 0 All
            int SchoolID = Utils.GetInt(SearchIno, "SchoolID");
            //EducationLevelID: default =0; 0 All
            int EducationLevelID = Utils.GetInt(SearchIno, "EducationLevelID");
            //AppliedLevel: default =0; 0 All; tìm kiếm theo EducationLevel(EducationLevelID).Grade
            int AppliedLevel = (int)Utils.GetInt(SearchIno, "AppliedLevel");
            //ExaminationID: default = 0 =>All
            int ExaminationID = Utils.GetInt(SearchIno, "ExaminationID");
            int ExaminationSubjectID = Utils.GetInt(SearchIno, "ExaminationSubjectID");
            if (InvigilatorAssignmentID != 0)
            {
                lsInvigilatorAssignment = lsInvigilatorAssignment.Where(o => o.InvigilatorAssignmentID == InvigilatorAssignmentID);
            }
            if (InvigilatorID != 0)
            {
                lsInvigilatorAssignment = lsInvigilatorAssignment.Where(o => o.InvigilatorID == InvigilatorID);
            }
            if (RoomID != 0)
            {
                lsInvigilatorAssignment = lsInvigilatorAssignment.Where(o => o.RoomID == RoomID);
            }
            if (AssignedDate.HasValue)
            {
                lsInvigilatorAssignment = lsInvigilatorAssignment.Where(o => o.AssignedDate.Value == AssignedDate.Value);
            }
            if (AcademicYearID != 0)
            {
                lsInvigilatorAssignment = lsInvigilatorAssignment.Where(o => o.Invigilator.Examination.AcademicYearID == AcademicYearID);
            }
            if (SchoolID != 0)
            {
                lsInvigilatorAssignment = lsInvigilatorAssignment.Where(o => o.Invigilator.Examination.SchoolID == SchoolID);
            }
            if (EducationLevelID != 0)
            {
                lsInvigilatorAssignment = lsInvigilatorAssignment.Where(o => o.ExaminationRoom.EducationLevelID == EducationLevelID);
            }
            if (AppliedLevel != 0)
            {
                lsInvigilatorAssignment = lsInvigilatorAssignment.Where(o => o.ExaminationRoom.EducationLevel.Grade == AppliedLevel);
            }
            if (ExaminationID != 0)
            {
                lsInvigilatorAssignment = lsInvigilatorAssignment.Where(o => o.Invigilator.ExaminationID == ExaminationID);
            }
            if (ExaminationSubjectID != 0)
            {
                lsInvigilatorAssignment = lsInvigilatorAssignment.Where(o => o.ExaminationRoom.ExaminationSubjectID == ExaminationSubjectID);
            }
            return lsInvigilatorAssignment;
        }

        /// <summary>
        /// SearchBySchool
        /// </summary>
        /// <param name="SchoolID">The school ID.</param>
        /// <param name="SearchInfo">The search info.</param>
        /// <returns>
        /// IQueryable{InvigilatorAssignment}
        /// </returns>
        /// <author>hath</author>
        /// <date>11/19/2012</date>
        public IQueryable<InvigilatorAssignment> SearchBySchool(int SchoolID, IDictionary<string, object> SearchInfo)
        {
            if (SchoolID != 0)
            {
                SearchInfo["SchoolID"] = SchoolID;
            }
            return this.Search(SearchInfo);
        }

        /// <summary>
        /// Tự động phân công giám thị cho cả kỳ thi
        /// </summary>
        /// <param name="SchoolID"></param>
        /// <param name="AppliedLevel"></param>
        /// <param name="ExaminationID"></param>
        /// <param name="InvigilatorPerRoom"></param>
        /// <author>AuNH</author>
        public void AutoAssign(int SchoolID, int AppliedLevel, int ExaminationID, int InvigilatorPerRoom)
        {

            //ExaminationID: PK(Examination)
            this.ExaminationBusiness.CheckAvailable(ExaminationID, "ExaminationID");
            Examination Exam = ExaminationBusiness.Find(ExaminationID);
            //ExaminationID, SchoolID: not compatible (Examination)
            IDictionary<string, object> SearchInfo1 = new Dictionary<string, object>();
            SearchInfo1["SchoolID"] = SchoolID;
            SearchInfo1["ExaminationID"] = ExaminationID;
            bool compatible1 = this.repository.ExistsRow(GlobalConstants.EXAM_SCHEMA, "Examination", SearchInfo1);
            if (!compatible1)
            {
                throw new BusinessException("Common_Validate_NotCompatible");
            }
            //ExaminationID, AppliedLevel: not compatible (Examination)
            IDictionary<string, object> SearchInfo2 = new Dictionary<string, object>();
            SearchInfo2["AppliedLevel"] = AppliedLevel;
            SearchInfo2["ExaminationID"] = ExaminationID;
            bool compatible2 = this.repository.ExistsRow(GlobalConstants.EXAM_SCHEMA, "Examination", SearchInfo2);
            if (!compatible2)
            {
                throw new BusinessException("Common_Validate_NotCompatible");
            }
            //InvigilatorPerRoom > 0
            Utils.ValidateCompareToNumber(InvigilatorPerRoom, 0, "InvigilatorAssignment_Label_InvigilatorPerRoom");
            //Examination(ExaminationID). CurrentStage (lấy trong CSDL) chỉ được nhận <  EXAMINATION_STAGE_HEAD_ATTACHED (5)
            Utils.ValidateCompareWithNumber(Exam.CurrentStage, SystemParamsInFile.EXAMINATION_STAGE_HEAD_ATTACHED, "Examination_Status_1", "Examination_Status_2");
            //AcademicYearBusiness.IsCurrentYear(AcademicYearID) = FALSE => Năm học không phải năm hiện tại

            if (Exam != null)
            {
                if (!AcademicYearBusiness.IsCurrentYear(Exam.AcademicYearID))
                {
                    throw new BusinessException("Common_IsCurrentYear_Err");
                }
            }
            //Có môn thi chưa được phân phòng thi: select count(*) from ExaminationSubject es where es.ExaminationID = ExaminationID and not exists (select 1 from ExaminationRoom er where er.SubjectID = es. ExaminationSubjectID)
            //Nếu lớn hơn 0 thì ném ra lỗi

            //1 <= InvigilatorPerRoom <= 3
            Utils.ValidateRange((double)InvigilatorPerRoom, 1, 3, "InvigilatorAssignment_Label_InvigilatorPerRoom");
            // Kiểm tra tính hợp lệ dữ liệu
            IDictionary<string, object> SearchInfo = new Dictionary<string, object>();
            SearchInfo["ExaminationID"] = ExaminationID;
            SearchInfo["SchoolID"] = SchoolID;
            bool compatible = this.repository.ExistsRow(GlobalConstants.EXAM_SCHEMA, "Examination", SearchInfo);
            if (!compatible)
            {
                throw new BusinessException("Common_Validate_NotCompatible");
            }

            SearchInfo = new Dictionary<string, object>();
            SearchInfo["ExaminationID"] = ExaminationID;
            SearchInfo["AppliedLevel"] = AppliedLevel;
            compatible = this.repository.ExistsRow(GlobalConstants.EXAM_SCHEMA, "Examination", SearchInfo);
            if (!compatible)
            {
                throw new BusinessException("Common_Validate_NotCompatible");
            }

            Examination Examination = this.ExaminationBusiness.Find(ExaminationID);
            compatible = Examination.CurrentStage < SystemParamsInFile.EXAMINATION_STAGE_HEAD_ATTACHED;
            if (!compatible)
            {
                throw new BusinessException("InvigilatorAssignment_ExamStage_NotValid");
            }


            bool isCurrentYear = this.AcademicYearBusiness.IsCurrentYear(Examination.AcademicYearID);
            if (!isCurrentYear)
            {
                throw new BusinessException("Common_Validate_NotCurrentYear");
            }

            var query = this.ExaminationSubjectRepository.All.Where(o => o.ExaminationID == ExaminationID).Select(o => o.ExaminationSubjectID);
            var query2 = this.ExaminationRoomRepository.All.Where(o => o.ExaminationSubject.ExaminationID == ExaminationID).Select(o => o.ExaminationSubjectID);
            int allSubject = query.Distinct().Count();
            int roomSetedSubject = query2.Distinct().Count();
            bool hasSubjectNotSetRoom = allSubject > roomSetedSubject;
            if (hasSubjectNotSetRoom)
            {
                throw new BusinessException("InvigilatorAssignment_HasSomeSubject_NotSet_Room");
            }

            compatible = 1 <= InvigilatorPerRoom && InvigilatorPerRoom <= 3;
            if (!compatible)
            {
                throw new BusinessException("InvigilatorAssignment_InvigilatorPerRoom_NotInRank");
            }



            // Xử lý dữ liệu
            // Xóa các phân công trước
            string sql = "delete from invigilator_assignment ia where exists (select 1 from examination_room er where"
            + " ia.room_id = er.examination_room_id and er.examination_id = " + ExaminationID + ")";
            ObjectContext objectContext = ((IObjectContextAdapter)this.context).ObjectContext;
            objectContext.ExecuteStoreCommand(sql);

            // Lay danh sach phong thi
            List<ExaminationRoom> ListERoom = this.ExaminationRoomBusiness.SearchBySchool(SchoolID, new Dictionary<string, object> { { "ExaminationID", ExaminationID } }).Include("ExaminationSubject").ToList();
            List<Room> ListRoom = new List<Room>();
            foreach (ExaminationRoom er in ListERoom)
            {
                Room Room = new Room(er.ExaminationRoomID, er.ExaminationSubject.StartTime.Value,
                    er.ExaminationSubject.StartTime.Value.AddMinutes(er.ExaminationSubject.DurationInMinute.Value));

                ListRoom.Add(Room);
            }

            // Danh sach giam thi
            List<Invigilator> ListInvigilator = this.InvigilatorBusiness.SearchBySchool(SchoolID, new Dictionary<string, object> { { "ExaminationID", ExaminationID } }).ToList();
            List<PInvigilator> ListPInvigilator = new List<PInvigilator>();
            foreach (Invigilator pi in ListInvigilator)
            {
                PInvigilator PInvigilator = new PInvigilator(pi.InvigilatorID);
                ListPInvigilator.Add(PInvigilator);
            }

            // Tinh toan so luong giam thi toi thieu
            int num = ListRoom.Count();
            int max = 0; // So luong toi da cac phong thi trong cung mot thoi diem
            for (int i = 0; i < num; i++)
            {
                int n = 0; // So luong ca thi co khoang thowi gian chứa thoi gian bat dau cua ca thi dang duyet
                Room r1 = ListRoom[i];
                for (int j = 0; j < num; j++)
                {
                    Room r2 = ListRoom[j];
                    // thoi gian bat dau r1 co nam trong khoang thoi gian cua r2 khong
                    bool intersec = r2.Start <= r1.Start && r1.Start <= r2.End;
                    if (intersec)
                    {
                        n++;
                    }
                }
                if (n > max)
                {
                    max = n;
                }
            }

            // So luong giam thi toi thieu bang max*InvigilatorPerRoom
            int MinOfInvigilator = max * InvigilatorPerRoom;
            int NumOfInvigilator = ListPInvigilator.Count();

            if (NumOfInvigilator < MinOfInvigilator)
            {
                List<object> Params = new List<object>();
                Params.Add(MinOfInvigilator);
                throw new BusinessException("InvigilatorAssignment_NotEnough_Invigilator", Params);
            }

            // Phan cong giam thi
            List<InvigilatorAssignment> ListResult = new List<InvigilatorAssignment>();
            foreach (Room ro in ListRoom)
            {
                for (int i = 0; i < InvigilatorPerRoom; i++)
                {
                    InvigilatorAssignment InvigilatorAssignment = new InvigilatorAssignment();
                    InvigilatorAssignment.RoomID = ro.RoomID;
                    PInvigilator Invigilator = GetFreeInvigilator(ListPInvigilator, ro.Start, ro.End);
                    if (Invigilator != null)
                    {
                        InvigilatorAssignment.InvigilatorID = Invigilator.InvigilatorID;
                        InvigilatorAssignment.AssignedDate = DateTime.Now;
                        ListResult.Add(InvigilatorAssignment);
                        Invigilator.ListRoom.Add(ro);
                    }
                    else
                    {
                        throw new BusinessException("InvigilatorAssignment_NotEnough_Invigilator");
                    }
                }
            }

            foreach (InvigilatorAssignment ia in ListResult)
            {
                base.Insert(ia);
            }
        }

        /// <summary>
        /// Hoán đổi giám thị
        /// </summary>
        /// <param name="SchoolID"></param>
        /// <param name="AppliedLevel"></param>
        /// <param name="InvigilatorAssignmentID1"></param>
        /// <param name="InvigilatorAssignmentID2"></param>
        /// <author>AuNH</author>
        public void SwatchInvigilator(int SchoolID, int AppliedLevel, int InvigilatorAssignmentID1, int InvigilatorAssignmentID2)
        {
            this.CheckAvailable(InvigilatorAssignmentID1, "InvigilatorAssignmentID", false);
            this.CheckAvailable(InvigilatorAssignmentID2, "InvigilatorAssignmentID", false);

            InvigilatorAssignment InvigilatorAssignment1 = this.Find(InvigilatorAssignmentID1);
            int ExaminationID = InvigilatorAssignment1.ExaminationRoom.ExaminationID.Value;
            int AcademicYearID = InvigilatorAssignment1.ExaminationRoom.Examination.AcademicYearID;

            // Kiểm tra tính hợp lệ dữ liệu
            IDictionary<string, object> SearchInfo = new Dictionary<string, object>();
            SearchInfo["ExaminationID"] = ExaminationID;
            SearchInfo["SchoolID"] = SchoolID;
            bool compatible = this.repository.ExistsRow(GlobalConstants.EXAM_SCHEMA, "Examination", SearchInfo);
            if (!compatible)
            {
                throw new BusinessException("Common_Validate_NotCompatible");
            }

            SearchInfo = new Dictionary<string, object>();
            SearchInfo["ExaminationID"] = ExaminationID;
            SearchInfo["AppliedLevel"] = AppliedLevel;
            compatible = this.repository.ExistsRow(GlobalConstants.EXAM_SCHEMA, "Examination", SearchInfo);
            if (!compatible)
            {
                throw new BusinessException("Common_Validate_NotCompatible");
            }

            compatible = InvigilatorAssignment1.ExaminationRoom.Examination.CurrentStage < SystemParamsInFile.EXAMINATION_STAGE_HEAD_ATTACHED;
            if (!compatible)
            {
                throw new BusinessException("InvigilatorAssignment_ExamStage_NotValid");
            }

            bool isCurrentYear = this.AcademicYearBusiness.IsCurrentYear(AcademicYearID);
            if (!isCurrentYear)
            {
                throw new BusinessException("Common_Validate_NotCurrentYear");
            }

            InvigilatorAssignment InvigilatorAssignment2 = this.Find(InvigilatorAssignmentID2);
            int EducationLevel1 = InvigilatorAssignment1.ExaminationRoom.EducationLevelID;
            int EducationLevel2 = InvigilatorAssignment2.ExaminationRoom.EducationLevelID;
            int ExaminationID2 = InvigilatorAssignment2.ExaminationRoom.ExaminationID.Value;

            if (ExaminationID != ExaminationID2 || EducationLevel1 != EducationLevel2)
            {
                throw new BusinessException("Common_Validate_NotCompatible");
            }

            // Kiem tra xem giam thi 1 co bi conflic voi ca phong thi khac khong
            var query1 = this.All.Where(o => o.InvigilatorID == InvigilatorAssignment1.InvigilatorID);
            query1 = query1.Where(o => o.InvigilatorAssignmentID != InvigilatorAssignmentID1).Include("ExaminationRoom.ExaminationSubject");
            List<InvigilatorAssignment> ListAssigned1 = query1.ToList();

            DateTime Start2 = InvigilatorAssignment2.ExaminationRoom.ExaminationSubject.StartTime.Value;
            DateTime End2 = Start2.AddMinutes(InvigilatorAssignment2.ExaminationRoom.ExaminationSubject.DurationInMinute.Value);

            foreach (InvigilatorAssignment ia in ListAssigned1)
            {
                DateTime Start = ia.ExaminationRoom.ExaminationSubject.StartTime.Value;
                DateTime End = Start.AddMinutes(ia.ExaminationRoom.ExaminationSubject.DurationInMinute.Value);
                bool cond1 = Start <= Start2 && Start2 <= End;
                bool cond2 = Start <= End2 && End2 <= End;

                List<DateRange> lsAllStartDateForAllSubject = new List<DateRange>();
                DateRange dr = new DateRange(Start, End);
                DateRange dr2 = new DateRange(Start2, End2);
                lsAllStartDateForAllSubject.Add(dr);
                lsAllStartDateForAllSubject.Add(dr2);
                bool hasOverLap = DateRange.HasOverlap(false, lsAllStartDateForAllSubject);


                //if (cond1 || cond2)
                if (hasOverLap)
                {
                    List<object> Params = new List<object>();
                    Params.Add(InvigilatorAssignment1.Invigilator.Employee.FullName);
                    throw new BusinessException("InvigilatorAssignment_InvigilatorConflict", Params);
                }
            }

            // Kiem tra xem giam thi 2 co bi conflic voi ca phong thi khac khong
            var query2 = this.All.Where(o => o.InvigilatorID == InvigilatorAssignment2.InvigilatorID);
            query2 = query2.Where(o => o.InvigilatorAssignmentID != InvigilatorAssignmentID2).Include("ExaminationRoom.ExaminationSubject");
            List<InvigilatorAssignment> ListAssigned2 = query2.ToList();

            DateTime Start1 = InvigilatorAssignment1.ExaminationRoom.ExaminationSubject.StartTime.Value;
            DateTime End1 = Start1.AddMinutes(InvigilatorAssignment1.ExaminationRoom.ExaminationSubject.DurationInMinute.Value);

            foreach (InvigilatorAssignment ia in ListAssigned2)
            {
                DateTime Start = ia.ExaminationRoom.ExaminationSubject.StartTime.Value;
                DateTime End = Start.AddMinutes(ia.ExaminationRoom.ExaminationSubject.DurationInMinute.Value);
                bool cond1 = Start <= Start1 && Start1 <= End;
                bool cond2 = Start <= End1 && End1 <= End;

                List<DateRange> lsAllStartDateForAllSubject = new List<DateRange>();
                DateRange dr = new DateRange(Start, End);
                DateRange dr2 = new DateRange(Start1, End1);
                lsAllStartDateForAllSubject.Add(dr);
                lsAllStartDateForAllSubject.Add(dr2);
                bool hasOverLap = DateRange.HasOverlap(false, lsAllStartDateForAllSubject);

                //if (cond1 || cond2)
                if (hasOverLap)
                {
                    List<object> Params = new List<object>();
                    Params.Add(InvigilatorAssignment2.Invigilator.Employee.FullName);
                    throw new BusinessException("InvigilatorAssignment_InvigilatorConflict", Params);
                }
            }

            // Thuc hien hoan doi
            int TmpInvigilatorID = InvigilatorAssignment1.InvigilatorID;
            InvigilatorAssignment1.InvigilatorID = InvigilatorAssignment2.InvigilatorID;
            InvigilatorAssignment2.InvigilatorID = TmpInvigilatorID;

            base.Update(InvigilatorAssignment1);
            base.Update(InvigilatorAssignment2);
        }

        /// <summary>
        /// Thay đổi giám thị
        /// </summary>
        /// <param name="SchoolID"></param>
        /// <param name="AppliedLevel"></param>
        /// <param name="InvigilatorAssignmentID"></param>
        /// <param name="InvigilatorID"></param>
        /// <author>AuNH</author>
        public void ChangeInvigilator(int SchoolID, int AppliedLevel, int InvigilatorAssignmentID, int InvigilatorID)
        {
            this.CheckAvailable(InvigilatorAssignmentID, "InvigilatorAssignmentID", false);
            this.InvigilatorBusiness.CheckAvailable(InvigilatorID, "InvigilatorID", false);

            InvigilatorAssignment InvigilatorAssignment = this.Find(InvigilatorAssignmentID);
            int ExaminationID = InvigilatorAssignment.ExaminationRoom.ExaminationID.Value;
            int AcademicYearID = InvigilatorAssignment.ExaminationRoom.Examination.AcademicYearID;

            // Kiểm tra tính hợp lệ dữ liệu
            IDictionary<string, object> SearchInfo = new Dictionary<string, object>();
            SearchInfo["ExaminationID"] = ExaminationID;
            SearchInfo["SchoolID"] = SchoolID;
            bool compatible = this.repository.ExistsRow(GlobalConstants.EXAM_SCHEMA, "Examination", SearchInfo);
            if (!compatible)
            {
                throw new BusinessException("Common_Validate_NotCompatible");
            }

            SearchInfo = new Dictionary<string, object>();
            SearchInfo["ExaminationID"] = ExaminationID;
            SearchInfo["AppliedLevel"] = AppliedLevel;
            compatible = this.repository.ExistsRow(GlobalConstants.EXAM_SCHEMA, "Examination", SearchInfo);
            if (!compatible)
            {
                throw new BusinessException("Common_Validate_NotCompatible");
            }

            compatible = InvigilatorAssignment.ExaminationRoom.Examination.CurrentStage < SystemParamsInFile.EXAMINATION_STAGE_HEAD_ATTACHED;
            if (!compatible)
            {
                throw new BusinessException("InvigilatorAssignment_ExamStage_NotValid");
            }

            bool isCurrentYear = this.AcademicYearBusiness.IsCurrentYear(AcademicYearID);
            if (!isCurrentYear)
            {
                throw new BusinessException("Common_Validate_NotCurrentYear");
            }


            Invigilator Invigilator = this.InvigilatorBusiness.Find(InvigilatorID);
            int ExaminationID2 = Invigilator.ExaminationID;

            if (ExaminationID != ExaminationID2)
            {
                throw new BusinessException("Common_Validate_NotCompatible");
            }

            // Kiem tra xem giam thi co bi conflic voi cac phong thi khac khong
            var query = this.All.Where(o => o.InvigilatorID == InvigilatorID).Include("ExaminationRoom.ExaminationSubject");
            List<InvigilatorAssignment> ListAssigned = query.ToList();

            DateTime Start2 = InvigilatorAssignment.ExaminationRoom.ExaminationSubject.StartTime.Value;
            DateTime End2 = Start2.AddMinutes(InvigilatorAssignment.ExaminationRoom.ExaminationSubject.DurationInMinute.Value);

            foreach (InvigilatorAssignment ia in ListAssigned)
            {
                DateTime Start = ia.ExaminationRoom.ExaminationSubject.StartTime.Value;
                DateTime End = Start.AddMinutes(ia.ExaminationRoom.ExaminationSubject.DurationInMinute.Value);
                bool cond1 = Start <= Start2 && Start2 <= End;
                bool cond2 = Start <= End2 && End2 <= End;

                List<DateRange> lsAllStartDateForAllSubject = new List<DateRange>();
                DateRange dr = new DateRange(Start, End);
                DateRange dr2 = new DateRange(Start2, End2);
                lsAllStartDateForAllSubject.Add(dr);
                lsAllStartDateForAllSubject.Add(dr2);
                bool hasOverLap = DateRange.HasOverlap(false, lsAllStartDateForAllSubject);

                //if (cond1 || cond2)
                if (hasOverLap)
                {
                    List<object> Params = new List<object>();
                    Params.Add(Invigilator.Employee.FullName);
                    throw new BusinessException("InvigilatorAssignment_InvigilatorConflict", Params);
                }
            }

            // Cap nhat vao db
            InvigilatorAssignment.InvigilatorID = InvigilatorID;
            base.Update(InvigilatorAssignment);
        }


        /// <summary>
        /// Delete
        /// </summary>
        /// <param name="SchoolID">The school ID.</param>
        /// <param name="AppliedLevel">The applied level.</param>
        /// <param name="InvigilatorAssignmentID">The invigilator assignment ID.</param>
        /// <author>AuNH</author>
        /// <date>11/20/2012</date>
        /// <exception cref="BusinessException">Common_Validate_NotCompatible</exception>
        public void Delete(int SchoolID, int AppliedLevel, int InvigilatorAssignmentID)
        {
            this.CheckAvailable(InvigilatorAssignmentID, "InvigilatorAssignment_Label_InvigilatorAssignmentID", false);

            InvigilatorAssignment InvigilatorAssignment = this.Find(InvigilatorAssignmentID);
            int ExaminationID = InvigilatorAssignment.ExaminationRoom.ExaminationID.Value;
            int AcademicYearID = InvigilatorAssignment.ExaminationRoom.Examination.AcademicYearID;

            // Kiểm tra tính hợp lệ dữ liệu
            IDictionary<string, object> SearchInfo = new Dictionary<string, object>();
            SearchInfo["ExaminationID"] = ExaminationID;
            SearchInfo["SchoolID"] = SchoolID;
            bool compatible = this.repository.ExistsRow(GlobalConstants.EXAM_SCHEMA, "Examination", SearchInfo);
            if (!compatible)
            {
                throw new BusinessException("Common_Validate_NotCompatible");
            }

            SearchInfo = new Dictionary<string, object>();
            SearchInfo["ExaminationID"] = ExaminationID;
            SearchInfo["AppliedLevel"] = AppliedLevel;
            compatible = this.repository.ExistsRow(GlobalConstants.EXAM_SCHEMA, "Examination", SearchInfo);
            if (!compatible)
            {
                throw new BusinessException("Common_Validate_NotCompatible");
            }

            compatible = InvigilatorAssignment.ExaminationRoom.Examination.CurrentStage < SystemParamsInFile.EXAMINATION_STAGE_HEAD_ATTACHED;
            if (!compatible)
            {
                throw new BusinessException("InvigilatorAssignment_ExamStage_NotValid");
            }

            bool isCurrentYear = this.AcademicYearBusiness.IsCurrentYear(AcademicYearID);
            if (!isCurrentYear)
            {
                throw new BusinessException("Common_Validate_NotCurrentYear");
            }

            // Xoa trong bang ViolatedInvigilator
            IDictionary<string, object> dic = new Dictionary<string, object>();
            dic["InvigilatorID"] = InvigilatorAssignment.InvigilatorID;
            dic["RoomID"] = InvigilatorAssignment.RoomID;

            List<ViolatedInvigilator> ListViIn = this.ViolatedInvigilatorBusiness.SearchBySchool(SchoolID, dic).ToList();
            this.ViolatedInvigilatorBusiness.DeleteAll(ListViIn);

            // Xoa trong bang InvigilatorAssignment
            base.Delete(InvigilatorAssignmentID);
        }
    }
}
