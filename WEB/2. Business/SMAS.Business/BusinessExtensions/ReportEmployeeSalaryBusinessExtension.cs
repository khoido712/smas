﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Linq.Expressions;
using System.Web;
using log4net;

using SMAS.DAL.Repository;
using SMAS.DAL.IRepository;
using SMAS.Business.IBusiness;
using SMAS.Models.Models;
using SMAS.Business.BusinessObject;
using SMAS.Business.Common;
using System.IO;
using SMAS.VTUtils.Excel.Export;

namespace SMAS.Business.Business
{
    public class ReportEmployeeSalaryBusiness : GenericBussiness<ProcessedReport>, IReportEmployeeSalaryBusiness
    {
        IEmployeeHistoryStatusRepository EmployeeHistoryStatusRepository;
        ITeacherOfFacultyRepository TeacherOfFacultyRepository;
        IEmployeeSalaryRepository EmployeeSalaryRepository;

        public ReportEmployeeSalaryBusiness(ILog logger, SMASEntities context = null)
            : base(logger)
        {
            if (context == null) { context = new SMASEntities(); } this.context = context;
            this.EmployeeHistoryStatusRepository = new EmployeeHistoryStatusRepository(context);
            this.TeacherOfFacultyRepository = new TeacherOfFacultyRepository(context);
            this.EmployeeSalaryRepository = new EmployeeSalaryRepository(context);
        }
        #region Lấy danh sách bảng lương cán bộ
        //
        public IQueryable<ReportEmployeeSalaryBO> GetListEmployeeSalaryBySchool(IDictionary<string, object> SearchInfo)
        {
            int FacultyID = Utils.GetInt(SearchInfo, "FacultyID");
            int AppliedLevel = Utils.GetInt(SearchInfo, "AppliedLevel");
            DateTime? ReportDate = Utils.GetDateTime(SearchInfo, "ReportDate");
            int SchoolID = Utils.GetInt(SearchInfo, "SchoolID");
            //Them 1 ngay do ReportDate chi lay kieu Date so sanh voi DateTime
            DateTime rpDate = ReportDate.Value.AddDays(1);
            IQueryable<Employee> lsEmloyee = from ehs in EmployeeHistoryStatusRepository.All
                                             join tof in TeacherOfFacultyRepository.All on ehs.EmployeeID equals tof.TeacherID
                                             join emp in EmployeeBusiness.All on ehs.EmployeeID equals emp.EmployeeID
                                             where emp.IsActive == true &&
                                             ehs.SchoolID == SchoolID
                                             && tof.SchoolFaculty.SchoolID == SchoolID
                                             && (tof.FacultyID == FacultyID || FacultyID == 0)
                                             //&& ehs.FromDate < rpDate
                                             && (ehs.ToDate == null || ehs.ToDate >= ReportDate)
                                             //& ehs.Employee.AppliedLevel == entity.AppliedLevel
                                             select emp;

            List<EmployeeSalary> listEmployeeSalary = EmployeeSalaryBusiness.SearchBySchool(SchoolID, SearchInfo).ToList();

            var lsEmployeeSalary = from e in lsEmloyee.ToList()
                                   join es in listEmployeeSalary on e.EmployeeID equals es.EmployeeID
                                   where (es.AppliedDate == (from es1 in EmployeeSalaryBusiness.All where e.EmployeeID == es1.EmployeeID && es1.SchoolID == SchoolID && es1.AppliedDate < rpDate select es1.AppliedDate).Max())
                                   && es.SchoolID == SchoolID
                                   select new ReportEmployeeSalaryBO
                                   {
                                       EmployeeID = e.EmployeeID,
                                       FullName = e.FullName,
                                       Name = e.Name,
                                       BirthDate = e.BirthDate,
                                       Genre = e.Genre,// == true ? "Nam" : "Nữ",
                                       FacultyName = e.SchoolFaculty != null ? e.SchoolFaculty.FacultyName : string.Empty,
                                       EmployeeScale = es.EmployeeScale != null ? es.EmployeeScale.Resolution : "",
                                       SubLevel = es.SalaryLevel.SubLevel,
                                       Coefficent = es.SalaryLevel.Coefficient,
                                       SalaryAmount = es.SalaryAmount,
                                       AppliedDate = es.AppliedDate,
                                       SalaryResolution = es.SalaryResolution
                                   };
            if (lsEmployeeSalary != null && lsEmployeeSalary.Count() > 0)
            {
                return lsEmployeeSalary.AsQueryable();
            }
            else return null;
        }
        #endregion

        #region Lấy danh sách cán bộ đến kỳ tăng lương
        //
        public List<ReportEmployeeSalaryBO> GetListEmployeeIncreaseSalaryBySchool(DateTime FromDate, DateTime ToDate, int SchoolID, int AppliedLevel)
        {
            List<ReportEmployeeSalaryBO> lstEmployee = new List<ReportEmployeeSalaryBO>();
            List<EmployeeBO> lsEmloyee = (from ehs in EmployeeHistoryStatusRepository.All
                                          join tof in TeacherOfFacultyRepository.All on ehs.EmployeeID equals tof.TeacherID
                                          where ehs.SchoolID == SchoolID
                                          && tof.SchoolFaculty.SchoolID == SchoolID
                                          && ehs.EmployeeStatus == SystemParamsInFile.EMPLOYMENT_STATUS_WORKING
                                          select new EmployeeBO
                                          {
                                              EmployeeID = ehs.EmployeeID,
                                              EmploymentStatus = ehs.EmployeeStatus,
                                              IsActive = ehs.Employee.IsActive,
                                              FullName = ehs.Employee.FullName,
                                              Name = ehs.Employee.Name,
                                              BirthDate = ehs.Employee.BirthDate,
                                              Genre = ehs.Employee.Genre,
                                              SchoolFacultyName = ehs.Employee.SchoolFaculty.FacultyName,
                                          }).ToList();
            if (lsEmloyee.Count == 0)
            {
                return lstEmployee;
            }
            else
            {
                Dictionary<string, object> SearchInfo = new Dictionary<string, object>();
                List<EmployeeSalary> listEmployeeSalary = EmployeeSalaryBusiness.SearchBySchool(SchoolID, SearchInfo).ToList();

                var lsEmloyeeIncre = from e in lsEmloyee
                                     join es in listEmployeeSalary on e.EmployeeID equals es.EmployeeID
                                     where (es.AppliedDate == (from es1 in EmployeeSalaryBusiness.All where e.EmployeeID == es1.EmployeeID && es1.SchoolID == SchoolID select es1.AppliedDate).Max())
                                     && es.SchoolID == SchoolID
                                     && (e.EmploymentStatus.HasValue && e.EmploymentStatus.Value == 1)// giao vien dang lam viec
                                     && e.IsActive == true
                                     select new ReportEmployeeSalaryBO
                                     {
                                         EmployeeID = e.EmployeeID,
                                         FullName = e.FullName,
                                         BirthDate = e.BirthDate,
                                         Genre = e.Genre,// == true ? "Nam" : "Nữ",
                                         FacultyName = e.SchoolFacultyName,//e.SchoolFaculty != null ? e.SchoolFaculty.FacultyName : string.Empty,
                                         EmployeeScale = es.EmployeeScale.Resolution,
                                         DurationInYear = es.EmployeeScale.DurationInYear,
                                         SubLevel = es.SalaryLevel.SubLevel,
                                         Coefficent = es.SalaryLevel.Coefficient,
                                         SalaryAmount = es.SalaryAmount,
                                         AppliedDate = es.AppliedDate,
                                         SalaryResolution = es.SalaryResolution,
                                         Name = e.Name
                                     };
                foreach (var employee in lsEmloyeeIncre.ToList())
                {
                    EmployeeSalary es = EmployeeSalaryRepository.All.Where(o => o.EmployeeID == employee.EmployeeID).OrderByDescending(o => o.AppliedDate).FirstOrDefault();
                    DateTime appliedDate = es.AppliedDate.Value.AddYears((int)es.EmployeeScale.DurationInYear);
                    if (appliedDate >= FromDate && appliedDate <= ToDate)
                    {
                        //employee.AppliedDate = appliedDate;
                        lstEmployee.Add(employee);
                    }
                }
                if (lstEmployee != null && lstEmployee.Count > 0)
                {
                    lstEmployee = lstEmployee.OrderBy(p => Utils.SortABC(p.Name + " " + p.FullName)).ToList();
                }
                return lstEmployee;
            }
        }
        #endregion

        #region Tạo báo cáo danh sách bảng lương cán bộ
        /// <summary>
        /// 
        /// </summary>
        /// <param name="entity"></param>
        /// <returns></returns>
        public Stream CreateReportEmployeeSalary(IDictionary<string, object> SearchInfo)
        {
            int FacultyID = Utils.GetInt(SearchInfo, "FacultyID");
            int AppliedLevel = Utils.GetInt(SearchInfo, "AppliedLevel");
            DateTime? ReportDate = Utils.GetDateTime(SearchInfo, "ReportDate");
            int SchoolID = Utils.GetInt(SearchInfo, "SchoolID");


            string reportCode = SystemParamsInFile.REPORT_DANH_SACH_BANG_LUONG_CAN_BO;

            ReportDefinition reportDef = ReportDefinitionBusiness.GetByCode(reportCode);
            string templatePath = SystemParamsInFile.TEMPLATE_FOLDER + reportDef.TemplateDirectory + "/" + reportDef.TemplateName;
            IVTWorkbook oBook = VTExport.OpenWorkbook(templatePath);

            int firstRow = 10;
            int lastCol = 11;
            //Lấy sheet template
            IVTWorksheet firstSheet = oBook.GetSheet(1);

            //Tạo template chuẩn
            IVTWorksheet tempSheet = oBook.CopySheetToLast(firstSheet, "K" + (firstRow - 1));
            tempSheet.CopyPasteSameSize(firstSheet.GetRange(firstRow, 1, firstRow + 4, lastCol), "A" + firstRow);

            //Fill dữ liệu chung cho template

            SchoolProfile school = SchoolProfileBusiness.Find(SchoolID);
            string schoolName = school.SchoolName.ToUpper();
            string supervisingDeptName = UtilsBusiness.GetSupervisingDeptName(school.SchoolProfileID, AppliedLevel).ToUpper();
            DateTime reportDate = ReportDate.Value;
            string date = reportDate.ToShortDateString();
            Dictionary<string, object> dicGeneralInfo = new Dictionary<string, object> 
                {
                    {"SchoolName", schoolName}, 
                    {"DeptName", supervisingDeptName},
                    {"ReportDate", reportDate},
                    {"Date", date},
                };

            tempSheet.GetRange(1, 1, firstRow - 1, lastCol).FillVariableValue(dicGeneralInfo);

            //Tạo sheet Fill dữ liệu
            IVTWorksheet dataSheet = oBook.CopySheetToLast(tempSheet,
                new VTVector(firstRow - 1, lastCol).ToString());
            string sheetName = "Bảng lương cán bộ";
            dataSheet.Name = sheetName;
            //Tạo vùng 
            IVTRange range = tempSheet.GetRange(firstRow, 1, firstRow, lastCol);
            IVTRange createUser = tempSheet.GetRange(firstRow + 3, 1, firstRow + 3, lastCol);
            IVTRange rangeDate = tempSheet.GetRange(firstRow + 2, 1, firstRow + 2, lastCol);
            //Lấy dữ liệu
            IDictionary<string, object> dic = new Dictionary<string, object>();
            //Quá trình đào tạo của tất cả cán bộ

            IQueryable<ReportEmployeeSalaryBO> iqReportEmployeeSalary = GetListEmployeeSalaryBySchool(SearchInfo);

            if (iqReportEmployeeSalary.Count() > 0)
            {
                List<object> listData = new List<object>();
                List<ReportEmployeeSalaryBO> lstEmployee = iqReportEmployeeSalary.OrderBy(o => o.Name).ThenBy(o => o.FullName).ToList();
                Dictionary<string, object> dicData = new Dictionary<string, object>();
                for (int i = 0; i < lstEmployee.Count; i++)
                {

                    dataSheet.CopyPasteSameSize(range, firstRow + i, 1);
                    dicData = new Dictionary<string, object>();
                    dicData["Order"] = i + 1;
                    dicData["FullName"] = lstEmployee[i].FullName;
                    dicData["BirthDate"] = lstEmployee[i].BirthDate != null ? lstEmployee[i].BirthDate.Value.ToShortDateString() : null;
                    dicData["Genre"] = lstEmployee[i].Genre == true ? "Nam" : "Nữ";
                    dicData["FacultyName"] = lstEmployee[i].FacultyName;

                    dicData["EmployeeScale"] = lstEmployee[i].EmployeeScale;
                    dicData["SubLevel"] = lstEmployee[i].SubLevel;
                    dicData["Coefficent"] = lstEmployee[i].Coefficent.ToString();
                    dicData["SalaryAmount"] = lstEmployee[i].SalaryAmount;
                    dicData["AppliedDate"] = lstEmployee[i].AppliedDate.Value.ToShortDateString();
                    dicData["SalaryResolution"] = lstEmployee[i].SalaryResolution;
                    listData.Add(dicData);
                }
                dataSheet.GetRange(firstRow, 1, firstRow + lstEmployee.Count, lastCol).FillVariableValue(new Dictionary<string, object>
                    {
                        {"list",listData}
                    });
            }
            //fill dòng người lập báo cáo
            dataSheet.CopyPasteSameSize(rangeDate, firstRow + iqReportEmployeeSalary.Count() + 2, 1);
            dataSheet.GetRange(firstRow + iqReportEmployeeSalary.Count() + 2, 1, firstRow + iqReportEmployeeSalary.Count() + 2, lastCol).FillVariableValue(dicGeneralInfo);
            dataSheet.CopyPasteSameSize(createUser, firstRow + iqReportEmployeeSalary.Count() + 3, 1);
            dataSheet.FitToPage = true;
            //Xoá sheet template
            firstSheet.Delete();
            tempSheet.Delete();

            return oBook.ToStream();
        }
        #endregion

        #region Tạo báo cáo danh sách cán bộ đến kỳ tăng lương
        /// <summary>
        /// 
        /// </summary>
        /// <param name="entity"></param>
        /// <returns></returns>
        public Stream CreateReportEmployeeIncreaseSalary(DateTime FromDate, DateTime ToDate, int SchoolID, int AppliedLevel)
        {

            string reportCode = SystemParamsInFile.REPORT_DANH_SACH_CAN_BO_DEN_KY_TANG_LUONG;

            ReportDefinition reportDef = ReportDefinitionBusiness.GetByCode(reportCode);
            string templatePath = SystemParamsInFile.TEMPLATE_FOLDER + reportDef.TemplateDirectory + "/" + reportDef.TemplateName;
            IVTWorkbook oBook = VTExport.OpenWorkbook(templatePath);

            int firstRow = 9;
            int lastCol = 12;
            //Lấy sheet template
            IVTWorksheet firstSheet = oBook.GetSheet(1);

            //Tạo template chuẩn
            IVTWorksheet tempSheet = oBook.CopySheetToLast(firstSheet, "L" + (firstRow - 1));
            tempSheet.CopyPasteSameSize(firstSheet.GetRange(firstRow, 1, firstRow + 4, lastCol), "A" + firstRow);

            //Fill dữ liệu chung cho template

            SchoolProfile school = SchoolProfileBusiness.Find(SchoolID);
            Province province = school.Province;
            string schoolName = school.SchoolName.ToUpper();
            string supervisingDeptName = school.SupervisingDept.SupervisingDeptName.ToUpper();
            DateTime reportDate = DateTime.Now;//entity.ReportDate;
            string fromdate = FromDate.ToShortDateString();
            string todate = ToDate.ToShortDateString();
            Dictionary<string, object> dicGeneralInfo = new Dictionary<string, object> 
                {
                    {"SchoolName", schoolName}, 
                    {"DeptName", supervisingDeptName},
                    {"ReportDate", reportDate},
                    {"FromDate", fromdate},
                    {"ToDate", todate},
                };

            tempSheet.GetRange(1, 1, firstRow - 1, lastCol).FillVariableValue(dicGeneralInfo);

            //Tạo sheet Fill dữ liệu
            IVTWorksheet dataSheet = oBook.CopySheetToLast(tempSheet,
                new VTVector(firstRow - 1, lastCol).ToString());
            string sheetName = "Danh sách cán bộ tăng lương";
            dataSheet.Name = sheetName;
            //Tạo vùng 
            IVTRange range = tempSheet.GetRange(firstRow, 1, firstRow, lastCol);
            IVTRange createUser = tempSheet.GetRange(firstRow + 3, 1, firstRow + 3, lastCol);
            IVTRange rangeDate = tempSheet.GetRange(firstRow + 2, 1, firstRow + 2, lastCol);

            //Lấy dữ liệu
            IDictionary<string, object> dic = new Dictionary<string, object>();
            //Quá trình đào tạo của tất cả cán bộ

            List<ReportEmployeeSalaryBO> iqReportEmployeeSalary = GetListEmployeeIncreaseSalaryBySchool(FromDate, ToDate, SchoolID, AppliedLevel);

            if (iqReportEmployeeSalary.Count > 0)
            {
                List<object> listData = new List<object>();
                List<ReportEmployeeSalaryBO> lstEmployee = iqReportEmployeeSalary;
                Dictionary<string, object> dicData = new Dictionary<string, object>();
                for (int i = 0; i < lstEmployee.Count; i++)
                {

                    dataSheet.CopyPasteSameSize(range, firstRow + i, 1);
                    dicData = new Dictionary<string, object>();
                    dicData["Order"] = i + 1;
                    dicData["FullName"] = lstEmployee[i].FullName;
                    dicData["BirthDate"] = lstEmployee[i].BirthDate != null ? lstEmployee[i].BirthDate.Value.ToShortDateString() : null;
                    dicData["Genre"] = lstEmployee[i].Genre == true ? "Nam" : "Nữ";
                    dicData["FacultyName"] = lstEmployee[i].FacultyName;

                    dicData["EmployeeScale"] = lstEmployee[i].EmployeeScale;
                    dicData["SubLevel"] = lstEmployee[i].SubLevel;
                    dicData["Coefficent"] = lstEmployee[i].Coefficent;
                    dicData["SalaryAmount"] = lstEmployee[i].SalaryAmount;
                    dicData["AppliedDate"] = lstEmployee[i].AppliedDate.Value.ToShortDateString();
                    dicData["SalaryResolution"] = lstEmployee[i].SalaryResolution;
                    dicData["NextAppliedDate"] = lstEmployee[i].AppliedDate.Value.AddYears(lstEmployee[i].DurationInYear.Value).ToShortDateString();
                    listData.Add(dicData);
                }
                dataSheet.GetRange(firstRow, 1, firstRow + lstEmployee.Count, lastCol).FillVariableValue(new Dictionary<string, object>
                    {
                        {"list",listData}
                    });
            }
            //fill dòng người lập báo cáo
            dataSheet.CopyPasteSameSize(rangeDate, firstRow + iqReportEmployeeSalary.Count() + 2, 1);
            dataSheet.GetRange(firstRow + iqReportEmployeeSalary.Count() + 2, 1, firstRow + iqReportEmployeeSalary.Count() + 2, lastCol).FillVariableValue(dicGeneralInfo);
            dataSheet.CopyPasteSameSize(createUser, firstRow + iqReportEmployeeSalary.Count() + 3, 1);
            //Xoá sheet template
            firstSheet.Delete();
            tempSheet.Delete();

            return oBook.ToStream();
        }
        #endregion
    }
}
