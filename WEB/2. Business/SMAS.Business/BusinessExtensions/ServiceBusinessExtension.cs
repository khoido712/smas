/**
 * The Viettel License
 *
 * Copyright 2012 Viettel Telecom. All rights reserved.
 * VIETTEL PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */

/** 
* @author  
* @version $Revision: $
*/

using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Linq.Expressions;
using System.Web;
using log4net;

using SMAS.DAL.Repository;
using SMAS.DAL.IRepository;
using SMAS.Business.IBusiness;
using SMAS.Models.Models;
using SMAS.Business.Common;

namespace SMAS.Business.Business
{ 
    public partial class ServiceBusiness
    {
        #region Validate
        public void CheckServiceIDAvailable(int ServiceID)
        {
            bool AvailableServiceID = this.repository.ExistsRow(GlobalConstants.TRS_SCHEMA, "Service",
                   new Dictionary<string, object>()
                {
                    {"ServiceID", ServiceID}
                    
                }, null);
            if (!AvailableServiceID)
            {
                throw new BusinessException("Common_Validate_NotAvailable");
            }
        }
        #endregion
        #region Insert
        public Service Insert(Service Service)
        {

            //Check Require, Max Length
            ValidationMetadata.ValidateObject(Service);

            return base.Insert(Service);

        }
        #endregion
        #region Update
        public Service Update(Service Service)
        {
            //Check Require, Max Length
            ValidationMetadata.ValidateObject(Service);

            return base.Update(Service);
        }
        #endregion
        #region Delete
        public bool Delete(int ServiceID)
        {
            try
            {
                //Check available
                CheckServiceIDAvailable(ServiceID);

                //Check Using
                this.CheckConstraints(GlobalConstants.TRS_SCHEMA, "Service", ServiceID, "Service_Label_ServiceID");

                base.Delete(ServiceID);
                return true;
            }
            catch
            {
                return false;
            }

        }
        #endregion

        #region Search
        /// <summary>
        /// Tìm kiếm Thông báo
        /// </summary>
        /// <author>BaLX</author>
        /// <date>7/12/2012</date>
        /// <param name="dic">Tiêu chí tìm kiếm</param>
        /// <returns>Đối tượng Service</returns>

        public IQueryable<Service> Search(IDictionary<string, object> dic)
        {
            int? ServiceID = Utils.GetNullableInt(dic, "ServiceID");
            string ServiceCode = Utils.GetString(dic, "ServiceCode");
            string ServiceName = Utils.GetString(dic, "ServiceName");
            string UnitPrice = Utils.GetString(dic, "UnitPrice");
            int? ServiceType = Utils.GetNullableInt(dic, "ServiceType");
            int? SchedularType = Utils.GetNullableInt(dic, "SchedularType");
            int? MaxSms = Utils.GetNullableInt(dic, "MaxSms");
            string Description = Utils.GetString(dic, "Description");
            int? IsStatus = Utils.ConvertBoolToByte(Utils.GetNullableBool(dic, "IsStatus"));
            string createuser = Utils.GetString(dic, "create_user");
            string updateuser = Utils.GetString(dic, "update_user");
            DateTime? createtime = Utils.GetDateTime(dic, "create_time");
            DateTime? updatetime = Utils.GetDateTime(dic, "update_time");

            IQueryable<Service> listService = this.ServiceRepository.All;

            if (ServiceID.HasValue)
            {
                listService = listService.Where(x => x.ServiceID == ServiceID);
            }
            if (!ServiceCode.Equals(string.Empty))
            {
                listService = listService.Where(x => x.ServiceCode.ToLower().Contains(ServiceCode.ToLower()));
            }
            if (!ServiceName.Equals(string.Empty))
            {
                listService = listService.Where(x => x.ServiceName.ToLower().Contains(ServiceName.ToLower()));
            }
            if (UnitPrice.Equals(string.Empty))
            {
                listService = listService.Where(x => x.UnitPrice == UnitPrice);
            }
            if (ServiceType.HasValue && ServiceType != -1)
            {
                listService = listService.Where(x => x.ServiceType == ServiceType);
            }
            if (SchedularType.HasValue && SchedularType != -1)
            {
                listService = listService.Where(x => x.SchedularType == SchedularType);
            }
            if (MaxSms.HasValue && MaxSms != -1)
            {
                listService = listService.Where(x => x.MaxSMS == MaxSms);
            }
            if (!Description.Equals(string.Empty))
            {
                listService = listService.Where(x => x.Description.ToLower().Contains(Description.ToLower()));
            }
            if (IsStatus.HasValue)
            {
                listService = listService.Where(x => x.IsStatus == IsStatus);
            }
            if (!createuser.Equals(string.Empty))
            {
                listService = listService.Where(x => x.CreateUser.ToLower().Contains(createuser.ToLower()));
            }
            if (!updateuser.Equals(string.Empty))
            {
                listService = listService.Where(x => x.UpdateUser.ToLower().Contains(updateuser.ToLower()));
            }
            if (createtime.HasValue)
            {
                listService = listService.Where(x => x.CreateTime == createtime);
            }
            if (updatetime.HasValue)
            {
                listService = listService.Where(x => x.UpdateTime == updatetime);
            }

            return listService;
        }
        #endregion

    }
}
