﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SMAS.Business.Common;
using SMAS.Models.Models;
using System.IO;
using SMAS.Business.Business;
using SMAS.Business.BusinessObject;
using SMAS.VTUtils.Excel.Export;
namespace SMAS.Business.Business
{
    public partial class ReportMarkSubjectByTeacherBusiness
    {

        public IQueryable<ClassProfileTempBO> GetListClassByTeacher(IDictionary<string, object> dic = null)
        {
            int teacherID = Utils.GetInt(dic, "TeacherID");
            int semester = Utils.GetInt(dic, "Semester");
            int subjectID = Utils.GetInt(dic, "SubjectID");
            int schoolID = Utils.GetInt(dic, "SchoolID");
            int academicYearID = Utils.GetInt(dic, "AcademicYearID");
            int appliedLevel = Utils.GetInt(dic, "AppliedLevel");
            //Thuc hien tim kiem trong bang phancong giang day theo cacs dieu kien
            IQueryable<TeachingAssignment> iqTA = TeachingAssignmentBusiness.SearchBySchool(schoolID, dic);
            IQueryable<ClassProfileTempBO> iqCP = (from p in iqTA
                                                   join q in ClassProfileBusiness.All on p.ClassID equals q.ClassProfileID
                                                   join s in EducationLevelBusiness.All on q.EducationLevelID equals s.EducationLevelID
                                                   join r in ClassSubjectBusiness.All on p.ClassID equals r.ClassID
                                                   where s.Grade == appliedLevel
                                                   && p.IsActive == true
                                                   && p.SubjectID == r.SubjectID
                                                   && ((semester == 1 && r.SectionPerWeekFirstSemester > 0) || (semester == 2 && r.SectionPerWeekSecondSemester > 0))
                                                   && (r.IsSubjectVNEN == null || r.IsSubjectVNEN == false)
                                                   && q.IsActive.Value
                                                   select new ClassProfileTempBO
                                                   {
                                                       ClassProfileID = q.ClassProfileID,
                                                       EducationLevelID = q.EducationLevelID,
                                                       DisplayName = q.DisplayName,
                                                       OrderNumber = q.OrderNumber
                                                   }).Distinct();
            return iqCP;

        }
        public ProcessedReport GetReportMarkSubjectByTeacher(ReportMarkSubjectBO entity)
        {
            string reportCode = SystemParamsInFile.REPORT_SODIEMBOMONTHEOGIAOVIEN;

            string inputParameterHashKey = GetHashKey(entity);
            return ProcessedReportBusiness.GetProcessedReport(reportCode, inputParameterHashKey);
        }
        public ProcessedReport InsertReportMarkSubjectByTeacher(ReportMarkSubjectBO entity, Stream data)
        {
            string reportCode = SystemParamsInFile.REPORT_SODIEMBOMONTHEOGIAOVIEN;

            ReportDefinition reportDef = ReportDefinitionBusiness.GetByCode(reportCode);
            ProcessedReport pr = new ProcessedReport();
            pr.ReportCode = reportCode;
            pr.ProcessedDate = DateTime.Now;
            pr.InputParameterHashKey = GetHashKey(entity);
            pr.ReportData = ReportUtils.Compress(data);


            //Tạo tên file HS_[SchoolLevel]_BangDiemTH_[EducationLevel/Class]_[Semester]

            //Tạo tên file HS_[SchoolLevel]_BangDiem_[Class]_[Semester]_[Subject]
            //SoDiemBoMon_[Subject]_[Semester]
            string outputNamePattern = reportDef.OutputNamePattern;
            string semester = ReportUtils.ConvertSemesterForReportName(entity.Semester);
            string schoolLevel = ReportUtils.ConvertAppliedLevelForReportName(entity.AppliedLevel);
            string subject = SubjectCatBusiness.Find(entity.SubjectID).DisplayName;


            outputNamePattern = outputNamePattern.Replace("[Semester]", ReportUtils.StripVNSign(semester));
            outputNamePattern = outputNamePattern.Replace("[Subject]", ReportUtils.StripVNSign(subject));
            pr.ReportName = ReportUtils.RemoveSpecialCharacters(outputNamePattern) + "." + reportDef.OutputFormat;




            IDictionary<string, object> dic = new Dictionary<string, object> {
                {"AcademicYearID", entity.AcademicYearID},
                {"SchoolID ", entity.SchoolID},
                {"TeacherID", entity.TeacherID},
                {"SubjectID", entity.SubjectID},
                {"Semester", entity.Semester}
            };
            ProcessedReportParameterRepository.Insert(dic, pr);
            ProcessedReportRepository.Insert(pr);
            ProcessedReportRepository.Save();
            return pr;
        }
        public Stream CreateReportMarkSubjectByTeacher(ReportMarkSubjectBO entity)
        {
            System.Threading.Thread.CurrentThread.CurrentCulture = System.Globalization.CultureInfo.CreateSpecificCulture("en-US");
            IFormatProvider provider = new System.Globalization.CultureInfo("en-US", true);
            string reportCode = SystemParamsInFile.REPORT_SODIEMBOMONTHEOGIAOVIEN;
            ReportDefinition reportDef = ReportDefinitionBusiness.GetByCode(reportCode);

            string templatePath = SystemParamsInFile.TEMPLATE_FOLDER + reportDef.TemplateDirectory + "/" + reportDef.TemplateName;
            IVTWorkbook oBook = VTExport.OpenWorkbook(templatePath);
            IVTWorksheet oSheet = null;
            //Lấy sheet template
            IVTWorksheet firstsheet = oBook.GetSheet(1);
            IVTRange rangeHK = firstsheet.GetRange("Y5", "Y6");
            IVTRange rangeTBM = firstsheet.GetRange("Z5", "Z6");
            IVTRange rangeHKII = firstsheet.GetRange("AA5", "AD6");
            IVTRange rangeSoDiem = firstsheet.GetRange("AP2", "AW3");
            IVTRange rangeMonHoc = firstsheet.GetRange("AP4", "AW4");

            int academicYearID = entity.AcademicYearID;
            int schoolID = entity.SchoolID;
            int teacherID = entity.TeacherID;
            int subjectID = entity.SubjectID;
            int semester = entity.Semester;
            int appliedLevel = entity.AppliedLevel;
            AcademicYear academicYear = AcademicYearBusiness.Find(academicYearID);
            SchoolProfile sp = SchoolProfileBusiness.Find(schoolID);
            SubjectCat subjectCat = SubjectCatBusiness.Find(subjectID);
            firstsheet.SetCellValue("A2", sp.SupervisingDept.SupervisingDeptName.ToUpper());
            firstsheet.SetCellValue("A3", sp.SchoolName.ToUpper());
            string titleHK = "";
            if (semester == SystemParamsInFile.SEMESTER_OF_YEAR_FIRST)
                titleHK = "HKI";
            else
                titleHK = "HKII";
            firstsheet.SetCellValue("A4", "Năm học: " + academicYear.Year + " - " + (academicYear.Year + 1) + ", " + titleHK);
            //Lay danh sach lop hoc
            IDictionary<string, object> dic = new Dictionary<string, object>();
            dic["AcademicYearID"] = academicYearID;
            dic["SchoolID"] = schoolID;
            dic["TeacherID"] = teacherID;
            dic["SubjectID"] = subjectID;
            dic["Semester"] = semester;
            dic["AppliedLevel"] = appliedLevel;

            IQueryable<ClassProfileTempBO> iqCP = this.GetListClassByTeacher(dic);
            List<ClassProfileTempBO> lstCP = iqCP.ToList();
            lstCP = lstCP.OrderBy(o => o.EducationLevelID).ThenBy(o => o.DisplayName).ToList();
            //Lấy khai báo số con điểm theo trường

            IEnumerable<string> titles = MarkRecordBusiness.GetMarkSemesterTitle(schoolID, academicYearID, semester).Split(',').Where(u => !string.IsNullOrEmpty(u));
            var titleGroup = titles.Select(u => u[0]).GroupBy(u => u).Select(v => new { StartCharacter = v.Key, Count = v.Count() });
            //Lấy danh sách học sinh trong trường
            IDictionary<string, object> dicPupilOfClass = new Dictionary<string, object>();
            dicPupilOfClass["AcademicYearID"] = academicYearID;
            dicPupilOfClass["CheckWithClass"] = "CheckWithClass";
            IQueryable<PupilOfClass> iqPupil = PupilOfClassBusiness.SearchBySchool(schoolID, dicPupilOfClass);

            List<int> lstPupilRemove = new List<int>();
            if ((academicYear.IsShowPupil.HasValue && academicYear.IsShowPupil.Value == true))
            {
                lstPupilRemove = iqPupil.Where(x => x.Status != SystemParamsInFile.PUPIL_STATUS_GRADUATED
                                            && x.Status != SystemParamsInFile.PUPIL_STATUS_STUDYING).Select(x => x.PupilID).ToList();
                iqPupil = iqPupil.Where(x => x.Status == SystemParamsInFile.PUPIL_STATUS_GRADUATED
                                            || x.Status == SystemParamsInFile.PUPIL_STATUS_STUDYING);
            }

            List<PupilOfClassBO> lstPupil = iqPupil.Select(u => new PupilOfClassBO
            {
                PupilOfClassID = u.PupilOfClassID,
                PupilID = u.PupilID,
                ClassID = u.ClassID,
                Status = u.Status,
                PupilCode = u.PupilProfile.PupilCode,
                Name = u.PupilProfile.Name,
                PupilFullName = u.PupilProfile.FullName,
                OrderInClass = u.OrderInClass
            }).ToList();
            List<PupilOfClassBO> lstPocHKI = iqPupil.AddCriteriaSemester(academicYear, SystemParamsInFile.SEMESTER_OF_YEAR_FIRST).Select(u => new PupilOfClassBO
            {
                PupilOfClassID = u.PupilOfClassID,
                PupilID = u.PupilID,
                ClassID = u.ClassID,
                Status = u.Status,
                PupilCode = u.PupilProfile.PupilCode,
                Name = u.PupilProfile.Name,
                PupilFullName = u.PupilProfile.FullName,
                OrderInClass = u.OrderInClass
            }).ToList();
            List<PupilOfClassBO> lstPocHKII = new List<PupilOfClassBO>();
            if (semester == SystemParamsInFile.SEMESTER_OF_YEAR_SECOND)
            {
                lstPocHKII = iqPupil.AddCriteriaSemester(academicYear, SystemParamsInFile.SEMESTER_OF_YEAR_SECOND).Select(u => new PupilOfClassBO
                {
                    PupilOfClassID = u.PupilOfClassID,
                    PupilID = u.PupilID,
                    ClassID = u.ClassID,
                    Status = u.Status,
                    PupilCode = u.PupilProfile.PupilCode,
                    Name = u.PupilProfile.Name,
                    PupilFullName = u.PupilProfile.FullName,
                    OrderInClass = u.OrderInClass
                }).ToList();
            }

            //Kiểm tra môn học là môn học nhận xét hay là môn tính điểm trong trường

            //Lấy danh sách điểm học sinh trong trường
            IDictionary<string, object> dicMark = new Dictionary<string, object>();
            dicMark["AcademicYearID"] = academicYearID;
            dicMark["SubjectID"] = subjectID;
            dicMark["Semester"] = semester;
            dicMark["Year"] = academicYear.Year;
            int? isCommenting = SchoolSubjectBusiness.SearchBySchool(schoolID, dicMark).FirstOrDefault().IsCommenting;
            List<MarkRecordBO> lstMarkRecord = new List<MarkRecordBO>();
            List<JudgeRecordBO> lstJudgeRecord = new List<JudgeRecordBO>();
            if (isCommenting == SystemParamsInFile.ISCOMMENTING_TYPE_JUDGE)
            {
                lstJudgeRecord = VJudgeRecordBusiness.SearchBySchool(schoolID, dicMark).Select(u => new JudgeRecordBO
                {
                    JudgeRecordID = u.JudgeRecordID,
                    ClassID = u.ClassID,
                    PupilID = u.PupilID,
                    AcademicYearID = u.AcademicYearID,
                    SchoolID = u.SchoolID,
                    MarkTypeID = u.MarkTypeID,
                    Title = u.Title,
                    Semester = u.Semester,
                    Judgement = u.Judgement
                }).ToList();
            }
            else
            {
                lstMarkRecord = VMarkRecordBusiness.SearchBySchool(schoolID, dicMark).Select(u => new MarkRecordBO
                {
                    ClassID = u.ClassID,
                    MarkRecordID = u.MarkRecordID,
                    PupilID = u.PupilID,
                    AcademicYearID = u.AcademicYearID,
                    SchoolID = u.SchoolID,
                    Mark = u.Mark,
                    Semester = u.Semester,
                    MarkTypeID = u.MarkTypeID,
                    OrderNumber = u.OrderNumber,
                    Title = u.Title
                }).ToList();
            }
            //Lấy điểm tổng kết
            IDictionary<string, object> dicSummedUp = new Dictionary<string, object>();
            dicSummedUp["AcademicYearID"] = academicYearID;
            dicSummedUp["SubjectID"] = subjectID;
            List<SummedUpRecordBO> lstSummedUpRecord = VSummedUpRecordBusiness.SearchBySchool(schoolID, dicSummedUp).Select(u => new SummedUpRecordBO
            {
                SummedUpRecordID = u.SummedUpRecordID,
                SummedUpMark = u.SummedUpMark,
                JudgementResult = u.JudgementResult,
                ReTestJudgement = u.ReTestJudgement,
                ReTestMark = u.ReTestMark,
                SubjectID = u.SubjectID,
                ClassID = u.ClassID,
                Semester = u.Semester,
                PeriodID = u.PeriodID,
                PupilID = u.PupilID
            }).ToList();
            List<SummedUpRecordBO> lstSummedUpRecordHKI = new List<SummedUpRecordBO>();
            List<SummedUpRecordBO> lstSummedUpRecordHKII = new List<SummedUpRecordBO>();
            List<SummedUpRecordBO> lstSummedUpRecordHK_CN = new List<SummedUpRecordBO>();
            if (semester == SystemParamsInFile.SEMESTER_OF_YEAR_FIRST)
            {
                lstSummedUpRecord = lstSummedUpRecord.Where(o => o.Semester == SystemParamsInFile.SEMESTER_OF_YEAR_FIRST && o.PeriodID == null).ToList();
            }
            if (semester == SystemParamsInFile.SEMESTER_OF_YEAR_SECOND)
            {
                lstSummedUpRecordHKI = lstSummedUpRecord.Where(o => o.Semester == SystemParamsInFile.SEMESTER_OF_YEAR_FIRST && o.PeriodID == null).ToList();
                lstSummedUpRecordHKII = lstSummedUpRecord.Where(o => o.Semester == SystemParamsInFile.SEMESTER_OF_YEAR_SECOND && o.PeriodID == null).ToList();
                lstSummedUpRecordHK_CN = lstSummedUpRecord.Where(o => o.Semester >= SystemParamsInFile.SEMESTER_OF_YEAR_ALL && o.PeriodID == null).ToList();
                lstSummedUpRecordHK_CN = lstSummedUpRecordHK_CN.Where(o => o.Semester == lstSummedUpRecordHK_CN.Where(u => u.PupilID == o.PupilID && u.SubjectID == o.SubjectID && u.ClassID == o.ClassID).Select(u => u.Semester).Max()).ToList();
            }

            if (lstPupilRemove.Count() > 0 && (academicYear.IsShowPupil.HasValue && academicYear.IsShowPupil.Value == true))
            {
                if (isCommenting == SystemParamsInFile.ISCOMMENTING_TYPE_JUDGE)
                {
                    lstJudgeRecord = lstJudgeRecord.Where(x => x.PupilID.HasValue && !lstPupilRemove.Contains(x.PupilID.Value)).ToList();
                }
                else
                {
                    lstMarkRecord = lstMarkRecord.Where(x => x.PupilID.HasValue && !lstPupilRemove.Contains(x.PupilID.Value)).ToList();
                }

                lstSummedUpRecord = lstSummedUpRecord.Where(x => !lstPupilRemove.Contains(x.PupilID)).ToList();
                lstSummedUpRecordHKI = lstSummedUpRecordHKI.Where(x => !lstPupilRemove.Contains(x.PupilID)).ToList();
                lstSummedUpRecordHKII = lstSummedUpRecordHKII.Where(x => !lstPupilRemove.Contains(x.PupilID)).ToList();
                lstSummedUpRecordHK_CN = lstSummedUpRecordHK_CN.Where(x => !lstPupilRemove.Contains(x.PupilID)).ToList();
            }

            int Subject_Start_Title_Row_Index = 7;
            int countM = titleGroup.Where(o => o.StartCharacter == 'M').Select(o => o.Count).FirstOrDefault();
            int countP = titleGroup.Where(o => o.StartCharacter == 'P').Select(o => o.Count).FirstOrDefault();
            int countV = titleGroup.Where(o => o.StartCharacter == 'V').Select(o => o.Count).FirstOrDefault();
            int countMPV = countM + countP + countV;
            //Tạo template firstSheet
            int columnIndex = 3;
            int mergeIndex = 3;

            foreach (var title in titleGroup)
            {
                var group = titles.Where(u => u.StartsWith(title.StartCharacter.ToString())).OrderBy(u => u);
                string str = "";
                if (group.FirstOrDefault().Trim().StartsWith("M"))
                {
                    str = "Miệng";
                }
                else
                {
                    str = "Viết";
                }

                IVTRange rangeM = firstsheet.GetRange(Subject_Start_Title_Row_Index - 1, mergeIndex, Subject_Start_Title_Row_Index - 1, mergeIndex + group.Count() - 1);
                rangeM.Merge();
                foreach (var titleName in group)
                {
                    firstsheet.SetCellValue(Subject_Start_Title_Row_Index - 1, mergeIndex, str);
                }
                if (group.FirstOrDefault().Trim().StartsWith("M"))
                {
                    IVTRange range = firstsheet.GetRange(Subject_Start_Title_Row_Index - 2, columnIndex, Subject_Start_Title_Row_Index - 2, mergeIndex + countM + countP - 1);
                    range.Merge();
                    firstsheet.SetCellValue(Subject_Start_Title_Row_Index - 2, mergeIndex, "Hệ số 1");
                }
                if (group.FirstOrDefault().Trim().StartsWith("V"))
                {
                    IVTRange rangeV = firstsheet.GetRange(Subject_Start_Title_Row_Index - 2, columnIndex + countM + countP, Subject_Start_Title_Row_Index - 2, columnIndex + countMPV - 1);
                    rangeV.Merge();
                    firstsheet.SetCellValue(Subject_Start_Title_Row_Index - 2, mergeIndex, "Hệ số 2");
                }

                mergeIndex += group.Count();
                //columnIndex += group.Count();
            }
            if (countMPV > 3)
            {
                firstsheet.CopyPasteSameSize(rangeSoDiem, 2, countMPV - 3);
                firstsheet.CopyPasteSameSize(rangeMonHoc, 4, countMPV - 3);
            }
            if (semester == 1)
            {
                firstsheet.CopyPasteSameSize(rangeHK, 5, 3 + countMPV);
                firstsheet.CopyPasteSameSize(rangeTBM, 5, 4 + countMPV);

            }
            else
            {
                firstsheet.CopyPasteSameSize(rangeHKII, 5, 3 + countMPV);
            }

            //IVTRange rangeTH = firstsheet.GetRange("B60", "T62");
            //IVTRange rangeTK_NX = firstsheet.GetRange("B64", "H66");


            //Mỗi lớp là một sheet
            string subjectName = subjectCat.DisplayName.ToUpper();
            if (!subjectCat.DisplayName.Trim().ToUpper().Contains("MÔN"))
            {
                subjectName = "MÔN " + subjectCat.DisplayName.ToUpper();
            }




            if (semester == SystemParamsInFile.SEMESTER_OF_YEAR_FIRST)
            {
                string columnTK = GetExcelColumnName(4 + countMPV);
                IVTRange rangeTH = firstsheet.GetRange("B60", "T62");
                IVTRange rangeTK_NX = firstsheet.GetRange("B64", "H66");
                foreach (ClassProfileTempBO cp in lstCP)
                {
                    DemSoLuong objSoLuongHK1 = new DemSoLuong();
                    int ClassID = cp.ClassProfileID;
                    List<SummedUpRecordBO> lstSummedUpRecord_Class = lstSummedUpRecord.Where(o => o.ClassID == ClassID).ToList();
                    List<MarkRecordBO> lstMarkRecord_Class = lstMarkRecord.Where(o => o.ClassID == ClassID).ToList();
                    List<JudgeRecordBO> lstJudgeRecord_Class = lstJudgeRecord.Where(o => o.ClassID == ClassID).ToList();
                    oSheet = oBook.CopySheetToLast(firstsheet, columnTK + 6);
                    oSheet.Name =Utils.StripVNSignAndSpace(cp.DisplayName);
                    string className = cp.DisplayName.ToUpper();

                    if (!cp.DisplayName.Trim().ToUpper().Contains("LỚP"))
                    {
                        className = "LỚP " + cp.DisplayName.ToUpper();
                    }

                    oSheet.SetCellValue(4, countMPV - 3, subjectName.ToUpper() + " - " + className.ToUpper());
                    //Lấy học sinh trong lớp
                    List<PupilOfClassBO> lstPupilOfClass = lstPupil.Where(o => o.ClassID == ClassID).ToList();
                    lstPupilOfClass = lstPupilOfClass.OrderBy(o => o.OrderInClass).ThenBy(o => o.Name).ThenBy(o => o.PupilFullName).ToList();

                    //Tạo các range cho các học sinh
                    IVTRange topRange = firstsheet.GetRange(7, 1, 7, 2 + countMPV);
                    IVTRange lastRange = firstsheet.GetRange(11, 1, 11, 2 + countMPV);
                    IVTRange middleRange = firstsheet.GetRange(8, 1, 8, 2 + countMPV);
                    IVTRange redmiddleRange = firstsheet.GetRange(13, 1, 13, 2 + countMPV);
                    IVTRange redlastRange = firstsheet.GetRange(16, 1, 16, 2 + countMPV);
                    //Tạo range 2 cột cuối
                    IVTRange topRange_HKI = firstsheet.GetRange(7, 25, 7, 26);
                    IVTRange lastRange_HKI = firstsheet.GetRange(11, 25, 11, 26);
                    IVTRange middleRange_HKI = firstsheet.GetRange(8, 25, 8, 26);
                    IVTRange redmiddleRange_HKI = firstsheet.GetRange(13, 25, 13, 26);
                    IVTRange redlastRange_HKI = firstsheet.GetRange(16, 25, 16, 26);

                    int order = 1;
                    int rowIndex = Subject_Start_Title_Row_Index;
                    int countPupil = lstPupilOfClass.Count();
                    List<PupilOfClassBO> lstPocClass = lstPocHKI.Where(o => o.ClassID == ClassID).ToList();
                    int countPupilHK = lstPocClass.Count();

                    //Dem so luong gioi, kha, TB, yeu, kem, D, CD
                    //Mon tinh diem
                    if (lstSummedUpRecord_Class != null && isCommenting == GlobalConstants.ISCOMMENTING_TYPE_MARK)
                    {
                        objSoLuongHK1.SoLuongGioi = lstSummedUpRecord_Class.Count(s => s.SummedUpMark.HasValue && s.SummedUpMark >= 8.0m);
                        objSoLuongHK1.SoLuongKha = lstSummedUpRecord_Class.Count(s => s.SummedUpMark.HasValue && s.SummedUpMark >= 6.5m && s.SummedUpMark < 8.0m);
                        objSoLuongHK1.SoLuongTB = lstSummedUpRecord_Class.Count(s => s.SummedUpMark.HasValue && s.SummedUpMark >= 5.0m && s.SummedUpMark < 6.5m);
                        objSoLuongHK1.SoLuongYeu = lstSummedUpRecord_Class.Count(s => s.SummedUpMark.HasValue && s.SummedUpMark >= 3.5m && s.SummedUpMark < 5.0m);
                        objSoLuongHK1.SoLuongKem = lstSummedUpRecord_Class.Count(s => s.SummedUpMark.HasValue && s.SummedUpMark < 3.5m);
                    }

                    if (lstSummedUpRecord_Class != null && isCommenting == GlobalConstants.ISCOMMENTING_TYPE_JUDGE)
                    {
                        objSoLuongHK1.SoLuongDat = lstSummedUpRecord_Class.Count(s => s.JudgementResult == GlobalConstants.PASS);
                        objSoLuongHK1.SoLuongCD = lstSummedUpRecord_Class.Count(s => s.JudgementResult == GlobalConstants.NOPASS);
                    }



                    for (int i = 0; i < lstPupilOfClass.Count(); i++)
                    {
                        PupilOfClassBO pp = lstPupilOfClass[i];
                        //Xét template cho học sinh
                        if (pp.Status == SystemParamsInFile.PUPIL_STATUS_STUDYING || pp.Status == SystemParamsInFile.PUPIL_STATUS_GRADUATED)
                        {
                            if (i == 0 && countPupil != 1)
                            {
                                oSheet.CopyPasteSameSize(topRange, rowIndex, 1);
                                oSheet.CopyPasteSameSize(topRange_HKI, rowIndex, 3 + countMPV);
                            }
                            else if ((i + 1) % 5 == 0 || (i + 1) == countPupil)
                            {
                                oSheet.CopyPasteSameSize(lastRange, rowIndex, 1);
                                oSheet.CopyPasteSameSize(lastRange_HKI, rowIndex, 3 + countMPV);
                            }
                            else
                            {
                                oSheet.CopyPasteSameSize(middleRange, rowIndex, 1);
                                oSheet.CopyPasteSameSize(middleRange_HKI, rowIndex, 3 + countMPV);
                            }
                        }
                        else
                        {
                            if ((i + 1) % 5 == 0 || (i + 1) == countPupil)
                            {
                                oSheet.CopyPasteSameSize(redlastRange, rowIndex, 1);
                                oSheet.CopyPasteSameSize(redlastRange_HKI, rowIndex, 3 + countMPV);
                            }
                            else
                            {
                                oSheet.CopyPasteSameSize(redmiddleRange, rowIndex, 1);
                                oSheet.CopyPasteSameSize(redmiddleRange_HKI, rowIndex, 3 + countMPV);
                            }

                        }
                        //Kẻ thêm các ô khi kết thúc các con điểm M, P, V
                        IVTRange rangeM_Think = oSheet.GetRange(6, 2 + countM, 6 + countPupil, 2 + countM);
                        rangeM_Think.SetBorder(VTBorderStyle.Solid, VTBorderWeight.Thin, VTBorderIndex.EdgeRight);
                        IVTRange rangeP_Think = oSheet.GetRange(5, 2 + countM + countP, 6 + countPupil, 2 + countM + countP);
                        rangeP_Think.SetBorder(VTBorderStyle.Solid, VTBorderWeight.Thin, VTBorderIndex.EdgeRight);


                        int colIndex = 3;
                        oSheet.SetCellValue("A" + rowIndex, order++);
                        oSheet.SetCellValue("B" + rowIndex, pp.PupilFullName);
                        bool showData = lstPocClass.Any(o => o.PupilID == pp.PupilID);
                        if (showData)
                        {
                            //Lấy dữ liệu điểm của học sinh
                            var pupilMarks = lstMarkRecord_Class.Where(u => u.PupilID == pp.PupilID);
                            var pupilJudges = lstJudgeRecord_Class.Where(u => u.PupilID == pp.PupilID);
                            var pMark = lstSummedUpRecord_Class.Where(u => u.PupilID == pp.PupilID).Select(u => u.SummedUpMark).FirstOrDefault();
                            var pJudge = lstSummedUpRecord_Class.Where(u => u.PupilID == pp.PupilID).Select(u => u.JudgementResult).FirstOrDefault();
                            var pupilSummed = isCommenting == SystemParamsInFile.ISCOMMENTING_TYPE_JUDGE ? (!string.IsNullOrEmpty(pJudge) ? pJudge : string.Empty) : (pMark.HasValue ? ReportUtils.ConvertMarkForV(pMark.Value) : string.Empty);
                            foreach (var title in titleGroup)
                            {
                                foreach (var titleName in titles.Where(u => u.StartsWith(title.StartCharacter.ToString())).OrderBy(u => u))
                                {
                                    if (isCommenting == SystemParamsInFile.ISCOMMENTING_TYPE_JUDGE)
                                    {
                                        JudgeRecordBO judgeRecord = pupilJudges.Where(u => u.Title == titleName).FirstOrDefault();
                                        oSheet.SetCellValue(rowIndex, colIndex, judgeRecord != null ? judgeRecord.Judgement : string.Empty);
                                    }
                                    else
                                    {
                                        MarkRecordBO markRecord = pupilMarks.Where(u => u.Title == titleName).FirstOrDefault();
                                        if (markRecord != null)
                                        {
                                            if (titleName.Contains("M"))
                                            {
                                                string mark = ReportUtils.ConvertMarkForM(markRecord.Mark.Value);
                                                oSheet.SetCellValue(rowIndex, colIndex, mark);
                                            }
                                            else if (titleName.Contains("P") || titleName.Contains("V"))
                                            {
                                                string mark = ReportUtils.ConvertMarkForV(markRecord.Mark.Value);
                                                oSheet.SetCellValue(rowIndex, colIndex, mark);
                                            }
                                            else if (titleName.Contains("HK"))
                                            {
                                                string mark = ReportUtils.ConvertMarkForV(markRecord.Mark.Value);
                                                oSheet.SetCellValue(rowIndex, colIndex, mark);
                                            }
                                        }
                                    }
                                    colIndex++;
                                }
                            }
                            if (isCommenting == SystemParamsInFile.ISCOMMENTING_TYPE_JUDGE)
                            {
                                oSheet.SetCellValue(rowIndex, colIndex, pupilSummed);


                            }
                            else
                            {
                                if (pupilSummed != null && pupilSummed != "")
                                    oSheet.SetCellValue(rowIndex, colIndex, pupilSummed);


                            }
                        }
                        rowIndex++;

                    }

                    //Tạo công thức cho môn tính điểm
                    int increaseCol = 3;
                    if (isCommenting != SystemParamsInFile.ISCOMMENTING_TYPE_JUDGE)
                    {
                        oSheet.CopyPasteSameSize(rangeTH, countPupil + 8, 2);
                        //giỏi
                        //string gioi = "=COUNTIF(" + columnTK + "7:" + (columnTK + (countPupil + 6)) + "," + '"' + ">=8.0" + '"' + ")";
                        string gioiPercent = "=100*ROUND(C" + (countPupil + 10) + "/" + countPupilHK + ",4)";
                        oSheet.SetCellValue(countPupil + 10, increaseCol, objSoLuongHK1.SoLuongGioi);
                        oSheet.SetFormulaValue(countPupil + 10, increaseCol + 1, gioiPercent.Replace(",", ";").Replace("\'", "\""));
                        //khá =COUNTIFS(V7:V58,">=6.5",V7:V58,"<8")
                        increaseCol += 3;
                        //string kha = "=COUNTIF(" + columnTK + "7:" + (columnTK + (countPupil + 6)) + "," + '"' + ">=6.5" + '"' + ")" + "-" + "COUNTIF(" + columnTK + "7:" + (columnTK + (countPupil + 6)) + "," + '"' + ">=8.0" + '"' + ")";
                        string khaPercent = "=100*ROUND(F" + (countPupil + 10) + "/" + countPupilHK + ",4)";
                        oSheet.SetCellValue(countPupil + 10, increaseCol, objSoLuongHK1.SoLuongKha);
                        oSheet.SetFormulaValue(countPupil + 10, increaseCol + 1, khaPercent.Replace(",", ";").Replace("\'", "\""));
                        //trung bình
                        increaseCol += 3;
                        //string tb = "=COUNTIF(" + columnTK + "7:" + (columnTK + (countPupil + 6)) + "," + '"' + ">=5.0" + '"' + ")" + "-" + "COUNTIF(" + columnTK + "7:" + (columnTK + (countPupil + 6)) + "," + '"' + ">=6.5" + '"' + ")";
                        string tbPercent = "=100*ROUND(I" + (countPupil + 10) + "/" + countPupilHK + ",4)";
                        oSheet.SetCellValue(countPupil + 10, increaseCol, objSoLuongHK1.SoLuongTB);
                        oSheet.SetFormulaValue(countPupil + 10, increaseCol + 1, tbPercent.Replace(",", ";").Replace("\'", "\""));
                        //yếu 
                        increaseCol += 3;
                        //string yeu = "=COUNTIF(" + columnTK + "7:" + (columnTK + (countPupil + 6)) + "," + '"' + ">=3.5" + '"' + ")" + "-" + "COUNTIF(" + columnTK + "7:" + (columnTK + (countPupil + 6)) + "," + '"' + ">=5.0" + '"' + ")";
                        string yeuPercent = "=100*ROUND(L" + (countPupil + 10) + "/" + countPupilHK + ",4)";
                        oSheet.SetCellValue(countPupil + 10, increaseCol, objSoLuongHK1.SoLuongYeu);
                        oSheet.SetFormulaValue(countPupil + 10, increaseCol + 1, yeuPercent.Replace(",", ";").Replace("\'", "\""));
                        //Kém
                        increaseCol += 3;
                        //string kem = "=COUNTIF(" + columnTK + "7:" + (columnTK + (countPupil + 6)) + "," + '"' + ">=0.0" + '"' + ")" + "-" + "COUNTIF(" + columnTK + "7:" + (columnTK + (countPupil + 6)) + "," + '"' + ">=3.5" + '"' + ")";
                        string kemPercent = "=100*ROUND(O" + (countPupil + 10) + "/" + countPupilHK + ",4)";
                        oSheet.SetCellValue(countPupil + 10, increaseCol, objSoLuongHK1.SoLuongKem);
                        oSheet.SetFormulaValue(countPupil + 10, increaseCol + 1, kemPercent.Replace(",", ";").Replace("\'", "\""));
                        //Trên trung bình
                        increaseCol += 3;
                        //string upTb = "=COUNTIF(" + columnTK + "7:" + (columnTK + (countPupil + 6)) + "," + '"' + ">=5.0" + '"' + ")";
                        string upTbPercent = "=100*ROUND(R" + (countPupil + 10) + "/" + countPupilHK + ",4)";
                        objSoLuongHK1.SoLuongTrenTB = objSoLuongHK1.SoLuongGioi + objSoLuongHK1.SoLuongKha + objSoLuongHK1.SoLuongTB;
                        oSheet.SetCellValue(countPupil + 10, increaseCol, objSoLuongHK1.SoLuongTrenTB);
                        oSheet.SetFormulaValue(countPupil + 10, increaseCol + 1, upTbPercent.Replace(",", ";").Replace("\'", "\""));

                    }
                    else
                    {
                        oSheet.CopyPasteSameSize(rangeTK_NX, countPupil + 8, 2);
                        //string pass = "=COUNTIF(" + columnTK + "7:" + (columnTK + (countPupil + 6)) + "," + '"' + "=Đ" + '"' + ")";
                        string passPercent = "=100*ROUND(C" + (countPupil + 10) + "/" + countPupilHK + ",4)";
                        oSheet.SetCellValue(countPupil + 10, increaseCol, objSoLuongHK1.SoLuongDat);
                        oSheet.SetFormulaValue(countPupil + 10, increaseCol + 1, passPercent.Replace(",", ";").Replace("\'", "\""));
                        increaseCol += 3;
                        //string fail = "=COUNTIF(" + columnTK + "7:" + (columnTK + (countPupil + 6)) + "," + '"' + "=CĐ" + '"' + ")";
                        string failPercent = "=100*ROUND(F" + (countPupil + 10) + "/" + countPupilHK + ",4)";
                        oSheet.SetCellValue(countPupil + 10, increaseCol, objSoLuongHK1.SoLuongCD);
                        oSheet.SetFormulaValue(countPupil + 10, increaseCol + 1, failPercent.Replace(",", ";").Replace("\'", "\""));

                    }
                    oSheet.PageMaginLeft = 0.5;
                    oSheet.PageMaginRight = 0.5;
                    oSheet.PageMaginTop = 0.5;
                    oSheet.PageMaginBottom = 0.5;
                    oSheet.PageSize = VTXPageSize.VTxlPaperA4;
                    oSheet.FitAllColumnsOnOnePage = true;

                }

            }
            else
            {
                string columnTK_CN = GetExcelColumnName(6 + countMPV);
                string columnTK_HKI = GetExcelColumnName(5 + countMPV);
                string columnTK_HKII = GetExcelColumnName(4 + countMPV);
                IVTRange rangeTH = firstsheet.GetRange("B68", "T72");
                IVTRange rangeTK_NX = firstsheet.GetRange("B74", "H78");
                foreach (ClassProfileTempBO cp in lstCP)
                {
                    DemSoLuong objSoLuongHK1 = new DemSoLuong();
                    DemSoLuong objSoLuongHK2 = new DemSoLuong();
                    DemSoLuong objSoLuongCN = new DemSoLuong();
                    int ClassID = cp.ClassProfileID;
                    List<SummedUpRecordBO> lstSummedUpRecordHKII_Class = lstSummedUpRecordHKII.Where(o => o.ClassID == ClassID).ToList();
                    List<SummedUpRecordBO> lstSummedUpRecordHKI_Class = lstSummedUpRecordHKI.Where(o => o.ClassID == ClassID).ToList();
                    List<SummedUpRecordBO> lstSummedUpRecordHK_CN_Class = lstSummedUpRecordHK_CN.Where(o => o.ClassID == ClassID).ToList();
                    List<MarkRecordBO> lstMarkRecord_Class = lstMarkRecord.Where(o => o.ClassID == ClassID).ToList();
                    List<JudgeRecordBO> lstJudgeRecord_Class = lstJudgeRecord.Where(o => o.ClassID == ClassID).ToList();
                    oSheet = oBook.CopySheetToLast(firstsheet, columnTK_CN + 6);
                    oSheet.Name =Utils.StripVNSignAndSpace(cp.DisplayName);
                    int rowStart = 7;
                    string className = cp.DisplayName.ToUpper();

                    if (!cp.DisplayName.Trim().ToUpper().Contains("LỚP"))
                    {
                        className = "LỚP " + cp.DisplayName.ToUpper();
                    }

                    oSheet.SetCellValue(4, countMPV - 3, subjectName.ToUpper() + " - " + className.ToUpper());
                    //Lấy học sinh trong lớp
                    List<PupilOfClassBO> lstPupilOfClass = lstPupil.Where(o => o.ClassID == ClassID).ToList();
                    lstPupilOfClass = lstPupilOfClass.OrderBy(o => o.OrderInClass).ThenBy(o => o.Name).ThenBy(o => o.PupilFullName).ToList();

                    //Tạo các range cho các học sinh
                    IVTRange topRange = firstsheet.GetRange(rowStart, 1, rowStart, 2 + countMPV);
                    IVTRange lastRange = firstsheet.GetRange(rowStart + 4, 1, rowStart + 4, 2 + countMPV);
                    IVTRange middleRange = firstsheet.GetRange(rowStart + 1, 1, rowStart + 1, 2 + countMPV);
                    IVTRange redmiddleRange = firstsheet.GetRange(rowStart + 6, 1, rowStart + 6, 2 + countMPV);
                    IVTRange redlastRange = firstsheet.GetRange(rowStart + 9, 1, rowStart + 9, 2 + countMPV);
                    //Tạo range 2 cột cuối
                    IVTRange topRange_HKII = firstsheet.GetRange(rowStart, 27, rowStart, 30);
                    IVTRange lastRange_HKII = firstsheet.GetRange(rowStart + 4, 27, rowStart + 4, 30);
                    IVTRange middleRange_HKII = firstsheet.GetRange(rowStart + 1, 27, rowStart + 1, 30);
                    IVTRange redmiddleRange_HKII = firstsheet.GetRange(rowStart + 6, 27, rowStart + 6, 30);
                    IVTRange redlastRange_HKII = firstsheet.GetRange(rowStart + 9, 27, rowStart + 9, 30);

                    int order = 1;
                    int rowIndex = Subject_Start_Title_Row_Index;
                    int countPupil = lstPupilOfClass.Count();
                    List<PupilOfClassBO> lstPocClass = lstPocHKII.Where(o => o.ClassID == ClassID).ToList();
                    List<PupilOfClassBO> lstPocClassHKI = lstPocHKI.Where(o => o.ClassID == ClassID).ToList();
                    int countPupilHK = lstPocClass.Count();
                    int countPupilHKI = lstPocClassHKI.Count();

                    //Dem so luong gioi, kha, TB, yeu, kem, D, CD
                    //Mon tinh diem
                    if (lstSummedUpRecordHKI_Class != null && isCommenting == GlobalConstants.ISCOMMENTING_TYPE_MARK)
                    {
                        objSoLuongHK1.SoLuongGioi = lstSummedUpRecordHKI_Class.Count(s => s.SummedUpMark.HasValue && s.SummedUpMark >= 8.0m);
                        objSoLuongHK1.SoLuongKha = lstSummedUpRecordHKI_Class.Count(s => s.SummedUpMark.HasValue && s.SummedUpMark >= 6.5m && s.SummedUpMark < 8.0m);
                        objSoLuongHK1.SoLuongTB = lstSummedUpRecordHKI_Class.Count(s => s.SummedUpMark.HasValue && s.SummedUpMark >= 5.0m && s.SummedUpMark < 6.5m);
                        objSoLuongHK1.SoLuongYeu = lstSummedUpRecordHKI_Class.Count(s => s.SummedUpMark.HasValue && s.SummedUpMark >= 3.5m && s.SummedUpMark < 5.0m);
                        objSoLuongHK1.SoLuongKem = lstSummedUpRecordHKI_Class.Count(s => s.SummedUpMark.HasValue && s.SummedUpMark < 3.5m);
                    }



                    if (lstSummedUpRecordHKI_Class != null && isCommenting == GlobalConstants.ISCOMMENTING_TYPE_JUDGE)
                    {
                        objSoLuongHK1.SoLuongDat = lstSummedUpRecordHKI_Class.Count(s => s.JudgementResult == GlobalConstants.PASS);
                        objSoLuongHK1.SoLuongCD = lstSummedUpRecordHKI_Class.Count(s => s.JudgementResult == GlobalConstants.NOPASS);
                    }


                    //Dem HKII
                    if (lstSummedUpRecordHKII_Class != null && isCommenting == GlobalConstants.ISCOMMENTING_TYPE_MARK)
                    {
                        objSoLuongHK2.SoLuongGioi = lstSummedUpRecordHKII_Class.Count(s => s.SummedUpMark.HasValue && s.SummedUpMark >= 8.0m);
                        objSoLuongHK2.SoLuongKha = lstSummedUpRecordHKII_Class.Count(s => s.SummedUpMark.HasValue && s.SummedUpMark >= 6.5m && s.SummedUpMark < 8.0m);
                        objSoLuongHK2.SoLuongTB = lstSummedUpRecordHKII_Class.Count(s => s.SummedUpMark.HasValue && s.SummedUpMark >= 5.0m && s.SummedUpMark < 6.5m);
                        objSoLuongHK2.SoLuongYeu = lstSummedUpRecordHKII_Class.Count(s => s.SummedUpMark.HasValue && s.SummedUpMark >= 3.5m && s.SummedUpMark < 5.0m);
                        objSoLuongHK2.SoLuongKem = lstSummedUpRecordHKII_Class.Count(s => s.SummedUpMark.HasValue && s.SummedUpMark < 3.5m);
                    }

                    if (lstSummedUpRecordHKII_Class != null && isCommenting == GlobalConstants.ISCOMMENTING_TYPE_JUDGE)
                    {
                        objSoLuongHK2.SoLuongDat = lstSummedUpRecordHKII_Class.Count(s => s.JudgementResult == GlobalConstants.PASS);
                        objSoLuongHK2.SoLuongCD = lstSummedUpRecordHKII_Class.Count(s => s.JudgementResult == GlobalConstants.NOPASS);
                    }


                    //Dem CN
                    if (lstSummedUpRecordHK_CN_Class != null && isCommenting == GlobalConstants.ISCOMMENTING_TYPE_JUDGE)
                    {
                        objSoLuongHK2.SoLuongDat = lstSummedUpRecordHK_CN_Class.Count(s => s.JudgementResult == GlobalConstants.PASS);
                        objSoLuongHK2.SoLuongCD = lstSummedUpRecordHK_CN_Class.Count(s => s.JudgementResult == GlobalConstants.NOPASS);
                    }


                    if (lstSummedUpRecordHK_CN_Class != null && isCommenting == GlobalConstants.ISCOMMENTING_TYPE_MARK)
                    {
                        objSoLuongCN.SoLuongGioi = lstSummedUpRecordHK_CN_Class.Count(s => s.SummedUpMark.HasValue && s.SummedUpMark >= 8.0m);
                        objSoLuongCN.SoLuongKha = lstSummedUpRecordHK_CN_Class.Count(s => s.SummedUpMark.HasValue && s.SummedUpMark >= 6.5m && s.SummedUpMark < 8.0m);
                        objSoLuongCN.SoLuongTB = lstSummedUpRecordHK_CN_Class.Count(s => s.SummedUpMark.HasValue && s.SummedUpMark >= 5.0m && s.SummedUpMark < 6.5m);
                        objSoLuongCN.SoLuongYeu = lstSummedUpRecordHK_CN_Class.Count(s => s.SummedUpMark.HasValue && s.SummedUpMark >= 3.5m && s.SummedUpMark < 5.0m);
                        objSoLuongCN.SoLuongKem = lstSummedUpRecordHK_CN_Class.Count(s => s.SummedUpMark.HasValue && s.SummedUpMark < 3.5m);
                    }

                    if (lstSummedUpRecordHK_CN_Class != null && isCommenting == GlobalConstants.ISCOMMENTING_TYPE_JUDGE)
                    {
                        objSoLuongCN.SoLuongDat = lstSummedUpRecordHK_CN_Class.Count(s => s.JudgementResult == GlobalConstants.PASS);
                        objSoLuongCN.SoLuongCD = lstSummedUpRecordHK_CN_Class.Count(s => s.JudgementResult == GlobalConstants.NOPASS);
                    }


                    for (int i = 0; i < countPupil; i++)
                    {
                        PupilOfClassBO pp = lstPupilOfClass[i];
                        //Xét template cho học sinh
                        if (pp.Status == SystemParamsInFile.PUPIL_STATUS_STUDYING || pp.Status == SystemParamsInFile.PUPIL_STATUS_GRADUATED)
                        {
                            if (i == 0 && countPupil != 1)
                            {
                                oSheet.CopyPasteSameSize(topRange, rowIndex, 1);
                                oSheet.CopyPasteSameSize(topRange_HKII, rowIndex, 3 + countMPV);
                            }
                            else if ((i + 1) % 5 == 0 || (i + 1) == countPupil)
                            {
                                oSheet.CopyPasteSameSize(lastRange, rowIndex, 1);
                                oSheet.CopyPasteSameSize(lastRange_HKII, rowIndex, 3 + countMPV);
                            }
                            else
                            {
                                oSheet.CopyPasteSameSize(middleRange, rowIndex, 1);
                                oSheet.CopyPasteSameSize(middleRange_HKII, rowIndex, 3 + countMPV);
                            }
                        }
                        else
                        {
                            if ((i + 1) % 5 == 0 || (i + 1) == countPupil)
                            {
                                oSheet.CopyPasteSameSize(redlastRange, rowIndex, 1);
                                oSheet.CopyPasteSameSize(redlastRange_HKII, rowIndex, 3 + countMPV);
                            }
                            else
                            {
                                oSheet.CopyPasteSameSize(redmiddleRange, rowIndex, 1);
                                oSheet.CopyPasteSameSize(redmiddleRange_HKII, rowIndex, 3 + countMPV);
                            }

                        }
                        //Kẻ thêm các ô khi kết thúc các con điểm M, P, V
                        IVTRange rangeM_Think = oSheet.GetRange(6, 2 + countM, 6 + countPupil, 2 + countM);
                        rangeM_Think.SetBorder(VTBorderStyle.Solid, VTBorderWeight.Thin, VTBorderIndex.EdgeRight);
                        IVTRange rangeP_Think = oSheet.GetRange(5, 2 + countM + countP, 6 + countPupil, 2 + countM + countP);
                        rangeP_Think.SetBorder(VTBorderStyle.Solid, VTBorderWeight.Thin, VTBorderIndex.EdgeRight);


                        int colIndex = 3;
                        oSheet.SetCellValue("A" + rowIndex, order++);
                        oSheet.SetCellValue("B" + rowIndex, pp.PupilFullName);
                        bool showData = lstPocClass.Any(o => o.PupilID == pp.PupilID);
                        bool showDataHKI = lstPocClassHKI.Any(o => o.PupilID == pp.PupilID);
                        if (showData)
                        {
                            //Lấy dữ liệu điểm của học sinh
                            var pupilMarks = lstMarkRecord_Class.Where(u => u.PupilID == pp.PupilID);
                            var pupilJudges = lstJudgeRecord_Class.Where(u => u.PupilID == pp.PupilID);

                            var pMarkHKII = lstSummedUpRecordHKII_Class.Where(u => u.PupilID == pp.PupilID).Select(u => u.SummedUpMark).FirstOrDefault();
                            var pJudgeHKII = lstSummedUpRecordHKII_Class.Where(u => u.PupilID == pp.PupilID).Select(u => u.JudgementResult).FirstOrDefault();
                            SummedUpRecordBO summed_CN = lstSummedUpRecordHK_CN_Class.Where(u => u.PupilID == pp.PupilID).FirstOrDefault();
                            decimal? pMark_CN = null;
                            var pJudge_CN = "";
                            if (summed_CN != null && isCommenting != SystemParamsInFile.ISCOMMENTING_TYPE_JUDGE)
                            {
                                if (summed_CN.ReTestMark != null)
                                {
                                    pMark_CN = summed_CN.ReTestMark;
                                }
                                else
                                {
                                    pMark_CN = summed_CN.SummedUpMark;
                                }
                            }
                            if (summed_CN != null && isCommenting == SystemParamsInFile.ISCOMMENTING_TYPE_JUDGE)
                            {
                                if (summed_CN.ReTestJudgement != null)
                                {
                                    pJudge_CN = summed_CN.ReTestJudgement;
                                }
                                else
                                {
                                    pJudge_CN = summed_CN.JudgementResult;
                                }
                            }


                            var pupilSummed_HKII = isCommenting == SystemParamsInFile.ISCOMMENTING_TYPE_JUDGE ? (!string.IsNullOrEmpty(pJudgeHKII) ? pJudgeHKII : string.Empty) : (pMarkHKII.HasValue ? ReportUtils.ConvertMarkForV(pMarkHKII.Value) : string.Empty);
                            var pupilSummed_CN = isCommenting == SystemParamsInFile.ISCOMMENTING_TYPE_JUDGE ? (!string.IsNullOrEmpty(pJudge_CN) ? pJudge_CN : string.Empty) : (pMark_CN.HasValue ? ReportUtils.ConvertMarkForV(pMark_CN.Value) : string.Empty);
                            foreach (var title in titleGroup)
                            {
                                foreach (var titleName in titles.Where(u => u.StartsWith(title.StartCharacter.ToString())).OrderBy(u => u))
                                {
                                    if (isCommenting == SystemParamsInFile.ISCOMMENTING_TYPE_JUDGE)
                                    {
                                        JudgeRecordBO judgeRecord = pupilJudges.Where(u => u.Title == titleName).FirstOrDefault();
                                        oSheet.SetCellValue(rowIndex, colIndex, judgeRecord != null ? judgeRecord.Judgement : string.Empty);
                                    }
                                    else
                                    {
                                        MarkRecordBO markRecord = pupilMarks.Where(u => u.Title == titleName).FirstOrDefault();
                                        if (markRecord != null)
                                        {
                                            if (titleName.Contains("M"))
                                            {
                                                string mark = ReportUtils.ConvertMarkForM(markRecord.Mark.Value);
                                                oSheet.SetCellValue(rowIndex, colIndex, mark);
                                            }
                                            else if (titleName.Contains("P") || titleName.Contains("V"))
                                            {
                                                string mark = ReportUtils.ConvertMarkForV(markRecord.Mark.Value);
                                                oSheet.SetCellValue(rowIndex, colIndex, mark);
                                            }
                                            else if (titleName.Contains("HK"))
                                            {
                                                string mark = ReportUtils.ConvertMarkForV(markRecord.Mark.Value);
                                                oSheet.SetCellValue(rowIndex, colIndex, mark);
                                            }
                                        }
                                    }
                                    colIndex++;
                                }
                            }
                            //Diem trung binh mon
                            if (isCommenting == SystemParamsInFile.ISCOMMENTING_TYPE_JUDGE)
                            {
                                oSheet.SetCellValue(rowIndex, colIndex, pupilSummed_HKII);
                                colIndex++;
                                colIndex++;
                                oSheet.SetCellValue(rowIndex, colIndex, pupilSummed_CN);

                            }
                            else
                            {
                                if (pMarkHKII != null)
                                {
                                    oSheet.SetCellValue(rowIndex, colIndex, Convert.ToDecimal(pupilSummed_HKII, provider));
                                    
                                }
                                colIndex++;
                                colIndex++;
                                if (pMark_CN != null)
                                {
                                    oSheet.SetCellValue(rowIndex, colIndex, Convert.ToDecimal(pupilSummed_CN, provider));
                                }
                            }
                        }
                        if (showDataHKI)
                        {
                            var pMarkHKI = lstSummedUpRecordHKI_Class.Where(o => o.PupilID == pp.PupilID).Select(u => u.SummedUpMark).FirstOrDefault();
                            var pJudgeHKI = lstSummedUpRecordHKI_Class.Where(o => o.PupilID == pp.PupilID).Select(u => u.JudgementResult).FirstOrDefault();
                            var pupilSummed_HKI = isCommenting == SystemParamsInFile.ISCOMMENTING_TYPE_JUDGE ? (!string.IsNullOrEmpty(pJudgeHKI) ? pJudgeHKI : string.Empty) : (pMarkHKI.HasValue ? ReportUtils.ConvertMarkForV(pMarkHKI.Value) : string.Empty);
                            if (isCommenting == SystemParamsInFile.ISCOMMENTING_TYPE_JUDGE)
                            {
                                oSheet.SetCellValue(rowIndex, 5 + countMPV, pupilSummed_HKI);
                            }
                            else
                            {
                                if (pMarkHKI != null)
                                {
                                    oSheet.SetCellValue(rowIndex, 5 + countMPV, Convert.ToDecimal(pupilSummed_HKI, provider));
                                }
                            }
                        }
                        rowIndex++;

                    }

                    //Tạo công thức cho môn tính điểm
                    int increaseCol = 3;

                    if (isCommenting != SystemParamsInFile.ISCOMMENTING_TYPE_JUDGE)
                    {
                        oSheet.CopyPasteSameSize(rangeTH, countPupil + 8, 2);
                        #region Cột HKI
                        //giỏi
                        //string gioi_HKI = "=COUNTIF(" + columnTK_HKI + "7:" + (columnTK_HKI + (countPupil + 6)) + "," + '"' + ">=8" + '"' + ")";
                        string gioiPercent_HKI = "=100*ROUND(C" + (countPupil + 10) + "/" + countPupilHKI + ",4)";
                        oSheet.SetCellValue(countPupil + 10, increaseCol, objSoLuongHK1.SoLuongGioi);
                        oSheet.SetFormulaValue(countPupil + 10, increaseCol + 1, gioiPercent_HKI.Replace(",", ";").Replace("\'", "\""));
                        //khá =COUNTIFS(V7:V58,">=6.5",V7:V58,"<8")
                        increaseCol += 3;
                        //string kha_HKI = "=COUNTIF(" + columnTK_HKI + "7:" + (columnTK_HKI + (countPupil + 6)) + "," + '"' + ">=6.5" + '"' + ")" + "-" + "COUNTIF(" + columnTK_HKI + "7:" + (columnTK_HKI + (countPupil + 6)) + "," + '"' + ">=8" + '"' + ")";
                        string khaPercent_HKI = "=100*ROUND(F" + (countPupil + 10) + "/" + countPupilHKI + ",4)";
                        oSheet.SetCellValue(countPupil + 10, increaseCol, objSoLuongHK1.SoLuongKha);
                        oSheet.SetFormulaValue(countPupil + 10, increaseCol + 1, khaPercent_HKI.Replace(",", ";").Replace("\'", "\""));
                        //trung bình
                        increaseCol += 3;
                        //string tb_HKI = "=COUNTIF(" + columnTK_HKI + "7:" + (columnTK_HKI + (countPupil + 6)) + "," + '"' + ">=5" + '"' + ")" + "-" + "COUNTIF(" + columnTK_HKI + "7:" + (columnTK_HKI + (countPupil + 6)) + "," + '"' + ">=6.5" + '"' + ")";
                        string tbPercent_HKI = "=100*ROUND(I" + (countPupil + 10) + "/" + countPupilHKI + ",4)";
                        oSheet.SetCellValue(countPupil + 10, increaseCol, objSoLuongHK1.SoLuongTB);
                        oSheet.SetFormulaValue(countPupil + 10, increaseCol + 1, tbPercent_HKI.Replace(",", ";").Replace("\'", "\""));
                        //yếu 
                        increaseCol += 3;
                        //string yeu_HKI = "=COUNTIF(" + columnTK_HKI + "7:" + (columnTK_HKI + (countPupil + 6)) + "," + '"' + ">=3.5" + '"' + ")" + "-" + "COUNTIF(" + columnTK_HKI + "7:" + (columnTK_HKI + (countPupil + 6)) + "," + '"' + ">=5" + '"' + ")";
                        string yeuPercent_HKI = "=100*ROUND(L" + (countPupil + 10) + "/" + countPupilHKI + ",4)";
                        oSheet.SetCellValue(countPupil + 10, increaseCol, objSoLuongHK1.SoLuongYeu);
                        oSheet.SetFormulaValue(countPupil + 10, increaseCol + 1, yeuPercent_HKI.Replace(",", ";").Replace("\'", "\""));
                        //Kém
                        increaseCol += 3;
                        //string kem_HKI = "=COUNTIF(" + columnTK_HKI + "7:" + (columnTK_HKI + (countPupil + 6)) + "," + '"' + ">= 0" + '"' + ")" + "-" + "COUNTIF(" + columnTK_HKI + "7:" + (columnTK_HKI + (countPupil + 6)) + "," + '"' + ">=3.5" + '"' + ")";
                        string kemPercent_HKI = "=100*ROUND(O" + (countPupil + 10) + "/" + countPupilHKI + ",4)";
                        oSheet.SetCellValue(countPupil + 10, increaseCol, objSoLuongHK1.SoLuongKem);
                        oSheet.SetFormulaValue(countPupil + 10, increaseCol + 1, kemPercent_HKI.Replace(",", ";").Replace("\'", "\""));
                        //Trên trung bình
                        increaseCol += 3;
                        //string upTb_HKI = "=COUNTIF(" + columnTK_HKI + "7:" + (columnTK_HKI + (countPupil + 6)) + "," + '"' + ">=5" + '"' + ")";

                        objSoLuongHK1.SoLuongTrenTB = objSoLuongHK1.SoLuongGioi + objSoLuongHK1.SoLuongKha + objSoLuongHK1.SoLuongTB;
                        string upTbPercent_HKI = "=100*ROUND(R" + (countPupil + 10) + "/" + countPupilHKI + ",4)";
                        oSheet.SetCellValue(countPupil + 10, increaseCol, objSoLuongHK1.SoLuongTrenTB);
                        oSheet.SetFormulaValue(countPupil + 10, increaseCol + 1, upTbPercent_HKI.Replace(",", ";").Replace("\'", "\""));
                        #endregion end Cột HKI
                        #region Cột HKII
                        //giỏi
                        increaseCol = 3;
                        //string gioi_HKII = "=COUNTIF(" + columnTK_HKII + "7:" + (columnTK_HKII + (countPupil + 6)) + "," + '"' + ">=8" + '"' + ")";
                        string gioiPercent_HKII = "=100*ROUND(C" + (countPupil + 11) + "/" + countPupilHK + ",4)";
                        oSheet.SetCellValue(countPupil + 11, increaseCol, objSoLuongHK2.SoLuongGioi);
                        oSheet.SetFormulaValue(countPupil + 11, increaseCol + 1, gioiPercent_HKII.Replace(",", ";").Replace("\'", "\""));
                        //khá =COUNTIFS(V7:V58,">=6.5",V7:V58,"<8")
                        increaseCol += 3;
                        //string kha_HKII = "=COUNTIF(" + columnTK_HKII + "7:" + (columnTK_HKII + (countPupil + 6)) + "," + '"' + ">=6.5" + '"' + ")" + "-" + "COUNTIF(" + columnTK_HKII + "7:" + (columnTK_HKII + (countPupil + 6)) + "," + '"' + ">=8" + '"' + ")";
                        string khaPercent_HKII = "=100*ROUND(F" + (countPupil + 11) + "/" + countPupilHK + ",4)";
                        oSheet.SetCellValue(countPupil + 11, increaseCol, objSoLuongHK2.SoLuongKha);
                        oSheet.SetFormulaValue(countPupil + 11, increaseCol + 1, khaPercent_HKII.Replace(",", ";").Replace("\'", "\""));
                        //trung bình
                        increaseCol += 3;
                        //string tb_HKII = "=COUNTIF(" + columnTK_HKII + "7:" + (columnTK_HKII + (countPupil + 6)) + "," + '"' + ">=5" + '"' + ")" + "-" + "COUNTIF(" + columnTK_HKII + "7:" + (columnTK_HKII + (countPupil + 6)) + "," + '"' + ">=6.5" + '"' + ")";
                        string tbPercent_HKII = "=100*ROUND(I" + (countPupil + 11) + "/" + countPupilHK + ",4)";
                        oSheet.SetCellValue(countPupil + 11, increaseCol, objSoLuongHK2.SoLuongTB);
                        oSheet.SetFormulaValue(countPupil + 11, increaseCol + 1, tbPercent_HKII.Replace(",", ";").Replace("\'", "\""));
                        //yếu 
                        increaseCol += 3;
                        //string yeu_HKII = "=COUNTIF(" + columnTK_HKII + "7:" + (columnTK_HKII + (countPupil + 6)) + "," + '"' + ">=3.5" + '"' + ")" + "-" + "COUNTIF(" + columnTK_HKII + "7:" + (columnTK_HKII + (countPupil + 6)) + "," + '"' + ">=5" + '"' + ")";
                        string yeuPercent_HKII = "=100*ROUND(L" + (countPupil + 11) + "/" + countPupilHK + ",4)";
                        oSheet.SetCellValue(countPupil + 11, increaseCol, objSoLuongHK2.SoLuongYeu);
                        oSheet.SetFormulaValue(countPupil + 11, increaseCol + 1, yeuPercent_HKII.Replace(",", ";").Replace("\'", "\""));
                        //Kém
                        increaseCol += 3;
                        //string kem_HKII = "=COUNTIF(" + columnTK_HKII + "7:" + (columnTK_HKII + (countPupil + 6)) + "," + '"' + ">= 0" + '"' + ")" + "-" + "COUNTIF(" + columnTK_HKII + "7:" + (columnTK_HKII + (countPupil + 6)) + "," + '"' + ">=3.5" + '"' + ")";
                        string kemPercent_HKII = "=100*ROUND(O" + (countPupil + 11) + "/" + countPupilHK + ",4)";
                        oSheet.SetCellValue(countPupil + 11, increaseCol, objSoLuongHK2.SoLuongKem);
                        oSheet.SetFormulaValue(countPupil + 11, increaseCol + 1, kemPercent_HKII.Replace(",", ";").Replace("\'", "\""));
                        //Trên trung bình
                        increaseCol += 3;
                        //string upTb_HKII = "=COUNTIF(" + columnTK_HKII + "7:" + (columnTK_HKII + (countPupil + 6)) + "," + '"' + ">=5" + '"' + ")";
                        string upTbPercent_HKII = "=100*ROUND(R" + (countPupil + 11) + "/" + countPupilHK + ",4)";
                        objSoLuongHK2.SoLuongTrenTB = objSoLuongHK2.SoLuongGioi + objSoLuongHK2.SoLuongKha + objSoLuongHK2.SoLuongTB;
                        oSheet.SetCellValue(countPupil + 11, increaseCol, objSoLuongHK2.SoLuongTrenTB);
                        oSheet.SetFormulaValue(countPupil + 11, increaseCol + 1, upTbPercent_HKII.Replace(",", ";").Replace("\'", "\""));
                        #endregion Kết thúc cột HKII
                        #region Cột Cả năm
                        //giỏi
                        increaseCol = 3;
                        //string gioi = "=COUNTIF(" + columnTK_CN + "7:" + (columnTK_CN + (countPupil + 6)) + "," + '"' + ">=8" + '"' + ")";
                        string gioiPercent = "=100*ROUND(C" + (countPupil + 12) + "/" + countPupilHK + ",4)";
                        oSheet.SetCellValue(countPupil + 12, increaseCol, objSoLuongCN.SoLuongGioi);
                        oSheet.SetFormulaValue(countPupil + 12, increaseCol + 1, gioiPercent.Replace(",", ";").Replace("\'", "\""));
                        //khá =COUNTIFS(V7:V58,">=6.5",V7:V58,"<8")
                        increaseCol += 3;
                        //string kha = "=COUNTIF(" + columnTK_CN + "7:" + (columnTK_CN + (countPupil + 6)) + "," + '"' + ">=6.5" + '"' + ")" + "-" + "COUNTIF(" + columnTK_CN + "7:" + (columnTK_CN + (countPupil + 6)) + "," + '"' + ">=8" + '"' + ")";
                        string khaPercent = "=100*ROUND(F" + (countPupil + 12) + "/" + countPupilHK + ",4)";
                        oSheet.SetCellValue(countPupil + 12, increaseCol, objSoLuongCN.SoLuongKha);
                        oSheet.SetFormulaValue(countPupil + 12, increaseCol + 1, khaPercent.Replace(",", ";").Replace("\'", "\""));
                        //trung bình
                        increaseCol += 3;
                        //string tb = "=COUNTIF(" + columnTK_CN + "7:" + (columnTK_CN + (countPupil + 6)) + "," + '"' + ">=5" + '"' + ")" + "-" + "COUNTIF(" + columnTK_CN + "7:" + (columnTK_CN + (countPupil + 6)) + "," + '"' + ">=6.5" + '"' + ")";
                        string tbPercent = "=100*ROUND(I" + (countPupil + 12) + "/" + countPupilHK + ",4)";
                        oSheet.SetCellValue(countPupil + 12, increaseCol, objSoLuongCN.SoLuongTB);
                        oSheet.SetFormulaValue(countPupil + 12, increaseCol + 1, tbPercent.Replace(",", ";").Replace("\'", "\""));
                        //yếu 
                        increaseCol += 3;
                        //string yeu = "=COUNTIF(" + columnTK_CN + "7:" + (columnTK_CN + (countPupil + 6)) + "," + '"' + ">=3.5" + '"' + ")" + "-" + "COUNTIF(" + columnTK_CN + "7:" + (columnTK_CN + (countPupil + 6)) + "," + '"' + ">=5" + '"' + ")";
                        string yeuPercent = "=100*ROUND(L" + (countPupil + 12) + "/" + countPupilHK + ",4)";
                        oSheet.SetCellValue(countPupil + 12, increaseCol, objSoLuongCN.SoLuongYeu);
                        oSheet.SetFormulaValue(countPupil + 12, increaseCol + 1, yeuPercent.Replace(",", ";").Replace("\'", "\""));
                        //Kém
                        increaseCol += 3;
                        //string kem = "=COUNTIF(" + columnTK_CN + "7:" + (columnTK_CN + (countPupil + 6)) + "," + '"' + ">= 0" + '"' + ")" + "-" + "COUNTIF(" + columnTK_CN + "7:" + (columnTK_CN + (countPupil + 6)) + "," + '"' + ">=3.5" + '"' + ")";
                        string kemPercent = "=100*ROUND(O" + (countPupil + 12) + "/" + countPupilHK + ",4)";
                        oSheet.SetCellValue(countPupil + 12, increaseCol, objSoLuongCN.SoLuongKem);
                        oSheet.SetFormulaValue(countPupil + 12, increaseCol + 1, kemPercent.Replace(",", ";").Replace("\'", "\""));
                        //Trên trung bình
                        increaseCol += 3;
                        //string upTb = "=COUNTIF(" + columnTK_CN + "7:" + (columnTK_CN + (countPupil + 6)) + "," + '"' + ">=5" + '"' + ")";
                        objSoLuongCN.SoLuongTrenTB = objSoLuongCN.SoLuongGioi + objSoLuongCN.SoLuongKha + objSoLuongCN.SoLuongTB;
                        string upTbPercent = "=100*ROUND(R" + (countPupil + 12) + "/" + countPupilHK + ",4)";
                        oSheet.SetCellValue(countPupil + 12, increaseCol, objSoLuongCN.SoLuongTrenTB);
                        oSheet.SetFormulaValue(countPupil + 12, increaseCol + 1, upTbPercent.Replace(",", ";").Replace("\'", "\""));
                        #endregion

                    }
                    else
                    {
                        oSheet.CopyPasteSameSize(rangeTK_NX, countPupil + 8, 2);
                        #region HKI
                        //string pass_HKI = "=COUNTIF(" + columnTK_HKI + "7:" + (columnTK_HKI + (countPupil + 6)) + "," + '"' + "=Đ" + '"' + ")";
                        string passPercent_HKI = "=100*ROUND(C" + (countPupil + 10) + "/" + countPupilHKI + ",4)";
                        oSheet.SetCellValue(countPupil + 10, increaseCol, objSoLuongHK1.SoLuongDat);
                        oSheet.SetFormulaValue(countPupil + 10, increaseCol + 1, passPercent_HKI.Replace(",", ";").Replace("\'", "\""));
                        increaseCol += 3;
                        //string fail_HKI = "=COUNTIF(" + columnTK_HKI + "7:" + (columnTK_HKI + (countPupil + 6)) + "," + '"' + "=CĐ" + '"' + ")";
                        string failPercent_HKI = "=100*ROUND(F" + (countPupil + 10) + "/" + countPupilHKI + ",4)";
                        oSheet.SetCellValue(countPupil + 10, increaseCol, objSoLuongHK1.SoLuongCD);
                        oSheet.SetFormulaValue(countPupil + 10, increaseCol + 1, failPercent_HKI.Replace(",", ";").Replace("\'", "\""));
                        #endregion
                        #region HKII
                        increaseCol = 3;
                        //string pass_HKII = "=COUNTIF(" + columnTK_HKII + "7:" + (columnTK_HKII + (countPupil + 6)) + "," + '"' + "=Đ" + '"' + ")";
                        string passPercent_HKII = "=100*ROUND(C" + (countPupil + 11) + "/" + countPupilHK + ",4)";
                        oSheet.SetCellValue(countPupil + 11, increaseCol, objSoLuongHK2.SoLuongDat);
                        oSheet.SetFormulaValue(countPupil + 11, increaseCol + 1, passPercent_HKII.Replace(",", ";").Replace("\'", "\""));
                        increaseCol += 3;
                        //string fail_HKII = "=COUNTIF(" + columnTK_HKII + "7:" + (columnTK_HKII + (countPupil + 6)) + "," + '"' + "=CĐ" + '"' + ")";
                        string failPercent_HKII = "=100*ROUND(F" + (countPupil + 11) + "/" + countPupilHK + ",4)";
                        oSheet.SetCellValue(countPupil + 11, increaseCol, objSoLuongHK2.SoLuongCD);
                        oSheet.SetFormulaValue(countPupil + 11, increaseCol + 1, failPercent_HKII.Replace(",", ";").Replace("\'", "\""));
                        #endregion
                        #region Cả năm
                        increaseCol = 3;
                        //string pass = "=COUNTIF(" + columnTK_CN + "7:" + (columnTK_CN + (countPupil + 6)) + "," + '"' + "=Đ" + '"' + ")";
                        string passPercent = "=100*ROUND(C" + (countPupil + 12) + "/" + countPupilHK + ",4)";
                        oSheet.SetCellValue(countPupil + 12, increaseCol, objSoLuongCN.SoLuongDat);
                        oSheet.SetFormulaValue(countPupil + 12, increaseCol + 1, passPercent.Replace(",", ";").Replace("\'", "\""));
                        increaseCol += 3;
                        //string fail = "=COUNTIF(" + columnTK_CN + "7:" + (columnTK_CN + (countPupil + 6)) + "," + '"' + "=CĐ" + '"' + ")";
                        string failPercent = "=100*ROUND(F" + (countPupil + 12) + "/" + countPupilHK + ",4)";
                        oSheet.SetCellValue(countPupil + 12, increaseCol, objSoLuongCN.SoLuongCD);
                        oSheet.SetFormulaValue(countPupil + 12, increaseCol + 1, failPercent.Replace(",", ";").Replace("\'", "\""));
                        #endregion

                    }
                    oSheet.PageMaginLeft = 0.5;
                    oSheet.PageMaginRight = 0.5;
                    oSheet.PageMaginTop = 0.5;
                    oSheet.PageMaginBottom = 0.5;
                    oSheet.FitSheetOnOnePage = true;
                }
            }
            firstsheet.Delete();
            firstsheet.Delete();
            return oBook.ToStream();

        }
        public string GetHashKey(ReportMarkSubjectBO ReportMarkSubject)
        {
            IDictionary<string, object> dic = new Dictionary<string, object> {
                {"AcademicYearID", ReportMarkSubject.AcademicYearID},
                {"SchoolID ", ReportMarkSubject.SchoolID},
                {"TeacherID", ReportMarkSubject.TeacherID},
                {"SubjectID", ReportMarkSubject.SubjectID},
                {"Semester", ReportMarkSubject.Semester}
            };
            return ReportUtils.GetHashKey(dic);
        }
        private string GetExcelColumnName(int columnNumber)
        {
            int dividend = columnNumber;
            string columnName = String.Empty;
            int modulo;

            while (dividend > 0)
            {
                modulo = (dividend - 1) % 26;
                columnName = Convert.ToChar(65 + modulo).ToString() + columnName;
                dividend = (int)((dividend - modulo) / 26);
            }

            return columnName;
        }


    }

    public class DemSoLuong
    {
        public int SoLuongGioi { get; set; }
        public int SoLuongKha { get; set; }
        public int SoLuongTB { get; set; }
        public int SoLuongYeu { get; set; }
        public int SoLuongKem { get; set; }
        public int SoLuongDat { get; set; }
        public int SoLuongCD { get; set; }

        public int SoLuongTrenTB
        {
            get; set;
        }
    }
}

