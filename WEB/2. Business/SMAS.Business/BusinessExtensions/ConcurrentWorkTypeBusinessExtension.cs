/**
 * The Viettel License
 *
 * Copyright 2012 Viettel Telecom. All rights reserved.
 * VIETTEL PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */

/** 
* @author  
* @version $Revision: $
*/

using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Linq.Expressions;
using System.Web;
using log4net;

using SMAS.DAL.Repository;
using SMAS.DAL.IRepository;
using SMAS.Business.IBusiness;
using SMAS.Models.Models;
using SMAS.Business.Common;

namespace SMAS.Business.Business
{
    public partial class ConcurrentWorkTypeBusiness
    {

        #region Search
        /// <summary>
        /// Tìm kiếm thông tin phân công công việc kiêm nhiệm
        /// </summary>
        /// <author>hath</author>
        /// <date>4/9/2012</date>
        /// 
        /// <param name="Resolution">Diễn giải về công việc kiêm nhiệm</param>
        /// <param name="Description">Mô tả</param>
        /// <param name="SchoolID">ID trường</param>
        /// <param name="IsActive">Biến kích hoạt</param>
        /// 
        /// <returns>IQueryable<ConcurrentWorkType></returns>
        public IQueryable<ConcurrentWorkType> Search(IDictionary<string, object> dic)
        {
            string Resolution = Utils.GetString(dic, "Resolution");
            string Description = Utils.GetString(dic, "Description");
            int SchoolID = Utils.GetInt(dic, "SchoolID");
            bool? IsActive = Utils.GetIsActive(dic, "IsActive");

            IQueryable<ConcurrentWorkType> lsConcurrentWorkType = ConcurrentWorkTypeRepository.All;

            if (IsActive.HasValue)
            {
                lsConcurrentWorkType = lsConcurrentWorkType.Where(con => con.IsActive == true);
            }
            if (Resolution.Trim().Length > 0)
            {
                lsConcurrentWorkType = lsConcurrentWorkType.Where(con => con.Resolution.ToLower().Contains(Resolution.ToLower()));
            }
            if (Description.Trim().Length > 0)
            {
                lsConcurrentWorkType = lsConcurrentWorkType.Where(con => con.Description.ToLower().Contains(Description.ToLower()));
            }
            if (SchoolID != 0)
            {
                lsConcurrentWorkType = lsConcurrentWorkType.Where(con => con.SchoolID == SchoolID);
            }

            return lsConcurrentWorkType;

        }
        #endregion

        #region Delete

        /// <summary>
        /// Xóa thông tin phân công công việc kiêm nhiệm
        /// </summary>
        /// <author>hath</author>
        /// <date>4/9/2012</date>
        /// <param name="ConcurrentWorkTypeID"> ID Công việc kiêm nhiệm</param>
        public void Delete(int ConcurrentWorkTypeID)
        {
            //Bạn chưa chọn công việc kiêm nhiệm cần xóa hoặc công việc kiêm nhiệm bạn chọn đã bị xóa khỏi hệ thống
            new ConcurrentWorkTypeBusiness(null).CheckAvailable((int)ConcurrentWorkTypeID, "ConcurrentWorkType_Label_ConcurrentWorkType", true);

            //Không thể xóa công việc kiêm nhiệm đang sử dụng
            //new ConcurrentWorkTypeBusiness(null).CheckConstraintsWithExceptTable(GlobalConstants.LIST_SCHEMA, "ConcurrentWorkType", "ConcurrentWorkTypeAssignment", ConcurrentWorkTypeID, "ConcurrentWorkType_Label_Resolution");
            base.Delete(ConcurrentWorkTypeID, true);
        }
        #endregion

        #region Insert

        /// <summary>
        /// Thêm thông tin phân công công việc kiêm nhiệm
        /// </summary>
        /// <author>hath</author>
        /// <date>4/9/2012</date>
        /// <param name="concurrentWorkType">object ConcurrentWorkType</param>
        /// <returns>object ConcurrentWorkType</returns>
        public override ConcurrentWorkType Insert(ConcurrentWorkType concurrentWorkType)
        {
            //Tên công việc kiêm nhiệm không được để trống 
            Utils.ValidateRequire(concurrentWorkType.Resolution, "ConcurrentWorkType_Label_Resolution");
            //Độ dài tên công việc kiêm nhiệm không được vượt quá 50 
            Utils.ValidateMaxLength(concurrentWorkType.Resolution, 50, "ConcurrentWorkType_Label_Resolution");
            //Tên công việc kiêm nhiệm đã tồn tại 
            // DungVA - 14/08 check trùng lại công việc kiêm nhiệm theo trường
            IDictionary<string, object> expDic = null;
            if (concurrentWorkType.ConcurrentWorkTypeID > 0)
            {
                expDic = new Dictionary<string, object>();
                expDic["ConcurrentWorkTypeID"] = concurrentWorkType.ConcurrentWorkTypeID;
            }
            bool ConcurrentWorkTypeDuplicate = this.repository.ExistsRow(GlobalConstants.LIST_SCHEMA, "ConcurrentWorkType",
                   new Dictionary<string, object>()
                {
                    {"SchoolID",concurrentWorkType.SchoolID},
                    {"Resolution",concurrentWorkType.Resolution},
                    {"IsActive",true}
                }, expDic);
            if (ConcurrentWorkTypeDuplicate)
            {
                List<object> listParam = new List<object>();
                listParam.Add("ConcurrentWorkType_Label_Resolution");
                throw new BusinessException("Common_Validate_Duplicate", listParam);
            }
            // DungVA - 14/08 check trùng lại công việc kiêm nhiệm theo trường
            //new ConcurrentWorkTypeBusiness(null).CheckDuplicate(concurrentWorkType.Resolution, GlobalConstants.LIST_SCHEMA, "ConcurrentWorkType", "Resolution", true, concurrentWorkType.ConcurrentWorkTypeID, "ConcurrentWorkType_Label_Resolution");

            //Độ dài ghi chú không được vượt quá 400 
            Utils.ValidateMaxLength(concurrentWorkType.Description, 400, "Description");
            //Số tiết/ tuần phải là số nguyên dương trong khoảng 0-99 – Kiểm tra SectionPerWeek
            Utils.ValidateRange(concurrentWorkType.SectionPerWeek, 0, 99, "ConcurrentWorkType_Label_SectionPerWeek");
            //Không tồn tại Trường học hoặc đã bị xóa - Kiểm tra SchoolID - SchoolProfileID trong bảng SchoolProfile với IsActive =1
            SchoolProfileBusiness.CheckAvailable(concurrentWorkType.SchoolID, "SchoolProfile_Label_SchoolID", true);
            //Insert
            return base.Insert(concurrentWorkType);

        }
        #endregion

        #region Update

        /// <summary>
        /// Sửa thông tin phân công công việc kiêm nhiệm
        /// </summary>
        /// <author>hath</author>
        /// <date>4/9/2012</date>
        /// <param name="concurrentWorkType">object ConcurrentWorkType</param>
        /// <returns>object ConcurrentWorkType</returns>
        public override ConcurrentWorkType Update(ConcurrentWorkType concurrentWorkType)
        {
            //Bạn chưa chọn công việc kiêm nhiệm cần sửa hoặc công việc kiêm nhiệm bạn chọn đã bị xóa khỏi hệ thống
            new ConcurrentWorkTypeBusiness(null).CheckAvailable((int)concurrentWorkType.ConcurrentWorkTypeID, "ConcurrentWorkType_Label_ConcurrentWorkType", true);
            //Tên công việc kiêm nhiệm không được để trống 
            Utils.ValidateRequire(concurrentWorkType.Resolution, "ConcurrentWorkType_Label_Resolution");
            //Độ dài tên công việc kiêm nhiệm không được vượt quá 50 
            Utils.ValidateMaxLength(concurrentWorkType.Resolution, 50, "ConcurrentWorkType_Label_Resolution");
            //Tên công việc kiêm nhiệm đã tồn tại 
            //new ConcurrentWorkTypeBusiness(null).CheckDuplicate(concurrentWorkType.Resolution, GlobalConstants.LIST_SCHEMA, "ConcurrentWorkType", "Resolution", true, concurrentWorkType.ConcurrentWorkTypeID, "ConcurrentWorkType_Label_Resolution");
            IDictionary<string, object> SearchInfo = new Dictionary<string, object>();
            SearchInfo["Resolution"] = concurrentWorkType.Resolution;
            SearchInfo["SchoolID"] = concurrentWorkType.SchoolID;
            SearchInfo["IsActive"] = true;
            IDictionary<string, object> dicExcept = new Dictionary<string, object>();
            dicExcept["ConcurrentWorkTypeID"] = concurrentWorkType.ConcurrentWorkTypeID;
            bool compatible = this.repository.ExistsRow(GlobalConstants.LIST_SCHEMA, "ConcurrentWorkType", SearchInfo, dicExcept);
            if (compatible)
            {
                List<object> Params = new List<object>();
                Params.Add("ConcurrentWorkType_Label_Resolution");
                throw new BusinessException("Common_Validate_Duplicate", Params);
            }
            //Độ dài ghi chú không được vượt quá 400 
            Utils.ValidateMaxLength(concurrentWorkType.Description, 400, "Description");
            //Số tiết/ tuần phải là số nguyên dương trong khoảng 0-99 – Kiểm tra SectionPerWeek
            Utils.ValidateRange(concurrentWorkType.SectionPerWeek, 0, 99, "ConcurrentWorkType_Label_SectionPerWeek");
            //Không tồn tại Trường học hoặc đã bị xóa - Kiểm tra SchoolID - SchoolProfileID trong bảng SchoolProfile với IsActive =1
            SchoolProfileBusiness.CheckAvailable(concurrentWorkType.SchoolID, "SchoolProfile_Label_SchoolID", true);



            return base.Update(concurrentWorkType);
        }
        #endregion
    }
}
