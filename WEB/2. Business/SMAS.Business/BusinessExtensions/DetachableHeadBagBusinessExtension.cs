/**
 * The Viettel License
 *
 * Copyright 2012 Viettel Telecom. All rights reserved.
 * VIETTEL PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */

/** 
* @author  
* @version $Revision: $
*/

using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Linq.Expressions;
using System.Web;
using log4net;

using SMAS.DAL.Repository;
using SMAS.DAL.IRepository;
using SMAS.Business.IBusiness;
using SMAS.Models.Models;
using SMAS.Business.Common;
using System.Transactions;

namespace SMAS.Business.Business
{
    public partial class DetachableHeadBagBusiness
    {
        #region private number variable
        private int numberOfCandidate = 0;
        private const int PREFIX_MAX_LENGHT = 5;
        private const int NUMBEROFBAG_MIN = 1;
        private const int NUMBEROFBAG_MAX = 99;
        private const int STARTING_NUMBER_MIN = 1;
        private const int STARTING_NUMBER_MAX = 999;
        #endregion
        public IQueryable<DetachableHeadBag> Search(IDictionary<string, object> dic)
        {
            int? DetachableHeadBagID = Utils.GetNullableInt(dic, "DetachableHeadBagID");
            int? ExaminationID = Utils.GetNullableInt(dic, "ExaminationID");
            int? SchoolID = Utils.GetNullableInt(dic, "SchoolID");
            int? EducationLevelID = Utils.GetNullableInt(dic, "EducationLevelID");
            int? AppliedLevel = Utils.GetNullableInt(dic, "AppliedLevel");
            int? ExaminationSubjectID = Utils.GetNullableInt(dic, "ExaminationSubjectID");
            string BagTitle = Utils.GetString(dic, "BagTitle");
            string Prefix = Utils.GetString(dic, "Prefix");
            DateTime? CreatedDate = Utils.GetDateTime(dic, "CreatedDate");

            var list = this.DetachableHeadBagRepository.All;

            if (DetachableHeadBagID.HasValue && DetachableHeadBagID.Value > 0)
                list = list.Where(u => u.DetachableHeadBagID == DetachableHeadBagID.Value);

            if (ExaminationID.HasValue && ExaminationID.Value > 0)
                list = list.Where(u => u.ExaminationID == ExaminationID.Value);

            if (SchoolID.HasValue && SchoolID.Value > 0)
                list = list.Where(u => u.Examination.SchoolID.Value == SchoolID.Value);

            if (EducationLevelID.HasValue && EducationLevelID.Value > 0)
                list = list.Where(u => u.EducationLevelID == EducationLevelID.Value);

            if (AppliedLevel.HasValue && AppliedLevel.Value > 0)
                list = list.Where(u => u.EducationLevel.Grade == AppliedLevel.Value);

            if (ExaminationSubjectID.HasValue && ExaminationSubjectID.Value > 0)
                list = list.Where(u => u.ExaminationSubjectID == ExaminationSubjectID.Value);

            if (CreatedDate.HasValue)
                list = list.Where(u => u.CreatedDate.Value.Date == CreatedDate.Value.Date);

            return list;
        }

        public IQueryable<DetachableHeadBag> SearchBySchool(int schoolID, IDictionary<string, object> dic)
        {
            if (schoolID == 0 || dic == null)
                return null;

            dic["SchoolID"] = schoolID;
            return Search(dic);
        }

        /// <summary>
        /// AutoGenDetachableHeadNumber
        /// </summary>
        /// <param name="schoolID">The school ID.</param>
        /// <param name="appliedLevel">The applied level.</param>
        /// <param name="examinationSubjectID">The examination subject ID.</param>
        /// <param name="numberOfBag">The number of bag.</param>
        /// <param name="prefix">The prefix.</param>
        /// <param name="startingNumber">The starting number.</param>
        /// <author>TungNT48</author>
        /// <date>11/13/2012</date>
        /// <exception cref="BusinessException">Common_Validate_NotCompatible</exception>
        public void AutoGenDetachableHeadNumber(int schoolID, int appliedLevel, int examinationSubjectID, int numberOfBag,
            string prefix, int startingNumber)
        {
            // Check Business Exception
            //Trường không tồn tại hoặc đã bị xóa – kiểm tra SchoolProfileID trong bảng SchoolProfile với isActive  = 1
            SchoolProfileBusiness.CheckAvailable(schoolID, "DetachableHeadBag_Label_SchoolID", true);
            //Môn thi không tồn tại hoặc đã bị xóa – kiểm tra ExaminationSubjectID trong bảng ExaminationSubject
            ExaminationSubjectBusiness.CheckAvailable(examinationSubjectID, "DetachableHeadBag_Label_ExaminationSubjectID", false);
            ExaminationSubject es = ExaminationSubjectBusiness.Find(examinationSubjectID);
            Examination exam = es.Examination;
            // Chỉ cho phép đánh phách đối với các kỳ thi có trạng thái từ Mới khởi tạo đến trạng thái Đã phân công giám thị
            if (exam.CurrentStage == SystemParamsInFile.EXAMINATION_STAGE_FINISHED || exam.CurrentStage == SystemParamsInFile.EXAMINATION_STAGE_HEAD_ATTACHED || exam.CurrentStage == SystemParamsInFile.EXAMINATION_STAGE_MARK_COMPLETED)
            {
                throw new BusinessException("DetachableHeadBag_Label_StageExamination");
            }

            //  ExaminationID và SchoolID not compatible trong Examination
            IDictionary<string, object> SearchInfoExamination = new Dictionary<string, object>();
            SearchInfoExamination["SchoolID"] = schoolID;
            SearchInfoExamination["ExaminationID"] = es.ExaminationID;
            bool SearchInfoExaminationExist = new ExaminationRepository(this.context).ExistsRow(GlobalConstants.EXAM_SCHEMA, "Examination", SearchInfoExamination, null);
            if (!SearchInfoExaminationExist)
            {
                throw new BusinessException("Common_Validate_NotCompatible");
            }
            //  ExaminationID và AppliedLevel not compatible trong Examination
            IDictionary<string, object> SearchInfoExamination1 = new Dictionary<string, object>();
            SearchInfoExamination1["AppliedLevel"] = appliedLevel;
            SearchInfoExamination1["ExaminationID"] = es.ExaminationID;
            bool SearchInfoExaminationExist1 = new ExaminationRepository(this.context).ExistsRow(GlobalConstants.EXAM_SCHEMA, "Examination", SearchInfoExamination1, null);
            if (!SearchInfoExaminationExist1)
            {
                throw new BusinessException("Common_Validate_NotCompatible");
            }
            // kiểm tra range số túi bài thi
            Utils.ValidateRange(numberOfBag, NUMBEROFBAG_MIN, NUMBEROFBAG_MAX, "DetachableHeadBag_Label_NumberOfBag");
            // kiểm tra range StartingNumber
            Utils.ValidateRange(startingNumber, STARTING_NUMBER_MIN, STARTING_NUMBER_MAX, "DetachableHeadBag_Label_StartingNumber");
            //tiền tố không được nhập quá 5 ký tự. Kiểm tra Prefix
            //tiền tố phai la cac ky tu a-zA-Z0-9
            if (prefix != null)
            {
                Utils.ValidateMaxLength(prefix.Length, PREFIX_MAX_LENGHT, "DetachableHeadBag_Label_Prefix");
                Utils.ValidateNumbericAndLetters(prefix, "DetachableHeadBag_Label_PrefixInvalid");
            }
            else prefix = string.Empty;


            // check Stage
            Examination e = ExaminationBusiness.Find(es.ExaminationID);
            if (e.CurrentStage >= SystemParamsInFile.EXAMINATION_STAGE_MARK_COMPLETED)
            {
                throw new BusinessException("DetachableHeadBag_Label_CurrentStage");
            }
            // check nam hoc hien tai
            if (AcademicYearBusiness.IsCurrentYear(e.AcademicYearID) == false)
            {
                throw new BusinessException("DetachableHeadBag_Label_AcademicYearID");
            }

            //Xoá với các bản ghi trong DetachableHeadMapping 
            IDictionary<string, object> SearchDetachableHeadMapping = new Dictionary<string, object>();
            SearchDetachableHeadMapping["AppliedLevel"] = appliedLevel;
            SearchDetachableHeadMapping["ExaminationID"] = es.ExaminationID;
            SearchDetachableHeadMapping["EducationLevelID"] = es.EducationLevelID;
            SearchDetachableHeadMapping["ExaminationSubjectID"] = examinationSubjectID;
            IQueryable<DetachableHeadMapping> lstDetachableHeadMapping = DetachableHeadMappingBusiness.SearchBySchool(schoolID, SearchDetachableHeadMapping);
            if (lstDetachableHeadMapping != null)
            {
                DetachableHeadMappingBusiness.DeleteAll(lstDetachableHeadMapping.ToList());
            }
            //Xoá với các bản ghi trong DetachableHeadBag 
            IQueryable<DetachableHeadBag> lstDetachableHeadBag = this.SearchBySchool(schoolID, SearchDetachableHeadMapping);
            if (lstDetachableHeadBag != null)
            {
                this.DeleteAll(lstDetachableHeadBag.ToList());
            }
            //Lấy danh sách các thí sinh đã xếp phòng thi, đánh số báo danh ListCandidate
            IDictionary<string, object> SearchCandidate = new Dictionary<string, object>();
            SearchCandidate["AcademicYearID"] = e.AcademicYearID;
            SearchCandidate["AppliedLevel"] = appliedLevel;
            SearchCandidate["ExaminationID"] = es.ExaminationID;
            SearchCandidate["EducationLevelID"] = es.EducationLevelID;
            SearchCandidate["ExaminationSubjectID"] = examinationSubjectID;
            List<Candidate> lstCandidate = CandidateBusiness.SearchBySchool(schoolID, SearchCandidate).ToList();
            List<Candidate> lstCandidateAfterRandom = lstCandidate.OrderBy(u => Guid.NewGuid()).ToList();

            for (int k = 0; k < lstCandidateAfterRandom.Count; k++)
                if (!lstCandidateAfterRandom[k].RoomID.HasValue || lstCandidateAfterRandom[k].RoomID.Value == 0 || string.IsNullOrEmpty(lstCandidateAfterRandom[k].NamedListCode))
                    throw new BusinessException("DetachableHeadBag_Label_lstCandidateAfterRandom");

            //Tính số thí sinh cho 1 túi phách NumberOfCandidate = Round(ListCandidate.Size() / NumberOfBag) + 1
            numberOfCandidate = lstCandidateAfterRandom.Count / numberOfBag + 1;
            // Insert du lieu vao bang DetachableHeadBag
            int lengthHeadBag = (lstCandidate.Count + startingNumber).ToString().Length;
            string CodeHeadBag = string.Empty;
            for (int i = 0; i < numberOfBag; i++)
            {
                DetachableHeadBag dhb = new DetachableHeadBag()
                {
                    ExaminationID = es.ExaminationID,
                    EducationLevelID = es.EducationLevelID,
                    ExaminationSubjectID = es.ExaminationSubjectID,
                    BagTitle = Convert.ToString(i + 1),
                    Prefix = prefix
                };

                // Insert du lieu vao bang DetachableHeadMapping
                for (int j = 0; j < numberOfCandidate; j++)
                {
                    int currentIdx = i * numberOfCandidate + j;
                    if (currentIdx >= lstCandidateAfterRandom.Count)
                    {
                        break;
                    }
                    Candidate c = CandidateBusiness.Find(lstCandidateAfterRandom[currentIdx].CandidateID);
                    if (lengthHeadBag > 1)// lon do dai so tui phách lơn hơn 1
                    {
                        string strZero = "";
                        string strHeadBage = (currentIdx + startingNumber).ToString();
                        for (int k = 0; k < lengthHeadBag - strHeadBage.Length; k++)
                        {
                            strZero += "0";
                        }
                        CodeHeadBag = strZero + strHeadBage;
                    }
                    else
                    {
                        CodeHeadBag = (currentIdx + startingNumber).ToString();
                    }
                    DetachableHeadMapping dhm = new DetachableHeadMapping()
                    {
                        OrderNumber = (int)(currentIdx + 1),
                        CandidateID = c.CandidateID,
                        NamedListCode = c.NamedListCode,
                        DetachableHeadNumber = prefix + CodeHeadBag
                    };
                    dhb.DetachableHeadMappings.Add(dhm);
                }

                base.Insert(dhb);
            }
        }

        public void ManualGenDetachHeadNumber(int schoolID, int appliedLevel, int examinationSubjectID, string[] NameListCodes, string[] DetachableHeadBags, string[] DetachableHeadNumbers)
        {
            ExaminationSubject examSubject = ExaminationSubjectBusiness.Find(examinationSubjectID);
            Examination exam = examSubject.Examination;
            // Chỉ cho phép đánh phách đối với các kỳ thi có trạng thái từ Mới khởi tạo đến trạng thái Đã phân công giám thị
            if (exam.CurrentStage == SystemParamsInFile.EXAMINATION_STAGE_FINISHED || exam.CurrentStage == SystemParamsInFile.EXAMINATION_STAGE_HEAD_ATTACHED || exam.CurrentStage == SystemParamsInFile.EXAMINATION_STAGE_MARK_COMPLETED)
            {
                throw new BusinessException("DetachableHeadBag_Label_StageExamination");
            }
            //Danh sach hoc sinh
            List<Candidate> listCandidate = exam.Candidates.Where(u => u.SubjectID == examinationSubjectID).ToList();

            //Danh sach cac tui phach
            IEnumerable<DetachableHeadBag> listBagOfSubject = exam.DetachableHeadBags.Where(u => u.ExaminationSubjectID == examinationSubjectID);

            //Danh sach cac so phach 
            IEnumerable<DetachableHeadMapping> lstDetachableMappingAll = listBagOfSubject.SelectMany(u => u.DetachableHeadMappings);

            //Danh sach so phach khong nam trong danh sach truyen len
            IEnumerable<DetachableHeadMapping> listDetachableMappingRemain = lstDetachableMappingAll.Where(u => !NameListCodes.Any(v => v.Trim().Equals(u.Candidate.NamedListCode, StringComparison.InvariantCultureIgnoreCase)));

            if (listCandidate.Any(u => string.IsNullOrEmpty(u.NamedListCode)))
                throw new BusinessException("DetachableHeadBag_Label_lstCandidateAfterRandom");

            //Neu ton tai so phach truyen len da co trong co so du lieu
            for (int i = 0; i < DetachableHeadNumbers.Length; i++)
                if (listDetachableMappingRemain.Any(u => u.DetachableHeadNumber != null && u.DetachableHeadNumber.Trim().Equals(DetachableHeadNumbers[i], StringComparison.InvariantCultureIgnoreCase)))
                    throw new BusinessException("DetachableHeadBag_Validate_DuplicateHeadBagNumber");

            using (TransactionScope ts = new TransactionScope())
            {
                for (int i = 0; i < NameListCodes.Length; i++)
                {
                    //Lay candidate 
                    var candidate = listCandidate.SingleOrDefault(u => u.NamedListCode.Equals(NameListCodes[i], StringComparison.InvariantCultureIgnoreCase));

                    //Tim SBD da duoc danh phach
                    List<DetachableHeadMapping> _dhm = DetachableHeadMappingBusiness.SearchBySchool(schoolID, new Dictionary<string, object> { { "ExaminationSubjectID", examinationSubjectID }, { "NamedListCode", NameListCodes[i] } }).ToList();
                    DetachableHeadMapping dhm = null;
                    if (_dhm != null && _dhm.Count() > 0)
                    {
                        dhm = _dhm.Where(o => o.NamedListCode == NameListCodes[i]).SingleOrDefault();
                    }
                    //Tim tui phach 
                    List<DetachableHeadBag> _bag = DetachableHeadBagBusiness.SearchBySchool(schoolID, new Dictionary<string, object> { { "ExaminationSubjectID", examinationSubjectID }, { "BagTitle", DetachableHeadBags[i] } }).ToList();
                    DetachableHeadBag bag = null;
                    if (_bag != null && _bag.Count() > 0)
                    {
                        bag = _bag.Where(o => o.BagTitle == DetachableHeadBags[i]).SingleOrDefault();
                    }

                    if (dhm != null) //So bao danh da duoc danh phach
                    {
                        dhm.DetachableHeadNumber = DetachableHeadNumbers[i];

                        if (bag != null) //Tui phach da ton tai
                        {
                            dhm.DetachableHeadBagID = bag.DetachableHeadBagID;
                        }
                        else //Tui phach chua ton tai
                        {
                            //Tao moi tui phach
                            bag = new DetachableHeadBag();
                            bag.BagTitle = DetachableHeadBags[i];
                            bag.CreatedDate = DateTime.Now;
                            bag.EducationLevelID = examSubject.EducationLevelID;
                            bag.ExaminationID = exam.ExaminationID;
                            bag.ExaminationSubjectID = examinationSubjectID;

                            ValidationMetadata.ValidateObject(bag);
                            DetachableHeadBagBusiness.Insert(bag);
                            this.Save();
                            dhm.DetachableHeadBagID = bag.DetachableHeadBagID;
                        }
                        
                        int order = 1;
                        bag.DetachableHeadMappings.OrderBy(u => u.NamedListNumber).ToList().ForEach(u => u.OrderNumber = order++);

                        DetachableHeadBagBusiness.Update(bag);
                        DetachableHeadMappingBusiness.Update(dhm);

                        this.Save();
                    }
                    else
                    {
                        dhm = new DetachableHeadMapping();
                        dhm.CandidateID = candidate.CandidateID;
                        dhm.DetachableHeadNumber = DetachableHeadNumbers[i];
                        dhm.NamedListCode = NameListCodes[i];
                        dhm.NamedListNumber = candidate.NamedListNumber;

                        if (bag != null) //Tui phach co trong co so du lieu
                        {
                            dhm.DetachableHeadBagID = bag.DetachableHeadBagID;
                        }
                        else //Tui phach khong co trong co so du lieu
                        {
                            bag = new DetachableHeadBag();
                            bag.BagTitle = DetachableHeadBags[i];
                            bag.CreatedDate = DateTime.Now;
                            bag.EducationLevelID = examSubject.EducationLevelID;
                            bag.ExaminationID = exam.ExaminationID;
                            bag.ExaminationSubjectID = examinationSubjectID;

                            ValidationMetadata.ValidateObject(bag);
                            DetachableHeadBagBusiness.Insert(bag);

                            this.Save();
                            dhm.DetachableHeadBagID = bag.DetachableHeadBagID;
                        }

                        ValidationMetadata.ValidateObject(dhm);
                        DetachableHeadMappingBusiness.Insert(dhm);

                        int order = 1;
                        bag.DetachableHeadMappings.OrderBy(u => u.NamedListNumber).ToList().ForEach(u => u.OrderNumber = order++);
                        DetachableHeadBagBusiness.Update(bag);

                        this.Save();
                    } 
                }
                ts.Complete();
            }
        }
    }
}
