/**
 * The Viettel License
 *
 * Copyright 2012 Viettel Telecom. All rights reserved.
 * VIETTEL PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */

/** 
* @author  
* @version $Revision: $
*/

using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Linq.Expressions;
using System.Web;
using log4net;

using SMAS.DAL.Repository;
using SMAS.DAL.IRepository;
using SMAS.Business.IBusiness;
using SMAS.Models.Models;
using SMAS.Business.Common;

namespace SMAS.Business.Business
{
    public partial class TeacherGradeBusiness
    {
        #region private member variable
        private const int RESOLUTION_MAX_LENGTH = 50;   //Độ dài trường diễn giải
        private const int DESCRIPTION_MAX_LENGTH = 400; //Độ dài ghi chú
        #endregion

        #region Search

        /// <summary>
        /// Tìm kiếm xếp loại giáo viên
        /// </summary>
        /// <author>hath</author>
        /// <date>4/9/2012</date>
        /// <param name="GradeResolution"> Diễn giải xếp loại giáo viên</param>
        /// <param name="Description">Mô tả</param>
        /// <param name="IsActive">Biến kích hoạt</param>
        /// <returns>IQueryable<TeacherGrade></returns>
        public IQueryable<TeacherGrade> Search(IDictionary<string, object> dic)
        {
            string GradeResolution = Utils.GetString(dic, "GradeResolution");
            string Description = Utils.GetString(dic, "Description");
            bool? IsActive = Utils.GetNullableBool(dic, "IsActive");

            IQueryable<TeacherGrade> lsTeacherGrade = TeacherGradeRepository.All;

            if (IsActive.HasValue)
                lsTeacherGrade = lsTeacherGrade.Where(tea => tea.IsActive == IsActive);
            if (GradeResolution.Trim().Length != 0)
                lsTeacherGrade = lsTeacherGrade.Where(tea => tea.GradeResolution.ToLower().Contains(GradeResolution.ToLower()));
            if (Description.Trim().Length != 0)
                lsTeacherGrade = lsTeacherGrade.Where(tea => tea.Description.ToLower().Contains(Description.ToLower()));
            return lsTeacherGrade;

        }
        #endregion

        #region Delete
        /// <summary>
        /// Xóa xếp loại giáo viên
        /// </summary>
        /// <author>hath</author>
        /// <date>4/9/2012</date>
        /// <param name="TeacherGradeID">ID xếp loại giáo viên</param>
        public void Delete(int TeacherGradeID)
        {
            //Bạn chưa chọn loại đánh giá xếp loại cán bộ cần xóa hoặc loại đánh giá xếp loại cán bộ bạn chọn đã bị xóa khỏi hệ thống
            new TeacherGradeBusiness(null).CheckAvailable((int)TeacherGradeID, "TeacherGrade_Label_TeacherGradeID", true);

            //Không thể xóa loại đánh giá xếp loại cán bộ đang sử dụng
            new TeacherGradeBusiness(null).CheckConstraints(GlobalConstants.LIST_SCHEMA, "TeacherGrade", TeacherGradeID, "TeacherGrade_Label_TeacherGradeID");
            base.Delete(TeacherGradeID, true);
        }
        #endregion

        #region Insert
        /// <summary>
        /// Thêm xếp loại giáo viên
        /// </summary>
        /// <author>hath</author>
        /// <date>4/9/2012</date>
        /// <param name="teacherGrade">Đối tượng xếp loại giáo viên</param>
        /// <returns>Đối tượng xếp loại giáo viên</returns>
        public override TeacherGrade Insert(TeacherGrade teacherGrade)
        {
            //Tên loại đánh giá xếp loại cán bộ không được để trống 
            Utils.ValidateRequire(teacherGrade.GradeResolution, "TeacherGrade_Label_GradeResolution");
            //Độ dài trường loại đánh giá xếp loại cán bộ không được vượt quá 50 
            Utils.ValidateMaxLength(teacherGrade.GradeResolution,RESOLUTION_MAX_LENGTH , "TeacherGrade_Label_GradeResolution");
            //Tên loại đánh giá xếp loại cán bộ đã tồn tại 
            new TeacherGradeBusiness(null).CheckDuplicate(teacherGrade.GradeResolution, GlobalConstants.LIST_SCHEMA, "TeacherGrade", "GradeResolution", true, teacherGrade.TeacherGradeID, "TeacherGrade_Label_GradeResolution");

            //Độ dài ghi chú không được vượt quá 400 
            Utils.ValidateMaxLength(teacherGrade.Description, DESCRIPTION_MAX_LENGTH, "Description");


            //Insert
            return base.Insert(teacherGrade);

        }
        #endregion

        #region Update
        /// <summary>
        /// Sửa xếp loại giáo viên
        /// </summary>
        /// <author>hath</author>
        /// <date>4/9/2012</date>
        /// <param name="teacherGrade">Đối tượng xếp loại giáo viên</param>
        /// <returns>Đối tượng xếp loại giáo viên</returns>
        public override TeacherGrade Update(TeacherGrade teacherGrade)
        {
            //Bạn chưa chọn loại đánh giá xếp loại cán bộ cần sửa hoặc loại đánh giá xếp loại cán bộ bạn chọn đã bị xóa khỏi hệ thống
            new TeacherGradeBusiness(null).CheckAvailable((int)teacherGrade.TeacherGradeID, "TeacherGrade_Label_TeacherGradeID", true);
            //Tên loại đánh giá xếp loại cán bộ không được để trống 
            Utils.ValidateRequire(teacherGrade.GradeResolution, "TeacherGrade_Label_GradeResolution");
            //Độ dài trường loại đánh giá xếp loại cán bộ không được vượt quá 50 
            Utils.ValidateMaxLength(teacherGrade.GradeResolution, RESOLUTION_MAX_LENGTH, "TeacherGrade_Label_GradeResolution");
            //Tên loại đánh giá xếp loại cán bộ đã tồn tại 
            new TeacherGradeBusiness(null).CheckDuplicate(teacherGrade.GradeResolution, GlobalConstants.LIST_SCHEMA, "TeacherGrade", "GradeResolution", true, teacherGrade.TeacherGradeID, "TeacherGrade_Label_GradeResolution");

            //Độ dài ghi chú không được vượt quá 400 
            Utils.ValidateMaxLength(teacherGrade.Description, DESCRIPTION_MAX_LENGTH, "Description");



            return base.Update(teacherGrade);
        }
        #endregion
    }
}
