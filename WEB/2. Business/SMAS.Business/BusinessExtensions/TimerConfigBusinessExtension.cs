/**
 * The Viettel License
 *
 * Copyright 2012 Viettel Telecom. All rights reserved.
 * VIETTEL PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */

/** 
* @author  
* @version $Revision: $
*/

using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Linq.Expressions;
using System.Web;
using log4net;

using SMAS.DAL.Repository;
using SMAS.DAL.IRepository;
using SMAS.Business.IBusiness;
using SMAS.Models.Models;
using SMAS.Business.Common;
using SMAS.Business.BusinessObject;
using System.Transactions;

namespace SMAS.Business.Business
{
    public partial class TimerConfigBusiness
    {
        public void DeleteTimerConfig(IDictionary<string,object> dic, Guid timerConfigId)
        {
            try
            {
                this.SetAutoDetectChangesEnabled(false);
                //Xoa het tin nhan cu
                int SchoolID = Utils.GetInt(dic, "SchoolID");
                int AcademicYearID = Utils.GetInt(dic, "AcademicYearID");
                int PartitionID = UtilsBusiness.GetPartionId(SchoolID);
                AcademicYear objAy = AcademicYearBusiness.Find(AcademicYearID);
                List<SMS_HISTORY> lstSmsHistory = SMSHistoryBusiness.All.Where(o => o.PARTITION_ID == PartitionID
                    && o.SCHOOL_ID == SchoolID
                    && o.SMS_TIMER_CONFIG_ID.HasValue
                    && o.SMS_TIMER_CONFIG_ID == timerConfigId).ToList();

                List<SMS_MT> lstMT = MTBusiness.All.Where(o => o.SMS_TIMER_CONFIG_ID == timerConfigId).ToList();
                MTBusiness.DeleteAll(lstMT);

                SMSHistoryBusiness.DeleteAll(lstSmsHistory);

                //Cap nhat lai qui tin cho lop
                List<SMS_TIMER_TRANSACTION> lstTimerTransaction = TimerTransactionBusiness.All.Where(o => o.TIMER_CONFIG_ID == timerConfigId
                    && o.SCHOOL_ID == SchoolID
                    && o.IS_ACTIVE == true).ToList();
                List<SMS_PARENT_CONTRACT_CLASS> lstClassDetails = SMSParentContractInClassDetailBusiness.All.Where(c => c.ACADEMIC_YEAR_ID == AcademicYearID
                                                         && c.SCHOOL_ID == SchoolID).ToList();

                List<SMS_PARENT_SENT_DETAIL> lstSendDetail = SMSParentSentDetailBusiness.All.Where(o => o.PARTITION_ID == PartitionID
                    && o.SCHOOL_ID == SchoolID && o.YEAR == objAy.Year).ToList();

                for (int i = 0; i < lstTimerTransaction.Count; i++)
                {
                    SMS_TIMER_TRANSACTION tt = lstTimerTransaction[i];

                    if (tt.TRANSACTION_TYPE == GlobalConstantsEdu.TIMER_TRANSACTION_CONTRACT_IN_CLASS)
                    {
                        SMS_PARENT_CONTRACT_CLASS sc = lstClassDetails.FirstOrDefault(o => o.SMS_PARENT_CONTRACT_CLASS_ID == tt.SMS_PARENT_CONTRACT_CLASS_ID);
                        if (sc != null)
                        {
                            sc.SENT_TOTAL -= tt.SMS_NUMBER_ADDED;
                            SMSParentContractInClassDetailBusiness.Update(sc);
                        }
                    }
                    else
                    {
                        SMS_PARENT_SENT_DETAIL sd = lstSendDetail.FirstOrDefault(o => o.SMS_PARENT_SENT_DETAIL_ID == tt.SMS_PARENT_SENT_DETAIL_ID);
                        if (sd != null && sd.SENT_TOTAL > 0)
                        {
                            sd.SENT_TOTAL -= tt.SENT_TOTAL;
                            if (sd.SENT_TOTAL < 0) sd.SENT_TOTAL = 0;
                            SMSParentSentDetailBusiness.Update(sd);
                        }
                    }

                    tt.IS_ACTIVE = false;
                    TimerTransactionBusiness.Update(tt);
                }

                //Xoa cau hinh cu
                SMS_TIMER_CONFIG tc = this.Find(timerConfigId);
                TimerConfigBusiness.Delete(tc.SMS_TIMER_CONFIG_ID);
                this.Save();
            }
            catch (Exception ex)
            {
                logger.Error(ex.Message, ex);
            }
            finally 
            {
                this.SetAutoDetectChangesEnabled(true);
            }
        }
        public void UpdateTimerConfig(IDictionary<string,object> dic, Guid timerConfigId, List<SMS_TIMER_TRANSACTION> lstTimerTransaction)
        {
            try
            {
                this.SetAutoDetectChangesEnabled(false);
                //Xoa het tin nhan cu
                int SchoolID = Utils.GetInt(dic, "SchoolID");
                int AcademicYearID = Utils.GetInt(dic, "AcademicYearID");
                int PartitionID = UtilsBusiness.GetPartionId(SchoolID);
                List<SMS_HISTORY> lstSmsHistory = SMSHistoryBusiness.All.Where(o => o.PARTITION_ID == PartitionID
                    && o.SCHOOL_ID == SchoolID
                    && o.SMS_TIMER_CONFIG_ID.HasValue
                    && o.SMS_TIMER_CONFIG_ID == timerConfigId).ToList();

                List<SMS_MT> lstMT = MTBusiness.All.Where(o => o.SMS_TIMER_CONFIG_ID == timerConfigId).ToList();
                MTBusiness.DeleteAll(lstMT);

                SMSHistoryBusiness.DeleteAll(lstSmsHistory);


                //Bo transaction
                for (int i = 0; i < lstTimerTransaction.Count; i++)
                {
                    SMS_TIMER_TRANSACTION tt = lstTimerTransaction[i];
                    tt.IS_ACTIVE = false;
                    TimerTransactionBusiness.Update(tt);
                }

                //Xoa cau hinh cu
                SMS_TIMER_CONFIG tc = this.Find(timerConfigId);
                TimerConfigBusiness.Delete(tc.SMS_TIMER_CONFIG_ID);
                this.Save();
            }
            catch (Exception ex)
            {
                logger.Error(ex.Message, ex);
            }
            finally
            {
                this.SetAutoDetectChangesEnabled(true);
            }
        }
    }
}
