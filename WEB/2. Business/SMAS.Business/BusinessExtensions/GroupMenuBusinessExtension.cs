/**
 * The Viettel License
 *
 * Copyright 2012 Viettel Telecom. All rights reserved.
 * VIETTEL PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */

/** 
* @author  
* @version $Revision: $
*/

using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Linq.Expressions;
using System.Web;
using log4net;

using SMAS.DAL.Repository;
using SMAS.DAL.IRepository;
using SMAS.Business.IBusiness;
using SMAS.Models.Models;
using SMAS.Business.Common;
namespace SMAS.Business.Business
{ 
    public partial class GroupMenuBusiness
    {
        IGroupMenuRepository GroupMenuRepository;
      
        public GroupMenuBusiness(ILog logger, SMASEntities context = null)
            : base(logger)
        {
            if (context == null) { context = new SMASEntities(); } this.context = context;
            this.GroupMenuRepository = new GroupMenuRepository(context);
            repository = GroupMenuRepository;
            
        }

        public IQueryable<GroupMenu> GetGroupMenus(int GroupID)
        {
            if (GroupID == 0) return null;
            IQueryable<GroupMenu> groupMenus = GroupMenuRepository.All.Where(gm => gm.GroupID == GroupID);
            return groupMenus;
        }
        public IQueryable<Menu> GetMenuOfGroup(int GroupID)
        {
            if (GroupID == 0) return null;
            IQueryable<GroupMenu> groupMenus = GroupMenuRepository.All.Where(gm => gm.GroupID == GroupID);
            if (groupMenus == null) return null;
            return groupMenus.Select(o => o.Menu).Where(o => o.IsActive).Distinct();
        }

        public IQueryable<Menu> GetMenuOfListGroup(List<int> ListGroupID)
        {
            if (ListGroupID.Count == 0) return null;
            IQueryable<GroupMenu> groupMenus = GroupMenuRepository.All.Where(gm => ListGroupID.Contains(gm.GroupID));
            if (groupMenus == null) return null;            
            return groupMenus.Select(o => o.Menu).Where(o => o.IsActive).Distinct();
        }

        public IQueryable<Menu> GetMenuOfGroupByPermission(int GroupID, int Permission)
        {
            if (GroupID == 0 || Permission == 0)
                return null;
            IQueryable<GroupMenu> groupMenus = GroupMenuRepository.All.Where(gm => gm.GroupID == GroupID && gm.Permission <= Permission);
            if (groupMenus == null) return null;
            return groupMenus.Select(o => o.Menu).Where(o => o.IsActive).Distinct();
        }

        public bool CheckMenuPermissionOfGroup(int GroupID, int Permission, int MenuID)
        {
            if (GroupID == 0 || Permission == 0 || MenuID == 0) return false;
            IQueryable<GroupMenu> gms = GroupMenuRepository.All.Where(gm => gm.GroupID == GroupID && gm.MenuID == MenuID && gm.Permission == Permission);
            if (gms == null) return false;
            if (gms.Count() == 1) return true;
            else return false;
        }
        public bool CheckMenuPermissionOfGroup(int GroupID, int Permission, string ControllerName, string ActionName, string AreaName)
        {
            Menu m = MenuBusiness.GetMenuByControllerAndActionName(ControllerName, ActionName, AreaName);
            if (m == null) return false;
            else return this.CheckMenuPermissionOfGroup(GroupID, Permission, m.MenuID);
        }
    }
}