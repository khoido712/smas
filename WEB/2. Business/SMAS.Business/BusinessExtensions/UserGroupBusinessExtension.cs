﻿/**
 * The Viettel License
 *
 * Copyright 2012 Viettel Telecom. All rights reserved.
 * VIETTEL PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */

/** 
* @author  
* @version $Revision: $
*/

using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Linq.Expressions;
using System.Web;
using log4net;

using SMAS.DAL.Repository;
using SMAS.DAL.IRepository;
using SMAS.Business.IBusiness;
using SMAS.Models.Models;
using SMAS.Business.Common;
using SMAS.Models.Models.CustomModels;
namespace SMAS.Business.Business
{
    public partial class UserGroupBusiness
    {
        IUserGroupRepository UserGroupRepository;
        IUserAccountRepository UserAccountRepository;
        IGroupCatRepository GroupCatRepository;
        public UserGroupBusiness(ILog logger, SMASEntities context = null)
            : base(logger)
        {
            if (context == null) { context = new SMASEntities(); } this.context = context;
            this.UserGroupRepository = new UserGroupRepository(context);
            this.UserAccountRepository = new UserAccountRepository(context);
            this.GroupCatRepository = new GroupCatRepository(context);
            repository = UserGroupRepository;
        }

        public bool CheckPrivilegeToAssignGroup(int LogonUserID, List<int> ListGroupID, int RoleID, int UserAccountID)
        {
            if (LogonUserID == 0 || RoleID == 0 || UserAccountID == 0)
            {
                return false;
            }
            SupervisingDept sd = new SupervisingDept();
            SchoolProfile sp = new SchoolProfile();
            Employee employee = new Employee();
            bool isAdmin = UserAccountBusiness.IsAdmin(LogonUserID);
            if (isAdmin)
            {
                sd = SupervisingDeptBusiness.All.Where(o => o.AdminID == LogonUserID).FirstOrDefault();
                sp = SchoolProfileBusiness.All.Where(o => o.AdminID == LogonUserID).FirstOrDefault();
            }
            else
            {
                employee = UserAccountBusiness.MapFromUserAccountToEmployee(LogonUserID);
                sd = employee.SupervisingDept;
                sp = employee.SchoolProfile;
            }

            if (sd != null)//Account Sở phòng
            {
                SupervisingDept sd1 = new SupervisingDept();
                foreach (int groupID in ListGroupID)
                {
                    GroupCat groupcat = GroupCatBusiness.Find(groupID);
                    if (!groupcat.IsActive)
                    {
                        throw new BusinessException();
                    }
                    sd1 = SupervisingDeptBusiness.GetSupervisingDeptOfUser(groupcat.AdminUserID);

                    if (sd1 != null && sd.SupervisingDeptID != sd1.SupervisingDeptID)
                    {
                        throw new BusinessException();
                    }

                    //if (!GroupCatRepository.All.Where(gc => gc.GroupCatID == groupID && gc.AdminUserID == LogonUserID && gc.IsActive == true).Any())
                    //{
                    //    //SupervisingDept sd1 = new SupervisingDept();
                    //    //ko co quyen, chi nguoi tao group moi co quyen gan
                    //    throw new BusinessException();
                    //}
                }
            }
            else if (sp != null)
            {
                SchoolProfile sp1 = new SchoolProfile();
                foreach (int groupID in ListGroupID)
                {
                    GroupCat groupcat = GroupCatBusiness.Find(groupID);
                    if (!groupcat.IsActive)
                    {
                        throw new BusinessException();
                    }
                    if (UserAccountBusiness.IsAdmin(groupcat.AdminUserID))
                    {
                        sp1 = SchoolProfileBusiness.All.Where(o => o.AdminID == groupcat.AdminUserID).FirstOrDefault();
                    }
                    else
                    {
                        employee = UserAccountBusiness.MapFromUserAccountToEmployee(groupcat.AdminUserID);
                        sp1 = employee.SchoolProfile;
                    }
                    if (sp1!= null && sp1.SchoolProfileID != sp.SchoolProfileID)
                    {
                        throw new BusinessException();
                    }
                }
            }
            return true;
        }

        public void AssignGroupsForUser(List<GroupCheck> lsGroupCheck, int userAccountID)
        {
            //danh sach cac group da duoc gan cho User
            List<UserGroup> listUg = this.All.Where(ug => ug.UserID == userAccountID).ToList();
            if (listUg.Any())
            {
                //Neu da co userAccountID thi chi insert nhung group chua ton tai
                //danh sach Group duoc chon de gan
                foreach (GroupCheck ug in lsGroupCheck.Where(gc => gc.isChecked == true).ToList())
                {
                    //neu group duoc chon khong nam trong listUg thi insert
                    if (!listUg.Where(g => g.GroupID == ug.group.GroupCatID).Any())
                    {
                        UserGroup userGroup = new UserGroup { GroupID = ug.group.GroupCatID, UserID = userAccountID };
                        this.Insert(userGroup);
                    }
                }
            }
            else
            {
                foreach (GroupCheck ug in lsGroupCheck.Where(gc => gc.isChecked == true).ToList())
                {
                    //neu group duoc chon khong nam trong listUg thi insert
                    UserGroup userGroup = new UserGroup { GroupID = ug.group.GroupCatID, UserID = userAccountID };
                    this.Insert(userGroup);
                }
            }
        }

        public bool AssignGroupsForUser(int LogonUserID, List<int> ListGroupID, int RoleID, int UserAccountID)
        {
            if (!CheckPrivilegeToAssignGroup(LogonUserID, ListGroupID, RoleID, UserAccountID))
            {
                return false;
            }
            if (ListGroupID != null && ListGroupID.Count > 0)
            {
                foreach (int groupID in ListGroupID)
                {
                    UserGroup ug = new UserGroup { UserID = UserAccountID, GroupID = groupID };
                    UserGroupRepository.Insert(ug);
                }
            }
            return true;
        }


        public IQueryable<UserAccount> GetUsersInGroup(int GroupID)
        {
            if (GroupID == 0) return null;
            IQueryable<UserGroup> userGroups = UserGroupRepository.All.Where(ug => ug.GroupID == GroupID);
            if (userGroups == null) return null;
            return userGroups.Select(o => o.UserAccount).Where(o => o.IsActive);
        }

        public IQueryable<GroupCat> GetGroupsOfUser(int UserAccountID)
        {
            if (UserAccountID == 0) return null;
            IQueryable<UserGroup> userGroups = UserGroupRepository.All.Where(ug => ug.UserID == UserAccountID);
            if (userGroups == null) return null;
            return userGroups.Select(o => o.GroupCat).Where(o => o.IsActive);
        }

        public IQueryable<UserGroup> GetUserGroupsByUser(int UserAccountID,int RoleID)
        {
            
            if (UserAccountID == 0) return null;
            IQueryable<UserGroup> userGroups = UserGroupRepository.All.Where(ug => ug.UserID == UserAccountID && ug.GroupCat.IsActive == true && ug.GroupCat.RoleID == RoleID);
            if (userGroups == null) return null;
            return userGroups;
        }
    }
}