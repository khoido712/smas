﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SMAS.Models.Models;
using SMAS.Business.BusinessObject;
using SMAS.Business.Common;
using System.Transactions;
using System.IO;
using SMAS.VTUtils.Excel.Export;
using System.Data.Entity;
using System.Data;
using SMAS.VTUtils.Log;

namespace SMAS.Business.Business
{
    public partial class EvaluationCommentsBusiness
    {
        private static string CommentStr = "Nhận xét tháng: ";

        public List<EvaluationCommentsBO> getListEvaluationCommentsBySchool(int schoolID, int academicYearID, int classID, int semesterID, int subjectID, int TypeOfTeacher, int evaluationID)
        {
            AcademicYear objAcademicYear = AcademicYearBusiness.Find(academicYearID);
            if (UtilsBusiness.IsMoveHistory(objAcademicYear))//Neu da chuyen vao bang lich su thi lay du lieu tu bang lich su
            {
                List<EvaluationCommentsBO> listResultHistory = EvaluationCommentsHistoryBusiness.GetEvaluationCommentsHistoryByClass(schoolID, academicYearID, classID, semesterID, subjectID, TypeOfTeacher, evaluationID);
                return listResultHistory;
            }
            int partitionId = UtilsBusiness.GetPartionId(schoolID);
            //list hoc sinh trong lop
            List<PupilOfClassBO> pupilOfClassList = PupilOfClassBusiness.GetPupilInClass(classID).OrderBy(c => c.OrderInClass).ThenBy(c => c.Name).ThenBy(c => c.PupilFullName).ToList();
            //List<long> pupilIDList = pupilOfClassList.Select(p => Convert.ToInt64(p.PupilID)).ToList();
            //list hoc sinh duoc danh gia.
            List<EvaluationComments> listEvaluationComments = new List<EvaluationComments>();
            //Neu la GVCN
            if (TypeOfTeacher == GlobalConstants.EVALUTION_HEAD_TEACHER)
            {
                listEvaluationComments = (from ev in EvaluationCommentsBusiness.All
                                          where
                                          ev.LastDigitSchoolID == partitionId
                                          && ev.AcademicYearID == academicYearID
                                           && ev.ClassID == classID
                                           && ev.SemesterID == semesterID
                                              //&& ev.EvaluationCriteriaID == subjectID
                                           && ev.TypeOfTeacher == TypeOfTeacher
                                           && ev.EvaluationID == evaluationID //tab mat danh gia
                                          //&& pupilIDList.Contains(ev.PupilID)
                                          select ev).ToList();
            }
            else // GVBM
            {
                listEvaluationComments = (from ev in EvaluationCommentsBusiness.All
                                          where
                                          ev.LastDigitSchoolID == partitionId
                                          && ev.AcademicYearID == academicYearID
                                           && ev.ClassID == classID
                                           && ev.SemesterID == semesterID
                                           && ev.EvaluationCriteriaID == subjectID
                                           && ev.TypeOfTeacher == TypeOfTeacher
                                           && ev.EvaluationID == evaluationID //tab mat danh gia
                                          //&& pupilIDList.Contains(ev.PupilID)
                                          select ev).ToList();
            }
            //Danh gia nhan xet cuoi ky
            List<SummedEvaluation> listSummedEvaluation = (from ev in SummedEvaluationBusiness.All
                                                           where
                                                            ev.LastDigitSchoolID == partitionId
                                                            && ev.AcademicYearID == academicYearID
                                                            && ev.ClassID == classID
                                                            && ev.SemesterID == semesterID
                                                               //&& ev.EvaluationCriteriaID == subjectID
                                                            && ev.EvaluationID == evaluationID //tab mat danh gia
                                                           //&& pupilIDList.Contains(ev.PupilID)
                                                           select ev).ToList();
            //List Danh gia cua hoc sinh theo hoc ky
            List<SummedEndingEvaluation> listSummedEndingEvaluation = (from sn in SummedEndingEvaluationBusiness.All
                                                                       where
                                                                       sn.LastDigitSchoolID == partitionId
                                                                       && sn.AcademicYearID == academicYearID
                                                                       && sn.ClassID == classID
                                                                       && sn.SemesterID == semesterID
                                                                       && sn.EvaluationID == evaluationID
                                                                       select sn).ToList();


            //list hoc sinh cua lop da duoc danh gia va chua danh gia
            List<EvaluationCommentsBO> listResult = new List<EvaluationCommentsBO>();
            EvaluationCommentsBO evaluationBO = null;
            EvaluationComments ObjTemp;
            SummedEndingEvaluation summedEndingObj = null;
            int pupilID = 0;
            PupilOfClassBO pocObj;
            SummedEvaluation objSE;
            DateTime? MaxUpdateTime = null;
            if (listEvaluationComments.Count > 0 || listSummedEvaluation.Count > 0 || listSummedEndingEvaluation.Count > 0)
            {
                List<EvaluationComments> lsttmpEC = listEvaluationComments.Where(p => !string.IsNullOrEmpty(p.CommentsM1M6) || !string.IsNullOrEmpty(p.CommentsM2M7)
                                                    || !string.IsNullOrEmpty(p.CommentsM3M8) || !string.IsNullOrEmpty(p.CommentsM4M9) || !string.IsNullOrEmpty(p.CommentsM5M10)).ToList();
                List<SummedEvaluation> lstSE = listSummedEvaluation.Where(p => !string.IsNullOrEmpty(p.EndingComments) || p.PeriodicEndingMark.HasValue || !string.IsNullOrEmpty(p.EndingEvaluation)).ToList();
                List<SummedEndingEvaluation> lstSEE = listSummedEndingEvaluation.Where(p => !string.IsNullOrEmpty(p.EndingEvaluation)).ToList();
                if (lsttmpEC.Count > 0 || lstSE.Count > 0 || lstSEE.Count > 0)
                {
                    MaxUpdateTime = lsttmpEC.Select(p => (p.UpdateTime.HasValue ? p.UpdateTime : p.CreateTime))
                                        .Union(lstSE.Select(p => (p.UpdateTime.HasValue ? p.UpdateTime : p.CreateTime)))
                                        .Union(lstSEE.Select(p => (p.UpdateTime.HasValue ? p.UpdateTime : p.CreateTime))).Max();
                }
            }
            listSummedEvaluation = listSummedEvaluation.Where(p => p.EvaluationCriteriaID == subjectID).ToList();
            if (pupilOfClassList != null && pupilOfClassList.Count > 0)
            {
                for (int i = 0; i < pupilOfClassList.Count; i++)
                {
                    pocObj = pupilOfClassList[i];
                    pupilID = pocObj.PupilID;
                    ObjTemp = listEvaluationComments.Where(p => p.PupilID == pupilID).FirstOrDefault();
                    evaluationBO = new EvaluationCommentsBO();
                    evaluationBO.FullName = pocObj.PupilFullName;
                    evaluationBO.Name = pocObj.Name;
                    evaluationBO.PupilID = pocObj.PupilID;
                    evaluationBO.Status = pocObj.Status;
                    evaluationBO.PupilCode = pocObj.PupilCode;
                    evaluationBO.Birthday = pocObj.Birthday;
                    evaluationBO.OrderInClass = pocObj.OrderInClass ?? 0;
                    if (ObjTemp != null)
                    {
                        evaluationBO.EvaluationCommentsID = ObjTemp.EvaluationCommentsID;
                        evaluationBO.Comment1 = ObjTemp.CommentsM1M6;
                        evaluationBO.Comment2 = ObjTemp.CommentsM2M7;
                        evaluationBO.Comment3 = ObjTemp.CommentsM3M8;
                        evaluationBO.Comment4 = ObjTemp.CommentsM4M9;
                        evaluationBO.Comment5 = ObjTemp.CommentsM5M10;
                        evaluationBO.EvaluationCriteriaID = ObjTemp.EvaluationCriteriaID;
                    }
                    else
                    {
                        evaluationBO.EvaluationCommentsID = 0;
                        evaluationBO.Comment1 = string.Empty;
                        evaluationBO.Comment2 = string.Empty;
                        evaluationBO.Comment3 = string.Empty;
                        evaluationBO.Comment4 = string.Empty;
                        evaluationBO.Comment5 = string.Empty;
                        evaluationBO.CommentEnding = string.Empty;
                        evaluationBO.PiriodicEndingMark = null;
                        evaluationBO.EvaluationEnding = string.Empty;
                        evaluationBO.RetestMark = null;
                        evaluationBO.EvaluationReTraining = string.Empty;
                        evaluationBO.EvaluationCommentsID = 0;
                    }

                    objSE = listSummedEvaluation.Where(s => s.PupilID == pupilID).FirstOrDefault();
                    if (objSE != null)
                    {
                        evaluationBO.CommentEnding = objSE.EndingComments;
                        evaluationBO.PiriodicEndingMark = objSE.PeriodicEndingMark;
                        evaluationBO.CreateTime = objSE.CreateTime;
                        evaluationBO.UpdateTime = objSE.UpdateTime;
                        evaluationBO.RetestMark = objSE.RetestMark;
                        //evaluationBO.EvaluationReTraining = objSE.EvaluationReTraining;
                        evaluationBO.SummedEvaluationID = objSE.SummedEvaluationID;
                        if (evaluationID == GlobalConstants.TAB_EDUCATION_BOOKMARK_PRIMARY)
                        {
                            evaluationBO.EvaluationEnding = objSE.EndingEvaluation;
                        }
                    }

                    if (evaluationID == GlobalConstants.TAB_CAPACTIES_BOOKMARK_PRIMARY || evaluationID == GlobalConstants.TAB_QUALITIES_BOOKMARK_PRIMARY)
                    {
                        summedEndingObj = listSummedEndingEvaluation.Where(p => p.PupilID == pupilID).FirstOrDefault();
                        if (summedEndingObj != null)
                        {
                            evaluationBO.SummedEndingEvaluationID = summedEndingObj.SummedEndingEvaluationID;
                            evaluationBO.EvaluationEnding = summedEndingObj.EndingEvaluation;
                            evaluationBO.EvaluationReTraining = summedEndingObj.EvaluationReTraining;
                        }
                    }
                    evaluationBO.MaxUpdateTime = MaxUpdateTime;
                    listResult.Add(evaluationBO);
                }
            }
            return listResult.OrderBy(c => c.OrderInClass).ThenBy(c => SMAS.Business.Common.Utils.SortABC(c.Name + " " + c.FullName)).ToList();
        }

        public List<EvaluationCommentsBO> getListEvaluationCommentsBySchoolExport(int schoolID, int academicYearID, int classID, int semesterID, int subjectID, int TypeOfTeacher, int evaluationID)
        {
            AcademicYear objAcademicYear = AcademicYearBusiness.Find(academicYearID);
            if (UtilsBusiness.IsMoveHistory(objAcademicYear))
            {
                List<EvaluationCommentsBO> lstResult = EvaluationCommentsHistoryBusiness.getListEvaluationCommentsHistoryBySchoolExport(schoolID, academicYearID, classID, semesterID, subjectID, TypeOfTeacher, evaluationID);
                return lstResult;
            }
            int partitionId = UtilsBusiness.GetPartionId(schoolID);
            //list hoc sinh trong lop
            List<PupilOfClassBO> pupilOfClassList = PupilOfClassBusiness.GetPupilInClass(classID).ToList();

            //Danh gia nhan xet cuoi ky
            List<SummedEvaluation> listSummedEvaluation = (from ev in SummedEvaluationBusiness.All
                                                           where
                                                            ev.LastDigitSchoolID == partitionId
                                                            && ev.AcademicYearID == academicYearID
                                                            && ev.ClassID == classID
                                                            && ev.SemesterID == semesterID
                                                            && ev.EvaluationID == evaluationID //tab mat danh gia
                                                           //&& pupilIDList.Contains(ev.PupilID)
                                                           select ev).ToList();
            //List Danh gia cua hoc sinh theo hoc ky
            List<SummedEndingEvaluation> listSummedEndingEvaluation = (from sn in SummedEndingEvaluationBusiness.All
                                                                       where
                                                                       sn.LastDigitSchoolID == partitionId
                                                                       && sn.AcademicYearID == academicYearID
                                                                       && sn.ClassID == classID
                                                                       && sn.SemesterID == semesterID
                                                                       && sn.EvaluationID == evaluationID
                                                                       select sn).ToList();


            //list hoc sinh cua lop da duoc danh gia va chua danh gia
            List<EvaluationCommentsBO> listResult = new List<EvaluationCommentsBO>();
            EvaluationCommentsBO evaluationBO = null;
            SummedEndingEvaluation summedEndingObj = null;
            int pupilID = 0;
            PupilOfClassBO pocObj;
            List<SummedEvaluation> summedEvaluationList;
            if (pupilOfClassList != null && pupilOfClassList.Count > 0)
            {
                for (int i = 0; i < pupilOfClassList.Count; i++)
                {
                    pocObj = pupilOfClassList[i];
                    pupilID = pocObj.PupilID;
                    summedEvaluationList = listSummedEvaluation.Where(s => s.PupilID == pupilID).ToList();
                    summedEndingObj = listSummedEndingEvaluation.Where(p => p.PupilID == pupilID).FirstOrDefault();

                    if (summedEvaluationList != null && summedEvaluationList.Count > 0)
                    {
                        for (int j = 0; j < summedEvaluationList.Count; j++)
                        {
                            evaluationBO = new EvaluationCommentsBO();
                            evaluationBO.FullName = pocObj.PupilFullName;
                            evaluationBO.Name = pocObj.Name;
                            evaluationBO.PupilID = pocObj.PupilID;
                            evaluationBO.Status = pocObj.Status;
                            evaluationBO.PupilCode = pocObj.PupilCode;
                            evaluationBO.Birthday = pocObj.Birthday;
                            evaluationBO.OrderInClass = pocObj.OrderInClass ?? 0;
                            evaluationBO.CommentEnding = summedEvaluationList[j].EndingComments;
                            evaluationBO.SummedEvaluationID = summedEvaluationList[j].SummedEvaluationID;
                            if (summedEndingObj != null)
                            {
                                evaluationBO.SummedEndingEvaluationID = summedEndingObj.SummedEndingEvaluationID;
                                evaluationBO.EvaluationEnding = summedEndingObj.EndingEvaluation;
                                evaluationBO.EvaluationReTraining = summedEndingObj.EvaluationReTraining;
                            }
                            evaluationBO.EvaluationCriteriaID = summedEvaluationList[j].EvaluationCriteriaID;
                            listResult.Add(evaluationBO);
                        }
                    }
                    else
                    {
                        evaluationBO = new EvaluationCommentsBO();
                        evaluationBO.FullName = pocObj.PupilFullName;
                        evaluationBO.Name = pocObj.Name;
                        evaluationBO.PupilID = pocObj.PupilID;
                        evaluationBO.Status = pocObj.Status;
                        evaluationBO.PupilCode = pocObj.PupilCode;
                        evaluationBO.Birthday = pocObj.Birthday;
                        evaluationBO.OrderInClass = pocObj.OrderInClass ?? 0;
                        evaluationBO.CommentEnding = string.Empty;
                        evaluationBO.SummedEvaluationID = 0;
                        evaluationBO.SummedEndingEvaluationID = 0;
                        evaluationBO.EvaluationEnding = string.Empty;
                        evaluationBO.EvaluationCriteriaID = 0;
                        evaluationBO.EvaluationReTraining = string.Empty;
                        listResult.Add(evaluationBO);
                    }
                }
            }
            return listResult;
        }

        /// <summary>
        /// Insert hoac update nhan xet cap 1(Nếu insert thì truyền noteinsert vào, update không cần)
        /// </summary>
        /// <param name="schoolID"></param>
        /// <param name="academicYearID"></param>
        /// <param name="classID"></param>
        /// <param name="educationLevelID"></param>
        /// <param name="semesterID"></param>
        /// <param name="evaluationID">Id cac tieu chi (tab dang chon)</param>
        /// <param name="evaluationCriteriaID">ID các mặt đánh giá (nếu là tab 1 thì là ID môn học) </param>
        /// <param name="typeOfTeacher">1:GVBM, 2: GVCN</param>
        /// <param name="listInsertOrUpdate"></param>
        public List<EvaluationCommentActionAuditBO> InsertOrUpdateEvaluationComments(int MonthID, int schoolID, int academicYearID, int classID, int educationLevelID, int semesterID, int evaluationID, int evaluationCriteriaID, int typeOfTeacher, List<EvaluationComments> listInsertOrUpdate, List<SummedEvaluation> listInsertOrUpdateSummed, List<SummedEndingEvaluation> listInsertOrUpdateSummedEnding)
        {
            List<EvaluationCommentActionAuditBO> lstActionAuditBO = new List<EvaluationCommentActionAuditBO>();
            try
            {
                context.Configuration.AutoDetectChangesEnabled = false;
                context.Configuration.ValidateOnSaveEnabled = false;
                int partitionNumber = schoolID % GlobalConstants.PARTITION;
                using (TransactionScope scope = new TransactionScope())
                {

                    //Lay danh sach lop cua truong
                    ClassProfile cp = ClassProfileBusiness.Find(classID);

                    //danh sach hoc sinh cua lop
                    List<PupilOfClass> lstPoc = PupilOfClassBusiness.All.Where
                        (o => o.AcademicYearID == academicYearID && o.ClassID == classID).ToList();

                    //Lay nam hoc
                    AcademicYear aca = AcademicYearBusiness.Find(academicYearID);

                    //Lay mon hoc
                    List<SubjectCat> lstSuject = SubjectCatBusiness.All.Where(o => o.AppliedLevel == GlobalConstants.APPLIED_LEVEL_PRIMARY
                        ).ToList();

                    //Lay tieu chi
                    List<EvaluationCriteria> lstEvaluationCriteria = EvaluationCriteriaBusiness.All.ToList();

                    #region Them nhan xet cho thang
                    if (listInsertOrUpdate != null && listInsertOrUpdate.Count > 0)
                    {
                        List<EvaluationComments> evaluationCommentsList = EvaluationCommentsBusiness.All
                        .Where(p => p.LastDigitSchoolID == partitionNumber

                                 && p.ClassID == classID
                                 && p.AcademicYearID == academicYearID
                            //&& p.EducationLevelID == educationLevelID
                                 && p.SemesterID == semesterID
                                 && p.EvaluationID == evaluationID
                                 && p.EvaluationCriteriaID == evaluationCriteriaID
                                 && p.TypeOfTeacher == typeOfTeacher).ToList();

                        List<EvaluationComments> InsertList = new List<EvaluationComments>();
                        List<EvaluationComments> UpdateList = new List<EvaluationComments>();
                        EvaluationComments checkInlist = null;


                        for (int i = 0; i < listInsertOrUpdate.Count; i++)
                        {
                            EvaluationComments evaluationCommentsObj = listInsertOrUpdate[i];
                            long pupilID = evaluationCommentsObj.PupilID;
                            string strMonth = string.Empty;
                            //kiem tra pupilID da ton tai trong list da select len chua
                            checkInlist = evaluationCommentsList.Where(p => p.PupilID == pupilID).FirstOrDefault();
                            if (checkInlist == null)
                            {
                                // insert thong tin moi
                                evaluationCommentsObj.UpdateTime = null;
                                InsertList.Add(evaluationCommentsObj);
                            }
                            else
                            {
                                //update thong tin
                                if (MonthID == GlobalConstants.MonthID_1 || MonthID == GlobalConstants.MonthID_6)
                                {
                                    if (MonthID == GlobalConstants.MonthID_1)
                                    {
                                        strMonth += "T1,";
                                    }
                                    else
                                    {
                                        strMonth += "T6,";
                                    }
                                    checkInlist.CommentsM1M6 = evaluationCommentsObj.CommentsM1M6;
                                }
                                else if (MonthID == GlobalConstants.MonthID_2 || MonthID == GlobalConstants.MonthID_7)
                                {
                                    if (MonthID == GlobalConstants.MonthID_2)
                                    {
                                        strMonth += "T2,";
                                    }
                                    else
                                    {
                                        strMonth += "T7,";
                                    }
                                    checkInlist.CommentsM2M7 = evaluationCommentsObj.CommentsM2M7;
                                }
                                else if (MonthID == GlobalConstants.MonthID_3 || MonthID == GlobalConstants.MonthID_8)
                                {
                                    if (MonthID == GlobalConstants.MonthID_3)
                                    {
                                        strMonth += "T3,";
                                    }
                                    else
                                    {
                                        strMonth += "T8,";
                                    }
                                    checkInlist.CommentsM3M8 = evaluationCommentsObj.CommentsM3M8;
                                }
                                else if (MonthID == GlobalConstants.MonthID_4 || MonthID == GlobalConstants.MonthID_9)
                                {
                                    if (MonthID == GlobalConstants.MonthID_4)
                                    {
                                        strMonth += "T4,";
                                    }
                                    else
                                    {
                                        strMonth += "T9,";
                                    }
                                    checkInlist.CommentsM4M9 = evaluationCommentsObj.CommentsM4M9;
                                }
                                else if (MonthID == GlobalConstants.MonthID_5 || MonthID == GlobalConstants.MonthID_10)
                                {
                                    if (MonthID == GlobalConstants.MonthID_5)
                                    {
                                        strMonth += "T5,";
                                    }
                                    else
                                    {
                                        strMonth += "T10,";
                                    }
                                    checkInlist.CommentsM5M10 = evaluationCommentsObj.CommentsM5M10;
                                }
                                checkInlist.UpdateTime = DateTime.Now;
                                UpdateList.Add(checkInlist);
                            }
                        }



                        if (InsertList != null && InsertList.Count > 0)
                        {
                            foreach (EvaluationComments insertObj in InsertList)
                            {
                                insertObj.EvaluationCommentsID = EvaluationCommentsBusiness.GetNextSeq<long>();

                                if (!String.IsNullOrEmpty(insertObj.CommentsM1M6)
                                    || !String.IsNullOrEmpty(insertObj.CommentsM2M7)
                                    || !String.IsNullOrEmpty(insertObj.CommentsM3M8)
                                    || !String.IsNullOrEmpty(insertObj.CommentsM4M9)
                                    || !String.IsNullOrEmpty(insertObj.CommentsM5M10))
                                {
                                    EvaluationCommentsBusiness.Insert(insertObj);


                                    //Tao action audit
                                    EvaluationCommentActionAuditBO aab = new EvaluationCommentActionAuditBO(EvaluationCommentActionAuditBO.EVALUATION_COMMENT_AUDIT_ACTION_UPDATE);
                                    aab.AcademicYear = aca.DisplayTitle;
                                    aab.ClassName = cp.DisplayName;
                                    aab.Month = MonthID;
                                    aab.ObjectId = insertObj.PupilID;
                                    PupilOfClass poc = lstPoc.Where(o => o.PupilID == insertObj.PupilID).FirstOrDefault();
                                    aab.PupilCode = poc.PupilProfile.PupilCode;
                                    aab.PupilName = poc.PupilProfile.FullName;
                                    aab.Semester = semesterID;
                                    SubjectCat sc = lstSuject.Where(o => o.SubjectCatID == insertObj.EvaluationCriteriaID).FirstOrDefault();
                                    if (sc != null)
                                    {
                                        aab.SubjectName = sc.DisplayName;
                                    }
                                    EvaluationCriteria ec = lstEvaluationCriteria.FirstOrDefault(o => o.EvaluationCriteriaID == insertObj.EvaluationCriteriaID);
                                    if (ec != null)
                                    {
                                        aab.EvaluationCriteriaName = ec.CriteriaName;
                                    }
                                    aab.EvaluationID = evaluationID;

                                    string val = "";
                                    if (MonthID == 1 || MonthID == 6)
                                    {
                                        val = insertObj.CommentsM1M6;
                                    }
                                    else if (MonthID == 2 || MonthID == 7)
                                    {
                                        val = insertObj.CommentsM2M7;
                                    }
                                    else if (MonthID == 3 || MonthID == 8)
                                    {
                                        val = insertObj.CommentsM3M8;
                                    }
                                    else if (MonthID == 4 || MonthID == 9)
                                    {
                                        val = insertObj.CommentsM4M9;
                                    }
                                    else if (MonthID == 5 || MonthID == 10)
                                    {
                                        val = insertObj.CommentsM5M10;
                                    }
                                    aab.NewObjectValue.Add(CRITERIA_NXT, val);
                                    aab.OldObjectValue.Add(CRITERIA_NXT, string.Empty);



                                    lstActionAuditBO.Add(aab);
                                }
                            }
                        }

                        if (UpdateList != null && UpdateList.Count > 0)
                        {
                            foreach (EvaluationComments updateObj in UpdateList)
                            {
                                if (String.IsNullOrEmpty(updateObj.CommentsM1M6)) updateObj.CommentsM1M6 = null;
                                if (String.IsNullOrEmpty(updateObj.CommentsM2M7)) updateObj.CommentsM2M7 = null;
                                if (String.IsNullOrEmpty(updateObj.CommentsM3M8)) updateObj.CommentsM3M8 = null;
                                if (String.IsNullOrEmpty(updateObj.CommentsM4M9)) updateObj.CommentsM4M9 = null;
                                if (String.IsNullOrEmpty(updateObj.CommentsM5M10)) updateObj.CommentsM5M10 = null;

                                if (updateObj.CommentsM1M6 != this.context.Entry<EvaluationComments>(updateObj).OriginalValues.GetValue<string>("CommentsM1M6")
                                    || updateObj.CommentsM2M7 != this.context.Entry<EvaluationComments>(updateObj).OriginalValues.GetValue<string>("CommentsM2M7")
                                    || updateObj.CommentsM3M8 != this.context.Entry<EvaluationComments>(updateObj).OriginalValues.GetValue<string>("CommentsM3M8")
                                    || updateObj.CommentsM4M9 != this.context.Entry<EvaluationComments>(updateObj).OriginalValues.GetValue<string>("CommentsM4M9")
                                    || updateObj.CommentsM5M10 != this.context.Entry<EvaluationComments>(updateObj).OriginalValues.GetValue<string>("CommentsM5M10"))
                                {
                                    EvaluationCommentsBusiness.Update(updateObj);

                                    //Tao action audit
                                    EvaluationCommentActionAuditBO aab = new EvaluationCommentActionAuditBO(EvaluationCommentActionAuditBO.EVALUATION_COMMENT_AUDIT_ACTION_UPDATE);
                                    aab.AcademicYear = aca.DisplayTitle;
                                    aab.ClassName = cp.DisplayName;
                                    aab.Month = MonthID;
                                    aab.ObjectId = updateObj.PupilID;

                                    string oldVal = "";
                                    string newVal = "";
                                    if (MonthID == 1 || MonthID == 6)
                                    {
                                        newVal = updateObj.CommentsM1M6;
                                        oldVal = this.context.Entry<EvaluationComments>(updateObj).OriginalValues.GetValue<string>("CommentsM1M6");
                                    }
                                    else if (MonthID == 2 || MonthID == 7)
                                    {
                                        newVal = updateObj.CommentsM2M7;
                                        oldVal = this.context.Entry<EvaluationComments>(updateObj).OriginalValues.GetValue<string>("CommentsM2M7");
                                    }
                                    else if (MonthID == 3 || MonthID == 8)
                                    {
                                        newVal = updateObj.CommentsM3M8;
                                        oldVal = this.context.Entry<EvaluationComments>(updateObj).OriginalValues.GetValue<string>("CommentsM3M8");
                                    }
                                    else if (MonthID == 4 || MonthID == 9)
                                    {
                                        newVal = updateObj.CommentsM4M9;
                                        oldVal = this.context.Entry<EvaluationComments>(updateObj).OriginalValues.GetValue<string>("CommentsM4M9");
                                    }
                                    else if (MonthID == 5 || MonthID == 10)
                                    {
                                        newVal = updateObj.CommentsM5M10;
                                        oldVal = this.context.Entry<EvaluationComments>(updateObj).OriginalValues.GetValue<string>("CommentsM5M10");
                                    }
                                    aab.NewObjectValue.Add(CRITERIA_NXT, newVal);
                                    aab.OldObjectValue.Add(CRITERIA_NXT, oldVal);

                                    PupilOfClass poc = lstPoc.Where(o => o.PupilID == updateObj.PupilID).FirstOrDefault();
                                    aab.PupilCode = poc.PupilProfile.PupilCode;
                                    aab.PupilName = poc.PupilProfile.FullName;
                                    aab.Semester = semesterID;
                                    SubjectCat sc = lstSuject.Where(o => o.SubjectCatID == updateObj.EvaluationCriteriaID).FirstOrDefault();
                                    if (sc != null)
                                    {
                                        aab.SubjectName = sc.DisplayName;
                                    }
                                    EvaluationCriteria ec = lstEvaluationCriteria.FirstOrDefault(o => o.EvaluationCriteriaID == updateObj.EvaluationCriteriaID);
                                    if (ec != null)
                                    {
                                        aab.EvaluationCriteriaName = ec.CriteriaName;
                                    }
                                    aab.EvaluationID = evaluationID;

                                    lstActionAuditBO.Add(aab);
                                }

                            }
                        }

                        EvaluationCommentsBusiness.Save();
                    }
                    #endregion

                    #region Them du lieu nhận xét cuối kỳ vào summedEvaluation (chỉ insert tab CLGD va nhan xet cuoi ky)
                    if (listInsertOrUpdateSummed != null && listInsertOrUpdateSummed.Count > 0)
                    {
                        List<SummedEvaluation> listSummedEvaluation = SummedEvaluationBusiness.All.Where(p => p.LastDigitSchoolID == partitionNumber

                                 && p.ClassID == classID
                                 && p.AcademicYearID == academicYearID
                                 && p.EducationLevelID == educationLevelID
                                 && p.SemesterID == semesterID
                                 && p.EvaluationID == evaluationID
                                 && p.EvaluationCriteriaID == evaluationCriteriaID).ToList();
                        List<SummedEvaluation> InsertListSummed = new List<SummedEvaluation>();
                        List<SummedEvaluation> UpdateListSummed = new List<SummedEvaluation>();
                        for (int j = 0; j < listInsertOrUpdateSummed.Count; j++)
                        {
                            SummedEvaluation summedEvaluationObj = listInsertOrUpdateSummed[j];
                            int pupilID = summedEvaluationObj.PupilID;
                            SummedEvaluation checklist = null;

                            //kiem tra hoc sinh da co trong table summedEvaluation chua
                            checklist = listSummedEvaluation.Where(p => p.PupilID == pupilID).FirstOrDefault();
                            if (checklist != null)
                            {
                                checklist.PeriodicEndingMark = summedEvaluationObj.PeriodicEndingMark;
                                checklist.EndingEvaluation = summedEvaluationObj.EndingEvaluation;
                                checklist.EndingComments = summedEvaluationObj.EndingComments;
                                checklist.UpdateTime = DateTime.Now;
                                UpdateListSummed.Add(checklist);
                            }
                            else
                            {
                                //insert
                                summedEvaluationObj.SummedEvaluationID = SummedEvaluationBusiness.GetNextSeq<long>();
                                summedEvaluationObj.CreateTime = DateTime.Now;
                                summedEvaluationObj.UpdateTime = null;
                                InsertListSummed.Add(summedEvaluationObj);
                            }
                        }
                        if (InsertListSummed != null && InsertListSummed.Count > 0)
                        {
                            for (int i = 0; i < InsertListSummed.Count; i++)
                            {
                                SummedEvaluation se = InsertListSummed[i];

                                if (!String.IsNullOrEmpty(se.EndingEvaluation)
                                    || !String.IsNullOrEmpty(se.EndingComments)
                                    || se.PeriodicEndingMark.HasValue)
                                {

                                    SummedEvaluationBusiness.Insert(se);

                                    //them action audit
                                    //Kiem tra da co action audit hay chua
                                    EvaluationCommentActionAuditBO aab = lstActionAuditBO.Where(o => o.ObjectId == se.PupilID).FirstOrDefault();
                                    if (aab == null)
                                    {
                                        aab = new EvaluationCommentActionAuditBO(EvaluationCommentActionAuditBO.EVALUATION_COMMENT_AUDIT_ACTION_UPDATE);
                                        lstActionAuditBO.Add(aab);
                                        aab.AcademicYear = aca.DisplayTitle;
                                        aab.ClassName = cp.DisplayName;
                                        aab.Month = MonthID;
                                        aab.ObjectId = se.PupilID;
                                        PupilOfClass poc = lstPoc.Where(o => o.PupilID == se.PupilID).FirstOrDefault();
                                        aab.PupilCode = poc.PupilProfile.PupilCode;
                                        aab.PupilName = poc.PupilProfile.FullName;
                                        aab.Semester = semesterID;
                                        SubjectCat sc = lstSuject.Where(o => o.SubjectCatID == se.EvaluationCriteriaID).FirstOrDefault();
                                        if (sc != null)
                                        {
                                            aab.SubjectName = sc.DisplayName;
                                        }
                                        EvaluationCriteria ec = lstEvaluationCriteria.FirstOrDefault(o => o.EvaluationCriteriaID == se.EvaluationCriteriaID);
                                        if (ec != null)
                                        {
                                            aab.EvaluationCriteriaName = ec.CriteriaName;
                                        }
                                        aab.EvaluationID = evaluationID;

                                    }
                                    if (se.PeriodicEndingMark != null)
                                    {
                                        aab.NewObjectValue.Add(CRITERIA_KTDKCK, se.PeriodicEndingMark.Value.ToString());
                                        aab.OldObjectValue.Add(CRITERIA_KTDKCK, string.Empty);
                                    }

                                    if (!String.IsNullOrEmpty(se.EndingEvaluation))
                                    {
                                        aab.NewObjectValue.Add(CRITERIA_DG, se.EndingEvaluation);
                                        aab.OldObjectValue.Add(CRITERIA_DG, string.Empty);
                                    }

                                    if (!string.IsNullOrEmpty(se.EndingComments))
                                    {
                                        aab.NewObjectValue.Add(CRITERIA_NXCK, se.EndingComments);
                                        aab.OldObjectValue.Add(CRITERIA_NXCK, string.Empty);

                                    }

                                }
                            }
                        }
                        if (UpdateListSummed != null && UpdateListSummed.Count > 0)
                        {
                            for (int j = 0; j < UpdateListSummed.Count; j++)
                            {
                                SummedEvaluation se = UpdateListSummed[j];

                                if (String.IsNullOrEmpty(se.EndingEvaluation)) se.EndingEvaluation = null;
                                if (String.IsNullOrEmpty(se.EndingComments)) se.EndingComments = null;

                                if (se.EndingEvaluation != this.context.Entry<SummedEvaluation>(se).OriginalValues.GetValue<string>("EndingEvaluation")
                                    || se.PeriodicEndingMark != this.context.Entry<SummedEvaluation>(se).OriginalValues.GetValue<Nullable<int>>("PeriodicEndingMark")
                                    || se.EndingComments != this.context.Entry<SummedEvaluation>(se).OriginalValues.GetValue<string>("EndingComments"))
                                {
                                    SummedEvaluationBusiness.Update(se);

                                    EvaluationCommentActionAuditBO aab = lstActionAuditBO.Where(o => o.ObjectId == se.PupilID).FirstOrDefault();
                                    if (aab == null)
                                    {
                                        aab = new EvaluationCommentActionAuditBO(EvaluationCommentActionAuditBO.EVALUATION_COMMENT_AUDIT_ACTION_UPDATE);
                                        lstActionAuditBO.Add(aab);
                                        aab.AcademicYear = aca.DisplayTitle;
                                        aab.ClassName = cp.DisplayName;
                                        aab.Month = MonthID;
                                        aab.ObjectId = se.PupilID;
                                        PupilOfClass poc = lstPoc.Where(o => o.PupilID == se.PupilID).FirstOrDefault();
                                        aab.PupilCode = poc.PupilProfile.PupilCode;
                                        aab.PupilName = poc.PupilProfile.FullName;
                                        aab.Semester = semesterID;
                                        SubjectCat sc = lstSuject.Where(o => o.SubjectCatID == se.EvaluationCriteriaID).FirstOrDefault();
                                        if (sc != null)
                                        {
                                            aab.SubjectName = sc.DisplayName;
                                        }
                                        EvaluationCriteria ec = lstEvaluationCriteria.FirstOrDefault(o => o.EvaluationCriteriaID == se.EvaluationCriteriaID);
                                        if (ec != null)
                                        {
                                            aab.EvaluationCriteriaName = ec.CriteriaName;
                                        }
                                        aab.EvaluationID = evaluationID;

                                    }
                                    if (se.PeriodicEndingMark != this.context.Entry<SummedEvaluation>(se).OriginalValues.GetValue<Nullable<int>>("PeriodicEndingMark"))
                                    {
                                        aab.NewObjectValue.Add(CRITERIA_KTDKCK, se.PeriodicEndingMark.HasValue ? se.PeriodicEndingMark.Value.ToString() : String.Empty);
                                        aab.OldObjectValue.Add(CRITERIA_KTDKCK, this.context.Entry<SummedEvaluation>(se).OriginalValues.GetValue<Nullable<int>>("PeriodicEndingMark").HasValue
                                            ? this.context.Entry<SummedEvaluation>(se).OriginalValues.GetValue<Nullable<int>>("PeriodicEndingMark").Value.ToString() : string.Empty);
                                    }

                                    if (se.EndingEvaluation
                                        != this.context.Entry<SummedEvaluation>(se).OriginalValues.GetValue<string>("EndingEvaluation"))
                                    {
                                        aab.NewObjectValue.Add(CRITERIA_DG, se.EndingEvaluation);
                                        aab.OldObjectValue.Add(CRITERIA_DG, this.context.Entry<SummedEvaluation>(se).OriginalValues.GetValue<string>("EndingEvaluation"));
                                    }

                                    if (se.EndingComments
                                        != this.context.Entry<SummedEvaluation>(se).OriginalValues.GetValue<string>("EndingComments"))
                                    {
                                        aab.NewObjectValue.Add(CRITERIA_NXCK, se.EndingComments);
                                        aab.OldObjectValue.Add(CRITERIA_NXCK, this.context.Entry<SummedEvaluation>(se).OriginalValues.GetValue<string>("EndingComments"));

                                    }

                                }
                            }
                        }
                        SummedEvaluationBusiness.Save();
                    }

                    #endregion

                    #region Thêm dữ liệu Đánh giá khi chọn Tab Năng lực hoặc phẩm chất

                    SummedEndingEvaluation checkInsummedEnding = null;
                    if (listInsertOrUpdateSummedEnding != null && listInsertOrUpdateSummedEnding.Count > 0)
                    {
                        List<SummedEndingEvaluation> listSummedEnding = SummedEndingEvaluationBusiness.All.Where(p => p.LastDigitSchoolID == partitionNumber

                             && p.ClassID == classID
                             && p.AcademicYearID == academicYearID
                             && p.SemesterID == semesterID
                             && p.EvaluationID == evaluationID).ToList();
                        List<SummedEndingEvaluation> InsertListSummedEnding = new List<SummedEndingEvaluation>();
                        List<SummedEndingEvaluation> UpdateListSummedEnding = new List<SummedEndingEvaluation>();
                        for (int k = 0; k < listInsertOrUpdateSummedEnding.Count; k++)
                        {
                            SummedEndingEvaluation summedEndingObj = listInsertOrUpdateSummedEnding[k];
                            if (summedEndingObj != null)
                            {
                                long pupilID = summedEndingObj.PupilID;
                                checkInsummedEnding = listSummedEnding.Where(p => p.PupilID == pupilID).FirstOrDefault();
                                if (checkInsummedEnding != null)
                                {
                                    //update
                                    checkInsummedEnding.EndingEvaluation = summedEndingObj.EndingEvaluation;
                                    checkInsummedEnding.UpdateTime = DateTime.Now;
                                    UpdateListSummedEnding.Add(checkInsummedEnding);
                                }
                                else
                                {
                                    //insert
                                    summedEndingObj.SummedEndingEvaluationID = SummedEndingEvaluationBusiness.GetNextSeq<long>();
                                    summedEndingObj.CreateTime = DateTime.Now;
                                    summedEndingObj.UpdateTime = null;
                                    InsertListSummedEnding.Add(summedEndingObj);
                                }
                            }
                        }

                        if (InsertListSummedEnding != null && InsertListSummedEnding.Count > 0)
                        {
                            for (int i = 0; i < InsertListSummedEnding.Count; i++)
                            {
                                SummedEndingEvaluation see = InsertListSummedEnding[i];

                                if (!string.IsNullOrEmpty(see.EndingEvaluation))
                                {
                                    SummedEndingEvaluationBusiness.Insert(see);

                                    //them action audit
                                    //Kiem tra da co action audit hay chua
                                    EvaluationCommentActionAuditBO aab = lstActionAuditBO.Where(o => o.ObjectId == see.PupilID).FirstOrDefault();
                                    if (aab == null)
                                    {
                                        aab = new EvaluationCommentActionAuditBO(EvaluationCommentActionAuditBO.EVALUATION_COMMENT_AUDIT_ACTION_UPDATE);
                                        lstActionAuditBO.Add(aab);
                                        aab.AcademicYear = aca.DisplayTitle;
                                        aab.ClassName = cp.DisplayName;
                                        aab.Month = MonthID;
                                        aab.ObjectId = see.PupilID;
                                        PupilOfClass poc = lstPoc.Where(o => o.PupilID == see.PupilID).FirstOrDefault();
                                        aab.PupilCode = poc.PupilProfile.PupilCode;
                                        aab.PupilName = poc.PupilProfile.FullName;
                                        aab.Semester = semesterID;
                                        aab.SubjectName = string.Empty;
                                        aab.EvaluationCriteriaName = string.Empty;
                                        aab.EvaluationID = evaluationID;

                                    }
                                    if (!String.IsNullOrEmpty(see.EndingEvaluation))
                                    {
                                        aab.NewObjectValue.Add(CRITERIA_DG, see.EndingEvaluation);
                                        aab.OldObjectValue.Add(CRITERIA_DG, string.Empty);
                                    }
                                }
                            }
                        }

                        if (UpdateListSummedEnding != null && UpdateListSummedEnding.Count > 0)
                        {
                            for (int j = 0; j < UpdateListSummedEnding.Count; j++)
                            {
                                SummedEndingEvaluation see = UpdateListSummedEnding[j];
                                if (string.IsNullOrEmpty(see.EndingEvaluation)) see.EndingEvaluation = null;

                                if (see.EndingEvaluation != this.context.Entry<SummedEndingEvaluation>(see).OriginalValues.GetValue<string>("EndingEvaluation"))
                                {
                                    SummedEndingEvaluationBusiness.Update(see);
                                    EvaluationCommentActionAuditBO aab = lstActionAuditBO.Where(o => o.ObjectId == see.PupilID).FirstOrDefault();
                                    if (aab == null)
                                    {
                                        aab = new EvaluationCommentActionAuditBO(EvaluationCommentActionAuditBO.EVALUATION_COMMENT_AUDIT_ACTION_UPDATE);
                                        lstActionAuditBO.Add(aab);
                                        aab.AcademicYear = aca.DisplayTitle;
                                        aab.ClassName = cp.DisplayName;
                                        aab.Month = MonthID;
                                        aab.ObjectId = see.PupilID;
                                        PupilOfClass poc = lstPoc.Where(o => o.PupilID == see.PupilID).FirstOrDefault();
                                        aab.PupilCode = poc.PupilProfile.PupilCode;
                                        aab.PupilName = poc.PupilProfile.FullName;
                                        aab.Semester = semesterID;
                                        aab.SubjectName = string.Empty;
                                        aab.EvaluationCriteriaName = string.Empty;
                                        aab.EvaluationID = evaluationID;

                                    }
                                    if (see.EndingEvaluation != this.context.Entry<SummedEndingEvaluation>(see).OriginalValues.GetValue<string>("EndingEvaluation"))
                                    {
                                        aab.NewObjectValue.Add(CRITERIA_DG, see.EndingEvaluation);
                                        aab.OldObjectValue.Add(CRITERIA_DG, this.context.Entry<SummedEndingEvaluation>(see).OriginalValues.GetValue<string>("EndingEvaluation"));
                                    }
                                }

                            }
                        }
                        SummedEndingEvaluationBusiness.Save();
                    }

                    #endregion


                    scope.Complete();
                }
            }
            catch (Exception ex)
            {
                string paramList = string.Format("MonthID={0}, schoolID={1}, academicYearID={2}, classID={3}, educationLevelID={4}, semesterID={5}, evaluationID={6}, evaluationCriteriaID={7}, typeOfTeacher={8}", MonthID, schoolID, academicYearID, classID, educationLevelID, semesterID, evaluationID, evaluationCriteriaID, typeOfTeacher);
                LogExtensions.ErrorExt(logger, DateTime.Now, "InsertOrUpdateEvaluationComments", paramList, ex);
            }
            finally
            {
                context.Configuration.AutoDetectChangesEnabled = true;
                context.Configuration.ValidateOnSaveEnabled = true;
            }
            return lstActionAuditBO;
        }

        /// <summary>
        /// Insert import (khong phan biet thang nhan xet)
        /// </summary>
        /// <param name="schoolID"></param>
        /// <param name="academicYearID"></param>
        /// <param name="classID"></param>
        /// <param name="educationLevelID"></param>
        /// <param name="semesterID"></param>
        /// <param name="evaluationID">Id cac tieu chi (tab dang chon)</param>
        /// <param name="evaluationCriteriaID">ID các mặt đánh giá (nếu là tab 1 thì là ID môn học) </param>
        /// <param name="typeOfTeacher">1:GVBM, 2: GVCN</param>
        /// <param name="listInsertOrUpdate"></param>
        public void InsertOrUpdateEvaluationCommentsImport(int monthID, int schoolID, int academicYearID, int classID, int educationLevelID, int semesterID, int evaluationID, int evaluationCriteriaID, int typeOfTeacher, List<EvaluationComments> listInsertOrUpdate, List<SummedEvaluation> listInsertOrUpdateSummed, List<SummedEndingEvaluation> listInsertOrUpdateSummedEnding)
        {
            try
            {
                SetAutoDetectChangesEnabled(false);
                int partitionNumber = schoolID % GlobalConstants.PARTITION;
                string strLockMark = this.CheckLockMark(classID, schoolID, academicYearID);
                strLockMark = strLockMark.Replace("T10", "TH10");

                #region Them nhan xet cho thang

                if (listInsertOrUpdate != null && listInsertOrUpdate.Count > 0)
                {
                    List<EvaluationComments> evaluationCommentsList = EvaluationCommentsBusiness.All
                        .Where(p => p.LastDigitSchoolID == partitionNumber

                                    && p.ClassID == classID
                                    && p.AcademicYearID == academicYearID
                                    && p.EducationLevelID == educationLevelID
                                    && p.SemesterID == semesterID
                                    && p.EvaluationID == evaluationID
                            //&& p.EvaluationCriteriaID == evaluationCriteriaID
                                    && p.TypeOfTeacher == typeOfTeacher).ToList();

                    List<EvaluationComments> InsertList = new List<EvaluationComments>();
                    List<EvaluationComments> UpdateList = new List<EvaluationComments>();
                    EvaluationComments checkInlist = null;
                    for (int i = 0; i < listInsertOrUpdate.Count; i++)
                    {
                        EvaluationComments evaluationCommentsObj = listInsertOrUpdate[i];
                        long pupilID = evaluationCommentsObj.PupilID;
                        string strMonth = string.Empty;
                        //kiem tra pupilID da ton tai trong list da select len chua
                        checkInlist = evaluationCommentsList.Where(p => p.PupilID == pupilID).FirstOrDefault();
                        if (checkInlist == null)
                        {
                            #region insert thong tin moi

                            if (semesterID == GlobalConstants.SEMESTER_OF_YEAR_FIRST)
                            {
                                if (!strLockMark.Contains("HK1"))
                                {
                                    if (strLockMark.Contains("T1")) //khoa thang 1
                                    {
                                        evaluationCommentsObj.CommentsM1M6 = null;
                                    }
                                    if (strLockMark.Contains("T2")) //khoa thang 2
                                    {
                                        evaluationCommentsObj.CommentsM2M7 = null;
                                    }
                                    if (strLockMark.Contains("T3")) //khoa thang 3
                                    {
                                        evaluationCommentsObj.CommentsM3M8 = null;
                                    }
                                    if (strLockMark.Contains("T4")) //khoa thang 4
                                    {
                                        evaluationCommentsObj.CommentsM4M9 = null;
                                    }
                                    if (strLockMark.Contains("T5")) //khoa thang 5
                                    {
                                        evaluationCommentsObj.CommentsM5M10 = null;
                                    }
                                    InsertList.Add(evaluationCommentsObj);
                                }
                            }
                            else
                            {
                                if (!strLockMark.Contains("HK2"))
                                {
                                    if (strLockMark.Contains("T6")) //khoa thang 1
                                    {
                                        evaluationCommentsObj.CommentsM1M6 = null;
                                    }
                                    if (strLockMark.Contains("T7")) //khoa thang 2
                                    {
                                        evaluationCommentsObj.CommentsM2M7 = null;
                                    }
                                    if (strLockMark.Contains("T8")) //khoa thang 3
                                    {
                                        evaluationCommentsObj.CommentsM3M8 = null;
                                    }
                                    if (strLockMark.Contains("T9")) //khoa thang 4
                                    {
                                        evaluationCommentsObj.CommentsM4M9 = null;
                                    }
                                    if (strLockMark.Contains("TH10")) //khoa thang 5
                                    {
                                        evaluationCommentsObj.CommentsM5M10 = null;
                                    }

                                    InsertList.Add(evaluationCommentsObj);
                                }
                            }

                            #endregion
                        }
                        else
                        {
                            #region update thong tin moi

                            if (semesterID == GlobalConstants.SEMESTER_OF_YEAR_FIRST)
                            {
                                if (!strLockMark.Contains("HK1"))
                                {
                                    if (!strLockMark.Contains("T1")) // k khoa thang 1
                                    {
                                        checkInlist.CommentsM1M6 = evaluationCommentsObj.CommentsM1M6;
                                    }
                                    if (!strLockMark.Contains("T2")) //k khoa thang 2
                                    {
                                        checkInlist.CommentsM2M7 = evaluationCommentsObj.CommentsM2M7;
                                    }
                                    if (!strLockMark.Contains("T3")) //k khoa thang 3
                                    {
                                        checkInlist.CommentsM3M8 = evaluationCommentsObj.CommentsM3M8;
                                    }
                                    if (!strLockMark.Contains("T4")) //k khoa thang 4
                                    {
                                        checkInlist.CommentsM4M9 = evaluationCommentsObj.CommentsM4M9;
                                    }
                                    if (!strLockMark.Contains("T5")) //k khoa thang 5
                                    {
                                        checkInlist.CommentsM5M10 = evaluationCommentsObj.CommentsM5M10;
                                    }
                                    UpdateList.Add(checkInlist);
                                }
                            }
                            else
                            {
                                if (!strLockMark.Contains("HK2"))
                                {
                                    if (!strLockMark.Contains("T6")) // k khoa thang 1
                                    {
                                        checkInlist.CommentsM1M6 = evaluationCommentsObj.CommentsM1M6;
                                    }
                                    if (!strLockMark.Contains("T7")) //k khoa thang 2
                                    {
                                        checkInlist.CommentsM2M7 = evaluationCommentsObj.CommentsM2M7;
                                    }
                                    if (!strLockMark.Contains("T8")) //k khoa thang 3
                                    {
                                        checkInlist.CommentsM3M8 = evaluationCommentsObj.CommentsM3M8;
                                    }
                                    if (!strLockMark.Contains("T9")) //k khoa thang 4
                                    {
                                        checkInlist.CommentsM4M9 = evaluationCommentsObj.CommentsM4M9;
                                    }
                                    if (!strLockMark.Contains("TH10")) //k khoa thang 5
                                    {
                                        checkInlist.CommentsM5M10 = evaluationCommentsObj.CommentsM5M10;
                                    }
                                    UpdateList.Add(checkInlist);
                                }
                            }

                            #endregion
                        }
                    }

                    if (InsertList != null && InsertList.Count > 0)
                    {
                        foreach (EvaluationComments insertObj in InsertList)
                        {
                            insertObj.EvaluationCommentsID = EvaluationCommentsBusiness.GetNextSeq<long>();
                            insertObj.CreateTime = DateTime.Now;
                            insertObj.UpdateTime = null;
                            EvaluationCommentsBusiness.Insert(insertObj);
                        }
                    }

                    if (UpdateList != null && UpdateList.Count > 0)
                    {
                        foreach (EvaluationComments updateObj in UpdateList)
                        {
                            updateObj.UpdateTime = DateTime.Now;
                            EvaluationCommentsBusiness.Update(updateObj);
                        }
                    }

                    EvaluationCommentsBusiness.Save();
                }

                #endregion

                #region Them du lieu nhận xét cuối kỳ vào summedEvaluation (chỉ insert tab CLGD)

                if (evaluationID == GlobalConstants.TAB_CAPACTIES_BOOKMARK_PRIMARY ||
                    evaluationID == GlobalConstants.TAB_QUALITIES_BOOKMARK_PRIMARY)
                {
                    if (monthID == GlobalConstants.MonthID_11)
                    {
                        if (listInsertOrUpdateSummed != null && listInsertOrUpdateSummed.Count > 0)
                        {

                            List<SummedEvaluation> listSummedEvaluation =
                                SummedEvaluationBusiness.All.Where(p => p.LastDigitSchoolID == partitionNumber

                                                                        && p.ClassID == classID
                                                                        && p.AcademicYearID == academicYearID
                                                                        && p.EducationLevelID == educationLevelID
                                                                        && p.SemesterID == semesterID
                                                                        && p.EvaluationID == evaluationID).ToList();
                            List<SummedEvaluation> InsertListSummed = new List<SummedEvaluation>();
                            List<SummedEvaluation> UpdateListSummed = new List<SummedEvaluation>();
                            for (int j = 0; j < listInsertOrUpdateSummed.Count; j++)
                            {
                                SummedEvaluation summedEvaluationObj = listInsertOrUpdateSummed[j];
                                int pupilID = summedEvaluationObj.PupilID;
                                int evaluationCriteria = summedEvaluationObj.EvaluationCriteriaID;
                                SummedEvaluation checklist =
                                    listSummedEvaluation.Where(
                                        p =>
                                            p.PupilID == pupilID &&
                                            (p.EvaluationCriteriaID != 0 &&
                                             p.EvaluationCriteriaID == evaluationCriteria)).FirstOrDefault();
                                //kiem tra hoc sinh da co trong table summedEvaluation chua                                 
                                if (checklist != null)
                                {
                                    // khong chua thong tin khoa diem CK1,CK2
                                    if (semesterID == GlobalConstants.SEMESTER_OF_YEAR_FIRST)
                                    {
                                        if (!strLockMark.Contains("HK1"))
                                        {
                                            if (!strLockMark.Contains("CK1"))
                                            {
                                                checklist.PeriodicEndingMark =
                                                    summedEvaluationObj.PeriodicEndingMark;
                                                checklist.EndingEvaluation = summedEvaluationObj.EndingEvaluation;
                                                checklist.EndingComments = summedEvaluationObj.EndingComments;
                                                UpdateListSummed.Add(checklist);
                                            }
                                        }
                                    }
                                    else
                                    {
                                        if (!strLockMark.Contains("HK2"))
                                        {
                                            if (!strLockMark.Contains("CK2"))
                                            {
                                                checklist.PeriodicEndingMark =
                                                    summedEvaluationObj.PeriodicEndingMark;
                                                checklist.EndingEvaluation = summedEvaluationObj.EndingEvaluation;
                                                checklist.EndingComments = summedEvaluationObj.EndingComments;
                                                UpdateListSummed.Add(checklist);
                                            }
                                        }
                                    }

                                }
                                else
                                {
                                    //insert
                                    summedEvaluationObj.SummedEvaluationID =
                                        SummedEvaluationBusiness.GetNextSeq<long>();
                                    if (semesterID == GlobalConstants.SEMESTER_OF_YEAR_FIRST)
                                    {
                                        if (!strLockMark.Contains("HK1"))
                                        {
                                            if (strLockMark.Contains("CK1"))
                                            {
                                                // neu chua thong tin khoa diem thi khong insert cac cot nay
                                                summedEvaluationObj.PeriodicEndingMark = null;
                                                summedEvaluationObj.EndingEvaluation = null;
                                                summedEvaluationObj.EndingComments = null;
                                                InsertListSummed.Add(summedEvaluationObj);
                                            }
                                            else
                                            {
                                                InsertListSummed.Add(summedEvaluationObj);
                                            }
                                        }
                                    }
                                    else
                                    {
                                        if (!strLockMark.Contains("HK2"))
                                        {
                                            if (strLockMark.Contains("CK2"))
                                            {
                                                // neu chua thong tin khoa diem thi khong insert cac cot nay
                                                summedEvaluationObj.PeriodicEndingMark = null;
                                                summedEvaluationObj.EndingEvaluation = null;
                                                summedEvaluationObj.EndingComments = null;
                                                InsertListSummed.Add(summedEvaluationObj);
                                            }
                                            else
                                            {
                                                InsertListSummed.Add(summedEvaluationObj);
                                            }
                                        }
                                    }
                                }
                            }


                            //insert or update in database
                            if (InsertListSummed != null && InsertListSummed.Count > 0)
                            {
                                for (int i = 0; i < InsertListSummed.Count; i++)
                                {

                                    SummedEvaluationBusiness.Insert(InsertListSummed[i]);
                                }
                            }
                            if (UpdateListSummed != null && UpdateListSummed.Count > 0)
                            {
                                for (int j = 0; j < UpdateListSummed.Count; j++)
                                {
                                    SummedEvaluationBusiness.Update(UpdateListSummed[j]);
                                }
                            }
                            SummedEvaluationBusiness.Save();
                        }
                    }
                }

                if (evaluationID == GlobalConstants.TAB_EDUCATION_BOOKMARK_PRIMARY)
                {
                    if (listInsertOrUpdateSummed != null && listInsertOrUpdateSummed.Count > 0)
                    {
                        List<SummedEvaluation> listSummedEvaluation =
                            SummedEvaluationBusiness.All.Where(p => p.LastDigitSchoolID == partitionNumber

                                                                    && p.ClassID == classID
                                                                    && p.AcademicYearID == academicYearID
                                                                    && p.EducationLevelID == educationLevelID
                                                                    && p.SemesterID == semesterID
                                                                    && p.EvaluationID == evaluationID
                                                                    &&
                                                                    p.EvaluationCriteriaID == evaluationCriteriaID)
                                .ToList();
                        List<SummedEvaluation> InsertListSummed = new List<SummedEvaluation>();
                        List<SummedEvaluation> UpdateListSummed = new List<SummedEvaluation>();
                        for (int j = 0; j < listInsertOrUpdateSummed.Count; j++)
                        {
                            SummedEvaluation summedEvaluationObj = listInsertOrUpdateSummed[j];
                            int pupilID = summedEvaluationObj.PupilID;
                            SummedEvaluation checklist = null;

                            //kiem tra hoc sinh da co trong table summedEvaluation chua
                            checklist = listSummedEvaluation.Where(p => p.PupilID == pupilID).FirstOrDefault();
                            if (checklist != null)
                            {
                                // khong chua thong tin khoa diem CK1,CK2
                                if (semesterID == GlobalConstants.SEMESTER_OF_YEAR_FIRST)
                                {
                                    if (!strLockMark.Contains("HK1"))
                                    {
                                        if (!strLockMark.Contains("CK1"))
                                        {
                                            checklist.PeriodicEndingMark = summedEvaluationObj.PeriodicEndingMark;
                                            checklist.EndingEvaluation = summedEvaluationObj.EndingEvaluation;
                                            checklist.EndingComments = summedEvaluationObj.EndingComments;
                                            UpdateListSummed.Add(checklist);
                                        }
                                    }
                                }
                                else
                                {
                                    if (!strLockMark.Contains("HK2"))
                                    {
                                        if (!strLockMark.Contains("CK2"))
                                        {
                                            checklist.PeriodicEndingMark = summedEvaluationObj.PeriodicEndingMark;
                                            checklist.EndingEvaluation = summedEvaluationObj.EndingEvaluation;
                                            checklist.EndingComments = summedEvaluationObj.EndingComments;
                                            UpdateListSummed.Add(checklist);
                                        }
                                    }
                                }

                            }
                            else
                            {
                                //insert
                                summedEvaluationObj.SummedEvaluationID = SummedEvaluationBusiness.GetNextSeq<long>();
                                if (semesterID == GlobalConstants.SEMESTER_OF_YEAR_FIRST)
                                {
                                    if (!strLockMark.Contains("HK1"))
                                    {
                                        if (strLockMark.Contains("CK1"))
                                        {
                                            // neu chua thong tin khoa diem thi khong insert cac cot nay
                                            summedEvaluationObj.PeriodicEndingMark = null;
                                            summedEvaluationObj.EndingEvaluation = null;
                                            summedEvaluationObj.EndingComments = null;
                                            InsertListSummed.Add(summedEvaluationObj);
                                        }
                                        else
                                        {
                                            InsertListSummed.Add(summedEvaluationObj);
                                        }
                                    }
                                }
                                else
                                {
                                    if (!strLockMark.Contains("HK2"))
                                    {
                                        if (strLockMark.Contains("CK2"))
                                        {
                                            // neu chua thong tin khoa diem thi khong insert cac cot nay
                                            summedEvaluationObj.PeriodicEndingMark = null;
                                            summedEvaluationObj.EndingEvaluation = null;
                                            summedEvaluationObj.EndingComments = null;
                                            InsertListSummed.Add(summedEvaluationObj);
                                        }
                                        else
                                        {
                                            InsertListSummed.Add(summedEvaluationObj);
                                        }
                                    }
                                }


                            }
                        }
                        if (InsertListSummed != null && InsertListSummed.Count > 0)
                        {
                            for (int i = 0; i < InsertListSummed.Count; i++)
                            {

                                SummedEvaluationBusiness.Insert(InsertListSummed[i]);
                            }
                        }
                        if (UpdateListSummed != null && UpdateListSummed.Count > 0)
                        {
                            for (int j = 0; j < UpdateListSummed.Count; j++)
                            {
                                SummedEvaluationBusiness.Update(UpdateListSummed[j]);
                            }
                        }
                        SummedEvaluationBusiness.Save();
                    }
                }

                #endregion

                #region Thêm dữ liệu Đánh giá khi chọn Tab Năng lực hoặc phẩm chất

                SummedEndingEvaluation checkInsummedEnding = null;
                if (listInsertOrUpdateSummedEnding != null && listInsertOrUpdateSummedEnding.Count > 0)
                {
                    List<SummedEndingEvaluation> listSummedEnding =
                        SummedEndingEvaluationBusiness.All.Where(p => p.LastDigitSchoolID == partitionNumber

                                                                      && p.ClassID == classID
                                                                      && p.AcademicYearID == academicYearID
                                                                      && p.SemesterID == semesterID
                                                                      && p.EvaluationID == evaluationID).ToList();
                    List<SummedEndingEvaluation> InsertListSummedEnding = new List<SummedEndingEvaluation>();
                    List<SummedEndingEvaluation> UpdateListSummedEnding = new List<SummedEndingEvaluation>();
                    for (int k = 0; k < listInsertOrUpdateSummedEnding.Count; k++)
                    {
                        SummedEndingEvaluation summedEndingObj = listInsertOrUpdateSummedEnding[k];
                        if (summedEndingObj != null)
                        {
                            long pupilID = summedEndingObj.PupilID;
                            checkInsummedEnding = listSummedEnding.Where(p => p.PupilID == pupilID).FirstOrDefault();
                            if (checkInsummedEnding != null)
                            {
                                //update
                                if (semesterID == GlobalConstants.SEMESTER_OF_YEAR_FIRST)
                                {
                                    if (!strLockMark.Contains("HK1"))
                                    {
                                        if (!strLockMark.Contains("CK1"))
                                        {
                                            checkInsummedEnding.EndingEvaluation = summedEndingObj.EndingEvaluation;
                                            UpdateListSummedEnding.Add(checkInsummedEnding);
                                        }
                                    }
                                }
                                else
                                {
                                    if (!strLockMark.Contains("HK2"))
                                    {
                                        if (!strLockMark.Contains("CK2"))
                                        {
                                            checkInsummedEnding.EndingEvaluation = summedEndingObj.EndingEvaluation;
                                            UpdateListSummedEnding.Add(checkInsummedEnding);
                                        }
                                    }
                                }
                            }
                            else
                            {
                                //insert
                                summedEndingObj.SummedEndingEvaluationID =
                                    SummedEndingEvaluationBusiness.GetNextSeq<long>();
                                if (semesterID == GlobalConstants.SEMESTER_OF_YEAR_FIRST)
                                {
                                    if (!strLockMark.Contains("HK1"))
                                    {
                                        if (strLockMark.Contains("CK1"))
                                        {
                                            summedEndingObj.EndingEvaluation = null;
                                            InsertListSummedEnding.Add(summedEndingObj);
                                        }
                                        else
                                        {
                                            InsertListSummedEnding.Add(summedEndingObj);
                                        }
                                    }
                                }
                                else
                                {
                                    if (!strLockMark.Contains("HK2"))
                                    {
                                        if (strLockMark.Contains("CK2"))
                                        {
                                            summedEndingObj.EndingEvaluation = null;
                                            InsertListSummedEnding.Add(summedEndingObj);
                                        }
                                        else
                                        {
                                            InsertListSummedEnding.Add(summedEndingObj);
                                        }
                                    }
                                }
                            }
                        }
                    }

                    if (InsertListSummedEnding != null && InsertListSummedEnding.Count > 0)
                    {
                        for (int i = 0; i < InsertListSummedEnding.Count; i++)
                        {
                            SummedEndingEvaluationBusiness.Insert(InsertListSummedEnding[i]);
                        }
                    }

                    if (UpdateListSummedEnding != null && UpdateListSummedEnding.Count > 0)
                    {
                        for (int j = 0; j < UpdateListSummedEnding.Count; j++)
                        {
                            SummedEndingEvaluationBusiness.Update(UpdateListSummedEnding[j]);
                        }
                    }
                    SummedEndingEvaluationBusiness.Save();
                }

                #endregion



            }
            finally
            {
                SetAutoDetectChangesEnabled(true);

            }
        }

        /// <summary>
        /// Insert hoac update nhan xet cap 1(Nếu insert thì truyền noteinsert vào, update không cần)
        /// </summary>
        /// <param name="MonthID"></param>
        /// <param name="insertOrUpdateObj"></param>
        public void InsertOrUpdateEvaluationCommentsByPupilID(int MonthID, EvaluationComments insertOrUpdateObj, ref ActionAuditDataBO objAc)
        {
            try
            {
                SetAutoDetectChangesEnabled(false);
                long pupilID = 0;
                int typeOfTeacher = 0;
                int evaluationCriteriaID = 0;
                int evaluationID = 0;
                int semesterID = 0;
                int educationLevelID = 0;
                int schoolID = 0;
                int academicYearID = 0;
                int classID = 0;
                int partitionNumber = 0;
                if (insertOrUpdateObj != null)
                {
                    pupilID = insertOrUpdateObj.PupilID;
                    typeOfTeacher = insertOrUpdateObj.TypeOfTeacher;
                    evaluationCriteriaID = insertOrUpdateObj.EvaluationCriteriaID;
                    evaluationID = insertOrUpdateObj.EvaluationID;
                    semesterID = insertOrUpdateObj.SemesterID;
                    educationLevelID = insertOrUpdateObj.EducationLevelID;
                    schoolID = insertOrUpdateObj.SchoolID;
                    academicYearID = insertOrUpdateObj.AcademicYearID;
                    classID = insertOrUpdateObj.ClassID;
                    partitionNumber = UtilsBusiness.GetPartionId(schoolID);
                }
                //
                AcademicYear objAcademicYear = AcademicYearBusiness.Find(academicYearID);
                if (objAcademicYear == null)
                {
                    return;
                }
                //Nau la du lieu da chuyen vao lich su thi thuc hien cap nhat vao bang du lieu lich su

                #region Tao du lieu ghi log

                StringBuilder objectIDStrtmp = new StringBuilder();
                StringBuilder descriptionStrtmp = new StringBuilder();
                StringBuilder paramsStrtmp = new StringBuilder();
                StringBuilder oldObjectStrtmp = new StringBuilder();
                StringBuilder newObjectStrtmp = new StringBuilder();
                StringBuilder userFuntionsStrtmp = new StringBuilder();
                StringBuilder userActionsStrtmp = new StringBuilder();
                StringBuilder userDescriptionsStrtmp = new StringBuilder();
                bool isChange = false;

                SubjectCat objSC = SubjectCatBusiness.Find(evaluationCriteriaID);
                PupilProfile objPF = PupilProfileBusiness.Find(pupilID);
                ClassProfile objCP = ClassProfileBusiness.Find(classID);
                string evaluationName = evaluationID == GlobalConstants.TAB_EDUCATION_BOOKMARK_PRIMARY
                    ? "Môn học & HĐGD"
                    : evaluationID == GlobalConstants.TAB_CAPACTIES_BOOKMARK_PRIMARY ? "Năng lực" : "Phẩm chất";

                #endregion


                bool isMovedHistory = UtilsBusiness.IsMoveHistory(objAcademicYear);
                if (isMovedHistory)
                {
                    EvaluationCommentsHistoryBusiness.InsertOrUpdateEvaluationCommentsByPupilID(MonthID,
                        insertOrUpdateObj, ref objAc);
                    return;
                }
                EvaluationComments evaluationCommentsObj = EvaluationCommentsBusiness.All
                    .Where(p => p.LastDigitSchoolID == partitionNumber

                                && p.ClassID == classID
                                && p.AcademicYearID == academicYearID
                                && p.EducationLevelID == educationLevelID
                                && p.SemesterID == semesterID
                                && p.EvaluationID == evaluationID
                                && p.EvaluationCriteriaID == evaluationCriteriaID
                                && p.TypeOfTeacher == typeOfTeacher
                                && p.PupilID == pupilID).FirstOrDefault();

                descriptionStrtmp.Append("Cập nhật sổ TD CLGD GVBM");
                paramsStrtmp.Append(MonthID + ", " + schoolID + ", " + academicYearID + ", " + classID + ", " +
                                    educationLevelID + ", " + semesterID + ", " + evaluationID + ", " +
                                    evaluationCriteriaID + ", " + typeOfTeacher);
                userFuntionsStrtmp.Append("Sổ TD CLGD (GVBM)");
                userActionsStrtmp.Append(SMAS.Business.Common.GlobalConstants.ACTION_UPDATE);
                userDescriptionsStrtmp.Append("Cập nhật đánh giá " + evaluationName + " HS " + objPF.FullName + ", " +
                                              "mã " + objPF.PupilCode + ", ");
                userDescriptionsStrtmp.Append("Lớp ")
                    .Append(objCP.DisplayName + "/" + objSC.DisplayName + "/" + "Tháng " + MonthID + "/Học kỳ " +
                            semesterID + "/Năm học " + objAcademicYear.Year + "-" + (objAcademicYear.Year + 1) + ". ");

                if (evaluationCommentsObj != null)
                {
                    //update thong tin
                    if (MonthID == GlobalConstants.MonthID_1 || MonthID == GlobalConstants.MonthID_6)
                    {
                        if (evaluationCommentsObj.CommentsM1M6 != insertOrUpdateObj.CommentsM1M6)
                        {
                            newObjectStrtmp.Append(CommentStr + insertOrUpdateObj.CommentsM1M6).Append("; ");
                            oldObjectStrtmp.Append(CommentStr + evaluationCommentsObj.CommentsM1M6).Append("; ");
                            isChange = true;
                            evaluationCommentsObj.CommentsM1M6 = insertOrUpdateObj.CommentsM1M6;
                        }
                    }
                    else if (MonthID == GlobalConstants.MonthID_2 || MonthID == GlobalConstants.MonthID_7)
                    {
                        if (evaluationCommentsObj.CommentsM2M7 != insertOrUpdateObj.CommentsM2M7)
                        {
                            newObjectStrtmp.Append(CommentStr + insertOrUpdateObj.CommentsM2M7).Append("; ");
                            oldObjectStrtmp.Append(CommentStr + evaluationCommentsObj.CommentsM2M7).Append("; ");
                            isChange = true;
                            evaluationCommentsObj.CommentsM2M7 = insertOrUpdateObj.CommentsM2M7;
                        }
                    }
                    else if (MonthID == GlobalConstants.MonthID_3 || MonthID == GlobalConstants.MonthID_8)
                    {
                        if (evaluationCommentsObj.CommentsM3M8 != insertOrUpdateObj.CommentsM3M8)
                        {
                            newObjectStrtmp.Append(CommentStr + insertOrUpdateObj.CommentsM3M8).Append("; ");
                            oldObjectStrtmp.Append(CommentStr + evaluationCommentsObj.CommentsM3M8).Append("; ");
                            isChange = true;
                            evaluationCommentsObj.CommentsM3M8 = insertOrUpdateObj.CommentsM3M8;
                        }
                    }
                    else if (MonthID == GlobalConstants.MonthID_4 || MonthID == GlobalConstants.MonthID_9)
                    {
                        if (evaluationCommentsObj.CommentsM4M9 != insertOrUpdateObj.CommentsM4M9)
                        {
                            newObjectStrtmp.Append(CommentStr + insertOrUpdateObj.CommentsM4M9).Append("; ");
                            oldObjectStrtmp.Append(CommentStr + evaluationCommentsObj.CommentsM4M9).Append("; ");
                            isChange = true;
                            evaluationCommentsObj.CommentsM4M9 = insertOrUpdateObj.CommentsM4M9;
                        }
                    }
                    else if (MonthID == GlobalConstants.MonthID_5 || MonthID == GlobalConstants.MonthID_10)
                    {
                        if (evaluationCommentsObj.CommentsM5M10 != insertOrUpdateObj.CommentsM5M10)
                        {
                            newObjectStrtmp.Append(CommentStr + insertOrUpdateObj.CommentsM5M10).Append("; ");
                            oldObjectStrtmp.Append(CommentStr + evaluationCommentsObj.CommentsM5M10).Append("; ");
                            isChange = true;
                            evaluationCommentsObj.CommentsM5M10 = insertOrUpdateObj.CommentsM5M10;
                        }
                    }
                    evaluationCommentsObj.UpdateTime = DateTime.Now;
                    EvaluationCommentsBusiness.Update(evaluationCommentsObj);
                }
                else
                {
                    long id = EvaluationCommentsBusiness.GetNextSeq<long>();
                    insertOrUpdateObj.EvaluationCommentsID = id;
                    if (MonthID == GlobalConstants.MonthID_1 || MonthID == GlobalConstants.MonthID_6)
                    {
                        if (!string.IsNullOrEmpty(insertOrUpdateObj.CommentsM1M6))
                        {
                            newObjectStrtmp.Append(CommentStr + insertOrUpdateObj.CommentsM1M6).Append("; ");
                            oldObjectStrtmp.Append(CommentStr).Append("; ");
                            isChange = true;
                        }
                    }
                    else if (MonthID == GlobalConstants.MonthID_2 || MonthID == GlobalConstants.MonthID_7)
                    {
                        if (!string.IsNullOrEmpty(insertOrUpdateObj.CommentsM2M7))
                        {
                            newObjectStrtmp.Append(CommentStr + insertOrUpdateObj.CommentsM2M7).Append("; ");
                            oldObjectStrtmp.Append(CommentStr).Append("; ");
                            isChange = true;
                        }
                    }
                    else if (MonthID == GlobalConstants.MonthID_3 || MonthID == GlobalConstants.MonthID_8)
                    {
                        if (!string.IsNullOrEmpty(insertOrUpdateObj.CommentsM3M8))
                        {
                            newObjectStrtmp.Append(CommentStr + insertOrUpdateObj.CommentsM3M8).Append("; ");
                            oldObjectStrtmp.Append(CommentStr).Append("; ");
                            isChange = true;
                        }
                    }
                    else if (MonthID == GlobalConstants.MonthID_4 || MonthID == GlobalConstants.MonthID_9)
                    {
                        if (!string.IsNullOrEmpty(insertOrUpdateObj.CommentsM4M9))
                        {
                            newObjectStrtmp.Append(CommentStr + insertOrUpdateObj.CommentsM4M9).Append("; ");
                            oldObjectStrtmp.Append(CommentStr).Append("; ");
                            isChange = true;
                        }
                    }
                    else if (MonthID == GlobalConstants.MonthID_5 || MonthID == GlobalConstants.MonthID_10)
                    {
                        if (!string.IsNullOrEmpty(insertOrUpdateObj.CommentsM5M10))
                        {
                            newObjectStrtmp.Append(CommentStr + insertOrUpdateObj.CommentsM5M10).Append("; ");
                            oldObjectStrtmp.Append(CommentStr).Append("; ");
                            isChange = true;
                        }
                    }
                    if (isChange)
                    {
                        insertOrUpdateObj.CreateTime = DateTime.Now;
                        insertOrUpdateObj.UpdateTime = null;
                        EvaluationCommentsBusiness.Insert(insertOrUpdateObj);
                    }
                }
                if (isChange)
                {
                    string tmpOld = string.Empty;
                    string tmpNew = string.Empty;
                    tmpOld = oldObjectStrtmp.Length > 0
                        ? oldObjectStrtmp.ToString().Substring(0, oldObjectStrtmp.Length - 2)
                        : "";
                    oldObjectStrtmp.Append("(Giá trị trước khi sửa): ").Append(tmpOld);
                    tmpNew = newObjectStrtmp.Length > 0
                        ? newObjectStrtmp.ToString().Substring(0, newObjectStrtmp.Length - 2)
                        : "";
                    newObjectStrtmp.Append("(Giá trị sau khi sửa): ").Append(tmpNew);
                    userDescriptionsStrtmp.Append("Giá trị cũ {").Append(tmpOld).Append("}. ");
                    userDescriptionsStrtmp.Append("Giá trị mới {").Append(tmpNew).Append("}");

                    objAc.PupilID = pupilID;
                    objAc.ClassID = classID;
                    objAc.EvaluationID = evaluationID;
                    objAc.SubjectID = evaluationCriteriaID;
                    objAc.ObjID = objectIDStrtmp;
                    objAc.Parameter = paramsStrtmp;
                    objAc.OldData = oldObjectStrtmp;
                    objAc.NewData = newObjectStrtmp;
                    objAc.UserAction = userActionsStrtmp;
                    objAc.UserFunction = userFuntionsStrtmp;
                    objAc.UserDescription = userDescriptionsStrtmp;
                    objAc.Description = descriptionStrtmp;
                    objAc.isInsertLog = isChange;
                }
                EvaluationCommentsBusiness.Save();
            }
            finally
            {
                SetAutoDetectChangesEnabled(true);
            }
        }

        /// <summary>
        /// Insert hoac update nhan xet cap 1(Nếu insert thì truyền noteinsert vào, update không cần)
        /// </summary>
        /// <param name="MonthID"></param>
        /// <param name="insertOrUpdateObj"></param>
        public List<EvaluationCommentActionAuditBO> InsertOrUpdateEvaluationCommentsByPupilIDForHeadTeacher(int MonthID, EvaluationComments insertOrUpdateObj)
        {
            try
            {

                SetAutoDetectChangesEnabled(false);
                long pupilID = 0;
                int typeOfTeacher = 0;
                long evaluationCriteriaID = 0;
                long evaluationID = 0;
                int semesterID = 0;
                int educationLevelID = 0;
                int schoolID = 0;
                int academicYearID = 0;
                long classID = 0;
                int partitionNumber = 0;
                string strMonth = string.Empty;

                List<EvaluationCommentActionAuditBO> lstActionAuditBO = new List<EvaluationCommentActionAuditBO>();

                if (insertOrUpdateObj != null)
                {
                    pupilID = insertOrUpdateObj.PupilID;
                    typeOfTeacher = insertOrUpdateObj.TypeOfTeacher;
                    evaluationCriteriaID = insertOrUpdateObj.EvaluationCriteriaID;
                    evaluationID = insertOrUpdateObj.EvaluationID;
                    semesterID = insertOrUpdateObj.SemesterID;
                    educationLevelID = insertOrUpdateObj.EducationLevelID;
                    schoolID = insertOrUpdateObj.SchoolID;
                    academicYearID = insertOrUpdateObj.AcademicYearID;
                    classID = insertOrUpdateObj.ClassID;
                    partitionNumber = UtilsBusiness.GetPartionId(schoolID);
                }
                //
                AcademicYear objAcademicYear = AcademicYearBusiness.Find(academicYearID);
                if (objAcademicYear == null)
                {
                    return lstActionAuditBO;
                }
                //Nau la du lieu da chuyen vao lich su thi thuc hien cap nhat vao bang du lieu lich su
                //bool isMovedHistory = UtilsBusiness.IsMoveHistory(objAcademicYear);
                //if (isMovedHistory)
                //{
                //    EvaluationCommentsHistoryBusiness.InsertOrUpdateEvaluationCommentsByPupilID(MonthID, insertOrUpdateObj);
                //    return lstActionAuditBO;
                //}

                //Lay danh sach lop cua truong
                ClassProfile cp = ClassProfileBusiness.Find(classID);

                //danh sach hoc sinh cua lop
                List<PupilOfClass> lstPoc = PupilOfClassBusiness.All.Where
                    (o => o.AcademicYearID == academicYearID && o.ClassID == classID).ToList();

                //Lay mon hoc
                List<SubjectCat> lstSuject =
                    SubjectCatBusiness.All.Where(o => o.AppliedLevel == GlobalConstants.APPLIED_LEVEL_PRIMARY
                        ).ToList();

                //Lay tieu chi
                List<EvaluationCriteria> lstEvaluationCriteria = EvaluationCriteriaBusiness.All.ToList();

                EvaluationComments evaluationCommentsObj = EvaluationCommentsBusiness.All
                    .Where(p => p.LastDigitSchoolID == partitionNumber

                                && p.ClassID == classID
                                && p.AcademicYearID == academicYearID
                                && p.EducationLevelID == educationLevelID
                                && p.SemesterID == semesterID
                                && p.EvaluationID == evaluationID
                                && p.EvaluationCriteriaID == evaluationCriteriaID
                                && p.TypeOfTeacher == typeOfTeacher
                                && p.PupilID == pupilID).FirstOrDefault();
                if (evaluationCommentsObj != null)
                {
                    //update thong tin
                    evaluationCommentsObj.UpdateTime = DateTime.Now;
                    if (MonthID == GlobalConstants.MonthID_1 || MonthID == GlobalConstants.MonthID_6)
                    {
                        if (MonthID == GlobalConstants.MonthID_1)
                        {
                            strMonth += "T1,";
                        }
                        else
                        {
                            strMonth += "T6,";
                        }
                        evaluationCommentsObj.CommentsM1M6 = insertOrUpdateObj.CommentsM1M6;
                    }
                    else if (MonthID == GlobalConstants.MonthID_2 || MonthID == GlobalConstants.MonthID_7)
                    {
                        if (MonthID == GlobalConstants.MonthID_2)
                        {
                            strMonth += "T2,";
                        }
                        else
                        {
                            strMonth += "T7,";
                        }
                        evaluationCommentsObj.CommentsM2M7 = insertOrUpdateObj.CommentsM2M7;
                    }
                    else if (MonthID == GlobalConstants.MonthID_3 || MonthID == GlobalConstants.MonthID_8)
                    {
                        if (MonthID == GlobalConstants.MonthID_3)
                        {
                            strMonth += "T3,";
                        }
                        else
                        {
                            strMonth += "T8,";
                        }
                        evaluationCommentsObj.CommentsM3M8 = insertOrUpdateObj.CommentsM3M8;
                    }
                    else if (MonthID == GlobalConstants.MonthID_4 || MonthID == GlobalConstants.MonthID_9)
                    {
                        if (MonthID == GlobalConstants.MonthID_4)
                        {
                            strMonth += "T4,";
                        }
                        else
                        {
                            strMonth += "T9,";
                        }
                        evaluationCommentsObj.CommentsM4M9 = insertOrUpdateObj.CommentsM4M9;
                    }
                    else if (MonthID == GlobalConstants.MonthID_5 || MonthID == GlobalConstants.MonthID_10)
                    {
                        if (MonthID == GlobalConstants.MonthID_5)
                        {
                            strMonth += "T5,";
                        }
                        else
                        {
                            strMonth += "T10,";
                        }
                        evaluationCommentsObj.CommentsM5M10 = insertOrUpdateObj.CommentsM5M10;
                    }

                    if (String.IsNullOrEmpty(evaluationCommentsObj.CommentsM1M6))
                        evaluationCommentsObj.CommentsM1M6 = null;
                    if (String.IsNullOrEmpty(evaluationCommentsObj.CommentsM2M7))
                        evaluationCommentsObj.CommentsM2M7 = null;
                    if (String.IsNullOrEmpty(evaluationCommentsObj.CommentsM3M8))
                        evaluationCommentsObj.CommentsM3M8 = null;
                    if (String.IsNullOrEmpty(evaluationCommentsObj.CommentsM4M9))
                        evaluationCommentsObj.CommentsM4M9 = null;
                    if (String.IsNullOrEmpty(evaluationCommentsObj.CommentsM5M10))
                        evaluationCommentsObj.CommentsM5M10 = null;

                    if (evaluationCommentsObj.CommentsM1M6 !=
                        this.context.Entry<EvaluationComments>(evaluationCommentsObj)
                            .OriginalValues.GetValue<string>("CommentsM1M6")
                        ||
                        evaluationCommentsObj.CommentsM2M7 !=
                        this.context.Entry<EvaluationComments>(evaluationCommentsObj)
                            .OriginalValues.GetValue<string>("CommentsM2M7")
                        ||
                        evaluationCommentsObj.CommentsM3M8 !=
                        this.context.Entry<EvaluationComments>(evaluationCommentsObj)
                            .OriginalValues.GetValue<string>("CommentsM3M8")
                        ||
                        evaluationCommentsObj.CommentsM4M9 !=
                        this.context.Entry<EvaluationComments>(evaluationCommentsObj)
                            .OriginalValues.GetValue<string>("CommentsM4M9")
                        ||
                        evaluationCommentsObj.CommentsM5M10 !=
                        this.context.Entry<EvaluationComments>(evaluationCommentsObj)
                            .OriginalValues.GetValue<string>("CommentsM5M10"))
                    {

                        evaluationCommentsObj.UpdateTime = DateTime.Now;
                        EvaluationCommentsBusiness.Update(evaluationCommentsObj);

                        //Tao action audit
                        EvaluationCommentActionAuditBO aab =
                            new EvaluationCommentActionAuditBO(
                                EvaluationCommentActionAuditBO.EVALUATION_COMMENT_AUDIT_ACTION_UPDATE);
                        aab.AcademicYear = objAcademicYear.DisplayTitle;
                        aab.ClassName = cp.DisplayName;
                        aab.Month = MonthID;
                        aab.ObjectId = evaluationCommentsObj.PupilID;

                        string oldVal = "";
                        string newVal = "";
                        if (MonthID == 1 || MonthID == 6)
                        {
                            newVal = evaluationCommentsObj.CommentsM1M6;
                            oldVal =
                                this.context.Entry<EvaluationComments>(evaluationCommentsObj)
                                    .OriginalValues.GetValue<string>("CommentsM1M6");
                        }
                        else if (MonthID == 2 || MonthID == 7)
                        {
                            newVal = evaluationCommentsObj.CommentsM2M7;
                            oldVal =
                                this.context.Entry<EvaluationComments>(evaluationCommentsObj)
                                    .OriginalValues.GetValue<string>("CommentsM2M7");
                        }
                        else if (MonthID == 3 || MonthID == 8)
                        {
                            newVal = evaluationCommentsObj.CommentsM3M8;
                            oldVal =
                                this.context.Entry<EvaluationComments>(evaluationCommentsObj)
                                    .OriginalValues.GetValue<string>("CommentsM3M8");
                        }
                        else if (MonthID == 4 || MonthID == 9)
                        {
                            newVal = evaluationCommentsObj.CommentsM4M9;
                            oldVal =
                                this.context.Entry<EvaluationComments>(evaluationCommentsObj)
                                    .OriginalValues.GetValue<string>("CommentsM4M9");
                        }
                        else if (MonthID == 5 || MonthID == 10)
                        {
                            newVal = evaluationCommentsObj.CommentsM5M10;
                            oldVal =
                                this.context.Entry<EvaluationComments>(evaluationCommentsObj)
                                    .OriginalValues.GetValue<string>("CommentsM5M10");
                        }
                        aab.NewObjectValue.Add(CRITERIA_NXT, newVal);
                        aab.OldObjectValue.Add(CRITERIA_NXT, oldVal);

                        PupilOfClass poc =
                            lstPoc.Where(o => o.PupilID == evaluationCommentsObj.PupilID).FirstOrDefault();
                        aab.PupilCode = poc.PupilProfile.PupilCode;
                        aab.PupilName = poc.PupilProfile.FullName;
                        aab.Semester = semesterID;
                        SubjectCat sc =
                            lstSuject.Where(o => o.SubjectCatID == evaluationCommentsObj.EvaluationCriteriaID)
                                .FirstOrDefault();
                        if (sc != null)
                        {
                            aab.SubjectName = sc.DisplayName;
                        }
                        EvaluationCriteria ec =
                            lstEvaluationCriteria.FirstOrDefault(
                                o => o.EvaluationCriteriaID == evaluationCommentsObj.EvaluationCriteriaID);
                        if (ec != null)
                        {
                            aab.EvaluationCriteriaName = ec.CriteriaName;
                        }
                        aab.EvaluationID = evaluationID;

                        lstActionAuditBO.Add(aab);

                    }
                }
                else
                {
                    long id = EvaluationCommentsBusiness.GetNextSeq<long>();
                    insertOrUpdateObj.EvaluationCommentsID = id;
                    insertOrUpdateObj.CreateTime = DateTime.Now;
                    if (!String.IsNullOrEmpty(insertOrUpdateObj.CommentsM1M6)
                        || !String.IsNullOrEmpty(insertOrUpdateObj.CommentsM2M7)
                        || !String.IsNullOrEmpty(insertOrUpdateObj.CommentsM3M8)
                        || !String.IsNullOrEmpty(insertOrUpdateObj.CommentsM4M9)
                        || !String.IsNullOrEmpty(insertOrUpdateObj.CommentsM5M10))
                    {
                        insertOrUpdateObj.UpdateTime = null;
                        insertOrUpdateObj.CreateTime = DateTime.Now;
                        EvaluationCommentsBusiness.Insert(insertOrUpdateObj);

                        //Tao action audit
                        EvaluationCommentActionAuditBO aab =
                            new EvaluationCommentActionAuditBO(
                                EvaluationCommentActionAuditBO.EVALUATION_COMMENT_AUDIT_ACTION_UPDATE);
                        aab.AcademicYear = objAcademicYear.DisplayTitle;
                        aab.ClassName = cp.DisplayName;
                        aab.Month = MonthID;
                        aab.ObjectId = insertOrUpdateObj.PupilID;
                        PupilOfClass poc = lstPoc.Where(o => o.PupilID == insertOrUpdateObj.PupilID).FirstOrDefault();
                        aab.PupilCode = poc.PupilProfile.PupilCode;
                        aab.PupilName = poc.PupilProfile.FullName;
                        aab.Semester = semesterID;
                        SubjectCat sc =
                            lstSuject.Where(o => o.SubjectCatID == insertOrUpdateObj.EvaluationCriteriaID)
                                .FirstOrDefault();
                        if (sc != null)
                        {
                            aab.SubjectName = sc.DisplayName;
                        }
                        EvaluationCriteria ec =
                            lstEvaluationCriteria.FirstOrDefault(
                                o => o.EvaluationCriteriaID == insertOrUpdateObj.EvaluationCriteriaID);
                        if (ec != null)
                        {
                            aab.EvaluationCriteriaName = ec.CriteriaName;
                        }
                        aab.EvaluationID = evaluationID;

                        string val = "";
                        if (MonthID == 1 || MonthID == 6)
                        {
                            val = insertOrUpdateObj.CommentsM1M6;
                        }
                        else if (MonthID == 2 || MonthID == 7)
                        {
                            val = insertOrUpdateObj.CommentsM2M7;
                        }
                        else if (MonthID == 3 || MonthID == 8)
                        {
                            val = insertOrUpdateObj.CommentsM3M8;
                        }
                        else if (MonthID == 4 || MonthID == 9)
                        {
                            val = insertOrUpdateObj.CommentsM4M9;
                        }
                        else if (MonthID == 5 || MonthID == 10)
                        {
                            val = insertOrUpdateObj.CommentsM5M10;
                        }
                        aab.NewObjectValue.Add(CRITERIA_NXT, val);
                        aab.OldObjectValue.Add(CRITERIA_NXT, string.Empty);



                        lstActionAuditBO.Add(aab);
                    }


                }
                EvaluationCommentsBusiness.Save();

                return lstActionAuditBO;
            }
            finally
            {
                SetAutoDetectChangesEnabled(true);
            }
        }


        public List<EvaluationCommentActionAuditBO> DeleteEvaluationComments(List<long> idList, List<long> idListSummedEnding, int schoolID, int academicYearID, int semesterID, int classID, int evaluationCriteriaID, int typeOfTeacher, int educationLevelID, int evaluationID, int monthID)
        {
            try
            {

                SetAutoDetectChangesEnabled(false);
                int partitionNumber = schoolID % GlobalConstants.PARTITION;
                string strCheckLockMark = this.CheckLockMark(classID, schoolID, academicYearID);
                List<EvaluationComments> DeleteList = new List<EvaluationComments>();

                //Lay danh sach lop cua truong
                ClassProfile cp = ClassProfileBusiness.Find(classID);

                //danh sach hoc sinh cua lop
                List<PupilOfClass> lstPoc = PupilOfClassBusiness.All.Where
                    (o => o.AcademicYearID == academicYearID && o.ClassID == classID).ToList();

                //Lay mon hoc
                List<SubjectCat> lstSuject =
                    SubjectCatBusiness.All.Where(o => o.AppliedLevel == GlobalConstants.APPLIED_LEVEL_PRIMARY
                        ).ToList();
                //Lay tieu chi
                List<EvaluationCriteria> lstEvaluationCriteria = EvaluationCriteriaBusiness.All.ToList();
                AcademicYear aca = AcademicYearBusiness.Find(academicYearID);

                List<EvaluationCommentActionAuditBO> lstActionAuditBO = new List<EvaluationCommentActionAuditBO>();

                if (idList != null && idList.Count > 0)
                {
                    if (monthID != GlobalConstants.MonthID_11)
                    {
                        List<EvaluationComments> evaluationCommentsList = EvaluationCommentsBusiness.All
                            .Where(p => p.LastDigitSchoolID == partitionNumber
                                        && p.ClassID == classID
                                        && p.AcademicYearID == academicYearID
                                        && p.EducationLevelID == educationLevelID
                                        && p.SemesterID == semesterID
                                        && p.EvaluationID == evaluationID
                                //&& p.EvaluationCriteriaID == evaluationCriteriaID
                                        && p.TypeOfTeacher == typeOfTeacher).ToList();
                        for (int i = 0; i < idList.Count; i++)
                        {
                            // kiem tra ID lay vao co dung cua lop duoc chon khong
                            long evaluationCommentsID = idList[i];
                            EvaluationComments evaluationCommentsObj =
                                evaluationCommentsList.Where(p => p.EvaluationCommentsID == evaluationCommentsID)
                                    .FirstOrDefault();
                            if (evaluationCommentsObj != null)
                            {
                                if ((!strCheckLockMark.Contains("HK1") &&
                                     semesterID == GlobalConstants.SEMESTER_OF_YEAR_FIRST) ||
                                    (!strCheckLockMark.Contains("HK2") &&
                                     semesterID == GlobalConstants.SEMESTER_OF_YEAR_SECOND))
                                {
                                    if (monthID == GlobalConstants.MonthID_1 || monthID == GlobalConstants.MonthID_6)
                                    {
                                        if (!strCheckLockMark.Contains("T1") || !strCheckLockMark.Contains("T6"))
                                        {
                                            evaluationCommentsObj.CommentsM1M6 = null;
                                        }
                                    }
                                    else if (monthID == GlobalConstants.MonthID_2 || monthID == GlobalConstants.MonthID_7)
                                    {
                                        if (!strCheckLockMark.Contains("T2") || !strCheckLockMark.Contains("T7"))
                                        {
                                            evaluationCommentsObj.CommentsM2M7 = null;
                                        }
                                    }
                                    else if (monthID == GlobalConstants.MonthID_3 ||
                                             monthID == GlobalConstants.MonthID_8)
                                    {
                                        if (!strCheckLockMark.Contains("T3") || !strCheckLockMark.Contains("T8"))
                                        {
                                            evaluationCommentsObj.CommentsM3M8 = null;
                                        }
                                    }
                                    else if (monthID == GlobalConstants.MonthID_4 ||
                                             monthID == GlobalConstants.MonthID_9)
                                    {
                                        if (!strCheckLockMark.Contains("T4") ||
                                            !strCheckLockMark.Contains("T9"))
                                        {
                                            evaluationCommentsObj.CommentsM4M9 = null;
                                        }
                                    }
                                    else if (monthID == GlobalConstants.MonthID_5 ||
                                             monthID == GlobalConstants.MonthID_10)
                                    {
                                        if (!strCheckLockMark.Contains("T5") ||
                                            !strCheckLockMark.Contains("T10"))
                                        {
                                            evaluationCommentsObj.CommentsM5M10 = null;
                                        }
                                    }
                                }
                                evaluationCommentsObj.UpdateTime = DateTime.Now;
                                DeleteList.Add(evaluationCommentsObj);
                            }
                        }

                        if (DeleteList != null && DeleteList.Count > 0)
                        {
                            for (int j = 0; j < DeleteList.Count; j++)
                            {
                                EvaluationComments objDelete = DeleteList[j];
                                EvaluationCommentsBusiness.Update(objDelete);

                                //tao action audit
                                if (objDelete.CommentsM1M6 !=
                                    this.context.Entry<EvaluationComments>(objDelete)
                                        .OriginalValues.GetValue<string>("CommentsM1M6")
                                    ||
                                    objDelete.CommentsM2M7 !=
                                    this.context.Entry<EvaluationComments>(objDelete)
                                        .OriginalValues.GetValue<string>("CommentsM2M7")
                                    ||
                                    objDelete.CommentsM3M8 !=
                                    this.context.Entry<EvaluationComments>(objDelete)
                                        .OriginalValues.GetValue<string>("CommentsM3M8")
                                    ||
                                    objDelete.CommentsM4M9 !=
                                    this.context.Entry<EvaluationComments>(objDelete)
                                        .OriginalValues.GetValue<string>("CommentsM4M9")
                                    ||
                                    objDelete.CommentsM5M10 !=
                                    this.context.Entry<EvaluationComments>(objDelete)
                                        .OriginalValues.GetValue<string>("CommentsM5M10"))
                                {
                                    EvaluationCommentActionAuditBO aab =
                                        new EvaluationCommentActionAuditBO(
                                            EvaluationCommentActionAuditBO.EVALUATION_COMMENT_AUDIT_ACTION_DELETE);
                                    aab.AcademicYear = aca.DisplayTitle;
                                    aab.ClassName = cp.DisplayName;
                                    aab.Month = monthID;
                                    aab.ObjectId = objDelete.PupilID;

                                    string oldVal = "";
                                    if (monthID == 1 || monthID == 6)
                                    {
                                        oldVal =
                                            this.context.Entry<EvaluationComments>(objDelete)
                                                .OriginalValues.GetValue<string>("CommentsM1M6");
                                    }
                                    else if (monthID == 2 || monthID == 7)
                                    {
                                        oldVal =
                                            this.context.Entry<EvaluationComments>(objDelete)
                                                .OriginalValues.GetValue<string>("CommentsM2M7");
                                    }
                                    else if (monthID == 3 || monthID == 8)
                                    {
                                        oldVal =
                                            this.context.Entry<EvaluationComments>(objDelete)
                                                .OriginalValues.GetValue<string>("CommentsM3M8");
                                    }
                                    else if (monthID == 4 || monthID == 9)
                                    {
                                        oldVal =
                                            this.context.Entry<EvaluationComments>(objDelete)
                                                .OriginalValues.GetValue<string>("CommentsM4M9");
                                    }
                                    else if (monthID == 5 || monthID == 10)
                                    {
                                        oldVal =
                                            this.context.Entry<EvaluationComments>(objDelete)
                                                .OriginalValues.GetValue<string>("CommentsM5M10");
                                    }
                                    aab.NewObjectValue.Add(CRITERIA_NXT, String.Empty);
                                    aab.OldObjectValue.Add(CRITERIA_NXT, oldVal);

                                    PupilOfClass poc =
                                        lstPoc.Where(o => o.PupilID == objDelete.PupilID).FirstOrDefault();
                                    aab.PupilCode = poc.PupilProfile.PupilCode;
                                    aab.PupilName = poc.PupilProfile.FullName;
                                    aab.Semester = semesterID;
                                    SubjectCat sc =
                                        lstSuject.Where(o => o.SubjectCatID == objDelete.EvaluationCriteriaID)
                                            .FirstOrDefault();
                                    if (sc != null)
                                    {
                                        aab.SubjectName = sc.DisplayName;
                                    }
                                    EvaluationCriteria ec =
                                        lstEvaluationCriteria.FirstOrDefault(
                                            o => o.EvaluationCriteriaID == objDelete.EvaluationCriteriaID);
                                    if (ec != null)
                                    {
                                        aab.EvaluationCriteriaName = ec.CriteriaName;
                                    }
                                    aab.EvaluationID = evaluationID;

                                    lstActionAuditBO.Add(aab);
                                }
                            }
                            EvaluationCommentsBusiness.Save();
                        }
                    }
                    else
                    {
                        List<long> summedEvaluationIDList = new List<long>();
                        List<SummedEvaluation> summedEvaluationList = SummedEvaluationBusiness.All
                            .Where(p => p.LastDigitSchoolID == partitionNumber

                                        && p.ClassID == classID
                                        && p.AcademicYearID == academicYearID
                                        && p.EducationLevelID == educationLevelID
                                        && p.SemesterID == semesterID
                                        && p.EvaluationID == evaluationID
                                        && p.EvaluationCriteriaID == evaluationCriteriaID).ToList();
                        for (int i = 0; i < idList.Count; i++)
                        {
                            // kiem tra ID lay vao co dung cua lop duoc chon khong
                            long summedEvaluationID = idList[i];
                            SummedEvaluation evaluationCommentsObj =
                                summedEvaluationList.Where(p => p.SummedEvaluationID == summedEvaluationID)
                                    .FirstOrDefault();
                            if (evaluationCommentsObj != null)
                            {
                                if ((!strCheckLockMark.Contains("HK1") &&
                                     semesterID == GlobalConstants.SEMESTER_OF_YEAR_FIRST) ||
                                    (!strCheckLockMark.Contains("HK2") &&
                                     semesterID == GlobalConstants.SEMESTER_OF_YEAR_SECOND))
                                {
                                    if ((!strCheckLockMark.Contains("CK1") &&
                                         semesterID == GlobalConstants.SEMESTER_OF_YEAR_FIRST) ||
                                        (!strCheckLockMark.Contains("CK2") &&
                                         semesterID == GlobalConstants.SEMESTER_OF_YEAR_SECOND))
                                    {
                                        summedEvaluationIDList.Add(evaluationCommentsObj.SummedEvaluationID);
                                    }
                                }
                            }
                        }

                        if (summedEvaluationIDList != null && summedEvaluationIDList.Count > 0)
                        {
                            for (int j = 0; j < summedEvaluationIDList.Count; j++)
                            {
                                SummedEvaluationBusiness.Delete(summedEvaluationIDList[j]);
                                SummedEvaluation se =
                                    summedEvaluationList.FirstOrDefault(
                                        o => o.SummedEvaluationID == summedEvaluationIDList[j]);

                                if (!String.IsNullOrEmpty(se.EndingEvaluation)
                                    || !String.IsNullOrEmpty(se.EndingComments)
                                    || se.PeriodicEndingMark.HasValue)
                                {
                                    //them action audit
                                    //Kiem tra da co action audit hay chua
                                    EvaluationCommentActionAuditBO aab =
                                        lstActionAuditBO.Where(o => o.ObjectId == se.PupilID).FirstOrDefault();
                                    if (aab == null)
                                    {
                                        aab =
                                            new EvaluationCommentActionAuditBO(
                                                EvaluationCommentActionAuditBO.EVALUATION_COMMENT_AUDIT_ACTION_DELETE);
                                        lstActionAuditBO.Add(aab);
                                        aab.AcademicYear = aca.DisplayTitle;
                                        aab.ClassName = cp.DisplayName;
                                        aab.Month = monthID;
                                        aab.ObjectId = se.PupilID;
                                        PupilOfClass poc = lstPoc.Where(o => o.PupilID == se.PupilID).FirstOrDefault();
                                        aab.PupilCode = poc.PupilProfile.PupilCode;
                                        aab.PupilName = poc.PupilProfile.FullName;
                                        aab.Semester = semesterID;
                                        SubjectCat sc =
                                            lstSuject.Where(o => o.SubjectCatID == se.EvaluationCriteriaID)
                                                .FirstOrDefault();
                                        if (sc != null)
                                        {
                                            aab.SubjectName = sc.DisplayName;
                                        }
                                        EvaluationCriteria ec =
                                            lstEvaluationCriteria.FirstOrDefault(
                                                o => o.EvaluationCriteriaID == se.EvaluationCriteriaID);
                                        if (ec != null)
                                        {
                                            aab.EvaluationCriteriaName = ec.CriteriaName;
                                        }
                                        aab.EvaluationID = evaluationID;

                                    }
                                    if (se.PeriodicEndingMark != null)
                                    {
                                        aab.OldObjectValue.Add(CRITERIA_KTDKCK, se.PeriodicEndingMark.Value.ToString());
                                        aab.NewObjectValue.Add(CRITERIA_KTDKCK, string.Empty);
                                    }

                                    if (!String.IsNullOrEmpty(se.EndingEvaluation))
                                    {
                                        aab.OldObjectValue.Add(CRITERIA_DG, se.EndingEvaluation);
                                        aab.NewObjectValue.Add(CRITERIA_DG, string.Empty);
                                    }

                                    if (!string.IsNullOrEmpty(se.EndingComments))
                                    {
                                        aab.OldObjectValue.Add(CRITERIA_NXCK, se.EndingComments);
                                        aab.NewObjectValue.Add(CRITERIA_NXCK, string.Empty);
                                    }
                                }
                            }
                            SummedEvaluationBusiness.Save();
                        }
                    }
                }

                if (idListSummedEnding != null && idListSummedEnding.Count > 0)
                {
                    List<long> summedEndingList = new List<long>();
                    List<SummedEndingEvaluation> summedEndingEvaluationList = SummedEndingEvaluationBusiness.All
                        .Where(p => p.LastDigitSchoolID == partitionNumber

                                    && p.ClassID == classID
                                    && p.AcademicYearID == academicYearID
                                    && p.SemesterID == semesterID
                                    && p.EvaluationID == evaluationID).ToList();
                    for (int i = 0; i < idListSummedEnding.Count; i++)
                    {
                        long summedEndingID = idListSummedEnding[i];
                        SummedEndingEvaluation summedEndingOBj =
                            summedEndingEvaluationList.Where(p => p.SummedEndingEvaluationID == summedEndingID)
                                .FirstOrDefault();
                        if (summedEndingOBj != null)
                        {
                            if ((!strCheckLockMark.Contains("HK1") &&
                                 semesterID == GlobalConstants.SEMESTER_OF_YEAR_FIRST) ||
                                (!strCheckLockMark.Contains("HK2") &&
                                 semesterID == GlobalConstants.SEMESTER_OF_YEAR_SECOND))
                            {
                                if ((!strCheckLockMark.Contains("CK1") &&
                                     semesterID == GlobalConstants.SEMESTER_OF_YEAR_FIRST) ||
                                    (!strCheckLockMark.Contains("CK2") &&
                                     semesterID == GlobalConstants.SEMESTER_OF_YEAR_SECOND))
                                {
                                    summedEndingList.Add(summedEndingOBj.SummedEndingEvaluationID);
                                }
                            }
                        }
                    }

                    if (summedEndingList != null && summedEndingList.Count > 0)
                    {
                        for (int j = 0; j < summedEndingList.Count; j++)
                        {
                            SummedEndingEvaluationBusiness.Delete(summedEndingList[j]);
                            SummedEndingEvaluation see =
                                summedEndingEvaluationList.FirstOrDefault(
                                    o => o.SummedEndingEvaluationID == summedEndingList[j]);

                            if (!string.IsNullOrEmpty(see.EndingEvaluation))
                            {
                                //them action audit
                                //Kiem tra da co action audit hay chua
                                EvaluationCommentActionAuditBO aab =
                                    lstActionAuditBO.Where(o => o.ObjectId == see.PupilID).FirstOrDefault();
                                if (aab == null)
                                {
                                    aab =
                                        new EvaluationCommentActionAuditBO(
                                            EvaluationCommentActionAuditBO.EVALUATION_COMMENT_AUDIT_ACTION_DELETE);
                                    lstActionAuditBO.Add(aab);
                                    aab.AcademicYear = aca.DisplayTitle;
                                    aab.ClassName = cp.DisplayName;
                                    aab.Month = monthID;
                                    aab.ObjectId = see.PupilID;
                                    PupilOfClass poc = lstPoc.Where(o => o.PupilID == see.PupilID).FirstOrDefault();
                                    aab.PupilCode = poc.PupilProfile.PupilCode;
                                    aab.PupilName = poc.PupilProfile.FullName;
                                    aab.Semester = semesterID;
                                    aab.SubjectName = String.Empty;
                                    aab.EvaluationCriteriaName = String.Empty;
                                    aab.EvaluationID = evaluationID;

                                }
                                if (!String.IsNullOrEmpty(see.EndingEvaluation))
                                {
                                    aab.OldObjectValue.Add(CRITERIA_DG, see.EndingEvaluation);
                                    aab.NewObjectValue.Add(CRITERIA_DG, string.Empty);
                                }
                            }
                        }
                    }
                    SummedEndingEvaluationBusiness.Save();
                }

                return lstActionAuditBO;
            }
            finally
            {
                SetAutoDetectChangesEnabled(true);
            }
        }

        public void InsertOrUpdateEducationQualityImport(int schoolID, int academicYearID, int classID, int educationLevelID, int semesterID, int evaluationID, int evaluationCriteriaID, int typeOfTeacher, List<EvaluationComments> listInsertOrUpdate)
        {
            try
            {
                SetAutoDetectChangesEnabled(false);

                AcademicYear academicYear = AcademicYearBusiness.Find(academicYearID);
                bool ismovedHistory = UtilsBusiness.IsMoveHistory(academicYear);
                if (ismovedHistory)
                {
                    IDictionary<string, object> dic = new Dictionary<string, object>();
                    dic.Add("SchoolId", schoolID);
                    dic.Add("AcademicYearId", academicYearID);
                    dic.Add("ClassId", classID);
                    dic.Add("EducationLevelId", educationLevelID);
                    dic.Add("SemesterId", semesterID);
                    dic.Add("EvaluationId", evaluationID);
                    dic.Add("EvaluationCriteriaID", evaluationCriteriaID);
                    dic.Add("TypeOfTeacher", typeOfTeacher);
                    EvaluationCommentsHistoryBusiness.InsertOrUpdateEducationQualityImport(dic, listInsertOrUpdate);
                    return;
                }

                int partitionNumber = schoolID % GlobalConstants.PARTITION;
                IDictionary<string, object> objDic = new Dictionary<string, object>()
            {
                {"SchoolID",schoolID},
                {"AcademicYearID",academicYearID},
                {"ClassID",classID}
            };
                LockMarkPrimary objLockMarkPrimary = LockMarkPrimaryBusiness.SearchLockedMark(objDic).ToList().FirstOrDefault();
                string LockMarkTitle = objLockMarkPrimary != null ? objLockMarkPrimary.LockPrimary : "";
                bool lockT1 = UtilsBusiness.CheckLockMarkBySemester(semesterID, LockMarkTitle, "T1", "");
                bool lockT2 = UtilsBusiness.CheckLockMarkBySemester(semesterID, LockMarkTitle, "T2", "");
                bool lockT3 = UtilsBusiness.CheckLockMarkBySemester(semesterID, LockMarkTitle, "T3", "");
                bool lockT4 = UtilsBusiness.CheckLockMarkBySemester(semesterID, LockMarkTitle, "T4", "");
                bool lockT5 = UtilsBusiness.CheckLockMarkBySemester(semesterID, LockMarkTitle, "T5", "");
                List<EvaluationComments> evaluationCommentsList = EvaluationCommentsBusiness.All
                    .Where(p => p.LastDigitSchoolID == partitionNumber

                             && p.ClassID == classID
                             && p.AcademicYearID == academicYearID
                             && p.EducationLevelID == educationLevelID
                             && p.SemesterID == semesterID
                             && p.EvaluationID == evaluationID
                             && (p.EvaluationCriteriaID == evaluationCriteriaID || evaluationCriteriaID == 0)
                             && p.TypeOfTeacher == typeOfTeacher).ToList();

                if (listInsertOrUpdate != null && listInsertOrUpdate.Count > 0)
                {
                    List<EvaluationComments> InsertList = new List<EvaluationComments>();
                    List<EvaluationComments> UpdateList = new List<EvaluationComments>();
                    EvaluationComments checkInlist = null;
                    for (int i = 0; i < listInsertOrUpdate.Count; i++)
                    {
                        EvaluationComments evaluationCommentsObj = listInsertOrUpdate[i];
                        long pupilID = evaluationCommentsObj.PupilID;
                        string strMonth = string.Empty;
                        //kiem tra pupilID da ton tai trong list da select len chua
                        checkInlist = evaluationCommentsList.Where(p => p.PupilID == pupilID).FirstOrDefault();
                        if (checkInlist == null)
                        {
                            // insert thong tin moi
                            if (lockT1) evaluationCommentsObj.CommentsM1M6 = "";
                            if (lockT2) evaluationCommentsObj.CommentsM2M7 = "";
                            if (lockT3) evaluationCommentsObj.CommentsM3M8 = "";
                            if (lockT4) evaluationCommentsObj.CommentsM4M9 = "";
                            if (lockT5) evaluationCommentsObj.CommentsM5M10 = "";
                            InsertList.Add(evaluationCommentsObj);
                        }
                        else
                        {
                            //update thong tin
                            if (semesterID == GlobalConstants.SEMESTER_OF_YEAR_FIRST)
                            {
                                strMonth += "T1,T2,T3,T4,T5";
                            }
                            else
                            {
                                strMonth += "T6,T7,T8,T9,T10";
                            }

                            if (!lockT1)
                            {
                                checkInlist.CommentsM1M6 = evaluationCommentsObj.CommentsM1M6;
                            }
                            if (!lockT2)
                            {
                                checkInlist.CommentsM2M7 = evaluationCommentsObj.CommentsM2M7;
                            }
                            if (!lockT3)
                            {
                                checkInlist.CommentsM3M8 = evaluationCommentsObj.CommentsM3M8;
                            }
                            if (!lockT4)
                            {
                                checkInlist.CommentsM4M9 = evaluationCommentsObj.CommentsM4M9;
                            }
                            if (!lockT5)
                            {
                                checkInlist.CommentsM5M10 = evaluationCommentsObj.CommentsM5M10;
                            }

                            checkInlist.NoteUpdate = strMonth + "," + DateTime.Now.ToString() + "|";
                            UpdateList.Add(checkInlist);
                        }
                    }
                    if (InsertList != null && InsertList.Count > 0)
                    {
                        foreach (EvaluationComments insertObj in InsertList)
                        {
                            insertObj.EvaluationCommentsID = EvaluationCommentsBusiness.GetNextSeq<long>();
                            insertObj.CreateTime = DateTime.Now;
                            insertObj.UpdateTime = null;
                            EvaluationCommentsBusiness.Insert(insertObj);
                        }
                    }

                    if (UpdateList != null && UpdateList.Count > 0)
                    {
                        foreach (EvaluationComments updateObj in UpdateList)
                        {
                            updateObj.UpdateTime = DateTime.Now;
                            EvaluationCommentsBusiness.Update(updateObj);
                        }
                    }
                }
                EvaluationCommentsBusiness.Save();

            }
            finally
            {
                SetAutoDetectChangesEnabled(true);
            }

        }

        public void InsertOrUpdate(int MonthID, int schoolID, int academicYearID, int classID, int educationLevelID, int semesterID, int evaluationID, int evaluationCriteriaID, int typeOfTeacher, List<EvaluationComments> listInsertOrUpdate, ref List<ActionAuditDataBO> lstActionAuditData)
        {
            try
            {
                #region Tao du lieu ghi log
                StringBuilder objectIDStrtmp = new StringBuilder();
                StringBuilder descriptionStrtmp = new StringBuilder();
                StringBuilder paramsStrtmp = new StringBuilder();
                StringBuilder oldObjectStrtmp = new StringBuilder();
                StringBuilder newObjectStrtmp = new StringBuilder();
                StringBuilder userFuntionsStrtmp = new StringBuilder();
                StringBuilder userActionsStrtmp = new StringBuilder();
                StringBuilder userDescriptionsStrtmp = new StringBuilder();
                ActionAuditDataBO objAc = null;
                List<int> lstPupilID = listInsertOrUpdate.Select(p => p.PupilID).Distinct().ToList();
                bool isChange = false;

                List<PupilOfClassBO> lstPOC = (from poc in PupilOfClassBusiness.All
                                               join pf in PupilProfileBusiness.All on poc.PupilID equals pf.PupilProfileID
                                               join cp in ClassProfileBusiness.All on poc.ClassID equals cp.ClassProfileID
                                               where poc.AcademicYearID == academicYearID
                                               && poc.SchoolID == schoolID
                                               && poc.ClassID == classID
                                               && lstPupilID.Contains(poc.PupilID)
                                               && cp.IsActive.Value
                                               select new PupilOfClassBO
                                               {
                                                   PupilID = poc.PupilID,
                                                   PupilCode = pf.PupilCode,
                                                   ClassID = poc.ClassID,
                                                   ClassName = cp.DisplayName,
                                                   PupilFullName = pf.FullName
                                               }).ToList();
                PupilOfClassBO objPOC = new PupilOfClassBO();

                SubjectCat objSC = SubjectCatBusiness.Find(evaluationCriteriaID);
                string evaluationName = evaluationID == GlobalConstants.TAB_EDUCATION_BOOKMARK_PRIMARY ? "Môn học & HĐGD" :
                                        evaluationID == GlobalConstants.TAB_CAPACTIES_BOOKMARK_PRIMARY ? "Năng lực" : "Phẩm chất";
                #endregion

                SetAutoDetectChangesEnabled(false);
                IDictionary<string, object> objDic = new Dictionary<string, object>()
            {
                {"SchoolID",schoolID},
                {"AcademicYearID",academicYearID},
                {"ClassID",classID}
            };
                LockMarkPrimary objLockMarkPrimary = LockMarkPrimaryBusiness.SearchLockedMark(objDic).ToList().FirstOrDefault();
                string LockMarkTitle = objLockMarkPrimary != null ? objLockMarkPrimary.LockPrimary : "";

                int partitionNumber = UtilsBusiness.GetPartionId(schoolID);
                AcademicYear objAcadimicYear = AcademicYearBusiness.Find(academicYearID);
                bool isMoveHistory = UtilsBusiness.IsMoveHistory(objAcadimicYear);
                //Neu la nam hoc da chuyen vao du lieu lich su them moi cap nhat du lieu trong lich su
                if (isMoveHistory)
                {
                    EvaluationCommentsHistoryBusiness.InsertOrUpdateHistory(MonthID, schoolID, academicYearID, classID, educationLevelID, semesterID, evaluationID, evaluationCriteriaID, typeOfTeacher, listInsertOrUpdate, ref lstActionAuditData);
                    return;
                }
                List<EvaluationComments> evaluationCommentsList = EvaluationCommentsBusiness.All.Where(p => p.LastDigitSchoolID == partitionNumber

                                                                              && p.ClassID == classID
                                                                              && p.AcademicYearID == academicYearID
                                                                              && p.SemesterID == semesterID
                                                                              && p.EvaluationID == evaluationID
                                                                              && (p.EvaluationCriteriaID == evaluationCriteriaID || evaluationCriteriaID == 0)
                                                                              && p.TypeOfTeacher == typeOfTeacher).ToList();

                List<EvaluationComments> insertList = new List<EvaluationComments>();
                List<EvaluationComments> updateList = new List<EvaluationComments>();
                EvaluationComments checkInlist = null;
                EvaluationComments evaluationCommentsObj;
                bool lockT1 = UtilsBusiness.CheckLockMarkBySemester(semesterID, LockMarkTitle, "T1", "");
                bool lockT2 = UtilsBusiness.CheckLockMarkBySemester(semesterID, LockMarkTitle, "T2", "");
                bool lockT3 = UtilsBusiness.CheckLockMarkBySemester(semesterID, LockMarkTitle, "T3", "");
                bool lockT4 = UtilsBusiness.CheckLockMarkBySemester(semesterID, LockMarkTitle, "T4", "");
                bool lockT5 = UtilsBusiness.CheckLockMarkBySemester(semesterID, LockMarkTitle, "T5", "");

                for (int i = 0; i < listInsertOrUpdate.Count; i++)
                {
                    isChange = false;
                    evaluationCommentsObj = listInsertOrUpdate[i];
                    long pupilID = evaluationCommentsObj.PupilID;
                    string strMonth = string.Empty;
                    checkInlist = evaluationCommentsList.Where(p => p.PupilID == pupilID).FirstOrDefault();
                    objPOC = lstPOC.Where(p => p.PupilID == pupilID).FirstOrDefault();

                    objectIDStrtmp.Append(pupilID);
                    descriptionStrtmp.Append("Cập nhật sổ TD CLGD GVBM");
                    paramsStrtmp.Append(MonthID + ", " + schoolID + ", " + academicYearID + ", " + classID + ", " + educationLevelID + ", " + semesterID + ", " + evaluationID + ", " + evaluationCriteriaID + ", " + typeOfTeacher);
                    userFuntionsStrtmp.Append("Sổ TD CLGD (GVBM)");
                    userActionsStrtmp.Append(SMAS.Business.Common.GlobalConstants.ACTION_UPDATE);
                    userDescriptionsStrtmp.Append("Cập nhật đánh giá " + evaluationName + " HS " + objPOC.PupilFullName + ", " + "mã " + objPOC.PupilCode + ", ");
                    userDescriptionsStrtmp.Append("Lớp ").Append(objPOC.ClassName + "/" + objSC.DisplayName + "/" + "Tháng " + MonthID + "/Học kỳ " + semesterID + "/Năm học " + objAcadimicYear.Year + "-" + (objAcadimicYear.Year + 1) + ". ");


                    if (checkInlist == null)
                    {
                        evaluationCommentsObj.EvaluationCommentsID = EvaluationCommentsRepository.GetNextSeq<long>();
                        if (MonthID == GlobalConstants.MonthID_1 || MonthID == GlobalConstants.MonthID_6)
                        {
                            if (!string.IsNullOrEmpty(evaluationCommentsObj.CommentsM1M6))
                            {
                                newObjectStrtmp.Append(CommentStr + evaluationCommentsObj.CommentsM1M6).Append("; ");
                                isChange = true;
                            }
                        }
                        else if (MonthID == GlobalConstants.MonthID_2 || MonthID == GlobalConstants.MonthID_7)
                        {
                            if (!string.IsNullOrEmpty(evaluationCommentsObj.CommentsM2M7))
                            {
                                newObjectStrtmp.Append(CommentStr + evaluationCommentsObj.CommentsM2M7).Append("; ");
                                isChange = true;
                            }
                        }
                        else if (MonthID == GlobalConstants.MonthID_3 || MonthID == GlobalConstants.MonthID_8)
                        {
                            if (!string.IsNullOrEmpty(evaluationCommentsObj.CommentsM3M8))
                            {
                                newObjectStrtmp.Append(CommentStr + evaluationCommentsObj.CommentsM3M8).Append("; ");
                                isChange = true;
                            }
                        }
                        else if (MonthID == GlobalConstants.MonthID_4 || MonthID == GlobalConstants.MonthID_9)
                        {
                            if (!string.IsNullOrEmpty(evaluationCommentsObj.CommentsM4M9))
                            {
                                newObjectStrtmp.Append(CommentStr + evaluationCommentsObj.CommentsM4M9).Append("; ");
                                isChange = true;
                            }
                        }
                        else if (MonthID == GlobalConstants.MonthID_5 || MonthID == GlobalConstants.MonthID_10)
                        {
                            if (!string.IsNullOrEmpty(evaluationCommentsObj.CommentsM5M10))
                            {
                                newObjectStrtmp.Append(CommentStr + evaluationCommentsObj.CommentsM5M10).Append("; ");
                                isChange = true;
                            }
                        }
                        if (isChange)
                        {
                            evaluationCommentsObj.CreateTime = DateTime.Now;
                            evaluationCommentsObj.UpdateTime = null;
                            oldObjectStrtmp.Append(CommentStr).Append("; ");
                            insertList.Add(evaluationCommentsObj);
                        }
                    }
                    else
                    {
                        //update thong tin

                        if ((MonthID == GlobalConstants.MonthID_1 || MonthID == GlobalConstants.MonthID_6) && !lockT1
                            && checkInlist.CommentsM1M6 != evaluationCommentsObj.CommentsM1M6)
                        {
                            if (!string.IsNullOrEmpty(checkInlist.CommentsM1M6))
                            {
                                if (!checkInlist.CommentsM1M6.Equals(evaluationCommentsObj.CommentsM1M6))
                                {
                                    strMonth += (MonthID == GlobalConstants.MonthID_1) ? "T1," : "T6,";
                                    isChange = true;
                                    newObjectStrtmp.Append(CommentStr + evaluationCommentsObj.CommentsM1M6).Append("; ");
                                    oldObjectStrtmp.Append(CommentStr + checkInlist.CommentsM1M6).Append("; ");
                                    checkInlist.CommentsM1M6 = evaluationCommentsObj.CommentsM1M6;
                                }
                            }
                            else if (!string.IsNullOrEmpty(evaluationCommentsObj.CommentsM1M6))
                            {
                                strMonth += (MonthID == GlobalConstants.MonthID_1) ? "T1," : "T6,";
                                isChange = true;
                                newObjectStrtmp.Append(CommentStr + evaluationCommentsObj.CommentsM1M6).Append("; ");
                                oldObjectStrtmp.Append(CommentStr + checkInlist.CommentsM1M6).Append("; ");
                                checkInlist.CommentsM1M6 = evaluationCommentsObj.CommentsM1M6;
                            }
                        }
                        else if ((MonthID == GlobalConstants.MonthID_2 || MonthID == GlobalConstants.MonthID_7) && !lockT2)
                        {
                            if (!string.IsNullOrEmpty(checkInlist.CommentsM2M7))
                            {
                                if (!checkInlist.CommentsM2M7.Equals(evaluationCommentsObj.CommentsM2M7))
                                {
                                    strMonth += (MonthID == GlobalConstants.MonthID_2) ? "T2," : "T7,";
                                    isChange = true;
                                    newObjectStrtmp.Append(CommentStr + evaluationCommentsObj.CommentsM2M7).Append("; ");
                                    oldObjectStrtmp.Append(CommentStr + checkInlist.CommentsM2M7).Append("; ");
                                    checkInlist.CommentsM2M7 = evaluationCommentsObj.CommentsM2M7;
                                }
                            }
                            else if (!string.IsNullOrEmpty(evaluationCommentsObj.CommentsM2M7))
                            {
                                strMonth += (MonthID == GlobalConstants.MonthID_2) ? "T2," : "T7,";
                                isChange = true;
                                newObjectStrtmp.Append(CommentStr + evaluationCommentsObj.CommentsM2M7).Append("; ");
                                oldObjectStrtmp.Append(CommentStr + checkInlist.CommentsM2M7).Append("; ");
                                checkInlist.CommentsM2M7 = evaluationCommentsObj.CommentsM2M7;
                            }
                        }
                        else if ((MonthID == GlobalConstants.MonthID_3 || MonthID == GlobalConstants.MonthID_8) && !lockT3)
                        {
                            if (!string.IsNullOrEmpty(checkInlist.CommentsM3M8))
                            {
                                if (!checkInlist.CommentsM3M8.Equals(evaluationCommentsObj.CommentsM3M8))
                                {
                                    strMonth += (MonthID == GlobalConstants.MonthID_3) ? "T3," : "T8,";
                                    isChange = true;
                                    newObjectStrtmp.Append(CommentStr + evaluationCommentsObj.CommentsM3M8).Append("; ");
                                    oldObjectStrtmp.Append(CommentStr + checkInlist.CommentsM3M8).Append("; ");
                                    checkInlist.CommentsM3M8 = evaluationCommentsObj.CommentsM3M8;
                                }
                            }
                            else if (!string.IsNullOrEmpty(evaluationCommentsObj.CommentsM3M8))
                            {
                                strMonth += (MonthID == GlobalConstants.MonthID_3) ? "T3," : "T8,";
                                isChange = true;
                                newObjectStrtmp.Append(CommentStr + evaluationCommentsObj.CommentsM3M8).Append("; ");
                                oldObjectStrtmp.Append(CommentStr + checkInlist.CommentsM3M8).Append("; ");
                                checkInlist.CommentsM3M8 = evaluationCommentsObj.CommentsM3M8;
                            }
                        }
                        else if ((MonthID == GlobalConstants.MonthID_4 || MonthID == GlobalConstants.MonthID_9) && !lockT4)
                        {
                            if (!string.IsNullOrEmpty(checkInlist.CommentsM4M9))
                            {
                                if (!checkInlist.CommentsM4M9.Equals(evaluationCommentsObj.CommentsM4M9))
                                {
                                    strMonth += (MonthID == GlobalConstants.MonthID_4) ? "T4," : "T9,";
                                    isChange = true;
                                    newObjectStrtmp.Append(CommentStr + evaluationCommentsObj.CommentsM4M9).Append("; ");
                                    oldObjectStrtmp.Append(CommentStr + checkInlist.CommentsM4M9).Append("; ");
                                    checkInlist.CommentsM4M9 = evaluationCommentsObj.CommentsM4M9;
                                }
                            }
                            else if (!string.IsNullOrEmpty(evaluationCommentsObj.CommentsM4M9))
                            {
                                strMonth += (MonthID == GlobalConstants.MonthID_4) ? "T4," : "T9,";
                                isChange = true;
                                newObjectStrtmp.Append(CommentStr + evaluationCommentsObj.CommentsM4M9).Append("; ");
                                oldObjectStrtmp.Append(CommentStr + checkInlist.CommentsM4M9).Append("; ");
                                checkInlist.CommentsM4M9 = evaluationCommentsObj.CommentsM4M9;
                            }
                        }
                        else if ((MonthID == GlobalConstants.MonthID_5 || MonthID == GlobalConstants.MonthID_10) && !lockT5)
                        {
                            if (!string.IsNullOrEmpty(checkInlist.CommentsM5M10))
                            {
                                if (!checkInlist.CommentsM5M10.Equals(evaluationCommentsObj.CommentsM5M10))
                                {
                                    strMonth += (MonthID == GlobalConstants.MonthID_5) ? "T5," : "T10,";
                                    isChange = true;
                                    newObjectStrtmp.Append(CommentStr + evaluationCommentsObj.CommentsM5M10).Append("; ");
                                    oldObjectStrtmp.Append(CommentStr + checkInlist.CommentsM5M10).Append("; ");
                                    checkInlist.CommentsM5M10 = evaluationCommentsObj.CommentsM5M10;
                                }
                            }
                            else if (!string.IsNullOrEmpty(evaluationCommentsObj.CommentsM5M10))
                            {
                                strMonth += (MonthID == GlobalConstants.MonthID_5) ? "T5," : "T10,";
                                isChange = true;
                                newObjectStrtmp.Append(CommentStr + evaluationCommentsObj.CommentsM5M10).Append("; ");
                                oldObjectStrtmp.Append(CommentStr + checkInlist.CommentsM5M10).Append("; ");
                                checkInlist.CommentsM5M10 = evaluationCommentsObj.CommentsM5M10;
                            }
                        }
                        if (isChange)
                        {
                            checkInlist.UpdateTime = DateTime.Now;
                            checkInlist.NoteUpdate = strMonth + "," + DateTime.Now.ToString() + "|";
                            updateList.Add(checkInlist);
                        }
                    }
                    objAc = new ActionAuditDataBO();
                    objAc.PupilID = pupilID;
                    objAc.ClassID = classID;
                    objAc.EvaluationID = evaluationID;
                    objAc.SubjectID = evaluationCriteriaID;
                    objAc.ObjID = objectIDStrtmp;
                    objAc.Parameter = paramsStrtmp;
                    objAc.OldData = oldObjectStrtmp;
                    objAc.NewData = newObjectStrtmp;
                    objAc.UserAction = userActionsStrtmp;
                    objAc.UserFunction = userFuntionsStrtmp;
                    objAc.UserDescription = userDescriptionsStrtmp;
                    objAc.Description = descriptionStrtmp;
                    objAc.isInsertLog = isChange;
                    lstActionAuditData.Add(objAc);


                    objectIDStrtmp = new StringBuilder();
                    descriptionStrtmp = new StringBuilder();
                    paramsStrtmp = new StringBuilder();
                    oldObjectStrtmp = new StringBuilder();
                    newObjectStrtmp = new StringBuilder();
                    userFuntionsStrtmp = new StringBuilder();
                    userActionsStrtmp = new StringBuilder();
                    userDescriptionsStrtmp = new StringBuilder();
                }

                if (insertList != null && insertList.Count > 0)
                {
                    for (int i = 0; i < insertList.Count; i++)
                    {

                        EvaluationCommentsBusiness.Insert(insertList[i]);
                    }
                }
                if (updateList != null && updateList.Count > 0)
                {
                    for (int i = 0; i < updateList.Count; i++)
                    {
                        EvaluationCommentsBusiness.Update(updateList[i]);
                    }
                }
                EvaluationCommentsBusiness.Save();
            }
            catch (Exception ex)
            {
                
                string paramList = string.Format("MonthID={0}, schoolID={1}, academicYearID={2}, classID={3}, educationLevelID={4}, semesterID={5}, evaluationID={6}, evaluationCriteriaID={7}, typeOfTeacher={8}", MonthID, schoolID, academicYearID, classID, educationLevelID, semesterID, evaluationID, evaluationCriteriaID, typeOfTeacher);
                LogExtensions.ErrorExt(logger, DateTime.Now, "InsertOrUpdate",paramList, ex);
            }
            finally
            {
                SetAutoDetectChangesEnabled(true);
            }
        }

        public List<EvaluationCommentsReportBO> GetAllEvaluationCommentsbyClass(int classId, IDictionary<string, object> dic, List<int> lstSubject)
        {
            int academicYearId = Utils.GetInt(dic["AcademicYearId"]);
            int semesterId = Utils.GetInt(dic["SemesterId"]);
            int typeOfTeacher = Utils.GetInt(dic["TypeOfTeacher"]);
            int schoolId = Utils.GetInt(dic["SchoolId"]);
            bool isSemester = Utils.GetBool(dic, "isSemester");
            int partitionId = UtilsBusiness.GetPartionId(schoolId);
            AcademicYear objAcademicYear = AcademicYearBusiness.Find(academicYearId);
            if (UtilsBusiness.IsMoveHistory(objAcademicYear))//Neu da chuyen vao bang lich su thi lay du lieu tu bang lich su
            {
                List<EvaluationCommentsReportBO> listResultHistory = EvaluationCommentsHistoryBusiness.GetAllEvaluationCommentsHistorybyClass(classId, dic, lstSubject);
                return listResultHistory;
            }

            //list hoc sinh trong lop
            List<PupilOfClassBO> pupilOfClassList = PupilOfClassBusiness.GetPupilInClass(classId).ToList();
            //list hoc sinh duoc danh gia.
            List<EvaluationComments> listEvaluationComments = new List<EvaluationComments>();
            if (typeOfTeacher == GlobalConstants.EVALUTION_TEACHER_SUBJECT)//GVBM
            {
                listEvaluationComments = (from ev in EvaluationCommentsBusiness.All
                                          where
                                          ev.LastDigitSchoolID == partitionId
                                          && ev.AcademicYearID == academicYearId
                                           && ev.ClassID == classId
                                           && ev.SemesterID == semesterId
                                           && ev.TypeOfTeacher == typeOfTeacher
                                          && lstSubject.Contains(ev.EvaluationCriteriaID)
                                          select ev).ToList();
            }
            else if (typeOfTeacher == GlobalConstants.EVALUTION_HEAD_TEACHER)
            {
                listEvaluationComments = (from ev in EvaluationCommentsBusiness.All
                                          where
                                          ev.LastDigitSchoolID == partitionId
                                          && ev.AcademicYearID == academicYearId
                                           && ev.ClassID == classId
                                           && ev.SemesterID == semesterId
                                           && ev.TypeOfTeacher == typeOfTeacher
                                          select ev).ToList();
            }

            //Danh gia nhan xet cuoi ky
            List<SummedEvaluation> listSummedEvaluation = new List<SummedEvaluation>();
            //Neu import cho ca hoc ky thi moi lay du lieu cuoi ky
            if (typeOfTeacher == GlobalConstants.EVALUTION_TEACHER_SUBJECT && isSemester)
            {
                listSummedEvaluation = (from ev in SummedEvaluationBusiness.All
                                        where
                                         ev.LastDigitSchoolID == partitionId
                                         && ev.AcademicYearID == academicYearId
                                         && ev.ClassID == classId
                                         && ev.SemesterID == semesterId
                                        && lstSubject.Contains(ev.EvaluationCriteriaID)
                                        select ev).ToList();
            }
            else if (typeOfTeacher == GlobalConstants.EVALUTION_HEAD_TEACHER && isSemester)
            {
                listSummedEvaluation = (from ev in SummedEvaluationBusiness.All
                                        where
                                         ev.LastDigitSchoolID == partitionId
                                         && ev.AcademicYearID == academicYearId
                                         && ev.ClassID == classId
                                         && ev.SemesterID == semesterId
                                        select ev).ToList();
            }
            //Neu la GVCN thi lay danh gia nhat xet cuoi ky cho mat danh gia Nang luc, pham chat
            List<SummedEndingEvaluation> lstSummedEndingEvaluation = new List<SummedEndingEvaluation>();
            if (typeOfTeacher == GlobalConstants.EVALUTION_HEAD_TEACHER && isSemester)
            {
                lstSummedEndingEvaluation = SummedEndingEvaluationBusiness.All.Where(c => c.LastDigitSchoolID == partitionId
                                                                                     && c.AcademicYearID == academicYearId
                                                                                     && c.ClassID == classId
                                                                                     && c.SemesterID == semesterId).ToList();
            }
            //list hoc sinh cua lop da duoc danh gia va chua danh gia
            List<EvaluationCommentsReportBO> listResult = new List<EvaluationCommentsReportBO>();
            EvaluationCommentsReportBO evaluationBO = null;
            EvaluationComments objEducation;
            EvaluationComments objCapacities;
            EvaluationComments objQualities;
            SummedEvaluation objSummedEvaluationEducation = null;
            SummedEndingEvaluation objSummedEndingEvaluation = null;
            int pupilID = 0;
            PupilOfClassBO pocObj;
            if (pupilOfClassList == null || pupilOfClassList.Count == 0)
            {
                return new List<EvaluationCommentsReportBO>();
            }
            int subjectId = 0;
            List<EvaluationComments> listEvaluationEducation = new List<EvaluationComments>();//Mon hoc va HDGD
            List<EvaluationComments> listEvaluationCapacities = new List<EvaluationComments>();//Nang luc
            List<EvaluationComments> listEvaluationQualities = new List<EvaluationComments>();//Nang luc
            List<SummedEvaluation> listSummedEvaluationbySubject = new List<SummedEvaluation>();
            IDictionary<int, EvaluationComments> dicEvaluation;
            IDictionary<int, SummedEvaluation> dicSummedEvaluation;
            IDictionary<int, SummedEndingEvaluation> dicSummedEndingEvaluation;
            for (int sc = 0; sc < lstSubject.Count; sc++)
            {
                subjectId = lstSubject[sc];

                listEvaluationEducation = listEvaluationComments.Where(e => e.EvaluationCriteriaID == subjectId && e.EvaluationID == GlobalConstants.TAB_EDUCATION_BOOKMARK_PRIMARY).ToList();
                listEvaluationCapacities = listEvaluationComments.Where(e => e.EvaluationCriteriaID == subjectId && e.EvaluationID == GlobalConstants.TAB_CAPACTIES_BOOKMARK_PRIMARY).ToList();
                listEvaluationQualities = listEvaluationComments.Where(e => e.EvaluationCriteriaID == subjectId && e.EvaluationID == GlobalConstants.TAB_QUALITIES_BOOKMARK_PRIMARY).ToList();
                listSummedEvaluationbySubject = listSummedEvaluation.Where(e => e.EvaluationCriteriaID == subjectId).ToList();
                for (int i = 0; i < pupilOfClassList.Count; i++)
                {
                    pocObj = pupilOfClassList[i];
                    pupilID = pocObj.PupilID;
                    evaluationBO = new EvaluationCommentsReportBO()
                    {
                        Birthday = pocObj.Birthday,
                        FullName = pocObj.PupilFullName,
                        Name = pocObj.Name,
                        OrderInClass = pocObj.OrderInClass ?? 0,
                        PupilCode = pocObj.PupilCode,
                        PupilID = pocObj.PupilID,
                        Status = pocObj.Status,
                        EvaluationCriteriaID = subjectId
                    };
                    objEducation = listEvaluationEducation.FirstOrDefault(e => e.PupilID == pupilID);
                    objCapacities = listEvaluationCapacities.FirstOrDefault(e => e.PupilID == pupilID);
                    objQualities = listEvaluationQualities.FirstOrDefault(e => e.PupilID == pupilID);
                    dicEvaluation = new Dictionary<int, EvaluationComments>();
                    dicEvaluation.Add(GlobalConstants.TAB_EDUCATION_BOOKMARK_PRIMARY, objEducation);
                    dicEvaluation.Add(GlobalConstants.TAB_CAPACTIES_BOOKMARK_PRIMARY, objCapacities);
                    dicEvaluation.Add(GlobalConstants.TAB_QUALITIES_BOOKMARK_PRIMARY, objQualities);
                    evaluationBO.DicEvaluation = dicEvaluation;
                    //Lay nhan xet cuoi ky, 
                    objSummedEvaluationEducation = listSummedEvaluationbySubject.FirstOrDefault(s => s.PupilID == pupilID && s.EvaluationID == GlobalConstants.TAB_EDUCATION_BOOKMARK_PRIMARY);

                    dicSummedEvaluation = new Dictionary<int, SummedEvaluation>();
                    dicSummedEvaluation.Add(GlobalConstants.TAB_EDUCATION_BOOKMARK_PRIMARY, objSummedEvaluationEducation);
                    evaluationBO.DicSummedEvaluation = dicSummedEvaluation;
                    /*if (typeOfTeacher == 2 && lstSummedEndingEvaluation.Count > 0)
                    {
                        dicSummedEndingEvaluation = new Dictionary<int, SummedEndingEvaluation>();
                        //nang luc
                        objSummedEndingEvaluation = lstSummedEndingEvaluation.FirstOrDefault(s => s.PupilID == pupilID && s.EvaluationID == GlobalConstants.TAB_CAPACTIES_BOOKMARK_PRIMARY);
                        dicSummedEndingEvaluation.Add(GlobalConstants.TAB_CAPACTIES_BOOKMARK_PRIMARY, objSummedEndingEvaluation);

                        objSummedEndingEvaluation = lstSummedEndingEvaluation.FirstOrDefault(s => s.PupilID == pupilID && s.EvaluationID == GlobalConstants.TAB_QUALITIES_BOOKMARK_PRIMARY);
                        dicSummedEndingEvaluation.Add(GlobalConstants.TAB_QUALITIES_BOOKMARK_PRIMARY, objSummedEndingEvaluation);
                        evaluationBO.DicSummedEndingEvaluation = dicSummedEndingEvaluation;
                    }*/
                    listResult.Add(evaluationBO);
                }
            }

            //Lay du lieu nang luc, pham chat cuoi ky theo tieu chi danh gia cho GVCN, EvaluationCriteriaID=-1;
            if (isSemester && typeOfTeacher == GlobalConstants.EVALUTION_HEAD_TEACHER)
            {

                List<EvaluationCriteria> lstEC = EvaluationCriteriaBusiness.getEvaluationCriteria();
                EvaluationCriteria objEvaluationCriteria;
                SummedEvaluation objSummedEvaluationCapacities = null;
                for (int i = 0; i < pupilOfClassList.Count; i++)
                {
                    pocObj = pupilOfClassList[i];
                    pupilID = pocObj.PupilID;
                    evaluationBO = new EvaluationCommentsReportBO()
                    {
                        Birthday = pocObj.Birthday,
                        FullName = pocObj.PupilFullName,
                        Name = pocObj.Name,
                        OrderInClass = pocObj.OrderInClass ?? 0,
                        PupilCode = pocObj.PupilCode,
                        PupilID = pocObj.PupilID,
                        Status = pocObj.Status,
                        EvaluationCriteriaID = -1
                    };
                    dicSummedEvaluation = new Dictionary<int, SummedEvaluation>();
                    for (int j = 0; j < lstEC.Count; j++)
                    {
                        objEvaluationCriteria = lstEC[j];
                        objSummedEvaluationCapacities = listSummedEvaluation.FirstOrDefault(e => e.PupilID == pupilID && e.EvaluationCriteriaID == objEvaluationCriteria.EvaluationCriteriaID);
                        dicSummedEvaluation.Add(objEvaluationCriteria.EvaluationCriteriaID, objSummedEvaluationCapacities);
                    }
                    evaluationBO.CapacitesOrQualitiesSemester = dicSummedEvaluation;
                    if (lstSummedEndingEvaluation.Count > 0)
                    {
                        dicSummedEndingEvaluation = new Dictionary<int, SummedEndingEvaluation>();
                        //nang luc
                        objSummedEndingEvaluation = lstSummedEndingEvaluation.FirstOrDefault(s => s.PupilID == pupilID && s.EvaluationID == GlobalConstants.TAB_CAPACTIES_BOOKMARK_PRIMARY);
                        dicSummedEndingEvaluation.Add(GlobalConstants.TAB_CAPACTIES_BOOKMARK_PRIMARY, objSummedEndingEvaluation);

                        objSummedEndingEvaluation = lstSummedEndingEvaluation.FirstOrDefault(s => s.PupilID == pupilID && s.EvaluationID == GlobalConstants.TAB_QUALITIES_BOOKMARK_PRIMARY);
                        dicSummedEndingEvaluation.Add(GlobalConstants.TAB_QUALITIES_BOOKMARK_PRIMARY, objSummedEndingEvaluation);
                        evaluationBO.DicSummedEndingEvaluation = dicSummedEndingEvaluation;
                    }
                    listResult.Add(evaluationBO);
                }
            }
            return listResult;
        }

        public void InsertOrUpdatebyClass(int schoolID, int academicYearID, int classID, int educationLevelID, int semesterID, int typeOfTeacher, List<EvaluationComments> listInsertOrUpdate)
        {
            try
            {

                this.context.Configuration.AutoDetectChangesEnabled = false;
                this.context.Configuration.ValidateOnSaveEnabled = false;
                AcademicYear objAcadimicYear = AcademicYearBusiness.Find(academicYearID);
                bool isMoveHistory = UtilsBusiness.IsMoveHistory(objAcadimicYear);
                //Neu la nam hoc da chuyen vao du lieu lich su them moi cap nhat du lieu trong lich su
                if (isMoveHistory)
                {
                    EvaluationCommentsHistoryBusiness.InsertOrUpdateHistorybyClass(schoolID, academicYearID, classID, educationLevelID, semesterID, typeOfTeacher, listInsertOrUpdate);
                    return;
                }

                IDictionary<string, object> objDic = new Dictionary<string, object>()
            {
                {"SchoolID",schoolID},
                {"AcademicYearID",academicYearID},
                {"ClassID",classID}
            };
                LockMarkPrimary objLockMarkPrimary = LockMarkPrimaryBusiness.SearchLockedMark(objDic).ToList().FirstOrDefault();
                string LockMarkTitle = objLockMarkPrimary != null ? objLockMarkPrimary.LockPrimary : "";
                int partitionNumber = UtilsBusiness.GetPartionId(schoolID);
                List<EvaluationComments> evaluationCommentsList = EvaluationCommentsBusiness.All.Where(p => p.LastDigitSchoolID == partitionNumber
                                                                              && p.ClassID == classID
                                                                              && p.AcademicYearID == academicYearID
                                                                              && p.SemesterID == semesterID
                                                                              && p.TypeOfTeacher == typeOfTeacher).ToList();

                List<EvaluationComments> insertList = new List<EvaluationComments>();
                List<EvaluationComments> updateList = new List<EvaluationComments>();
                EvaluationComments checkInlist = null;
                EvaluationComments evaluationCommentsObj;
                bool lockT1 = UtilsBusiness.CheckLockMarkBySemester(semesterID, LockMarkTitle, "T1", "");
                bool lockT2 = UtilsBusiness.CheckLockMarkBySemester(semesterID, LockMarkTitle, "T2", "");
                bool lockT3 = UtilsBusiness.CheckLockMarkBySemester(semesterID, LockMarkTitle, "T3", "");
                bool lockT4 = UtilsBusiness.CheckLockMarkBySemester(semesterID, LockMarkTitle, "T4", "");
                bool lockT5 = UtilsBusiness.CheckLockMarkBySemester(semesterID, LockMarkTitle, "T5", "");

                for (int i = 0; i < listInsertOrUpdate.Count; i++)
                {
                    evaluationCommentsObj = listInsertOrUpdate[i];
                    //Kiem tra du lieu cua hoc sinh theo mat danh gia, mon hoc da co dl trong DB
                    checkInlist = evaluationCommentsList.Where(p => p.PupilID == evaluationCommentsObj.PupilID &&
                                                                p.EvaluationID == evaluationCommentsObj.EvaluationID &&
                                                                p.EvaluationCriteriaID == evaluationCommentsObj.EvaluationCriteriaID).FirstOrDefault();
                    if (checkInlist == null)
                    {
                        evaluationCommentsObj.EvaluationCommentsID = EvaluationCommentsBusiness.GetNextSeq<long>();
                        evaluationCommentsObj.CreateTime = DateTime.Now;
                        insertList.Add(evaluationCommentsObj);
                    }
                    else
                    {

                        //update thong tin
                        if (!lockT1)
                        {
                            checkInlist.CommentsM1M6 = evaluationCommentsObj.CommentsM1M6;
                        }
                        if (!lockT2)
                        {
                            checkInlist.CommentsM2M7 = evaluationCommentsObj.CommentsM2M7;
                        }
                        if (!lockT3)
                        {
                            checkInlist.CommentsM3M8 = evaluationCommentsObj.CommentsM3M8;
                        }
                        if (!lockT4)
                        {
                            checkInlist.CommentsM4M9 = evaluationCommentsObj.CommentsM4M9;
                        }
                        if (!lockT5)
                        {
                            checkInlist.CommentsM5M10 = evaluationCommentsObj.CommentsM5M10;
                        }
                        checkInlist.UpdateTime = DateTime.Now;
                        updateList.Add(checkInlist);
                    }
                }

                IDictionary<string, object> columnMap = ColumnMappings();
                if (insertList != null && insertList.Count > 0)
                {
                    //BulkInsert(insertList, columnMap);
                    for (int i = 0; i < insertList.Count; i++)
                    {
                        this.Insert(insertList[i]);
                    }
                }
                if (updateList != null && updateList.Count > 0)
                {
                    /*List<string> columnWhere = new List<string>();
                columnWhere.Add("LAST_DIGIT_SCHOOL_ID");
                columnWhere.Add("EVALUATION_COMMENTS_ID");
                    BulkUpdate(updateList, columnMap, columnWhere);*/
                    for (int i = 0; i < updateList.Count; i++)
                    {
                        this.Update(updateList[i]);
                    }
                }
                this.Save();
            }
            catch (Exception ex)
            {
                
                string paramList = string.Format("schoolID={0}, academicYearID={1}, classID={2}, educationLevelID={3}, semesterID={4}, typeOfTeacher={5}", schoolID, academicYearID, classID, educationLevelID, semesterID, typeOfTeacher);
                LogExtensions.ErrorExt(logger, DateTime.Now, "InsertOrUpdatebyClass", paramList, ex);
            }
            finally
            {
                this.context.Configuration.AutoDetectChangesEnabled = true;
                this.context.Configuration.ValidateOnSaveEnabled = true;
            }
        }
        /// <summary>
        /// Xoa du lieu nhan xet danh gia
        /// </summary>
        /// <param name="IDList"></param>
        /// <param name="dic"></param>
        /// <param name="isRetest"></param>
        public void BulkDeleteEvaluationComments(List<long> IDList, IDictionary<string, object> dic, bool isRetest, ref List<ActionAuditDataBO> lstActionAuditData)
        {
            try
            {
                context.Configuration.AutoDetectChangesEnabled = false;
                context.Configuration.ValidateOnSaveEnabled = false;
                // Fix lỗi ATTT
                // if (IDList == null && IDList.Count == 0)
                if (IDList == null || !IDList.Any())
                {
                    return;
                }
                int classId = Utils.GetInt(dic["ClassId"]);
                int educationLevelId = Utils.GetInt(dic["EducationLevelId"]);
                int semesterId = Utils.GetInt(dic["SemesterId"]);
                int evaluationId = Utils.GetInt(dic["EvaluationId"]);
                int evaluationCriteriaID = Utils.GetInt(dic["EvaluationCriteriaID"]);
                int typeOfTeacher = Utils.GetInt(dic["TypeOfTeacher"]);
                int monthId = Utils.GetInt(dic["MonthId"]);
                int schoolId = Utils.GetInt(dic["SchoolID"]);
                int partitionId = UtilsBusiness.GetPartionId(schoolId);
                int academicYearId = Utils.GetInt(dic["AcademicYearID"]);
                string LockMarkTitle = Utils.GetString(dic["LockMarkTitle"]);
                int pupilID = 0;
                bool lockT1 = UtilsBusiness.CheckLockMarkBySemester(semesterId, LockMarkTitle, "T1", "");
                bool lockT2 = UtilsBusiness.CheckLockMarkBySemester(semesterId, LockMarkTitle, "T2", "");
                bool lockT3 = UtilsBusiness.CheckLockMarkBySemester(semesterId, LockMarkTitle, "T3", "");
                bool lockT4 = UtilsBusiness.CheckLockMarkBySemester(semesterId, LockMarkTitle, "T4", "");
                bool lockT5 = UtilsBusiness.CheckLockMarkBySemester(semesterId, LockMarkTitle, "T5", "");
                string ck = (semesterId == GlobalConstants.SEMESTER_OF_YEAR_FIRST) ? "CK1" : "CK2";
                bool lockCK = UtilsBusiness.CheckLockMarkBySemester(semesterId, LockMarkTitle, ck, "");
                #region Tao du lieu ghi log
                StringBuilder objectIDStrtmp = new StringBuilder();
                StringBuilder descriptionStrtmp = new StringBuilder();
                StringBuilder paramsStrtmp = new StringBuilder();
                StringBuilder oldObjectStrtmp = new StringBuilder();
                StringBuilder newObjectStrtmp = new StringBuilder();
                StringBuilder userFuntionsStrtmp = new StringBuilder();
                StringBuilder userActionsStrtmp = new StringBuilder();
                StringBuilder userDescriptionsStrtmp = new StringBuilder();
                ActionAuditDataBO objAc = null;

                List<PupilOfClassBO> lstPOC = (from poc in PupilOfClassBusiness.All
                                               join pf in PupilProfileBusiness.All on poc.PupilID equals pf.PupilProfileID
                                               join cp in ClassProfileBusiness.All on poc.ClassID equals cp.ClassProfileID
                                               where poc.AcademicYearID == academicYearId
                                               && poc.SchoolID == schoolId
                                               && poc.ClassID == classId
                                               && IDList.Contains(poc.PupilID)
                                               && cp.IsActive.Value
                                               select new PupilOfClassBO
                                               {
                                                   PupilID = poc.PupilID,
                                                   PupilCode = pf.PupilCode,
                                                   ClassID = poc.ClassID,
                                                   ClassName = cp.DisplayName,
                                                   PupilFullName = pf.FullName
                                               }).ToList();
                PupilOfClassBO objPOC = new PupilOfClassBO();

                SubjectCat objSC = SubjectCatBusiness.Find(evaluationCriteriaID);
                string evaluationName = evaluationId == GlobalConstants.TAB_EDUCATION_BOOKMARK_PRIMARY ? "Môn học & HĐGD" :
                                        evaluationId == GlobalConstants.TAB_CAPACTIES_BOOKMARK_PRIMARY ? "Năng lực" : "Phẩm chất";
                #endregion

                AcademicYear objAcadimicYear = AcademicYearBusiness.Find(academicYearId);
                bool isMoveHistory = UtilsBusiness.IsMoveHistory(objAcadimicYear);
                //Neu nam hoc dang thuc hien da chuyen di du lieu lich su thi thuc hien them moi cap nhat du lieu vao lich su
                if (isMoveHistory)
                {
                    EvaluationCommentsHistoryBusiness.BulkDeleteEvaluationCommentsHistory(IDList, dic, isRetest, ref lstActionAuditData);
                    return;
                }

                //Lay danh sach trong CSDL de co du lieu chinh xac can xoa.
                // Xoa du lieu thang
                bool isChange = false;
                if (monthId < GlobalConstants.MonthID_11)
                {
                    List<EvaluationComments> lstECDelete = EvaluationCommentsBusiness.All
                        .Where(p => p.LastDigitSchoolID == partitionId
                                 && p.ClassID == classId
                                 && p.AcademicYearID == academicYearId
                                 && p.SemesterID == semesterId
                                 && p.EvaluationID == evaluationId
                                 && (p.EvaluationCriteriaID == evaluationCriteriaID || evaluationCriteriaID == 0)
                                 && p.TypeOfTeacher == typeOfTeacher
                                 && IDList.Contains(p.PupilID)).ToList();

                    EvaluationComments objEC = null;
                    for (int i = 0; i < lstECDelete.Count; i++)
                    {
                        isChange = false;
                        objEC = lstECDelete[i];
                        pupilID = objEC.PupilID;
                        objPOC = lstPOC.Where(p => p.PupilID == pupilID).FirstOrDefault();
                        objectIDStrtmp.Append(pupilID);
                        descriptionStrtmp.Append("Xóa nhận xét sổ TD CLGD GVBM");
                        paramsStrtmp.Append(pupilID);
                        userFuntionsStrtmp.Append("Sổ TD CLGD (GVBM)");
                        userActionsStrtmp.Append(SMAS.Business.Common.GlobalConstants.ACTION_DELETE);
                        userDescriptionsStrtmp.Append("Xóa nhận xét đánh giá " + evaluationName + " HS " + objPOC.PupilFullName + ", " + "mã " + objPOC.PupilCode + ", ");
                        userDescriptionsStrtmp.Append("Lớp ").Append(objPOC.ClassName + "/" + objSC.DisplayName + "/" + "Tháng " + monthId + "/Học kỳ " + semesterId + "/Năm học " + objAcadimicYear.Year + "-" + (objAcadimicYear.Year + 1) + ". ");

                        if ((monthId == GlobalConstants.MonthID_1 || monthId == GlobalConstants.MonthID_6) && !lockT1 && !string.IsNullOrEmpty(objEC.CommentsM1M6))
                        {
                            oldObjectStrtmp.Append(CommentStr + objEC.CommentsM1M6).Append("; ");
                            objEC.CommentsM1M6 = "";
                            isChange = true;
                        }
                        else if ((monthId == GlobalConstants.MonthID_2 || monthId == GlobalConstants.MonthID_7) && !lockT2 && !string.IsNullOrEmpty(objEC.CommentsM2M7))
                        {
                            oldObjectStrtmp.Append(CommentStr + objEC.CommentsM2M7).Append("; ");
                            objEC.CommentsM2M7 = "";
                            isChange = true;
                        }
                        else if ((monthId == GlobalConstants.MonthID_3 || monthId == GlobalConstants.MonthID_8) && !lockT3 && !string.IsNullOrEmpty(objEC.CommentsM3M8))
                        {
                            oldObjectStrtmp.Append(CommentStr + objEC.CommentsM3M8).Append("; ");
                            objEC.CommentsM3M8 = "";
                            isChange = true;
                        }
                        else if ((monthId == GlobalConstants.MonthID_4 || monthId == GlobalConstants.MonthID_9) && !lockT4 && !string.IsNullOrEmpty(objEC.CommentsM4M9))
                        {
                            oldObjectStrtmp.Append(CommentStr + objEC.CommentsM4M9).Append("; ");
                            objEC.CommentsM4M9 = "";
                            isChange = true;
                        }
                        else if ((monthId == GlobalConstants.MonthID_5 || monthId == GlobalConstants.MonthID_10) && !lockT5 && !string.IsNullOrEmpty(objEC.CommentsM5M10))
                        {
                            oldObjectStrtmp.Append(CommentStr + objEC.CommentsM5M10).Append("; ");
                            objEC.CommentsM5M10 = "";
                            isChange = true;
                        }

                        if (isChange)
                        {
                            objAc = new ActionAuditDataBO();
                            objAc.PupilID = pupilID;
                            objAc.ClassID = classId;
                            objAc.EvaluationID = evaluationId;
                            objAc.SubjectID = evaluationCriteriaID;
                            objAc.ObjID = objectIDStrtmp;
                            objAc.Parameter = paramsStrtmp;
                            objAc.OldData = oldObjectStrtmp;
                            objAc.NewData = newObjectStrtmp;
                            objAc.UserAction = userActionsStrtmp;
                            objAc.UserFunction = userFuntionsStrtmp;
                            objAc.UserDescription = userDescriptionsStrtmp;
                            objAc.Description = descriptionStrtmp;
                            lstActionAuditData.Add(objAc);
                        }

                        objectIDStrtmp = new StringBuilder();
                        descriptionStrtmp = new StringBuilder();
                        paramsStrtmp = new StringBuilder();
                        oldObjectStrtmp = new StringBuilder();
                        newObjectStrtmp = new StringBuilder();
                        userFuntionsStrtmp = new StringBuilder();
                        userActionsStrtmp = new StringBuilder();
                        userDescriptionsStrtmp = new StringBuilder();
                        objEC.UpdateTime = DateTime.Now;
                        this.Update(objEC);
                    }
                    this.Save();
                    #region code cu
                    /*
                    if (lstECDelete != null && lstECDelete.Count > 0)
                    {
                        //Thuc hien cap nhat gia tri null cho thang duoc chon
                        if ((monthId == GlobalConstants.MonthID_1 || monthId == GlobalConstants.MonthID_6) && !lockT1)
                        {
                            lstECDelete.ForEach(e => e.CommentsM1M6 = "");
                        }
                        else if ((monthId == GlobalConstants.MonthID_2 || monthId == GlobalConstants.MonthID_7) && !lockT2)
                        {
                            lstECDelete.ForEach(e => e.CommentsM2M7 = "");
                        }
                        else if ((monthId == GlobalConstants.MonthID_3 || monthId == GlobalConstants.MonthID_8) && !lockT3)
                        {
                            lstECDelete.ForEach(e => e.CommentsM3M8 = "");
                        }
                        else if ((monthId == GlobalConstants.MonthID_4 || monthId == GlobalConstants.MonthID_9) && !lockT4)
                        {
                            lstECDelete.ForEach(e => e.CommentsM4M9 = "");
                        }
                        else if ((monthId == GlobalConstants.MonthID_5 || monthId == GlobalConstants.MonthID_10) && !lockT5)
                        {
                            lstECDelete.ForEach(e => e.CommentsM5M10 = "");
                        }
                        lstECDelete.ForEach(e => e.UpdateTime = DateTime.Now);
                        //Cap nhat gia tri null cho cac hoc sinh co cot nhan xet duoc xoa
                        
                        for (int i = 0; i < lstECDelete.Count; i++)
                        {
                            

                            this.Update(lstECDelete[i]);
                        }
                        this.Save();
                    }
                     */
                    #endregion
                }
                // Cap nhat du lieu danh gia nhat xet cuoi ky doi voi tab danh gia chat luong GD
                //Thuc hien xoa du lieu cuoi ky
                if (monthId == GlobalConstants.MonthID_11 && evaluationId == GlobalConstants.TAB_EDUCATION_BOOKMARK_PRIMARY)
                {
                    List<SummedEvaluation> listSummedEvaluation = SummedEvaluationBusiness.All
                                                                .Where(p => p.LastDigitSchoolID == partitionId

                                                                 && p.ClassID == classId
                                                                 && p.AcademicYearID == academicYearId
                                                                 && p.SemesterID == semesterId
                                                                 && p.EvaluationID == evaluationId
                                                                 && (p.EvaluationCriteriaID == evaluationCriteriaID || evaluationCriteriaID == 0)
                                                                 && IDList.Contains(p.PupilID)
                                                                 ).ToList();
                    SummedEvaluation objSU = null;
                    if (listSummedEvaluation != null && listSummedEvaluation.Count > 0)
                    {
                        for (int i = 0; i < listSummedEvaluation.Count; i++)
                        {
                            isChange = false;
                            objSU = listSummedEvaluation[i];
                            pupilID = objSU.PupilID;
                            objPOC = lstPOC.Where(p => p.PupilID == pupilID).FirstOrDefault();
                            objectIDStrtmp.Append(pupilID);
                            descriptionStrtmp.Append("Xóa nhận xét sổ TD CLGD GVBM");
                            paramsStrtmp.Append(pupilID);
                            userFuntionsStrtmp.Append("Sổ TD CLGD (GVBM)");
                            userActionsStrtmp.Append(SMAS.Business.Common.GlobalConstants.ACTION_DELETE);
                            userDescriptionsStrtmp.Append("Xóa nhận xét đánh giá " + evaluationName + " HS " + objPOC.PupilFullName + ", " + "mã " + objPOC.PupilCode + ", ");
                            userDescriptionsStrtmp.Append("Lớp ").Append(objPOC.ClassName + "/" + objSC.DisplayName + "/" + "Tháng " + monthId + "/Học kỳ " + semesterId + "/Năm học " + objAcadimicYear.Year + "-" + (objAcadimicYear.Year + 1) + ". ");
                            if (!isRetest)
                            {
                                if (!lockCK)
                                {
                                    if (!string.IsNullOrEmpty(objSU.EndingComments))
                                    {
                                        oldObjectStrtmp.Append(LOG_NXCK + objSU.EndingComments).Append("; ");
                                        isChange = true;
                                    }
                                    if (objSU.PeriodicEndingMark.HasValue)
                                    {
                                        oldObjectStrtmp.Append("KTĐK CK: " + objSU.PeriodicEndingMark).Append("; ");
                                        isChange = true;
                                    }
                                    if (!string.IsNullOrEmpty(objSU.EndingEvaluation))
                                    {
                                        oldObjectStrtmp.Append("Đánh giá: " + objSU.EndingEvaluation).Append("; ");
                                        isChange = true;
                                    }

                                    objSU.PeriodicEndingMark = null;
                                    objSU.EndingComments = "";
                                    objSU.UpdateTime = DateTime.Now;
                                    objSU.EndingEvaluation = "";
                                }
                            }
                            else
                            {
                                objSU.RetestMark = null;
                                objSU.EvaluationReTraining = null;
                            }
                            if (isChange)
                            {
                                objAc = new ActionAuditDataBO();
                                objAc.PupilID = pupilID;
                                objAc.ClassID = classId;
                                objAc.EvaluationID = evaluationId;
                                objAc.SubjectID = evaluationCriteriaID;
                                objAc.ObjID = objectIDStrtmp;
                                objAc.Parameter = paramsStrtmp;
                                objAc.OldData = oldObjectStrtmp;
                                objAc.NewData = newObjectStrtmp;
                                objAc.UserAction = userActionsStrtmp;
                                objAc.UserFunction = userFuntionsStrtmp;
                                objAc.UserDescription = userDescriptionsStrtmp;
                                objAc.Description = descriptionStrtmp;
                                lstActionAuditData.Add(objAc);
                            }
                            objectIDStrtmp = new StringBuilder();
                            descriptionStrtmp = new StringBuilder();
                            paramsStrtmp = new StringBuilder();
                            oldObjectStrtmp = new StringBuilder();
                            newObjectStrtmp = new StringBuilder();
                            userFuntionsStrtmp = new StringBuilder();
                            userActionsStrtmp = new StringBuilder();
                            userDescriptionsStrtmp = new StringBuilder();
                            SummedEvaluationBusiness.Update(objSU);
                        }
                        SummedEvaluationBusiness.Save();
                    }
                }
            }
            catch (Exception ex)
            {

                
                LogExtensions.ErrorExt(logger, DateTime.Now, "BulkDeleteEvaluationComments", "null", ex);
            }
            finally
            {
                context.Configuration.AutoDetectChangesEnabled = true;
                context.Configuration.ValidateOnSaveEnabled = true;
            }
        }

        private IDictionary<string, object> ColumnMappings()
        {
            Dictionary<string, object> columnMap = new Dictionary<string, object>();
            columnMap.Add("EvaluationCommentsID", "EVALUATION_COMMENTS_ID");
            columnMap.Add("PupilID", "PUPIL_ID");
            columnMap.Add("ClassID", "CLASS_ID");
            columnMap.Add("EducationLevelID", "EDUCATION_LEVEL_ID");
            columnMap.Add("AcademicYearID", "ACADEMIC_YEAR_ID");
            columnMap.Add("LastDigitSchoolID", "LAST_DIGIT_SCHOOL_ID");
            columnMap.Add("SchoolID", "SCHOOL_ID");
            columnMap.Add("TypeOfTeacher", "TYPE_OF_TEACHER");
            columnMap.Add("SemesterID", "SEMESTER_ID");
            columnMap.Add("EvaluationCriteriaID", "EVALUATION_CRITERIA_ID");
            columnMap.Add("EvaluationID", "EVALUATION_ID");
            //columnMap.Add("PeriodicEndingMark", "PERIODIC_ENDING_MARK");
            //columnMap.Add("EndingEvaluation", "ENDING_EVALUATION");
            columnMap.Add("CommentsM1M6", "COMMENTS_M1_M6");
            columnMap.Add("CommentsM2M7", "COMMENTS_M2_M7");
            columnMap.Add("CommentsM3M8", "COMMENTS_M3_M8");
            columnMap.Add("CommentsM4M9", "COMMENTS_M4_M9");
            columnMap.Add("CommentsM5M10", "COMMENTS_M5_M10");
            //columnMap.Add("CommentsEnding", "COMMENTS_ENDING");
            columnMap.Add("NoteInsert", "NOTE_INSERT");
            columnMap.Add("NoteUpdate", "NOTE_UPDATE");
            //columnMap.Add("RetestMark", "RETEST_MARK");
            //columnMap.Add("EvaluationReTraining", "EVALUATION_RETRAINING");
            columnMap.Add("CreateTime", "CREATETIME");
            columnMap.Add("UpdateTime", "UPDATETIME");

            return columnMap;
        }

        /// <summary>
        /// Chuyen du lieu tu EvaluationComments sang EvaluationCommentsHistory
        /// </summary>
        /// <param name="objA"></param>
        /// <param name="objB"></param>
        private EvaluationCommentsHistory ConvertToHistory(EvaluationComments objA)
        {
            if (objA == null)
            {
                return new EvaluationCommentsHistory();
            }
            EvaluationCommentsHistory objB = new EvaluationCommentsHistory
            {
                EvaluationCommentsID = objA.EvaluationCommentsID,
                AcademicYearID = objA.AcademicYearID,
                ClassID = objA.ClassID,
                // CommentsEnding = objA.CommentsEnding,
                CommentsM1M6 = objA.CommentsM1M6,
                CommentsM2M7 = objA.CommentsM2M7,
                CommentsM3M8 = objA.CommentsM3M8,
                CommentsM4M9 = objA.CommentsM4M9,
                CommentsM5M10 = objA.CommentsM5M10,
                CreateTime = objA.CreateTime,
                EducationLevelID = (int)objA.EvaluationID,
                //  EndingEvaluation = objA.EndingEvaluation,
                EvaluationCriteriaID = objA.EvaluationCriteriaID,
                EvaluationID = objA.EvaluationID,
                //  EvaluationReTraining = objA.EvaluationReTraining,
                LastDigitSchoolID = objA.LastDigitSchoolID,
                NoteInsert = objA.NoteInsert,
                NoteUpdate = objA.NoteUpdate,
                // PeriodicEndingMark = objA.PeriodicEndingMark,
                PupilID = objA.PupilID,
                // RetestMark = objA.RetestMark,
                SchoolID = objA.SchoolID,
                SemesterID = objA.SemesterID,
                TypeOfTeacher = objA.TypeOfTeacher,
                UpdateTime = objA.UpdateTime
            };
            return objB;
        }

        /// <summary>
        /// Chuyen du lieu 1 list EvaluationComments sang list EvaluationCommentsHistory
        /// </summary>
        /// <param name="lstA"></param>
        /// <returns></returns>
        private List<EvaluationCommentsHistory> ConvertListtoHistory(List<EvaluationComments> lstA)
        {
            List<EvaluationCommentsHistory> lstECHistory = lstA.ConvertAll(e => ConvertToHistory(e));
            return lstECHistory;
        }

        public string CheckLockMark(int classID, int schoolID, int academicYearID)
        {
            string strLockMark = string.Empty;
            Dictionary<string, object> dic = new Dictionary<string, object>();
            dic.Add("SchoolID", schoolID);
            dic.Add("AcademicYearID", academicYearID);
            dic.Add("ClassID", classID);
            List<LockMarkPrimary> lockmarlList = LockMarkPrimaryBusiness.SearchLockedMark(dic).ToList();

            if (lockmarlList != null && lockmarlList.Count > 0)
            {
                return lockmarlList.Select(p => p.LockPrimary).FirstOrDefault();
            }
            else
            {
                return string.Empty;
            }
        }

        #region Quang_HoangAnh Bao cao theo doi CLGD

        #region Bao cao theo doi CLGD_GVCN
        public ProcessedReport InsertReport_HeadTeacher(ReportEvalutionComments reportEC, Stream data)
        {
            ProcessedReport pr = new ProcessedReport();
            string reportCode = "";
            if (reportEC.AppliedLevel == SystemParamsInFile.REPORT_EDUCATION_GRADE_PRIMARY)
            {
                reportCode = SystemParamsInFile.REPORT_BAOCAOTHEODOICLGDLOPK1_GVCN;
            }

            pr.ReportCode = reportCode;
            pr.ProcessedDate = DateTime.Now;
            pr.InputParameterHashKey = GetHashKey(reportEC);
            pr.ReportData = ReportUtils.Compress(data);

            IDictionary<string, object> dic = new Dictionary<string, object> {
                {"AppliedLevel", reportEC.AppliedLevel},
                {"ClassID", reportEC.ClassID},
                {"SchoolID", reportEC.SchoolID}              
            };
            //Tạo tên file
            ReportDefinition reportDef = ReportDefinitionBusiness.GetByCode(reportCode);
            string outputNamePattern = reportDef.OutputNamePattern;
            // Lay ten lop
            ClassProfile cp = ClassProfileBusiness.Find(reportEC.ClassID);
            string className = string.Empty;
            if (cp != null)
            {
                className = Utils.StripVNSign(cp.DisplayName).ToUpper();
                if (className.Contains(SystemParamsInFile.REMOVE_CLASS) == true)
                {
                    className = className.Replace(SystemParamsInFile.REMOVE_CLASS, string.Empty).Trim();
                }
            }
            outputNamePattern = outputNamePattern.Replace("[ClassName]", className);
            pr.ReportName = ReportUtils.RemoveSpecialCharacters(outputNamePattern) + "." + reportDef.OutputFormat;
            ProcessedReportParameterRepository.Insert(dic, pr);
            ProcessedReportBusiness.Insert(pr);
            ProcessedReportBusiness.Save();

            return pr;
        }

        public ProcessedReport GetReport_HeadTeacher(ReportEvalutionComments reportEC)
        {
            string reportCode = "";
            if (reportEC.AppliedLevel == SystemParamsInFile.REPORT_EDUCATION_GRADE_PRIMARY)
            {
                reportCode = SystemParamsInFile.REPORT_BAOCAOTHEODOICLGDLOPK1_GVCN;
            }
            string inputParameterHashKey = GetHashKey(reportEC);
            return ProcessedReportBusiness.GetProcessedReport(reportCode, inputParameterHashKey);
        }

        public Stream CreateReport_HeadTeacher(ReportEvalutionComments reportEC)
        {
            IVTWorkbook oBook = null;

            string reportCode = string.Empty;
            reportCode = SystemParamsInFile.REPORT_BAOCAOTHEODOICLGDLOPK1_GVCN;
            ReportDefinition rp = ReportDefinitionBusiness.GetByCode(reportCode);
            string templatePath = SystemParamsInFile.TEMPLATE_FOLDER + rp.TemplateDirectory + "/" + rp.TemplateName;
            oBook = VTExport.OpenWorkbook(templatePath);

            #region Lay du lieu cho sheet trang bia

            string commnuneName = string.Empty;
            string districtName = string.Empty;
            string provinceName = string.Empty;
            string headTeacherName = string.Empty;

            ClassProfile cp = ClassProfileBusiness.Find(reportEC.ClassID);
            string className = Utils.StripVNSign(cp.DisplayName).ToUpper();
            if (className.Contains(SystemParamsInFile.REMOVE_CLASS) == true)
            {
                className = className.Replace(SystemParamsInFile.REMOVE_CLASS, string.Empty).Trim();
            }

            AcademicYear ay = AcademicYearBusiness.Find(reportEC.AcademicYearID);
            string yearName = ay.DisplayTitle;

            SchoolProfile sp = SchoolProfileBusiness.Find(reportEC.SchoolID);
            string schoolName = sp.SchoolName;
            Commune commune = CommuneBusiness.Find(sp.CommuneID);
            if (commune != null)
            {
                commnuneName = commune.CommuneName;
            }
            District district = DistrictBusiness.Find(sp.DistrictID);
            if (district != null)
            {
                districtName = district.DistrictName;
            }
            Province province = ProvinceBusiness.Find(sp.ProvinceID);
            if (province != null)
            {
                provinceName = province.ProvinceName;
            }

            if (cp.HeadTeacherID.HasValue)
            {
                Employee employee = EmployeeBusiness.Find(cp.HeadTeacherID);
                headTeacherName = employee.FullName;
            }
            #endregion

            #region Fill du lieu cho sheet trang bia
            IVTWorksheet firstSheet = oBook.GetSheet(1);

            //xu ly thong tin trang bia
            firstSheet.SetCellValue("A14", "LỚP " + className.ToUpper());
            firstSheet.SetCellValue("A15", "Năm học: " + yearName);
            firstSheet.SetCellValue("C23", schoolName);
            firstSheet.SetCellValue("C24", commnuneName);
            firstSheet.SetCellValue("C25", districtName);
            firstSheet.SetCellValue("C26", provinceName);
            firstSheet.SetCellValue("C27", headTeacherName);

            //lay sheet thu 2
            IVTWorksheet secondSheet = oBook.GetSheet(2);
            IVTRange rangeTitleSheet2 = secondSheet.GetRow(SystemParamsInFile.ROW1_SHEET2 - 2);
            IVTRange rangeRowSheet2 = secondSheet.GetRange(SystemParamsInFile.ROW1_SHEET2 - 1, 1, SystemParamsInFile.ROW1_SHEET2 - 1, 8);
            secondSheet.SetRowHeight(SystemParamsInFile.ROW1_SHEET2 - 1, 28);
            #endregion

            #region Lay thong tin du lieu cho sheet 2

            IDictionary<string, object> dic = new Dictionary<string, object>{
                {"ClassID", reportEC.ClassID},
                {"SchoolID", reportEC.SchoolID},
                {"AcademicYearID", reportEC.AcademicYearID}
            };

            List<PupilProfileBO> lstPupilProfile = PupilProfileBusiness.Search(dic)
                .Where(o => o.CurrentAcademicYearID == reportEC.AcademicYearID
                    && o.CurrentClassID == reportEC.ClassID && o.CurrentSchoolID == reportEC.SchoolID
                    && o.IsActive == true).OrderBy(u => u.OrderInClass).ThenBy(u => u.Name).ThenBy(o => o.FullName)
                .ToList();

            #endregion

            #region Fill du lieu cho sheet 2
            int indexColor = 0;

            List<System.Drawing.Color> listColor = new List<System.Drawing.Color>()
            {
                System.Drawing.Color.LightYellow,
                System.Drawing.Color.Wheat,
                System.Drawing.Color.PowderBlue,
                System.Drawing.Color.Aquamarine
            };

            secondSheet.SetCellValue("A2", "DANH SÁCH HỌC SINH LỚP " + className.ToUpper() + " NĂM HỌC " + yearName);

            int page = 1;
            int countRow = SystemParamsInFile.ROWPAGE1_SHEET2;
            int indexPage = 0;
            int countRowBefore = 0;

            int countPP = lstPupilProfile.Count;
            for (int i = 0; i < countPP; i++)
            {
                #region xu ly mau va coppy dong
                int rowSheet2 = 0;

                if (i == countRow)
                {
                    if (page == 1)
                    {
                        secondSheet.CopyPasteSameSize(rangeTitleSheet2, countRow + SystemParamsInFile.ROW1_SHEET2 + (page * 2), 1);
                    }
                    else
                    {
                        secondSheet.CopyPasteSameSize(rangeTitleSheet2, countRow + SystemParamsInFile.ROW1_SHEET2 + (page * 2) + 1, 1);
                    }
                    countRowBefore = countRow;
                    countRow += SystemParamsInFile.ROWPAGE2_SHEET2;
                    indexPage = 0;
                    indexColor = 0;
                    page++;
                }

                if (page == 1)
                {
                    rowSheet2 = SystemParamsInFile.ROW1_SHEET2;
                }
                if (page > 1)
                {
                    rowSheet2 = SystemParamsInFile.ROW1_SHEET2 + countRowBefore + ((page - 1) * 3);
                }

                if (i % 5 == 0)
                {
                    indexColor++;
                    if (indexColor == listColor.Count)
                    {
                        indexColor = 0;
                    }
                }

                int index = rowSheet2 + indexPage;
                indexPage++;

                secondSheet.CopyPasteSameSize(rangeRowSheet2, index, 1);
                secondSheet.GetRange(index, 1, index, 8).FillColor(listColor[indexColor]);
                #endregion xu ly mau va coppy dong

                secondSheet.GetRange(index, 1, index, 8).WrapText();

                secondSheet.SetCellValue(index, 1, i + 1);
                secondSheet.SetCellValue(index, 2, lstPupilProfile[i].FullName);
                secondSheet.SetCellValue(index, 3, lstPupilProfile[i].BirthDate.ToString("dd/MM/yyyy"));

                if (lstPupilProfile[i].Genre == 0)
                {
                    secondSheet.SetCellValue(index, 5, "x");
                }
                else
                {
                    secondSheet.SetCellValue(index, 4, "x");
                }
                secondSheet.SetCellValue(index, 6, lstPupilProfile[i].EthnicName);

                if (lstPupilProfile[i].IsDisabled == true)
                {
                    DisabledType dt = DisabledTypeBusiness.Find(lstPupilProfile[i].DisabledTypeID);
                    secondSheet.SetCellValue(index, 7, (dt == null) ? "" : dt.Resolution);
                }
                else
                {
                    secondSheet.SetCellValue(index, 7, "");
                }

                if (lstPupilProfile[i].TempResidentalAddress != null)
                {
                    secondSheet.SetCellValue(index, 8, lstPupilProfile[i].TempResidentalAddress);
                }
                else
                {
                    if (lstPupilProfile[i].PermanentResidentalAddress != null && lstPupilProfile[i].PermanentResidentalAddress != "")
                    {
                        secondSheet.SetCellValue(index, 8, lstPupilProfile[i].PermanentResidentalAddress);
                    }
                    else
                    {
                        secondSheet.SetCellValue(index, 8, "");
                    }
                }
                //Neu HS khac dang hoc
                if (lstPupilProfile[i].ProfileStatus != GlobalConstants.PUPIL_STATUS_STUDYING)
                {
                    secondSheet.GetRange(index, 1, index, 8).SetFontStyle(false, System.Drawing.Color.Red, false, null, true, false);
                }
            }

            secondSheet.DeleteRow(SystemParamsInFile.ROW1_SHEET2 - 1);
            #endregion
            AcademicYear objAcademicYear = AcademicYearBusiness.Find(reportEC.AcademicYearID);
            int yearCurrent = objAcademicYear.Year;
            int partitionId = UtilsBusiness.GetPartionId(reportEC.SchoolID);

            #region Lay thong tin du lieu cho sheet 3 va sheet 4
            List<InfoEvaluationPupil> lstEvaluationPupil = new List<InfoEvaluationPupil>();
            InfoEvaluationPupil infoValuationPupil;

            List<PupilAbsence> lstPupilAbsence = PupilAbsenceBusiness.All.Where(a => a.AcademicYearID == reportEC.AcademicYearID
                                                                                 && a.ClassID == reportEC.ClassID
                                                                                 && a.Last2digitNumberSchool == partitionId
                                                                                 ).ToList();

            List<InfoEvaluationPupil> lstInfoPupil = (from pt in PhysicalTestBusiness.All
                                                      join mn in MonitoringBookBusiness.All on pt.MonitoringBookID equals mn.MonitoringBookID
                                                      where mn.ClassID == reportEC.ClassID && mn.AcademicYearID == reportEC.AcademicYearID
                                                      orderby mn.HealthPeriodID descending
                                                      select new InfoEvaluationPupil
                                                      {
                                                          PupilID = mn.PupilID,
                                                          Height = pt.Height.HasValue ? pt.Height.Value : 0,
                                                          Weight = pt.Weight.HasValue ? pt.Weight.Value : 0,
                                                          EvaluationHealth = pt.PhysicalClassification.HasValue ? pt.PhysicalClassification.Value : 0
                                                      }).ToList();

            //Khai bao cac list du lieu su dung
            List<ReportSummedEvaluationBO> lstSummed = null;
            List<ReportEvaluationCommentsBO> lstEvaluation = null;
            List<ReportSummedEndingEvaluationBO> lstSummedEnding = null;


            lstSummedEnding = (from see in SummedEndingEvaluationBusiness.All
                               where see.LastDigitSchoolID == partitionId &&
                                     see.AcademicYearID == reportEC.AcademicYearID &&
                                     see.ClassID == reportEC.ClassID
                               select new ReportSummedEndingEvaluationBO
                               {
                                   AcademicYearID = see.AcademicYearID,
                                   ClassID = see.ClassID,
                                   CreateTime = see.CreateTime,
                                   EndingEvaluation = see.EndingEvaluation,
                                   EvaluationID = see.EvaluationID,
                                   EvaluationReTraining = see.EvaluationReTraining,
                                   LastDigitSchoolID = see.LastDigitSchoolID,
                                   PupilID = see.PupilID,
                                   SchoolID = see.SchoolID,
                                   SemesterID = see.SemesterID,
                                   SummedEndingEvaluationID = see.SummedEndingEvaluationID,
                                   UpdateTime = see.UpdateTime,
                                   RateAdd = see.RateAdd
                               }).ToList();

            //Kiem tra nam hien tai va gan list la current hoac history

            if (UtilsBusiness.IsMoveHistory(objAcademicYear))
            {
                lstEvaluation = (from ec in EvaluationCommentsHistoryBusiness.All
                                 where ec.LastDigitSchoolID == partitionId && ec.TypeOfTeacher == 2 && ec.ClassID == reportEC.ClassID
                                 select new ReportEvaluationCommentsBO
                                 {
                                     EvaluationCommentsID = ec.EvaluationCommentsID,
                                     PupilID = ec.PupilID,
                                     ClassID = ec.ClassID,
                                     EducationLevelID = ec.EducationLevelID,
                                     AcademicYearID = ec.AcademicYearID,
                                     LastDigitSchoolID = ec.LastDigitSchoolID,
                                     SchoolID = ec.SchoolID,
                                     TypeOfTeacher = ec.TypeOfTeacher,
                                     SemesterID = ec.SemesterID,
                                     EvaluationCriteriaID = ec.EvaluationCriteriaID,
                                     EvaluationID = ec.EvaluationID,
                                     CommentsM1M6 = ec.CommentsM1M6,
                                     CommentsM2M7 = ec.CommentsM2M7,
                                     CommentsM3M8 = ec.CommentsM3M8,
                                     CommentsM4M9 = ec.CommentsM4M9,
                                     CommentsM5M10 = ec.CommentsM5M10
                                 }).ToList();
                //Chi lay danh gia mon hoc khong lay cac nhan xet ve nang luc, pham chat
                lstSummed = (from pt in SummedEvaluationHistoryBusiness.All
                             where pt.LastDigitSchoolID == partitionId && pt.AcademicYearID == reportEC.AcademicYearID &&
                             pt.ClassID == reportEC.ClassID
                             && pt.EvaluationID == GlobalConstants.TAB_EDUCATION
                             select new ReportSummedEvaluationBO
                             {
                                 SummedEvaluationID = pt.SummedEvaluationID,
                                 PupilID = pt.PupilID,
                                 ClassID = pt.ClassID,
                                 EducationLevelID = pt.EducationLevelID,
                                 AcademicYearID = pt.AcademicYearID,
                                 LastDigitSchoolID = pt.LastDigitSchoolID,
                                 SchoolID = pt.SchoolID,
                                 SemesterID = pt.SemesterID,
                                 EvaluationCriteriaID = pt.EvaluationCriteriaID,
                                 EvaluationID = pt.EvaluationID,
                                 EndingEvaluation = pt.EndingEvaluation,
                                 EndingComments = pt.EndingComments,
                                 PeriodicEndingMark = pt.PeriodicEndingMark,
                                 RetestMark = pt.RetestMark

                             }).ToList();
            }
            else
            {
                lstEvaluation = (from ec in EvaluationCommentsBusiness.All
                                 where ec.LastDigitSchoolID == partitionId && ec.TypeOfTeacher == 2 && ec.ClassID == reportEC.ClassID && ec.AcademicYearID == reportEC.AcademicYearID
                                 select new ReportEvaluationCommentsBO
                                 {
                                     EvaluationCommentsID = ec.EvaluationCommentsID,
                                     PupilID = ec.PupilID,
                                     ClassID = ec.ClassID,
                                     EducationLevelID = ec.EducationLevelID,
                                     AcademicYearID = ec.AcademicYearID,
                                     LastDigitSchoolID = ec.LastDigitSchoolID,
                                     SchoolID = ec.SchoolID,
                                     TypeOfTeacher = ec.TypeOfTeacher,
                                     SemesterID = ec.SemesterID,
                                     EvaluationCriteriaID = ec.EvaluationCriteriaID,
                                     EvaluationID = ec.EvaluationID,
                                     CommentsM1M6 = ec.CommentsM1M6,
                                     CommentsM2M7 = ec.CommentsM2M7,
                                     CommentsM3M8 = ec.CommentsM3M8,
                                     CommentsM4M9 = ec.CommentsM4M9,
                                     CommentsM5M10 = ec.CommentsM5M10
                                 }).ToList();

                lstSummed = (from pt in SummedEvaluationBusiness.All
                             where pt.LastDigitSchoolID == partitionId && pt.AcademicYearID == reportEC.AcademicYearID &&
                             pt.ClassID == reportEC.ClassID
                             && pt.EvaluationID == GlobalConstants.TAB_EDUCATION
                             select new ReportSummedEvaluationBO
                             {
                                 SummedEvaluationID = pt.SummedEvaluationID,
                                 PupilID = pt.PupilID,
                                 ClassID = pt.ClassID,
                                 EducationLevelID = pt.EducationLevelID,
                                 AcademicYearID = pt.AcademicYearID,
                                 LastDigitSchoolID = pt.LastDigitSchoolID,
                                 SchoolID = pt.SchoolID,
                                 SemesterID = pt.SemesterID,
                                 EvaluationCriteriaID = pt.EvaluationCriteriaID,
                                 EvaluationID = pt.EvaluationID,
                                 EndingEvaluation = pt.EndingEvaluation,
                                 EndingComments = pt.EndingComments,
                                 PeriodicEndingMark = pt.PeriodicEndingMark,
                                 RetestMark = pt.RetestMark
                             }).ToList();

            }

            //lst mon hoc cua mot lop
            List<ClassSubjectBO> lstClassSubject = (from cs in ClassSubjectBusiness.All
                                                    join sc in SubjectCatBusiness.All on cs.SubjectID equals sc.SubjectCatID
                                                    join cpb in ClassProfileBusiness.All on cs.ClassID equals cpb.ClassProfileID
                                                    where sc.IsActive == true && cs.ClassID == reportEC.ClassID
                                                    && cpb.AcademicYearID == reportEC.AcademicYearID && cpb.SchoolID == reportEC.SchoolID
                                                    && cs.Last2digitNumberSchool == partitionId
                                                    && cpb.IsActive.Value
                                                    select new ClassSubjectBO
                                                    {
                                                        SubjectID = cs.SubjectID,
                                                        ClassID = cs.ClassID,
                                                        SubjectName = sc.SubjectName,
                                                        DisplayName = sc.DisplayName,
                                                    }).ToList();

            //lst mon hoc cua truong
            List<SchoolSubjectBO> lstSchoolSubject = (from ss in SchoolSubjectBusiness.All
                                                      where ss.SchoolID == reportEC.SchoolID && ss.AcademicYearID == reportEC.AcademicYearID
                                                      && ss.EducationLevelID == reportEC.EducationLevelID
                                                      select new SchoolSubjectBO
                                                      {
                                                          SubjectID = ss.SubjectID,
                                                          SchoolID = ss.SchoolID,
                                                          EducationLevelID = ss.EducationLevelID
                                                      }).ToList();

            //Duyet danh sach hoc sinh
            PupilProfileBO objPupilProfile = null;
            for (int i = 0; i < lstPupilProfile.Count; i++)
            {
                objPupilProfile = lstPupilProfile[i];
                infoValuationPupil = new InfoEvaluationPupil();
                infoValuationPupil.FullName = objPupilProfile.FullName;
                infoValuationPupil.ClassID = objPupilProfile.CurrentClassID;
                infoValuationPupil.SchoolID = objPupilProfile.CurrentSchoolID;
                infoValuationPupil.PupilID = objPupilProfile.PupilProfileID;
                infoValuationPupil.ProfileStatus = objPupilProfile.ProfileStatus;

                //Lay thong tin ngay nghi 
                GetAbsence(infoValuationPupil, objPupilProfile, lstPupilAbsence);

                //Lay thong tin suc khoe
                GetHealthInfo(infoValuationPupil, lstInfoPupil);

                //Lay nhan xet theo thang cua GVCN                
                GetEvaluationMonth_HeadTeacher(infoValuationPupil, objPupilProfile, reportEC, lstEvaluation);

                //Tong ket, danh gia GVCN
                ResultEvaluation_HeadTeacher(infoValuationPupil, objPupilProfile, reportEC, lstSummed, lstSummedEnding);

                //Khen thuong 
                ResultReward_HeadTeacher(infoValuationPupil, objPupilProfile, reportEC);

                lstEvaluationPupil.Add(infoValuationPupil);
            }
            #endregion

            #region Fill du lieu cho sheet 3
            IVTWorksheet tempSheet = oBook.GetSheet(3);

            int countLEP = lstEvaluationPupil.Count;
            InfoEvaluationPupil pupil = null;
            for (int i = 0; i < countLEP; i++)
            {
                pupil = lstEvaluationPupil[i];
                IVTWorksheet sheet = oBook.CopySheetToLast(tempSheet);
                //sheet.Name = pupil.FullName.Replace(" ", "");
                sheet.Name = String.Format("{0}_{1}", i + 1, pupil.FullName.Replace(" ", string.Empty));

                if (pupil.PupilID != 0)
                {
                    //Xu ly thong tin tung trang cho moi hoc sinh
                    sheet.SetCellValue("A2", (i + 1) + ". Họ tên học sinh:");
                    sheet.SetCellValue("E2", pupil.FullName);
                    if (pupil.Height != 0)
                    {
                        sheet.SetCellValue("C3", pupil.Height.HasValue ? Math.Round(pupil.Height.Value, 1).ToString() : "" + " cm");
                    }
                    else
                    {
                        sheet.SetCellValue("C3", String.Empty);
                    }
                    if (pupil.Weight != 0)
                    {
                        sheet.SetCellValue("H3", pupil.Weight.HasValue ? Math.Round(pupil.Weight.Value, 1).ToString() : "" + " kg");
                    }
                    else
                    {
                        sheet.SetCellValue("H3", String.Empty);
                    }
                    switch (pupil.EvaluationHealth)
                    {
                        case 1:
                            sheet.SetCellValue("K3", "Loại I (Tốt)");
                            break;
                        case 2:
                            sheet.SetCellValue("K3", "Loại II (Khá)");
                            break;
                        case 3:
                            sheet.SetCellValue("K3", "Loại III (Trung bình)");
                            break;
                        case 4:
                            sheet.SetCellValue("K3", "Loại IV (Yếu)");
                            break;
                        case 5:
                            sheet.SetCellValue("K3", "Loại V (Rất yếu)");
                            break;
                        default:
                            sheet.SetCellValue("K3", String.Empty);
                            break;
                    }
                    sheet.SetCellValue("C4", pupil.NumberAbsence.ToString());
                    sheet.SetCellValue("H4", pupil.HasAccepted.ToString());
                    sheet.SetCellValue("K4", pupil.NoAccepted.ToString());

                    //Duyet list va fill tu thang 1 den thang 10
                    int countEM = pupil.lstEvalutionMonth.Count;
                    for (int j = 0; j < countEM; j++)
                    {
                        int rowSheet3 = SystemParamsInFile.ROW1_SHEET3;
                        int month = pupil.lstEvalutionMonth[j].Month;
                        if (month <= 5)
                        {
                            rowSheet3 = SystemParamsInFile.ROW1_SHEET3 + (month - 1);
                        }
                        if (month > 5)
                        {
                            rowSheet3 = SystemParamsInFile.ROW2_SHEET3 + (month - 6);
                        }
                        string text = string.Empty;
                        if (month == 1)
                        {
                            text = "a) Môn học và hoạt động giáo dục: " + Environment.NewLine +
                                pupil.lstEvalutionMonth[j].Education + Environment.NewLine +
                                "b) Năng lực: " + pupil.lstEvalutionMonth[j].Fitness + Environment.NewLine +
                                "c) Phẩm chất: " + pupil.lstEvalutionMonth[j].Quality;
                        }
                        else
                        {
                            text = "a) " + pupil.lstEvalutionMonth[j].Education + Environment.NewLine +
                                "b) " + pupil.lstEvalutionMonth[j].Fitness + Environment.NewLine +
                                "c) " + pupil.lstEvalutionMonth[j].Quality;
                        }
                        sheet.SetCellValue("B" + rowSheet3.ToString(), text);
                    }
                }
                //Khoa sheet cho HS không phai trang thai dang hoc
                if (pupil.ProfileStatus != GlobalConstants.PUPIL_STATUS_STUDYING)
                {
                    sheet.ProtectSheet();
                }
            }
            #endregion

            #region Fill du lieu cho sheet 4
            var fourSheet = oBook.CopySheetToLast(oBook.GetSheet(4));

            SubjectCat subject = null;
            string nameFL = "NGOAINGU";
            if (cp.FirstForeignLanguageID != null && cp.FirstForeignLanguageID != 0)
            {
                ClassSubjectBO csb = lstClassSubject.Find(u => u.SubjectID == cp.FirstForeignLanguageID);
                SchoolSubjectBO ssb = lstSchoolSubject.Find(u => u.SubjectID == cp.FirstForeignLanguageID);
                if (ssb != null && csb != null)
                {
                    subject = SubjectCatBusiness.Find(cp.FirstForeignLanguageID);
                }
            }
            if (subject != null)
            {
                nameFL = Utils.StripVNSign(subject.SubjectName).Replace(" ", "").ToUpper();
                fourSheet.SetCellValue("S5", "Ngoại ngữ" + Environment.NewLine + subject.SubjectName);
            }
            else
            {
                fourSheet.SetCellValue("S5", "Ngoại ngữ" + Environment.NewLine + ".........");
            }

            IVTRange rangeTitleSheet4 = fourSheet.GetRange(4, 1, 7, 61);
            IVTRange rangeRowSheet4 = fourSheet.GetRow(SystemParamsInFile.ROW1_SHEET4_HEADTEACHER - 2);
            fourSheet.SetRowHeight(SystemParamsInFile.ROW1_SHEET4_HEADTEACHER - 2, 28);
            IVTRange rangeInfo = fourSheet.GetRow(SystemParamsInFile.ROW1_SHEET4_HEADTEACHER - 1);

            List<SubjectReportHeadTeacher> lstSubject = SubjectReportHeadTeacher.CreateSubject(nameFL);

            int rowFooter = lstPupilProfile.Count + (SystemParamsInFile.ROW1_SHEET4_HEADTEACHER);

            if (lstPupilProfile.Count > 22)
            {
                fourSheet.CopyPasteSameSize(rangeTitleSheet4, SystemParamsInFile.ROW2_SHEET4_HEADTEACHER - 4, 1);
                rowFooter = (lstPupilProfile.Count - 22) + (SystemParamsInFile.ROW2_SHEET4_HEADTEACHER);
            }

            int countSheet4 = lstEvaluationPupil.Count();
            InfoEvaluationPupil iep = null;
            for (int i = 0; i < countSheet4; i++)
            {
                iep = lstEvaluationPupil[i];

                int rowSheet4 = SystemParamsInFile.ROW1_SHEET4_HEADTEACHER;
                /*if (i <= 21)
                {
                    rowSheet4 = SystemParamsInFile.ROW1_SHEET4_HEADTEACHER;
                }
                if (i > 21)
                {
                    rowSheet4 = SystemParamsInFile.ROW2_SHEET4_HEADTEACHER - 22;
                }*/

                fourSheet.CopyPasteSameSize(rangeRowSheet4, rowSheet4 + i, 1);

                fourSheet.SetCellValue(rowSheet4 + i, 1, (i + 1));
                fourSheet.SetCellValue(rowSheet4 + i, 2, iep.FullName);

                int countResult = iep.lstResultHeadTeacher.Count();
                ResultEvaluationHeadTeacher reh = null;
                for (int j = 0; j < countResult; j++)
                {
                    reh = iep.lstResultHeadTeacher[j];
                    if (reh.evaluationID == GlobalConstants.TAB_EDUCATION)
                    {
                        string sb = "";

                        ClassSubjectBO tmp = lstClassSubject.FirstOrDefault(c => c.SubjectID == reh.subject);//SubjectCatBusiness.Find(reh.subject);

                        if (tmp != null)
                        {
                            sb = Utils.StripVNSign(tmp.SubjectName).Replace(" ", "").ToUpper();
                        }
                        if (lstSubject.Exists(c => c.ShortName.Equals(sb)))
                        {
                            FillDataWithSubject(fourSheet, sb, lstSubject, reh, rowSheet4 + i);
                        }
                    }
                    if (reh.evaluationID == GlobalConstants.TAB_CAPACTIES)
                    {
                        FillDataWithSubject(fourSheet, SubjectReportHeadTeacher.CAPACITIES_COL, lstSubject, reh, rowSheet4 + i);
                    }
                    if (reh.evaluationID == GlobalConstants.TAB_QUALITIES)
                    {
                        FillDataWithSubject(fourSheet, SubjectReportHeadTeacher.QUALITIES_COL, lstSubject, reh, rowSheet4 + i);
                    }
                    if (reh.evaluationID == GlobalConstants.EVALUATION_RESULT && reh.semester == GlobalConstants.SEMESTER_OF_YEAR_PRIMARY)
                    {
                        FillDataWithSubject(fourSheet, SubjectReportHeadTeacher.FINNISH_PRIMARY_EDUCATION, lstSubject, reh, rowSheet4 + i);
                    }
                }

                //int index = lstSubject["KHENTHUONG"];
                SubjectReportHeadTeacher objSubject2 = lstSubject.FirstOrDefault(c => c.ShortName.Contains(SubjectReportHeadTeacher.FELICITATION_COL));
                int firstCol = objSubject2 == null ? 0 : objSubject2.StartColumn;

                ResultRewardEvaluation rre = null;
                for (int k = iep.lstResultRewardEvaluation.Count - 1; k >= 0; k--)
                {
                    rre = iep.lstResultRewardEvaluation[k];
                    if (rre.semester == GlobalConstants.SEMESTER_OF_YEAR_SECOND && !String.IsNullOrEmpty(rre.reward))
                    {
                        fourSheet.SetCellValue(rowSheet4 + i, firstCol, "x");
                    }
                }
                //Neu HS khac dang hoc
                if (iep.ProfileStatus != GlobalConstants.PUPIL_STATUS_STUDYING)
                {
                    fourSheet.GetRange(rowSheet4 + i, 1, rowSheet4 + i, 47).SetFontStyle(false, System.Drawing.Color.Red, false, null, true, false);
                }
            }
            fourSheet.CopyPasteSameSize(rangeInfo, "A" + rowFooter);
            fourSheet.DeleteRow(SystemParamsInFile.ROW1_SHEET4_HEADTEACHER - 2);
            fourSheet.DeleteRow(SystemParamsInFile.ROW1_SHEET4_HEADTEACHER - 2);

            //Dong tong hop

            int endRow = SystemParamsInFile.ROW1_SHEET4_HEADTEACHER - 2 + countSheet4;
            fourSheet.CopyPasteSameSize(rangeRowSheet4, endRow, 1);
            fourSheet.SetCellValue(endRow, 1, "");
            fourSheet.SetCellValue(endRow, 2, "TỔNG HỢP");

            IVTRange rangTotal = fourSheet.GetRange(endRow, 1, endRow, SubjectReportHeadTeacher.END_COLUMN);
            //rangTotal.SetBorder(VTBorderStyle.Solid, VTBorderIndex.All);
            rangTotal.SetFontStyle(true, null, false, null, false, false);
            for (int i = 3; i <= SubjectReportHeadTeacher.END_COLUMN; i++)
            {
                VTVector fromCell = new VTVector(SystemParamsInFile.ROW1_SHEET4_HEADTEACHER - 2, i);
                VTVector toCell = new VTVector(endRow - 1, i);
                string text9 = string.Format("=COUNTA({0}:{1})", fromCell, toCell);
                fourSheet.SetFormulaValue(endRow, i, text9);
            }
            IVTRange range3 = fourSheet.GetRange(endRow + 1, 1, endRow + 1, SubjectReportHeadTeacher.END_COLUMN);
            range3.Merge(false);
            range3.SetHAlign(VTHAlign.xlHAlignCenter);
            fourSheet.SetRowHeight(endRow + 1, 24.0);
            fourSheet.SetCellValue(endRow + 1, 1, "TN-XH: Tự nhiên và Xã hội;  Đánh dấu x nếu Hoàn thành hoặc Đạt");
            fourSheet.GetRow(endRow + 1).SetFontColour(System.Drawing.Color.Red);
            fourSheet.Name = "TongHopDanhGia";

            ////Shet chu ky
            //var sheetInfo = oBook.CopySheetToLast(oBook.GetSheet(5));
            ////Ngay thang nam
            //String datemonthYear = String.Format("{0}, ngày {1} tháng {2} năm {3}", province.ProvinceName, DateTime.Now.Day, DateTime.Now.Month, DateTime.Now.Year);
            //sheetInfo.SetCellValue("E6", datemonthYear);

            /////Giao vien CN
            //sheetInfo.SetCellValue("E12", headTeacherName);
            //sheetInfo.Name = "TrangKy";
            ////Hieu truong
            //sheetInfo.SetCellValue("E33", sp.HeadMasterName);
            oBook.GetSheet(3).Delete();
            oBook.GetSheet(3).Delete();
            oBook.GetSheet(3).Delete();

            #endregion

            return oBook.ToStream();
        }

        #region Ham dung cho HeadTeacher
        private void ResultReward_HeadTeacher(InfoEvaluationPupil infoValuationPupil, PupilProfileBO pp, ReportEvalutionComments reportEC)
        {
            List<RewardCommentFinal> lstRcf = RewardCommentFinalBusiness.All
                .Where(u => u.PupilID == pp.PupilProfileID && u.ClassID == reportEC.ClassID && u.AcademicYearID == reportEC.AcademicYearID).ToList();

            infoValuationPupil.lstResultRewardEvaluation = new List<ResultRewardEvaluation>();
            int countRcf = lstRcf.Count();
            for (int i = 0; i < countRcf; i++)
            {
                RewardCommentFinal rcf = lstRcf[i];
                ResultRewardEvaluation rre = new ResultRewardEvaluation();
                rre.semester = rcf.SemesterID;
                rre.reward = rcf.RewardID;
                infoValuationPupil.lstResultRewardEvaluation.Add(rre);
            }
        }

        private void FillDataWithSubject(IVTWorksheet fourSheet, string nameFL, List<SubjectReportHeadTeacher> lstSubject, ResultEvaluationHeadTeacher reh, int row)
        {

            SubjectReportHeadTeacher objSubject = lstSubject.FirstOrDefault(s => s.ShortName.Contains(nameFL));
            if (objSubject == null)
            {
                return;
            }
            int firstCol = objSubject.StartColumn; //index * SystemParamsInFile.COL_INITIALIZATION + (index - 1);
            string ht = (reh.checkResult != null && (reh.checkResult.Equals(GlobalConstants.SHORTFINISH) || reh.checkResult.Equals(GlobalConstants.SHORT_OK))) ? "x" : "";
            if (reh.evaluationID == GlobalConstants.TAB_EDUCATION)
            {
                //Dien du lieu mon hoc            
                if (objSubject.IsCommenting == GlobalConstants.SUBJECTCAT_ISCOMMENTING_MARK)
                {
                    if (reh.semester == GlobalConstants.SEMESTER_OF_YEAR_FIRST)
                    {
                        //Nhan xet TX
                        fourSheet.SetCellValue(row, firstCol, ht);
                        //Diem dinh ky
                        fourSheet.SetCellValue(row, firstCol + 1, reh.EndingMark);
                    }
                    if (reh.semester == GlobalConstants.SEMESTER_OF_YEAR_SECOND)
                    {
                        fourSheet.SetCellValue(row, firstCol + 2, ht);
                        fourSheet.SetCellValue(row, firstCol + 3, reh.EndingMark);
                    }
                }
                else
                {
                    if (reh.semester == GlobalConstants.SEMESTER_OF_YEAR_FIRST)
                    {
                        //Nhan xet TX
                        fourSheet.SetCellValue(row, firstCol, ht);
                    }
                    if (reh.semester == GlobalConstants.SEMESTER_OF_YEAR_SECOND)
                    {
                        fourSheet.SetCellValue(row, firstCol + 1, ht);
                    }
                }
            }
            else if (reh.evaluationID == GlobalConstants.TAB_CAPACTIES || reh.evaluationID == GlobalConstants.TAB_QUALITIES)
            {
                //Dien du lieu nang luc pham chat
                if (reh.semester == GlobalConstants.SEMESTER_OF_YEAR_FIRST)
                {
                    fourSheet.SetCellValue(row, firstCol, ht);
                }
                if (reh.semester == GlobalConstants.SEMESTER_OF_YEAR_SECOND)
                {
                    fourSheet.SetCellValue(row, firstCol + 1, ht);
                }
            }
            else if (reh.evaluationID == GlobalConstants.EVALUATION_RESULT && reh.semester == GlobalConstants.SEMESTER_OF_YEAR_PRIMARY)
            {
                //Dien hoan thanh chuong trinh lop hoc va len lop
                if (reh.EndingEvaluation == GlobalConstants.SHORTFINISH || reh.RateAdd == 1)
                {
                    fourSheet.SetCellValue(row, firstCol, "x");
                    fourSheet.SetCellValue(row, firstCol + 1, "x");
                }
            }
        }



        private void ResultEvaluation_HeadTeacher(InfoEvaluationPupil infoValuationPupil, PupilProfileBO pp, ReportEvalutionComments reportEC, List<ReportSummedEvaluationBO> lstSummed, List<ReportSummedEndingEvaluationBO> lstSummedEnding)
        {
            List<ReportSummedEvaluationBO> lstSe = lstSummed.Where(u => u.PupilID == pp.PupilProfileID).ToList();
            List<ReportSummedEndingEvaluationBO> lstSummedEndingEva = lstSummedEnding.Where(u => u.PupilID == pp.PupilProfileID).ToList();

            infoValuationPupil.lstResultHeadTeacher = new List<ResultEvaluationHeadTeacher>();

            int countSE = lstSe.Count();
            ResultEvaluationHeadTeacher ret = null;
            for (int i = 0; i < countSE; i++)
            {
                ReportSummedEvaluationBO se = lstSe[i];
                ret = new ResultEvaluationHeadTeacher();
                ret.evaluationID = se.EvaluationID;
                ret.semester = se.SemesterID;
                if (se.EvaluationID == GlobalConstants.TAB_EDUCATION)
                {
                    ret.subject = se.EvaluationCriteriaID;
                    if (se.EndingEvaluation != null)
                    {
                        ret.checkResult = se.EndingEvaluation;
                    }
                    ret.EndingMark = se.PeriodicEndingMark;
                    ret.RetestMark = se.RetestMark;
                }
                infoValuationPupil.lstResultHeadTeacher.Add(ret);
            }

            ReportSummedEndingEvaluationBO summedEnding = null;
            if (lstSummedEndingEva != null && lstSummedEndingEva.Count > 0)
            {
                for (int i = 0; i < lstSummedEndingEva.Count; i++)
                {
                    summedEnding = lstSummedEndingEva[i];
                    ret = new ResultEvaluationHeadTeacher();
                    ret.checkResult = summedEnding.EndingEvaluation;
                    ret.semester = summedEnding.SemesterID;
                    ret.evaluationID = summedEnding.EvaluationID;
                    ret.EndingEvaluation = summedEnding.EndingEvaluation;
                    ret.RateAdd = summedEnding.RateAdd.HasValue ? summedEnding.RateAdd.Value : 0;
                    infoValuationPupil.lstResultHeadTeacher.Add(ret);
                }
            }
        }

        private void GetEvaluationMonth_HeadTeacher(InfoEvaluationPupil infoValuationPupil, PupilProfileBO pp, ReportEvalutionComments reportEC, List<ReportEvaluationCommentsBO> lstEvaluation)
        {
            List<ReportEvaluationCommentsBO> lstEc = lstEvaluation.Where(u => u.PupilID == pp.PupilProfileID).ToList();

            infoValuationPupil.lstEvalutionMonth = new List<InfoEvaluationMonth>();

            for (int i = 0; i < lstEc.Count(); i++)
            {
                GetDataListEvaluationMonth(lstEc[i], infoValuationPupil.lstEvalutionMonth);
            }
        }
        #endregion

        #endregion

        #region Bao cao theo doi CLGD_GVBM
        public ProcessedReport InsertReport_SubjectTeacher(ReportEvalutionComments reportEC, Stream data)
        {
            ProcessedReport pr = new ProcessedReport();
            string reportCode = string.Empty;
            if (reportEC.AppliedLevel == SystemParamsInFile.REPORT_EDUCATION_GRADE_PRIMARY)
            {
                reportCode = SystemParamsInFile.REPORT_BAOCAOTHEODOICLGDLOPK1_GVBM;
            }

            pr.ReportCode = reportCode;
            pr.ProcessedDate = DateTime.Now;
            pr.InputParameterHashKey = GetHashKey(reportEC);
            pr.ReportData = ReportUtils.Compress(data);

            IDictionary<string, object> dic = new Dictionary<string, object> {
                {"AppliedLevel", reportEC.AppliedLevel},
                {"ClassID", reportEC.ClassID},
                {"SchoolID", reportEC.SchoolID},
                {"SubjectID", reportEC.SubjectID}
            };

            ReportDefinition reportDef = ReportDefinitionBusiness.GetByCode(reportCode);
            string outputNamePattern = reportDef.OutputNamePattern;

            // Lay ten lop
            ClassProfile cp = ClassProfileBusiness.Find(reportEC.ClassID);
            string className = string.Empty;
            if (cp != null)
            {
                className = Utils.StripVNSign(cp.DisplayName).ToUpper();
                if (className.Contains(SystemParamsInFile.REMOVE_CLASS) == true)
                {
                    className = className.Replace(SystemParamsInFile.REMOVE_CLASS, string.Empty).Trim();
                }
            }
            outputNamePattern = outputNamePattern.Replace("[ClassName]", className);
            pr.ReportName = ReportUtils.RemoveSpecialCharacters(outputNamePattern) + "." + reportDef.OutputFormat;
            ProcessedReportParameterRepository.Insert(dic, pr);
            ProcessedReportBusiness.Insert(pr);
            ProcessedReportBusiness.Save();

            return pr;
        }

        public ProcessedReport GetReport_SubjectTeacher(ReportEvalutionComments reportEC)
        {
            string reportCode = string.Empty;
            if (reportEC.AppliedLevel == SystemParamsInFile.REPORT_EDUCATION_GRADE_PRIMARY)
            {
                reportCode = SystemParamsInFile.REPORT_BAOCAOTHEODOICLGDLOPK1_GVBM;
            }
            string inputParameterHashKey = GetHashKey(reportEC);
            return ProcessedReportBusiness.GetProcessedReport(reportCode, inputParameterHashKey);
        }

        public Stream CreateReport_SubjectTeacher(ReportEvalutionComments reportEC)
        {
            IVTWorkbook oBook = null;

            string reportCode = string.Empty;
            reportCode = SystemParamsInFile.REPORT_BAOCAOTHEODOICLGDLOPK1_GVBM;
            ReportDefinition rp = ReportDefinitionBusiness.GetByCode(reportCode);
            string templatePath = SystemParamsInFile.TEMPLATE_FOLDER + rp.TemplateDirectory + "/" + rp.TemplateName;
            oBook = VTExport.OpenWorkbook(templatePath);

            #region Lay thong tin cho sheet Trang Bia

            string commnuneName = string.Empty;
            string districtName = string.Empty;
            string provinceName = string.Empty;
            string subjectTeacherName = string.Empty;
            string subjectName = string.Empty;

            ClassProfile cp = ClassProfileBusiness.Find(reportEC.ClassID);
            string className = Utils.StripVNSign(cp.DisplayName).ToUpper();
            if (className.Contains(SystemParamsInFile.REMOVE_CLASS) == true)
            {
                className = className.Replace(SystemParamsInFile.REMOVE_CLASS, string.Empty).Trim();
            }

            AcademicYear ay = AcademicYearBusiness.Find(reportEC.AcademicYearID);
            string yearName = ay.DisplayTitle;

            SchoolProfile sp = SchoolProfileBusiness.Find(reportEC.SchoolID);
            string schoolName = sp.SchoolName;
            Commune commune = CommuneBusiness.Find(sp.CommuneID);
            if (commune != null)
            {
                commnuneName = commune.CommuneName;
            }
            District district = DistrictBusiness.Find(sp.DistrictID);
            if (district != null)
            {
                districtName = district.DistrictName;
            }
            Province province = ProvinceBusiness.Find(sp.ProvinceID);
            if (province != null)
            {
                provinceName = province.ProvinceName;
            }

            IDictionary<string, object> dicTeacher = new Dictionary<string, object>();
            dicTeacher.Add("ClassID", reportEC.ClassID);
            dicTeacher.Add("SubjectID", reportEC.SubjectID);
            dicTeacher.Add("AcademicYearID", reportEC.AcademicYearID);
            TeachingAssignmentBO objTeachingAssignment = TeachingAssignmentBusiness.GetSubjectTeacher(reportEC.SchoolID, dicTeacher).ToList().OrderByDescending(a => a.Semester).FirstOrDefault();
            if (objTeachingAssignment.TeacherID != null)
            {
                var objEmployee = EmployeeBusiness.Find(objTeachingAssignment.TeacherID);
                subjectTeacherName = objEmployee.FullName;
            }

            SubjectCat sc = SubjectCatBusiness.Find(reportEC.SubjectID);
            subjectName = sc.DisplayName;

            #endregion

            #region Xu ly do du lieu vao trang bia

            IVTWorksheet firstSheet = oBook.GetSheet(1);

            //Fill du lieu trang bia
            firstSheet.SetCellValue("A14", "LỚP " + className.ToUpper());
            firstSheet.SetCellValue("A15", "Năm học: " + yearName);
            firstSheet.SetCellValue("C23", schoolName);
            firstSheet.SetCellValue("C24", commnuneName);
            firstSheet.SetCellValue("C25", districtName);
            firstSheet.SetCellValue("C26", provinceName);
            firstSheet.SetCellValue("B27", "Giáo viên bộ môn: " + subjectName);
            firstSheet.Worksheet.Cells["B27"].Font.Bold = true;
            firstSheet.Worksheet.Cells["B27"].Characters[1, "Giáo viên bộ môn: ".Length].Font.Bold = false;
            firstSheet.SetRowHeight(27, 40);
            firstSheet.SetCellValue("C27", subjectTeacherName);
            #endregion

            #region Lay du lieu danh sach hoc sinh cho sheet 3 va sheet 4
            IDictionary<string, object> dic = new Dictionary<string, object>{
                {"ClassID", reportEC.ClassID},
                {"SchoolID", reportEC.SchoolID},
                {"AcademicYearID", reportEC.AcademicYearID}
            };

            List<PupilProfileBO> lstPupilProfile = PupilProfileBusiness.Search(dic)
                .Where(o => o.CurrentAcademicYearID == reportEC.AcademicYearID
                    && o.CurrentClassID == reportEC.ClassID && o.CurrentSchoolID == reportEC.SchoolID
                    && o.IsActive == true).OrderBy(u => u.OrderInClass).ThenBy(u => u.Name).ThenBy(o => o.FullName)
                .ToList();
            #endregion

            #region Fill Danh sach hoc sinh cho sheet 3 va sheet 4
            IVTWorksheet threeSheet = oBook.GetSheet(3);
            IVTWorksheet fourSheet = oBook.GetSheet(4);
            fourSheet.SetCellValue("A2", " LỚP " + className.ToUpper() + " - NĂM HỌC " + yearName);

            int countPP = lstPupilProfile.Count;
            for (int ii = 0; ii < countPP; ii++)
            {
                int index = 5 + ii;

                #region Sheet3
                threeSheet.SetCellValue(index, 1, (ii + 1));
                threeSheet.SetCellValue(index, 2, lstPupilProfile[ii].FullName);
                threeSheet.SetCellValue(index, 3, lstPupilProfile[ii].BirthDate.ToString("dd/MM/yyyy"));
                if (lstPupilProfile[ii].Genre == 0)
                {
                    threeSheet.SetCellValue(index, 5, "x");
                }
                else
                {
                    threeSheet.SetCellValue(index, 4, "x");
                }
                threeSheet.SetCellValue(index, 6, lstPupilProfile[ii].EthnicName);
                if (lstPupilProfile[ii].IsDisabled == true)
                {
                    DisabledType dt = DisabledTypeBusiness.Find(lstPupilProfile[ii].DisabledTypeID);
                    threeSheet.SetCellValue(index, 7, (dt == null) ? "" : dt.Resolution);
                }
                else
                {
                    threeSheet.SetCellValue(index, 7, "");
                }
                #endregion

                #region Sheet4
                fourSheet.SetCellValue(index, 1, (ii + 1));
                string dctamtru = lstPupilProfile[ii].TempResidentalAddress;
                string dcthuongchu = lstPupilProfile[ii].PermanentResidentalAddress;
                //Địa chỉ liên lạc điền thông tin "Địa chỉ tạm trú", nếu không thì điền thông tin "Địa chỉ thường trú". Nếu không có cả 2 thì để trống
                if (!string.IsNullOrEmpty(dctamtru))
                {
                    fourSheet.SetCellValue(index, 2, dctamtru);
                }
                else if (!string.IsNullOrEmpty(dcthuongchu))
                {
                    fourSheet.SetCellValue(index, 2, dcthuongchu);
                }
                else { fourSheet.SetCellValue(index, 2, ""); }
                #endregion

                //Khoa sheet cho HS không phai trang thai dang hoc
                if (lstPupilProfile[ii].ProfileStatus != GlobalConstants.PUPIL_STATUS_STUDYING)
                {
                    threeSheet.GetRange(index, 1, index, 7).SetFontStyle(false, System.Drawing.Color.Red, false, null, true, false);
                    fourSheet.GetRange(index, 1, index, 3).SetFontStyle(false, System.Drawing.Color.Red, false, null, true, false);
                }
                //tao duong ke o
                threeSheet.GetRange(index, 1, index, 7).SetBorder(VTBorderStyle.Solid, VTBorderIndex.All);
                fourSheet.GetRange(index, 1, index, 3).SetBorder(VTBorderStyle.Solid, VTBorderIndex.All);
            }
            //threeSheet.DeleteRow(SystemParamsInFile.ROW1_SHEET2 - 1);
            //fourSheet.DeleteRow(SystemParamsInFile.ROW1_SHEET2 - 1);
            #endregion

            AcademicYear objAcademicYear = AcademicYearBusiness.Find(reportEC.AcademicYearID);
            int yearCurrent = objAcademicYear.Year;
            int partitionId = UtilsBusiness.GetPartionId(objAcademicYear.SchoolID, 100);

            #region Lay du lieu cho sheet 3 & sheet 4
            List<PupilAbsence> lstPupilAbsence = PupilAbsenceBusiness.All.Where(a => a.AcademicYearID == reportEC.AcademicYearID
                                                                                 && a.ClassID == reportEC.ClassID
                                                                                 && a.Last2digitNumberSchool == partitionId
                                                                                 ).ToList();

            List<InfoEvaluationPupil> lstInfoPupil = (from pt in PhysicalTestBusiness.All
                                                      join mn in MonitoringBookBusiness.All on pt.MonitoringBookID equals mn.MonitoringBookID
                                                      where mn.ClassID == reportEC.ClassID && mn.AcademicYearID == reportEC.AcademicYearID
                                                      orderby mn.HealthPeriodID descending
                                                      select new InfoEvaluationPupil
                                                      {
                                                          PupilID = mn.PupilID,
                                                          Height = pt.Height.HasValue ? pt.Height.Value : 0,
                                                          Weight = pt.Weight.HasValue ? pt.Weight.Value : 0,
                                                          EvaluationHealth = pt.PhysicalClassification.HasValue ? pt.PhysicalClassification.Value : 0
                                                      }).ToList();


            //Khai bao cac list du lieu su dung
            List<ReportSummedEvaluationBO> lstSummed = null;
            List<ReportEvaluationCommentsBO> lstEvaluation = null;

            //Kiem tra nam hien tai va gan list la current hoac history

            if (UtilsBusiness.IsMoveHistory(objAcademicYear))
            {
                //Thong tin danh gia cuoi ky
                lstSummed = (from pt in SummedEvaluationHistoryBusiness.All
                             where pt.LastDigitSchoolID == partitionId && pt.EvaluationID == 1 && pt.AcademicYearID == reportEC.AcademicYearID &&
                             pt.ClassID == reportEC.ClassID && pt.EvaluationCriteriaID == reportEC.SubjectID
                             select new ReportSummedEvaluationBO
                             {
                                 SummedEvaluationID = pt.SummedEvaluationID,
                                 PupilID = pt.PupilID,
                                 ClassID = pt.ClassID,
                                 EducationLevelID = pt.EducationLevelID,
                                 AcademicYearID = pt.AcademicYearID,
                                 LastDigitSchoolID = pt.LastDigitSchoolID,
                                 SchoolID = pt.SchoolID,
                                 SemesterID = pt.SemesterID,
                                 EvaluationCriteriaID = pt.EvaluationCriteriaID,
                                 EvaluationID = pt.EvaluationID,
                                 EndingEvaluation = pt.EndingEvaluation,
                                 EndingComments = pt.EndingComments,
                                 PeriodicEndingMark = pt.PeriodicEndingMark

                             }).ToList();
                //Nhan xét thang
                lstEvaluation = (from ec in EvaluationCommentsHistoryBusiness.All
                                 where ec.LastDigitSchoolID == partitionId && ec.TypeOfTeacher == 1 && ec.AcademicYearID == reportEC.AcademicYearID &&
                                 ec.ClassID == reportEC.ClassID && ec.EvaluationCriteriaID == reportEC.SubjectID
                                 select new ReportEvaluationCommentsBO
                                 {
                                     EvaluationCommentsID = ec.EvaluationCommentsID,
                                     PupilID = ec.PupilID,
                                     ClassID = ec.ClassID,
                                     EducationLevelID = ec.EducationLevelID,
                                     AcademicYearID = ec.AcademicYearID,
                                     LastDigitSchoolID = ec.LastDigitSchoolID,
                                     SchoolID = ec.SchoolID,
                                     TypeOfTeacher = ec.TypeOfTeacher,
                                     SemesterID = ec.SemesterID,
                                     EvaluationCriteriaID = ec.EvaluationCriteriaID,
                                     EvaluationID = ec.EvaluationID,
                                     CommentsM1M6 = ec.CommentsM1M6,
                                     CommentsM2M7 = ec.CommentsM2M7,
                                     CommentsM3M8 = ec.CommentsM3M8,
                                     CommentsM4M9 = ec.CommentsM4M9,
                                     CommentsM5M10 = ec.CommentsM5M10

                                 }).ToList();
            }
            else
            {
                lstSummed = (from pt in SummedEvaluationBusiness.All
                             where pt.LastDigitSchoolID == partitionId && pt.EvaluationID == 1 && pt.AcademicYearID == reportEC.AcademicYearID &&
                             pt.ClassID == reportEC.ClassID && pt.EvaluationCriteriaID == reportEC.SubjectID
                             select new ReportSummedEvaluationBO
                             {
                                 SummedEvaluationID = pt.SummedEvaluationID,
                                 PupilID = pt.PupilID,
                                 ClassID = pt.ClassID,
                                 EducationLevelID = pt.EducationLevelID,
                                 AcademicYearID = pt.AcademicYearID,
                                 LastDigitSchoolID = pt.LastDigitSchoolID,
                                 SchoolID = pt.SchoolID,
                                 SemesterID = pt.SemesterID,
                                 EvaluationCriteriaID = pt.EvaluationCriteriaID,
                                 EvaluationID = pt.EvaluationID,
                                 EndingEvaluation = pt.EndingEvaluation,
                                 EndingComments = pt.EndingComments,
                                 PeriodicEndingMark = pt.PeriodicEndingMark

                             }).ToList();

                lstEvaluation = (from ec in EvaluationCommentsBusiness.All
                                 where ec.LastDigitSchoolID == partitionId && ec.TypeOfTeacher == 1 && ec.AcademicYearID == reportEC.AcademicYearID &&
                                 ec.ClassID == reportEC.ClassID && ec.EvaluationCriteriaID == reportEC.SubjectID
                                 select new ReportEvaluationCommentsBO
                                 {
                                     EvaluationCommentsID = ec.EvaluationCommentsID,
                                     PupilID = ec.PupilID,
                                     ClassID = ec.ClassID,
                                     EducationLevelID = ec.EducationLevelID,
                                     AcademicYearID = ec.AcademicYearID,
                                     LastDigitSchoolID = ec.LastDigitSchoolID,
                                     SchoolID = ec.SchoolID,
                                     TypeOfTeacher = ec.TypeOfTeacher,
                                     SemesterID = ec.SemesterID,
                                     EvaluationCriteriaID = ec.EvaluationCriteriaID,
                                     EvaluationID = ec.EvaluationID,
                                     CommentsM1M6 = ec.CommentsM1M6,
                                     CommentsM2M7 = ec.CommentsM2M7,
                                     CommentsM3M8 = ec.CommentsM3M8,
                                     CommentsM4M9 = ec.CommentsM4M9,
                                     CommentsM5M10 = ec.CommentsM5M10

                                 }).ToList();
            }

            //Khai bao va lay thong tin cho tung hoc sinh 
            List<InfoEvaluationPupil> lstEvaluationPupil = new List<InfoEvaluationPupil>();
            PupilProfileBO objPupilProfile = null;
            InfoEvaluationPupil infoValuationPupil = null;
            for (int i = 0; i < lstPupilProfile.Count; i++)
            {
                objPupilProfile = lstPupilProfile[i];
                infoValuationPupil = new InfoEvaluationPupil();
                infoValuationPupil.FullName = objPupilProfile.FullName;
                infoValuationPupil.ClassID = objPupilProfile.CurrentClassID;
                infoValuationPupil.SchoolID = objPupilProfile.CurrentSchoolID;
                infoValuationPupil.PupilID = objPupilProfile.PupilProfileID;
                infoValuationPupil.ProfileStatus = objPupilProfile.ProfileStatus;

                //Lay thong tin ngay nghi 
                GetAbsence(infoValuationPupil, objPupilProfile, lstPupilAbsence);

                //Lay thong tin suc khoe
                GetHealthInfo(infoValuationPupil, lstInfoPupil);

                //Lay nhan xet theo thang cua GVBM
                GetEvaluationMonth_Subject(infoValuationPupil, objPupilProfile, reportEC, lstEvaluation);

                //Tong hop ket qua danh gia cua GVBM
                ResultEvaluation_Subject(infoValuationPupil, objPupilProfile, reportEC, lstSummed);

                lstEvaluationPupil.Add(infoValuationPupil);
            }

            #endregion

            #region Fill du lieu cho sheet 5 => 24

            #region Khai bao Sheet sheet 5 => 24
            IVTWorksheet fiveSheet = oBook.GetSheet(5);
            IVTWorksheet sixSheet = oBook.GetSheet(6);
            IVTWorksheet sevenSheet = oBook.GetSheet(7);
            IVTWorksheet eightSheet = oBook.GetSheet(8);
            IVTWorksheet nineSheet = oBook.GetSheet(9);
            IVTWorksheet tenSheet = oBook.GetSheet(10);
            IVTWorksheet elevenSheet = oBook.GetSheet(11);
            IVTWorksheet twelveSheet = oBook.GetSheet(12);
            IVTWorksheet thirteenSheet = oBook.GetSheet(13);
            IVTWorksheet fourteenSheet = oBook.GetSheet(14);
            IVTWorksheet fifteenSheet = oBook.GetSheet(15);
            IVTWorksheet sixteenSheet = oBook.GetSheet(16);
            IVTWorksheet seventeenSheet = oBook.GetSheet(17);
            IVTWorksheet eighteenSheet = oBook.GetSheet(18);
            IVTWorksheet nineteenSheet = oBook.GetSheet(19);
            IVTWorksheet twentySheet = oBook.GetSheet(20);
            IVTWorksheet twentyoneSheet = oBook.GetSheet(21);
            IVTWorksheet twentytwoSheet = oBook.GetSheet(22);
            IVTWorksheet twentythreeSheet = oBook.GetSheet(23);
            IVTWorksheet twentyfourSheet = oBook.GetSheet(24);
            #endregion

            int countEP = lstEvaluationPupil.Count;
            InfoEvaluationPupil pupil = null;
            for (int i = 0; i < countEP; i++)
            {
                pupil = lstEvaluationPupil[i];
                int index = 5 + i;
                fiveSheet.SetCellValue(index, 1, (i + 1));
                sixSheet.SetCellValue(index, 1, (i + 1));
                sevenSheet.SetCellValue(index, 1, (i + 1));
                eightSheet.SetCellValue(index, 1, (i + 1));
                nineSheet.SetCellValue(index, 1, (i + 1));
                tenSheet.SetCellValue(index, 1, (i + 1));
                elevenSheet.SetCellValue(index, 1, (i + 1));
                twelveSheet.SetCellValue(index, 1, (i + 1));
                thirteenSheet.SetCellValue(index, 1, (i + 1));
                fourteenSheet.SetCellValue(index, 1, (i + 1));
                fifteenSheet.SetCellValue(index, 1, (i + 1));
                sixteenSheet.SetCellValue(index, 1, (i + 1));
                seventeenSheet.SetCellValue(index, 1, (i + 1));
                eighteenSheet.SetCellValue(index, 1, (i + 1));
                nineteenSheet.SetCellValue(index, 1, (i + 1));
                twentySheet.SetCellValue(index, 1, (i + 1));
                twentyoneSheet.SetCellValue(index, 1, (i + 1));
                twentytwoSheet.SetCellValue(index, 1, (i + 1));
                twentythreeSheet.SetCellValue(index, 1, (i + 1));
                twentyfourSheet.SetCellValue(index, 1, (i + 1));

                //Duyet list va fill tu thang 1 den thang 10
                int countLEM = pupil.lstEvalutionMonth.Count;
                for (int j = 0; j < countLEM; j++)
                {
                    int month = pupil.lstEvalutionMonth[j].Month;
                    if (month == 1 && !String.IsNullOrEmpty(pupil.lstEvalutionMonth[j].Education))
                    {
                        fiveSheet.SetCellValue(index, 2, pupil.lstEvalutionMonth[j].Education);
                    }
                    if (month == 1 && !String.IsNullOrEmpty(pupil.lstEvalutionMonth[j].Fitness))
                    {
                        sixSheet.SetCellValue(index, 2, pupil.lstEvalutionMonth[j].Fitness);
                    }
                    if (month == 1 && !String.IsNullOrEmpty(pupil.lstEvalutionMonth[j].Quality))
                    {
                        sixSheet.SetCellValue(index, 3, pupil.lstEvalutionMonth[j].Quality);
                    }
                    if (month == 2 && !String.IsNullOrEmpty(pupil.lstEvalutionMonth[j].Education))
                    {
                        sevenSheet.SetCellValue(index, 2, pupil.lstEvalutionMonth[j].Education);
                    }
                    if (month == 2 && !String.IsNullOrEmpty(pupil.lstEvalutionMonth[j].Fitness))
                    {
                        eightSheet.SetCellValue(index, 2, pupil.lstEvalutionMonth[j].Fitness);
                    }
                    if (month == 2 && !String.IsNullOrEmpty(pupil.lstEvalutionMonth[j].Quality))
                    {
                        eightSheet.SetCellValue(index, 3, pupil.lstEvalutionMonth[j].Quality);
                    }
                    if (month == 3 && !String.IsNullOrEmpty(pupil.lstEvalutionMonth[j].Education))
                    {
                        nineSheet.SetCellValue(index, 2, pupil.lstEvalutionMonth[j].Education);
                    }
                    if (month == 3 && !String.IsNullOrEmpty(pupil.lstEvalutionMonth[j].Fitness))
                    {
                        tenSheet.SetCellValue(index, 2, pupil.lstEvalutionMonth[j].Fitness);
                    }
                    if (month == 3 && !String.IsNullOrEmpty(pupil.lstEvalutionMonth[j].Quality))
                    {
                        tenSheet.SetCellValue(index, 3, pupil.lstEvalutionMonth[j].Quality);
                    }
                    if (month == 4 && !String.IsNullOrEmpty(pupil.lstEvalutionMonth[j].Education))
                    {
                        elevenSheet.SetCellValue(index, 2, pupil.lstEvalutionMonth[j].Education);
                    }
                    if (month == 4 && !String.IsNullOrEmpty(pupil.lstEvalutionMonth[j].Fitness))
                    {
                        twelveSheet.SetCellValue(index, 2, pupil.lstEvalutionMonth[j].Fitness);
                    }
                    if (month == 4 && !String.IsNullOrEmpty(pupil.lstEvalutionMonth[j].Quality))
                    {
                        twelveSheet.SetCellValue(index, 3, pupil.lstEvalutionMonth[j].Quality);
                    }
                    if (month == 5 && !String.IsNullOrEmpty(pupil.lstEvalutionMonth[j].Education))
                    {
                        thirteenSheet.SetCellValue(index, 2, pupil.lstEvalutionMonth[j].Education);
                    }
                    if (month == 5 && !String.IsNullOrEmpty(pupil.lstEvalutionMonth[j].Fitness))
                    {
                        fourteenSheet.SetCellValue(index, 2, pupil.lstEvalutionMonth[j].Fitness);
                    }
                    if (month == 5 && !String.IsNullOrEmpty(pupil.lstEvalutionMonth[j].Quality))
                    {
                        fourteenSheet.SetCellValue(index, 3, pupil.lstEvalutionMonth[j].Quality);
                    }
                    if (month == 6 && !String.IsNullOrEmpty(pupil.lstEvalutionMonth[j].Education))
                    {
                        fifteenSheet.SetCellValue(index, 2, pupil.lstEvalutionMonth[j].Education);
                    }
                    if (month == 6 && !String.IsNullOrEmpty(pupil.lstEvalutionMonth[j].Fitness))
                    {
                        sixteenSheet.SetCellValue(index, 2, pupil.lstEvalutionMonth[j].Fitness);
                    }
                    if (month == 6 && !String.IsNullOrEmpty(pupil.lstEvalutionMonth[j].Quality))
                    {
                        sixteenSheet.SetCellValue(index, 3, pupil.lstEvalutionMonth[j].Quality);
                    }
                    if (month == 7 && !String.IsNullOrEmpty(pupil.lstEvalutionMonth[j].Education))
                    {
                        seventeenSheet.SetCellValue(index, 2, pupil.lstEvalutionMonth[j].Education);
                    }
                    if (month == 7 && !String.IsNullOrEmpty(pupil.lstEvalutionMonth[j].Fitness))
                    {
                        eighteenSheet.SetCellValue(index, 2, pupil.lstEvalutionMonth[j].Fitness);
                    }
                    if (month == 7 && !String.IsNullOrEmpty(pupil.lstEvalutionMonth[j].Quality))
                    {
                        eighteenSheet.SetCellValue(index, 3, pupil.lstEvalutionMonth[j].Quality);
                    }
                    if (month == 8 && !String.IsNullOrEmpty(pupil.lstEvalutionMonth[j].Education))
                    {
                        nineteenSheet.SetCellValue(index, 2, pupil.lstEvalutionMonth[j].Education);
                    }
                    if (month == 8 && !String.IsNullOrEmpty(pupil.lstEvalutionMonth[j].Fitness))
                    {
                        twentySheet.SetCellValue(index, 2, pupil.lstEvalutionMonth[j].Fitness);
                    }
                    if (month == 8 && !String.IsNullOrEmpty(pupil.lstEvalutionMonth[j].Quality))
                    {
                        twentySheet.SetCellValue(index, 3, pupil.lstEvalutionMonth[j].Quality);
                    }
                    if (month == 9 && !String.IsNullOrEmpty(pupil.lstEvalutionMonth[j].Education))
                    {
                        twentyoneSheet.SetCellValue(index, 2, pupil.lstEvalutionMonth[j].Education);
                    }
                    if (month == 9 && !String.IsNullOrEmpty(pupil.lstEvalutionMonth[j].Fitness))
                    {
                        twentytwoSheet.SetCellValue(index, 2, pupil.lstEvalutionMonth[j].Fitness);
                    }
                    if (month == 9 && !String.IsNullOrEmpty(pupil.lstEvalutionMonth[j].Quality))
                    {
                        twentytwoSheet.SetCellValue(index, 3, pupil.lstEvalutionMonth[j].Quality);
                    }
                    if (month == 10 && !String.IsNullOrEmpty(pupil.lstEvalutionMonth[j].Education))
                    {
                        twentythreeSheet.SetCellValue(index, 2, pupil.lstEvalutionMonth[j].Education);
                    }
                    if (month == 10 && !String.IsNullOrEmpty(pupil.lstEvalutionMonth[j].Fitness))
                    {
                        twentyfourSheet.SetCellValue(index, 2, pupil.lstEvalutionMonth[j].Fitness);
                    }
                    if (month == 10 && !String.IsNullOrEmpty(pupil.lstEvalutionMonth[j].Quality))
                    {
                        twentyfourSheet.SetCellValue(index, 3, pupil.lstEvalutionMonth[j].Quality);
                    }
                }
                //Khoa sheet cho HS không phai trang thai dang hoc
                if (lstPupilProfile[i].ProfileStatus != GlobalConstants.PUPIL_STATUS_STUDYING)
                {
                    fiveSheet.GetRange(index, 1, index, 2).SetFontStyle(false, System.Drawing.Color.Red, false, null, true, false);
                    sixSheet.GetRange(index, 1, index, 3).SetFontStyle(false, System.Drawing.Color.Red, false, null, true, false);
                    sevenSheet.GetRange(index, 1, index, 2).SetFontStyle(false, System.Drawing.Color.Red, false, null, true, false);
                    eightSheet.GetRange(index, 1, index, 3).SetFontStyle(false, System.Drawing.Color.Red, false, null, true, false);
                    nineSheet.GetRange(index, 1, index, 2).SetFontStyle(false, System.Drawing.Color.Red, false, null, true, false);
                    tenSheet.GetRange(index, 1, index, 3).SetFontStyle(false, System.Drawing.Color.Red, false, null, true, false);
                    elevenSheet.GetRange(index, 1, index, 2).SetFontStyle(false, System.Drawing.Color.Red, false, null, true, false);
                    twelveSheet.GetRange(index, 1, index, 3).SetFontStyle(false, System.Drawing.Color.Red, false, null, true, false);
                    thirteenSheet.GetRange(index, 1, index, 2).SetFontStyle(false, System.Drawing.Color.Red, false, null, true, false);
                    fourteenSheet.GetRange(index, 1, index, 3).SetFontStyle(false, System.Drawing.Color.Red, false, null, true, false);
                    fifteenSheet.GetRange(index, 1, index, 2).SetFontStyle(false, System.Drawing.Color.Red, false, null, true, false);
                    sixteenSheet.GetRange(index, 1, index, 3).SetFontStyle(false, System.Drawing.Color.Red, false, null, true, false);
                    seventeenSheet.GetRange(index, 1, index, 2).SetFontStyle(false, System.Drawing.Color.Red, false, null, true, false);
                    eighteenSheet.GetRange(index, 1, index, 3).SetFontStyle(false, System.Drawing.Color.Red, false, null, true, false);
                    nineteenSheet.GetRange(index, 1, index, 2).SetFontStyle(false, System.Drawing.Color.Red, false, null, true, false);
                    twentySheet.GetRange(index, 1, index, 3).SetFontStyle(false, System.Drawing.Color.Red, false, null, true, false);
                    twentyoneSheet.GetRange(index, 1, index, 2).SetFontStyle(false, System.Drawing.Color.Red, false, null, true, false);
                    twentytwoSheet.GetRange(index, 1, index, 3).SetFontStyle(false, System.Drawing.Color.Red, false, null, true, false);
                    twentythreeSheet.GetRange(index, 1, index, 2).SetFontStyle(false, System.Drawing.Color.Red, false, null, true, false);
                    twentyfourSheet.GetRange(index, 1, index, 3).SetFontStyle(false, System.Drawing.Color.Red, false, null, true, false);
                }
                #region tao duong ke o
                fiveSheet.GetRange(index, 1, index, 2).SetBorder(VTBorderStyle.Solid, VTBorderIndex.All);
                sixSheet.GetRange(index, 1, index, 3).SetBorder(VTBorderStyle.Solid, VTBorderIndex.All);
                sevenSheet.GetRange(index, 1, index, 2).SetBorder(VTBorderStyle.Solid, VTBorderIndex.All);
                eightSheet.GetRange(index, 1, index, 3).SetBorder(VTBorderStyle.Solid, VTBorderIndex.All);
                nineSheet.GetRange(index, 1, index, 2).SetBorder(VTBorderStyle.Solid, VTBorderIndex.All);
                tenSheet.GetRange(index, 1, index, 3).SetBorder(VTBorderStyle.Solid, VTBorderIndex.All);
                elevenSheet.GetRange(index, 1, index, 2).SetBorder(VTBorderStyle.Solid, VTBorderIndex.All);
                twelveSheet.GetRange(index, 1, index, 3).SetBorder(VTBorderStyle.Solid, VTBorderIndex.All);
                thirteenSheet.GetRange(index, 1, index, 2).SetBorder(VTBorderStyle.Solid, VTBorderIndex.All);
                fourteenSheet.GetRange(index, 1, index, 3).SetBorder(VTBorderStyle.Solid, VTBorderIndex.All);
                fifteenSheet.GetRange(index, 1, index, 2).SetBorder(VTBorderStyle.Solid, VTBorderIndex.All);
                sixteenSheet.GetRange(index, 1, index, 3).SetBorder(VTBorderStyle.Solid, VTBorderIndex.All);
                seventeenSheet.GetRange(index, 1, index, 2).SetBorder(VTBorderStyle.Solid, VTBorderIndex.All);
                eighteenSheet.GetRange(index, 1, index, 3).SetBorder(VTBorderStyle.Solid, VTBorderIndex.All);
                nineteenSheet.GetRange(index, 1, index, 2).SetBorder(VTBorderStyle.Solid, VTBorderIndex.All);
                twentySheet.GetRange(index, 1, index, 3).SetBorder(VTBorderStyle.Solid, VTBorderIndex.All);
                twentyoneSheet.GetRange(index, 1, index, 2).SetBorder(VTBorderStyle.Solid, VTBorderIndex.All);
                twentytwoSheet.GetRange(index, 1, index, 3).SetBorder(VTBorderStyle.Solid, VTBorderIndex.All);
                twentythreeSheet.GetRange(index, 1, index, 2).SetBorder(VTBorderStyle.Solid, VTBorderIndex.All);
                twentyfourSheet.GetRange(index, 1, index, 3).SetBorder(VTBorderStyle.Solid, VTBorderIndex.All);
                #endregion
            }
            #endregion

            #region Fill du lieu cho sheet 4
            //IVTWorksheet fourSheet = oBook.CopySheetToLast(oBook.GetSheet(4));
            //IVTRange rangeTitleSheet4 = fourSheet.GetRange(4, 1, 6, 14);
            //IVTRange rangeRowSheet4 = fourSheet.GetRow(SystemParamsInFile.ROW1_SHEET4_SUBJECTTEACHER - 2);
            //IVTRange rangeInfo = fourSheet.GetRow(SystemParamsInFile.ROW1_SHEET4_SUBJECTTEACHER - 1);

            //fourSheet.SetCellValue("C4", "Môn: " + subjectName);
            //int countSheet4 = lstEvaluationPupil.Count();

            //int rowFooter = lstPupilProfile.Count + (SystemParamsInFile.ROW1_SHEET4_SUBJECTTEACHER);

            //if (lstPupilProfile.Count > 19)
            //{
            //    fourSheet.CopyPasteSameSize(rangeTitleSheet4, SystemParamsInFile.ROW2_SHEET4_SUBJECTTEACHER - 3, 1);
            //    rowFooter = (lstPupilProfile.Count - 19) + (SystemParamsInFile.ROW2_SHEET4_SUBJECTTEACHER);
            //}
            //InfoEvaluationPupil iep = null;
            //ResultEvaluationSubject res = null;
            //for (int j = 0; j < countSheet4; j++)
            //{
            //    iep = lstEvaluationPupil[j];

            //    int rowSheet4 = 0;
            //    if (j <= 18)
            //    {
            //        rowSheet4 = SystemParamsInFile.ROW1_SHEET4_SUBJECTTEACHER;
            //    }
            //    if (j > 18)
            //    {
            //        rowSheet4 = SystemParamsInFile.ROW2_SHEET4_SUBJECTTEACHER - 19;
            //    }

            //    fourSheet.CopyPasteSameSize(rangeRowSheet4, rowSheet4 + j, 1);

            //    fourSheet.SetCellValue(rowSheet4 + j, 1, (j + 1)); //STT
            //    fourSheet.SetCellValue(rowSheet4 + j, 2, iep.FullName);

            //    for (int ii = iep.lstResultEvalution.Count() - 1; ii >= 0; ii--)//TO DO
            //    {
            //        res = iep.lstResultEvalution[ii];
            //        if (res.SemesterSubject == 1)
            //        {
            //            if (res.CheckResult.Equals("HT") == true)
            //            {
            //                fourSheet.SetCellValue(rowSheet4 + j, 3, "x");
            //            }
            //            if (res.CheckResult.Equals("CHT") == true)
            //            {
            //                fourSheet.SetCellValue(rowSheet4 + j, 4, "x");
            //            }
            //        }
            //        if (res.SemesterSubject == 2)
            //        {
            //            if (res.CheckResult.Equals("HT") == true)
            //            {
            //                fourSheet.SetCellValue(rowSheet4 + j, 5, "x");
            //            }
            //            if (res.CheckResult.Equals("CHT") == true)
            //            {
            //                fourSheet.SetCellValue(rowSheet4 + j, 6, "x");
            //            }
            //        }
            //    }
            //    //Neu HS khac dang hoc
            //    if (iep.ProfileStatus != GlobalConstants.PUPIL_STATUS_STUDYING)
            //    {
            //        fourSheet.GetRange(rowSheet4 + j, 1, rowSheet4 + j, 14).SetFontStyle(false, System.Drawing.Color.Red, false, null, true, false);
            //    }
            //}
            //fourSheet.CopyPasteSameSize(rangeInfo, "A" + rowFooter);
            //fourSheet.DeleteRow(SystemParamsInFile.ROW1_SHEET4_SUBJECTTEACHER - 2);
            //fourSheet.DeleteRow(SystemParamsInFile.ROW1_SHEET4_SUBJECTTEACHER - 2);

            //oBook.GetSheet(3).Delete();
            //oBook.GetSheet(3).Delete();
            //fourSheet.Name = "TongHopDanhGia";

            #endregion

            #region Fill du lieu cho sheet 25 va sheet 26
            IVTWorksheet twentyfiveSheet = oBook.GetSheet(25);
            IVTRange rangeTitleSheet25 = twentyfiveSheet.GetRange(4, 1, 5, 4);
            IVTRange rangeRowSheet25 = twentyfiveSheet.GetRow(SystemParamsInFile.ROW1_SHEET4_SUBJECTTEACHER - 3);
            IVTWorksheet twentysixSheet = oBook.GetSheet(26);
            IVTRange rangeTitleSheet26 = twentysixSheet.GetRange(4, 1, 6, 6);
            IVTRange rangeRowSheet26 = twentysixSheet.GetRow(SystemParamsInFile.ROW1_SHEET4_SUBJECTTEACHER - 2);
            IVTRange rangeInfo = twentysixSheet.GetRow(SystemParamsInFile.ROW1_SHEET4_SUBJECTTEACHER - 1);

            twentyfiveSheet.SetCellValue("C4", "Môn: " + subjectName);
            twentysixSheet.SetCellValue("C4", "Môn: " + subjectName);
            int countSheet4 = lstEvaluationPupil.Count();

            int rowFooter = lstPupilProfile.Count + (SystemParamsInFile.ROW1_SHEET4_SUBJECTTEACHER);

            /// if (lstPupilProfile.Count > 19)
            // {
            //    twentysixSheet.CopyPasteSameSize(rangeTitleSheet26, SystemParamsInFile.ROW2_SHEET4_SUBJECTTEACHER - 3, 1);
            //     rowFooter = (lstPupilProfile.Count - 19) + (SystemParamsInFile.ROW2_SHEET4_SUBJECTTEACHER);
            // }
            InfoEvaluationPupil iep = null;
            ResultEvaluationSubject res = null;
            int rowSheet25 = 6;
            for (int j = 0; j < countSheet4; j++)
            {
                iep = lstEvaluationPupil[j];

                int rowSheet4 = 0;
                rowSheet4 = SystemParamsInFile.ROW1_SHEET4_SUBJECTTEACHER;
                //if (j <= 34)
                //{
                //    rowSheet4 = SystemParamsInFile.ROW1_SHEET4_SUBJECTTEACHER;
                //}
                //if (j > 34)
                //{
                //    rowSheet4 = SystemParamsInFile.ROW2_SHEET4_SUBJECTTEACHER - 19;
                //}

                twentyfiveSheet.CopyPasteSameSize(rangeRowSheet25, rowSheet25 + j, 1);
                twentyfiveSheet.SetCellValue(rowSheet25 + j, 1, (j + 1)); //STT
                twentyfiveSheet.SetCellValue(rowSheet25 + j, 2, iep.FullName);

                twentysixSheet.CopyPasteSameSize(rangeRowSheet26, rowSheet4 + j, 1);
                twentysixSheet.SetCellValue(rowSheet4 + j, 1, (j + 1)); //STT
                twentysixSheet.SetCellValue(rowSheet4 + j, 2, iep.FullName);

                for (int ii = iep.lstResultEvalution.Count() - 1; ii >= 0; ii--)//TO DO
                {
                    res = iep.lstResultEvalution[ii];
                    if (res.SemesterSubject == 1)
                    {
                        if (res.PeriodicEndingMark != 0)
                        {
                            twentyfiveSheet.SetCellValue(rowSheet25 + j, 3, res.PeriodicEndingMark);
                        }
                        else
                        {
                            twentyfiveSheet.SetCellValue(rowSheet25 + j, 3, "");
                        }
                        if (res.CheckResult.Equals("HT") == true)
                        {
                            twentysixSheet.SetCellValue(rowSheet4 + j, 3, "x");
                        }
                        if (res.CheckResult.Equals("CHT") == true)
                        {
                            twentysixSheet.SetCellValue(rowSheet4 + j, 4, "x");
                        }
                    }
                    if (res.SemesterSubject == 2)
                    {
                        if (res.PeriodicEndingMark != 0)
                        {
                            twentyfiveSheet.SetCellValue(rowSheet25 + j, 4, res.PeriodicEndingMark);
                        }
                        else
                        {
                            twentyfiveSheet.SetCellValue(rowSheet25 + j, 4, "");
                        }
                        if (res.CheckResult.Equals("HT") == true)
                        {
                            twentysixSheet.SetCellValue(rowSheet4 + j, 5, "x");
                        }
                        if (res.CheckResult.Equals("CHT") == true)
                        {
                            twentysixSheet.SetCellValue(rowSheet4 + j, 6, "x");
                        }
                    }
                }
                //Neu HS khac dang hoc
                if (iep.ProfileStatus != GlobalConstants.PUPIL_STATUS_STUDYING)
                {
                    twentysixSheet.GetRange(rowSheet4 + j, 1, rowSheet4 + j, 14).SetFontStyle(false, System.Drawing.Color.Red, false, null, true, false);
                    twentyfiveSheet.GetRange(rowSheet25 + j, 1, rowSheet25 + j, 14).SetFontStyle(false, System.Drawing.Color.Red, false, null, true, false);
                }
            }
            // twentysixSheet.CopyPasteSameSize(rangeInfo, "A" + rowFooter);
            twentysixSheet.MergeRow(rowFooter, 1, 2);
            twentysixSheet.SetCellValue("A" + rowFooter, "Tổng cộng");
            twentysixSheet.GetRange(rowFooter,1,rowFooter,6).SetBorder(VTBorderStyle.Solid, VTBorderWeight.Thin, VTBorderIndex.All);
            twentysixSheet.GetRow(rowFooter).SetFontStyle(true, System.Drawing.Color.Black, false, null, false, false);
            twentysixSheet.SetFormulaValue("C" + (rowFooter), "=COUNTA(C7:C" + (rowFooter - 1) + ")");
            twentysixSheet.SetFormulaValue("D" + (rowFooter), "=COUNTA(D7:D" + (rowFooter - 1) + ")");
            twentysixSheet.SetFormulaValue("E" + (rowFooter), "=COUNTA(E7:E" + (rowFooter - 1) + ")");
            twentysixSheet.SetFormulaValue("F" + (rowFooter), "=COUNTA(F7:F" + (rowFooter - 1) + ")");
            twentysixSheet.DeleteRow(SystemParamsInFile.ROW1_SHEET4_SUBJECTTEACHER - 2);
            twentysixSheet.DeleteRow(SystemParamsInFile.ROW1_SHEET4_SUBJECTTEACHER - 2);
            #endregion

            return oBook.ToStream();
        }

        #region Ham dung cho SubjectTeacher
        private void ResultEvaluation_Subject(InfoEvaluationPupil infoValuationPupil, PupilProfileBO pp, ReportEvalutionComments reportEC, List<ReportSummedEvaluationBO> lstSummed)
        {
            List<ReportSummedEvaluationBO> lstSe = lstSummed.Where(u => u.PupilID == pp.PupilProfileID).ToList();

            infoValuationPupil.lstResultEvalution = new List<ResultEvaluationSubject>();
            ResultEvaluationSubject resultEs = null;
            ReportSummedEvaluationBO se = null;
            for (int i = 0; i < lstSe.Count; i++)
            {
                se = lstSe[i];

                resultEs = new ResultEvaluationSubject();

                if (se.SemesterID == 1)
                {
                    resultEs.SemesterSubject = 1;
                    if (se.EndingEvaluation != null)
                    {
                        resultEs.CheckResult = se.EndingEvaluation;
                        resultEs.PeriodicEndingMark = se.PeriodicEndingMark;
                    }
                }
                else
                {
                    resultEs.SemesterSubject = 2;
                    if (se.EndingEvaluation != null)
                    {
                        resultEs.CheckResult = se.EndingEvaluation;
                        resultEs.PeriodicEndingMark = se.PeriodicEndingMark;
                    }
                }
                infoValuationPupil.lstResultEvalution.Add(resultEs);
            }
        }

        private void GetEvaluationMonth_Subject(InfoEvaluationPupil infoValuationPupil, PupilProfileBO pp, ReportEvalutionComments reportEC, List<ReportEvaluationCommentsBO> lstEvaluation)
        {
            List<ReportEvaluationCommentsBO> lstEc = lstEvaluation.Where(u => u.PupilID == pp.PupilProfileID).ToList();

            infoValuationPupil.lstEvalutionMonth = new List<InfoEvaluationMonth>();

            for (int i = 0; i < lstEc.Count; i++)
            {
                GetDataListEvaluationMonth(lstEc[i], infoValuationPupil.lstEvalutionMonth);
            }
        }
        #endregion

        #endregion Bao cao theo doi CLGD_GVBM

        #region Ham chung cho GVCN_GVBM
        public string GetHashKey(ReportEvalutionComments reportEC)
        {
            IDictionary<string, object> dic = new Dictionary<string, object> {
                {"AppliedLevel", reportEC.AppliedLevel},
                {"ClassID ", reportEC.ClassID},
                {"SchoolID", reportEC.SchoolID},
                {"SubjectID", reportEC.SubjectID}
            };
            return ReportUtils.GetHashKey(dic);
        }

        private void GetHealthInfo(InfoEvaluationPupil infoValuationPupil, List<InfoEvaluationPupil> lstInfo)
        {
            List<InfoEvaluationPupil> list = lstInfo.Where(a => a.PupilID == infoValuationPupil.PupilID).ToList();
            if (list.Count > 0)
            {
                infoValuationPupil.Height = list.FirstOrDefault().Height;
                infoValuationPupil.Weight = list.FirstOrDefault().Weight;
                infoValuationPupil.EvaluationHealth = list.FirstOrDefault().EvaluationHealth;
            }
            else
            {
                infoValuationPupil.Height = 0;
                infoValuationPupil.Weight = 0;
                infoValuationPupil.EvaluationHealth = 0;
            }
        }

        private void GetAbsence(InfoEvaluationPupil infoValuationPupil, PupilProfileBO pp, List<PupilAbsence> lstAbsence)
        {
            int countAccepted = 0;
            int countNoAccepted = 0;

            countNoAccepted = lstAbsence.Where(a => a.IsAccepted == false && a.PupilID == pp.PupilProfileID).Count();
            countAccepted = lstAbsence.Where(a => a.IsAccepted == true && a.PupilID == pp.PupilProfileID).Count();

            infoValuationPupil.NumberAbsence = lstAbsence.Where(a => a.PupilID == pp.PupilProfileID).Count();
            infoValuationPupil.HasAccepted = countAccepted;
            infoValuationPupil.NoAccepted = countNoAccepted;
        }

        private void GetDataListEvaluationMonth(ReportEvaluationCommentsBO evaluationComments, List<InfoEvaluationMonth> lstInfoEvaMonth)
        {
            if (evaluationComments.SemesterID == 1)
            {
                if (evaluationComments.CommentsM1M6 != null)
                {
                    FillDataMonth(1, evaluationComments, evaluationComments.CommentsM1M6, lstInfoEvaMonth);
                }
                if (evaluationComments.CommentsM2M7 != null)
                {
                    FillDataMonth(2, evaluationComments, evaluationComments.CommentsM2M7, lstInfoEvaMonth);
                }
                if (evaluationComments.CommentsM3M8 != null)
                {
                    FillDataMonth(3, evaluationComments, evaluationComments.CommentsM3M8, lstInfoEvaMonth);
                }
                if (evaluationComments.CommentsM4M9 != null)
                {
                    FillDataMonth(4, evaluationComments, evaluationComments.CommentsM4M9, lstInfoEvaMonth);
                }
                if (evaluationComments.CommentsM5M10 != null)
                {
                    FillDataMonth(5, evaluationComments, evaluationComments.CommentsM5M10, lstInfoEvaMonth);
                }
            }
            else
            {
                if (evaluationComments.CommentsM1M6 != null)
                {
                    FillDataMonth(6, evaluationComments, evaluationComments.CommentsM1M6, lstInfoEvaMonth);
                }

                if (evaluationComments.CommentsM2M7 != null)
                {
                    FillDataMonth(7, evaluationComments, evaluationComments.CommentsM2M7, lstInfoEvaMonth);
                }

                if (evaluationComments.CommentsM3M8 != null)
                {
                    FillDataMonth(8, evaluationComments, evaluationComments.CommentsM3M8, lstInfoEvaMonth);
                }

                if (evaluationComments.CommentsM4M9 != null)
                {
                    FillDataMonth(9, evaluationComments, evaluationComments.CommentsM4M9, lstInfoEvaMonth);
                }

                if (evaluationComments.CommentsM5M10 != null)
                {
                    FillDataMonth(10, evaluationComments, evaluationComments.CommentsM5M10, lstInfoEvaMonth);
                }
            }
        }

        private void FillDataMonth(int month, ReportEvaluationCommentsBO ec, string evalutionComments, List<InfoEvaluationMonth> lstInfoEvaMonth)
        {
            InfoEvaluationMonth infoEvaMonth = new InfoEvaluationMonth();
            infoEvaMonth.Month = month;
            if (ec.TypeOfTeacher == 1)
            {
                FillDataMonth_Subject(ec, evalutionComments, infoEvaMonth);
            }
            if (ec.TypeOfTeacher == 2)
            {
                FillDataMonth_Subject(ec, evalutionComments, infoEvaMonth);
            }

            if (CheckMonthContains(lstInfoEvaMonth, infoEvaMonth.Month) != -1) // Co roi
            {
                int index = CheckMonthContains(lstInfoEvaMonth, infoEvaMonth.Month);
                InfoEvaluationMonth temp = lstInfoEvaMonth[index];
                if (temp.Education == null || temp.Education == string.Empty)
                {
                    lstInfoEvaMonth[index].Education = infoEvaMonth.Education;
                }
                if (temp.Fitness == null || temp.Fitness == string.Empty)
                {
                    lstInfoEvaMonth[index].Fitness = infoEvaMonth.Fitness;
                }
                if (temp.Quality == null || temp.Quality == string.Empty)
                {
                    lstInfoEvaMonth[index].Quality = infoEvaMonth.Quality;
                }
            }
            else // Chua co
            {
                lstInfoEvaMonth.Add(infoEvaMonth);
            }
        }

        private int CheckMonthContains(List<InfoEvaluationMonth> list, int month)
        {
            int index = -1;
            InfoEvaluationMonth objInfoEvaluation = null;
            for (int i = 0; i < list.Count; i++)
            {
                objInfoEvaluation = list[i];
                if (objInfoEvaluation.Month == month)
                {
                    return i;
                }
            }
            return index;
        }

        private void FillDataMonth_Subject(ReportEvaluationCommentsBO ec, string evalutionComments, InfoEvaluationMonth infoEvaMonth)
        {
            if (ec.EvaluationID == 1)
            {
                infoEvaMonth.Education = evalutionComments;
            }
            if (ec.EvaluationID == 2)
            {
                infoEvaMonth.Fitness = evalutionComments;
            }
            if (ec.EvaluationID == 3)
            {
                infoEvaMonth.Quality = evalutionComments;
            }
        }

        #endregion Ham chung dung cho GVCN_GVBM

        #endregion Quang_HoangAnh Bao cao theo doi CLGD

        public List<EvaluationCommentsReportBO> GetAllEvaluationCommentsMonthbyClassofHeadTeacher(int classId, IDictionary<string, object> dic)
        {
            int academicYearId = Utils.GetInt(dic["AcademicYearID"]);
            int semesterId = Utils.GetInt(dic["SemesterID"]);
            int typeOfTeacher = Utils.GetInt(dic["TypeOfTeacher"]);
            int schoolId = Utils.GetInt(dic["SchoolID"]);
            int partitionId = UtilsBusiness.GetPartionId(schoolId);
            dic.Add("ClassID", classId);
            AcademicYear objAcademicYear = AcademicYearBusiness.Find(academicYearId);
            if (UtilsBusiness.IsMoveHistory(objAcademicYear))//Neu da chuyen vao bang lich su thi lay du lieu tu bang lich su
            {
                List<EvaluationCommentsReportBO> listResultHistory = EvaluationCommentsHistoryBusiness.GetAllEvaluationCommentsMonthbyClassofHeadTeacher(classId, dic);
                return listResultHistory;
            }

            //list hoc sinh trong lop
            List<PupilOfClassBO> pupilOfClassList = PupilOfClassBusiness.GetPupilInClass(classId).OrderBy(c => c.OrderInClass).ThenBy(c => c.Name).ThenBy(c => c.PupilFullName).ToList();
            //list hoc sinh duoc danh gia.
            List<EvaluationComments> listEvaluationComments = new List<EvaluationComments>();

            listEvaluationComments = (from ev in EvaluationCommentsBusiness.All
                                      where
                                      ev.LastDigitSchoolID == partitionId
                                      && ev.AcademicYearID == academicYearId
                                       && ev.ClassID == classId
                                       && ev.SemesterID == semesterId
                                       && ev.TypeOfTeacher == typeOfTeacher
                                      select ev).ToList();

            //list hoc sinh cua lop da duoc danh gia va chua danh gia
            List<EvaluationCommentsReportBO> listResult = new List<EvaluationCommentsReportBO>();
            EvaluationCommentsReportBO evaluationBO = null;
            EvaluationComments objEducation;
            EvaluationComments objCapacities;
            EvaluationComments objQualities;
            int pupilID = 0;
            PupilOfClassBO pocObj;
            if (pupilOfClassList == null || pupilOfClassList.Count == 0)
            {
                return new List<EvaluationCommentsReportBO>();
            }
            List<EvaluationComments> listEvaluationEducation = listEvaluationComments.Where(s => s.EvaluationID == GlobalConstants.TAB_EDUCATION_BOOKMARK_PRIMARY).ToList();
            List<EvaluationComments> listEvaluationCapacities = listEvaluationComments.Where(s => s.EvaluationID == GlobalConstants.TAB_CAPACTIES_BOOKMARK_PRIMARY).ToList();
            List<EvaluationComments> listEvaluationQualities = listEvaluationComments.Where(s => s.EvaluationID == GlobalConstants.TAB_QUALITIES_BOOKMARK_PRIMARY).ToList();
            dic.Add("EvaluationID", GlobalConstants.TAB_EDUCATION_BOOKMARK_PRIMARY);
            listEvaluationEducation = this.SetCommentByGVBM(listEvaluationEducation, dic);
            dic["EvaluationID"] = GlobalConstants.TAB_CAPACTIES_BOOKMARK_PRIMARY;
            listEvaluationCapacities = this.SetCommentByGVBM(listEvaluationCapacities, dic);
            dic["EvaluationID"] = GlobalConstants.TAB_QUALITIES_BOOKMARK_PRIMARY;
            listEvaluationQualities = this.SetCommentByGVBM(listEvaluationQualities, dic);
            IDictionary<int, EvaluationComments> dicEvaluation;

            for (int i = 0; i < pupilOfClassList.Count; i++)
            {
                pocObj = pupilOfClassList[i];
                pupilID = pocObj.PupilID;
                evaluationBO = new EvaluationCommentsReportBO()
                {
                    Birthday = pocObj.Birthday,
                    FullName = pocObj.PupilFullName,
                    Name = pocObj.Name,
                    OrderInClass = pocObj.OrderInClass ?? 0,
                    PupilCode = pocObj.PupilCode,
                    PupilID = pocObj.PupilID,
                    Status = pocObj.Status,
                    EvaluationCriteriaID = 0
                };
                objEducation = listEvaluationEducation.FirstOrDefault(e => e.PupilID == pupilID);
                objCapacities = listEvaluationCapacities.FirstOrDefault(e => e.PupilID == pupilID);
                objQualities = listEvaluationQualities.FirstOrDefault(e => e.PupilID == pupilID);
                dicEvaluation = new Dictionary<int, EvaluationComments>();
                dicEvaluation.Add(GlobalConstants.TAB_EDUCATION_BOOKMARK_PRIMARY, objEducation);
                dicEvaluation.Add(GlobalConstants.TAB_CAPACTIES_BOOKMARK_PRIMARY, objCapacities);
                dicEvaluation.Add(GlobalConstants.TAB_QUALITIES_BOOKMARK_PRIMARY, objQualities);
                evaluationBO.DicEvaluation = dicEvaluation;
                listResult.Add(evaluationBO);
            }


            return listResult;
        }

        public List<EvaluationCommentsReportBO> GetAllEvaluationCommentsbyClassAndHeadTeacher(int classId, IDictionary<string, object> dic, List<int> lstSubject)
        {
            int academicYearId = Utils.GetInt(dic["AcademicYearID"]);
            int semesterId = Utils.GetInt(dic["SemesterID"]);
            int typeOfTeacher = Utils.GetInt(dic["TypeOfTeacher"]);
            int schoolId = Utils.GetInt(dic["SchoolID"]);
            dic.Add("ClassID", classId);
            int partitionId = UtilsBusiness.GetPartionId(schoolId);
            AcademicYear objAcademicYear = AcademicYearBusiness.Find(academicYearId);
            if (UtilsBusiness.IsMoveHistory(objAcademicYear))//Neu da chuyen vao bang lich su thi lay du lieu tu bang lich su
            {
                List<EvaluationCommentsReportBO> listResultHistory = EvaluationCommentsHistoryBusiness.GetAllEvaluationCommentsbyClassAndHeadTeacher(classId, dic, lstSubject);
                return listResultHistory;
            }

            //list hoc sinh trong lop
            List<PupilOfClassBO> pupilOfClassList = PupilOfClassBusiness.GetPupilInClass(classId).OrderBy(p => p.OrderInClass).ThenBy(p => p.Name).ThenBy(p => p.PupilFullName).ToList();
            //list hoc sinh duoc danh gia.
            List<EvaluationComments> listEvaluationComments = new List<EvaluationComments>();
            listEvaluationComments = (from ev in EvaluationCommentsBusiness.All
                                      where
                                      ev.LastDigitSchoolID == partitionId
                                      && ev.AcademicYearID == academicYearId
                                       && ev.ClassID == classId
                                       && ev.SemesterID == semesterId
                                       && ev.TypeOfTeacher == typeOfTeacher
                                      select ev).ToList();

            List<SummedEvaluation> listSummedEvaluation = (from ev in SummedEvaluationBusiness.All
                                                           where
                                                            ev.LastDigitSchoolID == partitionId
                                                            && ev.AcademicYearID == academicYearId
                                                            && ev.ClassID == classId
                                                            && ev.SemesterID == semesterId
                                                           select ev).ToList();

            //Neu la GVCN thi lay danh gia nhat xet cuoi ky cho mat danh gia Nang luc, pham chat
            List<SummedEndingEvaluation> lstSummedEndingEvaluation = new List<SummedEndingEvaluation>();
            lstSummedEndingEvaluation = SummedEndingEvaluationBusiness.All.Where(c => c.LastDigitSchoolID == partitionId
                                                                                     && c.AcademicYearID == academicYearId
                                                                                     && c.ClassID == classId
                                                                                     && c.SemesterID == semesterId
                                                                                     ).ToList();

            //list hoc sinh cua lop da duoc danh gia va chua danh gia
            List<EvaluationCommentsReportBO> listResult = new List<EvaluationCommentsReportBO>();
            EvaluationCommentsReportBO evaluationBO = null;
            EvaluationComments objEducation;
            EvaluationComments objCapacities;
            EvaluationComments objQualities;
            SummedEvaluation objSummedEvaluationEducation = null;
            SummedEndingEvaluation objSummedEndingEvaluation = null;
            int pupilID = 0;
            PupilOfClassBO pocObj;
            if (pupilOfClassList == null || pupilOfClassList.Count == 0)
            {
                return new List<EvaluationCommentsReportBO>();
            }
            int subjectId = 0;
            List<EvaluationComments> listEvaluationEducation = new List<EvaluationComments>();//Mon hoc va HDGD
            List<EvaluationComments> listEvaluationCapacities = new List<EvaluationComments>();//Nang luc
            List<EvaluationComments> listEvaluationQualities = new List<EvaluationComments>();//Nang luc
            List<SummedEvaluation> listSummedEvaluationbySubject = new List<SummedEvaluation>();
            IDictionary<int, EvaluationComments> dicEvaluation;
            IDictionary<int, SummedEvaluation> dicSummedEvaluation;
            IDictionary<int, SummedEndingEvaluation> dicSummedEndingEvaluation;

            listEvaluationEducation = listEvaluationComments.Where(e => e.EvaluationID == GlobalConstants.TAB_EDUCATION_BOOKMARK_PRIMARY).ToList();
            listEvaluationCapacities = listEvaluationComments.Where(e => e.EvaluationID == GlobalConstants.TAB_CAPACTIES_BOOKMARK_PRIMARY).ToList();
            listEvaluationQualities = listEvaluationComments.Where(e => e.EvaluationID == GlobalConstants.TAB_QUALITIES_BOOKMARK_PRIMARY).ToList();
            for (int sc = 0; sc < lstSubject.Count; sc++)
            {
                subjectId = lstSubject[sc];

                listSummedEvaluationbySubject = listSummedEvaluation.Where(e => e.EvaluationCriteriaID == subjectId && e.EvaluationID == GlobalConstants.TAB_EDUCATION).ToList();
                for (int i = 0; i < pupilOfClassList.Count; i++)
                {
                    pocObj = pupilOfClassList[i];
                    pupilID = pocObj.PupilID;
                    evaluationBO = new EvaluationCommentsReportBO()
                    {
                        Birthday = pocObj.Birthday,
                        FullName = pocObj.PupilFullName,
                        Name = pocObj.Name,
                        OrderInClass = pocObj.OrderInClass ?? 0,
                        PupilCode = pocObj.PupilCode,
                        PupilID = pocObj.PupilID,
                        Status = pocObj.Status,
                        EvaluationCriteriaID = subjectId
                    };
                    objEducation = listEvaluationEducation.FirstOrDefault(e => e.PupilID == pupilID);
                    objCapacities = listEvaluationCapacities.FirstOrDefault(e => e.PupilID == pupilID);
                    objQualities = listEvaluationQualities.FirstOrDefault(e => e.PupilID == pupilID);
                    dicEvaluation = new Dictionary<int, EvaluationComments>();
                    dicEvaluation.Add(GlobalConstants.TAB_EDUCATION_BOOKMARK_PRIMARY, objEducation);
                    dicEvaluation.Add(GlobalConstants.TAB_CAPACTIES_BOOKMARK_PRIMARY, objCapacities);
                    dicEvaluation.Add(GlobalConstants.TAB_QUALITIES_BOOKMARK_PRIMARY, objQualities);
                    evaluationBO.DicEvaluation = dicEvaluation;
                    //Lay nhan xet cuoi ky, 
                    objSummedEvaluationEducation = listSummedEvaluationbySubject.FirstOrDefault(s => s.PupilID == pupilID && s.EvaluationID == GlobalConstants.TAB_EDUCATION_BOOKMARK_PRIMARY);

                    dicSummedEvaluation = new Dictionary<int, SummedEvaluation>();
                    dicSummedEvaluation.Add(GlobalConstants.TAB_EDUCATION_BOOKMARK_PRIMARY, objSummedEvaluationEducation);
                    evaluationBO.DicSummedEvaluation = dicSummedEvaluation;
                    listResult.Add(evaluationBO);
                }
            }

            //Dien du lieu nang luc, pham chat cuoi ky
            List<EvaluationCriteria> lstEC = EvaluationCriteriaBusiness.getEvaluationCriteria();
            EvaluationCriteria objEvaluationCriteria;
            SummedEvaluation objSummedEvaluationCapacities = null;
            for (int i = 0; i < pupilOfClassList.Count; i++)
            {
                pocObj = pupilOfClassList[i];
                pupilID = pocObj.PupilID;
                evaluationBO = new EvaluationCommentsReportBO()
                {
                    Birthday = pocObj.Birthday,
                    FullName = pocObj.PupilFullName,
                    Name = pocObj.Name,
                    OrderInClass = pocObj.OrderInClass ?? 0,
                    PupilCode = pocObj.PupilCode,
                    PupilID = pocObj.PupilID,
                    Status = pocObj.Status,
                    EvaluationCriteriaID = -1
                };
                dicSummedEvaluation = new Dictionary<int, SummedEvaluation>();
                for (int j = 0; j < lstEC.Count; j++)
                {
                    objEvaluationCriteria = lstEC[j];
                    objSummedEvaluationCapacities = listSummedEvaluation.FirstOrDefault(e => e.PupilID == pupilID
                                                                && e.EvaluationCriteriaID == objEvaluationCriteria.EvaluationCriteriaID
                                                                && e.EvaluationID > GlobalConstants.TAB_EDUCATION);
                    dicSummedEvaluation.Add(objEvaluationCriteria.EvaluationCriteriaID, objSummedEvaluationCapacities);
                }
                evaluationBO.CapacitesOrQualitiesSemester = dicSummedEvaluation;
                if (lstSummedEndingEvaluation.Count > 0)
                {
                    dicSummedEndingEvaluation = new Dictionary<int, SummedEndingEvaluation>();
                    //nang luc
                    objSummedEndingEvaluation = lstSummedEndingEvaluation.FirstOrDefault(s => s.PupilID == pupilID && s.EvaluationID == GlobalConstants.TAB_CAPACTIES_BOOKMARK_PRIMARY);
                    dicSummedEndingEvaluation.Add(GlobalConstants.TAB_CAPACTIES_BOOKMARK_PRIMARY, objSummedEndingEvaluation);

                    objSummedEndingEvaluation = lstSummedEndingEvaluation.FirstOrDefault(s => s.PupilID == pupilID && s.EvaluationID == GlobalConstants.TAB_QUALITIES_BOOKMARK_PRIMARY);
                    dicSummedEndingEvaluation.Add(GlobalConstants.TAB_QUALITIES_BOOKMARK_PRIMARY, objSummedEndingEvaluation);
                    evaluationBO.DicSummedEndingEvaluation = dicSummedEndingEvaluation;
                }
                else
                {
                    dicSummedEndingEvaluation = new Dictionary<int, SummedEndingEvaluation>();
                    //nang luc
                    dicSummedEndingEvaluation.Add(GlobalConstants.TAB_CAPACTIES_BOOKMARK_PRIMARY, new SummedEndingEvaluation());
                    dicSummedEndingEvaluation.Add(GlobalConstants.TAB_QUALITIES_BOOKMARK_PRIMARY, new SummedEndingEvaluation());
                    evaluationBO.DicSummedEndingEvaluation = dicSummedEndingEvaluation;
                }
                listResult.Add(evaluationBO);
            }

            return listResult;
        }

        public List<string> GetListComment(IDictionary<string, object> dic)
        {
            int SchoolID = Utils.GetInt(dic, "SchoolID");
            int AcademicYearID = Utils.GetInt(dic, "AcademicYearID");
            int EvaluationID = Utils.GetInt(dic, "EvaluationID");
            int TypeOfTeacher = Utils.GetInt(dic, "TypeOfTeacherID");
            int ClassID = Utils.GetInt(dic, "ClassID");
            int SemesterID = Utils.GetInt(dic, "SemesterID");
            int Month = Utils.GetInt(dic, "MonthID");
            int partition = UtilsBusiness.GetPartionId(SchoolID);
            List<string> lstComment = new List<string>();

            IQueryable<EvaluationComments> lstEvaluationComments = (from e in EvaluationCommentsBusiness.All
                                                                    where e.SchoolID == SchoolID
                                                                    && e.AcademicYearID == AcademicYearID
                                                                    && e.ClassID == ClassID
                                                                    && e.EvaluationID == EvaluationID
                                                                    && e.TypeOfTeacher == TypeOfTeacher
                                                                    && (e.SemesterID == SemesterID || SemesterID == 0)
                                                                    && e.LastDigitSchoolID == partition
                                                                    select e);
            if (Month == 1 || Month == 6)
            {
                lstComment = lstEvaluationComments.Where(p => !string.IsNullOrEmpty(p.CommentsM1M6)).Select(p => p.CommentsM1M6).Distinct().ToList();
            }
            else if (Month == 2 || Month == 7)
            {
                lstComment = lstEvaluationComments.Where(p => !string.IsNullOrEmpty(p.CommentsM2M7)).Select(p => p.CommentsM2M7).Distinct().ToList();
            }
            else if (Month == 3 || Month == 8)
            {
                lstComment = lstEvaluationComments.Where(p => !string.IsNullOrEmpty(p.CommentsM3M8)).Select(p => p.CommentsM3M8).Distinct().ToList();
            }
            else if (Month == 4 || Month == 9)
            {
                lstComment = lstEvaluationComments.Where(p => !string.IsNullOrEmpty(p.CommentsM4M9)).Select(p => p.CommentsM4M9).Distinct().ToList();
            }
            else if (Month == 5 || Month == 10)
            {
                lstComment = lstEvaluationComments.Where(p => !string.IsNullOrEmpty(p.CommentsM5M10)).Select(p => p.CommentsM5M10).Distinct().ToList();
            }
            else
            {
                List<string> ListStrTmp = new List<string>();
                lstComment = lstEvaluationComments.Where(p => !string.IsNullOrEmpty(p.CommentsM1M6)).Select(p => p.CommentsM1M6).Distinct().ToList();

                ListStrTmp = lstEvaluationComments.Where(p => !string.IsNullOrEmpty(p.CommentsM2M7)).Select(p => p.CommentsM2M7).Distinct().ToList();
                if (ListStrTmp != null && ListStrTmp.Count > 0) lstComment.AddRange(ListStrTmp);

                ListStrTmp = lstEvaluationComments.Where(p => !string.IsNullOrEmpty(p.CommentsM3M8)).Select(p => p.CommentsM3M8).Distinct().ToList();
                if (ListStrTmp != null && ListStrTmp.Count > 0) lstComment.AddRange(ListStrTmp);

                ListStrTmp = lstEvaluationComments.Where(p => !string.IsNullOrEmpty(p.CommentsM4M9)).Select(p => p.CommentsM4M9).Distinct().ToList();
                if (ListStrTmp != null && ListStrTmp.Count > 0) lstComment.AddRange(ListStrTmp);

                ListStrTmp = lstEvaluationComments.Where(p => !string.IsNullOrEmpty(p.CommentsM5M10)).Select(p => p.CommentsM5M10).Distinct().ToList();
                if (ListStrTmp != null && ListStrTmp.Count > 0) lstComment.AddRange(ListStrTmp);

                lstComment = lstComment.Distinct().ToList();
            }

            return lstComment.OrderBy(p => p).ToList();
        }

        public IQueryable<EvaluationComments> getListEvaluationByListSubject(IDictionary<string, object> dic)
        {
            int schoolId = Utils.GetInt(dic["SchoolId"]);
            int partitionId = UtilsBusiness.GetPartionId(schoolId);
            int academicYearId = Utils.GetInt(dic["AcademicYearId"]);
            int semesterId = Utils.GetInt(dic["SemesterId"]);
            List<int> listSubjectId = Utils.GetIntList(dic["listSubjectId"]);
            int evaluationId = Utils.GetInt(dic["EvaluationId"]);
            int typeOfTeacher = Utils.GetInt(dic["TypeOfTeacher"]);
            int educationLevelId = Utils.GetInt(dic["EduationLevelId"]);
            int classId = Utils.GetInt(dic["ClassId"]);

            IQueryable<EvaluationComments> iQuery = this.All;
            if (schoolId > 0)
            {
                iQuery = iQuery.Where(p => p.SchoolID == schoolId);
            }
            if (partitionId > 0)
            {
                iQuery = iQuery.Where(p => p.LastDigitSchoolID == partitionId);
            }
            if (academicYearId > 0)
            {
                iQuery = iQuery.Where(p => p.AcademicYearID == academicYearId);
            }
            if (semesterId > 0)
            {
                iQuery = iQuery.Where(p => p.SemesterID == semesterId);
            }
            if (evaluationId > 0)
            {
                iQuery = iQuery.Where(p => p.EvaluationID == evaluationId);
            }
            if (typeOfTeacher > 0)
            {
                iQuery = iQuery.Where(p => p.TypeOfTeacher == typeOfTeacher);
            }
            if (educationLevelId > 0)
            {
                iQuery = iQuery.Where(p => p.EducationLevelID == educationLevelId);
            }
            if (classId > 0)
            {
                iQuery = iQuery.Where(p => p.ClassID == classId);
            }
            if (listSubjectId != null && listSubjectId.Count > 0)
            {
                iQuery = iQuery.Where(p => listSubjectId.Contains(p.EvaluationCriteriaID));
            }

            return iQuery;
        }

        public void CopyDataEvaluation(IDictionary<string, object> objDic, int iCopyObject, int iCopyLocation, int? month, int subjectIdSelect, ref List<ActionAuditDataBO> lstActionAuditData)
        {
            int academicYearId = Utils.GetInt(objDic["AcademicYearId"]);
            AcademicYear acaObj = AcademicYearBusiness.Find(academicYearId);
            if (UtilsBusiness.IsMoveHistory(acaObj))
            {
                EvaluationCommentsHistoryBusiness.CopyDataEvaluationHitory(objDic, iCopyObject, iCopyLocation, month, subjectIdSelect, ref lstActionAuditData);
            }
            else
            {
                int schoolId = Utils.GetInt(objDic["SchoolId"]);
                int partitionId = UtilsBusiness.GetPartionId(schoolId);
                int classId = Utils.GetInt(objDic["ClassId"]);
                int educationLevelId = Utils.GetInt(objDic["EduationLevelId"]);
                int evaluationID = Utils.GetInt(objDic["EvaluationId"]);
                int semeseterID = Utils.GetInt(objDic["SemesterId"]);
                int typeOfTeacher = Utils.GetInt(objDic["TypeOfTeacher"]);
                //Danh sach hoc sinh dang hoc của lop
                IDictionary<string, object> dic = new Dictionary<string, object>();
                dic["ClassID"] = Utils.GetInt(objDic["ClassId"]);
                dic["Check"] = "Check";
                dic["Status"] = Utils.GetInt(objDic["Status"]);
                List<int> listSubjectID = Utils.GetIntList(objDic["listSubjectId"]);
                #region Tao du lieu ghi log
                StringBuilder objectIDStrtmp = new StringBuilder();
                StringBuilder descriptionStrtmp = new StringBuilder();
                StringBuilder paramsStrtmp = new StringBuilder();
                StringBuilder oldObjectStrtmp = new StringBuilder();
                StringBuilder newObjectStrtmp = new StringBuilder();
                StringBuilder userFuntionsStrtmp = new StringBuilder();
                StringBuilder userActionsStrtmp = new StringBuilder();
                StringBuilder userDescriptionsStrtmp = new StringBuilder();
                ActionAuditDataBO objAc = null;
                bool isChange = false;

                List<PupilOfClassBO> lstPOC = (from poc in PupilOfClassBusiness.SearchBySchool(schoolId, dic)
                                               join pf in PupilProfileBusiness.All on poc.PupilID equals pf.PupilProfileID
                                               join cp in ClassProfileBusiness.All on poc.ClassID equals cp.ClassProfileID
                                               where poc.AcademicYearID == academicYearId
                                               && poc.SchoolID == schoolId
                                               && poc.ClassID == classId
                                               && cp.IsActive.Value
                                               select new PupilOfClassBO
                                               {
                                                   PupilID = poc.PupilID,
                                                   PupilCode = pf.PupilCode,
                                                   ClassID = poc.ClassID,
                                                   ClassName = cp.DisplayName,
                                                   PupilFullName = pf.FullName
                                               }).ToList();
                PupilOfClassBO objPOC = null;

                List<SubjectCat> lstSC = (from sc in SubjectCatBusiness.All
                                          where sc.IsActive
                                          && listSubjectID.Contains(sc.SubjectCatID)
                                          select sc).ToList();
                SubjectCat objSC = null;

                string evaluationName = evaluationID == GlobalConstants.TAB_EDUCATION_BOOKMARK_PRIMARY ? "Môn học & HĐGD" :
                                        evaluationID == GlobalConstants.TAB_CAPACTIES_BOOKMARK_PRIMARY ? "Năng lực" : "Phẩm chất";
                #endregion
                List<int> listPupilId = lstPOC.Select(p => p.PupilID).ToList();
                //Danh sach tat ca hoc sinh co trong table nhan xet
                objDic["listSubjectId"] = null;
                List<EvaluationComments> ListEvaluationComment = new List<EvaluationComments>();
                if (iCopyLocation > 0)
                {
                    ListEvaluationComment = this.getListEvaluationByListSubject(objDic).Where(p => listPupilId.Contains(p.PupilID)).ToList();
                }
                List<EvaluationComments> listEvaSource = new List<EvaluationComments>();
                EvaluationComments evaluationCommentObj = null;

                List<EvaluationOfPupilBO> listEvaluationOfPupil = new List<EvaluationOfPupilBO>();
                EvaluationOfPupilBO evaluationTmp = null;

                int subjectID = 0;
                int pupilID = 0;
                //Lay thong tin khoa cot diem
                string strLockMark = this.CheckLockMark(classId, schoolId, academicYearId);
                strLockMark = strLockMark.Replace("T10", "TH10");
                if (iCopyObject == 1 && iCopyLocation == 1 && month != 11)
                {
                    #region TH Ghi de nhan xet cua tat ca hoc sinh,sao chep nhan xet cua thang
                    //Danh sach nhan xet cua thang voi mon hoc duoc chon
                    listEvaSource = ListEvaluationComment.Where(p => p.EvaluationCriteriaID == subjectIdSelect).ToList();
                    List<int> listPupilIDEva = listEvaSource.Select(p => p.PupilID).Distinct().ToList();

                    for (int j = 0; j < listEvaSource.Count; j++)
                    {
                        evaluationCommentObj = listEvaSource[j];
                        evaluationTmp = new EvaluationOfPupilBO();
                        evaluationTmp.PupilID = evaluationCommentObj.PupilID;
                        if (month.Value == GlobalConstants.MonthID_1 || month.Value == GlobalConstants.MonthID_6)
                        {
                            evaluationTmp.EvaluationComment = evaluationCommentObj.CommentsM1M6;
                        }
                        else if (month.Value == GlobalConstants.MonthID_2 || month.Value == GlobalConstants.MonthID_7)
                        {
                            evaluationTmp.EvaluationComment = evaluationCommentObj.CommentsM2M7;
                        }
                        else if (month.Value == GlobalConstants.MonthID_3 || month.Value == GlobalConstants.MonthID_8)
                        {
                            evaluationTmp.EvaluationComment = evaluationCommentObj.CommentsM3M8;
                        }
                        else if (month.Value == GlobalConstants.MonthID_4 || month.Value == GlobalConstants.MonthID_9)
                        {
                            evaluationTmp.EvaluationComment = evaluationCommentObj.CommentsM4M9;
                        }
                        else if (month.Value == GlobalConstants.MonthID_5 || month.Value == GlobalConstants.MonthID_10)
                        {
                            evaluationTmp.EvaluationComment = evaluationCommentObj.CommentsM5M10;
                        }
                        listEvaluationOfPupil.Add(evaluationTmp);
                    }

                    //Kiem tra co trong DB thi update khong co thi insert
                    //For qua danh danh sach mon hoc can cap nhat nhan xet
                    string strEvalutionpattern = string.Empty;

                    for (int i = 0; i < listSubjectID.Count; i++)
                    {
                        subjectID = listSubjectID[i];
                        //Danh sach nhan xet co trong DB
                        List<EvaluationComments> listEvaluationComment = ListEvaluationComment.Where(p => p.EvaluationCriteriaID == subjectID).ToList();
                        EvaluationComments ObjEvaluationTmp = null;
                        for (int j = 0; j < listPupilIDEva.Count; j++)
                        {
                            isChange = false;
                            strEvalutionpattern = listEvaluationOfPupil.Where(p => p.PupilID == listPupilIDEva[j]).Select(p => p.EvaluationComment).FirstOrDefault();
                            ObjEvaluationTmp = listEvaluationComment.Where(p => p.PupilID == listPupilIDEva[j]).FirstOrDefault();//nhan xet cua 1 hoc sinh
                            pupilID = listPupilIDEva[j];
                            objSC = lstSC.Where(p => p.SubjectCatID == subjectID).FirstOrDefault();
                            objPOC = lstPOC.Where(p => p.PupilID == pupilID).FirstOrDefault();
                            objectIDStrtmp.Append(pupilID);
                            descriptionStrtmp.Append("Cập nhật sổ TD CLGD GVBM");
                            paramsStrtmp.Append(pupilID);
                            userFuntionsStrtmp.Append("Sổ TD CLGD (GVBM)");
                            userActionsStrtmp.Append(SMAS.Business.Common.GlobalConstants.ACTION_UPDATE);
                            userDescriptionsStrtmp.Append("Sao chép nhận xét đánh giá " + evaluationName + " HS " + objPOC.PupilFullName + ", " + "mã " + objPOC.PupilCode + ", ");
                            userDescriptionsStrtmp.Append("Lớp ").Append(objPOC.ClassName + "/" + objSC.DisplayName + "/" + "Tháng " + month + "/Học kỳ " + semeseterID + "/Năm học " + acaObj.Year + "-" + (acaObj.Year + 1) + ". ");

                            if (ObjEvaluationTmp != null)
                            {
                                //update
                                if (semeseterID == GlobalConstants.SEMESTER_OF_YEAR_FIRST && !strLockMark.Contains("HK1"))
                                {
                                    if (month.Value == GlobalConstants.MonthID_1 && !strLockMark.Contains("T1") && ObjEvaluationTmp.CommentsM1M6 != strEvalutionpattern)
                                    {
                                        oldObjectStrtmp.Append(CommentStr).Append(ObjEvaluationTmp.CommentsM1M6).Append("; ");
                                        ObjEvaluationTmp.CommentsM1M6 = strEvalutionpattern;
                                        isChange = true;
                                    }
                                    else if (month.Value == GlobalConstants.MonthID_2 && !strLockMark.Contains("T2") && ObjEvaluationTmp.CommentsM2M7 != strEvalutionpattern)
                                    {
                                        oldObjectStrtmp.Append(CommentStr).Append(ObjEvaluationTmp.CommentsM1M6).Append("; ");
                                        ObjEvaluationTmp.CommentsM2M7 = strEvalutionpattern;
                                        isChange = true;
                                    }
                                    else if (month.Value == GlobalConstants.MonthID_3 && !strLockMark.Contains("T3") && ObjEvaluationTmp.CommentsM3M8 != strEvalutionpattern)
                                    {
                                        oldObjectStrtmp.Append(CommentStr).Append(ObjEvaluationTmp.CommentsM1M6).Append("; ");
                                        ObjEvaluationTmp.CommentsM3M8 = strEvalutionpattern;
                                        isChange = true;
                                    }
                                    else if (month.Value == GlobalConstants.MonthID_4 && !strLockMark.Contains("T4") && ObjEvaluationTmp.CommentsM4M9 != strEvalutionpattern)
                                    {
                                        oldObjectStrtmp.Append(CommentStr).Append(ObjEvaluationTmp.CommentsM1M6).Append("; ");
                                        ObjEvaluationTmp.CommentsM4M9 = strEvalutionpattern;
                                        isChange = true;
                                    }
                                    else if (month.Value == GlobalConstants.MonthID_5 && !strLockMark.Contains("T5") && ObjEvaluationTmp.CommentsM5M10 != strEvalutionpattern)
                                    {
                                        oldObjectStrtmp.Append(CommentStr).Append(ObjEvaluationTmp.CommentsM1M6).Append("; ");
                                        ObjEvaluationTmp.CommentsM5M10 = strEvalutionpattern;
                                        isChange = true;
                                    }
                                    ObjEvaluationTmp.UpdateTime = DateTime.Now;
                                    this.Update(ObjEvaluationTmp);
                                }
                                else if (semeseterID == GlobalConstants.SEMESTER_OF_YEAR_SECOND && !strLockMark.Contains("HK2"))
                                {
                                    if (month.Value == GlobalConstants.MonthID_6 && !strLockMark.Contains("T6") && ObjEvaluationTmp.CommentsM1M6 != strEvalutionpattern)
                                    {
                                        oldObjectStrtmp.Append(CommentStr).Append(ObjEvaluationTmp.CommentsM1M6).Append("; ");
                                        ObjEvaluationTmp.CommentsM1M6 = strEvalutionpattern;
                                        isChange = true;
                                    }
                                    else if (month.Value == GlobalConstants.MonthID_7 && !strLockMark.Contains("T7") && ObjEvaluationTmp.CommentsM2M7 != strEvalutionpattern)
                                    {
                                        oldObjectStrtmp.Append(CommentStr).Append(ObjEvaluationTmp.CommentsM2M7).Append("; ");
                                        ObjEvaluationTmp.CommentsM2M7 = strEvalutionpattern;
                                        isChange = true;
                                    }
                                    else if (month.Value == GlobalConstants.MonthID_8 && !strLockMark.Contains("T8") && ObjEvaluationTmp.CommentsM3M8 != strEvalutionpattern)
                                    {
                                        oldObjectStrtmp.Append(CommentStr).Append(ObjEvaluationTmp.CommentsM3M8).Append("; ");
                                        ObjEvaluationTmp.CommentsM3M8 = strEvalutionpattern;
                                        isChange = true;
                                    }
                                    else if (month.Value == GlobalConstants.MonthID_9 && !strLockMark.Contains("T9") && ObjEvaluationTmp.CommentsM4M9 != strEvalutionpattern)
                                    {
                                        oldObjectStrtmp.Append(CommentStr).Append(ObjEvaluationTmp.CommentsM4M9).Append("; ");
                                        ObjEvaluationTmp.CommentsM4M9 = strEvalutionpattern;
                                        isChange = true;
                                    }
                                    else if (month.Value == GlobalConstants.MonthID_10 && !strLockMark.Contains("TH10") && ObjEvaluationTmp.CommentsM5M10 != strEvalutionpattern)
                                    {
                                        oldObjectStrtmp.Append(CommentStr).Append(ObjEvaluationTmp.CommentsM5M10).Append("; ");
                                        ObjEvaluationTmp.CommentsM5M10 = strEvalutionpattern;
                                        isChange = true;
                                    }
                                    ObjEvaluationTmp.UpdateTime = DateTime.Now;
                                    this.Update(ObjEvaluationTmp);
                                }
                                newObjectStrtmp.Append(CommentStr).Append(strEvalutionpattern).Append("; ");
                            }
                            else
                            {
                                //insert
                                ObjEvaluationTmp = new EvaluationComments();
                                ObjEvaluationTmp.EvaluationCommentsID = EvaluationCommentsBusiness.GetNextSeq<long>(); ;
                                ObjEvaluationTmp.AcademicYearID = academicYearId;
                                ObjEvaluationTmp.ClassID = classId;
                                ObjEvaluationTmp.CreateTime = DateTime.Now;
                                ObjEvaluationTmp.EducationLevelID = educationLevelId;
                                ObjEvaluationTmp.EvaluationCriteriaID = subjectID;
                                ObjEvaluationTmp.EvaluationID = evaluationID;
                                ObjEvaluationTmp.LastDigitSchoolID = partitionId;
                                ObjEvaluationTmp.PupilID = listPupilIDEva[j];
                                ObjEvaluationTmp.SchoolID = schoolId;
                                ObjEvaluationTmp.SemesterID = semeseterID;
                                ObjEvaluationTmp.TypeOfTeacher = typeOfTeacher;

                                if (semeseterID == GlobalConstants.SEMESTER_OF_YEAR_FIRST && !strLockMark.Contains("HK1"))
                                {
                                    if (month.Value == GlobalConstants.MonthID_1 && !strLockMark.Contains("T1"))
                                    {
                                        if (!string.IsNullOrEmpty(strEvalutionpattern))
                                        {
                                            ObjEvaluationTmp.CommentsM1M6 = strEvalutionpattern;
                                            isChange = true;
                                        }
                                    }
                                    else if (month.Value == GlobalConstants.MonthID_2 && !strLockMark.Contains("T2"))
                                    {
                                        if (!string.IsNullOrEmpty(strEvalutionpattern))
                                        {
                                            ObjEvaluationTmp.CommentsM2M7 = strEvalutionpattern;
                                            isChange = true;
                                        }
                                    }
                                    else if (month.Value == GlobalConstants.MonthID_3 && !strLockMark.Contains("T3"))
                                    {
                                        if (!string.IsNullOrEmpty(strEvalutionpattern))
                                        {
                                            ObjEvaluationTmp.CommentsM3M8 = strEvalutionpattern;
                                            isChange = true;
                                        }
                                    }
                                    else if (month.Value == GlobalConstants.MonthID_4 && !strLockMark.Contains("T4"))
                                    {
                                        if (!string.IsNullOrEmpty(strEvalutionpattern))
                                        {
                                            ObjEvaluationTmp.CommentsM4M9 = strEvalutionpattern;
                                            isChange = true;
                                        }
                                    }
                                    else if (month.Value == GlobalConstants.MonthID_5 && !strLockMark.Contains("T5"))
                                    {
                                        if (!string.IsNullOrEmpty(strEvalutionpattern))
                                        {
                                            ObjEvaluationTmp.CommentsM5M10 = strEvalutionpattern;
                                            isChange = true;
                                        }
                                    }
                                }
                                else if (semeseterID == GlobalConstants.SEMESTER_OF_YEAR_SECOND && !strLockMark.Contains("HK2"))
                                {
                                    if (month.Value == GlobalConstants.MonthID_6 && !strLockMark.Contains("T6"))
                                    {
                                        if (!string.IsNullOrEmpty(strEvalutionpattern))
                                        {
                                            ObjEvaluationTmp.CommentsM1M6 = strEvalutionpattern;
                                            isChange = true;
                                        }
                                    }
                                    else if (month.Value == GlobalConstants.MonthID_7 && !strLockMark.Contains("T7"))
                                    {
                                        if (!string.IsNullOrEmpty(strEvalutionpattern))
                                        {
                                            ObjEvaluationTmp.CommentsM2M7 = strEvalutionpattern;
                                            isChange = true;
                                        }
                                    }
                                    else if (month.Value == GlobalConstants.MonthID_8 && !strLockMark.Contains("T8"))
                                    {
                                        if (!string.IsNullOrEmpty(strEvalutionpattern))
                                        {
                                            ObjEvaluationTmp.CommentsM3M8 = strEvalutionpattern;
                                            isChange = true;
                                        }
                                    }
                                    else if (month.Value == GlobalConstants.MonthID_9 && !strLockMark.Contains("T9"))
                                    {
                                        if (!string.IsNullOrEmpty(strEvalutionpattern))
                                        {
                                            ObjEvaluationTmp.CommentsM4M9 = strEvalutionpattern;
                                            isChange = true;
                                        }
                                    }
                                    else if (month.Value == GlobalConstants.MonthID_10 && !strLockMark.Contains("TH10"))
                                    {
                                        if (!string.IsNullOrEmpty(strEvalutionpattern))
                                        {
                                            ObjEvaluationTmp.CommentsM5M10 = strEvalutionpattern;
                                            isChange = true;
                                        }
                                    }
                                }
                                if (isChange)
                                {
                                    oldObjectStrtmp.Append(CommentStr).Append("; ");
                                    newObjectStrtmp.Append(CommentStr).Append(strEvalutionpattern).Append("; ");
                                    ObjEvaluationTmp.CreateTime = DateTime.Now;
                                    ObjEvaluationTmp.UpdateTime = null;
                                    EvaluationCommentsBusiness.Insert(ObjEvaluationTmp);
                                }
                            }
                            if (isChange)
                            {
                                objAc = new ActionAuditDataBO();
                                objAc.PupilID = pupilID;
                                objAc.ClassID = classId;
                                objAc.EvaluationID = evaluationID;
                                objAc.SubjectID = subjectID;
                                objAc.ObjID = objectIDStrtmp;
                                objAc.Parameter = paramsStrtmp;
                                objAc.OldData = oldObjectStrtmp;
                                objAc.NewData = newObjectStrtmp;
                                objAc.UserAction = userActionsStrtmp;
                                objAc.UserFunction = userFuntionsStrtmp;
                                objAc.UserDescription = userDescriptionsStrtmp;
                                objAc.Description = descriptionStrtmp;
                                lstActionAuditData.Add(objAc);
                            }

                            objectIDStrtmp = new StringBuilder();
                            descriptionStrtmp = new StringBuilder();
                            paramsStrtmp = new StringBuilder();
                            oldObjectStrtmp = new StringBuilder();
                            newObjectStrtmp = new StringBuilder();
                            userFuntionsStrtmp = new StringBuilder();
                            userActionsStrtmp = new StringBuilder();
                            userDescriptionsStrtmp = new StringBuilder();
                        }
                    }
                    this.Save();

                    #endregion
                }
                else if (iCopyObject == 1 && iCopyLocation == 2)
                {
                    #region TH Ghi de nhan xet cua tat ca hoc sinh,Sao chep nhan xet tat ca cac thang cua hoc ky

                    //Danh sach nhan xet cua thang voi mon hoc duoc chon tren giao dien
                    listEvaSource = ListEvaluationComment.Where(p => p.EvaluationCriteriaID == subjectIdSelect).ToList();
                    List<int> listPupilIDEva = listEvaSource.Select(p => p.PupilID).Distinct().ToList();

                    for (int j = 0; j < listEvaSource.Count; j++)
                    {
                        evaluationCommentObj = listEvaSource[j];
                        evaluationTmp = new EvaluationOfPupilBO();
                        evaluationTmp.PupilID = evaluationCommentObj.PupilID;
                        evaluationTmp.EvaluationComment16 = evaluationCommentObj.CommentsM1M6;
                        evaluationTmp.EvaluationComment27 = evaluationCommentObj.CommentsM2M7;
                        evaluationTmp.EvaluationComment38 = evaluationCommentObj.CommentsM3M8;
                        evaluationTmp.EvaluationComment49 = evaluationCommentObj.CommentsM4M9;
                        evaluationTmp.EvaluationComment510 = evaluationCommentObj.CommentsM5M10;
                        listEvaluationOfPupil.Add(evaluationTmp);
                    }

                    //Kiem tra co trong DB thi update khong co thi insert
                    //For qua danh danh sach mon hoc can cap nhat nhan xet
                    string CommentsM1M6, CommentsM2M7, CommentsM3M8, CommentsM4M9, CommentsM5M10;
                    for (int i = 0; i < listSubjectID.Count; i++)
                    {
                        subjectID = listSubjectID[i];

                        //Danh sach nhan xet co trong DB
                        List<EvaluationComments> listEvaluationComment = ListEvaluationComment.Where(p => p.EvaluationCriteriaID == subjectID).ToList();

                        EvaluationComments ObjEvaluationTmp = null;
                        for (int j = 0; j < listPupilIDEva.Count; j++)
                        {
                            isChange = false;
                            CommentsM1M6 = listEvaluationOfPupil.Where(p => p.PupilID == listPupilIDEva[j]).Select(p => p.EvaluationComment16).FirstOrDefault();
                            CommentsM2M7 = listEvaluationOfPupil.Where(p => p.PupilID == listPupilIDEva[j]).Select(p => p.EvaluationComment27).FirstOrDefault();
                            CommentsM3M8 = listEvaluationOfPupil.Where(p => p.PupilID == listPupilIDEva[j]).Select(p => p.EvaluationComment38).FirstOrDefault();
                            CommentsM4M9 = listEvaluationOfPupil.Where(p => p.PupilID == listPupilIDEva[j]).Select(p => p.EvaluationComment49).FirstOrDefault();
                            CommentsM5M10 = listEvaluationOfPupil.Where(p => p.PupilID == listPupilIDEva[j]).Select(p => p.EvaluationComment510).FirstOrDefault();
                            pupilID = listPupilIDEva[j];
                            ObjEvaluationTmp = listEvaluationComment.Where(p => p.PupilID == listPupilIDEva[j]).FirstOrDefault();//nhan xet cua 1 hoc sinh
                            objPOC = lstPOC.Where(p => p.PupilID == pupilID).FirstOrDefault();
                            objSC = lstSC.Where(p => p.SubjectCatID == subjectID).FirstOrDefault();
                            objectIDStrtmp.Append(pupilID);
                            descriptionStrtmp.Append("Cập nhật sổ TD CLGD GVBM");
                            paramsStrtmp.Append(pupilID);
                            userFuntionsStrtmp.Append("Sổ TD CLGD (GVBM)");
                            userActionsStrtmp.Append(SMAS.Business.Common.GlobalConstants.ACTION_UPDATE);
                            userDescriptionsStrtmp.Append("Sao chép nhận xét đánh giá " + evaluationName + " HS " + objPOC.PupilFullName + ", " + "mã " + objPOC.PupilCode + ", ");
                            userDescriptionsStrtmp.Append("Lớp ").Append(objPOC.ClassName + "/" + objSC.DisplayName + "/" + "Tháng " + month + "/Học kỳ " + semeseterID + "/Năm học " + acaObj.Year + "-" + (acaObj.Year + 1) + ". ");
                            if (ObjEvaluationTmp != null)
                            {
                                //update
                                if (semeseterID == GlobalConstants.SEMESTER_OF_YEAR_FIRST && !strLockMark.Contains("HK1"))
                                {
                                    if (!strLockMark.Contains("T1") && ObjEvaluationTmp.CommentsM1M6 != CommentsM1M6)
                                    {
                                        oldObjectStrtmp.Append(CommentStr).Append(ObjEvaluationTmp.CommentsM1M6).Append("; ");
                                        newObjectStrtmp.Append(CommentStr).Append(CommentsM1M6).Append("; ");
                                        ObjEvaluationTmp.CommentsM1M6 = CommentsM1M6;
                                        isChange = true;
                                    }
                                    if (!strLockMark.Contains("T2") && ObjEvaluationTmp.CommentsM2M7 != CommentsM2M7)
                                    {
                                        oldObjectStrtmp.Append(CommentStr).Append(ObjEvaluationTmp.CommentsM2M7).Append("; ");
                                        newObjectStrtmp.Append(CommentStr).Append(CommentsM2M7).Append("; ");
                                        ObjEvaluationTmp.CommentsM2M7 = CommentsM2M7;
                                        isChange = true;
                                    }
                                    if (!strLockMark.Contains("T3") && ObjEvaluationTmp.CommentsM3M8 != CommentsM3M8)
                                    {
                                        oldObjectStrtmp.Append(CommentStr).Append(ObjEvaluationTmp.CommentsM3M8).Append("; ");
                                        newObjectStrtmp.Append(CommentStr).Append(CommentsM3M8).Append("; ");
                                        ObjEvaluationTmp.CommentsM3M8 = CommentsM3M8;
                                        isChange = true;
                                    }
                                    if (!strLockMark.Contains("T4") && ObjEvaluationTmp.CommentsM4M9 != CommentsM4M9)
                                    {
                                        oldObjectStrtmp.Append(CommentStr).Append(ObjEvaluationTmp.CommentsM4M9).Append("; ");
                                        newObjectStrtmp.Append(CommentStr).Append(CommentsM4M9).Append("; ");
                                        ObjEvaluationTmp.CommentsM4M9 = CommentsM4M9;
                                        isChange = true;
                                    }
                                    if (!strLockMark.Contains("T5") && ObjEvaluationTmp.CommentsM5M10 != CommentsM5M10)
                                    {
                                        oldObjectStrtmp.Append(CommentStr).Append(ObjEvaluationTmp.CommentsM5M10).Append("; ");
                                        newObjectStrtmp.Append(CommentStr).Append(CommentsM5M10).Append("; ");
                                        ObjEvaluationTmp.CommentsM5M10 = CommentsM5M10;
                                        isChange = true;
                                    }

                                    ObjEvaluationTmp.UpdateTime = DateTime.Now;
                                    this.Update(ObjEvaluationTmp);
                                }
                                else if (semeseterID == GlobalConstants.SEMESTER_OF_YEAR_SECOND && !strLockMark.Contains("HK2"))
                                {
                                    if (!strLockMark.Contains("T6") && ObjEvaluationTmp.CommentsM1M6 != CommentsM1M6)
                                    {
                                        oldObjectStrtmp.Append(CommentStr).Append(ObjEvaluationTmp.CommentsM1M6).Append("; ");
                                        newObjectStrtmp.Append(CommentStr).Append(CommentsM1M6).Append("; ");
                                        isChange = true;
                                        ObjEvaluationTmp.CommentsM1M6 = CommentsM1M6;
                                    }
                                    if (!strLockMark.Contains("T7") && ObjEvaluationTmp.CommentsM1M6 != CommentsM2M7)
                                    {
                                        oldObjectStrtmp.Append(CommentStr).Append(ObjEvaluationTmp.CommentsM2M7).Append("; ");
                                        newObjectStrtmp.Append(CommentStr).Append(CommentsM2M7).Append("; ");
                                        isChange = true;
                                        ObjEvaluationTmp.CommentsM2M7 = CommentsM2M7;
                                    }
                                    if (!strLockMark.Contains("T8") && ObjEvaluationTmp.CommentsM1M6 != CommentsM3M8)
                                    {
                                        oldObjectStrtmp.Append(CommentStr).Append(ObjEvaluationTmp.CommentsM3M8).Append("; ");
                                        newObjectStrtmp.Append(CommentStr).Append(CommentsM3M8).Append("; ");
                                        isChange = true;
                                        ObjEvaluationTmp.CommentsM3M8 = CommentsM3M8;
                                    }
                                    if (!strLockMark.Contains("T9") && ObjEvaluationTmp.CommentsM1M6 != CommentsM4M9)
                                    {
                                        oldObjectStrtmp.Append(CommentStr).Append(ObjEvaluationTmp.CommentsM4M9).Append("; ");
                                        newObjectStrtmp.Append(CommentStr).Append(CommentsM4M9).Append("; ");
                                        isChange = true;
                                        ObjEvaluationTmp.CommentsM4M9 = CommentsM4M9;
                                    }
                                    if (!strLockMark.Contains("TH10") && ObjEvaluationTmp.CommentsM1M6 != CommentsM5M10)
                                    {
                                        oldObjectStrtmp.Append(CommentStr).Append(ObjEvaluationTmp.CommentsM5M10).Append("; ");
                                        newObjectStrtmp.Append(CommentStr).Append(CommentsM5M10).Append("; ");
                                        isChange = true;
                                        ObjEvaluationTmp.CommentsM5M10 = CommentsM5M10;
                                    }

                                    ObjEvaluationTmp.UpdateTime = DateTime.Now;
                                    this.Update(ObjEvaluationTmp);
                                }

                            }
                            else
                            {
                                //insert
                                ObjEvaluationTmp = new EvaluationComments();
                                ObjEvaluationTmp.EvaluationCommentsID = EvaluationCommentsBusiness.GetNextSeq<long>(); ;
                                ObjEvaluationTmp.AcademicYearID = academicYearId;
                                ObjEvaluationTmp.ClassID = classId;
                                ObjEvaluationTmp.CreateTime = DateTime.Now;
                                ObjEvaluationTmp.EducationLevelID = educationLevelId;
                                ObjEvaluationTmp.EvaluationCriteriaID = subjectID;
                                ObjEvaluationTmp.EvaluationID = evaluationID;
                                ObjEvaluationTmp.LastDigitSchoolID = partitionId;
                                ObjEvaluationTmp.PupilID = listPupilIDEva[j];
                                ObjEvaluationTmp.SchoolID = schoolId;
                                ObjEvaluationTmp.SemesterID = semeseterID;
                                ObjEvaluationTmp.TypeOfTeacher = typeOfTeacher;

                                if (semeseterID == GlobalConstants.SEMESTER_OF_YEAR_FIRST && !strLockMark.Contains("HK1"))
                                {
                                    if (!strLockMark.Contains("T1"))
                                    {
                                        if (!string.IsNullOrEmpty(CommentsM1M6))
                                        {
                                            newObjectStrtmp.Append(CommentStr).Append(CommentsM1M6).Append("; ");
                                            ObjEvaluationTmp.CommentsM1M6 = CommentsM1M6;
                                            isChange = true;
                                        }
                                    }
                                    if (!strLockMark.Contains("T2"))
                                    {
                                        if (!string.IsNullOrEmpty(CommentsM2M7))
                                        {
                                            newObjectStrtmp.Append(CommentStr).Append(CommentsM2M7).Append("; ");
                                            ObjEvaluationTmp.CommentsM2M7 = CommentsM2M7;
                                            isChange = true;
                                        }
                                    }
                                    if (!strLockMark.Contains("T3"))
                                    {
                                        if (!string.IsNullOrEmpty(CommentsM3M8))
                                        {
                                            newObjectStrtmp.Append(CommentStr).Append(CommentsM3M8).Append("; ");
                                            ObjEvaluationTmp.CommentsM3M8 = CommentsM3M8;
                                            isChange = true;
                                        }
                                    }
                                    if (!strLockMark.Contains("T4"))
                                    {
                                        if (!string.IsNullOrEmpty(CommentsM4M9))
                                        {
                                            newObjectStrtmp.Append(CommentStr).Append(CommentsM4M9).Append("; ");
                                            ObjEvaluationTmp.CommentsM4M9 = CommentsM4M9;
                                            isChange = true;
                                        }
                                    }
                                    if (!strLockMark.Contains("T5"))
                                    {
                                        if (!string.IsNullOrEmpty(CommentsM5M10))
                                        {
                                            newObjectStrtmp.Append(CommentStr).Append(CommentsM5M10).Append("; ");
                                            ObjEvaluationTmp.CommentsM5M10 = CommentsM5M10;
                                            isChange = true;
                                        }
                                    }
                                }
                                else if (semeseterID == GlobalConstants.SEMESTER_OF_YEAR_SECOND && !strLockMark.Contains("HK2"))
                                {
                                    if (!strLockMark.Contains("T6"))
                                    {
                                        if (!string.IsNullOrEmpty(CommentsM1M6))
                                        {
                                            newObjectStrtmp.Append(CommentStr).Append(CommentsM1M6).Append("; ");
                                            ObjEvaluationTmp.CommentsM1M6 = CommentsM1M6;
                                            isChange = true;
                                        }
                                    }
                                    if (!strLockMark.Contains("T7"))
                                    {
                                        if (!string.IsNullOrEmpty(CommentsM2M7))
                                        {
                                            newObjectStrtmp.Append(CommentStr).Append(CommentsM2M7).Append("; ");
                                            ObjEvaluationTmp.CommentsM2M7 = CommentsM2M7;
                                            isChange = true;
                                        }
                                    }
                                    if (!strLockMark.Contains("T8"))
                                    {
                                        if (!string.IsNullOrEmpty(CommentsM3M8))
                                        {
                                            newObjectStrtmp.Append(CommentStr).Append(CommentsM3M8).Append("; ");
                                            ObjEvaluationTmp.CommentsM3M8 = CommentsM3M8;
                                            isChange = true;
                                        }
                                    }
                                    if (!strLockMark.Contains("T9"))
                                    {
                                        if (!string.IsNullOrEmpty(CommentsM4M9))
                                        {
                                            newObjectStrtmp.Append(CommentStr).Append(CommentsM4M9).Append("; ");
                                            ObjEvaluationTmp.CommentsM4M9 = CommentsM4M9;
                                            isChange = true;
                                        }
                                    }
                                    if (!strLockMark.Contains("TH10"))
                                    {
                                        if (!string.IsNullOrEmpty(CommentsM5M10))
                                        {
                                            newObjectStrtmp.Append(CommentStr).Append(CommentsM5M10).Append("; ");
                                            ObjEvaluationTmp.CommentsM5M10 = CommentsM5M10;
                                            isChange = true;
                                        }
                                    }
                                }
                                if (isChange)
                                {
                                    ObjEvaluationTmp.CreateTime = DateTime.Now;
                                    ObjEvaluationTmp.UpdateTime = null;
                                    oldObjectStrtmp.Append(CommentStr).Append("; ");
                                    EvaluationCommentsBusiness.Insert(ObjEvaluationTmp);
                                }
                            }
                            if (isChange)
                            {
                                objAc = new ActionAuditDataBO();
                                objAc.PupilID = pupilID;
                                objAc.ClassID = classId;
                                objAc.EvaluationID = evaluationID;
                                objAc.SubjectID = subjectID;
                                objAc.ObjID = objectIDStrtmp;
                                objAc.Parameter = paramsStrtmp;
                                objAc.OldData = oldObjectStrtmp;
                                objAc.NewData = newObjectStrtmp;
                                objAc.UserAction = userActionsStrtmp;
                                objAc.UserFunction = userFuntionsStrtmp;
                                objAc.UserDescription = userDescriptionsStrtmp;
                                objAc.Description = descriptionStrtmp;
                                lstActionAuditData.Add(objAc);
                            }

                            objectIDStrtmp = new StringBuilder();
                            descriptionStrtmp = new StringBuilder();
                            paramsStrtmp = new StringBuilder();
                            oldObjectStrtmp = new StringBuilder();
                            newObjectStrtmp = new StringBuilder();
                            userFuntionsStrtmp = new StringBuilder();
                            userActionsStrtmp = new StringBuilder();
                            userDescriptionsStrtmp = new StringBuilder();
                        }
                    }
                    this.Save();

                    #endregion
                    #region Sao chep tat ca cac thang cua hoc sinh thi sao chep luon KTHK doi voi GVBM tab Mon Hoc và HDGD
                    if (evaluationID == 1 && typeOfTeacher == 1)//GVBM
                    {
                        //Lay thong tin nhan xet cuoi ky cua tat ca mon hoc duoc chon
                        objDic.Add("lstPupilID", listPupilId);
                        List<SummedEvaluation> listSummedEvaluation = SummedEvaluationBusiness.getListSummedEvaluationByListSubject(objDic).ToList();
                        //Lay thong tin nhan xet cuoi ky lop duoc chon voi mon duoc chon tren giao dien
                        List<SummedEvaluation> listSummEvaluationSource = listSummedEvaluation.Where(p => p.EvaluationCriteriaID == subjectIdSelect).ToList();
                        SummedEvaluation sumObj = null;
                        SummedEvaluation sumObjInsert = null;
                        listEvaluationOfPupil = new List<EvaluationOfPupilBO>();
                        string strEvalutionpattern = string.Empty;
                        for (int j = 0; j < listSummEvaluationSource.Count; j++)
                        {
                            evaluationTmp = new EvaluationOfPupilBO();
                            evaluationTmp.PupilID = listSummEvaluationSource[j].PupilID;
                            evaluationTmp.EvaluationComment = listSummEvaluationSource[j].EndingComments;
                            listEvaluationOfPupil.Add(evaluationTmp);
                        }

                        for (int i = 0; i < listSubjectID.Count; i++)
                        {
                            subjectID = listSubjectID[i];
                            List<SummedEvaluation> listSummedEvabySubjectID = listSummedEvaluation.Where(p => p.EvaluationCriteriaID == subjectID).ToList();
                            for (int j = 0; j < listPupilIDEva.Count; j++)
                            {
                                strEvalutionpattern = listEvaluationOfPupil.Where(p => p.PupilID == listPupilIDEva[j]).Select(p => p.EvaluationComment).FirstOrDefault();
                                sumObj = listSummedEvabySubjectID.Where(p => p.PupilID == listPupilIDEva[j]).FirstOrDefault();//nhan xet cua 1 hoc sinh
                                objAc = lstActionAuditData.Where(p => p.PupilID == listPupilIDEva[i]).FirstOrDefault();
                                if (sumObj != null)
                                {
                                    if ((semeseterID == GlobalConstants.SEMESTER_OF_YEAR_FIRST && !strLockMark.Contains("HK1") && !strLockMark.Contains("CK1"))
                                        || (semeseterID == GlobalConstants.SEMESTER_OF_YEAR_SECOND && !strLockMark.Contains("HK2") && !strLockMark.Contains("CK2")))
                                    {
                                        if (sumObj.EndingComments != strEvalutionpattern)
                                        {
                                            if (objAc != null)
                                            {
                                                objAc.OldData.Append(LOG_NXCK + sumObj.EndingComments).Append("; ");
                                                objAc.NewData.Append(LOG_NXCK + strEvalutionpattern).Append("; ");
                                            }
                                            sumObj.EndingComments = strEvalutionpattern;
                                        }
                                        sumObj.UpdateTime = DateTime.Now;
                                        SummedEvaluationBusiness.Update(sumObj);
                                    }
                                }
                                else
                                {
                                    if ((semeseterID == GlobalConstants.SEMESTER_OF_YEAR_FIRST && !strLockMark.Contains("HK1") && !strLockMark.Contains("CK1"))
                                        || (semeseterID == GlobalConstants.SEMESTER_OF_YEAR_SECOND && !strLockMark.Contains("HK2") && !strLockMark.Contains("CK2")))
                                    {
                                        sumObjInsert = new SummedEvaluation();
                                        sumObjInsert.SummedEvaluationID = SummedEvaluationBusiness.GetNextSeq<long>();
                                        sumObjInsert.AcademicYearID = academicYearId;
                                        sumObjInsert.ClassID = classId;
                                        sumObjInsert.CreateTime = DateTime.Now;
                                        sumObjInsert.EducationLevelID = educationLevelId;
                                        sumObjInsert.EndingComments = strEvalutionpattern;
                                        sumObjInsert.EndingEvaluation = string.Empty;
                                        sumObjInsert.EvaluationCriteriaID = subjectID;
                                        sumObjInsert.EvaluationID = evaluationID;
                                        sumObjInsert.EvaluationReTraining = null;
                                        sumObjInsert.LastDigitSchoolID = partitionId;
                                        sumObjInsert.PeriodicEndingMark = null;
                                        sumObjInsert.PupilID = listPupilIDEva[j];
                                        sumObjInsert.RetestMark = null;
                                        sumObjInsert.SchoolID = schoolId;
                                        sumObjInsert.SemesterID = semeseterID;
                                        if (objAc != null)
                                        {
                                            objAc.NewData.Append(LOG_NXCK + strEvalutionpattern).Append("; ");
                                        }
                                        sumObjInsert.CreateTime = DateTime.Now;
                                        sumObjInsert.UpdateTime = null;
                                        SummedEvaluationBusiness.Insert(sumObjInsert);
                                    }
                                }
                            }
                        }
                        SummedEvaluationBusiness.Save();
                    }
                    #endregion
                }
                else if (iCopyObject == 2 && iCopyLocation == 1 && month != 11)
                {
                    #region TH Chi cap nhat hoc sinh chua duoc nhan xet, sao chep theo thang
                    //Danh sach nhan xet cua thang voi mon hoc duoc chon
                    listEvaSource = ListEvaluationComment.Where(p => p.EvaluationCriteriaID == subjectIdSelect).ToList();
                    List<int> listPupilIDEva = listEvaSource.Select(p => p.PupilID).Distinct().ToList();

                    for (int j = 0; j < listEvaSource.Count; j++)
                    {
                        evaluationCommentObj = listEvaSource[j];
                        evaluationTmp = new EvaluationOfPupilBO();
                        evaluationTmp.PupilID = evaluationCommentObj.PupilID;

                        if (month.Value == GlobalConstants.MonthID_1 || month.Value == GlobalConstants.MonthID_6)
                        {
                            evaluationTmp.EvaluationComment = evaluationCommentObj.CommentsM1M6;
                        }
                        else if (month.Value == GlobalConstants.MonthID_2 || month.Value == GlobalConstants.MonthID_7)
                        {
                            evaluationTmp.EvaluationComment = evaluationCommentObj.CommentsM2M7;
                        }
                        else if (month.Value == GlobalConstants.MonthID_3 || month.Value == GlobalConstants.MonthID_8)
                        {
                            evaluationTmp.EvaluationComment = evaluationCommentObj.CommentsM3M8;
                        }
                        else if (month.Value == GlobalConstants.MonthID_4 || month.Value == GlobalConstants.MonthID_9)
                        {
                            evaluationTmp.EvaluationComment = evaluationCommentObj.CommentsM4M9;
                        }
                        else if (month.Value == GlobalConstants.MonthID_5 || month.Value == GlobalConstants.MonthID_10)
                        {
                            evaluationTmp.EvaluationComment = evaluationCommentObj.CommentsM5M10;
                        }
                        listEvaluationOfPupil.Add(evaluationTmp);
                    }

                    //Kiem tra co trong DB thi update khong co thi insert
                    //For qua danh danh sach mon hoc can cap nhat nhan xet
                    string strEvalutionpattern = string.Empty;

                    for (int i = 0; i < listSubjectID.Count; i++)
                    {
                        subjectID = listSubjectID[i];

                        //Danh sach nhan xet co trong DB
                        List<EvaluationComments> listEvaluationComment = ListEvaluationComment.Where(p => p.EvaluationCriteriaID == subjectID).ToList();
                        EvaluationComments ObjEvaluationTmp = null;
                        for (int j = 0; j < listPupilIDEva.Count; j++)
                        {
                            isChange = false;
                            strEvalutionpattern = listEvaluationOfPupil.Where(p => p.PupilID == listPupilIDEva[j]).Select(p => p.EvaluationComment).FirstOrDefault();
                            ObjEvaluationTmp = listEvaluationComment.Where(p => p.PupilID == listPupilIDEva[j]).FirstOrDefault();//nhan xet cua 1 hoc sinh
                            pupilID = listPupilIDEva[j];
                            objPOC = lstPOC.Where(p => p.PupilID == pupilID).FirstOrDefault();
                            objSC = lstSC.Where(p => p.SubjectCatID == subjectID).FirstOrDefault();
                            objectIDStrtmp.Append(pupilID);
                            descriptionStrtmp.Append("Cập nhật sổ TD CLGD GVBM");
                            paramsStrtmp.Append(pupilID);
                            userFuntionsStrtmp.Append("Sổ TD CLGD (GVBM)");
                            userActionsStrtmp.Append(SMAS.Business.Common.GlobalConstants.ACTION_UPDATE);
                            userDescriptionsStrtmp.Append("Sao chép nhận xét đánh giá " + evaluationName + " HS " + objPOC.PupilFullName + ", " + "mã " + objPOC.PupilCode + ", ");
                            userDescriptionsStrtmp.Append("Lớp ").Append(objPOC.ClassName + "/" + objSC.DisplayName + "/" + "Tháng " + month + "/Học kỳ " + semeseterID + "/Năm học " + acaObj.Year + "-" + (acaObj.Year + 1) + ". ");
                            if (ObjEvaluationTmp != null)
                            {
                                //update
                                if (semeseterID == GlobalConstants.SEMESTER_OF_YEAR_FIRST && !strLockMark.Contains("HK1"))
                                {
                                    if (month.Value == GlobalConstants.MonthID_1 && !strLockMark.Contains("T1"))
                                    {
                                        if (!string.IsNullOrEmpty(ObjEvaluationTmp.CommentsM1M6)) continue;
                                        oldObjectStrtmp.Append(CommentStr).Append("; ");
                                        isChange = true;
                                        ObjEvaluationTmp.CommentsM1M6 = strEvalutionpattern;
                                    }
                                    else if (month.Value == GlobalConstants.MonthID_2 && !strLockMark.Contains("T2"))
                                    {
                                        if (!string.IsNullOrEmpty(ObjEvaluationTmp.CommentsM2M7)) continue;
                                        oldObjectStrtmp.Append(CommentStr).Append("; ");
                                        isChange = true;
                                        ObjEvaluationTmp.CommentsM2M7 = strEvalutionpattern;
                                    }
                                    else if (month.Value == GlobalConstants.MonthID_3 && !strLockMark.Contains("T3"))
                                    {
                                        if (!string.IsNullOrEmpty(ObjEvaluationTmp.CommentsM3M8)) continue;
                                        oldObjectStrtmp.Append(CommentStr).Append("; ");
                                        isChange = true;
                                        ObjEvaluationTmp.CommentsM3M8 = strEvalutionpattern;
                                    }
                                    else if (month.Value == GlobalConstants.MonthID_4 && !strLockMark.Contains("T4"))
                                    {
                                        if (!string.IsNullOrEmpty(ObjEvaluationTmp.CommentsM4M9)) continue;
                                        oldObjectStrtmp.Append(CommentStr).Append("; ");
                                        isChange = true;
                                        ObjEvaluationTmp.CommentsM4M9 = strEvalutionpattern;
                                    }
                                    else if (month.Value == GlobalConstants.MonthID_5 && !strLockMark.Contains("T5"))
                                    {
                                        if (!string.IsNullOrEmpty(ObjEvaluationTmp.CommentsM5M10)) continue;
                                        oldObjectStrtmp.Append(CommentStr).Append("; ");
                                        isChange = true;
                                        ObjEvaluationTmp.CommentsM5M10 = strEvalutionpattern;
                                    }
                                    ObjEvaluationTmp.UpdateTime = DateTime.Now;
                                    this.Update(ObjEvaluationTmp);
                                }
                                else if (semeseterID == GlobalConstants.SEMESTER_OF_YEAR_SECOND && !strLockMark.Contains("HK2"))
                                {
                                    if (month.Value == GlobalConstants.MonthID_6 && !strLockMark.Contains("T6"))
                                    {
                                        if (!string.IsNullOrEmpty(ObjEvaluationTmp.CommentsM1M6)) continue;
                                        oldObjectStrtmp.Append(CommentStr).Append("; ");
                                        isChange = true;
                                        ObjEvaluationTmp.CommentsM1M6 = strEvalutionpattern;
                                    }
                                    else if (month.Value == GlobalConstants.MonthID_7 && !strLockMark.Contains("T7"))
                                    {
                                        if (!string.IsNullOrEmpty(ObjEvaluationTmp.CommentsM2M7)) continue;
                                        oldObjectStrtmp.Append(CommentStr).Append("; ");
                                        isChange = true;
                                        ObjEvaluationTmp.CommentsM2M7 = strEvalutionpattern;
                                    }
                                    else if (month.Value == GlobalConstants.MonthID_8 && !strLockMark.Contains("T8"))
                                    {
                                        if (!string.IsNullOrEmpty(ObjEvaluationTmp.CommentsM3M8)) continue;
                                        oldObjectStrtmp.Append(CommentStr).Append("; ");
                                        isChange = true;
                                        ObjEvaluationTmp.CommentsM3M8 = strEvalutionpattern;
                                    }
                                    else if (month.Value == GlobalConstants.MonthID_9 && !strLockMark.Contains("T9"))
                                    {
                                        if (!string.IsNullOrEmpty(ObjEvaluationTmp.CommentsM4M9)) continue;
                                        oldObjectStrtmp.Append(CommentStr).Append("; ");
                                        isChange = true;
                                        ObjEvaluationTmp.CommentsM4M9 = strEvalutionpattern;
                                    }
                                    else if (month.Value == GlobalConstants.MonthID_10 && !strLockMark.Contains("TH10"))
                                    {
                                        if (!string.IsNullOrEmpty(ObjEvaluationTmp.CommentsM5M10)) continue;
                                        oldObjectStrtmp.Append(CommentStr).Append("; ");
                                        isChange = true;
                                        ObjEvaluationTmp.CommentsM5M10 = strEvalutionpattern;
                                    }
                                    ObjEvaluationTmp.UpdateTime = DateTime.Now;
                                    this.Update(ObjEvaluationTmp);
                                }
                                newObjectStrtmp.Append(CommentStr).Append(strEvalutionpattern).Append("; ");
                            }
                            else
                            {
                                //insert
                                ObjEvaluationTmp = new EvaluationComments();
                                ObjEvaluationTmp.EvaluationCommentsID = EvaluationCommentsBusiness.GetNextSeq<long>(); ;
                                ObjEvaluationTmp.AcademicYearID = academicYearId;
                                ObjEvaluationTmp.ClassID = classId;
                                ObjEvaluationTmp.CreateTime = DateTime.Now;
                                ObjEvaluationTmp.EducationLevelID = educationLevelId;
                                ObjEvaluationTmp.EvaluationCriteriaID = subjectID;
                                ObjEvaluationTmp.EvaluationID = evaluationID;
                                ObjEvaluationTmp.LastDigitSchoolID = partitionId;
                                ObjEvaluationTmp.PupilID = listPupilIDEva[j];
                                ObjEvaluationTmp.SchoolID = schoolId;
                                ObjEvaluationTmp.SemesterID = semeseterID;
                                ObjEvaluationTmp.TypeOfTeacher = typeOfTeacher;
                                if (semeseterID == GlobalConstants.SEMESTER_OF_YEAR_FIRST && !strLockMark.Contains("HK1"))
                                {
                                    if (month.Value == GlobalConstants.MonthID_1 && !strLockMark.Contains("T1"))
                                    {
                                        if (!string.IsNullOrEmpty(strEvalutionpattern))
                                        {
                                            ObjEvaluationTmp.CommentsM1M6 = strEvalutionpattern;
                                            isChange = true;
                                        }
                                    }
                                    else if (month.Value == GlobalConstants.MonthID_2 && !strLockMark.Contains("T2"))
                                    {
                                        if (!string.IsNullOrEmpty(strEvalutionpattern))
                                        {
                                            ObjEvaluationTmp.CommentsM2M7 = strEvalutionpattern;
                                            isChange = true;
                                        }
                                    }
                                    else if (month.Value == GlobalConstants.MonthID_3 && !strLockMark.Contains("T3"))
                                    {
                                        if (!string.IsNullOrEmpty(strEvalutionpattern))
                                        {
                                            ObjEvaluationTmp.CommentsM3M8 = strEvalutionpattern;
                                            isChange = true;
                                        }
                                    }
                                    else if (month.Value == GlobalConstants.MonthID_4 && !strLockMark.Contains("T4"))
                                    {
                                        if (!string.IsNullOrEmpty(strEvalutionpattern))
                                        {
                                            ObjEvaluationTmp.CommentsM4M9 = strEvalutionpattern;
                                            isChange = true;
                                        }
                                    }
                                    else if (month.Value == GlobalConstants.MonthID_5 && !strLockMark.Contains("T51"))
                                    {
                                        if (!string.IsNullOrEmpty(strEvalutionpattern))
                                        {
                                            ObjEvaluationTmp.CommentsM5M10 = strEvalutionpattern;
                                            isChange = true;
                                        }
                                    }
                                }
                                else if (semeseterID == GlobalConstants.SEMESTER_OF_YEAR_SECOND && !strLockMark.Contains("HK2"))
                                {
                                    if (month.Value == GlobalConstants.MonthID_6 && !strLockMark.Contains("T6"))
                                    {
                                        if (!string.IsNullOrEmpty(strEvalutionpattern))
                                        {
                                            ObjEvaluationTmp.CommentsM1M6 = strEvalutionpattern;
                                            isChange = true;
                                        }
                                    }
                                    else if (month.Value == GlobalConstants.MonthID_7 && !strLockMark.Contains("T7"))
                                    {
                                        if (!string.IsNullOrEmpty(strEvalutionpattern))
                                        {
                                            ObjEvaluationTmp.CommentsM2M7 = strEvalutionpattern;
                                            isChange = true;
                                        }
                                    }
                                    else if (month.Value == GlobalConstants.MonthID_8 && !strLockMark.Contains("T8"))
                                    {
                                        if (!string.IsNullOrEmpty(strEvalutionpattern))
                                        {
                                            ObjEvaluationTmp.CommentsM3M8 = strEvalutionpattern;
                                            isChange = true;
                                        }
                                    }
                                    else if (month.Value == GlobalConstants.MonthID_9 && !strLockMark.Contains("T9"))
                                    {
                                        if (!string.IsNullOrEmpty(strEvalutionpattern))
                                        {
                                            ObjEvaluationTmp.CommentsM4M9 = strEvalutionpattern;
                                            isChange = true;
                                        }
                                    }
                                    else if (month.Value == GlobalConstants.MonthID_10 && !strLockMark.Contains("TH10"))
                                    {
                                        if (!string.IsNullOrEmpty(strEvalutionpattern))
                                        {
                                            ObjEvaluationTmp.CommentsM5M10 = strEvalutionpattern;
                                            isChange = true;
                                        }
                                    }

                                }
                                if (isChange)
                                {
                                    oldObjectStrtmp.Append(CommentStr).Append("; ");
                                    newObjectStrtmp.Append(CommentStr).Append(strEvalutionpattern).Append("; ");
                                    ObjEvaluationTmp.CreateTime = DateTime.Now;
                                    EvaluationCommentsBusiness.Insert(ObjEvaluationTmp);
                                }
                            }
                            if (isChange)
                            {
                                objAc = new ActionAuditDataBO();
                                objAc.PupilID = pupilID;
                                objAc.ClassID = classId;
                                objAc.EvaluationID = evaluationID;
                                objAc.SubjectID = subjectID;
                                objAc.ObjID = objectIDStrtmp;
                                objAc.Parameter = paramsStrtmp;
                                objAc.OldData = oldObjectStrtmp;
                                objAc.NewData = newObjectStrtmp;
                                objAc.UserAction = userActionsStrtmp;
                                objAc.UserFunction = userFuntionsStrtmp;
                                objAc.UserDescription = userDescriptionsStrtmp;
                                objAc.Description = descriptionStrtmp;
                                lstActionAuditData.Add(objAc);
                            }

                            objectIDStrtmp = new StringBuilder();
                            descriptionStrtmp = new StringBuilder();
                            paramsStrtmp = new StringBuilder();
                            oldObjectStrtmp = new StringBuilder();
                            newObjectStrtmp = new StringBuilder();
                            userFuntionsStrtmp = new StringBuilder();
                            userActionsStrtmp = new StringBuilder();
                            userDescriptionsStrtmp = new StringBuilder();
                        }
                    }
                    this.Save();

                    #endregion
                }
                else if (iCopyObject == 2 && iCopyLocation == 2 && month != 11)
                {
                    #region TH Chi cap nhat hoc sinh cua duoc nhan xet, sao chep tat ca cac thang cua hoc ky

                    //Danh sach nhan xet cua thang voi mon hoc duoc chon tren giao dien
                    listEvaSource = ListEvaluationComment.Where(p => p.EvaluationCriteriaID == subjectIdSelect).ToList();
                    List<int> listPupilIDEva = listEvaSource.Select(p => p.PupilID).Distinct().ToList();

                    for (int j = 0; j < listEvaSource.Count; j++)
                    {
                        evaluationCommentObj = listEvaSource[j];
                        evaluationTmp = new EvaluationOfPupilBO();
                        evaluationTmp.PupilID = evaluationCommentObj.PupilID;
                        evaluationTmp.EvaluationComment16 = evaluationCommentObj.CommentsM1M6;
                        evaluationTmp.EvaluationComment27 = evaluationCommentObj.CommentsM2M7;
                        evaluationTmp.EvaluationComment38 = evaluationCommentObj.CommentsM3M8;
                        evaluationTmp.EvaluationComment49 = evaluationCommentObj.CommentsM4M9;
                        evaluationTmp.EvaluationComment510 = evaluationCommentObj.CommentsM5M10;
                        listEvaluationOfPupil.Add(evaluationTmp);
                    }

                    //Kiem tra co trong DB thi update khong co thi insert
                    //For qua danh danh sach mon hoc can cap nhat nhan xet
                    string CommentsM1M6, CommentsM2M7, CommentsM3M8, CommentsM4M9, CommentsM5M10;
                    for (int i = 0; i < listSubjectID.Count; i++)
                    {
                        evaluationCommentObj = null;
                        subjectID = listSubjectID[i];

                        //Danh sach nhan xet co trong DB
                        List<EvaluationComments> listEvaluationComment = ListEvaluationComment.Where(p => p.EvaluationCriteriaID == subjectID).ToList();
                        EvaluationComments ObjEvaluationTmp = null;
                        for (int j = 0; j < listPupilIDEva.Count; j++)
                        {
                            isChange = false;
                            CommentsM1M6 = listEvaluationOfPupil.Where(p => p.PupilID == listPupilIDEva[j]).Select(p => p.EvaluationComment16).FirstOrDefault();
                            CommentsM2M7 = listEvaluationOfPupil.Where(p => p.PupilID == listPupilIDEva[j]).Select(p => p.EvaluationComment27).FirstOrDefault();
                            CommentsM3M8 = listEvaluationOfPupil.Where(p => p.PupilID == listPupilIDEva[j]).Select(p => p.EvaluationComment38).FirstOrDefault();
                            CommentsM4M9 = listEvaluationOfPupil.Where(p => p.PupilID == listPupilIDEva[j]).Select(p => p.EvaluationComment49).FirstOrDefault();
                            CommentsM5M10 = listEvaluationOfPupil.Where(p => p.PupilID == listPupilIDEva[j]).Select(p => p.EvaluationComment510).FirstOrDefault();
                            pupilID = listPupilIDEva[j];
                            ObjEvaluationTmp = listEvaluationComment.Where(p => p.PupilID == listPupilIDEva[j]).FirstOrDefault();//nhan xet cua 1 hoc sinh
                            objPOC = lstPOC.Where(p => p.PupilID == pupilID).FirstOrDefault();
                            objSC = lstSC.Where(p => p.SubjectCatID == subjectID).FirstOrDefault();
                            objectIDStrtmp.Append(pupilID);
                            descriptionStrtmp.Append("Cập nhật sổ TD CLGD GVBM");
                            paramsStrtmp.Append(pupilID);
                            userFuntionsStrtmp.Append("Sổ TD CLGD (GVBM)");
                            userActionsStrtmp.Append(SMAS.Business.Common.GlobalConstants.ACTION_UPDATE);
                            userDescriptionsStrtmp.Append("Sao chép nhận xét đánh giá " + evaluationName + " HS " + objPOC.PupilFullName + ", " + "mã " + objPOC.PupilCode + ", ");
                            userDescriptionsStrtmp.Append("Lớp ").Append(objPOC.ClassName + "/" + objSC.DisplayName + "/" + "Tháng " + month + "/Học kỳ " + semeseterID + "/Năm học " + acaObj.Year + "-" + (acaObj.Year + 1) + ". ");
                            if (ObjEvaluationTmp != null)
                            {
                                //update
                                if (semeseterID == GlobalConstants.SEMESTER_OF_YEAR_FIRST && !strLockMark.Contains("HK1"))
                                {
                                    if (!string.IsNullOrEmpty(ObjEvaluationTmp.CommentsM1M6) && !strLockMark.Contains("T1")) continue;
                                    {
                                        oldObjectStrtmp.Append(CommentStr).Append(ObjEvaluationTmp.CommentsM1M6).Append("; ");
                                        newObjectStrtmp.Append(CommentStr).Append(CommentsM1M6).Append("; ");
                                        isChange = true;
                                        ObjEvaluationTmp.CommentsM1M6 = CommentsM1M6;
                                    }
                                    if (!string.IsNullOrEmpty(ObjEvaluationTmp.CommentsM2M7) && !strLockMark.Contains("T2")) continue;
                                    {
                                        oldObjectStrtmp.Append(CommentStr).Append(ObjEvaluationTmp.CommentsM1M6).Append("; ");
                                        newObjectStrtmp.Append(CommentStr).Append(CommentsM1M6).Append("; ");
                                        isChange = true;
                                        ObjEvaluationTmp.CommentsM2M7 = CommentsM2M7;
                                    }
                                    if (!string.IsNullOrEmpty(ObjEvaluationTmp.CommentsM3M8) && !strLockMark.Contains("T3")) continue;
                                    {
                                        oldObjectStrtmp.Append(CommentStr).Append(ObjEvaluationTmp.CommentsM1M6).Append("; ");
                                        newObjectStrtmp.Append(CommentStr).Append(CommentsM1M6).Append("; ");
                                        isChange = true;
                                        ObjEvaluationTmp.CommentsM3M8 = CommentsM3M8;
                                    }
                                    if (!string.IsNullOrEmpty(ObjEvaluationTmp.CommentsM4M9) && !strLockMark.Contains("T4")) continue;
                                    {
                                        oldObjectStrtmp.Append(CommentStr).Append(ObjEvaluationTmp.CommentsM1M6).Append("; ");
                                        newObjectStrtmp.Append(CommentStr).Append(CommentsM1M6).Append("; ");
                                        isChange = true;
                                        ObjEvaluationTmp.CommentsM4M9 = CommentsM4M9;
                                    }
                                    if (!string.IsNullOrEmpty(ObjEvaluationTmp.CommentsM5M10) && !strLockMark.Contains("T5")) continue;
                                    {
                                        oldObjectStrtmp.Append(CommentStr).Append(ObjEvaluationTmp.CommentsM1M6).Append("; ");
                                        newObjectStrtmp.Append(CommentStr).Append(CommentsM1M6).Append("; ");
                                        isChange = true;
                                        ObjEvaluationTmp.CommentsM5M10 = CommentsM5M10;
                                    }
                                    ObjEvaluationTmp.UpdateTime = DateTime.Now;
                                    this.Update(ObjEvaluationTmp);
                                }
                                else if (semeseterID == GlobalConstants.SEMESTER_OF_YEAR_SECOND && !strLockMark.Contains("HK2"))
                                {
                                    if (!string.IsNullOrEmpty(ObjEvaluationTmp.CommentsM1M6) && !strLockMark.Contains("T6")) continue;
                                    {
                                        oldObjectStrtmp.Append(CommentStr).Append(ObjEvaluationTmp.CommentsM1M6).Append("; ");
                                        newObjectStrtmp.Append(CommentStr).Append(CommentsM1M6).Append("; ");
                                        isChange = true;
                                        ObjEvaluationTmp.CommentsM1M6 = CommentsM1M6;
                                    }
                                    if (!string.IsNullOrEmpty(ObjEvaluationTmp.CommentsM2M7) && !strLockMark.Contains("T7")) continue;
                                    {
                                        oldObjectStrtmp.Append(CommentStr).Append(ObjEvaluationTmp.CommentsM2M7).Append("; ");
                                        newObjectStrtmp.Append(CommentStr).Append(CommentsM2M7).Append("; ");
                                        isChange = true;
                                        ObjEvaluationTmp.CommentsM2M7 = CommentsM2M7;
                                    }
                                    if (!string.IsNullOrEmpty(ObjEvaluationTmp.CommentsM3M8) && !strLockMark.Contains("T8")) continue;
                                    {
                                        oldObjectStrtmp.Append(CommentStr).Append(ObjEvaluationTmp.CommentsM3M8).Append("; ");
                                        newObjectStrtmp.Append(CommentStr).Append(CommentsM3M8).Append("; ");
                                        isChange = true;
                                        ObjEvaluationTmp.CommentsM3M8 = CommentsM3M8;
                                    }
                                    if (!string.IsNullOrEmpty(ObjEvaluationTmp.CommentsM4M9) && !strLockMark.Contains("T9")) continue;
                                    {
                                        oldObjectStrtmp.Append(CommentStr).Append(ObjEvaluationTmp.CommentsM4M9).Append("; ");
                                        newObjectStrtmp.Append(CommentStr).Append(CommentsM4M9).Append("; ");
                                        isChange = true;
                                        ObjEvaluationTmp.CommentsM4M9 = CommentsM4M9;
                                    }
                                    if (!string.IsNullOrEmpty(ObjEvaluationTmp.CommentsM5M10) && !strLockMark.Contains("TH10")) continue;
                                    {
                                        oldObjectStrtmp.Append(CommentStr).Append(ObjEvaluationTmp.CommentsM5M10).Append("; ");
                                        newObjectStrtmp.Append(CommentStr).Append(CommentsM5M10).Append("; ");
                                        isChange = true;
                                        ObjEvaluationTmp.CommentsM5M10 = CommentsM5M10;
                                    }
                                    ObjEvaluationTmp.UpdateTime = DateTime.Now;
                                    this.Update(ObjEvaluationTmp);
                                }
                            }
                            else
                            {
                                //insert
                                ObjEvaluationTmp = new EvaluationComments();
                                ObjEvaluationTmp.EvaluationCommentsID = EvaluationCommentsBusiness.GetNextSeq<long>(); ;
                                ObjEvaluationTmp.AcademicYearID = academicYearId;
                                ObjEvaluationTmp.ClassID = classId;
                                ObjEvaluationTmp.CreateTime = DateTime.Now;
                                ObjEvaluationTmp.EducationLevelID = educationLevelId;
                                ObjEvaluationTmp.EvaluationCriteriaID = subjectID;
                                ObjEvaluationTmp.EvaluationID = evaluationID;
                                ObjEvaluationTmp.LastDigitSchoolID = partitionId;
                                ObjEvaluationTmp.PupilID = listPupilIDEva[j];
                                ObjEvaluationTmp.SchoolID = schoolId;
                                ObjEvaluationTmp.SemesterID = semeseterID;
                                ObjEvaluationTmp.TypeOfTeacher = typeOfTeacher;
                                if (semeseterID == GlobalConstants.SEMESTER_OF_YEAR_FIRST && !strLockMark.Contains("HK1"))
                                {
                                    if (!strLockMark.Contains("T1"))
                                    {
                                        if (!string.IsNullOrEmpty(CommentsM1M6))
                                        {
                                            newObjectStrtmp.Append(CommentStr).Append(CommentsM1M6).Append("; ");
                                            ObjEvaluationTmp.CommentsM1M6 = CommentsM1M6;
                                            isChange = true;
                                        }
                                    }
                                    if (!strLockMark.Contains("T2"))
                                    {
                                        if (!string.IsNullOrEmpty(CommentsM2M7))
                                        {
                                            newObjectStrtmp.Append(CommentStr).Append(CommentsM2M7).Append("; ");
                                            ObjEvaluationTmp.CommentsM2M7 = CommentsM2M7;
                                            isChange = true;
                                        }
                                    }
                                    if (!strLockMark.Contains("T3"))
                                    {
                                        if (!string.IsNullOrEmpty(CommentsM3M8))
                                        {
                                            newObjectStrtmp.Append(CommentStr).Append(CommentsM3M8).Append("; ");
                                            ObjEvaluationTmp.CommentsM3M8 = CommentsM3M8;
                                            isChange = true;
                                        }
                                    }
                                    if (!strLockMark.Contains("T4"))
                                    {
                                        if (!string.IsNullOrEmpty(CommentsM4M9))
                                        {
                                            newObjectStrtmp.Append(CommentStr).Append(CommentsM4M9).Append("; ");
                                            ObjEvaluationTmp.CommentsM4M9 = CommentsM4M9;
                                            isChange = true;
                                        }
                                    }
                                    if (!strLockMark.Contains("T5"))
                                    {
                                        if (!string.IsNullOrEmpty(CommentsM5M10))
                                        {
                                            newObjectStrtmp.Append(CommentStr).Append(CommentsM5M10).Append("; ");
                                            ObjEvaluationTmp.CommentsM5M10 = CommentsM5M10;
                                            isChange = true;
                                        }
                                    }
                                }
                                else if (semeseterID == GlobalConstants.SEMESTER_OF_YEAR_SECOND && !strLockMark.Contains("HK2"))
                                {
                                    if (!strLockMark.Contains("T6"))
                                    {
                                        if (!string.IsNullOrEmpty(CommentsM1M6))
                                        {
                                            newObjectStrtmp.Append(CommentStr).Append(CommentsM1M6).Append("; ");
                                            ObjEvaluationTmp.CommentsM1M6 = CommentsM1M6;
                                            isChange = true;
                                        }
                                    }
                                    if (!strLockMark.Contains("T7"))
                                    {
                                        if (!string.IsNullOrEmpty(CommentsM2M7))
                                        {
                                            newObjectStrtmp.Append(CommentStr).Append(CommentsM2M7).Append("; ");
                                            ObjEvaluationTmp.CommentsM2M7 = CommentsM2M7;
                                            isChange = true;
                                        }
                                    }
                                    if (!strLockMark.Contains("T8"))
                                    {
                                        if (!string.IsNullOrEmpty(CommentsM3M8))
                                        {
                                            newObjectStrtmp.Append(CommentStr).Append(CommentsM3M8).Append("; ");
                                            ObjEvaluationTmp.CommentsM3M8 = CommentsM3M8;
                                            isChange = true;
                                        }
                                    }
                                    if (!strLockMark.Contains("T9"))
                                    {
                                        if (!string.IsNullOrEmpty(CommentsM4M9))
                                        {
                                            newObjectStrtmp.Append(CommentStr).Append(CommentsM4M9).Append("; ");
                                            ObjEvaluationTmp.CommentsM4M9 = CommentsM4M9;
                                            isChange = true;
                                        }
                                    }
                                    if (!strLockMark.Contains("TH10"))
                                    {
                                        if (!string.IsNullOrEmpty(CommentsM5M10))
                                        {
                                            newObjectStrtmp.Append(CommentStr).Append(CommentsM5M10).Append("; ");
                                            ObjEvaluationTmp.CommentsM5M10 = CommentsM5M10;
                                            isChange = true;
                                        }
                                    }
                                }
                                if (isChange)
                                {
                                    ObjEvaluationTmp.CreateTime = DateTime.Now;
                                    oldObjectStrtmp.Append(CommentStr).Append("; ");
                                    EvaluationCommentsBusiness.Insert(ObjEvaluationTmp);
                                }
                            }
                            if (isChange)
                            {
                                objAc = new ActionAuditDataBO();
                                objAc.PupilID = pupilID;
                                objAc.ClassID = classId;
                                objAc.EvaluationID = evaluationID;
                                objAc.SubjectID = subjectID;
                                objAc.ObjID = objectIDStrtmp;
                                objAc.Parameter = paramsStrtmp;
                                objAc.OldData = oldObjectStrtmp;
                                objAc.NewData = newObjectStrtmp;
                                objAc.UserAction = userActionsStrtmp;
                                objAc.UserFunction = userFuntionsStrtmp;
                                objAc.UserDescription = userDescriptionsStrtmp;
                                objAc.Description = descriptionStrtmp;
                                lstActionAuditData.Add(objAc);
                            }

                            objectIDStrtmp = new StringBuilder();
                            descriptionStrtmp = new StringBuilder();
                            paramsStrtmp = new StringBuilder();
                            oldObjectStrtmp = new StringBuilder();
                            newObjectStrtmp = new StringBuilder();
                            userFuntionsStrtmp = new StringBuilder();
                            userActionsStrtmp = new StringBuilder();
                            userDescriptionsStrtmp = new StringBuilder();
                        }
                    }
                    this.Save();

                    #endregion
                    #region Sao chep tat ca cac thang cua hoc sinh thi sao chep luon KTHK doi voi GVBM tab Mon Hoc và HDGD
                    if (evaluationID == 1 && typeOfTeacher == 1)//GVBM
                    {
                        //Lay thong tin nhan xet cuoi ky cua tat ca mon hoc duoc chon
                        objDic.Add("lstPupilID", listPupilId);
                        List<SummedEvaluation> listSummedEvaluation = SummedEvaluationBusiness.getListSummedEvaluationByListSubject(objDic).ToList();
                        //Lay thong tin nhan xet cuoi ky lop duoc chon voi mon duoc chon tren giao dien
                        List<SummedEvaluation> listSummEvaluationSource = listSummedEvaluation.Where(p => p.EvaluationCriteriaID == subjectIdSelect).ToList();
                        SummedEvaluation sumObj = null;
                        SummedEvaluation sumObjInsert = null;
                        listEvaluationOfPupil = new List<EvaluationOfPupilBO>();
                        string strEvalutionpattern = string.Empty;
                        for (int j = 0; j < listSummEvaluationSource.Count; j++)
                        {
                            evaluationTmp = new EvaluationOfPupilBO();
                            evaluationTmp.PupilID = listSummEvaluationSource[j].PupilID;
                            evaluationTmp.EvaluationComment = listSummEvaluationSource[j].EndingComments;
                            listEvaluationOfPupil.Add(evaluationTmp);
                        }

                        for (int i = 0; i < listSubjectID.Count; i++)
                        {
                            subjectID = listSubjectID[i];
                            List<SummedEvaluation> listSummedEvabySubjectID = listSummedEvaluation.Where(p => p.EvaluationCriteriaID == subjectID).ToList();
                            for (int j = 0; j < listPupilIDEva.Count; j++)
                            {
                                strEvalutionpattern = listEvaluationOfPupil.Where(p => p.PupilID == listPupilIDEva[j]).Select(p => p.EvaluationComment).FirstOrDefault();
                                sumObj = listSummedEvabySubjectID.Where(p => p.PupilID == listPupilIDEva[j]).FirstOrDefault();//nhan xet cua 1 hoc sinh
                                objAc = lstActionAuditData.Where(p => p.PupilID == listPupilIDEva[j]).FirstOrDefault();
                                if (sumObj != null)
                                {
                                    if ((semeseterID == GlobalConstants.SEMESTER_OF_YEAR_FIRST && !strLockMark.Contains("HK1") && !strLockMark.Contains("CK1"))
                                        || (semeseterID == GlobalConstants.SEMESTER_OF_YEAR_SECOND && !strLockMark.Contains("HK2") && !strLockMark.Contains("CK2")))
                                    {
                                        if (!string.IsNullOrEmpty(sumObj.EndingComments)) continue;
                                        if (objAc != null)
                                        {
                                            objAc.OldData.Append(LOG_NXCK).Append("; ");
                                            objAc.NewData.Append(LOG_NXCK).Append(strEvalutionpattern).Append("; ");
                                        }
                                        sumObj.EndingComments = strEvalutionpattern;
                                        SummedEvaluationBusiness.Update(sumObj);
                                    }
                                }
                                else
                                {
                                    if ((semeseterID == GlobalConstants.SEMESTER_OF_YEAR_FIRST && !strLockMark.Contains("HK1") && !strLockMark.Contains("CK1"))
                                        || (semeseterID == GlobalConstants.SEMESTER_OF_YEAR_SECOND && !strLockMark.Contains("HK2") && !strLockMark.Contains("CK2")))
                                    {
                                        sumObjInsert = new SummedEvaluation();
                                        sumObjInsert.SummedEvaluationID = SummedEvaluationBusiness.GetNextSeq<long>();
                                        sumObjInsert.AcademicYearID = academicYearId;
                                        sumObjInsert.ClassID = classId;
                                        sumObjInsert.CreateTime = DateTime.Now;
                                        sumObjInsert.EducationLevelID = educationLevelId;
                                        sumObjInsert.EndingComments = strEvalutionpattern;
                                        sumObjInsert.EndingEvaluation = string.Empty;
                                        sumObjInsert.EvaluationCriteriaID = subjectID;
                                        sumObjInsert.EvaluationID = evaluationID;
                                        sumObjInsert.EvaluationReTraining = null;
                                        sumObjInsert.LastDigitSchoolID = partitionId;
                                        sumObjInsert.PeriodicEndingMark = null;
                                        sumObjInsert.PupilID = listPupilIDEva[j];
                                        sumObjInsert.RetestMark = null;
                                        sumObjInsert.SchoolID = schoolId;
                                        sumObjInsert.SemesterID = semeseterID;
                                        if (objAc != null)
                                        {
                                            objAc.OldData.Append(LOG_NXCK).Append("; ");
                                        }
                                        SummedEvaluationBusiness.Insert(sumObjInsert);
                                    }
                                }
                            }
                        }
                        SummedEvaluationBusiness.Save();
                    }
                    #endregion
                }
                else if (iCopyObject == 1 && (iCopyLocation == 0 || month == 11))
                {
                    #region TH Ghi de nhan xet cuoi ky cua tat ca hoc sinh
                    string strEvalutionpattern = string.Empty;
                    //Lay thong tin nhan xet cuoi ky cua tat ca mon hoc duoc chon
                    objDic.Add("lstPupilID", listPupilId);
                    List<SummedEvaluation> listSummedEvaluation = SummedEvaluationBusiness.getListSummedEvaluationByListSubject(objDic).ToList();
                    //Lay thong tin nhan xet cuoi ky lop duoc chon voi mon duoc chon tren giao dien
                    List<SummedEvaluation> listSummEvaluationSource = listSummedEvaluation.Where(p => p.EvaluationCriteriaID == subjectIdSelect).ToList();
                    List<int> listPupilIDSum = listSummEvaluationSource.Select(p => p.PupilID).Distinct().ToList();
                    SummedEvaluation sumObj = null;
                    SummedEvaluation sumObjInsert = null;
                    for (int j = 0; j < listSummEvaluationSource.Count; j++)
                    {
                        evaluationTmp = new EvaluationOfPupilBO();
                        evaluationTmp.PupilID = listSummEvaluationSource[j].PupilID;
                        evaluationTmp.EvaluationComment = listSummEvaluationSource[j].EndingComments;
                        listEvaluationOfPupil.Add(evaluationTmp);
                    }

                    //For qua danh sach mon hoc
                    for (int i = 0; i < listSubjectID.Count; i++)
                    {
                        subjectID = listSubjectID[i];
                        //List du lieu nhan xet co trong DB
                        List<SummedEvaluation> listSummedEvabySubjectID = listSummedEvaluation.Where(p => p.EvaluationCriteriaID == subjectID).ToList();
                        for (int k = 0; k < listPupilIDSum.Count; k++)
                        {
                            isChange = false;
                            strEvalutionpattern = listEvaluationOfPupil.Where(p => p.PupilID == listPupilIDSum[k]).Select(p => p.EvaluationComment).FirstOrDefault();
                            sumObj = listSummedEvabySubjectID.Where(p => p.PupilID == listPupilIDSum[k]).FirstOrDefault();//nhan xet cua 1 hoc sinh
                            pupilID = listPupilIDSum[k];
                            objPOC = lstPOC.Where(p => p.PupilID == pupilID).FirstOrDefault();
                            objSC = lstSC.Where(p => p.SubjectCatID == subjectID).FirstOrDefault();
                            objectIDStrtmp.Append(pupilID);
                            descriptionStrtmp.Append("Cập nhật sổ TD CLGD GVBM");
                            paramsStrtmp.Append(pupilID);
                            userFuntionsStrtmp.Append("Sổ TD CLGD (GVBM)");
                            userActionsStrtmp.Append(SMAS.Business.Common.GlobalConstants.ACTION_UPDATE);
                            userDescriptionsStrtmp.Append("Sao chép nhận xét đánh giá " + evaluationName + " HS " + objPOC.PupilFullName + ", " + "mã " + objPOC.PupilCode + ", ");
                            userDescriptionsStrtmp.Append("Lớp ").Append(objPOC.ClassName + "/" + objSC.DisplayName + "/" + "Tháng " + month + "/Học kỳ " + semeseterID + "/Năm học " + acaObj.Year + "-" + (acaObj.Year + 1) + ". ");
                            if (sumObj != null)
                            {
                                if ((semeseterID == GlobalConstants.SEMESTER_OF_YEAR_FIRST && !strLockMark.Contains("HK1") && !strLockMark.Contains("CK1"))
                                    || (semeseterID == GlobalConstants.SEMESTER_OF_YEAR_SECOND && !strLockMark.Contains("HK2") && !strLockMark.Contains("CK2")))
                                {
                                    if (sumObj.EndingComments != strEvalutionpattern)
                                    {
                                        oldObjectStrtmp.Append(LOG_NXCK).Append(sumObj.EndingComments).Append("; ");
                                        newObjectStrtmp.Append(LOG_NXCK).Append(strEvalutionpattern).Append("; ");
                                        isChange = true;
                                        sumObj.EndingComments = strEvalutionpattern;
                                    }
                                    SummedEvaluationBusiness.Update(sumObj);
                                }
                            }
                            else
                            {
                                if ((semeseterID == GlobalConstants.SEMESTER_OF_YEAR_FIRST && !strLockMark.Contains("HK1") && !strLockMark.Contains("CK1"))
                                    || (semeseterID == GlobalConstants.SEMESTER_OF_YEAR_SECOND && !strLockMark.Contains("HK2") && !strLockMark.Contains("CK2")))
                                {
                                    sumObjInsert = new SummedEvaluation();
                                    sumObjInsert.SummedEvaluationID = SummedEvaluationBusiness.GetNextSeq<long>();
                                    sumObjInsert.AcademicYearID = academicYearId;
                                    sumObjInsert.ClassID = classId;
                                    sumObjInsert.CreateTime = DateTime.Now;
                                    sumObjInsert.EducationLevelID = educationLevelId;
                                    sumObjInsert.EndingComments = strEvalutionpattern;
                                    sumObjInsert.EndingEvaluation = string.Empty;
                                    sumObjInsert.EvaluationCriteriaID = subjectID;
                                    sumObjInsert.EvaluationID = evaluationID;
                                    sumObjInsert.EvaluationReTraining = null;
                                    sumObjInsert.LastDigitSchoolID = partitionId;
                                    sumObjInsert.PeriodicEndingMark = null;
                                    sumObjInsert.PupilID = listPupilIDSum[k];
                                    sumObjInsert.RetestMark = null;
                                    sumObjInsert.SchoolID = schoolId;
                                    sumObjInsert.SemesterID = semeseterID;
                                    newObjectStrtmp.Append(LOG_NXCK).Append(strEvalutionpattern).Append("; ");
                                    isChange = true;
                                    SummedEvaluationBusiness.Insert(sumObjInsert);
                                }
                            }
                            if (isChange)
                            {
                                objAc = new ActionAuditDataBO();
                                objAc.PupilID = pupilID;
                                objAc.ClassID = classId;
                                objAc.EvaluationID = evaluationID;
                                objAc.SubjectID = subjectID;
                                objAc.ObjID = objectIDStrtmp;
                                objAc.Parameter = paramsStrtmp;
                                objAc.OldData = oldObjectStrtmp;
                                objAc.NewData = newObjectStrtmp;
                                objAc.UserAction = userActionsStrtmp;
                                objAc.UserFunction = userFuntionsStrtmp;
                                objAc.UserDescription = userDescriptionsStrtmp;
                                objAc.Description = descriptionStrtmp;
                                lstActionAuditData.Add(objAc);
                            }

                            objectIDStrtmp = new StringBuilder();
                            descriptionStrtmp = new StringBuilder();
                            paramsStrtmp = new StringBuilder();
                            oldObjectStrtmp = new StringBuilder();
                            newObjectStrtmp = new StringBuilder();
                            userFuntionsStrtmp = new StringBuilder();
                            userActionsStrtmp = new StringBuilder();
                            userDescriptionsStrtmp = new StringBuilder();
                        }
                    }
                    SummedEvaluationBusiness.Save();
                    #endregion
                }
                else if (iCopyObject == 2 && (iCopyLocation == 0 || month == 11))
                {
                    #region TH Ghi de nhan xet cuoi ky cua tat ca hoc sinh chua duoc nhan xet
                    string strEvalutionpattern = string.Empty;
                    //Lay thong tin nhan xet cuoi ky cua tat ca mon hoc duoc chon
                    objDic.Add("lstPupilID", listPupilId);
                    List<SummedEvaluation> listSummedEvaluation = SummedEvaluationBusiness.getListSummedEvaluationByListSubject(objDic).ToList();
                    //Lay thong tin nhan xet cuoi ky lop duoc chon voi mon duoc chon tren giao dien
                    List<SummedEvaluation> listSummEvaluationSource = listSummedEvaluation.Where(p => p.EvaluationCriteriaID == subjectIdSelect).ToList();
                    List<int> listPupilIDSum = listSummEvaluationSource.Select(p => p.PupilID).Distinct().ToList();
                    SummedEvaluation sumObj = null;
                    SummedEvaluation sumObjInsert = null;
                    for (int j = 0; j < listSummEvaluationSource.Count; j++)
                    {
                        evaluationTmp = new EvaluationOfPupilBO();
                        evaluationTmp.PupilID = listSummEvaluationSource[j].PupilID;
                        evaluationTmp.EvaluationComment = listSummEvaluationSource[j].EndingComments;
                        listEvaluationOfPupil.Add(evaluationTmp);
                    }

                    //For qua danh sach mon hoc
                    for (int i = 0; i < listSubjectID.Count; i++)
                    {
                        subjectID = listSubjectID[i];
                        //List du lieu nhan xet co trong DB
                        List<SummedEvaluation> listSummedEvabySubjectID = listSummedEvaluation.Where(p => p.EvaluationCriteriaID == subjectID).ToList();
                        for (int k = 0; k < listPupilIDSum.Count; k++)
                        {
                            strEvalutionpattern = listEvaluationOfPupil.Where(p => p.PupilID == listPupilIDSum[k]).Select(p => p.EvaluationComment).FirstOrDefault();
                            sumObj = listSummedEvabySubjectID.Where(p => p.PupilID == listPupilIDSum[k]).FirstOrDefault();//nhan xet cua 1 hoc sinh
                            pupilID = listPupilIDSum[k];
                            objPOC = lstPOC.Where(p => p.PupilID == pupilID).FirstOrDefault();
                            objSC = lstSC.Where(p => p.SubjectCatID == subjectID).FirstOrDefault();
                            objectIDStrtmp.Append(pupilID);
                            descriptionStrtmp.Append("Cập nhật sổ TD CLGD GVBM");
                            paramsStrtmp.Append(pupilID);
                            userFuntionsStrtmp.Append("Sổ TD CLGD (GVBM)");
                            userActionsStrtmp.Append(SMAS.Business.Common.GlobalConstants.ACTION_UPDATE);
                            userDescriptionsStrtmp.Append("Sao chép nhận xét đánh giá " + evaluationName + " HS " + objPOC.PupilFullName + ", " + "mã " + objPOC.PupilCode + ", ");
                            userDescriptionsStrtmp.Append("Lớp ").Append(objPOC.ClassName + "/" + objSC.DisplayName + "/" + "Tháng " + month + "/Học kỳ " + semeseterID + "/Năm học " + acaObj.Year + "-" + (acaObj.Year + 1) + ". ");
                            if (sumObj != null)
                            {
                                if ((semeseterID == GlobalConstants.SEMESTER_OF_YEAR_FIRST && !strLockMark.Contains("HK1") && !strLockMark.Contains("CK1"))
                                    || (semeseterID == GlobalConstants.SEMESTER_OF_YEAR_SECOND && !strLockMark.Contains("HK2") && !strLockMark.Contains("CK2")))
                                {
                                    if (!string.IsNullOrEmpty(sumObj.EndingComments)) continue;
                                    oldObjectStrtmp.Append(LOG_NXCK).Append("; ");
                                    newObjectStrtmp.Append(LOG_NXCK).Append(strEvalutionpattern).Append("; ");
                                    sumObj.EndingComments = strEvalutionpattern;
                                    SummedEvaluationBusiness.Update(sumObj);
                                }
                            }
                            else
                            {
                                if ((semeseterID == GlobalConstants.SEMESTER_OF_YEAR_FIRST && !strLockMark.Contains("HK1") && !strLockMark.Contains("CK1"))
                                    || (semeseterID == GlobalConstants.SEMESTER_OF_YEAR_SECOND && !strLockMark.Contains("HK2") && !strLockMark.Contains("CK2")))
                                {
                                    sumObjInsert = new SummedEvaluation();
                                    sumObjInsert.SummedEvaluationID = SummedEvaluationBusiness.GetNextSeq<long>();
                                    sumObjInsert.AcademicYearID = academicYearId;
                                    sumObjInsert.ClassID = classId;
                                    sumObjInsert.CreateTime = DateTime.Now;
                                    sumObjInsert.EducationLevelID = educationLevelId;
                                    sumObjInsert.EndingComments = strEvalutionpattern;
                                    sumObjInsert.EndingEvaluation = string.Empty;
                                    sumObjInsert.EvaluationCriteriaID = subjectID;
                                    sumObjInsert.EvaluationID = evaluationID;
                                    sumObjInsert.EvaluationReTraining = null;
                                    sumObjInsert.LastDigitSchoolID = partitionId;
                                    sumObjInsert.PeriodicEndingMark = null;
                                    sumObjInsert.PupilID = listPupilIDSum[k];
                                    sumObjInsert.RetestMark = null;
                                    sumObjInsert.SchoolID = schoolId;
                                    sumObjInsert.SemesterID = semeseterID;
                                    newObjectStrtmp.Append(LOG_NXCK).Append(strEvalutionpattern).Append("; ");
                                    SummedEvaluationBusiness.Insert(sumObjInsert);
                                }
                            }
                            objAc = new ActionAuditDataBO();
                            objAc.PupilID = pupilID;
                            objAc.ClassID = classId;
                            objAc.EvaluationID = evaluationID;
                            objAc.SubjectID = subjectID;
                            objAc.ObjID = objectIDStrtmp;
                            objAc.Parameter = paramsStrtmp;
                            objAc.OldData = oldObjectStrtmp;
                            objAc.NewData = newObjectStrtmp;
                            objAc.UserAction = userActionsStrtmp;
                            objAc.UserFunction = userFuntionsStrtmp;
                            objAc.UserDescription = userDescriptionsStrtmp;
                            objAc.Description = descriptionStrtmp;
                            lstActionAuditData.Add(objAc);

                            objectIDStrtmp = new StringBuilder();
                            descriptionStrtmp = new StringBuilder();
                            paramsStrtmp = new StringBuilder();
                            oldObjectStrtmp = new StringBuilder();
                            newObjectStrtmp = new StringBuilder();
                            userFuntionsStrtmp = new StringBuilder();
                            userActionsStrtmp = new StringBuilder();
                            userDescriptionsStrtmp = new StringBuilder();
                        }
                    }
                    SummedEvaluationBusiness.Save();
                    #endregion
                }
            }
        }

        public List<EvaluationCommentActionAuditBO> CopyDataEvaluationForHeadTeacher(IDictionary<string, object> objDic, int iCopyObject, int iCopyLocation, int? month, int subjectIdSelect)
        {
            int academicYearId = Utils.GetInt(objDic["AcademicYearId"]);
            AcademicYear acaObj = AcademicYearBusiness.Find(academicYearId);
            List<EvaluationCommentActionAuditBO> lstActionAuditBO = new List<EvaluationCommentActionAuditBO>();

            int schoolId = Utils.GetInt(objDic["SchoolId"]);
            int partitionId = UtilsBusiness.GetPartionId(schoolId);
            int classId = Utils.GetInt(objDic["ClassId"]);
            int educationLevelId = Utils.GetInt(objDic["EduationLevelId"]);
            int evaluationID = Utils.GetInt(objDic["EvaluationId"]);
            int semeseterID = Utils.GetInt(objDic["SemesterId"]);
            int typeOfTeacher = Utils.GetInt(objDic["TypeOfTeacher"]);

            ClassProfile classProfile = ClassProfileBusiness.Find(classId);
            //Lay mon hoc
            List<SubjectCat> lstSuject = SubjectCatBusiness.All.Where(o => o.AppliedLevel == GlobalConstants.APPLIED_LEVEL_PRIMARY
                ).ToList();

            //Lay tieu chi
            List<EvaluationCriteria> lstEvaluationCriteria = EvaluationCriteriaBusiness.All.ToList();

            //Danh sach hoc sinh dang hoc của lop
            IDictionary<string, object> dic = new Dictionary<string, object>();
            dic["ClassID"] = Utils.GetInt(objDic["ClassId"]);
            dic["Check"] = "Check";
            dic["Status"] = Utils.GetInt(objDic["Status"]);
            List<int> listSubjectID = Utils.GetIntList(objDic["listSubjectId"]);

            IQueryable<PupilOfClass> listPupilOfClass = PupilOfClassBusiness.SearchBySchool(schoolId, dic);
            List<int> listPupilId = listPupilOfClass.Select(p => p.PupilID).ToList();

            List<PupilOfClassBO> lstPOC = (from poc in PupilOfClassBusiness.SearchBySchool(schoolId, dic)
                                           join pf in PupilProfileBusiness.All on poc.PupilID equals pf.PupilProfileID
                                           join cp in ClassProfileBusiness.All on poc.ClassID equals cp.ClassProfileID
                                           where poc.AcademicYearID == academicYearId
                                           && poc.SchoolID == schoolId
                                           && poc.ClassID == classId
                                           && cp.IsActive.Value
                                           select new PupilOfClassBO
                                           {
                                               PupilID = poc.PupilID,
                                               PupilCode = pf.PupilCode,
                                               ClassID = poc.ClassID,
                                               ClassName = cp.DisplayName,
                                               PupilFullName = pf.FullName,
                                               PupilProfile = pf
                                           }).ToList();

            //Danh sach tat ca hoc sinh co trong table nhan xet
            objDic["listSubjectId"] = null;

            List<EvaluationOfPupilBO> listEvaluationOfPupil = new List<EvaluationOfPupilBO>();
            EvaluationOfPupilBO evaluationTmp = null;

            int subjectID = 0;

            //Lay thong tin khoa cot diem
            string strLockMark = this.CheckLockMark(classId, schoolId, academicYearId);
            strLockMark = strLockMark.Replace("T10", "TH10");


            if (iCopyObject == 1 && (iCopyLocation == 0 || month == 11))
            {
                #region TH Ghi de nhan xet cuoi ky cua tat ca hoc sinh
                string strEvalutionpattern = string.Empty;
                //Lay thong tin nhan xet cuoi ky cua tat ca mon hoc duoc chon
                objDic.Add("lstPupilID", listPupilId);
                List<SummedEvaluation> listSummedEvaluation = SummedEvaluationBusiness.getListSummedEvaluationByListSubject(objDic).ToList();
                //Lay thong tin nhan xet cuoi ky lop duoc chon voi mon duoc chon tren giao dien
                List<SummedEvaluation> listSummEvaluationSource = listSummedEvaluation.Where(p => p.EvaluationCriteriaID == subjectIdSelect).ToList();
                List<int> listPupilIDSum = listSummEvaluationSource.Select(p => p.PupilID).Distinct().ToList();
                SummedEvaluation sumObj = null;
                SummedEvaluation sumObjInsert = null;
                for (int j = 0; j < listSummEvaluationSource.Count; j++)
                {
                    evaluationTmp = new EvaluationOfPupilBO();
                    evaluationTmp.PupilID = listSummEvaluationSource[j].PupilID;
                    evaluationTmp.EvaluationComment = listSummEvaluationSource[j].EndingComments;
                    listEvaluationOfPupil.Add(evaluationTmp);
                }

                //For qua danh sach mon hoc
                for (int i = 0; i < listSubjectID.Count; i++)
                {
                    subjectID = listSubjectID[i];
                    //List du lieu nhan xet co trong DB
                    List<SummedEvaluation> listSummedEvabySubjectID = listSummedEvaluation.Where(p => p.EvaluationCriteriaID == subjectID).ToList();
                    for (int k = 0; k < listPupilIDSum.Count; k++)
                    {
                        strEvalutionpattern = listEvaluationOfPupil.Where(p => p.PupilID == listPupilIDSum[k]).Select(p => p.EvaluationComment).FirstOrDefault();
                        sumObj = listSummedEvabySubjectID.Where(p => p.PupilID == listPupilIDSum[k]).FirstOrDefault();//nhan xet cua 1 hoc sinh

                        if (sumObj != null)
                        {
                            if ((semeseterID == GlobalConstants.SEMESTER_OF_YEAR_FIRST && !strLockMark.Contains("HK1") && !strLockMark.Contains("CK1"))
                                || (semeseterID == GlobalConstants.SEMESTER_OF_YEAR_SECOND && !strLockMark.Contains("HK2") && !strLockMark.Contains("CK2")))
                            {
                                sumObj.EndingComments = strEvalutionpattern;
                                sumObj.UpdateTime = DateTime.Now;
                                SummedEvaluationBusiness.Update(sumObj);

                                //luu log
                                if (String.IsNullOrEmpty(sumObj.EndingEvaluation)) sumObj.EndingEvaluation = null;
                                if (String.IsNullOrEmpty(sumObj.EndingComments)) sumObj.EndingComments = null;

                                if (sumObj.EndingEvaluation != this.context.Entry<SummedEvaluation>(sumObj).OriginalValues.GetValue<string>("EndingEvaluation")
                                    || sumObj.PeriodicEndingMark != this.context.Entry<SummedEvaluation>(sumObj).OriginalValues.GetValue<Nullable<int>>("PeriodicEndingMark")
                                    || sumObj.EndingComments != this.context.Entry<SummedEvaluation>(sumObj).OriginalValues.GetValue<string>("EndingComments"))
                                {

                                    EvaluationCommentActionAuditBO aab = lstActionAuditBO.Where(o => o.ObjectId == sumObj.PupilID && o.EvaluationCriteriaID == sumObj.EvaluationCriteriaID).FirstOrDefault();
                                    if (aab == null)
                                    {
                                        aab = new EvaluationCommentActionAuditBO(EvaluationCommentActionAuditBO.EVALUATION_COMMENT_AUDIT_ACTION_COPPY);
                                        lstActionAuditBO.Add(aab);
                                        aab.AcademicYear = acaObj.DisplayTitle;
                                        aab.ClassName = classProfile.DisplayName;
                                        aab.Month = month.Value;
                                        aab.ObjectId = sumObj.PupilID;
                                        PupilOfClassBO poc = lstPOC.Where(o => o.PupilID == sumObj.PupilID).FirstOrDefault();
                                        aab.PupilCode = poc.PupilProfile.PupilCode;
                                        aab.PupilName = poc.PupilProfile.FullName;
                                        aab.Semester = semeseterID;
                                        SubjectCat sc = lstSuject.Where(o => o.SubjectCatID == sumObj.EvaluationCriteriaID).FirstOrDefault();
                                        if (sc != null)
                                        {
                                            aab.SubjectName = sc.DisplayName;
                                        }
                                        EvaluationCriteria ec = lstEvaluationCriteria.FirstOrDefault(o => o.EvaluationCriteriaID == sumObj.EvaluationCriteriaID);
                                        if (ec != null)
                                        {
                                            aab.EvaluationCriteriaName = ec.CriteriaName;
                                        }
                                        aab.EvaluationID = evaluationID;
                                        aab.EvaluationCriteriaID = sumObj.EvaluationCriteriaID;
                                    }
                                    if (sumObj.PeriodicEndingMark != this.context.Entry<SummedEvaluation>(sumObj).OriginalValues.GetValue<Nullable<int>>("PeriodicEndingMark"))
                                    {
                                        aab.NewObjectValue.Add(CRITERIA_KTDKCK, sumObj.PeriodicEndingMark.HasValue ? sumObj.PeriodicEndingMark.Value.ToString() : String.Empty);
                                        aab.OldObjectValue.Add(CRITERIA_KTDKCK, this.context.Entry<SummedEvaluation>(sumObj).OriginalValues.GetValue<Nullable<int>>("PeriodicEndingMark").HasValue
                                            ? this.context.Entry<SummedEvaluation>(sumObj).OriginalValues.GetValue<Nullable<int>>("PeriodicEndingMark").Value.ToString() : string.Empty);
                                    }

                                    if (sumObj.EndingEvaluation
                                        != this.context.Entry<SummedEvaluation>(sumObj).OriginalValues.GetValue<string>("EndingEvaluation"))
                                    {
                                        aab.NewObjectValue.Add(CRITERIA_DG, sumObj.EndingEvaluation);
                                        aab.OldObjectValue.Add(CRITERIA_DG, this.context.Entry<SummedEvaluation>(sumObj).OriginalValues.GetValue<string>("EndingEvaluation"));
                                    }

                                    if (sumObj.EndingComments
                                        != this.context.Entry<SummedEvaluation>(sumObj).OriginalValues.GetValue<string>("EndingComments"))
                                    {
                                        aab.NewObjectValue.Add(CRITERIA_NXCK, sumObj.EndingComments);
                                        aab.OldObjectValue.Add(CRITERIA_NXCK, this.context.Entry<SummedEvaluation>(sumObj).OriginalValues.GetValue<string>("EndingComments"));

                                    }

                                }
                            }
                        }
                        else
                        {
                            if ((semeseterID == GlobalConstants.SEMESTER_OF_YEAR_FIRST && !strLockMark.Contains("HK1") && !strLockMark.Contains("CK1"))
                                || (semeseterID == GlobalConstants.SEMESTER_OF_YEAR_SECOND && !strLockMark.Contains("HK2") && !strLockMark.Contains("CK2")))
                            {
                                sumObjInsert = new SummedEvaluation();
                                sumObjInsert.SummedEvaluationID = SummedEvaluationBusiness.GetNextSeq<long>();
                                sumObjInsert.AcademicYearID = academicYearId;
                                sumObjInsert.ClassID = classId;
                                sumObjInsert.CreateTime = DateTime.Now;
                                sumObjInsert.EducationLevelID = educationLevelId;
                                sumObjInsert.EndingComments = strEvalutionpattern;
                                sumObjInsert.EndingEvaluation = string.Empty;
                                sumObjInsert.EvaluationCriteriaID = subjectID;
                                sumObjInsert.EvaluationID = evaluationID;
                                sumObjInsert.EvaluationReTraining = null;
                                sumObjInsert.LastDigitSchoolID = partitionId;
                                sumObjInsert.PeriodicEndingMark = null;
                                sumObjInsert.PupilID = listPupilIDSum[k];
                                sumObjInsert.RetestMark = null;
                                sumObjInsert.SchoolID = schoolId;
                                sumObjInsert.SemesterID = semeseterID;
                                SummedEvaluationBusiness.Insert(sumObjInsert);

                                //luu log
                                if (!String.IsNullOrEmpty(sumObjInsert.EndingEvaluation)
                                    || !String.IsNullOrEmpty(sumObjInsert.EndingComments)
                                    || sumObjInsert.PeriodicEndingMark.HasValue)
                                {

                                    //them action audit
                                    //Kiem tra da co action audit hay chua
                                    EvaluationCommentActionAuditBO aab = lstActionAuditBO.Where(o => o.ObjectId == sumObjInsert.PupilID && o.EvaluationCriteriaID == sumObjInsert.EvaluationCriteriaID).FirstOrDefault();
                                    if (aab == null)
                                    {
                                        aab = new EvaluationCommentActionAuditBO(EvaluationCommentActionAuditBO.EVALUATION_COMMENT_AUDIT_ACTION_COPPY);
                                        lstActionAuditBO.Add(aab);
                                        aab.AcademicYear = acaObj.DisplayTitle;
                                        aab.ClassName = classProfile.DisplayName;
                                        aab.Month = month.Value;
                                        aab.ObjectId = sumObjInsert.PupilID;
                                        PupilOfClassBO poc = lstPOC.Where(o => o.PupilID == sumObjInsert.PupilID).FirstOrDefault();
                                        aab.PupilCode = poc.PupilProfile.PupilCode;
                                        aab.PupilName = poc.PupilProfile.FullName;
                                        aab.Semester = semeseterID;
                                        SubjectCat sc = lstSuject.Where(o => o.SubjectCatID == sumObjInsert.EvaluationCriteriaID).FirstOrDefault();
                                        if (sc != null)
                                        {
                                            aab.SubjectName = sc.DisplayName;
                                        }
                                        EvaluationCriteria ec = lstEvaluationCriteria.FirstOrDefault(o => o.EvaluationCriteriaID == sumObjInsert.EvaluationCriteriaID);
                                        if (ec != null)
                                        {
                                            aab.EvaluationCriteriaName = ec.CriteriaName;
                                        }
                                        aab.EvaluationID = evaluationID;
                                        aab.EvaluationCriteriaID = sumObjInsert.EvaluationCriteriaID;
                                    }
                                    if (sumObjInsert.PeriodicEndingMark != null)
                                    {
                                        aab.NewObjectValue.Add(CRITERIA_KTDKCK, sumObjInsert.PeriodicEndingMark.Value.ToString());
                                        aab.OldObjectValue.Add(CRITERIA_KTDKCK, string.Empty);
                                    }

                                    if (!String.IsNullOrEmpty(sumObjInsert.EndingEvaluation))
                                    {
                                        aab.NewObjectValue.Add(CRITERIA_DG, sumObjInsert.EndingEvaluation);
                                        aab.OldObjectValue.Add(CRITERIA_DG, string.Empty);
                                    }

                                    if (!string.IsNullOrEmpty(sumObjInsert.EndingComments))
                                    {
                                        aab.NewObjectValue.Add(CRITERIA_NXCK, sumObjInsert.EndingComments);
                                        aab.OldObjectValue.Add(CRITERIA_NXCK, string.Empty);

                                    }

                                }
                            }
                        }
                    }
                }
                SummedEvaluationBusiness.Save();
                #endregion
            }
            else if (iCopyObject == 2 && (iCopyLocation == 0 || month == 11))
            {
                #region TH Ghi de nhan xet cuoi ky cua tat ca hoc sinh chua duoc nhan xet
                string strEvalutionpattern = string.Empty;
                //Lay thong tin nhan xet cuoi ky cua tat ca mon hoc duoc chon
                objDic.Add("lstPupilID", listPupilId);
                List<SummedEvaluation> listSummedEvaluation = SummedEvaluationBusiness.getListSummedEvaluationByListSubject(objDic).ToList();
                //Lay thong tin nhan xet cuoi ky lop duoc chon voi mon duoc chon tren giao dien
                List<SummedEvaluation> listSummEvaluationSource = listSummedEvaluation.Where(p => p.EvaluationCriteriaID == subjectIdSelect).ToList();
                List<int> listPupilIDSum = listSummEvaluationSource.Select(p => p.PupilID).Distinct().ToList();
                SummedEvaluation sumObj = null;
                SummedEvaluation sumObjInsert = null;
                for (int j = 0; j < listSummEvaluationSource.Count; j++)
                {
                    evaluationTmp = new EvaluationOfPupilBO();
                    evaluationTmp.PupilID = listSummEvaluationSource[j].PupilID;
                    evaluationTmp.EvaluationComment = listSummEvaluationSource[j].EndingComments;
                    listEvaluationOfPupil.Add(evaluationTmp);
                }

                //For qua danh sach mon hoc
                for (int i = 0; i < listSubjectID.Count; i++)
                {
                    subjectID = listSubjectID[i];
                    //List du lieu nhan xet co trong DB
                    List<SummedEvaluation> listSummedEvabySubjectID = listSummedEvaluation.Where(p => p.EvaluationCriteriaID == subjectID).ToList();
                    for (int k = 0; k < listPupilIDSum.Count; k++)
                    {
                        strEvalutionpattern = listEvaluationOfPupil.Where(p => p.PupilID == listPupilIDSum[k]).Select(p => p.EvaluationComment).FirstOrDefault();
                        sumObj = listSummedEvabySubjectID.Where(p => p.PupilID == listPupilIDSum[k]).FirstOrDefault();//nhan xet cua 1 hoc sinh

                        if (sumObj != null)
                        {
                            if ((semeseterID == GlobalConstants.SEMESTER_OF_YEAR_FIRST && !strLockMark.Contains("HK1") && !strLockMark.Contains("CK1"))
                                || (semeseterID == GlobalConstants.SEMESTER_OF_YEAR_SECOND && !strLockMark.Contains("HK2") && !strLockMark.Contains("CK2")))
                            {
                                if (!string.IsNullOrEmpty(sumObj.EndingComments)) continue;
                                sumObj.EndingComments = strEvalutionpattern;
                                sumObj.UpdateTime = DateTime.Now;
                                SummedEvaluationBusiness.Update(sumObj);

                                //luu log
                                if (String.IsNullOrEmpty(sumObj.EndingEvaluation)) sumObj.EndingEvaluation = null;
                                if (String.IsNullOrEmpty(sumObj.EndingComments)) sumObj.EndingComments = null;

                                if (sumObj.EndingEvaluation != this.context.Entry<SummedEvaluation>(sumObj).OriginalValues.GetValue<string>("EndingEvaluation")
                                    || sumObj.PeriodicEndingMark != this.context.Entry<SummedEvaluation>(sumObj).OriginalValues.GetValue<Nullable<int>>("PeriodicEndingMark")
                                    || sumObj.EndingComments != this.context.Entry<SummedEvaluation>(sumObj).OriginalValues.GetValue<string>("EndingComments"))
                                {

                                    EvaluationCommentActionAuditBO aab = lstActionAuditBO.Where(o => o.ObjectId == sumObj.PupilID && o.EvaluationCriteriaID == sumObj.EvaluationCriteriaID).FirstOrDefault();
                                    if (aab == null)
                                    {
                                        aab = new EvaluationCommentActionAuditBO(EvaluationCommentActionAuditBO.EVALUATION_COMMENT_AUDIT_ACTION_COPPY);
                                        lstActionAuditBO.Add(aab);
                                        aab.AcademicYear = acaObj.DisplayTitle;
                                        aab.ClassName = classProfile.DisplayName;
                                        aab.Month = month.Value;
                                        aab.ObjectId = sumObj.PupilID;
                                        PupilOfClassBO poc = lstPOC.Where(o => o.PupilID == sumObj.PupilID).FirstOrDefault();
                                        aab.PupilCode = poc.PupilProfile.PupilCode;
                                        aab.PupilName = poc.PupilProfile.FullName;
                                        aab.Semester = semeseterID;
                                        SubjectCat sc = lstSuject.Where(o => o.SubjectCatID == sumObj.EvaluationCriteriaID).FirstOrDefault();
                                        if (sc != null)
                                        {
                                            aab.SubjectName = sc.DisplayName;
                                        }
                                        EvaluationCriteria ec = lstEvaluationCriteria.FirstOrDefault(o => o.EvaluationCriteriaID == sumObj.EvaluationCriteriaID);
                                        if (ec != null)
                                        {
                                            aab.EvaluationCriteriaName = ec.CriteriaName;
                                        }
                                        aab.EvaluationID = evaluationID;
                                        aab.EvaluationCriteriaID = sumObj.EvaluationCriteriaID;
                                    }
                                    if (sumObj.PeriodicEndingMark != this.context.Entry<SummedEvaluation>(sumObj).OriginalValues.GetValue<Nullable<int>>("PeriodicEndingMark"))
                                    {
                                        aab.NewObjectValue.Add(CRITERIA_KTDKCK, sumObj.PeriodicEndingMark.HasValue ? sumObj.PeriodicEndingMark.Value.ToString() : String.Empty);
                                        aab.OldObjectValue.Add(CRITERIA_KTDKCK, this.context.Entry<SummedEvaluation>(sumObj).OriginalValues.GetValue<Nullable<int>>("PeriodicEndingMark").HasValue
                                            ? this.context.Entry<SummedEvaluation>(sumObj).OriginalValues.GetValue<Nullable<int>>("PeriodicEndingMark").Value.ToString() : string.Empty);
                                    }

                                    if (sumObj.EndingEvaluation
                                        != this.context.Entry<SummedEvaluation>(sumObj).OriginalValues.GetValue<string>("EndingEvaluation"))
                                    {
                                        aab.NewObjectValue.Add(CRITERIA_DG, sumObj.EndingEvaluation);
                                        aab.OldObjectValue.Add(CRITERIA_DG, this.context.Entry<SummedEvaluation>(sumObj).OriginalValues.GetValue<string>("EndingEvaluation"));
                                    }

                                    if (sumObj.EndingComments
                                        != this.context.Entry<SummedEvaluation>(sumObj).OriginalValues.GetValue<string>("EndingComments"))
                                    {
                                        aab.NewObjectValue.Add(CRITERIA_NXCK, sumObj.EndingComments);
                                        aab.OldObjectValue.Add(CRITERIA_NXCK, this.context.Entry<SummedEvaluation>(sumObj).OriginalValues.GetValue<string>("EndingComments"));

                                    }

                                }
                            }
                        }
                        else
                        {
                            if ((semeseterID == GlobalConstants.SEMESTER_OF_YEAR_FIRST && !strLockMark.Contains("HK1") && !strLockMark.Contains("CK1"))
                                || (semeseterID == GlobalConstants.SEMESTER_OF_YEAR_SECOND && !strLockMark.Contains("HK2") && !strLockMark.Contains("CK2")))
                            {
                                sumObjInsert = new SummedEvaluation();
                                sumObjInsert.SummedEvaluationID = SummedEvaluationBusiness.GetNextSeq<long>();
                                sumObjInsert.AcademicYearID = academicYearId;
                                sumObjInsert.ClassID = classId;
                                sumObjInsert.CreateTime = DateTime.Now;
                                sumObjInsert.EducationLevelID = educationLevelId;
                                sumObjInsert.EndingComments = strEvalutionpattern;
                                sumObjInsert.EndingEvaluation = string.Empty;
                                sumObjInsert.EvaluationCriteriaID = subjectID;
                                sumObjInsert.EvaluationID = evaluationID;
                                sumObjInsert.EvaluationReTraining = null;
                                sumObjInsert.LastDigitSchoolID = partitionId;
                                sumObjInsert.PeriodicEndingMark = null;
                                sumObjInsert.PupilID = listPupilIDSum[k];
                                sumObjInsert.RetestMark = null;
                                sumObjInsert.SchoolID = schoolId;
                                sumObjInsert.SemesterID = semeseterID;
                                SummedEvaluationBusiness.Insert(sumObjInsert);

                                //luu log
                                if (!String.IsNullOrEmpty(sumObjInsert.EndingEvaluation)
                                    || !String.IsNullOrEmpty(sumObjInsert.EndingComments)
                                    || sumObjInsert.PeriodicEndingMark.HasValue)
                                {

                                    //them action audit
                                    //Kiem tra da co action audit hay chua
                                    EvaluationCommentActionAuditBO aab = lstActionAuditBO.Where(o => o.ObjectId == sumObjInsert.PupilID && o.EvaluationCriteriaID == sumObjInsert.EvaluationCriteriaID).FirstOrDefault();
                                    if (aab == null)
                                    {
                                        aab = new EvaluationCommentActionAuditBO(EvaluationCommentActionAuditBO.EVALUATION_COMMENT_AUDIT_ACTION_COPPY);
                                        lstActionAuditBO.Add(aab);
                                        aab.AcademicYear = acaObj.DisplayTitle;
                                        aab.ClassName = classProfile.DisplayName;
                                        aab.Month = month.Value;
                                        aab.ObjectId = sumObjInsert.PupilID;
                                        PupilOfClassBO poc = lstPOC.Where(o => o.PupilID == sumObjInsert.PupilID).FirstOrDefault();
                                        aab.PupilCode = poc.PupilProfile.PupilCode;
                                        aab.PupilName = poc.PupilProfile.FullName;
                                        aab.Semester = semeseterID;
                                        SubjectCat sc = lstSuject.Where(o => o.SubjectCatID == sumObjInsert.EvaluationCriteriaID).FirstOrDefault();
                                        if (sc != null)
                                        {
                                            aab.SubjectName = sc.DisplayName;
                                        }
                                        EvaluationCriteria ec = lstEvaluationCriteria.FirstOrDefault(o => o.EvaluationCriteriaID == sumObjInsert.EvaluationCriteriaID);
                                        if (ec != null)
                                        {
                                            aab.EvaluationCriteriaName = ec.CriteriaName;
                                        }
                                        aab.EvaluationID = evaluationID;
                                        aab.EvaluationCriteriaID = sumObjInsert.EvaluationCriteriaID;
                                    }
                                    if (sumObjInsert.PeriodicEndingMark != null)
                                    {
                                        aab.NewObjectValue.Add(CRITERIA_KTDKCK, sumObjInsert.PeriodicEndingMark.Value.ToString());
                                        aab.OldObjectValue.Add(CRITERIA_KTDKCK, string.Empty);
                                    }

                                    if (!String.IsNullOrEmpty(sumObjInsert.EndingEvaluation))
                                    {
                                        aab.NewObjectValue.Add(CRITERIA_DG, sumObjInsert.EndingEvaluation);
                                        aab.OldObjectValue.Add(CRITERIA_DG, string.Empty);
                                    }

                                    if (!string.IsNullOrEmpty(sumObjInsert.EndingComments))
                                    {
                                        aab.NewObjectValue.Add(CRITERIA_NXCK, sumObjInsert.EndingComments);
                                        aab.OldObjectValue.Add(CRITERIA_NXCK, string.Empty);

                                    }

                                }
                            }
                        }
                    }
                }
                SummedEvaluationBusiness.Save();
                #endregion
            }

            return lstActionAuditBO;
        }

        public List<string> getListCommentAutoComplete(IDictionary<string, object> dic)
        {
            int SchoolID = Utils.GetInt(dic, "SchoolID");
            int AcademicYearID = Utils.GetInt(dic, "AcademicYearID");
            int EvaluationID = Utils.GetInt(dic, "EvaluationID");
            int TypeOfTeacher = Utils.GetInt(dic, "TypeOfTeacherID");
            int ClassID = Utils.GetInt(dic, "ClassID");
            int SemesterID = Utils.GetInt(dic, "SemesterID");
            int Month = Utils.GetInt(dic, "MonthID");
            bool isAdmin = Utils.GetBool(dic, "isAdmin");
            List<string> lstComment = new List<string>();
            AcademicYear acaObj = AcademicYearBusiness.Find(AcademicYearID);
            List<string> listEvaluationComments = new List<string>();
            List<string> listSummedComments = new List<string>();
            if (UtilsBusiness.IsMoveHistory(acaObj))
            {
                listEvaluationComments = EvaluationCommentsHistoryBusiness.GetListCommentHistory(dic);
                if (isAdmin || TypeOfTeacher == 2)
                {
                    listSummedComments = SummedEvaluationHistoryBusiness.GetListCommentHistory(dic);
                }
            }
            else
            {
                listEvaluationComments = EvaluationCommentsBusiness.GetListComment(dic);
                if (isAdmin || TypeOfTeacher == 2)
                {
                    listSummedComments = SummedEvaluationBusiness.GetListComment(dic);
                }
            }

            if ((listEvaluationComments != null && listEvaluationComments.Count > 0)
                || (listSummedComments != null && listSummedComments.Count > 0))
            {
                lstComment = listEvaluationComments.Union(listSummedComments).Distinct().ToList();
                lstComment = lstComment.Select(p => UtilsBusiness.RemoveSpecialCharacter(p)).ToList();
            }
            return lstComment;
        }

        public List<EvaluationCommentsBO> GetListEvaluationComments(IDictionary<string, object> dic)
        {
            int schoolID = Utils.GetInt(dic, "SchoolID");
            int academicYearID = Utils.GetInt(dic, "AcademicYearID");
            int classID = Utils.GetInt(dic, "ClassID");
            int semesterID = Utils.GetInt(dic, "SemesterID");
            int evaluationID = Utils.GetInt(dic, "EvaluationID");
            int partitionId = UtilsBusiness.GetPartionId(schoolID);
            List<EvaluationCommentsBO> lstEvaluationComments = new List<EvaluationCommentsBO>();

            if (UtilsBusiness.IsMoveHistory(AcademicYearBusiness.Find(academicYearID)))
            {
                lstEvaluationComments = (from ev in EvaluationCommentsHistoryBusiness.All
                                         join sc in SubjectCatBusiness.All on ev.EvaluationCriteriaID equals sc.SubjectCatID
                                         where ev.LastDigitSchoolID == partitionId
                                          && ev.AcademicYearID == academicYearID
                                          && ev.ClassID == classID
                                          && ev.SemesterID == semesterID
                                          && ev.TypeOfTeacher == 1//GVBM
                                          && ev.EvaluationID == evaluationID //tab mat danh gia
                                         select new EvaluationCommentsBO
                                         {
                                             PupilID = ev.PupilID,
                                             EvaluationCriteriaID = ev.EvaluationCriteriaID,
                                             Comment1 = ev.CommentsM1M6,
                                             Comment2 = ev.CommentsM2M7,
                                             Comment3 = ev.CommentsM3M8,
                                             Comment4 = ev.CommentsM4M9,
                                             Comment5 = ev.CommentsM5M10,
                                             SubjectName = sc.DisplayName,
                                             OrderInSubject = sc.OrderInSubject
                                         }).OrderBy(p => p.OrderInSubject).ThenBy(p => p.SubjectName).ToList();
            }
            else
            {
                lstEvaluationComments = (from ev in EvaluationCommentsBusiness.All
                                         join sc in SubjectCatBusiness.All on ev.EvaluationCriteriaID equals sc.SubjectCatID
                                         where ev.LastDigitSchoolID == partitionId
                                          && ev.AcademicYearID == academicYearID
                                          && ev.ClassID == classID
                                          && ev.SemesterID == semesterID
                                          && ev.TypeOfTeacher == 1//GVBM
                                          && ev.EvaluationID == evaluationID //tab mat danh gia
                                         select new EvaluationCommentsBO
                                         {
                                             PupilID = ev.PupilID,
                                             EvaluationCriteriaID = ev.EvaluationCriteriaID,
                                             Comment1 = ev.CommentsM1M6,
                                             Comment2 = ev.CommentsM2M7,
                                             Comment3 = ev.CommentsM3M8,
                                             Comment4 = ev.CommentsM4M9,
                                             Comment5 = ev.CommentsM5M10,
                                             SubjectName = sc.DisplayName,
                                             OrderInSubject = sc.OrderInSubject
                                         }).OrderBy(p => p.OrderInSubject).ThenBy(p => p.SubjectName).ToList();
            }
            return lstEvaluationComments;
        }

        private List<EvaluationComments> SetCommentByGVBM(List<EvaluationComments> lstEvaluationComments, IDictionary<string, object> dic)
        {
            string SPACE = "\r\n";
            int evaluationID = Utils.GetInt(dic["EvaluationID"]);
            //lay ra danh sach nhan xet cua GVBM
            List<EvaluationCommentsBO> lstEV = new List<EvaluationCommentsBO>();
            lstEV = EvaluationCommentsBusiness.GetListEvaluationComments(dic);

            List<EvaluationCommentsBO> lsttmp = null;
            EvaluationCommentsBO objtmp = null;
            StringBuilder strComment1 = new StringBuilder();
            StringBuilder strComment2 = new StringBuilder();
            StringBuilder strComment3 = new StringBuilder();
            StringBuilder strComment4 = new StringBuilder();
            StringBuilder strComment5 = new StringBuilder();
            EvaluationComments objEvaluationComments = null;
            for (int i = 0; i < lstEvaluationComments.Count; i++)
            {
                strComment1 = new StringBuilder();
                strComment2 = new StringBuilder();
                strComment3 = new StringBuilder();
                strComment4 = new StringBuilder();
                strComment5 = new StringBuilder();
                objEvaluationComments = lstEvaluationComments[i];
                lsttmp = lstEV.Where(p => p.PupilID == objEvaluationComments.PupilID).ToList();
                for (int j = 0; j < lsttmp.Count; j++)
                {
                    objtmp = lsttmp[j];
                    if (evaluationID == 1)
                    {
                        if (string.IsNullOrEmpty(objEvaluationComments.CommentsM1M6) && !string.IsNullOrEmpty(objtmp.Comment1))
                        {
                            strComment1.Append("[").Append(objtmp.SubjectName).Append("]").Append(": ").Append(objtmp.Comment1).Append(SPACE);
                        }
                        if (string.IsNullOrEmpty(objEvaluationComments.CommentsM2M7) && !string.IsNullOrEmpty(objtmp.Comment2))
                        {
                            strComment2.Append("[").Append(objtmp.SubjectName).Append("]").Append(": ").Append(objtmp.Comment2).Append(SPACE);
                        }
                        if (string.IsNullOrEmpty(objEvaluationComments.CommentsM3M8) && !string.IsNullOrEmpty(objtmp.Comment3))
                        {
                            strComment3.Append("[").Append(objtmp.SubjectName).Append("]").Append(": ").Append(objtmp.Comment3).Append(SPACE);
                        }
                        if (string.IsNullOrEmpty(objEvaluationComments.CommentsM4M9) && !string.IsNullOrEmpty(objtmp.Comment4))
                        {
                            strComment4.Append("[").Append(objtmp.SubjectName).Append("]").Append(": ").Append(objtmp.Comment4).Append(SPACE);
                        }
                        if (string.IsNullOrEmpty(objEvaluationComments.CommentsM5M10) && !string.IsNullOrEmpty(objtmp.Comment5))
                        {
                            strComment5.Append("[").Append(objtmp.SubjectName).Append("]").Append(": ").Append(objtmp.Comment5).Append(SPACE);
                        }
                    }
                    else
                    {
                        if (string.IsNullOrEmpty(objEvaluationComments.CommentsM1M6) && !string.IsNullOrEmpty(objtmp.Comment1))
                        {
                            strComment1.Append(objtmp.Comment1).Append(SPACE);
                        }
                        if (string.IsNullOrEmpty(objEvaluationComments.CommentsM2M7) && !string.IsNullOrEmpty(objtmp.Comment2))
                        {
                            strComment2.Append(objtmp.Comment2).Append(SPACE);
                        }
                        if (string.IsNullOrEmpty(objEvaluationComments.CommentsM3M8) && !string.IsNullOrEmpty(objtmp.Comment3))
                        {
                            strComment3.Append(objtmp.Comment3).Append(SPACE);
                        }
                        if (string.IsNullOrEmpty(objEvaluationComments.CommentsM4M9) && !string.IsNullOrEmpty(objtmp.Comment4))
                        {
                            strComment4.Append(objtmp.Comment4).Append(SPACE);
                        }
                        if (string.IsNullOrEmpty(objEvaluationComments.CommentsM5M10) && !string.IsNullOrEmpty(objtmp.Comment5))
                        {
                            strComment5.Append(objtmp.Comment5).Append(SPACE);
                        }
                    }
                }

                if (string.IsNullOrEmpty(objEvaluationComments.CommentsM1M6) && !string.IsNullOrEmpty(strComment1.ToString()))
                {
                    objEvaluationComments.CommentsM1M6 = strComment1.ToString().Substring(0, strComment1.ToString().Length - 2);
                }
                if (string.IsNullOrEmpty(objEvaluationComments.CommentsM2M7) && !string.IsNullOrEmpty(strComment2.ToString()))
                {
                    objEvaluationComments.CommentsM2M7 = strComment2.ToString().Substring(0, strComment2.ToString().Length - 2);
                }
                if (string.IsNullOrEmpty(objEvaluationComments.CommentsM3M8) && !string.IsNullOrEmpty(strComment3.ToString()))
                {
                    objEvaluationComments.CommentsM3M8 = strComment3.ToString().Substring(0, strComment3.ToString().Length - 2);
                }
                if (string.IsNullOrEmpty(objEvaluationComments.CommentsM4M9) && !string.IsNullOrEmpty(strComment4.ToString()))
                {
                    objEvaluationComments.CommentsM4M9 = strComment4.ToString().Substring(0, strComment4.ToString().Length - 2);
                }
                if (string.IsNullOrEmpty(objEvaluationComments.CommentsM5M10) && !string.IsNullOrEmpty(strComment5.ToString()))
                {
                    objEvaluationComments.CommentsM5M10 = strComment5.ToString().Substring(0, strComment5.ToString().Length - 2);
                }
            }

            return lstEvaluationComments;
        }

        #region action audit


        public const string CRITERIA_NXT = "Nhận xét tháng";
        public const string CRITERIA_NXCK = "Nhận xét cuối kỳ";
        public const string CRITERIA_KTDKCK = "KTĐK CK";
        public const string CRITERIA_DG = "Đánh giá";
        public const string LOG_NXCK = "Nhận xét CK: ";


        #endregion
        #region SMS Edu
        public List<EvaluationCommentsBO> getEvaluationCommnetsBySchoolOfPrimary(List<int> pupilIDList, int schoolID, int academicYearID, List<int> lstClassID, int semesterID, int monthID, int userAccountID, bool viewAll = true)
        {
            AcademicYear academicYear = AcademicYearBusiness.Find(academicYearID);
            List<EvaluationCommentsBO> lstResult = new List<EvaluationCommentsBO>();
            if (UtilsBusiness.IsMoveHistory(academicYear)) lstResult = getEvaluationCommnetsBySchoolOfPrimaryHistory(pupilIDList, schoolID, academicYearID, lstClassID, semesterID, monthID, userAccountID, viewAll);
            else lstResult = getEvaluationCommnetsBySchoolOfPrimaryNoHistory(pupilIDList, schoolID, academicYearID, lstClassID, semesterID, monthID, userAccountID, viewAll);
            return lstResult;
        }
        /// <summary>
        /// lấy nhận xét học sinh theo mô hình mới VNEN
        /// </summary>
        /// <param name="pupilIDList"></param>
        /// <param name="schoolID"></param>
        /// <param name="academicYearID"></param>
        /// <param name="classID"></param>
        /// <param name="semesterID"></param>
        /// <param name="monthID"></param>
        /// <returns></returns>
        public List<EvaluationCommentsBO> getCommentSchoolVNEN(List<int> pupilIDList, int schoolID, int academicYearID, List<int> lstClassID, int semesterID, int monthID)
        {
            try
            {
                AcademicYear academicYear = AcademicYearBusiness.Find(academicYearID);
                int partitionId = UtilsBusiness.GetPartionId(schoolID);
                List<PupilOfClassBO> pupilOfClassList = (from poc in PupilOfClassBusiness.AllNoTracking
                                                         join pp in PupilProfileBusiness.AllNoTracking.OrderBy(x => x.PupilProfileID) on poc.PupilID equals pp.PupilProfileID
                                                         where pupilIDList.Contains(poc.PupilID)
                                                         && poc.SchoolID == schoolID && poc.AcademicYearID == academicYearID
                                                         && lstClassID.Contains(poc.ClassID)
                                                         && poc.Status == GlobalConstants.PUPIL_STATUS_STUDYING
                                                         select new PupilOfClassBO()
                                                         {
                                                             PupilOfClassID = poc.PupilOfClassID,
                                                             SchoolID = poc.SchoolID,
                                                             AcademicYearID = poc.AcademicYearID,
                                                             ClassID = poc.ClassID,
                                                             PupilID = poc.PupilID,
                                                             PupilCode = pp.PupilCode,
                                                             PupilFullName = pp.FullName,
                                                             Name = pp.Name,
                                                             OrderInClass = poc.OrderInClass,
                                                             Status = poc.Status,
                                                             EndDate = poc.EndDate,
                                                             Birthday = pp.BirthDate,
                                                             BirthPlace = pp.BirthPlace,
                                                             ResidentalAddress = pp.TempResidentalAddress,
                                                             PermanentResidentalAddress = pp.PermanentResidentalAddress,
                                                             EducationLevelID = poc.ClassProfile.EducationLevelID,
                                                             EthnicName = pp.Ethnic.EthnicName,
                                                             Genre = pp.Genre,
                                                             FatherFullName = pp.FatherFullName,
                                                             FatherJobName = pp.FatherJob,
                                                             MotherFullName = pp.MotherFullName,
                                                             MotherJobName = pp.MotherJob,
                                                             SponserFullName = pp.SponsorFullName,
                                                             SponserJobName = pp.SponsorJob,
                                                             ClassName = poc.ClassProfile.DisplayName,
                                                             FatherMobile = pp.FatherMobile,
                                                             FatherEmail = pp.FatherEmail,
                                                             MotherMobile = pp.MotherMobile,
                                                             MotherEmail = pp.MotherEmail,
                                                             SponserMobile = pp.SponsorMobile,
                                                             SponserEmail = pp.SponsorEmail,
                                                             HomeTown = pp.HomeTown,
                                                             EnrolmentDate = pp.EnrolmentDate,
                                                             StorageNumber = pp.StorageNumber
                                                         }).OrderBy(c => c.OrderInClass).ThenBy(c => c.Name).ThenBy(c => c.PupilFullName).ToList();
                List<RewardFinal> rewardFinalList = RewardFinalBusiness.All.Where(p => p.SchoolID == schoolID).ToList();
                List<EvaluationCommentsBO> listResult = new List<EvaluationCommentsBO>();
                int ClassID = 0;
                if (pupilOfClassList != null && pupilOfClassList.Count > 0)
                {
                    EvaluationCommentsBO obj = null;
                    PupilOfClassBO pupilOfClassBO = null;
                    int pupilID = 0;
                    StringBuilder monthComments = null, pupilContent = null;
                    string MonthStr = string.Empty;

                    if (monthID > 0 && monthID != GlobalConstants.MonthID_EndSemester1 && monthID != GlobalConstants.MonthID_EndSemester2)
                    {
                        int Year = academicYear.Year;
                        int month = 0;
                        month = Int32.Parse(monthID.ToString().Substring(4, monthID.ToString().Length - 4));
                        if (month <= 8 && semesterID == GlobalConstants.SEMESTER_OF_YEAR_SECOND) Year = academicYear.Year + 1;
                        #region Danh gia theo thang
                        List<TeacherNoteBookMonthBO> listNotesMonth = (from note in TeacherNoteBookMonthBusiness.All
                                                                       join sc in SubjectCatBusiness.All on note.SubjectID equals sc.SubjectCatID
                                                                       where note.SchoolID == schoolID && note.PartitionID == partitionId
                                                                        && note.AcademicYearID == academicYearID && lstClassID.Contains(note.ClassID)
                                                                        && note.MonthID == monthID
                                                                        && pupilIDList.Contains(note.PupilID)
                                                                       orderby sc.OrderInSubject
                                                                       select new TeacherNoteBookMonthBO
                                                                       {
                                                                           PupilID = note.PupilID,
                                                                           ClassID = note.ClassID,
                                                                           SubjectID = note.SubjectID,
                                                                           SubjectName = sc.SubjectName,
                                                                           MonthID = note.MonthID,
                                                                           CommentCQ = note.CommentCQ,
                                                                           CommentSubject = note.CommentSubject
                                                                       }).ToList();
                        TeacherNoteBookMonthBO noteSubjectMonthBO = null;
                        for (int i = 0; i < pupilOfClassList.Count; i++)
                        {
                            pupilOfClassBO = pupilOfClassList[i];
                            pupilID = pupilOfClassBO.PupilID;
                            obj = new EvaluationCommentsBO();
                            obj.PupilID = pupilID;
                            obj.FullName = pupilOfClassBO.PupilFullName;
                            monthComments = new StringBuilder();

                            List<TeacherNoteBookMonthBO> listCommentsBypupilID = listNotesMonth.Where(p => p.PupilID == pupilID && p.ClassID == pupilOfClassBO.ClassID).ToList();
                            for (int j = 0; j < listCommentsBypupilID.Count; j++)
                            {
                                noteSubjectMonthBO = listCommentsBypupilID[j];
                                if (!string.IsNullOrEmpty(noteSubjectMonthBO.CommentSubject) || !string.IsNullOrEmpty(noteSubjectMonthBO.CommentCQ))
                                {
                                    monthComments.Append(j + 1).Append("/").Append(noteSubjectMonthBO.SubjectName).Append(":\r\n");
                                    if (!string.IsNullOrEmpty(noteSubjectMonthBO.CommentSubject)) monthComments.AppendLine(noteSubjectMonthBO.CommentSubject.Trim());
                                    if (!string.IsNullOrEmpty(noteSubjectMonthBO.CommentCQ)) monthComments.AppendLine(noteSubjectMonthBO.CommentCQ.Trim());
                                }

                            }
                            if (monthComments != null && monthComments.Length > 0)
                            {
                                obj.EvaluationMonth = monthComments.ToString();
                            }
                            listResult.Add(obj);
                        }
                        #endregion
                    }
                    else if (monthID == GlobalConstants.MonthID_EndSemester1 || monthID == GlobalConstants.MonthID_EndSemester2)
                    {
                        #region Nhan xet cuoi ky
                        List<ReviewBookPupil> listReviewPupil = (from rv in ReviewBookPupilBusiness.All
                                                                 where rv.SchoolID == schoolID && rv.PartitionID == partitionId
                                                                    && rv.AcademicYearID == academicYearID && lstClassID.Contains(rv.ClassID)
                                                                    && rv.SemesterID == semesterID
                                                                 && pupilIDList.Contains(rv.PupilID)
                                                                 select rv).ToList();
                        ReviewBookPupil reviewPupil = null;
                        for (int i = 0; i < pupilOfClassList.Count; i++)
                        {
                            monthComments = new StringBuilder();
                            pupilOfClassBO = pupilOfClassList[i];
                            pupilID = pupilOfClassBO.PupilID;
                            ClassID = pupilOfClassBO.ClassID;
                            obj = new EvaluationCommentsBO();
                            obj.PupilID = pupilID;
                            obj.FullName = pupilOfClassBO.PupilFullName;

                            // lay thong tin nhan xet (So tay hoc sinh)
                            reviewPupil = listReviewPupil.FirstOrDefault(p => p.PupilID == pupilID && p.ClassID == ClassID);
                            if (reviewPupil != null)
                            {
                                if (!string.IsNullOrEmpty(reviewPupil.CQComment)) monthComments.AppendLine(string.Format("Biểu hiện nổi bật: {0}", reviewPupil.CQComment.Trim()));
                                if (!string.IsNullOrEmpty(reviewPupil.CommentAdd)) monthComments.AppendLine(string.Format("Điều cần khắc phục giúp đỡ, rèn luyện thêm: {0}", reviewPupil.CommentAdd.Trim()));
                            }


                            if (monthComments != null && monthComments.Length > 0)
                            {
                                obj.EvaluationMonth = monthComments.ToString();
                            }
                            listResult.Add(obj);
                        }
                        #endregion
                    }
                    else
                    {
                        #region Ket qua hoc ky
                        List<SubjectCatBO> lsClassSubject = new List<SubjectCatBO>();
                        if (lstClassID.Count > 0)
                        {
                            lsClassSubject = (from sb in ClassSubjectBusiness.All
                                              join sub in SubjectCatBusiness.All on sb.SubjectID equals sub.SubjectCatID
                                              orderby sb.IsSubjectVNEN descending, sb.IsCommenting, sub.OrderInSubject
                                              where lstClassID.Contains(sb.ClassID)
                                              && sub.IsActive == true
                                              && ((semesterID == GlobalConstants.SEMESTER_OF_YEAR_FIRST && sb.SectionPerWeekFirstSemester > 0)
                                              || (semesterID == GlobalConstants.SEMESTER_OF_YEAR_SECOND && sb.SectionPerWeekSecondSemester > 0))
                                              select new SubjectCatBO
                                              {
                                                  SubjectCatID = sb.SubjectID,
                                                  ClassID = sb.ClassID,
                                                  SubjectName = sub.SubjectName,
                                                  IsCommenting = sb.IsCommenting,
                                                  IsSubjectVNEN = sb.IsSubjectVNEN
                                              }).ToList();
                        }

                        // Danh sach danh gia So tay GV
                        List<TeacherNoteBookSemester> listNotesSem = (from tbs in TeacherNoteBookSemesterBusiness.All
                                                                      where tbs.SchoolID == schoolID && tbs.PartitionID == partitionId
                                                                       && tbs.AcademicYearID == academicYearID
                                                                       && lstClassID.Contains(tbs.ClassID)
                                                                       && tbs.SemesterID == semesterID
                                                                       && pupilIDList.Contains(tbs.PupilID)
                                                                      select tbs).ToList();
                        // Thong tin theo TT58
                        List<SummedUpRecord> listSummedUpRecord = (from su in SummedUpRecordBusiness.All
                                                                   where su.SchoolID == schoolID && su.Last2digitNumberSchool == partitionId
                                                                    && su.AcademicYearID == academicYearID
                                                                    && lstClassID.Contains(su.ClassID)
                                                                    && ((semesterID == GlobalConstants.SEMESTER_OF_YEAR_SECOND && su.Semester == GlobalConstants.SEMESTER_OF_YEAR_ALL)
                                                                     || (semesterID == GlobalConstants.SEMESTER_OF_YEAR_FIRST && su.Semester == semesterID))
                                                                    && su.PeriodID == null
                                                                    && pupilIDList.Contains(su.PupilID)
                                                                   select su).ToList();
                        // Danh sach danh gia So tay HS
                        List<ReviewBookPupil> listReviewPupil = (from rv in ReviewBookPupilBusiness.All
                                                                 where rv.SchoolID == schoolID && rv.PartitionID == partitionId
                                                                    && rv.AcademicYearID == academicYearID && lstClassID.Contains(rv.ClassID)
                                                                    && rv.SemesterID == semesterID
                                                                 && pupilIDList.Contains(rv.PupilID)
                                                                 select rv).ToList();
                        List<UpdateReward> listRewardComments = (from ur in UpdateRewardBusiness.All
                                                                 where ur.SchoolID == schoolID && ur.PartitionID == partitionId
                                                                            && ur.AcademicYearID == academicYearID
                                                                            && lstClassID.Contains(ur.ClassID)
                                                                            && ur.SemesterID == semesterID
                                                                            && pupilIDList.Contains(ur.PupilID)
                                                                 select ur).ToList();
                        List<TeacherNoteBookSemester> lstNotesPerPupil = null;
                        TeacherNoteBookSemester objNotesPupil = null;
                        SubjectCatBO objSubjectBO = null;
                        SummedUpRecord objSummedUpRecord = null;
                        ReviewBookPupil objReviewPupil = null;
                        for (int i = 0; i < pupilOfClassList.Count; i++)
                        {
                            pupilOfClassBO = pupilOfClassList[i];
                            pupilID = pupilOfClassBO.PupilID;
                            ClassID = pupilOfClassBO.ClassID;
                            obj = new EvaluationCommentsBO();
                            obj.PupilID = pupilID;
                            obj.FullName = pupilOfClassBO.PupilFullName;
                            monthComments = new StringBuilder();
                            // Danh gia mon hoc
                            lstNotesPerPupil = listNotesSem.Where(p => p.PupilID == pupilID && p.ClassID == ClassID).ToList();
                            var lstCS = lsClassSubject.Where(p => p.ClassID == ClassID).ToList();
                            for (int j = 0; j < lstCS.Count; j++)
                            {
                                objSubjectBO = lstCS[j];
                                pupilContent = new StringBuilder();

                                if (objSubjectBO.IsSubjectVNEN == true)
                                {
                                    objNotesPupil = lstNotesPerPupil.FirstOrDefault(o => o.SubjectID == objSubjectBO.SubjectCatID && o.ClassID == ClassID);
                                    if (objNotesPupil != null)
                                    {
                                        if (objNotesPupil.Rate == GlobalConstants.REVIEW_TYPE_FINISH) pupilContent.Append(GlobalConstants.SHORTFINISH).Append(", ");
                                        else if (objNotesPupil.Rate == GlobalConstants.REVIEW_TYPE_NOT_FINISH) pupilContent.Append(GlobalConstants.SHORTNOTFINISH).Append(", ");

                                        if (objSubjectBO.IsCommenting == GlobalConstants.SUBJECTCAT_ISCOMMENTING_MARK && objNotesPupil.AVERAGE_MARK.HasValue) pupilContent.Append((objNotesPupil.AVERAGE_MARK.Value <= 0 ? "0" : objNotesPupil.AVERAGE_MARK.Value == 10 ? "10" : objNotesPupil.AVERAGE_MARK.Value.ToString("0.0").Replace(",","."))).Append("; ");
                                        else if (objSubjectBO.IsCommenting == GlobalConstants.SUBJECTCAT_ISCOMMENTING_COMMENT && !String.IsNullOrEmpty(objNotesPupil.AVERAGE_MARK_JUDGE)) pupilContent.Append(objNotesPupil.AVERAGE_MARK_JUDGE).Append("; ");
                                    }
                                }
                                else
                                {
                                    objSummedUpRecord = listSummedUpRecord.FirstOrDefault(o => o.PupilID == pupilID && o.SubjectID == objSubjectBO.SubjectCatID && o.ClassID == pupilOfClassBO.ClassID);
                                    if (objSummedUpRecord != null)
                                    {
                                        if (objSubjectBO.IsCommenting == GlobalConstants.SUBJECTCAT_ISCOMMENTING_MARK && objSummedUpRecord.SummedUpMark.HasValue) pupilContent.Append(objSummedUpRecord.SummedUpMark.Value <= 0 ? "0" : objSummedUpRecord.SummedUpMark.Value == 10 ? "10" : objSummedUpRecord.SummedUpMark.Value.ToString("0.0").Replace(",",".")).Append("; ");
                                        else if (objSubjectBO.IsCommenting == GlobalConstants.SUBJECTCAT_ISCOMMENTING_COMMENT && !String.IsNullOrEmpty(objSummedUpRecord.JudgementResult)) pupilContent.Append(objSummedUpRecord.JudgementResult).Append("; ");
                                    }
                                }

                                if (pupilContent.Length > 0) monthComments.Append(objSubjectBO.SubjectName).Append(": ").Append(pupilContent.ToString());
                            }

                            if (monthComments.Length > 0) monthComments.Append("\r\n");

                            // Nang luc, pham chat
                            objReviewPupil = listReviewPupil.FirstOrDefault(o => o.PupilID == pupilID && o.ClassID == ClassID);
                            if (objReviewPupil != null)
                            {
                                if (objReviewPupil.CapacityRate == GlobalConstants.EVALUATION_GOOD_COMPLETE_VAL) monthComments.AppendLine("Năng lực: Tốt");
                                else if (objReviewPupil.CapacityRate == GlobalConstants.EVALUATION_CAPQUA_COMPLETE_VAL) monthComments.AppendLine("Năng lực: Đạt");
                                else if (objReviewPupil.CapacityRate == GlobalConstants.EVALUATION_NOT_COMPLETE_VAL) monthComments.AppendLine("Năng lực: Cần cố gắng");

                                if (objReviewPupil.QualityRate == GlobalConstants.EVALUATION_GOOD_COMPLETE_VAL) monthComments.AppendLine("Phẩm chất: Tốt");
                                else if (objReviewPupil.QualityRate == GlobalConstants.EVALUATION_CAPQUA_COMPLETE_VAL) monthComments.AppendLine("Phẩm chất: Đạt");
                                else if (objReviewPupil.QualityRate == GlobalConstants.EVALUATION_NOT_COMPLETE_VAL) monthComments.AppendLine("Phẩm chất: Cần cố gắng");
                            }

                            // Khen thuong
                            string strRewardID = listRewardComments.Where(p => p.PupilID == pupilID && p.ClassID == ClassID).Select(p => p.Rewards).FirstOrDefault();
                            if (!string.IsNullOrEmpty(strRewardID))
                            {
                                List<int> listInt = strRewardID.Split(new[] { "," }, StringSplitOptions.RemoveEmptyEntries).Select(p => int.Parse(p)).ToList();

                                string rewardMode = string.Empty;
                                pupilContent = new StringBuilder();
                                for (int g = 0; g < listInt.Count; g++)
                                {
                                    rewardMode = rewardFinalList.Where(p => p.RewardFinalID == listInt[g]).Select(p => p.RewardMode).FirstOrDefault();
                                    if (!string.IsNullOrWhiteSpace(rewardMode))
                                    {
                                        pupilContent.Append(rewardMode);
                                        if (g != listInt.Count - 1)
                                        {
                                            pupilContent.Append("; ");
                                        }
                                    }
                                }
                                if (pupilContent.Length > 0) monthComments.Append("Khen thưởng: ").AppendLine(pupilContent.ToString());
                            }

                            // Danh gia cuoi nam
                            if (semesterID == GlobalConstants.SEMESTER_OF_YEAR_SECOND)
                            {
                                if (objReviewPupil != null)
                                {
                                    if (objReviewPupil.RateAndYear == GlobalConstants.REVIEW_TYPE_FINISH)
                                    {
                                        monthComments.AppendLine("Được lên lớp");
                                    }
                                    else if (objReviewPupil.RateAndYear == GlobalConstants.REVIEW_TYPE_NOT_FINISH)
                                    {
                                        if (!objReviewPupil.RateAdd.HasValue) monthComments.AppendLine("Chưa hoàn thành chương trình lớp học");
                                        else if (objReviewPupil.RateAdd == GlobalConstants.REVIEW_TYPE_FINISH) monthComments.AppendLine("Được lên lớp");
                                        else if (objReviewPupil.RateAdd == GlobalConstants.REVIEW_TYPE_NOT_FINISH) monthComments.AppendLine("Ở lại lớp");
                                    }
                                }
                            }

                            if (monthComments != null && monthComments.Length > 0)
                            {
                                obj.EvaluationMonth = monthComments.ToString();
                            }
                            listResult.Add(obj);
                        }
                        #endregion
                    }
                }

                return listResult;
            }
            catch (Exception ex)
            {
                logger.Error(ex.Message, ex);
                return new List<EvaluationCommentsBO>();
            }
        }
        private List<EvaluationCommentsBO> getEvaluationCommnetsBySchoolOfPrimaryNoHistory(List<int> pupilIDList, int schoolID, int academicYearID, List<int> lstClassID, int semesterID, int monthID, int userAccountID, bool viewAll = true)
        {
            try
            {
                int TypeOfTeacher = 2;// GVCN
                int partitionId = UtilsBusiness.GetPartionId(schoolID);
                int partitionIDReward = UtilsBusiness.GetPartionId(schoolID, 20);
                //list hoc sinh trong lop
                List<PupilOfClassBO> pupilOfClassList = (from poc in PupilOfClassBusiness.AllNoTracking
                                                         join pp in PupilProfileBusiness.AllNoTracking.OrderBy(x => x.PupilProfileID) on poc.PupilID equals pp.PupilProfileID
                                                         where pupilIDList.Contains(poc.PupilID)
                                                         && poc.SchoolID == schoolID && poc.AcademicYearID == academicYearID
                                                         && lstClassID.Contains(poc.ClassID)
                                                         && poc.Status == GlobalConstants.PUPIL_STATUS_STUDYING
                                                         select new PupilOfClassBO()
                                                         {
                                                             PupilOfClassID = poc.PupilOfClassID,
                                                             SchoolID = poc.SchoolID,
                                                             AcademicYearID = poc.AcademicYearID,
                                                             ClassID = poc.ClassID,
                                                             PupilID = poc.PupilID,
                                                             PupilCode = pp.PupilCode,
                                                             PupilFullName = pp.FullName,
                                                             Name = pp.Name,
                                                             OrderInClass = poc.OrderInClass,
                                                             Status = poc.Status,
                                                             EndDate = poc.EndDate,
                                                             Birthday = pp.BirthDate,
                                                             BirthPlace = pp.BirthPlace,
                                                             ResidentalAddress = pp.TempResidentalAddress,
                                                             PermanentResidentalAddress = pp.PermanentResidentalAddress,
                                                             EducationLevelID = poc.ClassProfile.EducationLevelID,
                                                             EthnicName = pp.Ethnic.EthnicName,
                                                             Genre = pp.Genre,
                                                             FatherFullName = pp.FatherFullName,
                                                             FatherJobName = pp.FatherJob,
                                                             MotherFullName = pp.MotherFullName,
                                                             MotherJobName = pp.MotherJob,
                                                             SponserFullName = pp.SponsorFullName,
                                                             SponserJobName = pp.SponsorJob,
                                                             ClassName = poc.ClassProfile.DisplayName,
                                                             FatherMobile = pp.FatherMobile,
                                                             FatherEmail = pp.FatherEmail,
                                                             MotherMobile = pp.MotherMobile,
                                                             MotherEmail = pp.MotherEmail,
                                                             SponserMobile = pp.SponsorMobile,
                                                             SponserEmail = pp.SponsorEmail,
                                                             HomeTown = pp.HomeTown,
                                                             EnrolmentDate = pp.EnrolmentDate,
                                                             StorageNumber = pp.StorageNumber
                                                         }).OrderBy(c => c.OrderInClass).ThenBy(c => c.Name).ThenBy(c => c.PupilFullName).ToList();
                List<RewardFinal> rewardFinalList = RewardFinalBusiness.All.Where(p => p.SchoolID == schoolID).ToList();
                //list du lieu tra ve (1 hoc sinh thi co 1 dong nhan xet GLGD,PC,NL)
                List<EvaluationCommentsBO> listResult = new List<EvaluationCommentsBO>();
                EvaluationCommentsBO obj = null;
                PupilOfClassBO pupilOfClassBO = null;
                int pupilID = 0;
                StringBuilder monthComments = null;
                string title = string.Empty;
                int classID = 0;
                if (pupilOfClassList != null && pupilOfClassList.Count > 0)
                {
                    if (monthID < GlobalConstants.MonthID_11 && monthID > 0)
                    {
                        #region Danh gia theo thang
                        //list hoc sinh duoc danh gia GLGD nang luc và pham chat.
                        List<EvaluationComments> listEvaluationComments = (from ev in EvaluationCommentsBusiness.All
                                                                           where
                                                                           ev.LastDigitSchoolID == partitionId
                                                                           && ev.AcademicYearID == academicYearID
                                                                            && lstClassID.Contains(ev.ClassID)
                                                                            && ev.SemesterID == semesterID
                                                                            && ev.TypeOfTeacher == TypeOfTeacher
                                                                            && pupilIDList.Contains(ev.PupilID)
                                                                           select ev).ToList();
                        for (int i = 0; i < pupilOfClassList.Count; i++)
                        {
                            pupilOfClassBO = pupilOfClassList[i];
                            pupilID = pupilOfClassBO.PupilID;
                            classID = pupilOfClassBO.ClassID;
                            obj = new EvaluationCommentsBO();
                            obj.PupilID = pupilID;
                            obj.FullName = pupilOfClassBO.PupilFullName;
                            monthComments = new StringBuilder();
                            //tao chuoi nhan xet (CLGD,PC,NL)
                            List<EvaluationComments> listCommentsBypupilID = listEvaluationComments.Where(p => p.ClassID == classID && p.PupilID == pupilID).ToList();

                            monthComments.Append("A) Môn học và HĐGD: ");
                            if (monthID == GlobalConstants.MonthID_1 || monthID == GlobalConstants.MonthID_6)
                            {
                                monthComments.Append(listCommentsBypupilID.Where(p => p.EvaluationID == GlobalConstants.TAB_EDUCATION_BOOKMARK_PRIMARY).Select(p => p.CommentsM1M6).FirstOrDefault());
                            }
                            else if (monthID == GlobalConstants.MonthID_2 || monthID == GlobalConstants.MonthID_7)
                            {
                                monthComments.Append(listCommentsBypupilID.Where(p => p.EvaluationID == GlobalConstants.TAB_EDUCATION_BOOKMARK_PRIMARY).Select(p => p.CommentsM2M7).FirstOrDefault());
                            }
                            else if (monthID == GlobalConstants.MonthID_3 || monthID == GlobalConstants.MonthID_8)
                            {
                                monthComments.Append(listCommentsBypupilID.Where(p => p.EvaluationID == GlobalConstants.TAB_EDUCATION_BOOKMARK_PRIMARY).Select(p => p.CommentsM3M8).FirstOrDefault());
                            }
                            else if (monthID == GlobalConstants.MonthID_4 || monthID == GlobalConstants.MonthID_9)
                            {
                                monthComments.Append(listCommentsBypupilID.Where(p => p.EvaluationID == GlobalConstants.TAB_EDUCATION_BOOKMARK_PRIMARY).Select(p => p.CommentsM4M9).FirstOrDefault());
                            }
                            else if (monthID == GlobalConstants.MonthID_5 || monthID == GlobalConstants.MonthID_10)
                            {
                                monthComments.Append(listCommentsBypupilID.Where(p => p.EvaluationID == GlobalConstants.TAB_EDUCATION_BOOKMARK_PRIMARY).Select(p => p.CommentsM5M10).FirstOrDefault());
                            }

                            if (monthComments != null)
                            {
                                monthComments.Append("\r\n");
                            }

                            monthComments.Append("B) Năng lực: ");
                            if (monthID == GlobalConstants.MonthID_1 || monthID == GlobalConstants.MonthID_6)
                            {
                                monthComments.Append(listCommentsBypupilID.Where(p => p.EvaluationID == GlobalConstants.TAB_CAPACTIES_BOOKMARK_PRIMARY).Select(p => p.CommentsM1M6).FirstOrDefault());
                            }
                            else if (monthID == GlobalConstants.MonthID_2 || monthID == GlobalConstants.MonthID_7)
                            {
                                monthComments.Append(listCommentsBypupilID.Where(p => p.EvaluationID == GlobalConstants.TAB_CAPACTIES_BOOKMARK_PRIMARY).Select(p => p.CommentsM2M7).FirstOrDefault());
                            }
                            else if (monthID == GlobalConstants.MonthID_3 || monthID == GlobalConstants.MonthID_8)
                            {
                                monthComments.Append(listCommentsBypupilID.Where(p => p.EvaluationID == GlobalConstants.TAB_CAPACTIES_BOOKMARK_PRIMARY).Select(p => p.CommentsM3M8).FirstOrDefault());
                            }
                            else if (monthID == GlobalConstants.MonthID_4 || monthID == GlobalConstants.MonthID_9)
                            {
                                monthComments.Append(listCommentsBypupilID.Where(p => p.EvaluationID == GlobalConstants.TAB_CAPACTIES_BOOKMARK_PRIMARY).Select(p => p.CommentsM4M9).FirstOrDefault());
                            }
                            else if (monthID == GlobalConstants.MonthID_5 || monthID == GlobalConstants.MonthID_10)
                            {
                                monthComments.Append(listCommentsBypupilID.Where(p => p.EvaluationID == GlobalConstants.TAB_CAPACTIES_BOOKMARK_PRIMARY).Select(p => p.CommentsM5M10).FirstOrDefault());
                            }
                            if (monthComments != null)
                            {
                                monthComments.Append("\r\n");
                            }

                            monthComments.Append("C) Phẩm chất: ");
                            if (monthID == GlobalConstants.MonthID_1 || monthID == GlobalConstants.MonthID_6)
                            {
                                monthComments.Append(listCommentsBypupilID.Where(p => p.EvaluationID == GlobalConstants.TAB_QUALITIES_BOOKMARK_PRIMARY).Select(p => p.CommentsM1M6).FirstOrDefault());
                            }
                            else if (monthID == GlobalConstants.MonthID_2 || monthID == GlobalConstants.MonthID_7)
                            {
                                monthComments.Append(listCommentsBypupilID.Where(p => p.EvaluationID == GlobalConstants.TAB_QUALITIES_BOOKMARK_PRIMARY).Select(p => p.CommentsM2M7).FirstOrDefault());
                            }
                            else if (monthID == GlobalConstants.MonthID_3 || monthID == GlobalConstants.MonthID_8)
                            {
                                monthComments.Append(listCommentsBypupilID.Where(p => p.EvaluationID == GlobalConstants.TAB_QUALITIES_BOOKMARK_PRIMARY).Select(p => p.CommentsM3M8).FirstOrDefault());
                            }
                            else if (monthID == GlobalConstants.MonthID_4 || monthID == GlobalConstants.MonthID_9)
                            {
                                monthComments.Append(listCommentsBypupilID.Where(p => p.EvaluationID == GlobalConstants.TAB_QUALITIES_BOOKMARK_PRIMARY).Select(p => p.CommentsM4M9).FirstOrDefault());
                            }
                            else if (monthID == GlobalConstants.MonthID_5 || monthID == GlobalConstants.MonthID_10)
                            {
                                monthComments.Append(listCommentsBypupilID.Where(p => p.EvaluationID == GlobalConstants.TAB_QUALITIES_BOOKMARK_PRIMARY).Select(p => p.CommentsM5M10).FirstOrDefault());
                            }
                            if (monthComments != null)
                            {
                                monthComments.Append("\r\n");
                            }

                            if (monthComments != null)
                            {
                                obj.EvaluationMonth = monthComments.ToString();
                            }
                            listResult.Add(obj);
                        }
                        #endregion
                    }
                    else if (monthID == GlobalConstants.MonthID_11)
                    {
                        #region Ket qua hoc ky
                        monthComments = new StringBuilder();
                        title = string.Empty;
                        //list mon hoc
                        List<SubjectCatBO> lsClassSubject = new List<SubjectCatBO>();
                        List<SummedEndingEvaluation> listSummedEndingEvaluationByPupilID = new List<SummedEndingEvaluation>();
                        if (classID > 0)
                        {
                            lsClassSubject = (from sb in ClassSubjectBusiness.All
                                              join sub in SubjectCatBusiness.All on sb.SubjectID equals sub.SubjectCatID
                                              orderby sb.IsCommenting, sub.OrderInSubject
                                              where lstClassID.Contains(sb.ClassID)
                                              && sub.IsActive == true
                                              && ((semesterID == GlobalConstants.SEMESTER_OF_YEAR_FIRST && sb.SectionPerWeekFirstSemester > 0)
                                              || (semesterID == GlobalConstants.SEMESTER_OF_YEAR_SECOND && sb.SectionPerWeekSecondSemester > 0))
                                              select new SubjectCatBO
                                              {
                                                  ClassID = sb.ClassID,
                                                  SubjectCatID = sb.SubjectID,
                                                  SubjectName = sub.SubjectName,
                                                  IsCommenting = sb.IsCommenting
                                              }
                                             ).ToList();
                        }

                        // list danh gia CLGD
                        List<SummedEvaluation> listSummedEvaluationRes = (from ev in SummedEvaluationBusiness.All
                                                                          where ev.LastDigitSchoolID == partitionId
                                                                           && ev.AcademicYearID == academicYearID
                                                                           && lstClassID.Contains(ev.ClassID)
                                                                           && ev.SemesterID == semesterID
                                                                           && pupilIDList.Contains(ev.PupilID)
                                                                          select ev).ToList();
                        //List Danh gia cua hoc sinh theo hoc ky
                        List<SummedEndingEvaluation> listSummedEndingEvaluationRes = (from sn in SummedEndingEvaluationBusiness.All
                                                                                      where sn.LastDigitSchoolID == partitionId
                                                                                      && sn.AcademicYearID == academicYearID
                                                                                      && lstClassID.Contains(sn.ClassID)
                                                                                      && (sn.SemesterID == semesterID || sn.SemesterID == GlobalConstants.SEMESTER_OF_YEAR_PRIMARY)
                                                                                      && pupilIDList.Contains(sn.PupilID)
                                                                                      select sn).ToList();
                        //List Danh sach khen thuong
                        List<RewardCommentFinal> listRewardCommentsFinalRes = (from rf in RewardCommentFinalBusiness.All
                                                                               where rf.LastDigitSchoolID == partitionIDReward
                                                                               && rf.AcademicYearID == academicYearID
                                                                               && lstClassID.Contains(rf.ClassID)
                                                                               && rf.SemesterID == semesterID
                                                                               && pupilIDList.Contains(rf.PupilID)
                                                                               select rf).ToList();

                        for (int i = 0; i < pupilOfClassList.Count; i++)
                        {
                            pupilOfClassBO = pupilOfClassList[i];
                            pupilID = pupilOfClassBO.PupilID;
                            classID = pupilOfClassBO.ClassID;
                            obj = new EvaluationCommentsBO();
                            obj.PupilID = pupilID;
                            obj.FullName = pupilOfClassBO.PupilFullName;
                            //lay thong tinh diem va danh gia mon hoc
                            List<SummedEvaluation> listSummedEvaluationByPupilID = listSummedEvaluationRes.Where(p => p.ClassID == classID && p.PupilID == pupilID && p.EvaluationCriteriaID != 0).ToList();
                            SummedEvaluation summedEvaluationObj = null;
                            SubjectCatBO subjectBOOBJ = null;
                            int? iscommeting = null;
                            int subjectID = 0;
                            string subjectName = string.Empty;
                            monthComments = new StringBuilder();
                            for (int j = 0; j < lsClassSubject.Count; j++)
                            {
                                subjectBOOBJ = lsClassSubject[j];
                                subjectID = subjectBOOBJ.SubjectCatID;
                                summedEvaluationObj = listSummedEvaluationByPupilID.Where(p => p.ClassID == classID && p.EvaluationCriteriaID == subjectID).FirstOrDefault();// 1 hoc sinh 1 hoc ki chi co duy nhat 1 dong
                                subjectName = subjectBOOBJ.SubjectName;
                                iscommeting = subjectBOOBJ.IsCommenting; //0: tinh diem, 1: nhan xet

                                // if (!string.IsNullOrEmpty(summedEvaluationObj.EndingEvaluation) || summedEvaluationObj.PeriodicEndingMark.HasValue)
                                // {
                                monthComments.Append(subjectName + ": ");
                                if (summedEvaluationObj != null)
                                {
                                    if (!string.IsNullOrEmpty(summedEvaluationObj.EndingEvaluation))
                                    {
                                        monthComments.Append(summedEvaluationObj.EndingEvaluation); // danh gia
                                    }

                                    if (iscommeting != GlobalConstants.SUBJECTCAT_ISCOMMENTING_MARK)
                                    {
                                        if (j != lsClassSubject.Count - 1)
                                        {
                                            monthComments.Append("; ");
                                        }
                                    }
                                    else
                                    {
                                        if (!string.IsNullOrEmpty(summedEvaluationObj.EndingEvaluation))
                                        {
                                            if (summedEvaluationObj.PeriodicEndingMark.HasValue)
                                            {
                                                monthComments.Append(", KTCK " + summedEvaluationObj.PeriodicEndingMark + "; ");
                                            }
                                        }
                                        else
                                        {
                                            if (summedEvaluationObj.PeriodicEndingMark.HasValue)
                                            {
                                                monthComments.Append(" KTCK " + summedEvaluationObj.PeriodicEndingMark + "; ");
                                            }
                                        }
                                    }
                                }
                                //  }
                            }
                            if (monthComments != null)
                            {
                                monthComments.Append("\r\n");
                            }

                            // lay thong tin danh gia NL,PC
                            listSummedEndingEvaluationByPupilID = listSummedEndingEvaluationRes.Where(p => p.ClassID == classID && p.PupilID == pupilID && p.SemesterID == semesterID).ToList();// max 2 record

                            monthComments.Append("Năng lực: ");
                            monthComments.Append(listSummedEndingEvaluationByPupilID.Where(p => p.ClassID == classID && p.EvaluationID == GlobalConstants.TAB_CAPACTIES_BOOKMARK_PRIMARY).Select(p => ("Đ".Equals(p.EndingEvaluation) ? "Đạt" : "Chưa đạt")).FirstOrDefault());
                            monthComments.Append("\r\n");

                            monthComments.Append("Phẩm chất: ");
                            monthComments.Append(listSummedEndingEvaluationByPupilID.Where(p => p.ClassID == classID && p.EvaluationID == GlobalConstants.TAB_QUALITIES_BOOKMARK_PRIMARY).Select(p => ("Đ".Equals(p.EndingEvaluation) ? "Đạt" : "Chưa đạt")).FirstOrDefault());
                            monthComments.Append("\r\n");

                            //lay thong tin khen thuong
                            string strRewardID = listRewardCommentsFinalRes.Where(p => p.ClassID == classID && p.PupilID == pupilID).Select(p => p.RewardID).FirstOrDefault(); // khen thuong
                            monthComments.Append("Khen thưởng: ");
                            if (!string.IsNullOrEmpty(strRewardID))
                            {
                                List<int> listInt = strRewardID.Split(new[] { "," }, StringSplitOptions.RemoveEmptyEntries).Select(p => int.Parse(p)).ToList();
                                string rewardMone = string.Empty;
                                for (int g = 0; g < listInt.Count; g++)
                                {
                                    rewardMone = rewardFinalList.Where(p => p.RewardFinalID == listInt[g]).Select(p => p.RewardMode).FirstOrDefault();
                                    if (!string.IsNullOrWhiteSpace(rewardMone))
                                    {
                                        monthComments.Append(rewardMone);
                                        if (g != listInt.Count - 1)
                                        {
                                            monthComments.Append(", ");
                                        }
                                    }
                                }
                            }
                            monthComments.Append("\r\n");

                            // Danh gia hoan thanh chuong trinh neu la hoc ki 2
                            if (semesterID == GlobalConstants.SEMESTER_OF_YEAR_SECOND)
                            {
                                monthComments.Append("Đánh giá: ");
                                string evaluationEnding = listSummedEndingEvaluationRes
                                    .Where(p => p.ClassID == classID && p.PupilID == pupilID && p.SemesterID == GlobalConstants.SEMESTER_OF_YEAR_PRIMARY && p.EvaluationID == 4)
                                    .Select(p => !string.IsNullOrEmpty(p.EvaluationReTraining) ? p.EvaluationReTraining : p.EndingEvaluation).FirstOrDefault();
                                if ("HT".Equals(evaluationEnding))
                                {
                                    monthComments.Append(string.Format(GlobalConstantsEdu.COURSE_COMPLETE, pupilOfClassBO.EducationLevelID));
                                }
                                else if ("CHT".Equals(evaluationEnding))
                                {
                                    monthComments.Append(string.Format(GlobalConstantsEdu.NOT_COURSE_COMPLETE, pupilOfClassBO.EducationLevelID));
                                }
                            }
                            if (monthComments != null)
                            {
                                obj.EvaluationMonth = monthComments.ToString();
                            }
                            listResult.Add(obj);
                        }
                        #endregion
                    }
                    else
                    {
                        #region Nhan xet cuoi ky
                        List<RewardCommentFinal> listRewardCommentsFinalRes = (from rf in RewardCommentFinalBusiness.All
                                                                               where rf.LastDigitSchoolID == partitionIDReward
                                                                               && rf.AcademicYearID == academicYearID
                                                                               && lstClassID.Contains(rf.ClassID)
                                                                               && rf.SemesterID == semesterID
                                                                               && pupilIDList.Contains(rf.PupilID)
                                                                               select rf).ToList();
                        for (int i = 0; i < pupilOfClassList.Count; i++)
                        {
                            monthComments = new StringBuilder();
                            pupilOfClassBO = pupilOfClassList[i];
                            pupilID = pupilOfClassBO.PupilID;
                            classID = pupilOfClassBO.ClassID;
                            obj = new EvaluationCommentsBO();
                            obj.PupilID = pupilID;
                            obj.FullName = pupilOfClassBO.PupilFullName;

                            // lay thong tin nhan xet
                            monthComments.Append(listRewardCommentsFinalRes.Where(p => p.ClassID == classID && p.PupilID == pupilID).Select(p => p.OutstandingAchievement).FirstOrDefault());

                            if (monthComments != null)
                            {
                                obj.EvaluationMonth = monthComments.ToString();
                            }
                            listResult.Add(obj);
                        }
                        #endregion
                    }
                }
                return listResult;
            }
            catch (Exception ex)
            {
                logger.Error(ex.Message, ex);
                return new List<EvaluationCommentsBO>();
            }
        }
        private List<EvaluationCommentsBO> getEvaluationCommnetsBySchoolOfPrimaryHistory(List<int> pupilIDList, int schoolID, int academicYearID, List<int> lstClassID, int semesterID, int monthID, int userAccountID, bool viewAll = true)
        {
            try
            {
                int TypeOfTeacher = 2;// GVCN
                int partitionId = UtilsBusiness.GetPartionId(schoolID);
                int partitionIDReward = UtilsBusiness.GetPartionId(schoolID, 20);
                //list hoc sinh trong lop
                List<PupilOfClassBO> pupilOfClassList = (from poc in PupilOfClassBusiness.AllNoTracking
                                                         join pp in PupilProfileBusiness.AllNoTracking.OrderBy(x => x.PupilProfileID) on poc.PupilID equals pp.PupilProfileID
                                                         where pupilIDList.Contains(poc.PupilID)
                                                         && poc.SchoolID == schoolID && poc.AcademicYearID == academicYearID
                                                         && lstClassID.Contains(poc.ClassID)
                                                         && poc.Status == GlobalConstants.PUPIL_STATUS_STUDYING
                                                         select new PupilOfClassBO()
                                                         {
                                                             PupilOfClassID = poc.PupilOfClassID,
                                                             SchoolID = poc.SchoolID,
                                                             AcademicYearID = poc.AcademicYearID,
                                                             ClassID = poc.ClassID,
                                                             PupilID = poc.PupilID,
                                                             PupilCode = pp.PupilCode,
                                                             PupilFullName = pp.FullName,
                                                             Name = pp.Name,
                                                             OrderInClass = poc.OrderInClass,
                                                             Status = poc.Status,
                                                             EndDate = poc.EndDate,
                                                             Birthday = pp.BirthDate,
                                                             BirthPlace = pp.BirthPlace,
                                                             ResidentalAddress = pp.TempResidentalAddress,
                                                             PermanentResidentalAddress = pp.PermanentResidentalAddress,
                                                             EducationLevelID = poc.ClassProfile.EducationLevelID,
                                                             EthnicName = pp.Ethnic.EthnicName,
                                                             Genre = pp.Genre,
                                                             FatherFullName = pp.FatherFullName,
                                                             FatherJobName = pp.FatherJob,
                                                             MotherFullName = pp.MotherFullName,
                                                             MotherJobName = pp.MotherJob,
                                                             SponserFullName = pp.SponsorFullName,
                                                             SponserJobName = pp.SponsorJob,
                                                             ClassName = poc.ClassProfile.DisplayName,
                                                             FatherMobile = pp.FatherMobile,
                                                             FatherEmail = pp.FatherEmail,
                                                             MotherMobile = pp.MotherMobile,
                                                             MotherEmail = pp.MotherEmail,
                                                             SponserMobile = pp.SponsorMobile,
                                                             SponserEmail = pp.SponsorEmail,
                                                             HomeTown = pp.HomeTown,
                                                             EnrolmentDate = pp.EnrolmentDate,
                                                             StorageNumber = pp.StorageNumber
                                                         }).OrderBy(c => c.OrderInClass).ThenBy(c => c.Name).ThenBy(c => c.PupilFullName).ToList();
                List<RewardFinal> rewardFinalList = RewardFinalBusiness.All.Where(p => p.SchoolID == schoolID).ToList();

                //list du lieu tra ve (1 hoc sinh thi co 1 dong nhan xet GLGD,PC,NL)
                List<EvaluationCommentsBO> listResult = new List<EvaluationCommentsBO>();
                EvaluationCommentsBO obj = null;
                PupilOfClassBO pupilOfClassBO = null;
                int pupilID = 0;
                int classID = 0;
                StringBuilder monthComments = null;
                string title = string.Empty;
                if (pupilOfClassList != null && pupilOfClassList.Count > 0)
                {
                    if (monthID < GlobalConstants.MonthID_11 && monthID > 0)
                    {
                        #region Danh gia theo thang
                        //list hoc sinh duoc danh gia GLGD nang luc và pham chat.
                        List<EvaluationCommentsHistory> listEvaluationComments = (from ev in EvaluationCommentsHistoryBusiness.All
                                                                                  where
                                                                                  ev.LastDigitSchoolID == partitionId
                                                                                  && ev.AcademicYearID == academicYearID
                                                                                   && lstClassID.Contains(ev.ClassID)
                                                                                   && ev.SemesterID == semesterID
                                                                                   && ev.TypeOfTeacher == TypeOfTeacher
                                                                                   && pupilIDList.Contains(ev.PupilID)
                                                                                  select ev).ToList();
                        for (int i = 0; i < pupilOfClassList.Count; i++)
                        {
                            pupilOfClassBO = pupilOfClassList[i];
                            pupilID = pupilOfClassBO.PupilID;
                            classID = pupilOfClassBO.ClassID;
                            obj = new EvaluationCommentsBO();
                            obj.PupilID = pupilID;
                            obj.FullName = pupilOfClassBO.PupilFullName;
                            monthComments = new StringBuilder();
                            //tao chuoi nhan xet (CLGD,PC,NL)
                            List<EvaluationCommentsHistory> listCommentsBypupilID = listEvaluationComments.Where(p => p.ClassID == classID && p.PupilID == pupilID).ToList();
                            EvaluationCommentsHistory evaluationCommentsObj = null;
                            for (int j = 0; j < listCommentsBypupilID.Count; j++)
                            {
                                if ((j + 1) == GlobalConstants.TAB_EDUCATION_BOOKMARK_PRIMARY)
                                {
                                    evaluationCommentsObj = listCommentsBypupilID.Where(p => p.ClassID == classID && p.EvaluationID == GlobalConstants.TAB_EDUCATION_BOOKMARK_PRIMARY).FirstOrDefault();
                                    if ((!string.IsNullOrEmpty(evaluationCommentsObj.CommentsM1M6) && (monthID == GlobalConstants.MonthID_1 || monthID == GlobalConstants.MonthID_6))
                                        || (!string.IsNullOrEmpty(evaluationCommentsObj.CommentsM2M7) && (monthID == GlobalConstants.MonthID_2 || monthID == GlobalConstants.MonthID_7))
                                        || (!string.IsNullOrEmpty(evaluationCommentsObj.CommentsM3M8) && (monthID == GlobalConstants.MonthID_3 || monthID == GlobalConstants.MonthID_8))
                                        || (!string.IsNullOrEmpty(evaluationCommentsObj.CommentsM4M9) && (monthID == GlobalConstants.MonthID_4 || monthID == GlobalConstants.MonthID_9))
                                        || (!string.IsNullOrEmpty(evaluationCommentsObj.CommentsM5M10) && (monthID == GlobalConstants.MonthID_5 || monthID == GlobalConstants.MonthID_10)))
                                    {
                                        monthComments.Append("A) Môn học và HĐGD: ");
                                        if (monthID == GlobalConstants.MonthID_1 || monthID == GlobalConstants.MonthID_6)
                                        {
                                            monthComments.Append(listCommentsBypupilID.Where(p => p.EvaluationID == GlobalConstants.TAB_EDUCATION_BOOKMARK_PRIMARY).Select(p => p.CommentsM1M6).FirstOrDefault());
                                        }
                                        else if (monthID == GlobalConstants.MonthID_2 || monthID == GlobalConstants.MonthID_7)
                                        {
                                            monthComments.Append(listCommentsBypupilID.Where(p => p.EvaluationID == GlobalConstants.TAB_EDUCATION_BOOKMARK_PRIMARY).Select(p => p.CommentsM2M7).FirstOrDefault());
                                        }
                                        else if (monthID == GlobalConstants.MonthID_3 || monthID == GlobalConstants.MonthID_8)
                                        {
                                            monthComments.Append(listCommentsBypupilID.Where(p => p.EvaluationID == GlobalConstants.TAB_EDUCATION_BOOKMARK_PRIMARY).Select(p => p.CommentsM3M8).FirstOrDefault());
                                        }
                                        else if (monthID == GlobalConstants.MonthID_4 || monthID == GlobalConstants.MonthID_9)
                                        {
                                            monthComments.Append(listCommentsBypupilID.Where(p => p.EvaluationID == GlobalConstants.TAB_EDUCATION_BOOKMARK_PRIMARY).Select(p => p.CommentsM4M9).FirstOrDefault());
                                        }
                                        else if (monthID == GlobalConstants.MonthID_5 || monthID == GlobalConstants.MonthID_10)
                                        {
                                            monthComments.Append(listCommentsBypupilID.Where(p => p.EvaluationID == GlobalConstants.TAB_EDUCATION_BOOKMARK_PRIMARY).Select(p => p.CommentsM5M10).FirstOrDefault());
                                        }

                                        if (monthComments != null)
                                        {
                                            monthComments.Append("\r\n");
                                        }
                                    }
                                }
                                else if ((j + 1) == GlobalConstants.TAB_CAPACTIES_BOOKMARK_PRIMARY)
                                {
                                    evaluationCommentsObj = listCommentsBypupilID.Where(p => p.ClassID == classID && p.EvaluationID == GlobalConstants.TAB_CAPACTIES_BOOKMARK_PRIMARY).FirstOrDefault();
                                    if ((!string.IsNullOrEmpty(evaluationCommentsObj.CommentsM1M6) && (monthID == GlobalConstants.MonthID_1 || monthID == GlobalConstants.MonthID_6))
                                       || (!string.IsNullOrEmpty(evaluationCommentsObj.CommentsM2M7) && (monthID == GlobalConstants.MonthID_2 || monthID == GlobalConstants.MonthID_7))
                                       || (!string.IsNullOrEmpty(evaluationCommentsObj.CommentsM3M8) && (monthID == GlobalConstants.MonthID_3 || monthID == GlobalConstants.MonthID_8))
                                       || (!string.IsNullOrEmpty(evaluationCommentsObj.CommentsM4M9) && (monthID == GlobalConstants.MonthID_4 || monthID == GlobalConstants.MonthID_9))
                                       || (!string.IsNullOrEmpty(evaluationCommentsObj.CommentsM5M10) && (monthID == GlobalConstants.MonthID_5 || monthID == GlobalConstants.MonthID_10)))
                                    {
                                        monthComments.Append("B) Năng lực: ");
                                        if (monthID == GlobalConstants.MonthID_1 || monthID == GlobalConstants.MonthID_6)
                                        {
                                            monthComments.Append(listCommentsBypupilID.Where(p => p.EvaluationID == GlobalConstants.TAB_CAPACTIES_BOOKMARK_PRIMARY).Select(p => p.CommentsM1M6).FirstOrDefault());
                                        }
                                        else if (monthID == GlobalConstants.MonthID_2 || monthID == GlobalConstants.MonthID_7)
                                        {
                                            monthComments.Append(listCommentsBypupilID.Where(p => p.EvaluationID == GlobalConstants.TAB_CAPACTIES_BOOKMARK_PRIMARY).Select(p => p.CommentsM2M7).FirstOrDefault());
                                        }
                                        else if (monthID == GlobalConstants.MonthID_3 || monthID == GlobalConstants.MonthID_8)
                                        {
                                            monthComments.Append(listCommentsBypupilID.Where(p => p.EvaluationID == GlobalConstants.TAB_CAPACTIES_BOOKMARK_PRIMARY).Select(p => p.CommentsM3M8).FirstOrDefault());
                                        }
                                        else if (monthID == GlobalConstants.MonthID_4 || monthID == GlobalConstants.MonthID_9)
                                        {
                                            monthComments.Append(listCommentsBypupilID.Where(p => p.EvaluationID == GlobalConstants.TAB_CAPACTIES_BOOKMARK_PRIMARY).Select(p => p.CommentsM4M9).FirstOrDefault());
                                        }
                                        else if (monthID == GlobalConstants.MonthID_5 || monthID == GlobalConstants.MonthID_10)
                                        {
                                            monthComments.Append(listCommentsBypupilID.Where(p => p.EvaluationID == GlobalConstants.TAB_CAPACTIES_BOOKMARK_PRIMARY).Select(p => p.CommentsM5M10).FirstOrDefault());
                                        }
                                        if (monthComments != null)
                                        {
                                            monthComments.Append("\r\n");
                                        }
                                    }
                                }
                                else if ((j + 1) == GlobalConstants.TAB_QUALITIES_BOOKMARK_PRIMARY)
                                {
                                    evaluationCommentsObj = listCommentsBypupilID.Where(p => p.ClassID == classID && p.EvaluationID == GlobalConstants.TAB_QUALITIES_BOOKMARK_PRIMARY).FirstOrDefault();
                                    if ((!string.IsNullOrEmpty(evaluationCommentsObj.CommentsM1M6) && (monthID == GlobalConstants.MonthID_1 || monthID == GlobalConstants.MonthID_6))
                                        || (!string.IsNullOrEmpty(evaluationCommentsObj.CommentsM2M7) && (monthID == GlobalConstants.MonthID_2 || monthID == GlobalConstants.MonthID_7))
                                        || (!string.IsNullOrEmpty(evaluationCommentsObj.CommentsM3M8) && (monthID == GlobalConstants.MonthID_3 || monthID == GlobalConstants.MonthID_8))
                                        || (!string.IsNullOrEmpty(evaluationCommentsObj.CommentsM4M9) && (monthID == GlobalConstants.MonthID_4 || monthID == GlobalConstants.MonthID_9))
                                        || (!string.IsNullOrEmpty(evaluationCommentsObj.CommentsM5M10) && (monthID == GlobalConstants.MonthID_5 || monthID == GlobalConstants.MonthID_10)))
                                    {
                                        monthComments.Append("C) Phẩm chất: ");
                                        if (monthID == GlobalConstants.MonthID_1 || monthID == GlobalConstants.MonthID_6)
                                        {
                                            monthComments.Append(listCommentsBypupilID.Where(p => p.EvaluationID == GlobalConstants.TAB_QUALITIES_BOOKMARK_PRIMARY).Select(p => p.CommentsM1M6).FirstOrDefault());
                                        }
                                        else if (monthID == GlobalConstants.MonthID_2 || monthID == GlobalConstants.MonthID_7)
                                        {
                                            monthComments.Append(listCommentsBypupilID.Where(p => p.EvaluationID == GlobalConstants.TAB_QUALITIES_BOOKMARK_PRIMARY).Select(p => p.CommentsM2M7).FirstOrDefault());
                                        }
                                        else if (monthID == GlobalConstants.MonthID_3 || monthID == GlobalConstants.MonthID_8)
                                        {
                                            monthComments.Append(listCommentsBypupilID.Where(p => p.EvaluationID == GlobalConstants.TAB_QUALITIES_BOOKMARK_PRIMARY).Select(p => p.CommentsM3M8).FirstOrDefault());
                                        }
                                        else if (monthID == GlobalConstants.MonthID_4 || monthID == GlobalConstants.MonthID_9)
                                        {
                                            monthComments.Append(listCommentsBypupilID.Where(p => p.EvaluationID == GlobalConstants.TAB_QUALITIES_BOOKMARK_PRIMARY).Select(p => p.CommentsM4M9).FirstOrDefault());
                                        }
                                        else if (monthID == GlobalConstants.MonthID_5 || monthID == GlobalConstants.MonthID_10)
                                        {
                                            monthComments.Append(listCommentsBypupilID.Where(p => p.EvaluationID == GlobalConstants.TAB_QUALITIES_BOOKMARK_PRIMARY).Select(p => p.CommentsM5M10).FirstOrDefault());
                                        }
                                        if (monthComments != null)
                                        {
                                            monthComments.Append("\r\n");
                                        }
                                    }
                                }
                            }
                            if (monthComments != null)
                            {
                                obj.EvaluationMonth = monthComments.ToString();
                            }
                            listResult.Add(obj);
                        }
                        #endregion
                    }
                    else if (monthID == GlobalConstants.MonthID_11)
                    {
                        #region Ket qua hoc ky
                        monthComments = new StringBuilder();
                        title = string.Empty;
                        //list mon hoc
                        List<SubjectCatBO> lsClassSubject = new List<SubjectCatBO>();
                        if (classID > 0)
                        {
                            lsClassSubject = (from sb in ClassSubjectBusiness.All
                                              join sub in SubjectCatBusiness.All on sb.SubjectID equals sub.SubjectCatID
                                              orderby sub.OrderInSubject, sub.DisplayName
                                              where lstClassID.Contains(sb.ClassID)
                                              && sub.IsActive == true
                                              select new SubjectCatBO
                                              {
                                                  SubjectCatID = sb.SubjectID,
                                                  ClassID = sb.ClassID,
                                                  SubjectName = sub.SubjectName,
                                                  IsCommenting = sb.IsCommenting,
                                                  IsSubjectVNEN = sb.IsSubjectVNEN
                                              }).ToList();
                        }

                        // list danh gia CLGD
                        List<SummedEvaluationHistory> listSummedEvaluationRes = (from ev in SummedEvaluationHistoryBusiness.All
                                                                                 where ev.LastDigitSchoolID == partitionId
                                                                                  && ev.AcademicYearID == academicYearID
                                                                                  && lstClassID.Contains(ev.ClassID)
                                                                                  && ev.SemesterID == semesterID
                                                                                  && pupilIDList.Contains(ev.PupilID)
                                                                                 select ev).ToList();
                        //List Danh gia cua hoc sinh theo hoc ky
                        List<SummedEndingEvaluation> listSummedEndingEvaluationRes = (from sn in SummedEndingEvaluationBusiness.All
                                                                                      where sn.LastDigitSchoolID == partitionId
                                                                                      && sn.AcademicYearID == academicYearID
                                                                                      && lstClassID.Contains(sn.ClassID)
                                                                                      && (sn.SemesterID == semesterID || sn.SemesterID == GlobalConstants.SEMESTER_OF_YEAR_PRIMARY)
                                                                                      && pupilIDList.Contains(sn.PupilID)
                                                                                      select sn).ToList();
                        //List Danh sach khen thuong
                        List<RewardCommentFinal> listRewardCommentsFinalRes = (from rf in RewardCommentFinalBusiness.All
                                                                               where rf.LastDigitSchoolID == partitionIDReward
                                                                               && rf.AcademicYearID == academicYearID
                                                                               && lstClassID.Contains(rf.ClassID)
                                                                               && rf.SemesterID == semesterID
                                                                               && pupilIDList.Contains(rf.PupilID)
                                                                               select rf).ToList();

                        for (int i = 0; i < pupilOfClassList.Count; i++)
                        {
                            pupilOfClassBO = pupilOfClassList[i];
                            pupilID = pupilOfClassBO.PupilID;
                            classID = pupilOfClassBO.ClassID;
                            obj = new EvaluationCommentsBO();
                            obj.PupilID = pupilID;
                            obj.FullName = pupilOfClassBO.PupilFullName;
                            //lay thong tinh diem va danh gia mon hoc
                            List<SummedEvaluationHistory> listSummedEvaluationByPupilID = listSummedEvaluationRes.Where(p => p.ClassID == classID && p.PupilID == pupilID && p.EvaluationCriteriaID != 0).ToList();
                            SummedEvaluationHistory summedEvaluationObj = null;
                            int? iscommeting = -1;
                            int subjectID = 0;
                            string subjectName = string.Empty;
                            monthComments = new StringBuilder();
                            for (int j = 0; j < listSummedEvaluationByPupilID.Count; j++)
                            {
                                summedEvaluationObj = listSummedEvaluationByPupilID[j];
                                subjectID = summedEvaluationObj.EvaluationCriteriaID;
                                subjectName = lsClassSubject.Where(p => p.ClassID == classID && p.SubjectCatID == subjectID).Select(p => p.SubjectName).FirstOrDefault();
                                iscommeting = lsClassSubject.Where(p => p.ClassID == classID && p.SubjectCatID == subjectID).Select(p => p.IsCommenting.HasValue ? p.IsCommenting.Value : -1).FirstOrDefault(); //0: tinh diem, 1: nhan xet

                                if (!string.IsNullOrEmpty(summedEvaluationObj.EndingEvaluation) || summedEvaluationObj.PeriodicEndingMark.HasValue)
                                {
                                    monthComments.Append(subjectName + ": ");
                                    if (!string.IsNullOrEmpty(summedEvaluationObj.EndingEvaluation))
                                    {
                                        monthComments.Append(summedEvaluationObj.EndingEvaluation); // danh gia
                                    }

                                    if (iscommeting != GlobalConstants.SUBJECTCAT_ISCOMMENTING_MARK)
                                    {
                                        monthComments.Append("; ");
                                    }

                                    if (iscommeting == GlobalConstants.SUBJECTCAT_ISCOMMENTING_MARK)
                                    {
                                        if (!string.IsNullOrEmpty(summedEvaluationObj.EndingEvaluation))
                                        {
                                            if (summedEvaluationObj.PeriodicEndingMark.HasValue)
                                            {
                                                monthComments.Append(", KTCK: " + summedEvaluationObj.PeriodicEndingMark + "; ");
                                            }
                                        }
                                        else
                                        {
                                            if (summedEvaluationObj.PeriodicEndingMark.HasValue)
                                            {
                                                monthComments.Append(" KTCK: " + summedEvaluationObj.PeriodicEndingMark + "; ");
                                            }
                                        }
                                    }
                                }
                            }
                            if (monthComments != null)
                            {
                                monthComments.Append("\r\n");
                            }

                            // lay thong tin danh gia NL,PC
                            List<SummedEndingEvaluation> listSummedEndingEvaluationByPupilID = listSummedEndingEvaluationRes.Where(p => p.ClassID == classID && p.PupilID == pupilID && p.SemesterID == semesterID).ToList();// max 2 record
                            string evalutionResult = string.Empty;
                            for (int k = 0; k < listSummedEndingEvaluationByPupilID.Count; k++)
                            {
                                if (listSummedEndingEvaluationByPupilID[k].EvaluationID == GlobalConstants.TAB_CAPACTIES_BOOKMARK_PRIMARY)
                                {
                                    evalutionResult = listSummedEndingEvaluationByPupilID.Where(p => p.EvaluationID == GlobalConstants.TAB_CAPACTIES_BOOKMARK_PRIMARY).Select(p => p.EndingEvaluation).FirstOrDefault();
                                    if (!string.IsNullOrWhiteSpace(evalutionResult))
                                    {
                                        monthComments.Append("Năng lực: ");
                                        monthComments.Append("Đ".Equals(evalutionResult) ? "Đạt" : "Chưa đạt");
                                        monthComments.Append("\r\n");
                                    }
                                }
                                else if (listSummedEndingEvaluationByPupilID[k].EvaluationID == GlobalConstants.TAB_QUALITIES_BOOKMARK_PRIMARY)
                                {
                                    evalutionResult = listSummedEndingEvaluationByPupilID.Where(p => p.ClassID == classID && p.EvaluationID == GlobalConstants.TAB_QUALITIES_BOOKMARK_PRIMARY).Select(p => p.EndingEvaluation).FirstOrDefault();
                                    if (!string.IsNullOrWhiteSpace(evalutionResult))
                                    {
                                        monthComments.Append("Phẩm chất: ");
                                        monthComments.Append("Đ".Equals(evalutionResult) ? "Đạt" : "Chưa đạt");
                                        monthComments.Append("\r\n");
                                    }
                                }
                            }

                            //lay thong tin khen thuong
                            string strRewardID = listRewardCommentsFinalRes.Where(p => p.ClassID == classID && p.PupilID == pupilID).Select(p => p.RewardID).FirstOrDefault(); // khen thuong
                            if (!string.IsNullOrEmpty(strRewardID))
                            {
                                List<int> listInt = strRewardID.Split(new[] { "," }, StringSplitOptions.RemoveEmptyEntries).Select(p => int.Parse(p)).ToList();
                                string rewardMone = string.Empty;
                                for (int g = 0; g < listInt.Count; g++)
                                {
                                    rewardMone = rewardFinalList.Where(p => p.RewardFinalID == listInt[g]).Select(p => p.RewardMode).FirstOrDefault();
                                    if (!string.IsNullOrWhiteSpace(rewardMone))
                                    {
                                        monthComments.Append(rewardMone);
                                        if (g == listInt.Count - 1)
                                        {
                                            monthComments.Append(", ");
                                        }
                                    }
                                }
                                if (listInt != null && listInt.Count > 0)
                                {
                                    monthComments.Append("\r\n");
                                }
                            }

                            // Danh gia hoan thanh chuong trinh neu la hoc ki 2
                            if (semesterID == GlobalConstants.SEMESTER_OF_YEAR_SECOND)
                            {
                                string evaluationEnding = string.Empty;
                                evaluationEnding = listSummedEndingEvaluationRes
                                    .Where(p => p.ClassID == classID && p.PupilID == pupilID && p.SemesterID == GlobalConstants.SEMESTER_OF_YEAR_PRIMARY && p.EvaluationID == 4)
                                    .Select(p => !string.IsNullOrEmpty(p.EvaluationReTraining) ? p.EvaluationReTraining : p.EndingEvaluation).FirstOrDefault();
                                monthComments.Append(evaluationEnding);
                            }

                            if (monthComments != null)
                            {
                                obj.EvaluationMonth = monthComments.ToString();
                            }
                            listResult.Add(obj);
                        }
                        #endregion
                    }
                    else
                    {
                        #region Nhan xet cuoi ky
                        List<RewardCommentFinal> listRewardCommentsFinalRes = (from rf in RewardCommentFinalBusiness.All
                                                                               where rf.LastDigitSchoolID == partitionIDReward
                                                                               && rf.AcademicYearID == academicYearID
                                                                               && lstClassID.Contains(rf.ClassID)
                                                                               && rf.SemesterID == semesterID
                                                                               && pupilIDList.Contains(rf.PupilID)
                                                                               select rf).ToList();
                        for (int i = 0; i < pupilOfClassList.Count; i++)
                        {
                            pupilOfClassBO = pupilOfClassList[i];
                            pupilID = pupilOfClassBO.PupilID;
                            classID = pupilOfClassBO.ClassID;
                            obj = new EvaluationCommentsBO();
                            obj.PupilID = pupilID;
                            obj.FullName = pupilOfClassBO.PupilFullName;

                            // lay thong tin nhan xet
                            monthComments.Append(listRewardCommentsFinalRes.Where(p => p.ClassID == classID && p.PupilID == pupilID).Select(p => p.OutstandingAchievement).FirstOrDefault());
                            if (monthComments != null)
                            {
                                obj.EvaluationMonth = monthComments.ToString();
                            }
                            listResult.Add(obj);
                        }
                        #endregion
                    }
                }
                return listResult;
            }
            catch (Exception ex)
            {
                logger.Error(ex.Message, ex);
                return new List<EvaluationCommentsBO>();
            }
        }
        #endregion
    }

    public class EvaluationCommentActionAuditBO
    {

        public int ObjectId { get; set; }
        private int Action; //1: Update, 2:Delete
        public Dictionary<string, string> OldObjectValue { get; set; }
        public Dictionary<string, string> NewObjectValue { get; set; }
        public string Type
        {
            get
            {
                return EvaluationID == 1 ? "Môn học & HĐGD" : (EvaluationID == 2 ? "Năng lực" : "Phẩm chất");
            }
        }
        public long EvaluationID { get; set; }
        public long EvaluationCriteriaID { get; set; }
        public string SubjectName { get; set; }
        public string EvaluationCriteriaName { get; set; }
        public string SubjectOrEvaluationCriteriaName
        {
            get
            {
                if (EvaluationID == 1)
                {
                    return SubjectName;
                }
                else
                {
                    return EvaluationCriteriaName;
                }
            }
        }
        public string PupilName { get; set; }
        public string PupilCode { get; set; }
        public string ClassName { get; set; }
        public int Month { get; set; }
        public int Semester { get; set; }
        public string AcademicYear { get; set; }

        public string StrOldObjectValue
        {
            get
            {
                string str = "{";
                var i = 0;
                var count = OldObjectValue.Count;
                foreach (KeyValuePair<string, string> entry in OldObjectValue)
                {
                    str = str + entry.Key + ": " + entry.Value;

                    if (++i < count)
                    {
                        str = str + "; ";
                    }
                }

                str = str + "}";
                return str;
            }
        }

        public string StrNewObjectValue
        {
            get
            {
                string str = "{";
                var i = 0;
                var count = NewObjectValue.Count;
                foreach (KeyValuePair<string, string> entry in NewObjectValue)
                {
                    str = str + entry.Key + ": " + entry.Value;

                    if (++i < count)
                    {
                        str = str + "; ";
                    }
                }

                str = str + "}";
                return str;
            }
        }

        public string UserDescription
        {
            get
            {
                string str = String.Empty;
                //cap nhat
                if (Action == EVALUATION_COMMENT_AUDIT_ACTION_UPDATE)
                {
                    str = String.Format("Cập nhật đánh giá {0} HS {1}, mã {2}, {3}/{4}/{5}/{6}/{7}. Giá trị cũ {8}. Giá trị mới {9}.",
                        new string[] { Type, PupilName, PupilCode, ClassName, SubjectOrEvaluationCriteriaName, (Month != 11 ? "Tháng " + Month.ToString() : "Cuối kỳ"), "HK " + Semester, AcademicYear, StrOldObjectValue, StrNewObjectValue });
                }
                //xoa
                else if (Action == EVALUATION_COMMENT_AUDIT_ACTION_DELETE)
                {
                    str = String.Format("Xóa nhận xét đánh giá {0} HS {1}, mã {2}, {3}/{4}/{5}/{6}/{7}. Giá trị cũ {8}.",
                      new string[] { Type, PupilName, PupilCode, ClassName, SubjectOrEvaluationCriteriaName, (Month != 11 ? "Tháng " + Month.ToString() : "Cuối kỳ"), "HK " + Semester, AcademicYear, StrOldObjectValue });
                }
                else if (Action == EVALUATION_COMMENT_AUDIT_ACTION_COPPY)
                {
                    str = String.Format("Sao chép nhận xét đánh giá {0} HS {1}, mã {2}, {3}/{4}/{5}/{6}/{7}. Giá trị cũ {8}.",
                      new string[] { Type, PupilName, PupilCode, ClassName, SubjectOrEvaluationCriteriaName, (Month != 11 ? "Tháng " + Month.ToString() : "Cuối kỳ"), "HK " + Semester, AcademicYear, StrOldObjectValue });
                }

                return str;
            }
        }

        public EvaluationCommentActionAuditBO(int action)
        {
            OldObjectValue = new Dictionary<string, string>();
            NewObjectValue = new Dictionary<string, string>();
            this.Action = action;
        }

        public const int EVALUATION_COMMENT_AUDIT_ACTION_UPDATE = 1;
        public const int EVALUATION_COMMENT_AUDIT_ACTION_DELETE = 2;
        public const int EVALUATION_COMMENT_AUDIT_ACTION_COPPY = 3;


    }
}
