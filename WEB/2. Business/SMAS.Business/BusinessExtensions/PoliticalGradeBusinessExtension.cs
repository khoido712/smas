﻿/**
 * The Viettel License
 *
 * Copyright 2012 Viettel Telecom. All rights reserved.
 * VIETTEL PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */

/** 
* @author  dungnt
* @version $Revision: $
*/

using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Linq.Expressions;
using System.Web;
using log4net;

using SMAS.DAL.Repository;
using SMAS.DAL.IRepository;
using SMAS.Business.IBusiness;
using SMAS.Models.Models;
using SMAS.Business.Common;

namespace SMAS.Business.Business
{
    /// <summary>
    /// Trình độ lý luận chính trị
    /// <author>dungnt</author>
    /// <date>05/09/2012</date>
    /// </summary>
    public partial class PoliticalGradeBusiness
    {
        #region insert
        /// <summary>
        /// Thêm mới Trình độ lý luận chính trị
        /// <author>dungnt</author>
        /// <modified>hieund-16/10/2012</modified>
        /// <date>05/09/2012</date>
        /// </summary>
        /// <param name="insertPoliticalGrade">Đối tượng Insert</param>
        /// <returns>Đối tượng sau khi được insert vào DB thành công</returns>
        public override PoliticalGrade Insert(PoliticalGrade insertPoliticalGrade)
        {
            ValidationMetadata.ValidateObject(insertPoliticalGrade);
            //resolution rong
            //Utils.ValidateRequire(insertPoliticalGrade.Resolution, "PoliticalGrade_Label_Resolution");
            //Utils.ValidateMaxLength(insertPoliticalGrade.Resolution, RESOLUTION_MAX_LENGTH, "PoliticalGrade_Label_Resolution");
            //Utils.ValidateMaxLength(insertPoliticalGrade.Description, DESCRIPTION_MAX_LENGTH, "PoliticalGrade_Column_Description");

            //kiem tra ton tai - theo cap - can lam lai
            this.CheckDuplicate(insertPoliticalGrade.Resolution, GlobalConstants.LIST_SCHEMA, "PoliticalGrade", "Resolution", true, 0, "PoliticalGrade_Label_Resolution");
            //this.CheckDuplicate(insertPoliticalGrade.SchoolID.ToString(), GlobalConstants.LIST_SCHEMA, "PoliticalGrade", "AppliedLevel", true, 0, "PoliticalGrade_Label_ShoolID");
            // this.CheckDuplicateCouple(insertPoliticalGrade.Resolution, insertPoliticalGrade.SchoolID.ToString(),
            //GlobalConstants.LIST_SCHEMA, "PoliticalGrade", "Resolution", "SchoolID", true, 0, "PoliticalGrade_Label_Resolution");
            //kiem tra range
            insertPoliticalGrade.IsActive = true;
            return base.Insert(insertPoliticalGrade);
        }
        #endregion

        #region update
        /// <summary>
        /// Cập nhật Trình độ lý luận chính trị
        /// <author>dungnt</author>
        /// <date>05/09/2012</date>
        /// </summary>
        /// <param name="updatePoliticalGrade">Đối tượng Update</param>
        /// <returns>Đối tượng sau khi được Update vào DB thành công</returns>
        public override PoliticalGrade Update(PoliticalGrade updatePoliticalGrade)
        {
            //check avai
            new PoliticalGradeBusiness(null).CheckAvailable(updatePoliticalGrade.PoliticalGradeID, "PoliticalGrade_Label_PoliticalGradeID");
            ValidationMetadata.ValidateObject(updatePoliticalGrade);
            //resolution rong
            //Utils.ValidateRequire(updatePoliticalGrade.Resolution, "PoliticalGrade_Label_Resolution");
            //Utils.ValidateMaxLength(updatePoliticalGrade.Resolution, RESOLUTION_MAX_LENGTH, "PoliticalGrade_Label_Resolution");
            //Utils.ValidateMaxLength(updatePoliticalGrade.Description, DESCRIPTION_MAX_LENGTH, "PoliticalGrade_Column_Description");
            //kiem tra ton tai - theo cap - can lam lai
            this.CheckDuplicate(updatePoliticalGrade.Resolution, GlobalConstants.LIST_SCHEMA, "PoliticalGrade", "Resolution", true, updatePoliticalGrade.PoliticalGradeID, "PoliticalGrade_Label_Resolution");
            //this.CheckDuplicateCouple(updatePoliticalGrade.Resolution, updatePoliticalGrade.SchoolID.ToString(),
            //    GlobalConstants.LIST_SCHEMA, "PoliticalGrade", "Resolution", "SchoolID", true, updatePoliticalGrade.PoliticalGradeID, "PoliticalGrade_Label_Resolution");
            // this.CheckDuplicate(updatePoliticalGrade.SchoolID.ToString(), GlobalConstants.LIST_SCHEMA, "PoliticalGrade", "AppliedLevel", true, updatePoliticalGrade.PoliticalGradeID, "PoliticalGrade_Label_ShoolID");
            //Loi pham quy che thi khong thuoc truong truyen vao
            //tu PoliticalGradeID ->vao csdl lay PoliticalGrade tuong ung roi 
            //so sanh voi updatePoliticalGrade.SchoolID
            //.................
            updatePoliticalGrade.IsActive = true;
            return base.Update(updatePoliticalGrade);
        }
        #endregion

        #region Delete
        /// <summary>
        /// Xóa Trình độ lý luận chính trị
        /// <author>dungnt</author>
        /// <date>05/09/2012</date>
        /// </summary>
        /// <param name="PoliticalGradeId">ID Trình độ lý luận chính trị</param>
        public void Delete(int PoliticalGradeId)
        {
            //check avai
            new PoliticalGradeBusiness(null).CheckAvailable(PoliticalGradeId, "PoliticalGrade_Label_PoliticalGradeID");
            //da su dung hay chua
            this.CheckConstraints(GlobalConstants.LIST_SCHEMA, "PoliticalGrade", PoliticalGradeId, "PoliticalGrade_Label_PoliticalGradeIDFailed");
            base.Delete(PoliticalGradeId, true);
        }
        #endregion

        #region Search
        /// <summary>
        /// Tìm kiếm Trình độ lý luận chính trị
        /// <author>dungnt</author>
        /// <date>05/09/2012</date>
        /// </summary>
        /// <param name="dic">tham so doi tuong tim kiem</param>
        /// <returns>Danh sách kết quả tìm kiếm</returns>
        public IQueryable<PoliticalGrade> Search(IDictionary<string, object> dic)
        {
            string resolution = Utils.GetString(dic, "Resolution");
            string description = Utils.GetString(dic, "Description");
            bool? isActive = Utils.GetIsActive(dic, "IsActive");
            IQueryable<PoliticalGrade> lsPoliticalGrade = this.PoliticalGradeRepository.All;
            if (isActive.HasValue)
                lsPoliticalGrade = lsPoliticalGrade.Where(em => (em.IsActive == isActive));

            if (resolution.Trim().Length != 0)
                lsPoliticalGrade = lsPoliticalGrade.Where(em => (em.Resolution.ToUpper().Contains(resolution.ToUpper())));

            if (description.Trim().Length != 0)
                lsPoliticalGrade = lsPoliticalGrade.Where(em => (em.Description.ToUpper().Contains(description.ToUpper())));

            return lsPoliticalGrade;
        }
        #endregion
    }
}