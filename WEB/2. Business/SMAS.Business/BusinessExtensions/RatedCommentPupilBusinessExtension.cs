﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using SMAS.Models.Models;
using SMAS.Business.Common;
using SMAS.Business.BusinessObject;
using System.Text;
using System.IO;
using SMAS.VTUtils.Excel.Export;
using System.Threading.Tasks;
using Oracle.DataAccess.Client;
using SMAS.VTUtils.Log;
using Newtonsoft.Json;

namespace SMAS.Business.Business
{
    public partial class RatedCommentPupilBusiness
    {
        #region RatedComment
        private string STR_SPACE = ";";
        private string SPACE = " ";
        private string STR_DOT = ": ";
        public IQueryable<RatedCommentPupil> Search(IDictionary<string, object> dic)
        {
            int SchoolID = Utils.GetInt(dic, "SchoolID");
            int AcademicYearID = Utils.GetInt(dic, "AcademicYearID");
            int ClassID = Utils.GetInt(dic, "ClassID");
            int SubjectID = Utils.GetInt(dic, "SubjectID");
            int SemesterID = Utils.GetInt(dic, "SemesterID");
            int PupilID = Utils.GetInt(dic, "PupilID");
            int EvaluationID = Utils.GetInt(dic, "TypeID");
            List<int> lstClassID = Utils.GetIntList(dic, "lstClassID");
            List<int> lstSubjectID = Utils.GetIntList(dic, "lstSubjectID");
            List<int> lstPupilID = Utils.GetIntList(dic, "lstPupilID");
            IQueryable<RatedCommentPupil> iqQuery = RatedCommentPupilBusiness.All;
            if (SchoolID != 0)
            {
                int PartionID = UtilsBusiness.GetPartionId(SchoolID);
                iqQuery = iqQuery.Where(p => p.SchoolID == SchoolID && p.LastDigitSchoolID == PartionID);
            }
            if (AcademicYearID != 0)
            {
                iqQuery = iqQuery.Where(p => p.AcademicYearID == AcademicYearID);
            }
            if (ClassID != 0)
            {
                iqQuery = iqQuery.Where(p => p.ClassID == ClassID);
            }
            if (SubjectID != 0)
            {
                iqQuery = iqQuery.Where(p => p.SubjectID == SubjectID);
            }
            if (SemesterID != 0)
            {
                iqQuery = iqQuery.Where(p => p.SemesterID == SemesterID);
            }
            if (PupilID != 0)
            {
                iqQuery = iqQuery.Where(p => p.PupilID == PupilID);
            }
            if (EvaluationID != 0)
            {
                iqQuery = iqQuery.Where(p => p.EvaluationID == EvaluationID);

                //chiendd1: 29/12/2016. Sua loi khong lay du lieu cu
                if (EvaluationID == GlobalConstants.TAB_CAPACTIES)
                {
                    iqQuery = iqQuery.Where(p => (p.SubjectID > 0 && p.SubjectID < 4));
                }
                else if (EvaluationID == GlobalConstants.TAB_QUALITIES)
                {
                    iqQuery = iqQuery.Where(p => (p.SubjectID > 3 && p.SubjectID < 8));
                }

            }
            if (lstClassID.Count > 0)
            {
                iqQuery = iqQuery.Where(p => lstClassID.Contains(p.ClassID));
            }
            if (lstSubjectID.Count > 0)
            {
                iqQuery = iqQuery.Where(p => lstSubjectID.Contains(p.SubjectID));
            }
            if (lstPupilID.Count > 0)
            {
                iqQuery = iqQuery.Where(p => lstPupilID.Contains(p.PupilID));
            }
            return iqQuery;
        }


        public IQueryable<VRatedCommentPupil> VSearch(IDictionary<string, object> dic)
        {
            int SchoolID = Utils.GetInt(dic, "SchoolID");
            int AcademicYearID = Utils.GetInt(dic, "AcademicYearID");
            int ClassID = Utils.GetInt(dic, "ClassID");
            int SubjectID = Utils.GetInt(dic, "SubjectID");
            int SemesterID = Utils.GetInt(dic, "SemesterID");
            int PupilID = Utils.GetInt(dic, "PupilID");
            int EvaluationID = Utils.GetInt(dic, "TypeID");
            List<int> lstClassID = Utils.GetIntList(dic, "lstClassID");
            List<int> lstSubjectID = Utils.GetIntList(dic, "lstSubjectID");
            List<int> lstPupilID = Utils.GetIntList(dic, "lstPupilID");
            IQueryable<VRatedCommentPupil> iqQuery = VRatedCommentPupilBusiness.All;
            if (SchoolID != 0)
            {
                int PartionID = UtilsBusiness.GetPartionId(SchoolID);
                iqQuery = iqQuery.Where(p => p.SchoolID == SchoolID && p.LastDigitSchoolID == PartionID);
            }
            if (AcademicYearID != 0)
            {
                iqQuery = iqQuery.Where(p => p.AcademicYearID == AcademicYearID);
            }
            if (ClassID != 0)
            {
                iqQuery = iqQuery.Where(p => p.ClassID == ClassID);
            }
            if (SubjectID != 0)
            {
                iqQuery = iqQuery.Where(p => p.SubjectID == SubjectID);
            }
            if (SemesterID != 0)
            {
                iqQuery = iqQuery.Where(p => p.SemesterID == SemesterID);
            }
            if (PupilID != 0)
            {
                iqQuery = iqQuery.Where(p => p.PupilID == PupilID);
            }
            if (EvaluationID != 0)
            {
                iqQuery = iqQuery.Where(p => p.EvaluationID == EvaluationID);

                //chiendd1: 29/12/2016. Sua loi khong lay du lieu cu
                if (EvaluationID == GlobalConstants.TAB_CAPACTIES)
                {
                    iqQuery = iqQuery.Where(p => (p.SubjectID > 0 && p.SubjectID < 4));
                }
                else if (EvaluationID == GlobalConstants.TAB_QUALITIES)
                {
                    iqQuery = iqQuery.Where(p => (p.SubjectID > 3 && p.SubjectID < 8));
                }

            }
            if (lstClassID.Count > 0)
            {
                iqQuery = iqQuery.Where(p => lstClassID.Contains(p.ClassID));
            }
            if (lstSubjectID.Count > 0)
            {
                iqQuery = iqQuery.Where(p => lstSubjectID.Contains(p.SubjectID));
            }
            if (lstPupilID.Count > 0)
            {
                iqQuery = iqQuery.Where(p => lstPupilID.Contains(p.PupilID));
            }
            return iqQuery;
        }


        public void InsertOrUpdate(List<RatedCommentPupilBO> lstRatedCommentPupilBO, IDictionary<string, object> dic, ref List<ActionAuditDataBO> lstActionAudit)
        {
            try
            {
                SetAutoDetectChangesEnabled(false);
                int SchoolID = Utils.GetInt(dic, "SchoolID");
                int AcademicYearID = Utils.GetInt(dic, "AcademicYearID");
                int ClassID = Utils.GetInt(dic, "ClassID");
                int SubjectID = Utils.GetInt(dic, "SubjectID");
                int SemesterID = Utils.GetInt(dic, "SemesterID");
                int PartionID = UtilsBusiness.GetPartionId(SchoolID);
                int TypeID = Utils.GetInt(dic, "TypeID");
                bool isImport = Utils.GetBool(dic, "IsImport");
                List<int> lstPupilID = lstRatedCommentPupilBO.Select(p => p.PupilID).Distinct().ToList();
                List<int> lstSubjectID = lstRatedCommentPupilBO.Select(p => p.SubjectID).Distinct().ToList();
                List<int> lstClassID = lstRatedCommentPupilBO.Select(p => p.ClassID).Distinct().ToList();
                List<RatedCommentPupilBO> lstRatedtmp = new List<RatedCommentPupilBO>();
                AcademicYear objAy = AcademicYearBusiness.Find(AcademicYearID);
                List<RatedCommentPupil> lstRatedCommentPupilDB = new List<RatedCommentPupil>();
                IQueryable<RatedCommentPupil> iqRatedCommentDB = (from r in RatedCommentPupilBusiness.All
                                                                  where r.SchoolID == SchoolID
                                                                  && r.AcademicYearID == AcademicYearID
                                                                  && r.LastDigitSchoolID == PartionID
                                                                  && r.SemesterID == SemesterID
                                                                  select r);
                if (TypeID == GlobalConstants.EVALUATION_SUBJECT_AND_EDUCTIONGROUP)
                {
                    iqRatedCommentDB = iqRatedCommentDB.Where(p => p.EvaluationID == GlobalConstants.EVALUATION_SUBJECT_AND_EDUCTIONGROUP);
                }
                else
                {
                    iqRatedCommentDB = iqRatedCommentDB.Where(p => p.EvaluationID == GlobalConstants.EVALUATION_CAPACITY || p.EvaluationID == GlobalConstants.EVALUATION_QUALITY);
                }
                if (lstPupilID.Count > 0)
                {
                    iqRatedCommentDB = iqRatedCommentDB.Where(p => lstPupilID.Contains(p.PupilID));
                }
                if (lstClassID.Count > 0)
                {
                    iqRatedCommentDB = iqRatedCommentDB.Where(p => lstClassID.Contains(p.ClassID));
                }
                if (TypeID == GlobalConstants.TYPE_EVALUATION_SUBJECTANDEDUCATIONGROUP && lstSubjectID.Count > 0)
                {
                    iqRatedCommentDB = iqRatedCommentDB.Where(p => lstSubjectID.Contains(p.SubjectID));
                }
                lstRatedCommentPupilDB = iqRatedCommentDB.ToList();
                List<RatedCommentPupil> lstInsert = new List<RatedCommentPupil>();
                RatedCommentPupilBO objRatedCommentPupilBO = null;
                RatedCommentPupil objDB = null;
                RatedCommentPupil objInsert = null;
                // lay danh sach khoa danh gia
                IDictionary<string, object> dicLockMark = new Dictionary<string, object>()
                {
                    {"SchoolID",SchoolID},
                    {"AcademicYearID",AcademicYearID},
                    {"lstClassID",lstClassID},
                    {"EvaluationID",TypeID}
                };
                string strTitleLog = string.Empty;
                string strLockMarkTitle = string.Empty;
                List<LockRatedCommentPupil> lstLockRated = LockRatedCommentPupilBusiness.Search(dicLockMark).ToList();
                List<LockRatedCommentPupil> lstLockRatedtmp = new List<LockRatedCommentPupil>();
                LockRatedCommentPupil objLockTitle = null;
                // Tạo data ghi log
                StringBuilder objectIDStr = new StringBuilder();
                StringBuilder descriptionStr = new StringBuilder();
                StringBuilder paramsStr = new StringBuilder();
                StringBuilder oldObjectStr = new StringBuilder();
                StringBuilder newObjectStr = new StringBuilder();
                StringBuilder userFuntionsStr = new StringBuilder();
                StringBuilder userActionsStr = new StringBuilder();
                StringBuilder userDescriptionsStr = new StringBuilder();
                bool isInsertLog = false;
                string comment = string.Empty;
                string oldComment = string.Empty;
                #region Mon hoc va HDGD
                if (TypeID == GlobalConstants.EVALUATION_SUBJECT_AND_EDUCTIONGROUP)//Mon hoc va HDGD
                {
                    for (int i = 0; i < lstRatedCommentPupilBO.Count; i++)
                    {
                        objRatedCommentPupilBO = lstRatedCommentPupilBO[i];
                        objLockTitle = lstLockRated.Where(p => p.SubjectID == objRatedCommentPupilBO.SubjectID && p.ClassID == objRatedCommentPupilBO.ClassID).FirstOrDefault();
                        strLockMarkTitle = objLockTitle != null ? objLockTitle.LockTitle : "";
                        objDB = lstRatedCommentPupilDB.Where(p => p.PupilID == objRatedCommentPupilBO.PupilID && p.SubjectID == objRatedCommentPupilBO.SubjectID && p.ClassID == objRatedCommentPupilBO.ClassID).FirstOrDefault();
                        isInsertLog = false;
                        //doi tuong ghi log
                        objectIDStr.Append(objRatedCommentPupilBO.PupilID);
                        if (isImport)
                        {
                            strTitleLog = "Import nhận xét đánh giá ";
                            userActionsStr.Append(SMAS.Business.Common.GlobalConstants.ACTION_IMPORT);
                        }
                        else
                        {
                            strTitleLog = "Cập nhật đánh giá ";
                            userActionsStr.Append(SMAS.Business.Common.GlobalConstants.ACTION_UPDATE);
                        }
                        descriptionStr.Append(strTitleLog + "học sinh");
                        paramsStr.Append("PupilID: " + objRatedCommentPupilBO.PupilID + "ClassID: " + objRatedCommentPupilBO.ClassID + "SubjectID: " + objRatedCommentPupilBO.SubjectID + "EvaluationID: " + objRatedCommentPupilBO.EvaluationID);
                        userFuntionsStr.Append("Sổ đánh giá HS");

                        userDescriptionsStr.Append(strTitleLog + (TypeID == GlobalConstants.TYPE_EVALUATION_SUBJECTANDEDUCATIONGROUP ? ("Môn học & HĐGD") : "Năng lực – Phẩm chất"));
                        userDescriptionsStr.Append(" HS " + objRatedCommentPupilBO.FullName).Append(", mã " + objRatedCommentPupilBO.PupilCode);
                        userDescriptionsStr.Append(", Lớp " + objRatedCommentPupilBO.ClassName + (TypeID == GlobalConstants.TYPE_EVALUATION_SUBJECTANDEDUCATIONGROUP ? "/" + objRatedCommentPupilBO.SubjectName : "") + "/Học kỳ " + SemesterID + "/" + objAy.DisplayTitle).Append(". ");
                        oldObjectStr.Append("Giá trị cũ {");
                        newObjectStr.Append("Giá trị mới {");
                        if (objDB == null)
                        {
                            objInsert = new RatedCommentPupil();
                            objInsert.SchoolID = objRatedCommentPupilBO.SchoolID;
                            objInsert.AcademicYearID = objRatedCommentPupilBO.AcademicYearID;
                            objInsert.ClassID = objRatedCommentPupilBO.ClassID;
                            objInsert.PupilID = objRatedCommentPupilBO.PupilID;
                            objInsert.SemesterID = objRatedCommentPupilBO.SemesterID;
                            objInsert.EvaluationID = objRatedCommentPupilBO.EvaluationID;
                            objInsert.RetestMark = objRatedCommentPupilBO.RetestMark;
                            objInsert.EvaluationRetraning = objRatedCommentPupilBO.EvaluationRetraning;
                            objInsert.FullNameComment = objRatedCommentPupilBO.FullNameComment;
                            objInsert.LastDigitSchoolID = UtilsBusiness.GetPartionId(SchoolID);
                            objInsert.LogChangeID = objRatedCommentPupilBO.LogChangeID;
                            objInsert.CreateTime = DateTime.Now;
                            objInsert.UpdateTime = DateTime.Now;
                            //add log
                            #region Mon hoc & HDGD
                            objInsert.SubjectID = objRatedCommentPupilBO.SubjectID;
                            if (objRatedCommentPupilBO.isCommenting == 0)
                            {
                                if (objRatedCommentPupilBO.PeriodicMiddleMark > 0 && !strLockMarkTitle.Contains("KTGK" + SemesterID))
                                {
                                    oldObjectStr.Append("Điểm KTGK: ").Append(STR_SPACE);
                                    newObjectStr.Append("Điểm KTGK: " + objRatedCommentPupilBO.PeriodicMiddleMark).Append(STR_SPACE);
                                    objInsert.PeriodicMiddleMark = objRatedCommentPupilBO.PeriodicMiddleMark;
                                    isInsertLog = true;
                                }
                                if (objRatedCommentPupilBO.MiddleEvaluation > 0 && !strLockMarkTitle.Contains("DGGK" + SemesterID))
                                {
                                    oldObjectStr.Append("ĐGGK: ").Append(STR_SPACE);
                                    newObjectStr.Append("ĐGGK: " + objRatedCommentPupilBO.MiddleEvaluationName).Append(STR_SPACE);
                                    objInsert.MiddleEvaluation = objRatedCommentPupilBO.MiddleEvaluation;
                                    isInsertLog = true;
                                }
                                if (objRatedCommentPupilBO.PeriodicEndingMark > 0 && !strLockMarkTitle.Contains("KTCK" + SemesterID))
                                {
                                    oldObjectStr.Append("Điểm KTCK: ").Append(STR_SPACE);
                                    newObjectStr.Append("Điểm KTCK: " + objRatedCommentPupilBO.PeriodicEndingMark).Append(STR_SPACE);
                                    objInsert.PeriodicEndingMark = objRatedCommentPupilBO.PeriodicEndingMark;
                                    isInsertLog = true;
                                }
                                if (objRatedCommentPupilBO.EndingEvaluation > 0 && !strLockMarkTitle.Contains("DGCK" + SemesterID))
                                {
                                    oldObjectStr.Append("ĐGCK: ").Append(STR_SPACE);
                                    newObjectStr.Append("ĐGCK: " + objRatedCommentPupilBO.EndingEvaluationName).Append(STR_SPACE);
                                    objInsert.EndingEvaluation = objRatedCommentPupilBO.EndingEvaluation;
                                    isInsertLog = true;
                                }
                            }
                            else
                            {
                                if (objRatedCommentPupilBO.MiddleEvaluation > 0 && !strLockMarkTitle.Contains("DGGK" + SemesterID))
                                {
                                    oldObjectStr.Append("ĐGGK: ").Append(STR_SPACE);
                                    newObjectStr.Append("ĐGGK: " + objRatedCommentPupilBO.MiddleEvaluationName).Append(STR_SPACE);
                                    objInsert.MiddleEvaluation = objRatedCommentPupilBO.MiddleEvaluation;
                                    isInsertLog = true;
                                }
                                if (objRatedCommentPupilBO.EndingEvaluation > 0 && !strLockMarkTitle.Contains("DGCK" + SemesterID))
                                {
                                    oldObjectStr.Append("ĐGCK: ").Append(STR_SPACE);
                                    newObjectStr.Append("ĐGCK: " + objRatedCommentPupilBO.EndingEvaluationName).Append(STR_SPACE);
                                    objInsert.EndingEvaluation = objRatedCommentPupilBO.EndingEvaluation;
                                    isInsertLog = true;
                                }
                            }
                            if (!string.IsNullOrEmpty(objRatedCommentPupilBO.Comment))
                            {
                                oldObjectStr.Append("NX: ").Append(STR_SPACE);
                                newObjectStr.Append("NX: " + objRatedCommentPupilBO.Comment).Append(STR_SPACE);
                                objInsert.Comment = objRatedCommentPupilBO.Comment;
                                isInsertLog = true;
                            }
                            #endregion
                            oldObjectStr.Append("}.");
                            newObjectStr.Append("}.");
                            if (isInsertLog)
                            {
                                lstInsert.Add(objInsert);
                            }
                        }
                        else
                        {
                            #region Mon hoc & HDGD
                            if (objRatedCommentPupilBO.isCommenting == 0)
                            {
                                if (objDB.PeriodicMiddleMark != objRatedCommentPupilBO.PeriodicMiddleMark && !strLockMarkTitle.Contains("KTGK" + SemesterID))
                                {
                                    oldObjectStr.Append("Điểm KTGK: ").Append(objDB.PeriodicMiddleMark).Append(STR_SPACE);
                                    newObjectStr.Append("Điểm KTGK: ").Append(objRatedCommentPupilBO.PeriodicMiddleMark).Append(STR_SPACE);
                                    objDB.PeriodicMiddleMark = objRatedCommentPupilBO.PeriodicMiddleMark;
                                    isInsertLog = true;
                                }
                                if (objDB.MiddleEvaluation != objRatedCommentPupilBO.MiddleEvaluation && !strLockMarkTitle.Contains("DGGK" + SemesterID))
                                {
                                    oldObjectStr.Append("ĐGGK: ").Append(objDB.MiddleEvaluation == 1 ? GlobalConstants.EVALUATION_GOOD_COMPLETE : objDB.MiddleEvaluation == 2 ? GlobalConstants.EVALUATION_COMPLETE : objDB.MiddleEvaluation == 3 ? GlobalConstants.EVALUATION_NOT_COMPLETE : "").Append(STR_SPACE);
                                    newObjectStr.Append("ĐGGK: ").Append(objRatedCommentPupilBO.MiddleEvaluationName).Append(STR_SPACE);
                                    objDB.MiddleEvaluation = objRatedCommentPupilBO.MiddleEvaluation;
                                    isInsertLog = true;
                                }
                                if (objDB.PeriodicEndingMark != objRatedCommentPupilBO.PeriodicEndingMark && !strLockMarkTitle.Contains("KTCK" + SemesterID))
                                {
                                    oldObjectStr.Append("Điểm KTCK: ").Append(objDB.PeriodicEndingMark).Append(STR_SPACE);
                                    newObjectStr.Append("Điểm KTCK: ").Append(objRatedCommentPupilBO.PeriodicEndingMark).Append(STR_SPACE);
                                    objDB.PeriodicEndingMark = objRatedCommentPupilBO.PeriodicEndingMark;
                                    isInsertLog = true;
                                }
                                if (objDB.EndingEvaluation != objRatedCommentPupilBO.EndingEvaluation && !strLockMarkTitle.Contains("DGCK" + SemesterID))
                                {
                                    oldObjectStr.Append("ĐGCK: ").Append(objDB.EndingEvaluation == 1 ? GlobalConstants.EVALUATION_GOOD_COMPLETE : objDB.EndingEvaluation == 2 ? GlobalConstants.EVALUATION_COMPLETE : objDB.EndingEvaluation == 3 ? GlobalConstants.EVALUATION_NOT_COMPLETE : "").Append(STR_SPACE);
                                    newObjectStr.Append("ĐGCK: ").Append(objRatedCommentPupilBO.EndingEvaluationName).Append(STR_SPACE);
                                    objDB.EndingEvaluation = objRatedCommentPupilBO.EndingEvaluation;
                                    isInsertLog = true;
                                }
                            }
                            else
                            {
                                if (objDB.MiddleEvaluation != objRatedCommentPupilBO.MiddleEvaluation && !strLockMarkTitle.Contains("DGGK" + SemesterID))
                                {
                                    oldObjectStr.Append("ĐGGK: ").Append(objDB.MiddleEvaluation == 1 ? GlobalConstants.EVALUATION_GOOD_COMPLETE : objDB.MiddleEvaluation == 2 ? GlobalConstants.EVALUATION_COMPLETE : objDB.MiddleEvaluation == 3 ? GlobalConstants.EVALUATION_NOT_COMPLETE : "").Append(STR_SPACE);
                                    newObjectStr.Append("ĐGGK: ").Append(objRatedCommentPupilBO.MiddleEvaluationName).Append(STR_SPACE);
                                    objDB.MiddleEvaluation = objRatedCommentPupilBO.MiddleEvaluation;
                                    isInsertLog = true;
                                }
                                if (objDB.EndingEvaluation != objRatedCommentPupilBO.EndingEvaluation && !strLockMarkTitle.Contains("DGCK" + SemesterID))
                                {
                                    oldObjectStr.Append("ĐGCK: ").Append(objDB.EndingEvaluation == 1 ? GlobalConstants.EVALUATION_GOOD_COMPLETE : objDB.EndingEvaluation == 2 ? GlobalConstants.EVALUATION_COMPLETE : objDB.EndingEvaluation == 3 ? GlobalConstants.EVALUATION_NOT_COMPLETE : "").Append(STR_SPACE);
                                    newObjectStr.Append("ĐGCK: ").Append(objRatedCommentPupilBO.EndingEvaluationName).Append(STR_SPACE);
                                    objDB.EndingEvaluation = objRatedCommentPupilBO.EndingEvaluation;
                                    isInsertLog = true;
                                }
                            }
                            if ((!string.IsNullOrEmpty(objDB.Comment) && string.IsNullOrEmpty(objRatedCommentPupilBO.Comment))
                                        || (string.IsNullOrEmpty(objDB.Comment) && !string.IsNullOrEmpty(objRatedCommentPupilBO.Comment))
                                        || (!string.IsNullOrEmpty(objDB.Comment) && !string.IsNullOrEmpty(objRatedCommentPupilBO.Comment)))
                            {
                                if (objDB.Comment != objRatedCommentPupilBO.Comment)
                                {
                                    oldObjectStr.Append("NX: ").Append(objDB.Comment).Append(STR_SPACE);
                                    newObjectStr.Append("NX: ").Append(objRatedCommentPupilBO.Comment).Append(STR_SPACE);
                                    objDB.Comment = objRatedCommentPupilBO != null && !string.IsNullOrEmpty(objRatedCommentPupilBO.Comment) ? objRatedCommentPupilBO.Comment.Trim() : "";
                                    isInsertLog = true;
                                }
                            }
                            #endregion
                            oldObjectStr.Append("}. ");
                            newObjectStr.Append("}");

                            if (isInsertLog)
                            {
                                objDB.UpdateTime = DateTime.Now;
                                objDB.LogChangeID = objRatedCommentPupilBO.LogChangeID;
                                RatedCommentPupilBusiness.Update(objDB);
                            }
                        }

                        if (isInsertLog)
                        {
                            ActionAuditDataBO objActionAudit = new ActionAuditDataBO();
                            objActionAudit.ObjID = objectIDStr;
                            objActionAudit.Parameter = paramsStr;
                            objActionAudit.Description = descriptionStr;
                            objActionAudit.UserAction = userActionsStr;
                            objActionAudit.UserFunction = userFuntionsStr;
                            objActionAudit.UserDescription = userDescriptionsStr.Append(oldObjectStr).Append(newObjectStr);
                            lstActionAudit.Add(objActionAudit);
                        }
                        objectIDStr = new StringBuilder();
                        paramsStr = new StringBuilder();
                        descriptionStr = new StringBuilder();
                        userActionsStr = new StringBuilder();
                        userFuntionsStr = new StringBuilder();
                        userDescriptionsStr = new StringBuilder();
                        oldObjectStr = new StringBuilder();
                        newObjectStr = new StringBuilder();
                    }
                }
                #endregion
                #region Nang luc pham chat
                else//Nang luc pham chat
                {
                    int pupilID = 0;
                    RatedCommentPupilBO objtmp = null;
                    for (int i = 0; i < lstPupilID.Count; i++)
                    {
                        pupilID = lstPupilID[i];
                        lstRatedtmp = lstRatedCommentPupilBO.Where(p => p.PupilID == pupilID).ToList();
                        objtmp = lstRatedtmp.FirstOrDefault();
                        lstLockRatedtmp = lstLockRated.Where(p => p.ClassID == objtmp.ClassID).ToList();
                        oldComment = string.Empty;
                        for (int j = 0; j < lstLockRatedtmp.Count; j++)
                        {
                            objLockTitle = lstLockRatedtmp[j];
                            strLockMarkTitle += objLockTitle != null ? objLockTitle.LockTitle + "," : string.Empty;
                        }
                        isInsertLog = false;
                        //doi tuong ghi log
                        objectIDStr.Append(objtmp.PupilID);
                        if (isImport)
                        {
                            strTitleLog = "Import nhận xét đánh giá ";
                            userActionsStr.Append(SMAS.Business.Common.GlobalConstants.ACTION_IMPORT);
                        }
                        else
                        {
                            strTitleLog = "Cập nhật đánh giá ";
                            userActionsStr.Append(SMAS.Business.Common.GlobalConstants.ACTION_UPDATE);
                        }
                        descriptionStr.Append(strTitleLog + "học sinh");
                        paramsStr.Append("PupilID: " + objtmp.PupilID + "ClassID: " + objtmp.ClassID + "EvaluationID: " + objtmp.EvaluationID);
                        userFuntionsStr.Append("Sổ đánh giá HS");
                        userDescriptionsStr.Append(strTitleLog + "Năng lực – Phẩm chất");
                        userDescriptionsStr.Append(" HS " + objtmp.FullName).Append(", mã " + objtmp.PupilCode);
                        userDescriptionsStr.Append(", Lớp " + objtmp.ClassName + "/Học kỳ " + SemesterID + "/" + objAy.DisplayTitle).Append(". ");
                        oldObjectStr.Append("Giá trị cũ {");
                        newObjectStr.Append("Giá trị mới {");

                        for (int j = 0; j < lstRatedtmp.Count; j++)
                        {
                            objtmp = lstRatedtmp[j];
                            objDB = lstRatedCommentPupilDB.Where(p => p.PupilID == objtmp.PupilID && p.ClassID == objtmp.ClassID && p.SubjectID == objtmp.SubjectID && p.EvaluationID == objtmp.EvaluationID).FirstOrDefault();
                            if (objDB == null)
                            {
                                objInsert = new RatedCommentPupil();
                                objInsert.SchoolID = objtmp.SchoolID;
                                objInsert.AcademicYearID = objtmp.AcademicYearID;
                                objInsert.ClassID = objtmp.ClassID;
                                objInsert.PupilID = objtmp.PupilID;
                                objInsert.SemesterID = objtmp.SemesterID;
                                objInsert.EvaluationID = objtmp.EvaluationID;
                                objInsert.RetestMark = objtmp.RetestMark;
                                objInsert.EvaluationRetraning = objtmp.EvaluationRetraning;
                                objInsert.FullNameComment = objtmp.FullNameComment;
                                objInsert.SubjectID = objtmp.SubjectID;
                                objInsert.LastDigitSchoolID = UtilsBusiness.GetPartionId(SchoolID);
                                objInsert.LogChangeID = objtmp.LogChangeID;
                                objInsert.CreateTime = DateTime.Now;
                                objInsert.UpdateTime = DateTime.Now;
                                if (objtmp.EvaluationID == 2)
                                {
                                    if (objtmp.CapacityMiddleEvaluation > 0 && !strLockMarkTitle.Contains("NLGK" + SemesterID))
                                    {
                                        oldObjectStr.Append("GK NL").Append(objtmp.SubjectID == GlobalConstants.EVALUATION_CRITERIA_ID1 ? "1" : objtmp.SubjectID == GlobalConstants.EVALUATION_CRITERIA_ID2 ? "2" : "3").Append(STR_DOT).Append(STR_SPACE);
                                        newObjectStr.Append("GK NL").Append(objtmp.SubjectID == GlobalConstants.EVALUATION_CRITERIA_ID1 ? "1" : objtmp.SubjectID == GlobalConstants.EVALUATION_CRITERIA_ID2 ? "2" : "3").Append(STR_DOT)
                                            .Append(objtmp.CapacityMiddleEvaluationName).Append(SPACE).Append(STR_SPACE);
                                        objInsert.CapacityMiddleEvaluation = objtmp.CapacityMiddleEvaluation;
                                        isInsertLog = true;
                                    }
                                    if (objtmp.CapacityEndingEvaluation > 0 && !strLockMarkTitle.Contains("NLCK" + SemesterID))
                                    {
                                        oldObjectStr.Append("CK NL").Append(objtmp.SubjectID == GlobalConstants.EVALUATION_CRITERIA_ID1 ? "1" : objtmp.SubjectID == GlobalConstants.EVALUATION_CRITERIA_ID2 ? "2" : "3").Append(STR_DOT).Append(STR_SPACE);
                                        newObjectStr.Append("CK NL").Append(objtmp.SubjectID == GlobalConstants.EVALUATION_CRITERIA_ID1 ? "1" : objtmp.SubjectID == GlobalConstants.EVALUATION_CRITERIA_ID2 ? "2" : "3").Append(STR_DOT)
                                            .Append(objtmp.CapacityEndingEvaluationName).Append(SPACE).Append(STR_SPACE);
                                        objInsert.CapacityEndingEvaluation = objtmp.CapacityEndingEvaluation;
                                        isInsertLog = true;
                                    }
                                }
                                else
                                {
                                    if (objtmp.QualityMiddleEvaluation > 0 && !strLockMarkTitle.Contains("PCGK" + SemesterID))
                                    {
                                        oldObjectStr.Append("GK PC").Append(objtmp.SubjectID == GlobalConstants.EVALUATION_CRITERIA_ID4 ? "1" : objtmp.SubjectID == GlobalConstants.EVALUATION_CRITERIA_ID5 ? "2" :
                                            objtmp.SubjectID == GlobalConstants.EVALUATION_CRITERIA_ID6 ? "3" : "4").Append(STR_DOT).Append(STR_SPACE);
                                        newObjectStr.Append("GK PC").Append(objtmp.SubjectID == GlobalConstants.EVALUATION_CRITERIA_ID4 ? "1" : objtmp.SubjectID == GlobalConstants.EVALUATION_CRITERIA_ID5 ? "2" :
                                            objtmp.SubjectID == GlobalConstants.EVALUATION_CRITERIA_ID6 ? "3" : "4").Append(STR_DOT).Append(objtmp.QualityMiddleEvaluationName).Append(SPACE).Append(STR_SPACE);
                                        objInsert.QualityMiddleEvaluation = objtmp.QualityMiddleEvaluation;
                                        isInsertLog = true;
                                    }
                                    if (objtmp.QualityEndingEvaluation > 0 && !strLockMarkTitle.Contains("PCCK" + SemesterID))
                                    {
                                        oldObjectStr.Append("CK PC").Append(objtmp.SubjectID == GlobalConstants.EVALUATION_CRITERIA_ID4 ? "1" : objtmp.SubjectID == GlobalConstants.EVALUATION_CRITERIA_ID5 ? "2" :
                                            objtmp.SubjectID == GlobalConstants.EVALUATION_CRITERIA_ID6 ? "3" : "4").Append(STR_DOT).Append(STR_SPACE);
                                        newObjectStr.Append("CK PC").Append(objtmp.SubjectID == GlobalConstants.EVALUATION_CRITERIA_ID4 ? "1" : objtmp.SubjectID == GlobalConstants.EVALUATION_CRITERIA_ID5 ? "2" :
                                            objtmp.SubjectID == GlobalConstants.EVALUATION_CRITERIA_ID6 ? "3" : "4").Append(STR_DOT).Append(objtmp.QualityEndingEvaluationName).Append(SPACE).Append(STR_SPACE);
                                        objInsert.QualityEndingEvaluation = objtmp.QualityEndingEvaluation;
                                        isInsertLog = true;
                                    }
                                }

                                if (!string.IsNullOrEmpty(objtmp.Comment))
                                {
                                    comment = objtmp.Comment;
                                    objInsert.Comment = objtmp.Comment;
                                    isInsertLog = true;
                                }
                                if (isInsertLog)
                                {
                                    lstInsert.Add(objInsert);
                                }
                            }
                            else
                            {
                                if (objtmp.EvaluationID == 2)
                                {
                                    if (objDB.CapacityMiddleEvaluation != objtmp.CapacityMiddleEvaluation && !strLockMarkTitle.Contains("NLGK" + SemesterID))
                                    {
                                        oldObjectStr.Append("GK NL").Append(objtmp.SubjectID == GlobalConstants.EVALUATION_CRITERIA_ID1 ? "1" : objtmp.SubjectID == GlobalConstants.EVALUATION_CRITERIA_ID2 ? "2" : "3")
                                            .Append(STR_DOT).Append(objDB.CapacityMiddleEvaluation == 1 ? GlobalConstants.EVALUATION_GOOD_COMPLETE : objDB.CapacityMiddleEvaluation == 2 ? GlobalConstants.EVALUATION_CAPQUA_COMPLETE : objDB.CapacityMiddleEvaluation == 3 ? GlobalConstants.EVALUATION_NOT_COMPLETE : "").Append(SPACE).Append(STR_SPACE);
                                        newObjectStr.Append("GK NL").Append(objtmp.SubjectID == GlobalConstants.EVALUATION_CRITERIA_ID1 ? "1" : objtmp.SubjectID == GlobalConstants.EVALUATION_CRITERIA_ID2 ? "2" : "3")
                                            .Append(STR_DOT).Append(objtmp.CapacityMiddleEvaluationName).Append(SPACE).Append(STR_SPACE);
                                        objDB.CapacityMiddleEvaluation = objtmp.CapacityMiddleEvaluation;
                                        isInsertLog = true;
                                    }
                                    if (objDB.CapacityEndingEvaluation != objtmp.CapacityEndingEvaluation && !strLockMarkTitle.Contains("NLCK" + SemesterID))
                                    {
                                        oldObjectStr.Append("CK NL").Append(objtmp.SubjectID == GlobalConstants.EVALUATION_CRITERIA_ID1 ? "1" : objtmp.SubjectID == GlobalConstants.EVALUATION_CRITERIA_ID2 ? "2" : "3")
                                            .Append(STR_DOT).Append(objDB.CapacityEndingEvaluation == 1 ? GlobalConstants.EVALUATION_GOOD_COMPLETE : objDB.CapacityEndingEvaluation == 2 ? GlobalConstants.EVALUATION_CAPQUA_COMPLETE : objDB.CapacityEndingEvaluation == 3 ? GlobalConstants.EVALUATION_NOT_COMPLETE : "").Append(SPACE).Append(STR_SPACE);
                                        newObjectStr.Append("CK NL").Append(objtmp.SubjectID == GlobalConstants.EVALUATION_CRITERIA_ID1 ? "1" : objtmp.SubjectID == GlobalConstants.EVALUATION_CRITERIA_ID2 ? "2" : "3")
                                            .Append(STR_DOT).Append(objtmp.CapacityEndingEvaluationName).Append(SPACE).Append(STR_SPACE);
                                        objDB.CapacityEndingEvaluation = objtmp.CapacityEndingEvaluation;
                                        isInsertLog = true;
                                    }
                                }
                                else
                                {
                                    if (objDB.QualityMiddleEvaluation != objtmp.QualityMiddleEvaluation && !strLockMarkTitle.Contains("PCGK" + SemesterID))
                                    {
                                        oldObjectStr.Append("GK PC").Append(objtmp.SubjectID == GlobalConstants.EVALUATION_CRITERIA_ID4 ? "1" : objtmp.SubjectID == GlobalConstants.EVALUATION_CRITERIA_ID5 ? "2" :
                                             objtmp.SubjectID == GlobalConstants.EVALUATION_CRITERIA_ID6 ? "3" : "4").Append(SPACE)
                                            .Append(STR_DOT).Append(objDB.QualityMiddleEvaluation == 1 ? GlobalConstants.EVALUATION_GOOD_COMPLETE : objDB.QualityMiddleEvaluation == 2 ? GlobalConstants.EVALUATION_CAPQUA_COMPLETE : objDB.QualityMiddleEvaluation == 3 ? GlobalConstants.EVALUATION_NOT_COMPLETE : "").Append(SPACE).Append(STR_SPACE);
                                        newObjectStr.Append("GK PC").Append(objtmp.SubjectID == GlobalConstants.EVALUATION_CRITERIA_ID4 ? "1" : objtmp.SubjectID == GlobalConstants.EVALUATION_CRITERIA_ID5 ? "2" :
                                             objtmp.SubjectID == GlobalConstants.EVALUATION_CRITERIA_ID6 ? "3" : "4").Append(STR_DOT).Append(objtmp.QualityMiddleEvaluationName).Append(SPACE).Append(STR_SPACE);
                                        objDB.QualityMiddleEvaluation = objtmp.QualityMiddleEvaluation;
                                        isInsertLog = true;
                                    }
                                    if (objDB.QualityEndingEvaluation != objtmp.QualityEndingEvaluation && !strLockMarkTitle.Contains("PCCK" + SemesterID))
                                    {
                                        oldObjectStr.Append("CK PC").Append(objtmp.SubjectID == GlobalConstants.EVALUATION_CRITERIA_ID4 ? "1" : objtmp.SubjectID == GlobalConstants.EVALUATION_CRITERIA_ID5 ? "2" :
                                             objtmp.SubjectID == GlobalConstants.EVALUATION_CRITERIA_ID6 ? "3" : "4").Append(SPACE)
                                            .Append(STR_DOT).Append(objDB.QualityEndingEvaluation == 1 ? GlobalConstants.EVALUATION_GOOD_COMPLETE : objDB.QualityEndingEvaluation == 2 ? GlobalConstants.EVALUATION_CAPQUA_COMPLETE : objDB.QualityEndingEvaluation == 3 ? GlobalConstants.EVALUATION_NOT_COMPLETE : "").Append(SPACE).Append(STR_SPACE);
                                        newObjectStr.Append("CK PC").Append(objtmp.SubjectID == GlobalConstants.EVALUATION_CRITERIA_ID4 ? "1" : objtmp.SubjectID == GlobalConstants.EVALUATION_CRITERIA_ID5 ? "2" :
                                             objtmp.SubjectID == GlobalConstants.EVALUATION_CRITERIA_ID6 ? "3" : "4").Append(STR_DOT).Append(objtmp.QualityEndingEvaluationName).Append(SPACE).Append(STR_SPACE);
                                        objDB.QualityEndingEvaluation = objtmp.QualityEndingEvaluation;
                                        isInsertLog = true;
                                    }
                                }
                                if ((!string.IsNullOrEmpty(objDB.Comment) && string.IsNullOrEmpty(objtmp.Comment))
                                        || (string.IsNullOrEmpty(objDB.Comment) && !string.IsNullOrEmpty(objtmp.Comment))
                                        || (!string.IsNullOrEmpty(objDB.Comment) && !string.IsNullOrEmpty(objtmp.Comment)))
                                {
                                    if (objDB.Comment != objtmp.Comment)
                                    {
                                        oldComment = objDB.Comment;
                                        comment = objtmp.Comment.Trim();
                                        objDB.Comment = objtmp.Comment.Trim();
                                        isInsertLog = true;
                                    }
                                }
                                if (isInsertLog)
                                {
                                    objDB.UpdateTime = DateTime.Now;
                                    objDB.LogChangeID = objtmp.LogChangeID;
                                    RatedCommentPupilBusiness.Update(objDB);
                                }
                            }
                        }
                        //xu lu comment
                        if (oldComment != comment)
                        {
                            oldObjectStr.Append("NX: ").Append(oldComment);
                            newObjectStr.Append("NX: ").Append(comment);
                        }

                        oldObjectStr.Append("}. ");
                        newObjectStr.Append("}");
                        if (isInsertLog)
                        {
                            ActionAuditDataBO objActionAudit = new ActionAuditDataBO();
                            objActionAudit.ObjID = objectIDStr;
                            objActionAudit.Parameter = paramsStr;
                            objActionAudit.Description = descriptionStr;
                            objActionAudit.UserAction = userActionsStr;
                            objActionAudit.UserFunction = userFuntionsStr;
                            objActionAudit.UserDescription = userDescriptionsStr.Append(oldObjectStr).Append(newObjectStr);
                            lstActionAudit.Add(objActionAudit);
                        }
                        objectIDStr = new StringBuilder();
                        paramsStr = new StringBuilder();
                        descriptionStr = new StringBuilder();
                        userActionsStr = new StringBuilder();
                        userFuntionsStr = new StringBuilder();
                        userDescriptionsStr = new StringBuilder();
                        oldObjectStr = new StringBuilder();
                        newObjectStr = new StringBuilder();
                    }
                }
                #endregion
                if (lstInsert.Count > 0)
                {
                    for (int i = 0; i < lstInsert.Count; i++)
                    {
                        RatedCommentPupilBusiness.Insert(lstInsert[i]);
                    }
                }
                RatedCommentPupilBusiness.Save();
            }
            catch (Exception ex)
            {
                
                LogExtensions.ErrorExt(logger, DateTime.Now, "InsertOrUpdate", "null", ex);
            }
            finally
            {
                SetAutoDetectChangesEnabled(true);
            }
        }
        public void DeleteCommentPupil(IDictionary<string, object> dic, ref List<ActionAuditDataBO> lstActionAudit, ref List<RESTORE_DATA_DETAIL> lstRestoreDetail)
        {
            try
            {
                SetAutoDetectChangesEnabled(false);

                int SchoolID = Utils.GetInt(dic, "SchoolID");
                int AcademicYearID = Utils.GetInt(dic, "AcademicYearID");
                int ClassID = Utils.GetInt(dic, "ClassID");
                int SubjectID = Utils.GetInt(dic, "SubjectID");
                int SemesterID = Utils.GetInt(dic, "SemesterID");
                int PartionID = UtilsBusiness.GetPartionId(SchoolID);
                int TypeID = Utils.GetInt(dic, "TypeID");
                List<int> lstPupilID = Utils.GetIntList(dic, "lstPupilID");
                int LogChangeID = Utils.GetInt(dic, "LogChangeID");
                int isCommenting = Utils.GetInt(dic, "isCommenting");
                RESTORE_DATA objRes = Utils.GetObject<RESTORE_DATA>(dic, "RESTORE_DATA");

                List<RatedCommentPupil> lstRatedCommentPupilDB = new List<RatedCommentPupil>();
                List<RatedCommentPupil> lstRatedtmp = new List<RatedCommentPupil>();
                List<PupilOfClassBO> lstPOC = PupilOfClassBusiness.GetPupilInClass(ClassID).Where(p => lstPupilID.Contains(p.PupilID)).Distinct().ToList();
                PupilOfClassBO objPOC = null;
                SubjectCat objSC = SubjectCatBusiness.Find(SubjectID);
                AcademicYear objAy = AcademicYearBusiness.Find(AcademicYearID);
                // lay danh sach khoa danh gia
                IDictionary<string, object> dicLockMark = new Dictionary<string, object>()
            {
                {"SchoolID",SchoolID},
                {"AcademicYearID",AcademicYearID},
                {"ClassID",ClassID},
                {"EvaluationID",TypeID}
            };
                string strLockMarkTitle = string.Empty;
                IQueryable<LockRatedCommentPupil> iqLockRated = LockRatedCommentPupilBusiness.Search(dicLockMark);
                LockRatedCommentPupil objLockTitle = null;
                if (TypeID == GlobalConstants.TYPE_EVALUATION_SUBJECTANDEDUCATIONGROUP)
                {
                    iqLockRated = iqLockRated.Where(p => p.SubjectID == SubjectID);
                    objLockTitle = iqLockRated.FirstOrDefault();
                    strLockMarkTitle = objLockTitle != null ? objLockTitle.LockTitle : "";
                }
                else
                {
                    List<LockRatedCommentPupil> lstLockRatedtmp = iqLockRated.ToList();
                    for (int i = 0; i < lstLockRatedtmp.Count; i++)
                    {
                        objLockTitle = lstLockRatedtmp[i];
                        strLockMarkTitle += objLockTitle != null ? objLockTitle.LockTitle + "," : string.Empty;
                    }
                }

                // Tạo data ghi log
                StringBuilder objectIDStr = new StringBuilder();
                StringBuilder descriptionStr = new StringBuilder();
                StringBuilder paramsStr = new StringBuilder();
                StringBuilder oldObjectStr = new StringBuilder();
                StringBuilder newObjectStr = new StringBuilder();
                StringBuilder userFuntionsStr = new StringBuilder();
                StringBuilder userActionsStr = new StringBuilder();
                StringBuilder userDescriptionsStr = new StringBuilder();


                StringBuilder restoreMsgDescription = new StringBuilder();
                bool isInsertLog = false;

                IQueryable<RatedCommentPupil> iqtmp = (from r in RatedCommentPupilBusiness.All
                                                       where r.SchoolID == SchoolID
                                                       && r.AcademicYearID == AcademicYearID
                                                       && r.LastDigitSchoolID == PartionID
                                                       && r.ClassID == ClassID
                                                       && r.SemesterID == SemesterID
                                                       && r.EvaluationID == TypeID
                                                       && lstPupilID.Contains(r.PupilID)
                                                       select r);
                if (TypeID == GlobalConstants.TYPE_EVALUATION_SUBJECTANDEDUCATIONGROUP)
                {
                    lstRatedCommentPupilDB = iqtmp.Where(p => p.SubjectID == SubjectID).ToList();
                }
                else
                {
                    lstRatedCommentPupilDB = iqtmp.ToList();
                }
                RatedCommentPupil objDB = null;
                RESTORE_DATA_DETAIL objRestoreDetail;
                if (TypeID == GlobalConstants.EVALUATION_SUBJECT_AND_EDUCTIONGROUP)
                {
                    for (int i = 0; i < lstRatedCommentPupilDB.Count; i++)
                    {
                        isInsertLog = false;
                        objDB = lstRatedCommentPupilDB[i];

                        #region Luu thong tin phuc vu phuc hoi du lieu
                        objRestoreDetail = new RESTORE_DATA_DETAIL
                        {
                            ACADEMIC_YEAR_ID = objRes.ACADEMIC_YEAR_ID,
                            CREATED_DATE = DateTime.Now,
                            END_DATE = null,
                            IS_VALIDATE = 1,
                            LAST_2DIGIT_NUMBER_SCHOOL = UtilsBusiness.GetPartionId(objRes.SCHOOL_ID),
                            ORDER_ID = 1,
                            RESTORE_DATA_ID = objRes.RESTORE_DATA_ID,
                            RESTORE_DATA_TYPE_ID = objRes.RESTORE_DATA_TYPE_ID,
                            SCHOOL_ID = objRes.SCHOOL_ID,
                            SQL_DELETE = "",
                            SQL_UNDO = JsonConvert.SerializeObject(objDB),
                            TABLE_NAME = RestoreDataConstant.TABLE_RATED_COMMENT_PUPIL
                        };
                        lstRestoreDetail.Add(objRestoreDetail);
                        #endregion

                        objDB.LogChangeID = LogChangeID;
                        objPOC = lstPOC.Where(p => p.PupilID == objDB.PupilID).FirstOrDefault();
                        //doi tuong ghi log
                        objectIDStr.Append(objDB.PupilID);
                        descriptionStr.Append("Xóa nhận xét đánh giá học sinh");
                        paramsStr.Append("PupilID: " + objDB.PupilID + "ClassID: " + objDB.ClassID + "SubjectID: " + objDB.SubjectID + "EvaluationID: " + objDB.EvaluationID);
                        userFuntionsStr.Append("Sổ đánh giá HS");
                        userActionsStr.Append(SMAS.Business.Common.GlobalConstants.ACTION_DELETE);
                        userDescriptionsStr.Append("Xóa nhận xét đánh giá " + ("môn " + objSC.SubjectName));
                        userDescriptionsStr.Append(" HS " + objPOC.PupilFullName).Append(", mã " + objPOC.PupilCode);
                        restoreMsgDescription.Append(string.Format(" HS: {0}, mã: {1},", objPOC.PupilFullName, objPOC.PupilCode));
                        userDescriptionsStr.Append(", Lớp " + objPOC.ClassName + "/Học kỳ " + SemesterID + "/" + objAy.DisplayTitle).Append(". ");
                        oldObjectStr.Append("Giá trị cũ {");
                        if (isCommenting == 0)
                        {
                            if ((objDB.PeriodicMiddleMark.HasValue && (objDB.PeriodicMiddleMark > 0 || !strLockMarkTitle.Contains("KTGK" + SemesterID))) && !strLockMarkTitle.Contains("KTGK" + SemesterID))
                            {
                                oldObjectStr.Append("Điểm KTGK: ").Append(objDB.PeriodicMiddleMark.ToString()).Append(STR_SPACE);
                                objDB.PeriodicMiddleMark = null;
                                isInsertLog = true;
                            }
                        }
                        else
                        {
                            if (!string.IsNullOrEmpty(objDB.PeriodicMiddleJudgement) && !strLockMarkTitle.Contains("KTGK" + SemesterID))
                            {
                                oldObjectStr.Append("Điểm KTGK: ").Append(objDB.PeriodicMiddleJudgement).Append(STR_SPACE);
                                objDB.PeriodicMiddleJudgement = null;
                                isInsertLog = true;
                            }
                        }

                        if (objDB.MiddleEvaluation.HasValue && objDB.MiddleEvaluation > 0 && !strLockMarkTitle.Contains("DGGK" + SemesterID))
                        {
                            oldObjectStr.Append("ĐGGK: ").Append(objDB.MiddleEvaluation == 1 ? GlobalConstants.EVALUATION_GOOD_COMPLETE : objDB.MiddleEvaluation == 2 ? GlobalConstants.EVALUATION_COMPLETE : GlobalConstants.EVALUATION_NOT_COMPLETE).Append(STR_SPACE);
                            objDB.MiddleEvaluation = null;
                            isInsertLog = true;
                        }

                        if (isCommenting == 0)
                        {
                            if ((objDB.PeriodicEndingMark.HasValue && (objDB.PeriodicEndingMark > 0 || !strLockMarkTitle.Contains("KTCK" + SemesterID))) && !strLockMarkTitle.Contains("KTCK" + SemesterID))
                            {
                                oldObjectStr.Append("Điểm KTCK: ").Append(objDB.PeriodicEndingMark.ToString()).Append(STR_SPACE);
                                objDB.PeriodicEndingMark = null;
                                isInsertLog = true;
                            }
                        }
                        else
                        {
                            if (!string.IsNullOrEmpty(objDB.PeriodicEndingJudgement) && !strLockMarkTitle.Contains("KTCK" + SemesterID))
                            {
                                oldObjectStr.Append("Điểm KTCK: ").Append(objDB.PeriodicEndingJudgement).Append(STR_SPACE);
                                objDB.PeriodicEndingJudgement = null;
                                isInsertLog = true;
                            }
                        }

                        if (objDB.EndingEvaluation.HasValue && objDB.EndingEvaluation > 0 && !strLockMarkTitle.Contains("DGCK" + SemesterID))
                        {
                            oldObjectStr.Append("ĐGCK: ").Append(objDB.EndingEvaluation == 1 ? GlobalConstants.EVALUATION_GOOD_COMPLETE : objDB.EndingEvaluation == 2 ? GlobalConstants.EVALUATION_COMPLETE : GlobalConstants.EVALUATION_NOT_COMPLETE).Append(STR_SPACE);
                            objDB.EndingEvaluation = null;
                            isInsertLog = true;
                        }

                        if (!string.IsNullOrEmpty(objDB.Comment))
                        {
                            oldObjectStr.Append("NX: ").Append(objDB.Comment);
                            objDB.Comment = "";
                            isInsertLog = true;
                        }

                        oldObjectStr.Append("}.");
                        if (isInsertLog)
                        {
                            objDB.UpdateTime = null;
                            RatedCommentPupilBusiness.Update(objDB);
                            ActionAuditDataBO objActionAudit = new ActionAuditDataBO();
                            objActionAudit.ObjID = objectIDStr;
                            objActionAudit.Parameter = paramsStr;
                            objActionAudit.Description = descriptionStr;
                            objActionAudit.UserAction = userActionsStr;
                            objActionAudit.UserFunction = userFuntionsStr;
                            objActionAudit.UserDescription = userDescriptionsStr.Append(oldObjectStr);
                            objActionAudit.RestoreMsgDescription = restoreMsgDescription.ToString();
                            lstActionAudit.Add(objActionAudit);
                        }
                        objectIDStr = new StringBuilder();
                        paramsStr = new StringBuilder();
                        descriptionStr = new StringBuilder();
                        userActionsStr = new StringBuilder();
                        userFuntionsStr = new StringBuilder();
                        userDescriptionsStr = new StringBuilder();
                        oldObjectStr = new StringBuilder();
                        restoreMsgDescription = new StringBuilder();
                    }
                }
                else
                {

                    int PupilID = 0;
                    RatedCommentPupil objtmp = null;
                    string oldComment = string.Empty;
                    for (int i = 0; i < lstPupilID.Count; i++)
                    {
                        PupilID = lstPupilID[i];

                        isInsertLog = false;
                        objPOC = lstPOC.Where(p => p.PupilID == PupilID).FirstOrDefault();
                        lstRatedtmp = lstRatedCommentPupilDB.Where(p => p.PupilID == PupilID).ToList();
                        objtmp = lstRatedtmp.FirstOrDefault();
                        if (objtmp == null)
                        {
                            continue;
                        }
                        objectIDStr.Append(objtmp.PupilID);
                        descriptionStr.Append("Xóa nhận xét đánh giá học sinh");
                        paramsStr.Append("PupilID: " + objtmp.PupilID + "ClassID: " + objtmp.ClassID + "SubjectID: " + objtmp.SubjectID + "EvaluationID: " + objtmp.EvaluationID);
                        userFuntionsStr.Append("Sổ đánh giá HS");
                        userActionsStr.Append(SMAS.Business.Common.GlobalConstants.ACTION_DELETE);
                        userDescriptionsStr.Append("Xóa nhận xét đánh giá Năng lực – Phẩm chất");
                        userDescriptionsStr.Append(" HS " + objPOC.PupilFullName).Append(", mã " + objPOC.PupilCode);
                        userDescriptionsStr.Append(", Lớp " + objPOC.ClassName + "/Học kỳ " + SemesterID + "/" + objAy.DisplayTitle).Append(". ");

                        restoreMsgDescription.Append(string.Format(" HS: {0}, mã: {1},", objPOC.PupilFullName, objPOC.PupilCode));
                        oldObjectStr.Append("Giá trị cũ {");
                        for (int j = 0; j < lstRatedtmp.Count; j++)
                        {
                            objDB = lstRatedtmp[j];
                            #region Luu thong tin phuc vu phuc hoi du lieu
                            objRestoreDetail = new RESTORE_DATA_DETAIL
                            {
                                ACADEMIC_YEAR_ID = objRes.ACADEMIC_YEAR_ID,
                                CREATED_DATE = DateTime.Now,
                                END_DATE = null,
                                IS_VALIDATE = 1,
                                LAST_2DIGIT_NUMBER_SCHOOL = UtilsBusiness.GetPartionId(objRes.SCHOOL_ID),
                                ORDER_ID = 1,
                                RESTORE_DATA_ID = objRes.RESTORE_DATA_ID,
                                RESTORE_DATA_TYPE_ID = objRes.RESTORE_DATA_TYPE_ID,
                                SCHOOL_ID = objRes.SCHOOL_ID,
                                SQL_DELETE = "",
                                SQL_UNDO = JsonConvert.SerializeObject(objDB),
                                TABLE_NAME = RestoreDataConstant.TABLE_RATED_COMMENT_PUPIL
                            };
                            lstRestoreDetail.Add(objRestoreDetail);
                            #endregion

                            if (objDB.EvaluationID == GlobalConstants.EVALUATION_CAPACITY)
                            {
                                if (objDB.CapacityMiddleEvaluation.HasValue && objDB.CapacityMiddleEvaluation > 0 && !strLockMarkTitle.Contains("NLGK" + SemesterID))
                                {
                                    oldObjectStr.Append("GK NL").Append(objDB.SubjectID == GlobalConstants.EVALUATION_CRITERIA_ID1 ? "1" : objDB.SubjectID == GlobalConstants.EVALUATION_CRITERIA_ID2 ? "2" : "3")
                                                .Append(STR_DOT).Append(objDB.CapacityMiddleEvaluation == 1 ? GlobalConstants.EVALUATION_GOOD_COMPLETE : objDB.CapacityMiddleEvaluation == 2 ? GlobalConstants.EVALUATION_CAPQUA_COMPLETE : objDB.CapacityMiddleEvaluation == 3 ? GlobalConstants.EVALUATION_NOT_COMPLETE : "").Append(SPACE).Append(STR_SPACE);
                                    objDB.CapacityMiddleEvaluation = null;
                                    isInsertLog = true;
                                }
                                if (objDB.CapacityEndingEvaluation.HasValue && objDB.CapacityEndingEvaluation > 0 && !strLockMarkTitle.Contains("NLCK" + SemesterID))
                                {
                                    oldObjectStr.Append("CK NL").Append(objDB.SubjectID == GlobalConstants.EVALUATION_CRITERIA_ID1 ? "1" : objDB.SubjectID == GlobalConstants.EVALUATION_CRITERIA_ID2 ? "2" : "3")
                                                .Append(STR_DOT).Append(objDB.CapacityEndingEvaluation == 1 ? GlobalConstants.EVALUATION_GOOD_COMPLETE : objDB.CapacityEndingEvaluation == 2 ? GlobalConstants.EVALUATION_CAPQUA_COMPLETE : objDB.CapacityEndingEvaluation == 3 ? GlobalConstants.EVALUATION_NOT_COMPLETE : "").Append(SPACE).Append(STR_SPACE);
                                    objDB.CapacityEndingEvaluation = null;
                                    isInsertLog = true;
                                }
                            }
                            else
                            {
                                if (objDB.QualityMiddleEvaluation.HasValue && objDB.QualityMiddleEvaluation > 0 && !strLockMarkTitle.Contains("PCGK" + SemesterID))
                                {
                                    oldObjectStr.Append("GK PC").Append(objDB.SubjectID == GlobalConstants.EVALUATION_CRITERIA_ID4 ? "1" : objDB.SubjectID == GlobalConstants.EVALUATION_CRITERIA_ID5
                                        ? "2" : objDB.SubjectID == GlobalConstants.EVALUATION_CRITERIA_ID6 ? "3" : "4")
                                                .Append(STR_DOT).Append(objDB.QualityMiddleEvaluation == 1 ? GlobalConstants.EVALUATION_GOOD_COMPLETE : objDB.QualityMiddleEvaluation == 2 ? GlobalConstants.EVALUATION_CAPQUA_COMPLETE : objDB.QualityMiddleEvaluation == 3 ? GlobalConstants.EVALUATION_NOT_COMPLETE : "").Append(SPACE).Append(STR_SPACE);
                                    objDB.QualityMiddleEvaluation = null;
                                    isInsertLog = true;
                                }
                                if (objDB.QualityEndingEvaluation.HasValue && objDB.QualityEndingEvaluation > 0 && !strLockMarkTitle.Contains("PCCK" + SemesterID))
                                {
                                    oldObjectStr.Append("CK PC").Append(objDB.SubjectID == GlobalConstants.EVALUATION_CRITERIA_ID4 ? "1" : objDB.SubjectID == GlobalConstants.EVALUATION_CRITERIA_ID5
                                        ? "2" : objDB.SubjectID == GlobalConstants.EVALUATION_CRITERIA_ID6 ? "3" : "4")
                                                .Append(STR_DOT).Append(objDB.QualityEndingEvaluation == 1 ? GlobalConstants.EVALUATION_GOOD_COMPLETE : objDB.QualityEndingEvaluation == 2 ? GlobalConstants.EVALUATION_CAPQUA_COMPLETE : objDB.QualityEndingEvaluation == 3 ? GlobalConstants.EVALUATION_NOT_COMPLETE : "").Append(SPACE).Append(STR_SPACE);
                                    objDB.QualityEndingEvaluation = null;
                                    isInsertLog = true;
                                }
                            }

                            if (!string.IsNullOrEmpty(objDB.Comment))
                            {
                                oldComment = objDB.Comment;
                                objDB.Comment = "";
                                isInsertLog = true;
                            }
                        }
                        if (!string.IsNullOrEmpty(oldComment))
                        {
                            oldObjectStr.Append("NX: ").Append(oldComment);
                        }
                        oldObjectStr.Append("}.");
                        if (isInsertLog)
                        {
                            objDB.UpdateTime = null;
                            RatedCommentPupilBusiness.Update(objDB);
                            ActionAuditDataBO objActionAudit = new ActionAuditDataBO();
                            objActionAudit.ObjID = objectIDStr;
                            objActionAudit.Parameter = paramsStr;
                            objActionAudit.Description = descriptionStr;
                            objActionAudit.UserAction = userActionsStr;
                            objActionAudit.UserFunction = userFuntionsStr;
                            objActionAudit.UserDescription = userDescriptionsStr.Append(oldObjectStr);
                            objActionAudit.RestoreMsgDescription = restoreMsgDescription.ToString();
                            lstActionAudit.Add(objActionAudit);
                        }
                        objectIDStr = new StringBuilder();
                        paramsStr = new StringBuilder();
                        descriptionStr = new StringBuilder();
                        userActionsStr = new StringBuilder();
                        userFuntionsStr = new StringBuilder();
                        userDescriptionsStr = new StringBuilder();
                        oldObjectStr = new StringBuilder();
                        restoreMsgDescription = new StringBuilder();
                    }
                }
                RatedCommentPupilBusiness.Save();
            }
            catch (Exception ex)
            {
                logger.Error(ex.Message, ex);
            }
            finally
            {
                SetAutoDetectChangesEnabled(true);
            }
        }
        public ProcessedReport GetProcessReportRepord(IDictionary<string, object> dic, bool isZip = false)
        {
            string reportCode = SystemParamsInFile.REPORT_SUBJECT_AND_EDUCATION_GROUP;
            if (isZip)
            {
                reportCode = SystemParamsInFile.REPORT_SUBJECT_AND_EDUCATION_GROUP_FILE_ZIP;
            }
            string inputParameterHashKey = ReportUtils.GetHashKey(dic);
            return ProcessedReportBusiness.GetProcessedReport(reportCode, inputParameterHashKey);
        }
        public ProcessedReport GetProcessReportRepordCapQua(IDictionary<string, object> dic)
        {
            string reportCode = SystemParamsInFile.REPORT_CAPACITY_AND_QUALITY;
            string inputParameterHashKey = ReportUtils.GetHashKey(dic);
            return ProcessedReportBusiness.GetProcessedReport(reportCode, inputParameterHashKey);
        }
        public ProcessedReport InsertProcessReport(IDictionary<string, object> dic, Stream data, bool isZip = false)
        {
            string reportCode = isZip ? SystemParamsInFile.REPORT_SUBJECT_AND_EDUCATION_GROUP_FILE_ZIP : SystemParamsInFile.REPORT_SUBJECT_AND_EDUCATION_GROUP;
            ReportDefinition reportDef = ReportDefinitionBusiness.GetByCode(reportCode);
            ProcessedReport pr = new ProcessedReport();
            pr.ReportCode = reportCode;
            pr.ProcessedDate = DateTime.Now;
            pr.InputParameterHashKey = ReportUtils.GetHashKey(dic);
            pr.ReportData = isZip ? ((MemoryStream)data).ToArray() : ReportUtils.Compress(data);

            int SemesterTypeID = Utils.GetInt(dic, "SemesterTypeID");
            int SubjectID = Utils.GetInt(dic, "SubjectID");
            string SubjectName = isZip ? "" : Utils.StripVNSignAndSpace(SubjectCatBusiness.Find(SubjectID).DisplayName);
            string SemesterName = SemesterTypeID == SystemParamsInFile.GENERAL_SUMMED_EVALUATION_GKI ? "GiuaHKI"
                                 : SemesterTypeID == SystemParamsInFile.GENERAL_SUMMED_EVALUATION_GKII ? "GiuaHKII"
                                 : SemesterTypeID == SystemParamsInFile.GENERAL_SUMMED_EVALUATION_CKI ? "CuoiHKI"
                                 : "CuoiHKII";

            //Tạo tên file
            string outputNamePattern = reportDef.OutputNamePattern;
            outputNamePattern = outputNamePattern.Replace("[SemesterType]", SemesterName).Replace("[SubjectName]", SubjectName);
            pr.ReportName = ReportUtils.RemoveSpecialCharacters(outputNamePattern) + "." + reportDef.OutputFormat;
            ProcessedReportParameterRepository.Insert(dic, pr);
            ProcessedReportRepository.Insert(pr);
            ProcessedReportRepository.Save();
            return pr;
        }

        public Stream CreateSubjectAndEducationGroupReport(IDictionary<string, object> dic)
        {
            string reportCode = SystemParamsInFile.REPORT_SUBJECT_AND_EDUCATION_GROUP;
            ReportDefinition reportDef = ReportDefinitionBusiness.GetByCode(reportCode);
            string templatePath = ReportUtils.GetTemplatePath(reportDef);
            IVTWorkbook oBook = VTExport.OpenWorkbook(templatePath);

            int AppliedLevel = Utils.GetInt(dic, "AppliedLevel");
            int SchoolID = Utils.GetInt(dic, "SchoolID");
            int AcademicYearID = Utils.GetInt(dic, "AcademicYearID");
            int SemesterTypeID = Utils.GetInt(dic, "SemesterTypeID");
            int SubjectID = Utils.GetInt(dic, "SubjectID");
            int FemaleType = Utils.GetInt(dic, "FemaleType");
            int EthnicType = Utils.GetInt(dic, "EthnicType");
            int FemaleEthnicType = Utils.GetInt(dic, "FemaleEthnicType");
            int AppliedLevelID = Utils.GetInt(dic, "AppliedLevelID");
            int CountEducationLevel = Utils.GetInt(dic, "CountEducationLevel");

            int TypeExcelPDF = Utils.GetInt(dic, "TypeExcelPDF"); // Xác định xuất Excel hay PDf. Fix lỗi xuất PDF ZIP file không tính %
            bool IsZip = SubjectID > 0 ? false : true;

            SchoolProfile objSP = SchoolProfileBusiness.Find(SchoolID);
            AcademicYear objAy = AcademicYearBusiness.Find(AcademicYearID);

            int SemesterID = 0;
            if (SemesterTypeID == SystemParamsInFile.GENERAL_SUMMED_EVALUATION_GKI || SemesterTypeID == SystemParamsInFile.GENERAL_SUMMED_EVALUATION_CKI)
            {
                SemesterID = GlobalConstants.SEMESTER_OF_YEAR_FIRST;
            }
            else
            {
                SemesterID = GlobalConstants.SEMESTER_OF_YEAR_SECOND;
            }

            IDictionary<string, object> dicSS = new Dictionary<string, object>()
            {
                {"SchoolID", SchoolID},
                {"AcademicYearID", AcademicYearID},
                {"AppliedLevel", AppliedLevel}
            };
            List<SchoolSubjectBO> lstSchoolSubject = SchoolSubjectBusiness.Search(dicSS)
                                                          .Select(p => new SchoolSubjectBO
                                                          {
                                                              SubjectID = p.SubjectID,
                                                              SubjectName = p.SubjectCat.SubjectName,
                                                              OrderInSubject = p.SubjectCat.OrderInSubject
                                                          }).Distinct().OrderBy(p => p.OrderInSubject).ThenBy(p => p.SubjectName).ToList();

            if (SubjectID > 0)
            {
                lstSchoolSubject = lstSchoolSubject.Where(x => x.SubjectID == SubjectID).ToList();
            }
            List<int> lstSubjectID = lstSchoolSubject.Select(x => x.SubjectID).ToList();

            // lay danh sach cac lop co hoc mon duoc chon
            List<ClassSubjectBO> lstCS = new List<ClassSubjectBO>();
            IDictionary<string, object> dicSearchClass = new Dictionary<string, object>()
            {
                {"SchoolID",SchoolID},
                {"AcademicYearID",AcademicYearID}
            };
            lstCS = ClassSubjectBusiness.Search(dicSearchClass).Where(x => lstSubjectID.Contains(x.SubjectID))
                    .Select(p => new ClassSubjectBO
                    {
                        ClassID = p.ClassID,
                        ClassName = p.ClassProfile.DisplayName,
                        SubjectID = p.SubjectID,
                        EducationLevelID = p.ClassProfile.EducationLevelID,
                        IsCommenting = p.IsCommenting,
                        OrderInClass = p.ClassProfile.OrderNumber
                    }).OrderBy(p => p.OrderInClass).ThenBy(p => p.ClassName).ToList();

            List<int> lstClassID = lstCS.Select(p => p.ClassID).Distinct().ToList();

            List<SubjectCat> lstSubjectCat = SubjectCatBusiness.All.Where(x => x.IsActive && lstSubjectID.Contains(x.SubjectCatID))
                                                               .OrderBy(x => x.OrderInSubject).ThenBy(p => p.SubjectName).ToList();

            #region lay du lieu
            IDictionary<string, object> dicSearchPupil = new Dictionary<string, object>()
            {
                {"SchoolID",SchoolID},
                {"AcademicYearID",AcademicYearID}
            };
            var iquery = PupilOfClassBusiness.Search(dicSearchPupil).Where(p => lstClassID.Contains(p.ClassID)).AddCriteriaSemester(objAy, SemesterID);
            var lstPupilOfClass = iquery.ToList();

            // Danh sach hoc sinh
            List<GroupPOC> lstGroupPOCSheetTotal = new List<GroupPOC>();
            List<GroupPOC> lstGroupPOCSheetFemale = new List<GroupPOC>();
            List<GroupPOC> lstGroupPOCSheetEthnic = new List<GroupPOC>();
            List<GroupPOC> lstGroupPOCSheetFemaleEthnic = new List<GroupPOC>();

            lstGroupPOCSheetTotal = this.ListGroupPOC(1, lstPupilOfClass);

            if (FemaleType > 0)
            {
                lstGroupPOCSheetFemale = this.ListGroupPOC(2, lstPupilOfClass);
            }
            if (EthnicType > 0)
            {
                lstGroupPOCSheetEthnic = this.ListGroupPOC(3, lstPupilOfClass);
            }
            if (FemaleEthnicType > 0)
            {
                lstGroupPOCSheetFemaleEthnic = this.ListGroupPOC(4, lstPupilOfClass);
            }

            List<RatedCommentPupilBO> lstRatedCommentPupil = new List<RatedCommentPupilBO>();
            IDictionary<string, object> dicRated = new Dictionary<string, object>()
            {
                {"SchoolID",SchoolID},
                {"AcademicYearID",AcademicYearID},
                {"SemesterID",SemesterID},
                {"SubjectID",SubjectID},
                {"FemaleType",FemaleType},
                {"EthnicType",EthnicType},
                {"TypeID",GlobalConstants.TYPE_EVALUATION_SUBJECTANDEDUCATIONGROUP}
            };
            lstRatedCommentPupil = this.GetListRatedCommentPupilBO(dicRated, objAy, lstClassID, iquery).Where(x => lstSubjectID.Contains(x.SubjectID)).ToList();
            #endregion

            #region fill du lieu
            List<GroupPOC> GrouptmpTotal = new List<GroupPOC>();
            List<GroupPOC> GrouptmpFemale = new List<GroupPOC>();
            List<GroupPOC> GrouptmpEthnic = new List<GroupPOC>();
            List<GroupPOC> GrouptmpFemaleEthnic = new List<GroupPOC>();

            if (lstSubjectCat.Count() > 0)
            {
                SubjectCat objSC = lstSubjectCat[0];
               
                oBook = FillDatatoExcel(oBook, objSC, objSP, objAy, lstCS, lstRatedCommentPupil, lstGroupPOCSheetTotal,
                    lstGroupPOCSheetFemale, lstGroupPOCSheetEthnic, lstGroupPOCSheetFemaleEthnic,
                    SemesterTypeID, FemaleType, EthnicType, FemaleEthnicType, CountEducationLevel, TypeExcelPDF, IsZip);
            }
            else
            {
                IVTWorksheet sheet1 = oBook.GetSheet(1);
                IVTWorksheet sheet2 = oBook.GetSheet(2);
                sheet1.Delete();
                sheet2.Delete();
            }
            #endregion

            return oBook.ToStream();
        }

        public void DownloadZipFileSubjectAndEducationGroup(IDictionary<string, object> dic, out string folderSaveFile)
        {
            int AppliedLevel = Utils.GetInt(dic, "AppliedLevel");
            int SchoolID = Utils.GetInt(dic, "SchoolID");
            int AcademicYearID = Utils.GetInt(dic, "AcademicYearID");
            int SemesterTypeID = Utils.GetInt(dic, "SemesterTypeID");
            int SubjectID = Utils.GetInt(dic, "SubjectID");
            int FemaleType = Utils.GetInt(dic, "FemaleType");
            int EthnicType = Utils.GetInt(dic, "EthnicType");
            int FemaleEthnicType = Utils.GetInt(dic, "FemaleEthnicType");
            int AppliedLevelID = Utils.GetInt(dic, "AppliedLevelID");
            int CountEducationLevel = Utils.GetInt(dic, "CountEducationLevel");

            int TypeExcelPDF = Utils.GetInt(dic, "TypeExcelPDF"); // Xác định xuất Excel hay PDf. Fix lỗi xuất PDF ZIP file không tính %
            bool IsZip = SubjectID > 0 ? false: true;

            SchoolProfile objSP = SchoolProfileBusiness.Find(SchoolID);
            AcademicYear objAy = AcademicYearBusiness.Find(AcademicYearID);

            int SemesterID = 0;
            if (SemesterTypeID == SystemParamsInFile.GENERAL_SUMMED_EVALUATION_GKI || SemesterTypeID == SystemParamsInFile.GENERAL_SUMMED_EVALUATION_CKI)
            {
                SemesterID = GlobalConstants.SEMESTER_OF_YEAR_FIRST;
            }
            else
            {
                SemesterID = GlobalConstants.SEMESTER_OF_YEAR_SECOND;
            }

            IDictionary<string, object> dicSS = new Dictionary<string, object>()
            {
                {"SchoolID", SchoolID},
                {"AcademicYearID", AcademicYearID},
                {"AppliedLevel", AppliedLevel}
            };
            List<SchoolSubjectBO> lstSchoolSubject = SchoolSubjectBusiness.Search(dicSS)
                                                          .Select(p => new SchoolSubjectBO
                                                          {
                                                              SubjectID = p.SubjectID,
                                                              SubjectName = p.SubjectCat.SubjectName,
                                                              OrderInSubject = p.SubjectCat.OrderInSubject
                                                          }).Distinct().OrderBy(p => p.OrderInSubject).ThenBy(p => p.SubjectName).ToList();

            if (SubjectID > 0)
            {
                lstSchoolSubject = lstSchoolSubject.Where(x => x.SubjectID == SubjectID).ToList();
            }
            List<int> lstSubjectID = lstSchoolSubject.Select(x => x.SubjectID).ToList();

            // lay danh sach cac lop co hoc mon duoc chon
            List<ClassSubjectBO> lstCS = new List<ClassSubjectBO>();
            IDictionary<string, object> dicSearchClass = new Dictionary<string, object>()
            {
                {"SchoolID",SchoolID},
                {"AcademicYearID",AcademicYearID}
            };
            lstCS = ClassSubjectBusiness.Search(dicSearchClass).Where(x => lstSubjectID.Contains(x.SubjectID))
                    .Select(p => new ClassSubjectBO
                    {
                        ClassID = p.ClassID,
                        ClassName = p.ClassProfile.DisplayName,
                        SubjectID = p.SubjectID,
                        EducationLevelID = p.ClassProfile.EducationLevelID,
                        IsCommenting = p.IsCommenting,
                        OrderInClass = p.ClassProfile.OrderNumber
                    }).OrderBy(p => p.OrderInClass).ThenBy(p => p.ClassName).ToList();

            List<int> lstClassID = lstCS.Select(p => p.ClassID).Distinct().ToList();
            lstSubjectID = lstCS.Select(x => x.SubjectID).ToList();

            List<SubjectCat> lstSubjectCat = SubjectCatBusiness.All.Where(x => x.IsActive && lstSubjectID.Contains(x.SubjectCatID))
                                                               .OrderBy(x => x.OrderInSubject).ThenBy(p => p.SubjectName).ToList();

            #region lay du lieu
            IDictionary<string, object> dicSearchPupil = new Dictionary<string, object>()
            {
                {"SchoolID",SchoolID},
                {"AcademicYearID",AcademicYearID}
            };
            var iquery = PupilOfClassBusiness.Search(dicSearchPupil).Where(p => lstClassID.Contains(p.ClassID)).AddCriteriaSemester(objAy, SemesterID);
            var lstPupilOfClass = iquery.ToList();
            // Danh sach hoc sinh
            List<GroupPOC> lstGroupPOCSheetTotal = new List<GroupPOC>();
            List<GroupPOC> lstGroupPOCSheetFemale = new List<GroupPOC>();
            List<GroupPOC> lstGroupPOCSheetEthnic = new List<GroupPOC>();
            List<GroupPOC> lstGroupPOCSheetFemaleEthnic = new List<GroupPOC>();

            lstGroupPOCSheetTotal = this.ListGroupPOC(1, lstPupilOfClass);

            if (FemaleType > 0)
            {
                lstGroupPOCSheetFemale = this.ListGroupPOC(2, lstPupilOfClass);
            }
            if (EthnicType > 0)
            {
                lstGroupPOCSheetEthnic = this.ListGroupPOC(3, lstPupilOfClass);
            }
            if (FemaleEthnicType > 0)
            {
                lstGroupPOCSheetFemaleEthnic = this.ListGroupPOC(4, lstPupilOfClass);
            }
       
            List<RatedCommentPupilBO> lstRatedCommentPupil = new List<RatedCommentPupilBO>();
            IDictionary<string, object> dicRated = new Dictionary<string, object>()
            {
                {"SchoolID",SchoolID},
                {"AcademicYearID",AcademicYearID},
                {"SemesterID",SemesterID},
                {"SubjectID",SubjectID},
                {"FemaleType",FemaleType},
                {"EthnicType",EthnicType},
                {"TypeID",GlobalConstants.TYPE_EVALUATION_SUBJECTANDEDUCATIONGROUP}
            };
            lstRatedCommentPupil = this.GetListRatedCommentPupilBO(dicRated, objAy, lstClassID, iquery).Where(x => lstSubjectID.Contains(x.SubjectID)).ToList();
            #endregion

            #region fill du lieu
            string fileName = "BC_TH_ThongKeMonHocHDGD_{0}_{1}.xls"; //BC_TH_ThongKeMonHocHDGD_CuoiHKII_TiengViet           
            string tmp = string.Empty;
            string folder = Path.Combine(AppDomain.CurrentDomain.BaseDirectory + SystemParamsInFile.UPLOAD_FILE + "\\" + Guid.NewGuid().ToString());
            Directory.CreateDirectory(folder);

            string reportCode = SystemParamsInFile.REPORT_SUBJECT_AND_EDUCATION_GROUP;
            ReportDefinition reportDef = ReportDefinitionBusiness.GetByCode(reportCode);

            string SemesterName = SemesterTypeID == SystemParamsInFile.GENERAL_SUMMED_EVALUATION_GKI ? "GiuaHKI"
                                 : SemesterTypeID == SystemParamsInFile.GENERAL_SUMMED_EVALUATION_GKII ? "GiuaHKII"
                                 : SemesterTypeID == SystemParamsInFile.GENERAL_SUMMED_EVALUATION_CKI ? "CuoiHKI"
                                 : "CuoiHKII";

            SubjectCat objSubjectCat = null;
            for (int i = 0; i < lstSubjectCat.Count(); i++)
            {
                objSubjectCat = lstSubjectCat[i];
                string templatePath = ReportUtils.GetTemplatePath(reportDef);
                IVTWorkbook oBook = VTExport.OpenWorkbook(templatePath);
                string fileNameBySubject = string.Format(fileName, SemesterName, Utils.StripVNSignAndSpace(Utils.RemoveUnprintableChar(objSubjectCat.SubjectName)));              

                oBook = FillDatatoExcel(oBook, objSubjectCat, objSP, objAy, lstCS,
                    lstRatedCommentPupil, lstGroupPOCSheetTotal, lstGroupPOCSheetFemale, lstGroupPOCSheetEthnic, lstGroupPOCSheetFemaleEthnic,
                    SemesterTypeID, FemaleType, EthnicType, FemaleEthnicType, CountEducationLevel, TypeExcelPDF, IsZip);

                oBook.CreateZipFile(fileNameBySubject, folder);
            }
            folderSaveFile = folder;
            #endregion
        }

        private IVTWorkbook FillDatatoExcel(IVTWorkbook oBook, SubjectCat objSubjectCat,
            SchoolProfile objSP, AcademicYear objAy,
            List<ClassSubjectBO> lstCS,
            List<RatedCommentPupilBO> lstRatedCommentPupil,
            List<GroupPOC> lstGroupPOCSheetTotal,
            List<GroupPOC> lstGroupPOCSheetFemale,
            List<GroupPOC> lstGroupPOCSheetEthnic,
            List<GroupPOC> lstGroupPOCSheetFemaleEthnic,
            int SemesterTypeID, int FemaleType, int EthnicType, int FemaleEthnicType, int CountEducationLevel,
            int TypeExcelPDF, bool IsZip)
        {
            IVTWorksheet sheet1 = oBook.GetSheet(1);
            IVTWorksheet sheet2 = oBook.GetSheet(2);
            IVTWorksheet sheetTotal = null;         // Sheet tong
            IVTWorksheet sheetFemale = null;        // Sheet hoc sinh nu
            IVTWorksheet sheetEthnic = null;        // Sheet hoc sinh dan toc
            IVTWorksheet sheetFemaleEthnic = null;  // sheet hoc sinh nu dan toc

            #region fill du lieu
            List<GroupPOC> GrouptmpTotal = new List<GroupPOC>();
            List<GroupPOC> GrouptmpFemale = new List<GroupPOC>();
            List<GroupPOC> GrouptmpEthnic = new List<GroupPOC>();
            List<GroupPOC> GrouptmpFemaleEthnic = new List<GroupPOC>();

            SubjectCat objSC = objSubjectCat;
            List<int> lstClassIDBysubject = lstCS.Where(x => x.SubjectID == objSC.SubjectCatID).Select(x => x.ClassID).ToList();
            lstGroupPOCSheetTotal = lstGroupPOCSheetTotal.Where(x => lstClassIDBysubject.Contains(x.ClassID)).ToList();
            lstGroupPOCSheetFemale = lstGroupPOCSheetFemale.Where(x => lstClassIDBysubject.Contains(x.ClassID)).ToList();
            lstGroupPOCSheetEthnic = lstGroupPOCSheetEthnic.Where(x => lstClassIDBysubject.Contains(x.ClassID)).ToList();
            lstGroupPOCSheetFemaleEthnic = lstGroupPOCSheetFemaleEthnic.Where(x => lstClassIDBysubject.Contains(x.ClassID)).ToList();
            var lstRatedCommentPupilBySubject = lstRatedCommentPupil.Where(x => x.SubjectID == objSC.SubjectCatID && lstClassIDBysubject.Contains(x.ClassID)).ToList();

            #region // new sheet
            int countSubjectMark = lstCS.Where(p => p.IsCommenting == 0 && p.SubjectID == objSC.SubjectCatID).Count();
            bool isSheet1 = countSubjectMark > 0;
            int sheetID = 0;
            if (SemesterTypeID == SystemParamsInFile.GENERAL_SUMMED_EVALUATION_GKI || SemesterTypeID == SystemParamsInFile.GENERAL_SUMMED_EVALUATION_GKII)
            {
                if (objSC.Abbreviation == "Tt" || objSC.Abbreviation == "TV")
                {
                    sheetTotal = oBook.CopySheetToLast(sheet1, "IV1000");
                    if (FemaleType != 0)
                    {
                        sheetFemale = oBook.CopySheetToLast(sheet1, "IV1000");
                    }
                    if (EthnicType != 0)
                    {
                        sheetEthnic = oBook.CopySheetToLast(sheet1, "IV1000");
                    }
                    if (FemaleEthnicType != 0)
                    {
                        sheetFemaleEthnic = oBook.CopySheetToLast(sheet1, "IV1000");
                    }

                    sheetID = 1;
                }
                else
                {
                    sheetTotal = oBook.CopySheetToLast(sheet2, "IV1000");
                    if (FemaleType != 0)
                    {
                        sheetFemale = oBook.CopySheetToLast(sheet2, "IV1000");
                    }
                    if (EthnicType != 0)
                    {
                        sheetEthnic = oBook.CopySheetToLast(sheet2, "IV1000");
                    }
                    if (FemaleEthnicType != 0)
                    {
                        sheetFemaleEthnic = oBook.CopySheetToLast(sheet2, "IV1000");
                    }
                    sheetID = 2;
                }
            }
            else
            {
                if (isSheet1)
                {
                    sheetTotal = oBook.CopySheetToLast(sheet1, "IV1000");
                    if (FemaleType != 0)
                    {
                        sheetFemale = oBook.CopySheetToLast(sheet1, "IV1000");
                    }
                    if (EthnicType != 0)
                    {
                        sheetEthnic = oBook.CopySheetToLast(sheet1, "IV1000");
                    }
                    if (FemaleEthnicType != 0)
                    {
                        sheetFemaleEthnic = oBook.CopySheetToLast(sheet1, "IV1000");
                    }

                    sheetID = 1;
                }
                else
                {
                    sheetTotal = oBook.CopySheetToLast(sheet2, "IV1000");
                    if (FemaleType != 0)
                    {
                        sheetFemale = oBook.CopySheetToLast(sheet2, "IV1000");
                    }
                    if (EthnicType != 0)
                    {
                        sheetEthnic = oBook.CopySheetToLast(sheet2, "IV1000");
                    }
                    if (FemaleEthnicType != 0)
                    {
                        sheetFemaleEthnic = oBook.CopySheetToLast(sheet2, "IV1000");
                    }

                    sheetID = 2;
                }
            }
            #endregion

            #region // Fill thông tin chung
            sheetTotal = this.RenderInfo(sheetTotal, objSP, objAy, sheetID, objSC.DisplayName, SemesterTypeID, 1);

            int countFomularAll = 0;
            if (sheetID == 1)
            {
                countFomularAll = 31;
            }
            else
            {
                countFomularAll = 9;
            }

            sheetTotal.SetCellValue("A12", "Toàn trường");
            sheetTotal.GetRange("A12", "B12").Merge();
            sheetTotal.GetRange(12, 1, 12, countFomularAll).SetFontStyle(true, null, false, null, false, false);
            sheetTotal.GetRange(12, 3, 12, countFomularAll).FillColor(255, 204, 242);

            if (FemaleType != 0)
            {
                sheetFemale = this.RenderInfo(sheetFemale, objSP, objAy, sheetID, objSC.DisplayName, SemesterTypeID, 2);
                sheetFemale.SetCellValue("A12", "Toàn trường");
                sheetFemale.GetRange("A12", "B12").Merge();
                sheetFemale.GetRange(12, 1, 12, countFomularAll).SetFontStyle(true, null, false, null, false, false);
                sheetFemale.GetRange(12, 3, 12, countFomularAll).FillColor(255, 204, 242);
            }
            if (EthnicType != 0)
            {
                sheetEthnic = this.RenderInfo(sheetEthnic, objSP, objAy, sheetID, objSC.DisplayName, SemesterTypeID, 3);
                sheetEthnic.SetCellValue("A12", "Toàn trường");
                sheetEthnic.GetRange("A12", "B12").Merge();
                sheetEthnic.GetRange(12, 1, 12, countFomularAll).SetFontStyle(true, null, false, null, false, false);
                sheetEthnic.GetRange(12, 3, 12, countFomularAll).FillColor(255, 204, 242);
            }
            if (FemaleEthnicType != 0)
            {
                sheetFemaleEthnic = this.RenderInfo(sheetFemaleEthnic, objSP, objAy, sheetID, objSC.DisplayName, SemesterTypeID, 4);
                sheetFemaleEthnic.SetCellValue("A12", "Toàn trường");
                sheetFemaleEthnic.GetRange("A12", "B12").Merge();
                sheetFemaleEthnic.GetRange(12, 1, 12, countFomularAll).SetFontStyle(true, null, false, null, false, false);
                sheetFemaleEthnic.GetRange(12, 3, 12, countFomularAll).FillColor(255, 204, 242);
            }
            #endregion

            bool isFillMark = SemesterTypeID == SystemParamsInFile.GENERAL_SUMMED_EVALUATION_CKI || SemesterTypeID == SystemParamsInFile.GENERAL_SUMMED_EVALUATION_CKII;
            int startRowSheetTotal = 13;
            int startRowSheetFemale = 13;
            int startRowSheetEthnic = 13;
            int startRowSheetFemaleEthnic = 13;
            int countT = 0;
            int countH = 0;
            int countC = 0;
            int countMark = 0;
            int STT = 1;
            int startCol = 0;
            string formular = string.Empty;
            string fPercent = string.Empty;
            List<int> lstSumSheetTotal = new List<int>();
            List<int> lstSumSheetFemale = new List<int>();
            List<int> lstSumSheetEthnic = new List<int>();
            List<int> lstSumSheetFemaleEthnic = new List<int>();

            List<int> lstCountTotalSchool_SheetTotal = new List<int>();
            List<int> lstCountTotalSchool_SheetFemale = new List<int>();
            List<int> lstCountTotalSchool_SheetEthnic = new List<int>();
            List<int> lstCountTotalSchool_FemaleEthnic = new List<int>();
            for (int i = 0; i < CountEducationLevel; i++)
            {
                #region // Fill khối
                sheetTotal = this.RenderEducationLevel(sheetTotal, startRowSheetTotal, STT, countFomularAll);

                if (FemaleType != 0)
                {
                    sheetFemale = this.RenderEducationLevel(sheetFemale, startRowSheetFemale, STT, countFomularAll);
                }
                if (EthnicType != 0)
                {
                    sheetEthnic = this.RenderEducationLevel(sheetEthnic, startRowSheetEthnic, STT, countFomularAll);
                }
                if (FemaleEthnicType != 0)
                {
                    sheetFemaleEthnic = this.RenderEducationLevel(sheetFemaleEthnic, startRowSheetFemaleEthnic, STT, countFomularAll);
                }
                #endregion

                startRowSheetTotal++;
                startRowSheetFemale++;
                startRowSheetEthnic++;
                startRowSheetFemaleEthnic++;             

                #region // Fill lớp
                #region

                #region tat ca
                lstSumSheetTotal.Add((startRowSheetTotal - 1));

                GrouptmpTotal = lstGroupPOCSheetTotal.Where(p => p.EducationLevelID == STT).ToList();

                sheetTotal = this.RenderStatisticsClass(sheetTotal, objSC, GrouptmpTotal, lstRatedCommentPupilBySubject,
                                                        STT, SemesterTypeID, sheetID, countFomularAll, isFillMark, TypeExcelPDF, IsZip, ref startRowSheetTotal);
                #endregion

                #region hoc sinh nu
                if (FemaleType != 0)
                {
                    lstSumSheetFemale.Add((startRowSheetFemale - 1));
                    GrouptmpFemale = lstGroupPOCSheetFemale.Where(x => x.EducationLevelID == STT).ToList();
                    var lstRatedCommentPupilFemale = lstRatedCommentPupilBySubject.Where(x => x.Genre == 0 && x.EducationLevelID == STT).ToList();

                    sheetFemale = this.RenderStatisticsClass(sheetFemale, objSC, GrouptmpFemale, lstRatedCommentPupilFemale,
                                                            STT, SemesterTypeID, sheetID, countFomularAll, isFillMark, TypeExcelPDF, IsZip, ref startRowSheetFemale);
                }
                #endregion

                #region hoc sinh dan toc
                if (EthnicType != 0)
                {
                    lstSumSheetEthnic.Add((startRowSheetEthnic - 1));
                    GrouptmpEthnic = lstGroupPOCSheetEthnic.Where(x => x.EducationLevelID == STT).ToList();
                    var lstRatedCommentPupilEthnic = lstRatedCommentPupilBySubject.Where(x => x.EducationLevelID == STT && x.EthnicID != SystemParamsInFile.ETHNIC_ID_KINH
                                                                                    && x.EthnicID != SystemParamsInFile.ETHNIC_ID_NN && x.EthnicID.HasValue).ToList();

                    sheetEthnic = this.RenderStatisticsClass(sheetEthnic, objSC, GrouptmpEthnic, lstRatedCommentPupilEthnic,
                                                           STT, SemesterTypeID, sheetID, countFomularAll, isFillMark, TypeExcelPDF, IsZip, ref startRowSheetEthnic);
                }
                #endregion

                #region hoc sinh nu dan toc
                if (FemaleEthnicType != 0)
                {
                    lstSumSheetFemaleEthnic.Add((startRowSheetFemaleEthnic - 1));

                    GrouptmpFemaleEthnic = lstGroupPOCSheetFemaleEthnic.Where(x => x.EducationLevelID == STT).ToList();

                    var lstRatedCommentPupilFemaleEthnic = lstRatedCommentPupilBySubject.Where(x => x.EducationLevelID == STT && x.EthnicID != SystemParamsInFile.ETHNIC_ID_KINH
                                                                                    && x.EthnicID != SystemParamsInFile.ETHNIC_ID_NN
                                                                                    && x.EthnicID.HasValue && x.Genre == 0).ToList();

                    sheetFemaleEthnic = this.RenderStatisticsClass(sheetFemaleEthnic, objSC, GrouptmpFemaleEthnic, lstRatedCommentPupilFemaleEthnic,
                                                           STT, SemesterTypeID, sheetID, countFomularAll, isFillMark,TypeExcelPDF, IsZip, ref startRowSheetFemaleEthnic);
                }
                #endregion

                #endregion             
                #endregion

                #region // Tính sum từng khối

                sheetTotal = this.RenderSumEducation(sheetTotal, (startRowSheetTotal - GrouptmpTotal.Count()), sheetID, GrouptmpTotal.Count(), TypeExcelPDF, IsZip);             
                if (FemaleType != 0)
                {
                    sheetFemale = this.RenderSumEducation(sheetFemale, (startRowSheetFemale - GrouptmpFemale.Count()), sheetID, GrouptmpFemale.Count(), TypeExcelPDF, IsZip);                   
                }
                if (EthnicType != 0)
                {
                    sheetEthnic = this.RenderSumEducation(sheetEthnic, (startRowSheetEthnic - GrouptmpEthnic.Count()), sheetID, GrouptmpEthnic.Count(), TypeExcelPDF, IsZip);                   
                }
                if (FemaleEthnicType != 0)
                {
                    sheetFemaleEthnic = this.RenderSumEducation(sheetFemaleEthnic, (startRowSheetFemaleEthnic - GrouptmpFemaleEthnic.Count()), sheetID, GrouptmpFemaleEthnic.Count(), TypeExcelPDF, IsZip);                  
                }
                #endregion

                STT++;
            }

            #region //fill formular Toàn trường - sheet tat ca
            List<CountMarkCellValue> lstCountMark = new List<CountMarkCellValue>();
            formular = string.Empty;
            formular = "=SUM(";
            for (int i = 0; i < lstSumSheetTotal.Count; i++)
            {
                formular += "C" + lstSumSheetTotal[i];
                if (i < lstSumSheetTotal.Count - 1)
                {
                    formular += ",";
                }

                if ((TypeExcelPDF == 2 && IsZip))
                {
                    int countPupil = 0;
                    if (sheetTotal.GetCellValue(lstSumSheetTotal[i], 3) != null)
                    {
                        countPupil = Int32.Parse(sheetTotal.GetCellValue(lstSumSheetTotal[i], 3).ToString());
                    }
                    countT = 0;
                    if (sheetTotal.GetCellValue(lstSumSheetTotal[i], 4) != null)
                    {
                        countT = Int32.Parse(sheetTotal.GetCellValue(lstSumSheetTotal[i], 4).ToString());
                    }
                    countH = 0;
                    if (sheetTotal.GetCellValue(lstSumSheetTotal[i], 6) != null)
                    {
                        countH = Int32.Parse(sheetTotal.GetCellValue(lstSumSheetTotal[i], 6).ToString());
                    }
                    countC = 0;
                    if (sheetTotal.GetCellValue(lstSumSheetTotal[i], 8) != null)
                    {
                        countC = Int32.Parse(sheetTotal.GetCellValue(lstSumSheetTotal[i], 8).ToString());
                    }

                    lstCountMark.Add(new CountMarkCellValue() { Cell = 3, Count = countPupil });
                    lstCountMark.Add(new CountMarkCellValue() { Cell = 4, Count = countT });
                    lstCountMark.Add(new CountMarkCellValue() { Cell = 6, Count = countH });
                    lstCountMark.Add(new CountMarkCellValue() { Cell = 8, Count = countC });

                    if (sheetID == 1)
                    {
                        startCol = 10;
                        countMark = 0;
                        for (int j = 11; j > 0; j--)
                        {
                            if (sheetTotal.GetCellValue(lstSumSheetTotal[i], startCol) != null)
                            {
                                countMark = Int32.Parse(sheetTotal.GetCellValue(lstSumSheetTotal[i], startCol).ToString());
                            }

                            lstCountMark.Add(new CountMarkCellValue() { Cell = startCol, Count = countMark });
                            startCol += 2;
                            countMark = 0;
                        }
                    }  
                }               
            }
            formular += ")";
            sheetTotal.SetFormulaValue(12, 3, formular);

            int totalPupil = 0;
            if (TypeExcelPDF == 2 && IsZip)
            {
                totalPupil = lstCountMark.Where(x => x.Cell == 3).Count() > 0 ? lstCountMark.Where(x => x.Cell == 3).Select(x=>x.Count).Sum() : 0;
                int totalT = lstCountMark.Where(x => x.Cell == 4).Count() > 0 ? lstCountMark.Where(x => x.Cell == 4).Select(x => x.Count).Sum() : 0;
                int totalH = lstCountMark.Where(x => x.Cell == 6).Count() > 0 ? lstCountMark.Where(x => x.Cell == 6).Select(x => x.Count).Sum() : 0;
                int totalC = lstCountMark.Where(x => x.Cell == 8).Count() > 0 ? lstCountMark.Where(x => x.Cell == 8).Select(x => x.Count).Sum() : 0;

                sheetTotal.SetCellValue(12, 3, totalPupil);
                sheetTotal.SetCellValue(12, 4, totalT);
                sheetTotal.SetCellValue(12, 6, totalH);
                sheetTotal.SetCellValue(12, 8, totalC);

                sheetTotal.SetCellValue("E12", totalT > 0 ? (Math.Round((decimal)totalT / (decimal)totalPupil, 4) * 100) : 0);
                sheetTotal.SetCellValue("G12", totalH > 0 ? (Math.Round((decimal)totalH / (decimal)totalPupil, 4) * 100) : 0);
                sheetTotal.SetCellValue("I12", totalC > 0 ? (Math.Round((decimal)totalC / (decimal)totalPupil, 4) * 100) : 0);
            }
            else
            {
                fPercent = "=ROUND(D12/IF(C12<=0,1,C12),4)*100";
                sheetTotal.SetCellValue("E12", fPercent);
                fPercent = "=ROUND(F12/IF(C12<=0,1,C12),4)*100";
                sheetTotal.SetCellValue("G12", fPercent);
                fPercent = "=ROUND(H12/IF(C12<=0,1,C12),4)*100";
                sheetTotal.SetCellValue("I12", fPercent);
            }
       
            if (sheetID == 1)
            {
                //fill formular dong tong cong
                startCol = 10;
                int startRowFormular = 12;
                for (int j = 11; j > 0; j--)
                {
                    if (TypeExcelPDF == 2 && IsZip)
                    {
                        int totalOfMark = lstCountMark.Where(x => x.Cell == startCol).Count() > 0 ? lstCountMark.Where(x => x.Cell == startCol).Select(x => x.Count).Sum() : 0;
                        sheetTotal.SetCellValue(startRowFormular, startCol, totalOfMark);
                        sheetTotal.SetCellValue(startRowFormular, startCol + 1, totalOfMark > 0 ? (Math.Round((decimal)totalOfMark / (decimal)totalPupil, 4) * 100) : 0);
                    }
                    else
                    {
                        fPercent = "=ROUND(" + (UtilsBusiness.GetExcelColumnName(startCol) + startRowFormular) + "/IF(C" + startRowFormular + "<=0,1,C" + startRowFormular + "),4)*100";
                        sheetTotal.SetFormulaValue(startRowFormular, startCol + 1, fPercent);
                    }

                    startCol += 2;
                }
            }

            if (TypeExcelPDF != 2 || !IsZip)
            {
                for (int i = 4; i <= countFomularAll; i += 2)
                {
                    formular = string.Empty;
                    formular = "=SUM(";
                    for (int j = 0; j < lstSumSheetTotal.Count; j++)
                    {
                        formular += UtilsBusiness.GetExcelColumnName(i) + lstSumSheetTotal[j];
                        if (j < lstSumSheetTotal.Count - 1)
                        {
                            formular += ",";
                        }
                    }
                    formular += ")";
                    sheetTotal.SetFormulaValue(12, i, formular);
                }
            }
          
            sheetTotal.GetRange(12, 1, startRowSheetTotal - 1, countFomularAll).SetBorder(VTBorderStyle.Solid, VTBorderWeight.Thin, VTBorderIndex.All);
            sheetTotal.FitToPage = true;
            sheetTotal.Name = Utils.StripVNSignAndSpace(objSC.DisplayName);

            #endregion

            #region //fill formular Toàn trường - sheet hoc sinh nu
            if (FemaleType > 0)
            {
                lstCountMark = new List<CountMarkCellValue>();
                formular = string.Empty;
                formular = "=SUM(";
                for (int i = 0; i < lstSumSheetFemale.Count; i++)
                {
                    formular += "C" + lstSumSheetFemale[i];
                    if (i < lstSumSheetFemale.Count - 1)
                    {
                        formular += ",";
                    }

                    if (TypeExcelPDF == 2 && IsZip)
                    {
                        int countPupil = 0;
                        if (sheetFemale.GetCellValue(lstSumSheetFemale[i], 3) != null)
                        {
                            countPupil = Int32.Parse(sheetFemale.GetCellValue(lstSumSheetFemale[i], 3).ToString());
                        }
                        countT = 0;
                        if (sheetFemale.GetCellValue(lstSumSheetFemale[i], 4) != null)
                        {
                            countT = Int32.Parse(sheetFemale.GetCellValue(lstSumSheetFemale[i], 4).ToString());
                        }
                        countH = 0;
                        if (sheetFemale.GetCellValue(lstSumSheetFemale[i], 6) != null)
                        {
                            countH = Int32.Parse(sheetFemale.GetCellValue(lstSumSheetFemale[i], 6).ToString());
                        }
                        countC = 0;
                        if (sheetFemale.GetCellValue(lstSumSheetFemale[i], 8) != null)
                        {
                            countC = Int32.Parse(sheetFemale.GetCellValue(lstSumSheetFemale[i], 8).ToString());
                        }

                        lstCountMark.Add(new CountMarkCellValue() { Cell = 3, Count = countPupil });
                        lstCountMark.Add(new CountMarkCellValue() { Cell = 4, Count = countT });
                        lstCountMark.Add(new CountMarkCellValue() { Cell = 6, Count = countH });
                        lstCountMark.Add(new CountMarkCellValue() { Cell = 8, Count = countC });

                        if (sheetID == 1)
                        {
                            startCol = 10;
                            countMark = 0;
                            for (int j = 11; j > 0; j--)
                            {
                                if (sheetFemale.GetCellValue(lstSumSheetFemale[i], startCol) != null)
                                {
                                    countMark = Int32.Parse(sheetFemale.GetCellValue(lstSumSheetFemale[i], startCol).ToString());
                                }

                                lstCountMark.Add(new CountMarkCellValue() { Cell = startCol, Count = countMark });
                                startCol += 2;
                                countMark = 0;
                            }
                        } 
                    }                       
                }
                formular += ")";
                sheetFemale.SetFormulaValue(12, 3, formular);


                if (TypeExcelPDF == 2 && IsZip)
                {
                    totalPupil = lstCountMark.Where(x => x.Cell == 3).Count() > 0 ? lstCountMark.Where(x => x.Cell == 3).Select(x => x.Count).Sum() : 0;
                    int totalT = lstCountMark.Where(x => x.Cell == 4).Count() > 0 ? lstCountMark.Where(x => x.Cell == 4).Select(x => x.Count).Sum() : 0;
                    int totalH = lstCountMark.Where(x => x.Cell == 6).Count() > 0 ? lstCountMark.Where(x => x.Cell == 6).Select(x => x.Count).Sum() : 0;
                    int totalC = lstCountMark.Where(x => x.Cell == 8).Count() > 0 ? lstCountMark.Where(x => x.Cell == 8).Select(x => x.Count).Sum() : 0;

                    sheetFemale.SetCellValue(12, 3, totalPupil);
                    sheetFemale.SetCellValue(12, 4, totalT);
                    sheetFemale.SetCellValue(12, 6, totalH);
                    sheetFemale.SetCellValue(12, 8, totalC);

                    sheetFemale.SetCellValue("E12", totalT > 0 ? (Math.Round((decimal)totalT / (decimal)totalPupil, 4) * 100) : 0);
                    sheetFemale.SetCellValue("G12", totalH > 0 ? (Math.Round((decimal)totalH / (decimal)totalPupil, 4) * 100) : 0);
                    sheetFemale.SetCellValue("I12", totalC > 0 ? (Math.Round((decimal)totalC / (decimal)totalPupil, 4) * 100) : 0);
                }
                else
                {
                    fPercent = "=ROUND(D12/IF(C12<=0,1,C12),4)*100";
                    sheetFemale.SetCellValue("E12", fPercent);
                    fPercent = "=ROUND(F12/IF(C12<=0,1,C12),4)*100";
                    sheetFemale.SetCellValue("G12", fPercent);
                    fPercent = "=ROUND(H12/IF(C12<=0,1,C12),4)*100";
                    sheetFemale.SetCellValue("I12", fPercent);
                }  

                if (sheetID == 1)
                {
                    //fill formular dong tong cong
                    startCol = 10;
                    int startRowFormular = 12;
                    for (int j = 11; j > 0; j--)
                    {
                        if (TypeExcelPDF == 2 && IsZip)
                        {
                            int totalOfMark = lstCountMark.Where(x => x.Cell == startCol).Count() > 0 ? lstCountMark.Where(x => x.Cell == startCol).Select(x => x.Count).Sum() : 0;
                            sheetFemale.SetCellValue(startRowFormular, startCol, totalOfMark);
                            sheetFemale.SetCellValue(startRowFormular, startCol + 1, totalOfMark > 0 ? (Math.Round((decimal)totalOfMark / (decimal)totalPupil, 4) * 100) : 0);
                        }
                        else
                        {
                            fPercent = "=ROUND(" + (UtilsBusiness.GetExcelColumnName(startCol) + startRowFormular) + "/IF(C" + startRowFormular + "<=0,1,C" + startRowFormular + "),4)*100";
                            sheetFemale.SetFormulaValue(startRowFormular, startCol + 1, fPercent);
                        }

                        startCol += 2;
                    }
                }

                if (TypeExcelPDF != 2 || !IsZip)
                {
                    for (int i = 4; i <= countFomularAll; i += 2)
                    {
                        formular = string.Empty;
                        formular = "=SUM(";
                        for (int j = 0; j < lstSumSheetFemale.Count; j++)
                        {
                            formular += UtilsBusiness.GetExcelColumnName(i) + lstSumSheetFemale[j];
                            if (j < lstSumSheetFemale.Count - 1)
                            {
                                formular += ",";
                            }
                        }
                        formular += ")";
                        sheetFemale.SetFormulaValue(12, i, formular);
                    }
                }
                sheetFemale.GetRange(12, 1, startRowSheetFemale - 1, countFomularAll).SetBorder(VTBorderStyle.Solid, VTBorderWeight.Thin, VTBorderIndex.All);
                sheetFemale.FitToPage = true;
                sheetFemale.Name = Utils.StripVNSignAndSpace(objSC.DisplayName) + " (HS Nu)";
            }

            #endregion

            #region //fill formular Toàn trường - sheet hoc sinh dan toc
            if (EthnicType > 0)
            {
                lstCountMark = new List<CountMarkCellValue>();
                formular = string.Empty;
                formular = "=SUM(";
                for (int i = 0; i < lstSumSheetEthnic.Count; i++)
                {
                    formular += "C" + lstSumSheetEthnic[i];
                    if (i < lstSumSheetEthnic.Count - 1)
                    {
                        formular += ",";
                    }

                    if (TypeExcelPDF == 2 && IsZip)
                    {
                        int countPupil = 0;
                        if (sheetEthnic.GetCellValue(lstSumSheetEthnic[i], 3) != null)
                        {
                            countPupil = Int32.Parse(sheetEthnic.GetCellValue(lstSumSheetEthnic[i], 3).ToString());
                        }
                        countT = 0;
                        if (sheetEthnic.GetCellValue(lstSumSheetEthnic[i], 4) != null)
                        {
                            countT = Int32.Parse(sheetEthnic.GetCellValue(lstSumSheetEthnic[i], 4).ToString());
                        }
                        countH = 0;
                        if (sheetEthnic.GetCellValue(lstSumSheetEthnic[i], 6) != null)
                        {
                            countH = Int32.Parse(sheetEthnic.GetCellValue(lstSumSheetEthnic[i], 6).ToString());
                        }
                        countC = 0;
                        if (sheetEthnic.GetCellValue(lstSumSheetEthnic[i], 8) != null)
                        {
                            countC = Int32.Parse(sheetEthnic.GetCellValue(lstSumSheetEthnic[i], 8).ToString());
                        }

                        lstCountMark.Add(new CountMarkCellValue() { Cell = 3, Count = countPupil });
                        lstCountMark.Add(new CountMarkCellValue() { Cell = 4, Count = countT });
                        lstCountMark.Add(new CountMarkCellValue() { Cell = 6, Count = countH });
                        lstCountMark.Add(new CountMarkCellValue() { Cell = 8, Count = countC });

                        if (sheetID == 1)
                        {
                            startCol = 10;
                            countMark = 0;
                            for (int j = 11; j > 0; j--)
                            {
                                if (sheetEthnic.GetCellValue(lstSumSheetEthnic[i], startCol) != null)
                                {
                                    countMark = Int32.Parse(sheetEthnic.GetCellValue(lstSumSheetEthnic[i], startCol).ToString());
                                }
                                lstCountMark.Add(new CountMarkCellValue() { Cell = startCol, Count = countMark });
                                startCol += 2;
                                countMark = 0;
                            }
                        } 
                    }   
                }
                formular += ")";
                sheetEthnic.SetFormulaValue(12, 3, formular);

                if (TypeExcelPDF == 2 && IsZip)
                {
                    totalPupil = lstCountMark.Where(x => x.Cell == 3).Count() > 0 ? lstCountMark.Where(x => x.Cell == 3).Select(x => x.Count).Sum() : 0;
                    int totalT = lstCountMark.Where(x => x.Cell == 4).Count() > 0 ? lstCountMark.Where(x => x.Cell == 4).Select(x => x.Count).Sum() : 0;
                    int totalH = lstCountMark.Where(x => x.Cell == 6).Count() > 0 ? lstCountMark.Where(x => x.Cell == 6).Select(x => x.Count).Sum() : 0;
                    int totalC = lstCountMark.Where(x => x.Cell == 8).Count() > 0 ? lstCountMark.Where(x => x.Cell == 8).Select(x => x.Count).Sum() : 0;

                    sheetEthnic.SetCellValue(12, 3, totalPupil);
                    sheetEthnic.SetCellValue(12, 4, totalT);
                    sheetEthnic.SetCellValue(12, 6, totalH);
                    sheetEthnic.SetCellValue(12, 8, totalC);

                    sheetEthnic.SetCellValue("E12", totalT > 0 ? (Math.Round((decimal)totalT / (decimal)totalPupil, 4) * 100) : 0);
                    sheetEthnic.SetCellValue("G12", totalH > 0 ? (Math.Round((decimal)totalH / (decimal)totalPupil, 4) * 100) : 0);
                    sheetEthnic.SetCellValue("I12", totalC > 0 ? (Math.Round((decimal)totalC / (decimal)totalPupil, 4) * 100) : 0);
                }
                else
                {
                    fPercent = "=ROUND(D12/IF(C12<=0,1,C12),4)*100";
                    sheetEthnic.SetCellValue("E12", fPercent);
                    fPercent = "=ROUND(F12/IF(C12<=0,1,C12),4)*100";
                    sheetEthnic.SetCellValue("G12", fPercent);
                    fPercent = "=ROUND(H12/IF(C12<=0,1,C12),4)*100";
                    sheetEthnic.SetCellValue("I12", fPercent);
                }
               
                if (sheetID == 1)
                {
                    //fill formular dong tong cong
                    startCol = 10;
                    int startRowFormular = 12;
                    for (int j = 11; j > 0; j--)
                    {
                        if (TypeExcelPDF == 2 && IsZip)
                        {
                            int totalOfMark = lstCountMark.Where(x => x.Cell == startCol).Count() > 0 ? lstCountMark.Where(x => x.Cell == startCol).Select(x => x.Count).Sum() : 0;
                            sheetEthnic.SetCellValue(startRowFormular, startCol, totalOfMark);
                            sheetEthnic.SetCellValue(startRowFormular, startCol + 1, totalOfMark > 0 ? (Math.Round((decimal)totalOfMark / (decimal)totalPupil, 4) * 100) : 0);
                        }
                        else
                        {
                            fPercent = "=ROUND(" + (UtilsBusiness.GetExcelColumnName(startCol) + startRowFormular) + "/IF(C" + startRowFormular + "<=0,1,C" + startRowFormular + "),4)*100";
                            sheetEthnic.SetFormulaValue(startRowFormular, startCol + 1, fPercent);
                        }                      
                        startCol += 2;
                    }
                }

                if(TypeExcelPDF  != 2 || !IsZip)
                {
                    for (int i = 4; i <= countFomularAll; i += 2)
                    {
                        formular = string.Empty;
                        formular = "=SUM(";
                        for (int j = 0; j < lstSumSheetEthnic.Count; j++)
                        {
                            formular += UtilsBusiness.GetExcelColumnName(i) + lstSumSheetEthnic[j];
                            if (j < lstSumSheetEthnic.Count - 1)
                            {
                                formular += ",";
                            }
                        }
                        formular += ")";
                        sheetEthnic.SetFormulaValue(12, i, formular);
                    }
                }
                
                sheetEthnic.GetRange(12, 1, startRowSheetEthnic - 1, countFomularAll).SetBorder(VTBorderStyle.Solid, VTBorderWeight.Thin, VTBorderIndex.All);
                sheetEthnic.FitToPage = true;
                sheetEthnic.Name = Utils.StripVNSignAndSpace(objSC.DisplayName) + " (HS dan toc)";
            }

            #endregion

            #region //fill formular Toàn trường - sheet hoc sinh nu dan toc
            if (FemaleEthnicType > 0)
            {
                lstCountMark = new List<CountMarkCellValue>();
                formular = string.Empty;
                formular = "=SUM(";
                for (int i = 0; i < lstSumSheetFemaleEthnic.Count; i++)
                {
                    formular += "C" + lstSumSheetFemaleEthnic[i];
                    if (i < lstSumSheetFemaleEthnic.Count - 1)
                    {
                        formular += ",";
                    }

                    if(TypeExcelPDF == 2 && IsZip)
                    {
                        int countPupil = 0;
                        if (sheetFemaleEthnic.GetCellValue(lstSumSheetFemaleEthnic[i], 3) != null)
                        {
                            countPupil = Int32.Parse(sheetFemaleEthnic.GetCellValue(lstSumSheetFemaleEthnic[i], 3).ToString());
                        }
                        countT = 0;
                        if (sheetFemaleEthnic.GetCellValue(lstSumSheetFemaleEthnic[i], 4) != null)
                        {
                            countT = Int32.Parse(sheetFemaleEthnic.GetCellValue(lstSumSheetFemaleEthnic[i], 4).ToString());
                        }
                        countH = 0;
                        if (sheetFemaleEthnic.GetCellValue(lstSumSheetFemaleEthnic[i], 6) != null)
                        {
                            countH = Int32.Parse(sheetFemaleEthnic.GetCellValue(lstSumSheetFemaleEthnic[i], 6).ToString());
                        }
                        countC = 0;
                        if (sheetFemaleEthnic.GetCellValue(lstSumSheetFemaleEthnic[i], 8) != null)
                        {
                            countC = Int32.Parse(sheetFemaleEthnic.GetCellValue(lstSumSheetFemaleEthnic[i], 8).ToString());
                        }

                        lstCountMark.Add(new CountMarkCellValue() { Cell = 3, Count = countPupil });
                        lstCountMark.Add(new CountMarkCellValue() { Cell = 4, Count = countT });
                        lstCountMark.Add(new CountMarkCellValue() { Cell = 6, Count = countH });
                        lstCountMark.Add(new CountMarkCellValue() { Cell = 8, Count = countC });

                        if (sheetID == 1)
                        {
                            startCol = 10;
                            countMark = 0;
                            for (int j = 11; j > 0; j--)
                            {
                                countMark = Int32.Parse(sheetFemaleEthnic.GetCellValue(lstSumSheetFemaleEthnic[i], startCol).ToString());
                                lstCountMark.Add(new CountMarkCellValue() { Cell = startCol, Count = countMark });
                                startCol += 2;
                            }
                        }  
                    }
                }

                formular += ")";
                sheetFemaleEthnic.SetFormulaValue(12, 3, formular);

                if (TypeExcelPDF == 2 && IsZip)
                {
                    totalPupil = lstCountMark.Where(x => x.Cell == 3).Count() > 0 ? lstCountMark.Where(x => x.Cell == 3).Select(x => x.Count).Sum() : 0;
                    int totalT = lstCountMark.Where(x => x.Cell == 4).Count() > 0 ? lstCountMark.Where(x => x.Cell == 4).Select(x => x.Count).Sum() : 0;
                    int totalH = lstCountMark.Where(x => x.Cell == 6).Count() > 0 ? lstCountMark.Where(x => x.Cell == 6).Select(x => x.Count).Sum() : 0;
                    int totalC = lstCountMark.Where(x => x.Cell == 8).Count() > 0 ? lstCountMark.Where(x => x.Cell == 8).Select(x => x.Count).Sum() : 0;

                    sheetFemaleEthnic.SetCellValue(12, 3, totalPupil);
                    sheetFemaleEthnic.SetCellValue(12, 4, totalT);
                    sheetFemaleEthnic.SetCellValue(12, 6, totalH);
                    sheetFemaleEthnic.SetCellValue(12, 8, totalC);

                    sheetFemaleEthnic.SetCellValue("E12", totalT > 0 ? (Math.Round((decimal)totalT / (decimal)totalPupil, 4) * 100) : 0);
                    sheetFemaleEthnic.SetCellValue("G12", totalH > 0 ? (Math.Round((decimal)totalH / (decimal)totalPupil, 4) * 100) : 0);
                    sheetFemaleEthnic.SetCellValue("I12", totalC > 0 ? (Math.Round((decimal)totalC / (decimal)totalPupil, 4) * 100) : 0);
                }
                else
                {
                    fPercent = "=ROUND(D12/IF(C12<=0,1,C12),4)*100";
                    sheetFemaleEthnic.SetCellValue("E12", fPercent);
                    fPercent = "=ROUND(F12/IF(C12<=0,1,C12),4)*100";
                    sheetFemaleEthnic.SetCellValue("G12", fPercent);
                    fPercent = "=ROUND(H12/IF(C12<=0,1,C12),4)*100";
                    sheetFemaleEthnic.SetCellValue("I12", fPercent);
                }

               
                if (sheetID == 1)
                {
                    //fill formular dong tong cong
                    startCol = 10;
                    int startRowFormular = 12;
                    for (int j = 11; j > 0; j--)
                    {
                        if (TypeExcelPDF == 2 && IsZip)
                        {
                            int totalOfMark = lstCountMark.Where(x => x.Cell == startCol).Count() > 0 ? lstCountMark.Where(x => x.Cell == startCol).Select(x => x.Count).Sum() : 0;
                            sheetFemaleEthnic.SetCellValue(startRowFormular, startCol, totalOfMark);
                            sheetFemaleEthnic.SetCellValue(startRowFormular, startCol + 1, totalOfMark > 0 ? (Math.Round((decimal)totalOfMark / (decimal)totalPupil, 4) * 100) : 0);
                        }
                        else
                        {
                            fPercent = "=ROUND(" + (UtilsBusiness.GetExcelColumnName(startCol) + startRowFormular) + "/IF(C" + startRowFormular + "<=0,1,C" + startRowFormular + "),4)*100";
                            sheetFemaleEthnic.SetFormulaValue(startRowFormular, startCol + 1, fPercent);
                        }                    
                        startCol += 2;
                    }
                }

                if (TypeExcelPDF != 2 || !IsZip)
                {
                    for (int i = 4; i <= countFomularAll; i += 2)
                    {
                        formular = string.Empty;
                        formular = "=SUM(";
                        for (int j = 0; j < lstSumSheetFemaleEthnic.Count; j++)
                        {
                            formular += UtilsBusiness.GetExcelColumnName(i) + lstSumSheetFemaleEthnic[j];
                            if (j < lstSumSheetFemaleEthnic.Count - 1)
                            {
                                formular += ",";
                            }
                        }
                        formular += ")";
                        sheetFemaleEthnic.SetFormulaValue(12, i, formular);
                    }
                }
                
                sheetFemaleEthnic.GetRange(12, 1, startRowSheetFemaleEthnic - 1, countFomularAll).SetBorder(VTBorderStyle.Solid, VTBorderWeight.Thin, VTBorderIndex.All);
                sheetFemaleEthnic.FitToPage = true;
                sheetFemaleEthnic.Name = Utils.StripVNSignAndSpace(objSC.DisplayName) + " (HS nu dan toc)";
            }

            #endregion

            sheet1.Delete();
            sheet2.Delete();

            return oBook;
            #endregion
        }

        private IVTWorksheet RenderInfo(IVTWorksheet sheet, SchoolProfile objSP, AcademicYear objAy, int sheetID, string SubjectName, int SemesterTypeID, int Type)
        {
            #region fill thong tin chung
            DateTime datetimeNow = DateTime.Now;
            string SemesterName = SemesterTypeID == SystemParamsInFile.GENERAL_SUMMED_EVALUATION_GKI ? "Giữa học kỳ I" :
                                  SemesterTypeID == SystemParamsInFile.GENERAL_SUMMED_EVALUATION_GKII ? "Giữa học kỳ II" :
                                  SemesterTypeID == SystemParamsInFile.GENERAL_SUMMED_EVALUATION_CKI ? "Cuối học kỳ I" : "Cuối học kỳ II";
            string Title = string.Empty;
            Title = (objSP.District != null ? objSP.District.DistrictName : "") + ", Ngày " + datetimeNow.Day + " tháng " + datetimeNow.Month + " năm " + datetimeNow.Year;
            sheet.SetCellValue("A2", objSP.SupervisingDept.SupervisingDeptName.ToUpper());
            sheet.SetCellValue("A3", objSP.SchoolName.ToUpper());
            sheet.SetCellValue(sheetID == 2 ? "G4" : "J4", Title);
            Title = string.Empty;
            Title = "Môn " + SubjectName + " - " + SemesterName + " - " + "Năm học " + objAy.DisplayTitle;
            sheet.SetCellValue(sheetID == 2 ? "A7" : "J7", Title.ToUpper());

            if (Type == 1) // sheet tat ca
            {
                Title = "THỐNG KÊ MÔN HỌC VÀ HOẠT ĐỘNG GIÁO DỤC";
            }
            else if (Type == 2) // Sheet hoc sinh nu
            {
                Title = "THỐNG KÊ MÔN HỌC VÀ HOẠT ĐỘNG GIÁO DỤC HỌC SINH NỮ";
            }
            else if (Type == 3) // Sheet hoc sinh dan toc
            {
                Title = "THỐNG KÊ MÔN HỌC VÀ HOẠT ĐỘNG GIÁO DỤC HỌC SINH DÂN TỘC";
            }
            else if (Type == 4) // Sheet hoc sinh nu dan toc
            {
                Title = "THỐNG KÊ MÔN HỌC VÀ HOẠT ĐỘNG GIÁO DỤC HỌC SINH NỮ DÂN TỘC";
            }
            sheet.SetCellValue(sheetID == 2 ? "A6" : "J6", Title);

            #endregion

            return sheet;
        }

        private IVTWorksheet RenderEducationLevel(IVTWorksheet sheet, int startRow, int STT, int countFomularAll)
        {
            sheet.SetCellValue(startRow, 1, STT);
            sheet.SetCellValue(startRow, 2, "Khối " + STT);
            sheet.GetRange(startRow, 1, startRow, countFomularAll).SetFontStyle(true, null, false, null, false, false);
            sheet.GetRange(startRow, 3, startRow, countFomularAll).FillColor(255, 204, 242);

            return sheet;
        }

        private IVTWorksheet RenderStatisticsClass(IVTWorksheet sheet, SubjectCat objSC, List<GroupPOC> GrouptmpByCriteria, List<RatedCommentPupilBO> lstRatedCommentPupil,
            int STT, int SemesterTypeID, int sheetID, int countFomularAll, bool isFillMark, int TypeExcelPDF, bool IsZip,  ref int startRowSheetEthnic)
        {
            int startCol = 0;
            int countT, countH, countC, countMark;
            string fPercent = "";
            string formular = "";
            for (int j = 0; j < GrouptmpByCriteria.Count; j++)
            {
                var objGrouptmpEthnic =GrouptmpByCriteria[j];

                countT = countH = countC = 0;
                sheet.SetCellValue(startRowSheetEthnic, 2, objGrouptmpEthnic.ClassName);
                sheet.SetCellValue(startRowSheetEthnic, 3, objGrouptmpEthnic.CountPupil);

                if (SemesterTypeID == SystemParamsInFile.GENERAL_SUMMED_EVALUATION_GKI || SemesterTypeID == SystemParamsInFile.GENERAL_SUMMED_EVALUATION_GKII)
                {
                    countT = lstRatedCommentPupil.Where(p => p.ClassID == objGrouptmpEthnic.ClassID && p.MiddleEvaluation == 1).Count();
                    countH = lstRatedCommentPupil.Where(p => p.ClassID == objGrouptmpEthnic.ClassID && p.MiddleEvaluation == 2).Count();
                    countC = lstRatedCommentPupil.Where(p => p.ClassID == objGrouptmpEthnic.ClassID && p.MiddleEvaluation == 3).Count();
                }
                else
                {
                    countT = lstRatedCommentPupil.Where(p => p.ClassID == objGrouptmpEthnic.ClassID && p.EndingEvaluation == 1).Count();
                    countH = lstRatedCommentPupil.Where(p => p.ClassID == objGrouptmpEthnic.ClassID && p.EndingEvaluation == 2).Count();
                    countC = lstRatedCommentPupil.Where(p => p.ClassID == objGrouptmpEthnic.ClassID && p.EndingEvaluation == 3).Count();
                }
                if (countT > 0)
                {
                    sheet.SetCellValue(startRowSheetEthnic, 4, countT);
                }
                if (countH > 0)
                {
                    sheet.SetCellValue(startRowSheetEthnic, 6, countH);
                }
                if (countC > 0)
                {
                    sheet.SetCellValue(startRowSheetEthnic, 8, countC);
                }
  
                if (TypeExcelPDF == 2 && IsZip)
                {
                    int totalT = 0;
                    int totalH = 0;
                    int totalC = 0;

                    if( sheet.GetCellValue(startRowSheetEthnic, 4) != null)
                    {
                        totalT = Int32.Parse(sheet.GetCellValue(startRowSheetEthnic, 4).ToString());
                    }
                    if (sheet.GetCellValue(startRowSheetEthnic, 6) != null)
                    {
                        totalH = Int32.Parse(sheet.GetCellValue(startRowSheetEthnic, 6).ToString());
                    }
                    if (sheet.GetCellValue(startRowSheetEthnic, 8) != null)
                    {
                        totalC = Int32.Parse(sheet.GetCellValue(startRowSheetEthnic, 8).ToString());
                    }

                    sheet.SetCellValue(startRowSheetEthnic, 5, totalT > 0 ? (Math.Round((decimal)totalT / (decimal)objGrouptmpEthnic.CountPupil, 4) * 100) : 0);
                    sheet.SetCellValue(startRowSheetEthnic, 7, totalH > 0 ? (Math.Round((decimal)totalH / (decimal)objGrouptmpEthnic.CountPupil, 4) * 100) : 0);
                    sheet.SetCellValue(startRowSheetEthnic, 9, totalC > 0 ? (Math.Round((decimal)totalC / (decimal)objGrouptmpEthnic.CountPupil, 4) * 100) : 0);
                }
                else
                {
                    fPercent = "=ROUND(D" + startRowSheetEthnic + "/IF(C" + startRowSheetEthnic + "<=0,1,C" + startRowSheetEthnic + "),4)*100";
                    sheet.SetFormulaValue(startRowSheetEthnic, 5, fPercent);
                    fPercent = "=ROUND(F" + startRowSheetEthnic + "/IF(C" + startRowSheetEthnic + "<=0,1,C" + startRowSheetEthnic + "),4)*100";
                    sheet.SetFormulaValue(startRowSheetEthnic, 7, fPercent);
                    fPercent = "=ROUND(H" + startRowSheetEthnic + "/IF(C" + startRowSheetEthnic + "<=0,1,C" + startRowSheetEthnic + "),4)*100";
                    sheet.SetFormulaValue(startRowSheetEthnic, 9, fPercent);
                }               

                if (sheetID == 1 && (isFillMark || ((STT == GlobalConstants.EDUCATION_LEVEL_ID4 || STT == GlobalConstants.EDUCATION_LEVEL_ID5)
                    && (objSC.Abbreviation.Equals("Tt") || objSC.Abbreviation.Equals("TV")))))
                {
                    startCol = 10;
                    for (int k = 10; k > 0; k--)
                    {
                        countMark = 0;
                        formular = string.Empty;
                        fPercent = string.Empty;
                        if (SemesterTypeID == SystemParamsInFile.GENERAL_SUMMED_EVALUATION_GKI || SemesterTypeID == SystemParamsInFile.GENERAL_SUMMED_EVALUATION_GKII)
                        {
                            countMark = lstRatedCommentPupil.Where(p => p.ClassID == objGrouptmpEthnic.ClassID && p.PeriodicMiddleMark == k).Count();
                        }
                        else
                        {
                            countMark = lstRatedCommentPupil.Where(p => p.ClassID == objGrouptmpEthnic.ClassID && p.PeriodicEndingMark == k).Count();
                        }
                        if (countMark > 0)
                        {
                            sheet.SetCellValue(startRowSheetEthnic, startCol, countMark);
                        }

                        if (TypeExcelPDF == 2 && IsZip)
                        {
                            int totalOfMark = 0;
                            if (sheet.GetCellValue(startRowSheetEthnic, startCol) != null)
                            {
                                totalOfMark = Int32.Parse(sheet.GetCellValue(startRowSheetEthnic, startCol).ToString());
                            }
                            sheet.SetCellValue(startRowSheetEthnic, startCol + 1, totalOfMark > 0 ? (Math.Round((decimal)totalOfMark / (decimal)objGrouptmpEthnic.CountPupil, 4) * 100) : 0);
                        }
                        else
                        {
                            fPercent = "=ROUND(" + (UtilsBusiness.GetExcelColumnName(startCol) + startRowSheetEthnic) + "/IF(C" + startRowSheetEthnic + "<=0,1,C" + startRowSheetEthnic + "),4)*100";
                            sheet.SetFormulaValue(startRowSheetEthnic, startCol + 1, fPercent);
                        }
                     
                        startCol += 2;
                    }
                    countMark = 0;
                    if (SemesterTypeID == SystemParamsInFile.GENERAL_SUMMED_EVALUATION_GKI || SemesterTypeID == SystemParamsInFile.GENERAL_SUMMED_EVALUATION_GKII)
                    {
                        countMark = lstRatedCommentPupil.Where(p => p.ClassID == objGrouptmpEthnic.ClassID && p.PeriodicMiddleMark < 5).Count();
                    }
                    else
                    {
                        countMark = lstRatedCommentPupil.Where(p => p.ClassID == objGrouptmpEthnic.ClassID && p.PeriodicEndingMark < 5).Count();
                    }
                    if (countMark > 0)
                    {
                        sheet.SetCellValue(startRowSheetEthnic, startCol, countMark);
                    }

                    if (TypeExcelPDF == 2 && IsZip)
                    {
                        int totalOfMark = 0;
                        if (sheet.GetCellValue(startRowSheetEthnic, startCol) != null)
                        {
                            totalOfMark = Int32.Parse(sheet.GetCellValue(startRowSheetEthnic, startCol).ToString());
                        }
                        sheet.SetCellValue(startRowSheetEthnic, startCol + 1, totalOfMark > 0 ? (Math.Round((decimal)totalOfMark / (decimal)objGrouptmpEthnic.CountPupil, 4) * 100) : 0);
                    }
                    else
                    {
                        fPercent = "=ROUND(" + (UtilsBusiness.GetExcelColumnName(startCol) + startRowSheetEthnic) + "/IF(C" + startRowSheetEthnic + "<=0,1,C" + startRowSheetEthnic + "),4)*100";
                        sheet.SetFormulaValue(startRowSheetEthnic, startCol + 1, fPercent);
                    }  
                }
                else if (sheetID == 1)
                {
                    sheet.GetRange(startRowSheetEthnic, 7, startRowSheetEthnic, countFomularAll).FillColor(255, 204, 242);
                }

                startRowSheetEthnic++;
            }

            return sheet;
        }

        private IVTWorksheet RenderSumEducation(IVTWorksheet sheet, int startRow, int sheetID, int countGrouptmp, int TypeExcelPDF, bool IsZip)
        {

            if (countGrouptmp > 0)
            {
                string formular = "";
                string fPercent = "";

                formular = "=SUM(C" + startRow + ":C" + (startRow + (countGrouptmp - 1)) + ")";
                sheet.SetFormulaValue(startRow - 1, 3, formular);
                formular = "=SUM(D" + startRow + ":D" + (startRow + (countGrouptmp - 1)) + ")";
                sheet.SetFormulaValue(startRow - 1, 4, formular);
                formular = "=SUM(F" + startRow + ":F" + (startRow + (countGrouptmp - 1)) + ")";
                sheet.SetFormulaValue(startRow - 1, 6, formular);
                formular = "=SUM(H" + startRow + ":H" + (startRow + (countGrouptmp - 1)) + ")";
                sheet.SetFormulaValue(startRow - 1, 8, formular);

                int totalPupil = 0;
                if (IsZip && TypeExcelPDF == 2)
                {                   
                    int totalT = 0;
                    int totalH = 0;
                    int totalC = 0;
                    if (sheet.GetCellValue(startRow - 1, 3) != null)
                    {
                        totalPupil = Int32.Parse(sheet.GetCellValue(startRow - 1, 3).ToString());
                    }
                    if (sheet.GetCellValue(startRow - 1, 4) != null)
                    {
                        totalT = Int32.Parse(sheet.GetCellValue(startRow - 1, 4).ToString());
                    }
                    if (sheet.GetCellValue(startRow - 1, 6) != null)
                    {
                        totalH = Int32.Parse(sheet.GetCellValue(startRow - 1, 6).ToString());
                    }
                    if (sheet.GetCellValue(startRow - 1, 8) != null)
                    {
                        totalC = Int32.Parse(sheet.GetCellValue(startRow - 1, 8).ToString());
                    }

                    sheet.SetCellValue(startRow - 1, 5, totalT > 0 ? (Math.Round((decimal)totalT / (decimal)totalPupil, 4) * 100) : 0);
                    sheet.SetCellValue(startRow - 1, 7, totalH > 0 ? (Math.Round((decimal)totalH / (decimal)totalPupil, 4) * 100) : 0);
                    sheet.SetCellValue(startRow - 1, 9, totalC > 0 ? (Math.Round((decimal)totalC / (decimal)totalPupil, 4) * 100) : 0);
                }
                else
                {
                    fPercent = "=ROUND(D" + (startRow - 1) + "/IF(C" + (startRow - 1) + "<=0,1,C" + (startRow - 1) + "),4)*100";
                    sheet.SetFormulaValue(startRow - 1, 5, fPercent);
                    fPercent = "=ROUND(F" + (startRow - 1) + "/IF(C" + (startRow - 1) + "<=0,1,C" + (startRow - 1) + "),4)*100";
                    sheet.SetFormulaValue(startRow - 1, 7, fPercent);
                    fPercent = "=ROUND(H" + (startRow - 1) + "/IF(C" + (startRow - 1) + "<=0,1,C" + (startRow - 1) + "),4)*100";
                    sheet.SetFormulaValue(startRow - 1, 9, fPercent);
                }          

                if (sheetID == 1)
                {
                    int startCol = 10;
                    for (int j = 11; j > 0; j--)
                    {
                        formular = "=SUM(" + UtilsBusiness.GetExcelColumnName(startCol) + startRow + ":" + UtilsBusiness.GetExcelColumnName(startCol) + (startRow + countGrouptmp - 1) + ")";
                        sheet.SetFormulaValue(startRow - 1, startCol, formular);

                        if (IsZip && TypeExcelPDF == 2)
                        {
                            int totalOfMark = 0;
                            if (sheet.GetCellValue(startRow - 1, startCol) != null)
                            {
                                totalOfMark = Int32.Parse(sheet.GetCellValue(startRow - 1, startCol).ToString());
                            }
                            sheet.SetCellValue(startRow - 1, startCol + 1, totalOfMark > 0 ? (Math.Round((decimal)totalOfMark / (decimal)totalPupil, 4) * 100) : 0);
                        }
                        else
                        {
                            fPercent = "=ROUND(" + (UtilsBusiness.GetExcelColumnName(startCol) + (startRow - 1)) + "/IF(C" + (startRow - 1) + "<=0,1,C" + (startRow - 1) + "),4)*100";
                            sheet.SetFormulaValue(startRow - 1, startCol + 1, fPercent);
                            
                        }
                        startCol += 2;
                    }
                }
            }

            return sheet;
        }

        private List<GroupPOC> ListGroupPOC(int Type, List<PupilOfClass> lstPupilOfClass)
        {
            // type = 1: tất cả
            // type = 2: Học sinh nữ
            // type = 3: Học sinh dân tộc
            // type = 4: Học sinh nữ dân tộc
            List<GroupPOC> lstResult = new List<GroupPOC>();

            var lstTemptPOC = lstPupilOfClass;
            if (Type == 2)
            {
                lstTemptPOC = lstTemptPOC.Where(x => x.PupilProfile.Genre == 0).ToList();
            }
            if (Type == 3)
            {
                lstTemptPOC = lstTemptPOC.Where(x => x.PupilProfile.EthnicID != SystemParamsInFile.ETHNIC_ID_KINH
                                                    && x.PupilProfile.EthnicID != SystemParamsInFile.ETHNIC_ID_NN
                                                    && x.PupilProfile.EthnicID.HasValue).ToList();
            }
            if (Type == 4)
            {
                lstTemptPOC = lstTemptPOC.Where(x => x.PupilProfile.EthnicID != SystemParamsInFile.ETHNIC_ID_KINH
                                                     && x.PupilProfile.EthnicID != SystemParamsInFile.ETHNIC_ID_NN
                                                     && x.PupilProfile.EthnicID.HasValue
                                                     && x.PupilProfile.Genre == 0).ToList();
            }

            lstResult = (from poc in lstTemptPOC
                         group poc by new
                         {
                             poc.ClassProfile.EducationLevelID,
                             poc.ClassID,
                             poc.ClassProfile.DisplayName,
                         } into g
                         select new GroupPOC()
                         {
                             EducationLevelID = g.Key.EducationLevelID,
                             ClassID = g.Key.ClassID,
                             ClassName = g.Key.DisplayName,
                             CountPupil = g.Count()
                         }).OrderBy(p => p.EducationLevelID).ThenBy(p => p.ClassName).ToList();

            return lstResult;
        }

        private List<RatedCommentPupilBO> GetListRatedCommentPupilBO(IDictionary<string, object> dicRated,
            AcademicYear objAy, List<int> lstClassID, IQueryable<PupilOfClass> iQueryPOC)
        {
            List<RatedCommentPupilBO> lstRatedCommentPupil = new List<RatedCommentPupilBO>();
            if (UtilsBusiness.IsMoveHistory(objAy))
            {
                lstRatedCommentPupil = (from rh in RatedCommentPupilHistoryBusiness.Search(dicRated)
                                        join pf in PupilProfileBusiness.All on rh.PupilID equals pf.PupilProfileID
                                        join pc in iQueryPOC on pf.PupilProfileID equals pc.PupilID
                                        where lstClassID.Contains(rh.ClassID)

                                        select new RatedCommentPupilBO
                                        {
                                            PupilID = rh.PupilID,
                                            ClassID = rh.ClassID,
                                            SubjectID = rh.SubjectID,
                                            SemesterID = rh.SemesterID,
                                            EvaluationID = rh.EvaluationID,
                                            PeriodicMiddleMark = rh.PeriodicMiddleMark,
                                            PeriodicEndingMark = rh.PeriodicEndingMark,
                                            PeriodicMiddleJudgement = rh.PeriodicMiddleJudgement,
                                            PeriodicEndingJudgement = rh.PeriodicEndingJudgement,
                                            MiddleEvaluation = rh.MiddleEvaluation,
                                            EndingEvaluation = rh.EndingEvaluation,
                                            Comment = rh.Comment,
                                            Genre = pf.Genre,
                                            EthnicID = pf.EthnicID,
                                            EducationLevelID = pc.ClassProfile.EducationLevelID
                                        }).ToList();
            }
            else
            {
                lstRatedCommentPupil = (from rh in RatedCommentPupilBusiness.Search(dicRated)
                                        join pf in PupilProfileBusiness.All on rh.PupilID equals pf.PupilProfileID
                                        join pc in iQueryPOC on pf.PupilProfileID equals pc.PupilID
                                        where lstClassID.Contains(rh.ClassID)
                                        select new RatedCommentPupilBO
                                        {
                                            PupilID = rh.PupilID,
                                            ClassID = rh.ClassID,
                                            SubjectID = rh.SubjectID,
                                            SemesterID = rh.SemesterID,
                                            EvaluationID = rh.EvaluationID,
                                            PeriodicMiddleMark = rh.PeriodicMiddleMark,
                                            PeriodicEndingMark = rh.PeriodicEndingMark,
                                            PeriodicMiddleJudgement = rh.PeriodicMiddleJudgement,
                                            PeriodicEndingJudgement = rh.PeriodicEndingJudgement,
                                            MiddleEvaluation = rh.MiddleEvaluation,
                                            EndingEvaluation = rh.EndingEvaluation,
                                            Comment = rh.Comment,
                                            Genre = pf.Genre,
                                            EthnicID = pf.EthnicID,
                                            EducationLevelID = pc.ClassProfile.EducationLevelID
                                        }).ToList();
            }

            return lstRatedCommentPupil;
        }

        public Stream CreateCapacityAndQualityReport(IDictionary<string, object> dic)
        {
            string reportCode = SystemParamsInFile.REPORT_CAPACITY_AND_QUALITY;
            ReportDefinition reportDef = ReportDefinitionBusiness.GetByCode(reportCode);
            string templatePath = ReportUtils.GetTemplatePath(reportDef);
            IVTWorkbook oBook = VTExport.OpenWorkbook(templatePath);
            this.CreateDataToTable(dic);
            List<MarkStatisticBO> lstMark = GetDataMarkStatisticForPrimary(dic);
            this.ExportPrimaryStatisticStream(oBook, lstMark, dic);//fill du lieu vao file
            return oBook.ToStream();
        }
        public ProcessedReport InsertProcessCapQuaReport(IDictionary<string, object> dic, Stream data)
        {
            string reportCode = SystemParamsInFile.REPORT_CAPACITY_AND_QUALITY;
            ReportDefinition reportDef = ReportDefinitionBusiness.GetByCode(reportCode);
            ProcessedReport pr = new ProcessedReport();
            pr.ReportCode = reportCode;
            pr.ProcessedDate = DateTime.Now;
            pr.InputParameterHashKey = ReportUtils.GetHashKey(dic);
            pr.ReportData = ReportUtils.Compress(data);
            int SemesterTypeID = Utils.GetInt(dic, "ReportTypeID");
            string SemesterName = SemesterTypeID == SystemParamsInFile.GENERAL_SUMMED_EVALUATION_GKI ? "GiuaHKI"
                                 : SemesterTypeID == SystemParamsInFile.GENERAL_SUMMED_EVALUATION_GKII ? "GiuaHKII"
                                 : SemesterTypeID == SystemParamsInFile.GENERAL_SUMMED_EVALUATION_CKI ? "CuoiHKI"
                                 : "CuoiHKII";

            //Tạo tên file
            string outputNamePattern = reportDef.OutputNamePattern;
            outputNamePattern = outputNamePattern.Replace("[SemesterType]", SemesterName);
            pr.ReportName = ReportUtils.RemoveSpecialCharacters(outputNamePattern) + "." + reportDef.OutputFormat;
            ProcessedReportParameterRepository.Insert(dic, pr);
            ProcessedReportRepository.Insert(pr);
            ProcessedReportRepository.Save();
            return pr;
        }
        private void CreateDataToTable(IDictionary<string, object> dic)
        {
            int provinceID = Utils.GetInt(dic, "ProvinceID");
            int districtID = Utils.GetInt(dic, "DistrictID");
            int schoolID = Utils.GetInt(dic, "SchoolID");
            int academicYearID = Utils.GetInt(dic, "AcademicYearID");
            int last2digitProvince = UtilsBusiness.GetPartionId(schoolID, 100);
            int year = Utils.GetInt(dic, "Year");
            string ReportCode = SystemParamsInFile.REPORT_CAPACITY_AND_QUALITY;
            int semester = Utils.GetInt(dic, "SemesterID");
            int educationLevelId = Utils.GetInt(dic, "EducationLevelID");
            int appliedLevel = Utils.GetInt(dic, "AppliedLevelID");
            bool isFemale = Utils.GetBool(dic, "IsFemale");
            bool isEthnic = Utils.GetBool(dic, "IsEthnic");
            bool isFemaleEthnic = Utils.GetBool(dic, "IsFemaleEthnic");
            int reportTypeId = Utils.GetInt(dic, "ReportTypeID");
            IDictionary<string, object> dicSearchClass = new Dictionary<string, object>()
            {
                {"SchoolID",schoolID},
                {"AcademicYearID",academicYearID},
                {"AppliedLevel",appliedLevel}
            };
            //lay tat ca cac lop can chay trong truong
            List<ClassProfile> lstCP = ClassProfileBusiness.Search(dicSearchClass).ToList();
            #region Xoa du lieu truoc khi tong hop
            IQueryable<MarkStatistic> iQMarkStatisticDelete = from m in MarkStatisticBusiness.AllNoTracking
                                                              where m.Last2DigitNumberProvince == last2digitProvince
                                                              && m.SchoolID == schoolID
                                                              && m.AcademicYearID == academicYearID
                                                              && m.Year == year
                                                              && m.AppliedLevel == appliedLevel
                                                              && m.Semester == semester
                                                              && m.ReportCode == ReportCode
                                                              select m;
            List<int> lstAdd = new List<int>();
            lstAdd.Add(0);//Tat ca
            if (isFemale)
            {
                lstAdd.Add(1);//Hoc sinh nu
            }
            if (isEthnic)
            {
                lstAdd.Add(2);//Hoc sinh dan toc
            }
            if (isFemaleEthnic)
            {
                lstAdd.Add(3);//Hoc sinh nu dan toc
            }
            iQMarkStatisticDelete = iQMarkStatisticDelete.Where(m => lstAdd.Contains(m.CriteriaReportID));
            List<MarkStatistic> lstMarkStatistic = iQMarkStatisticDelete.ToList();
            try
            {
                context.Configuration.AutoDetectChangesEnabled = false;
                context.Configuration.ValidateOnSaveEnabled = false;
                if (lstMarkStatistic != null && lstMarkStatistic.Count > 0)
                {
                    MarkStatisticBusiness.DeleteAll(lstMarkStatistic);
                    MarkTypeBusiness.Save();
                }
            }
            catch (Exception ex)
            {
                
                LogExtensions.ErrorExt(logger, DateTime.Now, "CreateDataToTable", "null", ex);
            }
            finally
            {
                context.Configuration.AutoDetectChangesEnabled = true;
                context.Configuration.ValidateOnSaveEnabled = true;
            }
            #endregion
            #region Run task
            int numTask = UtilsBusiness.GetNumTask;
            Task[] arrTask = new Task[numTask];
            for (int z = 0; z < numTask; z++)
            {
                int currentIndex = z;
                arrTask[z] = Task.Factory.StartNew(() =>
                {
                    #region run task
                    List<ClassProfile> lstReportRunTask = lstCP.Where(p => p.ClassProfileID % numTask == currentIndex).ToList();
                    if (lstReportRunTask != null && lstReportRunTask.Count > 0)
                    {
                        for (int j = 0; j < lstReportRunTask.Count; j++)
                        {
                            ClassProfile objCPTask = lstReportRunTask[j];
                            if (objCPTask == null)
                            {
                                continue;
                            }
                            for (int add = 0; add < lstAdd.Count; add++)
                            {
                                this.InsertCapQuaToTable(provinceID, districtID, schoolID, academicYearID, objCPTask.ClassProfileID, year, appliedLevel, semester, objCPTask.EducationLevelID, ReportCode, lstAdd[add], reportTypeId);
                            }

                        }
                    }
                    #endregion
                });
            }
            Task.WaitAll(arrTask);
            #endregion
        }
        private void InsertCapQuaToTable(int ProvinceID, int DistrictID, int SchoolID, int AcademicYearID, int ClassID, int Year, int AppliedLevel, int SemesterID, int EducationLevelID,
                                        string ReportCode, int CriteriaReportID, int ReportTypeID)
        {
            SMASEntities contenxt1 = new SMASEntities();
            using (OracleConnection conn = new OracleConnection(contenxt1.Database.Connection.ConnectionString))
            {
                try
                {
                    if (conn != null)
                    {
                        if (conn.State != ConnectionState.Open)
                        {
                            conn.Open();
                        }

                        using (OracleTransaction tran = conn.BeginTransaction())
                        {
                            try
                            {
                                OracleCommand command = new OracleCommand
                                {
                                    CommandType = CommandType.StoredProcedure,
                                    CommandText = "SP_SCHOOL_CAPQUA_STATISTIC",
                                    Connection = conn,
                                    CommandTimeout = 0
                                };
                                command.Parameters.Add("P_PROVINCE_ID", ProvinceID);
                                command.Parameters.Add("P_DISTRICT_ID", DistrictID);
                                command.Parameters.Add("P_SCHOOL_ID", SchoolID);
                                command.Parameters.Add("P_ACADEMIC_YEAR_ID", AcademicYearID);
                                command.Parameters.Add("P_YEAR", Year);
                                command.Parameters.Add("P_APPLIED_LEVEL", AppliedLevel);
                                command.Parameters.Add("P_EDUCATION_LEVEL_ID", EducationLevelID);
                                command.Parameters.Add("P_SEMESTER_ID", SemesterID);
                                command.Parameters.Add("P_REPORT_CODE", ReportCode);
                                command.Parameters.Add("P_CRITERIA_REPORT_ID", CriteriaReportID);
                                command.Parameters.Add("P_REPORT_TYPE", ReportTypeID);
                                command.Parameters.Add("P_CLASS_ID", ClassID);
                                command.ExecuteNonQuery();
                                tran.Commit();
                            }
                            catch (Exception)
                            {
                                tran.Rollback();
                            }
                            finally
                            {
                                tran.Dispose();
                            }
                        }
                    }
                }
                finally
                {
                    if (conn != null)
                    {
                        conn.Close();
                    }
                }
            }
        }
        private List<MarkStatisticBO> GetDataMarkStatisticForPrimary(IDictionary<string, object> dic)
        {
            int schoolID = Utils.GetInt(dic, "SchoolID");
            int academicYearID = Utils.GetInt(dic, "AcademicYearID");
            int last2digitProvince = UtilsBusiness.GetPartionId(schoolID, 100);
            string ReportCode = SystemParamsInFile.REPORT_CAPACITY_AND_QUALITY;
            int semester = Utils.GetInt(dic, "SemesterID");
            int appliedLevel = Utils.GetInt(dic, "AppliedLevelID");
            int reportTypeId = Utils.GetInt(dic, "ReportTypeID");
            bool isFemale = Utils.GetBool(dic, "IsFemale");
            bool isEthnic = Utils.GetBool(dic, "IsEthnic");
            bool isFemaleEthnic = Utils.GetBool(dic, "IsFemaleEthnic");
            List<int> lstAdd = new List<int>();
            lstAdd.Add(0);//Tat ca
            if (isFemale)
            {
                lstAdd.Add(1);//Hoc sinh nu
            }
            if (isEthnic)
            {
                lstAdd.Add(2);//Hoc sinh dan toc
            }
            if (isFemaleEthnic)
            {
                lstAdd.Add(3);//Hoc sinh nu dan toc
            }

            //Xoa du lieu truoc khi tong hop
            List<MarkStatisticBO> lstMark = (from m in MarkStatisticBusiness.AllNoTracking
                                             join cp in ClassProfileBusiness.AllNoTracking on m.ClassID equals cp.ClassProfileID
                                             where m.Last2DigitNumberProvince == last2digitProvince
                                             && m.SchoolID == schoolID
                                             && m.AcademicYearID == academicYearID
                                             && m.AppliedLevel == appliedLevel
                                             && m.Semester == semester
                                             && m.ReportCode == ReportCode
                                             && lstAdd.Contains(m.CriteriaReportID)
                                             //&& m.CriteriaReportID == P_CRITERIA_REPORT_ID
                                             select new MarkStatisticBO
                                             {
                                                 AcademicYearID = m.AcademicYearID,
                                                 AppliedLevel = m.AppliedLevel,
                                                 BelowAverage = m.BelowAverage,
                                                 CriteriaReportID = m.CriteriaReportID,
                                                 EducationLevelID = m.EducationLevelID,
                                                 ClassID = cp.ClassProfileID,
                                                 ClassName = cp.DisplayName,
                                                 OrderInClass = cp.OrderNumber,
                                                 MarkLevel00 = m.MarkLevel00,
                                                 MarkLevel01 = m.MarkLevel01,
                                                 MarkLevel02 = m.MarkLevel02,
                                                 MarkLevel03 = m.MarkLevel03,
                                                 MarkLevel04 = m.MarkLevel04,
                                                 MarkLevel05 = m.MarkLevel05,
                                                 MarkLevel06 = m.MarkLevel06,
                                                 MarkLevel07 = m.MarkLevel07,
                                                 MarkLevel08 = m.MarkLevel08,
                                                 MarkLevel09 = m.MarkLevel09,
                                                 MarkLevel10 = m.MarkLevel10,
                                                 MarkLevel11 = m.MarkLevel11,
                                                 MarkLevel12 = m.MarkLevel12,
                                                 MarkLevel13 = m.MarkLevel13,
                                                 MarkLevel14 = m.MarkLevel14,
                                                 MarkLevel15 = m.MarkLevel15,
                                                 MarkLevel16 = m.MarkLevel16,
                                                 MarkLevel17 = m.MarkLevel17,
                                                 MarkLevel18 = m.MarkLevel18,
                                                 MarkLevel19 = m.MarkLevel19,
                                                 MarkLevel20 = m.MarkLevel20,
                                                 MarkLevel21 = m.MarkLevel21,
                                                 MarkLevel22 = m.MarkLevel22,
                                                 MarkLevel23 = m.MarkLevel23,
                                                 MarkLevel24 = m.MarkLevel24,
                                                 MarkLevel25 = m.MarkLevel25,
                                                 MarkLevel26 = m.MarkLevel26,
                                                 MarkLevel27 = m.MarkLevel27,
                                                 MarkLevel28 = m.MarkLevel28,
                                                 MarkLevel29 = m.MarkLevel29,
                                                 MarkLevel30 = m.MarkLevel30,
                                                 MarkLevel31 = m.MarkLevel31,
                                                 MarkLevel32 = m.MarkLevel32,
                                                 MarkLevel33 = m.MarkLevel33,
                                                 MarkLevel34 = m.MarkLevel34,
                                                 MarkLevel35 = m.MarkLevel35,
                                                 MarkLevel36 = m.MarkLevel36,
                                                 MarkLevel37 = m.MarkLevel37,
                                                 MarkLevel38 = m.MarkLevel38,
                                                 MarkLevel39 = m.MarkLevel39,
                                                 MarkLevel40 = m.MarkLevel40,
                                                 MarkTotal = m.MarkTotal,
                                                 MarkType = m.MarkType,
                                                 OnAverage = m.OnAverage,
                                                 ProvinceID = m.ProvinceID,
                                                 ProcessedDate = m.ProcessedDate,
                                                 PupilTotal = m.PupilTotal,
                                                 ReportCode = m.ReportCode,
                                                 SchoolID = m.SchoolID,
                                                 Semester = m.Semester,
                                                 Year = m.Year
                                             }).ToList();
            return lstMark;
        }
        private void ExportPrimaryStatisticStream(IVTWorkbook oBook, List<MarkStatisticBO> lstMark, IDictionary<string, object> dic)
        {
            int StartRow = 10;
            int supervisingDeptID = Utils.GetInt(dic, "SupervisingDeptID");
            int schoolID = Utils.GetInt(dic, "SchoolID");
            int academicYearID = Utils.GetInt(dic, "AcademicYearID");
            int last2digitProvince = UtilsBusiness.GetPartionId(schoolID, 100);
            int year = Utils.GetInt(dic, "Year");
            int semester = Utils.GetInt(dic, "Semester");
            int appliedLevel = Utils.GetInt(dic, "AppliedLevel");
            bool isFemale = Utils.GetBool(dic, "IsFemale");
            bool isEthnic = Utils.GetBool(dic, "IsEthnic");
            bool isFemaleEthnic = Utils.GetBool(dic, "IsFemaleEthnic");
            int reportTypeId = Utils.GetInt(dic, "ReportTypeID");
            SupervisingDept supervisingDept = SupervisingDeptBusiness.Find(supervisingDeptID);
            IVTWorksheet sheet = null;
            List<MarkStatisticBO> lstMarkExport = new List<MarkStatisticBO>();
            MarkStatisticBO objMark;
            List<int> lstClassID = new List<int>();
            List<int?> lstEducationLevelID = new List<int?>();
            int? EducationLevelID;
            string title2 = string.Empty;
            string title1 = string.Empty;
            int row = 0;
            int orderId = 1;
            int rowEducationLevel = 9;
            List<int> lstSumClass;
            string strFemaleEthnic = string.Empty;
            List<int> lstAdd = new List<int>();
            lstAdd.Add(0);//Tat ca
            if (isFemale)
            {
                lstAdd.Add(1);//Hoc sinh nu
            }
            if (isEthnic)
            {
                lstAdd.Add(2);//Hoc sinh dan toc
            }
            if (isFemaleEthnic)
            {
                lstAdd.Add(3);//Hoc sinh nu dan toc
            }
            IVTWorksheet curentSheet = oBook.GetSheet(1);
            IVTWorksheet sheetCopy = oBook.GetSheet(2);
            int addNum = 0;
            for (int add = 0; add < lstAdd.Count; add++)
            {
                addNum = lstAdd[add];
                if (addNum == 0)
                {
                    strFemaleEthnic = "";
                }
                else if (addNum == 1)
                {
                    strFemaleEthnic = "HỌC SINH NỮ";
                }
                else if (addNum == 2)
                {
                    strFemaleEthnic = "HỌC SINH DÂN TỘC";
                }
                else if (addNum == 3)
                {
                    strFemaleEthnic = "HỌC SINH NỮ DÂN TỘC";
                }
                title1 = string.Format("THỐNG KÊ NĂNG LỰC, PHẨM CHẤT {0}", strFemaleEthnic);
                title2 = "{0}" + (reportTypeId == 1 ? " GIỮA HỌC KỲ I " : reportTypeId == 2 ? " GIỮA HỌC KỲ II " : reportTypeId == 3 ? " CUỐI HỌC KỲ I " : " CUỐI HỌC KỲ II ") + "NĂM HỌC " + year + "-" + (year + 1);
                sheet = oBook.CopySheetToLast(curentSheet);
                sheet.SetCellValue("A6", title1);
                sheet.SetCellValue("A7", String.Format(title2, String.Empty));
                #region fill du lieu nang luc pham chat
                lstSumClass = new List<int>();
                lstEducationLevelID = lstMark.Where(p => p.CriteriaReportID == addNum).OrderBy(d => d.EducationLevelID).Select(m => m.EducationLevelID).Distinct().ToList();
                StartRow = 14;
                row = StartRow;
                rowEducationLevel = 13;
                orderId = 1;
                lstSumClass.Add(rowEducationLevel);
                for (int d = 0; d < lstEducationLevelID.Count; d++)
                {
                    EducationLevelID = lstEducationLevelID[d];
                    orderId = 1;
                    if (d > 0)
                    {
                        sheet.CopyPasteSameRowHeigh(sheetCopy.GetRange("A3", "X3"), rowEducationLevel);
                    }
                    lstMarkExport = lstMark.Where(m => m.EducationLevelID == EducationLevelID && m.CriteriaReportID == addNum).OrderBy(m => m.OrderInClass).ThenBy(m => m.ClassName).ToList();
                    for (int j = 0; j < lstMarkExport.Count; j++)
                    {
                        objMark = lstMarkExport[j];
                        if (j == 0)
                        {
                            sheet.SetCellValue(rowEducationLevel, 2, "Khối " + EducationLevelID);
                        }
                        //if (j > 0 || (j == 0 && d > 0))
                        //{
                        sheet.CopyPasteSameRowHeigh(sheetCopy.GetRange("A4", "X4"), row);
                        //}
                        FillCAPQUAToExcel(sheet, objMark, row, orderId);
                        //sheet.GetRange(row, 1, row, 24).SetBorder(VTBorderStyle.Solid, VTBorderIndex.All);
                        row = row + 1;
                        orderId = orderId + 1;
                    }

                    if (row > StartRow)
                    {
                        SetFormulaEducationSAE(sheet, rowEducationLevel + 1, rowEducationLevel + lstMarkExport.Count, 3, 25);//fill tong so 
                    }
                    if (d > 0)
                    {
                        lstSumClass.Add(rowEducationLevel);
                    }
                    rowEducationLevel = row;
                    row = row + 1;
                }
                SetFormulaTotal(sheet, 12, lstSumClass, 3, 25);//fill cot toan so
                SchoolProfile objSP = SchoolProfileBusiness.Find(schoolID);
                sheet.SetCellValue(2, 1, UtilsBusiness.GetSupervisingDeptName(objSP.SchoolProfileID, appliedLevel).ToUpper());
                sheet.SetCellValue(3, 1, objSP.SchoolName.ToUpper());
                string provinceName = (objSP.District != null ? objSP.District.DistrictName : "");
                DateTime date = DateTime.Now;
                string day = "ngày " + date.Day.ToString() + " tháng " + date.Month.ToString() + " năm " + date.Year.ToString();
                string provinceAndDate = provinceName + ", " + day;
                sheet.SetCellValue("M4", provinceAndDate);
                sheet.GetRange(row + 1, 19, row + 1, 24).Merge();
                sheet.GetRange(row + 1, 19, row + 1, 19).SetFontStyle(true, null, false, 11, false, false);
                sheet.GetRange(row + 1, 19, row + 1, 19).SetFontName("Times New Roman", null);
                sheet.SetCellValue(row + 1, 19, "Người lập báo cáo");

                sheet.FitAllColumnsOnOnePage = true;
                sheet.Name = addNum == 1 ? "HS_Nu" : addNum == 2 ? "HS_DT" : addNum == 3 ? "HS_Nu_DT" : "Tatca";
            }
            curentSheet.Delete();
            sheetCopy.Delete();
                #endregion
        }
        private void FillCAPQUAToExcel(IVTWorksheet sheet, MarkStatisticBO objMark, int row, int OrderId)
        {
            string formular = string.Empty;
            sheet.SetCellValue(row, 1, OrderId);
            sheet.SetCellValue(row, 2, objMark.ClassName);
            sheet.SetCellValue(row, 3, objMark.PupilTotal);
            sheet.SetCellValue(row, 4, objMark.MarkLevel01 != null ? objMark.MarkLevel01 : 0);
            sheet.SetCellValue(row, 5, objMark.MarkLevel02 != null ? objMark.MarkLevel02 : 0);
            sheet.SetCellValue(row, 6, objMark.MarkLevel03 != null ? objMark.MarkLevel03 : 0);
            sheet.SetCellValue(row, 7, objMark.MarkLevel04 != null ? objMark.MarkLevel04 : 0);
            sheet.SetCellValue(row, 8, objMark.MarkLevel05 != null ? objMark.MarkLevel05 : 0);
            sheet.SetCellValue(row, 9, objMark.MarkLevel06 != null ? objMark.MarkLevel06 : 0);
            sheet.SetCellValue(row, 10, objMark.MarkLevel07 != null ? objMark.MarkLevel07 : 0);
            sheet.SetCellValue(row, 11, objMark.MarkLevel08 != null ? objMark.MarkLevel08 : 0);
            sheet.SetCellValue(row, 12, objMark.MarkLevel09 != null ? objMark.MarkLevel09 : 0);
            sheet.SetCellValue(row, 13, objMark.MarkLevel10 != null ? objMark.MarkLevel10 : 0);
            sheet.SetCellValue(row, 14, objMark.MarkLevel11 != null ? objMark.MarkLevel11 : 0);
            sheet.SetCellValue(row, 15, objMark.MarkLevel12 != null ? objMark.MarkLevel12 : 0);
            sheet.SetCellValue(row, 16, objMark.MarkLevel13 != null ? objMark.MarkLevel13 : 0);
            sheet.SetCellValue(row, 17, objMark.MarkLevel14 != null ? objMark.MarkLevel14 : 0);
            sheet.SetCellValue(row, 18, objMark.MarkLevel15 != null ? objMark.MarkLevel15 : 0);
            sheet.SetCellValue(row, 19, objMark.MarkLevel16 != null ? objMark.MarkLevel16 : 0);
            sheet.SetCellValue(row, 20, objMark.MarkLevel17 != null ? objMark.MarkLevel17 : 0);
            sheet.SetCellValue(row, 21, objMark.MarkLevel18 != null ? objMark.MarkLevel18 : 0);
            sheet.SetCellValue(row, 22, objMark.MarkLevel19 != null ? objMark.MarkLevel19 : 0);
            sheet.SetCellValue(row, 23, objMark.MarkLevel20 != null ? objMark.MarkLevel20 : 0);
            sheet.SetCellValue(row, 24, objMark.MarkLevel21 != null ? objMark.MarkLevel21 : 0);
        }
        private void SetFormulaEducationSAE(IVTWorksheet sheet, int fromRow, int toRow, int fromColumn, int toColumn)
        {
            VTVector fromCell;
            VTVector toCell;
            string sumValue;
            for (int i = fromColumn; i < toColumn; i++)
            {
                fromCell = new VTVector(fromRow, i);
                toCell = new VTVector(toRow, i);
                sumValue = String.Format("=SUM({0}:{1})", fromCell.ToString(), toCell.ToString());
                sheet.SetFormulaValue(fromRow - 1, i, sumValue);
            }
        }
        private void SetFormulaTotal(IVTWorksheet sheet, int fromRow, List<int> lsttoRow, int fromColumn, int toColumn)
        {
            VTVector fromCell;
            string sumValue = "";
            int toRow = 0;
            List<string> lstVal;
            for (int i = fromColumn; i < toColumn; i++)
            {
                sumValue = "";
                lstVal = new List<string>();
                for (int row = 0; row < lsttoRow.Count; row++)
                {
                    toRow = lsttoRow[row];
                    fromCell = new VTVector(toRow, i);
                    lstVal.Add(fromCell.ToString());
                }
                if (lstVal != null && lstVal.Count > 0)
                {
                    sumValue = String.Format("=SUM({0})", String.Join(",", lstVal.ToArray()));
                    sheet.SetFormulaValue(fromRow, i, sumValue);
                }
            }
        }
        #region Module SMAS_MOBILE
        public void InsertOrUpdateToMobile(RatedCommentPupilBO objRatedCommentPupilBO, IDictionary<string, object> dic)
        {
            try
            {
                SetAutoDetectChangesEnabled(false);
                int SchoolID = Utils.GetInt(dic, "SchoolID");
                int AcademicYearID = Utils.GetInt(dic, "AcademicYearID");
                int ClassID = Utils.GetInt(dic, "ClassID");
                int SubjectID = Utils.GetInt(dic, "SubjectID");
                int SemesterID = Utils.GetInt(dic, "SemesterID");
                int PupilID = Utils.GetInt(dic, "PupilID");
                int PartionID = UtilsBusiness.GetPartionId(SchoolID);
                int TypeID = Utils.GetInt(dic, "TypeID");
                string TitleMark = Utils.GetString(dic, "TitleMark");
                AcademicYear objAy = AcademicYearBusiness.Find(AcademicYearID);
                List<RatedCommentPupil> lstRatedCommentPupilDB = new List<RatedCommentPupil>();
                IQueryable<RatedCommentPupil> iqRatedCommentDB = (from r in RatedCommentPupilBusiness.All
                                                                  where r.SchoolID == SchoolID
                                                                  && r.AcademicYearID == AcademicYearID
                                                                  && r.LastDigitSchoolID == PartionID
                                                                  && r.SemesterID == SemesterID
                                                                  && r.ClassID == ClassID
                                                                  && r.PupilID == PupilID
                                                                  select r);
                if (TypeID == GlobalConstants.EVALUATION_SUBJECT_AND_EDUCTIONGROUP)
                {
                    iqRatedCommentDB = iqRatedCommentDB.Where(p => p.EvaluationID == GlobalConstants.EVALUATION_SUBJECT_AND_EDUCTIONGROUP && p.SubjectID == SubjectID);
                }
                else
                {
                    iqRatedCommentDB = iqRatedCommentDB.Where(p => p.EvaluationID == GlobalConstants.EVALUATION_CAPACITY || p.EvaluationID == GlobalConstants.EVALUATION_QUALITY);
                }
                lstRatedCommentPupilDB = iqRatedCommentDB.ToList();
                List<RatedCommentPupil> lstInsert = new List<RatedCommentPupil>();
                RatedCommentPupil objDB = null;
                RatedCommentPupil objInsert = null;
                // lay danh sach khoa danh gia
                List<int> lstClassID = new List<int>();
                lstClassID.Add(ClassID);
                IDictionary<string, object> dicLockMark = new Dictionary<string, object>()
                {
                    {"SchoolID",SchoolID},
                    {"AcademicYearID",AcademicYearID},
                    {"lstClassID",lstClassID},
                    {"EvaluationID",TypeID}
                };
                string strTitleLog = string.Empty;
                string strLockMarkTitle = string.Empty;
                List<LockRatedCommentPupil> lstLockRated = LockRatedCommentPupilBusiness.Search(dicLockMark).ToList();
                List<LockRatedCommentPupil> lstLockRatedtmp = new List<LockRatedCommentPupil>();
                LockRatedCommentPupil objLockTitle = null;
                bool isInsertLog = false;
                #region Mon hoc va HDGD
                if (TypeID == GlobalConstants.EVALUATION_SUBJECT_AND_EDUCTIONGROUP)//Mon hoc va HDGD
                {
                    objLockTitle = lstLockRated.Where(p => p.SubjectID == objRatedCommentPupilBO.SubjectID && p.ClassID == objRatedCommentPupilBO.ClassID).FirstOrDefault();
                    strLockMarkTitle = objLockTitle != null ? objLockTitle.LockTitle : "";
                    objDB = lstRatedCommentPupilDB.Where(p => p.PupilID == objRatedCommentPupilBO.PupilID && p.SubjectID == objRatedCommentPupilBO.SubjectID && p.ClassID == objRatedCommentPupilBO.ClassID).FirstOrDefault();
                    if (objDB == null)
                    {
                        objInsert = new RatedCommentPupil();
                        objInsert.SchoolID = objRatedCommentPupilBO.SchoolID;
                        objInsert.AcademicYearID = objRatedCommentPupilBO.AcademicYearID;
                        objInsert.ClassID = objRatedCommentPupilBO.ClassID;
                        objInsert.PupilID = objRatedCommentPupilBO.PupilID;
                        objInsert.SemesterID = objRatedCommentPupilBO.SemesterID;
                        objInsert.EvaluationID = objRatedCommentPupilBO.EvaluationID;
                        objInsert.RetestMark = objRatedCommentPupilBO.RetestMark;
                        objInsert.EvaluationRetraning = objRatedCommentPupilBO.EvaluationRetraning;
                        objInsert.FullNameComment = objRatedCommentPupilBO.FullNameComment;
                        objInsert.LastDigitSchoolID = UtilsBusiness.GetPartionId(SchoolID);
                        objInsert.LogChangeID = objRatedCommentPupilBO.LogChangeID;
                        objInsert.CreateTime = DateTime.Now;
                        objInsert.UpdateTime = DateTime.Now;
                        #region Mon hoc & HDGD
                        objInsert.SubjectID = objRatedCommentPupilBO.SubjectID;
                        if (objRatedCommentPupilBO.isCommenting == 0)
                        {
                            if (objRatedCommentPupilBO.PeriodicMiddleMark > 0 && !strLockMarkTitle.Contains("KTGK" + SemesterID) && "KTGK".Equals(TitleMark))
                            {
                                objInsert.PeriodicMiddleMark = objRatedCommentPupilBO.PeriodicMiddleMark;
                                isInsertLog = true;
                            }
                            if (objRatedCommentPupilBO.MiddleEvaluation > 0 && !strLockMarkTitle.Contains("DGGK" + SemesterID) && "DGGK".Equals(TitleMark))
                            {
                                objInsert.MiddleEvaluation = objRatedCommentPupilBO.MiddleEvaluation;
                                isInsertLog = true;
                            }
                            if (objRatedCommentPupilBO.PeriodicEndingMark > 0 && !strLockMarkTitle.Contains("KTCK" + SemesterID) && "KTCK".Equals(TitleMark))
                            {
                                objInsert.PeriodicEndingMark = objRatedCommentPupilBO.PeriodicEndingMark;
                                isInsertLog = true;
                            }
                            if (objRatedCommentPupilBO.EndingEvaluation > 0 && !strLockMarkTitle.Contains("DGCK" + SemesterID) && "DGCK".Equals(TitleMark))
                            {
                                objInsert.EndingEvaluation = objRatedCommentPupilBO.EndingEvaluation;
                                isInsertLog = true;
                            }
                        }
                        else
                        {
                            if (objRatedCommentPupilBO.MiddleEvaluation > 0 && !strLockMarkTitle.Contains("DGGK" + SemesterID) && "DGGK".Equals(TitleMark))
                            {
                                objInsert.MiddleEvaluation = objRatedCommentPupilBO.MiddleEvaluation;
                                isInsertLog = true;
                            }
                            if (objRatedCommentPupilBO.EndingEvaluation > 0 && !strLockMarkTitle.Contains("DGCK" + SemesterID) && "DGCK".Equals(TitleMark))
                            {
                                objInsert.EndingEvaluation = objRatedCommentPupilBO.EndingEvaluation;
                                isInsertLog = true;
                            }
                        }
                        if (!string.IsNullOrEmpty(objRatedCommentPupilBO.Comment))
                        {
                            objInsert.Comment = objRatedCommentPupilBO.Comment;
                            isInsertLog = true;
                        }
                        #endregion
                        if (isInsertLog)
                        {
                            lstInsert.Add(objInsert);
                        }
                    }
                    else
                    {
                        #region Mon hoc & HDGD
                        if (objRatedCommentPupilBO.isCommenting == 0)
                        {
                            if (objDB.PeriodicMiddleMark != objRatedCommentPupilBO.PeriodicMiddleMark && "KTGK".Equals(TitleMark) && !strLockMarkTitle.Contains("KTGK" + SemesterID))
                            {
                                objDB.PeriodicMiddleMark = objRatedCommentPupilBO.PeriodicMiddleMark;
                                isInsertLog = true;
                            }
                            if (objDB.MiddleEvaluation != objRatedCommentPupilBO.MiddleEvaluation && "DGGK".Equals(TitleMark) && !strLockMarkTitle.Contains("DGGK" + SemesterID))
                            {
                                objDB.MiddleEvaluation = objRatedCommentPupilBO.MiddleEvaluation;
                                isInsertLog = true;
                            }
                            if (objDB.PeriodicEndingMark != objRatedCommentPupilBO.PeriodicEndingMark && "KTCK".Equals(TitleMark) && !strLockMarkTitle.Contains("KTCK" + SemesterID))
                            {
                                objDB.PeriodicEndingMark = objRatedCommentPupilBO.PeriodicEndingMark;
                                isInsertLog = true;
                            }
                            if (objDB.EndingEvaluation != objRatedCommentPupilBO.EndingEvaluation && "DGCK".Equals(TitleMark) && !strLockMarkTitle.Contains("DGCK" + SemesterID))
                            {
                                objDB.EndingEvaluation = objRatedCommentPupilBO.EndingEvaluation;
                                isInsertLog = true;
                            }
                        }
                        else
                        {
                            if (objDB.MiddleEvaluation != objRatedCommentPupilBO.MiddleEvaluation && "DGGK".Equals(TitleMark) && !strLockMarkTitle.Contains("DGGK" + SemesterID))
                            {
                                objDB.MiddleEvaluation = objRatedCommentPupilBO.MiddleEvaluation;
                                isInsertLog = true;
                            }
                            if (objDB.EndingEvaluation != objRatedCommentPupilBO.EndingEvaluation && "DGCK".Equals(TitleMark) && !strLockMarkTitle.Contains("DGCK" + SemesterID))
                            {
                                objDB.EndingEvaluation = objRatedCommentPupilBO.EndingEvaluation;
                                isInsertLog = true;
                            }
                        }
                        if ("NX".Equals(TitleMark) && (!string.IsNullOrEmpty(objDB.Comment) && string.IsNullOrEmpty(objRatedCommentPupilBO.Comment))
                                    || (string.IsNullOrEmpty(objDB.Comment) && !string.IsNullOrEmpty(objRatedCommentPupilBO.Comment))
                                    || (!string.IsNullOrEmpty(objDB.Comment) && !string.IsNullOrEmpty(objRatedCommentPupilBO.Comment)))
                        {
                            if (objDB.Comment != objRatedCommentPupilBO.Comment)
                            {
                                objDB.Comment = objRatedCommentPupilBO != null && !string.IsNullOrEmpty(objRatedCommentPupilBO.Comment) ? objRatedCommentPupilBO.Comment.Trim() : "";
                                isInsertLog = true;
                            }
                        }
                        if (isInsertLog)
                        {
                            objDB.UpdateTime = DateTime.Now;
                            objDB.LogChangeID = objRatedCommentPupilBO.LogChangeID;
                            RatedCommentPupilBusiness.Update(objDB);
                        }
                        #endregion
                    }
                }
                #endregion
                #region Nang luc pham chat
                else//Nang luc pham chat
                {

                    lstLockRatedtmp = lstLockRated.Where(p => p.ClassID == ClassID).ToList();
                    for (int j = 0; j < lstLockRatedtmp.Count; j++)
                    {
                        objLockTitle = lstLockRatedtmp[j];
                        strLockMarkTitle += objLockTitle != null ? objLockTitle.LockTitle + "," : string.Empty;
                    }
                    isInsertLog = false;
                    objDB = lstRatedCommentPupilDB.Where(p => p.PupilID == objRatedCommentPupilBO.PupilID && p.ClassID == objRatedCommentPupilBO.ClassID && p.SubjectID == objRatedCommentPupilBO.SubjectID && p.EvaluationID == objRatedCommentPupilBO.EvaluationID).FirstOrDefault();
                    if (objDB == null)
                    {
                        objInsert = new RatedCommentPupil();
                        objInsert.SchoolID = objRatedCommentPupilBO.SchoolID;
                        objInsert.AcademicYearID = objRatedCommentPupilBO.AcademicYearID;
                        objInsert.ClassID = objRatedCommentPupilBO.ClassID;
                        objInsert.PupilID = objRatedCommentPupilBO.PupilID;
                        objInsert.SemesterID = objRatedCommentPupilBO.SemesterID;
                        objInsert.EvaluationID = objRatedCommentPupilBO.EvaluationID;
                        objInsert.RetestMark = objRatedCommentPupilBO.RetestMark;
                        objInsert.EvaluationRetraning = objRatedCommentPupilBO.EvaluationRetraning;
                        objInsert.FullNameComment = objRatedCommentPupilBO.FullNameComment;
                        objInsert.SubjectID = objRatedCommentPupilBO.SubjectID;
                        objInsert.LastDigitSchoolID = UtilsBusiness.GetPartionId(SchoolID);
                        objInsert.LogChangeID = objRatedCommentPupilBO.LogChangeID;
                        objInsert.CreateTime = DateTime.Now;
                        objInsert.UpdateTime = DateTime.Now;
                        if (objRatedCommentPupilBO.EvaluationID == 2)
                        {
                            if (objRatedCommentPupilBO.CapacityMiddleEvaluation > 0 && TitleMark.IndexOf("NLGK") == 0 && !strLockMarkTitle.Contains("NLGK" + SemesterID))
                            {
                                objInsert.CapacityMiddleEvaluation = objRatedCommentPupilBO.CapacityMiddleEvaluation;
                                isInsertLog = true;
                            }
                            if (objRatedCommentPupilBO.CapacityEndingEvaluation > 0 && TitleMark.IndexOf("NLCK") == 0 && !strLockMarkTitle.Contains("NLCK" + SemesterID))
                            {
                                objInsert.CapacityEndingEvaluation = objRatedCommentPupilBO.CapacityEndingEvaluation;
                                isInsertLog = true;
                            }
                        }
                        else
                        {
                            if (objRatedCommentPupilBO.QualityMiddleEvaluation > 0 && TitleMark.IndexOf("PCGK") == 0 && !strLockMarkTitle.Contains("PCGK" + SemesterID))
                            {
                                objInsert.QualityMiddleEvaluation = objRatedCommentPupilBO.QualityMiddleEvaluation;
                                isInsertLog = true;
                            }
                            if (objRatedCommentPupilBO.QualityEndingEvaluation > 0 && TitleMark.IndexOf("PCCK") == 0 && !strLockMarkTitle.Contains("PCCK" + SemesterID))
                            {
                                objInsert.QualityEndingEvaluation = objRatedCommentPupilBO.QualityEndingEvaluation;
                                isInsertLog = true;
                            }
                        }

                        if (!string.IsNullOrEmpty(objRatedCommentPupilBO.Comment))
                        {
                            objInsert.Comment = objRatedCommentPupilBO.Comment;
                            isInsertLog = true;
                        }
                        if (isInsertLog)
                        {
                            lstInsert.Add(objInsert);
                        }
                    }
                    else
                    {
                        if (objRatedCommentPupilBO.EvaluationID == 2)
                        {
                            if (objDB.CapacityMiddleEvaluation != objRatedCommentPupilBO.CapacityMiddleEvaluation && TitleMark.IndexOf("NLGK") == 0 && !strLockMarkTitle.Contains("NLGK" + SemesterID))
                            {
                                objDB.CapacityMiddleEvaluation = objRatedCommentPupilBO.CapacityMiddleEvaluation;
                                isInsertLog = true;
                            }
                            if (objDB.CapacityEndingEvaluation != objRatedCommentPupilBO.CapacityEndingEvaluation && TitleMark.IndexOf("NLCK") == 0 && !strLockMarkTitle.Contains("NLCK" + SemesterID))
                            {
                                objDB.CapacityEndingEvaluation = objRatedCommentPupilBO.CapacityEndingEvaluation;
                                isInsertLog = true;
                            }
                        }
                        else
                        {
                            if (objDB.QualityMiddleEvaluation != objRatedCommentPupilBO.QualityMiddleEvaluation && TitleMark.IndexOf("PCGK") == 0 && !strLockMarkTitle.Contains("PCGK" + SemesterID))
                            {
                                objDB.QualityMiddleEvaluation = objRatedCommentPupilBO.QualityMiddleEvaluation;
                                isInsertLog = true;
                            }
                            if (objDB.QualityEndingEvaluation != objRatedCommentPupilBO.QualityEndingEvaluation && TitleMark.IndexOf("PCCK") == 0 && !strLockMarkTitle.Contains("PCCK" + SemesterID))
                            {
                                objDB.QualityEndingEvaluation = objRatedCommentPupilBO.QualityEndingEvaluation;
                                isInsertLog = true;
                            }
                        }
                        if (TitleMark.IndexOf("NX") == 0 && (!string.IsNullOrEmpty(objDB.Comment) && string.IsNullOrEmpty(objRatedCommentPupilBO.Comment))
                                || (string.IsNullOrEmpty(objDB.Comment) && !string.IsNullOrEmpty(objRatedCommentPupilBO.Comment))
                                || (!string.IsNullOrEmpty(objDB.Comment) && !string.IsNullOrEmpty(objRatedCommentPupilBO.Comment)))
                        {
                            if (objDB.Comment != objRatedCommentPupilBO.Comment)
                            {
                                objDB.Comment = objRatedCommentPupilBO.Comment.Trim();
                                isInsertLog = true;
                            }
                        }
                        if (isInsertLog)
                        {
                            objDB.UpdateTime = DateTime.Now;
                            objDB.LogChangeID = objRatedCommentPupilBO.LogChangeID;
                            if (TitleMark.IndexOf("NX") == 0)
                            {
                                var lsttmp = lstRatedCommentPupilDB.Where(p => p.PupilID == objRatedCommentPupilBO.PupilID && p.ClassID == objRatedCommentPupilBO.ClassID && p.EvaluationID == objRatedCommentPupilBO.EvaluationID).ToList();
                                for (int i = 0; i < lsttmp.Count; i++)
                                {
                                    lsttmp[i].Comment = objRatedCommentPupilBO.Comment.Trim();
                                    RatedCommentPupilBusiness.Update(lsttmp[i]);
                                }
                            }
                            else
                            {
                                RatedCommentPupilBusiness.Update(objDB);
                            }

                        }
                    }
                }
                #endregion
                if (lstInsert.Count > 0)
                {
                    for (int i = 0; i < lstInsert.Count; i++)
                    {
                        RatedCommentPupilBusiness.Insert(lstInsert[i]);
                    }
                }
                RatedCommentPupilBusiness.Save();
            }
            catch (Exception ex)
            {
                
                LogExtensions.ErrorExt(logger, DateTime.Now, "InsertOrUpdateToMobile", "null", ex);
            }
            finally
            {
                SetAutoDetectChangesEnabled(true);
            }
        }
        #endregion
        #endregion

        public void ImportFromInputMark(List<Object> insertList, List<Object> updateList)
        {
            try
            {
                this.context.Configuration.AutoDetectChangesEnabled = false;
                this.context.Configuration.ValidateOnSaveEnabled = false;

                for (int i = 0; i < insertList.Count; i++)
                {
                    RatedCommentPupil entity = (RatedCommentPupil)insertList[i];
                    RatedCommentPupilBusiness.Insert(entity);
                }
                for (int i = 0; i < updateList.Count; i++)
                {
                    RatedCommentPupil entity = (RatedCommentPupil)updateList[i];
                    RatedCommentPupilBusiness.Update(entity);
                }

                RatedCommentPupilBusiness.Save();
            }
            finally
            {
                this.context.Configuration.AutoDetectChangesEnabled = true;
                this.context.Configuration.ValidateOnSaveEnabled = true;
            }
        }

        class GroupPOC
        {
            public int EducationLevelID { get; set; }
            public int ClassID { get; set; }
            public string ClassName { get; set; }
            public int CountPupil { get; set; }
            public int? EthnicID { get; set; }
            public int Genre { get; set; }
        }

        class CountMarkCellValue
        {
            public int Cell { get; set; }
            public int Count { get; set; }
        }
 #region SMS EDU
        /// <summary>
        /// Lay noi dung tin nhan TT22
        /// </summary>
        /// <param name="pupilIDList">List PupilID</param>
        /// <param name="schoolID"></param>
        /// <param name="academicYearID"></param>
        /// <param name="classID"></param>
        /// <param name="type">1: Ket qua giua ky, 2: Ket qua cuoi ky, 3: Nhan xet cuoi ky</param>
        /// <param name="semesterID"></param>
        /// <returns></returns>
        public List<RatedCommentPupilBO> GetListRatedCommentPupil(List<long> pupilIDList, int schoolID, int academicYearID, List<int> lstClassID, int type, int semesterID)
        {
            try
            {
                IDictionary<string, object> dic;
                //list hoc sinh trong lop
                List<PupilOfClassBO> lstPupilOfClass = (from poc in PupilOfClassBusiness.AllNoTracking
                                                        join pp in PupilProfileBusiness.AllNoTracking.OrderBy(x => x.PupilProfileID) on poc.PupilID equals pp.PupilProfileID
                                                        where pupilIDList.Contains(poc.PupilID)
                                                        && poc.SchoolID == schoolID && poc.AcademicYearID == academicYearID
                                                        && lstClassID.Contains(poc.ClassID)
                                                        && poc.Status == GlobalConstants.PUPIL_STATUS_STUDYING
                                                        select new PupilOfClassBO()
                                                        {
                                                            PupilOfClassID = poc.PupilOfClassID,
                                                            SchoolID = poc.SchoolID,
                                                            AcademicYearID = poc.AcademicYearID,
                                                            ClassID = poc.ClassID,
                                                            PupilID = poc.PupilID,
                                                            PupilCode = pp.PupilCode,
                                                            PupilFullName = pp.FullName,
                                                            Name = pp.Name,
                                                            OrderInClass = poc.OrderInClass,
                                                            Status = poc.Status,
                                                            EndDate = poc.EndDate,
                                                            Birthday = pp.BirthDate,
                                                            BirthPlace = pp.BirthPlace,
                                                            ResidentalAddress = pp.TempResidentalAddress,
                                                            PermanentResidentalAddress = pp.PermanentResidentalAddress,
                                                            EducationLevelID = poc.ClassProfile.EducationLevelID,
                                                            EthnicName = pp.Ethnic.EthnicName,
                                                            Genre = pp.Genre,
                                                            FatherFullName = pp.FatherFullName,
                                                            FatherJobName = pp.FatherJob,
                                                            MotherFullName = pp.MotherFullName,
                                                            MotherJobName = pp.MotherJob,
                                                            SponserFullName = pp.SponsorFullName,
                                                            SponserJobName = pp.SponsorJob,
                                                            ClassName = poc.ClassProfile.DisplayName,
                                                            FatherMobile = pp.FatherMobile,
                                                            FatherEmail = pp.FatherEmail,
                                                            MotherMobile = pp.MotherMobile,
                                                            MotherEmail = pp.MotherEmail,
                                                            SponserMobile = pp.SponsorMobile,
                                                            SponserEmail = pp.SponsorEmail,
                                                            HomeTown = pp.HomeTown,
                                                            EnrolmentDate = pp.EnrolmentDate,
                                                            StorageNumber = pp.StorageNumber
                                                        }).OrderBy(c => c.OrderInClass).ThenBy(c => c.Name).ThenBy(c => c.PupilFullName).ToList();

                //list mon hoc
                List<SubjectCatBO> lstClassSubject = new List<SubjectCatBO>();
                if (lstClassID.Count > 0)
                {
                    lstClassSubject = (from sb in ClassSubjectBusiness.All
                                       join sub in SubjectCatBusiness.All on sb.SubjectID equals sub.SubjectCatID
                                       orderby sb.IsCommenting, sub.OrderInSubject
                                       where lstClassID.Contains(sb.ClassID)
                                       && sub.IsActive == true
                                       && ((semesterID == GlobalConstants.SEMESTER_OF_YEAR_FIRST && sb.SectionPerWeekFirstSemester > 0)
                                       || (semesterID == GlobalConstants.SEMESTER_OF_YEAR_SECOND && sb.SectionPerWeekSecondSemester > 0))
                                       select new SubjectCatBO
                                       {
                                           ClassID = sb.ClassID,
                                           SubjectCatID = sb.SubjectID,
                                           SubjectName = sub.SubjectName,
                                           IsCommenting = sb.IsCommenting
                                       }
                                     ).ToList();
                }

                //Khen thuong 
                dic = new Dictionary<string, object>()
                {
                    {"SchoolID",schoolID},
                    {"AcademicYearID",academicYearID},
                    {"lstClassID",lstClassID},
                    {"Semester", GlobalConstants.SEMESTER_OF_EVALUATION_RESULT}
                };
                List<SummedEndingEvaluationBO> lstSEE = SummedEndingEvaluationBusiness.Search(dic).ToList();

                dic = new Dictionary<string, object>()
                {
                    {"SchoolID",schoolID},
                    {"AcademicYearID",academicYearID},
                    {"lstClassID",lstClassID},
                    {"SemesterID", semesterID},
                    {"lstPupilID", pupilIDList.ConvertAll(i => (int)i)},

                };

                List<EvaluationReward> lstEvaluationReward = EvaluationRewardBusiness.Search(dic).ToList();

                //List loai nang luc va pham chat
                List<EvaluationCriteria> lstEc = EvaluationCriteriaBusiness.All.ToList();
                List<EvaluationCriteria> lstEcNL = lstEc.Where(o => o.TypeID == 2).OrderBy(o => o.EvaluationCriteriaID).ToList();
                List<EvaluationCriteria> lstEcPC = lstEc.Where(o => o.TypeID == 3).OrderBy(o => o.EvaluationCriteriaID).ToList();

                List<RatedCommentPupilBO> lstData = new List<RatedCommentPupilBO>();
                RatedCommentPupilBO objData;

                dic = new Dictionary<string, object>();
                dic["SchoolID"] = schoolID;
                dic["AcademicYearID"] = academicYearID;
                //dic["ClassID"] = classID;
                dic["SemesterID"] = semesterID;

                AcademicYear objAy = AcademicYearBusiness.Find(academicYearID);

                List<RatedCommentPupilBO> lstRatedCommentAll;
                if (UtilsBusiness.IsMoveHistory(objAy))
                {
                    lstRatedCommentAll = RatedCommentPupilHistoryBusiness.Search(dic)
                        .Where(p => lstClassID.Contains(p.ClassID))
                        .Select(o => new RatedCommentPupilBO
                        {
                            CapacityEndingEvaluation = o.CapacityEndingEvaluation,
                            CapacityMiddleEvaluation = o.CapacityMiddleEvaluation,
                            Comment = o.Comment,
                            EndingEvaluation = o.EndingEvaluation,
                            EvaluationID = o.EvaluationID,
                            MiddleEvaluation = o.MiddleEvaluation,
                            PeriodicEndingJudgement = o.PeriodicEndingJudgement,
                            PeriodicEndingMark = o.PeriodicEndingMark,
                            PeriodicMiddleJudgement = o.PeriodicMiddleJudgement,
                            PeriodicMiddleMark = o.PeriodicMiddleMark,
                            PupilID = o.PupilID,
                            QualityEndingEvaluation = o.QualityEndingEvaluation,
                            QualityMiddleEvaluation = o.QualityMiddleEvaluation,
                            RetestMark = o.RetestMark,
                            SemesterID = o.SemesterID,
                            SubjectID = o.SubjectID,
                            ClassID = o.ClassID
                        }).ToList();
                }
                else
                {
                    lstRatedCommentAll = RatedCommentPupilBusiness.Search(dic)
                        .Where(p => lstClassID.Contains(p.ClassID))
                        .Select(o => new RatedCommentPupilBO
                        {
                            CapacityEndingEvaluation = o.CapacityEndingEvaluation,
                            CapacityMiddleEvaluation = o.CapacityMiddleEvaluation,
                            Comment = o.Comment,
                            EndingEvaluation = o.EndingEvaluation,
                            EvaluationID = o.EvaluationID,
                            MiddleEvaluation = o.MiddleEvaluation,
                            PeriodicEndingJudgement = o.PeriodicEndingJudgement,
                            PeriodicEndingMark = o.PeriodicEndingMark,
                            PeriodicMiddleJudgement = o.PeriodicMiddleJudgement,
                            PeriodicMiddleMark = o.PeriodicMiddleMark,
                            PupilID = o.PupilID,
                            QualityEndingEvaluation = o.QualityEndingEvaluation,
                            QualityMiddleEvaluation = o.QualityMiddleEvaluation,
                            RetestMark = o.RetestMark,
                            SemesterID = o.SemesterID,
                            SubjectID = o.SubjectID,
                            ClassID = o.ClassID
                        }).ToList();
                }
                int ClassID = 0;
                for (int i = 0; i < lstPupilOfClass.Count; i++)
                {
                    PupilOfClassBO poc = lstPupilOfClass[i];
                    ClassID = poc.ClassID;
                    objData = new RatedCommentPupilBO();
                    objData.PupilID = poc.PupilID;
                    List<RatedCommentPupilBO> lstRatedCommentPupil = lstRatedCommentAll.Where(o =>o.ClassID == ClassID && o.PupilID == poc.PupilID).ToList();
                    StringBuilder sb = new StringBuilder();
                    string tempNL;
                    string tempPC;
                    string tempKT;
                    //Tong hop ket qua
                    var lstCS = lstClassSubject.Where(p => p.ClassID == ClassID).ToList();
                    switch (type)
                    {
                        //Ket qua giua ky
                        case 1:
                            //Mon hoc & HDGD
                            for (int j = 0; j < lstCS.Count; j++)
                            {
                                SubjectCatBO subject = lstCS[j];

                                RatedCommentPupilBO ratedCommentSubject = lstRatedCommentPupil
                                    .Where(o => o.EvaluationID == 1 && o.SubjectID == subject.SubjectCatID).FirstOrDefault();

                                if (ratedCommentSubject != null)
                                {
                                    string evaluation = ratedCommentSubject.MiddleEvaluation == 1 ? GlobalConstants.EVALUATION_GOOD_COMPLETE
                                        : (ratedCommentSubject.MiddleEvaluation == 2 ? GlobalConstants.EVALUATION_COMPLETE :
                                        (ratedCommentSubject.MiddleEvaluation == 3 ? GlobalConstants.EVALUATION_NOT_COMPLETE : string.Empty));

                                    string mark;

                                    if (subject.IsCommenting.HasValue && subject.IsCommenting == 1)
                                    {
                                        mark = ratedCommentSubject.PeriodicMiddleJudgement;
                                    }
                                    else
                                    {
                                        mark = ratedCommentSubject.PeriodicMiddleMark.HasValue ?
                                            ratedCommentSubject.PeriodicMiddleMark.Value.ToString() : string.Empty;
                                    }

                                    if (!string.IsNullOrEmpty(evaluation) || !string.IsNullOrEmpty(mark))
                                    {
                                        sb.Append(subject.SubjectName);
                                        sb.Append(": ");
                                        if (!string.IsNullOrEmpty(evaluation))
                                        {
                                            sb.Append(evaluation);
                                        }

                                        if (!string.IsNullOrEmpty(evaluation) && !string.IsNullOrEmpty(mark))
                                        {
                                            sb.Append(",");
                                        }

                                        if (!string.IsNullOrEmpty(mark))
                                        {
                                            sb.Append(mark);
                                        }

                                        sb.Append(";");
                                    }
                                }
                            }
                            sb.Append("\n");

                            //Nang luc
                            tempNL = string.Empty;
                            for (int j = 0; j < lstEcNL.Count; j++)
                            {
                                EvaluationCriteria ec = lstEcNL[j];

                                RatedCommentPupilBO ratedCommentSubject = lstRatedCommentPupil
                                    .Where(o => o.EvaluationID == 2 && o.SubjectID == ec.EvaluationCriteriaID).FirstOrDefault();

                                if (ratedCommentSubject != null)
                                {
                                    string evaluation = ratedCommentSubject.CapacityMiddleEvaluation == 1 ? GlobalConstants.EVALUATION_GOOD_COMPLETE
                                        : (ratedCommentSubject.CapacityMiddleEvaluation == 2 ? GlobalConstants.EVALUATION_CAPQUA_COMPLETE :
                                        (ratedCommentSubject.CapacityMiddleEvaluation == 3 ? GlobalConstants.EVALUATION_NOT_COMPLETE : string.Empty));

                                    if (!string.IsNullOrEmpty(evaluation))
                                    {
                                        tempNL += evaluation;
                                        tempNL += ";";
                                    }
                                }
                            }
                            if (!string.IsNullOrWhiteSpace(tempNL))
                            {
                                sb.Append("Năng lực: ");
                                sb.Append(tempNL);
                                sb.Append("\n");
                            }

                            //Pham chat
                            tempPC = string.Empty;
                            for (int j = 0; j < lstEcPC.Count; j++)
                            {
                                EvaluationCriteria ec = lstEcPC[j];

                                RatedCommentPupilBO ratedCommentSubject = lstRatedCommentPupil
                                    .Where(o => o.EvaluationID == 3 && o.SubjectID == ec.EvaluationCriteriaID).FirstOrDefault();

                                if (ratedCommentSubject != null)
                                {
                                    string evaluation = ratedCommentSubject.QualityMiddleEvaluation == 1 ? GlobalConstants.EVALUATION_GOOD_COMPLETE
                                        : (ratedCommentSubject.QualityMiddleEvaluation == 2 ? GlobalConstants.EVALUATION_CAPQUA_COMPLETE :
                                        (ratedCommentSubject.QualityMiddleEvaluation == 3 ? GlobalConstants.EVALUATION_NOT_COMPLETE : string.Empty));

                                    if (!string.IsNullOrEmpty(evaluation))
                                    {
                                        tempPC += evaluation;
                                        tempPC += ";";
                                    }
                                }
                            }

                            if (!string.IsNullOrWhiteSpace(tempPC))
                            {
                                sb.Append("Phẩm chất: ");
                                sb.Append(tempPC);
                                sb.Append("\n");
                            }

                            break;
                        //Ket qua cuoi ky
                        case 2:
                            //Mon hoc & HDGD
                            for (int j = 0; j < lstCS.Count; j++)
                            {
                                SubjectCatBO subject = lstCS[j];

                                RatedCommentPupilBO ratedCommentSubject = lstRatedCommentPupil
                                    .Where(o => o.EvaluationID == 1 && o.SubjectID == subject.SubjectCatID).FirstOrDefault();

                                if (ratedCommentSubject != null)
                                {
                                    string evaluation = ratedCommentSubject.EndingEvaluation == 1 ? GlobalConstants.EVALUATION_GOOD_COMPLETE
                                        : (ratedCommentSubject.EndingEvaluation == 2 ? GlobalConstants.EVALUATION_COMPLETE :
                                        (ratedCommentSubject.EndingEvaluation == 3 ? GlobalConstants.EVALUATION_NOT_COMPLETE : string.Empty));

                                    string mark;

                                    if (subject.IsCommenting.HasValue && subject.IsCommenting == 1)
                                    {
                                        mark = ratedCommentSubject.PeriodicEndingJudgement;
                                    }
                                    else
                                    {
                                        mark = ratedCommentSubject.PeriodicEndingMark.HasValue ?
                                            ratedCommentSubject.PeriodicEndingMark.Value.ToString() : string.Empty;
                                    }

                                    if (!string.IsNullOrEmpty(evaluation) || !string.IsNullOrEmpty(mark))
                                    {
                                        sb.Append(subject.SubjectName);
                                        sb.Append(": ");
                                        if (!string.IsNullOrEmpty(evaluation))
                                        {
                                            sb.Append(evaluation);
                                        }

                                        if (!string.IsNullOrEmpty(evaluation) && !string.IsNullOrEmpty(mark))
                                        {
                                            sb.Append(",");
                                        }

                                        if (!string.IsNullOrEmpty(mark))
                                        {
                                            sb.Append(mark);
                                        }

                                        sb.Append(";");
                                    }
                                }
                            }
                            sb.Append("\n");

                            //Nang luc

                            tempNL = string.Empty;
                            for (int j = 0; j < lstEcNL.Count; j++)
                            {
                                EvaluationCriteria ec = lstEcNL[j];

                                RatedCommentPupilBO ratedCommentSubject = lstRatedCommentPupil
                                    .Where(o => o.EvaluationID == 2 && o.SubjectID == ec.EvaluationCriteriaID).FirstOrDefault();

                                if (ratedCommentSubject != null)
                                {
                                    string evaluation = ratedCommentSubject.CapacityEndingEvaluation == 1 ? GlobalConstants.EVALUATION_GOOD_COMPLETE
                                        : (ratedCommentSubject.CapacityEndingEvaluation == 2 ? GlobalConstants.EVALUATION_CAPQUA_COMPLETE :
                                        (ratedCommentSubject.CapacityEndingEvaluation == 3 ? GlobalConstants.EVALUATION_NOT_COMPLETE : string.Empty));

                                    if (!string.IsNullOrEmpty(evaluation))
                                    {
                                        tempNL += evaluation;
                                        tempNL += ";";
                                    }
                                }
                            }

                            if (!string.IsNullOrWhiteSpace(tempNL))
                            {
                                sb.Append("Năng lực: ");
                                sb.Append(tempNL);
                                sb.Append("\n");
                            }

                            //Pham chat

                            tempPC = string.Empty;
                            for (int j = 0; j < lstEcPC.Count; j++)
                            {
                                EvaluationCriteria ec = lstEcPC[j];

                                RatedCommentPupilBO ratedCommentSubject = lstRatedCommentPupil
                                    .Where(o => o.EvaluationID == 3 && o.SubjectID == ec.EvaluationCriteriaID).FirstOrDefault();

                                if (ratedCommentSubject != null)
                                {
                                    string evaluation = ratedCommentSubject.QualityEndingEvaluation == 1 ? GlobalConstants.EVALUATION_GOOD_COMPLETE
                                        : (ratedCommentSubject.QualityEndingEvaluation == 2 ? GlobalConstants.EVALUATION_CAPQUA_COMPLETE :
                                        (ratedCommentSubject.QualityEndingEvaluation == 3 ? GlobalConstants.EVALUATION_NOT_COMPLETE : string.Empty));

                                    if (!string.IsNullOrEmpty(evaluation))
                                    {
                                        tempPC += evaluation;
                                        tempPC += ";";
                                    }
                                }
                            }

                            if (!string.IsNullOrWhiteSpace(tempPC))
                            {
                                sb.Append("Phẩm chất: ");
                                sb.Append(tempPC);
                                sb.Append("\n");
                            }

                            //Khen thuong

                            tempKT = string.Empty;
                            SummedEndingEvaluationBO see = lstSEE.Where(o =>o.ClassID == ClassID && o.PupilID == poc.PupilID && semesterID == GlobalConstants.SEMESTER_OF_YEAR_SECOND && o.RewardID != null && o.RewardID != 0).FirstOrDefault();
                            if (see != null)
                            {
                                tempKT += "Học sinh xuất sắc";
                                tempKT += ";";
                            }

                            List<EvaluationReward> lstEvaluationRewardPupil = lstEvaluationReward.Where(o => o.PupilID == poc.PupilID).ToList();

                            for (int j = 0; j < lstEvaluationRewardPupil.Count; j++)
                            {
                                EvaluationReward er = lstEvaluationRewardPupil[j];
                                string rewardName = er.RewardID == 1 || er.RewardID == 2 ? er.Content : string.Empty;
                                if (!string.IsNullOrEmpty(rewardName))
                                {
                                    tempKT += rewardName;
                                    tempKT += ";";
                                }
                            }

                            if (!string.IsNullOrWhiteSpace(tempKT))
                            {
                                sb.Append("Khen thưởng: ");
                                sb.Append(tempKT);
                                sb.Append("\n");
                            }

                            if (semesterID == 2)
                            {
                                SummedEndingEvaluationBO objSee = lstSEE.Where(o => o.ClassID == ClassID && o.PupilID == poc.PupilID).FirstOrDefault();

                                if (objSee != null)
                                {

                                    string evaluation = string.Empty;
                                    if (objSee.EndingEvaluation == "HT")
                                    {
                                        evaluation = "Được lên lớp";
                                    }
                                    else if (objSee.EndingEvaluation == "CHT" && objSee.RateAdd != 0 && objSee.RateAdd != 1)
                                    {
                                        evaluation = "Chưa hoàn thành chương trình lớp học";
                                    }
                                    else if (objSee.EndingEvaluation == "CHT" && objSee.RateAdd == 1)
                                    {
                                        evaluation = "Được lên lớp";
                                    }
                                    else if (objSee.EndingEvaluation == "CHT" && objSee.RateAdd == 0)
                                    {
                                        evaluation = "Ở lại lớp";
                                    }

                                    sb.Append(evaluation);
                                }
                            }

                            break;
                        //Nhat xet cuoi ky
                        case 3:
                            //Mon hoc & HDGD
                            for (int j = 0; j < lstCS.Count; j++)
                            {
                                SubjectCatBO subject = lstCS[j];

                                RatedCommentPupilBO ratedCommentSubject = lstRatedCommentPupil
                                    .Where(o => o.EvaluationID == 1 && o.SubjectID == subject.SubjectCatID).FirstOrDefault();

                                if (ratedCommentSubject != null)
                                {
                                    string comment = ratedCommentSubject.Comment;

                                    if (!string.IsNullOrEmpty(comment))
                                    {
                                        sb.Append(subject.SubjectName);
                                        sb.Append(": ");
                                        sb.Append(comment);
                                        sb.Append(";");
                                    }
                                }
                            }
                            sb.Append("\n");

                            //Nang luc

                            tempNL = string.Empty;

                            RatedCommentPupilBO objRatedComment = lstRatedCommentPupil
                                .Where(o => o.EvaluationID == 2).FirstOrDefault();

                            if (objRatedComment != null)
                            {
                                string comment = objRatedComment.Comment;

                                if (!string.IsNullOrEmpty(comment))
                                {
                                    tempNL += comment;
                                    tempNL += ";";
                                }
                            }

                            if (!string.IsNullOrWhiteSpace(tempNL))
                            {
                                sb.Append("Năng lực: ");
                                sb.Append(tempNL);
                                sb.Append("\n");
                            }

                            //Pham chat
                            tempPC = string.Empty;

                            objRatedComment = lstRatedCommentPupil
                                .Where(o => o.EvaluationID == 3).FirstOrDefault();

                            if (objRatedComment != null)
                            {
                                string comment = objRatedComment.Comment;

                                if (!string.IsNullOrEmpty(comment))
                                {
                                    tempPC += comment;
                                    tempPC += ";";
                                }
                            }


                            if (!string.IsNullOrWhiteSpace(tempPC))
                            {
                                sb.Append("Phẩm chất: ");
                                sb.Append(tempPC);
                            }
                            break;
                    }

                    objData.SMSContent = sb.ToString().Trim();
                    if (objData.SMSContent.EndsWith(";"))
                    {
                        objData.SMSContent = objData.SMSContent.Substring(0, objData.SMSContent.Length - 1);
                    }
                    lstData.Add(objData);
                }

                return lstData;
            }
            catch (Exception ex)
            {
                logger.Error(ex.Message, ex);
                return new List<RatedCommentPupilBO>();
            }
        }
        #endregion
    }
}
