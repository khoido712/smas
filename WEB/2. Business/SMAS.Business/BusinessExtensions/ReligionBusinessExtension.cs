/**
 * The Viettel License
 *
 * Copyright 2012 Viettel Telecom. All rights reserved.
 * VIETTEL PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */

/** 
* @author  
* @version $Revision: $
*/

using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Linq.Expressions;
using System.Web;
using log4net;

using SMAS.DAL.Repository;
using SMAS.DAL.IRepository;
using SMAS.Business.IBusiness;
using SMAS.Models.Models;
using SMAS.Business.Common;

namespace SMAS.Business.Business
{
    public partial class ReligionBusiness
    {

        #region private member variable
        private const int RESOLUTION_MAX_LENGTH = 50;   //Độ dài trường diễn giải
        private const int DESCRIPTION_MAX_LENGTH = 400; //Độ dài ghi chú

        #endregion

        #region Search
        /// <summary>
        /// Tìm kiếm Tôn giáo
        /// </summary>
        /// <author>hath</author>
        /// <date>4/9/2012</date>
        /// <param name="Resolution">Diễn giải tôn giáo</param>
        /// <param name="Description">Mô tả</param>
        /// <param name="IsActive">Biến kích hoạt</param>
        /// <returns>IQueryable<Religion></returns>
        public IQueryable<Religion> Search(IDictionary<string, object> dic)
        {
            string Resolution = Utils.GetString(dic, "Resolution");
            string Description = Utils.GetString(dic, "Description");
            bool? IsActive = Utils.GetIsActive(dic, "IsActive");

            IQueryable<Religion> lsReligion = ReligionRepository.All;

            if (IsActive.HasValue)
                lsReligion = lsReligion.Where(rel => rel.IsActive == IsActive);
            if (Resolution.Trim().Length != 0)
                lsReligion = lsReligion.Where(rel => rel.Resolution.Contains(Resolution.ToLower()));
            if (Description.Trim().Length != 0)
                lsReligion = lsReligion.Where(rel => rel.Description.Contains(Description.ToLower()));
            return lsReligion;

        }
        #endregion

        #region Delete
        /// <summary>
        /// Xóa Tôn giáo
        /// </summary>
        /// <author>hath</author>
        /// <date>4/9/2012</date>
        /// <param name="ReligionID">ID tôn giáo</param>
        public void Delete(int ReligionID )
        {
            //Bạn chưa chọn tôn giáo cần xóa hoặc tôn giáo bạn chọn đã bị xóa khỏi hệ thống
            new ReligionBusiness(null).CheckAvailable((int)ReligionID, "Religion_Label_ReligionID", true);

            //Không thể xóa tôn giáo đang sử dụng
            new ReligionBusiness(null).CheckConstraints(GlobalConstants.LIST_SCHEMA, "Religion", ReligionID, "Religion_Label_ReligionID");
            base.Delete(ReligionID, true);
        }
        #endregion

        #region Insert

        /// <summary>
        /// Thêm Tôn giáo
        /// </summary>
        /// <author>hath</author>
        /// <date>4/9/2012</date>
        /// <param name="religion">Đối tượng tôn giáo</param>
        /// <returns>Đối tượng tôn giáo</returns>
        public override Religion Insert(Religion religion)
        {
            //Tên tôn giáo không được để trống 
            Utils.ValidateRequire(religion.Resolution, "Religion_Label_Resolution");
            //Độ dài trường tôn giáo không được vượt quá 50 
            Utils.ValidateMaxLength(religion.Resolution, RESOLUTION_MAX_LENGTH, "Religion_Label_Resolution");
            //Tên tôn giáo đã tồn tại 
            new ReligionBusiness(null).CheckDuplicate(religion.Resolution, GlobalConstants.LIST_SCHEMA, "Religion", "Resolution", true, religion.ReligionID, "Religion_Label_Resolution");
           
            //Độ dài ghi chú không được vượt quá 400 
            Utils.ValidateMaxLength(religion.Description, DESCRIPTION_MAX_LENGTH, "Description");


            //Insert
            return base.Insert(religion);

        }
        #endregion

        #region Update
        /// <summary>
        /// Sửa Tôn giáo
        /// </summary>
        /// <author>hath</author>
        /// <date>4/9/2012</date>
        /// <param name="religion">Đối tượng tôn giáo</param>
        /// <returns>Đối tượng tôn giáo</returns>
        public  Religion Update(Religion religion)
        {
            //Bạn chưa chọn tôn giáo cần sửa hoặc tôn giáo bạn chọn đã bị xóa khỏi hệ thống
            new ReligionBusiness(null).CheckAvailable((int)religion.ReligionID, "Religion_Label_ReligionID", true);
            //Tên tôn giáo không được để trống 
            Utils.ValidateRequire(religion.Resolution, "Religion_Label_Resolution");
            //Độ dài trường tôn giáo không được vượt quá 50 
            Utils.ValidateMaxLength(religion.Resolution, RESOLUTION_MAX_LENGTH, "Religion_Label_Resolution");
            //Tên tôn giáo đã tồn tại 
           // new ReligionBusiness(null).CheckDuplicate(religion.Resolution, GlobalConstants.LIST_SCHEMA, "Religion", "Resolution", true, religion.ReligionID, "Religion_Label_Resolution");

            //Độ dài ghi chú không được vượt quá 400 
            Utils.ValidateMaxLength(religion.Description, DESCRIPTION_MAX_LENGTH, "Description");



           return  base.Update(religion);
        }
        #endregion
    }
}
