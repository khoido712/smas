/**
 * The Viettel License
 *
 * Copyright 2012 Viettel Telecom. All rights reserved.
 * VIETTEL PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */

/** 
* @author  
* @version $Revision: $
*/

using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Linq.Expressions;
using System.Web;
using log4net;

using SMAS.DAL.Repository;
using SMAS.DAL.IRepository;
using SMAS.Business.IBusiness;
using SMAS.Models.Models;
using SMAS.Business.Common;
namespace SMAS.Business.Business
{
    public partial class GroupCatBusiness
    {


        public IQueryable<GroupCat> GetAllActive()
        {
            return this.All.Where(g => g.IsActive == true);
        }

        public void InsertGroup(GroupCat model)
        {
            IDictionary<string, object> expDic = null;
            if (model.GroupCatID > 0)
            {
                expDic = new Dictionary<string, object>();
                expDic["GroupCatID"] = model.GroupCatID;
            }
            bool GroupCatNameCompatible = this.repository.ExistsRow(GlobalConstants.ADM_SCHEMA, "GroupCat",
                   new Dictionary<string, object>()
                {
                    {"GroupName",model.GroupName},
                    {"AdminUserID",model.AdminUserID},
                    {"IsActive",true}
                }, expDic);
            if (GroupCatNameCompatible)
            {
                List<object> listParam = new List<object>();
                listParam.Add("GroupCat_Label_Name");
                throw new BusinessException("Common_Validate_Duplicate", listParam);
            }
            model.IsActive = true;
            GroupCatRepository.Insert(model);
        }

        public void Delete(int id)
        {
            //this.CheckConstraints(GlobalConstants.ADM_SCHEMA, "UserGroup", id, "GroupCat_Label_GroupName");
            IQueryable<UserGroup> iqUserGroup = UserGroupBusiness.All.Where(p => p.GroupID == id);
            if (iqUserGroup.Count() > 0)
            {
                throw new BusinessException(string.Format("Nhóm người dùng đang được sử dụng."));
            }
            GroupCat group = GroupCatRepository.Find(id);
            group.IsActive = false;
            GroupCatRepository.Update(group);
        }
        public void DeleteGroupCat(int id)
        {
            GroupCat gc = GroupCatRepository.Find(id);
            gc.IsActive = false;
            GroupCatRepository.Save();
        }

        public IQueryable<GroupCat> GetGroupByRoleAndCreatedUser(int roleID, int createdUserId)
        {
            return GroupCatRepository.All.Where(gc => (gc.RoleID == roleID && gc.CreatedUserID == createdUserId && gc.IsActive == true));
        }

        public IQueryable<GroupCat> GetGroupByRoleAndAdminID(int roleID, int AdminID)
        {
            return GroupCatRepository.All.Where(gc => (gc.RoleID == roleID && gc.AdminUserID == AdminID && gc.IsActive == true));
        }
        public bool CheckPrivilegeToCreateGroup(int UserAccountID)
        {
            UserAccount user = UserAccountRepository.Find(UserAccountID);
            if (user == null) return false;
            return user.IsAdmin;
        }

        public bool CreateGroup(int UserAccountID, GroupCat group, int RoleID)
        {
            if (UserAccountID == 0 || RoleID == 0)
            {
                throw new BusinessException();
            }
            if (!CheckPrivilegeToCreateGroup(UserAccountID))
            {
                return false;
            }

            group.RoleID = RoleID;
            group.CreatedUserID = UserAccountID;
            GroupCatRepository.Insert(group);
            return true;
        }

        public IQueryable<GroupCat> GetGroupsOfRole(int RoleID)
        {
            if (RoleID == 0) return null;
            return GroupCatRepository.All.Where(gc => gc.RoleID == RoleID && gc.IsActive == true);
        }

        public int GetRoleOfGroup(int GroupID)
        {
            GroupCat group = GroupCatRepository.All.Where(gc => gc.GroupCatID == GroupID && gc.IsActive == true).First();
            if (group == null) throw new BusinessException();
            return group.RoleID;
        }

        public IQueryable<GroupCat> Search(IDictionary<string, object> dic)
        {
            IQueryable<GroupCat> result = this.GetAllActive();

            int roleID = Utils.GetInt(dic, "RoleID");
            int AdminUserID = Utils.GetInt(dic, "AdminUserID");
            string GroupCatName = Utils.GetString(dic, "GroupCatName");
            int CreateUserID = Utils.GetInt(dic, "CreateUserID");
            if (roleID != 0)
            {
                result = result.Where(g => g.RoleID == roleID);
            }
            if (AdminUserID != 0)
            {
                result = result.Where(g => g.AdminUserID == AdminUserID);
            }
            if (GroupCatName.Trim().Length != 0)
            {
                result = result.Where(g => g.GroupName.ToLower().Contains(GroupCatName.ToLower()));
            }
            return result;
        }

        /// <summary>
        /// Gán Menu cho nhóm
        /// author: namdv3
        /// </summary>
        /// <param name="GroupCatID"></param>
        /// <param name="ListPermissionMenu">key: MenuID, value: Permission</param>
        public GroupCat AssignMenu(int GroupCatID, Dictionary<int, int?> ListPermissionMenu)
        {
            GroupCat GroupCat = GroupCatRepository.Find(GroupCatID);
            // Lay danh sach menu duoc gan cho role cua nhom
            List<int> listMenuID = RoleMenuBusiness.All.Where(o => o.RoleID == GroupCat.RoleID).Select(o => o.MenuID).ToList();
            foreach (var item in ListPermissionMenu)
            {
                if (item.Value.HasValue)
                {
                    Utils.ValidateRange(int.Parse(item.Value.ToString()), 1, 4, "DefaultGroupMenu_Label_Permission");
                }

                if (!listMenuID.Contains(item.Key))
                {
                    List<object> Params = new List<object>();
                    Params.Add("DefaultGroupMenu_Label_MenuID");
                    Params.Add("DefaultGroup_Label_RoleID");
                    throw new BusinessException("Common_Validate_NotExist", Params);
                }
            }
            //I:validation:
            //  1: Permission: 1-> 4
            //  2: MenuID: not Exist
            //  3: GroupCatID: not Exist
            //  4: GroupCat(GroupCatID).RoleID – MenuID (trong ListMenu): Compatible
            //II: processing
            //  1: Thực hiện xóa các bản ghi cũ trong GroupMenu theo GroupCatID.GroupMenuBusiness.Delete()
            List<GroupMenu> listGroupMenu = GroupMenuBusiness.All.Where(o => o.GroupID == GroupCatID).ToList();
            if (listGroupMenu != null && listGroupMenu.Count > 0)
            {
                GroupMenuBusiness.DeleteAll(listGroupMenu);
            }
            //  2: Thực hiện insert từng phẩn tủ ListPermissionMenu và GroupCatID  vào bảng GroupMenu
            if (ListPermissionMenu != null && ListPermissionMenu.Count > 0)
            {
                foreach (var PermissionMenu in ListPermissionMenu)
                {
                    GroupMenu groupMenu = new GroupMenu();
                    groupMenu.GroupID = GroupCatID;
                    groupMenu.MenuID = PermissionMenu.Key;
                    groupMenu.Permission = PermissionMenu.Value;
                    GroupMenuBusiness.Insert(groupMenu);
                }
            }
            return GroupCat;
        }
    }
}
