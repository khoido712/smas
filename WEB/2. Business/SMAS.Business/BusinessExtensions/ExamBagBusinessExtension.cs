/**
 * The Viettel License
 *
 * Copyright 2012 Viettel Telecom. All rights reserved.
 * VIETTEL PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */

/** 
* @author  
* @version $Revision: $
*/
using System;
using log4net;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Linq.Expressions;
using System.Web;
using SMAS.DAL.Repository;
using SMAS.DAL.IRepository;
using SMAS.Business.IBusiness;
using SMAS.Models.Models;
using SMAS.Business.Common;
using System.Collections.Generic;
using SMAS.Business.BusinessObject;

namespace SMAS.Business.Business
{ 
    public partial class ExamBagBusiness
    {
        public IQueryable<ExamBag> Search(IDictionary<string, object> search)
        {
            long examinationsID = Utils.GetLong(search, "ExaminationsID");
            long examGroupID = Utils.GetLong(search, "ExamGroupID");
            long examRoomID = Utils.GetInt(search, "ExamRoomID");
            int subjectID = Utils.GetInt(search, "SubjectID");
            List<long> lstExamGroupID = Utils.GetLongList(search, "lstExamGroupID");

            IQueryable<ExamBag> query = this.All;
            if (examinationsID != 0)
            {
                query = query.Where(o => o.ExaminationsID == examinationsID);
            }
            if (examGroupID != 0)
            {
                query = query.Where(o => o.ExamGroupID == examGroupID);
            }
            if (examRoomID != 0)
            {
                query = query.Where(o => o.ExamRoomID == examRoomID);
            }
            if (subjectID != 0)
            {
                query = query.Where(o => o.SubjectID == subjectID);
            }
            if (lstExamGroupID.Count > 0)
            {
                query = query.Where(p => lstExamGroupID.Contains(p.ExamGroupID));
            }
            return query;
        }

        public IQueryable<ExamBagBO> GetExamBagOfExaminations(long examinationsID)
        {

            IQueryable<ExamBagBO> query = (from eb in this.ExamBagRepository.All
                                           join er in this.ExamRoomRepository.All on eb.ExamRoomID equals er.ExamRoomID
                                           where eb.ExaminationsID == examinationsID
                                                 
                                           select new ExamBagBO
                                           {
                                               ExamBagCode = eb.ExamBagCode,
                                               ExamBagID = eb.ExamBagID,
                                               ExamGroupID = eb.ExamGroupID,
                                               ExaminationsID = eb.ExaminationsID,
                                               ExamRoomCode = er.ExamRoomCode,
                                               ExamRoomID = eb.ExamRoomID,
                                               SubjectID = eb.SubjectID
                                           });
          
            return query;
        }

        public List<ExamBagBO> GetListExamBag(IDictionary<string, object> search)
        {
            long examinationsID = Utils.GetLong(search, "ExaminationsID");
            long examGroupID = Utils.GetLong(search, "ExamGroupID");
            int subjectID = Utils.GetInt(search, "SubjectID");

            List<ExamBagBO> queryRoom = (from eb in ExamRoomRepository.All
                                               where eb.ExaminationsID == examinationsID && eb.ExamGroupID == examGroupID
                                               select new ExamBagBO
                                               {
                                                   ExaminationsID = eb.ExaminationsID,
                                                   ExamGroupID = eb.ExamGroupID,
                                                   ExamRoomID = eb.ExamRoomID,
                                                   ExamRoomCode = eb.ExamRoomCode

                                               }).ToList();

            List<long> listID = queryRoom.Select(o => o.ExamRoomID).ToList();
            List<ExamBag> lstExamBag = ExamBagBusiness.Search(search).Where(c => listID.Contains(c.ExamRoomID)).ToList();
            ExamBag bag = null;
            for (int i = 0; i < listID.Count; i++)
            {
                //search["ExamRoomID"] = listID[i];
                bag = lstExamBag.FirstOrDefault(c=>c.ExamRoomID==listID[i]);
                if (bag != null)
                {
                    queryRoom[i].ExamBagID = bag.ExamBagID;
                    queryRoom[i].ExamBagCode = bag.ExamBagCode;
                    queryRoom[i].SubjectID = bag.SubjectID;
                }
                else
                {
                    queryRoom[i].ExamBagID = 0;
                    queryRoom[i].ExamBagCode = string.Empty;
                    queryRoom[i].SubjectID = 0;
                }
            }

            return queryRoom;
        }

        private void CheckExamBagForSubject(IDictionary<string, object> search, long ExaminationsID, ExamGroup group, ExamSubject subject,
            int[] Criteria, int LengthCode)
        {
            ExamBag insert = null;
            ExamBag checkUpdate = null;

            string codeSample = string.Empty;
            if (Criteria != null && Criteria.Contains(1))
            {
                codeSample = codeSample + group.ExamGroupCode;
            }
            if (Criteria != null && Criteria.Contains(2))
            {
                codeSample = codeSample + subject.SubjectCode;
            }

            List<ExamRoom> listExamRoom = ExamRoomBusiness.Search(search).OrderBy(o => o.ExamRoomCode).ToList();
            for (int i = 0; i < listExamRoom.Count; i++)
            {
                string code = codeSample;
                string STT = string.Empty;
                if (Criteria != null && Criteria.Contains(3))
                {
                    int lenght = LengthCode - (i + 1).ToString().Length;
                    for (int j = 0; j < lenght; j++)
                    {
                        STT = STT + "0";
                    }
                }
                else
                {
                    int count = listExamRoom.Count.ToString().Length - (i + 1).ToString().Length;
                    for (int j = 0; j < count; j++)
                    {
                        STT = STT + "0";
                    }
                }
                STT = STT + (i + 1).ToString();
                code = code + STT;

                search["ExamRoomID"] = listExamRoom[i].ExamRoomID;
                checkUpdate = this.Search(search).FirstOrDefault();
                if (checkUpdate == null)
                {
                    insert = new ExamBag();
                    insert.ExamBagID = this.GetNextSeq<long>();
                    insert.ExamBagCode = code;
                    insert.ExaminationsID = ExaminationsID;
                    insert.ExamGroupID = group.ExamGroupID;
                    insert.ExamRoomID = listExamRoom[i].ExamRoomID;
                    insert.SubjectID = subject.SubjectID;
                    insert.CreateTime = DateTime.Now;
                    this.Insert(insert);
                }
                else
                {
                    checkUpdate.ExamBagCode = code;
                    checkUpdate.UpdateTime = DateTime.Now;
                    this.Update(checkUpdate);
                }
            }
        }

        public void CheckExamBag(long ExaminationsID, long ExamGroupID, int SubjectID, int[] Criteria, int LengthCode, int Type)
        {
            IDictionary<string, object> search = new Dictionary<string, object>();
            search["ExaminationsID"] = ExaminationsID;
            if (Type == 3) // Tat ca nhom thi va mon thi
            {
                List<ExamGroup> listGroup = ExamGroupBusiness.Search(search).ToList();
                for (int i = 0; i < listGroup.Count; i++)
                {
                    search["ExamGroupID"] = listGroup[i].ExamGroupID;
                    search["SubjectID"] = 0;
                    search["ExamRoomID"] = 0;
                    List<ExamSubject> listSubject = ExamSubjectBusiness.Search(search).ToList();
                    for (int j = 0; j < listSubject.Count; j++)
                    {
                        search["SubjectID"] = listSubject[j].SubjectID;
                        CheckExamBagForSubject(search, ExaminationsID, listGroup[i], listSubject[j], Criteria, LengthCode);
                    }
                }
            }

            ExamGroup group = ExamGroupBusiness.Find(ExamGroupID);
            search["ExamGroupID"] = group.ExamGroupID;
            if (Type == 2) // Nhom thi hien tai
            {
                search["SubjectID"] = 0;
                List<ExamSubject> listSubject = ExamSubjectBusiness.Search(search).ToList();
                for (int i = 0; i < listSubject.Count; i++)
                {
                    search["SubjectID"] = listSubject[i].SubjectID;
                    CheckExamBagForSubject(search, ExaminationsID, group, listSubject[i], Criteria, LengthCode);
                }
            }

            search["SubjectID"] = SubjectID;
            ExamSubject subject = ExamSubjectBusiness.Search(search).FirstOrDefault();
            
            if (Type == 1) // Mon thi hien tai
            {
                CheckExamBagForSubject(search, ExaminationsID, group, subject, Criteria, LengthCode);
            }

            this.Save();
        }

        public void DeleteExamBag(List<long> listExamBagID)
        {
            if (listExamBagID == null)
            {
                return;
            }
            for (int i = 0; i < listExamBagID.Count; i++)
            {
                this.Delete(listExamBagID[i]);
            }
            this.Save();
        }

        private IDictionary<string, object> ColumnMappings()
        {
            Dictionary<string, object> columnMap = new Dictionary<string, object>();
            columnMap.Add("ExamBagID", "EXAM_BAG_ID");
            columnMap.Add("ExaminationsID", "EXAMINATIONS_ID");
            columnMap.Add("ExamGroupID", "EXAM_GROUP_ID");
            columnMap.Add("ExamRoomID", "EXAM_ROOM_ID");
            columnMap.Add("ExamBagCode", "EXAM_BAG_CODE");
            columnMap.Add("SubjectID", "SUBJECT_ID");
            columnMap.Add("CreateTime", "CREATE_TIME");
            columnMap.Add("UpdateTime", "UPDATE_TIME");
            return columnMap;
        }
    }
}