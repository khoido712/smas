﻿/**
 * The Viettel License
 *
 * Copyright 2012 Viettel Telecom. All rights reserved.
 * VIETTEL PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */

/** 
* @author  
* @version $Revision: $
*/

using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Linq.Expressions;
using System.Web;
using log4net;

using SMAS.DAL.Repository;
using SMAS.DAL.IRepository;
using SMAS.Business.IBusiness;
using SMAS.Models.Models;
using SMAS.Business.Common;
using SMAS.Business.BusinessObject;
using System.Transactions;
using System.IO;
using SMAS.VTUtils.Excel.Export;

namespace SMAS.Business.Business
{
    public partial class MarkInputSituationBusiness
    {
        public List<MarkInputSituationBO> GetMarkInputSituation(int subjectId, int semester, List<ClassProfile> lstClass, List<TeachingAssignment> lstTaAll, List<ClassSubject> lstCsAll, List<PupilOfClass> listPupilOfClass
            , List<MaxMarkNumberBO> listMarkAll, AcademicYear aca)
        {

            //Lay ra danh sach phan cong giang day
            List<TeachingAssignment> lstTaSubject = lstTaAll.Where(o => o.SubjectID == subjectId).ToList();


            List<ClassSubject> lstCs = lstCsAll.Where(o => o.SubjectID == subjectId).ToList();

            List<TeachingAssignment> lstTa = (from ta in lstTaSubject
                                              join cs in lstCs on ta.SubjectID equals cs.SubjectID
                                              where ta.ClassID == cs.ClassID
                                              select ta).ToList();

            List<MarkInputSituationBO> lstMarkInputSituation = lstTa.Select(o => new
            {
                TeacherID = o.TeacherID,
                TeacherName = o.Employee.Name,
                TeacherFullName = o.Employee.FullName
            }).Distinct().OrderBy(o => o.TeacherName).ThenBy(o => o.TeacherFullName)
                                                        .Select(x => new MarkInputSituationBO
                                                        {
                                                            TeacherID = x.TeacherID,
                                                            TeacherName = x.TeacherName,
                                                            TeacherFullName = x.TeacherFullName
                                                        }).ToList();


            List<MaxMarkNumberBO> listMark = listMarkAll.Where(o => o.SubjectID == subjectId).ToList();

            for (int i = 0; i < lstMarkInputSituation.Count; i++)
            {
                MarkInputSituationBO model = lstMarkInputSituation[i];

                model.lstMarkM = new List<ClassSituationBO>();
                model.lstMarkP = new List<ClassSituationBO>();
                model.lstMarkV = new List<ClassSituationBO>();
                model.lstMarkHK = new List<ClassSituationBO>();

                //Lay ra cac lop ma giao vien nay giang day
                List<int> lstAssignedClassId = lstTa.Where(o => o.TeacherID == model.TeacherID).Select(o => o.ClassID.GetValueOrDefault()).ToList();
                for (int j = 0; j < lstClass.Count; j++)
                {
                    ClassProfile cp = lstClass[j];
                    ClassSubject cs = lstCs.Where(o => o.ClassID == cp.ClassProfileID).FirstOrDefault();
                    List<PupilOfClass> lstPoc = listPupilOfClass.Where(o => o.ClassID == cp.ClassProfileID).ToList();

                    ClassSituationBO modelM;
                    ClassSituationBO modelP;
                    ClassSituationBO modelV;
                    ClassSituationBO modelHK;

                    //Neu giao vien co day lop nay
                    if (cs != null && lstAssignedClassId.Contains(cp.ClassProfileID))
                    {
                        modelM = new ClassSituationBO(cp.ClassProfileID);
                        modelP = new ClassSituationBO(cp.ClassProfileID);
                        modelV = new ClassSituationBO(cp.ClassProfileID);
                        modelHK = new ClassSituationBO(cp.ClassProfileID);

                        model.lstMarkM.Add(modelM);
                        model.lstMarkP.Add(modelP);
                        model.lstMarkV.Add(modelV);
                        model.lstMarkHK.Add(modelHK);

                        //M
                        //Tong so hoc sinh
                        modelM.TotalPupil = lstPoc.Count;
                        //So con diem da nhap
                        var markM = listMark.Where(x => x.ClassID == cp.ClassProfileID
                            && x.Title.Contains(SystemParamsInFile.MARK_TYPE_M)).ToList();
                        int mM = 0;
                        if (markM != null && markM.Count > 0)
                        {
                            mM = markM.Max(o => o.MarkIndex);
                        }

                        modelM.InputedNum = mM;
                        //So hoc sinh thieu diem
                        //Neu da cau hinh so con diem toi thieu
                        int baseNumM = mM;
                        if (semester == 1)
                        {
                            if (cs.MMinFirstSemester.HasValue && cs.MMinFirstSemester > 0)
                            {
                                baseNumM = cs.MMinFirstSemester.Value;
                            }
                        }
                        else
                        {
                            if (cs.MMinSecondSemester.HasValue && cs.MMinSecondSemester > 0)
                            {
                                baseNumM = cs.MMinSecondSemester.Value;
                            }
                        }

                        int t_mM = 0;
                        if (baseNumM > 0)
                        {
                            t_mM = markM.Where(o => o.MarkIndex < baseNumM).Count() + modelM.TotalPupil - markM.Count();
                        }

                        modelM.MissPupilNum = t_mM;


                        //P
                        //Tong so hoc sinh
                        modelP.TotalPupil = lstPoc.Count;
                        //So con diem da nhap
                        var markP = listMark.Where(x => x.ClassID == cp.ClassProfileID
                            && x.Title.Contains(SystemParamsInFile.MARK_TYPE_P)).ToList();
                        int mP = 0;
                        if (markP != null && markP.Count > 0)
                        {
                            mP = markP.Max(o => o.MarkIndex);
                        }

                        modelP.InputedNum = mP;
                        //So hoc sinh thieu diem
                        //Neu da cau hinh so con diem toi thieu
                        int baseNumP = mP;
                        if (semester == 1)
                        {
                            if (cs.PMinFirstSemester.HasValue && cs.PMinFirstSemester > 0)
                            {
                                baseNumP = cs.PMinFirstSemester.Value;
                            }
                        }
                        else
                        {
                            if (cs.PMinSecondSemester.HasValue && cs.PMinSecondSemester > 0)
                            {
                                baseNumP = cs.PMinSecondSemester.Value;
                            }
                        }

                        int t_mP = 0;
                        if (baseNumP > 0)
                        {
                            t_mP = markP.Where(o => o.MarkIndex < baseNumP).Count() + modelP.TotalPupil - markP.Count();
                        }

                        modelP.MissPupilNum = t_mP;

                        //V
                        //Tong so hoc sinh
                        modelV.TotalPupil = lstPoc.Count;
                        //So con diem da nhap
                        var markV = listMark.Where(x => x.ClassID == cp.ClassProfileID
                            && x.Title.Contains(SystemParamsInFile.MARK_TYPE_V)).ToList();
                        int mV = 0;
                        if (markV != null && markV.Count > 0)
                        {
                            mV = markV.Max(o => o.MarkIndex);
                        }

                        modelV.InputedNum = mV;
                        //So hoc sinh thieu diem
                        //Neu da cau hinh so con diem toi thieu
                        int baseNumV = mV;
                        if (semester == 1)
                        {
                            if (cs.VMinFirstSemester.HasValue && cs.VMinFirstSemester > 0)
                            {
                                baseNumV = cs.VMinFirstSemester.Value;
                            }
                        }
                        else
                        {
                            if (cs.VMinSecondSemester.HasValue && cs.VMinSecondSemester > 0)
                            {
                                baseNumV = cs.VMinSecondSemester.Value;
                            }
                        }

                        int t_mV = 0;
                        if (baseNumV > 0)
                        {
                            t_mV = markV.Where(o => o.MarkIndex < baseNumV).Count() + modelV.TotalPupil - markV.Count();
                        }

                        modelV.MissPupilNum = t_mV;

                        //HK
                        //Tong so hoc sinh
                        modelHK.TotalPupil = lstPoc.Count;
                        //So con diem da nhap
                        var markHK = listMark.Where(x => x.ClassID == cp.ClassProfileID
                            && x.Title.Contains(SystemParamsInFile.MARK_TYPE_HK)).ToList();
                        int mHK = 0;
                        if (markHK != null && markHK.Count > 0)
                        {
                            mHK = markHK.Max(o => o.MarkIndex);
                        }

                        modelHK.InputedNum = mHK;
                        //So hoc sinh thieu diem
                        int baseNumHK = mHK;

                        int t_mHK = 0;
                        if (baseNumHK > 0)
                        {
                            t_mHK = markHK.Where(o => o.MarkIndex < baseNumHK).Count() + modelHK.TotalPupil - markHK.Count();
                        }
                        else
                        {
                            t_mHK = modelHK.TotalPupil;
                        }

                        modelHK.MissPupilNum = t_mHK;
                    }
                }
            }

            return lstMarkInputSituation;

        }

        public List<MaxMarkNumberBO> GetMaxInputMarkOfClass(int SchoolID, int AcademicYearID, int Semester, int EducationLevelID, int ClassID = 0)
        {
            List<MaxMarkNumberBO> listMark = new List<MaxMarkNumberBO>();
            // Danh sach hoc sinh hoc trong ky
            AcademicYear ay = AcademicYearBusiness.Find(AcademicYearID);
            IQueryable<PupilOfClass> listPupil = PupilOfClassBusiness.SearchBySchool(SchoolID, new Dictionary<string, object>
            {
                {"AcademicYearID", AcademicYearID},
                {"EducationLevelID", EducationLevelID},
                {"CheckWithClass", "CheckWithClass"}
            }).AddCriteriaSemester(ay, Semester);
            List<MaxMarkNumberBO> listMark1 = (from m in VMarkRecordBusiness.SearchBySchool(SchoolID, new Dictionary<string, object>
            {
                {"AcademicYearID", AcademicYearID},
                {"EducationLevelID", EducationLevelID},
                {"ClassID",ClassID},
                {"Semester", Semester},
                {"Year", ay.Year},
                {"checkWithClassMovement","true"}
            })
                                               where listPupil.Any(o => o.PupilID == m.PupilID && o.ClassID == m.ClassID)
                                               join mt in MarkTypeBusiness.All
                                               on m.MarkTypeID equals mt.MarkTypeID
                                               group m by new { m.SubjectID, m.ClassID, mt.Title, m.PupilID } into g
                                               select new MaxMarkNumberBO
                                               {
                                                   SubjectID = g.Key.SubjectID,
                                                   ClassID = g.Key.ClassID,
                                                   MarkIndex = g.Count(),//g.Max(x => x.OrderNumber),
                                                   Title = g.Key.Title,
                                                   PupilID = g.Key.PupilID
                                               }).Distinct().ToList();
            List<MaxMarkNumberBO> listMark2 = (from m in VJudgeRecordBusiness.SearchBySchool(SchoolID, new Dictionary<string, object>
            {
                {"AcademicYearID", AcademicYearID},
                {"EducationLevelID", EducationLevelID},
                {"Semester", Semester},
                {"Year", ay.Year},
                {"checkWithClassMovement","true"}
            })
                                               where listPupil.Any(o => o.PupilID == m.PupilID && o.ClassID == m.ClassID)
                                               join mt in MarkTypeBusiness.All
                                               on m.MarkTypeID equals mt.MarkTypeID
                                               group m by new { m.SubjectID, m.ClassID, mt.Title, m.PupilID } into g
                                               select new MaxMarkNumberBO
                                               {
                                                   SubjectID = g.Key.SubjectID,
                                                   ClassID = g.Key.ClassID,
                                                   MarkIndex = g.Count(),//g.Max(x => x.OrderNumber),
                                                   Title = g.Key.Title,
                                                   PupilID = g.Key.PupilID
                                               }).Distinct().ToList();

            listMark.AddRange(listMark1);
            listMark.AddRange(listMark2);
            return listMark;
        }

        public ReportMissingMarkSituationBO GetMissingMark(
            int SchoolID, int AcademicYearID,
            int Semester, int EducationLevelID, int ClassID, int SubjectID)
        {
            AcademicYear ay = AcademicYearBusiness.Find(AcademicYearID);
            ReportMissingMarkSituationBO result = new ReportMissingMarkSituationBO();
            //Danh sach hoc sinh mien giam
            IDictionary<string, object> dicEx = new Dictionary<string, object>();
            dicEx["AcademicYearID"] = AcademicYearID;
            dicEx["EducationLevelID"] = EducationLevelID;
            dicEx["ClassID"] = ClassID;
            if (Semester == 1)
            {
                dicEx["HasFirstSemester"] = true;
            }
            if (Semester == 2)
            {
                dicEx["HasSecondSemester"] = true;
            }
            IQueryable<ExemptedSubject> lstes = ExemptedSubjectBusiness.SearchBySchool(SchoolID, dicEx);

            // Danh sach hoc sinh hoc trong kỳ
            List<PupilOfClass> listPupilInClass = PupilOfClassBusiness.SearchBySchool(SchoolID, new Dictionary<string, object>
            {
                {"AcademicYearID", AcademicYearID},
                {"EducationLevelID", EducationLevelID},
                {"ClassID",ClassID}
            }).AddCriteriaSemester(ay, Semester).ToList();
            //Lấy danh sách môn học cho lớp 
            List<ClassSubject> listClassSubject = ClassSubjectBusiness.SearchBySchool(SchoolID, new Dictionary<string, object>
            {
                {"AcademicYearID", AcademicYearID},
                {"EducationLevelID", EducationLevelID},
                {"ClassID",ClassID}
            }).ToList();

            //Lấy danh sách môn học theo khối
            List<SchoolSubject> listSchoolSubject = SchoolSubjectBusiness.SearchBySchool(SchoolID, new Dictionary<string, object>
            {
                {"AcademicYearID", AcademicYearID},
                {"EducationLevelID", EducationLevelID}
            }).OrderBy(x => x.SubjectCat.OrderInSubject).ToList();


            if (ClassID != 0)
            {
                listSchoolSubject = listSchoolSubject.Where(x =>
                    listClassSubject.Exists(y => y.ClassID == ClassID
                        && y.SubjectID == x.SubjectID
                        && (
                        (Semester == SystemParamsInFile.SEMESTER_OF_YEAR_FIRST && y.SectionPerWeekFirstSemester > 0)
                        || (Semester == SystemParamsInFile.SEMESTER_OF_YEAR_SECOND && y.SectionPerWeekSecondSemester > 0)))).ToList();
            }

            if (SubjectID != 0)
            {
                listSchoolSubject = listSchoolSubject.Where(o => o.SubjectID == SubjectID).ToList();
            }
            result.ListSubjectName = listSchoolSubject.Select(x => x.SubjectCat.DisplayName).ToList();

            var temp = PupilOfClassBusiness.SearchBySchool(SchoolID, new Dictionary<string, object>()
            {
                {"AcademicYearID", AcademicYearID},
                {"EducationLevelID", EducationLevelID},
                {"ClassID", ClassID}
           }
            ).AsEnumerable().Where(poc => listPupilInClass.Any(o => o.PupilID == poc.PupilID && o.ClassID == poc.ClassID))
            .OrderBy(x => x.ClassProfile.OrderNumber.HasValue ? x.ClassProfile.OrderNumber : 0)
            .ThenBy(x => x.ClassProfile.DisplayName).ThenBy(x => x.OrderInClass)
            .ThenBy(x => x.PupilProfile.Name)
            .ThenBy(x => x.PupilProfile.FullName);
            List<PupilOfClass> listPupil = temp.ToList();

            //Lấy thông tin điểm đã nhập của lớp
            List<MaxMarkNumberBO> listMark = GetMaxInputMarkOfClass(SchoolID, AcademicYearID, Semester, EducationLevelID, ClassID);

            //Danh sach diem hoc sinh
            IQueryable<VMarkRecord> lstMarkTemp = VMarkRecordBusiness.SearchBySchool(SchoolID, new Dictionary<string, object>
            {
                {"AcademicYearID", AcademicYearID},
                {"EducationLevelID", EducationLevelID},
                {"Semester", Semester},
                {"ClassID", ClassID},
                {"Year", ay.Year},
                {"checkWithClassMovement","true"}
            });
            //Lấy thông tin điểm đã nhập của học sinh
            List<MarkBO> listPupilMark = new List<MarkBO>();
            List<MarkBO> listPupilMark1 = (from m in lstMarkTemp
                                           join mt in MarkTypeBusiness.All
                                      on m.MarkTypeID equals mt.MarkTypeID
                                           group m by new { m.SubjectID, m.ClassID, mt.Title, m.PupilID } into g
                                           select new MarkBO
                                           {
                                               SubjectID = g.Key.SubjectID,
                                               ClassID = g.Key.ClassID,
                                               MarkIndex = g.Count(),
                                               Title = g.Key.Title,
                                               PupilID = g.Key.PupilID
                                           }).ToList();
            List<MarkBO> listPupilMark2 = (from m in VJudgeRecordBusiness.SearchBySchool(SchoolID, new Dictionary<string, object>
            {
                {"AcademicYearID", AcademicYearID},
                {"EducationLevelID", EducationLevelID},
                {"Semester", Semester},
                {"ClassID", ClassID},
                {"Year", ay.Year},
                {"checkWithClassMovement","true"}
            })
                                           join mt in MarkTypeBusiness.All
                                      on m.MarkTypeID equals mt.MarkTypeID
                                           group m by new { m.SubjectID, m.ClassID, mt.Title, m.PupilID } into g
                                           select new MarkBO
                                           {
                                               SubjectID = g.Key.SubjectID,
                                               ClassID = g.Key.ClassID,
                                               MarkIndex = g.Count(),
                                               Title = g.Key.Title,
                                               PupilID = g.Key.PupilID
                                           }).ToList();

            listPupilMark.AddRange(listPupilMark1);
            listPupilMark.AddRange(listPupilMark2);

            //Tính con điểm theo các môn
            List<MissingMarkOfPupilBO> listMoS = new List<MissingMarkOfPupilBO>();

            foreach (PupilOfClass pupilOfClass in listPupil)
            {
                MissingMarkOfPupilBO bo = new MissingMarkOfPupilBO();
                //List<MarkBO> lstMarkPP = listMark.Where(o => o.PupilID == pupilOfClass.PupilID).ToList();
                bo.PupilName = pupilOfClass.PupilProfile.FullName;

                bo.ClassName = pupilOfClass.ClassProfile.DisplayName;
                bo.Status = pupilOfClass.Status;
                List<string> listM = new List<string>();
                bo.ListMissingMark = listM;
                bool missMark = false;

                //listMoS.Add(bo);
                foreach (SchoolSubject schoolSubject in listSchoolSubject)
                {
                    //Danh sach hoc sinh duoc mien giam
                    List<int> lstpp = lstes.Where(o => o.SubjectID == schoolSubject.SubjectID).Select(o => o.PupilID).ToList();

                    //Kiểm tra lớp có học môn đó trong học kỳ x không
                    ClassSubject cs = listClassSubject.Where(
                        x => x.ClassID == pupilOfClass.ClassID
                            && x.SubjectID == schoolSubject.SubjectID
                            && (
                            (Semester == SystemParamsInFile.SEMESTER_OF_YEAR_FIRST
                            && x.SectionPerWeekFirstSemester.GetValueOrDefault() > 0)
                            ||
                            (Semester == SystemParamsInFile.SEMESTER_OF_YEAR_SECOND
                            && x.SectionPerWeekSecondSemester.GetValueOrDefault() > 0)
                            )).FirstOrDefault();

                    if (cs != null)
                    {
                        if (!lstpp.Contains(pupilOfClass.PupilID))
                        {

                            //Tính điểm 15 phút thiếu
                            int? missMarkP = 0;
                            //Danh sach so con diem da nhap
                            var lstmarkP = listMark.Where(x => (x.ClassID == pupilOfClass.ClassID
                                && x.SubjectID == schoolSubject.SubjectID
                                && x.Title.Contains(SystemParamsInFile.MARK_TYPE_P))
                                ).ToList();

                            int? baseNumP = null;
                            if (lstmarkP != null && lstmarkP.Count > 0)
                            {
                                int markP = lstmarkP.Max(o => o.MarkIndex);

                                baseNumP = markP;

                            }

                            if (Semester == 1)
                            {
                                if (cs.PMinFirstSemester.HasValue && cs.PMinFirstSemester > 0)
                                {
                                    baseNumP = cs.PMinFirstSemester.Value;
                                }
                            }
                            else
                            {
                                if (cs.PMinSecondSemester.HasValue && cs.PMinSecondSemester > 0)
                                {
                                    baseNumP = cs.PMinSecondSemester.Value;
                                }
                            }

                            if (baseNumP.HasValue)
                            {
                                MarkBO mMarkP = listPupilMark.Find(x => (x.ClassID == pupilOfClass.ClassID
                                && x.SubjectID == schoolSubject.SubjectID
                                && x.Title.Contains(SystemParamsInFile.MARK_TYPE_P)
                                && x.PupilID == pupilOfClass.PupilID));
                                //Lay so con diem toi da tru di so con diem da nhap
                                if (mMarkP != null)
                                {
                                    missMarkP = baseNumP - mMarkP.MarkIndex;
                                }
                                else
                                {
                                    missMarkP = baseNumP;
                                }
                            }

                            if (missMarkP < 0) missMarkP = 0;

                            listM.Add(missMarkP.ToString());
                            if (missMarkP > 0) missMark = true;

                            //Tính điểm 1 tiết thiếu
                            int? missMarkV = 0;
                            //Danh sach so con diem da nhap
                            var lstmarkV = listMark.Where(x => (x.ClassID == pupilOfClass.ClassID
                                && x.SubjectID == schoolSubject.SubjectID
                                && x.Title.Contains(SystemParamsInFile.MARK_TYPE_V))).ToList();
                            int? baseNumV = null;
                            if (lstmarkV != null && lstmarkV.Count > 0)
                            {
                                int markV = lstmarkV.Max(o => o.MarkIndex);
                                baseNumV = markV;

                            }
                            if (Semester == 1)
                            {
                                if (cs.VMinFirstSemester.HasValue && cs.VMinFirstSemester > 0)
                                {
                                    baseNumV = cs.VMinFirstSemester.Value;
                                }
                            }
                            else
                            {
                                if (cs.VMinSecondSemester.HasValue && cs.VMinSecondSemester > 0)
                                {
                                    baseNumV = cs.VMinSecondSemester.Value;
                                }
                            }

                            if (baseNumV.HasValue)
                            {
                                MarkBO mMarkV = listPupilMark.Find(x => (x.ClassID == pupilOfClass.ClassID
                                && x.SubjectID == schoolSubject.SubjectID
                                && x.Title.Contains(SystemParamsInFile.MARK_TYPE_V)
                                && x.PupilID == pupilOfClass.PupilID));
                                //Lay so con diem toi da tru di so con diem da nhap
                                if (mMarkV != null)
                                {
                                    missMarkV = baseNumV - mMarkV.MarkIndex;
                                }
                                else
                                {
                                    missMarkV = baseNumV;
                                }
                            }
                            if (missMarkV < 0) missMarkV = 0;

                            listM.Add(missMarkV.ToString());
                            if (missMarkV > 0) missMark = true;

                            //Tính điểm Hoc ky
                            int? missMarkHK = 0;
                            //Danh sach so con diem da nhap
                            var lstmarkHK = listMark.Where(x => (x.ClassID == pupilOfClass.ClassID
                                && x.SubjectID == schoolSubject.SubjectID
                                && x.Title.Contains(SystemParamsInFile.MARK_TYPE_HK))).ToList();
                            if (lstmarkHK != null && lstmarkHK.Count > 0)
                            {
                                MarkBO mMarkHK = listPupilMark.Find(x => (x.ClassID == pupilOfClass.ClassID
                                && x.SubjectID == schoolSubject.SubjectID
                                && x.Title.Contains(SystemParamsInFile.MARK_TYPE_HK)
                                && x.PupilID == pupilOfClass.PupilID));
                                //Lay so con diem toi da tru di so con diem da nhap
                                if (mMarkHK == null)
                                {
                                    missMarkHK = 1;
                                }

                            }
                            else
                            {
                                missMarkHK = 1;
                            }
                            listM.Add(missMarkHK.ToString());
                            if (missMarkHK > 0) missMark = true;
                        }
                        else
                        {
                            listM.Add("MG");
                            listM.Add("MG");
                            listM.Add("MG");
                        }
                    }
                    else
                    {
                        listM.Add("");
                        listM.Add("");
                        listM.Add("");
                    }

                }
                if (missMark)
                {
                    listMoS.Add(bo);
                }
            }
            result.ListPupil = listMoS;
            return result;
        }

        #region Bao cao tinh hinh nhap diem
        /// <summary>
        ///Lay mang bam cho cac tham so dau vao
        /// </summary>
        /// <param name="entity"></param>
        /// <returns></returns>
        public string GetHashKey(IDictionary<string, object> dic)
        {
            return ReportUtils.GetHashKey(dic);
        }

        public ProcessedReport GetMarkInputSituationReport(IDictionary<string, object> dic)
        {
            string reportCode = SystemParamsInFile.REPORT_MARK_INPUT_SITUATION;
            string inputParameterHashKey = GetHashKey(dic);
            return ProcessedReportBusiness.GetProcessedReport(reportCode, inputParameterHashKey);
        }

        public Stream CreateMarkInputSituationReport(IDictionary<string, object> dic)
        {

            int academicYearID = Utils.GetInt(dic["AcademicYearID"]);
            int schoolID = Utils.GetInt(dic["SchoolID"]);
            int partition = UtilsBusiness.GetPartionId(schoolID);
            int appliedLevel = Utils.GetInt(dic["AppliedLevel"]);
            int semester = Utils.GetInt(dic["Semester"]);
            int educationLevel = Utils.GetInt(dic["EducationLevel"]);
            int subjectID = Utils.GetInt(dic["SubjectID"]);

            //Lấy đường dẫn báo cáo
            string reportCode = SystemParamsInFile.REPORT_MARK_INPUT_SITUATION;
            ReportDefinition rp = ReportDefinitionBusiness.GetByCode(reportCode);
            string templatePath = SystemParamsInFile.TEMPLATE_FOLDER + rp.TemplateDirectory + "/" + rp.TemplateName;
            //Khởi tạo
            IVTWorkbook oBook = VTExport.OpenWorkbook(templatePath);

            //Lấy ra sheet đầu tiên
            IVTWorksheet sheet1 = oBook.GetSheet(1);
            IVTWorksheet sheet2 = oBook.GetSheet(2);
            IVTRange templateRange = sheet2.GetRange("B1", "B5");

            //Dien thong tin chung vao tieu de bao cao
            AcademicYear academicYear = AcademicYearBusiness.Find(academicYearID);
            SchoolProfile school = SchoolProfileBusiness.Find(schoolID);

            string schoolName = school.SchoolName.ToUpper();
            string supervisingDeptName = UtilsBusiness.GetSupervisingDeptName(schoolID, appliedLevel);
            string provinceName = school.Province.ProvinceName;
            string strSemester = "HỌC KỲ " + semester;
            string strAcademicYear = "NĂM HỌC " + academicYear.DisplayTitle;
            string title = "BẢNG THỐNG KÊ TÌNH HÌNH NHẬP ĐIỂM " + strSemester + ", " + strAcademicYear;

            //Dien du lieu vao phan tieu de
            sheet1.SetCellValue("B2", supervisingDeptName);
            sheet1.SetCellValue("B3", schoolName);
            sheet1.SetCellValue("B5", title);
            sheet1.SetCellValue("B6", "KHỐI " + educationLevel);

            //Fill cac lop
            IDictionary<string, object> tmpDic = new Dictionary<string, object>();

            tmpDic.Add("EducationLevelID", educationLevel);
            tmpDic.Add("SchoolID", schoolID);
            List<ClassProfile> lstClass = ClassProfileBusiness.SearchByAcademicYear(academicYearID, tmpDic)
                .OrderBy(p => p.OrderNumber.HasValue ? p.OrderNumber : 0).ThenBy(o => o.DisplayName).ToList();

            int headerRow = 8;
            int startHeaderCol = 4;
            int headerCol = startHeaderCol;
            for (int i = 0; i < lstClass.Count; i++)
            {
                sheet1.SetCellValue(headerRow + 1, headerCol, lstClass[i].DisplayName);
                sheet1.SetCellValue(headerRow + 2, headerCol, "Số con điểm đã nhập");
                sheet1.SetColumnWidth(headerCol, 6);
                sheet1.SetCellValue(headerRow + 2, headerCol + 1, "Số HS thiếu điểm/ TS");
                sheet1.SetColumnWidth(headerCol + 1, 6);
                sheet1.GetRange(headerRow + 1, headerCol, headerRow + 1, headerCol + 1).Merge();

                headerCol += 2;
            }
            if (lstClass.Count > 0)
            {
                sheet1.GetRange(headerRow, startHeaderCol, headerRow, headerCol - 1).Merge();
                sheet1.SetCellValue(headerRow, startHeaderCol, "Lớp");
            }

            //Lay danh sach mon hoc
            tmpDic = new Dictionary<string, object>();
            tmpDic["SchoolID"] = schoolID;
            tmpDic["EducationLevelID"] = educationLevel;
            tmpDic["AcademicYearID"] = academicYearID;
            tmpDic["AppliedLevel"] = appliedLevel;
            tmpDic["SubjectID"] = subjectID;
            tmpDic["IsVNEN"] = true;

            List<SubjectCatBO> lstSubject = ClassSubjectBusiness.SearchBySchool(schoolID, tmpDic)
                                            .Select(o => new SubjectCatBO
                                            {
                                                SubjectCatID = o.SubjectID,
                                                DisplayName = o.SubjectCat.DisplayName
                                            }).Distinct().ToList();


            //Lay ra danh sach phan cong giang day
            tmpDic = new Dictionary<string, object>();
            tmpDic["EducationLevel"] = educationLevel;
            tmpDic["AcademicYearID"] = academicYearID;
            tmpDic["AppliedLevel"] = appliedLevel;
            tmpDic["SchoolID"] = schoolID;
            tmpDic["Semester"] = semester;
            tmpDic["IsActive"] = true;
            List<TeachingAssignment> lstTa = TeachingAssignmentBusiness.SearchBySchool(schoolID, tmpDic).ToList();

            //Lay danh sach mon hoc cho lop
            tmpDic = new Dictionary<string, object>();
            tmpDic["SchoolID"] = schoolID;
            tmpDic["EducationLevelID"] = educationLevel;
            tmpDic["AcademicYearID"] = academicYearID;
            tmpDic["AppliedLevel"] = appliedLevel;
            tmpDic["Semester"] = semester;
            tmpDic["IsVNEN"] = true;
            List<ClassSubject> lstCs = ClassSubjectBusiness.SearchBySchool(schoolID, tmpDic).ToList();

            //Lấy thông tin học sinh đang học
            AcademicYear aca = AcademicYearBusiness.Find(academicYearID);
            tmpDic = new Dictionary<string, object>();
            tmpDic["AcademicYearID"] = academicYearID;
            tmpDic["EducationLevelID"] = educationLevel;
            tmpDic["CheckWithClass"] = "CheckWithClass";
            List<PupilOfClass> listPupilOfClass = PupilOfClassBusiness.SearchBySchool(schoolID, tmpDic).AddCriteriaSemester(aca, semester).ToList();

            //Lấy thông tin điểm 
            List<MaxMarkNumberBO> listMark = this.GetMaxInputMarkOfClass(schoolID, academicYearID, semester, educationLevel).ToList();

            //Tao tung sheet cho moi mon
            for (int i = 0; i < lstSubject.Count; i++)
            {
                SubjectCatBO subject = lstSubject[i];

                IVTWorksheet sheet = oBook.CopySheetToLast(sheet1, "AZ1000");

                List<MarkInputSituationBO> lstMis = this.GetMarkInputSituation(subject.SubjectCatID, semester, lstClass, lstTa, lstCs, listPupilOfClass, listMark, aca);

                int startRow = 11;
                int lastRow = startRow + lstMis.Count * 4 - 1;
                int curRow = startRow;
                int startColumn = 1;
                int curColumn = startColumn;

                for (int j = 0; j < lstMis.Count; j++)
                {
                    int startMisRow = curRow;
                    MarkInputSituationBO mis = lstMis[j];
                    //STT
                    sheet.SetCellValue(curRow, curColumn, j + 1);
                    curColumn++;

                    //Giao vien
                    sheet.SetCellValue(curRow, curColumn, mis.TeacherFullName);
                    curColumn++;

                    //M
                    sheet.SetCellValue(curRow, curColumn, "M");
                    curColumn++;
                    for (int k = 0; k < lstClass.Count; k++)
                    {
                        ClassProfile cp = lstClass[k];
                        ClassSituationBO cs = mis.lstMarkM.Where(o => o.ClassID == cp.ClassProfileID).FirstOrDefault();
                        if (cs != null)
                        {
                            sheet.SetCellValue(curRow, curColumn, cs.InputedNum);
                            sheet.SetCellValue(curRow, curColumn + 1, cs.StrMissDivTotal);
                        }
                        curColumn += 2;
                    }
                    curRow++;
                    curColumn = 3;

                    //P
                    sheet.SetCellValue(curRow, curColumn, "15P");
                    curColumn++;
                    for (int k = 0; k < lstClass.Count; k++)
                    {
                        ClassProfile cp = lstClass[k];
                        ClassSituationBO cs = mis.lstMarkP.Where(o => o.ClassID == cp.ClassProfileID).FirstOrDefault();
                        if (cs != null)
                        {
                            sheet.SetCellValue(curRow, curColumn, cs.InputedNum);
                            sheet.SetCellValue(curRow, curColumn + 1, cs.StrMissDivTotal);
                        }
                        curColumn += 2;
                    }
                    curRow++;
                    curColumn = 3;

                    //V
                    sheet.SetCellValue(curRow, curColumn, "1T");
                    curColumn++;
                    for (int k = 0; k < lstClass.Count; k++)
                    {
                        ClassProfile cp = lstClass[k];
                        ClassSituationBO cs = mis.lstMarkV.Where(o => o.ClassID == cp.ClassProfileID).FirstOrDefault();
                        if (cs != null)
                        {
                            sheet.SetCellValue(curRow, curColumn, cs.InputedNum);
                            sheet.SetCellValue(curRow, curColumn + 1, cs.StrMissDivTotal);
                        }
                        curColumn += 2;
                    }
                    curRow++;
                    curColumn = 3;

                    //HK
                    sheet.SetCellValue(curRow, curColumn, "HK");
                    curColumn++;
                    for (int k = 0; k < lstClass.Count; k++)
                    {
                        ClassProfile cp = lstClass[k];
                        ClassSituationBO cs = mis.lstMarkHK.Where(o => o.ClassID == cp.ClassProfileID).FirstOrDefault();
                        if (cs != null)
                        {
                            sheet.SetCellValue(curRow, curColumn, cs.InputedNum);
                            sheet.SetCellValue(curRow, curColumn + 1, cs.StrMissDivTotal);
                        }
                        curColumn += 2;
                    }

                    sheet.GetRange(startMisRow, 1, curRow, 1).Merge();
                    sheet.GetRange(startMisRow, 2, curRow, 2).Merge();
                    sheet.GetRange(startMisRow, 1, curRow, curColumn - 1).SetBorder(VTBorderStyle.Solid, VTBorderIndex.All);
                    sheet.GetRange(startMisRow, 3, curRow, curColumn - 1).SetBorder(VTBorderStyle.Dotted, VTBorderIndex.InsideHorizontal);

                    curRow++;
                    curColumn = startColumn;
                }

                sheet.GetRange(8, 1, 10, headerCol - 1).SetBorder(VTBorderStyle.Solid, VTBorderWeight.Medium, VTBorderIndex.Around);
                sheet.GetRange(8, 1, 10, headerCol - 1).SetBorder(VTBorderStyle.Solid, VTBorderWeight.Thin, VTBorderIndex.InsideAll);

                //Fill footer
                sheet.CopyPaste(templateRange, curRow + 1, 2);
                sheet.GetRange(curRow + 1, 2, curRow + 5, 2).Range.WrapText = false;

                sheet.Orientation = VTXPageOrientation.VTxlPortrait;
                sheet.FitAllColumnsOnOnePage = true;
                sheet.Name = subject.DisplayName;
            }

            //Xoa sheet dau tien
            sheet1.Delete();
            sheet2.Delete();

            return oBook.ToStream();
        }

        public ProcessedReport InsertMarkInputSituationReport(IDictionary<string, object> dic, Stream data)
        {
            string reportCode = SystemParamsInFile.REPORT_MARK_INPUT_SITUATION;

            ProcessedReport pr = new ProcessedReport();

            ReportDefinition reportDef = ReportDefinitionBusiness.GetByCode(reportCode);
            pr.ReportCode = reportCode;
            pr.ProcessedDate = DateTime.Now;
            pr.InputParameterHashKey = GetHashKey(dic);
            pr.ReportData = ReportUtils.Compress(data);

            //Tạo tên file
            string outputNamePattern = reportDef.OutputNamePattern;

            int appliedLevel = Utils.GetInt(dic["AppliedLevel"]);
            string strAppliedLevel = ReportUtils.ConvertAppliedLevelForReportName(appliedLevel);
            outputNamePattern = outputNamePattern.Replace("[AppliedLevel]", ReportUtils.StripVNSign(strAppliedLevel));

            int educationLevel = Utils.GetInt(dic["EducationLevel"]);
            outputNamePattern = outputNamePattern.Replace("[EducationLevel]", educationLevel.ToString());

            int semester = Utils.GetInt(dic["Semester"]);
            string strSemester = semester == GlobalConstants.SEMESTER_OF_YEAR_FIRST ? "HKI" : "HKII";
            outputNamePattern = outputNamePattern.Replace("[Semester]", strSemester);

            pr.ReportName = ReportUtils.RemoveSpecialCharacters(outputNamePattern) + "." + reportDef.OutputFormat;

            ProcessedReportParameterRepository.Insert(dic, pr);
            ProcessedReportRepository.Insert(pr);
            ProcessedReportRepository.Save();
            return pr;
        }

        #endregion

        #region Bao cao danh sach thieu diem
        public Stream CreateReportPupilNotEnoughMark(int SchoolID, int AcademicYearID, int Semester, int EducationLevelID, int ClassID, int SubjectID, int AppliedLevelID = 0)
        {
            EducationLevel Edu = EducationLevelBusiness.Find(EducationLevelID);
            AcademicYear Aca = AcademicYearBusiness.Find(AcademicYearID);
            SchoolProfile sp = SchoolProfileBusiness.Find(SchoolID);
            SemeterDeclaration sd = SemeterDeclarationBusiness.Find(Semester);
            ReportMissingMarkSituationBO data = this.GetMissingMark(
                SchoolID, AcademicYearID, Semester, EducationLevelID, ClassID, SubjectID);
            SheetData SheetData = new SheetData("Danh sách học sinh thiếu điểm");
            SheetData.Data["AcademicYear"] = Aca.DisplayTitle; ;
            SheetData.Data["SchoolName"] = sp.SchoolName.ToUpper();
            SheetData.Data["ProvinceName"] = sp.Province.ProvinceName;
            SheetData.Data["SupervisingDeptName"] = UtilsBusiness.GetSupervisingDeptName(SchoolID, AppliedLevelID);
            SheetData.Data["EducationLevelName"] = Edu.Resolution.ToUpper();
            SheetData.Data["Semester"] = "HỌC KỲ " + Semester;

            ReportDefinition reportDef = ReportDefinitionBusiness.GetByCode(SystemParamsInFile.REPORT_MISSING_MARK);
            string templatePath = ReportUtils.GetTemplatePath(reportDef);

            IVTWorkbook oBook = VTExport.OpenWorkbook(templatePath);
            IVTWorksheet Template = oBook.GetSheet(1);

            Template.FillVariableValue(SheetData.Data);
            int StartRow = 8;
            int StartCol = 1;
            if (data.ListSubjectName.Count > 0)
            {
                Template.SetCellValue(StartRow, StartCol + 3, data.ListSubjectName[0]);
                IVTRange subjectRange = Template.GetRange("D8", "F10");
                for (int j = 1; j < data.ListSubjectName.Count; j++)
                {
                    Template.CopyPaste(subjectRange, StartRow, StartCol + 3 + j * 3);
                    Template.SetCellValue(StartRow, StartCol + 3 + j * 3, data.ListSubjectName[j]);
                }
            }

            //Tạo Sheet fill dữ liệu
            IVTWorksheet Sheet = oBook.GetSheet(2);
            Sheet.CopyPasteSameSize(Template.GetRange(1, 1, StartRow + 1, 100), 1, 1);

            IVTRange rangeSubject = Template.GetRange(StartRow + 2, 1, StartRow + 2, 100);
            for (int i = 0; i < data.ListPupil.Count; i++)
            {

                MissingMarkOfPupilBO bo = data.ListPupil[i];

                Sheet.CopyPaste(rangeSubject, StartRow + i + 2, 1);
                if (bo.Status != SystemParamsInFile.PUPIL_STATUS_STUDYING && bo.Status != SystemParamsInFile.PUPIL_STATUS_GRADUATED)
                {
                    // Gach ngang va boi do voi cac hoc sinh co trang thai khac dang hoc va khac da tot nghiep
                    //Sheet.GetRow(StartRow + i + 2).SetFontColour(System.Drawing.Color.Red);
                    Sheet.GetRow(StartRow + i + 2).SetFontStyle(false, System.Drawing.Color.Red, false, null, true, false);
                }
                Sheet.SetCellValue(StartRow + i + 2, 1, i + 1);
                Sheet.SetCellValue(StartRow + i + 2, 2, bo.ClassName);
                Sheet.SetCellValue(StartRow + i + 2, 3, bo.PupilName);
                List<string> listTemp = bo.ListMissingMark;
                Sheet.FillDataHorizon(listTemp, StartRow + i + 2, 4);
            }
            Sheet.GetRange(8, 1, 9, data.ListSubjectName.Count * 3 + 3).SetBorder(VTBorderStyle.Solid, VTBorderWeight.Medium, VTBorderIndex.Around);
            //Fill ghi chu
            Sheet.SetCellValue(data.ListPupil.Count + 11, 1, "Ghi chú: con số tương ứng với từng loại điểm");
            Sheet.Worksheet.Cells[data.ListPupil.Count + 11, 1].Font.Bold = false;
            Sheet.Worksheet.Cells[data.ListPupil.Count + 11, 1].Characters[1, "Ghi chú:".Length].Font.Bold = true;
            Sheet.SetCellValue(data.ListPupil.Count + 12, 1, "'- Là số con điểm còn thiếu so với quy định số con điểm tối thiểu của môn học");
            Sheet.SetCellValue(data.ListPupil.Count + 13, 1, "'- Hoặc là số con điểm còn thiếu so với số con điểm lớn nhất đã nhập với từng loại điểm cho môn học đó của lớp ");
            Sheet.SetCellValue(data.ListPupil.Count + 14, 1, "'(trong trường hợp không cấu hình số con điểm tối thiểu)");

            Template.Delete();
            string sheetName = Edu.Resolution;
            Sheet.Name = sheetName;
            return oBook.ToStream();
        }

        public ProcessedReport InsertReportPupilNotEnoughMark(IDictionary<string, object> dic, Stream data)
        {
            string reportCode = SystemParamsInFile.REPORT_MISSING_MARK;

            ProcessedReport pr = new ProcessedReport();

            ReportDefinition reportDef = ReportDefinitionBusiness.GetByCode(reportCode);
            pr.ReportCode = reportCode;
            pr.ProcessedDate = DateTime.Now;
            pr.InputParameterHashKey = GetHashKey(dic);
            pr.ReportData = ReportUtils.Compress(data);

            //Tạo tên file
            string outputNamePattern = reportDef.OutputNamePattern;

            int appliedLevel = Utils.GetInt(dic["AppliedLevel"]);
            string strAppliedLevel = ReportUtils.ConvertAppliedLevelForReportName(appliedLevel);
            outputNamePattern = outputNamePattern.Replace("[AppliedLevel]", ReportUtils.StripVNSign(strAppliedLevel));

            int educationLevel = Utils.GetInt(dic["EducationLevel"]);
            outputNamePattern = outputNamePattern.Replace("[EducationLevel]", educationLevel.ToString());

            int semester = Utils.GetInt(dic["Semester"]);
            string strSemester = semester == GlobalConstants.SEMESTER_OF_YEAR_FIRST ? "HKI" : "HKII";
            outputNamePattern = outputNamePattern.Replace("[Semester]", strSemester);

            pr.ReportName = ReportUtils.RemoveSpecialCharacters(outputNamePattern) + "." + reportDef.OutputFormat;

            ProcessedReportParameterRepository.Insert(dic, pr);
            ProcessedReportRepository.Insert(pr);
            ProcessedReportRepository.Save();
            return pr;
        }
        #endregion
    }
}
