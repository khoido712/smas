/**
 * The Viettel License
 *
 * Copyright 2012 Viettel Telecom. All rights reserved.
 * VIETTEL PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */

/** 
* @author  
* @version $Revision: $
*/

using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Linq.Expressions;
using System.Web;
using log4net;

using SMAS.DAL.Repository;
using SMAS.DAL.IRepository;
using SMAS.Business.IBusiness;
using SMAS.Models.Models;
using SMAS.Business.Common;
using SMAS.Business.BusinessObject;
using System.Transactions;

namespace SMAS.Business.Business
{
    public partial class ReceivedRecordBusiness
    {
        public List<SMS_RECEIVED_RECORD> GetListReceiRecord(Dictionary<string, object> dic)
        {
            IQueryable<SMS_RECEIVED_RECORD> iqReceiveRecord = this.ReceivedRecordBusiness.All;
            short type = Utils.GetShort(dic, "Type");
            DateTime dateTimeNow = Utils.GetDateTime(dic, "dateTimeNow").Value;
            int month = dateTimeNow.Month;
            int year = dateTimeNow.Year;
            iqReceiveRecord = iqReceiveRecord.Where(rr => rr.RECEIVED_YEAR == year && rr.RECEIVED_MONTH == month);
            iqReceiveRecord = iqReceiveRecord.Where(rr => rr.RECEIVED_TYPE == type);
            if (type == GlobalConstantsEdu.COMMON_HISTORY_RECEIVER_TEACHER_ID)//nguoi nhan la giao vien
            {
                List<int> lstTeacherReceived = Utils.GetIntList(dic, "lstTeacherReceived");
                int schoolId = Utils.GetInt(dic, "SchoolID");
                int partitionId = UtilsBusiness.GetPartionId(schoolId);
                iqReceiveRecord = iqReceiveRecord.Where(rr => rr.PARTITION_ID == partitionId
                                                        && rr.SCHOOL_ID == schoolId
                                                        && rr.RECEIVED_TYPE == GlobalConstantsEdu.COMMON_HISTORY_RECEIVER_TEACHER_ID
                                                        && lstTeacherReceived.Contains(rr.EMPLOYEE_ID));
            }
            else if (type == GlobalConstantsEdu.COMMON_HISTORY_RECEIVER_HEAD_MASTER_ID)//nguoi nhan la hieu truong
            {
                List<int> lstSchoolID = Utils.GetIntList(dic, "lstSchoolID");
                iqReceiveRecord = iqReceiveRecord.Where(p => lstSchoolID.Contains(p.SCHOOL_ID) && p.RECEIVED_TYPE == GlobalConstantsEdu.COMMON_HISTORY_RECEIVER_HEAD_MASTER_ID);
            }
            else if (type == GlobalConstantsEdu.COMMON_HISTORY_RECEIVER_EMPLOYEE_ID)//nguoi nhan la nhan vien phong ban
            {
                int SupervisingDeptID = Utils.GetInt(dic, "SupervisingDeptID");
                List<int> lstEmployeeId = Utils.GetIntList(dic, "LstEmployeeID");
                int partitionId = UtilsBusiness.GetPartionId(SupervisingDeptID);
                iqReceiveRecord = iqReceiveRecord.Where(p => p.SUPER_VISING_DEPT_ID == SupervisingDeptID && p.PARTITION_ID == partitionId
                                                        && p.RECEIVED_TYPE == GlobalConstantsEdu.COMMON_HISTORY_RECEIVER_EMPLOYEE_ID
                                                        );
                if (lstEmployeeId.Count > 0)
                {
                    iqReceiveRecord = iqReceiveRecord.Where(r => lstEmployeeId.Contains(r.EMPLOYEE_ID));
                }
            }
            return iqReceiveRecord.ToList();
        }

    }
}
