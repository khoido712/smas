﻿/**
 * The Viettel License
 *
 * Copyright 2012 Viettel Telecom. All rights reserved.
 * VIETTEL PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */

/** 
* @author  
* @version $Revision: $
*/

using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Linq.Expressions;
using System.Web;
using log4net;
using SMAS.DAL.Repository;
using SMAS.DAL.IRepository;
using SMAS.Business.IBusiness;
using SMAS.Models.Models;
using SMAS.Business.Common;
using SMAS.Business.BusinessObject;
using System.Transactions;
using SMAS.VTUtils.Log;
using System.Data.Objects;
using SMAS.Business.Common.Extension;
using System.Data.Entity.Validation;
using System.Web.Configuration;
using System.Text;
using System.Text.RegularExpressions;

namespace SMAS.Business.Business
{
    public partial class SMSHistoryBusiness
    {
        public IQueryable<SMS_HISTORY> Search(IDictionary<string,object> dic)
        {
            int SchoolID = Utils.GetInt(dic, "SchoolID");
            int AcademicYearID = Utils.GetInt(dic, "AcademicYearID");
            Guid? SMSTimmerConfigID = Utils.GetGuid(dic, "TimmerConfigID");
            int ReceiveType = Utils.GetInt(dic, "ReceiveType");
            IQueryable<SMS_HISTORY> iquery = SMSHistoryBusiness.All;
            if (SchoolID > 0)
            {
                int PartitionID = UtilsBusiness.GetPartionId(SchoolID);
                iquery = iquery.Where(p => p.SCHOOL_ID == SchoolID && p.PARTITION_ID == PartitionID);
            }
            if (AcademicYearID > 0)
            {
                iquery = iquery.Where(p => p.ACADEMIC_YEAR_ID == AcademicYearID);
            }
            if (SMSTimmerConfigID != null)
            {
                iquery = iquery.Where(p => p.SMS_TIMER_CONFIG_ID == SMSTimmerConfigID);
            }
            if (ReceiveType > 0)
            {
                iquery = iquery.Where(p => p.RECEIVE_TYPE == ReceiveType);
            }
            return iquery;
        }
        public ResultBO SendSMS(Dictionary<string, object> dic)
        {
            ResultBO resultBO = new ResultBO();
            
            try
            {
                SetAutoDetectChangesEnabled(false);
                string content = Utils.GetString(dic,"content");
                bool isSendImport = Utils.GetBool(dic, "isSendImport", false);
                int academicYearID = Utils.GetInt(dic,"academicYearID");
                int semester = Utils.GetInt(dic,"semester");
                int schoolID = Utils.GetInt(dic,"schoolID");
                string UserName = Utils.GetString(dic,"UserName");
                int appliedLevelID = Utils.GetInt(dic,"AppliedLevelID");
                List<int> lstClassID = Utils.GetIntList(dic, "lstClassID");
                SchoolProfile school = SchoolProfileBusiness.Find(schoolID);
                List<string> contentList = Utils.GetStringList(dic,"lstContent");
                AcademicYear objAy = AcademicYearBusiness.Find(academicYearID);
                SMS_SCHOOL_CONFIG schoolCfg = SchoolConfigBusiness.All.Where(p => p.SCHOOL_ID == schoolID).FirstOrDefault();
                //neu khong phai truong hop noi dung tin nhan khac nhau doi voi moi receiver thi check content
                if (contentList.Count == 0)
                {
                    if (!isSendImport && (string.IsNullOrWhiteSpace(content) || content.Length > GlobalConstantsEdu.COMMON_TABLE_SMSHISTORY_CONTENT_MAXLENGTH))
                    {
                        resultBO.isError = true;
                        resultBO.ErrorMsg = ResEdu.Get("SendSMS_Label_InvalidContent");
                        return resultBO;
                    }

                    List<string> lstInputedSensitiveWord = new List<string>();
                    if (content.CheckContentSMS(ref lstInputedSensitiveWord))
                    {
                        resultBO.isError = true;
                        resultBO.ErrorMsg = string.Format(ResEdu.Get("SendSMS_Label_Sensitive_Words"), string.Join(", ", lstInputedSensitiveWord));
                        return resultBO;
                    }
                }
                int type = Utils.GetInt(dic,"type");
                DateTime dateTimeNow = DateTime.Now;
                if (type != GlobalConstantsEdu.COMMON_SMS_COMUNICATION_TEACHER_TO_TEACHER)
                {
                    if (semester == 0)
                    {
                        resultBO.isError = true;
                        resultBO.ErrorMsg = ResEdu.Get("SendSMS_Label_SemesterError");
                        return resultBO;
                    }
                }

                dic["year"] = objAy.Year;
                //check quyen user                            
                dic["dateTime"] = dateTimeNow;
                bool inValidNumber = false;
                string contentStripVNSign = ExtensionMethods.ConvertLineToSpace(content.StripVNSign().StripVNSignComposed());
                int senderID = Utils.GetInt(dic,"senderID");
                List<int> lstReceiverID = Utils.GetIntList(dic,"lstReceiverID");
                int contactGroupID = Utils.GetInt(dic,"contactGroupID");
                int classID = Utils.GetInt(dic,"classID");
                string shortContent = Utils.GetString(dic,"shortContent").Trim();
                bool isPrincipal = Utils.GetBool(dic,"isPrincipal", false);
                bool isAdmin = Utils.GetBool(dic, "isAdmin", false);
                int? typeHistory = Utils.GetInt(dic,"typeHistory");
                bool isCustomType = Utils.GetBool(dic, "IsCustomType", false);
                int iEducationLevelID = Utils.GetInt(dic,"iEducationLevelID");
                bool sendAllSchool = Utils.GetBool(dic, "BoolSendAllSchool", false);
                bool sendAllEdu = Utils.GetBool(dic, "isSendAllEdu", false);
                int iLevelID = Utils.GetInt(dic,"iLevelID");
                List<string> lstShortContent = Utils.GetStringList(dic,"lstShortContent");
                long senderGroup = DateTime.Now.Ticks;
                dic["isSubmitChange"] = false;
                string schoolUserName = UserName;
                bool isSignMsg = Utils.GetBool(dic, "isSignMsg", false);
                int countSMS = content.countNumSMS(isSignMsg);
                bool isSendSMSCommentOfPupil = Utils.GetBool(dic, "isSendSMSComment", false);
                PromotionProgramBO promotion = Utils.GetObject<PromotionProgramBO>(dic, "Promotion");
                bool isAllClass = Utils.GetBool(dic, "IsAllClass", false);
                bool isAllGrid = Utils.GetBool(dic, "IsAllGrid");

                //viethd4: Gui hen gio
                bool isTimerUsed = Utils.GetBool(dic, "isTimerUsed", false);
                DateTime? sendTime = null;
                if (dic.ContainsKey("sendTime"))
                {
                    sendTime = (DateTime)dic["sendTime"];
                }
                string timerConfigName = Utils.GetString(dic,"timerConfigName");

                //viethd4: Gui tin import toan truong, toan khoi
                List<SendSMSByImportBO> lstImportSMS = new List<SendSMSByImportBO>();
                if (isSendImport && dic.ContainsKey("ListPupilImport"))
                {
                    lstImportSMS = (List<SendSMSByImportBO>)dic["ListPupilImport"];
                }

                int PartitionID = UtilsBusiness.GetPartionId(schoolID);
                SMSParentSendDetailSearchBO objSMSSendDetail = new SMSParentSendDetailSearchBO
                {
                    SchoolId = schoolID,
                    Year = objAy.Year
                };

                IQueryable<SMS_PARENT_SENT_DETAIL> qSMSParentSendDetail = SMSParentSentDetailBusiness.Search(objSMSSendDetail);
                switch (type)
                {

                    case GlobalConstantsEdu.COMMON_SMS_COMUNICATION_TEACHER_TO_PARENT:
                        {
                            #region teacher to parent

                            bool hasTimerMessage = false;
                            //17.01.2017 Danh sach brandName da dang ky
                            List<BrandNameUsingBO> lstBrandName = BrandnameRegistrationBusiness.GetBrandNameUsing(schoolID);
                            string brandName = GlobalConstantsEdu.BRANDNAME_DEFAULT;
                            // Kiem tra cap nhat cac thue bao No cuoc
                            int numDeferredPaymentDays = ConfigEdu.GetInt(GlobalConstantsEdu.DEFERRED_PAYMENT_DAYS_KEY); // So ngay no cuoc

                            var lstUnpaidContract = (from ct in SMSParentContractBusiness.All
                                                     join ctd in SMSParentContractDetailBusiness.All on ct.SMS_PARENT_CONTRACT_ID equals ctd.SMS_PARENT_CONTRACT_ID
                                                     where ct.SCHOOL_ID == schoolID && ct.PARTITION_ID == PartitionID
                                                     && ctd.YEAR_ID == objAy.Year
                                                     && ctd.SUBSCRIPTION_STATUS == GlobalConstantsEdu.STATUS_UNPAID
                                                     && ctd.IS_ACTIVE
                                                     && (ct.IS_DELETED == null || ct.IS_DELETED == false)
                                                     && EntityFunctions.DiffDays(ctd.CREATED_TIME, DateTime.Now) > numDeferredPaymentDays
                                                     select ctd).ToList();
                            foreach (var item in lstUnpaidContract)
                            {
                                item.SUBSCRIPTION_STATUS = GlobalConstantsEdu.STATUS_OVERDUE_UNPAID;
                            }

                            // Lay danh sach cac goi co gioi han - Voi cac HĐ co goi cuoc loai nay thi khong tinh vao quy tin 
                            List<SMS_SERVICE_PACKAGE_DECLARE> LstLimitPackage = (from sp in ServicePackageBusiness.All
                                                                                 join spdd in ServicePackageDeclareBusiness.All on sp.SERVICE_PACKAGE_ID equals spdd.SERVICE_PACKAGE_ID
                                                                                 where spdd.IS_LIMIT
                                                                                 && spdd.IS_LIMIT
                                                                                 select spdd).ToList();

                            if ((sendAllSchool || sendAllEdu) && (isAdmin || isPrincipal))
                            {
                                #region gui cho phhs toan truong/ hoac toan khoi
                                if (typeHistory == GlobalConstantsEdu.COMMON_HISTORYSMS_TO_COMMUNICATION_ID && isCustomType == false)
                                {
                                    #region neu la ban tin trao doi
                                    List<LevelBO> levelBOList = null;
                                    if (sendAllSchool && iEducationLevelID == 0)
                                    {
                                        levelBOList = (from e in EducationLevelBusiness.GetByGrade(iLevelID)
                                                       select new LevelBO
                                                       {
                                                           EducationLevelID = e.EducationLevelID,
                                                           EducationLevelName = e.Resolution
                                                       }).OrderBy(p => p.EducationLevelID).ToList();
                                    }
                                    // AnhVD9 20150106 - Bo sung thong tin lop de kiem tra chi gui cho lop con quy tin
                                    List<PupilOfClass> lstPupilOfClass = new List<PupilOfClass>();
                                    IDictionary<string, object> dicClass = new Dictionary<string, object>();
                                    if (!isAllGrid)//neu khong chon gui toan danh sach thi se chi gui cho nhung hoc sinh duoc chon
                                    {
                                        dicClass.Add("lstPupilID", lstReceiverID);
                                    }
                                    if (levelBOList == null)
                                    {
                                        dicClass.Add("SchoolID", schoolID);
                                        dicClass.Add("AcademicYearID", academicYearID);
                                        dicClass.Add("EducationLevelID", iEducationLevelID);
                                        lstPupilOfClass = PupilOfClassBusiness.Search(dicClass).Where(p => lstClassID.Contains(p.ClassID) && (p.Status == GlobalConstants.PUPIL_STATUS_STUDYING || p.Status == GlobalConstants.PUPIL_STATUS_GRADUATED)).Distinct().ToList();
                                        if (lstPupilOfClass == null || lstPupilOfClass.Count == 0)
                                        {
                                            resultBO.isError = false;
                                            resultBO.NumSendSuccess = 0;
                                            return resultBO;
                                        }
                                    }
                                    else
                                    {
                                        dicClass.Add("SchoolID", schoolID);
                                        dicClass.Add("AcademicYearID", academicYearID);
                                        dicClass.Add("AppliedLevel", iLevelID);
                                        lstPupilOfClass = PupilOfClassBusiness.Search(dicClass).Where(p => lstClassID.Contains(p.ClassID) && p.Status == GlobalConstants.PUPIL_STATUS_STUDYING || p.Status == GlobalConstants.PUPIL_STATUS_GRADUATED).Distinct().ToList();
                                        if (lstPupilOfClass == null || lstPupilOfClass.Count == 0)
                                        {
                                            resultBO.isError = false;
                                            resultBO.NumSendSuccess = 0;
                                            return resultBO;
                                        }
                                    }

                                    bool isSameContent = true;

                                    if (isSendImport)
                                    {
                                        List<int> lstPupilImportID = lstImportSMS.Select(o => o.PupilFileID).ToList();
                                        string firstContent = lstImportSMS.FirstOrDefault().Content;
                                        if (lstImportSMS.Count > 0 && lstImportSMS.Where(o => o.Content != firstContent).Count() > 0)
                                        {
                                            isSameContent = false;
                                        }
                                        lstPupilOfClass = lstPupilOfClass.Where(o => lstPupilImportID.Contains(o.PupilID)).ToList();

                                    }

                                    lstReceiverID = lstPupilOfClass.Select(o => o.PupilID).ToList();
                                    List<PupilContract> lstPupilContract = new List<PupilContract>();
                                    List<PupilProfileBO> pupilProfileList = new List<PupilProfileBO>();
                                    lstPupilContract = this.GetListPupilContract(schoolID, objAy.Year, semester, lstReceiverID);
                                    // lay danh sach chua giao vien chu nhiem
                                    lstReceiverID = lstPupilContract.Select(p => p.PupilFileID).ToList();
                                    pupilProfileList.AddRange(PupilProfileBusiness.GetListPupilSendSMS(lstReceiverID, academicYearID));
                                    List<int> lstPupilFileID = lstPupilContract.Select(p => p.PupilFileID).Distinct().ToList();
                                    SMS_PARENT_CONTRACT_CLASS objClassDetail = null;
                                    List<SMS_PARENT_CONTRACT_CLASS> lstClassDetails = SMSParentContractInClassDetailBusiness.All.Where(c => c.ACADEMIC_YEAR_ID == objAy.AcademicYearID
                                                    && c.SCHOOL_ID == schoolID && c.YEAR == objAy.Year && (c.SEMESTER == semester || c.SEMESTER == GlobalConstantsEdu.SEMESTER_OF_YEAR_ALL)).ToList();
                                    List<int> lstPupilIDInClass = new List<int>();
                                    List<int> lstClassSent = new List<int>();
                                    List<ClassBO> lstClassNotSent = new List<ClassBO>();
                                    ClassBO classNotSend = null;
                                    List<SMS_PARENT_CONTRACT_CLASS> lstClassDetailstmp = null;
                                    int vClassID = 0;
                                    for (int i = lstClassID.Count - 1; i >= 0; i--)
                                    {
                                        vClassID = lstClassID[i];
                                        lstPupilIDInClass = lstPupilOfClass.Where(o => o.ClassID == vClassID).Select(o => o.PupilID).ToList();

                                        lstClassDetailstmp = lstClassDetails.Where(o => o.CLASS_ID == vClassID).ToList();
                                        if (lstClassDetailstmp.Count == 0)
                                        {
                                            int TotalSMSInClass = lstPupilContract.Where(o => lstPupilIDInClass.Contains(o.PupilFileID) && o.IsLimitPackage != true
                                                && o.Status == GlobalConstantsEdu.STATUS_REGISTED).Sum(o => o.TotalSMS);
                                            int TotalSMSSent = lstPupilContract.Where(o => lstPupilIDInClass.Contains(o.PupilFileID) && o.IsLimitPackage != true
                                                && o.Status == GlobalConstantsEdu.STATUS_REGISTED).Sum(o => o.SentSMS);
                                            objClassDetail = new SMS_PARENT_CONTRACT_CLASS();
                                            objClassDetail.CLASS_ID = vClassID;
                                            objClassDetail.ACADEMIC_YEAR_ID = objAy.AcademicYearID;
                                            objClassDetail.SCHOOL_ID = schoolID;
                                            objClassDetail.YEAR = objAy.Year;
                                            objClassDetail.SEMESTER = (byte)semester;
                                            objClassDetail.TOTAL_SMS = TotalSMSInClass;
                                            objClassDetail.SENT_TOTAL = TotalSMSSent;
                                            objClassDetail.CREATED_DATE = DateTime.Now;
                                            lstClassDetails.Add(objClassDetail);
                                            if (TotalSMSInClass > 0) SMSParentContractInClassDetailBusiness.Insert(objClassDetail);
                                        }
                                        // so tin can gui trong lop
                                        int numSMSNeedSend = countSMS * lstPupilContract.Count(o => lstPupilIDInClass.Contains(o.PupilFileID) && o.IsLimitPackage != true && o.Status == GlobalConstantsEdu.STATUS_REGISTED);

                                        if ((lstClassDetailstmp.Sum(o => o.TOTAL_SMS) - lstClassDetailstmp.Sum(o => o.SENT_TOTAL)) < numSMSNeedSend)
                                        {
                                            // Het quy tin - Loai cac HS da thanh toan
                                            lstPupilFileID = lstPupilFileID.Except(lstPupilIDInClass).ToList();
                                            lstPupilFileID.Union(lstPupilContract.Where(o => lstPupilIDInClass.Contains(o.PupilFileID) && o.Status == GlobalConstantsEdu.STATUS_UNPAID).Select(o => o.PupilFileID).ToList());
                                            classNotSend = new ClassBO();
                                            classNotSend.ClassID = vClassID;
                                            classNotSend.ClassName = lstPupilOfClass.FirstOrDefault(o => o.ClassID == vClassID).ClassProfile.DisplayName;
                                            lstClassNotSent.Add(classNotSend);
                                        }
                                        else
                                        {
                                            if (numSMSNeedSend > 0)
                                            {
                                                lstClassSent.Add(vClassID);
                                            }
                                        }
                                    }

                                    SMS_TIMER_CONFIG tc = new SMS_TIMER_CONFIG();
                                    if (isTimerUsed)
                                    {
                                        tc = new SMS_TIMER_CONFIG();
                                        tc.SMS_TIMER_CONFIG_ID = Guid.NewGuid();
                                        tc.CONTENT = content;
                                        tc.CREATE_TIME = DateTime.Now;
                                        tc.SCHOOL_ID = schoolID;
                                        tc.APPLIED_LEVEL = iLevelID;
                                        tc.PARTITION_ID = PartitionID;
                                        tc.SEND_TIME = sendTime.Value;
                                        tc.STATUS = GlobalConstantsEdu.TIMER_CONFIG_STATUS_ACTIVE;//chua gui
                                        tc.TARGET_ID = iEducationLevelID != 0 ? iEducationLevelID : 0;
                                        tc.CLASS_SEND_ID = string.Join(",", lstClassSent);
                                        tc.CONFIG_NAME = timerConfigName;
                                        tc.CONFIG_TYPE = iEducationLevelID != 0 ? GlobalConstantsEdu.TIMER_CONFIG_TYPE_EDU : GlobalConstantsEdu.TIMER_CONFIG_TYPE_SCHOOL;
                                        tc.SEND_ALL_CLASS = isAllClass;
                                        tc.SEND_UNICODE = isSignMsg;
                                        tc.IS_IMPORT = isSendImport;

                                        TimerConfigBusiness.Insert(tc);
                                        TimerConfigBusiness.Save();
                                    }

                                    PupilProfileBO pupilProfileBO = null;

                                    int intPupilFileID = 0;
                                    PupilContract objPupilContractUspResult = null;
                                    List<PupilSubscriber> lstPupilSubscriber = null;
                                    PupilSubscriber objPupilSubscriber = null;
                                    long smsParentContractDetailID = 0;
                                    int countNumOfSend = 0;
                                    List<SMS_MT> lstMTInsertBO = new List<SMS_MT>();
                                    //List<SMSCommunicationInsertBO> lstSMSCommunicationInsertBO = new List<SMSCommunicationInsertBO>();
                                    List<SMS_HISTORY> lstHistorySMSInsertBO = new List<SMS_HISTORY>();
                                    SMS_MT objMTInsertBO = null;
                                    //SMSCommunicationInsertBO objSMSCommunicationInsertBO = null;
                                    SMS_HISTORY objHistorySMSInsertBO = null;
                                    SMS_SERVICE_PACKAGE_DECLARE detailLimitSP = null;
                                    //bool isHasManyMobileReceiver = false;
                                    bool isSendOnWebOK = false;
                                    //danh dau record history de lay HistoryID
                                    int iHistoryIndex = 0;
                                    string fullNameSender = school.SchoolName;

                                    #region For pupil
                                    for (int i = lstPupilFileID.Count - 1; i >= 0; i--)
                                    {
                                        intPupilFileID = lstPupilFileID[i];
                                        //isHasManyMobileReceiver = false;
                                        isSendOnWebOK = false;
                                        lstPupilSubscriber = (from pc in lstPupilContract
                                                              where pc.PupilFileID == intPupilFileID
                                                              select new PupilSubscriber
                                                              {
                                                                  Subscriber = pc.Mobile,
                                                                  SMSParentContractDetailID = pc.SMSParentContactDetailID
                                                              }).ToList();

                                        if (isSendImport)
                                        {
                                            SendSMSByImportBO sImport = lstImportSMS.FirstOrDefault(o => o.PupilFileID == intPupilFileID);
                                            content = sImport.Content;
                                            contentStripVNSign = ExtensionMethods.ConvertLineToSpace(content.StripVNSign().StripVNSignComposed());
                                            shortContent = sImport.ShortContent;
                                            countSMS = content.countNumSMS(isSignMsg);
                                        }


                                        for (int k = lstPupilSubscriber.Count - 1; k >= 0; k--)
                                        {
                                            objPupilSubscriber = lstPupilSubscriber[k];
                                            // Chỉ cho phép gửi tin có dấu với các đầu số cho trước
                                            if (isSignMsg && !objPupilSubscriber.Subscriber.isAllowSendSignSMSPhone()) continue;
                                            //Lay brandName theo thue bao
                                            brandName = this.GetBrandNamebyMobile(objPupilSubscriber.Subscriber, lstPreNumberTelco, lstBrandName);
                                            objPupilContractUspResult = (from pc in lstPupilContract where pc.Mobile == objPupilSubscriber.Subscriber && pc.PupilFileID == intPupilFileID && pc.SMSParentContactDetailID == objPupilSubscriber.SMSParentContractDetailID select pc).FirstOrDefault();
                                            pupilProfileBO = pupilProfileList.FirstOrDefault(p => p.PupilProfileID == intPupilFileID);
                                            if (pupilProfileBO == null) continue;

                                            //Not found from Contract Detail
                                            if (objPupilContractUspResult == null) continue;

                                            smsParentContractDetailID = objPupilContractUspResult.SMSParentContactDetailID;

                                            if (!objPupilSubscriber.Subscriber.CheckMobileNumber()) continue;

                                            #region Kiem tra gui tin voi goi dich vu GIOI HAN
                                            // Neu la goi trial
                                            if (objPupilContractUspResult.IsLimitPackage.HasValue && objPupilContractUspResult.IsLimitPackage.Value)
                                            {

                                                //// Kiem tra thoi gian cho phep gui trong thoi han, ke tu ngay dau noi
                                                detailLimitSP = LstLimitPackage.FirstOrDefault(o => o.SERVICE_PACKAGE_ID == objPupilContractUspResult.ServicePackageID
                                                    && o.EFFECTIVE_DAYS > 0 &&
                                                    ((o.FROM_DATE.HasValue && o.FROM_DATE.Value.Date <= objPupilContractUspResult.RegisterDate.Date &&
                                                    o.TO_DATE.HasValue && o.TO_DATE.Value.Date >= objPupilContractUspResult.RegisterDate.Date)
                                                    || (!o.TO_DATE.HasValue && !o.FROM_DATE.HasValue)));
                                                if (detailLimitSP != null)
                                                {
                                                    if ((DateTime.Now - objPupilContractUspResult.RegisterDate.Date).TotalDays > detailLimitSP.EFFECTIVE_DAYS) continue;
                                                }

                                                //Kiem tra so luong tin nhan chi (cho phep gui khi con tin nhan)
                                                if (objPupilContractUspResult.TotalSMS - objPupilContractUspResult.SentSMS <= 0) continue;
                                            }
                                            #endregion


                                            // Cac goi No cuoc kiem tra con so luong tin
                                            if (objPupilContractUspResult.Status == GlobalConstantsEdu.STATUS_UNPAID && objPupilContractUspResult.TotalSMS - objPupilContractUspResult.SentSMS <= 0) continue;
                                            //Lay brandName theo thue bao
                                            brandName = GetBrandNamebyMobile(objPupilSubscriber.Subscriber, lstPreNumberTelco, lstBrandName);

                                            //insert historySMS
                                            objHistorySMSInsertBO = new SMS_HISTORY();
                                            Guid SMSHistoryRawID = Guid.NewGuid();
                                            objHistorySMSInsertBO.HISTORY_RAW_ID = SMSHistoryRawID;
                                            objHistorySMSInsertBO.MOBILE = objPupilSubscriber.Subscriber;
                                            objHistorySMSInsertBO.IS_INTERNAL = objHistorySMSInsertBO.MOBILE.CheckMobileNumberVT();
                                            objHistorySMSInsertBO.RECEIVER_NAME = pupilProfileBO != null ? pupilProfileBO.FullName : string.Empty;
                                            if (typeHistory.HasValue && typeHistory.Value != 0) objHistorySMSInsertBO.SMS_TYPE_ID = typeHistory.Value;
                                            else objHistorySMSInsertBO.SMS_TYPE_ID = GlobalConstantsEdu.COMMON_HISTORYSMS_TO_COMMUNICATION_ID;

                                            objHistorySMSInsertBO.IS_CUSTOM_TYPE = isCustomType;
                                            objHistorySMSInsertBO.RECEIVE_TYPE = GlobalConstantsEdu.COMMON_HISTORY_RECEIVER_PARENT_ID;
                                            objHistorySMSInsertBO.CREATE_DATE = dateTimeNow;
                                            objHistorySMSInsertBO.SCHOOL_ID = schoolID;
                                            objHistorySMSInsertBO.YEAR = objAy.Year;
                                            objHistorySMSInsertBO.ACADEMIC_YEAR_ID = objAy.AcademicYearID;
                                            objHistorySMSInsertBO.SMS_CONTENT = content;
                                            objHistorySMSInsertBO.RECEIVER_ID = objPupilContractUspResult.PupilFileID;
                                            objHistorySMSInsertBO.SHORT_CONTENT = shortContent;
                                            objHistorySMSInsertBO.SENDER_NAME = fullNameSender;
                                            objHistorySMSInsertBO.SMS_CNT = countSMS;
                                            objHistorySMSInsertBO.SCHOOL_USERNAME = schoolUserName;
                                            objHistorySMSInsertBO.SMS_PARENTCONTRACT_DETAIL_ID = smsParentContractDetailID;
                                            objHistorySMSInsertBO.CONTENT_TYPE = isSignMsg ? GlobalConstantsEdu.SMS_CONTENT_TYPE_UNICODE : GlobalConstantsEdu.SMS_CONTENT_TYPE_NOT_SIGN;
                                            objHistorySMSInsertBO.IS_SMS = true;
                                            objHistorySMSInsertBO.SENDER_GROUP = senderGroup;
                                            objHistorySMSInsertBO.PARTITION_ID = PartitionID;
                                            objHistorySMSInsertBO.IS_ADMIN = isAdmin;
                                            objHistorySMSInsertBO.SENDER_ID = senderID;
                                            if (isTimerUsed)
                                            {
                                                objHistorySMSInsertBO.SMS_TIMER_CONFIG_ID = tc.SMS_TIMER_CONFIG_ID;
                                                objHistorySMSInsertBO.IS_ON_SCHEDULE = true;
                                                objHistorySMSInsertBO.SEND_TIME = tc.SEND_TIME;
                                                hasTimerMessage = true;
                                            }

                                            lstHistorySMSInsertBO.Add(objHistorySMSInsertBO);

                                            //insert MT
                                            objMTInsertBO = new SMS_MT();
                                            objMTInsertBO.REQUEST_ID = Guid.NewGuid();
                                            objMTInsertBO.HISTORY_RAW_ID = SMSHistoryRawID;
                                            objMTInsertBO.USER_ID = objPupilSubscriber.Subscriber;
                                            if (isSignMsg)
                                            {
                                                objMTInsertBO.CONTENT_TYPE = "1"; // có dấu
                                                objMTInsertBO.SMS_CONTENT = content;
                                            }
                                            else
                                            {
                                                objMTInsertBO.CONTENT_TYPE = "0"; // ko dấu
                                                objMTInsertBO.SMS_CONTENT = contentStripVNSign;
                                            }
                                            objMTInsertBO.STATUS = GlobalConstantsEdu.MT_STATUS_NOT_SEND;
                                            objMTInsertBO.TIME_SEND_REQUEST = dateTimeNow;
                                            objMTInsertBO.UNIT_ID = schoolID;
                                            objMTInsertBO.SERVICE_ID = brandName;
                                            if (isTimerUsed)
                                            {
                                                objMTInsertBO.SMS_TIMER_CONFIG_ID = tc.SMS_TIMER_CONFIG_ID;
                                                hasTimerMessage = true;
                                            }
                                            lstMTInsertBO.Add(objMTInsertBO);

                                            //Chi tru khi gui qua MT
                                            objPupilContractUspResult.IsSent = true;
                                            objPupilContractUspResult.SentSMS += countSMS;
                                            isSendOnWebOK = true;

                                        }
                                        if (isSendOnWebOK)
                                        {
                                            countNumOfSend++;
                                        }
                                    }
                                    #endregion end for

                                    if (countNumOfSend == 0)
                                    {
                                        resultBO.isError = false;
                                        resultBO.NumSendSuccess = 0;
                                        return resultBO;
                                    }

                                    #region Send to head teacher - AnhVD9 20150922
                                    if (isSameContent && schoolCfg != null && schoolCfg.IS_ALLOW_SEND_HEADTEACHER)
                                    {
                                        List<TeacherBO> lstHeadTeachers = EmployeeBusiness.GetHeadTeacherByClassIDs(lstClassSent, objAy.AcademicYearID, schoolID);
                                        TeacherBO objEmployeeReceiver = null;
                                        for (int i = lstHeadTeachers.Count - 1; i >= 0; i--)
                                        {
                                            Guid SMSHistoryRawID = Guid.NewGuid();
                                            objEmployeeReceiver = lstHeadTeachers[i];
                                            // Chỉ cho phép gửi tin có dấu với các đầu số cho trước
                                            if (isSignMsg && !objEmployeeReceiver.Mobile.isAllowSendSignSMSPhone()) continue;
                                            if (!objEmployeeReceiver.Mobile.CheckMobileNumber()) continue;
                                            //Lay brandName theo thue bao
                                            brandName = GetBrandNamebyMobile(objEmployeeReceiver.Mobile, lstPreNumberTelco, lstBrandName);
                                            //insert historySMS 
                                            objHistorySMSInsertBO = new SMS_HISTORY();
                                            objHistorySMSInsertBO.HISTORY_RAW_ID = SMSHistoryRawID;
                                            objHistorySMSInsertBO.MOBILE = objEmployeeReceiver.Mobile;
                                            objHistorySMSInsertBO.IS_INTERNAL = objHistorySMSInsertBO.MOBILE.CheckMobileNumberVT();
                                            objHistorySMSInsertBO.RECEIVER_NAME = objEmployeeReceiver.FullName;
                                            // Loai tin nhan trao doi gui tu PHHS
                                            if (typeHistory.HasValue && typeHistory.Value != 0) objHistorySMSInsertBO.SMS_TYPE_ID = typeHistory.Value;
                                            else objHistorySMSInsertBO.SMS_TYPE_ID = GlobalConstantsEdu.COMMON_HISTORYSMS_TO_COMMUNICATION_ID;

                                            objHistorySMSInsertBO.IS_CUSTOM_TYPE = isCustomType;
                                            objHistorySMSInsertBO.RECEIVE_TYPE = GlobalConstantsEdu.COMMON_HISTORY_RECEIVER_TEACHER_ID;
                                            objHistorySMSInsertBO.CREATE_DATE = dateTimeNow;
                                            objHistorySMSInsertBO.SCHOOL_ID = schoolID;
                                            objHistorySMSInsertBO.YEAR = objAy.Year;
                                            objHistorySMSInsertBO.ACADEMIC_YEAR_ID = objAy.AcademicYearID;
                                            objHistorySMSInsertBO.SMS_CONTENT = content;
                                            objHistorySMSInsertBO.SENDER_NAME = fullNameSender;
                                            objHistorySMSInsertBO.SHORT_CONTENT = shortContent;
                                            objHistorySMSInsertBO.RECEIVER_ID = objEmployeeReceiver.TeacherID;
                                            objHistorySMSInsertBO.SMS_CNT = countSMS;
                                            objHistorySMSInsertBO.SCHOOL_USERNAME = schoolUserName;
                                            objHistorySMSInsertBO.SENDER_GROUP = senderGroup;
                                            objHistorySMSInsertBO.CONTENT_TYPE = isSignMsg ? GlobalConstantsEdu.SMS_CONTENT_TYPE_UNICODE : GlobalConstantsEdu.SMS_CONTENT_TYPE_NOT_SIGN;
                                            objHistorySMSInsertBO.IS_SMS = true;
                                            objHistorySMSInsertBO.PARTITION_ID = PartitionID;
                                            objHistorySMSInsertBO.IS_ADMIN = isAdmin;
                                            objHistorySMSInsertBO.SENDER_ID = senderID;
                                            if (isTimerUsed)
                                            {
                                                objHistorySMSInsertBO.SMS_TIMER_CONFIG_ID = tc.SMS_TIMER_CONFIG_ID;
                                                objHistorySMSInsertBO.IS_ON_SCHEDULE = true;
                                                objHistorySMSInsertBO.SEND_TIME = tc.SEND_TIME;
                                                hasTimerMessage = true;
                                            }
                                            lstHistorySMSInsertBO.Add(objHistorySMSInsertBO);

                                            //insert MT
                                            objMTInsertBO = new SMS_MT();
                                            if (isSignMsg)
                                            {
                                                objMTInsertBO.CONTENT_TYPE = "1"; // có dấu
                                                objMTInsertBO.SMS_CONTENT = content;
                                            }
                                            else
                                            {
                                                objMTInsertBO.CONTENT_TYPE = "0"; // ko dấu
                                                objMTInsertBO.SMS_CONTENT = contentStripVNSign;
                                            }
                                            objMTInsertBO.REQUEST_ID = Guid.NewGuid();
                                            objMTInsertBO.HISTORY_RAW_ID = SMSHistoryRawID;
                                            objMTInsertBO.STATUS = GlobalConstantsEdu.MT_STATUS_NOT_SEND;
                                            objMTInsertBO.TIME_SEND_REQUEST = dateTimeNow;
                                            objMTInsertBO.USER_ID = objEmployeeReceiver.Mobile;
                                            objMTInsertBO.UNIT_ID = schoolID;
                                            objMTInsertBO.SERVICE_ID = brandName;
                                            if (isTimerUsed)
                                            {
                                                objMTInsertBO.SMS_TIMER_CONFIG_ID = tc.SMS_TIMER_CONFIG_ID;
                                                hasTimerMessage = true;
                                            }
                                            lstMTInsertBO.Add(objMTInsertBO);

                                            iHistoryIndex++;

                                        }
                                    }
                                    #endregion

                                    #region bulk insert
                                    if (lstHistorySMSInsertBO.Count > 0)
                                    {
                                        SMSHistoryBusiness.BulkInsert(lstHistorySMSInsertBO, this.ColumnHistoryMappings(), "HISTORY_ID");

                                        int insert = MTBusiness.BulkInsert(lstMTInsertBO, this.ColumnMTMappings());
                                        if (insert == 0)
                                        {
                                            List<SMS_HISTORY> lstDelete = new List<SMS_HISTORY>();
                                            List<Guid> lstSMSHistoryRawID = lstHistorySMSInsertBO.Select(p=>p.HISTORY_RAW_ID).Distinct().ToList();
                                            lstDelete = SMSHistoryBusiness.All.Where(p => lstSMSHistoryRawID.Contains(p.HISTORY_RAW_ID) && p.PARTITION_ID == PartitionID).ToList();
                                            SMSHistoryBusiness.DeleteAll(lstDelete);
                                            SMSHistoryBusiness.Save();
                                            resultBO.isError = true;
                                            resultBO.ErrorMsg = ResEdu.Get("SendSMS_Label_CoreError");
                                            return resultBO;
                                        }
                                        resultBO.NumSendSuccess = insert;
                                    }
                                    else
                                    {
                                        resultBO.isError = true;
                                        resultBO.ErrorMsg = ResEdu.Get("SendSMS_Label_CoreError");
                                        return resultBO;
                                    }
                                    #endregion

                                    #region cap nhat tin nhan theo tung thue bao
                                    List<PupilContract> lstSent = lstPupilContract.Where(a => a.IsSent.HasValue && a.IsSent.Value == true).ToList();
                                    PupilContract itmSent = null;
                                    SMS_PARENT_SENT_DETAIL sentItem = null;
                                    List<long> LstContractDetailID = lstSent.Select(o => o.SMSParentContactDetailID).Distinct().ToList();
                                    List<SMS_PARENT_SENT_DETAIL> lstPupilSentDetail = qSMSParentSendDetail.Where(c => LstContractDetailID.Contains(c.SMS_PARENT_CONTRACT_DETAIL_ID)).ToList();

                                    List<SMS_PARENT_SENT_DETAIL> lstSmsParentSentDetailInsert = new List<SMS_PARENT_SENT_DETAIL>();
                                    for (int i = 0, SentCnt = lstSent.Count; i < SentCnt; i++)
                                    {
                                        itmSent = lstSent[i];
                                        if (itmSent.SMSParentSentDetailID > 0)
                                        {
                                            sentItem = lstPupilSentDetail.FirstOrDefault(a => a.SMS_PARENT_SENT_DETAIL_ID == itmSent.SMSParentSentDetailID);
                                            if (sentItem != null)
                                            {
                                                if (isTimerUsed)
                                                {
                                                    //Luu lai quy tin cu
                                                    SMS_TIMER_TRANSACTION tt = new SMS_TIMER_TRANSACTION();
                                                    tt.SCHOOL_ID = schoolID;
                                                    tt.TRANSACTION_TYPE = GlobalConstantsEdu.TIMER_TRANSACTION_SENT_DETAIL;
                                                    tt.IS_ACTIVE = true;
                                                    tt.SMS_PARENT_SENT_DETAIL_ID = sentItem.SMS_PARENT_SENT_DETAIL_ID;
                                                    tt.SENT_TOTAL = itmSent.SentSMS - sentItem.SENT_TOTAL;
                                                    tt.TIMER_CONFIG_ID = tc.SMS_TIMER_CONFIG_ID;
                                                    TimerTransactionBusiness.Insert(tt);
                                                }
                                                sentItem.UPDATED_DATE = dateTimeNow;
                                                sentItem.SENT_TOTAL = itmSent.SentSMS;
                                                SMSParentSentDetailBusiness.Update(sentItem);
                                            }
                                        }
                                        else
                                        {
                                            sentItem = new SMS_PARENT_SENT_DETAIL();
                                            sentItem.SEMESTER = semester;
                                            sentItem.ACADEMIC_YEAR_ID = academicYearID;
                                            sentItem.YEAR = objAy.Year;
                                            sentItem.CREATED_DATE = dateTimeNow;
                                            sentItem.SMS_PARENT_CONTRACT_DETAIL_ID = itmSent.SMSParentContactDetailID;
                                            sentItem.SENT_TOTAL = itmSent.SentSMS;
                                            sentItem.SCHOOL_ID = schoolID;
                                            sentItem.PARTITION_ID = PartitionID;
                                            SMSParentSentDetailBusiness.Insert(sentItem);
                                            lstSmsParentSentDetailInsert.Add(sentItem);
                                        }
                                    }
                                    //this.Save();
                                    #endregion

                                    #region Cap nhat quy tin cua tung lop - HoanTV5 20170506
                                    int? CountSMStmp = 0;
                                    bool breakout = false;
                                    for (int i = lstClassSent.Count - 1; i >= 0; i--)
                                    {
                                        vClassID = lstClassSent[i];

                                        lstPupilIDInClass = lstPupilOfClass.Where(o => o.ClassID == vClassID).Select(o => o.PupilID).ToList();
                                        CountSMStmp = 0;
                                        lstClassDetailstmp = lstClassDetails.Where(c => c.ACADEMIC_YEAR_ID == objAy.AcademicYearID
                                                    && c.SCHOOL_ID == schoolID && c.YEAR == objAy.Year && c.CLASS_ID == vClassID).OrderBy(c => c.SEMESTER).ToList();
                                        for (int j = 0; j < lstClassDetailstmp.Count; j++)
                                        {
                                            objClassDetail = lstClassDetailstmp[j];
                                            SMS_TIMER_TRANSACTION tt = new SMS_TIMER_TRANSACTION();
                                            // cap nhat quy tin khong tinh goi co gioi han va cac thue bao No cuoc
                                            int numSMSNeedSend = countSMS * lstPupilContract.Count(o => lstPupilIDInClass.Contains(o.PupilFileID) && o.IsLimitPackage != true
                                                && o.Status != GlobalConstantsEdu.STATUS_UNPAID) - CountSMStmp.Value;
                                            //tru quy tin lan luot, het quy HK thi tru sang quy tin CN
                                            CountSMStmp = objClassDetail.TOTAL_SMS - objClassDetail.SENT_TOTAL;
                                            if (CountSMStmp <= numSMSNeedSend)
                                            {
                                                objClassDetail.SENT_TOTAL = objClassDetail.TOTAL_SMS;
                                                tt.SMS_NUMBER_ADDED = CountSMStmp.Value;
                                            }
                                            else
                                            {
                                                objClassDetail.SENT_TOTAL += numSMSNeedSend;
                                                tt.SMS_NUMBER_ADDED = numSMSNeedSend;
                                                breakout = true;
                                            }
                                            //objClassDetail.SentTotal += numSMSNeedSend;
                                            objClassDetail.UPDATED_DATE = DateTime.Now;
                                            if (objClassDetail.SMS_PARENT_CONTRACT_CLASS_ID > 0)
                                            {
                                                SMSParentContractInClassDetailBusiness.Update(objClassDetail);
                                            }

                                            //Luu lai quy tin cu
                                            tt.SCHOOL_ID = schoolID;
                                            tt.TRANSACTION_TYPE = GlobalConstantsEdu.TIMER_TRANSACTION_CONTRACT_IN_CLASS;
                                            tt.IS_ACTIVE = true;
                                            tt.SMS_PARENT_CONTRACT_CLASS_ID = objClassDetail.SMS_PARENT_CONTRACT_CLASS_ID;
                                            tt.TIMER_CONFIG_ID = tc.SMS_TIMER_CONFIG_ID;
                                            TimerTransactionBusiness.Insert(tt);
                                            if (breakout)
                                            {
                                                break;
                                            }
                                        }
                                    }
                                    #endregion

                                    if (isTimerUsed && lstSmsParentSentDetailInsert.Count > 0)
                                    {
                                        for (int i = 0; i < lstSmsParentSentDetailInsert.Count; i++)
                                        {
                                            SMS_PARENT_SENT_DETAIL ssd = lstSmsParentSentDetailInsert[i];
                                            //Luu lai quy tin cu
                                            SMS_TIMER_TRANSACTION tt = new SMS_TIMER_TRANSACTION();
                                            tt.SCHOOL_ID = schoolID;
                                            tt.TRANSACTION_TYPE = GlobalConstantsEdu.TIMER_TRANSACTION_SENT_DETAIL;
                                            tt.IS_ACTIVE = true;
                                            tt.SMS_PARENT_SENT_DETAIL_ID = ssd.SMS_PARENT_SENT_DETAIL_ID;
                                            tt.SENT_TOTAL = ssd.SENT_TOTAL;
                                            tt.TIMER_CONFIG_ID = tc.SMS_TIMER_CONFIG_ID;

                                            TimerTransactionBusiness.Insert(tt);

                                        }
                                    }
                                    this.Save();
                                    // Cap nhat thong tin 
                                    resultBO.NumSendSuccess = countNumOfSend;
                                    // danh sach cac lop khong gui
                                    resultBO.LstClassNotSent = lstClassNotSent;
                                    return resultBO;
                                    #endregion
                                }
                                else
                                {
                                    #region gui toan khoi voi cac ban tin khac trao doi
                                    List<int> lstEducationLevelID = new List<int>(); ;
                                    if (sendAllSchool && iEducationLevelID == 0)
                                    {
                                        lstEducationLevelID = EducationLevelBusiness.GetByGrade(appliedLevelID).Select(p=>p.EducationLevelID).ToList();
                                    }
                                    List<PupilOfClass> lstPupilOfClass = new List<PupilOfClass>();
                                    IDictionary<string, object> dicClass = new Dictionary<string, object>();
                                    if (lstEducationLevelID.Count == 0)
                                    {
                                        dicClass.Add("SchoolID", schoolID);
                                        dicClass.Add("AcademicYearID", academicYearID);
                                        dicClass.Add("EducationLevelID", iEducationLevelID);
                                        lstPupilOfClass = PupilOfClassBusiness.Search(dicClass).Where(p => lstClassID.Contains(p.ClassID) && (p.Status == GlobalConstants.PUPIL_STATUS_STUDYING || p.Status == GlobalConstants.PUPIL_STATUS_GRADUATED)).Distinct().ToList();
                                        if (lstPupilOfClass == null || lstPupilOfClass.Count == 0)
                                        {
                                            resultBO.isError = false;
                                            resultBO.NumSendSuccess = 0;
                                            return resultBO;
                                        }
                                    }
                                    else
                                    {
                                        dicClass.Add("SchoolID", schoolID);
                                        dicClass.Add("AcademicYearID", academicYearID);
                                        lstPupilOfClass = PupilOfClassBusiness.Search(dicClass).Where(p => lstEducationLevelID.Contains(p.ClassProfile.EducationLevelID) 
                                            && lstClassID.Contains(p.ClassID)
                                            && (p.Status == GlobalConstants.PUPIL_STATUS_STUDYING || p.Status == GlobalConstants.PUPIL_STATUS_GRADUATED)).Distinct().ToList();
                                    }

                                    if (lstPupilOfClass == null || lstPupilOfClass.Count == 0)
                                    {
                                        resultBO.isError = false;
                                        resultBO.NumSendSuccess = 0;
                                        return resultBO;
                                    }

                                    List<int> lstPupilIDInLevel = lstPupilOfClass.Select(o => o.PupilID).Distinct().ToList();
                                    if (lstReceiverID != null && ((lstReceiverID.Count != contentList.Count) || (lstReceiverID.Except(lstPupilIDInLevel).Count() > 0)))
                                    {
                                        resultBO.isError = true;
                                        resultBO.ErrorMsg = ResEdu.Get("SendSMS_Label_CoreError");
                                        return resultBO;
                                    }

                                    List<PupilContent> lstPupilContent = new List<PupilContent>();
                                    PupilContent objPupilContent = null;
                                    for (int i = 0, size = lstReceiverID.Count; i < size; i++)
                                    {
                                        objPupilContent = new PupilContent();
                                        objPupilContent.PupilFileID = lstReceiverID[i];
                                        objPupilContent.Content = contentList[i].Trim();
                                        objPupilContent.SMS_CNT = objPupilContent.Content.countNumSMS(isSignMsg);
                                        objPupilContent.ShortContent = lstShortContent[i].Trim();
                                        lstPupilContent.Add(objPupilContent);
                                    }

                                    #region check truong hop 2000 parameter
                                    List<PupilContract> lstPupilContract = new List<PupilContract>();
                                    List<PupilProfileBO> pupilProfileList = new List<PupilProfileBO>();
                                    pupilProfileList = PupilProfileBusiness.GetListPupilSendSMS(lstReceiverID, academicYearID);
                                    lstPupilContract = this.GetListPupilContract(schoolID, objAy.Year, semester, lstReceiverID);
                                    #endregion

                                    //Lay danh sach lop va duyet theo tung lop - AnhVD9 20150106
                                    //List<int> lstClassID = lstPupilOfClass.Where(o => lstReceiverID.Contains(o.PupilID)).Select(o => o.ClassID).Distinct().ToList();
                                    SMS_PARENT_CONTRACT_CLASS objClassDetail = null;
                                    IDictionary<string, object> dicSearchClassDetail = new Dictionary<string, object>()
                                    {
                                        {"AcademicYearID",academicYearID},
                                        {"SchoolID",schoolID},
                                        {"Year",objAy.Year}
                                    };
                                    List<SMS_PARENT_CONTRACT_CLASS> lstClassDetails = SMSParentContractInClassDetailBusiness.Search(dicSearchClassDetail).Where(c => 
                                                    lstClassID.Contains(c.CLASS_ID) && (c.SEMESTER == semester || c.SEMESTER == GlobalConstantsEdu.SEMESTER_OF_YEAR_ALL)).ToList();
                                    List<SMS_PARENT_CONTRACT_CLASS> lstClassDetailstmp = null;

                                    List<int> lstPupilIDInClass = new List<int>();
                                    List<int> lstClassSent = new List<int>();
                                    List<ClassBO> lstClassNotSent = new List<ClassBO>();
                                    ClassBO classNotSend = null;
                                    int vClassID = 0;
                                    for (int i = lstClassID.Count - 1; i >= 0; i--)
                                    {
                                        vClassID = lstClassID[i];
                                        lstPupilIDInClass = lstPupilOfClass.Where(o => o.ClassID == vClassID).Select(o => o.PupilID).ToList();
                                        lstClassDetailstmp = lstClassDetails.Where(o => o.CLASS_ID == vClassID).ToList();
                                        //objClassDetail = lstClassDetails.FirstOrDefault(o => o.ClassID == vClassID);

                                        if (lstClassDetailstmp.Count == 0)
                                        {
                                            int TotalSMSInClass = lstPupilContract.Where(o => lstPupilIDInClass.Contains(o.PupilFileID) && o.IsLimitPackage != true
                                                && o.Status == GlobalConstantsEdu.STATUS_REGISTED).Sum(o => o.TotalSMS);
                                            int TotalSMSSent = lstPupilContract.Where(o => lstPupilIDInClass.Contains(o.PupilFileID) && o.IsLimitPackage != true
                                                && o.Status == GlobalConstantsEdu.STATUS_REGISTED).Sum(o => o.SentSMS);
                                            objClassDetail = new SMS_PARENT_CONTRACT_CLASS();
                                            objClassDetail.CLASS_ID = vClassID;
                                            objClassDetail.ACADEMIC_YEAR_ID = objAy.AcademicYearID;
                                            objClassDetail.SCHOOL_ID = schoolID;
                                            objClassDetail.YEAR = objAy.Year;
                                            objClassDetail.SEMESTER = (byte)semester;
                                            objClassDetail.TOTAL_SMS = TotalSMSInClass;
                                            objClassDetail.SENT_TOTAL = TotalSMSSent;
                                            objClassDetail.CREATED_DATE = DateTime.Now;
                                            lstClassDetails.Add(objClassDetail);
                                            if (TotalSMSInClass > 0) SMSParentContractInClassDetailBusiness.Insert(objClassDetail);
                                        }

                                        lstPupilIDInClass = lstPupilContract.Where(o => lstPupilIDInClass.Contains(o.PupilFileID) && o.IsLimitPackage != true).Select(o => o.PupilFileID).ToList();
                                        // so tin can gui trong lop
                                        int numSMSNeedSend = lstPupilContent.Where(o => lstPupilIDInClass.Contains(o.PupilFileID)).Sum(o => o.SMS_CNT * lstPupilContract.Count(p => p.PupilFileID == o.PupilFileID && p.IsLimitPackage != true && p.Status == GlobalConstantsEdu.STATUS_REGISTED));
                                        // Neu khong du quy tin va khong co thue bao No cuoc thi ko gui
                                        if ((lstClassDetailstmp.Sum(o => o.TOTAL_SMS) - lstClassDetailstmp.Sum(o => o.SENT_TOTAL)) < numSMSNeedSend)
                                        {
                                            lstReceiverID = lstReceiverID.Except(lstPupilIDInClass).ToList();
                                            lstReceiverID.Union(lstPupilContract.Where(o => lstPupilIDInClass.Contains(o.PupilFileID) && o.Status == GlobalConstantsEdu.STATUS_UNPAID).Select(o => o.PupilFileID).ToList());
                                            classNotSend = new ClassBO();
                                            classNotSend.ClassID = vClassID;
                                            classNotSend.ClassName = lstPupilOfClass.FirstOrDefault(o => o.ClassID == vClassID).ClassProfile.DisplayName;
                                            lstClassNotSent.Add(classNotSend);
                                        }
                                        else lstClassSent.Add(vClassID);
                                    }
                                    // End AnhVD9
                                    PupilProfileBO pupilProfileBO = null;

                                    int intPupilFileID = 0;
                                    PupilContract objPupilContractUspResult = null;
                                    List<PupilSubscriber> lstPupilSubscriber = null;
                                    PupilSubscriber objPupilSubscriber = null;
                                    long smsParentContractDetailID = 0;
                                    int countNumOfSend = 0;
                                    List<SMS_MT> lstMTInsertBO = new List<SMS_MT>();
                                    List<SMS_HISTORY> lstHistorySMSInsertBO = new List<SMS_HISTORY>();
                                    SMS_MT objMTInsertBO = null;
                                    SMS_HISTORY objHistorySMSInsertBO = null;
                                    SMS_SERVICE_PACKAGE_DECLARE detailLimitSP = null;
                                    bool isHasManyMobileReceiver = false;
                                    bool isSendOnWebOK = false;
                                    //danh dau record history de lay HistoryID
                                    int iHistoryIndex = 0;
                                    string fullNameSender = school.SchoolName;

                                    for (int i = lstReceiverID.Count - 1; i >= 0; i--)
                                    {
                                        intPupilFileID = lstReceiverID[i];
                                        isHasManyMobileReceiver = false;
                                        objPupilContent = lstPupilContent.FirstOrDefault(p => p.PupilFileID == intPupilFileID);

                                        // set content
                                        content = objPupilContent.Content;
                                        shortContent = objPupilContent.ShortContent;
                                        contentStripVNSign = ExtensionMethods.ConvertLineToSpace(content.StripVNSign().StripVNSignComposed());
                                        // end set content
                                        countSMS = objPupilContent.SMS_CNT;
                                        isSendOnWebOK = false;
                                        lstPupilSubscriber = (from pc in lstPupilContract
                                                              where pc.PupilFileID == intPupilFileID
                                                              select new PupilSubscriber
                                                              {
                                                                  Subscriber = pc.Mobile,
                                                                  SMSParentContractDetailID = pc.SMSParentContactDetailID
                                                              }).ToList();

                                        for (int k = lstPupilSubscriber.Count - 1; k >= 0; k--)
                                        {
                                            objPupilSubscriber = lstPupilSubscriber[k];
                                            // Chỉ cho phép gửi tin có dấu với các đầu số cho trước
                                            if (isSignMsg && !objPupilSubscriber.Subscriber.isAllowSendSignSMSPhone()) continue;
                                            objPupilContractUspResult = (from pc in lstPupilContract where pc.Mobile == objPupilSubscriber.Subscriber && pc.PupilFileID == intPupilFileID && pc.SMSParentContactDetailID == objPupilSubscriber.SMSParentContractDetailID select pc).FirstOrDefault();
                                            pupilProfileBO = pupilProfileList.FirstOrDefault(p => p.PupilProfileID == intPupilFileID);
                                            if (pupilProfileBO == null) continue;

                                            //Not found from Contract Detail
                                            if (objPupilContractUspResult == null) continue;

                                            smsParentContractDetailID = objPupilContractUspResult.SMSParentContactDetailID;
                                            if (!objPupilSubscriber.Subscriber.CheckMobileNumber()) continue;
                                            //Lay brandName theo thue bao
                                            brandName = GetBrandNamebyMobile(objPupilSubscriber.Subscriber, lstPreNumberTelco, lstBrandName);

                                            #region Kiem tra gui tin voi goi dich vu GIOI HAN
                                            // Neu la goi trial
                                            if (objPupilContractUspResult.IsLimitPackage.HasValue && objPupilContractUspResult.IsLimitPackage.Value)
                                            {

                                                //// Kiem tra thoi gian cho phep gui trong thoi han, ke tu ngay dau noi
                                                detailLimitSP = LstLimitPackage.FirstOrDefault(o => o.SERVICE_PACKAGE_ID == objPupilContractUspResult.ServicePackageID
                                                    && o.EFFECTIVE_DAYS > 0 &&
                                                    ((o.FROM_DATE.HasValue && o.FROM_DATE.Value.Date <= objPupilContractUspResult.RegisterDate.Date &&
                                                    o.TO_DATE.HasValue && o.TO_DATE.Value.Date >= objPupilContractUspResult.RegisterDate.Date)
                                                    || (!o.TO_DATE.HasValue && !o.FROM_DATE.HasValue)));
                                                if (detailLimitSP != null)
                                                {
                                                    if ((DateTime.Now - objPupilContractUspResult.RegisterDate.Date).TotalDays > detailLimitSP.EFFECTIVE_DAYS) continue;
                                                }

                                                //Kiem tra so luong tin nhan chi (cho phep gui khi con tin nhan)
                                                if (objPupilContractUspResult.TotalSMS - objPupilContractUspResult.SentSMS <= 0) continue;
                                            }
                                            #endregion

                                            // Cac goi No cuoc kiem tra con so luong tin
                                            if (objPupilContractUspResult.Status == GlobalConstantsEdu.STATUS_UNPAID && objPupilContractUspResult.TotalSMS - objPupilContractUspResult.SentSMS <= 0) continue;

                                            //insert historySMS
                                            objHistorySMSInsertBO = new SMS_HISTORY();
                                            Guid HistoryRawID = Guid.NewGuid();
                                            objHistorySMSInsertBO.HISTORY_RAW_ID = HistoryRawID;
                                            objHistorySMSInsertBO.MOBILE = objPupilSubscriber.Subscriber;
                                            objHistorySMSInsertBO.IS_INTERNAL = objHistorySMSInsertBO.MOBILE.CheckMobileNumberVT();
                                            objHistorySMSInsertBO.RECEIVER_NAME = pupilProfileBO != null ? pupilProfileBO.FullName : string.Empty;
                                            if (typeHistory.HasValue && typeHistory.Value != 0) objHistorySMSInsertBO.SMS_TYPE_ID = typeHistory.Value;
                                            else objHistorySMSInsertBO.SMS_TYPE_ID = GlobalConstantsEdu.COMMON_HISTORYSMS_TO_COMMUNICATION_ID;
                                            objHistorySMSInsertBO.IS_CUSTOM_TYPE = isCustomType;
                                            objHistorySMSInsertBO.RECEIVE_TYPE = GlobalConstantsEdu.COMMON_HISTORY_RECEIVER_PARENT_ID;
                                            objHistorySMSInsertBO.CREATE_DATE = dateTimeNow;
                                            objHistorySMSInsertBO.SCHOOL_ID = schoolID;
                                            objHistorySMSInsertBO.YEAR = objAy.Year;
                                            objHistorySMSInsertBO.ACADEMIC_YEAR_ID = objAy.AcademicYearID;
                                            objHistorySMSInsertBO.SMS_CONTENT = content;
                                            objHistorySMSInsertBO.RECEIVER_ID = objPupilContractUspResult.PupilFileID;
                                            objHistorySMSInsertBO.SHORT_CONTENT = shortContent;
                                            objHistorySMSInsertBO.SENDER_NAME = fullNameSender;
                                            objHistorySMSInsertBO.SMS_CNT = countSMS;
                                            objHistorySMSInsertBO.SCHOOL_USERNAME = schoolUserName;
                                            objHistorySMSInsertBO.SMS_PARENTCONTRACT_DETAIL_ID = smsParentContractDetailID;
                                            if (inValidNumber) objHistorySMSInsertBO.FLAG_ID = GlobalConstantsEdu.COMMON_TABLE_SMSHISTORY_FLAG_INVALID_MOBILE;
                                            objHistorySMSInsertBO.CONTENT_TYPE = isSignMsg ? GlobalConstantsEdu.SMS_CONTENT_TYPE_UNICODE : GlobalConstantsEdu.SMS_CONTENT_TYPE_NOT_SIGN;
                                            objHistorySMSInsertBO.IS_SMS = true;
                                            objHistorySMSInsertBO.SENDER_GROUP = senderGroup;
                                            objHistorySMSInsertBO.PARTITION_ID = PartitionID;
                                            objHistorySMSInsertBO.IS_ADMIN = isAdmin;
                                            objHistorySMSInsertBO.SENDER_ID = senderID;
                                            lstHistorySMSInsertBO.Add(objHistorySMSInsertBO);

                                            //insert MT
                                            objMTInsertBO = new SMS_MT();
                                            objMTInsertBO.HISTORY_RAW_ID = HistoryRawID;
                                            objMTInsertBO.REQUEST_ID = Guid.NewGuid();
                                            objMTInsertBO.USER_ID = objPupilSubscriber.Subscriber;
                                            if (isSignMsg)
                                            {
                                                objMTInsertBO.CONTENT_TYPE = "1"; // có dấu
                                                objMTInsertBO.SMS_CONTENT = content;
                                            }
                                            else
                                            {
                                                objMTInsertBO.CONTENT_TYPE = "0"; // ko dấu
                                                objMTInsertBO.SMS_CONTENT = contentStripVNSign;
                                            }
                                            objMTInsertBO.STATUS = GlobalConstantsEdu.MT_STATUS_NOT_SEND;
                                            objMTInsertBO.TIME_SEND_REQUEST = dateTimeNow;
                                            objMTInsertBO.UNIT_ID = schoolID;
                                            objMTInsertBO.SERVICE_ID = brandName;
                                            lstMTInsertBO.Add(objMTInsertBO);
                                            //Chi tru khi gui qua MT
                                            objPupilContractUspResult.IsSent = true;
                                            objPupilContractUspResult.SentSMS += countSMS;
                                            isSendOnWebOK = true;

                                        }
                                        if (isSendOnWebOK)
                                        {
                                            countNumOfSend++;
                                        }
                                    }

                                    if (countNumOfSend == 0)
                                    {
                                        resultBO.isError = false;
                                        resultBO.NumSendSuccess = 0;
                                        return resultBO;
                                    }

                                    #region bulk insert
                                    if (lstHistorySMSInsertBO.Count > 0)
                                    {
                                        SMSHistoryBusiness.BulkInsert(lstHistorySMSInsertBO, this.ColumnHistoryMappings(), "HISTORY_ID");

                                        int insert = MTBusiness.BulkInsert(lstMTInsertBO, this.ColumnMTMappings(), "TIME_SEND_REQUEST");
                                        if (insert == 0)
                                        {
                                            List<SMS_HISTORY> lstDelete = new List<SMS_HISTORY>();
                                            List<Guid> lstSMSHistoryRawID = lstHistorySMSInsertBO.Select(p => p.HISTORY_RAW_ID).Distinct().ToList();
                                            lstDelete = SMSHistoryBusiness.All.Where(p => lstSMSHistoryRawID.Contains(p.HISTORY_RAW_ID) && p.PARTITION_ID == PartitionID).ToList();
                                            SMSHistoryBusiness.DeleteAll(lstDelete);
                                            SMSHistoryBusiness.Save();
                                            resultBO.isError = true;
                                            resultBO.ErrorMsg = ResEdu.Get("SendSMS_Label_CoreError");
                                            return resultBO;
                                        }
                                    }
                                    else
                                    {
                                        resultBO.isError = true;
                                        resultBO.ErrorMsg = ResEdu.Get("SendSMS_Label_CoreError");
                                        return resultBO;
                                    }
                                    #endregion

                                    #region cap nhat tin nhan theo tung thue bao/ cap nhat quy tin cua lop
                                    List<PupilContract> lstSent = lstPupilContract.Where(a => a.IsSent.HasValue && a.IsSent.Value == true).ToList();
                                    List<long> LstContractDetailID = lstSent.Select(o => o.SMSParentContactDetailID).Distinct().ToList();
                                    List<SMS_PARENT_SENT_DETAIL> lstPupilSentDetail = qSMSParentSendDetail.Where(c => LstContractDetailID.Contains(c.SMS_PARENT_CONTRACT_DETAIL_ID)).ToList();
                                    PupilContract itmSent = null;
                                    SMS_PARENT_SENT_DETAIL sentItem = null;
                                    for (int i = 0, SentCnt = lstSent.Count; i < SentCnt; i++)
                                    {
                                        itmSent = lstSent[i];
                                        if (itmSent.SMSParentSentDetailID > 0)
                                        {
                                            sentItem = lstPupilSentDetail.FirstOrDefault(a => a.SMS_PARENT_SENT_DETAIL_ID == itmSent.SMSParentSentDetailID);
                                            if (sentItem != null)
                                            {
                                                sentItem.UPDATED_DATE = dateTimeNow;
                                                sentItem.SENT_TOTAL = itmSent.SentSMS;
                                                SMSParentSentDetailBusiness.Update(sentItem);
                                            }
                                        }
                                        else
                                        {
                                            sentItem = new SMS_PARENT_SENT_DETAIL();
                                            sentItem.SEMESTER = semester;
                                            sentItem.YEAR = objAy.Year;
                                            sentItem.ACADEMIC_YEAR_ID = academicYearID;
                                            sentItem.CREATED_DATE = dateTimeNow;
                                            sentItem.SMS_PARENT_CONTRACT_DETAIL_ID = itmSent.SMSParentContactDetailID;
                                            sentItem.SENT_TOTAL = itmSent.SentSMS;
                                            sentItem.SCHOOL_ID = schoolID;
                                            sentItem.PARTITION_ID = PartitionID;
                                            SMSParentSentDetailBusiness.Insert(sentItem);
                                        }
                                    }

                                    // Cap nhat quy tin cua tung lop - HoanTV5 20170506
                                    //Tru lan luot quy tin theo hoc ky=>quy tin CN
                                    int? CountSMStmp = 0;
                                    bool breakout = false;
                                    for (int i = lstClassSent.Count - 1; i >= 0; i--)
                                    {
                                        vClassID = lstClassSent[i];
                                        CountSMStmp = 0;
                                        // cap nhat quy tin khong tinh goi co gioi han
                                        // danh sach hoc sinh trong lop
                                        lstPupilIDInClass = lstPupilOfClass.Where(o => o.ClassID == vClassID).Select(o => o.PupilID).ToList();
                                        // danh sach hoc sinh co HD tra tien trong lop va da gui thanh cong
                                        lstPupilIDInClass = lstPupilContract.Where(o => lstPupilIDInClass.Contains(o.PupilFileID) && o.IsLimitPackage != true && o.IsSent == true).Select(o => o.PupilFileID).ToList();
                                        lstClassDetailstmp = lstClassDetails.Where(c => c.ACADEMIC_YEAR_ID == objAy.AcademicYearID
                                                    && c.SCHOOL_ID == schoolID && c.YEAR == objAy.Year && c.CLASS_ID == vClassID).OrderBy(c => c.SEMESTER).ToList();
                                        for (int j = 0; j < lstClassDetailstmp.Count; j++)
                                        {
                                            objClassDetail = lstClassDetailstmp[j];
                                            // so tin da gui thanh cong trong lop
                                            int totalSentInClass = lstPupilContent.Where(o => lstPupilIDInClass.Contains(o.PupilFileID))
                                                .Sum(o => o.SMS_CNT * lstPupilContract.Count(p => p.PupilFileID == o.PupilFileID && p.IsLimitPackage != true
                                                    && p.Status == GlobalConstantsEdu.STATUS_REGISTED)) - CountSMStmp.Value;
                                            //tru quy tin lan luot, het quy HK thi tru sang quy tin CN
                                            CountSMStmp = objClassDetail.TOTAL_SMS - objClassDetail.SENT_TOTAL;
                                            if (CountSMStmp <= totalSentInClass)
                                            {
                                                objClassDetail.SENT_TOTAL = objClassDetail.TOTAL_SMS;
                                            }
                                            else
                                            {
                                                objClassDetail.SENT_TOTAL += totalSentInClass;
                                                breakout = true;
                                            }
                                            objClassDetail.UPDATED_DATE = DateTime.Now;
                                            if (objClassDetail.SMS_PARENT_CONTRACT_CLASS_ID > 0)
                                            {
                                                SMSParentContractInClassDetailBusiness.Update(objClassDetail);
                                            }
                                            if (breakout)
                                            {
                                                break;
                                            }
                                        }
                                    }
                                    this.Save();
                                    #endregion

                                    // Cap nhat thong tin 
                                    resultBO.NumSendSuccess = countNumOfSend;
                                    // danh sach cac lop khong gui
                                    resultBO.LstClassNotSent = lstClassNotSent;
                                    return resultBO;
                                    #endregion
                                }

                                #endregion
                            }
                            else if (contentList.Count > 0)
                            {
                                SMS_TIMER_CONFIG tc = new SMS_TIMER_CONFIG();
                                if (isTimerUsed)
                                {
                                    tc = new SMS_TIMER_CONFIG();
                                    tc.SMS_TIMER_CONFIG_ID = Guid.NewGuid();
                                    tc.CONTENT = content;
                                    tc.CREATE_TIME = DateTime.Now;
                                    tc.SCHOOL_ID = schoolID;
                                    tc.APPLIED_LEVEL = iLevelID;
                                    tc.PARTITION_ID = PartitionID;
                                    tc.SEND_TIME = sendTime.Value;
                                    tc.STATUS = GlobalConstantsEdu.TIMER_CONFIG_STATUS_ACTIVE;//chua gui
                                    tc.TARGET_ID = classID;
                                    tc.CLASS_SEND_ID = string.Join(",", lstClassID);
                                    tc.CONFIG_NAME = timerConfigName;
                                    tc.CONFIG_TYPE = GlobalConstantsEdu.TIMER_CONFIG_TYPE_CLASS;
                                    tc.SEND_ALL_CLASS = isAllClass;
                                    tc.SEND_UNICODE = isSignMsg;
                                    tc.IS_IMPORT = isSendImport;
                                    TimerConfigBusiness.Insert(tc);
                                    TimerConfigBusiness.Save();
                                }

                                #region gui cho phhs noi dung khac nhau doi voi tung hoc sinh trong 1 lop
                                int headTeacherID = EmployeeBusiness.GetHeadTeacherID(classID, objAy.AcademicYearID, schoolID);
                                Employee objE = null;
                                string fullNameSender = string.Empty;
                                if (isAdmin) fullNameSender = school.SchoolName;
                                else // giao vien
                                {
                                    objE = EmployeeBusiness.Find(senderID);
                                    if (objE != null) fullNameSender = objE.FullName;
                                }

                                if (string.IsNullOrEmpty(fullNameSender))
                                {
                                    resultBO.isError = true;
                                    resultBO.ErrorMsg = ResEdu.Get("SendSMS_Label_CoreError");
                                    return resultBO;
                                }
                                List<PupilOfClassBO> lstPupilStudying = PupilProfileBusiness.getListPupilByListPupilID(lstReceiverID, academicYearID).ToList();
                                //neu so luong hoc sinh dang hoc va ListContent va ShortContent khong giong nhau thi thong bao loi
                                if (lstPupilStudying != null && ((lstPupilStudying.Count != contentList.Count) || (lstPupilStudying.Count != lstShortContent.Count)))
                                {
                                    resultBO.isError = true;
                                    resultBO.ErrorMsg = ResEdu.Get("SendSMS_Label_CoreError");
                                    return resultBO;
                                }

                                // Kiem tra noi dung gui co nhu nhau hay ko?
                                bool isSameContent = true;
                                List<PupilContent> lstPupilContent = new List<PupilContent>();
                                PupilContent objPupilContent = null;
                                for (int i = 0, size = lstReceiverID.Count; i < size; i++)
                                {
                                    objPupilContent = new PupilContent();
                                    objPupilContent.PupilFileID = lstReceiverID[i];
                                    objPupilContent.Content = contentList[i].Trim();
                                    if (i < size - 1 && isSameContent)
                                    {
                                        if (string.Compare(objPupilContent.Content, contentList[i + 1].Trim()) != 0) isSameContent = false;
                                    }

                                    objPupilContent.SMS_CNT = objPupilContent.Content.countNumSMS(isSignMsg);
                                    objPupilContent.ShortContent = lstShortContent[i].Trim();
                                    lstPupilContent.Add(objPupilContent);
                                }
                                lstReceiverID = lstPupilStudying.Select(p => p.PupilID).ToList();

                                List<PupilContract> lstPupilContract = this.GetListPupilContract(schoolID, objAy.Year, semester, lstReceiverID);
                                if (lstPupilContract == null || lstPupilContract.Count == 0)
                                {
                                    resultBO.isError = true;
                                    resultBO.ErrorMsg = ResEdu.Get("SendSMS_Label_CoreError");
                                    return resultBO;
                                }

                                #region  Kiem tra quy tin cua lop truoc khi gui - HoanTV5 20170506
                                //Kiem tra quy tin theo HK va CN
                                SMS_PARENT_CONTRACT_CLASS objClassSentDetail = new SMS_PARENT_CONTRACT_CLASS();
                                SMS_PARENT_CONTRACT_CLASS objClassSentDetailOfAll = new SMS_PARENT_CONTRACT_CLASS();
                                IDictionary<string, object> dicSearchClassSentDetail = new Dictionary<string, object>()
                                {
                                    {"AcademicYearID",academicYearID},
                                    {"SchoolID",schoolID},
                                    {"ClassID",classID},
                                    {"Year",objAy.Year}
                                };
                                List<SMS_PARENT_CONTRACT_CLASS> lstClassSentDetails = SMSParentContractInClassDetailBusiness.Search(dicSearchClassSentDetail)
                                    .Where(c => (c.SEMESTER == semester || c.SEMESTER == GlobalConstantsEdu.SEMESTER_OF_YEAR_ALL)).OrderBy(c => c.SEMESTER).ToList();
                                int? countSentSMSDB = 0;
                                if (lstClassSentDetails.Count == 0)
                                {
                                    objClassSentDetail = SMSParentContractInClassDetailBusiness.GetMessageBudgetDetailInClass(schoolID, objAy.AcademicYearID, classID, semester);
                                    objClassSentDetailOfAll = SMSParentContractInClassDetailBusiness.GetMessageBudgetDetailInClass(schoolID, objAy.AcademicYearID, classID, GlobalConstantsEdu.SEMESTER_OF_YEAR_ALL);
                                    countSentSMSDB = objClassSentDetail.TOTAL_SMS + objClassSentDetailOfAll.TOTAL_SMS - objClassSentDetail.SENT_TOTAL - objClassSentDetailOfAll.SENT_TOTAL;
                                }
                                else
                                {
                                    countSentSMSDB = lstClassSentDetails.Sum(c => c.TOTAL_SMS) - lstClassSentDetails.Sum(c => c.SENT_TOTAL);
                                }

                                int totalSent = lstPupilContent.Sum(o => o.SMS_CNT * lstPupilContract.Count(p => p.PupilFileID == o.PupilFileID && p.IsLimitPackage != true && p.Status == GlobalConstantsEdu.STATUS_REGISTED));
                                bool isOutOfQuota = false;
                                // Neu het quota tin nhan
                                if (countSentSMSDB < totalSent)
                                {
                                    isOutOfQuota = true;
                                    // KO co goi thue bao No cuoc
                                    if (lstPupilContract.Count(o => o.Status == GlobalConstantsEdu.STATUS_UNPAID) == 0)
                                    {
                                        if (lstPupilContract.Count(o => (o.IsLimitPackage == true) && o.SentSMS < o.TotalSMS) > 0)
                                        {
                                            lstPupilContract = lstPupilContract.Where(o => o.IsLimitPackage == true).ToList();
                                        }
                                        else
                                        {
                                            resultBO.isError = true;
                                            resultBO.ErrorMsg = isSendSMSCommentOfPupil ? ResEdu.Get("SendSMSComment_Error_OutOfMessageBudget") : ResEdu.Get("SMSParent_Error_OutOfMessageBudget");
                                            return resultBO;
                                        }
                                    }
                                }
                                #endregion

                                PupilOfClassBO objPupilInfo = lstPupilStudying.FirstOrDefault();
                                PupilContract objContract = null;
                                List<PupilSubscriber> lstPupilSubscriber = null;
                                PupilSubscriber objPupilSubscriber = null;
                                SMS_SERVICE_PACKAGE_DECLARE detailLimitSP = null;
                                long smsParentContractDetailID = 0;
                                int countNumOfSend = 0;
                                int countSendSMS = 0;
                                int countTotalSentSMS = 0;
                                SMS_MT objMT = null;
                                //SMSCommunication objSMSCommunication = null;
                                SMS_HISTORY objHistorySMS = null;
                                //-> doi voi truong hop 1 hoc sinh ma co nhiu hon 1 so dien thoai nhan tin nhan: 
                                //Chi insert 1 record vao smscommunication de tranh truong hop xuat hien 2 noi dung giong nhau khi xem chi tiet hop thu 
                                for (int i = lstPupilStudying.Count - 1; i >= 0; i--)
                                {
                                    objPupilInfo = lstPupilStudying[i];
                                    objPupilContent = lstPupilContent.FirstOrDefault(p => p.PupilFileID == objPupilInfo.PupilID);
                                    content = objPupilContent.Content;
                                    shortContent = objPupilContent.ShortContent;
                                    contentStripVNSign = ExtensionMethods.ConvertLineToSpace(content.StripVNSign().StripVNSignComposed());
                                    countSMS = objPupilContent.SMS_CNT;

                                    List<string> lstInputedSensitiveWord = new List<string>();
                                    if (content.CheckContentSMS(ref lstInputedSensitiveWord))
                                    {
                                        resultBO.isError = true;
                                        resultBO.ErrorMsg = string.Format(ResEdu.Get("SendSMS_Label_Sensitive_Words"), string.Join(", ", lstInputedSensitiveWord));
                                        return resultBO;
                                    }

                                    lstPupilSubscriber = (from pc in lstPupilContract
                                                          where pc.PupilFileID == objPupilInfo.PupilID
                                                          select new PupilSubscriber
                                                          {
                                                              Subscriber = pc.Mobile,
                                                              SMSParentContractDetailID = pc.SMSParentContactDetailID
                                                          }).ToList();
                                    bool sendSMSSuccessfullyToAtLeastOneSubscriber = false; // Bien dung de danh dau co gui tin thanh cong den phu huynh 1 hoc sinh hay khong.
                                    // Mot hoc sinh xem nhu da nhan duoc tin nhan neu gui thanh cong den it nhat 1 so thue bao ung voi hoc sinh do
                                    for (int k = lstPupilSubscriber.Count - 1; k >= 0; k--)
                                    {
                                        objPupilSubscriber = lstPupilSubscriber[k];
                                        // Chỉ cho phép gửi tin có dấu với các đầu số cho trước
                                        if (isSignMsg && !objPupilSubscriber.Subscriber.isAllowSendSignSMSPhone()) continue;
                                        inValidNumber = false;
                                        objContract = (from pc in lstPupilContract where pc.Mobile == objPupilSubscriber.Subscriber && pc.PupilFileID == objPupilInfo.PupilID && pc.SMSParentContactDetailID == objPupilSubscriber.SMSParentContractDetailID select pc).FirstOrDefault();
                                        //Not found from Contract Detail
                                        if (objContract == null) continue;
                                        smsParentContractDetailID = objContract.SMSParentContactDetailID;
                                        objMT = null;
                                        objHistorySMS = null;
                                        //objSMSCommunication = null;

                                        if (!objPupilSubscriber.Subscriber.CheckMobileNumber()) continue;

                                        #region Kiem tra gui tin doi voi goi dich vu CO GIOI HAN
                                        // Neu la goi trial
                                        if (objContract.IsLimitPackage.HasValue && objContract.IsLimitPackage.Value)
                                        {

                                            //// Kiem tra thoi gian cho phep gui trong thoi han, ke tu ngay dau noi
                                            detailLimitSP = LstLimitPackage.FirstOrDefault(o => o.SERVICE_PACKAGE_ID == objContract.ServicePackageID
                                                && o.EFFECTIVE_DAYS > 0 &&
                                                ((o.FROM_DATE.HasValue && o.FROM_DATE.Value.Date <= objContract.RegisterDate.Date &&
                                                o.TO_DATE.HasValue && o.TO_DATE.Value.Date >= objContract.RegisterDate.Date)
                                                || (!o.TO_DATE.HasValue && !o.FROM_DATE.HasValue)));
                                            if (detailLimitSP != null)
                                            {
                                                if ((DateTime.Now - objContract.RegisterDate.Date).TotalDays > detailLimitSP.EFFECTIVE_DAYS) continue;
                                            }

                                            //Kiem tra so luong tin nhan chi (cho phep gui khi con tin nhan)
                                            if (objContract.TotalSMS - objContract.SentSMS <= 0) continue;
                                        }
                                        #endregion

                                        //Lay brandName theo thue bao
                                        brandName = GetBrandNamebyMobile(objPupilSubscriber.Subscriber, lstPreNumberTelco, lstBrandName);

                                        // Cac goi No cuoc kiem tra con so luong tin
                                        if (objContract.Status == GlobalConstantsEdu.STATUS_UNPAID && objContract.TotalSMS - objContract.SentSMS <= 0) continue;

                                        // Khong gui cac Hop dong khi het Quota
                                        if (isOutOfQuota && objContract.Status == GlobalConstantsEdu.STATUS_REGISTED) continue;
                                        //insert historySMS
                                        objHistorySMS = new SMS_HISTORY();
                                        Guid SMSHistoryRawID = Guid.NewGuid();
                                        objHistorySMS.HISTORY_RAW_ID = SMSHistoryRawID;
                                        objHistorySMS.MOBILE = objPupilSubscriber.Subscriber;
                                        objHistorySMS.IS_INTERNAL = objHistorySMS.MOBILE.CheckMobileNumberVT();
                                        objHistorySMS.RECEIVER_NAME = objPupilInfo.PupilFullName;
                                        if (typeHistory.HasValue && typeHistory.Value != 0)
                                        {
                                            objHistorySMS.SMS_TYPE_ID = typeHistory.Value;
                                            objHistorySMS.RECEIVE_TYPE = GlobalConstantsEdu.COMMON_HISTORY_RECEIVER_PARENT_ID;
                                        }
                                        else
                                        {
                                            objHistorySMS.SMS_TYPE_ID = GlobalConstantsEdu.COMMON_HISTORYSMS_TO_COMMUNICATION_ID;
                                            objHistorySMS.RECEIVE_TYPE = GlobalConstantsEdu.COMMON_HISTORY_RECEIVER_PARENT_ID;
                                        }

                                        objHistorySMS.IS_CUSTOM_TYPE = isCustomType;
                                        objHistorySMS.CREATE_DATE = dateTimeNow;
                                        objHistorySMS.SCHOOL_ID = schoolID;
                                        objHistorySMS.YEAR = objAy.Year;
                                        objHistorySMS.ACADEMIC_YEAR_ID = objAy.AcademicYearID;
                                        objHistorySMS.SMS_CONTENT = content;
                                        objHistorySMS.RECEIVER_ID = objPupilInfo.PupilID;
                                        objHistorySMS.SHORT_CONTENT = shortContent;
                                        objHistorySMS.SENDER_NAME = fullNameSender;
                                        objHistorySMS.SMS_CNT = countSMS;
                                        objHistorySMS.SCHOOL_USERNAME = schoolUserName;
                                        objHistorySMS.SMS_PARENTCONTRACT_DETAIL_ID = smsParentContractDetailID;
                                        objHistorySMS.CONTENT_TYPE = isSignMsg ? GlobalConstantsEdu.SMS_CONTENT_TYPE_UNICODE : GlobalConstantsEdu.SMS_CONTENT_TYPE_NOT_SIGN;
                                        objHistorySMS.IS_SMS = true;
                                        objHistorySMS.SENDER_GROUP = senderGroup;
                                        objHistorySMS.PARTITION_ID = PartitionID;
                                        objHistorySMS.IS_ADMIN = isAdmin;
                                        objHistorySMS.SENDER_ID = senderID;
                                        if (isTimerUsed)
                                        {
                                            objHistorySMS.SMS_TIMER_CONFIG_ID = tc.SMS_TIMER_CONFIG_ID;
                                            objHistorySMS.IS_ON_SCHEDULE = true;
                                            objHistorySMS.SEND_TIME = tc.SEND_TIME;
                                        }
                                        SMSHistoryBusiness.Insert(objHistorySMS);

                                        //insert MT
                                        objMT = new SMS_MT();
                                        objMT.REQUEST_ID = Guid.NewGuid();
                                        objMT.HISTORY_RAW_ID = SMSHistoryRawID;
                                        objMT.USER_ID = objPupilSubscriber.Subscriber;
                                        if (isSignMsg)
                                        {
                                            objMT.CONTENT_TYPE = "1"; // có dấu
                                            objMT.SMS_CONTENT = content;
                                        }
                                        else
                                        {
                                            objMT.CONTENT_TYPE = "0"; // ko dấu
                                            objMT.SMS_CONTENT = contentStripVNSign;
                                        }
                                        objMT.STATUS = GlobalConstantsEdu.MT_STATUS_NOT_SEND;
                                        objMT.TIME_SEND_REQUEST = dateTimeNow;
                                        objMT.UNIT_ID = schoolID;
                                        objMT.SERVICE_ID = brandName;

                                        if (isTimerUsed)
                                        {
                                            objMT.SMS_TIMER_CONFIG_ID = tc.SMS_TIMER_CONFIG_ID;
                                        }
                                        MTBusiness.Insert(objMT);
                                        countSendSMS++;

                                        //Chi tru khi gui qua MT
                                        objContract.IsSent = true;
                                        objContract.SentSMS += countSMS;
                                        // chi tinh tin nhan voi nhung goi KO GIOI HAN
                                        if (objContract.IsLimitPackage != true && objContract.Status == GlobalConstantsEdu.STATUS_REGISTED) countTotalSentSMS += countSMS;

                                        sendSMSSuccessfullyToAtLeastOneSubscriber = true; // Danh dau da gui thanh cong den cho hoc sinh dang xet

                                    }
                                    if (sendSMSSuccessfullyToAtLeastOneSubscriber)
                                    {
                                        countNumOfSend++; // Tang so luong hoc sinh da nhan tin nhan thanh cong
                                    }
                                }

                                if (countNumOfSend == 0)
                                {
                                    resultBO.isError = false;
                                    resultBO.NumSendSuccess = 0;
                                    return resultBO;
                                }

                                #region Send to head teacher - AnhVD9 20150922
                                if (schoolCfg != null && schoolCfg.IS_ALLOW_SEND_HEADTEACHER)
                                {
                                    if (isSameContent)
                                    {
                                        if (objE != null)
                                        {
                                            // Chỉ cho phép gửi tin có dấu với các đầu số cho trước
                                            if ((isSignMsg && objE.Mobile.isAllowSendSignSMSPhone()) ||
                                                (!isSignMsg && objE.Mobile.CheckMobileNumber())) // Tin thong thuong thi so dien thoai phai hop le
                                            {
                                                brandName = GetBrandNamebyMobile(objE.Mobile, lstPreNumberTelco, lstBrandName);
                                                //insert historySMS 
                                                objHistorySMS = new SMS_HISTORY();
                                                Guid SMSHistoryRawID = Guid.NewGuid();
                                                objHistorySMS.HISTORY_RAW_ID = SMSHistoryRawID;
                                                objHistorySMS.MOBILE = objE.Mobile;
                                                objHistorySMS.IS_INTERNAL = objHistorySMS.MOBILE.CheckMobileNumberVT();
                                                objHistorySMS.RECEIVER_NAME = objE.FullName;
                                                if (typeHistory.HasValue && typeHistory.Value != 0) objHistorySMS.SMS_TYPE_ID = typeHistory.Value;
                                                else objHistorySMS.SMS_TYPE_ID = GlobalConstantsEdu.COMMON_HISTORYSMS_TO_COMMUNICATION_ID;

                                                objHistorySMS.IS_CUSTOM_TYPE = isCustomType;
                                                objHistorySMS.RECEIVE_TYPE = GlobalConstantsEdu.COMMON_HISTORY_RECEIVER_TEACHER_ID;
                                                objHistorySMS.CREATE_DATE = dateTimeNow;
                                                objHistorySMS.SCHOOL_ID = schoolID;
                                                objHistorySMS.YEAR = objAy.Year;
                                                objHistorySMS.ACADEMIC_YEAR_ID = objAy.AcademicYearID;
                                                objHistorySMS.SMS_CONTENT = content;
                                                objHistorySMS.SENDER_NAME = fullNameSender;
                                                objHistorySMS.SHORT_CONTENT = shortContent;
                                                objHistorySMS.RECEIVER_ID = objE.EmployeeID;
                                                objHistorySMS.SMS_CNT = countSMS;
                                                objHistorySMS.SCHOOL_USERNAME = schoolUserName;
                                                objHistorySMS.SENDER_GROUP = senderGroup;
                                                objHistorySMS.CONTENT_TYPE = isSignMsg ? GlobalConstantsEdu.SMS_CONTENT_TYPE_UNICODE : GlobalConstantsEdu.SMS_CONTENT_TYPE_NOT_SIGN;
                                                objHistorySMS.IS_SMS = true;
                                                objHistorySMS.PARTITION_ID = PartitionID;
                                                objHistorySMS.IS_ADMIN = isAdmin;
                                                objHistorySMS.SENDER_ID = senderID;
                                                if (isTimerUsed)
                                                {
                                                    objHistorySMS.SMS_TIMER_CONFIG_ID = tc.SMS_TIMER_CONFIG_ID;
                                                    objHistorySMS.IS_ON_SCHEDULE = true;
                                                    objHistorySMS.SEND_TIME = tc.SEND_TIME;
                                                }
                                                SMSHistoryBusiness.Insert(objHistorySMS);
                                                //insert MT
                                                objMT = new SMS_MT();
                                                objMT.REQUEST_ID = Guid.NewGuid();
                                                objMT.HISTORY_RAW_ID = SMSHistoryRawID;
                                                if (isSignMsg)
                                                {
                                                    objMT.CONTENT_TYPE = "1"; // có dấu
                                                    objMT.SMS_CONTENT = content;
                                                }
                                                else
                                                {
                                                    objMT.CONTENT_TYPE = "0"; // ko dấu
                                                    objMT.SMS_CONTENT = contentStripVNSign;
                                                }
                                                objMT.STATUS = GlobalConstantsEdu.MT_STATUS_NOT_SEND;
                                                objMT.TIME_SEND_REQUEST = dateTimeNow;
                                                objMT.USER_ID = objHistorySMS.MOBILE;
                                                objMT.UNIT_ID = schoolID;
                                                objMT.SERVICE_ID = brandName;
                                                if (isTimerUsed)
                                                {
                                                    objMT.SMS_TIMER_CONFIG_ID = tc.SMS_TIMER_CONFIG_ID;
                                                }
                                                MTBusiness.Insert(objMT);
                                            }
                                        }
                                    }
                                }
                                #endregion

                                #region Cap nhat tin nhan theo tung thue bao
                                var lstSent = lstPupilContract.Where(a => a.IsSent.HasValue && a.IsSent.Value).ToList();
                                List<long> LstContractDetailID = lstSent.Select(o => o.SMSParentContactDetailID).Distinct().ToList();
                                List<SMS_PARENT_SENT_DETAIL> lstPupilSentDetail = qSMSParentSendDetail.Where(c => LstContractDetailID.Contains(c.SMS_PARENT_CONTRACT_DETAIL_ID)).ToList();
                                PupilContract itmSent = null;
                                SMS_PARENT_SENT_DETAIL sentItem = null;
                                int countSentSMS = 0;
                                List<SMS_PARENT_SENT_DETAIL> lstSmsParentSentDetailInsert = new List<SMS_PARENT_SENT_DETAIL>();
                                for (int i = 0, SentCnt = lstSent.Count; i < SentCnt; i++)
                                {
                                    itmSent = lstSent[i];
                                    if (itmSent.SMSParentSentDetailID > 0)
                                    {
                                        sentItem = lstPupilSentDetail.FirstOrDefault(a => a.SMS_PARENT_SENT_DETAIL_ID == itmSent.SMSParentSentDetailID);
                                        if (sentItem != null)
                                        {
                                            if (isTimerUsed)
                                            {
                                                //Luu lai quy tin cu
                                                SMS_TIMER_TRANSACTION tt = new SMS_TIMER_TRANSACTION();
                                                tt.SCHOOL_ID = schoolID;
                                                tt.TRANSACTION_TYPE = GlobalConstantsEdu.TIMER_TRANSACTION_SENT_DETAIL;
                                                tt.IS_ACTIVE = true;
                                                tt.SMS_PARENT_SENT_DETAIL_ID = sentItem.SMS_PARENT_SENT_DETAIL_ID;
                                                tt.SENT_TOTAL = itmSent.SentSMS - sentItem.SENT_TOTAL;
                                                tt.TIMER_CONFIG_ID = tc.SMS_TIMER_CONFIG_ID;
                                                TimerTransactionBusiness.Insert(tt);
                                            }
                                            sentItem.UPDATED_DATE = dateTimeNow;
                                            sentItem.SENT_TOTAL = itmSent.SentSMS;
                                            SMSParentSentDetailBusiness.Update(sentItem);
                                        }
                                    }
                                    else
                                    {
                                        sentItem = new SMS_PARENT_SENT_DETAIL();
                                        sentItem.SEMESTER = semester;
                                        sentItem.YEAR = objAy.Year;
                                        sentItem.ACADEMIC_YEAR_ID = academicYearID;
                                        sentItem.CREATED_DATE = dateTimeNow;
                                        sentItem.SMS_PARENT_CONTRACT_DETAIL_ID = itmSent.SMSParentContactDetailID;
                                        sentItem.SENT_TOTAL = itmSent.SentSMS;
                                        sentItem.SCHOOL_ID = schoolID;
                                        sentItem.PARTITION_ID = PartitionID;
                                        SMSParentSentDetailBusiness.Insert(sentItem);
                                        lstSmsParentSentDetailInsert.Add(sentItem);
                                    }
                                }
                                #endregion

                                #region Cap nhat lai quy tin trong lop - HoanTV5 20170506
                                //Tru lan luot quy tin: tru het quy HK sang tru quy CN
                                int? CountSMStmp = 0;
                                int? countSentSMSClass = 0;
                                bool breakout = false;
                                for (int i = 0; i < lstClassSentDetails.Count; i++)
                                {
                                    SMS_TIMER_TRANSACTION tt = new SMS_TIMER_TRANSACTION();
                                    objClassSentDetail = lstClassSentDetails[i];
                                    countSentSMSClass = countTotalSentSMS - CountSMStmp;

                                    CountSMStmp = objClassSentDetail.TOTAL_SMS - objClassSentDetail.SENT_TOTAL;
                                    if (CountSMStmp <= countSentSMSClass)
                                    {
                                        objClassSentDetail.SENT_TOTAL = objClassSentDetail.TOTAL_SMS;
                                        tt.SMS_NUMBER_ADDED = CountSMStmp.Value;
                                    }
                                    else
                                    {
                                        objClassSentDetail.SENT_TOTAL += countSentSMSClass.Value;
                                        tt.SMS_NUMBER_ADDED = countSentSMSClass.Value;
                                        breakout = true;
                                    }
                                    objClassSentDetail.UPDATED_DATE = DateTime.Now;
                                    if (objClassSentDetail.SMS_PARENT_CONTRACT_CLASS_ID > 0)
                                    {
                                        SMSParentContractInClassDetailBusiness.Update(objClassSentDetail);
                                    }
                                    if (isTimerUsed)
                                    {
                                        //Luu lai quy tin cu
                                        tt.SCHOOL_ID = schoolID;
                                        tt.TRANSACTION_TYPE = GlobalConstantsEdu.TIMER_TRANSACTION_CONTRACT_IN_CLASS;
                                        tt.IS_ACTIVE = true;
                                        tt.SMS_NUMBER_ADDED = countTotalSentSMS;
                                        tt.SMS_PARENT_CONTRACT_CLASS_ID = objClassSentDetail.SMS_PARENT_CONTRACT_CLASS_ID;
                                        tt.TIMER_CONFIG_ID = tc.SMS_TIMER_CONFIG_ID;
                                        TimerTransactionBusiness.Insert(tt);
                                    }
                                    
                                    if (breakout)
                                    {
                                        break;
                                    }
                                }
                                #endregion

                                if (isTimerUsed && lstSmsParentSentDetailInsert.Count > 0)
                                {
                                    for (int i = 0; i < lstSmsParentSentDetailInsert.Count; i++)
                                    {
                                        SMS_PARENT_SENT_DETAIL ssd = lstSmsParentSentDetailInsert[i];
                                        //Luu lai quy tin cu
                                        SMS_TIMER_TRANSACTION tt = new SMS_TIMER_TRANSACTION();
                                        tt.SCHOOL_ID = schoolID;
                                        tt.TRANSACTION_TYPE = GlobalConstantsEdu.TIMER_TRANSACTION_SENT_DETAIL;
                                        tt.IS_ACTIVE = true;
                                        tt.SMS_PARENT_SENT_DETAIL_ID = ssd.SMS_PARENT_SENT_DETAIL_ID;
                                        tt.SENT_TOTAL = ssd.SENT_TOTAL;
                                        tt.TIMER_CONFIG_ID = tc.SMS_TIMER_CONFIG_ID;
                                        TimerTransactionBusiness.Insert(tt);
                                    }
                                }
                                this.Save();
                                resultBO.NumSendSuccess = countNumOfSend;
                                return resultBO;
                                #endregion
                            }
                            else
                            {
                                SMS_TIMER_CONFIG tc = new SMS_TIMER_CONFIG();
                                if (isTimerUsed)
                                {
                                    tc = new SMS_TIMER_CONFIG();
                                    tc.SMS_TIMER_CONFIG_ID = Guid.NewGuid();
                                    tc.CONTENT = content;
                                    tc.CREATE_TIME = DateTime.Now;
                                    tc.SCHOOL_ID = schoolID;
                                    tc.APPLIED_LEVEL = iLevelID;
                                    tc.PARTITION_ID = PartitionID;
                                    tc.SEND_TIME = sendTime.Value;
                                    tc.STATUS = GlobalConstantsEdu.TIMER_CONFIG_STATUS_ACTIVE;//chua gui
                                    tc.TARGET_ID = classID;
                                    tc.CLASS_SEND_ID = string.Join(",", lstClassID);
                                    tc.CONFIG_NAME = timerConfigName;
                                    tc.CONFIG_TYPE = GlobalConstantsEdu.TIMER_CONFIG_TYPE_CLASS;
                                    tc.SEND_ALL_CLASS = isAllClass;
                                    tc.SEND_UNICODE = isSignMsg;
                                    tc.IS_IMPORT = isSendImport;
                                    TimerConfigBusiness.Insert(tc);
                                }
                                #region gui cho phhs theo tung lop/hoc sinh voi cung noi dung
                                int headTeacherID = EmployeeBusiness.GetHeadTeacherID(classID, academicYearID, schoolID);
                                Employee objE = null;
                                string fullNameSender = string.Empty;
                                if (isAdmin) fullNameSender = school.SchoolName;
                                else // giao vien
                                {
                                    objE = EmployeeBusiness.Find(senderID);
                                    if (objE != null) fullNameSender = objE.FullName;
                                }

                                if (string.IsNullOrEmpty(fullNameSender))
                                {
                                    resultBO.isError = true;
                                    resultBO.ErrorMsg = ResEdu.Get("SendSMS_Label_CoreError");
                                    return resultBO;
                                }

                                List<PupilOfClassBO> lstPupilStudying = PupilProfileBusiness.getListPupilByListPupilID(lstReceiverID,academicYearID).ToList();
                                lstReceiverID = lstPupilStudying.Select(p => p.PupilID).ToList();
                                List<PupilContract> lstPupilContract = this.GetListPupilContract(schoolID, objAy.Year, semester, lstReceiverID);
                                if (lstPupilContract == null || lstPupilContract.Count == 0)
                                {
                                    resultBO.isError = true;
                                    resultBO.ErrorMsg = ResEdu.Get("SendSMS_Label_ParentContractError");
                                    return resultBO;
                                }

                                #region  Kiem tra quy tin cua lop truoc khi gui - HoanTV5 20170506
                                //Check quy tin theo HK va quy tin cua ca nam
                                SMS_PARENT_CONTRACT_CLASS objClassSentDetail = null;
                                SMS_PARENT_CONTRACT_CLASS objClassSentDetailOfAll = null;
                                IDictionary<string, object> dicSearchClassSentDetail = new Dictionary<string, object>()
                                {
                                    {"AcademicYearID",academicYearID},
                                    {"SchoolID",schoolID},
                                    {"ClassID",classID},
                                    {"Year",objAy.Year}
                                };
                                List<SMS_PARENT_CONTRACT_CLASS> lstClassSentDetails = SMSParentContractInClassDetailBusiness.Search(dicSearchClassSentDetail)
                                    .Where(c => (c.SEMESTER == semester || c.SEMESTER == GlobalConstantsEdu.SEMESTER_OF_YEAR_ALL)).OrderBy(p => p.SEMESTER).ToList();
                                int? countTotalSMS_DB = 0;
                                if (lstClassSentDetails.Count == 0)
                                {
                                    objClassSentDetail = SMSParentContractInClassDetailBusiness.GetMessageBudgetDetailInClass(schoolID, objAy.AcademicYearID, classID, semester);
                                    objClassSentDetailOfAll = SMSParentContractInClassDetailBusiness.GetMessageBudgetDetailInClass(schoolID, objAy.AcademicYearID, classID, GlobalConstantsEdu.SEMESTER_OF_YEAR_ALL);
                                    countTotalSMS_DB = objClassSentDetail.TOTAL_SMS + objClassSentDetailOfAll.TOTAL_SMS - objClassSentDetail.SENT_TOTAL - objClassSentDetailOfAll.SENT_TOTAL;
                                }
                                else
                                {
                                    countTotalSMS_DB = lstClassSentDetails.Sum(p => p.TOTAL_SMS) - lstClassSentDetails.Sum(p => p.SENT_TOTAL);
                                }
                                int totalSent = countSMS * lstPupilContract.Count(o => o.IsLimitPackage != true && o.Status == GlobalConstantsEdu.STATUS_REGISTED);
                                bool isOutOfQuota = false;
                                // Neu het quota tin nhan
                                if (countTotalSMS_DB < totalSent)
                                {
                                    isOutOfQuota = true;
                                    // KO co thue bao No cuoc can gui nao
                                    if (lstPupilContract.Count(o => o.Status == GlobalConstantsEdu.STATUS_UNPAID) == 0)
                                    {
                                        if (lstPupilContract.Count(o => (o.IsLimitPackage == true) && o.SentSMS < o.TotalSMS) > 0)
                                        {
                                            lstPupilContract = lstPupilContract.Where(o => o.IsLimitPackage == true).ToList();
                                        }
                                        else
                                        {
                                            resultBO.isError = true;
                                            resultBO.ErrorMsg = isSendSMSCommentOfPupil ? ResEdu.Get("SendSMSComment_Error_OutOfMessageBudget") : ResEdu.Get("SMSParent_Error_OutOfMessageBudget");
                                            return resultBO;
                                        }
                                    }
                                }
                                #endregion
                                PupilOfClassBO objPupilInfo = null;
                                PupilContract objContract = null;
                                List<PupilSubscriber> lstPupilSubscriber = null;
                                PupilSubscriber objPupilSubscriber = null;
                                long smsParentContractDetailID = 0;
                                int countNumOfSend = 0;
                                int countTotalSentSMS = 0;
                                SMS_MT objMT = null;
                                SMS_HISTORY objHistorySMS = null;
                                SMS_SERVICE_PACKAGE_DECLARE detailLimitSP = null;
                                //-> doi voi truong hop 1 hoc sinh ma co nhiu hon 1 so dien thoai nhan tin nhan: 
                                //Chi insert 1 record vao smscommunication de tranh truong hop xuat hien 2 noi dung giong nhau khi xem chi tiet hop thu 
                                //bool isHasManyMobileReceiver = false;
                                //bool isFirstInsert = true;
                                bool isPhoneNotSupport = false;
                                for (int i = lstPupilStudying.Count - 1; i >= 0; i--)
                                {
                                    //isFirstInsert = true;
                                    //isHasManyMobileReceiver = false;
                                    objPupilInfo = lstPupilStudying[i];
                                    lstPupilSubscriber = (from pc in lstPupilContract
                                                          where pc.PupilFileID == objPupilInfo.PupilID
                                                          select new
                                                          {
                                                              Subscriber = pc.Mobile,
                                                              SMSParentContractDetailID = pc.SMSParentContactDetailID
                                                          }).Distinct().Select(o => new PupilSubscriber { Subscriber = o.Subscriber, SMSParentContractDetailID = o.SMSParentContractDetailID }).ToList();
                                    bool sendSMSSuccessfullyToAtLeastOneSubscriber = false; // Bien dung de danh dau co gui tin thanh cong den phu huynh 1 hoc sinh hay khong.

                                    // Mot hoc sinh xem nhu da nhan duoc tin nhan neu gui thanh cong den it nhat 1 so thue bao ung voi hoc sinh do
                                    for (int k = lstPupilSubscriber.Count - 1; k >= 0; k--)
                                    {
                                        objPupilSubscriber = lstPupilSubscriber[k];
                                        // Chỉ cho phép gửi tin có dấu với các đầu số cho trước
                                        if (isSignMsg && !objPupilSubscriber.Subscriber.isAllowSendSignSMSPhone())
                                        {
                                            isPhoneNotSupport = true;
                                            continue;
                                        }
                                        objContract = (from pc in lstPupilContract where pc.Mobile == objPupilSubscriber.Subscriber && pc.PupilFileID == objPupilInfo.PupilID && pc.SMSParentContactDetailID == objPupilSubscriber.SMSParentContractDetailID select pc).FirstOrDefault();
                                        //Not found from Contract Detail
                                        if (objContract == null) continue;
                                        smsParentContractDetailID = objContract.SMSParentContactDetailID;
                                        objMT = null;
                                        objHistorySMS = null;
                                        //objSMSCommunication = null;

                                        if (!objPupilSubscriber.Subscriber.CheckMobileNumber()) continue;

                                        #region Doi voi cac goi CO GIOI HAN
                                        // Neu la goi trial
                                        if (objContract.IsLimitPackage.HasValue && objContract.IsLimitPackage.Value)
                                        {
                                            //// Kiem tra thoi gian cho phep gui trong thoi han, ke tu ngay dau noi
                                            detailLimitSP = LstLimitPackage.FirstOrDefault(o => o.SERVICE_PACKAGE_ID == objContract.ServicePackageID
                                                && o.EFFECTIVE_DAYS > 0 &&
                                                ((o.FROM_DATE.HasValue && o.FROM_DATE.Value.Date <= objContract.RegisterDate.Date &&
                                                o.TO_DATE.HasValue && o.TO_DATE.Value.Date >= objContract.RegisterDate.Date)
                                                || (!o.TO_DATE.HasValue && !o.FROM_DATE.HasValue)));
                                            if (detailLimitSP != null)
                                            {
                                                if ((DateTime.Now - objContract.RegisterDate.Date).TotalDays > detailLimitSP.EFFECTIVE_DAYS)
                                                {
                                                    // Neu gui nhieu hoc sinh - hoac 1 hoc sinh co nhieu hop dong thi van tiep tuc
                                                    if ((lstReceiverID != null && lstReceiverID.Count > 1) || lstPupilSubscriber.Count > 1) continue;
                                                    resultBO.isError = true;
                                                    resultBO.ErrorMsg = string.Format("Gói dùng thử của phụ huynh học sinh đã hết hạn {0} ngày sử dụng.", detailLimitSP.EFFECTIVE_DAYS);
                                                    return resultBO;
                                                }
                                            }

                                            //Kiem tra so luong tin nhan chi (cho phep gui khi con tin nhan)
                                            if (objContract.TotalSMS - objContract.SentSMS <= 0)
                                            {
                                                // Neu gui nhieu hoc sinh - hoac 1 hoc sinh co nhieu hop dong thi van tiep tuc
                                                if ((lstReceiverID != null && lstReceiverID.Count > 1) || lstPupilSubscriber.Count > 1) continue;

                                                resultBO.isError = true;
                                                resultBO.ErrorMsg = ResEdu.Get("SendSMS_Label_ErrorOutOfQuotaPupil");
                                                return resultBO;
                                            }
                                        }
                                        #endregion

                                        //Lay brandName theo thue bao
                                        brandName = GetBrandNamebyMobile(objPupilSubscriber.Subscriber, lstPreNumberTelco, lstBrandName);

                                        // Cac goi No cuoc kiem tra con so luong tin
                                        if (objContract.Status == GlobalConstantsEdu.STATUS_UNPAID && objContract.TotalSMS - objContract.SentSMS <= 0) continue;

                                        //Khong gui khi het Quota
                                        if (isOutOfQuota && (objContract.Status == GlobalConstantsEdu.STATUS_REGISTED)) continue;
                                        //insert historySMS
                                        objHistorySMS = new SMS_HISTORY();
                                        Guid SMSHistoryRawID = Guid.NewGuid();
                                        objHistorySMS.HISTORY_RAW_ID = SMSHistoryRawID;
                                        objHistorySMS.MOBILE = objPupilSubscriber.Subscriber;
                                        objHistorySMS.IS_INTERNAL = objHistorySMS.MOBILE.CheckMobileNumberVT();
                                        objHistorySMS.RECEIVER_NAME = objPupilInfo.PupilFullName;
                                        if (typeHistory.HasValue && typeHistory.Value != 0)
                                        {
                                            objHistorySMS.SMS_TYPE_ID = typeHistory.Value;
                                            objHistorySMS.RECEIVE_TYPE = GlobalConstantsEdu.COMMON_HISTORY_RECEIVER_PARENT_ID;
                                        }
                                        else
                                        {
                                            objHistorySMS.SMS_TYPE_ID = GlobalConstantsEdu.COMMON_HISTORYSMS_TO_COMMUNICATION_ID;
                                            objHistorySMS.RECEIVE_TYPE = GlobalConstantsEdu.COMMON_HISTORY_RECEIVER_PARENT_ID;
                                        }

                                        objHistorySMS.IS_CUSTOM_TYPE = isCustomType;
                                        objHistorySMS.CREATE_DATE = dateTimeNow;
                                        objHistorySMS.SCHOOL_ID = schoolID;
                                        objHistorySMS.YEAR = objAy.Year;
                                        objHistorySMS.ACADEMIC_YEAR_ID = objAy.AcademicYearID;
                                        objHistorySMS.SMS_CONTENT = content;
                                        objHistorySMS.RECEIVER_ID = objPupilInfo.PupilID;
                                        objHistorySMS.SHORT_CONTENT = shortContent;
                                        objHistorySMS.SENDER_NAME = fullNameSender;
                                        objHistorySMS.SMS_CNT = countSMS;
                                        objHistorySMS.SCHOOL_USERNAME = schoolUserName;
                                        objHistorySMS.SMS_PARENTCONTRACT_DETAIL_ID = smsParentContractDetailID;
                                        if (inValidNumber) objHistorySMS.FLAG_ID = GlobalConstantsEdu.COMMON_TABLE_SMSHISTORY_FLAG_INVALID_MOBILE;
                                        objHistorySMS.CONTENT_TYPE = isSignMsg ? GlobalConstantsEdu.SMS_CONTENT_TYPE_UNICODE : GlobalConstantsEdu.SMS_CONTENT_TYPE_NOT_SIGN;
                                        objHistorySMS.IS_SMS = inValidNumber == false;
                                        objHistorySMS.SENDER_GROUP = senderGroup;
                                        objHistorySMS.PARTITION_ID = PartitionID;
                                        objHistorySMS.IS_ADMIN = isAdmin;
                                        objHistorySMS.SENDER_ID = senderID;
                                        if (isTimerUsed)
                                        {
                                            objHistorySMS.SMS_TIMER_CONFIG_ID = tc.SMS_TIMER_CONFIG_ID;
                                            objHistorySMS.IS_ON_SCHEDULE = true;
                                            objHistorySMS.SEND_TIME = tc.SEND_TIME;
                                        }
                                        SMSHistoryBusiness.Insert(objHistorySMS);

                                        //insert MT
                                        objMT = new SMS_MT();
                                        objMT.HISTORY_RAW_ID = SMSHistoryRawID;
                                        objMT.REQUEST_ID = Guid.NewGuid();
                                        objMT.USER_ID = objPupilSubscriber.Subscriber;
                                        if (isSignMsg)
                                        {
                                            objMT.CONTENT_TYPE = "1"; // có dấu
                                            objMT.SMS_CONTENT = content;
                                        }
                                        else
                                        {
                                            objMT.CONTENT_TYPE = "0"; // ko dấu
                                            objMT.SMS_CONTENT = contentStripVNSign;
                                        }
                                        objMT.STATUS = GlobalConstantsEdu.MT_STATUS_NOT_SEND;
                                        objMT.TIME_SEND_REQUEST = dateTimeNow;
                                        objMT.UNIT_ID = schoolID;
                                        objMT.SERVICE_ID = brandName;
                                        if (isTimerUsed)
                                        {
                                            objMT.SMS_TIMER_CONFIG_ID = tc.SMS_TIMER_CONFIG_ID;
                                        }
                                        MTBusiness.Insert(objMT);
                                        //Chi tru khi da gui qua MT
                                        objContract.IsSent = true;
                                        objContract.SentSMS += countSMS;
                                        // chi tinh tin nhan voi cac hop dong KO GIOI HAN
                                        if (objContract.IsLimitPackage != true && objContract.Status == GlobalConstantsEdu.STATUS_REGISTED) countTotalSentSMS += countSMS;
                                        sendSMSSuccessfullyToAtLeastOneSubscriber = true; // Danh dau da gui thanh cong den cho hoc sinh dang xet

                                    }
                                    if (sendSMSSuccessfullyToAtLeastOneSubscriber)
                                    {
                                        countNumOfSend++; // Tang so luong hoc sinh da nhan tin nhan thanh cong
                                    }
                                }

                                if (countNumOfSend == 0)
                                {
                                    if (isPhoneNotSupport)
                                    {
                                        resultBO.isError = true;
                                        resultBO.ErrorMsg = ResEdu.Get("Số điện thoại không hỗ trợ nhắn tin có dấu.");
                                        return resultBO;
                                    }
                                    else
                                    {
                                        resultBO.isError = true;
                                        resultBO.ErrorMsg = ResEdu.Get("SMSParent_Error_OutOfMessageBudget");
                                        return resultBO;
                                    }
                                }

                                #region Send to head teacher - AnhVD9 20150922
                                if (schoolCfg != null && schoolCfg.IS_ALLOW_SEND_HEADTEACHER)
                                {
                                    if (headTeacherID > 0) objE = EmployeeBusiness.Find(headTeacherID);
                                    if (objE != null)
                                    {
                                        // Chỉ cho phép gửi tin có dấu với các đầu số cho trước
                                        if ((isSignMsg && objE.Mobile.isAllowSendSignSMSPhone()) ||
                                            (!isSignMsg && objE.Mobile.CheckMobileNumber()))
                                        {
                                            //Lay brandName theo thue bao
                                            brandName = GetBrandNamebyMobile(objE.Mobile, lstPreNumberTelco, lstBrandName);
                                            //insert historySMS 
                                            objHistorySMS = new SMS_HISTORY();
                                            Guid SMSHistoryRawID = Guid.NewGuid();
                                            objHistorySMS.HISTORY_RAW_ID = SMSHistoryRawID;
                                            objHistorySMS.MOBILE = objE.Mobile;
                                            objHistorySMS.IS_INTERNAL = objHistorySMS.MOBILE.CheckMobileNumberVT();
                                            objHistorySMS.RECEIVER_NAME = objE.FullName;
                                            if (typeHistory.HasValue && typeHistory.Value != 0) objHistorySMS.SMS_TYPE_ID = typeHistory.Value;
                                            else objHistorySMS.SMS_TYPE_ID = GlobalConstantsEdu.COMMON_HISTORYSMS_TO_COMMUNICATION_ID;

                                            objHistorySMS.IS_CUSTOM_TYPE = isCustomType;
                                            objHistorySMS.RECEIVE_TYPE = GlobalConstantsEdu.COMMON_HISTORY_RECEIVER_TEACHER_ID;
                                            objHistorySMS.CREATE_DATE = dateTimeNow;
                                            objHistorySMS.SCHOOL_ID = schoolID;
                                            objHistorySMS.YEAR = objAy.Year;
                                            objHistorySMS.ACADEMIC_YEAR_ID = objAy.AcademicYearID;
                                            objHistorySMS.SMS_CONTENT = content;
                                            objHistorySMS.SENDER_NAME = fullNameSender;
                                            objHistorySMS.SHORT_CONTENT = shortContent;
                                            objHistorySMS.RECEIVER_ID = objE.EmployeeID;
                                            objHistorySMS.SMS_CNT = countSMS;
                                            objHistorySMS.SCHOOL_USERNAME = schoolUserName;
                                            objHistorySMS.SENDER_GROUP = senderGroup;
                                            objHistorySMS.CONTENT_TYPE = isSignMsg ? GlobalConstantsEdu.SMS_CONTENT_TYPE_UNICODE : GlobalConstantsEdu.SMS_CONTENT_TYPE_NOT_SIGN;
                                            objHistorySMS.IS_SMS = true;
                                            objHistorySMS.PARTITION_ID = PartitionID;
                                            objHistorySMS.IS_ADMIN = isAdmin;
                                            objHistorySMS.SENDER_ID = senderID;
                                            if (isTimerUsed)
                                            {
                                                objHistorySMS.SMS_TIMER_CONFIG_ID = tc.SMS_TIMER_CONFIG_ID;
                                                objHistorySMS.IS_ON_SCHEDULE = true;
                                                objHistorySMS.SEND_TIME = tc.SEND_TIME;
                                            }
                                            SMSHistoryBusiness.Insert(objHistorySMS);
                                            //insert MT
                                            objMT = new SMS_MT();
                                            objMT.REQUEST_ID = Guid.NewGuid();
                                            objMT.HISTORY_RAW_ID = SMSHistoryRawID;
                                            if (isSignMsg)
                                            {
                                                objMT.CONTENT_TYPE = "1"; // có dấu
                                                objMT.SMS_CONTENT = content;
                                            }
                                            else
                                            {
                                                objMT.CONTENT_TYPE = "0"; // ko dấu
                                                objMT.SMS_CONTENT = contentStripVNSign;
                                            }
                                            objMT.STATUS = GlobalConstantsEdu.MT_STATUS_NOT_SEND;
                                            objMT.TIME_SEND_REQUEST = dateTimeNow;
                                            objMT.USER_ID = objHistorySMS.MOBILE;
                                            objMT.UNIT_ID = schoolID;
                                            objMT.SERVICE_ID = brandName;
                                            objMT.RETRY_NUM = 0;
                                            if (isTimerUsed)
                                            {
                                                objMT.SMS_TIMER_CONFIG_ID = tc.SMS_TIMER_CONFIG_ID;
                                            }
                                            MTBusiness.Insert(objMT);
                                        }
                                    }
                                }
                                #endregion

                                #region Cap nhat lai so tin nhan da gui theo tung thue bao
                                var lstSent = lstPupilContract.Where(a => a.IsSent.HasValue && a.IsSent.Value == true).ToList();
                                List<long> LstContractDetailID = lstSent.Select(o => o.SMSParentContactDetailID).Distinct().ToList();
                                List<SMS_PARENT_SENT_DETAIL> lstPupilSentDetail = qSMSParentSendDetail.Where(c => LstContractDetailID.Contains(c.SMS_PARENT_CONTRACT_DETAIL_ID)).ToList();

                                PupilContract itmSent = null;
                                SMS_PARENT_SENT_DETAIL sentItem = null;
                                int countSentSMS = 0;
                                List<SMS_PARENT_SENT_DETAIL> lstSmsParentSentDetailInsert = new List<SMS_PARENT_SENT_DETAIL>();
                                for (int i = 0, SentCnt = lstSent.Count; i < SentCnt; i++)
                                {
                                    itmSent = lstSent[i];
                                    if (itmSent.SMSParentSentDetailID > 0)
                                    {
                                        sentItem = lstPupilSentDetail.FirstOrDefault(a => a.SMS_PARENT_SENT_DETAIL_ID == itmSent.SMSParentSentDetailID);
                                        if (sentItem != null)
                                        {
                                            if (isTimerUsed)
                                            {
                                                //Luu lai quy tin cu
                                                SMS_TIMER_TRANSACTION tt = new SMS_TIMER_TRANSACTION();
                                                tt.SCHOOL_ID = schoolID;
                                                tt.TRANSACTION_TYPE = GlobalConstantsEdu.TIMER_TRANSACTION_SENT_DETAIL;
                                                tt.IS_ACTIVE = true;
                                                tt.SMS_PARENT_SENT_DETAIL_ID = sentItem.SMS_PARENT_SENT_DETAIL_ID;
                                                tt.SENT_TOTAL = itmSent.SentSMS - sentItem.SENT_TOTAL;
                                                tt.TIMER_CONFIG_ID = tc.SMS_TIMER_CONFIG_ID;
                                                TimerTransactionBusiness.Insert(tt);
                                            }

                                            sentItem.UPDATED_DATE = DateTime.Now;
                                            sentItem.SENT_TOTAL = itmSent.SentSMS;
                                            SMSParentSentDetailBusiness.Update(sentItem);
                                        }
                                    }
                                    else
                                    {

                                        sentItem = new SMS_PARENT_SENT_DETAIL();
                                        sentItem.SEMESTER = semester;
                                        sentItem.YEAR = objAy.Year;
                                        sentItem.ACADEMIC_YEAR_ID = academicYearID;
                                        sentItem.CREATED_DATE = DateTime.Now;
                                        sentItem.SMS_PARENT_CONTRACT_DETAIL_ID = itmSent.SMSParentContactDetailID;
                                        sentItem.SENT_TOTAL = itmSent.SentSMS;
                                        sentItem.SCHOOL_ID = schoolID;
                                        sentItem.PARTITION_ID = PartitionID;
                                        SMSParentSentDetailBusiness.Insert(sentItem);
                                        lstSmsParentSentDetailInsert.Add(sentItem);
                                    }
                                }
                                #endregion

                                #region Cap nhat lai quy tin trong lop - HoanTV5 20170506 tru lan luot quy tin theo HK
                                int? CountSMStmp = 0;
                                int? countSentSMSClass = 0;
                                bool breakout = false;
                                for (int i = 0; i < lstClassSentDetails.Count; i++)
                                {
                                    SMS_TIMER_TRANSACTION tt = new SMS_TIMER_TRANSACTION();
                                    objClassSentDetail = lstClassSentDetails[i];
                                    countSentSMSClass = countTotalSentSMS - CountSMStmp;

                                    CountSMStmp = objClassSentDetail.TOTAL_SMS - objClassSentDetail.SENT_TOTAL;
                                    if (CountSMStmp <= countSentSMSClass)
                                    {
                                        objClassSentDetail.SENT_TOTAL = objClassSentDetail.TOTAL_SMS;
                                        tt.SMS_NUMBER_ADDED = CountSMStmp.Value;
                                    }
                                    else
                                    {
                                        objClassSentDetail.SENT_TOTAL += countSentSMSClass.Value;
                                        tt.SMS_NUMBER_ADDED = countSentSMSClass.Value;
                                        breakout = true;
                                    }
                                    objClassSentDetail.UPDATED_DATE = DateTime.Now;
                                    if (objClassSentDetail.SMS_PARENT_CONTRACT_CLASS_ID > 0)
                                    {
                                        SMSParentContractInClassDetailBusiness.Update(objClassSentDetail);
                                    }
                                    if (isTimerUsed)
                                    {
                                        //Luu lai quy tin cu
                                        tt.SCHOOL_ID = schoolID;
                                        tt.TRANSACTION_TYPE = GlobalConstantsEdu.TIMER_TRANSACTION_CONTRACT_IN_CLASS;
                                        tt.IS_ACTIVE = true;
                                        tt.SMS_NUMBER_ADDED = countTotalSentSMS;
                                        tt.SMS_PARENT_CONTRACT_CLASS_ID = objClassSentDetail.SMS_PARENT_CONTRACT_CLASS_ID;
                                        tt.TIMER_CONFIG_ID = tc.SMS_TIMER_CONFIG_ID;

                                        TimerTransactionBusiness.Insert(tt);
                                    }
                                    if (breakout)
                                    {
                                        break;
                                    }
                                }
                                #endregion

                                if (isTimerUsed && lstSmsParentSentDetailInsert.Count > 0)
                                {
                                    for (int i = 0; i < lstSmsParentSentDetailInsert.Count; i++)
                                    {
                                        SMS_PARENT_SENT_DETAIL ssd = lstSmsParentSentDetailInsert[i];
                                        //Luu lai quy tin cu
                                        SMS_TIMER_TRANSACTION tt = new SMS_TIMER_TRANSACTION();
                                        tt.SCHOOL_ID = schoolID;
                                        tt.TRANSACTION_TYPE = GlobalConstantsEdu.TIMER_TRANSACTION_SENT_DETAIL;
                                        tt.IS_ACTIVE = true;
                                        tt.SMS_PARENT_SENT_DETAIL_ID = ssd.SMS_PARENT_SENT_DETAIL_ID;
                                        tt.SENT_TOTAL = ssd.SENT_TOTAL;
                                        tt.TIMER_CONFIG_ID = tc.SMS_TIMER_CONFIG_ID;
                                        TimerTransactionBusiness.Insert(tt);
                                    }
                                }
                                this.Save();
                                resultBO.NumSendSuccess = countNumOfSend;
                                return resultBO;
                                #endregion
                            }
                            #endregion

                        }
                    case GlobalConstantsEdu.COMMON_SMS_COMUNICATION_TEACHER_TO_TEACHER:
                        {
                            #region teacher to teacher
                            SMSSchoolTeacherBO objInputSMS = new SMSSchoolTeacherBO
                            {
                                ClassId = classID,
                                ContactGroupId = contactGroupID,
                                Content = content,
                                CountSMS = countSMS,
                                EducationLevelId = iEducationLevelID,
                                IsAdmin = isAdmin,
                                IsPrincipal = isPrincipal,
                                IsSendSMSCommentOfPupil = isSendSMSCommentOfPupil,
                                IsSignMsg = isSignMsg,
                                LevelId = iLevelID,
                                ListContent = contentList,
                                ListReceiverId = lstReceiverID,
                                ListShortContent = lstShortContent,
                                Promotion = promotion,
                                SchoolId = schoolID,
                                SchoolUserName = schoolUserName,
                                Semester = semester,
                                SendAllSchool = sendAllSchool,
                                SenderGroup = senderGroup,
                                SenderID = senderID,
                                ShortContent = shortContent,
                                Type = type,
                                TypeHistory = typeHistory,
                                AcademicYearID = academicYearID

                            };
                            resultBO = SendSmstoTeacher(objInputSMS);

                            #endregion
                            break;
                        }
                    default:
                        break;
                }
            }
            catch (DbEntityValidationException e)
            {
                foreach (var eve in e.EntityValidationErrors)
                {
                    System.Diagnostics.Debug.WriteLine("Entity of type \"{0}\" in state \"{1}\" has the following validation errors:",
                        eve.Entry.Entity.GetType().Name, eve.Entry.State);
                    foreach (var ve in eve.ValidationErrors)
                    {
                        System.Diagnostics.Debug.WriteLine("- Property: \"{0}\", Error: \"{1}\"",
                            ve.PropertyName, ve.ErrorMessage);
                    }
                }
                string para = "null";
                LogExtensions.ErrorExt(logger, DateTime.Now, "SendSMS", para, e);
            }
            catch (Exception ex)
            {
                string para = "null";
                LogExtensions.ErrorExt(logger, DateTime.Now, "SendSMS", para, ex);
            }
            finally
            {
                SetAutoDetectChangesEnabled(true);
            }


            return resultBO;
        }
        public ResultBO SendSmstoTeacher(SMSSchoolTeacherBO inputSMS)
        {
            ResultBO resultBO = new ResultBO();
            try
            {
                SetAutoDetectChangesEnabled(false);
                SMS_SENT_RECORD sentRecord = null;
                bool isInsertSentRecord = false;
                DateTime dateTimeNow = DateTime.Now;
                int partitionID = UtilsBusiness.GetPartionId(inputSMS.SchoolId);
                SchoolProfile objSP = SchoolProfileBusiness.Find(inputSMS.SchoolId);
                AcademicYear objAy = AcademicYearBusiness.Find(inputSMS.AcademicYearID);

                List<BrandNameUsingBO> lstBrandName = BrandnameRegistrationBusiness.GetBrandNameUsing(inputSMS.SchoolId);
                if (objSP != null && objSP.SMSTeacherActiveType != 0 && objSP.SMSTeacherActiveType == GlobalConstantsEdu.COMMON_SCHOOL_SMS_ACTIVE_NO_LIMIT)
                {
                    sentRecord = SentRecordBusiness.All.Where(s => s.SCHOOL_ID == inputSMS.SchoolId
                                                            && s.SENT_MONTH == dateTimeNow.Month
                                                            && s.SENT_YEAR == dateTimeNow.Year).FirstOrDefault();
                    if (sentRecord == null)
                    {
                        sentRecord = new SMS_SENT_RECORD();
                        sentRecord.SENT_COUNT = 0;
                        sentRecord.SENT_TIME = 0;
                        sentRecord.SENT_MONTH = dateTimeNow.Month;
                        sentRecord.SENT_YEAR = dateTimeNow.Year;
                        sentRecord.SCHOOL_ID = inputSMS.SchoolId;
                        sentRecord.PARTITION_ID = partitionID;
                        isInsertSentRecord = true;
                    }
                    sentRecord.SENT_DATE = dateTimeNow;
                }
                else
                {
                    resultBO.isError = true;
                    resultBO.ErrorMsg = ResEdu.Get("SendSMS_Label_CoreError");
                    return resultBO;
                }

                List<string> lstInputedSensitiveWord = new List<string>();
                if (Utils.StripVNSign(inputSMS.Content.ToUpper()).CheckContentSMS(ref lstInputedSensitiveWord))
                {
                    resultBO.isError = true;
                    resultBO.ErrorMsg = string.Format("Nội dung thư chứa các từ không hợp lệ: {0}. Vui lòng kiểm tra lại nội dung thư!", string.Join(", ", lstInputedSensitiveWord));
                    return resultBO;
                }

                string fullNameSender = string.Empty;
                List<TeacherBO> lstEmployeeContactBO = new List<TeacherBO>();
                SMS_TEACHER_CONTACT_GROUP objContactGroup = ContactGroupBusiness.All.Where(p => p.CONTACT_GROUP_ID == inputSMS.ContactGroupId && p.SCHOOL_ID == inputSMS.SchoolId).FirstOrDefault();
                //kiem tra neu la defaultGroup thi gui cho toan truong
                if (objContactGroup.IS_DEFAULT)
                {
                    //chi co admin voi ban giam hieu moi duoc quyen gui tin nhan toan truong
                    if (inputSMS.IsAdmin)
                    {
                        if (inputSMS.ListReceiverId != null && inputSMS.ListReceiverId.Count > 0) lstEmployeeContactBO = EmployeeBusiness.GetListTeacher(inputSMS.ListReceiverId);
                        else lstEmployeeContactBO = EmployeeBusiness.GetTeachersOfSchool(inputSMS.SchoolId, objAy.AcademicYearID);
                        fullNameSender = objSP.SchoolName;
                    }
                    else if (inputSMS.IsPrincipal)
                    {
                        inputSMS.IsAdmin = true;
                        if (inputSMS.ListReceiverId != null && inputSMS.ListReceiverId.Count > 0)
                            lstEmployeeContactBO = EmployeeBusiness.GetListTeacher(inputSMS.ListReceiverId);
                        else lstEmployeeContactBO = EmployeeBusiness.GetTeachersOfSchool(inputSMS.SchoolId, inputSMS.AcademicYearID);

                        if (lstEmployeeContactBO != null && lstEmployeeContactBO.Count > 0)
                            lstEmployeeContactBO = lstEmployeeContactBO.Where(p => p.TeacherID != inputSMS.SenderID).ToList();

                        Employee teacherBO = EmployeeBusiness.Find(inputSMS.SenderID);
                        if (teacherBO != null) fullNameSender = teacherBO.FullName;
                    }
                }
                else
                {
                    IQueryable<TeacherBO> iqtmp = (from ec in TeacherContactBusiness.All
                                                   join cg in ContactGroupBusiness.All on ec.CONTACT_GROUP_ID equals cg.CONTACT_GROUP_ID
                                                   where ec.CONTACT_GROUP_ID == inputSMS.ContactGroupId
                                                   && ec.IS_ACTIVE == true
                                                   && ec.TEACHER_ID != inputSMS.SenderID
                                                   && cg.SCHOOL_ID == inputSMS.SchoolId
                                                   select new TeacherBO
                                                   {
                                                       TeacherID = ec.TEACHER_ID,
                                                   });
                    if (inputSMS.ListReceiverId.Count > 0)
                    {
                        iqtmp = iqtmp.Where(p => inputSMS.ListReceiverId.Contains(p.TeacherID));
                    }
                    lstEmployeeContactBO = iqtmp.ToList();

                    List<int> lstTeacherInContact = lstEmployeeContactBO.Select(p => p.TeacherID).ToList();
                    lstEmployeeContactBO = EmployeeBusiness.GetListTeacher(lstTeacherInContact, GlobalConstants.EMPLOYMENT_STATUS_WORKING);

                    if (inputSMS.IsAdmin) fullNameSender = objSP.SchoolName;
                    else
                    {
                        Employee teacherBO = EmployeeBusiness.Find(inputSMS.SenderID);
                        if (teacherBO != null) fullNameSender = teacherBO.FullName;
                    }
                }

                if (string.IsNullOrEmpty(fullNameSender))
                {
                    resultBO.isError = true;
                    resultBO.ErrorMsg = ResEdu.Get("SendSMS_Label_CoreError");
                    return resultBO;
                }
                if (lstEmployeeContactBO == null || lstEmployeeContactBO.Count == 0)
                {
                    resultBO.isError = true;
                    resultBO.ErrorMsg = ResEdu.Get("SendSMS_Label_ContactGroupError");
                    return resultBO;
                }

                #region Han muc tin nhan theo chuong trinh KM
                List<int> LstTeacherReceived = lstEmployeeContactBO.Select(o => o.TeacherID).ToList();

                //Lay tin nhan da gui trong thang 
                List<SMS_RECEIVED_RECORD> LstReceivedRecord = (from rr in ReceivedRecordBusiness.All
                                                               where rr.SCHOOL_ID == inputSMS.SchoolId && rr.RECEIVED_TYPE == GlobalConstantsEdu.COMMON_HISTORY_RECEIVER_TEACHER_ID
                                                               && rr.RECEIVED_YEAR == dateTimeNow.Year && rr.RECEIVED_MONTH == dateTimeNow.Month
                                                               && rr.EMPLOYEE_ID > 0 && LstTeacherReceived.Contains(rr.EMPLOYEE_ID)
                                                               && rr.PARTITION_ID == partitionID
                                                               select rr).ToList();

                #endregion

                TeacherBO objEmployeeReceiver = null;

                SMS_MT objMT = null;
                SMS_HISTORY objHistorySMS = null;
                int countNumOfSend = 0;
                decimal priceSendSMS = 0;
                // AnhVD9 20141027 - Bổ sung thông tin số tin nội, ngoại mạng
                int InternalSendSMS = 0;
                int ExternalSendSMS = 0;

                SMS_RECEIVED_RECORD recordReceived = null;
                //bool isFirstInsert = true;
                //schoolUserName = systemLoaderBusiness.GetUserNameBySchoolID(inputSMS.SchoolId);

                ///Neu la tin nhan khong dau thi loai bo dau
                string contentStripVNSign = "";
                if (!inputSMS.IsSignMsg)
                {
                    contentStripVNSign = ExtensionMethods.ConvertLineToSpace(inputSMS.Content.StripVNSign().StripVNSignComposed());
                }
                string brandName = GlobalConstantsEdu.BRANDNAME_DEFAULT;
                for (int i = lstEmployeeContactBO.Count - 1; i >= 0; i--)
                {

                    #region Tinh toan gui tin nhan cho tung giao vien
                    objEmployeeReceiver = lstEmployeeContactBO[i];
                    // Chỉ cho phép gửi tin có dấu với các đầu số cho trước
                    if (inputSMS.IsSignMsg && !objEmployeeReceiver.Mobile.isAllowSendSignSMSPhone()) continue;
                    if (!objEmployeeReceiver.Mobile.CheckMobileNumber()) continue;

                    brandName = GetBrandNamebyMobile(objEmployeeReceiver.Mobile, lstPreNumberTelco, lstBrandName);
                    //insert historySMS 
                    objHistorySMS = new SMS_HISTORY();
                    Guid HistoryRawID = Guid.NewGuid();
                    objHistorySMS.HISTORY_RAW_ID = HistoryRawID;
                    objHistorySMS.MOBILE = objEmployeeReceiver.Mobile;
                    objHistorySMS.IS_INTERNAL = objHistorySMS.MOBILE.CheckMobileNumberVT();
                    objHistorySMS.RECEIVER_NAME = objEmployeeReceiver.FullName;
                    if (inputSMS.TypeHistory.HasValue && inputSMS.TypeHistory.Value != 0) objHistorySMS.SMS_TYPE_ID = inputSMS.TypeHistory.Value;
                    else objHistorySMS.SMS_TYPE_ID = GlobalConstantsEdu.COMMON_HISTORYSMS_TO_COMMUNICATION_TEACHER;

                    objHistorySMS.RECEIVE_TYPE = GlobalConstantsEdu.COMMON_HISTORY_RECEIVER_TEACHER_ID;
                    objHistorySMS.CREATE_DATE = dateTimeNow;
                    objHistorySMS.SCHOOL_ID = inputSMS.SchoolId;
                    objHistorySMS.YEAR = objAy.Year;
                    objHistorySMS.ACADEMIC_YEAR_ID = objAy.AcademicYearID;
                    objHistorySMS.SMS_CONTENT = inputSMS.Content;
                    objHistorySMS.SENDER_NAME = fullNameSender;
                    objHistorySMS.SHORT_CONTENT = inputSMS.ShortContent;
                    objHistorySMS.RECEIVER_ID = objEmployeeReceiver.TeacherID;
                    objHistorySMS.SMS_CNT = inputSMS.CountSMS;
                    objHistorySMS.SCHOOL_USERNAME = inputSMS.SchoolUserName;
                    objHistorySMS.SENDER_GROUP = inputSMS.SenderGroup;
                    objHistorySMS.CONTENT_TYPE = inputSMS.IsSignMsg ? GlobalConstantsEdu.SMS_CONTENT_TYPE_UNICODE : GlobalConstantsEdu.SMS_CONTENT_TYPE_NOT_SIGN;
                    objHistorySMS.IS_SMS = true;
                    objHistorySMS.PARTITION_ID = partitionID;
                    objHistorySMS.SERVICE_ID = brandName;
                    objHistorySMS.IS_ADMIN = inputSMS.IsAdmin;
                    objHistorySMS.SENDER_ID = inputSMS.SenderID;
                    objHistorySMS.CONTACT_GROUP_ID = inputSMS.ContactGroupId;

                    SMSHistoryBusiness.Insert(objHistorySMS);

                    // ReceivedRecord
                    recordReceived = LstReceivedRecord.FirstOrDefault(o => o.EMPLOYEE_ID == objEmployeeReceiver.TeacherID);
                    // Luu thong tin tin nhan da gui
                    if (recordReceived == null)
                    {
                        recordReceived = new SMS_RECEIVED_RECORD();
                        recordReceived.SCHOOL_ID = inputSMS.SchoolId;
                        recordReceived.RECEIVED_TYPE = GlobalConstantsEdu.COMMON_HISTORY_RECEIVER_TEACHER_ID;
                        recordReceived.EMPLOYEE_ID = objEmployeeReceiver.TeacherID;
                        recordReceived.RECEIVED_YEAR = dateTimeNow.Year;
                        recordReceived.RECEIVED_MONTH = dateTimeNow.Month;
                        recordReceived.TOTAL_RECEIVED_SMS = 0;
                        recordReceived.CREATED_TIME = dateTimeNow.Date;
                        recordReceived.TOTAL_INTERNAL_SMS = 0;
                        recordReceived.TOTAL_EXTERNAL_SMS = 0;
                        recordReceived.PARTITION_ID = partitionID;
                        ReceivedRecordBusiness.Insert(recordReceived);
                    }

                    //insert MT
                    objMT = new SMS_MT();
                    objMT.REQUEST_ID = Guid.NewGuid();
                    objMT.HISTORY_RAW_ID = HistoryRawID;
                    if (inputSMS.IsSignMsg)
                    {
                        objMT.CONTENT_TYPE = GlobalConstantsEdu.SMS_CONTENT_TYPE_UNICODE.ToString(); // có dấu
                        objMT.SMS_CONTENT = inputSMS.Content;
                    }
                    else
                    {
                        objMT.CONTENT_TYPE = GlobalConstantsEdu.SMS_CONTENT_TYPE_NOT_SIGN.ToString(); // ko dấu
                        objMT.SMS_CONTENT = contentStripVNSign;
                    }

                    objMT.STATUS = GlobalConstantsEdu.MT_STATUS_NOT_SEND;
                    objMT.TIME_SEND_REQUEST = dateTimeNow;
                    objMT.USER_ID = objHistorySMS.MOBILE;
                    objMT.UNIT_ID = inputSMS.SchoolId;
                    objMT.SERVICE_ID = brandName;
                    if (objMT.USER_ID.isSupportPhone()) objMT.MT_TYPE = GlobalConstantsEdu.COMMON_HISTORYSMS_TO_TEACHER_ID;
                    MTBusiness.Insert(objMT);

                    //cap nhat lai so tin nhan da gui
                    if (sentRecord != null) sentRecord.SENT_COUNT += inputSMS.CountSMS;

                    // tru tien trong tai khoan
                    PriceSendSMSBO objPriceSMS = CalculateSMSPromotion(objHistorySMS, inputSMS.Promotion, inputSMS.CountSMS, recordReceived.TOTAL_RECEIVED_SMS, recordReceived.TOTAL_INTERNAL_SMS, recordReceived.TOTAL_EXTERNAL_SMS);
                    if (objPriceSMS != null)
                    {
                        priceSendSMS += objPriceSMS.PriceSendSMS;
                        InternalSendSMS += objPriceSMS.InternalSendSMS;
                        ExternalSendSMS += objPriceSMS.ExternalSendSMS;
                        recordReceived.TOTAL_RECEIVED_SMS = objPriceSMS.TotalReceivedSMS;
                        recordReceived.TOTAL_INTERNAL_SMS = objPriceSMS.TotalInternalSMS;
                        recordReceived.TOTAL_EXTERNAL_SMS = objPriceSMS.TotalExternalSMS;
                    }

                    //Cap nhat tin nha da nhan
                    if (recordReceived.SMS_RECEIVED_RECORD_ID > 0)
                    {
                        recordReceived.UPDATED_TIME = DateTime.Now;
                        ReceivedRecordBusiness.Update(recordReceived);
                    }

                    countNumOfSend++;
                    #endregion
                } // End loop for


                if (countNumOfSend == 0)
                {
                    resultBO.isError = false;
                    resultBO.NumSendSuccess = 0;
                    return resultBO;
                }

                if (sentRecord != null)
                {
                    //cap nhat lai sendTime
                    sentRecord.SENT_TIME++;
                    if (isInsertSentRecord)
                    {
                        SentRecordBusiness.Insert(sentRecord);
                    }
                    else
                    {
                        SentRecordBusiness.Update(sentRecord);
                    }
                }

                // AnhVD9 20141028 - Kiểm tra tài khoản
                EwalletResultBO ewalletRS = this.ComputeBalanceAccount(inputSMS.SchoolId, UserType.SchoolUser, priceSendSMS, InternalSendSMS, ExternalSendSMS);
                if (ewalletRS != null)
                {
                    if (ewalletRS.OK)
                    {
                        this.Save();
                        resultBO.NumSendSuccess = countNumOfSend;
                        // tru tien trong vi dien tu
                        Dictionary<string, object> dicEwallet = new Dictionary<string, object>();
                        dicEwallet["EWalletID"] = ewalletRS.EwalletID;
                        dicEwallet["Amount"] = ewalletRS.SubMainBalance;
                        dicEwallet["TransTypeID"] = GlobalConstantsEdu.TRANSACTION_TYPE_AMOUNT_SUB;
                        dicEwallet["ServiceType"] = GlobalConstantsEdu.SERVICE_TYPE_SENT_TO_TECHER;
                        dicEwallet["Quantity"] = ewalletRS.TotalSMSInMainBalance;
                        dicEwallet["PromotionAccountID"] = ewalletRS.PromotionAccountID;
                        dicEwallet["PromotionBalance"] = ewalletRS.SubPromBalance;
                        dicEwallet["InternalBalance"] = ewalletRS.SubInternalBalance;
                        dicEwallet["InternalSMS"] = ewalletRS.SubPromOfInSMS;
                        dicEwallet["ExternalSMS"] = ewalletRS.SubPromOfExSMS;
                        EWalletObject ewallet = EWalletBusiness.DeductMoneyAccount(dicEwallet);
                        resultBO.Balance = ewallet.Balance.ToString("#,##0");
                        resultBO.NumOfPromotionInternalSMS = ewallet.InternalSMS;
                        resultBO.TransAmount = ewalletRS.SubMainBalance;
                        resultBO.TotalSMSSentInMainBalance = ewalletRS.TotalSMSInMainBalance;
                        return resultBO;
                    }
                    else
                    {
                        resultBO.isError = true;
                        resultBO.ErrorMsg = ewalletRS.ErrorMsg;
                        return resultBO;
                    }
                }
                else
                {
                    resultBO.isError = true;
                    resultBO.ErrorMsg = ResEdu.Get("SendSMS_Label_CoreError");
                    return resultBO;
                }

            }
            catch (Exception ex)
            {
                string para = string.Format("academicYearId={0}, schoolId={1}", inputSMS.AcademicYearID, inputSMS.SchoolId);
                LogExtensions.ErrorExt(logger, DateTime.Now, "SendSmstoTeacher", para, ex);
                resultBO.isError = true;
                resultBO.ErrorMsg = ResEdu.Get("SendSMS_Label_CoreError");
                return resultBO;
            }
            finally
            {
                SetAutoDetectChangesEnabled(true);
            }
        }
        public List<CustomSMSBO> GetCustomSMS(List<int> pupilIDList, int schoolID, int academicYearID, List<int> lstClassID, int grade, string template, int periodType, bool forVNEN, List<string> lstContentTypeCode, int? nameDisplay,
            int? semester = null, int? month = null, int? year = null, DateTime? fromDate = null, DateTime? toDate = null)
        {
            try
            {
                //list hoc sinh trong lop
                List<PupilOfClassBO> lstPupilOfClass = (from poc in PupilOfClassBusiness.AllNoTracking
                                                        join pp in PupilProfileBusiness.AllNoTracking.OrderBy(x => x.PupilProfileID) on poc.PupilID equals pp.PupilProfileID
                                                        where poc.SchoolID == schoolID && poc.AcademicYearID == academicYearID
                                                        && lstClassID.Contains(poc.ClassID)
                                                        && pupilIDList.Contains(poc.PupilID)
                                                        && pp.IsActive
                                                        select new PupilOfClassBO
                                                        {
                                                            PupilOfClassID = poc.PupilOfClassID,
                                                            SchoolID = poc.SchoolID,
                                                            AcademicYearID = poc.AcademicYearID,
                                                            ClassID = poc.ClassID,
                                                            PupilID = poc.PupilID,
                                                            PupilCode = pp.PupilCode,
                                                            PupilFullName = pp.FullName,
                                                            Name = pp.Name,
                                                            OrderInClass = poc.OrderInClass,
                                                            Status = poc.Status,
                                                            EndDate = poc.EndDate,
                                                            Birthday = pp.BirthDate,
                                                            BirthPlace = pp.BirthPlace,
                                                            ResidentalAddress = pp.TempResidentalAddress,
                                                            PermanentResidentalAddress = pp.PermanentResidentalAddress,
                                                            EducationLevelID = poc.ClassProfile.EducationLevelID,
                                                            EthnicName = pp.Ethnic.EthnicName,
                                                            Genre = pp.Genre,
                                                            FatherFullName = pp.FatherFullName,
                                                            FatherJobName = pp.FatherJob,
                                                            MotherFullName = pp.MotherFullName,
                                                            MotherJobName = pp.MotherJob,
                                                            SponserFullName = pp.SponsorFullName,
                                                            SponserJobName = pp.SponsorJob,
                                                            ClassName = poc.ClassProfile.DisplayName,
                                                            FatherMobile = pp.FatherMobile,
                                                            FatherEmail = pp.FatherEmail,
                                                            MotherMobile = pp.MotherMobile,
                                                            MotherEmail = pp.MotherEmail,
                                                            SponserMobile = pp.SponsorMobile,
                                                            SponserEmail = pp.SponsorEmail,
                                                            HomeTown = pp.HomeTown,
                                                            EnrolmentDate = pp.EnrolmentDate,
                                                            StorageNumber = pp.StorageNumber
                                                        }).OrderBy(c => c.OrderInClass)
                                                         .ThenBy(c => c.Name)
                                                         .ThenBy(c => c.PupilFullName).ToList();


                List<CustomSMSBO> lstData = new List<CustomSMSBO>();
                CustomSMSBO objData;
                int classID = 0;
                //Lay hoc ky da chon
                int bigSemester = 0;
                if (semester == 1 || semester == 3)
                {
                    bigSemester = 1;
                }
                else if (semester == 2 || semester == 4)
                {
                    bigSemester = 2;
                }
                else if (semester == 5)
                {
                    bigSemester = 3;
                }

                //lay hoc ky hien tai
                int curSemester = 0;
                DateTime dateTimeNow = DateTime.Now;
                AcademicYear objAcademicYear = (from a in AcademicYearBusiness.All
                                                where a.SchoolID == schoolID
                                                && a.AcademicYearID == academicYearID
                                                select a).FirstOrDefault();

                if (objAcademicYear != null)
                {
                    if (objAcademicYear.FirstSemesterStartDate.Value.Date <= dateTimeNow && objAcademicYear.SecondSemesterStartDate.Value.Date.AddDays(-1).AddHours(23).AddMinutes(59).AddSeconds(59).AddMilliseconds(59) >= dateTimeNow)
                    {
                        curSemester = 1;
                    }
                    // Anhvd9 - Cho phep nhan tin toi 31.8 neu nam hoc cu da ket thuc
                    if (objAcademicYear.SecondSemesterStartDate.Value.Date <= dateTimeNow && new DateTime(objAcademicYear.SecondSemesterEndDate.Value.Year, 8, 31, 23, 59, 59) >= dateTimeNow)
                    {
                        curSemester = 2;
                    }
                }

                DateTime startTime = default(DateTime);
                DateTime endTime = default(DateTime);
                if (periodType == GlobalConstants.PERIOD_TYPE_SEMESTER)
                {

                    DateTime FromDateHKI = DateTime.Now;
                    DateTime ToDateHKI = DateTime.Now;
                    DateTime FromDateHKII = DateTime.Now;
                    DateTime ToDateHKII = DateTime.Now;
                    if (objAcademicYear != null)
                    {
                        FromDateHKI = objAcademicYear.FirstSemesterStartDate.Value.Date;
                        ToDateHKI = objAcademicYear.FirstSemesterEndDate.Value.Date.AddHours(23).AddMinutes(59).AddSeconds(59);
                        FromDateHKII = objAcademicYear.SecondSemesterStartDate.Value.Date;
                        ToDateHKII = objAcademicYear.SecondSemesterEndDate.Value.Date.AddHours(23).AddMinutes(59).AddSeconds(59);
                    }

                    if (bigSemester == 1)
                    {
                        startTime = FromDateHKI;
                        endTime = ToDateHKI;
                    }
                    else if (bigSemester == 2)
                    {
                        startTime = FromDateHKII;
                        endTime = ToDateHKII;
                    }
                    else if (bigSemester == 3)
                    {
                        startTime = FromDateHKI;
                        endTime = ToDateHKII;
                    }
                }
                else
                {
                    if (periodType == GlobalConstants.PERIOD_TYPE_DAILY)
                    {
                        startTime = DateTime.Now.Date;
                        endTime = DateTime.Now.Date;
                    }
                    else if (periodType == GlobalConstants.PERIOD_TYPE_WEEKLY)
                    {
                        startTime = DateTime.Now.AddDays(-6).Date;
                        endTime = DateTime.Now.Date;
                    }
                    else if (periodType == GlobalConstants.PERIOD_TYPE_MONTHLY)
                    {
                        if (!forVNEN)
                        {
                            startTime = DateTime.Now.AddMonths(-1).AddDays(1).Date;
                            endTime = DateTime.Now.Date;
                        }
                        else
                        {
                            startTime = new DateTime(year.Value, month.Value, 1);
                            endTime = new DateTime(year.Value, month.Value, DateTime.DaysInMonth(year.Value, month.Value));
                            if (DateTime.Compare(endTime, DateTime.Now.Date) >= 0)
                            {
                                endTime = DateTime.Now.Date;
                            }
                        }
                    }
                    else if (periodType == GlobalConstants.PERIOD_TYPE_CUSTOM)
                    {
                        startTime = fromDate.Value;
                        endTime = toDate.Value;
                    }
                }

                IDictionary<string, object> dic = new Dictionary<string, object>();
                #region thoi gian dinh ky
                //Thoi gian dinh ky
                string TGDK = string.Empty;
                switch (periodType)
                {
                    case GlobalConstants.PERIOD_TYPE_DAILY:
                        TGDK = string.Format("ngày {0}", DateTime.Now.Date.ToString("dd/MM/yyyy"));
                        break;
                    case GlobalConstants.PERIOD_TYPE_WEEKLY:
                        TGDK = string.Format("từ {0} đến {1}", startTime.ToString("dd/MM/yyyy"), endTime.ToString("dd/MM/yyyy"));
                        break;
                    case GlobalConstants.PERIOD_TYPE_MONTHLY:

                        TGDK = string.Format("từ {0} đến {1}", startTime.ToString("dd/MM/yyyy"), endTime.ToString("dd/MM/yyyy"));
                        break;
                    case GlobalConstants.PERIOD_TYPE_SEMESTER:
                        TGDK = semester == 1 ? "học kỳ 1" : (semester == 2 ? "học kỳ 2" : (semester == 3 ? "giữa kỳ 1" : (semester == 4 ? "giữa kỳ 2" : (semester == 5 ? "cả năm" : string.Empty))));
                        break;
                    case GlobalConstants.PERIOD_TYPE_CUSTOM:
                        TGDK = string.Format("từ {0} đến {1}", startTime.ToString("dd/MM/yyyy"), endTime.ToString("dd/MM/yyyy"));
                        break;
                }

                dic["TGDK"] = TGDK;
                #endregion

                #region Diem danh
                if (lstContentTypeCode.Contains("DD"))
                {
                    List<CustomSMSBO> listPupilTraining = new List<CustomSMSBO>();
                    int partitionId = schoolID % 100;


                    // thong tin diem danh cua hoc sinh
                    var lstPupilAbsent = (from p in PupilAbsenceBusiness.All
                                          where lstClassID.Contains(p.ClassID)
                                          && p.SchoolID == schoolID
                                          && p.AcademicYearID == academicYearID
                                          && p.Last2digitNumberSchool == partitionId
                                          && p.AbsentDate >= startTime
                                          && p.AbsentDate <= endTime
                                          select p).ToList();


                    List<int> pupilIds = new List<int>();
                    if (lstPupilAbsent != null && lstPupilAbsent.Count > 0)
                    {
                        pupilIds = lstPupilAbsent.Select(p => p.PupilID).Distinct().ToList();
                    }

                    List<PupilAbsence> itemAbsences = null;
                    //Tao content cho object
                    int pupilID = 0;
                    string content = string.Empty;
                    int cp = 0, kp = 0;
                    CustomSMSBO item = null;
                    for (int i = pupilIds.Count - 1; i >= 0; i--)
                    {
                        content = string.Empty;
                        item = new CustomSMSBO();
                        pupilID = pupilIds[i];
                        // diem danh
                        itemAbsences = lstPupilAbsent.Where(o => o.PupilID == pupilID).ToList();
                        if (itemAbsences != null && itemAbsences.Count > 0)
                        {
                            cp = itemAbsences.Where(p => p.IsAccepted == true).Count();
                            kp = itemAbsences.Where(p => p.IsAccepted != true).Count();
                            if (cp > 0)
                            {
                                content += string.Format(GlobalConstantsEdu.FORMAT_0_1, string.Format("Nghỉ học có phép: {0}", cp), Environment.NewLine);
                            }

                            if (kp > 0)
                            {
                                content += string.Format(GlobalConstantsEdu.FORMAT_0_1, string.Format("Nghỉ học không phép: {0}", kp), Environment.NewLine);
                            }
                        }

                        item.PupilID = pupilID;
                        item.SMSContent = content;
                        listPupilTraining.Add(item);
                    }

                    dic["DD"] = listPupilTraining;
                }

                #endregion

                #region Vi pham
                if (lstContentTypeCode.Contains("VP"))
                {
                    List<CustomSMSBO> listPupilViolate = new List<CustomSMSBO>();

                    // thong tin vi pham cua hoc sinh
                    var lstFault = (from f in PupilFaultBusiness.AllNoTracking
                                    join lf in FaultCriteriaBusiness.AllNoTracking on f.FaultID equals lf.FaultCriteriaID
                                    join fg in FaultGroupBusiness.AllNoTracking on lf.GroupID equals fg.FaultGroupID
                                    where f.SchoolID == schoolID
                                    && f.AcademicYearID == academicYearID
                                    && EntityFunctions.TruncateTime(f.ViolatedDate) >= EntityFunctions.TruncateTime(startTime)
                                    && EntityFunctions.TruncateTime(f.ViolatedDate) <= EntityFunctions.TruncateTime(endTime)
                                    select new
                                    {
                                        PupilID = f.PupilID,
                                        FaultID = f.FaultID,
                                        NumberOfFault = f.NumberOfFault,
                                        Resolution = lf.Resolution,
                                        Note = f.Note
                                    }).ToList();

                    List<int> pupilIds = new List<int>();

                    if (lstFault != null && lstFault.Count > 0)
                    {
                        pupilIds.AddRange(lstFault.Select(p => p.PupilID).Distinct().ToList());
                        pupilIds = pupilIds.Distinct().ToList();
                    }

                    //Tao content cho object
                    int pupilID = 0;
                    string content = string.Empty;
                    CustomSMSBO item = null;
                    for (int i = pupilIds.Count - 1; i >= 0; i--)
                    {
                        content = string.Empty;
                        item = new CustomSMSBO();
                        pupilID = pupilIds[i];

                        //Vi pham
                        var itemFaults = lstFault.Where(a => a.PupilID == pupilID).GroupBy(l => new { l.FaultID, l.Resolution })
                            .Select(g => new
                            {
                                FaultID = g.Key.FaultID,
                                Resolution = g.Key.Resolution,
                                Note = string.Join(", ", g.Where(o => !string.IsNullOrWhiteSpace(o.Note))
                                    .Select(x => x.Note).Distinct()),
                                Number = g.Sum(x => x.NumberOfFault)
                            }).ToList();


                        if (itemFaults != null && itemFaults.Count > 0)
                        {
                            content = "Vi phạm: ";
                            for (int ik = 0, iksize = itemFaults.Count; ik < iksize; ik++)
                            {
                                var itemFaulst = itemFaults[ik];
                                if (itemFaulst.Number > 1)
                                {
                                    content += string.Format("{0}{1} ({2}), ", string.IsNullOrEmpty(itemFaulst.Resolution) ? string.Empty : itemFaulst.Resolution, string.IsNullOrEmpty(itemFaulst.Note) ? string.Empty : " " + itemFaulst.Note, itemFaulst.Number);
                                }
                                else
                                {
                                    content += string.Format("{0}{1}, ", string.IsNullOrEmpty(itemFaulst.Resolution) ? string.Empty : itemFaulst.Resolution, string.IsNullOrEmpty(itemFaulst.Note) ? string.Empty : " " + itemFaulst.Note);
                                }
                            }

                            // cat bo dau phay
                            content = content.Substring(0, content.Length - 2);
                        }

                        item.PupilID = pupilID;
                        item.SMSContent = content;
                        listPupilViolate.Add(item);
                    }

                    dic["VP"] = listPupilViolate;
                }
                #endregion

                #region Cap 2, 3 khong VNEN
                if ((grade == GlobalConstants.APPLIED_LEVEL_SECONDARY || grade == GlobalConstants.APPLIED_LEVEL_TERTIARY) && forVNEN == false)
                {
                    #region Diem
                    if (lstContentTypeCode.Contains("DIEM"))
                    {

                        //lay len diem chi tiet cac mon theo datetime
                        IDictionary<string, object> tmpDic = new Dictionary<string, object>();
                        tmpDic["SchoolID"] = schoolID;
                        tmpDic["lstClassID"] = lstClassID;
                        tmpDic["AcademicYearID"] = academicYearID;
                        tmpDic["Semester"] = curSemester;
                        //Lay len diem cua lop
                        IQueryable<MarkRecordWSBO> IQMarkRecord = MarkRecordBusiness.GetMardRecordOfAllSubjectInClass(tmpDic);
                        //Lay diem nhan xet cua lop
                        IQueryable<JudgeRecordWSBO> IQJudgeRecord = JudgeRecordBusiness.GetAllJudgeRecordOfClass(tmpDic);

                        //Loc thoi gian cham diem mon tinh diem
                        //Lay lai thoi gian bat dau va ket thuc cho chinh xac
                        DateTime fromDateMark = new DateTime(startTime.Year, startTime.Month, startTime.Day);
                        DateTime toDateMark = new DateTime(endTime.Year, endTime.Month, endTime.Day).AddDays(1);
                        bool isPrimary = false;
                        var IQFilterMarkRecordTmp = (from m in IQMarkRecord
                                                     where m.MarkedDate >= fromDateMark
                                                     && m.MarkedDate < toDateMark
                                                     select new
                                                     {
                                                         m.PupilID,
                                                         m.SubjectName,
                                                         m.SubjectOrderNumber,
                                                         m.MarkOrderNumber,
                                                         m.Mark,
                                                         m.Title,
                                                         m.MarkTypeID,
                                                         m.MarkedDate,
                                                         m.MarkredDatePrimary
                                                     }).ToList();

                        var IQFilterMarkRecord = (from m in IQFilterMarkRecordTmp
                                                  where (isPrimary && !m.Title.Contains("VGK") && !m.Title.Contains("ĐGK")
                                                                   && !m.Title.Contains("VCK") && !m.Title.Contains("ĐCK")
                                                                   && !m.Title.Contains("VCN") && !m.Title.Contains("ĐCN"))
                                                  || (isPrimary == false)
                                                  select new
                                                  {
                                                      m.PupilID,
                                                      m.SubjectName,
                                                      m.SubjectOrderNumber,
                                                      m.MarkOrderNumber,
                                                      Mark = m.Mark.HasValue ? m.Mark.Value.ToString("0.#").Replace(",",".") : string.Empty,
                                                      Title = !isPrimary && m.Title.Contains("M") ? "M" : !isPrimary && m.Title.Contains("P") ? "15P" : !isPrimary && m.Title.Contains("V")
                                                                ? "1T" : !isPrimary && m.Title.Contains("HK") ? "KTHK" : isPrimary && m.Title.Contains("ĐTX") && m.MarkredDatePrimary.HasValue
                                                                ? m.Title + (SetMonthWithPrimary(m.MarkredDatePrimary.Value.Month))
                                                                : isPrimary && m.Title.Contains("GK") ? "GK" : isPrimary && (m.Title.Contains("CK") || m.Title.Contains("CN")) ? "CK" : m.Title + " ",
                                                      m.MarkTypeID
                                                  }).ToList();

                        //Loc thoi gian cham diem mon nhan xet
                        var LsFilterJudgeRecordTmp = (from j in IQJudgeRecord
                                                      where j.MarkedDate >= fromDateMark
                                                      && j.MarkedDate < toDateMark
                                                      select new
                                                      {
                                                          j.PupilID,
                                                          j.SubjectID,
                                                          j.SubjectName,
                                                          j.SubjectOrderNumber,
                                                          j.MarkOrderNumber,
                                                          Judgement = j.Judgement == null ? string.Empty : j.Judgement,
                                                          j.MarkTypeID,
                                                          Title = j.Title,
                                                          EducationLevelID = j.EducationLevelID
                                                      }).ToList();

                        var LsFilterJudgeRecord = (from j in LsFilterJudgeRecordTmp
                                                   select new
                                                   {
                                                       j.PupilID,
                                                       j.SubjectID,
                                                       j.SubjectName,
                                                       j.SubjectOrderNumber,
                                                       j.MarkOrderNumber,
                                                       Judgement = j.Judgement == null ? string.Empty : j.Judgement,
                                                       j.MarkTypeID,
                                                       Title = j.Title.Contains("P") ? "15P" : j.Title.Contains("V") ? "1T" : j.Title.Contains("M") ? "M" : isPrimary ? "NX" + (curSemester == 2 ? (j.EducationLevelID > 2 ? j.MarkOrderNumber + 5 : j.MarkOrderNumber + 4) : j.MarkOrderNumber) : (!isPrimary && j.Title.Contains("HK") ? "KTHK" : j.Title),
                                                   }).ToList();
                        //Loc nhung hoc sinh co diem mon tin diem phat sinh
                        var IQMarkRecordGroup = (from p in IQFilterMarkRecord
                                                 orderby p.MarkTypeID, p.SubjectOrderNumber, p.SubjectName, p.MarkOrderNumber
                                                 group p by new
                                                 {
                                                     p.PupilID,
                                                     p.SubjectName,
                                                     p.SubjectOrderNumber,
                                                     p.Title
                                                 } into g
                                                 select new
                                                 {
                                                     PupilID = g.Key.PupilID.Value,
                                                     SubjectName = g.Key.SubjectName,
                                                     SubjectOrderNumber = g.Key.SubjectOrderNumber,
                                                     OrderPrimaryID = SetOrderPrimary(isPrimary, g.Key.Title),
                                                     MarkContent = (isPrimary && g.Key.Title.Contains("ĐTX")) ? (g.Key.Title.Replace("ĐTX", "T") + ": " + g.Select(x => x.Mark).Aggregate((a, b) => (a + " " + b)))
                                                            : isPrimary && (g.Key.Title.Contains("GK") || g.Key.Title.Contains("CK")) ? (g.Key.Title + ": " + g.Select(x => x.Mark).Aggregate((a, b) => (a + ", " + b))) : (g.Key.Title + ": " + g.Select(x => x.Mark).Aggregate((a, b) => (a + " " + b)))
                                                 }).ToList();
                        //Nhom diem cua cac mon tinh diem lai
                        var IQMarkRecordResponse = (from mg in IQMarkRecordGroup
                                                    orderby mg.SubjectOrderNumber, mg.SubjectName, mg.OrderPrimaryID
                                                    group mg
                                                    by new
                                                    {
                                                        mg.PupilID,
                                                        mg.SubjectName
                                                    }
                                                        into g
                                                        select new CustomSMSBO
                                                        {
                                                            PupilID = g.Key.PupilID,
                                                            SMSContent = "-" + g.Key.SubjectName + "\n" + g.Select(x => x.MarkContent).Aggregate((a, b) => (a + "\n" + b)) + "\n"
                                                        }).ToList();


                        //Loc nhung hoc sinh co diem  mon nhan xet phat sinh
                        var IQMarkJudgeGroup = (from p in LsFilterJudgeRecord
                                                orderby p.MarkTypeID, p.MarkOrderNumber
                                                group p by new
                                                {
                                                    p.PupilID,
                                                    p.SubjectName,
                                                    p.SubjectID,
                                                    p.SubjectOrderNumber,
                                                    Title = p.Title
                                                } into g
                                                select new
                                                {
                                                    PupilID = g.Key.PupilID.Value,
                                                    SubjectName = g.Key.SubjectName,
                                                    SubjectID = g.Key.SubjectID,
                                                    SubjectOrderNumber = g.Key.SubjectOrderNumber,
                                                    JudgementContent = g.Key.Title
                                                        + ": " + g.Select(x => (x.Judgement.ToString())).Aggregate((a, b) => (a + " " + b))

                                                }).ToList();

                        //Nhom diem cua cac mon nhan xet lai
                        var IQMarkJudgeResponse = (from mg in IQMarkJudgeGroup
                                                   orderby mg.SubjectOrderNumber, mg.SubjectName
                                                   group mg
                                                    by new
                                                    {
                                                        mg.PupilID,
                                                        mg.SubjectName,
                                                        mg.SubjectID
                                                    }
                                                       into g
                                                       select new CustomSMSBO
                                                       {
                                                           PupilID = g.Key.PupilID,
                                                           SMSContent = "-" + g.Key.SubjectName + "\n"
                                                                    + g.Select(x => x.JudgementContent).Aggregate((a, b) => (a + "\n" + b)) + "\n"
                                                       }).ToList();
                        // loc hoc sinh trung nhau
                        IQMarkJudgeResponse = (from p in IQMarkJudgeResponse
                                               group p by new { p.PupilID }
                                                   into g
                                                   select new CustomSMSBO
                                                   {
                                                       PupilID = g.Key.PupilID,
                                                       SMSContent = g.Where(p => p.PupilID == g.Key.PupilID).Select(p => p.SMSContent).Aggregate((a, b) => (a + b))
                                                   }).ToList();

                        IQMarkRecordResponse = (from p in IQMarkRecordResponse
                                                group p by new { p.PupilID }
                                                    into g
                                                    select new CustomSMSBO
                                                    {
                                                        PupilID = g.Key.PupilID,
                                                        SMSContent = g.Where(p => p.PupilID == g.Key.PupilID).Select(p => p.SMSContent).Aggregate((a, b) => (a + b))
                                                    }).ToList();

                        // Bo sung them mon
                        foreach (var item in IQMarkJudgeResponse)
                        {
                            var t = IQMarkRecordResponse.FirstOrDefault(o => o.PupilID == item.PupilID);
                            if (t != null)
                            {
                                t.SMSContent += item.SMSContent;
                                t.SMSContent = t.SMSContent.Substring(0, t.SMSContent.Length - 1);
                            }
                            else
                            {
                                IQMarkRecordResponse.Add(item);
                            }
                        }

                        dic["DIEM"] = IQMarkRecordResponse;
                    }


                    #endregion

                    #region Trung binh mon
                    if (lstContentTypeCode.Contains("TBM"))
                    {
                        IDictionary<string,object> dicSearchCS = new Dictionary<string, object>() 
                        {
                            {"SchoolID",schoolID},
                            {"lstClassID",lstClassID},
                            { "Semester", bigSemester } 
                        };
                        List<ClassSubjectBO> listClassSubject = ClassSubjectBusiness.Search(dicSearchCS).Select(o => new ClassSubjectBO
                        {
                            ClassID = o.ClassID,
                            SubjectID = o.SubjectID,
                            DisplayName = o.SubjectCat.DisplayName,
                            IsCommenting = o.IsCommenting,
                            OrderInSubject = o.SubjectCat.OrderInSubject
                        }).OrderBy(o => o.IsCommenting).ThenBy(o => o.OrderInSubject).ThenBy(o => o.DisplayName).ToList();

                        //lay diem chi tiet trung binh cua cac mon
                        List<SummedUpRecordBO> listSummedUpRecord = SummedUpRecordBusiness.GetSummedUpRecordByListClassID(lstClassID, academicYearID,schoolID, bigSemester, null).ToList();
                        //lay diem trung binh cua tat ca cac mon theo dot + lay hoc luc, hanh kiem, xep hang theo dot
                        List<PupilOfClassRanking> listPupilRanking = PupilRankingBusiness.GetPupilRankingByClassID(lstClassID,academicYearID, schoolID, bigSemester, null).ToList();

                        List<CustomSMSBO> lstMarkTBM = new List<CustomSMSBO>();

                        List<SummedUpRecordBO> listPupilSum = new List<SummedUpRecordBO>();
                        //Object mapping vao list
                        CustomSMSBO objMarkOfSemester = new CustomSMSBO();
                        //object mapping thong tin diem trung binh mon (dung de tao content)
                        SummedUpRecordBO objSummedUpRecordBO = new SummedUpRecordBO();
                        List<ClassSubjectBO> lstCStmp = null;
                        foreach (PupilOfClassRanking pupilRank in listPupilRanking)
                        {
                            int pupilID = pupilRank.PupilID;
                            // Khoi tao item
                            objMarkOfSemester = new CustomSMSBO();
                            // Lay len thong tin diem cua hoc sinh
                            listPupilSum = listSummedUpRecord.Where(o => o.PupilID == pupilID && o.ClassID == pupilRank.ClassID).ToList();

                            // Lay thong tin hoc luc hanh kiem
                            string pupilInfo = "";
                            lstCStmp = listClassSubject.Where(p => p.ClassID == pupilRank.ClassID).ToList();
                            int countSubject = lstCStmp.Count;
                            

                            // Thong diem diem theo mon
                            foreach (ClassSubjectBO cs in lstCStmp)
                            {
                                objSummedUpRecordBO = listPupilSum.Find(o => o.SubjectID == cs.SubjectID);
                                if (objSummedUpRecordBO != null)
                                {
                                    if (!objSummedUpRecordBO.SummedUpMark.HasValue && string.IsNullOrEmpty(objSummedUpRecordBO.JudgementResult))
                                    {
                                        continue;
                                    }
                                    pupilInfo += cs.DisplayName + ": " + (cs.IsCommenting.Value == SystemParamsInFile.ISCOMMENTING_TYPE_JUDGE ? objSummedUpRecordBO.JudgementResult + "\n" : (objSummedUpRecordBO.SummedUpMark.HasValue ? (grade > 1 ? (objSummedUpRecordBO.SummedUpMark.Value == 0 ? "0" : objSummedUpRecordBO.SummedUpMark.Value == 10 ? "10" : objSummedUpRecordBO.SummedUpMark.Value.ToString("0.0").Replace(",",".")) : objSummedUpRecordBO.SummedUpMark.Value.ToString("0.#").Replace(",",".")) : string.Empty)) + (grade > 1 && objSummedUpRecordBO.IsCommenting == SystemParamsInFile.ISCOMMENTING_TYPE_MARK ? "\n" : "");

                                }
                            }
                            if (pupilInfo.EndsWith("\n"))
                            {
                                pupilInfo = pupilInfo.Substring(0, pupilInfo.Length - 1);
                            }

                            // gan thong tin chi tiet object de add
                            objMarkOfSemester.SMSContent = pupilInfo;
                            objMarkOfSemester.PupilID = pupilID;
                            // Gan vao list de tra ve
                            lstMarkTBM.Add(objMarkOfSemester);
                        }

                        dic["TBM"] = lstMarkTBM;
                    }
                    #endregion

                    #region Trung binh cac mon, hoc luc, hanh kiem, danh hieu thi dua
                    if (lstContentTypeCode.Contains("TBCM") || lstContentTypeCode.Contains("HL") || lstContentTypeCode.Contains("HK") || lstContentTypeCode.Contains("XH") || lstContentTypeCode.Contains("DHTD"))
                    {
                        //lay diem trung binh cua tat ca cac mon theo dot + lay hoc luc, hanh kiem, xep hang theo dot
                        List<PupilOfClassRanking> listPupilRanking = PupilRankingBusiness.GetPupilRankingByClassID(lstClassID,academicYearID, schoolID, bigSemester,null).ToList();

                        //lay danh hieu thi dua
                        List<PupilEmulationBO> listPupilEmulation = PupilEmulationBusiness.SearchBySchool(schoolID, new Dictionary<string, object>() {
                            { "AcademicYearID", academicYearID } ,
                            { "lstClassID", lstClassID },
                            { "Semester", bigSemester }
                            }).Select(o => new PupilEmulationBO
                            {
                                ClassID = o.ClassID,
                                PupilID = o.PupilID,
                                HonourAchivementTypeResolution = o.HonourAchivementType.Resolution
                            }).ToList();

                        List<CustomSMSBO> listTBCM = new List<CustomSMSBO>();
                        List<CustomSMSBO> listHL = new List<CustomSMSBO>();
                        List<CustomSMSBO> listHK = new List<CustomSMSBO>();
                        List<CustomSMSBO> listXH = new List<CustomSMSBO>();
                        List<CustomSMSBO> listDHTH = new List<CustomSMSBO>();

                        //Object mapping vao list
                        CustomSMSBO objTBCM = new CustomSMSBO();
                        CustomSMSBO objHL = new CustomSMSBO();
                        CustomSMSBO objHK = new CustomSMSBO();
                        CustomSMSBO objXH = new CustomSMSBO();
                        CustomSMSBO objDHTD = new CustomSMSBO();

                        // Thong tin khen thuong
                        PupilEmulationBO pupilEmulationBO = new PupilEmulationBO();

                        foreach (PupilOfClassRanking pupilRank in listPupilRanking)
                        {
                            int pupilID = pupilRank.PupilID;

                            // Lay len thon tin khen thuong
                            pupilEmulationBO = listPupilEmulation.Find(o => o.PupilID == pupilID && o.ClassID == pupilRank.ClassID);

                            // Khoi tao item
                            objTBCM = new CustomSMSBO();
                            objHL = new CustomSMSBO();
                            objHK = new CustomSMSBO();
                            objXH = new CustomSMSBO();
                            objDHTD = new CustomSMSBO();

                            //TBM
                            string pupilInfo = string.Empty;

                            if (pupilRank.AverageMark.HasValue && pupilRank.AverageMark.Value > 0)
                            {
                                pupilInfo += "\nTrung bình các môn: " + (pupilRank.AverageMark.Value == 0 ? "0" : pupilRank.AverageMark.Value == 10 ? "10" : pupilRank.AverageMark.Value.ToString("0.0").Replace(",","."));
                            }

                            // gan thong tin chi tiet object de add
                            objTBCM.SMSContent = pupilInfo;
                            objTBCM.PupilID = pupilID;
                            // Gan vao list de tra ve
                            listTBCM.Add(objTBCM);

                            //Hoc luc
                            pupilInfo = string.Empty;

                            if (!string.IsNullOrEmpty(pupilRank.CapacityLevel))
                            {
                                pupilInfo += "\nHọc lực: " + pupilRank.CapacityLevel;
                            }

                            // gan thong tin chi tiet object de add
                            objHL.SMSContent = pupilInfo;
                            objHL.PupilID = pupilID;
                            // Gan vao list de tra ve
                            listHL.Add(objHL);

                            //Hanh kiem
                            pupilInfo = string.Empty;

                            if (!string.IsNullOrEmpty(pupilRank.ConductLevel))
                            {
                                pupilInfo += "\nHạnh kiểm: " + pupilRank.ConductLevel;
                            }

                            // gan thong tin chi tiet object de add
                            objHK.SMSContent = pupilInfo;
                            objHK.PupilID = pupilID;
                            // Gan vao list de tra ve
                            listHK.Add(objHK);

                            //Xep hang
                            pupilInfo = string.Empty;

                            if (pupilRank.Rank.HasValue && pupilRank.Rank.Value > 0)
                            {
                                pupilInfo += "\nXếp hạng: " + pupilRank.Rank.ToString();
                            }

                            // gan thong tin chi tiet object de add
                            objXH.SMSContent = pupilInfo;
                            objXH.PupilID = pupilID;
                            // Gan vao list de tra ve
                            listXH.Add(objXH);

                            //Danh hieu thi dua
                            pupilInfo = string.Empty;

                            if (pupilEmulationBO != null && !string.IsNullOrEmpty(pupilEmulationBO.HonourAchivementTypeResolution))
                            {
                                pupilInfo += "\nDanh hiệu thi đua: " + pupilEmulationBO.HonourAchivementTypeResolution;
                            }

                            // gan thong tin chi tiet object de add
                            objDHTD.SMSContent = pupilInfo;
                            objDHTD.PupilID = pupilID;
                            // Gan vao list de tra ve
                            listDHTH.Add(objDHTD);

                        }

                        dic["TBCM"] = listTBCM;
                        dic["HL"] = listHL;
                        dic["HK"] = listHK;
                        dic["XH"] = listXH;
                        dic["DHTD"] = listDHTH;
                    }
                    #endregion
                }
                #endregion

                #region Cap 2 VNEN
                if (grade == GlobalConstants.APPLIED_LEVEL_SECONDARY && forVNEN == true)
                {
                    AcademicYear academicYear = AcademicYearBusiness.Find(academicYearID);
                    int partitionId = UtilsBusiness.GetPartionId(schoolID);
                    int semesterVNEN = bigSemester == 3 ? 2 : bigSemester;

                    List<CustomSMSBO> listNXT = new List<CustomSMSBO>();

                    CustomSMSBO obj = null;
                    PupilOfClassBO pupilOfClassBO = null;
                    int pupilID = 0;
                    StringBuilder monthComments = null, pupilContent = null;
                    string MonthStr = string.Empty;

                    //nhan xet thang

                    MonthStr = string.Format("{0}{1}", year, month);

                    int v_monthID = 0;
                    if (Int32.TryParse(MonthStr, out v_monthID))
                    {
                        List<TeacherNoteBookMonthBO> listNotesMonth = (from note in TeacherNoteBookMonthBusiness.All
                                                                       join sc in SubjectCatBusiness.All on note.SubjectID equals sc.SubjectCatID
                                                                       where note.SchoolID == schoolID && note.PartitionID == partitionId
                                                                           && note.AcademicYearID == academicYearID && lstClassID.Contains(note.ClassID)
                                                                           && note.MonthID == v_monthID
                                                                            && pupilIDList.Contains(note.PupilID)
                                                                       orderby sc.OrderInSubject
                                                                       select new TeacherNoteBookMonthBO
                                                                       {
                                                                           PupilID = note.PupilID,
                                                                           ClassID = note.ClassID,
                                                                           SubjectID = note.SubjectID,
                                                                           SubjectName = sc.SubjectName,
                                                                           MonthID = note.MonthID,
                                                                           CommentCQ = note.CommentCQ,
                                                                           CommentSubject = note.CommentSubject
                                                                       }).ToList();
                        TeacherNoteBookMonthBO noteSubjectMonthBO = null;
                        for (int i = 0; i < lstPupilOfClass.Count; i++)
                        {
                            pupilOfClassBO = lstPupilOfClass[i];
                            pupilID = pupilOfClassBO.PupilID;
                            obj = new CustomSMSBO();
                            obj.PupilID = pupilID;
                            monthComments = new StringBuilder();

                            List<TeacherNoteBookMonthBO> listCommentsBypupilID = listNotesMonth.Where(p => p.PupilID == pupilID && p.ClassID == pupilOfClassBO.ClassID).ToList();
                            for (int j = 0; j < listCommentsBypupilID.Count; j++)
                            {
                                noteSubjectMonthBO = listCommentsBypupilID[j];
                                if (!string.IsNullOrEmpty(noteSubjectMonthBO.CommentSubject) || !string.IsNullOrEmpty(noteSubjectMonthBO.CommentCQ))
                                {
                                    monthComments.Append(j + 1).Append("/").Append(noteSubjectMonthBO.SubjectName).Append(":\r\n");
                                    if (!string.IsNullOrEmpty(noteSubjectMonthBO.CommentSubject)) monthComments.AppendLine(noteSubjectMonthBO.CommentSubject);
                                    if (!string.IsNullOrEmpty(noteSubjectMonthBO.CommentCQ)) monthComments.AppendLine(noteSubjectMonthBO.CommentCQ);
                                }

                            }
                            if (monthComments != null && monthComments.Length > 0)
                            {
                                obj.SMSContent = monthComments.ToString();
                            }
                            else
                            {
                                obj.SMSContent = string.Empty;
                            }
                            listNXT.Add(obj);
                        }

                        dic["NXT"] = listNXT;

                    }

                    //Danh gia mon hoc
                    List<SubjectCatBO> lsClassSubject = new List<SubjectCatBO>();

                    lsClassSubject = (from sb in ClassSubjectBusiness.All
                                      join sub in SubjectCatBusiness.All on sb.SubjectID equals sub.SubjectCatID
                                      orderby sb.IsSubjectVNEN descending, sb.IsCommenting, sub.OrderInSubject
                                      where lstClassID.Contains(sb.ClassID)
                                      && sub.IsActive == true
                                      && ((semesterVNEN == GlobalConstants.SEMESTER_OF_YEAR_FIRST && sb.SectionPerWeekFirstSemester > 0)
                                      || (semesterVNEN == GlobalConstants.SEMESTER_OF_YEAR_SECOND && sb.SectionPerWeekSecondSemester > 0))
                                      select new SubjectCatBO
                                      {
                                          ClassID = sb.ClassID,
                                          SubjectCatID = sb.SubjectID,
                                          SubjectName = sub.SubjectName,
                                          IsCommenting = sb.IsCommenting,
                                          IsSubjectVNEN = sb.IsSubjectVNEN
                                      }).ToList();

                    // Danh sach danh gia So tay GV
                    List<TeacherNoteBookSemester> listNotesSem = (from tbs in TeacherNoteBookSemesterBusiness.All
                                                                  where tbs.SchoolID == schoolID && tbs.PartitionID == partitionId
                                                                   && tbs.AcademicYearID == academicYearID
                                                                   && lstClassID.Contains(tbs.ClassID)
                                                                   && tbs.SemesterID == semesterVNEN
                                                                   && pupilIDList.Contains(tbs.PupilID)
                                                                  select tbs).ToList();
                    // Thong tin theo TT58
                    List<SummedUpRecord> listSummedUpRecord = (from su in SummedUpRecordBusiness.All
                                                               where su.SchoolID == schoolID && su.Last2digitNumberSchool == partitionId
                                                                && su.AcademicYearID == academicYearID
                                                                && lstClassID.Contains(su.ClassID)
                                                                && ((semesterVNEN == GlobalConstants.SEMESTER_OF_YEAR_SECOND && su.Semester == GlobalConstants.SEMESTER_OF_YEAR_ALL)
                                                                 || (semesterVNEN == GlobalConstants.SEMESTER_OF_YEAR_FIRST && su.Semester == semesterVNEN))
                                                                && su.PeriodID == null
                                                                && pupilIDList.Contains(su.PupilID)
                                                               select su).ToList();
                    // Danh sach danh gia So tay HS
                    List<ReviewBookPupil> listReviewPupil = (from rv in ReviewBookPupilBusiness.All
                                                             where rv.SchoolID == schoolID && rv.PartitionID == partitionId
                                                                && rv.AcademicYearID == academicYearID && lstClassID.Contains(rv.ClassID)
                                                                && rv.SemesterID == semesterVNEN
                                                             && pupilIDList.Contains(rv.PupilID)
                                                             select rv).ToList();



                    // Danh sach khen thuong
                    List<UpdateReward> listRewardComments = (from ur in UpdateRewardBusiness.All
                                                             where ur.SchoolID == schoolID && ur.PartitionID == partitionId
                                                                        && ur.AcademicYearID == academicYearID
                                                                        && lstClassID.Contains(ur.ClassID)
                                                                        && ur.SemesterID == semesterVNEN
                                                                        && pupilIDList.Contains(ur.PupilID)
                                                             select ur).ToList();

                    List<RewardFinal> rewardFinalList = RewardFinalBusiness.All.Where(p => p.SchoolID == schoolID).ToList();
                    List<TeacherNoteBookSemester> lstNotesPerPupil = null;
                    TeacherNoteBookSemester objNotesPupil = null;
                    SubjectCatBO objSubjectBO = null;
                    SummedUpRecord objSummedUpRecord = null;
                    ReviewBookPupil objReviewPupil = null;

                    List<CustomSMSBO> lstDGMH = new List<CustomSMSBO>();
                    List<CustomSMSBO> lstNL = new List<CustomSMSBO>();
                    List<CustomSMSBO> lstPC = new List<CustomSMSBO>();
                    List<CustomSMSBO> lstKT = new List<CustomSMSBO>();
                    List<CustomSMSBO> lstDGCN = new List<CustomSMSBO>();
                    List<CustomSMSBO> lstNXCK = new List<CustomSMSBO>();
                    for (int i = 0; i < lstPupilOfClass.Count; i++)
                    {
                        pupilOfClassBO = lstPupilOfClass[i];
                        classID = pupilOfClassBO.ClassID;
                        pupilID = pupilOfClassBO.PupilID;
                        obj = new CustomSMSBO();
                        obj.PupilID = pupilID;
                        monthComments = new StringBuilder();
                        // Danh gia mon hoc
                        lstNotesPerPupil = listNotesSem.Where(p => p.PupilID == pupilID && p.ClassID == classID).ToList();
                        var lsCS = lsClassSubject.Where(p => p.ClassID == pupilOfClassBO.ClassID).ToList();
                        for (int j = 0; j < lsCS.Count; j++)
                        {
                            objSubjectBO = lsCS[j];
                            pupilContent = new StringBuilder();

                            if (objSubjectBO.IsSubjectVNEN == true)
                            {
                                objNotesPupil = lstNotesPerPupil.FirstOrDefault(o => o.SubjectID == objSubjectBO.SubjectCatID && o.ClassID == classID);
                                if (objNotesPupil != null)
                                {
                                    if (objNotesPupil.Rate == GlobalConstants.REVIEW_TYPE_FINISH) pupilContent.Append(GlobalConstants.SHORTFINISH).Append(", ");
                                    else if (objNotesPupil.Rate == GlobalConstants.REVIEW_TYPE_NOT_FINISH) pupilContent.Append(GlobalConstants.SHORTNOTFINISH).Append(", ");

                                    if (objSubjectBO.IsCommenting == GlobalConstants.SUBJECTCAT_ISCOMMENTING_MARK && objNotesPupil.AVERAGE_MARK.HasValue) pupilContent.Append(objNotesPupil.AVERAGE_MARK.Value > 0 ? (objNotesPupil.AVERAGE_MARK.Value == 10 ? "10" : objNotesPupil.AVERAGE_MARK.Value.ToString("0.0").Replace(",", ".")) : "0").Append("; ");
                                    else if (objSubjectBO.IsCommenting == GlobalConstants.SUBJECTCAT_ISCOMMENTING_COMMENT && !String.IsNullOrEmpty(objNotesPupil.PERIODIC_SCORE_END_JUDGLE)) pupilContent.Append(objNotesPupil.AVERAGE_MARK_JUDGE).Append("; ");
                                }
                            }
                            else
                            {
                                objSummedUpRecord = listSummedUpRecord.FirstOrDefault(o => o.PupilID == pupilID && o.SubjectID == objSubjectBO.SubjectCatID && o.ClassID == classID);
                                if (objSummedUpRecord != null)
                                {
                                    if (objSubjectBO.IsCommenting == GlobalConstants.SUBJECTCAT_ISCOMMENTING_MARK && objSummedUpRecord.SummedUpMark.HasValue) pupilContent.Append(objSummedUpRecord.SummedUpMark.Value > 0 ? (objSummedUpRecord.SummedUpMark.Value == 10 ? "10" : objSummedUpRecord.SummedUpMark.Value.ToString("0.0").Replace(",",".")) : "0").Append("; ");
                                    else if (objSubjectBO.IsCommenting == GlobalConstants.SUBJECTCAT_ISCOMMENTING_COMMENT && !String.IsNullOrEmpty(objSummedUpRecord.JudgementResult)) pupilContent.Append(objSummedUpRecord.JudgementResult).Append("; ");
                                }
                            }

                            if (pupilContent.Length > 0) monthComments.Append(objSubjectBO.SubjectName).Append(": ").Append(pupilContent.ToString());
                        }

                        obj.SMSContent = monthComments.ToString();
                        lstDGMH.Add(obj);

                        // Nang luc
                        obj = new CustomSMSBO();
                        obj.PupilID = pupilID;
                        monthComments = new StringBuilder();

                        objReviewPupil = listReviewPupil.FirstOrDefault(o => o.PupilID == pupilID && o.ClassID == classID);
                        if (objReviewPupil != null)
                        {
                            if (objReviewPupil.CapacityRate == GlobalConstants.EVALUATION_GOOD_COMPLETE_VAL) monthComments.AppendLine("Năng lực: T");
                            else if (objReviewPupil.CapacityRate == GlobalConstants.EVALUATION_CAPQUA_COMPLETE_VAL) monthComments.AppendLine("Năng lực: Đ");
                            else if (objReviewPupil.CapacityRate == GlobalConstants.EVALUATION_NOT_COMPLETE_VAL) monthComments.AppendLine("Năng lực: C");
                        }

                        obj.SMSContent = monthComments.ToString();
                        lstNL.Add(obj);

                        // Pham chat
                        obj = new CustomSMSBO();
                        obj.PupilID = pupilID;
                        monthComments = new StringBuilder();

                        if (objReviewPupil != null)
                        {
                            if (objReviewPupil.QualityRate == GlobalConstants.EVALUATION_GOOD_COMPLETE_VAL) monthComments.AppendLine("Phẩm chất: T");
                            else if (objReviewPupil.QualityRate == GlobalConstants.EVALUATION_CAPQUA_COMPLETE_VAL) monthComments.AppendLine("Phẩm chất: Đ");
                            else if (objReviewPupil.QualityRate == GlobalConstants.EVALUATION_NOT_COMPLETE_VAL) monthComments.AppendLine("Phẩm chất: C");
                        }

                        obj.SMSContent = monthComments.ToString();
                        lstPC.Add(obj);

                        // Khen thuong
                        obj = new CustomSMSBO();
                        obj.PupilID = pupilID;
                        monthComments = new StringBuilder();
                        string strRewardID = listRewardComments.Where(p => p.PupilID == pupilID && p.ClassID == classID).Select(p => p.Rewards).FirstOrDefault();
                        if (!string.IsNullOrEmpty(strRewardID))
                        {
                            List<int> listInt = strRewardID.Split(new[] { "," }, StringSplitOptions.RemoveEmptyEntries).Select(p => int.Parse(p)).ToList();
                            
                            string rewardMode = string.Empty;
                            pupilContent = new StringBuilder();
                            for (int g = 0; g < listInt.Count; g++)
                            {
                                rewardMode = rewardFinalList.Where(p => p.RewardFinalID == listInt[g]).Select(p => p.RewardMode).FirstOrDefault();
                                if (!string.IsNullOrWhiteSpace(rewardMode))
                                {
                                    pupilContent.Append(rewardMode);
                                    if (g != listInt.Count - 1)
                                    {
                                        pupilContent.Append("; ");
                                    }
                                }
                            }
                            if (pupilContent.Length > 0) monthComments.Append("Khen thưởng: ").AppendLine(pupilContent.ToString());
                        }

                        obj.SMSContent = monthComments.ToString();
                        lstKT.Add(obj);

                        // Danh gia cuoi nam
                        obj = new CustomSMSBO();
                        obj.PupilID = pupilID;
                        monthComments = new StringBuilder();
                        if (semesterVNEN == GlobalConstants.SEMESTER_OF_YEAR_SECOND)
                        {
                            if (objReviewPupil != null)
                            {
                                if (objReviewPupil.RateAndYear == GlobalConstants.REVIEW_TYPE_FINISH)
                                {
                                    monthComments.AppendLine("Được lên lớp");
                                }
                                else if (objReviewPupil.RateAndYear == GlobalConstants.REVIEW_TYPE_NOT_FINISH)
                                {
                                    if (!objReviewPupil.RateAdd.HasValue) monthComments.AppendLine("Chưa hoàn thành chương trình lớp học");
                                    else if (objReviewPupil.RateAdd == GlobalConstants.REVIEW_TYPE_FINISH) monthComments.AppendLine("Được lên lớp");
                                    else if (objReviewPupil.RateAdd == GlobalConstants.REVIEW_TYPE_NOT_FINISH) monthComments.AppendLine("Ở lại lớp");
                                }
                            }
                        }

                        obj.SMSContent = monthComments.ToString();
                        lstDGCN.Add(obj);

                        //NXCK
                        monthComments = new StringBuilder();
                        pupilID = pupilOfClassBO.PupilID;
                        obj = new CustomSMSBO();
                        obj.PupilID = pupilID;

                        // lay thong tin nhan xet (So tay hoc sinh)
                        if (objReviewPupil != null)
                        {
                            if (!string.IsNullOrEmpty(objReviewPupil.CQComment)) monthComments.AppendLine(string.Format("Biểu hiện nổi bật: {0}", objReviewPupil.CQComment));
                            if (!string.IsNullOrEmpty(objReviewPupil.CommentAdd)) monthComments.AppendLine(string.Format("Điều cần khắc phục giúp đỡ, rèn luyện thêm: {0}", objReviewPupil.CommentAdd));
                        }


                        if (monthComments != null && monthComments.Length > 0)
                        {
                            obj.SMSContent = monthComments.ToString();
                        }
                        lstNXCK.Add(obj);
                    }

                    dic["DGMH"] = lstDGMH;
                    dic["DGCN"] = lstDGCN;
                    dic["NL"] = lstNL;
                    dic["PC"] = lstPC;
                    dic["KT"] = lstKT;
                    dic["NXCK"] = lstNXCK;


                }
                #endregion

                #region Cap 1
                else
                {
                    IDictionary<string, object> tmpDic = new Dictionary<string, object>();
                    //list mon hoc
                    List<SubjectCatBO> lstClassSubject = new List<SubjectCatBO>();
                    if (lstClassID.Count > 0)
                    {
                        lstClassSubject = (from sb in ClassSubjectBusiness.All
                                           join sub in SubjectCatBusiness.All on sb.SubjectID equals sub.SubjectCatID
                                           orderby sb.IsCommenting, sub.OrderInSubject
                                           where lstClassID.Contains(sb.ClassID)
                                           && sub.IsActive == true
                                           && ((bigSemester == GlobalConstants.SEMESTER_OF_YEAR_FIRST && sb.SectionPerWeekFirstSemester > 0)
                                           || (bigSemester == GlobalConstants.SEMESTER_OF_YEAR_SECOND && sb.SectionPerWeekSecondSemester > 0))
                                           select new SubjectCatBO
                                           {
                                               ClassID = sb.ClassID,
                                               SubjectCatID = sb.SubjectID,
                                               SubjectName = sub.SubjectName,
                                               IsCommenting = sb.IsCommenting
                                           }
                                         ).ToList();
                    }

                    //Khen thuong 
                    tmpDic = new Dictionary<string, object>()
                    {
                        {"SchoolID",schoolID},
                        {"AcademicYearID",academicYearID},
                        {"lstClassID",lstClassID},
                        {"Semester", GlobalConstants.SEMESTER_OF_EVALUATION_RESULT}
                    };
                    List<SummedEndingEvaluationBO> lstSEE = SummedEndingEvaluationBusiness.Search(tmpDic).ToList();

                    tmpDic = new Dictionary<string, object>()
                    {
                        {"SchoolID",schoolID},
                        {"AcademicYearID",academicYearID},
                        {"lstClassID",lstClassID},
                        {"SemesterID", bigSemester},
                        {"lstPupilID", pupilIDList.ConvertAll(i => (int)i)},

                    };

                    List<EvaluationReward> lstEvaluationReward = EvaluationRewardBusiness.Search(tmpDic).ToList();

                    //List loai nang luc va pham chat
                    List<EvaluationCriteria> lstEc = EvaluationCriteriaBusiness.All.ToList();
                    List<EvaluationCriteria> lstEcNL = lstEc.Where(o => o.TypeID == 2).OrderBy(o => o.EvaluationCriteriaID).ToList();
                    List<EvaluationCriteria> lstEcPC = lstEc.Where(o => o.TypeID == 3).OrderBy(o => o.EvaluationCriteriaID).ToList();

                    tmpDic = new Dictionary<string, object>();
                    tmpDic["SchoolID"] = schoolID;
                    tmpDic["AcademicYearID"] = academicYearID;
                    //tmpDic["ClassID"] = classID;
                    tmpDic["lstClassID"] = lstClassID;
                    tmpDic["SemesterID"] = bigSemester;

                    AcademicYear objAy = AcademicYearBusiness.Find(academicYearID);

                    List<RatedCommentPupilBO> lstRatedCommentAll;
                    if (UtilsBusiness.IsMoveHistory(objAy))
                    {
                        lstRatedCommentAll = RatedCommentPupilHistoryBusiness.Search(tmpDic)
                            .Where(p => lstClassID.Contains(p.ClassID))
                            .Select(o => new RatedCommentPupilBO
                            {
                                CapacityEndingEvaluation = o.CapacityEndingEvaluation,
                                CapacityMiddleEvaluation = o.CapacityMiddleEvaluation,
                                Comment = o.Comment,
                                EndingEvaluation = o.EndingEvaluation,
                                EvaluationID = o.EvaluationID,
                                MiddleEvaluation = o.MiddleEvaluation,
                                PeriodicEndingJudgement = o.PeriodicEndingJudgement,
                                PeriodicEndingMark = o.PeriodicEndingMark,
                                PeriodicMiddleJudgement = o.PeriodicMiddleJudgement,
                                PeriodicMiddleMark = o.PeriodicMiddleMark,
                                PupilID = o.PupilID,
                                QualityEndingEvaluation = o.QualityEndingEvaluation,
                                QualityMiddleEvaluation = o.QualityMiddleEvaluation,
                                RetestMark = o.RetestMark,
                                SemesterID = o.SemesterID,
                                SubjectID = o.SubjectID
                            }).ToList();
                    }
                    else
                    {
                        lstRatedCommentAll = RatedCommentPupilBusiness.Search(tmpDic)
                            .Where(p => lstClassID.Contains(p.ClassID))
                            .Select(o => new RatedCommentPupilBO
                            {
                                CapacityEndingEvaluation = o.CapacityEndingEvaluation,
                                CapacityMiddleEvaluation = o.CapacityMiddleEvaluation,
                                Comment = o.Comment,
                                EndingEvaluation = o.EndingEvaluation,
                                EvaluationID = o.EvaluationID,
                                MiddleEvaluation = o.MiddleEvaluation,
                                PeriodicEndingJudgement = o.PeriodicEndingJudgement,
                                PeriodicEndingMark = o.PeriodicEndingMark,
                                PeriodicMiddleJudgement = o.PeriodicMiddleJudgement,
                                PeriodicMiddleMark = o.PeriodicMiddleMark,
                                PupilID = o.PupilID,
                                QualityEndingEvaluation = o.QualityEndingEvaluation,
                                QualityMiddleEvaluation = o.QualityMiddleEvaluation,
                                RetestMark = o.RetestMark,
                                SemesterID = o.SemesterID,
                                SubjectID = o.SubjectID
                            }).ToList();
                    }

                    List<CustomSMSBO> lstMHHDGD = new List<CustomSMSBO>();
                    List<CustomSMSBO> lstNL = new List<CustomSMSBO>();
                    List<CustomSMSBO> lstPC = new List<CustomSMSBO>();
                    List<CustomSMSBO> lstKT = new List<CustomSMSBO>();
                    List<CustomSMSBO> lstKQNH = new List<CustomSMSBO>();
                    CustomSMSBO obj;

                    int type = semester == 3 || semester == 4 ? 1 : 2;
                    for (int i = 0; i < lstPupilOfClass.Count; i++)
                    {
                        PupilOfClassBO poc = lstPupilOfClass[i];
                        classID = poc.ClassID;
                        List<RatedCommentPupilBO> lstRatedCommentPupil = lstRatedCommentAll.Where(o => o.PupilID == poc.PupilID && poc.ClassID == classID).ToList();


                        string tempNL;
                        string tempPC;
                        string tempKT;
                        //Tong hop ket qua
                        StringBuilder sb = new StringBuilder();
                        switch (type)
                        {
                            //Ket qua giua ky
                            case 1:
                                //Mon hoc & HDGD
                                obj = new CustomSMSBO();
                                obj.PupilID = poc.PupilID;
                                sb = new StringBuilder();
                                var lstCS = lstClassSubject.Where(p => p.ClassID == classID).ToList();
                                for (int j = 0; j < lstCS.Count; j++)
                                {
                                    SubjectCatBO subject = lstCS[j];

                                    RatedCommentPupilBO ratedCommentSubject = lstRatedCommentPupil
                                        .Where(o => o.EvaluationID == 1 && o.SubjectID == subject.SubjectCatID).FirstOrDefault();

                                    if (ratedCommentSubject != null)
                                    {
                                        string evaluation = ratedCommentSubject.MiddleEvaluation == 1 ? "T"
                                            : (ratedCommentSubject.MiddleEvaluation == 2 ? "H" :
                                            (ratedCommentSubject.MiddleEvaluation == 3 ? "C" : string.Empty));

                                        string mark;

                                        if (subject.IsCommenting.HasValue && subject.IsCommenting == 1)
                                        {
                                            mark = ratedCommentSubject.PeriodicMiddleJudgement;
                                        }
                                        else
                                        {
                                            mark = ratedCommentSubject.PeriodicMiddleMark.HasValue ?
                                                ratedCommentSubject.PeriodicMiddleMark.Value.ToString() : string.Empty;
                                        }

                                        if (!string.IsNullOrEmpty(evaluation) || !string.IsNullOrEmpty(mark))
                                        {
                                            sb.Append(subject.SubjectName);
                                            sb.Append(": ");
                                            if (!string.IsNullOrEmpty(evaluation))
                                            {
                                                sb.Append(evaluation);
                                            }

                                            if (!string.IsNullOrEmpty(evaluation) && !string.IsNullOrEmpty(mark))
                                            {
                                                sb.Append(",");
                                            }

                                            if (!string.IsNullOrEmpty(mark))
                                            {
                                                sb.Append(mark);
                                            }

                                            sb.Append(";");
                                        }
                                    }
                                }

                                obj.SMSContent = sb.ToString();
                                lstMHHDGD.Add(obj);

                                //Nang luc
                                obj = new CustomSMSBO();
                                obj.PupilID = poc.PupilID;
                                sb = new StringBuilder();

                                tempNL = string.Empty;
                                for (int j = 0; j < lstEcNL.Count; j++)
                                {
                                    EvaluationCriteria ec = lstEcNL[j];

                                    RatedCommentPupilBO ratedCommentSubject = lstRatedCommentPupil
                                        .Where(o => o.EvaluationID == 2 && o.SubjectID == ec.EvaluationCriteriaID).FirstOrDefault();

                                    if (ratedCommentSubject != null)
                                    {
                                        string evaluation = ratedCommentSubject.CapacityMiddleEvaluation == 1 ? "T"
                                            : (ratedCommentSubject.CapacityMiddleEvaluation == 2 ? "Đ" :
                                            (ratedCommentSubject.CapacityMiddleEvaluation == 3 ? "C" : string.Empty));

                                        if (!string.IsNullOrEmpty(evaluation))
                                        {
                                            tempNL += evaluation;
                                            tempNL += ";";
                                        }
                                    }
                                }
                                if (!string.IsNullOrWhiteSpace(tempNL))
                                {
                                    sb.Append("Năng lực: ");
                                    sb.Append(tempNL);
                                }

                                obj.SMSContent = sb.ToString();
                                lstNL.Add(obj);

                                //Pham chat
                                obj = new CustomSMSBO();
                                obj.PupilID = poc.PupilID;
                                sb = new StringBuilder();

                                tempPC = string.Empty;
                                for (int j = 0; j < lstEcPC.Count; j++)
                                {
                                    EvaluationCriteria ec = lstEcPC[j];

                                    RatedCommentPupilBO ratedCommentSubject = lstRatedCommentPupil
                                        .Where(o => o.EvaluationID == 3 && o.SubjectID == ec.EvaluationCriteriaID).FirstOrDefault();

                                    if (ratedCommentSubject != null)
                                    {
                                        string evaluation = ratedCommentSubject.QualityMiddleEvaluation == 1 ? "T"
                                            : (ratedCommentSubject.QualityMiddleEvaluation == 2 ? "Đ" :
                                            (ratedCommentSubject.QualityMiddleEvaluation == 3 ? "C" : string.Empty));

                                        if (!string.IsNullOrEmpty(evaluation))
                                        {
                                            tempPC += evaluation;
                                            tempPC += ";";
                                        }
                                    }
                                }

                                if (!string.IsNullOrWhiteSpace(tempPC))
                                {
                                    sb.Append("Phẩm chất: ");
                                    sb.Append(tempPC);
                                }

                                obj.SMSContent = sb.ToString();
                                lstPC.Add(obj);
                                break;
                            //Ket qua cuoi ky
                            case 2:
                                //Mon hoc & HDGD
                                obj = new CustomSMSBO();
                                obj.PupilID = poc.PupilID;
                                sb = new StringBuilder();
                                lstCS = lstClassSubject.Where(p => p.ClassID == classID).ToList();
                                for (int j = 0; j < lstCS.Count; j++)
                                {
                                    SubjectCatBO subject = lstCS[j];

                                    RatedCommentPupilBO ratedCommentSubject = lstRatedCommentPupil
                                        .Where(o => o.EvaluationID == 1 && o.SubjectID == subject.SubjectCatID).FirstOrDefault();

                                    if (ratedCommentSubject != null)
                                    {
                                        string evaluation = ratedCommentSubject.EndingEvaluation == 1 ? "T"
                                            : (ratedCommentSubject.EndingEvaluation == 2 ? "H" :
                                            (ratedCommentSubject.EndingEvaluation == 3 ? "C" : string.Empty));

                                        string mark;

                                        if (subject.IsCommenting.HasValue && subject.IsCommenting == 1)
                                        {
                                            mark = ratedCommentSubject.PeriodicEndingJudgement;
                                        }
                                        else
                                        {
                                            mark = ratedCommentSubject.PeriodicEndingMark.HasValue ?
                                                ratedCommentSubject.PeriodicEndingMark.Value.ToString() : string.Empty;
                                        }

                                        if (!string.IsNullOrEmpty(evaluation) || !string.IsNullOrEmpty(mark))
                                        {
                                            sb.Append(subject.SubjectName);
                                            sb.Append(": ");
                                            if (!string.IsNullOrEmpty(evaluation))
                                            {
                                                sb.Append(evaluation);
                                            }

                                            if (!string.IsNullOrEmpty(evaluation) && !string.IsNullOrEmpty(mark))
                                            {
                                                sb.Append(",");
                                            }

                                            if (!string.IsNullOrEmpty(mark))
                                            {
                                                sb.Append(mark);
                                            }

                                            sb.Append(";");
                                        }
                                    }
                                }
                                obj.SMSContent = sb.ToString();
                                lstMHHDGD.Add(obj);

                                //Nang luc
                                obj = new CustomSMSBO();
                                obj.PupilID = poc.PupilID;
                                sb = new StringBuilder();
                                tempNL = string.Empty;
                                for (int j = 0; j < lstEcNL.Count; j++)
                                {
                                    EvaluationCriteria ec = lstEcNL[j];

                                    RatedCommentPupilBO ratedCommentSubject = lstRatedCommentPupil
                                        .Where(o => o.EvaluationID == 2 && o.SubjectID == ec.EvaluationCriteriaID).FirstOrDefault();

                                    if (ratedCommentSubject != null)
                                    {
                                        string evaluation = ratedCommentSubject.CapacityEndingEvaluation == 1 ? "T"
                                            : (ratedCommentSubject.CapacityEndingEvaluation == 2 ? "Đ" :
                                            (ratedCommentSubject.CapacityEndingEvaluation == 3 ? "C" : string.Empty));

                                        if (!string.IsNullOrEmpty(evaluation))
                                        {
                                            tempNL += evaluation;
                                            tempNL += ";";
                                        }
                                    }
                                }

                                if (!string.IsNullOrWhiteSpace(tempNL))
                                {
                                    sb.Append("Năng lực: ");
                                    sb.Append(tempNL);

                                }

                                obj.SMSContent = sb.ToString();
                                lstNL.Add(obj);

                                //Pham chat
                                obj = new CustomSMSBO();
                                obj.PupilID = poc.PupilID;
                                sb = new StringBuilder();
                                tempPC = string.Empty;
                                for (int j = 0; j < lstEcPC.Count; j++)
                                {
                                    EvaluationCriteria ec = lstEcPC[j];

                                    RatedCommentPupilBO ratedCommentSubject = lstRatedCommentPupil
                                        .Where(o => o.EvaluationID == 3 && o.SubjectID == ec.EvaluationCriteriaID).FirstOrDefault();

                                    if (ratedCommentSubject != null)
                                    {
                                        string evaluation = ratedCommentSubject.QualityEndingEvaluation == 1 ? "T"
                                            : (ratedCommentSubject.QualityEndingEvaluation == 2 ? "Đ" :
                                            (ratedCommentSubject.QualityEndingEvaluation == 3 ? "C" : string.Empty));

                                        if (!string.IsNullOrEmpty(evaluation))
                                        {
                                            tempPC += evaluation;
                                            tempPC += ";";
                                        }
                                    }
                                }

                                if (!string.IsNullOrWhiteSpace(tempPC))
                                {
                                    sb.Append("Phẩm chất: ");
                                    sb.Append(tempPC);

                                }

                                obj.SMSContent = sb.ToString();
                                lstPC.Add(obj);

                                break;
                        }

                        //Khen thuong
                        if (semester == GlobalConstants.SEMESTER_OF_YEAR_SECOND || semester == GlobalConstants.SEMESTER_OF_YEAR_FIRST)
                        {
                            obj = new CustomSMSBO();
                            obj.PupilID = poc.PupilID;
                            sb = new StringBuilder();
                            tempKT = string.Empty;
                            SummedEndingEvaluationBO see = lstSEE.Where(o => o.ClassID == classID && o.PupilID == poc.PupilID && bigSemester == GlobalConstants.SEMESTER_OF_YEAR_SECOND && o.RewardID != null && o.RewardID != 0).FirstOrDefault();
                            if (see != null)
                            {
                                tempKT += "Học sinh xuất sắc";
                                tempKT += ";";
                            }

                            List<EvaluationReward> lstEvaluationRewardPupil = lstEvaluationReward.Where(o => o.ClassID == classID && o.PupilID == poc.PupilID).ToList();

                            for (int j = 0; j < lstEvaluationRewardPupil.Count; j++)
                            {
                                EvaluationReward er = lstEvaluationRewardPupil[j];
                                string rewardName = er.RewardID == 1 || er.RewardID == 2 ? er.Content : string.Empty;
                                if (!string.IsNullOrEmpty(rewardName))
                                {
                                    tempKT += rewardName;
                                    tempKT += ";";
                                }
                            }

                            if (!string.IsNullOrWhiteSpace(tempKT))
                            {
                                sb.Append("Khen thưởng: ");
                                sb.Append(tempKT);

                            }
                            obj.SMSContent = sb.ToString();
                            lstKT.Add(obj);
                        }

                        if (semester == GlobalConstants.SEMESTER_OF_YEAR_SECOND)
                        {
                            //Ket qua nam hoc
                            obj = new CustomSMSBO();
                            obj.PupilID = poc.PupilID;
                            sb = new StringBuilder();

                            SummedEndingEvaluationBO objSee = lstSEE.Where(o => o.ClassID == classID && o.PupilID == poc.PupilID).FirstOrDefault();

                            if (objSee != null)
                            {

                                string evaluation = string.Empty;
                                if (objSee.EndingEvaluation == "HT")
                                {
                                    evaluation = "Được lên lớp";
                                }
                                else if (objSee.EndingEvaluation == "CHT" && objSee.RateAdd != 0 && objSee.RateAdd != 1)
                                {
                                    evaluation = "Chưa hoàn thành chương trình lớp học";
                                }
                                else if (objSee.EndingEvaluation == "CHT" && objSee.RateAdd == 1)
                                {
                                    evaluation = "Được lên lớp";
                                }
                                else if (objSee.EndingEvaluation == "CHT" && objSee.RateAdd == 0)
                                {
                                    evaluation = "Ở lại lớp";
                                }

                                sb.Append(evaluation);
                            }

                            obj.SMSContent = sb.ToString();
                            lstKQNH.Add(obj);

                        }


                    }


                    dic["MHHDGD"] = lstMHHDGD;
                    dic["NL"] = lstNL;
                    dic["PC"] = lstPC;
                    dic["KQNH"] = lstKQNH;
                    dic["KT"] = lstKT;

                }

                #endregion

                bool isEmpty;
                for (int i = 0; i < lstPupilOfClass.Count; i++)
                {
                    PupilOfClassBO poc = lstPupilOfClass[i];
                    isEmpty = true;
                    objData = new CustomSMSBO();
                    objData.PupilID = poc.PupilID;
                    string content = template;

                    for (int j = 0; j < lstContentTypeCode.Count; j++)
                    {
                        string code = lstContentTypeCode[j];
                        string contentOfCode = string.Empty;
                        if (code == "THS")
                        {
                            contentOfCode = poc.PupilFullName.FormatName(nameDisplay);
                        }
                        else if (code == "TGDK")
                        {
                            contentOfCode = TGDK;
                        }
                        else
                        {
                            if (dic.ContainsKey(code))
                            {
                                List<CustomSMSBO> lstSMS = (List<CustomSMSBO>)dic[code];
                                if (lstSMS != null)
                                {
                                    CustomSMSBO sms = lstSMS.FirstOrDefault(o => o.PupilID == poc.PupilID);
                                    if (sms != null && !string.IsNullOrWhiteSpace(sms.SMSContent))
                                    {
                                        contentOfCode = sms.SMSContent.Trim();
                                        isEmpty = false;
                                    }
                                }
                            }
                        }

                        content = Regex.Replace(content.Replace("{" + code + "}", contentOfCode), @"(\r\n)+", "\r\n", RegexOptions.Singleline | RegexOptions.Compiled | RegexOptions.ExplicitCapture | RegexOptions.IgnoreCase);
                    }

                    if (lstContentTypeCode.Except(new string[] { "THS", "TGDK" }).Count() == 0)
                    {
                        isEmpty = false;
                    }

                    if (!isEmpty)
                    {
                        objData.SMSContent = content.Trim();
                        if (objData.SMSContent.EndsWith(";"))
                        {
                            objData.SMSContent = objData.SMSContent.Substring(0, objData.SMSContent.Length - 1);
                        }
                    }
                    else
                    {
                        objData.SMSContent = string.Empty;
                    }
                    lstData.Add(objData);
                }

                return lstData;
            }
            catch (Exception ex)
            {
                logger.Error(ex.Message, ex);
                return new List<CustomSMSBO>();
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="dic"></param>
        /// <param name="superVisingDeptID"></param>
        /// <param name="totalRecord"></param>
        /// <returns></returns>
        public List<SMSCommunicationBO> SearchUnitHistorySMS(Dictionary<string, object> dic, int superVisingDeptID, ref int totalRecord)
        {
            
            try
            {
                int Type = dic.GetInt("type");
                DateTime? fromDate = dic.GetDateTime("fromDate");
                DateTime? toDate = dic.GetDateTime("toDate");
                string senderUsername = dic.GetString("senderID");
                int currentPage = dic.GetInt("page");
                int numRecord = dic.GetInt("numRecord");//to do
                if (numRecord == 0) numRecord = GlobalConstantsEdu.PAGESIZE;
                if (toDate.HasValue)
                {
                    //End day
                    toDate = new DateTime(toDate.Value.Year, toDate.Value.Month, toDate.Value.Day, 23, 59, 59);
                }

                #region Using LinQ
                SupervisingDept curUnit = SupervisingDeptBusiness.Find(superVisingDeptID);
                List<int> lstPBUnitID = new List<int>();
                if (curUnit != null)
                {
                    var lstAll = SupervisingDeptBusiness.GetListSuperVisingDeptBO(superVisingDeptID);
                    if (lstAll != null)
                    {
                        var lstRev = lstAll.Where(a => a.HierachyLevel == GlobalConstantsEdu.COMMON_SUPERVISING_DEPT_HIERACHY_LEVEL_DISTRICT_OFFICE
                        || a.HierachyLevel == GlobalConstantsEdu.COMMON_SUPERVISING_DEPT_HIERACHY_LEVEL_DISTRICT_OFFICE_DEPARTMENT).Select(a => a.Traversal_Path).ToList();
                        foreach (string s in lstRev)//to do
                        {
                            lstAll = lstAll.Where(a => a.SupervisingDeptID > 0 && !(a.Traversal_Path.StartsWith(s))).ToList();
                        }
                        lstPBUnitID = lstAll.Select(a => a.SupervisingDeptID).ToList();
                        if (lstPBUnitID == null) lstPBUnitID = new List<int>();
                        lstPBUnitID.Add(superVisingDeptID);
                    }
                }
                int typeToSchool = GlobalConstantsEdu.COMMON_HISTORYSMS_TO_SCHOOL_ID;
                int typeToUnit = GlobalConstantsEdu.COMMON_HISTORYSMS_TO_EMPLOYEE_ID;
                IQueryable<SMSCommunicationBO> query = (from his in this.All
                                                        where EntityFunctions.TruncateTime(his.CREATE_DATE) >= EntityFunctions.TruncateTime(fromDate)
                                                        && EntityFunctions.TruncateTime(his.CREATE_DATE) <= EntityFunctions.TruncateTime(toDate)
                                                        && his.SENDER_UNIT_ID.HasValue && lstPBUnitID.Contains(his.SENDER_UNIT_ID.Value)
                                                        && (his.SMS_TYPE_ID == typeToUnit || his.SMS_TYPE_ID == typeToSchool)
                                                        && (his.SMS_TYPE_ID == Type || Type == 0)
                                                        select new SMSCommunicationBO
                                                        {
                                                            HistoryID = his.HISTORY_ID,
                                                            Content = his.SHORT_CONTENT,
                                                            TeacherSenderName = his.SENDER_NAME,
                                                            SenderID = his.SENDER_UNIT_ID.HasValue ? his.SENDER_UNIT_ID.Value : 0,
                                                            ContentCount = his.SMS_CNT,
                                                            CreateDate = his.CREATE_DATE,
                                                            Subscriber = his.MOBILE,
                                                            TeacherReceiverName = his.RECEIVER_NAME,
                                                            ParentReceiverName = his.RECEIVER_UNIT_NAME,
                                                            TypeHistory = his.SMS_TYPE_ID,
                                                            ReceiverID = his.RECEIVER_ID,
                                                            Status = his.STATUS ? "1" : "0",
                                                        });
                if (!string.IsNullOrEmpty(senderUsername))
                {
                    query = query.Where(p => p.TeacherSenderName.ToUpper().Equals(senderUsername.ToUpper()));
                }
                //Dem so luong total sms
                totalRecord = query.Count();
                //Sort the result
                query = query.OrderByDescending(p => p.CreateDate);
                //Lay theo so luong phan trang
                var result = query.Skip((currentPage - 1) * GlobalConstantsEdu.PAGESIZE).Take(numRecord).OrderByDescending(p => p.CreateDate).ToList();

                return result;

                #endregion
            }
            catch (Exception ex)
            {
                string para = string.Format("superVisingDeptID={0}", superVisingDeptID);
                LogExtensions.ErrorExt(logger, DateTime.Now, "SearchUnitHistorySMS", para, ex);
                throw ex;
            }
        }

        public IQueryable<TeacherBO> GetListReceiverBySenderGroup(Dictionary<string, object> dic)
        {
            try
            {
                long senderGroup = dic.GetLong("senderGroup");
                int schoolID = dic.GetInt("schoolID");
                int type = GlobalConstantsEdu.COMMON_HISTORY_RECEIVER_TEACHER_ID;
                int partitionID = UtilsBusiness.GetPartionId(schoolID);

                //var list = this.Context.GetTeachersBySenderGroup(schoolID, senderGroup, type, currentPage, pageSize);
                IQueryable<TeacherBO> iquery = (from h in SMSHistoryBusiness.All
                                                where h.SCHOOL_ID == schoolID
                                                && h.PARTITION_ID == partitionID
                                                && h.SENDER_GROUP == senderGroup
                                                && h.RECEIVE_TYPE == type
                                                group h by new
                                                {
                                                    h.RECEIVER_NAME,
                                                    h.RECEIVER_ID,
                                                    h.IS_SMS,
                                                    h.MOBILE,
                                                    h.STATUS,
                                                } into g1
                                                select new TeacherBO
                                                {
                                                    FullName = g1.Key.RECEIVER_NAME,
                                                    Status = g1.Key.STATUS == true ? "Gửi thành công" : "Gửi không thành công",
                                                    Mobile = g1.Key.MOBILE,
                                                    TotalSMS = g1.Sum(p => p.SMS_CNT)
                                                }).OrderBy(p => p.FullName);
                return iquery;
            }
            catch (NullReferenceException ex)
            {
                throw ex;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public List<SMSCommunicationBO> GetListNotification(Dictionary<string, object> dic, ref int totalRecord)
        {
            int schoolID = dic.GetInt("schoolID");
            int academicYearID = dic.GetInt("AcademicYearID");
            int classID = dic.GetInt("classID");
            int Type = dic.GetInt("Type");
            int year = dic.GetInt("year");
            DateTime? fromDate = dic.GetDateTime("fromDate");
            DateTime? toDate = dic.GetDateTime("toDate");
            string teacherName = dic.GetString("teacherID");
            int contactGroupID = dic.GetInt("contactGroupID");
            int pupilID = dic.GetInt("pupilID", 0);
            int currentPage = dic.GetInt("page");
            int numRecord = dic.GetInt("numRecord");
            int level = dic.GetInt("level", 0);
            int levelType = dic.GetInt("levelType", 0);
            int SMSTypeID = dic.GetInt("TypeID", 0);
            int isCustomType = dic.GetInt("isCustomType");
            bool getAccount = dic.GetBool("getAccount", false);
            bool IsAdmin = dic.GetBool("IsAdmin", false);
            if (numRecord == 0) numRecord = 20;
            fromDate = fromDate.Value.Date;
            toDate = toDate.Value;
            toDate = new DateTime(toDate.Value.Year, toDate.Value.Month, toDate.Value.Day, 23, 59, 59);
            List<SMSCommunicationBO> lstSMSCommunicationBO = new List<SMSCommunicationBO>();
            List<SMSHistoryBO> searchParentSMSList = new List<SMSHistoryBO>();
            int partitionID = UtilsBusiness.GetPartionId(schoolID);
            IDictionary<string, object> dicSearchPupil = new Dictionary<string, object>()
            {
                {"SchoolID",schoolID},
                {"AcademicYearID",academicYearID},
                {"AppliedLevel",level}
            };
            if (Type == GlobalConstantsEdu.COMMON_SMS_COMUNICATION_TEACHER_TO_PARENT)
            {
                #region lay danh sach smscommunication truong hop la phhs
                searchParentSMSList = this.SearchParentSMS(schoolID,academicYearID, level, classID,pupilID, teacherName, year, SMSTypeID, isCustomType, fromDate.Value, toDate.Value, currentPage, ref totalRecord, GlobalConstantsEdu.PAGESIZE).ToList();
                // lay 1 list pupilProfileID tu danh sach goi tu sp
                List<int> recieverIDList = searchParentSMSList.Select(p => p.ReceiverID).ToList();// list pupilprofileID
                List<int> senderIDList = searchParentSMSList.Select(p => p.SenderID).ToList();// list employeeID

                // goi api truyen list ID (reciever) vao lay hoc sinh {ID , FullName}
                List<PupilOfClassBO> pupilProfileList = PupilProfileBusiness.getListPupilByListPupilID(recieverIDList, academicYearID).ToList();
                pupilProfileList = pupilProfileList != null ? pupilProfileList : new List<PupilOfClassBO>();
                //************************************************************************************************************

                //*************************************************************************************************************
                // goi API lay list ID giao vien (sender) lay giao vien {ID , FullName}
                List<TeacherBO> teacherList = EmployeeBusiness.GetListTeacher(senderIDList);
                teacherList = teacherList != null ? teacherList : new List<TeacherBO>();
                //*************************************************************************************************************
                List<SMSCommunicationBO> smsCommunicationBoList = new List<SMSCommunicationBO>();
                SMSCommunicationBO smsCommunicationBOObj = null;
                SMSHistoryBO searchParentSMSOBj = null;
                // for list sp -> gan ten hoc sinh, giao vien vao
                for (int i = 0; i < searchParentSMSList.Count; i++)
                {
                    searchParentSMSOBj = searchParentSMSList[i];
                    smsCommunicationBOObj = new SMSCommunicationBO();
                    smsCommunicationBOObj.CreateDate = searchParentSMSOBj.CreateDate;
                    smsCommunicationBOObj.IsAdmin = searchParentSMSOBj.IsAdmin;
                    smsCommunicationBOObj.SenderGroup = searchParentSMSOBj.SenderGroup.HasValue ? searchParentSMSOBj.SenderGroup.Value : 0;
                    smsCommunicationBOObj.Content = searchParentSMSOBj.SMSContent;
                    //smsCommunicationBOObj.ContentCount = searchParentSMSOBj.ContentCount * searchParentSMSOBj.Subscriber.Split(new char[] { GlobalConstants.charCOMMA }, StringSplitOptions.RemoveEmptyEntries).Length;
                    smsCommunicationBOObj.ContentCount = string.IsNullOrEmpty(searchParentSMSOBj.Subscriber) ? searchParentSMSOBj.SMSCNT : searchParentSMSOBj.SMSCNT * searchParentSMSOBj.Subscriber.Split(new char[] { GlobalConstantsEdu.charCOMMA }, StringSplitOptions.RemoveEmptyEntries).Length;
                    smsCommunicationBOObj.Subscriber = searchParentSMSOBj.Subscriber;
                    smsCommunicationBOObj.ParentReceiverName = pupilProfileList.Where(p => p.PupilID == searchParentSMSOBj.ReceiverID).Select(p => p.PupilFullName).FirstOrDefault();
                    smsCommunicationBOObj.TeacherSenderName = teacherList.Where(p => p.TeacherID == searchParentSMSOBj.SenderID).Select(p => p.FullName).FirstOrDefault();
                    smsCommunicationBOObj.SMSType = searchParentSMSOBj.TypeName;
                    smsCommunicationBOObj.IsOnSchedule = searchParentSMSOBj.IsOnSchedule;
                    smsCommunicationBOObj.SendTime = searchParentSMSOBj.SendTime.GetValueOrDefault();

                    if (searchParentSMSOBj.Status == true)
                    {
                        smsCommunicationBOObj.Status = "Gửi thành công";
                    }
                    else if (searchParentSMSOBj.Status == false)
                    {
                        smsCommunicationBOObj.Status = "Gửi không thành công";
                    }
                    else
                    {
                        smsCommunicationBOObj.Status = "Chưa gửi";
                    }

                    if (searchParentSMSOBj.IsOnSchedule == true)
                    {
                        smsCommunicationBOObj.Status = "Đang chờ gửi";
                    }

                    smsCommunicationBoList.Add(smsCommunicationBOObj);
                }
                #endregion
                return smsCommunicationBoList;
            }
            else
            {
                #region lay danh sach smscommunication truong hop la giao vien
                var lstTmp = (
                              from hs in SMSHistoryBusiness.All
                              join tcg in ContactGroupBusiness.All on hs.CONTACT_GROUP_ID equals tcg.CONTACT_GROUP_ID
                              where EntityFunctions.TruncateTime(fromDate) <= EntityFunctions.TruncateTime(hs.CREATE_DATE) 
                              && EntityFunctions.TruncateTime(hs.CREATE_DATE) <= EntityFunctions.TruncateTime(toDate)
                              && hs.SCHOOL_ID == schoolID
                              && hs.PARTITION_ID == partitionID
                              && (hs.RECEIVE_TYPE == GlobalConstantsEdu.COMMON_HISTORY_RECEIVER_TEACHER_ID)
                              select new
                              {
                                  ContactGroupID = hs.CONTACT_GROUP_ID,
                                  ReceiverID = hs.RECEIVER_ID,
                                  SenderID = hs.SENDER_ID,
                                  TypeHistory = hs.SMS_TYPE_ID,
                                  Content = hs.SMS_CONTENT,
                                  ContentCount = hs.SMS_CNT,
                                  ShortContent = hs.SHORT_CONTENT,
                                  CreateDate = hs.CREATE_DATE,
                                  Type = hs.CONTENT_TYPE,
                                  IsAdmin = hs.IS_ADMIN,
                                  SenderGroup = (hs.SENDER_GROUP.HasValue) ? hs.SENDER_GROUP.Value : 0,
                                  IsSMS = hs.IS_SMS,
                                  SenderName = hs.SENDER_NAME,
                              });
                if (contactGroupID > 0)
                {
                    lstTmp = lstTmp.Where(s => s.ContactGroupID == contactGroupID);
                }
                if (IsAdmin)
                {
                    lstTmp = lstTmp.Where(s => s.IsAdmin == IsAdmin && s.SenderID == 0);
                }
                if (!String.IsNullOrEmpty(teacherName))
                {
                    lstTmp = lstTmp.Where(s => s.SenderName == teacherName.Trim());
                }
                //Group (Query only)
                var lstSMSCommunicationBOTemp = lstTmp.ToList().GroupBy(a => a.SenderGroup).Select(b => new SMSCommunicationBO
                {
                    SenderGroup = b.Key,
                    ContactGroupID = b.FirstOrDefault().ContactGroupID,
                    ReceiverID = b.FirstOrDefault().ReceiverID,
                    SenderID = b.FirstOrDefault().SenderID,
                    TypeHistory = b.FirstOrDefault().TypeHistory,
                    Content = b.FirstOrDefault().Content,
                    ContentCount = b.Where(a => a.IsSMS).Sum(x => x.ContentCount), //Tinh lai tong so SMS da gui (chi lay SMS)
                    ShortContent = b.FirstOrDefault().ShortContent,
                    CreateDate = b.FirstOrDefault().CreateDate,
                    Type = b.FirstOrDefault().Type,
                    TotalReceived = b.Count(),
                    IsAdmin = b.FirstOrDefault().IsAdmin,
                    IsSMS = b.Any(a => a.IsSMS)
                }).OrderByDescending(p => p.CreateDate);
                //Get Total (chi lay cac record gui SMS) (Query only)
                var resultQuery = lstSMSCommunicationBOTemp.Where(a => a.IsSMS == true);
                totalRecord = resultQuery.Count();
                //Load data (20 records)
                lstSMSCommunicationBO = totalRecord > 0 ? resultQuery.Skip((currentPage - 1) * GlobalConstantsEdu.PAGESIZE).Take(numRecord).ToList() : new List<SMSCommunicationBO>();
                #region lay thong tin hoc sinh va giao vien de fill ho va ten khi for
                //Max: 20 records
                List<int> lstSenderID = (from s in lstSMSCommunicationBO
                                         where s.SenderID > 0
                                         select s.SenderID).Distinct().ToList();
                //Max: 20 records
                List<int> lstReceiverID = (from s in lstSMSCommunicationBO
                                           select s.ReceiverID).Distinct().ToList();
                List<int?> lstContractGroupID = lstSMSCommunicationBO.Select(p => p.ContactGroupID).Distinct().ToList();
                List<SMS_TEACHER_CONTACT_GROUP> lstContactGroup = ContactGroupBusiness.All.Where(p => p.SCHOOL_ID == schoolID && lstContractGroupID.Contains(p.CONTACT_GROUP_ID)).ToList();
                SMS_TEACHER_CONTACT_GROUP objContactGroup = null;
                //Max: 20 records
                //**************************************************Goi API GetListPupilByCondition(lstReceiver,schoolID)********************************
                List<PupilOfClassBO> pupiProfileList = PupilProfileBusiness.getListPupilByListPupilID(lstReceiverID, academicYearID).ToList();
                pupiProfileList = pupiProfileList != null ? pupiProfileList : new List<PupilOfClassBO>();
                //***************************************************************************************************************************************
                //Max: 20 records                
                //*************************************************Goi API GetListTeacher(lstSender)*****************************************************
                List<TeacherBO> teacherList = EmployeeBusiness.GetListTeacher(lstSenderID);
                teacherList = teacherList != null ? teacherList : new List<TeacherBO>();
                //**************************************************************************************************************************************
                SMSCommunicationBO objSMSCommunicationBO = null;
                //Max: 20 records
                for (int i = lstSMSCommunicationBO.Count - 1; i >= 0; i--)
                {
                    objSMSCommunicationBO = lstSMSCommunicationBO[i];
                    objSMSCommunicationBO.SenderGroupStringType = objSMSCommunicationBO.SenderGroup.ToString();

                    objSMSCommunicationBO.TeacherSenderName = (from t in teacherList
                                                               where t.TeacherID == objSMSCommunicationBO.SenderID
                                                               select t.FullName).FirstOrDefault();
                    objSMSCommunicationBO.EmployeeShortName = (from t in teacherList
                                                               where t.TeacherID == objSMSCommunicationBO.SenderID
                                                               select t.ShortName).FirstOrDefault();
                    objSMSCommunicationBO.ParentReceiverName = (from pf in pupiProfileList
                                                                where pf.PupilID == objSMSCommunicationBO.ReceiverID
                                                                select pf.PupilFullName).FirstOrDefault();
                    objContactGroup = lstContactGroup.Where(p => p.CONTACT_GROUP_ID == objSMSCommunicationBO.ContactGroupID).FirstOrDefault();
                    if (objContactGroup != null)
                    {
                        objSMSCommunicationBO.ContactGroupName = objContactGroup.CONTACT_GROUP_NAME;
                    }
                }
                #endregion
                #endregion
                return lstSMSCommunicationBO;
            }
        }

        public SummarySMSBO GetSummarySMS(Dictionary<string, object> dic)
        {
            int schoolID = dic.GetInt("schoolID");
            int classID = dic.GetInt("classID");
            int pupilID = dic.GetInt("pupilID");
            int Type = dic.GetInt("Type");
            DateTime? fromDate = dic.GetDateTime("fromDate");
            DateTime? toDate = dic.GetDateTime("toDate");
            SummarySMSBO summary = new SummarySMSBO();

            fromDate = fromDate.Value.Date;
            toDate = toDate.Value.Date;
            if (toDate.HasValue)
            {
                toDate = new DateTime(toDate.Value.Year, toDate.Value.Month, toDate.Value.Day, 23, 59, 59);
            }

            int partitionID = UtilsBusiness.GetPartionId(schoolID);
            IQueryable<SMSCommunicationBO> SummarySMS = (from hs in SMSHistoryBusiness.All
                                                         where fromDate <= hs.CREATE_DATE && hs.CREATE_DATE <= toDate
                                                         && (hs.RECEIVER_ID == pupilID || pupilID == 0)
                                                         && hs.SCHOOL_ID == schoolID
                                                         && hs.PARTITION_ID == partitionID
                                                         && hs.RECEIVE_TYPE == GlobalConstantsEdu.COMMON_HISTORY_RECEIVER_PUPIL_ID
                                                         select new SMSCommunicationBO
                                                         {
                                                             TypeHistory = hs.SMS_TYPE_ID
                                                         });
            //diem ngay 
            summary.DayMark = SummarySMS.Where(p => p.TypeHistory == 11).Count();
            summary.MonthMark = SummarySMS.Where(p => p.TypeHistory == 12).Count();
            summary.WeekMark = SummarySMS.Where(p => p.TypeHistory == 13).Count();
            summary.SemesterResult = SummarySMS.Where(p => p.TypeHistory == 14).Count();

            summary.TrainingDate = SummarySMS.Where(p => p.TypeHistory == 15).Count();
            summary.HealthDate = SummarySMS.Where(p => p.TypeHistory == 16).Count();
            summary.PeriodicHealth = SummarySMS.Where(p => p.TypeHistory == 17).Count();
            summary.ActivitiDate = SummarySMS.Where(p => p.TypeHistory == 18).Count();
            summary.GrowthMonth = SummarySMS.Where(p => p.TypeHistory == 19).Count();
            summary.Exchange = SummarySMS.Where(p => p.TypeHistory == 20).Count();
            summary.Teacher = SummarySMS.Where(p => p.TypeHistory == 1).Count();

            return summary;
        }

        public List<SMSCommunicationBO> GetListNotificationTeacherExecl(Dictionary<string, object> dic)
        {
            int schoolID = dic.GetInt("schoolID");
            int classID = dic.GetInt("classID");
            int Type = dic.GetInt("Type");
            int year = dic.GetInt("year");
            DateTime? fromDate = dic.GetDateTime("fromDate");
            DateTime? toDate = dic.GetDateTime("toDate");
            string teacherName = dic.GetString("teacherID");
            int contactGroupID = dic.GetInt("contactGroupID");
            int pupilID = dic.GetInt("pupilID", 0);
            int level = dic.GetInt("level", 0);
            int levelType = dic.GetInt("levelType", 0);
            int SMSTypeID = dic.GetInt("TypeID", 0);
            int levelID = dic.GetInt("LevelID");// cap hoc    
            bool IsAdmin = dic.GetBool("IsAdmin", false);
            int academicYearID = dic.GetInt("AcademicYearID");
            fromDate = fromDate.Value.Date;
            toDate = toDate.Value;
            toDate = new DateTime(toDate.Value.Year, toDate.Value.Month, toDate.Value.Day, 23, 59, 59);
            List<SMSCommunicationBO> lstSMSCommunicationBO = new List<SMSCommunicationBO>();
            int partitionID = UtilsBusiness.GetPartionId(schoolID);
            var lstTmp = (
                          from hs in SMSHistoryBusiness.All
                          join ctg in ContactGroupBusiness.All on hs.CONTACT_GROUP_ID equals ctg.CONTACT_GROUP_ID
                          where fromDate <= hs.CREATE_DATE && hs.CREATE_DATE <= toDate
                              && hs.SCHOOL_ID == schoolID
                              && hs.PARTITION_ID == partitionID
                              && (hs.RECEIVE_TYPE == GlobalConstantsEdu.COMMON_HISTORY_RECEIVER_TEACHER_ID)
                          select new
                          {
                              ContactGroupID = ctg.CONTACT_GROUP_ID,
                              ContactGroupName = ctg.CONTACT_GROUP_NAME,
                              ReceiverID = hs.RECEIVER_ID,
                              SenderID = hs.SENDER_ID,
                              TypeHistory = hs.SMS_TYPE_ID,
                              Content = hs.SMS_CONTENT,
                              ContentCount = hs.SMS_CNT,
                              ShortContent = hs.SHORT_CONTENT,
                              CreateDate = hs.CREATE_DATE,
                              Type = hs.RECEIVE_TYPE,
                              IsAdmin = hs.IS_ADMIN,
                              SenderGroup = (hs.SENDER_GROUP.HasValue) ? hs.SENDER_GROUP.Value : 0,
                              IsSMS = hs.IS_SMS,
                              SenderName = hs.SENDER_NAME,
                          });
            if (contactGroupID > 0)
            {
                lstTmp = lstTmp.Where(s => s.ContactGroupID == contactGroupID);
            }
            if (IsAdmin)
            {
                lstTmp = lstTmp.Where(s => s.IsAdmin == IsAdmin && s.SenderID == 0);
            }
            if (!String.IsNullOrEmpty(teacherName))
            {
                lstTmp = lstTmp.Where(s => s.SenderName == teacherName.Trim());
            }
            //Group (Query only)
            var lstSMSCommunicationBOTemp = lstTmp.ToList().GroupBy(a => a.SenderGroup).Select(b => new SMSCommunicationBO
            {
                SenderGroup = b.Key,
                //SMSCommunicationID = b.FirstOrDefault().SMSCommunicationID,
                ContactGroupID = b.FirstOrDefault().ContactGroupID,
                ContactGroupName = b.FirstOrDefault().ContactGroupName,
                ReceiverID = b.FirstOrDefault().ReceiverID,
                SenderID = b.FirstOrDefault().SenderID,
                TypeHistory = b.FirstOrDefault().TypeHistory,
                Content = b.FirstOrDefault().Content,
                ContentCount = b.Where(a => a.IsSMS).Sum(x => x.ContentCount), //Tinh lai tong so SMS da gui (chi lay SMS)
                ShortContent = b.FirstOrDefault().ShortContent,
                CreateDate = b.FirstOrDefault().CreateDate,
                Type = b.FirstOrDefault().Type,
                TotalReceived = b.Count(),
                IsAdmin = b.FirstOrDefault().IsAdmin,
                IsSMS = b.Any(a => a.IsSMS)
            }).OrderByDescending(p => p.CreateDate);
            lstSMSCommunicationBO = lstSMSCommunicationBOTemp.Where(a => a.IsSMS == true).ToList();
            List<int> lstSenderID = (from s in lstSMSCommunicationBO
                                     where s.SenderID > 0
                                     select s.SenderID).Distinct().ToList();

            List<int> lstReceiverID = (from s in lstSMSCommunicationBO
                                       select s.ReceiverID).Distinct().ToList();

            List<PupilOfClassBO> pupiProfileList = PupilProfileBusiness.getListPupilByListPupilID(lstReceiverID,academicYearID).ToList();
            pupiProfileList = pupiProfileList != null ? pupiProfileList : new List<PupilOfClassBO>();
            //***************************************************************************************************************************************
            //*************************************************Goi API GetListTeacher(lstSender)*****************************************************
            List<TeacherBO> teacherList = EmployeeBusiness.GetListTeacher(lstSenderID);
            teacherList = teacherList != null ? teacherList : new List<TeacherBO>();
            //**************************************************************************************************************************************
            //****************************GOI API***************************************************************************************************
            List<UserBO> lstAccount = EmployeeBusiness.GetListFullUsernameByEmployeeID(schoolID, lstSenderID);
            lstAccount = lstAccount != null ? lstAccount : new List<UserBO>();
            //***************************************************************************************************************************************
            SMSCommunicationBO objSMSCommunicationBO = null;
            for (int i = lstSMSCommunicationBO.Count - 1; i >= 0; i--)
            {
                objSMSCommunicationBO = lstSMSCommunicationBO[i];
                objSMSCommunicationBO.SenderGroupStringType = objSMSCommunicationBO.SenderGroup.ToString();

                objSMSCommunicationBO.TeacherSenderName = (from t in teacherList
                                                           where t.TeacherID == objSMSCommunicationBO.SenderID
                                                           select t.FullName).FirstOrDefault();
                objSMSCommunicationBO.EmployeeShortName = (from t in teacherList
                                                           where t.TeacherID == objSMSCommunicationBO.SenderID
                                                           select t.ShortName).FirstOrDefault();
                objSMSCommunicationBO.ParentReceiverName = (from pf in pupiProfileList
                                                            where pf.PupilID == objSMSCommunicationBO.ReceiverID
                                                            select pf.PupilFullName).FirstOrDefault();

                objSMSCommunicationBO.UserName = (from pf in lstAccount
                                                  where pf.EmployeeID == objSMSCommunicationBO.SenderID
                                                  select pf.UserName).FirstOrDefault();

            }
            return lstSMSCommunicationBO;
        }

        public List<SMSCommunicationBO> GetListParentContract(Dictionary<string, object> dic)
        {
            DateTime? fromDate = dic.GetDateTime("fromDate");
            DateTime? toDate = dic.GetDateTime("toDate");
            int schoolID = dic.GetInt("schoolID");
            int classID = dic.GetInt("classID");
            int pupilID = dic.GetInt("pupilID");
            int level = dic.GetInt("level", 0);//cap
            int TypeID = dic.GetInt("TypeID", 0);
            int year = dic.GetInt("year", 0);
            int academicYearID = dic.GetInt("AcademicYearID");
            fromDate = fromDate.Value.Date;
            toDate = toDate.Value.Date;
            toDate = new DateTime(toDate.Value.Year, toDate.Value.Month, toDate.Value.Day, 23, 59, 59);
            List<StatisticsOfSentSMSToPupil> contractList = null;
            List<int> pupilProfileIDList = new List<int>();
            if (classID > 0)
            {
                if (pupilID > 0)
                {
                    pupilProfileIDList.Add(pupilID);
                    contractList = this.GetListStatisticsOfSentSMSToPupil(schoolID, pupilProfileIDList, fromDate.Value, toDate.Value, level, TypeID, year).ToList();
                }
                else
                {
                    pupilProfileIDList = PupilProfileBusiness.GetListPupilByClassID(schoolID,academicYearID,classID)
                        .Where(p => p.ProfileStatus != GlobalConstantsEdu.COMMON_PUPIL_STATUS_MOVED_TO_OTHER_CLASS)
                        .Select(p => p.PupilProfileID).Distinct().ToList();
                    contractList = this.GetListStatisticsOfSentSMSToPupil(schoolID, pupilProfileIDList, fromDate.Value, toDate.Value, level, TypeID, year).ToList();
                }
            }
            else
            {
                IDictionary<string, object> dicPupil = new Dictionary<string, object>()
                {
                    {"SchoolID",schoolID},
                    {"Appliedlevel",level},
                    {"AcademicYearID",academicYearID}
                };
                pupilProfileIDList = PupilProfileBusiness.GetListPupilProfile(dicPupil).Select(p => p.PupilProfileID).Distinct().ToList();
                contractList = this.GetListStatisticsOfSentSMSToPupil(schoolID, pupilProfileIDList, fromDate.Value, toDate.Value, level, TypeID, year).ToList();
            }

            //List pupilprofileID
            List<int> pupilProFileIDList = contractList.Select(p => p.PupilID).ToList();

            //*******************************Goi API************************************************************
            List<PupilOfClassBO> pupilListByIDList = PupilProfileBusiness.getListPupilByListPupilID(pupilProFileIDList, academicYearID).ToList();
            //*****************************************************************************************************

            List<SMSCommunicationBO> result = (from pf in pupilListByIDList
                                               join ctr in contractList on pf.PupilID equals ctr.PupilID
                                               orderby pf.ClassName, pf.PupilFullName
                                               select new SMSCommunicationBO
                                               {
                                                   PupilID = pf.PupilID,
                                                   PupilFileName = pf.PupilFullName,
                                                   ClassName = pf.ClassName,
                                                   ContractorName = ctr.ContractorName,
                                                   SMSParentContractID = ctr.SMSParentContractID,
                                                   PhoneMainContract = ctr.ReceiverPhoneNumber,
                                                   TotalActive = ctr.TotalActive,
                                                   TotalManual = ctr.TotalManual,
                                                   TotalParent = 0,
                                                   ClassID = pf.CurrentClassId,
                                                   EducaitonLevelID = pf.EducationLevelID,
                                                   OrderInClass = pf.OrderInClass.GetValueOrDefault(),
                                                   EmployeeShortName = pf.Name
                                               }).OrderBy(p => p.EducaitonLevelID).ThenBy(p => p.OrderInClass).ThenBy(p => p.ClassName).ToList();
            return result;
        }
        private List<StatisticsOfSentSMSToPupil> GetListStatisticsOfSentSMSToPupil(int schoolID, List<int> lstPupilID,DateTime fromDate,DateTime toDate,int level,int TypeID,int year)
        {
            int partitionID = UtilsBusiness.GetPartionId(schoolID);
            List<StatisticsOfSentSMSToPupil> lstStatisticsOfSentSMSToPupil = new List<StatisticsOfSentSMSToPupil>();
            StatisticsOfSentSMSToPupil objStatisticsOfSentSMSToPupil = null;
            lstStatisticsOfSentSMSToPupil = (from smspc in SMSParentContractBusiness.All
                                             join smspcd in SMSParentContractDetailBusiness.All on smspc.SMS_PARENT_CONTRACT_ID equals smspcd.SMS_PARENT_CONTRACT_ID
                                             join smsh in SMSHistoryBusiness.All on smspcd.SMS_PARENT_CONTRACT_DETAIL_ID equals smsh.SMS_PARENTCONTRACT_DETAIL_ID
                                             where smspc.SCHOOL_ID == schoolID
                                             && smspc.PARTITION_ID == partitionID
                                             && smsh.SCHOOL_ID == schoolID
                                             && smsh.PARTITION_ID == partitionID
                                             && smspc.STATUS == 1
                                             && smspcd.IS_ACTIVE == true
                                             && (smspcd.SUBSCRIPTION_STATUS == 1 || smspcd.SUBSCRIPTION_STATUS == 5)
                                             && smspcd.YEAR_ID == year
                                             && smsh.CREATE_DATE >= fromDate
                                             && smsh.CREATE_DATE <= toDate
                                             && (smsh.SMS_TYPE_ID == TypeID || TypeID == 0)
                                             && lstPupilID.Contains(smspc.PUPIL_ID)
                                             //select new
                                             //{
                                             //    PupilID = smspc.PUPIL_ID,
                                             //    ContractorName = smspc.CONTRACTOR_NAME,
                                             //    SMSParentContractID = smspc.SMS_PARENT_CONTRACT_ID,
                                             //    IsAuto = smsh.IS_AUTO,
                                             //    Status = smsh.STATUS,
                                             //    SMS_CNT = smsh.SMS_CNT
                                             //}).GroupBy(g => new { g.PupilID, g.ContractorName, g.SMSParentContractID })
                                             //.Select(p => new
                                             //  {
                                             //      PupilID = p.Key.PupilID,
                                             //      ContractorName = p.Key.ContractorName,
                                             //      SMSParentContractID = p.Key.SMSParentContractID,
                                             //      TotalActive = p.Sum(x => (x.IsAuto == true && x.Status == true) ? x.SMS_CNT : 0),
                                             //      TotalManual = p.Sum(x => (x.IsAuto == false && x.Status == true) ? x.SMS_CNT : 0),
                                             //  }).ToList();
                                             group smsh by new
                                             {
                                                 PupilID = smspc.PUPIL_ID,
                                                 ContractorName = smspc.CONTRACTOR_NAME,
                                                 SMSParentContractID = smspc.SMS_PARENT_CONTRACT_ID
                                             } into g
                                             select new StatisticsOfSentSMSToPupil
                                             {
                                                 PupilID = g.Key.PupilID,
                                                 ContractorName = g.Key.ContractorName,
                                                 SMSParentContractID = g.Key.SMSParentContractID,
                                                 TotalActive = g.Sum(p => (p.IS_AUTO == true && p.STATUS == true) ? p.SMS_CNT : 0),
                                                 TotalManual = g.Sum(p => (p.IS_AUTO == false && p.STATUS == true) ? p.SMS_CNT : 0)
                                             }).ToList();
            //for (int i = 0; i < iquery.Count; i++)
            //{
            //    var objQuery = iquery[i];
            //    objStatisticsOfSentSMSToPupil = new StatisticsOfSentSMSToPupil();
            //    objStatisticsOfSentSMSToPupil.PupilID = objQuery.PupilID;
            //    objStatisticsOfSentSMSToPupil.ContractorName = objQuery.ContractorName;
            //    objStatisticsOfSentSMSToPupil.SMSParentContractID = objQuery.SMSParentContractID;
            //    objStatisticsOfSentSMSToPupil.TotalActive = objQuery.TotalActive;
            //    objStatisticsOfSentSMSToPupil.TotalManual = objQuery.TotalManual;
            //    lstStatisticsOfSentSMSToPupil.Add(objStatisticsOfSentSMSToPupil);
            //}
            List<int> lstPupilContract = lstStatisticsOfSentSMSToPupil.Select(p => p.PupilID).Distinct().ToList();
            List<Receiver> lstReceiver = (from smspc in SMSParentContractBusiness.All
                                          join smspcd in SMSParentContractDetailBusiness.All on smspc.SMS_PARENT_CONTRACT_ID equals smspcd.SMS_PARENT_CONTRACT_ID
                                          where smspc.SCHOOL_ID == schoolID
                                          && smspc.PARTITION_ID == partitionID
                                          && smspcd.PARTITION_ID == partitionID
                                          && lstPupilContract.Contains(smspc.PUPIL_ID)
                                          select new Receiver
                                          {
                                              PupilID = smspc.PUPIL_ID,
                                              SMSParentContractID = smspc.SMS_PARENT_CONTRACT_ID,
                                              SchoolID = smspc.SCHOOL_ID,
                                              year = smspcd.YEAR_ID,
                                              PhoneNumber = smspcd.SUBSCRIBER
                                          }).ToList();
            List<string> lsttmp = new List<string>();
            for (int i = 0; i < lstStatisticsOfSentSMSToPupil.Count; i++)
            {
                objStatisticsOfSentSMSToPupil = lstStatisticsOfSentSMSToPupil[i];
                lsttmp = lstReceiver.Where(p => p.PupilID == objStatisticsOfSentSMSToPupil.PupilID && p.SMSParentContractID == objStatisticsOfSentSMSToPupil.SMSParentContractID).Select(p => p.PhoneNumber).Distinct().ToList();
                objStatisticsOfSentSMSToPupil.ReceiverPhoneNumber = string.Join(",", lsttmp);
            }
            return lstStatisticsOfSentSMSToPupil;
        }
        private List<SMSHistoryBO> SearchParentSMS(int schoolID,int AcademicYearID,int AppliedLevel,int ClassID,int PupilID,string teacherName,int year,int SMSTypeID,int isCustomType,DateTime fromDate,DateTime toDate,int currentPage,ref int TotalRecord,int pagesize = GlobalConstantsEdu.PAGESIZE)
        {
            int partitionID = UtilsBusiness.GetPartionId(schoolID);
            List<SMSHistoryBO> lstResult = new List<SMSHistoryBO>();
            SMSHistoryBO objResult = null;
            //Ban tin thuong
            IQueryable<SMSHistoryBO> iqueryType = (from h in SMSHistoryBusiness.All
                                                   join poc in PupilOfClassBusiness.All on h.RECEIVER_ID equals poc.PupilID
                                                   join s in SMSTypeBusiness.All on h.SMS_TYPE_ID equals s.SMS_TYPE_ID
                                                   where h.SCHOOL_ID == schoolID
                                                   && h.PARTITION_ID == partitionID
                                                   && EntityFunctions.TruncateTime(fromDate) <= EntityFunctions.TruncateTime(h.CREATE_DATE)
                                                   && EntityFunctions.TruncateTime(h.CREATE_DATE) <= EntityFunctions.TruncateTime(toDate)
                                                   && h.RECEIVE_TYPE == 1
                                                   && h.IS_SMS == true
                                                   && h.YEAR == year
                                                   && h.IS_CUSTOM_TYPE == false
                                                   && poc.SchoolID == schoolID
                                                   && poc.AcademicYearID == AcademicYearID
                                                   && poc.ClassProfile.EducationLevel.Grade == AppliedLevel
                                                   && poc.Status == GlobalConstants.PUPIL_STATUS_STUDYING
                                                   select new
                                                   {
                                                       ClassID = poc.ClassID,
                                                       IsAdmin = h.IS_ADMIN,
                                                       SenderGroup = h.SENDER_GROUP,
                                                       CreateDate = h.CREATE_DATE,
                                                       SMSContent = h.SMS_CONTENT,
                                                       SMSCNT = h.SMS_CNT,
                                                       TypeName = s.SMS_TYPE_NAME,
                                                       TypeID = s.SMS_TYPE_ID,
                                                       ReceiverID = h.RECEIVER_ID,
                                                       SenderID = h.SENDER_ID,
                                                       SenderName = h.SENDER_NAME,
                                                       Status = h.STATUS,
                                                       IsOnSchedule = h.IS_ON_SCHEDULE,
                                                       SendTime = h.SEND_TIME,
                                                       IsCustomType = h.IS_CUSTOM_TYPE
                                                   }).GroupBy(p => new
                                                   {
                                                       p.ClassID,
                                                       p.IsAdmin,
                                                       p.SenderGroup,
                                                       p.CreateDate,
                                                       p.SMSContent,
                                                       p.SMSCNT,
                                                       p.TypeName,
                                                       p.TypeID,
                                                       p.ReceiverID,
                                                       p.SenderID,
                                                       p.Status,
                                                       p.IsOnSchedule,
                                                       p.SendTime,
                                                       p.IsCustomType
                                                   }).Select(p => new SMSHistoryBO
                                                   {
                                                       ClassID = p.Key.ClassID,
                                                       IsAdmin = p.Key.IsAdmin,
                                                       SenderGroup = p.Key.SenderGroup,
                                                       CreateDate = p.Key.CreateDate,
                                                       SMSContent = p.Key.SMSContent,
                                                       SMSCNT = p.Key.SMSCNT,
                                                       TypeName = p.Key.TypeName,
                                                       TypeID = p.Key.TypeID,
                                                       ReceiverID = p.Key.ReceiverID,
                                                       SenderID = p.Key.SenderID,
                                                       Status = p.Key.Status,
                                                       IsOnSchedule = p.Key.IsOnSchedule,
                                                       SendTime = p.Key.SendTime,
                                                       IsCustomType = p.Key.IsCustomType
                                                   });
            if (SMSTypeID > 0)
            {
                iqueryType = iqueryType.Where(p => p.TypeID == SMSTypeID);
            }
            if (ClassID > 0)
            {
                iqueryType = iqueryType.Where(p => p.ClassID == ClassID);
            }
            if (PupilID > 0)
            {
                iqueryType = iqueryType.Where(p => p.ReceiverID == PupilID);
            }
            if (!string.IsNullOrEmpty(teacherName))
            {
                iqueryType = iqueryType.Where(p => p.SenderName == teacherName);
            }

            //Ban tin tu dinh nghia
            IQueryable<SMSHistoryBO> iqueryCustomType = (from h in SMSHistoryBusiness.All
                                                         join poc in PupilOfClassBusiness.All on h.RECEIVER_ID equals poc.PupilID
                                                         join s in CustomSMSTypeBusiness.All on h.SMS_TYPE_ID equals s.CUSTOM_SMS_TYPE_ID
                                                         where h.SCHOOL_ID == schoolID
                                                         && h.PARTITION_ID == partitionID
                                                         && EntityFunctions.TruncateTime(fromDate) <= EntityFunctions.TruncateTime(h.CREATE_DATE)
                                                         && EntityFunctions.TruncateTime(h.CREATE_DATE) <= EntityFunctions.TruncateTime(toDate)
                                                         && h.RECEIVE_TYPE == 1
                                                         && h.IS_SMS == true
                                                         && h.YEAR == year
                                                         && h.IS_CUSTOM_TYPE == true
                                                         && poc.SchoolID == schoolID
                                                         && poc.AcademicYearID == AcademicYearID
                                                         && poc.ClassProfile.EducationLevel.Grade == AppliedLevel
                                                         && poc.Status == GlobalConstants.PUPIL_STATUS_STUDYING
                                                         select new
                                                         {
                                                             ClassID = poc.ClassID,
                                                             IsAdmin = h.IS_ADMIN,
                                                             SenderGroup = h.SENDER_GROUP,
                                                             CreateDate = h.CREATE_DATE,
                                                             SMSContent = h.SMS_CONTENT,
                                                             SMSCNT = h.SMS_CNT,
                                                             TypeName = s.TYPE_NAME,
                                                             TypeID = s.CUSTOM_SMS_TYPE_ID,
                                                             ReceiverID = h.RECEIVER_ID,
                                                             SenderID = h.SENDER_ID,
                                                             SenderName = h.SENDER_NAME,
                                                             Status = h.STATUS,
                                                             IsOnSchedule = h.IS_ON_SCHEDULE,
                                                             SendTime = h.SEND_TIME,
                                                             IsCustomType = h.IS_CUSTOM_TYPE
                                                         }).GroupBy(p => new
                                                         {
                                                             p.ClassID,
                                                             p.IsAdmin,
                                                             p.SenderGroup,
                                                             p.CreateDate,
                                                             p.SMSContent,
                                                             p.SMSCNT,
                                                             p.TypeName,
                                                             p.TypeID,
                                                             p.ReceiverID,
                                                             p.SenderID,
                                                             p.Status,
                                                             p.IsOnSchedule,
                                                             p.SendTime,
                                                             p.IsCustomType
                                                         }).Select(p => new SMSHistoryBO
                                                         {
                                                             ClassID = p.Key.ClassID,
                                                             IsAdmin = p.Key.IsAdmin,
                                                             SenderGroup = p.Key.SenderGroup,
                                                             CreateDate = p.Key.CreateDate,
                                                             SMSContent = p.Key.SMSContent,
                                                             SMSCNT = p.Key.SMSCNT,
                                                             TypeName = p.Key.TypeName,
                                                             TypeID = p.Key.TypeID,
                                                             ReceiverID = p.Key.ReceiverID,
                                                             SenderID = p.Key.SenderID,
                                                             Status = p.Key.Status,
                                                             IsOnSchedule = p.Key.IsOnSchedule,
                                                             SendTime = p.Key.SendTime,
                                                             IsCustomType = p.Key.IsCustomType
                                                         });
            if (SMSTypeID > 0)
            {
                iqueryCustomType = iqueryCustomType.Where(p => p.TypeID == SMSTypeID);
            }
            if (ClassID > 0)
            {
                iqueryCustomType = iqueryCustomType.Where(p => p.ClassID == ClassID);
            }
            if (PupilID > 0)
            {
                iqueryCustomType = iqueryCustomType.Where(p => p.ReceiverID == PupilID);
            }
            if (!string.IsNullOrEmpty(teacherName))
            {
                iqueryCustomType = iqueryCustomType.Where(p => p.SenderName == teacherName);
            }

            var iqueryAll = iqueryType.Union(iqueryCustomType);

            if (isCustomType == 0)
            {
                iqueryAll = iqueryAll.Where(p => p.IsCustomType == false);
            }
            else if (isCustomType == 1)
            {
                iqueryAll = iqueryAll.Where(p => p.IsCustomType == true);
            }

            TotalRecord = iqueryAll.Count();
            lstResult = iqueryAll.OrderByDescending(p => p.CreateDate).Skip((currentPage - 1) * pagesize).Take(pagesize).ToList();
            List<int> lstReceiverID = lstResult.Select(p=>p.ReceiverID).Distinct().ToList();
            List<long?> lstSenderGroupID = lstResult.Select(p=>p.SenderGroup).Distinct().ToList();

            var lstSubscriber = (from h in SMSHistoryBusiness.All
                                 where h.SCHOOL_ID == schoolID
                                 && h.PARTITION_ID == partitionID
                                 && lstSenderGroupID.Contains(h.SENDER_GROUP)
                                 && lstReceiverID.Contains(h.RECEIVER_ID)
                                 && h.YEAR == year
                                 && h.RECEIVE_TYPE == 1
                                 select h).ToList();
            List<string> lstMobile = new List<string>();
            for (int i = 0; i < lstResult.Count; i++)
            {
                objResult = lstResult[i];
                lstMobile = lstSubscriber.Where(p => p.RECEIVER_ID == objResult.ReceiverID && p.SENDER_GROUP == objResult.SenderGroup)
                    .Select(p => p.MOBILE).Distinct().ToList();
                objResult.Subscriber = lstMobile.Count > 0 ? string.Join(",", lstMobile) : "";
            }
            return lstResult;
        }

        public List<SMSCommunicationBO> GetListSMSForReport(int schoolId)
        {
            int partitionSMSHistory = UtilsBusiness.GetPartionId(schoolId);
            //int partitionSMSParentContract = UtilsBusiness.GetPartitionSMSParentContract(schoolId);
            List<SMSCommunicationBO> q = (from hs in this.All
                                          where hs.PARTITION_ID == partitionSMSHistory
                                          && hs.SCHOOL_ID == schoolId
                                          && hs.IS_ADMIN == false
                                          && hs.SENDER_ID != 0
                                          && (hs.RECEIVE_TYPE == GlobalConstantsEdu.COMMON_HISTORY_RECEIVER_PARENT_ID ||
                                          ((hs.RECEIVE_TYPE == GlobalConstantsEdu.COMMON_HISTORY_RECEIVER_TEACHER_ID || hs.RECEIVE_TYPE == GlobalConstantsEdu.COMMON_HISTORY_RECEIVER_HEAD_MASTER_ID) && hs.RECEIVER_ID != hs.SENDER_ID))

                                          select new SMSCommunicationBO
                                          {
                                              CreateDate = hs.CREATE_DATE,
                                              SenderID = hs.SENDER_ID,
                                              Type = hs.RECEIVE_TYPE,
                                              ReceiverID = hs.RECEIVER_ID,
                                              SmsCount = hs.SMS_CNT
                                          }).ToList();
            return q;
        }

        public SMSEduResultBO SendSMSFromSuperUser(Dictionary<string, object> dic)
        {
            DateTime eventDate = DateTime.Now;
            int? superVisingDeptID = dic.GetInt("unitID");
            SMSEduResultBO objResultBO = new SMSEduResultBO();
            try
            {
                SetAutoDetectChangesEnabled(false);
                List<int> lstReceiver = Utils.GetIntList(dic, "lstReceiver");
                DateTime dateTimeNow = DateTime.Now;
                int month = dateTimeNow.Month;
                int year = dateTimeNow.Year;
                PromotionProgramBO promotion = Utils.GetObject<PromotionProgramBO>(dic, "Promotion");

                #region check permission
                SupervisingDept objSuperUnit = null;
                
                int? SMSActive = 0;
                int? unitIDSMSActive = 0;
                bool isToSchool = ExtensionMethods.GetBool(dic, "isToSchool", false);
                int? educationLevel = Utils.GetNullableInt(dic, "EducationLevel");
                int? SelectedUnitID = Utils.GetNullableInt(dic, "SelectedUnitID");
                int? ProvinceID = Utils.GetNullableInt(dic, "ProvinceID");
                int? EmployeeID = Utils.GetNullableInt(dic, "EmployeeID");

                //neu la phong ban don vi thi lay smsactive cua cha la cap phong/so gan nhat
                if (dic.Keys.Contains(GlobalConstantsEdu.COMMON_SUPERVISINGDEPT) && dic[GlobalConstantsEdu.COMMON_SUPERVISINGDEPT] != null)
                {
                    objSuperUnit = (SupervisingDept)dic[GlobalConstantsEdu.COMMON_SUPERVISINGDEPT];
                    superVisingDeptID = objSuperUnit.SupervisingDeptID;
                    if (objSuperUnit.HierachyLevel == GlobalConstantsEdu.COMMON_SUPERVISING_DEPT_HIERACHY_LEVEL_PROVINCE_OFFICE_DEPARTMENT ||
                        objSuperUnit.HierachyLevel == GlobalConstantsEdu.COMMON_SUPERVISING_DEPT_HIERACHY_LEVEL_DISTRICT_OFFICE_DEPARTMENT)
                    {
                        superVisingDeptID = objSuperUnit.ParentID;
                    }

                    if (objSuperUnit.HierachyLevel == GlobalConstantsEdu.COMMON_SUPERVISING_DEPT_HIERACHY_LEVEL_MINISTRY_DEPARTMENT)
                    {
                        if (dic.Keys.Contains(GlobalConstantsEdu.COMMON_PARENTUNIT) && dic[GlobalConstantsEdu.COMMON_PARENTUNIT] != null)
                        {
                            SupervisingDept objSuperUnitTmp = (SupervisingDept)dic[GlobalConstantsEdu.COMMON_PARENTUNIT];
                            SMSActive = objSuperUnitTmp.SMSActiveType;
                            unitIDSMSActive = objSuperUnitTmp.SupervisingDeptID;
                            if (isToSchool)
                            {
                                objSuperUnit = objSuperUnitTmp;
                            }
                        }
                        else
                        {
                            objResultBO.isError = true;
                            objResultBO.ErrorMsg = "SendSMS_Label_CoreError";
                            return objResultBO;
                        }
                    }
                    else
                    {
                        SMSActive = objSuperUnit.SMSActiveType;
                        unitIDSMSActive = superVisingDeptID;
                    }
                }

                if (objSuperUnit == null)
                {
                    objResultBO.isError = true;
                    objResultBO.ErrorMsg = "SendSMS_Label_CoreError";
                    return objResultBO;
                }

                string content = ExtensionMethods.GetString(dic, "content");
                bool isSignMsg = dic.GetBool("isSignMsg");
                int countSMS = content.countNumSMS(isSignMsg);

                if (SMSActive.HasValue == false || SMSActive.Value == GlobalConstantsEdu.COMMON_SCHOOL_SMS_ACTIVE_NOT_USE)
                {
                    objResultBO.isError = true;
                    objResultBO.ErrorMsg = "SendSMSSuperUser_Label_NoActiveSMS";
                    return objResultBO;
                }

                #endregion

                string shortContent = dic.GetString("shortContent");
                string superUserName = dic.GetString("superUserName");
                if (content.CheckContentSMS())
                {
                    objResultBO.isError = true;
                    objResultBO.ErrorMsg = "Nội dung thư không hợp lệ. Vui lòng kiểm tra lại nội dung thư!";
                    return objResultBO;
                }

                if (isToSchool)
                {
                    #region gui tin nhan cho hieu truong truong

                    SMSSupervisingDeptSchoolBO objInputSMS = new SMSSupervisingDeptSchoolBO
                    {
                        Content = content,
                        CountSMS = countSMS,
                        IsSignMsg = isSignMsg,
                        ListReceiverId = lstReceiver,
                        Promotion = promotion,
                        ShortContent = shortContent,
                        SuperUserName = superUserName,
                        SuperVisingDeptId = superVisingDeptID.GetValueOrDefault(),
                        SuperVisingDepts = objSuperUnit,
                        SenderId = objSuperUnit.SupervisingDeptID,
                        EducationLevel = educationLevel.GetValueOrDefault()
                    };
                     objResultBO = SendSmstoSchool(objInputSMS);

                    #endregion
                }
                else
                {
                    #region gui tin nhan cho nhan vien phong so

                    SMSSupervisingDeptEmployeeBO objInputSMS = new SMSSupervisingDeptEmployeeBO
                    {
                        Content = content,
                        CountSMS = countSMS,
                        IsSignMsg = isSignMsg,
                        ListReceiverId = lstReceiver,
                        Promotion = promotion,
                        ShortContent = shortContent,
                        SuperUserName = superUserName,
                        SuperVisingDeptId = superVisingDeptID.GetValueOrDefault(),
                        SuperVisingDepts = objSuperUnit,
                        SenderId = objSuperUnit.SupervisingDeptID,
                        SelectedUnitID = SelectedUnitID.GetValueOrDefault(),
                        ProvinceID = ProvinceID.GetValueOrDefault(),
                        EmployeeID = EmployeeID.GetValueOrDefault()

                    };
                    objResultBO = SendSmstoEmployee(objInputSMS);
                    #endregion
                }

                return objResultBO;
            }
            catch (Exception ex)
            {
                objResultBO.isError = true;
                objResultBO.ErrorMsg = "SendSMS_Label_CoreError";
                logger.Error(ex.Message,ex);
                LogExtensions.ErrorExt(logger, eventDate, "SendSMSFromSuperUser", "UnitId=" + superVisingDeptID, ex);
                return objResultBO;
            }
            finally
            {
                SetAutoDetectChangesEnabled(false);
            }
        }

        /// <summary>
        /// Gui tin nhan tu phong/so de hieu truong truong
        /// </summary>
        /// <param name="inputSMS"></param>
        /// <returns></returns>
        public SMSEduResultBO SendSmstoSchool(SMSSupervisingDeptSchoolBO inputSMS)
        {
            SMSEduResultBO resultBO = new SMSEduResultBO();
            try
            {
                SetAutoDetectChangesEnabled(false);
                string contentStripVNSign = Utils.ConvertLineToSpace(inputSMS.Content.StripVNSign().StripVNSignComposed());
                bool isInsertSentRecord = false;
                DateTime dateTimeNow = DateTime.Now;
                decimal priceSendSMS = 0;
                int InternalSendSMS = 0;
                int ExternalSendSMS = 0;

                #region gui tin nhan cho hieu truong truong

                //Gọi API GetListSchoolByListReceiver
                //danh sach nguoi nhan
                List<HistoryReceiverBO> lstSchoolBO;
                if (inputSMS.ListReceiverId != null && inputSMS.ListReceiverId.Count > 0)
                {
                    lstSchoolBO = SchoolProfileBusiness.GetListSchoolByListID(inputSMS.SuperVisingDeptId, inputSMS.ListReceiverId);
                }
                else
                {
                    List<int> lstIds = SchoolProfileBusiness.GetSchoolByRegionNotPaging(inputSMS.EducationLevel, inputSMS.SuperVisingDeptId, string.Empty).Select(o => o.SchoolID).ToList();
                    lstSchoolBO = SchoolProfileBusiness.GetListSchoolByListID(inputSMS.SuperVisingDeptId, lstIds);
                }
                if (lstSchoolBO == null || lstSchoolBO.Count == 0)
                {
                    resultBO.isError = true;
                    resultBO.ErrorMsg = "SendSMS_Label_CoreError";
                    return resultBO;
                }

                SMS_SENT_RECORD objSentRecord = (from sr in this.SentRecordBusiness.All
                                            where sr.SENT_YEAR == dateTimeNow.Year
                                            && sr.SENT_MONTH == dateTimeNow.Month
                                            && (sr.SUPER_VISING_DEPT_ID == inputSMS.SuperVisingDeptId)
                                            select sr).FirstOrDefault();
                if (objSentRecord == null)
                {
                    objSentRecord = new SMS_SENT_RECORD();
                    objSentRecord.SENT_DATE = dateTimeNow;
                    objSentRecord.SENT_MONTH = dateTimeNow.Month;
                    objSentRecord.SENT_YEAR = dateTimeNow.Year;
                    objSentRecord.SUPER_VISING_DEPT_ID = inputSMS.SuperVisingDeptId;
                    objSentRecord.PARTITION_ID = UtilsBusiness.GetPartionId(inputSMS.SuperVisingDeptId);
                    objSentRecord.SENT_COUNT = 0;
                    isInsertSentRecord = true;
                }


                SMS_HISTORY objHistorySMS = null;
                SMS_MT objMT = null;
                HistoryReceiverBO objSchoolBO = null;
                SMS_RECEIVED_RECORD recordReceived = null;
                List<BrandNameUsingBO> lstBrandName = BrandnameRegistrationBusiness.GetBrandNameUsing(inputSMS.SuperVisingDeptId);
                #region Han muc tin nhan hang thang
                List<int> LstSchoolID = lstSchoolBO.Select(o => o.ReceiverID).ToList();

                Dictionary<string, object> dicReceiver = new Dictionary<string, object>();
                dicReceiver.Add("Type", GlobalConstantsEdu.COMMON_HISTORY_RECEIVER_HEAD_MASTER_ID);
                dicReceiver.Add("dateTimeNow", dateTimeNow);
                dicReceiver.Add("lstSchoolID", LstSchoolID);
                List<SMS_RECEIVED_RECORD> LstReceivedRecord = ReceivedRecordBusiness.GetListReceiRecord(dicReceiver).ToList();
                #endregion
                string brandName = GlobalConstantsEdu.BRANDNAME_DEFAULT;
                byte contentType = inputSMS.IsSignMsg ? GlobalConstantsEdu.SMS_CONTENT_TYPE_UNICODE : GlobalConstantsEdu.SMS_CONTENT_TYPE_NOT_SIGN;
                int partitionId = UtilsBusiness.GetPartionId(inputSMS.SuperVisingDeptId);
                for (int i = lstSchoolBO.Count - 1; i >= 0; i--)
                {
                    objSchoolBO = lstSchoolBO[i];
                    // Chỉ cho phép gửi tin có dấu với các đầu số cho trước
                    if (inputSMS.IsSignMsg && !objSchoolBO.Mobile.isAllowSendSignSMSPhone()) continue;
                    if (!objSchoolBO.Mobile.CheckMobileNumber()) continue;
                    brandName = GetBrandNamebyMobile(objSchoolBO.Mobile, lstPreNumberTelco, lstBrandName);

                    //insert bang history
                    objHistorySMS = new SMS_HISTORY
                    {
                        MOBILE = objSchoolBO.Mobile,
                        IS_INTERNAL = objSchoolBO.Mobile.CheckMobileNumberVT(),
                        RECEIVER_NAME = objSchoolBO.ReceiverName,
                        SMS_TYPE_ID = GlobalConstantsEdu.COMMON_HISTORYSMS_TO_SCHOOL_ID,
                        RECEIVER_ID = objSchoolBO.ReceiverID,
                        CREATE_DATE = dateTimeNow,
                        SCHOOL_ID = objSchoolBO.ReceiverID,
                        YEAR = dateTimeNow.Year,
                        SMS_CONTENT = inputSMS.Content,
                        SENDER_NAME = inputSMS.SuperUserName,
                        SMS_CNT = inputSMS.CountSMS,
                        SEND_TIME = dateTimeNow,
                        SHORT_CONTENT = inputSMS.ShortContent,
                        RECEIVER_UNIT_NAME = objSchoolBO.ReceiverUnitName,
                        RECEIVE_TYPE = GlobalConstantsEdu.COMMON_HISTORY_RECEIVER_HEAD_MASTER_ID,
                        CONTENT_TYPE = contentType,
                        IS_SMS = true,
                        SENDER_UNIT_ID = inputSMS.SuperVisingDeptId,
                        SCHOOL_USERNAME = " ",
                        PARTITION_ID = partitionId,
                        SERVICE_ID = brandName,
                        IS_ADMIN = inputSMS.isAdmin,
                        SENDER_ID = inputSMS.SenderId,
                        HISTORY_RAW_ID = Guid.NewGuid()

                    };
                    this.Insert(objHistorySMS);
                    // ReceivedRecord
                    recordReceived = LstReceivedRecord.FirstOrDefault(o => o.SCHOOL_ID == objSchoolBO.ReceiverID);
                    // Chi luu thong tin GV co so noi mang
                    if (recordReceived == null)
                    {
                        recordReceived = new SMS_RECEIVED_RECORD();
                        recordReceived.SCHOOL_ID = objSchoolBO.ReceiverID;
                        //recordReceived.SupervisingDeptID = inputSMS.SuperVisingDeptId;
                        recordReceived.RECEIVED_TYPE = GlobalConstantsEdu.COMMON_HISTORY_RECEIVER_HEAD_MASTER_ID;
                        recordReceived.RECEIVED_YEAR = dateTimeNow.Year;
                        recordReceived.RECEIVED_MONTH = dateTimeNow.Month;
                        recordReceived.TOTAL_RECEIVED_SMS = 0;
                        recordReceived.CREATED_TIME = dateTimeNow;
                        recordReceived.TOTAL_INTERNAL_SMS = 0;
                        recordReceived.TOTAL_EXTERNAL_SMS = 0;
                        recordReceived.PARTITION_ID = partitionId;
                        this.ReceivedRecordBusiness.Insert(recordReceived);
                    }

                    //insert MT
                    objMT = new SMS_MT();
                    objMT.REQUEST_ID = Guid.NewGuid();
                    objMT.HISTORY_RAW_ID = objHistorySMS.HISTORY_RAW_ID;
                    if (inputSMS.IsSignMsg)
                    {
                        objMT.CONTENT_TYPE = GlobalConstantsEdu.SMS_CONTENT_TYPE_UNICODE.ToString();  // có dấu
                        objMT.SMS_CONTENT = inputSMS.Content;
                    }
                    else
                    {
                        objMT.CONTENT_TYPE = GlobalConstantsEdu.SMS_CONTENT_TYPE_NOT_SIGN.ToString();  // ko dấu
                        objMT.SMS_CONTENT = contentStripVNSign;
                    }
                    objMT.STATUS = GlobalConstants.MT_STATUS_NOT_SEND;
                    objMT.TIME_SEND_REQUEST = dateTimeNow;
                    objMT.USER_ID = objHistorySMS.MOBILE;
                    objMT.SERVICE_ID = brandName;
                    objMT.UNIT_ID = inputSMS.SuperVisingDeptId;
                    this.MTBusiness.Insert(objMT);

                    PriceSendSMSBO objPriceSMS = CalculateSMSPromotion(objHistorySMS, inputSMS.Promotion, inputSMS.CountSMS, recordReceived.TOTAL_RECEIVED_SMS, recordReceived.TOTAL_INTERNAL_SMS, recordReceived.TOTAL_EXTERNAL_SMS);
                    if (objPriceSMS != null)
                    {
                        priceSendSMS += objPriceSMS.PriceSendSMS;
                        InternalSendSMS += objPriceSMS.InternalSendSMS;
                        ExternalSendSMS += objPriceSMS.ExternalSendSMS;
                        recordReceived.TOTAL_RECEIVED_SMS = objPriceSMS.TotalReceivedSMS;
                        recordReceived.TOTAL_INTERNAL_SMS = objPriceSMS.TotalInternalSMS;
                        recordReceived.TOTAL_EXTERNAL_SMS = objPriceSMS.TotalExternalSMS;
                    }

                    //Cap nhat tin nha da nhan
                    if (recordReceived.SMS_RECEIVED_RECORD_ID > 0)
                    {
                        recordReceived.UPDATED_TIME = DateTime.Now;
                        if (recordReceived.SUPER_VISING_DEPT_ID == 0)
                        {
                            recordReceived.SUPER_VISING_DEPT_ID = inputSMS.SuperVisingDeptId;
                        }
                        this.ReceivedRecordBusiness.Update(recordReceived);
                    }


                    //cap nhat lai so tin nhan da gui
                    if (objSentRecord != null)
                    {
                        objSentRecord.SENT_COUNT += inputSMS.CountSMS;
                    }
                    resultBO.NumSendSuccess++;

                }
                if (objSentRecord != null)
                {
                    //cap nhat lai sendTime
                    objSentRecord.SENT_TIME++;
                    if (isInsertSentRecord)
                    {
                        this.SentRecordBusiness.Insert(objSentRecord);
                    }
                    else
                    {
                        this.SentRecordBusiness.Update(objSentRecord);
                    }
                }
                #endregion

                #region  Kiểm tra tài khoản
                EwalletResultBO ewalletRS = this.ComputeBalanceAccount(inputSMS.SuperVisingDeptId, UserType.SupervisingDeptUser, priceSendSMS, InternalSendSMS, ExternalSendSMS);
                if (ewalletRS != null)
                {
                    if (ewalletRS.OK)
                    {
                        this.Save();
                        //SetAutoDetectChangesEnabled(true);

                        resultBO.Value = inputSMS.CountSMS * resultBO.NumSendSuccess;
                        // tru tien trong vi dien tu
                        Dictionary<string, object> dicEwallet = new Dictionary<string, object>();
                        dicEwallet["EWalletID"] = ewalletRS.EwalletID;
                        dicEwallet["Amount"] = ewalletRS.SubMainBalance;
                        dicEwallet["TransTypeID"] = GlobalConstantsEdu.TRANSACTION_TYPE_AMOUNT_SUB;
                        dicEwallet["ServiceType"] = GlobalConstantsEdu.SERVICE_TYPE_SENT_TO_SCHOOL;
                        dicEwallet["Quantity"] = ewalletRS.TotalSMSInMainBalance;
                        dicEwallet["PromotionAccountID"] = ewalletRS.PromotionAccountID;
                        dicEwallet["PromotionBalance"] = ewalletRS.SubPromBalance;
                        dicEwallet["InternalBalance"] = ewalletRS.SubInternalBalance;
                        dicEwallet["InternalSMS"] = ewalletRS.SubPromOfInSMS;
                        dicEwallet["ExternalSMS"] = ewalletRS.SubPromOfExSMS;
                        EWalletObject ewallet = EWalletBusiness.DeductMoneyAccount(dicEwallet);
                        resultBO.Balance = ewallet.Balance.ToString("#,##0");
                        resultBO.NumOfPromotionInternalSMS = ewallet.InternalSMS;
                        resultBO.TransAmount = ewalletRS.SubMainBalance;
                        resultBO.TotalSMSSentInMainBalance = ewalletRS.TotalSMSInMainBalance;
                        return resultBO;
                    }
                    else
                    {
                        resultBO.isError = true;
                        resultBO.ErrorMsg = ewalletRS.ErrorMsg;
                        return resultBO;
                    }
                }
                else
                {
                    resultBO.isError = true;
                    resultBO.ErrorMsg = "Có lỗi xảy ra trong quá trình gửi thư. Thầy/cô vui lòng nhấn F5 và thực hiện gửi lại!";
                    return resultBO;
                }
                #endregion
            }
            catch (Exception ex)
            {

                resultBO.isError = true;
                resultBO.ErrorMsg = "Có lỗi xảy ra trong quá trình gửi thư. Thầy/cô vui lòng nhấn F5 và thực hiện gửi lại!";
                string para = "null";
                LogExtensions.ErrorExt(logger, DateTime.Now, "SendSmstoSchool", para, ex);
                return resultBO;
            }
        }

        /// <summary>
        /// Gui tin nhan den nhan vien phong/so
        /// </summary>
        /// <param name="inputSMS"></param>
        /// <returns></returns>
        public SMSEduResultBO SendSmstoEmployee(SMSSupervisingDeptEmployeeBO inputSMS)
        {
            SMSEduResultBO resultBO = new SMSEduResultBO();
            try
            {
                SetAutoDetectChangesEnabled(false);

                string contentStripVNSign = Utils.ConvertLineToSpace(inputSMS.Content.StripVNSign().StripVNSignComposed());
                bool isInsertSentRecord = false;
                DateTime dateTimeNow = DateTime.Now;
                decimal priceSendSMS = 0;
                int InternalSendSMS = 0;
                int ExternalSendSMS = 0;
                #region gui tin nhan cho nhan vien phong so

                //Gọi API GetlistEmployee
                List<HistoryReceiverBO> employeeBOList;
                if (inputSMS.ListReceiverId != null && inputSMS.ListReceiverId.Count > 0)
                {
                    employeeBOList = EmployeeBusiness.GetListEmployeeNameByListEmployeeID(inputSMS.ListReceiverId);
                }
                else
                {
                    List<int> lstIds = SupervisingDeptBusiness.GetEmployeeBySuperVisingDept(inputSMS.ProvinceID, inputSMS.SuperVisingDeptId, inputSMS.EmployeeID, string.Empty, inputSMS.SelectedUnitID).Select(o => o.EmployeeID).ToList();

                    employeeBOList = EmployeeBusiness.GetListEmployeeNameByListEmployeeID(lstIds);
                }

                if (employeeBOList == null || employeeBOList.Count == 0)
                {
                    resultBO.isError = true;
                    resultBO.ErrorMsg = "Có lỗi xảy ra trong quá trình gửi thư. Thầy/cô vui lòng nhấn F5 và thực hiện gửi lại!";
                    return resultBO;
                }


                SMS_SENT_RECORD objSentRecord = (from sr in this.SentRecordBusiness.All
                                            where sr.SENT_YEAR == dateTimeNow.Year
                                            && sr.SENT_MONTH == dateTimeNow.Month
                                            && (sr.SUPER_VISING_DEPT_ID == inputSMS.SuperVisingDeptId)
                                            select sr).FirstOrDefault();
                if (objSentRecord == null)
                {
                    objSentRecord = new SMS_SENT_RECORD();
                    objSentRecord.SENT_DATE = dateTimeNow;
                    objSentRecord.SENT_MONTH = dateTimeNow.Month;
                    objSentRecord.SENT_YEAR = dateTimeNow.Year;
                    objSentRecord.SUPER_VISING_DEPT_ID = inputSMS.SuperVisingDeptId;
                    objSentRecord.PARTITION_ID = UtilsBusiness.GetPartionId(inputSMS.SuperVisingDeptId);
                    objSentRecord.SENT_COUNT = 0;
                    isInsertSentRecord = true;
                }

                SMS_HISTORY objHistorySMS = null;
                SMS_MT objMT = null;
                HistoryReceiverBO objEmployeeBO = null;
                SMS_RECEIVED_RECORD recordReceived = null;


                #region Han muc tin nhan hang thang
                List<int> LstEmployeeID = employeeBOList.Select(o => o.ReceiverID).ToList();

                Dictionary<string, object> dicReceiver = new Dictionary<string, object>();
                dicReceiver.Add("Type", GlobalConstantsEdu.COMMON_HISTORY_RECEIVER_EMPLOYEE_ID);
                dicReceiver.Add("dateTimeNow", dateTimeNow);
                dicReceiver.Add("LstEmployeeID", LstEmployeeID);
                dicReceiver.Add("SupervisingDeptID", inputSMS.SuperVisingDeptId);
                List<SMS_RECEIVED_RECORD> LstReceivedRecord = ReceivedRecordBusiness.GetListReceiRecord(dicReceiver).ToList();
                int partitionId = UtilsBusiness.GetPartionId(inputSMS.SuperVisingDeptId);

                #endregion

                ///Danh sach brandName da dang ky
                List<BrandNameUsingBO> lstBrandName = BrandnameRegistrationBusiness.GetBrandNameUsing(inputSMS.SuperVisingDeptId);

                byte contentType = inputSMS.IsSignMsg ? GlobalConstantsEdu.SMS_CONTENT_TYPE_UNICODE : GlobalConstantsEdu.SMS_CONTENT_TYPE_NOT_SIGN;
                string brandName = GlobalConstantsEdu.BRANDNAME_DEFAULT;
                for (int i = employeeBOList.Count - 1; i >= 0; i--)
                {
                    objEmployeeBO = employeeBOList[i];
                    // Chỉ cho phép gửi tin có dấu với các đầu số cho trước
                    if (inputSMS.IsSignMsg && !objEmployeeBO.Mobile.isAllowSendSignSMSPhone()) continue;
                    if (!objEmployeeBO.Mobile.CheckMobileNumber()) continue;

                    brandName = GetBrandNamebyMobile(objEmployeeBO.Mobile, lstPreNumberTelco, lstBrandName);

                    objHistorySMS = new SMS_HISTORY()
                    {
                        MOBILE = objEmployeeBO.Mobile,
                        IS_INTERNAL = objEmployeeBO.Mobile.CheckMobileNumberVT(),
                        RECEIVER_NAME = objEmployeeBO.ReceiverName,
                        SMS_TYPE_ID = GlobalConstantsEdu.COMMON_HISTORYSMS_TO_EMPLOYEE_ID,
                        RECEIVER_ID = objEmployeeBO.ReceiverID,
                        CREATE_DATE = dateTimeNow,
                        SCHOOL_ID = 0,
                        PARTITION_ID = partitionId,
                        YEAR = dateTimeNow.Year,
                        SMS_CONTENT = inputSMS.Content,
                        SENDER_NAME = inputSMS.SuperUserName,
                        RECEIVER_UNIT_NAME = objEmployeeBO.ReceiverUnitName,
                        SMS_CNT = inputSMS.CountSMS,
                        SEND_TIME = dateTimeNow,
                        SHORT_CONTENT = inputSMS.ShortContent,
                        RECEIVE_TYPE = GlobalConstantsEdu.COMMON_HISTORY_RECEIVER_EMPLOYEE_ID,
                        CONTENT_TYPE = contentType,
                        IS_SMS = true,
                        SENDER_UNIT_ID = inputSMS.SuperVisingDeptId,
                        SCHOOL_USERNAME = " ",
                        SERVICE_ID = brandName,
                        IS_ADMIN = inputSMS.isAdmin,
                        SENDER_ID = inputSMS.SenderId,
                        HISTORY_RAW_ID = Guid.NewGuid()
                    };

                    this.Insert(objHistorySMS);

                    // ReceivedRecord
                    recordReceived = LstReceivedRecord.FirstOrDefault(o => o.EMPLOYEE_ID == objEmployeeBO.ReceiverID);
                    // Chi luu thong tin GV co so noi mang
                    if (recordReceived == null)
                    {
                        recordReceived = new SMS_RECEIVED_RECORD();
                        recordReceived.SUPER_VISING_DEPT_ID = inputSMS.SuperVisingDeptId;
                        recordReceived.RECEIVED_TYPE = GlobalConstantsEdu.COMMON_HISTORY_RECEIVER_EMPLOYEE_ID;
                        recordReceived.EMPLOYEE_ID = objEmployeeBO.ReceiverID;
                        recordReceived.RECEIVED_YEAR = dateTimeNow.Year;
                        recordReceived.RECEIVED_MONTH = dateTimeNow.Month;
                        recordReceived.TOTAL_RECEIVED_SMS = 0;
                        recordReceived.CREATED_TIME = dateTimeNow;
                        recordReceived.TOTAL_INTERNAL_SMS = 0;
                        recordReceived.TOTAL_EXTERNAL_SMS = 0;
                        recordReceived.PARTITION_ID = partitionId;
                        this.ReceivedRecordBusiness.Insert(recordReceived);
                    }

                    //insert MT
                    objMT = new SMS_MT();
                    objMT.REQUEST_ID = Guid.NewGuid();
                    objMT.HISTORY_RAW_ID = objHistorySMS.HISTORY_RAW_ID;
                    if (inputSMS.IsSignMsg)
                    {
                        objMT.CONTENT_TYPE = GlobalConstantsEdu.SMS_CONTENT_TYPE_UNICODE.ToString(); // có dấu
                        objMT.SMS_CONTENT = inputSMS.Content;
                    }
                    else
                    {
                        objMT.CONTENT_TYPE = GlobalConstantsEdu.SMS_CONTENT_TYPE_NOT_SIGN.ToString(); // ko dấu
                        objMT.SMS_CONTENT = contentStripVNSign;
                    }
                    objMT.STATUS = GlobalConstantsEdu.MT_STATUS_NOT_SEND;
                    objMT.TIME_SEND_REQUEST = dateTimeNow;
                    objMT.USER_ID = objHistorySMS.MOBILE;
                    objMT.SERVICE_ID = brandName;
                    objMT.UNIT_ID = inputSMS.SuperVisingDeptId;
                    this.MTBusiness.Insert(objMT);

                    //Tinh SMS han muc KM
                    PriceSendSMSBO objPriceSMS = CalculateSMSPromotion(objHistorySMS, inputSMS.Promotion, inputSMS.CountSMS, recordReceived.TOTAL_RECEIVED_SMS, recordReceived.TOTAL_INTERNAL_SMS, recordReceived.TOTAL_EXTERNAL_SMS);
                    if (objPriceSMS != null)
                    {
                        priceSendSMS += objPriceSMS.PriceSendSMS;
                        InternalSendSMS += objPriceSMS.InternalSendSMS;
                        ExternalSendSMS += objPriceSMS.ExternalSendSMS;
                        recordReceived.TOTAL_RECEIVED_SMS = objPriceSMS.TotalReceivedSMS;
                        recordReceived.TOTAL_INTERNAL_SMS = objPriceSMS.TotalInternalSMS;
                        recordReceived.TOTAL_EXTERNAL_SMS = objPriceSMS.TotalExternalSMS;
                    }

                    //Cap nhat tin nha da nhan
                    if (recordReceived.SMS_RECEIVED_RECORD_ID > 0)
                    {
                        recordReceived.UPDATED_TIME = DateTime.Now;
                        this.ReceivedRecordBusiness.Update(recordReceived);
                    }


                    //cap nhat lai so tin nhan da gui
                    if (objSentRecord != null)
                    {
                        objSentRecord.SENT_COUNT += inputSMS.CountSMS;
                    }
                    resultBO.NumSendSuccess++;

                }
                if (objSentRecord != null)
                {
                    //cap nhat lai sendTime
                    objSentRecord.SENT_TIME++;
                    if (isInsertSentRecord)
                    {
                        this.SentRecordBusiness.Insert(objSentRecord);
                    }
                    else
                    {
                        this.SentRecordBusiness.Update(objSentRecord);
                    }
                }
                #endregion

                #region  Kiểm tra tài khoản
                EwalletResultBO ewalletRS = this.ComputeBalanceAccount(inputSMS.SuperVisingDeptId, UserType.SupervisingDeptUser, priceSendSMS, InternalSendSMS, ExternalSendSMS);
                if (ewalletRS != null)
                {
                    if (ewalletRS.OK)
                    {
                        this.Save();
                        //SetAutoDetectChangesEnabled(true);

                        resultBO.Value = inputSMS.CountSMS * resultBO.NumSendSuccess;
                        // tru tien trong vi dien tu
                        Dictionary<string, object> dicEwallet = new Dictionary<string, object>();
                        dicEwallet["EWalletID"] = ewalletRS.EwalletID;
                        dicEwallet["Amount"] = ewalletRS.SubMainBalance;
                        dicEwallet["TransTypeID"] = GlobalConstantsEdu.TRANSACTION_TYPE_AMOUNT_SUB;
                        dicEwallet["ServiceType"] = GlobalConstantsEdu.SERVICE_TYPE_SENT_TO_DEPARTMENT;

                        dicEwallet["Quantity"] = ewalletRS.TotalSMSInMainBalance;
                        dicEwallet["PromotionAccountID"] = ewalletRS.PromotionAccountID;
                        dicEwallet["PromotionBalance"] = ewalletRS.SubPromBalance;
                        dicEwallet["InternalBalance"] = ewalletRS.SubInternalBalance;
                        dicEwallet["InternalSMS"] = ewalletRS.SubPromOfInSMS;
                        dicEwallet["ExternalSMS"] = ewalletRS.SubPromOfExSMS;
                        EWalletObject ewallet = EWalletBusiness.DeductMoneyAccount(dicEwallet);
                        resultBO.Balance = ewallet.Balance.ToString("#,##0");
                        resultBO.NumOfPromotionInternalSMS = ewallet.InternalSMS;
                        resultBO.TransAmount = ewalletRS.SubMainBalance;
                        resultBO.TotalSMSSentInMainBalance = ewalletRS.TotalSMSInMainBalance;
                        return resultBO;
                    }
                    else
                    {
                        resultBO.isError = true;
                        resultBO.ErrorMsg = ewalletRS.ErrorMsg;
                        return resultBO;
                    }
                }
                else
                {
                    resultBO.isError = true;
                    resultBO.ErrorMsg = "Có lỗi xảy ra trong quá trình gửi thư. Thầy/cô vui lòng nhấn F5 và thực hiện gửi lại!";
                    return resultBO;
                }

                #endregion
            }
            catch (Exception ex)
            {
                string para = "null";
                LogExtensions.ErrorExt(logger, DateTime.Now, "SendSmstoEmployee", para, ex);
                resultBO.isError = true;
                resultBO.ErrorMsg = "Có lỗi xảy ra trong quá trình gửi thư. Thầy/cô vui lòng nhấn F5 và thực hiện gửi lại!";
                return resultBO;
            }
            finally
            {
                SetAutoDetectChangesEnabled(true);
            }
        }

        public ResultBO SendSMSExternal(Dictionary<string, object> dic, int AcademicYearId, int Semester, int Grade, string SchoolName)
        {
            ResultBO resultBO = new ResultBO();
            try
            {
                SetAutoDetectChangesEnabled(false);
                string content = dic.GetString("content").Trim();
                bool isSendImport = dic.GetBool("isSendImport");
                List<string> contentList = dic.GetStringList("lstContent");
                //neu khong phai truong hop noi dung tin nhan khac nhau doi voi moi receiver thi check content
                if (contentList.Count == 0)
                {
                    if (!isSendImport && (string.IsNullOrWhiteSpace(content) || content.Length > GlobalConstantsEdu.COMMON_TABLE_SMSHISTORY_CONTENT_MAXLENGTH))
                    {
                        resultBO.isError = true;
                        resultBO.ErrorMsg = "Nội dung thư không hợp lệ. Vui lòng kiểm tra lại nội dung thư!";
                        return resultBO;
                    }

                    List<string> lstInputedSensitiveWord = new List<string>();
                    if (content.CheckContentSMS(ref lstInputedSensitiveWord))
                    {
                        resultBO.isError = true;
                        resultBO.ErrorMsg = string.Format("Nội dung thư chứa các từ không hợp lệ: {0}. Vui lòng kiểm tra lại nội dung thư!", string.Join(", ", lstInputedSensitiveWord));
                        return resultBO;
                    }
                }

                int schoolID = dic.GetInt("schoolID");
                AcademicYear academicYearBO = null;
                //if (dic["academicYearID"] != null)
                //{
                //    academicYearBO = systemLoaderBusiness.GetAcademicYearByID(AcademicYearId);
                //}
                //else
                //{
                //    academicYearBO = GlobalInfo.AcademicYear;
                //}

                academicYearBO = this.AcademicYearBusiness.Find(AcademicYearId);

                if (academicYearBO == null)
                {
                    resultBO.isError = true;
                    resultBO.ErrorMsg = "SendSMS_Label_SchoolYearError";
                    return resultBO;
                }

                int type = dic.GetInt("type");
                int semester = Semester;
                DateTime dateTimeNow = DateTime.Now;
                if (type != GlobalConstantsEdu.COMMON_SMS_COMUNICATION_TEACHER_TO_TEACHER)
                {
                    if (semester == 0)
                    {
                        resultBO.isError = true;
                        resultBO.ErrorMsg = "Quá trình gửi thư xảy ra lỗi liên quan đến thời gian năm học. Thấy/cô vui lòng kiểm tra lại.";
                        return resultBO;
                    }
                }

                dic["year"] = academicYearBO.Year;
                //check quyen user                            
                dic["dateTime"] = dateTimeNow;
                SchoolProfile school = this.SchoolProfileBusiness.Find(schoolID);
                bool inValidNumber = false;
                string contentStripVNSign = ExtensionMethods.ConvertLineToSpace(content.StripVNSign().StripVNSignComposed());
                int senderID = dic.GetInt("senderID");
                List<int> lstReceiverID = dic.GetIntList("lstReceiverID");
                int contactGroupID = dic.GetInt("contactGroupID");
                int classID = dic.GetInt("classID");
                string shortContent = dic.GetString("shortContent").Trim();
                bool isPrincipal = dic.GetBool("isPrincipal");
                bool isAdmin = dic.GetBool("isAdmin");
                int? typeHistory = dic.GetNullableInt("typeHistory");
                bool isCustomType = dic.GetBool("IsCustomType");
                int iEducationLevelID = dic.GetInt("iEducationLevelID");
                bool sendAllSchool = dic.GetBool("BoolSendAllSchool");
                int iLevelID = dic.GetInt("iLevelID");
                List<string> lstShortContent = dic.GetStringList("lstShortContent");
                long senderGroup = DateTime.Now.Ticks;
                dic["isSubmitChange"] = false;
                string schoolUserName = this.SchoolProfileBusiness.GetUserNameBySchoolID(schoolID);
                bool isSignMsg = dic.GetBool("isSignMsg");
                int countSMS = content.countNumSMS(isSignMsg);
                bool isSendSMSCommentOfPupil = dic.GetBool("isSendSMSComment");
                PromotionProgramBO promotion = Utils.GetObject<PromotionProgramBO>(dic, "Promotion");
                bool isAllClass = dic.GetBool("IsAllClass");

                //viethd4: Gui hen gio
                bool isTimerUsed = dic.GetBool("isTimerUsed");
                DateTime? sendTime = null;
                if (dic.ContainsKey("sendTime"))
                {
                    sendTime = (DateTime)dic["sendTime"];
                }
                string timerConfigName = dic.GetString("timerConfigName");

                //viethd4: Gui tin import toan truong, toan khoi
                List<SendSMSByImportBO> lstImportSMS = new List<SendSMSByImportBO>();
                if (isSendImport && dic.ContainsKey("ListPupilImport"))
                {
                    lstImportSMS = (List<SendSMSByImportBO>)dic["ListPupilImport"];
                }

                int partitionSMSHistory = UtilsBusiness.GetPartionId(schoolID);
                int partitionSMSParentContract = UtilsBusiness.GetPartionId(schoolID);
                SMSParentSendDetailSearchBO objSMSSendDetail = new SMSParentSendDetailSearchBO
                {
                    SchoolId = schoolID,
                    //Semester = semester,
                    Year = academicYearBO.Year
                };

                IQueryable<SMS_PARENT_SENT_DETAIL> qSMSParentSendDetail = SMSParentSentDetailBusiness.Search(objSMSSendDetail);
                switch (type)
                {

                    case GlobalConstantsEdu.COMMON_SMS_COMUNICATION_TEACHER_TO_PARENT:
                        {
                            #region teacher to parent

                            bool hasTimerMessage = false;
                            //17.01.2017 Danh sach brandName da dang ky
                            List<BrandNameUsingBO> lstBrandName = BrandnameRegistrationBusiness.GetBrandNameUsing(schoolID);
                            string brandName = GlobalConstantsEdu.BRANDNAME_DEFAULT;
                            // Kiem tra cap nhat cac thue bao No cuoc
                            int numDeferredPaymentDays = Convert.ToInt32(System.Configuration.ConfigurationManager.AppSettings.Get(GlobalConstantsEdu.DEFERRED_PAYMENT_DAYS_KEY)); // So ngay no cuoc

                            var lstUnpaidContract = (from ct in this.SMSParentContractBusiness.All
                                                     join ctd in this.SMSParentContractDetailBusiness.All on ct.SMS_PARENT_CONTRACT_ID equals ctd.SMS_PARENT_CONTRACT_ID
                                                     where ct.SCHOOL_ID == schoolID && ct.PARTITION_ID == partitionSMSParentContract
                                                     && ctd.YEAR_ID == academicYearBO.Year
                                                     && ctd.SUBSCRIPTION_STATUS == GlobalConstantsEdu.STATUS_UNPAID
                                                     && ctd.IS_ACTIVE
                                                     && (ct.IS_DELETED == false)
                                                     && EntityFunctions.DiffDays(ctd.CREATED_TIME, DateTime.Now) > numDeferredPaymentDays
                                                     select ctd).ToList();
                            foreach (var item in lstUnpaidContract)
                            {
                                item.SUBSCRIPTION_STATUS = GlobalConstantsEdu.STATUS_OVERDUE_UNPAID;
                            }

                            // Lay danh sach cac goi co gioi han - Voi cac HĐ co goi cuoc loai nay thi khong tinh vao quy tin 
                            List<SMS_SERVICE_PACKAGE_DECLARE> LstLimitPackage = (from sp in this.ServicePackageBusiness.All
                                                                                 join spdd in this.ServicePackageDeclareBusiness.All on sp.SERVICE_PACKAGE_ID equals spdd.SERVICE_PACKAGE_ID
                                                                                 where spdd.IS_LIMIT
                                                                                 select spdd).ToList();

                            if ((iEducationLevelID != 0 || (sendAllSchool && iEducationLevelID == 0)) && (isAdmin || isPrincipal))
                            {
                                #region gui cho phhs toan truong/ hoac toan khoi
                                if (typeHistory == GlobalConstantsEdu.COMMON_HISTORYSMS_TO_COMMUNICATION_ID && isCustomType == false)
                                {
                                    //viethd31: Tao cau hinh hen gio
                                    SMS_TIMER_CONFIG tc = new SMS_TIMER_CONFIG();
                                    if (isTimerUsed)
                                    {
                                        tc = new SMS_TIMER_CONFIG();
                                        tc.SMS_TIMER_CONFIG_ID = Guid.NewGuid();
                                        tc.CONTENT = content;
                                        tc.CREATE_TIME = DateTime.Now;
                                        tc.SCHOOL_ID = schoolID;
                                        tc.APPLIED_LEVEL = iLevelID;
                                        tc.PARTITION_ID = partitionSMSHistory;
                                        tc.SEND_TIME = sendTime.Value;
                                        tc.STATUS = GlobalConstantsEdu.TIMER_CONFIG_STATUS_ACTIVE;//chua gui
                                        tc.TARGET_ID = iEducationLevelID != 0 ? iEducationLevelID : 0;
                                        tc.CONFIG_NAME = timerConfigName;
                                        tc.CONFIG_TYPE = iEducationLevelID != 0 ? GlobalConstantsEdu.TIMER_CONFIG_TYPE_EDU : GlobalConstantsEdu.TIMER_CONFIG_TYPE_SCHOOL;
                                        tc.SEND_ALL_CLASS = isAllClass;
                                        tc.SEND_UNICODE = isSignMsg;
                                        tc.IS_IMPORT = isSendImport;

                                        this.TimerConfigBusiness.Insert(tc);
                                    }

                                    #region neu la ban tin trao doi
                                    List<EducationLevel> levelBOList = null;
                                    if (sendAllSchool && iEducationLevelID == 0)
                                    {
                                        levelBOList = this.EducationLevelBusiness.GetByGrade(Grade).ToList();
                                    }
                                    // AnhVD9 20150106 - Bo sung thong tin lop de kiem tra chi gui cho lop con quy tin
                                    List<PupilOfClass> lstPupilOfClass = new List<PupilOfClass>();
                                    IDictionary<string, object> dicClass = new Dictionary<string, object>();
                                    if (levelBOList == null)
                                    {
                                        
                                        dicClass.Add("SchoolID", schoolID);
                                        dicClass.Add("AcademicYearID", academicYearBO.AcademicYearID);
                                        dicClass.Add("EducationLevelID", iEducationLevelID);
                                        lstPupilOfClass = PupilOfClassBusiness.Search(dicClass).Where(p => (p.Status == GlobalConstants.PUPIL_STATUS_STUDYING || p.Status == GlobalConstants.PUPIL_STATUS_GRADUATED)).Distinct().ToList();
                                        if (lstPupilOfClass == null || lstPupilOfClass.Count == 0)
                                        {
                                            resultBO.isError = false;
                                            resultBO.NumSendSuccess = 0;
                                            return resultBO;
                                        }
                                    }
                                    else
                                    {
                                        dicClass.Add("SchoolID", schoolID);
                                        dicClass.Add("AcademicYearID", academicYearBO.AcademicYearID);
                                        lstPupilOfClass = PupilOfClassBusiness.Search(dicClass).Where(p => (p.Status == GlobalConstants.PUPIL_STATUS_STUDYING || p.Status == GlobalConstants.PUPIL_STATUS_GRADUATED)).Distinct().ToList();
                                    }

                                    bool isSameContent = true;

                                    if (isSendImport)
                                    {
                                        List<int> lstPupilImportID = lstImportSMS.Select(o => o.PupilFileID).ToList();
                                        string firstContent = lstImportSMS.FirstOrDefault().Content;
                                        if (lstImportSMS.Count > 0 && lstImportSMS.Where(o => o.Content != firstContent).Count() > 0)
                                        {
                                            isSameContent = false;
                                        }
                                        lstPupilOfClass = lstPupilOfClass.Where(o => lstPupilImportID.Contains(o.PupilID)).ToList();

                                    }

                                    lstReceiverID = lstPupilOfClass.Select(o => o.PupilID).ToList();

                                    // check truong hop 2000 parameter
                                    List<PupilContract> lstPupilContract = new List<PupilContract>();
                                    List<PupilProfileBO> pupilProfileList = new List<PupilProfileBO>();
                                    int numOfSplit = 2000;
                                    if (lstReceiverID.Count > numOfSplit)
                                    {
                                        int numOfPupil = (int)Math.Ceiling((decimal)lstReceiverID.Count / numOfSplit);
                                        List<int> lstReceiverTmp = null;
                                        for (int i = 0; i < numOfPupil; i++)
                                        {
                                            lstReceiverTmp = lstReceiverID.Skip((i) * numOfSplit).Take(numOfSplit).ToList();
                                            //danh sach hoc sinh                 
                                            var pupilQuery = (from sc in this.SMSParentContractBusiness.All
                                                              join scd in this.SMSParentContractDetailBusiness.All on sc.SMS_PARENT_CONTRACT_ID equals scd.SMS_PARENT_CONTRACT_ID
                                                              join pdtl in this.ServicePackageDetailBusiness.All on scd.SERVICE_PACKAGE_ID equals pdtl.SERVICE_PACKAGE_ID
                                                              join spdd in this.ServicePackageDeclareBusiness.All on scd.SERVICE_PACKAGE_ID equals spdd.SERVICE_PACKAGE_ID
                                                              join sentDtl in qSMSParentSendDetail on scd.SMS_PARENT_CONTRACT_DETAIL_ID equals sentDtl.SMS_PARENT_CONTRACT_DETAIL_ID into gj
                                                              from x in gj.DefaultIfEmpty(null)
                                                              where sc.SCHOOL_ID == schoolID && sc.PARTITION_ID == partitionSMSParentContract
                                                              && (scd.SUBSCRIPTION_STATUS == GlobalConstantsEdu.STATUS_REGISTED || scd.SUBSCRIPTION_STATUS == GlobalConstantsEdu.STATUS_UNPAID) // Bổ sung gửi tin cho các thuê bao Nợ cước
                                                              && scd.IS_ACTIVE == true && sc.STATUS == GlobalConstantsEdu.SMS_PARENT_CONTRACT_STATUS_IS_ACCEPT && (sc.IS_DELETED == false || sc.IS_DELETED == null)
                                                              && lstReceiverTmp.Contains(sc.PUPIL_ID)
                                                              &&
                                                              (
                                                               (semester == 1 && (scd.SUBSCRIPTION_TIME == GlobalConstantsEdu.CONTRACTDETAIL_SUBSCRIPTIONTIME_FIRST_SMESTER || scd.SUBSCRIPTION_TIME == GlobalConstantsEdu.CONTRACTDETAIL_SUBSCRIPTIONTIME_ALLYEAR))
                                                               ||
                                                               (semester == 2 && (scd.SUBSCRIPTION_TIME == GlobalConstantsEdu.CONTRACTDETAIL_SUBSCRIPTIONTIME_SECOND_SMESTER || scd.SUBSCRIPTION_TIME == GlobalConstantsEdu.CONTRACTDETAIL_SUBSCRIPTIONTIME_ALLYEAR))
                                                              )
                                                              && scd.YEAR_ID == academicYearBO.Year
                                                              && spdd.YEAR == academicYearBO.Year
                                                              && spdd.STATUS == true
                                                              select new PupilContractDetail
                                                              {
                                                                  PupilFileID = sc.PUPIL_ID,
                                                                  Mobile = scd.SUBSCRIBER,
                                                                  SMSParentContactDetailID = scd.SMS_PARENT_CONTRACT_DETAIL_ID,
                                                                  MaxSMS = (pdtl.MAX_SMS.HasValue) ? scd.SUBSCRIPTION_TIME == 3 ? pdtl.MAX_SMS.Value * 2 : pdtl.MAX_SMS.Value : 0,
                                                                  SentSMS = ((x == null) ? 0 : (x.SENT_TOTAL)),
                                                                  SMSParentSentDetailID = ((x == null) ? 0 : x.SMS_PARENT_SENT_DETAIL_ID),
                                                                  ServicePackageID = pdtl.SERVICE_PACKAGE_ID,
                                                                  IsLimitPackage = spdd.IS_LIMIT,
                                                                  RegisterDate = scd.CREATED_TIME,
                                                                  Status = scd.SUBSCRIPTION_STATUS,
                                                              });
                                            //Xu ly lai query de lay ra tong so tin nhan cua tung thue bao dang ky.
                                            lstPupilContract.AddRange(pupilQuery
                                                .GroupBy(x => new { x.PupilFileID, x.SMSParentContactDetailID, x.Mobile, x.SentSMS, x.SMSParentSentDetailID, x.ServicePackageID, x.RegisterDate, x.IsLimitPackage, x.Status })
                                                .Select(y => new PupilContract
                                                {
                                                    Mobile = y.Key.Mobile,
                                                    SMSParentContactDetailID = y.Key.SMSParentContactDetailID,
                                                    PupilFileID = y.Key.PupilFileID,
                                                    SentSMS = y.Key.SentSMS,
                                                    IsSent = false,
                                                    SMSParentSentDetailID = y.Key.SMSParentSentDetailID,
                                                    ServicePackageID = y.Key.ServicePackageID,
                                                    IsLimitPackage = y.Key.IsLimitPackage,
                                                    RegisterDate = y.Key.RegisterDate,
                                                    TotalSMS = y.Sum(a => a.MaxSMS),
                                                    Status = y.Key.Status
                                                }).ToList());
                                            // lay danh sach chua giao vien chu nhiem
                                            //lstReceiverTmp = lstPupilContract.Select(p => p.PupilFileID).ToList();
                                            pupilProfileList.AddRange(this.PupilProfileBusiness.GetListPupilSendSMS(lstReceiverTmp, AcademicYearId));
                                        }
                                    }
                                    else
                                    {
                                        //danh sach hoc sinh                 
                                        var pupilQuery = (from sc in this.SMSParentContractBusiness.All
                                                          join scd in this.SMSParentContractDetailBusiness.All on sc.SMS_PARENT_CONTRACT_ID equals scd.SMS_PARENT_CONTRACT_ID
                                                          join pdtl in this.ServicePackageDetailBusiness.All on scd.SERVICE_PACKAGE_ID equals pdtl.SERVICE_PACKAGE_ID
                                                          join spdd in this.ServicePackageDeclareBusiness.All on scd.SERVICE_PACKAGE_ID equals spdd.SERVICE_PACKAGE_ID
                                                          join sentDtl in qSMSParentSendDetail on scd.SMS_PARENT_CONTRACT_DETAIL_ID equals sentDtl.SMS_PARENT_CONTRACT_DETAIL_ID into gj
                                                          from x in gj.DefaultIfEmpty(null)
                                                          where sc.SCHOOL_ID == schoolID && sc.PARTITION_ID == partitionSMSParentContract
                                                          && (scd.SUBSCRIPTION_STATUS == GlobalConstantsEdu.STATUS_REGISTED || scd.SUBSCRIPTION_STATUS == GlobalConstantsEdu.STATUS_UNPAID) // Bổ sung gửi tin cho các thuê bao Nợ cước 
                                                          && scd.IS_ACTIVE == true && sc.STATUS == GlobalConstantsEdu.SMS_PARENT_CONTRACT_STATUS_IS_ACCEPT && (sc.IS_DELETED == false || sc.IS_DELETED == null)
                                                          && lstReceiverID.Contains(sc.PUPIL_ID)
                                                          &&
                                                          (
                                                           (semester == 1 && (scd.SUBSCRIPTION_TIME == GlobalConstantsEdu.CONTRACTDETAIL_SUBSCRIPTIONTIME_FIRST_SMESTER || scd.SUBSCRIPTION_TIME == GlobalConstantsEdu.CONTRACTDETAIL_SUBSCRIPTIONTIME_ALLYEAR))
                                                           ||
                                                           (semester == 2 && (scd.SUBSCRIPTION_TIME == GlobalConstantsEdu.CONTRACTDETAIL_SUBSCRIPTIONTIME_SECOND_SMESTER || scd.SUBSCRIPTION_TIME == GlobalConstantsEdu.CONTRACTDETAIL_SUBSCRIPTIONTIME_ALLYEAR))
                                                          )
                                                          && scd.YEAR_ID == academicYearBO.Year
                                                          && spdd.YEAR == academicYearBO.Year
                                                              && spdd.STATUS == true
                                                          select new PupilContractDetail
                                                          {
                                                              PupilFileID = sc.PUPIL_ID,
                                                              Mobile = scd.SUBSCRIBER,
                                                              SMSParentContactDetailID = scd.SMS_PARENT_CONTRACT_DETAIL_ID,
                                                              MaxSMS = (pdtl.MAX_SMS.HasValue) ? scd.SUBSCRIPTION_TIME == 3 ? pdtl.MAX_SMS.Value * 2 : pdtl.MAX_SMS.Value : 0,
                                                              SentSMS = ((x == null) ? 0 : (x.SENT_TOTAL)),
                                                              SMSParentSentDetailID = ((x == null) ? 0 : x.SMS_PARENT_SENT_DETAIL_ID),
                                                              ServicePackageID = pdtl.SERVICE_PACKAGE_ID,
                                                              IsLimitPackage = spdd.IS_LIMIT,
                                                              RegisterDate = scd.CREATED_TIME,
                                                              Status = scd.SUBSCRIPTION_STATUS,
                                                              SemesterID = scd.SUBSCRIPTION_TIME
                                                          });
                                        //Xu ly lai query de lay ra tong so tin nhan cua tung thue bao dang ky.
                                        lstPupilContract = pupilQuery
                                            .GroupBy(x => new { x.PupilFileID, x.SMSParentContactDetailID, x.Mobile, x.SentSMS, x.SMSParentSentDetailID, x.ServicePackageID, x.RegisterDate, x.IsLimitPackage, x.Status })
                                            .Select(y => new PupilContract
                                            {
                                                Mobile = y.Key.Mobile,
                                                SMSParentContactDetailID = y.Key.SMSParentContactDetailID,
                                                PupilFileID = y.Key.PupilFileID,
                                                SentSMS = y.Key.SentSMS,
                                                IsSent = false,
                                                SMSParentSentDetailID = y.Key.SMSParentSentDetailID,
                                                ServicePackageID = y.Key.ServicePackageID,
                                                IsLimitPackage = y.Key.IsLimitPackage,
                                                RegisterDate = y.Key.RegisterDate,
                                                TotalSMS = y.Sum(a => a.MaxSMS),
                                                Status = y.Key.Status
                                            }).ToList();
                                        // lay danh sach chua giao vien chu nhiem
                                        lstReceiverID = lstPupilContract.Select(p => p.PupilFileID).ToList();
                                        pupilProfileList = PupilProfileBusiness.GetListPupilSendSMS(lstReceiverID, AcademicYearId);
                                    }
                                    List<int> lstPupilFileID = lstPupilContract.Select(p => p.PupilFileID).Distinct().ToList();
                                    //Lay danh sach lop va duyet theo tung lop - AnhVD9 20150106
                                    List<int> lstClassID = lstPupilOfClass.Where(o => lstPupilFileID.Contains(o.PupilID)).Select(o => o.ClassID).Distinct().ToList();
                                    SMS_PARENT_CONTRACT_CLASS objClassDetail = null;
                                    List<SMS_PARENT_CONTRACT_CLASS> lstClassDetails = this.SMSParentContractInClassDetailBusiness.All.Where(c => c.ACADEMIC_YEAR_ID == academicYearBO.AcademicYearID
                                                    && c.SCHOOL_ID == schoolID && c.YEAR == academicYearBO.Year && (c.SEMESTER == semester || c.SEMESTER == GlobalConstants.SEMESTER_OF_YEAR_ALL)).ToList();

                                    // Danh sach chi tiet gui tin cua hoc sinh 
                                    //List<int> LstContractDetailID = lstPupilContract.Select(o => o.SMSParentContactDetailID).Distinct().ToList();
                                    //List<SMSParentSentDetail> lstPupilSentDetail = qSMSParentSendDetail.Where(c => LstContractDetailID.Contains(c.SMSParentContractDetailID)).ToList();

                                    List<int> lstPupilIDInClass = new List<int>();
                                    List<int> lstClassSent = new List<int>();
                                    List<ClassBO> lstClassNotSent = new List<ClassBO>();
                                    ClassBO classNotSend = null;
                                    List<SMS_PARENT_CONTRACT_CLASS> lstClassDetailstmp = null;
                                    int vClassID = 0;
                                    for (int i = lstClassID.Count - 1; i >= 0; i--)
                                    {
                                        vClassID = lstClassID[i];
                                        lstPupilIDInClass = lstPupilOfClass.Where(o => o.ClassID == vClassID).Select(o => o.PupilID).ToList();

                                        lstClassDetailstmp = lstClassDetails.Where(o => o.CLASS_ID == vClassID).ToList();
                                        if (lstClassDetailstmp.Count == 0)
                                        {
                                            int TotalSMSInClass = lstPupilContract.Where(o => lstPupilIDInClass.Contains(o.PupilFileID) && o.IsLimitPackage != true
                                                && o.Status == GlobalConstantsEdu.STATUS_REGISTED).Sum(o => o.TotalSMS);
                                            int TotalSMSSent = lstPupilContract.Where(o => lstPupilIDInClass.Contains(o.PupilFileID) && o.IsLimitPackage != true
                                                && o.Status == GlobalConstantsEdu.STATUS_REGISTED).Sum(o => o.SentSMS);
                                            objClassDetail = new SMS_PARENT_CONTRACT_CLASS();
                                            objClassDetail.CLASS_ID = vClassID;
                                            objClassDetail.ACADEMIC_YEAR_ID = academicYearBO.AcademicYearID;
                                            objClassDetail.SCHOOL_ID = schoolID;
                                            objClassDetail.YEAR = academicYearBO.Year;
                                            objClassDetail.SEMESTER = (byte)semester;
                                            objClassDetail.TOTAL_SMS = TotalSMSInClass;
                                            objClassDetail.SENT_TOTAL = TotalSMSSent;
                                            objClassDetail.CREATED_DATE = DateTime.Now;
                                            lstClassDetails.Add(objClassDetail);
                                            if (TotalSMSInClass > 0) this.SMSParentContractInClassDetailBusiness.Insert(objClassDetail);
                                        }
                                        // so tin can gui trong lop
                                        int numSMSNeedSend = countSMS * lstPupilContract.Count(o => lstPupilIDInClass.Contains(o.PupilFileID) && o.IsLimitPackage != true && o.Status == GlobalConstantsEdu.STATUS_REGISTED);

                                        if ((lstClassDetailstmp.Sum(o => o.TOTAL_SMS) - lstClassDetailstmp.Sum(o => o.SENT_TOTAL)) < numSMSNeedSend)
                                        {
                                            // Het quy tin - Loai cac HS da thanh toan
                                            lstPupilFileID = lstPupilFileID.Except(lstPupilIDInClass).ToList();
                                            lstPupilFileID.Union(lstPupilContract.Where(o => lstPupilIDInClass.Contains(o.PupilFileID) && o.Status == GlobalConstantsEdu.STATUS_UNPAID).Select(o => o.PupilFileID).ToList());
                                            classNotSend = new ClassBO();
                                            classNotSend.ClassID = vClassID;
                                            classNotSend.ClassName = lstPupilOfClass.FirstOrDefault(o => o.ClassID == vClassID).ClassProfile.DisplayName;
                                            lstClassNotSent.Add(classNotSend);
                                        }
                                        else
                                        {
                                            lstClassSent.Add(vClassID);
                                        }
                                    }
                                    // End AnhVD9
                                    PupilProfileBO pupilProfileBO = null;

                                    int intPupilFileID = 0;
                                    PupilContract objPupilContractUspResult = null;
                                    List<PupilSubscriber> lstPupilSubscriber = null;
                                    PupilSubscriber objPupilSubscriber = null;
                                    int smsParentContractDetailID = 0;
                                    int countNumOfSend = 0;
                                    List<SMS_MT> lstMTInsertBO = new List<SMS_MT>();
                                    //List<SMSCommunicationInsertBO> lstSMSCommunicationInsertBO = new List<SMSCommunicationInsertBO>();
                                    List<SMS_HISTORY> lstHistorySMSInsertBO = new List<SMS_HISTORY>();
                                    SMS_MT objMTInsertBO = null;
                                    //SMSCommunicationInsertBO objSMSCommunicationInsertBO = null;
                                    SMS_HISTORY objHistorySMSInsertBO = null;
                                    SMS_SERVICE_PACKAGE_DECLARE detailLimitSP = null;
                                    //bool isHasManyMobileReceiver = false;
                                    bool isSendOnWebOK = false;
                                    //danh dau record history de lay HistoryID
                                    int iHistoryIndex = 0;
                                    string fullNameSender = SchoolName;

                                    #region For pupil
                                    for (int i = lstPupilFileID.Count - 1; i >= 0; i--)
                                    {
                                        intPupilFileID = lstPupilFileID[i];
                                        //isHasManyMobileReceiver = false;
                                        isSendOnWebOK = false;
                                        lstPupilSubscriber = (from pc in lstPupilContract
                                                              where pc.PupilFileID == intPupilFileID
                                                              select new PupilSubscriber
                                                              {
                                                                  Subscriber = pc.Mobile,
                                                                  SMSParentContractDetailID = pc.SMSParentContactDetailID
                                                              }).ToList();

                                        if (isSendImport)
                                        {
                                            SendSMSByImportBO sImport = lstImportSMS.FirstOrDefault(o => o.PupilFileID == intPupilFileID);
                                            content = sImport.Content;
                                            contentStripVNSign = ExtensionMethods.ConvertLineToSpace(content.StripVNSign().StripVNSignComposed());
                                            shortContent = sImport.ShortContent;
                                            countSMS = content.countNumSMS(isSignMsg);
                                        }


                                        for (int k = lstPupilSubscriber.Count - 1; k >= 0; k--)
                                        {
                                            objPupilSubscriber = lstPupilSubscriber[k];
                                            // Chỉ cho phép gửi tin có dấu với các đầu số cho trước
                                            if (isSignMsg && !objPupilSubscriber.Subscriber.isAllowSendSignSMSPhone()) continue;
                                            //Lay brandName theo thue bao
                                            brandName = GetBrandNamebyMobile(objPupilSubscriber.Subscriber, lstPreNumberTelco, lstBrandName);
                                            objPupilContractUspResult = (from pc in lstPupilContract where pc.Mobile == objPupilSubscriber.Subscriber && pc.PupilFileID == intPupilFileID && pc.SMSParentContactDetailID == objPupilSubscriber.SMSParentContractDetailID select pc).FirstOrDefault();
                                            pupilProfileBO = pupilProfileList.FirstOrDefault(p => p.PupilProfileID == intPupilFileID);
                                            if (pupilProfileBO == null) continue;

                                            //Not found from Contract Detail
                                            if (objPupilContractUspResult == null) continue;

                                            smsParentContractDetailID = (int)objPupilContractUspResult.SMSParentContactDetailID;

                                            if (!objPupilSubscriber.Subscriber.CheckMobileNumber()) continue;

                                            #region Kiem tra gui tin voi goi dich vu GIOI HAN
                                            // Neu la goi trial
                                            if (objPupilContractUspResult.IsLimitPackage.HasValue && objPupilContractUspResult.IsLimitPackage.Value)
                                            {

                                                //// Kiem tra thoi gian cho phep gui trong thoi han, ke tu ngay dau noi
                                                detailLimitSP = LstLimitPackage.FirstOrDefault(o => o.SERVICE_PACKAGE_ID == objPupilContractUspResult.ServicePackageID
                                                    && o.EFFECTIVE_DAYS > 0 &&
                                                    ((o.FROM_DATE.HasValue && o.FROM_DATE.Value.Date <= objPupilContractUspResult.RegisterDate.Date &&
                                                    o.TO_DATE.HasValue && o.TO_DATE.Value.Date >= objPupilContractUspResult.RegisterDate.Date)
                                                    || (!o.TO_DATE.HasValue && !o.FROM_DATE.HasValue)));
                                                if (detailLimitSP != null)
                                                {
                                                    if ((DateTime.Now - objPupilContractUspResult.RegisterDate.Date).TotalDays > detailLimitSP.EFFECTIVE_DAYS) continue;
                                                }

                                                //Kiem tra so luong tin nhan chi (cho phep gui khi con tin nhan)
                                                if (objPupilContractUspResult.TotalSMS - objPupilContractUspResult.SentSMS <= 0) continue;
                                            }
                                            #endregion


                                            // Cac goi No cuoc kiem tra con so luong tin
                                            if (objPupilContractUspResult.Status == GlobalConstantsEdu.STATUS_UNPAID && objPupilContractUspResult.TotalSMS - objPupilContractUspResult.SentSMS <= 0) continue;
                                            //Lay brandName theo thue bao
                                            brandName = GetBrandNamebyMobile(objPupilSubscriber.Subscriber, lstPreNumberTelco, lstBrandName);

                                            //insert historySMS
                                            objHistorySMSInsertBO = new SMS_HISTORY();
                                            objHistorySMSInsertBO.MOBILE = objPupilSubscriber.Subscriber;
                                            objHistorySMSInsertBO.IS_INTERNAL = objHistorySMSInsertBO.MOBILE.CheckMobileNumberVT();
                                            objHistorySMSInsertBO.RECEIVER_NAME = pupilProfileBO != null ? pupilProfileBO.FullName : string.Empty;
                                            if (typeHistory.HasValue && typeHistory.Value != 0) objHistorySMSInsertBO.SMS_TYPE_ID = typeHistory.Value;
                                            else objHistorySMSInsertBO.SMS_TYPE_ID = GlobalConstantsEdu.COMMON_HISTORYSMS_TO_COMMUNICATION_ID;

                                            objHistorySMSInsertBO.IS_CUSTOM_TYPE = isCustomType;
                                            objHistorySMSInsertBO.RECEIVE_TYPE = GlobalConstantsEdu.COMMON_HISTORY_RECEIVER_PARENT_ID;
                                            objHistorySMSInsertBO.CREATE_DATE = dateTimeNow;
                                            objHistorySMSInsertBO.SCHOOL_ID = schoolID;
                                            objHistorySMSInsertBO.YEAR = academicYearBO.Year;
                                            objHistorySMSInsertBO.SMS_CONTENT = content;
                                            objHistorySMSInsertBO.RECEIVER_ID = objPupilContractUspResult.PupilFileID;
                                            objHistorySMSInsertBO.SHORT_CONTENT = shortContent;
                                            objHistorySMSInsertBO.SENDER_NAME = !string.IsNullOrEmpty(fullNameSender) ? fullNameSender : " ";
                                            objHistorySMSInsertBO.SMS_CNT = countSMS;
                                            objHistorySMSInsertBO.SCHOOL_USERNAME = schoolUserName;
                                            objHistorySMSInsertBO.SMS_PARENTCONTRACT_DETAIL_ID = smsParentContractDetailID;
                                            objHistorySMSInsertBO.CONTENT_TYPE = isSignMsg ? GlobalConstantsEdu.SMS_CONTENT_TYPE_UNICODE : GlobalConstantsEdu.SMS_CONTENT_TYPE_NOT_SIGN;
                                            objHistorySMSInsertBO.IS_SMS = true;
                                            objHistorySMSInsertBO.SENDER_GROUP = senderGroup;
                                            objHistorySMSInsertBO.PARTITION_ID = partitionSMSHistory;
                                            objHistorySMSInsertBO.IS_ADMIN = isAdmin;
                                            objHistorySMSInsertBO.SENDER_ID = senderID;
                                            Guid guid = Guid.NewGuid();
                                            objHistorySMSInsertBO.HISTORY_RAW_ID = guid;
                                            if (isTimerUsed)
                                            {
                                                objHistorySMSInsertBO.SMS_TIMER_CONFIG_ID = tc.SMS_TIMER_CONFIG_ID;
                                                objHistorySMSInsertBO.IS_ON_SCHEDULE = true;
                                                objHistorySMSInsertBO.SEND_TIME = tc.SEND_TIME;
                                                hasTimerMessage = true;
                                            }

                                            lstHistorySMSInsertBO.Add(objHistorySMSInsertBO);

                                            //insert MT
                                            objMTInsertBO = new SMS_MT();
                                            objMTInsertBO.HISTORY_RAW_ID = guid;
                                            objMTInsertBO.USER_ID = objPupilSubscriber.Subscriber;
                                            if (isSignMsg)
                                            {
                                                objMTInsertBO.CONTENT_TYPE = "1"; // có dấu
                                                objMTInsertBO.SMS_CONTENT = content;
                                            }
                                            else
                                            {
                                                objMTInsertBO.CONTENT_TYPE = "0"; // ko dấu
                                                objMTInsertBO.SMS_CONTENT = contentStripVNSign;
                                            }
                                            objMTInsertBO.STATUS = GlobalConstants.MT_STATUS_NOT_SEND;
                                            objMTInsertBO.TIME_SEND_REQUEST = dateTimeNow;
                                            objMTInsertBO.UNIT_ID = schoolID;
                                            objMTInsertBO.SERVICE_ID = brandName;
                                            if (isTimerUsed)
                                            {
                                                objMTInsertBO.SMS_TIMER_CONFIG_ID = tc.SMS_TIMER_CONFIG_ID;
                                                hasTimerMessage = true;
                                            }
                                            lstMTInsertBO.Add(objMTInsertBO);

                                            //Chi tru khi gui qua MT
                                            objPupilContractUspResult.IsSent = true;
                                            objPupilContractUspResult.SentSMS += countSMS;
                                            isSendOnWebOK = true;
                                            iHistoryIndex++;

                                        }
                                        if (isSendOnWebOK)
                                        {
                                            countNumOfSend++;
                                        }
                                    }
                                    #endregion end for

                                    if (countNumOfSend == 0)
                                    {
                                        resultBO.isError = false;
                                        resultBO.NumSendSuccess = 0;
                                        return resultBO;
                                    }

                                    #region Send to head teacher - AnhVD9 20150922
                                    SMS_SCHOOL_CONFIG schoolCfg = this.SchoolConfigBusiness.All.FirstOrDefault(o => o.SCHOOL_ID == schoolID);
                                    if (isSameContent && schoolCfg == null || (schoolCfg != null && (!schoolCfg.IS_ALLOW_SEND_HEADTEACHER)))
                                    {
                                        List<TeacherBO> lstHeadTeachers = EmployeeBusiness.GetHeadTeacherByClassIDs(lstClassSent, academicYearBO.AcademicYearID, schoolID);
                                        TeacherBO objEmployeeReceiver = null;
                                        for (int i = lstHeadTeachers.Count - 1; i >= 0; i--)
                                        {
                                            objEmployeeReceiver = lstHeadTeachers[i];
                                            // Chỉ cho phép gửi tin có dấu với các đầu số cho trước
                                            if (isSignMsg && !objEmployeeReceiver.Mobile.isAllowSendSignSMSPhone()) continue;
                                            if (!objEmployeeReceiver.Mobile.CheckMobileNumber()) continue;
                                            //Lay brandName theo thue bao
                                            brandName = GetBrandNamebyMobile(objEmployeeReceiver.Mobile, lstPreNumberTelco, lstBrandName);
                                            //insert historySMS 
                                            objHistorySMSInsertBO = new SMS_HISTORY();
                                            objHistorySMSInsertBO.MOBILE = objEmployeeReceiver.Mobile;
                                            objHistorySMSInsertBO.IS_INTERNAL = objHistorySMSInsertBO.MOBILE.CheckMobileNumberVT();
                                            objHistorySMSInsertBO.RECEIVER_NAME = objEmployeeReceiver.FullName;
                                            // Loai tin nhan trao doi gui tu PHHS
                                            if (typeHistory.HasValue && typeHistory.Value != 0) objHistorySMSInsertBO.SMS_TYPE_ID = typeHistory.Value;
                                            else objHistorySMSInsertBO.SMS_TYPE_ID = GlobalConstantsEdu.COMMON_HISTORYSMS_TO_COMMUNICATION_ID;

                                            objHistorySMSInsertBO.IS_CUSTOM_TYPE = isCustomType;
                                            objHistorySMSInsertBO.RECEIVE_TYPE = GlobalConstantsEdu.COMMON_HISTORY_RECEIVER_TEACHER_ID;
                                            objHistorySMSInsertBO.CREATE_DATE = dateTimeNow;
                                            objHistorySMSInsertBO.SCHOOL_ID = schoolID;
                                            objHistorySMSInsertBO.YEAR = academicYearBO.Year;
                                            objHistorySMSInsertBO.SMS_CONTENT = content;
                                            objHistorySMSInsertBO.SENDER_NAME = fullNameSender;
                                            objHistorySMSInsertBO.SHORT_CONTENT = shortContent;
                                            objHistorySMSInsertBO.RECEIVER_ID = objEmployeeReceiver.TeacherID;
                                            objHistorySMSInsertBO.SMS_CNT = countSMS;
                                            objHistorySMSInsertBO.SCHOOL_USERNAME = schoolUserName;
                                            objHistorySMSInsertBO.SENDER_GROUP = senderGroup;
                                            objHistorySMSInsertBO.CONTENT_TYPE = isSignMsg ? GlobalConstantsEdu.SMS_CONTENT_TYPE_UNICODE : GlobalConstantsEdu.SMS_CONTENT_TYPE_NOT_SIGN;
                                            objHistorySMSInsertBO.IS_SMS = true;
                                            objHistorySMSInsertBO.PARTITION_ID = partitionSMSHistory;
                                            objHistorySMSInsertBO.IS_ADMIN = isAdmin;
                                            objHistorySMSInsertBO.SENDER_ID = senderID;
                                            Guid guid = Guid.NewGuid();
                                            objHistorySMSInsertBO.HISTORY_RAW_ID = guid;
                                            if (isTimerUsed)
                                            {
                                                objHistorySMSInsertBO.SMS_TIMER_CONFIG_ID = tc.SMS_TIMER_CONFIG_ID;
                                                objHistorySMSInsertBO.IS_ON_SCHEDULE = true;
                                                objHistorySMSInsertBO.SEND_TIME = tc.SEND_TIME;
                                                hasTimerMessage = true;
                                            }
                                            lstHistorySMSInsertBO.Add(objHistorySMSInsertBO);

                                            //insert SMSCommunication
                                            /*objSMSCommunicationInsertBO = new SMSCommunicationInsertBO();
                                            objSMSCommunicationInsertBO.HistoryIndex = iHistoryIndex;
                                            objSMSCommunicationInsertBO.ReceiverID = objEmployeeReceiver.TeacherID;
                                            objSMSCommunicationInsertBO.SenderID = senderID;
                                            objSMSCommunicationInsertBO.SenderGroup = senderGroup;
                                            objSMSCommunicationInsertBO.SchoolID = schoolID;
                                            objSMSCommunicationInsertBO.IsAdmin = isAdmin;
                                            objSMSCommunicationInsertBO.Type = GlobalConstants.COMMON_SMS_COMUNICATION_TEACHER_TO_TEACHER;
                                            objSMSCommunicationInsertBO.IsRead = false;
                                            objSMSCommunicationInsertBO.CreateDate = dateTimeNow;
                                            objSMSCommunicationInsertBO.IsShow = true;
                                            objSMSCommunicationInsertBO.PartitionSchoolID = partitionSMSCommunication;
                                            lstSMSCommunicationInsertBO.Add(objSMSCommunicationInsertBO);*/

                                            //insert MT
                                            objMTInsertBO = new SMS_MT();
                                            objMTInsertBO.HISTORY_RAW_ID = guid;
                                            if (isSignMsg)
                                            {
                                                objMTInsertBO.CONTENT_TYPE = "1"; // có dấu
                                                objMTInsertBO.SMS_CONTENT = content;
                                            }
                                            else
                                            {
                                                objMTInsertBO.CONTENT_TYPE = "0"; // ko dấu
                                                objMTInsertBO.SMS_CONTENT = contentStripVNSign;
                                            }
                                            objMTInsertBO.STATUS = GlobalConstants.MT_STATUS_NOT_SEND;
                                            objMTInsertBO.TIME_SEND_REQUEST = dateTimeNow;
                                            objMTInsertBO.USER_ID = objEmployeeReceiver.Mobile;
                                            objMTInsertBO.UNIT_ID = schoolID;
                                            objMTInsertBO.SERVICE_ID = brandName;
                                            if (isTimerUsed)
                                            {
                                                objMTInsertBO.SMS_TIMER_CONFIG_ID = tc.SMS_TIMER_CONFIG_ID;
                                                hasTimerMessage = true;
                                            }
                                            lstMTInsertBO.Add(objMTInsertBO);

                                            iHistoryIndex++;

                                        }
                                    }
                                    #endregion

                                    #region bulk insert
                                    if (lstHistorySMSInsertBO.Count > 0)
                                    {
                                        int res = SMSHistoryBusiness.BulkInsert(lstHistorySMSInsertBO, this.ColumnHistoryMappings(), "HISTORY_ID");

                                        if (res > 0)
                                        {
                                            //lay list historyID vua insert
                                            List<long> lstHistoryID = (from h in this.SMSHistoryBusiness.All
                                                                       where h.SENDER_GROUP == senderGroup
                                                                       && h.PARTITION_ID == partitionSMSHistory
                                                                       && h.SCHOOL_ID == schoolID
                                                                       select h.HISTORY_ID).ToList();
                                            if (lstHistoryID.Count > 0)
                                            {
                                                int resMT = MTBusiness.BulkInsert(lstMTInsertBO, this.ColumnMTMappings());
                                                if (resMT == 0)
                                                {
                                                    
                                                    List<SMS_HISTORY> lstHistorySMS = (from h in this.SMSHistoryBusiness.All
                                                                                      where h.SENDER_GROUP == senderGroup
                                                                                       && h.PARTITION_ID == partitionSMSHistory
                                                                                      && h.SCHOOL_ID == schoolID
                                                                                      select h).ToList();
                                                    this.SMSHistoryBusiness.DeleteAll(lstHistorySMS);
                                                    this.Save();

                                                    resultBO.isError = true;
                                                    resultBO.ErrorMsg = "Có lỗi xảy ra trong quá trình gửi thư. Thầy/cô vui lòng nhấn F5 và thực hiện gửi lại!";
                                                    return resultBO;
                                                }
                                            }
                                        }
                                    }
                                    else
                                    {
                                        resultBO.isError = true;
                                        resultBO.ErrorMsg = "Có lỗi xảy ra trong quá trình gửi thư. Thầy/cô vui lòng nhấn F5 và thực hiện gửi lại!";
                                        return resultBO;
                                    }
                                    #endregion

                                    #region update smsCommunicationGroup
                                    /*List<SMSCommunication> lstSMSCommunicationUpdateGroup = (from s in this.Context.SMSCommunications
                                                                                             where s.SenderGroup == senderGroup && s.IsShow == true
                                                                                             && s.PartitionSchoolID == partitionSMSCommunication
                                                                                             && s.SchoolID == schoolID
                                                                                             select s).ToList();

                                    SMSCommunication objSMSCommunication = null;
                                    dic["dbEdu"] = this.Context;

                                    for (int i = lstSMSCommunicationUpdateGroup.Count - 1; i >= 0; i--)
                                    {
                                        objSMSCommunication = lstSMSCommunicationUpdateGroup[i];
                                        dic["teacherID"] = senderID;
                                        dic["headTeacherID"] = objSMSCommunication.HeadTeacherSenderID;
                                        dic["pupilProfileID"] = objSMSCommunication.ReceiverID;
                                        dic["iSMSCommunicationID"] = objSMSCommunication.SMSCommunicationID;
                                        UpdateSMSGroupBulkInsert(dic);
                                    }*/
                                    #endregion

                                    #region cap nhat tin nhan theo tung thue bao
                                    List<PupilContract> lstSent = lstPupilContract.Where(a => a.IsSent.HasValue && a.IsSent.Value == true).ToList();
                                    PupilContract itmSent = null;
                                    SMS_PARENT_SENT_DETAIL sentItem = null;
                                    List<long> LstContractDetailID = lstSent.Select(o => o.SMSParentContactDetailID).Distinct().ToList();
                                    List<SMS_PARENT_SENT_DETAIL> lstPupilSentDetail = qSMSParentSendDetail.Where(c => LstContractDetailID.Contains(c.SMS_PARENT_CONTRACT_DETAIL_ID)).ToList();

                                    List<SMS_PARENT_SENT_DETAIL> lstSmsParentSentDetailInsert = new List<SMS_PARENT_SENT_DETAIL>();
                                    for (int i = 0, SentCnt = lstSent.Count; i < SentCnt; i++)
                                    {
                                        itmSent = lstSent[i];
                                        if (itmSent.SMSParentSentDetailID > 0)
                                        {
                                            sentItem = lstPupilSentDetail.FirstOrDefault(a => a.SMS_PARENT_SENT_DETAIL_ID == itmSent.SMSParentSentDetailID);
                                            if (sentItem != null)
                                            {
                                                if (isTimerUsed)
                                                {
                                                    //Luu lai quy tin cu
                                                    SMS_TIMER_TRANSACTION tt = new SMS_TIMER_TRANSACTION();
                                                    tt.SCHOOL_ID = schoolID;
                                                    tt.TRANSACTION_TYPE = GlobalConstantsEdu.TIMER_TRANSACTION_SENT_DETAIL;
                                                    tt.IS_ACTIVE = true;
                                                    tt.SMS_PARENT_SENT_DETAIL_ID = sentItem.SMS_PARENT_SENT_DETAIL_ID;
                                                    tt.SENT_TOTAL = itmSent.SentSMS - sentItem.SENT_TOTAL;
                                                    tt.TIMER_CONFIG_ID = tc.SMS_TIMER_CONFIG_ID;

                                                    this.TimerTransactionBusiness.Insert(tt);
                                                }
                                                sentItem.UPDATED_DATE = dateTimeNow;
                                                sentItem.SENT_TOTAL = itmSent.SentSMS;
                                                this.SMSParentSentDetailBusiness.Update(sentItem);
                                            }
                                        }
                                        else
                                        {
                                            sentItem = new SMS_PARENT_SENT_DETAIL();
                                            sentItem.SEMESTER = semester;
                                            sentItem.YEAR = academicYearBO.Year;
                                            sentItem.CREATED_DATE = dateTimeNow;
                                            sentItem.SMS_PARENT_CONTRACT_DETAIL_ID = itmSent.SMSParentContactDetailID;
                                            sentItem.SENT_TOTAL = itmSent.SentSMS;
                                            sentItem.SCHOOL_ID = schoolID;
                                            sentItem.PARTITION_ID = partitionSMSHistory;
                                            this.SMSParentSentDetailBusiness.Insert(sentItem);
                                            lstSmsParentSentDetailInsert.Add(sentItem);

                                        }
                                    }
                                    #endregion

                                    #region Cap nhat quy tin cua tung lop - HoanTV5 20170506
                                    int? CountSMStmp = 0;
                                    bool breakout = false;
                                    for (int i = lstClassSent.Count - 1; i >= 0; i--)
                                    {
                                        vClassID = lstClassSent[i];

                                        lstPupilIDInClass = lstPupilOfClass.Where(o => o.ClassID == vClassID).Select(o => o.PupilID).ToList();
                                        CountSMStmp = 0;
                                        //objClassDetail = lstClassDetails.FirstOrDefault(c => c.AcademicYearID == academicYearBO.AcademicYearID
                                        //            && c.SchoolID == schoolID && c.Year == academicYearBO.Year && c.Semester == semester && c.ClassID == vClassID);
                                        lstClassDetailstmp = lstClassDetails.Where(c => c.ACADEMIC_YEAR_ID == academicYearBO.AcademicYearID
                                                    && c.SCHOOL_ID == schoolID && c.YEAR == academicYearBO.Year && c.CLASS_ID == vClassID).OrderBy(c => c.SEMESTER).ToList();
                                        for (int j = 0; j < lstClassDetailstmp.Count; j++)
                                        {
                                            objClassDetail = lstClassDetailstmp[j];
                                            SMS_TIMER_TRANSACTION tt = new SMS_TIMER_TRANSACTION();
                                            // cap nhat quy tin khong tinh goi co gioi han va cac thue bao No cuoc
                                            int numSMSNeedSend = countSMS * lstPupilContract.Count(o => lstPupilIDInClass.Contains(o.PupilFileID) && o.IsLimitPackage != true
                                                && o.Status != GlobalConstantsEdu.STATUS_UNPAID) - CountSMStmp.Value;
                                            //tru quy tin lan luot, het quy HK thi tru sang quy tin CN
                                            CountSMStmp = objClassDetail.TOTAL_SMS - objClassDetail.SENT_TOTAL;
                                            if (CountSMStmp <= numSMSNeedSend)
                                            {
                                                objClassDetail.SENT_TOTAL = objClassDetail.TOTAL_SMS;
                                                tt.SMS_NUMBER_ADDED = CountSMStmp.Value;
                                            }
                                            else
                                            {
                                                objClassDetail.SENT_TOTAL += numSMSNeedSend;
                                                tt.SMS_NUMBER_ADDED = numSMSNeedSend;
                                                breakout = true;
                                            }
                                            //objClassDetail.SentTotal += numSMSNeedSend;
                                            objClassDetail.UPDATED_DATE = DateTime.Now;
                                            if (objClassDetail.SMS_PARENT_CONTRACT_CLASS_ID > 0)
                                            {
                                                this.SMSParentContractInClassDetailBusiness.Update(objClassDetail);
                                            }

                                            //Luu lai quy tin cu
                                            tt.SCHOOL_ID = schoolID;
                                            tt.TRANSACTION_TYPE = GlobalConstantsEdu.TIMER_TRANSACTION_CONTRACT_IN_CLASS;
                                            tt.IS_ACTIVE = true;
                                            tt.SMS_PARENT_CONTRACT_CLASS_ID = objClassDetail.SMS_PARENT_CONTRACT_CLASS_ID;
                                            tt.TIMER_CONFIG_ID = tc.SMS_TIMER_CONFIG_ID;
                                            this.TimerTransactionBusiness.Insert(tt);
                                            if (breakout)
                                            {
                                                break;
                                            }
                                        }
                                    }
                                    #endregion

                                    // enabled detectChange and validateOnSave
                                    this.Save();

                                    if (isTimerUsed && lstSmsParentSentDetailInsert.Count > 0)
                                    {
                                        for (int i = 0; i < lstSmsParentSentDetailInsert.Count; i++)
                                        {
                                            SMS_PARENT_SENT_DETAIL ssd = lstSmsParentSentDetailInsert[i];
                                            //Luu lai quy tin cu
                                            SMS_TIMER_TRANSACTION tt = new SMS_TIMER_TRANSACTION();
                                            tt.SCHOOL_ID = schoolID;
                                            tt.TRANSACTION_TYPE = GlobalConstantsEdu.TIMER_TRANSACTION_SENT_DETAIL;
                                            tt.IS_ACTIVE = true;
                                            tt.SMS_PARENT_SENT_DETAIL_ID = ssd.SMS_PARENT_SENT_DETAIL_ID;
                                            tt.SENT_TOTAL = ssd.SENT_TOTAL;
                                            tt.TIMER_CONFIG_ID = tc.SMS_TIMER_CONFIG_ID;

                                            this.TimerTransactionBusiness.Insert(tt);

                                        }
                                        this.TimerTransactionBusiness.Save();
                                    }

                                    // Cap nhat thong tin 
                                    resultBO.NumSendSuccess = countNumOfSend;
                                    // danh sach cac lop khong gui
                                    resultBO.LstClassNotSent = lstClassNotSent;
                                    return resultBO;
                                    #endregion
                                }
                                else
                                {
                                    #region gui toan khoi voi cac ban tin khac trao doi
                                    List<EducationLevel> levelBOList = null;
                                    // AnhVD9 20150106 - Bo sung thong tin lop de kiem tra chi gui cho lop con quy tin
                                    if (sendAllSchool && iEducationLevelID == 0)
                                    {
                                        levelBOList = EducationLevelBusiness.GetByGrade(Grade).ToList();
                                    }
                                    // AnhVD9 20150106 - Bo sung thong tin lop de kiem tra chi gui cho lop con quy tin
                                    List<PupilOfClass> lstPupilOfClass = new List<PupilOfClass>();
                                    IDictionary<string, object> dicClass = new Dictionary<string, object>();
                                    if (levelBOList == null)
                                    {
                                        dicClass.Add("SchoolID", schoolID);
                                        dicClass.Add("AcademicYearID", academicYearBO.AcademicYearID);
                                        dicClass.Add("EducationLevelID", iEducationLevelID);
                                        lstPupilOfClass = PupilOfClassBusiness.Search(dicClass).Where(p => (p.Status == GlobalConstants.PUPIL_STATUS_STUDYING || p.Status == GlobalConstants.PUPIL_STATUS_GRADUATED)).Distinct().ToList();
                                        if (lstPupilOfClass == null || lstPupilOfClass.Count == 0)
                                        {
                                            resultBO.isError = false;
                                            resultBO.NumSendSuccess = 0;
                                            return resultBO;
                                        }
                                    }
                                    else
                                    {
                                        dicClass.Add("SchoolID", schoolID);
                                        dicClass.Add("AcademicYearID", academicYearBO.AcademicYearID);
                                        lstPupilOfClass = PupilOfClassBusiness.Search(dicClass).Where(p => (p.Status == GlobalConstants.PUPIL_STATUS_STUDYING || p.Status == GlobalConstants.PUPIL_STATUS_GRADUATED)).Distinct().ToList();
                                    }

                                    if (lstPupilOfClass == null || lstPupilOfClass.Count == 0)
                                    {
                                        resultBO.isError = false;
                                        resultBO.NumSendSuccess = 0;
                                        return resultBO;
                                    }

                                    List<int> lstPupilIDInLevel = lstPupilOfClass.Select(o => o.PupilID).Distinct().ToList();
                                    if (lstReceiverID != null && ((lstReceiverID.Count != contentList.Count) || (lstReceiverID.Except(lstPupilIDInLevel).Count() > 0)))
                                    {
                                        resultBO.isError = true;
                                        resultBO.ErrorMsg = "Có lỗi xảy ra trong quá trình gửi thư. Thầy/cô vui lòng nhấn F5 và thực hiện gửi lại!";
                                        return resultBO;
                                    }

                                    List<PupilContent> lstPupilContent = new List<PupilContent>();
                                    PupilContent objPupilContent = null;
                                    for (int i = 0, size = lstReceiverID.Count; i < size; i++)
                                    {
                                        objPupilContent = new PupilContent();
                                        objPupilContent.PupilFileID = lstReceiverID[i];
                                        objPupilContent.Content = contentList[i].Trim();
                                        objPupilContent.SMS_CNT = objPupilContent.Content.countNumSMS(isSignMsg);
                                        objPupilContent.ShortContent = lstShortContent[i].Trim();
                                        lstPupilContent.Add(objPupilContent);
                                    }

                                    #region check truong hop 2000 parameter
                                    List<PupilContract> lstPupilContract = new List<PupilContract>();
                                    List<PupilProfileBO> pupilProfileList = new List<PupilProfileBO>();
                                    int numOfSplit = 2000;

                                    if (lstReceiverID.Count > numOfSplit)
                                    {
                                        int numOfPupil = (int)Math.Ceiling((decimal)lstReceiverID.Count / numOfSplit);
                                        List<int> lstPupilIDTmp = null;

                                        for (int i = 0; i < numOfPupil; i++)
                                        {
                                            lstPupilIDTmp = lstReceiverID.Skip((i) * numOfSplit).Take(numOfSplit).ToList();

                                            List<PupilProfileBO> lstTmpPupilProfileBO = PupilProfileBusiness.GetListPupilSendSMS(lstPupilIDTmp, AcademicYearId);

                                            pupilProfileList.AddRange(lstTmpPupilProfileBO);
                                        }
                                    }
                                    else
                                    {
                                        pupilProfileList = PupilProfileBusiness.GetListPupilSendSMS(lstReceiverID, AcademicYearId);
                                    }

                                    if (lstPupilIDInLevel.Count > numOfSplit)
                                    {
                                        int numOfPupil = (int)Math.Ceiling((decimal)lstPupilIDInLevel.Count / numOfSplit);
                                        List<int> lstPupilIDTmp = null;
                                        for (int i = 0; i < numOfPupil; i++)
                                        {
                                            lstPupilIDTmp = lstPupilIDInLevel.Skip((i) * numOfSplit).Take(numOfSplit).ToList();
                                            //danh sach hoc sinh                 
                                            var pupilQuery = (from sc in this.SMSParentContractBusiness.All
                                                              join scd in this.SMSParentContractDetailBusiness.All on sc.SMS_PARENT_CONTRACT_ID equals scd.SMS_PARENT_CONTRACT_ID
                                                              join pdtl in this.ServicePackageDetailBusiness.All on scd.SERVICE_PACKAGE_ID equals pdtl.SERVICE_PACKAGE_ID
                                                              join spdd in this.ServicePackageDeclareBusiness.All on scd.SERVICE_PACKAGE_ID equals spdd.SERVICE_PACKAGE_ID
                                                              join sentDtl in qSMSParentSendDetail on scd.SMS_PARENT_CONTRACT_DETAIL_ID equals sentDtl.SMS_PARENT_CONTRACT_DETAIL_ID into gj
                                                              from x in gj.DefaultIfEmpty(null)
                                                              where sc.SCHOOL_ID == schoolID && sc.PARTITION_ID == partitionSMSParentContract
                                                              && (scd.SUBSCRIPTION_STATUS == GlobalConstantsEdu.STATUS_REGISTED || scd.SUBSCRIPTION_STATUS == GlobalConstantsEdu.STATUS_UNPAID) // Bổ sung gửi tin cho các thuê bao Nợ cước
                                                              && scd.IS_ACTIVE == true && sc.STATUS == GlobalConstantsEdu.SMS_PARENT_CONTRACT_STATUS_IS_ACCEPT && (sc.IS_DELETED == false || sc.IS_DELETED == null)
                                                              && lstPupilIDTmp.Contains(sc.PUPIL_ID)
                                                              &&
                                                              (
                                                               (semester == 1 && (scd.SUBSCRIPTION_TIME == GlobalConstantsEdu.CONTRACTDETAIL_SUBSCRIPTIONTIME_FIRST_SMESTER || scd.SUBSCRIPTION_TIME == GlobalConstantsEdu.CONTRACTDETAIL_SUBSCRIPTIONTIME_ALLYEAR))
                                                               ||
                                                               (semester == 2 && (scd.SUBSCRIPTION_TIME == GlobalConstantsEdu.CONTRACTDETAIL_SUBSCRIPTIONTIME_SECOND_SMESTER || scd.SUBSCRIPTION_TIME == GlobalConstantsEdu.CONTRACTDETAIL_SUBSCRIPTIONTIME_ALLYEAR))
                                                              )
                                                              && scd.YEAR_ID == academicYearBO.Year
                                                              && spdd.YEAR == academicYearBO.Year
                                                              && spdd.STATUS == true
                                                              select new PupilContractDetail
                                                              {
                                                                  PupilFileID = sc.PUPIL_ID,
                                                                  Mobile = scd.SUBSCRIBER,
                                                                  SMSParentContactDetailID = scd.SMS_PARENT_CONTRACT_DETAIL_ID,
                                                                  MaxSMS = (pdtl.MAX_SMS.HasValue) ? scd.SUBSCRIPTION_TIME == 3 ? pdtl.MAX_SMS.Value * 2 : pdtl.MAX_SMS.Value : 0,
                                                                  SentSMS = ((x == null) ? 0 : (x.SENT_TOTAL)),
                                                                  SMSParentSentDetailID = ((x == null) ? 0 : x.SMS_PARENT_SENT_DETAIL_ID),
                                                                  ServicePackageID = pdtl.SERVICE_PACKAGE_ID,
                                                                  IsLimitPackage = spdd.IS_LIMIT,
                                                                  RegisterDate = scd.CREATED_TIME,
                                                                  Status = scd.SUBSCRIPTION_STATUS,
                                                              });
                                            //Xu ly lai query de lay ra tong so tin nhan cua tung thue bao dang ky.
                                            lstPupilContract.AddRange(pupilQuery
                                                .GroupBy(x => new { x.PupilFileID, x.SMSParentContactDetailID, x.Mobile, x.SentSMS, x.SMSParentSentDetailID, x.ServicePackageID, x.RegisterDate, x.IsLimitPackage, x.Status })
                                                .Select(y => new PupilContract
                                                {
                                                    Mobile = y.Key.Mobile,
                                                    SMSParentContactDetailID = y.Key.SMSParentContactDetailID,
                                                    PupilFileID = y.Key.PupilFileID,
                                                    SentSMS = y.Key.SentSMS,
                                                    IsSent = false,
                                                    SMSParentSentDetailID = y.Key.SMSParentSentDetailID,
                                                    ServicePackageID = y.Key.ServicePackageID,
                                                    IsLimitPackage = y.Key.IsLimitPackage,
                                                    RegisterDate = y.Key.RegisterDate,
                                                    TotalSMS = y.Sum(a => a.MaxSMS),
                                                    Status = y.Key.Status
                                                }).ToList());
                                        }
                                    }
                                    else
                                    {
                                        //danh sach hoc sinh                 
                                        var pupilQuery = (from sc in this.SMSParentContractBusiness.All
                                                          join scd in this.SMSParentContractDetailBusiness.All on sc.SMS_PARENT_CONTRACT_ID equals scd.SMS_PARENT_CONTRACT_ID
                                                          join pdtl in this.ServicePackageDetailBusiness.All on scd.SERVICE_PACKAGE_ID equals pdtl.SERVICE_PACKAGE_ID
                                                          join spdd in this.ServicePackageDeclareBusiness.All on scd.SERVICE_PACKAGE_ID equals spdd.SERVICE_PACKAGE_ID
                                                          join sentDtl in qSMSParentSendDetail on scd.SMS_PARENT_CONTRACT_DETAIL_ID equals sentDtl.SMS_PARENT_CONTRACT_DETAIL_ID into gj
                                                          from x in gj.DefaultIfEmpty(null)
                                                          where sc.SCHOOL_ID == schoolID && sc.PARTITION_ID == partitionSMSParentContract
                                                          && (scd.SUBSCRIPTION_STATUS == GlobalConstantsEdu.STATUS_REGISTED || scd.SUBSCRIPTION_STATUS == GlobalConstantsEdu.STATUS_UNPAID) // Bổ sung gửi tin cho các thuê bao Nợ cước
                                                          && scd.IS_ACTIVE == true && sc.STATUS == GlobalConstantsEdu.SMS_PARENT_CONTRACT_STATUS_IS_ACCEPT && (sc.IS_DELETED == false || sc.IS_DELETED == null)
                                                          && lstPupilIDInLevel.Contains(sc.PUPIL_ID)
                                                          &&
                                                          (
                                                           (semester == 1 && (scd.SUBSCRIPTION_TIME == GlobalConstantsEdu.CONTRACTDETAIL_SUBSCRIPTIONTIME_FIRST_SMESTER || scd.SUBSCRIPTION_TIME == GlobalConstantsEdu.CONTRACTDETAIL_SUBSCRIPTIONTIME_ALLYEAR))
                                                           ||
                                                           (semester == 2 && (scd.SUBSCRIPTION_TIME == GlobalConstantsEdu.CONTRACTDETAIL_SUBSCRIPTIONTIME_SECOND_SMESTER || scd.SUBSCRIPTION_TIME == GlobalConstantsEdu.CONTRACTDETAIL_SUBSCRIPTIONTIME_ALLYEAR))
                                                          )
                                                          && scd.YEAR_ID == academicYearBO.Year
                                                          && spdd.YEAR == academicYearBO.Year
                                                            && spdd.STATUS == true
                                                          select new PupilContractDetail
                                                          {
                                                              PupilFileID = sc.PUPIL_ID,
                                                              Mobile = scd.SUBSCRIBER,
                                                              SMSParentContactDetailID = scd.SMS_PARENT_CONTRACT_DETAIL_ID,
                                                              MaxSMS = (pdtl.MAX_SMS.HasValue) ? scd.SUBSCRIPTION_TIME == 3 ? pdtl.MAX_SMS.Value * 2 : pdtl.MAX_SMS.Value : 0,
                                                              SentSMS = ((x == null) ? 0 : (x.SENT_TOTAL)),
                                                              SMSParentSentDetailID = ((x == null) ? 0 : x.SMS_PARENT_SENT_DETAIL_ID),
                                                              ServicePackageID = pdtl.SERVICE_PACKAGE_ID,
                                                              IsLimitPackage = spdd.IS_LIMIT,
                                                              RegisterDate = scd.CREATED_TIME,
                                                              Status = scd.SUBSCRIPTION_STATUS,
                                                          });
                                        //Xu ly lai query de lay ra tong so tin nhan cua tung thue bao dang ky.
                                        lstPupilContract = pupilQuery
                                            .GroupBy(x => new { x.PupilFileID, x.SMSParentContactDetailID, x.Mobile, x.SentSMS, x.SMSParentSentDetailID, x.ServicePackageID, x.RegisterDate, x.IsLimitPackage, x.Status })
                                            .Select(y => new PupilContract
                                            {
                                                Mobile = y.Key.Mobile,
                                                SMSParentContactDetailID = y.Key.SMSParentContactDetailID,
                                                PupilFileID = y.Key.PupilFileID,
                                                SentSMS = y.Key.SentSMS,
                                                IsSent = false,
                                                SMSParentSentDetailID = y.Key.SMSParentSentDetailID,
                                                ServicePackageID = y.Key.ServicePackageID,
                                                IsLimitPackage = y.Key.IsLimitPackage,
                                                RegisterDate = y.Key.RegisterDate,
                                                TotalSMS = y.Sum(a => a.MaxSMS),
                                                Status = y.Key.Status
                                            }).ToList();
                                    }

                                    #endregion

                                    //Lay danh sach lop va duyet theo tung lop - AnhVD9 20150106
                                    List<int> lstClassID = lstPupilOfClass.Where(o => lstReceiverID.Contains(o.PupilID)).Select(o => o.ClassID).Distinct().ToList();
                                    SMS_PARENT_CONTRACT_CLASS objClassDetail = null;
                                    List<SMS_PARENT_CONTRACT_CLASS> lstClassDetails = this.SMSParentContractInClassDetailBusiness.All.Where(c => c.ACADEMIC_YEAR_ID == academicYearBO.AcademicYearID
                                                    && c.SCHOOL_ID == schoolID && c.YEAR == academicYearBO.Year
                                                    && (c.SEMESTER == semester || c.SEMESTER == GlobalConstants.SEMESTER_OF_YEAR_ALL)).ToList();
                                    List<SMS_PARENT_CONTRACT_CLASS> lstClassDetailstmp = null;

                                    List<int> lstPupilIDInClass = new List<int>();
                                    List<int> lstClassSent = new List<int>();
                                    List<ClassBO> lstClassNotSent = new List<ClassBO>();
                                    ClassBO classNotSend = null;
                                    int vClassID = 0;
                                    for (int i = lstClassID.Count - 1; i >= 0; i--)
                                    {
                                        vClassID = lstClassID[i];
                                        lstPupilIDInClass = lstPupilOfClass.Where(o => o.ClassID == vClassID).Select(o => o.PupilID).ToList();
                                        lstClassDetailstmp = lstClassDetails.Where(o => o.CLASS_ID == vClassID).ToList();
                                        //objClassDetail = lstClassDetails.FirstOrDefault(o => o.ClassID == vClassID);

                                        if (lstClassDetailstmp.Count == 0)
                                        {
                                            int TotalSMSInClass = lstPupilContract.Where(o => lstPupilIDInClass.Contains(o.PupilFileID) && o.IsLimitPackage != true
                                                && o.Status == GlobalConstantsEdu.STATUS_REGISTED).Sum(o => o.TotalSMS);
                                            int TotalSMSSent = lstPupilContract.Where(o => lstPupilIDInClass.Contains(o.PupilFileID) && o.IsLimitPackage != true
                                                && o.Status == GlobalConstantsEdu.STATUS_REGISTED).Sum(o => o.SentSMS);
                                            objClassDetail = new SMS_PARENT_CONTRACT_CLASS();
                                            objClassDetail.CLASS_ID = vClassID;
                                            objClassDetail.ACADEMIC_YEAR_ID = academicYearBO.AcademicYearID;
                                            objClassDetail.SCHOOL_ID = schoolID;
                                            objClassDetail.YEAR = academicYearBO.Year;
                                            objClassDetail.SEMESTER = (byte)semester;
                                            objClassDetail.TOTAL_SMS = TotalSMSInClass;
                                            objClassDetail.SENT_TOTAL = TotalSMSSent;
                                            objClassDetail.CREATED_DATE = DateTime.Now;
                                            lstClassDetails.Add(objClassDetail);
                                            if (TotalSMSInClass > 0) this.SMSParentContractInClassDetailBusiness.Insert(objClassDetail);
                                        }

                                        lstPupilIDInClass = lstPupilContract.Where(o => lstPupilIDInClass.Contains(o.PupilFileID) && o.IsLimitPackage != true).Select(o => o.PupilFileID).ToList();
                                        // so tin can gui trong lop
                                        int numSMSNeedSend = lstPupilContent.Where(o => lstPupilIDInClass.Contains(o.PupilFileID)).Sum(o => o.SMS_CNT * lstPupilContract.Count(p => p.PupilFileID == o.PupilFileID && p.IsLimitPackage != true && p.Status == GlobalConstantsEdu.STATUS_REGISTED));
                                        // Neu khong du quy tin va khong co thue bao No cuoc thi ko gui
                                        if ((lstClassDetailstmp.Sum(o => o.TOTAL_SMS) - lstClassDetailstmp.Sum(o => o.SENT_TOTAL)) < numSMSNeedSend)
                                        {
                                            lstReceiverID = lstReceiverID.Except(lstPupilIDInClass).ToList();
                                            lstReceiverID.Union(lstPupilContract.Where(o => lstPupilIDInClass.Contains(o.PupilFileID) && o.Status == GlobalConstantsEdu.STATUS_UNPAID).Select(o => o.PupilFileID).ToList());
                                            classNotSend = new ClassBO();
                                            classNotSend.ClassID = vClassID;
                                            classNotSend.ClassName = lstPupilOfClass.FirstOrDefault(o => o.ClassID == vClassID).ClassProfile.DisplayName;
                                            lstClassNotSent.Add(classNotSend);
                                        }
                                        else lstClassSent.Add(vClassID);
                                    }
                                    // End AnhVD9
                                    PupilProfileBO pupilProfileBO = null;

                                    int intPupilFileID = 0;
                                    PupilContract objPupilContractUspResult = null;
                                    List<PupilSubscriber> lstPupilSubscriber = null;
                                    PupilSubscriber objPupilSubscriber = null;
                                    int smsParentContractDetailID = 0;
                                    int countNumOfSend = 0;
                                    List<SMS_MT> lstMTInsertBO = new List<SMS_MT>();
                                    //List<SMSCommunicationInsertBO> lstSMSCommunicationInsertBO = new List<SMSCommunicationInsertBO>();
                                    List<SMS_HISTORY> lstHistorySMSInsertBO = new List<SMS_HISTORY>();
                                    SMS_MT objMTInsertBO = null;
                                    //SMSCommunicationInsertBO objSMSCommunicationInsertBO = null;
                                    SMS_HISTORY objHistorySMSInsertBO = null;
                                    SMS_SERVICE_PACKAGE_DECLARE detailLimitSP = null;
                                    bool isHasManyMobileReceiver = false;
                                    bool isSendOnWebOK = false;
                                    //danh dau record history de lay HistoryID
                                    int iHistoryIndex = 0;
                                    string fullNameSender = SchoolName;

                                    for (int i = lstReceiverID.Count - 1; i >= 0; i--)
                                    {
                                        intPupilFileID = lstReceiverID[i];
                                        isHasManyMobileReceiver = false;
                                        objPupilContent = lstPupilContent.FirstOrDefault(p => p.PupilFileID == intPupilFileID);

                                        // set content
                                        content = objPupilContent.Content;
                                        shortContent = objPupilContent.ShortContent;
                                        contentStripVNSign = ExtensionMethods.ConvertLineToSpace(content.StripVNSign().StripVNSignComposed());
                                        // end set content
                                        countSMS = objPupilContent.SMS_CNT;
                                        isSendOnWebOK = false;
                                        lstPupilSubscriber = (from pc in lstPupilContract
                                                              where pc.PupilFileID == intPupilFileID
                                                              select new PupilSubscriber
                                                              {
                                                                  Subscriber = pc.Mobile,
                                                                  SMSParentContractDetailID = pc.SMSParentContactDetailID
                                                              }).ToList();

                                        for (int k = lstPupilSubscriber.Count - 1; k >= 0; k--)
                                        {
                                            objPupilSubscriber = lstPupilSubscriber[k];
                                            // Chỉ cho phép gửi tin có dấu với các đầu số cho trước
                                            if (isSignMsg && !objPupilSubscriber.Subscriber.isAllowSendSignSMSPhone()) continue;
                                            objPupilContractUspResult = (from pc in lstPupilContract where pc.Mobile == objPupilSubscriber.Subscriber && pc.PupilFileID == intPupilFileID && pc.SMSParentContactDetailID == objPupilSubscriber.SMSParentContractDetailID select pc).FirstOrDefault();
                                            pupilProfileBO = pupilProfileList.FirstOrDefault(p => p.PupilProfileID == intPupilFileID);
                                            if (pupilProfileBO == null) continue;

                                            //Not found from Contract Detail
                                            if (objPupilContractUspResult == null) continue;

                                            smsParentContractDetailID = (int)objPupilContractUspResult.SMSParentContactDetailID;
                                            if (!objPupilSubscriber.Subscriber.CheckMobileNumber()) continue;
                                            //Lay brandName theo thue bao
                                            brandName = GetBrandNamebyMobile(objPupilSubscriber.Subscriber, lstPreNumberTelco, lstBrandName);

                                            #region Kiem tra gui tin voi goi dich vu GIOI HAN
                                            // Neu la goi trial
                                            if (objPupilContractUspResult.IsLimitPackage.HasValue && objPupilContractUspResult.IsLimitPackage.Value)
                                            {

                                                //// Kiem tra thoi gian cho phep gui trong thoi han, ke tu ngay dau noi
                                                detailLimitSP = LstLimitPackage.FirstOrDefault(o => o.SERVICE_PACKAGE_ID == objPupilContractUspResult.ServicePackageID
                                                    && o.EFFECTIVE_DAYS > 0 &&
                                                    ((o.FROM_DATE.HasValue && o.FROM_DATE.Value.Date <= objPupilContractUspResult.RegisterDate.Date &&
                                                    o.TO_DATE.HasValue && o.TO_DATE.Value.Date >= objPupilContractUspResult.RegisterDate.Date)
                                                    || (!o.TO_DATE.HasValue && !o.FROM_DATE.HasValue)));
                                                if (detailLimitSP != null)
                                                {
                                                    if ((DateTime.Now - objPupilContractUspResult.RegisterDate.Date).TotalDays > detailLimitSP.EFFECTIVE_DAYS) continue;
                                                }

                                                //Kiem tra so luong tin nhan chi (cho phep gui khi con tin nhan)
                                                if (objPupilContractUspResult.TotalSMS - objPupilContractUspResult.SentSMS <= 0) continue;
                                            }
                                            #endregion

                                            // Cac goi No cuoc kiem tra con so luong tin
                                            if (objPupilContractUspResult.Status == GlobalConstantsEdu.STATUS_UNPAID && objPupilContractUspResult.TotalSMS - objPupilContractUspResult.SentSMS <= 0) continue;

                                            //insert historySMS
                                            objHistorySMSInsertBO = new SMS_HISTORY();
                                            objHistorySMSInsertBO.MOBILE = objPupilSubscriber.Subscriber;
                                            objHistorySMSInsertBO.IS_INTERNAL = objHistorySMSInsertBO.MOBILE.CheckMobileNumberVT();
                                            objHistorySMSInsertBO.RECEIVER_NAME = pupilProfileBO != null ? pupilProfileBO.FullName : string.Empty;
                                            if (typeHistory.HasValue && typeHistory.Value != 0) objHistorySMSInsertBO.SMS_TYPE_ID = typeHistory.Value;
                                            else objHistorySMSInsertBO.SMS_TYPE_ID = GlobalConstantsEdu.COMMON_HISTORYSMS_TO_COMMUNICATION_ID;
                                            objHistorySMSInsertBO.IS_CUSTOM_TYPE = isCustomType;
                                            objHistorySMSInsertBO.RECEIVE_TYPE = GlobalConstantsEdu.COMMON_HISTORY_RECEIVER_PARENT_ID;
                                            objHistorySMSInsertBO.CREATE_DATE = dateTimeNow;
                                            objHistorySMSInsertBO.SCHOOL_ID = schoolID;
                                            objHistorySMSInsertBO.YEAR = academicYearBO.Year;
                                            objHistorySMSInsertBO.SMS_CONTENT = content;
                                            objHistorySMSInsertBO.RECEIVER_ID = objPupilContractUspResult.PupilFileID;
                                            objHistorySMSInsertBO.SHORT_CONTENT = shortContent;
                                            objHistorySMSInsertBO.SENDER_NAME = fullNameSender;
                                            objHistorySMSInsertBO.SMS_CNT = countSMS;
                                            objHistorySMSInsertBO.SCHOOL_USERNAME = schoolUserName;
                                            objHistorySMSInsertBO.SMS_PARENTCONTRACT_DETAIL_ID = smsParentContractDetailID;
                                            if (inValidNumber) objHistorySMSInsertBO.FLAG_ID = GlobalConstantsEdu.COMMON_TABLE_SMSHISTORY_FLAG_INVALID_MOBILE;
                                            objHistorySMSInsertBO.CONTENT_TYPE = isSignMsg ? GlobalConstantsEdu.SMS_CONTENT_TYPE_UNICODE : GlobalConstantsEdu.SMS_CONTENT_TYPE_NOT_SIGN;
                                            objHistorySMSInsertBO.IS_SMS = true;
                                            objHistorySMSInsertBO.SENDER_GROUP = senderGroup;
                                            objHistorySMSInsertBO.PARTITION_ID = partitionSMSHistory;
                                            objHistorySMSInsertBO.IS_ADMIN = isAdmin;
                                            objHistorySMSInsertBO.SENDER_ID = senderID;
                                            Guid guid = Guid.NewGuid();
                                            objHistorySMSInsertBO.HISTORY_RAW_ID = guid;
                                            lstHistorySMSInsertBO.Add(objHistorySMSInsertBO);

                                            //insert MT
                                            objMTInsertBO = new SMS_MT();
                                            objMTInsertBO.HISTORY_RAW_ID = guid;
                                            objMTInsertBO.USER_ID = objPupilSubscriber.Subscriber;
                                            if (isSignMsg)
                                            {
                                                objMTInsertBO.CONTENT_TYPE = "1"; // có dấu
                                                objMTInsertBO.SMS_CONTENT = content;
                                            }
                                            else
                                            {
                                                objMTInsertBO.CONTENT_TYPE = "0"; // ko dấu
                                                objMTInsertBO.SMS_CONTENT = contentStripVNSign;
                                            }
                                            objMTInsertBO.STATUS = GlobalConstants.MT_STATUS_NOT_SEND;
                                            objMTInsertBO.TIME_SEND_REQUEST = dateTimeNow;
                                            objMTInsertBO.UNIT_ID = schoolID;
                                            objMTInsertBO.SERVICE_ID = brandName;
                                            lstMTInsertBO.Add(objMTInsertBO);
                                            //Chi tru khi gui qua MT
                                            objPupilContractUspResult.IsSent = true;
                                            objPupilContractUspResult.SentSMS += countSMS;
                                            isSendOnWebOK = true;
                                            iHistoryIndex++;

                                        }
                                        if (isSendOnWebOK)
                                        {
                                            countNumOfSend++;
                                        }
                                    }

                                    if (countNumOfSend == 0)
                                    {
                                        resultBO.isError = false;
                                        resultBO.NumSendSuccess = 0;
                                        return resultBO;
                                    }

                                    #region bulk insert
                                    if (lstHistorySMSInsertBO.Count > 0)
                                    {
                                        int res = SMSHistoryBusiness.BulkInsert(lstHistorySMSInsertBO, this.ColumnHistoryMappings(), "HISTORY_ID");
                                        if (res > 0)
                                        {
                                            //lay list historyID vua insert
                                            List<long> lstHistoryID = (from h in this.SMSHistoryBusiness.All
                                                                       where h.SENDER_GROUP == senderGroup
                                                                       && h.PARTITION_ID == partitionSMSHistory
                                                                       && h.SCHOOL_ID == schoolID
                                                                       select h.HISTORY_ID).ToList();
                                            if (lstHistoryID.Count > 0)
                                            {
                                                int resMT = MTBusiness.BulkInsert(lstMTInsertBO, this.ColumnMTMappings());
                                                if (resMT == 0)
                                                {
                                                   
                                                    List<SMS_HISTORY> lstHistorySMS = (from h in this.SMSHistoryBusiness.All
                                                                                      where h.SENDER_GROUP == senderGroup
                                                                                      && h.PARTITION_ID == partitionSMSHistory
                                                                                      && h.SCHOOL_ID == schoolID
                                                                                      select h).ToList();
                                                    this.SMSHistoryBusiness.DeleteAll(lstHistorySMS);
                                                    this.Save();

                                                    resultBO.isError = true;
                                                    resultBO.ErrorMsg = "Có lỗi xảy ra trong quá trình gửi thư. Thầy/cô vui lòng nhấn F5 và thực hiện gửi lại!";
                                                    return resultBO;
                                                }
                                            }
                                        }
                                    }
                                    else
                                    {
                                        resultBO.isError = true;
                                        resultBO.ErrorMsg = "Có lỗi xảy ra trong quá trình gửi thư. Thầy/cô vui lòng nhấn F5 và thực hiện gửi lại!";
                                        return resultBO;
                                    }
                                    #endregion

                                    #region cap nhat tin nhan theo tung thue bao/ cap nhat quy tin cua lop
                                    List<PupilContract> lstSent = lstPupilContract.Where(a => a.IsSent.HasValue && a.IsSent.Value == true).ToList();
                                    List<long> LstContractDetailID = lstSent.Select(o => o.SMSParentContactDetailID).Distinct().ToList();
                                    List<SMS_PARENT_SENT_DETAIL> lstPupilSentDetail = qSMSParentSendDetail.Where(c => LstContractDetailID.Contains(c.SMS_PARENT_CONTRACT_DETAIL_ID)).ToList();
                                    PupilContract itmSent = null;
                                    SMS_PARENT_SENT_DETAIL sentItem = null;
                                    for (int i = 0, SentCnt = lstSent.Count; i < SentCnt; i++)
                                    {
                                        itmSent = lstSent[i];
                                        if (itmSent.SMSParentSentDetailID > 0)
                                        {
                                            sentItem = lstPupilSentDetail.FirstOrDefault(a => a.SMS_PARENT_SENT_DETAIL_ID == itmSent.SMSParentSentDetailID);
                                            if (sentItem != null)
                                            {
                                                sentItem.UPDATED_DATE = dateTimeNow;
                                                sentItem.SENT_TOTAL = itmSent.SentSMS;
                                                this.SMSParentSentDetailBusiness.Update(sentItem);
                                            }
                                        }
                                        else
                                        {
                                            sentItem = new SMS_PARENT_SENT_DETAIL();
                                            sentItem.SEMESTER = semester;
                                            sentItem.YEAR = academicYearBO.Year;
                                            sentItem.CREATED_DATE = dateTimeNow;
                                            sentItem.SMS_PARENT_CONTRACT_DETAIL_ID = itmSent.SMSParentContactDetailID;
                                            sentItem.SENT_TOTAL = itmSent.SentSMS;
                                            sentItem.SCHOOL_ID = schoolID;
                                            sentItem.PARTITION_ID = partitionSMSHistory;
                                            this.SMSParentSentDetailBusiness.Insert(sentItem);
                                        }
                                    }

                                    // Cap nhat quy tin cua tung lop - HoanTV5 20170506
                                    //Tru lan luot quy tin theo hoc ky=>quy tin CN
                                    int? CountSMStmp = 0;
                                    bool breakout = false;
                                    for (int i = lstClassSent.Count - 1; i >= 0; i--)
                                    {
                                        vClassID = lstClassSent[i];
                                        CountSMStmp = 0;
                                        // cap nhat quy tin khong tinh goi co gioi han
                                        // danh sach hoc sinh trong lop
                                        lstPupilIDInClass = lstPupilOfClass.Where(o => o.ClassID == vClassID).Select(o => o.PupilID).ToList();
                                        // danh sach hoc sinh co HD tra tien trong lop va da gui thanh cong
                                        lstPupilIDInClass = lstPupilContract.Where(o => lstPupilIDInClass.Contains(o.PupilFileID) && o.IsLimitPackage != true && o.IsSent == true).Select(o => o.PupilFileID).ToList();
                                        lstClassDetailstmp = lstClassDetails.Where(c => c.ACADEMIC_YEAR_ID == academicYearBO.AcademicYearID
                                                    && c.SCHOOL_ID == schoolID && c.YEAR == academicYearBO.Year && c.CLASS_ID == vClassID).OrderBy(c => c.SEMESTER).ToList();
                                        for (int j = 0; j < lstClassDetailstmp.Count; j++)
                                        {
                                            objClassDetail = lstClassDetailstmp[j];
                                            // so tin da gui thanh cong trong lop
                                            int totalSentInClass = lstPupilContent.Where(o => lstPupilIDInClass.Contains(o.PupilFileID))
                                                .Sum(o => o.SMS_CNT * lstPupilContract.Count(p => p.PupilFileID == o.PupilFileID && p.IsLimitPackage != true
                                                    && p.Status == GlobalConstantsEdu.STATUS_REGISTED)) - CountSMStmp.Value;
                                            //tru quy tin lan luot, het quy HK thi tru sang quy tin CN
                                            CountSMStmp = objClassDetail.TOTAL_SMS - objClassDetail.SENT_TOTAL;
                                            if (CountSMStmp <= totalSentInClass)
                                            {
                                                objClassDetail.SENT_TOTAL = objClassDetail.TOTAL_SMS;
                                            }
                                            else
                                            {
                                                objClassDetail.SENT_TOTAL += totalSentInClass;
                                                breakout = true;
                                            }
                                            objClassDetail.UPDATED_DATE = DateTime.Now;
                                            if (objClassDetail.SMS_PARENT_CONTRACT_CLASS_ID > 0)
                                            {
                                                this.SMSParentContractInClassDetailBusiness.Update(objClassDetail);
                                            }
                                            if (breakout)
                                            {
                                                break;
                                            }
                                        }
                                    }
                                    this.Save();
                                    #endregion

                                    // Cap nhat thong tin 
                                    resultBO.NumSendSuccess = countNumOfSend;
                                    // danh sach cac lop khong gui
                                    resultBO.LstClassNotSent = lstClassNotSent;
                                    return resultBO;
                                    #endregion
                                }

                                #endregion
                            }
                            else if (contentList.Count > 0)
                            {
                                SMS_TIMER_CONFIG tc = new SMS_TIMER_CONFIG();
                                if (isTimerUsed)
                                {
                                    tc = new SMS_TIMER_CONFIG();
                                    tc.SMS_TIMER_CONFIG_ID = Guid.NewGuid();
                                    tc.CONTENT = content;
                                    tc.CREATE_TIME = DateTime.Now;
                                    tc.SCHOOL_ID = schoolID;
                                    tc.APPLIED_LEVEL = iLevelID;
                                    tc.PARTITION_ID = schoolID % 20;
                                    tc.SEND_TIME = sendTime.Value;
                                    tc.STATUS = GlobalConstantsEdu.TIMER_CONFIG_STATUS_ACTIVE;//chua gui
                                    tc.TARGET_ID = classID;
                                    tc.CONFIG_NAME = timerConfigName;
                                    tc.CONFIG_TYPE = GlobalConstantsEdu.TIMER_CONFIG_TYPE_CLASS;
                                    tc.SEND_ALL_CLASS = isAllClass;
                                    tc.SEND_UNICODE = isSignMsg;
                                    tc.IS_IMPORT = isSendImport;

                                    this.TimerConfigBusiness.Insert(tc);
                                }

                                #region gui cho phhs noi dung khac nhau doi voi tung hoc sinh trong 1 lop
                                int headTeacherID = EmployeeBusiness.GetHeadTeacherID(classID, academicYearBO.AcademicYearID, schoolID);
                                Employee teacherBO = null;
                                string fullNameSender = string.Empty;
                                if (isAdmin) fullNameSender = SchoolName;
                                else // giao vien
                                {
                                    teacherBO = EmployeeBusiness.Find(senderID);
                                    if (teacherBO != null) fullNameSender = teacherBO.FullName;
                                }

                                if (string.IsNullOrEmpty(fullNameSender))
                                {
                                    resultBO.isError = true;
                                    resultBO.ErrorMsg = "Có lỗi xảy ra trong quá trình gửi thư. Thầy/cô vui lòng nhấn F5 và thực hiện gửi lại!";
                                    return resultBO;
                                }
                                List<PupilProfile> lstPupilStudying = (from pp in PupilProfileBusiness.All
                                                                       where pp.IsActive
                                                                       && lstReceiverID.Contains(pp.PupilProfileID)
                                                                       select pp).ToList();
                                //neu so luong hoc sinh dang hoc va ListContent va ShortContent khong giong nhau thi thong bao loi
                                if (lstPupilStudying != null && ((lstPupilStudying.Count != contentList.Count) || (lstPupilStudying.Count != lstShortContent.Count)))
                                {
                                    resultBO.isError = true;
                                    resultBO.ErrorMsg = "Có lỗi xảy ra trong quá trình gửi thư. Thầy/cô vui lòng nhấn F5 và thực hiện gửi lại!";
                                    return resultBO;
                                }

                                // Kiem tra noi dung gui co nhu nhau hay ko?
                                bool isSameContent = true;
                                List<PupilContent> lstPupilContent = new List<PupilContent>();
                                PupilContent objPupilContent = null;
                                for (int i = 0, size = lstReceiverID.Count; i < size; i++)
                                {
                                    objPupilContent = new PupilContent();
                                    objPupilContent.PupilFileID = lstReceiverID[i];
                                    objPupilContent.Content = contentList[i].Trim();
                                    if (i < size - 1 && isSameContent)
                                    {
                                        if (string.Compare(objPupilContent.Content, contentList[i + 1].Trim()) != 0) isSameContent = false;
                                    }

                                    objPupilContent.SMS_CNT = objPupilContent.Content.countNumSMS(isSignMsg);
                                    objPupilContent.ShortContent = lstShortContent[i].Trim();
                                    lstPupilContent.Add(objPupilContent);
                                }

                                lstReceiverID = lstPupilStudying.Select(p => p.PupilProfileID).ToList();

                                //danh sach hoc sinh                 
                                var pupilQuery = (from sc in this.SMSParentContractBusiness.All
                                                  join scd in this.SMSParentContractDetailBusiness.All on sc.SMS_PARENT_CONTRACT_ID equals scd.SMS_PARENT_CONTRACT_ID
                                                  join pdtl in this.ServicePackageDetailBusiness.All on scd.SERVICE_PACKAGE_ID equals pdtl.SERVICE_PACKAGE_ID
                                                  join spdd in this.ServicePackageDeclareBusiness.All on scd.SERVICE_PACKAGE_ID equals spdd.SERVICE_PACKAGE_ID
                                                  join sentDtl in qSMSParentSendDetail on scd.SMS_PARENT_CONTRACT_DETAIL_ID equals sentDtl.SMS_PARENT_CONTRACT_DETAIL_ID into gj
                                                  from x in gj.DefaultIfEmpty(null)
                                                  where sc.SCHOOL_ID == schoolID && sc.PARTITION_ID == partitionSMSParentContract
                                                  && (scd.SUBSCRIPTION_STATUS == GlobalConstantsEdu.STATUS_REGISTED || scd.SUBSCRIPTION_STATUS == GlobalConstantsEdu.STATUS_UNPAID) // Bổ sung gửi tin cho các thuê bao Nợ cước
                                                  && scd.IS_ACTIVE == true && sc.STATUS == GlobalConstantsEdu.SMS_PARENT_CONTRACT_STATUS_IS_ACCEPT && (sc.IS_DELETED == false || sc.IS_DELETED == null)
                                                  && lstReceiverID.Contains(sc.PUPIL_ID)
                                                  &&
                                                  (
                                                   (semester == 1 && (scd.SUBSCRIPTION_TIME == GlobalConstantsEdu.CONTRACTDETAIL_SUBSCRIPTIONTIME_FIRST_SMESTER || scd.SUBSCRIPTION_TIME == GlobalConstantsEdu.CONTRACTDETAIL_SUBSCRIPTIONTIME_ALLYEAR))
                                                   ||
                                                   (semester == 2 && (scd.SUBSCRIPTION_TIME == GlobalConstantsEdu.CONTRACTDETAIL_SUBSCRIPTIONTIME_SECOND_SMESTER || scd.SUBSCRIPTION_TIME == GlobalConstantsEdu.CONTRACTDETAIL_SUBSCRIPTIONTIME_ALLYEAR))
                                                  )
                                                  && scd.YEAR_ID == academicYearBO.Year
                                                  && spdd.YEAR == academicYearBO.Year
                                                  && spdd.STATUS == true
                                                  select new PupilContractDetail
                                                  {
                                                      PupilFileID = sc.PUPIL_ID,
                                                      Mobile = scd.SUBSCRIBER,
                                                      SMSParentContactDetailID = scd.SMS_PARENT_CONTRACT_DETAIL_ID,
                                                      MaxSMS = (pdtl.MAX_SMS.HasValue) ? scd.SUBSCRIPTION_TIME == 3 ? pdtl.MAX_SMS.Value * 2 : pdtl.MAX_SMS.Value : 0,
                                                      SentSMS = ((x == null) ? 0 : (x.SENT_TOTAL)),
                                                      SMSParentSentDetailID = ((x == null) ? 0 : x.SMS_PARENT_SENT_DETAIL_ID),
                                                      ServicePackageID = pdtl.SERVICE_PACKAGE_ID,
                                                      IsLimitPackage = spdd.IS_LIMIT,
                                                      RegisterDate = scd.CREATED_TIME,
                                                      Status = scd.SUBSCRIPTION_STATUS
                                                  });
                                //Xu ly lai query de lay ra tong so tin nhan cua tung thue bao dang ky.
                                List<PupilContract> lstPupilContract = pupilQuery
                                    .GroupBy(x => new { x.PupilFileID, x.SMSParentContactDetailID, x.Mobile, x.SentSMS, x.SMSParentSentDetailID, x.ServicePackageID, x.RegisterDate, x.IsLimitPackage, x.Status })
                                    .Select(y => new PupilContract
                                    {
                                        Mobile = y.Key.Mobile,
                                        SMSParentContactDetailID = y.Key.SMSParentContactDetailID,
                                        PupilFileID = y.Key.PupilFileID,
                                        SentSMS = y.Key.SentSMS,
                                        IsSent = false,
                                        SMSParentSentDetailID = y.Key.SMSParentSentDetailID,
                                        ServicePackageID = y.Key.ServicePackageID,
                                        IsLimitPackage = y.Key.IsLimitPackage,
                                        RegisterDate = y.Key.RegisterDate,
                                        TotalSMS = y.Sum(a => a.MaxSMS),
                                        Status = y.Key.Status
                                    }).ToList();

                                if (lstPupilContract == null || lstPupilContract.Count == 0)
                                {
                                    resultBO.isError = true;
                                    resultBO.ErrorMsg = "Có lỗi xảy ra trong quá trình gửi thư. Thầy/cô vui lòng nhấn F5 và thực hiện gửi lại!";
                                    return resultBO;
                                }

                                #region  Kiem tra quy tin cua lop truoc khi gui - HoanTV5 20170506
                                //Kiem tra quy tin theo HK va CN
                                SMS_PARENT_CONTRACT_CLASS objClassSentDetail = new SMS_PARENT_CONTRACT_CLASS();
                                SMS_PARENT_CONTRACT_CLASS objClassSentDetailOfAll = new SMS_PARENT_CONTRACT_CLASS();
                                List<SMS_PARENT_CONTRACT_CLASS> lstClassSentDetails = this.SMSParentContractInClassDetailBusiness.All.Where(c => c.CLASS_ID == classID &&
                                       c.ACADEMIC_YEAR_ID == academicYearBO.AcademicYearID && c.SCHOOL_ID == schoolID && c.YEAR == academicYearBO.Year
                                       && (c.SEMESTER == semester || c.SEMESTER == GlobalConstants.SEMESTER_OF_YEAR_ALL)).OrderBy(c => c.SEMESTER).ToList();
                                int? countSentSMSDB = 0;
                                if (lstClassSentDetails.Count == 0)
                                {
                                    objClassSentDetail = SMSParentContractInClassDetailBusiness.GetMessageBudgetDetailInClass(schoolID, academicYearBO.AcademicYearID, classID, semester);
                                    objClassSentDetailOfAll = SMSParentContractInClassDetailBusiness.GetMessageBudgetDetailInClass(schoolID, academicYearBO.AcademicYearID, classID, GlobalConstants.SEMESTER_OF_YEAR_ALL);
                                    countSentSMSDB = objClassSentDetail.TOTAL_SMS + objClassSentDetailOfAll.TOTAL_SMS - objClassSentDetail.SENT_TOTAL - objClassSentDetailOfAll.SENT_TOTAL;
                                }
                                else
                                {
                                    countSentSMSDB = lstClassSentDetails.Sum(c => c.TOTAL_SMS) - lstClassSentDetails.Sum(c => c.SENT_TOTAL);
                                }

                                int totalSent = lstPupilContent.Sum(o => o.SMS_CNT * lstPupilContract.Count(p => p.PupilFileID == o.PupilFileID && p.IsLimitPackage != true && p.Status == GlobalConstantsEdu.STATUS_REGISTED));
                                bool isOutOfQuota = false;
                                // Neu het quota tin nhan
                                if (countSentSMSDB < totalSent)
                                {
                                    isOutOfQuota = true;
                                    // KO co goi thue bao No cuoc
                                    if (lstPupilContract.Count(o => o.Status == GlobalConstantsEdu.STATUS_UNPAID) == 0)
                                    {
                                        if (lstPupilContract.Count(o => (o.IsLimitPackage == true) && o.SentSMS < o.TotalSMS) > 0)
                                        {
                                            lstPupilContract = lstPupilContract.Where(o => o.IsLimitPackage == true).ToList();
                                        }
                                        else
                                        {
                                            resultBO.isError = true;
                                            resultBO.ErrorMsg = isSendSMSCommentOfPupil ? "Quỹ tin của Lớp không đủ để gửi tin. Vui lòng kiểm tra trong chức năng Liên lạc/PHHS" : "Quỹ tin nhắn còn lại không đủ để thực hiện gửi tin. Thầy cô vui lòng chọn học sinh để gửi hoặc nâng cấp gói cước cho thuê bao PHHS.";
                                            return resultBO;
                                        }
                                    }
                                }
                                #endregion

                                PupilProfile objPupilInfo = lstPupilStudying.FirstOrDefault();
                                PupilContract objContract = null;
                                List<PupilSubscriber> lstPupilSubscriber = null;
                                PupilSubscriber objPupilSubscriber = null;
                                SMS_SERVICE_PACKAGE_DECLARE detailLimitSP = null;
                                long smsParentContractDetailID = 0;
                                int countNumOfSend = 0;
                                int countSendSMS = 0;
                                int countTotalSentSMS = 0;
                                SMS_MT objMT = null;
                                //SMSCommunication objSMSCommunication = null;
                                SMS_HISTORY objHistorySMS = null;
                                //-> doi voi truong hop 1 hoc sinh ma co nhiu hon 1 so dien thoai nhan tin nhan: 
                                //Chi insert 1 record vao smscommunication de tranh truong hop xuat hien 2 noi dung giong nhau khi xem chi tiet hop thu 
                                for (int i = lstPupilStudying.Count - 1; i >= 0; i--)
                                {
                                    objPupilInfo = lstPupilStudying[i];
                                    objPupilContent = lstPupilContent.FirstOrDefault(p => p.PupilFileID == objPupilInfo.PupilProfileID);
                                    content = objPupilContent.Content;
                                    shortContent = objPupilContent.ShortContent;
                                    contentStripVNSign = ExtensionMethods.ConvertLineToSpace(content.StripVNSign().StripVNSignComposed());
                                    countSMS = objPupilContent.SMS_CNT;

                                    List<string> lstInputedSensitiveWord = new List<string>();
                                    if (content.CheckContentSMS(ref lstInputedSensitiveWord))
                                    {
                                        resultBO.isError = true;
                                        resultBO.ErrorMsg = string.Format("Nội dung thư chứa các từ không hợp lệ: {0}. Vui lòng kiểm tra lại nội dung thư!", string.Join(", ", lstInputedSensitiveWord));
                                        return resultBO;
                                    }

                                    lstPupilSubscriber = (from pc in lstPupilContract
                                                          where pc.PupilFileID == objPupilInfo.PupilProfileID
                                                          select new PupilSubscriber
                                                          {
                                                              Subscriber = pc.Mobile,
                                                              SMSParentContractDetailID = pc.SMSParentContactDetailID
                                                          }).ToList();
                                    bool sendSMSSuccessfullyToAtLeastOneSubscriber = false; // Bien dung de danh dau co gui tin thanh cong den phu huynh 1 hoc sinh hay khong.
                                    // Mot hoc sinh xem nhu da nhan duoc tin nhan neu gui thanh cong den it nhat 1 so thue bao ung voi hoc sinh do
                                    for (int k = lstPupilSubscriber.Count - 1; k >= 0; k--)
                                    {
                                        objPupilSubscriber = lstPupilSubscriber[k];
                                        // Chỉ cho phép gửi tin có dấu với các đầu số cho trước
                                        if (isSignMsg && !objPupilSubscriber.Subscriber.isAllowSendSignSMSPhone()) continue;
                                        inValidNumber = false;
                                        objContract = (from pc in lstPupilContract where pc.Mobile == objPupilSubscriber.Subscriber && pc.PupilFileID == objPupilInfo.PupilProfileID && pc.SMSParentContactDetailID == objPupilSubscriber.SMSParentContractDetailID select pc).FirstOrDefault();
                                        //Not found from Contract Detail
                                        if (objContract == null) continue;
                                        smsParentContractDetailID = objContract.SMSParentContactDetailID;
                                        objMT = null;
                                        objHistorySMS = null;
                                        //objSMSCommunication = null;

                                        if (!objPupilSubscriber.Subscriber.CheckMobileNumber()) continue;

                                        #region Kiem tra gui tin doi voi goi dich vu CO GIOI HAN
                                        // Neu la goi trial
                                        if (objContract.IsLimitPackage.HasValue && objContract.IsLimitPackage.Value)
                                        {

                                            //// Kiem tra thoi gian cho phep gui trong thoi han, ke tu ngay dau noi
                                            detailLimitSP = LstLimitPackage.FirstOrDefault(o => o.SERVICE_PACKAGE_ID == objContract.ServicePackageID
                                                && o.EFFECTIVE_DAYS > 0 &&
                                                ((o.FROM_DATE.HasValue && o.FROM_DATE.Value.Date <= objContract.RegisterDate.Date &&
                                                o.TO_DATE.HasValue && o.TO_DATE.Value.Date >= objContract.RegisterDate.Date)
                                                || (!o.TO_DATE.HasValue && !o.FROM_DATE.HasValue)));
                                            if (detailLimitSP != null)
                                            {
                                                if ((DateTime.Now - objContract.RegisterDate.Date).TotalDays > detailLimitSP.EFFECTIVE_DAYS) continue;
                                            }

                                            //Kiem tra so luong tin nhan chi (cho phep gui khi con tin nhan)
                                            if (objContract.TotalSMS - objContract.SentSMS <= 0) continue;
                                        }
                                        #endregion

                                        //Lay brandName theo thue bao
                                        brandName = GetBrandNamebyMobile(objPupilSubscriber.Subscriber, lstPreNumberTelco, lstBrandName);

                                        // Cac goi No cuoc kiem tra con so luong tin
                                        if (objContract.Status == GlobalConstantsEdu.STATUS_UNPAID && objContract.TotalSMS - objContract.SentSMS <= 0) continue;

                                        // Khong gui cac Hop dong khi het Quota
                                        if (isOutOfQuota && objContract.Status == GlobalConstantsEdu.STATUS_REGISTED) continue;
                                        //insert historySMS
                                        objHistorySMS = new SMS_HISTORY();
                                        objHistorySMS.MOBILE = objPupilSubscriber.Subscriber;
                                        objHistorySMS.IS_INTERNAL = objHistorySMS.MOBILE.CheckMobileNumberVT();
                                        objHistorySMS.RECEIVER_NAME = objPupilInfo.FullName;
                                        if (typeHistory.HasValue && typeHistory.Value != 0)
                                        {
                                            objHistorySMS.SMS_TYPE_ID = typeHistory.Value;
                                            objHistorySMS.RECEIVE_TYPE = GlobalConstantsEdu.COMMON_HISTORY_RECEIVER_PARENT_ID;
                                        }
                                        else
                                        {
                                            objHistorySMS.SMS_TYPE_ID = GlobalConstantsEdu.COMMON_HISTORYSMS_TO_COMMUNICATION_ID;
                                            objHistorySMS.RECEIVE_TYPE = GlobalConstantsEdu.COMMON_HISTORY_RECEIVER_PARENT_ID;
                                        }

                                        objHistorySMS.IS_CUSTOM_TYPE = isCustomType;
                                        objHistorySMS.CREATE_DATE = dateTimeNow;
                                        objHistorySMS.SCHOOL_ID = schoolID;
                                        objHistorySMS.YEAR = academicYearBO.Year;
                                        objHistorySMS.SMS_CONTENT = content;
                                        objHistorySMS.RECEIVER_ID = objPupilInfo.PupilProfileID;
                                        objHistorySMS.SHORT_CONTENT = shortContent;
                                        objHistorySMS.SENDER_NAME = fullNameSender;
                                        objHistorySMS.SMS_CNT = countSMS;
                                        objHistorySMS.SCHOOL_USERNAME = schoolUserName;
                                        objHistorySMS.SMS_PARENTCONTRACT_DETAIL_ID = smsParentContractDetailID;
                                        objHistorySMS.CONTENT_TYPE = isSignMsg ? GlobalConstantsEdu.SMS_CONTENT_TYPE_UNICODE : GlobalConstantsEdu.SMS_CONTENT_TYPE_NOT_SIGN;
                                        objHistorySMS.IS_SMS = true;
                                        objHistorySMS.SENDER_GROUP = senderGroup;
                                        objHistorySMS.PARTITION_ID = partitionSMSHistory;
                                        objHistorySMS.IS_ADMIN = isAdmin;
                                        objHistorySMS.SENDER_ID = senderID;
                                        Guid guid = Guid.NewGuid();
                                        objHistorySMS.HISTORY_RAW_ID = guid;
                                        if (isTimerUsed)
                                        {
                                            objHistorySMS.SMS_TIMER_CONFIG_ID = tc.SMS_TIMER_CONFIG_ID;
                                            objHistorySMS.IS_ON_SCHEDULE = true;
                                            objHistorySMS.SEND_TIME = tc.SEND_TIME;
                                        }
                                        this.SMSHistoryBusiness.Insert(objHistorySMS);

                                        //insert MT
                                        objMT = new SMS_MT();
                                        objMT.HISTORY_RAW_ID = guid;
                                        objMT.USER_ID = objPupilSubscriber.Subscriber;
                                        if (isSignMsg)
                                        {
                                            objMT.CONTENT_TYPE = "1"; // có dấu
                                            objMT.SMS_CONTENT = content;
                                        }
                                        else
                                        {
                                            objMT.CONTENT_TYPE = "0"; // ko dấu
                                            objMT.SMS_CONTENT = contentStripVNSign;
                                        }
                                        objMT.STATUS = GlobalConstants.MT_STATUS_NOT_SEND;
                                        objMT.TIME_SEND_REQUEST = dateTimeNow;
                                        objMT.UNIT_ID = schoolID;
                                        objMT.SERVICE_ID = brandName;

                                        if (isTimerUsed)
                                        {
                                            objMT.SMS_TIMER_CONFIG_ID = tc.SMS_TIMER_CONFIG_ID;
                                        }
                                        this.MTBusiness.Insert(objMT);
                                        countSendSMS++;

                                        //Chi tru khi gui qua MT
                                        objContract.IsSent = true;
                                        objContract.SentSMS += countSMS;
                                        // chi tinh tin nhan voi nhung goi KO GIOI HAN
                                        if (objContract.IsLimitPackage != true && objContract.Status == GlobalConstantsEdu.STATUS_REGISTED) countTotalSentSMS += countSMS;

                                        sendSMSSuccessfullyToAtLeastOneSubscriber = true; // Danh dau da gui thanh cong den cho hoc sinh dang xet

                                    }
                                    if (sendSMSSuccessfullyToAtLeastOneSubscriber)
                                    {
                                        countNumOfSend++; // Tang so luong hoc sinh da nhan tin nhan thanh cong
                                    }
                                }

                                if (countNumOfSend == 0)
                                {
                                    resultBO.isError = false;
                                    resultBO.NumSendSuccess = 0;
                                    return resultBO;
                                }

                                #region Send to head teacher - AnhVD9 20150922
                                SMS_SCHOOL_CONFIG schoolCfg = this.SchoolConfigBusiness.All.FirstOrDefault(o => o.SCHOOL_ID == schoolID);
                                if (schoolCfg == null || !schoolCfg.IS_ALLOW_SEND_HEADTEACHER)
                                {
                                    if (isSameContent)
                                    {
                                        if (headTeacherID > 0) teacherBO = EmployeeBusiness.Find(headTeacherID);
                                        if (teacherBO != null)
                                        {
                                            // Chỉ cho phép gửi tin có dấu với các đầu số cho trước
                                            if ((isSignMsg && teacherBO.Mobile.isAllowSendSignSMSPhone()) ||
                                                (!isSignMsg && teacherBO.Mobile.CheckMobileNumber())) // Tin thong thuong thi so dien thoai phai hop le
                                            {
                                                brandName = GetBrandNamebyMobile(teacherBO.Mobile, lstPreNumberTelco, lstBrandName);
                                                //insert historySMS 
                                                objHistorySMS = new SMS_HISTORY();
                                                objHistorySMS.MOBILE = teacherBO.Mobile;
                                                objHistorySMS.IS_INTERNAL = objHistorySMS.MOBILE.CheckMobileNumberVT();
                                                objHistorySMS.RECEIVER_NAME = teacherBO.FullName;
                                                if (typeHistory.HasValue && typeHistory.Value != 0) objHistorySMS.SMS_TYPE_ID = typeHistory.Value;
                                                else objHistorySMS.SMS_TYPE_ID = GlobalConstantsEdu.COMMON_HISTORYSMS_TO_COMMUNICATION_ID;

                                                objHistorySMS.IS_CUSTOM_TYPE = isCustomType;
                                                objHistorySMS.RECEIVE_TYPE = GlobalConstantsEdu.COMMON_HISTORY_RECEIVER_TEACHER_ID;
                                                objHistorySMS.CREATE_DATE = dateTimeNow;
                                                objHistorySMS.SCHOOL_ID = schoolID;
                                                objHistorySMS.YEAR = academicYearBO.Year;
                                                objHistorySMS.SMS_CONTENT = content;
                                                objHistorySMS.SENDER_NAME = fullNameSender;
                                                objHistorySMS.SHORT_CONTENT = shortContent;
                                                objHistorySMS.RECEIVER_ID = teacherBO.EmployeeID;
                                                objHistorySMS.SMS_CNT = countSMS;
                                                objHistorySMS.SCHOOL_USERNAME = schoolUserName;
                                                objHistorySMS.SENDER_GROUP = senderGroup;
                                                objHistorySMS.CONTENT_TYPE = isSignMsg ? GlobalConstantsEdu.SMS_CONTENT_TYPE_UNICODE : GlobalConstantsEdu.SMS_CONTENT_TYPE_NOT_SIGN;
                                                objHistorySMS.IS_SMS = true;
                                                objHistorySMS.PARTITION_ID = partitionSMSHistory;
                                                objHistorySMS.IS_ADMIN = isAdmin;
                                                objHistorySMS.SENDER_ID = senderID;
                                                Guid guid = Guid.NewGuid();
                                                objHistorySMS.HISTORY_RAW_ID = guid;
                                                if (isTimerUsed)
                                                {
                                                    objHistorySMS.SMS_TIMER_CONFIG_ID = tc.SMS_TIMER_CONFIG_ID;
                                                    objHistorySMS.IS_ON_SCHEDULE = true;
                                                    objHistorySMS.SEND_TIME = tc.SEND_TIME;
                                                }
                                                this.SMSHistoryBusiness.Insert(objHistorySMS);

                                                //insert MT
                                                objMT = new SMS_MT();
                                                objMT.HISTORY_RAW_ID = guid;
                                                if (isSignMsg)
                                                {
                                                    objMT.CONTENT_TYPE = "1"; // có dấu
                                                    objMT.SMS_CONTENT = content;
                                                }
                                                else
                                                {
                                                    objMT.CONTENT_TYPE = "0"; // ko dấu
                                                    objMT.SMS_CONTENT = contentStripVNSign;
                                                }
                                                objMT.STATUS = GlobalConstants.MT_STATUS_NOT_SEND;
                                                objMT.TIME_SEND_REQUEST = dateTimeNow;
                                                objMT.USER_ID = objHistorySMS.MOBILE;
                                                objMT.UNIT_ID = schoolID;
                                                objMT.SERVICE_ID = brandName;
                                                if (isTimerUsed)
                                                {
                                                    objMT.SMS_TIMER_CONFIG_ID = tc.SMS_TIMER_CONFIG_ID;
                                                }
                                                this.MTBusiness.Insert(objMT);
                                            }
                                        }
                                    }
                                }
                                #endregion

                                #region Cap nhat tin nhan theo tung thue bao
                                var lstSent = lstPupilContract.Where(a => a.IsSent.HasValue && a.IsSent.Value).ToList();
                                List<long> LstContractDetailID = lstSent.Select(o => o.SMSParentContactDetailID).Distinct().ToList();
                                List<SMS_PARENT_SENT_DETAIL> lstPupilSentDetail = qSMSParentSendDetail.Where(c => LstContractDetailID.Contains(c.SMS_PARENT_CONTRACT_DETAIL_ID)).ToList();
                                PupilContract itmSent = null;
                                SMS_PARENT_SENT_DETAIL sentItem = null;
                                int countSentSMS = 0;
                                List<SMS_PARENT_SENT_DETAIL> lstSmsParentSentDetailInsert = new List<SMS_PARENT_SENT_DETAIL>();
                                for (int i = 0, SentCnt = lstSent.Count; i < SentCnt; i++)
                                {
                                    itmSent = lstSent[i];
                                    if (itmSent.SMSParentSentDetailID > 0)
                                    {
                                        sentItem = lstPupilSentDetail.FirstOrDefault(a => a.SMS_PARENT_SENT_DETAIL_ID == itmSent.SMSParentSentDetailID);
                                        if (sentItem != null)
                                        {
                                            if (isTimerUsed)
                                            {
                                                //Luu lai quy tin cu
                                                SMS_TIMER_TRANSACTION tt = new SMS_TIMER_TRANSACTION();
                                                tt.SCHOOL_ID = schoolID;
                                                tt.TRANSACTION_TYPE = GlobalConstantsEdu.TIMER_TRANSACTION_SENT_DETAIL;
                                                tt.IS_ACTIVE = true;
                                                tt.SMS_PARENT_SENT_DETAIL_ID = sentItem.SMS_PARENT_SENT_DETAIL_ID;
                                                tt.SENT_TOTAL = itmSent.SentSMS - sentItem.SENT_TOTAL;
                                                tt.TIMER_CONFIG_ID = tc.SMS_TIMER_CONFIG_ID;

                                                this.TimerTransactionBusiness.Insert(tt);
                                            }

                                            sentItem.UPDATED_DATE = dateTimeNow;
                                            sentItem.SENT_TOTAL = itmSent.SentSMS;
                                            this.SMSParentSentDetailBusiness.Update(sentItem);
                                        }
                                    }
                                    else
                                    {
                                        sentItem = new SMS_PARENT_SENT_DETAIL();
                                        sentItem.SEMESTER = semester;
                                        sentItem.YEAR = academicYearBO.Year;
                                        sentItem.CREATED_DATE = dateTimeNow;
                                        sentItem.SMS_PARENT_CONTRACT_DETAIL_ID = itmSent.SMSParentContactDetailID;
                                        sentItem.SENT_TOTAL = itmSent.SentSMS;
                                        sentItem.SCHOOL_ID = schoolID;
                                        sentItem.PARTITION_ID = partitionSMSHistory;
                                        this.SMSParentSentDetailBusiness.Insert(sentItem);

                                        lstSmsParentSentDetailInsert.Add(sentItem);
                                    }
                                }
                                #endregion

                                #region Cap nhat lai quy tin trong lop - HoanTV5 20170506
                                //Tru lan luot quy tin: tru het quy HK sang tru quy CN
                                int? CountSMStmp = 0;
                                int? countSentSMSClass = 0;
                                bool breakout = false;
                                for (int i = 0; i < lstClassSentDetails.Count; i++)
                                {
                                    SMS_TIMER_TRANSACTION tt = new SMS_TIMER_TRANSACTION();
                                    objClassSentDetail = lstClassSentDetails[i];
                                    countSentSMSClass = countTotalSentSMS - CountSMStmp.GetValueOrDefault();

                                    CountSMStmp = objClassSentDetail.TOTAL_SMS - objClassSentDetail.SENT_TOTAL;
                                    if (CountSMStmp <= countSentSMS)
                                    {
                                        objClassSentDetail.SENT_TOTAL = objClassSentDetail.TOTAL_SMS;
                                        tt.SMS_NUMBER_ADDED = CountSMStmp.Value;
                                    }
                                    else
                                    {
                                        objClassSentDetail.SENT_TOTAL += countSentSMSClass.GetValueOrDefault();
                                        tt.SMS_NUMBER_ADDED = countSentSMSClass.Value;
                                        breakout = true;
                                    }
                                    objClassSentDetail.UPDATED_DATE = DateTime.Now;
                                    if (objClassSentDetail.SMS_PARENT_CONTRACT_CLASS_ID > 0)
                                    {
                                        this.SMSParentContractInClassDetailBusiness.Update(objClassSentDetail);
                                    }
                                    //Luu lai quy tin cu
                                    tt.SCHOOL_ID = schoolID;
                                    tt.TRANSACTION_TYPE = GlobalConstantsEdu.TIMER_TRANSACTION_CONTRACT_IN_CLASS;
                                    tt.IS_ACTIVE = true;
                                    tt.SMS_NUMBER_ADDED = countTotalSentSMS;
                                    tt.SMS_PARENT_CONTRACT_CLASS_ID = objClassSentDetail.SMS_PARENT_CONTRACT_CLASS_ID;
                                    tt.TIMER_CONFIG_ID = tc.SMS_TIMER_CONFIG_ID;

                                    this.TimerTransactionBusiness.Insert(tt);
                                    if (breakout)
                                    {
                                        break;
                                    }
                                }
                                #endregion

                                this.Save();

                                if (isTimerUsed && lstSmsParentSentDetailInsert.Count > 0)
                                {
                                    for (int i = 0; i < lstSmsParentSentDetailInsert.Count; i++)
                                    {
                                        SMS_PARENT_SENT_DETAIL ssd = lstSmsParentSentDetailInsert[i];
                                        //Luu lai quy tin cu
                                        SMS_TIMER_TRANSACTION tt = new SMS_TIMER_TRANSACTION();
                                        tt.SCHOOL_ID = schoolID;
                                        tt.TRANSACTION_TYPE = GlobalConstantsEdu.TIMER_TRANSACTION_SENT_DETAIL;
                                        tt.IS_ACTIVE = true;
                                        tt.SMS_PARENT_SENT_DETAIL_ID = ssd.SMS_PARENT_SENT_DETAIL_ID;
                                        tt.SENT_TOTAL = ssd.SENT_TOTAL;
                                        tt.TIMER_CONFIG_ID = tc.SMS_TIMER_CONFIG_ID;

                                        this.TimerTransactionBusiness.Insert(tt);

                                    }
                                    this.TimerTransactionBusiness.Save();
                                }

                                resultBO.NumSendSuccess = countNumOfSend;
                                return resultBO;
                                #endregion
                            }
                            else
                            {
                                SMS_TIMER_CONFIG tc = new SMS_TIMER_CONFIG();
                                if (isTimerUsed)
                                {
                                    tc = new SMS_TIMER_CONFIG();
                                    tc.SMS_TIMER_CONFIG_ID = Guid.NewGuid();
                                    tc.CONTENT = content;
                                    tc.CREATE_TIME = DateTime.Now;
                                    tc.SCHOOL_ID = schoolID;
                                    tc.APPLIED_LEVEL = iLevelID;
                                    tc.PARTITION_ID = schoolID % 100;
                                    tc.SEND_TIME = sendTime.Value;
                                    tc.STATUS = GlobalConstantsEdu.TIMER_CONFIG_STATUS_ACTIVE;//chua gui
                                    tc.TARGET_ID = classID;
                                    tc.CONFIG_NAME = timerConfigName;
                                    tc.CONFIG_TYPE = GlobalConstantsEdu.TIMER_CONFIG_TYPE_CLASS;
                                    tc.SEND_ALL_CLASS = isAllClass;
                                    tc.SEND_UNICODE = isSignMsg;
                                    tc.IS_IMPORT = isSendImport;

                                    this.TimerConfigBusiness.Insert(tc);
                                }

                                #region gui cho phhs theo tung lop/hoc sinh voi cung noi dung
                                int headTeacherID = EmployeeBusiness.GetHeadTeacherID(classID, academicYearBO.AcademicYearID, schoolID);
                                Employee teacherBO = null;
                                string fullNameSender = string.Empty;
                                if (isAdmin) fullNameSender = SchoolName;
                                else // giao vien
                                {
                                    teacherBO = EmployeeBusiness.Find(senderID);
                                    if (teacherBO != null) fullNameSender = teacherBO.FullName;
                                }

                                if (string.IsNullOrEmpty(fullNameSender))
                                {
                                    resultBO.isError = true;
                                    resultBO.ErrorMsg = "Có lỗi xảy ra trong quá trình gửi thư. Thầy/cô vui lòng nhấn F5 và thực hiện gửi lại!";
                                    return resultBO;
                                }

                                List<PupilProfile> lstPupilStudying = (from pp in PupilProfileBusiness.All
                                                                       where pp.IsActive
                                                                       && lstReceiverID.Contains(pp.PupilProfileID)
                                                                       select pp).ToList();

                                lstReceiverID = lstPupilStudying.Select(p => p.PupilProfileID).ToList();
                                //danh sach hoc sinh                 
                                var pupilQuery = (from sc in this.SMSParentContractBusiness.All
                                                  join scd in this.SMSParentContractDetailBusiness.All on sc.SMS_PARENT_CONTRACT_ID equals scd.SMS_PARENT_CONTRACT_ID
                                                  join pdtl in this.ServicePackageDetailBusiness.All on scd.SERVICE_PACKAGE_ID equals pdtl.SERVICE_PACKAGE_ID
                                                  join spdd in this.ServicePackageDeclareBusiness.All on scd.SERVICE_PACKAGE_ID equals spdd.SERVICE_PACKAGE_ID
                                                  join sentDtl in qSMSParentSendDetail on scd.SMS_PARENT_CONTRACT_DETAIL_ID equals sentDtl.SMS_PARENT_CONTRACT_DETAIL_ID into gj
                                                  from x in gj.DefaultIfEmpty(null)
                                                  where sc.SCHOOL_ID == schoolID && sc.PARTITION_ID == partitionSMSParentContract
                                                  && (scd.SUBSCRIPTION_STATUS == GlobalConstantsEdu.STATUS_REGISTED || scd.SUBSCRIPTION_STATUS == GlobalConstantsEdu.STATUS_UNPAID) // Bổ sung gửi tin cho các thuê bao Nợ cước
                                                  && scd.IS_ACTIVE == true && sc.STATUS == GlobalConstantsEdu.SMS_PARENT_CONTRACT_STATUS_IS_ACCEPT && sc.IS_DELETED != true
                                                  && lstReceiverID.Contains(sc.PUPIL_ID)
                                                  &&
                                                  (
                                                   (semester == 1 && (scd.SUBSCRIPTION_TIME == GlobalConstantsEdu.CONTRACTDETAIL_SUBSCRIPTIONTIME_FIRST_SMESTER || scd.SUBSCRIPTION_TIME == GlobalConstantsEdu.CONTRACTDETAIL_SUBSCRIPTIONTIME_ALLYEAR))
                                                   ||
                                                   (semester == 2 && (scd.SUBSCRIPTION_TIME == GlobalConstantsEdu.CONTRACTDETAIL_SUBSCRIPTIONTIME_SECOND_SMESTER || scd.SUBSCRIPTION_TIME == GlobalConstantsEdu.CONTRACTDETAIL_SUBSCRIPTIONTIME_ALLYEAR))
                                                  )
                                                  && scd.YEAR_ID == academicYearBO.Year
                                                  && spdd.YEAR == academicYearBO.Year
                                                  && spdd.STATUS == true
                                                  select new PupilContractDetail
                                                  {
                                                      PupilFileID = sc.PUPIL_ID,
                                                      Mobile = scd.SUBSCRIBER,
                                                      SMSParentContactDetailID = scd.SMS_PARENT_CONTRACT_DETAIL_ID,
                                                      MaxSMS = (pdtl.MAX_SMS.HasValue) ? scd.SUBSCRIPTION_TIME == 3 ? pdtl.MAX_SMS.Value * 2 : pdtl.MAX_SMS.Value : 0,
                                                      SentSMS = ((x == null) ? 0 : (x.SENT_TOTAL)),
                                                      SMSParentSentDetailID = ((x == null) ? 0 : x.SMS_PARENT_SENT_DETAIL_ID),
                                                      ServicePackageID = pdtl.SERVICE_PACKAGE_ID, // Loai goi dich vu ma thue bao nay dang ky
                                                      IsLimitPackage = spdd.IS_LIMIT,
                                                      RegisterDate = scd.CREATED_TIME, // Ngay dang ky dich vu
                                                      Status = scd.SUBSCRIPTION_STATUS
                                                  });
                                //Xu ly lai query de lay ra tong so tin nhan cua tung thue bao dang ky.
                                List<PupilContract> lstPupilContract = pupilQuery
                                    .GroupBy(x => new { x.PupilFileID, x.SMSParentContactDetailID, x.Mobile, x.SentSMS, x.SMSParentSentDetailID, x.ServicePackageID, x.RegisterDate, x.IsLimitPackage, x.Status })
                                    .Select(y => new PupilContract
                                    {
                                        Mobile = y.Key.Mobile,
                                        SMSParentContactDetailID = y.Key.SMSParentContactDetailID,
                                        PupilFileID = y.Key.PupilFileID,
                                        SentSMS = y.Key.SentSMS,
                                        IsSent = false,
                                        SMSParentSentDetailID = y.Key.SMSParentSentDetailID,
                                        ServicePackageID = y.Key.ServicePackageID,
                                        IsLimitPackage = y.Key.IsLimitPackage,
                                        RegisterDate = y.Key.RegisterDate,
                                        TotalSMS = y.Sum(a => a.MaxSMS),
                                        Status = y.Key.Status
                                    }).ToList();
                                if (lstPupilContract == null || lstPupilContract.Count == 0)
                                {
                                    resultBO.isError = true;
                                    resultBO.ErrorMsg = "Tồn tại học sinh có thuê bao không hợp lệ. Thầy/cô vui lòng kiểm tra lại!";
                                    return resultBO;
                                }

                                #region  Kiem tra quy tin cua lop truoc khi gui - HoanTV5 20170506
                                //Check quy tin theo HK va quy tin cua ca nam
                                SMS_PARENT_CONTRACT_CLASS objClassSentDetail = null;
                                SMS_PARENT_CONTRACT_CLASS objClassSentDetailOfAll = null;
                                List<SMS_PARENT_CONTRACT_CLASS> lstClassSentDetails = this.SMSParentContractInClassDetailBusiness.All.Where(c => c.CLASS_ID == classID &&
                                       c.ACADEMIC_YEAR_ID == academicYearBO.AcademicYearID && c.SCHOOL_ID == schoolID && c.YEAR == academicYearBO.Year
                                       && (c.SEMESTER == semester || c.SEMESTER == GlobalConstants.SEMESTER_OF_YEAR_ALL)).OrderBy(p => p.SEMESTER).ToList();
                                int? countTotalSMS_DB = 0;
                                if (lstClassSentDetails.Count == 0)
                                {
                                    objClassSentDetail = SMSParentContractInClassDetailBusiness.GetMessageBudgetDetailInClass(schoolID, academicYearBO.AcademicYearID, classID, semester);
                                    objClassSentDetailOfAll = SMSParentContractInClassDetailBusiness.GetMessageBudgetDetailInClass(schoolID, academicYearBO.AcademicYearID, classID, GlobalConstants.SEMESTER_OF_YEAR_ALL);
                                    countTotalSMS_DB = objClassSentDetail.TOTAL_SMS + objClassSentDetailOfAll.TOTAL_SMS - objClassSentDetail.SENT_TOTAL - objClassSentDetailOfAll.SENT_TOTAL;
                                }
                                else
                                {
                                    countTotalSMS_DB = lstClassSentDetails.Sum(p => p.TOTAL_SMS) - lstClassSentDetails.Sum(p => p.SENT_TOTAL);
                                }

                                int totalSent = countSMS * lstPupilContract.Count(o => o.IsLimitPackage != true && o.Status == GlobalConstantsEdu.STATUS_REGISTED);

                                bool isOutOfQuota = false;
                                // Neu het quota tin nhan
                                if (countTotalSMS_DB < totalSent)
                                {
                                    isOutOfQuota = true;
                                    // KO co thue bao No cuoc can gui nao
                                    if (lstPupilContract.Count(o => o.Status == GlobalConstantsEdu.STATUS_UNPAID) == 0)
                                    {
                                        if (lstPupilContract.Count(o => (o.IsLimitPackage == true) && o.SentSMS < o.TotalSMS) > 0)
                                        {
                                            lstPupilContract = lstPupilContract.Where(o => o.IsLimitPackage == true).ToList();
                                        }
                                        else
                                        {
                                            resultBO.isError = true;
                                            resultBO.ErrorMsg = isSendSMSCommentOfPupil ? "Quỹ tin của Lớp không đủ để gửi tin. Vui lòng kiểm tra trong chức năng Liên lạc/PHHS" : "Quỹ tin nhắn còn lại không đủ để thực hiện gửi tin. Thầy cô vui lòng chọn học sinh để gửi hoặc nâng cấp gói cước cho thuê bao PHHS.";
                                            return resultBO;
                                        }

                                    }
                                }
                                #endregion

                                PupilProfile objPupilInfo = null;
                                PupilContract objContract = null;
                                List<PupilSubscriber> lstPupilSubscriber = null;
                                PupilSubscriber objPupilSubscriber = null;
                                long smsParentContractDetailID = 0;
                                int countNumOfSend = 0;
                                int countTotalSentSMS = 0;
                                SMS_MT objMT = null;
                                //SMSCommunication objSMSCommunication = null;
                                SMS_HISTORY objHistorySMS = null;
                                SMS_SERVICE_PACKAGE_DECLARE detailLimitSP = null;
                                //-> doi voi truong hop 1 hoc sinh ma co nhiu hon 1 so dien thoai nhan tin nhan: 
                                //Chi insert 1 record vao smscommunication de tranh truong hop xuat hien 2 noi dung giong nhau khi xem chi tiet hop thu 
                                //bool isHasManyMobileReceiver = false;
                                //bool isFirstInsert = true;
                                bool isPhoneNotSupport = false;
                                for (int i = lstPupilStudying.Count - 1; i >= 0; i--)
                                {
                                    //isFirstInsert = true;
                                    //isHasManyMobileReceiver = false;
                                    objPupilInfo = lstPupilStudying[i];
                                    lstPupilSubscriber = (from pc in lstPupilContract
                                                          where pc.PupilFileID == objPupilInfo.PupilProfileID
                                                          select new
                                                          {
                                                              Subscriber = pc.Mobile,
                                                              SMSParentContractDetailID = pc.SMSParentContactDetailID
                                                          }).Distinct().Select(o => new PupilSubscriber { Subscriber = o.Subscriber, SMSParentContractDetailID = o.SMSParentContractDetailID }).ToList();
                                    bool sendSMSSuccessfullyToAtLeastOneSubscriber = false; // Bien dung de danh dau co gui tin thanh cong den phu huynh 1 hoc sinh hay khong.

                                    // Mot hoc sinh xem nhu da nhan duoc tin nhan neu gui thanh cong den it nhat 1 so thue bao ung voi hoc sinh do
                                    for (int k = lstPupilSubscriber.Count - 1; k >= 0; k--)
                                    {
                                        objPupilSubscriber = lstPupilSubscriber[k];
                                        // Chỉ cho phép gửi tin có dấu với các đầu số cho trước
                                        if (isSignMsg && !objPupilSubscriber.Subscriber.isAllowSendSignSMSPhone())
                                        {
                                            isPhoneNotSupport = true;
                                            continue;
                                        }
                                        objContract = (from pc in lstPupilContract where pc.Mobile == objPupilSubscriber.Subscriber && pc.PupilFileID == objPupilInfo.PupilProfileID && pc.SMSParentContactDetailID == objPupilSubscriber.SMSParentContractDetailID select pc).FirstOrDefault();
                                        //Not found from Contract Detail
                                        if (objContract == null) continue;
                                        smsParentContractDetailID = objContract.SMSParentContactDetailID;
                                        objMT = null;
                                        objHistorySMS = null;
                                        //objSMSCommunication = null;

                                        if (!objPupilSubscriber.Subscriber.CheckMobileNumber()) continue;

                                        #region Doi voi cac goi CO GIOI HAN
                                        // Neu la goi trial
                                        if (objContract.IsLimitPackage.HasValue && objContract.IsLimitPackage.Value)
                                        {
                                            //// Kiem tra thoi gian cho phep gui trong thoi han, ke tu ngay dau noi
                                            detailLimitSP = LstLimitPackage.FirstOrDefault(o => o.SERVICE_PACKAGE_ID == objContract.ServicePackageID
                                                && o.EFFECTIVE_DAYS > 0 &&
                                                ((o.FROM_DATE.HasValue && o.FROM_DATE.Value.Date <= objContract.RegisterDate.Date &&
                                                o.TO_DATE.HasValue && o.TO_DATE.Value.Date >= objContract.RegisterDate.Date)
                                                || (!o.TO_DATE.HasValue && !o.FROM_DATE.HasValue)));
                                            if (detailLimitSP != null)
                                            {
                                                if ((DateTime.Now - objContract.RegisterDate.Date).TotalDays > detailLimitSP.EFFECTIVE_DAYS)
                                                {
                                                    // Neu gui nhieu hoc sinh - hoac 1 hoc sinh co nhieu hop dong thi van tiep tuc
                                                    if ((lstReceiverID != null && lstReceiverID.Count > 1) || lstPupilSubscriber.Count > 1) continue;
                                                    resultBO.isError = true;
                                                    resultBO.ErrorMsg = string.Format("Gói dùng thử của phụ huynh học sinh đã hết hạn {0} ngày sử dụng.", detailLimitSP.EFFECTIVE_DAYS);
                                                    return resultBO;
                                                }
                                            }

                                            //Kiem tra so luong tin nhan chi (cho phep gui khi con tin nhan)
                                            if (objContract.TotalSMS - objContract.SentSMS <= 0)
                                            {
                                                // Neu gui nhieu hoc sinh - hoac 1 hoc sinh co nhieu hop dong thi van tiep tuc
                                                if ((lstReceiverID != null && lstReceiverID.Count > 1) || lstPupilSubscriber.Count > 1) continue;

                                                resultBO.isError = true;
                                                resultBO.ErrorMsg = "Các thuê bao trong hợp đồng SMS Parent của học sinh đã hết!";
                                                return resultBO;
                                            }
                                        }
                                        #endregion

                                        //Lay brandName theo thue bao
                                        brandName = GetBrandNamebyMobile(objPupilSubscriber.Subscriber, lstPreNumberTelco, lstBrandName);

                                        // Cac goi No cuoc kiem tra con so luong tin
                                        if (objContract.Status == GlobalConstantsEdu.STATUS_UNPAID && objContract.TotalSMS - objContract.SentSMS <= 0) continue;

                                        //Khong gui khi het Quota
                                        if (isOutOfQuota && (objContract.Status == GlobalConstantsEdu.STATUS_REGISTED)) continue;
                                        //insert historySMS
                                        objHistorySMS = new SMS_HISTORY();
                                        objHistorySMS.MOBILE = objPupilSubscriber.Subscriber;
                                        objHistorySMS.IS_INTERNAL = objHistorySMS.MOBILE.CheckMobileNumberVT();
                                        objHistorySMS.RECEIVER_NAME = objPupilInfo.FullName;
                                        if (typeHistory.HasValue && typeHistory.Value != 0)
                                        {
                                            objHistorySMS.SMS_TYPE_ID = typeHistory.Value;
                                            objHistorySMS.RECEIVE_TYPE = GlobalConstantsEdu.COMMON_HISTORY_RECEIVER_PARENT_ID;
                                        }
                                        else
                                        {
                                            objHistorySMS.SMS_TYPE_ID = GlobalConstantsEdu.COMMON_HISTORYSMS_TO_COMMUNICATION_ID;
                                            objHistorySMS.RECEIVE_TYPE = GlobalConstantsEdu.COMMON_HISTORY_RECEIVER_PARENT_ID;
                                        }

                                        objHistorySMS.IS_CUSTOM_TYPE = isCustomType;
                                        objHistorySMS.CREATE_DATE = dateTimeNow;
                                        objHistorySMS.SCHOOL_ID = schoolID;
                                        objHistorySMS.YEAR = academicYearBO.Year;
                                        objHistorySMS.SMS_CONTENT = content;
                                        objHistorySMS.RECEIVER_ID = objPupilInfo.PupilProfileID;
                                        objHistorySMS.SHORT_CONTENT = shortContent;
                                        objHistorySMS.SENDER_NAME = fullNameSender;
                                        objHistorySMS.SMS_CNT = countSMS;
                                        objHistorySMS.SCHOOL_USERNAME = schoolUserName;
                                        objHistorySMS.SMS_PARENTCONTRACT_DETAIL_ID = smsParentContractDetailID;
                                        if (inValidNumber) objHistorySMS.FLAG_ID = GlobalConstantsEdu.COMMON_TABLE_SMSHISTORY_FLAG_INVALID_MOBILE;
                                        objHistorySMS.CONTENT_TYPE = isSignMsg ? GlobalConstantsEdu.SMS_CONTENT_TYPE_UNICODE : GlobalConstantsEdu.SMS_CONTENT_TYPE_NOT_SIGN;
                                        objHistorySMS.IS_SMS = inValidNumber == false;
                                        objHistorySMS.SENDER_GROUP = senderGroup;
                                        objHistorySMS.PARTITION_ID = partitionSMSHistory;
                                        objHistorySMS.IS_ADMIN = isAdmin;
                                        objHistorySMS.SENDER_ID = senderID;
                                        if (isTimerUsed)
                                        {
                                            objHistorySMS.SMS_TIMER_CONFIG_ID = tc.SMS_TIMER_CONFIG_ID;
                                            objHistorySMS.IS_ON_SCHEDULE = true;
                                            objHistorySMS.SEND_TIME = tc.SEND_TIME;
                                        }
                                        Guid guid = Guid.NewGuid();
                                        objHistorySMS.HISTORY_RAW_ID = guid;
                                        this.SMSHistoryBusiness.Insert(objHistorySMS);

                                        //insert MT
                                        objMT = new SMS_MT();
                                        objMT.HISTORY_RAW_ID = guid;
                                        objMT.USER_ID = objPupilSubscriber.Subscriber;
                                        if (isSignMsg)
                                        {
                                            objMT.CONTENT_TYPE = "1"; // có dấu
                                            objMT.SMS_CONTENT = content;
                                        }
                                        else
                                        {
                                            objMT.CONTENT_TYPE = "0"; // ko dấu
                                            objMT.SMS_CONTENT = contentStripVNSign;
                                        }
                                        objMT.STATUS = GlobalConstants.MT_STATUS_NOT_SEND;
                                        objMT.TIME_SEND_REQUEST = dateTimeNow;
                                        objMT.UNIT_ID = schoolID;
                                        objMT.SERVICE_ID = brandName;
                                        if (isTimerUsed)
                                        {
                                            objMT.SMS_TIMER_CONFIG_ID = tc.SMS_TIMER_CONFIG_ID;
                                        }
                                        this.MTBusiness.Insert(objMT);
                                        //Chi tru khi da gui qua MT
                                        objContract.IsSent = true;
                                        objContract.SentSMS += countSMS;
                                        // chi tinh tin nhan voi cac hop dong KO GIOI HAN
                                        if (objContract.IsLimitPackage != true && objContract.Status == GlobalConstantsEdu.STATUS_REGISTED) countTotalSentSMS += countSMS;
                                        sendSMSSuccessfullyToAtLeastOneSubscriber = true; // Danh dau da gui thanh cong den cho hoc sinh dang xet

                                    }
                                    if (sendSMSSuccessfullyToAtLeastOneSubscriber)
                                    {
                                        countNumOfSend++; // Tang so luong hoc sinh da nhan tin nhan thanh cong
                                    }
                                }

                                if (countNumOfSend == 0)
                                {
                                    if (isPhoneNotSupport)
                                    {
                                        resultBO.isError = true;
                                        resultBO.ErrorMsg = "Số điện thoại không hỗ trợ nhắn tin có dấu.";
                                        return resultBO;
                                    }
                                    else
                                    {
                                        resultBO.isError = true;
                                        resultBO.ErrorMsg = "Quỹ tin nhắn còn lại không đủ để thực hiện gửi tin. Thầy cô vui lòng chọn học sinh để gửi hoặc nâng cấp gói cước cho thuê bao PHHS.";
                                        return resultBO;
                                    }
                                }

                                #region Send to head teacher - AnhVD9 20150922
                                SMS_SCHOOL_CONFIG schoolCfg = this.SchoolConfigBusiness.All.FirstOrDefault(o => o.SCHOOL_ID == schoolID);
                                if (schoolCfg == null || !schoolCfg.IS_ALLOW_SEND_HEADTEACHER)
                                {
                                    if (headTeacherID > 0) teacherBO = EmployeeBusiness.Find(headTeacherID);
                                    if (teacherBO != null)
                                    {
                                        // Chỉ cho phép gửi tin có dấu với các đầu số cho trước
                                        if ((isSignMsg && teacherBO.Mobile.isAllowSendSignSMSPhone()) ||
                                            (!isSignMsg && teacherBO.Mobile.CheckMobileNumber()))
                                        {
                                            //Lay brandName theo thue bao
                                            brandName = GetBrandNamebyMobile(teacherBO.Mobile, lstPreNumberTelco, lstBrandName);

                                            //insert historySMS 
                                            objHistorySMS = new SMS_HISTORY();
                                            objHistorySMS.MOBILE = teacherBO.Mobile;
                                            objHistorySMS.IS_INTERNAL = objHistorySMS.MOBILE.CheckMobileNumberVT();
                                            objHistorySMS.RECEIVER_NAME = teacherBO.FullName;
                                            if (typeHistory.HasValue && typeHistory.Value != 0) objHistorySMS.SMS_TYPE_ID = typeHistory.Value;
                                            else objHistorySMS.SMS_TYPE_ID = GlobalConstantsEdu.COMMON_HISTORYSMS_TO_COMMUNICATION_ID;

                                            objHistorySMS.IS_CUSTOM_TYPE = isCustomType;
                                            objHistorySMS.RECEIVE_TYPE = GlobalConstantsEdu.COMMON_HISTORY_RECEIVER_TEACHER_ID;
                                            objHistorySMS.CREATE_DATE = dateTimeNow;
                                            objHistorySMS.SCHOOL_ID = schoolID;
                                            objHistorySMS.YEAR = academicYearBO.Year;
                                            objHistorySMS.SMS_CONTENT = content;
                                            objHistorySMS.SENDER_NAME = fullNameSender;
                                            objHistorySMS.SHORT_CONTENT = shortContent;
                                            objHistorySMS.RECEIVER_ID = teacherBO.EmployeeID;
                                            objHistorySMS.SMS_CNT = countSMS;
                                            objHistorySMS.SCHOOL_USERNAME = schoolUserName;
                                            objHistorySMS.SENDER_GROUP = senderGroup;
                                            objHistorySMS.CONTENT_TYPE = isSignMsg ? GlobalConstantsEdu.SMS_CONTENT_TYPE_UNICODE : GlobalConstantsEdu.SMS_CONTENT_TYPE_NOT_SIGN;
                                            objHistorySMS.IS_SMS = true;
                                            objHistorySMS.PARTITION_ID = partitionSMSHistory;
                                            objHistorySMS.IS_ADMIN = isAdmin;
                                            objHistorySMS.SENDER_ID = senderID;
                                            Guid guid = Guid.NewGuid();
                                            objHistorySMS.HISTORY_RAW_ID = guid;
                                            if (isTimerUsed)
                                            {
                                                objHistorySMS.SMS_TIMER_CONFIG_ID = tc.SMS_TIMER_CONFIG_ID;
                                                objHistorySMS.IS_ON_SCHEDULE = true;
                                                objHistorySMS.SEND_TIME = tc.SEND_TIME;
                                            }
                                            this.SMSHistoryBusiness.Insert(objHistorySMS);

                                            //insert MT
                                            objMT = new SMS_MT();
                                            objMT.HISTORY_RAW_ID = guid;
                                            if (isSignMsg)
                                            {
                                                objMT.CONTENT_TYPE = "1"; // có dấu
                                                objMT.SMS_CONTENT = content;
                                            }
                                            else
                                            {
                                                objMT.CONTENT_TYPE = "0"; // ko dấu
                                                objMT.SMS_CONTENT = contentStripVNSign;
                                            }
                                            objMT.STATUS = GlobalConstants.MT_STATUS_NOT_SEND;
                                            objMT.TIME_SEND_REQUEST = dateTimeNow;
                                            objMT.USER_ID = objHistorySMS.MOBILE;
                                            objMT.UNIT_ID = schoolID;
                                            objMT.SERVICE_ID = brandName;
                                            if (isTimerUsed)
                                            {
                                                objMT.SMS_TIMER_CONFIG_ID = tc.SMS_TIMER_CONFIG_ID;
                                            }
                                            this.MTBusiness.Insert(objMT);
                                        }
                                    }
                                }
                                #endregion

                                #region Cap nhat lai so tin nhan da gui theo tung thue bao
                                var lstSent = lstPupilContract.Where(a => a.IsSent.HasValue && a.IsSent.Value == true).ToList();
                                List<long> LstContractDetailID = lstSent.Select(o => o.SMSParentContactDetailID).Distinct().ToList();
                                List<SMS_PARENT_SENT_DETAIL> lstPupilSentDetail = qSMSParentSendDetail.Where(c => LstContractDetailID.Contains(c.SMS_PARENT_CONTRACT_DETAIL_ID)).ToList();

                                PupilContract itmSent = null;
                                SMS_PARENT_SENT_DETAIL sentItem = null;
                                int countSentSMS = 0;
                                List<SMS_PARENT_SENT_DETAIL> lstSmsParentSentDetailInsert = new List<SMS_PARENT_SENT_DETAIL>();
                                for (int i = 0, SentCnt = lstSent.Count; i < SentCnt; i++)
                                {
                                    itmSent = lstSent[i];
                                    if (itmSent.SMSParentSentDetailID > 0)
                                    {
                                        sentItem = lstPupilSentDetail.FirstOrDefault(a => a.SMS_PARENT_SENT_DETAIL_ID == itmSent.SMSParentSentDetailID);
                                        if (sentItem != null)
                                        {
                                            if (isTimerUsed)
                                            {
                                                //Luu lai quy tin cu
                                                SMS_TIMER_TRANSACTION tt = new SMS_TIMER_TRANSACTION();
                                                tt.SCHOOL_ID = schoolID;
                                                tt.TRANSACTION_TYPE = GlobalConstantsEdu.TIMER_TRANSACTION_SENT_DETAIL;
                                                tt.IS_ACTIVE = true;
                                                tt.SMS_PARENT_SENT_DETAIL_ID = sentItem.SMS_PARENT_SENT_DETAIL_ID;
                                                tt.SENT_TOTAL = itmSent.SentSMS - sentItem.SENT_TOTAL;
                                                tt.TIMER_CONFIG_ID = tc.SMS_TIMER_CONFIG_ID;

                                                this.TimerTransactionBusiness.Insert(tt);
                                            }

                                            sentItem.UPDATED_DATE = DateTime.Now;
                                            sentItem.SENT_TOTAL = itmSent.SentSMS;
                                            this.SMSParentSentDetailBusiness.Update(sentItem);
                                        }
                                    }
                                    else
                                    {

                                        sentItem = new SMS_PARENT_SENT_DETAIL();
                                        sentItem.SEMESTER = semester;
                                        sentItem.YEAR = academicYearBO.Year;
                                        sentItem.CREATED_DATE = DateTime.Now;
                                        sentItem.SMS_PARENT_CONTRACT_DETAIL_ID = itmSent.SMSParentContactDetailID;
                                        sentItem.SENT_TOTAL = itmSent.SentSMS;
                                        sentItem.SCHOOL_ID = schoolID;
                                        sentItem.PARTITION_ID = partitionSMSHistory;

                                        this.SMSParentSentDetailBusiness.Insert(sentItem);
                                        lstSmsParentSentDetailInsert.Add(sentItem);
                                    }
                                }
                                #endregion

                                #region Cap nhat lai quy tin trong lop - HoanTV5 20170506 tru lan luot quy tin theo HK
                                int? CountSMStmp = 0;
                                int countSentSMSClass = 0;
                                bool breakout = false;
                                for (int i = 0; i < lstClassSentDetails.Count; i++)
                                {
                                    SMS_TIMER_TRANSACTION tt = new SMS_TIMER_TRANSACTION();
                                    objClassSentDetail = lstClassSentDetails[i];
                                    countSentSMSClass = countTotalSentSMS - CountSMStmp.GetValueOrDefault();

                                    CountSMStmp = objClassSentDetail.TOTAL_SMS - objClassSentDetail.SENT_TOTAL;
                                    if (CountSMStmp <= countSentSMS)
                                    {
                                        objClassSentDetail.SENT_TOTAL = objClassSentDetail.TOTAL_SMS;
                                        tt.SMS_NUMBER_ADDED = CountSMStmp.Value;
                                    }
                                    else
                                    {
                                        objClassSentDetail.SENT_TOTAL += countSentSMSClass;
                                        tt.SMS_NUMBER_ADDED = countSentSMSClass;
                                        breakout = true;
                                    }
                                    objClassSentDetail.UPDATED_DATE = DateTime.Now;
                                    if (objClassSentDetail.SMS_PARENT_CONTRACT_CLASS_ID > 0)
                                    {
                                        this.SMSParentContractInClassDetailBusiness.Update(objClassSentDetail);
                                    }
                                    //Luu lai quy tin cu
                                    tt.SCHOOL_ID = schoolID;
                                    tt.TRANSACTION_TYPE = GlobalConstantsEdu.TIMER_TRANSACTION_CONTRACT_IN_CLASS;
                                    tt.IS_ACTIVE = true;
                                    tt.SMS_NUMBER_ADDED = countTotalSentSMS;
                                    tt.SMS_PARENT_CONTRACT_CLASS_ID = objClassSentDetail.SMS_PARENT_CONTRACT_CLASS_ID;
                                    tt.TIMER_CONFIG_ID = tc.SMS_TIMER_CONFIG_ID;

                                    this.TimerTransactionBusiness.Insert(tt);
                                    if (breakout)
                                    {
                                        break;
                                    }
                                }
                                #endregion

                                // enabled detectChange and validateOnSave
                                //this.Context.Configuration.AutoDetectChangesEnabled = true;
                                //this.Context.Configuration.ValidateOnSaveEnabled = true;
                                this.Save();

                                if (isTimerUsed && lstSmsParentSentDetailInsert.Count > 0)
                                {
                                    for (int i = 0; i < lstSmsParentSentDetailInsert.Count; i++)
                                    {
                                        SMS_PARENT_SENT_DETAIL ssd = lstSmsParentSentDetailInsert[i];
                                        //Luu lai quy tin cu
                                        SMS_TIMER_TRANSACTION tt = new SMS_TIMER_TRANSACTION();
                                        tt.SCHOOL_ID = schoolID;
                                        tt.TRANSACTION_TYPE = GlobalConstantsEdu.TIMER_TRANSACTION_SENT_DETAIL;
                                        tt.IS_ACTIVE = true;
                                        tt.SMS_PARENT_SENT_DETAIL_ID = ssd.SMS_PARENT_SENT_DETAIL_ID;
                                        tt.SENT_TOTAL = ssd.SENT_TOTAL;
                                        tt.TIMER_CONFIG_ID = tc.SMS_TIMER_CONFIG_ID;

                                        this.TimerTransactionBusiness.Insert(tt);


                                    }
                                    this.TimerTransactionBusiness.Save();
                                }

                                resultBO.NumSendSuccess = countNumOfSend;
                                return resultBO;
                                #endregion
                            }
                            #endregion

                        }
                    case GlobalConstantsEdu.COMMON_SMS_COMUNICATION_TEACHER_TO_TEACHER:
                        {
                            #region teacher to teacher
                            SMSSchoolTeacherBO objInputSMS = new SMSSchoolTeacherBO
                            {
                                AcademicYearBO = academicYearBO,
                                ClassId = classID,
                                ContactGroupId = contactGroupID,
                                Content = content,
                                CountSMS = countSMS,
                                EducationLevelId = iEducationLevelID,
                                IsAdmin = isAdmin,
                                IsPrincipal = isPrincipal,
                                IsSendSMSCommentOfPupil = isSendSMSCommentOfPupil,
                                IsSignMsg = isSignMsg,
                                LevelId = iLevelID,
                                ListContent = contentList,
                                ListReceiverId = lstReceiverID,
                                ListShortContent = lstShortContent,
                                Promotion = promotion,
                                School = school,
                                SchoolId = schoolID,
                                SchoolUserName = schoolUserName,
                                Semester = semester,
                                SendAllSchool = sendAllSchool,
                                SenderGroup = senderGroup,
                                SenderID = senderID,
                                ShortContent = shortContent,
                                Type = type,
                                TypeHistory = typeHistory,
                                AcademicYearID = AcademicYearId

                            };
                            resultBO = SendSmstoTeacherExternal(objInputSMS, SchoolName);

                            #endregion
                            break;
                        }
                    default:
                        break;
                }
            }
            catch (DbEntityValidationException e)
            {
                foreach (var eve in e.EntityValidationErrors)
                {
                    System.Diagnostics.Debug.WriteLine("Entity of type \"{0}\" in state \"{1}\" has the following validation errors:",
                        eve.Entry.Entity.GetType().Name, eve.Entry.State);
                    foreach (var ve in eve.ValidationErrors)
                    {
                        System.Diagnostics.Debug.WriteLine("- Property: \"{0}\", Error: \"{1}\"",
                            ve.PropertyName, ve.ErrorMessage);
                    }
                }
                string para = "null";
                LogExtensions.ErrorExt(logger, DateTime.Now, "SendSMSExternal", para, e);
            }
            catch (Exception ex)
            {
                string para = "null";
                LogExtensions.ErrorExt(logger, DateTime.Now, "SendSMSExternal", para, ex);
            }
            finally
            {
                SetAutoDetectChangesEnabled(true);
            }


            return resultBO;
        }

        public ResultBO SendSmstoTeacherExternal(SMSSchoolTeacherBO inputSMS, string schoolName)
        {
            ResultBO resultBO = new ResultBO();
            try
            {
                SetAutoDetectChangesEnabled(false);
                SMS_SENT_RECORD sentRecord = null;
                bool isInsertSentRecord = false;
                DateTime dateTimeNow = DateTime.Now;
                int partitionSMSHistory = UtilsBusiness.GetPartionId(inputSMS.SchoolId);
                int partitionSMSCommunication = UtilsBusiness.GetPartionId(inputSMS.SchoolId);
                int partitionSMSParentContract = UtilsBusiness.GetPartionId(inputSMS.SchoolId);
                AcademicYear objAy = AcademicYearBusiness.Find(inputSMS.AcademicYearID);

                List<BrandNameUsingBO> lstBrandName = BrandnameRegistrationBusiness.GetBrandNameUsing(inputSMS.SchoolId);
                if (inputSMS.School != null && inputSMS.School.SMSTeacherActiveType != 0 && inputSMS.School.SMSTeacherActiveType == GlobalConstantsEdu.COMMON_SCHOOL_SMS_ACTIVE_NO_LIMIT)
                {
                    sentRecord = this.SentRecordBusiness.All.FirstOrDefault(s => s.SCHOOL_ID == inputSMS.SchoolId
                                                                                        && s.SENT_MONTH == dateTimeNow.Month
                                                                                        && s.SENT_YEAR == dateTimeNow.Year);
                    if (sentRecord == null)
                    {
                        sentRecord = new SMS_SENT_RECORD();
                        sentRecord.SENT_COUNT = 0;
                        sentRecord.SENT_TIME = 0;
                        sentRecord.SENT_MONTH = dateTimeNow.Month;
                        sentRecord.SENT_YEAR = dateTimeNow.Year;
                        sentRecord.SCHOOL_ID = inputSMS.SchoolId;
                        sentRecord.PARTITION_ID = UtilsBusiness.GetPartionId(inputSMS.SchoolId);
                        isInsertSentRecord = true;
                    }

                    sentRecord.SENT_DATE = dateTimeNow;
                }
                else
                {
                    resultBO.isError = true;
                    resultBO.ErrorMsg = "Có lỗi xảy ra trong quá trình gửi thư. Thầy/cô vui lòng nhấn F5 và thực hiện gửi lại!";
                    return resultBO;
                }

                string fullNameSender = string.Empty;
                List<TeacherBO> lstEmployeeContactBO = new List<TeacherBO>();
                SMS_TEACHER_CONTACT_GROUP objContactGroup = this.ContactGroupBusiness.All.Where(p => p.CONTACT_GROUP_ID == inputSMS.ContactGroupId && p.SCHOOL_ID == inputSMS.SchoolId).FirstOrDefault();
                //kiem tra neu la defaultGroup thi gui cho toan truong
                if (objContactGroup.IS_DEFAULT)
                {
                    //chi co admin voi ban giam hieu moi duoc quyen gui tin nhan toan truong
                    if (inputSMS.IsAdmin)
                    {
                        if (inputSMS.ListReceiverId != null && inputSMS.ListReceiverId.Count > 0) lstEmployeeContactBO = EmployeeBusiness.GetListTeacher(inputSMS.ListReceiverId);
                        else lstEmployeeContactBO = EmployeeBusiness.GetTeachersOfSchool(inputSMS.SchoolId, inputSMS.AcademicYearID);

                        fullNameSender = schoolName;
                    }
                    else if (inputSMS.IsPrincipal)
                    {
                        inputSMS.IsAdmin = true;
                        if (inputSMS.ListReceiverId != null && inputSMS.ListReceiverId.Count > 0)
                            lstEmployeeContactBO = EmployeeBusiness.GetListTeacher(inputSMS.ListReceiverId);
                        else lstEmployeeContactBO = EmployeeBusiness.GetTeachersOfSchool(inputSMS.SchoolId, inputSMS.AcademicYearID);

                        if (lstEmployeeContactBO != null && lstEmployeeContactBO.Count > 0)
                            lstEmployeeContactBO = lstEmployeeContactBO.Where(p => p.TeacherID != inputSMS.SenderID).ToList();

                        Employee teacherBO = EmployeeBusiness.Find(inputSMS.SenderID);
                        if (teacherBO != null) fullNameSender = teacherBO.FullName;
                    }
                }
                else
                {
                    IQueryable<TeacherBO> iqtmp = (from ec in this.TeacherContactBusiness.All
                                                   join cg in this.ContactGroupBusiness.All on ec.CONTACT_GROUP_ID equals cg.CONTACT_GROUP_ID
                                                   where ec.CONTACT_GROUP_ID == inputSMS.ContactGroupId
                                                   && ec.IS_ACTIVE == true
                                                   && ec.TEACHER_ID != inputSMS.SenderID
                                                   && cg.SCHOOL_ID == inputSMS.SchoolId
                                                   select new TeacherBO
                                                   {
                                                       TeacherID = ec.TEACHER_ID,
                                                   });
                    if (inputSMS.ListReceiverId.Count > 0)
                    {
                        iqtmp = iqtmp.Where(p => inputSMS.ListReceiverId.Contains(p.TeacherID));
                    }
                    lstEmployeeContactBO = iqtmp.ToList();

                    List<int> lstTeacherInContact = lstEmployeeContactBO.Select(p => p.TeacherID).ToList();
                    lstEmployeeContactBO = (from e in EmployeeBusiness.All
                                            join f in lstTeacherInContact on e.EmployeeID equals f
                                            where e.EmployeeType == SystemParamsInFile.EMPLOYEE_TYPE_SCHOOL_TEACHER
                                            && e.EmploymentStatus == SystemParamsInFile.EMPLOYMENT_STATUS_WORKING
                                            && e.IsActive == true
                                            select new TeacherBO
                                            {
                                                TeacherID = e.EmployeeID,
                                                TeacherCode = e.EmployeeCode,
                                                FullName = e.FullName,
                                                Genre = e.Genre,
                                                Name = e.Name,
                                                Mobile = e.Mobile,
                                                EmployeeStatus = e.EmploymentStatus.Value
                                            }).ToList();

                    if (inputSMS.IsAdmin) fullNameSender = schoolName;
                    else
                    {
                        Employee teacherBO = EmployeeBusiness.Find(inputSMS.SenderID);
                        if (teacherBO != null) fullNameSender = teacherBO.FullName;
                    }
                }

                if (string.IsNullOrEmpty(fullNameSender))
                {
                    resultBO.isError = true;
                    resultBO.ErrorMsg = "Có lỗi xảy ra trong quá trình gửi thư. Thầy/cô vui lòng nhấn F5 và thực hiện gửi lại!";
                    return resultBO;
                }
                if (lstEmployeeContactBO == null || lstEmployeeContactBO.Count == 0)
                {
                    resultBO.isError = true;
                    resultBO.ErrorMsg = "SendSMS_Label_ContactGroupError";
                    return resultBO;
                }

                #region Han muc tin nhan theo chuong trinh KM
                List<int> LstTeacherReceived = lstEmployeeContactBO.Select(o => o.TeacherID).ToList();

                //Lay tin nhan da gui trong thang 
                List<SMS_RECEIVED_RECORD> LstReceivedRecord = (from rr in this.ReceivedRecordBusiness.All.AsNoTracking()
                                                          where rr.SCHOOL_ID == inputSMS.SchoolId && rr.RECEIVED_TYPE == GlobalConstantsEdu.COMMON_HISTORY_RECEIVER_TEACHER_ID
                                                          && rr.RECEIVED_YEAR == dateTimeNow.Year && rr.RECEIVED_MONTH == dateTimeNow.Month
                                                          && LstTeacherReceived.Contains(rr.EMPLOYEE_ID)
                                                          && rr.PARTITION_ID == partitionSMSHistory
                                                          select rr).ToList();

                #endregion

                TeacherBO objEmployeeReceiver = null;

                SMS_MT objMT = null;
                SMS_HISTORY objHistorySMS = null;
                int countNumOfSend = 0;
                decimal priceSendSMS = 0;
                // AnhVD9 20141027 - Bổ sung thông tin số tin nội, ngoại mạng
                int InternalSendSMS = 0;
                int ExternalSendSMS = 0;

                SMS_RECEIVED_RECORD recordReceived = null;
                //bool isFirstInsert = true;
                //schoolUserName = systemLoaderBusiness.GetUserNameBySchoolID(inputSMS.SchoolId);

                ///Neu la tin nhan khong dau thi loai bo dau
                string contentStripVNSign = "";
                if (!inputSMS.IsSignMsg)
                {
                    contentStripVNSign = ExtensionMethods.ConvertLineToSpace(inputSMS.Content.StripVNSign().StripVNSignComposed());
                }
                string brandName = GlobalConstantsEdu.BRANDNAME_DEFAULT;
                for (int i = lstEmployeeContactBO.Count - 1; i >= 0; i--)
                {

                    #region Tinh toan gui tin nhan cho tung giao vien
                    objEmployeeReceiver = lstEmployeeContactBO[i];
                    // Chỉ cho phép gửi tin có dấu với các đầu số cho trước
                    if (inputSMS.IsSignMsg && !objEmployeeReceiver.Mobile.isAllowSendSignSMSPhone()) continue;
                    if (!objEmployeeReceiver.Mobile.CheckMobileNumber()) continue;

                    brandName = GetBrandNamebyMobile(objEmployeeReceiver.Mobile, lstPreNumberTelco, lstBrandName);
                    //insert historySMS 
                    objHistorySMS = new SMS_HISTORY();
                    objHistorySMS.MOBILE = objEmployeeReceiver.Mobile;
                    objHistorySMS.IS_INTERNAL = objHistorySMS.MOBILE.CheckMobileNumberVT();
                    objHistorySMS.RECEIVER_NAME = objEmployeeReceiver.FullName;
                    if (inputSMS.TypeHistory.HasValue && inputSMS.TypeHistory.Value != 0) objHistorySMS.SMS_TYPE_ID = inputSMS.TypeHistory.Value;
                    else objHistorySMS.SMS_TYPE_ID = GlobalConstantsEdu.COMMON_HISTORYSMS_TO_COMMUNICATION_TEACHER;

                    objHistorySMS.RECEIVE_TYPE = GlobalConstantsEdu.COMMON_HISTORY_RECEIVER_TEACHER_ID;
                    objHistorySMS.CREATE_DATE = dateTimeNow;
                    objHistorySMS.SCHOOL_ID = inputSMS.SchoolId;
                    objHistorySMS.YEAR = objAy.Year;
                    objHistorySMS.SMS_CONTENT = inputSMS.Content;
                    objHistorySMS.SENDER_NAME = fullNameSender;
                    objHistorySMS.SHORT_CONTENT = inputSMS.ShortContent;
                    objHistorySMS.RECEIVER_ID = objEmployeeReceiver.TeacherID;
                    objHistorySMS.SMS_CNT = inputSMS.CountSMS;
                    objHistorySMS.SCHOOL_USERNAME = inputSMS.SchoolUserName;
                    objHistorySMS.SENDER_GROUP = inputSMS.SenderGroup;
                    objHistorySMS.CONTENT_TYPE = inputSMS.IsSignMsg ? GlobalConstantsEdu.SMS_CONTENT_TYPE_UNICODE : GlobalConstantsEdu.SMS_CONTENT_TYPE_NOT_SIGN;
                    objHistorySMS.IS_SMS = true;
                    objHistorySMS.PARTITION_ID = partitionSMSHistory;
                    objHistorySMS.SERVICE_ID = brandName;
                    objHistorySMS.IS_ADMIN = inputSMS.IsAdmin;
                    objHistorySMS.SENDER_ID = inputSMS.SenderID;
                    objHistorySMS.CONTACT_GROUP_ID = inputSMS.ContactGroupId;
                    Guid guid = Guid.NewGuid();
                    objHistorySMS.HISTORY_RAW_ID = guid;
                    this.SMSHistoryBusiness.Insert(objHistorySMS);

                    // ReceivedRecord
                    recordReceived = LstReceivedRecord.FirstOrDefault(o => o.EMPLOYEE_ID == objEmployeeReceiver.TeacherID);
                    // Luu thong tin tin nhan da gui
                    if (recordReceived == null)
                    {
                        recordReceived = new SMS_RECEIVED_RECORD();
                        recordReceived.SCHOOL_ID = inputSMS.SchoolId;
                        recordReceived.RECEIVED_TYPE = GlobalConstantsEdu.COMMON_HISTORY_RECEIVER_TEACHER_ID;
                        recordReceived.EMPLOYEE_ID = objEmployeeReceiver.TeacherID;
                        recordReceived.RECEIVED_YEAR = dateTimeNow.Year;
                        recordReceived.RECEIVED_MONTH = dateTimeNow.Month;
                        recordReceived.TOTAL_RECEIVED_SMS = 0;
                        recordReceived.CREATED_TIME = dateTimeNow.Date;
                        recordReceived.TOTAL_INTERNAL_SMS = 0;
                        recordReceived.TOTAL_EXTERNAL_SMS = 0;
                        recordReceived.PARTITION_ID = partitionSMSHistory;
                        this.ReceivedRecordBusiness.Insert(recordReceived);
                    }

                    //insert MT
                    objMT = new SMS_MT();
                    objMT.REQUEST_ID = Guid.NewGuid();
                    objMT.HISTORY_RAW_ID = guid;
                    if (inputSMS.IsSignMsg)
                    {
                        objMT.CONTENT_TYPE = GlobalConstantsEdu.SMS_CONTENT_TYPE_UNICODE.ToString(); // có dấu
                        objMT.SMS_CONTENT = inputSMS.Content;
                    }
                    else
                    {
                        objMT.CONTENT_TYPE = GlobalConstantsEdu.SMS_CONTENT_TYPE_NOT_SIGN.ToString(); // ko dấu
                        objMT.SMS_CONTENT = contentStripVNSign;
                    }

                    objMT.STATUS = GlobalConstantsEdu.MT_STATUS_NOT_SEND;
                    objMT.TIME_SEND_REQUEST = dateTimeNow;
                    objMT.USER_ID = objHistorySMS.MOBILE;
                    objMT.UNIT_ID = inputSMS.SchoolId;
                    objMT.SERVICE_ID = brandName;
                    if (objMT.USER_ID.isSupportPhone()) objMT.MT_TYPE = GlobalConstantsEdu.COMMON_HISTORYSMS_TO_TEACHER_ID;
                    this.MTBusiness.Insert(objMT);

                    //cap nhat lai so tin nhan da gui
                    if (sentRecord != null) sentRecord.SENT_COUNT += inputSMS.CountSMS;

                    // tru tien trong tai khoan
                    PriceSendSMSBO objPriceSMS = CalculateSMSPromotion(objHistorySMS, inputSMS.Promotion, inputSMS.CountSMS, recordReceived.TOTAL_RECEIVED_SMS, recordReceived.TOTAL_INTERNAL_SMS, recordReceived.TOTAL_EXTERNAL_SMS);
                    if (objPriceSMS != null)
                    {
                        priceSendSMS += objPriceSMS.PriceSendSMS;
                        InternalSendSMS += objPriceSMS.InternalSendSMS;
                        ExternalSendSMS += objPriceSMS.ExternalSendSMS;
                        recordReceived.TOTAL_RECEIVED_SMS = objPriceSMS.TotalReceivedSMS;
                        recordReceived.TOTAL_INTERNAL_SMS = objPriceSMS.TotalInternalSMS;
                        recordReceived.TOTAL_EXTERNAL_SMS = objPriceSMS.TotalExternalSMS;
                    }

                    //Cap nhat tin nha da nhan
                    if (recordReceived.SMS_RECEIVED_RECORD_ID > 0)
                    {
                        recordReceived.UPDATED_TIME = DateTime.Now;
                        this.ReceivedRecordBusiness.Update(recordReceived);
                    }

                    countNumOfSend++;
                    #endregion
                } // End loop for


                if (countNumOfSend == 0)
                {
                    resultBO.isError = false;
                    resultBO.NumSendSuccess = 0;
                    return resultBO;
                }

                if (sentRecord != null)
                {
                    //cap nhat lai sendTime
                    sentRecord.SENT_TIME++;
                    if (isInsertSentRecord)
                    {
                        this.SentRecordBusiness.Insert(sentRecord);
                    }
                    else
                    {
                        this.SentRecordBusiness.Update(sentRecord);
                    }
                }

                // AnhVD9 20141028 - Kiểm tra tài khoản
                EwalletResultBO ewalletRS = this.ComputeBalanceAccountMobile(inputSMS.SchoolId, UserType.SchoolUser, priceSendSMS, InternalSendSMS, ExternalSendSMS);
                if (ewalletRS != null)
                {
                    if (ewalletRS.OK)
                    {
                        // enabled detectChange and validateOnSave
                        this.Save();

                        resultBO.NumSendSuccess = countNumOfSend;

                        // tru tien trong vi dien tu
                        Dictionary<string, object> dicEwallet = new Dictionary<string, object>();
                        dicEwallet["EWalletID"] = ewalletRS.EwalletID;
                        dicEwallet["Amount"] = ewalletRS.SubMainBalance;
                        dicEwallet["TransTypeID"] = GlobalConstantsEdu.TRANSACTION_TYPE_AMOUNT_SUB;
                        dicEwallet["ServiceType"] = GlobalConstantsEdu.SERVICE_TYPE_SENT_TO_TECHER;
                        dicEwallet["Quantity"] = ewalletRS.TotalSMSInMainBalance;
                        dicEwallet["PromotionAccountID"] = ewalletRS.PromotionAccountID;
                        dicEwallet["PromotionBalance"] = ewalletRS.SubPromBalance;
                        dicEwallet["InternalBalance"] = ewalletRS.SubInternalBalance;
                        dicEwallet["InternalSMS"] = ewalletRS.SubPromOfInSMS;
                        dicEwallet["ExternalSMS"] = ewalletRS.SubPromOfExSMS;
                        EWalletObject ewallet = EWalletBusiness.DeductMoneyAccount(dicEwallet);
                        resultBO.Balance = ewallet.Balance.ToString("#,##0");
                        resultBO.NumOfPromotionInternalSMS = ewallet.InternalSMS;
                        resultBO.TransAmount = ewalletRS.SubMainBalance;
                        resultBO.TotalSMSSentInMainBalance = ewalletRS.TotalSMSInMainBalance;
                        return resultBO;
                    }
                    else
                    {
                        resultBO.isError = true;
                        resultBO.ErrorMsg = ewalletRS.ErrorMsg;
                        return resultBO;
                    }
                }
                else
                {
                    resultBO.isError = true;
                    resultBO.ErrorMsg = "Có lỗi xảy ra trong quá trình gửi thư. Thầy/cô vui lòng thực hiện gửi lại!";
                    return resultBO;
                }

            }
            catch (Exception ex)
            {
                string para = string.Format("academicYearId={0}, schoolId={1}", inputSMS.AcademicYearID, inputSMS.SchoolId);
                LogExtensions.ErrorExt(logger, DateTime.Now, "SendSmstoTeacherExternal", para, ex);
                resultBO.isError = true;
                resultBO.ErrorMsg = "Có lỗi xảy ra trong quá trình gửi thư. Thầy/cô vui lòng thực hiện gửi lại!";
                return resultBO;
            }
            finally
            {
                SetAutoDetectChangesEnabled(true);
            }
        }

        public string GetBrandNamebyMobile(string mobile, List<PreNumberTelco> lstPreNumberTelco, List<BrandNameUsingBO> lstBrandName)
        {

            string brandName = GlobalConstantsEdu.BRANDNAME_DEFAULT;
            if (lstBrandName == null || lstBrandName.Count == 0)
            {
                return brandName;
            }
            if (string.IsNullOrEmpty(mobile))
            {
                return brandName;
            }
            if ((mobile.Length < 9) || (mobile.Length > 12))
            {
                return brandName;
            }
            string number2 = mobile.Substring(0, mobile.Length - 7);
            var objTelco = lstPreNumberTelco.FirstOrDefault(s => s.PrefixNumber.Contains(number2) && s.PrefixNumber.Length == number2.Length);
            if (objTelco == null)
            {
                return brandName;
            }
            BrandNameUsingBO objBrandName = lstBrandName.FirstOrDefault(s => s.ProviderName.ToUpper() == objTelco.Telco.ToUpper());
            if (objBrandName != null)
            {
                brandName = objBrandName.Brandname;
            }

            return brandName;
        }

        /// <summary>
        /// Kiểm tra tài khoản có đủ số tin cần gửi hay không?
        /// </summary>
        /// <author date="2014/10/28">AnhVD9</author>
        /// <param name="userID"></param>
        /// <param name="priceSendSMS"></param>
        /// <param name="internalSMS"></param>
        /// <param name="externalSMS"></param>
        /// <returns></returns>
        public EwalletResultBO ComputeBalanceAccount(int userID, UserType userType, decimal priceSendSMS, int internalSMS, int externalSMS)
        {
            int InternalSMSPrice = Convert.ToInt32(WebConfigurationManager.AppSettings["PriceSMSInternal"]);
            int ExternalSMSPrice = Convert.ToInt32(WebConfigurationManager.AppSettings["PriceSMSExternal"]);
            // check ewallet
            var ewallet = EWalletBusiness.GetEWalletIncludePromotion(userID, userType == UserType.SchoolUser, userType == UserType.SupervisingDeptUser);
            if (ewallet == null)
            {
                return new EwalletResultBO()
                {
                    isError = true,
                    ErrorMsg = "Có lỗi xảy ra trong quá trình gửi thư. Thầy/cô vui lòng nhấn F5 và thực hiện gửi lại!"
                };
            }
            // Điều kiện cần
            bool preRequisites = (ewallet.Balance + ewallet.PromotionBalance + ewallet.InternalSMS * InternalSMSPrice + ewallet.InternalBalance + ewallet.ExternalSMS * ExternalSMSPrice) >= priceSendSMS ?
                                                         true : false;
            /* 
             * Điều kiện đủ phải tính đến chi tiết tin nhắn 
             * Do có trường hợp tổng tiền thì đủ nhưng là ở mỗi tài khoản là không đủ
             * Ràng buộc số tiền trừ luôn là bội số của giá cước  
             * 
             */

            if (preRequisites)
            {
                EwalletResultBO eResult = new EwalletResultBO();

                #region Ưu tiên lấy tài khoản khuyến mãi và Trừ tiền tin nhắn nội mạng trước
                // so tin noi mang o tai khoan khuyen mai (bao gom ca tin noi mang khuyen mai va tai khoan noi mang khuyen mai)
                int promInternalSMS = ewallet.InternalSMS + (int)(ewallet.InternalBalance / InternalSMSPrice);
                decimal remainInternalPrice = 0;
                if (internalSMS > promInternalSMS)
                {
                    remainInternalPrice = InternalSMSPrice * (internalSMS - promInternalSMS);
                }
                decimal remainPromotionPrice = ewallet.PromotionBalance >= remainInternalPrice ? ewallet.PromotionBalance - remainInternalPrice : -1;
                #endregion

                int promExternalSMS = ewallet.ExternalSMS;
                // Neu tai khoan khuyen mai con tien
                if (remainPromotionPrice >= 0)
                {
                    #region Tài khoản khuyến mãi còn tiền
                    promExternalSMS += (int)(remainPromotionPrice / ExternalSMSPrice);
                    // Truong hop sau khi tru di so tin noi mang van con tien trong tai khoan khuyen mai noi mang
                    if (remainInternalPrice > 0) // Neu co tru vao tai khoan khuyen mai
                    {
                        eResult.SubPromOfInSMS = ewallet.InternalSMS;
                        eResult.SubInternalBalance = ewallet.InternalBalance - (promInternalSMS - ewallet.InternalSMS) * InternalSMSPrice;
                        eResult.SubPromBalance = ewallet.PromotionBalance - remainInternalPrice;
                    }
                    else // Neu chua dung het tin noi mang khuyen mai
                    {
                        if (ewallet.InternalSMS > internalSMS)
                        {
                            eResult.SubPromOfInSMS = internalSMS;
                        }
                        else
                        {
                            eResult.SubPromOfInSMS = ewallet.InternalSMS;
                            eResult.SubInternalBalance = ewallet.InternalBalance - (internalSMS - ewallet.InternalSMS) * InternalSMSPrice;
                        }
                    }

                    #region Kiểm tra tài khoản khuyến mãi để gửi tin ngoại mạng
                    if (promExternalSMS > externalSMS) // nếu đủ tiền để gửi tin ngoại mạng
                    {
                        if (ewallet.ExternalSMS > externalSMS)
                        {
                            eResult.SubPromOfExSMS = externalSMS;
                        }
                        else
                        {
                            eResult.SubPromOfExSMS = ewallet.ExternalSMS;
                            eResult.SubPromBalance += remainPromotionPrice - (externalSMS - ewallet.ExternalSMS) * ExternalSMSPrice;
                        }
                    }
                    else // nếu tài khoản khuyến mãi không đủ thì trừ tài khoản chính
                    {
                        eResult.SubPromOfExSMS = ewallet.ExternalSMS;
                        eResult.SubPromBalance += (promExternalSMS - ewallet.ExternalSMS) * ExternalSMSPrice;
                        eResult.SubMainBalance = (externalSMS - promExternalSMS) * ExternalSMSPrice;
                        // số lượng tin nhắn trừ ở tài khoản chính
                        eResult.TotalSMSInMainBalance += (externalSMS - promExternalSMS);
                    }
                    #endregion
                    #endregion
                }
                else
                {
                    #region Nếu không có đủ tiền ở tài khoản khuyến mãi để gửi tin nội mạng
                    // Noi mang
                    eResult.SubPromOfInSMS = ewallet.InternalSMS;
                    eResult.SubInternalBalance = (promInternalSMS - ewallet.InternalSMS) * InternalSMSPrice;
                    int inSMSPromBalance = (int)(ewallet.PromotionBalance / InternalSMSPrice);
                    eResult.SubPromBalance = inSMSPromBalance * InternalSMSPrice;
                    eResult.SubMainBalance = (internalSMS - promInternalSMS - inSMSPromBalance) * InternalSMSPrice;
                    // số lượng tin nhắn trừ ở tài khoản chính
                    eResult.TotalSMSInMainBalance += (internalSMS - promInternalSMS - inSMSPromBalance);
                    // Ngoai mang
                    if (promExternalSMS > externalSMS)
                    {
                        eResult.SubPromOfExSMS = externalSMS;
                    }
                    else
                    {
                        eResult.SubPromOfExSMS = ewallet.ExternalSMS;
                        eResult.SubMainBalance += (externalSMS - promExternalSMS) * ExternalSMSPrice;
                        // số lượng tin nhắn trừ ở tài khoản chính
                        eResult.TotalSMSInMainBalance += (externalSMS - promExternalSMS);
                    }
                    #endregion
                }

                if (eResult.SubMainBalance == 0 || ewallet.Balance >= eResult.SubMainBalance)
                {
                    eResult.OK = true;
                    eResult.isError = false;
                    eResult.OutOfBalance = false;
                    eResult.EwalletID = ewallet.EWalletID;
                    eResult.PromotionAccountID = ewallet.PromotionAccountID;
                }
                else
                {
                    eResult.OK = false;
                    eResult.OutOfBalance = true;
                    eResult.isError = true;
                    eResult.ErrorMsg = (userType == UserType.SchoolUser ? "Số tiền trong tài khoản của trường không đủ để thực hiện gửi tin nhắn.<br />Thầy/cô vui lòng vào chức năng tài khoản để nạp tiền và thực hiện gửi lại tin nhắn." 
                        : "Số tiền trong tài khoản của đơn vị không đủ để thực hiện gửi tin nhắn.<br />Thầy/cô vui lòng vào chức năng tài khoản để nạp tiền và thực hiện gửi lại tin nhắn.");
                }
                return eResult;
            }
            else
            {
                return new EwalletResultBO()
                {
                    OK = false,
                    OutOfBalance = true,
                    isError = true,
                    ErrorMsg = (userType == UserType.SchoolUser ? "Số tiền trong tài khoản của trường không đủ để thực hiện gửi tin nhắn.<br />Thầy/cô vui lòng vào chức năng tài khoản để nạp tiền và thực hiện gửi lại tin nhắn." 
                    : "Số tiền trong tài khoản của đơn vị không đủ để thực hiện gửi tin nhắn.<br />Thầy/cô vui lòng vào chức năng tài khoản để nạp tiền và thực hiện gửi lại tin nhắn.")
                };
            }
        }

        public EwalletResultBO ComputeBalanceAccountMobile(int userID, UserType userType, decimal priceSendSMS, int internalSMS, int externalSMS)
        {
            int InternalSMSPrice = Convert.ToInt32(WebConfigurationManager.AppSettings["PriceSMSInternal"]);
            int ExternalSMSPrice = Convert.ToInt32(WebConfigurationManager.AppSettings["PriceSMSExternal"]);
            // check ewallet
            var ewallet = EWalletBusiness.GetEWalletIncludePromotion(userID, userType == UserType.SchoolUser, userType == UserType.SupervisingDeptUser);
            if (ewallet == null)
            {
                return new EwalletResultBO()
                {
                    isError = true,
                    ErrorMsg = "Có lỗi xảy ra trong quá trình gửi thư. Thầy/cô vui lòng thực hiện gửi lại!"
                };
            }
            // Điều kiện cần
            bool preRequisites = (ewallet.Balance + ewallet.PromotionBalance + ewallet.InternalSMS * InternalSMSPrice + ewallet.InternalBalance + ewallet.ExternalSMS * ExternalSMSPrice) >= priceSendSMS ?
                                                         true : false;
            /* 
             * Điều kiện đủ phải tính đến chi tiết tin nhắn 
             * Do có trường hợp tổng tiền thì đủ nhưng là ở mỗi tài khoản là không đủ
             * Ràng buộc số tiền trừ luôn là bội số của giá cước  
             * 
             */

            if (preRequisites)
            {
                EwalletResultBO eResult = new EwalletResultBO();

                #region Ưu tiên lấy tài khoản khuyến mãi và Trừ tiền tin nhắn nội mạng trước
                // so tin noi mang o tai khoan khuyen mai (bao gom ca tin noi mang khuyen mai va tai khoan noi mang khuyen mai)
                int promInternalSMS = ewallet.InternalSMS + (int)(ewallet.InternalBalance / InternalSMSPrice);
                decimal remainInternalPrice = 0;
                if (internalSMS > promInternalSMS)
                {
                    remainInternalPrice = InternalSMSPrice * (internalSMS - promInternalSMS);
                }
                decimal remainPromotionPrice = ewallet.PromotionBalance >= remainInternalPrice ? ewallet.PromotionBalance - remainInternalPrice : -1;
                #endregion

                int promExternalSMS = ewallet.ExternalSMS;
                // Neu tai khoan khuyen mai con tien
                if (remainPromotionPrice >= 0)
                {
                    #region Tài khoản khuyến mãi còn tiền
                    promExternalSMS += (int)(remainPromotionPrice / ExternalSMSPrice);
                    // Truong hop sau khi tru di so tin noi mang van con tien trong tai khoan khuyen mai noi mang
                    if (remainInternalPrice > 0) // Neu co tru vao tai khoan khuyen mai
                    {
                        eResult.SubPromOfInSMS = ewallet.InternalSMS;
                        eResult.SubInternalBalance = ewallet.InternalBalance - (promInternalSMS - ewallet.InternalSMS) * InternalSMSPrice;
                        eResult.SubPromBalance = ewallet.PromotionBalance - remainInternalPrice;
                    }
                    else // Neu chua dung het tin noi mang khuyen mai
                    {
                        if (ewallet.InternalSMS > internalSMS)
                        {
                            eResult.SubPromOfInSMS = internalSMS;
                        }
                        else
                        {
                            eResult.SubPromOfInSMS = ewallet.InternalSMS;
                            eResult.SubInternalBalance = ewallet.InternalBalance - (internalSMS - ewallet.InternalSMS) * InternalSMSPrice;
                        }
                    }

                    #region Kiểm tra tài khoản khuyến mãi để gửi tin ngoại mạng
                    if (promExternalSMS > externalSMS) // nếu đủ tiền để gửi tin ngoại mạng
                    {
                        if (ewallet.ExternalSMS > externalSMS)
                        {
                            eResult.SubPromOfExSMS = externalSMS;
                        }
                        else
                        {
                            eResult.SubPromOfExSMS = ewallet.ExternalSMS;
                            eResult.SubPromBalance += remainPromotionPrice - (externalSMS - ewallet.ExternalSMS) * ExternalSMSPrice;
                        }
                    }
                    else // nếu tài khoản khuyến mãi không đủ thì trừ tài khoản chính
                    {
                        eResult.SubPromOfExSMS = ewallet.ExternalSMS;
                        eResult.SubPromBalance += (promExternalSMS - ewallet.ExternalSMS) * ExternalSMSPrice;
                        eResult.SubMainBalance = (externalSMS - promExternalSMS) * ExternalSMSPrice;
                        // số lượng tin nhắn trừ ở tài khoản chính
                        eResult.TotalSMSInMainBalance += (externalSMS - promExternalSMS);
                    }
                    #endregion
                    #endregion
                }
                else
                {
                    #region Nếu không có đủ tiền ở tài khoản khuyến mãi để gửi tin nội mạng
                    // Noi mang
                    eResult.SubPromOfInSMS = ewallet.InternalSMS;
                    eResult.SubInternalBalance = (promInternalSMS - ewallet.InternalSMS) * InternalSMSPrice;
                    int inSMSPromBalance = (int)(ewallet.PromotionBalance / InternalSMSPrice);
                    eResult.SubPromBalance = inSMSPromBalance * InternalSMSPrice;
                    eResult.SubMainBalance = (internalSMS - promInternalSMS - inSMSPromBalance) * InternalSMSPrice;
                    // số lượng tin nhắn trừ ở tài khoản chính
                    eResult.TotalSMSInMainBalance += (internalSMS - promInternalSMS - inSMSPromBalance);
                    // Ngoai mang
                    if (promExternalSMS > externalSMS)
                    {
                        eResult.SubPromOfExSMS = externalSMS;
                    }
                    else
                    {
                        eResult.SubPromOfExSMS = ewallet.ExternalSMS;
                        eResult.SubMainBalance += (externalSMS - promExternalSMS) * ExternalSMSPrice;
                        // số lượng tin nhắn trừ ở tài khoản chính
                        eResult.TotalSMSInMainBalance += (externalSMS - promExternalSMS);
                    }
                    #endregion
                }

                if (eResult.SubMainBalance == 0 || ewallet.Balance >= eResult.SubMainBalance)
                {
                    eResult.OK = true;
                    eResult.isError = false;
                    eResult.OutOfBalance = false;
                    eResult.EwalletID = ewallet.EWalletID;
                    eResult.PromotionAccountID = ewallet.PromotionAccountID;
                }
                else
                {
                    eResult.OK = false;
                    eResult.OutOfBalance = true;
                    eResult.isError = true;
                    eResult.ErrorMsg = (userType == UserType.SchoolUser ? "Số tiền trong tài khoản của trường không đủ để thực hiện gửi tin nhắn. Thầy/cô vui lòng vào chức năng tài khoản để nạp tiền và thực hiện gửi lại tin nhắn." : "Số tiền trong tài khoản của đơn vị không đủ để thực hiện gửi tin nhắn. Thầy/cô vui lòng vào chức năng tài khoản để nạp tiền và thực hiện gửi lại tin nhắn.");
                }
                return eResult;
            }
            else
            {
                return new EwalletResultBO()
                {
                    OK = false,
                    OutOfBalance = true,
                    isError = true,
                    ErrorMsg = (userType == UserType.SchoolUser ? "Số tiền trong tài khoản của trường không đủ để thực hiện gửi tin nhắn. Thầy/cô vui lòng vào chức năng tài khoản để nạp tiền và thực hiện gửi lại tin nhắn." : "Số tiền trong tài khoản của đơn vị không đủ để thực hiện gửi tin nhắn. Thầy/cô vui lòng vào chức năng tài khoản để nạp tiền và thực hiện gửi lại tin nhắn.")
                };
            }
        }

        public List<ConsumptionReportBO> GetReportConsumption(int schoolId, DateTime startDate, DateTime endDate)
        {
            var query1 = from et in this.EWalletTransactionBusiness.All
                         join e in this.EWalletBusiness.All on et.EWALLET_ID equals e.EWALLET_ID
                         where e.UNIT_ID == schoolId
                         && e.EWALLET_TYPE == GlobalConstantsEdu.USER_TYPE_SCHOOL
                         && EntityFunctions.TruncateTime(startDate) <= EntityFunctions.TruncateTime(et.CREATED_TIME)
                         && EntityFunctions.TruncateTime(et.CREATED_TIME) <= EntityFunctions.TruncateTime(endDate)
                         && et.TRANS_AMOUNT > 0
                         select new ConsumptionBO
                         {
                             NDChiPhi = et.TRANS_TYPE_ID == 1 ? "Nạp tiền vào tài khoản" :
                             (et.TRANS_TYPE_ID == 2 && et.SERVICE_TYPE == 1 ? "Thanh toán tiền gửi tin nhắn Giáo viên - Mạng Viettel" :
                             (et.TRANS_TYPE_ID == 2 && et.SERVICE_TYPE >= 2 && et.SERVICE_TYPE < 5 ? "Thanh toán tiền đăng ký thuê bao cho PHHS" :
                             "Thanh toán tiền nợ cước đăng ký thuê bao cho PHHS")),
                             DonGia = et.TRANS_TYPE_ID == 1 ? et.TRANS_AMOUNT :
                             (et.TRANS_TYPE_ID == 2 && et.SERVICE_TYPE == 1 ? 150 : et.TRANS_AMOUNT / et.QUANTITY.Value),
                             SoLuong = et.TRANS_TYPE_ID == 1 ? 1 :
                             (et.TRANS_TYPE_ID == 2 && et.SERVICE_TYPE == 1 ? (decimal)(450 * et.QUANTITY.Value - et.TRANS_AMOUNT) / 300 :
                             et.QUANTITY.Value),
                             SoTien = et.TRANS_TYPE_ID == 1 ? et.TRANS_AMOUNT :
                             (et.TRANS_TYPE_ID == 2 && et.SERVICE_TYPE == 1 ? (decimal)(450 * et.QUANTITY - et.TRANS_AMOUNT) / 2 :
                             et.TRANS_AMOUNT),
                             CreatedTime = et.CREATED_TIME
                         };

            var query2 = from et in this.EWalletTransactionBusiness.All
                         join e in this.EWalletBusiness.All on et.EWALLET_ID equals e.EWALLET_ID
                         where e.UNIT_ID == schoolId
                         && e.EWALLET_TYPE == GlobalConstantsEdu.USER_TYPE_SCHOOL
                         && EntityFunctions.TruncateTime(startDate) <= EntityFunctions.TruncateTime(et.CREATED_TIME)
                         && EntityFunctions.TruncateTime(et.CREATED_TIME) <= EntityFunctions.TruncateTime(endDate)
                         && et.TRANS_TYPE_ID == 2
                         && et.SERVICE_TYPE == 1
                         && et.TRANS_AMOUNT > 0
                         select new ConsumptionBO
                         {
                             NDChiPhi = "Thanh toán tiền gửi tin nhắn Giáo viên - Mạng khác",
                             DonGia = 450,
                             SoLuong = (decimal)(et.TRANS_AMOUNT - 150 * et.QUANTITY) / 300,
                             SoTien = (decimal)(3 * (et.TRANS_AMOUNT - 150 * et.QUANTITY)) / 2,
                             CreatedTime = et.CREATED_TIME
                         };

            var query = query1.Union<ConsumptionBO>(query2).ToList();

            return (from q in query.Where(o => o.SoTien > 0)
                    group q by new { NDChiPhi = q.NDChiPhi, DonGia = q.DonGia, CreateTime = q.CreatedTime.Date } into g
                    select new ConsumptionReportBO
                    {
                        Content = g.Key.NDChiPhi,
                        Price = g.Key.DonGia,
                        Quantity = g.Sum(o => o.SoLuong),
                        Amount = g.Sum(o => o.SoTien),
                        CreatedTime = g.Key.CreateTime
                    }).OrderBy(o=>o.CreatedTime).ToList();
        }

        private PriceSendSMSBO CalculateSMSPromotion(SMS_HISTORY objHistory, PromotionProgramBO promotion, int countSMS, int totalReceivedSMS, int totalInternalSMS, int totalExternalSMS)
        {
            // 201050629 - AnhVD9 - Sửa luồng nhắn tin Phòng/Sở
            // tru tien vao tai khoan
            // so tien ngoai mang/noi mang tru theo chuong trinh KM
            //Tong so tin nhan da gui
            PriceSendSMSBO objVal = new PriceSendSMSBO();

            objVal.TotalReceivedSMS = totalReceivedSMS;
            objVal.TotalInternalSMS = totalInternalSMS;
            objVal.TotalExternalSMS = totalExternalSMS;


            if (objHistory.MOBILE.CheckMobileNumberVT())
            {
                if (promotion != null)
                {
                    if (promotion.IsFreeSMS.HasValue && promotion.IsFreeSMS.Value)//TH dang ky mien phi tin nhan noi mang
                    {
                        objVal.TotalReceivedSMS += countSMS;
                        objVal.TotalInternalSMS += countSMS;
                    }
                    else
                    {
                        //Neu tong tin nhan da nhan + tin nhan gui>tin nhan KM thi thuc hien tru tien
                        //So tin nhan khuyen mai noi mang trong thang
                        int A = promotion.FromOfInternalSMS;
                        //So tin nhan da gui trong thang
                        int M = objVal.TotalReceivedSMS;
                        //So tin nhan can gui
                        int X = countSMS;
                        //Nếu M>=A, tính tiền X SMS
                        if (M >= A)
                        {
                            int countFeesSMS = X;
                            objVal.PriceSendSMS = (countFeesSMS * Convert.ToInt32(WebConfigurationManager.AppSettings["PriceSMSInternal"]));
                            //InternalSendSMS += countFeesSMS;
                            objVal.InternalSendSMS = countFeesSMS;
                            objVal.TotalReceivedSMS += countSMS;
                            objVal.TotalInternalSMS += countSMS;
                        }
                        else if (M + X > A) //Nếu A – M<X, tính tiền X-A+M (SMS)
                        {
                            int countFeesSMS = M + X - A;
                            objVal.PriceSendSMS = (countFeesSMS * Convert.ToInt32(WebConfigurationManager.AppSettings["PriceSMSInternal"]));
                            //InternalSendSMS += countFeesSMS;
                            objVal.InternalSendSMS = countFeesSMS;
                            objVal.TotalReceivedSMS += countSMS;
                            objVal.TotalInternalSMS += countSMS;
                        }
                        else // mien phi tin nhan
                        {
                            objVal.TotalReceivedSMS += countSMS;
                            objVal.TotalInternalSMS += countSMS;
                        }
                    }
                }
                else
                {
                    objVal.PriceSendSMS = (countSMS * Convert.ToInt32(WebConfigurationManager.AppSettings["PriceSMSInternal"]));
                    objVal.InternalSendSMS = countSMS;
                    objVal.TotalReceivedSMS += countSMS;
                    objVal.TotalInternalSMS += countSMS;
                }
            }
            else if (promotion != null)//Tin nhan ngoai mang
            {
                int priceExternal = Convert.ToInt32(WebConfigurationManager.AppSettings["PriceSMSExternal"]);
                //Tong so tin nhan da nhan
                int M = objVal.TotalReceivedSMS;
                int N = objVal.TotalExternalSMS;
                //Tong so tin nhan KM
                int A = promotion.FromOfInternalSMS;
                //Tong 
                int B = promotion.FromOfExternalSMS;
                int X = countSMS;
                if (promotion.IsFreeExSMS.HasValue && promotion.IsFreeExSMS.Value)//TH dang ky mien phi tin nhan ngoai mang
                {
                    objVal.TotalReceivedSMS += countSMS;
                    objVal.TotalExternalSMS = N + countSMS;
                }
                else
                {
                    //-	Nếu M >=A, hoặc N>=B: tính tiền X SMS
                    if (M >= A || N >= B)
                    {
                        //// Dinh muc hang thang cua thue bao ngoai mang
                        int countFeesSMS = X;
                        objVal.PriceSendSMS = (countFeesSMS * priceExternal);
                        objVal.ExternalSendSMS = countFeesSMS;
                        objVal.TotalReceivedSMS += countSMS;
                        objVal.TotalExternalSMS = N + countSMS;

                    }
                    else if (A - M < X && B - N < X)
                    {
                        //-Nếu A –M < X và B-N < X, tính tiền MAX(X - A + M, X - B + N) SMS
                        int countFeesSMS = Math.Max(X + M - A, X + N - B);
                        objVal.PriceSendSMS = (countFeesSMS * priceExternal);
                        objVal.ExternalSendSMS = countFeesSMS;
                        objVal.TotalReceivedSMS += countSMS;
                        objVal.TotalExternalSMS = N + countSMS;

                    }
                    else if (A - M >= X && B - N < X)
                    {
                        //-	Nếu A – M > =X và B-N <X, tính tiền  X-B+N SMS
                        int countFeesSMS = X + N - B;
                        objVal.PriceSendSMS = (countFeesSMS * priceExternal);
                        objVal.ExternalSendSMS = countFeesSMS;
                        objVal.TotalReceivedSMS += countSMS;
                        objVal.TotalExternalSMS = N + countSMS;
                    }
                    else if (A - M < X && B - N < X)
                    {
                        //-	Nếu A –M<X và B- N>=X, tính tiền X -A+M SMS
                        int countFeesSMS = X + M - A;
                        objVal.PriceSendSMS = (countFeesSMS * priceExternal);
                        objVal.ExternalSendSMS = countFeesSMS;
                        objVal.TotalReceivedSMS += countSMS;
                        objVal.TotalExternalSMS = N + countSMS;
                    }
                    else // huong khuyen mai 0d ngoai mang
                    {
                        objVal.TotalReceivedSMS += countSMS;
                        objVal.TotalExternalSMS = N + countSMS;
                        //priceSendSMS = 0;
                        //ExternalSendSMS += countSMS;
                    }
                }
            }
            else
            {
                objVal.TotalReceivedSMS += countSMS;
                objVal.TotalExternalSMS += countSMS;
                objVal.PriceSendSMS = (countSMS * Convert.ToInt32(WebConfigurationManager.AppSettings["PriceSMSExternal"]));
                objVal.ExternalSendSMS = countSMS;
            }

            return objVal;
        }
        private int SetOrderPrimary(bool isPrimary, string title)
        {
            int orderID = 0;
            if (isPrimary)
            {
                if (title.Contains("ĐTX1") || title.Contains("ĐTX6"))
                {
                    orderID = 1;
                }

                if (title.Contains("ĐTX2") || title.Contains("ĐTX7"))
                {
                    orderID = 2;
                }

                if (title.Contains("ĐTX3"))
                {
                    orderID = 3;
                }

                if (title.Contains("GK"))
                {
                    orderID = 4;
                }

                if (title.Contains("ĐTX4") || title.Contains("ĐTX8"))
                {
                    orderID = 5;
                }

                if (title.Contains("ĐTX5") || title.Contains("ĐTX9"))
                {
                    orderID = 5;
                }

                if (title.Contains("CK"))
                {
                    orderID = 6;
                }
            }

            return orderID;
        }
        private int SetMonthWithPrimary(int month)
        {
            int Item = 0;
            if (month == 1)
            {
                Item = 5;
            }
            else if (month == 2)
            {
                Item = 6;
            }
            else if (month == 3)
            {
                Item = 7;
            }
            else if (month == 4)
            {
                Item = 8;
            }
            else if (month == 5)
            {
                Item = 9;
            }
            else if (month == 9)
            {
                Item = 1;
            }
            else if (month == 10)
            {
                Item = 2;
            }
            else if (month == 11)
            {
                Item = 3;
            }
            else if (month == 12)
            {
                Item = 4;
            }
            return Item;
        }
        private List<PupilContract> GetListPupilContract(int schoolID, int year, int semester, List<int> lstReceiverID)
        {
            SMSParentSendDetailSearchBO objSMSSendDetail = new SMSParentSendDetailSearchBO
            {
                SchoolId = schoolID,
                Year = year
            };

            IQueryable<SMS_PARENT_SENT_DETAIL> qSMSParentSendDetail = SMSParentSentDetailBusiness.Search(objSMSSendDetail);
            List<PupilContract> lstPupilContract = new List<PupilContract>();
            var pupilQuery = (from sc in SMSParentContractBusiness.All
                              join scd in SMSParentContractDetailBusiness.All on sc.SMS_PARENT_CONTRACT_ID equals scd.SMS_PARENT_CONTRACT_ID
                              join poc in PupilOfClassBusiness.All on sc.PUPIL_ID equals poc.PupilID
                              join pf in PupilProfileBusiness.All on sc.PUPIL_ID equals pf.PupilProfileID
                              join pdtl in ServicePackageDetailBusiness.All on scd.SERVICE_PACKAGE_ID equals pdtl.SERVICE_PACKAGE_ID
                              join spdd in ServicePackageDeclareBusiness.All on scd.SERVICE_PACKAGE_ID equals spdd.SERVICE_PACKAGE_ID
                              join sentDtl in qSMSParentSendDetail on scd.SMS_PARENT_CONTRACT_DETAIL_ID equals sentDtl.SMS_PARENT_CONTRACT_DETAIL_ID into gj
                              from x in gj.DefaultIfEmpty(null)
                              where sc.SCHOOL_ID == schoolID && sc.PARTITION_ID == schoolID % GlobalConstantsEdu.PARTITION_SMSPARENTCONTRACT_SCHOOLID
                              && (scd.SUBSCRIPTION_STATUS == GlobalConstantsEdu.STATUS_REGISTED || scd.SUBSCRIPTION_STATUS == GlobalConstantsEdu.STATUS_UNPAID) // Bổ sung gửi tin cho các thuê bao Nợ cước
                              && scd.IS_ACTIVE == true && sc.STATUS == GlobalConstantsEdu.SMS_PARENT_CONTRACT_STATUS_IS_ACCEPT
                              && (sc.IS_DELETED == null || sc.IS_DELETED == false)
                              && lstReceiverID.Contains(sc.PUPIL_ID)
                              &&
                              (
                                  (semester == 1 && (scd.SUBSCRIPTION_TIME == GlobalConstantsEdu.CONTRACTDETAIL_SUBSCRIPTIONTIME_FIRST_SMESTER || scd.SUBSCRIPTION_TIME == GlobalConstantsEdu.CONTRACTDETAIL_SUBSCRIPTIONTIME_ALLYEAR))
                                  ||
                                  (semester == 2 && (scd.SUBSCRIPTION_TIME == GlobalConstantsEdu.CONTRACTDETAIL_SUBSCRIPTIONTIME_SECOND_SMESTER || scd.SUBSCRIPTION_TIME == GlobalConstantsEdu.CONTRACTDETAIL_SUBSCRIPTIONTIME_ALLYEAR))
                              )
                              && scd.YEAR_ID == year
                              && spdd.YEAR == year
                              && spdd.STATUS == true
                              && poc.Year == year
                              && sc.CLASS_ID == poc.ClassID
                              && pf.IsActive
                              && poc.Status == GlobalConstants.PUPIL_STATUS_STUDYING
                              select new PupilContractDetail
                              {
                                  ClassID = sc.CLASS_ID,
                                  PupilFileID = sc.PUPIL_ID,
                                  Mobile = scd.SUBSCRIBER,
                                  SMSParentContactDetailID = scd.SMS_PARENT_CONTRACT_DETAIL_ID,
                                  MaxSMS = (pdtl.MAX_SMS.HasValue) ? scd.SUBSCRIPTION_TIME == 3 ? pdtl.MAX_SMS.Value * 2 : pdtl.MAX_SMS.Value : 0,
                                  SentSMS = ((x == null) ? 0 : x.SENT_TOTAL),
                                  SMSParentSentDetailID = ((x == null) ? 0 : x.SMS_PARENT_SENT_DETAIL_ID),
                                  ServicePackageID = pdtl.SERVICE_PACKAGE_ID,
                                  IsLimitPackage = spdd.IS_LIMIT,
                                  RegisterDate = scd.CREATED_TIME,
                                  Status = scd.SUBSCRIPTION_STATUS,
                              });
            //Xu ly lai query de lay ra tong so tin nhan cua tung thue bao dang ky.
            lstPupilContract.AddRange(pupilQuery
                .GroupBy(x => new {x.ClassID, x.PupilFileID, x.SMSParentContactDetailID, x.Mobile, x.SentSMS, x.SMSParentSentDetailID, x.ServicePackageID, x.RegisterDate, x.IsLimitPackage, x.Status })
                .Select(y => new PupilContract
                {
                    ClassID = y.Key.ClassID,
                    Mobile = y.Key.Mobile,
                    SMSParentContactDetailID = y.Key.SMSParentContactDetailID,
                    PupilFileID = y.Key.PupilFileID,
                    SentSMS = y.Key.SentSMS,
                    IsSent = false,
                    SMSParentSentDetailID = y.Key.SMSParentSentDetailID,
                    ServicePackageID = y.Key.ServicePackageID,
                    IsLimitPackage = y.Key.IsLimitPackage,
                    RegisterDate = y.Key.RegisterDate,
                    TotalSMS = y.Sum(a => a.MaxSMS),
                    Status = y.Key.Status
                }).ToList());

            return lstPupilContract;
        }
        private IDictionary<string, object> ColumnHistoryMappings()
        {
            Dictionary<string, object> columnMap = new Dictionary<string, object>();
            columnMap.Add("HISTORY_RAW_ID", "HISTORY_RAW_ID");
            columnMap.Add("MOBILE", "MOBILE");
            columnMap.Add("SMS_TYPE_ID", "SMS_TYPE_ID");
            columnMap.Add("RECEIVER_ID", "RECEIVER_ID");
            columnMap.Add("CREATE_DATE", "CREATE_DATE");
            columnMap.Add("SCHOOL_ID", "SCHOOL_ID");
            columnMap.Add("ACADEMIC_YEAR_ID", "ACADEMIC_YEAR_ID");
            columnMap.Add("YEAR", "YEAR");
            columnMap.Add("SMS_CONTENT", "SMS_CONTENT");
            columnMap.Add("STATUS", "STATUS");
            columnMap.Add("SCHOOL_USERNAME", "SCHOOL_USERNAME");
            columnMap.Add("SENDER_NAME", "SENDER_NAME");
            columnMap.Add("RECEIVER_NAME", "RECEIVER_NAME");
            columnMap.Add("FLAG_ID", "FLAG_ID");
            columnMap.Add("SMS_CNT", "SMS_CNT");
            columnMap.Add("SEND_TIME", "SEND_TIME");
            columnMap.Add("SHORT_CONTENT", "SHORT_CONTENT");
            columnMap.Add("RECEIVE_TYPE", "RECEIVE_TYPE");
            columnMap.Add("SMS_PARENTCONTRACT_DETAIL_ID", "SMS_PARENTCONTRACT_DETAIL_ID");
            columnMap.Add("IS_SMS", "IS_SMS");
            columnMap.Add("SENDER_UNIT_ID", "SENDER_UNIT_ID");
            columnMap.Add("RECEIVER_UNIT_NAME", "RECEIVER_UNIT_NAME");
            columnMap.Add("IS_AUTO", "IS_AUTO");
            columnMap.Add("SENDER_GROUP", "SENDER_GROUP");
            columnMap.Add("PARTITION_ID", "PARTITION_ID");
            columnMap.Add("SERVICE_ID", "SERVICE_ID");
            columnMap.Add("CONTENT_TYPE", "CONTENT_TYPE");
            columnMap.Add("IS_ON_SCHEDULE", "IS_ON_SCHEDULE");
            columnMap.Add("SMS_TIMER_CONFIG_ID", "SMS_TIMER_CONFIG_ID");
            columnMap.Add("IS_ADMIN", "IS_ADMIN");
            columnMap.Add("SENDER_ID", "SENDER_ID");
            columnMap.Add("CONTACT_GROUP_ID", "CONTACT_GROUP_ID");
            columnMap.Add("IS_CUSTOM_TYPE", "IS_CUSTOM_TYPE");
            columnMap.Add("SYNC_TIME", "SYNC_TIME");
            columnMap.Add("SYNC_CODE", "SYNC_CODE");
            columnMap.Add("IS_INTERNAL", "IS_INTERNAL");
            return columnMap;
        }
        private IDictionary<string, object> ColumnMTMappings()
        {
            Dictionary<string, object> columnMap = new Dictionary<string, object>();
            columnMap.Add("REQUEST_ID", "REQUEST_ID");
            columnMap.Add("USER_ID", "USER_ID");
            columnMap.Add("COMMAND_CODE", "COMMAND_CODE");
            columnMap.Add("TIME_SEND_REQUEST", "TIME_SEND_REQUEST");
            columnMap.Add("TIME_SENT", "TIME_SENT");
            columnMap.Add("SERVICE_ID", "SERVICE_ID");
            columnMap.Add("SMS_CONTENT", "SMS_CONTENT");
            columnMap.Add("CONTENT_TYPE", "CONTENT_TYPE");
            columnMap.Add("STATUS", "STATUS");
            columnMap.Add("RETRY_NUM", "RETRY_NUM");
            columnMap.Add("ISDN_TAIL", "ISDN_TAIL");
            columnMap.Add("MO_HIS_ID", "MO_HIS_ID");
            columnMap.Add("SUB_ID", "SUB_ID");
            columnMap.Add("MT_TYPE", "MT_TYPE");
            columnMap.Add("CP_CODE", "CP_CODE");
            columnMap.Add("BLOCK_ID", "BLOCK_ID");
            columnMap.Add("HISTORY_RAW_ID", "HISTORY_RAW_ID");
            columnMap.Add("SYNC_TIME", "SYNC_TIME");
            columnMap.Add("UNIT_ID", "UNIT_ID");
            columnMap.Add("SMS_TIMER_CONFIG_ID", "SMS_TIMER_CONFIG_ID");
            return columnMap;
        }
        private class PupilContract
        {
            public int ClassID { get; set; }
            public int PupilFileID { get; set; }

            public string Mobile { get; set; }

            public int TotalSMS { get; set; }

            public int SentSMS { get; set; }

            public long SMSParentContactDetailID { get; set; }

            public long SMSParentSentDetailID { get; set; }

            public bool? IsSent { get; set; }

            public int ServicePackageID { get; set; }

            public int Status { get; set; }

            public DateTime RegisterDate { get; set; }

            public bool? IsLimitPackage { get; set; }

            public int? EffectiveDay { get; set; }
            public int SemesterID { get; set; }
        }
        private class PupilContractDetail
        {
            public int ClassID { get; set; }
            public int PupilFileID { get; set; }
            public string Mobile { get; set; }
            public int MaxSMS { get; set; }
            public int SentSMS { get; set; }
            public long SMSParentContactDetailID { get; set; }
            public long SMSParentSentDetailID { get; set; }

            //public string PackageCode { get; set; }

            public DateTime RegisterDate { get; set; }
            public int ServicePackageID { get; set; }
            public bool? IsLimitPackage { get; set; }
            public int? EffectiveDay { get; set; }
            public int Status { get; set; }
            public int SemesterID { get; set; }
        }
        private class PupilContent
        {
            public int PupilFileID { get; set; }

            public string Content { get; set; }

            public string ShortContent { get; set; }

            public int SMS_CNT { get; set; }
        }
        private class PupilSubscriber
        {
            public string Subscriber { get; set; }

            public long SMSParentContractDetailID { get; set; }
        }
        private class PupilRegister
        {
            public string Subscriber { get; set; }

            public int PupilProfileID { get; set; }
        }
        private class StatisticsOfSentSMSToPupil
        {
            public string ContractorName { get; set; }
            public long SMSParentContractID { get; set; }
            public int PupilID { get; set; }
            public int TotalActive { get; set; }
            public int TotalManual { get; set; }
            public string ReceiverPhoneNumber { get; set; }
        }
        private class Receiver
        {
            public int SchoolID { get; set; }
            public int PupilID { get; set; }
            public long SMSParentContractID { get; set; }
            public int year { get; set; }
            public string PhoneNumber { get; set; }
        }
    }
}
