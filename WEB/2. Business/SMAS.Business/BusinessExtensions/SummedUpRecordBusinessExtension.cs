﻿/**
 * The Viettel License
 *
 * Copyright 2012 Viettel Telecom. All rights reserved.
 * VIETTEL PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */

/**
* @author  tungnd
* @version $Revision: $
*/

using System;
using System.Collections.Generic;
using System.Linq;
using SMAS.Business.BusinessObject;
using SMAS.Business.Common;
using SMAS.Models.Models;
using Newtonsoft.Json;
using SMAS.DAL.IRepository;
using SMAS.DAL.Repository;

namespace SMAS.Business.Business
{
    public partial class SummedUpRecordBusiness
    {
        private const int PUPIL_STATUS_STUDYING = 1;

        #region Validate

        public void Validate(SummedUpRecord summeduprecord)
        {
            ValidationMetadata.ValidateObject(summeduprecord);

            //  SubjectID và ClassID not compatible trong ClassSubject
            IDictionary<string, object> SearchInfoClassSubject = new Dictionary<string, object>();
            SearchInfoClassSubject["SubjectID"] = summeduprecord.SubjectID;
            SearchInfoClassSubject["ClassID"] = summeduprecord.ClassID;

            bool ClassSubjectExist = new ClassSubjectRepository(this.context).ExistsRow(GlobalConstants.SCHOOL_SCHEMA, "ClassSubject", SearchInfoClassSubject, null);

            if (!ClassSubjectExist)
            {
                throw new BusinessException("Common_Validate_NotCompatible");
            }

            //  PupilID và ClassID not compatible trong PupilProfile
            IDictionary<string, object> SearchInfoPupil = new Dictionary<string, object>();
            SearchInfoPupil["PupilProfileID"] = summeduprecord.PupilID;
            SearchInfoPupil["CurrentClassID"] = summeduprecord.ClassID;

            bool PupilProfileExist = new PupilProfileRepository(this.context).ExistsRow(GlobalConstants.PUPIL_SCHEMA, "PupilProfile", SearchInfoPupil, null);

            if (!PupilProfileExist)
            {
                throw new BusinessException("Common_Validate_NotCompatible");
            }

            //  SchoolID và AcademicYearID not compatible trong AcademicYear
            IDictionary<string, object> SearchInfoAcademicYear = new Dictionary<string, object>();
            SearchInfoAcademicYear["SchoolID"] = summeduprecord.SchoolID;
            SearchInfoAcademicYear["AcademicYearID"] = summeduprecord.AcademicYearID;

            bool AcademicYearExist = new AcademicYearRepository(this.context).ExistsRow(GlobalConstants.SCHOOL_SCHEMA, "AcademicYear", SearchInfoAcademicYear, null);

            if (!AcademicYearExist)
            {
                throw new BusinessException("Common_Validate_NotCompatible");
            }

            //  PupilID và AcademicYearID  not compatible trong PupilProfile
            IDictionary<string, object> SearchInfo = new Dictionary<string, object>();
            SearchInfo["PupilProfileID"] = summeduprecord.PupilID;
            SearchInfo["CurrentAcademicYearID"] = summeduprecord.AcademicYearID;

            bool SearchInfoExist = new PupilProfileRepository(this.context).ExistsRow(GlobalConstants.PUPIL_SCHEMA, "PupilProfile", SearchInfo, null);

            if (!SearchInfoExist)
            {
                throw new BusinessException("Common_Validate_NotCompatible");
            }

            // kiểm tra năm học đã tồn tại

            if (!AcademicYearBusiness.IsCurrentYear(summeduprecord.AcademicYearID))
            {
                throw new BusinessException("Common_Validate_NotIsCurrentYear");
            }

            // kiểm tra giá trị của ProfileStatus trong bảng PupilOfClass có bàng giá trị của PUPIL_STATUS_STUDYING không
            //Kiểm tra PupilOfClassBusiness.SearchBySchool(SchoolID, Dictionary) với Dictionary[“PupilID”] = PupilID, Dictionary[“ClassID”] = ClassID >Status
            IDictionary<string, object> Dictionary = new Dictionary<string, object>();
            Dictionary["PupilID"] = summeduprecord.PupilID;
            Dictionary["ClassID"] = summeduprecord.ClassID;
            PupilOfClass pupilOfClass = PupilOfClassBusiness.SearchBySchool(summeduprecord.SchoolID, Dictionary).OrderByDescending(u => u.AssignedDate).FirstOrDefault();
            if (pupilOfClass.Status != GlobalConstants.PUPIL_STATUS_STUDYING)
            {
                throw new BusinessException("Common_Validate_NotIsCurrentStatus");
            }
        }

        public void ValidateRetest(SummedUpRecord summeduprecord)
        {
            ValidationMetadata.ValidateObject(summeduprecord);
        }
        #endregion Validate

        #region Insert

        /// <summary>
        /// Lưu điểm trung bình cả năm theo học kỳ
        /// </summary>
        /// <author>tungnd</author>
        /// <date>9/10/2012</date>
        /// <param name="ethnic">Đối tượng SummedUpRecord</param>
        /// <returns>Đối tượng SummedUpRecord</returns>
        public void InsertSummedUpRecord(List<SummedUpRecord> lstSummedUpRecord)
        {
            if (lstSummedUpRecord.Count == 0)
            {
                return;
            }

            //Lấy từng phần tử trong lstMarkRecordOfPrimary
            foreach (SummedUpRecord SummedUpRecord in lstSummedUpRecord)
            {
                Validate(SummedUpRecord);
            }

            foreach (SummedUpRecord SummedUpRecord in lstSummedUpRecord)
            {
                SummedUpRecord.SummedUpDate = DateTime.Now;
                this.Insert(SummedUpRecord);
            }
        }


        public void InsertSummedUpRecordRetest(List<SummedUpRecord> lstSummedUpRecord)
        {
            if (lstSummedUpRecord.Count == 0)
            {
                return;
            }
            #region Kiem tra du lieu dau vao
            SummedUpRecord first = lstSummedUpRecord.First();
            int schoolID = first.SchoolID;
            int academicYearID = first.AcademicYearID;
            AcademicYear aca = AcademicYearBusiness.Find(academicYearID);

            // Kiem tra nam hoc co thuoc truong
            //  SubjectID và ClassID not compatible trong ClassSubject
            var listClassSubjectID = lstSummedUpRecord.Select(o => new { o.ClassID, o.SubjectID }).Distinct().ToList();
            foreach (var cs in listClassSubjectID)
            {
                IDictionary<string, object> SearchInfoClassSubject = new Dictionary<string, object>();
                SearchInfoClassSubject["SubjectID"] = cs.SubjectID;
                SearchInfoClassSubject["ClassID"] = cs.ClassID;

                bool ClassSubjectExist = new ClassSubjectRepository(this.context).ExistsRow(GlobalConstants.SCHOOL_SCHEMA, "ClassSubject", SearchInfoClassSubject, null);

                if (!ClassSubjectExist)
                {
                    throw new BusinessException("Common_Validate_NotCompatible");
                }
            }


            // Kiem tra nam hoc co thuoc truong
            if (aca.SchoolID != schoolID)
            {
                throw new BusinessException("Common_Validate_NotCompatible");
            }

            // kiểm tra giá trị của ProfileStatus trong bảng PupilOfClass có bàng giá trị của PUPIL_STATUS_STUDYING không
            List<int> listPupilID = lstSummedUpRecord.Select(o => o.PupilID).Distinct().ToList();
            int countPupilStudying = PupilOfClassBusiness.AllNoTracking.Where(o => o.SchoolID == schoolID
                && o.AcademicYearID == academicYearID && o.Status == SystemParamsInFile.PUPIL_STATUS_STUDYING
                && listPupilID.Contains(o.PupilID)).Count();

            if (listPupilID.Count != countPupilStudying)
            {
                throw new BusinessException("Common_Validate_NotIsCurrentStatus");
            }

            //Lấy từng phần tử trong lstSummedUpRecord
            foreach (SummedUpRecord SummedUpRecord in lstSummedUpRecord)
            {
                ValidateRetest(SummedUpRecord);
            }
            #endregion

            this.context.Configuration.AutoDetectChangesEnabled = false;
            this.context.Configuration.ValidateOnSaveEnabled = false;
            foreach (SummedUpRecord SummedUpRecord in lstSummedUpRecord)
            {
                SummedUpRecord.SummedUpDate = DateTime.Now;
                SummedUpRecord.CreatedAcademicYear = aca.Year;
                this.Insert(SummedUpRecord);
            }
            this.context.Configuration.AutoDetectChangesEnabled = true;
            this.context.Configuration.ValidateOnSaveEnabled = true;
        }
        #endregion Insert

        #region Update

        /// <summary>
        /// Lưu điểm trung bình cả năm theo học kỳ
        /// </summary>
        /// <author>tungnd</author>
        /// <date>9/10/2012</date>
        /// <param name="ethnic">Đối tượng SummedUpRecord</param>
        /// <returns>Đối tượng SummedUpRecord</returns>

        public void UpdateSummedUpRecord(List<SummedUpRecord> lstSummedUpRecord)
        {
            foreach (var item in lstSummedUpRecord)
            {
                Validate(item);
            }

            foreach (var item in lstSummedUpRecord)
            {
                item.SummedUpDate = DateTime.Now;
                base.Update(item);
            }
        }

        /// <summary>
        /// 03/04/2014
        /// QuangLM
        /// Cap nhat du lieu thi lai
        /// </summary>
        /// <param name="lstSummedUpRecord"></param>
        public void UpdateSummedUpRecordRetest(List<SummedUpRecord> lstSummedUpRecord)
        {
            if (lstSummedUpRecord.Count == 0)
            {
                return;
            }
            #region Kiem tra du lieu dau vao
            SummedUpRecord first = lstSummedUpRecord.First();
            int schoolID = first.SchoolID;
            int academicYearID = first.AcademicYearID;
            AcademicYear aca = AcademicYearBusiness.Find(academicYearID);

            // Kiem tra nam hoc co thuoc truong
            //  SubjectID và ClassID not compatible trong ClassSubject
            var listClassSubjectID = lstSummedUpRecord.Select(o => new { o.ClassID, o.SubjectID }).Distinct().ToList();
            foreach (var cs in listClassSubjectID)
            {
                IDictionary<string, object> SearchInfoClassSubject = new Dictionary<string, object>();
                SearchInfoClassSubject["SubjectID"] = cs.SubjectID;
                SearchInfoClassSubject["ClassID"] = cs.ClassID;

                bool ClassSubjectExist = new ClassSubjectRepository(this.context).ExistsRow(GlobalConstants.SCHOOL_SCHEMA, "ClassSubject", SearchInfoClassSubject, null);

                if (!ClassSubjectExist)
                {
                    throw new BusinessException("Common_Validate_NotCompatible");
                }
            }


            // Kiem tra nam hoc co thuoc truong
            if (aca.SchoolID != schoolID)
            {
                throw new BusinessException("Common_Validate_NotCompatible");
            }

            // kiểm tra giá trị của ProfileStatus trong bảng PupilOfClass có bàng giá trị của PUPIL_STATUS_STUDYING không
            List<int> listPupilID = lstSummedUpRecord.Select(o => o.PupilID).Distinct().ToList();
            int countPupilStudying = PupilOfClassBusiness.AllNoTracking.Where(o => o.SchoolID == schoolID
                && o.AcademicYearID == academicYearID && o.Status == SystemParamsInFile.PUPIL_STATUS_STUDYING
                && listPupilID.Contains(o.PupilID)).Count();

            if (listPupilID.Count != countPupilStudying)
            {
                throw new BusinessException("Common_Validate_NotIsCurrentStatus");
            }

            //Lấy từng phần tử trong lstSummedUpRecord
            foreach (SummedUpRecord SummedUpRecord in lstSummedUpRecord)
            {
                ValidateRetest(SummedUpRecord);
            }
            #endregion

            this.context.Configuration.AutoDetectChangesEnabled = false;
            this.context.Configuration.ValidateOnSaveEnabled = false;
            foreach (var item in lstSummedUpRecord)
            {
                item.CreatedAcademicYear = aca.Year;
                item.SummedUpDate = DateTime.Now;
                base.Update(item);
            }
            this.context.Configuration.AutoDetectChangesEnabled = true;
            this.context.Configuration.ValidateOnSaveEnabled = true;
        }

        #endregion Update

        #region Delete

        /// <summary>
        /// Xoá thông tin tổng kết kết quả môn học chấm điểm và đánh giá theo giai đoạn / học kỳ / năm học
        /// </summary>
        /// <author>tungnd</author>
        /// <date>9/10/2012</date>
        /// <param name="ethnic">Đối tượng SummedUpRecord</param>

        public void DeleteSummedUpRecord(int UserID, long SummedUpRecordID, int? SchoolID)
        {
            //SummedUpRecordID, SchoolID: not compatible(SummedUpRecord)
            bool SearchInfo = new SummedUpRecordRepository(this.context).ExistsRow(GlobalConstants.PUPIL_SCHEMA, "SummedUpRecord",
                    new Dictionary<string, object>()
                {
                    {"SummedUpRecordID",SummedUpRecordID},
                    {"SchoolID",SchoolID}
                }, null);
            if (!SearchInfo)
            {
                throw new BusinessException("Common_Validate_NotCompatible");
            }

            SummedUpRecord summeduprecord = SummedUpRecordRepository.Find(SummedUpRecordID);
            if (summeduprecord != null)
            {

                if (!AcademicYearBusiness.IsCurrentYear(summeduprecord.AcademicYearID))
                {
                    throw new BusinessException("Common_Validate_NotIsCurrentYear");
                }
                if (summeduprecord.SubjectCat.IsApprenticeshipSubject != true)
                {
                    PupilOfClass pupilOfClass = PupilOfClassBusiness.SearchBySchool(SchoolID.Value, new Dictionary<string, object>()
                                                                                                            {
                                                                                                                {"PupilID",summeduprecord.PupilID},
                                                                                                                {"ClassID",summeduprecord.ClassID},
                                                                                                                {"AcademicYearID",summeduprecord.AcademicYearID},
                                                                                                            }).OrderByDescending(u => u.AssignedDate).FirstOrDefault();
                    if (pupilOfClass.Status != PUPIL_STATUS_STUDYING)
                    {
                        throw new BusinessException("Common_Validate_NotIsCurrentStatus");
                    }
                    //Hungnd 17/04/2013 Fix 0167578 + 0167606
                    if (!UtilsBusiness.HasSubjectTeacherPermission(UserID, summeduprecord.ClassID, summeduprecord.SubjectID))
                    {
                        throw new BusinessException("Common_Error_HasHadTeacherPermission");
                    }
                }

                //Tìm điểm cả năm
                IDictionary<string, object> dic = new Dictionary<string, object>();
                dic["PupilID"] = summeduprecord.PupilID;
                dic["AcademicYearID"] = summeduprecord.AcademicYearID;
                dic["SubjectID"] = summeduprecord.SubjectID;
                dic["Semester"] = SystemParamsInFile.SEMESTER_OF_YEAR_ALL;
                SummedUpRecord sumCN = SearchBySchool(summeduprecord.SchoolID, dic).Where(o => o.PeriodID == null).FirstOrDefault();
                base.Delete(SummedUpRecordID);
                if (sumCN != null)
                {
                    base.Delete(sumCN.SummedUpRecordID);
                }

            }
        }


        /// <summary>
        /// 
        /// Chiendd1: 19.09.2017.
        /// Xoa diem TBM. Return lai danh sach diem bi xoa de phuc vu phuc hoi du lieu
        /// </summary>
        /// <param name="UserID"></param>
        /// <param name="SchoolID"></param>
        /// <param name="AcademicYearID"></param>
        /// <param name="ClassID"></param>
        /// <param name="Semester"></param>
        /// <param name="PeriodID"></param>
        /// <param name="SubjectID"></param>
        /// <param name="listPupilID"></param>
        /// <returns></returns>
        public List<SummedUpRecord> DeleteSummedUpRecord(int UserID, int SchoolID, int AcademicYearID, int ClassID, int Semester, int? PeriodID, int SubjectID, List<int> listPupilID)
        {

            List<int> lsPupilId = listPupilID.Distinct().ToList();
            if (!AcademicYearBusiness.IsCurrentYear(AcademicYearID))
            {
                throw new BusinessException("Common_Validate_NotIsCurrentYear");
            }
            List<SummedUpRecord> lstResSummed = new List<SummedUpRecord>();
            SubjectCat subjectCat = SubjectCatBusiness.Find(SubjectID);

            if (!subjectCat.IsApprenticeshipSubject && !UtilsBusiness.HasSubjectTeacherPermission(UserID, ClassID, SubjectID))
                throw new BusinessException("Common_Label_HasSubjectTeacherPermission");

            if (subjectCat.IsApprenticeshipSubject != true)
            {
                IDictionary<string, object> SearchPOC = new Dictionary<string, object>();
                SearchPOC["ClassID"] = ClassID;
                SearchPOC["Check"] = "Check";
                List<PupilOfClass> lstPOC = PupilOfClassBusiness.SearchBySchool(SchoolID, SearchPOC).ToList();

                if (lstPOC.Any(u => listPupilID.Contains(u.PupilID) && u.Status != SystemParamsInFile.PUPIL_STATUS_STUDYING))
                    throw new BusinessException("Common_Validate_Pupil_Status_Studying");
            }

            //Tìm điểm theo hoc ky
            IDictionary<string, object> dic = new Dictionary<string, object>();
            dic["AcademicYearID"] = AcademicYearID;
            dic["SubjectID"] = SubjectID;
            dic["Semester"] = Semester;
            dic["PeriodID"] = PeriodID;
            dic["ClassID"] = ClassID;
            dic["ListPupilID"] = listPupilID;
            List<SummedUpRecord> listSummedUp = SearchBySchool(SchoolID, dic).ToList();
            
            //Diem ca nam
            IDictionary<string, object> dicIII = new Dictionary<string, object>();
            dic["AcademicYearID"] = AcademicYearID;
            dic["SubjectID"] = SubjectID;
            dic["ClassID"] = ClassID;
            dic["ListPupilID"] = listPupilID;
            dic["Semester"] = SystemParamsInFile.SEMESTER_OF_YEAR_ALL;
            List<SummedUpRecord> sumCN = SearchBySchool(SchoolID, dic).ToList();


            //Xoa diem hoc ky
            List<int> listPupilIDSummedUpRecord = listSummedUp.Select(p => p.PupilID).Distinct().ToList();
            MarkRecordBusiness.SP_DeleteSummedUpRecord(0, listPupilIDSummedUpRecord, PeriodID.HasValue ? PeriodID.Value : 0, SubjectID, ClassID, Semester, SchoolID, AcademicYearID);// khong phai historyId = 0, period = 0
            if (listSummedUp != null)
            {
                lstResSummed.AddRange(listSummedUp);
            }
            dic = new Dictionary<string, object>();
            dic["AcademicYearID"] = AcademicYearID;
            dic["SchoolID"] = SchoolID;
            dic["SubjectID"] = SubjectID;
            ClassSubject classSubjectObj = ClassSubjectBusiness.SearchByClass(ClassID, dic).FirstOrDefault();
            List<RESTORE_DATA_DETAIL> lstRestoreDeltail = new List<RESTORE_DATA_DETAIL>();
            //Tìm điểm cả năm nếu đợt không có
            if (!PeriodID.HasValue && (Semester == SystemParamsInFile.SEMESTER_OF_YEAR_SECOND || classSubjectObj.SectionPerWeekSecondSemester == 0))
            {                
                //Xoa diem hoc ky
                List<int> listPupilIDSummedUpRecordPeriod = sumCN.Select(p => p.PupilID).Distinct().ToList();
                MarkRecordBusiness.SP_DeleteSummedUpRecord(0, listPupilIDSummedUpRecordPeriod, PeriodID.HasValue ? PeriodID.Value : 0, SubjectID, ClassID, Semester, SchoolID, AcademicYearID);// khong phai historyId = 0, period = 0
                if(sumCN!=null && sumCN.Count > 0)
                {
                    lstResSummed.AddRange(sumCN);
                }
            }

            //xoa voi mon mien giam
            Dictionary<string, object> search = new Dictionary<string, object>();
            search["ClassID"] = ClassID;
            search["SubjectID"] = SubjectID;
            search["Semester"] = Semester;
            search["PeriodID"] = PeriodID;
            List<int> listExemptedSubjectII = ExemptedSubjectBusiness.GetListExemptedSubject(ClassID, SystemParamsInFile.SEMESTER_OF_YEAR_SECOND).Select(u => u.PupilID).ToList();

            listPupilID = listPupilID.Where(u => listExemptedSubjectII.Contains(u)).ToList();

            if (listPupilID != null && listPupilID.Count() > 0)
            {
                IDictionary<string, object> dicMG = new Dictionary<string, object>();
                dic["AcademicYearID"] = AcademicYearID;
                dic["SubjectID"] = SubjectID;
                dic["ClassID"] = ClassID;
                //xoa diem voi hoc sinh bi mien giam hoc ky II
                dic["ListPupilID"] = listPupilID;
                dic["Semester"] = SystemParamsInFile.SEMESTER_OF_YEAR_ALL;
                List<SummedUpRecord> sumCN_MG = SearchBySchool(SchoolID, dicMG).ToList();
                //base.DeleteAll(sumCN);
                //Xoa diem hoc ky
                List<int> listPupilIDSummedRecordPeriod = sumCN_MG.Select(p => p.PupilID).Distinct().ToList();
                MarkRecordBusiness.SP_DeleteSummedUpRecord(0, listPupilIDSummedRecordPeriod, PeriodID.HasValue ? PeriodID.Value : 0, SubjectID, ClassID, Semester, SchoolID, AcademicYearID);// khong phai historyId = 0, period = 0
                if (sumCN != null && sumCN.Count > 0)
                {
                    lstResSummed.AddRange(sumCN_MG);
                }

            }

            //Sau khi xoa tinh lai diem TBM
            string strMarkTitle = PeriodID.HasValue ? MarkRecordBusiness.GetMarkTitle(PeriodID.Value) : MarkRecordBusiness.GetMarkSemesterTitle(SchoolID, AcademicYearID, Semester);
            //MarkRecordBusiness.SummedUpMark(lsPupilId, SchoolID, AcademicYearID, Semester, PeriodID, strMarkTitle, classSubjectObj);
            
             MarkRecordBusiness.SummedMarkAllPeriodAndSemester(lsPupilId, SchoolID, AcademicYearID, Semester, classSubjectObj,strMarkTitle,PeriodID);
            
            


            return lstResSummed;
        }

        public void DeleteSummedUpRecordPrimary(int UserID, int SchoolID, int AcademicYearID, int ClassID, int Semester, int? PeriodID, int SubjectID, List<int> listPupilID)
        {
            if (!AcademicYearBusiness.IsCurrentYear(AcademicYearID))
            {
                throw new BusinessException("Common_Validate_NotIsCurrentYear");
            }

            SubjectCat subjectCat = SubjectCatBusiness.Find(SubjectID);

            if (!subjectCat.IsApprenticeshipSubject && !UtilsBusiness.HasSubjectTeacherPermission(UserID, ClassID, SubjectID))
                throw new BusinessException("Common_Label_HasSubjectTeacherPermission");

            if (subjectCat.IsApprenticeshipSubject != true)
            {
                IDictionary<string, object> SearchPOC = new Dictionary<string, object>();
                SearchPOC["ClassID"] = ClassID;
                SearchPOC["Check"] = "Check";
                List<PupilOfClass> lstPOC = PupilOfClassBusiness.SearchBySchool(SchoolID, SearchPOC).ToList();

                if (lstPOC.Any(u => listPupilID.Contains(u.PupilID) && u.Status != SystemParamsInFile.PUPIL_STATUS_STUDYING))
                    throw new BusinessException("Common_Validate_Pupil_Status_Studying");
            }

            //Tìm điểm theo hoc ky
            IDictionary<string, object> dic = new Dictionary<string, object>();
            dic["AcademicYearID"] = AcademicYearID;
            dic["SubjectID"] = SubjectID;
            dic["Semester"] = Semester;
            dic["PeriodID"] = PeriodID;
            dic["ListPupilID"] = listPupilID;
            List<SummedUpRecord> listSummedUp = SearchBySchoolPrimary(SchoolID, dic).ToList();
            base.DeleteAll(listSummedUp);

            dic = new Dictionary<string, object>();
            dic["AcademicYearID"] = AcademicYearID;
            dic["SchoolID"] = SchoolID;
            dic["SubjectID"] = SubjectID;
            ClassSubject classSubjectList = ClassSubjectBusiness.SearchByClass(ClassID, dic).FirstOrDefault();

            //Tìm điểm cả năm nếu đợt không có
            if (!PeriodID.HasValue && (Semester == SystemParamsInFile.SEMESTER_OF_YEAR_SECOND || classSubjectList.SectionPerWeekSecondSemester == 0))
            {
                IDictionary<string, object> dicIII = new Dictionary<string, object>();
                dic["AcademicYearID"] = AcademicYearID;
                dic["SubjectID"] = SubjectID;
                dic["ClassID"] = ClassID;
                dic["ListPupilID"] = listPupilID;
                dic["Semester"] = SystemParamsInFile.SEMESTER_OF_YEAR_ALL;
                List<SummedUpRecord> sumCN = SearchBySchoolPrimary(SchoolID, dic).ToList();
                base.DeleteAll(sumCN);
            }

            //xoa voi mon mien giam
            Dictionary<string, object> search = new Dictionary<string, object>();
            search["ClassID"] = ClassID;
            search["SubjectID"] = SubjectID;
            search["Semester"] = Semester;
            search["PeriodID"] = PeriodID;
            List<int> listExemptedSubjectII = ExemptedSubjectBusiness.GetListExemptedSubject(ClassID, SystemParamsInFile.SEMESTER_OF_YEAR_SECOND).Select(u => u.PupilID).ToList();

            listPupilID = listPupilID.Where(u => listExemptedSubjectII.Contains(u)).ToList();

            if (listPupilID != null && listPupilID.Count() > 0)
            {
                IDictionary<string, object> dicIII = new Dictionary<string, object>();
                dic["AcademicYearID"] = AcademicYearID;
                dic["SubjectID"] = SubjectID;
                dic["ClassID"] = ClassID;
                //xoa diem voi hoc sinh bi mien giam hoc ky II
                dic["ListPupilID"] = listPupilID;
                dic["Semester"] = SystemParamsInFile.SEMESTER_OF_YEAR_ALL;
                List<SummedUpRecord> sumCN = SearchBySchoolPrimary(SchoolID, dic).ToList();
                base.DeleteAll(sumCN);
            }
        }

        #endregion Delete

        #region Search

        /// <summary>
        /// Tìm kiếm thông tin tổng kết kết quả môn học chấm điểm và đánh giá theo giai đoạn / học kỳ / năm học
        /// </summary>
        /// <author>tungnd</author>
        /// <date>9/10/2012</date>
        /// <param name="ethnic">Đối tượng SummedUpRecord</param>
        /// <returns>Đối tượng SummedUpRecord</returns>

        private IQueryable<SummedUpRecord> Search(IDictionary<string, object> dic)
        {
            int SummedUpRecordID = Utils.GetInt(dic, "SummedUpRecordID");

            int PupilID = Utils.GetInt(dic, "PupilID");

            int ClassID = Utils.GetInt(dic, "ClassID");

            int? SchoolID = Utils.GetInt(dic, "SchoolID");

            int AcademicYearID = Utils.GetInt(dic, "AcademicYearID");

            int SubjectID = Utils.GetInt(dic, "SubjectID");

            int AppliedLevel = Utils.GetInt(dic, "AppliedLevel");

            int EducationLevelID = Utils.GetInt(dic, "EducationLevelID");


            //  int IsCommenting = Utils.GetByte(dic,"IsCommenting");

            //int IsCommenting = Utils.GetInt(dic, "IsCommenting");

            int? Year = Utils.GetInt(dic, "Year");

            //  public Nullable<int> Semester { get; set; }
            int Semester = Utils.GetInt(dic, "Semester", -1);

            int? PeriodID = Utils.GetNullableInt(dic, "PeriodID");

            double SummedUpMark = Utils.GetDouble(dic, "SummedUpMark");

            string JudgementResult = Utils.GetString(dic, "JudgementResult");

            string Comment = Utils.GetString(dic, "Comment");

            DateTime? SummedUpDate = Utils.GetDateTime(dic, "SummedUpDate");

            double? SummedUpFromMark = Utils.GetNullableDouble(dic, "SummedUpFromMark");

            double? SummedUpToMark = Utils.GetNullableDouble(dic, "SummedUpToMark");
            List<int> lstPupilID = Utils.GetIntList(dic, "ListPupilID");
            string checkWithClassMovement = Utils.GetString(dic, "checkWithClassMovement");
            string NoCheckIsActivePupilSchool = Utils.GetString(dic, "NoCheckIsActivePupilSchool");

            IQueryable<SummedUpRecord> lsSummedUpRecord = this.SummedUpRecordRepository.All;
            if (ClassID != 0)
            {
                lsSummedUpRecord = from sur in lsSummedUpRecord                                   
                                   where sur.ClassID == ClassID
                                   select sur;
            }
            /*if (string.IsNullOrWhiteSpace(NoCheckIsActivePupilSchool))
            {
                lsSummedUpRecord = lsSummedUpRecord.Where(o => o.PupilProfile.IsActive == true && o.SchoolProfile.IsActive == true);
            }*/
            //End Bổ sung tìm kiếm theo IsActive
            // bắt đầu tìm kiếm
            if (SummedUpRecordID != 0)
            {
                lsSummedUpRecord = lsSummedUpRecord.Where(su => (su.SummedUpRecordID == SummedUpRecordID));
            }
            if (PupilID != 0)
            {
                lsSummedUpRecord = lsSummedUpRecord.Where(su => (su.PupilID == PupilID));
            }
            if (SchoolID != 0)
            {
                int partitionId = UtilsBusiness.GetPartionId(SchoolID.Value);
                lsSummedUpRecord = lsSummedUpRecord.Where(su => (su.SchoolID == SchoolID));
                lsSummedUpRecord = lsSummedUpRecord.Where(su => su.Last2digitNumberSchool == partitionId);
            }
            if (AcademicYearID != 0)
            {
                lsSummedUpRecord = lsSummedUpRecord.Where(su => (su.AcademicYearID == AcademicYearID));
            }
            if (lstPupilID != null && lstPupilID.Count() > 0)
            {
                lsSummedUpRecord = lsSummedUpRecord.Where(o => (lstPupilID.Contains(o.PupilID)));
            }
            if (SubjectID != 0)
            {
                lsSummedUpRecord = lsSummedUpRecord.Where(su => (su.SubjectID == SubjectID));
            }

            if (AppliedLevel != 0)
            {
                lsSummedUpRecord = lsSummedUpRecord.Where(su => (su.ClassProfile.EducationLevel.Grade == AppliedLevel));
            }
            if (EducationLevelID != 0)
            {
                lsSummedUpRecord = lsSummedUpRecord.Where(su => (su.ClassProfile.EducationLevelID == EducationLevelID));
            }

            if (Year != 0)
            {
                lsSummedUpRecord = lsSummedUpRecord.Where(su => (su.CreatedAcademicYear == Year));
            }
            if (Semester != -1)
            {
                lsSummedUpRecord = lsSummedUpRecord.Where(su => su.Semester == Semester);// xoa diem xoa luon diem TBCN
            }
            if (!string.IsNullOrEmpty(checkWithClassMovement))
            {
                if (PeriodID == null)
                {
                    lsSummedUpRecord = lsSummedUpRecord.Where(su => su.PeriodID == null);

                }
                else
                {
                    lsSummedUpRecord = lsSummedUpRecord.Where(su => su.PeriodID == PeriodID.Value);
                }
            }
            if (PeriodID > 0)
            {
                lsSummedUpRecord = lsSummedUpRecord.Where(su => su.PeriodID == PeriodID);
            }
            else
            {
                lsSummedUpRecord = lsSummedUpRecord.Where(su => !su.PeriodID.HasValue);
            }
            if (SummedUpFromMark.HasValue)
            {
                lsSummedUpRecord = lsSummedUpRecord.Where(su => (su.SummedUpMark >= (decimal)SummedUpFromMark));
            }

            if (SummedUpToMark.HasValue)
            {
                lsSummedUpRecord = lsSummedUpRecord.Where(su => (su.SummedUpMark <= (decimal)SummedUpToMark));
            }
            if (JudgementResult.Length != 0)
            {
                lsSummedUpRecord = lsSummedUpRecord.Where(su => (su.JudgementResult == JudgementResult));
            }
            if (SummedUpDate != null)
            {
                lsSummedUpRecord = lsSummedUpRecord.Where(su => (su.SummedUpDate == SummedUpDate));
            }

            return lsSummedUpRecord;
        }

        private IQueryable<SummedUpRecord> SearchPrimary(IDictionary<string, object> dic)
        {
            int SummedUpRecordID = Utils.GetInt(dic, "SummedUpRecordID");

            int PupilID = Utils.GetInt(dic, "PupilID");

            int ClassID = Utils.GetInt(dic, "ClassID");

            int? SchoolID = Utils.GetInt(dic, "SchoolID");

            int AcademicYearID = Utils.GetInt(dic, "AcademicYearID");

            int SubjectID = Utils.GetInt(dic, "SubjectID");

            int AppliedLevel = Utils.GetInt(dic, "AppliedLevel");

            int EducationLevelID = Utils.GetInt(dic, "EducationLevelID");


            //  int IsCommenting = Utils.GetByte(dic,"IsCommenting");

            //int IsCommenting = Utils.GetInt(dic, "IsCommenting");

            int? Year = Utils.GetInt(dic, "Year");

            //  public Nullable<int> Semester { get; set; }
            int Semester = Utils.GetInt(dic, "Semester", -1);

            int? PeriodID = Utils.GetNullableInt(dic, "PeriodID");

            double SummedUpMark = Utils.GetDouble(dic, "SummedUpMark");

            string JudgementResult = Utils.GetString(dic, "JudgementResult");

            string Comment = Utils.GetString(dic, "Comment");

            DateTime? SummedUpDate = Utils.GetDateTime(dic, "SummedUpDate");

            double? SummedUpFromMark = Utils.GetNullableDouble(dic, "SummedUpFromMark");

            double? SummedUpToMark = Utils.GetNullableDouble(dic, "SummedUpToMark");
            List<int> lstPupilID = Utils.GetIntList(dic, "ListPupilID");
            string checkWithClassMovement = Utils.GetString(dic, "checkWithClassMovement");
            string NoCheckIsActivePupilSchool = Utils.GetString(dic, "NoCheckIsActivePupilSchool");

            IQueryable<SummedUpRecord> lsSummedUpRecord = this.SummedUpRecordRepository.All;
            //PhuongTD5
            //Bổ sung tìm kiếm theo IsActive
            if (string.IsNullOrWhiteSpace(NoCheckIsActivePupilSchool))
            {
                lsSummedUpRecord = lsSummedUpRecord.Where(o => o.PupilProfile.IsActive == true && o.SchoolProfile.IsActive == true);
            }
            //End Bổ sung tìm kiếm theo IsActive
            // bắt đầu tìm kiếm
            if (SummedUpRecordID != 0)
            {
                lsSummedUpRecord = lsSummedUpRecord.Where(su => (su.SummedUpRecordID == SummedUpRecordID));
            }
            if (PupilID != 0)
            {
                lsSummedUpRecord = lsSummedUpRecord.Where(su => (su.PupilID == PupilID));
            }
            if (ClassID != 0)
            {
                lsSummedUpRecord = lsSummedUpRecord.Where(su => (su.ClassID == ClassID));
            }
            if (SchoolID != 0)
            {
                lsSummedUpRecord = lsSummedUpRecord.Where(su => (su.SchoolID == SchoolID));
            }
            if (AcademicYearID != 0)
            {
                lsSummedUpRecord = lsSummedUpRecord.Where(su => (su.AcademicYearID == AcademicYearID));
            }
            if (lstPupilID != null && lstPupilID.Count() > 0)
            {
                lsSummedUpRecord = lsSummedUpRecord.Where(o => (lstPupilID.Contains(o.PupilID)));
            }
            if (SubjectID != 0)
            {
                lsSummedUpRecord = lsSummedUpRecord.Where(su => (su.SubjectID == SubjectID));
            }

            if (AppliedLevel != 0)
            {
                lsSummedUpRecord = lsSummedUpRecord.Where(su => (su.ClassProfile.EducationLevel.Grade == AppliedLevel));
            }
            if (EducationLevelID != 0)
            {
                lsSummedUpRecord = lsSummedUpRecord.Where(su => (su.ClassProfile.EducationLevelID == EducationLevelID));
            }

            if (Year != 0)
            {
                lsSummedUpRecord = lsSummedUpRecord.Where(su => (su.CreatedAcademicYear == Year));
            }
            if (Semester != -1)
            {
                if (Semester == 1)
                {
                    lsSummedUpRecord = lsSummedUpRecord.Where(su => su.Semester == Semester);// xoa diem xoa luon diem TBCN
                }
                else if (Semester == 2)
                {
                    lsSummedUpRecord = lsSummedUpRecord.Where(su => (su.Semester == Semester || su.Semester == 5) && su.ReTestMark != null);// lay diem thi lai de xoa
                }
                else if (Semester == 3)
                {
                    lsSummedUpRecord = lsSummedUpRecord.Where(su => (su.Semester == 3 || su.Semester == 2));//neu = 3 thi lay HK2,CN va lay diem thi lai de xoa
                }
            }
            if (checkWithClassMovement.Length == 0)
            {
                if (PeriodID == null)
                {
                    lsSummedUpRecord = lsSummedUpRecord.Where(su => su.PeriodID == null);

                }
                else
                {
                    lsSummedUpRecord = lsSummedUpRecord.Where(su => su.PeriodID == PeriodID.Value);
                }
            }

            if (SummedUpFromMark.HasValue)
            {
                lsSummedUpRecord = lsSummedUpRecord.Where(su => (su.SummedUpMark >= (decimal)SummedUpFromMark));
            }

            if (SummedUpToMark.HasValue)
            {
                lsSummedUpRecord = lsSummedUpRecord.Where(su => (su.SummedUpMark <= (decimal)SummedUpToMark));
            }
            if (JudgementResult.Length != 0)
            {
                lsSummedUpRecord = lsSummedUpRecord.Where(su => (su.JudgementResult == JudgementResult));
            }
            if (SummedUpDate != null)
            {
                lsSummedUpRecord = lsSummedUpRecord.Where(su => (su.SummedUpDate == SummedUpDate));
            }

            return lsSummedUpRecord;
        }
        #endregion Search


        #region Search summed up history

        /// <summary>
        /// Tìm kiếm thông tin tổng kết kết quả môn học chấm điểm và đánh giá theo giai đoạn / học kỳ / năm học
        /// </summary>
        /// <author>tungnd</author>
        /// <date>9/10/2012</date>
        /// <param name="ethnic">Đối tượng SummedUpRecord</param>
        /// <returns>Đối tượng SummedUpRecord</returns>



        #endregion Search

        #region GetPupilRetestSubject

        /// <summary>
        /// Lấy ra danh sách các môn thi lại của từng học sinh
        /// </summary>
        /// <author>tungnd</author>
        /// <date>9/10/2012</date>
        /// <param name="ethnic">Đối tượng SummedUpRecord</param>
        /// <returns>Đối tượng SummedUpRecord</returns>

        public IQueryable<SummedUpRecord> GetPupilRetestSubject(int PupilID, int AcademicYearID, int SchoolID, int Semester, int Year, IDictionary<string, object> SearchInfo)
        {
            if (PupilID == 0)
            {
                return null;
            }
            else if (AcademicYearID == 0)
            {
                return null;
            }
            else if (SchoolID == 0)
            {
                return null;
            }
            else if (Semester == 0)
            {
                return null;
            }
            else if (Year == 0)
            {
                return null;
            }
            else
            {
                SearchInfo["PupilID"] = PupilID;

                SearchInfo["AcademicYearID"] = AcademicYearID;

                SearchInfo["SchoolID"] = SchoolID;

                SearchInfo["Semester"] = Semester;

                SearchInfo["Year"] = Year;

                return Search(SearchInfo);
            }
        }

        #endregion GetPupilRetestSubject

        #region SearchBySchool

        /// <summary>
        /// Lấy ra thông tin từ mã trường
        /// </summary>
        /// <author>tungnd</author>
        /// <date>9/10/2012</date>
        /// <param name="ethnic">Đối tượng SummedUpRecord</param>
        /// <returns>Đối tượng SummedUpRecord</returns>

        public IQueryable<SummedUpRecord> SearchBySchool(int SchoolID, IDictionary<string, object> SearchInfo)
        {
            if (SchoolID == 0)
            {
                return null;
            }
            else
            {
                SearchInfo["SchoolID"] = SchoolID;
                return Search(SearchInfo);
            }
        }

        public IQueryable<SummedUpRecord> SearchBySchoolPrimary(int SchoolID, IDictionary<string, object> SearchInfo)
        {
            if (SchoolID == 0)
            {
                return null;
            }
            else
            {
                SearchInfo["SchoolID"] = SchoolID;
                return SearchPrimary(SearchInfo);
            }
        }

        #endregion SearchBySchool

        #region GetSummedUpRecordOfClass

        /// <summary>
        /// Lấy bảng điểm của lớp
        /// </summary>
        /// <author>tungnd</author>
        /// <date>9/10/2012</date>
        /// <param name="ethnic">Đối tượng SummedUpRecord</param>
        /// <returns>Đối tượng SummedUpRecord</returns>
        public IQueryable<SummedUpRecordBO> GetSummedUpRecordOfClass(int AcademicYearID, int SchoolID, int Semester, int ClassID, int? PeriodID, int SubjectID)
        {
            IQueryable<SummedUpRecordBO> _query;
            IQueryable<PupilOfClass> lstPOCLearning;

            var listPupilOfClass = PupilOfClassBusiness.SearchBySchool(SchoolID, new Dictionary<string, object>(){
                                        {"AcademicYearID", AcademicYearID},
                                        {"ClassID", ClassID},
                                        {"Check", "Check"},
                                    });

            //Gioi han danh sach hoc sinh neu la mon nghe
            SubjectCat subjectCat = SubjectCatBusiness.Find(SubjectID);
            int? createdAcaYear = AcademicYearBusiness.Find(AcademicYearID).Year;
            int last2SchoolNumber = SchoolID % 100;
            if (subjectCat.IsApprenticeshipSubject)
            {
                lstPOCLearning = from poc in listPupilOfClass
                                 join app in ApprenticeshipTrainingBusiness.All.Where(u => u.ApprenticeshipSubject.SubjectID == SubjectID)
                                 on new { poc.SchoolID, poc.AcademicYearID, poc.ClassID, poc.PupilID } equals new { SchoolID = app.SchoolID.Value, AcademicYearID = app.AcademicYearID.Value, ClassID = app.ClassID.Value, app.PupilID }
                                 select poc;
            }
            else
            {
                lstPOCLearning = listPupilOfClass;
            }

            if (PeriodID != null)
            {
                _query = from poc in lstPOCLearning
                         join pp in PupilProfileBusiness.All on poc.PupilID equals pp.PupilProfileID
                         join sur in VSummedUpRecordBusiness.All.Where(u => u.Last2digitNumberSchool == last2SchoolNumber && u.CreatedAcademicYear == createdAcaYear
                            && u.SubjectID == SubjectID && u.Semester == Semester && u.PeriodID == PeriodID.Value)
                         on new { poc.SchoolID, poc.AcademicYearID, poc.ClassID, poc.PupilID } equals new { sur.SchoolID, sur.AcademicYearID, sur.ClassID, sur.PupilID } into g1
                         from j2 in g1.DefaultIfEmpty()
                         select new SummedUpRecordBO
                         {
                             SummedUpRecordID = j2.SummedUpRecordID,
                             PupilID = poc.PupilID,
                             AcademicYearID = poc.AcademicYearID,
                             ClassID = poc.ClassID,
                             SchoolID = poc.SchoolID,
                             SubjectID = SubjectID,
                             Semester = Semester,
                             SummedUpMark = j2.SummedUpMark,
                             JudgementResult = j2.JudgementResult,
                             ReTestMark = j2.ReTestMark,
                             ReTestJudgement = j2.ReTestJudgement,
                             OrderInClass = poc.OrderInClass,
                             Status = poc.Status,
                             PupilCode = pp.PupilCode,
                             PupilFullName = pp.FullName,
                             Comment = j2.Comment,
                             Year = j2.CreatedAcademicYear,
                             IsCommenting = j2.IsCommenting,
                             AssignedDate = poc.AssignedDate
                         };
            }
            else if (Semester == GlobalConstants.SEMESTER_OF_YEAR_FIRST)
            {
                _query = from poc in lstPOCLearning
                         join pp in PupilProfileBusiness.All on poc.PupilID equals pp.PupilProfileID
                         join sur in VSummedUpRecordBusiness.All.Where(u => u.Last2digitNumberSchool == last2SchoolNumber && u.CreatedAcademicYear == createdAcaYear
                         && u.SubjectID == SubjectID && u.Semester == Semester && !u.PeriodID.HasValue)
                         on new { poc.SchoolID, poc.AcademicYearID, poc.ClassID, poc.PupilID } equals new { sur.SchoolID, sur.AcademicYearID, sur.ClassID, sur.PupilID } into g1
                         from j2 in g1.DefaultIfEmpty()
                         select new SummedUpRecordBO
                         {
                             PupilID = poc.PupilID,
                             AcademicYearID = poc.AcademicYearID,
                             ClassID = poc.ClassID,
                             SchoolID = poc.SchoolID,
                             SubjectID = SubjectID,
                             Semester = j2.Semester,
                             SummedUpMark = j2.SummedUpMark,
                             JudgementResult = j2.JudgementResult,
                             ReTestMark = j2.ReTestMark,
                             ReTestJudgement = j2.ReTestJudgement,
                             OrderInClass = poc.OrderInClass,
                             Status = poc.Status,
                             PupilCode = pp.PupilCode,
                             PupilFullName = pp.FullName,
                             Year = j2.CreatedAcademicYear,
                             Comment = j2.Comment,
                             IsCommenting = j2.IsCommenting,
                             AssignedDate = poc.AssignedDate
                         };
            }
            else
            {
                _query = from poc in lstPOCLearning
                         join pp in PupilProfileBusiness.All on poc.PupilID equals pp.PupilProfileID
                         join sur in VSummedUpRecordBusiness.All.Where(u => u.Last2digitNumberSchool == last2SchoolNumber && u.CreatedAcademicYear == createdAcaYear
                             && !u.PeriodID.HasValue && u.SubjectID == SubjectID && (u.Semester == GlobalConstants.SEMESTER_OF_YEAR_SECOND || u.Semester == GlobalConstants.SEMESTER_OF_YEAR_ALL))
                         on new { poc.SchoolID, poc.AcademicYearID, poc.ClassID, poc.PupilID } equals new { sur.SchoolID, sur.AcademicYearID, sur.ClassID, sur.PupilID } into g1
                         from j2 in g1.DefaultIfEmpty()
                         select new SummedUpRecordBO
                         {
                             PupilID = poc.PupilID,
                             AcademicYearID = poc.AcademicYearID,
                             ClassID = poc.ClassID,
                             SchoolID = poc.SchoolID,
                             SubjectID = SubjectID,
                             Semester = j2.Semester,
                             SummedUpMark = j2.SummedUpMark,
                             JudgementResult = j2.JudgementResult,
                             ReTestMark = j2.ReTestMark,
                             ReTestJudgement = j2.ReTestJudgement,
                             OrderInClass = poc.OrderInClass,
                             Status = poc.Status,
                             PupilCode = pp.PupilCode,
                             PupilFullName = pp.FullName,
                             Year = j2.CreatedAcademicYear,
                             Comment = j2.Comment,
                             IsCommenting = j2.IsCommenting,
                             AssignedDate = poc.AssignedDate
                         };
            }

            return _query;
        }

        #endregion GetSummedUpRecordOfClass

        #region GetSumedUpRecordCareerOfClass
        /// <summary>
        /// Lấy bảng điểm nghề của lớp
        /// </summary>
        /// <author>tungnt</author>
        /// <date>30/5/2013</date>
        /// <param name="ethnic">Đối tượng SummedUpRecord</param>
        /// <returns>Đối tượng SummedUpRecord</returns>
        public IQueryable<SummedUpRecordBO> GetSummedUpRecordCareerOfClass(int AcademicYearID, int SchoolID, int ClassID, int SubjectID)
        {
            IQueryable<SummedUpRecordBO> _query = from poc in PupilOfClassBusiness.SearchBySchool(SchoolID, new Dictionary<string, object>(){
                                                            {"AcademicYearID", AcademicYearID},
                                                            {"Check", "Check"},
                                                        })
                                                  join pp in PupilProfileBusiness.All on poc.PupilID equals pp.PupilProfileID
                                                  join sur in this.All.Where(u => u.SubjectID == SubjectID
                                                                              && (u.Semester == GlobalConstants.SEMESTER_OF_YEAR_FIRST || u.Semester == GlobalConstants.SEMESTER_OF_YEAR_ALL)
                                                                              && (u.ClassID == ClassID || ClassID == 0))
                                                  on new { poc.SchoolID, poc.AcademicYearID, poc.PupilID } equals new { sur.SchoolID, sur.AcademicYearID, sur.PupilID } into g1
                                                  from j2 in g1.DefaultIfEmpty()
                                                  select new SummedUpRecordBO
                                                  {
                                                      PupilID = poc.PupilID,
                                                      AcademicYearID = poc.AcademicYearID,
                                                      ClassID = poc.ClassID,
                                                      SchoolID = poc.SchoolID,
                                                      SubjectID = j2.SubjectID,
                                                      Semester = j2.Semester,
                                                      SummedUpMark = j2.SummedUpMark,
                                                      JudgementResult = j2.JudgementResult,
                                                      ReTestMark = j2.ReTestMark,
                                                      ReTestJudgement = j2.ReTestJudgement,
                                                      OrderInClass = poc.OrderInClass,
                                                      Status = poc.Status,
                                                      PupilCode = pp.PupilCode,
                                                      PupilFullName = pp.FullName,
                                                      Year = j2.CreatedAcademicYear,
                                                      Comment = j2.Comment,
                                                      IsCommenting = j2.IsCommenting,
                                                      AssignedDate = poc.AssignedDate
                                                  };
            return _query;
        }

        #endregion

        #region CaculatorSummedUpRecord

        /// <summary>
        /// Tính điểm trung bình
        /// </summary>
        /// <author>tungnd</author>
        /// <date>9/10/2012</date>
        /// <param name="ethnic">Đối tượng SummedUpRecord</param>
        /// <returns>Đối tượng SummedUpRecord</returns>
        /// QuangLM edit 21/03/2012

        public decimal? CaculatorSummedUpRecord(List<MarkRecord> lstMarkRecord, List<MarkType> listMarkType, SemeterDeclaration semeterDeclaration, ClassSubject objClassSubject, List<RegisterSubjectSpecializeBO> lstRegisterBO = null)
        {
            if (lstMarkRecord == null || lstMarkRecord.Count == 0)
            {
                return null;
            }
            // Neu chua co khai bao con diem hoc ky tra lai null    
            if (semeterDeclaration == null)
            {
                return null;
            }
            Dictionary<int, string> dicMarkType = new Dictionary<int, string>();
            foreach (MarkType mt in listMarkType)
            {
                dicMarkType[mt.MarkTypeID] = mt.Title;
            }
            decimal totalMark = 0;
            int count = 0;
            // Tach ra thanh cac diem M, P, V, HK
            // Tinh tong diem va tong so con diem thuc te nhap nhan voi he so tuong ung
            foreach (MarkRecord mr in lstMarkRecord)
            {
                int markTypeID = mr.MarkTypeID;
                if (dicMarkType.ContainsKey(markTypeID))
                {
                    string mtTitle = dicMarkType[markTypeID];
                    decimal mark = mr.Mark;
                    if (mtTitle.ToLower().Equals(GlobalConstants.MARK_TYPE_M.ToLower()))
                    {
                        totalMark += mark * semeterDeclaration.CoefficientInterview;
                        count += semeterDeclaration.CoefficientInterview;
                    }
                    else if (mtTitle.ToLower().Equals(GlobalConstants.MARK_TYPE_P.ToLower()))
                    {
                        totalMark += mark * semeterDeclaration.CoefficientWriting;
                        count += semeterDeclaration.CoefficientWriting;
                    }
                    else if (mtTitle.ToLower().Equals(GlobalConstants.MARK_TYPE_V.ToLower()))
                    {
                        totalMark += mark * semeterDeclaration.CoefficientTwice;
                        count += semeterDeclaration.CoefficientTwice;
                    }
                    else if (mtTitle.ToLower().Equals(GlobalConstants.MARK_TYPE_HK.ToLower()))
                    {
                        totalMark += mark * semeterDeclaration.CoefficientSemester;
                        count += semeterDeclaration.CoefficientSemester;
                    }
                }
            }
            if (count == 0)
            {
                return 0;
            }
            return Math.Round(totalMark / count, 1, MidpointRounding.AwayFromZero);
        }

        /// <summary>
        /// Tính điểm trung bình xem có đạt không
        /// </summary>
        /// <author>tungnd</author>
        /// <date>9/10/2012</date>
        /// <updated>namdv3, 24-12-2012</updated>
        /// <param name="ethnic">Đối tượng SummedUpRecord</param>
        /// <returns>Đối tượng SummedUpRecord</returns>

        public string CaculatorJudgementResult(List<JudgeRecord> lstJudgeRecord, List<MarkType> listMarkType)
        {
            if (lstJudgeRecord == null || lstJudgeRecord.Count == 0)
            {
                return string.Empty;
            }
            else
            {
                double countD = 0;
                double countCD = 0;
                string Mark_HK = "";
                Dictionary<int, string> dicMarkType = new Dictionary<int, string>();
                foreach (MarkType mt in listMarkType)
                {
                    dicMarkType[mt.MarkTypeID] = mt.Title;
                }
                for (int i = 0; i < lstJudgeRecord.Count; i++)
                {
                    string mtTitle = dicMarkType[lstJudgeRecord[i].MarkTypeID];
                    if (lstJudgeRecord[i].Judgement == "Đ")
                    {
                        countD++;
                    }
                    if (lstJudgeRecord[i].Judgement == "CĐ")
                    {
                        countCD++;
                    }
                    if (mtTitle.Equals(SystemParamsInFile.MARK_TYPE_HK))
                    {
                        Mark_HK = lstJudgeRecord[i].Judgement;
                    }
                }
                //namdv3
                //Nếu countD - Round((countD+countCD)*2/3) >= 0 return D
                if (countD - Math.Round((countD + countCD) * (2 / 3) + 0.01) >= 0 && Mark_HK == "Đ")
                {
                    return "Đ";
                }
                else
                {
                    return "CĐ";
                }
            }
        }

        #endregion CaculatorSummedUpRecord

        /// <summary>
        ///  GetSummedUpRecord
        /// </summary>
        /// <param name="AcademicYearID"></param>
        /// <param name="SchoolID"></param>
        /// <param name="Semester"></param>
        /// <param name="ClassID"></param>
        /// <param name="PeriodID"></param>
        /// <returns></returns>
        public IQueryable<SummedUpRecordBO> GetSummedUpRecord(int AcademicYearID, int SchoolID, int Semester, int ClassID, int? PeriodID)
        {
            IQueryable<SummedUpRecordBO> _query;
            AcademicYear academicYear = AcademicYearBusiness.Find(AcademicYearID);
            bool isMovedHistory = UtilsBusiness.IsMoveHistory(academicYear);
            int? createdAcaYear = academicYear.Year;
            int last2SchoolNumber = SchoolID % 100;


            if (PeriodID != null)
            {
                if (isMovedHistory)
                {
                    _query = from poc in PupilOfClassBusiness.SearchBySchool(SchoolID, new Dictionary<string, object> { { "AcademicYearID", AcademicYearID }, { "ClassID", ClassID }, { "Check", "Check" } })
                             join pp in PupilProfileBusiness.All on poc.PupilID equals pp.PupilProfileID
                             join sur in SummedUpRecordHistoryBusiness.All.Where(u => u.Last2digitNumberSchool == last2SchoolNumber && u.CreatedAcademicYear == createdAcaYear
                                 && u.Semester == Semester && u.PeriodID == PeriodID.Value && u.AcademicYearID == AcademicYearID &&
                                                                            u.SchoolID == SchoolID && u.ClassID == ClassID)
                                 on new { poc.AcademicYearID, poc.ClassID, poc.PupilID } equals new { sur.AcademicYearID, sur.ClassID, sur.PupilID } into g1
                             from j1 in g1.DefaultIfEmpty()
                             join sub in SubjectCatBusiness.All on j1.SubjectID equals sub.SubjectCatID
                             select new SummedUpRecordBO
                             {
                                 SummedUpRecordID = j1.SummedUpRecordID,
                                 PupilID = poc.PupilID,
                                 AcademicYearID = poc.AcademicYearID,
                                 ClassID = poc.ClassID,
                                 SchoolID = poc.SchoolID,
                                 SubjectID = j1.SubjectID,
                                 SubjectName = sub.DisplayName,
                                 Semester = Semester,
                                 SummedUpMark = j1.SummedUpMark,
                                 JudgementResult = j1.JudgementResult,
                                 ReTestMark = j1.ReTestMark,
                                 ReTestJudgement = j1.ReTestJudgement,
                                 OrderInClass = poc.OrderInClass,
                                 Status = poc.Status,
                                 PupilCode = pp.PupilCode,
                                 PupilFullName = pp.FullName,
                                 Name = pp.Name,
                                 Year = j1.CreatedAcademicYear,
                                 PeriodID = j1.PeriodID,
                                 IsCommenting = j1.IsCommenting,
                                 Comment = j1.Comment,
                                 AssignedDate = poc.AssignedDate
                             };
                }
                else
                {
                    _query = from poc in PupilOfClassBusiness.SearchBySchool(SchoolID, new Dictionary<string, object> { { "AcademicYearID", AcademicYearID }, { "ClassID", ClassID }, { "Check", "Check" } })
                             join pp in PupilProfileBusiness.All on poc.PupilID equals pp.PupilProfileID
                             join sur in SummedUpRecordBusiness.All.Where(u => u.Last2digitNumberSchool == last2SchoolNumber && u.CreatedAcademicYear == createdAcaYear
                                 && u.Semester == Semester && u.PeriodID == PeriodID.Value && u.AcademicYearID == AcademicYearID &&
                                                                            u.SchoolID == SchoolID && u.ClassID == ClassID)
                                 on new { poc.AcademicYearID, poc.ClassID, poc.PupilID } equals new { sur.AcademicYearID, sur.ClassID, sur.PupilID } into g1
                             from j1 in g1.DefaultIfEmpty()
                             join sub in SubjectCatBusiness.All on j1.SubjectID equals sub.SubjectCatID
                             select new SummedUpRecordBO
                             {
                                 SummedUpRecordID = j1.SummedUpRecordID,
                                 PupilID = poc.PupilID,
                                 AcademicYearID = poc.AcademicYearID,
                                 ClassID = poc.ClassID,
                                 SchoolID = poc.SchoolID,
                                 SubjectID = j1.SubjectID,
                                 SubjectName = sub.DisplayName,
                                 Semester = Semester,
                                 SummedUpMark = j1.SummedUpMark,
                                 JudgementResult = j1.JudgementResult,
                                 ReTestMark = j1.ReTestMark,
                                 ReTestJudgement = j1.ReTestJudgement,
                                 OrderInClass = poc.OrderInClass,
                                 Status = poc.Status,
                                 PupilCode = pp.PupilCode,
                                 PupilFullName = pp.FullName,
                                 Name = pp.Name,
                                 Year = j1.CreatedAcademicYear,
                                 PeriodID = j1.PeriodID,
                                 IsCommenting = j1.IsCommenting,
                                 Comment = j1.Comment,
                                 AssignedDate = poc.AssignedDate
                             };
                }

            }
            else if (Semester == GlobalConstants.SEMESTER_OF_YEAR_FIRST || Semester == GlobalConstants.SEMESTER_OF_YEAR_SECOND)
            {
                if (isMovedHistory)
                {
                    _query = from poc in PupilOfClassBusiness.SearchBySchool(SchoolID, new Dictionary<string, object> { { "AcademicYearID", AcademicYearID }, { "ClassID", ClassID }, { "Check", "Check" } })
                             join pp in PupilProfileBusiness.All on poc.PupilID equals pp.PupilProfileID
                             join sur in SummedUpRecordHistoryBusiness.All.Where(u => u.Last2digitNumberSchool == last2SchoolNumber && u.CreatedAcademicYear == createdAcaYear
                                 && u.SchoolID == SchoolID && u.Semester == Semester && u.PeriodID == null)
                                 on new { poc.AcademicYearID, poc.ClassID, poc.PupilID } equals new { sur.AcademicYearID, sur.ClassID, sur.PupilID } into g1
                             from j1 in g1.DefaultIfEmpty()
                             join sub in SubjectCatBusiness.All on j1.SubjectID equals sub.SubjectCatID
                             select new SummedUpRecordBO
                             {
                                 SummedUpRecordID = j1.SummedUpRecordID,
                                 PupilID = poc.PupilID,
                                 AcademicYearID = poc.AcademicYearID,
                                 ClassID = poc.ClassID,
                                 SchoolID = poc.SchoolID,
                                 SubjectID = j1.SubjectID,
                                 SubjectName = sub.DisplayName,
                                 Semester = Semester,
                                 SummedUpMark = j1.SummedUpMark,
                                 JudgementResult = j1.JudgementResult,
                                 ReTestMark = j1.ReTestMark,
                                 ReTestJudgement = j1.ReTestJudgement,
                                 OrderInClass = poc.OrderInClass,
                                 Status = poc.Status,
                                 PupilCode = pp.PupilCode,
                                 PupilFullName = pp.FullName,
                                 Name = pp.Name,
                                 Year = j1.CreatedAcademicYear,
                                 IsCommenting = j1.IsCommenting,
                                 Comment = j1.Comment,
                                 AssignedDate = poc.AssignedDate
                             };
                }
                else
                {
                    _query = from poc in PupilOfClassBusiness.SearchBySchool(SchoolID, new Dictionary<string, object> { { "AcademicYearID", AcademicYearID }, { "ClassID", ClassID }, { "Check", "Check" } })
                             join pp in PupilProfileBusiness.All on poc.PupilID equals pp.PupilProfileID
                             join sur in SummedUpRecordBusiness.All.Where(u => u.Last2digitNumberSchool == last2SchoolNumber && u.CreatedAcademicYear == createdAcaYear
                                 && u.SchoolID == SchoolID && u.Semester == Semester && u.PeriodID == null)
                                 on new { poc.AcademicYearID, poc.ClassID, poc.PupilID } equals new { sur.AcademicYearID, sur.ClassID, sur.PupilID } into g1
                             from j1 in g1.DefaultIfEmpty()
                             join sub in SubjectCatBusiness.All on j1.SubjectID equals sub.SubjectCatID
                             select new SummedUpRecordBO
                             {
                                 SummedUpRecordID = j1.SummedUpRecordID,
                                 PupilID = poc.PupilID,
                                 AcademicYearID = poc.AcademicYearID,
                                 ClassID = poc.ClassID,
                                 SchoolID = poc.SchoolID,
                                 SubjectID = j1.SubjectID,
                                 SubjectName = sub.DisplayName,
                                 Semester = Semester,
                                 SummedUpMark = j1.SummedUpMark,
                                 JudgementResult = j1.JudgementResult,
                                 ReTestMark = j1.ReTestMark,
                                 ReTestJudgement = j1.ReTestJudgement,
                                 OrderInClass = poc.OrderInClass,
                                 Status = poc.Status,
                                 PupilCode = pp.PupilCode,
                                 PupilFullName = pp.FullName,
                                 Name = pp.Name,
                                 Year = j1.CreatedAcademicYear,
                                 IsCommenting = j1.IsCommenting,
                                 Comment = j1.Comment,
                                 AssignedDate = poc.AssignedDate
                             };
                }

            }
            else
            {
                if (isMovedHistory)
                {
                    _query = from poc in PupilOfClassBusiness.SearchBySchool(SchoolID, new Dictionary<string, object> { { "AcademicYearID", AcademicYearID }, { "ClassID", ClassID }, { "Check", "Check" } })
                             join pp in PupilProfileBusiness.All on poc.PupilID equals pp.PupilProfileID
                             join sur in SummedUpRecordHistoryBusiness.All.Where(u => u.Last2digitNumberSchool == last2SchoolNumber && u.CreatedAcademicYear == createdAcaYear
                                 && u.SchoolID == SchoolID && (u.Semester == SystemParamsInFile.SEMESTER_OF_YEAR_ALL || u.Semester == SystemParamsInFile.SEMESTER_OF_YEAR_RETEST) && !u.PeriodID.HasValue)
                                 on new { poc.AcademicYearID, poc.ClassID, poc.PupilID } equals new { sur.AcademicYearID, sur.ClassID, sur.PupilID } into g1
                             from j1 in g1.DefaultIfEmpty()
                             join sub in SubjectCatBusiness.All on j1.SubjectID equals sub.SubjectCatID
                             select new SummedUpRecordBO
                             {
                                 SummedUpRecordID = j1.SummedUpRecordID,
                                 PupilID = poc.PupilID,
                                 AcademicYearID = poc.AcademicYearID,
                                 ClassID = poc.ClassID,
                                 SchoolID = poc.SchoolID,
                                 SubjectID = j1.SubjectID,
                                 SubjectName = sub.DisplayName,
                                 Semester = j1.Semester,
                                 SummedUpMark = j1.SummedUpMark,
                                 JudgementResult = j1.JudgementResult,
                                 ReTestMark = j1.ReTestMark,
                                 ReTestJudgement = j1.ReTestJudgement,
                                 OrderInClass = poc.OrderInClass,
                                 Status = poc.Status,
                                 PupilCode = pp.PupilCode,
                                 PupilFullName = pp.FullName,
                                 Name = pp.Name,
                                 Year = j1.CreatedAcademicYear,
                                 IsCommenting = j1.IsCommenting,
                                 Comment = j1.Comment,
                                 AssignedDate = poc.AssignedDate
                             };
                }
                else
                {
                    _query = from poc in PupilOfClassBusiness.SearchBySchool(SchoolID, new Dictionary<string, object> { { "AcademicYearID", AcademicYearID }, { "ClassID", ClassID }, { "Check", "Check" } })
                             join pp in PupilProfileBusiness.All on poc.PupilID equals pp.PupilProfileID
                             join sur in SummedUpRecordBusiness.All.Where(u => u.Last2digitNumberSchool == last2SchoolNumber && u.CreatedAcademicYear == createdAcaYear
                                 && u.SchoolID == SchoolID && (u.Semester == SystemParamsInFile.SEMESTER_OF_YEAR_ALL || u.Semester == SystemParamsInFile.SEMESTER_OF_YEAR_RETEST) && !u.PeriodID.HasValue)
                                 on new { poc.AcademicYearID, poc.ClassID, poc.PupilID } equals new { sur.AcademicYearID, sur.ClassID, sur.PupilID } into g1
                             from j1 in g1.DefaultIfEmpty()
                             join sub in SubjectCatBusiness.All on j1.SubjectID equals sub.SubjectCatID
                             select new SummedUpRecordBO
                             {
                                 SummedUpRecordID = j1.SummedUpRecordID,
                                 PupilID = poc.PupilID,
                                 AcademicYearID = poc.AcademicYearID,
                                 ClassID = poc.ClassID,
                                 SchoolID = poc.SchoolID,
                                 SubjectID = j1.SubjectID,
                                 SubjectName = sub.DisplayName,
                                 Semester = j1.Semester,
                                 SummedUpMark = j1.SummedUpMark,
                                 JudgementResult = j1.JudgementResult,
                                 ReTestMark = j1.ReTestMark,
                                 ReTestJudgement = j1.ReTestJudgement,
                                 OrderInClass = poc.OrderInClass,
                                 Status = poc.Status,
                                 PupilCode = pp.PupilCode,
                                 PupilFullName = pp.FullName,
                                 Name = pp.Name,
                                 Year = j1.CreatedAcademicYear,
                                 IsCommenting = j1.IsCommenting,
                                 Comment = j1.Comment,
                                 AssignedDate = poc.AssignedDate
                             };
                }

            }


            return _query;
        }

        /// <summary>
        ///  GetSummedUpRecord
        /// </summary>
        /// <param name="AcademicYearID"></param>
        /// <param name="SchoolID"></param>
        /// <param name="Semester"></param>
        /// <param name="ClassID"></param>
        /// <param name="PeriodID"></param>
        /// <returns></returns>
        public IQueryable<SummedUpRecordBO> GetSummedUpRecordByListClassID(List<int> lstClassID, int AcademicYearID, int SchoolID, int Semester, int? PeriodID)
        {
            IQueryable<SummedUpRecordBO> _query;
            AcademicYear academicYear = AcademicYearBusiness.Find(AcademicYearID);
            bool isMovedHistory = UtilsBusiness.IsMoveHistory(academicYear);
            int? createdAcaYear = academicYear.Year;
            int last2SchoolNumber = SchoolID % 100;


            if (PeriodID != null)
            {
                if (isMovedHistory)
                {
                    _query = from poc in PupilOfClassBusiness.SearchBySchool(SchoolID, new Dictionary<string, object> { { "AcademicYearID", AcademicYearID }, { "ListClassID", lstClassID }, { "Check", "Check" } })
                             join pp in PupilProfileBusiness.All on poc.PupilID equals pp.PupilProfileID
                             join sur in SummedUpRecordHistoryBusiness.All.Where(u => u.Last2digitNumberSchool == last2SchoolNumber && u.CreatedAcademicYear == createdAcaYear
                                 && u.Semester == Semester && u.PeriodID == PeriodID.Value && u.AcademicYearID == AcademicYearID &&
                                                                            u.SchoolID == SchoolID && lstClassID.Contains(u.ClassID))
                                 on new { poc.AcademicYearID, poc.ClassID, poc.PupilID } equals new { sur.AcademicYearID, sur.ClassID, sur.PupilID } into g1
                             from j1 in g1.DefaultIfEmpty()
                             join sub in SubjectCatBusiness.All on j1.SubjectID equals sub.SubjectCatID
                             select new SummedUpRecordBO
                             {
                                 SummedUpRecordID = j1.SummedUpRecordID,
                                 PupilID = poc.PupilID,
                                 AcademicYearID = poc.AcademicYearID,
                                 ClassID = poc.ClassID,
                                 SchoolID = poc.SchoolID,
                                 SubjectID = j1.SubjectID,
                                 SubjectName = sub.DisplayName,
                                 Semester = Semester,
                                 SummedUpMark = j1.SummedUpMark,
                                 JudgementResult = j1.JudgementResult,
                                 ReTestMark = j1.ReTestMark,
                                 ReTestJudgement = j1.ReTestJudgement,
                                 OrderInClass = poc.OrderInClass,
                                 Status = poc.Status,
                                 PupilCode = pp.PupilCode,
                                 PupilFullName = pp.FullName,
                                 Name = pp.Name,
                                 Year = j1.CreatedAcademicYear,
                                 PeriodID = j1.PeriodID,
                                 IsCommenting = j1.IsCommenting,
                                 Comment = j1.Comment,
                                 AssignedDate = poc.AssignedDate
                             };
                }
                else
                {
                    _query = from poc in PupilOfClassBusiness.SearchBySchool(SchoolID, new Dictionary<string, object> { { "AcademicYearID", AcademicYearID }, { "ListClassID", lstClassID }, { "Check", "Check" } })
                             join pp in PupilProfileBusiness.All on poc.PupilID equals pp.PupilProfileID
                             join sur in SummedUpRecordBusiness.All.Where(u => u.Last2digitNumberSchool == last2SchoolNumber && u.CreatedAcademicYear == createdAcaYear
                                 && u.Semester == Semester && u.PeriodID == PeriodID.Value && u.AcademicYearID == AcademicYearID &&
                                                                            u.SchoolID == SchoolID && lstClassID.Contains(u.ClassID))
                                 on new { poc.AcademicYearID, poc.ClassID, poc.PupilID } equals new { sur.AcademicYearID, sur.ClassID, sur.PupilID } into g1
                             from j1 in g1.DefaultIfEmpty()
                             join sub in SubjectCatBusiness.All on j1.SubjectID equals sub.SubjectCatID
                             select new SummedUpRecordBO
                             {
                                 SummedUpRecordID = j1.SummedUpRecordID,
                                 PupilID = poc.PupilID,
                                 AcademicYearID = poc.AcademicYearID,
                                 ClassID = poc.ClassID,
                                 SchoolID = poc.SchoolID,
                                 SubjectID = j1.SubjectID,
                                 SubjectName = sub.DisplayName,
                                 Semester = Semester,
                                 SummedUpMark = j1.SummedUpMark,
                                 JudgementResult = j1.JudgementResult,
                                 ReTestMark = j1.ReTestMark,
                                 ReTestJudgement = j1.ReTestJudgement,
                                 OrderInClass = poc.OrderInClass,
                                 Status = poc.Status,
                                 PupilCode = pp.PupilCode,
                                 PupilFullName = pp.FullName,
                                 Name = pp.Name,
                                 Year = j1.CreatedAcademicYear,
                                 PeriodID = j1.PeriodID,
                                 IsCommenting = j1.IsCommenting,
                                 Comment = j1.Comment,
                                 AssignedDate = poc.AssignedDate
                             };
                }

            }
            else if (Semester == GlobalConstants.SEMESTER_OF_YEAR_FIRST || Semester == GlobalConstants.SEMESTER_OF_YEAR_SECOND)
            {
                if (isMovedHistory)
                {
                    _query = from poc in PupilOfClassBusiness.SearchBySchool(SchoolID, new Dictionary<string, object> { { "AcademicYearID", AcademicYearID }, { "ListClassID", lstClassID }, { "Check", "Check" } })
                             join pp in PupilProfileBusiness.All on poc.PupilID equals pp.PupilProfileID
                             join sur in SummedUpRecordHistoryBusiness.All.Where(u => u.Last2digitNumberSchool == last2SchoolNumber && u.CreatedAcademicYear == createdAcaYear
                                 && u.SchoolID == SchoolID && u.Semester == Semester && u.PeriodID == null && lstClassID.Contains(u.ClassID))
                                 on new { poc.AcademicYearID, poc.ClassID, poc.PupilID } equals new { sur.AcademicYearID, sur.ClassID, sur.PupilID } into g1
                             from j1 in g1.DefaultIfEmpty()
                             join sub in SubjectCatBusiness.All on j1.SubjectID equals sub.SubjectCatID
                             select new SummedUpRecordBO
                             {
                                 SummedUpRecordID = j1.SummedUpRecordID,
                                 PupilID = poc.PupilID,
                                 AcademicYearID = poc.AcademicYearID,
                                 ClassID = poc.ClassID,
                                 SchoolID = poc.SchoolID,
                                 SubjectID = j1.SubjectID,
                                 SubjectName = sub.DisplayName,
                                 Semester = Semester,
                                 SummedUpMark = j1.SummedUpMark,
                                 JudgementResult = j1.JudgementResult,
                                 ReTestMark = j1.ReTestMark,
                                 ReTestJudgement = j1.ReTestJudgement,
                                 OrderInClass = poc.OrderInClass,
                                 Status = poc.Status,
                                 PupilCode = pp.PupilCode,
                                 PupilFullName = pp.FullName,
                                 Name = pp.Name,
                                 Year = j1.CreatedAcademicYear,
                                 IsCommenting = j1.IsCommenting,
                                 Comment = j1.Comment,
                                 AssignedDate = poc.AssignedDate
                             };
                }
                else
                {
                    _query = from poc in PupilOfClassBusiness.SearchBySchool(SchoolID, new Dictionary<string, object> { { "AcademicYearID", AcademicYearID }, { "ListClassID", lstClassID }, { "Check", "Check" } })
                             join pp in PupilProfileBusiness.All on poc.PupilID equals pp.PupilProfileID
                             join sur in SummedUpRecordBusiness.All.Where(u => u.Last2digitNumberSchool == last2SchoolNumber && u.CreatedAcademicYear == createdAcaYear
                                 && u.SchoolID == SchoolID && u.Semester == Semester && u.PeriodID == null && lstClassID.Contains(u.ClassID))
                                 on new { poc.AcademicYearID, poc.ClassID, poc.PupilID } equals new { sur.AcademicYearID, sur.ClassID, sur.PupilID } into g1
                             from j1 in g1.DefaultIfEmpty()
                             join sub in SubjectCatBusiness.All on j1.SubjectID equals sub.SubjectCatID
                             select new SummedUpRecordBO
                             {
                                 SummedUpRecordID = j1.SummedUpRecordID,
                                 PupilID = poc.PupilID,
                                 AcademicYearID = poc.AcademicYearID,
                                 ClassID = poc.ClassID,
                                 SchoolID = poc.SchoolID,
                                 SubjectID = j1.SubjectID,
                                 SubjectName = sub.DisplayName,
                                 Semester = Semester,
                                 SummedUpMark = j1.SummedUpMark,
                                 JudgementResult = j1.JudgementResult,
                                 ReTestMark = j1.ReTestMark,
                                 ReTestJudgement = j1.ReTestJudgement,
                                 OrderInClass = poc.OrderInClass,
                                 Status = poc.Status,
                                 PupilCode = pp.PupilCode,
                                 PupilFullName = pp.FullName,
                                 Name = pp.Name,
                                 Year = j1.CreatedAcademicYear,
                                 IsCommenting = j1.IsCommenting,
                                 Comment = j1.Comment,
                                 AssignedDate = poc.AssignedDate
                             };
                }

            }
            else
            {
                if (isMovedHistory)
                {
                    _query = from poc in PupilOfClassBusiness.SearchBySchool(SchoolID, new Dictionary<string, object> { { "AcademicYearID", AcademicYearID }, { "ListClassID", lstClassID }, { "Check", "Check" } })
                             join pp in PupilProfileBusiness.All on poc.PupilID equals pp.PupilProfileID
                             join sur in SummedUpRecordHistoryBusiness.All.Where(u => u.Last2digitNumberSchool == last2SchoolNumber && u.CreatedAcademicYear == createdAcaYear
                                 && u.SchoolID == SchoolID && (u.Semester == SystemParamsInFile.SEMESTER_OF_YEAR_ALL || u.Semester == SystemParamsInFile.SEMESTER_OF_YEAR_RETEST)
                                 && !u.PeriodID.HasValue && lstClassID.Contains(u.ClassID))
                                 on new { poc.AcademicYearID, poc.ClassID, poc.PupilID } equals new { sur.AcademicYearID, sur.ClassID, sur.PupilID } into g1
                             from j1 in g1.DefaultIfEmpty()
                             join sub in SubjectCatBusiness.All on j1.SubjectID equals sub.SubjectCatID
                             select new SummedUpRecordBO
                             {
                                 SummedUpRecordID = j1.SummedUpRecordID,
                                 PupilID = poc.PupilID,
                                 AcademicYearID = poc.AcademicYearID,
                                 ClassID = poc.ClassID,
                                 SchoolID = poc.SchoolID,
                                 SubjectID = j1.SubjectID,
                                 SubjectName = sub.DisplayName,
                                 Semester = j1.Semester,
                                 SummedUpMark = j1.SummedUpMark,
                                 JudgementResult = j1.JudgementResult,
                                 ReTestMark = j1.ReTestMark,
                                 ReTestJudgement = j1.ReTestJudgement,
                                 OrderInClass = poc.OrderInClass,
                                 Status = poc.Status,
                                 PupilCode = pp.PupilCode,
                                 PupilFullName = pp.FullName,
                                 Name = pp.Name,
                                 Year = j1.CreatedAcademicYear,
                                 IsCommenting = j1.IsCommenting,
                                 Comment = j1.Comment,
                                 AssignedDate = poc.AssignedDate
                             };
                }
                else
                {
                    _query = from poc in PupilOfClassBusiness.SearchBySchool(SchoolID, new Dictionary<string, object> { { "AcademicYearID", AcademicYearID }, { "ListClassID", lstClassID }, { "Check", "Check" } })
                             join pp in PupilProfileBusiness.All on poc.PupilID equals pp.PupilProfileID
                             join sur in SummedUpRecordBusiness.All.Where(u => u.Last2digitNumberSchool == last2SchoolNumber && u.CreatedAcademicYear == createdAcaYear
                                 && u.SchoolID == SchoolID && (u.Semester == SystemParamsInFile.SEMESTER_OF_YEAR_ALL || u.Semester == SystemParamsInFile.SEMESTER_OF_YEAR_RETEST)
                                 && !u.PeriodID.HasValue && lstClassID.Contains(u.ClassID))
                                 on new { poc.AcademicYearID, poc.ClassID, poc.PupilID } equals new { sur.AcademicYearID, sur.ClassID, sur.PupilID } into g1
                             from j1 in g1.DefaultIfEmpty()
                             join sub in SubjectCatBusiness.All on j1.SubjectID equals sub.SubjectCatID
                             select new SummedUpRecordBO
                             {
                                 SummedUpRecordID = j1.SummedUpRecordID,
                                 PupilID = poc.PupilID,
                                 AcademicYearID = poc.AcademicYearID,
                                 ClassID = poc.ClassID,
                                 SchoolID = poc.SchoolID,
                                 SubjectID = j1.SubjectID,
                                 SubjectName = sub.DisplayName,
                                 Semester = j1.Semester,
                                 SummedUpMark = j1.SummedUpMark,
                                 JudgementResult = j1.JudgementResult,
                                 ReTestMark = j1.ReTestMark,
                                 ReTestJudgement = j1.ReTestJudgement,
                                 OrderInClass = poc.OrderInClass,
                                 Status = poc.Status,
                                 PupilCode = pp.PupilCode,
                                 PupilFullName = pp.FullName,
                                 Name = pp.Name,
                                 Year = j1.CreatedAcademicYear,
                                 IsCommenting = j1.IsCommenting,
                                 Comment = j1.Comment,
                                 AssignedDate = poc.AssignedDate
                             };
                }

            }


            return _query;
        }


        /// <summary>
        /// Lay sanh sach tong ket diem
        /// </summary>
        /// <param name="AcademicYearID"></param>
        /// <param name="SchoolID"></param>
        /// <param name="Semester"></param>
        /// <param name="ClassID"></param>
        /// <param name="PeriodID"></param>
        /// <returns></returns>
        public IQueryable<SummedUpRecordBO> GetSummedUpRecordCurrentYear(int AcademicYearID, int SchoolID, int Semester, int ClassID, int? PeriodID, bool isShowRetetResutl)
        {
            IQueryable<SummedUpRecordBO> _query;
            int? createdAcaYear = AcademicYearBusiness.Find(AcademicYearID).Year;
            int last2SchoolNumber = SchoolID % 100;
            IDictionary<string, object> dicClass = new Dictionary<string, object>();
            dicClass["SchoolID"] = SchoolID;

            if (PeriodID != null)
            {
                _query = from poc in PupilOfClassBusiness.SearchBySchool(SchoolID, new Dictionary<string, object> { { "AcademicYearID", AcademicYearID }, { "ClassID", ClassID }, { "Check", "Check" } })
                         join pp in PupilProfileBusiness.All on poc.PupilID equals pp.PupilProfileID
                         join soc in ClassSubjectBusiness.SearchByClass(ClassID, dicClass) on poc.ClassID equals soc.ClassID
                         join sur in SummedUpRecordBusiness.All.Where(u => u.Last2digitNumberSchool == last2SchoolNumber
                                                                            && u.Semester == Semester
                                                                            && u.PeriodID == PeriodID.Value
                                                                            && u.AcademicYearID == AcademicYearID
                                                                            && u.SchoolID == SchoolID
                                                                            && u.ClassID == ClassID)
                             on new { poc.AcademicYearID, poc.ClassID, poc.PupilID, soc.SubjectID } equals new { sur.AcademicYearID, sur.ClassID, sur.PupilID, sur.SubjectID } into g1
                         from j1 in g1.DefaultIfEmpty()
                         join sub in SubjectCatBusiness.All on j1.SubjectID equals sub.SubjectCatID
                         select new SummedUpRecordBO
                         {
                             SummedUpRecordID = j1.SummedUpRecordID,
                             PupilID = poc.PupilID,
                             AcademicYearID = poc.AcademicYearID,
                             ClassID = poc.ClassID,
                             SchoolID = poc.SchoolID,
                             SubjectID = j1.SubjectID,
                             SubjectName = sub.DisplayName,
                             Semester = Semester,
                             SummedUpMark = j1.SummedUpMark,
                             JudgementResult = j1.JudgementResult,
                             ReTestMark = j1.ReTestMark,
                             ReTestJudgement = j1.ReTestJudgement,
                             OrderInClass = poc.OrderInClass,
                             Status = poc.Status,
                             PupilCode = pp.PupilCode,
                             PupilFullName = pp.FullName,
                             Name = pp.Name,
                             Year = j1.CreatedAcademicYear,
                             PeriodID = j1.PeriodID,
                             IsCommenting = soc.IsCommenting, //j1.IsCommenting,
                             Comment = j1.Comment,
                             AssignedDate = poc.AssignedDate,
                             SubjectTypeID = soc.AppliedType.Value,
                             IsSpecialize = soc.IsSpecializedSubject

                         };
            }
            else if (Semester == GlobalConstants.SEMESTER_OF_YEAR_FIRST || Semester == GlobalConstants.SEMESTER_OF_YEAR_SECOND)
            {
                _query = from poc in PupilOfClassBusiness.SearchBySchool(SchoolID, new Dictionary<string, object> { { "AcademicYearID", AcademicYearID }, { "ClassID", ClassID }, { "Check", "Check" } })
                         join pp in PupilProfileBusiness.All.Where(pp => pp.IsActive) on poc.PupilID equals pp.PupilProfileID
                         join soc in ClassSubjectBusiness.SearchByClass(ClassID, dicClass) on poc.ClassID equals soc.ClassID
                         join sur in SummedUpRecordBusiness.All.Where(u => u.Last2digitNumberSchool == last2SchoolNumber
                                                                           && u.AcademicYearID == AcademicYearID
                                                                           && u.ClassID == ClassID
                                                                           && u.SchoolID == SchoolID
                                                                           && u.Semester == Semester
                                                                           && u.PeriodID == null) on poc.PupilID equals sur.PupilID into g1
                         //on new { poc.AcademicYearID, poc.ClassID, poc.PupilID } equals new { sur.AcademicYearID, sur.ClassID, sur.PupilID } into g1
                         from j1 in g1.DefaultIfEmpty()
                         join sub in SubjectCatBusiness.All on j1.SubjectID equals sub.SubjectCatID
                         select new SummedUpRecordBO
                         {
                             SummedUpRecordID = j1.SummedUpRecordID,
                             PupilID = poc.PupilID,
                             AcademicYearID = poc.AcademicYearID,
                             ClassID = poc.ClassID,
                             SchoolID = poc.SchoolID,
                             SubjectID = j1.SubjectID,
                             SubjectName = sub.DisplayName,
                             Semester = Semester,
                             SummedUpMark = j1.SummedUpMark,
                             JudgementResult = j1.JudgementResult,
                             ReTestMark = j1.ReTestMark,
                             ReTestJudgement = j1.ReTestJudgement,
                             OrderInClass = poc.OrderInClass,
                             Status = poc.Status,
                             PupilCode = pp.PupilCode,
                             PupilFullName = pp.FullName,
                             Name = pp.Name,
                             Year = j1.CreatedAcademicYear,
                             IsCommenting = j1.IsCommenting,
                             Comment = j1.Comment,
                             AssignedDate = poc.AssignedDate,
                             SubjectTypeID = soc.AppliedType.Value,
                             IsSpecialize = soc.IsSpecializedSubject
                         };
            }
            else
            {
                _query = from poc in PupilOfClassBusiness.SearchBySchool(SchoolID, new Dictionary<string, object> { { "AcademicYearID", AcademicYearID }, { "ClassID", ClassID }, { "Check", "Check" } })
                         join pp in PupilProfileBusiness.All on poc.PupilID equals pp.PupilProfileID
                         join soc in ClassSubjectBusiness.SearchByClass(ClassID, dicClass) on poc.ClassID equals soc.ClassID
                         join sur in SummedUpRecordBusiness.All.Where(u => u.Last2digitNumberSchool == last2SchoolNumber
                                                                            && u.AcademicYearID == AcademicYearID
                                                                            && u.ClassID == ClassID
                                                                            && u.SchoolID == SchoolID
                                                                            //&& (u.Semester == SystemParamsInFile.SEMESTER_OF_YEAR_ALL || u.Semester == SystemParamsInFile.SEMESTER_OF_YEAR_RETEST)
                                                                            && !u.PeriodID.HasValue)
                             on new { poc.AcademicYearID, poc.ClassID, poc.PupilID } equals new { sur.AcademicYearID, sur.ClassID, sur.PupilID } into g1
                         from j1 in g1.DefaultIfEmpty()
                         join sub in SubjectCatBusiness.All on j1.SubjectID equals sub.SubjectCatID
                         select new SummedUpRecordBO
                         {
                             SummedUpRecordID = j1.SummedUpRecordID,
                             PupilID = poc.PupilID,
                             AcademicYearID = poc.AcademicYearID,
                             ClassID = poc.ClassID,
                             SchoolID = poc.SchoolID,
                             SubjectID = j1.SubjectID,
                             SubjectName = sub.DisplayName,
                             Semester = j1.Semester,
                             SummedUpMark = j1.SummedUpMark,
                             JudgementResult = j1.JudgementResult,
                             ReTestMark = j1.ReTestMark,
                             ReTestJudgement = j1.ReTestJudgement,
                             OrderInClass = poc.OrderInClass,
                             Status = poc.Status,
                             PupilCode = pp.PupilCode,
                             PupilFullName = pp.FullName,
                             Name = pp.Name,
                             Year = j1.CreatedAcademicYear,
                             IsCommenting = j1.IsCommenting,
                             Comment = j1.Comment,
                             AssignedDate = poc.AssignedDate,
                             SubjectTypeID = soc.AppliedType.Value,
                             IsSpecialize = soc.IsSpecializedSubject
                         };
                if (isShowRetetResutl)
                {
                    _query = _query.Where(u => u.Semester == SystemParamsInFile.SEMESTER_OF_YEAR_ALL || u.Semester == SystemParamsInFile.SEMESTER_OF_YEAR_RETEST);
                }
                else
                {
                    _query = _query.Where(u => u.Semester == SystemParamsInFile.SEMESTER_OF_YEAR_ALL);
                }
            }
            return _query;
        }


        /// <summary>
        /// Thêm mới hoặc cập nhật SummedUpRecord, ModifiedDate của PupilRetestRepository.
        /// </summary>
        /// <author>Hieund</author>
        /// <param name="lstPupilRetest"></param>
        public void InsertOrUpdateSummedUpRecordAfterRetest(int SchoolID, int AcademicYearID, IEnumerable<PupilRetestRegistrationSubjectBO> lstPupilRetest)
        {
            //Lấy những phần tử có lstPupilRetest[i].prr.ModifiedDate = null. Tạo List<SummedUpRecord> bởi lstPupilRetest[i].sur => lstSummedUpRecord
            //và gọi hàm SummedUpRecordBusiness.InsertSummedUpRecord(lstSummedUpRecord)

            //Lấy những phần tử có lstPupilRetest[i].prr.ModifiedDate != null. Tạo List<SummedUpRecord> bởi lstPupilRetest[i].sur => lstSummedUpRecord
            //SummedUpRecordBusiness.UpdateSummedUpRecord(lstSummedUpRecord)
            //Lấy từng phần tử trong lstPupilRetest 
            //Thực hiện update ngày cập nhật PupilRetestRegistrationBusiness.UpdatePupilRetestRegistration(lstPupilRetest.prr)

            var lstModifiedNull = lstPupilRetest
                                        .Select(u => new SummedUpRecord
                                        {
                                            AcademicYearID = u.AcademicYearID,
                                            ClassID = u.ClassID,
                                            Comment = u.Comment,
                                            IsCommenting = u.IsCommenting,
                                            JudgementResult = u.JudgementResult,
                                            PeriodID = u.PeriodID,
                                            PupilID = u.PupilID,
                                            ReTestJudgement = u.ReTestJudgement,
                                            ReTestMark = u.ReTestMark,
                                            SchoolID = u.SchoolID,
                                            Semester = u.Semester,
                                            SubjectID = u.SubjectID,
                                            SummedUpDate = DateTime.Now,
                                            SummedUpMark = u.SummedUpMark,
                                            CreatedAcademicYear = u.Year,
                                            SummedUpRecordID = u.SummedUpRecordID
                                        }).ToList();
            this.InsertSummedUpRecordRetest(lstModifiedNull);

            //var lstModifedNotNull = new List<SummedUpRecord>();
            //var lstPupilRetestUpdate = lstPupilRetest.Where(u => u.ModifiedDatePRR.HasValue).ToList();
            //foreach (var sur in lstPupilRetestUpdate)
            //{
            //    var item = this.Find(sur.SummedUpRecordID);
            //    item.Semester = sur.Semester;
            //    item.ReTestJudgement = sur.ReTestJudgement;
            //    item.ReTestMark = sur.ReTestMark;
            //    item.SummedUpDate = DateTime.Now;
            //    lstModifedNotNull.Add(item);
            //}
            //this.UpdateSummedUpRecordRetest(lstModifedNotNull);

            var PupilRetestRegistrationIDs = lstPupilRetest.Select(v => v.PupilRetestRegistrationID).ToArray();
            var PupilRetestRegistrations = this.PupilRetestRegistrationBusiness.All.Where(u => PupilRetestRegistrationIDs.Contains(u.PupilRetestRegistrationID)).ToList();
            PupilRetestRegistrations.ForEach(u => u.ModifiedDate = DateTime.Now);
            //this.PupilRetestRegistrationBusiness.UpdatePupilRetestRegistration(SchoolID, AcademicYearID, lstPrr);
        }

        public IQueryable<SumUpRecordPrimaryBO> GetSummedUpWithRestestOfPrimary(int SchoolID, int AcademicYearID, int Semester, int ClassID, int SubjectID)
        {
            int Year = AcademicYearBusiness.Find(AcademicYearID).Year;
            IQueryable<SumUpRecordPrimaryBO> _query;
            // Danh sach hoc sinh trong lop
            IQueryable<PupilOfClass> listPupilOfClass = PupilOfClassBusiness.SearchBySchool(SchoolID, new Dictionary<string, object>(){
                {"AcademicYearID", AcademicYearID},
                {"ClassID", ClassID},
                //{"Status", SystemParamsInFile.PUPIL_STATUS_STUDYING},
                {"Check", "Check"},
            });
            IQueryable<SummedUpRecordBO> iqSummedUp;
            Dictionary<string, object> dicSummed = new Dictionary<string, object>();
            dicSummed.Add("AcademicYearID", AcademicYearID);
            dicSummed.Add("SubjectID", SubjectID);
            dicSummed.Add("Year", Year);
            dicSummed.Add("SchoolID", SchoolID);
            if (ClassID > 0)
            {
                //iqSummedUp = this.All.Where(u => u.SchoolID == SchoolID && u.AcademicYearID == AcademicYearID
                //&& u.SubjectID == SubjectID && u.ClassID == ClassID);

                dicSummed.Add("ClassID", ClassID);
                iqSummedUp = VSummedUpRecordBusiness.SearchSummedUpRecord(dicSummed);
            }
            else
            {
                iqSummedUp = VSummedUpRecordBusiness.SearchSummedUpRecord(dicSummed);
            }
            if (Semester == SystemParamsInFile.SEMESTER_OF_YEAR_FIRST)
            {

                //kiem tra co hoc ky 1 khong
                //List<ClassSubject> listClassSubject = ClassSubjectBusiness.SearchByClass(ClassID, new Dictionary<string, object>() { { "SubjectID", SubjectID } }).ToList();


                _query = from poc in listPupilOfClass
                         join sur in iqSummedUp on new { poc.ClassID, poc.PupilID, poc.AcademicYearID } equals new { sur.ClassID, sur.PupilID, sur.AcademicYearID } into g1
                         from j1 in g1.DefaultIfEmpty()
                         select new SumUpRecordPrimaryBO
                         {
                             SummedUpRecordID = j1.SummedUpRecordID,
                             PupilID = poc.PupilID,
                             AcademicYearID = poc.AcademicYearID,
                             ClassID = poc.ClassID,
                             SchoolID = poc.SchoolID,
                             SubjectID = SubjectID,
                             Semester = j1.Semester,
                             SummedUpMark = j1.SummedUpMark,
                             JudgementResult = j1.JudgementResult,
                             ReTestMark = j1.ReTestMark,
                             ReTestJudgement = j1.ReTestJudgement,
                             OrderInClass = poc.OrderInClass,
                             Status = poc.Status,
                             PupilCode = "",
                             PupilFullName = "",
                             Comment = j1.Comment,
                             Year = j1.CreatedAcademicYear,
                             IsCommenting = j1.IsCommenting,
                             AssignedDate = poc.AssignedDate
                         };
            }
            else
            {
                IQueryable<SummedUpRecordBO> listSummedUp = iqSummedUp.Where(u => u.Semester >= SystemParamsInFile.SEMESTER_OF_YEAR_SECOND);

                _query = from poc in listPupilOfClass
                             //join pp in PupilProfileBusiness.All on poc.PupilID equals pp.PupilProfileID
                         join sur in listSummedUp on new { poc.ClassID, poc.PupilID, poc.AcademicYearID } equals new { sur.ClassID, sur.PupilID, sur.AcademicYearID } into g1
                         from j1 in g1.DefaultIfEmpty()
                         select new SumUpRecordPrimaryBO
                         {
                             SummedUpRecordID = j1.SummedUpRecordID,
                             PupilID = poc.PupilID,
                             AcademicYearID = poc.AcademicYearID,
                             ClassID = poc.ClassID,
                             SchoolID = poc.SchoolID,
                             SubjectID = SubjectID,
                             Semester = Semester,
                             SummedUpMark = j1.SummedUpMark,
                             JudgementResult = j1.JudgementResult,
                             ReTestMark = j1.ReTestMark,
                             ReTestJudgement = j1.ReTestJudgement,
                             OrderInClass = poc.OrderInClass,
                             Status = poc.Status,
                             PupilCode = "",
                             PupilFullName = "",
                             Comment = j1.Comment,
                             Year = j1.CreatedAcademicYear,
                             IsCommenting = j1.IsCommenting,
                             AssignedDate = poc.AssignedDate
                         };
            }
            return _query;
        }


        /// <summary>
        /// Luu thong phuc vu viec phuc hoi diem TBM
        /// </summary>
        /// <param name="lstSummed"></param>
        /// <returns></returns>
        public List<RESTORE_DATA_DETAIL> BackUpSummedMark(List<SummedUpRecord> lstSummed, RESTORE_DATA objRes)
        {
            if (lstSummed == null || lstSummed.Count == 0)
            {
                return new List<RESTORE_DATA_DETAIL>();
            }

            List<RESTORE_DATA_DETAIL> lstRestoreDetail = new List<RESTORE_DATA_DETAIL>();
            RESTORE_DATA_DETAIL objRestoreDetail;
            ResKeyDelSummedUpBO objKeyMark;
            BakSummedUpBO objResSummed;
            foreach (SummedUpRecord summed in lstSummed)
            {
                //Luu thong tin khoi phuc diem
                objKeyMark = new ResKeyDelSummedUpBO
                {
                    AcademicYearID = summed.AcademicYearID,
                    ClassID = summed.ClassID,
                    PeriodID = summed.PeriodID,
                    PupilID = summed.PupilID,
                    SchoolID = summed.SchoolID,
                    Semester = summed.Semester,
                    SubjectID = summed.SubjectID,
                    IsCommenting = summed.IsCommenting
                };

                objResSummed = new BakSummedUpBO
                {
                    AcademicYearID = summed.AcademicYearID,
                    ClassID = summed.ClassID,
                    Comment = summed.Comment,
                    CreatedAcademicYear = summed.CreatedAcademicYear,
                    IsCommenting = summed.IsCommenting,
                    IsOldData = summed.IsOldData,
                    JudgementResult = summed.JudgementResult,
                    Last2digitNumberSchool = summed.Last2digitNumberSchool,
                    MSourcedb = summed.MSourcedb,
                    M_OldID = summed.M_OldID,
                    M_ProvinceID = summed.M_ProvinceID,
                    PeriodID = summed.PeriodID,
                    PupilID = summed.PupilID,
                    ReTestJudgement = summed.ReTestJudgement,
                    ReTestMark = summed.ReTestMark,
                    SchoolID = summed.SchoolID,
                    Semester = summed.Semester,
                    SubjectID = summed.SubjectID,
                    SummedUpDate = summed.SummedUpDate,
                    SummedUpMark = summed.SummedUpMark,
                    SummedUpRecordID = summed.SummedUpRecordID,
                    SynchronizeID = summed.SynchronizeID,
                    Year = summed.Year
                };
                objRestoreDetail = new RESTORE_DATA_DETAIL()
                {
                    ACADEMIC_YEAR_ID = objRes.ACADEMIC_YEAR_ID,
                    CREATED_DATE = DateTime.Now,
                    END_DATE = null,
                    IS_VALIDATE = 1,
                    LAST_2DIGIT_NUMBER_SCHOOL = summed.Last2digitNumberSchool,
                    ORDER_ID = 2,
                    RESTORE_DATA_ID = objRes.RESTORE_DATA_ID,
                    RESTORE_DATA_TYPE_ID = objRes.RESTORE_DATA_TYPE_ID,
                    SCHOOL_ID = objRes.SCHOOL_ID,
                    SQL_DELETE = JsonConvert.SerializeObject(objKeyMark),
                    SQL_UNDO = JsonConvert.SerializeObject(objResSummed),
                    TABLE_NAME = RestoreDataConstant.TABLE_SUMMED_UP_RECORD
                };
                lstRestoreDetail.Add(objRestoreDetail);
            }
            return lstRestoreDetail;
        }
    }
}
