﻿/**
 * The Viettel License
 *
 * Copyright 2012 Viettel Telecom. All rights reserved.
 * VIETTEL PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */

/** 
* @author  
* @version $Revision: $
*/

using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Linq.Expressions;
using System.Web;
using log4net;

using SMAS.DAL.Repository;
using SMAS.DAL.IRepository;
using SMAS.Business.IBusiness;
using SMAS.Models.Models;
using SMAS.Business.Common;

namespace SMAS.Business.Business
{ 
    public partial class ReportDefinitionBusiness
    {

        public ReportDefinition GetByCode(string code)
        {
            IQueryable<ReportDefinition> lst = ReportDefinitionRepository.All;
            lst = lst.Where(x => x.ReportCode == code);
            var lt = lst.ToList();
            if (lt != null && lt.Count > 0)
            {
                return lt[0];
            }
            else
            {
                throw new BusinessException("Không có loại báo cáo có mã báo cáo là " + code);
            }
        }
    }
}