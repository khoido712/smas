﻿/**
 * The Viettel License
 *
 * Copyright 2012 Viettel Telecom. All rights reserved.
 * VIETTEL PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */

/** 
* @author  
* @version $Revision: $
*/

using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Linq.Expressions;
using System.Web;
using log4net;

using SMAS.DAL.Repository;
using SMAS.DAL.IRepository;
using SMAS.Business.IBusiness;
using SMAS.Models.Models;
using SMAS.Business.Common;

namespace SMAS.Business.Business
{ 
    public partial class DefaultGroupMenuBusiness
    {
        public IQueryable<Menu> GetListMenu(int DefaultGroupID)
        {
            IQueryable<DefaultGroupMenu> groupMenus = DefaultGroupMenuRepository.All.Where(gm => gm.DefaultGroupID == DefaultGroupID);
            if (groupMenus == null) return null;
            List<Menu> menus = new List<Menu>();
            foreach (DefaultGroupMenu gm in groupMenus)
            {
                if (gm.Menu.IsActive)
                {
                    menus.Add(gm.Menu);
                }
            }
            return menus.AsQueryable();
        }

        public IQueryable<DefaultGroupMenu> GetDefaultGroupMenu(int DefaultGroupID)
        {
            if (DefaultGroupID == 0) return null;
            IQueryable<DefaultGroupMenu> groupMenus = DefaultGroupMenuRepository.All.Where(gm => gm.DefaultGroupID == DefaultGroupID);
            return groupMenus;
        }

        /// <summary>
        /// Cập nhật thông tin menu cho nhóm người dùng mặc định. Nhóm người dùng mặc định 
        /// author: namdv3
        /// </summary>
        /// <param name="DefaultGroupID"></param>
        /// <param name="ListMenu"></param>
        /// <returns></returns>
        public DefaultGroup AssignMenu(int DefaultGroupID, IQueryable<DefaultGroupMenu> ListMenu)
        {
            //validation
            //1: Permission chỉ nhận giá trị từ 1-4 hoặc null – Kiểm tra DefaultGroupMenu trong ListMenu
            //2: Không tồn tại chức năng - Kiểm tra MenuID của DefaultGroupMenu trong ListMenu
            //3: Không tồn tại nhóm mặc định này – Kiểm tra DefaultGroupID
            //4: Nhóm này không được phép gán với Menu này – Kiểm tra  cặp DefaultGroup(DefaultGroupID).RoleID – MenuID (trong ListMenu) trong RoleMenu

            //Thực hiện xóa các bản ghi cũ trong DefaultGroupMenu theo DefaultGroupID
            List<DefaultGroupMenu> listDefaultGroupMenu = DefaultGroupMenuBusiness.All.Where(o => o.DefaultGroupID == DefaultGroupID).ToList();
            if (listDefaultGroupMenu != null && listDefaultGroupMenu.Count > 0)
            {
                DefaultGroupMenuBusiness.DeleteAll(listDefaultGroupMenu);
            }
            //Thực hiện insert ListMenu  vào bảng DefaultGroupMenu 
            if (ListMenu != null && ListMenu.Count() > 0)
            {
                foreach (var defaultGroupMenu in ListMenu)
                {
                    DefaultGroupMenuBusiness.Insert(defaultGroupMenu);
                }
            }

            DefaultGroup defaultGroup = DefaultGroupBusiness.Find(DefaultGroupID);
            return defaultGroup;
        }
    }
}