﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Linq.Expressions;
using System.Web;
using log4net;

using SMAS.DAL.Repository;
using SMAS.DAL.IRepository;
using SMAS.Business.IBusiness;
using SMAS.Models.Models;
using SMAS.Business.Common;
using System.IO;
using SMAS.Business.BusinessObject;
using SMAS.VTUtils;
using SMAS.VTUtils.Excel.Export;

namespace SMAS.Business.Business
{
    public partial class ReportSituationOfViolationBusiness
    {
        public ReportDefinition GetReportDefinitionOfPupilFault(ReportSituationOfViolationBO reportSituationOfViolationBO)
        {
            string reportCode = SystemParamsInFile.REPORT_BAO_CAO_HS_VI_PHAM;
            return ReportDefinitionBusiness.GetByCode(reportCode);
        }

        public ReportDefinition GetReportDefinitionOfPupilFaultByClass(ReportSituationOfViolationBO reportSituationOfViolationBO)
        {
            string reportCode = SystemParamsInFile.REPORT_THONG_KE_VI_PHAM;
            return ReportDefinitionBusiness.GetByCode(reportCode);
        }
        public string GetHashKey(ReportSituationOfViolationBO entity)
        {
            IDictionary<string, object> dic = new Dictionary<string, object> {
                {"AcademicYearID", entity.AcademicYearID},
                {"SchoolID", entity.SchoolID},
                {"EducationLevelID", entity.EducationLevelID},
                {"FromDate", entity.FromDate},
                {"ToDate", entity.ToDate},
                {"AppliedLevel", entity.AppliedLevel}
            };
            return ReportUtils.GetHashKey(dic);
        }
        //HỌc sinh vi pham
        public ProcessedReport ExcelGetPupilFault(ReportSituationOfViolationBO entity)
        {
            string reportCode = SystemParamsInFile.REPORT_BAO_CAO_HS_VI_PHAM;
            string inputParameterHashKey = GetHashKey(entity);
            return ProcessedReportBusiness.GetProcessedReport(reportCode, inputParameterHashKey);
        }

        public ProcessedReport ExcelInsertPupilFault(ReportSituationOfViolationBO entity, Stream data)
        {
            string reportCode = SystemParamsInFile.REPORT_BAO_CAO_HS_VI_PHAM;
            ReportDefinition reportDef = ReportDefinitionBusiness.GetByCode(reportCode);
            ProcessedReport pr = new ProcessedReport();
            pr.ReportCode = reportCode;
            pr.ProcessedDate = DateTime.Now;
            pr.InputParameterHashKey = GetHashKey(entity);
            pr.ReportData = ReportUtils.Compress(data);

            //Tạo tên file
            string outputNamePattern = reportDef.OutputNamePattern;
            string schoolLevel = ReportUtils.ConvertAppliedLevelForReportName(entity.AppliedLevel);
            EducationLevel educationLevel = EducationLevelRepository.Find(entity.EducationLevelID);
            outputNamePattern = outputNamePattern.Replace("[SchoolLevel]", schoolLevel);
            outputNamePattern = outputNamePattern.Replace("[EducationLevel]", educationLevel.Resolution);
            pr.ReportName = ReportUtils.RemoveSpecialCharacters(outputNamePattern) + "." + reportDef.OutputFormat;


            IDictionary<string, object> dic = new Dictionary<string, object> {
                {"AcademicYearID", entity.AcademicYearID},
                {"SchoolID", entity.SchoolID},
                {"AppliedLevel", entity.AppliedLevel}
            };
            ProcessedReportParameterRepository.Insert(dic, pr);
            ProcessedReportRepository.Insert(pr);
            ProcessedReportRepository.Save();
            return pr;
        }

        public Stream ExcelCreatePupilFault(ReportSituationOfViolationBO entity)
        {
            AcademicYear academicYear = AcademicYearRepository.Find(entity.AcademicYearID);
            if (entity.FromDate > entity.ToDate)
            {
                throw new BusinessException("ReportSituationOfViolation_Label_FromDateToDate");
            }
            else if (entity.ToDate > DateTime.Now)
            {
                throw new BusinessException("ReportSituationOfViolation_Label_ToDateNow");

            }

            else if (entity.FromDate < academicYear.FirstSemesterStartDate || entity.FromDate > academicYear.SecondSemesterEndDate || entity.ToDate < academicYear.FirstSemesterStartDate || entity.ToDate > academicYear.SecondSemesterEndDate)
            {
                throw new BusinessException("ReportSituationOfViolation_Label_FromDateAcademicYear");
            }
            string reportCode = SystemParamsInFile.REPORT_BAO_CAO_HS_VI_PHAM;
            ReportDefinition reportDef = ReportDefinitionBusiness.GetByCode(reportCode);
            string templatePath = SystemParamsInFile.TEMPLATE_FOLDER + reportDef.TemplateDirectory + "/" + reportDef.TemplateName;
            IVTWorkbook oBook = VTExport.OpenWorkbook(templatePath);

            //Lấy sheet thống kê học sinh
            int firstRow = 8;

            IVTWorksheet firstSheet = oBook.GetSheet(1);
            IVTWorksheet sheet = oBook.CopySheetToLast(firstSheet, "F" + (firstRow));
            sheet.Name = "BaoCaoHSViPham";
            EducationLevel educationLevel = EducationLevelRepository.Find(entity.EducationLevelID);

            //fill dữ liệu vào sheet thông tin chung
            SchoolProfile school = SchoolProfileBusiness.Find(entity.SchoolID);
            sheet.SetCellValue("A2", school.SchoolName.ToUpper());
            sheet.SetCellValue("A5", educationLevel.Resolution);

            string fromDate = entity.FromDate.ToString();

            string date = "Từ ngày " + entity.FromDate.ToShortDateString() + " Đến ngày " + entity.ToDate.ToShortDateString();
            sheet.SetCellValue("A6", date);


            VTVector cellOrderNumber = new VTVector("A9");
            VTVector cellClassName = new VTVector("B9");
            VTVector cellFullName = new VTVector("C9");
            VTVector cellViolationDate = new VTVector("D9");
            VTVector cellViolationInfo = new VTVector("E9");
            VTVector cellMark = new VTVector("F9");

            int countRow = 0;
            IVTRange topRow = firstSheet.GetRange("A10", "F10");

            IVTRange middleRow = firstSheet.GetRange("A11", "F11");
            IVTRange bottomRow = firstSheet.GetRange("A13", "F13");


            List<ReportPupilFaultBO> lstPupilFault = GetListPupilFault(entity).ToList();
            List<int> lstClass = lstPupilFault.Select(x => x.ClassID).Distinct().ToList();
            List<ClassProfile> lstClassProfile = ClassProfileBusiness.All.Where(o => lstClass.Contains(o.ClassProfileID)).OrderBy(o => o.OrderNumber).ThenBy(o => o.DisplayName).ToList();
            int rowFirst = 9;
            VTVector vtIndex;
            int currentRow;
            ReportPupilFaultBO objPupilFaultBO;
            //Chiendd:tuning 30.12.2014
            List<FaultCriteriaBO> lstFaultCriteriaBO;
            List<FaultCriteria> lstFaultCriteria = FaultCriteriaRepository.All.Where(c => c.SchoolID == entity.SchoolID).ToList();

            //Lay danh sach vi pham trong khoi
            var queryFaultList = (from p in PupilFaultRepository.All.Where(c => c.AcademicYearID == entity.AcademicYearID && c.EducationLevelID == entity.EducationLevelID)
                                  join s in FaultCriteriaRepository.All on p.FaultID equals s.FaultCriteriaID
                                 select p).ToList();

            //lstFaultCriteriaBO = queryFaultList.ToList();

            for (int i = 0; i < lstClassProfile.Count(); i++)
            {
                ClassProfile className = lstClassProfile[i];
                List<ReportPupilFaultBO> lstPupilFaultByClass = lstPupilFault.Where(x => x.ClassID == className.ClassProfileID).ToList();
                lstPupilFaultByClass = lstPupilFaultByClass.OrderBy(o => o.OrderInClass).ThenBy(o => o.Name).ThenBy(o => o.FullName).ThenByDescending(o => o.ViolateDate).ToList();

                for (int j = 0; j < lstPupilFaultByClass.Count(); j++)
                {
                    vtIndex = new VTVector(countRow, 0);
                    currentRow = 9 + countRow;
                    if (j == 0)
                    {
                        sheet.CopyPasteSameRowHeigh(topRow, currentRow);
                    }
                    if (j == (lstPupilFaultByClass.Count() - 1))
                    {
                        sheet.CopyPasteSameRowHeigh(bottomRow, currentRow);
                    }
                    if (j != 0 && j != (lstPupilFaultByClass.Count() - 1))
                    {
                        sheet.CopyPasteSameRowHeigh(middleRow, currentRow);
                    }
                    objPupilFaultBO = lstPupilFaultByClass[j];
                    //PupilProfile pupilProfile = PupilProfileRepository.Find(lstPupilFaultByClass[j].PupilID);
                    sheet.SetCellValue(cellOrderNumber + vtIndex, (countRow + 1));
                    //sheet.SetCellValue(cellClassName + vtIndex, className.DisplayName);
                    sheet.SetCellValue(cellFullName + vtIndex, objPupilFaultBO.FullName);
                    sheet.SetCellValue(cellViolationDate + vtIndex, objPupilFaultBO.ViolateDate);
                    lstFaultCriteriaBO = (from p in queryFaultList
                                          where p.PupilID == objPupilFaultBO.PupilID.Value
                                             && p.ViolatedDate == objPupilFaultBO.ViolateDate
                                        group p by new {  p.FaultID } into g
                                         select new FaultCriteriaBO
                                         {
                                             FaultCriteriaID = g.Key.FaultID,

                                             NumberFault = g.Sum(p => p.NumberOfFault)
                                         }).ToList();
                    sheet.SetCellValue(cellViolationInfo + vtIndex, FaultName(entity, objPupilFaultBO.PupilID.Value, objPupilFaultBO.ViolateDate, lstFaultCriteriaBO, lstFaultCriteria));
                    sheet.SetCellValue(cellMark + vtIndex, objPupilFaultBO.NewField);
                    countRow++;
                }
                int numberOfFault = lstPupilFaultByClass.Count();
                sheet.GetRange(rowFirst, 2, rowFirst + numberOfFault - 1, 2).Merge();
                sheet.GetRange(rowFirst, 2, rowFirst + numberOfFault - 1, 2).Value = className.DisplayName;
                rowFirst = rowFirst + numberOfFault;

            }
            //ke khung
            sheet.GetRange(9, 1, lstPupilFault.Count + 8, 6).SetBorder(VTBorderStyle.Solid, VTBorderWeight.Thin, VTBorderIndex.All);
            firstSheet.Delete();
            return oBook.ToStream();

        }
        public List<ReportPupilFaultBO> GetListPupilFault(ReportSituationOfViolationBO entity)
        {
            Dictionary<string, object> dic = new Dictionary<string, object>
            {
                {"AcademicYearID", entity.AcademicYearID},
                {"SchoolID", entity.SchoolID},
                {"AppliedLevel", entity.AppliedLevel}
            };
            IQueryable<PupilOfClass> listPupilAll = PupilOfClassBusiness.SearchBySchool(entity.SchoolID, dic);
            IQueryable<PupilOfClass> listPupil = listPupilAll.Where(o => o.AssignedDate == listPupilAll.Where(u => u.PupilID == o.PupilID && u.ClassID == o.ClassID).Select(u => u.AssignedDate).Max());
            var query = from p in PupilFaultRepository.All
                        join q in PupilProfileRepository.All on p.PupilID equals q.PupilProfileID
                        join r in listPupil on new { p.PupilID, p.ClassID } equals new { r.PupilID, r.ClassID }
                        where p.AcademicYearID == entity.AcademicYearID
                        && p.SchoolID == entity.SchoolID
                        && p.EducationLevelID == entity.EducationLevelID
                        && p.ViolatedDate >= entity.FromDate
                        && p.ViolatedDate <= entity.ToDate
                        group p by new { p.PupilID, p.ViolatedDate, p.ClassID, q.Name, q.FullName, r.OrderInClass } into g
                        select new ReportPupilFaultBO
                        {
                            PupilID = g.Key.PupilID,
                            ViolateDate = g.Key.ViolatedDate,
                            ClassID = g.Key.ClassID,
                            Name = g.Key.Name,
                            OrderInClass = g.Key.OrderInClass,
                            FullName = g.Key.FullName,
                            NewField = g.Sum(k => k.TotalPenalizedMark)
                        };
            List<ReportPupilFaultBO> lstReportFault = query.ToList();
            return lstReportFault;
        }

        public string FaultName(ReportSituationOfViolationBO entity, int pupilID, DateTime? ViolateDate, List<FaultCriteriaBO> lstFaultCriteriaBO, List<FaultCriteria> lstFaultCriteria)
        {
            if (lstFaultCriteriaBO == null || lstFaultCriteriaBO.Count == 0)
                        {
                return "";
            }
            string str = "";
            string name = "";
            int j = 0;
            FaultCriteria objFaultCriteria;
            for (int i = 0; i < (lstFaultCriteriaBO.Count()); i++)
            {
                objFaultCriteria = lstFaultCriteria.Where(c => c.FaultCriteriaID == lstFaultCriteriaBO[i].FaultCriteriaID).FirstOrDefault();
                name = (objFaultCriteria == null) ? "" : objFaultCriteria.Resolution;
                if (j < (lstFaultCriteriaBO.Count() - 1))
                {
                    if (lstFaultCriteriaBO[i].NumberFault == 1)
                    {
                        str += name + " - ";
                        j++;
                    }
                    else
                    {
                        str += name + "(" + lstFaultCriteriaBO[i].NumberFault + ") - ";
                        j++;
                    }
                }
                else if (j == (lstFaultCriteriaBO.Count() - 1))
                {
                    if (lstFaultCriteriaBO[i].NumberFault == 1)
                    {
                        str += name;
                    }
                    else
                    {
                        str += name + "(" + lstFaultCriteriaBO[i].NumberFault + ")";
                    }
                }
            }
            return str;
        }

        //thống kê vi phạm
        public ProcessedReport ExcelGetPupilFaultByClass(ReportSituationOfViolationBO entity)
        {
            string reportCode = SystemParamsInFile.REPORT_THONG_KE_VI_PHAM;
            string inputParameterHashKey = GetHashKey(entity);
            return ProcessedReportBusiness.GetProcessedReport(reportCode, inputParameterHashKey);
        }

        public ProcessedReport ExcelInsertPupilFaultByClass(ReportSituationOfViolationBO entity, Stream data)
        {
            string reportCode = SystemParamsInFile.REPORT_THONG_KE_VI_PHAM;
            ReportDefinition reportDef = ReportDefinitionBusiness.GetByCode(reportCode);
            ProcessedReport pr = new ProcessedReport();
            pr.ReportCode = reportCode;
            pr.ProcessedDate = DateTime.Now;
            pr.InputParameterHashKey = GetHashKey(entity);
            pr.ReportData = ReportUtils.Compress(data);

            //Tạo tên file
            string outputNamePattern = reportDef.OutputNamePattern;
            string schoolLevel = ReportUtils.ConvertAppliedLevelForReportName(entity.AppliedLevel);
            EducationLevel educationLevel = EducationLevelRepository.Find(entity.EducationLevelID);
            outputNamePattern = outputNamePattern.Replace("[SchoolLevel]", schoolLevel);
            outputNamePattern = outputNamePattern.Replace("[EducationLevel]", educationLevel.Resolution);
            pr.ReportName = ReportUtils.RemoveSpecialCharacters(outputNamePattern) + "." + reportDef.OutputFormat;


            IDictionary<string, object> dic = new Dictionary<string, object> {
                {"AcademicYearID", entity.AcademicYearID},
                {"SchoolID", entity.SchoolID},
                {"AppliedLevel", entity.AppliedLevel}
            };
            ProcessedReportParameterRepository.Insert(dic, pr);
            ProcessedReportRepository.Insert(pr);
            ProcessedReportRepository.Save();
            return pr;
        }

        public Stream ExcelCreatePupilFaultByClass(ReportSituationOfViolationBO entity)
        {
            AcademicYear academicYear = AcademicYearRepository.Find(entity.AcademicYearID);
            if (entity.FromDate > entity.ToDate)
            {
                throw new BusinessException("ReportSituationOfViolation_Label_FromDateToDate");
            }
            else if (entity.ToDate > DateTime.Now)
            {
                throw new BusinessException("ReportSituationOfViolation_Label_ToDateNow");

            }

            else if (entity.FromDate < academicYear.FirstSemesterStartDate || entity.FromDate > academicYear.SecondSemesterEndDate || entity.ToDate < academicYear.FirstSemesterStartDate || entity.ToDate > academicYear.SecondSemesterEndDate)
            {
                throw new BusinessException("ReportSituationOfViolation_Label_FromDateAcademicYear");
            }
            string reportCode = SystemParamsInFile.REPORT_THONG_KE_VI_PHAM;
            ReportDefinition reportDef = ReportDefinitionBusiness.GetByCode(reportCode);
            string templatePath = SystemParamsInFile.TEMPLATE_FOLDER + reportDef.TemplateDirectory + "/" + reportDef.TemplateName;
            IVTWorkbook oBook = VTExport.OpenWorkbook(templatePath);

            //Lấy sheet thống kê học sinh
            IVTWorksheet firstSheet = oBook.GetSheet(1);
            int firstRow = 7;
            int firstDynamicCol = VTVector.dic['D'];
            IVTRange classTemp = firstSheet.GetRange("D" + firstRow, "D" + (firstRow));
            IVTRange faultTemp = firstSheet.GetRange("D" + (firstRow + 1), "D" + (firstRow + 1));
            IVTRange faultNameRange = firstSheet.GetRange("B8", "B8");
            IVTRange mergeCell = firstSheet.GetRange("A" + (firstRow + 2), "B" + (firstRow + 2));
            IVTWorksheet sheet = oBook.CopySheetToLast(firstSheet, "E" + (firstRow - 1));
            sheet.CopyPasteSameSize(firstSheet.GetRange("A" + firstRow, "C" + (firstRow + 1)), "A" + firstRow);
            sheet.Name = "TKViPham";
            EducationLevel educationLevel = EducationLevelRepository.Find(entity.EducationLevelID);

            //fill dữ liệu vào sheet thông tin chung
            SchoolProfile school = SchoolProfileBusiness.Find(entity.SchoolID);
            sheet.SetCellValue("A2", school.SchoolName.ToUpper());

            string fromDate = entity.FromDate.ToString();




            VTVector cellOrderNumber = new VTVector("A8");
            VTVector cellFaultName = new VTVector("B8");
            VTVector cellMinusMark = new VTVector("C8");
            VTVector cellClass = new VTVector("D7");
            VTVector cellClassValue = new VTVector("D8");

            int countRow = 0;
            int countColumn = 0;

            List<FaultCriteria> listFaultCriteria;
            IDictionary<string, object> dic = new Dictionary<string, object>();
            listFaultCriteria = FaultCriteriaBusiness.SearchBySchool(
                academicYear.SchoolID, dic).ToList();
            IDictionary<string, object> dicEdu = new Dictionary<string, object>();
            dicEdu["EducationLevelID"] = entity.EducationLevelID;
            dicEdu["AcademicYearID"] = entity.AcademicYearID;
            List<ClassProfile> listClassProfile = ClassProfileBusiness.SearchBySchool(entity.SchoolID, dicEdu).ToList();
            string date = "Từ ngày " + entity.FromDate.ToShortDateString() + " Đến ngày " + entity.ToDate.ToShortDateString();
            sheet.MergeRow(5, 1, 4 + listClassProfile.Count());
            sheet.SetCellValue("A5", date);
            string nameReport = "BÁO CÁO VI PHẠM " + educationLevel.Resolution;
            sheet.MergeRow(4, 1, 4 + listClassProfile.Count());
            sheet.SetCellValue("A4", nameReport);
            int i = 0;
            foreach (ClassProfile classProfile in listClassProfile)
            {

                VTVector vtIndexColumn = new VTVector(0, countColumn);
                sheet.CopyPasteSameSize(classTemp, firstRow, firstDynamicCol + i);
                sheet.SetCellValue(cellClass + vtIndexColumn, classProfile.DisplayName);
                countColumn++;
                i++;
            }
            sheet.CopyPasteSameSize(classTemp, firstRow, firstDynamicCol + i);
            sheet.SetCellValue(firstRow, firstDynamicCol + i, "Tổng");
            //Thực hiện việc for theo lỗi vi phạm
            Dictionary<string, object> search = new Dictionary<string, object>();
            search["AcademicYearID"] = entity.AcademicYearID;
            search["FromViolatedDate"] = entity.FromDate;
            search["ToViolatedDate"] = entity.ToDate;
            IQueryable<PupilFault> iqPupilFault = PupilFaultBusiness.SearchBySchool(entity.SchoolID, search);
            var listFault = from s in listFaultCriteria
                                join m in iqPupilFault on s.FaultCriteriaID equals m.FaultID
                                select new
                                {
                                    s.FaultCriteriaID,
                                    m.ClassID,
                                    m.TotalPenalizedMark,
                                };

            var listFaultGroup = (from g in listFault
                                      group g by new
                                      {
                                          g.FaultCriteriaID,
                                          g.ClassID,
                                      } into c
                                      select new
                                      {
                                          c.Key.FaultCriteriaID,
                                          c.Key.ClassID,
                                          SumMark = c.Sum(o => o.TotalPenalizedMark)
                                      }
                                     ).ToList();


            foreach (FaultCriteria faultCriteria in listFaultCriteria)
            {
                VTVector vtIndexRow = new VTVector(countRow, 0);
                int currentRow = 8 + countRow;

                int countCol = 0;
                sheet.CopyPasteSameSize(faultTemp, currentRow, 1);
                sheet.SetCellValue(cellOrderNumber + vtIndexRow, (countRow + 1));
                sheet.CopyPasteSameSize(faultNameRange, currentRow, 2);
                sheet.SetCellValue(cellFaultName + vtIndexRow, faultCriteria.Resolution);
                sheet.CopyPasteSameSize(faultTemp, currentRow, 3);
                sheet.SetCellValue(cellMinusMark + vtIndexRow, faultCriteria.PenalizedMark);
                //Đối với mỗi loại vi phạm thì thực hiện for theo lớp
                foreach (ClassProfile classProfile in listClassProfile)
                {
                    VTVector vtIndexColumn = new VTVector(countRow, countCol);
                    sheet.CopyPasteSameSize(faultTemp, currentRow, countCol + 4);
                    var totalFaultMark = listFaultGroup.Where(o => o.ClassID == classProfile.ClassProfileID && o.FaultCriteriaID == faultCriteria.FaultCriteriaID).FirstOrDefault();
                    sheet.SetCellValue(cellClassValue + vtIndexColumn, totalFaultMark != null ? totalFaultMark.SumMark : 0);
                    countCol++;

                }
                //Thực hiện tính tổng điểm trừ theo từng lỗi vi phạm
                string str = VTVector.ColumnIntToString(countCol + 3);
                string fomula = "=SUM(D" + currentRow + ":" + str + currentRow + ")";

                sheet.CopyPasteSameSize(classTemp, currentRow, countCol + 4);
                sheet.SetFormulaValue(currentRow, countCol + 4, fomula);

                countRow++;
            }
            //int lastCol = countCol;
            int lastRow = (countRow == 0) ? 1 : countRow + 8;
            //Tính tổng điểm trừ theo từng lớp 
            sheet.CopyPasteSameSize(classTemp, lastRow, 3);
            //sheet.SetFormulaValue(lastRow, 3, "=SUM(C"+ (firstRow + 1) +":"+ "C" + (firstRow + listFaultCriteria.Count())+")");
            sheet.SetFormulaValue(lastRow, 3, "");
            int j = 0;
            foreach (ClassProfile classProfile in listClassProfile)
            {

                string alphabet = VTVector.ColumnIntToString(j + 4);
                string fomular = "=SUM(#" + (firstRow + 1) + ":" + "#" + (firstRow + listFaultCriteria.Count()) + ")";
                sheet.CopyPasteSameSize(classTemp, lastRow, j + 4);
                if (listFaultCriteria.Count() != 0)
                sheet.SetFormulaValue(lastRow, j + 4, fomular.Replace("#", alphabet));
                j++;
            }

            //Tính tổng điểm vi phạm
            string alpha = VTVector.ColumnIntToString(4 + listClassProfile.Count());
            sheet.CopyPasteSameSize(mergeCell, lastRow, 1);
            sheet.SetCellValue(lastRow, 1, "Tổng điểm vi phạm");
            sheet.CopyPasteSameSize(classTemp, lastRow, 4 + listClassProfile.Count());
            if (listFaultCriteria.Count() != 0)
                sheet.SetFormulaValue(lastRow, 4 + listClassProfile.Count(), "=SUM(" + alpha + (firstRow + 1) + ":" + alpha + (firstRow + listFaultCriteria.Count()) + ")");
            sheet.FitToPage = true;
            firstSheet.Delete();
            return oBook.ToStream();

        }
        //Tính điểm trừ theo lớp trong một khoảng thời gian từ entity.FromDate => entity.ToDate
        public decimal? CalculateMinusMarkByClass(ReportSituationOfViolationBO entity, int classID, int faultCriteriaID)
        {
            IQueryable<PupilFault> query = from p in PupilFaultRepository.All
                              where p.ClassID == classID && p.FaultID == faultCriteriaID
                              && p.ViolatedDate >= entity.FromDate
                              && p.ViolatedDate <= entity.ToDate
                              select p;

            decimal? result = query.ToList().Sum(p => p.TotalPenalizedMark);
            return result;
        }
    }
}
