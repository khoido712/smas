﻿/**
 * The Viettel License
 *
 * Copyright 2012 Viettel Telecom. All rights reserved.
 * VIETTEL PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */

/** 
* @author  
* @version $Revision: $
*/

using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Linq.Expressions;
using System.Web;
using log4net;

using SMAS.DAL.Repository;
using SMAS.DAL.IRepository;
using SMAS.Business.IBusiness;
using SMAS.Models.Models;
using SMAS.Business.Common;

namespace SMAS.Business.Business
{
    public partial class HistorySMSBusiness
    {
        #region Validate
        public void CheckHistorySMSIDAvailable(int historySMSID)
        {
            bool AvailableHistorySMSID = this.repository.ExistsRow(GlobalConstants.TRS_SCHEMA, "HistorySMS",
                   new Dictionary<string, object>()
                {
                    {"HistorySMSID", historySMSID}
                    
                }, null);
            if (!AvailableHistorySMSID)
            {
                throw new BusinessException("Common_Validate_NotAvailable");
            }
        }
        public void ValidateValue(HistorySMS historySMS)
        {
            if (historySMS.TypeID != GlobalConstants.HISTORYSMS_TO_TEACHER_ID &&
                historySMS.TypeID != GlobalConstants.HISTORYSMS_TO_SEMESTERSMS_ID &&
                historySMS.TypeID != GlobalConstants.HISTORYSMS_TO_ACTIVITY_ID &&
                historySMS.TypeID != GlobalConstants.HISTORYSMS_TO_GROWTH_ID &&
                historySMS.TypeID != GlobalConstants.HISTORYSMS_TO_COMMUNICATION_ID &&
                historySMS.TypeID != GlobalConstants.HISTORYSMS_TO_DAYMARK_ID &&
                historySMS.TypeID != GlobalConstants.HISTORYSMS_TO_WEEKMARK_ID &&
                historySMS.TypeID != GlobalConstants.HISTORYSMS_TO_HEALTH_ID &&
                historySMS.TypeID != GlobalConstants.HISTORYSMS_TO_ACTIVE_ID &&
                historySMS.TypeID != GlobalConstants.HISTORYSMS_TO_TRAINING_ID &&
                historySMS.TypeID != GlobalConstants.HISTORYSMS_TO_MONTHSMS_ID
               )
            {
                List<object> listParam = new List<object>();
                listParam.Add("HistorySMS_Label_TypeID");
                throw new BusinessException("Common_Validate_HistorySMSNumber", listParam);
            }
            if (
                historySMS.ReceiveType != GlobalConstants.HISTORY_RECEIVER_PUPIL_ID
                && historySMS.ReceiveType != GlobalConstants.HISTORY_RECEIVER_PARENT_ID
                && historySMS.ReceiveType != GlobalConstants.HISTORY_RECEIVER_TEACHER_ID                             
                )
            {
                List<object> listParam = new List<object>();
                listParam.Add("HistorySMS_Label_ReceiveType");
                throw new BusinessException("Common_Validate_HistorySMSNumber", listParam);
            }
            if (historySMS.FlagID != null && historySMS.FlagID < 0)
            {
                List<object> listParam = new List<object>();
                listParam.Add("HistorySMS_Label_FlagID");
                throw new BusinessException("Common_Validate_HistorySMSNumber", listParam);
            }
        }
        #endregion
        #region Insert
        //public HistorySMS Insert(HistorySMS historySMS)
        //{
        //    //Check Require, Max Length
        //    ValidationMetadata.ValidateObject(historySMS);

        //    // Check value of ReceiveType, TypeID
        //    ValidateValue(historySMS);

        //    historySMS.CreateDate = DateTime.Now;

        //    return base.Insert(historySMS);

        //}
        public void InsertHistorySMS(List<HistorySMS> lsHistorySMS)
        {
            if (lsHistorySMS.Count() > 0)
            {
                for (int i = 0; i < lsHistorySMS.Count(); i++)
                {
                    Insert(lsHistorySMS[i]);
                }
            }
            else
            {
                throw new BusinessException("Common_Validate_HistorySMSInsertLS");
            }
        }
        #endregion
        #region Update
        public HistorySMS Update(HistorySMS HistorySMS)
        {
            //Check Require, Max Length
            ValidationMetadata.ValidateObject(HistorySMS);

            // Check value of ReceiveType, TypeID
            ValidateValue(HistorySMS);

            return base.Update(HistorySMS);
        }
        #endregion
        #region Delete
        public bool Delete(int HistorySMSID)
        {
            try
            {
                //Check available
                CheckHistorySMSIDAvailable(HistorySMSID);

                //Check Using
                this.CheckConstraints(GlobalConstants.TRS_SCHEMA, "HistorySMS", HistorySMSID, "HistorySMS_Label_HistorySMSID");

                base.Delete(HistorySMSID);
                return true;
            }
            catch
            {
                return false;
            }

        }
        #endregion
        #region Search
        /// <summary>
        /// Tìm kiếm Lịch sử gửi tin nhắn
        /// </summary>
        /// <author>BaLX</author>
        /// <date>11/12/2012</date>
        /// <param name="ethnic">Đối tượng HistorySMS</param>
        /// <returns>Đối tượng HistorySMS</returns>

        public IQueryable<HistorySMS> Search(IDictionary<string, object> dic)
        {
            int SchoolID = Utils.GetInt(dic, "SchoolID");
            int Year = Utils.GetInt(dic, "Year");
            string Mobile = Utils.GetString(dic, "Mobile");
            int? TypeID = Utils.GetNullableInt(dic, "TypeID");
            int? PupilID = Utils.GetNullableInt(dic, "PupilID");
            int? ReceiveType = Utils.GetNullableInt(dic, "ReceiveType");
            int? Status = Utils.GetNullableInt(dic, "Status");
            DateTime? FromDate = Utils.GetDateTime(dic, "FromDate");
            DateTime? ToDate = Utils.GetDateTime(dic, "ToDate");
            DateTime? CreateDate = Utils.GetDateTime(dic, "CreateDate");

            // bat dau tìm kiem
            var query = from h in HistorySMSRepository.All where h.PupilID == PupilID && h.TypeID == TypeID select h;
            if (SchoolID != 0)
            {
                query = query.Where(m => (m.SchoolID == SchoolID));
            }
            if (Year > 0)
            {
                query = query.Where(m => (m.Year == Year));
            }
            if (Mobile.Trim().Length != 0)
            {
                query = query.Where(m => (m.Mobile.ToUpper().Contains(Mobile.ToUpper())));

            }
            if (TypeID.HasValue)
            {
                query = query.Where(m => (m.TypeID == TypeID));
            }
            if (ReceiveType.HasValue)
            {
                query = query.Where(m => (m.ReceiveType == ReceiveType));
            }
            if (Status.HasValue)
            {
                if (Status.Value == 0)
                    query = query.Where(m => (m.Status == false));
                else if (Status.Value == 1)
                    query = query.Where(m => (m.Status == true));
            }
            if (FromDate.HasValue && ToDate.HasValue)
            {
                query = query.Where(m => (m.CreateDate >= FromDate && m.CreateDate <= ToDate));

            }
            if (FromDate.HasValue && !ToDate.HasValue)
            {
                query = query.Where(m => (m.CreateDate >= FromDate));

            }
            if (!FromDate.HasValue && ToDate.HasValue)
            {
                query = query.Where(m => (m.CreateDate <= ToDate));

            }
            if (CreateDate.HasValue)
            {
                query = query.Where(m => (m.CreateDate.Value.Day == CreateDate.Value.Day && m.CreateDate.Value.Month == CreateDate.Value.Month
                    && m.CreateDate.Value.Year == CreateDate.Value.Year));
            }
            return query;
        }
        #endregion

    }
}