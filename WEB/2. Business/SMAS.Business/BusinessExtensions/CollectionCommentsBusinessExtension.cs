﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Linq.Expressions;
using System.Web;
using log4net;
using SMAS.DAL.Repository;
using SMAS.DAL.IRepository;
using SMAS.Business.IBusiness;
using SMAS.Models.Models;
using SMAS.Business.Common;
using SMAS.Business.BusinessObject;

namespace SMAS.Business.Business
{ 
    public partial class CollectionCommentsBusiness
    {
        #region Collection Comment
        public IQueryable<CollectionComments> SearchCollectionComment(IDictionary<string, object> dic)
        {
            int SchoolID = Utils.GetInt(dic, "SchoolID");
            int AcademicYearID = Utils.GetInt(dic, "AcademicYearID");
            int SubjectID = Utils.GetInt(dic, "SubjectID");
            int AccountID = Utils.GetInt(dic, "AccountID");
            List<long> lstCollectionCommentID = Utils.GetLongList(dic, "lstCollectionCommentID");
            IQueryable<CollectionComments> iquery = this.CollectionCommentsBusiness.All;
            if (lstCollectionCommentID.Count > 0)
            {
                iquery = iquery.Where(p => lstCollectionCommentID.Contains(p.CollectionCommentsID));
            }
            if (SchoolID != 0)
            {
                iquery = iquery.Where(p => p.SchoolID == SchoolID);
            }
            if (AcademicYearID != 0)
            {
                iquery = iquery.Where(p => p.AcademicYearID == AcademicYearID);
            }
            if (SubjectID != 0)
            {
                iquery = iquery.Where(p => p.SubjectID == SubjectID);
            }
            if (AccountID != 0)
            {
                iquery = iquery.Where(p => p.AccountID == AccountID);
            }
            return iquery;
        }
        public void InsertOrUpdateCollectionComment(List<CollectionComments> lstCollectionComment, IDictionary<string, object> dic)
        {
            List<CollectionComments> lstDB = this.SearchCollectionComment(dic).ToList();
            List<CollectionComments> lstInsert = new List<CollectionComments>();
            CollectionComments objInsert = null;
            CollectionComments objDB = null;
            for (int i = 0; i < lstCollectionComment.Count; i++)
            {
                objInsert = lstCollectionComment[i];
                objDB = lstDB.Where(p => p.CollectionCommentsID == objInsert.CollectionCommentsID).FirstOrDefault();
                if (objDB != null)
                {
                    objDB.CommentCode = objInsert.CommentCode;
                    objDB.Comment = objInsert.Comment;
                    objDB.UpdateTime = DateTime.Now;
                    CollectionCommentsBusiness.Update(objDB);
                }
                else
                {
                    objInsert.CreateTime = DateTime.Now;
                    lstInsert.Add(objInsert);
                }
            }
            if (lstInsert.Count > 0)
            {
                for (int i = 0; i < lstInsert.Count; i++)
                {
                    CollectionCommentsBusiness.Insert(lstInsert[i]);
                }
            }
            CollectionCommentsBusiness.Save();
        }
        #endregion
    }
}