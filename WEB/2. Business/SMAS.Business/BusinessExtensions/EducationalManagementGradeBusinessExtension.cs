﻿/**
 * The Viettel License
 *
 * Copyright 2012 Viettel Telecom. All rights reserved.
 * VIETTEL PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */

/** 
* @author  dungnt
* @version $Revision: $
*/

using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Linq.Expressions;
using System.Web;
using log4net;

using SMAS.DAL.Repository;
using SMAS.DAL.IRepository;
using SMAS.Business.IBusiness;
using SMAS.Models.Models;
using SMAS.Business.Common;

namespace SMAS.Business.Business
{
    /// <summary>
    /// Trình độ quản lý giáo dục
    /// <author>dungnt</author>
    /// <date>05/09/2012</date>
    /// </summary>
    public partial class EducationalManagementGradeBusiness
    {


        #region private member variable
        private const int RESOLUTION_MAX_LENGTH = 50;   //do dai lon nhat truong ten
        private const int DESCRIPTION_MAX_LENGTH = 400; //do dai lon nhat truong mieu ta
        #endregion



        #region insert
        /// <summary>
        /// Thêm mới Trình độ quản lý giáo dục
        /// <author>dungnt</author>
        /// <date>05/09/2012</date>
        /// </summary>
        /// <param name="insertEducationalManagementGrade">doi tuong can them moi</param>
        /// <returns></returns>
        public override EducationalManagementGrade Insert(EducationalManagementGrade insertEducationalManagementGrade)
        {
            ValidationMetadata.ValidateObject(insertEducationalManagementGrade);

            //resolution rong
            //Utils.ValidateRequire(insertEducationalManagementGrade.Resolution, "EducationalManagementGrade_Label_Resolution");
            //Utils.ValidateMaxLength(insertEducationalManagementGrade.Resolution, RESOLUTION_MAX_LENGTH, "EducationalManagementGrade_Label_Resolution");
            //Utils.ValidateMaxLength(insertEducationalManagementGrade.Description, DESCRIPTION_MAX_LENGTH, "EducationalManagementGrade_Column_Description");

            //kiem tra ton tai - theo cap - can lam lai
            //this.CheckDuplicate(insertEducationalManagementGrade.Resolution, GlobalConstants.LIST_SCHEMA, "EducationalManagementGrade", "Resolution", true, 0, "EducationalManagementGrade_Label_Resolution");
            //this.CheckDuplicate(insertEducationalManagementGrade.SchoolID.ToString(), GlobalConstants.LIST_SCHEMA, "EducationalManagementGrade", "AppliedLevel", true, 0, "EducationalManagementGrade_Label_ShoolID");
           // this.CheckDuplicateCouple(insertEducationalManagementGrade.Resolution, insertEducationalManagementGrade.SchoolID.ToString(),
           //GlobalConstants.LIST_SCHEMA, "EducationalManagementGrade", "Resolution", "SchoolID", true, 0, "EducationalManagementGrade_Label_Resolution");
            //kiem tra range
            // Check Duplicate - DungVA 2608

            IDictionary<string, object> expDic = null;
            if (insertEducationalManagementGrade.EducationalManagementGradeID > 0)
            {
                expDic = new Dictionary<string, object>();
                expDic["EducationalManagementGradeID"] = insertEducationalManagementGrade.EducationalManagementGradeID;
            }
            bool EducationalManagementGradeDuplicate = this.repository.ExistsRow(GlobalConstants.LIST_SCHEMA, "EducationalManagementGrade",
                   new Dictionary<string, object>()
                {
                    {"Resolution",insertEducationalManagementGrade.Resolution},
                    {"IsActive",true}
                }, expDic);
            if (EducationalManagementGradeDuplicate)
            {
                List<object> listParam = new List<object>();
                listParam.Add("EducationalManagementGrade_Label_Resolution");
                throw new BusinessException("Common_Validate_Duplicate", listParam);
            }


            insertEducationalManagementGrade.IsActive = true;

          return  base.Insert(insertEducationalManagementGrade);
        }
        #endregion

        #region update
        /// <summary>
        /// Cập nhật Trình độ quản lý giáo dục
        /// <author>dungnt</author>
        /// <date>05/09/2012</date>
        /// </summary>
        /// <param name="updateEducationalManagementGrade">doi tuong da sua</param>
        /// <returns></returns>
        public override EducationalManagementGrade Update(EducationalManagementGrade updateEducationalManagementGrade)
        {
            //check avai
            new EducationalManagementGradeBusiness(null).CheckAvailable(updateEducationalManagementGrade.EducationalManagementGradeID, "EducationalManagementGrade_Label_EducationalManagementGradeID");
            
            ValidationMetadata.ValidateObject(updateEducationalManagementGrade);

            //resolution rong
            //Utils.ValidateRequire(updateEducationalManagementGrade.Resolution, "EducationalManagementGrade_Label_Resolution");
            //Utils.ValidateMaxLength(updateEducationalManagementGrade.Resolution, RESOLUTION_MAX_LENGTH, "EducationalManagementGrade_Label_Resolution");
            //Utils.ValidateMaxLength(updateEducationalManagementGrade.Description, DESCRIPTION_MAX_LENGTH, "EducationalManagementGrade_Column_Description");
            //kiem tra ton tai - theo cap - can lam lai
             this.CheckDuplicate(updateEducationalManagementGrade.Resolution, GlobalConstants.LIST_SCHEMA, "EducationalManagementGrade", "Resolution", true, updateEducationalManagementGrade.EducationalManagementGradeID, "EducationalManagementGrade_Label_Resolution");
            //this.CheckDuplicateCouple(updateEducationalManagementGrade.Resolution, updateEducationalManagementGrade.SchoolID.ToString(),
            //    GlobalConstants.LIST_SCHEMA, "EducationalManagementGrade", "Resolution", "SchoolID", true, updateEducationalManagementGrade.EducationalManagementGradeID, "EducationalManagementGrade_Label_Resolution");

            // this.CheckDuplicate(updateEducationalManagementGrade.SchoolID.ToString(), GlobalConstants.LIST_SCHEMA, "EducationalManagementGrade", "AppliedLevel", true, updateEducationalManagementGrade.EducationalManagementGradeID, "EducationalManagementGrade_Label_ShoolID");
            //Loi pham quy che thi khong thuoc truong truyen vao
            //tu EducationalManagementGradeID ->vao csdl lay EducationalManagementGrade tuong ung roi 
            //so sanh voi updateEducationalManagementGrade.SchoolID
            //.................

            updateEducationalManagementGrade.IsActive = true;

           return base.Update(updateEducationalManagementGrade);
        }
        #endregion

        #region Delete
        /// <summary>
        /// Xóa Trình độ quản lý giáo dục
        /// <author>dungnt</author>
        /// <date>05/09/2012</date>
        /// </summary>
        /// <param name="EducationalManagementGradeId">id doi tuong can xoa</param>
        public void Delete(int EducationalManagementGradeId)
        {
            //check avai
            new EducationalManagementGradeBusiness(null).CheckAvailable(EducationalManagementGradeId, "EducationalManagementGrade_Label_EducationalManagementGradeID");

            //da su dung hay chua
            this.CheckConstraints(GlobalConstants.LIST_SCHEMA, "EducationalManagementGrade",EducationalManagementGradeId, "EducationalManagementGrade_Label_EducationalManagementGradeIDFailed");

            base.Delete(EducationalManagementGradeId,true);


        }
        #endregion

        #region Search
        /// <summary>
        /// Tìm kiếm Trình độ quản lý giáo dục
        /// <author>dungnt</author>
        /// <date>05/09/2012</date>
        /// </summary>
        /// <param name="dic">tham so doi tuong tim kiem</param>
        /// <returns>Danh sách kết quả tìm kiếm</returns>
        public IQueryable<EducationalManagementGrade> Search(IDictionary<string, object> dic)
        {
            string resolution = Utils.GetString(dic,"Resolution");
            string description = Utils.GetString(dic,"Description");
           
            bool? isActive =Utils.GetIsActive(dic,"IsActive");
            IQueryable<EducationalManagementGrade> lsEducationalManagementGrade = this.EducationalManagementGradeRepository.All.OrderBy(o => o.Resolution);


             if (isActive.HasValue)
            {
                lsEducationalManagementGrade = lsEducationalManagementGrade.Where(em => (em.IsActive == isActive));
            }

            

            if (resolution.Trim().Length != 0)
            {

                lsEducationalManagementGrade = lsEducationalManagementGrade.Where(em => (em.Resolution.ToUpper().Contains(resolution.ToUpper())));
            }
            if (description.Trim().Length != 0)
            {

                lsEducationalManagementGrade = lsEducationalManagementGrade.Where(em => (em.Description.ToUpper().Contains(description.ToUpper())));
            }
           

            return lsEducationalManagementGrade;
        }
        #endregion
    }
}