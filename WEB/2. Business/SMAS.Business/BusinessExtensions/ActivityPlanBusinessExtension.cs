/**
 * The Viettel License
 *
 * Copyright 2012 Viettel Telecom. All rights reserved.
 * VIETTEL PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */

/** 
* @author  
* @version $Revision: $
*/

using System;
using System.Collections.Generic;
using System.Linq;
using SMAS.DAL.Repository;
using SMAS.Models.Models;
using SMAS.Business.Common;
using System.Transactions;
using SMAS.Business.BusinessObject;
using System.Data.Objects;

namespace SMAS.Business.Business
{
    public partial class ActivityPlanBusiness
    {
        public void InsertOrUpdateActivityPlan(List<int> listClass, ActivityPlan ap, List<Activity> lstActivity)
        {
            if (listClass.Count == 0)
            {
                throw new BusinessException("ActivityPlan_Validate_ClassCount");
            }
            if (lstActivity.Count == 0)
            {
                throw new BusinessException("ActivityPlan_Validate_ActivityCount");
            }
            foreach (int classId in listClass)
            {
                new ClassProfileBusiness(null).CheckAvailable(classId, "ClassProfile_Label_ClassProfile");
            }
            if (ap.FromDate > ap.ToDate)
            {
                throw new BusinessException("ActivityPlan_Validate_Date");
            }

            foreach (int classId in listClass)
            {
                List<int> lsActivityPlanId = ActivityPlanClassBusiness.All.Where(o => o.ClassID == classId && o.ActivityPlanID != ap.ActivityPlanID).Select(o => o.ActivityPlanID).ToList();
                for (int i = 0; i < lsActivityPlanId.Count; i++)
                {
                    ActivityPlan activityPlan = ActivityPlanBusiness.Find(lsActivityPlanId[i]);
                    if (activityPlan.FromDate.Date <= ap.FromDate.Date && activityPlan.ToDate.Date >= ap.FromDate.Date)
                    {
                        throw new BusinessException("ActivityPlan_Validate_Date");
                    }
                    if (activityPlan.FromDate.Date <= ap.ToDate.Date && activityPlan.ToDate.Date >= ap.ToDate.Date)
                    {
                        throw new BusinessException("ActivityPlan_Validate_Date");
                    }
                    if (activityPlan.FromDate.Date >= ap.FromDate.Date && activityPlan.FromDate.Date <= ap.ToDate.Date)
                    {
                        throw new BusinessException("ActivityPlan_Validate_Date");
                    }
                }
            }
            bool isUpdate = false;
            using (TransactionScope scope = new TransactionScope())
            {
                if (ap.ActivityPlanID != 0)
                {
                    List<Activity> q = ActivityRepository.All.Where(o => o.ActivityPlanID == ap.ActivityPlanID).ToList();
                    ActivityBusiness.DeleteAll(q);
                    List<ActivityPlanClass> qApc = ActivityPlanClassRepository.All.Where(o => o.ActivityPlanID == ap.ActivityPlanID).ToList();
                    ActivityPlanClassBusiness.DeleteAll(qApc);
                    isUpdate = true;
                }

                foreach (int classId in listClass)
                {
                    ActivityPlanClass apc = new ActivityPlanClass();
                    apc.ActivityPlanID = ap.ActivityPlanID;
                    apc.ClassID = classId;
                    ActivityPlanClassBusiness.Insert(apc);
                }
                foreach (Activity item in lstActivity)
                {
                    item.ActivityPlanID = ap.ActivityPlanID;

                }
                ActivityBusiness.Insert(lstActivity);
                if (isUpdate)
                {
                    base.Update(ap);
                }
                else
                {
                    ap.IsActive = true;
                    base.Insert(ap);
                }
                scope.Complete();
            }
        }

        public int Insert(ActivityPlan ap)
        {
            ValidationMetadata.ValidateObject(ap);
            //this.CheckDuplicate(ap.ActivityPlanName, GlobalConstants.PUPIL_SCHEMA, "ActivityPlan", "ActivityPlanName", true, 0, "ActivityPlan_Label_ActivityPlanName");

            bool AcademicYearCompatible = new AcademicYearRepository(this.context).ExistsRow(GlobalConstants.SCHOOL_SCHEMA, "AcademicYear",
                 new Dictionary<string, object>()
                {
                    {"SchoolID",ap.SchoolID},
                    {"AcademicYearID",ap.AcademicYearID}
                }, null);
            if (!AcademicYearCompatible)
            {
                throw new BusinessException("Common_Validate_NotCompatible");
            }
            if (ap.FromDate >= ap.ToDate)
            {
                throw new BusinessException("ActivityPlan_Validate_Month");
            }

            if (ap.FromDate.DayOfWeek != DayOfWeek.Monday)
            {
                throw new BusinessException("ActivityPlan_Validate_FromDate");
            }
            if (ap.ToDate.DayOfWeek != DayOfWeek.Saturday)
            {
                throw new BusinessException("ActivityPlan_Validate_ToDate");
            }
            base.Insert(ap);
            return ap.ActivityPlanID;
        }

        public void Delete(int UserAccountID, int ActivityPlanID, int SchoolID)
        {
            ActivityPlan ap = this.Find(ActivityPlanID);
            /*if (!UserAccountBusiness.IsSchoolAdmin(UserAccountID))
            {
                throw new BusinessException("ActivityPlan_Validate_UserAccountID");
            }*/
            bool activityPlanCompatible = new ActivityPlanRepository(this.context).ExistsRow(GlobalConstants.PUPIL_SCHEMA, "ActivityPlan",
                new Dictionary<string, object>()
                {
                    {"ActivityPlanID",ActivityPlanID},
                    {"SchoolID",SchoolID}
                }, null);
            if (!activityPlanCompatible)
            {
                throw new BusinessException("Common_Validate_NotCompatible");
            }
            //if (ap.FromDate <= DateTime.Now)
            //{
            //    //throw new BusinessException("ActivityPlan_Validate_FromDate");
            //    throw new BusinessException("ActivityPlan_Invalid_FromDate");
            //}
            ap.IsActive = false;
            base.Update(ap);
        }

        private IQueryable<ActivityPlanBO> Search(IDictionary<string, object> SearchInfo)
        {
            int AppliedLevel = Utils.GetShort(SearchInfo, "AppliedLevel");
            int EducationLevelID = Utils.GetInt(SearchInfo, "EducationLevelID");
            int SchoolID = Utils.GetInt(SearchInfo, "SchoolID");
            int? ClassID = Utils.GetNullableInt(SearchInfo, "ClassID");
            int AcademicYearID = Utils.GetInt(SearchInfo, "AcademicYearID");
            System.DateTime? ActivityDate = Utils.GetDateTime(SearchInfo, "ActivityDate");
            System.DateTime? FromDate = Utils.GetDateTime(SearchInfo, "FromDate");
            System.DateTime? ToDate = Utils.GetDateTime(SearchInfo, "ToDate");
            System.DateTime? CreatedDate = Utils.GetDateTime(SearchInfo, "CreatedDate");
            System.DateTime? ModifiedDate = Utils.GetDateTime(SearchInfo, "ModifiedDate");
            string ActivityPlanName = Utils.GetString(SearchInfo, "ActivityPlanName");
            string CommentOfTeacher = Utils.GetString(SearchInfo, "CommentOfTeacher");
            bool? IsActive = Utils.GetNullableBool(SearchInfo, "IsActive");
            IQueryable<ActivityPlanBO> query;
            if (EducationLevelID != 0)
            {
                if (ClassID == null)
                {
                    query = from eg in ActivityPlanBusiness.All
                            join egc in ActivityPlanClassBusiness.All.Where(o => (o.ClassProfile.EducationLevel.Grade == AppliedLevel && o.ClassProfile.EducationLevelID == EducationLevelID)) 
                            on eg.ActivityPlanID equals egc.ActivityPlanID
                            where eg.IsActive
                            select new ActivityPlanBO
                            {
                                SchoolID = eg.SchoolID,
                                AcademicYearID = eg.AcademicYearID,
                                ActivityPlanID = eg.ActivityPlanID,
                                EducationLevelID = egc.ClassProfile.EducationLevelID,
                                ClassID = egc.ClassID,
                                ClassName = egc.ClassProfile.DisplayName,
                                PlannedDate = eg.PlannedDate,
                                FromDate = eg.FromDate,
                                ToDate = eg.ToDate,
                                ActivityPlanName = eg.ActivityPlanName,
                                CommentOfTeacher = eg.CommentOfTeacher,
                                AppliedLevel = egc.ClassProfile.EducationLevel.Grade,
                                IsActive = eg.IsActive,
                                CreatedDate = eg.CreatedDate,
                                ModifiedDate = eg.ModifiedDate
                            };
                }
                else
                {
                    query = from eg in ActivityPlanBusiness.All
                            join egc in ActivityPlanClassBusiness.All.Where(o => (o.ClassID == ClassID && o.ClassProfile.EducationLevel.Grade == AppliedLevel && o.ClassProfile.EducationLevelID == EducationLevelID))
                            on eg.ActivityPlanID equals egc.ActivityPlanID
                            where eg.IsActive
                            select new ActivityPlanBO
                            {
                                SchoolID = eg.SchoolID,
                                AcademicYearID = eg.AcademicYearID,
                                ActivityPlanID = eg.ActivityPlanID,
                                EducationLevelID = egc.ClassProfile.EducationLevelID,
                                ClassID = egc.ClassID,
                                ClassName = egc.ClassProfile.DisplayName,
                                PlannedDate = eg.PlannedDate,
                                FromDate = eg.FromDate,
                                ToDate = eg.ToDate,
                                ActivityPlanName = eg.ActivityPlanName,
                                CommentOfTeacher = eg.CommentOfTeacher,
                                AppliedLevel = egc.ClassProfile.EducationLevel.Grade,
                                IsActive = eg.IsActive,
                                CreatedDate = eg.CreatedDate,
                                ModifiedDate = eg.ModifiedDate
                            };
                }
            }
            else 
            {
                query = from eg in ActivityPlanBusiness.All
                        join egc in ActivityPlanClassBusiness.All.Where(o => o.ClassProfile.EducationLevel.Grade == AppliedLevel) 
                                                on eg.ActivityPlanID equals egc.ActivityPlanID
                        where eg.IsActive
                        select new ActivityPlanBO
                        {
                            ActivityPlanID = eg.ActivityPlanID,
                            SchoolID = eg.SchoolID,
                            AcademicYearID = eg.AcademicYearID,
                            EducationLevelID = egc.ClassProfile.EducationLevelID,
                            ClassID = egc.ClassID,
                            ClassName = egc.ClassProfile.DisplayName,
                            PlannedDate = eg.PlannedDate,
                            FromDate = eg.FromDate,
                            ToDate = eg.ToDate,
                            ActivityPlanName = eg.ActivityPlanName,
                            CommentOfTeacher = eg.CommentOfTeacher,
                            AppliedLevel = egc.ClassProfile.EducationLevel.Grade,
                            IsActive = eg.IsActive,
                            CreatedDate = eg.CreatedDate,
                            ModifiedDate = eg.ModifiedDate
                        };
            }
            if (IsActive.HasValue)
            {
                query = query.Where(o => o.IsActive == IsActive);
            }

            // Fixbug 0016133
            if (FromDate.HasValue && !ToDate.HasValue)
            {
                query = query.Where(o => o.ToDate >= FromDate);
            }
            else if (ToDate.HasValue && !FromDate.HasValue)
            {
                query = query.Where(o => o.FromDate <= ToDate);
            }
            else if (FromDate.HasValue)
            {
                query = query.Where(o => (o.FromDate >= FromDate && o.FromDate <= ToDate) || o.ToDate >= FromDate && o.ToDate <= ToDate);
            }
            // Ngay co hoat dong phai thuoc trong ke hoach

            if (ActivityDate.HasValue)
            {
                query = query.Where(o =>EntityFunctions.TruncateTime(o.FromDate)<= EntityFunctions.TruncateTime(ActivityDate.Value) && EntityFunctions.TruncateTime(o.ToDate) >= EntityFunctions.TruncateTime(ActivityDate.Value));
            }
            if (CreatedDate.HasValue)
            {
                query = query.Where(o => o.CreatedDate.Day == CreatedDate.Value.Day && 
                    o.CreatedDate.Month >= CreatedDate.Value.Month &&o.CreatedDate.Year >= CreatedDate.Value.Year);
            }
            if (ModifiedDate.HasValue)
            {
                query = query.Where(o => o.ModifiedDate.Value.Day == ModifiedDate.Value.Day &&
                    o.ModifiedDate.Value.Month == ModifiedDate.Value.Month && o.ModifiedDate.Value.Year >= ModifiedDate.Value.Year);
            }
            if (!string.IsNullOrEmpty(ActivityPlanName))
            {
                query = query.Where(o => o.ActivityPlanName.ToLower().Contains(ActivityPlanName.ToLower()));
            }
            if (!string.IsNullOrEmpty(CommentOfTeacher))
            {
                query = query.Where(o => o.CommentOfTeacher.ToLower().Contains(CommentOfTeacher.ToLower()));
            }
            if (SchoolID > 0)
            {
                query = query.Where(o => o.SchoolID == SchoolID);
            }
            if (AcademicYearID != 0)
            {
                query = query.Where(o => o.AcademicYearID == AcademicYearID);
            }
            return query;
        }

        public IQueryable<ActivityPlanBO> SearchBySchool(int schoolId, IDictionary<string, object> dic)
        {
            if (schoolId == 0) return null;
            if (schoolId != 0)
                dic["SchoolID"] = schoolId;
            return Search(dic);
        }

    }
}
