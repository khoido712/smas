﻿/**
 * The Viettel License
 *
 * Copyright 2012 Viettel Telecom. All rights reserved.
 * VIETTEL PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */

/** 
* @author trangdd 
* @version $Revision: $
*/

using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Linq.Expressions;
using System.Web;
using log4net;

using SMAS.DAL.Repository;
using SMAS.DAL.IRepository;
using SMAS.Business.IBusiness;
using SMAS.Models.Models;
using SMAS.Business.Common;

namespace SMAS.Business.Business
{ 
    public partial class ClassificationLevelHealthBusiness
    {
        #region Search


        /// <summary>
        /// Tìm kiếm dân tộc
        /// </summary>
        /// <author>trangdd</author>
        /// <date>06/11/2012</date>    
        /// <returns>IQueryable<Ethnic></returns>
        public IQueryable<ClassificationLevelHealth> Search(IDictionary<string, object> dic)
        {
            int Level = Utils.GetInt(dic, "Level");            
            int TypeConfigID = Utils.GetInt(dic, "TypeConfigID");
            int EvaluationConfigID = Utils.GetInt(dic, "EvaluationConfigID");
            int SymbolConfigID = Utils.GetInt(dic, "SymbolConfigID");
            bool? IsActive = Utils.GetIsActive(dic, "IsActive");
            IQueryable<ClassificationLevelHealth> lsClassificationLevelHealth = ClassificationLevelHealthRepository.All;
            if (IsActive.HasValue)
            {
                lsClassificationLevelHealth = lsClassificationLevelHealth.Where(cfh => cfh.IsActive == IsActive);
            }
            if (Level != 0)
            {
                lsClassificationLevelHealth = lsClassificationLevelHealth.Where(cfh => cfh.Level == Level);
            }            
            if (TypeConfigID != 0)
            {
                lsClassificationLevelHealth = lsClassificationLevelHealth.Where(cfh => cfh.TypeConfigID == TypeConfigID);
            }
            if (EvaluationConfigID != 0)
            {
                lsClassificationLevelHealth = lsClassificationLevelHealth.Where(cfh => cfh.EvaluationConfigID == EvaluationConfigID);
            }
            if (SymbolConfigID != 0)
            {
                lsClassificationLevelHealth = lsClassificationLevelHealth.Where(cfh => cfh.SymbolConfigID == SymbolConfigID);
            }   
            return lsClassificationLevelHealth;

        }
        #endregion

        #region Delete

        /// <summary>
        /// Xóa mức phân loại sức khỏe
        /// </summary>
        /// <author>trangdd</author>
        /// <date>06/11/2012</date>
        /// <param name="ClassificationLevelHealthID"></param>
        public void Delete(int ClassificationLevelHealthID)
        {
            //Bạn chưa chọn mức phân loại sức khỏe cần xóa hoặc mức phân loại sức khỏe bạn chọn đã bị xóa khỏi hệ thống
            new ClassificationLevelHealthBusiness(null).CheckAvailable((int)ClassificationLevelHealthID, "ClassificationLevelHealth_Label_ClassificationLevelHealthID", true);

            //Không thể xóa mức phân loại sức khỏe đang sử dụng
            new ClassificationLevelHealthBusiness(null).CheckConstraints(GlobalConstants.LIST_SCHEMA, "ClassificationLevelHealth", ClassificationLevelHealthID, "ClassificationLevelHealth_Label_ClassificationLevelHealthID");          
            base.Delete(ClassificationLevelHealthID, true);
        }
        #endregion

        #region Insert

        /// <summary>
        /// Thêm mới mức phân loại sức khỏe
        /// </summary>
        /// <author>trangdd</author>
        /// <date>06/11/2012</date>
        /// <param name="ethnic">Đối tượng ClassificationLevelHealth</param>
        /// <returns>Đối tượng ClassificationLevelHealth</returns>
        public override ClassificationLevelHealth Insert(ClassificationLevelHealth classificationLevelHealth)
        {
            
            //Level, TypeConfigID, SchoolID: duplicate
            bool Levelduplicate = this.repository.ExistsRow(GlobalConstants.LIST_SCHEMA, "ClassificationLevelHealth",
                 new Dictionary<string, object>()
                {
                    {"Level",classificationLevelHealth.Level},
                    {"TypeConfigID",classificationLevelHealth.TypeConfigID},                    
                    {"IsActive",true}
                },
                   new Dictionary<string, object>()
                 {
                    {"ClassificationLevelHealthID",classificationLevelHealth.ClassificationLevelHealthID},                    
                });
            string TypeConfigName = "";
            if (classificationLevelHealth.TypeConfigID == 1)
            {
                TypeConfigName = "Cân nặng";
            }
            else if (classificationLevelHealth.TypeConfigID == 2)
            {
                TypeConfigName = "Chiều cao";
            }
            else
            {
                TypeConfigName = "BMI";
            }
            if (Levelduplicate)
            {
                throw new BusinessException("ClassificationLevelHealth_Label_Duplicate", new List<object> { classificationLevelHealth.Level.ToString(), TypeConfigName });
            }

            //Insert
            return base.Insert(classificationLevelHealth);

        }
        #endregion

        #region Update
        /// <summary>
        /// Sửa mức phân loại sức khỏe
        /// </summary>
        /// <author>trangdd</author>
        /// <date>06/11/2012</date>
        /// <param name="ethnic">Đối tượng Ethnic</param>
        /// <returns>Đối tượng Ethnic</returns>
        public override ClassificationLevelHealth Update(ClassificationLevelHealth classificationLevelHealth)
        {
            //Level, TypeConfigID, SchoolID: duplicate
            bool Levelduplicate = new ClassificationLevelHealthRepository(this.context).ExistsRow(GlobalConstants.LIST_SCHEMA, "ClassificationLevelHealth",
                 new Dictionary<string, object>()
                {
                    {"Level",classificationLevelHealth.Level},
                    {"TypeConfigID",classificationLevelHealth.TypeConfigID},                   
                    {"IsActive",true}
                },
                   new Dictionary<string, object>()
                 {
                    {"ClassificationLevelHealthID",classificationLevelHealth.ClassificationLevelHealthID},                    
                });

            string TypeConfigName = "";
            if (classificationLevelHealth.TypeConfigID == 1)
            {
                TypeConfigName = "Cân nặng";
            }
            else if (classificationLevelHealth.TypeConfigID == 2)
            {
                TypeConfigName = "Chiều cao";
            }
            else
            {
                TypeConfigName = "BMI";
            }
            if (Levelduplicate)
            {
                throw new BusinessException("ClassificationLevelHealth_Label_Duplicate", new List<object> { classificationLevelHealth.Level.ToString(), TypeConfigName });
            }
           
            return base.Update(classificationLevelHealth);
        }
        #endregion      
    }
}