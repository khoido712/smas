﻿/**
 * The Viettel License
 *
 * Copyright 2012 Viettel Telecom. All rights reserved.
 * VIETTEL PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */

/** 
* @author  chiendd1
* @version $Revision: $
*/

using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Linq.Expressions;
using System.Web;
using log4net;

using SMAS.DAL.Repository;
using SMAS.DAL.IRepository;
using SMAS.Business.IBusiness;
using SMAS.Models.Models;
using SMAS.Business.Common;
using SMAS.Business.BusinessObject;
using System.Transactions;
using System.IO;
using SMAS.VTUtils.Excel.Export;
using System.Configuration;

namespace SMAS.Business.Business
{
    /// <summary>
    /// thông tin xếp loại học    lực / hạnh kiểm và xếp thứ bậc học sinh trong lớp
    /// <author>Chiendd1</author>
    /// <date>16/06/2016</date>
    /// </summary>
    public partial class PupilRankingHistoryBusiness
    {
        public IQueryable<PupilRankingHistory> SearchBySchool(int SchoolID, IDictionary<string, object> dic)
        {
            if (SchoolID == 0)
                return null;
            dic["SchoolID"] = SchoolID;
            return Search(dic);
        }

        public void InsertOrUpdateList(List<PupilRanking> lstPupilRankingInsert, List<PupilRanking> lstPupilRankingUpdate)
        {
            try
            {
                context.Configuration.AutoDetectChangesEnabled = false;
                context.Configuration.ValidateOnSaveEnabled = false;
                //Chuyen du lieu sang lich su
                if (lstPupilRankingInsert != null && lstPupilRankingInsert.Count > 0)
                {
                    for (int i = 0; i < lstPupilRankingInsert.Count; i++)
                    {
                        PupilRankingHistoryBusiness.Insert(ConvertHistory(lstPupilRankingInsert[i]));
                    }
                }
                if (lstPupilRankingUpdate != null && lstPupilRankingUpdate.Count > 0)
                {
                    for (int i = 0; i < lstPupilRankingUpdate.Count; i++)
                    {
                        PupilRankingHistoryBusiness.Update(ConvertHistory(lstPupilRankingUpdate[i]));
                    }
                }
                PupilRankingHistoryBusiness.Save();
            }
            finally
            {
                context.Configuration.AutoDetectChangesEnabled = true;
                context.Configuration.ValidateOnSaveEnabled = true;
            }
        }


        /// <summary>
        /// Chiendd1. 
        /// Copy to PupilRanking to history
        /// </summary>
        /// <param name="UserID"></param>
        /// <param name="lstPupilRanking"></param>
        /// <param name="Semester"></param>
        /// <param name="IsSecondSemesterToSemesterAll"></param>
        public void RankingPupilConductHistory(int UserID, List<PupilRanking> lstPupilRanking, int Semester, bool IsSecondSemesterToSemesterAll)
        {
            try
            {

                if (lstPupilRanking == null || lstPupilRanking.Count == 0) return;
                SetAutoDetectChangesEnabled(false);
                List<PupilRankingHistory> lstPupilRankingHistory = ConvertListToHistory(lstPupilRanking);
                #region validate du lieu
                PupilRankingHistory firstPr = lstPupilRankingHistory.First();
                int semester = firstPr.Semester.Value;
                int? periodID = firstPr.PeriodID;
                int? grade = EducationLevelBusiness.Find(firstPr.EducationLevelID).Grade;

                var lstClassVsSchool = lstPupilRankingHistory.Select(u => new { u.ClassID, u.EducationLevelID, u.SchoolID, u.AcademicYearID }).Distinct();
                foreach (var item in lstClassVsSchool)
                {
                    // Validate du lieu dau vao
                    ClassProfile classProfile = ClassProfileBusiness.Find(item.ClassID);
                    if (classProfile == null || classProfile.EducationLevelID != item.EducationLevelID)
                    {
                        throw new BusinessException("Common_Validate_NotCompatible");
                    }
                    if (classProfile.AcademicYearID != item.AcademicYearID)
                    {
                        throw new BusinessException("Common_Validate_NotCompatible");
                    }
                    AcademicYear academicYear = AcademicYearBusiness.Find(item.AcademicYearID);
                    if (academicYear == null || academicYear.SchoolID != item.SchoolID)
                    {
                        throw new BusinessException("Common_Validate_NotCompatible");
                    }
                    // Nam hoc hien tai
                    if (!AcademicYearBusiness.IsCurrentYear(academicYear))
                    {
                        throw new BusinessException("Common_Validate_IsCurrentYear");
                    }

                    var lstPoc = PupilOfClassBusiness.SearchBySchool(item.SchoolID, new Dictionary<string, object> { { "ClassID", item.ClassID }, { "Check", "Check" } })
                        .Select(o => new { o.PupilID, o.Status })
                        .ToList();

                    List<PupilRankingHistory> lstPrOfClass = lstPupilRankingHistory.Where(u => u.ClassID == item.ClassID).ToList();
                    foreach (PupilRankingHistory pr in lstPrOfClass)
                    {
                        var poc = lstPoc.FirstOrDefault(u => u.PupilID == pr.PupilID);
                        if (poc == null || poc.Status != SystemParamsInFile.PUPIL_STATUS_STUDYING)
                            throw new BusinessException("PupilRanking_Validate_ProfileStatus");
                        // Chi can check co phai la nam hoc hien tai (nhu tren) la du.
                        //if (poc.CurrentAcademicYearID != pr.AcademicYearID)
                        //    throw new BusinessException("Common_Validate_NotCompatible");
                    }

                }
                #endregion

                Dictionary<int, bool> dicPermission = new Dictionary<int, bool>();
                lstPupilRankingHistory.Select(u => u.ClassID).Distinct().ToList().ForEach(u =>
                {
                    dicPermission[u] = UtilsBusiness.HasHeadTeacherPermission(UserID, u);
                });

                Dictionary<int, List<PupilAbsenceBO>> dicAbsence = new Dictionary<int, List<PupilAbsenceBO>>();
                lstClassVsSchool.ToList().ForEach(u =>
                {
                    dicAbsence[u.ClassID] = PupilAbsenceBusiness.GetListPupilAbsenceBySection(new Dictionary<string, object> { { "SchoolID", u.SchoolID }, { "AcademicYearID", u.AcademicYearID }, { "ClassID", u.ClassID } }).ToList();
                });


                Dictionary<int, List<PupilRankingHistory>> dicPupilRanking = new Dictionary<int, List<PupilRankingHistory>>();
                Dictionary<int, List<PupilRankingHistory>> dicPupilRankingAll = new Dictionary<int, List<PupilRankingHistory>>();
                Dictionary<int, List<PupilRankingHistory>> dicPupilRankingNotRLL = new Dictionary<int, List<PupilRankingHistory>>();
                lstClassVsSchool.ToList().ForEach(u =>
                {
                    dicPupilRanking[u.ClassID] = this.SearchBySchool(u.SchoolID, new Dictionary<string, object> { { "ClassID", u.ClassID }, { "Semester", semester }, { "PeriodID", periodID } }).ToList();
                    dicPupilRankingNotRLL[u.ClassID] = this.SearchBySchool(u.SchoolID, new Dictionary<string, object> { { "ClassID", u.ClassID }, { "Semester", SystemParamsInFile.SEMESTER_OF_YEAR_BACKTRAINING }, { "PeriodID", periodID } }).ToList();
                    if (IsSecondSemesterToSemesterAll == true && Semester == SystemParamsInFile.SEMESTER_OF_YEAR_SECOND)
                        dicPupilRankingAll[u.ClassID] = this.SearchBySchool(u.SchoolID, new Dictionary<string, object> { { "ClassID", u.ClassID }, { "Semester", SystemParamsInFile.SEMESTER_OF_YEAR_ALL } }).ToList();
                });
                Dictionary<int, List<int>> dicPupilThreadMark = new Dictionary<int, List<int>>();
                foreach (var PupilRanking in lstPupilRankingHistory)
                {
                    if (dicPermission[PupilRanking.ClassID])
                    {
                        PupilRankingHistory existedPupilRanking = dicPupilRanking[PupilRanking.ClassID].FirstOrDefault(u => u.PupilID == PupilRanking.PupilID);

                        if (existedPupilRanking != null)//Nếu tìm thấy thì thêm vào đối tượng tìm được các thuộc tính của lstPupilRanking[i] và thực hiện update
                        {
                            #region Update

                            // Doi voi truong cap 2 - 3 thi se luu lai thong tin cap nhat cua hanh kiem de
                            // tinh lai xep hang va xep loai cho hoc sinh
                            #region Cap nhat thay doi hanh kiem cho tien trinh tu dong
                            if (existedPupilRanking.ConductLevelID != PupilRanking.ConductLevelID)
                            {
                                if (dicPupilThreadMark.ContainsKey(PupilRanking.ClassID))
                                {
                                    List<int> listPupilVal = dicPupilThreadMark[PupilRanking.ClassID];
                                    if (!listPupilVal.Contains(PupilRanking.PupilID))
                                    {
                                        dicPupilThreadMark[PupilRanking.ClassID].Add(PupilRanking.PupilID);
                                    }
                                }
                                else
                                {
                                    dicPupilThreadMark[PupilRanking.ClassID] = new List<int>();
                                    dicPupilThreadMark[PupilRanking.ClassID].Add(PupilRanking.PupilID);
                                }
                            }
                            #endregion

                            existedPupilRanking.ConductLevelID = PupilRanking.ConductLevelID;
                            existedPupilRanking.TotalAbsentDaysWithoutPermission = PupilRanking.TotalAbsentDaysWithoutPermission;
                            existedPupilRanking.TotalAbsentDaysWithPermission = PupilRanking.TotalAbsentDaysWithPermission;
                            existedPupilRanking.RankingDate = DateTime.Now;
                            existedPupilRanking.PupilRankingComment = PupilRanking.PupilRankingComment;
                            this.Update(existedPupilRanking);
                            if (existedPupilRanking.Semester == SystemParamsInFile.SEMESTER_OF_YEAR_ALL)
                            {
                                if (existedPupilRanking.StudyingJudgementID != SystemParamsInFile.STUDYING_JUDGEMENT_BACKTRAINING)
                                {
                                    int totalAbsenceWithoutPer = dicAbsence[PupilRanking.ClassID].Where(o => o.PupilID == PupilRanking.PupilID && o.IsAccepted == false).Count();
                                    int totalAbsenceWithPer = dicAbsence[PupilRanking.ClassID].Where(o => o.PupilID == PupilRanking.PupilID && o.IsAccepted == true).Count();
                                    PupilRankingHistory pupilRankingNotRLL = dicPupilRankingNotRLL[PupilRanking.ClassID].FirstOrDefault(u => u.PupilID == PupilRanking.PupilID);
                                    if (pupilRankingNotRLL != null)
                                    {
                                        pupilRankingNotRLL.ConductLevelID = PupilRanking.ConductLevelID;
                                        pupilRankingNotRLL.TotalAbsentDaysWithoutPermission = totalAbsenceWithPer;
                                        pupilRankingNotRLL.TotalAbsentDaysWithPermission = totalAbsenceWithoutPer;
                                        pupilRankingNotRLL.RankingDate = DateTime.Now;
                                        this.Update(pupilRankingNotRLL);
                                    }
                                }
                            }
                            if (existedPupilRanking.Semester == SystemParamsInFile.SEMESTER_OF_YEAR_BACKTRAINING)
                            {
                                PupilRankingHistory pupilRankingNotRLL = dicPupilRankingNotRLL[PupilRanking.ClassID].FirstOrDefault(u => u.PupilID == PupilRanking.PupilID);
                                if (pupilRankingNotRLL != null)
                                {
                                    if (PupilRanking.ConductLevelID == null)
                                    {
                                        this.Delete(pupilRankingNotRLL.PupilRankingID);
                                    }
                                }
                            }
                            #endregion
                        }
                        else//Nếu không tìm thấy thực hiện insert vào bảng PupilRanking đối tượng lstPupilRanking[i]
                        {
                            #region Them moi

                            PupilRanking.RankingDate = DateTime.Now;
                            if (PupilRanking.Semester == SystemParamsInFile.SEMESTER_OF_YEAR_BACKTRAINING)
                            {

                                if (PupilRanking.ConductLevelID != null)
                                {
                                    this.Insert(PupilRanking);
                                }

                            }
                            else
                            {
                                this.Insert(PupilRanking);
                                // Khong xu ly doi voi truong hop ren luyen lai
                                // Doi voi truong cap 2 - 3 thi se luu lai thong tin cap nhat cua hanh kiem de
                                // tinh lai xep hang va xep loai cho hoc sinh
                                #region Cap nhat thay doi hanh kiem cho tien trinh tu dong
                                if (dicPupilThreadMark.ContainsKey(PupilRanking.ClassID))
                                {
                                    List<int> listPupilVal = dicPupilThreadMark[PupilRanking.ClassID];
                                    if (!listPupilVal.Contains(PupilRanking.PupilID))
                                    {
                                        dicPupilThreadMark[PupilRanking.ClassID].Add(PupilRanking.PupilID);
                                    }
                                }
                                else
                                {
                                    dicPupilThreadMark[PupilRanking.ClassID] = new List<int>();
                                    dicPupilThreadMark[PupilRanking.ClassID].Add(PupilRanking.PupilID);
                                }
                                #endregion
                            }
                            #endregion
                        }

                        if (IsSecondSemesterToSemesterAll == true && Semester == SystemParamsInFile.SEMESTER_OF_YEAR_SECOND)
                        {
                            PupilRankingHistory pr2 = dicPupilRankingAll[PupilRanking.ClassID].FirstOrDefault(u => u.PupilID == PupilRanking.PupilID);

                            int totalAbsenceWithoutPer = dicAbsence[PupilRanking.ClassID].Where(o => o.PupilID == PupilRanking.PupilID && o.IsAccepted == false).Count();
                            int totalAbsenceWithPer = dicAbsence[PupilRanking.ClassID].Where(o => o.PupilID == PupilRanking.PupilID && o.IsAccepted == true).Count();

                            if (pr2 != null)
                            {
                                // Doi voi truong cap 2 - 3 thi se luu lai thong tin cap nhat cua hanh kiem de
                                // tinh lai xep hang va xep loai cho hoc sinh
                                #region Cap nhat thay doi hanh kiem cho tien trinh tu dong
                                if (pr2.ConductLevelID != PupilRanking.ConductLevelID)
                                {
                                    if (dicPupilThreadMark.ContainsKey(PupilRanking.ClassID))
                                    {
                                        List<int> listPupilVal = dicPupilThreadMark[PupilRanking.ClassID];
                                        if (!listPupilVal.Contains(PupilRanking.PupilID))
                                        {
                                            dicPupilThreadMark[PupilRanking.ClassID].Add(PupilRanking.PupilID);
                                        }
                                    }
                                    else
                                    {
                                        dicPupilThreadMark[PupilRanking.ClassID] = new List<int>();
                                        dicPupilThreadMark[PupilRanking.ClassID].Add(PupilRanking.PupilID);
                                    }
                                }
                                #endregion
                                pr2.ConductLevelID = PupilRanking.ConductLevelID;
                                pr2.TotalAbsentDaysWithoutPermission = totalAbsenceWithPer;
                                pr2.TotalAbsentDaysWithPermission = totalAbsenceWithoutPer;
                                pr2.RankingDate = DateTime.Now;
                                pr2.PupilRankingComment = PupilRanking.PupilRankingComment;
                                this.Update(pr2);
                                PupilRankingHistory pupilRankingNotRLL = dicPupilRankingNotRLL[PupilRanking.ClassID].FirstOrDefault(u => u.PupilID == PupilRanking.PupilID);
                                if (pupilRankingNotRLL != null)
                                {
                                    pupilRankingNotRLL.ConductLevelID = PupilRanking.ConductLevelID;
                                    pupilRankingNotRLL.TotalAbsentDaysWithoutPermission = totalAbsenceWithPer;
                                    pupilRankingNotRLL.TotalAbsentDaysWithPermission = totalAbsenceWithoutPer;
                                    pupilRankingNotRLL.RankingDate = DateTime.Now;
                                    this.Update(pupilRankingNotRLL);
                                }
                            }
                            else if (pr2 == null)
                            {
                                PupilRankingHistory pr = new PupilRankingHistory();
                                pr.SchoolID = PupilRanking.SchoolID;
                                pr.AcademicYearID = PupilRanking.AcademicYearID;
                                pr.CapacityLevelID = PupilRanking.CapacityLevelID;
                                pr.ClassID = PupilRanking.ClassID;
                                pr.ConductLevelID = PupilRanking.ConductLevelID;
                                pr.EducationLevelID = PupilRanking.EducationLevelID;
                                pr.PupilID = PupilRanking.PupilID;
                                pr.Semester = SystemParamsInFile.SEMESTER_OF_YEAR_ALL;
                                pr.TotalAbsentDaysWithoutPermission = totalAbsenceWithoutPer;
                                pr.TotalAbsentDaysWithPermission = totalAbsenceWithPer;
                                pr.RankingDate = DateTime.Now;
                                pr.CreatedAcademicYear = PupilRanking.CreatedAcademicYear;
                                pr.Last2digitNumberSchool = PupilRanking.SchoolID % 100;
                                pr.PupilRankingComment = PupilRanking.PupilRankingComment;
                                this.Insert(pr);
                                // Doi voi truong cap 2 - 3 thi se luu lai thong tin cap nhat cua hanh kiem de
                                // tinh lai xep hang va xep loai cho hoc sinh
                                #region Cap nhat thay doi hanh kiem cho tien trinh tu dong
                                if (dicPupilThreadMark.ContainsKey(PupilRanking.ClassID))
                                {
                                    List<int> listPupilVal = dicPupilThreadMark[PupilRanking.ClassID];
                                    if (!listPupilVal.Contains(PupilRanking.PupilID))
                                    {
                                        dicPupilThreadMark[PupilRanking.ClassID].Add(PupilRanking.PupilID);
                                    }
                                }
                                else
                                {
                                    dicPupilThreadMark[PupilRanking.ClassID] = new List<int>();
                                    dicPupilThreadMark[PupilRanking.ClassID].Add(PupilRanking.PupilID);
                                }
                                #endregion
                            }
                        }
                    }
                }

                this.Save();
                #region Save ThreadMark
                if (grade != SystemParamsInFile.EDUCATION_GRADE_PRIMARY)
                {
                    foreach (int key in dicPupilThreadMark.Keys)
                    {
                        List<int> listPupilVal = dicPupilThreadMark[key];
                        ThreadMarkBO info = new ThreadMarkBO();
                        info.SchoolID = firstPr.SchoolID;
                        info.ClassID = key;
                        info.AcademicYearID = firstPr.AcademicYearID;
                        info.Semester = Semester;
                        info.PeriodID = periodID;
                        info.Type = GlobalConstants.CONDUCT_AUTO_TYPE;
                        info.PupilIds = listPupilVal;
                        ThreadMarkBusiness.InsertActionList(info);
                    }
                }
                #endregion

            }
            finally
            {
                SetAutoDetectChangesEnabled(true);
            }
        }


        /// <summary>
        /// RankingPupilOfClass: thuc hien tong ket diem va xep hang
        /// </summary>
        /// <param name="UserID"></param>
        /// <param name="lstSummedUpRecord"></param>
        /// <param name="lstPupilRanking"></param>
        /// <param name="PeriodID"></param>
        /// <returns></returns>
        public void RankingPupilOfClassHistory(int UserID, List<SummedUpRecord> lstSummedUpRecord, List<PupilRanking> lstPupilRanking, int? PeriodID, List<ClassSubject> lstSubject, bool isShowRetetResult, string AutoMark, List<RegisterSubjectSpecializeBO> lstRegisterSubjectBO = null)
        {
            try
            {

                if (lstSummedUpRecord == null || lstSummedUpRecord.Count == 0)
                    throw new BusinessException("PupilRanking_Label_AllPupilNotMark");
                SetAutoDetectChangesEnabled(false);
                SummedUpRecord Sum = lstSummedUpRecord.FirstOrDefault();

                int ClassID = Sum.ClassID;
                int AcademicYearID = Sum.AcademicYearID;
                int SchoolID = Sum.SchoolID;
                int Semester = Sum.Semester.Value;

                // Kiem tra nam hoc hien tai
                AcademicYear academicYear = AcademicYearBusiness.Find(AcademicYearID);

                if (academicYear == null || !AcademicYearBusiness.IsCurrentYear(academicYear))
                    throw new BusinessException("ClassMovement_Validate_IsNotCurrentYear");


                // Kiem tra neu he so mon cua lop hoc ko co thi bao loi
                #region
                if (Semester == SystemParamsInFile.SEMESTER_OF_YEAR_FIRST)
                {
                    if (!lstSubject.Where(o => o.IsCommenting == SystemParamsInFile.ISCOMMENTING_TYPE_MARK || o.IsCommenting == SystemParamsInFile.ISCOMMENTING_TYPE_MARK_JUDGE)
                        .Any(o => o.FirstSemesterCoefficient.HasValue && o.FirstSemesterCoefficient.Value > 0))
                    {
                        throw new BusinessException("PupilRanking_NotPupil_HS");
                    }
                }
                else if (Semester == SystemParamsInFile.SEMESTER_OF_YEAR_SECOND)
                {
                    if (!lstSubject.Where(o => o.IsCommenting == SystemParamsInFile.ISCOMMENTING_TYPE_MARK || o.IsCommenting == SystemParamsInFile.ISCOMMENTING_TYPE_MARK_JUDGE)
                        .Any(o => o.SecondSemesterCoefficient.HasValue && o.SecondSemesterCoefficient.Value > 0))
                    {
                        throw new BusinessException("PupilRanking_NotPupil_HS");
                    }
                }
                #endregion
                List<PupilRankingHistory> lstPupilRankingHistory = ConvertListToHistory(lstPupilRanking);

                List<int> listPupilID = lstPupilRankingHistory.Select(o => o.PupilID).Distinct().ToList();
                List<int> listSubjectID = lstSummedUpRecord.Select(o => o.SubjectID).Distinct().ToList();
                var listPOC = (from poc in PupilOfClassBusiness.AllNoTracking
                               join pp in PupilProfileBusiness.All on poc.PupilID equals pp.PupilProfileID
                               where poc.ClassID == ClassID
                                    && (poc.Status == SystemParamsInFile.PUPIL_STATUS_STUDYING || poc.Status == SystemParamsInFile.PUPIL_STATUS_GRADUATED)
                                    && pp.IsActive
                               select new { ClassID = poc.ClassID, AcademicYearID = poc.AcademicYearID, poc.PupilID, poc.Status, pp.BirthDate, pp.PupilLearningType })
                    .ToList();
                List<ExemptedSubject> listExempteds = ExemptedSubjectBusiness.GetListExemptedSubject(ClassID, Semester).ToList();
                #region Validate du lieu dau vao
                if (academicYear.SchoolID != SchoolID)
                    throw new BusinessException("Common_Validate_NotCompatible");


                //Cac mon phai co diem
                foreach (ClassSubject classSubject in lstSubject)
                {
                    if (lstSummedUpRecord.Any(u => u.ClassID == classSubject.ClassID && u.SubjectID == classSubject.SubjectID
                            && u.IsCommenting == SystemParamsInFile.ISCOMMENTING_TYPE_JUDGE && string.IsNullOrEmpty(u.JudgementResult)))
                        Utils.ValidateRequire(string.Empty, "SummedUpRecord_Label_SummedUpMark");

                    if (lstSummedUpRecord.Any(u => u.ClassID == classSubject.ClassID && u.SubjectID == classSubject.SubjectID
                           && (u.IsCommenting == SystemParamsInFile.ISCOMMENTING_TYPE_MARK || u.IsCommenting == SystemParamsInFile.ISCOMMENTING_TYPE_MARK_JUDGE)
                           && !u.SummedUpMark.HasValue))
                        Utils.ValidateRequire(string.Empty, "SummedUpRecord_Label_SummedUpMark");
                }

                //neu ton tai mot hoc sinh duoc mien giam thi bao loi
                if (listExempteds.Any(u => lstSummedUpRecord.Any(v => v.PupilID == u.PupilID && v.SubjectID == u.SubjectID)))
                    throw new BusinessException("SummedUpRecord_Labal_ErrMGSubject");

                foreach (int PupilID in listPupilID)
                {
                    var poc = listPOC.FirstOrDefault(u => u.PupilID == PupilID);
                    if (poc == null || (poc.Status != SystemParamsInFile.PUPIL_STATUS_STUDYING && poc.Status != SystemParamsInFile.PUPIL_STATUS_GRADUATED))
                        throw new BusinessException("ClassMovement_Validate_PupilNotWorking");

                    if (poc.ClassID != ClassID)
                        throw new BusinessException("Common_Validate_NotCompatible");

                    if (poc.AcademicYearID != AcademicYearID)
                        throw new BusinessException("Common_Validate_NotCompatible");
                }
                #endregion

                // Process
                // Lay 2 danh sach tu 2 danh sach dau vao join qua dieu kien pupilID     
                SchoolProfile schoolProfile = SchoolProfileBusiness.Find(SchoolID);
                List<int> listCheckedPupilID = new List<int>();
                bool isGDTX = schoolProfile.TrainingType != null && schoolProfile.TrainingType.Resolution == "GDTX";
                int EducationLevelID = 0;
                int AppliedLevel = 0;
                if (lstPupilRankingHistory != null && lstPupilRankingHistory.Count > 0)
                {
                    EducationLevelID = lstPupilRanking[0].EducationLevelID;
                    if (isGDTX)
                        AppliedLevel = EducationLevelBusiness.Find(EducationLevelID).Grade;
                }
                List<PupilRankingHistory> ListPupilRanking4Save = new List<PupilRankingHistory>();

                TrainingType traningType = schoolProfile.TrainingType;

                // Lay du lieu trong PupilRanking theo truong, nam hoc, ky, dot neu co truoc
                Dictionary<string, object> dicRanking = new Dictionary<string, object>();
                dicRanking["AcademicYearID"] = AcademicYearID;
                dicRanking["ClassID"] = ClassID;
                dicRanking["Semester"] = Semester;
                if (PeriodID.HasValue)
                {
                    dicRanking["PeriodID"] = PeriodID.Value;
                }
                else
                {
                    dicRanking["PeriodID"] = null;
                }
                List<PupilRankingHistory> listPupilRankingByClass = this.SearchBySchool(SchoolID, dicRanking).ToList();
                List<PupilRankingHistory> lstPupilRankingRe = new List<PupilRankingHistory>();

                if (!PeriodID.HasValue)
                {
                    listPupilRankingByClass = listPupilRankingByClass.Where(o => !o.PeriodID.HasValue).ToList();
                }
                List<int> listPupilRetestID = listPupilRankingByClass.Where(o => o.StudyingJudgementID == SystemParamsInFile.STUDYING_JUDGEMENT_RETEST)
                   .Select(o => o.PupilID).ToList();
                //Neu hoc ky la ca nam thi lay ra cac ban ghi thi lai, ren luyen lai de cap nhat.
                // Lay danh sach hoc sinh da co nhap diem thi lai roi thi moi tinh vao truong hop nay
                // Neu chi moi xep loai la thi lai ma chua nhap diem thi lai thi van cap nhat vao ban ghi ca nam

                if (Semester == SystemParamsInFile.SEMESTER_OF_YEAR_ALL && isShowRetetResult)
                {
                    // Co HS thi lai moi tim dan sach chi tiet
                    if (listPupilRetestID.Count > 0)
                    {
                        dicRanking["Semester"] = SystemParamsInFile.SEMESTER_OF_YEAR_BACKTRAINING;
                        lstPupilRankingRe = this.Search(dicRanking).ToList();
                        List<int> listPupilIDInputMarkRetest = null;
                        listPupilIDInputMarkRetest = VSummedUpRecordBusiness.SearchBySchool(SchoolID, new Dictionary<string, object>()
                           {
                                {"AcademicYearID", AcademicYearID},
                                {"ClassID", ClassID}
                           }).Where(o => o.Semester > SystemParamsInFile.SEMESTER_OF_YEAR_ALL && !o.PeriodID.HasValue).Select(o => o.PupilID).ToList();
                        listPupilRetestID.RemoveAll(o => !listPupilIDInputMarkRetest.Contains(o));

                    }
                }
                List<int> listPupilIDNotRankConduct = new List<int>();
                Dictionary<string, ClassSubject> dicClassSubject = new Dictionary<string, ClassSubject>();

                foreach (PupilRankingHistory PupilRanking in lstPupilRankingHistory)
                {
                    List<SummedUpRecord> lstPupilRankingOfPupil_Mandatory = new List<SummedUpRecord>();
                    List<SummedUpRecord> lstPupilRankingOfPupil_Voluntary = new List<SummedUpRecord>();
                    if (listCheckedPupilID.Contains(PupilRanking.PupilID))
                    {
                        continue;
                    }

                    //if(classSubject.FirstSemesterCoefficient.HasValue
                    foreach (SummedUpRecord SummedUpRecord in lstSummedUpRecord)
                    {
                        string key = SummedUpRecord.ClassID + "_" + SummedUpRecord.SubjectID;
                        // Lay mon hoc cua lop
                        ClassSubject classSubject = null;
                        if (dicClassSubject.ContainsKey(key))
                        {
                            classSubject = dicClassSubject[key];
                        }
                        else
                        {
                            classSubject = lstSubject.Where(u => u.ClassID == SummedUpRecord.ClassID && u.SubjectID == SummedUpRecord.SubjectID).FirstOrDefault();
                            dicClassSubject[key] = classSubject;
                        }
                        if (classSubject == null)
                        {
                            throw new BusinessException("ClassSubject_label_NotExist");
                        }
                        // Kiem tra neu mon nay la mon tinh diem co he so = 0 (khong co he so) trong ky thi khong xem cho viec tong ket diem va xet hoc luc
                        // ky 1
                        if (Semester == SystemParamsInFile.SEMESTER_OF_YEAR_FIRST)
                        {
                            // He so mon hoc trong ky 1
                            if ((classSubject.IsCommenting == SystemParamsInFile.ISCOMMENTING_TYPE_MARK || classSubject.IsCommenting == SystemParamsInFile.ISCOMMENTING_TYPE_MARK_JUDGE)
                                && (!classSubject.FirstSemesterCoefficient.HasValue || classSubject.FirstSemesterCoefficient.Value == 0))
                            {
                                continue;
                            }
                        }
                        else
                        {
                            // He so mon hoc trong ky 2
                            if ((classSubject.IsCommenting == SystemParamsInFile.ISCOMMENTING_TYPE_MARK || classSubject.IsCommenting == SystemParamsInFile.ISCOMMENTING_TYPE_MARK_JUDGE)
                                && (!classSubject.SecondSemesterCoefficient.HasValue || classSubject.SecondSemesterCoefficient.Value == 0))
                            {
                                continue;
                            }
                        }
                        // Gan vao danh sach diem tinh trung binh mon hay la mon cong diem
                        if (SummedUpRecord.PupilID.Equals(PupilRanking.PupilID))
                        {
                            if (classSubject != null && classSubject.AppliedType == SystemParamsInFile.SUBJECT_TYPE_VOLUNTARY_WITH_PLUS_PRIORITIZE)
                            {
                                lstPupilRankingOfPupil_Voluntary.Add(SummedUpRecord);
                            }
                            else
                            {
                                lstPupilRankingOfPupil_Mandatory.Add(SummedUpRecord);
                            }
                        }
                    }

                    // Neu khong co thong tin diem thi bo qua
                    if (lstPupilRankingOfPupil_Mandatory.Count == 0)
                    {
                        continue;
                    }

                    // Tinh diem
                    decimal? AverageMark = PupilRankingBusiness.CaculatorAverageSubject(lstPupilRankingOfPupil_Mandatory, lstSubject, Semester, lstRegisterSubjectBO);

                    // Tinh them diem trung binh trong danh sach con lai
                    foreach (SummedUpRecord SummedUpRecord in lstPupilRankingOfPupil_Voluntary)
                    {
                        if (SummedUpRecord.SummedUpMark.HasValue)
                        {
                            decimal SummedUpMark = SummedUpRecord.SummedUpMark.Value;
                            if (SummedUpMark >= GlobalConstants.EXCELLENT_MARK)
                            {
                                AverageMark += GlobalConstants.ADD_EXCELLENT_MARK;
                            }
                            else if (SummedUpMark >= GlobalConstants.GOOD_MARK)
                            {
                                AverageMark += GlobalConstants.ADD_GOOD_MARK;
                            }
                            else if (SummedUpMark >= GlobalConstants.NORMAL_MARK)
                            {
                                AverageMark += GlobalConstants.ADD_NORMAL_MARK;
                            }
                        }
                    }

                    if (AverageMark > GlobalConstants.MAX_MARK)
                    {
                        AverageMark = GlobalConstants.MAX_MARK;
                    }

                    // Lay thong tin cua xep loai Capacity
                    int? CapacityID = null;
                    if (AverageMark != null)
                    {
                        CapacityID = PupilRankingBusiness.GetCapacityTypeByMark(AverageMark, lstPupilRankingOfPupil_Mandatory, lstSubject, SchoolID, traningType, lstRegisterSubjectBO);
                    }

                    // Xep hang hoc sinh
                    // Lay thong tin xep hang
                    if (CapacityID.HasValue)
                    {
                        PupilRankingHistory pr = new PupilRankingHistory();
                        bool isNotRank = false;
                        int ConductLevelID_Tot = 0;
                        if (isGDTX)
                        {
                            var poc = listPOC.FirstOrDefault(u => u.PupilID == PupilRanking.PupilID);
                            isNotRank = PupilProfileBusiness.IsNotConductRankingPupil(isGDTX, AppliedLevel, poc.PupilLearningType, poc.BirthDate);
                            if (AppliedLevel == SystemParamsInFile.APPLIED_LEVEL_SECONDARY)
                            {
                                ConductLevelID_Tot = SystemParamsInFile.CONDUCT_TYPE_GOOD_SECONDARY;
                            }
                            else if (AppliedLevel == SystemParamsInFile.APPLIED_LEVEL_TERTIARY)
                            {
                                ConductLevelID_Tot = SystemParamsInFile.CONDUCT_TYPE_GOOD_TERTIARY;
                            }
                            if (isNotRank)
                            {
                                listPupilIDNotRankConduct.Add(poc.PupilID);
                            }
                        }
                        pr.PupilID = PupilRanking.PupilID;
                        pr.ClassID = PupilRanking.ClassID;
                        pr.SchoolID = PupilRanking.SchoolID;
                        pr.AcademicYearID = PupilRanking.AcademicYearID;
                        pr.CreatedAcademicYear = academicYear.Year;
                        pr.Last2digitNumberSchool = PupilRanking.SchoolID % 100;
                        pr.Semester = PupilRanking.Semester;
                        pr.AverageMark = AverageMark;
                        pr.CapacityLevelID = CapacityID;
                        pr.ConductLevelID = isNotRank ? ConductLevelID_Tot : PupilRanking.ConductLevelID;
                        pr.PeriodID = PeriodID;
                        pr.EducationLevelID = EducationLevelID;
                        pr.RankingDate = DateTime.Now;
                        pr.MSourcedb = AutoMark;
                        ListPupilRanking4Save.Add(pr);
                    }
                }

                if (ListPupilRanking4Save.Count() > 0)
                {
                    if ((academicYear.RankingCriteria == SystemParamsInFile.RANKING_CRITERIA_AVERAGE_CONDUCT
                        || academicYear.RankingCriteria == SystemParamsInFile.RANKING_CRITERIA_AVERAGE_CAPACITY_CONDUCT))
                    {
                        if ((ListPupilRanking4Save.Any(o => o.ConductLevelID.HasValue)))
                        {

                            List<PupilRankingHistory> ListPupilRankingConduct = ListPupilRanking4Save.Where(o => o.ConductLevelID.HasValue).ToList();
                            this.Rank(academicYear.RankingCriteria, ListPupilRankingConduct);
                            ListPupilRanking4Save.RemoveAll(o => o.ConductLevelID.HasValue);
                            ListPupilRanking4Save.AddRange(ListPupilRankingConduct);
                        }
                    }
                    else
                    {
                        // Neu ko lien quan den hanh kiem thi chi can co diem trung binh la du xep hang
                        this.Rank(academicYear.RankingCriteria, ListPupilRanking4Save);
                    }

                    foreach (PupilRankingHistory item in ListPupilRanking4Save)
                    {

                        PupilRankingHistory PupilRankingUpdated = listPupilRankingByClass.Where(o => o.PupilID == item.PupilID).FirstOrDefault();
                        if (PupilRankingUpdated != null)
                        {
                            // Neu da co thong tin tu truoc
                            if (Semester == SystemParamsInFile.SEMESTER_OF_YEAR_ALL && listPupilRetestID.Contains(item.PupilID))
                            {
                                // Neu thuoc danh sach thi lai
                                // Neu da co diem thi lai thi chi cap nhat diem thi lai
                                PupilRankingHistory prBackTraining = lstPupilRankingRe.Where(o => o.PupilID == item.PupilID).FirstOrDefault();
                                if (prBackTraining != null)
                                {
                                    // Neu cp diem thi lai roi
                                    prBackTraining.AverageMark = item.AverageMark;
                                    prBackTraining.CapacityLevelID = item.CapacityLevelID;
                                    prBackTraining.RankingDate = DateTime.Now;
                                    prBackTraining.Rank = item.Rank;
                                    prBackTraining.MSourcedb = AutoMark;
                                    base.Update(prBackTraining);
                                }
                                else
                                {
                                    PupilRankingHistory prRetest = new PupilRankingHistory();
                                    prRetest.PupilID = item.PupilID;
                                    prRetest.ClassID = item.ClassID;
                                    prRetest.SchoolID = item.SchoolID;
                                    prRetest.AcademicYearID = item.AcademicYearID;
                                    prRetest.CreatedAcademicYear = academicYear.Year;
                                    prRetest.Last2digitNumberSchool = item.SchoolID % 100;
                                    prRetest.Semester = SystemParamsInFile.SEMESTER_OF_YEAR_BACKTRAINING;
                                    prRetest.AverageMark = item.AverageMark;
                                    prRetest.CapacityLevelID = item.CapacityLevelID;
                                    prRetest.ConductLevelID = item.ConductLevelID;
                                    prRetest.PeriodID = PeriodID;
                                    prRetest.EducationLevelID = EducationLevelID;
                                    prRetest.RankingDate = DateTime.Now;
                                    prRetest.MSourcedb = AutoMark;
                                    base.Insert(prRetest);
                                }
                            }
                            else
                            {
                                // Neu khong thuoc danh sach thi lai thi cap nhat vao diem ca nam
                                PupilRankingUpdated.AverageMark = item.AverageMark;
                                PupilRankingUpdated.CapacityLevelID = item.CapacityLevelID;
                                PupilRankingUpdated.RankingDate = DateTime.Now;
                                PupilRankingUpdated.Rank = item.Rank;
                                PupilRankingUpdated.MSourcedb = AutoMark;
                                base.Update(PupilRankingUpdated);
                            }
                        }
                        else
                        {
                            // Neu khong thuoc dien xep loai hanh kiem thi cap nhat lai hanh kiem la null
                            if (listPupilIDNotRankConduct.Contains(item.PupilID))
                            {
                                item.MSourcedb = AutoMark;
                                item.ConductLevelID = null;
                            }
                            base.Insert(item);
                        }
                    }
                }
                this.Save();
            }

            finally
            {
                SetAutoDetectChangesEnabled(true);
            }
        }


        /// <summary>
        /// RankingPupilOfClassNotRank: chi thuc hien tong ket, ko xep hang
        /// </summary>
        /// <param name="UserID"></param>
        /// <param name="lstSummedUpRecord"></param>
        /// <param name="lstPupilRanking"></param>
        /// <param name="PeriodID"></param>
        /// <returns></returns>
        public void RankingPupilOfClassNotRankHistory(int UserID, List<SummedUpRecord> lstSummedUpRecord, List<PupilRanking> lstPupilRanking, int? PeriodID, List<ClassSubject> lstSubject, bool isPassRetest = false, List<RegisterSubjectSpecializeBO> lstRegisterSubjectBO = null)
        {
            try
            {
                if (lstSummedUpRecord == null || lstSummedUpRecord.Count == 0)
                    throw new BusinessException("PupilRanking_Label_AllPupilNotMark");
                if (lstPupilRanking == null || lstPupilRanking.Count == 0)
                    return;
                SetAutoDetectChangesEnabled(false);
                SummedUpRecord firstSum = lstSummedUpRecord[0];

                int schoolID = firstSum.SchoolID;
                int academicYearID = firstSum.AcademicYearID;
                int classID = firstSum.ClassID;
                int semester = firstSum.Semester.Value;
                int modSchoolID = schoolID % 100;
                // Kiem tra neu he so mon cua lop hoc ko co thi bao loi
                if (semester == SystemParamsInFile.SEMESTER_OF_YEAR_FIRST)
                {
                    if (!lstSubject.Where(o => o.IsCommenting == SystemParamsInFile.ISCOMMENTING_TYPE_MARK || o.IsCommenting == SystemParamsInFile.ISCOMMENTING_TYPE_MARK_JUDGE)
                        .Any(o => o.FirstSemesterCoefficient.HasValue && o.FirstSemesterCoefficient.Value > 0))
                    {
                        throw new BusinessException("PupilRanking_NotPupil_HS");
                    }
                }
                else if (semester == SystemParamsInFile.SEMESTER_OF_YEAR_SECOND)
                {
                    if (!lstSubject.Where(o => o.IsCommenting == SystemParamsInFile.ISCOMMENTING_TYPE_MARK || o.IsCommenting == SystemParamsInFile.ISCOMMENTING_TYPE_MARK_JUDGE)
                        .Any(o => o.SecondSemesterCoefficient.HasValue && o.SecondSemesterCoefficient.Value > 0))
                    {
                        throw new BusinessException("PupilRanking_NotPupil_HS");
                    }
                }

                #region Validate du lieu dau vao

                var listPOC = (from poc in PupilOfClassBusiness.AllNoTracking
                               join pp in PupilProfileBusiness.All on poc.PupilID equals pp.PupilProfileID
                               where poc.ClassID == classID && poc.Status == SystemParamsInFile.PUPIL_STATUS_STUDYING && pp.IsActive
                               select new
                               {
                                   CurrentClassID = poc.ClassID,
                                   CurrentAcademicYearID = poc.AcademicYearID,
                                   poc.PupilID,
                                   poc.Status
                               })
                    .ToList();
                List<ExemptedSubject> listExempteds = ExemptedSubjectBusiness.GetListExemptedSubject(classID, semester).ToList();
                AcademicYear academicYear = AcademicYearBusiness.Find(academicYearID);

                // Kiem tra nam hoc hien tai
                if (academicYear == null || !AcademicYearBusiness.IsCurrentYear(academicYear))
                    throw new BusinessException("ClassMovement_Validate_IsNotCurrentYear");


                //SchoolID, AcademicYearID: not compatible(AcademicYearID)
                if (academicYear.SchoolID != schoolID)
                {
                    throw new BusinessException("Common_Validate_NotCompatible");
                }

                //neu ton tai mot hoc sinh duoc mien giam thi bao loi
                if (listExempteds.Any(u => lstSummedUpRecord.Any(v => v.PupilID == u.PupilID && v.SubjectID == u.SubjectID)))
                    throw new BusinessException("SummedUpRecord_Labal_ErrMGSubject");

                foreach (SummedUpRecord SummedUpRecord in lstSummedUpRecord)
                {
                    ValidationMetadata.ValidateObject(SummedUpRecord);
                    if (!lstSubject.Any(u => u.ClassID == SummedUpRecord.ClassID && u.SubjectID == SummedUpRecord.SubjectID))
                        throw new BusinessException("Common_Validate_NotCompatible");

                    var poc = listPOC.FirstOrDefault(u => u.PupilID == SummedUpRecord.PupilID);
                    if (poc == null || poc.Status != SystemParamsInFile.PUPIL_STATUS_STUDYING)
                        throw new BusinessException("ClassMovement_Validate_PupilNotWorking");

                    if (poc.CurrentClassID != SummedUpRecord.ClassID)
                        throw new BusinessException("Common_Validate_NotCompatible");

                    if (poc.CurrentAcademicYearID != SummedUpRecord.AcademicYearID)
                        throw new BusinessException("Common_Validate_NotCompatible");
                }
                #endregion

                //Chuyen danh sach sang history
                List<PupilRankingHistory> lstPupilRankingHistory = ConvertListToHistory(lstPupilRanking);

                // Process
                // Lay 2 danh sach tu 2 danh sach dau vao join qua dieu kien pupilID            
                List<int> listCheckedPupilID = new List<int>();
                PupilRanking firstRanking = lstPupilRanking[0];
                int EducationLevelID = firstRanking.EducationLevelID;

                SchoolProfile schoolProfile = SchoolProfileBusiness.Find(schoolID);
                TrainingType traningType = schoolProfile.TrainingType;

                // Lay du lieu trong PupilRanking theo truong, nam hoc, ky, dot neu co truoc
                Dictionary<string, object> dicRanking = new Dictionary<string, object>();
                dicRanking["AcademicYearID"] = academicYearID;
                dicRanking["ClassID"] = classID;
                dicRanking["Semester"] = semester;
                if (PeriodID.HasValue)
                {
                    dicRanking["PeriodID"] = PeriodID.Value;
                }
                else
                {
                    dicRanking["PeriodID"] = null;
                }


                List<PupilRankingHistory> listPupilRankingByClass = this.SearchBySchool(schoolID, dicRanking).ToList();
                List<PupilRankingHistory> lstPupilRankingRe = new List<PupilRankingHistory>();

                if (!PeriodID.HasValue)
                {
                    listPupilRankingByClass = listPupilRankingByClass.Where(o => !o.PeriodID.HasValue).ToList();
                }
                List<int> listPupilRetestID = listPupilRankingByClass.Where(o => o.StudyingJudgementID == SystemParamsInFile.STUDYING_JUDGEMENT_RETEST)
                   .Select(o => o.PupilID).ToList();
                //Neu hoc ky la ca nam thi lay ra cac ban ghi thi lai, ren luyen lai de cap nhat.
                // Lay danh sach hoc sinh da co nhap diem thi lai roi thi moi tinh vao truong hop nay
                // Neu chi moi xep loai la thi lai ma chua nhap diem thi lai thi van cap nhat vao ban ghi ca nam

                if (semester == SystemParamsInFile.SEMESTER_OF_YEAR_ALL)
                {
                    if (listPupilRetestID.Count > 0)
                    {
                        dicRanking["Semester"] = SystemParamsInFile.SEMESTER_OF_YEAR_BACKTRAINING;
                        lstPupilRankingRe = this.Search(dicRanking).ToList();
                        List<int> listPupilIDInputMarkRetest = SummedUpRecordBusiness.SearchBySchool(schoolID, new Dictionary<string, object>()
                {
                    {"AcademicYearID", academicYearID},
                    {"ClassID", classID}
                }).Where(o => o.Semester > SystemParamsInFile.SEMESTER_OF_YEAR_ALL && !o.PeriodID.HasValue).Select(o => o.PupilID).ToList();
                        listPupilRetestID.RemoveAll(o => !listPupilIDInputMarkRetest.Contains(o));
                    }
                }
                Dictionary<string, ClassSubject> dicClassSubject = new Dictionary<string, ClassSubject>();
                List<SummedUpRecord> lstSummedUpRecordCheck = new List<SummedUpRecord>();
                foreach (PupilRankingHistory PupilRanking in lstPupilRankingHistory)
                {
                    List<SummedUpRecord> lstPupilRankingOfPupil_Mandatory = new List<SummedUpRecord>();
                    List<SummedUpRecord> lstPupilRankingOfPupil_Voluntary = new List<SummedUpRecord>();
                    if (listCheckedPupilID.Contains(PupilRanking.PupilID))
                    {
                        continue;
                    }
                    listCheckedPupilID.Add(PupilRanking.PupilID);
                    //Sua lai vong lap
                    lstSummedUpRecordCheck = lstSummedUpRecord.Where(s => s.PupilID == PupilRanking.PupilID).ToList();
                    if (lstSummedUpRecordCheck == null || lstSummedUpRecordCheck.Count == 0)
                    {
                        continue;
                    }

                    foreach (SummedUpRecord SummedUpRecord in lstSummedUpRecordCheck)
                    {
                        string key = SummedUpRecord.ClassID + "_" + SummedUpRecord.SubjectID;
                        // Lay mon hoc cua lop
                        ClassSubject classSubject = null;
                        if (dicClassSubject.ContainsKey(key))
                        {
                            classSubject = dicClassSubject[key];
                        }
                        else
                        {
                            classSubject = lstSubject.FirstOrDefault(u => u.ClassID == SummedUpRecord.ClassID && u.SubjectID == SummedUpRecord.SubjectID);
                            dicClassSubject[key] = classSubject;
                        }
                        if (classSubject == null)
                        {
                            throw new BusinessException("Không tồn tại môn học của lớp");
                        }
                        // Kiem tra neu mon nay khong co he so = 0 trong ky thi khong xem cho viec tong ket diem va xet hoc luc
                        // ky 1
                        if (semester == SystemParamsInFile.SEMESTER_OF_YEAR_FIRST)
                        {
                            // He so mon hoc trong ky 1
                            // Chi xet cho mon tinh diem
                            if ((classSubject.IsCommenting == SystemParamsInFile.ISCOMMENTING_TYPE_MARK || classSubject.IsCommenting == SystemParamsInFile.ISCOMMENTING_TYPE_MARK_JUDGE)
                            && (!classSubject.FirstSemesterCoefficient.HasValue || classSubject.FirstSemesterCoefficient.Value == 0))
                            {
                                continue;
                            }
                        }
                        else
                        {
                            // He so mon hoc trong ky 2
                            if ((classSubject.IsCommenting == SystemParamsInFile.ISCOMMENTING_TYPE_MARK || classSubject.IsCommenting == SystemParamsInFile.ISCOMMENTING_TYPE_MARK_JUDGE)
                            && (!classSubject.SecondSemesterCoefficient.HasValue || classSubject.SecondSemesterCoefficient.Value == 0))
                            {
                                continue;
                            }
                        }

                        // Gan vao danh sach diem tinh trung binh mon hay la mon cong diem
                        if (classSubject != null && classSubject.AppliedType == SystemParamsInFile.SUBJECT_TYPE_VOLUNTARY_WITH_PLUS_MARK)
                        {
                            lstPupilRankingOfPupil_Voluntary.Add(SummedUpRecord);
                        }
                        else
                        {
                            lstPupilRankingOfPupil_Mandatory.Add(SummedUpRecord);
                        }
                    }

                    // Neu khong co thong tin diem thi bo qua
                    if (lstPupilRankingOfPupil_Mandatory.Count == 0)
                    {
                        continue;
                    }

                    // Tinh diem
                    decimal? AverageMark = PupilRankingBusiness.CaculatorAverageSubject(lstPupilRankingOfPupil_Mandatory, lstSubject, semester, lstRegisterSubjectBO);

                    // Tinh them diem trung binh trong danh sach con lai
                    foreach (SummedUpRecord SummedUpRecord in lstPupilRankingOfPupil_Voluntary)
                    {
                        if (SummedUpRecord.SummedUpMark.HasValue)
                        {
                            decimal SummedUpMark = SummedUpRecord.SummedUpMark.Value;
                            if (SummedUpMark >= GlobalConstants.EXCELLENT_MARK)
                            {
                                AverageMark += GlobalConstants.ADD_EXCELLENT_MARK;
                            }
                            else if (SummedUpMark >= GlobalConstants.GOOD_MARK)
                            {
                                AverageMark += GlobalConstants.ADD_GOOD_MARK;
                            }
                            else if (SummedUpMark >= GlobalConstants.NORMAL_MARK)
                            {
                                AverageMark += GlobalConstants.ADD_NORMAL_MARK;
                            }
                        }
                    }

                    if (AverageMark > GlobalConstants.MAX_MARK)
                    {
                        AverageMark = GlobalConstants.MAX_MARK;
                    }

                    // Lay thong tin cua xep loai Capacity
                    int? CapacityID = null;
                    if (AverageMark.HasValue)
                    {
                        CapacityID = PupilRankingBusiness.GetCapacityTypeByMark(AverageMark, lstPupilRankingOfPupil_Mandatory, lstSubject, schoolID, traningType, lstRegisterSubjectBO);
                    }

                    // Luu du lieu vao bang PupilRanking
                    if (CapacityID.HasValue)
                    {
                        PupilRankingHistory PupilRankingUpdated = listPupilRankingByClass.FirstOrDefault(o => o.PupilID == PupilRanking.PupilID);
                        if (PupilRankingUpdated != null) // thuc hien update
                        {
                            // Neu da co thong tin tu truoc
                            if (semester == SystemParamsInFile.SEMESTER_OF_YEAR_ALL && !isPassRetest && listPupilRetestID.Contains(PupilRanking.PupilID))
                            {
                                // Neu thuoc danh sach thi lai
                                // Neu da co diem thi lai thi chi cap nhat diem thi lai
                                PupilRankingHistory prBackTraining = lstPupilRankingRe.FirstOrDefault(o => o.PupilID == PupilRanking.PupilID);
                                if (prBackTraining != null)
                                {
                                    // Neu cp diem thi lai roi
                                    prBackTraining.AverageMark = AverageMark;
                                    prBackTraining.CapacityLevelID = CapacityID;
                                    prBackTraining.RankingDate = DateTime.Now;
                                    prBackTraining.Rank = PupilRanking.Rank;
                                    prBackTraining.MSourcedb = "1";
                                    base.Update(prBackTraining);
                                }
                                else
                                {
                                    PupilRankingHistory prRetest = new PupilRankingHistory();
                                    prRetest.PupilID = PupilRanking.PupilID;
                                    prRetest.ClassID = PupilRanking.ClassID;
                                    prRetest.SchoolID = PupilRanking.SchoolID;
                                    prRetest.AcademicYearID = PupilRanking.AcademicYearID;
                                    prRetest.CreatedAcademicYear = academicYear.Year;
                                    prRetest.Semester = SystemParamsInFile.SEMESTER_OF_YEAR_BACKTRAINING;
                                    prRetest.AverageMark = AverageMark;
                                    prRetest.CapacityLevelID = CapacityID;
                                    prRetest.ConductLevelID = PupilRanking.ConductLevelID;
                                    prRetest.PeriodID = PeriodID;
                                    prRetest.EducationLevelID = EducationLevelID;
                                    prRetest.RankingDate = DateTime.Now;
                                    prRetest.Rank = PupilRanking.Rank;
                                    prRetest.Last2digitNumberSchool = modSchoolID;
                                    prRetest.MSourcedb = "0";
                                    this.Insert(prRetest);
                                }
                            }
                            else
                            {
                                // Neu khong thuoc danh sach thi lai thi cap nhat vao diem ca nam
                                PupilRankingUpdated.AverageMark = AverageMark;
                                PupilRankingUpdated.CapacityLevelID = CapacityID;
                                PupilRankingUpdated.RankingDate = DateTime.Now;
                                PupilRankingUpdated.Rank = PupilRanking.Rank;
                                //PupilRankingUpdated.MSourcedb = "0";
                                base.Update(PupilRankingUpdated);
                            }
                        }
                        else // thuc hien insert
                        {
                            PupilRankingHistory pr = new PupilRankingHistory();
                            pr.PupilID = PupilRanking.PupilID;
                            pr.ClassID = PupilRanking.ClassID;
                            pr.SchoolID = PupilRanking.SchoolID;
                            pr.AcademicYearID = PupilRanking.AcademicYearID;
                            pr.CreatedAcademicYear = academicYear.Year;
                            pr.Last2digitNumberSchool = modSchoolID;
                            pr.Semester = PupilRanking.Semester;
                            pr.AverageMark = AverageMark;
                            pr.CapacityLevelID = CapacityID;
                            pr.PeriodID = PeriodID;
                            pr.EducationLevelID = EducationLevelID;
                            pr.RankingDate = DateTime.Now;
                            pr.MSourcedb = "0";
                            this.Insert(pr);
                        }
                    }
                }
            }
            finally
            {
                SetAutoDetectChangesEnabled(true);
            }
        }


        public void ClassifyPupilHistory(List<PupilRankingBO> lstPupilRankingBO, int Semester, bool isShowRetetResult, string auto)
        {
            try
            {
                if (lstPupilRankingBO == null || lstPupilRankingBO.Count == 0)
                {
                    return;
                }
                //SetAutoDetectChangesEnabled(true);
                List<PupilRankingHistory> lstPupilRankingUpdate = new List<PupilRankingHistory>();
                
                PupilRankingBO pro = lstPupilRankingBO.FirstOrDefault();
                int SchoolID = pro.SchoolID.Value;
                int ClassID = pro.ClassID.Value;
                int AcademicYearID = pro.AcademicYearID.Value;
                AcademicYear objAcademicYear = AcademicYearBusiness.Find(AcademicYearID);
                ClassProfile cp = ClassProfileBusiness.Find(ClassID);
                EducationLevel EducationLevel = cp.EducationLevel;
                int AppliedLevel = EducationLevel.Grade;
                int partitionId = UtilsBusiness.GetPartionId(SchoolID);
                var HonourType1 = HonourAchivementTypeBusiness.All.Where(o => o.Resolution == GlobalConstants.HONOURACHIVEMENTTYPE_RESOLUTION_GOOD
                                && o.Type == GlobalConstants.HONOUR_ACHIVEMENT_TYPE_PUPIL).FirstOrDefault();
                var HonourType2 = HonourAchivementTypeBusiness.All.Where(o => o.Resolution == GlobalConstants.HONOURACHIVEMENTTYPE_RESOLUTION_EXCELLENT
                               && o.Type == GlobalConstants.HONOUR_ACHIVEMENT_TYPE_PUPIL).FirstOrDefault();
                List<int> lstHKPass = new List<int> {GlobalConstants.CONDUCT_TYPE_GOOD_SECONDARY, GlobalConstants.CONDUCT_TYPE_FAIR_SECONDARY, GlobalConstants.CONDUCT_TYPE_NORMAL_SECONDARY,
                                                 GlobalConstants.CONDUCT_TYPE_GOOD_TERTIARY, GlobalConstants.CONDUCT_TYPE_FAIR_TERTIARY, GlobalConstants.CONDUCT_TYPE_NORMAL_TERTIARY};
                string strGDTX = "GDTX";
                List<int> lstPupilID = lstPupilRankingBO.Select(o => o.PupilID.Value).Distinct().ToList();

                SchoolProfile sp = SchoolProfileBusiness.Find(SchoolID);
                bool isGDTX = (sp.TrainingType != null && sp.TrainingType.Resolution == strGDTX);



                Dictionary<string, object> dic = new Dictionary<string, object>();
                dic["AcademicYearID"] = pro.AcademicYearID;
                dic["SchoolID"] = pro.SchoolID;
                dic["ClassID"] = pro.ClassID;

                //Chi lay HS thi lai khi HK la ca nam va co hien thi ket qua thi lai
                List<PupilRetestRegistration> listP = new List<PupilRetestRegistration>();
                List<int> listPupil = new List<int>();
                List<int> lstPupilRetest = new List<int>();
                if (Semester == GlobalConstants.SEMESTER_OF_YEAR_ALL && isShowRetetResult)
                {
                    listP = PupilRetestRegistrationBusiness.SearchPupilRetestRegistration(dic).ToList();
                    listPupil = listP.Where(o => o.ModifiedDate == null).Select(o => o.PupilID).Distinct().ToList();
                    lstPupilRetest = listP.Select(o => o.PupilID).Distinct().ToList();
                }

                dic = new Dictionary<string, object>();
                dic["AcademicYearID"] = pro.AcademicYearID;
                dic["ClassID"] = pro.ClassID;
                dic["Semester"] = Semester;
                List<PupilEmulation> lstPupilEmulation = PupilEmulationBusiness.SearchBySchool(pro.SchoolID.Value, dic).ToList();
                if (lstPupilEmulation.Any())
                {
                    PupilEmulationBusiness.DeleteAll(lstPupilEmulation);
                }

                dic = new Dictionary<string, object>();
                dic["AcademicYearID"] = pro.AcademicYearID;
                dic["ClassID"] = pro.ClassID;
                List<PupilRankingHistory> lstPupilRanking = PupilRankingHistoryBusiness.SearchBySchool(pro.SchoolID.Value, dic).Where(o => o.PeriodID == null).ToList();

                if (lstPupilRankingBO != null && lstPupilRankingBO.Count() > 0)
                {
                    //Step1: Lấy từng phần tử trong lstPupilToRank.
                    foreach (var PupilRankingBO in lstPupilRankingBO)
                    {
                        int TotalAbsentDaysWithPermission = PupilRankingBO.TotalAbsentDaysWithPermission.HasValue ? PupilRankingBO.TotalAbsentDaysWithPermission.Value : 0;
                        int TotalAbsentDaysWithoutPermission = PupilRankingBO.TotalAbsentDaysWithoutPermission.HasValue ? PupilRankingBO.TotalAbsentDaysWithoutPermission.Value : 0;
                        bool isKXLHK = PupilProfileBusiness.IsNotConductRankingPupil(isGDTX, AppliedLevel, PupilRankingBO.LearningType, PupilRankingBO.BirthDate);
                        if (isKXLHK)
                        {
                            if (AppliedLevel == SystemParamsInFile.APPLIED_LEVEL_SECONDARY)
                            {
                                PupilRankingBO.ConductLevelID = GlobalConstants.CONDUCT_TYPE_GOOD_SECONDARY;
                            }
                            else
                            {
                                PupilRankingBO.ConductLevelID = GlobalConstants.CONDUCT_TYPE_GOOD_TERTIARY;
                            }
                        }
                        if (PupilRankingBO.CapacityLevelID.HasValue) //Nếu có học lực, hạnh kiểm thì mới xếp loại
                        {
                            if (PupilRankingBO.TotalAbsentDaysWithPermission == null)
                            {
                                PupilRankingBO.TotalAbsentDaysWithPermission = 0;
                            }
                            if (PupilRankingBO.TotalAbsentDaysWithoutPermission == null)
                            {
                                PupilRankingBO.TotalAbsentDaysWithoutPermission = 0;
                            }
                            PupilRankingBO.HonourAchivementTypeID = null;

                            //Nếu học lực khá trở lên, hạnh kiểm khá trở lên buổi thì đạt học sinh tiên tiến
                            if (PupilRankingBO.Semester != SystemParamsInFile.SEMESTER_OF_YEAR_BACKTRAINING && ((!lstPupilRetest.Contains(PupilRankingBO.PupilID.Value) && Semester == GlobalConstants.SEMESTER_OF_YEAR_ALL) || Semester != GlobalConstants.SEMESTER_OF_YEAR_ALL) &&
                                (PupilRankingBO.CapacityLevelID == GlobalConstants.CAPACITY_TYPE_EXCELLENT || PupilRankingBO.CapacityLevelID == GlobalConstants.CAPACITY_TYPE_GOOD)

                                && (((PupilRankingBO.ConductLevelID == GlobalConstants.CONDUCT_TYPE_GOOD_SECONDARY || PupilRankingBO.ConductLevelID == GlobalConstants.CONDUCT_TYPE_FAIR_SECONDARY
                                    || PupilRankingBO.ConductLevelID == GlobalConstants.CONDUCT_TYPE_GOOD_TERTIARY || PupilRankingBO.ConductLevelID == GlobalConstants.CONDUCT_TYPE_FAIR_TERTIARY)))
                                )
                            {

                                if (HonourType1 != null)
                                    PupilRankingBO.HonourAchivementTypeID = HonourType1.HonourAchivementTypeID;
                            }


                            //Nếu học lực giỏi, hạnh kiểm tốt, buổi thì đạt học sinh giỏi
                            if (PupilRankingBO.Semester != SystemParamsInFile.SEMESTER_OF_YEAR_BACKTRAINING && ((!lstPupilRetest.Contains(PupilRankingBO.PupilID.Value) && Semester == GlobalConstants.SEMESTER_OF_YEAR_ALL) || Semester != GlobalConstants.SEMESTER_OF_YEAR_ALL) &&
                                PupilRankingBO.CapacityLevelID == GlobalConstants.CAPACITY_TYPE_EXCELLENT
                                && (((PupilRankingBO.ConductLevelID == GlobalConstants.CONDUCT_TYPE_GOOD_SECONDARY || PupilRankingBO.ConductLevelID == GlobalConstants.CONDUCT_TYPE_GOOD_TERTIARY)))
                                )
                            {

                                if (HonourType2 != null)
                                    PupilRankingBO.HonourAchivementTypeID = HonourType2.HonourAchivementTypeID;
                            }


                            if (PupilRankingBO.ProfileStatus == SystemParamsInFile.PUPIL_STATUS_STUDYING && (Semester == GlobalConstants.SEMESTER_OF_YEAR_FIRST || Semester == GlobalConstants.SEMESTER_OF_YEAR_SECOND))
                            {
                                var pr = lstPupilRanking.FirstOrDefault(o => o.PupilRankingID == PupilRankingBO.PupilRankingID);
                                if (pr != null)
                                {
                                    pr.IsCategory = true;
                                    pr.MSourcedb = auto;
                                    //PupilRankingHistoryBusiness.Update(pr);
                                    lstPupilRankingUpdate.Add(pr);
                                }
                            }
                            //Xét thuộc diện: Chỉ với Semester = 3
                            if (Semester == GlobalConstants.SEMESTER_OF_YEAR_ALL)
                            {
                                // Neu la lop cuoi cap thi xu ly khac
                                if (EducationLevel.IsLastYear)
                                {
                                    // Neu la cap 3 thi HL >= Yeu va HK >= TB la du dieu kien du thi tot ngiep
                                    // Cap 2 thi HL >= TB
                                    if (EducationLevel.Grade == SystemParamsInFile.APPLIED_LEVEL_TERTIARY || EducationLevel.Grade == SystemParamsInFile.APPLIED_LEVEL_SECONDARY)
                                    {
                                        // Cap 2, 3
                                        if ((PupilRankingBO.ConductLevelID == GlobalConstants.CONDUCT_TYPE_GOOD_SECONDARY ||
                                        PupilRankingBO.ConductLevelID == GlobalConstants.CONDUCT_TYPE_FAIR_SECONDARY ||
                                        PupilRankingBO.ConductLevelID == GlobalConstants.CONDUCT_TYPE_NORMAL_SECONDARY ||
                                        PupilRankingBO.ConductLevelID == GlobalConstants.CONDUCT_TYPE_GOOD_TERTIARY ||
                                        PupilRankingBO.ConductLevelID == GlobalConstants.CONDUCT_TYPE_FAIR_TERTIARY ||
                                        PupilRankingBO.ConductLevelID == GlobalConstants.CONDUCT_TYPE_NORMAL_TERTIARY))
                                        {
                                            if (EducationLevel.Grade == SystemParamsInFile.APPLIED_LEVEL_TERTIARY)
                                            {
                                                // Cap 3 thi HL tu Yeu tro len la duoc du dk du thi tot nghiep
                                                if (PupilRankingBO.CapacityLevelID <= GlobalConstants.CAPACITY_TYPE_WEAK)
                                                {
                                                    PupilRankingBO.StudyingJudgementID = GlobalConstants.STUDYING_JUDGEMENT_GRADUATION;
                                                }
                                                else
                                                {
                                                    PupilRankingBO.StudyingJudgementID = null;
                                                }
                                            }
                                            else
                                            {
                                                // Cap 2 thi HL tu TB tro len la duoc du dk du thi tot nghiep
                                                if (PupilRankingBO.CapacityLevelID <= GlobalConstants.CAPACITY_TYPE_NORMAL)
                                                {
                                                    PupilRankingBO.StudyingJudgementID = GlobalConstants.STUDYING_JUDGEMENT_GRADUATION;
                                                }
                                                else
                                                {
                                                    PupilRankingBO.StudyingJudgementID = null;
                                                }
                                            }
                                        }
                                        else
                                        {
                                            PupilRankingBO.StudyingJudgementID = null;
                                        }
                                    }

                                }
                                else
                                {
                                    // Nếu nghỉ quá 45 buổi => ở lại lớp
                                    if (((PupilRankingBO.TotalAbsentDaysWithPermission + PupilRankingBO.TotalAbsentDaysWithoutPermission > 45 && !isGDTX)
                                        || (PupilRankingBO.TotalAbsentDaysWithPermission + PupilRankingBO.TotalAbsentDaysWithoutPermission > 45 && isGDTX)))
                                    {
                                        PupilRankingBO.StudyingJudgementID = GlobalConstants.STUDYING_JUDGEMENT_STAYCLASS;
                                    }
                                    //Else
                                    //Nếu lstPupilToRank[i].CapacityLevelID = CAPACITY_TYPE_POOR thì
                                    //lstPupilToRank[i].StudyingJudgementID = STUDYING_JUDGEMENT_STAYCLASS (4)
                                    //Hạnh kiểm cả năm xếp loại yếu mà rèn luyện lại vẫn không đạt => ở lại lớp
                                    else if (PupilRankingBO.CapacityLevelID == GlobalConstants.CAPACITY_TYPE_POOR)
                                    {
                                        PupilRankingBO.StudyingJudgementID = GlobalConstants.STUDYING_JUDGEMENT_STAYCLASS;
                                    }
                                    //Else
                                    //Nếu lstPupilToRank[i].ConductLevelID  = CONDUCT_TYPE_WEAK_SECONDARY/ CONDUCT_TYPE_WEAK_TERTIARY && lstPupilToRank[i].pr.Semester = 4 thì:
                                    //lstPupilToRank[i].StudyingJudgementID = STUDYING_JUDGEMENT_STAYCLASS (4)
                                    else if ((PupilRankingBO.ConductLevelID == GlobalConstants.CONDUCT_TYPE_WEAK_SECONDARY
                                        || PupilRankingBO.ConductLevelID == GlobalConstants.CONDUCT_TYPE_WEAK_TERTIARY)
                                        && PupilRankingBO.Semester == SystemParamsInFile.SEMESTER_OF_YEAR_BACKTRAINING)
                                    {
                                        PupilRankingBO.StudyingJudgementID = GlobalConstants.STUDYING_JUDGEMENT_STAYCLASS;
                                    }
                                    //Nếu hạnh kiểm yếu, học lực dưới TB thì ở lại lớp
                                    else if ((((PupilRankingBO.ConductLevelID == GlobalConstants.CONDUCT_TYPE_WEAK_SECONDARY ||
                                         PupilRankingBO.ConductLevelID == GlobalConstants.CONDUCT_TYPE_WEAK_TERTIARY)))
                                         && PupilRankingBO.CapacityLevelID == GlobalConstants.CAPACITY_TYPE_WEAK)
                                    {
                                        PupilRankingBO.StudyingJudgementID = GlobalConstants.STUDYING_JUDGEMENT_STAYCLASS;
                                    }
                                    //Nếu hạnh kiểm trên TB, học lực dưới TB thì ở lại lớp:
                                    else if ((PupilRankingBO.ConductLevelID == GlobalConstants.CONDUCT_TYPE_GOOD_SECONDARY ||
                                        PupilRankingBO.ConductLevelID == GlobalConstants.CONDUCT_TYPE_FAIR_SECONDARY ||
                                        PupilRankingBO.ConductLevelID == GlobalConstants.CONDUCT_TYPE_NORMAL_SECONDARY ||
                                        PupilRankingBO.ConductLevelID == GlobalConstants.CONDUCT_TYPE_GOOD_TERTIARY ||
                                        PupilRankingBO.ConductLevelID == GlobalConstants.CONDUCT_TYPE_FAIR_TERTIARY ||
                                        PupilRankingBO.ConductLevelID == GlobalConstants.CONDUCT_TYPE_NORMAL_TERTIARY)
                                        && PupilRankingBO.CapacityLevelID >= GlobalConstants.CAPACITY_TYPE_WEAK
                                        && !listPupil.Any(o => o == PupilRankingBO.PupilID) && listP.Where(o => o.PupilID == PupilRankingBO.PupilID).Count() > 0)
                                    {
                                        PupilRankingBO.StudyingJudgementID = GlobalConstants.STUDYING_JUDGEMENT_STAYCLASS;
                                    }
                                    //Nếu hạnh kiểm yếu, học lực trên TB thì rèn luyện lại:
                                    else if ((((PupilRankingBO.ConductLevelID == GlobalConstants.CONDUCT_TYPE_WEAK_SECONDARY ||
                                        PupilRankingBO.ConductLevelID == GlobalConstants.CONDUCT_TYPE_WEAK_TERTIARY)))
                                        && PupilRankingBO.CapacityLevelID < GlobalConstants.CAPACITY_TYPE_WEAK && PupilRankingBO.Semester != SystemParamsInFile.SEMESTER_OF_YEAR_BACKTRAINING)
                                    {
                                        // Doi voi lop cuoi cap thi de tron
                                        PupilRankingBO.StudyingJudgementID = GlobalConstants.STUDYING_JUDGEMENT_BACKTRAINING;
                                    }
                                    //Nếu hạnh kiểm trên TB, học lực dưới TB thì thi lại:
                                    else if ((((PupilRankingBO.ConductLevelID == GlobalConstants.CONDUCT_TYPE_GOOD_SECONDARY ||
                                        PupilRankingBO.ConductLevelID == GlobalConstants.CONDUCT_TYPE_FAIR_SECONDARY ||
                                        PupilRankingBO.ConductLevelID == GlobalConstants.CONDUCT_TYPE_NORMAL_SECONDARY ||
                                        PupilRankingBO.ConductLevelID == GlobalConstants.CONDUCT_TYPE_GOOD_TERTIARY ||
                                        PupilRankingBO.ConductLevelID == GlobalConstants.CONDUCT_TYPE_FAIR_TERTIARY ||
                                        PupilRankingBO.ConductLevelID == GlobalConstants.CONDUCT_TYPE_NORMAL_TERTIARY)))
                                        && PupilRankingBO.CapacityLevelID >= GlobalConstants.CAPACITY_TYPE_WEAK
                                        && (listPupil.Any(o => o == PupilRankingBO.PupilID) || listP.Count(o => o.PupilID == PupilRankingBO.PupilID) == 0))
                                    {
                                        PupilRankingBO.StudyingJudgementID = GlobalConstants.STUDYING_JUDGEMENT_RETEST;
                                    }
                                    else
                                    {
                                        //Các trường hợp còn lại được lên lớp
                                        PupilRankingBO.StudyingJudgementID = GlobalConstants.STUDYING_JUDGEMENT_UPCLASS;

                                    }
                                }
                                //Step2: Thực hiện update lstPupilToRank[i].pr vào bảng PupilRanking
                                var pr = lstPupilRanking.FirstOrDefault(o => o.PupilRankingID == PupilRankingBO.PupilRankingID);
                                if (pr != null)
                                {
                                    pr.ConductLevelID = PupilRankingBO.IsKXLHK ? new Nullable<Int32>() : PupilRankingBO.ConductLevelID;
                                    pr.CapacityLevelID = PupilRankingBO.CapacityLevelID;
                                    pr.StudyingJudgementID = PupilRankingBO.StudyingJudgementID;
                                    pr.IsCategory = true;
                                    pr.MSourcedb = auto;
                                    //PupilRankingHistoryBusiness.Update(pr);
                                    lstPupilRankingUpdate.Add(pr);
                                }

                            }
                            // DungVA
                            //Step 3:
                            /* Nếu lstPupilToRank[i].HonourAchivementTypeID = null
                             Thực hiện tìm kiếm trong bảng PupilEmulation theo PupilID, ClassID, SchoolID, AcademicYearID, Semester
                             */

                            if (PupilRankingBO.HonourAchivementTypeID.HasValue)
                            {
                                PupilEmulation pe = new PupilEmulation
                                {
                                    PupilID = PupilRankingBO.PupilID.Value,
                                    ClassID = PupilRankingBO.ClassID.Value,
                                    SchoolID = PupilRankingBO.SchoolID.Value,
                                    AcademicYearID = PupilRankingBO.AcademicYearID.Value,
                                    Semester = Semester,
                                    HonourAchivementTypeID = PupilRankingBO.HonourAchivementTypeID.Value,
                                    MSourcedb = auto,
                                    Last2digitNumberSchool = partitionId
                                };
                                PupilEmulationBusiness.Insert(pe);
                                
                            }

                        }
                    }
                }

                if (lstPupilRankingUpdate.Count > 0)
                {
                    for(int i = 0; i < lstPupilRankingUpdate.Count; i++)
                    {
                        PupilRankingHistoryBusiness.Update(lstPupilRankingUpdate[i]);
                    }
                }

                
                
                PupilRankingHistoryBusiness.Save();
            }
            finally
            {
                SetAutoDetectChangesEnabled(true);
            }
        }


        /// <summary>
        /// Từ học lực, hạnh kiểm, số buổi nghỉ => Xếp loại HS. Áp dụng cho cấp 2-3        
        /// </summary>
        /// <param name="lstPupilRankingBO"></param>
        public void ClassifyPupilAfterRetestHistory(List<PupilRankingBO> lstPupilRankingBO, List<PupilRetestRegistration> lstPupilRetest, int SchoolID, int AppliedLevel, int AcademicYearID)
        {
            try
            {
                SetAutoDetectChangesEnabled(false);
                List<int> lstHKPass = new List<int> {GlobalConstants.CONDUCT_TYPE_GOOD_SECONDARY, GlobalConstants.CONDUCT_TYPE_FAIR_SECONDARY, GlobalConstants.CONDUCT_TYPE_NORMAL_SECONDARY,
                                                 GlobalConstants.CONDUCT_TYPE_GOOD_TERTIARY, GlobalConstants.CONDUCT_TYPE_FAIR_TERTIARY, GlobalConstants.CONDUCT_TYPE_NORMAL_TERTIARY};
                string strGDTX = "GDTX";
                AcademicYear objAy = AcademicYearBusiness.Find(AcademicYearID);
                // Danh sach Semester=3
                if (lstPupilRankingBO == null || lstPupilRankingBO.Count == 0)
                {
                    return;
                }
                int Year = objAy.Year;
                List<int> lstPupilID = lstPupilRankingBO.Select(o => o.PupilID.Value).Distinct().ToList();
                List<int> lstClassID = lstPupilRankingBO.Select(o => o.ClassID.Value).Distinct().ToList();
                //List<PupilProfile> lstPupilProfile = PupilProfileBusiness.All
                //                                    .Where(o => lstPupilID.Contains(o.PupilProfileID) && o.IsActive == true).ToList();
                // Thông tin tổng kết sau thi lại
                List<PupilRankingHistory> lstPupilRanking = PupilRankingHistoryBusiness.SearchBySchool(SchoolID, new Dictionary<string, object> { 
                                                            {"AcademicYearID", AcademicYearID }, 
                                                            {"Semester", SystemParamsInFile.SEMESTER_OF_YEAR_BACKTRAINING}
                                                            })
                                                        .Where(o => lstPupilID.Contains(o.PupilID) && o.PeriodID == null).ToList();

                List<int> lstPupilBacktrain = (from p in VPupilRankingBusiness.All
                                               .Where(u => u.Last2digitNumberSchool == SchoolID % 100
                                                        && u.CreatedAcademicYear == objAy.Year
                                                        && u.SchoolID == SchoolID
                                                        && u.AcademicYearID == AcademicYearID
                                                        && lstClassID.Contains(u.ClassID) && u.Semester == SystemParamsInFile.SEMESTER_OF_YEAR_ALL
                                                        && (u.StudyingJudgementID == SystemParamsInFile.STUDYING_JUDGEMENT_BACKTRAINING))
                                               select p.PupilID).ToList();

                #region Nghiệp vụ GDTX
                SchoolProfile sp = SchoolProfileBusiness.Find(SchoolID);
                bool isGDTX = sp.TrainingType != null && sp.TrainingType.Resolution == strGDTX;
                bool isAbsentContest = false;
                List<int> lstPupilRetestID = lstPupilRetest.Select(p => p.PupilID).Distinct().ToList();

                Dictionary<string, object> dicAbsence = new Dictionary<string, object>();
                dicAbsence["AcademicYearID"] = AcademicYearID;
                dicAbsence["lstClassID"] = lstClassID;
                dicAbsence["lstPupilID"] = lstPupilID;
                dicAbsence["SchoolID"] = SchoolID;
                List<int> lstSectionKeyID = new List<int>();
                List<PupilAbsence> lstAbsenceAll = PupilAbsenceBusiness.SearchBySchool(SchoolID, dicAbsence)
                                                                        .Where(o => ((o.AbsentDate >= objAy.FirstSemesterStartDate && o.AbsentDate <= objAy.FirstSemesterEndDate)
                                                                        || (o.AbsentDate >= objAy.SecondSemesterStartDate && o.AbsentDate <= objAy.SecondSemesterEndDate))).ToList();
                List<PupilAbsence> lstAbsencePupil = null;
                int TotalAbsentDaysWithPermission = 0;
                int TotalAbsentDaysWithoutPermission = 0;
                if (isGDTX) //Nghiep vu GDTX
                {
                    foreach (var PupilRankingBO in lstPupilRankingBO)
                    {
                        var pr = lstPupilRanking.FirstOrDefault(o => o.PupilID == PupilRankingBO.PupilID);
                        isAbsentContest = lstPupilRetest.Where(p => p.PupilID == PupilRankingBO.PupilID && p.IsAbsentContest.HasValue && p.IsAbsentContest.Value).Count() > 0;
                        //Kiểm tra học sinh có thuộc diện xếp loại hạnh kiểm không
                        //•	Các trường thuộc hệ GDTX  có các đối tượng sau không thuộc diện xếp loại hạnh kiểm: 
                        //-	Học viên có hình thức học là vừa làm vừa học
                        //-	Người lao động từ 20 tuổi trở lên đối với cấp THCS và 25 tuổi trở lên đối với cấp THPT
                        //-	Học viên học theo hình thức tự học có hướng dẫn - Bo qua vi chua luu vao db
                        if (pr != null)
                        {
                            lstSectionKeyID = PupilRankingBO.SectionKeyID.HasValue ? UtilsBusiness.GetListSectionID(PupilRankingBO.SectionKeyID.Value) : new List<int>();
                            lstAbsencePupil = lstAbsenceAll.Where(o => o.PupilID == pr.PupilID && lstSectionKeyID.Contains(o.Section)).ToList();
                            if ((isAbsentContest || !lstPupilRetestID.Contains(PupilRankingBO.PupilID.Value))
                                && !lstPupilBacktrain.Contains(PupilRankingBO.PupilID.Value))//vang thi hoac khong dang ky du thi update thuoc dien o lai lop
                            {
                                pr.StudyingJudgementID = GlobalConstants.STUDYING_JUDGEMENT_STAYCLASS;
                            }
                            else
                            {
                                TotalAbsentDaysWithoutPermission = lstAbsencePupil.Where(o => o.IsAccepted != true).Count();
                                TotalAbsentDaysWithPermission = lstAbsencePupil.Where(o => o.IsAccepted == true).Count();
                                bool isKXLHK = PupilProfileBusiness.IsNotConductRankingPupil(isGDTX, AppliedLevel, PupilRankingBO.LearningType, PupilRankingBO.BirthDate);
                                if (pr.CapacityLevelID.HasValue && (pr.ConductLevelID.HasValue || isKXLHK))
                                {
                                    if (isKXLHK)  //Nếu không thuộc diện xếp loại hạnh kiểm
                                    {
                                        if (pr.CapacityLevelID < GlobalConstants.CAPACITY_TYPE_WEAK  //Nếu học lực trên TB
                                        && (TotalAbsentDaysWithPermission + TotalAbsentDaysWithoutPermission) <= 45)  //Nghỉ không quá 45 buổi
                                        {
                                            pr.StudyingJudgementID = GlobalConstants.STUDYING_JUDGEMENT_UPCLASS;
                                        }
                                        else
                                        {
                                            pr.StudyingJudgementID = GlobalConstants.STUDYING_JUDGEMENT_STAYCLASS;
                                        }
                                    }
                                    else   //Có xếp loại hạnh kiểm
                                    {
                                        if (pr.CapacityLevelID < GlobalConstants.CAPACITY_TYPE_WEAK  //Nếu học lực trên TB
                                        && (TotalAbsentDaysWithPermission + TotalAbsentDaysWithoutPermission) <= 45 //Nghỉ không quá 45 buổi
                                            && pr.ConductLevelID.HasValue && lstHKPass.Contains(pr.ConductLevelID.Value))
                                        {
                                            pr.StudyingJudgementID = GlobalConstants.STUDYING_JUDGEMENT_UPCLASS;
                                        }
                                        else
                                        {
                                            pr.StudyingJudgementID = GlobalConstants.STUDYING_JUDGEMENT_STAYCLASS;
                                        }
                                    }
                                }
                            }
                            this.Update(pr);
                        }
                        else //Nếu không tìm thấy thì  thêm pr.Semester = 4 và thực hiện Insert vào bảng PupilRanking
                        {
                            PupilRankingHistory _pr = new PupilRankingHistory();
                            _pr.PupilID = PupilRankingBO.PupilID.Value;
                            _pr.SchoolID = PupilRankingBO.SchoolID.Value;
                            _pr.AcademicYearID = PupilRankingBO.AcademicYearID.Value;
                            _pr.TotalAbsentDaysWithoutPermission = PupilRankingBO.TotalAbsentDaysWithoutPermission;
                            _pr.TotalAbsentDaysWithPermission = PupilRankingBO.TotalAbsentDaysWithPermission;
                            _pr.ConductLevelID = PupilRankingBO.ConductLevelID;
                            _pr.CapacityLevelID = PupilRankingBO.CapacityLevelID;
                            _pr.PeriodID = PupilRankingBO.PeriodID;
                            _pr.Semester = PupilRankingBO.Semester;
                            _pr.CreatedAcademicYear = Year;
                            _pr.Last2digitNumberSchool = PupilRankingBO.SchoolID.Value % 100;
                            if (isAbsentContest || !lstPupilRetestID.Contains(PupilRankingBO.PupilID.Value))//vang thi hoac khong dang ky du thi update thuoc dien o lai lop
                            {
                                _pr.StudyingJudgementID = GlobalConstants.STUDYING_JUDGEMENT_STAYCLASS;
                            }
                            else
                            {
                                _pr.StudyingJudgementID = PupilRankingBO.StudyingJudgementID;
                            }
                            _pr.ClassID = PupilRankingBO.ClassID.Value;
                            _pr.Semester = GlobalConstants.SEMESTER_OF_YEAR_BACKTRAINING;
                            _pr.EducationLevelID = PupilRankingBO.EducationLevelID.Value;
                            this.Insert(_pr);
                        }
                    }
                }
                #endregion
                //Lấy từng phần tử trong lstPupilToRank.
                else
                {
                    #region Trường bình thường
                    // danh sach pupil_ranking voi Semester=3
                    foreach (var PupilRankingBO in lstPupilRankingBO)
                    {
                        var pr = lstPupilRanking.FirstOrDefault(o => o.PupilID == PupilRankingBO.PupilID);
                        isAbsentContest = lstPupilRetest.Where(p => p.PupilID == PupilRankingBO.PupilID && p.IsAbsentContest.HasValue && p.IsAbsentContest.Value).Count() > 0;
                        //Step1: Thực hiện update lstPupilToRank[i].pr vào bảng PupilRanking nếu tìm thấy bản ghi.
                        if (pr != null)
                        {
                            lstSectionKeyID = PupilRankingBO.SectionKeyID.HasValue ? UtilsBusiness.GetListSectionID(PupilRankingBO.SectionKeyID.Value) : new List<int>();
                            lstAbsencePupil = lstAbsenceAll.Where(o => o.PupilID == pr.PupilID && lstSectionKeyID.Contains(o.Section)).ToList();
                            if ((isAbsentContest || !lstPupilRetestID.Contains(PupilRankingBO.PupilID.Value))
                                && !lstPupilBacktrain.Contains(PupilRankingBO.PupilID.Value))//vang thi hoac khong dang ky du thi update thuoc dien o lai lop
                            {
                                pr.StudyingJudgementID = GlobalConstants.STUDYING_JUDGEMENT_STAYCLASS;
                            }
                            else
                            {
                                if (pr.CapacityLevelID.HasValue && pr.ConductLevelID.HasValue)
                                {
                                    TotalAbsentDaysWithoutPermission = lstAbsencePupil.Where(o => o.IsAccepted != true).Count();
                                    TotalAbsentDaysWithPermission = lstAbsencePupil.Where(o => o.IsAccepted == true).Count();
                                    // Nếu học lực trên TB, hạnh kiểm trên TB và nghỉ không quá 45 buổi thì thuộc diện lên lớp
                                    if (pr.ConductLevelID.HasValue && lstHKPass.Contains(pr.ConductLevelID.Value)
                                    && (TotalAbsentDaysWithPermission + TotalAbsentDaysWithoutPermission <= GlobalConstants.TOTALABSENTDAY)
                                        && pr.CapacityLevelID <= GlobalConstants.CAPACITY_TYPE_WEAK)
                                    {
                                        pr.StudyingJudgementID = GlobalConstants.STUDYING_JUDGEMENT_UPCLASS;
                                    }
                                    // Nếu học lực trên TB và hạnh kiểm yếu thì rèn luyện lại
                                    else if ((pr.ConductLevelID == GlobalConstants.CONDUCT_TYPE_WEAK_SECONDARY
                                        || pr.ConductLevelID == GlobalConstants.CONDUCT_TYPE_WEAK_TERTIARY)
                                        && pr.CapacityLevelID <= GlobalConstants.CAPACITY_TYPE_WEAK && pr.Semester == 4)
                                    {
                                        pr.StudyingJudgementID = GlobalConstants.STUDYING_JUDGEMENT_STAYCLASS;
                                    }
                                    else
                                        //Nếu nghỉ học quá 45 buổi thì ở lại lớp
                                        if (TotalAbsentDaysWithPermission + TotalAbsentDaysWithoutPermission > GlobalConstants.TOTALABSENTDAY)
                                        {
                                            pr.StudyingJudgementID = GlobalConstants.STUDYING_JUDGEMENT_STAYCLASS;
                                        }
                                    if ((pr.ConductLevelID == GlobalConstants.CONDUCT_TYPE_GOOD_SECONDARY ||
                                        pr.ConductLevelID == GlobalConstants.CONDUCT_TYPE_FAIR_SECONDARY ||
                                        pr.ConductLevelID == GlobalConstants.CONDUCT_TYPE_NORMAL_SECONDARY ||
                                        pr.ConductLevelID == GlobalConstants.CONDUCT_TYPE_GOOD_TERTIARY ||
                                        pr.ConductLevelID == GlobalConstants.CONDUCT_TYPE_FAIR_TERTIARY ||
                                        pr.ConductLevelID == GlobalConstants.CONDUCT_TYPE_NORMAL_TERTIARY)
                                    && pr.CapacityLevelID >= GlobalConstants.CAPACITY_TYPE_WEAK) // HK Tot-Kha-TB va HL >= Yeu
                                    {
                                        pr.StudyingJudgementID = GlobalConstants.STUDYING_JUDGEMENT_STAYCLASS;
                                    }
                                    else
                                        if ((pr.ConductLevelID == GlobalConstants.CONDUCT_TYPE_WEAK_SECONDARY
                                            || pr.ConductLevelID == GlobalConstants.CONDUCT_TYPE_WEAK_TERTIARY)
                                            && pr.CapacityLevelID >= GlobalConstants.CAPACITY_TYPE_WEAK)
                                        {
                                            pr.StudyingJudgementID = GlobalConstants.STUDYING_JUDGEMENT_STAYCLASS;
                                        }
                                }
                            }
                            this.Update(pr);
                        }
                        else
                        {
                            //Nếu không tìm thấy thì  thêm pr.Semester = 4 và thực hiện Insert vào bảng PupilRanking
                            PupilRankingHistory _pr = new PupilRankingHistory();
                            _pr.PupilID = PupilRankingBO.PupilID.Value;
                            _pr.SchoolID = PupilRankingBO.SchoolID.Value;
                            _pr.AcademicYearID = PupilRankingBO.AcademicYearID.Value;
                            _pr.TotalAbsentDaysWithoutPermission = PupilRankingBO.TotalAbsentDaysWithoutPermission;
                            _pr.TotalAbsentDaysWithPermission = PupilRankingBO.TotalAbsentDaysWithPermission;
                            _pr.ConductLevelID = PupilRankingBO.ConductLevelID;
                            _pr.CapacityLevelID = PupilRankingBO.CapacityLevelID;
                            _pr.PeriodID = PupilRankingBO.PeriodID;
                            _pr.Semester = GlobalConstants.SEMESTER_OF_YEAR_BACKTRAINING;
                            if (isAbsentContest || !lstPupilRetestID.Contains(PupilRankingBO.PupilID.Value))//vang thi hoac khong dang ky du thi update thuoc dien o lai lop
                            {
                                _pr.StudyingJudgementID = GlobalConstants.STUDYING_JUDGEMENT_STAYCLASS;
                            }
                            else
                            {
                                _pr.StudyingJudgementID = PupilRankingBO.StudyingJudgementID;
                            }
                            _pr.ClassID = PupilRankingBO.ClassID.Value;
                            _pr.Semester = GlobalConstants.SEMESTER_OF_YEAR_BACKTRAINING;
                            _pr.EducationLevelID = PupilRankingBO.EducationLevelID.Value;
                            _pr.Last2digitNumberSchool = PupilRankingBO.SchoolID.Value % 100;
                            _pr.CreatedAcademicYear = Year;
                            this.Insert(_pr);
                        }
                    }
                    #endregion
                }

                this.Save();
            }
            finally
            {
                SetAutoDetectChangesEnabled(true);
            }
        }

        /// <summary>
        /// Tổng kết sau khi thi lại
        /// </summary>
        /// <param name="UserID"></param>
        /// <param name="lstPupilRetestToCategoryBO"></param>
        public void RankingPupilRetestHistory(int UserID, List<PupilRetestHistoryToCategory> lstPupilRetestToCategoryBO, List<RegisterSubjectSpecializeBO> lstRegisterBO = null)
        {
            try
            {
                SetAutoDetectChangesEnabled(false);

                //UtilsBusiness.HasHeadTeacherPermission(UserID, ClassID) = false
                int schoolID = lstPupilRetestToCategoryBO[0].lstSummedUpRecordHistory[0].SchoolID;
                int modSchoolID = schoolID % 100;
                SchoolProfile schoolProfile = SchoolProfileBusiness.Find(schoolID);
                AcademicYear objAcademicYear = AcademicYearBusiness.Find(lstPupilRetestToCategoryBO[0].lstSummedUpRecordHistory[0].AcademicYearID);
                TrainingType traningType = schoolProfile.TrainingType;
                if (lstPupilRetestToCategoryBO != null && lstPupilRetestToCategoryBO.Count > 0)
                {
                    List<int> ListClass = new List<int>();
                    foreach (var PupilRetestToCategoryBO in lstPupilRetestToCategoryBO)
                    {
                        foreach (var item in PupilRetestToCategoryBO.lstSummedUpRecordHistory)
                        {
                            if (!ListClass.Any(o => o == item.ClassID))
                                ListClass.Add(item.ClassID);
                        }
                    }
                    foreach (var ClassID in ListClass)
                    {
                        if (UtilsBusiness.HasHeadTeacherPermission(UserID, ClassID) == false)
                        {
                            throw new BusinessException("PuPilRanking_Not_Authen");
                        }
                    }
                    // Lay Dictionary luu danh sach mon hoc cua tung lop
                    Dictionary<int, List<ClassSubject>> dicClassSubject = new Dictionary<int, List<ClassSubject>>();
                    foreach (var PupilRetestToCategoryBO in lstPupilRetestToCategoryBO)
                    {
                        int classID = PupilRetestToCategoryBO.ClassID;
                        if (!dicClassSubject.ContainsKey(classID))
                        {
                            dicClassSubject[classID] = ClassSubjectBusiness.SearchByClass(classID).ToList();
                        }
                    }

                    foreach (var PupilRetestToCategoryBO in lstPupilRetestToCategoryBO)
                    {
                        List<string> lstmark = PupilRetestToCategoryBO.lstMark;
                        List<SummedUpRecordHistory> listsummeduprecord = PupilRetestToCategoryBO.lstSummedUpRecordHistory;
                        if (lstmark.Count == listsummeduprecord.Count)
                        {
                            List<SummedUpRecordMarkHistoryBO> lstPupilRankingOfPupil = new List<SummedUpRecordMarkHistoryBO>();
                            for (var i = 0; i < lstmark.Count; i++)
                            {
                                SummedUpRecordMarkHistoryBO SummedUpRecordMarkBO = new SummedUpRecordMarkHistoryBO();
                                SummedUpRecordMarkBO.SummedUpRecordHistory = listsummeduprecord[i];
                                SummedUpRecordMarkBO.Mark = lstmark[i];
                                //lstPupilRankingOfPupil.Add(SummedUpRecordMarkBO);
                                //Chi tinh TBM cac mon tinh diem va co he so >0

                                //Fix lỗi ATTT
                                //if (dicClassSubject[listsummeduprecord[i].ClassID].Exists(c => c.FirstSemesterCoefficient > 0
                                //                            && c.FirstSemesterCoefficient > 0
                                //                            && c.SubjectID == listsummeduprecord[i].SubjectID
                                //                            && c.IsCommenting == GlobalConstants.SUBJECTCAT_ISCOMMENTING_MARK))
                                if (dicClassSubject[listsummeduprecord[i].ClassID].Exists(c => c.FirstSemesterCoefficient > 0
                                                                                            && c.SecondSemesterCoefficient > 0
                                                                                            && c.SubjectID == listsummeduprecord[i].SubjectID
                                                                                            && c.IsCommenting == GlobalConstants.SUBJECTCAT_ISCOMMENTING_MARK))
                                {
                                    lstPupilRankingOfPupil.Add(SummedUpRecordMarkBO);
                                }

                            }
                            var lstPupilRankingOfPupil_Voluntary = new List<SummedUpRecordMarkHistoryBO>();
                            var lstPupilRankingOfPupil_Mandatory = new List<SummedUpRecordMarkHistoryBO>();
                            if (lstPupilRankingOfPupil != null && lstPupilRankingOfPupil.Count > 0)
                            {
                                foreach (var PupilRankingOfPupil in lstPupilRankingOfPupil)
                                {
                                    var objSummedUpRecordHistory = PupilRankingOfPupil.SummedUpRecordHistory;
                                    var isAppliedTypeSubject = PupilRankingBusiness.IsAppliedTypeSubject(dicClassSubject[objSummedUpRecordHistory.ClassID], objSummedUpRecordHistory.ClassID, objSummedUpRecordHistory.SubjectID);
                                    if (isAppliedTypeSubject)
                                    {
                                        lstPupilRankingOfPupil_Voluntary.Add(PupilRankingOfPupil); // môn tự chọn
                                    }
                                    else
                                    {
                                        lstPupilRankingOfPupil_Mandatory.Add(PupilRankingOfPupil); // Môn bắt buộc
                                    }
                                }
                            }

                            //If (lstPupilRankingOfPupil_Mandatory.Count >0)
                            //thì tính điểm trung bình các môn AverageMark = CaculatorAverageSubject(lstPupilRankingOfPupil_Mandatory)
                            if (lstPupilRankingOfPupil_Mandatory != null && lstPupilRankingOfPupil_Mandatory.Count > 0)
                            {
                                decimal? AverageMark = 0;
                                List<SummedUpRecordHistory> ListSummedUpRecord = lstPupilRankingOfPupil_Mandatory.Select(o => o.SummedUpRecordHistory).ToList();
                                AverageMark = CaculatorAverageSubject(ListSummedUpRecord, dicClassSubject[PupilRetestToCategoryBO.ClassID], SystemParamsInFile.SEMESTER_OF_YEAR_ALL, lstRegisterBO);
                                //For(lstPupilRankingOfPupil_Voluntary)
                                foreach (var item in lstPupilRankingOfPupil_Voluntary)
                                {
                                    if (item.SummedUpRecordHistory.ReTestMark.HasValue)
                                    {
                                        item.SummedUpRecordHistory.SummedUpMark = item.SummedUpRecordHistory.ReTestMark;
                                    }
                                    //Nếu lstPupilRankingOfPupil_Voluntary[i].SummedUpMark >= 8.0 thì AverageMark = AverageMark + 0.3
                                    if (item.SummedUpRecordHistory.SummedUpMark >= GlobalConstants.EXCELLENT_MARK)
                                    {
                                        AverageMark = AverageMark + GlobalConstants.ADD_EXCELLENT_MARK;
                                    }
                                    //Nếu lstPupilRankingOfPupil_Voluntary[i].SummedUpMark >= 6.5 và < 8.0 thì AverageMark = AverageMark + 0.2
                                    else if (item.SummedUpRecordHistory.SummedUpMark >= GlobalConstants.GOOD_MARK && item.SummedUpRecordHistory.SummedUpMark < GlobalConstants.EXCELLENT_MARK)
                                    {
                                        AverageMark = AverageMark + GlobalConstants.ADD_GOOD_MARK;
                                    }
                                    //Nếu lstPupilRankingOfPupil_Voluntary[i].SummedUpMark >= 5.0 và < 6.5 thì AverageMark = AverageMark + 0.1
                                    else if (item.SummedUpRecordHistory.SummedUpMark >= GlobalConstants.NORMAL_MARK && item.SummedUpRecordHistory.SummedUpMark < GlobalConstants.GOOD_MARK)
                                    {
                                        AverageMark = AverageMark + GlobalConstants.ADD_NORMAL_MARK;
                                    }
                                }
                                if (AverageMark > GlobalConstants.MAX_MARK)
                                {
                                    AverageMark = GlobalConstants.MAX_MARK;
                                }
                                // Set lai diem trung binh mon de lay xep loai hoc luc
                                foreach (SummedUpRecordHistory item in ListSummedUpRecord)
                                {
                                    if (item.ReTestMark.HasValue)
                                    {
                                        item.SummedUpMark = item.ReTestMark;
                                    }
                                    if (item.ReTestJudgement != null && string.IsNullOrEmpty(item.ReTestJudgement.Trim()))
                                    {
                                        item.JudgementResult = item.ReTestJudgement;
                                    }
                                }

                                int CapacityLevelID = GetCapacityTypeByMark(AverageMark, ListSummedUpRecord, dicClassSubject[PupilRetestToCategoryBO.ClassID], schoolID, traningType, lstRegisterBO);

                                Dictionary<string, object> dic = new Dictionary<string, object>();
                                dic["PupilID"] = PupilRetestToCategoryBO.PupilID;
                                dic["AcademicYearID"] = objAcademicYear.AcademicYearID;
                                dic["ClassID"] = PupilRetestToCategoryBO.ClassID;
                                dic["Semester"] = GlobalConstants.SEMESTER_OF_YEAR_BACKTRAINING;
                                //IQueryable<PupilRankingHistory> IQueryableRanking = this.SearchBySchool(schoolID, dic);
                                if (PupilRetestToCategoryBO.pr.PupilRankingID > 0)
                                {
                                    PupilRankingHistory PupilRanking = PupilRetestToCategoryBO.pr; //IQueryableRanking.FirstOrDefault();
                                    PupilRanking.AverageMark = AverageMark;
                                    PupilRanking.CapacityLevelID = CapacityLevelID;
                                    PupilRankingHistoryBusiness.Update(PupilRanking);
                                }
                                else
                                {
                                    PupilRankingHistory pr = new PupilRankingHistory();
                                    pr.PupilID = PupilRetestToCategoryBO.PupilID;
                                    pr.ClassID = PupilRetestToCategoryBO.ClassID;
                                    pr.SchoolID = objAcademicYear.SchoolID;
                                    pr.AcademicYearID = objAcademicYear.AcademicYearID;
                                    pr.EducationLevelID = PupilRetestToCategoryBO.pr.EducationLevelID;
                                    pr.TotalAbsentDaysWithoutPermission = PupilRetestToCategoryBO.pr.TotalAbsentDaysWithoutPermission;
                                    pr.TotalAbsentDaysWithPermission = PupilRetestToCategoryBO.pr.TotalAbsentDaysWithPermission;
                                    pr.ConductLevelID = PupilRetestToCategoryBO.pr.ConductLevelID;
                                    pr.CreatedAcademicYear = objAcademicYear.Year;
                                    pr.Semester = GlobalConstants.SEMESTER_OF_YEAR_BACKTRAINING;
                                    pr.AverageMark = AverageMark;
                                    pr.CapacityLevelID = CapacityLevelID;
                                    pr.RankingDate = DateTime.Now;
                                    pr.Last2digitNumberSchool = modSchoolID;
                                    PupilRankingHistoryBusiness.Insert(pr);
                                }

                            }
                        }
                    }
                    PupilRankingHistoryBusiness.Save();
                }
            }

            finally
            {
                SetAutoDetectChangesEnabled(true);
            }
        }



        /// <summary>
        /// Xep hang danh sach
        /// </summary>
        /// <param name="AcademicYearID"></param>
        /// <param name="listPupilRanking"></param>
        public void Rank(int RankingCriteria, List<PupilRankingHistory> lstPupilRanking, bool isValidate = true)
        {
            if (lstPupilRanking == null || lstPupilRanking.Count == 0)
            {
                return;
            }

            // Xep thu tu trong lop cho hoc sinh
            // Neu hoc sinh co 1 tieu chi trong so cac tieu chi de xep hang chua co thi se khong xep hang cho hoc sinh nay
            if (RankingCriteria == SystemParamsInFile.RANKING_CRITERIA_AVERAGE_MARK)
            {
                lstPupilRanking = lstPupilRanking.OrderByDescending(o => o.AverageMark ?? -1).ToList();
                // Phan tu dau tien
                if (lstPupilRanking[0].AverageMark.HasValue)
                {
                    lstPupilRanking[0].Rank = 1;
                }
                else
                {
                    lstPupilRanking[0].Rank = null;
                }
                for (var i = 1; i < lstPupilRanking.Count; i++)
                {
                    // Neu khong diem TBM thi khong xep hang
                    if (!lstPupilRanking[i].AverageMark.HasValue)
                    {
                        lstPupilRanking[i].Rank = null;
                    }
                    else
                    {
                        if (lstPupilRanking[i - 1].AverageMark == lstPupilRanking[i].AverageMark)
                            lstPupilRanking[i].Rank = lstPupilRanking[i - 1].Rank;
                        else
                            lstPupilRanking[i].Rank = i + 1;
                    }
                }
            }
            else if (RankingCriteria == SystemParamsInFile.RANKING_CRITERIA_AVERAGE_CAPACITY)
            {
                lstPupilRanking = lstPupilRanking.OrderBy(o => o.CapacityLevelID ?? int.MaxValue)
                    .ThenByDescending(o => o.AverageMark ?? -1).ToList();
                // Phan tu dau tien
                if (lstPupilRanking[0].AverageMark.HasValue)
                {
                    lstPupilRanking[0].Rank = 1;
                }
                else
                {
                    lstPupilRanking[0].Rank = null;
                }
                for (var i = 1; i < lstPupilRanking.Count; i++)
                {
                    // Co diem TBM thi se co hoc luc nen chi can kiem tra diem TBM la du
                    if (!lstPupilRanking[i].AverageMark.HasValue)
                    {
                        lstPupilRanking[i].Rank = null;
                    }
                    else
                    {
                        if (lstPupilRanking[i - 1].CapacityLevelID == lstPupilRanking[i].CapacityLevelID
                            && lstPupilRanking[i - 1].AverageMark == lstPupilRanking[i].AverageMark)
                            lstPupilRanking[i].Rank = lstPupilRanking[i - 1].Rank;
                        else
                            lstPupilRanking[i].Rank = i + 1;
                    }
                }
            }
            else if (RankingCriteria == SystemParamsInFile.RANKING_CRITERIA_AVERAGE_CONDUCT)
            {
                lstPupilRanking = lstPupilRanking.OrderBy(o => (o.ConductLevelID.HasValue && o.AverageMark.HasValue) ? o.ConductLevelID.Value : int.MaxValue)
                    .ThenByDescending(o => o.AverageMark ?? -1).ToList();
                // Phan tu dau tien
                if (lstPupilRanking[0].AverageMark.HasValue && lstPupilRanking[0].ConductLevelID.HasValue)
                {
                    lstPupilRanking[0].Rank = 1;
                }
                else
                {
                    lstPupilRanking[0].Rank = null;
                }
                for (var i = 1; i < lstPupilRanking.Count; i++)
                {
                    if (!lstPupilRanking[i].AverageMark.HasValue || !lstPupilRanking[i].ConductLevelID.HasValue)
                    {
                        lstPupilRanking[i].Rank = null;
                    }
                    else
                    {
                        if (lstPupilRanking[i - 1].ConductLevelID == lstPupilRanking[i].ConductLevelID
                            && lstPupilRanking[i - 1].AverageMark == lstPupilRanking[i].AverageMark)
                            lstPupilRanking[i].Rank = lstPupilRanking[i - 1].Rank;
                        else
                            lstPupilRanking[i].Rank = i + 1;
                    }
                }

            }
            else if (RankingCriteria == SystemParamsInFile.RANKING_CRITERIA_AVERAGE_CAPACITY_CONDUCT)
            {
                lstPupilRanking = lstPupilRanking
                    .OrderBy(o => (o.ConductLevelID.HasValue && o.AverageMark.HasValue) ? o.CapacityLevelID.Value : int.MaxValue)
                    .ThenBy(o => (o.ConductLevelID.HasValue && o.AverageMark.HasValue) ? o.ConductLevelID.Value : int.MaxValue)
                    .ThenByDescending(o => o.AverageMark ?? -1).ToList();
                // Phan tu dau tien
                if (lstPupilRanking[0].AverageMark.HasValue && lstPupilRanking[0].ConductLevelID.HasValue)
                {
                    lstPupilRanking[0].Rank = 1;
                }
                else
                {
                    lstPupilRanking[0].Rank = null;
                }
                for (var i = 1; i < lstPupilRanking.Count; i++)
                {
                    // TBM tuong duong voi hoc luc nen chi can kiem tra 1 cai la du
                    if (!lstPupilRanking[i].AverageMark.HasValue || !lstPupilRanking[i].ConductLevelID.HasValue)
                    {
                        lstPupilRanking[i].Rank = null;
                    }
                    else
                    {
                        if (lstPupilRanking[i - 1].CapacityLevelID == lstPupilRanking[i].CapacityLevelID
                            && lstPupilRanking[i - 1].ConductLevelID == lstPupilRanking[i].ConductLevelID
                            && lstPupilRanking[i - 1].AverageMark == lstPupilRanking[i].AverageMark)
                            lstPupilRanking[i].Rank = lstPupilRanking[i - 1].Rank;
                        else
                            lstPupilRanking[i].Rank = i + 1;
                    }
                }

            }
        }


        /// <summary>
        /// Tinh diem trung binh cho hoc sinh
        /// </summary>
        /// <param name="ListSummedUpRecord"></param>
        /// <returns></returns>
        public decimal? CaculatorAverageSubject(List<SummedUpRecordHistory> ListSummedUpRecord, List<ClassSubject> lstSubject, int semester, List<RegisterSubjectSpecializeBO> lstRegisterSubjectBO = null)
        {
            if (ListSummedUpRecord == null || ListSummedUpRecord.Count == 0)
            {
                return 0;
            }

            //Chiendd1: 17/08/2015, mon tang cuong khong tham gia tinh diem tong ket
            //Danh sach mon hoc tang cuong
            List<int> lstClassSubjectPlus = lstSubject.Where(c => c.SubjectIDIncrease.HasValue && c.SubjectIDIncrease > 0).Select(c => c.SubjectIDIncrease.Value).ToList();

            // Lay thong tin he so cua mon hoc theo mon hoc va hoc ky
            Dictionary<int, int> dicCof = new Dictionary<int, int>();
            ClassSubject objClassSubject = null;
            foreach (ClassSubject cs in lstSubject)
            {
                if (semester == SystemParamsInFile.SEMESTER_OF_YEAR_FIRST)
                {
                    dicCof[cs.SubjectID] = cs.FirstSemesterCoefficient.HasValue ? cs.FirstSemesterCoefficient.Value : 0;
                }
                else
                {
                    dicCof[cs.SubjectID] = cs.SecondSemesterCoefficient.HasValue ? cs.SecondSemesterCoefficient.Value : 0;
                }
            }
            // Tinh diem
            decimal sumMark = 0;
            int count = 0;
            int countPupil = 0;
            foreach (SummedUpRecordHistory SummedUpRecord in ListSummedUpRecord)
            {
                //Lay thong tin he so diem
                int cof = 0;
                objClassSubject = lstSubject.Where(p => p.ClassID == SummedUpRecord.ClassID && p.SubjectID == SummedUpRecord.SubjectID).FirstOrDefault();
                //Neu la mon tang cuong thi set he so =0 de khong tham gia tinh tong ket
                if (lstClassSubjectPlus != null && lstClassSubjectPlus.Contains(objClassSubject.SubjectID))
                {
                    cof = 0;
                }
                else if (objClassSubject.IsSpecializedSubject.HasValue && objClassSubject.IsSpecializedSubject.Value)
                {
                    countPupil = lstRegisterSubjectBO != null ? lstRegisterSubjectBO.Where(p => p.PupilID == SummedUpRecord.PupilID && p.SubjectID == SummedUpRecord.SubjectID).Count() : 0;
                    if (countPupil > 0 && dicCof.ContainsKey(SummedUpRecord.SubjectID))
                    {
                        cof = dicCof[SummedUpRecord.SubjectID];
                    }
                    else
                    {
                        cof = 1;
                    }
                }
                else if (objClassSubject.AppliedType == GlobalConstants.APPLIED_SUBJECT_ELECTIVE || objClassSubject.AppliedType == GlobalConstants.APPLIED_SUBJECT_ELECTIVE_PRIORITIE
                        || objClassSubject.AppliedType == GlobalConstants.APPLIED_SUBJECT_ELECTIVE_SCORE)
                {
                    countPupil = lstRegisterSubjectBO != null ? lstRegisterSubjectBO.Where(p => p.PupilID == SummedUpRecord.PupilID && p.SubjectID == SummedUpRecord.SubjectID && (semester > GlobalConstants.SEMESTER_OF_YEAR_SECOND ? (p.SemesterID == GlobalConstants.SEMESTER_OF_YEAR_FIRST || p.SemesterID == GlobalConstants.SEMESTER_OF_YEAR_SECOND) : p.SemesterID == semester)).Count() : 0;
                    if (countPupil > 0 && dicCof.ContainsKey(SummedUpRecord.SubjectID))
                    {
                        cof = dicCof[SummedUpRecord.SubjectID];
                    }
                    else
                    {
                        continue;
                    }
                }
                else if (dicCof.ContainsKey(SummedUpRecord.SubjectID))
                {
                    cof = dicCof[SummedUpRecord.SubjectID];
                }
                // Lay diem
                if (SummedUpRecord.ReTestMark.HasValue)
                {
                    sumMark += cof * SummedUpRecord.ReTestMark.Value;
                    count = count + cof;
                }
                else
                {
                    if (SummedUpRecord.SummedUpMark.HasValue)
                    {
                        sumMark += cof * SummedUpRecord.SummedUpMark.Value;
                        count = count + cof;
                    }
                }
            }
            if (count == 0)
            {
                return null;
            }
            return decimal.Round(sumMark / count, 1, MidpointRounding.AwayFromZero);
        }


        public List<PupilRankingHistory> ConvertListToHistory(List<PupilRanking> lstPupilRanking)
        {
            List<PupilRankingHistory> lstVal = new List<PupilRankingHistory>();
            for (int i = 0; i < lstPupilRanking.Count; i++)
            {
                lstVal.Add(ConvertHistory(lstPupilRanking[i]));
            }
            return lstVal;
        }

        public int RankClassHistory(int UserID, int SchoolID, int AcademicYearID, int ClassID, int Semester, int? Period, bool isShowRetetResult, string auto)
        {
            Dictionary<int, int> retValue = new Dictionary<int, int>();

            AcademicYear academicYear = AcademicYearBusiness.Find(AcademicYearID);
            ClassProfile classProfile = ClassProfileBusiness.Find(ClassID);

            //lay danh sach hoc sinh dang ky mon chuyen,mon tu chon
            IDictionary<string, object> dicRegister = new Dictionary<string, object>()
            {
                {"SchoolID",SchoolID},
                {"AcademicYearID",AcademicYearID},
                {"ClassID",ClassID},
                //{"SemesterID",Semester},
                {"YearID",academicYear.Year}
            };

            List<RegisterSubjectSpecializeBO> lstRegisterSubjectBO = RegisterSubjectSpecializeBusiness.GetlistSubjectByClass(dicRegister);

            Dictionary<string, object> dicSubject = new Dictionary<string, object>();
            dicSubject["ClassID"] = ClassID;
            dicSubject["AcademicYearID"] = AcademicYearID;
            dicSubject["IsApprenticeShipSubject"] = false;
            if (Semester != SystemParamsInFile.SEMESTER_OF_YEAR_ALL) dicSubject["Semester"] = Semester;
            List<ClassSubject> lstClassSubject = ClassSubjectBusiness.SearchBySchool(SchoolID, dicSubject).ToList();
            if (Semester == SystemParamsInFile.SEMESTER_OF_YEAR_ALL)
            {
                lstClassSubject = lstClassSubject.Where(o => o.SectionPerWeekFirstSemester > 0 || o.SectionPerWeekSecondSemester > 0).ToList();
            }
            //loc ra nhung mon tang cuong
            List<int?> lstInCreaseID = lstClassSubject.Where(p => p.SubjectIDIncrease.HasValue && p.SubjectIDIncrease.Value > 0).Select(p => p.SubjectIDIncrease).ToList();
            lstClassSubject = lstClassSubject.Where(p => !lstInCreaseID.Contains(p.SubjectID)).ToList();
            List<SummedUpRecordBO> lstSummedUp = SummedUpRecordBusiness.GetSummedUpRecord(AcademicYearID, SchoolID, Semester, ClassID, Period)
                                                                    .Where(u => u.Status == SystemParamsInFile.PUPIL_STATUS_STUDYING).ToList();

            //neu hoc ky la Ca nam thi lay them danh sach diem trung binh mon cua hoc ky 1
            List<SummedUpRecordBO> listSummedUpHKI = Semester == SystemParamsInFile.SEMESTER_OF_YEAR_ALL
                                                                ? SummedUpRecordBusiness.GetSummedUpRecord(AcademicYearID, SchoolID, SystemParamsInFile.SEMESTER_OF_YEAR_FIRST, ClassID, null).ToList()
                                                                : new List<SummedUpRecordBO>();

            // Lay danh sach hoc sinh mien giảm
            // ky 1
            List<ExemptedSubject> lstExemptedI = ExemptedSubjectBusiness.GetListExemptedSubject(ClassID, SystemParamsInFile.SEMESTER_OF_YEAR_FIRST).ToList();
            // ky 2
            List<ExemptedSubject> lstExemptedII = ExemptedSubjectBusiness.GetListExemptedSubject(ClassID, SystemParamsInFile.SEMESTER_OF_YEAR_SECOND).ToList();

            List<int> lstPupilID = lstSummedUp.Select(u => u.PupilID).Distinct().ToList();

            List<PupilRanking> lstPupilRanking = new List<PupilRanking>();
            List<SummedUpRecord> lstSummedUpRanking = new List<SummedUpRecord>();

            List<PupilRankingHistory> listExistedRanking = this.SearchBySchool(SchoolID, new Dictionary<string, object> { { "ClassID", ClassID }, { "Semester", Semester }, { "PeriodID", Period } }).ToList();
            List<PupilRankingHistory> listExistedBackTraining = new List<PupilRankingHistory>();
            if (Semester == SystemParamsInFile.SEMESTER_OF_YEAR_ALL && isShowRetetResult)
            {
                listExistedBackTraining = this.SearchBySchool(SchoolID, new Dictionary<string, object> { { "ClassID", ClassID }, { "Semester", SystemParamsInFile.SEMESTER_OF_YEAR_BACKTRAINING }, { "PeriodID", null } }).ToList();
            }

            List<PupilRankingClassBO> lstPupilForRanking = new List<PupilRankingClassBO>();
            List<RegisterSubjectSpecializeBO> lstRegistmp = null;
            foreach (int pupilID in lstPupilID)
            {
                IEnumerable<SummedUpRecordBO> lstSummedOfPupil = lstSummedUp.Where(u => u.PupilID == pupilID);
                PupilRankingHistory pocExistedRanking = listExistedRanking.FirstOrDefault(u => u.PupilID == pupilID);

                PupilRankingHistory pocRanking = null;
                if (isShowRetetResult)
                {
                    pocRanking = Semester == SystemParamsInFile.SEMESTER_OF_YEAR_ALL ? (listExistedBackTraining.FirstOrDefault(u => u.PupilID == pupilID) ?? pocExistedRanking) : pocExistedRanking;
                }
                else
                {
                    pocRanking = pocExistedRanking;
                }

                PupilRankingClassBO pupilForRanking = new PupilRankingClassBO();
                pupilForRanking.SchoolID = SchoolID;
                pupilForRanking.AcademicYearID = AcademicYearID;
                pupilForRanking.ClassID = ClassID;
                pupilForRanking.Semester = Semester;
                pupilForRanking.PeriodID = Period;
                pupilForRanking.PupilID = pupilID;
                pupilForRanking.CapacityLevelID = pocRanking != null ? pocRanking.CapacityLevelID : null;
                pupilForRanking.ConductLevelID = pocRanking != null ? pocRanking.ConductLevelID : null;
                pupilForRanking.ListSubject = new List<PupilRankingSubjectBO>();
                lstRegistmp = lstRegisterSubjectBO.Where(p => p.PupilID == pupilID).ToList();

                #region Nếu là HK 1 hoặc HK 2
                if (Semester == SystemParamsInFile.SEMESTER_OF_YEAR_FIRST || Semester == SystemParamsInFile.SEMESTER_OF_YEAR_SECOND)
                {
                    foreach (ClassSubject classSubject in lstClassSubject)
                    {
                        if ((classSubject.AppliedType != GlobalConstants.APPLIED_SUBJECT_ELECTIVE && classSubject.AppliedType != GlobalConstants.APPLIED_SUBJECT_ELECTIVE_PRIORITIE
                            && classSubject.AppliedType != GlobalConstants.APPLIED_SUBJECT_ELECTIVE_SCORE)
                            || (lstRegistmp.Count > 0 && lstRegistmp.Where(p => p.SubjectID == classSubject.SubjectID && p.SemesterID == Semester && p.SubjectTypeID == classSubject.AppliedType).Count() > 0)
                            )
                        {
                            SummedUpRecordBO surBo = lstSummedOfPupil.FirstOrDefault(u => u.SubjectID == classSubject.SubjectID);
                            bool isExempted = Semester == SystemParamsInFile.SEMESTER_OF_YEAR_FIRST && lstExemptedI.Any(u => u.SubjectID == classSubject.SubjectID && u.PupilID == pupilID)
                                            || Semester == SystemParamsInFile.SEMESTER_OF_YEAR_SECOND && lstExemptedII.Any(u => u.SubjectID == classSubject.SubjectID && u.PupilID == pupilID);
                            PupilRankingSubjectBO pupilRankingSubject = new PupilRankingSubjectBO();
                            pupilRankingSubject.IsCommenting = classSubject.IsCommenting.Value;
                            pupilRankingSubject.IsExempted = isExempted;
                            pupilRankingSubject.RankingJudgement = classSubject.IsCommenting == SystemParamsInFile.ISCOMMENTING_TYPE_JUDGE ? (surBo != null ? surBo.JudgementResult : null) : null;
                            pupilRankingSubject.RankingMark = classSubject.IsCommenting == SystemParamsInFile.ISCOMMENTING_TYPE_JUDGE ? null : (surBo != null ? surBo.SummedUpMark : null);
                            pupilRankingSubject.SubjectID = classSubject.SubjectID;
                            pupilRankingSubject.AppliedType = classSubject.AppliedType;

                            pupilForRanking.ListSubject.Add(pupilRankingSubject);
                        }
                    }
                }
                #endregion

                #region Nếu là cả năm
                if (Semester == SystemParamsInFile.SEMESTER_OF_YEAR_ALL)
                {
                    foreach (ClassSubject classSubject in lstClassSubject)
                    {
                        if ((classSubject.AppliedType != GlobalConstants.APPLIED_SUBJECT_ELECTIVE && classSubject.AppliedType != GlobalConstants.APPLIED_SUBJECT_ELECTIVE_PRIORITIE
                            && classSubject.AppliedType != GlobalConstants.APPLIED_SUBJECT_ELECTIVE_SCORE)
                            || (lstRegistmp.Count > 0 && lstRegistmp.Where(p => p.SubjectID == classSubject.SubjectID && p.SubjectTypeID == classSubject.AppliedType).Count() > 0))
                        {

                            SummedUpRecordBO summedUp = lstSummedOfPupil.FirstOrDefault(u => u.SubjectID == classSubject.SubjectID && u.Semester == SystemParamsInFile.SEMESTER_OF_YEAR_ALL);
                            SummedUpRecordBO summedRetest = null;
                            if (isShowRetetResult)
                            {
                                summedRetest = lstSummedOfPupil.FirstOrDefault(u => u.SubjectID == classSubject.SubjectID && u.Semester == SystemParamsInFile.SEMESTER_OF_YEAR_RETEST);
                            }

                            bool isExemptedI = lstExemptedI.Any(w => w.PupilID == pupilID && w.SubjectID == classSubject.SubjectID);
                            bool isExemptedII = lstExemptedII.Any(w => w.PupilID == pupilID && w.SubjectID == classSubject.SubjectID);

                            PupilRankingSubjectBO pupilRankingSubject = new PupilRankingSubjectBO();
                            pupilRankingSubject.IsCommenting = classSubject.IsCommenting.Value;
                            pupilRankingSubject.IsExempted = isExemptedI && isExemptedII;
                            pupilRankingSubject.SubjectID = classSubject.SubjectID;
                            pupilRankingSubject.AppliedType = classSubject.AppliedType;

                            //Nếu được miễn giảm cả năm
                            if (isExemptedI && isExemptedII)
                            {
                                pupilRankingSubject.RankingJudgement = null;
                                pupilRankingSubject.RankingMark = null;
                            }

                            //Nếu không miễn giảm học kỳ nào
                            if (!isExemptedI && !isExemptedII)
                            {
                                pupilRankingSubject.RankingMark = classSubject.IsCommenting == SystemParamsInFile.ISCOMMENTING_TYPE_JUDGE ? null : (summedRetest != null ? summedRetest.ReTestMark : (summedUp != null ? summedUp.SummedUpMark : null));
                                pupilRankingSubject.RankingJudgement = classSubject.IsCommenting == SystemParamsInFile.ISCOMMENTING_TYPE_JUDGE ? (summedRetest != null ? summedRetest.ReTestJudgement : (summedUp != null ? summedUp.JudgementResult : null)) : null;
                            }

                            //Nếu chỉ miễn giảm học kỳ 1 hoặc không học trong hk 1 
                            if (isExemptedI || !classSubject.SectionPerWeekFirstSemester.HasValue || classSubject.SectionPerWeekFirstSemester.Value == 0)
                            {
                                pupilRankingSubject.RankingMark = classSubject.IsCommenting == SystemParamsInFile.ISCOMMENTING_TYPE_JUDGE ? null : (summedRetest != null ? summedRetest.ReTestMark : (summedUp != null ? summedUp.SummedUpMark : null));
                                pupilRankingSubject.RankingJudgement = classSubject.IsCommenting == SystemParamsInFile.ISCOMMENTING_TYPE_JUDGE ? (summedRetest != null ? summedRetest.ReTestJudgement : (summedUp != null ? summedUp.JudgementResult : null)) : null;
                            }

                            //Nếu chỉ được miễn giảm học kỳ 2 hoặc không học trong hk2
                            if (isExemptedII || !classSubject.SectionPerWeekSecondSemester.HasValue || classSubject.SectionPerWeekSecondSemester.Value == 0)
                            {
                                SummedUpRecordBO summedHKI = listSummedUpHKI.FirstOrDefault(u => u.SubjectID == classSubject.SubjectID && u.PupilID == pupilID);
                                pupilRankingSubject.RankingMark = classSubject.IsCommenting == SystemParamsInFile.ISCOMMENTING_TYPE_JUDGE ? null : (summedHKI != null ? summedHKI.SummedUpMark : null);
                                pupilRankingSubject.RankingJudgement = classSubject.IsCommenting == SystemParamsInFile.ISCOMMENTING_TYPE_JUDGE ? (summedHKI != null ? summedHKI.JudgementResult : null) : null;
                            }

                            pupilForRanking.ListSubject.Add(pupilRankingSubject);
                        }
                    }
                }
                #endregion

                lstPupilForRanking.Add(pupilForRanking);
            }


            List<PupilRankingClassBO> lstPupilForRankingValid = lstPupilForRanking.Where(u => (u.PeriodID.HasValue && u.ListSubject.Any(v => !v.IsExempted && (!string.IsNullOrEmpty(v.RankingJudgement) || v.RankingMark.HasValue)))
                                                                                            || (!u.PeriodID.HasValue && !u.ListSubject.Any(v => !v.IsExempted && (string.IsNullOrEmpty(v.RankingJudgement) && !v.RankingMark.HasValue)))).ToList();

            foreach (PupilRankingClassBO pupilForRanking in lstPupilForRankingValid)
            {
                PupilRanking pr = new PupilRanking();
                pr.AcademicYearID = AcademicYearID;
                pr.PupilID = pupilForRanking.PupilID;
                pr.SchoolID = classProfile.SchoolID;
                pr.ClassID = ClassID;
                pr.EducationLevelID = classProfile.EducationLevelID;
                pr.CreatedAcademicYear = academicYear.Year;
                pr.Semester = Semester;
                pr.PeriodID = Period;
                pr.CapacityLevelID = pupilForRanking.CapacityLevelID;
                pr.ConductLevelID = pupilForRanking.ConductLevelID;
                pr.MSourcedb = auto;
                lstPupilRanking.Add(pr);

                foreach (PupilRankingSubjectBO pupilRankingSubject in pupilForRanking.ListSubject.Where(u => !u.IsExempted && (!string.IsNullOrEmpty(u.RankingJudgement) || u.RankingMark.HasValue)))
                {
                    SummedUpRecord sur = new SummedUpRecord();
                    sur.AcademicYearID = AcademicYearID;
                    sur.ClassID = ClassID;
                    sur.IsCommenting = pupilRankingSubject.IsCommenting;
                    sur.PeriodID = Semester != SystemParamsInFile.SEMESTER_OF_YEAR_ALL ? Period : null;
                    sur.PupilID = pupilForRanking.PupilID;
                    sur.SchoolID = SchoolID;
                    sur.Semester = Semester;
                    sur.SubjectID = pupilRankingSubject.SubjectID;
                    sur.SummedUpDate = DateTime.Now;
                    sur.CreatedAcademicYear = academicYear.Year;
                    sur.SummedUpMark = pupilRankingSubject.RankingMark;
                    sur.JudgementResult = pupilRankingSubject.RankingJudgement;
                    sur.MSourcedb = auto;

                    lstSummedUpRanking.Add(sur);
                }
            }

            if (lstPupilRanking.Count > 0 && lstSummedUpRanking.Count > 0)
                this.RankingPupilOfClassHistory(UserID, lstSummedUpRanking, lstPupilRanking, Period, lstClassSubject, false, GlobalConstants.NOT_AUTO_MARK, lstRegisterSubjectBO);

            return lstPupilRanking.Count;
        }

        #region PrivateMothed
        /// <summary>
        /// Tìm kiếm thông tin xếp loại học lực / hạnh kiểm và xếp thứ bậc học sinh trong lớp theo đợt (giai đoạn) / học kỳ / năm học
        /// </summary>
        /// <param name="dic"></param>
        /// <returns></returns>
        private IQueryable<PupilRankingHistory> Search(IDictionary<string, object> dic)
        {
            int SchoolID = Utils.GetInt(dic, "SchoolID");
            if (SchoolID <= 0)
            {
                return null;
            }
            IQueryable<PupilRankingHistory> lsPupilRanking = this.PupilRankingHistoryRepository.AllNoTracking;
            int StudyingJudgementID = Utils.GetInt(dic, "StudyingJudgementID");
            int PupilRankingID = Utils.GetInt(dic, "PupilRankingID");
            int PupilID = Utils.GetInt(dic, "PupilID");
            int ClassID = Utils.GetInt(dic, "ClassID");
            int AcademicYearID = Utils.GetInt(dic, "AcademicYearID");

            //int AppliedLevel = Utils.GetByte(dic, "AppliedLevel");
            int EducationLevelID = Utils.GetInt(dic, "EducationLevelID");
            int Year = Utils.GetInt(dic, "Year");
            int Semester = Utils.GetByte(dic, "Semester");
            int? PeriodID = Utils.GetNullInt(dic, "PeriodID");
            decimal? AverageMark = Utils.GetNullableDecimal(dic, "AverageMark");
            int? TotalAbsentDaysWithPermission = Utils.GetNullableByte(dic, "TotalAbsentDaysWithPermission");
            int? TotalAbsentDaysWithoutPermission = Utils.GetNullableByte(dic, "TotalAbsentDaysWithoutPermission");
            int CapacityLevelID = Utils.GetInt(dic, "CapacityLevelID");
            int ConductLevelID = Utils.GetInt(dic, "ConductLevelID");
            int? Rank = Utils.GetNullableInt(dic, "Rank");
            DateTime? RankingDay = Utils.GetDateTime(dic, "RankingDay");
            bool? IsCategory = Utils.GetNullableBool(dic, "IsCategory");
            string checkWithClassMovement = Utils.GetString(dic, "checkWithClassMovement");

            if (PupilRankingID != 0)
                lsPupilRanking = lsPupilRanking.Where(em => (em.PupilRankingID == PupilRankingID));
            if (PupilID != 0)
                lsPupilRanking = lsPupilRanking.Where(em => (em.PupilID == PupilID));
            if (ClassID != 0)
                lsPupilRanking = lsPupilRanking.Where(em => (em.ClassID == ClassID));
            if (AcademicYearID != 0)
            {
                lsPupilRanking = lsPupilRanking.Where(em => (em.AcademicYearID == AcademicYearID));
                if (SchoolID <= 0)
                {
                    var aca = this.AcademicYearBusiness.Find(AcademicYearID);
                    if (aca != null)
                    {
                        lsPupilRanking = lsPupilRanking.Where(o => (o.Last2digitNumberSchool == aca.SchoolID % 100));
                    }
                }
            }
            if (SchoolID != 0)
            {
                lsPupilRanking = lsPupilRanking.Where(em => (em.SchoolID == SchoolID));
                lsPupilRanking = lsPupilRanking.Where(em => em.Last2digitNumberSchool == (SchoolID % 100));
            }
            if (EducationLevelID != 0)
            {
                lsPupilRanking = lsPupilRanking.Where(em => em.EducationLevelID == EducationLevelID);
            }
            if (Year != 0)
            {
                lsPupilRanking = lsPupilRanking.Where(em => (em.CreatedAcademicYear == Year));
            }

            if (Semester != 0)
            {
                lsPupilRanking = lsPupilRanking.Where(em => (em.Semester == Semester));
            }
            if (checkWithClassMovement.Length == 0)
            {
                if (PeriodID.HasValue)
                {
                    if (PeriodID != 0)
                    {
                        lsPupilRanking = lsPupilRanking.Where(em => (em.PeriodID == PeriodID));
                    }
                    else
                    {
                        lsPupilRanking = lsPupilRanking.Where(em => !em.PeriodID.HasValue);
                    }
                }
                else
                {
                    lsPupilRanking = lsPupilRanking.Where(em => !em.PeriodID.HasValue);
                }
            }
            if (AverageMark != null)
            {
                lsPupilRanking = lsPupilRanking.Where(em => (em.AverageMark == AverageMark));
            }
            if (TotalAbsentDaysWithPermission != null)
            {
                lsPupilRanking = lsPupilRanking.Where(em => (em.TotalAbsentDaysWithPermission == TotalAbsentDaysWithPermission));
            }
            if (TotalAbsentDaysWithoutPermission != null)
            {
                lsPupilRanking = lsPupilRanking.Where(em => (em.TotalAbsentDaysWithoutPermission == TotalAbsentDaysWithoutPermission));
            }
            if (CapacityLevelID != 0)
            {
                lsPupilRanking = lsPupilRanking.Where(em => (em.CapacityLevelID == CapacityLevelID));
            }
            if (ConductLevelID != 0)
            {
                lsPupilRanking = lsPupilRanking.Where(em => (em.ConductLevelID == ConductLevelID));
            }
            if (Rank != null)
            {
                lsPupilRanking = lsPupilRanking.Where(em => (em.Rank == Rank));
            }
            if (RankingDay != null)
            {
                lsPupilRanking = lsPupilRanking.Where(em => (em.RankingDate == RankingDay));
            }
            if (IsCategory.HasValue)
            {
                lsPupilRanking = lsPupilRanking.Where(em => (em.IsCategory == IsCategory.Value));
            }
            if (StudyingJudgementID != 0)
            {
                lsPupilRanking = lsPupilRanking.Where(o => o.StudyingJudgementID == StudyingJudgementID);
            }


            return lsPupilRanking;

        }

        private PupilRankingHistory ConvertHistory(PupilRanking objpupilRanking)
        {
            PupilRankingHistory objPrHis = new PupilRankingHistory
            {

                PupilRankingID = objpupilRanking.PupilRankingID,
                PupilID = objpupilRanking.PupilID,
                ClassID = objpupilRanking.ClassID,
                AcademicYearID = objpupilRanking.AcademicYearID,
                EducationLevelID = objpupilRanking.EducationLevelID,
                Semester = objpupilRanking.Semester,
                PeriodID = objpupilRanking.PeriodID,
                AverageMark = objpupilRanking.AverageMark,
                TotalAbsentDaysWithPermission = objpupilRanking.TotalAbsentDaysWithPermission,
                TotalAbsentDaysWithoutPermission = objpupilRanking.TotalAbsentDaysWithoutPermission,
                CapacityLevelID = objpupilRanking.CapacityLevelID,
                ConductLevelID = objpupilRanking.ConductLevelID,
                Rank = objpupilRanking.Rank,
                RankingDate = objpupilRanking.RankingDate,
                StudyingJudgementID = objpupilRanking.StudyingJudgementID,
                SchoolID = objpupilRanking.SchoolID,
                IsCategory = objpupilRanking.IsCategory,
                M_ProvinceID = objpupilRanking.M_ProvinceID,
                M_OldID = objpupilRanking.M_OldID,
                IsOldData = objpupilRanking.IsOldData,
                MSourcedb = objpupilRanking.MSourcedb,
                Last2digitNumberSchool = objpupilRanking.Last2digitNumberSchool,
                CreatedAcademicYear = objpupilRanking.CreatedAcademicYear,
                SynchronizeID = objpupilRanking.SynchronizeID,
                Year = objpupilRanking.Year,
                PupilRankingComment = objpupilRanking.PupilRankingComment,
            };
            return objPrHis;
        }

        /// <summary>
        /// Get xep loai hoc luc cho hoc sinh
        /// </summary>
        /// <param name="AverageMark"></param>
        /// <param name="ListSummedUpRecord"></param>
        /// <returns></returns>
        private int GetCapacityTypeByMark(decimal? averageMark, List<SummedUpRecordHistory> ListSummedUpRecord, List<ClassSubject> lstClassSubject, int schoolID, TrainingType traningType, List<RegisterSubjectSpecializeBO> lstRegisterSubjectBO)
        {
            // Lay thong tin mon hoc tu ID            
            foreach (SummedUpRecordHistory s in ListSummedUpRecord)
            {
                if (s.SubjectCat == null)
                {
                    s.SubjectCat = SubjectCatBusiness.Find(s.SubjectID);
                    //s.SubjectCat = lstClassSubject.FirstOrDefault(cs => cs.ClassID == s.ClassID && cs.SubjectID == s.SubjectID).SubjectCat;
                }
            }

            //Chiendd1: 17/08/2015, mon tang cuong khong tham gia tinh diem tong ket
            //Danh sach mon hoc tang cuong
            List<int> lstClassSubjectPlus = lstClassSubject.Where(c => c.SubjectIDIncrease.HasValue && c.SubjectIDIncrease > 0).Select(c => c.SubjectIDIncrease.Value).ToList();

            //Danh sach mon tinh diem
            List<int> lstSubjectMark = lstClassSubject.Where(s => s.IsCommenting == GlobalConstants.SUBJECTCAT_ISCOMMENTING_MARK).Select(s => s.SubjectID).ToList();
            //Danh sach mon nhan xet
            List<int> lstSubjectJudge = lstClassSubject.Where(s => s.IsCommenting == GlobalConstants.SUBJECTCAT_ISCOMMENTING_COMMENT).Select(s => s.SubjectID).ToList();


            // Sắp xếp lại danh sách điểm theo thứ thự giảm dần
            // Lấy n - 1 môn để xếp loại, môn cuối cùng thấp điểm nhất dùng để xem xét việc nâng hạng    
            //Khong lay mon hoc 
            List<SummedUpRecordHistory> lstMandatoryMark = ListSummedUpRecord.Where(u => (lstSubjectMark != null && lstSubjectMark.Contains(u.SubjectID))
                                                                                        && (lstClassSubjectPlus != null && !lstClassSubjectPlus.Contains(u.SubjectID))
                                                                                    ).OrderByDescending(o => o.SummedUpMark)
                                                                                     .ToList();
            // Chi lay n - 1 mon neu tat ca cac mon deu D
            List<SummedUpRecordHistory> lstMandatoryJudge = ListSummedUpRecord.Where(u => (lstSubjectJudge != null && lstSubjectJudge.Contains(u.SubjectID))).ToList();
            decimal lowestMark = -1;
            // Mon hoc khai bao cho lop o mon thap nhat
            List<ClassSubject> lstClassSubjectLowest = new List<ClassSubject>();
            // Diem mon thap nhat
            List<SummedUpRecordHistory> lstSummedUpRecordLowest = new List<SummedUpRecordHistory>();
            // Co mon tin diem va mon nhan xet nao cung D
            int countCD = lstMandatoryJudge.Where(u => (!string.IsNullOrEmpty(u.ReTestJudgement) && u.ReTestJudgement == SystemParamsInFile.JUDGE_MARK_CD) ||
               (string.IsNullOrEmpty(u.ReTestJudgement) && u.JudgementResult != SystemParamsInFile.JUDGE_MARK_D)).Count();  // AnhVD 20140808 - cap nhat lay diem sau khi thi lai

            if (lstMandatoryMark.Count > 1 && countCD == 0)
            {
                lstClassSubjectLowest = lstClassSubject.Where(o => (o.IsSpecializedSubject.HasValue && o.IsSpecializedSubject.Value) || o.SubjectCat.IsCoreSubject).ToList();
                lstSummedUpRecordLowest = lstMandatoryMark.Where(u => lstClassSubjectLowest.Any(o => o.SubjectID == u.SubjectID && o.ClassID == u.ClassID)).ToList();
                SummedUpRecordHistory lastSu = lstMandatoryMark.Last();
                lowestMark = (lastSu != null && lastSu.SummedUpMark.HasValue) ? lastSu.SummedUpMark.Value : -1;
                lstMandatoryMark.RemoveAt(lstMandatoryMark.Count - 1);
                if (!lstClassSubjectLowest.Any(o => o.SubjectID == lastSu.SubjectID && o.ClassID == lastSu.ClassID))
                {
                    lstSummedUpRecordLowest.Add(lastSu);
                    List<ClassSubject> lstTemp = lstClassSubject.Where(o => o.SubjectID == lastSu.SubjectID
                                                                        && o.ClassID == lastSu.ClassID
                                                                        && (lstClassSubjectPlus != null && !lstClassSubjectPlus.Contains(o.SubjectID))
                                                                        ).ToList();
                    lstClassSubjectLowest.AddRange(lstTemp);

                    //lstClassSubjectLowest.AddRange(lstClassSubject.Where(o => o.SubjectID == lastSu.SubjectID && o.ClassID == lastSu.ClassID).ToList());
                }
            }
            string type_GDTX = "GDTX";
            bool isGDTX = (traningType != null && traningType.Resolution == type_GDTX);
            int capacityID = lstMandatoryMark.Count > 0 ?
                this.GetCapacityTypeByOnlyMark(averageMark, lstMandatoryMark, lstClassSubject, isGDTX, lstRegisterSubjectBO) : 0;
            // Neu khong phai la GDTX thi xet them mon nhan xet truoc khi xet toi viec nang bac            
            #region Xet mon nhan xet cho truong khong phai GDTX
            if (!isGDTX)
            {
                // Neu co 2 mon nhan xet CD thi xep loai yeu, neu TBM cac mon tinh diem tot hon la diem yeu thi xep loai hoc luc la yeu
                if (countCD > 1)
                {
                    if (capacityID < SystemParamsInFile.CAPACITY_TYPE_WEAK)
                    {
                        capacityID = SystemParamsInFile.CAPACITY_TYPE_WEAK;
                    }
                }
                else if (countCD == 1)
                {
                    // Neu sau khi xet theo mon tinh diem co loai xep hang tren trung binh thi moi tinh viec nang loai doi voi mon nhan xet chua dat
                    // Neu co mon nhan xet chua dat thi moi xet
                    if (capacityID == SystemParamsInFile.CAPACITY_TYPE_EXCELLENT || capacityID == SystemParamsInFile.CAPACITY_TYPE_GOOD)
                    {
                        capacityID = SystemParamsInFile.CAPACITY_TYPE_NORMAL;
                    }
                    else
                    {
                        // Neu TBM tinh diem tot hon loai yeu thi xep loai hoc luc la yeu
                        if (capacityID < SystemParamsInFile.CAPACITY_TYPE_WEAK)
                        {
                            capacityID = SystemParamsInFile.CAPACITY_TYPE_WEAK;
                        }
                    }
                }
            }
            #endregion
            // Xet viec nang diem neu co mon tinh diem
            if (lowestMark > -1)
            {
                // Hoc luc cua phan tu cuoi cung
                int lowestCapacityID = GetCapacityTypeByOnlyMark(averageMark, lstSummedUpRecordLowest, lstClassSubjectLowest, isGDTX, lstRegisterSubjectBO);
                // Xet nang bac
                if (capacityID == SystemParamsInFile.CAPACITY_TYPE_EXCELLENT
                    || capacityID == SystemParamsInFile.CAPACITY_TYPE_GOOD
                    || (isGDTX && capacityID == SystemParamsInFile.CAPACITY_TYPE_NORMAL))
                {
                    capacityID = PupilRankingBusiness.GetUpCapacity(capacityID, lowestCapacityID, isGDTX);
                }
                else
                {
                    // Neu khong thuoc dang nang bac thi lay bac cua diem thap nhat neu hoc luc diem thap nha kem hon cai hien tai
                    if (capacityID < lowestCapacityID)
                    {
                        capacityID = lowestCapacityID;
                    }
                }
            }

            return capacityID;
        }

        /// <summary>
        /// Lay xep loai hoc luc chi theo mon tinh diem va chua xet den nang bac - QuangLM
        /// </summary>
        /// <param name="averageMark"></param>
        /// <param name="ListSummedUpRecord"></param>
        /// <returns></returns>
        private int GetCapacityTypeByOnlyMark(decimal? averageMark, List<SummedUpRecordHistory> lstMandatoryMark, List<ClassSubject> lstClassSubject, bool isGDTX, List<RegisterSubjectSpecializeBO> lstRegisterSubjectBO)
        {
            Func<ClassSubject, bool> IsSpecializedSubject = u => u != null && u.IsSpecializedSubject.HasValue && u.IsSpecializedSubject.Value;
            // Bổ sung nghiệp vụ GDTX
            int capacityID = SystemParamsInFile.CAPACITY_TYPE_WEAK;
            bool continueToCheck = true;
            int pupilID = lstMandatoryMark.Select(p => p.PupilID).FirstOrDefault();
            #region Truong GDTX
            if (isGDTX)
            {
                // TODO GDTX
                /*•	CapacityID = CAPACITY_TYPE_EXCELLENT nếu:
                Điểm trung bình các môn học từ 8.0 trở lên (AverageMark đợt đang xét >= 8.0), trong đó điểm trung bình của 1 trong 2 môn Toán, Ngữ văn từ 8.0 trở lên 
                (SummedUpMark đợt đang xét của 1 trong các môn có IsCoreSubject (trong bảng SubjectCat) = 1 >= 8.0); 
                 	Không có môn học nào điểm trung bình dưới 6.5 (lstPupilRankingOfPupil_Mandatory [i].SummedUpMark >= 6.5) */
                if (continueToCheck
                    && averageMark >= GlobalConstants.EXCELLENT_MARK //Điểm trung bình các môn học từ 8.0 trở lên
                    && (!lstMandatoryMark.Any(u => u.SubjectCat.IsCoreSubject) || lstMandatoryMark.Any(u => u.SubjectCat.IsCoreSubject && u.SummedUpMark >= GlobalConstants.EXCELLENT_MARK)) //Không có môn chính hoặc phải Có môn chính trên 8
                    && !lstMandatoryMark.Any(u => u.SummedUpMark < GlobalConstants.GOOD_MARK) //Không có điểm trung bình dưới 6.5                
                )
                {
                    capacityID = SystemParamsInFile.CAPACITY_TYPE_EXCELLENT;
                    continueToCheck = false;
                }
                /*•	CapacityID = CAPACITY_TYPE_GOOD nếu:
                	Điểm trung bình các môn học từ 6.5 trở lên (AverageMark đợt đang xét >= 6.5), trong đó điểm trung bình của 1 trong 2 môn Toán, Ngữ văn từ 6.5 trở lên 
                (SummedUpMark đợt đang xét của 1 trong các môn có IsCoreSubject (trong bảng SubjectCat) = 1 >= 6.5); 
                	Không có môn học nào điểm trung bình dưới 5.0 (lstPupilRankingOfPupil_Mandatory [i].SummedUpMark >= 5.0);*/
                if (continueToCheck
                    && averageMark >= GlobalConstants.GOOD_MARK //Điểm trung bình các môn học từ 6.5 trở lên
                    && (!lstMandatoryMark.Any(u => u.SubjectCat.IsCoreSubject) || lstMandatoryMark.Any(u => u.SubjectCat.IsCoreSubject && u.SummedUpMark >= GlobalConstants.GOOD_MARK)) //Không có môn chính hoặc phải Có môn chính trên 6.5
                    && !lstMandatoryMark.Any(u => u.SummedUpMark < GlobalConstants.NORMAL_MARK) //Không có điểm trung bình dưới 5                
                )
                {
                    capacityID = SystemParamsInFile.CAPACITY_TYPE_GOOD;
                    continueToCheck = false;
                }
                /*•	CapacityID = CAPACITY_TYPE_NORMAL nếu:
                	Điểm trung bình các môn học từ 5.0 trở lên (AverageMark đợt đang xét >= 5.0), 
                trong đó điểm trung bình của 1 trong 2 môn Toán, Ngữ văn từ 5.0 trở lên 
                (SummedUpMark đợt đang xét của 1 trong các môn có IsCoreSubject (trong bảng SubjectCat) = 1 >= 5.0); 
                	Không có môn học nào điểm trung bình dưới 3.5 (lstPupilRankingOfPupil_Mandatory [i].SummedUpMark >= 3.5);*/
                if (continueToCheck
                    && averageMark >= GlobalConstants.NORMAL_MARK //Điểm trung bình các môn học từ 5.0 trở lên
                    && (!lstMandatoryMark.Any(u => u.SubjectCat.IsCoreSubject) || lstMandatoryMark.Any(u => u.SubjectCat.IsCoreSubject && u.SummedUpMark >= GlobalConstants.NORMAL_MARK)) //Không có môn chính hoặc phải Có môn chính tren 5
                    && !lstMandatoryMark.Any(u => u.SummedUpMark < GlobalConstants.WEAK_MARK) //Không có điểm trung bình dưới 3.5
                )
                {
                    capacityID = SystemParamsInFile.CAPACITY_TYPE_NORMAL;
                    continueToCheck = false;
                }
                /*•	CapacityID = CAPACITY_TYPE_WEAK nếu:
                //	Điểm trung bình các môn từ 3.5 trở lên (AverageMark đợt đang xét >= 3.5), 
                //không có môn nào điểm trung bình dưới 2.0 (lstPupilRankingOfPupil_Mandatory [i].SummedUpMark >= 2.0)*/
                if (continueToCheck
                    && averageMark >= GlobalConstants.WEAK_MARK //Điểm trung bình các môn học từ 3.5 trở lên
                    && (!lstMandatoryMark.Any(u => u.SubjectCat.IsCoreSubject) || lstMandatoryMark.Any(u => u.SubjectCat.IsCoreSubject && u.SummedUpMark >= GlobalConstants.WEAK_MARK)) //Không có môn chính hoặc phải Có môn chính tren 3.5
                    && !lstMandatoryMark.Any(u => u.SummedUpMark < GlobalConstants.POOR_MARK) //Không có điểm trung bình dưới 2                
                )
                {
                    capacityID = SystemParamsInFile.CAPACITY_TYPE_WEAK;
                    continueToCheck = false;
                }

                if (continueToCheck)
                {
                    capacityID = SystemParamsInFile.CAPACITY_TYPE_POOR;
                    continueToCheck = false;
                }
            }
            #endregion
            #region Truong binh thuong
            else
            {
                //•	CapacityID = CAPACITY_TYPE_EXCELLENT nếu:
                //	Điểm trung bình các môn học từ 8.0 trở lên (AverageMark đợt đang xét >= 8.0), 
                //trong đó điểm trung bình của 1 trong 2 môn Toán, Ngữ văn từ 8.0 trở lên 
                //(SummedUpMark đợt đang xét của 1 trong các môn có IsCoreSubject (trong bảng SubjectCat) = 1 >= 8.0); 
                //riêng đối với học sinh lớp chuyên của trường THPT chuyên phải thêm điều kiện điểm trung bình môn chuyên từ 8,0 trở lên 
                //(SummedUpMark đợt đang xét của môn có IsSpecializedSubject (trong bảng ClassSubject) = 1 >= 8.0); 
                //	Không có môn học nào điểm trung bình dưới 6.5 (lstPupilRankingOfPupil_Mandatory [i].SummedUpMark >= 6.5);
                //	Các môn học đánh giá bằng nhận xét đạt loại Đ (lstPupilRankingOfPupil_Mandatory [i].JudgementResult = “Đ”) --> Chuyen xuong xet o duoi
                //Nếu là môn chuyên nhưng không đăng ký học thì vẫn tính hoc lưc như môn bình thường

                if (continueToCheck
                    && averageMark >= GlobalConstants.EXCELLENT_MARK //Điểm trung bình các môn học từ 8.0 trở lên
                    && (!lstMandatoryMark.Any(u => u.SubjectCat.IsCoreSubject) || lstMandatoryMark.Any(u => u.SubjectCat.IsCoreSubject && u.SummedUpMark >= GlobalConstants.EXCELLENT_MARK)) //Không có môn chính hoặc phải Có môn chính trên 8
                    && (this.checkIsSpecializeHis(pupilID, lstRegisterSubjectBO, lstClassSubject, lstMandatoryMark, GlobalConstants.EXCELLENT_MARK)) //không có môn chuyên dưới 8
                    && !lstMandatoryMark.Any(u => u.SummedUpMark < GlobalConstants.GOOD_MARK) //Không có điểm trung bình dưới 6.5                
                )
                {
                    capacityID = SystemParamsInFile.CAPACITY_TYPE_EXCELLENT;
                    continueToCheck = false;
                }

                //•	CapacityID = CAPACITY_TYPE_GOOD nếu:
                //	Điểm trung bình các môn học từ 6.5 trở lên (AverageMark đợt đang xét >= 6.5), trong đó điểm trung bình của 1 trong 2 môn Toán, Ngữ văn từ 6.5 trở lên 
                //(SummedUpMark đợt đang xét của 1 trong các môn có IsCoreSubject (trong bảng SubjectCat) = 1 >= 6.5); 
                //riêng đối với học sinh lớp chuyên của trường THPT chuyên phải thêm điều kiện điểm trung bình môn chuyên từ 6.5 trở lên 
                //nếu môn chuyên mà học sinh không đăng ký học thì vẫn tính học lực như môn bình thường
                //(SummedUpMark đợt đang xét của môn có IsSpecializedSubject (trong bảng ClassSubject) = 1 >= 6.5); 
                //	Không có môn học nào điểm trung bình dưới 5.0 (lstPupilRankingOfPupil_Mandatory [i].SummedUpMark >= 5.0);
                //	Các môn học đánh giá bằng nhận xét đạt loại Đ (lstPupilRankingOfPupil_Mandatory [i].JudgementResult = “Đ”) --> Chuyen xuong xet o duoi
                if (continueToCheck
                    && averageMark >= GlobalConstants.GOOD_MARK //Điểm trung bình các môn học từ 8.0 trở lên
                    && (!lstMandatoryMark.Any(u => u.SubjectCat.IsCoreSubject) || lstMandatoryMark.Any(u => u.SubjectCat.IsCoreSubject && u.SummedUpMark >= GlobalConstants.GOOD_MARK)) //Không có môn chính hoặc phải Có môn chính trên 6.5
                    && (this.checkIsSpecializeHis(pupilID, lstRegisterSubjectBO, lstClassSubject, lstMandatoryMark, GlobalConstants.GOOD_MARK)) //Là trường chuyên và không có môn chuyên dưới 6.5
                    && !lstMandatoryMark.Any(u => u.SummedUpMark < GlobalConstants.NORMAL_MARK) //Không có điểm trung bình dưới 5                
                )
                {
                    capacityID = SystemParamsInFile.CAPACITY_TYPE_GOOD;
                    continueToCheck = false;
                }

                //•	CapacityID = CAPACITY_TYPE_NORMAL nếu:
                //	Điểm trung bình các môn học từ 5.0 trở lên (AverageMark đợt đang xét >= 5.0), 
                //trong đó điểm trung bình của 1 trong 2 môn Toán, Ngữ văn từ 5.0 trở lên 
                //(SummedUpMark đợt đang xét của 1 trong các môn có IsCoreSubject (trong bảng SubjectCat) = 1 >= 5.0); 
                //riêng đối với học sinh lớp chuyên của trường THPT chuyên phải thêm điều kiện điểm trung bình môn chuyên từ 5.0trở lên 
                //nếu môn chuyên mà học sinh không đăng ký học thì vẫn tính học lực như môn bình thường
                //(SummedUpMark đợt đang xét của môn có IsSpecializedSubject (trong bảng ClassSubject) = 1 >= 5.0); 
                //	Không có môn học nào điểm trung bình dưới 3.5 (lstPupilRankingOfPupil_Mandatory [i].SummedUpMark >= 3.5);
                //	Các môn học đánh giá bằng nhận xét đạt loại Đ (lstPupilRankingOfPupil_Mandatory [i].JudgementResult = “Đ”) --> Chuyen xuong xet o duoi
                if (continueToCheck
                    && averageMark >= GlobalConstants.NORMAL_MARK //Điểm trung bình các môn học từ 8.0 trở lên
                    && (!lstMandatoryMark.Any(u => u.SubjectCat.IsCoreSubject) || lstMandatoryMark.Any(u => u.SubjectCat.IsCoreSubject && u.SummedUpMark >= GlobalConstants.NORMAL_MARK)) //Không có môn chính hoặc phải Có môn chính tren 5
                    && (this.checkIsSpecializeHis(pupilID, lstRegisterSubjectBO, lstClassSubject, lstMandatoryMark, GlobalConstants.NORMAL_MARK)) //Là trường chuyên và không có môn chuyên dưới 5
                    && !lstMandatoryMark.Any(u => u.SummedUpMark < GlobalConstants.WEAK_MARK) //Không có điểm trung bình dưới 3.5
                )
                {
                    capacityID = SystemParamsInFile.CAPACITY_TYPE_NORMAL;
                    continueToCheck = false;
                }

                //•	CapacityID = CAPACITY_TYPE_WEAK nếu:
                //	Điểm trung bình các môn từ 3.5 trở lên (AverageMark đợt đang xét >= 3.5), 
                //không có môn nào điểm trung bình dưới 2.0 (lstPupilRankingOfPupil_Mandatory [i].SummedUpMark >= 2.0)
                if (continueToCheck
                    && averageMark >= GlobalConstants.WEAK_MARK //Điểm trung bình các môn học từ 8.0 trở lên
                    && !lstMandatoryMark.Any(u => u.SummedUpMark.Value < GlobalConstants.POOR_MARK) //Không có điểm trung bình dưới 2                
                )
                {
                    capacityID = SystemParamsInFile.CAPACITY_TYPE_WEAK;
                    continueToCheck = false;
                }

                if (continueToCheck)
                {
                    capacityID = SystemParamsInFile.CAPACITY_TYPE_POOR;
                    continueToCheck = false;
                }
                // Neu sau khi xet nang van bi loai kem thi giu nguyen
                if (capacityID == SystemParamsInFile.CAPACITY_TYPE_POOR)
                {
                    return capacityID;
                }
            }
            #endregion
            return capacityID;
        }

        private bool checkIsSpecializeHis(int pupilID, List<RegisterSubjectSpecializeBO> lstRegister, List<ClassSubject> lstClassSubject, List<SummedUpRecordHistory> lstSummed, decimal equalMark)
        {
            ClassSubject objClassSubject = new ClassSubject();
            SummedUpRecordHistory objSummed = new SummedUpRecordHistory();
            int count = 0;
            bool check = true;
            for (int i = 0; i < lstClassSubject.Count; i++)
            {
                objClassSubject = lstClassSubject[i];
                if (objClassSubject.IsCommenting == 1)
                {
                    continue;
                }
                objSummed = lstSummed.Where(p => p.SubjectID == objClassSubject.SubjectID).FirstOrDefault();
                if (objSummed == null)
                {
                    continue;
                }
                count = lstRegister.Where(p => p.PupilID == pupilID && p.ClassID == objClassSubject.ClassID && p.SubjectID == objClassSubject.SubjectID).Count();
                if (objClassSubject.IsSpecializedSubject.HasValue && objClassSubject.IsSpecializedSubject.Value && objSummed.SummedUpMark < equalMark && count > 0)
                {
                    check = false;
                }
                else
                {
                    check = true;
                }
            }
            return check;
        }


        #endregion

    }
}

