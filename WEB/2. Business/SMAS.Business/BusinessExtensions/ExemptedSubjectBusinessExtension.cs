﻿/**
 * The Viettel License
 *
 * Copyright 2012 Viettel Telecom. All rights reserved.
 * VIETTEL PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */

/** 
* @author  
* @version $Revision: $
*/

using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Linq.Expressions;
using System.Web;
using log4net;

using SMAS.DAL.Repository;
using SMAS.DAL.IRepository;
using SMAS.Business.IBusiness;
using SMAS.Models.Models;
using SMAS.Business.Common;
using System.Text;
using SMAS.Business.BusinessObject;

namespace SMAS.Business.Business
{
    public partial class ExemptedSubjectBusiness
    {
        private void ValidateObject(ExemptedSubject entity)
        {
            //Validate object
            ValidationMetadata.ValidateObject(entity);

            //Da check trong metadata
            //Utils.ValidateNull(entity.Year, "ExemptedSubject_Label_Year");
            //this.AcademicYearBusiness.CheckAvailable(entity.AcademicYearID, "ExemptedSubject_Label_AcademicYearID");
            //this.EducationLevelBusiness.CheckAvailable(entity.EducationLevelID, "ExemptedSubject_Label_EducationLevelID");
            //this.SubjectCatBusiness.CheckAvailable(entity.SubjectID, "ClassSubject_Label_ClassSubjectID");

            //Kiem tra null SchoolID
            //Utils.ValidateNull(entity.SchoolID, "ExemptedSubject_Label_ClassID");

            //Check SchoolID, AcademicYearID compatible in AcademicYear
            bool AcademicYearCompatible = new AcademicYearRepository(this.context).ExistsRow(GlobalConstants.SCHOOL_SCHEMA, "AcademicYear"
                                            , new Dictionary<string, object>()
                                            {
                                                {"AcademicYearID" , entity.AcademicYearID},
                                                {"SchoolID", entity.SchoolID}
                                            }, null);
            if (!AcademicYearCompatible)
                throw new BusinessException("Common_Validate_NotCompatible");

            //Kiem tra null ClassID
            //Utils.ValidateNull(entity.ClassID, "ExemptedSubject_Label_ClassID");

            //Kiem tra tuong thichs ClassProfile 
            bool ClassProfileCompatible = new ClassProfileRepository(this.context).ExistsRow(GlobalConstants.SCHOOL_SCHEMA, "ClassProfile"
                                            , new Dictionary<string, object>()
                                            {
                                                {"ClassProfileID" , entity.ClassID},
                                                {"AcademicYearID", entity.AcademicYearID}
                                            }, null);
            if (!ClassProfileCompatible)
                throw new BusinessException("Common_Validate_NotCompatible");

            //Kiem tra tuong thich ClassSubject
            bool ClassSubjectCompatible = new ClassSubjectRepository(this.context).ExistsRow(GlobalConstants.SCHOOL_SCHEMA, "ClassSubject"
                                            , new Dictionary<string, object>()
                                            {
                                                {"SubjectID" , entity.SubjectID},
                                                {"ClassID", entity.ClassID}
                                            }, null);
            if (!ClassSubjectCompatible)
                throw new BusinessException("Common_Validate_NotCompatible");

            //Kiem tra tuong thichs ClassProfile 
            bool ClassProfileCompatibleEdu = new ClassProfileRepository(this.context).ExistsRow(GlobalConstants.SCHOOL_SCHEMA, "ClassProfile"
                                            , new Dictionary<string, object>()
                                            {
                                                {"ClassProfileID" , entity.ClassID},
                                                {"EducationLevelID", entity.EducationLevelID}
                                            }, null);
            if (!ClassProfileCompatibleEdu)
                throw new BusinessException("Common_Validate_NotCompatible");

            //Neu da co diem thi khong the insert.
            //Diem da ton tai trong bang MarkRecord thi khong cho mien toan phan
            AcademicYear academicYear = AcademicYearBusiness.Find(entity.AcademicYearID);
            MarkRecord markRecord = this.MarkRecordBusiness.SearchBySchool(entity.SchoolID, new Dictionary<string, object> { { "Year", academicYear.Year }, { "PupilID", entity.PupilID }, { "ClassID", entity.ClassID }, { "SubjectID", entity.SubjectID }, { "Semester", SystemParamsInFile.SEMESTER_OF_YEAR_FIRST } }).FirstOrDefault();
            JudgeRecord judgeRecord = this.JudgeRecordBusiness.SearchBySchool(entity.SchoolID, new Dictionary<string, object> { { "Year", academicYear.Year }, { "PupilID", entity.PupilID }, { "ClassID", entity.ClassID }, { "SubjectID", entity.SubjectID }, { "Semester", SystemParamsInFile.SEMESTER_OF_YEAR_FIRST } }).FirstOrDefault();

            //if (this.MarkRecordBusiness
            //        .SearchBySchool(entity.SchoolID
            //                        , new Dictionary<string, object>
            //                        {
            //                            { "PupilID", entity.PupilID },
            //                            { "ClassID", entity.ClassID },
            //                            { "SubjectID", entity.SubjectID }
            //                        })
            //        .FirstOrDefault() != null 
            //        && ((entity.FirstSemesterExemptType.HasValue && entity.FirstSemesterExemptType.Value == SystemParamsInFile.EXEMPT_TYPE_ALL) 
            //                || (entity.SecondSemesterExemptType.HasValue && entity.SecondSemesterExemptType.Value == SystemParamsInFile.EXEMPT_TYPE_ALL))
            //    )
            //    throw new BusinessException("ExemptedSubject_Validate_ExemptTypeAll");

            if (entity.FirstSemesterExemptType.HasValue && (markRecord != null || judgeRecord != null))
            {
                string subjectName = markRecord != null ? markRecord.SubjectCat.SubjectName : judgeRecord.SubjectCat.SubjectName;
                List<object> lstParams = new List<object>();
                lstParams.Add(subjectName);
                throw new BusinessException("ExemptedSubject_Validate_ExemptTypeAllSemester1", lstParams);
            }
            IQueryable<MarkRecord> iqMarkRecord = this.MarkRecordBusiness.SearchBySchool(entity.SchoolID, new Dictionary<string, object> { { "Year", academicYear.Year }, { "PupilID", entity.PupilID }, { "ClassID", entity.ClassID }, { "SubjectID", entity.SubjectID }, { "Semester", SystemParamsInFile.SEMESTER_OF_YEAR_SECOND } });
            MarkRecord markRecord2 = null;
            if (iqMarkRecord.Count() > 0)
            {
                markRecord2 = iqMarkRecord.FirstOrDefault();
            }
            IQueryable<JudgeRecord> iqJudgeRecord = this.JudgeRecordBusiness.SearchBySchool(entity.SchoolID, new Dictionary<string, object> { { "Year", academicYear.Year }, { "PupilID", entity.PupilID }, { "ClassID", entity.ClassID }, { "SubjectID", entity.SubjectID }, { "Semester", SystemParamsInFile.SEMESTER_OF_YEAR_SECOND } });
            JudgeRecord judgeRecord2 = null;
            if (iqJudgeRecord.Count() > 0)
            {
                judgeRecord2 = iqJudgeRecord.FirstOrDefault();
            }

            if (entity.SecondSemesterExemptType.HasValue && (markRecord2 != null || judgeRecord2 != null))
            {
                string subjectName = markRecord2 != null ? markRecord2.SubjectCat.SubjectName : judgeRecord2.SubjectCat.SubjectName;
                List<object> lstParams = new List<object>();
                lstParams.Add(subjectName);
                throw new BusinessException("ExemptedSubject_Validate_ExemptTypeAllSemester2", lstParams);
            }
            //Diem da ton tai trong bang JudgeRecord thi khong cho mien toan phan
            //if (this.JudgeRecordBusiness
            //        .SearchBySchool(entity.SchoolID
            //                        , new Dictionary<string, object>
            //                        {
            //                            { "PupilID", entity.PupilID },
            //                            { "ClassID", entity.ClassID },
            //                            { "SubjectID", entity.SubjectID }
            //                        })
            //        .FirstOrDefault() != null 
            //        && ((entity.FirstSemesterExemptType.HasValue && entity.FirstSemesterExemptType.Value == SystemParamsInFile.EXEMPT_TYPE_ALL) 
            //            || (entity.SecondSemesterExemptType.HasValue && entity.SecondSemesterExemptType.Value == SystemParamsInFile.EXEMPT_TYPE_ALL))
            //    )
            //    throw new BusinessException("ExemptedSubject_Validate_ExemptTypeAll");
        }

        public void InsertExemptedSubject(int UserID, int PupilID, int ClassID, int ExemptedOjectID, List<ExemptedSubject> lstExemptedSubject)
        {
            Utils.ValidateNull(lstExemptedSubject, "List is null");

            //Kiem tra ton tai hoc sinh
            var pupilProfile = this.PupilProfileBusiness.Find(PupilID);
            Utils.ValidateNull(pupilProfile, "PupilProfile_Label_PupilProfileID");

            //Kiem tra ton tai Lop hoc
            var classProfile = this.ClassProfileBusiness.Find(ClassID);
            Utils.ValidateNull(classProfile, "ClassProfile_Label_ClassProfileID");

            //Kiem tra ton tai truong
            this.SchoolProfileBusiness.CheckAvailable(classProfile.SchoolID, "SchoolProfile_Label_SchoolProfileID");

            //Check duplicate
            bool ExemptedSubjectDuplicate = new ExemptedSubjectRepository(this.context).ExistsRow(GlobalConstants.PUPIL_SCHEMA, "ExemptedSubject",
                                            new Dictionary<string, object>()
                                            {
                                                {"PupilID" , PupilID},
                                                {"ClassID", ClassID}
                                            }, null);
            if (ExemptedSubjectDuplicate)
                throw new BusinessException("Duplicate");

            //Kiem tra Academic year, education level
            foreach (var entity in lstExemptedSubject)
                ValidateObject(entity);

            //Kiem tra ton tai exemptObject
            this.ExemptedObjectTypeBusiness.CheckAvailable(ExemptedOjectID, "ExemptedObjectType_Label_ExemptedObjectTypeID");

            //Kiem tra trang thai cua hoc sinh
            if (this.PupilOfClassBusiness.All.Count(u => u.PupilID == PupilID && u.ClassID == ClassID && u.Status == GlobalConstants.PUPIL_STATUS_STUDYING) == 0)
                throw new BusinessException("Common_Validate_Pupil_Status_Studying");

            //Kiem tra tuong thichs PupilProfile 
            bool PupilProfileCompatible = new PupilProfileRepository(this.context).ExistsRow(GlobalConstants.PUPIL_SCHEMA, "PupilProfile",
                                            new Dictionary<string, object>()
                                            {
                                                {"PupilProfileID" , PupilID},
                                                {"CurrentClassID", ClassID}
                                            }, null);
            if (!PupilProfileCompatible)
                throw new BusinessException("Common_Validate_NotCompatible");

            if (!UtilsBusiness.HasHeadTeacherPermission(UserID, ClassID))
                throw new BusinessException("Common_Validate_HeadTeacherPermission");

            foreach (var es in lstExemptedSubject)
            {
                es.SchoolID = classProfile.SchoolID;
                es.AcademicYearID = classProfile.AcademicYearID;
                es.EducationLevelID = classProfile.EducationLevelID;
                //TODO : HIEUND : Bo % 100
                es.Year = (int)(classProfile.AcademicYear.Year % 100);
                es.PupilID = PupilID;
                es.ExemptedObjectID = (int)ExemptedOjectID;
                base.Insert(es);
            }
        }

        public void UpdateExemptedSubject(int UserID, int PupilID, int ClassID, int ExemptedOjectID, List<ExemptedSubject> lstExemptedSubject, ref ActionAuditDataBO objAC)
        {
            Utils.ValidateNull(lstExemptedSubject, "List is null");

            //Kiem tra ton tai hoc sinh
            var pupilProfile = this.PupilProfileBusiness.Find(PupilID);
            Utils.ValidateNull(pupilProfile, "PupilProfile_Label_PupilProfileID");

            //Kiem tra ton tai Lop hoc
            var classProfile = this.ClassProfileBusiness.Find(ClassID);
            Utils.ValidateNull(classProfile, "ClassProfile_Label_ClassProfileID");

            //Kiem tra ton tai truong
            this.SchoolProfileBusiness.CheckAvailable(classProfile.SchoolID, "SchoolProfile_Label_SchoolProfileID");

            //Kiem tra Academic year, education level
            foreach (var entity in lstExemptedSubject)
                ValidateObject(entity);

            //Kiem tra ton tai exemptObject
            this.ExemptedObjectTypeBusiness.CheckAvailable(ExemptedOjectID, "ExemptedObjectType_Label_ExemptedObjectTypeID");

            //Kiem tra tuong thichs ClassProfile 
            bool PupilProfileCompatible = new PupilProfileRepository(this.context).ExistsRow(GlobalConstants.PUPIL_SCHEMA, "PupilProfile",
                                            new Dictionary<string, object>()
                                            {
                                                {"PupilProfileID" , PupilID},
                                                {"CurrentClassID", ClassID}
                                            }, null);
            if (!PupilProfileCompatible)
                throw new BusinessException("Common_Validate_NotCompatible");

            if (!UtilsBusiness.HasHeadTeacherPermission(UserID, ClassID))
                throw new BusinessException("Common_Validate_HeadTeacherPermission");

            #region Tao du lieu ghi log
            StringBuilder objectIDStr = new StringBuilder();
            StringBuilder descriptionStr = new StringBuilder();
            StringBuilder paramsStr = new StringBuilder();
            StringBuilder userFuntionsStr = new StringBuilder();
            StringBuilder userActionsStr = new StringBuilder();
            StringBuilder userDescriptionsStr = new StringBuilder();

            PupilProfile objPF = PupilProfileBusiness.Find(PupilID);
            string ExemptedTypeName = string.Empty;
            objectIDStr.Append(PupilID);
            descriptionStr.Append("Cập nhật hồ sơ học sinh");
            paramsStr.Append(PupilID);
            userFuntionsStr.Append("Hồ sơ học sinh");
            userActionsStr.Append(SMAS.Business.Common.GlobalConstants.ACTION_UPDATE);
            #endregion
            List<ExemptedSubject> lstExemptedDB = ExemptedSubjectBusiness.All.Where(u => u.ClassID == ClassID && u.PupilID == PupilID).ToList();
            ExemptedSubject es = null;
            StringBuilder strExempted = new StringBuilder();
            if (lstExemptedDB.Count > 0)
            {
                userDescriptionsStr.Append("Xóa miễn giảm cho HS ").Append(objPF.FullName + ", mã ").Append(objPF.PupilCode).Append(", ");
                for (int i = 0; i < lstExemptedDB.Count; i++)
                {
                    strExempted = new StringBuilder();
                    es = lstExemptedDB[i];
                    strExempted.Append(es.SubjectCat.SubjectName).Append("/");
                    if (es.FirstSemesterExemptType.HasValue)
                    {
                        ExemptedTypeName = "(Miễn kỳ 1/Miễn giảm toàn phần" + (es.SecondSemesterExemptType.HasValue ? ", " : ")");
                    }
                    if (es.SecondSemesterExemptType.HasValue)
                    {
                        ExemptedTypeName += "Miễn kỳ 2/Miễn giảm toàn phần)";
                    }
                    strExempted.Append(ExemptedTypeName);
                    if (i < lstExemptedSubject.Count - 1)
                    {
                        strExempted.Append("; ");
                    }
                    userDescriptionsStr.Append(strExempted);

                    es.SchoolID = classProfile.SchoolID;
                    es.AcademicYearID = classProfile.AcademicYearID;
                    es.EducationLevelID = classProfile.EducationLevelID;
                    es.Year = classProfile.AcademicYear.Year;
                    es.PupilID = PupilID;
                    es.ExemptedObjectID = (int)ExemptedOjectID;
                    //base.Insert(es);
                }
                base.DeleteAll(lstExemptedDB);
            }

            if (lstExemptedSubject.Count > 0)
            {
                List<int> lstSubjectID = lstExemptedSubject.Select(p => p.SubjectID).Distinct().ToList();
                List<SubjectCat> lstSC = SubjectCatBusiness.All.Where(p => lstSubjectID.Contains(p.SubjectCatID)).ToList();
                SubjectCat objSC = null;
                userDescriptionsStr.Append("Cập nhật miễn giảm cho HS ").Append(objPF.FullName + ", mã ").Append(objPF.PupilCode).Append(", ");
                for (int i = 0; i < lstExemptedSubject.Count; i++)
                {
                    strExempted = new StringBuilder();
                    es = lstExemptedSubject[i];
                    es.SchoolID = classProfile.SchoolID;
                    es.AcademicYearID = classProfile.AcademicYearID;
                    es.EducationLevelID = classProfile.EducationLevelID;
                    es.Year = classProfile.AcademicYear.Year;
                    es.PupilID = PupilID;
                    es.ExemptedObjectID = (int)ExemptedOjectID;
                    base.Insert(es);

                    objSC = lstSC.Where(p => p.SubjectCatID == es.SubjectID).FirstOrDefault();
                    strExempted.Append(objSC.DisplayName).Append("/");
                    if (es.FirstSemesterExemptType.HasValue)
                    {
                        ExemptedTypeName = "(Miễn kỳ 1/Miễn giảm toàn phần" + (es.SecondSemesterExemptType.HasValue ? ", " : ")");
                    }

                    if (es.SecondSemesterExemptType.HasValue)
                    {
                        ExemptedTypeName += "Miễn kỳ 2/Miễn giảm toàn phần)";
                    }
                    strExempted.Append(ExemptedTypeName);
                    if (i < lstExemptedSubject.Count - 1)
                    {
                        strExempted.Append("; ");
                    }
                    userDescriptionsStr.Append(strExempted);
                }
            }
            objAC.PupilID = PupilID;
            objAC.ClassID = ClassID;
            objAC.ObjID = objectIDStr;
            objAC.Parameter = paramsStr;
            objAC.Description = descriptionStr;
            objAC.UserFunction = userFuntionsStr;
            objAC.UserAction = userActionsStr;
            objAC.UserDescription = userDescriptionsStr;
        }

        public void DeleteExemptedSubject(int UserID, int ExemptedSubjectID, int SchoolID)
        {
            //Kiem tra ton tai mien giam mon hoc
            var exemptedSubject = this.ExemptedSubjectRepository.All.SingleOrDefault(u => u.ExemptedSubjectID == ExemptedSubjectID);
            Utils.ValidateNull(exemptedSubject, "ExemptedSubject_Label_ExemptedSubjectID");

            //Kiem tra tuong thichs PupilProfile 
            bool PupilProfileCompatible = new PupilProfileRepository(this.context).ExistsRow(GlobalConstants.PUPIL_SCHEMA, "PupilProfile",
                                            new Dictionary<string, object>()
                                            {
                                                {"PupilProfileID" , exemptedSubject.PupilID},
                                                {"CurrentSchoolID", SchoolID}
                                            }, null);
            if (!PupilProfileCompatible)
                throw new BusinessException("Common_Validate_NotCompatible");

            //Kiem tra trang thai cua hoc sinh
            if (this.PupilOfClassBusiness.All.Count(u => u.PupilID == exemptedSubject.PupilID && u.ClassID == exemptedSubject.ClassID && u.Status == GlobalConstants.PUPIL_STATUS_STUDYING) == 0)
                throw new BusinessException("Common_Validate_Pupil_Status_Studying");

            //Kiem tra nam hoc la nam hien tai
            if (!this.AcademicYearBusiness.IsCurrentYear(exemptedSubject.AcademicYearID))
                throw new BusinessException("ExemptedSubject_Label_AcademicYear");

            //Kiem tra quyen xoa
            if (!UtilsBusiness.HasHeadTeacherPermission(UserID, exemptedSubject.SubjectID))
                throw new BusinessException("Common_Validate_HeadTeacherPermission");

            base.Delete(ExemptedSubjectID);
        }

        public IQueryable<ExemptedSubject> Search(IDictionary<string, object> dic)
        {
            int? ExemptedSubjectID = Utils.GetNullableInt(dic, "ExemptedSubjectID");
            int? PupilID = Utils.GetNullableInt(dic, "PupilID");
            int? ClassID = Utils.GetNullableInt(dic, "ClassID");
            int? SchoolID = Utils.GetNullableInt(dic, "SchoolID");
            int? AcademicYearID = Utils.GetNullableInt(dic, "AcademicYearID");
            int? EducationLevelID = Utils.GetNullableInt(dic, "EducationLevelID");
            int? Year = Utils.GetNullableInt(dic, "Year");
            int? ExemptedObjectID = Utils.GetNullableInt(dic, "ExemptedObjectID");
            string PupilCode = Utils.GetString(dic, "PupilCode");
            string PupilName = Utils.GetString(dic, "PupilName");
            int? SubjectID = Utils.GetNullableInt(dic, "SubjectID");
            int? FirstSemesterExemptType = Utils.GetNullableInt(dic, "FirstSemesterExemptType");
            int? SecondSemesterExemptType = Utils.GetNullableInt(dic, "SecondSemesterExemptType");
            string Description = Utils.GetString(dic, "Description");
            DateTime? CreatedDate = Utils.GetDateTime(dic, "CreatedDate");
            DateTime? ModifiedDate = Utils.GetDateTime(dic, "ModifiedDate");
            bool? HasFirstSemester = Utils.GetNullableBool(dic, "HasFirstSemester");
            bool? HasSecondSemester = Utils.GetNullableBool(dic, "HasSecondSemester");
            int? AppliedLevel = Utils.GetNullableInt(dic, "AppliedLevel");
            List<int> lstClassID = Utils.GetIntList(dic, "lstClassID");
            List<int> lstSubjectID = Utils.GetIntList(dic, "lstSubjectID");
            int semesterID = Utils.GetInt(dic, "SemesterID");
            var lstExemptedSubject = this.ExemptedSubjectRepository.All;

            if (ClassID != 0 && ClassID != null)
            {
                lstExemptedSubject = from es in lstExemptedSubject
                                     join cp in ClassProfileBusiness.All on es.ClassID equals cp.ClassProfileID
                                     where (!cp.IsActive.HasValue || (cp.IsActive.HasValue && cp.IsActive.Value))
                                     && es.ClassID == ClassID
                                     select es;
            }
            if (lstClassID.Count > 0)
            {
                lstExemptedSubject = from es in lstExemptedSubject
                                     join cp in ClassProfileBusiness.All on es.ClassID equals cp.ClassProfileID
                                     where (!cp.IsActive.HasValue || (cp.IsActive.HasValue && cp.IsActive.Value))
                                     && lstClassID.Contains(es.ClassID)
                                     select es;
            }
            if (ExemptedSubjectID.HasValue && ExemptedSubjectID.Value > 0) lstExemptedSubject = lstExemptedSubject.Where(u => u.ExemptedSubjectID == ExemptedSubjectID.Value);
            if (PupilID.HasValue && PupilID.Value > 0) lstExemptedSubject = lstExemptedSubject.Where(u => u.PupilID == PupilID.Value);
            if (ClassID.HasValue && ClassID.Value > 0) lstExemptedSubject = lstExemptedSubject.Where(u => u.ClassID == ClassID.Value);
            if (SchoolID.HasValue && SchoolID.Value > 0) lstExemptedSubject = lstExemptedSubject.Where(u => u.SchoolID == SchoolID.Value);
            if (AcademicYearID.HasValue && AcademicYearID.Value > 0) lstExemptedSubject = lstExemptedSubject.Where(u => u.AcademicYearID == AcademicYearID.Value);
            if (EducationLevelID.HasValue && EducationLevelID.Value > 0) lstExemptedSubject = lstExemptedSubject.Where(u => u.EducationLevelID == EducationLevelID.Value);
            if (lstClassID.Count > 0) lstExemptedSubject = lstExemptedSubject.Where(p => lstClassID.Contains(p.ClassID));
            if (lstSubjectID.Count > 0) lstExemptedSubject = lstExemptedSubject.Where(p => lstSubjectID.Contains(p.SubjectID));
            {

            }
            if (semesterID > 0)
            {
                if (semesterID == SystemParamsInFile.SEMESTER_OF_YEAR_FIRST)
                {
                    lstExemptedSubject = lstExemptedSubject.Where(c => c.FirstSemesterExemptType.HasValue);
                }
                else if (semesterID == SystemParamsInFile.SEMESTER_OF_YEAR_SECOND)
                {
                    lstExemptedSubject = lstExemptedSubject.Where(c => c.SecondSemesterExemptType.HasValue);
                }
                else if (semesterID == SystemParamsInFile.SEMESTER_OF_YEAR_ALL)
                {
                    lstExemptedSubject = lstExemptedSubject.Where(c => c.SecondSemesterExemptType.HasValue && c.FirstSemesterExemptType.HasValue);
                }
            }
            if (Year.HasValue && Year.Value > 0) lstExemptedSubject = lstExemptedSubject.Where(u => u.Year == Year.Value);
            if (ExemptedObjectID.HasValue && ExemptedObjectID.Value > 0) lstExemptedSubject = lstExemptedSubject.Where(u => u.ExemptedObjectID == ExemptedObjectID.Value);
            if (!string.IsNullOrEmpty(PupilCode)) lstExemptedSubject = lstExemptedSubject.Join(this.PupilProfileBusiness.All.Where(u => u.PupilCode.ToLower().Contains(PupilCode.ToLower())), es => es.PupilID, pp => pp.PupilProfileID, (u, v) => u);
            if (!string.IsNullOrEmpty(PupilName)) lstExemptedSubject = lstExemptedSubject.Join(this.PupilProfileBusiness.All.Where(u => u.FullName.ToLower().Contains(PupilName.ToLower())), es => es.PupilID, pp => pp.PupilProfileID, (u, v) => u);
            if (SubjectID.HasValue && SubjectID.Value > 0) lstExemptedSubject = lstExemptedSubject.Where(u => u.SubjectID == SubjectID.Value);
            if (FirstSemesterExemptType.HasValue && FirstSemesterExemptType.Value > 0) lstExemptedSubject = lstExemptedSubject.Where(u => u.FirstSemesterExemptType == FirstSemesterExemptType.Value);
            if (SecondSemesterExemptType.HasValue && SecondSemesterExemptType.Value > 0) lstExemptedSubject = lstExemptedSubject.Where(u => u.SecondSemesterExemptType == SecondSemesterExemptType.Value);
            if (!string.IsNullOrEmpty(Description)) lstExemptedSubject = lstExemptedSubject.Where(u => u.Description.ToLower().Contains(Description.ToLower()));
            if (CreatedDate.HasValue) lstExemptedSubject = lstExemptedSubject.Where(u => u.CreatedDate.Value.Date == CreatedDate.Value.Date);
            if (ModifiedDate.HasValue) lstExemptedSubject = lstExemptedSubject.Where(u => u.ModifiedDate.Value.Date == ModifiedDate.Value.Date);
            if (HasFirstSemester.HasValue) lstExemptedSubject = lstExemptedSubject.Where(u => u.FirstSemesterExemptType.HasValue == HasFirstSemester.HasValue);
            if (HasSecondSemester.HasValue) lstExemptedSubject = lstExemptedSubject.Where(u => u.SecondSemesterExemptType.HasValue == HasSecondSemester.HasValue);
            if (AppliedLevel.HasValue && AppliedLevel.Value > 0) lstExemptedSubject = lstExemptedSubject.Where(u => u.EducationLevel.Grade == AppliedLevel.Value);
            return lstExemptedSubject;
        }

        public IQueryable<ExemptedSubject> SearchBySchool(int SchoolID, IDictionary<string, object> dic)
        {
            if (SchoolID == 0) return null;

            dic = dic == null ? new Dictionary<string, object>() : dic;
            dic["SchoolID"] = SchoolID;

            return Search(dic);
        }

        public List<PupilProfile> GetPupilExemptedSubject(int schoolID, int classID)
        {
            if (schoolID <= 0) return null;

            IDictionary<string, object> dic = new Dictionary<string, object>();
            dic.Add("ClassID", classID);
            dic.Add("Status", SystemParamsInFile.PUPIL_STATUS_STUDYING);

            var lstPupil = this.PupilOfClassBusiness.SearchBySchool(schoolID, dic) //Tim kiem cac hoc sinh theo truong, lop va dang hoc
                                .Select(u => u.PupilProfile) //Lay cac hoc sinh
                                .Where(u => u.IsActive == true && u.ExemptedSubjects.Count() == 0) //Khong co trong bang ExemptedSubject
                                .ToList();

            return lstPupil;
        }

        public bool IsExemptedSubject(int PupilID, int ClassID, int AcademicYearID, int SubjectID, int Semester)
        {
            var exemptedSubject = this.ExemptedSubjectRepository.All.Any(o => o.PupilID == PupilID && o.ClassID == ClassID && o.AcademicYearID == AcademicYearID && o.SubjectID == SubjectID && ((Semester == 1 && o.FirstSemesterExemptType.HasValue && o.FirstSemesterExemptType == 2) || (Semester == 2 && o.SecondSemesterExemptType.HasValue && o.SecondSemesterExemptType == 2)));
            return exemptedSubject;
        }


        public IQueryable<ExemptedSubject> GetListExemptedSubject(int ClassID, int Semester = 0)
        {
            var query = this.ExemptedSubjectRepository.All.AsNoTracking();
            if (ClassID > 0)
            {
                query = query.Where(c => c.ClassID == ClassID);
            }
            if (Semester == SystemParamsInFile.SEMESTER_OF_YEAR_FIRST)
            {
                query = query.Where(c => c.FirstSemesterExemptType.HasValue);
            }
            if (Semester == SystemParamsInFile.SEMESTER_OF_YEAR_SECOND)
            {
                query = query.Where(c => c.SecondSemesterExemptType.HasValue);
            }
            if (Semester == SystemParamsInFile.SEMESTER_OF_YEAR_ALL)
            {
                query = query.Where(c => c.SecondSemesterExemptType.HasValue && c.FirstSemesterExemptType.HasValue);
            }
            return query;
        }

        public IQueryable<ExemptedSubject> GetListExemptedSubject(IEnumerable<int> lstClassID, int Semester)
        {
            /*var query = this.ExemptedSubjectRepository.All
                                        .Where(u => lstClassID.Contains(u.ClassID)
                                                    && ((Semester == SystemParamsInFile.SEMESTER_OF_YEAR_FIRST && u.FirstSemesterExemptType.HasValue) //&& u.FirstSemesterExemptType == SystemParamsInFile.EXEMPT_TYPE_ALL)
                                                        || (Semester == SystemParamsInFile.SEMESTER_OF_YEAR_SECOND && u.SecondSemesterExemptType.HasValue))); //&& u.SecondSemesterExemptType == SystemParamsInFile.EXEMPT_TYPE_ALL)));*/
            var query = this.ExemptedSubjectRepository.All.AsNoTracking();
            if (lstClassID != null)
            {
                query = query.Where(c => lstClassID.Contains(c.ClassID));
            }
            if (Semester == SystemParamsInFile.SEMESTER_OF_YEAR_FIRST)
            {
                query = query.Where(c => c.FirstSemesterExemptType.HasValue);
            }
            if (Semester == SystemParamsInFile.SEMESTER_OF_YEAR_SECOND)
            {
                query = query.Where(c => c.SecondSemesterExemptType.HasValue);
            }
            return query;

        }

        public IQueryable<ExemptedSubject> GetListExemptedSubjectBySchoolID(int SchoolID, int AcademicYearID, int SubjectID, int Semester = 0)
        {
            var query = this.ExemptedSubjectRepository.All
                                        .Where(u => u.SchoolID == SchoolID && u.AcademicYearID == AcademicYearID
                                            && u.SubjectID == SubjectID
                                                    && ((Semester == 0) || (Semester == SystemParamsInFile.SEMESTER_OF_YEAR_FIRST && u.FirstSemesterExemptType.HasValue)
                                                        || (Semester == SystemParamsInFile.SEMESTER_OF_YEAR_SECOND && u.SecondSemesterExemptType.HasValue)));
            return query;
        }

        public List<ExemptedSubject> GetExmpSubjectbyAcademicYear(int academicYearId, int semesterId, int subjectId = 0, int classId = 0)
        {
            var query = ExemptedSubjectBusiness.All.Where(s => s.AcademicYearID == academicYearId);
            if (subjectId > 0)
            {
                query = query.Where(s => s.SubjectID == subjectId);
            }
            if (classId > 0)
            {
                query = query.Where(s => s.ClassID == classId);
            }
            if (semesterId == GlobalConstants.SEMESTER_OF_YEAR_FIRST)
            {
                query = query.Where(s => s.FirstSemesterExemptType.HasValue);
            }
            else if (semesterId == GlobalConstants.SEMESTER_OF_YEAR_SECOND)
            {
                query = query.Where(s => s.SecondSemesterExemptType.HasValue);
            }
            else if (semesterId == GlobalConstants.SEMESTER_OF_YEAR_ALL)
            {
                query = query.Where(s => s.FirstSemesterExemptType.HasValue && s.SecondSemesterExemptType.HasValue);
            }
            return query.ToList();
        }
    }
}