/**
 * The Viettel License
 *
 * Copyright 2012 Viettel Telecom. All rights reserved.
 * VIETTEL PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */

/** 
* @author  
* @version $Revision: $
*/

using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Linq.Expressions;
using System.Web;
using log4net;

using SMAS.DAL.Repository;
using SMAS.DAL.IRepository;
using SMAS.Business.IBusiness;
using SMAS.Models.Models;
using SMAS.Business.Common;
using SMAS.Business.BusinessObject;

namespace SMAS.Business.Business
{
    public partial class DailyMenuDetailBusiness
    {

        public IQueryable<DailyMenuDetail> Search(IDictionary<string, object> dic)
        {
            int DailyMenuID = Utils.GetInt(dic, "DailyMenuID");
            string DishName = Utils.GetString(dic, "DishName");
            int DishID = Utils.GetInt(dic, "DishID");
            int MealID = Utils.GetInt(dic, "MealID");
            int SchoolID = Utils.GetInt(dic, "SchoolID");
            int AcademicYearID = Utils.GetInt(dic, "AcademicYearID");
            IQueryable<DailyMenuDetail> Query = DailyMenuDetailRepository.All;

            if (DishName.Trim().Length != 0)
            {
                Query = Query.Where(cfh => cfh.DishName.Contains(DishName));
            }
            if (DailyMenuID != 0)
            {
                Query = Query.Where(cfh => cfh.DailyMenuID == DailyMenuID);
            }
            if (DishID != 0)
            {
                Query = Query.Where(cfh => cfh.DishID == DishID);
            }
            if (MealID != 0)
            {
                Query = Query.Where(cfh => cfh.MealID == MealID);
            }
            if (SchoolID != 0)
            {
                Query = Query.Where(cfh => cfh.DailyMenu.SchoolID == SchoolID);
            }
            if (AcademicYearID != 0)
            {
                Query = Query.Where(cfh => cfh.DailyMenu.AcademicYearID == AcademicYearID);
            }

            return Query;

        }

        public IQueryable<DailyMenuDetailDishInspectionBO> SearchDish(IDictionary<string, object> dic)
        {
            int DailyMenuID = Utils.GetInt(dic, "DailyMenuID");
            string DishName = Utils.GetString(dic, "DishName");
            int DishID = Utils.GetInt(dic, "DishID");
            int MealID = Utils.GetInt(dic, "MealID");
            int SchoolID = Utils.GetInt(dic, "SchoolID");
            int AcademicYearID = Utils.GetInt(dic, "AcademicYearID");
            IQueryable<DailyMenuDetail> Query = DailyMenuDetailRepository.All;

            if (DishName.Trim().Length != 0)
            {
                Query = Query.Where(cfh => cfh.DishName.Contains(DishName));
            }
            if (DailyMenuID != 0)
            {
                Query = Query.Where(cfh => cfh.DailyMenuID == DailyMenuID);
            }
            if (DishID != 0)
            {
                Query = Query.Where(cfh => cfh.DishID == DishID);
            }
            if (MealID != 0)
            {
                Query = Query.Where(cfh => cfh.MealID == MealID);
            }
            if (SchoolID != 0)
            {
                Query = Query.Where(cfh => cfh.DailyMenu.SchoolID == SchoolID);
            }
            if (AcademicYearID != 0)
            {
                Query = Query.Where(cfh => cfh.DailyMenu.AcademicYearID == AcademicYearID);
            }

            var list = from lstdaily in Query
                       join lstdishcat in DishCatBusiness.All on lstdaily.DishID equals lstdishcat.DishCatID into g1
                       from g2 in g1.DefaultIfEmpty()
                       select new DailyMenuDetailDishInspectionBO
                       {
                           DailyMenuDetailID = lstdaily.DailyMenuDetailID,
                           DailyMenuID = lstdaily.DailyMenuID,
                           MealName = lstdaily.MealCat.MealName,
                           Dish_Name= g2.DishName,
                           DishID = lstdaily.DishID,
                           DishName = lstdaily.DishName,
                           MealCatID = lstdaily.MealID,
                           MealCat = lstdaily.MealCat
                       };

            return list;

        }


        public IQueryable<DailyMenuDetailDishInspectionBO> SearchDishInspection(int SchoolID, IDictionary<string, object> dic)
        {
            if (SchoolID <= 0 || dic == null)
                return null;
            dic["SchoolID"] = SchoolID;
            return SearchDish(dic);
        }

        public IQueryable<DailyMenuDetail> SearchBySchool(int SchoolID, IDictionary<string, object> dic)
        {
            if (SchoolID <= 0 || dic == null)
                return null;
            dic["SchoolID"] = SchoolID;
            return Search(dic);
        }
    }
}