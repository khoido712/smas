﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SMAS.Business.BusinessObject;
using SMAS.Business.Business;
using SMAS.Business.Common;
using SMAS.Models.Models;
using System.IO;
using SMAS.VTUtils.Excel.Export;
using SMAS.DAL.Repository;

namespace SMAS.Business.Business
{
    public partial class TranscriptsBySemesterBusiness
    {
        #region private member variable


        public string BD_LOPCAP23_HocKy_I = "BangDiemMonHocCap23KyI";
        public string BD_LOPCAP23_HocKy_II = "BangDiemMonHocCap23KyII";
        public string BD_LOPCAP23_HocKy_CANAM = "BangDiemMonHocCap23CN";

        #endregion

        #region Lấy mảng băm cho các tham số đầu vào của bảng điểm học sinh theo kỳ
        /// <summary>
        /// Lấy mảng băm cho các tham số đầu vào của bảng điểm học sinh theo kỳ
        /// </summary>
        /// <param name="entity"></param>
        /// <returns>tungnd</returns>
        public string GetHashKeyForTranscriptsBySemester(TranscriptOfClass entity)
        {
            IDictionary<string, object> dic = new Dictionary<string, object> {
                {"SchoolID", entity.SchoolID},
                {"AcademicYearID ", entity.AcademicYearID},
                {"EducationLevelID ", entity.EducationLevelID},
                {"ClassID", entity.ClassID},
                {"SubjectID ", entity.SubjectID},
                {"Semester", entity.Semester}
            };
            return ReportUtils.GetHashKey(dic);
        }
        #endregion


        #region Lấy bảng điểm học sinh theo kỳ được cập nhật mới nhất
        /// <summary>
        /// Lấy bảng điểm học sinh theo kỳ được cập nhật mới nhất
        /// </summary>
        /// <param name="entity"></param>
        /// <returns>tungnd</returns>
        public ProcessedReport GetTranscriptsBySemester(TranscriptOfClass entity)
        {
            string reportCode = "";
            if (entity.Semester == SystemParamsInFile.SEMESTER_OF_YEAR_FIRST)
            {
                reportCode = SystemParamsInFile.REPORT_BANG_DIEM_LOP_CAP23_HOCKYI;
            }
            if (entity.Semester == SystemParamsInFile.SEMESTER_OF_YEAR_SECOND)
            {
                reportCode = SystemParamsInFile.REPORT_BANG_DIEM_LOP_CAP23_HOCKYII;
            }
            if (entity.Semester == SystemParamsInFile.SEMESTER_OF_YEAR_ALL)
            {
                reportCode = SystemParamsInFile.REPORT_BANG_DIEM_LOP_CAP23_CANAM;
            }

            string inputParameterHashKey = GetHashKeyForTranscriptsBySemester(entity);
            return ProcessedReportBusiness.GetProcessedReport(reportCode, inputParameterHashKey);
        }
        #endregion

        #region Lưu lại thông tin bảng điểm học sinh theo kỳ
        /// <summary>
        /// Lưu lại thông tin bảng điểm học sinh theo kỳ
        /// </summary>
        /// <param name="entity"></param>
        /// <returns>tungnd</returns>
        public ProcessedReport InsertTranscriptsBySemester(TranscriptOfClass entity, Stream data)
        {
            string reportCode = "";

            if (entity.Semester == SystemParamsInFile.SEMESTER_OF_YEAR_FIRST)
            {
                reportCode = SystemParamsInFile.REPORT_BANG_DIEM_LOP_CAP23_HOCKYI;

            }
            if (entity.Semester == SystemParamsInFile.SEMESTER_OF_YEAR_SECOND)
            {
                reportCode = SystemParamsInFile.REPORT_BANG_DIEM_LOP_CAP23_HOCKYII;

            }
            if (entity.Semester == SystemParamsInFile.SEMESTER_OF_YEAR_ALL)
            {
                reportCode = SystemParamsInFile.REPORT_BANG_DIEM_LOP_CAP23_CANAM;

            }
            ReportDefinition reportDef = ReportDefinitionBusiness.GetByCode(reportCode);
            ProcessedReport pr = new ProcessedReport();
            pr.ReportCode = reportCode;
            pr.ProcessedDate = DateTime.Now;
            pr.InputParameterHashKey = GetHashKeyForTranscriptsBySemester(entity);
            pr.ReportData = ReportUtils.Compress(data);

            int partitionId = UtilsBusiness.GetPartionId(entity.SchoolID);
            //Tạo tên file HS_[SchoolLevel]_BangDiem_[Class]_[Semester]_[Subject]
            string outputNamePattern = reportDef.OutputNamePattern;
            string semester = ReportUtils.ConvertSemesterForReportName(entity.Semester);
            string schoolLevel = ReportUtils.ConvertAppliedLevelForReportName(entity.AppliedLevel);
            string Class = this.ClassProfileBusiness.Find(entity.ClassID).DisplayName;
            string Subject = "";
            if (entity.SubjectID == 0)
            {
                Subject = "";
            }
            else
            {
                Subject = this.ClassSubjectBusiness.All.Where(o => o.SubjectID == entity.SubjectID && o.Last2digitNumberSchool == partitionId).FirstOrDefault().SubjectCat.DisplayName;
            }

            outputNamePattern = outputNamePattern.Replace("[Semester]", ReportUtils.StripVNSign(semester));
            outputNamePattern = outputNamePattern.Replace("[SchoolLevel]", ReportUtils.StripVNSign(schoolLevel));
            outputNamePattern = outputNamePattern.Replace("[Class]", ReportUtils.StripVNSign(Class));
            outputNamePattern = outputNamePattern.Replace("[Subject]", ReportUtils.StripVNSign(Subject));
            pr.ReportName = ReportUtils.RemoveSpecialCharacters(outputNamePattern) + "." + reportDef.OutputFormat;

            IDictionary<string, object> dic = new Dictionary<string, object> {
                {"ClassID", entity.ClassID},
                {"SubjectID ", entity.SubjectID},
                {"Semester", entity.Semester}
            };
            ProcessedReportParameterRepository.Insert(dic, pr);
            ProcessedReportRepository.Insert(pr);
            ProcessedReportRepository.Save();
            return pr;
        }
        #endregion



        #region Tạo file báo cáo thông tin bảng điểm học sinh theo kỳ
        /// <summary>
        /// Tạo file báo cáo thông tin bảng điểm học sinh theo kỳ
        /// </summary>
        /// <param name="entity"></param>
        /// <returns>tungnd</returns>
        public Stream CreateTranscriptsBySemester(TranscriptOfClass entity)
        {
            string reportCode = "";
            string SemesterName = "";
            if (entity.Semester == SystemParamsInFile.SEMESTER_OF_YEAR_FIRST)
            {
                reportCode = BD_LOPCAP23_HocKy_I;
                SemesterName = "HỌC KỲ 1";

            }
            if (entity.Semester == SystemParamsInFile.SEMESTER_OF_YEAR_SECOND)
            {
                reportCode = BD_LOPCAP23_HocKy_II;
                SemesterName = "HỌC KỲ 2";
            }
            if (entity.Semester == SystemParamsInFile.SEMESTER_OF_YEAR_ALL)
            {
                reportCode = BD_LOPCAP23_HocKy_CANAM;
                SemesterName = "CẢ NĂM";
            }

            ReportDefinition reportDef = ReportDefinitionBusiness.GetByCode(reportCode);
            ClassProfile classProfile = this.ClassProfileBusiness.Find(entity.ClassID);
            AcademicYear academicYear = AcademicYearBusiness.Find(entity.AcademicYearID);
            string ClassName = classProfile.DisplayName.ToUpper();
            string SchoolName = classProfile.SchoolProfile.SchoolName.ToUpper();
            string AcademicYear = classProfile.AcademicYear.DisplayTitle;

            string templatePath = SystemParamsInFile.TEMPLATE_FOLDER + reportDef.TemplateDirectory + "/" + reportDef.TemplateName;
            IVTWorkbook oBook = VTExport.OpenWorkbook(templatePath);

            //Lấy sheet template
            IVTWorksheet firstSheet = oBook.GetSheet(1);
            IVTRange toprange = firstSheet.GetRange("A8", "AR8");
            IVTRange midrange = firstSheet.GetRange("A9", "AR9");
            IVTRange botrange = firstSheet.GetRange("A12", "AR12");
            IVTRange topRedrange = firstSheet.GetRange("A15", "AR15");
            IVTRange midRedrange = firstSheet.GetRange("A16", "AR16");
            IVTRange botRedrange = firstSheet.GetRange("A17", "AR17");

            //- Lấy danh sách môn học theo thứ tự (nếu entity.SubjectID = 0)
            List<ClassSubject> lstclassSubject = new List<ClassSubject>();


            Dictionary<string, object> dic = new Dictionary<string, object>();
            dic["AcademicYearID"] = entity.AcademicYearID;
            dic["AppliedLevel"] = entity.AppliedLevel;
            dic["Semester"] = entity.Semester;
            dic["EducationLevelID"] = entity.EducationLevelID;
            dic["ClassID"] = entity.ClassID;
            dic["SubjectID"] = entity.SubjectID;
            dic["IsVNEN"] = true;

            lstclassSubject = ClassSubjectBusiness.SearchBySchool(entity.SchoolID, dic)
                                            .OrderBy(o => o.SubjectCat.OrderInSubject).ThenBy(o => o.SubjectCat.DisplayName).ToList();


            //Danh sách học sinh theo thứ tự: PupilOfClassBusiness.SearchBySchool(SchoolID, Dictionary) 
            var lstPupilOfClass = this.PupilOfClassBusiness.SearchBySchool(entity.SchoolID, new Dictionary<string, object> { 
                                                                                                                      { "ClassID", entity.ClassID }
                                                                                                                    , { "Check", "Check" } 
                                                                          }).Select(o => new
                                                                          {
                                                                              o.ClassID,
                                                                              o.PupilID,
                                                                              o.PupilProfile.FullName,
                                                                              o.PupilProfile.Name,
                                                                              o.OrderInClass,
                                                                              o.Status
                                                                          })
                                                                          .OrderBy(o => o.OrderInClass).ThenBy(o => o.Name).ThenBy(o => o.FullName).ToList();
            List<int> lstPupilOfClassHKI = new List<int>();
            List<int> lstPupilOfClassHKII = new List<int>();
            if (entity.Semester == SystemParamsInFile.SEMESTER_OF_YEAR_FIRST)
            {
                lstPupilOfClassHKI = this.PupilOfClassBusiness.SearchBySchool(entity.SchoolID, new Dictionary<string, object> { 
                                                                                                                      { "ClassID", entity.ClassID }
                                                                                                                      
            }).AddCriteriaSemester(classProfile.AcademicYear, entity.Semester).Select(o => o.PupilID).ToList();
            }
            else if (entity.Semester == SystemParamsInFile.SEMESTER_OF_YEAR_SECOND)
            {
                lstPupilOfClassHKII = this.PupilOfClassBusiness.SearchBySchool(entity.SchoolID, new Dictionary<string, object> { 
                                                                                                                      { "ClassID", entity.ClassID }
                                                                                                                      
            }).AddCriteriaSemester(classProfile.AcademicYear, entity.Semester).Select(o => o.PupilID).ToList();
            }
            else
            {
                lstPupilOfClassHKI = this.PupilOfClassBusiness.SearchBySchool(entity.SchoolID, new Dictionary<string, object> { 
                                                                                                                      { "ClassID", entity.ClassID }
                                                                                                                      
            }).AddCriteriaSemester(classProfile.AcademicYear, SystemParamsInFile.SEMESTER_OF_YEAR_FIRST).Select(o => o.PupilID).ToList();
                lstPupilOfClassHKII = this.PupilOfClassBusiness.SearchBySchool(entity.SchoolID, new Dictionary<string, object> { 
                                                                                                                      { "ClassID", entity.ClassID }
                                                                                                                      
            }).AddCriteriaSemester(classProfile.AcademicYear, SystemParamsInFile.SEMESTER_OF_YEAR_SECOND).Select(o => o.PupilID).ToList();
            }


            if ((entity.EducationLevelID >= 6 && entity.EducationLevelID <= 12) && (academicYear.IsShowPupil.HasValue && academicYear.IsShowPupil.Value == true))
            {
                lstPupilOfClass = lstPupilOfClass.Where(x => x.Status == SystemParamsInFile.PUPIL_STATUS_GRADUATED
                                            || x.Status == SystemParamsInFile.PUPIL_STATUS_STUDYING).ToList();             
            }

            Dictionary<string, object> Dic = new Dictionary<string, object> { { "ClassID", entity.ClassID } };
            Dic["AcademicYearID"] = entity.AcademicYearID;
            if (entity.Semester == SystemParamsInFile.SEMESTER_OF_YEAR_FIRST)
            {
                Dic["Semester"] = entity.Semester;
            }
            if (entity.SubjectID != 0)
            {
                Dic["SubjectID"] = entity.SubjectID;
            }
            if (academicYear != null)
            {
                Dic["Year"] = academicYear.Year;
            }
            //+ Danh sách điểm môn tính điểm: MarkRecordBusiness.SearchBySchool(SchoolID, Dictionary)
            List<VMarkRecord> lstMarkRecord = this.VMarkRecordBusiness.SearchBySchool(entity.SchoolID, Dic).ToList();

            //+ Danh sách điểm môn nhận xét: JudgeRecordBusiness.SearchBySchool(SchoolID, Dictionary)
            IDictionary<string, object> dicJudge = new Dictionary<string, object>();
            dicJudge["ClassID"] = entity.ClassID;
            dicJudge["AcademicYearID"] = entity.AcademicYearID;
            if (entity.Semester == SystemParamsInFile.SEMESTER_OF_YEAR_FIRST)
            {
                dicJudge["Semester"] = entity.Semester;
            }
            if (academicYear != null)
            {
                dicJudge["Year"] = academicYear.Year;
            }
            if (entity.SubjectID != 0)
            {
                dicJudge["SubjectID"] = entity.SubjectID;
            }
            dicJudge["checkWithClassMovement"] = "checkWithClassMovement";
            List<VJudgeRecord> lstJudgeRecord = this.VJudgeRecordBusiness.SearchBySchool(entity.SchoolID, dicJudge).ToList();

            //+ Danh sách điểm trung bình môn: SummedUpRecordBusiness.SearchBySchool(SchoolID, Dictionary)
            List<VSummedUpRecord> lstSummedUpRecord;
            lstSummedUpRecord = this.VSummedUpRecordBusiness.SearchBySchool(entity.SchoolID, Dic).ToList();


            string sheetName = "";
            //Với mỗi môn học (SubjectID) tương ứng một sheet 
            for (int k = 0; k < lstclassSubject.Count; k++)
            {
                ClassSubject classSubject = lstclassSubject[k];
                SubjectCat subjectCat = lstclassSubject[k].SubjectCat;

                //Tạo sheet
                IVTWorksheet sheet = oBook.CopySheetToLast(firstSheet, "AR7");
                //Fill dữ liệu chung
                sheetName = Utils.StripVNSignAndSpace(subjectCat.DisplayName);
                sheet.Name = sheetName;
                sheet.SetCellValue("A2", SchoolName);
                sheet.SetCellValue("A3", "BẢNG ĐIỂM MÔN " + subjectCat.DisplayName.ToUpper() + " " + "LỚP " + ClassName + " " + SemesterName);
                sheet.SetCellValue("A4", "NĂM HỌC " + AcademicYear);

                int startRow = 8;
                List<VMarkRecord> lstMarkRecordTempM = new List<VMarkRecord>();
                List<VMarkRecord> lstMarkRecordTempP = new List<VMarkRecord>();
                List<VMarkRecord> lstMarkRecordTempV = new List<VMarkRecord>();
                List<VJudgeRecord> lstJudgeRecordTempM = new List<VJudgeRecord>();
                List<VJudgeRecord> lstJudgeRecordTempP = new List<VJudgeRecord>();
                List<VJudgeRecord> lstJudgeRecordTempV = new List<VJudgeRecord>();

                VMarkRecord mr = new VMarkRecord();
                VJudgeRecord jr = new VJudgeRecord();
                VSummedUpRecord sur = new VSummedUpRecord();

                int PupilID = 0, SubjectID = 0;
                for (int i = 0; i < lstPupilOfClass.Count; i++)
                {
                    bool showDataHKI = lstPupilOfClassHKI.Any(u => u == lstPupilOfClass[i].PupilID);
                    bool showDataHKII = lstPupilOfClassHKII.Any(u => u == lstPupilOfClass[i].PupilID);
                    bool showData;
                    if (entity.Semester == SystemParamsInFile.SEMESTER_OF_YEAR_FIRST)
                    {
                        showData = showDataHKI;
                    }
                    else
                    {
                        showData = showDataHKII;
                    }
                    bool status = (lstPupilOfClass[i].Status == SystemParamsInFile.PUPIL_STATUS_STUDYING || lstPupilOfClass[i].Status == SystemParamsInFile.PUPIL_STATUS_GRADUATED) ? true : false;
                    //Copy row style
                    if (i == lstPupilOfClass.Count - 1 || i % 5 == 4)
                    {
                        sheet.CopyPasteSameRowHeigh((showData && status) ? botrange : botRedrange, startRow);
                    }
                    else if (i % 5 == 0)
                    {
                        sheet.CopyPasteSameRowHeigh((showData && status) ? toprange : topRedrange, startRow);
                    }
                    else
                    {
                        sheet.CopyPasteSameRowHeigh((showData && status) ? midrange : midRedrange, startRow);
                    }

                    //STT
                    sheet.SetCellValue(startRow, 1, i + 1);

                    //Ma HS
                    //sheet.SetCellValue(startRow, 2, lstPupilOfClass[i].PupilProfile.PupilCode);
                    //Ho ten
                    sheet.SetCellValue(startRow, 2, lstPupilOfClass[i].FullName);

                    PupilID = lstPupilOfClass[i].PupilID;
                    SubjectID = lstclassSubject[k].SubjectID;

                    if (entity.Semester != SystemParamsInFile.SEMESTER_OF_YEAR_ALL)
                    {
                        #region HK1

                        if (entity.Semester == SystemParamsInFile.SEMESTER_OF_YEAR_FIRST)
                        {
                            if (showDataHKI)
                            {
                                //Diem mieng
                                if (classSubject.IsCommenting == SystemParamsInFile.ISCOMMENTING_TYPE_JUDGE) //Mon nhan xet
                                {
                                    lstJudgeRecordTempM = lstJudgeRecord.Where(o => (o.Title.ToUpper().Contains(SystemParamsInFile.MARK_TYPE_M) && o.PupilID == PupilID && o.SubjectID == SubjectID)).OrderBy(o => o.Title).ToList();
                                    for (int j = 0; j < lstJudgeRecordTempM.Count(); j++)
                                    {
                                        if (lstJudgeRecordTempM[j].OrderNumber < 6)
                                        {
                                            sheet.SetCellValue(startRow, lstJudgeRecordTempM[j].OrderNumber + 2, lstJudgeRecordTempM[j].Judgement);
                                        }
                                    }
                                }
                                else //Mon tinh diem
                                {
                                    lstMarkRecordTempM = lstMarkRecord.Where(o => o.Title.ToUpper().Contains(SystemParamsInFile.MARK_TYPE_M) && o.PupilID == PupilID && o.SubjectID == SubjectID).OrderBy(o => o.Title).ToList();
                                    for (int j = 0; j < lstMarkRecordTempM.Count; j++)
                                    {
                                        if (lstMarkRecordTempM[j].OrderNumber < 6)
                                        {
                                            sheet.SetCellValue(startRow, lstMarkRecordTempM[j].OrderNumber + 2, lstMarkRecordTempM[j].Mark);
                                        }
                                    }
                                }

                                //Diem 15p
                                if (lstclassSubject[k].IsCommenting != SystemParamsInFile.ISCOMMENTING_TYPE_JUDGE) //Mon tinh diem
                                {
                                    lstMarkRecordTempP = lstMarkRecord.Where(o => (o.Title.ToUpper().Contains(SystemParamsInFile.MARK_TYPE_P) && o.PupilID == PupilID && o.SubjectID == SubjectID)).OrderBy(o => o.Title).ToList();
                                    for (int j = 0; j < lstMarkRecordTempP.Count(); j++)
                                    {
                                        if (lstMarkRecordTempP[j].OrderNumber < 6)
                                        {
                                            string summedUpMark = ReportUtils.ConvertMarkForV(lstMarkRecordTempP[j].Mark);
                                            sheet.SetCellValue(startRow, lstMarkRecordTempP[j].OrderNumber + 7, summedUpMark);
                                        }
                                    }
                                }
                                else //Mon nhan xet
                                {
                                    lstJudgeRecordTempP = lstJudgeRecord.Where(o => (o.Title.ToUpper().Contains(SystemParamsInFile.MARK_TYPE_P) && o.PupilID == PupilID && o.SubjectID == SubjectID)).OrderBy(o => o.Title).ToList();
                                    for (int j = 0; j < lstJudgeRecordTempP.Count(); j++)
                                    {
                                        if (lstJudgeRecordTempP[j].OrderNumber < 6)
                                        {
                                            string judgementResult = lstJudgeRecordTempP[j].Judgement;
                                            sheet.SetCellValue(startRow, lstJudgeRecordTempP[j].OrderNumber + 7, judgementResult);
                                        }
                                    }
                                }
                                //Diem viet
                                if (lstclassSubject[k].IsCommenting != SystemParamsInFile.ISCOMMENTING_TYPE_JUDGE) //Mon tinh diem
                                {
                                    lstMarkRecordTempV = lstMarkRecord.Where(o => o.Title.ToUpper().Contains(SystemParamsInFile.MARK_TYPE_V) && o.PupilID == PupilID && o.SubjectID == SubjectID).OrderBy(o => o.Title).ToList();
                                    for (int j = 0; j < lstMarkRecordTempV.Count(); j++)
                                    {
                                        if (lstMarkRecordTempV[j].OrderNumber < 9)
                                        {
                                            string summedUpMark = ReportUtils.ConvertMarkForV(lstMarkRecordTempV[j].Mark);
                                            sheet.SetCellValue(startRow, lstMarkRecordTempV[j].OrderNumber + 12, summedUpMark);
                                        }
                                    }
                                }
                                else
                                {
                                    lstJudgeRecordTempV = lstJudgeRecord.Where(o => (o.Title.ToUpper().Contains(SystemParamsInFile.MARK_TYPE_V) && o.PupilID == PupilID && o.SubjectID == SubjectID)).OrderBy(o => o.Title).ToList();
                                    for (int j = 0; j < lstJudgeRecordTempV.Count(); j++)
                                    {
                                        if (lstJudgeRecordTempV[j].OrderNumber < 9)
                                        {
                                            string judgementResult = lstJudgeRecordTempV[j].Judgement;
                                            sheet.SetCellValue(startRow, lstJudgeRecordTempV[j].OrderNumber + 12, judgementResult);
                                        }
                                    }
                                }
                                //HK
                                if (lstclassSubject[k].IsCommenting != SystemParamsInFile.ISCOMMENTING_TYPE_JUDGE) //Mon tinh diem
                                {
                                    mr = lstMarkRecord.Where(o => (o.Title == SystemParamsInFile.MARK_TYPE_HK && o.PupilID == PupilID && o.SubjectID == SubjectID)).FirstOrDefault();
                                    if (mr != null)
                                    {
                                        string summedUpMark = ReportUtils.ConvertMarkForV(mr.Mark);
                                        sheet.SetCellValue(startRow, 21, summedUpMark);
                                    }
                                }
                                else
                                {
                                    jr = lstJudgeRecord.Where(o => (o.Title == SystemParamsInFile.MARK_TYPE_HK && o.PupilID == PupilID && o.SubjectID == SubjectID)).FirstOrDefault();
                                    if (jr != null)
                                    {
                                        sheet.SetCellValue(startRow, 21, jr.Judgement);
                                    }
                                }
                                //TBM

                                sur = lstSummedUpRecord.Where(o => o.PupilID == PupilID && o.SubjectID == SubjectID).FirstOrDefault();
                                if (sur != null)
                                {
                                    if (lstclassSubject[k].IsCommenting != SystemParamsInFile.ISCOMMENTING_TYPE_JUDGE) //Mon tinh diem
                                    {
                                        string summedUpMark = "";
                                        if (sur.SummedUpMark != null)
                                        {
                                            summedUpMark = ReportUtils.ConvertMarkForV(sur.SummedUpMark.Value);
                                        }
                                        sheet.SetCellValue(startRow, 22, summedUpMark);
                                    }
                                    else
                                    {
                                        sheet.SetCellValue(startRow, 22, sur.JudgementResult);
                                    }
                                }
                            }
                        }
                        #endregion

                        #region HK II
                        if (entity.Semester == SystemParamsInFile.SEMESTER_OF_YEAR_SECOND)
                        {
                            if (showDataHKII)
                            {
                                //TBM HK I
                                sur = lstSummedUpRecord.Where(o => o.PupilID == PupilID && o.SubjectID == SubjectID && o.Semester == SystemParamsInFile.SEMESTER_OF_YEAR_FIRST).FirstOrDefault();
                                if (sur != null)
                                {
                                    if (lstclassSubject[k].IsCommenting != SystemParamsInFile.ISCOMMENTING_TYPE_JUDGE) //Mon tinh diem
                                    {

                                        string summedUpMark = "";
                                        if (sur.SummedUpMark != null)
                                        {
                                            summedUpMark = ReportUtils.ConvertMarkForV(sur.SummedUpMark.Value);
                                        }
                                        sheet.SetCellValue(startRow, 3, summedUpMark);
                                    }
                                    else
                                    {
                                        sheet.SetCellValue(startRow, 3, sur.JudgementResult);
                                    }
                                }
                                //Diem mieng
                                if (lstclassSubject[k].IsCommenting != SystemParamsInFile.ISCOMMENTING_TYPE_JUDGE) //Mon tinh diem
                                {
                                    lstMarkRecordTempM = lstMarkRecord.Where(o => (o.Title.ToUpper().Contains(SystemParamsInFile.MARK_TYPE_M) && o.PupilID == PupilID && o.SubjectID == SubjectID && o.Semester == SystemParamsInFile.SEMESTER_OF_YEAR_SECOND)).OrderBy(o => o.Title).ToList();
                                    for (int j = 0; j < lstMarkRecordTempM.Count(); j++)
                                    {
                                        if (lstMarkRecordTempM[j].OrderNumber < 6)
                                        {
                                            sheet.SetCellValue(startRow, lstMarkRecordTempM[j].OrderNumber + 3, lstMarkRecordTempM[j].Mark);
                                        }
                                    }
                                }
                                else //Mon nhan xet
                                {
                                    lstJudgeRecordTempM = lstJudgeRecord.Where(o => (o.Title.ToUpper().Contains(SystemParamsInFile.MARK_TYPE_M) && o.PupilID == PupilID && o.SubjectID == SubjectID && o.Semester == SystemParamsInFile.SEMESTER_OF_YEAR_SECOND)).OrderBy(o => o.Title).ToList();
                                    for (int j = 0; j < lstJudgeRecordTempM.Count(); j++)
                                    {
                                        if (lstJudgeRecordTempM[j].OrderNumber < 6)
                                        {
                                            sheet.SetCellValue(startRow, lstJudgeRecordTempM[j].OrderNumber + 3, lstJudgeRecordTempM[j].Judgement);
                                        }
                                    }
                                }
                                //Diem 15p
                                if (lstclassSubject[k].IsCommenting != SystemParamsInFile.ISCOMMENTING_TYPE_JUDGE) //Mon tinh diem
                                {
                                    lstMarkRecordTempP = lstMarkRecord.Where(o => (o.Title.ToUpper().Contains(SystemParamsInFile.MARK_TYPE_P) && o.PupilID == PupilID && o.SubjectID == SubjectID && o.Semester == SystemParamsInFile.SEMESTER_OF_YEAR_SECOND)).OrderBy(o => o.Title).ToList();
                                    for (int j = 0; j < lstMarkRecordTempP.Count(); j++)
                                    {
                                        if (lstMarkRecordTempP[j].OrderNumber < 6)
                                        {
                                            string summedUpMark = ReportUtils.ConvertMarkForP(lstMarkRecordTempP[j].Mark);
                                            sheet.SetCellValue(startRow, lstMarkRecordTempP[j].OrderNumber + 8, summedUpMark);
                                        }
                                    }
                                }
                                else //Mon nhan xet
                                {
                                    lstJudgeRecordTempP = lstJudgeRecord.Where(o => (o.Title.ToUpper().Contains(SystemParamsInFile.MARK_TYPE_P) && o.PupilID == PupilID && o.SubjectID == SubjectID && o.Semester == SystemParamsInFile.SEMESTER_OF_YEAR_SECOND)).OrderBy(o => o.Title).ToList();
                                    for (int j = 0; j < lstJudgeRecordTempP.Count(); j++)
                                    {
                                        if (lstJudgeRecordTempP[j].OrderNumber < 6)
                                        {
                                            sheet.SetCellValue(startRow, lstJudgeRecordTempP[j].OrderNumber + 8, lstJudgeRecordTempP[j].Judgement);
                                        }
                                    }
                                }
                                //Diem viet
                                if (lstclassSubject[k].IsCommenting != SystemParamsInFile.ISCOMMENTING_TYPE_JUDGE) //Mon tinh diem
                                {
                                    lstMarkRecordTempV = lstMarkRecord.Where(o => (o.Title.ToUpper().Contains(SystemParamsInFile.MARK_TYPE_V) && o.PupilID == PupilID && o.SubjectID == SubjectID && o.Semester == SystemParamsInFile.SEMESTER_OF_YEAR_SECOND)).OrderBy(o => o.Title).ToList();
                                    for (int j = 0; j < lstMarkRecordTempV.Count(); j++)
                                    {
                                        if (lstMarkRecordTempV[j].OrderNumber < 9)
                                        {
                                            string summedUpMark = ReportUtils.ConvertMarkForV(lstMarkRecordTempV[j].Mark);
                                            sheet.SetCellValue(startRow, lstMarkRecordTempV[j].OrderNumber + 13, summedUpMark);
                                        }
                                    }
                                }
                                else
                                {
                                    lstJudgeRecordTempV = lstJudgeRecord.Where(o => (o.Title.ToUpper().Contains(SystemParamsInFile.MARK_TYPE_V) && o.PupilID == PupilID && o.SubjectID == SubjectID && o.Semester == SystemParamsInFile.SEMESTER_OF_YEAR_SECOND)).OrderBy(o => o.Title).ToList();
                                    for (int j = 0; j < lstJudgeRecordTempV.Count(); j++)
                                    {
                                        if (lstJudgeRecordTempV[j].OrderNumber < 9)
                                        {
                                            sheet.SetCellValue(startRow, lstJudgeRecordTempV[j].OrderNumber + 13, lstJudgeRecordTempV[j].Judgement);
                                        }
                                    }
                                }
                                //HK
                                if (lstclassSubject[k].IsCommenting != SystemParamsInFile.ISCOMMENTING_TYPE_JUDGE) //Mon tinh diem
                                {
                                    mr = lstMarkRecord.Where(o => (o.Title == SystemParamsInFile.MARK_TYPE_HK && o.PupilID == PupilID && o.SubjectID == SubjectID && o.Semester == SystemParamsInFile.SEMESTER_OF_YEAR_SECOND)).FirstOrDefault();
                                    if (mr != null)
                                    {
                                        string summedUpMark = ReportUtils.ConvertMarkForV(mr.Mark);
                                        sheet.SetCellValue(startRow, 22, summedUpMark);
                                    }
                                }
                                else
                                {
                                    jr = lstJudgeRecord.Where(o => (o.Title == SystemParamsInFile.MARK_TYPE_HK && o.PupilID == PupilID && o.SubjectID == SubjectID && o.Semester == SystemParamsInFile.SEMESTER_OF_YEAR_SECOND)).FirstOrDefault();
                                    if (jr != null)
                                    {
                                        sheet.SetCellValue(startRow, 22, jr.Judgement);
                                    }
                                }
                                //TBM

                                sur = lstSummedUpRecord.Where(o => o.PupilID == PupilID && o.SubjectID == SubjectID && o.Semester == SystemParamsInFile.SEMESTER_OF_YEAR_SECOND).FirstOrDefault();
                                if (sur != null)
                                {
                                    if (lstclassSubject[k].IsCommenting != SystemParamsInFile.ISCOMMENTING_TYPE_JUDGE) //Mon tinh diem
                                    {
                                        string summedUpMark = "";
                                        if (sur.SummedUpMark != null)
                                        {
                                            summedUpMark = ReportUtils.ConvertMarkForV(sur.SummedUpMark.Value);
                                        }
                                        sheet.SetCellValue(startRow, 23, summedUpMark);
                                    }
                                    else
                                    {
                                        sheet.SetCellValue(startRow, 23, sur.JudgementResult);
                                    }
                                }
                                sur = lstSummedUpRecord.Where(o => o.PupilID == PupilID && o.SubjectID == SubjectID && o.Semester >= SystemParamsInFile.SEMESTER_OF_YEAR_ALL).OrderByDescending(o => o.Semester).FirstOrDefault();

                                if (sur != null)
                                {

                                    if (lstclassSubject[k].IsCommenting != SystemParamsInFile.ISCOMMENTING_TYPE_JUDGE) //Mon tinh diem
                                    {
                                        decimal? summdUpMark;
                                        if (sur.ReTestMark != null)
                                        {
                                            summdUpMark = sur.ReTestMark;
                                        }
                                        else
                                        {
                                            summdUpMark = sur.SummedUpMark;
                                        }
                                        string summedUpMark_E = "";
                                        if (summdUpMark.HasValue)
                                        {
                                            summedUpMark_E = ReportUtils.ConvertMarkForV(summdUpMark.Value);
                                        }
                                        sheet.SetCellValue(startRow, 24, summedUpMark_E);
                                    }
                                    else
                                    {
                                        string judgementResult;
                                        if (sur.ReTestJudgement != null && sur.ReTestJudgement != " ")
                                        {
                                            judgementResult = sur.ReTestJudgement;
                                        }
                                        else
                                        {
                                            judgementResult = sur.JudgementResult;
                                        }
                                        sheet.SetCellValue(startRow, 24, judgementResult);
                                    }
                                }
                            }



                        }

                        #endregion

                    }
                    else
                    {
                        #region Hoc ky 1
                        if (showDataHKI)
                        {
                            //Diem mieng
                            if (lstclassSubject[k].IsCommenting != SystemParamsInFile.ISCOMMENTING_TYPE_JUDGE) //Mon tinh diem
                            {
                                lstMarkRecordTempM = lstMarkRecord.Where(o => (o.Title.ToUpper().Contains(SystemParamsInFile.MARK_TYPE_M) && o.PupilID == PupilID && o.SubjectID == SubjectID && o.Semester == SystemParamsInFile.SEMESTER_OF_YEAR_FIRST)).OrderBy(o => o.Title).ToList();
                                for (int j = 0; j < lstMarkRecordTempM.Count(); j++)
                                {
                                    if (lstMarkRecordTempM[j].OrderNumber < 6)
                                    {
                                        sheet.SetCellValue(startRow, lstMarkRecordTempM[j].OrderNumber + 2, lstMarkRecordTempM[j].Mark);
                                    }
                                }
                            }
                            else //Mon nhan xet
                            {
                                lstJudgeRecordTempM = lstJudgeRecord.Where(o => (o.Title.ToUpper().Contains(SystemParamsInFile.MARK_TYPE_M) && o.PupilID == PupilID && o.SubjectID == SubjectID && o.Semester == SystemParamsInFile.SEMESTER_OF_YEAR_FIRST)).OrderBy(o => o.Title).ToList();
                                for (int j = 0; j < lstJudgeRecordTempM.Count(); j++)
                                {
                                    if (lstJudgeRecordTempM[j].OrderNumber < 6)
                                    {
                                        sheet.SetCellValue(startRow, lstJudgeRecordTempM[j].OrderNumber + 2, lstJudgeRecordTempM[j].Judgement);
                                    }
                                }
                            }
                            //Diem 15p
                            if (lstclassSubject[k].IsCommenting != SystemParamsInFile.ISCOMMENTING_TYPE_JUDGE) //Mon tinh diem
                            {
                                lstMarkRecordTempP = lstMarkRecord.Where(o => (o.Title.ToUpper().Contains(SystemParamsInFile.MARK_TYPE_P) && o.PupilID == PupilID && o.SubjectID == SubjectID && o.Semester == SystemParamsInFile.SEMESTER_OF_YEAR_FIRST)).OrderBy(o => o.Title).ToList();
                                for (int j = 0; j < lstMarkRecordTempP.Count(); j++)
                                {
                                    if (lstMarkRecordTempP[j].OrderNumber < 6)
                                    {
                                        string summedUpMark = ReportUtils.ConvertMarkForP(lstMarkRecordTempP[j].Mark);
                                        sheet.SetCellValue(startRow, lstMarkRecordTempP[j].OrderNumber + 7, summedUpMark);
                                    }
                                }
                            }
                            else //Mon nhan xet
                            {
                                lstJudgeRecordTempP = lstJudgeRecord.Where(o => (o.Title.ToUpper().Contains(SystemParamsInFile.MARK_TYPE_P) && o.PupilID == PupilID && o.SubjectID == SubjectID && o.Semester == SystemParamsInFile.SEMESTER_OF_YEAR_FIRST)).OrderBy(o => o.Title).ToList();
                                for (int j = 0; j < lstJudgeRecordTempP.Count(); j++)
                                {
                                    if (lstJudgeRecordTempP[j].OrderNumber < 6)
                                    {
                                        sheet.SetCellValue(startRow, lstJudgeRecordTempP[j].OrderNumber + 7, lstJudgeRecordTempP[j].Judgement);
                                    }
                                }
                            }
                            //Diem viet
                            if (lstclassSubject[k].IsCommenting != SystemParamsInFile.ISCOMMENTING_TYPE_JUDGE) //Mon tinh diem
                            {
                                lstMarkRecordTempV = lstMarkRecord.Where(o => (o.Title.ToUpper().Contains(SystemParamsInFile.MARK_TYPE_V) && o.PupilID == PupilID && o.SubjectID == SubjectID && o.Semester == SystemParamsInFile.SEMESTER_OF_YEAR_FIRST)).OrderBy(o => o.Title).ToList();
                                for (int j = 0; j < lstMarkRecordTempV.Count(); j++)
                                {
                                    if (lstMarkRecordTempV[j].OrderNumber < 9)
                                    {
                                        string summedUpMark = ReportUtils.ConvertMarkForV(lstMarkRecordTempV[j].Mark);
                                        sheet.SetCellValue(startRow, lstMarkRecordTempV[j].OrderNumber + 12, summedUpMark);
                                    }
                                }
                            }
                            else
                            {
                                lstJudgeRecordTempV = lstJudgeRecord.Where(o => (o.Title.ToUpper().Contains(SystemParamsInFile.MARK_TYPE_V) && o.PupilID == PupilID && o.SubjectID == SubjectID && o.Semester == SystemParamsInFile.SEMESTER_OF_YEAR_FIRST)).OrderBy(o => o.Title).ToList();
                                for (int j = 0; j < lstJudgeRecordTempV.Count(); j++)
                                {
                                    if (lstJudgeRecordTempV[j].OrderNumber < 9)
                                    {
                                        sheet.SetCellValue(startRow, lstJudgeRecordTempV[j].OrderNumber + 12, lstJudgeRecordTempV[j].Judgement);
                                    }
                                }
                            }
                            //HK
                            if (lstclassSubject[k].IsCommenting != SystemParamsInFile.ISCOMMENTING_TYPE_JUDGE) //Mon tinh diem
                            {
                                mr = lstMarkRecord.Where(o => (o.Title == SystemParamsInFile.MARK_TYPE_HK && o.PupilID == PupilID && o.SubjectID == SubjectID && o.Semester == SystemParamsInFile.SEMESTER_OF_YEAR_FIRST)).FirstOrDefault();
                                if (mr != null)
                                {
                                    string summedUpMark = ReportUtils.ConvertMarkForV(mr.Mark);
                                    sheet.SetCellValue(startRow, 21, summedUpMark);
                                }
                            }
                            else
                            {
                                jr = lstJudgeRecord.Where(o => (o.Title == SystemParamsInFile.MARK_TYPE_HK && o.PupilID == PupilID && o.SubjectID == SubjectID && o.Semester == SystemParamsInFile.SEMESTER_OF_YEAR_FIRST)).FirstOrDefault();
                                if (jr != null)
                                {
                                    sheet.SetCellValue(startRow, 21, jr.Judgement);
                                }
                            }
                            //TBM

                            sur = lstSummedUpRecord.Where(o => o.PupilID == PupilID && o.SubjectID == SubjectID && o.Semester == SystemParamsInFile.SEMESTER_OF_YEAR_FIRST).FirstOrDefault();
                            if (sur != null)
                            {
                                if (lstclassSubject[k].IsCommenting != SystemParamsInFile.ISCOMMENTING_TYPE_JUDGE) //Mon tinh diem
                                {

                                    string summedUpMark = "";
                                    if (sur.SummedUpMark != null)
                                    {
                                        summedUpMark = ReportUtils.ConvertMarkForV(sur.SummedUpMark.Value);
                                    }
                                    sheet.SetCellValue(startRow, 22, summedUpMark);
                                }
                                else
                                {
                                    sheet.SetCellValue(startRow, 22, sur.JudgementResult);
                                }
                            }
                        }
                        #endregion

                        #region Hoc ky 2
                        if (showDataHKII)
                        {
                            //Diem mieng
                            if (lstclassSubject[k].IsCommenting != SystemParamsInFile.ISCOMMENTING_TYPE_JUDGE) //Mon tinh diem
                            {
                                lstMarkRecordTempM = lstMarkRecord.Where(o => (o.Title.ToUpper().Contains(SystemParamsInFile.MARK_TYPE_M) && o.PupilID == PupilID && o.SubjectID == SubjectID && o.Semester == SystemParamsInFile.SEMESTER_OF_YEAR_SECOND)).OrderBy(o => o.Title).ToList();
                                for (int j = 0; j < lstMarkRecordTempM.Count(); j++)
                                {
                                    if (lstMarkRecordTempM[j].OrderNumber < 6)
                                    {
                                        sheet.SetCellValue(startRow, lstMarkRecordTempM[j].OrderNumber + 22, lstMarkRecordTempM[j].Mark);
                                    }
                                }
                            }
                            else //Mon nhan xet
                            {
                                lstJudgeRecordTempM = lstJudgeRecord.Where(o => (o.Title.ToUpper().Contains(SystemParamsInFile.MARK_TYPE_M) && o.PupilID == PupilID && o.SubjectID == SubjectID && o.Semester == SystemParamsInFile.SEMESTER_OF_YEAR_SECOND)).OrderBy(o => o.Title).ToList();
                                for (int j = 0; j < lstJudgeRecordTempM.Count(); j++)
                                {
                                    if (lstJudgeRecordTempM[j].OrderNumber < 6)
                                    {
                                        sheet.SetCellValue(startRow, lstJudgeRecordTempM[j].OrderNumber + 22, lstJudgeRecordTempM[j].Judgement);
                                    }
                                }
                            }
                            //Diem 15p
                            if (lstclassSubject[k].IsCommenting != SystemParamsInFile.ISCOMMENTING_TYPE_JUDGE) //Mon tinh diem
                            {
                                lstMarkRecordTempP = lstMarkRecord.Where(o => (o.Title.ToUpper().Contains(SystemParamsInFile.MARK_TYPE_P) && o.PupilID == PupilID && o.SubjectID == SubjectID && o.Semester == SystemParamsInFile.SEMESTER_OF_YEAR_SECOND)).OrderBy(o => o.Title).ToList();
                                for (int j = 0; j < lstMarkRecordTempP.Count(); j++)
                                {
                                    if (lstMarkRecordTempP[j].OrderNumber < 6)
                                    {
                                        string summedUpMark = ReportUtils.ConvertMarkForP(lstMarkRecordTempP[j].Mark);
                                        sheet.SetCellValue(startRow, lstMarkRecordTempP[j].OrderNumber + 27, summedUpMark);
                                    }
                                }
                            }
                            else //Mon nhan xet
                            {
                                lstJudgeRecordTempP = lstJudgeRecord.Where(o => (o.Title.ToUpper().Contains(SystemParamsInFile.MARK_TYPE_P) && o.PupilID == PupilID && o.SubjectID == SubjectID && o.Semester == SystemParamsInFile.SEMESTER_OF_YEAR_SECOND)).OrderBy(o => o.Title).ToList();
                                for (int j = 0; j < lstJudgeRecordTempP.Count(); j++)
                                {
                                    if (lstJudgeRecordTempP[j].OrderNumber < 6)
                                    {
                                        sheet.SetCellValue(startRow, lstJudgeRecordTempP[j].OrderNumber + 27, lstJudgeRecordTempP[j].Judgement);
                                    }
                                }
                            }
                            //Diem viet
                            if (lstclassSubject[k].IsCommenting != SystemParamsInFile.ISCOMMENTING_TYPE_JUDGE) //Mon tinh diem
                            {
                                lstMarkRecordTempV = lstMarkRecord.Where(o => (o.Title.ToUpper().Contains(SystemParamsInFile.MARK_TYPE_V) && o.PupilID == PupilID && o.SubjectID == SubjectID && o.Semester == SystemParamsInFile.SEMESTER_OF_YEAR_SECOND)).OrderBy(o => o.Title).ToList();
                                for (int j = 0; j < lstMarkRecordTempV.Count(); j++)
                                {
                                    if (lstMarkRecordTempV[j].OrderNumber < 9)
                                    {
                                        string summedUpMark = ReportUtils.ConvertMarkForV(lstMarkRecordTempV[j].Mark);
                                        sheet.SetCellValue(startRow, lstMarkRecordTempV[j].OrderNumber + 32, summedUpMark);
                                    }
                                }
                            }
                            else
                            {
                                lstJudgeRecordTempV = lstJudgeRecord.Where(o => (o.Title.ToUpper().Contains(SystemParamsInFile.MARK_TYPE_V) && o.PupilID == PupilID && o.SubjectID == SubjectID && o.Semester == SystemParamsInFile.SEMESTER_OF_YEAR_SECOND)).OrderBy(o => o.Title).ToList();
                                for (int j = 0; j < lstJudgeRecordTempV.Count(); j++)
                                {
                                    if (lstJudgeRecordTempV[j].OrderNumber < 9)
                                    {
                                        sheet.SetCellValue(startRow, lstJudgeRecordTempV[j].OrderNumber + 32, lstJudgeRecordTempV[j].Judgement);
                                    }
                                }
                            }
                            //HK
                            if (lstclassSubject[k].IsCommenting != SystemParamsInFile.ISCOMMENTING_TYPE_JUDGE) //Mon tinh diem
                            {
                                mr = lstMarkRecord.Where(o => (o.Title == SystemParamsInFile.MARK_TYPE_HK && o.PupilID == PupilID && o.SubjectID == SubjectID && o.Semester == SystemParamsInFile.SEMESTER_OF_YEAR_SECOND)).FirstOrDefault();
                                if (mr != null)
                                {
                                    string summedUpMark = ReportUtils.ConvertMarkForV(mr.Mark);
                                    sheet.SetCellValue(startRow, 41, summedUpMark);
                                }
                            }
                            else
                            {
                                jr = lstJudgeRecord.Where(o => (o.Title == SystemParamsInFile.MARK_TYPE_HK && o.PupilID == PupilID && o.SubjectID == SubjectID && o.Semester == SystemParamsInFile.SEMESTER_OF_YEAR_SECOND)).FirstOrDefault();
                                if (jr != null)
                                {
                                    sheet.SetCellValue(startRow, 41, jr.Judgement);
                                }
                            }
                            //TBM

                            sur = lstSummedUpRecord.Where(o => o.PupilID == PupilID && o.SubjectID == SubjectID && o.Semester == SystemParamsInFile.SEMESTER_OF_YEAR_SECOND).FirstOrDefault();
                            if (sur != null)
                            {
                                if (lstclassSubject[k].IsCommenting != SystemParamsInFile.ISCOMMENTING_TYPE_JUDGE) //Mon tinh diem
                                {
                                    string summedUpMark = "";
                                    if (sur.SummedUpMark != null)
                                    {
                                        summedUpMark = ReportUtils.ConvertMarkForV(sur.SummedUpMark.Value);
                                    }
                                    sheet.SetCellValue(startRow, 42, summedUpMark);
                                }
                                else
                                {
                                    sheet.SetCellValue(startRow, 42, sur.JudgementResult);
                                }
                            }
                            sur = lstSummedUpRecord.Where(o => o.PupilID == PupilID && o.SubjectID == SubjectID && o.Semester >= SystemParamsInFile.SEMESTER_OF_YEAR_ALL).OrderByDescending(o => o.Semester).FirstOrDefault();

                            if (sur != null)
                            {

                                if (lstclassSubject[k].IsCommenting != SystemParamsInFile.ISCOMMENTING_TYPE_JUDGE) //Mon tinh diem
                                {
                                    decimal? summedUpMarkRe;
                                    if (sur.ReTestMark != null)
                                    {
                                        summedUpMarkRe = sur.ReTestMark;
                                    }
                                    else
                                    {
                                        summedUpMarkRe = sur.SummedUpMark;
                                    }
                                    string summedUpMark = (summedUpMarkRe.HasValue) ? ReportUtils.ConvertMarkForV(summedUpMarkRe.Value) : string.Empty;
                                    sheet.SetCellValue(startRow, 43, summedUpMark);
                                }
                                else
                                {
                                    string judgementResult;
                                    if (sur.ReTestJudgement != null && sur.ReTestJudgement != " ")
                                    {
                                        judgementResult = sur.ReTestJudgement;
                                    }
                                    else
                                    {
                                        judgementResult = sur.JudgementResult;
                                    }
                                    sheet.SetCellValue(startRow, 43, judgementResult);
                                }
                            }

                        }
                        #endregion
                    }
                    startRow++;

                }
                if (entity.Semester != GlobalConstants.SEMESTER_OF_YEAR_ALL)
                {
                    sheet.FitAllRowsOnOnePage = true;
                    sheet.ZoomPage = 90;
                }
                else
                {
                    sheet.FitAllColumnsOnOnePage = true;
                }
            }

            //Xoá sheet template
            firstSheet.Delete();

            return oBook.ToStream();
        }

        #endregion


        #region Lấy mảng băm cho các tham số đầu vào của bảng điểm tổng hợp học sinh cấp 2,3 theo kỳ
        /// <summary>
        /// Lấy mảng băm cho các tham số đầu vào của bảng điểm tổng hợp học sinh cấp 2,3 theo kỳ
        /// </summary>
        /// <param name="entity"></param>
        /// <returns>Namta</returns>
        public string GetHashKeyForSummariseTranscriptsBySemester(TranscriptOfClass entity)
        {
            IDictionary<string, object> dic = new Dictionary<string, object> {
                {"AcademicYearID", entity.AcademicYearID},
                {"EducationLevelID  ", entity.EducationLevelID},
                {"ClassID", entity.ClassID},
                {"SchoolID ", entity.SchoolID},
                {"Semester", entity.Semester}
            };
            return ReportUtils.GetHashKey(dic);
        }
        #endregion

        #region Lấy bảng điểm tổng hợp học sinh cấp 2,3 theo kỳđược cập nhật mới nhất

        /// <summary>
        /// GetSummariseTranscriptsBySemester
        /// </summary>
        /// <param name="entity">The entity.</param>
        /// <returns>
        /// ProcessedReport
        /// </returns>
        ///<author>Nam ta</author>
        ///<Date>11/23/2012</Date>
        public ProcessedReport GetSummariseTranscriptsBySemester(TranscriptOfClass entity)
        {
            string reportCode = SystemParamsInFile.REPORT_BANGDIEMTONGHOPLOPCAP23THEOKY;

            string inputParameterHashKey = GetHashKeyForTranscriptsBySemester(entity);
            return ProcessedReportBusiness.GetProcessedReport(reportCode, inputParameterHashKey);
        }
        #endregion


        #region Lưu lại thông tin bảng điểm tổng hợp học sinh cấp 2,3 theo kỳ
        /// <summary>
        /// Lưu lại thông tin bảng điểm học sinh theo kỳ
        /// </summary>
        /// <param name="entity"></param>
        /// <returns>namta</returns>
        public ProcessedReport InsertSummariseTranscriptsBySemester(TranscriptOfClass entity, Stream data)
        {
            string reportCode = SystemParamsInFile.REPORT_BANGDIEMTONGHOPLOPCAP23THEOKY;

            ReportDefinition reportDef = ReportDefinitionBusiness.GetByCode(reportCode);
            ProcessedReport pr = new ProcessedReport();
            pr.ReportCode = reportCode;
            pr.ProcessedDate = DateTime.Now;
            pr.InputParameterHashKey = GetHashKeyForTranscriptsBySemester(entity);
            pr.ReportData = ReportUtils.Compress(data);


            //Tạo tên file HS_[SchoolLevel]_BangDiemTH_[EducationLevel/Class]_[Semester]

            //Tạo tên file HS_[SchoolLevel]_BangDiem_[Class]_[Semester]_[Subject]
            string outputNamePattern = reportDef.OutputNamePattern;
            string semester = ReportUtils.ConvertSemesterForReportName(entity.Semester);
            string schoolLevel = ReportUtils.ConvertAppliedLevelForReportName(entity.AppliedLevel);
            string educationLevelOrClass = (entity.ClassID == 0) ?
                EducationLevelRepository.Find(entity.EducationLevelID).Resolution : ClassProfileBusiness.Find(entity.ClassID).DisplayName;

            outputNamePattern = outputNamePattern.Replace("[Semester]", ReportUtils.StripVNSign(semester));
            outputNamePattern = outputNamePattern.Replace("[SchoolLevel]", ReportUtils.StripVNSign(schoolLevel));
            outputNamePattern = outputNamePattern.Replace("[EducationLevel/Class]", ReportUtils.StripVNSign(educationLevelOrClass));
            pr.ReportName = ReportUtils.RemoveSpecialCharacters(outputNamePattern) + "." + reportDef.OutputFormat;




            IDictionary<string, object> dic = new Dictionary<string, object> {
                {"AcademicYearID", entity.AcademicYearID},
                {"EducationLevelID  ", entity.EducationLevelID},
                {"ClassID", entity.ClassID},
                {"SchoolID ", entity.SchoolID},
                {"Semester", entity.Semester}
            };
            ProcessedReportParameterRepository.Insert(dic, pr);
            ProcessedReportRepository.Insert(pr);
            ProcessedReportRepository.Save();
            return pr;
        }
        #endregion

        #region Lưu lại thông tin bảng điểm tổng hợp học sinh cấp 2,3 theo kỳ

        /// <summary>
        /// CreateTranscriptsBySemester
        /// </summary>
        /// <param name="entity">The entity.</param>
        /// <returns>
        /// Stream
        /// </returns>
        ///<author>Nam ta</author>
        ///<Date>11/23/2012</Date>
        public Stream CreateSummariseTranscriptsBySemester(TranscriptOfClass entity)
        {
            string reportCode = SystemParamsInFile.REPORT_BANGDIEMTONGHOPLOPCAP23THEOKY;
            ReportDefinition reportDef = ReportDefinitionBusiness.GetByCode(reportCode);

            string templatePath = SystemParamsInFile.TEMPLATE_FOLDER + reportDef.TemplateDirectory + "/" + reportDef.TemplateName;
            IVTWorkbook oBook = VTExport.OpenWorkbook(templatePath);

            // Lấy sheet template
            IVTWorksheet firstsheet = oBook.GetSheet(1);

            // ClassProfile classProfile = ClassProfileBusiness.Find(entity.ClassID);
            List<ClassProfile> lstClass = new List<ClassProfile>();
            if (entity.ClassID == 0)
            {
                lstClass = ClassProfileBusiness.getClassByAccountRole(entity.AcademicYearID, entity.SchoolID, entity.EducationLevelID, null,
                                                                       entity.UserAccountID, entity.IsSuperVisingDept, entity.IsSubSuperVisingDept).ToList();

            }
            else
            {
                ClassProfile cp = ClassProfileBusiness.Find(entity.ClassID);
                lstClass.Add(cp);
            }
            if (lstClass == null || lstClass.Count() == 0)
            {
                throw new BusinessException("Không có lớp nào trong khối được chọn");
            }
            //Lấy dữ liệu điểm trung bình các môn trong bảng SummedUpRecord
            Dictionary<string, object> dicSummedUpRecord = new Dictionary<string, object>
            {
                 {"AcademicYearID", entity.AcademicYearID},
                 {"EducationLevelID", entity.EducationLevelID},
                 {"ClassID", entity.ClassID}
            };
            List<VSummedUpRecord> lstSummedUpRecordHK = VSummedUpRecordBusiness.SearchBySchool(entity.SchoolID, dicSummedUpRecord).ToList();
            //Lấy danh sách danh hiệu thi đua
            Dictionary<string, object> dicPupilEmulation = new Dictionary<string, object>
            {
                 {"AcademicYearID", entity.AcademicYearID},
                 {"EducationLevelID", entity.EducationLevelID},
                 {"ClassID", entity.ClassID},
                 {"Semester", entity.Semester}
            };
            var lstPupilEmulationHK = (from p in PupilEmulationBusiness.SearchBySchool(entity.SchoolID, dicPupilEmulation)
                                       join q in HonourAchivementTypeBusiness.All on p.HonourAchivementTypeID equals q.HonourAchivementTypeID
                                       select new PupilEmulationBO
                                       {
                                           PupilEmulationID = p.PupilEmulationID,
                                           PupilID = p.PupilID,
                                           ClassID = p.ClassID,
                                           Semester = p.Semester,
                                           HonourAchivementTypeID = p.HonourAchivementTypeID,
                                           HonourAchivementTypeResolution = q.Resolution
                                       }).ToList();


            //Lấy thông tin điểm danh
            AcademicYear academicYear = AcademicYearBusiness.Find(entity.AcademicYearID);
            DateTime? fromAbsenceDate;
            DateTime? toAbsenceDate;
            if (entity.Semester == SystemParamsInFile.SEMESTER_OF_YEAR_FIRST)
            {
                fromAbsenceDate = academicYear.FirstSemesterStartDate;
                toAbsenceDate = academicYear.FirstSemesterEndDate;
            }
            else if (entity.Semester == SystemParamsInFile.SEMESTER_OF_YEAR_SECOND)
            {
                fromAbsenceDate = academicYear.SecondSemesterStartDate;
                toAbsenceDate = academicYear.SecondSemesterEndDate;
            }
            else
            {
                fromAbsenceDate = academicYear.FirstSemesterStartDate;
                toAbsenceDate = academicYear.SecondSemesterEndDate;
            }
            Dictionary<string, object> dicPupilAbsence = new Dictionary<string, object> 
                                                            {
                                                                {"AcademicYearID", entity.AcademicYearID}, 
                                                                {"EducationLevelID", entity.EducationLevelID},
                                                                {"ClassID", entity.ClassID},
                                                                {"FromAbsentDate", fromAbsenceDate},
                                                                {"ToAbsentDate", toAbsenceDate}
                                                            };
            List<PupilAbsence> listPupilAbsenceEdu = PupilAbsenceBusiness.SearchBySchool(entity.SchoolID, dicPupilAbsence).ToList();

            // QuangNN2 - Loại bỏ những trường hợp vắng không nằm trong thời gian 2 học kì của năm học trong TH báo cáo Cả năm
            /*if (entity.Semester == SystemParamsInFile.SEMESTER_OF_YEAR_ALL)
            {
                listPupilAbsenceEdu = listPupilAbsenceEdu.Where(o => o.AbsentDate < academicYear.FirstSemesterEndDate || o.AbsentDate > academicYear.SecondSemesterStartDate).ToList();
            }*/

            //Lấy các thông tin TBCM, HL, HK, Danh Hieu, Xep Hang
            Dictionary<string, object> dicPupilRanking = new Dictionary<string, object>
            {
                {"AcademicYearID", entity.AcademicYearID},
                {"EducationLevelID", entity.EducationLevelID},
                {"ClassID", entity.ClassID}
            };
            IQueryable<VPupilRanking> listPupilRankingHKTemp = VPupilRankingBusiness.SearchBySchool(entity.SchoolID, dicPupilRanking).Where(o => o.PeriodID == null);
            List<PupilRankingBO> listPupilRankingHK = (from p in listPupilRankingHKTemp
                                                       join pp in PupilProfileBusiness.All on p.PupilID equals pp.PupilProfileID
                                                       join q in CapacityLevelBusiness.All on p.CapacityLevelID equals q.CapacityLevelID into g1
                                                       from j1 in g1.DefaultIfEmpty()
                                                       join r in ConductLevelBusiness.All on p.ConductLevelID equals r.ConductLevelID into g2
                                                       from j2 in g2.DefaultIfEmpty()
                                                       select new PupilRankingBO
                                                       {
                                                           PupilRankingID = p.PupilRankingID,
                                                           PupilID = p.PupilID,
                                                           ClassID = p.ClassID,
                                                           Semester = p.Semester,
                                                           ConductLevelName = j2.Resolution,
                                                           CapacityLevel = j1.CapacityLevel1,
                                                           AverageMark = p.AverageMark,
                                                           Rank = p.Rank,
                                                           TotalAbsentDaysWithPermission = p.TotalAbsentDaysWithPermission,
                                                           TotalAbsentDaysWithoutPermission = p.TotalAbsentDaysWithoutPermission
                                                       }).ToList();
            //Lấy dữ liệu ClassSubject
            Dictionary<string, object> dicClassSubject = new Dictionary<string, object>
            {
                {"AcademicYearID", entity.AcademicYearID},
                {"EducationLevelID", entity.EducationLevelID},
                {"ClassID", entity.ClassID}
            };
            IQueryable<ClassSubject> lstClassSubjectTemp = ClassSubjectBusiness.SearchBySchool(entity.SchoolID, dicClassSubject);
            List<ClassSubjectBO> lstClassSubject = (from p in lstClassSubjectTemp
                                                    join s in SubjectCatBusiness.All on p.SubjectID equals s.SubjectCatID
                                                    select new ClassSubjectBO
                                                    {
                                                        SubjectID = p.SubjectID,
                                                        ClassID = p.ClassID,
                                                        DisplayName = s.DisplayName,
                                                        SubjectName = s.SubjectName,
                                                        OrderInSubject = s.OrderInSubject,
                                                        IsCommenting = p.IsCommenting,
                                                        SectionPerWeekSecondSemester = p.SectionPerWeekSecondSemester,
                                                        SectionPerWeekFirstSemester = p.SectionPerWeekFirstSemester
                                                    }).ToList();

            //Lấy danh sách học sinh trong pupilOfClass
            Dictionary<string, object> dicPupilOfClass = new Dictionary<string, object>
            {
                 {"AcademicYearID", entity.AcademicYearID},
                 {"EducationLevelID", entity.EducationLevelID},
                 {"ClassID", entity.ClassID},
                 {"CheckWithClass", "checkWithClass"}
            };
            List<PupilOfClassBO> lstPupilOfEdu = (from p in PupilOfClassBusiness.SearchBySchool(entity.SchoolID, dicPupilOfClass)
                                                  join q in PupilProfileBusiness.All on p.PupilID equals q.PupilProfileID
                                                  select new PupilOfClassBO
                                                  {
                                                      PupilID = p.PupilID,
                                                      ClassID = p.ClassID,
                                                      Status = p.Status,
                                                      PupilCode = q.PupilCode,
                                                      Name = q.Name,
                                                      PupilFullName = q.FullName,
                                                      OrderInClass = p.OrderInClass,
                                                      // QuangNN2 - Bo sung 2 cot Giới tính và Dân tộc
                                                      Genre = q.Genre,
                                                      EthnicName = q.Ethnic.EthnicName
                                                  }).ToList();        

            //Danh sach hoc sinh mien giam            
            IDictionary<string, object> dic = new Dictionary<string, object>()
            {
                {"SchoolID",entity.SchoolID},
                {"AcademicYearID",entity.AcademicYearID},
                {"SemesterID",entity.Semester},                
                {"YearID",academicYear.Year}
            };
            if (entity.Semester == 1)
            {
                dic["HasFirstSemester"] = true;
            }
            if (entity.Semester == 2)
            {
                dic["HasSecondSemester"] = true;
            }
            List<ExemptedSubject> lstPupilExempted = ExemptedSubjectBusiness.Search(dic).ToList();
            ExemptedSubject examptedPupilObj = null;
            //end hoc sinh mien giam

            Dictionary<string, object> dicPupilOfClassHK = new Dictionary<string, object>
            {
                 {"AcademicYearID", entity.AcademicYearID},
                 {"EducationLevelID", entity.EducationLevelID},
                 {"ClassID", entity.ClassID}
            };
            List<PupilOfClassBO> lstPupilOfEduHK = (from p in PupilOfClassBusiness.SearchBySchool(entity.SchoolID, dicPupilOfClassHK).AddCriteriaSemester(academicYear, entity.Semester)
                                                    join q in PupilProfileBusiness.All on p.PupilID equals q.PupilProfileID
                                                    select new PupilOfClassBO
                                                    {
                                                        PupilID = p.PupilID,
                                                        ClassID = p.ClassID,
                                                        Status = p.Status,
                                                        PupilCode = q.PupilCode,
                                                        Name = q.Name,
                                                        PupilFullName = q.FullName,
                                                        OrderInClass = p.OrderInClass,
                                                        // QuangNN2 - Bo sung 2 cot Giới tính và Dân tộc
                                                        Genre = q.Genre,
                                                        EthnicName = q.Ethnic.EthnicName
                                                    }).ToList();

            if ((entity.AppliedLevel == SystemParamsInFile.APPLIED_LEVEL_SECONDARY || entity.AppliedLevel == SystemParamsInFile.APPLIED_LEVEL_TERTIARY)
               && academicYear.IsShowPupil.HasValue && academicYear.IsShowPupil.Value == true)
            {
                lstPupilOfEdu = lstPupilOfEdu.Where(x => x.Status == SystemParamsInFile.PUPIL_STATUS_GRADUATED
                                           || x.Status == SystemParamsInFile.PUPIL_STATUS_STUDYING).ToList();

                lstPupilOfEduHK = lstPupilOfEduHK.Where(x => x.Status == SystemParamsInFile.PUPIL_STATUS_GRADUATED
                                           || x.Status == SystemParamsInFile.PUPIL_STATUS_STUDYING).ToList();
            }


            if (entity.Semester != SystemParamsInFile.SEMESTER_OF_YEAR_ALL)
            {
                lstSummedUpRecordHK = lstSummedUpRecordHK.Where(o => o.Semester == entity.Semester && o.PeriodID == null).ToList();
                listPupilRankingHK = listPupilRankingHK.Where(o => o.Semester == entity.Semester && o.PeriodID == null).ToList();
            }
            else
            {
                lstSummedUpRecordHK = lstSummedUpRecordHK.Where(o => o.Semester >= SystemParamsInFile.SEMESTER_OF_YEAR_ALL && o.PeriodID == null).ToList();
                lstSummedUpRecordHK = lstSummedUpRecordHK.Where(o => o.Semester == lstSummedUpRecordHK.Where(u => u.PupilID == o.PupilID && u.ClassID == o.ClassID && u.SubjectID == o.SubjectID).Max(u => u.Semester)).ToList();
                listPupilRankingHK = listPupilRankingHK.Where(o => o.Semester >= SystemParamsInFile.SEMESTER_OF_YEAR_ALL && o.PeriodID == null).ToList();
                listPupilRankingHK = listPupilRankingHK.Where(o => o.Semester == listPupilRankingHK.Where(u => u.PupilID == o.PupilID && u.ClassID == o.ClassID).Max(v => v.Semester)).ToList();
            }

            // Các thông tin chung:
            SchoolProfile schoolProfile = SchoolProfileBusiness.Find(entity.SchoolID);
            string SchoolName = schoolProfile.SchoolName.ToUpper();
            string AcademicYear = academicYear.DisplayTitle;
            string Province = (schoolProfile.District != null ? schoolProfile.District.DistrictName : "");
            string supervisingDeptName = UtilsBusiness.GetSupervisingDeptName(schoolProfile.SchoolProfileID, entity.AppliedLevel).ToUpper();
            List<int> lstSectionKeyID = new List<int>();

            foreach (ClassProfile classProfile in lstClass)
            {
                string ClassName = classProfile.DisplayName.ToUpper();
                lstSectionKeyID = classProfile.SeperateKey.HasValue ? UtilsBusiness.GetListSectionID(classProfile.SeperateKey.Value) : new List<int>();
                // Lấy danh sách môn học theo thứ tự (nếu TranscriptOfClass.SubjectID = 0)
                List<ClassSubjectBO> lstclassSubject = lstClassSubject.Where(o => o.ClassID == classProfile.ClassProfileID).OrderBy(o => o.OrderInSubject).ThenBy(o => o.DisplayName).ToList();
                if (entity.Semester == 1)
                {
                    lstclassSubject = lstclassSubject.Where(o => o.SectionPerWeekFirstSemester > 0).ToList();
                }
                if (entity.Semester == 2)
                {
                    lstclassSubject = lstclassSubject.Where(o => o.SectionPerWeekSecondSemester > 0).ToList();
                }
                if (entity.Semester == 3)
                {
                    lstclassSubject = lstclassSubject.Where(o => o.SectionPerWeekSecondSemester > 0 || o.SectionPerWeekFirstSemester > 0).ToList();
                }

                // Danh sách học sinh theo thứ tự: PupilOfClassBusiness.SearchBySchool(SchoolID, Dictionary) 
                List<PupilOfClassBO> lstPupilOfClass = lstPupilOfEdu.Where(o => o.ClassID == classProfile.ClassProfileID).OrderBy(o => o.OrderInClass).ThenBy(o => o.Name).ThenBy(o => o.PupilFullName).ToList();

                var lstPupilOfClassQHK = lstPupilOfEduHK.Where(o => o.ClassID == classProfile.ClassProfileID).Select(o => new { o.PupilID, o.ClassID }).ToList();

                //Chiendd1: 03/06/2016 Bo lay diem danh truc tiep, lay tu thong tin xep loai hanh kiem.
                //Chiendd1: 25.04.2018 lay diem danh truc tiep trong bang diem danh
                // Thông tin nghỉ học (cột CP, KP)
                List<PupilAbsence> listPupilAbsence = listPupilAbsenceEdu.Where(o => o.ClassID == classProfile.ClassProfileID).Where(p=>lstSectionKeyID.Contains(p.Section)).ToList();

                // QuangNN2 - Lọc danh sách buổi nghỉ, nếu có nhiều buổi trong cùng 1 ngày thì chỉ lấy buổi đầu tiên
                //listPupilAbsence = PupilAbsenceBusiness.GetPupilAbsenceByDate(listPupilAbsence);

                List<VSummedUpRecord> lstSummedUpRecord = lstSummedUpRecordHK.Where(o => o.ClassID == classProfile.ClassProfileID).ToList();
                List<PupilEmulationBO> lstPupilEmulation = lstPupilEmulationHK.Where(o => o.ClassID == classProfile.ClassProfileID).ToList();
                List<PupilRankingBO> listPupilRanking = listPupilRankingHK.Where(o => o.ClassID == classProfile.ClassProfileID).ToList();

                //Tạo sheet
                IVTWorksheet sheet = oBook.CopySheetToLast(firstsheet, "AB300");

                //Fill dữ liệu chung
                sheet.Name =Utils.StripVNSignAndSpace(ClassName);
                sheet.SetCellValue("B15", "");
                sheet.SetCellValue("B2", supervisingDeptName); // QuangNN2 - Bat dau fill tu cot B - Cot A de luu trang thai Hoc Sinh
                sheet.SetCellValue("B3", SchoolName);

                /*String semeterString = "";
                if (entity.Semester == GlobalConstants.SEMESTER_OF_YEAR_FIRST)
                {
                    semeterString = "I";
                    sheet.SetCellValue("A5", "BẢNG ĐIỂM TỔNG HỢP HỌC KỲ " + semeterString + " " + AcademicYear);
                }
                else if (entity.Semester == GlobalConstants.SEMESTER_OF_YEAR_SECOND)
                {
                    semeterString = "II";
                    sheet.SetCellValue("A5", "BẢNG ĐIỂM TỔNG HỢP HỌC KỲ " + semeterString + " " + AcademicYear);
                }
                else
                {
                    sheet.SetCellValue("A5", "BẢNG ĐIỂM TỔNG HỢP CẢ NĂM " + AcademicYear);
                }
                //sheet.SetCellValue("A5", "BẢNG ĐIỂM TỔNG HỢP HỌC KỲ " + semeterString + " " + AcademicYear);
                sheet.SetCellValue("A6", "LỚP " + classProfile.DisplayName);*/

                // Sua HeaderTeamplate
                int headerStartCol = 6; // QuangNN2 - Them 2 cot Gioi tinh va Dan toc va 1 cot Trang thai o dau tien - 3 + 2 + 1 = 6
                int headerRow = 9;
                foreach (ClassSubjectBO ClassSubject in lstclassSubject)
                {
                    sheet.SetCellValue(headerRow, headerStartCol, ClassSubject.DisplayName);
                    headerStartCol++;
                }

                //Cac cot con lai
                int colTBCM = headerStartCol;
                const int firstColData = 2; // QuangNN2 - Mac dinh la 2 - Chua 1 cot de Trang thai (Ẩn)
                headerRow = 8;
                /*sheet.SetCellValue(headerRow, colTBCM, "TBcm");*/

                int colXLHL = colTBCM + 1;
                /*sheet.SetCellValue(headerRow, colXLHL, "XLHL");*/

                int colXLHLK = colXLHL + 1;
                /*sheet.SetCellValue(headerRow, colXLHLK, "colXLHLK");*/

                int colCP = colXLHLK + 1;
                /*sheet.SetCellValue(headerRow, colCP, "CP");*/

                int colKP = colCP + 1;
                /*sheet.SetCellValue(headerRow, colKP, "KP");*/

                int colDanhHieu = colKP + 1;
                /*sheet.SetCellValue(headerRow, colDanhHieu, "Danh hiệu");*/

                int colXepHang = colDanhHieu + 1;
                /*sheet.SetCellValue(headerRow, colXepHang, "Xếp hạng");*/

                // Di chuyen vi tri header chxhvn
                /*IVTRange columnHead = firstsheet.GetRange("N20", "V22");
                sheet.CopyPasteSameSize(columnHead, "O2");*/

                IVTRange columnHead = firstsheet.GetRange("AM2", "AU3"); // QuangNN2 - Đổi chỗ gắn tiêu đề
                sheet.CopyPasteSameSize(columnHead, 2, colXepHang - 10);
                sheet.SetCellValue(4, colXepHang - 8, Province + ", ngày " + DateTime.Now.Day.ToString() + " tháng " + DateTime.Now.Month.ToString() + " năm " + DateTime.Now.Year.ToString());


                //Merger col Title file
                /*IVTRange rangeDL = sheet.GetRange(2, colXepHang - 8, 2, colXepHang + 1);
                rangeDL.Merge();
                IVTRange rangeTD = sheet.GetRange(3, colXepHang - 8, 3, colXepHang + 1);
                rangeTD.Merge();*/

                IVTRange rangeDate = sheet.GetRange(4, colXepHang - 8, 4, colXepHang);
                rangeDate.Merge();
                rangeDate.SetHAlign(VTHAlign.xlHAlignCenter);
                IVTRange rangeBangDiem = sheet.GetRange(5, 2, 5, colXepHang);
                rangeBangDiem.Merge();

                /*sheet.MergeRow(5, 1, colXepHang - 1);*/
                IVTRange rangeClass = sheet.GetRange(6, 2, 6, colXepHang);
                rangeClass.Merge();

                /*sheet.MergeRow(6, 1, colXepHang - 1);*/

                String semeterString = "";
                if (entity.Semester == GlobalConstants.SEMESTER_OF_YEAR_FIRST)
                {
                    semeterString = "I";
                    sheet.SetCellValue("B5", "BẢNG ĐIỂM TỔNG HỢP HỌC KỲ " + semeterString + " " + AcademicYear);
                }
                else if (entity.Semester == GlobalConstants.SEMESTER_OF_YEAR_SECOND)
                {
                    semeterString = "II";
                    sheet.SetCellValue("B5", "BẢNG ĐIỂM TỔNG HỢP HỌC KỲ " + semeterString + " " + AcademicYear);
                }
                else
                {
                    sheet.SetCellValue("B5", "BẢNG ĐIỂM TỔNG HỢP CẢ NĂM " + AcademicYear);
                }

                /*sheet.SetCellValue("A5", "BẢNG ĐIỂM TỔNG HỢP HỌC KỲ " + semeterString + " " + AcademicYear);*/
                sheet.SetCellValue("B6", "LỚP " + classProfile.DisplayName);

                // Merger col title DIEM TRUNG BINH CAC MON HOC
                columnHead = sheet.GetRange(8, 6, 8, colTBCM - 1); // QuangNN2 Doi 3 lai thanh 6
                columnHead.Merge();

                // Copy col header tu cot TBCM
                columnHead = firstsheet.GetRange("Y8", "AE14");

                sheet.CopyPasteSameSize(columnHead, headerRow, colTBCM);

                IVTRange emptyRange = firstsheet.GetRange("B18", "U24");
                sheet.CopyPasteSameSize(emptyRange, headerRow, colXepHang + 1);

                int startRow = 10;
                SummedUpRecord sur = new SummedUpRecord();
                int cntStudent = lstPupilOfClass.Count();

                IVTRange RangeContent = sheet.GetRange(startRow, firstColData, startRow + 4, colXepHang);
                IVTRange cpyRange;
                int RowEnd = 15;
                if (lstPupilOfClass.Count() > 5)
                {
                    int numberGroupStudent = cntStudent / 5;
                    if (cntStudent % 5 > 0)
                    {
                        numberGroupStudent++;
                    }
                    for (int j = 0; j < numberGroupStudent - 1; j++)
                    {
                        sheet.CopyPasteSameSize(RangeContent, RowEnd, firstColData);
                        RowEnd = RowEnd + 5;
                    }
                }

                sheet.SetCellValue("F8", "ĐIỂM TRUNG BÌNH CÁC MÔN HỌC");
                for (int i = 0; i < cntStudent; i++)
                {
                    int pupilID = lstPupilOfClass[i].PupilID;
                    var poc = lstPupilOfClass[i];
                    IEnumerable<VSummedUpRecord> lstSummedUpOfPupil = lstSummedUpRecord.Where(u => u.PupilID == pupilID);

                    //Cot diem dau tien
                    headerStartCol = 6; // QuangNN2 - Thêm 2 cột Giới tính và Dân tộc và 1 cột Trạng thái - 3 + 2 + 1 = 6

                    bool check = lstPupilOfClassQHK.Any(u => u.PupilID == pupilID);
                    int countSubject = lstclassSubject.Count();
                    if (poc.Status != SystemParamsInFile.PUPIL_STATUS_STUDYING && poc.Status != SystemParamsInFile.PUPIL_STATUS_GRADUATED)
                    {
                        if ((i + 1) % 5 == 1)
                        {
                            cpyRange = firstsheet.GetRange(286, 2, 286, countSubject + 12);
                            sheet.CopyPaste(cpyRange, startRow, 2);
                        }
                        else if ((i + 1) % 5 == 0 || (i + 1) == cntStudent)
                        {
                            cpyRange = firstsheet.GetRange(288, 2, 288, countSubject + 12);
                            sheet.CopyPaste(cpyRange, startRow, 2);
                        }
                        else
                        {
                            cpyRange = firstsheet.GetRange(287, 2, 287, countSubject + 12);
                            sheet.CopyPaste(cpyRange, startRow, 2);
                        }
                    }

                    if ((i + 1) == cntStudent && (poc.Status != SystemParamsInFile.PUPIL_STATUS_STUDYING && poc.Status != SystemParamsInFile.PUPIL_STATUS_GRADUATED))
                    {
                        cpyRange = firstsheet.GetRange(288, 2, 288, countSubject + 12);
                        sheet.CopyPaste(cpyRange, startRow, 2);
                    }
                    else if ((i + 1) == cntStudent)
                    {
                        cpyRange = firstsheet.GetRange(290, 2, 290, countSubject + 12);
                        sheet.CopyPaste(cpyRange, startRow, 2);
                    }

                    //STT
                    sheet.SetCellValue(startRow, 2, i + 1); // QuangNN2 - Set lai vi tri cot bat dau STT

                    // Diem tong ket cua tung mn hoc
                    foreach (ClassSubjectBO ClassSubject in lstclassSubject)
                    {
                        VSummedUpRecord SummedUpRecord = lstSummedUpOfPupil.FirstOrDefault(o => o.SubjectID == ClassSubject.SubjectID);

                        if (SummedUpRecord != null)
                        {
                            int status = ClassSubject.IsCommenting.HasValue ? ClassSubject.IsCommenting.Value : 0;
                            decimal? summedUp;
                            string judgementResult;
                            if (SummedUpRecord.ReTestMark != null)
                            {
                                summedUp = SummedUpRecord.ReTestMark;
                            }
                            else
                            {
                                summedUp = SummedUpRecord.SummedUpMark;
                            }

                            if (SummedUpRecord.ReTestJudgement != null && SummedUpRecord.ReTestJudgement != " ")
                            {
                                judgementResult = SummedUpRecord.ReTestJudgement;
                            }
                            else
                            {
                                judgementResult = SummedUpRecord.JudgementResult;
                            }
                            if (status == GlobalConstants.ISCOMMENTING_TYPE_MARK || status == GlobalConstants.ISCOMMENTING_TYPE_MARK_JUDGE)
                            {
                                if (summedUp.HasValue && check)
                                {
                                    sheet.SetCellValue(startRow, headerStartCol, ReportUtils.ConvertMarkForV(summedUp.Value));
                                }
                            }
                            if (status == GlobalConstants.ISCOMMENTING_TYPE_JUDGE)
                            {
                                if (check)
                                {
                                    sheet.SetCellValue(startRow, headerStartCol, judgementResult);
                                }
                            }
                        }
                        else
                        {
                            //Neu la hoc sinh miem giam o ki 1 thi fill M1, ki 2: M2; ca nam: M
                            examptedPupilObj = lstPupilExempted.Where(p => p.ClassID == classProfile.ClassProfileID
                                && ((p.FirstSemesterExemptType.HasValue && p.FirstSemesterExemptType.Value > 0)
                                   || (p.SecondSemesterExemptType.HasValue && p.SecondSemesterExemptType.Value > 0))
                                   && p.PupilID == pupilID && p.SubjectID == ClassSubject.SubjectID).FirstOrDefault();

                            if (examptedPupilObj != null &&
                                (entity.Semester == SystemParamsInFile.SEMESTER_OF_YEAR_FIRST || entity.Semester == SystemParamsInFile.SEMESTER_OF_YEAR_SECOND))
                            {
                                if ((examptedPupilObj.FirstSemesterExemptType.HasValue && examptedPupilObj.FirstSemesterExemptType.Value > 0)
                                     && (examptedPupilObj.SecondSemesterExemptType.HasValue && examptedPupilObj.SecondSemesterExemptType.Value > 0))
                                {
                                    sheet.SetCellValue(startRow, headerStartCol, "M");
                                }
                                else if (examptedPupilObj.FirstSemesterExemptType.HasValue && examptedPupilObj.FirstSemesterExemptType.Value > 0)
                                {
                                    sheet.SetCellValue(startRow, headerStartCol, "M1");
                                }
                                else if (examptedPupilObj.SecondSemesterExemptType.HasValue && examptedPupilObj.SecondSemesterExemptType.Value > 0)
                                {
                                    sheet.SetCellValue(startRow, headerStartCol, "M2");
                                }
                            }
                            else
                            {
                                if (examptedPupilObj != null
                                    && examptedPupilObj.FirstSemesterExemptType.HasValue && examptedPupilObj.FirstSemesterExemptType.Value > 0
                                    && examptedPupilObj.SecondSemesterExemptType.HasValue && examptedPupilObj.SecondSemesterExemptType.Value > 0)
                                {
                                    sheet.SetCellValue(startRow, headerStartCol, "M");
                                }
                            }
                        }
                        headerStartCol = headerStartCol + 1;
                    }

                    // Dien thong tin cho 7 cot con la
                    if (check)
                    {
                        PupilRankingBO PupilRanking = listPupilRanking.FirstOrDefault(o => o.PupilID == pupilID);
                        if (PupilRanking != null)
                        {
                            // Cot TBCM
                            if (PupilRanking.AverageMark != null)
                            {
                                sheet.SetCellValue(startRow, colTBCM, ReportUtils.ConvertMarkForV(PupilRanking.AverageMark.Value));
                            }

                            // Cot xep hang
                            if (PupilRanking.Rank != null)
                            {
                                sheet.SetCellValue(startRow, colXepHang, PupilRanking.Rank.Value);
                            }

                            // Cot hanh kiem
                            if (PupilRanking.ConductLevelName != null)
                            {
                                string Resolution = UtilsBusiness.ConvertCapacity(PupilRanking.ConductLevelName);
                                sheet.SetCellValue(startRow, colXLHLK, Resolution);
                            }

                            // Cot hoc luc
                            if (PupilRanking.CapacityLevel != null)
                            {
                                string Resolution = UtilsBusiness.ConvertCapacity(PupilRanking.CapacityLevel);
                                sheet.SetCellValue(startRow, colXLHL, Resolution);
                            }
                            //Dem so buoi co phep va khong phep
                            int cntAbsence = listPupilAbsence.Count(o => o.PupilID == pupilID && o.IsAccepted == true);//PupilRanking.TotalAbsentDaysWithPermission ?? 0; 
                            sheet.SetCellValue(startRow, colCP, cntAbsence);

                            int cntNotAbsence = listPupilAbsence.Count(o => o.PupilID == pupilID && o.IsAccepted == false); //PupilRanking.TotalAbsentDaysWithoutPermission ?? 0;
                            sheet.SetCellValue(startRow, colKP, cntNotAbsence);

                        }

                       
                        PupilEmulationBO pupilEmulation = lstPupilEmulation.Where(o => o.PupilID == pupilID).FirstOrDefault();
                        if (pupilEmulation != null && pupilEmulation.HonourAchivementTypeID != null)
                        {
                            string Resolution = UtilsBusiness.ConvertCapacity(pupilEmulation.HonourAchivementTypeResolution);
                            sheet.SetCellValue(startRow, colDanhHieu, Resolution);
                        }
                    }
                    sheet.SetCellValue(startRow, 3, poc.PupilFullName); // Fill cot ten

                    // QuangNN2 - Fill cot Trang thai, Gioi tính và Dân tộc
                    string strStatus = string.Empty;
                    if (poc.Status == GlobalConstants.PUPIL_STATUS_STUDYING || poc.Status == GlobalConstants.PUPIL_STATUS_GRADUATED)
                    {
                        strStatus = "DH";
                    }

                    string strGenre = poc.Genre == GlobalConstants.GENRE_MALE ? "Nam" : "Nữ";

                    sheet.SetCellValue(startRow, 1, strStatus);
                    sheet.SetCellValue(startRow, 4, strGenre);
                    sheet.SetCellValue(startRow, 5, poc.EthnicName);

                    startRow = startRow + 1;
                }

                int rowDel = cntStudent + 10;
                sheet.DeleteRow(rowDel);
                sheet.DeleteRow(rowDel);
                sheet.DeleteRow(rowDel);
                sheet.DeleteRow(rowDel);

                // Dán ghi chú cho bảng
                cpyRange = firstsheet.GetRange("B15", "B15");
                sheet.CopyPasteSameSize(cpyRange, 10 + cntStudent, 2);

                #region QuangNN2 - Phần Thống kê
                // QuangNN2 - Fill phan thong ke
                IVTRange rangeStatistic = sheet.GetRange(274, 2, 280, 26);
                sheet.CopyPaste(rangeStatistic, cntStudent + 12, 2);

                // Khai báo các dòng, cột thống kê
                int rowTotalPupil = cntStudent + 15;
                int rowFemale = rowTotalPupil + 1;
                int rowEthnic = rowFemale + 1;
                int rowFemaleEthnic = rowEthnic + 1;

                int colStaSL = 4;
                int colStaHK = 5;
                int colStaHL = 13;
                int colStaDH = 23;

                int startStaRow = 10;
                int endStaRow = 10 + cntStudent;

                string strEmpty = "\"\"";
                string statusStudy = "\"DH\"";
                string gender = "\"Nữ\"";
                string ethnic = "\"Kinh\"";

                // Thiết lập công thức và điền dữ liệu
                //-- Cột số lượng
                sheet.GetRange(rowTotalPupil, colStaSL, rowTotalPupil, colStaSL).Range.Formula = "=SUMPRODUCT(--(C" + startStaRow + ":C" + endStaRow + "<>" + strEmpty + "),--(A" + startStaRow + ":A" + endStaRow + "=" + statusStudy + "))";
                sheet.GetRange(rowFemale, colStaSL, rowFemale, colStaSL).Range.Formula = "=SUMPRODUCT(--(D" + startStaRow + ":D" + endStaRow + "=" + gender + "),--(A" + startStaRow + ":A" + endStaRow + "=" + statusStudy + "))";
                sheet.GetRange(rowEthnic, colStaSL, rowEthnic, colStaSL).Range.Formula = "=SUMPRODUCT(--(E" + startStaRow + ":E" + endStaRow + "<>" + strEmpty + "),--(E" + startStaRow + ":E" + endStaRow + "<>" + ethnic + "),--(A" + startStaRow + ":A" + endStaRow + "=" + statusStudy + "))";
                sheet.GetRange(rowFemaleEthnic, colStaSL, rowFemaleEthnic, colStaSL).Range.Formula = "=SUMPRODUCT(--(D" + startStaRow + ":D" + endStaRow + "=" + gender + "),--(E" + startStaRow + ":E" + endStaRow + "<>" + strEmpty + "),--(E" + startStaRow + ":E" + endStaRow + "<>" + ethnic + "),--(A" + startStaRow + ":A" + endStaRow + "=" + statusStudy + "))";

                string[] arrCriHK = new string[4] { "\"T\"", "\"K\"", "\"Tb\"", "\"Y\"" };
                string[] arrCriHL = new string[5] { "\"G\"", "\"K\"", "\"Tb\"", "\"Y\"", "\"Kém\"" };
                string[] arrCriDH = new string[2] { "\"HSG\"", "\"HSTT\"" };

                // Cột Hạnh kiểm
                for (int i = 0; i < arrCriHK.Length; i++)
                {
                    // Tính cột thống kê
                    string am = "=SUMPRODUCT(--(" + UtilsBusiness.GetExcelColumnName(colXLHLK) + startStaRow + ":" + UtilsBusiness.GetExcelColumnName(colXLHLK) + endStaRow + "=" + arrCriHK[i] + "),--(A" + startStaRow + ":A" + endStaRow + "=" + statusStudy + "))";
                    sheet.GetRange(rowTotalPupil, colStaHK + (i * 2), rowTotalPupil, colStaHK + (i * 2)).Range.Formula = "=SUMPRODUCT(--(" + UtilsBusiness.GetExcelColumnName(colXLHLK) + startStaRow + ":" + UtilsBusiness.GetExcelColumnName(colXLHLK) + endStaRow + "=" + arrCriHK[i] + "),--(A" + startStaRow + ":A" + endStaRow + "=" + statusStudy + "))";
                    sheet.GetRange(rowFemale, colStaHK + (i * 2), rowFemale, colStaHK + (i * 2)).Range.Formula = "=SUMPRODUCT(--(" + UtilsBusiness.GetExcelColumnName(colXLHLK) + startStaRow + ":" + UtilsBusiness.GetExcelColumnName(colXLHLK) + endStaRow + "=" + arrCriHK[i] + "),--(D" + startStaRow + ":D" + endStaRow + "=" + gender + "),--(A" + startStaRow + ":A" + endStaRow + "=" + statusStudy + "))";
                    sheet.GetRange(rowEthnic, colStaHK + (i * 2), rowEthnic, colStaHK + (i * 2)).Range.Formula = "=SUMPRODUCT(--(" + UtilsBusiness.GetExcelColumnName(colXLHLK) + startStaRow + ":" + UtilsBusiness.GetExcelColumnName(colXLHLK) + endStaRow + "=" + arrCriHK[i] + "),--(E" + startStaRow + ":E" + endStaRow + "<>" + strEmpty + "),--(E" + startStaRow + ":E" + endStaRow + "<>" + ethnic + "),--(A" + startStaRow + ":A" + endStaRow + "=" + statusStudy + "))";
                    sheet.GetRange(rowFemaleEthnic, colStaHK + (i * 2), rowFemaleEthnic, colStaHK + (i * 2)).Range.Formula = "=SUMPRODUCT(--(" + UtilsBusiness.GetExcelColumnName(colXLHLK) + startStaRow + ":" + UtilsBusiness.GetExcelColumnName(colXLHLK) + endStaRow + "=" + arrCriHK[i] + "),--(D" + startStaRow + ":D" + endStaRow + "=" + gender + "),--(E" + startStaRow + ":E" + endStaRow + "<>" + strEmpty + "),--(E" + startStaRow + ":E" + endStaRow + "<>" + ethnic + "),--(A" + startStaRow + ":A" + endStaRow + "=" + statusStudy + "))";

                    // Tính cột %
                    sheet.GetRange(rowTotalPupil, colStaHK + (i * 2) + 1, rowTotalPupil, colStaHK + (i * 2) + 1).Range.Formula = "=ROUND(" + UtilsBusiness.GetExcelColumnName(colStaHK + (i * 2)) + rowTotalPupil + "/IF(D" + rowTotalPupil + "<=0,1,D" + rowTotalPupil + "),4)*100";
                    sheet.GetRange(rowFemale, colStaHK + (i * 2) + 1, rowFemale, colStaHK + (i * 2) + 1).Range.Formula = "=ROUND(" + UtilsBusiness.GetExcelColumnName(colStaHK + (i * 2)) + rowFemale + "/IF(D" + rowFemale + "<=0,1,D" + rowFemale + "),4)*100";
                    sheet.GetRange(rowEthnic, colStaHK + (i * 2) + 1, rowEthnic, colStaHK + (i * 2) + 1).Range.Formula = "=ROUND(" + UtilsBusiness.GetExcelColumnName(colStaHK + (i * 2)) + rowEthnic + "/IF(D" + rowEthnic + "<=0,1,D" + rowEthnic + "),4)*100";
                    sheet.GetRange(rowFemaleEthnic, colStaHK + (i * 2) + 1, rowFemaleEthnic, colStaHK + (i * 2) + 1).Range.Formula = "=ROUND(" + UtilsBusiness.GetExcelColumnName(colStaHK + (i * 2)) + rowFemaleEthnic + "/IF(D" + rowFemaleEthnic + "<=0,1,D" + rowFemaleEthnic + "),4)*100";
                }

                // Cột Học lực
                for (int i = 0; i < arrCriHL.Length; i++)
                {
                    // Tính cột thống kê
                    sheet.GetRange(rowTotalPupil, colStaHL + (i * 2), rowTotalPupil, colStaHL + (i * 2)).Range.Formula = "=SUMPRODUCT(--(" + UtilsBusiness.GetExcelColumnName(colXLHL) + startStaRow + ":" + UtilsBusiness.GetExcelColumnName(colXLHL) + endStaRow + "=" + arrCriHL[i] + "),--(A" + startStaRow + ":A" + endStaRow + "=" + statusStudy + "))";
                    sheet.GetRange(rowFemale, colStaHL + (i * 2), rowFemale, colStaHL + (i * 2)).Range.Formula = "=SUMPRODUCT(--(" + UtilsBusiness.GetExcelColumnName(colXLHL) + startStaRow + ":" + UtilsBusiness.GetExcelColumnName(colXLHL) + endStaRow + "=" + arrCriHL[i] + "),--(D" + startStaRow + ":D" + endStaRow + "=" + gender + "),--(A" + startStaRow + ":A" + endStaRow + "=" + statusStudy + "))";
                    sheet.GetRange(rowEthnic, colStaHL + (i * 2), rowEthnic, colStaHL + (i * 2)).Range.Formula = "=SUMPRODUCT(--(" + UtilsBusiness.GetExcelColumnName(colXLHL) + startStaRow + ":" + UtilsBusiness.GetExcelColumnName(colXLHL) + endStaRow + "=" + arrCriHL[i] + "),--(E" + startStaRow + ":E" + endStaRow + "<>" + strEmpty + "),--(E" + startStaRow + ":E" + endStaRow + "<>" + ethnic + "),--(A" + startStaRow + ":A" + endStaRow + "=" + statusStudy + "))";
                    sheet.GetRange(rowFemaleEthnic, colStaHL + (i * 2), rowFemaleEthnic, colStaHL + (i * 2)).Range.Formula = "=SUMPRODUCT(--(" + UtilsBusiness.GetExcelColumnName(colXLHL) + startStaRow + ":" + UtilsBusiness.GetExcelColumnName(colXLHL) + endStaRow + "=" + arrCriHL[i] + "),--(D" + startStaRow + ":D" + endStaRow + "=" + gender + "),--(E" + startStaRow + ":E" + endStaRow + "<>" + strEmpty + "),--(E" + startStaRow + ":E" + endStaRow + "<>" + ethnic + "),--(A" + startStaRow + ":A" + endStaRow + "=" + statusStudy + "))";

                    // Tính cột %
                    sheet.GetRange(rowTotalPupil, colStaHL + (i * 2) + 1, rowTotalPupil, colStaHL + (i * 2) + 1).Range.Formula = "=ROUND(" + UtilsBusiness.GetExcelColumnName(colStaHL + (i * 2)) + rowTotalPupil + "/IF(D" + rowTotalPupil + "<=0,1,D" + rowTotalPupil + "),4)*100";
                    sheet.GetRange(rowFemale, colStaHL + (i * 2) + 1, rowFemale, colStaHL + (i * 2) + 1).Range.Formula = "=ROUND(" + UtilsBusiness.GetExcelColumnName(colStaHL + (i * 2)) + rowFemale + "/IF(D" + rowFemale + "<=0,1,D" + rowFemale + "),4)*100";
                    sheet.GetRange(rowEthnic, colStaHL + (i * 2) + 1, rowEthnic, colStaHL + (i * 2) + 1).Range.Formula = "=ROUND(" + UtilsBusiness.GetExcelColumnName(colStaHL + (i * 2)) + rowEthnic + "/IF(D" + rowEthnic + "<=0,1,D" + rowEthnic + "),4)*100";
                    sheet.GetRange(rowFemaleEthnic, colStaHL + (i * 2) + 1, rowFemaleEthnic, colStaHL + (i * 2) + 1).Range.Formula = "=ROUND(" + UtilsBusiness.GetExcelColumnName(colStaHL + (i * 2)) + rowFemaleEthnic + "/IF(D" + rowFemaleEthnic + "<=0,1,D" + rowFemaleEthnic + "),4)*100";
                }

                // Cột Danh hiệu
                for (int i = 0; i < arrCriDH.Length; i++)
                {
                    // Tính cột thống kê
                    sheet.GetRange(rowTotalPupil, colStaDH + (i * 2), rowTotalPupil, colStaDH + (i * 2)).Range.Formula = "=SUMPRODUCT(--(" + UtilsBusiness.GetExcelColumnName(colDanhHieu) + startStaRow + ":" + UtilsBusiness.GetExcelColumnName(colDanhHieu) + endStaRow + "=" + arrCriDH[i] + "),--(A" + startStaRow + ":A" + endStaRow + "=" + statusStudy + "))";
                    sheet.GetRange(rowFemale, colStaDH + (i * 2), rowFemale, colStaDH + (i * 2)).Range.Formula = "=SUMPRODUCT(--(" + UtilsBusiness.GetExcelColumnName(colDanhHieu) + startStaRow + ":" + UtilsBusiness.GetExcelColumnName(colDanhHieu) + endStaRow + "=" + arrCriDH[i] + "),--(D" + startStaRow + ":D" + endStaRow + "=" + gender + "),--(A" + startStaRow + ":A" + endStaRow + "=" + statusStudy + "))";
                    sheet.GetRange(rowEthnic, colStaDH + (i * 2), rowEthnic, colStaDH + (i * 2)).Range.Formula = "=SUMPRODUCT(--(" + UtilsBusiness.GetExcelColumnName(colDanhHieu) + startStaRow + ":" + UtilsBusiness.GetExcelColumnName(colDanhHieu) + endStaRow + "=" + arrCriDH[i] + "),--(E" + startStaRow + ":E" + endStaRow + "<>" + strEmpty + "),--(E" + startStaRow + ":E" + endStaRow + "<>" + ethnic + "),--(A" + startStaRow + ":A" + endStaRow + "=" + statusStudy + "))";
                    sheet.GetRange(rowFemaleEthnic, colStaDH + (i * 2), rowFemaleEthnic, colStaDH + (i * 2)).Range.Formula = "=SUMPRODUCT(--(" + UtilsBusiness.GetExcelColumnName(colDanhHieu) + startStaRow + ":" + UtilsBusiness.GetExcelColumnName(colDanhHieu) + endStaRow + "=" + arrCriDH[i] + "),--(D" + startStaRow + ":D" + endStaRow + "=" + gender + "),--(E" + startStaRow + ":E" + endStaRow + "<>" + strEmpty + "),--(E" + startStaRow + ":E" + endStaRow + "<>" + ethnic + "),--(A" + startStaRow + ":A" + endStaRow + "=" + statusStudy + "))";

                    // Tính cột %
                    sheet.GetRange(rowTotalPupil, colStaDH + (i * 2) + 1, rowTotalPupil, colStaDH + (i * 2) + 1).Range.Formula = "=ROUND(" + UtilsBusiness.GetExcelColumnName(colStaDH + (i * 2)) + rowTotalPupil + "/IF(D" + rowTotalPupil + "<=0,1,D" + rowTotalPupil + "),4)*100";
                    sheet.GetRange(rowFemale, colStaDH + (i * 2) + 1, rowFemale, colStaDH + (i * 2) + 1).Range.Formula = "=ROUND(" + UtilsBusiness.GetExcelColumnName(colStaDH + (i * 2)) + rowFemale + "/IF(D" + rowFemale + "<=0,1,D" + rowFemale + "),4)*100";
                    sheet.GetRange(rowEthnic, colStaDH + (i * 2) + 1, rowEthnic, colStaDH + (i * 2) + 1).Range.Formula = "=ROUND(" + UtilsBusiness.GetExcelColumnName(colStaDH + (i * 2)) + rowEthnic + "/IF(D" + rowEthnic + "<=0,1,D" + rowEthnic + "),4)*100";
                    sheet.GetRange(rowFemaleEthnic, colStaDH + (i * 2) + 1, rowFemaleEthnic, colStaDH + (i * 2) + 1).Range.Formula = "=ROUND(" + UtilsBusiness.GetExcelColumnName(colStaDH + (i * 2)) + rowFemaleEthnic + "/IF(D" + rowFemaleEthnic + "<=0,1,D" + rowFemaleEthnic + "),4)*100";
                }

                #endregion

                sheet.GetRange(cntStudent + 20, colXLHLK, cntStudent + 20, colXepHang).Merge();
                sheet.GetRange(cntStudent + 20, colXLHLK, cntStudent + 20, colXepHang).SetFontStyle(true, System.Drawing.Color.Black, false, 11, false, false);
                sheet.SetCellValue(cntStudent + 20, colXLHLK, "Người lập báo cáo");

                // Ẩn cột A - Trạng thái của sheet
                sheet.HideColumn(1);

                // Set do cao cho row header
                sheet.SetRowHeight(9, 45);

                // Set chiều rộng cho cột TBCM
                sheet.SetColumnWidth(colTBCM, 6);

                // Set chiều rộng cho các cột từ 6 đến 26
                for (int p = 6; p < 27; p++)
                {
                    sheet.SetColumnWidth(p, 6);
                }

                /*sheet.DeleteColumn(19);*/
                int rowDelete = 274;
                for (int w = 0; w < 15; w++)
                {
                    sheet.DeleteRow(rowDelete);
                }

                sheet.Orientation = VTXPageOrientation.VTxlLandscape;
                sheet.FitToPage = true;
            }

            // Xoá sheet template
            firstsheet.Delete();

            return oBook.ToStream();
        }

        #endregion


        #region Lấy mảng băm cho các tham số đầu vào của bảng điểm bình quân cả năm
        /// <summary>
        /// minhh - 22/11/2012
        ///Lấy mảng băm cho các tham số đầu vào của bảng điểm bình quân cả năm
        /// </summary>
        /// <param name="entity"></param>
        /// <returns></returns>
        public string GetHashKeyForAverageAnnualMark(TranscriptOfClass entity)
        {
            IDictionary<string, object> dic = new Dictionary<string, object> {
                {"AcademicYearID", entity.AcademicYearID},
                {"EducationLevelID", entity.EducationLevelID},
                {"ClassID", entity.ClassID}
            };
            return ReportUtils.GetHashKey(dic);
        }
        #endregion

        #region Lưu lại thông tin bảng điểm bình quân cả năm
        /// <summary>
        /// minhh - 22/11/2012
        /// Lưu lại thông tin bảng điểm bình quân cả năm
        /// </summary>
        /// <param name="entity"></param>
        /// <param name="data"></param>
        /// <returns></returns>
        public ProcessedReport InsertAverageAnnualMark(TranscriptOfClass entity, Stream data)
        {
            //  Khởi tạo đối tượng ProcessedReport:
            //- ReportCode : BangDiemBinhQuanCN
            //- ProcessedDate : Ngày hiện tại
            //- InputParameterHashKey : GetHashKeyForAverageAnnualMark(TranscriptOfClass)
            //- ReportData : ReportUtils.Serialize(Data)
            //Insert đối tượng ProcessedReport vào bảng ProcessedReport
            //Insert vào bảng ProcessedReportParameter các bản ghi tương ứng key – value: ClassID,EducationLevelID , AcademicYearID
            string reportCode = SystemParamsInFile.REPORT_BANG_DIEM_BINH_QUAN_CA_NAM;

            ReportDefinition reportDef = ReportDefinitionBusiness.GetByCode(reportCode);
            ProcessedReport pr = new ProcessedReport();
            pr.ReportCode = reportCode;
            pr.ProcessedDate = DateTime.Now;
            pr.InputParameterHashKey = GetHashKeyForAverageAnnualMark(entity);
            pr.ReportData = ReportUtils.Compress(data);

            //Tạo tên file
            string outputNamePattern = reportDef.OutputNamePattern;
            string schoolLevel = ReportUtils.ConvertAppliedLevelForReportName(entity.AppliedLevel);
            string educationLevelOrClass = (entity.ClassID == 0) ?
                EducationLevelRepository.Find(entity.EducationLevelID).Resolution : ClassProfileBusiness.Find(entity.ClassID).DisplayName;

            outputNamePattern = outputNamePattern.Replace("[SchoolLevel]", schoolLevel);
            outputNamePattern = outputNamePattern.Replace("[EducationLevel/Class]", ReportUtils.StripVNSign(educationLevelOrClass));

            pr.ReportName = ReportUtils.RemoveSpecialCharacters(outputNamePattern) + "." + reportDef.OutputFormat;

            IDictionary<string, object> dic = new Dictionary<string, object> {
                {"ClassID", entity.ClassID},
                {"EducationLevelID ", entity.EducationLevelID},
                {"AcademicYearID", entity.AcademicYearID}
            };
            ProcessedReportParameterRepository.Insert(dic, pr);
            ProcessedReportRepository.Insert(pr);
            ProcessedReportRepository.Save();
            return pr;
        }
        #endregion

        #region  Lấy bảng điểm bình quân cả năm được cập nhật mới nhất
        /// <summary>
        /// <author>minhh</author>
        /// Lấy bảng điểm bình quân cả năm được cập nhật mới nhất
        /// </summary>
        /// <param name="entity"></param>
        /// <returns></returns>
        public ProcessedReport GetAverageAnnualMark(TranscriptOfClass entity)
        {
            string reportCode = SystemParamsInFile.REPORT_BANG_DIEM_BINH_QUAN_CA_NAM;
            string inputParameterHashKey = GetHashKeyForAverageAnnualMark(entity);
            return ProcessedReportBusiness.GetProcessedReport(reportCode, inputParameterHashKey);
        }
        #endregion

        #region Lưu lại thông tin bảng điểm bình quân cả năm
        /// <summary>
        /// <author>minhh</author>
        /// Lưu lại thông tin bảng điểm bình quân cả năm
        /// </summary>
        /// <param name="entity"></param>
        /// <returns></returns>
        public Stream CreateAverageAnnualMark(TranscriptOfClass entity)
        {
            string reportCode = SystemParamsInFile.REPORT_BANG_DIEM_BINH_QUAN_CA_NAM;
            ReportDefinition reportDef = ReportDefinitionBusiness.GetByCode(reportCode);
            string templatePath = SystemParamsInFile.TEMPLATE_FOLDER + reportDef.TemplateDirectory + "/" + reportDef.TemplateName;
            IVTWorkbook oBook;
            try
            {
                oBook = VTExport.OpenWorkbook(templatePath);
            }
            catch (Exception)
            {
                throw new BusinessException("File template đang sử dụng.Bạn phải đóng file template");
            }
            //Lấy sheet phiếu điểm
            IVTWorksheet firstSheet = oBook.GetSheet(1);

            //fill dữ liệu vào sheet thông tin chung
            AcademicYear academicYear = AcademicYearBusiness.Find(entity.AcademicYearID);
            SchoolProfile school = academicYear.SchoolProfile;

            string supervisingDeptName = UtilsBusiness.GetSupervisingDeptName(school.SchoolProfileID, entity.AppliedLevel).ToUpper();
            string schoolName = school.SchoolName.ToUpper();
            string provinceName = (school.District != null ? school.District.DistrictName : "");
            string dateTime = "ngày " + DateTime.Now.Date.Day + " tháng " + DateTime.Now.Month + " năm " + DateTime.Now.Year;
            string provinceAndDate = provinceName + ", " + dateTime;

            firstSheet.SetCellValue("A2", supervisingDeptName);
            firstSheet.SetCellValue("A3", schoolName);
            firstSheet.SetCellValue("G4", provinceAndDate);

            //Lấy danh sách các lớp cần in 
            List<ClassProfile> lstClass = new List<ClassProfile>();
            if (entity.ClassID == 0)
            {
                lstClass = ClassProfileBusiness.getClassByAccountRole(entity.AcademicYearID, entity.SchoolID, entity.EducationLevelID, null,
                                                                        entity.UserAccountID, entity.IsSuperVisingDept, entity.IsSubSuperVisingDept).ToList();

            }
            else
            {
                ClassProfile cp = ClassProfileBusiness.Find(entity.ClassID);
                lstClass.Add(cp);
            }
            if (lstClass == null || lstClass.Count() == 0)
            {
                throw new BusinessException("Không có lớp nào trong khối được chọn");
            }
            //Tạo các sheet theo lớp
            Dictionary<ClassProfile, IVTWorksheet> dicSheet = new Dictionary<ClassProfile, IVTWorksheet>();
            foreach (ClassProfile cp in lstClass)
            {
                string classAndAcademic = "Lớp " + cp.DisplayName + " năm học " + academicYear.DisplayTitle;
                firstSheet.SetCellValue("A8", classAndAcademic);
                IVTWorksheet sheet = oBook.CopySheetToBeforeLast(firstSheet);
                sheet.Name =Utils.StripVNSignAndSpace(cp.DisplayName);
                dicSheet.Add(cp, sheet);
            }

            //Lấy danh sách thông tin học sinh nghỉ học
            Dictionary<string, object> dicPupilAbsence = new Dictionary<string, object> 
                                                            {
                                                                {"AcademicYearID", entity.AcademicYearID}, 
                                                                {"EducationLevelID", entity.EducationLevelID},
                                                                {"FromAbsentDate", academicYear.FirstSemesterStartDate},
                                                                {"ToAbsentDate",  academicYear.SecondSemesterEndDate}
                                                            };
            IQueryable<PupilAbsence> iqPupilAbsence = PupilAbsenceBusiness.SearchBySchool(entity.SchoolID, dicPupilAbsence);
            var listPupilAbsence = (from a in iqPupilAbsence
                                    let IsK1 = (a.AbsentDate <= academicYear.FirstSemesterEndDate && a.AbsentDate >= academicYear.FirstSemesterStartDate)
                                    let IsK2 = (a.AbsentDate <= academicYear.SecondSemesterEndDate && a.AbsentDate >= academicYear.SecondSemesterStartDate)
                                    group a by new { a.ClassID, a.PupilID, IsK1, IsK2, a.Section } into grouping
                                    select new
                                    {
                                        //get the range variables from the grouping key
                                        grouping.Key.ClassID,
                                        grouping.Key.PupilID,
                                        grouping.Key.IsK1,
                                        grouping.Key.IsK2,
                                        grouping.Key.Section,
                                        Count = grouping.Count(),
                                    }).ToList();

            //Lấy danh sách thông tin đánh giá học lực
            Dictionary<string, object> dicPupilRanking = new Dictionary<string, object> 
                                                            {
                                                                {"AcademicYearID", entity.AcademicYearID}, 
                                                                {"EducationLevelID", entity.EducationLevelID} 
                                                            };
            List<VPupilRanking> listPupilRanking = VPupilRankingBusiness.SearchBySchool(entity.SchoolID, dicPupilRanking).ToList();

            //Lấy danh sách học sinh
            Dictionary<string, object> dicPupil = new Dictionary<string, object>  {
                                                                {"AcademicYearID", entity.AcademicYearID}, 
                                                                {"EducationLevelID", entity.EducationLevelID},
                                                                {"CheckWithClass", "CheckWithClass"}
                                                            };

            var listPupilOfEdu = PupilOfClassBusiness.SearchBySchool(entity.SchoolID, dicPupil)
                                                    .Select(u => new { u.ClassID, u.PupilID, u.Status, u.PupilProfile.FullName, u.PupilProfile.Name, u.OrderInClass }).ToList();

            var lstPupilOfEduHKI = this.PupilOfClassBusiness.SearchBySchool(entity.SchoolID, new Dictionary<string, object> { 
                                                                                                                       {"AcademicYearID", entity.AcademicYearID}, 
                                                                                                                       {"EducationLevelID", entity.EducationLevelID}                                                                                                                      
            }).AddCriteriaSemester(academicYear, SystemParamsInFile.SEMESTER_OF_YEAR_FIRST).Select(o => new { o.PupilID, o.ClassID }).ToList();
            var lstPupilOfEduHKII = this.PupilOfClassBusiness.SearchBySchool(entity.SchoolID, new Dictionary<string, object> { 
                                                                                                                       {"AcademicYearID", entity.AcademicYearID}, 
                                                                                                                       {"EducationLevelID", entity.EducationLevelID}                                                                                                                   
            }).AddCriteriaSemester(academicYear, SystemParamsInFile.SEMESTER_OF_YEAR_SECOND).Select(o => new { o.PupilID, o.ClassID }).ToList();


            if ((entity.EducationLevelID >= 6 && entity.EducationLevelID <= 12) && (academicYear.IsShowPupil.HasValue && academicYear.IsShowPupil.Value == true))
            {
                List<int> lstPupilRemove = listPupilOfEdu.Where(x => x.Status != SystemParamsInFile.PUPIL_STATUS_GRADUATED 
                                                            && x.Status != SystemParamsInFile.PUPIL_STATUS_STUDYING).Select(x => x.PupilID).ToList();

                listPupilOfEdu = listPupilOfEdu.Where(x => x.Status == SystemParamsInFile.PUPIL_STATUS_GRADUATED
                                            || x.Status == SystemParamsInFile.PUPIL_STATUS_STUDYING).ToList();

                if (lstPupilRemove.Count > 0)
                {
                    lstPupilOfEduHKI = lstPupilOfEduHKI.Where(x => !lstPupilRemove.Contains(x.PupilID)).ToList();
                    lstPupilOfEduHKII = lstPupilOfEduHKII.Where(x => !lstPupilRemove.Contains(x.PupilID)).ToList();
                }
            }

            // Lay toan bo cau hinh ve hoc luc va hanh kiem ra truong (Do 2 bang nay it du lieu)
            List<CapacityLevel> listCapLevel = CapacityLevelBusiness.AllNoTracking.ToList();
            List<ConductLevel> listConductLevel = ConductLevelBusiness.AllNoTracking.ToList();
            //Fill dữ liệu vào các lớp
            List<int> lstSectionKeyID = new List<int>();
            foreach (ClassProfile cp in lstClass)
            {
                lstSectionKeyID = cp.SeperateKey.HasValue ? UtilsBusiness.GetListSectionID(cp.SeperateKey.Value) : new List<int>();
                var listPA = listPupilAbsence.Where(u => u.ClassID == cp.ClassProfileID && lstSectionKeyID.Contains(u.Section)).ToList();
                List<VPupilRanking> listPR = listPupilRanking.Where(u => u.ClassID == cp.ClassProfileID).ToList();

                IVTWorksheet sheet = dicSheet[cp];

                var listPP = listPupilOfEdu.Where(u => u.ClassID == cp.ClassProfileID).OrderBy(o => o.OrderInClass).ThenBy(o => o.Name).ToList();
                var listPPHKI = lstPupilOfEduHKI.Where(u => u.ClassID == cp.ClassProfileID).ToList();
                var listPPHKII = lstPupilOfEduHKII.Where(u => u.ClassID == cp.ClassProfileID).ToList();

                int startRow = 12;
                IVTRange range = firstSheet.GetRange("A12", "N12");
                IVTRange rangeRed = firstSheet.GetRange("A198", "N198");
                IVTRange range5Pupil = firstSheet.GetRange("A200", "N200");
                IVTRange rangeRed5Pupil = firstSheet.GetRange("A202", "N202");

                for (int i = 0; i < listPP.Count; i++)
                {
                    bool isShow = listPPHKII.Any(u => u.PupilID == listPP[i].PupilID);
                    bool isShowHKI = listPPHKI.Any(u => u.PupilID == listPP[i].PupilID);
                    if (startRow == 12)
                    {
                        sheet.CopyPasteSameRowHeigh(isShow ? range : rangeRed, startRow);
                    }
                    else if (i == listPP.Count - 1)
                    {
                        sheet.CopyPasteSameRowHeigh(isShow ? range5Pupil : rangeRed5Pupil, startRow);
                    }
                    else
                    {
                        //Copy row style
                        if (startRow + i > 12)
                        {
                            if ((i + 1) % 5 == 0)
                            {
                                sheet.CopyPasteSameRowHeigh(isShow ? range5Pupil : rangeRed5Pupil, startRow);
                            }
                            else
                            {
                                sheet.CopyPasteSameRowHeigh(isShow ? range : rangeRed, startRow);
                            }
                        }
                    }

                    //STT
                    sheet.SetCellValue(startRow, 1, i + 1);

                    //Ho ten
                    sheet.SetCellValue(startRow, 2, listPP[i].FullName);

                    //TBCM HK1
                    VPupilRanking pr1 = listPR.Where(o => (o.Semester == SystemParamsInFile.SEMESTER_OF_YEAR_FIRST && o.PupilID == listPP[i].PupilID)).FirstOrDefault();
                    if (pr1 != null && isShowHKI)
                    {
                        if (pr1.AverageMark != null)
                        {
                            string averageMark = ReportUtils.ConvertMarkForV(pr1.AverageMark.Value);
                            sheet.SetCellValue(startRow, 3, averageMark);
                        }
                        if (pr1.CapacityLevelID != null && pr1.CapacityLevelID != 0)
                        {
                            string Resolution = UtilsBusiness.ConvertCapacity(listCapLevel.Find(o => o.CapacityLevelID == pr1.CapacityLevelID).CapacityLevel1);
                            sheet.SetCellValue(startRow, 6, Resolution);
                        }
                        if (pr1.ConductLevelID != null && pr1.ConductLevelID != 0)
                        {
                            string Resolution = UtilsBusiness.ConvertCapacity(listConductLevel.Find(o => o.ConductLevelID == pr1.ConductLevelID).Resolution);
                            sheet.SetCellValue(startRow, 9, Resolution);
                        }
                    }

                    //TBCM HK2
                    VPupilRanking pr2 = listPR.Where(o => (o.Semester == SystemParamsInFile.SEMESTER_OF_YEAR_SECOND && o.PupilID == listPP[i].PupilID)).FirstOrDefault();
                    if (pr2 != null && isShow)
                    {
                        if (pr2.AverageMark != null)
                        {
                            string averageMark = ReportUtils.ConvertMarkForV(pr2.AverageMark.Value);
                            sheet.SetCellValue(startRow, 4, averageMark);
                        }
                        if (pr2.CapacityLevelID != null && pr2.CapacityLevelID != 0)
                        {
                            string Resolution = UtilsBusiness.ConvertCapacity(listCapLevel.Find(o => o.CapacityLevelID == pr2.CapacityLevelID).CapacityLevel1);
                            sheet.SetCellValue(startRow, 7, Resolution);
                        }
                        if (pr2.ConductLevelID != null && pr2.ConductLevelID != 0)
                        {
                            string Resolution = UtilsBusiness.ConvertCapacity(listConductLevel.Find(o => o.ConductLevelID == pr2.ConductLevelID).Resolution);
                            sheet.SetCellValue(startRow, 10, Resolution);
                        }
                    }

                    //TBCM CN
                    VPupilRanking pr3 = listPR.Where(o => (o.Semester >= SystemParamsInFile.SEMESTER_OF_YEAR_ALL && o.PupilID == listPP[i].PupilID)).OrderByDescending(o => o.Semester).FirstOrDefault();
                    if (pr3 != null && isShow)
                    {
                        if (pr3.AverageMark != null)
                        {
                            string averageMark = ReportUtils.ConvertMarkForV(pr3.AverageMark.Value);
                            sheet.SetCellValue(startRow, 5, averageMark);
                        }
                        if (pr3.CapacityLevelID != null && pr3.CapacityLevelID != 0)
                        {
                            string Resolution = UtilsBusiness.ConvertCapacity(listCapLevel.Find(o => o.CapacityLevelID == pr3.CapacityLevelID).CapacityLevel1);
                            sheet.SetCellValue(startRow, 8, Resolution);
                        }
                        if (pr3.ConductLevelID != null && pr3.ConductLevelID != 0)
                        {
                            string Resolution = UtilsBusiness.ConvertCapacity(listConductLevel.Find(o => o.ConductLevelID == pr3.ConductLevelID).Resolution);
                            sheet.SetCellValue(startRow, 11, Resolution);
                        }
                    }

                    //SO BUOI NGHI
                    int countAbsent1 = listPA.Where(o => o.PupilID == listPP[i].PupilID && o.IsK1).Count() > 0 ? listPA.Where(o => o.PupilID == listPP[i].PupilID && o.IsK1).Sum(o => o.Count) : 0;
                    int countAbsent2 = listPA.Where(o => o.PupilID == listPP[i].PupilID && o.IsK2).Count() > 0 ? listPA.Where(o => o.PupilID == listPP[i].PupilID && o.IsK2).Sum(o => o.Count) : 0;

                    //SO BUOI NGHI HK1
                    if (isShowHKI)
                    {
                        sheet.SetCellValue(startRow, 12, countAbsent1);
                    }

                    //SO BUOI NGHI HK2
                    if (isShow)
                    {
                        sheet.SetCellValue(startRow, 13, countAbsent2);

                        //SO BUOI NGHI CA NAM
                        sheet.SetCellValue(startRow, 14, countAbsent1 + countAbsent2);
                    }

                    startRow++;
                }

                sheet.DeleteRow(198);
                sheet.DeleteRow(199);
                sheet.DeleteRow(200);
                sheet.FitToPage = true;
            }
            //Xoá sheet template
            firstSheet.Delete();
            return oBook.ToStream();
        }
        #endregion

    }
}
