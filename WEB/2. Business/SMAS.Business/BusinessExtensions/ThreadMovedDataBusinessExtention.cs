﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using SMAS.Models.Models;
using SMAS.Business.Common;
using SMAS.Business.BusinessObject;
using Oracle.DataAccess.Client;
using System.Data;
using SMAS.VTUtils.Log;

namespace SMAS.Business.Business
{
    public partial class ThreadMovedDataBusiness
    {
        public IQueryable<ThreadMovedDataBO> Search(IDictionary<string, object> SearchInfo)
        {
            IQueryable<SchoolProfile> iqSchool = SchoolProfileBusiness.Search(SearchInfo);
            int Year = Utils.GetInt(SearchInfo["YearId"], 0);
            string schoolName = Utils.GetString(SearchInfo["SchoolName"], "");
            string schoolCode = Utils.GetString(SearchInfo["SchoolCode"], "");
            int provinceID = Utils.GetInt(SearchInfo["ProvinceID"], 0);
            int districtID = Utils.GetInt(SearchInfo["DistrictID"], 0);
            int educationGrade = Utils.GetInt(SearchInfo["EducationGrade"], 0);
            string administrator = Utils.GetString(SearchInfo["Administrator"], "");


            //Move ca cac du lieu co nam hoc da xoa
            IQueryable<ThreadMovedDataBO> IQThreadMoveData = (from school in iqSchool
                                                              join ac in AcademicYearBusiness.AllNoTracking.Where(c => c.Year == Year && c.IsActive == true && (c.IsMovedHistory == null || c.IsMovedHistory == 0)) on school.SchoolProfileID equals ac.SchoolID
                                                              join p in ProvinceBusiness.AllNoTracking on school.ProvinceID equals p.ProvinceID
                                                              join d in DistrictBusiness.AllNoTracking on school.DistrictID equals d.DistrictID
                                                              select new ThreadMovedDataBO
                                                              {
                                                                  AcademicYearId = ac.AcademicYearID,
                                                                  Year = ac.Year,
                                                                  DistrictName = d.DistrictName,
                                                                  ProvinceName = p.ProvinceName,
                                                                  EducationGrade = school.EducationGrade,
                                                                  SchoolCode = school.SchoolCode,
                                                                  SchoolName = school.SchoolName,
                                                                  SupervisingDeptName = school.SupervisingDept.SupervisingDeptName,
                                                                  SchoolProfileID = school.SchoolProfileID,
                                                                  UserName = school.UserAccount.aspnet_Users.UserName,
                                                                  ProvinceID = p.ProvinceID,
                                                                  DistrictID = d.DistrictID
                                                              });

            if (!String.IsNullOrEmpty(administrator))
            {
                IQThreadMoveData = IQThreadMoveData.Where(t => t.UserName.Contains(administrator));
            }

            if (!String.IsNullOrEmpty(schoolName))
            {
                IQThreadMoveData = IQThreadMoveData.Where(t => t.SchoolName.Contains(schoolName));
            }
            if (!String.IsNullOrEmpty(schoolCode))
            {
                IQThreadMoveData = IQThreadMoveData.Where(t => t.SchoolCode.Contains(schoolCode));
            }
            if (provinceID > 0)
            {
                IQThreadMoveData = IQThreadMoveData.Where(t => t.ProvinceID == provinceID);
            }
            if (districtID > 0)
            {
                IQThreadMoveData = IQThreadMoveData.Where(t => t.DistrictID == districtID);
            }
            if (educationGrade > 0)
            {
                IQThreadMoveData = IQThreadMoveData.Where(t => t.EducationGrade == educationGrade);
            }
            return IQThreadMoveData;
        }


        public IQueryable<ThreadMovedDataBO> SearchSummedSchool(IDictionary<string, object> SearchInfo)
        {
            IQueryable<SchoolProfile> iqSchool = SchoolProfileBusiness.Search(SearchInfo);
            int Year = Utils.GetInt(SearchInfo["YearId"], 0);
            string schoolName = Utils.GetString(SearchInfo["SchoolName"], "");
            string schoolCode = Utils.GetString(SearchInfo["SchoolCode"], "");
            int provinceID = Utils.GetInt(SearchInfo["ProvinceID"], 0);
            int districtID = Utils.GetInt(SearchInfo["DistrictID"], 0);
            string administrator = Utils.GetString(SearchInfo["Administrator"], "");
            int appliedLevelId = Utils.GetInt(SearchInfo["appliedLevelId"], 0);

            //Move ca cac du lieu co nam hoc da xoa
            IQueryable<ThreadMovedDataBO> IQThreadMoveData = (from school in iqSchool
                                                              join ac in AcademicYearBusiness.AllNoTracking.Where(c => c.Year == Year && c.IsActive == true) on school.SchoolProfileID equals ac.SchoolID
                                                              join p in ProvinceBusiness.AllNoTracking on school.ProvinceID equals p.ProvinceID
                                                              join d in DistrictBusiness.AllNoTracking on school.DistrictID equals d.DistrictID
                                                              select new ThreadMovedDataBO
                                                              {
                                                                  AcademicYearId = ac.AcademicYearID,
                                                                  Year = ac.Year,
                                                                  DistrictName = d.DistrictName,
                                                                  ProvinceName = p.ProvinceName,
                                                                  EducationGrade = school.EducationGrade,
                                                                  SchoolCode = school.SchoolCode,
                                                                  SchoolName = school.SchoolName,
                                                                  SupervisingDeptName = school.SupervisingDept.SupervisingDeptName,
                                                                  SchoolProfileID = school.SchoolProfileID,
                                                                  UserName = school.UserAccount.aspnet_Users.UserName,
                                                                  ProvinceID = p.ProvinceID,
                                                                  DistrictID = d.DistrictID
                                                              });

            if (!String.IsNullOrEmpty(administrator))
            {
                IQThreadMoveData = IQThreadMoveData.Where(t => t.UserName.Contains(administrator));
            }

            if (!String.IsNullOrEmpty(schoolName))
            {
                IQThreadMoveData = IQThreadMoveData.Where(t => t.SchoolName.Contains(schoolName));
            }
            if (!String.IsNullOrEmpty(schoolCode))
            {
                IQThreadMoveData = IQThreadMoveData.Where(t => t.SchoolCode.Contains(schoolCode));
            }
            if (provinceID > 0)
            {
                IQThreadMoveData = IQThreadMoveData.Where(t => t.ProvinceID == provinceID);
            }
            if (districtID > 0)
            {
                IQThreadMoveData = IQThreadMoveData.Where(t => t.DistrictID == districtID);
            }

            if (appliedLevelId == GlobalConstants.APPLIED_LEVEL_SECONDARY || appliedLevelId == GlobalConstants.APPLIED_LEVEL_TERTIARY)
            {
                List<int> lstEducationGrade = UtilsBusiness.GetEducationGrade(GlobalConstants.APPLIED_LEVEL_SECONDARY);
                lstEducationGrade = lstEducationGrade.Union(UtilsBusiness.GetEducationGrade(GlobalConstants.APPLIED_LEVEL_TERTIARY)).Distinct().ToList();
                if (lstEducationGrade != null && lstEducationGrade.Count > 0)
                {
                    IQThreadMoveData = IQThreadMoveData.Where(t => lstEducationGrade.Contains(t.EducationGrade));
                }

            }
            return IQThreadMoveData;
        }

        public int Insert(List<ThreadMovedDataBO> lstThreadMoveData)
        {
            try
            {
                List<ThreadMovedData> lstThreadInsert = new List<ThreadMovedData>();
                // Fix lỗi ATTT
                // if (lstThreadMoveData == null && lstThreadMoveData.Count == 0)
                if (lstThreadMoveData == null || !lstThreadMoveData.Any())
                {
                    return 0;
                }
                context.Configuration.AutoDetectChangesEnabled = false;
                context.Configuration.ValidateOnSaveEnabled = false;
                int retVal = 0;
                ThreadMovedDataBO objThread = null;
                ThreadMovedData objThreadMoveData = null;
                for (int i = 0; i < lstThreadMoveData.Count; i++)
                {
                    objThread = lstThreadMoveData[i];
                    objThreadMoveData = new ThreadMovedData
                    {
                        CreatedDate = DateTime.Now,
                        MoveAcademicYearID = objThread.AcademicYearId,
                        MoveYear = objThread.Year,
                        NodeId = UtilsBusiness.GetPartionId(objThread.SchoolProfileID, 3),
                        NumberScan = 0,
                        SchoolID = objThread.SchoolProfileID,
                        Status = 0,
                        Type = 1
                    };
                    objThreadMoveData.AppliedLevel = UtilsBusiness.JoinPupilId(UtilsBusiness.GetAppliedLevelbyEducationGrade(objThread.EducationGrade));
                    if (!String.IsNullOrEmpty(objThreadMoveData.AppliedLevel))
                    {
                        lstThreadInsert.Add(objThreadMoveData);
                    }
                }
                retVal = BulkInsert(lstThreadInsert, ThreadMovedColumnMappings(), "ThreadMovedDataID");

                if (retVal > 0)
                {
                    List<int> lstAcademicYearId = lstThreadMoveData.Select(c => c.AcademicYearId).Distinct().ToList();
                    //List<AcademicYear> lstAcademicYear = AcademicYearBusiness.AllNoTracking.Where(c => lstAcademicYearId.Contains(c.AcademicYearID)).ToList();
                    /*lstAcademicYear.ForEach(c=>c.IsMovedHistory=2);
                    foreach (var item in lstAcademicYear)
                    {
                        AcademicYearBusiness.Update(item);
                    }*/
                    for (int j = 0; j < lstAcademicYearId.Count; j++)
                    {
                        AcademicYear objAc = AcademicYearBusiness.Find(lstAcademicYearId[j]);
                        objAc.IsMovedHistory = 2;
                        AcademicYearBusiness.Update(objAc);
                    }
                    AcademicYearBusiness.Save();

                }
                return retVal;
            }
            catch (Exception ex)
            {

                LogExtensions.ErrorExt(logger, DateTime.Now, "Insert","null", ex);
                return 0;
            }
            finally
            {
                context.Configuration.AutoDetectChangesEnabled = true;
                context.Configuration.ValidateOnSaveEnabled = true;
            }
        }


        public bool ThreadSummedMarkbyProvince(List<ThreadMovedDataBO> lstThreadMoveData, int semesterId)
        {

            string paramList = "semesterId=" + semesterId;
            try
            {
                if (lstThreadMoveData == null || lstThreadMoveData.Count == 0)
                {
                    return false;
                }

                List<Task> taskMark = new List<Task>();
                for (int j = 0; j < lstThreadMoveData.Count; j++)
                {
                    ThreadMovedDataBO objAcademicYearTask = lstThreadMoveData[j];
                    if (objAcademicYearTask == null)
                    {
                        continue;
                    }

                    taskMark.Add(Task.Run(() =>
                    {
                        SummedUpMark(objAcademicYearTask.SchoolProfileID, objAcademicYearTask.AcademicYearId, semesterId);
                    }));

                }

                if (taskMark.Count > 0)
                {
                    Task.WaitAll(taskMark.ToArray());
                }


                #region "Delete"
                #endregion

                return true;
            }
            catch (Exception ex)
            {

                LogExtensions.ErrorExt(logger, DateTime.Now, "ThreadSummedMarkbyProvince", paramList, ex);
                return false;
            }
            finally
            {
                LogExtensions.InfoExt(logger, LogExtensions.LOG_TYPE_INFO, DateTime.Now, paramList, "", "");
            }

        }

        public bool ThreadDeleteMarkDuplicate(List<ThreadMovedDataBO> lstThreadMoveData, int semesterId)
        {

            string paramList = "semesterId=" + semesterId;
            try
            {
                if (lstThreadMoveData == null || lstThreadMoveData.Count == 0)
                {
                    return false;
                }

                List<Task> taskMark = new List<Task>();
                for (int j = 0; j < lstThreadMoveData.Count; j++)
                {
                    ThreadMovedDataBO objAcademicYearTask = lstThreadMoveData[j];
                    if (objAcademicYearTask == null)
                    {
                        continue;
                    }

                    taskMark.Add(Task.Run(() =>
                    {
                        DelteMarkDuplicate(objAcademicYearTask.SchoolProfileID, objAcademicYearTask.AcademicYearId, semesterId);
                    }));

                }

                if (taskMark.Count > 0)
                {
                    Task.WaitAll(taskMark.ToArray());
                }

                return true;
            }
            catch (Exception ex)
            {

                LogExtensions.ErrorExt(logger, DateTime.Now, "ThreadSummedMarkbyProvince", paramList, ex);
                return false;
            }
            finally
            {
                LogExtensions.InfoExt(logger, LogExtensions.LOG_TYPE_INFO, DateTime.Now, paramList, "", "");
            }

        }

        private IDictionary<string, object> ThreadMovedColumnMappings()
        {
            Dictionary<string, object> columnMap = new Dictionary<string, object>();
            columnMap.Add("ThreadMovedDataID", "THREAD_MOVED_DATA_ID");
            columnMap.Add("SchoolID", "SCHOOL_ID");
            columnMap.Add("MoveAcademicYearID", "MOVE_ACADEMIC_YEAR_ID");
            columnMap.Add("MoveYear", "MOVE_YEAR");
            columnMap.Add("AppliedLevel", "APPLIED_LEVEL");
            columnMap.Add("NodeId", "NODE_ID");
            columnMap.Add("Type", "TYPE");
            columnMap.Add("Status", "STATUS");
            columnMap.Add("CreatedDate", "CREATED_DATE");
            columnMap.Add("ModifiedDate", "MODIFIED_DATE");
            columnMap.Add("NumberScan", "NUMBER_SCAN");

            return columnMap;
        }

        public void SummedUpMark(int schoolId, int academicYearId, int semesterId)
        {
            string connectionString = System.Web.Configuration.WebConfigurationManager.ConnectionStrings["OraAspNetConnectionString"].ToString();
            #region "Task tinh TBM cho toan tinh"
            using (OracleConnection conn = new OracleConnection(connectionString))
            {
                try
                {
                    if (conn != null)
                    {
                        if (conn.State != ConnectionState.Open)
                        {
                            conn.Open();
                        }

                        using (OracleTransaction tran = conn.BeginTransaction())
                        {
                            try
                            {
                                OracleCommand command = new OracleCommand
                                {
                                    CommandType = CommandType.StoredProcedure,
                                    CommandText = "PRO_MARK_SUMMED_DUPLICATE",
                                    Connection = conn,
                                    CommandTimeout = 300
                                };
                                command.Parameters.Add("P_ACADEMIC_YEAR_ID", academicYearId);
                                command.Parameters.Add("P_SCHOOL_ID", schoolId);
                                command.Parameters.Add("P_SEMESTER_ID", semesterId);
                                command.ExecuteNonQuery();
                                tran.Commit();
                            }
                            catch (Exception ex)
                            {
                                string paramList = string.Format("schoolId={0}, academicYearId={1}, semesterId={2}", schoolId, academicYearId, semesterId);
                                LogExtensions.ErrorExt(logger, DateTime.Now, "SummedUpMark", paramList, ex);
                                tran.Rollback();
                            }
                            finally
                            {
                                tran.Dispose();
                            }
                        }
                    }
                }
                finally
                {
                    if (conn != null)
                    {
                        conn.Close();
                    }
                }
            }
            #endregion

        }


        public void DelteMarkDuplicate(int schoolId, int academicYearId, int semesterId)
        {
            string connectionString = System.Web.Configuration.WebConfigurationManager.ConnectionStrings["OraAspNetConnectionString"].ToString();
            #region "Task xoa diem trung cua hoc sinh"

            int classId = 0;
            using (OracleConnection conn = new OracleConnection(connectionString))
            {
                try
                {
                    if (conn != null)
                    {
                        if (conn.State != ConnectionState.Open)
                        {
                            conn.Open();
                        }

                        using (OracleTransaction tran = conn.BeginTransaction())
                        {
                            try
                            {
                                OracleCommand command = new OracleCommand
                                {
                                    CommandType = CommandType.StoredProcedure,
                                    CommandText = "PRO_DELETE_MARK_DUPLICATE",
                                    Connection = conn,
                                    CommandTimeout = 300
                                };
                                command.Parameters.Add("P_ACADEMIC_YEAR_ID", academicYearId);
                                command.Parameters.Add("P_SCHOOL_ID", schoolId);
                                command.Parameters.Add("P_CLASS_ID", classId);
                                command.Parameters.Add("P_SEMESTER", semesterId);
                                command.ExecuteNonQuery();
                                tran.Commit();
                            }
                            catch (Exception ex)
                            {
                                string paramList = string.Format("schoolId={0}, academicYearId={1}, semesterId={2}", schoolId, academicYearId, semesterId);
                                LogExtensions.ErrorExt(logger, DateTime.Now, "DelteMarkDuplicate", paramList,ex);
                                tran.Rollback();
                            }
                            finally
                            {
                                tran.Dispose();
                            }
                        }
                    }
                }
                finally
                {
                    if (conn != null)
                    {
                        conn.Close();
                    }
                }
            }
            #endregion

        }
    }
}
