﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Linq.Expressions;
using System.Web;
using log4net;

using SMAS.DAL.Repository;
using SMAS.DAL.IRepository;
using SMAS.Business.IBusiness;
using SMAS.Models.Models;
using SMAS.Business.BusinessObject;
using SMAS.Business.Common;
using System.IO;
using SMAS.VTUtils.Excel.Export;

namespace SMAS.Business.Business
{
    public class ReportEmployeeByGraduationLevelBusiness : GenericBussiness<ProcessedReport>, IReportEmployeeByGraduationLevelBusiness
    {
        IEmployeeHistoryStatusRepository EmployeeHistoryStatusRepository;
        ITeacherOfFacultyRepository TeacherOfFacultyRepository;
        IEmployeeSalaryRepository EmployeeSalaryRepository;
        IEmployeeQualificationRepository EmployeeQualificationRepository;
        IProcessedReportParameterRepository ProcessedReportParameterRepository;
        IProcessedReportRepository ProcessedReportRepository;

        public ReportEmployeeByGraduationLevelBusiness(ILog logger, SMASEntities context = null)
            : base(logger)
        {
            if (context == null) { context = new SMASEntities(); } this.context = context;
            this.EmployeeHistoryStatusRepository = new EmployeeHistoryStatusRepository(context);
            this.TeacherOfFacultyRepository = new TeacherOfFacultyRepository(context);
            this.EmployeeSalaryRepository = new EmployeeSalaryRepository(context);
            this.EmployeeQualificationRepository = new EmployeeQualificationRepository(context);
            this.ProcessedReportParameterRepository = new ProcessedReportParameterRepository(context);
            this.ProcessedReportRepository = new ProcessedReportRepository(context);
        }

        #region Lấy mảng băm cho các tham số đầu vào
        public string GetHashKey(ReportEmployeeByGraduationLevelBO reportEmployeeByGraduationLevel)
        {
            IDictionary<string, object> Dictionary = new Dictionary<string, object>();
            Dictionary["SchoolID"] = reportEmployeeByGraduationLevel.SchoolID;
            Dictionary["ReportDate"] = reportEmployeeByGraduationLevel.ReportDate;
            Dictionary["FacultyID"] = reportEmployeeByGraduationLevel.FacultyID;
            Dictionary["AppliedLevel"] = reportEmployeeByGraduationLevel.AppliedLevel;
            Dictionary["GraduationLevelID"] = reportEmployeeByGraduationLevel.GraduationLevelID != null ? reportEmployeeByGraduationLevel.GraduationLevelID : 0;
            Dictionary["SpecialityCatID"] = reportEmployeeByGraduationLevel.SpecialityCatID != null ? reportEmployeeByGraduationLevel.SpecialityCatID : 0;
            return ReportUtils.GetHashKey(Dictionary);
        }
        #endregion

        #region Lưu lại thông tin thống kê cán bộ theo chuyên ngành đào tạo
        public ProcessedReport InsertReportEmployeeByGraduationLevel(ReportEmployeeByGraduationLevelBO entity, Stream data)
        {
            string reportCode = SystemParamsInFile.REPORT_DANH_SACH_CAN_BO_THEO_CHUYEN_NGANH;
            ReportDefinition reportDef = ReportDefinitionBusiness.GetByCode(reportCode);
            ProcessedReport pr = new ProcessedReport();
            pr.ReportCode = reportCode;
            pr.ProcessedDate = DateTime.Now;
            pr.InputParameterHashKey = GetHashKey(entity);
            pr.ReportData = ReportUtils.Compress(data);

            //Tạo tên file GV_[SchoolLevel]_BCGVTheoTDCM
            string outputNamePattern = reportDef.OutputNamePattern;
            string schoolLevel = ReportUtils.ConvertAppliedLevelForReportName(entity.AppliedLevel);
            outputNamePattern = outputNamePattern.Replace("[SchoolLevel]", schoolLevel);
            pr.ReportName = ReportUtils.RemoveSpecialCharacters(outputNamePattern) + "." + reportDef.OutputFormat;

            IDictionary<string, object> dic = new Dictionary<string, object> {
                {"SchoolID", entity.SchoolID},
                {"ReportDate", entity.ReportDate},
                {"FacultyID", entity.FacultyID},
                {"AppliedLevel", entity.AppliedLevel},
                {"GraduationLevelID", entity.GraduationLevelID != null ? entity.GraduationLevelID : 0},
                {"SpecialityCatID", entity.SpecialityCatID != null ? entity.SpecialityCatID : 0},
            };
            ProcessedReportParameterRepository.Insert(dic, pr);
            ProcessedReportRepository.Insert(pr);
            ProcessedReportRepository.Save();
            return pr;
        }
        #endregion

        #region Lấy báo cáo danh sách cán bộ theo chuyên ngành đào tạo được cập nhật mới nhất
        public ProcessedReport GetReportEmployeeByGraduationLevel(ReportEmployeeByGraduationLevelBO entity)
        {
            string reportCode = SystemParamsInFile.REPORT_DANH_SACH_CAN_BO_THEO_CHUYEN_NGANH;
            string inputParameterHashKey = GetHashKey(entity);
            return ProcessedReportBusiness.GetProcessedReport(reportCode, inputParameterHashKey);
        }
        #endregion

        #region Tạo báo cáo danh sách cán bộ theo chuyên ngành đào tạo
        /// <summary>
        /// 
        /// </summary>
        /// <param name="entity"></param>
        /// <returns></returns>
        public Stream CreateReportEmployeeByGraduationLevel(ReportEmployeeByGraduationLevelBO entity)
        {

            string reportCode = SystemParamsInFile.REPORT_DANH_SACH_CAN_BO_THEO_CHUYEN_NGANH;
            ReportDefinition reportDef = ReportDefinitionBusiness.GetByCode(reportCode);
            string templatePath = SystemParamsInFile.TEMPLATE_FOLDER + reportDef.TemplateDirectory + "/" + reportDef.TemplateName;
            IVTWorkbook oBook = VTExport.OpenWorkbook(templatePath);

            int firstRow = 9;
            int lastCol = 9;
            //Lấy sheet template
            IVTWorksheet firstSheet = oBook.GetSheet(1);

            //Tạo template chuẩn
            IVTWorksheet tempSheet = oBook.CopySheetToLast(firstSheet, "I" + (firstRow - 1));
            tempSheet.CopyPasteSameSize(firstSheet.GetRange(firstRow, 1, firstRow + 6, lastCol), "A" + firstRow);

            //Fill dữ liệu chung cho template

            SchoolProfile school = SchoolProfileBusiness.Find(entity.SchoolID);
            Province province = school.Province;
            string schoolName = school.SchoolName.ToUpper();
            string supervisingDeptName = UtilsBusiness.GetSupervisingDeptName(school.SchoolProfileID, entity.AppliedLevel).ToUpper();
            string provinceName = (school.District != null ? school.District.DistrictName : "");
            DateTime reportDate = DateTime.Now;//entity.ReportDate;
            string date = entity.ReportDate.ToShortDateString();
            string graduationLevel = "";
            string speciality = "";

            //Sua lai GraduationLevel => TrainingLevel
            if (entity.GraduationLevelID == 0)
            {
                graduationLevel = "[Tất cả]";
            }
            else
            {

                TrainingLevel gra = TrainingLevelBusiness.Find(entity.GraduationLevelID);
                if (gra != null)
                {
                    graduationLevel = gra.Resolution;
                }
            }
            if (entity.SpecialityCatID == 0)
            {
                speciality = "[Tất cả]";
            }
            else
            {
                SpecialityCat spe = SpecialityCatBusiness.Find(entity.SpecialityCatID);
                if (spe != null)
                {
                    speciality = spe.Resolution;
                }
            }
            Dictionary<string, object> dicGeneralInfo = new Dictionary<string, object> 
                {
                    {"SchoolName", schoolName}, 
                    {"DeptName", supervisingDeptName},
                    {"ProvinceName", provinceName},
                    {"ReportDate", reportDate},
                    {"Date", date},
                    {"GraduationLevel", graduationLevel},
                    {"Speciality", speciality},
                };

            tempSheet.GetRange(1, 1, firstRow - 1, lastCol).FillVariableValue(dicGeneralInfo);

            //Tạo sheet Fill dữ liệu
            IVTWorksheet dataSheet = oBook.CopySheetToLast(tempSheet,
                new VTVector(firstRow - 1, lastCol).ToString());

            //Tạo vùng cho từng khối
            IVTRange range = tempSheet.GetRange(firstRow, 1, firstRow, lastCol);
            IVTRange rangeHeadTeacher = tempSheet.GetRange(firstRow + 3, 1, firstRow + 3, lastCol);
            IVTRange rangeDateTime = tempSheet.GetRange(firstRow + 2, 1, firstRow + 2, lastCol);
            //Lấy dữ liệu
            IQueryable<EmployeeQualification> iqEmpQualify = EmployeeQualificationRepository.All;
            var iqEmloyee = from ehs in EmployeeHistoryStatusRepository.All
                            join em in EmployeeBusiness.All on ehs.EmployeeID equals em.EmployeeID
                            join tof in TeacherOfFacultyRepository.All on ehs.EmployeeID equals tof.TeacherID
                            where em.IsActive == true && ehs.SchoolID == entity.SchoolID
                            && tof.SchoolFaculty.SchoolID == entity.SchoolID
                                //&& ehs.EmployeeStatus == SystemParamsInFile.EMPLOYMENT_STATUS_WORKING
                            && (tof.FacultyID == entity.FacultyID || entity.FacultyID == 0)
                            && ehs.FromDate <= entity.ReportDate
                            && (ehs.ToDate == null || ehs.ToDate >= entity.ReportDate)
                                //&& ehs.Employee.AppliedLevel == entity.AppliedLevel
                            && (em.TrainingLevelID == entity.GraduationLevelID || entity.GraduationLevelID == 0)
                            && (em.SpecialityCatID == entity.SpecialityCatID || entity.SpecialityCatID == 0)
                            select em;
            EmployeeQualification empQualify = new EmployeeQualification();
            int empID;
            if (iqEmloyee.Count() > 0)
            {
                List<object> listData = new List<object>();
                List<Employee> lstEmployee = iqEmloyee.ToList();
                lstEmployee = lstEmployee.OrderBy(o => o.Name).ThenBy(o => o.FullName).ToList();
                Dictionary<string, object> dicData = new Dictionary<string, object>();
                for (int i = 0; i < lstEmployee.Count; i++)
                {
                    dataSheet.CopyPasteSameSize(range, firstRow + i, 1);
                    dicData = new Dictionary<string, object>();
                    dicData["Order"] = i + 1;
                    dicData["FullName"] = lstEmployee[i].FullName;
                    dicData["BirthDate"] = lstEmployee[i].BirthDate != null ? lstEmployee[i].BirthDate.Value.ToShortDateString() : "";
                    dicData["Genre"] = lstEmployee[i].Genre == true ? "Nam" : "Nữ";
                    SchoolFaculty faculty = lstEmployee[i].SchoolFaculty;
                    dicData["FacultyName"] = faculty != null ? faculty.FacultyName : string.Empty;
                    dicData["GraduationLevel"] = lstEmployee[i].TrainingLevel != null ? lstEmployee[i].TrainingLevel.Resolution : "";
                    dicData["Speciality"] = lstEmployee[i].SpecialityCat != null ? lstEmployee[i].SpecialityCat.Resolution : "";
                    empID = lstEmployee[i].EmployeeID;
                    empQualify = iqEmpQualify.Where(o => o.EmployeeID == empID).OrderByDescending(o => o.FromDate).FirstOrDefault();
                    dicData["QualifiedAt"] = empQualify != null ? empQualify.QualifiedAt : "";
                    dicData["Description"] = empQualify != null ? empQualify.Description : "";
                    listData.Add(dicData);
                }
                dataSheet.GetRange(firstRow, 1, firstRow + lstEmployee.Count, lastCol).FillVariableValue(new Dictionary<string, object>
                    {
                        {"list",listData}
                    });
            }
            //fill dòng  hiệu trưởng
            dataSheet.CopyPasteSameSize(rangeDateTime, firstRow + iqEmloyee.Count() + 1, 1);
            dataSheet.GetRange(firstRow + iqEmloyee.Count() + 1, 1, firstRow + iqEmloyee.Count() + 1, lastCol).FillVariableValue(dicGeneralInfo);
            dataSheet.CopyPasteSameSize(rangeHeadTeacher, firstRow + iqEmloyee.Count() + 2, 1);
            dataSheet.FitToPage = true;
            //Xoá sheet template
            firstSheet.Delete();
            tempSheet.Delete();

            return oBook.ToStream();
        }
        #endregion

        #region Lưu lại thông tin thống kê cán bộ đạt và vượt chuẩn
        public ProcessedReport InsertReportEmployeeAboveStandard(ReportEmployeeByGraduationLevelBO entity, Stream data)
        {
            string reportCode = SystemParamsInFile.REPORT_DANH_SACH_CAN_BO_DAT_VA_VUOT_CHUAN;
            ReportDefinition reportDef = ReportDefinitionBusiness.GetByCode(reportCode);
            ProcessedReport pr = new ProcessedReport();
            pr.ReportCode = reportCode;
            pr.ProcessedDate = DateTime.Now;
            pr.InputParameterHashKey = GetHashKey(entity);
            pr.ReportData = ReportUtils.Compress(data);

            //Tạo tên file GV_[SchoolLevel]_BCCanBoDatChuanVaVuotChuan
            string outputNamePattern = reportDef.OutputNamePattern;
            string schoolLevel = ReportUtils.ConvertAppliedLevelForReportName(entity.AppliedLevel);
            outputNamePattern = outputNamePattern.Replace("[SchoolLevel]", schoolLevel);
            pr.ReportName = ReportUtils.RemoveSpecialCharacters(outputNamePattern) + "." + reportDef.OutputFormat;

            IDictionary<string, object> dic = new Dictionary<string, object> {
                {"SchoolID", entity.SchoolID},
                {"ReportDate", entity.ReportDate},
                {"FacultyID", entity.FacultyID},
                {"AppliedLevel", entity.AppliedLevel},
            };
            ProcessedReportParameterRepository.Insert(dic, pr);
            ProcessedReportRepository.Insert(pr);
            ProcessedReportRepository.Save();
            return pr;
        }
        #endregion

        #region Lấy báo cáo danh sách cán bộ đạt và vượt chuẩn được cập nhật mới nhất
        public ProcessedReport GetReportEmployeeAboveStandard(ReportEmployeeByGraduationLevelBO entity)
        {
            string reportCode = SystemParamsInFile.REPORT_DANH_SACH_CAN_BO_DAT_VA_VUOT_CHUAN;
            string inputParameterHashKey = GetHashKey(entity);
            return ProcessedReportBusiness.GetProcessedReport(reportCode, inputParameterHashKey);
        }
        #endregion

        #region Tạo báo cáo danh sách cán bộ đạt và vượt chuẩn
        /// <summary>
        /// 
        /// </summary>
        /// <param name="entity"></param>
        /// <returns></returns>
        public Stream CreateReportEmployeeAboveStandard(ReportEmployeeByGraduationLevelBO entity)
        {

            string reportCode = SystemParamsInFile.REPORT_DANH_SACH_CAN_BO_DAT_VA_VUOT_CHUAN;
            ReportDefinition reportDef = ReportDefinitionBusiness.GetByCode(reportCode);
            string templatePath = SystemParamsInFile.TEMPLATE_FOLDER + reportDef.TemplateDirectory + "/" + reportDef.TemplateName;
            IVTWorkbook oBook = VTExport.OpenWorkbook(templatePath);

            int firstRow = 10;
            int lastCol = 9;
            //Lấy sheet template
            IVTWorksheet firstSheet = oBook.GetSheet(1);

            //Tạo template chuẩn
            IVTWorksheet tempSheet = oBook.CopySheetToLast(firstSheet, "I" + (firstRow - 1));
            tempSheet.CopyPasteSameSize(firstSheet.GetRange(firstRow, 1, firstRow + 6, lastCol), "A" + firstRow);

            //Fill dữ liệu chung cho template

            SchoolProfile school = SchoolProfileBusiness.Find(entity.SchoolID);
            Province province = school.Province;
            string schoolName = school.SchoolName.ToUpper();
            string supervisingDeptName = UtilsBusiness.GetSupervisingDeptName(school.SchoolProfileID, entity.AppliedLevel).ToUpper();
            string provinceName = (school.District != null ? school.District.DistrictName : "");
            DateTime reportDate = DateTime.Now;//entity.ReportDate;
            string date = entity.ReportDate.ToShortDateString();

            Dictionary<string, object> dicGeneralInfo = new Dictionary<string, object> 
                {
                    {"SchoolName", schoolName}, 
                    {"DeptName", supervisingDeptName},
                    {"ProvinceName", provinceName},
                    {"ReportDate", reportDate},
                    {"Date", date},
                };

            tempSheet.GetRange(1, 1, firstRow - 1, lastCol).FillVariableValue(dicGeneralInfo);

            //Tạo sheet Fill dữ liệu
            IVTWorksheet dataSheet = oBook.CopySheetToLast(tempSheet,
                new VTVector(firstRow - 1, lastCol).ToString());

            //Tạo vùng cho từng khối
            IVTRange range = tempSheet.GetRange(firstRow, 1, firstRow, lastCol);
            IVTRange rangeHeadTeacher = tempSheet.GetRange(firstRow + 3, 1, firstRow + 3, lastCol);
            IVTRange rangeDateTime = tempSheet.GetRange(firstRow + 2, 1, firstRow + 2, lastCol);

            //Lấy dữ liệu
            IQueryable<EmployeeQualification> iqEmpQualify = EmployeeQualificationRepository.All;
            var iqEmloyee = from ehs in EmployeeHistoryStatusRepository.All
                            join tof in TeacherOfFacultyRepository.All on ehs.EmployeeID equals tof.TeacherID
                            join em in EmployeeBusiness.All on ehs.EmployeeID equals em.EmployeeID
                            where em.IsActive == true && ehs.SchoolID == entity.SchoolID
                            && tof.SchoolFaculty.SchoolID == entity.SchoolID
                                //&& ehs.EmployeeStatus == SystemParamsInFile.EMPLOYMENT_STATUS_WORKING
                            && (tof.FacultyID == entity.FacultyID || entity.FacultyID == 0)
                            && ehs.FromDate <= entity.ReportDate
                            && (ehs.ToDate == null || ehs.ToDate >= entity.ReportDate)
                                //&& em.AppliedLevel == entity.AppliedLevel
                            && em.GraduationLevel.FullfilmentStatus >= SystemParamsInFile.FULLFILLMENT_STATUS_STANDARD
                            select em;
            EmployeeQualification empQualify = new EmployeeQualification();
            int empID;
            if (iqEmloyee.Count() > 0)
            {
                List<object> listData = new List<object>();
                List<Employee> lstEmployee = iqEmloyee.ToList();
                lstEmployee = lstEmployee.OrderBy(o => o.Name).ThenBy(o => o.FullName).ToList();
                Dictionary<string, object> dicData = new Dictionary<string, object>();
                for (int i = 0; i < lstEmployee.Count; i++)
                {
                    dataSheet.CopyPasteSameSize(range, firstRow + i, 1);
                    dicData = new Dictionary<string, object>();
                    dicData["Order"] = i + 1;
                    dicData["FullName"] = lstEmployee[i].FullName;
                    dicData["BirthDate"] = lstEmployee[i].BirthDate != null ? lstEmployee[i].BirthDate.Value.ToShortDateString() : "";
                    dicData["Genre"] = lstEmployee[i].Genre == true ? "Nam" : "Nữ";
                    SchoolFaculty faculty = lstEmployee[i].SchoolFaculty;
                    dicData["FacultyName"] = faculty != null ? faculty.FacultyName : string.Empty;
                    // Trinh do dao tao
                    dicData["GraduationLevel"] = lstEmployee[i].TrainingLevel != null ? lstEmployee[i].TrainingLevel.Resolution : ""; 
                    //  GraduationLevel != null ? lstEmployee[i].GraduationLevel.Resolution : "";
                    dicData["Speciality"] = (lstEmployee[i].SpecialityCatID != null && lstEmployee[i].SpecialityCat != null) ? lstEmployee[i].SpecialityCat.Resolution : "";
                    empID = lstEmployee[i].EmployeeID;
                    empQualify = iqEmpQualify.Where(o => o.EmployeeID == empID).OrderByDescending(o => o.FromDate).FirstOrDefault();
                    dicData["QualifiedAt"] = empQualify != null ? empQualify.QualifiedAt : "";
                    dicData["Description"] = !(String.IsNullOrEmpty(lstEmployee[i].Description)) ? lstEmployee[i].Description : "";
                    listData.Add(dicData);
                }
                dataSheet.GetRange(firstRow, 1, firstRow + lstEmployee.Count, lastCol).FillVariableValue(new Dictionary<string, object>
                    {
                        {"list",listData}
                    });
            }
            //fill dòng hiệu trưởng
            dataSheet.CopyPasteSameSize(rangeDateTime, firstRow + iqEmloyee.Count() + 1, 1);
            dataSheet.GetRange(firstRow + iqEmloyee.Count() + 1, 1, firstRow + iqEmloyee.Count() + 1, lastCol).FillVariableValue(dicGeneralInfo);
            dataSheet.CopyPasteSameSize(rangeHeadTeacher, firstRow + iqEmloyee.Count() + 2, 1);
            //Xoá sheet template
            dataSheet.FitToPage = true;
            firstSheet.Delete();
            tempSheet.Delete();

            return oBook.ToStream();
        }
        #endregion

        #region Lưu lại thông tin thống kê cán bộ dưới chuẩn
        public ProcessedReport InsertReportEmployeeUnderStandard(ReportEmployeeByGraduationLevelBO entity, Stream data)
        {
            string reportCode = SystemParamsInFile.REPORT_DANH_SACH_CAN_BO_DUOI_CHUAN;
            ReportDefinition reportDef = ReportDefinitionBusiness.GetByCode(reportCode);
            ProcessedReport pr = new ProcessedReport();
            pr.ReportCode = reportCode;
            pr.ProcessedDate = DateTime.Now;
            pr.InputParameterHashKey = GetHashKey(entity);
            pr.ReportData = ReportUtils.Compress(data);

            //Tạo tên file GV_[SchoolLevel]_BCCanBoDuoiChuan
            string outputNamePattern = reportDef.OutputNamePattern;
            string schoolLevel = ReportUtils.ConvertAppliedLevelForReportName(entity.AppliedLevel);
            outputNamePattern = outputNamePattern.Replace("[SchoolLevel]", schoolLevel);
            pr.ReportName = ReportUtils.RemoveSpecialCharacters(outputNamePattern) + "." + reportDef.OutputFormat;

            IDictionary<string, object> dic = new Dictionary<string, object> {
                {"SchoolID", entity.SchoolID},
                {"ReportDate", entity.ReportDate},
                {"FacultyID", entity.FacultyID},
                {"AppliedLevel", entity.AppliedLevel},
            };
            ProcessedReportParameterRepository.Insert(dic, pr);
            ProcessedReportRepository.Insert(pr);
            ProcessedReportRepository.Save();
            return pr;
        }
        #endregion

        #region Lấy báo cáo danh sách cán bộ dưới chuẩn được cập nhật mới nhất
        public ProcessedReport GetReportEmployeeUnderStandard(ReportEmployeeByGraduationLevelBO entity)
        {
            string reportCode = SystemParamsInFile.REPORT_DANH_SACH_CAN_BO_DUOI_CHUAN;
            string inputParameterHashKey = GetHashKey(entity);
            return ProcessedReportBusiness.GetProcessedReport(reportCode, inputParameterHashKey);
        }
        #endregion

        #region Tạo báo cáo danh sách cán bộ dưới chuẩn
        /// <summary>
        /// 
        /// </summary>
        /// <param name="entity"></param>
        /// <returns></returns>
        public Stream CreateReportEmployeeUnderStandard(ReportEmployeeByGraduationLevelBO entity)
        {

            string reportCode = SystemParamsInFile.REPORT_DANH_SACH_CAN_BO_DUOI_CHUAN;
            ReportDefinition reportDef = ReportDefinitionBusiness.GetByCode(reportCode);
            string templatePath = SystemParamsInFile.TEMPLATE_FOLDER + reportDef.TemplateDirectory + "/" + reportDef.TemplateName;
            IVTWorkbook oBook = VTExport.OpenWorkbook(templatePath);

            int firstRow = 10;
            int lastCol = 9;
            //Lấy sheet template
            IVTWorksheet firstSheet = oBook.GetSheet(1);

            //Tạo template chuẩn
            IVTWorksheet tempSheet = oBook.CopySheetToLast(firstSheet, "I" + (firstRow - 1));
            tempSheet.CopyPasteSameSize(firstSheet.GetRange(firstRow, 1, firstRow + 6, lastCol), "A" + firstRow);

            //Fill dữ liệu chung cho template

            SchoolProfile school = SchoolProfileBusiness.Find(entity.SchoolID);
            Province province = school.Province;
            string schoolName = school.SchoolName.ToUpper();
            string supervisingDeptName = UtilsBusiness.GetSupervisingDeptName(school.SchoolProfileID, entity.AppliedLevel).ToUpper();
            string provinceName = (school.District != null ? school.District.DistrictName : "");
            DateTime reportDate = DateTime.Now;//entity.ReportDate;
            string date = entity.ReportDate.ToShortDateString();

            Dictionary<string, object> dicGeneralInfo = new Dictionary<string, object> 
                {
                    {"SchoolName", schoolName}, 
                    {"DeptName", supervisingDeptName},
                    {"ProvinceName", provinceName},
                    {"ReportDate", reportDate},
                    {"Date", date},
                };

            tempSheet.GetRange(1, 1, firstRow - 1, lastCol).FillVariableValue(dicGeneralInfo);

            //Tạo sheet Fill dữ liệu
            IVTWorksheet dataSheet = oBook.CopySheetToLast(tempSheet,
                new VTVector(firstRow - 1, lastCol).ToString());

            //Tạo vùng cho từng khối
            IVTRange midrange = tempSheet.GetRange(firstRow, 1, firstRow, lastCol);
            IVTRange lastrange = tempSheet.GetRange(firstRow + 1, 1, firstRow + 1, lastCol);
            IVTRange rangeHeadTeacher = tempSheet.GetRange(firstRow + 4, 1, firstRow + 4, lastCol);
            IVTRange rangeDateTime = tempSheet.GetRange(firstRow + 3, 1, firstRow + 3, lastCol);

            //Lấy dữ liệu
            IQueryable<EmployeeQualification> iqEmpQualify = EmployeeQualificationRepository.All;
            var iqEmloyee = from ehs in EmployeeHistoryStatusRepository.All
                            join em in EmployeeBusiness.All on ehs.EmployeeID equals em.EmployeeID
                            join tof in TeacherOfFacultyRepository.All on ehs.EmployeeID equals tof.TeacherID
                            where em.IsActive == true && ehs.SchoolID == entity.SchoolID
                            && tof.SchoolFaculty.SchoolID == entity.SchoolID
                                //&& ehs.EmployeeStatus == SystemParamsInFile.EMPLOYMENT_STATUS_WORKING
                            && (tof.FacultyID == entity.FacultyID || entity.FacultyID == 0)
                            && ehs.FromDate <= entity.ReportDate
                            && (ehs.ToDate == null || ehs.ToDate >= entity.ReportDate)
                                //&& em.AppliedLevel == entity.AppliedLevel
                            && em.GraduationLevel.FullfilmentStatus == SystemParamsInFile.FULLFILLMENT_STATUS_UNDER_STANDARD
                            select em;
            EmployeeQualification empQualify = new EmployeeQualification();
            int empID;
            if (iqEmloyee.Count() > 0)
            {
                List<object> listData = new List<object>();
                List<Employee> lstEmployee = iqEmloyee.ToList();
                lstEmployee = lstEmployee.OrderBy(o => o.Name).ThenBy(o => o.FullName).ToList();
                Dictionary<string, object> dicData = new Dictionary<string, object>();
                for (int i = 0; i < lstEmployee.Count; i++)
                {
                    if (i != lstEmployee.Count - 1)
                    {
                        dataSheet.CopyPasteSameSize(midrange, firstRow + i, 1);
                    }
                    else
                    {
                        dataSheet.CopyPasteSameSize(lastrange, firstRow + i, 1);
                    }
                    dicData = new Dictionary<string, object>();
                    dicData["Order"] = i + 1;
                    dicData["FullName"] = lstEmployee[i].FullName;
                    dicData["BirthDate"] = lstEmployee[i].BirthDate != null ? lstEmployee[i].BirthDate.Value.ToShortDateString() : "";
                    dicData["Genre"] = lstEmployee[i].Genre == true ? "Nam" : "Nữ";
                    SchoolFaculty faculty = lstEmployee[i].SchoolFaculty;
                    dicData["FacultyName"] = faculty != null ? faculty.FacultyName : string.Empty;
                    dicData["GraduationLevel"] = lstEmployee[i].GraduationLevel != null ? lstEmployee[i].GraduationLevel.Resolution : "";
                    dicData["Speciality"] = lstEmployee[i].SpecialityCat != null ? lstEmployee[i].SpecialityCat.Resolution : "";
                    empID = lstEmployee[i].EmployeeID;
                    empQualify = iqEmpQualify.Where(o => o.EmployeeID == empID).OrderByDescending(o => o.FromDate).FirstOrDefault();
                    dicData["QualifiedAt"] = empQualify != null ? empQualify.QualifiedAt : "";
                    dicData["Description"] = empQualify != null ? empQualify.Description : "";
                    listData.Add(dicData);
                }
                dataSheet.GetRange(firstRow, 1, firstRow + lstEmployee.Count, lastCol).FillVariableValue(new Dictionary<string, object>
                    {
                        {"list",listData}
                    });
            }
            //fill dòng  hiệu trưởng
            dataSheet.CopyPasteSameSize(rangeDateTime, firstRow + iqEmloyee.Count() + 1, 1);
            dataSheet.GetRange(firstRow + iqEmloyee.Count() + 1, 1, firstRow + iqEmloyee.Count() + 1, lastCol).FillVariableValue(dicGeneralInfo);
            dataSheet.CopyPasteSameSize(rangeHeadTeacher, firstRow + iqEmloyee.Count() + 2, 1);
            //Xoá sheet template
            dataSheet.FitToPage = true;
            firstSheet.Delete();
            tempSheet.Delete();

            return oBook.ToStream();
        }
        #endregion
    }
}
