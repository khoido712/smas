﻿/**
 * The Viettel License
 *
 * Copyright 2012 Viettel Telecom. All rights reserved.
 * VIETTEL PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */

/**
* @author
* @version $Revision: $
*/

using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using SMAS.Business.BusinessObject;
using SMAS.Business.Common;
using SMAS.DAL.Repository;
using SMAS.Models.Models;
using SMAS.VTUtils.Excel.Export;

namespace SMAS.Business.Business
{
    /// <summary>
    /// sổ gọi tên và ghi điểm THCS
    /// </summary>
    /// <author>
    /// DungNT77
    /// </author>
    /// <remarks>
    /// 21/11/2012   2:17 PM
    /// </remarks>
    public partial class MasterBookBusiness
    {

        /// <summary>
        /// Xuất báo cáo cho cấp 2
        /// </summary>
        /// <param name="entity"></param>
        /// <returns></returns>
        private Stream CreateMasterBookAppliedLevelSecondary(MasterBook entity, string suffixName = null)
        {
            int partitionId = UtilsBusiness.GetPartionId(entity.SchoolID);
            #region lay cac sheet tu template

            string reportCode = SystemParamsInFile.REPORT_THCS_INSOGOITENVAGHIDIEM;
            ReportDefinition reportDef = ReportDefinitionBusiness.GetByCode(reportCode);
            string templatePath = SystemParamsInFile.TEMPLATE_FOLDER + reportDef.TemplateDirectory + "/" + reportDef.TemplateName;
            if (!string.IsNullOrWhiteSpace(suffixName))
            {
                // Nếu là hcm thì lấy template khác
                templatePath = SystemParamsInFile.TEMPLATE_FOLDER + reportDef.TemplateDirectory + "/" + string.Format("{0}-{1}{2}", Path.GetFileNameWithoutExtension(reportDef.TemplateName), suffixName, Path.GetExtension(reportDef.TemplateName));
            }
            IVTWorkbook oBook = VTExport.OpenWorkbook(templatePath);

            //lấy các sheet ra:
            IVTWorksheet sheetBia = oBook.GetSheet(2);
            IVTWorksheet sheetSoYeuLyLich = oBook.GetSheet(4);
            IVTWorksheet sheetDiemDanh = oBook.GetSheet(6);
            IVTWorksheet sheetBiaGhiDiemHK1 = oBook.GetSheet(8);
            IVTWorksheet sheetDiemHK1 = oBook.GetSheet(10);
            IVTWorksheet sheetDiemHK1_2 = oBook.GetSheet(12);
            IVTWorksheet sheetDiemHK1mtc = oBook.GetSheet(14);
            IVTWorksheet sheetDiemTKHK1 = oBook.GetSheet(16);
            IVTWorksheet sheetBiaGhiDiemHK2 = oBook.GetSheet(18);
            IVTWorksheet sheetDiemHK2 = oBook.GetSheet(20);
            IVTWorksheet sheetDiemHK2_2 = oBook.GetSheet(22);
            IVTWorksheet sheetDiemHK2mtc = oBook.GetSheet(24);
            IVTWorksheet sheetDiemTKHK2 = oBook.GetSheet(26);
            IVTWorksheet sheetBiaTKCaNam = oBook.GetSheet(28);
            IVTWorksheet sheetTongKetCaNam = oBook.GetSheet(30);
            IVTWorksheet sheetDiemThiLai = oBook.GetSheet(31);
            IVTWorksheet sheetNhanXetHT_HK1 = oBook.GetSheet(32);
            IVTWorksheet sheetNhanXetHT_HK2 = oBook.GetSheet(33);
            IVTWorksheet sheetGiaovienBM = oBook.GetSheet(34);
            //Sheet Template
            IVTWorksheet sheetDiemHKITemp = oBook.GetSheet(9);
            IVTWorksheet sheetDiemHKI_2Temp = oBook.GetSheet(11);
            IVTWorksheet sheetDiem_HKI_MTCTemp = oBook.GetSheet(13);
            IVTWorksheet sheetDiemTKHKITemp = oBook.GetSheet(15);
            IVTWorksheet sheetBiaGhiDiemHK2Temp = oBook.GetSheet(17);
            IVTWorksheet sheetDiemHK2Temp = oBook.GetSheet(19);
            IVTWorksheet sheetDiemHK2_2Temp = oBook.GetSheet(21);
            IVTWorksheet sheetDiemHK2mtcTemp = oBook.GetSheet(23);
            IVTWorksheet sheetDiemTKHK2Temp = oBook.GetSheet(25);
            IVTWorksheet sheetBiaTKCaNamTemp = oBook.GetSheet(27);
            IVTWorksheet sheetTongKetCaNamTemp = oBook.GetSheet(29);
            //So mon thi lai
            int countSubjectRetest = 0;
            #endregion
            #region fill du lieu sheet bia
            //entity chua cac truong classID, AcademicYearID, SchoolID, EducationLevelID, Semester, AppliedLevel
            //Thoong tin ve so phong
            SchoolProfile sp = SchoolProfileBusiness.Find(entity.SchoolID);
            ClassProfile cp = ClassProfileBusiness.Find(entity.ClassID);
            AcademicYear academicYear = AcademicYearBusiness.Find(entity.AcademicYearID);
            Employee employee = new Employee();
            string HeadTeacherName = "";
            if (cp.HeadTeacherID > 0)
            {
                employee = EmployeeBusiness.Find(cp.HeadTeacherID);
                HeadTeacherName = employee.FullName;
            }
            bool isSoHCM = false;

            SupervisingDept supervisingDept = sp.SupervisingDept;
            if (supervisingDept.Province.ProvinceName.Trim().ToUpper() == GlobalConstants.Province_HCM)
            {
                isSoHCM = true;
            }
            string supervisingDeptName = supervisingDept.SupervisingDeptName;
            int? parentID = SupervisingDeptBusiness.Find(supervisingDept.SupervisingDeptID).ParentID;
            string supervisingDeptNameParent = "SỞ GIÁO DỤC & ĐÀO TẠO .......................";
            if (parentID > 0)
            {
                SupervisingDept sDeptParent = SupervisingDeptBusiness.Find(parentID.Value);
                if (sDeptParent.SupervisingDeptName.Substring(0, 3).ToUpper().Contains("SỞ"))
                {
                    supervisingDeptNameParent = sDeptParent.SupervisingDeptName.ToUpper();
                }
                else if (supervisingDeptName.Substring(0, 5).ToUpper().Contains("SỞ"))
                {
                    supervisingDeptNameParent = supervisingDeptName.ToUpper();
                }
            }
            if (string.IsNullOrWhiteSpace(suffixName))
            {
                sheetBia.SetCellValue("A3", supervisingDeptNameParent);
                //Phòng giáo dục & đào tạo
                string superPGD = "";
                if (supervisingDeptName.Substring(0, 5).ToUpper().Contains("PHÒNG"))
                {
                    superPGD = supervisingDeptName.ToUpper();
                }
                sheetBia.SetCellValue("A4", superPGD);
            }
            else
            {
                //Lay district
                sheetBia.SetCellValue("A2", "PHÒNG GIÁO DỤC VÀ ĐÀO TẠO");
                sheetBia.SetCellValue("A3", sp.District.DistrictName.ToUpper());
            }
            //Thong tin ve ten truong
            string schoolName = sp.SchoolName.Trim();
            //string startSchoolName = schoolName.Substring(0, 6).ToUpper();
            if (Utils.StripVNSignAndSpace(schoolName).ToUpper().StartsWith("TRUONG"))
            {
                schoolName = schoolName.Remove(0, 6).Trim().ToUpper();
            }
            sheetBia.SetCellValue("A28", "TRƯỜNG " + schoolName.ToUpper());
            //Xa, huyen, tinh, lop, nam hoc
            string communeName = sp.Commune != null ? sp.Commune.CommuneName : "....................";
            string districtName = sp.District != null ? sp.District.DistrictName : "...................";
            string provinceName = sp.Province != null ? sp.Province.ProvinceName : ".....................";
            string className = cp.DisplayName;
            string academicYearTitle = academicYear.Year.ToString();
            string academicYearTitle1 = (academicYear.Year + 1).ToString();

            sheetBia.SetCellValue("E34", communeName);
            sheetBia.SetCellValue("P34", districtName);
            sheetBia.SetCellValue("H35", provinceName);
            sheetBia.SetCellValue("E36", className);
            sheetBia.SetCellValue("N36", academicYearTitle);
            sheetBia.SetCellValue("P36", academicYearTitle1);
            //Giao vien chu nhiem, hieu truong
            string employeeName = employee != null ? employee.FullName : string.Empty;
            sheetBia.SetCellValue("B56", employeeName);
            string headMasterName = sp.HeadMasterName;
            sheetBia.SetCellValue("O56", headMasterName);
            #endregion
            #region fill du lieu sheet so yeu li lich
            //set style cho hoc sinh dang hoc, da tot nghiep / chuyen truong, chuyen lop, thoi hoc
            IVTWorksheet sheetSYLLTemp = oBook.GetSheet(3);
            IVTRange rangeStudyingStyle = sheetSYLLTemp.GetRange("A5", "M5");
            IVTRange rangeNotStudyingStyle = sheetSYLLTemp.GetRange("A4", "M4");
            IVTRange rangeStudygingStyleDevide5 = sheetSYLLTemp.GetRange("A13", "M13");
            IVTRange rangeNotStudyingStyleDevide5 = sheetSYLLTemp.GetRange("A8", "M8");
            //Kiểm tra có phải là sở hồ chí minh hay không?

            //Lay danh sach hoc sinh
            IDictionary<string, object> dic = new Dictionary<string, object>();
            dic["AcademicYearID"] = entity.AcademicYearID;
            dic["ClassID"] = entity.ClassID;
            dic["CheckWithClass"] = "checkWithClass";

            IQueryable<PupilOfClass> lstPoc = PupilOfClassBusiness.SearchBySchool(entity.SchoolID, dic);
            List<PupilOfClass> lstPocHK1 = new List<PupilOfClass>();
            List<PupilOfClass> lstPocHK2 = new List<PupilOfClass>();

            if (entity.Semester == SystemParamsInFile.SEMESTER_OF_YEAR_FIRST)
            {
                lstPocHK1 = lstPoc.AddCriteriaSemester(academicYear, SystemParamsInFile.SEMESTER_OF_YEAR_FIRST)
                    .ToList()
                    .OrderBy(p => p.OrderInClass).ThenBy(p => p.PupilProfile.Name).ThenBy(p => p.PupilProfile.FullName)
                    .ToList();
            }
            else if (entity.Semester == SystemParamsInFile.SEMESTER_OF_YEAR_SECOND)
            {
                lstPocHK2 = lstPoc.AddCriteriaSemester(academicYear, SystemParamsInFile.SEMESTER_OF_YEAR_SECOND)
                    .ToList()
                    .OrderBy(p => p.OrderInClass).ThenBy(p => p.PupilProfile.Name).ThenBy(p => p.PupilProfile.FullName)
                    .ToList();
            }
            else
            {
                lstPocHK1 = lstPoc.AddCriteriaSemester(academicYear, SystemParamsInFile.SEMESTER_OF_YEAR_FIRST)
                    .ToList()
                    .OrderBy(p => p.OrderInClass).ThenBy(p => p.PupilProfile.Name).ThenBy(p => p.PupilProfile.FullName)
                    .ToList();
                lstPocHK2 = lstPoc.AddCriteriaSemester(academicYear, SystemParamsInFile.SEMESTER_OF_YEAR_SECOND)
                    .ToList()
                    .OrderBy(p => p.OrderInClass).ThenBy(p => p.PupilProfile.Name).ThenBy(p => p.PupilProfile.FullName)
                    .ToList();
            }

            IQueryable<PupilProfileBO> lsPoC = from p in lstPoc
                                               join q in PupilProfileBusiness.All on p.PupilID equals q.PupilProfileID
                                               select new PupilProfileBO
                                               {
                                                   PupilProfileID = p.PupilID,
                                                   PupilCode = q.PupilCode,
                                                   FullName = q.FullName,
                                                   EthnicID = q.EthnicID,
                                                   CommuneName = q.Commune.CommuneName,
                                                   EthnicName = q.Ethnic.EthnicName,
                                                   DistrictName = q.District.DistrictName,
                                                   ProvinceName = q.Province.ProvinceName,
                                                   BirthDate = q.BirthDate,
                                                   BirthPlace = q.BirthPlace,
                                                   PolicyTargetName = q.PolicyTarget.Resolution,
                                                   PermanentResidentalAddress = q.PermanentResidentalAddress,
                                                   TempResidentalAddress = q.TempResidentalAddress,
                                                   FatherFullName = q.FatherFullName,
                                                   MotherFullName = q.MotherFullName,
                                                   SponsorFullName = q.SponsorFullName,
                                                   FatherJob = q.FatherJob,
                                                   MotherJob = q.MotherJob,
                                                   SponsorJob = q.SponsorJob,
                                                   Genre = q.Genre,
                                                   Name = q.Name,
                                                   OrderInClass = p.OrderInClass,
                                                   ProfileStatus = p.Status,
                                                   AssignedDate = p.AssignedDate,
                                                   EndDate = p.EndDate
                                               };

            List<PupilProfileBO> lsPP = lsPoC.ToList().OrderBy(o => o.OrderInClass).ThenBy(o => o.Name).ThenBy(p => p.FullName).ToList();
            //Dem so hoc sinh trong lop
            int countPP = lsPP.Count();
            int countStudying = lsPP.Where(o => o.ProfileStatus == SystemParamsInFile.PUPIL_STATUS_STUDYING || o.ProfileStatus == SystemParamsInFile.PUPIL_STATUS_GRADUATED).Count();
            int startRow = 4;
            for (int i = 0; i < countPP; i++)
            {
                PupilProfileBO pp = lsPP[i];
                // Dien Style
                if (pp.ProfileStatus == SystemParamsInFile.PUPIL_STATUS_STUDYING || pp.ProfileStatus == SystemParamsInFile.PUPIL_STATUS_GRADUATED)
                {
                    if (i % 5 == 4)
                    {
                        sheetSoYeuLyLich.CopyPaste(rangeStudygingStyleDevide5, i + 4, 1);
                    }
                    else
                    {
                        sheetSoYeuLyLich.CopyPaste(rangeStudyingStyle, i + 4, 1);
                    }
                }
                else
                {
                    if (i % 5 == 4)
                    {
                        sheetSoYeuLyLich.CopyPaste(rangeNotStudyingStyleDevide5, i + 4, 1);
                    }
                    else
                    {
                        sheetSoYeuLyLich.CopyPaste(rangeNotStudyingStyle, i + 4, 1);
                    }
                }
                //Dien du lieu
                sheetSoYeuLyLich.SetCellValue("A" + startRow, i + 1);
                sheetSoYeuLyLich.SetCellValue("J" + startRow, i + 1);
                sheetSoYeuLyLich.SetCellValue("B" + startRow, pp.FullName);
                sheetSoYeuLyLich.SetCellValue("C" + startRow, String.Format("{0:dd/MM/yyyy}", pp.BirthDate));
                if (pp.BirthPlace != null)
                {
                    sheetSoYeuLyLich.SetCellValue("D" + startRow, pp.BirthPlace);
                }
                else
                {
                    string birthPlace = "";
                    if (pp.CommuneName != null && pp.CommuneName.Trim() != "")
                    {
                        birthPlace = pp.CommuneName + ", " + pp.DistrictName + ", " + pp.ProvinceName;
                    }
                    else if (pp.DistrictName != null && pp.DistrictName.Trim() != "")
                    {
                        birthPlace = pp.DistrictName + ", " + pp.ProvinceName;
                    }
                    else
                    {
                        birthPlace = pp.ProvinceName;
                    }
                    sheetSoYeuLyLich.SetCellValue("D" + startRow, birthPlace);
                }
                sheetSoYeuLyLich.SetCellValue("E" + startRow, SMASConvert.ConvertGenre(pp.Genre));
                sheetSoYeuLyLich.SetCellValue("F" + startRow, pp.EthnicName);
                sheetSoYeuLyLich.SetCellValue("G" + startRow, pp.PolicyTargetName);
                if (pp.TempResidentalAddress != null && pp.TempResidentalAddress.Trim() != "")
                {
                    sheetSoYeuLyLich.SetCellValue("H" + startRow, pp.TempResidentalAddress);
                }
                else
                {
                    sheetSoYeuLyLich.SetCellValue("H" + startRow, pp.PermanentResidentalAddress);
                }
                string fatherName = lsPP[i].FatherFullName;
                string motherName = lsPP[i].MotherFullName;
                string sponsorName = lsPP[i].SponsorFullName;
                string fatherOrSponsor = "";
                string motherOrSponsor = "";
                if (fatherName != null && fatherName != "" && fatherName.Trim().Length > 0)
                {
                    fatherOrSponsor = fatherName + "-Nghề nghiệp: " + pp.FatherJob;
                }
                else if (sponsorName != null && sponsorName != "" && sponsorName.Trim().Length > 0)
                {
                    fatherOrSponsor = sponsorName + "-Nghề nghiệp: " + pp.SponsorJob;
                }

                if (motherName != null && motherName != "" && motherName.Trim().Length > 0)
                {
                    motherOrSponsor = motherName + "-Nghề nghiệp: " + pp.MotherJob;
                }
                else if (sponsorName != null && sponsorName != "" && sponsorName.Trim().Length > 0)
                {
                    motherOrSponsor = sponsorName + "-Nghề nghiệp: " + pp.SponsorJob;
                }
                sheetSoYeuLyLich.SetCellValue("K" + startRow, fatherOrSponsor);
                sheetSoYeuLyLich.SetCellValue("L" + startRow, motherOrSponsor);
                startRow++;
            }

            #endregion
            #region Fill du lieu sheet diem danh
            //Lấy style cho những học sinh chuyển trường, chuyển lớp, thôi học/ Đang học, dã tốt nghiệp
            IVTWorksheet sheetDiemDanhTemplate = oBook.GetSheet(5);
            //IVTRange rangeStudyingDiemDanh = sheetDiemDanhTemplate.GetRange("A8", "AJ8");
            //IVTRange rangeNotStudyingDiemDanh = sheetDiemDanhTemplate.GetRange("A7", "AJ7");
            //IVTRange rangeStudyingDevideDiemDanh = sheetDiemDanhTemplate.GetRange("A9", "AJ9");
            //IVTRange rangeNotStudyingDevideDiemDanh = sheetDiemDanhTemplate.GetRange("A11", "AJ11");
            //IVTRange cellBlank = sheetDiemDanhTemplate.GetRange("A12", "AJ12");

            IVTRange temDiemDanh = sheetDiemDanhTemplate.GetRange("A1", "AJ75");
            IVTRange strikeThroughRange = sheetDiemDanhTemplate.GetRange("A8", "AJ8");
            IVTRange strikeThrouthRangeWithBottom = sheetDiemDanhTemplate.GetRange("A11", "AJ11");
            IVTRange cellBlank = sheetDiemDanhTemplate.GetRange("A9", "AJ9");
            IVTRange cellBlankWithBottom = sheetDiemDanhTemplate.GetRange("A16", "AJ16");
            // Tannd
            // Du lieu diem danh la du lieu diem danh trong buoi hoc chinh cua lop
            // Buoi hoc chinh cua lop duoc dinh nghia la buoi hoc dau tien trong ngay co du lieu diem danh
            List<int> lstSectionKeyID = cp.SeperateKey.HasValue ? UtilsBusiness.GetListSectionID(cp.SeperateKey.Value) : new List<int>();
            // Danh sach nghi hoc cua cac hoc sinh trong lop
            var query = PupilAbsenceBusiness
                .SearchBySchool(entity.SchoolID, new Dictionary<string, object>()
                {
                        {"ClassID",entity.ClassID},
                        {"AcademicYearID",entity.AcademicYearID},
                        {"EducationLevelID",entity.EducationLevelID}
                })
                .Where(p => lstSectionKeyID.Contains(p.Section));// Chi lay cac du lieu diem danh trong cac buoi hoc chinh

            //var temp = query.OrderBy(p => p.Section).FirstOrDefault(); // Lay buoi hoc chinh la buoi co du lieu diem danh va co section nho nhat
            //int mainSectionOfClass = temp != null ? temp.Section : sectionOfClass.Count > 0 ? sectionOfClass[0] : -1;

            List<PupilAbsence> iqPA = query.ToList(); // Danh sach diem danh trong buoi hoc chinh

            int FirstSemesterStartmonth = academicYear.FirstSemesterStartDate.Value.Month;
            int FirstSemesterStartyear = academicYear.FirstSemesterStartDate.Value.Year;
            //neu ki 2 thi chi hien thi diem danh ki 2
            if (entity.Semester == SystemParamsInFile.SEMESTER_OF_YEAR_SECOND)
            {
                FirstSemesterStartmonth = academicYear.SecondSemesterStartDate.Value.Month;
                FirstSemesterStartyear = academicYear.SecondSemesterStartDate.Value.Year;
            }
            int SecondSemesterEndDatemonth = academicYear.SecondSemesterEndDate.Value.Month;
            int SecondSemesterEndDateyear = academicYear.SecondSemesterEndDate.Value.Year;
            //neu ki 1 thi chi hien thi diem danh ki 1
            if (entity.Semester == SystemParamsInFile.SEMESTER_OF_YEAR_FIRST)
            {
                SecondSemesterEndDatemonth = academicYear.FirstSemesterEndDate.Value.Month;
                SecondSemesterEndDateyear = academicYear.FirstSemesterEndDate.Value.Year;
            }
            int pupilcount = lsPoC.Count();
            string strpupilCount = "Tổng số học sinh của lớp: " + pupilcount;
            sheetDiemDanhTemplate.SetCellValue("AA2", strpupilCount);

            //neu so hoc sinh > 60 thi them
            //if (pupilcount > 60)
            //{
            //    for (int i = 0; i < pupilcount - 60; i++)
            //    {
            //        sheetDiemDanhTemplate.CopyAndInsertARow(66, 66 + i + 1);
            //        sheetDiemDanhTemplate.SetCellValue("A" + (66 + i + 1), 60 + i + 1);
            //    }
            //}

            //copy
            int totalRow = 74;
            if (SecondSemesterEndDateyear > FirstSemesterStartyear)
            {
                SecondSemesterEndDatemonth += (SecondSemesterEndDateyear - FirstSemesterStartyear) * 12;
            }
            Dictionary<string, object> dicDiemDanh = new Dictionary<string, object>();
            List<object> insideDic = new List<object>();
            int countP = 0;
            int countK = 0;
            string result = string.Empty;

            for (int i = 0; i < SecondSemesterEndDatemonth - FirstSemesterStartmonth + 1; i++)
            {

                dicDiemDanh = new Dictionary<string, object>();
                insideDic = new List<object>();
                sheetDiemDanh.CopyPasteSameRowHeigh(temDiemDanh, totalRow * i + 1, 1);
                sheetDiemDanh.FitAllColumnsOnOnePage = true;

                int nextFirstRow = totalRow * i + 1;
                sheetDiemDanh.SetBreakPage(nextFirstRow);
                int currentMonth = FirstSemesterStartmonth + i;
                int currentYear = FirstSemesterStartyear;
                if (currentMonth > 12)
                {
                    currentYear += 1;
                    currentMonth -= 12;
                }
                string monthYear = "Tháng {0} năm {1}";
                string strMY = string.Format(monthYear, new string[] { currentMonth.ToString(), currentYear.ToString() });
                sheetDiemDanh.SetCellValue("A" + (totalRow * i + 2), strMY);
                if (i == 1)
                {
                    IVTRange rangeDD2 = sheetDiemDanh.GetRange("A" + (totalRow * i + 72), "A" + (totalRow * i + 72));
                    sheetDiemDanh.SetCellValue("A" + (totalRow * i + 72), "2-SGTGĐ THCS");
                    rangeDD2.SetFontStyle(true, null, false, 11, false, false);
                }
                if (i == 5)
                {
                    IVTRange rangeDD3 = sheetDiemDanh.GetRange("A" + (totalRow * i + 72), "A" + (totalRow * i + 72));
                    sheetDiemDanh.SetCellValue("A" + (totalRow * i + 72), "3-SGTGĐ THCS");
                    rangeDD3.SetFontStyle(true, null, false, 11, false, false);
                }
                //truyen dictionary du lieu vao template truoc khi copy
                //fill du lieu hoc sinh
                //Điền thông tin vào sheet
                //${Rows[].get(FullName)},${Rows[].get(i)},${Rows[].get(Total)},${Rows[].get(P)},${Rows[].get(K)},${Date1}
                DateTime firstDayofMonth = new DateTime(currentYear, currentMonth, 1);
                DateTime lastDayOfMonth = new DateTime(currentYear, currentMonth, DateTime.DaysInMonth(currentYear, currentMonth));

                List<PupilAbsence> lsPAthisMonth = iqPA.Where(o => o.AbsentDate >= firstDayofMonth && o.AbsentDate <= lastDayOfMonth).ToList();
                List<PupilAbsence> lstPAtmp = new List<PupilAbsence>();
                List<string> lstDataTime = lsPAthisMonth.Select(p => p.AbsentDate.ToString()).Distinct().ToList();

                #region dien thong tin diem danh cho tung hs

                #endregion
                for (int j = 0; j < lsPP.Count(); j++)
                {
                    //int column = j + 7;
                    //sheetDiemDanhTemplate.SetCellValue("B" + column, lsPP[j].FullName);
                    PupilProfileBO pp = lsPP[j];
                    if (pp.ProfileStatus == SystemParamsInFile.PUPIL_STATUS_STUDYING || pp.ProfileStatus == SystemParamsInFile.PUPIL_STATUS_GRADUATED)
                    {
                        if (j % 5 == 4)
                        {
                            sheetDiemDanh.CopyPasteSameColumnWidth(cellBlankWithBottom, 6 + nextFirstRow + j, 1, true);
                            //sheetDiemDanh.CopyPasteSameSize(rangeStudyingDevideDiemDanh, 6 + nextFirstRow + j, 1);
                        }
                        else
                        {
                            sheetDiemDanh.CopyPasteSameColumnWidth(cellBlank, 6 + nextFirstRow + j, 1, true);
                            //sheetDiemDanh.CopyPasteSameSize(rangeStudyingDiemDanh, 6 + nextFirstRow + j, 1);
                        }
                    }
                    else
                    {
                        if (j % 5 == 4)
                        {
                            sheetDiemDanh.CopyPasteSameColumnWidth(strikeThrouthRangeWithBottom, 6 + nextFirstRow + j, 1, true);
                            //sheetDiemDanh.CopyPasteSameSize(rangeNotStudyingDevideDiemDanh, 6 + nextFirstRow + j, 1);
                        }
                        else
                        {
                            sheetDiemDanh.CopyPasteSameColumnWidth(strikeThroughRange, 6 + nextFirstRow + j, 1, true);
                            //sheetDiemDanh.CopyPasteSameSize(rangeNotStudyingDiemDanh, 6 + nextFirstRow + j, 1);
                        }
                    }
                    Dictionary<string, object> innerDic = new Dictionary<string, object>();
                    innerDic.Add("FullName", pp.FullName);
                    int pupilID = pp.PupilProfileID;
                    innerDic.Add("Order", j + 1);
                    bool showPupilData = true;
                    bool showPupilData1 = true;
                    bool showPupilData2 = true;
                    if (entity.Semester == SystemParamsInFile.SEMESTER_OF_YEAR_FIRST)
                    {
                        showPupilData = lstPocHK1.Where(o => o.PupilID == pupilID).Count() > 0 ? true : false;
                        showPupilData1 = showPupilData;
                    }
                    else if (entity.Semester == SystemParamsInFile.SEMESTER_OF_YEAR_SECOND)
                    {
                        showPupilData = lstPocHK2.Where(o => o.PupilID == pupilID).Count() > 0 ? true : false;
                        showPupilData2 = showPupilData;
                    }
                    else
                    {
                        showPupilData1 = lstPocHK1.Where(o => o.PupilID == pupilID).Count() > 0 ? true : false;
                        showPupilData2 = lstPocHK2.Where(o => o.PupilID == pupilID).Count() > 0 ? true : false;
                    }
                    if (showPupilData == true)
                    {
                        List<PupilAbsence> lsThisPupilAbsence = lsPAthisMonth.Where(o => o.PupilID == pupilID).ToList();
                        for (int k = 0; k < lstDataTime.Count; k++)
                        {
                            DateTime absencetime = Convert.ToDateTime(lstDataTime[k]);
                            lstPAtmp = lsThisPupilAbsence.Where(o => o.AbsentDate == absencetime).ToList();
                            countP = lstPAtmp.Where(p => p.IsAccepted.HasValue && p.IsAccepted.Value).Count();
                            countK = lstPAtmp.Where(p => p.IsAccepted.HasValue && !p.IsAccepted.Value).Count();
                            if (countP > 0 && countK > 0)
                            {
                                result = countP + "P" + countK + "K";
                            }
                            else if (countP > 0 && countK == 0)
                            {
                                if (countP > 1)
                                {
                                    result = countP + "P";
                                }
                                else
                                {
                                    result = "P";
                                }
                            }
                            else if (countP == 0 && countK > 0)
                            {
                                if (countK > 1)
                                {
                                    result = countK + "K";
                                }
                                else
                                {
                                    result = "K";
                                }
                            }
                            else
                            {
                                result = "";
                            }
                            if (showPupilData1 && absencetime >= academicYear.FirstSemesterStartDate && absencetime <= academicYear.FirstSemesterEndDate)
                            {
                                if (pp.ProfileStatus == SystemParamsInFile.PUPIL_STATUS_STUDYING
                                                    || pp.ProfileStatus == SystemParamsInFile.PUPIL_STATUS_GRADUATED)
                                {
                                    innerDic[absencetime.Day.ToString()] = result;
                                }
                                else
                                {
                                    if (absencetime >= academicYear.FirstSemesterStartDate && absencetime <= academicYear.FirstSemesterEndDate)
                                    {
                                        innerDic[absencetime.Day.ToString()] = result;
                                    }
                                }
                            }
                            if (showPupilData2 && absencetime >= academicYear.SecondSemesterStartDate && absencetime <= academicYear.SecondSemesterEndDate)
                            {
                                if (pp.ProfileStatus == SystemParamsInFile.PUPIL_STATUS_STUDYING
                                                    || pp.ProfileStatus == SystemParamsInFile.PUPIL_STATUS_GRADUATED)
                                {
                                    innerDic[absencetime.Day.ToString()] = result;
                                }
                                else
                                {
                                    if (absencetime >= academicYear.SecondSemesterStartDate && absencetime <= academicYear.SecondSemesterEndDate)
                                    {
                                        innerDic[absencetime.Day.ToString()] = result;
                                    }
                                }
                            }

                        }

                        //Xét học kỳ, trạng thái
                        if (pp.ProfileStatus == SystemParamsInFile.PUPIL_STATUS_STUDYING
                                                || pp.ProfileStatus == SystemParamsInFile.PUPIL_STATUS_GRADUATED)
                        {
                            innerDic.Add("TS", lsThisPupilAbsence.Count());
                            innerDic.Add("P", lsThisPupilAbsence.Where(o => o.IsAccepted == true).Count());
                            innerDic.Add("K", lsThisPupilAbsence.Where(o => o.IsAccepted == false).Count());
                        }
                        else
                        {
                            if (showPupilData1)
                            {
                                innerDic.Add("TS", lsThisPupilAbsence.Where(o => o.AbsentDate >= academicYear.FirstSemesterStartDate && o.AbsentDate <= academicYear.FirstSemesterEndDate).Count());
                                innerDic.Add("P", lsThisPupilAbsence.Where(o => o.IsAccepted == true && o.AbsentDate >= academicYear.FirstSemesterStartDate && o.AbsentDate <= academicYear.FirstSemesterEndDate).Count());
                                innerDic.Add("K", lsThisPupilAbsence.Where(o => o.IsAccepted == false && o.AbsentDate >= academicYear.FirstSemesterStartDate && o.AbsentDate <= academicYear.FirstSemesterEndDate).Count());
                            }
                            else
                            {
                                innerDic.Add("TS", "");
                                innerDic.Add("P", "");
                                innerDic.Add("K", "");
                            }
                        }
                    }
                    //
                    insideDic.Add(innerDic);
                }

                //for (int k = lsPP.Count(); k <= 5; k++) // Xoa cac dong thua khi so hoc sinh be hon 5
                //{
                //    sheetDiemDanh.CopyPasteSameSize(cellBlank, 6 + nextFirstRow + k, 1);
                //    sheetDiemDanh.SetCellValue("A" + (6 + nextFirstRow + k), k + 1);
                //}

                //sheetDiemDanh.GetRange("A" + (6 + nextFirstRow + 5), "AJ" + (6 + nextFirstRow + 5)).SetBorder(VTBorderStyle.Solid, VTBorderIndex.EdgeBottom);


                //dien thong tin ngay trong tuan
                dicDiemDanh.Add("Rows", insideDic);
                Dictionary<string, object> dateDic = new Dictionary<string, object>();
                for (int day = 1; day <= DateTime.DaysInMonth(currentYear, currentMonth); day++)
                {
                    Dictionary<string, object> tempDic = new Dictionary<string, object>();
                    DateTime thisDate = new DateTime(currentYear, currentMonth, day);
                    dateDic[day.ToString()] = GetDateInWeekString(thisDate);
                }
                dicDiemDanh["Date"] = dateDic;

                //copy vao template
                sheetDiemDanh.FillVariableValue(dicDiemDanh);
                //dien ten giao vien CN
                //sheetDiemDanh.SetCellValue(totalRow * (i + 1) - 1, 30, HeadTeacherName);
                //sheetDiemDanh.CopyPasteSameRowHeigh(temDiemDanh, nextFirstRow);
                //sheetDiemDanh.SetCellValue("A" + (totalRow * i + 2), strMY);
                int rowHead = totalRow * (i + 1) - 1;
                sheetDiemDanh.MergeRow(rowHead, 30, 36);
                IVTRange rangHead = sheetDiemDanh.GetRange(rowHead, 30, rowHead, 36);
                rangHead.SetFontStyle(true, null, false, null, false, false);
                rangHead.SetHAlign(VTHAlign.xlHAlignCenter);
                sheetDiemDanh.SetCellValue(rowHead, 30, HeadTeacherName);
            }
            //Bo sung sheet cuoi cung vao khi so trang diem danh la chan de dam bao in 2 mat
            if ((SecondSemesterEndDatemonth - FirstSemesterStartmonth + 1) % 2 == 0)
            {

                int row = 74 * (SecondSemesterEndDatemonth - FirstSemesterStartmonth + 1);
                sheetDiemDanh.CopyPaste(temDiemDanh, row + 1, 1);

                int nextFirstRow = row + 1;
                sheetDiemDanh.SetBreakPage(nextFirstRow);

                //sheetDiemDanh.SetBreakPage(row + 1);
                sheetDiemDanh.SetCellValue(row + 2, 27, "Tổng số học sinh của lớp: ");
                Dictionary<string, object> dicDiemDanh1 = new Dictionary<string, object>();
                sheetDiemDanh.FillVariableValue(dicDiemDanh1);
            }
            #endregion

            #region Lay du lieu

            List<TeachingAssignmentBO> lstTeachingBo = GetTeachingAssinment(entity);

            string nameNN = "NGOẠI NGỮ: ";
            string subjectNN = "";
            string nameTuChon = ".................................";
            string nameTinHoc = "TIN HỌC";
            string nameNN2 = "";

            Dictionary<string, object> searchClass = new Dictionary<string, object>();
            searchClass["ClassID"] = entity.ClassID;
            searchClass["AcademicYearID"] = entity.AcademicYearID;
            searchClass["SchoolID"] = entity.SchoolID;
            //searchClass["Year"] = academicYear.Year; //AnhVD 20131218
            //IQueryable<MarkRecord> iqMarkRecord = MarkRecordBusiness.SearchBySchool(entity.SchoolID, searchClass);
            // IQueryable<JudgeRecord> iqJudgeRecord = JudgeRecordBusiness.SearchBySchool(entity.SchoolID, searchClass);
            //IQueryable<SummedUpRecord> iqSummedUpRecord = SummedUpRecordBusiness.SearchBySchool(entity.SchoolID, searchClass);
            // IQueryable<PupilRanking> iqPupilRanking = PupilRankingBusiness.SearchBySchool(entity.SchoolID, searchClass);
            // IQueryable<PupilEmulation> iqPupilEmulation = PupilEmulationBusiness.SearchBySchool(entity.SchoolID, searchClass);
            //Chiendd1: 20/052016. Bo sung theo orderNumber de hien thi STT diem dung trong so diem
            List<MarkRecordBO> iqMarkRecord = (from p in VMarkRecordBusiness.SearchBySchool(entity.SchoolID, searchClass)
                                               join q in MarkTypeBusiness.All on p.MarkTypeID equals q.MarkTypeID
                                               select new MarkRecordBO
                                               {
                                                   MarkRecordID = p.MarkRecordID,
                                                   Mark = p.Mark,
                                                   PupilID = p.PupilID,
                                                   SubjectID = p.SubjectID,
                                                   Semester = p.Semester,
                                                   Title = q.Title,
                                                   OrderNumber = p.OrderNumber
                                               }).ToList();
            List<JudgeRecordBO> iqJudgeRecord = (from p in VJudgeRecordBusiness.SearchBySchool(entity.SchoolID, searchClass)
                                                 join q in MarkTypeBusiness.All on p.MarkTypeID equals q.MarkTypeID
                                                 select new JudgeRecordBO
                                                 {
                                                     JudgeRecordID = p.JudgeRecordID,
                                                     PupilID = p.PupilID,
                                                     SubjectID = p.SubjectID,
                                                     Title = q.Title,
                                                     Semester = p.Semester,
                                                     Judgement = p.Judgement,
                                                     OrderNumber = p.OrderNumber
                                                 }).ToList();
            List<SummedUpRecordBO> iqSummedUpRecord = VSummedUpRecordBusiness.SearchBySchool(entity.SchoolID, searchClass).Select(u => new SummedUpRecordBO
            {
                SummedUpRecordID = u.SummedUpRecordID,
                SummedUpMark = u.SummedUpMark,
                JudgementResult = u.JudgementResult,
                ReTestMark = u.ReTestMark,
                ReTestJudgement = u.ReTestJudgement,
                PupilID = u.PupilID,
                Semester = u.Semester,
                SubjectID = u.SubjectID,
                PeriodID = u.PeriodID
            }).Where(o => o.PeriodID == null).ToList();
            List<PupilRankingBO> iqPupilRanking = (from p in VPupilRankingBusiness.SearchBySchool(entity.SchoolID, searchClass)
                                                   join q in CapacityLevelBusiness.All on p.CapacityLevelID equals q.CapacityLevelID into g1
                                                   from j1 in g1.DefaultIfEmpty()
                                                   join r in ConductLevelBusiness.All on p.ConductLevelID equals r.ConductLevelID into g2
                                                   from j2 in g2.DefaultIfEmpty()
                                                   join s in StudyingJudgementBusiness.All on p.StudyingJudgementID equals s.StudyingJudgementID into g3
                                                   from j3 in g3.DefaultIfEmpty()
                                                   select new PupilRankingBO
                                                   {
                                                       PupilRankingID = p.PupilRankingID,
                                                       EducationLevelID = p.EducationLevelID,
                                                       PupilID = p.PupilID,
                                                       Semester = p.Semester,
                                                       CapacityLevelID = p.CapacityLevelID,
                                                       CapacityLevel = j1.CapacityLevel1,
                                                       ConductLevelName = j2.Resolution,
                                                       AverageMark = p.AverageMark,
                                                       PeriodID = p.PeriodID,
                                                       StudyingJudgementID = p.StudyingJudgementID,
                                                       StudyingJudgementResolution = j3.Resolution
                                                   }).Where(o => o.PeriodID == null).ToList();

            List<PupilEmulationBO> iqPupilEmulation = PupilEmulationBusiness.SearchBySchool(entity.SchoolID, searchClass).Select(u => new PupilEmulationBO
            {
                PupilEmulationID = u.PupilEmulationID,
                PupilID = u.PupilID,
                HonourAchivementTypeID = u.HonourAchivementTypeID,
                HonourAchivementTypeResolution = u.HonourAchivementType.Resolution,
                Semester = u.Semester
            }).ToList();
            IQueryable<ClassSubject> iqAllSuject = ClassSubjectBusiness.SearchBySchool(entity.SchoolID, searchClass).OrderBy(o => o.SubjectCat.OrderInSubject);
            if (entity.Semester == SystemParamsInFile.SEMESTER_OF_YEAR_FIRST)
            {
                iqAllSuject = iqAllSuject.Where(o => o.SectionPerWeekFirstSemester > 0);
            }
            else if (entity.Semester == SystemParamsInFile.SEMESTER_OF_YEAR_SECOND)
            {
                iqAllSuject = iqAllSuject.Where(o => o.SectionPerWeekSecondSemester > 0);
            }
            else
            {
                iqAllSuject = iqAllSuject.Where(o => o.SectionPerWeekFirstSemester > 0 || o.SectionPerWeekSecondSemester > 0);
            }
            IQueryable<ClassSubject> lsMonNN2 = iqAllSuject.Where(o => o.SubjectCat.IsForeignLanguage == true && (o.AppliedType.Value == SystemParamsInFile.APPLIED_TYPE_VOLUNTARY_PLUS_MARK || o.AppliedType.Value == SystemParamsInFile.APPLIED_TYPE_VOLUNTARY_PRIORITY));
            ClassSubjectBO scNN2 = (from p in lsMonNN2
                                    join q in SubjectCatBusiness.All on p.SubjectID equals q.SubjectCatID
                                    where q.DisplayName.ToUpper() != nameTinHoc.ToUpper()
                                    select new ClassSubjectBO
                                    {
                                        ClassID = p.ClassID,
                                        SubjectID = p.SubjectID,
                                        OrderInSubject = q.OrderInSubject,
                                        DisplayName = q.DisplayName,
                                        SubjectName = q.SubjectName,
                                        IsCommenting = p.IsCommenting,
                                        SectionPerWeekFirstSemester = p.SectionPerWeekFirstSemester,
                                        SectionPerWeekSecondSemester = p.SectionPerWeekSecondSemester
                                    }).OrderBy(o => o.OrderInSubject).FirstOrDefault();
            int scNN2ID = scNN2 != null ? scNN2.SubjectID : 0;
            if (scNN2 != null)
            {
                nameNN2 = scNN2.DisplayName;
            }

            ClassSubjectBO classsubjectNN = (from p in iqAllSuject.Where(o => o.SubjectCat.IsForeignLanguage == true && o.AppliedType.Value == SystemParamsInFile.APPLIED_TYPE_MANDATORY)
                                             join q in SubjectCatBusiness.All on p.SubjectID equals q.SubjectCatID
                                             select new ClassSubjectBO
                                             {
                                                 ClassID = p.ClassID,
                                                 SubjectID = p.SubjectID,
                                                 OrderInSubject = q.OrderInSubject,
                                                 DisplayName = q.DisplayName,
                                                 SubjectName = q.SubjectName,
                                                 IsCommenting = p.IsCommenting,
                                                 SectionPerWeekFirstSemester = p.SectionPerWeekFirstSemester,
                                                 SectionPerWeekSecondSemester = p.SectionPerWeekSecondSemester
                                             }).OrderBy(o => o.OrderInSubject)
                                                .FirstOrDefault();


            if (classsubjectNN != null)
            {
                nameNN += classsubjectNN.DisplayName.ToUpper();
                subjectNN = classsubjectNN.DisplayName.ToUpper();
                sheetDiemHK1_2.SetCellValue("T4", nameNN);
                sheetDiemHK2_2.SetCellValue("T4", nameNN);
                sheetDiemTKHK2.SetCellValue("J3", classsubjectNN.DisplayName);
                sheetTongKetCaNam.SetCellValue("J4", classsubjectNN.DisplayName);
            }


            //mon tu chon:
            List<int> lsAppliedTypeMonTuChon = new List<int>() { SystemParamsInFile.APPLIED_TYPE_VOLUNTARY, SystemParamsInFile.APPLIED_TYPE_VOLUNTARY_PLUS_MARK, SystemParamsInFile.APPLIED_TYPE_VOLUNTARY_PRIORITY };
            //Chi lay mot mon TC neu co nhieu mon
            IQueryable<ClassSubject> lsMonTC = iqAllSuject.Where(o => lsAppliedTypeMonTuChon.Contains(o.AppliedType.Value));



            ClassSubjectBO scTH = (from p in lsMonTC
                                   join q in SubjectCatBusiness.All on p.SubjectID equals q.SubjectCatID
                                   where q.DisplayName.ToUpper() == nameTinHoc.ToUpper()
                                   select new ClassSubjectBO
                                   {
                                       ClassID = p.ClassID,
                                       SubjectID = p.SubjectID,
                                       OrderInSubject = q.OrderInSubject,
                                       DisplayName = q.DisplayName,
                                       SubjectName = q.SubjectName,
                                       IsCommenting = p.IsCommenting,
                                       SectionPerWeekFirstSemester = p.SectionPerWeekFirstSemester,
                                       SectionPerWeekSecondSemester = p.SectionPerWeekSecondSemester
                                   }).OrderBy(o => o.OrderInSubject).FirstOrDefault();

            ClassSubjectBO scTC = (from p in lsMonTC
                                   join q in SubjectCatBusiness.All on p.SubjectID equals q.SubjectCatID
                                   where (!q.DisplayName.Trim().ToLower().Contains(nameTinHoc.Trim().ToLower()) || q.DisplayName.Trim().Length != nameTinHoc.Trim().Length) && (!q.DisplayName.Trim().ToLower().Contains(nameNN2.Trim().ToLower()) || q.DisplayName.Trim().Length != nameNN2.Trim().Length || nameNN2 == "")
                                   select new ClassSubjectBO
                                   {
                                       ClassID = p.ClassID,
                                       SubjectID = p.SubjectID,
                                       OrderInSubject = q.OrderInSubject,
                                       DisplayName = q.DisplayName,
                                       SubjectName = q.SubjectName,
                                       IsCommenting = p.IsCommenting,
                                       SectionPerWeekFirstSemester = p.SectionPerWeekFirstSemester,
                                       SectionPerWeekSecondSemester = p.SectionPerWeekSecondSemester
                                   }).OrderBy(o => o.OrderInSubject).FirstOrDefault();


            string nameTH = "";
            if (scTH != null)
            {
                nameTH = scTH.DisplayName.ToUpper();
            }
            if (scTC != null)
            {
                nameTuChon = scTC.DisplayName.ToUpper();
            }
            List<ClassSubjectBO> lsAllSubject = (from p in iqAllSuject
                                                 join q in SubjectCatBusiness.All on p.SubjectID equals q.SubjectCatID
                                                 select new ClassSubjectBO
                                                 {
                                                     ClassID = p.ClassID,
                                                     DisplayName = q.DisplayName,
                                                     OrderInSubject = q.OrderInSubject,
                                                     SubjectID = q.SubjectCatID,
                                                     SubjectName = q.SubjectName,
                                                     IsCommenting = p.IsCommenting,
                                                     Abbreviation = q.Abbreviation,
                                                     SectionPerWeekFirstSemester = p.SectionPerWeekFirstSemester,
                                                     SectionPerWeekSecondSemester = p.SectionPerWeekSecondSemester
                                                 }).ToList();


            //viethd31 Lay thong tin tot nghiep
            Dictionary<string, object> dicGra = new Dictionary<string, object>();
            dicGra["ClassID"] = entity.ClassID;
            dicGra["AcademicYearID"] = entity.AcademicYearID;
            dicGra["SchoolID"] = entity.SchoolID;

            List<PupilGraduation> lstPgAll = PupilGraduationBusiness.SearchBySchool(entity.SchoolID, dicGra).ToList();
            #endregion

            if (entity.Semester == SystemParamsInFile.SEMESTER_OF_YEAR_ALL || entity.Semester == SystemParamsInFile.SEMESTER_OF_YEAR_FIRST)
            {
                #region fill dữ liệu sheet Diem_HKI
                //Lấy mẫu style cho học sinh đang học, đã tốt nghiêp/ chuyển trường, chuyển lớp, thôi học

                //Sheet DiemHKI
                IVTRange rangeStudyingDiemHKI = sheetDiemHKITemp.GetRange("A9", "AC9");
                IVTRange rangeNotStudyingDiemHKI = sheetDiemHKITemp.GetRange("A8", "AC8");
                IVTRange rangeStudyingDevideDiemHKI = sheetDiemHKITemp.GetRange("A17", "AC17");
                IVTRange rangeNotStudyingDevideDiemHKI = sheetDiemHKITemp.GetRange("A12", "AC12");
                //Shet DiemHKI (2)
                IVTRange rangeStudyingDiemHKI_2 = sheetDiemHKI_2Temp.GetRange("A9", "AH9");
                IVTRange rangeNotStudyingDiemHKI_2 = sheetDiemHKI_2Temp.GetRange("A8", "AH8");
                IVTRange rangeStudyingDevideDiemHKI_2 = sheetDiemHKI_2Temp.GetRange("A17", "AH17");
                IVTRange rangeNotStudyingDevideDiemHKI_2 = sheetDiemHKI_2Temp.GetRange("A12", "AH12");
                //Sheet DiemHKI_MTC
                IVTRange rangeStudyingDiemHKI_MTC = sheetDiem_HKI_MTCTemp.GetRange("A9", "AH9");
                IVTRange rangeNotStudyingDiemHKI_MTC = sheetDiem_HKI_MTCTemp.GetRange("A8", "AH8");
                IVTRange rangeStudyingDevideDiemHKI_MTC = sheetDiem_HKI_MTCTemp.GetRange("A17", "AH17");
                IVTRange rangeNotStudyingDevideDiemHKI_MTC = sheetDiem_HKI_MTCTemp.GetRange("A12", "AH12");
                //Sheet DiemTKHKI
                IVTRange rangeStudyingDiemTKHKI = sheetDiemTKHKITemp.GetRange("A6", "V6");
                IVTRange rangeNotStudyingDiemTKHKI = sheetDiemTKHKITemp.GetRange("A5", "V5");
                IVTRange rangeStudyingDevideDiemTKHKI = sheetDiemTKHKITemp.GetRange("A14", "V14");
                IVTRange rangeNotStudyingDevideDiemTKHKI = sheetDiemTKHKITemp.GetRange("A9", "V9");
                #endregion
                #region BiaPhanGhiDiem_HKI

                string mst = "4-SGTGD ";
                string schoolLevel = ReportUtils.ConvertAppliedLevelForReportName(entity.AppliedLevel);
                mst += schoolLevel;
                sheetBiaGhiDiemHK1.SetCellValue("B71", mst);

                #endregion BiaPhanGhiDiem_HKI

                #region Diem_HKI, DiemHKI(2), DiemHKI_MTC
                //Điền thông tin cho môn nghề, môn tự chọn
                sheetDiemHK1mtc.SetCellValue("AD5", nameTuChon);

                //Dien giao vien chu nhiem vao cac sheet diem HK1
                //1. Sheet diemHK1
                if (string.IsNullOrEmpty(suffixName))
                {
                    FillHeadTeacher(sheetDiemHK1, "I74", HeadTeacherName);
                    FillHeadTeacher(sheetDiemHK1, "Z74", HeadTeacherName);
                    FillHeadTeacher(sheetDiemHK1_2, "I74", HeadTeacherName);
                    FillHeadTeacher(sheetDiemHK1_2, "AE74", HeadTeacherName);
                    FillHeadTeacher(sheetDiemHK1mtc, "N74", HeadTeacherName);
                    FillHeadTeacher(sheetDiemHK1mtc, "AE74", HeadTeacherName);
                }
                FillHeadTeacher(sheetDiemTKHK1, "R70", HeadTeacherName);

                //Điền style
                for (int i = 0; i < lsPP.Count(); i++)
                {
                    PupilProfileBO pp = lsPP[i];
                    if (pp.ProfileStatus == SystemParamsInFile.PUPIL_STATUS_STUDYING || pp.ProfileStatus == SystemParamsInFile.PUPIL_STATUS_GRADUATED)
                    {

                        if (i % 5 == 4)
                        {
                            sheetDiemHK1.CopyPasteSameSize(rangeStudyingDevideDiemHKI, 8 + i, 1);
                            sheetDiemHK1_2.CopyPasteSameSize(rangeStudyingDevideDiemHKI_2, 8 + i, 1);
                            sheetDiemHK1mtc.CopyPasteSameSize(rangeStudyingDevideDiemHKI_MTC, 8 + i, 1);
                            sheetDiemTKHK1.CopyPasteSameSize(rangeStudyingDevideDiemTKHKI, 5 + i, 1);
                        }
                        else
                        {
                            sheetDiemHK1.CopyPasteSameSize(rangeStudyingDiemHKI, 8 + i, 1);
                            sheetDiemHK1_2.CopyPasteSameSize(rangeStudyingDiemHKI_2, 8 + i, 1);
                            sheetDiemHK1mtc.CopyPasteSameSize(rangeStudyingDiemHKI_MTC, 8 + i, 1);
                            sheetDiemTKHK1.CopyPasteSameSize(rangeStudyingDiemTKHKI, 5 + i, 1);
                        }
                    }
                    else
                    {
                        if (i % 5 == 4)
                        {
                            sheetDiemHK1.CopyPasteSameSize(rangeNotStudyingDevideDiemHKI, 8 + i, 1);
                            sheetDiemHK1_2.CopyPasteSameSize(rangeNotStudyingDevideDiemHKI_2, 8 + i, 1);
                            sheetDiemHK1mtc.CopyPasteSameSize(rangeNotStudyingDevideDiemHKI_MTC, 8 + i, 1);
                            sheetDiemTKHK1.CopyPasteSameSize(rangeNotStudyingDevideDiemTKHKI, 5 + i, 1);
                        }
                        else
                        {
                            sheetDiemHK1.CopyPasteSameSize(rangeNotStudyingDiemHKI, 8 + i, 1);
                            sheetDiemHK1_2.CopyPasteSameSize(rangeNotStudyingDiemHKI_2, 8 + i, 1);
                            sheetDiemHK1mtc.CopyPasteSameSize(rangeNotStudyingDiemHKI_MTC, 8 + i, 1);
                            sheetDiemTKHK1.CopyPasteSameSize(rangeNotStudyingDiemTKHKI, 5 + i, 1);
                        }
                    }
                    sheetDiemHK1.SetCellValue(8 + i, 1, i + 1);
                    sheetDiemHK1.SetCellValue(8 + i, 14, i + 1);
                    sheetDiemHK1_2.SetCellValue(8 + i, 1, i + 1);
                    sheetDiemHK1_2.SetCellValue(8 + i, 14, i + 1);
                    sheetDiemHK1mtc.SetCellValue(8 + i, 1, i + 1);
                    sheetDiemHK1mtc.SetCellValue(8 + i, 19, i + 1);
                    sheetDiemTKHK1.SetCellValue(5 + i, 1, i + 1);
                    sheetDiemHK1.SetCellValue(8 + i, 2, pp.FullName);
                    sheetDiemHK1_2.SetCellValue(8 + i, 2, pp.FullName);
                    sheetDiemHK1mtc.SetCellValue(8 + i, 2, pp.FullName);
                    sheetDiemTKHK1.SetCellValue(5 + i, 2, pp.FullName);
                }


                #region Lay danh sach diem
                //diem mon tinh diem
                List<MarkRecordBO> lsMR = iqMarkRecord.Where(o => o.Semester == SystemParamsInFile.SEMESTER_OF_YEAR_FIRST).ToList();


                //diem mon nhan xet
                List<JudgeRecordBO> lsJR = iqJudgeRecord.Where(o => o.Semester == SystemParamsInFile.SEMESTER_OF_YEAR_FIRST).ToList();


                //diem tbm
                List<SummedUpRecordBO> lsSUR = iqSummedUpRecord.Where(o => o.Semester == SystemParamsInFile.SEMESTER_OF_YEAR_FIRST).ToList();

                //Điểm TBCM, HL, HK, TD
                List<PupilRankingBO> lsPR = iqPupilRanking.Where(o => o.Semester == SystemParamsInFile.SEMESTER_OF_YEAR_FIRST).ToList();

                //diem tk TD: PupilEmulationBusiness.SearchBySchool(SchoolID, Dictionary)
                List<PupilEmulationBO> lsPE = iqPupilEmulation.Where(o => o.Semester == SystemParamsInFile.SEMESTER_OF_YEAR_FIRST).ToList();

                #endregion

                for (int i = 0; i < lsAllSubject.Count(); i++)
                {
                    ClassSubjectBO thisSubject = lsAllSubject[i];
                    #region Mon nhan xet
                    if (thisSubject.IsCommenting == SystemParamsInFile.ISCOMMENTING_TYPE_JUDGE && thisSubject.SectionPerWeekFirstSemester > 0)
                    {
                        for (int j = 0; j < lsPP.Count(); j++)
                        {

                            int pupilID = lsPP[j].PupilProfileID;
                            bool showPupilData = lstPocHK1.Where(o => o.PupilID == pupilID).Count() > 0 ? true : false;
                            if (!showPupilData)
                            {
                                continue;
                            }
                            //fill du lieu diem

                            List<JudgeRecordBO> jrOfThisPupil = lsJR.Where(o => o.PupilID == pupilID).ToList();
                            List<string> lsdiemMieng = jrOfThisPupil.Where(o => o.Title == GlobalConstants.MARK_TYPE_M)
                                                                    .Where(o => o.SubjectID == thisSubject.SubjectID)
                                                                    .OrderBy(o => o.OrderNumber)
                                                                    .Select(o => o.Judgement).ToList();
                            List<string> lsdiem15ph = jrOfThisPupil.Where(o => o.Title == GlobalConstants.MARK_TYPE_P)
                                                                    .Where(o => o.SubjectID == thisSubject.SubjectID)
                                                                    .OrderBy(o => o.OrderNumber)
                                                                    .Select(o => o.Judgement).ToList();
                            List<string> lsdiem1t = jrOfThisPupil.Where(o => o.Title == GlobalConstants.MARK_TYPE_V)
                                                                    .Where(o => o.SubjectID == thisSubject.SubjectID)
                                                                    .OrderBy(o => o.OrderNumber)
                                                                    .Select(o => o.Judgement).ToList();
                            List<string> lsdiemhk = jrOfThisPupil.Where(o => o.Title == GlobalConstants.MARK_TYPE_HK)
                                                                    .Where(o => o.SubjectID == thisSubject.SubjectID)
                                                                    .Select(o => o.Judgement).ToList();
                            string diemTBM = lsSUR.Where(o => o.PupilID == pupilID).Where(o => o.SubjectID == thisSubject.SubjectID).Select(o => o.JudgementResult).FirstOrDefault();

                            string diemMieng = string.Join(" ", lsdiemMieng.ToArray());
                            string diem15 = string.Join(" ", lsdiem15ph.ToArray());
                            string diem1Tiet = string.Join(" ", lsdiem1t.ToArray());
                            string diemHK = string.Join(" ", lsdiemhk.ToArray());

                            #region Kiem tra mon hoc
                            if (thisSubject.DisplayName.ToUpper() == subjectNN.ToUpper())
                            {
                                sheetDiemHK1_2.SetCellValue(8 + j, 20, diemMieng);
                                sheetDiemHK1_2.SetCellValue(8 + j, 21, diem15);
                                sheetDiemHK1_2.SetCellValue(8 + j, 22, diem1Tiet);
                                sheetDiemHK1_2.SetCellValue(8 + j, 23, diemHK);
                                sheetDiemHK1_2.SetCellValue(8 + j, 24, diemTBM);
                                if (j == 0 && !string.IsNullOrEmpty(suffixName))
                                {
                                    if (suffixName != null)
                                    {
                                        sheetDiemHK1_2.SetCellValue("T69", "Xác nhận điểm của GV " + thisSubject.DisplayName);
                                    }
                                    //sheetDiemHK1_2.SetCellValue("T70", "Giáo viên môn " + thisSubject.DisplayName);
                                    FillTeachingSubject(sheetDiemHK1_2, "T72", thisSubject.SubjectID, GlobalConstants.SEMESTER_OF_YEAR_FIRST, suffixName, lstTeachingBo, j);
                                }
                            }
                            else if (scNN2 != null && thisSubject.DisplayName.ToUpper() == scNN2.DisplayName.ToUpper())
                            {
                                sheetDiemHK1mtc.SetCellValue(8 + j, 20, diemMieng);
                                sheetDiemHK1mtc.SetCellValue(8 + j, 21, diem15);
                                sheetDiemHK1mtc.SetCellValue(8 + j, 22, diem1Tiet);
                                sheetDiemHK1mtc.SetCellValue(8 + j, 23, diemHK);
                                sheetDiemHK1mtc.SetCellValue(8 + j, 24, diemTBM);
                                if (j == 0 && !string.IsNullOrEmpty(suffixName))
                                {
                                    if (suffixName != null)
                                    {
                                        sheetDiemHK1mtc.SetCellValue("T69", "Xác nhận điểm của GV " + thisSubject.DisplayName);
                                    }
                                    //sheetDiemHK1mtc.SetCellValue("T70", "Giáo viên môn " + thisSubject.DisplayName);
                                    FillTeachingSubject(sheetDiemHK1mtc, "T72", thisSubject.SubjectID, GlobalConstants.SEMESTER_OF_YEAR_FIRST, suffixName, lstTeachingBo, j);
                                }
                            }
                            else if (thisSubject.DisplayName.ToUpper() == nameTuChon.ToUpper())
                            {
                                sheetDiemHK1mtc.SetCellValue(8 + j, 30, diemMieng);
                                sheetDiemHK1mtc.SetCellValue(8 + j, 31, diem15);
                                sheetDiemHK1mtc.SetCellValue(8 + j, 32, diem1Tiet);
                                sheetDiemHK1mtc.SetCellValue(8 + j, 33, diemHK);
                                sheetDiemHK1mtc.SetCellValue(8 + j, 34, diemTBM);
                                if (j == 0 && !string.IsNullOrEmpty(suffixName))
                                {
                                    if (suffixName != null)
                                    {
                                        sheetDiemHK1mtc.SetCellValue("AD69", "Xác nhận điểm của GV" + thisSubject.DisplayName);
                                    }
                                    //sheetDiemHK1mtc.SetCellValue("AD70", "Giáo viên môn " + thisSubject.DisplayName);
                                    FillTeachingSubject(sheetDiemHK1mtc, "AD72", thisSubject.SubjectID, GlobalConstants.SEMESTER_OF_YEAR_FIRST, suffixName, lstTeachingBo, j);
                                }
                            }
                            else if (thisSubject.DisplayName.ToUpper() == nameTH.ToUpper())
                            {
                                sheetDiemHK1mtc.SetCellValue(8 + j, 25, diemMieng);
                                sheetDiemHK1mtc.SetCellValue(8 + j, 26, diem15);
                                sheetDiemHK1mtc.SetCellValue(8 + j, 27, diem1Tiet);
                                sheetDiemHK1mtc.SetCellValue(8 + j, 28, diemHK);
                                sheetDiemHK1mtc.SetCellValue(8 + j, 29, diemTBM);
                                if (j == 0 && !string.IsNullOrEmpty(suffixName))
                                {
                                    FillTeachingSubject(sheetDiemHK1mtc, "Y72", thisSubject.SubjectID, GlobalConstants.SEMESTER_OF_YEAR_FIRST, suffixName, lstTeachingBo, j);
                                }
                            }
                            else
                            {
                                switch (thisSubject.DisplayName.ToUpper())
                                {
                                    case "TOÁN":
                                        sheetDiemHK1.SetCellValue(8 + j, 3, diemMieng);
                                        sheetDiemHK1.SetCellValue(8 + j, 4, diem15);
                                        sheetDiemHK1.SetCellValue(8 + j, 5, diem1Tiet);
                                        sheetDiemHK1.SetCellValue(8 + j, 6, diemHK);
                                        sheetDiemHK1.SetCellValue(8 + j, 7, diemTBM);
                                        if (j == 0 && !string.IsNullOrEmpty(suffixName))
                                        {
                                            FillTeachingSubject(sheetDiemHK1, "D72", thisSubject.SubjectID, GlobalConstants.SEMESTER_OF_YEAR_FIRST, suffixName, lstTeachingBo, j);
                                        }
                                        break;
                                    case "VẬT LÍ":
                                        sheetDiemHK1.SetCellValue(8 + j, 8, diemMieng);
                                        sheetDiemHK1.SetCellValue(8 + j, 9, diem15);
                                        sheetDiemHK1.SetCellValue(8 + j, 10, diem1Tiet);
                                        sheetDiemHK1.SetCellValue(8 + j, 11, diemHK);
                                        sheetDiemHK1.SetCellValue(8 + j, 12, diemTBM);
                                        if (j == 0 && !string.IsNullOrEmpty(suffixName))
                                        {
                                            FillTeachingSubject(sheetDiemHK1, "I72", thisSubject.SubjectID, GlobalConstants.SEMESTER_OF_YEAR_FIRST, suffixName, lstTeachingBo, j);
                                        }
                                        break;
                                    case "HÓA HỌC":
                                        sheetDiemHK1.SetCellValue(8 + j, 15, diemMieng);
                                        sheetDiemHK1.SetCellValue(8 + j, 16, diem15);
                                        sheetDiemHK1.SetCellValue(8 + j, 17, diem1Tiet);
                                        sheetDiemHK1.SetCellValue(8 + j, 18, diemHK);
                                        sheetDiemHK1.SetCellValue(8 + j, 19, diemTBM);
                                        if (j == 0 && !string.IsNullOrEmpty(suffixName))
                                        {
                                            FillTeachingSubject(sheetDiemHK1, "P72", thisSubject.SubjectID, GlobalConstants.SEMESTER_OF_YEAR_FIRST, suffixName, lstTeachingBo, j);
                                        }
                                        break;
                                    case "SINH HỌC":
                                        sheetDiemHK1.SetCellValue(8 + j, 20, diemMieng);
                                        sheetDiemHK1.SetCellValue(8 + j, 21, diem15);
                                        sheetDiemHK1.SetCellValue(8 + j, 22, diem1Tiet);
                                        sheetDiemHK1.SetCellValue(8 + j, 23, diemHK);
                                        sheetDiemHK1.SetCellValue(8 + j, 24, diemTBM);
                                        if (j == 0 && !string.IsNullOrEmpty(suffixName))
                                        {
                                            FillTeachingSubject(sheetDiemHK1, "U72", thisSubject.SubjectID, GlobalConstants.SEMESTER_OF_YEAR_FIRST, suffixName, lstTeachingBo, j);
                                        }
                                        break;
                                    case "NGỮ VĂN":
                                        sheetDiemHK1_2.SetCellValue(8 + j, 3, diemMieng);
                                        sheetDiemHK1_2.SetCellValue(8 + j, 4, diem15);
                                        sheetDiemHK1_2.SetCellValue(8 + j, 5, diem1Tiet);
                                        sheetDiemHK1_2.SetCellValue(8 + j, 6, diemHK);
                                        sheetDiemHK1_2.SetCellValue(8 + j, 7, diemTBM);
                                        if (j == 0 && !string.IsNullOrEmpty(suffixName))
                                        {
                                            FillTeachingSubject(sheetDiemHK1_2, "D72", thisSubject.SubjectID, GlobalConstants.SEMESTER_OF_YEAR_FIRST, suffixName, lstTeachingBo, j);
                                        }
                                        break;
                                    case "LỊCH SỬ":
                                        sheetDiemHK1_2.SetCellValue(8 + j, 8, diemMieng);
                                        sheetDiemHK1_2.SetCellValue(8 + j, 9, diem15);
                                        sheetDiemHK1_2.SetCellValue(8 + j, 10, diem1Tiet);
                                        sheetDiemHK1_2.SetCellValue(8 + j, 11, diemHK);
                                        sheetDiemHK1_2.SetCellValue(8 + j, 12, diemTBM);
                                        if (j == 0 && !string.IsNullOrEmpty(suffixName))
                                        {
                                            FillTeachingSubject(sheetDiemHK1_2, "I72", thisSubject.SubjectID, GlobalConstants.SEMESTER_OF_YEAR_FIRST, suffixName, lstTeachingBo, j);
                                        }
                                        break;
                                    case "ĐỊA LÍ":
                                        sheetDiemHK1_2.SetCellValue(8 + j, 15, diemMieng);
                                        sheetDiemHK1_2.SetCellValue(8 + j, 16, diem15);
                                        sheetDiemHK1_2.SetCellValue(8 + j, 17, diem1Tiet);
                                        sheetDiemHK1_2.SetCellValue(8 + j, 18, diemHK);
                                        sheetDiemHK1_2.SetCellValue(8 + j, 19, diemTBM);
                                        if (j == 0 && !string.IsNullOrEmpty(suffixName))
                                        {
                                            FillTeachingSubject(sheetDiemHK1_2, "O72", thisSubject.SubjectID, GlobalConstants.SEMESTER_OF_YEAR_FIRST, suffixName, lstTeachingBo, j);
                                        }
                                        break;
                                    case "GDCD":
                                        sheetDiemHK1_2.SetCellValue(8 + j, 25, diemMieng);
                                        sheetDiemHK1_2.SetCellValue(8 + j, 26, diem15);
                                        sheetDiemHK1_2.SetCellValue(8 + j, 27, diem1Tiet);
                                        sheetDiemHK1_2.SetCellValue(8 + j, 28, diemHK);
                                        sheetDiemHK1_2.SetCellValue(8 + j, 29, diemTBM);
                                        if (j == 0 && !string.IsNullOrEmpty(suffixName))
                                        {
                                            FillTeachingSubject(sheetDiemHK1_2, "Y72", thisSubject.SubjectID, GlobalConstants.SEMESTER_OF_YEAR_FIRST, suffixName, lstTeachingBo, j);
                                        }
                                        break;
                                    case "CÔNG NGHỆ":
                                        sheetDiemHK1_2.SetCellValue(8 + j, 30, diemMieng);
                                        sheetDiemHK1_2.SetCellValue(8 + j, 31, diem15);
                                        sheetDiemHK1_2.SetCellValue(8 + j, 32, diem1Tiet);
                                        sheetDiemHK1_2.SetCellValue(8 + j, 33, diemHK);
                                        sheetDiemHK1_2.SetCellValue(8 + j, 34, diemTBM);
                                        if (j == 0 && !string.IsNullOrEmpty(suffixName))
                                        {
                                            FillTeachingSubject(sheetDiemHK1_2, "AE72", thisSubject.SubjectID, GlobalConstants.SEMESTER_OF_YEAR_FIRST, suffixName, lstTeachingBo, j);
                                        }
                                        break;
                                    case "THỂ DỤC":
                                        sheetDiemHK1mtc.SetCellValue(8 + j, 3, diemMieng);
                                        sheetDiemHK1mtc.SetCellValue(8 + j, 4, diem15);
                                        sheetDiemHK1mtc.SetCellValue(8 + j, 5, diem1Tiet);
                                        sheetDiemHK1mtc.SetCellValue(8 + j, 6, diemHK);
                                        sheetDiemHK1mtc.SetCellValue(8 + j, 7, diemTBM);
                                        if (j == 0 && !string.IsNullOrEmpty(suffixName))
                                        {
                                            FillTeachingSubject(sheetDiemHK1mtc, "C72", thisSubject.SubjectID, GlobalConstants.SEMESTER_OF_YEAR_FIRST, suffixName, lstTeachingBo, j);
                                        }
                                        break;
                                    case "ÂM NHẠC":
                                        sheetDiemHK1mtc.SetCellValue(8 + j, 8, diemMieng);
                                        sheetDiemHK1mtc.SetCellValue(8 + j, 9, diem15);
                                        sheetDiemHK1mtc.SetCellValue(8 + j, 10, diem1Tiet);
                                        sheetDiemHK1mtc.SetCellValue(8 + j, 11, diemHK);
                                        sheetDiemHK1mtc.SetCellValue(8 + j, 12, diemTBM);
                                        if (j == 0 && !string.IsNullOrEmpty(suffixName))
                                        {
                                            FillTeachingSubject(sheetDiemHK1mtc, "H72", thisSubject.SubjectID, GlobalConstants.SEMESTER_OF_YEAR_FIRST, suffixName, lstTeachingBo, j);
                                        }
                                        break;
                                    case "MỸ THUẬT":
                                        sheetDiemHK1mtc.SetCellValue(8 + j, 13, diemMieng);
                                        sheetDiemHK1mtc.SetCellValue(8 + j, 14, diem15);
                                        sheetDiemHK1mtc.SetCellValue(8 + j, 15, diem1Tiet);
                                        sheetDiemHK1mtc.SetCellValue(8 + j, 16, diemHK);
                                        sheetDiemHK1mtc.SetCellValue(8 + j, 17, diemTBM);
                                        if (j == 0 && !string.IsNullOrEmpty(suffixName))
                                        {
                                            FillTeachingSubject(sheetDiemHK1mtc, "M72", thisSubject.SubjectID, GlobalConstants.SEMESTER_OF_YEAR_FIRST, suffixName, lstTeachingBo, j);
                                        }
                                        break;
                                    default:
                                        break;
                                }
                            }

                        }
                            #endregion



                    }
                    #endregion
                    #region mon tinh diem
                    else //mon tinh diem
                    {
                        if (thisSubject.SectionPerWeekFirstSemester > 0)
                        {
                            PupilProfileBO objPF = null;
                            for (int j = 0; j < lsPP.Count(); j++)
                            {
                                objPF = lsPP[j];
                                int pupilID = lsPP[j].PupilProfileID;

                                bool showPupilData = true;

                                if (objPF.ProfileStatus == GlobalConstants.PUPIL_STATUS_STUDYING || objPF.ProfileStatus == GlobalConstants.PUPIL_STATUS_GRADUATED)
                                {
                                    showPupilData = objPF.AssignedDate <= academicYear.SecondSemesterEndDate;
                                }
                                else
                                {
                                    showPupilData = objPF.EndDate > academicYear.FirstSemesterEndDate;
                                }

                                if (!showPupilData)
                                {
                                    continue;
                                }

                                //fill du lieu diem
                                List<MarkRecordBO> mrOfThisPupil = lsMR.Where(o => o.PupilID == pupilID).ToList();
                                List<decimal?> lsdiemMieng = mrOfThisPupil.Where(o => o.Title == GlobalConstants.MARK_TYPE_M)
                                                                        .Where(o => o.SubjectID == thisSubject.SubjectID)
                                                                        .OrderBy(o => o.OrderNumber)
                                                                        .Select(o => o.Mark).ToList();
                                List<decimal?> lsdiem15ph = mrOfThisPupil.Where(o => o.Title == GlobalConstants.MARK_TYPE_P)
                                                                        .Where(o => o.SubjectID == thisSubject.SubjectID)
                                                                        .OrderBy(o => o.OrderNumber)
                                                                        .Select(o => o.Mark).ToList();
                                List<decimal?> lsdiem1t = mrOfThisPupil.Where(o => o.Title == GlobalConstants.MARK_TYPE_V)
                                                                        .Where(o => o.SubjectID == thisSubject.SubjectID)
                                                                        .OrderBy(o => o.OrderNumber)
                                                                        .Select(o => o.Mark).ToList();
                                List<decimal?> lsdiemhk = mrOfThisPupil.Where(o => o.Title == GlobalConstants.MARK_TYPE_HK)
                                                                        .Where(o => o.SubjectID == thisSubject.SubjectID)
                                                                        .Select(o => o.Mark).ToList();
                                decimal? lsDiemTBM = lsSUR.Where(o => o.PupilID == pupilID).Where(o => o.SubjectID == thisSubject.SubjectID).Select(o => o.SummedUpMark).FirstOrDefault();

                                //List<string> strlsdiemMieng = lsdiemMieng.Select(s => string.Format("{0:0}", s)).ToList();
                                List<string> strlsdiemMieng = ReportUtils.ConvertListMarkForM(lsdiemMieng, isSoHCM);
                                List<string> strlsdiem15ph = ReportUtils.ConvertListMarkForV(lsdiem15ph, isSoHCM);
                                List<string> strlsdiem1t = ReportUtils.ConvertListMarkForV(lsdiem1t, isSoHCM);
                                List<string> strlsdiemhk = ReportUtils.ConvertListMarkForV(lsdiemhk, isSoHCM);


                                string diemMieng = string.Join(" ", strlsdiemMieng.ToArray());
                                string diem15 = string.Join(" ", strlsdiem15ph.ToArray());
                                string diem1Tiet = string.Join(" ", strlsdiem1t.ToArray());
                                string diemHK = string.Join(" ", strlsdiemhk.ToArray());
                                string diemTBM = lsDiemTBM != null ? ReportUtils.ConvertMarkForV_SGTGD(lsDiemTBM.Value, isSoHCM) : string.Empty;

                                #region Kiem tra mon hoc
                                if (thisSubject.DisplayName.ToUpper() == subjectNN.ToUpper())
                                {
                                    sheetDiemHK1_2.SetCellValue(8 + j, 20, diemMieng);
                                    sheetDiemHK1_2.SetCellValue(8 + j, 21, diem15);
                                    sheetDiemHK1_2.SetCellValue(8 + j, 22, diem1Tiet);
                                    sheetDiemHK1_2.SetCellValue(8 + j, 23, diemHK);
                                    sheetDiemHK1_2.SetCellValue(8 + j, 24, diemTBM);
                                    if (j == 0 && !string.IsNullOrEmpty(suffixName))
                                    {
                                        if (suffixName != null)
                                        {
                                            sheetDiemHK1_2.SetCellValue("T69", "Xác nhận điểm của GV " + thisSubject.DisplayName);
                                        }
                                        //sheetDiemHK1_2.SetCellValue("T70", "Giáo viên môn " + thisSubject.DisplayName);
                                        FillTeachingSubject(sheetDiemHK1_2, "T72", thisSubject.SubjectID, GlobalConstants.SEMESTER_OF_YEAR_FIRST, suffixName, lstTeachingBo, j);
                                    }
                                }
                                else if (scNN2 != null && thisSubject.DisplayName.ToUpper() == scNN2.DisplayName.ToUpper())
                                {
                                    sheetDiemHK1mtc.SetCellValue(8 + j, 20, diemMieng);
                                    sheetDiemHK1mtc.SetCellValue(8 + j, 21, diem15);
                                    sheetDiemHK1mtc.SetCellValue(8 + j, 22, diem1Tiet);
                                    sheetDiemHK1mtc.SetCellValue(8 + j, 23, diemHK);
                                    sheetDiemHK1mtc.SetCellValue(8 + j, 24, diemTBM);
                                    if (j == 0 && !string.IsNullOrEmpty(suffixName))
                                    {
                                        FillTeachingSubject(sheetDiemHK1mtc, "T72", thisSubject.SubjectID, GlobalConstants.SEMESTER_OF_YEAR_FIRST, suffixName, lstTeachingBo, j);
                                    }
                                }
                                else if (thisSubject.DisplayName.ToUpper() == nameTuChon.ToUpper())
                                {
                                    sheetDiemHK1mtc.SetCellValue(8 + j, 30, diemMieng);
                                    sheetDiemHK1mtc.SetCellValue(8 + j, 31, diem15);
                                    sheetDiemHK1mtc.SetCellValue(8 + j, 32, diem1Tiet);
                                    sheetDiemHK1mtc.SetCellValue(8 + j, 33, diemHK);
                                    sheetDiemHK1mtc.SetCellValue(8 + j, 34, diemTBM);
                                    if (j == 0 && !string.IsNullOrEmpty(suffixName))
                                    {
                                        if (suffixName != null)
                                        {
                                            sheetDiemHK1mtc.SetCellValue("AD69", "Xác nhận điểm của GV " + thisSubject.DisplayName);
                                        }
                                        //sheetDiemHK1mtc.SetCellValue("AD70", "Giáo viên môn " + thisSubject.DisplayName);
                                        FillTeachingSubject(sheetDiemHK1mtc, "AD72", thisSubject.SubjectID, GlobalConstants.SEMESTER_OF_YEAR_FIRST, suffixName, lstTeachingBo, j);
                                    }
                                }
                                else if (thisSubject.DisplayName.ToUpper() == nameTH.ToUpper())
                                {
                                    sheetDiemHK1mtc.SetCellValue(8 + j, 25, diemMieng);
                                    sheetDiemHK1mtc.SetCellValue(8 + j, 26, diem15);
                                    sheetDiemHK1mtc.SetCellValue(8 + j, 27, diem1Tiet);
                                    sheetDiemHK1mtc.SetCellValue(8 + j, 28, diemHK);
                                    sheetDiemHK1mtc.SetCellValue(8 + j, 29, diemTBM);
                                    if (j == 0 && !string.IsNullOrEmpty(suffixName))
                                    {
                                        FillTeachingSubject(sheetDiemHK1mtc, "Y72", thisSubject.SubjectID, GlobalConstants.SEMESTER_OF_YEAR_FIRST, suffixName, lstTeachingBo, j);
                                    }
                                }
                                else
                                {
                                    switch (thisSubject.DisplayName.ToUpper())
                                    {
                                        case "TOÁN":
                                            sheetDiemHK1.SetCellValue(8 + j, 3, diemMieng);
                                            sheetDiemHK1.SetCellValue(8 + j, 4, diem15);
                                            sheetDiemHK1.SetCellValue(8 + j, 5, diem1Tiet);
                                            sheetDiemHK1.SetCellValue(8 + j, 6, diemHK);
                                            sheetDiemHK1.SetCellValue(8 + j, 7, diemTBM);
                                            if (j == 0 && !string.IsNullOrEmpty(suffixName))
                                            {
                                                FillTeachingSubject(sheetDiemHK1, "D72", thisSubject.SubjectID, GlobalConstants.SEMESTER_OF_YEAR_FIRST, suffixName, lstTeachingBo, j);
                                            }
                                            break;
                                        case "VẬT LÍ":
                                            sheetDiemHK1.SetCellValue(8 + j, 8, diemMieng);
                                            sheetDiemHK1.SetCellValue(8 + j, 9, diem15);
                                            sheetDiemHK1.SetCellValue(8 + j, 10, diem1Tiet);
                                            sheetDiemHK1.SetCellValue(8 + j, 11, diemHK);
                                            sheetDiemHK1.SetCellValue(8 + j, 12, diemTBM);
                                            if (j == 0 && !string.IsNullOrEmpty(suffixName))
                                            {
                                                FillTeachingSubject(sheetDiemHK1, "I72", thisSubject.SubjectID, GlobalConstants.SEMESTER_OF_YEAR_FIRST, suffixName, lstTeachingBo, j);
                                            }
                                            break;
                                        case "HÓA HỌC":
                                            sheetDiemHK1.SetCellValue(8 + j, 15, diemMieng);
                                            sheetDiemHK1.SetCellValue(8 + j, 16, diem15);
                                            sheetDiemHK1.SetCellValue(8 + j, 17, diem1Tiet);
                                            sheetDiemHK1.SetCellValue(8 + j, 18, diemHK);
                                            sheetDiemHK1.SetCellValue(8 + j, 19, diemTBM);
                                            if (j == 0 && !string.IsNullOrEmpty(suffixName))
                                            {
                                                FillTeachingSubject(sheetDiemHK1, "P72", thisSubject.SubjectID, GlobalConstants.SEMESTER_OF_YEAR_FIRST, suffixName, lstTeachingBo, j);
                                            }
                                            break;
                                        case "SINH HỌC":
                                            sheetDiemHK1.SetCellValue(8 + j, 20, diemMieng);
                                            sheetDiemHK1.SetCellValue(8 + j, 21, diem15);
                                            sheetDiemHK1.SetCellValue(8 + j, 22, diem1Tiet);
                                            sheetDiemHK1.SetCellValue(8 + j, 23, diemHK);
                                            sheetDiemHK1.SetCellValue(8 + j, 24, diemTBM);
                                            if (j == 0 && !string.IsNullOrEmpty(suffixName))
                                            {
                                                FillTeachingSubject(sheetDiemHK1, "U72", thisSubject.SubjectID, GlobalConstants.SEMESTER_OF_YEAR_FIRST, suffixName, lstTeachingBo, j);
                                            }
                                            break;
                                        case "NGỮ VĂN":
                                            sheetDiemHK1_2.SetCellValue(8 + j, 3, diemMieng);
                                            sheetDiemHK1_2.SetCellValue(8 + j, 4, diem15);
                                            sheetDiemHK1_2.SetCellValue(8 + j, 5, diem1Tiet);
                                            sheetDiemHK1_2.SetCellValue(8 + j, 6, diemHK);
                                            sheetDiemHK1_2.SetCellValue(8 + j, 7, diemTBM);
                                            if (j == 0 && !string.IsNullOrEmpty(suffixName))
                                            {
                                                FillTeachingSubject(sheetDiemHK1_2, "D72", thisSubject.SubjectID, GlobalConstants.SEMESTER_OF_YEAR_FIRST, suffixName, lstTeachingBo, j);
                                            }
                                            break;
                                        case "LỊCH SỬ":
                                            sheetDiemHK1_2.SetCellValue(8 + j, 8, diemMieng);
                                            sheetDiemHK1_2.SetCellValue(8 + j, 9, diem15);
                                            sheetDiemHK1_2.SetCellValue(8 + j, 10, diem1Tiet);
                                            sheetDiemHK1_2.SetCellValue(8 + j, 11, diemHK);
                                            sheetDiemHK1_2.SetCellValue(8 + j, 12, diemTBM);
                                            if (j == 0 && !string.IsNullOrEmpty(suffixName))
                                            {
                                                FillTeachingSubject(sheetDiemHK1_2, "I72", thisSubject.SubjectID, GlobalConstants.SEMESTER_OF_YEAR_FIRST, suffixName, lstTeachingBo, j);
                                            }
                                            break;
                                        case "ĐỊA LÍ":
                                            sheetDiemHK1_2.SetCellValue(8 + j, 15, diemMieng);
                                            sheetDiemHK1_2.SetCellValue(8 + j, 16, diem15);
                                            sheetDiemHK1_2.SetCellValue(8 + j, 17, diem1Tiet);
                                            sheetDiemHK1_2.SetCellValue(8 + j, 18, diemHK);
                                            sheetDiemHK1_2.SetCellValue(8 + j, 19, diemTBM);
                                            if (j == 0 && !string.IsNullOrEmpty(suffixName))
                                            {
                                                FillTeachingSubject(sheetDiemHK1_2, "O72", thisSubject.SubjectID, GlobalConstants.SEMESTER_OF_YEAR_FIRST, suffixName, lstTeachingBo, j);
                                            }
                                            break;
                                        case "GDCD":
                                            sheetDiemHK1_2.SetCellValue(8 + j, 25, diemMieng);
                                            sheetDiemHK1_2.SetCellValue(8 + j, 26, diem15);
                                            sheetDiemHK1_2.SetCellValue(8 + j, 27, diem1Tiet);
                                            sheetDiemHK1_2.SetCellValue(8 + j, 28, diemHK);
                                            sheetDiemHK1_2.SetCellValue(8 + j, 29, diemTBM);
                                            if (j == 0 && !string.IsNullOrEmpty(suffixName))
                                            {
                                                FillTeachingSubject(sheetDiemHK1_2, "Y72", thisSubject.SubjectID, GlobalConstants.SEMESTER_OF_YEAR_FIRST, suffixName, lstTeachingBo, j);
                                            }
                                            break;
                                        case "CÔNG NGHỆ":
                                            sheetDiemHK1_2.SetCellValue(8 + j, 30, diemMieng);
                                            sheetDiemHK1_2.SetCellValue(8 + j, 31, diem15);
                                            sheetDiemHK1_2.SetCellValue(8 + j, 32, diem1Tiet);
                                            sheetDiemHK1_2.SetCellValue(8 + j, 33, diemHK);
                                            sheetDiemHK1_2.SetCellValue(8 + j, 34, diemTBM);
                                            if (j == 0 && !string.IsNullOrEmpty(suffixName))
                                            {
                                                FillTeachingSubject(sheetDiemHK1_2, "AE72", thisSubject.SubjectID, GlobalConstants.SEMESTER_OF_YEAR_FIRST, suffixName, lstTeachingBo, j);
                                            }
                                            break;
                                        case "THỂ DỤC":
                                            sheetDiemHK1mtc.SetCellValue(8 + j, 3, diemMieng);
                                            sheetDiemHK1mtc.SetCellValue(8 + j, 4, diem15);
                                            sheetDiemHK1mtc.SetCellValue(8 + j, 5, diem1Tiet);
                                            sheetDiemHK1mtc.SetCellValue(8 + j, 6, diemHK);
                                            sheetDiemHK1mtc.SetCellValue(8 + j, 7, diemTBM);
                                            if (j == 0 && !string.IsNullOrEmpty(suffixName))
                                            {
                                                FillTeachingSubject(sheetDiemHK1mtc, "C72", thisSubject.SubjectID, GlobalConstants.SEMESTER_OF_YEAR_FIRST, suffixName, lstTeachingBo, j);
                                            }
                                            break;
                                        case "ÂM NHẠC":
                                            sheetDiemHK1mtc.SetCellValue(8 + j, 8, diemMieng);
                                            sheetDiemHK1mtc.SetCellValue(8 + j, 9, diem15);
                                            sheetDiemHK1mtc.SetCellValue(8 + j, 10, diem1Tiet);
                                            sheetDiemHK1mtc.SetCellValue(8 + j, 11, diemHK);
                                            sheetDiemHK1mtc.SetCellValue(8 + j, 12, diemTBM);
                                            if (j == 0 && !string.IsNullOrEmpty(suffixName))
                                            {
                                                FillTeachingSubject(sheetDiemHK1mtc, "H72", thisSubject.SubjectID, GlobalConstants.SEMESTER_OF_YEAR_FIRST, suffixName, lstTeachingBo, j);
                                            }
                                            break;
                                        case "MỸ THUẬT":
                                            sheetDiemHK1mtc.SetCellValue(8 + j, 13, diemMieng);
                                            sheetDiemHK1mtc.SetCellValue(8 + j, 14, diem15);
                                            sheetDiemHK1mtc.SetCellValue(8 + j, 15, diem1Tiet);
                                            sheetDiemHK1mtc.SetCellValue(8 + j, 16, diemHK);
                                            sheetDiemHK1mtc.SetCellValue(8 + j, 17, diemTBM);
                                            if (j == 0 && !string.IsNullOrEmpty(suffixName))
                                            {
                                                FillTeachingSubject(sheetDiemHK1mtc, "M72", thisSubject.SubjectID, GlobalConstants.SEMESTER_OF_YEAR_FIRST, suffixName, lstTeachingBo, j);
                                            }
                                            break;
                                        default:
                                            break;
                                    }
                                }
                                #endregion

                            }
                        }
                    }

                    #endregion

                }
                #endregion Diem_HKI

                #region sheetDiemTKHKI

                Dictionary<string, object> HK1TotalMarkDic = new Dictionary<string, object>();
                List<object> insideDicTK1 = new List<object>();
                Dictionary<string, object> innerDicTK1 = new Dictionary<string, object>();
                foreach (ClassSubjectBO cs in lsAllSubject)
                {
                    string position = SetDisplaySubjectNX12_THCS(cs.DisplayName);
                    if (cs.IsCommenting == SystemParamsInFile.ISCOMMENTING_TYPE_JUDGE)
                    {
                        if (cs.Abbreviation.ToUpper() == "CN")
                        {
                            sheetDiemTKHK1.SetCellValue(3, 12, "CN(loại)");
                        }
                        else
                        {
                            string ouput = cs.DisplayName + "(loại)";

                            if (position.Length > 0)
                            {
                                sheetDiemTKHK1.SetCellValue(position, ouput);
                            }
                        }
                    }
                    if (cs.DisplayName.ToUpper() == nameTuChon.ToUpper())
                    {
                        if (position.Length > 0)
                        {
                            sheetDiemTKHK1.SetCellValue(position, "");
                        }
                    }
                }
                if (classsubjectNN != null)
                {
                    if (classsubjectNN.IsCommenting == SystemParamsInFile.ISCOMMENTING_TYPE_JUDGE)
                    {
                        sheetDiemTKHK1.SetCellValue(3, 10, classsubjectNN.DisplayName + "(loại)");
                    }
                    else
                    {
                        sheetDiemTKHK1.SetCellValue(3, 10, classsubjectNN.DisplayName);
                    }
                }

                if (scTH != null)
                {
                    if (scTH.IsCommenting == SystemParamsInFile.ISCOMMENTING_TYPE_JUDGE)
                    {
                        sheetDiemTKHK1.SetCellValue(4, 17, scTH.DisplayName + "(loại)");
                    }
                }
                if (scTC != null)
                {

                    if (scTC.IsCommenting == SystemParamsInFile.ISCOMMENTING_TYPE_JUDGE)
                    {
                        sheetDiemTKHK1.SetCellValue("R4", scTC.DisplayName + "(loại)");
                    }
                    else
                    {
                        sheetDiemTKHK1.SetCellValue("R4", scTC.DisplayName);
                    }
                }

                for (int j = 0; j < lsPP.Count(); j++)
                {

                    innerDicTK1 = new Dictionary<string, object>();
                    int pupilID = lsPP[j].PupilProfileID;


                    bool showPupilData = lstPocHK1.Where(o => o.PupilID == pupilID).Count() > 0 ? true : false;
                    if (showPupilData)
                    {
                        PupilRankingBO pupilRanking = lsPR.Where(o => o.PupilID == pupilID).FirstOrDefault();
                        if (pupilRanking != null)
                        {
                            string diemTBCM = pupilRanking.AverageMark.HasValue ? ReportUtils.ConvertMarkForV_SGTGD(pupilRanking.AverageMark.Value, isSoHCM) : string.Empty;
                            innerDicTK1.Add("TBCM", diemTBCM);

                            innerDicTK1.Add("HL", pupilRanking.CapacityLevel != null ? UtilsBusiness.ConvertCapacity(pupilRanking.CapacityLevel) : "");

                            innerDicTK1.Add("HK", pupilRanking.ConductLevelName != null ? UtilsBusiness.ConvertCapacity(pupilRanking.ConductLevelName) : "");
                        }
                        else
                        {
                            innerDicTK1.Add("TBCM", "");

                            innerDicTK1.Add("HL", "");

                            innerDicTK1.Add("HK", "");
                        }
                        PupilEmulationBO pe = lsPE.Where(o => o.PupilID == pupilID).FirstOrDefault();
                        string lsTD = "";
                        if (pe != null && pe.HonourAchivementTypeResolution != null)
                        {
                            lsTD = UtilsBusiness.ConvertCapacity(pe.HonourAchivementTypeResolution);
                        }

                        innerDicTK1.Add("TD", lsTD);

                        for (int i = 0; i < lsAllSubject.Count(); i++)
                        {
                            ClassSubjectBO thisSubject = lsAllSubject[i];
                            //mon bat buoc
                            if (thisSubject.SectionPerWeekFirstSemester > 0)
                            {
                                if (thisSubject.IsCommenting == SystemParamsInFile.ISCOMMENTING_TYPE_JUDGE)//mon nhan xet
                                {
                                    //fill du lieu diem
                                    List<string> lsDiemTBM = lsSUR.Where(o => o.PupilID == pupilID).Where(o => o.SubjectID == thisSubject.SubjectID).Select(o => o.JudgementResult).ToList();
                                    string diemTBM = string.Join(" ", lsDiemTBM.ToArray());
                                    //innerDicTK1.Add("SubjectTBM" + thisSubject.SubjectID.ToString(), diemTBM);
                                    if (thisSubject.DisplayName.ToUpper() == nameTuChon.ToUpper())
                                    {
                                        innerDicTK1["TC"] = diemTBM;
                                    }
                                    else if (thisSubject.DisplayName.ToUpper() == nameNN2.ToUpper())
                                    {
                                        innerDicTK1["NN2"] = diemTBM;
                                    }
                                    else if (thisSubject.DisplayName.ToUpper() == nameTH.ToUpper())
                                    {
                                        innerDicTK1["TH"] = diemTBM;
                                    }
                                    else if (thisSubject.DisplayName.ToUpper() == subjectNN.ToUpper())
                                    {
                                        sheetDiemTKHK1.SetCellValue(5 + j, 10, diemTBM);
                                    }
                                    else if (thisSubject.Abbreviation.ToUpper() == "CN")
                                    {
                                        sheetDiemTKHK1.SetCellValue(5 + j, 12, diemTBM);
                                    }
                                    else
                                    {
                                        switch (thisSubject.DisplayName.ToUpper())
                                        {
                                            case "TOÁN":
                                                sheetDiemTKHK1.SetCellValue(5 + j, 3, diemTBM);
                                                break;
                                            case "VẬT LÍ":
                                                sheetDiemTKHK1.SetCellValue(5 + j, 4, diemTBM);
                                                break;
                                            case "HÓA HỌC":
                                                sheetDiemTKHK1.SetCellValue(5 + j, 5, diemTBM);
                                                break;
                                            case "SINH HỌC":
                                                sheetDiemTKHK1.SetCellValue(5 + j, 6, diemTBM);
                                                break;
                                            case "NGỮ VĂN":
                                                sheetDiemTKHK1.SetCellValue(5 + j, 7, diemTBM);
                                                break;
                                            case "LỊCH SỬ":
                                                sheetDiemTKHK1.SetCellValue(5 + j, 8, diemTBM);
                                                break;
                                            case "ĐỊA LÍ":
                                                sheetDiemTKHK1.SetCellValue(5 + j, 9, diemTBM);
                                                break;
                                            case "GDCD":
                                                sheetDiemTKHK1.SetCellValue(5 + j, 11, diemTBM);
                                                break;
                                            case "THỂ DỤC":
                                                sheetDiemTKHK1.SetCellValue(5 + j, 13, diemTBM);
                                                break;
                                            case "ÂM NHẠC":
                                                sheetDiemTKHK1.SetCellValue(5 + j, 14, diemTBM);
                                                break;
                                            case "MỸ THUẬT":
                                                sheetDiemTKHK1.SetCellValue(5 + j, 15, diemTBM);
                                                break;
                                            default:
                                                break;
                                        }
                                    }

                                }
                                else //mon tinh diem
                                {
                                    //fill du lieu diem
                                    decimal? DiemTBM = lsSUR.Where(o => o.PupilID == pupilID).Where(o => o.SubjectID == thisSubject.SubjectID).Select(o => o.SummedUpMark).FirstOrDefault();
                                    string diemTBM = "";
                                    if (DiemTBM != null)
                                    {
                                        diemTBM = ReportUtils.ConvertMarkForV_SGTGD(DiemTBM.Value, isSoHCM);
                                    }
                                    //innerDicTK1.Add("SubjectTBM" + thisSubject.SubjectID.ToString(), diemTBM);  
                                    if (thisSubject.DisplayName.ToUpper() == nameTuChon.ToUpper())
                                    {
                                        innerDicTK1["TC"] = diemTBM;
                                    }
                                    else if (thisSubject.DisplayName.ToUpper() == nameNN2.ToUpper())
                                    {
                                        innerDicTK1["NN2"] = diemTBM;
                                    }
                                    else if (thisSubject.DisplayName.ToUpper() == nameTH.ToUpper())
                                    {
                                        innerDicTK1["TH"] = diemTBM;
                                    }
                                    else if (thisSubject.DisplayName.ToUpper() == subjectNN.ToUpper())
                                    {
                                        sheetDiemTKHK1.SetCellValue(5 + j, 10, diemTBM);
                                    }
                                    else if (thisSubject.Abbreviation.ToUpper() == "CN")
                                    {
                                        sheetDiemTKHK1.SetCellValue(5 + j, 12, diemTBM);
                                    }
                                    else
                                    {
                                        switch (thisSubject.DisplayName.ToUpper())
                                        {
                                            case "TOÁN":
                                                sheetDiemTKHK1.SetCellValue(5 + j, 3, diemTBM);
                                                break;
                                            case "VẬT LÍ":
                                                sheetDiemTKHK1.SetCellValue(5 + j, 4, diemTBM);
                                                break;
                                            case "HÓA HỌC":
                                                sheetDiemTKHK1.SetCellValue(5 + j, 5, diemTBM);
                                                break;
                                            case "SINH HỌC":
                                                sheetDiemTKHK1.SetCellValue(5 + j, 6, diemTBM);
                                                break;
                                            case "NGỮ VĂN":
                                                sheetDiemTKHK1.SetCellValue(5 + j, 7, diemTBM);
                                                break;
                                            case "LỊCH SỬ":
                                                sheetDiemTKHK1.SetCellValue(5 + j, 8, diemTBM);
                                                break;
                                            case "ĐỊA LÍ":
                                                sheetDiemTKHK1.SetCellValue(5 + j, 9, diemTBM);
                                                break;
                                            case "GDCD":
                                                sheetDiemTKHK1.SetCellValue(5 + j, 11, diemTBM);
                                                break;
                                            case "THỂ DỤC":
                                                sheetDiemTKHK1.SetCellValue(5 + j, 13, diemTBM);
                                                break;
                                            case "ÂM NHẠC":
                                                sheetDiemTKHK1.SetCellValue(5 + j, 14, diemTBM);
                                                break;
                                            case "MỸ THUẬT":
                                                sheetDiemTKHK1.SetCellValue(5 + j, 15, diemTBM);
                                                break;
                                            default:
                                                break;
                                        }
                                    }

                                }
                            }

                        }


                        //if (scTH != null && scTH.SectionPerWeekFirstSemester > 0)
                        //{

                        //    if (scTH.IsCommenting == SystemParamsInFile.ISCOMMENTING_TYPE_JUDGE)//mon nhan xet
                        //    {
                        //        //fill du lieu diem
                        //        List<string> lsDiemPT2 = lsSUR.Where(o => o.PupilID == pupilID && o.SubjectID == scTH.SubjectID).Select(o => o.JudgementResult).ToList();
                        //        string DiemPT2 = string.Join(" ", lsDiemPT2.ToArray());
                        //        innerDicTK1["TH"] = DiemPT2;
                        //    }
                        //    else //mon tinh diem
                        //    {
                        //        //fill du lieu diem
                        //        decimal? lsDiemPT2 = lsSUR.Where(o => o.PupilID == pupilID && o.SubjectID == scTH.SubjectID).Select(o => o.SummedUpMark).FirstOrDefault();
                        //        string DiemPT2 = "";
                        //        if (DiemPT2 != null)
                        //        {
                        //            DiemPT2 = lsDiemPT2 != null ? ReportUtils.ConvertMarkForV(lsDiemPT2.Value) : string.Empty;
                        //        }
                        //        innerDicTK1["TH"] = DiemPT2;
                        //    }
                        //}

                    }
                    insideDicTK1.Add(innerDicTK1);
                }
                HK1TotalMarkDic.Add("Rows", insideDicTK1);
                sheetDiemTKHK1.FillVariableValue(HK1TotalMarkDic);
                #endregion sheetDiemTKHK1
            }
            if (entity.Semester == SystemParamsInFile.SEMESTER_OF_YEAR_ALL || entity.Semester == SystemParamsInFile.SEMESTER_OF_YEAR_SECOND)
            {
                #region style các sheet DiemHKII, DiemHKII(2), DiemHKII_MTC, Diem_TKHKII
                //Sheet DiemHKII
                IVTRange rangeStudyingDiemHKII = sheetDiemHK2Temp.GetRange("A9", "AC9");
                IVTRange rangeNotStudyingDiemHKII = sheetDiemHK2Temp.GetRange("A8", "AC8");
                IVTRange rangeStudyingDevideDiemHKII = sheetDiemHK2Temp.GetRange("A17", "AC17");
                IVTRange rangeNotStudyingDevideDiemHKII = sheetDiemHK2Temp.GetRange("A12", "AC12");
                //Shet DiemHKII (2)
                IVTRange rangeStudyingDiemHKII_2 = sheetDiemHK2_2Temp.GetRange("A9", "AH9");
                IVTRange rangeNotStudyingDiemHKII_2 = sheetDiemHK2_2Temp.GetRange("A8", "AH8");
                IVTRange rangeStudyingDevideDiemHKII_2 = sheetDiemHK2_2Temp.GetRange("A17", "AH17");
                IVTRange rangeNotStudyingDevideDiemHKII_2 = sheetDiemHK2_2Temp.GetRange("A12", "AH12");
                //Sheet DiemHKII_MTC
                IVTRange rangeStudyingDiemHKII_MTC = sheetDiemHK2mtcTemp.GetRange("A9", "AH9");
                IVTRange rangeNotStudyingDiemHKII_MTC = sheetDiemHK2mtcTemp.GetRange("A8", "AH8");
                IVTRange rangeStudyingDevideDiemHKII_MTC = sheetDiemHK2mtcTemp.GetRange("A17", "AH17");
                IVTRange rangeNotStudyingDevideDiemHKII_MTC = sheetDiemHK2mtcTemp.GetRange("A12", "AH12");
                //Sheet DiemTKHKII
                IVTRange rangeStudyingDiemTKHKII = sheetDiemTKHK2Temp.GetRange("A6", "V6");
                IVTRange rangeNotStudyingDiemTKHKII = sheetDiemTKHK2Temp.GetRange("A5", "V5");
                IVTRange rangeStudyingDevideDiemTKHKII = sheetDiemTKHK2Temp.GetRange("A14", "V14");
                IVTRange rangeNotStudyingDevideDiemTKHKII = sheetDiemTKHK2Temp.GetRange("A9", "V9");
                #endregion
                #region Điền Style
                //Điền style
                for (int i = 0; i < lsPP.Count(); i++)
                {
                    PupilProfileBO pp = lsPP[i];
                    if (pp.ProfileStatus == SystemParamsInFile.PUPIL_STATUS_STUDYING || pp.ProfileStatus == SystemParamsInFile.PUPIL_STATUS_GRADUATED)
                    {

                        if (i % 5 == 4)
                        {
                            sheetDiemHK2.CopyPasteSameSize(rangeStudyingDevideDiemHKII, 8 + i, 1);
                            sheetDiemHK2_2.CopyPasteSameSize(rangeStudyingDevideDiemHKII_2, 8 + i, 1);
                            sheetDiemHK2mtc.CopyPasteSameSize(rangeStudyingDevideDiemHKII_MTC, 8 + i, 1);
                            sheetDiemTKHK2.CopyPasteSameSize(rangeStudyingDevideDiemTKHKII, 5 + i, 1);
                        }
                        else
                        {
                            sheetDiemHK2.CopyPasteSameSize(rangeStudyingDiemHKII, 8 + i, 1);
                            sheetDiemHK2_2.CopyPasteSameSize(rangeStudyingDiemHKII_2, 8 + i, 1);
                            sheetDiemHK2mtc.CopyPasteSameSize(rangeStudyingDiemHKII_MTC, 8 + i, 1);
                            sheetDiemTKHK2.CopyPasteSameSize(rangeStudyingDiemTKHKII, 5 + i, 1);
                        }
                    }
                    else
                    {
                        if (i % 5 == 4)
                        {
                            sheetDiemHK2.CopyPasteSameSize(rangeNotStudyingDevideDiemHKII, 8 + i, 1);
                            sheetDiemHK2_2.CopyPasteSameSize(rangeNotStudyingDevideDiemHKII_2, 8 + i, 1);
                            sheetDiemHK2mtc.CopyPasteSameSize(rangeNotStudyingDevideDiemHKII_MTC, 8 + i, 1);
                            sheetDiemTKHK2.CopyPasteSameSize(rangeNotStudyingDevideDiemTKHKII, 5 + i, 1);
                        }
                        else
                        {
                            sheetDiemHK2.CopyPasteSameSize(rangeNotStudyingDiemHKII, 8 + i, 1);
                            sheetDiemHK2_2.CopyPasteSameSize(rangeNotStudyingDiemHKII_2, 8 + i, 1);
                            sheetDiemHK2mtc.CopyPasteSameSize(rangeNotStudyingDiemHKII_MTC, 8 + i, 1);
                            sheetDiemTKHK2.CopyPasteSameSize(rangeNotStudyingDiemTKHKII, 5 + i, 1);
                        }
                    }
                    sheetDiemHK2.SetCellValue(8 + i, 1, i + 1);
                    sheetDiemHK2.SetCellValue(8 + i, 14, i + 1);
                    sheetDiemHK2_2.SetCellValue(8 + i, 1, i + 1);
                    sheetDiemHK2_2.SetCellValue(8 + i, 14, i + 1);
                    sheetDiemHK2mtc.SetCellValue(8 + i, 1, i + 1);
                    sheetDiemHK2mtc.SetCellValue(8 + i, 19, i + 1);
                    sheetDiemTKHK2.SetCellValue(5 + i, 1, i + 1);
                    sheetDiemHK2.SetCellValue(8 + i, 2, pp.FullName);
                    sheetDiemHK2_2.SetCellValue(8 + i, 2, pp.FullName);
                    sheetDiemHK2mtc.SetCellValue(8 + i, 2, pp.FullName);
                    sheetDiemTKHK2.SetCellValue(5 + i, 2, pp.FullName);
                }
                #endregion
                #region Fill dữ liệu DiemHKII, DiemHKII(2), DiemHKII_MTC
                #region Lay danh sach diem
                //diem mon tinh diem
                List<MarkRecordBO> lsMR = iqMarkRecord.Where(o => o.Semester == SystemParamsInFile.SEMESTER_OF_YEAR_SECOND).ToList();
                //diem mon nhan xet
                List<JudgeRecordBO> lsJR = iqJudgeRecord.Where(o => o.Semester == SystemParamsInFile.SEMESTER_OF_YEAR_SECOND).ToList();
                //diem tbm
                List<SummedUpRecordBO> lsSUR = iqSummedUpRecord.Where(o => o.Semester == SystemParamsInFile.SEMESTER_OF_YEAR_SECOND).ToList();
                //Điểm TBCM, HL, HK, TD
                List<PupilRankingBO> lsPR = iqPupilRanking.Where(o => o.Semester == SystemParamsInFile.SEMESTER_OF_YEAR_SECOND).ToList();
                //diem tk TD: PupilEmulationBusiness.SearchBySchool(SchoolID, Dictionary)
                List<PupilEmulationBO> lsPE = iqPupilEmulation.Where(o => o.Semester == SystemParamsInFile.SEMESTER_OF_YEAR_SECOND).ToList();

                #endregion
                sheetDiemHK2mtc.SetCellValue("AD5", nameTuChon);
                //1. Sheet diemHK2
                if (string.IsNullOrEmpty(suffixName))
                {
                    FillHeadTeacher(sheetDiemHK2, "I74", HeadTeacherName);
                    FillHeadTeacher(sheetDiemHK2, "Z74", HeadTeacherName);
                    FillHeadTeacher(sheetDiemHK2_2, "I74", HeadTeacherName);
                    FillHeadTeacher(sheetDiemHK2_2, "AE74", HeadTeacherName);
                    FillHeadTeacher(sheetDiemHK2mtc, "N74", HeadTeacherName);
                    FillHeadTeacher(sheetDiemHK2mtc, "AE74", HeadTeacherName);
                }
                FillHeadTeacher(sheetDiemTKHK2, "R71", HeadTeacherName);
                FillHeadTeacher(sheetTongKetCaNam, "R71", HeadTeacherName);


                for (int i = 0; i < lsAllSubject.Count(); i++)
                {
                    ClassSubjectBO thisSubject = lsAllSubject[i];
                    #region Mon nhan xet
                    if (thisSubject.IsCommenting == SystemParamsInFile.ISCOMMENTING_TYPE_JUDGE && thisSubject.SectionPerWeekSecondSemester > 0)
                    {
                        for (int j = 0; j < lsPP.Count(); j++)
                        {

                            int pupilID = lsPP[j].PupilProfileID;
                            bool showPupilData = lstPocHK2.Where(o => o.PupilID == pupilID).Count() > 0 ? true : false;
                            //fill du lieu diem
                            if (!showPupilData)
                            {
                                continue;
                            }
                            List<JudgeRecordBO> jrOfThisPupil = lsJR.Where(o => o.PupilID == pupilID).ToList();
                            List<string> lsdiemMieng = jrOfThisPupil.Where(o => o.Title == GlobalConstants.MARK_TYPE_M)
                                                                    .Where(o => o.SubjectID == thisSubject.SubjectID)
                                                                    .OrderBy(o => o.OrderNumber)
                                                                    .Select(o => o.Judgement).ToList();
                            List<string> lsdiem15ph = jrOfThisPupil.Where(o => o.Title == GlobalConstants.MARK_TYPE_P)
                                                                    .Where(o => o.SubjectID == thisSubject.SubjectID)
                                                                    .OrderBy(o => o.OrderNumber)
                                                                    .Select(o => o.Judgement).ToList();
                            List<string> lsdiem1t = jrOfThisPupil.Where(o => o.Title == GlobalConstants.MARK_TYPE_V)
                                                                 .Where(o => o.SubjectID == thisSubject.SubjectID)
                                                                 .OrderBy(o => o.OrderNumber)
                                                                 .Select(o => o.Judgement).ToList();
                            List<string> lsdiemhk = jrOfThisPupil.Where(o => o.Title == GlobalConstants.MARK_TYPE_HK)
                                                                .Where(o => o.SubjectID == thisSubject.SubjectID)
                                                                .Select(o => o.Judgement).ToList();
                            string diemTBM = lsSUR.Where(o => o.PupilID == pupilID).Where(o => o.SubjectID == thisSubject.SubjectID).Select(o => o.JudgementResult).FirstOrDefault();

                            string diemMieng = string.Join(" ", lsdiemMieng.ToArray());
                            string diem15 = string.Join(" ", lsdiem15ph.ToArray());
                            string diem1Tiet = string.Join(" ", lsdiem1t.ToArray());
                            string diemHK = string.Join(" ", lsdiemhk.ToArray());

                            #region Kiem tra mon hoc
                            if (thisSubject.DisplayName.ToUpper() == subjectNN.ToUpper())
                            {
                                sheetDiemHK2_2.SetCellValue(8 + j, 20, diemMieng);
                                sheetDiemHK2_2.SetCellValue(8 + j, 21, diem15);
                                sheetDiemHK2_2.SetCellValue(8 + j, 22, diem1Tiet);
                                sheetDiemHK2_2.SetCellValue(8 + j, 23, diemHK);
                                sheetDiemHK2_2.SetCellValue(8 + j, 24, diemTBM);
                                if (j == 0)
                                {
                                    if (suffixName != null)
                                    {
                                        sheetDiemHK2_2.SetCellValue("T69", "Xác nhận điểm của GV " + thisSubject.DisplayName);
                                    }
                                    //sheetDiemHK2_2.SetCellValue("T70", "Giáo viên môn " + thisSubject.DisplayName);
                                    FillTeachingSubject(sheetDiemHK2_2, "T72", thisSubject.SubjectID, GlobalConstants.SEMESTER_OF_YEAR_SECOND, suffixName, lstTeachingBo, j);
                                }
                            }
                            else if (scNN2 != null && thisSubject.DisplayName.ToUpper() == scNN2.DisplayName.ToUpper())
                            {
                                sheetDiemHK2mtc.SetCellValue(8 + j, 20, diemMieng);
                                sheetDiemHK2mtc.SetCellValue(8 + j, 21, diem15);
                                sheetDiemHK2mtc.SetCellValue(8 + j, 22, diem1Tiet);
                                sheetDiemHK2mtc.SetCellValue(8 + j, 23, diemHK);
                                sheetDiemHK2mtc.SetCellValue(8 + j, 24, diemTBM);
                                if (j == 0)
                                {
                                    FillTeachingSubject(sheetDiemHK2mtc, "T72", thisSubject.SubjectID, GlobalConstants.SEMESTER_OF_YEAR_SECOND, suffixName, lstTeachingBo, j);
                                }
                            }
                            else if (thisSubject.DisplayName.ToUpper() == nameTuChon.ToUpper())
                            {
                                sheetDiemHK2mtc.SetCellValue(8 + j, 30, diemMieng);
                                sheetDiemHK2mtc.SetCellValue(8 + j, 31, diem15);
                                sheetDiemHK2mtc.SetCellValue(8 + j, 32, diem1Tiet);
                                sheetDiemHK2mtc.SetCellValue(8 + j, 33, diemHK);
                                sheetDiemHK2mtc.SetCellValue(8 + j, 34, diemTBM);
                                if (j == 0)
                                {
                                    if (suffixName != null)
                                    {
                                        sheetDiemHK2mtc.SetCellValue("AD69", "Xác nhận điểm của GV " + thisSubject.DisplayName);
                                    }
                                    //sheetDiemHK2mtc.SetCellValue("AD70", "Giáo viên môn " + thisSubject.DisplayName);
                                    FillTeachingSubject(sheetDiemHK2mtc, "T72", thisSubject.SubjectID, GlobalConstants.SEMESTER_OF_YEAR_SECOND, suffixName, lstTeachingBo, j);
                                }
                            }
                            else if (thisSubject.DisplayName.ToUpper() == nameTH.ToUpper())
                            {
                                sheetDiemHK2mtc.SetCellValue(8 + j, 25, diemMieng);
                                sheetDiemHK2mtc.SetCellValue(8 + j, 26, diem15);
                                sheetDiemHK2mtc.SetCellValue(8 + j, 27, diem1Tiet);
                                sheetDiemHK2mtc.SetCellValue(8 + j, 28, diemHK);
                                sheetDiemHK2mtc.SetCellValue(8 + j, 29, diemTBM);
                                if (j == 0)
                                {
                                    FillTeachingSubject(sheetDiemHK2mtc, "Y72", thisSubject.SubjectID, GlobalConstants.SEMESTER_OF_YEAR_SECOND, suffixName, lstTeachingBo, j);
                                }
                            }
                            else
                            {
                                switch (thisSubject.DisplayName.ToUpper())
                                {
                                    case "TOÁN":
                                        sheetDiemHK2.SetCellValue(8 + j, 3, diemMieng);
                                        sheetDiemHK2.SetCellValue(8 + j, 4, diem15);
                                        sheetDiemHK2.SetCellValue(8 + j, 5, diem1Tiet);
                                        sheetDiemHK2.SetCellValue(8 + j, 6, diemHK);
                                        sheetDiemHK2.SetCellValue(8 + j, 7, diemTBM);
                                        if (j == 0)
                                        {
                                            FillTeachingSubject(sheetDiemHK2, "D72", thisSubject.SubjectID, GlobalConstants.SEMESTER_OF_YEAR_SECOND, suffixName, lstTeachingBo, j);
                                        }
                                        break;
                                    case "VẬT LÍ":
                                        sheetDiemHK2.SetCellValue(8 + j, 8, diemMieng);
                                        sheetDiemHK2.SetCellValue(8 + j, 9, diem15);
                                        sheetDiemHK2.SetCellValue(8 + j, 10, diem1Tiet);
                                        sheetDiemHK2.SetCellValue(8 + j, 11, diemHK);
                                        sheetDiemHK2.SetCellValue(8 + j, 12, diemTBM);
                                        if (j == 0)
                                        {
                                            FillTeachingSubject(sheetDiemHK2, "I72", thisSubject.SubjectID, GlobalConstants.SEMESTER_OF_YEAR_SECOND, suffixName, lstTeachingBo, j);
                                        }
                                        break;
                                    case "HÓA HỌC":
                                        sheetDiemHK2.SetCellValue(8 + j, 15, diemMieng);
                                        sheetDiemHK2.SetCellValue(8 + j, 16, diem15);
                                        sheetDiemHK2.SetCellValue(8 + j, 17, diem1Tiet);
                                        sheetDiemHK2.SetCellValue(8 + j, 18, diemHK);
                                        sheetDiemHK2.SetCellValue(8 + j, 19, diemTBM);
                                        if (j == 0)
                                        {
                                            FillTeachingSubject(sheetDiemHK2, "P72", thisSubject.SubjectID, GlobalConstants.SEMESTER_OF_YEAR_SECOND, suffixName, lstTeachingBo, j);
                                        }
                                        break;
                                    case "SINH HỌC":
                                        sheetDiemHK2.SetCellValue(8 + j, 20, diemMieng);
                                        sheetDiemHK2.SetCellValue(8 + j, 21, diem15);
                                        sheetDiemHK2.SetCellValue(8 + j, 22, diem1Tiet);
                                        sheetDiemHK2.SetCellValue(8 + j, 23, diemHK);
                                        sheetDiemHK2.SetCellValue(8 + j, 24, diemTBM);
                                        if (j == 0)
                                        {
                                            FillTeachingSubject(sheetDiemHK2, "U72", thisSubject.SubjectID, GlobalConstants.SEMESTER_OF_YEAR_SECOND, suffixName, lstTeachingBo, j);
                                        }
                                        break;
                                    case "NGỮ VĂN":
                                        sheetDiemHK2_2.SetCellValue(8 + j, 3, diemMieng);
                                        sheetDiemHK2_2.SetCellValue(8 + j, 4, diem15);
                                        sheetDiemHK2_2.SetCellValue(8 + j, 5, diem1Tiet);
                                        sheetDiemHK2_2.SetCellValue(8 + j, 6, diemHK);
                                        sheetDiemHK2_2.SetCellValue(8 + j, 7, diemTBM);
                                        if (j == 0)
                                        {
                                            FillTeachingSubject(sheetDiemHK2_2, "D72", thisSubject.SubjectID, GlobalConstants.SEMESTER_OF_YEAR_SECOND, suffixName, lstTeachingBo, j);
                                        }
                                        break;
                                    case "LỊCH SỬ":
                                        sheetDiemHK2_2.SetCellValue(8 + j, 8, diemMieng);
                                        sheetDiemHK2_2.SetCellValue(8 + j, 9, diem15);
                                        sheetDiemHK2_2.SetCellValue(8 + j, 10, diem1Tiet);
                                        sheetDiemHK2_2.SetCellValue(8 + j, 11, diemHK);
                                        sheetDiemHK2_2.SetCellValue(8 + j, 12, diemTBM);
                                        if (j == 0)
                                        {
                                            FillTeachingSubject(sheetDiemHK2_2, "I72", thisSubject.SubjectID, GlobalConstants.SEMESTER_OF_YEAR_SECOND, suffixName, lstTeachingBo, j);
                                        }
                                        break;
                                    case "ĐỊA LÍ":
                                        sheetDiemHK2_2.SetCellValue(8 + j, 15, diemMieng);
                                        sheetDiemHK2_2.SetCellValue(8 + j, 16, diem15);
                                        sheetDiemHK2_2.SetCellValue(8 + j, 17, diem1Tiet);
                                        sheetDiemHK2_2.SetCellValue(8 + j, 18, diemHK);
                                        sheetDiemHK2_2.SetCellValue(8 + j, 19, diemTBM);
                                        if (j == 0)
                                        {
                                            FillTeachingSubject(sheetDiemHK2_2, "O72", thisSubject.SubjectID, GlobalConstants.SEMESTER_OF_YEAR_SECOND, suffixName, lstTeachingBo, j);
                                        }
                                        break;
                                    case "GDCD":
                                        sheetDiemHK2_2.SetCellValue(8 + j, 25, diemMieng);
                                        sheetDiemHK2_2.SetCellValue(8 + j, 26, diem15);
                                        sheetDiemHK2_2.SetCellValue(8 + j, 27, diem1Tiet);
                                        sheetDiemHK2_2.SetCellValue(8 + j, 28, diemHK);
                                        sheetDiemHK2_2.SetCellValue(8 + j, 29, diemTBM);
                                        if (j == 0)
                                        {
                                            FillTeachingSubject(sheetDiemHK2_2, "Y72", thisSubject.SubjectID, GlobalConstants.SEMESTER_OF_YEAR_SECOND, suffixName, lstTeachingBo, j);
                                        }
                                        break;
                                    case "CÔNG NGHỆ":
                                        sheetDiemHK2_2.SetCellValue(8 + j, 30, diemMieng);
                                        sheetDiemHK2_2.SetCellValue(8 + j, 31, diem15);
                                        sheetDiemHK2_2.SetCellValue(8 + j, 32, diem1Tiet);
                                        sheetDiemHK2_2.SetCellValue(8 + j, 33, diemHK);
                                        sheetDiemHK2_2.SetCellValue(8 + j, 34, diemTBM);
                                        if (j == 0)
                                        {
                                            FillTeachingSubject(sheetDiemHK2_2, "AE72", thisSubject.SubjectID, GlobalConstants.SEMESTER_OF_YEAR_SECOND, suffixName, lstTeachingBo, j);
                                        }
                                        break;
                                    case "THỂ DỤC":
                                        sheetDiemHK2mtc.SetCellValue(8 + j, 3, diemMieng);
                                        sheetDiemHK2mtc.SetCellValue(8 + j, 4, diem15);
                                        sheetDiemHK2mtc.SetCellValue(8 + j, 5, diem1Tiet);
                                        sheetDiemHK2mtc.SetCellValue(8 + j, 6, diemHK);
                                        sheetDiemHK2mtc.SetCellValue(8 + j, 7, diemTBM);
                                        if (j == 0)
                                        {
                                            FillTeachingSubject(sheetDiemHK2mtc, "C72", thisSubject.SubjectID, GlobalConstants.SEMESTER_OF_YEAR_SECOND, suffixName, lstTeachingBo, j);
                                        }
                                        break;
                                    case "ÂM NHẠC":
                                        sheetDiemHK2mtc.SetCellValue(8 + j, 8, diemMieng);
                                        sheetDiemHK2mtc.SetCellValue(8 + j, 9, diem15);
                                        sheetDiemHK2mtc.SetCellValue(8 + j, 10, diem1Tiet);
                                        sheetDiemHK2mtc.SetCellValue(8 + j, 11, diemHK);
                                        sheetDiemHK2mtc.SetCellValue(8 + j, 12, diemTBM);
                                        if (j == 0)
                                        {
                                            FillTeachingSubject(sheetDiemHK2mtc, "H72", thisSubject.SubjectID, GlobalConstants.SEMESTER_OF_YEAR_SECOND, suffixName, lstTeachingBo, j);
                                        }
                                        break;
                                    case "MỸ THUẬT":
                                        sheetDiemHK2mtc.SetCellValue(8 + j, 13, diemMieng);
                                        sheetDiemHK2mtc.SetCellValue(8 + j, 14, diem15);
                                        sheetDiemHK2mtc.SetCellValue(8 + j, 15, diem1Tiet);
                                        sheetDiemHK2mtc.SetCellValue(8 + j, 16, diemHK);
                                        sheetDiemHK2mtc.SetCellValue(8 + j, 17, diemTBM);
                                        if (j == 0)
                                        {
                                            FillTeachingSubject(sheetDiemHK2mtc, "M72", thisSubject.SubjectID, GlobalConstants.SEMESTER_OF_YEAR_SECOND, suffixName, lstTeachingBo, j);
                                        }
                                        break;
                                    default:
                                        break;
                                }
                            }


                            #endregion


                        }
                    }
                    #endregion
                    #region mon tinh diem
                    else //mon tinh diem
                    {
                        if (thisSubject.SectionPerWeekSecondSemester > 0)
                        {
                            for (int j = 0; j < lsPP.Count(); j++)
                            {
                                int pupilID = lsPP[j].PupilProfileID;

                                bool showPupilData = lstPocHK2.Where(o => o.PupilID == pupilID).Count() > 0 ? true : false;
                                if (showPupilData)
                                {
                                    //fill du lieu diem
                                    List<MarkRecordBO> mrOfThisPupil = lsMR.Where(o => o.PupilID == pupilID).ToList();
                                    List<decimal?> lsdiemMieng = mrOfThisPupil.Where(o => o.Title == GlobalConstants.MARK_TYPE_M)
                                                                                .Where(o => o.SubjectID == thisSubject.SubjectID)
                                                                                .OrderBy(c => c.OrderNumber)
                                                                                .Select(o => o.Mark).ToList();
                                    List<decimal?> lsdiem15ph = mrOfThisPupil.Where(o => o.Title == GlobalConstants.MARK_TYPE_P)
                                                                            .Where(o => o.SubjectID == thisSubject.SubjectID)
                                                                            .OrderBy(o => o.OrderNumber)
                                                                            .Select(o => o.Mark).ToList();
                                    List<decimal?> lsdiem1t = mrOfThisPupil.Where(o => o.Title == GlobalConstants.MARK_TYPE_V)
                                                                            .Where(o => o.SubjectID == thisSubject.SubjectID)
                                                                            .OrderBy(o => o.OrderNumber)
                                                                            .Select(o => o.Mark).ToList();
                                    List<decimal?> lsdiemhk = mrOfThisPupil.Where(o => o.Title == GlobalConstants.MARK_TYPE_HK)
                                                                            .Where(o => o.SubjectID == thisSubject.SubjectID)
                                                                            .Select(o => o.Mark).ToList();
                                    decimal? lsDiemTBM = lsSUR.Where(o => o.PupilID == pupilID).Where(o => o.SubjectID == thisSubject.SubjectID).Select(o => o.SummedUpMark).FirstOrDefault();

                                    //List<string> strlsdiemMieng = lsdiemMieng.Select(s => string.Format("{0:0}", s)).ToList();
                                    List<string> strlsdiemMieng = ReportUtils.ConvertListMarkForM(lsdiemMieng, isSoHCM);
                                    List<string> strlsdiem15ph = ReportUtils.ConvertListMarkForV(lsdiem15ph, isSoHCM);
                                    List<string> strlsdiem1t = ReportUtils.ConvertListMarkForV(lsdiem1t, isSoHCM);
                                    List<string> strlsdiemhk = ReportUtils.ConvertListMarkForV(lsdiemhk, isSoHCM);


                                    string diemMieng = string.Join(" ", strlsdiemMieng.ToArray());
                                    string diem15 = string.Join(" ", strlsdiem15ph.ToArray());
                                    string diem1Tiet = string.Join(" ", strlsdiem1t.ToArray());
                                    string diemHK = string.Join(" ", strlsdiemhk.ToArray());
                                    string diemTBM = lsDiemTBM != null ? ReportUtils.ConvertMarkForV_SGTGD(lsDiemTBM.Value, isSoHCM) : string.Empty;

                                    #region Kiem tra mon hoc
                                    if (thisSubject.DisplayName.ToUpper() == subjectNN.ToUpper())
                                    {
                                        sheetDiemHK2_2.SetCellValue(8 + j, 20, diemMieng);
                                        sheetDiemHK2_2.SetCellValue(8 + j, 21, diem15);
                                        sheetDiemHK2_2.SetCellValue(8 + j, 22, diem1Tiet);
                                        sheetDiemHK2_2.SetCellValue(8 + j, 23, diemHK);
                                        sheetDiemHK2_2.SetCellValue(8 + j, 24, diemTBM);
                                        if (j == 0)
                                        {
                                            if (suffixName != null)
                                            {
                                                sheetDiemHK2_2.SetCellValue("T69", "Xác nhận điểm của GV " + thisSubject.DisplayName);
                                            }
                                            //sheetDiemHK2_2.SetCellValue("T70", "Giáo viên môn " + thisSubject.DisplayName);
                                            FillTeachingSubject(sheetDiemHK2_2, "T72", thisSubject.SubjectID, GlobalConstants.SEMESTER_OF_YEAR_SECOND, suffixName, lstTeachingBo, j);
                                        }
                                    }
                                    else if (scNN2 != null && thisSubject.DisplayName.ToUpper() == scNN2.DisplayName.ToUpper())
                                    {
                                        sheetDiemHK2mtc.SetCellValue(8 + j, 20, diemMieng);
                                        sheetDiemHK2mtc.SetCellValue(8 + j, 21, diem15);
                                        sheetDiemHK2mtc.SetCellValue(8 + j, 22, diem1Tiet);
                                        sheetDiemHK2mtc.SetCellValue(8 + j, 23, diemHK);
                                        sheetDiemHK2mtc.SetCellValue(8 + j, 24, diemTBM);
                                        if (j == 0)
                                        {
                                            FillTeachingSubject(sheetDiemHK2mtc, "T72", thisSubject.SubjectID, GlobalConstants.SEMESTER_OF_YEAR_SECOND, suffixName, lstTeachingBo, j);
                                        }
                                    }
                                    else if (thisSubject.DisplayName.ToUpper() == nameTuChon.ToUpper())
                                    {
                                        sheetDiemHK2mtc.SetCellValue(8 + j, 30, diemMieng);
                                        sheetDiemHK2mtc.SetCellValue(8 + j, 31, diem15);
                                        sheetDiemHK2mtc.SetCellValue(8 + j, 32, diem1Tiet);
                                        sheetDiemHK2mtc.SetCellValue(8 + j, 33, diemHK);
                                        sheetDiemHK2mtc.SetCellValue(8 + j, 34, diemTBM);
                                        if (j == 0)
                                        {
                                            if (suffixName != null)
                                            {
                                                sheetDiemHK2mtc.SetCellValue("AD69", "Xác nhận điểm của GV " + thisSubject.DisplayName);
                                            }
                                            //sheetDiemHK2mtc.SetCellValue("AD70", "Giáo viên môn " + thisSubject.DisplayName);
                                            FillTeachingSubject(sheetDiemHK2mtc, "T72", thisSubject.SubjectID, GlobalConstants.SEMESTER_OF_YEAR_SECOND, suffixName, lstTeachingBo, j);
                                        }
                                    }
                                    else if (thisSubject.DisplayName.ToUpper() == nameTH.ToUpper())
                                    {
                                        sheetDiemHK2mtc.SetCellValue(8 + j, 25, diemMieng);
                                        sheetDiemHK2mtc.SetCellValue(8 + j, 26, diem15);
                                        sheetDiemHK2mtc.SetCellValue(8 + j, 27, diem1Tiet);
                                        sheetDiemHK2mtc.SetCellValue(8 + j, 28, diemHK);
                                        sheetDiemHK2mtc.SetCellValue(8 + j, 29, diemTBM);
                                        if (j == 0)
                                        {
                                            FillTeachingSubject(sheetDiemHK2mtc, "Y72", thisSubject.SubjectID, GlobalConstants.SEMESTER_OF_YEAR_SECOND, suffixName, lstTeachingBo, j);
                                        }
                                    }
                                    else
                                    {
                                        switch (thisSubject.DisplayName.ToUpper())
                                        {
                                            case "TOÁN":
                                                sheetDiemHK2.SetCellValue(8 + j, 3, diemMieng);
                                                sheetDiemHK2.SetCellValue(8 + j, 4, diem15);
                                                sheetDiemHK2.SetCellValue(8 + j, 5, diem1Tiet);
                                                sheetDiemHK2.SetCellValue(8 + j, 6, diemHK);
                                                sheetDiemHK2.SetCellValue(8 + j, 7, diemTBM);
                                                if (j == 0)
                                                {
                                                    FillTeachingSubject(sheetDiemHK2, "D72", thisSubject.SubjectID, GlobalConstants.SEMESTER_OF_YEAR_SECOND, suffixName, lstTeachingBo, j);
                                                }
                                                break;
                                            case "VẬT LÍ":
                                                sheetDiemHK2.SetCellValue(8 + j, 8, diemMieng);
                                                sheetDiemHK2.SetCellValue(8 + j, 9, diem15);
                                                sheetDiemHK2.SetCellValue(8 + j, 10, diem1Tiet);
                                                sheetDiemHK2.SetCellValue(8 + j, 11, diemHK);
                                                sheetDiemHK2.SetCellValue(8 + j, 12, diemTBM);
                                                if (j == 0)
                                                {
                                                    FillTeachingSubject(sheetDiemHK2, "I72", thisSubject.SubjectID, GlobalConstants.SEMESTER_OF_YEAR_SECOND, suffixName, lstTeachingBo, j);
                                                }
                                                break;
                                            case "HÓA HỌC":
                                                sheetDiemHK2.SetCellValue(8 + j, 15, diemMieng);
                                                sheetDiemHK2.SetCellValue(8 + j, 16, diem15);
                                                sheetDiemHK2.SetCellValue(8 + j, 17, diem1Tiet);
                                                sheetDiemHK2.SetCellValue(8 + j, 18, diemHK);
                                                sheetDiemHK2.SetCellValue(8 + j, 19, diemTBM);
                                                if (j == 0)
                                                {
                                                    FillTeachingSubject(sheetDiemHK2, "P72", thisSubject.SubjectID, GlobalConstants.SEMESTER_OF_YEAR_SECOND, suffixName, lstTeachingBo, j);
                                                }
                                                break;
                                            case "SINH HỌC":
                                                sheetDiemHK2.SetCellValue(8 + j, 20, diemMieng);
                                                sheetDiemHK2.SetCellValue(8 + j, 21, diem15);
                                                sheetDiemHK2.SetCellValue(8 + j, 22, diem1Tiet);
                                                sheetDiemHK2.SetCellValue(8 + j, 23, diemHK);
                                                sheetDiemHK2.SetCellValue(8 + j, 24, diemTBM);
                                                if (j == 0)
                                                {
                                                    FillTeachingSubject(sheetDiemHK2, "U72", thisSubject.SubjectID, GlobalConstants.SEMESTER_OF_YEAR_SECOND, suffixName, lstTeachingBo, j);
                                                }
                                                break;
                                            case "NGỮ VĂN":
                                                sheetDiemHK2_2.SetCellValue(8 + j, 3, diemMieng);
                                                sheetDiemHK2_2.SetCellValue(8 + j, 4, diem15);
                                                sheetDiemHK2_2.SetCellValue(8 + j, 5, diem1Tiet);
                                                sheetDiemHK2_2.SetCellValue(8 + j, 6, diemHK);
                                                sheetDiemHK2_2.SetCellValue(8 + j, 7, diemTBM);
                                                if (j == 0)
                                                {
                                                    FillTeachingSubject(sheetDiemHK2_2, "D72", thisSubject.SubjectID, GlobalConstants.SEMESTER_OF_YEAR_SECOND, suffixName, lstTeachingBo, j);
                                                }
                                                break;
                                            case "LỊCH SỬ":
                                                sheetDiemHK2_2.SetCellValue(8 + j, 8, diemMieng);
                                                sheetDiemHK2_2.SetCellValue(8 + j, 9, diem15);
                                                sheetDiemHK2_2.SetCellValue(8 + j, 10, diem1Tiet);
                                                sheetDiemHK2_2.SetCellValue(8 + j, 11, diemHK);
                                                sheetDiemHK2_2.SetCellValue(8 + j, 12, diemTBM);
                                                if (j == 0)
                                                {
                                                    FillTeachingSubject(sheetDiemHK2_2, "I72", thisSubject.SubjectID, GlobalConstants.SEMESTER_OF_YEAR_SECOND, suffixName, lstTeachingBo, j);
                                                }
                                                break;
                                            case "ĐỊA LÍ":
                                                sheetDiemHK2_2.SetCellValue(8 + j, 15, diemMieng);
                                                sheetDiemHK2_2.SetCellValue(8 + j, 16, diem15);
                                                sheetDiemHK2_2.SetCellValue(8 + j, 17, diem1Tiet);
                                                sheetDiemHK2_2.SetCellValue(8 + j, 18, diemHK);
                                                sheetDiemHK2_2.SetCellValue(8 + j, 19, diemTBM);
                                                if (j == 0)
                                                {
                                                    FillTeachingSubject(sheetDiemHK2_2, "O72", thisSubject.SubjectID, GlobalConstants.SEMESTER_OF_YEAR_SECOND, suffixName, lstTeachingBo, j);
                                                }
                                                break;
                                            case "GDCD":
                                                sheetDiemHK2_2.SetCellValue(8 + j, 25, diemMieng);
                                                sheetDiemHK2_2.SetCellValue(8 + j, 26, diem15);
                                                sheetDiemHK2_2.SetCellValue(8 + j, 27, diem1Tiet);
                                                sheetDiemHK2_2.SetCellValue(8 + j, 28, diemHK);
                                                sheetDiemHK2_2.SetCellValue(8 + j, 29, diemTBM);
                                                if (j == 0)
                                                {
                                                    FillTeachingSubject(sheetDiemHK2_2, "Y72", thisSubject.SubjectID, GlobalConstants.SEMESTER_OF_YEAR_SECOND, suffixName, lstTeachingBo, j);
                                                }
                                                break;
                                            case "CÔNG NGHỆ":
                                                sheetDiemHK2_2.SetCellValue(8 + j, 30, diemMieng);
                                                sheetDiemHK2_2.SetCellValue(8 + j, 31, diem15);
                                                sheetDiemHK2_2.SetCellValue(8 + j, 32, diem1Tiet);
                                                sheetDiemHK2_2.SetCellValue(8 + j, 33, diemHK);
                                                sheetDiemHK2_2.SetCellValue(8 + j, 34, diemTBM);
                                                if (j == 0)
                                                {
                                                    FillTeachingSubject(sheetDiemHK2_2, "AE72", thisSubject.SubjectID, GlobalConstants.SEMESTER_OF_YEAR_SECOND, suffixName, lstTeachingBo, j);
                                                }
                                                break;
                                            case "THỂ DỤC":
                                                sheetDiemHK2mtc.SetCellValue(8 + j, 3, diemMieng);
                                                sheetDiemHK2mtc.SetCellValue(8 + j, 4, diem15);
                                                sheetDiemHK2mtc.SetCellValue(8 + j, 5, diem1Tiet);
                                                sheetDiemHK2mtc.SetCellValue(8 + j, 6, diemHK);
                                                sheetDiemHK2mtc.SetCellValue(8 + j, 7, diemTBM);
                                                if (j == 0)
                                                {
                                                    FillTeachingSubject(sheetDiemHK2mtc, "C72", thisSubject.SubjectID, GlobalConstants.SEMESTER_OF_YEAR_SECOND, suffixName, lstTeachingBo, j);
                                                }
                                                break;
                                            case "ÂM NHẠC":
                                                sheetDiemHK2mtc.SetCellValue(8 + j, 8, diemMieng);
                                                sheetDiemHK2mtc.SetCellValue(8 + j, 9, diem15);
                                                sheetDiemHK2mtc.SetCellValue(8 + j, 10, diem1Tiet);
                                                sheetDiemHK2mtc.SetCellValue(8 + j, 11, diemHK);
                                                sheetDiemHK2mtc.SetCellValue(8 + j, 12, diemTBM);
                                                if (j == 0)
                                                {
                                                    FillTeachingSubject(sheetDiemHK2mtc, "H72", thisSubject.SubjectID, GlobalConstants.SEMESTER_OF_YEAR_SECOND, suffixName, lstTeachingBo, j);
                                                }
                                                break;
                                            case "MỸ THUẬT":
                                                sheetDiemHK2mtc.SetCellValue(8 + j, 13, diemMieng);
                                                sheetDiemHK2mtc.SetCellValue(8 + j, 14, diem15);
                                                sheetDiemHK2mtc.SetCellValue(8 + j, 15, diem1Tiet);
                                                sheetDiemHK2mtc.SetCellValue(8 + j, 16, diemHK);
                                                sheetDiemHK2mtc.SetCellValue(8 + j, 17, diemTBM);
                                                if (j == 0)
                                                {
                                                    FillTeachingSubject(sheetDiemHK2mtc, "M72", thisSubject.SubjectID, GlobalConstants.SEMESTER_OF_YEAR_SECOND, suffixName, lstTeachingBo, j);
                                                }
                                                break;
                                            default:
                                                break;
                                        }
                                    }

                                }
                                    #endregion

                            }
                        }
                    }

                    #endregion
                }
                #endregion
                #region Fill dữ liệu sheetDiemTKHKII

                Dictionary<string, object> HK2TotalMarkDic = new Dictionary<string, object>();
                List<object> insideDicTK2 = new List<object>();
                Dictionary<string, object> innerDicTK2 = new Dictionary<string, object>();
                foreach (ClassSubjectBO cs in lsAllSubject)
                {
                    string position = SetDisplaySubjectNX12_THCS(cs.DisplayName);
                    if (cs.IsCommenting == SystemParamsInFile.ISCOMMENTING_TYPE_JUDGE)
                    {
                        if (cs.Abbreviation.ToUpper() == "CN")
                        {
                            sheetDiemTKHK2.SetCellValue(3, 12, "CN(loại)");
                        }
                        else
                        {
                            string ouput = cs.DisplayName + "(loại)";

                            if (position.Length > 0)
                            {
                                sheetDiemTKHK2.SetCellValue(position, ouput);
                            }
                        }
                    }
                    if (cs.DisplayName.ToUpper() == nameTuChon.ToUpper())
                    {
                        if (position.Length > 0)
                        {
                            sheetDiemTKHK2.SetCellValue(position, "");
                        }
                    }
                }
                if (classsubjectNN != null)
                {
                    if (classsubjectNN.IsCommenting == SystemParamsInFile.ISCOMMENTING_TYPE_JUDGE)
                    {
                        sheetDiemTKHK2.SetCellValue(3, 10, classsubjectNN.DisplayName + "(loại)");
                    }
                    else
                    {
                        sheetDiemTKHK2.SetCellValue(3, 10, classsubjectNN.DisplayName);
                    }
                }

                if (scTH != null)
                {
                    if (scTH.IsCommenting == SystemParamsInFile.ISCOMMENTING_TYPE_JUDGE)
                    {
                        sheetDiemTKHK2.SetCellValue("Q4", scTH.DisplayName + "(loại)");
                    }
                }
                if (scTC != null)
                {

                    if (scTC.IsCommenting == SystemParamsInFile.ISCOMMENTING_TYPE_JUDGE)
                    {
                        sheetDiemTKHK2.SetCellValue("R4", scTC.DisplayName + "(loại)");
                    }
                    else
                    {
                        sheetDiemTKHK2.SetCellValue("R4", scTC.DisplayName);
                    }
                }

                for (int j = 0; j < lsPP.Count(); j++)
                {

                    innerDicTK2 = new Dictionary<string, object>();
                    int pupilID = lsPP[j].PupilProfileID;


                    bool showPupilData = lstPocHK2.Where(o => o.PupilID == pupilID).Count() > 0 ? true : false;
                    if (showPupilData)
                    {
                        PupilRankingBO pupilRanking = lsPR.Where(o => o.PupilID == pupilID).FirstOrDefault();
                        if (pupilRanking != null)
                        {
                            string diemTBCM = pupilRanking.AverageMark.HasValue ? ReportUtils.ConvertMarkForV_SGTGD(pupilRanking.AverageMark.Value, isSoHCM) : string.Empty;
                            innerDicTK2.Add("TBCM", diemTBCM);

                            innerDicTK2.Add("HL", pupilRanking.CapacityLevel != null ? UtilsBusiness.ConvertCapacity(pupilRanking.CapacityLevel) : "");

                            innerDicTK2.Add("HK", pupilRanking.ConductLevelName != null ? UtilsBusiness.ConvertCapacity(pupilRanking.ConductLevelName) : "");
                        }
                        else
                        {
                            innerDicTK2.Add("TBCM", "");

                            innerDicTK2.Add("HL", "");

                            innerDicTK2.Add("HK", "");
                        }
                        PupilEmulationBO pe = lsPE.Where(o => o.PupilID == pupilID).FirstOrDefault();
                        string lsTD = "";
                        if (pe != null && pe.HonourAchivementTypeResolution != null)
                        {
                            lsTD = UtilsBusiness.ConvertCapacity(pe.HonourAchivementTypeResolution);
                        }

                        innerDicTK2.Add("TD", lsTD);

                        for (int i = 0; i < lsAllSubject.Count(); i++)
                        {
                            ClassSubjectBO thisSubject = lsAllSubject[i];
                            //mon bat buoc
                            if (thisSubject.SectionPerWeekSecondSemester > 0)
                            {
                                if (thisSubject.IsCommenting == SystemParamsInFile.ISCOMMENTING_TYPE_JUDGE)//mon nhan xet
                                {
                                    //fill du lieu diem
                                    List<string> lsDiemTBM = lsSUR.Where(o => o.PupilID == pupilID).Where(o => o.SubjectID == thisSubject.SubjectID).Select(o => o.JudgementResult).ToList();
                                    string diemTBM = string.Join(" ", lsDiemTBM.ToArray());
                                    //innerDicTK1.Add("SubjectTBM" + thisSubject.SubjectID.ToString(), diemTBM);

                                    if (thisSubject.DisplayName.ToUpper() == subjectNN.ToUpper())
                                    {
                                        sheetDiemTKHK2.SetCellValue(5 + j, 10, diemTBM);
                                    }
                                    else if (thisSubject.DisplayName.ToUpper() == nameNN2.ToUpper())
                                    {
                                        innerDicTK2["NN2"] = diemTBM;
                                    }
                                    else if (thisSubject.DisplayName.ToUpper() == nameTH.ToUpper())
                                    {
                                        innerDicTK2["TH"] = diemTBM;
                                    }
                                    else if (thisSubject.DisplayName.ToUpper() == nameTuChon.ToUpper())
                                    {
                                        innerDicTK2["TC"] = diemTBM;
                                    }
                                    else if (thisSubject.Abbreviation.ToUpper() == "CN")
                                    {
                                        sheetDiemTKHK2.SetCellValue(5 + j, 12, diemTBM);
                                    }
                                    else
                                    {
                                        switch (thisSubject.DisplayName.ToUpper())
                                        {
                                            case "TOÁN":
                                                sheetDiemTKHK2.SetCellValue(5 + j, 3, diemTBM);
                                                break;
                                            case "VẬT LÍ":
                                                sheetDiemTKHK2.SetCellValue(5 + j, 4, diemTBM);
                                                break;
                                            case "HÓA HỌC":
                                                sheetDiemTKHK2.SetCellValue(5 + j, 5, diemTBM);
                                                break;
                                            case "SINH HỌC":
                                                sheetDiemTKHK2.SetCellValue(5 + j, 6, diemTBM);
                                                break;
                                            case "NGỮ VĂN":
                                                sheetDiemTKHK2.SetCellValue(5 + j, 7, diemTBM);
                                                break;
                                            case "LỊCH SỬ":
                                                sheetDiemTKHK2.SetCellValue(5 + j, 8, diemTBM);
                                                break;
                                            case "ĐỊA LÍ":
                                                sheetDiemTKHK2.SetCellValue(5 + j, 9, diemTBM);
                                                break;
                                            case "GDCD":
                                                sheetDiemTKHK2.SetCellValue(5 + j, 11, diemTBM);
                                                break;
                                            case "THỂ DỤC":
                                                sheetDiemTKHK2.SetCellValue(5 + j, 13, diemTBM);
                                                break;
                                            case "ÂM NHẠC":
                                                sheetDiemTKHK2.SetCellValue(5 + j, 14, diemTBM);
                                                break;
                                            case "MỸ THUẬT":
                                                sheetDiemTKHK2.SetCellValue(5 + j, 15, diemTBM);
                                                break;
                                            default:
                                                break;
                                        }
                                    }

                                }
                                else //mon tinh diem
                                {
                                    //fill du lieu diem
                                    decimal? DiemTBM = lsSUR.Where(o => o.PupilID == pupilID).Where(o => o.SubjectID == thisSubject.SubjectID).Select(o => o.SummedUpMark).FirstOrDefault();
                                    string diemTBM = "";
                                    if (DiemTBM != null)
                                    {
                                        diemTBM = ReportUtils.ConvertMarkForV_SGTGD(DiemTBM.Value, isSoHCM);
                                    }
                                    //innerDicTK1.Add("SubjectTBM" + thisSubject.SubjectID.ToString(), diemTBM);  
                                    if (thisSubject.DisplayName.ToUpper() == subjectNN.ToUpper())
                                    {
                                        sheetDiemTKHK2.SetCellValue(5 + j, 10, diemTBM);
                                    }
                                    else if (thisSubject.DisplayName.ToUpper() == nameNN2.ToUpper())
                                    {
                                        innerDicTK2["NN2"] = diemTBM;
                                    }
                                    else if (thisSubject.DisplayName.ToUpper() == nameTH.ToUpper())
                                    {
                                        innerDicTK2["TH"] = diemTBM;
                                    }
                                    else if (thisSubject.DisplayName.ToUpper() == nameTuChon.ToUpper())
                                    {
                                        innerDicTK2["TC"] = diemTBM;
                                    }
                                    else if (thisSubject.Abbreviation.ToUpper() == "CN")
                                    {
                                        sheetDiemTKHK2.SetCellValue(5 + j, 12, diemTBM);
                                    }
                                    else
                                    {
                                        switch (thisSubject.DisplayName.ToUpper())
                                        {
                                            case "TOÁN":
                                                sheetDiemTKHK2.SetCellValue(5 + j, 3, diemTBM);
                                                break;
                                            case "VẬT LÍ":
                                                sheetDiemTKHK2.SetCellValue(5 + j, 4, diemTBM);
                                                break;
                                            case "HÓA HỌC":
                                                sheetDiemTKHK2.SetCellValue(5 + j, 5, diemTBM);
                                                break;
                                            case "SINH HỌC":
                                                sheetDiemTKHK2.SetCellValue(5 + j, 6, diemTBM);
                                                break;
                                            case "NGỮ VĂN":
                                                sheetDiemTKHK2.SetCellValue(5 + j, 7, diemTBM);
                                                break;
                                            case "LỊCH SỬ":
                                                sheetDiemTKHK2.SetCellValue(5 + j, 8, diemTBM);
                                                break;
                                            case "ĐỊA LÍ":
                                                sheetDiemTKHK2.SetCellValue(5 + j, 9, diemTBM);
                                                break;
                                            case "GDCD":
                                                sheetDiemTKHK2.SetCellValue(5 + j, 11, diemTBM);
                                                break;
                                            case "THỂ DỤC":
                                                sheetDiemTKHK2.SetCellValue(5 + j, 13, diemTBM);
                                                break;
                                            case "ÂM NHẠC":
                                                sheetDiemTKHK2.SetCellValue(5 + j, 14, diemTBM);
                                                break;
                                            case "MỸ THUẬT":
                                                sheetDiemTKHK2.SetCellValue(5 + j, 15, diemTBM);
                                                break;
                                            default:
                                                break;
                                        }
                                    }

                                }
                            }

                        }
                    }
                    insideDicTK2.Add(innerDicTK2);
                }
                HK2TotalMarkDic.Add("Rows", insideDicTK2);
                sheetDiemTKHK2.FillVariableValue(HK2TotalMarkDic);
                #endregion sheetDiemTKHK2
            }

            #region SheetTong ket Ca Nam
            if (entity.Semester == SystemParamsInFile.SEMESTER_OF_YEAR_ALL)
            {
                //Lấy style cho sheet tổng kết cả năm
                IVTRange rangeStudyingTKCN = sheetTongKetCaNamTemp.GetRange("A7", "AF7");
                IVTRange rangeNotStudyingTKCN = sheetTongKetCaNamTemp.GetRange("A6", "AF6");
                IVTRange rangeStudyingDevideTKCN = sheetTongKetCaNamTemp.GetRange("A15", "AF15");
                IVTRange rangeNotStudyingDevideTKCN = sheetTongKetCaNamTemp.GetRange("A10", "AF10");
                foreach (ClassSubjectBO cs in lsAllSubject)
                {
                    string position = SetDisplaySubjectNX_THCS(cs.DisplayName);
                    if (cs.IsCommenting == SystemParamsInFile.ISCOMMENTING_TYPE_JUDGE)
                    {
                        if (cs.Abbreviation.ToUpper() == "CN")
                        {
                            sheetTongKetCaNam.SetCellValue(4, 12, "CN(loại)");
                        }
                        else
                        {
                            string ouput = cs.DisplayName + "(loại)";

                            if (position.Length > 0)
                            {
                                sheetTongKetCaNam.SetCellValue(position, ouput);
                            }
                        }
                    }
                    if (cs.DisplayName.ToUpper() == nameTuChon.ToUpper())
                    {
                        if (position.Length > 0)
                        {
                            sheetTongKetCaNam.SetCellValue(position, "");
                        }
                    }
                }
                if (classsubjectNN != null)
                {
                    if (classsubjectNN.IsCommenting == SystemParamsInFile.ISCOMMENTING_TYPE_JUDGE)
                    {
                        sheetTongKetCaNam.SetCellValue("J4", classsubjectNN.DisplayName + "(loại)");
                    }
                    else
                    {
                        sheetTongKetCaNam.SetCellValue("J4", classsubjectNN.DisplayName);
                    }
                }

                if (scTH != null)
                {
                    if (scTH.IsCommenting == SystemParamsInFile.ISCOMMENTING_TYPE_JUDGE)
                    {
                        sheetTongKetCaNam.SetCellValue("Q5", scTH.DisplayName + "(loại)");
                    }
                }
                if (scTC != null)
                {

                    if (scTC.IsCommenting == SystemParamsInFile.ISCOMMENTING_TYPE_JUDGE)
                    {
                        sheetTongKetCaNam.SetCellValue("R5", scTC.DisplayName + "(loại)");
                    }
                    else
                    {
                        sheetTongKetCaNam.SetCellValue("R5", scTC.DisplayName);
                    }
                }
                //Lấy danh sách điểm cả năm của học sinh
                //diem tbm
                List<SummedUpRecordBO> lsSUR = iqSummedUpRecord.Where(o => o.Semester == SystemParamsInFile.SEMESTER_OF_YEAR_ALL).ToList();
                //Điểm học sinh thi lại 
                List<SummedUpRecordBO> lsSUR_Retest = iqSummedUpRecord.Where(o => o.Semester == SystemParamsInFile.SEMESTER_OF_YEAR_RETEST).ToList();
                //Điểm TBCM, HL, HK, TD
                List<PupilRankingBO> lsPR = iqPupilRanking.Where(o => o.Semester == SystemParamsInFile.SEMESTER_OF_YEAR_ALL).ToList();

                //diem tk TD: PupilEmulationBusiness.SearchBySchool(SchoolID, Dictionary)
                List<PupilEmulationBO> lsPE = iqPupilEmulation.Where(o => o.Semester == SystemParamsInFile.SEMESTER_OF_YEAR_ALL).ToList();

                List<PupilRankingBO> lsPR_Retest = iqPupilRanking.Where(o => o.Semester == SystemParamsInFile.SEMESTER_OF_YEAR_BACKTRAINING).ToList();

                //Điểm kiểm tra lại, nếu nhiều hơn 3 môn thì chỉ lấy 3 môn học đầu tiên theo số thứ tự môn học
                IDictionary<string, object> dicPupilRetest = new Dictionary<string, object>();
                dicPupilRetest["AcademicYearID"] = entity.AcademicYearID;
                dicPupilRetest["ClassID"] = entity.ClassID;
                IQueryable<PupilRetestRegistration> iqPupilRegistration = PupilRetestRegistrationBusiness.SearchBySchool(entity.SchoolID, dicPupilRetest);
                List<PupilRetestRegistration> lstPupilRetestRegistration = iqPupilRegistration.ToList();

                List<ClassSubjectBO> lstSubjectRetest = (from p in iqPupilRegistration
                                                         join r in ClassSubjectBusiness.All on new { p.SubjectID, p.ClassID } equals new { r.SubjectID, r.ClassID }
                                                         join q in SubjectCatBusiness.All on p.SubjectID equals q.SubjectCatID
                                                         where r.Last2digitNumberSchool == partitionId
                                                         orderby q.OrderInSubject
                                                         select new ClassSubjectBO
                                                         {
                                                             SubjectID = q.SubjectCatID,
                                                             DisplayName = q.DisplayName,
                                                             IsCommenting = r.IsCommenting,
                                                             OrderInSubject = q.OrderInSubject
                                                         }).Distinct().ToList();
                lstSubjectRetest = lstSubjectRetest.OrderBy(o => o.OrderInSubject).ToList();
                List<int> lsScRetest = lstSubjectRetest.Select(o => o.SubjectID).ToList();
                countSubjectRetest = lstSubjectRetest.Count();
                if (countSubjectRetest > 3)
                {
                    List<int> lstPPRetest = lstPupilRetestRegistration.Select(p => p.PupilID).Distinct().ToList();
                    List<PupilProfileBO> lstPupilReset = lsPP.Where(p => lstPPRetest.Contains(p.PupilProfileID)).ToList();
                    FillSubjectReTest(sheetDiemThiLai, lstSubjectRetest, lstPupilReset, lsSUR_Retest, isSoHCM);
                }
                else
                {
                    for (int i = 0; i < lstSubjectRetest.Count(); i++)
                    {
                        ClassSubjectBO sc = lstSubjectRetest[i];
                        //Điền tên môn thi lại 
                        sheetTongKetCaNam.SetCellValue(5, 20 + i, sc.DisplayName);
                    }
                }

                Dictionary<string, object> dicTKCN = new Dictionary<string, object>();
                List<object> insideTKCN = new List<object>();
                Dictionary<string, object> resultTKCN = new Dictionary<string, object>();
                int countLL = 0;
                int countOLL = 0;
                int countLLSTL = 0;
                //Xóa môn nếu môn học bắt buộc đó là môn tự chọn

                for (int i = 0; i < countPP; i++)
                {
                    PupilProfileBO pp = lsPP[i];
                    int pupilID = pp.PupilProfileID;
                    dicTKCN = new Dictionary<string, object>();
                    if (pp.ProfileStatus == SystemParamsInFile.PUPIL_STATUS_STUDYING || pp.ProfileStatus == SystemParamsInFile.PUPIL_STATUS_GRADUATED)
                    {

                        if (i % 5 == 4)
                        {
                            sheetTongKetCaNam.CopyPasteSameSize(rangeStudyingDevideTKCN, 6 + i, 1);
                        }
                        else
                        {
                            sheetTongKetCaNam.CopyPasteSameSize(rangeStudyingTKCN, 6 + i, 1);
                        }
                    }
                    else
                    {
                        if (i % 5 == 4)
                        {
                            sheetTongKetCaNam.CopyPasteSameSize(rangeNotStudyingDevideTKCN, 6 + i, 1);
                        }
                        else
                        {
                            sheetTongKetCaNam.CopyPasteSameSize(rangeNotStudyingTKCN, 6 + i, 1);
                        }
                    }

                    sheetTongKetCaNam.SetCellValue(6 + i, 1, i + 1);
                    sheetTongKetCaNam.SetCellValue(6 + i, 24, i + 1);
                    sheetTongKetCaNam.SetCellValue(6 + i, 2, pp.FullName);
                    bool showData = lstPocHK2.Where(o => o.PupilID == pupilID).Count() > 0 ? true : false;
                    bool showRetest = lstPupilRetestRegistration.Where(o => o.PupilID == pupilID && lsScRetest.Contains(o.SubjectID)).Count() > 0 ? true : false;
                    if (showData)
                    {
                        if (showRetest && countSubjectRetest <= 3)
                        {
                            for (int j = 0; j < lstSubjectRetest.Count(); j++)
                            {
                                ClassSubjectBO thisSubject = lstSubjectRetest[j];
                                if (thisSubject.IsCommenting == SystemParamsInFile.ISCOMMENTING_TYPE_JUDGE)
                                {
                                    List<string> lsDiemTBM = lsSUR_Retest.Where(o => o.PupilID == pupilID && o.SubjectID == thisSubject.SubjectID).Select(o => o.ReTestJudgement).ToList();
                                    string diemTBM = string.Join(" ", lsDiemTBM.ToArray());
                                    sheetTongKetCaNam.SetCellValue(6 + i, 20 + j, diemTBM);
                                }
                                else
                                {
                                    decimal? DiemTBM = lsSUR_Retest.Where(o => o.PupilID == pupilID).Where(o => o.SubjectID == thisSubject.SubjectID).Select(o => o.ReTestMark).FirstOrDefault();
                                    string diemTBM = "";
                                    if (DiemTBM != null)
                                    {
                                        diemTBM = ReportUtils.ConvertMarkForV_SGTGD(DiemTBM.Value, isSoHCM);
                                    }
                                    sheetTongKetCaNam.SetCellValue(6 + i, 20 + j, diemTBM);
                                }
                            }
                        }
                        for (int j = 0; j < lsAllSubject.Count(); j++)
                        {
                            ClassSubjectBO thisSubject = lsAllSubject[j];
                            //mon bat buoc
                            if (thisSubject.IsCommenting == SystemParamsInFile.ISCOMMENTING_TYPE_JUDGE)//mon nhan xet
                            {
                                //fill du lieu diem
                                List<string> lsDiemTBM = lsSUR.Where(o => o.PupilID == pupilID).Where(o => o.SubjectID == thisSubject.SubjectID).Select(o => o.JudgementResult).ToList();
                                string diemTBM = string.Join(" ", lsDiemTBM.ToArray());
                                //innerDicTK1.Add("SubjectTBM" + thisSubject.SubjectID.ToString(), diemTBM);
                                if (thisSubject.DisplayName.ToUpper() == nameTuChon.ToUpper())
                                {
                                    dicTKCN["TC"] = diemTBM;
                                }
                                else if (thisSubject.DisplayName.ToUpper() == nameNN2.ToUpper())
                                {
                                    dicTKCN["NN2"] = diemTBM;
                                }
                                else if (thisSubject.DisplayName.ToUpper() == nameTH.ToUpper())
                                {
                                    dicTKCN["TH"] = diemTBM;
                                }
                                else if (thisSubject.DisplayName.ToUpper() == subjectNN.ToUpper())
                                {
                                    sheetTongKetCaNam.SetCellValue(6 + i, 10, diemTBM);
                                }
                                else if (thisSubject.Abbreviation.ToUpper() == "CN")
                                {
                                    sheetTongKetCaNam.SetCellValue(6 + i, 12, diemTBM);
                                }
                                else
                                {
                                    switch (thisSubject.DisplayName.ToUpper())
                                    {
                                        case "TOÁN":
                                            sheetTongKetCaNam.SetCellValue(6 + i, 3, diemTBM);
                                            break;
                                        case "VẬT LÍ":
                                            sheetTongKetCaNam.SetCellValue(6 + i, 4, diemTBM);
                                            break;
                                        case "HÓA HỌC":
                                            sheetTongKetCaNam.SetCellValue(6 + i, 5, diemTBM);
                                            break;
                                        case "SINH HỌC":
                                            sheetTongKetCaNam.SetCellValue(6 + i, 6, diemTBM);
                                            break;
                                        case "NGỮ VĂN":
                                            sheetTongKetCaNam.SetCellValue(6 + i, 7, diemTBM);
                                            break;
                                        case "LỊCH SỬ":
                                            sheetTongKetCaNam.SetCellValue(6 + i, 8, diemTBM);
                                            break;
                                        case "ĐỊA LÍ":
                                            sheetTongKetCaNam.SetCellValue(6 + i, 9, diemTBM);
                                            break;
                                        case "GDCD":
                                            sheetTongKetCaNam.SetCellValue(6 + i, 11, diemTBM);
                                            break;
                                        case "THỂ DỤC":
                                            sheetTongKetCaNam.SetCellValue(6 + i, 13, diemTBM);
                                            break;
                                        case "ÂM NHẠC":
                                            sheetTongKetCaNam.SetCellValue(6 + i, 14, diemTBM);
                                            break;
                                        case "MỸ THUẬT":
                                            sheetTongKetCaNam.SetCellValue(6 + i, 15, diemTBM);
                                            break;
                                        default:
                                            break;
                                    }
                                }

                            }
                            else //mon tinh diem
                            {
                                //fill du lieu diem
                                decimal? DiemTBM = lsSUR.Where(o => o.PupilID == pupilID).Where(o => o.SubjectID == thisSubject.SubjectID).Select(o => o.SummedUpMark).FirstOrDefault();
                                string diemTBM = "";
                                if (DiemTBM != null)
                                {
                                    diemTBM = ReportUtils.ConvertMarkForV_SGTGD(DiemTBM.Value, isSoHCM);
                                }
                                //innerDicTK1.Add("SubjectTBM" + thisSubject.SubjectID.ToString(), diemTBM);  
                                if (thisSubject.DisplayName.ToUpper() == nameTuChon.ToUpper())
                                {
                                    dicTKCN["TC"] = diemTBM;
                                }
                                else if (thisSubject.DisplayName.ToUpper() == nameNN2.ToUpper())
                                {
                                    dicTKCN["NN2"] = diemTBM;
                                }
                                else if (thisSubject.DisplayName.ToUpper() == nameTH.ToUpper())
                                {
                                    dicTKCN["TH"] = diemTBM;
                                }
                                else if (thisSubject.DisplayName.ToUpper() == subjectNN.ToUpper())
                                {
                                    sheetTongKetCaNam.SetCellValue(6 + i, 10, diemTBM);
                                }
                                else if (thisSubject.Abbreviation.ToUpper() == "CN")
                                {
                                    sheetTongKetCaNam.SetCellValue(6 + i, 12, diemTBM);
                                }
                                else
                                {
                                    switch (thisSubject.DisplayName.ToUpper())
                                    {
                                        case "TOÁN":
                                            sheetTongKetCaNam.SetCellValue(6 + i, 3, diemTBM);
                                            break;
                                        case "VẬT LÍ":
                                            sheetTongKetCaNam.SetCellValue(6 + i, 4, diemTBM);
                                            break;
                                        case "HÓA HỌC":
                                            sheetTongKetCaNam.SetCellValue(6 + i, 5, diemTBM);
                                            break;
                                        case "SINH HỌC":
                                            sheetTongKetCaNam.SetCellValue(6 + i, 6, diemTBM);
                                            break;
                                        case "NGỮ VĂN":
                                            sheetTongKetCaNam.SetCellValue(6 + i, 7, diemTBM);
                                            break;
                                        case "LỊCH SỬ":
                                            sheetTongKetCaNam.SetCellValue(6 + i, 8, diemTBM);
                                            break;
                                        case "ĐỊA LÍ":
                                            sheetTongKetCaNam.SetCellValue(6 + i, 9, diemTBM);
                                            break;
                                        case "GDCD":
                                            sheetTongKetCaNam.SetCellValue(6 + i, 11, diemTBM);
                                            break;
                                        case "THỂ DỤC":
                                            sheetTongKetCaNam.SetCellValue(6 + i, 13, diemTBM);
                                            break;
                                        case "ÂM NHẠC":
                                            sheetTongKetCaNam.SetCellValue(6 + i, 14, diemTBM);
                                            break;
                                        case "MỸ THUẬT":
                                            sheetTongKetCaNam.SetCellValue(6 + i, 15, diemTBM);
                                            break;
                                        default:
                                            break;
                                    }
                                }

                            }
                        }
                        //Điền thông tin trung bình các môn, học lực, hạnh kiểm, danh hiệu, lên lớp, không lên lớp
                        PupilRankingBO pr = lsPR.Where(o => o.PupilID == pupilID).FirstOrDefault();
                        PupilEmulationBO pe = lsPE.Where(o => o.PupilID == pupilID).FirstOrDefault();
                        PupilRankingBO pr_Retest = lsPR_Retest.Where(o => o.PupilID == pupilID).FirstOrDefault();
                        PupilGraduation pg = lstPgAll.FirstOrDefault(o => o.PupilID == pupilID);
                        if (pr != null)
                        {
                            string averageMark = pr.AverageMark.HasValue ? ReportUtils.ConvertMarkForV_SGTGD(pr.AverageMark.Value, isSoHCM) : string.Empty;
                            string averageMarkRetest = "";


                            string capacity = pr.CapacityLevel != null ? UtilsBusiness.ConvertCapacity(pr.CapacityLevel) : string.Empty;
                            dicTKCN["HL"] = capacity;
                            string conduct = pr.ConductLevelName != null ? UtilsBusiness.ConvertCapacity(pr.ConductLevelName) : string.Empty;
                            dicTKCN["HK"] = conduct;

                            //viethd31: neu co thong tin tot nghiep thi lay thong tin tot nghiep
                            if (pg != null)
                            {
                                if (pg.Status.HasValue)
                                {
                                    if (pg.Status == SystemParamsInFile.GRADUATION_STATUS_APPROVE_SUFFICIENT)
                                    {
                                        dicTKCN.Add("LL", "Đủ điều kiện tốt nghiệp");
                                        countLL++;
                                        dicTKCN.Add("KLL", "");
                                    }
                                    else
                                    {
                                        dicTKCN.Add("LL", "");
                                        dicTKCN.Add("KLL", "");
                                    }
                                }
                                else
                                {
                                    dicTKCN.Add("LL", "");
                                    dicTKCN.Add("KLL", "");
                                }
                            }
                            else if (pr_Retest == null)
                            {

                                if (pr.StudyingJudgementID != null && pr.StudyingJudgementID > 0)
                                {
                                    if (pr.StudyingJudgementID == SystemParamsInFile.STUDYING_JUDGEMENT_UPCLASS || pr.StudyingJudgementID == SystemParamsInFile.STUDYING_JUDGEMENT_GRADUATION)
                                    {
                                        if (pr.EducationLevelID == 9)
                                        {
                                            dicTKCN.Add("LL", "Đủ ĐK xét duyệt TN");
                                        }
                                        else
                                        {
                                            dicTKCN.Add("LL", UtilsBusiness.ConvertCapacity(pr.StudyingJudgementResolution));
                                        }
                                        countLL++;
                                        dicTKCN.Add("KLL", "");
                                    }
                                    else if (pr.StudyingJudgementID == SystemParamsInFile.STUDYING_JUDGEMENT_STAYCLASS)
                                    {
                                        countOLL++;
                                        dicTKCN.Add("LL", "");
                                        dicTKCN.Add("KLL", UtilsBusiness.ConvertCapacity(pr.StudyingJudgementResolution));
                                    }
                                    else if (pr.StudyingJudgementID == SystemParamsInFile.STUDYING_JUDGEMENT_RETEST)
                                    {
                                        dicTKCN.Add("KLL", "Thi lại");
                                    }
                                    else if (pr.StudyingJudgementID == SystemParamsInFile.STUDYING_JUDGEMENT_BACKTRAINING)
                                    {
                                        dicTKCN.Add("KLL", "Rèn luyện lại");
                                    }
                                    else
                                    {
                                        dicTKCN.Add("LL", "");
                                        dicTKCN.Add("KLL", "");
                                    }
                                }
                                else
                                {
                                    dicTKCN.Add("LL", "");
                                    dicTKCN.Add("KLL", "");
                                }
                            }
                            else
                            {
                                averageMarkRetest = pr_Retest.AverageMark.HasValue ? ReportUtils.ConvertMarkForV_SGTGD(pr_Retest.AverageMark.Value, isSoHCM) : string.Empty;

                                if (pr_Retest.StudyingJudgementID == SystemParamsInFile.STUDYING_JUDGEMENT_UPCLASS || pr_Retest.StudyingJudgementID == SystemParamsInFile.STUDYING_JUDGEMENT_GRADUATION)
                                {
                                    if (pr_Retest.EducationLevelID == 9)
                                    {
                                        dicTKCN.Add("LL", "Đủ ĐK xét duyệt TN");
                                    }
                                    else
                                    {
                                        dicTKCN.Add("LL", pr_Retest.StudyingJudgementResolution);
                                    }
                                    countLL++;
                                    countLLSTL++;
                                }
                                else if (pr_Retest.StudyingJudgementID == SystemParamsInFile.STUDYING_JUDGEMENT_STAYCLASS)
                                {
                                    if (pr_Retest.EducationLevelID == 9)
                                    {
                                        dicTKCN.Add("KLL", "");
                                    }
                                    else
                                    {
                                        dicTKCN.Add("KLL", pr_Retest.StudyingJudgementResolution);
                                    }
                                    countOLL++;
                                }
                            }
                            if (showRetest)
                            {
                                if (averageMarkRetest != null && averageMarkRetest.Trim() != "")
                                {
                                    dicTKCN["TBCM"] = averageMarkRetest;
                                }
                                else
                                {
                                    dicTKCN["TBCM"] = averageMark;
                                }
                            }
                            else
                            {
                                dicTKCN["TBCM"] = averageMark;
                            }
                        }
                        else
                        {
                            dicTKCN.Add("TBCM", "");

                            dicTKCN.Add("HL", "");

                            dicTKCN.Add("HK", "");
                        }

                        if (pe != null)
                        {
                            string honourAchivement = pe.HonourAchivementTypeResolution != null ? UtilsBusiness.ConvertCapacity(pe.HonourAchivementTypeResolution) : string.Empty;
                            dicTKCN["DH"] = honourAchivement;
                        }
                        else
                        {
                            dicTKCN.Add("DH", "");
                        }
                        //Tổng số ngày nghỉ học
                        int countPupilAbsence = iqPA.Where(o => o.PupilID == pupilID).Count();
                        dicTKCN["TS"] = countPupilAbsence;
                        //Học lực, hạnh kiểm sau thi lại 

                        string capacityAfterRetest = "";
                        string conductAfterRetest = "";
                        if (pr != null && pr.StudyingJudgementID == SystemParamsInFile.STUDYING_JUDGEMENT_RETEST)
                        {
                            if (pr_Retest != null)
                            {
                                capacityAfterRetest = UtilsBusiness.ConvertCapacity(pr_Retest.CapacityLevel);
                                if (pr_Retest.EducationLevelID == 9)
                                {
                                    capacityAfterRetest = "";
                                }

                            }

                            dicTKCN["HLSTL"] = capacityAfterRetest;
                        }
                        else
                        {
                            dicTKCN["HLSTL"] = "";
                        }
                        if (pr != null && pr.StudyingJudgementID == SystemParamsInFile.STUDYING_JUDGEMENT_BACKTRAINING)
                        {
                            if (pr_Retest != null)
                            {
                                conductAfterRetest = UtilsBusiness.ConvertCapacity(pr_Retest.ConductLevelName);
                                if (pr_Retest.EducationLevelID == 9)
                                {
                                    conductAfterRetest = "";
                                }
                            }

                            dicTKCN["HKSTL"] = conductAfterRetest;
                        }
                        else
                        {
                            dicTKCN["HKSTL"] = "";
                        }


                    }
                    insideTKCN.Add(dicTKCN);
                }
                //Cac thong tin tong so hoc sinh, len lop,  o lai lop, duoc len lop sau thi lai
                string strTSHS = "Tổng số học sinh: ...............................................";
                if (countStudying > 0)
                {
                    strTSHS = "Tổng số học sinh: " + countStudying.ToString();
                }
                sheetTongKetCaNam.SetCellValue("AG7", strTSHS);
                string strLenLop = "Được lên lớp: ..............................................";
                if (countLL > 0)
                {
                    strLenLop = "Được lên lớp: " + countLL.ToString();
                }
                string strOLaiLop = "Ở lại lớp: ...............................................";
                if (countOLL > 0)
                {
                    strOLaiLop = "Ở lại lớp: " + countOLL.ToString();
                }
                string strLenLopSTL = "Được lên lớp sau kiểm tra lại các môn học hoặc rèn luyện trong hè: .............................................................................................................................................................";
                if (countLLSTL > 0)
                {
                    strLenLopSTL = "Được lên lớp sau kiểm tra lại các môn học hoặc rèn luyện trong hè: " + countLLSTL.ToString();
                }

                sheetTongKetCaNam.SetCellValue("AG9", strLenLop);
                sheetTongKetCaNam.SetCellValue("AG11", strOLaiLop);
                sheetTongKetCaNam.SetCellValue("AG14", strLenLopSTL);
                resultTKCN.Add("Rows", insideTKCN);
                sheetTongKetCaNam.FillVariableValue(resultTKCN);
                sheetTongKetCaNam.SetCellValue("AG26", HeadTeacherName);
            }
            #endregion

            #region Sheet giao vien bm
            FillSheetTeachingAssignment(sheetGiaovienBM, entity, lstTeachingBo);

            #endregion
            //Xóa các sheet thừa
            IVTWorksheet sheetBiaTem = oBook.GetSheet(1);
            IVTWorksheet sheetDiemDanhTemp = oBook.GetSheet(5);
            IVTWorksheet sheetBiaGhiDiemHKITemp = oBook.GetSheet(7);
            sheetBiaTem.Delete();
            sheetDiemDanhTemp.Delete();
            sheetSYLLTemp.Delete();
            sheetDiemHKITemp.Delete();
            sheetDiemHKI_2Temp.Delete();
            sheetDiem_HKI_MTCTemp.Delete();
            sheetDiemTKHKITemp.Delete();
            sheetDiemHK2_2Temp.Delete();
            sheetDiemHK2Temp.Delete();
            sheetDiemHK2mtcTemp.Delete();
            sheetDiemTKHK2Temp.Delete();
            sheetTongKetCaNamTemp.Delete();
            sheetBiaGhiDiemHK2Temp.Delete();
            sheetBiaTKCaNamTemp.Delete();
            sheetBiaGhiDiemHKITemp.Delete();
            if (entity.Semester == SystemParamsInFile.SEMESTER_OF_YEAR_FIRST)
            {
                sheetDiemHK2.Delete();
                sheetDiemHK2_2.Delete();
                sheetDiemHK2mtc.Delete();
                sheetDiemTKHK2.Delete();
                sheetBiaGhiDiemHK2.Delete();
                sheetBiaTKCaNam.Delete();
                sheetTongKetCaNam.Delete();
                sheetNhanXetHT_HK2.Delete();
            }
            if (entity.Semester == SystemParamsInFile.SEMESTER_OF_YEAR_SECOND)
            {
                sheetDiemHK1.Delete();
                sheetDiemHK1_2.Delete();
                sheetDiemHK1mtc.Delete();
                sheetDiemTKHK1.Delete();
                sheetBiaGhiDiemHK1.Delete();
                sheetBiaTKCaNam.Delete();
                sheetTongKetCaNam.Delete();
                sheetNhanXetHT_HK1.Delete();
            }
            if (countSubjectRetest <= 3)
            {
                sheetDiemThiLai.Delete();
            }
            return oBook.ToStream();
        }


        public string SetDisplaySubjectNX_THCS(string displayName)
        {
            string output = "";
            switch (displayName.ToUpper())
            {
                case "TOÁN":
                    output = "C4";
                    break;
                case "VẬT LÍ":
                    output = "D4";
                    break;
                case "HÓA HỌC":
                    output = "E4";
                    break;
                case "SINH HỌC":
                    output = "F4";
                    break;
                case "NGỮ VĂN":
                    output = "G4";
                    break;
                case "LỊCH SỬ":
                    output = "H4";
                    break;
                case "ĐỊA LÍ":
                    output = "I4";
                    break;
                case "GDCD":
                    output = "K4";
                    break;
                case "THỂ DỤC":
                    output = "M4";
                    break;
                case "ÂM NHẠC":
                    output = "N4";
                    break;
                case "MỸ THUẬT":
                    output = "O4";
                    break;
                default:
                    break;
            }
            return output;
        }

        public string SetDisplaySubjectNX12_THCS(string displayName)
        {
            string output = "";
            switch (displayName.ToUpper())
            {
                case "TOÁN":
                    output = "C3";
                    break;
                case "VẬT LÍ":
                    output = "D3";
                    break;
                case "HÓA HỌC":
                    output = "E3";
                    break;
                case "SINH HỌC":
                    output = "F3";
                    break;
                case "NGỮ VĂN":
                    output = "G3";
                    break;
                case "LỊCH SỬ":
                    output = "H3";
                    break;
                case "ĐỊA LÍ":
                    output = "I3";
                    break;
                case "GDCD":
                    output = "K3";
                    break;
                case "THỂ DỤC":
                    output = "M3";
                    break;
                case "ÂM NHẠC":
                    output = "N3";
                    break;
                case "MỸ THUẬT":
                    output = "O3";
                    break;
                default:
                    break;
            }

            return output;
        }


    }
}
