/**
 * The Viettel License
 *
 * Copyright 2012 Viettel Telecom. All rights reserved.
 * VIETTEL PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */

/** 
* @author  
* @version $Revision: $
*/

using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Linq.Expressions;
using System.Web;
using log4net;

using SMAS.DAL.Repository;
using SMAS.DAL.IRepository;
using SMAS.Business.IBusiness;
using SMAS.Models.Models;
using SMAS.Business.Common;

namespace SMAS.Business.Business
{ 
    public partial class ApprenticeshipSubjectBusiness
    {
        

        #region Search

        /// <summary>
        /// Tìm kiếm danh sách môn học nghề
        /// </summary>
        /// <param name="dic">Đối tượng dic</param>
        /// <returns>
        /// Danh sách môn học nghề
        /// </returns>
        /// <author>hath</author>
        /// <date> 9/7/2012</date>
        public IQueryable<ApprenticeshipSubject> Search(IDictionary<string, object> dic)
        {
            string SubjectName = Utils.GetString(dic, "SubjectName");
            int GroupID = Utils.GetShort(dic, "GroupID");
            int? AppledLevel = Utils.GetInt(dic, "AppliedLevel");
            bool? IsActive = Utils.GetIsActive(dic, "IsActive");
            IQueryable<ApprenticeshipSubject> lsSubject = ApprenticeshipSubjectRepository.All;

            if (IsActive.HasValue)
                lsSubject = lsSubject.Where(app => app.SubjectCat.IsActive == true);
            if (SubjectName!="")
                lsSubject = lsSubject.Where(app => app.SubjectName.Contains(SubjectName));
            if (AppledLevel.HasValue)
                lsSubject = lsSubject.Where(app => app.SubjectCat.AppliedLevel == AppledLevel);
            //if (GroupID!=0)
            //    lsSubject = lsSubject.Where(app => app.GroupID == GroupID);
            


            return lsSubject;
        }
        #endregion

        #region Insert
        /// <summary>
        /// Thêm môn học nghề
        /// </summary>
        /// <param name="apprenticeshipSubject">Đối tượng ApprenticeshipSubject</param>
        /// <returns>
        /// Đối tượng ApprenticeshipSubject
        /// </returns>
        /// <author>hath</author>
        /// <date> 9/7/2012</date>
        public override ApprenticeshipSubject Insert(ApprenticeshipSubject apprenticeshipSubject)
        {
            //Tên môn học nghề không được để trống - Kiểm tra SubjectName
            Utils.ValidateRequire(apprenticeshipSubject.SubjectName, "ApprenticeshipSubject_Label_SubjectName");
            //Độ dài trường Tên môn học nghề không được vượt quá 200 - Kiểm tra SubjectName
            Utils.ValidateMaxLength(apprenticeshipSubject.SubjectName,200, "ApprenticeshipSubject_Label_SubjectName");
            //Tên môn học nghề đã tồn tại - Kiểm tra SubjectName  với IsActive=TRUE
            //this.CheckDuplicate(apprenticeshipSubject.SubjectName, GlobalConstants.LIST_SCHEMA, "ApprenticeshipSubject", "SubjectName", true, apprenticeshipSubject.ApprenticeshipSubjectID, "ApprenticeshipSubject_SubjectName");
            {
                bool Exist = repository.ExistsRow(GlobalConstants.LIST_SCHEMA, "ApprenticeshipSubject",
                        new Dictionary<string, object>()
                {
                    {"SubjectName",apprenticeshipSubject.SubjectName}
                    ,{"SubjectID",apprenticeshipSubject.SubjectID}
                    ,{"IsActive",true}
                }, null);
                if (Exist)
                {
                    List<object> Params = new List<object>();
                    Params.Add("ApprenticeshipSubject_Label_SubjectName");
                    throw new BusinessException("Common_Validate_Duplicate", Params);
                }
            }

            //Không tồn tại Nhóm môn học nghề hoặc đã bị xóa - Kiểm tra GroupID - ApprenticeshipGroupID trong bảng ApprenticeshipGroup với IsActive =1
            //this.CheckAvailable(apprenticeshipSubject.GroupID, "ApprenticeshipSubject_Label_GroupID", true);
           return  base.Insert(apprenticeshipSubject);
        }
        #endregion

        #region Update
        /// <summary>
        /// Sửa môn học nghề
        /// </summary>
        /// <param name="apprenticeshipSubject">Đối tượng ApprenticeshipSubject</param>
        /// <returns>
        /// Đối tượng ApprenticeshipSubject
        /// </returns>
        /// <author>hath</author>
        /// <date> 9/7/2012</date>
        public override ApprenticeshipSubject Update(ApprenticeshipSubject apprenticeshipSubject)
        {
            var temp = this.Find(apprenticeshipSubject.ApprenticeshipSubjectID);
            if(temp==null)
            {
                    List<object> Params = new List<object>();
                    Params.Add("ApprenticeshipSubject_Label_ApprenticeshipSubjectID");
                    throw new BusinessException("Common_Validate_NotAvailable", Params);
            }
            //this.CheckAvailable(apprenticeshipSubject.ApprenticeshipSubjectID, "ApprenticeshipSubject_Label_ApprenticeshipSubjectID", true);
            //Tên môn học nghề không được để trống - Kiểm tra SubjectName
            Utils.ValidateRequire(apprenticeshipSubject.SubjectName, "ApprenticeshipSubject_Label_SubjectName");
            //Độ dài trường Tên môn học nghề không được vượt quá 200 - Kiểm tra SubjectName
            Utils.ValidateMaxLength(apprenticeshipSubject.SubjectName, 200, "ApprenticeshipSubject_Label_SubjectName");
            //Tên môn học nghề đã tồn tại - Kiểm tra SubjectName  với IsActive=TRUE
            //this.CheckDuplicate(apprenticeshipSubject.SubjectName, GlobalConstants.LIST_SCHEMA, "ApprenticeshipSubject", "SubjectName", true, apprenticeshipSubject.ApprenticeshipSubjectID, "ApprenticeshipSubject_SubjectName");

            {
                bool Exist = repository.ExistsRow(GlobalConstants.LIST_SCHEMA, "ApprenticeshipSubject",
                        new Dictionary<string, object>()
                {
                    {"SubjectName",apprenticeshipSubject.SubjectName}
                    ,{"SubjectID",apprenticeshipSubject.SubjectID}
                    ,{"IsActive",true}
                }, new Dictionary<string,object>()
                {
                {"ApprenticeshipSubjectID",apprenticeshipSubject.ApprenticeshipSubjectID}    
                });
                if (Exist)
                {
                    List<object> Params = new List<object>();
                    Params.Add("ApprenticeshipSubject_Label_SubjectName");
                    throw new BusinessException("Common_Validate_Duplicate", Params);
                }
            }
            //Không tồn tại Nhóm môn học nghề hoặc đã bị xóa - Kiểm tra GroupID - ApprenticeshipGroupID trong bảng ApprenticeshipGroup với IsActive =1
            //this.CheckAvailable(apprenticeshipSubject.GroupID, "ApprenticeshipSubject_Label_GroupID", true);
            //Không tồn tại môn học nghề hoặc đã bị xóa – kiểm tra ApprenticeshipSubjectID với IsActive = TRUE
            

            return base.Update(apprenticeshipSubject);
        }
        #endregion

        #region Delete
        /// <summary>
        /// Xóa môn học nghề
        /// </summary>
        /// <param name="ApprenticeshipSubjectID">Đối tượng ApprenticeshipSubjectID</param>
        /// <author>hath</author>
        /// <date> 9/7/2012</date>
        public void Delete(int ApprenticeshipSubjectID)
        {
            //Bạn chưa chọn Môn học nghề cần xóa hoặc Môn học nghề bạn chọn đã bị xóa khỏi hệ thống
            this.CheckAvailable(ApprenticeshipSubjectID, "ApprenticeshipSubject_ApprenticeshipSubjectID", true);
            base.Delete(ApprenticeshipSubjectID, true);

        }
        #endregion
    }
}
