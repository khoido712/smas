﻿/**
 * The Viettel License
 *
 * Copyright 2012 Viettel Telecom. All rights reserved.
 * VIETTEL PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */

/** 
* @author  
* @version $Revision: $
*/

using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Linq.Expressions;
using System.Web;
using log4net;
using SMAS.DAL.Repository;
using SMAS.DAL.IRepository;
using SMAS.Business.IBusiness;
using SMAS.Models.Models;
using SMAS.Business.Common;
using SMAS.Business.BusinessObject;
using System.Transactions;
using SMS_SCHOOL.Utility.BusinessObject;
using System.Globalization;
using System.Data.Objects;
using SMAS.Business.Common.Extension;
using SMAS.VTUtils.Log;
using SMAS.Business.Common.Util;
using System.Web.Configuration;
namespace SMAS.Business.Business
{
    public partial class SMSParentContractBusiness
    {
        #region constant/parameter
        /// <summary>
        /// them moi account
        /// </summary>
        private const string SMS_NOTICE_SPARENT_ACCOUNT_ADD = "SMAS: Quy Phu huynh da dang ky thanh cong DV So lien lac dien tu cua HS {0}. Phu huynh se nhan thong bao tu nha truong qua SMS hoac truy cap {1} bang SDT {2} va mat khau {3}. Chi tiet LH 18008000 nhanh 1 (mien phi)";
        private const string SMS_NOTICE_SPARENT_ACCOUNT_ADD_NEW = "SMAS: Quy Phu huynh da dang ky thanh cong DV So lien lac dien tu cua HS {0}. Phu huynh se nhan thong bao tu nha truong qua SMS hoac truy cap {1} bang SDT {2}. Chi tiet LH 18008000 nhanh 1 (mien phi)";
        private const int ACTION_TYPE_SEND_TO_PARENT_ADD = 1;

        /// <summary>
        /// xoa account
        /// </summary>
        private const string SMS_NOTICE_SPARENT_ACCOUNT_DELETE = "SMAS: Quy Phu huynh da huy thanh cong DV So lien lac dien tu cua HS {0}. De dang ky lai, vui long lien he nha truong, hoac goi 18008000 nhanh 1 (mien phi).";
        private const int ACTION_TYPE_SEND_TO_PARENT_DELETE = 2;
        /// <summary>
        /// chan cat
        /// </summary>
        private const string SMS_NOTICE_SPARENT_ACCOUNT_DISABLE = "SMAS: Quy Phu huynh da tam ngung thanh cong DV So lien lac dien tu cua HS {0}. De dang ky lai, vui long lien he nha truong, hoac goi 18008000 nhanh 1 (mien phi).";
        private const int ACTION_TYPE_SEND_TO_PARENT_DISABLE = 3;

        /// <summary>
        /// gia han
        /// </summary>
        private const string SMS_NOTICE_PASSWORD_RENEW = "SMAS: Quy Phu huynh da gia han thanh cong DV So lien lac dien tu cua HS {0}. Phu huynh se nhan thong bao tu nha truong qua SMS hoac truy cap {1} bang SDT {2}. Chi tiet LH 18008000 nhanh 1 (mien phi)";
        private const int ACTION_TYPE_SEND_TO_PARENT_RENEWABLE = 4;

        /// <summary>
        /// huy chan cat
        /// </summary>
        private const string SMS_NOTICE_SPARENT_ACCOUNT_ENABLED = "SMAS: Quy Phu huynh da khoi phuc thanh cong DV So lien lac dien tu cua HS {0}. Phu huynh se nhan thong bao tu nha truong qua SMS hoac truy cap {1} bang SDT {2}. Chi tiet LH 18008000 nhanh 1 (mien phi)";
        private const int ACTION_TYPE_SEND_TO_PARENT_ENABLED = 5;

        #endregion

        #region search
        /// <summary>
        /// Search smasparentContract
        /// </summary>
        /// <author date="2014/02/18">HaiVT</author>
        /// <param name="dic"></param>
        /// <returns></returns>
        public IQueryable<SMS_PARENT_CONTRACT> Search(Dictionary<string, object> dic)
        {
            int pupilID = Utils.GetInt(dic, "pupilID");
            int schoolID = Utils.GetInt(dic, "schoolID");
            string contractCode = Utils.GetString(dic, "contractCode");
            bool? isDelete = Utils.GetNullableBool("isDelete");
            List<int> pupilIDList = Utils.GetIntList(dic, "pupilIDList");
            string subscriber = Utils.GetString(dic, "subscriber");
            int year = Utils.GetInt(dic, "year");
            int subscriptionTime = Utils.GetInt(dic, "subsriptionTime");
            int RegistrationType = Utils.GetInt(dic, "registrationType");

            IQueryable<SMS_PARENT_CONTRACT> result = (from s in this.All
                                                      join pf in PupilProfileBusiness.All on s.PUPIL_ID equals pf.PupilProfileID
                                                      where pf.IsActive
                                                      select s);

            if (pupilID > 0)
            {
                result = result.Where(p => p.PUPIL_ID == pupilID);
            }

            if (schoolID > 0)
            {
                result = result.Where(p => p.SCHOOL_ID == schoolID && p.PARTITION_ID == schoolID % GlobalConstantsEdu.PARTITION_SMSPARENTCONTRACT_SCHOOLID);
            }

            if (!string.IsNullOrEmpty(contractCode))
            {
                result = result.Where(p => p.CONTRACT_CODE == contractCode);
            }

            if (isDelete.HasValue)
            {
                if (isDelete.Value)
                {
                    result = result.Where(p => p.IS_DELETED == true);
                }
                else
                {
                    result = result.Where(p => p.IS_DELETED == null || p.IS_DELETED == false);
                }
            }

            if (pupilIDList != null && pupilIDList.Count > 0)
            {
                result = result.Where(p => pupilIDList.Contains(p.PUPIL_ID));
            }

            if (!string.IsNullOrEmpty(subscriber))
            {
                result = from sc in result
                         join scd in SMSParentContractDetailBusiness.All on sc.SMS_PARENT_CONTRACT_ID equals scd.SMS_PARENT_CONTRACT_ID
                         where scd.SUBSCRIBER == subscriber
                         select sc;
            }

            if (year > 0)
            {
                result = from sc in result
                         join scd in SMSParentContractDetailBusiness.All on sc.SMS_PARENT_CONTRACT_ID equals scd.SMS_PARENT_CONTRACT_ID
                         where scd.YEAR_ID == year
                         select sc;
            }

            if (subscriptionTime > 0)
            {
                result = from sc in result
                         join scd in SMSParentContractDetailBusiness.All on sc.SMS_PARENT_CONTRACT_ID equals scd.SMS_PARENT_CONTRACT_ID
                         where scd.SUBSCRIPTION_TIME == subscriptionTime
                         select sc;
            }
            if (RegistrationType > 0)
            {
                result = result.Where(p => p.REGISTRATION_TYPE == RegistrationType);
            }

            return result;
        }
        #endregion

        #region Hợp đồng cho phép gửi tin
        /// <summary>
        /// Lay danh sach Hop dong gui tin
        /// </summary>
        /// <param name="dic"></param>
        /// <returns></returns>
        public IQueryable<SMSParentContractBO> getSMSParentContract(Dictionary<string, object> dic)
        {
            int semesterID = Utils.GetInt(dic, "SemesterID");
            int schoolID = dic.GetInt("SchoolID");
            int year = dic.GetInt("Year");
            List<int> pupilIDList = dic.GetIntList("lstPupilID");

            SMSParentSendDetailSearchBO objSMSSendDetail = new SMSParentSendDetailSearchBO
            {
                SchoolId = schoolID,
                //Semester = semesterID,
                Year = year
            };
            IQueryable<SMS_PARENT_SENT_DETAIL> qSMSParentSendDetail = SMSParentSentDetailBusiness.Search(objSMSSendDetail);

            IQueryable<SMSParentContractBO> iqContractBO = (from sc in SMSParentContractBusiness.All
                                                            join scd in SMSParentContractDetailBusiness.All on sc.SMS_PARENT_CONTRACT_ID equals scd.SMS_PARENT_CONTRACT_ID
                                                            join poc in PupilOfClassBusiness.All on sc.PUPIL_ID equals poc.PupilID
                                                            join pf in PupilProfileBusiness.All on sc.PUPIL_ID equals pf.PupilProfileID
                                                            join pdtl in ServicePackageDetailBusiness.All on scd.SERVICE_PACKAGE_ID equals pdtl.SERVICE_PACKAGE_ID
                                                            join spdd in ServicePackageDeclareBusiness.All on scd.SERVICE_PACKAGE_ID equals spdd.SERVICE_PACKAGE_ID
                                                            join sentDtl in qSMSParentSendDetail on scd.SMS_PARENT_CONTRACT_DETAIL_ID equals sentDtl.SMS_PARENT_CONTRACT_DETAIL_ID into gj
                                                            from x in gj.DefaultIfEmpty(null)
                                                            where sc.SCHOOL_ID == schoolID && sc.PARTITION_ID == schoolID % GlobalConstantsEdu.PARTITION_SMSPARENTCONTRACT_SCHOOLID
                                                            && (scd.SUBSCRIPTION_STATUS == GlobalConstantsEdu.STATUS_REGISTED || scd.SUBSCRIPTION_STATUS == GlobalConstantsEdu.STATUS_UNPAID) // Bổ sung gửi tin cho các thuê bao Nợ cước
                                                            && scd.IS_ACTIVE == true && sc.STATUS == GlobalConstantsEdu.SMS_PARENT_CONTRACT_STATUS_IS_ACCEPT
                                                            && (sc.IS_DELETED == null || sc.IS_DELETED == false)
                                                            &&
                                                            (
                                                             (semesterID == 1 && (scd.SUBSCRIPTION_TIME == GlobalConstantsEdu.CONTRACTDETAIL_SUBSCRIPTIONTIME_FIRST_SMESTER || scd.SUBSCRIPTION_TIME == GlobalConstantsEdu.CONTRACTDETAIL_SUBSCRIPTIONTIME_ALLYEAR))
                                                             ||
                                                             (semesterID == 2 && (scd.SUBSCRIPTION_TIME == GlobalConstantsEdu.CONTRACTDETAIL_SUBSCRIPTIONTIME_SECOND_SMESTER || scd.SUBSCRIPTION_TIME == GlobalConstantsEdu.CONTRACTDETAIL_SUBSCRIPTIONTIME_ALLYEAR))
                                                            )
                                                            && scd.YEAR_ID == year
                                                            && spdd.YEAR == year
                                                            && spdd.STATUS == true
                                                            && poc.Year == year
                                                            && poc.ClassID == sc.CLASS_ID
                                                            && pf.IsActive
                                                            select new
                                                            {
                                                                PupilFileID = sc.PUPIL_ID,
                                                                Subscriber = scd.SUBSCRIBER,
                                                                SMSParentContractDetailID = scd.SMS_PARENT_CONTRACT_DETAIL_ID,
                                                                ServicePackageID = scd.SERVICE_PACKAGE_ID,
                                                                SubscriptionTime = scd.SUBSCRIPTION_TIME,
                                                                TotalSent = ((x == null) ? 0 : x.SENT_TOTAL),
                                                                Year = scd.YEAR_ID,
                                                                IsLimit = spdd.IS_LIMIT,
                                                                MaxSMS = (pdtl.MAX_SMS.HasValue) ? scd.SUBSCRIPTION_TIME == 3 ? pdtl.MAX_SMS.Value * 2 : pdtl.MAX_SMS.Value : 0
                                                            })
                                                                          .GroupBy(x => new { x.PupilFileID, x.Subscriber, x.SMSParentContractDetailID, x.ServicePackageID, x.Year, x.SubscriptionTime, x.TotalSent, x.IsLimit })
                                                                            .Select(y => new SMSParentContractBO
                                                                            {
                                                                                PupilFileID = y.Key.PupilFileID,
                                                                                PhoneNumberRecever = y.Key.Subscriber,
                                                                                SMSParentContractDetailID = y.Key.SMSParentContractDetailID,
                                                                                ServicePackageID = y.Key.ServicePackageID,
                                                                                Year = y.Key.Year,
                                                                                SubscriptionTime = y.Key.SubscriptionTime,
                                                                                TotalSent = y.Key.TotalSent,
                                                                                IsLimitPackage = y.Key.IsLimit,
                                                                                TotalSMS = y.Sum(a => a.MaxSMS)
                                                                            });


            if (pupilIDList != null && pupilIDList.Count > 0)
            {
                iqContractBO = iqContractBO.Where(p => pupilIDList.Contains(p.PupilFileID));
            }
            List<SMSParentContractBO> lstContractBO = iqContractBO.ToList();

            //viethd31: Lay danh sach goi cuoc mua them
            var LstPupilContractInClassExtra = (from sc in SMSParentContractBusiness.All
                                                join scd in SMSParentContractDetailBusiness.All on sc.SMS_PARENT_CONTRACT_ID equals scd.SMS_PARENT_CONTRACT_ID
                                                join poc in PupilOfClassBusiness.All on sc.PUPIL_ID equals poc.PupilID
                                                join pf in PupilProfileBusiness.All on sc.PUPIL_ID equals pf.PupilProfileID
                                                join scde in SMSParentContractExtraBusiness.All on scd.SMS_PARENT_CONTRACT_DETAIL_ID equals scde.SMS_PARENT_CONTRACT_DETAIL_ID
                                                join pdtl in ServicePackageDetailBusiness.All on scde.SERVICE_PACKAGE_ID equals pdtl.SERVICE_PACKAGE_ID
                                                join spdd in ServicePackageDeclareBusiness.All on scd.SERVICE_PACKAGE_ID equals spdd.SERVICE_PACKAGE_ID

                                                where sc.SCHOOL_ID == schoolID && sc.PARTITION_ID == schoolID % GlobalConstantsEdu.PARTITION_SMSPARENTCONTRACT_SCHOOLID
                                                && (scd.SUBSCRIPTION_STATUS == GlobalConstantsEdu.STATUS_REGISTED || scd.SUBSCRIPTION_STATUS == GlobalConstantsEdu.STATUS_UNPAID) // Bổ sung gửi tin cho các thuê bao Nợ cước
                                                && scd.IS_ACTIVE == true && sc.STATUS == GlobalConstantsEdu.SMS_PARENT_CONTRACT_STATUS_IS_ACCEPT
                                                && (sc.IS_DELETED == null || sc.IS_DELETED == false)
                                                &&
                                                (
                                                    (semesterID == 1 && (scd.SUBSCRIPTION_TIME == GlobalConstantsEdu.CONTRACTDETAIL_SUBSCRIPTIONTIME_FIRST_SMESTER || scd.SUBSCRIPTION_TIME == GlobalConstantsEdu.CONTRACTDETAIL_SUBSCRIPTIONTIME_ALLYEAR))
                                                    ||
                                                    (semesterID == 2 && (scd.SUBSCRIPTION_TIME == GlobalConstantsEdu.CONTRACTDETAIL_SUBSCRIPTIONTIME_SECOND_SMESTER || scd.SUBSCRIPTION_TIME == GlobalConstantsEdu.CONTRACTDETAIL_SUBSCRIPTIONTIME_ALLYEAR))
                                                )
                                                && scd.YEAR_ID == year
                                                && spdd.YEAR == year
                                                && spdd.STATUS == true
                                                && poc.Year == year
                                                && poc.ClassID == sc.CLASS_ID
                                                && pf.IsActive
                                                select new
                                                {
                                                    PupilFileID = sc.PUPIL_ID,
                                                    SMSParentContactDetailID = scd.SMS_PARENT_CONTRACT_DETAIL_ID,
                                                    MaxSMS = (pdtl.MAX_SMS.HasValue) ? scd.SUBSCRIPTION_TIME == 3 ? pdtl.MAX_SMS.Value * 2 : pdtl.MAX_SMS.Value : 0,
                                                    ServicePackageID = pdtl.SERVICE_PACKAGE_ID,
                                                })
                                                .GroupBy(x => new { x.PupilFileID, x.SMSParentContactDetailID, x.ServicePackageID })
                                                .Select(y => new PupilContractBO
                                                {
                                                    PupilID = y.Key.PupilFileID,
                                                    SMSParentContactDetailID = y.Key.SMSParentContactDetailID,
                                                    ServicePackageID = y.Key.ServicePackageID,
                                                    TotalSMS = y.Sum(a => a.MaxSMS),
                                                }).ToList();

            //viethd31: Cong phan tin mua them vao danh sach
            for (int i = 0; i < lstContractBO.Count; i++)
            {
                SMSParentContractBO pp = lstContractBO[i];
                List<PupilContractBO> lstExtra = LstPupilContractInClassExtra.Where(o => o.PupilID == pp.PupilFileID && o.SMSParentContactDetailID == pp.SMSParentContractDetailID).ToList();
                pp.TotalSMS = pp.TotalSMS + lstExtra.Sum(o => o.TotalSMS);
            }

            return lstContractBO.AsQueryable<SMSParentContractBO>();
        }

        /// <summary>
        /// lay danh sach hop dong toan truong/khoi
        /// </summary>
        /// <param name="schoolID"></param>
        /// <param name="year"></param>
        /// <param name="semester"></param>
        /// <returns></returns>
        public List<ContractSimplifyBO> getSMSParentContract(int schoolID, int year, int semester)
        {
            return (from sc in SMSParentContractBusiness.All
                    join scd in SMSParentContractDetailBusiness.All on sc.SMS_PARENT_CONTRACT_ID equals scd.SMS_PARENT_CONTRACT_ID
                    join poc in PupilOfClassBusiness.All on sc.PUPIL_ID equals poc.PupilID
                    join pf in PupilProfileBusiness.All on sc.PUPIL_ID equals pf.PupilProfileID
                    where sc.SCHOOL_ID == schoolID && sc.PARTITION_ID == schoolID % GlobalConstantsEdu.PARTITION_SMSPARENTCONTRACT_SCHOOLID
                    && (scd.SUBSCRIPTION_STATUS == GlobalConstantsEdu.STATUS_REGISTED || scd.SUBSCRIPTION_STATUS == GlobalConstantsEdu.STATUS_UNPAID) // Bổ sung gửi tin cho các thuê bao Nợ cước
                    && scd.IS_ACTIVE == true && sc.STATUS == GlobalConstantsEdu.SMS_PARENT_CONTRACT_STATUS_IS_ACCEPT
                    && (sc.IS_DELETED == null || sc.IS_DELETED == false)
                    && scd.YEAR_ID == year
                    && poc.Year == year
                    && poc.ClassID == sc.CLASS_ID
                    && pf.IsActive
                    &&
                    (
                        (semester == GlobalConstantsEdu.CONTRACTDETAIL_SUBSCRIPTIONTIME_FIRST_SMESTER && (scd.SUBSCRIPTION_TIME == GlobalConstantsEdu.CONTRACTDETAIL_SUBSCRIPTIONTIME_FIRST_SMESTER || scd.SUBSCRIPTION_TIME == GlobalConstantsEdu.CONTRACTDETAIL_SUBSCRIPTIONTIME_ALLYEAR))
                        ||
                        (semester == GlobalConstantsEdu.CONTRACTDETAIL_SUBSCRIPTIONTIME_SECOND_SMESTER && (scd.SUBSCRIPTION_TIME == GlobalConstantsEdu.CONTRACTDETAIL_SUBSCRIPTIONTIME_SECOND_SMESTER || scd.SUBSCRIPTION_TIME == GlobalConstantsEdu.CONTRACTDETAIL_SUBSCRIPTIONTIME_ALLYEAR))
                    )
                    select new ContractSimplifyBO
                    {
                        PupilID = sc.PUPIL_ID,
                        Subscriber = scd.SUBSCRIBER
                    }).ToList();
        }

        /// <summary>
        /// lay danh sach hoc sinh theo lop phuc vu gui tin PHHS
        /// </summary>
        /// <author date="27/04/2016">Anhvd9</author>
        /// <param name="schoolID"></param>
        /// <param name="year"></param>
        /// <param name="semester"></param>
        /// <param name="lstPupil"></param>
        /// <returns></returns>
        public List<PupilProfileBO> getSMSParentContract(int schoolID, int year, int semester, List<PupilProfileBO> lstPupil)
        {
            SMSParentSendDetailSearchBO objSMSSendDetail = new SMSParentSendDetailSearchBO
            {
                SchoolId = schoolID,
                Year = year
            };
            IQueryable<SMS_PARENT_SENT_DETAIL> qSMSParentSendDetail = SMSParentSentDetailBusiness.Search(objSMSSendDetail);


            List<PupilProfileBO> lstResult = lstPupil.Join((from sc in SMSParentContractBusiness.All
                                                            join scd in SMSParentContractDetailBusiness.All on sc.SMS_PARENT_CONTRACT_ID equals scd.SMS_PARENT_CONTRACT_ID
                                                            join poc in PupilOfClassBusiness.All on sc.PUPIL_ID equals poc.PupilID
                                                            join pf in PupilProfileBusiness.All on sc.PUPIL_ID equals pf.PupilProfileID
                                                            join pdtl in ServicePackageDetailBusiness.All on scd.SERVICE_PACKAGE_ID equals pdtl.SERVICE_PACKAGE_ID
                                                            join spdd in ServicePackageDeclareBusiness.All on scd.SERVICE_PACKAGE_ID equals spdd.SERVICE_PACKAGE_ID
                                                            join sentDtl in qSMSParentSendDetail on scd.SMS_PARENT_CONTRACT_DETAIL_ID equals sentDtl.SMS_PARENT_CONTRACT_DETAIL_ID into gj
                                                            from x in gj.DefaultIfEmpty(null)
                                                            where sc.SCHOOL_ID == schoolID && sc.PARTITION_ID == schoolID % GlobalConstantsEdu.PARTITION_SMSPARENTCONTRACT_SCHOOLID
                                                            && (scd.SUBSCRIPTION_STATUS == GlobalConstantsEdu.STATUS_REGISTED || scd.SUBSCRIPTION_STATUS == GlobalConstantsEdu.STATUS_UNPAID) // Bổ sung gửi tin cho các thuê bao Nợ cước
                                                            && scd.IS_ACTIVE == true && sc.STATUS == GlobalConstantsEdu.SMS_PARENT_CONTRACT_STATUS_IS_ACCEPT
                                                            && (sc.IS_DELETED == null || sc.IS_DELETED == false)
                                                            &&
                                                            (
                                                                (semester == 1 && (scd.SUBSCRIPTION_TIME == GlobalConstantsEdu.CONTRACTDETAIL_SUBSCRIPTIONTIME_FIRST_SMESTER || scd.SUBSCRIPTION_TIME == GlobalConstantsEdu.CONTRACTDETAIL_SUBSCRIPTIONTIME_ALLYEAR))
                                                                ||
                                                                (semester == 2 && (scd.SUBSCRIPTION_TIME == GlobalConstantsEdu.CONTRACTDETAIL_SUBSCRIPTIONTIME_SECOND_SMESTER || scd.SUBSCRIPTION_TIME == GlobalConstantsEdu.CONTRACTDETAIL_SUBSCRIPTIONTIME_ALLYEAR))
                                                            )
                                                            && scd.YEAR_ID == year
                                                            && spdd.YEAR == year
                                                            && spdd.STATUS == true
                                                            && poc.Year == year
                                                            && poc.ClassID == sc.CLASS_ID
                                                            && pf.IsActive
                                                            select new
                                                            {
                                                                PupilFileID = sc.PUPIL_ID,
                                                                Subscriber = scd.SUBSCRIBER,
                                                                TotalSent = ((x == null) ? 0 : x.SENT_TOTAL),
                                                                IsLimit = spdd.IS_LIMIT,
                                                                MaxSMS = (pdtl.MAX_SMS.HasValue) ? scd.SUBSCRIPTION_TIME == 3 ? pdtl.MAX_SMS.Value * 2 : pdtl.MAX_SMS.Value : 0,
                                                            }).ToList()
                        .GroupBy(x => new { x.PupilFileID })
                        .Select
                        (y => new PupilProfileBO
                        {
                            PupilProfileID = y.Key.PupilFileID,
                            NumOfSMSSubscriber = y.Count(),
                            TotalSend = y.Sum(a => a.TotalSent),
                            MaxSMS = y.Sum(a => a.MaxSMS),
                            RemainSMS = string.Format("{0}/{1}", y.Sum(a => a.TotalSent), y.Sum(a => a.MaxSMS)),
                            ExpireTrial = y.Count(o => o.IsLimit == true && y.Sum(a => a.MaxSMS) <= y.Sum(a => a.TotalSent)) > 0 && y.Count(o => o.IsLimit != true) == 0,
                            PhoneReceiver = y.Select(o => o.Subscriber).ToList()
                        }), x => x.PupilProfileID, y => y.PupilProfileID, (x, y) => new PupilProfileBO
                        {
                            PupilProfileID = x.PupilProfileID,
                            PupilCode = x.PupilCode,
                            CurrentClassID = x.CurrentClassID,
                            CurrentClassName = x.CurrentClassName,
                            EducationLevelID = x.EducationLevelID,
                            ClassName = x.ClassName,
                            FullName = x.FullName,
                            NumOfSMSSubscriber = y.NumOfSMSSubscriber,
                            RemainSMS = y.RemainSMS,
                            MaxSMS = y.MaxSMS,
                            TotalSend = y.TotalSend,
                            ExpireTrial = y.ExpireTrial,
                            PhoneReceiver = y.PhoneReceiver
                        }).ToList();

            List<int> lstPupilID = lstPupil.Select(o => o.PupilProfileID).ToList();
            //viethd31: Lay danh sach goi cuoc mua them
            var LstPupilContractInClassExtra = (from sc in SMSParentContractBusiness.All
                                                join scd in SMSParentContractDetailBusiness.All on sc.SMS_PARENT_CONTRACT_ID equals scd.SMS_PARENT_CONTRACT_ID
                                                join poc in PupilOfClassBusiness.All on sc.PUPIL_ID equals poc.PupilID
                                                join pf in PupilProfileBusiness.All on sc.PUPIL_ID equals pf.PupilProfileID
                                                join scde in SMSParentContractExtraBusiness.All on scd.SMS_PARENT_CONTRACT_DETAIL_ID equals scde.SMS_PARENT_CONTRACT_DETAIL_ID
                                                join pdtl in ServicePackageDetailBusiness.All on scde.SERVICE_PACKAGE_ID equals pdtl.SERVICE_PACKAGE_ID
                                                join spdd in ServicePackageDeclareBusiness.All on scd.SERVICE_PACKAGE_ID equals spdd.SERVICE_PACKAGE_ID

                                                where sc.SCHOOL_ID == schoolID && sc.PARTITION_ID == schoolID % GlobalConstantsEdu.PARTITION_SMSPARENTCONTRACT_SCHOOLID
                                                && (scd.SUBSCRIPTION_STATUS == GlobalConstantsEdu.STATUS_REGISTED || scd.SUBSCRIPTION_TIME == GlobalConstantsEdu.STATUS_UNPAID) // Bổ sung gửi tin cho các thuê bao Nợ cước
                                                && scd.IS_ACTIVE == true && sc.STATUS == GlobalConstantsEdu.SMS_PARENT_CONTRACT_STATUS_IS_ACCEPT
                                                && (sc.IS_DELETED == null || sc.IS_DELETED == false)
                                                &&
                                                (
                                                    (semester == 1 && (scd.SUBSCRIPTION_TIME == GlobalConstantsEdu.CONTRACTDETAIL_SUBSCRIPTIONTIME_FIRST_SMESTER || scd.SUBSCRIPTION_TIME == GlobalConstantsEdu.CONTRACTDETAIL_SUBSCRIPTIONTIME_ALLYEAR))
                                                    ||
                                                    (semester == 2 && (scd.SUBSCRIPTION_TIME == GlobalConstantsEdu.CONTRACTDETAIL_SUBSCRIPTIONTIME_SECOND_SMESTER || scd.SUBSCRIPTION_TIME == GlobalConstantsEdu.CONTRACTDETAIL_SUBSCRIPTIONTIME_ALLYEAR))
                                                )
                                                && scd.YEAR_ID == year
                                                && spdd.YEAR == year
                                                && spdd.STATUS == true
                                                && poc.Year == year
                                                && poc.ClassID == sc.CLASS_ID
                                                && pf.IsActive
                                                select new
                                                {
                                                    PupilFileID = sc.PUPIL_ID,
                                                    SMSParentContactDetailID = scd.SMS_PARENT_CONTRACT_DETAIL_ID,
                                                    MaxSMS = (pdtl.MAX_SMS.HasValue) ? scd.SUBSCRIPTION_TIME == 3 ? pdtl.MAX_SMS.Value * 2 : pdtl.MAX_SMS.Value : 0,
                                                    ServicePackageID = pdtl.SERVICE_PACKAGE_ID,
                                                })
                                                .GroupBy(x => new { x.PupilFileID, x.SMSParentContactDetailID, x.ServicePackageID })
                                                .Select(y => new PupilContractBO
                                                {
                                                    PupilID = y.Key.PupilFileID,
                                                    SMSParentContactDetailID = y.Key.SMSParentContactDetailID,
                                                    ServicePackageID = y.Key.ServicePackageID,
                                                    TotalSMS = y.Sum(a => a.MaxSMS),
                                                }).ToList();

            //viethd31: Cong phan tin mua them vao danh sach
            for (int i = 0; i < lstResult.Count; i++)
            {
                PupilProfileBO pp = lstResult[i];
                List<PupilContractBO> lstExtra = LstPupilContractInClassExtra.Where(o => o.PupilID == pp.PupilProfileID).ToList();
                pp.MaxSMS = pp.MaxSMS + lstExtra.Sum(o => o.TotalSMS);
                pp.RemainSMS = string.Format("{0}/{1}", pp.TotalSend, pp.MaxSMS);
            }

            return lstResult;
        }
        #endregion

        #region Đếm số lượng Hợp đồng
        /// <summary>
        /// Đếm số lượng học sinh cần gửi tin nhắn đăng ký SLLĐT Online
        /// </summary>
        /// <author></author>
        /// <param name="dic"></param>
        /// <returns></returns>
        public int countNumberPeopleRegister(Dictionary<string, object> dic, List<PupilProfileBO> lstPupil)
        {
            List<SMSParentContractBO> smsParentContractBOIQuery = getSMSParentContractRegister(dic).Distinct().ToList();
            lstPupil = lstPupil.Where(o => (o.FatherMobile != null || o.MotherMobile != null || o.SponsorMobile != null)).ToList();
            int schoolID = dic.GetInt("SchoolID");
            int year = dic.GetInt("Year");
            int academicYearID = dic.GetInt("AcademicYearID");
            int TypeID = 0;
            DateTime now = DateTime.Now;
            AcademicYear aca = AcademicYearBusiness.Find(academicYearID);
            DateTime dateStartTypeHKI = new DateTime(aca.Year, 8, 1);
            DateTime dateEndTypeHKI = new DateTime(aca.Year + 1, 1, 31);
            DateTime dateStartTypeHKII = new DateTime(aca.Year + 1, 2, 1);
            DateTime dateEndTypeHKII = new DateTime(aca.Year + 1, 7, 31);

            if (DateTime.Compare(now, dateStartTypeHKII) >= 0 && DateTime.Compare(now, dateEndTypeHKII) <= 0)
            {
                TypeID = SMSTypeBusiness.All.Where(o => o.TYPE_CODE.Trim().Equals("DKSMSHKII") && o.TYPE_ID == 2).FirstOrDefault().TYPE_ID;
            }
            else
            {
                TypeID = SMSTypeBusiness.All.Where(o => o.TYPE_CODE.Trim().Equals("DKSMSHKI") && o.TYPE_ID == 2).FirstOrDefault().TYPE_ID;
            }
            List<SMS_HISTORY> lstSMSHistory = SMSHistoryBusiness.All.Where(o => o.SCHOOL_ID == schoolID && o.YEAR == year && o.SMS_TYPE_ID == TypeID && o.STATUS == true).ToList();
            List<PupilProfileBO> lstPupilBO = new List<PupilProfileBO>();
            List<int> lstPP = new List<int>();
            PupilProfileBO pp = new PupilProfileBO();
            PupilProfileBO ppBO = new PupilProfileBO();
            for (int i = 0; i < lstPupil.Count(); i++)
            {
                ppBO = lstPupil[i];
                if (ppBO.FatherMobile != null && ppBO.FatherMobile != "" && ppBO.FatherMobile.CheckMobileNumber())
                {
                    pp = new PupilProfileBO();
                    pp.PupilProfileID = ppBO.PupilProfileID;
                    pp.Mobile = ppBO.FatherMobile;
                    lstPupilBO.Add(pp);
                }
                if (ppBO.MotherMobile != null && ppBO.MotherMobile != "" && ppBO.MotherMobile.CheckMobileNumber())
                {
                    pp = new PupilProfileBO();
                    pp.PupilProfileID = ppBO.PupilProfileID;
                    pp.Mobile = ppBO.MotherMobile;
                    lstPupilBO.Add(pp);
                }
                if (ppBO.SponsorMobile != null && ppBO.SponsorMobile != "" && ppBO.SponsorMobile.CheckMobileNumber())
                {
                    pp = new PupilProfileBO();
                    pp.PupilProfileID = ppBO.PupilProfileID;
                    pp.Mobile = ppBO.SponsorMobile;
                    lstPupilBO.Add(pp);
                }
            }
            int countSMSParent = 0;
            lstPP = lstPupilBO.Where(o => !smsParentContractBOIQuery.Any(u => u.PupilFileID == o.PupilProfileID && u.PhoneNumberRecever.SubstringPhone().Equals(o.Mobile.SubstringPhone())) && !lstSMSHistory.Any(u => u.RECEIVER_ID == o.PupilProfileID && u.MOBILE.SubstringPhone().Equals(o.Mobile.SubstringPhone()))).Select(p => p.PupilProfileID).Distinct().ToList();
            countSMSParent = lstPP.Count();
            return countSMSParent;
        }
        #endregion

        #region Ham lay so hoc sinh duoc dang ky goi SMS_FREE
        public List<SMS_REGISTER_FREE> GetListRegisterSMSFree(Dictionary<string, object> dic)
        {
            int SchoolID = dic.GetInt("SchoolID");
            int YearID = dic.GetInt("YearID");//truyen academicYearID cho nay
            int StatusRegisterID = dic.GetInt("StatusRegisterID");
            int partitionID = SchoolID % 100;
            List<int> lstPupilID = dic.GetIntList("lstPupilID");
            List<SMS_REGISTER_FREE> lstRegisterSMSFree = new List<SMS_REGISTER_FREE>();
            lstRegisterSMSFree = (from r in RegisterSMSFreeBusiness.All
                                  where r.SCHOOL_ID == SchoolID
                                  && r.ACADEMIC_YEAR_ID == YearID
                                  && r.STATUS_REGISTER == StatusRegisterID
                                  && lstPupilID.Contains(r.PUPIL_ID)
                                  && r.PARTITION_ID == partitionID
                                  select r).ToList();
            return lstRegisterSMSFree;
        }
        #endregion

        #region Thông báo Đăng ký SLLĐT Online từ trường
        /// <summary>
        /// Lấy danh sách các Hợp đồng đăng ký SLLĐT Online
        /// </summary>
        /// <param name="dic"></param>
        /// <returns></returns>
        private IQueryable<SMSParentContractBO> getSMSParentContractRegister(Dictionary<string, object> dic)
        {
            int semesterID = dic.GetInt("SemesterID");//truyen them semester cho nay
            int? schoolID = dic.GetInt("SchoolID");
            int? year = dic.GetInt("Year");
            int subscriptionStatus = dic.GetInt("SubscriptionStatus");
            int? status = dic.GetNullableInt("Status");
            List<int> pupilIDList = dic.GetIntList("lstPupilID");
            Dictionary<string, object> SearchInfo = new Dictionary<string, object>();
            SearchInfo["strServiceCode"] = "SMS_DT";
            SMS_SERVICE_PACKAGE spack = ServicePackageBusiness.Search(SearchInfo).FirstOrDefault();
            int ServicePackageID = 0;
            if (spack != null)
            {
                ServicePackageID = spack.SERVICE_PACKAGE_ID;
            }

            IQueryable<SMSParentContractBO> smsParentContractBOIQuery = (from sc in SMSParentContractBusiness.All
                                                                         join scd in SMSParentContractDetailBusiness.All on sc.SMS_PARENT_CONTRACT_ID equals scd.SMS_PARENT_CONTRACT_ID
                                                                         join poc in PupilOfClassBusiness.All on sc.PUPIL_ID equals poc.PupilID
                                                                         join pf in PupilProfileBusiness.All on sc.PUPIL_ID equals pf.PupilProfileID
                                                                         join pdtl in ServicePackageBusiness.All on scd.SERVICE_PACKAGE_ID equals pdtl.SERVICE_PACKAGE_ID
                                                                         where sc.SCHOOL_ID == schoolID && sc.PARTITION_ID == schoolID % GlobalConstantsEdu.PARTITION_SMSPARENTCONTRACT_SCHOOLID
                                                                         && (scd.SUBSCRIPTION_STATUS == GlobalConstantsEdu.STATUS_REGISTED || scd.SUBSCRIPTION_STATUS == GlobalConstantsEdu.STATUS_UNPAID) // Bổ sung gửi tin cho các thuê bao Nợ cước 
                                                                         && scd.IS_ACTIVE == true && sc.STATUS == GlobalConstantsEdu.SMS_PARENT_CONTRACT_STATUS_IS_ACCEPT && (sc.IS_DELETED == false || sc.IS_DELETED == null)
                                                                         &&
                                                                         (
                                                                          (semesterID == 1 && (scd.SUBSCRIPTION_TIME == GlobalConstantsEdu.CONTRACTDETAIL_SUBSCRIPTIONTIME_FIRST_SMESTER || scd.SUBSCRIPTION_TIME == GlobalConstantsEdu.CONTRACTDETAIL_SUBSCRIPTIONTIME_ALLYEAR))
                                                                          ||
                                                                          (semesterID == 2 && (scd.SUBSCRIPTION_TIME == GlobalConstantsEdu.CONTRACTDETAIL_SUBSCRIPTIONTIME_SECOND_SMESTER || scd.SUBSCRIPTION_TIME == GlobalConstantsEdu.CONTRACTDETAIL_SUBSCRIPTIONTIME_ALLYEAR))
                                                                         ) && scd.SERVICE_PACKAGE_ID != ServicePackageID
                                                                         && scd.YEAR_ID == year
                                                                         && poc.Year == year
                                                                         && poc.ClassID == sc.CLASS_ID
                                                                         && pf.IsActive
                                                                         select new SMSParentContractBO
                                                                         {
                                                                             PupilFileID = sc.PUPIL_ID,
                                                                             PhoneNumberRecever = scd.SUBSCRIBER,
                                                                             SMSParentContractDetailID = scd.SMS_PARENT_CONTRACT_DETAIL_ID,
                                                                             SubscriptionTime = scd.SUBSCRIPTION_TIME,
                                                                             Year = scd.YEAR_ID
                                                                         });


            if (pupilIDList != null && pupilIDList.Count > 0)
            {
                smsParentContractBOIQuery = smsParentContractBOIQuery.Where(p => pupilIDList.Contains(p.PupilFileID));
            }
            return smsParentContractBOIQuery;
        }

        /// <summary>
        /// Tamhm1 - Tìm kiếm theo lớp
        /// </summary>
        /// <param name="dic"></param>
        /// <param name="pupilProfileBOList"></param>
        /// <returns></returns>
        public List<PupilProfileBO> SearchSMSParentRegister(Dictionary<string, object> dic, List<PupilProfileBO> pupilProfileBOList)
        {
            dic["lstPupilID"] = pupilProfileBOList.Select(p => p.PupilProfileID).ToList();
            int? Year = dic.GetInt("Year");
            int currentSemester = dic.GetInt("currentSemester");
            int TypeID = dic.GetInt("TypeID");
            int SchoolID = dic.GetInt("SchoolID");
            List<SMS_HISTORY> lstSMSHistory = SMSHistoryBusiness.All.Where(o => o.SCHOOL_ID == SchoolID && o.YEAR == Year && o.SMS_TYPE_ID == TypeID && o.STATUS == true).ToList();
            List<SMSParentContractBO> smsParentContractBOListTemp = getSMSParentContractRegister(dic).Distinct().ToList();
            PupilProfileBO ppBO = new PupilProfileBO();
            PupilProfileBO pp = new PupilProfileBO();
            List<PupilProfileBO> lstPupilBO = new List<PupilProfileBO>();
            pupilProfileBOList = pupilProfileBOList.Where(o => (o.FatherMobile != null || o.MotherMobile != null || o.SponsorMobile != null)).ToList();
            for (int i = 0; i < pupilProfileBOList.Count(); i++)
            {
                ppBO = pupilProfileBOList[i];
                if (!string.IsNullOrEmpty(ppBO.FatherMobile) && ppBO.FatherMobile.CheckMobileNumber())
                {
                    pp = new PupilProfileBO();
                    pp.PupilProfileID = ppBO.PupilProfileID;
                    pp.ClassName = ppBO.ClassName;
                    pp.OrderInClass = ppBO.OrderInClass;
                    pp.Mobile = ppBO.FatherMobile;
                    lstPupilBO.Add(pp);
                }
                if (!string.IsNullOrEmpty(ppBO.MotherMobile) && ppBO.MotherMobile.CheckMobileNumber() && CheckDuplicateMobile(ppBO.MotherMobile, ppBO.FatherMobile))
                {
                    pp = new PupilProfileBO();
                    pp.PupilProfileID = ppBO.PupilProfileID;
                    pp.Mobile = ppBO.MotherMobile;
                    pp.ClassName = ppBO.ClassName;
                    pp.OrderInClass = ppBO.OrderInClass;
                    lstPupilBO.Add(pp);
                }
                if (!string.IsNullOrEmpty(ppBO.SponsorMobile) && ppBO.SponsorMobile.CheckMobileNumber() && CheckDuplicateMobile(ppBO.SponsorMobile, ppBO.FatherMobile) && CheckDuplicateMobile(ppBO.SponsorMobile, pp.MotherMobile))
                {
                    pp = new PupilProfileBO();
                    pp.PupilProfileID = ppBO.PupilProfileID;
                    pp.Mobile = ppBO.SponsorMobile;
                    pp.ClassName = ppBO.ClassName;
                    pp.OrderInClass = ppBO.OrderInClass;
                    lstPupilBO.Add(pp);
                }
            }
            lstPupilBO = lstPupilBO.Where(o => !smsParentContractBOListTemp.Any(u => (u.PupilFileID == o.PupilProfileID && u.PhoneNumberRecever.SubstringPhone().Equals(o.Mobile.SubstringPhone()))) && !lstSMSHistory.Any(u => u.RECEIVER_ID == o.PupilProfileID && u.MOBILE.SubstringPhone().Equals(o.Mobile.SubstringPhone()))).ToList();
            return lstPupilBO;
        }

        public IQueryable<PupilProfileBO> GetSMSParentContract(IDictionary<string,object> dic)
        {
            int SchoolID = Utils.GetInt(dic, "SchoolID");
            int AcademicYearID = Utils.GetInt(dic, "AcademicYearID");
            int ClassID = Utils.GetInt(dic, "ClassID");
            int semester = Utils.GetInt(dic, "SemesterID");
            int year = Utils.GetInt(dic, "Year");
            List<int> lstClassID = Utils.GetIntList(dic, "lstClassID");
            int PartitionID = UtilsBusiness.GetPartionId(SchoolID);
            IQueryable<PupilProfileBO> iquery = (from sc in SMSParentContractBusiness.All
                                                 join scd in SMSParentContractDetailBusiness.All on sc.SMS_PARENT_CONTRACT_ID equals scd.SMS_PARENT_CONTRACT_ID
                                                 join poc in PupilOfClassBusiness.All on sc.PUPIL_ID equals poc.PupilID
                                                 join pf in PupilProfileBusiness.All on sc.PUPIL_ID equals pf.PupilProfileID
                                                 join pdtl in ServicePackageDetailBusiness.All on scd.SERVICE_PACKAGE_ID equals pdtl.SERVICE_PACKAGE_ID
                                                 join spdd in ServicePackageDeclareBusiness.All on scd.SERVICE_PACKAGE_ID equals spdd.SERVICE_PACKAGE_ID
                                                 join cp in ClassProfileBusiness.All on poc.ClassID equals cp.ClassProfileID
                                                 where sc.SCHOOL_ID == SchoolID && sc.PARTITION_ID == SchoolID % GlobalConstantsEdu.PARTITION_SMSPARENTCONTRACT_SCHOOLID
                                                    && (scd.SUBSCRIPTION_STATUS == GlobalConstantsEdu.STATUS_REGISTED || scd.SUBSCRIPTION_STATUS == GlobalConstantsEdu.STATUS_UNPAID) // Bổ sung gửi tin cho các thuê bao Nợ cước
                                                    && scd.IS_ACTIVE == true && sc.STATUS == GlobalConstantsEdu.SMS_PARENT_CONTRACT_STATUS_IS_ACCEPT
                                                    && (sc.IS_DELETED == null || sc.IS_DELETED == false)
                                                    &&
                                                    (
                                                        (semester == 1 && (scd.SUBSCRIPTION_TIME == GlobalConstantsEdu.CONTRACTDETAIL_SUBSCRIPTIONTIME_FIRST_SMESTER || scd.SUBSCRIPTION_TIME == GlobalConstantsEdu.CONTRACTDETAIL_SUBSCRIPTIONTIME_ALLYEAR))
                                                        ||
                                                        (semester == 2 && (scd.SUBSCRIPTION_TIME == GlobalConstantsEdu.CONTRACTDETAIL_SUBSCRIPTIONTIME_SECOND_SMESTER || scd.SUBSCRIPTION_TIME == GlobalConstantsEdu.CONTRACTDETAIL_SUBSCRIPTIONTIME_ALLYEAR))
                                                    )
                                                    && scd.YEAR_ID == year
                                                    && spdd.YEAR == year
                                                    && spdd.STATUS == true
                                                    && poc.Year == year
                                                    && poc.ClassID == sc.CLASS_ID
                                                    && pf.IsActive
                                                 group sc by new
                                                 {
                                                     sc.PUPIL_ID,
                                                     pf.PupilCode,
                                                     pf.FullName,
                                                     pf.Name,
                                                     cp.ClassProfileID,
                                                     cp.DisplayName,
                                                     cp.EducationLevelID,
                                                     cp.OrderNumber,
                                                     poc.OrderInClass
                                                 }into g
                                                 select new PupilProfileBO
                                                 {
                                                     PupilProfileID = g.Key.PUPIL_ID,
                                                     PupilCode = g.Key.PupilCode,
                                                     Name = g.Key.Name,
                                                     FullName = g.Key.FullName,
                                                     CurrentClassID = g.Key.ClassProfileID,
                                                     ClassName = g.Key.DisplayName,
                                                     EducationLevelID = g.Key.EducationLevelID,
                                                     ClassOrderNumber = g.Key.OrderNumber.HasValue ? g.Key.OrderNumber : 0,
                                                     OrderInClass = g.Key.OrderInClass
                                                 }).OrderBy(p=>p.EducationLevelID).ThenBy(p=>p.ClassOrderNumber)
                                                 .ThenBy(p=>p.ClassName).ThenBy(p=>p.OrderInClass).ThenBy(p=>p.Name);
            if (ClassID > 0)
            {
                iquery = iquery.Where(p => p.CurrentClassID == ClassID);
            }
            if (lstClassID.Count > 0)
            {
                iquery = iquery.Where(p => lstClassID.Contains(p.CurrentClassID));
            }
            return iquery;
        }
        #endregion

        #region Áp dụng chức năng Quản lý Hợp đồng
        public List<SMS_PARENT_CONTRACT_DETAIL> getListContractDetailByContractID(int schoolID, int year, int semesterid, int contractID, bool excludeLimitPackage = false)
        {
            List<SMS_PARENT_CONTRACT_DETAIL> listContract = (from pc in SMSParentContractBusiness.All
                                                             join pcd in SMSParentContractDetailBusiness.All on pc.SMS_PARENT_CONTRACT_ID equals pcd.SMS_PARENT_CONTRACT_ID
                                                             join poc in PupilOfClassBusiness.All on pc.PUPIL_ID equals poc.PupilID
                                                             join pf in PupilProfileBusiness.All on pc.PUPIL_ID equals pf.PupilProfileID
                                                             join sp in ServicePackageBusiness.All on pcd.SERVICE_PACKAGE_ID equals sp.SERVICE_PACKAGE_ID
                                                             join spd in ServicePackageDeclareBusiness.All on sp.SERVICE_PACKAGE_ID equals spd.SERVICE_PACKAGE_ID
                                                             where pc.SCHOOL_ID == schoolID && pc.PARTITION_ID == schoolID % GlobalConstantsEdu.PARTITION_SMSPARENTCONTRACT_SCHOOLID
                                                             && pcd.SMS_PARENT_CONTRACT_ID == contractID
                                                             && pcd.YEAR_ID == year
                                                             && spd.YEAR == year
                                                             && pcd.IS_ACTIVE
                                                                   && (pcd.SUBSCRIPTION_TIME == semesterid)
                                                             && (pc.IS_DELETED == false || pc.IS_DELETED == null)
                                                             && pcd.SUBSCRIPTION_STATUS != GlobalConstantsEdu.STATUS_DELETE_CONTRACT_DETAIL
                                                             && (excludeLimitPackage == false || (excludeLimitPackage == true && spd.IS_LIMIT != true))
                                                             && pc.CLASS_ID == poc.ClassID
                                                             && poc.Year == year
                                                             && pf.IsActive
                                                             select pcd).ToList();
            return listContract;
        }

        public List<SMSParentContractBO> GetListSMSParentContractDetailForExtra(int schoolId, int academicYearId, int classId, int year, int semester, int servicePackageId)
        {
            var lstPupilID = PupilOfClassBusiness.GetListPupilByClassID(schoolId, academicYearId, classId).Select(p => p.PupilID).Distinct().ToList();


            //Danh sch hop dong cu truong theo status
            var listSMSParentContract = (from ct in SMSParentContractBusiness.All
                                         join ctd in SMSParentContractDetailBusiness.All on ct.SMS_PARENT_CONTRACT_ID equals ctd.SMS_PARENT_CONTRACT_ID
                                         join poc in PupilOfClassBusiness.All on ct.PUPIL_ID equals poc.PupilID
                                         join pf in PupilProfileBusiness.All on ct.PUPIL_ID equals pf.PupilProfileID
                                         join sp in ServicePackageBusiness.All on ctd.SERVICE_PACKAGE_ID equals sp.SERVICE_PACKAGE_ID
                                         where ct.SCHOOL_ID == schoolId && ct.PARTITION_ID == schoolId % GlobalConstantsEdu.PARTITION_SMSPARENTCONTRACT_SCHOOLID
                                         && ctd.YEAR_ID == year
                                         && ctd.SUBSCRIPTION_STATUS == GlobalConstantsEdu.STATUS_REGISTED
                                         && ctd.IS_ACTIVE
                                         && (ct.IS_DELETED == null || ct.IS_DELETED == false)
                                         && (ctd.SUBSCRIPTION_TIME == semester || semester == 0)
                                         && (ctd.SERVICE_PACKAGE_ID == servicePackageId || servicePackageId == 0)
                                         && lstPupilID.Contains(ct.PUPIL_ID)
                                         && ct.CLASS_ID == poc.ClassID
                                         && poc.Year == year
                                         && pf.IsActive
                                         select new SMSParentContractBO
                                         {
                                             PupilFileID = ct.PUPIL_ID,
                                             PhoneNumberRecever = ctd.SUBSCRIBER,
                                             ServicePackageID = ctd.SERVICE_PACKAGE_ID,
                                             ServiceCode = sp.SERVICE_CODE,
                                             SubscriptionTime = ctd.SUBSCRIPTION_TIME,
                                             SubscriptionStatus = ctd.SUBSCRIPTION_STATUS,
                                             SMSParentContractDetailID = ctd.SMS_PARENT_CONTRACT_DETAIL_ID,
                                             SMSParentContractID = ct.SMS_PARENT_CONTRACT_ID,
                                             RelationShip = ctd.RELATION_SHIP,
                                             StatusContract = ct.STATUS,
                                             CreateTime = ctd.CREATED_TIME,
                                             UpdateTime = ctd.UPDATED_TIME,
                                             ViettelPrice = sp.VIETTEL_PRICE,
                                             OtherPrice = sp.OTHER_PRICE,
                                             Subcriber = ctd.SUBSCRIBER
                                         }).ToList();


            return listSMSParentContract;
        }

        /// <summary>
        /// Danh sach tat ca hoc sinh co hop dong va khong co hop dong
        /// </summary>
        /// <param name="listPupil"></param>
        /// <param name="schoolID"></param>
        /// <param name="year"></param>
        /// <param name="status"></param>
        /// <returns></returns>
        public List<SMSParentContractGroupBO> GetListSMSParentContract(int schoolID,int academicYearID,int appliedLevelID,int employeeID, int classID, int educationLevelID, string pupilCode, string pupilName,string mobilePhone,
            int year, int status, int currentPage, int pageSize, ref int totalRecord)
        {
            #region Ra soat thue bao No cuoc qua han
            this.checkOverdueUnpaid(schoolID, year);
            #endregion

            List<int> pupilIDList = new List<int>();
            List<PupilOfClassBO> listPupil = new List<PupilOfClassBO>();
            List<SMSParentContractBO> listSMSParentContract = new List<SMSParentContractBO>();
            //hoc sinh co hop dong
            PupilOfClassBO objPOC = null;
            List<SMSParentContractGroupBO> smsParentContractGroupList = new List<SMSParentContractGroupBO>();
            SMSParentContractGroupBO smsParentContractGroupBO = null;
            //chi tiet hop dong
            List<SMSParentContractBO> smsParentContractBOList = new List<SMSParentContractBO>();
            List<SMSParentContractBO> smsParentContractBOListTmp = new List<SMSParentContractBO>();
            List<ContractDetailList> contractDetailList = null;
            ContractDetailList contractDetailListObj = null;
            SMSParentContractBO contractObjTmp = null;
            //chi tiet thoi gian cua hop dong
            List<int?> subscriptionTime = null;
            int pupilID = 0;
            string phoneNumber = string.Empty;
            DateTime? updateTime;
            int subscriptionStatus = 0;
            int? servicePackageID = 0;
            if (!string.IsNullOrWhiteSpace(mobilePhone) && status == GlobalConstantsEdu.STATUS_NOT_REGISTED)
            {
                return smsParentContractGroupList;
            }
            if (!string.IsNullOrWhiteSpace(mobilePhone) && status == 0)
            {
                status = GlobalConstantsEdu.STATUS_REGISTED;
            }
            //Lay danh sach theo status, se lay thong tin hop dong truong khi lay thong tin hoc sinh
            if (status != 0 && (status == GlobalConstantsEdu.STATUS_REGISTED || status == GlobalConstantsEdu.STATUS_STOP_USING || status == GlobalConstantsEdu.STATUS_DELETE_CONTRACT_DETAIL ||
                                   status == GlobalConstantsEdu.STATUS_UNPAID || status == GlobalConstantsEdu.STATUS_OVERDUE_UNPAID || status == GlobalConstantsEdu.STATUS_IN_ACTIVE))
            {
                //Danh sch hop dong cu truong theo status
                listSMSParentContract = (from ct in SMSParentContractBusiness.All
                                         join ctd in SMSParentContractDetailBusiness.All on ct.SMS_PARENT_CONTRACT_ID equals ctd.SMS_PARENT_CONTRACT_ID
                                         join sp in ServicePackageBusiness.All on ctd.SERVICE_PACKAGE_ID equals sp.SERVICE_PACKAGE_ID
                                         join spd in ServicePackageDeclareBusiness.All on sp.SERVICE_PACKAGE_ID equals spd.SERVICE_PACKAGE_ID
                                         where ct.SCHOOL_ID == schoolID && ct.PARTITION_ID == schoolID % GlobalConstantsEdu.PARTITION_SMSPARENTCONTRACT_SCHOOLID
                                         && ctd.YEAR_ID == year
                                         && (mobilePhone == null || mobilePhone == "" || ctd.SUBSCRIBER.Contains(mobilePhone))
                                         && (
                                             (status == GlobalConstantsEdu.STATUS_REGISTED && (ctd.SUBSCRIPTION_STATUS == GlobalConstantsEdu.STATUS_REGISTED || ctd.SUBSCRIPTION_STATUS == GlobalConstantsEdu.STATUS_STOP_USING || ctd.SUBSCRIPTION_STATUS == GlobalConstantsEdu.STATUS_DELETE_CONTRACT_DETAIL ||
                                               ctd.SUBSCRIPTION_STATUS == GlobalConstantsEdu.STATUS_UNPAID || ctd.SUBSCRIPTION_STATUS == GlobalConstantsEdu.STATUS_OVERDUE_UNPAID))
                                             || (status == GlobalConstantsEdu.STATUS_IN_ACTIVE && ctd.SUBSCRIPTION_STATUS == GlobalConstantsEdu.STATUS_REGISTED)
                                             || (ctd.SUBSCRIPTION_STATUS == status)
                                         )
                                         && ctd.IS_ACTIVE
                                         && (ct.IS_DELETED == null || ct.IS_DELETED == false)
                                         && spd.YEAR == year
                                         && spd.STATUS == true
                                         select new SMSParentContractBO
                                         {
                                             PupilFileID = ct.PUPIL_ID,
                                             PhoneNumberRecever = ctd.SUBSCRIBER,
                                             ServicePackageID = ctd.SERVICE_PACKAGE_ID,
                                             ServiceCode = sp.SERVICE_CODE,
                                             IsWholeYearPackage = spd.IS_WHOLE_YEAR_PACKAGE,
                                             IsLimitPackage = spd.IS_LIMIT,
                                             SubscriptionTime = ctd.SUBSCRIPTION_TIME,
                                             SubscriptionStatus = ctd.SUBSCRIPTION_STATUS,
                                             SMSParentContractDetailID = ctd.SMS_PARENT_CONTRACT_DETAIL_ID,
                                             SMSParentContractID = ct.SMS_PARENT_CONTRACT_ID,
                                             RelationShip = ctd.RELATION_SHIP,
                                             StatusContract = ct.STATUS,
                                             CreateTime = ctd.CREATED_TIME,
                                             UpdateTime = ctd.UPDATED_TIME
                                         }).ToList();
                //Danh sach ID hoc sinh co hop dong 
                pupilIDList = listSMSParentContract.Select(p => p.PupilFileID).Distinct().ToList();
                //lay danh sach hoc sinh theo listPupilID
                listPupil = PupilProfileBusiness.GetPupilsByIDsPaging(pupilIDList, schoolID, academicYearID, classID, educationLevelID, pupilCode, pupilName, appliedLevelID, currentPage, pageSize, ref totalRecord, employeeID);

                //|Danh sach cac goi mua them
                var lstSMSParentContractExtra = (from ct in SMSParentContractBusiness.All
                                                 join ctd in SMSParentContractDetailBusiness.All on ct.SMS_PARENT_CONTRACT_ID equals ctd.SMS_PARENT_CONTRACT_ID
                                                 join sp in ServicePackageBusiness.All on ctd.SERVICE_PACKAGE_ID equals sp.SERVICE_PACKAGE_ID
                                                 join se in SMSParentContractExtraBusiness.All on ctd.SMS_PARENT_CONTRACT_DETAIL_ID equals se.SMS_PARENT_CONTRACT_DETAIL_ID
                                                 join spe in ServicePackageBusiness.All on se.SERVICE_PACKAGE_ID equals spe.SERVICE_PACKAGE_ID
                                                 where ct.SCHOOL_ID == schoolID && ct.PARTITION_ID == schoolID % GlobalConstantsEdu.PARTITION_SMSPARENTCONTRACT_SCHOOLID
                                                 && ctd.YEAR_ID == year
                                                 && (mobilePhone == null || mobilePhone == "" || ctd.SUBSCRIBER.Contains(mobilePhone))
                                                 && (
                                                     (status == GlobalConstantsEdu.STATUS_REGISTED && (ctd.SUBSCRIPTION_STATUS == GlobalConstantsEdu.STATUS_REGISTED || ctd.SUBSCRIPTION_STATUS == GlobalConstantsEdu.STATUS_STOP_USING || ctd.SUBSCRIPTION_STATUS == GlobalConstantsEdu.STATUS_DELETE_CONTRACT_DETAIL ||
                                                       ctd.SUBSCRIPTION_STATUS == GlobalConstantsEdu.STATUS_UNPAID || ctd.SUBSCRIPTION_STATUS == GlobalConstantsEdu.STATUS_OVERDUE_UNPAID))
                                                        || (status == GlobalConstantsEdu.STATUS_IN_ACTIVE && ctd.SUBSCRIPTION_STATUS == GlobalConstantsEdu.STATUS_REGISTED)
                                                       || (ctd.SUBSCRIPTION_STATUS == status)
                                                 )
                                                 && ctd.IS_ACTIVE
                                                 && (ct.IS_DELETED == null || ct.IS_DELETED == false)
                                                 select new {
                                                     SMS_PARENT_CONTRACT_ID = se.SMS_PARENT_CONTRACT_ID,
                                                     SMS_PARENT_CONTRACT_DETAIL_ID = se.SMS_PARENT_CONTRACT_DETAIL_ID,
                                                     SERVICE_PACKAGE_CODE = spe.SERVICE_CODE
                                                 }).ToList();
                for (int i = 0; i < listPupil.Count; i++)
                {
                    // Danh sach hoc sinh co hop dong
                    smsParentContractGroupBO = new SMSParentContractGroupBO();
                    objPOC = listPupil[i];
                    pupilID = objPOC.PupilID;
                    smsParentContractGroupBO.PupilID = objPOC.PupilID;
                    smsParentContractGroupBO.PupilCode = objPOC.PupilCode;
                    smsParentContractGroupBO.PupilName = objPOC.PupilFullName;
                    smsParentContractGroupBO.ClassID = objPOC.ClassID;
                    smsParentContractGroupBO.ClassName = objPOC.ClassName;
                    smsParentContractGroupBO.StatusPupil = objPOC.Status;
                    smsParentContractGroupBO.PupilOfClassID = objPOC.PupilOfClassID;
                    //chi  tiet hop dong cua tung hoc sinh
                    smsParentContractBOList = listSMSParentContract.Where(p => p.PupilFileID == pupilID).ToList();
                    //list danh sach so dien thoai cua 1 hoc sinh dung de for
                    List<SMSParentContractBO> listContractTmp = (from l in smsParentContractBOList
                                                                 group l by new { l.UpdateTime, l.PhoneNumberRecever, l.SubscriptionStatus, l.ServicePackageID } into g
                                                                 orderby g.Key.UpdateTime
                                                                 select new SMSParentContractBO
                                                                 {
                                                                     PhoneNumberRecever = g.Key.PhoneNumberRecever,
                                                                     SubscriptionStatus = g.Key.SubscriptionStatus,
                                                                     UpdateTime = g.Key.UpdateTime,
                                                                     ServicePackageID = g.Key.ServicePackageID
                                                                 }).ToList();

                    if (listContractTmp != null && listContractTmp.Count > 0)
                    {
                        contractDetailList = new List<ContractDetailList>();
                        for (int j = 0; j < listContractTmp.Count; j++)
                        {
                            //them vao danh sach chi tiet hop dong
                            // smsParentContractID = smsParentContractBOList[j].SMSParentContractID;
                            contractObjTmp = listContractTmp[j];
                            phoneNumber = contractObjTmp.PhoneNumberRecever;
                            servicePackageID = contractObjTmp.ServicePackageID;
                            updateTime = contractObjTmp.UpdateTime;

                            subscriptionStatus = contractObjTmp.SubscriptionStatus;
                            contractDetailListObj = new ContractDetailList();
                            SMSParentContractBO objDetail = smsParentContractBOList.Where(p => p.PhoneNumberRecever == phoneNumber && p.ServicePackageID == servicePackageID && p.SubscriptionStatus == subscriptionStatus).FirstOrDefault();
                            contractDetailListObj.RelationShip = objDetail.RelationShip;
                            contractDetailListObj.PhoneReceiver = objDetail.PhoneNumberRecever;
                            contractDetailListObj.ServicesPakageID = objDetail.ServicePackageID;
                            contractDetailListObj.ServiceCode = objDetail.ServiceCode;
                            contractDetailListObj.IsWholeYearPackage = objDetail.IsWholeYearPackage;
                            contractDetailListObj.IsLimitPackage = objDetail.IsLimitPackage;
                            contractDetailListObj.SubscriptionStatus = objDetail.SubscriptionStatus;
                            contractDetailListObj.SMSParentContractDetailID = objDetail.SMSParentContractDetailID;
                            contractDetailListObj.SMSParentContractID = objDetail.SMSParentContractID;
                            contractDetailListObj.CreateTime = objDetail.CreateTime;

                            //thoi gian hop dong HK1, HK2
                            // review: de obj subsciptionTime vao detail
                            subscriptionTime = new List<int?>();
                            if (updateTime != null) smsParentContractBOListTmp = smsParentContractBOList.Where(p => p.PhoneNumberRecever == phoneNumber && p.UpdateTime == updateTime && p.SubscriptionStatus == subscriptionStatus && p.ServicePackageID == servicePackageID).ToList();
                            else smsParentContractBOListTmp = smsParentContractBOList.Where(p => p.PhoneNumberRecever == phoneNumber && p.SubscriptionStatus == subscriptionStatus && p.ServicePackageID == servicePackageID && p.UpdateTime == null).ToList();

                            subscriptionTime = smsParentContractBOListTmp.Select(p => p.SubscriptionTime).ToList();

                            if (subscriptionTime.Count == 1)
                            {
                                contractDetailListObj.SMSParentContractDetailID = smsParentContractBOList.FirstOrDefault(p => p.ServicePackageID == servicePackageID && p.SubscriptionStatus == subscriptionStatus && p.SubscriptionTime == subscriptionTime.FirstOrDefault()).SMSParentContractDetailID;
                            }
                            contractDetailListObj = this.GetSemester(subscriptionTime, contractDetailListObj);// lay thong tin semester

                            //Lay thong tin goi mua them
                            List<string> lstExtraServicePackageCode = new List<string>();
                            List<string> lstExtraServicePackageCode1 = new List<string>();
                            List<string> lstExtraServicePackageCode2 = new List<string>();
                            if (contractDetailListObj.FirstSemester == GlobalConstantsEdu.SEMESTER_OF_YEAR_FIRST && contractDetailListObj.SecondSemester == GlobalConstantsEdu.SEMESTER_OF_YEAR_SECOND)
                            {
                                long contractDetailID1 = smsParentContractBOList.FirstOrDefault(p => p.PhoneNumberRecever == contractDetailListObj.PhoneReceiver && p.ServicePackageID == servicePackageID && p.SubscriptionStatus == subscriptionStatus && p.SubscriptionTime == GlobalConstantsEdu.SEMESTER_OF_YEAR_FIRST).SMSParentContractDetailID;
                                lstExtraServicePackageCode1 = lstSMSParentContractExtra.Where(o => o.SMS_PARENT_CONTRACT_ID == objDetail.SMSParentContractID
                                 && o.SMS_PARENT_CONTRACT_DETAIL_ID == contractDetailID1).Select(o => o.SERVICE_PACKAGE_CODE + "(HKI)").ToList();

                                long contractDetailID2 = smsParentContractBOList.FirstOrDefault(p => p.PhoneNumberRecever == contractDetailListObj.PhoneReceiver && p.ServicePackageID == servicePackageID && p.SubscriptionStatus == subscriptionStatus && p.SubscriptionTime == GlobalConstantsEdu.SEMESTER_OF_YEAR_SECOND).SMSParentContractDetailID;
                                lstExtraServicePackageCode2 = lstSMSParentContractExtra.Where(o => o.SMS_PARENT_CONTRACT_ID == objDetail.SMSParentContractID
                                 && o.SMS_PARENT_CONTRACT_DETAIL_ID == contractDetailID2).Select(o => o.SERVICE_PACKAGE_CODE + "(HKII)").ToList();

                                lstExtraServicePackageCode.AddRange(lstExtraServicePackageCode1);
                                lstExtraServicePackageCode.AddRange(lstExtraServicePackageCode2);
                            }
                            else
                            {
                                lstExtraServicePackageCode = lstSMSParentContractExtra.Where(o => o.SMS_PARENT_CONTRACT_ID == objDetail.SMSParentContractID
                                && o.SMS_PARENT_CONTRACT_DETAIL_ID == objDetail.SMSParentContractDetailID).Select(o => o.SERVICE_PACKAGE_CODE).ToList();
                            }

                            contractDetailListObj.ListExtraServicePackageCode = lstExtraServicePackageCode;
                            //contractDetailListObj.subscriptionTime = subscriptionTime;
                            contractDetailList.Add(contractDetailListObj);
                        }
                    }
                    smsParentContractGroupBO.contractDetailList = contractDetailList.OrderBy(p => p.CreateTime).ToList();
                    smsParentContractGroupList.Add(smsParentContractGroupBO);
                }
            }
            else
            {
                //khoi tao bien dung cho tim hoc sinh chua co hop dong
                bool isRegisContract = true;
                if (status == GlobalConstantsEdu.STATUS_NOT_REGISTED)
                {
                    isRegisContract = false;
                }
                if (classID > 0)
                {
                    listPupil = PupilProfileBusiness.GetPupilsbySchoolIDPaging(schoolID, academicYearID, classID, pupilCode, pupilName, educationLevelID, appliedLevelID, 1, pageSize, isRegisContract, ref totalRecord, employeeID);
                }
                else
                {
                    listPupil = PupilProfileBusiness.GetPupilsbySchoolIDPaging(schoolID, academicYearID, classID, pupilCode, pupilName, educationLevelID, appliedLevelID, currentPage, pageSize, isRegisContract, ref totalRecord, employeeID);
                }
                if (listPupil != null && listPupil.Count > 0)
                {
                    pupilIDList = listPupil.Select(p => p.PupilID).Distinct().ToList();//  review Distinct()
                    listSMSParentContract = (from ct in SMSParentContractBusiness.All
                                             join ctd in SMSParentContractDetailBusiness.All on ct.SMS_PARENT_CONTRACT_ID equals ctd.SMS_PARENT_CONTRACT_ID
                                             join sp in ServicePackageBusiness.All on ctd.SERVICE_PACKAGE_ID equals sp.SERVICE_PACKAGE_ID
                                             join spd in ServicePackageDeclareBusiness.All on sp.SERVICE_PACKAGE_ID equals spd.SERVICE_PACKAGE_ID
                                             where pupilIDList.Contains(ct.PUPIL_ID)
                                             && ct.SCHOOL_ID == schoolID && ct.PARTITION_ID == schoolID % GlobalConstantsEdu.PARTITION_SMSPARENTCONTRACT_SCHOOLID
                                             && ctd.YEAR_ID == year
                                             && ctd.IS_ACTIVE
                                             && (ctd.SUBSCRIPTION_STATUS == status || status == 0)
                                             && (mobilePhone == null || mobilePhone == "" || ctd.SUBSCRIBER.Contains(mobilePhone))
                                             && (ct.IS_DELETED == null || ct.IS_DELETED == false)
                                             && spd.YEAR == year
                                             && spd.STATUS == true
                                             select new SMSParentContractBO
                                             {
                                                 PupilFileID = ct.PUPIL_ID,
                                                 PhoneNumberRecever = ctd.SUBSCRIBER,
                                                 ServicePackageID = ctd.SERVICE_PACKAGE_ID,
                                                 ServiceCode = sp.SERVICE_CODE,
                                                 IsWholeYearPackage = spd.IS_WHOLE_YEAR_PACKAGE,
                                                 IsLimitPackage = spd.IS_LIMIT,
                                                 SubscriptionTime = ctd.SUBSCRIPTION_TIME,
                                                 SubscriptionStatus = ctd.SUBSCRIPTION_STATUS,
                                                 SMSParentContractDetailID = ctd.SMS_PARENT_CONTRACT_DETAIL_ID,
                                                 SMSParentContractID = ct.SMS_PARENT_CONTRACT_ID,
                                                 RelationShip = ctd.RELATION_SHIP,
                                                 StatusContract = ct.STATUS,
                                                 CreateTime = ctd.CREATED_TIME,
                                                 UpdateTime = ctd.UPDATED_TIME
                                             }).ToList();

                    //|Danh sach cac goi mua them
                    var lstSMSParentContractExtra = (from ct in SMSParentContractBusiness.All
                                                     join ctd in SMSParentContractDetailBusiness.All on ct.SMS_PARENT_CONTRACT_ID equals ctd.SMS_PARENT_CONTRACT_ID
                                                     join sp in ServicePackageBusiness.All on ctd.SERVICE_PACKAGE_ID equals sp.SERVICE_PACKAGE_ID
                                                     join se in SMSParentContractExtraBusiness.All on ctd.SMS_PARENT_CONTRACT_DETAIL_ID equals se.SMS_PARENT_CONTRACT_DETAIL_ID
                                                     join spe in ServicePackageBusiness.All on se.SERVICE_PACKAGE_ID equals spe.SERVICE_PACKAGE_ID
                                                     where pupilIDList.Contains(ct.PUPIL_ID)
                                                     && ct.SCHOOL_ID == schoolID && ct.PARTITION_ID == schoolID % GlobalConstantsEdu.PARTITION_SMSPARENTCONTRACT_SCHOOLID
                                                     && ctd.YEAR_ID == year
                                                     && (mobilePhone == null || mobilePhone == "" || ctd.SUBSCRIBER.Contains(mobilePhone))
                                                     && ctd.IS_ACTIVE
                                                     && (ctd.SUBSCRIPTION_STATUS == status || status == 0)
                                                     && (ct.IS_DELETED == null || ct.IS_DELETED == false)
                                                     select new
                                                     {
                                                         SMS_PARENT_CONTRACT_ID = se.SMS_PARENT_CONTRACT_ID,
                                                         SMS_PARENT_CONTRACT_DETAIL_ID = se.SMS_PARENT_CONTRACT_DETAIL_ID,
                                                         SERVICE_PACKAGE_CODE = spe.SERVICE_CODE
                                                     }).ToList();

                    for (int i = 0; i < listPupil.Count; i++)
                    {
                        // Danh sach hoc sinh co hop dong
                        smsParentContractGroupBO = new SMSParentContractGroupBO();
                        objPOC = listPupil[i];
                        pupilID = objPOC.PupilID;
                        smsParentContractGroupBO.PupilID = objPOC.PupilID;
                        smsParentContractGroupBO.PupilCode = objPOC.PupilCode;
                        smsParentContractGroupBO.PupilName = objPOC.PupilFullName;
                        smsParentContractGroupBO.ClassID = objPOC.ClassID;
                        smsParentContractGroupBO.ClassName = objPOC.ClassName;
                        smsParentContractGroupBO.StatusPupil = objPOC.Status;
                        smsParentContractGroupBO.PupilOfClassID = objPOC.PupilOfClassID;
                        //chi  tiet hop dong cua tung hoc sinh
                        smsParentContractBOList = listSMSParentContract.Where(p => p.PupilFileID == pupilID).ToList();
                        //list danh sach so dien thoai cua 1 hoc sinh dung de for
                        List<SMSParentContractBO> listContractTmp = (from l in smsParentContractBOList
                                                                     group l by new { l.UpdateTime, l.PhoneNumberRecever, l.SubscriptionStatus, l.ServicePackageID } into g
                                                                     orderby g.Key.UpdateTime
                                                                     select new SMSParentContractBO
                                                                     {
                                                                         PhoneNumberRecever = g.Key.PhoneNumberRecever,
                                                                         SubscriptionStatus = g.Key.SubscriptionStatus,
                                                                         UpdateTime = g.Key.UpdateTime,
                                                                         ServicePackageID = g.Key.ServicePackageID
                                                                     }).Distinct().ToList();


                        if (listContractTmp != null && listContractTmp.Count > 0)
                        {
                            contractDetailList = new List<ContractDetailList>();
                            for (int j = 0; j < listContractTmp.Count; j++)
                            {
                                //them vao danh sach chi tiet hop dong
                                // smsParentContractID = smsParentContractBOList[j].SMSParentContractID;
                                contractObjTmp = listContractTmp[j];
                                phoneNumber = contractObjTmp.PhoneNumberRecever;
                                servicePackageID = contractObjTmp.ServicePackageID;
                                updateTime = contractObjTmp.UpdateTime;

                                subscriptionStatus = contractObjTmp.SubscriptionStatus;
                                contractDetailListObj = new ContractDetailList();
                                SMSParentContractBO objDetail = smsParentContractBOList.Where(p => p.PhoneNumberRecever == phoneNumber && p.ServicePackageID == servicePackageID && p.SubscriptionStatus == subscriptionStatus).FirstOrDefault();
                                contractDetailListObj.RelationShip = objDetail.RelationShip;
                                contractDetailListObj.PhoneReceiver = objDetail.PhoneNumberRecever;
                                contractDetailListObj.ServicesPakageID = objDetail.ServicePackageID;
                                contractDetailListObj.ServiceCode = objDetail.ServiceCode;
                                contractDetailListObj.IsWholeYearPackage = objDetail.IsWholeYearPackage;
                                contractDetailListObj.IsLimitPackage = objDetail.IsLimitPackage;
                                contractDetailListObj.SubscriptionStatus = objDetail.SubscriptionStatus;
                                contractDetailListObj.SMSParentContractDetailID = objDetail.SMSParentContractDetailID;
                                contractDetailListObj.SMSParentContractID = objDetail.SMSParentContractID;
                                contractDetailListObj.CreateTime = objDetail.CreateTime;
                                //thoi gian hop dong HK1, HK2
                                subscriptionTime = new List<int?>();
                                if (updateTime != null) smsParentContractBOListTmp = smsParentContractBOList.Where(p => p.PhoneNumberRecever == phoneNumber && p.UpdateTime == updateTime && p.SubscriptionStatus == subscriptionStatus && p.ServicePackageID == servicePackageID).ToList();
                                else smsParentContractBOListTmp = smsParentContractBOList.Where(p => p.PhoneNumberRecever == phoneNumber && p.SubscriptionStatus == subscriptionStatus && p.ServicePackageID == servicePackageID && p.UpdateTime == null).ToList();

                                subscriptionTime = smsParentContractBOListTmp.Select(p => p.SubscriptionTime).ToList();
                                if (subscriptionTime.Count == 1)
                                {
                                    contractDetailListObj.SMSParentContractDetailID = smsParentContractBOList.FirstOrDefault(p => p.ServicePackageID == servicePackageID && p.SubscriptionStatus == subscriptionStatus && p.SubscriptionTime == subscriptionTime.FirstOrDefault()).SMSParentContractDetailID;
                                }
                                contractDetailListObj = this.GetSemester(subscriptionTime, contractDetailListObj);// lay thong tin semseter

                                //Lay thong tin goi mua them
                                List<string> lstExtraServicePackageCode = new List<string>();
                                List<string> lstExtraServicePackageCode1 = new List<string>();
                                List<string> lstExtraServicePackageCode2 = new List<string>();
                                if (contractDetailListObj.FirstSemester == GlobalConstantsEdu.SEMESTER_OF_YEAR_FIRST && contractDetailListObj.SecondSemester == GlobalConstantsEdu.SEMESTER_OF_YEAR_SECOND)
                                {
                                    long contractDetailID1 = smsParentContractBOList.FirstOrDefault(p => p.PhoneNumberRecever == contractDetailListObj.PhoneReceiver && p.ServicePackageID == servicePackageID && p.SubscriptionStatus == subscriptionStatus && p.SubscriptionTime == GlobalConstantsEdu.SEMESTER_OF_YEAR_FIRST).SMSParentContractDetailID;
                                    lstExtraServicePackageCode1 = lstSMSParentContractExtra.Where(o => o.SMS_PARENT_CONTRACT_ID == objDetail.SMSParentContractID
                                     && o.SMS_PARENT_CONTRACT_DETAIL_ID == contractDetailID1).Select(o => o.SERVICE_PACKAGE_CODE + "(HKI)").ToList();

                                    long contractDetailID2 = smsParentContractBOList.FirstOrDefault(p => p.PhoneNumberRecever == contractDetailListObj.PhoneReceiver && p.ServicePackageID == servicePackageID && p.SubscriptionStatus == subscriptionStatus && p.SubscriptionTime == GlobalConstantsEdu.SEMESTER_OF_YEAR_SECOND).SMSParentContractDetailID;
                                    lstExtraServicePackageCode2 = lstSMSParentContractExtra.Where(o => o.SMS_PARENT_CONTRACT_ID == objDetail.SMSParentContractID
                                     && o.SMS_PARENT_CONTRACT_DETAIL_ID == contractDetailID2).Select(o => o.SERVICE_PACKAGE_CODE + "(HKII)").ToList();

                                    lstExtraServicePackageCode.AddRange(lstExtraServicePackageCode1);
                                    lstExtraServicePackageCode.AddRange(lstExtraServicePackageCode2);
                                }
                                else
                                {
                                    lstExtraServicePackageCode = lstSMSParentContractExtra.Where(o => o.SMS_PARENT_CONTRACT_ID == objDetail.SMSParentContractID
                                    && o.SMS_PARENT_CONTRACT_DETAIL_ID == objDetail.SMSParentContractDetailID).Select(o => o.SERVICE_PACKAGE_CODE).ToList();
                                }

                                contractDetailListObj.ListExtraServicePackageCode = lstExtraServicePackageCode;

                                // contractDetailListObj.subscriptionTime = subscriptionTime;
                                contractDetailList.Add(contractDetailListObj);
                            }
                        }
                        else
                        {
                            //Neu hoc sinh chua co hop dong thi fill ra cac thong tin trong pupilprofile
                            // Danh sach hoc sinh co hop dong
                            contractDetailList = new List<ContractDetailList>();
                            smsParentContractGroupBO = new SMSParentContractGroupBO();
                            objPOC = listPupil[i];
                            pupilID = objPOC.PupilID;
                            smsParentContractGroupBO.PupilID = objPOC.PupilID;
                            smsParentContractGroupBO.PupilCode = objPOC.PupilCode;
                            smsParentContractGroupBO.PupilName = objPOC.PupilFullName;
                            smsParentContractGroupBO.ClassID = objPOC.ClassID;
                            smsParentContractGroupBO.ClassName = objPOC.ClassName;
                            smsParentContractGroupBO.FatherName = objPOC.FatherFullName;
                            smsParentContractGroupBO.MotherName = objPOC.MotherFullName;
                            smsParentContractGroupBO.StatusPupil = objPOC.Status;
                            smsParentContractGroupBO.PupilOfClassID = objPOC.PupilOfClassID;
                            contractDetailListObj = new ContractDetailList();
                            contractDetailListObj.RelationShip = "Mother";
                            contractDetailListObj.PhoneReceiver = objPOC.MotherMobile;
                            contractDetailListObj.RelationshipFather = "Father";
                            contractDetailListObj.PhoneFather = objPOC.FatherMobile;
                            contractDetailListObj.RelationshipMother = "Mother";
                            contractDetailListObj.PhoneMother = objPOC.MotherMobile;
                            contractDetailListObj.RelationshipOther = "Other";
                            contractDetailListObj.PhoneOther = objPOC.SponserMobile;
                            contractDetailList.Add(contractDetailListObj);
                        }
                        smsParentContractGroupBO.contractDetailList = contractDetailList.OrderBy(p => p.CreateTime).ToList();
                        smsParentContractGroupList.Add(smsParentContractGroupBO);
                    }
                }
            }
            return smsParentContractGroupList;
        }

        /// <summary>
        /// Lấy danh sách hợp đồng đang NỢ cước
        /// </summary>
        /// <param name="dic"></param>
        /// <returns></returns>
        public List<SMSParentContractGroupBO> getListContractUnPaid(Dictionary<string, object> dic)
        {
            int schoolID = dic.GetInt("SchoolID");
            int year = dic.GetInt("Year");
            int academicYearID = dic.GetInt("AcademicYearID");//truyen nam hoc
            short status = dic.GetShort("Status");
            int levelID = dic.GetInt("LevelID");
            int classID = dic.GetInt("ClassID");
            int level = dic.GetInt("Level");
            int appliedLevelID = dic.GetInt("AppliedLevelID");//truyen cap hoc
            int employeeID = dic.GetInt("EmployeeID");
            string pupilName = dic.GetString("PupilName");
            string pupilCode = dic.GetString("PupilCode");
            string mobile = dic.GetString("Mobile").Trim();

            #region Ra soat thue bao No cuoc qua han
            this.checkOverdueUnpaid(schoolID, year);
            #endregion

            List<int> pupilIDList = new List<int>();
            List<PupilOfClassBO> listPupil = new List<PupilOfClassBO>();

            //hoc sinh co hop dong
            PupilOfClassBO objPOC = null;
            List<SMSParentContractGroupBO> smsParentContractGroupList = new List<SMSParentContractGroupBO>();
            SMSParentContractGroupBO smsParentContractGroupBO = null;
            //chi tiet hop dong
            List<SMSParentContractBO> smsParentContractBOList = new List<SMSParentContractBO>();
            List<SMSParentContractBO> smsParentContractBOListTmp = new List<SMSParentContractBO>();
            List<ContractDetailList> contractDetailList = null;
            ContractDetailList contractDetailListObj = null;
            SMSParentContractBO contractObjTmp = null;
            List<SMSParentContractBO> lstUnpaidContract = (from ct in SMSParentContractBusiness.All
                                                           join ctd in SMSParentContractDetailBusiness.All on ct.SMS_PARENT_CONTRACT_ID equals ctd.SMS_PARENT_CONTRACT_ID
                                                           where ct.SCHOOL_ID == schoolID && ct.PARTITION_ID == schoolID % GlobalConstantsEdu.PARTITION_SMSPARENTCONTRACT_SCHOOLID
                                                           && ctd.YEAR_ID == year
                                                           && ((
                                                                   (status == 0 && (ctd.SUBSCRIPTION_STATUS == GlobalConstantsEdu.STATUS_UNPAID || ctd.SUBSCRIPTION_STATUS == GlobalConstantsEdu.STATUS_OVERDUE_UNPAID))
                                                               ) || ctd.SUBSCRIPTION_STATUS == status)
                                                           && ctd.IS_ACTIVE
                                                           && (ct.IS_DELETED == null || ct.IS_DELETED == false)
                                                           select new SMSParentContractBO
                                                           {
                                                               PupilFileID = ct.PUPIL_ID,
                                                               PhoneNumberRecever = ctd.SUBSCRIBER,
                                                               ServicePackageID = ctd.SERVICE_PACKAGE_ID,
                                                               SubscriptionTime = ctd.SUBSCRIPTION_TIME,
                                                               SubscriptionStatus = ctd.SUBSCRIPTION_STATUS,
                                                               SMSParentContractDetailID = ctd.SMS_PARENT_CONTRACT_DETAIL_ID,
                                                               SMSParentContractID = ct.SMS_PARENT_CONTRACT_ID,
                                                               RelationShip = ctd.RELATION_SHIP,
                                                               StatusContract = ct.STATUS,
                                                               CreateTime = ctd.CREATED_TIME,
                                                               UpdateTime = ctd.UPDATED_TIME,
                                                               Subcriber = ctd.SUBSCRIBER
                                                           }).ToList();

            if (!string.IsNullOrEmpty(mobile))
            {
                lstUnpaidContract = lstUnpaidContract.Where(o => o.Subcriber.Contains(mobile)).ToList();
            }
            //Danh sach ID hoc sinh co hop dong 
            pupilIDList = lstUnpaidContract.Select(p => p.PupilFileID).Distinct().ToList(); // Distinct - HS nhiều hợp đồng

            int PageSize = 10000;
            int totalRecord = 0;

            listPupil = PupilProfileBusiness.GetPupilsByIDsPaging(pupilIDList, schoolID, academicYearID, classID, levelID, pupilCode, pupilName, appliedLevelID, 1, PageSize, ref totalRecord, employeeID);

            for (int i = 0; i < listPupil.Count; i++)
            {
                // Danh sach hoc sinh co hop dong
                smsParentContractGroupBO = new SMSParentContractGroupBO();
                objPOC = listPupil[i];
                smsParentContractGroupBO.PupilID = objPOC.PupilID;
                smsParentContractGroupBO.PupilCode = objPOC.PupilCode;
                smsParentContractGroupBO.PupilName = objPOC.PupilFullName;
                smsParentContractGroupBO.ClassID = objPOC.ClassID;
                smsParentContractGroupBO.ClassName = objPOC.ClassName;
                smsParentContractGroupBO.StatusPupil = objPOC.Status;
                smsParentContractGroupBO.PupilOfClassID = objPOC.PupilOfClassID;
                //chi  tiet hop dong cua tung hoc sinh
                smsParentContractBOList = lstUnpaidContract.Where(p => p.PupilFileID == objPOC.PupilID).ToList();
                //list danh sach so dien thoai cua 1 hoc sinh dung de for
                List<SMSParentContractBO> listContractTmp = (from l in smsParentContractBOList
                                                             group l by new
                                                             {
                                                                 l.UpdateTime,
                                                                 l.PhoneNumberRecever,
                                                                 l.SubscriptionStatus,
                                                                 l.ServicePackageID,
                                                                 l.SMSParentContractID,
                                                                 l.SMSParentContractDetailID,
                                                                 l.RelationShip,
                                                                 l.CreateTime,
                                                                 l.SubscriptionTime
                                                             } into g
                                                             orderby g.Key.UpdateTime
                                                             select new SMSParentContractBO
                                                             {
                                                                 PhoneNumberRecever = g.Key.PhoneNumberRecever,
                                                                 SubscriptionStatus = g.Key.SubscriptionStatus,
                                                                 UpdateTime = g.Key.UpdateTime,
                                                                 ServicePackageID = g.Key.ServicePackageID,
                                                                 SMSParentContractID = g.Key.SMSParentContractID,
                                                                 SMSParentContractDetailID = g.Key.SMSParentContractDetailID,
                                                                 RelationShip = g.Key.RelationShip,
                                                                 CreateTime = g.Key.CreateTime,
                                                                 SubscriptionTime = g.Key.SubscriptionTime
                                                             }).ToList();

                if (listContractTmp != null && listContractTmp.Count > 0)
                {
                    contractDetailList = new List<ContractDetailList>();
                    for (int j = 0; j < listContractTmp.Count; j++)
                    {
                        //them vao danh sach chi tiet hop dong
                        contractObjTmp = listContractTmp[j];
                        smsParentContractGroupBO.SMSParentContractID = contractObjTmp.SMSParentContractID;
                        contractDetailListObj = new ContractDetailList();
                        contractDetailListObj.RelationShip = contractObjTmp.RelationShip;
                        contractDetailListObj.PhoneReceiver = contractObjTmp.PhoneNumberRecever;
                        contractDetailListObj.ServicesPakageID = contractObjTmp.ServicePackageID;
                        contractDetailListObj.SubscriptionStatus = contractObjTmp.SubscriptionStatus;
                        contractDetailListObj.SMSParentContractDetailID = contractObjTmp.SMSParentContractDetailID;
                        contractDetailListObj.SMSParentContractID = contractObjTmp.SMSParentContractID;
                        contractDetailListObj.CreateTime = contractObjTmp.CreateTime;

                        if (contractObjTmp.SubscriptionTime == GlobalConstantsEdu.SEMESTER_OF_YEAR_ALL)
                        {
                            contractDetailListObj.AllSemester = GlobalConstantsEdu.SEMESTER_OF_YEAR_ALL;
                        }
                        else if (contractObjTmp.SubscriptionTime == GlobalConstantsEdu.SEMESTER_OF_YEAR_FIRST)
                        {
                            contractDetailListObj.FirstSemester = GlobalConstantsEdu.SEMESTER_OF_YEAR_FIRST;
                        }
                        else if (contractObjTmp.SubscriptionTime == GlobalConstantsEdu.SEMESTER_OF_YEAR_SECOND)
                        {
                            contractDetailListObj.SecondSemester = GlobalConstantsEdu.SEMESTER_OF_YEAR_SECOND;
                        }
                        contractDetailList.Add(contractDetailListObj);
                    }
                }
                smsParentContractGroupBO.contractDetailList = contractDetailList.OrderBy(p => p.CreateTime).ToList();
                smsParentContractGroupList.Add(smsParentContractGroupBO);
            }

            return smsParentContractGroupList;
        }

        /// <summary>
        /// Xuat excel hop dong da duoc dang ky
        /// </summary>
        /// <param name="schoolID"></param>
        /// <param name="year"></param>
        /// <param name="status"></param>
        /// <param name="educationLevelID"></param>
        /// <param name="semesterid"></param>
        /// <param name="classID"></param>
        /// <param name="pupilName"></param>
        /// <param name="pupilCode"></param>
        /// <param name="grade"></param>
        /// <returns></returns>
        public List<SMSParentContractGroupBO> GetListExcelRegisted(int schoolID,int academicYearID, int year, int status, int educationLevelID, int semesterid, int classID, string pupilName, string pupilCode, string mobile, int grade,int employeeID)
        {
            #region Ra soat thue bao No cuoc qua han
            this.checkOverdueUnpaid(schoolID, year);
            #endregion

            //hoc sinh co hop dong
            PupilOfClassBO objPOC = null;
            List<SMSParentContractGroupBO> smsParentContractGroupList = new List<SMSParentContractGroupBO>();
            SMSParentContractGroupBO smsParentContractGroupBO = null;

            //Danh sach id hoc sinh da co hop dong 
            List<SMSParentContractBO> listSMSParentContract = (from ct in SMSParentContractBusiness.All
                                                               join ctd in SMSParentContractDetailBusiness.All on ct.SMS_PARENT_CONTRACT_ID equals ctd.SMS_PARENT_CONTRACT_ID
                                                               where ctd.YEAR_ID == year
                                                               && ct.SCHOOL_ID == schoolID && ct.PARTITION_ID == schoolID % GlobalConstantsEdu.PARTITION_SMSPARENTCONTRACT_SCHOOLID
                                                               && ((status == GlobalConstantsEdu.STATUS_REGISTED &&
                                                                  (ctd.SUBSCRIPTION_STATUS == GlobalConstantsEdu.STATUS_REGISTED || ctd.SUBSCRIPTION_STATUS == GlobalConstantsEdu.STATUS_STOP_USING || ctd.SUBSCRIPTION_STATUS == GlobalConstantsEdu.STATUS_DELETE_CONTRACT_DETAIL
                                                                          || ctd.SUBSCRIPTION_STATUS == GlobalConstantsEdu.STATUS_UNPAID || ctd.SUBSCRIPTION_STATUS == GlobalConstantsEdu.STATUS_OVERDUE_UNPAID))
                                                                     || (ctd.SUBSCRIPTION_STATUS == status)
                                                                   )
                                                               && ctd.IS_ACTIVE
                                                               && (ct.IS_DELETED == null || ct.IS_DELETED == false)
                                                               && (ctd.SUBSCRIBER == mobile || string.IsNullOrEmpty(mobile))
                                                               select new SMSParentContractBO
                                                               {
                                                                   PupilFileID = ct.PUPIL_ID,
                                                                   PhoneNumberRecever = ctd.SUBSCRIBER,
                                                                   ServicePackageID = ctd.SERVICE_PACKAGE_ID,
                                                                   SubscriptionTime = ctd.SUBSCRIPTION_TIME,
                                                                   SubscriptionStatus = ctd.SUBSCRIPTION_STATUS,
                                                                   SMSParentContractDetailID = ctd.SMS_PARENT_CONTRACT_DETAIL_ID,
                                                                   SMSParentContractID = ct.SMS_PARENT_CONTRACT_ID,
                                                                   RelationShip = ctd.RELATION_SHIP,
                                                                   StatusContract = ct.STATUS,
                                                                   CreateTime = ctd.CREATED_TIME,
                                                                   RegistrationType = ctd.REGISTRATION_TYPE // Chi su dung ResistrationType trong SMSParentContractDetail
                                                               }).ToList();

            //Lay danh sach goi cuoc mua them
            List<SMSParentContractBO> listSMSParentContractExtra = (from ct in SMSParentContractBusiness.All
                                                                    join ctd in SMSParentContractDetailBusiness.All on ct.SMS_PARENT_CONTRACT_ID equals ctd.SMS_PARENT_CONTRACT_ID
                                                                    join cte in SMSParentContractExtraBusiness.All on ctd.SMS_PARENT_CONTRACT_DETAIL_ID equals cte.SMS_PARENT_CONTRACT_DETAIL_ID
                                                                    where ctd.YEAR_ID == year
                                                                    && ct.SCHOOL_ID == schoolID && ct.PARTITION_ID == schoolID % GlobalConstantsEdu.PARTITION_SMSPARENTCONTRACT_SCHOOLID
                                                                    && ((status == GlobalConstantsEdu.STATUS_REGISTED &&
                                                                       (ctd.SUBSCRIPTION_STATUS == GlobalConstantsEdu.STATUS_REGISTED || ctd.SUBSCRIPTION_STATUS == GlobalConstantsEdu.STATUS_STOP_USING || ctd.SUBSCRIPTION_STATUS == GlobalConstantsEdu.STATUS_DELETE_CONTRACT_DETAIL
                                                                               || ctd.SUBSCRIPTION_STATUS == GlobalConstantsEdu.STATUS_UNPAID || ctd.SUBSCRIPTION_STATUS == GlobalConstantsEdu.STATUS_OVERDUE_UNPAID))
                                                                          || (ctd.SUBSCRIPTION_STATUS == status)
                                                                        )
                                                                    && ctd.IS_ACTIVE
                                                                    && (ctd.SUBSCRIBER == mobile || string.IsNullOrEmpty(mobile))
                                                                    && (ct.IS_DELETED == null || ct.IS_DELETED == false)
                                                                    && cte.IS_ACTIVE
                                                                    select new SMSParentContractBO
                                                                    {
                                                                        PupilFileID = ct.PUPIL_ID,
                                                                        PhoneNumberRecever = ctd.SUBSCRIBER,
                                                                        ServicePackageID = cte.SERVICE_PACKAGE_ID,
                                                                        SubscriptionTime = ctd.SUBSCRIPTION_TIME,
                                                                        SubscriptionStatus = ctd.SUBSCRIPTION_STATUS,
                                                                        SMSParentContractDetailID = ctd.SMS_PARENT_CONTRACT_DETAIL_ID,
                                                                        SMSParentContractID = ct.SMS_PARENT_CONTRACT_ID,
                                                                        RelationShip = ctd.RELATION_SHIP,
                                                                        StatusContract = ct.STATUS,
                                                                        CreateTime = ctd.CREATED_TIME,
                                                                        RegistrationType = ctd.REGISTRATION_TYPE // Chi su dung ResistrationType trong SMSParentContractDetail
                                                                    }).ToList();


            List<int> pupilIDList = listSMSParentContract.Select(p => p.PupilFileID).Distinct().ToList();
            //Lay thong tin hoc sinh theo list id
            List<PupilOfClassBO> listpupilProfile = PupilProfileBusiness.GetListPupilByListPupilIDForContract(pupilIDList, schoolID, academicYearID, classID, educationLevelID, pupilCode, pupilName, grade, employeeID).ToList();

            //chi tiet hop dong
            List<SMSParentContractBO> smsParentContractBOList = new List<SMSParentContractBO>();
            List<SMSParentContractBO> smsParentContractBOListExtra = new List<SMSParentContractBO>();
            List<SMSParentContractBO> smsParentDetailListTmp = new List<SMSParentContractBO>();
            SMSParentContractBO smsParentContractSubscriberTime = null;
            List<SMSParentContractBO> smsParentContractBOListTmp = new List<SMSParentContractBO>();
            List<ContractDetailList> contractDetailList = null;
            ContractDetailList contractDetailListObj = null;
            //chi tiet thoi gian cua hop dong
            List<int?> subscriptionTime = null;
            int pupilID = 0;
            string phoneNumber = string.Empty;
            DateTime? updateTime;
            int subscriptionStatus = 0;
            int? servicePackageID = 0;
            SMSParentContractBO contractObjTmp = null;

            if (listSMSParentContract.Count > 0 && listSMSParentContract != null)
            {
                for (int i = 0; i < listpupilProfile.Count; i++)
                {
                    // Danh sach hoc sinh co hop dong
                    smsParentContractGroupBO = new SMSParentContractGroupBO();
                    objPOC = listpupilProfile[i];
                    pupilID = objPOC.PupilID;
                    smsParentContractGroupBO.PupilID = objPOC.PupilID;
                    smsParentContractGroupBO.PupilCode = objPOC.PupilCode;
                    smsParentContractGroupBO.PupilName = objPOC.PupilFullName;
                    smsParentContractGroupBO.ClassID = objPOC.ClassID;
                    smsParentContractGroupBO.ClassName = objPOC.ClassName;

                    //chi  tiet hop dong cua tung hoc sinh
                    smsParentContractBOList = listSMSParentContract.Where(p => p.PupilFileID == pupilID).ToList();
                    smsParentContractBOListExtra = listSMSParentContractExtra.Where(p => p.PupilFileID == pupilID).ToList();
                    //KHong su dung RegistrationType trong SMSParenContract, chi su dung trong SMSParentContractDetail
                    // smsParentContractGroupBO.RegistrationType = smsParentContractObj != null ? smsParentContractObj.RegistrationType.HasValue ? smsParentContractObj.RegistrationType.Value : 1 : 1;
                    //list danh sach so dien thoai cua 1 hoc sinh dung de for
                    List<SMSParentContractBO> listContractTmp = (from l in smsParentContractBOList
                                                                 group l by new { l.UpdateTime, l.PhoneNumberRecever, l.SubscriptionStatus, l.ServicePackageID, l.SubscriptionTime } into g
                                                                 orderby g.Key.UpdateTime
                                                                 select new SMSParentContractBO
                                                                 {
                                                                     PhoneNumberRecever = g.Key.PhoneNumberRecever,
                                                                     SubscriptionStatus = g.Key.SubscriptionStatus,
                                                                     UpdateTime = g.Key.UpdateTime,
                                                                     ServicePackageID = g.Key.ServicePackageID,
                                                                     SubscriptionTime = g.Key.SubscriptionTime
                                                                 }).Distinct().ToList();

                    List<SMSParentContractBO> listContractTmpExtra = (from l in smsParentContractBOListExtra
                                                                      orderby l.UpdateTime
                                                                      select new SMSParentContractBO
                                                                      {
                                                                          PhoneNumberRecever = l.PhoneNumberRecever,
                                                                          SubscriptionStatus = l.SubscriptionStatus,
                                                                          UpdateTime = l.UpdateTime,
                                                                          ServicePackageID = l.ServicePackageID,
                                                                          SubscriptionTime = l.SubscriptionTime
                                                                      }).ToList();

                    if (listContractTmp != null && listContractTmp.Count > 0)
                    {
                        contractDetailList = new List<ContractDetailList>();
                        for (int j = 0; j < listContractTmp.Count; j++)
                        {
                            //them vao danh sach chi tiet hop dong
                            // smsParentContractID = smsParentContractBOList[j].SMSParentContractID;
                            contractObjTmp = listContractTmp[j];
                            phoneNumber = contractObjTmp.PhoneNumberRecever;
                            servicePackageID = contractObjTmp.ServicePackageID;
                            updateTime = contractObjTmp.UpdateTime;
                            subscriptionStatus = contractObjTmp.SubscriptionStatus;

                            contractDetailListObj = new ContractDetailList();
                            smsParentDetailListTmp = smsParentContractBOList.Where(p => p.PhoneNumberRecever == phoneNumber).ToList();
                            SMSParentContractBO objDetail = smsParentDetailListTmp.Where(p => p.ServicePackageID == servicePackageID && p.SubscriptionStatus == subscriptionStatus).FirstOrDefault();
                            contractDetailListObj.RelationShip = objDetail.RelationShip;
                            contractDetailListObj.PhoneReceiver = objDetail.PhoneNumberRecever;
                            contractDetailListObj.ServicesPakageID = objDetail.ServicePackageID;
                            contractDetailListObj.SubscriptionStatus = objDetail.SubscriptionStatus;
                            contractDetailListObj.SMSParentContractDetailID = objDetail.SMSParentContractDetailID;
                            contractDetailListObj.SMSParentContractID = objDetail.SMSParentContractID;
                            contractDetailListObj.CreateTime = objDetail.CreateTime;
                            contractDetailListObj.RegistrationType = smsParentDetailListTmp.Where(p => p.ServicePackageID == servicePackageID && p.SubscriptionStatus == subscriptionStatus).ToList().OrderByDescending(p => p.CreateTime).FirstOrDefault().RegistrationType;//hinh thuc dang ky SMS or truc tiep
                            //thoi gian hop dong KH1,KH2
                            subscriptionTime = new List<int?>();
                            if (updateTime != null) smsParentContractBOListTmp = smsParentDetailListTmp.Where(p => p.UpdateTime == updateTime && p.SubscriptionStatus == subscriptionStatus && p.ServicePackageID == objDetail.ServicePackageID).ToList();
                            else smsParentContractBOListTmp = smsParentDetailListTmp.Where(p => p.SubscriptionStatus == subscriptionStatus && p.ServicePackageID == objDetail.ServicePackageID && p.UpdateTime == null).ToList();

                            subscriptionTime = new List<int?> { contractObjTmp.SubscriptionTime };
                            if (subscriptionTime.Count == 1)
                            {
                                contractDetailListObj.SMSParentContractDetailID = smsParentDetailListTmp.FirstOrDefault(p => p.ServicePackageID == servicePackageID && p.SubscriptionStatus == subscriptionStatus && p.SubscriptionTime == subscriptionTime.FirstOrDefault()).SMSParentContractDetailID;
                            }
                            contractDetailListObj = this.GetSemester(subscriptionTime, contractDetailListObj);

                            #region Danh dau hinh thuc dang ky SMSParentContractDetail
                            //Neu co KH1 or HK2 dang ky SMS thi danh dau lai                            
                            if (contractDetailListObj.AllSemester == GlobalConstantsEdu.SEMESTER_OF_YEAR_ALL || (contractDetailListObj.FirstSemester == GlobalConstantsEdu.SEMESTER_OF_YEAR_FIRST && contractDetailListObj.SecondSemester == GlobalConstantsEdu.SEMESTER_OF_YEAR_SECOND))
                            {
                                smsParentContractSubscriberTime = smsParentContractBOListTmp.Where(p => p.SubscriptionTime == GlobalConstantsEdu.SEMESTER_OF_YEAR_ALL).FirstOrDefault();
                                if (smsParentContractSubscriberTime != null)
                                {
                                    contractDetailListObj.RegistrationTypeAllSemester = smsParentContractSubscriberTime.RegistrationType;
                                }
                                else
                                {
                                    smsParentContractSubscriberTime = smsParentContractBOListTmp.Where(p => p.SubscriptionTime == GlobalConstantsEdu.SEMESTER_OF_YEAR_FIRST).FirstOrDefault();
                                    if (smsParentContractSubscriberTime != null)
                                    {
                                        contractDetailListObj.RegistrationTypeFirstSemester = smsParentContractSubscriberTime.RegistrationType;
                                    }

                                    smsParentContractSubscriberTime = smsParentContractBOListTmp.Where(p => p.SubscriptionTime == GlobalConstantsEdu.SEMESTER_OF_YEAR_SECOND).FirstOrDefault();
                                    if (smsParentContractSubscriberTime != null)
                                    {
                                        contractDetailListObj.RegistrationTypeSecondSemester = smsParentContractSubscriberTime.RegistrationType;
                                    }
                                }
                            }
                            else if (contractDetailListObj.FirstSemester == GlobalConstantsEdu.SEMESTER_OF_YEAR_FIRST)
                            {
                                smsParentContractSubscriberTime = smsParentContractBOListTmp.Where(p => p.SubscriptionTime == GlobalConstantsEdu.SEMESTER_OF_YEAR_FIRST).FirstOrDefault();
                                if (smsParentContractSubscriberTime != null)
                                {
                                    contractDetailListObj.RegistrationTypeFirstSemester = smsParentContractSubscriberTime.RegistrationType;
                                }
                            }
                            else if (contractDetailListObj.SecondSemester == GlobalConstantsEdu.SEMESTER_OF_YEAR_SECOND)
                            {
                                smsParentContractSubscriberTime = smsParentContractBOListTmp.Where(p => p.SubscriptionTime == GlobalConstantsEdu.SEMESTER_OF_YEAR_SECOND).FirstOrDefault();
                                if (smsParentContractSubscriberTime != null)
                                {
                                    contractDetailListObj.RegistrationTypeSecondSemester = smsParentContractSubscriberTime.RegistrationType;
                                }
                            }
                            #endregion

                            contractDetailList.Add(contractDetailListObj);
                        }

                        for (int j = 0; j < listContractTmpExtra.Count; j++)
                        {
                            //them vao danh sach chi tiet hop dong
                            // smsParentContractID = smsParentContractBOList[j].SMSParentContractID;
                            contractObjTmp = listContractTmpExtra[j];
                            phoneNumber = contractObjTmp.PhoneNumberRecever;
                            servicePackageID = contractObjTmp.ServicePackageID;
                            updateTime = contractObjTmp.UpdateTime;
                            subscriptionStatus = contractObjTmp.SubscriptionStatus;

                            contractDetailListObj = new ContractDetailList();
                            smsParentDetailListTmp = smsParentContractBOListExtra.Where(p => p.PhoneNumberRecever == phoneNumber).ToList();
                            SMSParentContractBO objDetail = smsParentDetailListTmp.Where(p => p.ServicePackageID == servicePackageID && p.SubscriptionStatus == subscriptionStatus).FirstOrDefault();
                            contractDetailListObj.RelationShip = objDetail.RelationShip;
                            contractDetailListObj.PhoneReceiver = objDetail.PhoneNumberRecever;
                            contractDetailListObj.ServicesPakageID = objDetail.ServicePackageID;
                            contractDetailListObj.SubscriptionStatus = objDetail.SubscriptionStatus;
                            contractDetailListObj.SMSParentContractDetailID = objDetail.SMSParentContractDetailID;
                            contractDetailListObj.SMSParentContractID = objDetail.SMSParentContractID;
                            contractDetailListObj.CreateTime = objDetail.CreateTime;
                            contractDetailListObj.RegistrationType = smsParentDetailListTmp.Where(p => p.ServicePackageID == servicePackageID && p.SubscriptionStatus == subscriptionStatus).ToList().OrderByDescending(p => p.CreateTime).FirstOrDefault().RegistrationType;//hinh thuc dang ky SMS or truc tiep
                            //thoi gian hop dong KH1,KH2
                            subscriptionTime = new List<int?>();
                            if (updateTime != null) smsParentContractBOListTmp = smsParentDetailListTmp.Where(p => p.UpdateTime == updateTime && p.SubscriptionStatus == subscriptionStatus && p.ServicePackageID == objDetail.ServicePackageID).ToList();
                            else smsParentContractBOListTmp = smsParentDetailListTmp.Where(p => p.SubscriptionStatus == subscriptionStatus && p.ServicePackageID == objDetail.ServicePackageID && p.UpdateTime == null).ToList();

                            subscriptionTime = new List<int?> { contractObjTmp.SubscriptionTime };
                            if (subscriptionTime.Count == 1)
                            {
                                contractDetailListObj.SMSParentContractDetailID = smsParentDetailListTmp.FirstOrDefault(p => p.ServicePackageID == servicePackageID && p.SubscriptionStatus == subscriptionStatus && p.SubscriptionTime == subscriptionTime.FirstOrDefault()).SMSParentContractDetailID;
                            }
                            contractDetailListObj = this.GetSemester(subscriptionTime, contractDetailListObj);

                            #region Danh dau hinh thuc dang ky SMSParentContractDetail
                            //Neu co KH1 or HK2 dang ky SMS thi danh dau lai                            
                            if (contractDetailListObj.AllSemester == GlobalConstantsEdu.SEMESTER_OF_YEAR_ALL || (contractDetailListObj.FirstSemester == GlobalConstantsEdu.SEMESTER_OF_YEAR_FIRST && contractDetailListObj.SecondSemester == GlobalConstantsEdu.SEMESTER_OF_YEAR_SECOND))
                            {
                                smsParentContractSubscriberTime = smsParentContractBOListTmp.Where(p => p.SubscriptionTime == GlobalConstantsEdu.SEMESTER_OF_YEAR_ALL).FirstOrDefault();
                                if (smsParentContractSubscriberTime != null)
                                {
                                    contractDetailListObj.RegistrationTypeAllSemester = smsParentContractSubscriberTime.RegistrationType;
                                }
                                else
                                {
                                    smsParentContractSubscriberTime = smsParentContractBOListTmp.Where(p => p.SubscriptionTime == GlobalConstantsEdu.SEMESTER_OF_YEAR_FIRST).FirstOrDefault();
                                    if (smsParentContractSubscriberTime != null)
                                    {
                                        contractDetailListObj.RegistrationTypeFirstSemester = smsParentContractSubscriberTime.RegistrationType;
                                    }

                                    smsParentContractSubscriberTime = smsParentContractBOListTmp.Where(p => p.SubscriptionTime == GlobalConstantsEdu.SEMESTER_OF_YEAR_SECOND).FirstOrDefault();
                                    if (smsParentContractSubscriberTime != null)
                                    {
                                        contractDetailListObj.RegistrationTypeSecondSemester = smsParentContractSubscriberTime.RegistrationType;
                                    }
                                }
                            }
                            else if (contractDetailListObj.FirstSemester == GlobalConstantsEdu.SEMESTER_OF_YEAR_FIRST)
                            {
                                smsParentContractSubscriberTime = smsParentContractBOListTmp.Where(p => p.SubscriptionTime == GlobalConstantsEdu.SEMESTER_OF_YEAR_FIRST).FirstOrDefault();
                                if (smsParentContractSubscriberTime != null)
                                {
                                    contractDetailListObj.RegistrationTypeFirstSemester = smsParentContractSubscriberTime.RegistrationType;
                                }
                            }
                            else if (contractDetailListObj.SecondSemester == GlobalConstantsEdu.SEMESTER_OF_YEAR_SECOND)
                            {
                                smsParentContractSubscriberTime = smsParentContractBOListTmp.Where(p => p.SubscriptionTime == GlobalConstantsEdu.SEMESTER_OF_YEAR_SECOND).FirstOrDefault();
                                if (smsParentContractSubscriberTime != null)
                                {
                                    contractDetailListObj.RegistrationTypeSecondSemester = smsParentContractSubscriberTime.RegistrationType;
                                }
                            }
                            #endregion

                            contractDetailList.Add(contractDetailListObj);
                        }
                    }
                    smsParentContractGroupBO.contractDetailList = contractDetailList.OrderBy(p => p.CreateTime).ToList();
                    smsParentContractGroupList.Add(smsParentContractGroupBO);
                }
            }

            return smsParentContractGroupList;
        }
        /// <summary>
        /// Lay Danh sach dang ky hop dong (lay hoc sinh chua co thong tin hop dong)
        /// </summary>
        /// <param name="listPupil"></param>
        /// <param name="schoolID"></param>
        /// <param name="year"></param>
        /// <param name="status"></param>
        /// <returns></returns>
        public List<SMSParentContractGroupBO> GetListExcelRegis(List<PupilProfileBO> listPupil, int schoolID, int year, int status)
        {
            PupilProfileBO pupilProfileBO = null;
            List<SMSParentContractGroupBO> smsParentContractGroupList = new List<SMSParentContractGroupBO>();
            SMSParentContractGroupBO smsParentContractGroupBO = null;
            if (listPupil.Count > 0 && listPupil != null)
            {
                List<int> pupilIDList = listPupil.Select(p => p.PupilProfileID).ToList();
                //Danh sach ma hoc sinh da co hop dong
                List<int> pupilIDsRegistratedContract = (from ct in SMSParentContractBusiness.All
                                                         join ctd in SMSParentContractDetailBusiness.All on ct.SMS_PARENT_CONTRACT_ID equals ctd.SMS_PARENT_CONTRACT_ID
                                                         where pupilIDList.Contains(ct.PUPIL_ID)
                                                         && ct.SCHOOL_ID == schoolID && ct.PARTITION_ID == schoolID % GlobalConstantsEdu.PARTITION_SMSPARENTCONTRACT_SCHOOLID
                                                         && ctd.YEAR_ID == year
                                                         && ctd.IS_ACTIVE
                                                         && (ct.IS_DELETED == null || ct.IS_DELETED == false)
                                                         select ct.PUPIL_ID).ToList();

                pupilIDList = pupilIDList.Except(pupilIDsRegistratedContract).ToList();
                //Danh sach hoc sinh chua co hop dong
                listPupil = listPupil.Where(p => pupilIDList.Contains(p.PupilProfileID)).ToList();

                List<ContractDetailList> contractDetailList = null;
                ContractDetailList contractDetailListObj = null;

                int pupilID = 0;

                if (listPupil.Count > 0 && listPupil != null)
                {
                    for (int i = 0; i < listPupil.Count; i++)
                    {
                        smsParentContractGroupBO = new SMSParentContractGroupBO();
                        pupilProfileBO = listPupil[i];
                        //Neu hoc sinh chua co hop dong thi fill ra cac thong tin trong pupilprofile
                        contractDetailList = new List<ContractDetailList>();
                        pupilID = pupilProfileBO.PupilProfileID;
                        smsParentContractGroupBO.PupilID = pupilProfileBO.PupilProfileID;
                        smsParentContractGroupBO.PupilCode = pupilProfileBO.PupilCode;
                        smsParentContractGroupBO.PupilName = pupilProfileBO.FullName;
                        smsParentContractGroupBO.ClassID = pupilProfileBO.CurrentClassID;
                        smsParentContractGroupBO.ClassName = pupilProfileBO.ClassName;
                        smsParentContractGroupBO.FatherName = pupilProfileBO.FatherFullName;

                        contractDetailListObj = new ContractDetailList();
                        contractDetailListObj.RelationShip = "Mẹ";
                        contractDetailListObj.PhoneReceiver = pupilProfileBO.MotherMobile;
                        contractDetailListObj.ReceiverName = pupilProfileBO.MotherFullName;

                        contractDetailList.Add(contractDetailListObj);
                        smsParentContractGroupBO.contractDetailList = contractDetailList;
                        smsParentContractGroupList.Add(smsParentContractGroupBO);
                    }
                }
            }
            return smsParentContractGroupList;
        }

        public List<SMSParentContractGroupBO> GetListExcelExtra(List<PupilProfileBO> listPupil, int schoolID, int year, int status, string mobile)
        {
            List<SMSParentContractGroupBO> smsParentContractGroupList = new List<SMSParentContractGroupBO>();
            if (listPupil.Count > 0 && listPupil != null)
            {
                List<int> pupilIDList = listPupil.Select(p => p.PupilProfileID).ToList();
                //Danh sach ma hoc sinh da co hop dong
                List<SMSParentContractGroupBO> lstRegistratedContract = (from ct in SMSParentContractBusiness.All
                                                                         join ctd in SMSParentContractDetailBusiness.All on ct.SMS_PARENT_CONTRACT_ID equals ctd.SMS_PARENT_CONTRACT_ID
                                                                         join sp in ServicePackageBusiness.All on ctd.SERVICE_PACKAGE_ID equals sp.SERVICE_PACKAGE_ID
                                                                         join spd in ServicePackageDeclareBusiness.All on sp.SERVICE_PACKAGE_ID equals spd.SERVICE_PACKAGE_ID
                                                                         where pupilIDList.Contains(ct.PUPIL_ID)
                                                                         && ct.SCHOOL_ID == schoolID && ct.PARTITION_ID == schoolID % GlobalConstantsEdu.PARTITION_SMSPARENTCONTRACT_SCHOOLID
                                                                         && ctd.YEAR_ID == year
                                                                         && ctd.IS_ACTIVE
                                                                         && (ctd.SUBSCRIBER == mobile || string.IsNullOrEmpty(mobile))
                                                                         && (ct.IS_DELETED == null || ct.IS_DELETED == false)
                                                                         && ctd.SUBSCRIPTION_STATUS == GlobalConstantsEdu.STATUS_REGISTED
                                                                         && spd.YEAR == year
                                                                         && (spd.IS_LIMIT == null || spd.IS_LIMIT == false)
                                                                         select new SMSParentContractGroupBO
                                                                         {
                                                                             PupilID = ct.PUPIL_ID,
                                                                             RelationshipContract = ctd.RELATION_SHIP,
                                                                             ReceiverName = ctd.RECEIVER_NAME,
                                                                             Subscriber = ctd.SUBSCRIBER,
                                                                             SubciptionTime = ctd.SUBSCRIPTION_TIME
                                                                         }).ToList();

                lstRegistratedContract = (from ct in lstRegistratedContract
                                          join pp in listPupil on ct.PupilID equals pp.PupilProfileID
                                          orderby pp.EducationLevelID, pp.ClassOrderNumber, pp.OrderInClass, pp.Name, pp.FullName
                                          select new SMSParentContractGroupBO
                                          {
                                              PupilID = ct.PupilID,
                                              RelationshipContract = ct.RelationshipContract,
                                              ReceiverName = ct.ReceiverName,
                                              Subscriber = ct.Subscriber,
                                              PupilCode = pp.PupilCode,
                                              PupilName = pp.FullName,
                                              ClassName = pp.ClassName,
                                              SubciptionTime = ct.SubciptionTime
                                          }).ToList();
                return lstRegistratedContract;
            }
            else
            {
                return new List<SMSParentContractGroupBO>();
            }
        }

        public List<SMSParentContractDetailBO> GetListSMSParentContractByShoolID(int schoolID, int year)
        {
            List<SMSParentContractDetailBO> listResult = (from ct in SMSParentContractBusiness.All
                                                          join ctd in SMSParentContractDetailBusiness.All on ct.SMS_PARENT_CONTRACT_ID equals ctd.SMS_PARENT_CONTRACT_ID
                                                          join spa in ServicePackageBusiness.All on ctd.SERVICE_PACKAGE_ID equals spa.SERVICE_PACKAGE_ID
                                                          join spd in ServicePackageDeclareBusiness.All on spa.SERVICE_PACKAGE_ID equals spd.SERVICE_PACKAGE_ID
                                                          where ct.SCHOOL_ID == schoolID && ct.PARTITION_ID == schoolID % GlobalConstantsEdu.PARTITION_SMSPARENTCONTRACT_SCHOOLID
                                                          && ctd.YEAR_ID == year
                                                          && ct.STATUS == GlobalConstantsEdu.STATUS_CONTRACT_APPROVAL
                                                          && ctd.IS_ACTIVE
                                                          && (ct.IS_DELETED == null || ct.IS_DELETED == false)
                                                          && spd.YEAR == year
                                                          select new SMSParentContractDetailBO
                                                          {
                                                              PupilID = ct.PUPIL_ID,
                                                              SMSParentContractID = ctd.SMS_PARENT_CONTRACT_ID,
                                                              SMSParentContractDetailID = ctd.SMS_PARENT_CONTRACT_DETAIL_ID,
                                                              Subscriber = ctd.SUBSCRIBER,
                                                              SubscriptionTime = ctd.SUBSCRIPTION_TIME,
                                                              ServicePackageID = ctd.SERVICE_PACKAGE_ID,
                                                              SubscriptionStatus = ctd.SUBSCRIPTION_STATUS,
                                                              ServiceCode = spa.SERVICE_CODE,
                                                              ViettelPrice = spa.VIETTEL_PRICE,
                                                              OtherPrice = spa.OTHER_PRICE,
                                                              IsLimit = spd.IS_LIMIT,
                                                              ClassID = ct.CLASS_ID
                                                          }).ToList();
            return listResult;
        }

        /// <summary>
        /// luu thong tin dang ky hop dong
        /// </summary>
        /// <param name="smsParentContractList">danh sach luu</param>
        public Dictionary<string, object> SaveRegisContract(List<SMSParentContractBO> smsParentContractList, int year)
        {
            Dictionary<string, object> dic = new Dictionary<string, object>();

            short? registrationType = GlobalConstantsEdu.REGISTRATION_TYPE_BY_SCHOOL;
            if (smsParentContractList != null && smsParentContractList.Count > 0)
            {
                SMSParentContractBO smsParentContractBOInsert = new SMSParentContractBO();
                SMSParentContractDetailBO smsParentContractDetailBOInsert = new SMSParentContractDetailBO();
                SMS_PARENT_CONTRACT smsParentContract = null;

                List<ServicePackageBO> LstServicePackage = ServicePackageDetailBusiness.GetListMaxSMSSP(year);
                ServicePackageBO objSP = null;
                List<ContractDetailInClassBO> lstContractInClass = new List<ContractDetailInClassBO>();
                ContractDetailInClassBO objDetailInClass = null;

                List<SParentAccountBO> lstSParentAccount = new List<SParentAccountBO>();
                SParentAccountBO objSParentAccount = null;

                List<SMS_PARENT_CONTRACT> lstContractInsert = new List<SMS_PARENT_CONTRACT>();
                List<SMS_PARENT_CONTRACT_DETAIL> lstContractDetailInsert = new List<SMS_PARENT_CONTRACT_DETAIL>();
                List<Guid> lstGuid = new List<Guid>();
                int schoolID = smsParentContractList[0].SchoolID;
                #region Them hop dong
                for (int i = 0; i < smsParentContractList.Count; i++)
                {
                    smsParentContract = new SMS_PARENT_CONTRACT();
                    smsParentContractBOInsert = smsParentContractList[i];
                    smsParentContract.SCHOOL_ID = smsParentContractBOInsert.SchoolID;
                    smsParentContract.CLASS_ID = smsParentContractBOInsert.ClassID;
                    smsParentContract.ACADEMIC_YEAR_ID = smsParentContractBOInsert.AcademicYearID;
                    smsParentContract.MOBILE = smsParentContractBOInsert.PhoneMainContract;
                    smsParentContract.CONTRACTOR_NAME = smsParentContractBOInsert.ContractName;
                    smsParentContract.CONTRACTOR_RELATION_SHIP = smsParentContractBOInsert.RelationShip;
                    smsParentContract.PUPIL_ID = smsParentContractBOInsert.PupilFileID;
                    smsParentContract.STATUS = smsParentContractBOInsert.StatusContract;
                    smsParentContract.CREATED_TIME = smsParentContractBOInsert.CreateTime;
                    smsParentContract.PARTITION_ID = smsParentContractBOInsert.SchoolID % GlobalConstantsEdu.PARTITION_SMSPARENTCONTRACT_SCHOOLID;
                    Guid guid = Guid.NewGuid();
                    smsParentContract.SMS_PARENT_CONTRACT_RAW_ID = guid;
                    lstContractInsert.Add(smsParentContract);

                    if (smsParentContractBOInsert.SMSParentContractDetailList != null && smsParentContractBOInsert.SMSParentContractDetailList.Count > 0)
                    {
                        SMS_PARENT_CONTRACT_DETAIL smsParentContractDetail = null;
                        for (int j = 0; j < smsParentContractBOInsert.SMSParentContractDetailList.Count; j++)
                        {
                            smsParentContractDetail = new SMS_PARENT_CONTRACT_DETAIL();
                            smsParentContractDetailBOInsert = smsParentContractBOInsert.SMSParentContractDetailList[j];
                            //
                            smsParentContractDetail.SUBSCRIBER = smsParentContractDetailBOInsert.Subscriber.ExtensionMobile();
                            smsParentContractDetail.SUBSCRIBER_TYPE = smsParentContractDetail.SUBSCRIBER.CheckMobileNumberVT() ? 0 : 1;
                            smsParentContractDetail.SUBSCRIPTION_TIME = smsParentContractDetailBOInsert.SubscriptionTime;
                            smsParentContractDetail.SUBSCRIPTION_STATUS = smsParentContractDetailBOInsert.SubscriptionStatus;
                            smsParentContractDetail.PARTITION_ID = UtilsBusiness.GetPartionId(smsParentContractBOInsert.SchoolID);
                            smsParentContractDetail.YEAR_ID = smsParentContractDetailBOInsert.Year;
                            smsParentContractDetail.AMOUNT = (int)smsParentContractDetailBOInsert.Money;
                            smsParentContractDetail.IS_ACTIVE = smsParentContractDetailBOInsert.IsActive;
                            smsParentContractDetail.SERVICE_PACKAGE_ID = smsParentContractDetailBOInsert.ServicePackageID.GetValueOrDefault();
                            smsParentContractDetail.CREATED_TIME = smsParentContractDetailBOInsert.CreatedTime;
                            smsParentContractDetail.RELATION_SHIP = smsParentContractDetailBOInsert.Relationship;
                            smsParentContractDetail.RECEIVER_NAME = smsParentContractDetailBOInsert.ReceiverName;
                            smsParentContractDetail.REGISTRATION_TYPE = registrationType;
                            smsParentContractDetail.CLASS_ID = smsParentContractBOInsert.ClassID;
                            smsParentContractDetail.PUPIL_ID = smsParentContractBOInsert.PupilFileID;
                            smsParentContractDetail.SCHOOL_ID = smsParentContractBOInsert.SchoolID;
                            smsParentContractDetail.ACADEMIC_YEAR_ID = smsParentContractBOInsert.AcademicYearID;
                            lstContractDetailInsert.Add(smsParentContractDetail);
                            lstGuid.Add(guid);
                            #region Add data for update SMSparentContractDetailInClass
                            // Không cộng tin nhắn với Hợp đồng Nợ cước
                            if (smsParentContractDetail.SUBSCRIPTION_STATUS == GlobalConstantsEdu.STATUS_REGISTED)
                            {
                                objDetailInClass = new ContractDetailInClassBO();
                                objDetailInClass.SchoolID = smsParentContractBOInsert.SchoolID;
                                objDetailInClass.PupilID = smsParentContractBOInsert.PupilFileID;
                                objDetailInClass.ClassID = smsParentContractBOInsert.ClassID;
                                objDetailInClass.AcademicYearID = smsParentContractBOInsert.AcademicYearID;
                                objDetailInClass.ServicePackageID = smsParentContractDetailBOInsert.ServicePackageID;
                                objDetailInClass.Semester = smsParentContractDetailBOInsert.SubscriptionTime;
                                objSP = LstServicePackage.FirstOrDefault(o => o.ServicePackageID == smsParentContractDetailBOInsert.ServicePackageID);
                                if (objSP != null && objSP.IsLimit != true && objSP.MaxSMS.HasValue)
                                {
                                    objDetailInClass.MaxSMS = objSP.MaxSMS.HasValue ? smsParentContractDetailBOInsert.SubscriptionTime == 3 ? objSP.MaxSMS.Value * 2 : objSP.MaxSMS : 0;
                                }
                                lstContractInClass.Add(objDetailInClass);
                            }
                            #endregion

                            #region Send register account information SParent SMS to parent
                            if (!lstSParentAccount.Exists(o => o.PupilID == smsParentContractBOInsert.PupilFileID && o.UserName.Equals(smsParentContractDetailBOInsert.Subscriber)))
                            {
                                objSParentAccount = new SParentAccountBO();
                                objSParentAccount.PupilID = smsParentContractBOInsert.PupilFileID;
                                objSParentAccount.FullName = smsParentContractBOInsert.FullName;
                                objSParentAccount.UserName = smsParentContractDetailBOInsert.Subscriber.ExtensionMobile();
                                objSParentAccount.IsNew = true;
                                lstSParentAccount.Add(objSParentAccount);
                            }
                            #endregion
                        }
                    }
                    lstContractInsert.Add(smsParentContract);
                    SMSParentContractBusiness.Insert(smsParentContract);
                }

                SMSParentContractBusiness.Save();

                for (int i = 0; i < lstContractDetailInsert.Count; i++)
                {
                    SMS_PARENT_CONTRACT_DETAIL detail = lstContractDetailInsert[i];
                    Guid g = lstGuid[i];
                    SMS_PARENT_CONTRACT contract = lstContractInsert.FirstOrDefault(o => o.SMS_PARENT_CONTRACT_RAW_ID == g);
                    detail.SMS_PARENT_CONTRACT_ID = contract.SMS_PARENT_CONTRACT_ID;
                    SMSParentContractDetailBusiness.Insert(detail);

                    detail.PARTITION_ID = UtilsBusiness.GetPartionId(schoolID);
                }
                SMSParentContractDetailBusiness.Save();
                #endregion

                // Update message budget in class
                SMSParentContractInClassDetailBusiness.UpdateMessageBudgetInClass(lstContractInClass, year);
                //// Send SParent account to parent
                //this.CreateListSParentAccount(lstSParentAccount);
                // Tra ve list Sparent khi dang ky hop dong
                dic[GlobalConstantsEdu.LIST_SPARENT] = lstSParentAccount;
            }

            return dic;

        }
        #endregion

        #region Ultils
        /// <summary>
        /// Lấy thông tin Học kỳ - Hiển thị ở chức năng QL Hợp đồng
        /// </summary>
        /// <param name="subscriptionTime"></param>
        /// <param name="contractDetailListObj"></param>
        /// <returns></returns>
        private ContractDetailList GetSemester(List<int?> subscriptionTime, ContractDetailList contractDetailListObj)
        {
            int? semseter1 = subscriptionTime.Where(p => p == GlobalConstantsEdu.SEMESTER_OF_YEAR_FIRST).Select(p => p).FirstOrDefault();
            int? semseter2 = subscriptionTime.Where(p => p == GlobalConstantsEdu.SEMESTER_OF_YEAR_SECOND).Select(p => p).FirstOrDefault();
            int? semseter3 = subscriptionTime.Where(p => p == GlobalConstantsEdu.SEMESTER_OF_YEAR_ALL).Select(p => p).FirstOrDefault();

            if (semseter3 == GlobalConstantsEdu.SEMESTER_OF_YEAR_ALL)
            {
                contractDetailListObj.AllSemester = GlobalConstantsEdu.SEMESTER_OF_YEAR_ALL;
            }
            if (semseter1 == GlobalConstantsEdu.SEMESTER_OF_YEAR_FIRST)
            {
                contractDetailListObj.FirstSemester = subscriptionTime.Where(p => p == GlobalConstantsEdu.SEMESTER_OF_YEAR_FIRST).Select(p => p).FirstOrDefault();
            }
            if (semseter2 == GlobalConstantsEdu.SEMESTER_OF_YEAR_SECOND)
            {
                contractDetailListObj.SecondSemester = subscriptionTime.Where(p => p == GlobalConstantsEdu.SEMESTER_OF_YEAR_SECOND).Select(p => p).FirstOrDefault();
            }
            return contractDetailListObj;
        }

        /// <summary>
        /// Kiểm tra năm đang thuộc thời gian hiện tại
        /// </summary>
        /// <param name="Year"></param>
        /// <returns></returns>
        private int CheckAcademicYear(int Year)
        {
            DateTime Date = DateTime.Now.Date;
            //////////////
            //Lay cac thong tin nam hoc cau hinh
            var startHK1 = ConfigEdu.GetString("StartSemester1") + Year.ToString();
            var endHK1 = ConfigEdu.GetString("EndSemester1") + Year.ToString();
            var startHK2 = ConfigEdu.GetString("StartSemester2") + (Year + 1).ToString();
            var endHK2 = ConfigEdu.GetString("EndSemester2") + (Year + 1).ToString();
            DateTime dtStartHK1 = DateTime.ParseExact(startHK1, "dd/MM/yyyy", CultureInfo.InvariantCulture);
            DateTime dtEndHK1 = DateTime.ParseExact(endHK1, "dd/MM/yyyy", CultureInfo.InvariantCulture);
            if (dtEndHK1 < dtStartHK1)
            {
                dtEndHK1 = dtEndHK1.AddYears(1);
            }
            DateTime dtStartHK2 = DateTime.ParseExact(startHK2, "dd/MM/yyyy", CultureInfo.InvariantCulture);
            DateTime dtEndHK2 = DateTime.ParseExact(endHK2, "dd/MM/yyyy", CultureInfo.InvariantCulture);
            if (dtEndHK2 < dtStartHK2)
            {
                dtStartHK2 = dtStartHK2.AddYears(-1);
            }
            //Truong hop khong nam trong nam hoc
            if (Date < dtStartHK1 || Date > dtEndHK2)
            {
                return 0;
            }
            //Truong hop nam trong HK1
            else if (Date >= dtStartHK1 && Date <= dtEndHK1)
            {
                return 1;
            }
            //Truong hop nam trong HK2
            else if (Date >= dtStartHK2 && Date <= dtEndHK2)
            {
                return 2;
            }
            else
            {
                return 3;
            }
        }

        /// <summary>
        /// TamHM1 - check duplicate phone
        /// </summary>
        /// <param name="Phone1"></param>
        /// <param name="Phone2"></param>
        /// <returns></returns>
        private bool CheckDuplicateMobile(string Phone1, string Phone2)
        {
            string PhoneA = Phone1.SubstringPhone();
            string PhoneB = Phone2.SubstringPhone();
            if (PhoneA != null && PhoneB != null && PhoneA.Trim().Equals(PhoneB.Trim()))
            {
                return false;
            }
            else
                return true;
        }
        #endregion

        #region Kiem tra Hinh thuc thue bao SLLDT cua truong
        /// <summary>
        /// Kiem tra ton tai Hop dong (chua huy) voi hinh thuc thanh toan cho truoc trong nam hoc hien tai hay chua?
        /// </summary>
        /// <param name="PaymentType"></param>
        /// <param name="SchoolID"></param>
        /// <param name="Year"></param>
        /// <returns></returns>
        public bool ExistPaymentTypeContract(int PaymentType, int SchoolID, int Year)
        {
            return (from ct in SMSParentContractBusiness.All
                    join ctd in SMSParentContractDetailBusiness.All on ct.SMS_PARENT_CONTRACT_ID equals ctd.SMS_PARENT_CONTRACT_ID
                    where ct.SCHOOL_ID == SchoolID && ct.PARTITION_ID == SchoolID % GlobalConstantsEdu.PARTITION_SMSPARENTCONTRACT_SCHOOLID
                    && ct.STATUS == GlobalConstantsEdu.STATUS_CONTRACT_APPROVAL
                    && ctd.PAYMENT_TYPE == PaymentType && ctd.YEAR_ID == Year
                    && ctd.IS_ACTIVE == true
                    && ctd.SUBSCRIPTION_STATUS != GlobalConstantsEdu.STATUS_DELETE_CONTRACT_DETAIL
                    select ct).Count() > 0;
        }

        /// <summary>
        /// lay Hinh thuc thanh toan Hop dong hien tai cua truong
        /// </summary>
        /// <param name="SchoolID"></param>
        /// <param name="Year"></param>
        /// <returns></returns>
        public int getCurrentPaymentTypeOfSchool(int SchoolID, int Year)
        {
            var obj = (from ct in SMSParentContractBusiness.All
                       join ctd in SMSParentContractDetailBusiness.All on ct.SMS_PARENT_CONTRACT_ID equals ctd.SMS_PARENT_CONTRACT_ID
                       where ct.SCHOOL_ID == SchoolID && ct.PARTITION_ID == SchoolID % GlobalConstantsEdu.PARTITION_SMSPARENTCONTRACT_SCHOOLID
                       && ct.STATUS == GlobalConstantsEdu.STATUS_CONTRACT_APPROVAL
                       && ctd.IS_ACTIVE == true && ctd.YEAR_ID == Year
                       && ctd.SUBSCRIPTION_STATUS != GlobalConstantsEdu.STATUS_DELETE_CONTRACT_DETAIL
                       select ctd).FirstOrDefault();
            if (obj != null && obj.PAYMENT_TYPE > 0) return obj.PAYMENT_TYPE;
            else return 0;
        }
        #endregion

        #region SLLĐT - Cho phép trả chậm
        /// <summary>
        /// Kiem tra cap nhat cac truong hop No cuoc qua han
        /// </summary>
        /// <param name="SchoolID"></param>
        /// <param name="Year"></param>
        private void checkOverdueUnpaid(int schoolID, int year)
        {
            List<string> LstPhone = new List<string>();
            int numDeferredPaymentDays = ConfigEdu.GetInt(GlobalConstantsEdu.DEFERRED_PAYMENT_DAYS_KEY);

            var lstUnpaidContract = from ct in SMSParentContractBusiness.All
                                    join ctd in SMSParentContractDetailBusiness.All on ct.SMS_PARENT_CONTRACT_ID equals ctd.SMS_PARENT_CONTRACT_ID
                                    where ct.SCHOOL_ID == schoolID && ct.PARTITION_ID == schoolID % GlobalConstantsEdu.PARTITION_SMSPARENTCONTRACT_SCHOOLID
                                    && ctd.YEAR_ID == year
                                    && ctd.SUBSCRIPTION_STATUS == GlobalConstantsEdu.STATUS_UNPAID
                                    && ctd.IS_ACTIVE
                                    && (ct.IS_DELETED == null || ct.IS_DELETED == false)
                                    && EntityFunctions.DiffDays(ctd.CREATED_TIME, DateTime.Now) > numDeferredPaymentDays
                                    select ctd;
            foreach (var item in lstUnpaidContract)
            {
                LstPhone.Add(item.SUBSCRIBER);
                item.SUBSCRIPTION_STATUS = GlobalConstantsEdu.STATUS_OVERDUE_UNPAID;
                SMSParentContractDetailBusiness.Update(item);
            }

            SMSParentContractDetailBusiness.Save();

            // Lock list account sparent
            //UserAccountBusiness.UpdateList(LstPhone, true);
        }

        /// <summary>
        /// Cap nhat lai cac thue bao No cuoc sau khi thanh toan
        /// </summary>
        /// <param name="lstParentContract"></param>
        public void UpdateDeferredContract(List<SMSParentContractBO> lstParentContract, int year)
        {
            if (lstParentContract != null && lstParentContract.Count > 0)
            {
                SMSParentContractBO contract = null;
                SMSParentContractDetailBO contractDetail = null;

                List<ServicePackageBO> LstServicePackage = ServicePackageDetailBusiness.GetListMaxSMSSP(year);
                ServicePackageBO objSP = null;
                List<ContractDetailInClassBO> lstContractInClass = new List<ContractDetailInClassBO>();
                ContractDetailInClassBO objDetailInClass = null;
                DateTime now = DateTime.Now;
                List<long> LstContractDetailIDUpdate = new List<long>();

                for (int i = 0; i < lstParentContract.Count; i++)
                {
                    contract = lstParentContract[i];
                    if (contract.SMSParentContractDetailList != null && contract.SMSParentContractDetailList.Count > 0)
                    {
                        for (int j = 0; j < contract.SMSParentContractDetailList.Count; j++)
                        {
                            contractDetail = contract.SMSParentContractDetailList[j];

                            #region Add data for update SMSparentContractDetailInClass
                            if (contractDetail.SubscriptionStatus == GlobalConstantsEdu.STATUS_REGISTED)
                            {
                                objDetailInClass = new ContractDetailInClassBO();
                                objDetailInClass.SchoolID = contract.SchoolID;
                                objDetailInClass.PupilID = contract.PupilFileID;
                                objDetailInClass.ClassID = contract.ClassID;
                                objDetailInClass.AcademicYearID = contract.AcademicYearID;
                                objDetailInClass.ServicePackageID = contractDetail.ServicePackageID;
                                objDetailInClass.Semester = contractDetail.SubscriptionTime;
                                objDetailInClass.SMSParentContractDetailID = contractDetail.SMSParentContractDetailID;
                                objSP = LstServicePackage.FirstOrDefault(o => o.ServicePackageID == contractDetail.ServicePackageID);
                                if (objSP != null && objSP.IsLimit != true && objSP.MaxSMS.HasValue)
                                {
                                    objDetailInClass.MaxSMS = objSP.MaxSMS.HasValue ? contractDetail.SubscriptionTime == 3 ? objSP.MaxSMS.Value * 2 : objSP.MaxSMS : 0;
                                }
                                lstContractInClass.Add(objDetailInClass);
                            }
                            #endregion

                            LstContractDetailIDUpdate.Add(contractDetail.SMSParentContractDetailID);
                        }
                    }
                }

                var LstDetail = SMSParentContractDetailBusiness.All.Where(o => LstContractDetailIDUpdate.Contains(o.SMS_PARENT_CONTRACT_DETAIL_ID));
                List<string> LstPhone = LstDetail.Select(o => o.SUBSCRIBER).ToList();
                foreach (var item in LstDetail)
                {
                    item.SUBSCRIPTION_STATUS = GlobalConstantsEdu.STATUS_REGISTED;
                    
                    //viethd31: Bỏ vì bị lỗi sau khi thanh toán đăng ký học kỳ khác cùng số đt thì update time khác nhau không group thành 1 được
                    //item.UPDATED_TIME = now;
                    item.PAYMENT_TIME = now;
                    SMSParentContractDetailBusiness.Update(item);
                }
                SMSParentContractDetailBusiness.Save();
                // Update message budget in class
                SMSParentContractInClassDetailBusiness.UpdateMessageBudgetInClass(lstContractInClass, year);
                // Unlock list account sparent
                //UserAccountBusiness.UpdateList(LstPhone, false);
            }
        }

        /// <summary>
        /// kiem tra hoc sinh chua co thue bao tra cham
        /// </summary>
        /// <param name="lstParentContract"></param>
        /// <returns>true: Neu co da; false: Neu chua co</returns>
        public bool checkExistUnpaidSubscriber(List<SMSParentContractBO> lstParentContract)
        {
            if (lstParentContract.Count > 0)
            {
                SMSParentContractBO contractBO = lstParentContract.FirstOrDefault();
                int SchoolID = contractBO.SchoolID;
                int AcademicYearID = contractBO.AcademicYearID;
                AcademicYear objAy = AcademicYearBusiness.Find(AcademicYearID);
                List<int> LstPupilID = lstParentContract.Select(o => o.PupilFileID).Distinct().ToList();
                return (from ct in SMSParentContractBusiness.All
                        join ctd in SMSParentContractDetailBusiness.All on ct.SMS_PARENT_CONTRACT_ID equals ctd.SMS_PARENT_CONTRACT_ID
                        where ct.SCHOOL_ID == SchoolID && ct.PARTITION_ID == SchoolID % GlobalConstantsEdu.PARTITION_SMSPARENTCONTRACT_SCHOOLID
                        && ctd.YEAR_ID == objAy.Year
                        && LstPupilID.Contains(ct.PUPIL_ID)
                        && (ctd.SUBSCRIPTION_STATUS == GlobalConstantsEdu.STATUS_UNPAID || ctd.SUBSCRIPTION_STATUS == GlobalConstantsEdu.STATUS_OVERDUE_UNPAID)
                        && ctd.IS_ACTIVE
                        && (ct.IS_DELETED == null || ct.IS_DELETED == false)
                        select ctd).Count() > 0;
            }

            return false;
        }
        #endregion

        #region private class
        private class PupilContract
        {
            public int PupilFileID { get; set; }

            public string Mobile { get; set; }

            public int TotalSMS { get; set; }

            public int SentSMS { get; set; }

            public long SMSParentContactDetailID { get; set; }

            public long SMSParentSentDetailID { get; set; }

            public bool? IsSent { get; set; }

            public int ServicePackageID { get; set; }

            public int Status { get; set; }

            public DateTime RegisterDate { get; set; }

            public bool? IsLimitPackage { get; set; }

            public int? EffectiveDay { get; set; }
            public int SemesterID { get; set; }
        }
        private class PupilContractDetail
        {
            public int PupilFileID { get; set; }
            public string Mobile { get; set; }
            public int MaxSMS { get; set; }
            public int SentSMS { get; set; }
            public long SMSParentContactDetailID { get; set; }
            public long SMSParentSentDetailID { get; set; }

            //public string PackageCode { get; set; }

            public DateTime RegisterDate { get; set; }
            public int ServicePackageID { get; set; }
            public bool? IsLimitPackage { get; set; }
            public int? EffectiveDay { get; set; }
            public int Status { get; set; }
            public int SemesterID { get; set; }
        }
        private class PupilContent
        {
            public int PupilFileID { get; set; }

            public string Content { get; set; }

            public string ShortContent { get; set; }

            public int SMS_CNT { get; set; }
        }
        private class PupilSubscriber
        {
            public string Subscriber { get; set; }

            public int SMSParentContractDetailID { get; set; }
        }
        private class PupilRegister
        {
            public string Subscriber { get; set; }

            public int PupilProfileID { get; set; }
        }
        #endregion

        #region tao account Sparent
        public void CreateListSParentAccount(List<SParentAccountBO> lstSParent)
        {
            if (lstSParent != null && lstSParent.Count > 0)
            {
                List<string> LstUserName = lstSParent.Select(o => o.UserName).ToList();
                var response = this.CreateList(LstUserName);
                if (response == null || response.Count == 0) return;

                SParentAccountBO objSParent = null;
                string msisdn = string.Empty;

                for (int i = lstSParent.Count - 1; i >= 0; i--)
                {
                    objSParent = lstSParent[i];
                    var objResponse = response.Where(p => p.UserName.Equals(objSParent.UserName)).FirstOrDefault();
                    objSParent.Password = objResponse.Password;
                    if (objSParent.IsNew.HasValue && objSParent.IsNew.Value)
                    {
                        bool isNewAccount = objResponse.IsExist ? false : true;
                        SendSMSToParent(ACTION_TYPE_SEND_TO_PARENT_ADD, objSParent.FullName, objSParent, isNewAccount);
                    }
                    if (objSParent.IsRenew.HasValue && objSParent.IsRenew.Value)
                    {
                        SendSMSToParent(ACTION_TYPE_SEND_TO_PARENT_RENEWABLE, objSParent.FullName, objSParent, false);
                    }

                }
            }
        }

        public List<SParentAccountResponse> CreateList(List<string> lstUserName)
        {
            List<SParentAccountResponse> lstObjRes = new List<SParentAccountResponse>();
            if (lstUserName != null && lstUserName.Count > 0)
            {
                for (int i = lstUserName.Count - 1; i >= 0; i--)
                {
                    string userName = lstUserName[i];
                    SParentAccountResponse objRes = new SParentAccountResponse() { UserName = userName };
                    if (!string.IsNullOrEmpty(userName))
                    {
                        APP_USERS memberShipUser = this.context.AppUsers.FirstOrDefault(o => o.USERNAME == userName);
                        //memberShipUser = Membership.GetUser(userName);
                        string newPassword = this.RandomText();
                        if (memberShipUser == null)
                        {
                            memberShipUser = new APP_USERS();
                            memberShipUser.USER_ID = Guid.NewGuid();
                            memberShipUser.USERNAME = userName;
                            memberShipUser.PASSWORD = BCrypt.Net.BCrypt.HashPassword(newPassword);
                            memberShipUser.IS_APPROVED = true;
                            memberShipUser.CREATE_DATE = DateTime.Now.Date;
                            memberShipUser.APPLICATION_ID = 3;

                            this.context.AppUsers.Add(memberShipUser);
                            objRes.Password = newPassword;

                        }
                        else
                        {
                            memberShipUser.IS_APPROVED = true;
                            if (memberShipUser.IS_LOCKED_OUT)
                            {
                                memberShipUser.IS_LOCKED_OUT = false;
                            }
                            this.context.Entry<APP_USERS>(memberShipUser).State = EntityState.Modified;
                            objRes.IsExist = true;
                            //objRes.Password = newPassword;
                        }
                    }
                    lstObjRes.Add(objRes);
                }

                this.context.SaveChanges();
            }
            
            return lstObjRes;
        }

        /// <summary>
        /// Tao tin nhan gui cho phhs khi dau noi hop dong
        /// </summary>
        /// <param name="type">1: them moi; 2: xoa tai khoan; 3: disabled; 4: gia han; 5: huy chan cat</param>
        /// <param name="islink">truong hop lien ket</param>
        /// <returns></returns>
        private void SendSMSToParent(int type, string pupilName, SParentAccountBO sparentAccount, bool islink)
        {
            string parentUrl = WebConfigurationManager.AppSettings["SParentURL"];
            string content = string.Empty;
            string msgPrice = string.Empty;
            if (ExtensionMethods.CheckViettelNumber(sparentAccount.UserName))
            {
                msgPrice = GlobalConstantsEdu.PRICE_190018098_VT;
            }
            else
            {
                msgPrice = GlobalConstantsEdu.PRICE_190018098_OTHER;
            }

            switch (type)
            {
                case ACTION_TYPE_SEND_TO_PARENT_ADD:
                    {
                        // them moi
                        if (islink)//them 1 account moi
                        {
                            content = string.Format(SMS_NOTICE_SPARENT_ACCOUNT_ADD, pupilName, parentUrl, sparentAccount.UserName, sparentAccount.Password);
                        }
                        else//update account
                        {
                            content = string.Format(SMS_NOTICE_SPARENT_ACCOUNT_ADD_NEW, pupilName, parentUrl, sparentAccount.UserName, sparentAccount.Password);
                        }
                        break;
                    }
                case ACTION_TYPE_SEND_TO_PARENT_DELETE:
                    {
                        if (islink)
                        {
                            // cat lien ket
                            content = string.Format(SMS_NOTICE_SPARENT_ACCOUNT_DISABLE, pupilName);
                        }
                        else
                        {
                            // xoa account
                            content = string.Format(SMS_NOTICE_SPARENT_ACCOUNT_DELETE, pupilName);
                        }
                        break;
                    }
                case ACTION_TYPE_SEND_TO_PARENT_DISABLE:
                    {
                        // chan cat
                        content = string.Format(SMS_NOTICE_SPARENT_ACCOUNT_DISABLE, pupilName);
                        break;
                    }
                case ACTION_TYPE_SEND_TO_PARENT_RENEWABLE:
                    {
                        // gia han
                        content = string.Format(SMS_NOTICE_PASSWORD_RENEW, pupilName, parentUrl, sparentAccount.UserName);
                        break;
                    }
                case ACTION_TYPE_SEND_TO_PARENT_ENABLED:
                    {
                        if (islink)
                        {
                            // lien ket hoc sinh moi
                            content = string.Format(SMS_NOTICE_SPARENT_ACCOUNT_ADD, pupilName, parentUrl, sparentAccount.UserName, sparentAccount.Password);
                        }
                        else
                        {
                            // huy chan cat
                            content = string.Format(SMS_NOTICE_SPARENT_ACCOUNT_ENABLED, pupilName, parentUrl, sparentAccount.UserName);
                        }
                        break;
                    }
                default:
                    break;
            }

            if (!string.IsNullOrEmpty(content))
            {
                Dictionary<string, object> dic = new Dictionary<string, object>();
                dic["Content"] = content;
                dic["Mobile"] = sparentAccount.UserName;
                dic["TypeID"] = GlobalConstantsEdu.COMMON_HISTORYSMS_TO_REGISTER_ACCOUNT_SPARENT;
                MTBusiness.SendSMS(dic);
            }
        }

        private string RandomText()
        {
            Random random = new Random();
            char[] chars = "abcdefghijklmnpqrstuvwxyzABCDEFGHIJKLMNPQRSTUVWXYZ".ToCharArray();
            char[] specChars = "$%#@&".ToCharArray();
            int i = random.Next(chars.Length);
            int j = random.Next(specChars.Length);
            return string.Format("{0}", DateTime.Now.ToString("ssffff")) + chars[i] + specChars[j];
        }
        #endregion

        #region Cap nhat ClassID khi chuyen lop
        public void UpdateClassIDToSMSParentContract(int NewClassID, int OldClassID, int SemesterID, int AcademicYearID, int SchoolID, int PupilID)
        {
            List<int> lstClassID = new List<int>();
            lstClassID.Add(NewClassID);
            lstClassID.Add(OldClassID);
            List<SMS_PARENT_CONTRACT_CLASS> lstPCC = (from pcc in SMSParentContractInClassDetailBusiness.All
                                                      where pcc.ACADEMIC_YEAR_ID == AcademicYearID
                                                      && pcc.SCHOOL_ID == SchoolID
                                                      && (pcc.SEMESTER == SemesterID || pcc.SEMESTER == GlobalConstants.SEMESTER_OF_YEAR_ALL)
                                                      && lstClassID.Contains(pcc.CLASS_ID)
                                                      select pcc).ToList();
            SMSParentContractInClassDetailBusiness.DeleteAll(lstPCC);
            SMS_PARENT_CONTRACT objSPC = SMSParentContractBusiness.All.Where(p => p.ACADEMIC_YEAR_ID == AcademicYearID && p.SCHOOL_ID == SchoolID
                                                                             && p.CLASS_ID == OldClassID && p.PUPIL_ID == PupilID).FirstOrDefault();
            if (objSPC != null)
            {
                objSPC.CLASS_ID = NewClassID;
                SMSParentContractBusiness.Update(objSPC);
            }
        }
        public void GetListSMSParentContractClass(int SchoolID, int AcademicYearID, int SemesterID, List<int> lstClassID)
        {
            AcademicYear academicYear = AcademicYearBusiness.Find(AcademicYearID);
            SMSParentSendDetailSearchBO objSMSSendDetail = new SMSParentSendDetailSearchBO
            {
                SchoolId = SchoolID,
                Year = academicYear.Year
            };
            context = new SMASEntities();
            IQueryable<SMS_PARENT_SENT_DETAIL> qSMSParentSendDetail = (from q in context.SMS_PARENT_SENT_DETAIL
                                                                      where q.SCHOOL_ID == SchoolID
                                                                          && q.YEAR == academicYear.Year
                                                                       select q); 
            // tinh so luong tin nhan trong quy tin cua lop                
            var LstPupilContractInClass = (from sc in context.SMS_PARENT_CONTRACT
                                           join scd in context.SMS_PARENT_CONTRACT_DETAIL on sc.SMS_PARENT_CONTRACT_ID equals scd.SMS_PARENT_CONTRACT_ID
                                           join spd in context.SMS_SERVICE_PACKAGE_DETAIL on scd.SERVICE_PACKAGE_ID equals spd.SERVICE_PACKAGE_ID
                                           join spdd in context.SMS_SERVICE_PACKAGE_DECLARE on scd.SERVICE_PACKAGE_ID equals spdd.SERVICE_PACKAGE_ID
                                           join sentDtl in qSMSParentSendDetail on scd.SMS_PARENT_CONTRACT_DETAIL_ID equals sentDtl.SMS_PARENT_CONTRACT_DETAIL_ID into gj
                                           join pp in context.PupilProfile on sc.PUPIL_ID equals pp.PupilProfileID
                                           from x in gj.DefaultIfEmpty(null)
                                           where sc.SCHOOL_ID == SchoolID
                                              && sc.PARTITION_ID == SchoolID % 100
                                              && scd.SUBSCRIPTION_STATUS == GlobalConstantsEdu.STATUS_REGISTED
                                              && scd.IS_ACTIVE == true && sc.STATUS == GlobalConstantsEdu.SMS_PARENT_CONTRACT_STATUS_IS_ACCEPT && sc.IS_DELETED != true
                                              && lstClassID.Contains(sc.CLASS_ID)
                                              && (scd.SUBSCRIPTION_TIME == SemesterID)
                                              && scd.YEAR_ID == academicYear.Year
                                              && spdd.YEAR == academicYear.Year
                                              && spdd.STATUS == true
                                              && spdd.IS_LIMIT != true
                                              && pp.IsActive
                                           select new
                                           {
                                               ClassID = sc.CLASS_ID,
                                               PupilFileID = sc.PUPIL_ID,
                                               SMSParentContactDetailID = scd.SMS_PARENT_CONTRACT_DETAIL_ID,
                                               MaxSMS = (spd.MAX_SMS.HasValue) ? SemesterID == 3 ? spd.MAX_SMS.Value * 2 : spd.MAX_SMS.Value : 0,
                                               SentSMS = ((x == null) ? 0 : (x.SENT_TOTAL)),
                                               ServicePackageID = spd.SERVICE_PACKAGE_ID,
                                           })
                                            .GroupBy(x => new { x.ClassID, x.PupilFileID, x.SMSParentContactDetailID, x.ServicePackageID, x.SentSMS })
                                            .Select(y => new PupilContractBO
                                            {
                                                ClassID = y.Key.ClassID,
                                                PupilID = y.Key.PupilFileID,
                                                SMSParentContactDetailID = y.Key.SMSParentContactDetailID,
                                                ServicePackageID = y.Key.ServicePackageID,
                                                SentSMS = y.Key.SentSMS,
                                                TotalSMS = y.Sum(a => a.MaxSMS),
                                            }).ToList();

            var LstPupilContractInClassExtra = (from sc in context.SMS_PARENT_CONTRACT
                                                join scd in context.SMS_PARENT_CONTRACT_DETAIL on sc.SMS_PARENT_CONTRACT_ID equals scd.SMS_PARENT_CONTRACT_ID
                                                join scde in context.SMS_PARENT_CONTRACT_EXTRA on scd.SMS_PARENT_CONTRACT_DETAIL_ID equals scde.SMS_PARENT_CONTRACT_DETAIL_ID
                                                join spd in context.SMS_SERVICE_PACKAGE_DETAIL on scde.SERVICE_PACKAGE_ID equals spd.SERVICE_PACKAGE_ID
                                                join spdd in context.SMS_SERVICE_PACKAGE_DECLARE on scd.SERVICE_PACKAGE_ID equals spdd.SERVICE_PACKAGE_ID
                                                join pp in context.PupilProfile on sc.PUPIL_ID equals pp.PupilProfileID
                                                where sc.SCHOOL_ID == SchoolID
                                                   && sc.PARTITION_ID == SchoolID % 100
                                                   && scd.SUBSCRIPTION_STATUS == GlobalConstantsEdu.STATUS_REGISTED
                                                   && scd.IS_ACTIVE == true && sc.STATUS == GlobalConstantsEdu.SMS_PARENT_CONTRACT_STATUS_IS_ACCEPT && sc.IS_DELETED != true
                                                   && lstClassID.Contains(sc.CLASS_ID)
                                                   && (scd.SUBSCRIPTION_TIME == SemesterID)
                                                   && scd.YEAR_ID == academicYear.Year
                                                   && spdd.YEAR == academicYear.Year
                                                   && spdd.STATUS == true
                                                   && spdd.IS_LIMIT != true
                                                   && pp.IsActive
                                                select new
                                                {
                                                    ClassID = sc.CLASS_ID,
                                                    PupilFileID = sc.PUPIL_ID,
                                                    SMSParentContactDetailID = scd.SMS_PARENT_CONTRACT_DETAIL_ID,
                                                    MaxSMS = (spd.MAX_SMS.HasValue) ? SemesterID == 3 ? spd.MAX_SMS.Value * 2 : spd.MAX_SMS.Value : 0,
                                                    ServicePackageID = spd.SERVICE_PACKAGE_ID,
                                                })
                                            .GroupBy(x => new { x.ClassID, x.PupilFileID, x.SMSParentContactDetailID, x.ServicePackageID })
                                            .Select(y => new PupilContractBO
                                            {
                                                ClassID = y.Key.ClassID,
                                                PupilID = y.Key.PupilFileID,
                                                SMSParentContactDetailID = y.Key.SMSParentContactDetailID,
                                                ServicePackageID = y.Key.ServicePackageID,
                                                TotalSMS = y.Sum(a => a.MaxSMS),
                                            }).ToList();
            int ClassID = 0;
            for (int i = 0; i < lstClassID.Count; i++)
            {
                ClassID = lstClassID[i];
                var LstPupilContractInClasstmp = LstPupilContractInClass.Where(p => p.ClassID == ClassID).ToList();
                var LstPupilContractInClassExtratmp = LstPupilContractInClassExtra.Where(p => p.ClassID == ClassID).ToList();
                // chi them chi tiet khi lop co hop dong hoc sinh
                if ((LstPupilContractInClasstmp.Count > 0) || LstPupilContractInClassExtratmp.Count > 0)
                {
                    int TotalSMSInClass = LstPupilContractInClasstmp.Sum(o => o.TotalSMS);
                    int TotalSMSInClassExtra = LstPupilContractInClassExtratmp.Sum(o => o.TotalSMS);
                    TotalSMSInClass = TotalSMSInClass + TotalSMSInClassExtra;
                    int TotalSMSSent = LstPupilContractInClasstmp.Sum(o => o.SentSMS);

                    SMS_PARENT_CONTRACT_CLASS objClassDetail = new SMS_PARENT_CONTRACT_CLASS();
                    objClassDetail.CLASS_ID = ClassID;
                    objClassDetail.ACADEMIC_YEAR_ID = AcademicYearID;
                    objClassDetail.SCHOOL_ID = SchoolID;
                    objClassDetail.YEAR = academicYear.Year;
                    objClassDetail.SEMESTER = (byte)SemesterID;
                    objClassDetail.SENT_TOTAL = TotalSMSSent;
                    objClassDetail.TOTAL_SMS = TotalSMSInClass;
                    objClassDetail.CREATED_DATE = DateTime.Now;
                    context.SMS_PARENT_CONTRACT_CLASS.Add(objClassDetail);
                }
            }
            context.SaveChanges();
        }
        #endregion
    }
}
