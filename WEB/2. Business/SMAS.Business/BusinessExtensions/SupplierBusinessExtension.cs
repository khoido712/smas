/**
 * The Viettel License
 *
 * Copyright 2012 Viettel Telecom. All rights reserved.
 * VIETTEL PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */

/** 
* @author  
* @version $Revision: $
*/

using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Linq.Expressions;
using System.Web;
using log4net;

using SMAS.DAL.Repository;
using SMAS.DAL.IRepository;
using SMAS.Business.IBusiness;
using SMAS.Models.Models;
using SMAS.Business.Common;

namespace SMAS.Business.Business
{ 
    public partial class SupplierBusiness
    {
        #region Validate
        public void CheckSupplierDuplicate(Supplier supplier)
        {
            IDictionary<string, object> expDic = null;
            if (supplier.SupplierID > 0)
            {
                expDic = new Dictionary<string, object>();
                expDic["SupplierID"] = supplier.SupplierID;
            }
            bool SupplierNameCompatible = this.repository.ExistsRow(GlobalConstants.LIST_SCHEMA, "Supplier",
                   new Dictionary<string, object>()
                {
                    {"SupplierName",supplier.SupplierName},
                    {"SchoolID",supplier.SchoolID},
                    {"IsActive",true}
                }, expDic);
            if (SupplierNameCompatible)
            {
                List<object> listParam = new List<object>();
                listParam.Add("Supplier_Label_SupplierName");
                throw new BusinessException("Common_Validate_Duplicate", listParam);
            }

        }

        public void CheckSupplierIDAvailable(int supplierID) {
            bool AvailableSupplierIDCompatible = this.repository.ExistsRow(GlobalConstants.LIST_SCHEMA, "Supplier",
                   new Dictionary<string, object>()
                {
                    {"SupplierID",supplierID}
                    
                }, null);
            if (!AvailableSupplierIDCompatible)
            {
                throw new BusinessException("Common_Validate_NotAvailable");
            }
        }
        #endregion
        #region Insert
        public void Insert(Supplier supplier)
        {
            //Check Require, max length
            ValidationMetadata.ValidateObject(supplier);


            //Check duplicate
            CheckSupplierDuplicate(supplier);

           

            supplier.IsActive = true;
            base.Insert(supplier);
        }
        #endregion
        #region Update
        public void Update(Supplier supplier)
        {
            //Check duplicate
            CheckSupplierDuplicate(supplier);
            
            //Check available
            CheckSupplierIDAvailable(supplier.SupplierID);

            //Check require, max length
            ValidationMetadata.ValidateObject(supplier);

            //Check not compatible
            bool SupplierIDCompatible = this.repository.ExistsRow(GlobalConstants.LIST_SCHEMA, "Supplier",
                   new Dictionary<string, object>()
                {
                    {"SupplierID",supplier.SupplierID},
                    {"SchoolID",supplier.SchoolID},
                    {"IsActive",true}
                }, null);
            if (!SupplierIDCompatible)
            {
                throw new BusinessException("Common_Validate_NotCompatible");
            }

            base.Update(supplier);
        }
        #endregion
        #region Delete
        public void Delete(int supplierID, int schoolID)
        {
            //Check  Available
            CheckSupplierIDAvailable(supplierID);

            //Check not compatible
            bool SupplierIDCompatible = this.repository.ExistsRow(GlobalConstants.LIST_SCHEMA, "Supplier",
                   new Dictionary<string, object>()
                {
                    {"SupplierID",supplierID},
                    {"SchoolID",schoolID},
                    {"IsActive",true}
                }, null);
            if (!SupplierIDCompatible)
            {
                throw new BusinessException("Common_Validate_NotCompatible");
            }

            // Check Using
            this.CheckConstraints(GlobalConstants.LIST_SCHEMA, "Supplier", supplierID, "Supplier_Label_SupplierID");
            //Supplier s = this.Find(supplierID);
            //s.IsActive = false;
            //base.Update(s);
            base.Delete(supplierID, true);
        }
        #endregion
        #region Search
        public IQueryable<Supplier> Search(IDictionary<string, object> dic)
        {
            string supplierName = Utils.GetString(dic, "SupplierName");
            string address = Utils.GetString(dic, "Address");
            string phone = Utils.GetString(dic, "Phone");
            string shipper = Utils.GetString(dic, "Shipper");
            string shipperPhone = Utils.GetString(dic, "ShipperPhone");
            int schoolID = Utils.GetInt(dic, "SchoolID");
            bool? isActive = Utils.GetNullableBool(dic, "IsActive");
            IQueryable<Supplier> listSupplier = this.SupplierRepository.All;
            if (!supplierName.Equals(string.Empty))
            {
                listSupplier = listSupplier.Where(x => x.SupplierName.ToLower().Contains(supplierName.ToLower()));
            }
            if (!address.Equals(string.Empty))
            {
                listSupplier = listSupplier.Where(x => x.Address.ToLower().Contains(address.ToLower()));
            }
            if (!phone.Equals(string.Empty))
            {
                listSupplier = listSupplier.Where(x => x.Phone.ToLower().Contains(phone.ToLower()));
            }
            if (!shipper.Equals(string.Empty))
            {
                listSupplier = listSupplier.Where(x => x.Shipper.ToLower().Contains(shipper.ToLower()));
            }
            if (!shipperPhone.Equals(string.Empty))
            {
                listSupplier = listSupplier.Where(x => x.ShipperPhone.ToLower().Contains(shipperPhone.ToLower()));
            }
            if (isActive.HasValue)
            {
                listSupplier = listSupplier.Where(x => x.IsActive == true);
            }
            if (schoolID != 0)
            {
                listSupplier = listSupplier.Where(x => x.SchoolID == schoolID);
            }
            return listSupplier;
        }
        public IQueryable<Supplier> SearchBySchool(int schoolID, IDictionary<string, object> dic = null)
        {
            //IQueryable<Supplier> listSupplier = this.SupplierRepository.All;
            if (schoolID == 0)
            {
                return null;
            }
            else
            {
                //listSupplier = listSupplier.Where(x => x.SchoolID == schoolID);
                if (dic == null)
                {
                    dic = new Dictionary<string, object>();
                }
                dic["SchoolID"] = schoolID;
                return Search(dic);
            }
        }
        #endregion
    }
}