﻿/**
 * The Viettel License
 *
 * Copyright 2012 Viettel Telecom. All rights reserved.
 * VIETTEL PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */

/** 
* @author  
* @version $Revision: $
*/

using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Linq.Expressions;
using System.Web;
using log4net;

using SMAS.DAL.Repository;
using SMAS.DAL.IRepository;
using SMAS.Business.IBusiness;
using SMAS.Models.Models;
using SMAS.Business.Common;
using SMAS.Business.BusinessObject;
using System.Transactions;
using System.Threading;

namespace SMAS.Business.Business
{
    public partial class EWalletBusiness
    {
        public EwalletBO GetEWallet(int unitId, bool schoolUser, bool superUser)
        {
            SMASEntities context = new SMASEntities();
            EwalletBO res = new EwalletBO();
            if (unitId > 0 && (schoolUser || superUser))
            {
                var ewallet = (from a in context.EW_EWALLET
                               where a.UNIT_ID == unitId
                               && ((schoolUser && a.EWALLET_TYPE == GlobalConstantsEdu.USER_TYPE_SCHOOL) || (superUser && a.EWALLET_TYPE == GlobalConstantsEdu.USER_TYPE_SUPER_VISING_DEPT))
                               select a).FirstOrDefault();

                // neu chua ton tai vi thi tao moi vi
                if (ewallet == null)
                {
                    ewallet = new EW_EWALLET();
                    ewallet.UNIT_ID = unitId;
                    ewallet.BALANCE = 0;
                    ewallet.CREATED_TIME = DateTime.Now;
                    ewallet.TOPUP_FAIL_COUNT = 0;
                    ewallet.IS_LOCKED_OUT = false;
                    ewallet.EWALLET_TYPE = schoolUser ? GlobalConstantsEdu.USER_TYPE_SCHOOL : superUser ? GlobalConstantsEdu.USER_TYPE_SUPER_VISING_DEPT : 0;
                    context.EW_EWALLET.Add(ewallet);
                    context.SaveChanges();
                }

                // AnhVD9 - Bổ sung thông tin tài khoản
                res.UnitID = ewallet.UNIT_ID;
                res.EWalletID = ewallet.EWALLET_ID;
                res.EwalletType = ewallet.EWALLET_TYPE;
                res.Balance = ewallet.BALANCE;

                // Kiem tra trang thai khoa cua vi dien tu
                if (ewallet.IS_LOCKED_OUT == false) // neu ko bi khoa thi tra lai ket qua la khong bi khoa
                {
                    res.IsLockedOut = false;
                    res.RemainOfTimeToUnlock = new TimeSpan(0);
                }
                else // Neu dang bi khoa thi kiem tra xem co con trong thoi gian bi khoa hay khong. Neu khong con trong thoi gian bi khoa thi mo khoa cho nguoi dung
                {
                    if (ewallet.LAST_TOPUP_FAIL_DATE.HasValue)
                    {
                        // thoi gian khoa
                        int lockOutMinute = GlobalConstantsEdu.EWALLET_LOCK_MIN;
                        TimeSpan lockOutTimeSpan = new TimeSpan(0, lockOutMinute, 0);
                        TimeSpan lockedOutTimeSpan = DateTime.Now - ewallet.LAST_TOPUP_FAIL_DATE.Value;
                        if (lockedOutTimeSpan >= lockOutTimeSpan)
                        {
                            // Mo kho nguoi dung
                            ewallet.IS_LOCKED_OUT = false;
                            ewallet.TOPUP_FAIL_COUNT = 0;
                            context.SaveChanges();
                            res.IsLockedOut = false;
                            res.RemainOfTimeToUnlock = new TimeSpan(0);
                        }
                        else
                        {
                            res.IsLockedOut = true;
                            res.RemainOfTimeToUnlock = lockOutTimeSpan - lockedOutTimeSpan;
                        }
                    }
                    else
                    {
                        ewallet.IS_LOCKED_OUT = false;
                        ewallet.TOPUP_FAIL_COUNT = 0;
                        context.SaveChanges();
                        res.IsLockedOut = false;
                        res.RemainOfTimeToUnlock = new TimeSpan(0);
                    }
                }
            }

            return res;
        }

        /// <summary>
        /// change amount sub
        /// </summary>
        /// <param name="ewalletID">id vi dien tu</param>
        /// <param name="amount">so tien thay doi</param>
        /// <returns></returns>
        public EW_EWALLET ChangeAmountSub(int ewalletID, decimal amount, byte transTypeID, int quantity, byte? serviceType)
        {
            SMASEntities context = new SMASEntities();
            // tao transaction
            var ewallet = context.EW_EWALLET.Find(ewalletID);
            if (ewallet != null)
            {
                // tao transaction, mac dinh la that bai
                // sau khi thuc hien xong giao dich nap the thi update lai thong tin transaction
                EW_EWALLET_TRANSACTION etrans = new EW_EWALLET_TRANSACTION();
                etrans.EWALLET_ID = ewallet.EWALLET_ID;
                etrans.TRANS_TYPE_ID = transTypeID;

                etrans.BALANCE_BEFORE = (int)ewallet.BALANCE;
                etrans.BALANCE_AFTER = (int)(ewallet.BALANCE - amount);
                etrans.CREATED_TIME = DateTime.Now;
                etrans.TRANS_AMOUNT = (int)amount;
                etrans.STATUS = 1;
                etrans.UPDATED_TIME = DateTime.Now;
                // cap nhat lai vi dien tu

                ewallet.BALANCE = ewallet.BALANCE - (int)amount;
                ewallet.UPDATED_TIME = DateTime.Now;
                // nếu Loại giao dịch bằng 2 thì lưu loại dịch vụ
                if (transTypeID == GlobalConstantsEdu.TRANSACTION_TYPE_AMOUNT_SUB_CHECKOUT)
                {
                    if (quantity > 0)
                        etrans.QUANTITY = quantity;
                    if (serviceType.HasValue && serviceType.Value > 0)
                        etrans.SERVICE_TYPE = serviceType;
                }

                context.EW_EWALLET_TRANSACTION.Add(etrans);
                context.Entry<EW_EWALLET>(ewallet).State = EntityState.Modified;
                context.SaveChanges();
            }

            return ewallet;
        }

        /// <summary>
        /// lay thong tin vi dien tu: tai khoan chinh + tai khoan khuyen mai
        /// </summary>
        /// <author>AnhVD</author>
        /// <param name="dic"></param>
        /// <returns></returns>
        public EwalletBO GetEWalletIncludePromotion(int unitId, bool schoolUser, bool superUser)
        {
            EwalletBO response = this.GetEWallet(unitId, schoolUser, superUser);

            return response;
        }

        /// <summary>
        /// Đợi để nạp card (không cho Nạp đồng thời)
        /// </summary>
        /// <param name="EwalletID"></param>
        /// <returns></returns>
        public void WaitingTopup(int EwalletID)
        {

            EW_EWALLET ewallet = this.Find(EwalletID);
            while (ewallet != null && ewallet.IS_PROCESSING)
            {
                Thread.Sleep(GlobalConstantsEdu.WAITING_NUMBER_OF_MILISECONDS);
                // Su dung context moi
                context = new SMASEntities();
                IEWalletBusiness ewBusiness = new EWalletBusiness(logger, context);
                ewallet = ewBusiness.Find(EwalletID);
            }
        }

        /// <summary>
        /// Dieu huong vi dien tu
        /// </summary>
        /// <author date="22/09/2015">AnhVD9</author>
        /// <param name="EwalletID"></param>
        /// <param name="isLock"></param>
        /// <returns></returns>
        public bool LockEWallet(int EwalletID, bool isLock)
        {
            try
            {
                EW_EWALLET ewallet = this.Find(EwalletID);
                if (ewallet != null)
                {
                    ewallet.IS_PROCESSING = isLock;
                    context.SaveChanges();
                }
                return true;
            }
            catch
            {
                return false;
            }

        }

        public EwalletBO ChangesAmountAddDirectly(int ewalletID, decimal amount)
        {

            EwalletBO response = new EwalletBO();

            var ewallet = this.Find(ewalletID);
            if (ewallet != null)
            {

                ewallet.BALANCE = ewallet.BALANCE + (int)amount;
                ewallet.UPDATED_TIME = DateTime.Now;

                this.Save();

                response.Balance = ewallet.BALANCE;
                response.EWalletID = ewallet.EWALLET_ID;
            }

            return response;
        }

        /// <summary>
        /// Deduct money 2 account: main + promotion
        /// </summary>
        /// <param name="dic"></param>
        /// <returns></returns>
        public EWalletObject DeductMoneyAccount(Dictionary<string, object> dic)
        {
            int ewalletID = Utils.GetInt(dic, "EWalletID");
            decimal amount = Utils.GetDecimal(dic, "Amount");
            byte transType = Utils.GetByte(dic, "TransTypeID");
            int? quantity = Utils.GetNullableInt(dic, "Quantity");
            byte? serviceType = Utils.GetNullableByte(dic, "ServiceType");
            // Trừ tài khoản khuyến mãi
            int promotionAccountID = Utils.GetInt(dic, "PromotionAccountID");
            decimal promotionBalance = Utils.GetDecimal(dic, "PromotionBalance");
            decimal internalBalance = Utils.GetDecimal(dic, "InternalBalance");
            int internalSMS = Utils.GetInt(dic, "InternalSMS");
            int externalSMS = Utils.GetInt(dic, "ExternalSMS");

            EWalletObject response = new EWalletObject();

            // tao transaction
            var ewallet = this.Find(ewalletID);
            if (ewallet != null)
            {
                // tao transaction, mac dinh la that bai
                // sau khi thuc hien xong giao dich nap the thi update lai thong tin transaction
                if (amount > 0)
                {
                    EW_EWALLET_TRANSACTION etrans = new EW_EWALLET_TRANSACTION();
                    etrans.EWALLET_ID = ewallet.EWALLET_ID;
                    etrans.TRANS_TYPE_ID = transType;

                    etrans.BALANCE_BEFORE = (int)ewallet.BALANCE;
                    etrans.BALANCE_AFTER = (int)(ewallet.BALANCE - amount);
                    etrans.CREATED_TIME = DateTime.Now;
                    etrans.TRANS_AMOUNT = (int)amount;
                    etrans.STATUS = 1;
                    etrans.UPDATED_TIME = DateTime.Now;
                    // cap nhat lai vi dien tu
                    ewallet.BALANCE = (int)(ewallet.BALANCE - amount);
                    ewallet.UPDATED_TIME = DateTime.Now;
                    // loai dich vu trong giao dich tru tien
                    if (transType == GlobalConstantsEdu.TRANSACTION_TYPE_AMOUNT_SUB_CHECKOUT)
                    {
                        if (serviceType.HasValue && serviceType.Value > 0)
                            etrans.SERVICE_TYPE = serviceType;
                        if (quantity.HasValue && quantity.Value > 0)
                            etrans.QUANTITY = quantity;
                    }
                    this.EWalletTransactionBusiness.Insert(etrans);
                    this.Update(ewallet);
                }

                this.Save();

                response.Balance = ewallet.BALANCE;
                response.EWalletID = ewallet.EWALLET_ID;
                response.UnitID = ewallet.UNIT_ID;
            }


            //VTODO LogBusiness.InsertLog(DateTime.Now, DateTime.Now, ewallet.AccountID.ToString(), "EWalletBusiness_DeductMoneyAccount", GlobalConstants.COMMON_LOG_ACTION_ADD, JsonConvert.SerializeObject(dic, new JsonSerializerSettings() { PreserveReferencesHandling = PreserveReferencesHandling.Objects }), JsonConvert.SerializeObject(response, new JsonSerializerSettings() { PreserveReferencesHandling = PreserveReferencesHandling.Objects }), "Goi Ham Tru Tien Vao Tai Khoan Khi Gui Tin Nhan");

            return response;
        }
    }

    /// <summary>
    /// Loai nguoi dung
    /// </summary>
    public enum UserType
    {
        /// <summary>
        /// Nguoi dung cap truong
        /// </summary>
        SchoolUser,
        /// <summary>
        /// Nguoi dung cap phong, so
        /// </summary>
        SupervisingDeptUser
    }

    /// <summary>
    /// Thong tin ve vi dien tu cua nguoi dung
    /// </summary>
    public class EWalletObject
    {
        /// <summary>
        /// ID
        /// </summary>
        public int EWalletID { get; set; }

        /// <summary>
        /// So du hien tai
        /// </summary>
        public decimal Balance { get; set; }

        /// <summary>
        /// So luong tin nhan khuyen mai
        /// </summary>
        public int NumberOfPromotiveSMS { get; set; }

        /// <summary>
        /// True neu vi dien tu bi khoa
        /// </summary>
        public bool IsLockedOut { get; set; }

        public TimeSpan RemainOfTimeToUnlock { get; set; }

        #region AnhVD9 - Bổ sung thông tin tài khoản khuyến mãi
        /// <summary>
        /// Số hiệu tài khoản khuyến mãi
        /// </summary>
        public int PromotionAccountID { get; set; }

        /// <summary>
        /// Số dư tài khoản khuyến mãi
        /// </summary>
        public decimal PromotionBalance { get; set; }

        /// <summary>
        /// Số dư tài khoản khuyến mãi nội mạng
        /// </summary>
        public decimal InternalBalance { get; set; }

        /// <summary>
        /// Số tin nội mạng khuyến mãi
        /// </summary>
        public int InternalSMS;

        /// <summary>
        /// Số tin ngoại mạng khuyến mãi
        /// </summary>
        public int ExternalSMS;

        /// <summary>
        /// Số hiệu tài khoản
        /// </summary>
        public int UnitID { get; set; }
        #endregion End AnhVD9 - 20141027
    }
}
