﻿/**
 * The Viettel License
 *
 * Copyright 2012 Viettel Telecom. All rights reserved.
 * VIETTEL PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */

/** 
* @author  
* @version $Revision: $
*/

using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Linq.Expressions;
using System.Web;
using log4net;

using SMAS.DAL.Repository;
using SMAS.DAL.IRepository;
using SMAS.Business.IBusiness;
using SMAS.Models.Models;
using SMAS.Business.Common;
using System.IO;
using SMAS.Business.BusinessObject;
using SMAS.VTUtils;
using SMAS.VTUtils.Excel.Export;
using System.Text;

namespace SMAS.Business.Business
{
    public partial class PointReportBusiness
    {
        #region private member variable

        #endregion

        #region Lấy thông tin chung của báo cáo theo đợt
        public ReportDefinition GetReportDefinitionOfPeriodReport(PointReportBO entity)
        {
            string reportCode = (entity.PaperSize == SystemParamsInFile.PAPER_SIZE_A4) ?
                SystemParamsInFile.REPORT_PHIEU_BAO_DIEM_THEO_DOT_A4 : SystemParamsInFile.REPORT_PHIEU_BAO_DIEM_THEO_DOT_A5;
            return ReportDefinitionBusiness.GetByCode(reportCode);
        }
        #endregion

        #region Tạo mảng băm cho các tham số đầu vào của phiếu báo điểm theo đợt
        public string GetHashKeyForPeriod(PointReportBO entity)
        {
            IDictionary<string, object> dic = new Dictionary<string, object> {
                {"AcademicYearID", entity.AcademicYearID},
                {"PeriodDeclarationID", entity.PeriodDeclarationID},
                {"EducationLevelID ", entity.EducationLevelID},
                {"ClassID", entity.ClassID},
                {"PaperSize", entity.PaperSize},
                {"SchoolID", entity.SchoolID},
                {"AppliedLevel", entity.AppliedLevel},
                {"UserAccountID", entity.UserAccountID}
            };
            return ReportUtils.GetHashKey(dic);
        }
        #endregion

        #region Lấy phiếu báo điểm theo đợt được cập nhật mới nhất
        public ProcessedReport GetPointReportByPeriod(PointReportBO entity)
        {
            string reportCode = (entity.PaperSize == SystemParamsInFile.PAPER_SIZE_A4) ?
                SystemParamsInFile.REPORT_PHIEU_BAO_DIEM_THEO_DOT_A4 : SystemParamsInFile.REPORT_PHIEU_BAO_DIEM_THEO_DOT_A5;
            string inputParameterHashKey = GetHashKeyForPeriod(entity);
            return ProcessedReportBusiness.GetProcessedReport(reportCode, inputParameterHashKey);
        }
        #endregion

        #region Lưu báo cáo điểm theo đợt vào CSDL
        public ProcessedReport InsertPointReportByPeriod(PointReportBO entity, Stream data)
        {
            string reportCode = (entity.PaperSize == SystemParamsInFile.PAPER_SIZE_A4) ?
                SystemParamsInFile.REPORT_PHIEU_BAO_DIEM_THEO_DOT_A4 : SystemParamsInFile.REPORT_PHIEU_BAO_DIEM_THEO_DOT_A5;
            ReportDefinition reportDef = ReportDefinitionBusiness.GetByCode(reportCode);
            ProcessedReport pr = new ProcessedReport();
            pr.ReportCode = reportCode;
            pr.ProcessedDate = DateTime.Now;
            pr.InputParameterHashKey = GetHashKeyForPeriod(entity);
            pr.ReportData = ReportUtils.Compress(data);

            //Tạo tên file
            string outputNamePattern = reportDef.OutputNamePattern;
            string schoolLevel = ReportUtils.ConvertAppliedLevelForReportName(entity.AppliedLevel);
            string educationLevelOrClass = (entity.ClassID == 0) ?
                EducationLevelRepository.Find(entity.EducationLevelID).Resolution : ClassProfileBusiness.Find(entity.ClassID).DisplayName;
            string semester = ReportUtils.ConvertSemesterForReportName(entity.Semester);
            string period = PeriodDeclarationBusiness.Find(entity.PeriodDeclarationID).Resolution;

            outputNamePattern = outputNamePattern.Replace("[SchoolLevel]", schoolLevel);
            outputNamePattern = outputNamePattern.Replace("[EducationLevel/Class]", ReportUtils.StripVNSign(educationLevelOrClass));
            outputNamePattern = outputNamePattern.Replace("[Semester]", semester);
            outputNamePattern = outputNamePattern.Replace("[Period]", ReportUtils.StripVNSign(period));

            pr.ReportName = ReportUtils.RemoveSpecialCharacters(outputNamePattern) + "." + reportDef.OutputFormat;

            IDictionary<string, object> dic = new Dictionary<string, object> {
                {"AcademicYearID", entity.AcademicYearID},
                {"PeriodDeclarationID", entity.PeriodDeclarationID},
                {"EducationLevelID ", entity.EducationLevelID},
                {"ClassID", entity.ClassID},
                {"PaperSize", entity.PaperSize},
                {"SchoolID", entity.SchoolID},
                {"AppliedLevel", entity.AppliedLevel},
                {"UserAccountID", entity.UserAccountID}
            };
            ProcessedReportParameterRepository.Insert(dic, pr);
            ProcessedReportRepository.Insert(pr);
            ProcessedReportRepository.Save();
            return pr;
        }
        #endregion

        #region Tạo file phiếu báo điểm theo đợt
        /// <summary>
        /// Tạo file phiếu báo điểm theo đợt
        /// </summary>
        /// <param name="entity"></param>
        /// <returns></returns>
        public Stream CreatePointReportByPeriod(PointReportBO entity)
        {
            // Neu khong co dot bao loi
            if (entity.PeriodDeclarationID == 0)
            {
                throw new BusinessException("Common_Null_PeriodID");
            }
            string reportCode = (entity.PaperSize == SystemParamsInFile.PAPER_SIZE_A4) ?
                SystemParamsInFile.REPORT_PHIEU_BAO_DIEM_THEO_DOT_A4 : SystemParamsInFile.REPORT_PHIEU_BAO_DIEM_THEO_DOT_A5;
            ReportDefinition reportDef = ReportDefinitionBusiness.GetByCode(reportCode);
            string templatePath = SystemParamsInFile.TEMPLATE_FOLDER + reportDef.TemplateDirectory + "/" + reportDef.TemplateName;
            IVTWorkbook oBook = VTExport.OpenWorkbook(templatePath);

            //Lấy sheet phiếu điểm
            IVTWorksheet firstSheet = oBook.GetSheet(1);

            //Lấy sheet thông tin chung
            IVTWorksheet GeneralInfoSheet = oBook.GetSheet(2);

            //fill dữ liệu vào sheet thông tin chung
            SchoolProfile school = SchoolProfileBusiness.Find(entity.SchoolID);
            PeriodDeclaration period = PeriodDeclarationBusiness.Find(entity.PeriodDeclarationID);
            AcademicYear academicYear = period.AcademicYear;
            string supervisingDeptName = UtilsBusiness.GetSupervisingDeptName(school.SchoolProfileID, entity.AppliedLevel).ToUpper();
            string schoolName = school.SchoolName.ToUpper();
            string periodName = period.Resolution +
                " (" + Utils.ConvertDateToString(period.FromDate) +
                " - " + Utils.ConvertDateToString(period.EndDate) + ") - " +
                SMASConvert.ConvertSemester(entity.Semester).ToUpper() + ", " +
                academicYear.Year + " - " + (academicYear.Year + 1);

            GeneralInfoSheet.SetCellValue("B2", supervisingDeptName);
            GeneralInfoSheet.SetCellValue("B3", schoolName);
            GeneralInfoSheet.SetCellValue("B5", periodName);
            //GeneralInfoSheet.SetCellValue("B6", school.HeadMasterName);
            GeneralInfoSheet.SetCellValue("B6", "Phụ huynh học sinh");
            //Lấy danh sách các lớp cần in phiếu báo điểm
            IQueryable<ClassProfile> lstClassTemp = ClassProfileBusiness.getClassByAccountRole(entity.AcademicYearID, entity.SchoolID, entity.EducationLevelID, entity.ClassID, entity.UserAccountID, entity.IsSuperVisingDept, entity.IsSubSuperVisingDept);

            List<ClassProfileTempBO> lstClass = (from p in lstClassTemp
                                                 join q in EmployeeBusiness.All.Where(o => o.SchoolID == entity.SchoolID) on p.HeadTeacherID equals q.EmployeeID into g1
                                                 from j in g1.DefaultIfEmpty()
                                                 select new ClassProfileTempBO
                                             {
                                                 ClassProfileID = p.ClassProfileID,
                                                 EducationLevelID = p.EducationLevelID,
                                                 OrderNumber = p.OrderNumber,
                                                 SchoolID = p.SchoolID,
                                                 AcademicYearID = p.AcademicYearID,
                                                 DisplayName = p.DisplayName,
                                                 EmployeeID = j.EmployeeID,
                                                 EmployeeName = j.FullName,
                                                 SectionKey = p.SeperateKey
                                             }).OrderBy(o => o.DisplayName).ToList();


            //Lấy danh sách các môn học cho các lớp
            Dictionary<string, object> dicSubject = new Dictionary<string, object> 
            {
                {"AcademicYearID", entity.AcademicYearID}, 
                {"EducationLevelID", entity.EducationLevelID},
                {"ClassID", entity.ClassID},
                {"AppliedLevel", entity.AppliedLevel}
            };
            IQueryable<ClassSubject> lstSubjectTemp = ClassSubjectBusiness.SearchBySchool(entity.SchoolID, dicSubject);
            List<ClassSubjectBO> lstSubject = (from p in lstSubjectTemp
                                               join s in SubjectCatBusiness.All on p.SubjectID equals s.SubjectCatID
                                               select new ClassSubjectBO
                                               {
                                                   SubjectID = p.SubjectID,
                                                   ClassID = p.ClassID,
                                                   SubjectName = s.SubjectName,
                                                   DisplayName = s.DisplayName,
                                                   OrderInSubject = s.OrderInSubject,
                                                   IsCommenting = p.IsCommenting,
                                                   SubjectIDCrease = p.SubjectIDIncrease != null ? p.SubjectIDIncrease.Value : 0
                                               }).ToList();

            //Lấy danh sách học sinh
            Dictionary<string, object> dicPupil = new Dictionary<string, object> 
            {
                {"AcademicYearID", entity.AcademicYearID}, 
                {"EducationLevelID", entity.EducationLevelID},
                {"ClassID", entity.ClassID}
                //{"Check", "check"}
            };

            // Lấy thông tin học sinh theo khối, lớp
            // Lấy luôn ra PupilProfile và OrderInClass để sắp xếp và tăng hiệu năng
            List<PupilOfClassBO> lstPupil = (from poc in PupilOfClassBusiness.SearchBySchool(entity.SchoolID, dicPupil)
                                             join pp in PupilProfileBusiness.All on poc.PupilID equals pp.PupilProfileID
                                             select new PupilOfClassBO
                                             {
                                                 PupilID = poc.PupilID,
                                                 PupilCode = pp.PupilCode,
                                                 PupilFullName = pp.FullName,
                                                 Name = pp.Name,
                                                 ClassID = poc.ClassID,
                                                 EndDate = poc.EndDate,
                                                 Status = poc.Status,
                                                 AssignedDate = poc.AssignedDate,
                                                 OrderInClass = poc.OrderInClass,
                                             })
                .ToList();

            //Lấy danh sách thông tin học sinh nghỉ học
            Dictionary<string, object> dicPupilAbsence = new Dictionary<string, object> 
            {
                {"SchoolID",entity.SchoolID},
                {"AcademicYearID", entity.AcademicYearID}, 
                {"EducationLevelID", entity.EducationLevelID},
                {"ClassID", entity.ClassID},
                {"SemesterID",entity.Semester},
                {"FromAbsentDate", period.FromDate},
                {"ToAbsentDate", period.EndDate}
            };
            List<PupilAbsenceBO> lstPupilAbsence = PupilAbsenceBusiness.GetListPupilAbsenceBySection(dicPupilAbsence).ToList();

            //Lấy danh sách thông tin học sinh vi phạm
            Dictionary<string, object> dicPupilFault = new Dictionary<string, object> 
            {
                {"AcademicYearID", entity.AcademicYearID}, 
                {"EducationLevelID", entity.EducationLevelID},
                {"ClassID", entity.ClassID},
                {"FromViolatedDate", period.FromDate},
                {"ToViolatedDate", period.EndDate}
            };
            List<PupilFaultBO> lstPupilFault = PupilFaultBusiness.SearchBySchool(entity.SchoolID, dicPupilFault)
                .Select(o => new PupilFaultBO
                {
                    PupilFaultID = o.PupilFaultID,
                    PupilID = o.PupilID,
                    EducationLevelID = o.EducationLevelID,
                    ClassID = o.ClassID,
                    FaultID = o.FaultID,
                    NumberOfFault = o.NumberOfFault,
                    Resolution = o.FaultCriteria.Resolution
                })
                .ToList();

            //Lấy danh sách thông tin đánh giá học lực
            Dictionary<string, object> dicPupilRanking = new Dictionary<string, object> 
            {
                {"AcademicYearID", entity.AcademicYearID}, 
                {"EducationLevelID", entity.EducationLevelID},
                {"ClassID", entity.ClassID},
                {"PeriodID", entity.PeriodDeclarationID}, 
                {"SchoolID", entity.SchoolID}
            };
            List<PupilRankingBO> lstPupilRanking = (from p in PupilRankingBusiness.SearchBySchool(entity.SchoolID, dicPupilRanking)
                                                    join q in CapacityLevelBusiness.All on p.CapacityLevelID equals q.CapacityLevelID into g1
                                                    from j1 in g1.DefaultIfEmpty()
                                                    join r in ConductLevelBusiness.All on p.ConductLevelID equals r.ConductLevelID into g2
                                                    from j2 in g2.DefaultIfEmpty()
                                                    select new PupilRankingBO
                                                    {
                                                        PupilRankingID = p.PupilRankingID,
                                                        PupilID = p.PupilID,
                                                        EducationLevelID = p.EducationLevelID,
                                                        ClassID = p.ClassID,
                                                        Semester = p.Semester,
                                                        ConductLevelID = p.ConductLevelID,
                                                        CapacityLevelID = p.CapacityLevelID,
                                                        ConductLevelName = j2.Resolution,
                                                        CapacityLevel = j1.CapacityLevel1,
                                                        Comment = p.PupilRankingComment,
                                                        AverageMark = p.AverageMark,
                                                        Rank = p.Rank
                                                    })
                .ToList();

            // Lấy danh sách đầu điểm theo đợt
            string strMarkType = MarkRecordBusiness.GetMarkTitle(entity.PeriodDeclarationID);
            //Lấy danh sách thông tin điểm môn tính điểm
            Dictionary<string, object> dicMarkRecord = new Dictionary<string, object> 
            {
                {"AcademicYearID", entity.AcademicYearID}, 
                {"SchoolID", entity.SchoolID},
                {"EducationLevelID", entity.EducationLevelID},
                {"ClassID", entity.ClassID},
                {"Semester", entity.Semester},
                {"StrContainMarkTitle", strMarkType},
                {"Year", academicYear.Year}
            };
            List<MarkRecordBO> lstMarkRecord = (from p in VMarkRecordBusiness.SearchBySchool(entity.SchoolID, dicMarkRecord)
                                                join q in MarkTypeBusiness.All on p.MarkTypeID equals q.MarkTypeID
                                                select new MarkRecordBO
                                                {
                                                    PupilID = p.PupilID,
                                                    ClassID = p.ClassID,
                                                    SubjectID = p.SubjectID,
                                                    Title = q.Title,
                                                    Mark = p.Mark,
                                                    Coefficient = q.Coefficient,
                                                    Semester = p.Semester,
                                                    OrderNumber = p.OrderNumber
                                                }).ToList();


            //Lấy danh sách thông tin điểm môn nhận xét

            Dictionary<string, object> dicJudgeRecord = new Dictionary<string, object> 
            {
                {"AcademicYearID", entity.AcademicYearID}, 
                {"SchoolID", entity.SchoolID},
                {"EducationLevelID", entity.EducationLevelID},
                {"ClassID", entity.ClassID},
                {"Semester", entity.Semester},
                {"StrContainMarkTitle", strMarkType},
                {"checkWithClassMovement", "checkWithClassMovement"},
                 {"Year", academicYear.Year}
            };
            List<JudgeRecordBO> lstJudgeRecord = (from p in VJudgeRecordBusiness.SearchBySchool(entity.SchoolID, dicJudgeRecord)
                                                  join q in MarkTypeBusiness.All on p.MarkTypeID equals q.MarkTypeID
                                                  select new JudgeRecordBO
                                                  {
                                                      PupilID = p.PupilID,
                                                      ClassID = p.ClassID,
                                                      SubjectID = p.SubjectID,
                                                      Title = q.Title,
                                                      Coefficient = q.Coefficient,
                                                      Judgement = p.Judgement,
                                                      Semester = p.Semester,
                                                      OrderNumber = p.OrderNumber
                                                  })
                .ToList();

            //Lấy danh sách thông tin điểm tổng kết
            Dictionary<string, object> dicSummedUpRecord = new Dictionary<string, object> 
            {
                {"AcademicYearID", entity.AcademicYearID}, 
                {"SchoolID", entity.SchoolID},
                {"EducationLevelID", entity.EducationLevelID},
                {"ClassID", entity.ClassID},
                {"PeriodID", entity.PeriodDeclarationID}
            };
            List<SummedUpRecordBO> lstSummedUpRecord = VSummedUpRecordBusiness.SearchBySchool(entity.SchoolID, dicSummedUpRecord)
                .Select(o => new SummedUpRecordBO
                {
                    PupilID = o.PupilID,
                    ClassID = o.ClassID,
                    PeriodID = o.PeriodID,
                    SubjectID = o.SubjectID,
                    JudgementResult = o.JudgementResult,
                    SummedUpMark = o.SummedUpMark,
                    Semester = o.Semester
                })
                .ToList();

            //Tạo các sheet theo lớp
            Dictionary<ClassProfileTempBO, IVTWorksheet> dicSheet = new Dictionary<ClassProfileTempBO, IVTWorksheet>();
            foreach (ClassProfileTempBO cp in lstClass)
            {
                IVTWorksheet sheet = oBook.CopySheetToBeforeLast(firstSheet);
                sheet.Name = Utils.StripVNSignAndSpace(cp.DisplayName);
                dicSheet.Add(cp, sheet);
            }

            List<int> lstSectionKeyID = new List<int>();
            //Fill dữ liệu vào các lớp
            foreach (ClassProfileTempBO cp in lstClass)
            {
                int rowTotal = 40;
                int increment = 0;
                IVTWorksheet sheet = dicSheet[cp];
                lstSectionKeyID = cp.SectionKey.HasValue ? UtilsBusiness.GetListSectionID(cp.SectionKey.Value) : new List<int>();
                //Lấy danh sách môn học trong lớp
                var listCS = (from s in lstSubject
                              where s.ClassID == cp.ClassProfileID
                              select s)
                              .OrderBy(x => x.OrderInSubject).ThenBy(o => o.DisplayName)
                              .ToList();

                List<int> lstIncreaseID = listCS.Where(o => o.SubjectIDCrease != 0).Select(o => o.SubjectIDCrease).ToList();
                listCS = listCS.Where(o => !lstIncreaseID.Contains(o.SubjectID)).ToList();

                //Lấy danh sách học sinh trong lớp
                List<PupilOfClassBO> lstTMP = new List<PupilOfClassBO>();
                lstTMP = lstPupil.Where(p => p.ClassID == cp.ClassProfileID).ToList();
                DateTime? endSemester = entity.Semester == SystemParamsInFile.SEMESTER_OF_YEAR_FIRST ? academicYear.FirstSemesterEndDate : academicYear.SecondSemesterEndDate;
                if (entity.Semester == GlobalConstants.SEMESTER_OF_YEAR_FIRST)
                {
                    lstTMP = lstTMP.Where(x => x.AssignedDate <= academicYear.FirstSemesterEndDate
                        && ((x.Status == SystemParamsInFile.PUPIL_STATUS_STUDYING || x.Status == SystemParamsInFile.PUPIL_STATUS_GRADUATED)
                            || x.EndDate > endSemester)).ToList();
                }
                else if (entity.Semester == GlobalConstants.SEMESTER_OF_YEAR_SECOND)
                {
                    lstTMP = lstTMP.Where(x => x.AssignedDate <= academicYear.SecondSemesterEndDate && (x.Status == SystemParamsInFile.PUPIL_STATUS_STUDYING || x.Status == SystemParamsInFile.PUPIL_STATUS_GRADUATED)
                            || (x.Status == SystemParamsInFile.PUPIL_STATUS_LEAVED_OFF && x.EndDate >= endSemester)).ToList();
                }
                var listPP = lstTMP.OrderBy(x => x.OrderInClass).ThenBy(x => x.Name).ThenBy(x => x.PupilFullName).ToList();


                //Lấy danh sách thông tin học sinh vắng mặt trong lớp
                var listPA = (from s in lstPupilAbsence
                              where s.ClassID == cp.ClassProfileID
                              && lstSectionKeyID.Contains(s.Section)
                              select s).ToList();

                //Lấy danh sách thông tin học sinh vi phạm trong lớp
                var listPF = (from s in lstPupilFault
                              where s.ClassID == cp.ClassProfileID
                              select s).ToList();

                //Lấy danh sách thông tin đánh giá học lực trong lớp
                var listPR = (from s in lstPupilRanking
                              where s.ClassID == cp.ClassProfileID
                              select s).ToList();

                //Lấy danh sách thông tin điểm môn tính điếm trong lớp
                var listMR = (from s in lstMarkRecord
                              where s.ClassID == cp.ClassProfileID
                              select s).ToList();

                //Lấy danh sách thông tin điểm môn nhận xét trong lớp
                var listJR = (from s in lstJudgeRecord
                              where s.ClassID == cp.ClassProfileID
                              select s).ToList();

                //Lấy danh sách thông tin điểm tổng kết trong lớp
                var listSR = (from s in lstSummedUpRecord
                              where s.ClassID == cp.ClassProfileID
                              select s).ToList();

                //Fill dữ liệu môn học 
                if (listCS.Count > 14)
                {
                    //Nếu số môn nhiều hơn 14 thì chỉnh lại template insert thêm các bản ghi, chỉnh lại kích thước cho phù hợp
                    for (int i = 14; i < listCS.Count; i++)
                    {
                        sheet.CopyAndInsertARow(22, 21);
                    }
                    //Tính lại số dòng cho 1 trang
                    increment = listCS.Count - 14;
                    rowTotal += increment;
                    //Tính lại độ cao của Row cho phù hợp
                    double oldHeight = sheet.GetRowHeight(12);
                    double currentHeight = oldHeight * 14 / listCS.Count;
                    for (int i = 12; i < 12 + listCS.Count; i++)
                    {
                        sheet.SetRowHeight(i, currentHeight);
                    }
                }

                //Fill dữ liệu chung của lớp
                FillClassInfoForPeriodReport(0, cp, increment, sheet, listCS);
                if (reportCode == SystemParamsInFile.REPORT_PHIEU_BAO_DIEM_THEO_DOT_A4)
                {
                    FillClassInfoForPeriodReport(12, cp, increment, sheet, listCS);
                    //sheet.FitToPage = true;
                }

                if (reportCode == SystemParamsInFile.REPORT_PHIEU_BAO_DIEM_THEO_DOT_A5)
                {
                    //Tạo các vùng thông tin cho từng học sinh
                    IVTRange template = sheet.GetRange("A1", "K" + rowTotal);
                    sheet.PageMaginLeft = 0.4;
                    sheet.PageMaginRight = 0.4;
                    sheet.PageMaginTop = 0.2;
                    sheet.PageMaginBottom = 0.2;
                    sheet.PageSize = VTXPageSize.VTxlPaperA5;
                    sheet.FitToPage = true;
                    for (int i = 1; i < listPP.Count; i++)
                    {
                        int nextFirstRow = rowTotal * i + 1;
                        sheet.CopyPasteSameRowHeigh(template, nextFirstRow);
                        sheet.SetBreakPage(nextFirstRow);
                    }

                    sheet.SetPrintArea("$A$1:$K" + rowTotal * Math.Max(1, listPP.Count));
                    //sheet.PrintArea = ("$A$1:$K" + rowTotal * Math.Max(1, listPP.Count));


                    //Fill dữ liệu cho từng học sinh
                    for (int indexOfClass = 0; indexOfClass < listPP.Count; indexOfClass++)
                    {
                        PupilOfClassBO pp = listPP[indexOfClass];
                        int startRow = rowTotal * indexOfClass;
                        int startColumn = 0;
                        //Fill dữ liệu học sinh
                        FillPupilInfoForPeriodReport(increment, sheet, listCS, listPA, listPF, listPR, listMR,
                            listJR, listSR, pp, startRow, startColumn);
                    }

                }
                else
                {
                    //Tạo các vùng thông tin cho từng học sinh
                    IVTRange template = sheet.GetRange("A1", "W" + rowTotal);
                    int pageCount = listPP.Count / 2;
                    sheet.PageMaginLeft = 0.2;
                    sheet.PageMaginRight = 0.2;
                    sheet.PageMaginTop = 0.2;
                    sheet.PageMaginBottom = 0.2;
                    sheet.PageSize = VTXPageSize.VTxlPaperA4;
                    sheet.FitToPage = true;
                    for (int i = 1; i < pageCount; i++)
                    {
                        int nextFirstRow = rowTotal * i + 1;
                        sheet.CopyPasteSameRowHeigh(template, nextFirstRow);
                        sheet.SetBreakPage(nextFirstRow);
                    }
                    if (listPP.Count % 2 == 1)
                    {
                        if (listPP.Count > 2)
                        {
                            IVTRange templateForSinglePupil = sheet.GetRange("A1", "K" + rowTotal);
                            int nextFirstRow = rowTotal * pageCount + 1;
                            sheet.CopyPasteSameRowHeigh(templateForSinglePupil, nextFirstRow);
                            sheet.SetBreakPage(nextFirstRow);
                        }
                        pageCount++;
                    }

                    //sheet.SetPrintArea("$A$1:$W" + rowTotal * Math.Max(1, pageCount));
                    //sheet.PrintArea = ("$A$1:$W" + rowTotal * Math.Max(1, pageCount));

                    //Fill dữ liệu cho từng học sinh
                    for (int indexOfPupil = 0; indexOfPupil < listPP.Count; indexOfPupil++)
                    {
                        PupilOfClassBO pp = listPP[indexOfPupil];
                        int startRow = rowTotal * (indexOfPupil / 2);
                        int startColumn = (indexOfPupil % 2) * 12;
                        //Fill dữ liệu học sinh
                        FillPupilInfoForPeriodReport(increment, sheet, listCS, listPA, listPF, listPR, listMR,
                           listJR, listSR, pp, startRow, startColumn);
                    }
                }
            }

            //Xoá sheet template
            firstSheet.Delete();

            return oBook.ToStream();
        }

        private static void FillPupilInfoForPeriodReport(int increment, IVTWorksheet sheet, List<ClassSubjectBO> listCS,
            List<PupilAbsenceBO> listPA, List<PupilFaultBO> listPF, List<PupilRankingBO> listPR,
            List<MarkRecordBO> listMR, List<JudgeRecordBO> listJR, List<SummedUpRecordBO> listSR, PupilOfClassBO pp, int startRow, int startColumn)
        {

            //Khởi tạo các dữ liệu mặc định
            VTVector defaultVector = new VTVector(startRow, startColumn);
            VTVector cellPupilCode = new VTVector(8, 3) + defaultVector;
            VTVector cellPupilName = new VTVector(9, 3) + defaultVector;
            VTVector cellAbsenceHasReason = new VTVector(24 + increment, 11) + defaultVector;
            VTVector cellAbsenceNoReason = new VTVector(25 + increment, 11) + defaultVector;
            VTVector cellFault = new VTVector(26 + increment, 2) + defaultVector;
            VTVector cellAverageMark = new VTVector(12, 9) + defaultVector;
            VTVector cellCapacityLevel = new VTVector(12, 10) + defaultVector;
            VTVector cellRank = new VTVector(12, 11) + defaultVector;
            VTVector cellConductLevel = new VTVector(14, 9) + defaultVector;
            VTVector cellCoefficient1 = new VTVector(12, 2) + defaultVector;
            VTVector cellCoefficient2 = new VTVector(12, 5) + defaultVector;
            VTVector cellSummedUp = new VTVector(12, 8) + defaultVector;
            //VTVector cellHeadMasterName = new VTVector(21, 1) + defaultVector;
            //sheet.SetCellValue(cellHeadMasterName, "=Thongtinchung!B6");


            //Fill dữ liệu Mã học sinh
            sheet.SetCellValue(cellPupilCode, pp.PupilCode);

            //Fill dữ liệu Tên học sinh
            sheet.SetCellValue(cellPupilName, pp.PupilFullName);

            //Fill dữ liệu nghỉ học
            int absenceHasReason = (from s in listPA
                                    where s.IsAccepted == true && s.PupilID == pp.PupilID
                                    select s.IsAccepted).Count();
            int absenceNoReason = (from s in listPA
                                   where s.IsAccepted == false && s.PupilID == pp.PupilID
                                   select s.IsAccepted).Count();
            sheet.SetCellValue(cellAbsenceHasReason, absenceHasReason);
            sheet.SetCellValue(cellAbsenceNoReason, absenceNoReason);

            //Fill dữ liệu thông tin vi phạm
            var pupilFault = (from s in listPF.Where(o => o.PupilID == pp.PupilID)
                              group s by new { s.FaultID, s.Resolution } into g
                              select new { FaultID = g.Key.FaultID, Resolution = g.Key.Resolution, TotalNumberFault = g.Sum(o => o.NumberOfFault) }).ToList();
            string pupilFaultDescription = "";
            foreach (var pf in pupilFault)
            {
                if (pf.FaultID > 0 && pf.TotalNumberFault > 0)
                {
                    pupilFaultDescription += pf.Resolution + " (" + pf.TotalNumberFault + "), ";
                }
            }
            if (pupilFaultDescription != "")
            {
                pupilFaultDescription = pupilFaultDescription.Substring(0, pupilFaultDescription.Length - 2);
            }
            sheet.SetCellValue(cellFault, pupilFaultDescription);

            //Fill dữ liệu đánh giá học lực
            List<PupilRankingBO> lPR = (from s in listPR
                                        where s.PupilID == pp.PupilID
                                        select s).ToList();
            string comment = string.Empty;
            if (lPR != null && lPR.Count > 0)
            {
                PupilRankingBO pr = lPR[0];
                comment = pr.Comment;
                sheet.SetCellValue(cellAverageMark, pr.AverageMark);
                sheet.SetCellValue(cellCapacityLevel, pr.CapacityLevel != null ? pr.CapacityLevel : "");
                sheet.SetCellValue(cellRank, pr.Rank);
                sheet.SetCellValue(cellConductLevel, pr.ConductLevelID != null ? pr.ConductLevelName : "");
            }

            //Fill dữ liệu điểm 
            for (int indexOfSubject = 0; indexOfSubject < listCS.Count; indexOfSubject++)
            {
                ClassSubjectBO subject = listCS[indexOfSubject];
                VTVector indexVector = new VTVector(indexOfSubject, 0);
                //Fill dữ liệu điểm hệ số 1 và hệ số 2
                if (subject.IsCommenting == SystemParamsInFile.ISCOMMENTING_TYPE_MARK ||
                    subject.IsCommenting == SystemParamsInFile.ISCOMMENTING_TYPE_MARK_JUDGE)
                {
                    //Lấy dữ liệu điểm hệ số 1
                    List<MarkRecordBO> lstCoefficient1 = (from s in listMR
                                                          where s.PupilID == pp.PupilID
                                                          && s.SubjectID == subject.SubjectID
                                                          && s.Coefficient == 1
                                                          select s).OrderBy(x => x.Title).ThenBy(x => x.OrderNumber).ToList();
                    string coefficient1 = "";
                    foreach (MarkRecordBO mr in lstCoefficient1)
                    {
                        coefficient1 += ReportUtils.ConvertMarkForSecondaryLevel(mr.Mark.Value) + " ";
                    }
                    if (coefficient1.Length > 0)
                    {
                        coefficient1 = coefficient1.Substring(0, coefficient1.Length - 1);
                    }
                    sheet.SetCellValue(cellCoefficient1 + indexVector, coefficient1);

                    //Lấy dữ liệu điểm hệ số 2
                    List<MarkRecordBO> lstCoefficient2 = (from s in listMR
                                                          where s.PupilID == pp.PupilID
                                                          && s.SubjectID == subject.SubjectID
                                                          && s.Coefficient == 2
                                                          select s).OrderBy(x => x.Title).ThenBy(x => x.OrderNumber).ToList();
                    string coefficient2 = "";
                    foreach (MarkRecordBO mr in lstCoefficient2)
                    {
                        coefficient2 += ReportUtils.ConvertMarkForSecondaryLevel(mr.Mark.Value) + " ";
                    }
                    if (coefficient2.Length > 0)
                    {
                        coefficient2 = coefficient2.Substring(0, coefficient2.Length - 1);
                    }
                    sheet.SetCellValue(cellCoefficient2 + indexVector, coefficient2);
                }
                else
                {
                    //Lấy dữ liệu điểm hệ số 1
                    List<JudgeRecordBO> lstCoefficient1 = (from s in listJR
                                                           where s.PupilID == pp.PupilID
                                                           && s.SubjectID == subject.SubjectID
                                                           && s.Coefficient == 1
                                                           select s).OrderBy(x => x.Title).ThenBy(x => x.OrderNumber).ToList();
                    string coefficient1 = "";
                    foreach (JudgeRecordBO jr in lstCoefficient1)
                    {
                        coefficient1 += jr.Judgement + " ";
                    }
                    if (coefficient1.Length > 0)
                    {
                        coefficient1 = coefficient1.Substring(0, coefficient1.Length - 1);
                    }
                    sheet.SetCellValue(cellCoefficient1 + indexVector, coefficient1);

                    //Lấy dữ liệu điểm hệ số 2
                    List<JudgeRecordBO> lstCoefficient2 = (from s in listJR
                                                           where s.PupilID == pp.PupilID
                                                           && s.SubjectID == subject.SubjectID
                                                           && s.Coefficient == 2
                                                           select s).OrderBy(x => x.Title).ThenBy(x => x.OrderNumber).ToList();
                    string coefficient2 = "";
                    foreach (JudgeRecordBO jr in lstCoefficient2)
                    {
                        coefficient2 += jr.Judgement + " ";
                    }
                    if (coefficient2.Length > 0)
                    {
                        coefficient2 = coefficient2.Substring(0, coefficient2.Length - 1);
                    }
                    sheet.SetCellValue(cellCoefficient2 + indexVector, coefficient2);
                }

                //Fill dữ liệu điểm trung bình môn
                List<SummedUpRecordBO> lSR = (from s in listSR
                                              where s.PupilID == pp.PupilID
                                              && s.SubjectID == subject.SubjectID
                                              select s).ToList();
                if (lSR != null && lSR.Count > 0)
                {

                    if (subject.IsCommenting == SystemParamsInFile.ISCOMMENTING_TYPE_MARK ||
                        subject.IsCommenting == SystemParamsInFile.ISCOMMENTING_TYPE_MARK_JUDGE)
                    {
                        string summedUpMark = lSR[0].SummedUpMark != null ? ReportUtils.ConvertMarkForV(lSR[0].SummedUpMark.Value) : string.Empty;
                        sheet.SetCellValue(cellSummedUp + indexVector, summedUpMark);
                    }
                    else
                    {
                        string judgementResult = lSR[0].JudgementResult;
                        sheet.SetCellValue(cellSummedUp + indexVector, judgementResult);
                    }
                }
            }
            VTVector cellCommentTeacher = new VTVector(30 + increment, 6) + defaultVector;
            if (!string.IsNullOrEmpty(comment))
            {
                sheet.SetCellValue(cellCommentTeacher, comment);
                if (comment.Length > 50)
                {
                    sheet.GetRange(startRow + 30 + increment, startColumn + 6, startRow + 34 + increment, startColumn + 6).Merge();
                    sheet.GetRange(startRow + 30 + increment, startColumn + 6, startRow + 30 + increment, startColumn + 6).WrapText();
                    sheet.GetRange(startRow + 30 + increment, startColumn + 6, startRow + 30 + increment, startColumn + 6).SetHAlign(VTHAlign.xlHAlignLeft);
                    sheet.GetRange(startRow + 30 + increment, startColumn + 6, startRow + 30 + increment, startColumn + 6).SetHAlignVer(NativeExcel.XlVAlign.xlVAlignTop);
                }
            }

        }

        private void FillClassInfoForPeriodReport(int startColumn, ClassProfileTempBO cp, int increment, IVTWorksheet sheet, List<ClassSubjectBO> listCS)
        {
            //Khởi tạo các dữ liệu mặc định
            VTVector defaultVector = new VTVector(0, startColumn);
            VTVector cellClass = new VTVector(8, 10) + defaultVector;
            VTVector cellHeadTeacher = new VTVector(39 + increment, 6) + defaultVector;
            VTVector cellSubject = new VTVector(12, 1) + defaultVector;

            //Fill dữ liệu môn học
            for (int i = 0; i < listCS.Count; i++)
            {
                sheet.SetCellValue(cellSubject + new VTVector(i, 0), listCS[i].DisplayName);
            }

            //Fill dữ liệu lớp học
            sheet.SetCellValue(cellClass, "Lớp: " + cp.DisplayName);

            //Fill dữ liệu giáo viên chủ nhiệm
            if (cp.EmployeeID != null && cp.EmployeeID > 0)
            {
                sheet.SetCellValue(cellHeadTeacher, cp.EmployeeName);
            }
        }
        #endregion

        #region Lấy thông tin chung của báo cáo theo kỳ
        public ReportDefinition GetReportDefinitionOfSemesterReport(PointReportBO entity)
        {
            string reportCode = GetSemesterReportCode(entity);
            return ReportDefinitionBusiness.GetByCode(reportCode);
        }
        #endregion

        #region Tạo mảng băm cho các tham số đầu vào của phiếu báo điểm theo kỳ
        public string GetHashKeyForSemester(PointReportBO entity)
        {
            IDictionary<string, object> dic = new Dictionary<string, object> {
                {"AcademicYearID", entity.AcademicYearID},
                {"EducationLevelID ", entity.EducationLevelID},
                {"ClassID", entity.ClassID},
                {"PaperSize", entity.PaperSize},
                {"SchoolID", entity.SchoolID},
                {"AppliedLevel", entity.AppliedLevel},
                {"Semester", entity.Semester},
                {"UserAccountID", entity.UserAccountID}
            };
            return ReportUtils.GetHashKey(dic);
        }
        #endregion

        #region Lấy phiếu báo điểm theo kỳ được cập nhật mới nhất
        public ProcessedReport GetPointReportBySemester(PointReportBO entity)
        {
            string reportCode = GetSemesterReportCode(entity);
            string inputParameterHashKey = GetHashKeyForSemester(entity);
            return ProcessedReportBusiness.GetProcessedReport(reportCode, inputParameterHashKey);
        }
        #endregion

        #region Lưu báo cáo điểm theo kỳ vào CSDL
        public ProcessedReport InsertPointReportBySemester(PointReportBO entity, Stream data)
        {
            string reportCode = GetSemesterReportCode(entity);
            ReportDefinition reportDef = ReportDefinitionBusiness.GetByCode(reportCode);
            ProcessedReport pr = new ProcessedReport();
            pr.ReportCode = reportCode;
            pr.ProcessedDate = DateTime.Now;
            pr.InputParameterHashKey = GetHashKeyForSemester(entity);
            pr.ReportData = ReportUtils.Compress(data);

            //Tạo tên file
            string outputNamePattern = reportDef.OutputNamePattern;
            string schoolLevel = ReportUtils.ConvertAppliedLevelForReportName(entity.AppliedLevel);
            string educationLevelOrClass = (entity.ClassID == 0) ?
                EducationLevelRepository.Find(entity.EducationLevelID).Resolution : ClassProfileBusiness.Find(entity.ClassID).DisplayName;
            string semester = ReportUtils.ConvertSemesterForReportName(entity.Semester);

            outputNamePattern = outputNamePattern.Replace("[SchoolLevel]", schoolLevel);
            outputNamePattern = outputNamePattern.Replace("[EducationLevel/Class]", ReportUtils.StripVNSign(educationLevelOrClass));
            outputNamePattern = outputNamePattern.Replace("[Semester]", semester);

            if (entity.ReportPatternID == 1) // Mẫu cũ
            {
                pr.ReportName = ReportUtils.RemoveSpecialCharacters(outputNamePattern) + "." + reportDef.OutputFormat;
            }
            else // Mẫu mới
            {
                pr.ReportName = ReportUtils.RemoveSpecialCharacters(outputNamePattern).Replace("THCS", schoolLevel) + "." + reportDef.OutputFormat;
            }

            IDictionary<string, object> dic = new Dictionary<string, object> {
                {"AcademicYearID", entity.AcademicYearID},
                {"EducationLevelID ", entity.EducationLevelID},
                {"ClassID", entity.ClassID},
                {"PaperSize", entity.PaperSize},
                {"SchoolID", entity.SchoolID},
                {"AppliedLevel", entity.AppliedLevel},
                {"Semester", entity.Semester},
                {"UserAccountID", entity.UserAccountID}
            };
            ProcessedReportParameterRepository.Insert(dic, pr);
            ProcessedReportRepository.Insert(pr);
            ProcessedReportRepository.Save();
            return pr;
        }
        #endregion

        #region Tạo file phiếu báo điểm theo kỳ

        private int soCotDiemMieng = 5;
        private int soCotDiem15Phut = 5;
        private int soCotDiemKiemTra = 5;


        /// <summary>
        /// Tạo file phiếu báo điểm theo kỳ
        /// </summary>
        /// <param name="entity"></param>
        /// <returns></returns>
        public Stream CreatePointReportBySemester(PointReportBO entity
                , List<MarkRecordBO> lstMarkRecord
                , List<JudgeRecordBO> lstJudgeRecord
                , List<SummedUpRecordBO> lstSummedUpRecord)
        {


            SchoolProfile school = SchoolProfileBusiness.Find(entity.SchoolID);
            AcademicYear academicYear = AcademicYearRepository.Find(entity.AcademicYearID);



            //Lấy danh sách các lớp cần in phiếu báo điểm
            IQueryable<ClassProfile> lstClassTemp = ClassProfileBusiness.getClassByAccountRole(entity.AcademicYearID, entity.SchoolID, entity.EducationLevelID, entity.ClassID, entity.UserAccountID, entity.IsSuperVisingDept, entity.IsSubSuperVisingDept);
            List<ClassProfileTempBO> lstClass = (from p in lstClassTemp
                                                 join q in EmployeeBusiness.All on p.HeadTeacherID equals q.EmployeeID into g
                                                 from j1 in g.DefaultIfEmpty()
                                                 select new ClassProfileTempBO
                                                 {
                                                     ClassProfileID = p.ClassProfileID,
                                                     SchoolID = p.SchoolID,
                                                     AcademicYearID = p.AcademicYearID,
                                                     OrderNumber = p.OrderNumber,
                                                     EducationLevelID = p.EducationLevelID,
                                                     DisplayName = p.DisplayName,
                                                     EmployeeID = j1.EmployeeID,
                                                     EmployeeName = j1.FullName,
                                                     SectionKey = p.SeperateKey
                                                 }).OrderBy(o => o.OrderNumber).ToList();

            //Lấy danh sách các môn học cho các lớp
            Dictionary<string, object> dicSubject = new Dictionary<string, object> 
            {
                {"AcademicYearID", entity.AcademicYearID}, 
                {"EducationLevelID", entity.EducationLevelID},
                {"ClassID", entity.ClassID},
                {"AppliedLevel", entity.AppliedLevel}
            };
            IQueryable<ClassSubject> lstSubjectTemp = ClassSubjectBusiness.SearchBySchool(entity.SchoolID, dicSubject);
            List<ClassSubjectBO> lstSubject = (from p in lstSubjectTemp
                                               join s in SubjectCatBusiness.All on p.SubjectID equals s.SubjectCatID
                                               select new ClassSubjectBO
                                               {
                                                   SubjectID = p.SubjectID,
                                                   ClassID = p.ClassID,
                                                   SubjectName = s.SubjectName,
                                                   DisplayName = s.DisplayName,
                                                   OrderInSubject = s.OrderInSubject,
                                                   IsCommenting = p.IsCommenting,
                                                   SubjectIDCrease = p.SubjectIDIncrease != null ? p.SubjectIDIncrease.Value : 0
                                               }).ToList();

            //Lấy danh sách học sinh
            Dictionary<string, object> dicPupil = new Dictionary<string, object> 
            {
                {"AcademicYearID", entity.AcademicYearID}, 
                {"EducationLevelID", entity.EducationLevelID},
                {"ClassID", entity.ClassID},
                {"Check", "check"}
            };
            // Lấy thông tin học sinh theo khối, lớp
            // Lấy luôn ra PupilProfile và OrderInClass để sắp xếp và tăng hiệu năng
            var lstPupil = (from poc in PupilOfClassBusiness.SearchBySchool(entity.SchoolID, dicPupil)
                .AddCriteriaSemester(academicYear, entity.Semester)
                            join pp in PupilProfileBusiness.All on poc.PupilID equals pp.PupilProfileID
                            select new PupilOfClassBO
                            {
                                PupilID = poc.PupilID,
                                PupilCode = pp.PupilCode,
                                PupilFullName = pp.FullName,
                                Name = pp.Name,
                                ClassID = poc.ClassID,
                                OrderInClass = poc.OrderInClass
                            })
                .ToList();

            //Lấy danh sách thông tin học sinh nghỉ học
            Dictionary<string, object> dicPupilAbsence = new Dictionary<string, object> 
            {
                {"SchoolID",entity.SchoolID},
                {"AcademicYearID", entity.AcademicYearID},
                {"ClassID", entity.ClassID},
                {"SemesterID",entity.Semester == SystemParamsInFile.SEMESTER_OF_YEAR_FIRST ? SystemParamsInFile.SEMESTER_OF_YEAR_FIRST : SystemParamsInFile.SEMESTER_OF_YEAR_ALL},
                {"EducationLevelID",entity.EducationLevelID},
                {"FromAbsentDate",academicYear.FirstSemesterStartDate}
            };
            if (entity.Semester == SystemParamsInFile.SEMESTER_OF_YEAR_FIRST)
            {
                dicPupilAbsence["ToAbsentDate"] = academicYear.FirstSemesterEndDate;
            }
            else
            {
                dicPupilAbsence["ToAbsentDate"] = academicYear.SecondSemesterEndDate;
            }
            List<PupilAbsenceBO> lstPupilAbsence = PupilAbsenceBusiness.GetListPupilAbsenceBySection(dicPupilAbsence, academicYear)
                .Select(o => new PupilAbsenceBO { PupilID = o.PupilID, ClassID = o.ClassID, AbsentDate = o.AbsentDate, IsAccepted = o.IsAccepted, Section = o.Section })
                .ToList();

            //Lấy danh sách thông tin đánh giá học lực
            Dictionary<string, object> dicPupilRanking = new Dictionary<string, object> 
            {
                {"AcademicYearID", entity.AcademicYearID}, 
                {"SchoolID", entity.SchoolID},
                {"EducationLevelID", entity.EducationLevelID},
                {"ClassID", entity.ClassID}
            };
            if (entity.Semester == SystemParamsInFile.SEMESTER_OF_YEAR_FIRST)
            {
                dicPupilRanking.Add("Semester", entity.Semester);
            }
            List<PupilRankingBO> lstPupilRanking = (from p in VPupilRankingBusiness.SearchBySchool(entity.SchoolID, dicPupilRanking)
                                                    join q in CapacityLevelBusiness.All on p.CapacityLevelID equals q.CapacityLevelID into g1
                                                    from j1 in g1.DefaultIfEmpty()
                                                    join r in ConductLevelBusiness.All on p.ConductLevelID equals r.ConductLevelID into g2
                                                    from j2 in g2.DefaultIfEmpty()
                                                    select new PupilRankingBO
                                                    {
                                                        PupilRankingID = p.PupilRankingID,
                                                        PupilID = p.PupilID,
                                                        EducationLevelID = p.EducationLevelID,
                                                        ClassID = p.ClassID,
                                                        Semester = p.Semester,
                                                        ConductLevelID = p.ConductLevelID,
                                                        CapacityLevelID = p.CapacityLevelID,
                                                        ConductLevelName = j2.Resolution,
                                                        CapacityLevel = j1.CapacityLevel1,
                                                        AverageMark = p.AverageMark,
                                                        Comment = p.PupilRankingComment,
                                                        Rank = p.Rank
                                                    }).ToList();

            //Lấy danh sách thông tin danh hiệu học sinh
            Dictionary<string, object> dicPupilEmulation = new Dictionary<string, object> 
            {
                {"AcademicYearID", entity.AcademicYearID}, 
                {"EducationLevelID", entity.EducationLevelID},
                {"ClassID", entity.ClassID}
            };
            List<PupilEmulation> lstPupilEmulation = PupilEmulationBusiness.SearchBySchool(entity.SchoolID, dicPupilEmulation).Include("HonourAchivementType").ToList();

            //////////////////////////////////////////////////////////////////////////////////////////////////////////////////
            // Lay so cot diem mieng, 15 phut, viet trong hoc ky
            IDictionary<string, object> dicM = new Dictionary<string, object>();
            dicM["AcademicYearID"] = entity.AcademicYearID;
            dicM["SchoolID"] = entity.SchoolID;
            dicM["Semester"] = entity.Semester;
            //dicM["Year"] = AcademicYear.Year;

            SemeterDeclaration SemeterDeclaration = SemeterDeclarationBusiness.Search(dicM).Where(u => u.AppliedDate <= DateTime.Now).OrderByDescending(u => u.AppliedDate).FirstOrDefault();
            if (SemeterDeclaration == null) throw new BusinessException("Validate_School_NotMark");

            soCotDiemMieng = SemeterDeclaration.InterviewMark;
            soCotDiem15Phut = SemeterDeclaration.WritingMark;
            soCotDiemKiemTra = SemeterDeclaration.TwiceCoeffiecientMark;
            //////////////////////////////////////////////////////////////////////////////////////////////////////////////////

            //fill dữ liệu vào sheet thông tin chung
            string reportCode;
            if (entity.Semester == SystemParamsInFile.SEMESTER_OF_YEAR_FIRST)
            {
                reportCode = (entity.PaperSize == SystemParamsInFile.PAPER_SIZE_A4) ?
                SystemParamsInFile.REPORT_PHIEU_BAO_DIEM_THEO_KY_I_A4 : SystemParamsInFile.REPORT_PHIEU_BAO_DIEM_THEO_KY_I_A5;
            }
            else
            {
                reportCode = (entity.PaperSize == SystemParamsInFile.PAPER_SIZE_A4) ?
                SystemParamsInFile.REPORT_PHIEU_BAO_DIEM_THEO_KY_II_A4 : SystemParamsInFile.REPORT_PHIEU_BAO_DIEM_THEO_KY_II_A5;
            }
            ReportDefinition reportDef = ReportDefinitionBusiness.GetByCode(reportCode);
            if (soCotDiemKiemTra >= 6) // Neu so cot viet >= 6 thi lay  template co 6 cot viet. Neu khong thi lay template binh thuong
            {
                reportDef.TemplateName = reportDef.TemplateName.Replace(".xls", "_6CotViet.xls");
            }
            string templatePath = SystemParamsInFile.TEMPLATE_FOLDER + reportDef.TemplateDirectory + "/" + reportDef.TemplateName;
            IVTWorkbook oBook = VTExport.OpenWorkbook(templatePath);

            //Lấy sheet phiếu điểm
            IVTWorksheet firstSheet = oBook.GetSheet(1);

            //Lấy sheet thông tin chung
            IVTWorksheet GeneralInfoSheet = oBook.GetSheet(2);

            string supervisingDeptName = UtilsBusiness.GetSupervisingDeptName(school.SchoolProfileID, entity.AppliedLevel).ToUpper();
            string schoolName = school.SchoolName.ToUpper();
            string semesterName = SMASConvert.ConvertSemester(entity.Semester).ToUpper() + ", " +
                academicYear.Year + " - " + (academicYear.Year + 1);
            string dateSignature = (school.District != null ? school.District.DistrictName : "") + ", " + ReportUtils.ConvertFullDate(DateTime.Now);

            GeneralInfoSheet.SetCellValue("B2", supervisingDeptName); // Ten don vi quan lý
            GeneralInfoSheet.SetCellValue("B3", schoolName); // Ten truong
            GeneralInfoSheet.SetCellValue("B5", semesterName); // Hoc ky
            //GeneralInfoSheet.SetCellValue("B6", school.HeadMasterName); // Xuat ten hieu truong
            GeneralInfoSheet.SetCellValue("B7", dateSignature); // Ngay ky
            GeneralInfoSheet.SetCellValue("B8", school.HeadMasterName);//Ten hieu truong

            //Tạo các sheet theo lớp
            Dictionary<ClassProfileTempBO, IVTWorksheet> dicSheet = new Dictionary<ClassProfileTempBO, IVTWorksheet>();
            foreach (ClassProfileTempBO cp in lstClass)
            {
                IVTWorksheet sheet = oBook.CopySheetToBeforeLast(firstSheet);
                sheet.Name = Utils.StripVNSignAndSpace(cp.DisplayName);
                dicSheet.Add(cp, sheet);
            }
            List<int> lstSectionKeyID = new List<int>();
            //Fill dữ liệu vào các lớp
            foreach (ClassProfileTempBO cp in lstClass)
            {
                IVTWorksheet sheet = dicSheet[cp];
                lstSectionKeyID = cp.SectionKey.HasValue ? UtilsBusiness.GetListSectionID(cp.SectionKey.Value) : new List<int>();
                //Lấy danh sách môn học trong lớp
                var listCS = (from s in lstSubject
                              where s.ClassID == cp.ClassProfileID
                              select s)
                              .OrderBy(x => x.OrderInSubject).ThenBy(o => o.DisplayName)
                              .ToList();

                List<int> lstIncreaseID = listCS.Where(o => o.SubjectIDCrease != 0).Select(o => o.SubjectIDCrease).ToList();
                listCS = listCS.Where(o => !lstIncreaseID.Contains(o.SubjectID)).ToList();

                //Lấy danh sách học sinh trong lớp
                var listPP = (from s in lstPupil
                              where s.ClassID == cp.ClassProfileID
                              select s).OrderBy(x => x.OrderInClass).ThenBy(x => x.Name).ThenBy(x => x.PupilFullName).ToList();

                //Lấy danh sách thông tin học sinh vắng mặt trong lớp
                var listPA = (from s in lstPupilAbsence
                              where s.ClassID == cp.ClassProfileID
                              && lstSectionKeyID.Contains(s.Section)
                              select s).ToList();

                //Lấy danh sách thông tin đánh giá học lực trong lớp
                var listPR = (from s in lstPupilRanking
                              where s.ClassID == cp.ClassProfileID
                              select s).ToList();

                //Lấy danh sách thông tin điểm môn tính điếm trong lớp
                var listMR = (from s in lstMarkRecord
                              where s.ClassID == cp.ClassProfileID
                              select s).ToList();

                //Lấy danh sách thông tin điểm môn nhận xét trong lớp
                var listJR = (from s in lstJudgeRecord
                              where s.ClassID == cp.ClassProfileID
                              select s).ToList();

                //Lấy danh sách thông tin điểm tổng kết trong lớp
                var listSR = (from s in lstSummedUpRecord
                              where s.ClassID == cp.ClassProfileID
                              select s).ToList();

                //Lấy danh sách thông tin danh hiệu học sinh
                var listPE = (from s in lstPupilEmulation
                              where s.ClassID == cp.ClassProfileID
                              select s).ToList();

                if (entity.Semester == SystemParamsInFile.SEMESTER_OF_YEAR_FIRST)
                {
                    int rowTotal = 38;
                    int increment = 0;
                    int maxSubjectRow = 12;
                    int shiftColumnRange = soCotDiemKiemTra < 6 ? 20 : 21;

                    if (reportCode == SystemParamsInFile.REPORT_PHIEU_BAO_DIEM_THEO_KY_I_A4)
                    {
                        rowTotal = 42;
                        maxSubjectRow = 16;
                    }

                    //Fill dữ liệu môn học 
                    for (int i = 1; i < listCS.Count; i++)
                    {
                        sheet.CopyAndInsertARow(12, 13);
                    }
                    //Số dòng tăng thêm
                    increment = listCS.Count - 1;

                    if (listCS.Count > maxSubjectRow)
                    {
                        //Tính lại độ cao của Row cho phù hợp
                        double oldHeight = sheet.GetRowHeight(12);
                        double currentHeight = oldHeight * maxSubjectRow / listCS.Count;
                        for (int i = 12; i < 12 + listCS.Count; i++)
                        {
                            sheet.SetRowHeight(i, currentHeight);
                        }

                        //Tính số dòng cho 1 trang
                        rowTotal += listCS.Count - maxSubjectRow;
                    }

                    //Thiết lập lại độ cao cho dòng cuối
                    sheet.SetRowHeight(rowTotal, sheet.GetRowHeight(1));

                    //Fill dữ liệu chung của lớp
                    FillClassInfoForFirstSemesterReport(0, cp, increment, sheet, listCS, school.HeadMasterName);
                    if (reportCode == SystemParamsInFile.REPORT_PHIEU_BAO_DIEM_THEO_KY_I_A4)
                    {
                        FillClassInfoForFirstSemesterReport(shiftColumnRange, cp, increment, sheet, listCS, school.HeadMasterName);
                    }

                    //Fill dữ liệu học sinh
                    if (reportCode == SystemParamsInFile.REPORT_PHIEU_BAO_DIEM_THEO_KY_I_A5)
                    {
                        //Tạo các vùng thông tin cho từng học sinh
                        IVTRange template = sheet.GetRange("A1", (soCotDiemKiemTra < 6 ? "S" : "T") + rowTotal); // Neu soCotDiemKiemTra >= 6 thi dung template co 6 cot diem kiem tra viet nen so cot keo dai den cot T
                        sheet.PageMaginLeft = 0.3;
                        sheet.PageMaginRight = 0.3;
                        sheet.PageMaginTop = 0.5;
                        sheet.PageMaginBottom = 0;
                        sheet.PageSize = VTXPageSize.VTxlPaperA5;

                        //Start 2014/12/12 viethd4 hot fix 0017005
                        //sheet.SetPrintArea("$A$1:$S$" + rowTotal * Math.Max(1, listPP.Count));
                        //sheet.PrintArea = ("$A$1:$S$" + rowTotal * Math.Max(1, listPP.Count));
                        sheet.SetPrintArea("$A$1:$" + (soCotDiemKiemTra < 6 ? "S" : "T") + "$" + rowTotal * Math.Max(1, listPP.Count));
                        sheet.PrintArea = ("$A$1:$" + (soCotDiemKiemTra < 6 ? "S" : "T") + "$" + rowTotal * Math.Max(1, listPP.Count));
                        //End: 2014/12/12 viethd4 hot fix 0017005
                        sheet.FitToPage = true;
                        for (int i = 1; i < listPP.Count; i++)
                        {
                            int nextFirstRow = rowTotal * i + 1;
                            sheet.CopyPasteSameRowHeigh(template, nextFirstRow);
                            sheet.SetBreakPage(nextFirstRow);
                        }

                        //Fill dữ liệu cho từng học sinh
                        for (int indexOfPupil = 0; indexOfPupil < listPP.Count; indexOfPupil++)
                        {
                            PupilOfClassBO pp = listPP[indexOfPupil];
                            int startRow = rowTotal * indexOfPupil;
                            int startColumn = 0;
                            //Fill dữ liệu học sinh
                            FillPupilInfoForFirstSemesterReport(increment, sheet, listCS, listPA, listPR, listMR,
                                listJR, listSR, listPE, pp, startRow, startColumn, soCotDiemMieng, soCotDiem15Phut, soCotDiemKiemTra);
                        }
                    }
                    else if (reportCode == SystemParamsInFile.REPORT_PHIEU_BAO_DIEM_THEO_KY_I_A4)
                    {
                        //Tạo các vùng thông tin cho từng học sinh
                        IVTRange template = sheet.GetRange("A1", (soCotDiemKiemTra < 6 ? "AM" : "AO") + rowTotal); // Neu soCotDiemKiemTra >= 6 thi dung template co 6 cot diem kiem tra viet nen so cot keo dai den cot AN
                        int pageCount = listPP.Count / 2;
                        sheet.PageMaginLeft = 0.45;
                        sheet.PageMaginRight = 0.45;
                        sheet.PageMaginTop = 0.5;
                        sheet.PageMaginBottom = 0;
                        sheet.PageSize = VTXPageSize.VTxlPaperA4;
                        sheet.FitToPage = true;
                        //sheet.SetPrintArea("$A$1:$AM$" + rowTotal * Math.Max(1, pageCount));
                        //sheet.PrintArea = ("$A$1:$AM$" + rowTotal * Math.Max(1, pageCount));
                        for (int i = 1; i < pageCount; i++)
                        {
                            int nextFirstRow = rowTotal * i + 1;
                            sheet.CopyPasteSameRowHeigh(template, nextFirstRow);
                            sheet.SetBreakPage(nextFirstRow);
                        }
                        if (listPP.Count % 2 == 1 && listPP.Count > 2)
                        {
                            if (listPP.Count > 2)
                            {
                                IVTRange templateForSinglePupil = sheet.GetRange("A1", (soCotDiemKiemTra < 6 ? "S" : "T") + rowTotal);
                                int nextFirstRow = rowTotal * pageCount + 1;
                                sheet.CopyPasteSameRowHeigh(templateForSinglePupil, nextFirstRow);
                                sheet.SetBreakPage(nextFirstRow);
                            }
                            pageCount++;
                        }
                        sheet.SetPrintArea("A1:" + (soCotDiemKiemTra < 6 ? "AM" : "AO") + rowTotal * Math.Max(1, pageCount));

                        //Fill dữ liệu cho từng học sinh
                        for (int indexOfPupil = 0; indexOfPupil < listPP.Count; indexOfPupil++)
                        {
                            PupilOfClassBO pp = listPP[indexOfPupil];
                            int startRow = rowTotal * (indexOfPupil / 2);
                            int startColumn = (indexOfPupil % 2) * shiftColumnRange;
                            //Fill dữ liệu học sinh
                            FillPupilInfoForFirstSemesterReport(increment, sheet, listCS, listPA, listPR, listMR,
                               listJR, listSR, listPE, pp, startRow, startColumn, soCotDiemMieng, soCotDiem15Phut, soCotDiemKiemTra);
                        }
                        //sheet.FitToPage = true;
                    }
                }
                else if (entity.Semester == SystemParamsInFile.SEMESTER_OF_YEAR_SECOND)
                {
                    int rowTotal = 41;
                    int increment = 0;
                    int maxSubjectRow = 14;
                    int shiftColumnRange = soCotDiemKiemTra < 6 ? 21 : 22;

                    if (reportCode == SystemParamsInFile.REPORT_PHIEU_BAO_DIEM_THEO_KY_II_A4)
                    {
                        rowTotal = 44;
                        maxSubjectRow = 17;
                    }

                    //Fill dữ liệu môn học 
                    for (int i = 1; i < listCS.Count; i++)
                    {
                        sheet.CopyAndInsertARow(12, 13);
                    }
                    //Số dòng tăng thêm
                    increment = listCS.Count - 1;

                    if (listCS.Count > maxSubjectRow)
                    {
                        //Tính lại độ cao của Row cho phù hợp
                        double oldHeight = sheet.GetRowHeight(12);
                        double currentHeight = oldHeight * maxSubjectRow / listCS.Count;
                        for (int i = 12; i < 12 + listCS.Count; i++)
                        {
                            sheet.SetRowHeight(i, currentHeight);
                        }

                        //Tính số dòng cho 1 trang
                        rowTotal += listCS.Count - maxSubjectRow;
                    }

                    //Thiết lập lại độ cao cho dòng cuối
                    sheet.SetRowHeight(rowTotal, sheet.GetRowHeight(1));

                    //Fill dữ liệu chung của lớp
                    FillClassInfoForSecondSemesterReport(0, cp, increment, sheet, listCS, school.HeadMasterName);
                    if (reportCode == SystemParamsInFile.REPORT_PHIEU_BAO_DIEM_THEO_KY_II_A4)
                    {
                        FillClassInfoForSecondSemesterReport(shiftColumnRange, cp, increment, sheet, listCS, school.HeadMasterName);
                    }

                    //Fill dữ liệu học sinh
                    if (reportCode == SystemParamsInFile.REPORT_PHIEU_BAO_DIEM_THEO_KY_II_A5)
                    {
                        //Tạo các vùng thông tin cho từng học sinh
                        IVTRange template = sheet.GetRange("A1", (soCotDiemKiemTra < 6 ? "T" : "U") + rowTotal);
                        sheet.PageMaginLeft = 0.3;
                        sheet.PageMaginRight = 0.3;
                        sheet.PageMaginTop = 0.5;
                        sheet.PageMaginBottom = 0;
                        sheet.PageSize = VTXPageSize.VTxlPaperA5;

                        sheet.SetPrintArea("$A$1:$" + (soCotDiemKiemTra < 6 ? "T" : "U") + "$" + rowTotal * Math.Max(1, listPP.Count));
                        sheet.PrintArea = ("$A$1:$" + (soCotDiemKiemTra < 6 ? "T" : "U") + "$" + rowTotal * Math.Max(1, listPP.Count));
                        sheet.FitToPage = true;
                        for (int i = 1; i < listPP.Count; i++)
                        {
                            int nextFirstRow = rowTotal * i + 1;
                            sheet.CopyPasteSameRowHeigh(template, nextFirstRow);
                            sheet.SetBreakPage(nextFirstRow);
                        }


                        //Fill dữ liệu cho từng học sinh
                        for (int indexOfPupil = 0; indexOfPupil < listPP.Count; indexOfPupil++)
                        {
                            PupilOfClassBO pp = listPP[indexOfPupil];
                            int startRow = rowTotal * indexOfPupil;
                            int startColumn = 0;
                            //Fill dữ liệu học sinh
                            FillPupilInfoForSecondSemesterReport(increment, sheet, listCS, listPA, listPR, listMR,
                               listJR, listSR, listPE, pp, academicYear, startRow, startColumn, soCotDiemMieng, soCotDiem15Phut, soCotDiemKiemTra);
                        }
                    }
                    else if (reportCode == SystemParamsInFile.REPORT_PHIEU_BAO_DIEM_THEO_KY_II_A4)
                    {
                        //Tạo các vùng thông tin cho từng học sinh
                        IVTRange template = sheet.GetRange("A1", (soCotDiemKiemTra < 6 ? "AO" : "AQ") + rowTotal);
                        int pageCount = listPP.Count / 2;
                        sheet.PageMaginLeft = 0.45;
                        sheet.PageMaginRight = 0.45;
                        sheet.PageMaginTop = 0.5;
                        sheet.PageMaginBottom = 0;
                        sheet.PageSize = VTXPageSize.VTxlPaperA4;

                        //sheet.SetPrintArea("$A$1:$AO$" + rowTotal * Math.Max(1, pageCount));
                        //sheet.PrintArea = ("$A$1:$AO$" + rowTotal * Math.Max(1, pageCount));
                        sheet.FitToPage = true;
                        for (int i = 1; i < pageCount; i++)
                        {
                            int nextFirstRow = rowTotal * i + 1;
                            sheet.CopyPasteSameRowHeigh(template, nextFirstRow);
                            sheet.SetBreakPage(nextFirstRow);
                        }
                        if (listPP.Count % 2 == 1 && listPP.Count > 2)
                        {
                            if (listPP.Count > 2)
                            {
                                IVTRange templateForSinglePupil = sheet.GetRange("A1", (soCotDiemKiemTra < 6 ? "T" : "U") + rowTotal);
                                int nextFirstRow = rowTotal * pageCount + 1;
                                sheet.CopyPasteSameRowHeigh(templateForSinglePupil, nextFirstRow);
                                sheet.SetBreakPage(nextFirstRow);
                            }
                            pageCount++;
                        }
                        sheet.SetPrintArea("$A$1:$" + (soCotDiemKiemTra < 6 ? "AO" : "AQ") + "$" + rowTotal * Math.Max(1, pageCount));


                        //Fill dữ liệu cho từng học sinh
                        for (int indexOfPupil = 0; indexOfPupil < listPP.Count; indexOfPupil++)
                        {
                            PupilOfClassBO pp = listPP[indexOfPupil];
                            int startRow = rowTotal * (indexOfPupil / 2);
                            int startColumn = (indexOfPupil % 2) * shiftColumnRange;
                            //Fill dữ liệu học sinh
                            FillPupilInfoForSecondSemesterReport(increment, sheet, listCS, listPA, listPR, listMR,
                               listJR, listSR, listPE, pp, academicYear, startRow, startColumn, soCotDiemMieng, soCotDiem15Phut, soCotDiemKiemTra);
                        }

                    }
                }
            }

            //Xoá sheet template
            firstSheet.Delete();

            return oBook.ToStream();
        }

        /// <summary>
        /// Tạo file phiếu báo điểm theo kỳ theo mẫu mới
        /// </summary>
        /// <param name="entity"></param>
        /// <returns></returns>
        public Stream CreatePointReportBySemesterFollowNewPattern(PointReportBO entity, List<SummedUpRecordBO> lstSummedUpRecord)
        {
            SchoolProfile school = SchoolProfileBusiness.Find(entity.SchoolID);
            AcademicYear academicYear = AcademicYearRepository.Find(entity.AcademicYearID);

            #region //Lấy danh sách các lớp cần in phiếu báo điểm
            IQueryable<ClassProfile> lstClassTemp = ClassProfileBusiness.getClassByAccountRole(entity.AcademicYearID, entity.SchoolID, entity.EducationLevelID, entity.ClassID, entity.UserAccountID, entity.IsSuperVisingDept, entity.IsSubSuperVisingDept);
            List<ClassProfileTempBO> lstClass = (from p in lstClassTemp
                                                 join q in EmployeeBusiness.All on p.HeadTeacherID equals q.EmployeeID into g
                                                 from j1 in g.DefaultIfEmpty()
                                                 select new ClassProfileTempBO
                                                 {
                                                     HeadTeacherID = p.HeadTeacherID,
                                                     ClassProfileID = p.ClassProfileID,
                                                     SchoolID = p.SchoolID,
                                                     AcademicYearID = p.AcademicYearID,
                                                     OrderNumber = p.OrderNumber,
                                                     EducationLevelID = p.EducationLevelID,
                                                     DisplayName = p.DisplayName,
                                                     EmployeeID = j1.EmployeeID,
                                                     EmployeeName = j1.FullName,
                                                     SectionKey = p.SeperateKey
                                                 }).OrderBy(o => o.OrderNumber).ToList();
            #endregion

            #region //Lấy danh sách các môn học cho các lớp
            Dictionary<string, object> dicSubject = new Dictionary<string, object> 
            {
                {"AcademicYearID", entity.AcademicYearID}, 
                {"EducationLevelID", entity.EducationLevelID},
                {"ClassID", entity.ClassID},
                {"AppliedLevel", entity.AppliedLevel}
            };
            IQueryable<ClassSubject> lstSubjectTemp = ClassSubjectBusiness.SearchBySchool(entity.SchoolID, dicSubject);
            List<ClassSubjectBO> lstSubject = (from p in lstSubjectTemp
                                               join s in SubjectCatBusiness.All on p.SubjectID equals s.SubjectCatID
                                               select new ClassSubjectBO
                                               {
                                                   SubjectID = p.SubjectID,
                                                   ClassID = p.ClassID,
                                                   SubjectName = s.SubjectName,
                                                   DisplayName = s.DisplayName,
                                                   OrderInSubject = s.OrderInSubject,
                                                   IsCommenting = p.IsCommenting,
                                                   SubjectIDCrease = p.SubjectIDIncrease != null ? p.SubjectIDIncrease.Value : 0
                                               }).ToList();
            #endregion

            #region //Lấy danh sách học sinh
            Dictionary<string, object> dicPupil = new Dictionary<string, object> 
            {
                {"AcademicYearID", entity.AcademicYearID}, 
                {"EducationLevelID", entity.EducationLevelID},
                {"ClassID", entity.ClassID},
                {"Check", "check"}
            };
            // Lấy thông tin học sinh theo khối, lớp
            // Lấy luôn ra PupilProfile và OrderInClass để sắp xếp và tăng hiệu năng
            var lstPupil = (from poc in PupilOfClassBusiness.SearchBySchool(entity.SchoolID, dicPupil)
                .AddCriteriaSemester(academicYear, entity.Semester)
                            join pp in PupilProfileBusiness.All on poc.PupilID equals pp.PupilProfileID
                            select new PupilOfClassBO
                            {
                                PupilID = poc.PupilID,
                                PupilCode = pp.PupilCode,
                                PupilFullName = pp.FullName,
                                Birthday = pp.BirthDate,
                                Name = pp.Name,
                                ClassID = poc.ClassID,
                                OrderInClass = poc.OrderInClass
                            })
                .ToList();
            #endregion

            #region //Lấy danh sách thông tin đánh giá học lực
            Dictionary<string, object> dicPupilRanking = new Dictionary<string, object> 
            {
                {"AcademicYearID", entity.AcademicYearID}, 
                {"SchoolID", entity.SchoolID},
                {"EducationLevelID", entity.EducationLevelID},
                {"ClassID", entity.ClassID}
            };
            if (entity.Semester == SystemParamsInFile.SEMESTER_OF_YEAR_FIRST)
            {
                dicPupilRanking.Add("Semester", entity.Semester);
            }
            List<PupilRankingBO> lstPupilRanking = (from p in VPupilRankingBusiness.SearchBySchool(entity.SchoolID, dicPupilRanking)
                                                    join q in CapacityLevelBusiness.All on p.CapacityLevelID equals q.CapacityLevelID into g1
                                                    from j1 in g1.DefaultIfEmpty()
                                                    join r in ConductLevelBusiness.All on p.ConductLevelID equals r.ConductLevelID into g2
                                                    from j2 in g2.DefaultIfEmpty()
                                                    select new PupilRankingBO
                                                    {
                                                        PupilRankingID = p.PupilRankingID,
                                                        PupilID = p.PupilID,
                                                        EducationLevelID = p.EducationLevelID,
                                                        ClassID = p.ClassID,
                                                        Semester = p.Semester,
                                                        ConductLevelID = p.ConductLevelID,
                                                        CapacityLevelID = p.CapacityLevelID,
                                                        ConductLevelName = j2.Resolution,
                                                        CapacityLevel = j1.CapacityLevel1,
                                                        AverageMark = p.AverageMark,
                                                        Comment = p.PupilRankingComment,
                                                        Rank = p.Rank
                                                    }).ToList();
            #endregion

            #region //Lấy danh sách thông tin danh hiệu học sinh
            Dictionary<string, object> dicPupilEmulation = new Dictionary<string, object> 
            {
                {"AcademicYearID", entity.AcademicYearID}, 
                {"EducationLevelID", entity.EducationLevelID},
                {"ClassID", entity.ClassID}
            };
            List<PupilEmulation> lstPupilEmulation = PupilEmulationBusiness.SearchBySchool(entity.SchoolID, dicPupilEmulation).Include("HonourAchivementType").ToList();
            #endregion

            #region // Lấy thông tin kỳ thi
            List<int> lstClassID = lstClass.Select(x => x.ClassProfileID).ToList();

            IDictionary<string, object> tmpDic;

            // Danh sach ky thi
            List<Examinations> lstExaminations = (from ex in ExaminationsBusiness.All
                                                  where ex.AcademicYearID == entity.AcademicYearID && ex.SchoolID == entity.SchoolID
                                                  && ex.AppliedLevel == entity.AppliedLevel
                                                  && ((ex.MarkClosing.HasValue && ex.MarkClosing.Value == true)
                                                    || (ex.MarkInput.HasValue && ex.MarkInput.Value == true))
                                                    && ex.SemesterID == entity.Semester
                                                  select ex).ToList();

            List<long> lstExaminationID = lstExaminations.Select(x => x.ExaminationsID).ToList();

            //Lay danh sach diem thi cua ky thi 
            List<ExamInputMark> listExamMark = new List<ExamInputMark>();
            tmpDic = new Dictionary<string, object>();
            tmpDic["AcademicYearID"] = academicYear.AcademicYearID;
            tmpDic["LastDigitSchoolID"] = school.SchoolProfileID % 100;
            tmpDic["SchoolID"] = school.SchoolProfileID;
            tmpDic["EducationLevelID"] = entity.EducationLevelID;
            listExamMark = ExamInputMarkBusiness.Search(tmpDic).Where(x => lstClassID.Contains(x.ClassID)
                                                                      && lstExaminationID.Contains(x.ExaminationsID)).ToList();
            //Lay danh sach thi sinh cua ky thi
            List<ExamPupil> listExamPupil = new List<ExamPupil>();
            tmpDic = new Dictionary<string, object>();
            tmpDic["SchoolID"] = entity.SchoolID;
            tmpDic["AcademicYearID"] = entity.AcademicYearID;
            tmpDic["EducationLevelID"] = entity.EducationLevelID;
            tmpDic["lstClassID"] = lstClassID;
            tmpDic["lstExaminationsID"] = lstExaminationID;
            listExamPupil = ExamPupilBusiness.Search(tmpDic).ToList();
            //Lay danh sach lop thi hoc cac mon
            tmpDic = new Dictionary<string, object>();
            tmpDic["AcademicYearID"] = entity.AcademicYearID;
            tmpDic["AppliedLevel"] = entity.AppliedLevel;
            tmpDic["Semester"] = entity.Semester;
            tmpDic["EducationLevelID"] = entity.EducationLevelID;
            List<ClassSubject> listClassSubject = ClassSubjectBusiness.SearchBySchool(entity.SchoolID, tmpDic).Where(x => lstClassID.Contains(x.ClassID)).ToList();

            //Lay danh sach mon thi cua nhom thi
            List<int> listSubjectId = listClassSubject.Select(x => x.SubjectID).ToList();
            List<ExamSubjectBO> listExamSubject = new List<ExamSubjectBO>();
            tmpDic = new Dictionary<string, object>();
            listExamSubject = ExamSubjectBusiness.GetListExamSubject(tmpDic).Where(x => lstExaminationID.Contains(x.ExaminationsID)
                                                                                    && listSubjectId.Contains(x.SubjectID))
                                                                            .OrderBy(o => o.OrderInSubject).ToList();
            //Loai cac mon thi trung nhau
            List<int> listSubjectInExam = listExamSubject.Select(s => s.SubjectID).Distinct().ToList();

            // Lấy danh sách xếp hạng trong kỳ thi
            List<ExamInputMarkBO> listExamMarkTemp = new List<ExamInputMarkBO>();
            listExamMarkTemp = (from em in listExamMark
                                join cs in listClassSubject on new { em.ClassID, em.SubjectID } equals new { cs.ClassID, cs.SubjectID }
                                select new ExamInputMarkBO()
                                {
                                    ActualMark = em.ActualMark,
                                    PupilID = em.PupilID,
                                    ClassID = em.ClassID,
                                    ExaminationsID = em.ExaminationsID
                                }).ToList();

            var lstLevel = listExamMarkTemp.GroupBy(s => new { s.ClassID, s.ExaminationsID, s.PupilID })
                                                    .Select(g => new LevelOfPupilInExamBO()
                                                    {
                                                        ExaminationID = g.Key.ExaminationsID,
                                                        ClassID = g.Key.ClassID,
                                                        PupilID = g.Key.PupilID,
                                                        LevelInExamination = g.Sum(x => x.ActualMark)
                                                    }).OrderByDescending(x => x.LevelInExamination).ToList();
            #endregion

            #region // Lấy danh sách Giáo viên chủ nhiệm
            List<HeadTeacherSubstitution> lstHeadTeachSubstitution = HeadTeacherSubstitutionBusiness.All
                                                                        .Where(x => x.SchoolID == entity.SchoolID
                                                                                && x.AcademicYearID == entity.SchoolID
                                                                                && lstClassID.Contains(x.ClassID)
                                                                                && x.SubstituedHeadTeacher > 0).ToList();

            List<ClassSupervisorAssignment> lstClassSuperAssignment = (from csa in ClassSupervisorAssignmentBusiness.All
                                                                       where csa.SchoolID == entity.SchoolID
                                                                       && csa.AcademicYearID == entity.SchoolID
                                                                       && lstClassID.Contains(csa.ClassID)
                                                                       && csa.PermissionLevel == 1
                                                                       select csa).ToList();

            List<int> lstHeadTeacherID = new List<int>();
            lstHeadTeacherID = lstClass.Where(x => x.HeadTeacherID.HasValue && x.HeadTeacherID > 0).Select(x => x.HeadTeacherID.Value).ToList();
            var lstTeacherId = lstHeadTeachSubstitution.Select(x => x.SubstituedHeadTeacher).ToList();
            if (lstTeacherId.Count() > 0) { lstHeadTeacherID.AddRange(lstTeacherId); }
            lstTeacherId = lstClassSuperAssignment.Select(x => x.TeacherID).ToList();
            if (lstTeacherId.Count() > 0){ lstHeadTeacherID.AddRange(lstTeacherId); }
            List<Employee> lstEmployee = EmployeeBusiness.All.Where(x => lstHeadTeacherID.Contains(x.EmployeeID) && x.IsActive).ToList();
            #endregion

            #region // xuat theo mau 2
            string reportCode;
            if (entity.Semester == SystemParamsInFile.SEMESTER_OF_YEAR_FIRST)
            {
                reportCode = SystemParamsInFile.REPORT_PHIEU_BAO_DIEM_THEO_KY_I_MAU_2;
            }
            else
            {
                reportCode = SystemParamsInFile.REPORT_PHIEU_BAO_DIEM_THEO_KY_II_MAU_2;
            }
            ReportDefinition reportDef = ReportDefinitionBusiness.GetByCode(reportCode);

            string templatePath = SystemParamsInFile.TEMPLATE_FOLDER + reportDef.TemplateDirectory + "/" + reportDef.TemplateName;
            IVTWorkbook oBook = VTExport.OpenWorkbook(templatePath);

            //Lấy sheet phiếu điểm
            IVTWorksheet firstSheet = oBook.GetSheet(1);    // sheet null
            IVTWorksheet secondSheet = oBook.GetSheet(2);   // sheet HK2
            IVTWorksheet thirdSheet = oBook.GetSheet(3);    // sheet HK1
            IVTWorksheet fourSheet = oBook.GetSheet(4);     // sheet thông tin chung        

            #region // Fill sheet thong tin chung
            string supervisingDeptName = UtilsBusiness.GetSupervisingDeptName(school.SchoolProfileID, entity.AppliedLevel).ToUpper();
            string schoolName = school.SchoolName.ToUpper();
            string semesterName = SMASConvert.ConvertSemester(entity.Semester).ToUpper() + ", " +
                                    "NĂM HỌC " + academicYear.Year + " - " + (academicYear.Year + 1);
            string dateSignature = (school.District != null ? school.District.DistrictName : "") + ", " + ReportUtils.ConvertFullDate(DateTime.Now);

            fourSheet.SetCellValue("B2", supervisingDeptName); // Ten don vi quan lý
            fourSheet.SetCellValue("B3", schoolName); // Ten truong
            fourSheet.SetCellValue("B5", semesterName); // Hoc ky
            fourSheet.SetCellValue("B6", dateSignature); // Ngay ky
            fourSheet.SetCellValue("B8", school.HeadMasterName);//Ten hieu truong
            #endregion

            #region // Fill sheet HK
            int startRow = 2;
            int count = 0;
            string headTeacherName = string.Empty;
            int teacherID = 0;
            if (entity.Semester == SystemParamsInFile.SEMESTER_OF_YEAR_FIRST)
            {
                #region // Fill HK 1
                IVTRange rangInfo = thirdSheet.GetRange("A2", "AV6");           // vùng thông tin chung
                IVTRange rangTitleExam = thirdSheet.GetRange("B9", "H9");       // Vùng ô title kỳ thi
                IVTRange rangExamination = thirdSheet.GetRange("B10", "H10");   // Vùng ô kỳ thi
                IVTRange rangCellSubject = thirdSheet.GetRange("I9", "K9");     // Vùng ô môn thi
                IVTRange rangCellMark = thirdSheet.GetRange("I10", "K10");      // vùng ô điểm thi
                IVTRange rangSubjectAVG = thirdSheet.GetRange("C14", "E15");         // vùng Điểm TBM
                IVTRange rangInfo2 = thirdSheet.GetRange("A12", "AV35");      // vùng Điểm tổng kết và kết quả xếp loại, Nhận xét của GVCN

                int startMin = 0;
                foreach (var item in lstClass)
                {
                    #region
                    headTeacherName = "";
                    // GVCN
                    var objTeacherSub = lstHeadTeachSubstitution.Where(x => x.ClassID == item.ClassProfileID).FirstOrDefault();
                    if (objTeacherSub != null)
                    {
                        teacherID = objTeacherSub.HeadTeacherID;
                    }
                    else
                    {
                        if (item.HeadTeacherID.HasValue && item.HeadTeacherID.Value > 0)
                        {
                            teacherID = item.HeadTeacherID.Value;
                        }
                        else
                        {
                            var objTeacherAss = lstClassSuperAssignment.Where(x => x.ClassID == item.ClassProfileID).FirstOrDefault();
                            if (objTeacherAss != null)
                            {
                                teacherID = objTeacherAss.TeacherID;
                            }
                        }
                    }

                    if (teacherID > 0)
                    { 
                        var objEmployee = lstEmployee.Where(x=>x.EmployeeID == teacherID).FirstOrDefault();
                        if (objEmployee != null)
                            headTeacherName = objEmployee.FullName;
                    }

                    // Danh sách điểm thi theo lớp
                    var listExamMarkByClass = listExamMark.Where(x => x.ClassID == item.ClassProfileID).ToList();

                    // Danh sách môn học trong kỳ thi theo lớp
                    //List<int> lstSubjectIDExamByClass = listExamMarkByClass.Select(x => x.SubjectID).Distinct().ToList();
                    var lstSubjectExamByClass = lstSubject.Where(x => x.ClassID == item.ClassProfileID && listSubjectInExam.Contains(x.SubjectID))
                                                       .OrderBy(x => x.OrderInSubject).ToList();
                    ClassSubjectBO objBO = new ClassSubjectBO();
                    objBO.SubjectName = "Xếp hạng";
                    lstSubjectExamByClass.Add(objBO);

                    // Danh sach hoc sinh trong lop
                    var lstPupilByClass = lstPupil.Where(x => x.ClassID == item.ClassProfileID)
                                                    .OrderBy(x => x.OrderInClass)
                                                    .ThenBy(x => x.Name).ThenBy(x => x.PupilFullName).ToList();
                    // Danh sach mon hoc cua lop
                    var lstSubjectByClass = lstSubject.Where(x => x.ClassID == item.ClassProfileID).OrderBy(x => x.OrderInSubject).ToList();

                    //Lấy danh sách thông tin điểm tổng kết trong lớp
                    var listSRByClass = (from s in lstSummedUpRecord
                                         where s.ClassID == item.ClassProfileID
                                         select s).ToList();
                    //Lấy danh sách thông tin đánh giá học lực trong lớp
                    var listPR = (from s in lstPupilRanking
                                  where s.ClassID == item.ClassProfileID
                                  select s).ToList();
                    //Lấy danh sách thông tin danh hiệu học sinh
                    var listPE = (from s in lstPupilEmulation
                                  where s.ClassID == item.ClassProfileID
                                  select s).ToList();
                    #endregion

                    PupilOfClassBO objPOC = null;
                    for (int i = 0; i < lstPupilByClass.Count(); i++)
                    {
                        startMin = startRow;
                        count++;

                        List<SetHeightBO> lstSetHeight = new List<SetHeightBO>();

                        objPOC = lstPupilByClass[i];
                        firstSheet.CopyPaste(rangInfo, startRow, 1);
                        firstSheet.SetCellValue("A" + startRow, "=Thongtinchung!$B$2"); // Sở
                        firstSheet.SetCellValue("U" + startRow, "=Thongtinchung!$B$4");
                        startRow++;
                        firstSheet.SetCellValue("A" + startRow, "=Thongtinchung!$B$3"); // Trường
                        firstSheet.SetCellValue("U" + startRow, "=Thongtinchung!$B$5"); // Thông tin học kỳ
                        //firstSheet.SetRowHeight((startRow + 1), 5);
                        lstSetHeight.Add(new SetHeightBO() { Height = 5, Row = (startRow + 1) });
                        startRow += 2;
                        firstSheet.SetCellValue("C" + startRow, objPOC.PupilFullName); // Họ tên
                        firstSheet.SetCellValue("V" + startRow, objPOC.Birthday.ToString("dd/MM/yyyy")); // Ngày sinh
                        firstSheet.SetCellValue("AG" + startRow, item.DisplayName); // Lớp
                        //firstSheet.SetRowHeight((startRow + 1), 5);
                        lstSetHeight.Add(new SetHeightBO() { Height = 5, Row = (startRow + 1) });

                        #region // Mục I
                        //var listExamPupilOfPupil = listExamPupil.Where(x => x.PupilID == objPOC.PupilID).ToList();
                        lstExaminationID = listExamPupil.Where(x => x.PupilID == objPOC.PupilID).Select(x => x.ExaminationsID).ToList();
                        // Danh sach ky thi cua hoc sinh
                        var lstlstExaminationOfPupil = lstExaminations.Where(x => lstExaminationID.Contains(x.ExaminationsID)).ToList();
                        // Kiểm tra học sinh có trong kỳ thi hay không
                        if (lstlstExaminationOfPupil.Count() > 0)
                        {
                            firstSheet.SetCellValue("B" + (startRow + 2), "I. Kết quả các đợt thi");
                            firstSheet.GetRange((startRow + 2), 2, (startRow + 2), 7).SetFontStyle(true, System.Drawing.Color.Black, false, 10, false, true);

                            var lstCheckLevelByExamOfPupil = lstLevel.Where(x => lstExaminationID.Contains(x.ExaminationID) && x.PupilID == objPOC.PupilID).ToList();
                            var lstCheckLevelByExamAll = lstLevel.Where(x => lstExaminationID.Contains(x.ExaminationID)).ToList();
                            startRow += 4;
                            lstSetHeight.Add(new SetHeightBO() { Height = 5, Row = (startRow - 1) });
                            lstSetHeight.Add(new SetHeightBO() { Height = 30, Row = (startRow) });

                            var listExamMarkByClassForPupil = listExamMarkByClass.Where(x => x.PupilID == objPOC.PupilID).ToList();
                            firstSheet = this.RenderAreaExam(firstSheet, startRow, rangTitleExam, rangExamination, rangCellSubject, rangCellMark,
                                                            lstSubjectExamByClass, lstlstExaminationOfPupil, listExamMarkByClassForPupil,
                                                            lstCheckLevelByExamOfPupil, lstCheckLevelByExamAll);

                            startRow += lstlstExaminationOfPupil.Count() + 2;
                            // firstSheet.SetRowHeight((startRow - 1), 5);
                            lstSetHeight.Add(new SetHeightBO() { Height = 5, Row = (startRow - 1) });
                        }
                        else
                        {
                            startRow += 2;
                        }
                        #endregion

                        #region // Mục II
                        firstSheet.CopyPaste(rangInfo2, startRow, 1);
                        if (lstlstExaminationOfPupil.Count() == 0)
                        {
                            firstSheet.SetCellValue("B" + startRow, "I. Điểm tổng kết và kết quả xếp loại");
                            firstSheet.SetCellValue("B" + (startRow + 7), "II. Nhận xét của GVCN");
                        }
                        int startCol = 3;
                        startRow += 2;
                        //firstSheet.SetRowHeight((startRow -1), 5);
                        lstSetHeight.Add(new SetHeightBO() { Height = 5, Row = (startRow - 1) });
                        for (int m = 0; m < lstSubjectByClass.Count(); m++)
                        {
                            var objSubject = lstSubjectByClass[m];
                            firstSheet.CopyPaste(rangSubjectAVG, startRow, startCol);
                            firstSheet.SetCellValue(startRow, startCol, objSubject.SubjectName);
                            //Fill dữ liệu điểm trung bình môn
                            List<SummedUpRecordBO> lSR = (from s in listSRByClass
                                                          where s.PupilID == objPOC.PupilID
                                                          && s.SubjectID == objSubject.SubjectID
                                                          && s.Semester == SystemParamsInFile.SEMESTER_OF_YEAR_FIRST
                                                          select s).ToList();
                            if (lSR != null && lSR.Count > 0)
                            {
                                if (objSubject.IsCommenting == SystemParamsInFile.ISCOMMENTING_TYPE_MARK ||
                                    objSubject.IsCommenting == SystemParamsInFile.ISCOMMENTING_TYPE_MARK_JUDGE)
                                {
                                    string summedUpMark = lSR[0].SummedUpMark != null ? ReportUtils.ConvertMarkForV(lSR[0].SummedUpMark.Value) : string.Empty;
                                    firstSheet.SetCellValue((startRow + 1), startCol, summedUpMark);
                                }
                                else
                                {
                                    string judgementResult = lSR[0].JudgementResult;
                                    firstSheet.SetCellValue((startRow + 1), startCol, judgementResult);
                                }
                            }
                            startCol += 3;
                        }
                        lstSetHeight.Add(new SetHeightBO() { Height = 30, Row = (startRow) });
                        lstSetHeight.Add(new SetHeightBO() { Height = 5, Row = (startRow + 2) });
                        lstSetHeight.Add(new SetHeightBO() { Height = 5, Row = (startRow + 4) });
                        lstSetHeight.Add(new SetHeightBO() { Height = 5, Row = (startRow + 6) });

                        startRow += 3;

                        //Fill dữ liệu đánh giá học lực
                        List<PupilRankingBO> lPR = (from s in listPR
                                                    where s.PupilID == objPOC.PupilID
                                                    && s.Semester == SystemParamsInFile.SEMESTER_OF_YEAR_FIRST
                                                    select s).ToList();

                        string capacityLevel = "";
                        string conductLevelName = "";
                        string comment = "";
                        string honourAchivementType = "";
                        decimal? averageMark = null;

                        if (lPR != null && lPR.Count > 0)
                        {
                            PupilRankingBO pr = lPR[0];
                            comment = pr.Comment;
                            capacityLevel = pr.CapacityLevelID != null ? pr.CapacityLevel : "";
                            conductLevelName = pr.ConductLevelID != null ? pr.ConductLevelName : "";
                            averageMark = pr.AverageMark;
                        }
                        //Fill dữ liệu danh hiệu học sinh
                        List<PupilEmulation> lPE = (from s in listPE
                                                    where s.PupilID == objPOC.PupilID
                                                    && s.Semester == SystemParamsInFile.SEMESTER_OF_YEAR_FIRST
                                                    select s).ToList();
                        if (lPE != null && lPE.Count > 0)
                        {
                            PupilEmulation pe = lPE[0];
                            honourAchivementType = pe.HonourAchivementType.Resolution;
                        }

                        firstSheet.SetCellValue("I" + startRow, averageMark);
                        firstSheet.SetCellValue("R" + startRow, capacityLevel);
                        firstSheet.SetCellValue("AA" + startRow, conductLevelName);
                        firstSheet.SetCellValue("AJ" + startRow, honourAchivementType);
                        startRow += 4;
                        firstSheet.SetCellValue("B" + startRow, comment);
                        startRow += 2;
                        firstSheet.SetCellValue("U" + startRow, "=Thongtinchung!$B$6");
                        startRow += 1;
                        firstSheet.SetCellValue("B" + startRow, "=Thongtinchung!$B$7");
                        firstSheet.SetCellValue("U" + startRow, "=Thongtinchung!$B$9");
                        startRow += 4;
                        firstSheet.SetCellValue("B" + startRow, "=Thongtinchung!$B$8");
                        firstSheet.SetCellValue("U" + startRow, headTeacherName);
                        startRow += 6;

                        if (count % 2 == 0)
                        {
                            firstSheet.SetBreakPage(startRow);
                            startRow += 1;
                        }

                        for (int r = startMin; r < startRow; r++)
                        {
                            var checkObj = lstSetHeight.Where(x => x.Row == r).FirstOrDefault();
                            firstSheet.SetRowHeight(r, (checkObj != null ? checkObj.Height : 12.75));
                        }
                        #endregion
                    }
                }

                firstSheet.Name = "HK1";
                #endregion
            }
            else
            {
                #region // Fill HK 2
                IVTRange rangInfo = secondSheet.GetRange("A2", "AV6");           // vùng thông tin chung
                IVTRange rangTitleExam = secondSheet.GetRange("B9", "H9");       // Vùng ô title kỳ thi
                IVTRange rangExamination = secondSheet.GetRange("B10", "H10");   // Vùng ô kỳ thi
                IVTRange rangCellSubject = secondSheet.GetRange("I9", "K9");     // Vùng ô môn thi
                IVTRange rangCellMark = secondSheet.GetRange("I10", "K10");      // vùng ô điểm thi
                IVTRange rangSubjectAVG = secondSheet.GetRange("C14", "E16");         // vùng Điểm TBM
                IVTRange rangInfo2 = secondSheet.GetRange("A12", "AS35");      // vùng Điểm tổng kết và kết quả xếp loại, Nhận xét của GVCN

                int startMin = 0;
                foreach (var item in lstClass)
                {
                    #region
                    headTeacherName = "";
                    // GVCN
                    var objTeacherSub = lstHeadTeachSubstitution.Where(x => x.ClassID == item.ClassProfileID).FirstOrDefault();
                    if (objTeacherSub != null)
                    {
                        teacherID = objTeacherSub.HeadTeacherID;
                    }
                    else
                    {
                        if (item.HeadTeacherID.HasValue && item.HeadTeacherID.Value > 0)
                        {
                            teacherID = item.HeadTeacherID.Value;
                        }
                        else
                        {
                            var objTeacherAss = lstClassSuperAssignment.Where(x => x.ClassID == item.ClassProfileID).FirstOrDefault();
                            if (objTeacherAss != null)
                            {
                                teacherID = objTeacherAss.TeacherID;
                            }
                        }
                    }

                    if (teacherID > 0)
                    {
                        var objEmployee = lstEmployee.Where(x => x.EmployeeID == teacherID).FirstOrDefault();
                        if (objEmployee != null)
                            headTeacherName = objEmployee.FullName;
                    }


                    // Danh sách môn học trong kỳ thi theo lớp
                    var lstSubjectExamByClass = lstSubject.Where(x => x.ClassID == item.ClassProfileID && listSubjectInExam.Contains(x.SubjectID))
                                                     .OrderBy(x => x.OrderInSubject).ToList();
                    ClassSubjectBO objBO = new ClassSubjectBO();
                    objBO.SubjectName = "Xếp hạng";
                    lstSubjectExamByClass.Add(objBO);

                    // Danh sách điểm thi theo lớp
                    var listExamMarkByClass = listExamMark.Where(x => x.ClassID == item.ClassProfileID).ToList();
                    // Danh sach hoc sinh trong lop
                    var lstPupilByClass = lstPupil.Where(x => x.ClassID == item.ClassProfileID)
                                                    .OrderBy(x => x.OrderInClass)
                                                    .ThenBy(x => x.Name).ThenBy(x => x.PupilFullName).ToList();
                    // Danh sach mon hoc cua lop
                    var lstSubjectByClass = lstSubject.Where(x => x.ClassID == item.ClassProfileID).OrderBy(x => x.OrderInSubject).ToList();

                    //Lấy danh sách thông tin điểm tổng kết trong lớp
                    var listSRByClass = (from s in lstSummedUpRecord
                                         where s.ClassID == item.ClassProfileID
                                         select s).ToList();
                    //Lấy danh sách thông tin đánh giá học lực trong lớp
                    var listPR = (from s in lstPupilRanking
                                  where s.ClassID == item.ClassProfileID
                                  select s).ToList();
                    //Lấy danh sách thông tin danh hiệu học sinh
                    var listPE = (from s in lstPupilEmulation
                                  where s.ClassID == item.ClassProfileID
                                  select s).ToList();
                    #endregion

                    PupilOfClassBO objPOC = null;
                    for (int i = 0; i < lstPupilByClass.Count(); i++)
                    {

                        startMin = startRow;
                        count++;
                        objPOC = lstPupilByClass[i];
                        firstSheet.CopyPaste(rangInfo, startRow, 1);

                        List<SetHeightBO> lstSetHeight = new List<SetHeightBO>();

                        firstSheet.SetCellValue("A" + startRow, "=Thongtinchung!$B$2"); // Sở
                        firstSheet.SetCellValue("U" + startRow, "=Thongtinchung!$B$4");
                        startRow++;
                        firstSheet.SetCellValue("A" + startRow, "=Thongtinchung!$B$3"); // Trường
                        firstSheet.SetCellValue("U" + startRow, "=Thongtinchung!$B$5"); // Thông tin học kỳ
                        //firstSheet.SetRowHeight((startRow + 1), 5);
                        lstSetHeight.Add(new SetHeightBO() { Height = 5, Row = (startRow + 1) });
                        startRow += 2;
                        firstSheet.SetCellValue("C" + startRow, objPOC.PupilFullName); // Họ tên
                        firstSheet.SetCellValue("V" + startRow, objPOC.Birthday.ToString("dd/MM/yyyy")); // Ngày sinh
                        firstSheet.SetCellValue("AG" + startRow, item.DisplayName); // Lớp
                        //firstSheet.SetRowHeight((startRow + 1), 5);
                        lstSetHeight.Add(new SetHeightBO() { Height = 5, Row = (startRow + 1) });
                        #region Mục I
                        lstExaminationID = listExamPupil.Where(x => x.PupilID == objPOC.PupilID).Select(x => x.ExaminationsID).ToList();
                        // Danh sach ky thi cua hoc sinh
                        var lstlstExaminationOfPupil = lstExaminations.Where(x => lstExaminationID.Contains(x.ExaminationsID)).ToList();
                        // Kiểm tra học sinh có trong kỳ thi hay không
                        if (lstlstExaminationOfPupil.Count() > 0)
                        {
                            firstSheet.SetCellValue("B" + (startRow + 2), "I. Kết quả các đợt thi");
                            firstSheet.GetRange((startRow + 2), 2, (startRow + 2), 7).SetFontStyle(true, System.Drawing.Color.Black, false, 10, false, true);

                            var lstCheckLevelByExamOfPupil = lstLevel.Where(x => lstExaminationID.Contains(x.ExaminationID) && x.PupilID == objPOC.PupilID).ToList();
                            var lstCheckLevelByExamAll = lstLevel.Where(x => lstExaminationID.Contains(x.ExaminationID)).ToList();
                            startRow += 4;
                            lstSetHeight.Add(new SetHeightBO() { Height = 5, Row = (startRow - 1) });
                            lstSetHeight.Add(new SetHeightBO() { Height = 30, Row = (startRow) });

                            var listExamMarkByClassForPupil = listExamMarkByClass.Where(x => x.PupilID == objPOC.PupilID).ToList();
                            firstSheet = this.RenderAreaExam(firstSheet, startRow, rangTitleExam, rangExamination, rangCellSubject, rangCellMark,
                                                            lstSubjectExamByClass, lstlstExaminationOfPupil, listExamMarkByClassForPupil,
                                                            lstCheckLevelByExamOfPupil, lstCheckLevelByExamAll);

                            startRow += lstlstExaminationOfPupil.Count() + 2;
                            //firstSheet.SetRowHeight((startRow - 1), 5);
                            lstSetHeight.Add(new SetHeightBO() { Height = 5, Row = (startRow - 1) });
                        }
                        else
                        {
                            startRow += 2;
                        }
                        #endregion

                        #region Mục II
                        firstSheet.CopyPaste(rangInfo2, startRow, 1);
                        if (lstlstExaminationOfPupil.Count() == 0)
                        {
                            firstSheet.SetCellValue("B" + startRow, "I. Điểm tổng kết và kết quả xếp loại");
                            firstSheet.SetCellValue("B" + (startRow + 9), "II. Nhận xét của GVCN");
                        }

                        startRow += 2;
                        lstSetHeight.Add(new SetHeightBO() { Height = 5, Row = (startRow - 1) });
                        //firstSheet.SetRowHeight((startRow - 1), 5);
                        int startCol = 3;

                        for (int m = 0; m < lstSubjectByClass.Count(); m++)
                        {
                            var objSubject = lstSubjectByClass[m];
                            firstSheet.CopyPaste(rangSubjectAVG, startRow, startCol);
                            firstSheet.SetCellValue(startRow, startCol, objSubject.SubjectName);
                            //Fill dữ liệu điểm trung bình môn
                            List<SummedUpRecordBO> lSR = (from s in listSRByClass
                                                          where s.PupilID == objPOC.PupilID
                                                          && s.SubjectID == objSubject.SubjectID
                                                          && s.Semester == SystemParamsInFile.SEMESTER_OF_YEAR_SECOND
                                                          select s).ToList();
                            if (lSR != null && lSR.Count > 0)
                            {
                                if (objSubject.IsCommenting == SystemParamsInFile.ISCOMMENTING_TYPE_MARK ||
                                    objSubject.IsCommenting == SystemParamsInFile.ISCOMMENTING_TYPE_MARK_JUDGE)
                                {
                                    string summedUpMark = lSR[0].SummedUpMark != null ? ReportUtils.ConvertMarkForV(lSR[0].SummedUpMark.Value) : string.Empty;
                                    firstSheet.SetCellValue((startRow + 1), startCol, summedUpMark);
                                }
                                else
                                {
                                    string judgementResult = lSR[0].JudgementResult;
                                    firstSheet.SetCellValue((startRow + 1), startCol, judgementResult);
                                }
                            }

                            //Fill dữ liệu điểm trung bình môn học cả năm
                            List<SummedUpRecordBO> lSRAll = (from s in listSRByClass
                                                             where s.PupilID == objPOC.PupilID
                                                         && s.SubjectID == objSubject.SubjectID
                                                             && s.Semester >= SystemParamsInFile.SEMESTER_OF_YEAR_ALL
                                                             select s).OrderByDescending(o => o.Semester).ToList();
                            if (lSRAll != null && lSRAll.Count > 0)
                            {

                                if (objSubject.IsCommenting == SystemParamsInFile.ISCOMMENTING_TYPE_MARK ||
                                    objSubject.IsCommenting == SystemParamsInFile.ISCOMMENTING_TYPE_MARK_JUDGE)
                                {
                                    if (lSRAll[0].ReTestMark != null)
                                    {
                                        lSRAll[0].SummedUpMark = lSRAll[0].ReTestMark;
                                    }
                                    string summedUpMark = lSRAll[0].SummedUpMark != null ? ReportUtils.ConvertMarkForV(lSRAll[0].SummedUpMark.Value) : string.Empty;
                                    firstSheet.SetCellValue((startRow + 2), startCol, summedUpMark);
                                }
                                else
                                {
                                    if (lSRAll[0].ReTestJudgement != null && lSRAll[0].ReTestJudgement != " ")
                                    {
                                        lSRAll[0].JudgementResult = lSRAll[0].ReTestJudgement;
                                    }
                                    string judgementResult = lSRAll[0].JudgementResult;
                                    firstSheet.SetCellValue((startRow + 2), startCol, judgementResult);
                                }
                            }
                            startCol += 3;
                        }

                        lstSetHeight.Add(new SetHeightBO() { Height = 30, Row = (startRow) });
                        lstSetHeight.Add(new SetHeightBO() { Height = 5, Row = (startRow + 3) });
                        lstSetHeight.Add(new SetHeightBO() { Height = 5, Row = (startRow + 6) });
                        lstSetHeight.Add(new SetHeightBO() { Height = 5, Row = (startRow + 8) });
                        startRow += 4;

                        //Fill dữ liệu đánh giá học lực HK2
                        List<PupilRankingBO> lPR = (from s in listPR
                                                    where s.PupilID == objPOC.PupilID
                                                    && s.Semester == SystemParamsInFile.SEMESTER_OF_YEAR_SECOND
                                                    select s).ToList();

                        string capacityLevel = "";
                        string conductLevelName = "";
                        string comment = "";
                        string honourAchivementType = "";
                        decimal? averageMark = null;

                        if (lPR != null && lPR.Count > 0)
                        {
                            PupilRankingBO pr = lPR[0];
                            comment = pr.Comment;
                            capacityLevel = pr.CapacityLevelID != null ? pr.CapacityLevel : "";
                            conductLevelName = pr.ConductLevelID != null ? pr.ConductLevelName : "";
                            averageMark = pr.AverageMark;
                        }
                        //Fill dữ liệu danh hiệu học sinh HK2
                        List<PupilEmulation> lPE = (from s in listPE
                                                    where s.PupilID == objPOC.PupilID
                                                    && s.Semester == SystemParamsInFile.SEMESTER_OF_YEAR_SECOND
                                                    select s).ToList();
                        if (lPE != null && lPE.Count > 0)
                        {
                            PupilEmulation pe = lPE[0];
                            honourAchivementType = pe.HonourAchivementType.Resolution;
                        }

                        firstSheet.SetCellValue("I" + startRow, averageMark);
                        firstSheet.SetCellValue("R" + startRow, capacityLevel);
                        firstSheet.SetCellValue("AA" + startRow, conductLevelName);
                        firstSheet.SetCellValue("AJ" + startRow, honourAchivementType);

                        capacityLevel = "";
                        conductLevelName = "";
                        honourAchivementType = "";
                        //Fill dữ liệu đánh giá học lực CN
                        List<PupilRankingBO> lPRAll = (from s in listPR
                                                       where s.PupilID == objPOC.PupilID
                                                       && s.Semester == SystemParamsInFile.SEMESTER_OF_YEAR_ALL
                                                       select s).ToList();

                        if (lPRAll != null && lPRAll.Count > 0)
                        {
                            PupilRankingBO pr = lPRAll[0];
                            capacityLevel = pr.CapacityLevelID != null ? pr.CapacityLevel : "";
                            conductLevelName = pr.ConductLevelID != null ? pr.ConductLevelName : "";
                            averageMark = pr.AverageMark;
                        }
                        //Fill dữ liệu danh hiệu học sinh HKCN
                        List<PupilEmulation> lPEAll = (from s in listPE
                                                       where s.PupilID == objPOC.PupilID
                                                       && s.Semester == SystemParamsInFile.SEMESTER_OF_YEAR_ALL
                                                       select s).ToList();
                        if (lPEAll != null && lPEAll.Count > 0)
                        {
                            PupilEmulation pe = lPEAll[0];
                            honourAchivementType = pe.HonourAchivementType.Resolution;
                        }

                        startRow++;
                        firstSheet.SetCellValue("I" + startRow, averageMark);
                        firstSheet.SetCellValue("R" + startRow, capacityLevel);
                        firstSheet.SetCellValue("AA" + startRow, conductLevelName);
                        firstSheet.SetCellValue("AJ" + startRow, honourAchivementType);

                        startRow += 4;
                        firstSheet.SetCellValue("B" + startRow, comment); // Nhân xet GVCN
                        startRow += 2;
                        firstSheet.SetCellValue("U" + startRow, "=Thongtinchung!$B$6");
                        startRow += 1;
                        firstSheet.SetCellValue("B" + startRow, "=Thongtinchung!$B$7");
                        firstSheet.SetCellValue("U" + startRow, "=Thongtinchung!$B$9");
                        startRow += 4;
                        firstSheet.SetCellValue("B" + startRow, "=Thongtinchung!$B$8");
                        firstSheet.SetCellValue("U" + startRow, headTeacherName);
                        startRow += 4;

                        if (count % 2 == 0)
                        {
                            firstSheet.SetBreakPage(startRow);
                            startRow += 1;
                        }

                        for (int r = startMin; r < startRow; r++)
                        {
                            var checkObj = lstSetHeight.Where(x => x.Row == r).FirstOrDefault();
                            firstSheet.SetRowHeight(r, (checkObj != null ? checkObj.Height : 12.75));
                        }

                        #endregion
                    }
                }
                firstSheet.Name = "HK2";
                #endregion
            }

            firstSheet.SetRowHeight(1, 12.75);
            firstSheet.PageMaginBottom = 0.25;
            firstSheet.PageMaginLeft = 0.25;
            firstSheet.PageMaginRight = 0.25;
            firstSheet.PageMaginTop = 0.25;
            firstSheet.FitAllColumnsOnOnePage = true;
            firstSheet.SetFontName("Times New Roman", 10);
            fourSheet.PageSize = VTXPageSize.VTxlPaperA4;
            secondSheet.Delete();
            thirdSheet.Delete();
            #endregion
            #endregion

            return oBook.ToStream();
        }

        private IVTWorksheet RenderAreaExam(IVTWorksheet firstSheet, int startRow, IVTRange rangTitleExam, IVTRange rangExamination, IVTRange rangCellSubject, IVTRange rangCellMark,
            List<ClassSubjectBO> lstSubjectExamByClass, List<Examinations> lstlstExaminationOfPupil, List<ExamInputMark> listExamMarkByClass,
           List<LevelOfPupilInExamBO> lstCheckLevelByExamOfPupil, List<LevelOfPupilInExamBO> lstLevelExam)
        {
            firstSheet.SetRowHeight((startRow - 1), 5);
            firstSheet.CopyPaste(rangTitleExam, startRow, 2);
            firstSheet.SetRowHeight((startRow), 30);
            int StartCol = 9;
            List<string> lstExaminationName = new List<string>();
            int indexExam = 1;
            // Fill môn học trong ky thi    
            bool levelIndex = false;
            for (int s = 0; s < lstSubjectExamByClass.Count(); s++)
            {
                firstSheet.CopyPaste(rangCellSubject, startRow, StartCol);
                firstSheet.SetCellValue(startRow, StartCol, lstSubjectExamByClass[s].SubjectName);

                if (lstSubjectExamByClass[s].SubjectName.ToUpper() == "XẾP HẠNG")
                {
                    levelIndex = true;
                }
                else
                {
                    levelIndex = false;
                }

                // Fill cac ky thi cua hoc sinh
                for (int e = 0; e < lstlstExaminationOfPupil.Count(); e++)
                {
                    string examName = lstlstExaminationOfPupil[e].ExaminationsName;
                    indexExam = lstlstExaminationOfPupil.FindIndex(x => x.ExaminationsID == lstlstExaminationOfPupil[e].ExaminationsID) + 1;

                    if (!lstExaminationName.Contains(examName))
                    {
                        lstExaminationName.Add(examName);
                        firstSheet.CopyPaste(rangExamination, (startRow + indexExam), 2);
                        firstSheet.SetCellValue((startRow + indexExam), 2, examName);
                    }

                    if (!levelIndex)
                    {
                        var objExamMarkOfPupil = listExamMarkByClass.Where(x => x.SubjectID == lstSubjectExamByClass[s].SubjectID
                                                                       && lstlstExaminationOfPupil[e].ExaminationsID == x.ExaminationsID).FirstOrDefault();

                        firstSheet.CopyPaste(rangCellMark, (startRow + (indexExam)), StartCol);

                        if (objExamMarkOfPupil != null)
                        {

                            if (lstSubjectExamByClass[s].IsCommenting == SystemParamsInFile.ISCOMMENTING_TYPE_MARK ||
                                    lstSubjectExamByClass[s].IsCommenting == SystemParamsInFile.ISCOMMENTING_TYPE_MARK_JUDGE)
                            {
                                string mark = objExamMarkOfPupil.ExamMark.HasValue ? objExamMarkOfPupil.ExamMark.FormatDecimal() : "";
                                firstSheet.SetCellValue((startRow + indexExam), StartCol, mark);
                            }
                            else
                            {
                                string judgeMark = objExamMarkOfPupil.ExamJudgeMark;
                                firstSheet.SetCellValue((startRow + indexExam), StartCol, judgeMark);
                            }
                        }
                    }
                    else // Xếp hạng trong kỳ thi
                    {
                        var lstcheckLevelPupil = lstCheckLevelByExamOfPupil.Where(x => x.ExaminationID == lstlstExaminationOfPupil[e].ExaminationsID).FirstOrDefault();
                        var lstLevelByExam = lstLevelExam.Where(x => x.ExaminationID == lstlstExaminationOfPupil[e].ExaminationsID).ToList();
                        int levelOfPupilForExam = -1;
                        if (lstcheckLevelPupil != null)
                        {
                            levelOfPupilForExam = lstLevelByExam.FindIndex(x => x.LevelInExamination == lstcheckLevelPupil.LevelInExamination);
                        }

                        firstSheet.CopyPaste(rangCellMark, (startRow + (indexExam)), StartCol);
                        firstSheet.SetCellValue((startRow + indexExam), StartCol, levelOfPupilForExam == -1 ? "" : (levelOfPupilForExam + 1).ToString());
                    }

                }

                StartCol += 3;
            }

            return firstSheet;
        }


        private void FillClassInfoForFirstSemesterReport(int startColumn, ClassProfileTempBO cp,
            int increment, IVTWorksheet sheet, List<ClassSubjectBO> listCS, string HeadMasterName)
        {
            //Khởi tạo các dữ liệu mặc định
            VTVector defaultVector = new VTVector(0, startColumn);
            VTVector cellClass = new VTVector(8, 17) + defaultVector;
            VTVector cellHeadMasterName = new VTVector(26 + increment, 1) + defaultVector;
            VTVector cellHeadTeacher = new VTVector(26 + increment, 9) + defaultVector;
            VTVector cellOrder = new VTVector(12, 1) + defaultVector;
            VTVector cellSubject = new VTVector(12, 2) + defaultVector;

            //Fill dữ liệu môn học, Số thứ tự
            for (int i = 0; i < listCS.Count; i++)
            {
                VTVector indexVector = new VTVector(i, 0);
                sheet.SetCellValue(cellOrder + indexVector, i + 1);
                sheet.SetCellValue(cellSubject + indexVector, listCS[i].DisplayName);
            }

            //Fill dữ liệu lớp học
            sheet.SetCellValue(cellClass, "Lớp: " + cp.DisplayName);

            //Fill dữ liệu giáo viên chủ nhiệm
            if (cp.EmployeeID != null)
            {
                sheet.SetCellValue(cellHeadTeacher, cp.EmployeeName);
            }
            string tmp = "=Thongtinchung!$B$8";
            //sheet.SetCellValue(cellHeadMasterName, tmp);
        }

        private void FillClassInfoForSecondSemesterReport(int startColumn, ClassProfileTempBO cp,
            int increment, IVTWorksheet sheet, List<ClassSubjectBO> listCS, string HeadMasterName)
        {
            //Khởi tạo các dữ liệu mặc định
            VTVector defaultVector = new VTVector(0, startColumn);
            VTVector cellClass = new VTVector(8, 18) + defaultVector;
            VTVector cellHeadMasterName = new VTVector(27 + increment, 1) + defaultVector;
            VTVector cellHeadTeacher = new VTVector(27 + increment, 9) + defaultVector;
            VTVector cellOrder = new VTVector(12, 1) + defaultVector;
            VTVector cellSubject = new VTVector(12, 2) + defaultVector;

            //Fill dữ liệu môn học, Số thứ tự
            for (int i = 0; i < listCS.Count; i++)
            {
                VTVector indexVector = new VTVector(i, 0);
                sheet.SetCellValue(cellOrder + indexVector, i + 1);
                sheet.SetCellValue(cellSubject + indexVector, listCS[i].DisplayName);
            }

            //Fill dữ liệu lớp học
            sheet.SetCellValue(cellClass, "Lớp: " + cp.DisplayName);

            //Fill dữ liệu giáo viên chủ nhiệm
            sheet.SetCellValue(cellHeadTeacher, cp.EmployeeID != null ? cp.EmployeeName : "");
            string tmp = "=Thongtinchung!$B$8";
            sheet.SetCellValue(cellHeadMasterName, tmp);
        }

        private void FillPupilInfoForFirstSemesterReport(int increment, IVTWorksheet sheet, List<ClassSubjectBO> listCS,
            List<PupilAbsenceBO> listPA, List<PupilRankingBO> listPR,
            List<MarkRecordBO> listMR, List<JudgeRecordBO> listJR, List<SummedUpRecordBO> listSR,
            List<PupilEmulation> listPE,
            PupilOfClassBO pp, int startRow, int startColumn, int markNumM, int markNumP, int markNumV)
        {

            //Khởi tạo các dữ liệu mặc định
            VTVector defaultVector = new VTVector(startRow, startColumn);
            VTVector cellPupilCode = new VTVector(8, 3) + defaultVector;
            VTVector cellPupilName = new VTVector(9, 3) + defaultVector;
            VTVector cellAbsenceHasReason = new VTVector(14 + increment, soCotDiemKiemTra < 6 ? 17 : 18) + defaultVector;
            VTVector cellAbsenceNoReason = new VTVector(14 + increment, soCotDiemKiemTra < 6 ? 19 : 20) + defaultVector;
            VTVector cellAverageMark = new VTVector(14 + increment, 4) + defaultVector;
            VTVector cellCapacityLevel = new VTVector(14 + increment, 7) + defaultVector;
            VTVector cellRank = new VTVector(14 + increment, 6) + defaultVector;
            VTVector cellConductLevel = new VTVector(14 + increment, 10) + defaultVector;
            VTVector cellPupilEmulation = new VTVector(14 + increment, 14) + defaultVector;
            VTVector cellCommentTeacher = new VTVector(16 + increment, 9) + defaultVector;
            VTVector cellHeadMasterName = new VTVector(26 + increment, 1) + defaultVector;
            sheet.SetCellValue(cellHeadMasterName, "=Thongtinchung!B8");
            VTVector cellM = new VTVector(12, 3) + defaultVector;
            int lengthM = 5;
            VTVector cellP = new VTVector(12, 8) + defaultVector;
            int lengthP = 5;
            VTVector cellV = new VTVector(12, 13) + defaultVector;
            int lengthV = soCotDiemKiemTra <= 5 ? 5 : 6;
            VTVector cellKTHK = new VTVector(12, soCotDiemKiemTra < 6 ? 18 : 19) + defaultVector;
            VTVector cellTBHK = new VTVector(12, soCotDiemKiemTra < 6 ? 19 : 20) + defaultVector;

            //Fill dữ liệu Mã học sinh
            sheet.SetCellValue(cellPupilCode, pp.PupilCode);

            //Fill dữ liệu Tên học sinh
            sheet.SetCellValue(cellPupilName, pp.PupilFullName);

            //Fill dữ liệu nghỉ học
            int absenceHasReason = (from s in listPA
                                    where s.IsAccepted == true
                                    && s.PupilID == pp.PupilID
                                    select s.IsAccepted).Count();
            int absenceNoReason = (from s in listPA
                                   where s.IsAccepted == false
                                   && s.PupilID == pp.PupilID
                                   select s.IsAccepted).Count();
            sheet.SetCellValue(cellAbsenceHasReason, absenceHasReason);
            sheet.SetCellValue(cellAbsenceNoReason, absenceNoReason);

            //Fill dữ liệu đánh giá học lực
            List<PupilRankingBO> lPR = (from s in listPR
                                        where s.PupilID == pp.PupilID
                                        && s.Semester == SystemParamsInFile.SEMESTER_OF_YEAR_FIRST
                                        select s).ToList();
            if (lPR != null && lPR.Count > 0)
            {
                PupilRankingBO pr = lPR[0];
                string comment = pr.Comment;
                sheet.SetCellValue(cellAverageMark, pr.AverageMark);
                sheet.SetCellValue(cellCapacityLevel, pr.CapacityLevelID != null ? pr.CapacityLevel : "");
                sheet.SetCellValue(cellRank, pr.Rank);
                sheet.SetCellValue(cellConductLevel, pr.ConductLevelID != null ? pr.ConductLevelName : "");
                sheet.SetCellValue(cellCommentTeacher, comment);
                if (!string.IsNullOrEmpty(comment) && comment.Length > 50)
                {
                    sheet.GetRange(startRow + 16 + increment, startColumn + 9, startRow + 20 + increment, startColumn + 9).Merge();
                    sheet.GetRange(startRow + 16 + increment, startColumn + 9, startRow + 20 + increment, startColumn + 9).WrapText();
                    sheet.GetRange(startRow + 16 + increment, startColumn + 9, startRow + 16 + increment, startColumn + 9).SetHAlign(VTHAlign.xlHAlignLeft);
                    sheet.GetRange(startRow + 16 + increment, startColumn + 9, startRow + 16 + increment, startColumn + 9).SetHAlignVer(NativeExcel.XlVAlign.xlVAlignTop);
                }
            }

            //Fill dữ liệu danh hiệu học sinh
            List<PupilEmulation> lPE = (from s in listPE
                                        where s.PupilID == pp.PupilID
                                        && s.Semester == SystemParamsInFile.SEMESTER_OF_YEAR_FIRST
                                        select s).ToList();
            if (lPE != null && lPE.Count > 0)
            {
                PupilEmulation pe = lPE[0];
                sheet.SetCellValue(cellPupilEmulation, UtilsBusiness.ConvertCapacity(pe.HonourAchivementType.Resolution.ToUpper()));
            }

            //Fill dữ liệu điểm 
            for (int indexOfSubject = 0; indexOfSubject < listCS.Count; indexOfSubject++)
            {
                ClassSubjectBO subject = listCS[indexOfSubject];
                VTVector indexVector = new VTVector(indexOfSubject, 0);
                //Fill dữ liệu điểm hệ số 1 và hệ số 2
                if (subject.IsCommenting == SystemParamsInFile.ISCOMMENTING_TYPE_MARK ||
                    subject.IsCommenting == SystemParamsInFile.ISCOMMENTING_TYPE_MARK_JUDGE)
                {
                    //Lấy dữ liệu điểm miệng
                    List<MarkRecordBO> lstM = (from s in listMR
                                               where s.PupilID == pp.PupilID
                                               && s.SubjectID == subject.SubjectID
                                               && s.Title == SystemParamsInFile.MARK_TYPE_M
                                               && s.Semester == SystemParamsInFile.SEMESTER_OF_YEAR_FIRST
                                               select s).OrderBy(x => x.OrderNumber).ToList();

                    //viethd31: fix bug Điền điểm không đúng cột khi các cột điểm đầu null
                    List<string> lM = new List<string>();
                    for (int i = 1; i <= markNumM; i++)
                    {
                        MarkRecordBO mr = lstM.FirstOrDefault(o => o.OrderNumber == i);
                        lM.Add(mr != null && mr.Mark.HasValue ? ReportUtils.ConvertMarkForM(mr.Mark.Value) : null);
                    }

                    //List<string> lM = lstM.Select(x => ReportUtils.ConvertMarkForM(x.Mark.Value)).ToList();

                    sheet.FillDataHorizon(lM, cellM + indexVector, lengthM);

                    //Lấy dữ liệu điểm 15 phút
                    List<MarkRecordBO> lstP = (from s in listMR
                                               where s.PupilID == pp.PupilID
                                               && s.SubjectID == subject.SubjectID
                                               && s.Title == SystemParamsInFile.MARK_TYPE_P
                                               && s.Semester == SystemParamsInFile.SEMESTER_OF_YEAR_FIRST
                                               select s).OrderBy(x => x.OrderNumber).ToList();

                    //viethd31: fix bug Điền điểm không đúng cột khi các cột điểm đầu null
                    List<string> lP = new List<string>();
                    for (int i = 1; i <= markNumP; i++)
                    {
                        MarkRecordBO mr = lstP.FirstOrDefault(o => o.OrderNumber == i);
                        lP.Add(mr != null && mr.Mark.HasValue ? ReportUtils.ConvertMarkForP(mr.Mark.Value) : null);
                    }

                    //List<string> lP = lstP.Select(x => ReportUtils.ConvertMarkForP(x.Mark.Value)).ToList();
                    sheet.FillDataHorizon(lP, cellP + indexVector, lengthP);

                    //Lấy dữ liệu điểm 1 tiết
                    List<MarkRecordBO> lstV = (from s in listMR
                                               where s.PupilID == pp.PupilID
                                               && s.SubjectID == subject.SubjectID
                                               && s.Title == SystemParamsInFile.MARK_TYPE_V
                                               && s.Semester == SystemParamsInFile.SEMESTER_OF_YEAR_FIRST
                                               select s).OrderBy(x => x.OrderNumber).ToList();

                    //viethd31: fix bug Điền điểm không đúng cột khi các cột điểm đầu null
                    List<string> lV = new List<string>();
                    for (int i = 1; i <= markNumV; i++)
                    {
                        MarkRecordBO mr = lstV.FirstOrDefault(o => o.OrderNumber == i);
                        lV.Add(mr != null && mr.Mark.HasValue ? ReportUtils.ConvertMarkForV(mr.Mark.Value) : null);
                    }

                    //List<string> lV = lstV.Select(x => ReportUtils.ConvertMarkForV(x.Mark.Value)).ToList();
                    sheet.FillDataHorizon(lV, cellV + indexVector, lengthV);

                    //Lấy dữ liệu điểm kiểm tra học kỳ
                    List<MarkRecordBO> lstHK = (from s in listMR
                                                where s.PupilID == pp.PupilID
                                                && s.SubjectID == subject.SubjectID
                                                && s.Title == SystemParamsInFile.MARK_TYPE_HK
                                                && s.Semester == SystemParamsInFile.SEMESTER_OF_YEAR_FIRST
                                                select s).OrderBy(x => x.OrderNumber).ToList();
                    if (lstHK != null && lstHK.Count > 0)
                    {
                        sheet.SetCellValue(cellKTHK + indexVector, ReportUtils.ConvertMarkForV(lstHK[0].Mark.Value));
                    }
                }
                else
                {
                    //Lấy dữ liệu điểm miệng
                    List<JudgeRecordBO> lstM = (from s in listJR
                                                where s.PupilID == pp.PupilID
                                                && s.SubjectID == subject.SubjectID
                                                && s.Title == SystemParamsInFile.MARK_TYPE_M
                                                && s.Semester == SystemParamsInFile.SEMESTER_OF_YEAR_FIRST
                                                select s).OrderBy(x => x.OrderNumber).ToList();

                    //viethd31: fix bug Điền điểm không đúng cột khi các cột điểm đầu null
                    List<string> lM = new List<string>();
                    for (int i = 1; i <= markNumM; i++)
                    {
                        JudgeRecordBO mr = lstM.FirstOrDefault(o => o.OrderNumber == i);
                        lM.Add(mr != null ? mr.Judgement : null);
                    }
                    //List<string> lM = lstM.Select(x => x.Judgement).ToList();
                    sheet.FillDataHorizon(lM, cellM + indexVector, lengthM);

                    //Lấy dữ liệu điểm 15 phút
                    List<JudgeRecordBO> lstP = (from s in listJR
                                                where s.PupilID == pp.PupilID
                                                && s.SubjectID == subject.SubjectID
                                                && s.Title == SystemParamsInFile.MARK_TYPE_P
                                                && s.Semester == SystemParamsInFile.SEMESTER_OF_YEAR_FIRST
                                                select s).OrderBy(x => x.OrderNumber).ToList();

                    //viethd31: fix bug Điền điểm không đúng cột khi các cột điểm đầu null
                    List<string> lP = new List<string>();
                    for (int i = 1; i <= markNumP; i++)
                    {
                        JudgeRecordBO mr = lstP.FirstOrDefault(o => o.OrderNumber == i);
                        lP.Add(mr != null ? mr.Judgement : null);
                    }
                    //List<string> lP = lstP.Select(x => x.Judgement).ToList();
                    sheet.FillDataHorizon(lP, cellP + indexVector, lengthP);

                    //Lấy dữ liệu điểm 1 tiết
                    List<JudgeRecordBO> lstV = (from s in listJR
                                                where s.PupilID == pp.PupilID
                                                && s.SubjectID == subject.SubjectID
                                                && s.Title == SystemParamsInFile.MARK_TYPE_V
                                                && s.Semester == SystemParamsInFile.SEMESTER_OF_YEAR_FIRST
                                                select s).OrderBy(x => x.OrderNumber).ToList();

                    //viethd31: fix bug Điền điểm không đúng cột khi các cột điểm đầu null
                    List<string> lV = new List<string>();
                    for (int i = 1; i <= markNumV; i++)
                    {
                        JudgeRecordBO mr = lstV.FirstOrDefault(o => o.OrderNumber == i);
                        lV.Add(mr != null ? mr.Judgement : null);
                    }
                    //List<string> lV = lstV.Select(x => x.Judgement).ToList();
                    sheet.FillDataHorizon(lV, cellV + indexVector, lengthV);

                    //Lấy dữ liệu điểm kiểm tra học kỳ
                    List<JudgeRecordBO> lstHK = (from s in listJR
                                                 where s.PupilID == pp.PupilID
                                                 && s.SubjectID == subject.SubjectID
                                                 && s.Title == SystemParamsInFile.MARK_TYPE_HK
                                                 && s.Semester == SystemParamsInFile.SEMESTER_OF_YEAR_FIRST
                                                 select s).OrderBy(x => x.OrderNumber).ToList();
                    if (lstHK != null && lstHK.Count > 0)
                    {
                        sheet.SetCellValue(cellKTHK + indexVector, lstHK[0].Judgement);
                    }
                }

                //Fill dữ liệu điểm trung bình môn
                List<SummedUpRecordBO> lSR = (from s in listSR
                                              where s.PupilID == pp.PupilID
                                              && s.SubjectID == subject.SubjectID
                                              && s.Semester == SystemParamsInFile.SEMESTER_OF_YEAR_FIRST
                                              select s).ToList();
                if (lSR != null && lSR.Count > 0)
                {

                    if (subject.IsCommenting == SystemParamsInFile.ISCOMMENTING_TYPE_MARK ||
                        subject.IsCommenting == SystemParamsInFile.ISCOMMENTING_TYPE_MARK_JUDGE)
                    {
                        string summedUpMark = lSR[0].SummedUpMark != null ? ReportUtils.ConvertMarkForV(lSR[0].SummedUpMark.Value) : string.Empty;
                        sheet.SetCellValue(cellTBHK + indexVector, summedUpMark);
                    }
                    else
                    {
                        string judgementResult = lSR[0].JudgementResult;
                        sheet.SetCellValue(cellTBHK + indexVector, judgementResult);
                    }
                }
            }
        }

        private void FillPupilInfoForSecondSemesterReport(int increment, IVTWorksheet sheet, List<ClassSubjectBO> listCS,
            List<PupilAbsenceBO> listPA, List<PupilRankingBO> listPR,
            List<MarkRecordBO> listMR, List<JudgeRecordBO> listJR, List<SummedUpRecordBO> listSR,
            List<PupilEmulation> listPE,
            PupilOfClassBO pp, AcademicYear ay, int startRow, int startColumn, int markNumM, int markNumP, int markNumV)
        {

            //Khởi tạo các dữ liệu mặc định
            VTVector defaultVector = new VTVector(startRow, startColumn);
            VTVector cellPupilCode = new VTVector(8, 3) + defaultVector;
            VTVector cellPupilName = new VTVector(9, 3) + defaultVector;
            VTVector cellAbsenceHasReason = new VTVector(14 + increment, soCotDiemKiemTra < 6 ? 17 : 18) + defaultVector;
            VTVector cellAbsenceNoReason = new VTVector(14 + increment, soCotDiemKiemTra < 6 ? 19 : 20) + defaultVector;
            VTVector cellAverageMark = new VTVector(14 + increment, 4) + defaultVector;
            VTVector cellCapacityLevel = new VTVector(14 + increment, 7) + defaultVector;
            VTVector cellRank = new VTVector(14 + increment, 6) + defaultVector;
            VTVector cellConductLevel = new VTVector(14 + increment, 10) + defaultVector;
            VTVector cellPupilEmulation = new VTVector(14 + increment, 14) + defaultVector;
            VTVector cellAbsenceHasReasonAll = new VTVector(15 + increment, soCotDiemKiemTra < 6 ? 17 : 18) + defaultVector;
            VTVector cellAbsenceNoReasonAll = new VTVector(15 + increment, soCotDiemKiemTra < 6 ? 19 : 20) + defaultVector;
            VTVector cellAverageMarkAll = new VTVector(15 + increment, 4) + defaultVector;
            VTVector cellCapacityLevelAll = new VTVector(15 + increment, 7) + defaultVector;
            VTVector cellRankAll = new VTVector(15 + increment, 6) + defaultVector;
            VTVector cellConductLevelAll = new VTVector(15 + increment, 10) + defaultVector;
            VTVector cellPupilEmulationAll = new VTVector(15 + increment, 14) + defaultVector;
            VTVector cellCommentTeacher = new VTVector(17 + increment, 9) + defaultVector;
            VTVector cellM = new VTVector(12, 3) + defaultVector;
            int lengthM = 5;
            VTVector cellP = new VTVector(12, 8) + defaultVector;
            int lengthP = 5;
            VTVector cellV = new VTVector(12, 13) + defaultVector;
            int lengthV = soCotDiemKiemTra <= 5 ? 5 : 6;
            VTVector cellKTHK = new VTVector(12, soCotDiemKiemTra < 6 ? 18 : 19) + defaultVector;
            VTVector cellTBHK = new VTVector(12, soCotDiemKiemTra < 6 ? 19 : 20) + defaultVector;
            VTVector cellTBCN = new VTVector(12, soCotDiemKiemTra < 6 ? 20 : 21) + defaultVector;

            //Fill dữ liệu Mã học sinh
            sheet.SetCellValue(cellPupilCode, pp.PupilCode);

            //Fill dữ liệu Tên học sinh
            sheet.SetCellValue(cellPupilName, pp.PupilFullName);

            //Fill dữ liệu nghỉ học
            int absenceHasReason = (from s in listPA
                                    where s.IsAccepted == true
                                    && s.PupilID == pp.PupilID
                                    && s.AbsentDate >= ay.SecondSemesterStartDate
                                    select s.IsAccepted).Count();
            int absenceHasReasonAll = (from s in listPA
                                       where s.IsAccepted == true
                                       && s.PupilID == pp.PupilID
                                       select s.IsAccepted).Count();
            int absenceNoReason = (from s in listPA
                                   where s.IsAccepted == false
                                   && s.PupilID == pp.PupilID
                                   && s.AbsentDate >= ay.SecondSemesterStartDate
                                   select s.IsAccepted).Count();
            int absenceNoReasonAll = (from s in listPA
                                      where s.IsAccepted == false
                                      && s.PupilID == pp.PupilID
                                      select s.IsAccepted).Count();
            sheet.SetCellValue(cellAbsenceHasReason, absenceHasReason);
            sheet.SetCellValue(cellAbsenceNoReason, absenceNoReason);
            sheet.SetCellValue(cellAbsenceHasReasonAll, absenceHasReasonAll);
            sheet.SetCellValue(cellAbsenceNoReasonAll, absenceNoReasonAll);

            //Fill dữ liệu đánh giá học lực học kỳ 2
            List<PupilRankingBO> lPR = (from s in listPR
                                        where s.PupilID == pp.PupilID
                                        && s.Semester == SystemParamsInFile.SEMESTER_OF_YEAR_SECOND
                                        select s).ToList();
            if (lPR != null && lPR.Count > 0)
            {
                PupilRankingBO pr = lPR[0];
                if (pr != null)
                {
                    string comment = pr.Comment;
                    sheet.SetCellValue(cellAverageMark, pr.AverageMark);
                    sheet.SetCellValue(cellCapacityLevel, pr.CapacityLevelID != null ? pr.CapacityLevel : "");
                    sheet.SetCellValue(cellRank, pr.Rank);
                    sheet.SetCellValue(cellConductLevel, pr.ConductLevelID != null ? pr.ConductLevelName : "");

                    if (!string.IsNullOrEmpty(comment))
                    {
                        sheet.SetCellValue(cellCommentTeacher, comment);
                        if (comment.Length > 50)
                        {
                            sheet.GetRange(startRow + 17 + increment, startColumn + 9, startRow + 20 + increment, startColumn + 9).Merge();
                            sheet.GetRange(startRow + 17 + increment, startColumn + 9, startRow + 20 + increment, startColumn + 9).WrapText();
                            sheet.GetRange(startRow + 17 + increment, startColumn + 9, startRow + 17 + increment, startColumn + 9).SetHAlign(VTHAlign.xlHAlignLeft);
                            sheet.GetRange(startRow + 17 + increment, startColumn + 9, startRow + 17 + increment, startColumn + 9).SetHAlignVer(NativeExcel.XlVAlign.xlVAlignTop);
                        }
                    }
                }
            }

            //Fill dữ liệu đánh giá học lực cả năm
            List<PupilRankingBO> lPRAll = (from s in listPR
                                           where s.PupilID == pp.PupilID
                                           && s.Semester >= SystemParamsInFile.SEMESTER_OF_YEAR_ALL
                                           select s).OrderByDescending(o => o.Semester).ToList();

            if (lPRAll != null && lPRAll.Count > 0)
            {
                PupilRankingBO pr = lPRAll[0];
                if (pr != null)
                {
                    sheet.SetCellValue(cellAverageMarkAll, pr.AverageMark);
                    sheet.SetCellValue(cellCapacityLevelAll, pr.CapacityLevelID != null ? pr.CapacityLevel : "");
                    sheet.SetCellValue(cellRankAll, pr.Rank);
                    sheet.SetCellValue(cellConductLevelAll, pr.ConductLevelID != null ? pr.ConductLevelName : "");
                }
            }

            //Fill dữ liệu danh hiệu học sinh học kỳ 2
            List<PupilEmulation> lPE = (from s in listPE
                                        where s.PupilID == pp.PupilID
                                        && s.Semester == SystemParamsInFile.SEMESTER_OF_YEAR_SECOND
                                        select s).ToList();
            if (lPE != null && lPE.Count > 0)
            {
                PupilEmulation pe = lPE[0];
                sheet.SetCellValue(cellPupilEmulation, UtilsBusiness.ConvertCapacity(pe.HonourAchivementType.Resolution.ToUpper()));
            }

            //Fill dữ liệu danh hiệu học sinh cả năm
            List<PupilEmulation> lPEAll = (from s in listPE
                                           where s.PupilID == pp.PupilID
                                           && s.Semester == SystemParamsInFile.SEMESTER_OF_YEAR_ALL
                                           select s).ToList();
            if (lPEAll != null && lPEAll.Count > 0)
            {
                PupilEmulation pe = lPEAll[0];
                sheet.SetCellValue(cellPupilEmulationAll, UtilsBusiness.ConvertCapacity(pe.HonourAchivementType.Resolution.ToUpper()));
            }

            //Fill dữ liệu điểm 
            for (int indexOfSubject = 0; indexOfSubject < listCS.Count; indexOfSubject++)
            {
                ClassSubjectBO subject = listCS[indexOfSubject];
                VTVector indexVector = new VTVector(indexOfSubject, 0);
                //Fill dữ liệu điểm hệ số 1 và hệ số 2
                if (subject.IsCommenting == SystemParamsInFile.ISCOMMENTING_TYPE_MARK ||
                    subject.IsCommenting == SystemParamsInFile.ISCOMMENTING_TYPE_MARK_JUDGE)
                {
                    //Lấy dữ liệu điểm miệng
                    List<MarkRecordBO> lstM = (from s in listMR
                                               where s.PupilID == pp.PupilID
                                               && s.SubjectID == subject.SubjectID
                                               && s.Title == SystemParamsInFile.MARK_TYPE_M
                                               && s.Semester == SystemParamsInFile.SEMESTER_OF_YEAR_SECOND
                                               select s).OrderBy(x => x.OrderNumber).ToList();

                    //viethd31: fix bug Điền điểm không đúng cột khi các cột điểm đầu null
                    List<string> lM = new List<string>();
                    for (int i = 1; i <= markNumM; i++)
                    {
                        MarkRecordBO mr = lstM.FirstOrDefault(o => o.OrderNumber == i);
                        lM.Add(mr != null && mr.Mark.HasValue ? ReportUtils.ConvertMarkForM(mr.Mark.Value) : null);
                    }

                    //List<string> lM = lstM.Select(x => ReportUtils.ConvertMarkForM(x.Mark.Value)).ToList();

                    sheet.FillDataHorizon(lM, cellM + indexVector, lengthM);

                    //Lấy dữ liệu điểm 15 phút
                    List<MarkRecordBO> lstP = (from s in listMR
                                               where s.PupilID == pp.PupilID
                                               && s.SubjectID == subject.SubjectID
                                               && s.Title == SystemParamsInFile.MARK_TYPE_P
                                               && s.Semester == SystemParamsInFile.SEMESTER_OF_YEAR_SECOND
                                               select s).OrderBy(x => x.OrderNumber).ToList();

                    //viethd31: fix bug Điền điểm không đúng cột khi các cột điểm đầu null
                    List<string> lP = new List<string>();
                    for (int i = 1; i <= markNumP; i++)
                    {
                        MarkRecordBO mr = lstP.FirstOrDefault(o => o.OrderNumber == i);
                        lP.Add(mr != null && mr.Mark.HasValue ? ReportUtils.ConvertMarkForP(mr.Mark.Value) : null);
                    }

                    //List<string> lP = lstP.Select(x => ReportUtils.ConvertMarkForP(x.Mark.Value)).ToList();


                    sheet.FillDataHorizon(lP, cellP + indexVector, lengthP);

                    //Lấy dữ liệu điểm 1 tiết
                    List<MarkRecordBO> lstV = (from s in listMR
                                               where s.PupilID == pp.PupilID
                                               && s.SubjectID == subject.SubjectID
                                               && s.Title == SystemParamsInFile.MARK_TYPE_V
                                               && s.Semester == SystemParamsInFile.SEMESTER_OF_YEAR_SECOND
                                               select s).OrderBy(x => x.OrderNumber).ToList();

                    //viethd31: fix bug Điền điểm không đúng cột khi các cột điểm đầu null
                    List<string> lV = new List<string>();
                    for (int i = 1; i <= markNumV; i++)
                    {
                        MarkRecordBO mr = lstV.FirstOrDefault(o => o.OrderNumber == i);
                        lV.Add(mr != null && mr.Mark.HasValue ? ReportUtils.ConvertMarkForV(mr.Mark.Value) : null);
                    }

                    //List<string> lV = lstV.Select(x => ReportUtils.ConvertMarkForV(x.Mark.Value)).ToList();

                    sheet.FillDataHorizon(lV, cellV + indexVector, lengthV);

                    //Lấy dữ liệu điểm kiểm tra học kỳ
                    List<MarkRecordBO> lstHK = (from s in listMR
                                                where s.PupilID == pp.PupilID
                                                && s.SubjectID == subject.SubjectID
                                                && s.Title == SystemParamsInFile.MARK_TYPE_HK
                                                && s.Semester == SystemParamsInFile.SEMESTER_OF_YEAR_SECOND
                                                select s).OrderBy(x => x.OrderNumber).ToList();
                    if (lstHK != null && lstHK.Count > 0)
                    {
                        sheet.SetCellValue(cellKTHK + indexVector, ReportUtils.ConvertMarkForV(lstHK[0].Mark.Value));
                    }
                }
                else
                {
                    //Lấy dữ liệu điểm miệng
                    List<JudgeRecordBO> lstM = (from s in listJR
                                                where s.PupilID == pp.PupilID
                                                && s.SubjectID == subject.SubjectID
                                                && s.Title == SystemParamsInFile.MARK_TYPE_M
                                                && s.Semester == SystemParamsInFile.SEMESTER_OF_YEAR_SECOND
                                                select s).OrderBy(x => x.OrderNumber).ToList();

                    //viethd31: fix bug Điền điểm không đúng cột khi các cột điểm đầu null
                    List<string> lM = new List<string>();
                    for (int i = 1; i <= markNumM; i++)
                    {
                        JudgeRecordBO mr = lstM.FirstOrDefault(o => o.OrderNumber == i);
                        lM.Add(mr != null ? mr.Judgement : null);
                    }

                    //List<string> lM = lstM.Select(x => x.Judgement).ToList();
                    sheet.FillDataHorizon(lM, cellM + indexVector, lengthM);

                    //Lấy dữ liệu điểm 15 phút
                    List<JudgeRecordBO> lstP = (from s in listJR
                                                where s.PupilID == pp.PupilID
                                                && s.SubjectID == subject.SubjectID
                                                && s.Title == SystemParamsInFile.MARK_TYPE_P
                                                && s.Semester == SystemParamsInFile.SEMESTER_OF_YEAR_SECOND
                                                select s).OrderBy(x => x.OrderNumber).ToList();

                    //viethd31: fix bug Điền điểm không đúng cột khi các cột điểm đầu null
                    List<string> lP = new List<string>();
                    for (int i = 1; i <= markNumP; i++)
                    {
                        JudgeRecordBO mr = lstP.FirstOrDefault(o => o.OrderNumber == i);
                        lP.Add(mr != null ? mr.Judgement : null);
                    }
                    //List<string> lP = lstP.Select(x => x.Judgement).ToList();
                    sheet.FillDataHorizon(lP, cellP + indexVector, lengthP);

                    //Lấy dữ liệu điểm 1 tiết
                    List<JudgeRecordBO> lstV = (from s in listJR
                                                where s.PupilID == pp.PupilID
                                                && s.SubjectID == subject.SubjectID
                                                && s.Title == SystemParamsInFile.MARK_TYPE_V
                                                && s.Semester == SystemParamsInFile.SEMESTER_OF_YEAR_SECOND
                                                select s).OrderBy(x => x.OrderNumber).ToList();

                    //viethd31: fix bug Điền điểm không đúng cột khi các cột điểm đầu null
                    List<string> lV = new List<string>();
                    for (int i = 1; i <= markNumV; i++)
                    {
                        JudgeRecordBO mr = lstV.FirstOrDefault(o => o.OrderNumber == i);
                        lV.Add(mr != null ? mr.Judgement : null);
                    }

                    //List<string> lV = lstV.Select(x => x.Judgement).ToList();
                    sheet.FillDataHorizon(lV, cellV + indexVector, lengthV);

                    //Lấy dữ liệu điểm kiểm tra học kỳ
                    List<JudgeRecordBO> lstHK = (from s in listJR
                                                 where s.PupilID == pp.PupilID
                                                 && s.SubjectID == subject.SubjectID
                                                 && s.Title == SystemParamsInFile.MARK_TYPE_HK
                                                 && s.Semester == SystemParamsInFile.SEMESTER_OF_YEAR_SECOND
                                                 select s).OrderBy(x => x.OrderNumber).ToList();
                    if (lstHK != null && lstHK.Count > 0)
                    {
                        sheet.SetCellValue(cellKTHK + indexVector, lstHK[0].Judgement);
                    }
                }

                //Fill dữ liệu điểm trung bình môn học kỳ 2
                List<SummedUpRecordBO> lSR = (from s in listSR
                                              where s.PupilID == pp.PupilID
                                              && s.SubjectID == subject.SubjectID
                                              && s.Semester == SystemParamsInFile.SEMESTER_OF_YEAR_SECOND
                                              select s).ToList();
                if (lSR != null && lSR.Count > 0)
                {

                    if (subject.IsCommenting == SystemParamsInFile.ISCOMMENTING_TYPE_MARK ||
                        subject.IsCommenting == SystemParamsInFile.ISCOMMENTING_TYPE_MARK_JUDGE)
                    {
                        string summedUpMark = lSR[0].SummedUpMark != null ? ReportUtils.ConvertMarkForV(lSR[0].SummedUpMark.Value) : string.Empty;
                        sheet.SetCellValue(cellTBHK + indexVector, summedUpMark);
                    }
                    else
                    {
                        string judgementResult = lSR[0].JudgementResult;
                        sheet.SetCellValue(cellTBHK + indexVector, judgementResult);
                    }
                }

                //Fill dữ liệu điểm trung bình môn học cả năm
                List<SummedUpRecordBO> lSRAll = (from s in listSR
                                                 where s.PupilID == pp.PupilID
                                                 && s.SubjectID == subject.SubjectID
                                                 && s.Semester >= SystemParamsInFile.SEMESTER_OF_YEAR_ALL
                                                 select s).OrderByDescending(o => o.Semester).ToList();
                if (lSRAll != null && lSRAll.Count > 0)
                {

                    if (subject.IsCommenting == SystemParamsInFile.ISCOMMENTING_TYPE_MARK ||
                        subject.IsCommenting == SystemParamsInFile.ISCOMMENTING_TYPE_MARK_JUDGE)
                    {
                        if (lSRAll[0].ReTestMark != null)
                        {
                            lSRAll[0].SummedUpMark = lSRAll[0].ReTestMark;
                        }
                        //decimal? summedUpMark = lSRAll[0].SummedUpMark;
                        string summedUpMark = lSRAll[0].SummedUpMark != null ? ReportUtils.ConvertMarkForV(lSRAll[0].SummedUpMark.Value) : string.Empty;
                        sheet.SetCellValue(cellTBCN + indexVector, summedUpMark);
                    }
                    else
                    {
                        if (lSRAll[0].ReTestJudgement != null && lSRAll[0].ReTestJudgement != " ")
                        {
                            lSRAll[0].JudgementResult = lSRAll[0].ReTestJudgement;
                        }
                        string judgementResult = lSRAll[0].JudgementResult;
                        sheet.SetCellValue(cellTBCN + indexVector, judgementResult);
                    }
                }
            }
        }
        #endregion

        private string GetSemesterReportCode(PointReportBO entity)
        {
            string reportCode = "";
            if (entity.ReportPatternID == 1)
            {
                if (entity.Semester == SystemParamsInFile.SEMESTER_OF_YEAR_FIRST)
                {
                    reportCode = (entity.PaperSize == SystemParamsInFile.PAPER_SIZE_A4) ?
                   SystemParamsInFile.REPORT_PHIEU_BAO_DIEM_THEO_KY_I_A4 : SystemParamsInFile.REPORT_PHIEU_BAO_DIEM_THEO_KY_I_A5;
                }
                else
                {
                    reportCode = (entity.PaperSize == SystemParamsInFile.PAPER_SIZE_A4) ?
                   SystemParamsInFile.REPORT_PHIEU_BAO_DIEM_THEO_KY_II_A4 : SystemParamsInFile.REPORT_PHIEU_BAO_DIEM_THEO_KY_II_A5;
                }
            }
            else
            {
                if (entity.Semester == SystemParamsInFile.SEMESTER_OF_YEAR_FIRST)
                {
                    reportCode = SystemParamsInFile.REPORT_PHIEU_BAO_DIEM_THEO_KY_I_MAU_2;
                }
                else
                {
                    reportCode = SystemParamsInFile.REPORT_PHIEU_BAO_DIEM_THEO_KY_II_MAU_2;
                }
            }

            return reportCode;
        }
    }
}