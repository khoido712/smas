﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SMAS.Business.BusinessObject;
using SMAS.Business.Common;
using SMAS.Models.Models;
using System.IO;
using SMAS.VTUtils.Excel.Export;

namespace SMAS.Business.Business
{
    /// <summary>
    /// Bang diem lop cap 1
    /// </summary>
    /// <author>AuNH</author>
    /// <date>11/20/2012</date>
    public partial class TranscriptOfPrimaryClassBusiness
    {
        // Thang va thu tu trong bao cao
        private Dictionary<int, int> dicOrderMonth = new Dictionary<int, int>() {
                {9,1},
                {10,2},
                {11,3},
                {12,4},
                {1,5},
                {2,6},
                {3,7},
                {4,8},
                {5,9}
        };

        private class PupilBO
        {
            public int PupilProfileID { get; set; }
			public string Name{get;set;}
            public string FullName { get; set; }
            public int ProfileStatus { get; set; }
            public int ClassID { get; set; }
            public int? OrderInClass { get; set; }
        }

        private string GetReportCode(TranscriptOfClass Entity)
        {
            string ReportCode = "";
            if (Entity.Semester == SystemParamsInFile.SEMESTER_OF_YEAR_FIRST)
            {
                ReportCode = SystemParamsInFile.REPORT_BANGDIEMMONHOCCAP1KYI;
            }
            else if (Entity.Semester == SystemParamsInFile.SEMESTER_OF_YEAR_SECOND)
            {
                ReportCode = SystemParamsInFile.REPORT_BANGDIEMMONHOCCAP1KYII;
            }
            else if (Entity.Semester == SystemParamsInFile.SEMESTER_OF_YEAR_ALL)
            {
                ReportCode = SystemParamsInFile.REPORT_BANGDIEMMONHOCCAP1CN;
            }

            return ReportCode;
        }

        private WorkBookData BuildReportDataForSemester(List<ClassSubject> ListSubject,
            List<PupilBO> ListPupil, List<int> ListPupilHK, List<SummedUpRecordBO> ListSummedUp, List<VMarkRecord> ListMarkRecord,
            List<VJudgeRecord> ListJudgeRecord)
        {
            WorkBookData WorkBookData = new WorkBookData();

            string ClassName = "";
            string AcademicYear = "";
            string SchoolName = "";
            string ProvinceName = "";
            string DeptName = "";
            int index;
            ClassSubject Subject = new ClassSubject();
            for (int i = 0, tempListSubject = ListSubject.Count(); i < tempListSubject; i++)
            {
                Subject = ListSubject[i];
                SheetData SheetData = new SheetData(Subject.SubjectCat.SubjectName);
                SheetData.Type = Subject.IsCommenting.Value;
                index = 1;

                if (ClassName == "")
                {
                    ClassName = Subject.ClassProfile.DisplayName;
                }

                if (AcademicYear == "")
                {
                    AcademicYear = Subject.ClassProfile.AcademicYear.DisplayTitle;
                }

                if (SchoolName == "")
                {
                    SchoolName = Subject.ClassProfile.SchoolProfile.SchoolName;
                }

                if (ProvinceName == "")
                {
                    ProvinceName = Subject.ClassProfile.SchoolProfile.Province.ProvinceName;
                }

                if (DeptName == "")
                {
                    DeptName = Subject.ClassProfile.SchoolProfile.SupervisingDept.SupervisingDeptName;
                }

                List<object> ListRows = new List<object>();

                Dictionary<int, bool> NotStudyingPupil = new Dictionary<int, bool>();
                Dictionary<int, bool> isCheck = new Dictionary<int, bool>();
                PupilBO Pupil = new PupilBO();
				ListPupil = ListPupil.OrderBy(o => o.OrderInClass).ThenBy(o => o.Name).ThenBy(o => o.FullName).ToList();
                for (int j = 0, tempListPupil = ListPupil.Count(); j < tempListPupil; j++)
                {
                    Pupil = ListPupil[j];
                    NotStudyingPupil[j] = !ListPupilHK.Any(u => u == Pupil.PupilProfileID);
                    bool isShow = ListPupilHK.Any(u => u == Pupil.PupilProfileID);
                    WorkBookData.Data["NotStudyingPupil"] = NotStudyingPupil;
                    isCheck[j] = (Pupil.ProfileStatus == SystemParamsInFile.PUPIL_STATUS_STUDYING || Pupil.ProfileStatus == SystemParamsInFile.PUPIL_STATUS_GRADUATED) ? true : false;
                    WorkBookData.Data["isCheck"] = isCheck;
                    SummedUpRecordBO SummedUpRecord = ListSummedUp.Where(o => o.PupilID == Pupil.PupilProfileID && o.SubjectID == Subject.SubjectID).FirstOrDefault();
                    Dictionary<string, object> Row = new Dictionary<string, object>();
                    Row["#"] = index++;
                    Row["FullName"] = Pupil.FullName;
                    if (isShow)
                    {
                        // Neu la mon nhan xet
                        if (Subject.IsCommenting == SystemParamsInFile.ISCOMMENTING_TYPE_JUDGE)
                        {
                            List<VJudgeRecord> List = ListJudgeRecord.Where(o => o.PupilID == Pupil.PupilProfileID && o.SubjectID == Subject.SubjectID).ToList();
                            VJudgeRecord Judge = new VJudgeRecord();
                            for (int k = 0, templistJudge = List.Count(); k < templistJudge; k++)
                            {
                                Judge = List[k];
                                Row["Judge" + Judge.OrderNumber] = Judge.Judgement;
                            }
                            Row["HLM"] = SummedUpRecord == null ? "" : SummedUpRecord.JudgementResult;
                        }
                        // Neu la mon tinh diem
                        else
                        {
                            List<VMarkRecord> List = ListMarkRecord.Where(o => o.PupilID == Pupil.PupilProfileID && o.SubjectID == Subject.SubjectID).ToList();
                            VMarkRecord Mark = new VMarkRecord();
                            for (int k = 0, tempList = List.Count(); k < tempList; k++)
                            {
                                Mark = List[k];
                                int Month = 0;
                                if (Mark.MarkedDate != null)
                                {
                                    if (dicOrderMonth.ContainsKey(Mark.MarkedDate.Value.Month))
                                    {
                                        Month = dicOrderMonth[Mark.MarkedDate.Value.Month];
                                    }
                                }
                                string ArrMark = "";
                                if (Row.ContainsKey("Month" + Month))
                                {
                                    ArrMark = Row["Month" + Month].ToString();
                                }
                                if (Mark.Title != null && (Mark.Title.Equals(SystemParamsInFile.MARK_TYPE_GK1) || Mark.Title.Equals(SystemParamsInFile.MARK_TYPE_GK2)))
                                {
                                    Row["KTDKGK"] = Row.ContainsKey("KTDKGK") ? Row["KTDKGK"].ToString() + " " + Utils.FormatMark(Mark.Mark) : Utils.FormatMark(Mark.Mark);
                                }
                                else if (Mark.Title != null && (Mark.Title.Equals(SystemParamsInFile.MARK_TYPE_CK1) || Mark.Title.Equals(SystemParamsInFile.MARK_TYPE_CK2)))
                                {
                                    Row["KTDKCK"] = SummedUpRecord == null ? "" : Row.ContainsKey("KTDKCK") ? Row["KTDKCK"].ToString() + " " + Utils.FormatMark(SummedUpRecord.SummedUpMark.Value) : Utils.FormatMark(SummedUpRecord.SummedUpMark.Value);
                                }
                                else
                                {
                                    Row["Month" + Month] = ArrMark + " " + Utils.FormatMark(Mark.Mark);
                                }
                            }
                            Row["HLM"] = SummedUpRecord == null ? "" : SMASConvert.ConvertPrimaryCapacityLevel(SummedUpRecord.SummedUpMark);
                        }
                    }

                    ListRows.Add(Row);
                }
                SheetData.Data["Rows"] = ListRows;

                SheetData.Data["Rows"] = ListRows;
                SheetData.Data["ReportDate"] = DateTime.Now;
                SheetData.Data["SubjectName"] = SheetData.Name.ToUpper();
                SheetData.Data["ClassName"] = "LỚP: " + ClassName.ToUpper().Replace("LỚP", "");
                SheetData.Data["AcademicYear"] = AcademicYear;
                SheetData.Data["DeptName"] = DeptName.ToUpper();
                SheetData.Data["SchoolName"] = SchoolName.ToUpper();
                SheetData.Data["ProvinceName"] = ProvinceName;
                WorkBookData.ListSheet.Add(SheetData);
            }

            return WorkBookData;
        }

        private WorkBookData BuildReportDataForAllYear(List<ClassSubject> ListSubject,
            List<PupilBO> ListPupil, List<int> ListPupilHK, List<SummedUpRecordBO> ListSummedUp, List<VMarkRecord> ListMarkRecord,
            List<VJudgeRecord> ListJudgeRecord)
        {
            WorkBookData WorkBookData = new WorkBookData();
            string ClassName = "";
            string AcademicYear = "";
            string SchoolName = "";
            string ProvinceName = "";
            string DeptName = "";

            Dictionary<int, bool> NotStudyingPupil = new Dictionary<int, bool>();
            //int i = 0;

            ClassSubject Subject = new ClassSubject();
            for (int i = 0, tempListSubject = ListSubject.Count(); i < tempListSubject; i++)
            {
                Subject = ListSubject[i];
                //}
                //foreach (ClassSubject Subject in ListSubject)
                //{
                SheetData SheetData = new SheetData(Subject.SubjectCat.SubjectName);
                int index = 1;
                List<object> ListRows = new List<object>();

                if (ClassName == "")
                {
                    ClassName = Subject.ClassProfile.DisplayName;
                }

                if (AcademicYear == "")
                {
                    AcademicYear = Subject.ClassProfile.AcademicYear.DisplayTitle;
                }

                if (SchoolName == "")
                {
                    SchoolName = Subject.ClassProfile.SchoolProfile.SchoolName;
                }

                if (ProvinceName == "")
                {
                    ProvinceName = Subject.ClassProfile.SchoolProfile.Province.ProvinceName;
                }

                if (DeptName == "")
                {
                    DeptName = Subject.ClassProfile.SchoolProfile.SupervisingDept.SupervisingDeptName;
                }
                PupilBO Pupil = new PupilBO();
				ListPupil = ListPupil.OrderBy(o => o.OrderInClass).ThenBy(o => o.Name).ThenBy(o => o.FullName).ToList();
                for (int j = 0, tempListPupil = ListPupil.Count(); j < tempListPupil; j++)
                {
                    Pupil = ListPupil[j];
                    SummedUpRecordBO SummedUpRecordAll = ListSummedUp.Where(o => o.PupilID == Pupil.PupilProfileID && o.Semester == SystemParamsInFile.SEMESTER_OF_YEAR_ALL && o.SubjectID == Subject.SubjectID).FirstOrDefault();
                    SummedUpRecordBO SummedUpRecord1 = ListSummedUp.Where(o => o.PupilID == Pupil.PupilProfileID && o.Semester == SystemParamsInFile.SEMESTER_OF_YEAR_FIRST && o.SubjectID == Subject.SubjectID).FirstOrDefault();
                    SummedUpRecordBO SummedUpRecord2 = ListSummedUp.Where(o => o.PupilID == Pupil.PupilProfileID && o.Semester == SystemParamsInFile.SEMESTER_OF_YEAR_SECOND && o.SubjectID == Subject.SubjectID).FirstOrDefault();

                    NotStudyingPupil[j] = !ListPupilHK.Any(u => u == Pupil.PupilProfileID);
                    WorkBookData.Data["NotStudyingPupil"] = NotStudyingPupil;
                    Dictionary<string, object> Row = new Dictionary<string, object>();
                    Row["#"] = index++;
                    Row["FullName"] = Pupil.FullName;

                    // Neu la mon nhan xet
                    if (Subject.IsCommenting == SystemParamsInFile.ISCOMMENTING_TYPE_JUDGE)
                    {
                        // Hoc ky 1
                        List<VJudgeRecord> List1 = ListJudgeRecord.Where(o => o.PupilID == Pupil.PupilProfileID && o.Semester == SystemParamsInFile.SEMESTER_OF_YEAR_FIRST && o.SubjectID == Subject.SubjectID).ToList();
                        VJudgeRecord Judge = new VJudgeRecord();
                        for (int k = 0, tempJudge1 = List1.Count(); k < tempJudge1; k++)
                        {
                            Judge = List1[k];
                            Row["Judge1_" + Judge.OrderNumber] = Judge.Judgement;
                        }
                        Row["HLM1"] = SummedUpRecord1 == null ? "" : SummedUpRecord1.JudgementResult;

                        // Hoc ky 2
                        List<VJudgeRecord> List2 = ListJudgeRecord.Where(o => o.PupilID == Pupil.PupilProfileID && o.Semester == SystemParamsInFile.SEMESTER_OF_YEAR_SECOND && o.SubjectID == Subject.SubjectID).ToList();
                        for (int l = 0, tempJudge2 = List2.Count(); l < tempJudge2; l++)
                        {
                            Judge = List2[l];
                            Row["Judge2_" + Judge.OrderNumber] = Judge.Judgement;
                        }
                        Row["HLM2"] = SummedUpRecord2 == null ? "" : SummedUpRecord2.JudgementResult;

                        // Ca nam
                        Row["HLM_ALL"] = SummedUpRecordAll == null ? "" : SummedUpRecordAll.JudgementResult;
                    }
                    // Neu la mon tinh diem
                    else
                    {
                        // Hoc ky 1
                        List<VMarkRecord> List1 = ListMarkRecord.Where(o => o.PupilID == Pupil.PupilProfileID && o.Semester == SystemParamsInFile.SEMESTER_OF_YEAR_FIRST && o.SubjectID == Subject.SubjectID).ToList();
                        VMarkRecord Mark = new VMarkRecord();
                        for (int k = 0, tempMark1 = List1.Count(); k < tempMark1; k++)
                        {
                            Mark = List1[k];
                            int Month = 0;
                            if (Mark.MarkedDate != null)
                            {
                                if (dicOrderMonth.ContainsKey(Mark.MarkedDate.Value.Month))
                                {
                                    Month = dicOrderMonth[Mark.MarkedDate.Value.Month];
                                }
                            }
                            string ArrMark = "";
                            if (Row.ContainsKey("Month" + Month))
                            {
                                ArrMark = Row["Month" + Month].ToString();
                            }

                            if (Mark.Title != null && (Mark.Title.Equals(SystemParamsInFile.MARK_TYPE_GK1) || Mark.Title.Equals(SystemParamsInFile.MARK_TYPE_GK2)))
                            {
                                Row["KTDKGK1"] = Row.ContainsKey("KTDKGK1") ? Row["KTDKGK1"].ToString() + " " + Utils.FormatMark(Mark.Mark) : Utils.FormatMark(Mark.Mark);
                            }

                            else if (Mark.Title != null && (Mark.Title.Equals(SystemParamsInFile.MARK_TYPE_CK1) || Mark.Title.Equals(SystemParamsInFile.MARK_TYPE_CK2)))
                            {
                                Row["KTDKCK1"] = Row.ContainsKey("KTDKCK1") ? Row["KTDKCK1"].ToString() + " " + Utils.FormatMark(Mark.Mark) : Utils.FormatMark(Mark.Mark);
                            }
                            else
                            {
                                if (Month <= 5)
                                {
                                    Row["Month" + Month] = ArrMark + " " + Utils.FormatMark(Mark.Mark);
                                }
                            }
                        }

                        Row["HLM1"] = SummedUpRecord1 == null ? "" : SMASConvert.ConvertPrimaryCapacityLevel(SummedUpRecord1.SummedUpMark);

                        // Hoc ky 2
                        List<VMarkRecord> List2 = ListMarkRecord.Where(o => o.PupilID == Pupil.PupilProfileID && o.Semester == SystemParamsInFile.SEMESTER_OF_YEAR_SECOND && o.SubjectID == Subject.SubjectID).ToList();
                        for (int l = 0, tempMark2 = List2.Count(); l < tempMark2; l++)
                        {
                            Mark = List2[l];
                            int Month = 0;
                            if (Mark.MarkedDate != null)
                            {
                                if (dicOrderMonth.ContainsKey(Mark.MarkedDate.Value.Month))
                                {
                                    Month = dicOrderMonth[Mark.MarkedDate.Value.Month];
                                }
                            }
                            string ArrMark = "";
                            if (Row.ContainsKey("Month" + Month))
                            {
                                ArrMark = Row["Month" + Month].ToString();
                            }

                            if (Mark.Title != null && (Mark.Title.Equals(SystemParamsInFile.MARK_TYPE_GK1) || Mark.Title.Equals(SystemParamsInFile.MARK_TYPE_GK2)))
                            {
                                Row["KTDKGK2"] = Row.ContainsKey("KTDKGK2") ? Row["KTDKGK2"].ToString() + " " + Utils.FormatMark(Mark.Mark) : Utils.FormatMark(Mark.Mark);
                            }

                            else if (Mark.Title != null && (Mark.Title.Equals(SystemParamsInFile.MARK_TYPE_CK1) || Mark.Title.Equals(SystemParamsInFile.MARK_TYPE_CK2)))
                            {
                                Row["KTDKCK2"] = Row.ContainsKey("KTDKCK2") ? Row["KTDKCK2"].ToString() + " " + Utils.FormatMark(Mark.Mark) : Utils.FormatMark(Mark.Mark);
                            }
                            else
                            {
                                if (Month > 5)
                                {
                                    Row["Month" + Month] = ArrMark + " " + Utils.FormatMark(Mark.Mark);
                                }
                            }
                        }

                        Row["HLM2"] = SummedUpRecord2 == null ? "" : SMASConvert.ConvertPrimaryCapacityLevel(SummedUpRecord2.SummedUpMark);
                    }

                    ListRows.Add(Row);
                }

                SheetData.Data["Rows"] = ListRows;
                SheetData.Data["ReportDate"] = DateTime.Now;
                SheetData.Data["SubjectName"] = SheetData.Name;
                SheetData.Data["ClassName"] = "LỚP: " + ClassName.ToUpper().Replace("LỚP", "");
                SheetData.Data["AcademicYear"] = AcademicYear;
                SheetData.Data["DeptName"] = DeptName.ToUpper();
                SheetData.Data["SchoolName"] = SchoolName.ToUpper();
                SheetData.Data["ProvinceName"] = ProvinceName;

                WorkBookData.ListSheet.Add(SheetData);
            }

            return WorkBookData;
        }

        private IVTWorkbook WriteToExcel(WorkBookData Data, string ReportCode)
        {
            ReportDefinition reportDef = ReportDefinitionBusiness.GetByCode(ReportCode);
            string templatePath = ReportUtils.GetTemplatePath(reportDef);
            IVTWorkbook oBook = VTExport.OpenWorkbook(templatePath);

            SheetData SheetData = new SheetData();
            IVTWorksheet Sheet = null;
            for (int i = 0, tempdata = Data.ListSheet.Count(); i < tempdata; i++)
            {
                SheetData = Data.ListSheet[i];
                //}
                //foreach (SheetData SheetData in Data.ListSheet)
                //{
                // Neu la mon nhan xet

                if (SheetData.Type == SystemParamsInFile.ISCOMMENTING_TYPE_JUDGE)
                {
                    Sheet = oBook.CopySheetToLast(oBook.GetSheet(2));
                }
                // neu la mon tinh diem
                else
                {
                    Sheet = oBook.CopySheetToLast(oBook.GetSheet(1));
                }
                // Can chinh style
                int StartRow = int.Parse(Sheet.GetRange("A1", "A1").Value.ToString());
                int NumOfRows = ((List<object>)SheetData.Data["Rows"]).Count;
                Dictionary<int, bool> NotStudyingPupil = new Dictionary<int, bool>();
                Dictionary<int, bool> isCheck = new Dictionary<int, bool>();

                if (NumOfRows > 0)
                {
                    NotStudyingPupil = (Dictionary<int, bool>)Data.Data["NotStudyingPupil"];
                    isCheck = (Dictionary<int, bool>)Data.Data["isCheck"];
                }
                for (int j = 0; j < NumOfRows; j++)
                {
                    if (j == 0)
                    {
                        Sheet.CopyAndInsertARow(StartRow, StartRow + 4);
                    }
                    bool IsNotStudying = NotStudyingPupil[j];
                    bool IsNumFive = (j + 1) % 5 == 0 || j + 1 == NumOfRows;
                    bool ischeck = isCheck[j];
                    IVTRange row;
                    //Neu trang thai hoc sinh khac dang hoc
                    if (!ischeck)
                    {
                        if ((j == 0 && NumOfRows == 1) || ((j + 1) == NumOfRows) || ((j + 1) % 5 == 0))
                        {
                            row = Sheet.GetRange(StartRow + 2, 1, StartRow + 2, 100);
                        }
                        else
                        {
                            row = Sheet.GetRange(StartRow + 1, 1, StartRow + 1, 100);
                        }

                    }
                    else
                    {
                        if ((j == 0 && NumOfRows == 1) || ((j + 1) == NumOfRows) || ((j + 1) % 5 == 0))
                        {
                            row = Sheet.GetRange(StartRow + 3, 1, StartRow + 3, 100);
                        }
                        else
                        {
                            row = Sheet.GetRange(StartRow, 1, StartRow, 100);
                        }
                    }
                    Sheet.CopyPaste(row, StartRow + 4 + j, 1, true);                  
                }

                Sheet.DeleteRow(StartRow);
                Sheet.DeleteRow(StartRow);
                Sheet.DeleteRow(StartRow);
                Sheet.DeleteRow(StartRow);


                // Fill du lieu
                Sheet.Name = SheetData.Name;
                Sheet.FillVariableValue(SheetData.Data);
                Sheet.PageMaginTop = 0.5;
                Sheet.PageMaginLeft = 0.5;
                Sheet.PageMaginRight = 0.5;
                Sheet.PageMaginBottom = 0.5;
                Sheet.PageSize = VTXPageSize.VTxlPaperA4;
                Sheet.FitToPage = true;
            }

            oBook.GetSheet(2).Delete();
            oBook.GetSheet(1).Delete();
            return oBook;
        }

        /// <summary>
        /// Lấy mảng băm cho các tham số đầu vào của bảng điểm học sinh cấp 1
        /// </summary>
        /// <param name="Entity">The entity.</param>
        /// <returns>
        /// String
        /// </returns>
        /// <author>AuNH</author>
        /// <date>11/21/2012</date>
        public string GetHashKeyForTranscriptOfPrimaryClass(TranscriptOfClass Entity)
        {
            IDictionary<string, object> dic = new Dictionary<string, object>();
            dic["ClassID"] = Entity.ClassID;
            dic["SubjectID"] = Entity.SubjectID;
            dic["Semester"] = Entity.Semester;

            return ReportUtils.GetHashKey(dic);
        }

        public ProcessedReport InsertTranscriptOfPrimaryClass(TranscriptOfClass Entity, Stream Data)
        {
            // Kiểm tra tính hợp lệ dữ liệu
            IDictionary<string, object> SearchInfo = new Dictionary<string, object>();
            SearchInfo["ClassProfileID"] = Entity.ClassID;
            SearchInfo["SchoolID"] = Entity.SchoolID;
            bool compatible = this.ProcessedReportRepository.ExistsRow(GlobalConstants.SCHOOL_SCHEMA, "ClassProfile", SearchInfo);
            if (!compatible)
            {
                throw new BusinessException("Common_Validate_NotCompatible");
            }

            ProcessedReport ProcessedReport = new ProcessedReport();

            ProcessedReport.ReportCode = this.GetReportCode(Entity);
            ProcessedReport.ProcessedDate = DateTime.Now;
            ProcessedReport.InputParameterHashKey = this.GetHashKeyForTranscriptOfPrimaryClass(Entity);
            ProcessedReport.ReportData = ReportUtils.Compress(Data);

            //Tạo tên file
            ReportDefinition reportDef = ReportDefinitionBusiness.GetByCode(ProcessedReport.ReportCode);
            string outputNamePattern = reportDef.OutputNamePattern;
            string className = this.ClassProfileBusiness.Find(Entity.ClassID).DisplayName;
            string subjectName = Entity.SubjectID > 0 ? this.SubjectCatBusiness.Find(Entity.SubjectID).DisplayName : "";

            outputNamePattern = outputNamePattern.Replace("[Class]", ReportUtils.StripVNSign(className));
            outputNamePattern = outputNamePattern.Replace("[Subject]", ReportUtils.StripVNSign(subjectName));
            ProcessedReport.ReportName = ReportUtils.RemoveSpecialCharacters(outputNamePattern) + "." + reportDef.OutputFormat;

            IDictionary<string, object> dic = new Dictionary<string, object> {
                {"ClassID", Entity.ClassID},
                {"SubjectID ", Entity.SubjectID},
                {"Semester", Entity.Semester},
                {"SchoolID", Entity.SchoolID}
            };

            this.ProcessedReportParameterRepository.Insert(dic, ProcessedReport);
            this.ProcessedReportRepository.Insert(ProcessedReport);

            this.ProcessedReportRepository.Save();

            return ProcessedReport;
        }

        public ProcessedReport GetTranscriptOfPrimaryClass(TranscriptOfClass Entity)
        {
            string ReportCode = this.GetReportCode(Entity);

            string HashKey = this.GetHashKeyForTranscriptOfPrimaryClass(Entity);
            IQueryable<ProcessedReport> query = this.ProcessedReportRepository.All.Where(o => o.ReportCode == ReportCode);
            query = query.Where(o => o.InputParameterHashKey == HashKey);
            query = query.OrderByDescending(o => o.ProcessedDate);

            return query.FirstOrDefault();
        }

        public Stream CreateTranscriptOfPrimaryClass(TranscriptOfClass Entity)
        {
            // Kiểm tra tính hợp lệ dữ liệu
            IDictionary<string, object> SearchInfo = new Dictionary<string, object>();
            SearchInfo["ClassProfileID"] = Entity.ClassID;
            SearchInfo["SchoolID"] = Entity.SchoolID;
            bool compatible = this.ProcessedReportRepository.ExistsRow(GlobalConstants.SCHOOL_SCHEMA, "ClassProfile", SearchInfo);
            if (!compatible)
            {
                throw new BusinessException("Common_Validate_NotCompatible");
            }

            string ReportCode = this.GetReportCode(Entity);
            AcademicYear acaYear = AcademicYearBusiness.Find(Entity.AcademicYearID);
            ReportDefinition ReportDef = ReportDefinitionBusiness.GetByCode(ReportCode);
            string TemplatePath = ReportUtils.GetTemplatePath(ReportDef);

            Dictionary<string, object> data = new Dictionary<string, object>();

            // Danh sach mon hoc
            Dictionary<string, object> dic = new Dictionary<string, object>();
            dic["SubjectID"] = Entity.SubjectID;
            dic["AcademicYearID"] = Entity.AcademicYearID;
            dic["SchoolID"] = Entity.SchoolID;
            List<ClassSubject> ListSubject = this.ClassSubjectBusiness.SearchByClass(Entity.ClassID, dic).OrderBy(o => o.SubjectCat.OrderInSubject).ToList();

            // Danh sach hoc sinh
            List<PupilBO> ListPupil = this.PupilOfClassBusiness.SearchBySchool(Entity.SchoolID, new Dictionary<string, object> { { "ClassID", Entity.ClassID }, { "Check", "Check" } })
                                                                .Select(o => new PupilBO
                                                                {
                                                                    PupilProfileID = o.PupilID,
																	Name = o.PupilProfile.Name,
                                                                    FullName = o.PupilProfile.FullName,
                                                                    ProfileStatus = o.Status,
                                                                    OrderInClass = o.OrderInClass
                                                                })
                                                                .OrderBy(o => o.OrderInClass).ThenBy(o => o.Name).ThenBy(o => o.FullName)
                                                                .ToList();

            // Danh sach hoc sinh
            List<int> ListPupilHK = this.PupilOfClassBusiness.SearchBySchool(Entity.SchoolID, new Dictionary<string, object> { { "ClassID", Entity.ClassID } }).AddCriteriaSemester(acaYear, Entity.Semester).Select(o => o.PupilID).ToList();
            // Thong tin hoc luc
            SearchInfo = new Dictionary<string, object>();
            SearchInfo["ClassID"] = Entity.ClassID;
            SearchInfo["AcademicYearID"] = Entity.AcademicYearID;
            List<SummedUpRecordBO> ListSummedUp = new List<SummedUpRecordBO>();
            if (Entity.Semester == SystemParamsInFile.SEMESTER_OF_YEAR_FIRST)
            {
                SearchInfo["Semester"] = Entity.Semester;
                ListSummedUp = this.VSummedUpRecordBusiness.SearchBySchool(Entity.SchoolID, SearchInfo).Select(u => new SummedUpRecordBO {
                    SummedUpRecordID = u.SummedUpRecordID,
                    SummedUpMark = u.SummedUpMark,
                    ReTestMark = u.ReTestMark,
                    JudgementResult = u.JudgementResult,
                    ReTestJudgement = u.ReTestJudgement,
                    PupilID = u.PupilID,
                    SubjectID = u.SubjectID,
                    ClassID = u.ClassID,
                    SchoolID = u.SchoolID,
                    AcademicYearID = u.AcademicYearID,
                    Semester = u.Semester
                }).ToList();
            }
            else
            {
                IQueryable<SummedUpRecordBO> lstSummed = this.VSummedUpRecordBusiness.SearchBySchool(Entity.SchoolID, SearchInfo).Where(o => o.Semester >= SystemParamsInFile.SEMESTER_OF_YEAR_SECOND).Select(u => new SummedUpRecordBO{
                    SummedUpRecordID = u.SummedUpRecordID,
                    SummedUpMark = u.SummedUpMark,
                    ReTestMark = u.ReTestMark,
                    JudgementResult = u.JudgementResult,
                    ReTestJudgement = u.ReTestJudgement,
                    PupilID = u.PupilID,
                    SubjectID = u.SubjectID,
                    ClassID = u.ClassID,
                    SchoolID = u.SchoolID,
                    AcademicYearID = u.AcademicYearID,
                    Semester = u.Semester
                });
                IQueryable<SummedUpRecordBO> lstSummed1 = lstSummed.Where(u => u.Semester == lstSummed.Where(v => v.PupilID == u.PupilID && v.SubjectID == u.SubjectID).Max(v => v.Semester));
                ListSummedUp = lstSummed1.Where(u => u.SummedUpRecordID == lstSummed1.Where(v => v.PupilID == u.PupilID && v.SubjectID == u.SubjectID).Max(v => v.SummedUpRecordID)).ToList();
            }
            int countSummed = ListSummedUp.Count();
            if (Entity.Semester == SystemParamsInFile.SEMESTER_OF_YEAR_SECOND)
            {
                for (int i = 0; i < countSummed; i++)
                {
                    SummedUpRecordBO sur = ListSummedUp[i];
                    if (sur.ReTestMark != null)
                    {
                        sur.SummedUpMark = sur.ReTestMark;
                    }
                    else if (sur.ReTestJudgement != null && sur.ReTestJudgement != " ")
                    {
                        sur.JudgementResult = sur.ReTestJudgement;
                    }
                }
            }
            SearchInfo = new Dictionary<string, object>();
            SearchInfo["ClassID"] = Entity.ClassID;
            SearchInfo["AcademicYearID"] = Entity.AcademicYearID;
            SearchInfo["Semester"] = Entity.Semester;
            SearchInfo["Year"] = acaYear.Year;
            List<VMarkRecord> ListMarkRecord = this.VMarkRecordBusiness.SearchBySchool(Entity.SchoolID, SearchInfo).ToList();

            List<VJudgeRecord> ListJudgeRecord = this.VJudgeRecordBusiness.SearchBySchool(Entity.SchoolID, SearchInfo).ToList();

            WorkBookData Data = null;

            if (Entity.Semester == SystemParamsInFile.SEMESTER_OF_YEAR_ALL)
            {
                Data = this.BuildReportDataForAllYear(ListSubject, ListPupil, ListPupilHK, ListSummedUp, ListMarkRecord, ListJudgeRecord);
            }
            else
            {
                    Data = this.BuildReportDataForSemester(ListSubject, ListPupil, ListPupilHK, ListSummedUp, ListMarkRecord, ListJudgeRecord);
                }

            IVTWorkbook oBook = this.WriteToExcel(Data, ReportCode);
            return oBook.ToStream();
        }

        // For sumary
        public string GetHashKeyForSummariseTranscriptOfPrimaryClass(TranscriptOfClass Entity)
        {

            IDictionary<string, object> dic = new Dictionary<string, object>();
            dic["ClassID"] = Entity.ClassID;
            dic["AcademicYearID"] = Entity.AcademicYearID;
            dic["EducationLevelID "] = Entity.EducationLevelID;
            dic["Semester"] = Entity.Semester;

            return ReportUtils.GetHashKey(dic);
        }

        public ProcessedReport InsertSummariseTranscriptOfPrimaryClass(TranscriptOfClass Entity, Stream Data)
        {
            ProcessedReport ProcessedReport = new ProcessedReport();

            ProcessedReport.ReportCode = SystemParamsInFile.REPORT_BANGDIEMTONGHOPLOPCAP1;
            ProcessedReport.ProcessedDate = DateTime.Now;
            ProcessedReport.InputParameterHashKey = this.GetHashKeyForSummariseTranscriptOfPrimaryClass(Entity);
            ProcessedReport.ReportData = ReportUtils.Compress(Data);

            //Tạo tên file
            ReportDefinition reportDef = ReportDefinitionBusiness.GetByCode(ProcessedReport.ReportCode);
            string outputNamePattern = reportDef.OutputNamePattern;
            string className = Entity.ClassID > 0 ? this.ClassProfileBusiness.Find(Entity.ClassID).DisplayName : "";
            string educationLevel = Entity.ClassID > 0 ? "" : EducationLevelBusiness.Find(Entity.EducationLevelID).Resolution;
            string semester = ReportUtils.ConvertSemesterForReportName(Entity.Semester);

            outputNamePattern = outputNamePattern.Replace("[EducationLevel/Class]", Entity.ClassID > 0 ? ReportUtils.StripVNSign(className) : ReportUtils.StripVNSign(educationLevel));
            outputNamePattern = outputNamePattern.Replace("[Semester]", ReportUtils.StripVNSign(semester));
            ProcessedReport.ReportName = ReportUtils.RemoveSpecialCharacters(outputNamePattern) + "." + reportDef.OutputFormat;

            IDictionary<string, object> dic = new Dictionary<string, object> {
                {"ClassID", Entity.ClassID},
                {"AcademicYearID ", Entity.AcademicYearID},
                {"Semester", Entity.Semester},
                {"EducationLevelID", Entity.EducationLevelID},
                {"SchoolID", Entity.SchoolID}
            };

            this.ProcessedReportParameterRepository.Insert(dic, ProcessedReport);
            this.ProcessedReportRepository.Insert(ProcessedReport);

            this.ProcessedReportRepository.Save();

            return ProcessedReport;
        }

        public ProcessedReport GetSummariseTranscriptOfPrimaryClass(TranscriptOfClass Entity)
        {
            string ReportCode = SystemParamsInFile.REPORT_BANGDIEMTONGHOPLOPCAP1;

            string HashKey = this.GetHashKeyForSummariseTranscriptOfPrimaryClass(Entity);
            IQueryable<ProcessedReport> query = this.ProcessedReportRepository.All.Where(o => o.ReportCode == ReportCode);
            query = query.Where(o => o.InputParameterHashKey == HashKey);
            query = query.OrderByDescending(o => o.ProcessedDate);

            return query.FirstOrDefault();
        }

        public Stream CreateSummariseTranscriptOfPrimaryClass(TranscriptOfClass Entity)
        {
            string ReportCode = SystemParamsInFile.REPORT_BANGDIEMTONGHOPLOPCAP1;
            AcademicYear acaYear = AcademicYearBusiness.Find(Entity.AcademicYearID);
            ReportDefinition ReportDef = ReportDefinitionBusiness.GetByCode(ReportCode);
            string TemplatePath = ReportUtils.GetTemplatePath(ReportDef);

            Dictionary<string, object> dic = new Dictionary<string, object>();

            // Danh sach lop hoc
            dic["AcademicYearID"] = Entity.AcademicYearID;
            dic["EducationLevelID"] = Entity.EducationLevelID;
            dic["ClassProfileID"] = Entity.ClassID;
            dic["SchoolID"] = Entity.SchoolID;
            List<ClassProfile> ListClass = ClassProfileBusiness.SearchBySchool(Entity.SchoolID, dic).OrderBy(o => o.DisplayName).ToList();

            // Danh sach mon hoc
            dic = new Dictionary<string, object>();
            dic["AcademicYearID"] = Entity.AcademicYearID;
            dic["EducationLevelID"] = Entity.EducationLevelID;
            dic["ClassID"] = Entity.ClassID;
            dic["Semester"] = Entity.Semester;
            dic["SchoolID"] = Entity.SchoolID;
            List<ClassSubject> ListSubject = this.ClassSubjectBusiness.SearchBySchool(Entity.SchoolID, dic).Where(o => o.SubjectCat.IsActive == true).OrderBy(o => o.SubjectCat.OrderInSubject).ToList();

            // Danh sach hoc sinh
            dic = new Dictionary<string, object>();
            dic["AcademicYearID"] = Entity.AcademicYearID;
            dic["EducationLevelID"] = Entity.EducationLevelID;
            dic["ClassID"] = Entity.ClassID;
            dic["SchoolID"] = Entity.SchoolID;
            //dic["Semester"] = Entity.Semester;
            dic["CheckWithClass"] = "CheckWithClass";
            List<PupilBO> ListPupil = this.PupilOfClassBusiness.SearchBySchool(Entity.SchoolID, dic)
                                            .Select(o => new PupilBO
                                            {
                                                ClassID = o.ClassID,
                                                PupilProfileID = o.PupilID,
												Name = o.PupilProfile.Name,
                                                FullName = o.PupilProfile.FullName,
                                                ProfileStatus = o.Status,
                                                OrderInClass = o.OrderInClass
                                            })
                                            .OrderBy(o => o.OrderInClass).ThenBy(o => o.Name).ThenBy(o => o.FullName)
                                            .ToList();

            dic = new Dictionary<string, object>();
            dic["AcademicYearID"] = Entity.AcademicYearID;
            dic["EducationLevelID"] = Entity.EducationLevelID;
            dic["ClassID"] = Entity.ClassID;
            dic["SchoolID"] = Entity.SchoolID;
            List<PupilBO> ListPupilHK = this.PupilOfClassBusiness.SearchBySchool(Entity.SchoolID, dic)
                                            .AddCriteriaSemester(acaYear, Entity.Semester)
                                            .Select(o => new PupilBO
                                            {
                                                ClassID = o.ClassID,
                                                PupilProfileID = o.PupilID,
												Name = o.PupilProfile.Name,
                                                FullName = o.PupilProfile.FullName,
                                                ProfileStatus = o.Status,
                                                OrderInClass = o.OrderInClass
                                            })
                                            .ToList();

            // Thong tin nghi hoc CP, KP
            AcademicYear AcadYear = AcademicYearBusiness.Find(Entity.AcademicYearID);
            dic = new Dictionary<string, object>();
            dic["AcademicYearID"] = Entity.AcademicYearID;
            dic["EducationLevelID"] = Entity.EducationLevelID;
            dic["ClassID"] = Entity.ClassID;
            dic["FromAbsentDate"] = Entity.Semester == SystemParamsInFile.SEMESTER_OF_YEAR_FIRST ? AcadYear.FirstSemesterStartDate : AcadYear.SecondSemesterStartDate;
            dic["ToAbsentDate"] = Entity.Semester == SystemParamsInFile.SEMESTER_OF_YEAR_FIRST ? AcadYear.FirstSemesterEndDate : AcadYear.SecondSemesterEndDate;

            List<PupilAbsence> ListAbsence = PupilAbsenceBusiness.SearchBySchool(Entity.SchoolID, dic).ToList();

            // Thong tin hoc luc
            dic = new Dictionary<string, object>();
            dic["AcademicYearID"] = Entity.AcademicYearID;
            dic["EducationLevelID"] = Entity.EducationLevelID;
            dic["ClassID"] = Entity.ClassID;

            List<SummedUpRecordBO> ListSummedUp = new List<SummedUpRecordBO>();
            if (Entity.Semester == SystemParamsInFile.SEMESTER_OF_YEAR_FIRST)
            {
                dic["Semester"] = Entity.Semester;
                ListSummedUp = this.VSummedUpRecordBusiness.SearchBySchool(Entity.SchoolID, dic).Select(u => new SummedUpRecordBO {
                    SummedUpRecordID = u.SummedUpRecordID,
                    SummedUpMark = u.SummedUpMark,
                    ReTestMark = u.ReTestMark,
                    JudgementResult = u.JudgementResult,
                    ReTestJudgement = u.ReTestJudgement,
                    PupilID = u.PupilID,
                    SubjectID = u.SubjectID,
                    ClassID = u.ClassID,
                    SchoolID = u.SchoolID,
                    AcademicYearID = u.AcademicYearID,
                    Semester = u.Semester
                }).ToList();
            }
            else
            {
                IQueryable<SummedUpRecordBO> lstSummedUp = this.VSummedUpRecordBusiness.SearchBySchool(Entity.SchoolID, dic).Where(o => o.Semester >= SystemParamsInFile.SEMESTER_OF_YEAR_SECOND).Select(u => new SummedUpRecordBO{
                    SummedUpRecordID = u.SummedUpRecordID,
                    SummedUpMark = u.SummedUpMark,
                    ReTestMark = u.ReTestMark,
                    JudgementResult = u.JudgementResult,
                    ReTestJudgement = u.ReTestJudgement,
                    PupilID = u.PupilID,
                    SubjectID = u.SubjectID,
                    ClassID = u.ClassID,
                    SchoolID = u.SchoolID,
                    AcademicYearID = u.AcademicYearID,
                    Semester = u.Semester
                });
                IQueryable<SummedUpRecordBO> lstSummedUpTemp = lstSummedUp.Where(u => u.Semester == lstSummedUp.Where(v => v.PupilID == u.PupilID && v.ClassID == u.ClassID && v.SubjectID == u.SubjectID).Max(v => v.Semester));
                ListSummedUp = lstSummedUpTemp.Where(u => u.SummedUpRecordID == lstSummedUpTemp.Where(v => v.PupilID == u.PupilID && v.ClassID == u.ClassID && v.SubjectID == u.SubjectID).Max(v => v.SummedUpRecordID)).ToList();
            }
            if (Entity.Semester == SystemParamsInFile.SEMESTER_OF_YEAR_SECOND)
            {
                foreach (var sur in ListSummedUp)
                {
                    if (sur.ReTestMark != null)
                    {
                        sur.SummedUpMark = sur.ReTestMark;
                    }
                    else if (sur.ReTestJudgement != null && sur.ReTestJudgement != " ")
                    {
                        sur.JudgementResult = sur.ReTestJudgement;
                    }
                }
            }
            // Thông tin HK, XLGD
            dic = new Dictionary<string, object>();
            dic["AcademicYearID"] = Entity.AcademicYearID;
            dic["EducationLevelID"] = Entity.EducationLevelID;
            dic["ClassID"] = Entity.ClassID;
            //dic["Semester"] = Entity.Semester;
            List<PupilRankingBO> ListRanking = new List<PupilRankingBO>();
            if (Entity.Semester == SystemParamsInFile.SEMESTER_OF_YEAR_FIRST)
            {
                dic["Semester"] = Entity.Semester;
                ListRanking = (from p in VPupilRankingBusiness.SearchBySchool(Entity.SchoolID, dic)
                               join q in CapacityLevelBusiness.All on p.CapacityLevelID equals q.CapacityLevelID into g1
                               from j1 in g1.DefaultIfEmpty()
                               join r in ConductLevelBusiness.All on p.ConductLevelID equals r.ConductLevelID into g2
                               from j2 in g2.DefaultIfEmpty()
                                  select new PupilRankingBO { 
                                   PupilRankingID = p.PupilRankingID,
                                   EducationLevelID = p.EducationLevelID,
                                   PupilID = p.PupilID,
                                   ClassID = p.ClassID,
                                   CapacityLevel = j1.CapacityLevel1,
                                   ConductLevelName = j2.Resolution,
                                   ConductLevelID = p.ConductLevelID,
                                   CapacityLevelID = p.CapacityLevelID
                               }).ToList();
            }
            else
            {
                IQueryable<VPupilRanking> lstPupilRanking = VPupilRankingBusiness.SearchBySchool(Entity.SchoolID, dic).Where(o => o.Semester >= SystemParamsInFile.SEMESTER_OF_YEAR_SECOND);
                ListRanking = (from p in lstPupilRanking.Where(u => u.Semester == lstPupilRanking.Where(v => v.PupilID == u.PupilID && v.ClassID == u.ClassID).Max(v => v.Semester))
                               join q in CapacityLevelBusiness.All on p.CapacityLevelID equals q.CapacityLevelID into g1
                               from j1 in g1.DefaultIfEmpty()
                               join r in ConductLevelBusiness.All on p.ConductLevelID equals r.ConductLevelID into g2
                               from j2 in g2.DefaultIfEmpty()
                                  select new PupilRankingBO { 
                                   PupilRankingID = p.PupilRankingID,
                                   EducationLevelID = p.EducationLevelID,
                                   PupilID = p.PupilID,
                                   ClassID = p.ClassID,
                                   CapacityLevel = j1.CapacityLevel1,
                                   ConductLevelName = j2.Resolution,
                                   ConductLevelID = p.ConductLevelID,
                                   CapacityLevelID = p.CapacityLevelID
                               })
                    .ToList();
            }
            // Danh hieu
            dic = new Dictionary<string, object>();
            dic["AcademicYearID"] = Entity.AcademicYearID;
            dic["EducationLevelID"] = Entity.EducationLevelID;
            dic["ClassID"] = Entity.ClassID;
            dic["Semester"] = Entity.Semester;

            List<PupilEmulationBO> ListEmulation = PupilEmulationBusiness.SearchBySchool(Entity.SchoolID, dic)
                .Select(u => new PupilEmulationBO{
                    PupilEmulationID = u.PupilEmulationID,
                    PupilID = u.PupilID,
                    ClassID = u.ClassID,
                    Semester = u.Semester,
                    HonourAchivementTypeID = u.HonourAchivementTypeID,
                    HonourAchivementTypeResolution = u.HonourAchivementType.Resolution
                })
                .ToList();

            // Thong tin mien giam
            dic = new Dictionary<string, object>();
            dic["AcademicYearID"] = Entity.AcademicYearID;
            dic["EducationLevelID"] = Entity.EducationLevelID;
            dic["ClassID"] = Entity.ClassID;

            List<ExemptedSubject> ListExempted = ExemptedSubjectBusiness.SearchBySchool(Entity.SchoolID, dic).ToList();

            WorkBookData Data = null;

            Data = this.BuildReportDataForSummarise(Entity.Semester, ListClass, ListSubject, ListPupil, ListPupilHK, ListAbsence, ListSummedUp, ListRanking, ListEmulation, ListExempted);
            Data.Data["Semester"] = Entity.Semester == SystemParamsInFile.SEMESTER_OF_YEAR_FIRST ? "HỌC KỲ I" : Entity.Semester == SystemParamsInFile.SEMESTER_OF_YEAR_SECOND ? "HỌC KỲ II" : "";

            IVTWorkbook oBook = this.WriteToExcelSummarise(Data, ReportCode);
            return oBook.ToStream();
        }

        private WorkBookData BuildReportDataForSummarise(int Semester, List<ClassProfile> ListClass,
            List<ClassSubject> ListSubject, List<PupilBO> ListPupil, List<PupilBO> ListPupilHK,
            List<PupilAbsence> ListAbsence, List<SummedUpRecordBO> ListSummedUp,
            List<PupilRankingBO> ListRanking, List<PupilEmulationBO> ListEmulation,
            List<ExemptedSubject> ListExempted)
        {
            WorkBookData WorkBookData = new WorkBookData();

            string ClassName = "";
            string AcademicYear = "";
            string SchoolName = "";
            string ProvinceName = "";
            string DeptName = "";
            if (ListClass.Count() <= 0)
            {
                throw new BusinessException("Không có lớp học trong khối được chọn");
            }
            ClassProfile cp = ListClass[0];
            if (AcademicYear == "")
            {
                AcademicYear = cp.AcademicYear.DisplayTitle;
            }

            if (SchoolName == "")
            {
                SchoolName = cp.SchoolProfile.SchoolName;
            }

            if (ProvinceName == "")
            {
                ProvinceName = cp.SchoolProfile.Province.ProvinceName;
            }

            if (DeptName == "")
            {
                DeptName = cp.SchoolProfile.SupervisingDept.SupervisingDeptName;
            }

            foreach (ClassProfile Class in ListClass)
            {
                SheetData SheetData = new SheetData(Class.DisplayName);

                List<PupilBO> ListPupilInClass = ListPupil.Where(o => o.ClassID == Class.ClassProfileID).ToList();
                List<PupilBO> ListPupilInClassHK = ListPupilHK.Where(o => o.ClassID == Class.ClassProfileID).ToList();
                ListPupilInClass = ListPupilInClass.OrderBy(o => o.OrderInClass).ThenBy(o => o.Name).ThenBy(o => o.FullName).ToList();
                Dictionary<int, bool> NotStudyingPupil = new Dictionary<int, bool>();
                Dictionary<int, bool> isCheck = new Dictionary<int, bool>();
                int i = 0;
                int j = 0;
                foreach (PupilBO Pupil in ListPupilInClass)
                {
                    NotStudyingPupil[i++] = !ListPupilInClassHK.Any(u => u.PupilProfileID == Pupil.PupilProfileID);
                    isCheck[j++] = (Pupil.ProfileStatus == SystemParamsInFile.PUPIL_STATUS_STUDYING || Pupil.ProfileStatus == SystemParamsInFile.PUPIL_STATUS_GRADUATED) ? true : false;
                }

                List<ClassSubject> ListClassSubject = ListSubject.Where(o => o.ClassID == Class.ClassProfileID).ToList();

                SheetData.Data["NotStudyingPupil"] = NotStudyingPupil;
                SheetData.Data["isCheck"] = isCheck;
                SheetData.Data["ListSubject"] = ListClassSubject;

                int index = 1;

                //if (ClassName == "")
                //{
                    ClassName = Class.DisplayName;
                //}              

                List<object> ListRows = new List<object>();
                int k = 0;
                foreach (PupilBO Pupil in ListPupilInClass)
                {
                    k++;
                    Dictionary<string, object> Row = new Dictionary<string, object>();
                    Row["#"] = index++;
                    Row["FullName"] = Pupil.FullName;
                    bool isShow = NotStudyingPupil[k - 1];
                    if (!isShow)
                    {
                        foreach (ClassSubject Subject in ListClassSubject)
                        {
                            ExemptedSubject ExemptedSubject = ListExempted.Where(o => o.PupilID == Pupil.PupilProfileID && o.SubjectID == Subject.SubjectID).FirstOrDefault();
                            if (ExemptedSubject != null)
                            {
                                bool ExemptFirstSemester = ExemptedSubject.FirstSemesterExemptType == SystemParamsInFile.EXEMPT_TYPE_ALL;
                                bool ExemptSecondSemester = ExemptedSubject.SecondSemesterExemptType == SystemParamsInFile.EXEMPT_TYPE_ALL;
                                bool isFirstSemester = Semester == SystemParamsInFile.SEMESTER_OF_YEAR_FIRST;

                                if (ExemptFirstSemester && ExemptSecondSemester)
                                {
                                    Row[Subject.SubjectID.ToString()] = "M";
                                }
                                else if (ExemptFirstSemester && isFirstSemester)
                                {
                                    Row[Subject.SubjectID.ToString()] = "M1";
                                }
                                else if (ExemptSecondSemester && !isFirstSemester)
                                {
                                    Row[Subject.SubjectID.ToString()] = "M2";
                                }
                            }

                            if (!Row.ContainsKey(Subject.SubjectID.ToString()))
                            {
                                SummedUpRecordBO SummedUpRecord = ListSummedUp.Where(o => o.PupilID == Pupil.PupilProfileID && o.SubjectID == Subject.SubjectID).FirstOrDefault();
                                if (SummedUpRecord == null)
                                {
                                    continue;
                                }
                                Row[Subject.SubjectID.ToString()] = SummedUpRecord.SummedUpMark == null ? SummedUpRecord.JudgementResult : Utils.FormatMark(SummedUpRecord.SummedUpMark.Value);
                            }
                        }

                        PupilRankingBO Ranking = ListRanking.Where(o => o.ClassID == Class.ClassProfileID && o.PupilID == Pupil.PupilProfileID).FirstOrDefault();
                        List<PupilAbsence> Absence = ListAbsence.Where(o => o.ClassID == Class.ClassProfileID && o.PupilID == Pupil.PupilProfileID).ToList();
                        PupilEmulationBO Emulation = ListEmulation.Where(o => o.ClassID == Class.ClassProfileID && o.PupilID == Pupil.PupilProfileID).FirstOrDefault();

                        Row["HK"] = Ranking == null || Ranking.ConductLevelID == null ? "" : Ranking.ConductLevelName;
                        Row["XLGD"] = Ranking == null || Ranking.CapacityLevel == null ? "" : Ranking.CapacityLevel;
                        Row["CP"] = Absence.Where(o => o.IsAccepted == true).Count();
                        Row["KP"] = Absence.Where(o => o.IsAccepted == false).Count();
                        Row["DH"] = Emulation == null || Emulation.HonourAchivementTypeResolution == null ? "" : UtilsBusiness.ConvertCapacity(Emulation.HonourAchivementTypeResolution);
                    }

                    ListRows.Add(Row);
                }

                SheetData.Data["Rows"] = ListRows;
                SheetData.Data["ReportDate"] = DateTime.Now;
                SheetData.Data["ClassName"] = "LỚP " + ClassName.ToUpper();
                SheetData.Data["AcademicYear"] = AcademicYear;
                SheetData.Data["DeptName"] = DeptName.ToUpper();
                SheetData.Data["SchoolName"] = SchoolName.ToUpper();
                SheetData.Data["ProvinceName"] = ProvinceName;

                WorkBookData.ListSheet.Add(SheetData);
            }

            return WorkBookData;
        }

        private IVTWorkbook WriteToExcelSummarise(WorkBookData Data, string ReportCode)
        {
            ReportDefinition reportDef = ReportDefinitionBusiness.GetByCode(ReportCode);
            string templatePath = ReportUtils.GetTemplatePath(reportDef);
            IVTWorkbook oBook = VTExport.OpenWorkbook(templatePath);

            IVTWorksheet TempSheet = oBook.GetSheet(2);
            IVTWorksheet Sheet1 = oBook.CopySheetToLast(oBook.GetSheet(1));
            foreach (SheetData SheetData in Data.ListSheet)
            {
                // Neu la mon nhan xet
                IVTWorksheet Sheet = oBook.CopySheetToLast(oBook.GetSheet(1));
                // Can chinh style
                int StartRow = int.Parse(Sheet.GetRange("A1", "A1").Value.ToString());
                string strCH = "CỘNG HÒA XÃ HỘI CHŨ NGHĨA VIỆT NAM";
                string strDL = "Độc lập - tự do - hạnh phúc";

                List<ClassSubject> ListSubject = (List<ClassSubject>)SheetData.Data["ListSubject"];
                int ind = 0;
                foreach (ClassSubject Subject in ListSubject)
                {
                    IVTRange RangeSubject = TempSheet.GetRange(1, 1, 6, 1);
                    Sheet.CopyPasteSameSize(RangeSubject, StartRow - 2, 3 + ind);
                    Sheet.CopyPasteSameColumnWidth(RangeSubject, StartRow - 2, 3 + ind);
                    Sheet.GetRange(StartRow - 1, 3 + ind, StartRow - 1, 3 + ind).Value = Subject.SubjectCat.DisplayName;
                    Sheet.GetRange(StartRow, 3 + ind, StartRow, 3 + ind).Value = "${Rows[].get(" + Subject.SubjectID + ")}";
                    ind++;
                }
                //Dòng cộng hòa
                int numberofSub = ListSubject.Count();
                if (numberofSub == 0)
                {
                    numberofSub = 5;
                }
                Sheet.GetRange(2, numberofSub, 2, numberofSub).Value = strCH;
                Sheet.GetRange(2, numberofSub, 2, numberofSub + 7).Merge();
                Sheet.GetRange(2, numberofSub, 2, numberofSub + 7).SetFontStyle(true, null, false, 11, false, false);

                //Dòng độc lập
                Sheet.GetRange(3, numberofSub, 3, numberofSub).Value = strDL;
                Sheet.GetRange(3, numberofSub, 3, numberofSub + 7).Merge();
                Sheet.GetRange(3, numberofSub, 3, numberofSub + 7).SetFontStyle(true, null, false, 11, false, true);
                //Ngày tháng năm
                string strProvince = SheetData.Data["ProvinceName"].ToString() + ", ngày " + DateTime.Now.Day + " tháng " + DateTime.Now.Month + " năm " + DateTime.Now.Year;
                Sheet.GetRange(4, numberofSub, 4, numberofSub + 1).Value = strProvince;
                Sheet.GetRange(4, numberofSub, 4, numberofSub + 7).Merge();
                //Dòng tiêu đề báo cáo
                string strClass = "BẢNG ĐIỂM TỔNG HỢP " + Data.Data["Semester"] + " NĂM HỌC " + SheetData.Data["AcademicYear"].ToString();
                Sheet.GetRange(5, 1, 5, numberofSub + 7).Value = strClass;
                Sheet.GetRange(5, 1, 5, numberofSub + 7).Merge();
                Sheet.GetRange(5, 1, 5, numberofSub + 7).SetFontStyle(true, null, false, 11, false, false);
                //Dòng lớp học
                string strClassName = SheetData.Data["ClassName"].ToString();
                Sheet.GetRange(6, 1, 6, numberofSub + 7).Value = strClassName;
                Sheet.GetRange(6, 1, 6, numberofSub + 7).Merge();
                Sheet.GetRange(6, 1, 6, numberofSub + 7).SetFontStyle(true, null, false, 11, false, false);

                Sheet.GetRange(StartRow - 2, 3, StartRow - 2, 2 + ind).Merge();

                //Sheet.CopyPasteSameSize(TempSheet.GetRange(1, 2, 6, 6), StartRow - 2, 3 + ind);
                IVTRange rangeHK = TempSheet.GetRange(1, 2, 6, 2);
                Sheet.CopyPasteSameSize(rangeHK, StartRow - 2, 3 + ind);
                ind++;
                IVTRange rangeCP = TempSheet.GetRange(1, 3, 6, 3);
                Sheet.CopyPasteSameSize(rangeCP, StartRow - 2, 3 + ind);
                ind++;
                IVTRange rangeKP = TempSheet.GetRange(1, 4, 6, 4);
                Sheet.CopyPasteSameSize(rangeKP, StartRow - 2, 3 + ind);
                ind++;
                IVTRange rangeXLGD = TempSheet.GetRange(1, 5, 6, 5);
                Sheet.CopyPasteSameSize(rangeXLGD, StartRow - 2, 3 + ind);
                ind++;
                IVTRange rangeDH = TempSheet.GetRange(1, 6, 6, 6);
                Sheet.CopyPasteSameSize(rangeDH, StartRow - 2, 3 + ind);

                int NumOfRows = ((List<object>)SheetData.Data["Rows"]).Count;
                Dictionary<int, bool> NotStudyingPupil = (Dictionary<int, bool>)SheetData.Data["NotStudyingPupil"];
                Dictionary<int, bool> isCheck = (Dictionary<int, bool>)SheetData.Data["isCheck"];
                for (int i = 0; i < NumOfRows; i++)
                {
                    if (i == 0)
                    {
                        Sheet.CopyAndInsertARow(StartRow, StartRow + 4);
                    }
                    bool IsNotStudying = NotStudyingPupil[i];
                    bool ischeck = isCheck[i];

                    IVTRange row;
                    //Nếu học sinh không ở trạng thái đang học = true;
                    if (!ischeck)
                    {
                        if ((i == 0 && NumOfRows == 1) || ((i + 1) % 5 == 0) || ((i + 1) == NumOfRows))
                        {
                            row = Sheet.GetRange(StartRow + 2, 1, StartRow + 2, 100);
                        }
                        else
                        {
                            row = Sheet.GetRange(StartRow + 1, 1, StartRow + 1, 100);
                        }
                    }
                    //Nếu học sinh không ở trạng thái đang học = false;
                    else
                    {
                        if ((i == 0 && NumOfRows == 1) || ((i + 1) % 5 == 0) || ((i + 1) == NumOfRows))
                        {
                            row = Sheet.GetRange(StartRow + 3, 1, StartRow + 3, 100);
                        }
                        else
                        {
                            row = Sheet.GetRange(StartRow, 1, StartRow, 100);
                        }
                    }

                    Sheet.CopyPaste(row, StartRow + 4 + i, 1, true);

                }

                Sheet.DeleteRow(StartRow);
                Sheet.DeleteRow(StartRow);
                Sheet.DeleteRow(StartRow);
                Sheet.DeleteRow(StartRow);

                //Sheet.GetRange(StartRow + NumOfRows + 1, 1, StartRow + NumOfRows + 1, 1).Value = TempSheet.GetRange(8, 1, 8, 1).Value;
                Sheet.CopyPaste(TempSheet.GetRange(8, 1, 8, 1), StartRow + NumOfRows + 1, 1);
                int columnBC = 3 + ListSubject.Count();
                Sheet.CopyPaste(TempSheet.GetRange("H8", "K8"), StartRow + NumOfRows + 1, columnBC);

                // Fill du lieu
                SheetData.Data["Semester"] = Data.Data["Semester"];
                string className = "";
                className = Sheet.StandartClassName(SheetData.Name);
                Sheet.Name = className;
                Sheet.FillVariableValue(SheetData.Data);
                Sheet.FitToPage = true;

            }
            oBook.GetSheet(2).Delete();
            oBook.GetSheet(1).Delete();
            oBook.GetSheet(1).Delete();
            return oBook;
        }
    }
}
