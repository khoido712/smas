﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SMAS.Models.Models;
using SMAS.Business.Common;

namespace SMAS.Business.Business
{
    public partial class DOETExamGroupBusiness
    {
        public IQueryable<DOETExamGroup> Search(IDictionary<string, object> search)
        {
            int examinationId = Utils.GetInt(search, "ExaminationsID");
            int appliedLevel = Utils.GetInt(search, "AppliedLevel");
            IQueryable<DOETExamGroup> iquery = DOETExamGroupBusiness.All;
            if (examinationId > 0)
            {
                iquery = iquery.Where(p => p.ExaminationsID == examinationId);
            }
            if (appliedLevel > 0)
            {
                iquery = iquery.Where(p => p.AppliedLevel == appliedLevel);
            }
            return iquery;
        }

        public void InsertOrUpdateGroup(DOETExamGroup groupObj, bool isInsert)
        {
            if (isInsert)
            {
                //insert
                DOETExamGroupBusiness.Insert(groupObj);
                DOETExamGroupBusiness.Save();
            }

            if (!isInsert)
            {
                //update
                // kiem tra ton tai
                DOETExamGroup objUpdate = this.All.Where(pp => pp.ExamGroupID == groupObj.ExamGroupID).FirstOrDefault();
                if (objUpdate != null)
                {
                    objUpdate.ExamGroupName = groupObj.ExamGroupName;
                    objUpdate.ExamGroupCode = groupObj.ExamGroupCode;
                    objUpdate.AppliedLevel = groupObj.AppliedLevel;
                    DOETExamGroupBusiness.Update(objUpdate);
                    DOETExamGroupBusiness.Save();
                }
                else
                {
                    throw new BusinessException("Không tồn tại nhóm mà thầy cô muốn sửa");
                }
            }
        }



        public bool CheckGroupName(int examinationId,string groupName)
        {
            return this.All.Where(p=>p.ExaminationsID == examinationId && p.ExamGroupName == groupName).Count() > 0;
        }

        public bool CheckGroupCode(int examinationId, string groupCode)
        {
          return this.All.Where(p => p.ExaminationsID == examinationId && p.ExamGroupCode == groupCode).Count() > 0;
        }

        public bool CheckGroupNameUpdate(int examinationId,int examGroupId, string groupName)
        {
            return this.All.Where(p => p.ExaminationsID == examinationId && p.ExamGroupName == groupName && p.ExamGroupID != examGroupId).Count() > 0;
        }

        public bool CheckGroupCodeUpdate(int examinationId, int examGroupId, string groupCode)
        {
            return this.All.Where(p => p.ExaminationsID == examinationId && p.ExamGroupCode == groupCode && p.ExamGroupID != examGroupId).Count() > 0;
        }

        public bool CheckExistPupilInGroup(int examinationId, int examgroupId)
        {
            return DOETExamPupilBusiness.All.Where(o => o.ExaminationsID == examinationId && o.ExamGroupID == examgroupId).Count() > 0;
        }
    }
}
