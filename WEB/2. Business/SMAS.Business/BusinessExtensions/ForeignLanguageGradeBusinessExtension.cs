/**
 * The Viettel License
 *
 * Copyright 2012 Viettel Telecom. All rights reserved.
 * VIETTEL PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */

/** 
* @author  
* @version $Revision: $
*/

using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Linq.Expressions;
using System.Web;
using log4net;

using SMAS.DAL.Repository;
using SMAS.DAL.IRepository;
using SMAS.Business.IBusiness;
using SMAS.Models.Models;
using SMAS.Business.Common;

namespace SMAS.Business.Business
{
    public partial class ForeignLanguageGradeBusiness
    {
        /// <summary>
        /// Insert vao CSDL
        /// </summary>
        /// <param name="ForeignLanguageGrade"></param>
        /// <returns></returns>
        public override ForeignLanguageGrade Insert(ForeignLanguageGrade ForeignLanguageGrade)
        {
            // Kiem tra truong bat buoc
            Utils.ValidateRequire(ForeignLanguageGrade.Resolution, "ForeignLanguageGrade_Label_Resolution");
            // Kiem tra truong Resolution maxlength khong duoc vuot qua 50 ky tu
            Utils.ValidateMaxLength(ForeignLanguageGrade.Resolution, 50, "ForeignLanguageGrade_Label_Resolution");
            // Kiem tra trung ten 
            this.CheckDuplicate(ForeignLanguageGrade.Resolution, GlobalConstants.LIST_SCHEMA, "ForeignLanguageGrade", "Resolution", true, ForeignLanguageGrade.ForeignLanguageGradeID, "ForeignLanguageGrade_Label_Resolution");
            // Kiem tra truong mo ta khong duoc vuot qua 400 ky tu
            Utils.ValidateMaxLength(ForeignLanguageGrade.Description, 400, "ForeignLanguageGrade_Label_Description");
            // Them vao CSDL            
            return base.Insert(ForeignLanguageGrade);
        }

        /// <summary>
        /// Cap nhat vao CSDL
        /// </summary>
        /// <param name="ForeignLanguageGrade"></param>
        /// <returns></returns>
        public override ForeignLanguageGrade Update(ForeignLanguageGrade ForeignLanguageGrade)
        {
            // Kiem tra truong bat buoc
            Utils.ValidateRequire(ForeignLanguageGrade.Resolution, "ForeignLanguageGrade_Label_Resolution");
            // Kiem tra truong Resolution maxlength khong duoc vuot qua 50 ky tu
            Utils.ValidateMaxLength(ForeignLanguageGrade.Resolution, 50, "ForeignLanguageGrade_Label_Resolution");
            // Kiem tra trung ten 
            this.CheckDuplicate(ForeignLanguageGrade.Resolution, GlobalConstants.LIST_SCHEMA, "ForeignLanguageGrade", "Resolution", true, ForeignLanguageGrade.ForeignLanguageGradeID, "ForeignLanguageGrade_Label_Resolution");
            // Kiem tra truong mo ta khong duoc vuot qua 400 ky tu
            Utils.ValidateMaxLength(ForeignLanguageGrade.Description, 400, "ForeignLanguageGrade_Label_Description");
            // Kiem tra ten trinh do ngoai ngu da co trong DB
            //this.CheckAvailable(ForeignLanguageGrade.ForeignLanguageGradeID, "ForeignLanguageGrade_Label_Resolution", false);
            new ForeignLanguageGradeBusiness(null).CheckAvailable(ForeignLanguageGrade.ForeignLanguageGradeID, "ForeignLanguageGrade_Label_Resolution", false);
            // Them vao CSDL            
            return base.Update(ForeignLanguageGrade);
        }

        /// <summary>
        /// Xoa trinh do ngoai ngu
        /// </summary>
        /// <param name="ForeignLanguageGradeID"></param>
        public void Delete(int ForeignLanguageGradeID)
        {
            // Kiem tra da ton tai trong  CSDL
            this.CheckAvailable(ForeignLanguageGradeID, "ForeignLanguageGrade_Label_Title", true);
            this.CheckConstraints(GlobalConstants.LIST_SCHEMA, "ForeignLanguageGrade", ForeignLanguageGradeID, "ForeignLanguageGrade_Label_TitleFailed");
            // Xoa bang cach thiet lap IsActive = 0;
            this.Delete(ForeignLanguageGradeID, true);
        }

        /// <summary>
        /// Tim kiem trinh do ngoai ngu
        /// </summary>
        /// <param name="dicParam"></param>
        /// <returns></returns>
        public IQueryable<ForeignLanguageGrade> Search(IDictionary<string, object> dicParam)
        {
            string Resolution = Utils.GetString(dicParam, "Resolution");
            string Description = Utils.GetString(dicParam, "Description");
            bool? IsActive = Utils.GetNullableBool(dicParam, "IsActive");
            int? Type = Utils.GetNullableInt(dicParam, "Type");

            IQueryable<ForeignLanguageGrade> listForeignLanguageGrade = this.ForeignLanguageGradeRepository.All.OrderBy(o => o.Resolution);
            if (!Resolution.Equals(string.Empty))
            {
                listForeignLanguageGrade = listForeignLanguageGrade.Where(ql => ql.Resolution.ToLower().Contains(Resolution.ToLower()));
            }
            if (!Description.Equals(string.Empty))
            {
                listForeignLanguageGrade = listForeignLanguageGrade.Where(ql => ql.Description.ToLower().Contains(Description.ToLower()));
            }
            if (IsActive.HasValue)
            {
                listForeignLanguageGrade = listForeignLanguageGrade.Where(ql => ql.IsActive == true);
            }

            if (Type.HasValue)
                listForeignLanguageGrade = listForeignLanguageGrade.Where(ql => ql.Type == Type);

            return listForeignLanguageGrade;
        }

    }
}