/**
 * The Viettel License
 *
 * Copyright 2012 Viettel Telecom. All rights reserved.
 * VIETTEL PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */

/** 
* @author  
* @version $Revision: $
*/
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Linq.Expressions;
using System.Web;
using log4net;
using SMAS.DAL.Repository;
using SMAS.DAL.IRepository;
using SMAS.Business.IBusiness;
using SMAS.Models.Models;
using SMAS.Business.Common;
using SMAS.Business.BusinessObject;
using System.Transactions;
using SMAS.Business.Common.Extension;

namespace SMAS.Business.Business
{
    public partial class BrandnameRegistrationBusiness
    {

        public IQueryable<SMS_BRANDNAME_REGISTRATION> Search(Dictionary<string, object> dic)
        {
            int UnitID = dic.GetInt("UnitID");
            List<int> lstUnitID = dic.GetIntList("lstUnitID");
            int StatusID = dic.GetInt("StatusID");
            bool isSupervisingDept = dic.GetBool("isSupervisingDept");
            bool isSchool = dic.GetBool("isSchool");
            IQueryable<SMS_BRANDNAME_REGISTRATION> iquery = this.All;
            if (UnitID != 0)
            {
                iquery = iquery.Where(p => p.UNIT_ID == UnitID);
            }
            if (lstUnitID.Count > 0)
            {
                iquery = iquery.Where(p => lstUnitID.Contains(p.UNIT_ID));
            }
            if (StatusID != 0)
            {
                iquery = iquery.Where(p => p.STATUS == StatusID);
            }
            if (isSupervisingDept)
            {
                iquery = iquery.Where(p => p.IS_SUPER_VISING_DEPT == true);
            }
            if (isSchool)
            {
                iquery = iquery.Where(p => p.IS_SUPER_VISING_DEPT == false);
            }
            return iquery;
        }
        public List<SMS_BRANDNAME_REGISTRATION> GetListByUnitID(int unitId, bool isSupervisingDept)
        {
            return this.All.Where(o => o.UNIT_ID == unitId && o.IS_SUPER_VISING_DEPT == isSupervisingDept).OrderByDescending(o => o.CREATE_TIME).ToList();
        }

        public List<BrandNameUsingBO> GetBrandNameUsing(int unitId)
        {
            DateTime dtSend = DateTime.Now.AddDays(-3);

            DateTime dtSend2 = new DateTime(dtSend.Year, dtSend.Month, dtSend.Day);
            string brandName = GlobalConstantsEdu.BRANDNAME_DEFAULT;
            List<BrandNameUsingBO> lstVal = new List<BrandNameUsingBO>();
            var lst = from b in All.Where(b => b.UNIT_ID == unitId && (b.STATUS == GlobalConstantsEdu.STATUS_IS_ACTIVE ||
                                                                    (b.STATUS == GlobalConstantsEdu.STATUS_WAIT_FOR_EXTENDING && b.TO_DATE.HasValue && b.TO_DATE >= dtSend2)
                                                                   ))
                      join p in BrandnameProviderBusiness.All on b.PROVIDER_ID equals p.BRANDNAME_PROVIDER_ID
                      select new BrandNameUsingBO
                      {
                          Brandname = b.BRAND_NAME,
                          BrandnameRegistrationID = b.BRANDNAME_REGISTRATION_ID,
                          ProviderName = p.PROVIDER_NAME,
                          FromDate = b.FROM_DATE,
                          ToDate = b.TO_DATE,
                          Status = b.STATUS,
                          ProviderID = b.PROVIDER_ID
                      };
            return lst.ToList();

        }
    }
}
