﻿/**
 * The Viettel License
 *
 * Copyright 2012 Viettel Telecom. All rights reserved.
 * VIETTEL PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */

/** 
* @author  
* @version $Revision: $
*/

using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Linq.Expressions;
using System.Web;
using log4net;

using SMAS.DAL.Repository;
using SMAS.DAL.IRepository;
using SMAS.Business.IBusiness;
using SMAS.Models.Models;
using SMAS.Business.Common;
using System.IO;
using SMAS.VTUtils.Excel.Export;
using SMAS.Business.BusinessObject;

namespace SMAS.Business.Business
{
    public partial class DailyMenuBusiness
    {
        /// <summary>
        /// Tim kiem theo tu dien truyen vao
        /// </summary>
        /// <param name="SearchInfo"></param>
        /// <returns></returns>
        private IQueryable<DailyMenu> Search(IDictionary<string, object> SearchInfo)
        {
            string DailyMenuCode = Utils.GetString(SearchInfo, "DailyMenuCode");
            int NumberOfChildren = Utils.GetInt(SearchInfo, "NumberOfChildren");
            int MenuType = Utils.GetInt(SearchInfo, "MenuType");
            int EatingGroupID = Utils.GetInt(SearchInfo, "EatingGroupID");
            int SchoolID = Utils.GetInt(SearchInfo, "SchoolID");
            int AcademicYearID = Utils.GetInt(SearchInfo, "AcademicYearID");
            bool? IsActive = Utils.GetIsActive(SearchInfo, "IsActive");
            IQueryable<DailyMenu> ListDailyMenu = this.repository.All;
            if (DailyMenuCode != null && !DailyMenuCode.Equals(string.Empty))
            {
                ListDailyMenu = ListDailyMenu.Where(o => o.DailyMenuCode.ToLower().Contains(DailyMenuCode.Trim().ToLower()));
            }
            if (NumberOfChildren != 0)
            {
                ListDailyMenu = ListDailyMenu.Where(o => o.NumberOfChildren == NumberOfChildren);
            }
            if (MenuType != 0)
            {
                ListDailyMenu = ListDailyMenu.Where(o => o.MenuType == MenuType);
            }
            if (EatingGroupID != 0)
            {
                ListDailyMenu = ListDailyMenu.Where(o => o.EatingGroupID == EatingGroupID);
            }
            if (SchoolID != 0)
            {
                ListDailyMenu = ListDailyMenu.Where(o => o.SchoolID == SchoolID);
            }
            if (AcademicYearID != 0)
            {
                ListDailyMenu = ListDailyMenu.Where(o => o.AcademicYearID == AcademicYearID);
            }

            if (IsActive.HasValue)
            {
                ListDailyMenu = ListDailyMenu.Where(o => o.IsActive == IsActive.Value);
            }
            return ListDailyMenu;

        }

        /// <summary>
        /// Tim kiem theo dien kien truong
        /// </summary>
        /// <param name="SchoolID"></param>
        /// <param name="SearchInfo"></param>
        /// <returns></returns>
        public IQueryable<DailyMenu> SearchBySchool(int SchoolID, IDictionary<string, object> SearchInfo = null)
        {
            if (SchoolID == 0)
            {
                return null;
            }
            if (SearchInfo == null)
            {
                SearchInfo = new Dictionary<string, object>();
            }
            SearchInfo["SchoolID"] = SchoolID;
            return this.Search(SearchInfo);
        }
        /// <summary>
        /// author: namdv3
        /// created date: 13/11/2012
        /// </summary>
        /// <param name="entity"></param>
        /// <param name="lstDailyMenuDetail"></param>
        public void Validation(DailyMenu entity, List<DailyMenuDetail> lstDailyMenuDetail, string type)
        {
            //DailyMenuCode: require, maxlength(50), duplicate
            if (type == "INSERT")
            {
                this.CheckDuplicate(entity.DailyMenuCode, GlobalConstants.NUTRITION_SCHEMA, "DailyMenu", "DailyMenuCode", true, 0, "WeeklyMenu_Column_DalyMenuCode");
            }
            else if (type == "UPDATE")
            {
                this.CheckDuplicate(entity.DailyMenuCode, GlobalConstants.NUTRITION_SCHEMA, "DailyMenu", "DailyMenuCode", true, entity.DailyMenuID, "DailyMenu_Validate_Duplicate_DailyMenuCode");
                //DailyMenuID, SchoolID: not compatible(DailyMenu)
                bool DailyMenuSchoolCompatible = new DailyMenuRepository(this.context).ExistsRow(GlobalConstants.NUTRITION_SCHEMA, "DailyMenu",
                new Dictionary<string, object>()
                {
                    {"DailyMenuID",entity.DailyMenuID},
                    {"SchoolID",entity.SchoolID}
                }, null);
                if (!DailyMenuSchoolCompatible)
                {
                    throw new BusinessException("Common_Validate_NotCompatible");
                }
                //AcademicYearBusiness.IsCurrentYear(AcademicYearID) = FALSE => Năm học không phải năm hiện tại
                if (!AcademicYearBusiness.IsCurrentYear(entity.AcademicYearID))
                    throw new BusinessException("DailyMenu_Validate_IsNotCurrentYear");
            }
            //NumberOfChildren: require, > 0, maxlength(4)
            if (entity.NumberOfChildren <= 0)
                throw new BusinessException("DailyMenu_Validate_NumberOfChildren");
            if (entity.NumberOfChildren > 0 && entity.NumberOfChildren.ToString().Length > 4)
                throw new BusinessException("DailyMenu_Validate_MaxLength_NumberOfChildren");
            //MenuType: in(1,2)
            List<int> listMenuType = new List<int>();
            listMenuType.Add(SystemParamsInFile.MENU_TYPE_GENERAL);
            listMenuType.Add(SystemParamsInFile.MENU_TYPE_ADDITIONAL);
            if (!listMenuType.Contains(entity.MenuType))
                throw new BusinessException("DailyMenu_Validate_MenuType");
            //SchoolID, AcademicYearID: not compatible(AcademicYear)
            bool AcademicYearCompatible = new AcademicYearRepository(this.context).ExistsRow(GlobalConstants.SCHOOL_SCHEMA, "AcademicYear",
                new Dictionary<string, object>()
                {
                    {"SchoolID",entity.SchoolID},
                    {"AcademicYearID",entity.AcademicYearID}
                }, null);
            if (!AcademicYearCompatible)
            {
                throw new BusinessException("Common_Validate_NotCompatible");
            }
        }

        /// <summary>
        /// author: namdv3
        /// created date: 13/11/2012
        /// </summary>
        /// <param name="entity"></param>
        /// <param name="lstDailyMenuDetail"></param>
        public void Insert(DailyMenu entity, List<DailyMenuDetail> lstDailyMenuDetail, int? SchoolID = null, int? AcademicYearID = null)
        {
            ValidationMetadata.ValidateObject(entity);
            if (SchoolID.HasValue && AcademicYearID.HasValue)
            {
                if (this.CheckExistDailyMenuCode(entity.DailyMenuCode, Convert.ToInt32(SchoolID), Convert.ToInt32(AcademicYearID)))
                {
                    Validation(entity, lstDailyMenuDetail, "INSERT");
                }
            }
            entity.DailyMenuDetails = lstDailyMenuDetail;
            this.Insert(entity);
        }

        /// <summary>
        /// author: namdv3
        /// created date: 13/11/2012
        /// </summary>
        /// <param name="entity"></param>
        /// <param name="lstDailyMenuDetail"></param>
        public void Update(DailyMenu entity, List<DailyMenuDetail> lstDailyMenuDetail, int? SchoolID = null, int? AcademicYearID= null)
        {
            ValidationMetadata.ValidateObject(entity);
            Validation(entity, lstDailyMenuDetail, "UPDATE");
            var _lstDailyMenuDetail = DailyMenuDetailRepository.All.Where(o => o.DailyMenuID == entity.DailyMenuID);
            if (_lstDailyMenuDetail.Count() > 0)
            {
                foreach (var item in _lstDailyMenuDetail)
                {
                    DailyMenuDetailBusiness.Delete(item.DailyMenuDetailID);
                }
            }
            entity.DailyMenuDetails = lstDailyMenuDetail;
            this.Update(entity);
        }

        /// <summary>
        /// author: namdv3
        /// created date: 13/11/2012
        /// Xóa thực đơn ngày
        /// </summary>
        /// <param name="DailyMenuID">Id thực đơn ngày</param>
        /// <param name="SchoolID">ID trường học</param>
        public void Delete(int DailyMenuID, int SchoolID)
        {
            /* Validation delete
             *  DailyMenuID => AcademicYearID
             DailyMenuID: PK(DailyMenu)
                DailyMenuID: Using
                SchooID, DailyMenuID: not compatible(DailyMenu)
                AcademicYearBusiness.IsCurrentYear(AcademicYearID) = FALSE => Năm học không phải năm hiện tại

             */
            int AcademicYearID = this.Find(DailyMenuID).AcademicYearID;
            bool DailyMenuSchoolCompatible = new DailyMenuRepository(this.context).ExistsRow(GlobalConstants.NUTRITION_SCHEMA, "DailyMenu",
               new Dictionary<string, object>()
                {
                    {"DailyMenuID",DailyMenuID},
                    {"SchoolID",SchoolID}
                }, null);
            if (!DailyMenuSchoolCompatible)
            {
                throw new BusinessException("Common_Validate_NotCompatible");
            }
            if (!AcademicYearBusiness.IsCurrentYear(AcademicYearID))
                throw new BusinessException("DailyMenu_Validate_IsNotCurrentYear");
            DailyMenu dailymenu = this.Find(DailyMenuID);
            dailymenu.IsActive = false;
            dailymenu.ModifiedDate = DateTime.Now;
            this.Update(dailymenu);
        }

        public bool CheckExistDailyMenuCode(string dailyMenuCode, int SchoolID, int AcademicYearID)
        {
            if (DailyMenuBusiness.All.Where(o => o.SchoolID == SchoolID && o.DailyMenuCode == dailyMenuCode && o.AcademicYearID == AcademicYearID).Count() > 0)
            {
                return true;
            }
            else
                return false;
        }

        //Xuất excel thực đơn ngày
        //PhuongTD
        //21/12/2012
        public Stream CreateDailyMenuReport(int SchoolID, int AcademicYearID, int DailyMenuID)
        {
            string reportCode = SystemParamsInFile.REPORT_THUC_DON_NGAY;
            string formular = "";
            ReportDefinition reportDef = ReportDefinitionBusiness.GetByCode(reportCode);
            string templatePath = SystemParamsInFile.TEMPLATE_FOLDER + reportDef.TemplateDirectory + "/" + reportDef.TemplateName;
            IVTWorkbook oBook = VTExport.OpenWorkbook(templatePath);
            #region Điền giá trị Sheet1 vào sheet tổng hợp
            IVTWorksheet firstSheet = oBook.GetSheet(1);
            DailyMenu dailyMenu = DailyMenuBusiness.Find(DailyMenuID);
            SchoolProfile school = SchoolProfileBusiness.Find(SchoolID);
            Province province = school.Province;
            string schoolName = school.SchoolName.ToUpper();
            string supervisingDeptName = school.SupervisingDept.SupervisingDeptName.ToUpper();
            string provinceName = province.ProvinceName;
            DateTime reportDate = DateTime.Now;//entity.ReportDate;
            Dictionary<string, object> dicGeneralInfo = new Dictionary<string, object> 
                {
                    {"SchoolName", schoolName}, 
                    {"DeptName", supervisingDeptName},
                    {"ProvinceName", provinceName},
                    {"ReportDate", reportDate},
                    {"DailyMenuCode", dailyMenu.DailyMenuCode},
                    {"EatingGroupName", dailyMenu.EatingGroup.EatingGroupName},
                    {"NumberOfChildren", dailyMenu.NumberOfChildren}
                };
            firstSheet.FillVariableValue(dicGeneralInfo);

            //Danh sách chi tiết thực đơn ngày
            Dictionary<string, object> dic = new Dictionary<string, object>();
            dic["DailyMenuID"] = DailyMenuID;
            IQueryable<DailyMenuDetail> lsDailyMenuDetail = DailyMenuDetailBusiness.SearchBySchool(SchoolID, dic);
            //Danh sách bữa ăn
            IQueryable<MealCat> lsMeal = MealCatBusiness.SearchBySchool(SchoolID, new Dictionary<string, object>());
            List<MealCat> lstMeal = new List<MealCat>();
            //Fill Sheet1 vào sheet tổng

            IVTWorksheet sheetData = oBook.CopySheetToLast(firstSheet, "G13");
            IVTRange sheet1_MealName = firstSheet.GetRange("A15", "A15");
            IVTRange sheet1_DishName = firstSheet.GetRange("A16", "A16");
            int sheet1_firstRow = 15;
            if (lsMeal != null && lsMeal.Count() > 0)
            {
                lstMeal = lsMeal.ToList();
            }
            int numberMeal = 0;
            int sheet1_lastRow = 0;
            int maxDish = 0;
            foreach (MealCat meal in lstMeal)
            {
                IQueryable<DailyMenuDetail> lsDailyMenuDetailOfMeal = lsDailyMenuDetail.Where(o => o.MealID == meal.MealCatID);
                if (lsDailyMenuDetailOfMeal != null && lsDailyMenuDetailOfMeal.Count() > 0)
                {
                    if (maxDish < lsDailyMenuDetailOfMeal.Count())
                    {
                        maxDish = lsDailyMenuDetailOfMeal.Count();
                    }
                }
            }
            foreach (MealCat meal in lstMeal)
            {
                numberMeal++;
                sheetData.CopyPasteSameSize(sheet1_MealName, sheet1_firstRow, numberMeal);
                sheetData.SetCellValue(sheet1_firstRow, numberMeal, meal.MealName);
                int numberDish = 0;
                IQueryable<DailyMenuDetail> lsDailyMenuDetailOfMeal = lsDailyMenuDetail.Where(o => o.MealID == meal.MealCatID);
                string concatMealName = "";
                if (lsDailyMenuDetailOfMeal != null && lsDailyMenuDetailOfMeal.Count() > 0)
                {
                    foreach (DailyMenuDetail dailyMenuDetail in lsDailyMenuDetailOfMeal.ToList())
                    {
                        numberDish++;
                        sheetData.CopyPasteSameSize(sheet1_DishName, sheet1_firstRow + numberDish, numberMeal);
                        concatMealName += dailyMenuDetail.DishName + "\r\n";
                    }
                    sheetData.GetRange(sheet1_firstRow + 1, numberMeal, sheet1_firstRow + maxDish, numberMeal).Merge();
                    sheetData.SetCellValue(sheet1_firstRow + 1, numberMeal, concatMealName.Remove(concatMealName.Length - 2));
                }
            }
            sheet1_lastRow = sheet1_firstRow + maxDish;

            #endregion


            #region Lấy dữ liệu thực phẩm - Sheet 5
            List<int> lstFoodID = new List<int>();
            List<decimal> lstWeight = new List<decimal>();
            IVTWorksheet sheet5 = oBook.GetSheet(5);
            dic = new Dictionary<string, object>();
            dic["DailyMenuID"] = DailyMenuID;
            IQueryable<DailyMenuFood> lsDailyMenuFood = DailyMenuFoodBusiness.SearchBySchool(SchoolID, dic);
            if (lsDailyMenuFood != null && lsDailyMenuFood.Count() > 0)
            {
                foreach(DailyMenuFood dailyMenuFood in lsDailyMenuFood.ToList())
                {
                    lstFoodID.Add(dailyMenuFood.FoodID);
                    lstWeight.Add(dailyMenuFood.Weight);
                }
            }
            #endregion
            #region Lấy dữ liệu dinh dưỡng sheet3
            IVTWorksheet sheet3 = oBook.GetSheet(3);
            MineralOfDailyMenuBO mineralOfDailyMenu = FoodCatBusiness.CaculatorContent(lstFoodID, lstWeight, dailyMenu.EatingGroupID, SchoolID, AcademicYearID);

            #endregion

            #region Điền dữ liệu vào sheet 4
            IVTWorksheet sheet4 = oBook.GetSheet(4);
            IDictionary<string, object> dic_nn = new Dictionary<string, object>();
            dic_nn["AcademicYearID"] = AcademicYearID;
            dic_nn["EatingGroupID"] = dailyMenu.EatingGroupID;
            NutritionalNorm nn = NutritionalNormBusiness.SearchBySchool(SchoolID, dic_nn).Where(o => o.EffectDate <= DateTime.Now).OrderByDescending(o => o.EffectDate).FirstOrDefault();
            if (lstFoodID.Count != 0 && nn != null)
            {
                Dictionary<string, object> sheet4_dic = new Dictionary<string, object> 
                {
                    {"PercentProteinAnimal", ((mineralOfDailyMenu.ProteinAnimal + mineralOfDailyMenu.ProteinPlant) != 0) ? mineralOfDailyMenu.ProteinAnimal / (mineralOfDailyMenu.ProteinAnimal + mineralOfDailyMenu.ProteinPlant) * 100 : 0},
                    {"PercentProteinAnimalDM", nn.AnimalProteinFrom + "-" + nn.AnimalProteinTo}, 
                    {"PercentProteinPlant", ((mineralOfDailyMenu.ProteinAnimal + mineralOfDailyMenu.ProteinPlant) != 0) ? mineralOfDailyMenu.ProteinPlant / (mineralOfDailyMenu.ProteinAnimal + mineralOfDailyMenu.ProteinPlant) * 100 : 0}, 
                    {"PercentProteinPlantDM", nn.PlantProteinFrom + "-" + nn.PlantProteinTo}, 
                    {"PercentFatAnimal", ((mineralOfDailyMenu.FatAnimal + mineralOfDailyMenu.FatPlant) != 0) ? mineralOfDailyMenu.FatAnimal / (mineralOfDailyMenu.FatAnimal + mineralOfDailyMenu.FatPlant) * 100 : 0}, 
                    {"PercentFatAnimalDM", nn.AnimalFatFrom + "-" + nn.AnimalFatTo}, 
                    {"PercentFatPlant", ((mineralOfDailyMenu.FatAnimal + mineralOfDailyMenu.FatPlant) != 0) ? mineralOfDailyMenu.FatPlant / (mineralOfDailyMenu.FatAnimal + mineralOfDailyMenu.FatPlant) * 100 : 0}, 
                    {"PercentFatPlantDM", nn.PlantFatFrom + "-" + nn.PlantFatTo},
                    {"PercentProtein", (mineralOfDailyMenu.Calo != 0) ? Math.Round((mineralOfDailyMenu.ProteinAnimal + mineralOfDailyMenu.ProteinPlant) * 4 / mineralOfDailyMenu.Calo * 100, 2) : 0},
                    {"PercentFat", (mineralOfDailyMenu.Calo != 0) ? Math.Round((mineralOfDailyMenu.FatPlant + mineralOfDailyMenu.FatAnimal) * 4 / mineralOfDailyMenu.Calo * 100, 2) : 0},
                    {"PercentSugar", (mineralOfDailyMenu.Calo != 0) ? Math.Round(mineralOfDailyMenu.Sugar * 4 / mineralOfDailyMenu.Calo * 100, 2) : 0},
                    {"PercentProteinDM", nn.ProteinFrom + "-" + nn.ProteinTo},
                    {"PercentFatDM", nn.FatFrom + "-" + nn.FatTo},
                    {"PercentSugarDM", nn.SugarFrom + "-" + nn.SugarTo}
                };
                sheet4.FillVariableValue(sheet4_dic);
            }
            #endregion


            #region Tạo sheet 2
            IVTWorksheet sheet2 = oBook.GetSheet(2);
            IVTRange sheet2_header = sheet2.GetRange("A6", "C8");
            IVTRange sheet2_row = sheet2.GetRange("A9", "C9");
            #endregion

            #region Tạo sheet6

            IVTWorksheet sheet6 = oBook.GetSheet(6);
            List<OtherService> ListOtherService = OtherServiceBusiness.SearchBySchool(SchoolID, new Dictionary<string, object>()).ToList();

            #endregion

            #region Nếu đã nhập thực phẩm thì fill dữ liệu
            if (lstFoodID.Count > 0)
            {
                int sheet2_firstRow = sheet1_lastRow + 2;
                sheetData.CopyPasteSameSize(sheet2_header, sheet2_firstRow, 1);
                List<Object> lstMealData = new List<object>();
                List<RateByMealBO> lstRateByMeal = FoodCatBusiness.CaculatorRateByMeal(DailyMenuID, SchoolID, AcademicYearID, dailyMenu.EatingGroupID);
                if (lstRateByMeal != null && lstRateByMeal.Count > 0)
                {
                    int countMeal = 0;
                    foreach (RateByMealBO rateByMeal in lstRateByMeal)
                    {
                        countMeal++;
                        sheetData.CopyPasteSameSize(sheet2_row, sheet2_firstRow + 2 + countMeal, 1);
                        Dictionary<string, object> mealData = new Dictionary<string, object>();
                        mealData["MealName"] = rateByMeal.MealName;
                        mealData["Percent"] = rateByMeal.Rate;
                        mealData["Norm"] = rateByMeal.EnergyDemandFrom + "-" + rateByMeal.EnergyDemandTo;
                        lstMealData.Add(mealData);
                    }
                }
                sheetData.GetRange("A" + (sheet2_firstRow + 3), "C" + (sheet2_firstRow + 3 + lstRateByMeal.Count)).FillVariableValue(new Dictionary<string, object> { { "listMeal", lstMealData } });
                int sheet2_lastRow = sheet2_firstRow + 3 + lstRateByMeal.Count;
            

                #region Điền dữ liệu sheet 3 vào sheet tổng hợp
                int sheet3_firstRow = sheet2_lastRow + 1;
                int sheet3_lastRow = sheet3_firstRow;
                IVTRange sheet3_PLG = sheet3.GetRange("A6", "C13");
                sheetData.CopyPasteSameSize(sheet3_PLG, sheet3_firstRow, 1);
                Dictionary<string, object> dicNutrition1 = new Dictionary<string, object>
                {
                    {"ProteinAnimal", mineralOfDailyMenu.ProteinAnimal}, 
                    {"ProteinAnimalDM", Math.Round(mineralOfDailyMenu.ProteinAnimalMin, 2) + "-" + Math.Round(mineralOfDailyMenu.ProteinAnimalMax, 2)},
                    {"ProteinPlant", mineralOfDailyMenu.ProteinPlant},
                    {"ProteinPlantDM", Math.Round(mineralOfDailyMenu.ProteinPlantMin, 2) + "-" + Math.Round(mineralOfDailyMenu.ProteinPlantMax, 2)},
                    {"FatAnimal", mineralOfDailyMenu.FatAnimal}, 
                    {"FatAnimalDM", Math.Round(mineralOfDailyMenu.FatAnimalMin, 2) + "-" + Math.Round(mineralOfDailyMenu.FatAnimalMax, 2)},
                    {"FatPlant", mineralOfDailyMenu.FatPlant}, 
                    {"FatPlantDM", Math.Round(mineralOfDailyMenu.FatPlantMin, 2) + "-" + Math.Round(mineralOfDailyMenu.FatPlantMax, 2)},
                    {"Sugar", mineralOfDailyMenu.Sugar}, 
                    {"SugarDM", Math.Round(mineralOfDailyMenu.SugarMin, 2) + "-" + Math.Round(mineralOfDailyMenu.SugarMax, 2)}
                };
                sheetData.GetRange("A" + sheet3_firstRow, "C" + (sheet3_firstRow + 7)).FillVariableValue(dicNutrition1);

                Dictionary<string, object> dicNutrition2 = new Dictionary<string, object>
                {
                    {"Calo", mineralOfDailyMenu.Calo}, 
                    {"CaloDM", Math.Round(mineralOfDailyMenu.CaloMin, 2) + "-" + Math.Round(mineralOfDailyMenu.CaloMax, 2)}
                };

                List<Object> sheet3_ListData = new List<object>();
                List<FoodMineralMenuBO> lstFoodMineralMenu = mineralOfDailyMenu.lstFoodMineral;
                IVTRange sheet3_Nutrition = sheet3.GetRange("A15", "C15");

                if (lstFoodMineralMenu != null && lstFoodMineralMenu.Count > 0)
                {
                    int countMineral = 0;
                    foreach (FoodMineralMenuBO foodMineralMenu in lstFoodMineralMenu)
                    {
                        countMineral++;
                        sheetData.CopyPasteSameSize(sheet3_Nutrition, sheet3_firstRow + 7 + countMineral, 1);
                        Dictionary<string, object> sheet3_data = new Dictionary<string, object>();
                        sheet3_data["MineralName"] = foodMineralMenu.MineralName;
                        sheet3_data["Content"] = foodMineralMenu.Value;
                        sheet3_data["Norm"] = Math.Round(foodMineralMenu.ValueFrom, 2) + "-" + Math.Round(foodMineralMenu.ValueTo, 2);
                        sheet3_ListData.Add(sheet3_data);
                    }
                }
                if (sheet3_ListData.Count > 0)
                {
                    sheetData.GetRange("A" + (sheet3_firstRow + 8), "C" + (sheet3_firstRow + 8 + lstFoodMineralMenu.Count)).FillVariableValue(new Dictionary<string, object> { { "listNutrition", sheet3_ListData } });
                    IVTRange sheet3_Calo = sheet3.GetRange("A14", "C14");
                    sheetData.CopyPasteSameSize(sheet3_Calo, sheet3_firstRow + 8 + lstFoodMineralMenu.Count, 1);
                    sheetData.GetRange("A" + (sheet3_firstRow + 8 + lstFoodMineralMenu.Count), "C" + (sheet3_firstRow + 8 + lstFoodMineralMenu.Count)).FillVariableValue(dicNutrition2);
                    sheet3_lastRow = sheet3_firstRow + 9 + lstFoodMineralMenu.Count;
                }
                #endregion

                #region Copy sheet4 vào sheet tổng
                int sheet4_firstRow = sheet3_lastRow + 1;
                IVTRange sheet4_all = sheet4.GetRange("A6", "F11");
                sheetData.CopyPasteSameSize(sheet4_all, sheet4_firstRow, 1);
                int sheet4_lastRow = sheet4_firstRow + 6;
                #endregion

                #region Điền dữ liệu sheet5 vào sheet tổng
                int sheet5_firstRow = sheet4_lastRow + 1;
                int sheet5_lastRow = sheet5_firstRow + 3;
                IVTRange sheet5_Header = sheet5.GetRange("A6", "G8");
                IVTRange sheet5_FoodGroup = sheet5.GetRange("A9", "G9");
                IVTRange sheet5_Food = sheet5.GetRange("A10", "G10");
                IVTRange sheet_Sum = sheet5.GetRange("A11", "G11");
                sheetData.CopyPasteSameSize(sheet5_Header, sheet5_firstRow, 1);
                sheet5_firstRow += 2;
                if (lsDailyMenuFood != null && lsDailyMenuFood.Count() > 0)
                {
                    int countFood = 0;
                    foreach (DailyMenuFood dailyMenuFood in lsDailyMenuFood.ToList())
                    {
                        countFood++;
                        sheetData.CopyPasteSameSize(sheet5_FoodGroup, sheet5_firstRow + countFood, 1);
                        lstFoodID.Add(dailyMenuFood.FoodID);
                        lstWeight.Add(dailyMenuFood.Weight);
                        sheetData.CopyPasteSameSize(sheet5_Food, sheet5_firstRow + countFood, 1);
                        sheetData.SetCellValue(sheet5_firstRow + countFood, 1, countFood);
                        sheetData.SetCellValue(sheet5_firstRow + countFood, 2, dailyMenuFood.FoodCat.FoodName);
                        sheetData.SetCellValue(sheet5_firstRow + countFood, 3, dailyMenuFood.Weight * dailyMenu.NumberOfChildren / 1000);
                        sheetData.SetCellValue(sheet5_firstRow + countFood, 4, dailyMenuFood.Weight);
                        sheetData.SetCellValue(sheet5_firstRow + countFood, 5, dailyMenuFood.Weight * dailyMenu.NumberOfChildren / 1000 * dailyMenuFood.FoodCat.DiscardedRate / 100);
                        sheetData.SetCellValue(sheet5_firstRow + countFood, 6, dailyMenuFood.FoodCat.Price);
                        formular = "=F" + (sheet5_firstRow + countFood) + "*C" + (sheet5_firstRow + countFood);
                        sheetData.SetFormulaValue(sheet5_firstRow + countFood, 7, formular);
                    }
                    sheet5_lastRow = sheet5_firstRow + countFood + 1;
                    sheetData.CopyPasteSameSize(sheet_Sum, sheet5_lastRow, 1);
                    formular = "=SUM(G" + (sheet5_firstRow + 1) + ":G" + (sheet5_lastRow - 1) + ")";
                    sheetData.SetFormulaValue(sheet5_lastRow, 7, formular);
                    sheet5_lastRow++;
                }
                #endregion

                #region Điền dữ liệu sheet6 vào sheet tổng
                int sheet6_firstRow = sheet5_lastRow + 1;
                IVTRange sheet6_header = sheet6.GetRange("A6", "D11");
                IVTRange sheet6_row = sheet6.GetRange("A12", "D12");
                IVTRange sheet6_footer = sheet6.GetRange("A13", "D15");

                sheetData.CopyPasteSameSize(sheet6_header, sheet6_firstRow, 1);
                List<Object> sheet6_listData = new List<object>();
                if (ListOtherService != null && ListOtherService.Count > 0)
                {
                    int countService = 0;
                    foreach (OtherService otherService in ListOtherService)
                    {
                        countService++;
                        sheetData.CopyPasteSameSize(sheet6_row, sheet6_firstRow + 5 + countService, 1);
                        Dictionary<string, object> sheet6_data = new Dictionary<string, object>();
                        sheet6_data["ServiceName"] = otherService.OtherServiceName;
                        sheet6_data["Number"] = dailyMenu.NumberOfChildren;
                        sheet6_data["Price"] = otherService.Price;
                        sheet6_listData.Add(sheet6_data);
                        formular = "=C" + (sheet6_firstRow + 5 + countService) + "*B" + (sheet6_firstRow + 5 + countService);
                        sheetData.SetFormulaValue(sheet6_firstRow + 5 + countService, 4, formular);
                    }
                    sheetData.GetRange("A" + (sheet6_firstRow + 5), "D" + (sheet6_firstRow + 5 + ListOtherService.Count)).FillVariableValue(new Dictionary<string, object> { { "listServices", sheet6_listData } });
                }
                sheetData.CopyPasteSameSize(sheet6_footer, sheet6_firstRow + 6 + ListOtherService.Count, 1);

                //Tổng tiền dịch vụ
                if (ListOtherService.Count != 0)
                {
                    formular = "=SUM(D" + (sheet6_firstRow + 6) + ":D" + (sheet6_firstRow + 5 + ListOtherService.Count) + ")";
                    sheetData.SetFormulaValue(sheet6_firstRow + 5, 4, formular);
                }
                else
                {
                    sheetData.SetCellValue(sheet6_firstRow + 5, 4, 0);
                }
                //Tiền thực phẩm
                formular = "=G" + (sheet5_lastRow - 1);
                sheetData.SetFormulaValue(sheet6_firstRow + 4, 4, formular);
                //Tiền thu
                Dictionary<string, object> dic_dailydishcost = new Dictionary<string, object>();
                DateTime max_effectdate = DailyDishCostBusiness.SearchBySchool(SchoolID, new Dictionary<string, object>()).Select(o => o.EffectDate).Max();
                dic_dailydishcost["EffectDate"] = max_effectdate;
                var daylidishcost = DailyDishCostBusiness.SearchBySchool(SchoolID, dic_dailydishcost).FirstOrDefault();
                int DailyPrice = daylidishcost != null ? daylidishcost.Cost : 0;
                sheetData.SetCellValue(sheet6_firstRow + 7 + ListOtherService.Count, 2, dailyMenu.NumberOfChildren);
                sheetData.SetCellValue(sheet6_firstRow + 7 + ListOtherService.Count, 3, DailyPrice);
                formular = "=C" + (sheet6_firstRow + 7 + ListOtherService.Count) + "*B" + (sheet6_firstRow + 7 + ListOtherService.Count);
                sheetData.SetFormulaValue(sheet6_firstRow + 7 + ListOtherService.Count, 4, formular);
                //Tiền chi trong ngày
                formular = "=D" + (sheet6_firstRow + 4) + "+D" + (sheet6_firstRow + 5);
                sheetData.SetFormulaValue(sheet6_firstRow + 3, 4, formular);
                //Chênh lệch
                formular = "=D" + (sheet6_firstRow + 7 + ListOtherService.Count) + "-D" + (sheet6_firstRow + 3);
                sheetData.SetFormulaValue(sheet6_firstRow + 8 + ListOtherService.Count, 4, formular);
                #endregion
            }
            #endregion
            firstSheet.Delete();
            sheet2.Delete();
            sheet3.Delete();
            sheet4.Delete();
            sheet5.Delete();
            sheet6.Delete();
            return oBook.ToStream();
        }
    }
}