﻿/**
 * The Viettel License
 *
 * Copyright 2012 Viettel Telecom. All rights reserved.
 * VIETTEL PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */

/** 
* @author  
* @version $Revision: $
*/

using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Linq.Expressions;
using System.Web;
using log4net;

using SMAS.DAL.Repository;
using SMAS.DAL.IRepository;
using SMAS.Business.IBusiness;
using SMAS.Models.Models;
using SMAS.Business.Common;

namespace SMAS.Business.Business
{
    public partial class DefaultGroupBusiness
    {
        public IQueryable<DefaultGroup> GetAllActive()
        {
            return this.All.Where(g => g.IsActive == true);
        }

        public void Validate(DefaultGroup dg)
        {
            IDictionary<string, object> expDic = null;
            if (dg.DefaultGroupID > 0)
            {
                expDic = new Dictionary<string, object>();
                expDic["DefaultGroupID"] = dg.DefaultGroupID;
            }
            bool DefaultGroupName = repository.ExistsRow(GlobalConstants.ADM_SCHEMA, "DefaultGroup",
                  new Dictionary<string, object>()
                {
                    {"DefaultGroupName", dg.DefaultGroupName},
                    {"RoleID", dg.RoleID},
                    {"IsActive",true}
                }, expDic);
            if (DefaultGroupName)
            {
                List<object> Params = new List<object>();
                Params.Add("DefaultGroup_Label_DefaultGroupName");
                throw new BusinessException("Common_Validate_Duplicate", Params);
            }
            Utils.ValidateDangerousString(dg.DefaultGroupName);
            Utils.ValidateDangerousString(dg.Description);
        }

        public IQueryable<DefaultGroup> Search(IDictionary<string, object> dic)
        {
            //- DefaultGroupName: default = ""; ""  -> tìm kiếm all 
            string DefaultGroupName = Utils.GetString(dic, "DefaultGroupName");
            //- Description: default = ""; ""  -> tìm kiếm all
            string Description = Utils.GetString(dic, "Description");
            //- RoleID: default = 0; 0 -> tìm kiếm all
            List<int> listRoleID = Utils.GetIntList(dic, "List<RoleID>");
            int RoleID = Utils.GetInt(dic, "RoleID");
            //- IsActive: default = TRUE; NULL -> tìm kiếm all
            bool? IsAvtive = Utils.GetIsActive(dic, "IsAvtive");

            IQueryable<DefaultGroup> lsDefaultGroup = DefaultGroupRepository.All;
            if (DefaultGroupName.Trim().Length != 0)
            {
                lsDefaultGroup = lsDefaultGroup.Where(o => o.DefaultGroupName.ToUpper().Contains(DefaultGroupName.ToUpper()));
            }
            if (Description.Trim().Length != 0)
            {
                lsDefaultGroup = lsDefaultGroup.Where(o => o.Description.Contains(Description));
            }
            if (listRoleID.Count > 0)
            {
                lsDefaultGroup = lsDefaultGroup.Where(o => listRoleID.Contains(o.RoleID));
            }
            if (RoleID != 0)
            {
                lsDefaultGroup = lsDefaultGroup.Where(o => o.RoleID == RoleID);
            }
            if (IsAvtive.HasValue)
            {
                lsDefaultGroup = lsDefaultGroup.Where(o => o.IsActive == IsAvtive);
            }

            return lsDefaultGroup;
        }

        public IQueryable<DefaultGroupMenu> GetListMenu(int DefaultGroupID)
        {
            IDictionary<string, object> dic = new Dictionary<string, object>();
            IQueryable<DefaultGroupMenu> lsDefaultGroupMenu = DefaultGroupMenuRepository.All.Where(o => o.DefaultGroupID == DefaultGroupID);
            return lsDefaultGroupMenu;
        }

        public override DefaultGroup Insert(DefaultGroup DefaultGroup)
        {
            ValidationMetadata.ValidateObject(DefaultGroup);
            Validate(DefaultGroup);

            //RoleID: not Exist
            bool DefaultGroupRole = repository.ExistsRow(GlobalConstants.ADM_SCHEMA, "Role",
               new Dictionary<string, object>()
                {
                    {"RoleID", DefaultGroup.RoleID},
                    
                }, null);
            if (!DefaultGroupRole)
            {
                List<object> Params = new List<object>();
                Params.Add("DefaultGroup_Label_RoleID");
                throw new BusinessException("Common_Validate_NotExist", Params);
            }
            return base.Insert(DefaultGroup);

        }

        public override DefaultGroup Update(DefaultGroup DefaultGroup)
        {
            Validate(DefaultGroup);

            ValidationMetadata.ValidateObject(DefaultGroup);

            //Không tồn tại nhóm mặc định này – Kiểm tra DefaultGroupID
            this.CheckAvailable(DefaultGroup.DefaultGroupID, "DefaultGroup_Label_DefaultGroupID", true);

            //RoleID not Exist
            bool DefaultGroupRole = repository.ExistsRow(GlobalConstants.ADM_SCHEMA, "DefaultGroup",
               new Dictionary<string, object>()
                {
                    {"RoleID", DefaultGroup.RoleID},
                    
                }, null);
            if (!DefaultGroupRole)
            {
                List<object> Params = new List<object>();
                Params.Add("DefaultGroup_Label_RoleID");
                throw new BusinessException("Common_Validate_NotExist", Params);
            }

            return base.Update(DefaultGroup);
        }


        public DefaultGroup AssignMenu(int DefaultGroupID, List<DefaultGroupMenu> lsDefaultGroupMenu)
        {
            //Permission chỉ nhận giá trị từ 1-4 hoặc null – Kiểm tra DefaultGroupMenu trong ListMenu
            //Không tồn tại chức năng - Kiểm tra MenuID của DefaultGroupMenu trong ListMenu
            //Không tồn tại nhóm mặc định này – Kiểm tra DefaultGroupID
            this.CheckAvailable(DefaultGroupID, "DefaultGroup_Label_DefaultGroupID", true);
            //Nhóm này không được phép gán với Menu này – Kiểm tra  cặp DefaultGroup(DefaultGroupID).RoleID – MenuID (trong ListMenu) trong RoleMenu
            DefaultGroup DefaultGroup = DefaultGroupRepository.Find(DefaultGroupID);

            //Thực hiện xóa các bản ghi cũ trong DefaultGroupMenu nếu ListMenu != null
            List<DefaultGroupMenu> listDefaultGroupMenuOld = DefaultGroupMenuBusiness.All.Where(o => o.DefaultGroupID == DefaultGroupID).ToList();
            if (listDefaultGroupMenuOld != null && listDefaultGroupMenuOld.Count > 0)
            {
                // QuangLM - Dung ham deleteAll de khong phai tim lai luc xoa
                DefaultGroupMenuBusiness.DeleteAll(listDefaultGroupMenuOld);
            }

            if (lsDefaultGroupMenu.Count() != 0)
            {
                foreach (DefaultGroupMenu DefaultGroupMenu in lsDefaultGroupMenu)
                {
                    if (DefaultGroupMenu.Permission != null)
                    {
                        Utils.ValidateRange(int.Parse(DefaultGroupMenu.Permission.ToString()), 1, 4, "DefaultGroupMenu_Label_Permission");
                    }
                }

                //Thực hiện insert vào bảng DefaultGroupMenu nếu ListMenu != null
                foreach (var def in lsDefaultGroupMenu)
                {
                    DefaultGroupMenuBusiness.Insert(def);
                }
            }
            return DefaultGroup;
        }

        public void Delete(int DefaultGroupID)
        {
            //Bạn chưa chọn nhóm người dùng mặc định cần xóa hoặc nhóm người dùng mặc định bạn chọn đã bị xóa khỏi hệ thống
            this.CheckAvailable(DefaultGroupID, "DefaultGroup_Label_DefaultGroupID", true);
            this.CheckConstraints(GlobalConstants.ADM_SCHEMA, "DefaultGroup", DefaultGroupID, "DefaultGroup_Label_DefaultGroupID");
            DefaultGroup defaultGroup = DefaultGroupBusiness.Find(DefaultGroupID);
            defaultGroup.IsActive = false;
            base.Update(defaultGroup);
            //base.Delete(DefaultGroupID);
        }


    }
}