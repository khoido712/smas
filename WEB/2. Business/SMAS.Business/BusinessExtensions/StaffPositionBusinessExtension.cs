﻿/**
 * The Viettel License
 *
 * Copyright 2012 Viettel Telecom. All rights reserved.
 * VIETTEL PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */

/** 
* @author  
* @version $Revision: $
*/

using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Linq.Expressions;
using System.Web;
using log4net;

using SMAS.DAL.Repository;
using SMAS.DAL.IRepository;
using SMAS.Business.IBusiness;
using SMAS.Models.Models;
using SMAS.Business.Common;

namespace SMAS.Business.Business
{ 
    public partial class StaffPositionBusiness
    {
        #region base method
        /// <summary>
        /// Tìm kiếm Chức vụ
        /// </summary>
        /// <author>hath</author>
        /// <date>4/9/2012</date>
        /// <param name="StaffPosition">object StaffPosition.</param>
        /// <param name="StaffPositionName"> Tên Chức vụ</param>
        /// <param name="StaffPositionCode">Mã Chức vụ</param>
        /// <param name="Description">Mô tả Chức vụ</param>
        /// <param name="IsActive">Biến kiểm tra có active hay ko</param>
        /// <returns> IQueryable<StaffPosition></returns>
        public IQueryable<StaffPosition> Search(IDictionary<string, object> dic)
        {
            string Resolution = Utils.GetString(dic, "Resolution");
            IQueryable<StaffPosition> lsStaffPosition = StaffPositionRepository.All.Where(o=>o.IsActive == true);


            if (Resolution.Trim().Length != 0)
            {
                lsStaffPosition = lsStaffPosition.Where(o => o.Resolution.ToLower().Contains(Resolution.ToLower()));
            }
            
            //if (Description.Trim().Length != 0)
            //    lsStaffPosition = lsStaffPosition.Where(StaffPosition => StaffPosition.Description.Contains(Description.ToLower()));

            return lsStaffPosition;

        }
        /// <summary>
        /// Thêm Chức vụ
        /// </summary>
        /// <author>hath</author>
        /// <date>4/9/2012</date>
        /// <param name="StaffPosition">object StaffPosition.</param>
        /// <returns>objiect StaffPosition</returns>
        public override StaffPosition Insert(StaffPosition StaffPosition)
        {
            //Mã Chức vụ đã tồn tại 
            new StaffPositionBusiness(null).CheckDuplicate(StaffPosition.Resolution, GlobalConstants.LIST_SCHEMA, "StaffPosition", "Resolution", true, StaffPosition.StaffPositionID, "StaffPosition_Label_Resolution");
            //Tên Chức vụ không được để trống 
            Utils.ValidateRequire(StaffPosition.Resolution, "StaffPosition_Label_Resolution");
            //Độ dài ghi chú không được vượt quá 400 
            Utils.ValidateMaxLength(StaffPosition.Resolution, 400, "StaffPosition_Label_Resolution");


            //Insert
            base.Insert(StaffPosition);
            return StaffPosition;

        }
        /// <summary>
        /// Sửa Chức vụ
        /// </summary>
        /// <author>hath</author>
        /// <date>4/9/2012</date>
        /// <param name="StaffPosition">object StaffPosition.</param>
        /// <returns>objiect StaffPosition</returns>
        public override StaffPosition Update(StaffPosition StaffPosition)
        {
            //Bạn chưa chọn Chức vụ cần sửa hoặc Chức vụ bạn chọn đã bị xóa khỏi hệ thống
            new StaffPositionBusiness(null).CheckAvailable((int)StaffPosition.StaffPositionID, "StaffPosition_Label_StaffPositionID", true);
            //Mã Chức vụ đã tồn tại 
            new StaffPositionBusiness(null).CheckDuplicate(StaffPosition.Resolution, GlobalConstants.LIST_SCHEMA, "StaffPosition", "Resolution", true, StaffPosition.StaffPositionID, "StaffPosition_Label_Resolution");
            //Tên Chức vụ không được để trống 
            Utils.ValidateRequire(StaffPosition.Resolution, "StaffPosition_Label_Resolution");
            //Độ dài ghi chú không được vượt quá 400 
            Utils.ValidateMaxLength(StaffPosition.Resolution, 400, "StaffPosition_Label_Resolution");

            base.Update(StaffPosition);
            return StaffPosition;

        }
        /// <summary>
        /// Xóa Chức vụ
        /// </summary>
        /// <author>hath</author>
        /// <date>4/9/2012</date>
        /// <param name="StaffPositionID"></param>
        public void Delete(int StaffPositionID)
        {
            //Bạn chưa chọn Chức vụ cần xóa hoặc Chức vụ bạn chọn đã bị xóa khỏi hệ thống
            new StaffPositionBusiness(null).CheckAvailable((int)StaffPositionID, "StaffPosition_Label_StaffPositionID", true);

            //Không thể xóa Chức vụ đang sử dụng
            new StaffPositionBusiness(null).CheckConstraints(GlobalConstants.LIST_SCHEMA, "StaffPosition", StaffPositionID, "StaffPosition_Label_StaffPositionID");

            base.Delete(StaffPositionID, true);
        }
        #endregion
        
    }
}