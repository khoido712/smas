/**
 * The Viettel License
 *
 * Copyright 2012 Viettel Telecom. All rights reserved.
 * VIETTEL PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */

/** 
* @author  
* @version $Revision: $
*/

using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Linq.Expressions;
using System.Web;
using log4net;

using SMAS.DAL.Repository;
using SMAS.DAL.IRepository;
using SMAS.Business.IBusiness;
using SMAS.Models.Models;
using SMAS.Business.Common;

namespace SMAS.Business.Business
{ 
    public partial class FoodMineralBusiness
    {  
		#region Search
        public IQueryable<FoodMineral> Search(IDictionary<string, object> SearchInfo)
        {

            int MineralID = Utils.GetInt(SearchInfo, "MineralID");
            int FoodMineralID = Utils.GetInt(SearchInfo, "FoodMineralID");
            int FoodID = Utils.GetInt(SearchInfo, "FoodID");

            IQueryable<FoodMineral> lstFoodMineral = this.FoodMineralRepository.All;

            if (MineralID != 0)
            {
                lstFoodMineral = lstFoodMineral.Where(o => o.MinenalID == MineralID);
            }
            if (FoodMineralID != 0)
            {
                lstFoodMineral = lstFoodMineral.Where(o => o.FoodMineralID == FoodMineralID);
            }
            if (FoodID != 0)
            {
                lstFoodMineral = lstFoodMineral.Where(o => o.FoodID == FoodID);
            }

            return lstFoodMineral;
        }
        #endregion

    }
}