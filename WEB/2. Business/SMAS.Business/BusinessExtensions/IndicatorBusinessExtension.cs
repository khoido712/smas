﻿/**
 * The Viettel License
 *
 * Copyright 2012 Viettel Telecom. All rights reserved.
 * VIETTEL PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */

/** 
* @author  
* @version $Revision: $
*/

using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Linq.Expressions;
using System.Web;
using log4net;

using SMAS.DAL.Repository;
using SMAS.DAL.IRepository;
using SMAS.Business.IBusiness;
using SMAS.Models.Models;
using SMAS.Business.Common;
using SMAS.Business.BusinessObject;
using System.Transactions;
using System.IO;

namespace SMAS.Business.Business
{
    public partial class IndicatorBusiness
    {
        private string REPORT_CODE = "CSVC";
        public IQueryable<Indicator> SearchIndicator(IDictionary<string, object> search)
        {
            int indicatorID = Utils.GetInt(search, "IndicatorID");
            IQueryable<Indicator> query = IndicatorBusiness.All;

            if (indicatorID > 0)
            {
                query = query.Where(o => o.IndicatorID == indicatorID);
            }


            return query;
        }

        public List<Indicator> GetListIndicatorByHtmlTemplateID(int htmltemplateId)
        {
            List<Indicator> listIndicator = IndicatorBusiness.All.AsNoTracking().Where(p => p.HtmlTemplateID == htmltemplateId).ToList();
            return listIndicator;
        }

        public int GetCurrentPeriodReport(int academicYearId)
        {
            int period = 0;
            AcademicYear aca = AcademicYearBusiness.Find(academicYearId);
            DateTime dateNow = DateTime.Now.Date;
            //Nến ngày hiện tại <= N+90 (với N là ngày đầu năm học đang chọn) : kỳ hiện tại là kỳ đầu năm
            if (dateNow <= aca.FirstSemesterStartDate.Value.AddDays(90).Date)
            {
                period = 1;
            }
            // Nếu này hiện tại nằm trong khoảng từ ngày N+91 đến ngày N+180: kỳ hiện tại là kỳ giữa năm
            if (dateNow >= aca.FirstSemesterStartDate.Value.AddDays(91).Date && dateNow <= aca.FirstSemesterStartDate.Value.AddDays(180).Date)
            {
                period = 2;
            }
            //Nếu ngày hiện tại >= N + 181 : kỳ hiện tại là kỳ cuối năm
            if (dateNow >= aca.FirstSemesterStartDate.Value.AddDays(181).Date)
            {
                period = 3;
            }
            return period;
        }

        public ProcessedReport GetReportIndicator(PhysicalFacilitiesBO physicalObj)
        {
            string ReportCode = REPORT_CODE;
            string HashKey = this.GetHashKey(physicalObj);
            IQueryable<ProcessedReport> query = this.ProcessedReportRepository.All.Where(o => o.ReportCode == ReportCode);
            query = query.Where(o => o.InputParameterHashKey == HashKey);
            query = query.OrderByDescending(o => o.ProcessedDate);

            return query.FirstOrDefault();
        }

        private string GetHashKey(PhysicalFacilitiesBO Entity)
        {
            IDictionary<string, object> dic = new Dictionary<string, object>();
            dic["SchoolID"] = Entity.SchoolID;
            dic["Period"] = Entity.Period;
            dic["Year"] = Entity.Year;
            return ReportUtils.GetHashKey(dic);
        }

        public ProcessedReport InsertFacilitiesReport(PhysicalFacilitiesBO Entity, Stream Data,string FileName)
        {
            ProcessedReport ProcessedReport = new ProcessedReport();

            ProcessedReport.ReportCode = REPORT_CODE;
            ProcessedReport.ProcessedDate = DateTime.Now;
            ProcessedReport.InputParameterHashKey = this.GetHashKey(Entity);
            ProcessedReport.ReportData = ReportUtils.Compress(Data);
            ProcessedReport.ReportName = FileName;

            IDictionary<string, object> dic = new Dictionary<string, object>();
            dic["SchoolID"] = Entity.SchoolID;
            dic["Year"] = Entity.Year;
            dic["Period"] = Entity.Period;
            ProcessedReportParameterRepository.Insert(dic, ProcessedReport);
            ProcessedReportRepository.Insert(ProcessedReport);

            ProcessedReportParameterRepository.Save();
            ProcessedReportRepository.Save();

            return ProcessedReport;
        }

    }
}
