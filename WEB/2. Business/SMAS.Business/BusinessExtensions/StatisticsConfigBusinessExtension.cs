﻿/**
 * The Viettel License
 *
 * Copyright 2012 Viettel Telecom. All rights reserved.
 * VIETTEL PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */

/** 
* @author  
* @version $Revision: $
*/

using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Linq.Expressions;
using System.Web;
using log4net;

using SMAS.DAL.Repository;
using SMAS.DAL.IRepository;
using SMAS.Business.IBusiness;
using SMAS.Models.Models;

namespace SMAS.Business.Business
{
    public partial class StatisticsConfigBusiness
    {
        public List<StatisticsConfig> GetList(int AppliedLevel, 
            string ReportType, int MarkType = 0, int SubjectType = 0)
        {
            var query = from s in StatisticsConfigRepository.All
                        where
                            s.AppliedLevel == AppliedLevel && s.ReportType == ReportType
                            && (MarkType == 0 || s.MarkType == MarkType)
                            && (SubjectType == 0 || s.SubjectType == SubjectType)
                            orderby s.DisplayOrder
                        select s;
            return query.ToList();
        }

        public IQueryable<StatisticsConfig> GetIQueryable(int AppliedLevel,
            string ReportType, int MarkType = 0, int SubjectType = 0)
        {
            var query = from s in StatisticsConfigRepository.All
                        where
                            s.AppliedLevel == AppliedLevel && s.ReportType == ReportType
                            && (MarkType == 0 || s.MarkType == MarkType)
                            && (SubjectType == 0 || s.SubjectType == SubjectType)
                        orderby s.DisplayOrder
                        select s;
            return query;
        }

    }
}