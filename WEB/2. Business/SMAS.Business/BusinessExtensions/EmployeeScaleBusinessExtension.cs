/**
 * The Viettel License
 *
 * Copyright 2012 Viettel Telecom. All rights reserved.
 * VIETTEL PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */

/** 
* @author  
* @version $Revision: $
*/

using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Linq.Expressions;
using System.Web;
using log4net;

using SMAS.DAL.Repository;
using SMAS.DAL.IRepository;
using SMAS.Business.IBusiness;
using SMAS.Models.Models;
using SMAS.Business.Common;

namespace SMAS.Business.Business
{ 
    public partial class EmployeeScaleBusiness
    {
        /// <summary>
        /// Tìm kiếm thang bậc ngạch lương công chức
        /// </summary>
        /// <author>hath</author>
        /// <date>4/9/2012</date>
        /// <param name="Resolution">Diễn giải thang bậc ngạch lương công chức</param>
        /// <param name="DurationInYear">Số năm theo quy định</param>
        /// <param name="IsActive">Biến kích hoạt</param>
        /// <returns>IQueryable<EmployeeScale></returns>
        public IQueryable<EmployeeScale> Search(IDictionary<string, object> dic)
        {
            string Resolution = Utils.GetString(dic, "Resolution");
            int DurationInYear = (int) Utils.GetInt(dic, "EmployeeScale_Label_DurationInYear");
            bool? IsActive = Utils.GetIsActive(dic, "IsActive");

            IQueryable<EmployeeScale> lsEmployeeScale = EmployeeScaleRepository.All;

            if (IsActive.HasValue)
            {
                lsEmployeeScale = lsEmployeeScale.Where(em => em.IsActive == IsActive);
            }
            if (Resolution.Trim().Length != 0)
            {
                lsEmployeeScale = lsEmployeeScale.Where(em => em.Resolution.Contains(Resolution));
            }
            if (DurationInYear != 0)
            {
                lsEmployeeScale = lsEmployeeScale.Where(em => em.DurationInYear == DurationInYear);
            }

            return lsEmployeeScale;

        }
        
    }
}
