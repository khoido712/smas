﻿/**
 * The Viettel License
 *
 * Copyright 2012 Viettel Telecom. All rights reserved.
 * VIETTEL PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */

/** 
* @author  
* @version $Revision: $
*/

using System;
using System.Collections.Generic;
using System.Linq;
using SMAS.Models.Models;
using SMAS.Business.Common;
using System.Text;
using SMAS.Business.BusinessObject;
using Newtonsoft.Json;

namespace SMAS.Business.Business
{
    public partial class TeacherNoteBookSemesterBusiness
    {
        private string strKTDK_GT = "KTDK GK: ";
        private string strKTDK_CT = "KTDK CK: ";
        private string strRate = "Đánh giá: ";
        private string log_space = "; ";
        public IQueryable<TeacherNoteBookSemester> Search(IDictionary<string, object> dic)
        {
            int schoolID = Utils.GetInt(dic, "SchoolID");
            int academicYearID = Utils.GetInt(dic, "AcademicYearID");
            int classID = Utils.GetInt(dic, "ClassID");
            int pupilID = Utils.GetInt(dic, "PupilID");
            int subjectID = Utils.GetInt(dic, "SubjectID");
            int semesterID = Utils.GetInt(dic, "SemesterID");
            List<int> lstPupilID = Utils.GetIntList(dic, "lstPupilID");
            List<int> lstClassID = Utils.GetIntList(dic, "lstClassID");
            List<int> lstSubjectID = Utils.GetIntList(dic, "lstSubjectID");
            IQueryable<TeacherNoteBookSemester> iqTeacherNoteBookSemester = TeacherNoteBookSemesterBusiness.All;
            if (schoolID > 0)
            {
                iqTeacherNoteBookSemester = iqTeacherNoteBookSemester.Where(p => p.SchoolID == schoolID);
            }
            if (academicYearID > 0)
            {
                iqTeacherNoteBookSemester = iqTeacherNoteBookSemester.Where(p => p.AcademicYearID == academicYearID);
            }
            if (classID > 0)
            {
                iqTeacherNoteBookSemester = iqTeacherNoteBookSemester.Where(p => p.ClassID == classID);
            }
            if (lstClassID.Count > 0)
            {
                iqTeacherNoteBookSemester = iqTeacherNoteBookSemester.Where(p => lstClassID.Contains(p.ClassID));
            }
            if (pupilID > 0)
            {
                iqTeacherNoteBookSemester = iqTeacherNoteBookSemester.Where(p => p.PupilID == pupilID);
            }
            if (lstPupilID.Count > 0)
            {
                iqTeacherNoteBookSemester = iqTeacherNoteBookSemester.Where(p => lstPupilID.Contains(p.PupilID));
            }
            if (subjectID > 0)
            {
                iqTeacherNoteBookSemester = iqTeacherNoteBookSemester.Where(p => p.SubjectID == subjectID);
            }
            if (semesterID > 0)
            {
                iqTeacherNoteBookSemester = iqTeacherNoteBookSemester.Where(p => p.SemesterID == semesterID);
            }
            if (lstSubjectID.Count > 0)
            {
                iqTeacherNoteBookSemester = iqTeacherNoteBookSemester.Where(p => lstSubjectID.Contains(p.SubjectID));
            }
            return iqTeacherNoteBookSemester;
        }

        public List<TeacherNoteBookSemester> GetListTeacherNoteBookSemester(IDictionary<string, object> dic)
        {
            int schoolID = Utils.GetInt(dic, "SchoolID");
            int academicYearID = Utils.GetInt(dic, "AcademicYearID");
            int classID = Utils.GetInt(dic, "ClassID");
            int subjectID = Utils.GetInt(dic, "SubjectID");
            int semesterID = Utils.GetInt(dic, "SemesterID");
            List<TeacherNoteBookSemester> lstTeacherNoteBookSemester = new List<TeacherNoteBookSemester>();
            lstTeacherNoteBookSemester = (from t in TeacherNoteBookSemesterBusiness.All
                                          where t.SchoolID == schoolID
                                          && t.AcademicYearID == academicYearID
                                          && t.ClassID == classID
                                          && t.SubjectID == subjectID
                                          && t.SemesterID == semesterID
                                          && t.PartitionID == (schoolID % 100)
                                          select t).ToList();
            return lstTeacherNoteBookSemester;
        }

        private void CalculateAverageMarkYear(IDictionary<string, object> dic, RESTORE_DATA objRes, ref List<RESTORE_DATA_DETAIL> lstRestoreDetail)
        {
            int SchoolID = Utils.GetInt(dic, "SchoolID");
            int AcademicYearID = Utils.GetInt(dic, "AcademicYearID");
            int ClassID = Utils.GetInt(dic, "ClassID");
            List<int> lstSubjectID = Utils.GetIntList(dic, "lstSubjectID");


            IDictionary<string, object> tmpDic = new Dictionary<string, object>();
            tmpDic["SchoolID"] = SchoolID;
            tmpDic["AcademicYearID"] = AcademicYearID;
            tmpDic["ClassID"] = ClassID;
            List<TeacherNoteBookSemester> lstTeacherNoteBookSemsterDBAll = this.Search(tmpDic).ToList();
            List<ClassSubject> lstCsAll = ClassSubjectBusiness.GetListSubjectByClass(AcademicYearID, ClassID, lstSubjectID).ToList();
            List<ExemptedSubject> lstExempAll = ExemptedSubjectBusiness.Search(tmpDic).ToList();
            RESTORE_DATA_DETAIL objRestoreDetail;
            for (int i = 0; i < lstSubjectID.Count; i++)
            {
                int SubjectID = lstSubjectID[i];
                List<TeacherNoteBookSemester> lstTeacherNoteBookSemsterDB = lstTeacherNoteBookSemsterDBAll.Where(o => o.SubjectID == SubjectID).ToList();
                ClassSubject cs = lstCsAll.FirstOrDefault(o => o.SubjectID == SubjectID);


                List<ExemptedSubject> lstExempSem1 = lstExempAll.Where(c => c.SubjectID == SubjectID && c.FirstSemesterExemptType.HasValue).ToList();
                List<ExemptedSubject> lstExempSem2 = lstExempAll.Where(c => c.SubjectID == SubjectID && c.SecondSemesterExemptType.HasValue).ToList();

                List<int> lstPupilID = lstTeacherNoteBookSemsterDB.Select(o => o.PupilID).Distinct().ToList();
                for (int j = 0; j < lstPupilID.Count; j++)
                {
                    int pupilID = lstPupilID[j];
                    TeacherNoteBookSemester tnSem1 = lstTeacherNoteBookSemsterDB.FirstOrDefault(o => o.PupilID == pupilID && o.SemesterID == GlobalConstants.SEMESTER_OF_YEAR_FIRST);
                    TeacherNoteBookSemester tnSem2 = lstTeacherNoteBookSemsterDB.FirstOrDefault(o => o.PupilID == pupilID && o.SemesterID == GlobalConstants.SEMESTER_OF_YEAR_SECOND);
                    TeacherNoteBookSemester tnSemYear = lstTeacherNoteBookSemsterDB.FirstOrDefault(o => o.PupilID == pupilID && o.SemesterID == GlobalConstants.SEMESTER_OF_YEAR_ALL);

                    if (tnSemYear != null && objRes != null)
                    {
                        #region Luu thong tin phuc vu phuc hoi du lieu
                        objRestoreDetail = new RESTORE_DATA_DETAIL
                        {
                            ACADEMIC_YEAR_ID = objRes.ACADEMIC_YEAR_ID,
                            CREATED_DATE = DateTime.Now,
                            END_DATE = null,
                            IS_VALIDATE = 1,
                            LAST_2DIGIT_NUMBER_SCHOOL = UtilsBusiness.GetPartionId(objRes.SCHOOL_ID),
                            ORDER_ID = 1,
                            RESTORE_DATA_ID = objRes.RESTORE_DATA_ID,
                            RESTORE_DATA_TYPE_ID = objRes.RESTORE_DATA_TYPE_ID,
                            SCHOOL_ID = objRes.SCHOOL_ID,
                            SQL_DELETE = "",
                            SQL_UNDO = JsonConvert.SerializeObject(tnSemYear),
                            TABLE_NAME = RestoreDataConstant.TABLE_TEACHER_NOTEBOOK_SEMESTER
                        };
                        lstRestoreDetail.Add(objRestoreDetail);
                        #endregion
                    }
                    bool studiedSem1 = true;
                    if (lstExempSem1.Any(p => p.PupilID == pupilID) || cs.SectionPerWeekFirstSemester <= 0)
                    {
                        studiedSem1 = false;
                    }

                    bool studiedSem2 = true;
                    if (lstExempSem2.Any(p => p.PupilID == pupilID) || cs.SectionPerWeekSecondSemester <= 0)
                    {
                        studiedSem2 = false;
                    }

                    bool studiedOnlyOneSem = (studiedSem1 && !studiedSem2) || (!studiedSem1 && studiedSem2);

                    if (tnSemYear == null)
                    {
                        tnSemYear = new TeacherNoteBookSemester();
                        tnSemYear.SchoolID = SchoolID;
                        tnSemYear.PartitionID = SchoolID % 100;
                        tnSemYear.AcademicYearID = AcademicYearID;
                        tnSemYear.ClassID = ClassID;
                        tnSemYear.PupilID = pupilID;
                        tnSemYear.SubjectID = SubjectID;
                        tnSemYear.SemesterID = GlobalConstants.SEMESTER_OF_YEAR_ALL;
                    }

                    if (cs.IsCommenting.HasValue && cs.IsCommenting.Value == 1)
                    {
                        //Neu chi hoc 1 ky
                        if (studiedOnlyOneSem)
                        {
                            if (studiedSem1)
                            {
                                if (tnSem1 != null && !string.IsNullOrEmpty(tnSem1.AVERAGE_MARK_JUDGE))
                                {
                                    tnSemYear.AVERAGE_MARK_JUDGE = tnSem1.AVERAGE_MARK_JUDGE;
                                }
                                else
                                {
                                    if (tnSemYear.TeacherNoteBookSemesterID != 0)
                                    {
                                        this.Delete(tnSemYear.TeacherNoteBookSemesterID);
                                    }

                                    continue;
                                }
                            }
                            else
                            {
                                if (tnSem2 != null && !string.IsNullOrEmpty(tnSem2.AVERAGE_MARK_JUDGE))
                                {
                                    tnSemYear.AVERAGE_MARK_JUDGE = tnSem2.AVERAGE_MARK_JUDGE;
                                }
                                else
                                {
                                    if (tnSemYear.TeacherNoteBookSemesterID != 0)
                                    {
                                        this.Delete(tnSemYear.TeacherNoteBookSemesterID);
                                    }

                                    continue;
                                }
                            }
                        }
                        else
                        {
                            if (tnSem2 != null && !string.IsNullOrEmpty(tnSem2.AVERAGE_MARK_JUDGE))
                            {
                                tnSemYear.AVERAGE_MARK_JUDGE = tnSem2.AVERAGE_MARK_JUDGE;
                            }
                            else
                            {
                                if (tnSemYear.TeacherNoteBookSemesterID != 0)
                                {
                                    this.Delete(tnSemYear.TeacherNoteBookSemesterID);
                                }

                                continue;
                            }
                        }
                    }
                    else
                    {
                        if (studiedOnlyOneSem)
                        {
                            if (studiedSem1)
                            {
                                if (tnSem1 != null && tnSem1.AVERAGE_MARK.HasValue)
                                {
                                    tnSemYear.AVERAGE_MARK = tnSem1.AVERAGE_MARK;
                                }
                                else
                                {
                                    if (tnSemYear.TeacherNoteBookSemesterID != 0)
                                    {
                                        this.Delete(tnSemYear.TeacherNoteBookSemesterID);
                                    }

                                    continue;
                                }
                            }
                            else
                            {
                                if (tnSem2 != null && tnSem2.AVERAGE_MARK.HasValue)
                                {
                                    tnSemYear.AVERAGE_MARK = tnSem2.AVERAGE_MARK;
                                }
                                else
                                {
                                    if (tnSemYear.TeacherNoteBookSemesterID != 0)
                                    {
                                        this.Delete(tnSemYear.TeacherNoteBookSemesterID);
                                    }

                                    continue;
                                }
                            }
                        }
                        else
                        {
                            if (tnSem1 != null && tnSem1.AVERAGE_MARK.HasValue && tnSem2 != null && tnSem2.AVERAGE_MARK.HasValue)
                            {
                                tnSemYear.AVERAGE_MARK = CalculateAverageMark(tnSem1.AVERAGE_MARK, tnSem2.AVERAGE_MARK);
                            }
                            else
                            {
                                if (tnSemYear.TeacherNoteBookSemesterID != 0)
                                {
                                    this.Delete(tnSemYear.TeacherNoteBookSemesterID);
                                }

                                continue;
                            }
                        }
                    }

                    if (tnSemYear.TeacherNoteBookSemesterID != 0)
                    {
                        this.Update(tnSemYear);
                    }
                    else
                    {
                        this.Insert(tnSemYear);
                    }
                }
            }

            this.Save();

        }

        private decimal? CalculateAverageMark(decimal? coef1Mark, decimal? coef2Mark)
        {
            if (!coef1Mark.HasValue || !coef2Mark.HasValue)
            {
                return null;
            }
            return System.Math.Round((coef1Mark.Value + (coef2Mark.Value * 2)) / 3, 1);
        }

        public void InsertOrUpdate(List<TeacherNoteBookSemester> lstTeacherNoteBookSemster, 
            IDictionary<string, object> dic, 
            List<ActionAuditDataBO> lstActionAuditData, 
            List<TeacherNoteBookSemester> lstTeacherNoteBookSemsterDB,
            ref ActionAuditDataBO objActionAuditBO)
        {
            int SchoolID = Utils.GetInt(dic, "SchoolID");
            int MonthID = Utils.GetInt(dic, "MonthID");
            int AcademicYearID = Utils.GetInt(dic, "AcademicYearID");
            List<int> lstSubjectID = Utils.GetIntList(dic, "lstSubjectID");
            int ClassID = Utils.GetInt(dic, "ClassID");
            List<ClassSubject> lstClassSubject = ClassSubjectBusiness.GetListSubjectByClass(AcademicYearID, ClassID, lstSubjectID).ToList();
            ClassSubject objCS = null;
            //List<TeacherNoteBookSemester> lstTeacherNoteBookSemsterDB = this.Search(dic).ToList();
            TeacherNoteBookSemester objTeacherNoteBookSemsterDB = null;
            List<TeacherNoteBookSemester> lstUpdate = new List<TeacherNoteBookSemester>();
            List<TeacherNoteBookSemester> lstInsert = new List<TeacherNoteBookSemester>();
            TeacherNoteBookSemester objTeacherNoteBookSemster = null;
            int partitionID = (SchoolID % 100);
            int? isCommenting = 0;
            //lay danh sach hoc sinh mien giam
            List<ExemptedSubject> lstExemp = ExemptedSubjectBusiness.Search(dic).ToList();
            #region tao du lieu ghi log
            StringBuilder objectIDSummed = new StringBuilder();
            StringBuilder descriptionSummed = new StringBuilder();
            StringBuilder paramsSummed = new StringBuilder();
            StringBuilder oldObjectSummed = new StringBuilder();
            StringBuilder newObjectSummed = new StringBuilder();
            StringBuilder userFuntionsSummed = new StringBuilder();
            StringBuilder userActionsSummed = new StringBuilder();
            StringBuilder userDescriptionsSummed = new StringBuilder();

            StringBuilder objectIDStrtmp = new StringBuilder();
            StringBuilder descriptionStrtmp = new StringBuilder();
            StringBuilder paramsStrtmp = new StringBuilder();
            StringBuilder oldObjectStrtmp = new StringBuilder();
            StringBuilder newObjectStrtmp = new StringBuilder();
            StringBuilder userFuntionsStrtmp = new StringBuilder();
            StringBuilder userActionsStrtmp = new StringBuilder();
            StringBuilder userDescriptionsStrtmp = new StringBuilder();
            ActionAuditDataBO objAc = null;
            bool isChange = false;
            #endregion

            for (int i = 0; i < lstTeacherNoteBookSemster.Count; i++)
            {
                isChange = false;
                objTeacherNoteBookSemster = lstTeacherNoteBookSemster[i];
                objCS = lstClassSubject.Where(p => p.SubjectID == objTeacherNoteBookSemster.SubjectID).FirstOrDefault();
                if (objCS != null)
                {
                    isCommenting = objCS.IsCommenting;
                }
                if (lstExemp.Any(p => p.PupilID == objTeacherNoteBookSemster.PupilID && p.SubjectID == objTeacherNoteBookSemster.SubjectID))
                {
                    continue;
                }
                objTeacherNoteBookSemsterDB = lstTeacherNoteBookSemsterDB.Where(p => p.PupilID == objTeacherNoteBookSemster.PupilID 
                                                                                && p.SubjectID == objTeacherNoteBookSemster.SubjectID).FirstOrDefault();
          
                objAc = lstActionAuditData.Where(p => p.PupilID == objTeacherNoteBookSemster.PupilID && p.ClassID == objTeacherNoteBookSemster.ClassID
                                                     && p.SubjectID == objTeacherNoteBookSemster.SubjectID).FirstOrDefault();
                if (objAc != null)
                {
                    objectIDStrtmp.Append(objAc.ObjID);
                    descriptionStrtmp.Append(objAc.Description);
                    paramsStrtmp.Append(objAc.Parameter);
                    oldObjectStrtmp.Append(objAc.OldData);
                    newObjectStrtmp.Append(objAc.NewData);
                    userFuntionsStrtmp.Append(objAc.UserFunction);
                    userActionsStrtmp.Append(objAc.UserAction);
                    userDescriptionsStrtmp.Append(objAc.UserDescription);
                }

                if (objTeacherNoteBookSemsterDB != null)
                {
                    if (MonthID != 15 && MonthID != 16)
                    {
                        if (isCommenting == 0)
                        {
                            if (objTeacherNoteBookSemsterDB.PERIODIC_SCORE_MIDDLE != objTeacherNoteBookSemster.PERIODIC_SCORE_MIDDLE)
                            {
                                oldObjectStrtmp.Append(strKTDK_GT).Append(objTeacherNoteBookSemsterDB.PERIODIC_SCORE_MIDDLE.HasValue ? objTeacherNoteBookSemsterDB.PERIODIC_SCORE_MIDDLE.Value.ToString("0.0") : "").Append(log_space);
                                newObjectStrtmp.Append(strKTDK_GT).Append(objTeacherNoteBookSemster.PERIODIC_SCORE_MIDDLE).Append(log_space);
                                objTeacherNoteBookSemsterDB.PERIODIC_SCORE_MIDDLE = objTeacherNoteBookSemster.PERIODIC_SCORE_MIDDLE;
                                isChange = true;
                            }
                        }
                        else
                        {
                            if (objTeacherNoteBookSemsterDB.PERIODIC_SCORE_MIDDLE_JUDGE != objTeacherNoteBookSemster.PERIODIC_SCORE_MIDDLE_JUDGE)
                            {
                                oldObjectStrtmp.Append(strKTDK_GT).Append(objTeacherNoteBookSemsterDB.PERIODIC_SCORE_MIDDLE_JUDGE).Append(log_space);
                                newObjectStrtmp.Append(strKTDK_GT).Append(objTeacherNoteBookSemster.PERIODIC_SCORE_MIDDLE_JUDGE).Append(log_space);
                                objTeacherNoteBookSemsterDB.PERIODIC_SCORE_MIDDLE_JUDGE = objTeacherNoteBookSemster.PERIODIC_SCORE_MIDDLE_JUDGE;
                                isChange = true;
                            }
                        }
                    }
                    else
                    {
                        if (isCommenting == 0)
                        {
                            if (objTeacherNoteBookSemsterDB.PERIODIC_SCORE_MIDDLE != objTeacherNoteBookSemster.PERIODIC_SCORE_MIDDLE)
                            {
                                oldObjectStrtmp.Append(strKTDK_GT).Append(objTeacherNoteBookSemsterDB.PERIODIC_SCORE_MIDDLE.HasValue ? objTeacherNoteBookSemsterDB.PERIODIC_SCORE_MIDDLE.Value.ToString("0.0") : "").Append(log_space);
                                newObjectStrtmp.Append(strKTDK_GT).Append(objTeacherNoteBookSemster.PERIODIC_SCORE_MIDDLE).Append(log_space);
                                objTeacherNoteBookSemsterDB.PERIODIC_SCORE_MIDDLE = objTeacherNoteBookSemster.PERIODIC_SCORE_MIDDLE;
                                isChange = true;
                            }

                            if (objTeacherNoteBookSemsterDB.PERIODIC_SCORE_END != objTeacherNoteBookSemster.PERIODIC_SCORE_END)
                            {
                                oldObjectStrtmp.Append(strKTDK_CT).Append(objTeacherNoteBookSemsterDB.PERIODIC_SCORE_END.HasValue ? objTeacherNoteBookSemsterDB.PERIODIC_SCORE_END.Value.ToString("0.0") : "").Append(log_space);
                                newObjectStrtmp.Append(strKTDK_CT).Append(objTeacherNoteBookSemster.PERIODIC_SCORE_END).Append(log_space);
                                objTeacherNoteBookSemsterDB.PERIODIC_SCORE_END = objTeacherNoteBookSemster.PERIODIC_SCORE_END;
                                isChange = true;
                            }

                            if (objTeacherNoteBookSemsterDB.AVERAGE_MARK != objTeacherNoteBookSemster.AVERAGE_MARK)
                            {
                                objTeacherNoteBookSemsterDB.AVERAGE_MARK = objTeacherNoteBookSemster.AVERAGE_MARK;
                                isChange = true;
                            }
                        }
                        else
                        {
                            if (objTeacherNoteBookSemsterDB.PERIODIC_SCORE_MIDDLE_JUDGE != objTeacherNoteBookSemster.PERIODIC_SCORE_MIDDLE_JUDGE)
                            {
                                oldObjectStrtmp.Append(strKTDK_GT).Append(objTeacherNoteBookSemsterDB.PERIODIC_SCORE_MIDDLE_JUDGE).Append(log_space);
                                newObjectStrtmp.Append(strKTDK_GT).Append(objTeacherNoteBookSemster.PERIODIC_SCORE_MIDDLE_JUDGE).Append(log_space);
                                objTeacherNoteBookSemsterDB.PERIODIC_SCORE_MIDDLE_JUDGE = objTeacherNoteBookSemster.PERIODIC_SCORE_MIDDLE_JUDGE;
                                isChange = true;
                            }

                            if (objTeacherNoteBookSemsterDB.PERIODIC_SCORE_END_JUDGLE != objTeacherNoteBookSemster.PERIODIC_SCORE_END_JUDGLE)
                            {
                                oldObjectStrtmp.Append(strKTDK_CT).Append(objTeacherNoteBookSemsterDB.PERIODIC_SCORE_END_JUDGLE).Append(log_space);
                                newObjectStrtmp.Append(strKTDK_CT).Append(objTeacherNoteBookSemster.PERIODIC_SCORE_END_JUDGLE).Append(log_space);
                                objTeacherNoteBookSemsterDB.PERIODIC_SCORE_END_JUDGLE = objTeacherNoteBookSemster.PERIODIC_SCORE_END_JUDGLE;
                                isChange = true;
                            }

                            if (objTeacherNoteBookSemsterDB.AVERAGE_MARK_JUDGE != objTeacherNoteBookSemster.AVERAGE_MARK_JUDGE)
                            {
                                objTeacherNoteBookSemsterDB.AVERAGE_MARK_JUDGE = objTeacherNoteBookSemster.AVERAGE_MARK_JUDGE;
                                isChange = true;
                            }
                        }

                        //if (objTeacherNoteBookSemsterDB.Rate != objTeacherNoteBookSemster.Rate)
                        //{
                        //    oldObjectStrtmp.Append(strRate).Append(objTeacherNoteBookSemsterDB.Rate == GlobalConstants.REVIEW_TYPE_FINISH ? "HT"
                        //        : objTeacherNoteBookSemsterDB.Rate == GlobalConstants.REVIEW_TYPE_NOT_FINISH ? "CHT" : "").Append(log_space);
                        //    newObjectStrtmp.Append(strRate).Append(objTeacherNoteBookSemster.Rate == GlobalConstants.REVIEW_TYPE_FINISH ? "HT"
                        //        : objTeacherNoteBookSemster.Rate == GlobalConstants.REVIEW_TYPE_NOT_FINISH ? "CHT" : "").Append(log_space);
                        //    objTeacherNoteBookSemsterDB.Rate = objTeacherNoteBookSemster.Rate;
                        //    isChange = true;
                        //}
                    }

                    if (isChange)
                    {
                        objTeacherNoteBookSemsterDB.UpdateTime = DateTime.Now.Date;
                        lstUpdate.Add(objTeacherNoteBookSemsterDB);
                    }
                }
                else
                {
                    if (isCommenting == 0)
                    {
                        if (objTeacherNoteBookSemster.PERIODIC_SCORE_MIDDLE.HasValue)
                        {
                            oldObjectStrtmp.Append(strKTDK_GT).Append(log_space);
                            newObjectStrtmp.Append(strKTDK_GT).Append(objTeacherNoteBookSemster.PERIODIC_SCORE_MIDDLE).Append(log_space);
                            isChange = true;
                        }
                        if (objTeacherNoteBookSemster.PERIODIC_SCORE_END.HasValue)
                        {
                            oldObjectStrtmp.Append(strKTDK_GT).Append(log_space);
                            newObjectStrtmp.Append(strKTDK_GT).Append(objTeacherNoteBookSemster.PERIODIC_SCORE_END).Append(log_space);
                            isChange = true;
                        }
                        if (objTeacherNoteBookSemster.AVERAGE_MARK.HasValue)
                        {
                            isChange = true;
                        }
                    }
                    else
                    {
                        if (!string.IsNullOrEmpty(objTeacherNoteBookSemster.PERIODIC_SCORE_MIDDLE_JUDGE))
                        {
                            oldObjectStrtmp.Append(strKTDK_GT).Append(log_space);
                            newObjectStrtmp.Append(strKTDK_GT).Append(objTeacherNoteBookSemster.PERIODIC_SCORE_MIDDLE_JUDGE).Append(log_space);
                            isChange = true;
                        }
                        if (!string.IsNullOrEmpty(objTeacherNoteBookSemster.PERIODIC_SCORE_END_JUDGLE))
                        {
                            oldObjectStrtmp.Append(strKTDK_CT).Append(log_space);
                            newObjectStrtmp.Append(strKTDK_CT).Append(objTeacherNoteBookSemster.PERIODIC_SCORE_END_JUDGLE).Append(log_space);
                            isChange = true;
                        }
                        if (!string.IsNullOrEmpty(objTeacherNoteBookSemster.PERIODIC_SCORE_END_JUDGLE))
                        {

                            isChange = true;
                        }
                    }
                    //if (objTeacherNoteBookSemster.Rate.HasValue)
                    //{
                    //    oldObjectStrtmp.Append(strRate).Append(log_space);
                    //    newObjectStrtmp.Append(strRate).Append(objTeacherNoteBookSemster.Rate == GlobalConstants.REVIEW_TYPE_FINISH ? "HT"
                    //        : objTeacherNoteBookSemster.Rate == GlobalConstants.REVIEW_TYPE_NOT_FINISH ? "CHT" : "").Append(log_space);
                    //    isChange = true;
                    //}
                    if (isChange)
                    {
                        objTeacherNoteBookSemster.CreateTime = DateTime.Now.Date;
                        objTeacherNoteBookSemster.PartitionID = partitionID;
                        lstInsert.Add(objTeacherNoteBookSemster);
                    }
                }
                if (isChange)
                {
                    string tmpOld = string.Empty;
                    string tmpNew = string.Empty;
                    tmpOld = oldObjectStrtmp.Length > 0 ? oldObjectStrtmp.ToString().Substring(0, oldObjectStrtmp.Length - 2) : "";
                    oldObjectSummed.Append("(Giá trị trước khi sửa): ").Append(tmpOld);
                    tmpNew = newObjectStrtmp.Length > 0 ? newObjectStrtmp.ToString().Substring(0, newObjectStrtmp.Length - 2) : "";
                    newObjectSummed.Append("(Giá trị sau khi sửa): ").Append(tmpNew);
                    userDescriptionsStrtmp.Append("Giá trị cũ {").Append(tmpOld).Append("}. ");
                    userDescriptionsStrtmp.Append("Giá trị mới {").Append(tmpNew).Append("}");

                    objectIDSummed.Append(objectIDStrtmp).Append(SMAS.Business.Common.GlobalConstants.WILD_LOG);
                    paramsSummed.Append(paramsStrtmp).Append(SMAS.Business.Common.GlobalConstants.WILD_LOG);
                    descriptionSummed.Append(descriptionStrtmp).Append(SMAS.Business.Common.GlobalConstants.WILD_LOG);
                    userFuntionsSummed.Append(userFuntionsStrtmp).Append(SMAS.Business.Common.GlobalConstants.WILD_LOG);
                    userActionsSummed.Append(userActionsStrtmp).Append(SMAS.Business.Common.GlobalConstants.WILD_LOG);
                    oldObjectSummed.Append(SMAS.Business.Common.GlobalConstants.WILD_LOG);
                    newObjectSummed.Append(SMAS.Business.Common.GlobalConstants.WILD_LOG);
                    userDescriptionsSummed.Append(userDescriptionsStrtmp).Append(SMAS.Business.Common.GlobalConstants.WILD_LOG);

                }
                else if (objAc != null && objAc.isInsertLog)
                {
                    objectIDSummed.Append(objAc.ObjID).Append(SMAS.Business.Common.GlobalConstants.WILD_LOG);
                    paramsSummed.Append(objAc.Parameter).Append(SMAS.Business.Common.GlobalConstants.WILD_LOG);
                    descriptionSummed.Append(objAc.Description).Append(SMAS.Business.Common.GlobalConstants.WILD_LOG);
                    userFuntionsSummed.Append(objAc.UserFunction).Append(SMAS.Business.Common.GlobalConstants.WILD_LOG);
                    userActionsSummed.Append(objAc.UserAction).Append(SMAS.Business.Common.GlobalConstants.WILD_LOG);
                    string tmpOld = string.Empty;
                    string tmpNew = string.Empty;
                    tmpOld = objAc.OldData.Length > 0 ? objAc.OldData.ToString().Substring(0, objAc.OldData.Length - 2) : "";
                    oldObjectSummed.Append("(Giá trị trước khi sửa): ").Append(tmpOld);
                    tmpNew = objAc.NewData.Length > 0 ? objAc.NewData.ToString().Substring(0, objAc.NewData.Length - 2) : "";
                    newObjectSummed.Append("(Giá trị sau khi sửa): ").Append(tmpNew);
                    userDescriptionsStrtmp.Append("Giá trị cũ {").Append(tmpOld).Append("}. ");
                    userDescriptionsStrtmp.Append("Giá trị mới {").Append(tmpNew).Append("}");
                    oldObjectSummed.Append(SMAS.Business.Common.GlobalConstants.WILD_LOG);
                    newObjectSummed.Append(SMAS.Business.Common.GlobalConstants.WILD_LOG);
                    userDescriptionsSummed.Append(userDescriptionsStrtmp).Append(SMAS.Business.Common.GlobalConstants.WILD_LOG);
                }
                objectIDStrtmp = new StringBuilder();
                descriptionStrtmp = new StringBuilder();
                paramsStrtmp = new StringBuilder();
                oldObjectStrtmp = new StringBuilder();
                newObjectStrtmp = new StringBuilder();
                userFuntionsStrtmp = new StringBuilder();
                userActionsStrtmp = new StringBuilder();
                userDescriptionsStrtmp = new StringBuilder();
            }
            objActionAuditBO.ObjID = objectIDSummed;
            objActionAuditBO.Parameter = paramsSummed;
            objActionAuditBO.UserFunction = userFuntionsSummed;
            objActionAuditBO.UserAction = userActionsSummed;
            objActionAuditBO.UserDescription = userDescriptionsSummed;
            objActionAuditBO.Description = descriptionSummed;
            objActionAuditBO.OldData = oldObjectSummed;
            objActionAuditBO.NewData = newObjectSummed;

            if (lstUpdate.Count > 0)
            {
                for (int i = 0; i < lstUpdate.Count; i++)
                {
                    this.Update(lstUpdate[i]);
                }
                this.Save();
            }
            if (lstInsert.Count > 0)
            {
                this.BulkInsert(lstInsert, TeacherNoteBookSemesterMaping(), "TeacherNoteBookSemesterID");
            }

            List<RESTORE_DATA_DETAIL> lstResotreDetail = new List<RESTORE_DATA_DETAIL>();
            CalculateAverageMarkYear(dic, null, ref lstResotreDetail);

        }

        public void DeleteTeacherNoteBookSemester(IDictionary<string, object> dic, List<ActionAuditDataBO> lstActionAuditData, ref ActionAuditDataBO objActionAuditBO, ref List<RESTORE_DATA_DETAIL> lstRestoreDetail)
        {

            List<int> lstpupilID = Utils.GetIntList(dic, "lstPupilID");
            int MonthID = Utils.GetInt(dic, "MonthID");
            int isCommenting = Utils.GetInt(dic, "IsCommenting");
            int subjectID = Utils.GetInt(dic, "SubjectID");
            int semesterID = Utils.GetInt(dic, "SemesterID");
            int classID = Utils.GetInt(dic, "ClassID");
            int academicYearID = Utils.GetInt(dic, "AcademicYearID");
            int schoolID = Utils.GetInt(dic, "SchoolID");
            bool lockGK = Utils.GetBool(dic, "lockGK");
            bool lockCK = Utils.GetBool(dic, "lockCK");

            RESTORE_DATA objRes = Utils.GetObject<RESTORE_DATA>(dic, "RESTORE_DATA");
            string monthName = string.Empty;
            if (MonthID != 15 && MonthID != 16)
            {
                int tmp = Int32.Parse(MonthID.ToString().Substring(4, MonthID.ToString().Length - 4));
                monthName = "Tháng " + MonthID.ToString().Substring(4, MonthID.ToString().Length - 4);
            }
            else
            {
                monthName = "Đánh giá cuối kỳ";
            }
            AcademicYear objAca = AcademicYearBusiness.Find(academicYearID);
            List<TeacherNoteBookSemester> lstDelete = new List<TeacherNoteBookSemester>();
            TeacherNoteBookSemester objTeacherNoteBookSemester = null;
            RESTORE_DATA_DETAIL objRestoreDetail;
            lstDelete = this.Search(dic).ToList();
            #region tao du lieu ghi log
            StringBuilder objectIDSummed = new StringBuilder();
            StringBuilder descriptionSummed = new StringBuilder();
            StringBuilder paramsSummed = new StringBuilder();
            StringBuilder oldObjectSummed = new StringBuilder();
            StringBuilder userFuntionsSummed = new StringBuilder();
            StringBuilder userActionsSummed = new StringBuilder();
            StringBuilder userDescriptionsSummed = new StringBuilder();

            StringBuilder objectIDStrtmp = new StringBuilder();
            StringBuilder descriptionStrtmp = new StringBuilder();
            StringBuilder paramsStrtmp = new StringBuilder();
            StringBuilder oldObjectStrtmp = new StringBuilder();
            StringBuilder userFuntionsStrtmp = new StringBuilder();
            StringBuilder userActionsStrtmp = new StringBuilder();
            StringBuilder userDescriptionsStrtmp = new StringBuilder();
            StringBuilder restoreDescription = new StringBuilder();
            ActionAuditDataBO objAc = null;
            bool isChange = false;
            List<PupilOfClassBO> lstPOC = (from poc in PupilOfClassBusiness.All
                                           join pf in PupilProfileBusiness.All on poc.PupilID equals pf.PupilProfileID
                                           join cp in ClassProfileBusiness.All on poc.ClassID equals cp.ClassProfileID
                                           where poc.AcademicYearID == academicYearID
                                           && poc.SchoolID == schoolID
                                           && poc.ClassID == classID
                                           && lstpupilID.Contains(poc.PupilID)
                                           && cp.IsActive.Value
                                           && cp.ClassProfileID == classID
                                           select new PupilOfClassBO
                                           {
                                               PupilID = poc.PupilID,
                                               PupilCode = pf.PupilCode,
                                               ClassID = poc.ClassID,
                                               ClassName = cp.DisplayName,
                                               PupilFullName = pf.FullName
                                           }).ToList();
            PupilOfClassBO objPOC = new PupilOfClassBO();

            SubjectCat objSC = SubjectCatBusiness.Find(subjectID);
            int pupilID = 0;
            int partitionId = UtilsBusiness.GetPartionId(objRes.SCHOOL_ID);
            #endregion
            for (int i = 0; i < lstDelete.Count; i++)
            {
                isChange = false;
                objTeacherNoteBookSemester = lstDelete[i];

                #region Luu thong tin phuc vu phuc hoi du lieu
                objRestoreDetail = new RESTORE_DATA_DETAIL
                {
                    ACADEMIC_YEAR_ID = objRes.ACADEMIC_YEAR_ID,
                    CREATED_DATE = DateTime.Now,
                    END_DATE = null,
                    IS_VALIDATE = 1,
                    LAST_2DIGIT_NUMBER_SCHOOL = partitionId,
                    ORDER_ID = 1,
                    RESTORE_DATA_ID = objRes.RESTORE_DATA_ID,
                    RESTORE_DATA_TYPE_ID = objRes.RESTORE_DATA_TYPE_ID,
                    SCHOOL_ID = objRes.SCHOOL_ID,
                    SQL_DELETE = "",
                    SQL_UNDO = JsonConvert.SerializeObject(objTeacherNoteBookSemester),
                    TABLE_NAME = RestoreDataConstant.TABLE_TEACHER_NOTEBOOK_SEMESTER
                };

                lstRestoreDetail.Add(objRestoreDetail);
                #endregion

                pupilID = objTeacherNoteBookSemester.PupilID;
                objAc = lstActionAuditData.Where(p => p.PupilID == objTeacherNoteBookSemester.PupilID && p.ClassID == objTeacherNoteBookSemester.ClassID
                                                     && p.SubjectID == objTeacherNoteBookSemester.SubjectID).FirstOrDefault();
                if (objAc != null)
                {
                    objectIDStrtmp.Append(objAc.ObjID);
                    descriptionStrtmp.Append(objAc.Description);
                    paramsStrtmp.Append(objAc.Parameter);
                    oldObjectStrtmp.Append(objAc.OldData);
                    userFuntionsStrtmp.Append(objAc.UserFunction);
                    userActionsStrtmp.Append(objAc.UserAction);
                    userDescriptionsStrtmp.Append(objAc.UserDescription);
                    restoreDescription.Append(objAc.RestoreMsgDescription);
                }

                if (MonthID != 15 && MonthID != 16)
                {
                    // Môn tính điểm
                    if (isCommenting == 0)
                    {
                        // Nếu chưa khóa Giữa Kỳ thì cập nhật điểm Giữa Kỳ là NULL
                        if (!lockGK)
                        {
                            if (objTeacherNoteBookSemester.PERIODIC_SCORE_MIDDLE.HasValue)
                            {
                                oldObjectStrtmp.Append(strKTDK_GT).Append(objTeacherNoteBookSemester.PERIODIC_SCORE_MIDDLE.HasValue
                                                    ? objTeacherNoteBookSemester.PERIODIC_SCORE_MIDDLE.Value.ToString("0.0") : "").Append(log_space);
                                objTeacherNoteBookSemester.PERIODIC_SCORE_MIDDLE = null;
                                isChange = true;
                            }
                        }
                      

                        if (objAca.Year == 2016 && semesterID == 1)
                        {
                        }
                        else
                        {
                            // Nếu chưa khóa Giữa Kỳ hoặc điểm GK là NULL thì cập nhật điểm TB là NULL
                            if (!lockGK || !objTeacherNoteBookSemester.PERIODIC_SCORE_MIDDLE.HasValue)
                            {
                                if (objTeacherNoteBookSemester.AVERAGE_MARK.HasValue)
                                {
                                    objTeacherNoteBookSemester.AVERAGE_MARK = null;
                                    isChange = true;
                                }
                            }
                           
                        }   
                    }
                    else
                    { // Môn nhận xét

                        if (!lockGK)
                        {
                            // Nếu chưa khóa Giữa Kỳ thì cập nhật nhận xét Giữa Kỳ là NULL
                            if (!string.IsNullOrEmpty(objTeacherNoteBookSemester.PERIODIC_SCORE_MIDDLE_JUDGE))
                            {
                                oldObjectStrtmp.Append(strKTDK_GT).Append(objTeacherNoteBookSemester.PERIODIC_SCORE_MIDDLE_JUDGE).Append(log_space);
                                objTeacherNoteBookSemester.PERIODIC_SCORE_MIDDLE_JUDGE = string.Empty;
                                isChange = true;
                            }
                        }
                        

                        if (objAca.Year == 2016 && semesterID == 1)
                        {
                        }
                        else
                        {
                            // Nếu chưa khóa Giữa Kỳ hoặc Nhận xét GK là NULL thì cập nhật Nhận xét TB là NULL
                            if (!lockGK || string.IsNullOrEmpty(objTeacherNoteBookSemester.PERIODIC_SCORE_MIDDLE_JUDGE))
                            {
                                if (!string.IsNullOrEmpty(objTeacherNoteBookSemester.AVERAGE_MARK_JUDGE))
                                {
                                    objTeacherNoteBookSemester.AVERAGE_MARK_JUDGE = string.Empty;
                                    isChange = true;
                                }
                            }
                           
                        }
                    }

                }
                else
                { 
                    // Môn tính điểm
                    if (isCommenting == 0)
                    {
                        // Nếu chưa khóa Giữa Kỳ thì cập nhật điểm Giữa Kỳ là NULL
                        if (!lockGK)
                        {
                            if (objTeacherNoteBookSemester.PERIODIC_SCORE_MIDDLE.HasValue)
                            {
                                oldObjectStrtmp.Append(strKTDK_GT).Append(objTeacherNoteBookSemester.PERIODIC_SCORE_MIDDLE.HasValue ?
                                    objTeacherNoteBookSemester.PERIODIC_SCORE_MIDDLE.Value.ToString("0.0") : "").Append(log_space);
                                objTeacherNoteBookSemester.PERIODIC_SCORE_MIDDLE = null;
                                isChange = true;
                            }
                        }

                        // Nếu chưa khóa Cuối kỳ thì cập nhật điểm Cuối kỳ là NULL
                        if (!lockCK)
                        {
                            if (objTeacherNoteBookSemester.PERIODIC_SCORE_END.HasValue)
                            {
                                oldObjectStrtmp.Append(strKTDK_CT).Append(objTeacherNoteBookSemester.PERIODIC_SCORE_END.HasValue ?
                                    objTeacherNoteBookSemester.PERIODIC_SCORE_END.Value.ToString("0.0") : "").Append(log_space);
                                objTeacherNoteBookSemester.PERIODIC_SCORE_END = null;
                                isChange = true;
                            }
                        }

                        // Nếu chưa khóa GK hoặc CK, hoặc điểm GK là NULL hoặc điểm CK là null thì cập nhật điểm TB là null
                        if (!lockGK || !lockCK || !objTeacherNoteBookSemester.PERIODIC_SCORE_END.HasValue || !objTeacherNoteBookSemester.PERIODIC_SCORE_MIDDLE.HasValue)
                        {
                            if (objTeacherNoteBookSemester.AVERAGE_MARK.HasValue)
                            {
                                objTeacherNoteBookSemester.AVERAGE_MARK = null;
                                isChange = true;
                            }
                        }       
                    }
                    else
                    {// Nhận xét

                        if (!lockGK)
                        {
                            if (!string.IsNullOrEmpty(objTeacherNoteBookSemester.PERIODIC_SCORE_MIDDLE_JUDGE))
                            {
                                oldObjectStrtmp.Append(strKTDK_GT).Append(objTeacherNoteBookSemester.PERIODIC_SCORE_MIDDLE_JUDGE).Append(log_space);
                                objTeacherNoteBookSemester.PERIODIC_SCORE_MIDDLE_JUDGE = string.Empty;
                                isChange = true;
                            }
                        }

                        if (!lockCK)
                        {
                            if (!string.IsNullOrEmpty(objTeacherNoteBookSemester.PERIODIC_SCORE_END_JUDGLE))
                            {
                                oldObjectStrtmp.Append(strKTDK_CT).Append(objTeacherNoteBookSemester.PERIODIC_SCORE_END_JUDGLE).Append(log_space);
                                objTeacherNoteBookSemester.PERIODIC_SCORE_END_JUDGLE = string.Empty;
                                isChange = true;
                            }
                        }

                        // Nếu nhận xét cuối kỳ là null thì nhận xét TB là null
                        if (string.IsNullOrEmpty(objTeacherNoteBookSemester.PERIODIC_SCORE_END_JUDGLE))
                        {
                            if (!string.IsNullOrEmpty(objTeacherNoteBookSemester.AVERAGE_MARK_JUDGE))
                            {
                                objTeacherNoteBookSemester.AVERAGE_MARK_JUDGE = string.Empty;
                                isChange = true;
                            }
                        }
                       
                    }
                    if (objTeacherNoteBookSemester.Rate.HasValue)
                    {
                        oldObjectStrtmp.Append(strRate).Append(objTeacherNoteBookSemester.Rate.Value == 1 ? "HT" : objTeacherNoteBookSemester.Rate.Value == 2 ? "CHT" : "").Append(log_space);
                        objTeacherNoteBookSemester.Rate = null;
                        isChange = true;
                    }
                }

                if (isChange)
                {
                    objTeacherNoteBookSemester.UpdateTime = DateTime.Now.Date;
                    this.Update(objTeacherNoteBookSemester);

                    if (objAc != null)
                    {
                        string tmpOld = string.Empty;
                        tmpOld = oldObjectStrtmp.Length > 0 ? oldObjectStrtmp.ToString().Substring(0, oldObjectStrtmp.Length - 2) : "";
                        oldObjectSummed.Append("(Giá trị trước khi xóa): ").Append(tmpOld);
                        userDescriptionsStrtmp.Append("Giá trị cũ {").Append(tmpOld).Append("}. ");

                        objectIDSummed.Append(objectIDStrtmp).Append(SMAS.Business.Common.GlobalConstants.WILD_LOG);
                        paramsSummed.Append(paramsStrtmp).Append(SMAS.Business.Common.GlobalConstants.WILD_LOG);
                        descriptionSummed.Append(descriptionStrtmp).Append(SMAS.Business.Common.GlobalConstants.WILD_LOG);
                        userFuntionsSummed.Append(userFuntionsStrtmp).Append(SMAS.Business.Common.GlobalConstants.WILD_LOG);
                        userActionsSummed.Append(userActionsStrtmp).Append(SMAS.Business.Common.GlobalConstants.WILD_LOG);
                        oldObjectSummed.Append(SMAS.Business.Common.GlobalConstants.WILD_LOG);
                        userDescriptionsSummed.Append(userDescriptionsStrtmp).Append(SMAS.Business.Common.GlobalConstants.WILD_LOG);
                    }
                    else
                    {
                        objPOC = lstPOC.Where(p => p.PupilID == pupilID).FirstOrDefault();
                        objectIDStrtmp.Append(pupilID);
                        descriptionStrtmp.Append("Xóa đánh giá sổ tay GV");
                        paramsStrtmp.Append(pupilID);
                        userFuntionsStrtmp.Append("Sổ tay giáo viên");
                        userActionsStrtmp.Append(SMAS.Business.Common.GlobalConstants.ACTION_DELETE);
                        userDescriptionsStrtmp.Append("Xóa đánh giá HS " + objPOC.PupilFullName + ", " + "mã " + objPOC.PupilCode + ", ");
                        userDescriptionsStrtmp.Append("Lớp ").Append(objPOC.ClassName + "/" + objSC.DisplayName + "/" + monthName + "/Học kỳ " + semesterID + "/Năm học " + objAca.Year + "-" + (objAca.Year + 1) + ". ");
                        restoreDescription.Append("HS: " + objPOC.PupilFullName + ", " + "mã " + objPOC.PupilCode + ", ");
                        string tmpOld = string.Empty;
                        tmpOld = oldObjectStrtmp.Length > 0 ? oldObjectStrtmp.ToString().Substring(0, oldObjectStrtmp.Length - 2) : "";
                        oldObjectSummed.Append("(Giá trị trước khi xóa): ").Append(tmpOld);
                        userDescriptionsStrtmp.Append("Giá trị cũ {").Append(tmpOld).Append("}. ");

                        objectIDSummed.Append(objectIDStrtmp).Append(SMAS.Business.Common.GlobalConstants.WILD_LOG);
                        paramsSummed.Append(paramsStrtmp).Append(SMAS.Business.Common.GlobalConstants.WILD_LOG);
                        descriptionSummed.Append(descriptionStrtmp).Append(SMAS.Business.Common.GlobalConstants.WILD_LOG);
                        userFuntionsSummed.Append(userFuntionsStrtmp).Append(SMAS.Business.Common.GlobalConstants.WILD_LOG);
                        userActionsSummed.Append(userActionsStrtmp).Append(SMAS.Business.Common.GlobalConstants.WILD_LOG);
                        oldObjectSummed.Append(SMAS.Business.Common.GlobalConstants.WILD_LOG);
                        userDescriptionsSummed.Append(userDescriptionsStrtmp).Append(SMAS.Business.Common.GlobalConstants.WILD_LOG);
                    }
                }
                else if (objAc != null && objAc.isInsertLog)
                {
                    objectIDSummed.Append(objAc.ObjID).Append(SMAS.Business.Common.GlobalConstants.WILD_LOG);
                    paramsSummed.Append(objAc.Parameter).Append(SMAS.Business.Common.GlobalConstants.WILD_LOG);
                    descriptionSummed.Append(objAc.Description).Append(SMAS.Business.Common.GlobalConstants.WILD_LOG);
                    userFuntionsSummed.Append(objAc.UserFunction).Append(SMAS.Business.Common.GlobalConstants.WILD_LOG);
                    userActionsSummed.Append(objAc.UserAction).Append(SMAS.Business.Common.GlobalConstants.WILD_LOG);
                    string tmpOld = string.Empty;
                    tmpOld = objAc.OldData.Length > 0 ? objAc.OldData.ToString().Substring(0, objAc.OldData.Length - 2) : "";
                    oldObjectSummed.Append("(Giá trị trước khi xóa): ").Append(tmpOld);
                    userDescriptionsStrtmp.Append("Giá trị cũ {").Append(tmpOld).Append("}. ");
                    oldObjectSummed.Append(SMAS.Business.Common.GlobalConstants.WILD_LOG);
                    userDescriptionsSummed.Append(userDescriptionsStrtmp).Append(SMAS.Business.Common.GlobalConstants.WILD_LOG);
                }
                objectIDStrtmp = new StringBuilder();
                descriptionStrtmp = new StringBuilder();
                paramsStrtmp = new StringBuilder();
                oldObjectStrtmp = new StringBuilder();
                userFuntionsStrtmp = new StringBuilder();
                userActionsStrtmp = new StringBuilder();
                userDescriptionsStrtmp = new StringBuilder();
                //restoreDescription = new StringBuilder();



            }
            objActionAuditBO.ObjID = objectIDSummed;
            objActionAuditBO.Parameter = paramsSummed;
            objActionAuditBO.UserFunction = userFuntionsSummed;
            objActionAuditBO.UserAction = userActionsSummed;
            objActionAuditBO.UserDescription = userDescriptionsSummed;
            objActionAuditBO.Description = descriptionSummed;
            objActionAuditBO.OldData = oldObjectSummed;
            objActionAuditBO.RestoreMsgDescription = restoreDescription.ToString();

            this.Save();

            IDictionary<string, object> tmpDic = new Dictionary<string, object>()
                {
                    {"SchoolID",schoolID},
                    {"AcademicYearID",academicYearID},
                    {"MonthID",MonthID},
                    {"ClassID",classID},
                    {"lstSubjectID",new List<int>{subjectID}},
                };

            CalculateAverageMarkYear(tmpDic, objRes, ref lstRestoreDetail);

        }

        public void ImportFromInputMark(List<Object> insertList, List<Object> updateList)
        {
            try
            {
                this.context.Configuration.AutoDetectChangesEnabled = false;
                this.context.Configuration.ValidateOnSaveEnabled = false;

                int schoolID = 0;
                int academicYearID = 0;

                List<int> lstPupilID = new List<int>();
                List<int> lstSubjectID = new List<int>();
                List<int> lstClassID = new List<int>();
                for (int i = 0; i < insertList.Count; i++)
                {
                    TeacherNoteBookSemester entity = (TeacherNoteBookSemester)insertList[i];
                    TeacherNoteBookSemesterBusiness.Insert(entity);
                    lstPupilID.Add(entity.PupilID);
                    lstSubjectID.Add(entity.SubjectID);
                    lstClassID.Add(entity.ClassID);
                    schoolID = entity.SchoolID;
                    academicYearID = entity.AcademicYearID;
                }
                for (int i = 0; i < updateList.Count; i++)
                {
                    TeacherNoteBookSemester entity = (TeacherNoteBookSemester)updateList[i];
                    TeacherNoteBookSemesterBusiness.Update(entity);
                    lstPupilID.Add(entity.PupilID);
                    lstSubjectID.Add(entity.SubjectID);
                    lstClassID.Add(entity.ClassID);
                    schoolID = entity.SchoolID;
                    academicYearID = entity.AcademicYearID;
                }

                TeacherNoteBookSemesterBusiness.Save();

                // tính lại điểm trung bình cả năm
                lstPupilID = lstPupilID.Distinct().ToList();
                lstSubjectID = lstSubjectID.Distinct().ToList();
                lstClassID = lstClassID.Distinct().ToList();

                IDictionary<string, object> dic = new Dictionary<string, object>();
                dic["AcademicYearID"] = academicYearID;
                dic["SchoolID"] = schoolID;
                dic["lstPupilID"] = lstPupilID;
                dic["lstClassID"] = lstClassID;

                IDictionary<string, object> tmpDic = new Dictionary<string, object>();
                tmpDic["SchoolID"] = schoolID;
                tmpDic["AcademicYearID"] = academicYearID;
                List<TeacherNoteBookSemester> lstTeacherNoteBookSemsterDBAll = this.Search(dic).ToList();
                List<ClassSubject> lstCsAll = ClassSubjectBusiness.All.Where(p => lstSubjectID.Contains(p.SubjectID) && lstClassID.Contains(p.ClassID)).ToList();
                List<ExemptedSubject> lstExempAll = ExemptedSubjectBusiness.Search(tmpDic).Where(x => lstClassID.Contains(x.ClassID)
                                                                                                    && lstSubjectID.Contains(x.SubjectID)).ToList();

                List<TeacherNoteBookSemester> lstTeacherNoteBookSemsterDBAllByClass = new List<TeacherNoteBookSemester>();
                List<ClassSubject> lstCsAllByClass = new List<ClassSubject>();
                List<ExemptedSubject> lstExempAllByClass = new List<ExemptedSubject>();
                int classID = 0;
                for (int i = 0; i < lstClassID.Count(); i++)
                {
                    classID = lstClassID[i];
                    lstTeacherNoteBookSemsterDBAllByClass = lstTeacherNoteBookSemsterDBAll.Where(x => x.ClassID == classID).ToList();
                    lstCsAllByClass = lstCsAll.Where(x => x.ClassID == classID).ToList();
                    lstExempAllByClass = lstExempAll.Where(x => x.ClassID == classID).ToList();
                    lstSubjectID = lstCsAllByClass.Select(x => x.SubjectID).Distinct().ToList();
                    dic["lstSubjectID"] = lstSubjectID;

                    CalculateAverageMarkYearByInputMark(dic, classID, lstTeacherNoteBookSemsterDBAllByClass, lstCsAllByClass, lstExempAllByClass);
                }                
            }
            finally
            {
                this.context.Configuration.AutoDetectChangesEnabled = true;
                this.context.Configuration.ValidateOnSaveEnabled = true;
            }
        }

        private void CalculateAverageMarkYearByInputMark(IDictionary<string, object> dic, int classID,
            List<TeacherNoteBookSemester> lstTeacherNoteBookSemsterDBAllByClass, List<ClassSubject> lstCsAllByClass, List<ExemptedSubject> lstExempAllByClass)
        {
            int SchoolID = Utils.GetInt(dic, "SchoolID");
            int AcademicYearID = Utils.GetInt(dic, "AcademicYearID");
            List<int> lstSubjectID = Utils.GetIntList(dic, "lstSubjectID");
            List<int> lstPupilIDInput = Utils.GetIntList(dic, "lstPupilID");
       
            for (int i = 0; i < lstSubjectID.Count; i++)
            {
                int SubjectID = lstSubjectID[i];
                List<TeacherNoteBookSemester> lstTeacherNoteBookSemsterDB = lstTeacherNoteBookSemsterDBAllByClass.Where(o => o.SubjectID == SubjectID).ToList();
                ClassSubject cs = lstCsAllByClass.FirstOrDefault(o => o.SubjectID == SubjectID);

                List<ExemptedSubject> lstExempSem1 = lstExempAllByClass.Where(c => c.SubjectID == SubjectID && c.FirstSemesterExemptType.HasValue).ToList();
                List<ExemptedSubject> lstExempSem2 = lstExempAllByClass.Where(c => c.SubjectID == SubjectID && c.SecondSemesterExemptType.HasValue).ToList();

                List<int> lstPupilID = lstTeacherNoteBookSemsterDB.Where(x => lstPupilIDInput.Contains(x.PupilID)).Select(o => o.PupilID).Distinct().ToList();
                for (int j = 0; j < lstPupilID.Count; j++)
                {
                    int pupilID = lstPupilID[j];
                    TeacherNoteBookSemester tnSem1 = lstTeacherNoteBookSemsterDB.FirstOrDefault(o => o.PupilID == pupilID && o.SemesterID == GlobalConstants.SEMESTER_OF_YEAR_FIRST);
                    TeacherNoteBookSemester tnSem2 = lstTeacherNoteBookSemsterDB.FirstOrDefault(o => o.PupilID == pupilID && o.SemesterID == GlobalConstants.SEMESTER_OF_YEAR_SECOND);
                    TeacherNoteBookSemester tnSemYear = lstTeacherNoteBookSemsterDB.FirstOrDefault(o => o.PupilID == pupilID && o.SemesterID == GlobalConstants.SEMESTER_OF_YEAR_ALL);
                  
                    bool studiedSem1 = true;
                    if (lstExempSem1.Any(p => p.PupilID == pupilID) || cs.SectionPerWeekFirstSemester <= 0)
                    {
                        studiedSem1 = false;
                    }

                    bool studiedSem2 = true;
                    if (lstExempSem2.Any(p => p.PupilID == pupilID) || cs.SectionPerWeekSecondSemester <= 0)
                    {
                        studiedSem2 = false;
                    }

                    bool studiedOnlyOneSem = (studiedSem1 && !studiedSem2) || (!studiedSem1 && studiedSem2);

                    if (tnSemYear == null)
                    {
                        tnSemYear = new TeacherNoteBookSemester();
                        tnSemYear.SchoolID = SchoolID;
                        tnSemYear.PartitionID = SchoolID % 100;
                        tnSemYear.AcademicYearID = AcademicYearID;
                        tnSemYear.ClassID = classID;
                        tnSemYear.PupilID = pupilID;
                        tnSemYear.SubjectID = SubjectID;
                        tnSemYear.SemesterID = GlobalConstants.SEMESTER_OF_YEAR_ALL;
                    }

                    if (cs.IsCommenting.HasValue && cs.IsCommenting.Value == 1)
                    {
                        //Neu chi hoc 1 ky
                        if (studiedOnlyOneSem)
                        {
                            if (studiedSem1)
                            {
                                if (tnSem1 != null && !string.IsNullOrEmpty(tnSem1.AVERAGE_MARK_JUDGE))
                                {
                                    tnSemYear.AVERAGE_MARK_JUDGE = tnSem1.AVERAGE_MARK_JUDGE;
                                }
                                else
                                {
                                    if (tnSemYear.TeacherNoteBookSemesterID != 0)
                                    {
                                        this.Delete(tnSemYear.TeacherNoteBookSemesterID);
                                    }

                                    continue;
                                }
                            }
                            else
                            {
                                if (tnSem2 != null && !string.IsNullOrEmpty(tnSem2.AVERAGE_MARK_JUDGE))
                                {
                                    tnSemYear.AVERAGE_MARK_JUDGE = tnSem2.AVERAGE_MARK_JUDGE;
                                }
                                else
                                {
                                    if (tnSemYear.TeacherNoteBookSemesterID != 0)
                                    {
                                        this.Delete(tnSemYear.TeacherNoteBookSemesterID);
                                    }

                                    continue;
                                }
                            }
                        }
                        else
                        {
                            if (tnSem2 != null && !string.IsNullOrEmpty(tnSem2.AVERAGE_MARK_JUDGE))
                            {
                                tnSemYear.AVERAGE_MARK_JUDGE = tnSem2.AVERAGE_MARK_JUDGE;
                            }
                            else
                            {
                                if (tnSemYear.TeacherNoteBookSemesterID != 0)
                                {
                                    this.Delete(tnSemYear.TeacherNoteBookSemesterID);
                                }

                                continue;
                            }
                        }
                    }
                    else
                    {
                        if (studiedOnlyOneSem)
                        {
                            if (studiedSem1)
                            {
                                if (tnSem1 != null && tnSem1.AVERAGE_MARK.HasValue)
                                {
                                    tnSemYear.AVERAGE_MARK = tnSem1.AVERAGE_MARK;
                                }
                                else
                                {
                                    if (tnSemYear.TeacherNoteBookSemesterID != 0)
                                    {
                                        this.Delete(tnSemYear.TeacherNoteBookSemesterID);
                                    }

                                    continue;
                                }
                            }
                            else
                            {
                                if (tnSem2 != null && tnSem2.AVERAGE_MARK.HasValue)
                                {
                                    tnSemYear.AVERAGE_MARK = tnSem2.AVERAGE_MARK;
                                }
                                else
                                {
                                    if (tnSemYear.TeacherNoteBookSemesterID != 0)
                                    {
                                        this.Delete(tnSemYear.TeacherNoteBookSemesterID);
                                    }

                                    continue;
                                }
                            }
                        }
                        else
                        {
                            if (tnSem1 != null && tnSem1.AVERAGE_MARK.HasValue && tnSem2 != null && tnSem2.AVERAGE_MARK.HasValue)
                            {
                                tnSemYear.AVERAGE_MARK = CalculateAverageMark(tnSem1.AVERAGE_MARK, tnSem2.AVERAGE_MARK);
                            }
                            else
                            {
                                if (tnSemYear.TeacherNoteBookSemesterID != 0)
                                {
                                    this.Delete(tnSemYear.TeacherNoteBookSemesterID);
                                }

                                continue;
                            }
                        }
                    }

                    if (tnSemYear.TeacherNoteBookSemesterID != 0)
                    {
                        this.Update(tnSemYear);
                    }
                    else
                    {
                        this.Insert(tnSemYear);
                    }
                }
            }

            this.Save();
        }



        private IDictionary<string, object> TeacherNoteBookSemesterMaping()
        {
            Dictionary<string, object> columnMap = new Dictionary<string, object>();
            columnMap.Add("SchoolID", "SCHOOL_ID");
            columnMap.Add("AcademicYearID", "ACADEMIC_YEAR_ID");
            columnMap.Add("ClassID", "CLASS_ID");
            columnMap.Add("PupilID", "PUPIL_ID");
            columnMap.Add("SubjectID", "SUBJECT_ID");
            columnMap.Add("SemesterID", "SEMESTER_ID");
            columnMap.Add("PERIODIC_SCORE_MIDDLE", "PERIODIC_SCORE_MIDDLE");
            columnMap.Add("PERIODIC_SCORE_END", "PERIODIC_SCORE_END");
            columnMap.Add("PERIODIC_SCORE_MIDDLE_JUDGE", "PERIODIC_SCORE_MIDDLE_JUDGE");
            columnMap.Add("PERIODIC_SCORE_END_JUDGLE", "PERIODIC_SCORE_END_JUDGLE");
            columnMap.Add("AVERAGE_MARK", "AVERAGE_MARK");
            columnMap.Add("AVERAGE_MARK_JUDGE", "AVERAGE_MARK_JUDGE");
            columnMap.Add("Rate", "RATE");
            columnMap.Add("PartitionID", "PARTITION_ID");
            columnMap.Add("CreateTime", "CREATE_TIME");
            columnMap.Add("UpdateTime", "UPDATE_TIME");
            return columnMap;
        }
        #region function SMAS_MOBILE
        public void InsertOrUpdateToMobile(TeacherNoteBookSemester objTeacherNoteBookSemster, IDictionary<string, object> dic)
        {
            int SchoolID = Utils.GetInt(dic, "SchoolID");
            int MonthID = Utils.GetInt(dic, "MonthID");
            int AcademicYearID = Utils.GetInt(dic, "AcademicYearID");
            int ClassID = Utils.GetInt(dic, "ClassID");
            int SemesterID = Utils.GetInt(dic, "SemesterID");
            string TitleName = Utils.GetString(dic, "TitleName");
            List<int> lstSubjectID = new List<int>();
            lstSubjectID.Add(objTeacherNoteBookSemster.SubjectID);
            List<ClassSubject> lstClassSubject = ClassSubjectBusiness.GetListSubjectByClass(AcademicYearID, ClassID, lstSubjectID).ToList();
            ClassSubject objCS = null;
            List<TeacherNoteBookSemester> lstTeacherNoteBookSemsterDB = this.Search(dic).ToList();
            TeacherNoteBookSemester objTeacherNoteBookSemsterDB = null;
            List<TeacherNoteBookSemester> lstUpdate = new List<TeacherNoteBookSemester>();
            List<TeacherNoteBookSemester> lstInsert = new List<TeacherNoteBookSemester>();
            int partitionID = (SchoolID % 100);
            int? isCommenting = 0;
            //lay danh sach hoc sinh mien giam
            List<ExemptedSubject> lstExemp = ExemptedSubjectBusiness.Search(dic).ToList();
            bool isChange = false;
            objCS = lstClassSubject.Where(p => p.SubjectID == objTeacherNoteBookSemster.SubjectID).FirstOrDefault();
            if (objCS != null)
            {
                isCommenting = objCS.IsCommenting;
            }
            if (!lstExemp.Any(p => p.PupilID == objTeacherNoteBookSemster.PupilID && p.SubjectID == objTeacherNoteBookSemster.SubjectID))
            {
                objTeacherNoteBookSemsterDB = lstTeacherNoteBookSemsterDB.Where(p => p.PupilID == objTeacherNoteBookSemster.PupilID).FirstOrDefault();

                if (objTeacherNoteBookSemsterDB != null)
                {
                    if (MonthID != 15 && MonthID != 16)
                    {
                        if (isCommenting == 0)
                        {
                            if (objTeacherNoteBookSemsterDB.PERIODIC_SCORE_MIDDLE != objTeacherNoteBookSemster.PERIODIC_SCORE_MIDDLE)
                            {
                                objTeacherNoteBookSemsterDB.PERIODIC_SCORE_MIDDLE = objTeacherNoteBookSemster.PERIODIC_SCORE_MIDDLE;
                                isChange = true;
                            }
                        }
                        else
                        {
                            if (objTeacherNoteBookSemsterDB.PERIODIC_SCORE_MIDDLE_JUDGE != objTeacherNoteBookSemster.PERIODIC_SCORE_MIDDLE_JUDGE)
                            {
                                objTeacherNoteBookSemsterDB.PERIODIC_SCORE_MIDDLE_JUDGE = objTeacherNoteBookSemster.PERIODIC_SCORE_MIDDLE_JUDGE;
                                isChange = true;
                            }
                        }
                    }
                    else
                    {
                        if (isCommenting == 0)
                        {
                            if (objTeacherNoteBookSemsterDB.PERIODIC_SCORE_MIDDLE != objTeacherNoteBookSemster.PERIODIC_SCORE_MIDDLE && "KTGK".Equals(TitleName))
                            {
                                objTeacherNoteBookSemsterDB.PERIODIC_SCORE_MIDDLE = objTeacherNoteBookSemster.PERIODIC_SCORE_MIDDLE;
                                isChange = true;
                            }

                            if (objTeacherNoteBookSemsterDB.PERIODIC_SCORE_END != objTeacherNoteBookSemster.PERIODIC_SCORE_END && "KTCK".Equals(TitleName))
                            {
                                objTeacherNoteBookSemsterDB.PERIODIC_SCORE_END = objTeacherNoteBookSemster.PERIODIC_SCORE_END;
                                isChange = true;
                            }
                        }
                        else
                        {
                            if (objTeacherNoteBookSemsterDB.PERIODIC_SCORE_MIDDLE_JUDGE != objTeacherNoteBookSemster.PERIODIC_SCORE_MIDDLE_JUDGE)
                            {
                                objTeacherNoteBookSemsterDB.PERIODIC_SCORE_MIDDLE_JUDGE = objTeacherNoteBookSemster.PERIODIC_SCORE_MIDDLE_JUDGE;
                                isChange = true;
                            }

                            if (objTeacherNoteBookSemsterDB.PERIODIC_SCORE_END_JUDGLE != objTeacherNoteBookSemster.PERIODIC_SCORE_END_JUDGLE)
                            {
                                objTeacherNoteBookSemsterDB.PERIODIC_SCORE_END_JUDGLE = objTeacherNoteBookSemster.PERIODIC_SCORE_END_JUDGLE;
                                isChange = true;
                            }
                        }
                    }

                    if (isChange)
                    {
                        //Tinh diem TBM hoc ky
                        AcademicYear aca = AcademicYearBusiness.Find(AcademicYearID);
                        if (objCS.IsCommenting == 0)
                        {
                            if (SemesterID == 1 && aca.Year == 2016)
                            {
                                objTeacherNoteBookSemsterDB.AVERAGE_MARK = objTeacherNoteBookSemster.PERIODIC_SCORE_END;
                            }
                            else
                            {
                                objTeacherNoteBookSemsterDB.AVERAGE_MARK = CalculateAverageMarkHK(objTeacherNoteBookSemsterDB.PERIODIC_SCORE_MIDDLE, objTeacherNoteBookSemsterDB.PERIODIC_SCORE_END);
                            }
                        }
                        else
                        {
                            objTeacherNoteBookSemsterDB.AVERAGE_MARK_JUDGE = objTeacherNoteBookSemster.PERIODIC_SCORE_END_JUDGLE;
                        }

                        objTeacherNoteBookSemsterDB.UpdateTime = DateTime.Now.Date;
                        lstUpdate.Add(objTeacherNoteBookSemsterDB);
                    }
                }
                else
                {
                    if (isCommenting == 0)
                    {
                        if (objTeacherNoteBookSemster.PERIODIC_SCORE_MIDDLE.HasValue)
                        {
                            isChange = true;
                        }
                        if (objTeacherNoteBookSemster.PERIODIC_SCORE_END.HasValue)
                        {
                            isChange = true;
                        }
                        if (objTeacherNoteBookSemster.AVERAGE_MARK.HasValue)
                        {
                            isChange = true;
                        }
                    }
                    else
                    {
                        if (!string.IsNullOrEmpty(objTeacherNoteBookSemster.PERIODIC_SCORE_MIDDLE_JUDGE))
                        {
                            isChange = true;
                        }
                        if (!string.IsNullOrEmpty(objTeacherNoteBookSemster.PERIODIC_SCORE_END_JUDGLE))
                        {
                            isChange = true;
                        }
                        if (!string.IsNullOrEmpty(objTeacherNoteBookSemster.PERIODIC_SCORE_END_JUDGLE))
                        {
                            isChange = true;
                        }
                    }
                    if (isChange)
                    {
                        objTeacherNoteBookSemster.CreateTime = DateTime.Now.Date;
                        objTeacherNoteBookSemster.PartitionID = partitionID;
                        lstInsert.Add(objTeacherNoteBookSemster);
                    }
                }
            }
            if (lstUpdate.Count > 0)
            {
                for (int i = 0; i < lstUpdate.Count; i++)
                {
                    this.Update(lstUpdate[i]);
                }
                this.Save();
            }
            if (lstInsert.Count > 0)
            {
                this.BulkInsert(lstInsert, TeacherNoteBookSemesterMaping(), "TeacherNoteBookSemesterID");
            }
            dic.Add("lstSubjectID", lstSubjectID);
            List<RESTORE_DATA_DETAIL> lstRestoreDetail = new List<RESTORE_DATA_DETAIL>();
            CalculateAverageMarkYear(dic, null, ref lstRestoreDetail);
        }
        private decimal? CalculateAverageMarkHK(decimal? coef1Mark, decimal? coef2Mark)
        {
            if (!coef1Mark.HasValue || !coef2Mark.HasValue)
            {
                return null;
            }
            return System.Math.Round((coef1Mark.Value + (coef2Mark.Value * 2)) / 3, 1);
        }
        #endregion
    }
}
