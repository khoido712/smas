/**
 * The Viettel License
 *
 * Copyright 2012 Viettel Telecom. All rights reserved.
 * VIETTEL PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */

/** 
* @author  
* @version $Revision: $
*/

using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Linq.Expressions;
using System.Web;
using log4net;

using SMAS.DAL.Repository;
using SMAS.DAL.IRepository;
using SMAS.Business.IBusiness;
using SMAS.Models.Models;
using SMAS.Business.Common;

namespace SMAS.Business.Business
{ 
    public partial class QualificationTypeBusiness
    {
        /// <summary>
        /// Insert vao CSDL
        /// </summary>
        /// <param name="QualificationType"></param>
        /// <returns></returns>
        public override QualificationType Insert(QualificationType QualificationType)
        {
            // Kiem tra truong bat buoc
            Utils.ValidateRequire(QualificationType.Resolution, "DisableType_Label_Resolution");
            // Kiem tra truong Resolution maxlength khong duoc vuot qua 100 ky tu
            Utils.ValidateMaxLength(QualificationType.Resolution, 100, "DisableType_Label_Resolution");
            // Kiem tra trung ten 
            this.CheckDuplicate(QualificationType.Resolution, GlobalConstants.LIST_SCHEMA, "QualificationType", "Resolution", true, null, "DisableType_Label_Resolution");            
            // Them vao CSDL            
            return base.Insert(QualificationType);
        }

        /// <summary>
        /// Cap nhat vao CSDL
        /// </summary>
        /// <param name="QualificationType"></param>
        /// <returns></returns>
        public override QualificationType Update(QualificationType QualificationType)
        {
            // Kiem tra truong bat buoc
            Utils.ValidateRequire(QualificationType.Resolution, "DisableType_Label_Resolution");
            // Kiem tra truong Resolution maxlength khong duoc vuot qua 100 ky tu
            Utils.ValidateMaxLength(QualificationType.Resolution, 100, "DisableType_Label_Resolution");
            // Kiem tra trung ten 
            this.CheckDuplicate(QualificationType.Resolution, GlobalConstants.LIST_SCHEMA, "QualificationType", "Resolution", true, QualificationType.QualificationTypeID, "DisableType_Label_Resolution");            
            // Kiem tra ten hinh thuc dao tao da co trong DB
            //this.CheckAvailable(QualificationType.QualificationTypeID, "DisableType_Label_Resolution", false);
            new QualificationTypeBusiness(null).CheckAvailable(QualificationType.QualificationTypeID, "DisableType_Label_Resolution", false);
            // Cap nhat vao CSDL  
            return base.Update(QualificationType);
        }

        /// <summary>
        /// Xoa hinh thuc dao tao
        /// </summary>
        /// <param name="QualificationTypeID"></param>
        public void Delete(int QualificationTypeID)
        {
            // Kiem tra da ton tai trong  CSDL
            this.CheckAvailable(QualificationTypeID, "DisableType_Label_Title", true);
            // Xoa bang cach thiet lap IsActive = 0;
            this.Delete(QualificationTypeID, true);
        }

        /// <summary>
        /// Tim kiem hinh thuc dao tao
        /// </summary>
        /// <param name="dicParam"></param>
        /// <returns></returns>
        public IQueryable<QualificationType> Search(IDictionary<string, object> dicParam)
        {
            string Resolution = Utils.GetString(dicParam, "Resolution");
            bool? IsActive = Utils.GetIsActive(dicParam, "IsActive");
            IQueryable<QualificationType> listQualificationType = this.QualificationTypeRepository.All;
            if (!Resolution.Equals(string.Empty))
            {
                listQualificationType = listQualificationType.Where(x => x.Resolution.ToLower().Contains(Resolution.ToLower()));
            }
            if (IsActive.HasValue)
            {
                listQualificationType = listQualificationType.Where(x => x.IsActive == true);
            }
            int count = listQualificationType != null ? listQualificationType.Count() : 0;
            if (count == 0)
            {
                return null;
            }
            return listQualificationType;

        }
        
    }
}