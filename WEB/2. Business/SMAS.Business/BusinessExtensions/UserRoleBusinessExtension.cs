﻿/**
 * The Viettel License
 *
 * Copyright 2012 Viettel Telecom. All rights reserved.
 * VIETTEL PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */

/** 
* @author  
* @version $Revision: $
*/

using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Linq.Expressions;
using System.Web;
using log4net;

using SMAS.DAL.Repository;
using SMAS.DAL.IRepository;
using SMAS.Business.IBusiness;
using SMAS.Models.Models;

namespace SMAS.Business.Business
{
    public partial class UserRoleBusiness
    {
        IUserRoleRepository UserRoleRepository;
        public UserRoleBusiness(ILog logger, SMASEntities context = null)
            : base(logger)
        {
            if (context == null) { context = new SMASEntities(); } this.context = context;
            this.UserRoleRepository = new UserRoleRepository(context);
            repository = UserRoleRepository;
        }

        /// <summary>
        /// lay len user trong role
        /// Hungnd8 26/04/2013
        /// </summary>
        /// <param name="RoleID"></param>
        /// <returns></returns>
        public IQueryable<UserAccount> GetUsersInRoles(int RoleID)
        {
            IQueryable<UserAccount> userRoles = UserRoleRepository.All.Where(ur => ur.RoleID == RoleID && ur.UserAccount.IsActive == true)
                .Select(ur => ur.UserAccount);
            return userRoles;
        }

        /// <summary>
        /// Lay len role theo nguoi dung
        /// Hungnd8 26/04/2013
        /// </summary>
        /// <param name="UserAccountID"></param>
        /// <returns></returns>
        public IQueryable<Role> GetRolesOfUser(int UserAccountID)
        {
            IQueryable<Role> IqRoles = UserRoleBusiness.All.Where(ur => ur.UserID == UserAccountID 
                && ur.UserAccount.IsActive == true
                && ur.Role.IsActive == true).Select(u=>u.Role);
            return IqRoles;
        }

        public bool IsInRole(int UserAccountID, int RoleID)
        {
            return UserRoleRepository.All.Where(ur => ur.RoleID == RoleID && ur.UserID == UserAccountID).Any();
        }

        /// <summary>
        /// AssignRolesForAdmin
        /// </summary>
        /// <param name="UserAccountID">The user account ID.</param>
        /// <param name="ListRoleID">The list role ID.</param>
        /// <param name="AdminID">The admin ID.</param>
        /// <param name="selectedRoleID">The selected role ID.</param>
        /// <returns>
        /// Boolean
        /// </returns>
        /// <author>hath</author>
        /// <date>12/19/2012</date>
        public bool AssignRolesForAdmin(int UserAccountID, List<int> ListRoleID, int AdminID, int selectedRoleID)
        {
            //Nếu một trong các tham số là null thì không thực hiện gán, trả về false

            //Kiểm tra quyền: danh sách tất cả các Role định gán có là Role con của Role người thực hiện không?
            string RolePath = RoleBusiness.Find(selectedRoleID).RolePath;
            IQueryable<Role> lsRole = RoleBusiness.All.Where(o => o.RolePath.StartsWith(RolePath));
            if (lsRole.Count() > 0)
            {
                var query = from r in lsRole
                            join lr in ListRoleID on r.RoleID equals lr
                            select r;
                if (query.Count() == 0)
                {
                    return false;
                }
                else
                {
                    foreach (int RoleID in ListRoleID)
                    {
                        UserRole ur = new UserRole();
                        ur.RoleID = RoleID;
                        ur.UserID = AdminID;
                        base.Insert(ur);
                        this.Save();
                    }
                    return true;
                }
            }
            else
            {
                return false;
            }

            //Nếu không có quyền, trả về false

            //Nếu có quyền, thực hiện insert vào bảng UserRole các bản ghi ứng với ListRoleID và AdminID

            //Nếu thực hiện thành công, trả về true

        }
    }
}