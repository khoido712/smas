using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SMAS.Business.Business;
using SMAS.Models.Models;
using SMAS.VTUtils.Excel.Export;
using SMAS.Business.BusinessObject;
using System.IO;
using SMAS.Business.Common;
using SMAS.DAL.IRepository;
using log4net;
using SMAS.DAL.Repository;
using SMAS.Business.IBusiness;

namespace SMAS.Business.Business
{
    /// <summary>
    /// 
    /// </summary>
    /// <author>AuNH</author>
    /// <date>11/29/2012</date>
    public partial class DetailClassifiedSubjectByPeriodBusiness : GenericBussiness<ProcessedReport>, IDetailClassifiedSubjectByPeriodBusiness
    {
        public const string REPORT_CODE = "ThongKeXepLoaiChiTietTheoMonTheoDot";

        IProcessedReportParameterRepository ProcessedReportParameterRepository;
        IProcessedReportRepository ProcessedReportRepository;
        IMarkRecordRepository MarkRecordRepository;
        IJudgeRecordRepository JudgeRecordRepository;

        public DetailClassifiedSubjectByPeriodBusiness(ILog logger, SMASEntities context = null)
            : base(logger)
        {
            if (context == null) { context = new SMASEntities(); } this.context = context;
            this.ProcessedReportParameterRepository = new ProcessedReportParameterRepository(context);
            this.ProcessedReportRepository = new ProcessedReportRepository(context);
            this.MarkRecordRepository = new MarkRecordRepository(context);
            this.JudgeRecordRepository = new JudgeRecordRepository(context);
        }

        private WorkBookData BuildReportData(TranscriptOfClass Entity, List<StatisticsConfig> ListConfig, List<ClassInfo> ListClass,
            List<SubjectInfo> ListSubject, List<SummedUpRecord> ListMark, List<JudgeRecord> ListJudge)
        {
            WorkBookData WorkBookData = new WorkBookData();


            string AcademicYear = AcademicYearBusiness.Find(Entity.AcademicYearID).DisplayTitle;
            SchoolProfile School = SchoolProfileBusiness.Find(Entity.SchoolID);
            string SchoolName = School.SchoolName;
            string ProvinceName = School.Province.ProvinceName;
            string DeptName = School.SupervisingDept.SupervisingDeptName;

            PeriodDeclaration Period = PeriodDeclarationBusiness.Find(Entity.PeriodDeclarationID);
            string PeriodName = Period.Resolution;
            string Semester = Period.Semester == SystemParamsInFile.SEMESTER_OF_YEAR_FIRST ? "I" : "II";

            foreach (ClassInfo Class in ListClass)
            {
                SheetData SheetData = new SheetData(Class.ClassName);
                List<SubjectInfo> lstSubjectInfo = ListSubject.Where(o => o.ClassID == Class.ClassID).ToList();
                // Bo sung thong tin cho SubjectInfo
                foreach (SubjectInfo Subject in lstSubjectInfo)
                {
                    foreach (StatisticsConfig Config in ListConfig)
                    {
                        int Num = 0;
                        if (Subject.IsCommenting == SystemParamsInFile.ISCOMMENTING_TYPE_MARK)
                        {
                            Num = ListMark.Where(o => o.ClassID == Class.ClassID && o.SubjectID == Subject.SubjectID && o.SummedUpMark >= Config.MinValue && o.SummedUpMark < Config.MaxValue).Count();
                        }
                        else
                        {
                            Num = ListJudge.Where(o => o.ClassID == Class.ClassID && o.SubjectID == Subject.SubjectID && o.Judgement == Config.EqualValue).Count();
                        }
                        Subject.CapacityInfo[Config.StatisticsConfigID.ToString()] = Num;
                    }
                }

                SheetData.Data["ListConfig"] = ListConfig;
                SheetData.Data["ListSubject"] = lstSubjectInfo;
                SheetData.Data["NumOfPupil"] = Class.NumOfPupil;

                SheetData.Data["AcademicYear"] = AcademicYear;
                SheetData.Data["SchoolName"] = SchoolName.ToUpper();
                SheetData.Data["ProvinceName"] = ProvinceName;
                SheetData.Data["DeptName"] = DeptName.ToUpper();
                SheetData.Data["ReportDate"] = DateTime.Now;
                SheetData.Data["PeriodName"] = PeriodName;
                SheetData.Data["Semester"] = Semester;
                SheetData.Data["ClassName"] = Class.ClassName.ToUpper();

                WorkBookData.ListSheet.Add(SheetData);
            }

            return WorkBookData;
        }

        private IVTWorkbook WriteToExcel(WorkBookData Data, string ReportCode)
        {
            ReportDefinition reportDef = ReportDefinitionBusiness.GetByCode(ReportCode);
            string templatePath = ReportUtils.GetTemplatePath(reportDef);
            IVTWorkbook oBook = VTExport.OpenWorkbook(templatePath);

            IVTWorksheet Template = oBook.GetSheet(1);
            int lastRow = 11;
            foreach (SheetData SheetData in Data.ListSheet)
            {
                IVTWorksheet Sheet = oBook.CopySheetToLast(Template);
                lastRow = 11;
                int StartRow = 9;
                // Tao day du cac tieu de cot
                List<StatisticLevelConfig> ListConfig = (List<StatisticLevelConfig>)SheetData.Data["ListConfig"];
                List<SubjectInfo> ListSubject = (List<SubjectInfo>)SheetData.Data["ListSubject"];
                List<int> lstSumPupil = (List<int>)SheetData.Data["lstSumPupil"];


                int i = 0;
                foreach (StatisticLevelConfig config in ListConfig)
                {
                    Sheet.CopyPaste(Template.GetRange("D9", "E11"), StartRow, i * 2 + 4);
                    Sheet.SetCellValue(StartRow, i * 2 + 4, config.Title);
                    i++;
                }

                int j = 0;
                int k = 0;

                foreach (SubjectInfo Subject in ListSubject)
                {
                    Sheet.CopyPaste(Template.GetRange("A11", "B11"), StartRow + 2 + j, 1, true);
                    Sheet.SetCellValue(StartRow + 2 + j, 1, j + 1);
                    Sheet.SetCellValue(StartRow + 2 + j, 2, Subject.SubjectName);
                    Sheet.SetCellValue(StartRow + 2 + j, 3, lstSumPupil[k]);
                    k++;
                    lastRow++;
                    
                    i = 0;
                    foreach (StatisticLevelConfig config in ListConfig)
                    {
                        Sheet.CopyPaste(Template.GetRange("D11", "E11"), StartRow + 2 + j, i * 2 + 4, true);
                        if (Subject.CapacityInfo.ContainsKey(config.StatisticLevelConfigID.ToString()))
                        {
                            string cell = VTVector.ColumnIntToString(i * 2 + 4) + (StartRow + 2 + j);
                            //=IF(C11>0,ROUND(D11/C11*100,2),0)
                            //=IF(P8>0;IF({0}=\"\";\"\";ROUND({0}*100/$P$8;2));0)
                            Sheet.SetCellValue(StartRow + 2 + j, i * 2 + 4, Subject.CapacityInfo[config.StatisticLevelConfigID.ToString()]);
                            Sheet.SetCellValue(StartRow + 2 + j, i * 2 + 5, string.Format("=IF(C{0}>0,ROUND({1}/C{0}*100,2),0)", (StartRow + 2 + j), cell));
                        }
                        i++;
                    }
                    j++;
                }
                Sheet.GetRange(11, 3, lastRow - 1, 3).SetBorder(VTBorderStyle.Solid, VTBorderWeight.Thin, VTBorderIndex.All);
                Sheet.SetCellValue("A" + lastRow, "(*): Tổng số học sinh của lớp không bao gồm các học sinh được miễn giảm môn tương ứng");
                Sheet.SetCellValue("M" + (lastRow + 1), "NGƯỜI LẬP BÁO CÁO");
                Sheet.GetRange((lastRow + 1), 13, (lastRow + 1), 17).SetHAlign(VTHAlign.xlHAlignCenter);
                Sheet.GetRange((lastRow + 1), 13, (lastRow + 1), 17).SetFontStyle(true, System.Drawing.Color.Black, false, 13, false, false);
                Sheet.GetRange(lastRow + 1, 13, lastRow + 1, 17).Merge();
                Sheet.SetFontName("Times New Roman", 0);
                Sheet.Name = SheetData.Name;
                Sheet.FitSheetOnOnePage = true;
                Sheet.PageMaginLeft = 0.5;
                Sheet.PageMaginRight = 0.25;
                Sheet.PageMaginBottom = 0.5;
                Sheet.PageMaginTop = 0.5;
                Sheet.FillVariableValue(SheetData.Data);
            }

            Template.Delete();
            return oBook;
        }

        // ====================== Public method ====================================

        /// <summary>
        /// Lấy mảng băm
        /// </summary>
        /// <param name="Entity">The entity.</param>
        /// <returns>
        /// String
        /// </returns>
        /// <author>AuNH</author>
        /// <date>11/21/2012</date>
        public string GetHashKey(TranscriptOfClass Entity)
        {
            IDictionary<string, object> dic = new Dictionary<string, object>();
            dic["PeriodID"] = Entity.PeriodDeclarationID;
            dic["EducationLevelID"] = Entity.EducationLevelID;
            dic["AcademicYearID"] = Entity.AcademicYearID;
            dic["AppliedLevel"] = Entity.AppliedLevel;
            dic["SchoolID"] = Entity.SchoolID;
            return ReportUtils.GetHashKey(dic);
        }

        public ProcessedReport InsertDetailClassifiedSubjectByPeriod(TranscriptOfClass Entity, Stream Data)
        {
            ProcessedReport ProcessedReport = new ProcessedReport();

            ProcessedReport.ReportCode = REPORT_CODE;
            ProcessedReport.ProcessedDate = DateTime.Now;
            ProcessedReport.InputParameterHashKey = this.GetHashKey(Entity);
            ProcessedReport.ReportData = ReportUtils.Compress(Data);

            //Tạo tên file
            ReportDefinition reportDef = ReportDefinitionBusiness.GetByCode(ProcessedReport.ReportCode);
            string outputNamePattern = reportDef.OutputNamePattern;
            string educationLevel = this.EducationLevelBusiness.Find(Entity.EducationLevelID).Resolution;
            string semester = ReportUtils.ConvertSemesterForReportName(Entity.Semester);
            string periodName = this.PeriodDeclarationBusiness.Find(Entity.PeriodDeclarationID).Resolution;
            string appliedLevel = ReportUtils.ConvertAppliedLevelForReportName(Entity.AppliedLevel);


            outputNamePattern = outputNamePattern.Replace("[Semester]", ReportUtils.StripVNSign(semester));
            outputNamePattern = outputNamePattern.Replace("[EducationLevel]", ReportUtils.StripVNSign(educationLevel));
            outputNamePattern = outputNamePattern.Replace("[Period]", ReportUtils.StripVNSign(periodName));
            outputNamePattern = outputNamePattern.Replace("[SchoolType]", ReportUtils.StripVNSign(appliedLevel));
            ProcessedReport.ReportName = ReportUtils.RemoveSpecialCharacters(outputNamePattern) + "." + reportDef.OutputFormat;

            IDictionary<string, object> dic = new Dictionary<string, object>();
            dic["PeriodID"] = Entity.PeriodDeclarationID;
            dic["EducationLevelID"] = Entity.EducationLevelID;
            dic["AcademicYearID"] = Entity.AcademicYearID;
            dic["AppliedLevel"] = Entity.AppliedLevel;
            dic["SchoolID"] = Entity.SchoolID;
            ProcessedReportParameterRepository.Insert(dic, ProcessedReport);
            ProcessedReportRepository.Insert(ProcessedReport);

            ProcessedReportParameterRepository.Save();
            ProcessedReportRepository.Save();

            return ProcessedReport;
        }

        public ProcessedReport GetDetailClassifiedSubjectByPeriod(TranscriptOfClass Entity)
        {
            string ReportCode = REPORT_CODE;

            string HashKey = this.GetHashKey(Entity);
            IQueryable<ProcessedReport> query = this.ProcessedReportBusiness.All.Where(o => o.ReportCode == ReportCode);
            query = query.Where(o => o.InputParameterHashKey == HashKey);
            query = query.OrderByDescending(o => o.ProcessedDate);

            return query.FirstOrDefault();
        }

        public Stream CreateDetailClassifiedSubjectByPeriod(TranscriptOfClass Entity)
        {
          
            //Danh sach lop hoc
            IQueryable<ClassProfile> lsClass = ClassProfileBusiness.SearchBySchool(Entity.SchoolID, new Dictionary<string, object> {
                                                {"AppliedLevel", Entity.AppliedLevel},
                                                {"EducationLevelID", Entity.EducationLevelID},
                                                {"AcademicYearID", Entity.AcademicYearID}});


            IQueryable<PupilOfClass> lsPupilOfClassSemester1 = PupilOfClassBusiness.SearchBySchool(Entity.SchoolID, new Dictionary<string, object> {
                                                {"AppliedLevel", Entity.AppliedLevel},
                                                {"EducationLevelID", Entity.EducationLevelID},
                                                {"AcademicYearID", Entity.AcademicYearID},
                                                {"EthnicID",Entity.EthnicID},
                                                {"FemaleID",Entity.FemaleID}}).AddCriteriaSemester(AcademicYearBusiness.Find(Entity.AcademicYearID), Entity.Semester);
            IQueryable<PupilOfClass> lsPupilOfClassSemester = lsPupilOfClassSemester1.Where(o => o.AssignedDate == lsPupilOfClassSemester1.Where(u => u.PupilID == o.PupilID && u.ClassID == o.ClassID).Select(u => u.AssignedDate).Max());
            var tmpPoc = (from poc in lsPupilOfClassSemester
                          join cs in ClassSubjectBusiness.All on poc.ClassID equals cs.ClassID
                          where poc.SchoolID == Entity.SchoolID
                          && poc.AcademicYearID == Entity.AcademicYearID
                          && (cs.IsSubjectVNEN == null || cs.IsSubjectVNEN == false)
                          select new PupilOfClassBO
                          {
                              PupilID = poc.PupilID,
                              ClassID = poc.ClassID,
                              SubjectID = cs.SubjectID,
                              AppliedType = cs.AppliedType,
                              IsSpecialize = cs.IsSpecializedSubject
                          });
            //lay danh sach hoc sinh dang ky mon chuyen mon tu chon
            IDictionary<string, object> dic = new Dictionary<string, object>()
            {
                {"SchoolID",Entity.SchoolID},
                {"AcademicYearID",Entity.AcademicYearID},
                //{"SubjectID",Entity.SubjectID},
                {"SemesterID",Entity.Semester},
                {"YearID",AcademicYearBusiness.Find(Entity.AcademicYearID).Year}
            };

            List<PupilOfClassBO> lstPOC = this.GetPupilOfClassInSpecialize(tmpPoc, dic);
            //Danh sach hoc sinh mien giam
            List<ExemptedSubject> lstExempSubject = ExemptedSubjectBusiness.GetExmpSubjectbyAcademicYear(Entity.AcademicYearID, Entity.Semester, 0, 0);

            if (lstExempSubject!=null && lstExempSubject.Count > 0 && lstPOC!=null && lstPOC.Count>0)
            {
                //Khong lay cac hoc sinh mien giam
                lstPOC = lstPOC.Where(p => !lstExempSubject.Exists(s => s.PupilID == p.PupilID && p.ClassID == s.ClassID && s.SubjectID == p.SubjectID)).ToList();
            }



            dic.Add("Type", SystemParamsInFile.GET_NUMPUPIL_IN_CLASS_TYPE0);
            dic.Add("EthnicID", Entity.EthnicID);
            dic.Add("FemaleID", Entity.FemaleID);

            IQueryable<ClassInfoBO> iqClassInfoBO = PupilOfClassBusiness.GetClassAndTotalPupilNotRegister(lsClass, dic)
                                                    .OrderBy(p => p.EducationLevelID).ThenBy(p => p.ClassOrderNumber.HasValue ? p.ClassOrderNumber : 0)
                                                    .ThenBy(o => o.ClassName);

            var iqClassInfo = from c in iqClassInfoBO
                              group c by new{c.ClassID,c.ClassName,c.NumOfPupil} into g
                              select new ClassInfo
                              {
                                  ClassID = g.Key.ClassID,
                                  ClassName = g.Key.ClassName,
                                  NumOfPupil = g.Key.NumOfPupil
                              };
            List<ClassInfo> ListClass = iqClassInfo.ToList();

            // Danh sach mon
            List<SubjectInfo> ListSubject = ClassSubjectBusiness.SearchBySchool(Entity.SchoolID, new Dictionary<string, object> { 
                {"AcademicYearID", Entity.AcademicYearID},
                {"EducationLevelID", Entity.EducationLevelID},
                {"Semester",Entity.Semester}
            }).OrderBy(o => o.OrderInSchoolReport).Select(o => new SubjectInfo
            {
                SubjectID = o.SubjectID,
                OrderInSubject = o.SubjectCat.OrderInSubject,
                SubjectName = o.SubjectCat.DisplayName,
                ClassID = o.ClassID,
                IsCommenting = o.IsCommenting,
                IsVNEN = o.IsSubjectVNEN
            }).Distinct().ToList();


            //Danh sach diem
            IQueryable<VSummedUpRecord> tmpSummedUp = VSummedUpRecordBusiness.SearchBySchool(Entity.SchoolID, new Dictionary<string, object> {
                                                {"AppliedLevel", Entity.AppliedLevel},
                                                {"EducationLevelID", Entity.EducationLevelID},
                                                {"AcademicYearID", Entity.AcademicYearID},
                                                {"PeriodID", Entity.PeriodDeclarationID},
                                                });

            var lsSummedUp = (from u in tmpSummedUp.ToList()
                              join l in lstPOC on new { u.ClassID, u.PupilID, u.SubjectID } equals new { l.ClassID, l.PupilID, l.SubjectID }
                              select u);

            //Khong lay cac hoc sinh mien giam
            if (lstExempSubject != null && lstExempSubject.Count > 0)
            {
                lsSummedUp = lsSummedUp.Where(p => !lstExempSubject.Exists(s => s.PupilID == p.PupilID && p.ClassID == s.ClassID && s.SubjectID == p.SubjectID)).ToList();
            }

            //Viethd4: Fix lay muc thong ke theo bang moi

            //IQueryable<StatisticsConfig> iqStatisticsConfig = StatisticsConfigBusiness.GetIQueryable(Entity.AppliedLevel, SystemParamsInFile.StatisticsConfig.REPORT_TYPE_HOC_LUC_MON_CHI_TIET);
            List<StatisticLevelConfig> lstStatisticsConfig;
            int StatisticLevelReportID = Entity.StatisticLevelReportID;
            lstStatisticsConfig = StatisticLevelConfigBusiness.Search(new Dictionary<string, object>() { { "StatisticLevelReportID", StatisticLevelReportID } }).OrderBy(o => o.OrderNumber).ToList();
            lstStatisticsConfig.AddRange(new List<StatisticLevelConfig>() {
                    new StatisticLevelConfig{
                        Title="Đ",
                        StatisticLevelConfigID=-1
                    },
                    new StatisticLevelConfig
                    {
                        Title="CĐ",
                        StatisticLevelConfigID=-2
                    }
                });

            var listMarkGroup = from s in lstStatisticsConfig
                                join m in lsSummedUp on 1 equals 1
                                join cp in ClassProfileBusiness.SearchBySchool(Entity.SchoolID, new Dictionary<string, object> { 
                {"AcademicYearID", Entity.AcademicYearID},
                {"EducationLevelID", Entity.EducationLevelID}
            })
            on m.ClassID equals cp.ClassProfileID
                                //where (s.MinValue <= m.SummedUpMark && s.MaxValue > m.SummedUpMark && (s.EqualValue == null || s.EqualValue == " "))
                                //|| (s.EqualValue == m.JudgementResult && s.MinValue == null)
                                where (s.SignMin == SystemParamsInFile.Sign_Compare.GREATER_OR_EQUAL_SIGN
                                    && m.SummedUpMark >= s.MinValue && ((s.SignMax == SystemParamsInFile.Sign_Compare.SMALLER_OR_EQUAL_SIGN && m.SummedUpMark <= s.MaxValue)
                                                                        || (s.SignMax == SystemParamsInFile.Sign_Compare.SMALLER_SIGN && m.SummedUpMark < s.MaxValue)
                                                                        || (s.SignMax == SystemParamsInFile.Sign_Compare.EQUAL_SIGN && m.SummedUpMark == s.MaxValue)
                                                                        || (s.SignMax == null)))
                                || (s.SignMin == SystemParamsInFile.Sign_Compare.GREATEER_THAN_SIGN
                                    && m.SummedUpMark > s.MinValue && ((s.SignMax == SystemParamsInFile.Sign_Compare.SMALLER_OR_EQUAL_SIGN && m.SummedUpMark <= s.MaxValue)
                                                                        || (s.SignMax == SystemParamsInFile.Sign_Compare.SMALLER_SIGN && m.SummedUpMark < s.MaxValue)
                                                                        || (s.SignMax == SystemParamsInFile.Sign_Compare.EQUAL_SIGN && m.SummedUpMark == s.MaxValue)
                                                                        || (s.SignMax == null)))
                                || (s.SignMin == null && ((s.SignMax == SystemParamsInFile.Sign_Compare.SMALLER_OR_EQUAL_SIGN && m.SummedUpMark <= s.MaxValue)
                                                                        || (s.SignMax == SystemParamsInFile.Sign_Compare.SMALLER_SIGN && m.SummedUpMark < s.MaxValue)
                                                                        || (s.SignMax == SystemParamsInFile.Sign_Compare.EQUAL_SIGN && m.SummedUpMark == s.MaxValue)))
                                || (s.MinValue == null && s.MaxValue == null && s.Title == m.JudgementResult)
                                select new
                                {
                                    s.StatisticLevelConfigID,
                                    m.SchoolID,
                                    cp.EducationLevelID,
                                    m.SubjectID,
                                    m.Semester,
                                    m.PeriodID,
                                    m.ClassID
                                };

            var listMarkGroupCount = (from g in listMarkGroup
                                      group g by new
                                      {
                                          g.SchoolID,
                                          g.EducationLevelID,
                                          g.SubjectID,
                                          g.StatisticLevelConfigID,
                                          g.Semester,
                                          g.PeriodID,
                                          g.ClassID
                                      } into c
                                      select new
                                      {
                                          c.Key.SchoolID,
                                          c.Key.EducationLevelID,
                                          c.Key.SubjectID,
                                          c.Key.StatisticLevelConfigID,
                                          c.Key.Semester,
                                          c.Key.PeriodID,
                                          c.Key.ClassID,
                                          CountMark = c.Count()
                                      }
                                     ).ToList();


            WorkBookData WorkBookData = new WorkBookData();

            string AcademicYear = AcademicYearBusiness.Find(Entity.AcademicYearID).DisplayTitle;
            SchoolProfile School = SchoolProfileBusiness.Find(Entity.SchoolID);
            string SchoolName = School.SchoolName;
            string ProvinceName = (School.District != null ? School.District.DistrictName : "");
            string DeptName = UtilsBusiness.GetSupervisingDeptName(School.SchoolProfileID, Entity.AppliedLevel);
            string PupilName = string.Empty;
            if (Entity.FemaleID > 0 && Entity.EthnicID > 0)
            {
                PupilName = "HỌC SINH NỮ DÂN TỘC";
            }
            else if (Entity.EthnicID > 0)
            {
                PupilName = "HỌC SINH DÂN TỘC";
            }
            else if (Entity.FemaleID > 0)
            {
                PupilName = "HỌC SINH NỮ";
            }

            PeriodDeclaration Period = PeriodDeclarationBusiness.Find(Entity.PeriodDeclarationID);
            if (Period == null)
            {
                throw new BusinessException("Chưa chọn đợt");
            }
            string PeriodName = Period.Resolution;
            string Semester = Period.Semester == SystemParamsInFile.SEMESTER_OF_YEAR_FIRST ? "I" : "II";

            //Viethd4: Fix sy so
            

            var listCountPupilByClass = lsPupilOfClassSemester1.GroupBy(o => o.ClassID).Select(o => new { ClassID = o.Key, TotalPupil = o.Count() }).ToList();
            //Khong lay cac hoc sinh mien giam

            int totalPupilOfSubject = 0;
            List<int> lstSumPupil = new List<int>();
            foreach (ClassInfo Class in ListClass)
            {
                totalPupilOfSubject = 0;
                lstSumPupil = new List<int>();
                SheetData SheetData = new SheetData(Utils.StripVNSignAndSpace(ReportUtils.RemoveSpecialCharacters(Class.ClassName)));
                List<SubjectInfo> lstSubjectInfo = ListSubject.Where(o => o.ClassID == Class.ClassID).OrderBy(o => o.OrderInSubject).ThenBy(o => o.SubjectName).ToList();
                // Bo sung thong tin cho SubjectInfo
                foreach (SubjectInfo Subject in lstSubjectInfo)
                {
                    // Tổng học sinh
                    totalPupilOfSubject = lstPOC.Where(p => p.ClassID == Class.ClassID
                                                                && p.SubjectID == Subject.SubjectID                             
                                                                ).Select(p => p.PupilID).Distinct().Count();
                    lstSumPupil.Add(totalPupilOfSubject);
                    foreach (StatisticLevelConfig Config in lstStatisticsConfig)
                    {
                        int Num = 0;
                        if (Subject.IsVNEN == null || Subject.IsVNEN == false)
                        {
                            var count = listMarkGroupCount.Where(o => o.StatisticLevelConfigID == Config.StatisticLevelConfigID && o.ClassID == Class.ClassID && o.SubjectID == Subject.SubjectID).FirstOrDefault();
                            if (count != null)
                            {
                                Num = count.CountMark;
                            }
                        }

                        Subject.CapacityInfo[Config.StatisticLevelConfigID.ToString()] = Num;
                    }
                }

                //Viethd4: Fix sy so
                var objCountPupil = listCountPupilByClass.Where(o => o.ClassID == Class.ClassID).FirstOrDefault();
                int sumPupil = objCountPupil != null ? objCountPupil.TotalPupil : 0;

                SheetData.Data["ListConfig"] = lstStatisticsConfig;
                SheetData.Data["ListSubject"] = lstSubjectInfo;
                SheetData.Data["NumOfPupil"] = sumPupil;
                SheetData.Data["lstSumPupil"] = lstSumPupil;

                SheetData.Data["AcademicYear"] = AcademicYear;
                SheetData.Data["SchoolName"] = SchoolName.ToUpper();
                SheetData.Data["ProvinceName"] = ProvinceName;
                SheetData.Data["DeptName"] = DeptName.ToUpper();
                SheetData.Data["ReportDate"] = DateTime.Now;
                SheetData.Data["PeriodName"] = PeriodName;
                SheetData.Data["Semester"] = Semester;
                SheetData.Data["ClassName"] = Class.ClassName.ToUpper();
                SheetData.Data["PupilName"] = PupilName;

                WorkBookData.ListSheet.Add(SheetData);
            }


            //Data = this.BuildReportData(Entity, ListConfig, ListClass, ListSubject, ListMark, ListJudge);

            IVTWorkbook oBook = this.WriteToExcel(WorkBookData, REPORT_CODE);
            return oBook.ToStream();
        }


        // ============= Private classes for private businesses ====================

        private class ClassInfo
        {
            public int ClassID { get; set; }
            public int EducationLevelID { get; set; }
            public int? ClassOrderNumber { get; set; }
            public string ClassName { get; set; }
            public int NumOfPupil { get; set; }
        }

        private class SubjectInfo
        {
            public int SubjectID { get; set; }
            public string SubjectName { get; set; }
            public int ClassID { get; set; }
            public int? IsCommenting { get; set; }
            public int? OrderInSubject { get; set; }
            public Dictionary<string, object> CapacityInfo { get; set; }
            public bool? IsVNEN { get; set; }

            public SubjectInfo()
            {
                CapacityInfo = new Dictionary<string, object>();
            }
        }
    }
}
