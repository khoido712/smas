﻿/**
 * The Viettel License
 *
 * Copyright 2012 Viettel Telecom. All rights reserved.
 * VIETTEL PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */

/** 
* @author  
* @version $Revision: $
*/

using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Linq.Expressions;
using System.Web;
using log4net;

using SMAS.DAL.Repository;
using SMAS.DAL.IRepository;
using SMAS.Business.IBusiness;
using SMAS.Models.Models;
using SMAS.Business.Common;

namespace SMAS.Business.Business
{ 
    public partial class ClassAssigmentBusiness
    {


        #region Search

        public IQueryable<ClassAssigment> Search(IDictionary<string, object> SearchInfo)
        {
            int ClassAssignmentID = Utils.GetInt(SearchInfo, "ClassAssignmentID");
            int ClassID = Utils.GetInt(SearchInfo, "ClassID");
            int EducationLevelID = Utils.GetInt(SearchInfo, "EducationLevelID");
            int AppliedLevel = Utils.GetInt(SearchInfo, "AppliedLevel");
            int SchoolID = Utils.GetInt(SearchInfo, "SchoolID");
            int AcademicYearID = Utils.GetInt(SearchInfo, "AcademicYearID");
            int TeacherID = Utils.GetInt(SearchInfo, "TeacherID");
            string AssignmentWork = Utils.GetString(SearchInfo, "AssignmentWork");
            bool? IsHeadTeacher = Utils.GetNullableBool(SearchInfo, "IsHeadTeacher");

            IQueryable<ClassAssigment> lstClassAssigment = this.ClassAssigmentRepository.All;

            if (ClassID != 0)
            {
                lstClassAssigment = from ca in lstClassAssigment
                                    join cp in ClassProfileBusiness.All on ca.ClassID equals cp.ClassProfileID
                                    where (!cp.IsActive.HasValue || (cp.IsActive.HasValue && cp.IsActive.Value))
                                    && ca.ClassID == ClassID
                                    select ca;
            }

            if (ClassAssignmentID != 0)
            {
                lstClassAssigment = lstClassAssigment.Where(o => o.ClassAssigmentID == ClassAssignmentID);
            }

            if (AppliedLevel != 0)
            {
                lstClassAssigment = lstClassAssigment.Where(o => o.ClassProfile.EducationLevel.Grade == AppliedLevel);
            }

            if (EducationLevelID != 0)
            {
                lstClassAssigment = lstClassAssigment.Where(o => o.ClassProfile.EducationLevelID == EducationLevelID);
            }

            if (SchoolID != 0)
            {
                lstClassAssigment = lstClassAssigment.Where(o => o.SchoolID == SchoolID);
            }

            if (AcademicYearID != 0)
            {
                lstClassAssigment = lstClassAssigment.Where(o => o.AcademicYearID == AcademicYearID);
            }

            if (TeacherID != 0)
            {
                lstClassAssigment = lstClassAssigment.Where(o => o.TeacherID == TeacherID);
            }

            if (AssignmentWork.Length != 0)
            {
                lstClassAssigment = lstClassAssigment.Where(o => o.AssignmentWork == AssignmentWork);
            }
            if (IsHeadTeacher.HasValue)
            {
                lstClassAssigment = lstClassAssigment.Where(o => o.IsHeadTeacher == IsHeadTeacher);
            }

            return lstClassAssigment;

        }
        
        #endregion

        #region SearchBySchool
        /// <summary>
        /// Lấy ra thông tin từ mã trường
        /// </summary>
        /// <author>tungnd</author>
        /// <date>12/11/2012</date>
        /// <param name="ethnic"></param>
        /// <returns></returns>

        public IQueryable<ClassAssigment> SearchBySchool(int SchoolID, IDictionary<string, object> SearchInfo)
        {
            if (SchoolID == 0)
            {
                return null;
            }
            else
            {
                SearchInfo["SchoolID"] = SchoolID;
                return Search(SearchInfo);

            }

        }


        #endregion

        #region UpdateClass
        public void UpdateClass(int AcademicYearID, int SchoolID, int ClassID,
            List<int> lstAssigmentTeacherID, List<string> lstAssignmentWork, List<int> lstIsTeacherID)
        {          
            
            ClassProfile classProfile = this.ClassProfileBusiness.Find(ClassID);
            int? headTeacherID = classProfile.HeadTeacherID;
            // SchoolID và AcademicYearID not compatible trong AcademicYear
            IDictionary<string, object> SearchInfoAcademicYear = new Dictionary<string, object>();
            SearchInfoAcademicYear["SchoolID"] = SchoolID;
            SearchInfoAcademicYear["AcademicYearID"] = AcademicYearID;

            bool AcademicYearExist = new AcademicYearRepository(this.context).ExistsRow(GlobalConstants.SCHOOL_SCHEMA, "AcademicYear", SearchInfoAcademicYear, null);
           
            if (!AcademicYearExist)
            {
                throw new BusinessException("Common_Validate_NotCompatible");
            }

            if (lstAssigmentTeacherID.Count != lstAssignmentWork.Count)
            {
                throw new BusinessException("Common_Validate_ErrorData");
            }

            List<int> lstEmployee = EmployeeBusiness.SearchWorkingTeacherByFaculty(SchoolID).Select(o => o.EmployeeID).ToList();
            foreach (var itemTeacherID in lstAssigmentTeacherID)
            {
                //TeacherID (EmployeeID), SchoolID: not compatiable(Employee)
                IDictionary<string, object> SearchInfoEmployee = new Dictionary<string, object>();
                SearchInfoEmployee["EmployeeID"] = itemTeacherID;
                SearchInfoEmployee["SchoolID"] = SchoolID;
                bool SearchInfoEmployeeExist = new EmployeeRepository(this.context).ExistsRow(GlobalConstants.SCHOOL_SCHEMA, "Employee", SearchInfoEmployee, null);
                if (!SearchInfoEmployeeExist)
                {
                    throw new BusinessException("Common_Validate_NotCompatible");
                }
                if (lstEmployee.Where(o => o == itemTeacherID).Count() == 0)
                {
                    throw new BusinessException("Common_Validate_NotEmployeeWorking");
                }
            }

            List<ClassProfile> lstClassProfile = new List<ClassProfile>();
          
            List<ClassAssigment> lstClassAssignment = ClassAssigmentBusiness.All.Where(o => classProfile.ClassProfileID == o.ClassID
                                                                                      && o.SchoolID == SchoolID
                                                                                       && o.AcademicYearID == AcademicYearID
                                                                                       && !o.IsHeadTeacher).ToList();
            
            this.ClassAssigmentRepository.DeleteAll(lstClassAssignment);

            ClassAssigment ca = null;
            int teacherID = 0;
            for (int i = 0; i < lstAssigmentTeacherID.Count(); i++)
            {
                teacherID = lstAssigmentTeacherID[i];
                ca = new ClassAssigment();
                ca.IsHeadTeacher = false;
                ca.AssignmentWork = lstAssignmentWork[i];
                ca.TeacherID = teacherID;
                ca.ClassID = ClassID;
                ca.SchoolID = SchoolID;
                ca.AcademicYearID = AcademicYearID;
                ca.IsTeacher = false;
                this.ClassAssigmentRepository.Insert(ca);

                if (lstIsTeacherID.Contains(teacherID))
                {
                    ca = new ClassAssigment();
                    ca.IsHeadTeacher = false;
                    ca.AssignmentWork = lstAssignmentWork[i];
                    ca.TeacherID = teacherID;
                    ca.ClassID = ClassID;
                    ca.SchoolID = SchoolID;
                    ca.AcademicYearID = AcademicYearID;
                    ca.IsTeacher = true;
                    this.ClassAssigmentRepository.Insert(ca);
                }
            }
            this.ClassAssigmentRepository.Save();

        }

        #endregion

        #region Delete

        public void Delete(int SchoolID, int ClassAssignmentID)
        {

            // SchoolID và AcademicYearID not compatible trong AcademicYear
            IDictionary<string, object> SearchInfo = new Dictionary<string, object>();
            SearchInfo["SchoolID"] = SchoolID;
            SearchInfo["ClassAssigmentID"] = ClassAssignmentID;

            bool SearchInfoExist = new ClassAssigmentRepository(this.context).ExistsRow(GlobalConstants.SCHOOL_SCHEMA, "ClassAssigment", SearchInfo, null);

            if (!SearchInfoExist)
            {
                throw new BusinessException("Common_Validate_NotCompatible");
            }

            ClassAssigment classassignment = this.ClassAssigmentRepository.Find(ClassAssignmentID);

            if (!AcademicYearBusiness.IsCurrentYear(classassignment.AcademicYearID))
            {
                throw new BusinessException("Common_Validate_NotAcademicYear");
            }

            if (classassignment != null)
            {
                List<ClassAssigment> lstClassAss = ClassAssigmentBusiness.All.Where(x => x.ClassID == classassignment.ClassID 
                                                                                    && x.TeacherID == classassignment.TeacherID 
                                                                                    && !x.IsHeadTeacher).ToList();

                ClassAssigmentRepository.DeleteAll(lstClassAss);
                ClassAssigmentRepository.Save();
            }
        }
        #endregion

    }
}