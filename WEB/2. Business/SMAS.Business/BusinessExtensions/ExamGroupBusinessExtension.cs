﻿/**
 * The Viettel License
 *
 * Copyright 2012 Viettel Telecom. All rights reserved.
 * VIETTEL PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */

/** 
* @author  
* @version $Revision: $
*/

using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Linq.Expressions;
using System.Web;
using log4net;

using SMAS.DAL.Repository;
using SMAS.DAL.IRepository;
using SMAS.Business.IBusiness;
using SMAS.Models.Models;
using SMAS.Business.Common;

namespace SMAS.Business.Business
{ 
    public partial class ExamGroupBusiness
    {

        public List<ExamGroup> GetListExamGroupByExaminationsID(long examID)
        {
            List<ExamGroup> listResult = this.All.Where(p => p.ExaminationsID == examID).ToList();
            return listResult;
        }

        public IQueryable<ExamGroup> Search(IDictionary<string, object> search)
        {
            long examinationsID = Utils.GetLong(search, "ExaminationsID");
            IQueryable<ExamGroup> query = this.All;
            if (examinationsID != 0)
            {
                query = query.Where(o => o.ExaminationsID == examinationsID);
            }
            return query.OrderBy(p=>p.ExamGroupCode);
        }

        #region insert
        /// <summary>
        /// Thêm mới 
        /// <author>viethd4</author>
        /// <date>05/12/2014</date>
        /// </summary>
        /// <param name="Entity">doi tuong can them moi</param>
        /// <returns></returns>
        public override ExamGroup Insert(ExamGroup Entity)
        {
            ValidationMetadata.ValidateObject(Entity);

            //check trùng tên
            int count = ExamGroupRepository.All.Where(o => o.ExaminationsID == Entity.ExaminationsID)
                                                        .Where(o => o.ExamGroupCode.ToUpper() == Entity.ExamGroupCode.ToUpper()).Count();
            if (count>0)
            {
                throw new BusinessException("ExamGroup_Validate_Exist_ExamGroupCode", "");
            }

            Entity.ExamGroupID = this.ExamGroupRepository.GetNextSeq<int>();
            return base.Insert(Entity);
        }
        #endregion

        #region update
        /// <summary>
        /// Thêm mới 
        /// <author>viethd4</author>
        /// <date>05/12/2014</date>
        /// </summary>
        /// <param name="Entity">doi tuong can them moi</param>
        /// <returns></returns>
        public override ExamGroup Update(ExamGroup Entity)
        {
            ValidationMetadata.ValidateObject(Entity);

            //check trùng tên
            int count = ExamGroupRepository.All.Where(o => o.ExaminationsID == Entity.ExaminationsID)
                                                        .Where(o => o.ExamGroupCode.ToUpper() == Entity.ExamGroupCode.ToUpper())
                                                        .Where(o=>o.ExamGroupID!=Entity.ExamGroupID).Count();
            if (count > 0)
            {
                throw new BusinessException("ExamGroup_Validate_Exist_ExamGroupCode", "");
            }

            
            return base.Update(Entity);
        }
        #endregion

        /// <summary>
        /// Kiểm tra nhóm thi đã có dữ liệu ràng buộc hay chưa
        /// </summary>
        /// <param name="examGroupID">Danh sach ID nhóm thi</param>
        /// <returns>Danh sach cac ID nhom thi da co du lieu rang buoc</returns>
        public List<long> ExamGroupInUsed(List<long> inputExamGroupID)
        {
            List<long> ret = new List<long>();

           ret.AddRange(this.ExamSubjectBusiness.All.Where(o => inputExamGroupID.Contains(o.ExamGroupID)).Select(o => o.ExamGroupID).ToList<long>());
           ret.AddRange(this.ExamPupilBusiness.All.Where(o => inputExamGroupID.Contains(o.ExamGroupID)).Select(o => o.ExamGroupID).ToList<long>());

           return ret.Distinct().ToList();
        }

        /// <summary>
        /// Kiểm tra nhóm thi đã có dữ liệu ràng buộc hay chưa
        /// </summary>
        /// <param name="examGroupID">ID nhóm thi</param>
        /// <returns>True neu da co du lieu rang buoc</returns>
        public bool isInUsed(long examGroupID)
        {
            if (this.ExamSubjectBusiness.All.Where(o => o.ExamGroupID == examGroupID).Count() > 0)
            {
                return true;
            }

            if (this.ExamPupilBusiness.All.Where(o => o.ExamGroupID == examGroupID).Count() > 0)
            {
                return true;
            }

            return false;
        }
    }
}