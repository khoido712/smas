﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SMAS.Business.Common;
using SMAS.Models.Models;
using System.Data.Entity;

namespace SMAS.Business.Business
{
    public partial class CodeConfigBusiness
    {
        /// <summary>
        /// Them moi ma so tu dong
        /// </summary>
        /// <author>HieuND</author>
        /// <param name="codeConfig">Entity</param>
        /// <returns>Entity sau khi da duoc them</returns>
        public override CodeConfig Insert(CodeConfig codeConfig)
        {
            ValidationMetadata.ValidateObject(codeConfig);
            if (codeConfig.MiddleCodeType.HasValue)
                Utils.ValidateRange(codeConfig.MiddleCodeType.Value, 1, 4, "CodeConfig_Label_MiddleCodeType");

            CodeConfig cc = this.All.OrderByDescending(u => u.CreatedDate).FirstOrDefault(u => u.ProvinceID == codeConfig.ProvinceID && u.CodeType == codeConfig.CodeType);

            if (cc == null || cc.CreatedDate.Date != DateTime.Now.Date)
            {
                if (cc != null)
                {
                    cc.IsActive = false;
                    base.Update(cc);
                }

                return base.Insert(codeConfig);
            }
            else
            {
                cc.IsActive = codeConfig.IsActive;
                cc.IsChoiceSchool = codeConfig.IsChoiceSchool;
                cc.IsSchoolModify = codeConfig.IsSchoolModify;
                cc.IsStringIdentify = codeConfig.IsStringIdentify;
                cc.MiddleCodeType = codeConfig.MiddleCodeType;
                cc.NumberLength = codeConfig.NumberLength;
                cc.StringIdentify = codeConfig.StringIdentify;
                cc.FormatNumber = codeConfig.FormatNumber;

                return base.Update(cc);
            }
        }

        /// <summary>
        /// Cap nhat ma so tu dong
        /// </summary>
        /// <param name="CodeConfig">Entity</param>
        /// <returns>Entity sau khi da duoc cap nhat</returns>
        public override CodeConfig Update(CodeConfig codeConfig)
        {
            ValidationMetadata.ValidateObject(codeConfig);
            if (codeConfig.MiddleCodeType.HasValue)
                Utils.ValidateRange(codeConfig.MiddleCodeType.Value, 1, 4, "CodeConfig_Label_MiddleCodeType");

            return base.Update(codeConfig);
        }

        /// <summary>
        /// Xoa ma so tu dong - Khong co trong thiet ke nghiep vu nhung phai implement Interface IGenericBusiness
        /// </summary>
        /// <param name="CodeConfigID">ID cua ma so tu dong</param>
        public void Delete(int CodeConfigID)
        {
            this.CheckAvailable(CodeConfigID, "CodeConfig_Label_CodeConfigID", true);

            this.CheckConstraints(GlobalConstants.LIST_SCHEMA, "CodeConfig", CodeConfigID, "CodeConfig_Label_CodeConfigID");

            base.Delete(CodeConfigID, true);

        }

        /// <summary>
        /// Tim kiem maso tu dong - Khong co trong thiet ke nghiep vu nhung phai implement Interface IGenericBusiness
        /// </summary>
        /// <author>HieuND</author>
        /// <param name="dic">Chua cac thong so de tim kiem</param>
        /// <param name="CodeType">Kieu ma so : Hoc sinh(1) hay giao vien(2)</param>
        /// <param name="ProvinceID">ID cua tinh thanh pho tim kiem</param>
        /// <returns>IQueryable chua danh sach tim kiem theo dieu kien tren</returns>
        public IQueryable<CodeConfig> Search(IDictionary<string, object> dic)
        {
            int? codeType = Utils.GetNullableByte(dic, "CodeType");
            int? provinceID = Utils.GetNullableShort(dic, "ProvinceID");

            IQueryable<CodeConfig> lsCodeConfig = this.CodeConfigRepository.All;
            if (codeType.HasValue) lsCodeConfig = lsCodeConfig.Where(u => u.CodeType == codeType.Value);
            if (provinceID.HasValue) lsCodeConfig = lsCodeConfig.Where(u => u.ProvinceID == provinceID.Value);
            return lsCodeConfig;
        }

        /// <summary>
        /// Tim kiem ma so tu dong theo tinh thanh
        /// </summary>
        /// <author>HieuND</author>
        /// <param name="provinceID">ID cua tinh thanh pho tim kiem </param>
        /// <param name="codeType">Kieu ma so : Hoc sinh(1) hay giao vien(2)</param>
        /// <returns>Entity cua tinh thanh tim duoc</returns>
        public CodeConfig SearchByProvince(int provinceID, int codeType)
        {
            return this.CodeConfigRepository.All.OrderByDescending(o => o.CreatedDate).FirstOrDefault(u => u.ProvinceID == provinceID && u.CodeType == codeType && u.IsActive == true);
        }

        /// <summary>
        /// Tim kiem ma so tu dong theo truong
        /// </summary>
        /// <author>HieuND</author>
        /// <param name="SchoolID">ID cua truong hoc can tim</param>
        /// <param name="CodeType">Kieu ma so : Hoc sinh(1) hay giao vien(2)</param>
        /// <returns></returns>
        public CodeConfig SearchBySchool(int SchoolID, int CodeType)
        {
            //this.SchoolProfileBusiness.CheckAvailable(SchoolID, "SchoolProfile_Label_SchoolProfileID");
            var school = this.SchoolProfileBusiness.Find(SchoolID);

            return this.CodeConfigRepository.All.OrderByDescending(u => u.CreatedDate).FirstOrDefault(u => u.ProvinceID == school.ProvinceID && u.CodeType == CodeType && u.IsActive == true);
        }

        /// <summary>
        /// Lay so thu tu nam trong code
        /// </summary>
        /// <author>HieuND</author>
        /// <param name="code">Ma so tu dong</param>
        /// <returns>So thu tu cua ma so (nam o vi tri cuoi cung)</returns>
        public int GetOrderInCode(string code, int pos)
        {
            if (string.IsNullOrEmpty(code)) return 0;

            //int lastIndex = code.LastIndexOf('-');
            int lastIndex = pos;
            if (lastIndex <= 0) return 0;

            int incrementCode = 0;
            try
            {
                string incrementCodeStr = code.Substring(lastIndex);
                incrementCode = Convert.ToInt32(incrementCodeStr);
            }
            catch (Exception )
            {
            }
            return incrementCode;
        }

        /// <summary>
        /// Kiem tra xem So co ma so tu dong da Active khong
        /// </summary>
        /// <author>HieuND</author>
        /// <param name="ShoolID">ID cua truong hoc</param>
        /// <param name="CodeType">Kieu ma so : Hoc sinh(1) hay giao vien(2)</param>
        /// <returns>Bool : Da Active hay chua</returns>
        public bool IsAutoGenCode(int ShoolID, int CodeType)
        {
            CodeConfig codeConfig = SearchBySchool(ShoolID, CodeType);
            if (codeConfig == null) return false;

            return codeConfig.IsActive;
        }

        /// <summary>
        /// Kiem tra So co cau hinh danh ma tu dong khong
        /// </summary>
        /// <author>HieuND</author>
        /// <param name="ShoolID">ID cua truong hoc thuoc so can kiem tra</param>
        /// <param name="CodeType">Kieu ma so : Hoc sinh(1) hay giao vien(2)</param>
        /// <returns>Bool: Co duoc thay doi ma so hay khong</returns>
        public bool IsSchoolModify(int ShoolID, int CodeType)
        {
            CodeConfig codeConfig = SearchBySchool(ShoolID, CodeType);
            // AnhVD 20140811 - Neu So chua chua cau hinh thi duoc phep sua ma
            if (codeConfig == null) return true;

            return codeConfig.IsSchoolModify;
        }

        /// <summary>
        /// Doi tu so sang xau co do dai mong muon
        /// </summary>
        /// <author>HieuND</author>
        /// <param name="number">So truyen vao</param>
        /// <param name="length">Do dai cua sau</param>
        /// <returns>So da duoc dien gia tri 0</returns>
        /// <example>number=23, length=5, tra ve : 00023</example>
        private string ToStringWithFixedLength(int number, int length)
        {
            if (number < 0) 
            {
                throw new BusinessException("CodeConfig_Label_Exception");
            }
            if (number.ToString().Length > length)
            {               
                throw new BusinessException("CodeConfig_Label_Exception");
            }

            return number.ToString().PadLeft(length, '0');
        }

        /// <summary>
        /// Lay ma so tu dong cua hoc sinh
        /// </summary>
        /// <author>HieuND</author>
        /// <param name="AcademicYearID">Nam hoc</param>
        /// <param name="educationLevelID">Khoi hoc</param>
        /// <param name="schoolID">Truong hoc</param>
        /// <returns></returns>
        public string CreatePupilCode(int AcademicYearID, int educationLevelID, int schoolID,int increase = 0)
        {
            var school = this.SchoolProfileBusiness.Find(schoolID);
            var academicYear = this.AcademicYearBusiness.Find(AcademicYearID);
            var educationLevel = this.EducationLevelBusiness.Find(educationLevelID);
            var codeConfig = SearchBySchool(schoolID, GlobalConstants.CODECONFIG_CODETYPE_PUPIL);
            int? formatNumber = codeConfig.FormatNumber;

            int minEducationLevel = this.EducationLevelRepository.All.Where(u => u.IsActive && u.Grade == educationLevel.Grade).Min(u => u.EducationLevelID);
            string pupilCode = school.SchoolCode;
            int year = academicYear.Year;

             

            switch (codeConfig.MiddleCodeType)
            {
                case SystemParamsInFile.CODECONFIG_MIDDLECODETYPE_START_COURSE_YEAR:
                    pupilCode += ((formatNumber.HasValue ? formatNumber.Value == 1 ? (year % 100) : year : (year % 100)) - (educationLevelID - minEducationLevel)).ToString().PadLeft(2, '0');
                    break;
                case SystemParamsInFile.CODECONFIG_MIDDLECODETYPE_START_LEVEL_1:
                    pupilCode += ((formatNumber.HasValue ? formatNumber.Value == 1 ? (year % 100) : year : (year % 100)) - (educationLevelID - 1)).ToString().PadLeft(2, '0');
                    break;
                case SystemParamsInFile.CODECONFIG_MIDDLECODETYPE_COURSE:
                    pupilCode += "K" + school.SchoolYearTitle;
                    break;
                default:
                    break;
            }

            if (codeConfig.IsStringIdentify) pupilCode += codeConfig.StringIdentify;

            var lstPupilCode = (from pos in PupilOfSchoolRepository.All
                                join pf in PupilProfileBusiness.All on pos.PupilID equals pf.PupilProfileID
                                where pos.SchoolID == schoolID
                                && pf.PupilCode.Contains(pupilCode)
                                select pf.PupilCode).ToList();

            int startNumber = codeConfig.SchoolCodeConfigs.Where(o => o.SchoolID == schoolID).OrderByDescending(o => o.CreatedDate).FirstOrDefault() != null ? codeConfig.SchoolCodeConfigs.Where(o => o.SchoolID == schoolID).OrderByDescending(o => o.CreatedDate).FirstOrDefault().StartNumber : 0;
            int maxOrderNumber = lstPupilCode.Count > 0 ? lstPupilCode.Max(code => { return GetOrderInCode(code, pupilCode.Length); }) : 0;
            if (maxOrderNumber < startNumber)
            {
                maxOrderNumber = startNumber;
                pupilCode += ToStringWithFixedLength(maxOrderNumber + increase, codeConfig.NumberLength);
            }
            else
            {
                pupilCode += ToStringWithFixedLength(maxOrderNumber + 1 + increase, codeConfig.NumberLength);
            }
           
            return pupilCode;
        }

        /// <summary>
        /// Lay ma so tu dong cua giao vien
        /// </summary>
        /// <author>HieuND</author>
        /// <param name="schoolID">Truong hoc</param>
        /// <returns>Ma so tu dong cua giao vien</returns>
        public string CreateTeacherCode(int schoolID,int increase = 0)
        {
            this.SchoolProfileBusiness.CheckAvailable(schoolID, "SchoolProfile_Label_SchoolProfileID");
            var school = this.SchoolProfileBusiness.Find(schoolID);
            CodeConfig codeConfig = SearchBySchool(schoolID, GlobalConstants.CODECONFIG_CODETYPE_TEACHER);
            string teacherCode = school.SchoolCode;
            if (codeConfig.IsStringIdentify) teacherCode += codeConfig.StringIdentify;

            var lstTeacherCode = (from ehs in EmployeeHistoryStatusRepository.All
                                  join e in EmployeeBusiness.All on ehs.EmployeeID equals e.EmployeeID
                                  where ehs.SchoolID == schoolID
                                  && e.IsActive == true
                                  && e.EmployeeCode.Contains(teacherCode)
                                  select e.EmployeeCode).ToList();
            int startNumber = codeConfig.SchoolCodeConfigs.Where(o=>o.SchoolID==schoolID).OrderByDescending(o=>o.CreatedDate).FirstOrDefault()!=null?codeConfig.SchoolCodeConfigs.Where(o=>o.SchoolID==schoolID).OrderByDescending(o=>o.CreatedDate).FirstOrDefault().StartNumber:0;
            int maxOrderNumber = lstTeacherCode.Count > 0 ? lstTeacherCode.Max(code => { return GetOrderInCode(code, teacherCode.Length); }) : 0;
            if(maxOrderNumber < startNumber)
            {
                maxOrderNumber = startNumber;
            }

            teacherCode += ToStringWithFixedLength(maxOrderNumber + 1 + increase, codeConfig.NumberLength);

            return teacherCode;
        }

        public void GetPupilCodeForImport(int AcademicYearID, int educationLevelID, int schoolID, out string OriginalPupilCode, out int maxOrderNumber, out int NumberLength)
        {
            var school = this.SchoolProfileBusiness.Find(schoolID);
            var academicYear = this.AcademicYearBusiness.Find(AcademicYearID);
            var educationLevel = this.EducationLevelBusiness.Find(educationLevelID);
            var codeConfig = SearchBySchool(schoolID, GlobalConstants.CODECONFIG_CODETYPE_PUPIL);

            int minEducationLevel = this.EducationLevelRepository.All.Where(u => u.IsActive && u.Grade == educationLevel.Grade).Min(u => u.EducationLevelID);
            string pupilCode = school.SchoolCode;
            int year = academicYear.Year;
            int? formatNumber = codeConfig.FormatNumber;


            switch (codeConfig.MiddleCodeType)
            {
                case SystemParamsInFile.CODECONFIG_MIDDLECODETYPE_START_COURSE_YEAR:
                    pupilCode += ((formatNumber.HasValue ? formatNumber.Value == 1 ? (year % 100) : year : (year % 100)) - (educationLevelID - minEducationLevel)).ToString().PadLeft(2, '0');
                    break;
                case SystemParamsInFile.CODECONFIG_MIDDLECODETYPE_START_LEVEL_1:
                    pupilCode += ((formatNumber.HasValue ? formatNumber.Value == 1 ? (year % 100) : year : (year % 100)) - (educationLevelID - 1)).ToString().PadLeft(2, '0');
                    break;
                case SystemParamsInFile.CODECONFIG_MIDDLECODETYPE_COURSE:
                    pupilCode += "K" + school.SchoolYearTitle;
                    break;
                default:
                    break;
            }

            if (codeConfig.IsStringIdentify) pupilCode += codeConfig.StringIdentify;

            var lstPupilCode = (from pos in PupilOfSchoolRepository.All
                                join pf in PupilProfileBusiness.All on pos.PupilID equals pf.PupilProfileID
                                where pos.SchoolID == schoolID
                                && pf.PupilCode.Contains(pupilCode)
                                select pf.PupilCode).ToList();

            int maxOrder = lstPupilCode.Count > 0 ? lstPupilCode.Max(code => { return GetOrderInCode(code, pupilCode.Length); }) : 0;

            //gan gia tri
            OriginalPupilCode = pupilCode;
            maxOrderNumber = maxOrder;
            NumberLength = codeConfig.NumberLength;
            //pupilCode += ToStringWithFixedLength(maxOrderNumber + 1 + increase, codeConfig.NumberLength);


        }

        public void ApproveNewPupilCode(int SchoolID,int AcademicYearID)
        {
            try
            {
                SetAutoDetectChangesEnabled(false);
                //lay danh sach hoc sinh toan truong
                List<PupilProfile> lstPPAll = (from pf in PupilProfileBusiness.All
                                               where pf.CurrentSchoolID == SchoolID
                                               && pf.IsActive
                                               select pf).ToList();
                List<PupilProfile> lstPPOfYear = lstPPAll.Where(p => p.CurrentAcademicYearID == AcademicYearID).ToList();
                List<PupilProfile> lstPPtmp = null;
                List<int> lstPupilID = lstPPOfYear.Select(p => p.PupilProfileID).Distinct().ToList();

                List<string> lstPupilCodeOldYear = lstPPAll.Where(p => !lstPupilID.Contains(p.PupilProfileID)).Select(p => p.PupilCode).Distinct().ToList();

                IDictionary<string, object> dicSearch = new Dictionary<string, object>()
                {
                    {"SchoolID",SchoolID},
                    {"AcademicYearID",AcademicYearID},
                    {"lstPupilID",lstPupilID}
                };
                List<PupilOfClass> lstPOC = PupilOfClassBusiness.Search(dicSearch).ToList();
                List<int> lstEducationLevelID = lstPOC.Select(p => p.ClassProfile.EducationLevelID).OrderBy(p => p).Distinct().ToList();

                var school = this.SchoolProfileBusiness.Find(SchoolID);
                var academicYear = this.AcademicYearBusiness.Find(AcademicYearID);
                var codeConfig = SearchBySchool(SchoolID, GlobalConstants.CODECONFIG_CODETYPE_PUPIL);
                var educationLevel = this.EducationLevelBusiness.All;
                EducationLevel objEducationLevel = null;
                int startNumber = codeConfig.SchoolCodeConfigs.Where(o => o.SchoolID == SchoolID).OrderByDescending(o => o.CreatedDate).FirstOrDefault() != null ? codeConfig.SchoolCodeConfigs.Where(o => o.SchoolID == SchoolID).OrderByDescending(o => o.CreatedDate).FirstOrDefault().StartNumber : 0;
                int? formatNumber = codeConfig.FormatNumber;
                int minEducationLevel = 0;//this.EducationLevelRepository.All.Where(u => u.IsActive && u.Grade == educationLevel.Grade).Min(u => u.EducationLevelID);
                string pupilCode = school.SchoolCode;
                int year = academicYear.Year;
                int educationLevelID = 0;
                int counttmp = startNumber;
                string currentPupilCode = string.Empty;
                string pupilCodetmp = string.Empty;

                List<PupilOfClass> lstPOCtmp = new List<PupilOfClass>();
                List<int> lstPupilIDtmp = new List<int>();
                for (int i = 0; i < lstEducationLevelID.Count; i++)
                {
                    educationLevelID = lstEducationLevelID[i];
                    pupilCode = school.SchoolCode;
                    pupilCodetmp = string.Empty;
                    objEducationLevel = educationLevel.Where(p => p.EducationLevelID == educationLevelID).FirstOrDefault();
                    lstPOCtmp = lstPOC.Where(p => p.ClassProfile.EducationLevelID == educationLevelID).ToList();
                    lstPupilIDtmp = lstPOCtmp.Select(p => p.PupilID).Distinct().ToList();
                    lstPPtmp = lstPPOfYear.Where(p => lstPupilIDtmp.Contains(p.PupilProfileID) && p.CurrentAcademicYearID == AcademicYearID).ToList()
                                                .OrderBy(u => SMAS.Business.Common.Utils.SortABC(u.FullName.getOrderingName(u.Ethnic != null ? u.Ethnic.EthnicCode : "0")))
                                                .ToList();
                    minEducationLevel = educationLevel.Where(p => p.IsActive && p.Grade == objEducationLevel.Grade).Min(p => p.EducationLevelID);

                    switch (codeConfig.MiddleCodeType)
                    {
                        case SystemParamsInFile.CODECONFIG_MIDDLECODETYPE_START_COURSE_YEAR:
                            pupilCode += ((formatNumber.HasValue ? (formatNumber.Value == 1 ? (year % 100) : year) : (year % 100)) - (educationLevelID - minEducationLevel)).ToString().PadLeft(2, '0');
                            break;
                        case SystemParamsInFile.CODECONFIG_MIDDLECODETYPE_START_LEVEL_1:
                            pupilCode += ((formatNumber.HasValue ? (formatNumber.Value == 1 ? (year % 100) : year) : (year % 100)) - (educationLevelID - 1)).ToString().PadLeft(2, '0');
                            break;
                        case SystemParamsInFile.CODECONFIG_MIDDLECODETYPE_COURSE:
                            pupilCode += "K" + school.SchoolYearTitle;
                            break;
                        default:
                            break;
                    }

                    if (codeConfig.IsStringIdentify) pupilCode += codeConfig.StringIdentify;
                    for (int j = 0; j < lstPPtmp.Count; j++)
                    {
                        PupilProfile objPP = lstPPtmp[j];
                        currentPupilCode = ToStringWithFixedLength(counttmp, codeConfig.NumberLength);
                        pupilCodetmp = pupilCode + currentPupilCode;

                        while (true)
                        {
                            if (lstPupilCodeOldYear.Exists(p => p == pupilCodetmp))
                            {
                                counttmp++;
                                currentPupilCode = ToStringWithFixedLength(counttmp, codeConfig.NumberLength);
                                pupilCodetmp = pupilCode + currentPupilCode;
                            }
                            else
                            {
                                break;
                            }
                        }
                        objPP.PupilCode = pupilCodetmp;
                        counttmp++;
                        objPP.ModifiedDate = DateTime.Now;
                        PupilProfileBusiness.Update(objPP);
                    }
                }
                PupilProfileBusiness.Save();
            }
            catch (Exception ex)
            {
                logger.Error(ex.Message, ex);
                throw new BusinessException("Có lỗi trong quá trình đánh mã. Thầy/cô vui lòng thử lại");
            }
            finally
            {
                SetAutoDetectChangesEnabled(true);
            }
        }

        public void ApproveNewTeacherCode(int SchoolID)
        {
            try
            {
                SetAutoDetectChangesEnabled(false);
                this.SchoolProfileBusiness.CheckAvailable(SchoolID, "SchoolProfile_Label_SchoolProfileID");
                var school = this.SchoolProfileBusiness.Find(SchoolID);
                //lay danh sach giao vien toan truong
                IDictionary<string, object> dicSearch = new Dictionary<string, object>()
                {
                    {"SchoolID",SchoolID}
                };
                List<Employee> lstEmployee = EmployeeBusiness.Search(dicSearch).ToList().OrderBy(p => p.Name).ThenBy(p => p.FullName).ToList();
                Employee objEmployee = null;
                List<string> lstEmployeeCode = lstEmployee.Select(p => p.EmployeeCode).Distinct().ToList();

                CodeConfig codeConfig = SearchBySchool(SchoolID, GlobalConstants.CODECONFIG_CODETYPE_TEACHER);
                string employeeCode = school.SchoolCode;
                if (codeConfig.IsStringIdentify) employeeCode += codeConfig.StringIdentify;
                string codetmp = employeeCode;
                int startNumber = codeConfig.SchoolCodeConfigs.Where(o => o.SchoolID == SchoolID).OrderByDescending(o => o.CreatedDate).FirstOrDefault() != null ? codeConfig.SchoolCodeConfigs.Where(o => o.SchoolID == SchoolID).OrderByDescending(o => o.CreatedDate).FirstOrDefault().StartNumber : 0;
                string currentEmployeeCode = string.Empty;
                int counttmp = startNumber;
                string employeeCodetmp = string.Empty;
                for (int i = 0; i < lstEmployee.Count; i++)
                {
                    objEmployee = lstEmployee[i];
                    employeeCode = codetmp;
                    employeeCodetmp = string.Empty;

                    currentEmployeeCode = ToStringWithFixedLength(counttmp, codeConfig.NumberLength);
                    employeeCodetmp = employeeCode + currentEmployeeCode;
                    objEmployee.EmployeeCode = employeeCodetmp;
                    counttmp++;
                    objEmployee.ModifiedDate = DateTime.Now;
                    EmployeeBusiness.Update(objEmployee);
                }
                EmployeeBusiness.Save();
            }
            catch (Exception ex)
            {
                logger.Error(ex.Message, ex);
                throw new BusinessException("Có lỗi trong quá trình đánh mã. Thầy/cô vui lòng thử lại");
            }
            finally
            {
                SetAutoDetectChangesEnabled(true);
            }
            
        }
       public void GetTeacherCodeForImport(int schoolID, out string OriginalTeacherCode, out int maxOrderNumber, out int NumberLength)
        {
            this.SchoolProfileBusiness.CheckAvailable(schoolID, "SchoolProfile_Label_SchoolProfileID");
            var school = this.SchoolProfileBusiness.Find(schoolID);
            CodeConfig codeConfig = SearchBySchool(schoolID, GlobalConstants.CODECONFIG_CODETYPE_TEACHER);
            string teacherCode = school.SchoolCode;
            if (codeConfig.IsStringIdentify) teacherCode += codeConfig.StringIdentify;

            var lstTeacherCode = (from ehs in EmployeeHistoryStatusRepository.All
                                  join e in EmployeeBusiness.All on ehs.EmployeeID equals e.EmployeeID
                                  where ehs.SchoolID == schoolID
                                  && e.IsActive == true
                                  && e.EmployeeCode.Contains(teacherCode)
                                  select e.EmployeeCode).ToList();

            int maxOrder = lstTeacherCode.Count > 0 ? lstTeacherCode.Max(code => { return GetOrderInCode(code, teacherCode.Length); }) : 0;

           //set gia tri
            OriginalTeacherCode = teacherCode;
            maxOrderNumber = maxOrder;
            NumberLength = codeConfig.NumberLength;
        }
    }
}