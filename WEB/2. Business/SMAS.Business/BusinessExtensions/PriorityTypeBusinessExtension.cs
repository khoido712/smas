﻿/**
 * The Viettel License
 *
 * Copyright 2012 Viettel Telecom. All rights reserved.
 * VIETTEL PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */

/** 
* @author dungnt 
* @version $Revision: $
*/

using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Linq.Expressions;
using System.Web;
using log4net;

using SMAS.DAL.Repository;
using SMAS.DAL.IRepository;
using SMAS.Business.IBusiness;
using SMAS.Models.Models;
using SMAS.Business.Common;


namespace SMAS.Business.Business
{
    /// <summary>
    /// Diện ưu tiên khuyến khích
    /// <author>dungnt</author>
    /// <date>05/09/2012</date>
    /// </summary>
    public partial class PriorityTypeBusiness
    {


        #region insert
        /// <summary>
        ///Thêm mới Diện ưu tiên khuyến khích
        /// <author>dungnt</author>
        /// <date>05/09/2012</date>
        /// </summary>
        /// <param name="insertPriorityType">Đối tượng Insert</param>
        /// <returns>Đối tượng sau khi được insert vào DB thành công</returns>
        public override PriorityType Insert(PriorityType insertPriorityType)
        {


            //resolution rong
            Utils.ValidateRequire(insertPriorityType.Resolution, "PriorityType_Label_Resolution");


            //kiem tra ton tai - theo cap - can lam lai
            //this.CheckDuplicate(insertPriorityType.Resolution, GlobalConstants.LIST_SCHEMA, "PriorityType", "Resolution", true, 0, "PriorityType_Label_Resolution");
            //this.CheckDuplicate(insertPriorityType.AppliedLevel.ToString(), GlobalConstants.LIST_SCHEMA, "PriorityType", "AppliedLevel", true, 0, "PriorityType_Label_AppliedLevel");
            this.CheckDuplicateCouple(insertPriorityType.Resolution, insertPriorityType.AppliedLevel.ToString(),
               GlobalConstants.LIST_SCHEMA, "PriorityType", "Resolution", "AppliedLevel", true, 0, "PriorityType_Label_Resolution");
            //kiem tra appliedlevel trong 1- 5 

            Utils.ValidateRange((int)insertPriorityType.AppliedLevel, 1, 5, "PriorityType_Label_AppliedLevel");

            //kiem tra coeffien
            Utils.ValidateRange((int)insertPriorityType.Mark, 0, 10, "SalaryLevel_Label_Coefficient");


            insertPriorityType.IsActive = true;

            return base.Insert(insertPriorityType);
        }
        #endregion

        #region update
        /// <summary>
        /// Cập nhật Diện ưu tiên khuyến khích
        /// <author>dungnt</author>
        /// <date>05/09/2012</date>
        /// </summary>
        /// <param name="updatePriorityType">Đối tượng Update</param>
        /// <returns>Đối tượng sau khi được Update vào DB thành công</returns>
        public override PriorityType Update(PriorityType updatePriorityType)
        {
            //check avai
            new PriorityTypeBusiness(null).CheckAvailable(updatePriorityType.PriorityTypeID, "PriorityType_Label_PriorityTypeID");
            //  this.CheckAvailable(updatePriorityType.PriorityTypeID, "PriorityType_Label_PriorityTypeID");

            //resolution rong
            Utils.ValidateRequire(updatePriorityType.Resolution, "PriorityType_Label_Resolution");


            //kiem tra ton tai
            //this.CheckDuplicate(updatePriorityType.Resolution, GlobalConstants.LIST_SCHEMA, "PriorityType", "Resolution", true, 0, "PriorityType_Label_Resolution");
            //this.CheckDuplicate(updatePriorityType.AppliedLevel.ToString(), GlobalConstants.LIST_SCHEMA, "PriorityType", "AppliedLevel", true, 0, "PriorityType_Label_AppliedLevel");


            this.CheckDuplicateCouple(updatePriorityType.Resolution, updatePriorityType.AppliedLevel.ToString(),
                GlobalConstants.LIST_SCHEMA, "PriorityType", "Resolution", "AppliedLevel", true, updatePriorityType.PriorityTypeID, "PriorityType_Label_AppliedLevel");
            //kiem tra appliedlevel trong 1- 5 

            Utils.ValidateRange((int)updatePriorityType.AppliedLevel, 1, 5, "PriorityType_Label_AppliedLevel");

            //kiem tra coeffien
            Utils.ValidateRange((double)updatePriorityType.Mark, 0.0, 10.0, "SalaryLevel_Label_Coefficient");


            updatePriorityType.IsActive = true;

            return base.Update(updatePriorityType);
        }
        #endregion

        #region Delete
        /// <summary>
        ///Xóa Diện ưu tiên khuyến khích
        /// <author>dungnt</author>
        /// <date>05/09/2012</date>
        /// </summary>
        /// <param name="PriorityTypeId">ID Diện ưu tiên khuyến khích</param>
        public void Delete(int PriorityTypeId)
        {

            //check avai
            new PriorityTypeBusiness(null).CheckAvailable(PriorityTypeId, "PriorityType_Label_PriorityTypeID");

            //da su dung hay chua
            this.CheckConstraints(GlobalConstants.LIST_SCHEMA, "PriorityType", PriorityTypeId, "PriorityType_Label_PriorityIDFailed");

            base.Delete(PriorityTypeId, true);

        }
        #endregion

        #region Search
        /// <summary>
        /// Tìm kiếm Diện ưu tiên khuyến khích
        /// <author>dungnt</author>
        /// <date>05/09/2012</date>
        /// </summary>
        /// <param name="dic">tham so doi tuong tim kiem</param>
        /// <returns>Danh sách kết quả tìm kiếm</returns>
        public IQueryable<PriorityType> Search(IDictionary<string, object> dic)
        {

            string resolution = Utils.GetString(dic, "Resolution");
            string description = Utils.GetString(dic, "Description");
            int appliedLevel = Utils.GetInt(dic, "AppliedLevel");
            int type = Utils.GetInt(dic, "Type");
            bool? isActive = Utils.GetIsActive(dic, "IsActive");

            IQueryable<PriorityType> lsPriorityType = this.PriorityTypeRepository.All;

            if (isActive.HasValue)
            {
                lsPriorityType = lsPriorityType.Where(em => (em.IsActive == isActive));
            }

            if (resolution.Trim().Length != 0)
            {
                lsPriorityType = lsPriorityType.Where(em => (em.Resolution.ToUpper().Contains(resolution.ToUpper())));
            }
            if (appliedLevel != 0)
            {
                lsPriorityType = lsPriorityType.Where(em => (em.AppliedLevel == appliedLevel));
            }
            if (type != 0)
            {
                lsPriorityType = lsPriorityType.Where(em => (em.Type == type));
            }
            return lsPriorityType;
        }
        #endregion
    }
}