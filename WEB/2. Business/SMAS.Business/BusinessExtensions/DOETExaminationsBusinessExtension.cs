﻿using SMAS.Business.BusinessObject;
using SMAS.Business.Common;
using SMAS.Models.Models;
using System;
using System.Collections.Generic;
using System.Linq;

namespace SMAS.Business.Business
{
    public partial class DOETExaminationsBusiness
    {
        public IQueryable<DOETExaminations> GetExaminationsOfDept(int supervisingDeptID, int? Year = null)
        {
            IQueryable<DOETExaminations> query = repository.All.Where(o => o.UnitID == supervisingDeptID);
            if (Year != null)
            {
                query = query.Where(o => o.Year == Year.Value);
            }
            return query;
        }

        public IQueryable<DOETExaminations> Search(IDictionary<string, object> search)
        {
            int unitId = Utils.GetInt(search, "UnitID");
            List<int> listUnitId = Utils.GetIntList(search, "LisUnitID");
            int year = Utils.GetInt(search, "Year");
            int examinationId = Utils.GetInt(search, "ExaminationsID");
            int grade = Utils.GetInt(search, "Grade");
            IQueryable<DOETExaminations> query = DOETExaminationsBusiness.All;
            if (listUnitId != null && listUnitId.Count > 0)
            {
                query = query.Where(o => listUnitId.Contains(o.UnitID));
            }
            if (unitId > 0)
            {
                query = query.Where(o => o.UnitID == unitId);
            }
            if (year > 0)
            {
                query = query.Where(o => o.Year == year);
            }
            if (examinationId > 0)
            {
                query = query.Where(o => o.ExaminationsID == examinationId);
            }
            return query;
        }



        public void SyncExamHCM(AcademicYear academicYear, SchoolProfile school, int appliedLevel)
        {
            int year = academicYear.Year;
            string schoolCode = school.SchoolCode; //"79767504";          
            var listExamHcm = DoetHcmApi.GetKiThiTheoTruong(year, schoolCode, appliedLevel);

            if (listExamHcm == null || listExamHcm.Count == 0)
            {
                return;
            }

            int supervisingDeptId = school.SupervisingDeptID.Value;
            SupervisingDept supervisingDept = SupervisingDeptBusiness.Find(supervisingDeptId);
            try
            {

                supervisingDept.MSourcedb = "1";
                SupervisingDeptBusiness.Update(supervisingDept);
                SupervisingDeptBusiness.Save();
                List<SupervisingDept> lstSuper = SupervisingDeptBusiness.All.Where(s => s.ProvinceID == school.ProvinceID && s.IsActive == true).ToList();

                List<DOETExaminations> lstSourceExam = new List<DOETExaminations>();

                List<DOETSubjectBO> lstDoetSubject = GetDoetSubject(appliedLevel);
                List<DOETSubject> lstDOETSubjectInsert = new List<DOETSubject>();
                for (int i = 0; i < listExamHcm.Count; i++)
                {
                    KyThi objKythi = listExamHcm[i];

                    MapSource<int> objSource = new MapSource<int>
                    {
                        SourceName = GlobalConstants.Province_CODE_HCM,
                        SourceId = objKythi.KiThiID
                    };

                    MapSource<int> mapsourceUnit = new MapSource<int>
                    {
                        SourceName = GlobalConstants.Province_CODE_HCM,
                        SourceId = objKythi.ManagingUnitID
                    };


                    string jsonUnit = Newtonsoft.Json.JsonConvert.SerializeObject(mapsourceUnit);
                    SupervisingDept supervising = lstSuper.FirstOrDefault(s => s.SyncSourceId == jsonUnit);

                    if (!lstSuper.Exists(s => s.SyncSourceId == jsonUnit))
                    {
                        logger.Info(string.Format("Khong tim thay thong tin don vi o du lieu SMAS theo ID so {0}",objKythi.ManagingUnitID));
                        continue;
                    }

                    DOETExaminations objExamination = new DOETExaminations
                    {
                        AppliedType = 0,
                        CreateTime = DateTime.Now,
                        DeadLine = objKythi.HanChotDangKy,
                        ExaminationsName = objKythi.TenKiThi,
                        MSourcedb = GlobalConstants.Province_CODE_HCM,
                        SyncTime = DateTime.Now,
                        UnitID = supervising.SupervisingDeptID,
                        ExamCouncil = "",
                        FromDate = null,
                        Note = "",
                        SyncSourceId = Newtonsoft.Json.JsonConvert.SerializeObject(objSource),
                        ToDate = null,
                        UpdateTime = null,
                        Year = year
                    };

                    lstSourceExam.Add(objExamination);

                    //Dong bo danh muc mon thi

                    List<MonThi> lstMonThi = DoetHcmApi.GetMonThi(objKythi.KiThiID);
                    if (lstMonThi == null)
                    {
                        continue;
                    }

                    for (int m = 0; m < lstMonThi.Count; m++)
                    {
                        MonThi objSourceMonthi = lstMonThi[m];

                        MapSource<int> objSourceSubject = new MapSource<int>
                        {
                            SourceName = GlobalConstants.Province_CODE_HCM,
                            SourceId = objSourceMonthi.MonThiID
                        };

                        DOETSubjectBO objDesExamSubject = lstDoetSubject.FirstOrDefault(s => s.ListMapSource.SourceId == objSourceMonthi.MonThiID);
                        if (objDesExamSubject == null)
                        {
                            string jsonSubjectId = Newtonsoft.Json.JsonConvert.SerializeObject(objSourceSubject);
                            DOETSubject dOETSubject = new DOETSubject
                            {
                                AppliedLevel = objKythi.KiThiCap.ToString(),
                                IsActive = true,
                                DOETSubjectCode = objSourceMonthi.TienTo,
                                DOETSubjectName = objSourceMonthi.TenMonThi,
                                SyncTime = DateTime.Now,
                                SyncSourceId = jsonSubjectId,
                            };

                            if (!lstDOETSubjectInsert.Any(s => s.DOETSubjectName == objSourceMonthi.TenMonThi && s.DOETSubjectCode == objSourceMonthi.TienTo))
                            {
                                lstDOETSubjectInsert.Add(dOETSubject);
                            }

                        }

                    }
                }

                List<DOETExaminationBO> lstDestExam = GetDoetExam(year, school.ProvinceID.Value);
                List<DOETExaminations> lstExamInsert = new List<DOETExaminations>();
                List<DOETExaminations> lstExamUpdate = new List<DOETExaminations>();
                for (int i = 0; i < lstSourceExam.Count; i++)
                {
                    DOETExaminations objSourceExam = lstSourceExam[i];
                    MapSource<int> mapSourceExam = Newtonsoft.Json.JsonConvert.DeserializeObject<MapSource<int>>(objSourceExam.SyncSourceId);
                    DOETExaminationBO objDesExam = lstDestExam.FirstOrDefault(s => s.ListMapSource.SourceId == mapSourceExam.SourceId);
                    if (objDesExam == null)
                    {
                        lstExamInsert.Add(objSourceExam);
                    }
                    /*else
                    {
                        objSourceExam.ExaminationsID = objDesExam.ExaminationsID;
                        objSourceExam.CreateTime = objDesExam.CreateTime;
                        objSourceExam.UpdateTime = DateTime.Now;
                        lstExamUpdate.Add(objSourceExam);
                    }*/
                }
                if (lstExamInsert.Count > 0)
                {

                    DOETExaminationsBusiness.BulkInsert(lstExamInsert, ColumnMapping.Instance.DOETExaminations(), "EXAMINATIONS_ID");
                }


                if (lstDOETSubjectInsert.Count > 0)
                {
                    DOETSubjectBusiness.BulkInsert(lstDOETSubjectInsert, ColumnMapping.Instance.DOETSubject(), "DOET_SUBJECT_ID");

                }

                /*if (lstExamUpdate.Count > 0)
                {
                    foreach (DOETExaminations item in lstExamUpdate)
                    {
                        DOETExaminationsBusiness.Update(item);

                    }
                    DOETExaminationsBusiness.SaveDetect();
                }*/
                SyncExamGroupHCM(academicYear, school, appliedLevel);
                SyncExamSubjectHCM(academicYear, school, appliedLevel);
            }
            finally
            {
                supervisingDept.MSourcedb = "0";
                SupervisingDeptBusiness.Update(supervisingDept);
                SupervisingDeptBusiness.Save();
            }
        }


        public void SyncPupilSMAStoHCM(AcademicYear academicYear, SchoolProfile school, int appliedLevel, int examinationId, int examGroupId)
        {
            DOETExaminations examinations = DOETExaminationsBusiness.Find(examinationId);
            if (examinations == null)
            {
                return;
            }
            MapSource<int> sourceExam = Newtonsoft.Json.JsonConvert.DeserializeObject<MapSource<int>>(examinations.SyncSourceId);
            if (sourceExam == null)
            {
                return;
            }

            DOETExamGroup dOETExamGroup = DOETExamGroupBusiness.Find(examGroupId);
            if (dOETExamGroup == null)
            {
                return;
            }


            List<DOETSubject> lstSubjcet = GetDoetSuject(examinationId, examGroupId, 0);
            if (lstSubjcet == null)
            {
                return;
            }


            foreach (DOETSubject subject in lstSubjcet)
            {
                MapSource<int> sourceSubject = Newtonsoft.Json.JsonConvert.DeserializeObject<MapSource<int>>(subject.SyncSourceId);
                if (sourceSubject == null)
                {
                    return;
                }

                List<DOETExamPupilBO> lstExamPupil = DOETExamPupilBusiness.GetSyncExamPupilHCM(academicYear, school, appliedLevel, examinationId, examGroupId, subject.DOETSubjectID, true);
                if (lstExamPupil == null || lstExamPupil.Count == 0)
                {
                    return;
                }
                List<ThiSinhSMAS> lstSyncThiSinh = new List<ThiSinhSMAS>();
                foreach (DOETExamPupilBO ex in lstExamPupil)
                {
                    MapSource<int> mapSourceEthnic = Newtonsoft.Json.JsonConvert.DeserializeObject<MapSource<int>>(ex.SyncSourceEthnicId);
                    ThiSinhSMAS thiSinhSMAS = new ThiSinhSMAS
                    {
                        MaThiSinh = "0",
                        HocSinhLopID = ex.ExamPupilID,
                        NgaySinh = ex.BirthDay.ToString("dd/MM/yyyy"),
                        TenDuAn = "Dự án 1",
                        Khoi = ex.EducationLevelID,
                        NoiSinh = string.IsNullOrEmpty(ex.BirthPlace) ? school.Province.ProvinceName : ex.BirthPlace,
                        Phai = ex.Genre == 1 ? false : true,
                        TenLop = ex.ClassName,
                        DanTocID = (mapSourceEthnic == null) ? 1 : mapSourceEthnic.SourceId,
                        Ho = (ex.FullName.Replace(ex.Name, "")).Trim(),
                        Ten = ex.Name
                    };
                    if (!lstSyncThiSinh.Exists(s => s.HocSinhLopID == thiSinhSMAS.HocSinhLopID))
                    {
                        lstSyncThiSinh.Add(thiSinhSMAS);
                    }
                }

                if (lstSyncThiSinh.Count == 0)
                {
                    continue;
                }

                string jsonExamPupil = Newtonsoft.Json.JsonConvert.SerializeObject(lstSyncThiSinh);
                if (string.IsNullOrEmpty(jsonExamPupil))
                {
                    continue;
                }

                var retVal = DoetHcmApi.NopThiSinh(sourceExam.SourceId, school.SchoolCode, sourceSubject.SourceId, jsonExamPupil);
                if (string.IsNullOrEmpty(retVal))
                {
                    continue;
                }

                if (retVal.Contains("ExceptionMessage") || retVal.Contains("Message"))
                {
                    ResponseExceptionMessage response = Newtonsoft.Json.JsonConvert.DeserializeObject<ResponseExceptionMessage>(retVal);
                    if (response != null)
                    {
                        logger.Info(response.ExceptionMessage);
                    }
                }
                else
                {
                    RootThiSinh retThiSinh = Newtonsoft.Json.JsonConvert.DeserializeObject<RootThiSinh>(retVal);

                    List<DanhSachThiSinh> lstThiSinh = retThiSinh.Table.ToList();
                    if (lstThiSinh == null || lstThiSinh.Count == 0)
                    {
                        return;
                    }
                    SyncPupilHCMtoSMAS(academicYear, school, appliedLevel, examinationId, examGroupId, subject.DOETSubjectID, lstThiSinh);
                }



            }




        }
        public void SyncPupilHCMtoSMAS(AcademicYear academicYear, SchoolProfile school, int appliedLevel, int examinationId, int examGroupId, int subjectId, List<DanhSachThiSinh> lstRetThiSinh)
        {
            DOETExaminations examinations = DOETExaminationsBusiness.Find(examinationId);
            if (examinations == null)
            {
                return;
            }
            MapSource<int> sourceExam = Newtonsoft.Json.JsonConvert.DeserializeObject<MapSource<int>>(examinations.SyncSourceId);
            if (sourceExam == null)
            {
                return;
            }

            DOETExamGroup dOETExamGroup = DOETExamGroupBusiness.Find(examGroupId);
            if (dOETExamGroup == null)
            {
                return;
            }

            List<DOETSubject> listSubject = GetDoetSuject(examinationId, examGroupId, subjectId);
            if (listSubject == null || listSubject.Count == 0)
            {
                return;
            }

            List<DOETExamPupil> lstExamPupilUpdate = new List<DOETExamPupil>();
            List<DOETExamPupil> lstExamPupil = DOETExamPupilBusiness.All.Where(s => s.ExaminationsID == examinationId
                                                                                && s.ExamGroupID == examGroupId
                                                                                && s.SchoolID == school.SchoolProfileID
                                                                                && s.Year == academicYear.Year
                                                                                ).ToList();

            foreach (DOETSubject doetSubject in listSubject)
            {
                MapSource<int> sourceSubject = Newtonsoft.Json.JsonConvert.DeserializeObject<MapSource<int>>(doetSubject.SyncSourceId);
                if (sourceSubject == null)
                {
                    continue;
                }
                List<DanhSachThiSinh> lstthiSinh = new List<DanhSachThiSinh>();
                if (lstRetThiSinh != null && lstRetThiSinh.Count > 0)
                {
                    lstthiSinh = lstRetThiSinh.Where(s => s.MonThiID == sourceSubject.SourceId && s.KiThiID == sourceExam.SourceId).ToList();
                }
                else
                {
                    lstthiSinh = DoetHcmApi.GetThiSinh(sourceExam.SourceId, school.SchoolCode, sourceSubject.SourceId);
                }

                if (lstthiSinh == null)
                {
                    continue;
                }
                if (lstthiSinh.Count == 0)
                {
                    continue;
                }
                foreach (DanhSachThiSinh dsThiSinh in lstthiSinh)
                {
                    DOETExamPupil dOETExamPupil = lstExamPupil.FirstOrDefault(s => s.ExamPupilID == dsThiSinh.HocSinhLopID);
                    if (dOETExamPupil == null)
                    {
                        continue;
                    }
                    if (string.IsNullOrEmpty(dsThiSinh.SBD) && !string.IsNullOrEmpty(doetSubject.SyncSourceId))
                    {
                        continue;
                    }
                    dOETExamPupil.ExamineeNumber = dsThiSinh.SBD;
                    dOETExamPupil.SyncTime = DateTime.Now;
                    MapSource<string> mapSourcePupil = new MapSource<string>
                    {
                        SourceName = GlobalConstants.Province_CODE_HCM,
                        SourceId = dsThiSinh.MaThiSinh
                    };
                    dOETExamPupil.SyncSourceId = Newtonsoft.Json.JsonConvert.SerializeObject(mapSourcePupil);

                    if (lstExamPupilUpdate.Exists(s => s.ExamPupilID == dOETExamPupil.ExamPupilID))
                    {
                        continue;
                    }

                    lstExamPupilUpdate.Add(dOETExamPupil);

                }

            }

            if (lstExamPupilUpdate.Count > 0)
            {
                for (int i = 0; i < lstExamPupilUpdate.Count; i++)
                {
                    DOETExamPupilBusiness.Update(lstExamPupilUpdate[i]);
                }
                DOETExamPupilBusiness.Save();
            }


        }


        public void DelteExamPupilHCM(AcademicYear academicYear, SchoolProfile school, List<DOETExamPupil> lstExamPupil)
        {
            //List<DOETExamPupil> lstExamPupil = DOETExamPupilBusiness.All.Where(s=>s.SchoolID==school.SchoolProfileID && lstExamPupilId.Contains(s.ExamPupilID)).ToList();
            foreach (DOETExamPupil examPupil in lstExamPupil)
            {
                if (string.IsNullOrEmpty(examPupil.SyncSourceId))
                {
                    continue;
                }
                MapSource<int> mapSource = Newtonsoft.Json.JsonConvert.DeserializeObject<MapSource<int>>(examPupil.SyncSourceId);
                if (mapSource == null || mapSource.SourceId == 0)
                {
                    continue;
                }
                DoetHcmApi.XoaThiSinh(new List<string> { mapSource.SourceId.ToString() });
            }
        }


        private void SyncExamGroupHCM(AcademicYear academicYear, SchoolProfile school, int appliedLevel)
        {


            int year = academicYear.Year;
            string schoolCode = school.SchoolCode;
            var listExamHcm = DoetHcmApi.GetKiThiTheoTruong(year, schoolCode, appliedLevel);

            if (listExamHcm == null || listExamHcm.Count == 0)
            {
                return;
            }

            List<DOETExaminationBO> lstDestExam = GetDoetExam(year, school.ProvinceID.Value);

            List<int> lstdesExamId = lstDestExam.Select(s => s.ExaminationsID).ToList();

            List<DOETExamGroupBO> lstdestExamGroup = GetDoetExamGroup(year, school.ProvinceID.Value, appliedLevel);
            List<DOETSubjectBO> lstExamSubject = GetDoetSubject(appliedLevel);

            List<DOETExamGroup> lstDoetExamGroupInsert = new List<DOETExamGroup>();
            foreach (KyThi kythi in listExamHcm)
            {
                DOETExaminationBO objDesExam = lstDestExam.FirstOrDefault(s => s.ListMapSource.SourceId == kythi.KiThiID);
                if (objDesExam == null)
                {
                    continue;
                }
                List<MonThi> lstMonThi = DoetHcmApi.GetMonThi(kythi.KiThiID);
                if (lstMonThi == null)
                {
                    continue;
                }
                for (int m = 0; m < lstMonThi.Count; m++)
                {
                    MonThi objSourceMonthi = lstMonThi[m];

                    DOETSubjectBO objDesDoetSubject = lstExamSubject.FirstOrDefault(s => s.ListMapSource.SourceId == objSourceMonthi.MonThiID);
                    if (objDesDoetSubject == null)
                    {
                        continue;
                    }
                    DOETExamGroupBO objExamGroupExists = lstdestExamGroup.FirstOrDefault(s => s.ExaminationsID == objDesExam.ExaminationsID
                                                                                          && s.ListMapSource.SourceId == objDesDoetSubject.ListMapSource.SourceId
                                                                                          && s.AppliedLevel == appliedLevel
                                                                                         );
                    if (objExamGroupExists != null)
                    {
                        continue;
                    }
                    DOETExamGroup examGroup = new DOETExamGroup
                    {
                        AppliedLevel = kythi.KiThiCap,
                        CreateTime = DateTime.Now,
                        ExamGroupCode = objSourceMonthi.TienTo,
                        ExamGroupName = objSourceMonthi.TenMonThi,
                        SyncSourceId = objDesDoetSubject.SyncSourceId,//Id mon hoc o du lieu dich
                        SyncTime = DateTime.Now,
                        ExaminationsID = objDesExam.ExaminationsID,
                    };

                    if (!lstDoetExamGroupInsert.Any(s => s.ExamGroupName == examGroup.ExamGroupName && s.ExaminationsID == examGroup.ExaminationsID))
                    {
                        lstDoetExamGroupInsert.Add(examGroup);
                    }

                }

            }

            if (lstDoetExamGroupInsert.Count > 0)
            {
                DOETExamGroupBusiness.BulkInsert(lstDoetExamGroupInsert, ColumnMapping.Instance.DOETExamGroup(), "EXAM_GROUP_ID");

            }


        }

        private void SyncExamSubjectHCM(AcademicYear academicYear, SchoolProfile school, int appliedLevel)
        {
            int year = academicYear.Year;
            string schoolCode = school.SchoolCode;
            var listExamHcm = DoetHcmApi.GetKiThiTheoTruong(year, schoolCode, appliedLevel);

            if (listExamHcm == null || listExamHcm.Count == 0)
            {
                return;
            }

            List<DOETExaminationBO> lstDestExam = GetDoetExam(year, school.ProvinceID.Value);

            List<int> lstdesExamId = lstDestExam.Select(s => s.ExaminationsID).Distinct().ToList();

            List<DOETExamGroupBO> lstdestExamGroup = GetDoetExamGroup(year, school.ProvinceID.Value, appliedLevel);

            List<int> lstExamGroupId = lstdestExamGroup.Select(s => s.ExamGroupID).Distinct().ToList();
            List<DOETSubjectBO> lstDoetSubject = GetDoetSubject(appliedLevel);
            List<DOETExamSubject> lstExamSubject = GetDoetExamSubject(lstExamGroupId);
            List<DOETExamSubject> lstDoetExamSubjectInsert = new List<DOETExamSubject>();
            foreach (KyThi kythi in listExamHcm)
            {
                DOETExaminationBO objDesExam = lstDestExam.FirstOrDefault(s => s.ListMapSource.SourceId == kythi.KiThiID);
                if (objDesExam == null)
                {
                    continue;
                }
                List<MonThi> lstMonThi = DoetHcmApi.GetMonThi(kythi.KiThiID);
                if (lstMonThi == null)
                {
                    continue;
                }
                for (int m = 0; m < lstMonThi.Count; m++)
                {
                    MonThi objSourceMonthi = lstMonThi[m];

                    DOETSubjectBO objDesDoetSubject = lstDoetSubject.FirstOrDefault(s => s.ListMapSource.SourceId == objSourceMonthi.MonThiID);
                    if (objDesDoetSubject == null)
                    {
                        continue;
                    }
                    DOETExamGroupBO objExamGroupExists = lstdestExamGroup.FirstOrDefault(s => s.ExaminationsID == objDesExam.ExaminationsID
                                                                                          && s.ListMapSource.SourceId == objDesDoetSubject.ListMapSource.SourceId
                                                                                          && s.AppliedLevel == appliedLevel
                                                                                         );

                    if (objExamGroupExists == null)
                    {
                        continue;
                    }

                    DOETExamSubject examSubjectExists = lstExamSubject.FirstOrDefault(s => s.ExamGroupID == objDesDoetSubject.DOETSubjectID
                                                                                        && s.ExamGroupID == objExamGroupExists.ExamGroupID
                                                                                        && s.ExaminationsID == objDesExam.ExaminationsID);
                    if (examSubjectExists != null)
                    {
                        continue;
                    }

                    DOETExamSubject examSubject = new DOETExamSubject
                    {
                        DOETSubjectCode = objSourceMonthi.TienTo,
                        DOETSubjectID = objDesDoetSubject.DOETSubjectID,
                        ExamGroupID = objExamGroupExists.ExamGroupID,
                        SchedulesExam = objSourceMonthi.MoTa,
                        CreateTime = DateTime.Now,
                        SyncSourceId = objDesDoetSubject.SyncSourceId,//Id mon hoc o du lieu dich
                        SyncTime = DateTime.Now,
                        ExaminationsID = objDesExam.ExaminationsID,
                    };


                    if (!lstDoetExamSubjectInsert.Exists(ex => ex.DOETSubjectCode == examSubject.DOETSubjectCode && ex.ExamGroupID == examSubject.ExamGroupID))
                    {
                        lstDoetExamSubjectInsert.Add(examSubject);
                    }

                }

            }

            if (lstDoetExamSubjectInsert.Count > 0)
            {
                DOETExamSubjectBusiness.BulkInsert(lstDoetExamSubjectInsert, ColumnMapping.Instance.DOETExamSubject(), "EXAM_SUBJECT_ID");
            }
        }


        private List<DOETExaminationBO> GetDoetExam(int year, int provinceId)
        {
            List<SupervisingDept> lstSuper = SupervisingDeptBusiness.All.Where(s => s.ProvinceID == provinceId && s.IsActive == true && (s.HierachyLevel == 3 || s.HierachyLevel == 5)).ToList();
            List<int> lstUnitId = lstSuper.Select(s => s.SupervisingDeptID).ToList();

            IQueryable<DOETExaminations> queryable = DOETExaminationsBusiness.All.Where(s => s.Year == year);
            if (lstUnitId.Count > 0)
            {
                queryable = queryable.Where(s => lstUnitId.Contains(s.UnitID));
            }

            List<DOETExaminations> listExam = queryable.ToList();
            if (listExam == null || lstUnitId.Count == 0)
            {
                return new List<DOETExaminationBO>();
            }

            List<DOETExaminationBO> lstRetVal = new List<DOETExaminationBO>();
            foreach (DOETExaminations exam in listExam)
            {
                DOETExaminationBO dOETExaminationBO = new DOETExaminationBO
                {
                    AppliedType = exam.AppliedType,
                    CreateTime = exam.CreateTime,
                    DeadLine = exam.CreateTime,
                    ExamCouncil = exam.ExamCouncil,
                    ExaminationsID = exam.ExaminationsID,
                    ExaminationsName = exam.ExaminationsName,
                    FromDate = exam.FromDate,
                    MSourcedb = exam.MSourcedb,
                    Note = exam.Note,
                    SyncSourceId = exam.SyncSourceId,
                    SyncTime = exam.SyncTime,
                    ToDate = exam.ToDate,
                    UnitID = exam.UnitID,
                    UpdateTime = exam.UpdateTime,
                    Year = exam.Year
                };

                MapSource<int> listMapSource = new MapSource<int>();
                if (!string.IsNullOrEmpty(dOETExaminationBO.SyncSourceId))
                {
                    listMapSource = Newtonsoft.Json.JsonConvert.DeserializeObject<MapSource<int>>(dOETExaminationBO.SyncSourceId);
                }

                dOETExaminationBO.ListMapSource = listMapSource;
                lstRetVal.Add(dOETExaminationBO);
            }

            return lstRetVal;

        }

        private List<DOETExamGroupBO> GetDoetExamGroup(int year, int provinceId, int appliedLevel)
        {
            List<SupervisingDept> lstSuper = SupervisingDeptBusiness.All.Where(s => s.ProvinceID == provinceId && s.IsActive == true && (s.HierachyLevel == 3 || s.HierachyLevel == 5)).ToList();
            List<int> lstUnitId = lstSuper.Select(s => s.SupervisingDeptID).ToList();

            IQueryable<DOETExaminations> queryable = DOETExaminationsBusiness.All.Where(s => s.Year == year);
            if (lstUnitId.Count > 0)
            {
                queryable = queryable.Where(s => lstUnitId.Contains(s.UnitID));
            }

            IQueryable<DOETExamGroup> iExamGroup = DOETExamGroupBusiness.All.Where(s => s.AppliedLevel == appliedLevel);

            IQueryable<DOETExamGroup> iq = from eg in iExamGroup
                                           join ex in queryable on eg.ExaminationsID equals ex.ExaminationsID
                                           select eg;
            List<DOETExamGroup> lstExamGroup = iq.ToList();
            List<DOETExamGroupBO> lstRetval = new List<DOETExamGroupBO>();
            foreach (DOETExamGroup item in lstExamGroup)
            {
                DOETExamGroupBO examGroup = new DOETExamGroupBO
                {
                    AppliedLevel = item.AppliedLevel,
                    CreateTime = item.CreateTime,
                    ExamGroupCode = item.ExamGroupCode,
                    ExamGroupID = item.ExamGroupID,
                    ExamGroupName = item.ExamGroupName,
                    ExaminationsID = item.ExaminationsID,
                    MSourcedb = item.MSourcedb,
                    SyncSourceId = item.SyncSourceId,
                    SyncTime = item.SyncTime,
                    UpdateTime = item.UpdateTime,
                };
                MapSource<int> listMapSource = new MapSource<int>();
                if (!string.IsNullOrEmpty(examGroup.SyncSourceId))
                {
                    listMapSource = Newtonsoft.Json.JsonConvert.DeserializeObject<MapSource<int>>(examGroup.SyncSourceId);
                }


                examGroup.ListMapSource = listMapSource;
                lstRetval.Add(examGroup);
            }

            return lstRetval;
        }

        private List<DOETExamSubject> GetDoetExamSubject(List<int> lstExamGroupId)
        {
            var lstSubject = DOETExamSubjectBusiness.All.Where(s => lstExamGroupId.Contains(s.ExamGroupID)).ToList();

            List<DOETSubjectBO> listRetval = new List<DOETSubjectBO>();
            return lstSubject;
        }
        private List<DOETSubjectBO> GetDoetSubject(int appliedLevel)
        {
            List<DOETSubject> lstExamSubject = DOETSubjectBusiness.All.Where(s => s.IsActive == true).ToList();
            if (lstExamSubject == null || lstExamSubject.Count == 0)
            {
                return new List<DOETSubjectBO>();
            }
            List<DOETSubjectBO> listRetval = new List<DOETSubjectBO>();
            foreach (DOETSubject doetSubject in lstExamSubject)
            {
                DOETSubjectBO dOETSubjectBO = new DOETSubjectBO
                {
                    AppliedLevel = doetSubject.AppliedLevel,
                    IsActive = doetSubject.IsActive,
                    DOETSubjectCode = doetSubject.DOETSubjectCode,
                    DOETSubjectID = doetSubject.DOETSubjectID,
                    DOETSubjectName = doetSubject.DOETSubjectName,
                    MSourcedb = doetSubject.MSourcedb,
                    SyncSourceId = doetSubject.SyncSourceId,
                    SyncTime = doetSubject.SyncTime
                };

                MapSource<int> listMapSource = new MapSource<int>();
                if (!string.IsNullOrEmpty(doetSubject.SyncSourceId))
                {
                    listMapSource = Newtonsoft.Json.JsonConvert.DeserializeObject<MapSource<int>>(doetSubject.SyncSourceId);
                }

                dOETSubjectBO.ListMapSource = listMapSource;
                listRetval.Add(dOETSubjectBO);

            }
            return listRetval;
        }


        /// <summary>
        /// Lay danh sach mon thi theo ky thi
        /// </summary>
        /// <param name="examinationId"></param>
        /// <param name="examGroupId"></param>
        /// <param name="examSubjectId"></param>
        /// <returns></returns>
        private List<DOETSubject> GetDoetSuject(int examinationId, int examGroupId, int doetSubjectId)
        {
            IQueryable<DOETExamSubject> queryExamSubject = DOETExamSubjectBusiness.All;
            if (doetSubjectId > 0)
            {
                queryExamSubject = queryExamSubject.Where(e => e.DOETSubjectID == doetSubjectId);
            }
            if (examGroupId > 0)
            {
                queryExamSubject = queryExamSubject.Where(e => e.ExamGroupID == examGroupId);
            }

            if (examinationId > 0)
            {
                queryExamSubject = queryExamSubject.Where(e => e.ExaminationsID == examinationId);
            }

            IQueryable<DOETSubject> querySubject = from exsubject in queryExamSubject
                                                   join s in DOETSubjectBusiness.All.Where(s => s.IsActive == true) on exsubject.DOETSubjectID equals s.DOETSubjectID
                                                   select s;
            List<DOETSubject> listSubject = querySubject.ToList();
            return listSubject;
        }


    }
}
