﻿/**
 * The Viettel License
 *
 * Copyright 2012 Viettel Telecom. All rights reserved.
 * VIETTEL PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */

/** 
* @author  dungnt
* @version $Revision: $
*/

using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Linq.Expressions;
using System.Web;
using log4net;

using SMAS.DAL.Repository;
using SMAS.DAL.IRepository;
using SMAS.Business.IBusiness;
using SMAS.Models.Models;
using SMAS.Business.Common;

namespace SMAS.Business.Business
{
    /// <summary>
    /// Chuyên ngành đào tạo
    /// <author>dungnt</author>
    /// <date>05/09/2012</date>
    /// </summary>
    public partial class SpecialityCatBusiness
    {

        #region private member variable
        private const int RESOLUTION_MAX_LENGTH = 50;   //do dai lon nhat truong ten
        private const int DESCRIPTION_MAX_LENGTH = 400; //do dai lon nhat truong mieu ta
        #endregion

        #region insert
        /// <summary>
        /// Thêm mới Chuyên ngành đào tạo
        /// <author>dungnt</author>
        /// <date>05/09/2012</date>
        /// </summary>
        /// <param name="insertSpecialityCat">doi tuong can them</param>
        /// <returns>Đối tượng sau khi được insert vào DB thành công</returns>
        public override SpecialityCat Insert(SpecialityCat insertSpecialityCat)
        {


            //resolution rong
            Utils.ValidateRequire(insertSpecialityCat.Resolution, "SpecialityCat_Label_Resolution");

            Utils.ValidateMaxLength(insertSpecialityCat.Resolution, RESOLUTION_MAX_LENGTH, "SpecialityCat_Label_Resolution");
            Utils.ValidateMaxLength(insertSpecialityCat.Description, DESCRIPTION_MAX_LENGTH, "SpecialityCat_Column_Description");

            //kiem tra ton tai - theo cap 
            this.CheckDuplicate(insertSpecialityCat.Resolution, GlobalConstants.LIST_SCHEMA, "SpecialityCat", "Resolution", true, 0, "SpecialityCat_Label_Resolution");
            //this.CheckDuplicate(insertSpecialityCat.SchoolID.ToString(), GlobalConstants.LIST_SCHEMA, "SpecialityCat", "AppliedLevel", true, 0, "SpecialityCat_Label_ShoolID");
            // this.CheckDuplicateCouple(insertSpecialityCat.Resolution, insertSpecialityCat.SchoolID.ToString(),
            //GlobalConstants.LIST_SCHEMA, "SpecialityCat", "Resolution", "SchoolID", true, 0, "SpecialityCat_Label_Resolution");
            //kiem tra range

            insertSpecialityCat.IsActive = true;

           return base.Insert(insertSpecialityCat);
        }
        #endregion

        #region update
        /// <summary>
        /// Cập nhật Chuyên ngành đào tạo
        /// <author>dungnt</author>
        /// <date>05/09/2012</date>
        /// </summary>
        /// <param name="updateSpecialityCat">doi tuong sua</param>
        /// <returns>Đối tượng sau khi được Update vào DB thành công</returns>
        public override SpecialityCat Update(SpecialityCat updateSpecialityCat)
        {

            //check avai
            new SpecialityCatBusiness(null).CheckAvailable(updateSpecialityCat.SpecialityCatID, "SpecialityCat_Label_SpecialityCatID");

            //resolution rong
            Utils.ValidateRequire(updateSpecialityCat.Resolution, "SpecialityCat_Label_Resolution");

            Utils.ValidateMaxLength(updateSpecialityCat.Resolution, RESOLUTION_MAX_LENGTH, "SpecialityCat_Label_Resolution");
            Utils.ValidateMaxLength(updateSpecialityCat.Description, DESCRIPTION_MAX_LENGTH, "SpecialityCat_Column_Description");

            //kiem tra ton tai - theo cap - can lam lai
            this.CheckDuplicate(updateSpecialityCat.Resolution, GlobalConstants.LIST_SCHEMA, "SpecialityCat", "Resolution", true, updateSpecialityCat.SpecialityCatID, "SpecialityCat_Label_Resolution");

            //this.CheckDuplicateCouple(updateSpecialityCat.Resolution, updateSpecialityCat.SchoolID.ToString(),
            //    GlobalConstants.LIST_SCHEMA, "SpecialityCat", "Resolution", "SchoolID", true, updateSpecialityCat.SpecialityCatID, "SpecialityCat_Label_Resolution");

            // this.CheckDuplicate(updateSpecialityCat.SchoolID.ToString(), GlobalConstants.LIST_SCHEMA, "SpecialityCat", "AppliedLevel", true, 0, "SpecialityCat_Label_ShoolID");


            //Loi pham quy che thi khong thuoc truong truyen vao
            //tu SpecialityCatID ->vao csdl lay SpecialityCat tuong ung roi 
            //so sanh voi updateSpecialityCat.SchoolID


            //.................



            updateSpecialityCat.IsActive = true;

           return base.Update(updateSpecialityCat);
        }
        #endregion

        #region Delete
        /// <summary>
        /// Xóa Chuyên ngành đào tạo
        /// <author>dungnt</author>
        /// <date>05/09/2012</date>
        /// </summary>
        /// <param name="SpecialityCatId">ID Chuyên ngành đào tạo</param>
        public  void Delete(int SpecialityCatId)
        {
            //check avai
            new SpecialityCatBusiness(null).CheckAvailable(SpecialityCatId, "SpecialityCat_Label_SpecialityCatID");

            //da su dung hay chua
            this.CheckConstraints(GlobalConstants.LIST_SCHEMA, "SpecialityCat", SpecialityCatId, "SpecialityCat_Label_SpecialityCatIDFailed");


            base.Delete(SpecialityCatId,true);


        }
        #endregion

        #region Search
        /// <summary>
        /// Tìm kiếm Chuyên ngành đào tạo
        /// <author>dungnt</author>
        /// <date>05/09/2012</date>
        /// </summary>
        /// <param name="dic">tham so doi tuong tim kiem</param>
        /// <returns>list doi tuong tim thay</returns>
        public IQueryable<SpecialityCat> Search(IDictionary<string, object> dic)
        {
            string resolution = Utils.GetString(dic,"Resolution");
            string description = Utils.GetString(dic,"Description");

            bool? isActive = Utils.GetIsActive(dic,"IsActive");
            IQueryable<SpecialityCat> lsSpecialityCat = this.SpecialityCatRepository.All;

             if (isActive.HasValue)
            {
                lsSpecialityCat = lsSpecialityCat.Where(em => (em.IsActive == isActive));
            }

         

            if (resolution.Trim().Length != 0)
            {

                lsSpecialityCat = lsSpecialityCat.Where(em => (em.Resolution.ToUpper().Contains(resolution.ToUpper())));
            }
            if (description.Trim().Length != 0)
            {

                lsSpecialityCat = lsSpecialityCat.Where(em => (em.Description.ToUpper().Contains(description.ToUpper())));
            }

            return lsSpecialityCat;
        }
        #endregion

    }
}