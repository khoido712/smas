/**
 * The Viettel License
 *
 * Copyright 2012 Viettel Telecom. All rights reserved.
 * VIETTEL PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */

/** 
* @author  
* @version $Revision: $
*/

using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Linq.Expressions;
using System.Web;
using log4net;

using SMAS.DAL.Repository;
using SMAS.DAL.IRepository;
using SMAS.Business.IBusiness;
using SMAS.Models.Models;
using SMAS.Business.Common;

namespace SMAS.Business.Business
{ 
    public partial class InvigilatorBusiness
    {
          #region Search
        /// <summary>
        /// Search
        /// </summary>
        /// <param name="SearchInfo">The search info.</param>
        /// <returns>
        /// IQueryable{Invigilator}
        /// </returns>
        /// <date>11/17/2012</date>
        /// <author>
        /// hath
        /// </author>
        private IQueryable<Invigilator> Search(IDictionary<string,object> SearchInfo)
        {
            IQueryable<Invigilator> lsInvigilator = InvigilatorRepository.All;
            //Thực hiện tìm kiếm trong bảng Invigilator theo các thông tin trong SearchInfo:
            //InvigilatorID: default = 0;0   All
            int InvigilatorID = Utils.GetInt(SearchInfo, "InvigilatorID");
            //ExaminationID: default =0;0   All
            int ExaminationID = Utils.GetInt(SearchInfo, "ExaminationID");
            //TeacherID: default = 0; 0   All
            int TeacherID = Utils.GetInt(SearchInfo, "TeacherID");
            //EmployeeName: default = “”; “”   All ~ tìm kiếm theo Employee(TeacherID).FullName
            string EmployeeName = Utils.GetString(SearchInfo, "EmployeeName");
            //EmployeeCode: default = “”; “”   All ~ tìm kiếm theo Employee(TeacherID).EmployeeCode
            string EmployeeCode = Utils.GetString(SearchInfo, "EmployeeCode");
            // RoomID: default = 0; 0   All; tìm kiếm exists (select 1 from InvigilatorAssignment ia 
            //where ia.RoomID = RoomID and Invigilator. InvigilatorID = ia. InvigilatorID)
            int RoomID = Utils.GetInt(SearchInfo, "RoomID");
            //FacultyID: default = 0; 0   All; tìm kiếm exists (select 1 from TeacherOfFaculty sf 
            //where sf. FacultyID = SchoolFacultyID and Invigilator.TeacherID = sf.TeacherID)
            int FacultyID = Utils.GetInt(SearchInfo, "SchoolFacultyID");
            //AcademicYearID: default =0; 0 All
            int AcademicYearID = Utils.GetInt(SearchInfo, "AcademicYearID");
            //SchoolID: default =0; 0 All
            int SchoolID = Utils.GetInt(SearchInfo, "SchoolID");
            //EducationLevelID: default =0; 0 All
            int EducationLevelID = Utils.GetInt(SearchInfo, "EducationLevelID");
            //AppliedLevel: default =0; 0 All; tìm kiếm theo Examination(ExaminationID).AppliedLevel
            int AppliedLevel = (int)Utils.GetInt(SearchInfo, "AppliedLevel");

            if (InvigilatorID != 0)
            {
                lsInvigilator = lsInvigilator.Where(o => o.InvigilatorID==InvigilatorID);
            }
            if (ExaminationID != 0)
            {
                lsInvigilator = lsInvigilator.Where(o => o.ExaminationID == ExaminationID);
            }
            if (TeacherID != 0)
            {
                lsInvigilator = lsInvigilator.Where(o => o.TeacherID == TeacherID);
            }

            if (EmployeeName.Trim().Length != 0)
            {
                lsInvigilator = lsInvigilator.Where(o => o.Employee.FullName.ToLower().Contains(EmployeeName.ToLower()));
            }
            if (EmployeeCode.Trim().Length != 0)
            {
                lsInvigilator = lsInvigilator.Where(o => o.Employee.EmployeeCode.ToLower().Contains(EmployeeCode.ToLower()));
            }
            if (RoomID != 0)
            {
                IQueryable<InvigilatorAssignment> lsia = InvigilatorAssignmentBusiness.All.Where(o => o.RoomID == RoomID);
                if (lsia.Count() > 0)
                {
                    List<InvigilatorAssignment> lstia =lsia.ToList();
                    foreach( InvigilatorAssignment ia in lstia )
                    {
                    lsInvigilator = lsInvigilator.Where(o => o.InvigilatorID == ia.InvigilatorID);
                    }
                }
            }

            if (FacultyID != 0)
            {
                lsInvigilator = lsInvigilator.Where(o => o.Employee.SchoolFacultyID == FacultyID);
                //IQueryable<TeacherOfFaculty> lstf = TeacherOfFacultyBusiness.All.Where(o => o.FacultyID == FacultyID);
                //if (lstf.Count() > 0)
                //{
                //    List<TeacherOfFaculty> lsttf = lstf.ToList();
                //    foreach (TeacherOfFaculty tf in lsttf)
                //    {
                //        lsInvigilator = lsInvigilator.Where(o => o.Employee.SchoolFacultyID == tf.FacultyID);
                //    }
                //}

            }
            if (AcademicYearID != 0)
            {
                lsInvigilator = lsInvigilator.Where(o => o.Examination.AcademicYearID == AcademicYearID);
            }
            if (SchoolID != 0)
            {
                lsInvigilator = lsInvigilator.Where(o => o.Examination.SchoolID== SchoolID);
            }

           

            if (AppliedLevel != 0)
            {
                lsInvigilator = lsInvigilator.Where(o => o.Examination.AppliedLevel == AppliedLevel);
            }

            return lsInvigilator;

        }

        /// <summary>
        /// SearchBySchool
        /// </summary>
        /// <param name="SchoolID">The school ID.</param>
        /// <param name="SearchInfo">The search info.</param>
        /// <returns>
        /// IQueryable{Invigilator}
        /// </returns>
        /// <author>hath</author>
        /// <date>11/17/2012</date>
        public IQueryable<Invigilator> SearchBySchool(int SchoolID,IDictionary<string,object> SearchInfo=null)
        {
            if (SchoolID == 0)
            {
                return null;
            }
            else {
                SearchInfo["SchoolID"] = SchoolID;
            }

            return this.Search(SearchInfo);
        }
        #endregion

        #region Delete All
        /// <summary>
        /// DeleteAll
        /// </summary>
        /// <param name="SchoolID">The school ID.</param>
        /// <param name="AppliedLevel">The applied level.</param>
        /// <param name="ListInvigilatorID">The list invigilator ID.</param>
        /// <author>hath</author>
        /// <date>1/14/2013</date>
        public void DeleteAll(int SchoolID, int AppliedLevel, List<int> ListInvigilatorID)
        {
            
            foreach (int i in ListInvigilatorID)
            {
                //this.CheckConstraints(GlobalConstants.EXAM_SCHEMA, "Invigilator", i, "InvigilatorAssignment_Label_Invigilator");
                this.CheckAvailable(i, "InvigilatorAssignment_Label_Invigilator");
                base.Delete(i);
            }
        }
        #endregion

        #region Insert All
        public void InsertAll(int SchoolID, int AppliedLevel, int ExaminationID, List<int> ListEmployeeID)
        {
            foreach(int epid in ListEmployeeID)
            {
                Invigilator iv = new Invigilator();
                iv.ExaminationID = ExaminationID;
                iv.TeacherID = epid;
                base.Insert(iv);

            }
        }

        #endregion

    }
}
