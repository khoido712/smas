/**
 * The Viettel License
 *
 * Copyright 2012 Viettel Telecom. All rights reserved.
 * VIETTEL PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */

/** 
* @author  
* @version $Revision: $
*/

using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Linq.Expressions;
using System.Web;
using log4net;

using SMAS.DAL.Repository;
using SMAS.DAL.IRepository;
using SMAS.Business.IBusiness;
using SMAS.Models.Models;
using SMAS.Business.Common;

namespace SMAS.Business.Business
{
    public partial class ITQualificationLevelBusiness
    {

        /// <summary>
        /// Insert vao CSDL
        /// </summary>
        /// <param name="ITQualificationLevel"></param>
        /// <returns></returns>
        public override ITQualificationLevel Insert(ITQualificationLevel ITQualificationLevel)
        {
            // Kiem tra truong bat buoc
            Utils.ValidateRequire(ITQualificationLevel.Resolution, "ITQualificationLevel_Label_Resolution");
            // Kiem tra truong Resolution maxlength khong duoc vuot qua 50 ky tu
            Utils.ValidateMaxLength(ITQualificationLevel.Resolution, 50, "ITQualificationLevel_Label_Resolution");
            // Kiem tra trung ten 
            ITQualificationLevelBusiness.CheckDuplicate(ITQualificationLevel.Resolution, GlobalConstants.LIST_SCHEMA, "ITQualificationLevel", "Resolution", true, ITQualificationLevel.ITQualificationLevelID, "ITQualificationLevel_Label_Resolution");
            //this.CheckDuplicate(ITQualificationLevel.Resolution, GlobalConstants.LIST_SCHEMA, "ITQualificationLevel", "Resolution", true, null, "ITQualificationLevel_Label_Resolution");
            // Kiem tra truong mo ta khong duoc vuot qua 400 ky tu
            Utils.ValidateMaxLength(ITQualificationLevel.Description, 400, "ITQualificationLevel_Label_Description");
            // Them vao CSDL            
            return base.Insert(ITQualificationLevel);
        }

        /// <summary>
        /// Cap nhat vao CSDL
        /// </summary>
        /// <param name="ITQualificationLevel"></param>
        /// <returns></returns>
        public override ITQualificationLevel Update(ITQualificationLevel ITQualificationLevel)
        {
            // Kiem tra truong bat buoc
            Utils.ValidateRequire(ITQualificationLevel.Resolution, "ITQualificationLevel_Label_Resolution");
            // Kiem tra truong Resolution maxlength khong duoc vuot qua 50 ky tu
            Utils.ValidateMaxLength(ITQualificationLevel.Resolution, 50, "ITQualificationLevel_Label_Resolution");
            // Kiem tra trung ten 
            this.CheckDuplicate(ITQualificationLevel.Resolution, GlobalConstants.LIST_SCHEMA, "ITQualificationLevel", "Resolution", true, ITQualificationLevel.ITQualificationLevelID, "ITQualificationLevel_Label_Resolution");
            // Kiem tra truong mo ta khong duoc vuot qua 400 ky tu
            Utils.ValidateMaxLength(ITQualificationLevel.Description, 400, "ITQualificationLevel_Label_Description");
            // Kiem tra ten trinh do tin hoc da co trong DB
            //this.CheckAvailable(ITQualificationLevel.ITQualificationLevelID, "ITQualificationLevel_Label_Resolution", false);
            new ITQualificationLevelBusiness(null).CheckAvailable(ITQualificationLevel.ITQualificationLevelID, "ITQualificationLevel_Label_Resolution", false);
            // Them vao CSDL            
            return base.Update(ITQualificationLevel);
        }

        /// <summary>
        /// Xoa trinh do tin hoc
        /// </summary>
        /// <param name="ITQualificationLevelID"></param>
        public void Delete(int ITQualificationLevelID)
        {
            // Kiem tra da ton tai trong  CSDL
            this.CheckAvailable(ITQualificationLevelID, "ITQualificationLevel_Label_Title", true);
            this.CheckConstraintsWithIsActive(GlobalConstants.LIST_SCHEMA, "ITQualificationLevel", ITQualificationLevelID, "ITQualificationLevel_Label_Title");
            // Xoa bang cach thiet lap IsActive = 0;
            this.Delete(ITQualificationLevelID, true);
        }

        /// <summary>
        /// Tim kiem trinh do tin hoc
        /// </summary>
        /// <param name="dicParam"></param>
        /// <returns></returns>
        public IQueryable<ITQualificationLevel> Search(IDictionary<string, object> dicParam)
        {
            string Resolution = Utils.GetString(dicParam, "Resolution");
            string Description = Utils.GetString(dicParam, "Description");
            int? Type = Utils.GetNullableInt(dicParam, "Type");
            bool? IsActive = Utils.GetNullableBool(dicParam, "IsActive");
            IQueryable<ITQualificationLevel> listITQualificationLevel = this.ITQualificationLevelRepository.All.OrderBy(o => o.Resolution);
            if (!Resolution.Equals(string.Empty))
            {
                listITQualificationLevel = listITQualificationLevel.Where(ql => ql.Resolution.ToLower().Contains(Resolution.ToLower()));
            }
            if (!Description.Equals(string.Empty))
            {
                listITQualificationLevel = listITQualificationLevel.Where(ql => ql.Description.ToLower().Contains(Description.ToLower()));
            }
            if (IsActive.HasValue)
            {
                listITQualificationLevel = listITQualificationLevel.Where(ql => ql.IsActive == true);
            }

            if (Type.HasValue)
                listITQualificationLevel = listITQualificationLevel.Where(ql => ql.Type == Type);

            return listITQualificationLevel;
        }

    }
}