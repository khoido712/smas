﻿/**
 * The Viettel License
 *
 * Copyright 2012 Viettel Telecom. All rights reserved.
 * VIETTEL PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */

/** 
* @author  
* @version $Revision: $
*/

using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Linq.Expressions;
using System.Web;
using log4net;

using SMAS.DAL.Repository;
using SMAS.DAL.IRepository;
using SMAS.Business.IBusiness;
using SMAS.Models.Models;
using SMAS.Business.Common;

namespace SMAS.Business.Business
{
    public partial class SchoolTypeBusiness
    {

        /// <summary>
        /// Tìm kiếm loại trường
        /// </summary>
        /// <author>hath</author>
        /// <date>4/9/2012</date>
        /// <param name="Resolution">Diễn giải loại trường</param>
        /// <param name="IsActive">Biến kích hoạt</param>
        /// <returns>IQueryable<SchoolType></returns>
        public IQueryable<SchoolType> Search(IDictionary<string, object> dicParam)
        {
            string Resolution = Utils.GetString(dicParam, "Resolution");
            bool? IsActive = Utils.GetIsActive(dicParam, "IsActive");
            IQueryable<SchoolType> listSchoolType = this.SchoolTypeRepository.All;
            if (Resolution.Trim().Length!=0)
            {
                listSchoolType = listSchoolType.Where(x => x.Resolution.ToLower().Contains(Resolution.ToLower()));
            }
            if (IsActive.HasValue)
            {
                listSchoolType = listSchoolType.Where(x => x.IsActive == IsActive);
            }
            int count = listSchoolType != null ? listSchoolType.Count() : 0;
            if (count == 0)
            {
                return null;
            }
            return listSchoolType;

        }

    }
}