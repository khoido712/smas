﻿/**
 * The Viettel License
 *
 * Copyright 2012 Viettel Telecom. All rights reserved.
 * VIETTEL PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */

/** 
* @author  minhh
* @version $Revision: $
*/

using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Linq.Expressions;
using System.Web;
using log4net;

using SMAS.DAL.Repository;
using SMAS.DAL.IRepository;
using SMAS.Business.IBusiness;
using SMAS.Models.Models;
using SMAS.Business.Common;
using SMAS.Business.BusinessObject;
using System.Text;

namespace SMAS.Business.Business
{
    public partial class GoodChildrenTicketBusiness
    {
        private const int NOTE_MAX_LENGTH = 250;
        private IQueryable<GoodChildrenTicket> Search(IDictionary<string, object> dic)
        {
            int GoodChildrenTicketID = Utils.GetInt(dic, "GoodChildrenTicketID");
            bool? IsGood = Utils.GetNullableBool(dic, "IsGood");
            string Note = Utils.GetString(dic, "Note");
            string PupilCode = Utils.GetString(dic, "PupilCode");
            string FullName = Utils.GetString(dic, "FullName");
            DateTime? Month = Utils.GetDateTime(dic, "Month");
            int EducationLevelID = Utils.GetShort(dic, "EducationLevelID");
            int AppliedLevel = Utils.GetShort(dic, "AppliedLevel");
            int Year = Utils.GetInt(dic, "Year");
            int SchoolID = Utils.GetInt(dic, "SchoolID");
            int ClassID = Utils.GetInt(dic, "ClassID");
            int AcademicYearID = Utils.GetInt(dic, "AcademicYearID");
            int PupilID = Utils.GetInt(dic, "PupilID");
            int WeekInMonth = Utils.GetByte(dic, "WeekInMonth");
            List<int> lstPupilID = Utils.GetIntList(dic, "lstPupilID");
            List<int> LstWeekInMonth = Utils.GetIntList(dic, "LstWeekInMonth");
            IQueryable<GoodChildrenTicket> Query = GoodChildrenTicketRepository.All;
            if (EducationLevelID > 0)
            {
                Query = Query.Where(evd => evd.ClassProfile.EducationLevelID == EducationLevelID);
            }
            if (AppliedLevel != 0)
            {
                Query = Query.Where(evd => evd.ClassProfile.EducationLevel.Grade == AppliedLevel);
            }
            if (GoodChildrenTicketID != 0)
            {
                Query = Query.Where(evd => evd.GoodChildrenTicketID == GoodChildrenTicketID);
            }
            if (SchoolID != 0)
            {
                Query = Query.Where(evd => evd.SchoolID == SchoolID);
            }
            if (AcademicYearID != 0)
            {
                Query = Query.Where(evd => evd.AcademicYearID == AcademicYearID);
            }
            if (PupilID != 0)
            {
                Query = Query.Where(evd => evd.PupilID == PupilID);
            }
            if (ClassID != 0)
            {
                Query = Query.Where(evd => evd.ClassID == ClassID);
            }
            if (Year != 0)
            {
                Query = Query.Where(evd => evd.Year == Year);
            }
            if (WeekInMonth != 0)
            {
                Query = Query.Where(evd => evd.WeekInMonth == WeekInMonth);
            }
            if (IsGood.HasValue)
            {
                Query = Query.Where(evd => evd.IsGood == IsGood);
            }

            if (lstPupilID.Count() > 0)
            {
                Query = Query.Where(evd => lstPupilID.Contains(evd.PupilID));
            }
            if (PupilCode.Trim().Length != 0) Query = Query.Where(o => o.PupilProfile.PupilCode.ToLower().Contains(PupilCode.ToLower()));
            if (FullName.Trim().Length != 0) Query = Query.Where(o => o.PupilProfile.FullName.ToLower().Contains(FullName.ToLower()));
            if (Note.Trim().Length != 0) Query = Query.Where(o => o.Note.Contains(Note.ToLower()));
            if (Month.HasValue)
            {
                DateTime EndMonth = Month.Value.AddMonths(1);
                DateTime monthStart = Month.Value.Date;
                DateTime endStart = EndMonth.Date;
                Query = Query.Where(o => o.Month >= monthStart && o.Month < endStart);
            }
            int count = 0;
            if (LstWeekInMonth.Count != 0)
            {
                List<int> lstInt = new List<int>();
                //Query = Query.Where(o => LstWeekInMonth.Contains(o.WeekInMonth) == true);
                for (int i = 0; i < LstWeekInMonth.Count(); i++)
                {
                    if (LstWeekInMonth[i] == 1)
                    {
                        lstInt.Add(i + 1);
                        count++;
                    }
                }
                if (count == 4)
                {
                    lstInt.Add(5);
                }
                Query = Query.Where(o => lstInt.Contains(o.WeekInMonth) == true);
            }
            return Query;
        }

        public IQueryable<GoodChildrenTicket> SearchBySchool(int SchoolID, IDictionary<string, object> dic)
        {
            if (SchoolID == 0) return null;
            if (SchoolID != 0)
                dic["SchoolID"] = SchoolID;
            return Search(dic);
        }

        private void Validate(GoodChildrenTicket entity)
        {
            //ValidationMetadata.ValidateObject(entity);
            Utils.ValidateMaxLength(entity.Note, NOTE_MAX_LENGTH, "GoodChildrenTicket_Label_Note");
            //SchoolID, AcademicYearID: not compatible(AcademicYear)
            bool AcademicYearCompatible = new AcademicYearRepository(this.context).ExistsRow(GlobalConstants.SCHOOL_SCHEMA, "AcademicYear",
                  new Dictionary<string, object>()
                {
                    {"SchoolID",entity.SchoolID},
                    {"AcademicYearID",entity.AcademicYearID}
                }, null);
            if (!AcademicYearCompatible)
            {
                throw new BusinessException("Common_Validate_NotCompatible");
            }
            //ClassID, AcademicYearID: not compatible(ClassProfile)
            bool ClassProfileCompatible = new ClassProfileRepository(this.context).ExistsRow(GlobalConstants.SCHOOL_SCHEMA, "ClassProfile",
                  new Dictionary<string, object>()
                {
                    {"ClassProfileID",entity.ClassID},
                    {"AcademicYearID",entity.AcademicYearID}
                }, null);
            if (!ClassProfileCompatible)
            {
                throw new BusinessException("Common_Validate_NotCompatible");
            }
            //PupilID, ClassID: not compatible(PupilProfile)
            bool PupilProfileCompatible = new PupilProfileRepository(this.context).ExistsRow(GlobalConstants.PUPIL_SCHEMA, "PupilProfile",
                  new Dictionary<string, object>()
                {
                    {"PupilProfileID",entity.PupilID},
                    {"CurrentClassID",entity.ClassID}
                }, null);
            if (!PupilProfileCompatible)
            {
                throw new BusinessException("Common_Validate_NotCompatible");
            }
        }

        public void Insert(IDictionary<string, object> dic, List<GoodChildrenTicket> lst)
        {
            int SchoolID = Utils.GetInt(dic, "SchoolID");
            IDictionary<string, object> SearchInfo = new Dictionary<string, object>();
            SearchInfo["SchoolID"] = SchoolID;
            SearchInfo["AcademicYearID"] = Utils.GetInt(dic, "AcademicYearID");
            SearchInfo["EducationLevelID"] = Utils.GetInt(dic, "EducationLevelID");
            SearchInfo["ClassID"] = Utils.GetInt(dic, "ClassID");
            SearchInfo["Month"] = Utils.GetDateTime(dic, "Month");
            SearchInfo["PupilCode"] = Utils.GetString(dic, "PupilCode");
            SearchInfo["FullName"] = Utils.GetString(dic, "PupilName");
            IQueryable<GoodChildrenTicket> lstGoodChildren = this.SearchBySchool(SchoolID, SearchInfo);

            IQueryable<GoodChildrenTicket> ls = lstGoodChildren;
            List<int> LstWeekInMonth = Utils.GetIntList(dic, "LstWeekInMonth");
            int count = 0;
            if (LstWeekInMonth.Count != 0)
            {
                List<int> lstInt = new List<int>();
                //Query = Query.Where(o => LstWeekInMonth.Contains(o.WeekInMonth) == true);
                for (int i = 0; i < LstWeekInMonth.Count(); i++)
                {
                    if (LstWeekInMonth[i] == 1)
                    {
                        lstInt.Add(i + 1);
                        count++;
                    }
                }
                lstInt.Add(5);
                ls = lstGoodChildren.Where(o => lstInt.Contains(o.WeekInMonth) == true);
            }
            List<GoodChildrenTicket> lstGood = (from p in ls
                                                join q in PupilOfClassBusiness.All on new { p.PupilID, p.ClassID } equals new { q.PupilID, q.ClassID }
                                                where q.Status == SystemParamsInFile.PUPIL_STATUS_STUDYING || q.Status == SystemParamsInFile.PUPIL_STATUS_GRADUATED
                                                select p).ToList();

            if (lstGood != null)
            {
                foreach (GoodChildrenTicket item in lstGood)
                {
                    base.Delete(item.GoodChildrenTicketID);
                }
            }
            foreach (GoodChildrenTicket gct in lst)
            {
                this.Validate(gct);
                base.Insert(gct);
            }

        }

        public void SearchToDelete(IDictionary<string, object> dic)
        {
            int SchoolID = Utils.GetInt(dic, "SchoolID");
            //IQueryable <GoodChildrenTicket> ls = this.SearchBySchool(SchoolID, dic);


            IDictionary<string, object> SearchInfo = new Dictionary<string, object>();
            SearchInfo["SchoolID"] = SchoolID;
            SearchInfo["AcademicYearID"] = Utils.GetInt(dic, "AcademicYearID");
            SearchInfo["EducationLevelID"] = Utils.GetInt(dic, "EducationLevelID");
            SearchInfo["ClassID"] = Utils.GetInt(dic, "ClassID");
            SearchInfo["Month"] = Utils.GetDateTime(dic, "Month");
            SearchInfo["PupilCode"] = Utils.GetString(dic, "PupilCode");
            SearchInfo["FullName"] = Utils.GetString(dic, "PupilName");
            IQueryable<GoodChildrenTicket> lstGoodChildren = this.SearchBySchool(SchoolID, SearchInfo);
            List<int> LstWeekInMonth = Utils.GetIntList(dic, "LstWeekInMonth");
            IQueryable<GoodChildrenTicket> ls = lstGoodChildren;
            int count = 0;
            if (LstWeekInMonth.Count != 0)
            {
                List<int> lstInt = new List<int>();
                //Query = Query.Where(o => LstWeekInMonth.Contains(o.WeekInMonth) == true);
                for (int i = 0; i < LstWeekInMonth.Count(); i++)
                {
                    if (LstWeekInMonth[i] == 1)
                    {
                        lstInt.Add(i + 1);
                        count++;
                    }
                }
                lstInt.Add(5);
                ls = lstGoodChildren.Where(o => lstInt.Contains(o.WeekInMonth) == true);
            }

            List<GoodChildrenTicket> lstGood = (from p in ls
                                                join q in PupilOfClassBusiness.All on new { p.PupilID, p.ClassID } equals new { q.PupilID, q.ClassID }
                                                where q.Status == SystemParamsInFile.PUPIL_STATUS_STUDYING || q.Status == SystemParamsInFile.PUPIL_STATUS_GRADUATED
                                                select p).ToList();

            if (lstGood != null)
            {
                foreach (GoodChildrenTicket item in lstGood)
                {
                    if ((item.WeekInMonth == 5 && (lstGoodChildren.Where(o => o.PupilID == item.PupilID && o.WeekInMonth != 5).Count() <= count || lstGoodChildren.Where(o => o.PupilID == item.PupilID && o.WeekInMonth != 5).Count() == 0)) || item.WeekInMonth != 5)
                    {
                        base.Delete(item.GoodChildrenTicketID);
                    }
                }
            }
        }

        public void InsertOrUpdateGoodChildren(List<GoodChildrenTicket> lstGoodChildren, IDictionary<string, object> dic)
        {
            List<GoodChildrenTicket> lstGoodChildrenTicketDB = this.Search(dic).ToList();

            GoodChildrenTicket objInput = null;
            for (int i = 0; i < lstGoodChildren.Count; i++)
            {
                objInput = lstGoodChildren[i];
                var obj = lstGoodChildrenTicketDB.Where(x => x.PupilID == objInput.PupilID && x.AcademicYearID == objInput.AcademicYearID
                                                        && x.SchoolID == objInput.SchoolID && x.WeekInMonth == objInput.WeekInMonth
                                                        && x.Month == objInput.Month && x.ClassID == objInput.ClassID).FirstOrDefault();
                if (obj != null)
                {
                    obj.Note = objInput.Note;
                    obj.IsGood = objInput.IsGood;
                    obj.LogID = objInput.LogID;
                    obj.ModifiedDate = objInput.ModifiedDate;
                    this.Update(obj);
                }
                else
                {
                    this.Insert(objInput);
                }
            }

            if (lstGoodChildren.Count() > 0)
            {
                this.Save();
            }


        }


        public void DeleteGoodChildren(IDictionary<string, object> dic)
        {
            int ClassID = Utils.GetInt(dic, "ClassID");
            List<int> lstPupilID = Utils.GetIntList(dic, "lstPupilID");

            List<GoodChildrenTicket> lstGoodChildrenTicketDB = this.Search(dic).ToList();
            List<PupilOfClassBO> lstPOC = PupilOfClassBusiness.GetPupilInClass(ClassID).Where(p => lstPupilID.Contains(p.PupilID)).Distinct().ToList();
            PupilOfClassBO objPOC = null;

            foreach (var item in lstGoodChildrenTicketDB)
            {
                objPOC = lstPOC.Where(p => p.PupilID == item.PupilID).FirstOrDefault();
                if (objPOC != null)
                {
                    this.Delete(item.GoodChildrenTicketID);
                    objPOC = null;
                }
            }

            this.Save();
        }

        /// <summary>
        /// Be ngoan tuan, thang
        /// </summary>
        /// <param name="pupilIDList"></param>
        /// <param name="schoolID"></param>
        /// <param name="academicYearID"></param>
        /// <param name="classID"></param>
        /// <param name="type"></param>
        /// <param name="month"></param>
        /// <param name="week"></param>
        /// <returns></returns>
        public List<PreschoolResultBO> GetGoodTicketOfPupil(List<int> pupilIDList, int schoolID, int academicYearID, List<int> lstClassID, int type, DateTime month, int week)
        {
            try
            {
                //list hoc sinh trong lop
                List<PupilOfClassBO> lstPupilOfClass = (from poc in PupilOfClassBusiness.AllNoTracking
                                                        join pp in PupilProfileBusiness.AllNoTracking.OrderBy(x => x.PupilProfileID) on poc.PupilID equals pp.PupilProfileID
                                                        where pupilIDList.Contains(poc.PupilID)
                                                        && poc.SchoolID == schoolID && poc.AcademicYearID == academicYearID
                                                        && lstClassID.Contains(poc.ClassID)
                                                        && poc.Status == GlobalConstants.PUPIL_STATUS_STUDYING
                                                        select new PupilOfClassBO()
                                                        {
                                                            ClassID = poc.ClassID,
                                                            PupilID = poc.PupilID,
                                                            PupilFullName = pp.FullName,
                                                            Name = pp.Name,
                                                            OrderInClass = poc.OrderInClass,
                                                        }).OrderBy(c => c.OrderInClass).ThenBy(c => c.Name).ThenBy(c => c.PupilFullName).ToList();

                //Lay danh sach phieu be ngoan
                IDictionary<string, object> dic = new Dictionary<string, object>();
                dic["IsGood"] = true;
                dic["Month"] = month;
                dic["SchoolID"] = schoolID;
                dic["AcademicYearID"] = academicYearID;
                if (type == 1)
                {
                    dic["WeekInMonth"] = week;
                }

                List<GoodChildrenTicket> lstGct = this.GoodChildrenTicketBusiness.SearchBySchool(schoolID, dic).Where(p => lstClassID.Contains(p.ClassID) && pupilIDList.Contains(p.PupilID)).ToList();

                List<PreschoolResultBO> lstData = new List<PreschoolResultBO>();
                PreschoolResultBO objData;
                int ClassID = 0;

                for (int i = 0; i < lstPupilOfClass.Count; i++)
                {
                    PupilOfClassBO poc = lstPupilOfClass[i];
                    ClassID = poc.ClassID;
                    StringBuilder sb = new StringBuilder();
                    objData = new PreschoolResultBO();
                    objData.PupilID = poc.PupilID;

                    if (type == 1)
                    {
                        GoodChildrenTicket cgt = lstGct.FirstOrDefault(o => o.ClassID == ClassID && o.PupilID == poc.PupilID);

                        if (cgt != null)
                        {
                            //Tuan
                            if (type == 1)
                            {
                                sb.Append(string.Format("đạt danh hiệu bé ngoan tuần {0} tháng {1}. ", week, month.Month));
                                if (!string.IsNullOrWhiteSpace(cgt.Note))
                                {
                                    sb.Append("Nhận xét gv: ").Append(cgt.Note.Trim());
                                }
                            }
                            //Thang
                            else
                            {
                                sb.Append(string.Format("đạt danh hiệu bé ngoan tháng {0}. ", month.Month));
                                if (!string.IsNullOrWhiteSpace(cgt.Note))
                                {
                                    sb.Append("Nhận xét gv: ").Append(cgt.Note.Trim());
                                }
                            }
                        }
                    }
                    else
                    {
                        GoodChildrenTicket cgt = lstGct.FirstOrDefault(o => o.ClassID == ClassID && o.PupilID == poc.PupilID && o.WeekInMonth == 6);
                        if (cgt != null)
                        {
                            sb.Append(string.Format("đạt danh hiệu bé ngoan tháng {0}. ", month.Month));
                            if (!string.IsNullOrWhiteSpace(cgt.Note))
                            {
                                sb.Append("Nhận xét gv: ").Append(cgt.Note.Trim());
                            }
                        }
                    }

                    objData.SMSContent = sb.ToString().Trim();

                    lstData.Add(objData);
                }

                return lstData;
            }
            catch (Exception ex)
            {
                logger.Error(ex.Message, ex);
                return new List<PreschoolResultBO>();
            }
        }
        /// <summary>
        /// Thong bao thuc don ngay va thuc don tuan
        /// </summary>
        /// <param name="pupilIDList"></param>
        /// <param name="schoolID"></param>
        /// <param name="academicYearID"></param>
        /// <param name="classID"></param>
        /// <param name="type"></param>
        /// <param name="fromDate"></param>
        /// <param name="toDate"></param>
        /// <returns></returns>
        public List<PreschoolResultBO> GetMealMenuOfPupil(List<int> pupilIDList, int schoolID, int academicYearID, List<int> lstClassID, int type, DateTime fromDate, DateTime toDate)
        {
            try
            {
                //list hoc sinh trong lop
                List<PupilOfClassBO> lstPupilOfClass = (from poc in PupilOfClassBusiness.AllNoTracking
                                                        join pp in PupilProfileBusiness.AllNoTracking.OrderBy(x => x.PupilProfileID) on poc.PupilID equals pp.PupilProfileID
                                                        where pupilIDList.Contains(poc.PupilID)
                                                        && poc.SchoolID == schoolID && poc.AcademicYearID == academicYearID
                                                        && lstClassID.Contains(poc.ClassID)
                                                        && poc.Status == GlobalConstants.PUPIL_STATUS_STUDYING
                                                        select new PupilOfClassBO()
                                                        {
                                                            ClassID = poc.ClassID,
                                                            PupilID = poc.PupilID,
                                                            PupilFullName = pp.FullName,
                                                            Name = pp.Name,
                                                            OrderInClass = poc.OrderInClass,
                                                        }).OrderBy(c => c.OrderInClass).ThenBy(c => c.Name).ThenBy(c => c.PupilFullName).ToList();

                //Lay ra danh sach bua an
                List<MealCategory> lstMealCategory = this.MealCategoryBusiness.GetMealCategoriesBySchool(schoolID);

                //Lay thuc don cua lop
                IDictionary<string, object> dic = new Dictionary<string, object>();
                dic["SchoolID"] = schoolID;
                dic["AcademicYearID"] = academicYearID;
                //dic["ClassID"] = classID;
                dic["DateFrom"] = fromDate;
                dic["DateTo"] = toDate;

                List<WeeklyMealMenu> lstWmm = this.WeeklyMealMenuBusiness.Search(dic).Where(p => lstClassID.Contains(p.ClassID)).OrderBy(o => o.OrderNumber).ToList();
                StringBuilder sb = new StringBuilder();
                IDictionary<int, StringBuilder> dicContent = new Dictionary<int, StringBuilder>();
                int ClassID = 0;
                //Thuc don ngay
                if (type == 1)
                {
                    for (int i = 0; i < lstClassID.Count; i++)
                    {
                        ClassID = lstClassID[i];
                        sb = new StringBuilder();
                        List<WeeklyMealMenu> lstWmmByDay = lstWmm.Where(o => o.DateOfMeal.CompareTo(fromDate) == 0 && o.ClassID == ClassID).ToList();
                        for (int j = 0; j < lstMealCategory.Count; j++)
                        {
                            MealCategory mc = lstMealCategory[j];
                            List<WeeklyMealMenu> lstWmmOfCat = lstWmmByDay.Where(o => o.MealCategoryID == mc.MealCategoryID).ToList();

                            if (lstWmmOfCat.Count > 0)
                            {
                                sb.Append(mc.MealName + ": ");
                                for (int k = 0; k < lstWmmOfCat.Count; k++)
                                {
                                    WeeklyMealMenu wmm = lstWmmOfCat[k];
                                    sb.Append(wmm.DishName);

                                    if (k < lstWmmOfCat.Count - 1)
                                    {
                                        sb.Append(", ");
                                    }

                                }

                                sb.Append("\n");
                            }
                        }
                        dicContent[ClassID] = sb;
                    }
                }
                //Thuc don tuan
                else
                {
                    for (int c = 0; c < lstClassID.Count; c++)
                    {
                        ClassID = lstClassID[c];
                        sb = new StringBuilder();
                        for (DateTime date = fromDate; date.CompareTo(toDate) <= 0; date = date.AddDays(1))
                        {
                            List<WeeklyMealMenu> lstWmmByDay = lstWmm.Where(o => o.DateOfMeal.CompareTo(date) == 0 && o.ClassID == ClassID).ToList();
                            if (lstWmmByDay.Count > 0)
                            {
                                sb.Append(date.ToString("dd/MM") + ".");

                                int index = 0;
                                int catCount = lstWmmByDay.Select(o => o.MealCategoryID).Distinct().Count();
                                for (int i = 0; i < lstMealCategory.Count; i++)
                                {
                                    MealCategory mc = lstMealCategory[i];
                                    List<WeeklyMealMenu> lstWmmOfCat = lstWmmByDay.Where(o => o.MealCategoryID == mc.MealCategoryID).ToList();

                                    if (lstWmmOfCat.Count > 0)
                                    {
                                        sb.Append(mc.MealName + ": ");
                                        for (int j = 0; j < lstWmmOfCat.Count; j++)
                                        {
                                            WeeklyMealMenu wmm = lstWmmOfCat[j];
                                            sb.Append(wmm.DishName);

                                            if (j < lstWmmOfCat.Count - 1)
                                            {
                                                sb.Append(", ");
                                            }

                                        }

                                        if (index < catCount - 1)
                                        {
                                            sb.Append(";");
                                        }

                                        index++;
                                    }
                                }

                                sb.Append("\n");
                            }
                        }
                        dicContent[ClassID] = sb;
                    }
                }
                List<PreschoolResultBO> lstData = new List<PreschoolResultBO>();
                PreschoolResultBO objData;

                for (int i = 0; i < lstPupilOfClass.Count; i++)
                {
                    PupilOfClassBO poc = lstPupilOfClass[i];
                    objData = new PreschoolResultBO();
                    objData.PupilID = poc.PupilID;
                    objData.SMSContent = dicContent[poc.ClassID].ToString().Trim();
                    lstData.Add(objData);
                }
                return lstData;
            }
            catch (Exception ex)
            {
                logger.Error(ex.Message, ex);
                return new List<PreschoolResultBO>();
            }
        }
        /// <summary>
        /// Can do suc khoe
        /// </summary>
        /// <param name="pupilIDList"></param>
        /// <param name="schoolID"></param>
        /// <param name="academicYearID"></param>
        /// <param name="classID"></param>
        /// <param name="month"></param>
        /// <returns></returns>
        public List<PreschoolResultBO> GetPhysicalResultOfPupil(List<int> pupilIDList, int schoolID, int academicYearID, List<int> lstClassID, int month)
        {
            try
            {
                //list hoc sinh trong lop
                List<PupilOfClassBO> lstPupilOfClass = (from poc in PupilOfClassBusiness.AllNoTracking
                                                        join pp in PupilProfileBusiness.AllNoTracking.OrderBy(x => x.PupilProfileID) on poc.PupilID equals pp.PupilProfileID
                                                        where pupilIDList.Contains(poc.PupilID)
                                                        && poc.SchoolID == schoolID && poc.AcademicYearID == academicYearID
                                                        && lstClassID.Contains(poc.ClassID)
                                                        && poc.Status == GlobalConstants.PUPIL_STATUS_STUDYING
                                                        select new PupilOfClassBO()
                                                        {
                                                            ClassID = poc.ClassID,
                                                            PupilID = poc.PupilID,
                                                            PupilFullName = pp.FullName,
                                                            Name = pp.Name,
                                                            OrderInClass = poc.OrderInClass,
                                                        }).OrderBy(c => c.OrderInClass).ThenBy(c => c.Name).ThenBy(c => c.PupilFullName).ToList();

                //Lay danh sach ket qua can do suc khoe
                IDictionary<string, object> dic = new Dictionary<string, object>();
                dic["SchoolID"] = schoolID;
                dic["AcademicYearID"] = academicYearID;
                //dic["ClassID"] = classID;
                dic["MonthID"] = month;

                List<PhysicalExamination> lstPe = this.PhysicalExaminationBusiness.Search(dic).Where(p => lstClassID.Contains(p.ClassID)).ToList();

                List<PreschoolResultBO> lstData = new List<PreschoolResultBO>();
                PreschoolResultBO objData;
                int ClassID = 0;
                for (int i = 0; i < lstPupilOfClass.Count; i++)
                {
                    PupilOfClassBO poc = lstPupilOfClass[i];
                    ClassID = poc.ClassID;
                    StringBuilder sb = new StringBuilder();

                    objData = new PreschoolResultBO();
                    objData.PupilID = poc.PupilID;

                    PhysicalExamination peOfPupil = lstPe.FirstOrDefault(o => o.ClassID == ClassID && o.PupilID == poc.PupilID);

                    if (peOfPupil != null)
                    {
                        if (peOfPupil.Height.HasValue)
                        {
                            sb.Append(string.Format("Cao: {0} cm - {1}", String.Format("{0:0.#}", peOfPupil.Height.Value).Replace(",","."), this.GetPhysicalState(peOfPupil.PhysicalHeightStatus, 1)));
                            sb.Append("\n");
                        }

                        if (peOfPupil.Weight.HasValue)
                        {
                            sb.Append(string.Format("Nặng: {0} kg - {1}", String.Format("{0:0.#}", peOfPupil.Weight.Value).Replace(",", "."), this.GetPhysicalState(peOfPupil.PhysicalWeightStatus, 2)));
                        }

                    }

                    objData.SMSContent = sb.ToString().Trim();

                    lstData.Add(objData);
                }

                return lstData;
            }
            catch (Exception ex)
            {
                logger.Error(ex.Message, ex);
                return new List<PreschoolResultBO>();
            }
        }
        /// <summary>
        /// Danh gia su phat trien
        /// </summary>
        /// <param name="pupilIDList"></param>
        /// <param name="schoolID"></param>
        /// <param name="academicYearID"></param>
        /// <param name="educationLevel"></param>
        /// <param name="classID"></param>
        /// <returns></returns>
        public List<PreschoolResultBO> GetGrowthEvaluationOfPupil(List<int> pupilIDList, int schoolID, int academicYearID, List<int> lstClassID)
        {
            try
            {
                //list hoc sinh trong lop
                List<PupilOfClassBO> lstPupilOfClass = (from poc in PupilOfClassBusiness.AllNoTracking
                                                        join pp in PupilProfileBusiness.AllNoTracking.OrderBy(x => x.PupilProfileID) on poc.PupilID equals pp.PupilProfileID
                                                        where pupilIDList.Contains(poc.PupilID)
                                                        && poc.SchoolID == schoolID && poc.AcademicYearID == academicYearID
                                                        && lstClassID.Contains(poc.ClassID)
                                                        && poc.Status == GlobalConstants.PUPIL_STATUS_STUDYING
                                                        select new PupilOfClassBO()
                                                        {
                                                            ClassID = poc.ClassID,
                                                            PupilID = poc.PupilID,
                                                            PupilFullName = pp.FullName,
                                                            Name = pp.Name,
                                                            OrderInClass = poc.OrderInClass,
                                                            EducationLevelID = poc.ClassProfile.EducationLevelID
                                                        }).OrderBy(c => c.OrderInClass).ThenBy(c => c.Name).ThenBy(c => c.PupilFullName).ToList();

                //Lay danh sach linh vuc
                List<DeclareEvaluationGroupBO> lstDeg = (from d in DeclareEvaluationGroupBusiness.All
                                                         join g in EvaluationDevelopmentGroupBusiness.All on d.EvaluationGroupID equals g.EvaluationDevelopmentGroupID
                                                         where d.SchoolID == schoolID
                                                         && d.AcademicYearID == academicYearID
                                                         orderby g.OrderID
                                                         select new DeclareEvaluationGroupBO
                                                         {
                                                             DeclareEvaluationGroupID = d.DeclareEvaluationGroupID,
                                                             EvaluationDevelopmentGroupName = g.EvaluationDevelopmentGroupName,
                                                             EvaluationGroupID = g.EvaluationDevelopmentGroupID,
                                                             EducationLevelID = d.EducationLevelID
                                                         }).ToList();

                //Lay danh sach danh gia su phat trien
                IDictionary<string, object> dic = new Dictionary<string, object>();
                dic["SchoolID"] = schoolID;
                dic["AcademicYearID"] = academicYearID;
                dic["lstClassID"] = lstClassID;
                //dic["lstPupilID"] = pupilIDList;
                List<GrowthEvaluation> lstGe = (from g in this.GrowthEvaluationBusiness.Search(dic)
                                                join i in this.DeclareEvaluationIndexBusiness.All
                                                    .Where(o => o.AcademicYearID == academicYearID && o.SchoolID == schoolID)
                                                    on new { p1 = g.DeclareEvaluationGroupID, p2 = g.DeclareEvaluationIndexID } equals new { p1 = i.EvaluationDevelopmentGroID, p2 = i.EvaluationDevelopmentID }
                                                select g).ToList();

                List<PreschoolResultBO> lstData = new List<PreschoolResultBO>();
                PreschoolResultBO objData;
                for (int i = 0; i < lstPupilOfClass.Count; i++)
                {
                    PupilOfClassBO poc = lstPupilOfClass[i];
                    StringBuilder sb = new StringBuilder();

                    objData = new PreschoolResultBO();
                    objData.PupilID = poc.PupilID;
                    var lstDegtmp = lstDeg.Where(p => p.EducationLevelID == poc.EducationLevelID).ToList();
                    for (int j = 0; j < lstDegtmp.Count; j++)
                    {
                        DeclareEvaluationGroupBO deg = lstDegtmp[j];
                        int count = lstGe.Where(o => o.PupilID == poc.PupilID && o.ClassID == poc.ClassID && o.DeclareEvaluationGroupID == deg.EvaluationGroupID).Count();
                        int dCount = lstGe.Where(o => o.PupilID == poc.PupilID && o.ClassID == poc.ClassID && o.DeclareEvaluationGroupID == deg.EvaluationGroupID && (o.EvaluationType == 1 || o.EvaluationType == 3)).Count();

                        if (count > 0)
                        {
                            sb.Append(string.Format("{0}: Đạt {1}/{2}", deg.EvaluationDevelopmentGroupName, dCount, count));
                            sb.Append("\n");
                        }
                    }
                    objData.SMSContent = sb.ToString().Trim();
                    lstData.Add(objData);
                }
                return lstData;
            }
            catch (Exception ex)
            {
                logger.Error(ex.Message, ex);
                return new List<PreschoolResultBO>();
            }
        }
        /// <summary>
        /// Danh gia hoat dong
        /// </summary>
        /// <param name="pupilIDList"></param>
        /// <param name="schoolID"></param>
        /// <param name="academicYearID"></param>
        /// <param name="classID"></param>
        /// <param name="date"></param>
        /// <returns></returns>
        public List<PreschoolResultBO> GetDailyActivityOfPupil(List<int> pupilIDList, int schoolID, int academicYearID, List<int> lstClassID, DateTime date)
        {
            try
            {
                //list hoc sinh trong lop
                List<PupilOfClassBO> lstPupilOfClass = (from poc in PupilOfClassBusiness.AllNoTracking
                                                        join pp in PupilProfileBusiness.AllNoTracking.OrderBy(x => x.PupilProfileID) on poc.PupilID equals pp.PupilProfileID
                                                        where pupilIDList.Contains(poc.PupilID)
                                                        && poc.SchoolID == schoolID && poc.AcademicYearID == academicYearID
                                                        && lstClassID.Contains(poc.ClassID)
                                                        && poc.Status == GlobalConstants.PUPIL_STATUS_STUDYING
                                                        select new PupilOfClassBO()
                                                        {
                                                            ClassID = poc.ClassID,
                                                            PupilID = poc.PupilID,
                                                            PupilFullName = pp.FullName,
                                                            Name = pp.Name,
                                                            OrderInClass = poc.OrderInClass,
                                                        }).OrderBy(c => c.OrderInClass).ThenBy(c => c.Name).ThenBy(c => c.PupilFullName).ToList();

                //Lay danh sach danh gia hoat dong ngay
                IDictionary<string, object> dic = new Dictionary<string, object>();
                dic["SchoolID"] = schoolID;
                dic["AcademicYearID"] = academicYearID;

                var iq = (from i in ActivityOfPupilBusiness.SearchBySchool(schoolID, dic)
                          join a in this.ActivityBusiness.All on i.ActivityOfClassID equals a.ActivityID
                          where a.ActivityDate == date
                          && lstClassID.Contains(i.ClassID)
                          orderby a.FromTime
                          select i).ToList();

                List<ActivityOfPupilToSMS> lstAct = (from i in iq
                                                     group i by new { i.ClassID, i.PupilID } into g
                                                     select new ActivityOfPupilToSMS
                                                     {
                                                         ClassID = g.Key.ClassID,
                                                         PupilID = g.Key.PupilID,
                                                         ContentActivity = string.Join("; ", g.Select(o => o.CommentOfTeacher).ToList())
                                                     }).ToList();
                List<PreschoolResultBO> lstData = new List<PreschoolResultBO>();
                PreschoolResultBO objData;
                int ClassID = 0;
                for (int i = 0; i < lstPupilOfClass.Count; i++)
                {
                    PupilOfClassBO poc = lstPupilOfClass[i];

                    objData = new PreschoolResultBO();
                    objData.PupilID = poc.PupilID;
                    ClassID = poc.ClassID;
                    ActivityOfPupilToSMS act = lstAct.FirstOrDefault(o => o.ClassID == ClassID && o.PupilID == poc.PupilID);

                    if (act != null)
                    {
                        objData.SMSContent = act.ContentActivity.Trim();
                    }
                    lstData.Add(objData);
                }
                return lstData;
            }
            catch (Exception ex)
            {
                logger.Error(ex.Message, ex);
                return new List<PreschoolResultBO>();
            }
        }
        private string GetPhysicalState(int? stt, int type)
        {
            //Chieu cao
            if (type == 1)
            {
                switch (stt.Value)
                {
                    case 1:
                        return "Cao hơn so với tuổi";
                    case 2:
                        return "Bình thuờng";
                    case 3:
                        return "Thấp còi độ 1";
                    case 4:
                        return "Thấp còi độ 2";
                    default:
                        return string.Empty;
                }
            }
            //Can nang
            else
            {
                switch (stt.Value)
                {
                    case 1:
                        return "Cân nặng hon so với tuổi";
                    case 2:
                        return "Bình thuờng";
                    case 3:
                        return "Suy dinh duỡng nhẹ";
                    case 4:
                        return "Suy dinh duỡng nặng";
                    default:
                        return string.Empty;
                }
            }
        }
    }
}
