﻿/**
 * The Viettel License
 *
 * Copyright 2012 Viettel Telecom. All rights reserved.
 * VIETTEL PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */

/** 
* @author  
* @version $Revision: $
*/

using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Linq.Expressions;
using System.Web;
using log4net;

using SMAS.DAL.Repository;
using SMAS.DAL.IRepository;
using SMAS.Business.IBusiness;
using SMAS.Models.Models;
using SMAS.Business.Common;
using SMAS.Business.BusinessObject;
using System.Transactions;
using System.Data.Objects;

namespace SMAS.Business.Business
{
    public partial class PromotionBusiness
    {
        public PromotionProgramBO GetPromotionProgramByProvinceId(int provinceId, int unitId)
        {

            DateTime dateNow = DateTime.Now.Date;
            var iqPromotion = from pp in this.All
                              join ppd in this.PromotionDetailBusiness.All on pp.PROMOTION_ID equals ppd.PROMOTION_ID
                              where ppd.PROVINCE_ID == provinceId
                              && ppd.UNIT_ID == unitId
                              && pp.IS_ACTIVE
                              && ((pp.START_DATE <= dateNow && dateNow <= EntityFunctions.TruncateTime(pp.END_DATE) && pp.END_DATE.HasValue)
                                || (pp.START_DATE <= dateNow && !pp.END_DATE.HasValue))
                              select new PromotionProgramBO
                              {
                                  CreateTime = pp.CREATE_TIME,
                                  CreateTimeDetail = ppd.CREATE_TIME,
                                  EndDate = pp.END_DATE,
                                  FromOfExternalSMS = pp.FROMOF_EXTERNAL_SMS,
                                  FromOfInternalSMS = pp.FROMOF_INTERNAL_SMS,
                                  IsActive = pp.IS_ACTIVE,
                                  IsFreeSMS = pp.IS_FREE_SMS,
                                  IsFreeExSMS = pp.IS_FREE_EX_SMS,
                                  Note = pp.NOTE,
                                  PromotionDetailID = ppd.SMS_PROMOTION_DETAIL_ID,
                                  PromotionID = pp.PROMOTION_ID,
                                  PromotionName = pp.PROMOTION_NAME,
                                  ProvinceID = ppd.PROVINCE_ID,
                                  StartDate = pp.START_DATE,
                                  UpdateTime = pp.UPDATE_TIME,
                                  UpdateTimeDetail = ppd.UPDATE_TIME
                              };
            return iqPromotion.FirstOrDefault();
        }

        public List<PromotionProgramBO> GetLstPromotionProgram()
        {
            //lay ra danh sach chuong trình khuyến mại
            List<PromotionProgramBO> lstPromotionProgram = (from s in this.All
                                                            //join d in this.Context.PromotionDetail on s.PromotionID equals d.PromotionID
                                                            where s.IS_ACTIVE == true
                                                            select new PromotionProgramBO
                                                            {
                                                                CreateTime = s.CREATE_TIME,
                                                                //CreateTimeDetail = s.PromotionDetail != null ? s.PromotionDetail.Cr : null,
                                                                EndDate = s.END_DATE,
                                                                FromOfExternalSMS = s.FROMOF_EXTERNAL_SMS,
                                                                FromOfInternalSMS = s.FROMOF_INTERNAL_SMS,
                                                                IsActive = s.IS_ACTIVE,
                                                                Note = s.NOTE,
                                                                //PromotionDetailID = d.PromotionDetailID,
                                                                PromotionID = s.PROMOTION_ID,
                                                                PromotionName = s.PROMOTION_NAME,
                                                                IsFreeSMS = s.IS_FREE_SMS,
                                                                IsFreeExSMS = s.IS_FREE_EX_SMS,
                                                                //ProvinceID = d.ProvinceID,
                                                                StartDate = s.START_DATE,

                                                            }).ToList();

            return lstPromotionProgram;
        }

        public IQueryable<PromotionProgramDetailBO> GetlistSchool(Dictionary<string, object> dic)
        {
            int ProvinceID = Utils.GetInt(dic, "ProvinceID");
            int DistrictID = Utils.GetInt(dic, "DistrictID");
            string SchoolName = Utils.GetString(dic, "SchoolName");
            string Account = Utils.GetString(dic, "Account");
            IQueryable<PromotionProgramDetailBO> iquery = (from sp in this.SchoolProfileBusiness.All
                                                           select new PromotionProgramDetailBO
                                                           {
                                                               UnitID = sp.SchoolProfileID,
                                                               UnitName = sp.SchoolName,
                                                               ProvinceID = sp.ProvinceID,
                                                               ProvinceName = sp.Province.ProvinceName,
                                                               DistrictID = sp.DistrictID,
                                                               DistrictName = sp.District.DistrictName,
                                                               Account = sp.UserAccount.aspnet_Users.UserName
                                                           });
            if (ProvinceID != 0)
            {
                iquery = iquery.Where(p => p.ProvinceID == ProvinceID);
            }

            if (DistrictID != 0)
            {
                iquery = iquery.Where(p => p.DistrictID == DistrictID);
            }

            if (!string.IsNullOrEmpty(SchoolName))
            {
                iquery = iquery.Where(p => p.UnitName.ToUpper().Contains(SchoolName.ToUpper()));
            }

            if (!string.IsNullOrEmpty(Account))
            {
                iquery = iquery.Where(p => p.Account.ToUpper().Contains(Account.ToUpper()));
            }
            return iquery;
        }

        public IQueryable<PromotionProgramDetailBO> GetlistSupervisingDept(Dictionary<string, object> dic)
        {
            int ProvinceID = Utils.GetInt(dic, "ProvinceID");
            int DistrictID = Utils.GetInt(dic, "DistrictID");
            string SchoolName = Utils.GetString(dic, "SchoolName");
            string Account = Utils.GetString(dic, "Account");
            IQueryable<PromotionProgramDetailBO> iquery = (from s in this.SupervisingDeptBusiness.All
                                                           select new PromotionProgramDetailBO
                                                           {
                                                               UnitID = s.SupervisingDeptID,
                                                               UnitName = s.SupervisingDeptName,
                                                               ProvinceID = s.ProvinceID,
                                                               ProvinceName = s.Province.ProvinceName,
                                                               DistrictID = s.DistrictID,
                                                               DistrictName = s.District.DistrictName,
                                                               Account = s.UserAccount.aspnet_Users.UserName
                                                           });
            if (ProvinceID != 0)
            {
                iquery = iquery.Where(p => p.ProvinceID == ProvinceID);
            }

            if (DistrictID != 0)
            {
                iquery = iquery.Where(p => p.DistrictID == DistrictID);
            }

            if (!string.IsNullOrEmpty(SchoolName))
            {
                iquery = iquery.Where(p => p.UnitName.ToUpper().Contains(SchoolName.ToUpper()));
            }

            if (!string.IsNullOrEmpty(Account))
            {
                iquery = iquery.Where(p => p.Account.ToUpper().Contains(Account.ToUpper()));
            }

            return iquery;
        }

        public string Insert(PromotionProgramBO obj, string lstProvince, ref long PromotionID)
        {
            SMS_PROMOTION currentEntity = new SMS_PROMOTION();
            currentEntity.PROMOTION_NAME = obj.PromotionName;
            currentEntity.START_DATE = obj.StartDate;
            currentEntity.NOTE = obj.Note;
            currentEntity.END_DATE = obj.EndDate;
            currentEntity.CREATE_TIME = DateTime.Now;
            currentEntity.UPDATE_TIME = DateTime.Now;
            currentEntity.IS_ACTIVE = true;
            currentEntity.IS_FREE_SMS = obj.IsFreeSMS.GetValueOrDefault();
            currentEntity.IS_FREE_EX_SMS = obj.IsFreeExSMS.GetValueOrDefault();
            currentEntity.FROMOF_EXTERNAL_SMS = obj.FromOfExternalSMS;
            currentEntity.FROMOF_INTERNAL_SMS = obj.FromOfInternalSMS;

            this.Insert(currentEntity);
            this.Save();
            PromotionID = currentEntity.PROMOTION_ID;
            return "";
        }

        public string Update(PromotionProgramBO obj, string lstProvince)
        {
            SMS_PROMOTION currentEntity = this.All.FirstOrDefault(p => p.PROMOTION_ID == obj.PromotionID && p.PROMOTION_ID > 0);
            if (currentEntity != null)
            {
                currentEntity.PROMOTION_NAME = obj.PromotionName;
                currentEntity.START_DATE = obj.StartDate;
                currentEntity.NOTE = obj.Note;
                currentEntity.END_DATE = obj.EndDate;
                currentEntity.UPDATE_TIME = DateTime.Now;
                currentEntity.FROMOF_EXTERNAL_SMS = obj.FromOfExternalSMS;
                currentEntity.FROMOF_INTERNAL_SMS = obj.FromOfInternalSMS;
                currentEntity.IS_FREE_SMS = obj.IsFreeSMS.GetValueOrDefault();
                currentEntity.IS_FREE_EX_SMS = obj.IsFreeExSMS.GetValueOrDefault();
                List<SMS_PROMOTION_DETAIL> lstPromotionDetail = PromotionDetailBusiness.All.Where(o => o.PROVINCE_ID == currentEntity.PROMOTION_ID).ToList();

                this.Update(currentEntity);
                this.Save();
                return "";
            }
            else
            {
                return "Chương trình khuyến mại không hợp lệ";
            }
        }

        public string DeletePromotion(int proId)
        {
            SMS_PROMOTION delItem = new SMS_PROMOTION();
            delItem = this.All.FirstOrDefault(p => p.PROMOTION_ID == proId);
            if (delItem != null)
            {
                List<SMS_PROMOTION_DETAIL> lstPromotionDetail = PromotionDetailBusiness.All.Where(o => o.PROMOTION_ID == delItem.PROMOTION_ID).ToList();

                if (DateTime.Now.Date >= delItem.START_DATE.Date)
                {
                    return "Không thể xóa chương trình khuyến mại đã áp dụng";
                }
                else
                {
                    this.PromotionDetailBusiness.DeleteAll(lstPromotionDetail);
                    this.Delete(delItem.PROMOTION_ID);

                    this.PromotionDetailBusiness.Save();
                    this.Save();
                    return "";
                }
            }
            else
            {
                return "Chương trình khuyến mại không hợp lệ";
            }
        }

        public PromotionProgramBO GetPromotionProgramById(int prId)
        {
            PromotionProgramBO res = null;
            var s = this.All.FirstOrDefault(p => p.PROMOTION_ID == prId);

            if (s != null)
            {
                res = new PromotionProgramBO();
                res.CreateTime = s.CREATE_TIME;
                //CreateTimeDetail = s.PromotionDetail != null ? s.PromotionDetail.Cr : null;
                res.EndDate = s.END_DATE;
                res.FromOfExternalSMS = s.FROMOF_EXTERNAL_SMS;
                res.FromOfInternalSMS = s.FROMOF_INTERNAL_SMS;
                res.IsActive = s.IS_ACTIVE;
                res.Note = s.NOTE;
                //PromotionDetailID = d.PromotionDetailID;
                res.PromotionID = s.PROMOTION_ID;
                res.PromotionName = s.PROMOTION_NAME;
                //ProvinceID = d.ProvinceID;
                res.IsFreeSMS = s.IS_FREE_SMS;
                res.IsFreeExSMS = s.IS_FREE_EX_SMS;
                res.StartDate = s.START_DATE;
            }

            return res;
        }
    }
}
