﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Linq.Expressions;
using System.Web;
using log4net;
using SMAS.DAL.Repository;
using SMAS.DAL.IRepository;
using SMAS.Business.IBusiness;
using SMAS.Models.Models;
using SMAS.Business.Common;
using SMAS.Business.BusinessObject;

namespace SMAS.Business.Business
{
    public partial class CommentsOfKindergartenBusiness
    {
        #region Collection Comment
        public IQueryable<CommentsOfKindergarten> SearchCommentsOfKindergarten(IDictionary<string, object> dic)
        {
            int SchoolID = Utils.GetInt(dic, "SchoolID");
            int AcademicYearID = Utils.GetInt(dic, "AcademicYearID");
            int AccountID = Utils.GetInt(dic, "AccountID");
            List<int> lstCommentsOfKindergartentID = Utils.GetIntList(dic, "lstCommentsOfKindergartenID");
            IQueryable<CommentsOfKindergarten> iquery = this.CommentsOfKindergartenBusiness.All;
            if (lstCommentsOfKindergartentID.Count > 0)
            {
                iquery = iquery.Where(p => lstCommentsOfKindergartentID.Contains(p.CommentsOfKindergartenID));
            }
            var sr = iquery.ToList();
            if (SchoolID != 0)
            {
                iquery = iquery.Where(p => p.SchoolID == SchoolID);
            }
            if (AcademicYearID != 0)
            {
                iquery = iquery.Where(p => p.AcademicYearID == AcademicYearID);
            }
           
            if (AccountID != 0)
            {
                iquery = iquery.Where(p => p.AccountID == AccountID);
            }
            return iquery;
        }
        public void InsertOrUpdateCommentsOfKindergarten(List<CommentsOfKindergarten> lstCommentsOfKindergarten, IDictionary<string, object> dic)
        {
            List<CommentsOfKindergarten> lstDB = this.SearchCommentsOfKindergarten(dic).ToList();
            List<CommentsOfKindergarten> lstInsert = new List<CommentsOfKindergarten>();
            CommentsOfKindergarten objInsert = null;
            CommentsOfKindergarten objDB = null;
            for (int i = 0; i < lstCommentsOfKindergarten.Count; i++)
            {
                objInsert = lstCommentsOfKindergarten[i];
                objDB = CommentsOfKindergartenBusiness.All.Where(t => t.CommentsOfKindergartenID == objInsert.CommentsOfKindergartenID).FirstOrDefault();
                if (objDB != null)
                {
                    objDB.CommentCode = objInsert.CommentCode;
                    objDB.Comment = objInsert.Comment;
                    objDB.AccountID = objInsert.AccountID;
                    objDB.ModifiedDate = DateTime.Now;
                    CommentsOfKindergartenBusiness.Update(objDB);
                }
                else
                {
                    objInsert.CreateDate = DateTime.Now;
                    lstInsert.Add(objInsert);
                }
            }
            if (lstInsert.Count > 0)
            {
                for (int i = 0; i < lstInsert.Count; i++)
                {
                    CommentsOfKindergartenBusiness.Insert(lstInsert[i]);
                }
            }
            CommentsOfKindergartenBusiness.Save();
        }
        #endregion
    }
}