/**
 * The Viettel License
 *
 * Copyright 2012 Viettel Telecom. All rights reserved.
 * VIETTEL PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */

/** 
* @author  
* @version $Revision: $
*/

using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Linq.Expressions;
using System.Web;
using log4net;
using SMAS.DAL.Repository;
using SMAS.DAL.IRepository;
using SMAS.Business.IBusiness;
using SMAS.Models.Models;
using SMAS.Business.Common;
using SMAS.Business.BusinessObject;

namespace SMAS.Business.Business
{
    public partial class ExamSubjectBusiness
    {
        public IQueryable<ExamSubjectBO> FindExamSubject(long ExamSubjectID)
        {
            IQueryable<ExamSubjectBO> query = (from es in ExamSubjectRepository.All
                                  join ex in ExaminationsRepository.All on es.ExaminationsID equals ex.ExaminationsID
                                  join eg in ExamGroupRepository.All on es.ExamGroupID equals eg.ExamGroupID
                                  join sc in SubjectCatRepository.All on es.SubjectID equals sc.SubjectCatID
                                  where es.ExamSubjectID == ExamSubjectID
                                  select new ExamSubjectBO
                                  {
                                      ExamSubjectID = es.ExamSubjectID,
                                      ExaminationsID = es.ExaminationsID,
                                      ExamGroupID = es.ExamGroupID,
                                      SubjectID = es.SubjectID,
                                      SubjectCode = es.SubjectCode,
                                      SchedulesExam = es.SchedulesExam,
                                      CreateTime = es.CreateTime,
                                      UpdateTime = es.UpdateTme,
                                      ExaminationsName = ex.ExaminationsName,
                                      ExamGroupName = eg.ExamGroupName,
                                      SubjectName = sc.SubjectName,
                                      Abbreviation = sc.Abbreviation
                                  });

            return query;
        }

        public IQueryable<ExamSubject> Search(IDictionary<string, object> search)
        {
            long examinationsID = Utils.GetLong(search, "ExaminationsID");
            long examGroupID = Utils.GetLong(search, "ExamGroupID");
            int subjectID = Utils.GetInt(search, "SubjectID");
            long examSubjectID = Utils.GetLong(search, "ExamSubjectID");          

            IQueryable<ExamSubject> query = this.All;
            if (examinationsID != 0)
            {
                query = query.Where(o => o.ExaminationsID == examinationsID);
            }
            if (examGroupID != 0)
            {
                query = query.Where(o => o.ExamGroupID == examGroupID);
            }
            if (subjectID != 0)
            {
                query = query.Where(o => o.SubjectID == subjectID);
            }
            if (examSubjectID != 0)
            {
                query = query.Where(o => o.ExamSubjectID == examSubjectID);
            }
           
            return query;
        }

        public IQueryable<ExamSubjectBO> GetListExamSubject(IDictionary<string, object> search)
        {
            long examinationsID = SMAS.Business.Common.Utils.GetLong(search, "ExaminationsID");
            long examGroupID = SMAS.Business.Common.Utils.GetLong(search, "ExamGroupID");
            int subjectID = SMAS.Business.Common.Utils.GetLong(search, "SubjectID");

            IQueryable<ExamSubjectBO> query = (from es in ExamSubjectRepository.All
                                               join ex in ExaminationsRepository.All on es.ExaminationsID equals ex.ExaminationsID
                                               join eg in ExamGroupRepository.All on es.ExamGroupID equals eg.ExamGroupID
                                               join sc in SubjectCatRepository.All on es.SubjectID equals sc.SubjectCatID
                                               select new ExamSubjectBO
                                               {
                                                   ExamSubjectID = es.ExamSubjectID,
                                                   ExaminationsID = es.ExaminationsID,
                                                   ExamGroupID = es.ExamGroupID,
                                                   SubjectID = es.SubjectID,
                                                   SubjectCode = es.SubjectCode,
                                                   SchedulesExam = es.SchedulesExam,
                                                   CreateTime = es.CreateTime,
                                                   UpdateTime = es.UpdateTme,
                                                   ExaminationsName = ex.ExaminationsName,
                                                   ExamGroupName = eg.ExamGroupName,
                                                   ExamGroupCode = eg.ExamGroupCode,
                                                   SubjectName = sc.SubjectName,
                                                   Abbreviation = sc.Abbreviation,
                                                   OrderInSubject = sc.OrderInSubject,
                                                   IsCommenting = sc.IsCommenting
                                               }).OrderBy(p => p.ExamGroupCode).ThenBy(p => p.OrderInSubject);

            if (examinationsID != 0)
            {
                query = query.Where(o => o.ExaminationsID == examinationsID);
            }
            if (examGroupID != 0)
            {
                query = query.Where(o => o.ExamGroupID == examGroupID);
            }
            if (subjectID != 0)
            {
                query = query.Where(o => o.SubjectID == subjectID);
            }

            return query;
        }

        private List<ExamSubject> CopyList(List<ExamSubject> listSubject, long ExamGroupID)
        {
            List<ExamSubject> newList = new List<ExamSubject>();
            ExamSubject subject = null;
            ExamSubject newSubject = null;
            for (int i = 0; i < listSubject.Count; i++)
            {
                subject = listSubject[i];
                newSubject = new ExamSubject();
                newSubject.ExamSubjectID = this.GetNextSeq<long>();
                newSubject.ExaminationsID = subject.ExaminationsID;
                newSubject.ExamGroupID = ExamGroupID;
                newSubject.SubjectID = subject.SubjectID;
                newSubject.SubjectCode = subject.SubjectCode;
                newSubject.SchedulesExam = subject.SchedulesExam;
                newSubject.CreateTime = DateTime.Now;
                newList.Add(newSubject);
            }
            return newList;
        }

        public void InsertExamSubject(List<ExamSubject> listExamSubject, int checkAll)
        {
            if (listExamSubject.Count == 0)
            {
                return;
            }
            
            if (checkAll == 0)
            {
                this.InsertListExamSubject(listExamSubject);
            }
            else
            {
                ExamSubject examSubject = listExamSubject[0];
                IDictionary<string, object> search = new Dictionary<string, object>();
                search.Add("ExaminationsID", examSubject.ExaminationsID);
                List<ExamGroup> listExamGroup = ExamGroupBusiness.Search(search).ToList();
                for (int i = 0; i < listExamGroup.Count; i++)
                {
                    if (listExamGroup[i].ExamGroupID == examSubject.ExamGroupID)
                    {
                        this.InsertListExamSubject(listExamSubject);
                    }
                    else
                    {
                        List<ExamSubject> newList = CopyList(listExamSubject, listExamGroup[i].ExamGroupID);
                        this.InsertListExamSubject(newList);
                    }
                }
            }
            this.Save();
        }

        public void UpdateExamSubject(ExamSubject examSubject, int checkAll)
        {
            if (examSubject == null)
            {
                return;
            }

            if (checkAll == 0)
            {
                this.Update(examSubject);
            }
            else
            {
                IDictionary<string, object> search = new Dictionary<string, object>();
                search.Add("ExaminationsID", examSubject.ExaminationsID);
                List<ExamGroup> listExamGroup = ExamGroupBusiness.Search(search).ToList();
                search.Add("ExamGroupID", examSubject.ExamGroupID);
                //search.Add("ExamSubjectID", examSubject.ExamSubjectID);
                search.Add("SubjectID", examSubject.SubjectID);
                
                ExamSubject newSubjectInsert = null;
                for (int i = 0; i < listExamGroup.Count; i++)
                {
                    search["ExamGroupID"] = listExamGroup[i].ExamGroupID;
                   
                    // Tim danh sach mon thi
                    List<ExamSubject> listExamSubject = this.Search(search).ToList();
                    if (listExamSubject != null && listExamSubject.Count > 0)
                    {
                        //update
                        ExamSubject newSubject = null;
                        for (int j = 0; j < listExamSubject.Count; j++)
                        {
                            newSubject = listExamSubject[j];
                            newSubject.SubjectCode = examSubject.SubjectCode;
                            newSubject.SchedulesExam = examSubject.SchedulesExam;
                            this.Update(newSubject);
                        }
                    }
                    else
                    {
                        newSubjectInsert = new ExamSubject();                        
                        newSubjectInsert.ExamSubjectID = this.GetNextSeq<long>();
                        newSubjectInsert.ExamGroupID = listExamGroup[i].ExamGroupID;
                        newSubjectInsert.ExaminationsID = examSubject.ExaminationsID;
                        newSubjectInsert.SchedulesExam = examSubject.SchedulesExam;
                        newSubjectInsert.SubjectCode = examSubject.SubjectCode;
                        newSubjectInsert.SubjectID = examSubject.SubjectID;
                        newSubjectInsert.CreateTime = DateTime.Now;
                        this.Insert(newSubjectInsert);
                    }                 
                }
            }

            this.Save();
        }

        public void DeleteExamSubject(List<long> examSubjectIDList)
        {
            for (int i = 0; i < examSubjectIDList.Count; i++)
            {
                this.Delete(examSubjectIDList[i]);
            }
            this.Save();                
        }

        private void InsertListExamSubject(List<ExamSubject> listExamSubject)
        {
            if (listExamSubject == null)
            {
                return;
            }
            ExamSubject insert = null;
            ExamSubject checkUpdate = null;
            IDictionary<string, object> search = new Dictionary<string, object>()
            {
                {"ExaminationsID", listExamSubject[0].ExaminationsID},
                {"ExamGroupID", listExamSubject[0].ExamGroupID}
            };

            for (int i = 0; i < listExamSubject.Count; i++)
            {
                insert = listExamSubject[i];
                search["SubjectID"] = insert.SubjectID;
                checkUpdate = this.Search(search).FirstOrDefault();
                if (checkUpdate == null)
                {
                    this.Insert(insert);
                }
                else
                {
                    checkUpdate.SubjectCode = insert.SubjectCode;
                    checkUpdate.SchedulesExam = insert.SchedulesExam;
                    checkUpdate.UpdateTme = insert.UpdateTme;
                    this.Update(checkUpdate);
                }
            }
        }

        public List<ExamSubjectBO> GetExamSubject(IDictionary<string, object> dic, int? applieLevel)
        {
            List<ExamSubjectBO> listExamSubject = new List<ExamSubjectBO>();

            if (applieLevel.HasValue && applieLevel.Value == GlobalConstants.APPLIED_LEVEL_PRIMARY)
            {
                //Lay danh sach mon hoc cua truong theo cap
                List<int> listSubjectID = ExamSubjectBusiness.GetListExamSubject(dic).OrderBy(o => o.OrderInSubject).Select(p => p.SubjectID).ToList();
                //Danh sach cac mon can thong ke, da tru ra cac mon nhan xet
                List<SubjectCat> listSubjectCat = SchoolSubjectBusiness.GetListSubjectMarkByListSubjectID(listSubjectID);
                listExamSubject = (from o in listSubjectCat
                                   select new ExamSubjectBO
                                   {
                                       SubjectID = o.SubjectCatID,
                                       SubjectName = o.SubjectName
                                   }).Distinct().ToList();
            }
            else
            {
                listExamSubject = ExamSubjectBusiness.GetListExamSubject(dic).OrderBy(o => o.OrderInSubject).ToList()
                                                .Select(o => new ExamSubjectBO
                                                {
                                                    SubjectID = o.SubjectID,
                                                    SubjectName = o.SubjectName
                                                }).Distinct().ToList();

            }

            return listExamSubject;
        }

        private IDictionary<string, object> ColumnMappings()
        {
            Dictionary<string, object> columnMap = new Dictionary<string, object>();
            columnMap.Add("ExamSubjectID", "EXAM_SUBJECT_ID");
            columnMap.Add("ExaminationsID", "EXAMINATIONS_ID");
            columnMap.Add("ExamGroupID", "EXAM_GROUP_ID");
            columnMap.Add("SubjectID", "SUBJECT_ID");
            columnMap.Add("SubjectCode", "SUBJECT_CODE");
            columnMap.Add("SchedulesExam", "SCHEDULES_EXAM");
            columnMap.Add("CreateTime", "CREATE_TIME");
            columnMap.Add("UpdateTme", "UPDATE_TIME");
            return columnMap;
        }
    }
}