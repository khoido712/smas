﻿/**
 * The Viettel License
 *
 * Copyright 2012 Viettel Telecom. All rights reserved.
 * VIETTEL PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */

/** 
* @author  dungnt
* @version $Revision: $
*/

using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Linq.Expressions;
using System.Web;
using log4net;

using SMAS.DAL.Repository;
using SMAS.DAL.IRepository;
using SMAS.Business.IBusiness;
using SMAS.Models.Models;
using SMAS.Business.Common;

namespace SMAS.Business.Business
{
    /// <summary>
    /// Loại công việc
    /// <author>dungnt</author>
    /// <date>05/09/2012</date>
    /// </summary>
    public partial class WorkTypeBusiness
    {
        #region private member variable
        private const int RESOLUTION_MAX_LENGTH = 50;   //do dai lon nhat truong ten
        private const int DESCRIPTION_MAX_LENGTH = 400; //do dai lon nhat truong mieu ta
        #endregion



        #region insert
        /// <summary>
        /// Thêm mới Loại công việc
        /// <author>dungnt</author>
        /// <date>05/09/2012</date>
        /// </summary>
        /// <param name="insertWorkType">doi tuong can them moi</param>
        /// <returns></returns>
        public override WorkType Insert(WorkType insertWorkType)
        {

            
            //resolution rong
            Utils.ValidateRequire(insertWorkType.Resolution, "WorkType_Label_Resolution");

            Utils.ValidateMaxLength(insertWorkType.Resolution, RESOLUTION_MAX_LENGTH, "WorkType_Label_Resolution");
            Utils.ValidateMaxLength(insertWorkType.Description, DESCRIPTION_MAX_LENGTH, "WorkType_Column_Description");

            //kiem tra ton tai - theo cap - can lam lai
            this.CheckDuplicate(insertWorkType.Resolution, GlobalConstants.LIST_SCHEMA, "WorkType", "Resolution", true, 0, "WorkType_Label_Resolution");
            //this.CheckDuplicate(insertWorkType.WorkGroupTypeID.ToString(), GlobalConstants.LIST_SCHEMA, "WorkType", "AppliedLevel", true, 0, "WorkType_Label_ShoolID");
           // this.CheckDuplicateCouple(insertWorkType.Resolution, insertWorkType.WorkGroupTypeID.ToString(),
           //GlobalConstants.LIST_SCHEMA, "WorkType", "Resolution", "WorkGroupTypeID", true, 0, "WorkType_Label_Resolution");
            //kiem tra range

           

            //
            new WorkGroupTypeBusiness(null).CheckAvailable(insertWorkType.WorkGroupTypeID, "WorkType_Label_WorkGroupTypeID");


            insertWorkType.IsActive = true;

          return  base.Insert(insertWorkType);
        }
        #endregion

        #region update
        /// <summary>
        /// Cập nhật Loại công việc
        /// <author>dungnt</author>
        /// <date>05/09/2012</date>
        /// </summary>
        /// <param name="updateWorkType">doi tuong da sua</param>
        /// <returns></returns>
        public override WorkType Update(WorkType updateWorkType)
        {

            //check avai
            new WorkTypeBusiness(null).CheckAvailable(updateWorkType.WorkTypeID, "WorkType_Label_WorkTypeID");

            //resolution rong
            Utils.ValidateRequire(updateWorkType.Resolution, "WorkType_Label_Resolution");

            Utils.ValidateMaxLength(updateWorkType.Resolution, RESOLUTION_MAX_LENGTH, "WorkType_Label_Resolution");
            Utils.ValidateMaxLength(updateWorkType.Description, DESCRIPTION_MAX_LENGTH, "WorkType_Column_Description");

            //kiem tra ton tai - theo cap - can lam lai
             this.CheckDuplicate(updateWorkType.Resolution, GlobalConstants.LIST_SCHEMA, "WorkType", "Resolution", true, updateWorkType.WorkTypeID, "WorkType_Label_Resolution");

            //this.CheckDuplicateCouple(updateWorkType.Resolution, updateWorkType.WorkGroupTypeID.ToString(),
            //    GlobalConstants.LIST_SCHEMA, "WorkType", "Resolution", "WorkGroupTypeID", true, updateWorkType.WorkTypeID, "WorkType_Label_Resolution");

            // this.CheckDuplicate(updateWorkType.WorkGroupTypeID.ToString(), GlobalConstants.LIST_SCHEMA, "WorkType", "AppliedLevel", true, updateWorkType.WorkTypeID, "WorkType_Label_ShoolID");

            //kiem tra range

            

            //check 
             new WorkGroupTypeBusiness(null).CheckAvailable(updateWorkType.WorkGroupTypeID, "WorkType_Label_WorkGroupTypeID");

            //Loi pham quy che thi khong thuoc truong truyen vao
            //tu WorkTypeID ->vao csdl lay WorkType tuong ung roi 
            //so sanh voi updateWorkType.WorkGroupTypeID


            //.................



            updateWorkType.IsActive = true;

           return base.Update(updateWorkType);
        }
        #endregion

        #region Delete
        /// <summary>
        /// Xóa Loại công việc
        /// <author>dungnt</author>
        /// <date>05/09/2012</date>
        /// </summary>
        /// <param name="WorkTypeId">id doi tuong can xoa</param>
        public void Delete(int WorkTypeId)
        {
            //check avai
            new WorkTypeBusiness(null).CheckAvailable(WorkTypeId, "WorkType_Label_WorkTypeID");

            //da su dung hay chua
            this.CheckConstraints(GlobalConstants.LIST_SCHEMA, "WorkType", WorkTypeId, "WorkType_Label_WorkTypeIDFailed");

            base.Delete(WorkTypeId,true);

            //Loi pham quy che thi khong thuoc truong truyen vao
            //tu WorkTypeID ->vao csdl lay WorkType tuong ung roi 
            //so sanh voi updateWorkType.WorkGroupTypeID


            //.................

        }
        #endregion

        #region Search
        /// <summary>
        /// Tìm kiếm Loại công việc
        /// <author>dungnt</author>
        /// <date>05/09/2012</date>
        /// </summary>
        /// <param name="dic">tham so doi tuong tim kiem</param>
        /// <returns>list doi tuong tim thay</returns>
        public IQueryable<WorkType> Search(IDictionary<string, object> dic)
        {
            string resolution = Utils.GetString(dic,"Resolution");
            int SchoolID = Utils.GetInt(dic, "SchoolID");
            string description = Utils.GetString(dic,"Description");
            int WorkGroupTypeID = Utils.GetInt(dic,"WorkGroupTypeID");
            bool? isActive = Utils.GetIsActive(dic,"IsActive");
            IQueryable<WorkType> lsWorkType = this.WorkTypeRepository.All;


            if (SchoolID != 0)
            {
                lsWorkType = lsWorkType.Where(o => o.SchoolID == SchoolID);
            }

             if (isActive.HasValue)
            {
            lsWorkType = lsWorkType.Where(em => (em.IsActive == isActive));
            }
            

            if (resolution.Trim().Length != 0)
            {

                lsWorkType = lsWorkType.Where(em => (em.Resolution.ToUpper().Contains(resolution.ToUpper())));
            }

            if (WorkGroupTypeID != 0)
            {
                lsWorkType = lsWorkType.Where(em => (em.WorkGroupTypeID == WorkGroupTypeID));
            }

            if (description.Trim().Length != 0)
            {

                lsWorkType = lsWorkType.Where(em => (em.Description.ToUpper().Contains(description.ToUpper())));
            }
          
            return lsWorkType;
        }
        #endregion
        
    }
}