/**
 * The Viettel License
 *
 * Copyright 2012 Viettel Telecom. All rights reserved.
 * VIETTEL PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */

/** 
* @author  
* @version $Revision: $
*/

using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Linq.Expressions;
using System.Web;
using log4net;

using SMAS.DAL.Repository;
using SMAS.DAL.IRepository;
using SMAS.Business.IBusiness;
using SMAS.Models.Models;
using SMAS.Business.Common;
using SMAS.Business.BusinessObject;
using System.Transactions;

namespace SMAS.Business.Business
{
    public partial class ColumnDescriptionBusiness
    {
        public IQueryable<ColumnDescription> Search(IDictionary<string, object> dic) {

            int ReportType = Utils.GetInt(dic, "ReportType");
            string TableName = Utils.GetString(dic, "TableName");
            List<int> listReportType = Utils.GetIntList(dic, "listReportType");

            IQueryable<ColumnDescription> query = ColumnDescriptionRepository.All;

            if (ReportType > 0)
            {
                query = query.Where(x => x.ReportType == ReportType);
            }

            if (listReportType.Count() > 0)
            {
                query = query.Where(x => x.ReportType.HasValue && listReportType.Contains(x.ReportType.Value));
            }

            if (!string.IsNullOrEmpty(TableName))
            {
                query = query.Where(x => x.TableName == TableName);
            }


            return query.OrderBy(p => p.OrderID);
        }
    }
}
