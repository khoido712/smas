using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SMAS.Business.Common;
using SMAS.Models.Models;
using System.IO;
using SMAS.Business.Business;
using SMAS.Business.BusinessObject;
using SMAS.VTUtils.Excel.Export;
using SMAS.DAL.Repository;

namespace SMAS.Business.Business
{
    public partial class ReportSynthesisCapacityConductByPeriodBusiness
    {
        public const string REPORT_CODE = SystemParamsInFile.REPORT_THONG_KE_TONG_HOP_HOC_LUC_HANH_KIEM_THEO_DOT;

        // ============= Private classes for private businesses ====================
        private class ReportData
        {
            public int EducationLevelID { get; set; }
            public int? CapacityLevelID { get; set; }
            public int Count { get; set; }
        }

        private class EducationLevelInfo
        {
            public int Index { get; set; }
            public int EducationLevelID { get; set; }
            public string EducationLevelName { get; set; }
            public int NumberOfPupil { get; set; }
            public Dictionary<string, object> CapacityInfoHL { get; set; }
            public Dictionary<string, object> CapacityInfoHK { get; set; }
            public EducationLevelInfo()
            {
                CapacityInfoHL = new Dictionary<string, object>();
                CapacityInfoHK = new Dictionary<string, object>();
            }
        }

        private WorkBookData BuildReportData(TranscriptOfClass Entity, List<StatisticsConfig> ListConfigHL, List<StatisticsConfig> ListConfigHK,
            List<EducationLevel> ListEducationLevel, List<ReportData> ListCount)
        {
            WorkBookData WorkBookData = new WorkBookData();

            SheetData SheetData = new SheetData("Report");
            List<ClassProfile> lstClass = new List<ClassProfile>();
            Dictionary<string, object> dic = new Dictionary<string, object>();
            dic["AcademicYearID"] = Entity.AcademicYearID;
            dic["AppliedLevel"] = Entity.AppliedLevel;
            IQueryable<ClassProfile> lsClass = ClassProfileBusiness.SearchBySchool(Entity.SchoolID, dic);
            IEnumerable<int> lstClassID = new List<int>();
            // Nhom du lieu theo cap hoc
            List<object> ListEduInfo = new List<object>();
            int index = 1;
            int NumberOfPupil = 0;
            for (int i = 0; i < ListEducationLevel.Count; i++)
            {
                NumberOfPupil = 0;
                EducationLevelInfo EduInfo = new EducationLevelInfo();
                EduInfo.Index = index++;
                EduInfo.EducationLevelID = ListEducationLevel[i].EducationLevelID;
                EduInfo.EducationLevelName = ListEducationLevel[i].Resolution;
                lstClassID = lsClass.Where(o => o.EducationLevelID == EduInfo.EducationLevelID).Select(o => o.ClassProfileID);
                if (lstClassID.Count() > 0)
                {
                    
                    dic = new Dictionary<string, object>();
                    dic["AcademicYearID"] = Entity.AcademicYearID;
                    dic["AppliedLevel"] = Entity.AppliedLevel;
                    dic["Check"] = "Checked";
                    dic["Status"] = SystemParamsInFile.PUPIL_STATUS_STUDYING.ToString() + "," + SystemParamsInFile.PUPIL_STATUS_GRADUATED.ToString();
                    IQueryable<PupilOfClass> lsPupil = PupilOfClassBusiness.SearchBySchool(Entity.SchoolID, dic).Where(o => lstClassID.Contains(o.ClassID));
                    NumberOfPupil = lsPupil.Count();
                }

                EduInfo.NumberOfPupil = NumberOfPupil;
                Dictionary<string, object> CapacityInfoHL = new Dictionary<string, object>();
                foreach (StatisticsConfig sc in ListConfigHL)
                {
                    int Capacity = int.Parse(sc.EqualValue);
                    var query = from mark in PupilRankingBusiness.All
                                where mark.SchoolID == Entity.SchoolID
                                      && mark.AcademicYearID == Entity.AcademicYearID
                                      && mark.EducationLevel.Grade == Entity.AppliedLevel
                                      && mark.PeriodID == Entity.PeriodDeclarationID
                                      && mark.CapacityLevelID == Capacity
                                      && mark.EducationLevelID == EduInfo.EducationLevelID
                                select new ReportData
                                {
                                    EducationLevelID = mark.EducationLevelID,
                                    CapacityLevelID = mark.CapacityLevelID,
                                    Count = NumberOfPupil,
                                };

                    CapacityInfoHL[sc.StatisticsConfigID.ToString()] = query.Count();


                }
                EduInfo.CapacityInfoHL = CapacityInfoHL;


                Dictionary<string, object> CapacityInfoHK = new Dictionary<string, object>();
                foreach (StatisticsConfig sc in ListConfigHK)
                {

                    int ConductLevelID = int.Parse(sc.EqualValue);
                    var query = from mark in PupilRankingBusiness.All
                                where mark.SchoolID == Entity.SchoolID
                                      && mark.AcademicYearID == Entity.AcademicYearID
                                      && mark.EducationLevel.Grade == Entity.AppliedLevel
                                      && mark.PeriodID == Entity.PeriodDeclarationID
                                      && mark.ConductLevelID == ConductLevelID
                                      && mark.EducationLevelID == EduInfo.EducationLevelID
                                select new ReportData
                                {
                                    EducationLevelID = mark.EducationLevelID,
                                    CapacityLevelID = mark.CapacityLevelID,
                                    Count = NumberOfPupil,
                                };

                    CapacityInfoHK[sc.StatisticsConfigID.ToString()] = query.Count();
                }
                EduInfo.CapacityInfoHK = CapacityInfoHK;
                ListEduInfo.Add(EduInfo);
            }

            SheetData.Data["ListEduInfo"] = ListEduInfo;
            SheetData.Data["ListConfigHL"] = ListConfigHL;
            SheetData.Data["ListConfigHK"] = ListConfigHK;

            string AcademicYear = AcademicYearBusiness.Find(Entity.AcademicYearID).DisplayTitle;
            SchoolProfile School = SchoolProfileBusiness.Find(Entity.SchoolID);
            string SchoolName = School.SchoolName;
            string ProvinceName = School.Province.ProvinceName;
            string DeptName = School.SupervisingDept.SupervisingDeptName;

            PeriodDeclaration Period = PeriodDeclarationBusiness.Find(Entity.PeriodDeclarationID);
            if (Period == null)
            {
                throw new BusinessException("Chưa chọn đợt");
            }
            string PeriodName = Period.Resolution;
            string Semester = Period.Semester == SystemParamsInFile.SEMESTER_OF_YEAR_FIRST ? "I" : "II";

            SheetData.Data["AcademicYear"] = AcademicYear;
            SheetData.Data["SchoolName"] = SchoolName.ToUpper();
            SheetData.Data["ProvinceName"] = ProvinceName;
            SheetData.Data["DeptName"] = DeptName.ToUpper();
            SheetData.Data["ReportDate"] = DateTime.Now;
            SheetData.Data["Period"] = PeriodName;
            SheetData.Data["Semester"] = Semester;

            WorkBookData.ListSheet.Add(SheetData);
            return WorkBookData;
        }


        private IVTWorkbook WriteToExcel(WorkBookData Data, string ReportCode)
        {
            ReportDefinition reportDef = ReportDefinitionBusiness.GetByCode(ReportCode);
            string templatePath = ReportUtils.GetTemplatePath(reportDef);
            IVTWorkbook oBook = VTExport.OpenWorkbook(templatePath);

            IVTWorksheet Template = oBook.GetSheet(1);
            IVTWorksheet Sheet = oBook.CopySheetToLast(oBook.GetSheet(1));
            SheetData SheetData = Data.ListSheet.FirstOrDefault();
            List<StatisticsConfig> ListConfigHL = (List<StatisticsConfig>)SheetData.Data["ListConfigHL"];
            List<StatisticsConfig> ListConfigHK = (List<StatisticsConfig>)SheetData.Data["ListConfigHK"];
            List<object> ListEduInfo = (List<object>)SheetData.Data["ListEduInfo"];

            int i = 0;
            int StartRow = 9;
            //int StartDataCell = 4;
            int StartDataRow = StartRow + 3;
            int NormalRowID = StartDataRow;
            int EduRow = StartDataRow + 2;
            int ThickRowID = StartDataRow + 1;



            foreach (StatisticsConfig config in ListConfigHL)
            {
                Sheet.CopyPaste(Template.GetRange("D9", "E13"), StartRow, i * 2 + 4);
                Sheet.GetRange(StartRow + 1, i * 2 + 4, StartRow + 1, i * 2 + 4).Value = config.DisplayName;
                Sheet.GetRange(StartDataRow, i * 2 + 4, StartDataRow, i * 2 + 4).Value = "${ListEduInfo[].CapacityInfoHL.Get(" + config.StatisticsConfigID + ")}";

                //Fill Formula
                NormalRowID = StartDataRow;
                foreach (var Edu in ListEduInfo)
                {
                    Sheet.SetFormulaValue(NormalRowID, i * 2 + 5, GetStrSumPercent(NormalRowID, i * 2 + 5));
                    NormalRowID++;
                }
                Sheet.SetFormulaValue(NormalRowID, i * 2 + 5, GetStrSumPercent(NormalRowID, i * 2 + 5));
                i++;
            }

            Sheet.GetRange(9, 4, 9, 3 + i * 2).Merge();
            int k = i;

            foreach (StatisticsConfig config in ListConfigHK)
            {
                Sheet.CopyPaste(Template.GetRange("F9", "G13"), StartRow, k * 2 + 4);
                Sheet.GetRange(StartRow + 1, k * 2 + 4, StartRow + 1, k * 2 + 4).Value = config.DisplayName;
                Sheet.GetRange(StartDataRow, k * 2 + 4, StartDataRow, k * 2 + 4).Value = "${ListEduInfo[].CapacityInfoHK.Get(" + config.StatisticsConfigID + ")}";
                Sheet.SetCellValue(StartDataRow + ListEduInfo.Count, i * 2 + 4, GetStrSum(StartDataRow, StartDataRow + ListEduInfo.Count, k * 2 + 4));
                NormalRowID = StartDataRow;
                foreach (var Edu in ListEduInfo)
                {
                    Sheet.SetFormulaValue(NormalRowID, k * 2 + 5, GetStrSumPercent(NormalRowID, k * 2 + 5));
                    NormalRowID++;
                }
                Sheet.SetFormulaValue(NormalRowID, k * 2 + 5, GetStrSumPercent(NormalRowID, k * 2 + 5));
                k++;
            }

            Sheet.GetRange(9, 5 + i * 2, 9, 6 + i + k).Merge();
            int j = StartDataRow;
            foreach (var Edu in ListEduInfo)
            {
                for (int n = 0; n < (ListConfigHL.Count + ListConfigHK.Count); n++)
                {
                    Sheet.SetCellValue(StartDataRow + ListEduInfo.Count, n * 2 + 4, GetStrSum(StartDataRow, StartDataRow + ListEduInfo.Count - 1, n * 2 + 4));

                }
                //Sheet.CopyPaste(Template.GetRange("D9", "E13"), StartRow, i * 2 + 6);
                Sheet.CopyPaste(Template.GetRange("A12", "C12"), j, 1);
                Sheet.CopyPaste(Sheet.GetRange("D12", "X12"), j, 4, true);

                j++;
            }
            Sheet.CopyPaste(Sheet.GetRange("D12", "X12"), j, 4, true);
            Sheet.CopyPaste(Template.GetRange("A13", "C13"), j, 1);
            Sheet.SetCellValue(j, 3, GetStrSum(StartDataRow, j - 1, 3));
            Sheet.FillVariableValue(SheetData.Data);
            Sheet.FitToPage = true;
            oBook.GetSheet(1).Delete();
            return oBook;
        }


        private string GetStrSum(int FromRow, int ToRow, int Col)
        {
            return string.Format("=SUM({0}:{1})", VTVector.ColumnIntToString(Col) + FromRow, VTVector.ColumnIntToString(Col) + ToRow);
        }

        private string GetStrSumPercent(int Row, int Col)
        {
            return string.Format("=CONCATENATE(ROUND({0}/IF({1}<=0;1;{1});4)*100;\"" + "%\")", VTVector.ColumnIntToString(Col - 1) + Row, VTVector.ColumnIntToString(3) + Row);
        }

        #region Lấy mảng băm cho các tham số đầu vào
        /// <summary>
        /// GetHashKey
        /// </summary>
        /// <param name="TranscriptOfClass">The transcript of class.</param>
        /// <returns>
        /// String
        /// </returns>
        /// <author>hath</author>
        /// <date>11/27/2012</date>
        public string GetHashKey(TranscriptOfClass TranscriptOfClass)
        {
            IDictionary<string, object> Dictionary = new Dictionary<string, object>();
            Dictionary["PeriodDeclarationID"] = TranscriptOfClass.PeriodDeclarationID;
            Dictionary["AppliedLevel"] = TranscriptOfClass.AppliedLevel;
            Dictionary["SchoolID"] = TranscriptOfClass.SchoolID;
            Dictionary["AcademicYearID"] = TranscriptOfClass.AcademicYearID;
            return ReportUtils.GetHashKey(Dictionary);
        }
        #endregion

        #region Lưu lại thông tin thống kê
        /// <summary>
        /// InsertResultEndOfYear
        /// </summary>
        /// <param name="entity">The entity.</param>
        /// <param name="data">The data.</param>
        /// <returns>
        /// ProcessedReport
        /// </returns>
        /// <author>hath</author>
        /// <date>11/27/2012</date>
        public ProcessedReport InsertSynthesisCapacityConductByPeriod(TranscriptOfClass entity, Stream data)
        {
            string reportCode = SystemParamsInFile.REPORT_THONG_KE_TONG_HOP_HOC_LUC_HANH_KIEM_THEO_DOT;
            ReportDefinition reportDef = ReportDefinitionBusiness.GetByCode(reportCode);
            ProcessedReport pr = new ProcessedReport();
            pr.ReportCode = reportCode;
            pr.ProcessedDate = DateTime.Now;
            pr.InputParameterHashKey = GetHashKey(entity);
            pr.ReportData = ReportUtils.Compress(data);
            
            //Tạo tên file HS_PTTH_ThongKeTongHopHL_HK_[Semester]_[Period]
            string outputNamePattern = reportDef.OutputNamePattern;
            PeriodDeclaration period = PeriodDeclarationBusiness.Find(entity.PeriodDeclarationID);
            string Period = period.Resolution;
            string schoolLevel = ReportUtils.ConvertAppliedLevelForReportName(entity.AppliedLevel);
            string semester = ReportUtils.ConvertSemesterForReportName(period.Semester.Value);
            outputNamePattern = outputNamePattern.Replace("[SchoolType]", schoolLevel);
            outputNamePattern = outputNamePattern.Replace("[Semester]", semester);
            outputNamePattern = outputNamePattern.Replace("[Period]", Period);
            pr.ReportName = ReportUtils.RemoveSpecialCharacters(outputNamePattern) + "." + reportDef.OutputFormat;



            IDictionary<string, object> dic = new Dictionary<string, object> {
                {"AcademicYearID", entity.AcademicYearID},
                {"SchoolID", entity.SchoolID},
                {"AppliedLevel", entity.AppliedLevel},
                {"PeriodDeclarationID", entity.PeriodDeclarationID},

            };
            ProcessedReportParameterRepository.Insert(dic, pr);
            ProcessedReportRepository.Insert(pr);
            ProcessedReportRepository.Save();
            return pr;
        }
        #endregion

        #region Lấy báo cáo thống kê tổng hợp mới nhất
        /// <summary>
        /// GetReportResultEndOfYear
        /// </summary>
        /// <param name="entity">The entity.</param>
        /// <returns>
        /// ProcessedReport
        /// </returns>
        /// <author>hath</author>
        /// <date>11/27/2012</date>
        public ProcessedReport GetSynthesisCapacityConductByPeriod(TranscriptOfClass entity)
        {
            string reportCode = SystemParamsInFile.REPORT_THONG_KE_TONG_HOP_HOC_LUC_HANH_KIEM_THEO_DOT;
            string inputParameterHashKey = GetHashKey(entity);
            return ProcessedReportBusiness.GetProcessedReport(reportCode, inputParameterHashKey);
        }
        #endregion

        #region Lưu lại báo cáo thống kê tổng hợp
        public Stream CreateSynthesisCapacityConductByPeriod(TranscriptOfClass Entity)
        {
            // Danh sach cau hinh hoc luc
            List<StatisticsConfig> ListConfigHL = StatisticsConfigBusiness.GetList(Entity.AppliedLevel, "HocLuc");
            List<StatisticsConfig> ListConfigHK = StatisticsConfigBusiness.GetList(Entity.AppliedLevel, "HanhKiem");

            #region Danh sach lop, si so
            // Danh sach khoi
            List<EducationLevel> ListEducationLevel = EducationLevelBusiness.GetByGrade(Entity.AppliedLevel).ToList();


            //Danh sach lop hoc
            IQueryable<ClassProfile> lsClass = ClassProfileBusiness.SearchBySchool(Entity.SchoolID, new Dictionary<string, object> {
                                                {"AppliedLevel", Entity.AppliedLevel},
                                                {"AcademicYearID", Entity.AcademicYearID},
                                                {"IsVNEN",true}
                                                }).OrderBy(o => o.DisplayName);

            IQueryable<PupilOfClass> lsPupilOfClassSemester1 = PupilOfClassBusiness.SearchBySchool(Entity.SchoolID, new Dictionary<string, object> {
                                                {"AppliedLevel", Entity.AppliedLevel},
                                                {"AcademicYearID", Entity.AcademicYearID},
                                                {"FemaleID",Entity.FemaleID},
                                                {"EthnicID",Entity.EthnicID}
                                                }).AddCriteriaSemester(AcademicYearBusiness.Find(Entity.AcademicYearID), Entity.Semester);

            IQueryable<PupilOfClass> lsPupilOfClassSemester = lsPupilOfClassSemester1.Where(o => o.AssignedDate == lsPupilOfClassSemester1.Where(u => u.PupilID == o.PupilID && u.ClassID == o.ClassID).Select(u => u.AssignedDate).Max());
            IDictionary<string, object> dicCATP = new Dictionary<string, object>()
                {
                    {"SchoolID",Entity.SchoolID},
                    {"AcademicYearID",Entity.AcademicYearID},
                    {"Semester",Entity.Semester},
                    {"Type",SystemParamsInFile.GET_NUMPUPIL_IN_CLASS_TYPE1}
                };
            IQueryable<ClassInfoBO> iqClassInfoBO = PupilOfClassBusiness.GetClassAndTotalPupil(lsClass, dicCATP);

            List<ClassInfoBO> ListClass = iqClassInfoBO.ToList();
            #endregion

            IQueryable<VPupilRanking> iqPupilRanking = VPupilRankingBusiness.SearchBySchool(Entity.SchoolID, new Dictionary<string, object> {
                                                    {"AppliedLevel", Entity.AppliedLevel},
                                                    {"AcademicYearID", Entity.AcademicYearID},
                                                    {"PeriodID", Entity.PeriodDeclarationID}
                                                }).Where(u => lsPupilOfClassSemester.Any(o => o.PupilID == u.PupilID && o.ClassID == u.ClassID))
                                                .Where(o => o.ConductLevelID != null || o.CapacityLevelID != null);


            var listMarkGroupHL = from s in ListConfigHL
                                join m in iqPupilRanking on 1 equals 1
                                where s.EqualValue == m.CapacityLevelID.ToString()
                                select new
                                {
                                    s.StatisticsConfigID,
                                    s.MinValue,
                                    s.MaxValue,
                                    s.MarkType,
                                    s.MappedColumn,
                                    m.SchoolID,
                                    m.EducationLevelID,
                                    m.Semester,
                                    m.PeriodID
                                };

            var listMarkGroupCountHL = (from g in listMarkGroupHL
                                      group g by new
                                      {
                                          g.SchoolID,
                                          g.EducationLevelID,
                                          g.StatisticsConfigID,
                                          g.MinValue,
                                          g.MaxValue,
                                          g.MarkType,
                                          g.MappedColumn,
                                          g.Semester,
                                          g.PeriodID
                                      } into c
                                      select new
                                      {
                                          c.Key.SchoolID,
                                          c.Key.EducationLevelID,
                                          c.Key.StatisticsConfigID,
                                          c.Key.MinValue,
                                          c.Key.MaxValue,
                                          c.Key.MarkType,
                                          c.Key.MappedColumn,
                                          c.Key.Semester,
                                          c.Key.PeriodID,
                                          CountMark = c.Count()
                                      }
                                     ).ToList();


            var listMarkGroupHK = from s in ListConfigHK
                                  join m in iqPupilRanking on 1 equals 1
                                  where s.EqualValue == m.ConductLevelID.ToString()
                                  select new
                                  {
                                      s.StatisticsConfigID,
                                      s.MinValue,
                                      s.MaxValue,
                                      s.MarkType,
                                      s.MappedColumn,
                                      m.SchoolID,
                                      m.EducationLevelID,
                                      m.Semester,
                                      m.PeriodID
                                  };

            var listMarkGroupCountHK = (from g in listMarkGroupHK
                                        group g by new
                                        {
                                            g.SchoolID,
                                            g.EducationLevelID,
                                            g.StatisticsConfigID,
                                            g.MinValue,
                                            g.MaxValue,
                                            g.MarkType,
                                            g.MappedColumn,
                                            g.Semester,
                                            g.PeriodID
                                        } into c
                                        select new
                                        {
                                            c.Key.SchoolID,
                                            c.Key.EducationLevelID,
                                            c.Key.StatisticsConfigID,
                                            c.Key.MinValue,
                                            c.Key.MaxValue,
                                            c.Key.MarkType,
                                            c.Key.MappedColumn,
                                            c.Key.Semester,
                                            c.Key.PeriodID,
                                            CountMark = c.Count()
                                        }
                                     ).ToList();



           
            WorkBookData WorkBookData = new WorkBookData();

            SheetData SheetData = new SheetData("Report");

            // Nhom du lieu theo cap hoc
            List<object> ListEduInfo = new List<object>();
            int index = 1;
            int NumberOfPupil = 0;
            foreach (EducationLevel edu in ListEducationLevel)
            {
                NumberOfPupil = 0;
                EducationLevelInfo EduInfo = new EducationLevelInfo();
                EduInfo.Index = index++;
                EduInfo.EducationLevelID = edu.EducationLevelID;
                EduInfo.EducationLevelName = edu.Resolution;
                NumberOfPupil = ListClass.Where(o => o.EducationLevelID == edu.EducationLevelID).Sum(o => o.ActualNumOfPupil);

                EduInfo.NumberOfPupil = NumberOfPupil;
                Dictionary<string, object> CapacityInfoHL = new Dictionary<string, object>();
                foreach (StatisticsConfig sc in ListConfigHL)
                {
                    var capacityCount = listMarkGroupCountHL.Where(o => o.EducationLevelID == edu.EducationLevelID && o.StatisticsConfigID == sc.StatisticsConfigID).FirstOrDefault();
                    CapacityInfoHL[sc.StatisticsConfigID.ToString()] = capacityCount != null ? capacityCount.CountMark : 0;
                }
                EduInfo.CapacityInfoHL = CapacityInfoHL;


                Dictionary<string, object> CapacityInfoHK = new Dictionary<string, object>();
                foreach (StatisticsConfig sc in ListConfigHK)
                {
                    var capacityCount = listMarkGroupCountHK.Where(o => o.EducationLevelID == edu.EducationLevelID && o.StatisticsConfigID == sc.StatisticsConfigID).FirstOrDefault();
                    CapacityInfoHK[sc.StatisticsConfigID.ToString()] = capacityCount != null ? capacityCount.CountMark : 0;
                }
                EduInfo.CapacityInfoHK = CapacityInfoHK;
                ListEduInfo.Add(EduInfo);
            }

            SheetData.Data["ListEduInfo"] = ListEduInfo;
            SheetData.Data["ListConfigHL"] = ListConfigHL;
            SheetData.Data["ListConfigHK"] = ListConfigHK;

            string AcademicYear = AcademicYearBusiness.Find(Entity.AcademicYearID).DisplayTitle;
            SchoolProfile School = SchoolProfileBusiness.Find(Entity.SchoolID);
            string SchoolName = School.SchoolName;
            string ProvinceName = (School.District != null ? School.District.DistrictName : "");
            string DeptName = UtilsBusiness.GetSupervisingDeptName(School.SchoolProfileID, Entity.AppliedLevel);

            PeriodDeclaration Period = PeriodDeclarationBusiness.Find(Entity.PeriodDeclarationID);
            if (Period == null)
            {
                throw new BusinessException("Chưa chọn đợt");
            }
            string PeriodName = Period.Resolution;
            string Semester = Period.Semester == SystemParamsInFile.SEMESTER_OF_YEAR_FIRST ? "I" : "II";

            SheetData.Data["AcademicYear"] = AcademicYear;
            SheetData.Data["SchoolName"] = SchoolName.ToUpper();
            SheetData.Data["ProvinceName"] = ProvinceName;
            SheetData.Data["DeptName"] = DeptName.ToUpper();
            SheetData.Data["ReportDate"] = DateTime.Now;
            SheetData.Data["Period"] = PeriodName;
            SheetData.Data["Semester"] = Semester;

            WorkBookData.ListSheet.Add(SheetData);

            IVTWorkbook oBook = this.WriteToExcel(WorkBookData, REPORT_CODE);
            return oBook.ToStream();

        }

        #endregion
    }
}
