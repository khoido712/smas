﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SMAS.Business.BusinessObject;
using SMAS.Business.Business;
using SMAS.Business.Common;
using SMAS.Models.Models;
using System.IO;
using SMAS.VTUtils.Excel.Export;
using SMAS.DAL.Repository;

namespace SMAS.Business.Business
{
    public partial class DetailClassifiedBySubjectBusiness
    {

        #region Lấy mảng băm cho các tham số đầu vào của thống kê xếp loại chi tiết theo môn
        /// <summary>
        /// Lấy mảng băm cho các tham số đầu vào của thống kê xếp loại chi tiết theo môn
        /// </summary>
        /// <param name="entity"></param>
        /// <returns>tungnd</returns>
        public string GetHashKey(DetailClassifiedBySubjectBO entity)
        {
            IDictionary<string, object> dic = new Dictionary<string, object> {
                {"AcademicYearID", entity.AcademicYearID},
                {"EducationLevelID ", entity.EducationLevelID},
                {"Semester", entity.Semester},
                {"AppliedLevel", entity.AppliedLevel}
            };
            return ReportUtils.GetHashKey(dic);
        }
        #endregion

        #region Lấy sổ gọi tên và ghi điểm được cập nhật mới nhất
        /// <summary>
        /// Lấy sổ gọi tên và ghi điểm được cập nhật mới nhất
        /// </summary>
        /// <param name="entity"></param>
        /// <returns>tungnd</returns>
        public ProcessedReport GetDetailClassifiedBySubject(DetailClassifiedBySubjectBO entity)
        {
            string reportCode = SystemParamsInFile.REPORT_TK_XEP_LOAI_CHITIET_MON;

            string inputParameterHashKey = GetHashKey(entity);
            return ProcessedReportBusiness.GetProcessedReport(reportCode, inputParameterHashKey);
        }
        #endregion


        #region Lưu lại thông tin thống kê xếp loại chi tiết theo môn
        /// <summary>
        /// Lưu lại thông tin thống kê xếp loại chi tiết theo môn
        /// </summary>
        /// <param name="entity"></param>
        /// <returns>tungnd</returns>
        public ProcessedReport InsertDetailClassifiedBySubject(DetailClassifiedBySubjectBO entity, Stream data)
        {
            string reportCode = SystemParamsInFile.REPORT_TK_XEP_LOAI_CHITIET_MON;

            ReportDefinition reportDef = ReportDefinitionBusiness.GetByCode(reportCode);
            ProcessedReport pr = new ProcessedReport();
            pr.ReportCode = reportCode;
            pr.ProcessedDate = DateTime.Now;
            pr.InputParameterHashKey = GetHashKey(entity);
            pr.ReportData = ReportUtils.Compress(data);

            //Tạo tên file HS_PTTH_XepLoaiChiTietTheoMon_[Khối]_[Học Kỳ]
            string outputNamePattern = reportDef.OutputNamePattern;
            string semester = ReportUtils.ConvertSemesterForReportName(entity.Semester);
            string EducationLevel = entity.EducationLevelID != 0 ? this.EducationLevelBusiness.Find(entity.EducationLevelID).Resolution : "Tất cả";
            outputNamePattern = outputNamePattern.Replace("[SchoolLevel]", ReportUtils.ConvertAppliedLevelForReportName(entity.AppliedLevel));
            outputNamePattern = outputNamePattern.Replace("[Semester]", ReportUtils.StripVNSign(semester));
            outputNamePattern = outputNamePattern.Replace("[EducationLevel]", ReportUtils.StripVNSign(EducationLevel));
            pr.ReportName = ReportUtils.RemoveSpecialCharacters(outputNamePattern) + "." + reportDef.OutputFormat;

            IDictionary<string, object> dic = new Dictionary<string, object> {
                {"EducationLevelID", entity.EducationLevelID},
                {"AcademicYearID ", entity.AcademicYearID},
                {"Semester", entity.Semester},
                {"AppliedLevel", entity.AppliedLevel}
            };

            ProcessedReportParameterRepository.Insert(dic, pr);
            ProcessedReportRepository.Insert(pr);
            ProcessedReportRepository.Save();
            return pr;
        }
        #endregion


        #region Lưu lại thông tin thống kê xếp loại chi tiết theo môn
        /// <summary>
        /// Lưu lại thông tin thống kê xếp loại chi tiết theo môn
        /// </summary>
        /// <param name="entity"></param>
        /// <returns>tungnd</returns>
        public Stream CreateDetailClassifiedBySubject(DetailClassifiedBySubjectBO entity)
        {
            string reportCode = SystemParamsInFile.REPORT_TK_XEP_LOAI_CHITIET_MON;
            ReportDefinition reportDef = ReportDefinitionBusiness.GetByCode(reportCode);
            SchoolProfile objSP = SchoolProfileBusiness.Find(entity.SchoolID);
            string SemesterName = "";
            string SchoolName = objSP.SchoolName.ToUpper();
            string AcademicYear = this.AcademicYearBusiness.Find(entity.AcademicYearID).DisplayTitle;
            string Suppervising = UtilsBusiness.GetSupervisingDeptName(objSP.SchoolProfileID, entity.AppliedLevel).ToUpper();
            string Province = (objSP.District != null ? objSP.District.DistrictName : "");

            if (entity.Semester == SystemParamsInFile.SEMESTER_OF_YEAR_FIRST)
            {
                SemesterName = "HỌC KỲ I";
            }
            if (entity.Semester == SystemParamsInFile.SEMESTER_OF_YEAR_SECOND)
            {
                SemesterName = "HỌC KỲ II";
            }
            if (entity.Semester == SystemParamsInFile.SEMESTER_OF_YEAR_ALL)
            {
                SemesterName = "CẢ NĂM";
            }
            string PupilName = string.Empty;
            if (entity.EthnicID > 0 && entity.FemaleID > 0)
            {
                PupilName = "HỌC SINH NỮ DÂN TỘC";
            }
            else if (entity.EthnicID > 0)
            {
                PupilName = "HỌC SINH DÂN TỘC";
            }
            else if (entity.FemaleID > 0)
            {
                PupilName = "HỌC SINH NỮ";
            }

            string templatePath = SystemParamsInFile.TEMPLATE_FOLDER + reportDef.TemplateDirectory + "/" + reportDef.TemplateName;
            IVTWorkbook oBook = VTExport.OpenWorkbook(templatePath);

            //Lấy sheet template
            IVTWorksheet firstSheet = oBook.GetSheet(1);
            IVTRange range = firstSheet.GetRange("A11", "P11");

            // Lấy danh sách các lớp trong khối
            IDictionary<string, object> Dic_ClassProfile = new Dictionary<string, object> {
                                                                    {"AcademicYearID", entity.AcademicYearID},
                                                                    {"SchoolID", entity.SchoolID},
                                                                    {"EducationLevelID", entity.EducationLevelID },
                                                                    {"AppliedLevel", entity.AppliedLevel },
                                                                };
            List<ClassProfile> lstClass = this.ClassProfileBusiness.SearchBySchool(entity.SchoolID, Dic_ClassProfile).OrderBy(o => o.EducationLevelID).ThenBy(p => p.OrderNumber).ThenBy(o => o.DisplayName).ToList();
            AcademicYear Aca = AcademicYearBusiness.Find(entity.AcademicYearID);
            Dictionary<string, object> dicPupilRanking = new Dictionary<string, object>
            {
                {"AcademicYearID", entity.AcademicYearID},
                {"SchoolID", entity.SchoolID},
                {"Semester", entity.Semester},
                {"AppliedLevel", entity.AppliedLevel},
                {"FemaleID",entity.FemaleID},
                {"EthnicID",entity.EthnicID}
            };

            IQueryable<PupilOfClass> lstQPoc = PupilOfClassBusiness.SearchBySchool(entity.SchoolID, dicPupilRanking).AddCriteriaSemester(Aca, entity.Semester);
            
            int partitionId = UtilsBusiness.GetPartionId(entity.SchoolID);
            var tmpPOC = (from p in lstQPoc
                          join cs in ClassSubjectBusiness.All on p.ClassID equals cs.ClassID
                          where cs.Last2digitNumberSchool == partitionId
                          && (cs.IsSubjectVNEN == null || cs.IsSubjectVNEN == false)
                          select new PupilOfClassBO
                          {
                              PupilID = p.PupilID,
                              ClassID = p.ClassID,
                              SubjectID = cs.SubjectID,
                              AppliedType = cs.AppliedType,
                              IsSpecialize = cs.IsSpecializedSubject,
                              EducationLevelID = cs.ClassProfile.EducationLevelID
                          });

            //loc bo nhung hoc sinh khong dang ky mon chuyen mon tu chon
            IDictionary<string, object> dicRegis = new Dictionary<string, object>()
            {
                {"SchoolID",entity.SchoolID},
                {"AcademicYearID",entity.AcademicYearID},
                //{"SubjectID",entity.SubjectID},
                {"SemesterID",entity.Semester},
                {"YearID",AcademicYearBusiness.Find(entity.AcademicYearID).Year}
            };
            List<PupilOfClassBO> lstPOC = this.GetPupilOfClassInSpecialize(tmpPOC, dicRegis);

            //Danh sach hoc sinh mien giam
            List<ExemptedSubject> lstExempSubject = ExemptedSubjectBusiness.GetExmpSubjectbyAcademicYear(entity.AcademicYearID, entity.Semester);

            if (lstExempSubject != null && lstExempSubject.Count > 0)
            {
                lstPOC = lstPOC.Where(p => !lstExempSubject.Exists(s => s.PupilID == p.PupilID && p.ClassID == s.ClassID && s.SubjectID == p.SubjectID)).ToList();
            }

            int StatisticLevelReportID = entity.StatisticLevelReportID;
            List<StatisticLevelConfig> lstStatisticLevel = StatisticLevelConfigBusiness.Search(new Dictionary<string, object>() { { "StatisticLevelReportID", StatisticLevelReportID } }).OrderBy(o => o.OrderNumber).ToList();

            lstStatisticLevel.AddRange(new List<StatisticLevelConfig>() {
                    new StatisticLevelConfig{
                        Title="Đ",
                        StatisticLevelConfigID=-1
                    },
                    new StatisticLevelConfig
                    {
                        Title="CĐ",
                        StatisticLevelConfigID=-2
                    }
                });

            int numberConfig = lstStatisticLevel.Count();
            int levelColumn = 4;
            int levelRow = 9;
            firstSheet.SetCellValue("C9", "Số HS (*)");           
            firstSheet.MergeColumn(levelColumn - 1, levelRow, levelRow + 1);
            firstSheet.GetRange(levelRow, levelColumn - 1, levelRow + 1, levelColumn - 1).WrapText();
            firstSheet.GetRange(levelRow, levelColumn - 1, levelRow + 1, levelColumn - 1).SetHAlign(VTHAlign.xlHAlignCenter);

            for (int i = 0; i < numberConfig; i++)
            {
                firstSheet.SetCellValue(levelRow, levelColumn, lstStatisticLevel[i].Title);
                firstSheet.MergeRow(levelRow, levelColumn, levelColumn + 1);
                firstSheet.SetCellValue(levelRow + 1, levelColumn, "SL");
                firstSheet.SetCellValue(levelRow + 1, levelColumn + 1, "%");
                levelColumn = levelColumn + 2;
            }

            IVTRange headerRange = firstSheet.GetRange(levelRow, 1, levelRow + 1, 3 + numberConfig * 2);
            headerRange.FillColor(System.Drawing.Color.DarkGray);

            headerRange.SetBorder(VTBorderStyle.Solid, VTBorderWeight.Thin, VTBorderIndex.All);

            //Neu la hoc ki 1
            if (entity.Semester == SystemParamsInFile.SEMESTER_OF_YEAR_FIRST)
            {
                IQueryable<VSummedUpRecord> lstSummedUpRecordAllStatus = this.VSummedUpRecordBusiness
                                                                        .SearchBySchool(entity.SchoolID, Dic_ClassProfile)
                                                                        .Where(o => o.PeriodID == null && o.Semester == entity.Semester);

                //Danh sach diem hoc ki 1
                //Chi lay cac diem thi cua hoc sinh dang hoc trong ky
                List<VSummedUpRecord> lstSummedUpRecordEdu = (from u in lstSummedUpRecordAllStatus.ToList()
                                                              join l in lstPOC on new { u.ClassID, u.PupilID, u.SubjectID } equals new { l.ClassID, l.PupilID, l.SubjectID }
                                                              //where lstQPoc.Any(o => o.PupilID == u.PupilID && o.ClassID == u.ClassID)
                                                              select u).ToList();

                // Lay danh sach hoc sinh theo lop
                Dic_ClassProfile.Add("Semester", GlobalConstants.SEMESTER_OF_YEAR_FIRST);
                var listCountPupilByClass = lstQPoc.GroupBy(o => o.ClassID).Select(o => new { ClassID = o.Key, TotalPupil = o.Count() }).ToList();
                List<ClassSubjectBO> listClassSubjectEdu = this.ClassSubjectBusiness.SearchBySchool(entity.SchoolID, Dic_ClassProfile)
                    .Select(o => new ClassSubjectBO
                    {
                        ClassID = o.ClassID,
                        SubjectID = o.SubjectID,
                        DisplayName = o.SubjectCat.DisplayName,
                        OrderInSubject = o.SubjectCat.OrderInSubject,
                        IsVNEN = o.IsSubjectVNEN
                    })
                    .ToList();

                for (int i = 0; i < lstClass.Count; i++)
                {
                    int classID = lstClass[i].ClassProfileID;
                    List<VSummedUpRecord> lstSummedUpRecord = lstSummedUpRecordEdu.Where(o => o.ClassID == classID).ToList();
                    var objCountPupil = listCountPupilByClass.Where(o => o.ClassID == classID).FirstOrDefault();
                    int sumPupil = objCountPupil != null ? objCountPupil.TotalPupil : 0;

                    //Tạo sheet
                    IVTWorksheet sheet = oBook.CopySheetToLast(firstSheet);
                    //Fill dữ liệu chung cho sheet class
                    sheet.Name = ReportUtils.RemoveSpecialCharacters(lstClass[i].DisplayName);
                    sheet.SetCellValue("A2", Suppervising);
                    sheet.SetCellValue("A3", SchoolName);
                    sheet.SetCellValue("I4", Province + ", ngày " + DateTime.Now.Day + " tháng " + DateTime.Now.Month + " năm " + DateTime.Now.Year);
                    sheet.SetCellValue("A6", "BÁO CÁO XẾP LOẠI CHI TIẾT THEO MÔN LỚP " + lstClass[i].DisplayName + " " + PupilName);
                    sheet.SetCellValue("A7", SemesterName + " Năm học " + AcademicYear);
                    sheet.SetCellValue("P8", sumPupil);

                    int startrow = 11;
                    List<ClassSubjectBO> lstSubject = listClassSubjectEdu.Where(o => o.ClassID == lstClass[i].ClassProfileID).OrderBy(o => o.OrderInSubject).ToList();
                    for (int j = 0; j < lstSubject.Count; j++)
                    {
                        ClassSubjectBO cs = lstSubject[j];
                        List<VSummedUpRecord> lstSummedUpRecordBySubject = lstSummedUpRecord.Where(o => o.SubjectID == cs.SubjectID).ToList();
                        if (lstExempSubject != null && lstExempSubject.Count > 0)
                        {
                            lstSummedUpRecordBySubject = lstSummedUpRecordBySubject.Where(p => !lstExempSubject.Exists(s => s.PupilID == p.PupilID && p.ClassID == s.ClassID && s.SubjectID == p.SubjectID)).ToList();
                        }

                        int totalPupilOfSubject = lstPOC.Where(p => p.ClassID == classID
                                                                && p.SubjectID == cs.SubjectID
                                                           ).Select(p => p.PupilID).Distinct().Count();
                        // fill dữ liệu các môn vào excel
                        if (j > 0)
                            sheet.CopyAndInsertARow(range, startrow);

                        sheet.SetCellValue(startrow, 1, j + 1);
                        sheet.SetCellValue(startrow, 2, lstSubject[j].DisplayName);
                        sheet.SetCellValue(startrow, 3, totalPupilOfSubject);

                        int SL = 0;
                        int curColumn = 4;
                        for (int iSLevel = 0; iSLevel < numberConfig; iSLevel++)
                        {
                            if (cs.IsVNEN == null || cs.IsVNEN == false)
                            {
                                StatisticLevelConfig slc = lstStatisticLevel[iSLevel];
                                if (slc.StatisticLevelConfigID > 0)
                                {
                                    if (slc.SignMin == SystemParamsInFile.Sign_Compare.GREATER_OR_EQUAL_SIGN)
                                    {
                                        if (slc.SignMax == SystemParamsInFile.Sign_Compare.SMALLER_OR_EQUAL_SIGN)
                                        {
                                            SL = lstSummedUpRecordBySubject.Where(o => o.SummedUpMark.HasValue && o.SummedUpMark >= slc.MinValue && o.SummedUpMark <= slc.MaxValue).Count();
                                        }
                                        else if (slc.SignMax == SystemParamsInFile.Sign_Compare.SMALLER_SIGN)
                                        {
                                            SL = lstSummedUpRecordBySubject.Where(o => o.SummedUpMark.HasValue && o.SummedUpMark >= slc.MinValue && o.SummedUpMark < slc.MaxValue).Count();
                                        }
                                        else if (slc.SignMax == SystemParamsInFile.Sign_Compare.EQUAL_SIGN)
                                        {
                                            SL = lstSummedUpRecordBySubject.Where(o => o.SummedUpMark.HasValue && o.SummedUpMark >= slc.MinValue && o.SummedUpMark == slc.MaxValue).Count();
                                        }
                                        else
                                        {
                                            SL = lstSummedUpRecordBySubject.Where(o => o.SummedUpMark.HasValue && o.SummedUpMark >= slc.MinValue).Count();
                                        }
                                    }
                                    else if (slc.SignMin == SystemParamsInFile.Sign_Compare.GREATEER_THAN_SIGN)
                                    {
                                        if (slc.SignMax == SystemParamsInFile.Sign_Compare.SMALLER_OR_EQUAL_SIGN)
                                        {
                                            SL = lstSummedUpRecordBySubject.Where(o => o.SummedUpMark.HasValue && o.SummedUpMark > slc.MinValue && o.SummedUpMark <= slc.MaxValue).Count();
                                        }
                                        else if (slc.SignMax == SystemParamsInFile.Sign_Compare.SMALLER_SIGN)
                                        {
                                            SL = lstSummedUpRecordBySubject.Where(o => o.SummedUpMark.HasValue && o.SummedUpMark > slc.MinValue && o.SummedUpMark < slc.MaxValue).Count();
                                        }
                                        else if (slc.SignMax == SystemParamsInFile.Sign_Compare.EQUAL_SIGN)
                                        {
                                            SL = lstSummedUpRecordBySubject.Where(o => o.SummedUpMark.HasValue && o.SummedUpMark > slc.MinValue && o.SummedUpMark == slc.MaxValue).Count();
                                        }
                                        else
                                        {
                                            SL = lstSummedUpRecordBySubject.Where(o => o.SummedUpMark.HasValue && o.SummedUpMark > slc.MinValue).Count();
                                        }
                                    }
                                    else
                                    {
                                        if (slc.SignMax == SystemParamsInFile.Sign_Compare.SMALLER_OR_EQUAL_SIGN)
                                        {
                                            SL = lstSummedUpRecordBySubject.Where(o => o.SummedUpMark.HasValue && o.SummedUpMark <= slc.MaxValue).Count();
                                        }
                                        else if (slc.SignMax == SystemParamsInFile.Sign_Compare.SMALLER_SIGN)
                                        {
                                            SL = lstSummedUpRecordBySubject.Where(o => o.SummedUpMark.HasValue && o.SummedUpMark < slc.MaxValue).Count();
                                        }
                                        else if (slc.SignMax == SystemParamsInFile.Sign_Compare.EQUAL_SIGN)
                                        {
                                            SL = lstSummedUpRecordBySubject.Where(o => o.SummedUpMark.HasValue && o.SummedUpMark == slc.MaxValue).Count();
                                        }
                                    }
                                }
                                else
                                {
                                    SL = lstSummedUpRecordBySubject.Where(o => o.JudgementResult == slc.Title).Count();
                                }

                                sheet.SetCellValue(startrow, curColumn, SL);//E
                            }
                            else
                            {
                                sheet.SetCellValue(startrow, curColumn, 0);//E
                            }


                            sheet.SetCellValue(startrow, curColumn + 1, "=IF(" + UtilsBusiness.GetExcelColumnName(3) + startrow + ">0" + "," + "ROUND(" + UtilsBusiness.GetExcelColumnName(curColumn) + startrow + "/" + UtilsBusiness.GetExcelColumnName(3) + startrow + "*100,2),0)");
                            curColumn += 2;

                        }

                        IVTRange subjectRange = sheet.GetRange(startrow, 1, startrow, 2 + numberConfig * 2);
                        subjectRange.SetBorder(VTBorderStyle.Solid, VTBorderWeight.Thin, VTBorderIndex.EdgeBottom);


                        startrow++;
                    }
                    sheet.SetCellValue(startrow + 1, 1, "(*): Tổng số học sinh của lớp không bao gồm các học sinh được miễn giảm môn tương ứng");
                    sheet.GetRange(9, 1, 10 + lstSubject.Count, 3 + numberConfig * 2).SetBorder(VTBorderStyle.Solid, VTBorderWeight.Thin, VTBorderIndex.All);
                    sheet.FitSheetOnOnePage = true;
                }

            }

            #region Neu la hoc ky 2
            if (entity.Semester >= SystemParamsInFile.SEMESTER_OF_YEAR_SECOND)
            {
                IQueryable<VSummedUpRecord> lstSummedUpRecordAllStatus = this.VSummedUpRecordBusiness
                                                                       .SearchBySchool(entity.SchoolID, Dic_ClassProfile)
                                                                       .Where(o => o.PeriodID == null && o.Semester >= entity.Semester);
                if (entity.Semester == SystemParamsInFile.SEMESTER_OF_YEAR_SECOND)
                {
                    lstSummedUpRecordAllStatus = lstSummedUpRecordAllStatus.Where(o => o.Semester == SystemParamsInFile.SEMESTER_OF_YEAR_SECOND);
                }
                //Danh sach diem hoc ki 1
                //Chi lay cac diem thi cua hoc sinh dang hoc trong ky
                List<VSummedUpRecord> lstSummedUpRecordEdu = (from u in lstSummedUpRecordAllStatus.ToList()
                                                              join l in lstPOC on new { u.ClassID, u.PupilID, u.SubjectID } equals new { l.ClassID, l.PupilID, l.SubjectID }
                                                              //where lstQPoc.Any(o => o.PupilID == u.PupilID && o.ClassID == u.ClassID)
                                                              select u).ToList();
                lstSummedUpRecordEdu = lstSummedUpRecordEdu.Where(u => u.Semester == lstSummedUpRecordEdu.Where(v => v.PupilID == u.PupilID && v.ClassID == u.ClassID && v.SubjectID == u.SubjectID).Max(v => v.Semester)).ToList();
                // Lay danh sach hoc sinh theo lop
                var listCountPupilByClass = lstQPoc.GroupBy(o => o.ClassID).Select(o => new { ClassID = o.Key, TotalPupil = o.Count() }).ToList();
                Dic_ClassProfile.Add("Semester", entity.Semester);
                List<ClassSubjectBO> listClassSubjectEdu = this.ClassSubjectBusiness.SearchBySchool(entity.SchoolID, Dic_ClassProfile)
                    .Select(o => new ClassSubjectBO
                    {
                        ClassID = o.ClassID,
                        SubjectID = o.SubjectID,
                        DisplayName = o.SubjectCat.DisplayName,
                        OrderInSubject = o.SubjectCat.OrderInSubject,
                        IsVNEN = o.IsSubjectVNEN
                    })
                    .ToList();

                for (int i = 0; i < lstClass.Count; i++)
                {
                    int classID = lstClass[i].ClassProfileID;
                    List<VSummedUpRecord> lstSummedUpRecord = lstSummedUpRecordEdu.Where(o => o.ClassID == classID).ToList();
                    var objCountPupil = listCountPupilByClass.Where(o => o.ClassID == classID).FirstOrDefault();
                    int sumPupil = objCountPupil != null ? objCountPupil.TotalPupil : 0;

                    //Tạo sheet
                    IVTWorksheet sheet = oBook.CopySheetToLast(firstSheet);
                    //Fill dữ liệu chung cho sheet class
                    sheet.Name = ReportUtils.RemoveSpecialCharacters(lstClass[i].DisplayName);
                    sheet.SetCellValue("A2", Suppervising);
                    sheet.SetCellValue("A3", SchoolName);
                    sheet.SetCellValue("I4", Province + ", ngày " + DateTime.Now.Day + " tháng " + DateTime.Now.Month + " năm " + DateTime.Now.Year);
                    sheet.SetCellValue("A6", "BÁO CÁO XẾP LOẠI CHI TIẾT THEO MÔN LỚP " + lstClass[i].DisplayName + " " + PupilName);
                    sheet.SetCellValue("A7", SemesterName + " Năm học " + AcademicYear);
                    sheet.SetCellValue("P8", sumPupil);

                    int startrow = 11;
                    List<ClassSubjectBO> lstSubject = listClassSubjectEdu.Where(o => o.ClassID == lstClass[i].ClassProfileID).OrderBy(o => o.OrderInSubject).ToList();
                    for (int j = 0; j < lstSubject.Count; j++)
                    {
                        ClassSubjectBO cs = lstSubject[j];
                        List<VSummedUpRecord> lstSummedUpRecordBySubject = lstSummedUpRecord.Where(o => o.SubjectID == cs.SubjectID).ToList();

                        int totalPupilOfSubject = lstPOC.Where(p => p.ClassID == classID
                                                                && p.SubjectID == cs.SubjectID
                                                                ).Select(p => p.PupilID).Distinct().Count();
                        // fill dữ liệu các môn vào excel
                        if (j > 0)
                            sheet.CopyAndInsertARow(range, startrow);

                        sheet.SetCellValue(startrow, 1, j + 1);
                        sheet.SetCellValue(startrow, 2, lstSubject[j].DisplayName);
                        sheet.SetCellValue(startrow, 3, totalPupilOfSubject);

                        int SL = 0;
                        int curColumn = 4;
                        for (int iSLevel = 0; iSLevel < numberConfig; iSLevel++)
                        {
                            if (cs.IsVNEN == null || cs.IsVNEN == false)
                            {
                                StatisticLevelConfig slc = lstStatisticLevel[iSLevel];
                                if (slc.StatisticLevelConfigID > 0)
                                {
                                    if (slc.SignMin == SystemParamsInFile.Sign_Compare.GREATER_OR_EQUAL_SIGN)
                                    {
                                        if (slc.SignMax == SystemParamsInFile.Sign_Compare.SMALLER_OR_EQUAL_SIGN)
                                        {
                                            SL = lstSummedUpRecordBySubject.Where(o => o.SummedUpMark.HasValue && o.SummedUpMark >= slc.MinValue && o.SummedUpMark <= slc.MaxValue).Count();
                                        }
                                        else if (slc.SignMax == SystemParamsInFile.Sign_Compare.SMALLER_SIGN)
                                        {
                                            SL = lstSummedUpRecordBySubject.Where(o => o.SummedUpMark.HasValue && o.SummedUpMark >= slc.MinValue && o.SummedUpMark < slc.MaxValue).Count();
                                        }
                                        else if (slc.SignMax == SystemParamsInFile.Sign_Compare.EQUAL_SIGN)
                                        {
                                            SL = lstSummedUpRecordBySubject.Where(o => o.SummedUpMark.HasValue && o.SummedUpMark >= slc.MinValue && o.SummedUpMark == slc.MaxValue).Count();
                                        }
                                        else
                                        {
                                            SL = lstSummedUpRecordBySubject.Where(o => o.SummedUpMark.HasValue && o.SummedUpMark >= slc.MinValue).Count();
                                        }
                                    }
                                    else if (slc.SignMin == SystemParamsInFile.Sign_Compare.GREATEER_THAN_SIGN)
                                    {
                                        if (slc.SignMax == SystemParamsInFile.Sign_Compare.SMALLER_OR_EQUAL_SIGN)
                                        {
                                            SL = lstSummedUpRecordBySubject.Where(o => o.SummedUpMark.HasValue && o.SummedUpMark > slc.MinValue && o.SummedUpMark <= slc.MaxValue).Count();
                                        }
                                        else if (slc.SignMax == SystemParamsInFile.Sign_Compare.SMALLER_SIGN)
                                        {
                                            SL = lstSummedUpRecordBySubject.Where(o => o.SummedUpMark.HasValue && o.SummedUpMark > slc.MinValue && o.SummedUpMark < slc.MaxValue).Count();
                                        }
                                        else if (slc.SignMax == SystemParamsInFile.Sign_Compare.EQUAL_SIGN)
                                        {
                                            SL = lstSummedUpRecordBySubject.Where(o => o.SummedUpMark.HasValue && o.SummedUpMark > slc.MinValue && o.SummedUpMark == slc.MaxValue).Count();
                                        }
                                        else
                                        {
                                            SL = lstSummedUpRecordBySubject.Where(o => o.SummedUpMark.HasValue && o.SummedUpMark > slc.MinValue).Count();
                                        }
                                    }
                                    else
                                    {
                                        if (slc.SignMax == SystemParamsInFile.Sign_Compare.SMALLER_OR_EQUAL_SIGN)
                                        {
                                            SL = lstSummedUpRecordBySubject.Where(o => o.SummedUpMark.HasValue && o.SummedUpMark <= slc.MaxValue).Count();
                                        }
                                        else if (slc.SignMax == SystemParamsInFile.Sign_Compare.SMALLER_SIGN)
                                        {
                                            SL = lstSummedUpRecordBySubject.Where(o => o.SummedUpMark.HasValue && o.SummedUpMark < slc.MaxValue).Count();
                                        }
                                        else if (slc.SignMax == SystemParamsInFile.Sign_Compare.EQUAL_SIGN)
                                        {
                                            SL = lstSummedUpRecordBySubject.Where(o => o.SummedUpMark.HasValue && o.SummedUpMark == slc.MaxValue).Count();
                                        }
                                    }
                                }
                                else
                                {
                                    SL = lstSummedUpRecordBySubject.Where(o => o.JudgementResult == slc.Title).Count();
                                }

                                sheet.SetCellValue(startrow, curColumn, SL);//E
                            }
                            else
                            {
                                sheet.SetCellValue(startrow, curColumn, 0);//E
                            }

                            sheet.SetCellValue(startrow, curColumn + 1, "=IF(" + UtilsBusiness.GetExcelColumnName(3) + startrow + ">0" + "," + "ROUND(" + UtilsBusiness.GetExcelColumnName(curColumn) + startrow + "/" + UtilsBusiness.GetExcelColumnName(3) + startrow + "*100,2),0)");
                            //sheet.SetCellValue(startrow, curColumn + 1, "=IF(P8>0" + "," + "ROUND(" + UtilsBusiness.GetExcelColumnName(curColumn) + startrow + "/" + "P8*100,2),0)");
                            curColumn += 2;

                        }


                        startrow++;
                    }
                    sheet.SetCellValue(startrow + 1, 1, "(*): Tổng số học sinh của lớp không bao gồm các học sinh được miễn giảm môn tương ứng");
                    sheet.GetRange(9, 1, 10 + lstSubject.Count, 3 + numberConfig * 2).SetBorder(VTBorderStyle.Solid, VTBorderWeight.Thin, VTBorderIndex.All);
                    sheet.FitSheetOnOnePage = true;
                }

            }
            #endregion

            //Xoá sheet template

            firstSheet.Delete();
            return oBook.ToStream();
        }
        #endregion
    }
}
