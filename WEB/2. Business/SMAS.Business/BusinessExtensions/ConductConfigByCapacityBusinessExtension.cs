/**
 * The Viettel License
 *
 * Copyright 2012 Viettel Telecom. All rights reserved.
 * VIETTEL PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */

/** 
* @author  dungnt
* @version $Revision: $
*/

using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Linq.Expressions;
using System.Web;
using log4net;

using SMAS.DAL.Repository;
using SMAS.DAL.IRepository;
using SMAS.Business.IBusiness;
using SMAS.Models.Models;
using SMAS.Business.Common;

namespace SMAS.Business.Business
{
    /// <summary>
    /// cấu hình hạnh kiểm theo học lực
    /// <author>dungnt</author>
    /// <date>05/09/2012</date>
    /// </summary>
    public partial class ConductConfigByCapacityBusiness
    {

        #region insert
        /// <summary>
        /// Thêm mới cấu hình hạnh kiểm theo học lực
        /// </summary>
        /// <param name="lstConductConfigByCapacity">Đối tượng Insert</param>
        public void Insert(List<ConductConfigByCapacity> lstConductConfigByCapacity)
        {
            foreach (ConductConfigByCapacity single in lstConductConfigByCapacity)
            {
                //Trường học không tồn tại hoặc đã bị xóa – Kiểm tra SchoolID với IsActive = TRUE
                SchoolProfileBusiness.CheckAvailable(single.SchoolID, "SchoolProfile_Label_SchoolProfileID");

                //Mức hạnh kiểm không tồn tại hoặc đã bị xóa – Kiểm tra ConductLevelID
                new ConductLevelBusiness(null).CheckAvailable(single.ConductLevelID, "ConductLevel_Label_ConductLevelID");

                //Mức học lực không tồn tại hoặc đã bị xóa – Kiểm tra CapacityLevelID
                new CapacityLevelBusiness(null).CheckAvailable(single.CapacityLevelID, "CapacityLevel_Label_CapacityLevelID");

                single.IsActive = true;
                base.Insert(single);
            }
        }
        #endregion

        #region update
        /// <summary>
        /// Cập nhật cấu hình hạnh kiểm theo học lực
        /// </summary>
        /// <param name="lstConductConfigByCapacity">Đối tượng Update</param>
        public void Update(List<ConductConfigByCapacity> lstConductConfigByCapacity)
        {
            foreach (ConductConfigByCapacity single in lstConductConfigByCapacity)
            {
                //Trường học không tồn tại hoặc đã bị xóa – Kiểm tra SchoolID với IsActive = TRUE
                SchoolProfileBusiness.CheckAvailable(single.SchoolID, "SchoolProfile_Label_SchoolProfileID");

                //Mức hạnh kiểm không tồn tại hoặc đã bị xóa – Kiểm tra ConductLevelID
                new ConductLevelBusiness(null).CheckAvailable(single.ConductLevelID, "ConductLevel_Label_ConductLevelID");

                //Mức học lực không tồn tại hoặc đã bị xóa – Kiểm tra CapacityLevelID
                new CapacityLevelBusiness(null).CheckAvailable(single.CapacityLevelID, "CapacityLevel_Label_CapacityLevelID");

                //check id
                new ConductConfigByCapacityBusiness(null).CheckAvailable(single.ConductConfigByCapacityID, "ConductConfigByCapacity_Label_ConductConfigByCapacityID ");

                // single.IsActive = true;
                base.Update(single);
            }
        }
        #endregion

        #region delete
        /// <summary>
        /// Xóa cấu hình hạnh kiểm theo học lực
        /// </summary>
        /// <param name="lstConductConfigByCapacity">Danh sách cấu hình hạnh kiểm theo học lực</param>
        public void Delete(List<ConductConfigByCapacity> lstConductConfigByCapacity, int SchoolID)
        {
            foreach (ConductConfigByCapacity single in lstConductConfigByCapacity)
            {
                if (single.SchoolID != SchoolID)
                    throw new BusinessException("ConductConfigByCapacity_Label_School_Err");
                single.IsActive = false;
                single.ModifiedDate = DateTime.Now;
                base.Update(single);
            }
        }
        #endregion

        #region Search
        /// <summary>
        /// Tìm kiếm cấu hình hạnh kiểm
        /// </summary>
        /// <param name="dic">tham so tim kiem</param>
        /// <returns>Trả về Kết quả tìm kiếm</returns>
        public IQueryable<ConductConfigByCapacity> Search(IDictionary<string, object> dic)
        {
            IQueryable<ConductConfigByCapacity> lsConductConfigByCapacity = this.ConductConfigByCapacityRepository.All.Where(em => (em.IsActive == true));

            int SchoolID = Utils.GetInt(dic, "SchoolID");
            int? ConductLevelID = Utils.GetNullableInt(dic, "ConductLevelID");
            int? CapacityLevelID = Utils.GetNullableInt(dic, "CapacityLevelID");
            int? AppliedLevel = Utils.GetNullableByte(dic, "AppliedLevel");

            if (SchoolID != 0)
                lsConductConfigByCapacity = lsConductConfigByCapacity.Where(em => em.SchoolID == SchoolID);

            if (ConductLevelID.HasValue && ConductLevelID.Value > 0)
                lsConductConfigByCapacity = lsConductConfigByCapacity.Where(em => em.ConductLevelID == ConductLevelID.Value);

            if (CapacityLevelID.HasValue && CapacityLevelID.Value > 0)
                lsConductConfigByCapacity = lsConductConfigByCapacity.Where(em => em.CapacityLevelID == CapacityLevelID.Value);

            if (AppliedLevel.HasValue && AppliedLevel.Value > 0)
                lsConductConfigByCapacity = lsConductConfigByCapacity.Where(em => em.AppliedLevel == AppliedLevel.Value);

            //Mac dinh IsActive la true
            lsConductConfigByCapacity = lsConductConfigByCapacity.Where(em => em.IsActive == true);

            return lsConductConfigByCapacity;
        }
        #endregion

        #region DeleteConductConfig
        /// <summary>
        /// Xóa cấu hình hạnh kiểm theo học lực
        /// </summary>
        /// <param name="lstConductConfigByCapacity"></param>
        /// <param name="SchoolID"></param>
        public void DeleteConductConfig(List<ConductConfigByCapacity> lstConductConfigByCapacity, int SchoolID)
        {
            foreach (ConductConfigByCapacity single in lstConductConfigByCapacity)
            {
                //Kiểm tra ID của từng phần tử trong lstConductConfigByCapacity xem có thuộc vùng dữ liệu tương ứng với SchoolID không
                if (single.SchoolID != SchoolID)
                    throw new BusinessException("SchoolProfile_Label_SchoolProfileID", new List<object>());
            }
            this.Delete(lstConductConfigByCapacity);
        }
        #endregion
    }
}
