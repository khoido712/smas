﻿/**
 * The Viettel License
 *
 * Copyright 2012 Viettel Telecom. All rights reserved.
 * VIETTEL PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */

/** 
* @author  
* @version $Revision: $
*/

using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Linq.Expressions;
using System.Web;
using log4net;

using SMAS.DAL.Repository;
using SMAS.DAL.IRepository;
using SMAS.Business.IBusiness;
using SMAS.Models.Models;
using SMAS.Business.Common;


namespace SMAS.Business.Business
{ 
    public partial class OtherServiceBusiness
    {
        #region Validate
        public void CheckOtherServiceIDAvailable(int otherServiceID)
        {
            bool AvailableOtherServiceIDCompatible = this.repository.ExistsRow(GlobalConstants.LIST_SCHEMA, "OtherService",
                   new Dictionary<string, object>()
                {
                    {"OtherServiceID",otherServiceID}
                    
                }, null);
            if (!AvailableOtherServiceIDCompatible)
            {
                throw new BusinessException("Common_Validate_NotAvailable");
            }
        }
        public void CheckOtherServiceDuplicate(OtherService otherService) {
            IDictionary<string, object> expDic = null;
            if (otherService.OtherServiceID > 0)
            {
                expDic = new Dictionary<string, object>();
                expDic["OtherServiceID"] = otherService.OtherServiceID;
            }

            bool OtherServiceNameCompatible = this.repository.ExistsRow(GlobalConstants.LIST_SCHEMA, "OtherService",
                  new Dictionary<string, object>()
                {
                    {"SchoolID", otherService.SchoolID},
                    {"OtherServiceName",otherService.OtherServiceName},
                    {"IsActive",true}
                }, expDic);
            if (OtherServiceNameCompatible)
            {
                List<object> listParam = new List<object>();
                listParam.Add("OtherService_Label_OtherServiceName");
                throw new BusinessException("Common_Validate_Duplicate", listParam);
            }
            bool OtherServiceEffectdateCompatible = this.repository.ExistsRow(GlobalConstants.LIST_SCHEMA, "OtherService",
                   new Dictionary<string, object>()
                {
                    {"EffectDate",otherService.EffectDate},
                    {"SchoolID", otherService.SchoolID},
                    {"IsActive",true}
                }, expDic);
            OtherService OS = OtherServiceBusiness.All.Where(o => o.EffectDate == otherService.EffectDate && o.IsActive == true).FirstOrDefault();
            if (OtherServiceEffectdateCompatible)
            {
                string str = "Đã khai báo tiền " + OS.OtherServiceName + " với ngày áp dụng " + otherService.EffectDate.ToShortDateString();
                throw new BusinessException(str);
            }

            bool OtherServiceNameEffectdateCompatible = this.repository.ExistsRow(GlobalConstants.LIST_SCHEMA, "OtherService",
                   new Dictionary<string, object>()
                {
                    {"EffectDate",otherService.EffectDate},
                    {"OtherServiceName", otherService.OtherServiceName},
                    {"SchoolID", otherService.SchoolID},
                    {"IsActive",true}
                }, expDic);
            
            if (OtherServiceNameEffectdateCompatible)
            {
                string str = "Đã khai báo tiền " + OS.OtherServiceName + " với ngày áp dụng " + otherService.EffectDate.ToShortDateString();
                throw new BusinessException(str);
            }
        }
        #endregion
        #region Insert
        public void Insert(OtherService otherService)
        {
            ValidationMetadata.ValidateObject(otherService);

            // Check duplicate
            CheckOtherServiceDuplicate(otherService);
            
            //Select max DishInspection
            IQueryable<DishInspection> ls = DishInspectionRepository.All.Where(o => o.SchoolID == otherService.SchoolID && o.IsActive == true);
            if (ls.Count() > 0)
            {
                var max = ls.Max(o => o.InspectedDate);
                if (otherService.EffectDate.CompareTo(max) < 0)
                {
                    string str = "Ngày áp dụng phải lớn hơn hoặc bằng ngày " + max.ToShortDateString();
                    throw new BusinessException(str);
                }
            }
            otherService.IsActive = true;
            base.Insert(otherService);

        }
        #endregion
        #region Update
        public void Update(OtherService otherService)
        {
            //Check available
            CheckOtherServiceIDAvailable(otherService.OtherServiceID);
            
            // Check require, max length
            ValidationMetadata.ValidateObject(otherService);
            
            // Check duplicate
            CheckOtherServiceDuplicate(otherService);
            
            // select max DishInspection
            IQueryable<DishInspection> ls = DishInspectionRepository.All.Where(o => o.SchoolID == otherService.SchoolID && o.IsActive == true);
            if (ls.Count() > 0)
            {
                var max = ls.Max(o => o.InspectedDate);
                if (otherService.EffectDate.CompareTo(max) < 0)
                {
                    //throw new BusinessException("OtherService_Label_EffectDate");
                    string str = "Ngày áp dụng phải lớn hơn hoặc bằng ngày " + max.ToShortDateString();
                    throw new BusinessException(str);
                }
            }

            //Check not compatible
            bool OtherServiceIDCompatible = this.repository.ExistsRow(GlobalConstants.LIST_SCHEMA, "OtherService",
                   new Dictionary<string, object>()
                {
                    {"OtherServiceID",otherService.OtherServiceID},
                    {"SchoolID",otherService.SchoolID},
                    {"IsActive",true}
                }, null);
            if (!OtherServiceIDCompatible)
            {
                throw new BusinessException("Common_Validate_NotCompatible");
            }
            base.Update(otherService);
        }
        #endregion
        #region Delete
        public void Delete(int OtherServiceID, int SchoolID)
        {
            //Check Available
            CheckOtherServiceIDAvailable(OtherServiceID);

            //Check not Compatible
            bool OtherServiceIDCompatible = this.repository.ExistsRow(GlobalConstants.LIST_SCHEMA, "OtherService",
                  new Dictionary<string, object>()
                {
                    {"OtherServiceID",OtherServiceID},
                    {"SchoolID",SchoolID},
                    {"IsActive",true}
                }, null);
            if (!OtherServiceIDCompatible)
            {
                throw new BusinessException("Common_Validate_NotCompatible");
            }
            // Check Using
            this.CheckConstraints(GlobalConstants.LIST_SCHEMA, "OtherService", OtherServiceID, "OtherService_Label_OtherServiceID");
            base.Delete(OtherServiceID,true);
        }
        #endregion
        #region Search
        public IQueryable<OtherService> Search(IDictionary<string, object> dic)
        {
            string otherServiceName = Utils.GetString(dic, "OtherServiceName");
            string note = Utils.GetString(dic, "Note");
            int schoolID = Utils.GetInt(dic, "SchoolID");
            bool? isActive = true;
            IQueryable<OtherService> listOtherService = this.OtherServiceRepository.All;
            if (!otherServiceName.Equals(string.Empty))
            {
                listOtherService = listOtherService.Where(x => x.OtherServiceName.ToLower().Contains(otherServiceName.ToLower()));
            }
            if (!note.Equals(string.Empty))
            {
                listOtherService = listOtherService.Where(x => x.Note.ToLower().Contains(note.ToLower()));
            }
            if (isActive.HasValue)
            {
                listOtherService = listOtherService.Where(x => x.IsActive == true);
            }
            if (schoolID != 0)
            {
                listOtherService = listOtherService.Where(x => x.SchoolID == schoolID);
            }
            return listOtherService;
        }
        public IQueryable<OtherService> SearchBySchool(int schoolID, IDictionary<string, object> dic)
        {
            //IQueryable<OtherService> listOtherService = this.OtherServiceRepository.All;

            if (schoolID == 0)
            {
                return null;
            }
            else
            {
                if (dic == null)
                {
                    dic = new Dictionary<string, object>();
                }
                dic["SchoolID"] = schoolID;
                return Search(dic);
            }
        }
        #endregion
    }
}