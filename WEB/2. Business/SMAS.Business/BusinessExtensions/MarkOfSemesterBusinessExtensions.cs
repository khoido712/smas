﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SMAS.Business.BusinessObject;
using SMAS.Business.Business;
using SMAS.Business.Common;
using SMAS.Models.Models;
using System.IO;
using SMAS.VTUtils.Excel.Export;
using SMAS.DAL.Repository;

namespace SMAS.Business.Business
{
    public partial class MarkOfSemesterBusiness
    {
        #region Lấy mảng băm cho các tham số đầu vào của thống kê điểm thi học kỳ
        /// <summary>
        /// Lấy mảng băm cho các tham số đầu vào của bảng điểm học sinh theo kỳ
        /// </summary>
        /// <param name="entity"></param>
        /// <returns>tungnd</returns>
        public string GetHashKey(MarkOfSemesterBO entity)
        {
            IDictionary<string, object> dic = new Dictionary<string, object> {
                {"AcademicYearID", entity.AcademicYearID},
                {"SchoolID", entity.SchoolID},
                {"AppliedLevel", entity.AppliedLevel},
                {"Semester", entity.Semester},
                {"StatisticLevelReportID",entity.StatisticReportID}
            };
            return ReportUtils.GetHashKey(dic);
        }
        #endregion


        #region Lấy sổ gọi tên và ghi điểm được cập nhật mới nhất
        /// <summary>
        /// Lấy sổ gọi tên và ghi điểm được cập nhật mới nhất
        /// </summary>
        /// <param name="entity"></param>
        /// <returns>tungnd</returns>
        public ProcessedReport GetMarkOfSemester(MarkOfSemesterBO entity)
        {

            string reportCode = SystemParamsInFile.REPORT_TK_DIEMTHI_HK;
            string inputParameterHashKey = GetHashKey(entity);
            return ProcessedReportBusiness.GetProcessedReport(reportCode, inputParameterHashKey);
        }
        #endregion

        #region Lưu lại thông tin thống kê điểm thi học kỳ
        /// <summary>
        /// Lưu lại thông tin thống kê điểm thi học kỳ
        /// </summary>
        /// <param name="entity"></param>
        /// <returns>tungnd</returns>
        public ProcessedReport InsertMarkOfSemester(MarkOfSemesterBO entity, Stream data)
        {

            string reportCode = SystemParamsInFile.REPORT_TK_DIEMTHI_HK;

            ReportDefinition reportDef = ReportDefinitionBusiness.GetByCode(reportCode);
            ProcessedReport pr = new ProcessedReport();
            pr.ReportCode = reportCode;
            pr.ProcessedDate = DateTime.Now;
            pr.InputParameterHashKey = GetHashKey(entity);
            pr.ReportData = ReportUtils.Compress(data);


            //Tạo tên file HS_PTTH_ThongKeDiemThi_[Học kỳ]
            string outputNamePattern = reportDef.OutputNamePattern;
            string semester = ReportUtils.ConvertSemesterForReportName(entity.Semester);
            string appliedLevel = ReportUtils.ConvertAppliedLevelForReportName(entity.AppliedLevel);
            outputNamePattern = outputNamePattern.Replace("[SchoolLevel]", appliedLevel);
            outputNamePattern = outputNamePattern.Replace("[Semester]", ReportUtils.StripVNSign(semester));
            pr.ReportName = ReportUtils.RemoveSpecialCharacters(outputNamePattern) + "." + reportDef.OutputFormat;

            IDictionary<string, object> dic = new Dictionary<string, object> {
                {"AcademicYearID", entity.AcademicYearID},
                {"StatisticLevelReportID", entity.StatisticReportID},
                {"Semester", entity.Semester}
            };
            ProcessedReportParameterRepository.Insert(dic, pr);
            ProcessedReportRepository.Insert(pr);
            ProcessedReportRepository.Save();
            return pr;
        }
        #endregion


        #region Tạo file báo cáo thông tin bảng điểm học sinh theo kỳ
        /// <summary>
        /// Tạo file báo cáo thông tin bảng điểm học sinh theo kỳ
        /// </summary>
        /// <param name="entity"></param>
        /// <returns>tungnd</returns>
        public Stream CreateMarkOfSemester(MarkOfSemesterBO entity)
        {
            string reportCode = SystemParamsInFile.REPORT_TK_DIEMTHI_HK;
            ReportDefinition reportDef = ReportDefinitionBusiness.GetByCode(reportCode);
            string SemesterName = "";
            SchoolProfile objSP = SchoolProfileBusiness.Find(entity.SchoolID);
            string SchoolName = objSP.SchoolName.ToUpper();
            string AcademicYear = this.AcademicYearBusiness.Find(entity.AcademicYearID).DisplayTitle;
            string Suppervising = UtilsBusiness.GetSupervisingDeptName(objSP.SchoolProfileID, entity.AppliedLevel).ToUpper();
            int partitionId = UtilsBusiness.GetPartionId(entity.SchoolID);
            if (entity.Semester == SystemParamsInFile.SEMESTER_OF_YEAR_FIRST) SemesterName = "HỌC KỲ I";
            if (entity.Semester == SystemParamsInFile.SEMESTER_OF_YEAR_SECOND) SemesterName = "HỌC KỲ II";
            if (entity.Semester == SystemParamsInFile.SEMESTER_OF_YEAR_ALL) SemesterName = "CẢ NĂM";

            string templatePath = SystemParamsInFile.TEMPLATE_FOLDER + reportDef.TemplateDirectory + "/" + reportDef.TemplateName;
            IVTWorkbook oBook = VTExport.OpenWorkbook(templatePath);
            //Lấy ra mức thống kê truyền lên

            int StatisticLevelReportID = entity.StatisticReportID;
            StatisticLevelReport statisticLevelReport = StatisticLevelReportBusiness.Find(StatisticLevelReportID);
            //Lấy cấu hình mức thống kê
            List<StatisticLevelConfig> lstStatisticLevelConfig = StatisticLevelConfigBusiness.Search(new Dictionary<string, object>() { { "StatisticLevelReportID", StatisticLevelReportID } }).ToList();
            lstStatisticLevelConfig = lstStatisticLevelConfig.OrderBy(o => o.OrderNumber).ToList();

            //Số cấu hình 
            int numberConfig = lstStatisticLevelConfig.Count();

            //Lấy sheet template
            IVTWorksheet firstSheet = oBook.GetSheet(1);
            IVTRange range_Level = firstSheet.GetRange("D7", "E8");
            IVTWorksheet sheet = oBook.CopySheetToLast(firstSheet, "Q8");
            int n = 0;
            for (int i = 0; i < numberConfig; i++)
            {
                sheet.CopyPasteSameSize(range_Level, 7, 4 + n);
                sheet.SetCellValue(7, 4 + n, lstStatisticLevelConfig[i].Title);
                n += 2;
            }
            sheet.CopyPasteSameSize(range_Level, 7, 4 + n);
            sheet.SetCellValue(7, 4 + n, "Đ");
            n += 2;
            sheet.CopyPasteSameSize(range_Level, 7, 4 + n);
            sheet.SetCellValue(7, 4 + n, "CĐ");
            //IVTRange range = firstSheet.GetRange("B10", "R10");
            //IVTRange rangefirst = firstSheet.GetRange("B9", "R9");
            //IVTRange rangeTS = firstSheet.GetRange("A13", "R13");
            //IVTRange rangeSubject = firstSheet.GetRange("A9", "A9");
            IVTRange range = firstSheet.GetRange(10, 2, 10, numberConfig * 2 + 7);
            IVTRange rangefirst = firstSheet.GetRange(9, 2, 9, numberConfig * 2 + 7);
            IVTRange rangeTS = firstSheet.GetRange(13, 1, 13, numberConfig * 2 + 7);
            IVTRange rangeSubject = firstSheet.GetRange(9, 1, 9, 1);

            //Tạo sheet           

            //Fill dữ liệu chung
            sheet.Name = "DIEMTHIHK";
            sheet.SetCellValue("A2", Suppervising);
            sheet.SetCellValue("A3", SchoolName);
            sheet.SetCellValue("B5", SemesterName.ToUpper() + " - " + "NĂM HỌC: " + AcademicYear);

            //Lấy danh sách khối học
            List<EducationLevel> lstEducationLevel = this.EducationLevelBusiness.GetByGrade(entity.AppliedLevel).ToList();

            // danh sách điểm lstMarkRecord
            int MarkTypeID = this.MarkTypeBusiness.All.Where(o => o.Title == "HK" && o.AppliedLevel == entity.AppliedLevel).SingleOrDefault().MarkTypeID;
            AcademicYear Aca = AcademicYearBusiness.Find(entity.AcademicYearID);
            IDictionary<string, object> Dic = new Dictionary<string, object> {
                {"AcademicYearID", entity.AcademicYearID},
                {"Semester", entity.Semester},
                {"SchoolID", entity.SchoolID},
                {"MarkTypeID", MarkTypeID},
                {"Year", Aca.Year}
            };

            //Chi lay diem cua cac hoc sinh        
            Dictionary<string, object> dicPupilRanking = new Dictionary<string, object> 
            {
                {"AcademicYearID", entity.AcademicYearID},
                {"SchoolID", entity.SchoolID},
                {"Semester", entity.Semester},
                {"AppliedLevel", entity.AppliedLevel}
            };

            //lay danh sach hoc sinh dang ky mon chuyen mon tu chon
            IDictionary<string, object> dicRegis = new Dictionary<string, object>()
            {
                {"SchoolID",entity.SchoolID},
                {"AcademicYearID",entity.AcademicYearID},
                //{"SubjectID",entity.SubjectID},
                {"SemesterID",entity.Semester},
                {"YearID",Aca.Year}
            };

            
            IQueryable<PupilOfClassBO> listPupil;
            if (entity.Semester == GlobalConstants.SEMESTER_OF_YEAR_FIRST)
            {
                listPupil = (from p in PupilOfClassBusiness.SearchBySchool(entity.SchoolID, dicPupilRanking).AddCriteriaSemester(Aca, entity.Semester)
                             join cs in ClassSubjectBusiness.All.Where(c => c.Last2digitNumberSchool == partitionId) on p.ClassID equals cs.ClassID
                             join q in ClassProfileBusiness.All on p.ClassID equals q.ClassProfileID
                             join r in PupilProfileBusiness.All on p.PupilID equals r.PupilProfileID
                             //Viethd4: Fix loi khong xet hoc mon trong 1 hoc ky
                             where cs.SectionPerWeekFirstSemester != null && cs.SectionPerWeekFirstSemester > 0
                             && q.IsActive.Value
                             select new PupilOfClassBO
                             {
                                 PupilID = p.PupilID,
                                 ClassID = p.ClassID,
                                 EducationLevelID = q.EducationLevelID,
                                 Genre = r.Genre,
                                 EthnicID = r.EthnicID,
                                 AppliedType = cs.AppliedType,
                                 IsSpecialize = cs.IsSpecializedSubject,
                                 SubjectID = cs.SubjectID,
                                 IsVNEN = cs.IsSubjectVNEN
                             });
            }
            else
            {
                listPupil = (from p in PupilOfClassBusiness.SearchBySchool(entity.SchoolID, dicPupilRanking).AddCriteriaSemester(Aca, entity.Semester)
                             join cs in ClassSubjectBusiness.All.Where(c => c.Last2digitNumberSchool == partitionId) on p.ClassID equals cs.ClassID
                             join q in ClassProfileBusiness.All on p.ClassID equals q.ClassProfileID
                             join r in PupilProfileBusiness.All on p.PupilID equals r.PupilProfileID
                             //Viethd4: Fix loi khong xet hoc mon trong 1 hoc ky
                             where cs.SectionPerWeekSecondSemester != null && cs.SectionPerWeekSecondSemester > 0
                             && q.IsActive.Value
                             select new PupilOfClassBO
                             {
                                 PupilID = p.PupilID,
                                 ClassID = p.ClassID,
                                 EducationLevelID = q.EducationLevelID,
                                 Genre = r.Genre,
                                 EthnicID = r.EthnicID,
                                 AppliedType = cs.AppliedType,
                                 IsSpecialize = cs.IsSpecializedSubject,
                                 SubjectID = cs.SubjectID,
                                 IsVNEN = cs.IsSubjectVNEN
                             });
            }

            //Danh sach hoc sinh mien giam
            List<ExemptedSubject> lstExempSubject = ExemptedSubjectBusiness.GetExmpSubjectbyAcademicYear(entity.AcademicYearID, entity.Semester, 0, 0);



            List<PupilOfClassBO> lstPOC = this.GetPupilOfClassInSpecialize(listPupil, dicRegis);
            //Loai bo cac hoc sinh mien giam
            if (lstExempSubject != null && lstExempSubject.Count > 0)
            {
                lstPOC = lstPOC.Where(p => !lstExempSubject.Exists(s => s.PupilID == p.PupilID && p.ClassID == s.ClassID && s.SubjectID == p.SubjectID)).ToList();
            }

            List<PupilOfClassBO> lstPOCNotVnen = this.GetPupilOfClassInSpecialize(listPupil.Where(o=>o.IsVNEN == null || o.IsVNEN == false), dicRegis);
            List<PupilOfClassBO> lstPOCVnen = this.GetPupilOfClassInSpecialize(listPupil.Where(o => o.IsVNEN == true), dicRegis);

            
            



            var tmp = (from u in VMarkRecordBusiness.SearchBySchool(entity.SchoolID, Dic)
                       join cp in ClassProfileBusiness.SearchBySchool(entity.SchoolID, Dic)
                       on u.ClassID equals cp.ClassProfileID
                       join pp in PupilProfileBusiness.All on u.PupilID equals pp.PupilProfileID
                       //where listPupil.Any(o => o.PupilID == u.PupilID && o.ClassID == u.ClassID)
                       select new VMarkRecordBO
                         {
                             EducationLevelID = cp.EducationLevelID,
                             SubjectID = u.SubjectID,
                             Mark = u.Mark,
                             Genre = pp.Genre,
                             EthnicID = pp.EthnicID,
                             ClassID = u.ClassID,
                             PupilID = u.PupilID
                         });

            var iqMark = (from u in tmp.ToList()
                          join l in lstPOCNotVnen on new { u.ClassID, u.PupilID, u.SubjectID } equals new { l.ClassID, l.PupilID, l.SubjectID }
                          select u);

            var lstMarkRecord = iqMark.ToList();
            //Loai bo cac hoc sinh mien giam
            if (lstExempSubject != null && lstExempSubject.Count > 0)
            {
                lstMarkRecord = lstMarkRecord.Where(p => !lstExempSubject.Exists(s => s.PupilID == p.PupilID && p.ClassID == s.ClassID && s.SubjectID == p.SubjectID)).ToList();
            }
            var lstMarkRecord_Female = lstMarkRecord.Where(o => o.Genre == SystemParamsInFile.GENRE_FEMALE).ToList();
            //Dân tộc
            Ethnic Ethnic_Kinh = EthnicBusiness.All.Where(o => o.EthnicCode.ToLower().Equals(SystemParamsInFile.ETHNIC_CODE_KINH.ToLower()) && o.IsActive == true).FirstOrDefault();
            Ethnic Ethnic_ForeignPeople = EthnicBusiness.All.Where(o => o.EthnicCode.ToLower().Equals(SystemParamsInFile.ETHNIC_CODE_NN.ToLower()) && o.IsActive == true).FirstOrDefault();
            int? EthnicID_Kinh = 0;
            int? EthnicID_ForeignPeople = 0;
            if (Ethnic_Kinh != null)
            {
                EthnicID_Kinh = Ethnic_Kinh.EthnicID;
            }
            if (Ethnic_ForeignPeople != null)
            {
                EthnicID_ForeignPeople = Ethnic_ForeignPeople.EthnicID;
            }
            var lstMarkRecord_Ethnic = lstMarkRecord.Where(o => o.EthnicID.HasValue && o.EthnicID != EthnicID_ForeignPeople && o.EthnicID != EthnicID_Kinh).ToList();
            var lstMarkRecord_FemaleEthnic = lstMarkRecord.Where(o => o.Genre == SystemParamsInFile.GENRE_FEMALE && o.EthnicID.HasValue && o.EthnicID != EthnicID_ForeignPeople && o.EthnicID != EthnicID_Kinh).ToList();

            var tmpJudge = from u in VJudgeRecordBusiness.SearchBySchool(entity.SchoolID, Dic)
                           join cp in ClassProfileBusiness.SearchBySchool(entity.SchoolID, Dic)
                           on u.ClassID equals cp.ClassProfileID
                           join pp in PupilProfileBusiness.All on u.PupilID equals pp.PupilProfileID
                           //where listPupil.Any(o => o.PupilID == u.PupilID && o.ClassID == u.ClassID && o.SubjectID == u.SubjectID)
                           //group u by new { cp.EducationLevelID, u.SubjectID, Loai = u.Mark <= mKMax ? 1 : u.Mark <= mYMax ? 2 : u.Mark <= mTbMax ? 3 : u.Mark <= mKhMax ? 4 : 5 } into g
                           select new
                           {
                               EducationLevelID = cp.EducationLevelID,
                               SubjectID = u.SubjectID,
                               Judgement = u.Judgement,
                               Genre = pp.Genre,
                               EthnicID = pp.EthnicID,
                               ClassID = u.ClassID,
                               PupilID = u.PupilID
                           };
            var iqJudge = (from j in tmpJudge.ToList()
                           join l in lstPOCNotVnen on new { j.ClassID, j.PupilID, j.SubjectID } equals new { l.ClassID, l.PupilID, l.SubjectID }
                           select j);

            var lstJudgeRecord = iqJudge.ToList();
            //Loai bo cac hoc sinh mien giam
            if (lstExempSubject != null && lstExempSubject.Count > 0)
            {
                lstJudgeRecord = lstJudgeRecord.Where(p => !lstExempSubject.Exists(s => s.PupilID == p.PupilID && p.ClassID == s.ClassID && s.SubjectID == p.SubjectID)).ToList();
            }
            var lstJudgeRecord_Female = lstJudgeRecord.Where(o => o.Genre == SystemParamsInFile.GENRE_FEMALE).ToList();
            var lstJudgeRecord_Ethnic = lstJudgeRecord.Where(o => o.EthnicID.HasValue && o.EthnicID != EthnicID_ForeignPeople && o.EthnicID != EthnicID_Kinh).ToList();
            var lstJudgeRecord_FemaleEthnic = lstJudgeRecord.Where(o => o.Genre == SystemParamsInFile.GENRE_FEMALE && o.EthnicID.HasValue && o.EthnicID != EthnicID_ForeignPeople && o.EthnicID != EthnicID_Kinh).ToList();


            //Lay diem cuoi ky cac hoc sinh hoc mon hoc VNEN
            var tmpTnbs = from tnbs in TeacherNoteBookSemesterBusiness.All
                          join cp in ClassProfileBusiness.All.Where(o => o.AcademicYearID == entity.AcademicYearID && o.SchoolID == entity.SchoolID && o.IsActive == true)
                             on tnbs.ClassID equals cp.ClassProfileID
                          join pp in PupilProfileBusiness.All on tnbs.PupilID equals pp.PupilProfileID
                          where tnbs.AcademicYearID == entity.AcademicYearID
                          && tnbs.PartitionID == partitionId
                          && tnbs.SchoolID == entity.SchoolID
                          && tnbs.SemesterID == entity.Semester
                          select new 
                          {
                              EducationLevelID = cp.EducationLevelID,
                              SubjectID = tnbs.SubjectID,
                              Judgement = tnbs.PERIODIC_SCORE_END_JUDGLE,
                              Mark = tnbs.PERIODIC_SCORE_END,
                              Genre = pp.Genre,
                              EthnicID = pp.EthnicID,
                              ClassID = tnbs.ClassID,
                              PupilID = tnbs.PupilID
                          };

            var iqTnbs = (from j in tmpTnbs.ToList()
                           join l in lstPOCVnen on new { j.ClassID, j.PupilID, j.SubjectID } equals new { l.ClassID, l.PupilID, l.SubjectID }
                           select new 
                          {
                              EducationLevelID = j.EducationLevelID,
                              SubjectID = j.SubjectID,
                              Judgement = j.Judgement != null ? j.Judgement : String.Empty,
                              Mark = j.Mark,
                              Genre = j.Genre,
                              EthnicID = j.EthnicID,
                              ClassID = j.ClassID,
                              PupilID = j.PupilID
                          });

            var lstTnbsAll = iqTnbs.ToList();
            //Loai bo cac hoc sinh mien giam
            if (lstExempSubject != null && lstExempSubject.Count > 0)
            {
                lstTnbsAll = lstTnbsAll.Where(p => !lstExempSubject.Exists(s => s.PupilID == p.PupilID && p.ClassID == s.ClassID && s.SubjectID == p.SubjectID)).ToList();
            }

            var lstTnbs_Female = lstTnbsAll.Where(o => o.Genre == SystemParamsInFile.GENRE_FEMALE).ToList();
            var lstTnbs_Ethnic = lstTnbsAll.Where(o => o.EthnicID.HasValue && o.EthnicID != EthnicID_ForeignPeople && o.EthnicID != EthnicID_Kinh).ToList();
            var lstTnbs_FemaleEthnic = lstTnbsAll.Where(o => o.Genre == SystemParamsInFile.GENRE_FEMALE && o.EthnicID.HasValue && o.EthnicID != EthnicID_ForeignPeople && o.EthnicID != EthnicID_Kinh).ToList();

            IDictionary<string, object> Dictionary = new Dictionary<string, object> {
                {"AcademicYearID", entity.AcademicYearID},
                {"AppliedLevel", entity.AppliedLevel}
            };
            IQueryable<SubjectCatBO> lstSubject_Temp = (from p in this.SchoolSubjectBusiness.SearchBySchool(entity.SchoolID, Dictionary)
                                                        join q in SubjectCatBusiness.All on p.SubjectID equals q.SubjectCatID
                                                        where q.AppliedLevel == entity.AppliedLevel && q.IsActive == true
                                                        select new SubjectCatBO
                                                        {
                                                            SubjectCatID = p.SubjectID,
                                                            OrderInSubject = q.OrderInSubject,
                                                            DisplayName = q.DisplayName,
                                                            IsCommenting = p.IsCommenting
                                                        });
            lstSubject_Temp = lstSubject_Temp.Distinct();
            List<SubjectCatBO> lstSubject = lstSubject_Temp.OrderBy(o => o.OrderInSubject).ThenBy(o => o.DisplayName).ToList();

            // Lay danh sach hoc sinh theo khoi
            var listCountPupilByEducation = lstPOC.GroupBy(o => new { o.EducationLevelID, o.SubjectID, o.Genre, o.EthnicID }).Select(o => new { EducationLevelID = o.Key.EducationLevelID, SubjectID = o.Key.SubjectID, Genre = o.Key.Genre, EthnicID = o.Key.EthnicID, TotalPupil = o.Count() }).ToList();
            SheetData sheetdata = new SheetData(firstSheet.Name);
            sheetdata.Data["ProvinceName"] = (objSP.District != null ? objSP.District.DistrictName : "");
            sheetdata.Data["day"] = DateTime.Now.Day;
            sheetdata.Data["month"] = DateTime.Now.Month;
            sheetdata.Data["year"] = DateTime.Now.Year;
            firstSheet.FillVariableValue(sheetdata.Data);
            IVTRange range_day = firstSheet.GetRange("S21", "Y22");
            #region Tạo sheet dữ liệu điểm thi học kỳ
            //Hệ thống thực hiện for theo danh sách môn học (Tức là for theo lstSubject)
            int countSubject = lstSubject.Count();
            if (entity.AppliedLevel == SystemParamsInFile.APPLIED_LEVEL_SECONDARY)
            {
                int positionRowHT = countSubject * 5 + 11;
                int positionColHT = (numberConfig + 2) * 2 + 3 - 6;
                sheet.CopyPasteSameSize(range_day, positionRowHT, positionColHT);
            }
            else
            {
                int positionRowHT = countSubject * 4 + 11;
                int positionColHT = (numberConfig + 2) * 2 + 3 - 6;
                sheet.CopyPasteSameSize(range_day, positionRowHT, positionColHT);
            }
            int startrow = 9;

            string column = "";
            for (int i = 0; i < lstSubject.Count; i++)
            {
                int startmin = startrow;
                int startmerge = startrow;
                string FomulerSumpupil = "";
                int SubjectID = lstSubject[i].SubjectCatID;
                sheet.CopyPasteSameRowHeigh(rangeSubject, startrow);
                sheet.SetCellValue(startmerge, 1, lstSubject[i].DisplayName);
                var listMarkBySubjectEdu_Subject = lstMarkRecord.Where(o => o.SubjectID == SubjectID).ToList();
                var listJudgeBySubjectEdu_Subject = lstJudgeRecord.Where(o => o.SubjectID == SubjectID).ToList();
                var listTnbsBySubjectEdu_Subject = lstTnbsAll.Where(o => o.SubjectID == SubjectID).ToList();

                for (int j = 0; j < lstEducationLevel.Count; j++)
                {
                    int EducationLevelID = lstEducationLevel[j].EducationLevelID;
                    //Copy row style
                    if (j == 0)
                        sheet.CopyPasteSameRowHeigh(rangefirst, startrow);
                    else
                        sheet.CopyPasteSameRowHeigh(range, startrow);

                    var listMarkBySubjectEdu = listMarkBySubjectEdu_Subject.Where(o => o.EducationLevelID == EducationLevelID).Select(o => o.Mark).ToList();
                    var ListTnbsBySubjectEdu = listTnbsBySubjectEdu_Subject.Where(o => o.EducationLevelID == EducationLevelID).Select(o => o.Mark).ToList();
                    n = 0;
                    if (lstSubject[i].IsCommenting == SystemParamsInFile.ISCOMMENTING_TYPE_MARK || lstSubject[i].IsCommenting == SystemParamsInFile.ISCOMMENTING_TYPE_MARK_JUDGE)
                    {
                        for (int m = 0; m < numberConfig; m++)
                        {
                            StatisticLevelConfig slc = lstStatisticLevelConfig[m];
                            if (slc.SignMin == SystemParamsInFile.Sign_Compare.GREATER_OR_EQUAL_SIGN)
                            {
                                if (slc.SignMax == SystemParamsInFile.Sign_Compare.SMALLER_OR_EQUAL_SIGN)
                                {
                                    int SL = listMarkBySubjectEdu.Where(o => o >= slc.MinValue && o <= slc.MaxValue).Count()
                                        + ListTnbsBySubjectEdu.Where(o => o >= slc.MinValue && o <= slc.MaxValue).Count();

                                    sheet.SetCellValue(startrow, 4 + n, SL);//E
                                    sheet.SetCellValue(startrow, 5 + n, "=IF(C" + startrow + ">0" + "," + "ROUND(" + UtilsBusiness.GetExcelColumnName(4 + n) + startrow + "/" + "C" + startrow + "*100,2),0)");
                                }
                                else if (slc.SignMax == SystemParamsInFile.Sign_Compare.SMALLER_SIGN)
                                {
                                    int SL = listMarkBySubjectEdu.Where(o => o >= slc.MinValue && o < slc.MaxValue).Count()
                                        + ListTnbsBySubjectEdu.Where(o => o >= slc.MinValue && o < slc.MaxValue).Count();

                                    sheet.SetCellValue(startrow, 4 + n, SL);//E
                                    sheet.SetCellValue(startrow, 5 + n, "=IF(C" + startrow + ">0" + "," + "ROUND(" + UtilsBusiness.GetExcelColumnName(4 + n) + startrow + "/" + "C" + startrow + "*100,2),0)");
                                }
                                else if (slc.SignMax == SystemParamsInFile.Sign_Compare.EQUAL_SIGN)
                                {
                                    int SL = listMarkBySubjectEdu.Where(o => o >= slc.MinValue && o == slc.MaxValue).Count()
                                        + ListTnbsBySubjectEdu.Where(o => o >= slc.MinValue && o == slc.MaxValue).Count();

                                    sheet.SetCellValue(startrow, 4 + n, SL);//E
                                    sheet.SetCellValue(startrow, 5 + n, "=IF(C" + startrow + ">0" + "," + "ROUND(" + UtilsBusiness.GetExcelColumnName(4 + n) + startrow + "/" + "C" + startrow + "*100,2),0)");
                                }
                                else
                                {
                                    int SL = listMarkBySubjectEdu.Where(o => o >= slc.MinValue).Count()
                                        + ListTnbsBySubjectEdu.Where(o => o >= slc.MinValue).Count();

                                    sheet.SetCellValue(startrow, 4 + n, SL);//E
                                    sheet.SetCellValue(startrow, 5 + n, "=IF(C" + startrow + ">0" + "," + "ROUND(" + UtilsBusiness.GetExcelColumnName(4 + n) + startrow + "/" + "C" + startrow + "*100,2),0)");
                                }
                            }
                            else if (slc.SignMin == SystemParamsInFile.Sign_Compare.GREATEER_THAN_SIGN)
                            {
                                if (slc.SignMax == SystemParamsInFile.Sign_Compare.SMALLER_OR_EQUAL_SIGN)
                                {
                                    int SL = listMarkBySubjectEdu.Where(o => o > slc.MinValue && o <= slc.MaxValue).Count()
                                        + ListTnbsBySubjectEdu.Where(o => o > slc.MinValue && o <= slc.MaxValue).Count();

                                    sheet.SetCellValue(startrow, 4 + n, SL);//E
                                    sheet.SetCellValue(startrow, 5 + n, "=IF(C" + startrow + ">0" + "," + "ROUND(" + UtilsBusiness.GetExcelColumnName(4 + n) + startrow + "/" + "C" + startrow + "*100,2),0)");
                                }
                                else if (slc.SignMax == SystemParamsInFile.Sign_Compare.SMALLER_SIGN)
                                {
                                    int SL = listMarkBySubjectEdu.Where(o => o > slc.MinValue && o < slc.MaxValue).Count()
                                        + ListTnbsBySubjectEdu.Where(o => o > slc.MinValue && o < slc.MaxValue).Count();

                                    sheet.SetCellValue(startrow, 4 + n, SL);//E
                                    sheet.SetCellValue(startrow, 5 + n, "=IF(C" + startrow + ">0" + "," + "ROUND(" + UtilsBusiness.GetExcelColumnName(4 + n) + startrow + "/" + "C" + startrow + "*100,2),0)");
                                }
                                else if (slc.SignMax == SystemParamsInFile.Sign_Compare.EQUAL_SIGN)
                                {
                                    int SL = listMarkBySubjectEdu.Where(o => o > slc.MinValue && o == slc.MaxValue).Count()
                                        + ListTnbsBySubjectEdu.Where(o => o > slc.MinValue && o == slc.MaxValue).Count();

                                    sheet.SetCellValue(startrow, 4 + n, SL);//E
                                    sheet.SetCellValue(startrow, 5 + n, "=IF(C" + startrow + ">0" + "," + "ROUND(" + UtilsBusiness.GetExcelColumnName(4 + n) + startrow + "/" + "C" + startrow + "*100,2),0)");
                                }
                                else
                                {
                                    int SL = listMarkBySubjectEdu.Where(o => o > slc.MinValue).Count()
                                        + ListTnbsBySubjectEdu.Where(o => o > slc.MinValue).Count();

                                    sheet.SetCellValue(startrow, 4 + n, SL);//E
                                    sheet.SetCellValue(startrow, 5 + n, "=IF(C" + startrow + ">0" + "," + "ROUND(" + UtilsBusiness.GetExcelColumnName(4 + n) + startrow + "/" + "C" + startrow + "*100,2),0)");
                                }
                            }
                            else
                            {
                                if (slc.SignMax == SystemParamsInFile.Sign_Compare.SMALLER_OR_EQUAL_SIGN)
                                {
                                    int SL = listMarkBySubjectEdu.Where(o => o <= slc.MaxValue).Count()
                                        + ListTnbsBySubjectEdu.Where(o => o <= slc.MaxValue).Count();

                                    sheet.SetCellValue(startrow, 4 + n, SL);//E
                                    sheet.SetCellValue(startrow, 5 + n, "=IF(C" + startrow + ">0" + "," + "ROUND(" + UtilsBusiness.GetExcelColumnName(4 + n) + startrow + "/" + "C" + startrow + "*100,2),0)");
                                }
                                else if (slc.SignMax == SystemParamsInFile.Sign_Compare.SMALLER_SIGN)
                                {
                                    int SL = listMarkBySubjectEdu.Where(o => o < slc.MaxValue).Count()
                                        + ListTnbsBySubjectEdu.Where(o => o < slc.MaxValue).Count();

                                    sheet.SetCellValue(startrow, 4 + n, SL);//E
                                    sheet.SetCellValue(startrow, 5 + n, "=IF(C" + startrow + ">0" + "," + "ROUND(" + UtilsBusiness.GetExcelColumnName(4 + n) + startrow + "/" + "C" + startrow + "*100,2),0)");
                                }
                                else if (slc.SignMax == SystemParamsInFile.Sign_Compare.EQUAL_SIGN)
                                {
                                    int SL = listMarkBySubjectEdu.Where(o => o == slc.MaxValue).Count()
                                        + ListTnbsBySubjectEdu.Where(o => o == slc.MaxValue).Count();

                                    sheet.SetCellValue(startrow, 4 + n, SL);//E
                                    sheet.SetCellValue(startrow, 5 + n, "=IF(C" + startrow + ">0" + "," + "ROUND(" + UtilsBusiness.GetExcelColumnName(4 + n) + startrow + "/" + "C" + startrow + "*100,2),0)");
                                }
                            }
                            n += 2;
                        }
                    }
                    n = 0;
                    //Điền dữ liệu Đ, CĐ
                    int positionD = numberConfig * 2 + 4;
                    if (lstSubject[i].IsCommenting == SystemParamsInFile.ISCOMMENTING_TYPE_JUDGE)
                    {
                        int SL = listJudgeBySubjectEdu_Subject.Where(o => o.Judgement.ToUpper().Equals(SystemParamsInFile.JUDGE_MARK_D) && o.EducationLevelID == EducationLevelID).Count()
                            + listTnbsBySubjectEdu_Subject.Where(o => o.Judgement.ToUpper().Equals(SystemParamsInFile.JUDGE_MARK_D) && o.EducationLevelID == EducationLevelID).Count();
                        
                        sheet.SetCellValue(startrow, positionD + n, SL);//E
                        sheet.SetCellValue(startrow, positionD + n + 1, "=IF(C" + startrow + ">0" + "," + "ROUND(" + UtilsBusiness.GetExcelColumnName(positionD + n) + startrow + "/" + "C" + startrow + "*100,2),0)");
                        n += 2;
                        
                        int SLCD = listJudgeBySubjectEdu_Subject.Where(o => o.Judgement.ToUpper().Equals(SystemParamsInFile.JUDGE_MARK_CD) && o.EducationLevelID == EducationLevelID).Count()
                            + listTnbsBySubjectEdu_Subject.Where(o => o.Judgement.ToUpper().Equals(SystemParamsInFile.JUDGE_MARK_CD) && o.EducationLevelID == EducationLevelID).Count();

                        sheet.SetCellValue(startrow, positionD + n, SLCD);//E
                        sheet.SetCellValue(startrow, positionD + n + 1, "=IF(C" + startrow + ">0" + "," + "ROUND(" + UtilsBusiness.GetExcelColumnName(positionD + n) + startrow + "/" + "C" + startrow + "*100,2),0)");
                    }


                    // Sĩ Số
                    var objCountPupil = listCountPupilByEducation.Where(o => o.EducationLevelID == EducationLevelID && o.SubjectID == SubjectID);
                    int SumPupil = objCountPupil != null ? objCountPupil.Sum(o => o.TotalPupil) : 0;

                    FomulerSumpupil += "C" + startrow + ",";
                    // fill vào excel
                    sheet.SetCellValue(startrow, 2, lstEducationLevel[j].Resolution);
                    sheet.SetCellValue(startrow, 3, SumPupil);
                    startrow++;
                }
                if (lstEducationLevel.Count > 1)
                {
                    sheet.CopyPasteSameSize(rangeTS, startrow, 1);
                }
                sheet.SetCellValue(startrow, 2, "TS");
                sheet.SetCellValue(startrow, 3, "=SUM(" + FomulerSumpupil.Substring(0, FomulerSumpupil.Length - 1) + ")");
                n = 0;
                for (int m = 0; m < numberConfig + 2; m++)
                {
                    column = UtilsBusiness.GetExcelColumnName(4 + n);
                    sheet.SetCellValue(startrow, 4 + n, "=SUM(" + column + startmin.ToString() + ":" + column + (startrow - 1).ToString() + ")");//E
                    sheet.SetCellValue(startrow, 5 + n, "=IF(C" + startrow + ">0" + "," + "ROUND(" + column + startrow + "/" + "C" + startrow + "*100,2),0)");
                    n += 2;
                }

                sheet.MergeColumn(1, startmerge, startrow);
                sheet.GetRange(startmerge, 1, startrow, 1).SetBorder(VTBorderStyle.Solid, VTBorderWeight.Medium, VTBorderIndex.EdgeLeft);
                startrow++;
            }
            if (entity.AppliedLevel == SystemParamsInFile.APPLIED_LEVEL_SECONDARY)
            {
                sheet.GetRange(7, (numberConfig + 2) * 2 + 3, countSubject * 5 + 8, (numberConfig + 2) * 2 + 3).SetBorder(VTBorderStyle.Solid, VTBorderWeight.Medium, VTBorderIndex.EdgeRight);
            }
            else
            {
                sheet.GetRange(7, (numberConfig + 2) * 2 + 3, countSubject * 4 + 8, (numberConfig + 2) * 2 + 3).SetBorder(VTBorderStyle.Solid, VTBorderWeight.Medium, VTBorderIndex.EdgeRight);
            }

            //sheet.CopyPaste(firstSheet.GetRange("T21", "Z22"), startrow + 1, 12);
            //sheet.CopyPaste(sheet.GetRange("T25", "Z26"), 21, 20);

            //sheet.SetCellValue(startrow + 2, 13, "${ProvinceName}, ngày ${day}  tháng ${month}   năm ${year}");
            //sheet.MergeColumn(startrow + 2, 13, 18);
            //sheet.SetCellValue(startrow + 3, 13, "Hiệu Trưởng");
            //sheet.MergeColumn(startrow + 4, 13, 18);
            //SheetData sheetdata = new SheetData(sheet.Name);
            //sheetdata.Data["ProvinceName"] = ProvinceBusiness.Find(SchoolProfileBusiness.Find(entity.SchoolID).ProvinceID).ProvinceName;
            //sheetdata.Data["day"] = DateTime.Now.Day;
            //sheetdata.Data["month"] = DateTime.Now.Month;
            //sheetdata.Data["year"] = DateTime.Now.Year;
            //sheet.FillVariableValue(sheetdata.Data);
            sheet.SetCellValue(UtilsBusiness.GetExcelColumnName(1) + (startrow), "(*): Tổng số học sinh của lớp không bao gồm các học sinh được miễn giảm môn tương ứng");
            sheet.SetFontName("Times New Roman", 0);
            sheet.PageSize = VTXPageSize.VTxlPaperA4;
            sheet.FitAllColumnsOnOnePage = true;
            #endregion

            
            if (entity.FemaleChecked)
            {
                #region Tạo file báo cáo điểm thi học kỳ học sinh nữ
                IVTWorksheet sheet_Female = oBook.CopySheetToLast(sheet);
                sheet_Female.SetCellValue("B4", "THỐNG KÊ ĐIỂM THI HỌC KỲ HỌC SINH NỮ");
                sheet_Female.Name = "HS_Nu";
                startrow = 9;
                column = "";
                for (int i = 0; i < lstSubject.Count; i++)
                {
                    int startmin = startrow;
                    int startmerge = startrow;
                    string FomulerSumpupil = "";
                    int SubjectID = lstSubject[i].SubjectCatID;
                    sheet_Female.CopyPasteSameRowHeigh(rangeSubject, startrow);
                    sheet_Female.SetCellValue(startmerge, 1, lstSubject[i].DisplayName);
                    var listMarkBySubjectEdu_Subject = lstMarkRecord_Female.Where(o => o.SubjectID == SubjectID).ToList();
                    var listJudgeBySubjectEdu_Subject = lstJudgeRecord_Female.Where(o => o.SubjectID == SubjectID).ToList();
                    var listTnbsBySubjectEdu_Subject = lstTnbs_Female.Where(o => o.SubjectID == SubjectID).ToList();

                    for (int j = 0; j < lstEducationLevel.Count; j++)
                    {
                        int EducationLevelID = lstEducationLevel[j].EducationLevelID;

                        var listMarkBySubjectEdu = listMarkBySubjectEdu_Subject.Where(o => o.EducationLevelID == EducationLevelID).Select(o => o.Mark).ToList();
                        var ListTnbsBySubjectEdu = listTnbsBySubjectEdu_Subject.Where(o => o.EducationLevelID == EducationLevelID).Select(o => o.Mark).ToList();
                        n = 0;
                        if (lstSubject[i].IsCommenting == SystemParamsInFile.ISCOMMENTING_TYPE_MARK || lstSubject[i].IsCommenting == SystemParamsInFile.ISCOMMENTING_TYPE_MARK_JUDGE)
                        {
                            for (int m = 0; m < numberConfig; m++)
                            {
                                StatisticLevelConfig slc = lstStatisticLevelConfig[m];
                                if (slc.SignMin == SystemParamsInFile.Sign_Compare.GREATER_OR_EQUAL_SIGN)
                                {
                                    if (slc.SignMax == SystemParamsInFile.Sign_Compare.SMALLER_OR_EQUAL_SIGN)
                                    {
                                        int SL = listMarkBySubjectEdu.Where(o => o >= slc.MinValue && o <= slc.MaxValue).Count()
                                            + ListTnbsBySubjectEdu.Where(o => o >= slc.MinValue && o <= slc.MaxValue).Count();
                                        sheet_Female.SetCellValue(startrow, 4 + n, SL);//E
                                    }
                                    else if (slc.SignMax == SystemParamsInFile.Sign_Compare.SMALLER_SIGN)
                                    {
                                        int SL = listMarkBySubjectEdu.Where(o => o >= slc.MinValue && o < slc.MaxValue).Count()
                                            + ListTnbsBySubjectEdu.Where(o => o >= slc.MinValue && o < slc.MaxValue).Count();
                                        sheet_Female.SetCellValue(startrow, 4 + n, SL);//E
                                    }
                                    else if (slc.SignMax == SystemParamsInFile.Sign_Compare.EQUAL_SIGN)
                                    {
                                        int SL = listMarkBySubjectEdu.Where(o => o >= slc.MinValue && o == slc.MaxValue).Count()
                                            + ListTnbsBySubjectEdu.Where(o => o >= slc.MinValue && o == slc.MaxValue).Count();
                                        sheet_Female.SetCellValue(startrow, 4 + n, SL);//E                          
                                    }
                                    else
                                    {
                                        int SL = listMarkBySubjectEdu.Where(o => o >= slc.MinValue).Count()
                                            + ListTnbsBySubjectEdu.Where(o => o >= slc.MinValue).Count();
                                        sheet_Female.SetCellValue(startrow, 4 + n, SL);//E
                                    }
                                }
                                else if (slc.SignMin == SystemParamsInFile.Sign_Compare.GREATEER_THAN_SIGN)
                                {
                                    if (slc.SignMax == SystemParamsInFile.Sign_Compare.SMALLER_OR_EQUAL_SIGN)
                                    {
                                        int SL = listMarkBySubjectEdu.Where(o => o > slc.MinValue && o <= slc.MaxValue).Count()
                                            + ListTnbsBySubjectEdu.Where(o => o > slc.MinValue && o <= slc.MaxValue).Count();
                                        sheet_Female.SetCellValue(startrow, 4 + n, SL);//E
                                    }
                                    else if (slc.SignMax == SystemParamsInFile.Sign_Compare.SMALLER_SIGN)
                                    {
                                        int SL = listMarkBySubjectEdu.Where(o => o > slc.MinValue && o < slc.MaxValue).Count()
                                            + ListTnbsBySubjectEdu.Where(o => o > slc.MinValue && o < slc.MaxValue).Count();
                                        sheet_Female.SetCellValue(startrow, 4 + n, SL);//E
                                    }
                                    else if (slc.SignMax == SystemParamsInFile.Sign_Compare.EQUAL_SIGN)
                                    {
                                        int SL = listMarkBySubjectEdu.Where(o => o > slc.MinValue && o == slc.MaxValue).Count()
                                            + ListTnbsBySubjectEdu.Where(o => o > slc.MinValue && o == slc.MaxValue).Count();
                                        sheet_Female.SetCellValue(startrow, 4 + n, SL);//E
                                    }
                                    else
                                    {
                                        int SL = listMarkBySubjectEdu.Where(o => o > slc.MinValue).Count()
                                            + ListTnbsBySubjectEdu.Where(o => o > slc.MinValue).Count();
                                        sheet_Female.SetCellValue(startrow, 4 + n, SL);//E
                                    }
                                }
                                else
                                {
                                    if (slc.SignMax == SystemParamsInFile.Sign_Compare.SMALLER_OR_EQUAL_SIGN)
                                    {
                                        int SL = listMarkBySubjectEdu.Where(o => o <= slc.MaxValue).Count()
                                            + ListTnbsBySubjectEdu.Where(o => o <= slc.MaxValue).Count();
                                        sheet_Female.SetCellValue(startrow, 4 + n, SL);//E
                                    }
                                    else if (slc.SignMax == SystemParamsInFile.Sign_Compare.SMALLER_SIGN)
                                    {
                                        int SL = listMarkBySubjectEdu.Where(o => o < slc.MaxValue).Count()
                                            + ListTnbsBySubjectEdu.Where(o => o < slc.MaxValue).Count();
                                        sheet_Female.SetCellValue(startrow, 4 + n, SL);//E
                                    }
                                    else if (slc.SignMax == SystemParamsInFile.Sign_Compare.EQUAL_SIGN)
                                    {
                                        int SL = listMarkBySubjectEdu.Where(o => o == slc.MaxValue).Count()
                                            + ListTnbsBySubjectEdu.Where(o => o == slc.MaxValue).Count();
                                        sheet_Female.SetCellValue(startrow, 4 + n, SL);//E
                                    }
                                }
                                n += 2;
                            }
                        }
                        n = 0;
                        //Điền dữ liệu Đ, CĐ
                        int positionD = numberConfig * 2 + 4;
                        if (lstSubject[i].IsCommenting == SystemParamsInFile.ISCOMMENTING_TYPE_JUDGE)
                        {
                            int SL = listJudgeBySubjectEdu_Subject.Where(o => o.Judgement.ToUpper().Equals(SystemParamsInFile.JUDGE_MARK_D) && o.EducationLevelID == EducationLevelID).Count()
                                + listTnbsBySubjectEdu_Subject.Where(o => o.Judgement.ToUpper().Equals(SystemParamsInFile.JUDGE_MARK_D) && o.EducationLevelID == EducationLevelID).Count();
                            sheet_Female.SetCellValue(startrow, positionD + n, SL);//E
                            n += 2;
                            int SLCD = listJudgeBySubjectEdu_Subject.Where(o => o.Judgement.ToUpper().Equals(SystemParamsInFile.JUDGE_MARK_CD) && o.EducationLevelID == EducationLevelID).Count()
                                + listTnbsBySubjectEdu_Subject.Where(o => o.Judgement.ToUpper().Equals(SystemParamsInFile.JUDGE_MARK_CD) && o.EducationLevelID == EducationLevelID).Count();
                            sheet_Female.SetCellValue(startrow, positionD + n, SLCD);//E
                        }


                        // Sĩ Số
                        var objCountPupil = listCountPupilByEducation.Where(o => o.EducationLevelID == EducationLevelID && o.Genre == SystemParamsInFile.GENRE_FEMALE && o.SubjectID == SubjectID);
                        int SumPupil = objCountPupil != null ? objCountPupil.Sum(o => o.TotalPupil) : 0;

                        FomulerSumpupil += "C" + startrow + ",";
                        // fill vào excel
                        sheet_Female.SetCellValue(startrow, 2, lstEducationLevel[j].Resolution);
                        sheet_Female.SetCellValue(startrow, 3, SumPupil);
                        startrow++;
                    }

                    sheet_Female.MergeColumn(1, startmerge, startrow);
                    sheet_Female.GetRange(startmerge, 1, startrow, 1).SetBorder(VTBorderStyle.Solid, VTBorderWeight.Medium, VTBorderIndex.EdgeLeft);
                    startrow++;
                }
                if (entity.AppliedLevel == SystemParamsInFile.APPLIED_LEVEL_SECONDARY)
                {
                    sheet_Female.GetRange(7, (numberConfig + 2) * 2 + 3, countSubject * 5 + 8, (numberConfig + 2) * 2 + 3).SetBorder(VTBorderStyle.Solid, VTBorderWeight.Medium, VTBorderIndex.EdgeRight);
                }
                else
                {
                    sheet_Female.GetRange(7, (numberConfig + 2) * 2 + 3, countSubject * 4 + 8, (numberConfig + 2) * 2 + 3).SetBorder(VTBorderStyle.Solid, VTBorderWeight.Medium, VTBorderIndex.EdgeRight);
                }
                sheet_Female.FitAllColumnsOnOnePage = true;
                sheet_Female.SetCellValue(UtilsBusiness.GetExcelColumnName(1) + (startrow), "(*): Tổng số học sinh của lớp không bao gồm các học sinh được miễn giảm môn tương ứng");
                sheet_Female.SetFontName("Times New Roman", 0);
                #endregion
            }
            if (entity.EthnicChecked)
            {
                #region Tạo file báo cáo điểm thi học kỳ học sinh dân tộc
                IVTWorksheet sheet_Ethnic = oBook.CopySheetToLast(sheet);
                sheet_Ethnic.SetCellValue("B4", "THỐNG KÊ ĐIỂM THI HỌC KỲ HỌC SINH DÂN TỘC");
                sheet_Ethnic.Name = "HS_DT";
                startrow = 9;
                column = "";
                for (int i = 0; i < lstSubject.Count; i++)
                {
                    int startmin = startrow;
                    int startmerge = startrow;
                    string FomulerSumpupil = "";
                    int SubjectID = lstSubject[i].SubjectCatID;
                    sheet_Ethnic.CopyPasteSameRowHeigh(rangeSubject, startrow);
                    sheet_Ethnic.SetCellValue(startmerge, 1, lstSubject[i].DisplayName);
                    var listMarkBySubjectEdu_Subject = lstMarkRecord_Ethnic.Where(o => o.SubjectID == SubjectID).ToList();
                    var listJudgeBySubjectEdu_Subject = lstJudgeRecord_Ethnic.Where(o => o.SubjectID == SubjectID).ToList();
                    var listTnbsBySubjectEdu_Subject = lstTnbs_Ethnic.Where(o => o.SubjectID == SubjectID).ToList();

                    for (int j = 0; j < lstEducationLevel.Count; j++)
                    {
                        int EducationLevelID = lstEducationLevel[j].EducationLevelID;
                        //Copy row style
                        if (j == 0)
                            sheet_Ethnic.CopyPasteSameRowHeigh(rangefirst, startrow);
                        else
                            sheet_Ethnic.CopyPasteSameRowHeigh(range, startrow);

                        var listMarkBySubjectEdu = listMarkBySubjectEdu_Subject.Where(o => o.EducationLevelID == EducationLevelID).Select(o => o.Mark).ToList();
                        var ListTnbsBySubjectEdu = listTnbsBySubjectEdu_Subject.Where(o => o.EducationLevelID == EducationLevelID).Select(o => o.Mark).ToList();
                        n = 0;
                        if (lstSubject[i].IsCommenting == SystemParamsInFile.ISCOMMENTING_TYPE_MARK || lstSubject[i].IsCommenting == SystemParamsInFile.ISCOMMENTING_TYPE_MARK_JUDGE)
                        {
                            for (int m = 0; m < numberConfig; m++)
                            {
                                StatisticLevelConfig slc = lstStatisticLevelConfig[m];
                                if (slc.SignMin == SystemParamsInFile.Sign_Compare.GREATER_OR_EQUAL_SIGN)
                                {
                                    if (slc.SignMax == SystemParamsInFile.Sign_Compare.SMALLER_OR_EQUAL_SIGN)
                                    {
                                        int SL = listMarkBySubjectEdu.Where(o => o >= slc.MinValue && o <= slc.MaxValue).Count()
                                            + ListTnbsBySubjectEdu.Where(o => o >= slc.MinValue && o <= slc.MaxValue).Count();
                                        sheet_Ethnic.SetCellValue(startrow, 4 + n, SL);//E
                                        sheet_Ethnic.SetCellValue(startrow, 5 + n, "=IF(C" + startrow + ">0" + "," + "ROUND(" + UtilsBusiness.GetExcelColumnName(4 + n) + startrow + "/" + "C" + startrow + "*100,2),0)");
                                    }
                                    else if (slc.SignMax == SystemParamsInFile.Sign_Compare.SMALLER_SIGN)
                                    {
                                        int SL = listMarkBySubjectEdu.Where(o => o >= slc.MinValue && o < slc.MaxValue).Count()
                                            + ListTnbsBySubjectEdu.Where(o => o >= slc.MinValue && o < slc.MaxValue).Count();
                                        sheet_Ethnic.SetCellValue(startrow, 4 + n, SL);//E
                                        sheet_Ethnic.SetCellValue(startrow, 5 + n, "=IF(C" + startrow + ">0" + "," + "ROUND(" + UtilsBusiness.GetExcelColumnName(4 + n) + startrow + "/" + "C" + startrow + "*100,2),0)");
                                    }
                                    else if (slc.SignMax == SystemParamsInFile.Sign_Compare.EQUAL_SIGN)
                                    {
                                        int SL = listMarkBySubjectEdu.Where(o => o >= slc.MinValue && o == slc.MaxValue).Count()
                                            + ListTnbsBySubjectEdu.Where(o => o >= slc.MinValue && o == slc.MaxValue).Count();
                                        sheet_Ethnic.SetCellValue(startrow, 4 + n, SL);//E
                                        sheet_Ethnic.SetCellValue(startrow, 5 + n, "=IF(C" + startrow + ">0" + "," + "ROUND(" + UtilsBusiness.GetExcelColumnName(4 + n) + startrow + "/" + "C" + startrow + "*100,2),0)");
                                    }
                                    else
                                    {
                                        int SL = listMarkBySubjectEdu.Where(o => o >= slc.MinValue).Count()
                                            + ListTnbsBySubjectEdu.Where(o => o >= slc.MinValue).Count();
                                        sheet_Ethnic.SetCellValue(startrow, 4 + n, SL);//E
                                        sheet_Ethnic.SetCellValue(startrow, 5 + n, "=IF(C" + startrow + ">0" + "," + "ROUND(" + UtilsBusiness.GetExcelColumnName(4 + n) + startrow + "/" + "C" + startrow + "*100,2),0)");
                                    }
                                }
                                else if (slc.SignMin == SystemParamsInFile.Sign_Compare.GREATEER_THAN_SIGN)
                                {
                                    if (slc.SignMax == SystemParamsInFile.Sign_Compare.SMALLER_OR_EQUAL_SIGN)
                                    {
                                        int SL = listMarkBySubjectEdu.Where(o => o > slc.MinValue && o <= slc.MaxValue).Count()
                                            + ListTnbsBySubjectEdu.Where(o => o > slc.MinValue && o <= slc.MaxValue).Count();
                                        sheet_Ethnic.SetCellValue(startrow, 4 + n, SL);//E
                                        sheet_Ethnic.SetCellValue(startrow, 5 + n, "=IF(C" + startrow + ">0" + "," + "ROUND(" + UtilsBusiness.GetExcelColumnName(4 + n) + startrow + "/" + "C" + startrow + "*100,2),0)");
                                    }
                                    else if (slc.SignMax == SystemParamsInFile.Sign_Compare.SMALLER_SIGN)
                                    {
                                        int SL = listMarkBySubjectEdu.Where(o => o > slc.MinValue && o < slc.MaxValue).Count()
                                            + ListTnbsBySubjectEdu.Where(o => o > slc.MinValue && o < slc.MaxValue).Count();
                                        sheet_Ethnic.SetCellValue(startrow, 4 + n, SL);//E
                                        sheet_Ethnic.SetCellValue(startrow, 5 + n, "=IF(C" + startrow + ">0" + "," + "ROUND(" + UtilsBusiness.GetExcelColumnName(4 + n) + startrow + "/" + "C" + startrow + "*100,2),0)");
                                    }
                                    else if (slc.SignMax == SystemParamsInFile.Sign_Compare.EQUAL_SIGN)
                                    {
                                        int SL = listMarkBySubjectEdu.Where(o => o > slc.MinValue && o == slc.MaxValue).Count()
                                            + ListTnbsBySubjectEdu.Where(o => o > slc.MinValue && o == slc.MaxValue).Count();
                                        sheet_Ethnic.SetCellValue(startrow, 4 + n, SL);//E
                                        sheet_Ethnic.SetCellValue(startrow, 5 + n, "=IF(C" + startrow + ">0" + "," + "ROUND(" + UtilsBusiness.GetExcelColumnName(4 + n) + startrow + "/" + "C" + startrow + "*100,2),0)");
                                    }
                                    else
                                    {
                                        int SL = listMarkBySubjectEdu.Where(o => o > slc.MinValue).Count()
                                            + ListTnbsBySubjectEdu.Where(o => o > slc.MinValue).Count();
                                        sheet_Ethnic.SetCellValue(startrow, 4 + n, SL);//E
                                        sheet_Ethnic.SetCellValue(startrow, 5 + n, "=IF(C" + startrow + ">0" + "," + "ROUND(" + UtilsBusiness.GetExcelColumnName(4 + n) + startrow + "/" + "C" + startrow + "*100,2),0)");
                                    }
                                }
                                else
                                {
                                    if (slc.SignMax == SystemParamsInFile.Sign_Compare.SMALLER_OR_EQUAL_SIGN)
                                    {
                                        int SL = listMarkBySubjectEdu.Where(o => o <= slc.MaxValue).Count()
                                            + ListTnbsBySubjectEdu.Where(o => o <= slc.MaxValue).Count();
                                        sheet_Ethnic.SetCellValue(startrow, 4 + n, SL);//E
                                        sheet_Ethnic.SetCellValue(startrow, 5 + n, "=IF(C" + startrow + ">0" + "," + "ROUND(" + UtilsBusiness.GetExcelColumnName(4 + n) + startrow + "/" + "C" + startrow + "*100,2),0)");
                                    }
                                    else if (slc.SignMax == SystemParamsInFile.Sign_Compare.SMALLER_SIGN)
                                    {
                                        int SL = listMarkBySubjectEdu.Where(o => o < slc.MaxValue).Count()
                                            + ListTnbsBySubjectEdu.Where(o => o < slc.MaxValue).Count();
                                        sheet_Ethnic.SetCellValue(startrow, 4 + n, SL);//E
                                        sheet_Ethnic.SetCellValue(startrow, 5 + n, "=IF(C" + startrow + ">0" + "," + "ROUND(" + UtilsBusiness.GetExcelColumnName(4 + n) + startrow + "/" + "C" + startrow + "*100,2),0)");
                                    }
                                    else if (slc.SignMax == SystemParamsInFile.Sign_Compare.EQUAL_SIGN)
                                    {
                                        int SL = listMarkBySubjectEdu.Where(o => o == slc.MaxValue).Count()
                                            + ListTnbsBySubjectEdu.Where(o => o == slc.MaxValue).Count();
                                        sheet_Ethnic.SetCellValue(startrow, 4 + n, SL);//E
                                        sheet_Ethnic.SetCellValue(startrow, 5 + n, "=IF(C" + startrow + ">0" + "," + "ROUND(" + UtilsBusiness.GetExcelColumnName(4 + n) + startrow + "/" + "C" + startrow + "*100,2),0)");
                                    }
                                }
                                n += 2;
                            }
                        }
                        n = 0;
                        //Điền dữ liệu Đ, CĐ
                        int positionD = numberConfig * 2 + 4;
                        if (lstSubject[i].IsCommenting == SystemParamsInFile.ISCOMMENTING_TYPE_JUDGE)
                        {
                            int SL = listJudgeBySubjectEdu_Subject.Where(o => o.Judgement.ToUpper().Equals(SystemParamsInFile.JUDGE_MARK_D) && o.EducationLevelID == EducationLevelID).Count()
                                + listTnbsBySubjectEdu_Subject.Where(o => o.Judgement.ToUpper().Equals(SystemParamsInFile.JUDGE_MARK_D) && o.EducationLevelID == EducationLevelID).Count();
                            sheet_Ethnic.SetCellValue(startrow, positionD + n, SL);//E
                            sheet_Ethnic.SetCellValue(startrow, positionD + n + 1, "=IF(C" + startrow + ">0" + "," + "ROUND(" + UtilsBusiness.GetExcelColumnName(positionD + n) + startrow + "/" + "C" + startrow + "*100,2),0)");
                            n += 2;
                            int SLCD = listJudgeBySubjectEdu_Subject.Where(o => o.Judgement.ToUpper().Equals(SystemParamsInFile.JUDGE_MARK_CD) && o.EducationLevelID == EducationLevelID).Count()
                                + listTnbsBySubjectEdu_Subject.Where(o => o.Judgement.ToUpper().Equals(SystemParamsInFile.JUDGE_MARK_CD) && o.EducationLevelID == EducationLevelID).Count();
                            sheet_Ethnic.SetCellValue(startrow, positionD + n, SLCD);//E
                            sheet_Ethnic.SetCellValue(startrow, positionD + n + 1, "=IF(C" + startrow + ">0" + "," + "ROUND(" + UtilsBusiness.GetExcelColumnName(positionD + n) + startrow + "/" + "C" + startrow + "*100,2),0)");
                        }


                        // Sĩ Số
                        var objCountPupil = listCountPupilByEducation.Where(o => o.EducationLevelID == EducationLevelID && o.EthnicID.HasValue && o.EthnicID != EthnicID_ForeignPeople && o.EthnicID != EthnicID_Kinh && o.SubjectID == SubjectID);
                        int SumPupil = objCountPupil != null ? objCountPupil.Sum(o => o.TotalPupil) : 0;

                        FomulerSumpupil += "C" + startrow + ",";
                        // fill vào excel
                        sheet_Ethnic.SetCellValue(startrow, 2, lstEducationLevel[j].Resolution);
                        sheet_Ethnic.SetCellValue(startrow, 3, SumPupil);
                        startrow++;
                    }

                    sheet_Ethnic.MergeColumn(1, startmerge, startrow);
                    sheet_Ethnic.GetRange(startmerge, 1, startrow, 1).SetBorder(VTBorderStyle.Solid, VTBorderWeight.Medium, VTBorderIndex.EdgeLeft);
                    sheet.GetRange(startmerge, 1, startrow, 1).SetBorder(VTBorderStyle.Solid, VTBorderWeight.Medium, VTBorderIndex.EdgeLeft);

                    startrow++;
                }
                if (entity.AppliedLevel == SystemParamsInFile.APPLIED_LEVEL_SECONDARY)
                {
                    sheet_Ethnic.GetRange(7, (numberConfig + 2) * 2 + 3, countSubject * 5 + 8, (numberConfig + 2) * 2 + 3).SetBorder(VTBorderStyle.Solid, VTBorderWeight.Medium, VTBorderIndex.EdgeRight);
                }
                else
                {
                    sheet_Ethnic.GetRange(7, (numberConfig + 2) * 2 + 3, countSubject * 4 + 8, (numberConfig + 2) * 2 + 3).SetBorder(VTBorderStyle.Solid, VTBorderWeight.Medium, VTBorderIndex.EdgeRight);
                }
                sheet_Ethnic.FitAllColumnsOnOnePage = true;
                sheet_Ethnic.SetCellValue(UtilsBusiness.GetExcelColumnName(1) + (startrow), "(*): Tổng số học sinh của lớp không bao gồm các học sinh được miễn giảm môn tương ứng");
                sheet_Ethnic.SetFontName("Times New Roman", 0);
                #endregion
            }
            if (entity.FemaleEthnicChecked)
            {
                #region Tạo file báo cáo điểm thi học kỳ học sinh nữ dân tộc
                IVTWorksheet sheet_FemaleEthnic = oBook.CopySheetToLast(sheet);
                sheet_FemaleEthnic.SetCellValue("B4", "THỐNG KÊ ĐIỂM THI HỌC KỲ HỌC SINH NỮ DÂN TỘC");
                sheet_FemaleEthnic.Name = "HS_NU_DT";
                startrow = 9;
                column = "";
                for (int i = 0; i < lstSubject.Count; i++)
                {
                    int startmin = startrow;
                    int startmerge = startrow;
                    string FomulerSumpupil = "";
                    int SubjectID = lstSubject[i].SubjectCatID;
                    sheet_FemaleEthnic.CopyPasteSameRowHeigh(rangeSubject, startrow);
                    sheet_FemaleEthnic.SetCellValue(startmerge, 1, lstSubject[i].DisplayName);
                    var listMarkBySubjectEdu_Subject = lstMarkRecord_FemaleEthnic.Where(o => o.SubjectID == SubjectID).ToList();
                    var listJudgeBySubjectEdu_Subject = lstJudgeRecord_FemaleEthnic.Where(o => o.SubjectID == SubjectID).ToList();
                    var listTnbsBySubjectEdu_Subject = lstTnbs_FemaleEthnic.Where(o => o.SubjectID == SubjectID).ToList();

                    for (int j = 0; j < lstEducationLevel.Count; j++)
                    {
                        int EducationLevelID = lstEducationLevel[j].EducationLevelID;
                        //Copy row style
                        if (j == 0)
                            sheet_FemaleEthnic.CopyPasteSameRowHeigh(rangefirst, startrow);
                        else
                            sheet_FemaleEthnic.CopyPasteSameRowHeigh(range, startrow);

                        var listMarkBySubjectEdu = listMarkBySubjectEdu_Subject.Where(o => o.EducationLevelID == EducationLevelID).Select(o => o.Mark).ToList();
                        var ListTnbsBySubjectEdu = listTnbsBySubjectEdu_Subject.Where(o => o.EducationLevelID == EducationLevelID).Select(o => o.Mark).ToList();
                        n = 0;
                        if (lstSubject[i].IsCommenting == SystemParamsInFile.ISCOMMENTING_TYPE_MARK || lstSubject[i].IsCommenting == SystemParamsInFile.ISCOMMENTING_TYPE_MARK_JUDGE)
                        {
                            for (int m = 0; m < numberConfig; m++)
                            {
                                StatisticLevelConfig slc = lstStatisticLevelConfig[m];
                                if (slc.SignMin == SystemParamsInFile.Sign_Compare.GREATER_OR_EQUAL_SIGN)
                                {
                                    if (slc.SignMax == SystemParamsInFile.Sign_Compare.SMALLER_OR_EQUAL_SIGN)
                                    {
                                        int SL = listMarkBySubjectEdu.Where(o => o >= slc.MinValue && o <= slc.MaxValue).Count()
                                            + ListTnbsBySubjectEdu.Where(o => o >= slc.MinValue && o <= slc.MaxValue).Count();

                                        sheet_FemaleEthnic.SetCellValue(startrow, 4 + n, SL);//E
                                        sheet_FemaleEthnic.SetCellValue(startrow, 5 + n, "=IF(C" + startrow + ">0" + "," + "ROUND(" + UtilsBusiness.GetExcelColumnName(4 + n) + startrow + "/" + "C" + startrow + "*100,2),0)");
                                    }
                                    else if (slc.SignMax == SystemParamsInFile.Sign_Compare.SMALLER_SIGN)
                                    {
                                        int SL = listMarkBySubjectEdu.Where(o => o >= slc.MinValue && o < slc.MaxValue).Count()
                                            + ListTnbsBySubjectEdu.Where(o => o >= slc.MinValue && o < slc.MaxValue).Count();

                                        sheet_FemaleEthnic.SetCellValue(startrow, 4 + n, SL);//E
                                        sheet_FemaleEthnic.SetCellValue(startrow, 5 + n, "=IF(C" + startrow + ">0" + "," + "ROUND(" + UtilsBusiness.GetExcelColumnName(4 + n) + startrow + "/" + "C" + startrow + "*100,2),0)");
                                    }
                                    else if (slc.SignMax == SystemParamsInFile.Sign_Compare.EQUAL_SIGN)
                                    {
                                        int SL = listMarkBySubjectEdu.Where(o => o >= slc.MinValue && o == slc.MaxValue).Count()
                                            + ListTnbsBySubjectEdu.Where(o => o >= slc.MinValue && o == slc.MaxValue).Count();

                                        sheet_FemaleEthnic.SetCellValue(startrow, 4 + n, SL);//E
                                        sheet_FemaleEthnic.SetCellValue(startrow, 5 + n, "=IF(C" + startrow + ">0" + "," + "ROUND(" + UtilsBusiness.GetExcelColumnName(4 + n) + startrow + "/" + "C" + startrow + "*100,2),0)");
                                    }
                                    else
                                    {
                                        int SL = listMarkBySubjectEdu.Where(o => o >= slc.MinValue).Count()
                                            + ListTnbsBySubjectEdu.Where(o => o >= slc.MinValue).Count();

                                        sheet_FemaleEthnic.SetCellValue(startrow, 4 + n, SL);//E
                                        sheet_FemaleEthnic.SetCellValue(startrow, 5 + n, "=IF(C" + startrow + ">0" + "," + "ROUND(" + UtilsBusiness.GetExcelColumnName(4 + n) + startrow + "/" + "C" + startrow + "*100,2),0)");
                                    }
                                }
                                else if (slc.SignMin == SystemParamsInFile.Sign_Compare.GREATEER_THAN_SIGN)
                                {
                                    if (slc.SignMax == SystemParamsInFile.Sign_Compare.SMALLER_OR_EQUAL_SIGN)
                                    {
                                        int SL = listMarkBySubjectEdu.Where(o => o > slc.MinValue && o <= slc.MaxValue).Count()
                                            + ListTnbsBySubjectEdu.Where(o => o > slc.MinValue && o <= slc.MaxValue).Count();

                                        sheet_FemaleEthnic.SetCellValue(startrow, 4 + n, SL);//E
                                        sheet_FemaleEthnic.SetCellValue(startrow, 5 + n, "=IF(C" + startrow + ">0" + "," + "ROUND(" + UtilsBusiness.GetExcelColumnName(4 + n) + startrow + "/" + "C" + startrow + "*100,2),0)");
                                    }
                                    else if (slc.SignMax == SystemParamsInFile.Sign_Compare.SMALLER_SIGN)
                                    {
                                        int SL = listMarkBySubjectEdu.Where(o => o > slc.MinValue && o < slc.MaxValue).Count()
                                            + ListTnbsBySubjectEdu.Where(o => o > slc.MinValue && o < slc.MaxValue).Count();

                                        sheet_FemaleEthnic.SetCellValue(startrow, 4 + n, SL);//E
                                        sheet_FemaleEthnic.SetCellValue(startrow, 5 + n, "=IF(C" + startrow + ">0" + "," + "ROUND(" + UtilsBusiness.GetExcelColumnName(4 + n) + startrow + "/" + "C" + startrow + "*100,2),0)");
                                    }
                                    else if (slc.SignMax == SystemParamsInFile.Sign_Compare.EQUAL_SIGN)
                                    {
                                        int SL = listMarkBySubjectEdu.Where(o => o > slc.MinValue && o == slc.MaxValue).Count()
                                            + ListTnbsBySubjectEdu.Where(o => o > slc.MinValue && o == slc.MaxValue).Count();

                                        sheet_FemaleEthnic.SetCellValue(startrow, 4 + n, SL);//E
                                        sheet_FemaleEthnic.SetCellValue(startrow, 5 + n, "=IF(C" + startrow + ">0" + "," + "ROUND(" + UtilsBusiness.GetExcelColumnName(4 + n) + startrow + "/" + "C" + startrow + "*100,2),0)");
                                    }
                                    else
                                    {
                                        int SL = listMarkBySubjectEdu.Where(o => o > slc.MinValue).Count()
                                            + ListTnbsBySubjectEdu.Where(o => o > slc.MinValue).Count();

                                        sheet_FemaleEthnic.SetCellValue(startrow, 4 + n, SL);//E
                                        sheet_FemaleEthnic.SetCellValue(startrow, 5 + n, "=IF(C" + startrow + ">0" + "," + "ROUND(" + UtilsBusiness.GetExcelColumnName(4 + n) + startrow + "/" + "C" + startrow + "*100,2),0)");
                                    }
                                }
                                else
                                {
                                    if (slc.SignMax == SystemParamsInFile.Sign_Compare.SMALLER_OR_EQUAL_SIGN)
                                    {
                                        int SL = listMarkBySubjectEdu.Where(o => o <= slc.MaxValue).Count()
                                            + ListTnbsBySubjectEdu.Where(o => o <= slc.MaxValue).Count();

                                        sheet_FemaleEthnic.SetCellValue(startrow, 4 + n, SL);//E
                                        sheet_FemaleEthnic.SetCellValue(startrow, 5 + n, "=IF(C" + startrow + ">0" + "," + "ROUND(" + UtilsBusiness.GetExcelColumnName(4 + n) + startrow + "/" + "C" + startrow + "*100,2),0)");
                                    }
                                    else if (slc.SignMax == SystemParamsInFile.Sign_Compare.SMALLER_SIGN)
                                    {
                                        int SL = listMarkBySubjectEdu.Where(o => o < slc.MaxValue).Count()
                                            + ListTnbsBySubjectEdu.Where(o => o < slc.MaxValue).Count();

                                        sheet_FemaleEthnic.SetCellValue(startrow, 4 + n, SL);//E
                                        sheet_FemaleEthnic.SetCellValue(startrow, 5 + n, "=IF(C" + startrow + ">0" + "," + "ROUND(" + UtilsBusiness.GetExcelColumnName(4 + n) + startrow + "/" + "C" + startrow + "*100,2),0)");
                                    }
                                    else if (slc.SignMax == SystemParamsInFile.Sign_Compare.EQUAL_SIGN)
                                    {
                                        int SL = listMarkBySubjectEdu.Where(o => o == slc.MaxValue).Count()
                                            + ListTnbsBySubjectEdu.Where(o => o == slc.MaxValue).Count();

                                        sheet_FemaleEthnic.SetCellValue(startrow, 4 + n, SL);//E
                                        sheet_FemaleEthnic.SetCellValue(startrow, 5 + n, "=IF(C" + startrow + ">0" + "," + "ROUND(" + UtilsBusiness.GetExcelColumnName(4 + n) + startrow + "/" + "C" + startrow + "*100,2),0)");
                                    }
                                }
                                n += 2;
                            }
                        }
                        n = 0;
                        //Điền dữ liệu Đ, CĐ
                        int positionD = numberConfig * 2 + 4;
                        if (lstSubject[i].IsCommenting == SystemParamsInFile.ISCOMMENTING_TYPE_JUDGE)
                        {
                            int SL = listJudgeBySubjectEdu_Subject.Where(o => o.Judgement.ToUpper().Equals(SystemParamsInFile.JUDGE_MARK_D) && o.EducationLevelID == EducationLevelID).Count()
                                + listTnbsBySubjectEdu_Subject.Where(o => o.Judgement.ToUpper().Equals(SystemParamsInFile.JUDGE_MARK_D) && o.EducationLevelID == EducationLevelID).Count();

                            sheet_FemaleEthnic.SetCellValue(startrow, positionD + n, SL);//E
                            sheet_FemaleEthnic.SetCellValue(startrow, positionD + n + 1, "=IF(C" + startrow + ">0" + "," + "ROUND(" + UtilsBusiness.GetExcelColumnName(positionD + n) + startrow + "/" + "C" + startrow + "*100,2),0)");
                            n += 2;
                            int SLCD = listJudgeBySubjectEdu_Subject.Where(o => o.Judgement.ToUpper().Equals(SystemParamsInFile.JUDGE_MARK_CD) && o.EducationLevelID == EducationLevelID).Count()
                                + listTnbsBySubjectEdu_Subject.Where(o => o.Judgement.ToUpper().Equals(SystemParamsInFile.JUDGE_MARK_CD) && o.EducationLevelID == EducationLevelID).Count();

                            sheet_FemaleEthnic.SetCellValue(startrow, positionD + n, SLCD);//E
                            sheet_FemaleEthnic.SetCellValue(startrow, positionD + n + 1, "=IF(C" + startrow + ">0" + "," + "ROUND(" + UtilsBusiness.GetExcelColumnName(positionD + n) + startrow + "/" + "C" + startrow + "*100,2),0)");
                        }


                        // Sĩ Số
                        var objCountPupil = listCountPupilByEducation.Where(o => o.Genre == GlobalConstants.GENRE_FEMALE && o.EducationLevelID == EducationLevelID && o.EthnicID.HasValue && o.EthnicID != EthnicID_ForeignPeople && o.EthnicID != EthnicID_Kinh && o.SubjectID == SubjectID);
                        int SumPupil = objCountPupil != null ? objCountPupil.Sum(o => o.TotalPupil) : 0;

                        FomulerSumpupil += "C" + startrow + ",";
                        // fill vào excel
                        sheet_FemaleEthnic.SetCellValue(startrow, 2, lstEducationLevel[j].Resolution);
                        sheet_FemaleEthnic.SetCellValue(startrow, 3, SumPupil);
                        startrow++;
                    }

                    sheet_FemaleEthnic.MergeColumn(1, startmerge, startrow);
                    sheet_FemaleEthnic.GetRange(startmerge, 1, startrow, 1).SetBorder(VTBorderStyle.Solid, VTBorderWeight.Medium, VTBorderIndex.EdgeLeft);
                    sheet.GetRange(startmerge, 1, startrow, 1).SetBorder(VTBorderStyle.Solid, VTBorderWeight.Medium, VTBorderIndex.EdgeLeft);

                    startrow++;
                }
                if (entity.AppliedLevel == SystemParamsInFile.APPLIED_LEVEL_SECONDARY)
                {
                    sheet_FemaleEthnic.GetRange(7, (numberConfig + 2) * 2 + 3, countSubject * 5 + 8, (numberConfig + 2) * 2 + 3).SetBorder(VTBorderStyle.Solid, VTBorderWeight.Medium, VTBorderIndex.EdgeRight);
                }
                else
                {
                    sheet_FemaleEthnic.GetRange(7, (numberConfig + 2) * 2 + 3, countSubject * 4 + 8, (numberConfig + 2) * 2 + 3).SetBorder(VTBorderStyle.Solid, VTBorderWeight.Medium, VTBorderIndex.EdgeRight);
                }
                sheet_FemaleEthnic.FitAllColumnsOnOnePage = true;
                sheet_FemaleEthnic.SetCellValue(UtilsBusiness.GetExcelColumnName(1) + (startrow), "(*): Tổng số học sinh của lớp không bao gồm các học sinh được miễn giảm môn tương ứng");
                sheet_FemaleEthnic.SetFontName("Times New Roman", 0);
                #endregion
            }
            //Xoá sheet template
            firstSheet.Delete();

            return oBook.ToStream();
        }
        #endregion
    }
}