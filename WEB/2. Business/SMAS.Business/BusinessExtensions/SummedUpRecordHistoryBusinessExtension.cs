﻿/**
 * The Viettel License
 *
 * Copyright 2012 Viettel Telecom. All rights reserved.
 * VIETTEL PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */

/**
* @author  tungnd
* @version $Revision: $
*/

using System;
using System.Collections.Generic;
using System.Linq;
using SMAS.Business.BusinessObject;
using SMAS.Business.Common;
using SMAS.DAL.Repository;
using SMAS.Models.Models;
using System.Web;

namespace SMAS.Business.Business
{
    public partial class SummedUpRecordHistoryBusiness
    {
        public void DeleteSummedUpRecordHistory(int UserID, int SchoolID, int AcademicYearID, int ClassID, int Semester, int? PeriodID, int SubjectID, List<int> listPupilID)
        {
            List<int> lsPupilId = listPupilID.Distinct().ToList();
            if (!AcademicYearBusiness.IsCurrentYear(AcademicYearID))
            {
                throw new BusinessException("Common_Validate_NotIsCurrentYear");
            }

            SubjectCat subjectCat = SubjectCatBusiness.Find(SubjectID);

            if (!subjectCat.IsApprenticeshipSubject && !UtilsBusiness.HasSubjectTeacherPermission(UserID, ClassID, SubjectID))
                throw new BusinessException("Common_Label_HasSubjectTeacherPermission");

            if (subjectCat.IsApprenticeshipSubject != true)
            {
                IDictionary<string, object> SearchPOC = new Dictionary<string, object>();
                SearchPOC["ClassID"] = ClassID;
                SearchPOC["Check"] = "Check";
                List<PupilOfClass> lstPOC = PupilOfClassBusiness.SearchBySchool(SchoolID, SearchPOC).ToList();

                if (lstPOC.Any(u => listPupilID.Contains(u.PupilID) && u.Status != SystemParamsInFile.PUPIL_STATUS_STUDYING))
                    throw new BusinessException("Common_Validate_Pupil_Status_Studying");
            }

            //Tìm điểm theo hoc ky
            IDictionary<string, object> dic = new Dictionary<string, object>();
            dic["AcademicYearID"] = AcademicYearID;
            dic["SubjectID"] = SubjectID;
            dic["Semester"] = Semester;
            dic["PeriodID"] = PeriodID;
            dic["ListPupilID"] = listPupilID;
            List<SummedUpRecordHistory> listSummedUp = SearchBySchool(SchoolID, dic).ToList();
            //base.DeleteAll(listSummedUp);
            //Xoa diem hoc ky
            List<int> listPupilIDDelete = listSummedUp.Select(p => p.PupilID).Distinct().ToList();
            MarkRecordBusiness.SP_DeleteSummedUpRecord(1, listPupilIDDelete, PeriodID != null ? PeriodID.Value : 0, SubjectID, ClassID, Semester, SchoolID, AcademicYearID);// khong phai historyId = 0, period = 0

            dic = new Dictionary<string, object>();
            dic["AcademicYearID"] = AcademicYearID;
            dic["SchoolID"] = SchoolID;
            dic["SubjectID"] = SubjectID;
            ClassSubject classSubjectObj = ClassSubjectBusiness.SearchByClass(ClassID, dic).FirstOrDefault();

            //Tìm điểm cả năm nếu đợt không có
            if (!PeriodID.HasValue && (Semester == SystemParamsInFile.SEMESTER_OF_YEAR_SECOND || classSubjectObj.SectionPerWeekSecondSemester == 0))
            {
                IDictionary<string, object> dicIII = new Dictionary<string, object>();
                dic["AcademicYearID"] = AcademicYearID;
                dic["SubjectID"] = SubjectID;
                dic["ClassID"] = ClassID;
                dic["ListPupilID"] = listPupilID;
                dic["Semester"] = SystemParamsInFile.SEMESTER_OF_YEAR_ALL;
                List<SummedUpRecordHistory> sumCN = SearchBySchool(SchoolID, dic).ToList();
                //base.DeleteAll(sumCN);
                //Xoa diem hoc ky
                List<int> listSummedPupilIDDelete = sumCN.Select(p => p.PupilID).Distinct().ToList();
                MarkRecordBusiness.SP_DeleteSummedUpRecord(1, listSummedPupilIDDelete, PeriodID != null ? PeriodID.Value : 0, SubjectID, ClassID, Semester, SchoolID, AcademicYearID);// khong phai historyId = 0, period = 0
            }

            //xoa voi mon mien giam
            Dictionary<string, object> search = new Dictionary<string, object>();
            search["ClassID"] = ClassID;
            search["SubjectID"] = SubjectID;
            search["Semester"] = Semester;
            search["PeriodID"] = PeriodID;
            List<int> listExemptedSubjectII = ExemptedSubjectBusiness.GetListExemptedSubject(ClassID, SystemParamsInFile.SEMESTER_OF_YEAR_SECOND).Select(u => u.PupilID).ToList();

            listPupilID = listPupilID.Where(u => listExemptedSubjectII.Contains(u)).ToList();

            if (listPupilID != null && listPupilID.Count() > 0)
            {
                IDictionary<string, object> dicIII = new Dictionary<string, object>();
                dic["AcademicYearID"] = AcademicYearID;
                dic["SubjectID"] = SubjectID;
                dic["ClassID"] = ClassID;
                //xoa diem voi hoc sinh bi mien giam hoc ky II
                dic["ListPupilID"] = listPupilID;
                dic["Semester"] = SystemParamsInFile.SEMESTER_OF_YEAR_ALL;
                List<SummedUpRecordHistory> sumCN = SearchBySchool(SchoolID, dic).ToList();
                //base.DeleteAll(sumCN);
                //Xoa diem hoc ky
                List<int> listPupilIDSummedRecord = sumCN.Select(p => p.PupilID).Distinct().ToList();
                MarkRecordBusiness.SP_DeleteSummedUpRecord(1, listPupilIDSummedRecord, PeriodID != null ? PeriodID.Value : 0,SubjectID,ClassID,Semester,SchoolID,AcademicYearID);// khong phai historyId = 0, period = 0
            }

            //Sau khi xoa tinh lai diem TBM
            string strMarkTitle = PeriodID.HasValue ? MarkRecordBusiness.GetMarkTitle(PeriodID.Value) : MarkRecordBusiness.GetMarkSemesterTitle(SchoolID, AcademicYearID, Semester);
            MarkRecordHistoryBusiness.SummedUpMarkHis(lsPupilId, SchoolID, AcademicYearID, Semester, PeriodID, strMarkTitle, classSubjectObj);
        }


        #region
        public IQueryable<SummedUpRecordHistory> SearchBySchoolPrimary(int SchoolID, IDictionary<string, object> SearchInfo)
        {
            if (SchoolID == 0)
            {
                return null;
            }
            else
            {
                SearchInfo["SchoolID"] = SchoolID;
                return SearchPrimary(SearchInfo);
            }
        }
        private IQueryable<SummedUpRecordHistory> SearchPrimary(IDictionary<string, object> dic)
        {
            int SummedUpRecordID = Utils.GetInt(dic, "SummedUpRecordID");

            int PupilID = Utils.GetInt(dic, "PupilID");

            int ClassID = Utils.GetInt(dic, "ClassID");

            int? SchoolID = Utils.GetInt(dic, "SchoolID");

            int AcademicYearID = Utils.GetInt(dic, "AcademicYearID");

            int SubjectID = Utils.GetInt(dic, "SubjectID");

            int AppliedLevel = Utils.GetInt(dic, "AppliedLevel");

            int EducationLevelID = Utils.GetInt(dic, "EducationLevelID");

            int? Year = Utils.GetInt(dic, "Year");

            //  public Nullable<int> Semester { get; set; }
            int Semester = Utils.GetInt(dic, "Semester", -1);

            int? PeriodID = Utils.GetNullableInt(dic, "PeriodID");

            double SummedUpMark = Utils.GetDouble(dic, "SummedUpMark");

            string JudgementResult = Utils.GetString(dic, "JudgementResult");

            string Comment = Utils.GetString(dic, "Comment");

            DateTime? SummedUpDate = Utils.GetDateTime(dic, "SummedUpDate");

            double? SummedUpFromMark = Utils.GetNullableDouble(dic, "SummedUpFromMark");

            double? SummedUpToMark = Utils.GetNullableDouble(dic, "SummedUpToMark");
            List<int> lstPupilID = Utils.GetIntList(dic, "ListPupilID");
            string checkWithClassMovement = Utils.GetString(dic, "checkWithClassMovement");
            string NoCheckIsActivePupilSchool = Utils.GetString(dic, "NoCheckIsActivePupilSchool");

            IQueryable<SummedUpRecordHistory> lsSummedUpRecordHistory = this.SummedUpRecordHistoryRepository.All;

            //Bổ sung tìm kiếm theo IsActive
            if (string.IsNullOrWhiteSpace(NoCheckIsActivePupilSchool))
            {
                lsSummedUpRecordHistory = lsSummedUpRecordHistory.Where(o => o.PupilProfile.IsActive == true && o.SchoolProfile.IsActive == true);
            }
            // bắt đầu tìm kiếm
            if (SummedUpRecordID != 0)
            {
                lsSummedUpRecordHistory = lsSummedUpRecordHistory.Where(su => (su.SummedUpRecordID == SummedUpRecordID));
            }
            if (PupilID != 0)
            {
                lsSummedUpRecordHistory = lsSummedUpRecordHistory.Where(su => (su.PupilID == PupilID));
            }
            if (ClassID != 0)
            {
                lsSummedUpRecordHistory = lsSummedUpRecordHistory.Where(su => (su.ClassID == ClassID));
            }
            if (SchoolID != 0)
            {
                lsSummedUpRecordHistory = lsSummedUpRecordHistory.Where(su => (su.SchoolID == SchoolID));
            }
            if (AcademicYearID != 0)
            {
                lsSummedUpRecordHistory = lsSummedUpRecordHistory.Where(su => (su.AcademicYearID == AcademicYearID));
            }
            if (lstPupilID != null && lstPupilID.Count() > 0)
            {
                lsSummedUpRecordHistory = lsSummedUpRecordHistory.Where(o => (lstPupilID.Contains(o.PupilID)));
            }
            if (SubjectID != 0)
            {
                lsSummedUpRecordHistory = lsSummedUpRecordHistory.Where(su => (su.SubjectID == SubjectID));
            }

            if (AppliedLevel != 0)
            {
                lsSummedUpRecordHistory = lsSummedUpRecordHistory.Where(su => (su.ClassProfile.EducationLevel.Grade == AppliedLevel));
            }
            if (EducationLevelID != 0)
            {
                lsSummedUpRecordHistory = lsSummedUpRecordHistory.Where(su => (su.ClassProfile.EducationLevelID == EducationLevelID));
            }

            if (Year != 0)
            {
                lsSummedUpRecordHistory = lsSummedUpRecordHistory.Where(su => (su.CreatedAcademicYear == Year));
            }
            if (Semester != -1)
            {
                if (Semester == 1)
                {
                    lsSummedUpRecordHistory = lsSummedUpRecordHistory.Where(su => su.Semester == Semester);// xoa diem xoa luon diem TBCN
                }
                if (Semester == 2)
                {
                    lsSummedUpRecordHistory = lsSummedUpRecordHistory.Where(su => (su.Semester == Semester || su.Semester == 5) && su.ReTestMark != null);// lay diem thi lai de xoa
                }
            }
            if (checkWithClassMovement.Length == 0)
            {
                if (PeriodID == null)
                {
                    lsSummedUpRecordHistory = lsSummedUpRecordHistory.Where(su => su.PeriodID == null);

                }
                else
                {
                    lsSummedUpRecordHistory = lsSummedUpRecordHistory.Where(su => su.PeriodID == PeriodID.Value);
                }
            }

            if (SummedUpFromMark.HasValue)
            {
                lsSummedUpRecordHistory = lsSummedUpRecordHistory.Where(su => (su.SummedUpMark >= (decimal)SummedUpFromMark));
            }

            if (SummedUpToMark.HasValue)
            {
                lsSummedUpRecordHistory = lsSummedUpRecordHistory.Where(su => (su.SummedUpMark <= (decimal)SummedUpToMark));
            }
            if (JudgementResult.Length != 0)
            {
                lsSummedUpRecordHistory = lsSummedUpRecordHistory.Where(su => (su.JudgementResult == JudgementResult));
            }
            if (SummedUpDate != null)
            {
                lsSummedUpRecordHistory = lsSummedUpRecordHistory.Where(su => (su.SummedUpDate == SummedUpDate));
            }

            return lsSummedUpRecordHistory;
        }
        #endregion
        public IQueryable<SummedUpRecordHistory> SearchBySchool(int SchoolID, IDictionary<string, object> SearchInfo)
        {
            if (SchoolID == 0)
            {
                return null;
            }
            else
            {
                SearchInfo["SchoolID"] = SchoolID;
                return Search(SearchInfo);
            }
        }
        private IQueryable<SummedUpRecordHistory> Search(IDictionary<string, object> dic)
        {
            int SummedUpRecordID = Utils.GetInt(dic, "SummedUpRecordID");

            int PupilID = Utils.GetInt(dic, "PupilID");

            int ClassID = Utils.GetInt(dic, "ClassID");

            int? SchoolID = Utils.GetInt(dic, "SchoolID");

            int AcademicYearID = Utils.GetInt(dic, "AcademicYearID");

            int SubjectID = Utils.GetInt(dic, "SubjectID");

            int AppliedLevel = Utils.GetInt(dic, "AppliedLevel");

            int EducationLevelID = Utils.GetInt(dic, "EducationLevelID");

            int? Year = Utils.GetInt(dic, "Year");

            //  public Nullable<int> Semester { get; set; }
            int Semester = Utils.GetInt(dic, "Semester", -1);

            int? PeriodID = Utils.GetNullableInt(dic, "PeriodID");

            double SummedUpMark = Utils.GetDouble(dic, "SummedUpMark");

            string JudgementResult = Utils.GetString(dic, "JudgementResult");

            string Comment = Utils.GetString(dic, "Comment");

            DateTime? SummedUpDate = Utils.GetDateTime(dic, "SummedUpDate");

            double? SummedUpFromMark = Utils.GetNullableDouble(dic, "SummedUpFromMark");

            double? SummedUpToMark = Utils.GetNullableDouble(dic, "SummedUpToMark");
            List<int> lstPupilID = Utils.GetIntList(dic, "ListPupilID");
            string checkWithClassMovement = Utils.GetString(dic, "checkWithClassMovement");
            string NoCheckIsActivePupilSchool = Utils.GetString(dic, "NoCheckIsActivePupilSchool");

            IQueryable<SummedUpRecordHistory> lsSummedUpRecordHistory = this.SummedUpRecordHistoryRepository.All;

            //Bổ sung tìm kiếm theo IsActive
            if (string.IsNullOrWhiteSpace(NoCheckIsActivePupilSchool))
            {
                lsSummedUpRecordHistory = lsSummedUpRecordHistory.Where(o => o.PupilProfile.IsActive == true && o.SchoolProfile.IsActive == true);
            }
            // bắt đầu tìm kiếm
            if (SummedUpRecordID != 0)
            {
                lsSummedUpRecordHistory = lsSummedUpRecordHistory.Where(su => (su.SummedUpRecordID == SummedUpRecordID));
            }
            if (PupilID != 0)
            {
                lsSummedUpRecordHistory = lsSummedUpRecordHistory.Where(su => (su.PupilID == PupilID));
            }
            if (ClassID != 0)
            {
                lsSummedUpRecordHistory = lsSummedUpRecordHistory.Where(su => (su.ClassID == ClassID));
            }
            if (SchoolID != 0)
            {
                lsSummedUpRecordHistory = lsSummedUpRecordHistory.Where(su => (su.SchoolID == SchoolID));
            }
            if (AcademicYearID != 0)
            {
                lsSummedUpRecordHistory = lsSummedUpRecordHistory.Where(su => (su.AcademicYearID == AcademicYearID));
            }
            if (lstPupilID != null && lstPupilID.Count() > 0)
            {
                lsSummedUpRecordHistory = lsSummedUpRecordHistory.Where(o => (lstPupilID.Contains(o.PupilID)));
            }
            if (SubjectID != 0)
            {
                lsSummedUpRecordHistory = lsSummedUpRecordHistory.Where(su => (su.SubjectID == SubjectID));
            }

            if (AppliedLevel != 0)
            {
                lsSummedUpRecordHistory = lsSummedUpRecordHistory.Where(su => (su.ClassProfile.EducationLevel.Grade == AppliedLevel));
            }
            if (EducationLevelID != 0)
            {
                lsSummedUpRecordHistory = lsSummedUpRecordHistory.Where(su => (su.ClassProfile.EducationLevelID == EducationLevelID));
            }

            if (Year != 0)
            {
                lsSummedUpRecordHistory = lsSummedUpRecordHistory.Where(su => (su.CreatedAcademicYear == Year));
            }
            if (Semester != -1)
            {
                lsSummedUpRecordHistory = lsSummedUpRecordHistory.Where(su => su.Semester == Semester);// xoa diem xoa luon diem TBCN
            }
            if (checkWithClassMovement.Length == 0)
            {
                if (PeriodID == null)
                {
                    lsSummedUpRecordHistory = lsSummedUpRecordHistory.Where(su => su.PeriodID == null);

                }
                else
                {
                    lsSummedUpRecordHistory = lsSummedUpRecordHistory.Where(su => su.PeriodID == PeriodID.Value);
                }
            }

            if (SummedUpFromMark.HasValue)
            {
                lsSummedUpRecordHistory = lsSummedUpRecordHistory.Where(su => (su.SummedUpMark >= (decimal)SummedUpFromMark));
            }

            if (SummedUpToMark.HasValue)
            {
                lsSummedUpRecordHistory = lsSummedUpRecordHistory.Where(su => (su.SummedUpMark <= (decimal)SummedUpToMark));
            }
            if (JudgementResult.Length != 0)
            {
                lsSummedUpRecordHistory = lsSummedUpRecordHistory.Where(su => (su.JudgementResult == JudgementResult));
            }
            if (SummedUpDate != null)
            {
                lsSummedUpRecordHistory = lsSummedUpRecordHistory.Where(su => (su.SummedUpDate == SummedUpDate));
            }

            return lsSummedUpRecordHistory;
        }
        public IQueryable<SummedUpRecordHistory> SearchSummedUpHistory(IDictionary<string, object> dic)
        {
            int SummedUpRecordID = Utils.GetInt(dic, "SummedUpRecordID");

            int PupilID = Utils.GetInt(dic, "PupilID");

            int ClassID = Utils.GetInt(dic, "ClassID");

            int? SchoolID = Utils.GetInt(dic, "SchoolID");

            int AcademicYearID = Utils.GetInt(dic, "AcademicYearID");

            int SubjectID = Utils.GetInt(dic, "SubjectID");

            int AppliedLevel = Utils.GetInt(dic, "AppliedLevel");

            int EducationLevelID = Utils.GetInt(dic, "EducationLevelID");


            //  int IsCommenting = Utils.GetByte(dic,"IsCommenting");

            //int IsCommenting = Utils.GetInt(dic, "IsCommenting");

            int? Year = Utils.GetInt(dic, "Year");

            //  public Nullable<int> Semester { get; set; }
            int Semester = Utils.GetInt(dic, "Semester", -1);

            int? PeriodID = Utils.GetNullableInt(dic, "PeriodID");

            double SummedUpMark = Utils.GetDouble(dic, "SummedUpMark");

            string JudgementResult = Utils.GetString(dic, "JudgementResult");

            string Comment = Utils.GetString(dic, "Comment");

            DateTime? SummedUpDate = Utils.GetDateTime(dic, "SummedUpDate");

            double? SummedUpFromMark = Utils.GetNullableDouble(dic, "SummedUpFromMark");

            double? SummedUpToMark = Utils.GetNullableDouble(dic, "SummedUpToMark");
            List<int> lstPupilID = Utils.GetIntList(dic, "ListPupilID");
            string checkWithClassMovement = Utils.GetString(dic, "checkWithClassMovement");
            string NoCheckIsActivePupilSchool = Utils.GetString(dic, "NoCheckIsActivePupilSchool");

            IQueryable<SummedUpRecordHistory> lsSummedUpRecord = SummedUpRecordHistoryBusiness.All;
            //PhuongTD5
            //Bổ sung tìm kiếm theo IsActive
            if (string.IsNullOrWhiteSpace(NoCheckIsActivePupilSchool))
            {
                lsSummedUpRecord = lsSummedUpRecord.Where(o => o.PupilProfile.IsActive == true && o.SchoolProfile.IsActive == true);
            }
            //End Bổ sung tìm kiếm theo IsActive
            // bắt đầu tìm kiếm
            if (SummedUpRecordID != 0)
            {
                lsSummedUpRecord = lsSummedUpRecord.Where(su => (su.SummedUpRecordID == SummedUpRecordID));
            }
            if (PupilID != 0)
            {
                lsSummedUpRecord = lsSummedUpRecord.Where(su => (su.PupilID == PupilID));
            }
            if (ClassID != 0)
            {
                lsSummedUpRecord = lsSummedUpRecord.Where(su => (su.ClassID == ClassID));
            }
            if (SchoolID != 0)
            {
                lsSummedUpRecord = lsSummedUpRecord.Where(su => (su.SchoolID == SchoolID));
            }
            if (AcademicYearID != 0)
            {
                lsSummedUpRecord = lsSummedUpRecord.Where(su => (su.AcademicYearID == AcademicYearID));
            }
            if (lstPupilID != null && lstPupilID.Count() > 0)
            {
                lsSummedUpRecord = lsSummedUpRecord.Where(o => (lstPupilID.Contains(o.PupilID)));
            }
            if (SubjectID != 0)
            {
                lsSummedUpRecord = lsSummedUpRecord.Where(su => (su.SubjectID == SubjectID));
            }

            if (AppliedLevel != 0)
            {
                lsSummedUpRecord = lsSummedUpRecord.Where(su => (su.ClassProfile.EducationLevel.Grade == AppliedLevel));
            }
            if (EducationLevelID != 0)
            {
                lsSummedUpRecord = lsSummedUpRecord.Where(su => (su.ClassProfile.EducationLevelID == EducationLevelID));
            }

            if (Year != 0)
            {
                lsSummedUpRecord = lsSummedUpRecord.Where(su => (su.CreatedAcademicYear == Year));
            }
            if (Semester != -1)
            {
                lsSummedUpRecord = lsSummedUpRecord.Where(su => su.Semester == Semester);// xoa diem xoa luon diem TBCN
            }
            if (checkWithClassMovement.Length == 0)
            {
                if (PeriodID == null)
                {
                    lsSummedUpRecord = lsSummedUpRecord.Where(su => su.PeriodID == null);

                }
                else
                {
                    lsSummedUpRecord = lsSummedUpRecord.Where(su => su.PeriodID == PeriodID.Value);
                }
            }

            if (SummedUpFromMark.HasValue)
            {
                lsSummedUpRecord = lsSummedUpRecord.Where(su => (su.SummedUpMark >= (decimal)SummedUpFromMark));
            }

            if (SummedUpToMark.HasValue)
            {
                lsSummedUpRecord = lsSummedUpRecord.Where(su => (su.SummedUpMark <= (decimal)SummedUpToMark));
            }
            if (JudgementResult.Length != 0)
            {
                lsSummedUpRecord = lsSummedUpRecord.Where(su => (su.JudgementResult == JudgementResult));
            }
            if (SummedUpDate != null)
            {
                lsSummedUpRecord = lsSummedUpRecord.Where(su => (su.SummedUpDate == SummedUpDate));
            }

            return lsSummedUpRecord;
        }
        public void DeleteSummedUpRecordPrimaryHistory(int UserID, int SchoolID, int AcademicYearID, int ClassID, int Semester, int? PeriodID, int SubjectID, List<int> listPupilID)
        {
            if (!AcademicYearBusiness.IsCurrentYear(AcademicYearID))
            {
                throw new BusinessException("Common_Validate_NotIsCurrentYear");
            }

            SubjectCat subjectCat = SubjectCatBusiness.Find(SubjectID);

            if (!subjectCat.IsApprenticeshipSubject && !UtilsBusiness.HasSubjectTeacherPermission(UserID, ClassID, SubjectID))
                throw new BusinessException("Common_Label_HasSubjectTeacherPermission");

            if (subjectCat.IsApprenticeshipSubject != true)
            {
                IDictionary<string, object> SearchPOC = new Dictionary<string, object>();
                SearchPOC["ClassID"] = ClassID;
                SearchPOC["Check"] = "Check";
                List<PupilOfClass> lstPOC = PupilOfClassBusiness.SearchBySchool(SchoolID, SearchPOC).ToList();

                if (lstPOC.Any(u => listPupilID.Contains(u.PupilID) && u.Status != SystemParamsInFile.PUPIL_STATUS_STUDYING))
                    throw new BusinessException("Common_Validate_Pupil_Status_Studying");
            }

            //Tìm điểm theo hoc ky
            IDictionary<string, object> dic = new Dictionary<string, object>();
            dic["AcademicYearID"] = AcademicYearID;
            dic["SubjectID"] = SubjectID;
            dic["Semester"] = Semester;
            dic["PeriodID"] = PeriodID;
            dic["ListPupilID"] = listPupilID;
            List<SummedUpRecordHistory> listSummedUp = SearchBySchoolPrimary(SchoolID, dic).ToList();
            SummedUpRecordHistoryBusiness.DeleteAll(listSummedUp);

            dic = new Dictionary<string, object>();
            dic["AcademicYearID"] = AcademicYearID;
            dic["SchoolID"] = SchoolID;
            dic["SubjectID"] = SubjectID;
            ClassSubject classSubjectList = ClassSubjectBusiness.SearchByClass(ClassID, dic).FirstOrDefault();

            //Tìm điểm cả năm nếu đợt không có
            if (!PeriodID.HasValue && (Semester == SystemParamsInFile.SEMESTER_OF_YEAR_SECOND || classSubjectList.SectionPerWeekSecondSemester == 0))
            {
                IDictionary<string, object> dicIII = new Dictionary<string, object>();
                dic["AcademicYearID"] = AcademicYearID;
                dic["SubjectID"] = SubjectID;
                dic["ClassID"] = ClassID;
                dic["ListPupilID"] = listPupilID;
                dic["Semester"] = SystemParamsInFile.SEMESTER_OF_YEAR_ALL;
                List<SummedUpRecordHistory> sumCN = SearchBySchoolPrimary(SchoolID, dic).ToList();
                SummedUpRecordHistoryBusiness.DeleteAll(sumCN);
            }

            //xoa voi mon mien giam
            Dictionary<string, object> search = new Dictionary<string, object>();
            search["ClassID"] = ClassID;
            search["SubjectID"] = SubjectID;
            search["Semester"] = Semester;
            search["PeriodID"] = PeriodID;
            List<int> listExemptedSubjectII = ExemptedSubjectBusiness.GetListExemptedSubject(ClassID, SystemParamsInFile.SEMESTER_OF_YEAR_SECOND).Select(u => u.PupilID).ToList();

            listPupilID = listPupilID.Where(u => listExemptedSubjectII.Contains(u)).ToList();

            if (listPupilID != null && listPupilID.Count() > 0)
            {
                IDictionary<string, object> dicIII = new Dictionary<string, object>();
                dic["AcademicYearID"] = AcademicYearID;
                dic["SubjectID"] = SubjectID;
                dic["ClassID"] = ClassID;
                //xoa diem voi hoc sinh bi mien giam hoc ky II
                dic["ListPupilID"] = listPupilID;
                dic["Semester"] = SystemParamsInFile.SEMESTER_OF_YEAR_ALL;
                List<SummedUpRecordHistory> sumCN = SearchBySchoolPrimary(SchoolID, dic).ToList();
                SummedUpRecordHistoryBusiness.DeleteAll(sumCN);
            }
        }
        /// <summary>
        ///  GetSummedUpRecordHistory
        /// </summary>
        /// <param name="AcademicYearID"></param>
        /// <param name="SchoolID"></param>
        /// <param name="Semester"></param>
        /// <param name="ClassID"></param>
        /// <param name="PeriodID"></param>
        /// <returns></returns>
        public IQueryable<SummedUpRecordBO> GetSummedUpRecordHistory(int AcademicYearID, int SchoolID, int Semester, int ClassID, int? PeriodID, bool isShowRetetResutl)
        {
            IQueryable<SummedUpRecordBO> _query;
            int? createdAcaYear = AcademicYearBusiness.Find(AcademicYearID).Year;
            int last2SchoolNumber = SchoolID % 100;
            IDictionary<string, object> dicClass = new Dictionary<string, object>();
            dicClass["SchoolID"] = SchoolID;
            if (PeriodID != null)
            {
                _query = from poc in PupilOfClassBusiness.SearchBySchool(SchoolID, new Dictionary<string, object> { { "AcademicYearID", AcademicYearID }, { "ClassID", ClassID }, { "Check", "Check" } })
                         join pp in PupilProfileBusiness.All on poc.PupilID equals pp.PupilProfileID
                         join soc in ClassSubjectBusiness.SearchByClass(ClassID,dicClass) on poc.ClassID equals soc.ClassID
                         join sur in SummedUpRecordHistoryBusiness.All.Where(u => u.Last2digitNumberSchool == last2SchoolNumber && u.CreatedAcademicYear == createdAcaYear
                             && u.Semester == Semester && u.PeriodID == PeriodID.Value && u.AcademicYearID == AcademicYearID &&
                                                                        u.SchoolID == SchoolID && u.ClassID == ClassID)
                             on new { poc.AcademicYearID, poc.ClassID, poc.PupilID } equals new { sur.AcademicYearID, sur.ClassID, sur.PupilID } into g1
                         from j1 in g1.DefaultIfEmpty()
                         join sub in SubjectCatBusiness.All on j1.SubjectID equals sub.SubjectCatID
                         select new SummedUpRecordBO
                         {
                             SummedUpRecordID = j1.SummedUpRecordID,
                             PupilID = poc.PupilID,
                             AcademicYearID = poc.AcademicYearID,
                             ClassID = poc.ClassID,
                             SchoolID = poc.SchoolID,
                             SubjectID = j1.SubjectID,
                             SubjectName = sub.DisplayName,
                             Semester = Semester,
                             SummedUpMark = j1.SummedUpMark,
                             JudgementResult = j1.JudgementResult,
                             ReTestMark = j1.ReTestMark,
                             ReTestJudgement = j1.ReTestJudgement,
                             OrderInClass = poc.OrderInClass,
                             Status = poc.Status,
                             PupilCode = pp.PupilCode,
                             PupilFullName = pp.FullName,
                             Name = pp.Name,
                             Year = j1.CreatedAcademicYear,
                             PeriodID = j1.PeriodID,
                             IsCommenting = j1.IsCommenting,
                             Comment = j1.Comment,
                             AssignedDate = poc.AssignedDate,
                             SubjectTypeID = soc.AppliedType.Value,
                             IsSpecialize = soc.IsSpecializedSubject
                         };
            }
            else if (Semester == GlobalConstants.SEMESTER_OF_YEAR_FIRST || Semester == GlobalConstants.SEMESTER_OF_YEAR_SECOND)
            {
                _query = from poc in PupilOfClassBusiness.SearchBySchool(SchoolID, new Dictionary<string, object> { { "AcademicYearID", AcademicYearID }, { "ClassID", ClassID }, { "Check", "Check" } })
                         join pp in PupilProfileBusiness.All on poc.PupilID equals pp.PupilProfileID
                         join soc in ClassSubjectBusiness.SearchByClass(ClassID, dicClass) on poc.ClassID equals soc.ClassID
                         join sur in SummedUpRecordHistoryBusiness.All.Where(u => u.Last2digitNumberSchool == last2SchoolNumber && u.CreatedAcademicYear == createdAcaYear
                             && u.SchoolID == SchoolID && u.Semester == Semester && u.PeriodID == null)
                             on new { poc.AcademicYearID, poc.ClassID, poc.PupilID } equals new { sur.AcademicYearID, sur.ClassID, sur.PupilID } into g1
                         from j1 in g1.DefaultIfEmpty()
                         join sub in SubjectCatBusiness.All on j1.SubjectID equals sub.SubjectCatID
                         select new SummedUpRecordBO
                         {
                             SummedUpRecordID = j1.SummedUpRecordID,
                             PupilID = poc.PupilID,
                             AcademicYearID = poc.AcademicYearID,
                             ClassID = poc.ClassID,
                             SchoolID = poc.SchoolID,
                             SubjectID = j1.SubjectID,
                             SubjectName = sub.DisplayName,
                             Semester = Semester,
                             SummedUpMark = j1.SummedUpMark,
                             JudgementResult = j1.JudgementResult,
                             ReTestMark = j1.ReTestMark,
                             ReTestJudgement = j1.ReTestJudgement,
                             OrderInClass = poc.OrderInClass,
                             Status = poc.Status,
                             PupilCode = pp.PupilCode,
                             PupilFullName = pp.FullName,
                             Name = pp.Name,
                             Year = j1.CreatedAcademicYear,
                             IsCommenting = j1.IsCommenting,
                             Comment = j1.Comment,
                             AssignedDate = poc.AssignedDate,
                             SubjectTypeID = soc.AppliedType.Value,
                             IsSpecialize = soc.IsSpecializedSubject
                         };
            }
            else
            {
                _query = from poc in PupilOfClassBusiness.SearchBySchool(SchoolID, new Dictionary<string, object> { { "AcademicYearID", AcademicYearID }, { "ClassID", ClassID }, { "Check", "Check" } })
                         join pp in PupilProfileBusiness.All on poc.PupilID equals pp.PupilProfileID
                         join soc in ClassSubjectBusiness.SearchByClass(ClassID, dicClass) on poc.ClassID equals soc.ClassID
                         join sur in SummedUpRecordHistoryBusiness.All.Where(u => u.Last2digitNumberSchool == last2SchoolNumber && u.CreatedAcademicYear == createdAcaYear
                             && u.SchoolID == SchoolID 
                             //&& (u.Semester == SystemParamsInFile.SEMESTER_OF_YEAR_ALL || u.Semester == SystemParamsInFile.SEMESTER_OF_YEAR_RETEST) && !u.PeriodID.HasValue
                             )
                             on new { poc.AcademicYearID, poc.ClassID, poc.PupilID } equals new { sur.AcademicYearID, sur.ClassID, sur.PupilID } into g1
                         from j1 in g1.DefaultIfEmpty()
                         join sub in SubjectCatBusiness.All on j1.SubjectID equals sub.SubjectCatID
                         select new SummedUpRecordBO
                         {
                             SummedUpRecordID = j1.SummedUpRecordID,
                             PupilID = poc.PupilID,
                             AcademicYearID = poc.AcademicYearID,
                             ClassID = poc.ClassID,
                             SchoolID = poc.SchoolID,
                             SubjectID = j1.SubjectID,
                             SubjectName = sub.DisplayName,
                             Semester = j1.Semester,
                             SummedUpMark = j1.SummedUpMark,
                             JudgementResult = j1.JudgementResult,
                             ReTestMark = j1.ReTestMark,
                             ReTestJudgement = j1.ReTestJudgement,
                             OrderInClass = poc.OrderInClass,
                             Status = poc.Status,
                             PupilCode = pp.PupilCode,
                             PupilFullName = pp.FullName,
                             Name = pp.Name,
                             Year = j1.CreatedAcademicYear,
                             IsCommenting = j1.IsCommenting,
                             Comment = j1.Comment,
                             AssignedDate = poc.AssignedDate,
                             SubjectTypeID = soc.AppliedType.Value,
                             IsSpecialize = soc.IsSpecializedSubject
                         };
                if (isShowRetetResutl)
                {
                    _query = _query.Where(u => u.Semester == SystemParamsInFile.SEMESTER_OF_YEAR_ALL || u.Semester == SystemParamsInFile.SEMESTER_OF_YEAR_RETEST);
                }
                else
                {
                    _query = _query.Where(u => u.Semester == SystemParamsInFile.SEMESTER_OF_YEAR_ALL);
                }
            }
            return _query;
        }

        /*
        /// <summary>
        /// Thêm mới hoặc cập nhật SummedUpRecord, ModifiedDate của PupilRetestRepository.
        /// </summary>
        /// <author>Hieund</author>
        /// <modifier>AnhVD</modifier>
        /// <param name="lstPupilRetest"></param>
        public void InsertOrUpdateSummedUpRecordAfterRetest(int SchoolID, int AcademicYearID, IEnumerable<PupilRetestRegistrationSubjectBO> lstPupilRetest)
        {
            //Lấy những phần tử có lstPupilRetest[i].prr.ModifiedDate = null. Tạo List<SummedUpRecord> bởi lstPupilRetest[i].sur => lstSummedUpRecord
            //và gọi hàm SummedUpRecordBusiness.InsertSummedUpRecord(lstSummedUpRecord)

            //Lấy những phần tử có lstPupilRetest[i].prr.ModifiedDate != null. Tạo List<SummedUpRecord> bởi lstPupilRetest[i].sur => lstSummedUpRecord
            //SummedUpRecordBusiness.UpdateSummedUpRecord(lstSummedUpRecord)
            //Lấy từng phần tử trong lstPupilRetest 
            //Thực hiện update ngày cập nhật PupilRetestRegistrationBusiness.UpdatePupilRetestRegistration(lstPupilRetest.prr)
            if (lstPupilRetest.Count() == 0) return;

            List<long> SummedUpRecordHistoryRetestIDs = lstPupilRetest.Select(p => p.SummedUpRecordID).ToList();
            IQueryable<SummedUpRecordHistory> SummedUpRecordHistoryRetests = this.SummedUpRecordHistoryRepository.All.Where(p => SummedUpRecordHistoryRetestIDs.Contains(p.SummedUpRecordID));

            List<SummedUpRecordHistory> lstInsert= new List<SummedUpRecordHistory>(lstPupilRetest.Count());
            List<SummedUpRecordHistory> lstUpdate = new List<SummedUpRecordHistory>(lstPupilRetest.Count());
            foreach (var item in lstPupilRetest)
            {
                var temp = SummedUpRecordHistoryRetests.FirstOrDefault(p => p.SummedUpRecordID == item.SummedUpRecordID);
                if (temp == null)
                {
                    lstInsert.Add(
                        new SummedUpRecordHistory()
                        {
                            SchoolID = item.SchoolID,
                            AcademicYearID = item.AcademicYearID,
                            ClassID = item.ClassID,
                            PupilID = item.PupilID,
                            Semester = item.Semester,
                            PeriodID = item.PeriodID,
                            SubjectID = item.SubjectID,
                            CreatedAcademicYear = item.Year,
                            Comment = item.Comment,
                            IsCommenting = item.IsCommenting,
                            JudgementResult = item.JudgementResult,
                            ReTestJudgement = item.ReTestJudgement,
                            ReTestMark = item.ReTestMark,
                            SummedUpMark = item.SummedUpMark,
                            SummedUpDate = DateTime.Now
                        }
                        );
                }
                else
                {
                    temp.Semester = item.Semester;
                    temp.ReTestJudgement = item.ReTestJudgement;
                    temp.ReTestMark = item.ReTestMark;
                    temp.SummedUpDate = DateTime.Now;
                    lstUpdate.Add(temp);
                }
            }
            this.InsertSummedUpRecordRetestHistory(lstInsert);
            this.UpdateSummedUpRecordRetestHistory(lstUpdate);
        }
        */
          
        
        /// <summary>
        /// Thêm mới hoặc cập nhật SummedUpRecord, ModifiedDate của PupilRetestRepository.
        /// </summary>
        /// <author>Hieund</author>
        /// <modifier>AnhVD</modifier>
        /// <param name="lstPupilRetest"></param>
        public void InsertOrUpdateSummedUpRecordAfterRetest(int SchoolID, int AcademicYearID, IEnumerable<PupilRetestRegistrationSubjectBO> lstPupilRetest)
        {
            //Lấy những phần tử có lstPupilRetest[i].prr.ModifiedDate = null. Tạo List<SummedUpRecord> bởi lstPupilRetest[i].sur => lstSummedUpRecord
            //và gọi hàm SummedUpRecordBusiness.InsertSummedUpRecord(lstSummedUpRecord)

            //Lấy những phần tử có lstPupilRetest[i].prr.ModifiedDate != null. Tạo List<SummedUpRecord> bởi lstPupilRetest[i].sur => lstSummedUpRecord
            //SummedUpRecordBusiness.UpdateSummedUpRecord(lstSummedUpRecord)
            //Lấy từng phần tử trong lstPupilRetest 
            //Thực hiện update ngày cập nhật PupilRetestRegistrationBusiness.UpdatePupilRetestRegistration(lstPupilRetest.prr)

            var lstModifiedNull = lstPupilRetest
                                        //.Where(u => !u.ModifiedDatePRR.HasValue)
                                        .Select(u => new SummedUpRecordHistory
                                        {
                                            AcademicYearID = u.AcademicYearID,
                                            ClassID = u.ClassID,
                                            Comment = u.Comment,
                                            IsCommenting = u.IsCommenting,
                                            JudgementResult = u.JudgementResult,
                                            PeriodID = u.PeriodID,
                                            PupilID = u.PupilID,
                                            ReTestJudgement = u.ReTestJudgement,
                                            ReTestMark = u.ReTestMark,
                                            SchoolID = u.SchoolID,
                                            Semester = u.Semester, // Sem=5
                                            SubjectID = u.SubjectID,
                                            SummedUpDate = DateTime.Now,
                                            SummedUpMark = u.SummedUpMark,
                                            CreatedAcademicYear = u.Year,
                                            SummedUpRecordID = u.SummedUpRecordID
                                        }).ToList();

            this.InsertSummedUpRecordRetestHistory(lstModifiedNull);

            //var lstModifedNotNull = new List<SummedUpRecordHistory>();
            //var lstPupilRetestUpdate = lstPupilRetest.Where(u => u.ModifiedDatePRR.HasValue).ToList();
            //foreach (var sur in lstPupilRetestUpdate)
            //{
            //    var item = this.Find(sur.SummedUpRecordID);
            //    item.Semester = sur.Semester;
            //    item.ReTestJudgement = sur.ReTestJudgement;
            //    item.ReTestMark = sur.ReTestMark;
            //    item.SummedUpDate = DateTime.Now;
            //    lstModifedNotNull.Add(item);
            //}
            //this.UpdateSummedUpRecordRetestHistory(lstModifedNotNull);

            var PupilRetestRegistrationIDs = lstPupilRetest.Select(v => v.PupilRetestRegistrationID).ToArray();
            var PupilRetestRegistrations = this.PupilRetestRegistrationBusiness.All.Where(u => PupilRetestRegistrationIDs.Contains(u.PupilRetestRegistrationID)).ToList();
            PupilRetestRegistrations.ForEach(u => u.ModifiedDate = DateTime.Now);
            //this.PupilRetestRegistrationBusiness.UpdatePupilRetestRegistration(SchoolID, AcademicYearID, lstPrr);
        }

        public void InsertSummedUpRecordRetestHistory(List<SummedUpRecordHistory> lstSummedUpRecordHistory)
        {
            if (lstSummedUpRecordHistory.Count == 0)
            {
                return;
            }
            #region Kiem tra du lieu dau vao
            SummedUpRecordHistory first = lstSummedUpRecordHistory.First();
            int schoolID = first.SchoolID;
            int academicYearID = first.AcademicYearID;
            AcademicYear aca = AcademicYearBusiness.Find(academicYearID);

            // Kiem tra nam hoc co thuoc truong
            //  SubjectID và ClassID not compatible trong ClassSubject
            var listClassSubjectID = lstSummedUpRecordHistory.Select(o => new { o.ClassID, o.SubjectID }).Distinct().ToList();
            foreach (var cs in listClassSubjectID)
            {
                IDictionary<string, object> SearchInfoClassSubject = new Dictionary<string, object>();
                SearchInfoClassSubject["SubjectID"] = cs.SubjectID;
                SearchInfoClassSubject["ClassID"] = cs.ClassID;

                bool ClassSubjectExist = new ClassSubjectRepository(this.context).ExistsRow(GlobalConstants.SCHOOL_SCHEMA, "ClassSubject", SearchInfoClassSubject, null);

                if (!ClassSubjectExist)
                {
                    throw new BusinessException("Common_Validate_NotCompatible");
                }
            }


            // Kiem tra nam hoc co thuoc truong
            if (aca.SchoolID != schoolID)
            {
                throw new BusinessException("Common_Validate_NotCompatible");
            }

            // kiểm tra giá trị của ProfileStatus trong bảng PupilOfClass có bàng giá trị của PUPIL_STATUS_STUDYING không
            List<int> listPupilID = lstSummedUpRecordHistory.Select(o => o.PupilID).Distinct().ToList();
            int countPupilStudying = PupilOfClassBusiness.AllNoTracking.Where(o => o.SchoolID == schoolID
                && o.AcademicYearID == academicYearID && o.Status == SystemParamsInFile.PUPIL_STATUS_STUDYING
                && listPupilID.Contains(o.PupilID)).Count();

            if (listPupilID.Count != countPupilStudying)
            {
                throw new BusinessException("Common_Validate_NotIsCurrentStatus");
            }

            //Lấy từng phần tử trong lstSummedUpRecord
            foreach (SummedUpRecordHistory summedUpRecordHistory in lstSummedUpRecordHistory)
            {
                ValidateRetest(summedUpRecordHistory);
            }
            #endregion

            this.context.Configuration.AutoDetectChangesEnabled = false;
            this.context.Configuration.ValidateOnSaveEnabled = false;
            foreach (SummedUpRecordHistory SummedUpRecordHistory in lstSummedUpRecordHistory)
            {
                SummedUpRecordHistory.SummedUpDate = DateTime.Now;
                SummedUpRecordHistory.CreatedAcademicYear = aca.Year;
                this.Insert(SummedUpRecordHistory);
            }
            this.context.Configuration.AutoDetectChangesEnabled = true;
            this.context.Configuration.ValidateOnSaveEnabled = true;
        }
        /// <summary>
        /// 03/04/2014
        /// QuangLM
        /// Cap nhat du lieu thi lai
        /// </summary>
        /// <param name="lstSummedUpRecord"></param>
        public void UpdateSummedUpRecordRetestHistory(List<SummedUpRecordHistory> lstSummedUpRecordHistory)
        {
            if (lstSummedUpRecordHistory.Count == 0)
            {
                return;
            }
            #region Kiem tra du lieu dau vao
            SummedUpRecordHistory first = lstSummedUpRecordHistory.First();
            int schoolID = first.SchoolID;
            int academicYearID = first.AcademicYearID;
            AcademicYear aca = AcademicYearBusiness.Find(academicYearID);

            // Kiem tra nam hoc co thuoc truong
            //  SubjectID và ClassID not compatible trong ClassSubject
            var listClassSubjectID = lstSummedUpRecordHistory.Select(o => new { o.ClassID, o.SubjectID }).Distinct().ToList();
            foreach (var cs in listClassSubjectID)
            {
                IDictionary<string, object> SearchInfoClassSubject = new Dictionary<string, object>();
                SearchInfoClassSubject["SubjectID"] = cs.SubjectID;
                SearchInfoClassSubject["ClassID"] = cs.ClassID;

                bool ClassSubjectExist = new ClassSubjectRepository(this.context).ExistsRow(GlobalConstants.SCHOOL_SCHEMA, "ClassSubject", SearchInfoClassSubject, null);

                if (!ClassSubjectExist)
                {
                    throw new BusinessException("Common_Validate_NotCompatible");
                }
            }


            // Kiem tra nam hoc co thuoc truong
            if (aca.SchoolID != schoolID)
            {
                throw new BusinessException("Common_Validate_NotCompatible");
            }

            // kiểm tra giá trị của ProfileStatus trong bảng PupilOfClass có bàng giá trị của PUPIL_STATUS_STUDYING không
            List<int> listPupilID = lstSummedUpRecordHistory.Select(o => o.PupilID).Distinct().ToList();
            int countPupilStudying = PupilOfClassBusiness.AllNoTracking.Where(o => o.SchoolID == schoolID
                && o.AcademicYearID == academicYearID && o.Status == SystemParamsInFile.PUPIL_STATUS_STUDYING
                && listPupilID.Contains(o.PupilID)).Count();

            if (listPupilID.Count != countPupilStudying)
            {
                throw new BusinessException("Common_Validate_NotIsCurrentStatus");
            }

            //Lấy từng phần tử trong lstSummedUpRecord
            foreach (SummedUpRecordHistory SummedUpRecordHistory in lstSummedUpRecordHistory)
            {
                ValidateRetest(SummedUpRecordHistory);
            }
            #endregion

            this.context.Configuration.AutoDetectChangesEnabled = false;
            this.context.Configuration.ValidateOnSaveEnabled = false;
            foreach (var item in lstSummedUpRecordHistory)
            {
                item.CreatedAcademicYear = aca.Year;
                item.SummedUpDate = DateTime.Now;
                base.Update(item);
            }
            this.context.Configuration.AutoDetectChangesEnabled = true;
            this.context.Configuration.ValidateOnSaveEnabled = true;
        }

        #region Valadate Object
        /// <summary>
        /// Rang buoc object
        /// </summary>
        /// <author>AnhVD</author>
        /// <param name="summeduprecord"></param>
        public void ValidateRetest(SummedUpRecordHistory summedUpRecordHistory)
        {
            ValidationMetadata.ValidateObject(summedUpRecordHistory);
        }
        #endregion
    }
}
