﻿/**
 * The Viettel License
 *
 * Copyright 2012 Viettel Telecom. All rights reserved.
 * VIETTEL PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */

/** 
* @author  
* @version $Revision: $
*/

using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Linq.Expressions;
using System.Web;
using log4net;

using SMAS.DAL.Repository;
using SMAS.DAL.IRepository;
using SMAS.Business.IBusiness;
using SMAS.Models.Models;
using SMAS.Business.Common;

namespace SMAS.Business.Business
{ 
    public partial class HabitOfChildrenBusiness
    {

        #region Search


        /// <summary>
        /// Tìm kiếm sở thích chi tiết của trẻ
        /// </summary>
        /// <author>trangdd</author>
        /// <date>06/11/2012</date>    
        /// <returns>IQueryable<Ethnic></returns>
        public IQueryable<HabitOfChildren> Search(IDictionary<string, object> dic)
        {
            int PupilID = Utils.GetInt(dic, "PupilID");
            int HabitDetailID = Utils.GetInt(dic, "HabitDetailID");
            IQueryable<HabitOfChildren> lsHabitOfChildren = HabitOfChildrenRepository.All;

            if (PupilID != 0)
            {
                lsHabitOfChildren = lsHabitOfChildren.Where(cfh => cfh.PupilID == PupilID);
            }
            if (HabitDetailID != 0)
            {
                lsHabitOfChildren = lsHabitOfChildren.Where(cfh => cfh.HabitDetailID == HabitDetailID);
            }
            return lsHabitOfChildren;

        }
        #endregion  
    }
}