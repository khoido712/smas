﻿/**
 * The Viettel License
 *
 * Copyright 2012 Viettel Telecom. All rights reserved.
 * VIETTEL PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */

/** 
* @author namdv3
* @version $Revision: $
*/

using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Linq.Expressions;
using System.Web;
using log4net;

using SMAS.DAL.Repository;
using SMAS.DAL.IRepository;
using SMAS.Business.IBusiness;
using SMAS.Models.Models;
using System.Transactions;
using SMAS.Business.Common;

namespace SMAS.Business.Business
{
    public partial class NutritionalNormBusiness
    {
        private const int MAXVALUEPER = 100;
        private const int MINVALUEPER = 0;
        private void Validation(NutritionalNorm entity, List<NutritionalNormMineral> lstNutritionalNormMineral, string action)
        {
            ValidationMetadata.ValidateObject(entity);
            var flag = false;
            if (action == "Insert")
            {
                flag = NutritionalNormRepository.ExistsRow(GlobalConstants.LIST_SCHEMA, "NutritionalNorm", new Dictionary<string, object> { { "EffectDate", entity.EffectDate }, { "EatingGroupID", entity.EatingGroupID }, { "IsActive", true } }, null);
                if (flag)
                {
                    throw new BusinessException("NutritionalNorm_Validate_EatingGroupID");
                }
            }
            //this.CheckDuplicateCouple(entity.EffectDate.ToString(), entity.EatingGroupID.ToString(), GlobalConstants.LIST_SCHEMA, "NutritionalNorm", "EffectDate", "EatingGroupID", true, 0, "NutritionalNorm_Label_EatingGroupID");
            if (action == "Update")
            {
                //this.CheckDuplicateCouple(entity.EffectDate.ToString(), entity.EatingGroupID.ToString(), GlobalConstants.LIST_SCHEMA, "NutritionalNorm", "EffectDate", "EatingGroupID", true, entity.NutritionalNormID, "NutritionalNorm_Label_EatingGroupID");
                flag = NutritionalNormRepository.ExistsRow(GlobalConstants.LIST_SCHEMA, "NutritionalNorm", new Dictionary<string, object> { { "EffectDate", entity.EffectDate }, { "EatingGroupID", entity.EatingGroupID } }, new Dictionary<string, object> { { "NutritionalNormID", entity.NutritionalNormID } });
                if (flag)
                {
                    throw new BusinessException("NutritionalNorm_Validate_EatingGroupID");
                }
                bool NutritionalNormCompatible = new NutritionalNormRepository(this.context).ExistsRow(GlobalConstants.LIST_SCHEMA, "NutritionalNorm",
                 new Dictionary<string, object>()
                {
                    {"SchoolID",entity.SchoolID},
                    {"NutritionalNormID",entity.NutritionalNormID}
                }, null);
                if (!NutritionalNormCompatible)
                {
                    throw new BusinessException("Common_Validate_NotCompatible");
                }
            }
            if (lstNutritionalNormMineral != null && lstNutritionalNormMineral.Count > 0)
            {
                foreach (var item in lstNutritionalNormMineral)
                {
                    //new NutritionalNormMineralBusiness(null).CheckAvailable(item.MinenalID, "NutritionalNorm_Label_NutritionalNormMineral");
                    if(item.ValueFrom > item.ValueTo)
                        throw new BusinessException("NutritionalNorm_Validate_MinMaxValue");
                }
            }
            //DailyDemandFrom and DailyDemandTo là số nguyên không cần chia hết cho 2
            //if (entity.DailyDemandFrom % 2 != 0)
            //    throw new BusinessException("NutritionalNorm_Validate_DailyDemandFrom");
            //if (entity.DailyDemandTo % 2 != 0)
            //    throw new BusinessException("NutritionalNorm_Validate_DailyDemandTo");
            if (entity.DailyDemandFrom >= entity.DailyDemandTo)
                throw new BusinessException("NutritionalNorm_Validate_DailyDemand");

            if (CheckMinMaxValue(entity.RateFrom, MINVALUEPER, MAXVALUEPER))
                throw new BusinessException("NutritionalNorm_Validate_RateFrom");
            if (CheckMinMaxValue(entity.RateTo, MINVALUEPER, MAXVALUEPER))
                throw new BusinessException("NutritionalNorm_Validate_RateTo");
            if (entity.RateFrom >= entity.RateTo)
                throw new BusinessException("NutritionalNorm_Validate_Rate");

            if (CheckMinMaxValue(entity.ProteinFrom, MINVALUEPER, MAXVALUEPER))
                throw new BusinessException("NutritionalNorm_Validate_ProteinFrom");
            if (CheckMinMaxValue(entity.ProteinTo, MINVALUEPER, MAXVALUEPER))
                throw new BusinessException("NutritionalNorm_Validate_ProteinTo");
            if (entity.ProteinFrom >= entity.ProteinTo)
                throw new BusinessException("NutritionalNorm_Validate_Protein");

            if (CheckMinMaxValue(entity.SugarFrom, MINVALUEPER, MAXVALUEPER))
                throw new BusinessException("NutritionalNorm_Validate_SugarFrom");
            if (CheckMinMaxValue(entity.SugarTo, MINVALUEPER, MAXVALUEPER))
                throw new BusinessException("NutritionalNorm_Validate_SugarTo");
            if (entity.SugarFrom >= entity.SugarTo)
                throw new BusinessException("NutritionalNorm_Validate_Sugar");

            if (CheckMinMaxValue(entity.FatFrom, MINVALUEPER, MAXVALUEPER))
                throw new BusinessException("NutritionalNorm_Validate_FatFrom");
            if (CheckMinMaxValue(entity.FatTo, MINVALUEPER, MAXVALUEPER))
                throw new BusinessException("NutritionalNorm_Validate_FatTo");
            if (entity.FatFrom >= entity.FatTo)
                throw new BusinessException("NutritionalNorm_Validate_Fat");

        }
        private bool CheckMinMaxValue(int value, int min, int max)
        {
            if (value >= min && value <= max)
                return false;
            else return true;
        }
        public void Insert(NutritionalNorm entity, List<NutritionalNormMineral> lstNutritionalNormMineral)
        {
            Validation(entity, lstNutritionalNormMineral, "Insert");
            entity.NutritionalNormMinerals = lstNutritionalNormMineral;
            this.Insert(entity);
        }
        public void Update(NutritionalNorm entity, List<NutritionalNormMineral> lstNutritionalNormMineral)
        {
            Validation(entity, lstNutritionalNormMineral, "Update");
            var lstnutritionalnormmineral = NutritionalNormMineralRepository.All.Where(o => o.NutritionalNormID == entity.NutritionalNormID);
            if (lstnutritionalnormmineral.Count() > 0)
            {
                foreach (var item in lstnutritionalnormmineral)
                {
                    NutritionalNormMineralRepository.Delete(item);
                }
            }
            if (lstNutritionalNormMineral != null && lstNutritionalNormMineral.Count > 0)
            {
                foreach (var item in lstNutritionalNormMineral)
                {
                    item.NutritionalNormID = entity.NutritionalNormID;
                    NutritionalNormMineralRepository.Insert(item);
                }
            }
            this.Update(entity);

        }

        private void ValidationDel(int NutritionalNormID, int SchoolID)
        {
            if (!NutritionalNormRepository.ExistsRow(GlobalConstants.LIST_SCHEMA, "NutritionalNorm", new Dictionary<string, object> { { "NutritionalNormID", NutritionalNormID } }))
                throw new BusinessException("NutritionalNorm_Validate_NotPK");

            this.CheckConstraints(GlobalConstants.LIST_SCHEMA, "DailyDishCost", NutritionalNormID, "NutritionalNorm_Validate_NutritionalNormID");

            bool NutritionalNormCompatible = new NutritionalNormRepository(this.context).ExistsRow(GlobalConstants.LIST_SCHEMA, "NutritionalNorm",
                 new Dictionary<string, object>()
                {
                    {"SchoolID",SchoolID},
                    {"NutritionalNormID",NutritionalNormID}
                }, null);
            if (!NutritionalNormCompatible)
            {
                throw new BusinessException("Common_Validate_NotCompatible");
            }
        }

        public void Delete(int NutritionalNormID, int SchoolID)
        {
            ValidationDel(NutritionalNormID, SchoolID);

            var NutritionalNorm = this.All.Where(o => o.NutritionalNormID == NutritionalNormID).FirstOrDefault();
            if (NutritionalNorm != null)
            {
                NutritionalNorm.IsActive = false;
                NutritionalNorm.ModifiedDate = DateTime.Now;
                this.Update(NutritionalNorm);
            }

        }

        private IQueryable<NutritionalNorm> Search(IDictionary<string, object> SearchInfo)
        {
            var query = from nn in this.All
                        join nnm in NutritionalNormMineralRepository.All
                        on nn.NutritionalNormID equals nnm.NutritionalNormID into g1
                        from j2 in g1.DefaultIfEmpty()
                        select nn;
            if (query.Count() > 0)
            {
                DateTime? EffectDate = Utils.GetDateTime(SearchInfo, "EffectDate");
                int SchoolID = Utils.GetInt(SearchInfo, "SchoolID");
                int EatingGroupID = Utils.GetInt(SearchInfo, "EatingGroupID");
                int DailyDemandFrom = Utils.GetInt(SearchInfo, "DailyDemandFrom");
                int DailyDemandTo = Utils.GetInt(SearchInfo, "DailyDemandTo");
                int RateFrom = Utils.GetInt(SearchInfo, "RateFrom");
                int RateTo = Utils.GetInt(SearchInfo, "RateTo");
                bool? IsActive = Utils.GetIsActive(SearchInfo, "IsActive");
                if (EffectDate.HasValue)
                    query = query.Where(o => o.EffectDate.Year == EffectDate.Value.Year && o.EffectDate.Month == EffectDate.Value.Month && o.EffectDate.Day == EffectDate.Value.Day);
                if (SchoolID > 0)
                    query = query.Where(o => o.SchoolID == SchoolID);
                if (EatingGroupID > 0)
                    query = query.Where(o => o.EatingGroupID == EatingGroupID);
                if (DailyDemandFrom > 0)
                    query = query.Where(o => o.DailyDemandFrom == DailyDemandFrom);
                if (DailyDemandTo > 0)
                    query = query.Where(o => o.DailyDemandTo == DailyDemandTo);
                if (RateFrom > 0)
                    query = query.Where(o => o.RateFrom == RateFrom);
                if (RateTo > 0)
                    query = query.Where(o => o.RateTo == RateTo);
                if (IsActive.HasValue)
                    query = query.Where(o => o.IsActive == IsActive.Value);
                return query;
            }
            else return null;
        }
        public IQueryable<NutritionalNorm> SearchBySchool(int SchoolID, IDictionary<string, object> SearchInfo)
        {
            if (SchoolID == 0)
                return null;
            SearchInfo["SchoolID"] = SchoolID;
            return Search(SearchInfo);
        }
    }
}