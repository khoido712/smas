/**
 * The Viettel License
 *
 * Copyright 2012 Viettel Telecom. All rights reserved.
 * VIETTEL PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */

/**
* @author
* @version $Revision: $
*/

using SMAS.Business.BusinessObject;
using SMAS.Business.Common;
using SMAS.DAL.Repository;
using SMAS.Models.Models;
using System;
using System.Collections.Generic;
using System.Linq;

namespace SMAS.Business.Business
{
    public partial class SubjectCatBusiness
    {
        /// <summary>
        /// Tìm kiếm môn học
        /// </summary>
        /// <author>hath</author>
        /// <date>4/9/2012</date
        /// <param name="SubjectName">Tên môn học</param>
        /// <param name="AppliedLevel">Cấp học</param>
        /// <param name="IsForeignLanguage">Xác định môn học là môn ngoại ngữ hay không ?</param>
        /// <param name="IsApprenticeshipSubject">Xác định môn học là môn nghề hay không (áp dụng cho cấp 2,3)</param>
        /// <param name="IsExemptible">Xác định môn học có thể miễn giảm được hay không ?</param>
        /// <param name="IsCoreSubject">Xác định môn học có phải là môn chính yếu </param>
        /// <param name="MiddleSemesterTest">Xác định môn học yêu cầu kiểm tra giữa học kì hay không ?</param>
        /// <param name="HasPractice">Xác định môn học là môn có thực hành hay không ?</param>
        /// <param name="Description">Mô tả</param>
        /// <param name="IsActive">Biến kích hoạt</param>
        /// <returns>IQueryable<SubjectCat></returns>
        public IQueryable<SubjectCat> Search(IDictionary<string, object> dic)
        {
            string SubjectName = Utils.GetString(dic, "SubjectName");
            int AppliedLevel = (int)Utils.GetInt(dic, "AppliedLevel");
            bool? IsForeignLanguage = Utils.GetNullableBool(dic, "IsForeignLanguage");
            bool? IsApprenticeshipSubject = Utils.GetNullableBool(dic, "IsApprenticeshipSubject");
            bool? IsExemptible = Utils.GetNullableBool(dic, "IsExemptible");
            bool? IsCoreSubject = Utils.GetNullableBool(dic, "IsCoreSubject");
            bool? MiddleSemesterTest = Utils.GetNullableBool(dic, "MiddleSemesterTest");
            bool? HasPractice = Utils.GetNullableBool(dic, "HasPractice");
            string Description = Utils.GetString(dic, "Description");
            bool? IsActive = Utils.GetNullableBool(dic, "IsActive");
            string IsCommenting = Utils.GetString(dic, "IsCommenting");
            string Abbreviation = Utils.GetString(dic, "Abbreviation");

            IQueryable<SubjectCat> lsSubjectCat = SubjectCatRepository.All;

            if (IsActive.HasValue)
            {
                lsSubjectCat = lsSubjectCat.Where(sub => sub.IsActive == IsActive);
            }
            else
            {
                lsSubjectCat = lsSubjectCat.Where(sub => sub.IsActive == true);
            }
            if (IsForeignLanguage.HasValue)
            {
                lsSubjectCat = lsSubjectCat.Where(sub => sub.IsForeignLanguage == IsForeignLanguage);
            }
            if (IsApprenticeshipSubject.HasValue)
            {
                lsSubjectCat = lsSubjectCat.Where(sub => sub.IsApprenticeshipSubject == IsApprenticeshipSubject);
            }
            if (IsExemptible.HasValue)
            {
                lsSubjectCat = lsSubjectCat.Where(sub => sub.IsExemptible == IsExemptible);
            }
            if (IsCoreSubject.HasValue)
            {
                lsSubjectCat = lsSubjectCat.Where(sub => sub.IsCoreSubject == IsCoreSubject);
            }
            if (MiddleSemesterTest.HasValue)
            {
                lsSubjectCat = lsSubjectCat.Where(sub => sub.MiddleSemesterTest == MiddleSemesterTest);
            }
            if (HasPractice.HasValue)
            {
                lsSubjectCat = lsSubjectCat.Where(sub => sub.HasPractice == HasPractice);
            }
            if (SubjectName != "")
            {
                lsSubjectCat = lsSubjectCat.Where(sub => sub.SubjectName.ToLower().Contains(SubjectName.Trim().ToLower()));
            }
            if (Description != "")
            {
                lsSubjectCat = lsSubjectCat.Where(sub => sub.Description.Contains(Description));
            }
            if (AppliedLevel != 0)
            {
                lsSubjectCat = lsSubjectCat.Where(sub => sub.AppliedLevel == AppliedLevel);
            }
            if (Abbreviation != "")
            {
                lsSubjectCat = lsSubjectCat.Where(sub => sub.Abbreviation.ToLower().Contains(Abbreviation.Trim().ToLower()));
            }

            if (IsCommenting != "")
            {
                string[] arrStrIsCommenting = IsCommenting.Split(',');
                int[] arrIsCommenting = new int[arrStrIsCommenting.Length];
                for (int i = 0; i < arrIsCommenting.Length; i++)
                {
                    arrIsCommenting[i] = Int32.Parse(arrStrIsCommenting[i]);
                }
                lsSubjectCat = lsSubjectCat.Where(o => arrIsCommenting.Contains(o.IsCommenting));
            }

            return lsSubjectCat;
        }

        /// <summary>
        /// Xóa môn học
        /// </summary>
        /// <author>hath</author>
        /// <date>4/9/2012</date
        /// <param name="SubjectCatID">ID môn học</param>
        public void Delete(int SubjectCatID)
        {
            //Bạn chưa chọn Môn học cần xóa hoặc Môn học bạn chọn đã bị xóa khỏi hệ thống
            new SubjectCatBusiness(null).CheckAvailable((int)SubjectCatID, "SubjectCat_Label_SubjectCatID", true);
            SubjectCat sc = this.Find(SubjectCatID);

            //if (sc.IsApprenticeshipSubject || (sc.ApprenticeshipSubjects != null && sc.ApprenticeshipSubjects.Count() > 0))
            //{
            repository.CheckConstraintsWithIsActive(GlobalConstants.LIST_SCHEMA, "SubjectCat", SubjectCatID);
            // var lsApprent = ApprenticeshipSubjectBusiness.All.Where(o => o.SubjectID == SubjectCatID).Where(o => o.IsActive);

            //    foreach (var item in lsApprent)
            //    {
            //        item.IsActive = false;
            //        ApprenticeshipSubjectBusiness.Update(item);
            //    }
            //}
            //else
            //{
            //    Không thể xóa Môn học đang sử dụng
            //    new SubjectCatBusiness(null).CheckConstraints(GlobalConstants.LIST_SCHEMA, "SubjectCat", SubjectCatID, "SubjectCat_Label_SubjectCatID");
            //}
            base.Delete(SubjectCatID, true);
        }

        private void Validate(SubjectCat subjectCat)
        {
            Utils.ValidateRequire(subjectCat.AppliedLevel, "SubjectCat_Label_AppliedLevel");

            //Tên Môn học không được để trống
            Utils.ValidateRequire(subjectCat.SubjectName, "SubjectCat_Label_SubjectName");

            //Độ dài Tên Môn học không được vượt quá 50
            Utils.ValidateMaxLength(subjectCat.SubjectName, 50, "SubjectCat_Label_SubjectName");

            //Cấp học chỉ nhận giá trị từ 1 - 5 - Kiểm tra AppliedLevel

            //Tên Môn học không được để trống
            Utils.ValidateRequire(subjectCat.AppliedLevel, "SubjectCat_Label_AppliedLevel");

            Utils.ValidateRange((int)subjectCat.AppliedLevel, 1, 5, "SubjectCat_Label_AppliedLevel");

            //Tên Môn học đã tồn tại - Kiểm tra SubjectName / AppliedLevel với IsActive=TRUE
            //Tên Môn học đã tồn tại - Kiểm tra SubjectName / AppliedLevel với IsActive=TRUE            
            IDictionary<string, object> expDic = null;
            if (subjectCat.SubjectCatID > 0)
            {
                expDic = new Dictionary<string, object>();
                expDic["SubjectCatID"] = subjectCat.SubjectCatID;
            }
            bool OtherSubjectNameCompatible = this.repository.ExistsRow(GlobalConstants.LIST_SCHEMA, "SubjectCat",
                new Dictionary<string, object>()
                {
                    {"SubjectName",subjectCat.SubjectName},
                    {"AppliedLevel",subjectCat.AppliedLevel},
                    {"IsActive",true}
                }, expDic);
            if (OtherSubjectNameCompatible)
            {
                List<object> listParam = new List<object>();
                listParam.Add("SubjectCat_Label_SubjectName");
                throw new BusinessException("Common_Validate_Duplicate", listParam);
            }

            //Độ dài trường Tên viết tắt không được vượt quá 10 - Kiểm tra Abbreviation
            Utils.ValidateMaxLength(subjectCat.Abbreviation, 10, "SubjectCat_Label_Abbreviation");

            //Abbreviation: require\
            Utils.ValidateRequire(subjectCat.Abbreviation, "SubjectCat_Label_Abbreviation");

            //Tên hiển thị không được để trống - Kiểm tra DisplayName
            Utils.ValidateRequire(subjectCat.DisplayName, "SubjectCat_Label_DisplayName");

            //Độ dài trường Tên hiển thị không được vượt quá 30 - Kiểm tra DisplayName
            Utils.ValidateMaxLength(subjectCat.DisplayName, 30, "SubjectCat_Label_DisplayName");
        }

        /// <summary>
        /// Thêm môn học
        /// </summary>
        /// <author>hath</author>
        /// <date>4/9/2012</date>
        /// <param name="subjectCat">Đối tượng môn học</param>
        /// <returns>Đối tượng môn học</returns>
        public void Insert(List<SubjectCat> lssubjectCat)
        {
            //SubjectCat subjectCat = lssubjectCat[0];
            //Validate(subjectCat);
            //if (subjectCat.IsApprenticeshipSubject)
            //{
            //    ApprenticeshipSubject ApprenticeshipSubject = new ApprenticeshipSubject();
            //    ApprenticeshipSubject.SubjectID = subjectCat.SubjectCatID;
            //    ApprenticeshipSubject.SubjectName = subjectCat.SubjectName;
            //    ApprenticeshipSubject.IsActive = true;
            //    ApprenticeshipSubjectBusiness.Insert(ApprenticeshipSubject);
            //}
            //base.Insert(subjectCat);
            for (int i = 0; i < lssubjectCat.Count; i++)
            {
                Validate(lssubjectCat[i]);
                base.Insert(lssubjectCat[i]);
                base.Save();
            }

            //return base.Insert(subjectCat);
        }

        /// <summary>
        /// Sửa môn học
        /// </summary>
        /// <author>hath</author>
        /// <date>4/9/2012</date>
        /// <param name="subjectCat">Đối tượng môn học</param>
        /// <returns>Đối tượng môn học</returns>
        public override SubjectCat Update(SubjectCat subjectCat)
        {
            //Bạn chưa chọn Môn học cần sửa hoặc Môn học bạn chọn đã bị xóa khỏi hệ thống
            new SubjectCatBusiness(null).CheckAvailable((int)subjectCat.SubjectCatID, "SubjectCat_Label_SubjectCatID", true);

            //Tên Môn học không được để trống
            Utils.ValidateRequire(subjectCat.SubjectName, "SubjectCat_Label_SubjectName");

            //Độ dài Tên Môn học không được vượt quá 50
            Utils.ValidateMaxLength(subjectCat.SubjectName, 50, "SubjectCat_Label_SubjectName");

            //Cấp học chỉ nhận giá trị từ 1 - 5 - Kiểm tra AppliedLevel
            Utils.ValidateRange((int)subjectCat.AppliedLevel, 1, 5, "SubjectCat_Label_AppliedLevel");

            //Tên Môn học đã tồn tại - Kiểm tra SubjectName / AppliedLevel với IsActive=TRUE            
            IDictionary<string, object> expDic = null;
            if (subjectCat.SubjectCatID > 0)
            {
                expDic = new Dictionary<string, object>();
                expDic["SubjectCatID"] = subjectCat.SubjectCatID;
            }
            bool OtherSubjectNameCompatible = this.repository.ExistsRow(GlobalConstants.LIST_SCHEMA, "SubjectCat",
                new Dictionary<string, object>()
                {
                    {"SubjectName",subjectCat.SubjectName},
                    {"AppliedLevel",subjectCat.AppliedLevel},
                    {"IsActive",true}
                }, expDic);
            if (OtherSubjectNameCompatible)
            {
                List<object> listParam = new List<object>();
                listParam.Add("SubjectCat_Label_SubjectName");
                throw new BusinessException("Common_Validate_Duplicate", listParam);
            }

            /****************************************************************/

            //Độ dài trường Tên viết tắt không được vượt quá 10 - Kiểm tra Abbreviation
            Utils.ValidateMaxLength(subjectCat.Abbreviation, 10, "SubjectCat_Label_Abbreviation");

            //Abbreviation: require\
            Utils.ValidateRequire(subjectCat.Abbreviation, "SubjectCat_Label_Abbreviation");

            //Tên hiển thị không được để trống - Kiểm tra DisplayName
            Utils.ValidateRequire(subjectCat.DisplayName, "SubjectCat_Label_DisplayName");

            //Độ dài trường Tên hiển thị không được vượt quá 30 - Kiểm tra DisplayName
            Utils.ValidateMaxLength(subjectCat.DisplayName, 30, "SubjectCat_Label_DisplayName");

            //IDictionary<string, object> dic = new Dictionary<string, object>();
            //dic["SubjectName"] = subjectCat.SubjectName;
            //dic["IsActive"] = true;
            //ApprenticeshipSubject aSubject = ApprenticeshipSubjectBusiness.Search(dic).ToList().FirstOrDefault();
            //if (aSubject != null)
            //{
            //    ApprenticeshipSubjectBusiness.Delete(aSubject.ApprenticeshipSubjectID);
            //    ApprenticeshipSubjectBusiness.Save();
            //}
            if (subjectCat.IsApprenticeshipSubject)
            {
                if (subjectCat.ApprenticeshipSubjects.Count() == 0)
                {
                    ApprenticeshipSubject ApprenticeshipSubject = new ApprenticeshipSubject();
                    ApprenticeshipSubject.SubjectID = subjectCat.SubjectCatID;
                    ApprenticeshipSubject.SubjectName = subjectCat.SubjectName;
                    ApprenticeshipSubject.IsActive = true;
                    ApprenticeshipSubjectBusiness.Insert(ApprenticeshipSubject);
                }

                //if (aSubject.SubjectID == subjectCat.SubjectCatID)
                //{
                //    ApprenticeshipSubjectBusiness.Update(ApprenticeshipSubject);
                //}
                //else
                //{
                //    ApprenticeshipSubjectBusiness.Insert(ApprenticeshipSubject);
                //}
            }
            else
            {
                if (subjectCat.ApprenticeshipSubjects.Count() > 0)
                {
                    foreach (var item in subjectCat.ApprenticeshipSubjects)
                    {
                        item.IsActive = false;
                        ApprenticeshipSubjectBusiness.Update(item);
                    }
                }
            }
            return base.Update(subjectCat);
        }

        /// <summary>
        /// namdv3
        /// </summary>
        /// <param name="AppliedLevel"></param>
        /// <returns></returns>
        public int GetMaxOrderIsSubjectByGrade(int AppliedLevel)
        {
            //Thực hiện tìm kiếm trong bảng SubjectCat theo các thông tin AppliedLevel, IsActive = true để lấy ra max(OrderIsSubject)
            IDictionary<string, object> dic = new Dictionary<string, object>();
            dic["AppliedLevel"] = AppliedLevel;
            dic["IsActive"] = true;
            SubjectCat SubjectCat = this.Search(dic).OrderByDescending(o => o.OrderInSubject).FirstOrDefault();
            if (SubjectCat != null && SubjectCat.OrderInSubject.HasValue)
            {
                return SubjectCat.OrderInSubject.Value + 1;
            }
            else return 1;
        }

        public void UpdateOrderInSubject(Dictionary<int, int?> dicData)
        {
            if (dicData != null && dicData.Count > 0)
            {
                foreach (var item in dicData)
                {
                    if (item.Value.HasValue)
                    {
                        SubjectCat subjectCat = this.Find(item.Key);
                        new SubjectCatBusiness(null).CheckAvailable((int)subjectCat.SubjectCatID, "SubjectCat_Label_SubjectCatID", true);
                        subjectCat.OrderInSubject = item.Value;
                        base.Update(subjectCat);
                    }
                }
            }
        }

        public List<SubjectCatBO> GetListSubjectBO(List<int> listSubjectID, int AppliedLevel, int AcademicYearID, int ClassID, int EducationLevelID, int Semester)
        {
            List<SubjectCatBO> listSubjectCat = (from cs in ClassSubjectBusiness.All
                                                 join sc in SubjectCatBusiness.All on cs.SubjectID equals sc.SubjectCatID
                                                 join cp in ClassProfileBusiness.All on cs.ClassID equals cp.ClassProfileID
                                                 where listSubjectID.Contains(cs.SubjectID)
                                                 && sc.AppliedLevel == AppliedLevel
                                                 && cp.AcademicYearID == AcademicYearID
                                                 && cp.EducationLevel.Grade == AppliedLevel
                                                 && (cs.ClassID == ClassID || ClassID == 0)
                                                 && ( cp.EducationLevelID == EducationLevelID || EducationLevelID == 0)
                                                 && sc.IsActive == true
                                                 && ((Semester == GlobalConstants.SEMESTER_OF_YEAR_FIRST && cs.SectionPerWeekFirstSemester > 0)
                                                     || (Semester == GlobalConstants.SEMESTER_OF_YEAR_SECOND && cs.SectionPerWeekSecondSemester > 0))
                                                 && cp.IsActive.Value
                                                 select new SubjectCatBO
                                                 {
                                                     SubjectCatID = cs.SubjectID,
                                                     SubjectName = sc.DisplayName,
                                                     IsCommenting = cs.IsCommenting,
                                                     DisplayName = sc.DisplayName,
                                                     SubjectIdInCrease = cs.SubjectIDIncrease,
                                                     ClassID = cp.ClassProfileID
                                                 }).ToList();
            return listSubjectCat;
        }
    }
}