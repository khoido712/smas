﻿/**
 * The Viettel License
 *
 * Copyright 2012 Viettel Telecom. All rights reserved.
 * VIETTEL PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */

/** 
* @author  
* @version $Revision: $
*/

using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Linq.Expressions;
using System.Web;
using log4net;

using SMAS.DAL.Repository;
using SMAS.DAL.IRepository;
using SMAS.Business.IBusiness;
using SMAS.Models.Models;
using SMAS.Business.Common;
using SMAS.Business.BusinessObject;
using System.Transactions;
using SMAS.VTUtils.Excel.ContextExt;
using SMAS.Business.Common.Extension;
using System.Web.Configuration;

namespace SMAS.Business.Business
{
    public partial class SMSParentContractDetailBusiness
    {
        #region constant/parameter
        /// <summary>
        /// them moi account
        /// </summary>
        private const string SMS_NOTICE_SPARENT_ACCOUNT_ADD = "SMAS: Quy Phu huynh da dang ky thanh cong DV So lien lac dien tu cua HS {0}. Phu huynh se nhan thong bao tu nha truong qua SMS hoac truy cap {1} bang SDT {2} va mat khau {3}. Chi tiet LH 18008000 nhanh 1 (mien phi)";
        private const string SMS_NOTICE_SPARENT_ACCOUNT_ADD_NEW = "SMAS: Quy Phu huynh da dang ky thanh cong DV So lien lac dien tu cua HS {0}. Phu huynh se nhan thong bao tu nha truong qua SMS hoac truy cap {1} bang SDT {2}. Chi tiet LH 18008000 nhanh 1 (mien phi)";
        private const int ACTION_TYPE_SEND_TO_PARENT_ADD = 1;

        /// <summary>
        /// xoa account
        /// </summary>
        private const string SMS_NOTICE_SPARENT_ACCOUNT_DELETE = "SMAS: Quy Phu huynh da huy thanh cong DV So lien lac dien tu cua HS {0}. De dang ky lai, vui long lien he nha truong, hoac goi 18008000 nhanh 1 (mien phi).";
        private const int ACTION_TYPE_SEND_TO_PARENT_DELETE = 2;
        /// <summary>
        /// chan cat
        /// </summary>
        private const string SMS_NOTICE_SPARENT_ACCOUNT_DISABLE = "SMAS: Quy Phu huynh da tam ngung thanh cong DV So lien lac dien tu cua HS {0}. De dang ky lai, vui long lien he nha truong, hoac goi 18008000 nhanh 1 (mien phi).";
        private const int ACTION_TYPE_SEND_TO_PARENT_DISABLE = 3;

        /// <summary>
        /// gia han
        /// </summary>
        private const string SMS_NOTICE_PASSWORD_RENEW = "SMAS: Quy Phu huynh da gia han thanh cong DV So lien lac dien tu cua HS {0}. Phu huynh se nhan thong bao tu nha truong qua SMS hoac truy cap {1} bang SDT {2}. Chi tiet LH 18008000 nhanh 1 (mien phi)";
        private const int ACTION_TYPE_SEND_TO_PARENT_RENEWABLE = 4;

        /// <summary>
        /// huy chan cat
        /// </summary>
        private const string SMS_NOTICE_SPARENT_ACCOUNT_ENABLED = "SMAS: Quy Phu huynh da khoi phuc thanh cong DV So lien lac dien tu cua HS {0}. Phu huynh se nhan thong bao tu nha truong qua SMS hoac truy cap {1} bang SDT {2}. Chi tiet LH 18008000 nhanh 1 (mien phi)";
        private const int ACTION_TYPE_SEND_TO_PARENT_ENABLED = 5;

        #endregion

        /// <summary>
        /// Them moi thue bao
        /// </summary>
        /// <param name="listDetailInsert"></param>
        public void SaveDetail(List<SMS_PARENT_CONTRACT_DETAIL> listDetailInsert, int year, int schooId)
        {
            if (listDetailInsert != null && listDetailInsert.Count > 0)
            {
                List<long> lstPupilContractID = listDetailInsert.Select(o => o.SMS_PARENT_CONTRACT_ID).ToList();
                #region viethd31 Lay ra cac dung thu de huy
                List<SMS_PARENT_CONTRACT_DETAIL> listLimitContractDetail = (from pc in this.SMSParentContractBusiness.All
                                                                            join pcd in this.SMSParentContractDetailBusiness.All on pc.SMS_PARENT_CONTRACT_ID equals pcd.SMS_PARENT_CONTRACT_ID
                                                                            join sp in this.ServicePackageBusiness.All on pcd.SERVICE_PACKAGE_ID equals sp.SERVICE_PACKAGE_ID
                                                                            join spd in this.ServicePackageDeclareBusiness.All on sp.SERVICE_PACKAGE_ID equals spd.SERVICE_PACKAGE_ID
                                                                            where pc.SCHOOL_ID == schooId && pc.PARTITION_ID == schooId % GlobalConstantsEdu.PARTITION_SMSPARENTCONTRACT_SCHOOLID
                                                                            && pcd.YEAR_ID == year
                                                                            && spd.YEAR == year
                                                                            && pcd.IS_ACTIVE
                                                                            && (pc.IS_DELETED == false || pc.IS_DELETED == null)
                                                                            && pcd.SUBSCRIPTION_STATUS != GlobalConstantsEdu.STATUS_DELETE_CONTRACT_DETAIL
                                                                            && lstPupilContractID.Contains(pc.SMS_PARENT_CONTRACT_ID)
                                                                            && spd.IS_LIMIT == true
                                                                            select pcd).ToList();

                #endregion
                #region Data AnhVD9 20150115
                List<long> contractIds = listDetailInsert.Select(o => o.SMS_PARENT_CONTRACT_ID).ToList();
                List<SMS_PARENT_CONTRACT> lstExistContract = this.SMSParentContractBusiness.All.Where(o => contractIds.Contains(o.SMS_PARENT_CONTRACT_ID)).ToList();
                SMS_PARENT_CONTRACT_DETAIL objFirst = listDetailInsert.FirstOrDefault();

                // 20151020 AnhVD9 Hotfix - Lay lai HS trong danh sach 
                List<int> pupilIds = lstExistContract.Select(o => o.PUPIL_ID).ToList();
                List<PupilProfileBO> LstPupilProfile = PupilProfileBusiness.GetPupilsByYear(pupilIds, objFirst.YEAR_ID);
                List<ServicePackageBO> LstServicePackage = ServicePackageDetailBusiness.GetListMaxSMSSP(year);
                ServicePackageBO objSP = null;
                List<ContractDetailInClassBO> lstContractInClass = new List<ContractDetailInClassBO>();
                ContractDetailInClassBO objDetailInClass = null;

                List<SParentAccountBO> lstSParentAccount = new List<SParentAccountBO>();
                SParentAccountBO objSParentAccount = null;

                PupilProfileBO objPupilProfile = null;
                SMS_PARENT_CONTRACT entity = null;
                #endregion

                
                SMS_PARENT_CONTRACT_DETAIL objInsert = null;
                for (int i = 0; i < listDetailInsert.Count; i++)
                {
                    objInsert = listDetailInsert[i];
                    entity = lstExistContract.FirstOrDefault(o => o.SMS_PARENT_CONTRACT_ID == objInsert.SMS_PARENT_CONTRACT_ID);
                    objPupilProfile = LstPupilProfile.FirstOrDefault(o => o.PupilProfileID == entity.PUPIL_ID);

                    this.SMSParentContractDetailBusiness.Insert(objInsert);

                    //viethd31: Kiem tra neu ton tai goi dung thu cung thoi han thi huy
                    List<SMS_PARENT_CONTRACT_DETAIL> lstLimitContract = listLimitContractDetail.Where(o => o.SUBSCRIBER.Equals(objInsert.SUBSCRIBER)).ToList();

                    lstLimitContract.ForEach(o =>
                    {
                        o.SUBSCRIPTION_STATUS = GlobalConstantsEdu.STATUS_DELETE_CONTRACT_DETAIL;
                        o.IS_ACTIVE = false;
                        o.UPDATED_TIME = DateTime.Now;
                        this.Update(o);
                    });

                    #region Add data for update SMSparentContractDetailInClass
                    // Không cộng tin nhắn với Hợp đồng Nợ cước
                    if (objInsert.SUBSCRIPTION_STATUS == GlobalConstantsEdu.STATUS_REGISTED)
                    {
                        objDetailInClass = new ContractDetailInClassBO();
                        objDetailInClass.SchoolID = entity.SCHOOL_ID;
                        objDetailInClass.PupilID = entity.PUPIL_ID;
                        objDetailInClass.ClassID = objPupilProfile.CurrentClassID;
                        objDetailInClass.AcademicYearID = objPupilProfile.CurrentAcademicYearID;
                        objDetailInClass.ServicePackageID = objInsert.SERVICE_PACKAGE_ID;
                        objDetailInClass.Semester = objInsert.SUBSCRIPTION_TIME;
                        objSP = LstServicePackage.FirstOrDefault(o => o.ServicePackageID == objInsert.SERVICE_PACKAGE_ID);
                        if (objSP != null && objSP.IsLimit != true && objSP.MaxSMS.HasValue)
                        {
                            objDetailInClass.MaxSMS = objSP.MaxSMS.HasValue ? objInsert.SUBSCRIPTION_TIME == 3 ? objSP.MaxSMS.Value * 2 : objSP.MaxSMS : 0;
                        }
                        lstContractInClass.Add(objDetailInClass);
                    }
                    #endregion

                    #region Send register account information SParent SMS to parent
                    if (!lstSParentAccount.Exists(o => o.PupilID == entity.PUPIL_ID && o.UserName.Equals(objInsert.SUBSCRIBER)))
                    {
                        objSParentAccount = new SParentAccountBO();
                        objSParentAccount.PupilID = entity.PUPIL_ID;
                        objSParentAccount.FullName = objPupilProfile.FullName;
                        objSParentAccount.UserName = objInsert.SUBSCRIBER.ExtensionMobile();
                        objSParentAccount.IsNew = true;
                        lstSParentAccount.Add(objSParentAccount);
                    }
                    #endregion
                }
                this.Save();

                // Update message budget in class
                SMSParentContractInClassDetailBusiness.UpdateMessageBudgetInClass(lstContractInClass, year);

                // Send SParent account to parent
                this.SMSParentContractBusiness.CreateListSParentAccount(lstSParentAccount);
            }
        }

        /// <summary>
        /// Kiem tra hop dong da ton tai hay chua?
        /// </summary>
        /// <param name="schoolID"></param>
        /// <param name="year"></param>
        /// <param name="contractID"></param>
        /// <returns></returns>
        public bool CheckIDSMSParentContract(int schoolID, int year, int contractID)
        {
            return this.SMSParentContractBusiness.All.Count(p => p.SMS_PARENT_CONTRACT_ID == contractID && p.SCHOOL_ID == schoolID) > 0;
        }

        /// <summary>
        /// Huy thue bao dong thoi huy hop dong neu chi co 1 thue bao (tra ve true neu co delete hop dong nguoc lai chi xoa chi tiet hop dong)
        /// </summary>
        /// <param name="contractID"></param>
        /// <param name="phone"></param>
        public void DeleteContractDetail(int contractID, string phone, int servicePackageID)
        {
            DateTime datenow = DateTime.Now;
            //update thong tin
            List<SMS_PARENT_CONTRACT_DETAIL> listUpdate = this.All
                .Where(p => p.SMS_PARENT_CONTRACT_ID == contractID && p.SUBSCRIBER == phone
                 && p.SUBSCRIPTION_STATUS != GlobalConstantsEdu.STATUS_DELETE_CONTRACT_DETAIL
                 && p.SERVICE_PACKAGE_ID == servicePackageID && p.IS_ACTIVE).ToList();

            List<SParentAccountBO> lstSParentAccount = new List<SParentAccountBO>();
            SParentAccountBO objSParentAccount = null;
            SMS_PARENT_CONTRACT entity = this.SMSParentContractBusiness.Find(contractID);

            if (listUpdate != null && listUpdate.Count > 0)
            {
                SMS_PARENT_CONTRACT_DETAIL objFirst = listUpdate.FirstOrDefault();
                PupilProfileBO objPupilProfile = PupilProfileBusiness.GetPupilProfileByYear(entity.PUPIL_ID, objFirst.YEAR_ID);

                for (int i = 0; i < listUpdate.Count; i++)
                {
                    listUpdate[i].SUBSCRIPTION_STATUS = GlobalConstantsEdu.STATUS_DELETE_CONTRACT_DETAIL;
                    listUpdate[i].UPDATED_TIME = datenow;

                    #region Send account information SParent SMS to parent
                    if (!lstSParentAccount.Exists(o => o.PupilID == entity.PUPIL_ID && o.UserName.Equals(listUpdate[i].SUBSCRIBER)))
                    {
                        objSParentAccount = new SParentAccountBO();
                        objSParentAccount.PupilID = entity.PUPIL_ID;
                        objSParentAccount.FullName = objPupilProfile.FullName;
                        objSParentAccount.UserName = listUpdate[i].SUBSCRIBER;
                        if (SubscriberOnlyOnePupil(entity.PUPIL_ID, listUpdate[i].SUBSCRIBER)) objSParentAccount.IsCancel = true;
                        lstSParentAccount.Add(objSParentAccount);
                    }
                    #endregion
                }
            }
            this.Save();

            // Send SParent account to parent
            this.DeleteListSParentAccount(lstSParentAccount);
        }

        /// <summary>
        /// Hủy danh sách thuê bao
        /// </summary>
        /// <author date="19/02/2016">Anhvd9</author>
        /// <param name="lstContractID"></param>
        /// <param name="lstPhone"></param>
        /// <param name="lstServicePackageID"></param>
        /// <param name="lstContractDetailID"></param>
        public void CancelListContractDetail(List<long> lstContractID, List<string> lstPhone, List<int> lstServicePackageID, List<long> lstContractDetailID)
        {
            List<SMS_PARENT_CONTRACT_DETAIL> lstContractDetail = this.SMSParentContractDetailBusiness.All
                .Where(p => lstContractID.Contains(p.SMS_PARENT_CONTRACT_ID) && lstPhone.Contains(p.SUBSCRIBER)
                 && p.SUBSCRIPTION_STATUS != GlobalConstantsEdu.STATUS_DELETE_CONTRACT_DETAIL
                 && lstServicePackageID.Contains(p.SERVICE_PACKAGE_ID) && p.IS_ACTIVE).ToList();

            for (int i = 0; i < lstContractID.Count; i++)
            {
                if (lstContractDetailID[i] == 0)
                {
                    List<SMS_PARENT_CONTRACT_DETAIL> lstDetail = lstContractDetail.Where(o => o.SMS_PARENT_CONTRACT_ID == lstContractID[i] && o.SUBSCRIBER == lstPhone[i]).ToList();
                    lstContractDetailID.AddRange(lstDetail.Select(o => o.SMS_PARENT_CONTRACT_DETAIL_ID));
                }
            }

            List<long> lstContractIDCancel1 = lstContractDetail.Where(o => lstContractDetailID.Contains(o.SMS_PARENT_CONTRACT_DETAIL_ID)).Select(o => o.SMS_PARENT_CONTRACT_ID).ToList();
            lstContractDetail = lstContractDetail.Except(lstContractDetail.Where(o => lstContractIDCancel1.Contains(o.SMS_PARENT_CONTRACT_ID) && !lstContractDetailID.Contains(o.SMS_PARENT_CONTRACT_DETAIL_ID))).ToList();

            List<SParentAccountBO> lstSParentAccount = new List<SParentAccountBO>();

            if (lstContractDetail != null && lstContractDetail.Count > 0)
            {
                SMS_PARENT_CONTRACT_DETAIL obj = lstContractDetail.FirstOrDefault();
                List<int> LstPupilID = this.SMSParentContractBusiness.All.Where(o => lstContractID.Contains(o.SMS_PARENT_CONTRACT_ID)).Select(o => o.PUPIL_ID).ToList();
                List<PupilProfileBO> lstPPBO = PupilProfileBusiness.GetPupilsByYear(LstPupilID, obj.YEAR_ID);
                PupilProfileBO pupilBO = null;
                SParentAccountBO objSParentAccount = null;
                DateTime Now = DateTime.Now;
                int PupilID = 0;
                for (int i = lstContractDetail.Count - 1; i >= 0; i--)
                {
                    obj = lstContractDetail[i];
                    obj.SUBSCRIPTION_STATUS = GlobalConstantsEdu.STATUS_DELETE_CONTRACT_DETAIL;
                    obj.UPDATED_TIME = Now;

                    #region Send account information SParent SMS to parent
                    PupilID = obj.PUPIL_ID;
                    pupilBO = lstPPBO.FirstOrDefault(o => o.PupilProfileID == PupilID);
                    if (!lstSParentAccount.Exists(o => o.PupilID == PupilID && o.UserName.Equals(obj.SUBSCRIBER)) && pupilBO != null)
                    {
                        objSParentAccount = new SParentAccountBO();
                        objSParentAccount.PupilID = PupilID;
                        objSParentAccount.FullName = pupilBO.FullName;
                        objSParentAccount.UserName = lstContractDetail[i].SUBSCRIBER;
                        lstSParentAccount.Add(objSParentAccount);
                    }
                    #endregion
                }
            }
            this.Save();

            // Send SParent account to parent
            this.DeleteListSParentAccount(lstSParentAccount);
        }

        /// <summary>
        /// lay thong tin hop dong
        /// </summary>
        /// <param name="contractID"></param>
        /// <param name="phone"></param>
        /// <returns></returns>
        public List<SMS_PARENT_CONTRACT_DETAIL> GetContractDetailByContactID(int contractID, string phone, int servicePackageID)
        {
            List<SMS_PARENT_CONTRACT_DETAIL> detailList = this.SMSParentContractDetailBusiness.All
               .Where(p => p.SMS_PARENT_CONTRACT_ID == contractID
                   && p.SUBSCRIBER == phone
                   && p.SERVICE_PACKAGE_ID == servicePackageID
                   && p.IS_ACTIVE
                   && p.SUBSCRIPTION_STATUS != GlobalConstantsEdu.STATUS_DELETE_CONTRACT_DETAIL).ToList();
            if (detailList != null && detailList.Count > 0)
            {
                return detailList;
            }
            return new List<SMS_PARENT_CONTRACT_DETAIL>();
        }

        /// <summary>
        /// kiem tra so thue bao chi cua 1 hoc sinh duy nhat 
        /// </summary>
        /// <param name="PupilID"></param>
        /// <param name="SUBSCRIBER"></param>
        /// <returns>true: neu thue bao chi cua hoc sinh</returns>
        private bool SubscriberOnlyOnePupil(int PupilID, string SUBSCRIBER)
        {
            return (from ct in this.SMSParentContractBusiness.All
                    join ctd in this.SMSParentContractDetailBusiness.All on ct.SMS_PARENT_CONTRACT_ID equals ctd.SMS_PARENT_CONTRACT_ID
                    where ct.STATUS == GlobalConstantsEdu.STATUS_CONTRACT_APPROVAL &&
                        ct.IS_DELETED != true && ct.PUPIL_ID != PupilID && ctd.SUBSCRIBER.Equals(SUBSCRIBER)
                        && ctd.IS_ACTIVE == true && ctd.SUBSCRIPTION_STATUS == GlobalConstantsEdu.SMS_PARENT_CONTRACT_DETAIL_AVAILABLE
                    select ct.PUPIL_ID).Distinct().Count() == 0;
        }

        /// <summary>
        /// Sua thong tin cua thue bao trong hop dong
        /// </summary>
        /// <param name="contractID"></param>
        /// <param name="phoneOld"></param>
        /// <param name="phoneNew"></param>
        /// <param name="year"></param>
        /// <param name="editList"></param>
        public void SaveEditDetail(int contractID, string phoneOld, string phoneNew, int year, List<SMS_PARENT_CONTRACT_DETAIL> editList)
        {
            List<SMS_PARENT_CONTRACT_DETAIL> detailList = this.SMSParentContractDetailBusiness.All
                                            .Where(p => p.SMS_PARENT_CONTRACT_ID == contractID
                                                && p.SUBSCRIBER == phoneOld
                                                && p.YEAR_ID == year
                                                && p.IS_ACTIVE
                                                && p.SUBSCRIPTION_STATUS != GlobalConstantsEdu.STATUS_DELETE_CONTRACT_DETAIL).ToList();

            List<SMS_PARENT_CONTRACT_EXTRA> detailListExtra = (from d in this.SMSParentContractDetailBusiness.All
                                                            join e in this.SMSParentContractExtraBusiness.All on d.SMS_PARENT_CONTRACT_DETAIL_ID equals e.SMS_PARENT_CONTRACT_DETAIL_ID
                                                            where d.SMS_PARENT_CONTRACT_ID == contractID
                                                            && d.YEAR_ID == year
                                                            && d.IS_ACTIVE
                                                            && e.IS_ACTIVE
                                                            && d.SUBSCRIPTION_STATUS != GlobalConstantsEdu.STATUS_DELETE_CONTRACT_DETAIL
                                                            select e).ToList();

            bool isSameEditView = detailList.Select(o => o.SERVICE_PACKAGE_ID).Distinct().Count() == 1;
            List<int> semesterListDetail = detailList.Select(p => p.SUBSCRIPTION_TIME).ToList();// list hoc ky co trong db

            List<int> semestEdit = new List<int>();

            List<ContractDetailInClassBO> lstContractInClass = new List<ContractDetailInClassBO>();
            List<SParentAccountBO> lstSParentAccountAdd = new List<SParentAccountBO>();
            List<SParentAccountBO> lstSParentAccountDel = new List<SParentAccountBO>();

            #region Data - AnhVD9 20150115
            SMS_PARENT_CONTRACT_DETAIL objFirst = editList.FirstOrDefault();
            int SubsriptionStatus = objFirst.SUBSCRIPTION_STATUS;
            int CurrentSubsriptionStatus = detailList.FirstOrDefault().SUBSCRIPTION_STATUS;
            SMS_PARENT_CONTRACT entity = this.SMSParentContractBusiness.Find(contractID);
            int schoolID = entity.SCHOOL_ID;
            PupilProfileBO objPupilProfile = PupilProfileBusiness.GetPupilProfileByYear(entity.PUPIL_ID, year);
            List<ServicePackageBO> LstServicePackage = this.ServicePackageDetailBusiness.GetListMaxSMSSP(year);
            ServicePackageBO objSPCurrent = null;
            ServicePackageBO objSPNew = null;
            ContractDetailInClassBO objDetailInClass = null;

            SParentAccountBO objSParentAccount = null;

            //VTODO SMSParentContractChangeHistory objChange = null;
            DateTime Now = DateTime.Now;
            SMS_PARENT_CONTRACT_DETAIL objInsert = null;
            #endregion

            if (editList != null && editList.Count > 0)
            {
                SMS_PARENT_CONTRACT_DETAIL objTmp = null;
                if (semesterListDetail.Exists(p => p == GlobalConstants.SEMESTER_OF_YEAR_ALL))
                {
                    #region Luu thong tin ca nam voi semester = 3
                    for (int i = 0; i < editList.Count; i++)
                    {
                        objTmp = editList[i];
                        SMS_PARENT_CONTRACT_DETAIL objUpdate = detailList.Where(p => p.SUBSCRIPTION_TIME == objTmp.SUBSCRIPTION_TIME).FirstOrDefault();
                        if (objUpdate != null)
                        {
                            //VTODO
                            //#region Recording change subscriber
                            //if (string.Compare(phoneNew, phoneOld) != 0)
                            //{
                            //    objChange = new SMSParentContractChangeHistory();
                            //    objChange.SMS_PARENT_CONTRACT_DETAIL_ID = objUpdate.SMS_PARENT_CONTRACT_DETAIL_ID;
                            //    objChange.ChangeType = GlobalConstants.SMSPARENT_CONTRACT_CHANGETYPE_SUBSCRIBER;
                            //    objChange.OldSubscriber = phoneOld;
                            //    objChange.NewSubscriber = phoneNew;
                            //    objChange.ChangeDate = Now;
                            //    this.Context.SMSParentContractChangeHistories.Add(objChange);
                            //}
                            //#endregion

                            //neu ton tai thue bao voi thoi gian dang ky thi update
                            objUpdate.RELATION_SHIP = objTmp.RELATION_SHIP;
                            objUpdate.RECEIVER_NAME = objTmp.RECEIVER_NAME;
                            objUpdate.SUBSCRIBER = objTmp.SUBSCRIBER.ExtensionMobile();
                            objUpdate.SUBSCRIBER_TYPE = objUpdate.SUBSCRIBER.CheckMobileNumberVT() ? 0 : 1;
                            objUpdate.SERVICE_PACKAGE_ID = objTmp.SERVICE_PACKAGE_ID;
                            objUpdate.SUBSCRIPTION_TIME = objTmp.SUBSCRIPTION_TIME;
                            objUpdate.AMOUNT = objTmp.AMOUNT;
                            // Các thuê bao có trạng thái Nợ cước thì không được Tạm ngừng
                            if (objUpdate.SUBSCRIPTION_STATUS < GlobalConstantsEdu.STATUS_UNPAID) objUpdate.SUBSCRIPTION_STATUS = objTmp.SUBSCRIPTION_STATUS;
                        }
                        else
                        {
                            //insert
                            //Neu khong ton tai thue bao trong thoi gian dang ky thi insert
                            objInsert = new SMS_PARENT_CONTRACT_DETAIL();
                            objInsert.SMS_PARENT_CONTRACT_ID = contractID;
                            objInsert.PARTITION_ID = UtilsBusiness.GetPartionId(entity.SCHOOL_ID);
                            objInsert.RELATION_SHIP = objTmp.RELATION_SHIP;
                            objInsert.RECEIVER_NAME = objTmp.RECEIVER_NAME;
                            objInsert.SUBSCRIBER = objTmp.SUBSCRIBER.ExtensionMobile();
                            objInsert.SERVICE_PACKAGE_ID = objTmp.SERVICE_PACKAGE_ID;
                            objInsert.SUBSCRIPTION_TIME = objTmp.SUBSCRIPTION_TIME;
                            objInsert.AMOUNT = objTmp.AMOUNT;
                            objInsert.SUBSCRIPTION_STATUS = objTmp.SUBSCRIPTION_STATUS;
                            objInsert.YEAR_ID = year;
                            objInsert.SUBSCRIBER_TYPE = objInsert.SUBSCRIBER.CheckMobileNumberVT() ? 0 : 1;
                            objInsert.IS_ACTIVE = true;
                            objInsert.CREATED_TIME = DateTime.Now;
                            objInsert.CLASS_ID = entity.CLASS_ID;
                            objInsert.PUPIL_ID = entity.PUPIL_ID;
                            objInsert.SCHOOL_ID = entity.SCHOOL_ID;
                            objInsert.ACADEMIC_YEAR_ID = entity.ACADEMIC_YEAR_ID;
                            this.SMSParentContractDetailBusiness.Insert(objInsert);

                            #region Add data for update SMSparentContractDetailInClass
                            if (objInsert.SUBSCRIPTION_STATUS == GlobalConstantsEdu.STATUS_REGISTED)
                            {
                                objDetailInClass = new ContractDetailInClassBO();
                                objDetailInClass.SchoolID = entity.SCHOOL_ID;
                                objDetailInClass.PupilID = entity.PUPIL_ID;
                                objDetailInClass.ClassID = objPupilProfile.CurrentClassID;
                                objDetailInClass.AcademicYearID = objPupilProfile.CurrentAcademicYearID;
                                objDetailInClass.ServicePackageID = objInsert.SERVICE_PACKAGE_ID;
                                objDetailInClass.Semester = objInsert.SUBSCRIPTION_TIME;
                                objSPNew = LstServicePackage.FirstOrDefault(o => o.ServicePackageID == objInsert.SERVICE_PACKAGE_ID);
                                if (objSPNew != null && objSPNew.IsLimit != true && objSPNew.MaxSMS.HasValue) objDetailInClass.MaxSMS = objSPNew.MaxSMS * 2;
                                lstContractInClass.Add(objDetailInClass);
                            }
                            #endregion
                        }
                    }

                    #region Send change account information SParent SMS to parent
                    if (string.Compare(phoneNew, phoneOld) != 0)
                    {
                        if (!lstSParentAccountAdd.Exists(o => o.PupilID == entity.PUPIL_ID && o.UserName.Equals(phoneNew)))
                        {
                            objSParentAccount = new SParentAccountBO();
                            objSParentAccount.PupilID = entity.PUPIL_ID;
                            objSParentAccount.FullName = objPupilProfile.FullName;
                            objSParentAccount.UserName = phoneNew;
                            objSParentAccount.IsNew = true;
                            lstSParentAccountAdd.Add(objSParentAccount);
                        }

                        if (!lstSParentAccountDel.Exists(o => o.PupilID == entity.PUPIL_ID && o.UserName.Equals(phoneOld)))
                        {
                            objSParentAccount = new SParentAccountBO();
                            objSParentAccount.PupilID = entity.PUPIL_ID;
                            objSParentAccount.FullName = objPupilProfile.FullName;
                            objSParentAccount.UserName = phoneOld;
                            if (SubscriberOnlyOnePupil(entity.PUPIL_ID, phoneOld)) objSParentAccount.IsCancel = true;
                            lstSParentAccountDel.Add(objSParentAccount);
                        }
                    }
                    #endregion

                    // Neu da dang ky 2 hoc ky hoac 1 hoc ky nhung bo check chon hoc ky thi isActive = false (huy so sdt nhan tin nhan)           
                    semestEdit = editList.Select(p => p.SUBSCRIPTION_TIME).ToList(); // Lay thong tin hoc ky;
                    List<int> deleteDetailList = semesterListDetail.Except(semestEdit).ToList();//List can phai xoa
                    if (deleteDetailList.Count > 0 && deleteDetailList != null)
                    {
                        int semesterDelete = 0;
                        for (int j = 0; j < deleteDetailList.Count; j++)
                        {
                            semesterDelete = deleteDetailList[j];
                            SMS_PARENT_CONTRACT_DETAIL objUpdate = detailList.Where(p => p.SUBSCRIPTION_TIME == semesterDelete).FirstOrDefault();
                            if (objUpdate != null) objUpdate.IS_ACTIVE = false;
                        }
                    }
                    #endregion
                }
                else
                {
                    #region luu thong tin hoc ky 1 va hoc ky 2
                    List<SMS_PARENT_CONTRACT_DETAIL> listEditModify = new List<SMS_PARENT_CONTRACT_DETAIL>();

                    #region chuan bi du lieu tach roi theo tung hoc ky
                    SMS_PARENT_CONTRACT_DETAIL detailObj = null;
                    if (editList.Exists(p => p.SUBSCRIPTION_TIME == GlobalConstants.SEMESTER_OF_YEAR_FIRST)
                        && editList.Exists(p => p.SUBSCRIPTION_TIME == GlobalConstants.SEMESTER_OF_YEAR_SECOND))
                    {
                        objTmp = editList.FirstOrDefault();
                        detailObj = new SMS_PARENT_CONTRACT_DETAIL();
                        detailObj.SMS_PARENT_CONTRACT_ID = contractID;
                        detailObj.PARTITION_ID = UtilsBusiness.GetPartionId(entity.SCHOOL_ID);
                        detailObj.RELATION_SHIP = objTmp.RELATION_SHIP;
                        detailObj.RECEIVER_NAME = objTmp.RECEIVER_NAME;
                        detailObj.SUBSCRIBER = objTmp.SUBSCRIBER.ExtensionMobile();
                        detailObj.SERVICE_PACKAGE_ID = objTmp.SERVICE_PACKAGE_ID;
                        detailObj.SUBSCRIPTION_TIME = GlobalConstants.SEMESTER_OF_YEAR_FIRST;
                        detailObj.AMOUNT = objTmp.AMOUNT;
                        detailObj.SUBSCRIPTION_STATUS = objTmp.SUBSCRIPTION_STATUS;
                        detailObj.YEAR_ID = year;
                        detailObj.SUBSCRIBER_TYPE = detailObj.SUBSCRIBER.CheckMobileNumberVT() ? 0 : 1;
                        detailObj.IS_ACTIVE = true;
                        detailObj.CREATED_TIME = DateTime.Now;
                        detailObj.CLASS_ID = entity.CLASS_ID;
                        detailObj.PUPIL_ID = entity.PUPIL_ID;
                        detailObj.SCHOOL_ID = entity.SCHOOL_ID;
                        detailObj.ACADEMIC_YEAR_ID = entity.ACADEMIC_YEAR_ID;
                        listEditModify.Add(detailObj);

                        detailObj = new SMS_PARENT_CONTRACT_DETAIL();
                        detailObj.SMS_PARENT_CONTRACT_ID = contractID;
                        detailObj.PARTITION_ID = UtilsBusiness.GetPartionId(entity.SCHOOL_ID);
                        detailObj.RELATION_SHIP = objTmp.RELATION_SHIP;
                        detailObj.RECEIVER_NAME = objTmp.RECEIVER_NAME;
                        detailObj.SUBSCRIBER = objTmp.SUBSCRIBER.ExtensionMobile();
                        detailObj.SERVICE_PACKAGE_ID = objTmp.SERVICE_PACKAGE_ID;
                        detailObj.SUBSCRIPTION_TIME = GlobalConstants.SEMESTER_OF_YEAR_SECOND;
                        detailObj.AMOUNT = objTmp.AMOUNT;
                        detailObj.SUBSCRIPTION_STATUS = objTmp.SUBSCRIPTION_STATUS;
                        detailObj.YEAR_ID = year;
                        detailObj.SUBSCRIBER_TYPE = detailObj.SUBSCRIBER.CheckMobileNumberVT() ? 0 : 1;
                        detailObj.IS_ACTIVE = true;
                        detailObj.CREATED_TIME = DateTime.Now;
                        detailObj.CLASS_ID = entity.CLASS_ID;
                        detailObj.PUPIL_ID = entity.PUPIL_ID;
                        detailObj.SCHOOL_ID = entity.SCHOOL_ID;
                        detailObj.ACADEMIC_YEAR_ID = entity.ACADEMIC_YEAR_ID;
                        listEditModify.Add(detailObj);
                    }
                    else
                    {
                        objTmp = editList.FirstOrDefault();
                        detailObj = new SMS_PARENT_CONTRACT_DETAIL();
                        detailObj.SMS_PARENT_CONTRACT_ID = contractID;
                        detailObj.PARTITION_ID = UtilsBusiness.GetPartionId(entity.SCHOOL_ID);
                        detailObj.RELATION_SHIP = objTmp.RELATION_SHIP;
                        detailObj.RECEIVER_NAME = objTmp.RECEIVER_NAME;
                        detailObj.SUBSCRIBER = objTmp.SUBSCRIBER.ExtensionMobile();
                        detailObj.SERVICE_PACKAGE_ID = objTmp.SERVICE_PACKAGE_ID;
                        detailObj.SUBSCRIPTION_TIME = objTmp.SUBSCRIPTION_TIME;
                        detailObj.AMOUNT = objTmp.AMOUNT;
                        detailObj.SUBSCRIPTION_STATUS = objTmp.SUBSCRIPTION_STATUS;
                        detailObj.YEAR_ID = year;
                        detailObj.SUBSCRIBER_TYPE = detailObj.SUBSCRIBER.CheckMobileNumberVT() ? 0 : 1;
                        detailObj.IS_ACTIVE = true;
                        detailObj.CREATED_TIME = DateTime.Now;
                        detailObj.CLASS_ID = entity.CLASS_ID;
                        detailObj.PUPIL_ID = entity.PUPIL_ID;
                        detailObj.SCHOOL_ID = entity.SCHOOL_ID;
                        detailObj.ACADEMIC_YEAR_ID = entity.ACADEMIC_YEAR_ID;
                        listEditModify.Add(detailObj);
                    }
                    #endregion

                    for (int i = 0; i < listEditModify.Count; i++)
                    {
                        objTmp = listEditModify[i];
                        SMS_PARENT_CONTRACT_DETAIL objUpdate = detailList.Where(p => p.SUBSCRIPTION_TIME == objTmp.SUBSCRIPTION_TIME).FirstOrDefault();
                        if (objUpdate != null)
                        {
                            //VTODO
                            //#region Recording change subscriber
                            //if (string.Compare(phoneNew, phoneOld) != 0)
                            //{
                            //    objChange = new SMSParentContractChangeHistory();
                            //    objChange.SMS_PARENT_CONTRACT_DETAIL_ID = objUpdate.SMS_PARENT_CONTRACT_DETAIL_ID;
                            //    objChange.ChangeType = GlobalConstants.SMSPARENT_CONTRACT_CHANGETYPE_SUBSCRIBER;
                            //    objChange.OldSubscriber = phoneOld;
                            //    objChange.NewSubscriber = phoneNew;
                            //    objChange.ChangeDate = Now;
                            //    this.Context.SMSParentContractChangeHistories.Add(objChange);
                            //}
                            //#endregion

                            //neu ton tai thue bao voi thoi gian dang ky thi update
                            objUpdate.RELATION_SHIP = objTmp.RELATION_SHIP;
                            objUpdate.RECEIVER_NAME = objTmp.RECEIVER_NAME;
                            objUpdate.SUBSCRIBER = objTmp.SUBSCRIBER;
                            objUpdate.SUBSCRIBER_TYPE = objUpdate.SUBSCRIBER.CheckMobileNumberVT() ? 0 : 1;
                            objUpdate.SERVICE_PACKAGE_ID = objTmp.SERVICE_PACKAGE_ID;
                            objUpdate.SUBSCRIPTION_TIME = objTmp.SUBSCRIPTION_TIME;
                            objUpdate.AMOUNT = objTmp.AMOUNT;
                            // Các thue bao có trạng thái Nợ cước thì không được Tạm ngừng
                            if (objUpdate.SUBSCRIPTION_STATUS < GlobalConstantsEdu.STATUS_UNPAID) objUpdate.SUBSCRIPTION_STATUS = objTmp.SUBSCRIPTION_STATUS;
                        }
                        else
                        {
                            //Neu khong ton tai thue bao trong thoi gian dang ky thi insert
                            objInsert = new SMS_PARENT_CONTRACT_DETAIL();
                            objInsert.SMS_PARENT_CONTRACT_ID = contractID;
                            objInsert.PARTITION_ID = UtilsBusiness.GetPartionId(entity.SCHOOL_ID);
                            objInsert.RELATION_SHIP = objTmp.RELATION_SHIP;
                            objInsert.RECEIVER_NAME = objTmp.RECEIVER_NAME;
                            objInsert.SUBSCRIBER = objTmp.SUBSCRIBER;
                            objInsert.SUBSCRIBER_TYPE = objInsert.SUBSCRIBER.CheckMobileNumberVT() ? 0 : 1;
                            objInsert.SERVICE_PACKAGE_ID = objTmp.SERVICE_PACKAGE_ID;
                            objInsert.SUBSCRIPTION_TIME = objTmp.SUBSCRIPTION_TIME;
                            objInsert.AMOUNT = objTmp.AMOUNT;
                            objInsert.SUBSCRIPTION_STATUS = objTmp.SUBSCRIPTION_STATUS;
                            objInsert.YEAR_ID = year;
                            objInsert.SUBSCRIBER_TYPE = objInsert.SUBSCRIBER.CheckMobileNumberVT() ? 0 : 1;
                            objInsert.IS_ACTIVE = true;
                            objInsert.CREATED_TIME = DateTime.Now;
                            objInsert.CLASS_ID = entity.CLASS_ID;
                            objInsert.PUPIL_ID = entity.PUPIL_ID;
                            objInsert.SCHOOL_ID = entity.SCHOOL_ID;
                            objInsert.ACADEMIC_YEAR_ID = entity.ACADEMIC_YEAR_ID;
                            this.SMSParentContractDetailBusiness.Insert(objInsert);

                            #region Add data for update SMSparentContractDetailInClass
                            if (objInsert.SUBSCRIPTION_STATUS == GlobalConstantsEdu.STATUS_REGISTED)
                            {
                                objDetailInClass = new ContractDetailInClassBO();
                                objDetailInClass.SchoolID = entity.SCHOOL_ID;
                                objDetailInClass.PupilID = entity.PUPIL_ID;
                                objDetailInClass.ClassID = objPupilProfile.CurrentClassID;
                                objDetailInClass.AcademicYearID = objPupilProfile.CurrentAcademicYearID;
                                objDetailInClass.ServicePackageID = objInsert.SERVICE_PACKAGE_ID;
                                objDetailInClass.Semester = objInsert.SUBSCRIPTION_TIME;
                                objSPNew = LstServicePackage.FirstOrDefault(o => o.ServicePackageID == objInsert.SERVICE_PACKAGE_ID);
                                if (objSPNew != null && objSPNew.IsLimit != true && objSPNew.MaxSMS.HasValue) objDetailInClass.MaxSMS = objSPNew.MaxSMS;
                                lstContractInClass.Add(objDetailInClass);
                            }
                            #endregion
                        }
                    }

                    #region Send change account information SParent SMS to parent
                    if (string.Compare(phoneNew, phoneOld) != 0)
                    {
                        if (!lstSParentAccountAdd.Exists(o => o.PupilID == entity.PUPIL_ID && o.UserName.Equals(phoneNew)))
                        {
                            objSParentAccount = new SParentAccountBO();
                            objSParentAccount.PupilID = entity.PUPIL_ID;
                            objSParentAccount.FullName = objPupilProfile.FullName;
                            objSParentAccount.UserName = phoneNew;
                            objSParentAccount.IsNew = true;
                            lstSParentAccountAdd.Add(objSParentAccount);
                        }

                        if (!lstSParentAccountDel.Exists(o => o.PupilID == entity.PUPIL_ID && o.UserName.Equals(phoneOld)))
                        {
                            objSParentAccount = new SParentAccountBO();
                            objSParentAccount.PupilID = entity.PUPIL_ID;
                            objSParentAccount.FullName = objPupilProfile.FullName;
                            objSParentAccount.UserName = phoneOld;
                            if (SubscriberOnlyOnePupil(entity.PUPIL_ID, phoneOld)) objSParentAccount.IsCancel = true;
                            lstSParentAccountDel.Add(objSParentAccount);
                        }
                    }
                    #endregion

                    #region Cap nhat lai hoc ky neu co bo chon 1 hoc ky
                    if (isSameEditView)
                    {
                        semestEdit = listEditModify.Select(p => p.SUBSCRIPTION_TIME).ToList();
                        List<int> deleteDetailList = semesterListDetail.Except(semestEdit).ToList();

                        if (deleteDetailList.Count > 0 && deleteDetailList != null)
                        {
                            int semesterDelete = 0;
                            for (int j = 0; j < deleteDetailList.Count; j++)
                            {
                                semesterDelete = deleteDetailList[j];
                                SMS_PARENT_CONTRACT_DETAIL objUpdate = detailList.Where(p => p.SUBSCRIPTION_TIME == semesterDelete).FirstOrDefault();
                                if (objUpdate != null) objUpdate.IS_ACTIVE = false;

                                if (objInsert != null)
                                {
                                    objDetailInClass = new ContractDetailInClassBO();
                                    objDetailInClass.SchoolID = entity.SCHOOL_ID;
                                    objDetailInClass.PupilID = entity.PUPIL_ID;
                                    objDetailInClass.ClassID = objPupilProfile.CurrentClassID;
                                    objDetailInClass.AcademicYearID = objPupilProfile.CurrentAcademicYearID;
                                    objDetailInClass.ServicePackageID = objUpdate.SERVICE_PACKAGE_ID;
                                    objDetailInClass.Semester = objUpdate.SUBSCRIPTION_TIME;
                                    objSPNew = LstServicePackage.FirstOrDefault(o => o.ServicePackageID == objUpdate.SERVICE_PACKAGE_ID);
                                    if (objSPNew != null && objSPNew.IsLimit != true && objSPNew.MaxSMS.HasValue) objDetailInClass.MaxSMS = 0 - objSPNew.MaxSMS;
                                    lstContractInClass.Add(objDetailInClass);

                                    //viethd31: Move cac goi mua them sang hoc ky moi
                                    List<SMS_PARENT_CONTRACT_EXTRA> lstExtra = detailListExtra.Where(o => o.SMS_PARENT_CONTRACT_DETAIL_ID == objUpdate.SMS_PARENT_CONTRACT_DETAIL_ID).ToList();
                                    lstExtra.ForEach(o =>
                                    {
                                        o.SMS_PARENT_CONTRACT_DETAIL_ID = objInsert.SMS_PARENT_CONTRACT_DETAIL_ID;
                                        this.SMSParentContractExtraBusiness.Update(o);

                                        objSPNew = LstServicePackage.FirstOrDefault(s => s.ServicePackageID == o.SERVICE_PACKAGE_ID);

                                        //tru quy tin hoc ky cu
                                        objDetailInClass = new ContractDetailInClassBO();
                                        objDetailInClass.SchoolID = entity.SCHOOL_ID;
                                        objDetailInClass.PupilID = entity.PUPIL_ID;
                                        objDetailInClass.ClassID = objPupilProfile.CurrentClassID;
                                        objDetailInClass.AcademicYearID = objPupilProfile.CurrentAcademicYearID;
                                        objDetailInClass.ServicePackageID = o.SERVICE_PACKAGE_ID;
                                        objDetailInClass.Semester = objUpdate.SUBSCRIPTION_TIME;

                                        if (objSPNew != null && objSPNew.IsLimit != true && objSPNew.MaxSMS.HasValue) objDetailInClass.MaxSMS = 0 - objSPNew.MaxSMS;

                                        lstContractInClass.Add(objDetailInClass);


                                        //cong quy tin hoc ky moi
                                        objDetailInClass = new ContractDetailInClassBO();
                                        objDetailInClass.SchoolID = entity.SCHOOL_ID;
                                        objDetailInClass.PupilID = entity.PUPIL_ID;
                                        objDetailInClass.ClassID = objPupilProfile.CurrentClassID;
                                        objDetailInClass.AcademicYearID = objPupilProfile.CurrentAcademicYearID;
                                        objDetailInClass.ServicePackageID = o.SERVICE_PACKAGE_ID;
                                        objDetailInClass.Semester = objInsert.SUBSCRIPTION_TIME;
                                        if (objSPNew != null && objSPNew.IsLimit != true && objSPNew.MaxSMS.HasValue) objDetailInClass.MaxSMS = objSPNew.MaxSMS;

                                        lstContractInClass.Add(objDetailInClass);

                                    });
                                }
                            }
                        }
                    }
                    #endregion

                    #region Send renewable subscriber information to parent
                    if (listEditModify.Count > 1 && SubsriptionStatus == GlobalConstantsEdu.STATUS_REGISTED && detailList.Count <= 1 && string.Compare(phoneNew, phoneOld) == 0)
                    {
                        if (!lstSParentAccountAdd.Exists(o => o.PupilID == entity.PUPIL_ID && o.UserName.Equals(objFirst.SUBSCRIBER)))
                        {
                            objSParentAccount = new SParentAccountBO();
                            objSParentAccount.PupilID = entity.PUPIL_ID;
                            objSParentAccount.FullName = objPupilProfile.FullName;
                            objSParentAccount.UserName = objFirst.SUBSCRIBER;
                            objSParentAccount.IsRenew = true;
                            lstSParentAccountAdd.Add(objSParentAccount);
                        }
                    }
                    #endregion

                    #region Send suspension/disable suspension subscriber information to parent
                    if (SubsriptionStatus == GlobalConstantsEdu.STATUS_STOP_USING && string.Compare(phoneNew, phoneOld) == 0)
                    {
                        if (!lstSParentAccountDel.Exists(o => o.PupilID == entity.PUPIL_ID && o.UserName.Equals(objFirst.SUBSCRIBER)))
                        {
                            objSParentAccount = new SParentAccountBO();
                            objSParentAccount.PupilID = entity.PUPIL_ID;
                            objSParentAccount.FullName = objPupilProfile.FullName;
                            objSParentAccount.UserName = objFirst.SUBSCRIBER;
                            objSParentAccount.IsSuspension = true;
                            if (SubscriberOnlyOnePupil(entity.PUPIL_ID, phoneOld)) objSParentAccount.IsCancel = true;
                            lstSParentAccountDel.Add(objSParentAccount);
                        }
                    }
                    else if (SubsriptionStatus == GlobalConstantsEdu.STATUS_REGISTED && CurrentSubsriptionStatus == GlobalConstantsEdu.STATUS_STOP_USING && string.Compare(phoneNew, phoneOld) == 0)
                    {
                        if (!lstSParentAccountDel.Exists(o => o.PupilID == entity.PUPIL_ID && o.UserName.Equals(objFirst.SUBSCRIBER)))
                        {
                            objSParentAccount = new SParentAccountBO();
                            objSParentAccount.PupilID = entity.PUPIL_ID;
                            objSParentAccount.FullName = objPupilProfile.FullName;
                            objSParentAccount.UserName = objFirst.SUBSCRIBER;
                            objSParentAccount.IsEnable = true;
                            lstSParentAccountDel.Add(objSParentAccount);
                        }
                    }
                    #endregion

                    #endregion
                }
            }

            this.Save();
            // Update message budget in class
            this.SMSParentContractInClassDetailBusiness.UpdateMessageBudgetInClass(lstContractInClass, year);
            // Send SParent account to parent
            this.SMSParentContractBusiness.CreateListSParentAccount(lstSParentAccountAdd);
            this.DeleteListSParentAccount(lstSParentAccountDel);
        }

        #region delete list sparent account
        /// <summary>
        /// xoa danh sach tai khoan
        /// </summary>
        /// <param name="lstSParent"></param>
        public void DeleteListSParentAccount(List<SParentAccountBO> lstSParent)
        {
            if (lstSParent != null && lstSParent.Count > 0)
            {
                SParentAccountBO objSParent = null;
                for (int i = lstSParent.Count - 1; i >= 0; i--)
                {
                    objSParent = lstSParent[i];
                    // Neu tam ngung
                    if (objSParent.IsSuspension.HasValue && objSParent.IsSuspension.Value)
                    {
                        // Neu chi co 1 HS thi huy luon tai khoan
                        if (objSParent.IsCancel.HasValue && objSParent.IsCancel.Value)
                            DeleteSParentLogonAccount(objSParent.FullName, objSParent.UserName, objSParent.UserName, true);
                        else // neu khong chi thong bao
                            SendSMSToParent(ACTION_TYPE_SEND_TO_PARENT_DISABLE, objSParent.FullName, objSParent, true);
                    }
                    else if (objSParent.IsCancel.HasValue && objSParent.IsCancel.Value) // Neu chi 1 HS thi xoa luon tai khoan
                    {
                        DeleteSParentLogonAccount(objSParent.FullName, objSParent.UserName, objSParent.UserName, false);
                    }
                    else if (objSParent.IsEnable.HasValue && objSParent.IsEnable.Value) // Neu huy tam ngung
                    {
                        UpdateSParentLogonAccount(objSParent.FullName, objSParent.UserName, false, false);
                    }
                    else
                    {
                        SendSMSToParent(ACTION_TYPE_SEND_TO_PARENT_DELETE, objSParent.FullName, objSParent, false);
                    }
                }
            }
        }

        /// <summary>
        /// xoa tai khoan thue bao cu
        /// </summary>
        /// <param name="pupilName">ten hoc sinh</param>
        /// <param name="oldUsername">thue bao cu</param>
        /// <param name="newUsername">thue bao moi</param>
        /// <param name="method"></param>
        private void DeleteSParentLogonAccount(string pupilName, string oldUsername, string newUsername, bool isLink)
        {
            SParentAccountBO sparentAccount = this.DeleteAccount(oldUsername);
            if (sparentAccount != null)
            {
                SendSMSToParent(ACTION_TYPE_SEND_TO_PARENT_DELETE, pupilName, sparentAccount, isLink);
            }
        }

        /// <summary>
        /// update tai khoan thue bao
        /// </summary>
        /// <param name="pupilName">ten hoc sinh</param>
        /// <param name="userName">thue bao</param>
        /// <param name="isLock">true: disbled user; false: enable user</param>
        private void UpdateSParentLogonAccount(string pupilName, string userName, bool isLock, bool isLink)
        {
            SParentAccountBO sparentAccount = this.UpdateAccount(userName, isLock);
            if (sparentAccount != null && !string.IsNullOrEmpty(sparentAccount.Password))
            {
                if (isLock)
                {
                    SendSMSToParent(ACTION_TYPE_SEND_TO_PARENT_DISABLE, pupilName, sparentAccount, isLink);
                }
                else
                {
                    SendSMSToParent(ACTION_TYPE_SEND_TO_PARENT_ENABLED, pupilName, sparentAccount, isLink);
                }
            }
           
        }

        public SParentAccountBO DeleteAccount(string userName)
        {
            
            SParentAccountBO response = new SParentAccountBO() { UserName = userName };
           
            if (!string.IsNullOrEmpty(userName))
            {
                APP_USERS user = this.context.Set<APP_USERS>().Where(o => o.USERNAME == userName).FirstOrDefault();
                if (user != null)
                {
                    user.IS_APPROVED = false;
                    this.context.Entry<APP_USERS>(user).State = EntityState.Modified;
                    this.context.SaveChanges();
                    response.Password = user.PASSWORD;
                }
            }

            return response;
            
        }

        public SParentAccountBO UpdateAccount(string userName, bool isLock)
        {
            SParentAccountBO response = new SParentAccountBO() { UserName = userName };
            if (!string.IsNullOrEmpty(userName))
            {
                APP_USERS user = this.context.Set<APP_USERS>().Where(o => o.USERNAME == userName).FirstOrDefault();
                if (user != null)
                {
                    if (isLock)
                    {
                        user.IS_APPROVED = false;
                    }
                    else
                    {
                        user.IS_LOCKED_OUT = false;
                        user.IS_APPROVED = true;
                    }

                    this.context.Entry<APP_USERS>(user).State = EntityState.Modified;
                    this.context.SaveChanges();
                    response.Password = user.PASSWORD;
                }
            }

            return response;
        }
        #endregion

        #region send SMS
        /// <summary>
        /// Tao tin nhan gui cho phhs khi dau noi hop dong
        /// </summary>
        /// <param name="type">1: them moi; 2: xoa tai khoan; 3: disabled; 4: gia han; 5: huy chan cat</param>
        /// <param name="islink">truong hop lien ket</param>
        /// <returns></returns>
        private void SendSMSToParent(int type, string pupilName, SParentAccountBO sparentAccount, bool islink)
        {
            string parentUrl = WebConfigurationManager.AppSettings["SParentURL"];
            string content = string.Empty;
            string msgPrice = string.Empty;
            if (ExtensionMethods.CheckViettelNumber(sparentAccount.UserName))
            {
                msgPrice = GlobalConstantsEdu.PRICE_190018098_VT;
            }
            else
            {
                msgPrice = GlobalConstantsEdu.PRICE_190018098_OTHER;
            }

            switch (type)
            {
                case ACTION_TYPE_SEND_TO_PARENT_ADD:
                    {
                        // them moi
                        if (islink)//them 1 account moi
                        {
                            content = string.Format(SMS_NOTICE_SPARENT_ACCOUNT_ADD, pupilName, parentUrl, sparentAccount.UserName, sparentAccount.Password);
                        }
                        else//update account
                        {
                            content = string.Format(SMS_NOTICE_SPARENT_ACCOUNT_ADD_NEW, pupilName, parentUrl, sparentAccount.UserName, sparentAccount.Password);
                        }
                        break;
                    }
                case ACTION_TYPE_SEND_TO_PARENT_DELETE:
                    {
                        if (islink)
                        {
                            // cat lien ket
                            content = string.Format(SMS_NOTICE_SPARENT_ACCOUNT_DISABLE, pupilName);
                        }
                        else
                        {
                            // xoa account
                            content = string.Format(SMS_NOTICE_SPARENT_ACCOUNT_DELETE, pupilName);
                        }
                        break;
                    }
                case ACTION_TYPE_SEND_TO_PARENT_DISABLE:
                    {
                        // chan cat
                        content = string.Format(SMS_NOTICE_SPARENT_ACCOUNT_DISABLE, pupilName);
                        break;
                    }
                case ACTION_TYPE_SEND_TO_PARENT_RENEWABLE:
                    {
                        // gia han
                        content = string.Format(SMS_NOTICE_PASSWORD_RENEW, pupilName, parentUrl, sparentAccount.UserName);
                        break;
                    }
                case ACTION_TYPE_SEND_TO_PARENT_ENABLED:
                    {
                        if (islink)
                        {
                            // lien ket hoc sinh moi
                            content = string.Format(SMS_NOTICE_SPARENT_ACCOUNT_ADD, pupilName, parentUrl, sparentAccount.UserName, sparentAccount.Password);
                        }
                        else
                        {
                            // huy chan cat
                            content = string.Format(SMS_NOTICE_SPARENT_ACCOUNT_ENABLED, pupilName, parentUrl, sparentAccount.UserName);
                        }
                        break;
                    }
                default:
                    break;
            }

            if (!string.IsNullOrEmpty(content))
            {
                Dictionary<string, object> dic = new Dictionary<string, object>();
                dic["Content"] = content;
                dic["Mobile"] = sparentAccount.UserName;
                dic["TypeID"] = GlobalConstantsEdu.COMMON_HISTORYSMS_TO_REGISTER_ACCOUNT_SPARENT;
                MTBusiness.SendSMS(dic);
            }
        }
        #endregion
    }
}
