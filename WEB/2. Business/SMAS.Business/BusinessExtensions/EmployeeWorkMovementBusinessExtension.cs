/**
 * The Viettel License
 *
 * Copyright 2012 Viettel Telecom. All rights reserved.
 * VIETTEL PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */

/** 
* @author  
* @version $Revision: $
*/

using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Linq.Expressions;
using System.Web;
using log4net;

using SMAS.DAL.Repository;
using SMAS.DAL.IRepository;
using SMAS.Business.IBusiness;
using SMAS.Models.Models;
using SMAS.Business.Common;

namespace SMAS.Business.Business
{

    public partial class EmployeeWorkMovementBusiness
    {
        #region Kiem tra du lieu dau vao
        private void Validate(EmployeeWorkMovement employeeWorkMovement, int type)
        {
            int employeeID = employeeWorkMovement.TeacherID;
            // Kiem tra cac thong tin chung nhu require, maxlength
            ValidationMetadata.ValidateObject(employeeWorkMovement);
            //TeacherID: PK(Employee)//Kiểm tra sự tồn tại của TeacherID truyền vào
            EmployeeBusiness.CheckAvailable(employeeID, "EmployeeWorkMovement_Label_TeacherID", true);
            //FromSchoolID: PK(SchoolProfile)

            SchoolProfileBusiness.CheckAvailable(employeeWorkMovement.FromSchoolID, "EmployeeWorkMovement_Label_FromSchoolID", true);
            //TeacherID(EmployeeID), FromSchoolID(SchoolID): not compatiable(Employee)



            //ToSchoolID: PK(SchoolProfile)
            //Trong trường hợp chuyển đến trường trong hệ thống. Phải kiểm tra sự tồn tại của trường chuyển đến – Kiểm tra SchoolID
            SchoolProfileBusiness.CheckAvailable(employeeWorkMovement.ToSchoolID, "EmployeeWorkMovement_Label_ToSchoolID", true);
            //ToSupervisingDeptID: PK(SupervisingDept)
            SupervisingDeptBusiness.CheckAvailable(employeeWorkMovement.ToSupervisingDeptID, "EmployeeWorkMovement_Label_ToSupervisingDeptID", true);
            //ToProvinceID: PK(Province)
            //Trong trường hợp chuyển đến trường trong hệ thống thì kiểm tra sự tồn tại của tỉnh thành  – Kiểm tra ToProvinceID
            ProvinceBusiness.CheckAvailable(employeeWorkMovement.ToProvinceID, "EmployeeWorkMovement_Label_ToProvinceID", true);
            //ToDistrictID: PK(District)
            //Trong trường hợp chuyển đến trường trong hệ thống thì kiểm tra sự tồn tại của quận huyện. – Kiểm tra ToDistrictID
            DistrictBusiness.CheckAvailable(employeeWorkMovement.ToDistrictID, "EmployeeWorkMovement_Label_ToDistrictID", true);

            //Trong trường hợp chuyển đến trường ngoài hệ thống/ cơ quan khác. Phải nhập nơi chuyển đến – Kiểm tra MoveTo
            if (GlobalConstants.MOVE_TYPE_OTHERDEPT == employeeWorkMovement.MovementType)
            {
                Utils.ValidateRequire(employeeWorkMovement.MoveTo, "EmployeeWorkMovement_Label_MoveTo");
            }
            //MoveDate <= DateTime.Now
            if (type == 2)
            {
                Utils.ValidatePastDate(employeeWorkMovement.MovedDate, "EmployeeRetire_Label_MovedDate");
                //MoveDate >  Employee(EmployeeID).StartingDate (nếu StartingDate != NULL)
                //Nếu đã nhập ngày vào nghề thì ngày chuyển công tác phải lớn hơn ngày vào nghề - Kiểm tra MoveDate, StartingDate (Employee)
                Employee Employee = EmployeeBusiness.Find(employeeID);
                if (Employee != null && Employee.StartingDate != null)
                {
                    Utils.ValidateBeforeDate(employeeWorkMovement.MovedDate, Employee.StartingDate, "EmployeeRetire_Label_MovedDate", "Employee_Label_StartingDate");
                }
                //MoveDate >  Employee(EmployeeID).JoinedDate (nếu JoinedDate != NULL)
                if (Employee.JoinedDate != null)
                {
                    Utils.ValidateBeforeDate(employeeWorkMovement.MovedDate, Employee.JoinedDate, "EmployeeRetire_Label_MovedDate", "Employee_Label_JoinedDate");
                }
            }
            else if (type == 3)
            {
                Utils.ValidatePastDate(employeeWorkMovement.MovedDate, "EmployeeSeverance_Label_MovedDate");
                //MoveDate >  Employee(EmployeeID).StartingDate (nếu StartingDate != NULL)
                //Nếu đã nhập ngày vào nghề thì ngày chuyển công tác phải lớn hơn ngày vào nghề - Kiểm tra MoveDate, StartingDate (Employee)
                Employee Employee = EmployeeBusiness.Find(employeeID);
                if (Employee != null && Employee.StartingDate != null)
                {
                    Utils.ValidateBeforeDate(employeeWorkMovement.MovedDate, Employee.StartingDate, "EmployeeSeverance_Label_MovedDate", "Employee_Label_StartingDate");
                }
                //MoveDate >  Employee(EmployeeID).JoinedDate (nếu JoinedDate != NULL)
                if (Employee.JoinedDate != null)
                {
                    Utils.ValidateBeforeDate(employeeWorkMovement.MovedDate, Employee.JoinedDate, "EmployeeSeverance_Label_MovedDate", "Employee_Label_JoinedDate");
                }
            }
            else
            {
                Utils.ValidatePastDate(employeeWorkMovement.MovedDate, "EmployeeWorkMovement_Label_MovedDate");
                //MoveDate >  Employee(EmployeeID).StartingDate (nếu StartingDate != NULL)
                //Nếu đã nhập ngày vào nghề thì ngày chuyển công tác phải lớn hơn ngày vào nghề - Kiểm tra MoveDate, StartingDate (Employee)
                Employee Employee = EmployeeBusiness.Find(employeeID);
                if (Employee != null && Employee.StartingDate != null)
                {
                    Utils.ValidateBeforeDate(employeeWorkMovement.MovedDate, Employee.StartingDate, "EmployeeWorkMovement_Label_MovedDate", "Employee_Label_StartingDate");
                }
                //MoveDate >  Employee(EmployeeID).JoinedDate (nếu JoinedDate != NULL)
                if (Employee.JoinedDate != null)
                {
                    Utils.ValidateBeforeDate(employeeWorkMovement.MovedDate, Employee.JoinedDate, "EmployeeWorkMovement_Label_MovedDate", "Employee_Label_JoinedDate");
                }
            }
            // Giao vien va truong
            bool EmployeeSchoolCompatible = this.repository.ExistsRow(GlobalConstants.SCHOOL_SCHEMA, "Employee",
                   new Dictionary<string, object>()
                {
                    {"EmployeeID",employeeWorkMovement.TeacherID},
                    {"SchoolID",employeeWorkMovement.FromSchoolID}
                }, null);
            if (!EmployeeSchoolCompatible)
            {
                throw new BusinessException("Common_Validate_NotCompatible");
            }
        }
        #endregion

        #region Search
        /// <summary>
        /// Tìm kiếm cán bộ chuyển công tác
        /// </summary>
        /// <author>hath</author>
        /// <date>4/9/2012</date>
        /// <param name="TeacherID">ID Cán bộ chuyển công tác</param>
        /// <param name="FromSchoolID">ID trường đang công tác</param>
        /// <param name="ResolutionDocument">Số văn bản / quyết định thay đổi công tác / nghỉ hưu</param>
        /// <param name="MovementType">Kiểu thay đổi công tác</param>
        /// <param name="ToSchoolID">ID trường đến công tác</param>
        /// <param name="ToSupervisingDeptID">Chuyển tới phòng/ban</param>
        /// <param name="ToProvinceID">Chuyển tới tỉnh thành phố</param>
        /// <param name="ToDistrictID">Chuyển tới quận huyện</param>
        /// <param name="MoveTo">Đơn vị mới chuyển đến (ngoài hệ thống)</param>
        /// <returns>Danh sách cán bộ chuyển công tác</returns>      
        public IQueryable<EmployeeWorkMovement> Search(IDictionary<string, object> dic)
        {

            int TeacherID = Utils.GetInt(dic, "TeacherID");
            int FromSchoolID = Utils.GetInt(dic, "FromSchoolID");
            string ResolutionDocument = Utils.GetString(dic, "ResolutionDocument");
            string MovementType = Utils.GetString(dic, "MovementType");
            int ToSchoolID = Utils.GetInt(dic, "ToSchoolID");
            int ToSupervisingDeptID = Utils.GetInt(dic, "ToSupervisingDeptID");
            int ToProvinceID = Utils.GetShort(dic, "ToProvinceID");
            int ToDistrictID = Utils.GetInt(dic, "ToDistrictID");
            string MoveTo = Utils.GetString(dic, "MoveTo");
            int AppliedLevel = Utils.GetInt(dic, "AppliedLevel");
            int AcademicYearID = Utils.GetInt(dic, "AcademicYearID");

            IQueryable<EmployeeWorkMovement> lsEmployeeWorkMovement = EmployeeWorkMovementRepository.All;
            if (TeacherID != 0)
            {
                lsEmployeeWorkMovement = lsEmployeeWorkMovement.Where(o => o.TeacherID == TeacherID);
            }
            if (FromSchoolID != 0)
            {
                lsEmployeeWorkMovement = lsEmployeeWorkMovement.Where(o => o.FromSchoolID == FromSchoolID);
            }
            if (ResolutionDocument.Trim().Length != 0)
            {
                lsEmployeeWorkMovement = lsEmployeeWorkMovement.Where(o => o.ResolutionDocument.Contains(ResolutionDocument.ToLower()));
            }
            if (MovementType != "")
            {
                string[] arrStrMovementType = MovementType.Split(',');
                int[] arrMovementType = new int[arrStrMovementType.Length];
                for (int i = 0; i < arrStrMovementType.Length; i++)
                {
                    arrMovementType[i] = int.Parse(arrStrMovementType[i]);
                }
                lsEmployeeWorkMovement = lsEmployeeWorkMovement.Where(o => arrMovementType.Contains(o.MovementType));
            }
            if (ToSchoolID != 0)
            {
                lsEmployeeWorkMovement = lsEmployeeWorkMovement.Where(o => o.ToSchoolID == ToSchoolID);
            }
            if (ToSupervisingDeptID != 0)
            {
                lsEmployeeWorkMovement = lsEmployeeWorkMovement.Where(o => o.ToSupervisingDeptID == ToSupervisingDeptID);
            }
            if (ToProvinceID != 0)
            {
                lsEmployeeWorkMovement = lsEmployeeWorkMovement.Where(o => o.ToProvinceID == ToProvinceID);
            }
            if (ToDistrictID != 0)
            {
                lsEmployeeWorkMovement = lsEmployeeWorkMovement.Where(o => o.ToDistrictID == ToDistrictID);
            }
            if (MoveTo.Trim().Length != 0)
            {
                lsEmployeeWorkMovement = lsEmployeeWorkMovement.Where(o => o.MoveTo.Contains(MoveTo.ToLower()));
            }
            if (AcademicYearID != 0)
            {
                AcademicYear AcademicYear = AcademicYearBusiness.Find(AcademicYearID);
                if (AcademicYear != null)
                {
                    lsEmployeeWorkMovement = lsEmployeeWorkMovement.Where(o => o.FromSchoolID == AcademicYear.SchoolID);
                    if (AcademicYear.FirstSemesterStartDate.HasValue)
                    {
                        lsEmployeeWorkMovement = lsEmployeeWorkMovement.Where(o => o.MovedDate >= AcademicYear.FirstSemesterStartDate);
                    }

                    if (AcademicYear.SecondSemesterEndDate.HasValue)
                    {
                        lsEmployeeWorkMovement = lsEmployeeWorkMovement.Where(o => o.MovedDate <= AcademicYear.SecondSemesterEndDate);
                    }
                }
            }


            return lsEmployeeWorkMovement;

        }

        #endregion

        #region InsertMovement


        /// <summary>
        /// Thêm cán bộ chuyển công tác
        /// </summary>
        /// <param name="employeeWorkMovement">Đối tượng EmployeeWorkMovement</param>
        /// <returns>
        /// Đối tượng EmployeeWorkMovement
        /// </returns>
        /// <date>
        /// 9/6/2012
        ///   </date>
        /// <author>
        /// hath
        /// </author>
        /// <remarks>
        /// 9/6/2012
        /// </remarks>
        public EmployeeWorkMovement InsertMovement(EmployeeWorkMovement employeeWorkMovement)
        {
            int EmployeeID = employeeWorkMovement.TeacherID;
            int type = 1;
            Validate(employeeWorkMovement, type);
            // Kiem tra trang thai lam viec
            Employee Employee = EmployeeBusiness.Find(EmployeeID);
            if (Employee.EmploymentStatus != SystemParamsInFile.EMPLOYMENT_STATUS_WORKING)
            {
                throw new BusinessException("HeadTeacherSubstitution_Label_WorkingStatusErrr");
            }
            int? FacultyID = Employee.SchoolFacultyID;
            employeeWorkMovement.FacultyID = FacultyID.Value;
            //Tìm kiếm bản ghi ua trong bảng UserAccount với EmployeeID = TeacherID, IsActive = TRUE\
            IQueryable<UserAccount> lsUserAccount = UserAccountBusiness.All.Where(o => o.EmployeeID == EmployeeID && o.IsActive == true);

            //Nếu có: 
            //-	Update lại ua: IsActive = FALSE, EmployeeID=NULL
            if (lsUserAccount.Count() != 0)
            {
                UserAccount ua = lsUserAccount.FirstOrDefault();
                ua.EmployeeID = null;
                ua.IsActive = false;

                UserAccountBusiness.Update(ua);

                //Gán OldAccountID = ua.UserAccountID rồi Insert dữ liệu vào bảng EmployeeWorkMovement
                employeeWorkMovement.OldAccountID = ua.UserAccountID;
            }
            //Tìm kiếm trong bảng EmployeeHistoryStatus bản ghi có EmployeeID = entity.TeacherID, SchoolID = entity.FromSchoolID lấy ra bản ghi có FromDate lớn nhất. 
            //Cập nhật lại bản ghi này với EmployeeStatus = EMPLOYMENT_STATUS_MOVED (2): Chuyển công tác, ToDate = MovedDate.
            IQueryable<EmployeeHistoryStatus> lsEmployeeHistoryStatus = EmployeeHistoryStatusBusiness.All.Where(o => o.EmployeeID == employeeWorkMovement.TeacherID && o.SchoolID == employeeWorkMovement.FromSchoolID).OrderBy(o => o.FromDate);
            if (lsEmployeeHistoryStatus.Count() != 0)
            {
                EmployeeHistoryStatus EmployeeHistoryStatus = lsEmployeeHistoryStatus.ToList().LastOrDefault();
                EmployeeHistoryStatus.EmployeeStatus = GlobalConstants.EMPLOYMENT_STATUS_MOVED;
                EmployeeHistoryStatus.ToDate = employeeWorkMovement.MovedDate;
                EmployeeHistoryStatusBusiness.Update(EmployeeHistoryStatus);
            }
            //Cập nhật lại EmployeeStatus = EMPLOYMENT_STATUS_MOVED cho bảng Employee
            Employee.EmploymentStatus = GlobalConstants.EMPLOYMENT_STATUS_MOVED;
            EmployeeBusiness.Update(Employee);
            //Update nghiệp vụ chuyển công tác
            //Lấy tất cả các bản ghi làm thay chủ nhiệm, làm thay kiêm nhiệm của cán bộ chuyển công tác
            int employeeID = employeeWorkMovement.TeacherID;
            int schoolID = employeeWorkMovement.FromSchoolID;
            IQueryable<ConcurrentWorkReplacement> lstConcurrentWorkReplacement = ConcurrentWorkReplacementBusiness.SearchBySchool(schoolID, new Dictionary<string, object> { { "ReplacedTeacherID", employeeID }, {"IsActive", true}});
            foreach(ConcurrentWorkReplacement p in lstConcurrentWorkReplacement)
            {
                if (p.FromDate >= employeeWorkMovement.MovedDate)
                {
                    ConcurrentWorkReplacementBusiness.Delete(p.ConcurrentWorkReplacementID);
                }
                else
                {
                    if (p.ToDate > employeeWorkMovement.MovedDate)
                    {
                        p.ToDate = employeeWorkMovement.MovedDate;
                        ConcurrentWorkReplacementBusiness.BaseUpdate(p);
                    }
                }
            }
            IQueryable<HeadTeacherSubstitution> lstHeadTeacherSubstitution = HeadTeacherSubstitutionBusiness.All.Where(o => o.SchoolID == schoolID && o.SubstituedHeadTeacher == employeeID);
            foreach (HeadTeacherSubstitution p in lstHeadTeacherSubstitution)
            {
                if (p.AssignedDate >= employeeWorkMovement.MovedDate)
                {
                    HeadTeacherSubstitutionBusiness.Delete(p.HeadTeacherSubstitutionID);
                }
                else
                {
                    if (p.EndDate > employeeWorkMovement.MovedDate)
                    {
                        p.EndDate = employeeWorkMovement.MovedDate;
                        HeadTeacherSubstitutionBusiness.BaseUpdate(p);
                    }
                }
            }

            return base.Insert(employeeWorkMovement);
        }
        #endregion

        #region Update


        /// <summary>
        /// Sửa cán bộ chuyển công tác
        /// </summary>
        /// <param name="employeeWorkMovement">Đối tượng EmployeeWorkMovement</param>
        /// <returns>
        /// Đối tượng EmployeeWorkMovement
        /// </returns>
        /// <author>
        /// hath
        /// </author>
        /// <date>
        /// 9/6/2012
        /// </date>
        public EmployeeWorkMovement UpdateMovement(EmployeeWorkMovement EmployeeWorkMovement)
        {
            int employeeID = EmployeeWorkMovement.TeacherID;
            int type = 1;
            Validate(EmployeeWorkMovement, type);
            //EmployeeWorkMovement(EmployeeWorkMovementID).MovementType (lấy trong CSDL)
            // chỉ được nhận giá trị: WORK_MOVEMENT_WITHIN_SYSTEM, WORK_MOVEMENT_OTHER_ORGANIZATION
            EmployeeWorkMovement ewm = EmployeeWorkMovementRepository.Find(EmployeeWorkMovement.EmployeeWorkMovementID);
            if (ewm.MovementType != GlobalConstants.WORK_MOVEMENT_WITHIN_SYSTEM && ewm.MovementType != GlobalConstants.WORK_MOVEMENT_OTHER_ORGANIZATION)
            {
                throw new BusinessException("Common_Validate_MovementType");
            }
            //EmployeeWorkMovement(EmployeeWorkMovementID).MovementType.IsAccepted
            //phải = FALSE -> Nếu không báo lỗi Đã được tiếp nhận ở đơn vị mới
            if (ewm.IsAccepted != false)
            {
                throw new BusinessException("Common_Validate_IsAccepted");
            }

            //Tìm kiếm trong bảng EmployeeHistoryStatus bản ghi có EmployeeID =Entity.TeacherID, SchoolID = Entity.FromSchoolID lấy ra bản ghi có FromDate lớn nhất. Cập nhật lại bản ghi này với EmployeeStatus = EMPLOYMENT_STATUS_MOVED (2): Chuyển công tác, ToDate = Entity.MovedDate.
            IQueryable<EmployeeHistoryStatus> lsEmployeeHistoryStatus = EmployeeHistoryStatusBusiness.All.Where(o => o.EmployeeID == EmployeeWorkMovement.TeacherID && o.SchoolID == EmployeeWorkMovement.FromSchoolID).OrderBy(o => o.FromDate);
            if (lsEmployeeHistoryStatus.Count() != 0)
            {
                EmployeeHistoryStatus EmployeeHistoryStatus = lsEmployeeHistoryStatus.ToList().LastOrDefault();
                EmployeeHistoryStatus.EmployeeStatus = GlobalConstants.EMPLOYMENT_STATUS_MOVED;
                EmployeeHistoryStatus.ToDate = EmployeeWorkMovement.MovedDate;
                EmployeeHistoryStatusBusiness.Update(EmployeeHistoryStatus);
            }
            //Nghiệp vụ update cập nhật thông tin trong bảng làm thay kiêm nhiệm, làm thay chủ nhiệm
            DateTime? movedDate = EmployeeWorkMovementBusiness.All.Where(o => o.EmployeeWorkMovementID == EmployeeWorkMovement.EmployeeWorkMovementID).Select(o => o.MovedDate).FirstOrDefault();
            if (EmployeeWorkMovement.MovedDate != movedDate)
            {
                int schoolID = EmployeeWorkMovement.FromSchoolID;
                IQueryable<ConcurrentWorkReplacement> lstConcurrentWorkReplacement = ConcurrentWorkReplacementBusiness.SearchBySchool(schoolID, new Dictionary<string, object> { { "ReplacedTeacherID", employeeID }, { "IsActive", true } });
                foreach (ConcurrentWorkReplacement p in lstConcurrentWorkReplacement)
                {
                    if (p.FromDate >= EmployeeWorkMovement.MovedDate)
                    {
                        ConcurrentWorkReplacementBusiness.Delete(p.ConcurrentWorkReplacementID);
                    }
                    else
                    {
                        if (p.ToDate > EmployeeWorkMovement.MovedDate)
                        {
                            p.ToDate = EmployeeWorkMovement.MovedDate;
                            ConcurrentWorkReplacementBusiness.BaseUpdate(p);
                        }
                    }
                }
                IQueryable<HeadTeacherSubstitution> lstHeadTeacherSubstitution = HeadTeacherSubstitutionBusiness.All.Where(o => o.SchoolID == schoolID && o.SubstituedHeadTeacher == employeeID);
                foreach (HeadTeacherSubstitution p in lstHeadTeacherSubstitution)
                {
                    if (p.AssignedDate >= EmployeeWorkMovement.MovedDate)
                    {
                        HeadTeacherSubstitutionBusiness.Delete(p.HeadTeacherSubstitutionID);
                    }
                    else
                    {
                        if (p.EndDate > EmployeeWorkMovement.MovedDate)
                        {
                            p.EndDate = EmployeeWorkMovement.MovedDate;
                            HeadTeacherSubstitutionBusiness.BaseUpdate(p);
                        }
                    }
                }

            }
            return base.Update(ewm);
        }
        #endregion

        #region DeleteMovement
        /// <summary>
        /// Xóa cán bộ chuyển công tác
        /// </summary>
        /// <param name="EmployeeWorkMovementID">ID cán bộ chuyển công tác</param>
        /// <author>
        /// hath
        /// </author>
        /// <remarks>
        /// 9/6/2012
        /// </remarks>
        public void DeleteMovement(int SchoolID, int EmployeeWorkMovementID)
        {
            //Bản ghi này đã bị xóa hoặc không tồn tại trong cơ sở dữ liệu. Kiểm tra EmployeeWorkMovementID != NULL
            Utils.ValidateNull(EmployeeWorkMovementID, "EmployeeWorkMovement_Label_EmployeeWorkMovementID");
            this.CheckAvailable(EmployeeWorkMovementID, "EmployeeWorkMovement_Label_EmployeeWorkMovementID", false);
            //EmployeeWorkMovement(EmployeeWorkMovementID).MovementType (lấy trong CSDL)
            // chỉ được nhận giá trị: WORK_MOVEMENT_WITHIN_SYSTEM, WORK_MOVEMENT_OTHER_ORGANIZATION
            EmployeeWorkMovement ewm = EmployeeWorkMovementRepository.Find(EmployeeWorkMovementID);
            if (ewm.MovementType != GlobalConstants.WORK_MOVEMENT_WITHIN_SYSTEM && ewm.MovementType != GlobalConstants.WORK_MOVEMENT_OTHER_ORGANIZATION)
            {
                List<object> Params = new List<object>();
                Params.Add("EmployeePraiseDiscipline_Label_MovementType");
                throw new BusinessException("Common_Validate_MovementType", Params);

            }
            //EmployeeWorkMovement(EmployeeWorkMovementID).MovementType.IsAccepted
            //phải = FALSE -> Nếu không báo lỗi Đã được tiếp nhận ở đơn vị mới
            if (ewm.IsAccepted != false)
            {

                throw new BusinessException("Common_Validate_IsAccepted");
            }

            //Lấy bản ghi EmployeeWorkMovement ewm tương ứng
            //Từ ewm.OldAccountID => UserAccount ua (nếu có), update lại:
            //ua.IsActive = True
            //ua.EmployeeID = ewm.TeacherID
            UserAccount ua = UserAccountBusiness.Find(ewm.OldAccountID);
            if (ua != null)
            {
                ua.IsActive = true;
                ua.EmployeeID = ewm.TeacherID;
                UserAccountBusiness.Update(ua);
            }

            //Tìm kiếm trong bảng EmployeeHistoryStatus bản ghi có EmployeeID = entity.TeacherID, SchoolID = entity.FromSchoolID lấy ra bản ghi có FromDate lớn nhất.
            //Cập nhật lại bản ghi này với EmployeeStatus = EMPLOYMENT_STATUS_WORKING (1): Đang làm việc, ToDate = null;
            //Cập nhật lại EmployeeStatus = EMPLOYMENT_STATUS_WORKING cho bảng Employee
            IQueryable<EmployeeHistoryStatus> lsEmployeeHistoryStatus = EmployeeHistoryStatusBusiness.All.Where(o => o.EmployeeID == ewm.TeacherID && o.SchoolID == ewm.FromSchoolID).OrderBy(o => o.FromDate);
            if (lsEmployeeHistoryStatus.Count() != 0)
            {
                EmployeeHistoryStatus EmployeeHistoryStatus = lsEmployeeHistoryStatus.ToList().LastOrDefault();
                EmployeeHistoryStatus.EmployeeStatus = GlobalConstants.EMPLOYMENT_STATUS_WORKING;
                EmployeeHistoryStatus.ToDate = null;
                EmployeeHistoryStatusBusiness.Update(EmployeeHistoryStatus);
            }

            // Cap nhat lai trang thai cho can bo la dang lam viec
            Employee Employee = EmployeeBusiness.Find(ewm.TeacherID);
            Employee.EmploymentStatus = SystemParamsInFile.EMPLOYMENT_STATUS_WORKING;
            EmployeeBusiness.Update(Employee);

            //Thực hiện xóa dữ liệu trong bảng EmployeeWorkMovement

            base.Delete(EmployeeWorkMovementID);
        }
        #endregion

        #region SearchBySchool


        /// <summary>
        /// Tìm kiếm cán bộ chuyển công tác theo trường
        /// </summary>
        /// <param name="SchoolID">ID trường</param>
        /// <returns>
        /// Danh sách cán bộ chuyển công tác
        /// </returns>
        /// <author>
        /// hath
        /// </author>
        /// <remarks>
        /// 9/6/2012
        /// </remarks>
        public IQueryable<EmployeeWorkMovement> SearchBySchool(IDictionary<string, object> dic, int SchoolID)
        {

            if (SchoolID == 0)
            {
                return null;
            }
            else
            {
                dic["ToSchoolID"] = SchoolID;
                return this.Search(dic);
            }

        }
        #endregion

        #region SearchRetire

        public IQueryable<EmployeeWorkMovement> SearchRetire(IDictionary<string, object> SearchInfo, int SchoolID)
        {
            if (SchoolID == 0)
            {
                return null;
            }
            else
            {
                SearchInfo["FromSchoolID"] = SchoolID;
                SearchInfo["MovementType"] = SystemParamsInFile.WORK_MOVEMENT_RETIREMENT.ToString() + "," + SystemParamsInFile.WORK_MOVEMENT_OTHER_ORGANIZATION.ToString();
                return this.Search(SearchInfo);
            }
        }
        #endregion

        #region DeleteRetire

        /// <summary>
        /// Xóa cán bộ nghỉ hưu
        /// </summary>
        /// <param name="EmployeeWorkMovementID">ID cán bộ nghỉ hưu</param>
        /// <param name="SchoolID">ID trường.</param>
        /// <author>
        /// hath
        /// </author>
        /// <remarks>
        /// 9/6/2012
        /// </remarks>
        public void DeleteRetire(int EmployeeWorkMovementID, int SchoolID)
        {
            EmployeeWorkMovement employeeWorkMovement = EmployeeWorkMovementRepository.Find(EmployeeWorkMovementID);
            //EmployeeWorkMovementID: PK(EmployeeWorkMovement)
            this.CheckAvailable(EmployeeWorkMovementID, "EmployeeWorkMovement_Label_EmployeeWorkMovementID", false);
            //EmployeeID, SchoolID: not compatible(Employee)
            bool EmployeeSchoolCompatible = this.repository.ExistsRow(GlobalConstants.SCHOOL_SCHEMA, "Employee",
                  new Dictionary<string, object>()
                {
                    {"EmployeeID",employeeWorkMovement.TeacherID},
                    {"SchoolID",employeeWorkMovement.FromSchoolID}
                }, null);
            if (!EmployeeSchoolCompatible)
            {
                throw new BusinessException("Common_Validate_NotCompatible");
            }
            //SchoolID(FromSchoolID), EmployeeWorkMovementID: not compatible(EmployeeWorkMovement)
            bool EmployeeWorkMovementCompatible = this.repository.ExistsRow(GlobalConstants.SCHOOL_SCHEMA, "EmployeeWorkMovement",
                  new Dictionary<string, object>()
                {
                    {"EmployeeWorkMovementID",employeeWorkMovement.EmployeeWorkMovementID},
                    {"FromSchoolID",employeeWorkMovement.FromSchoolID}
                }, null);
            if (!EmployeeSchoolCompatible)
            {
                throw new BusinessException("Common_Validate_NotCompatible");
            }
            //EmployeeWorkMovement(EmployeeWorkMovementID).MovementType chỉ được nhận giá trị: WORK_MOVEMENT_RETIREMENT
            if (employeeWorkMovement.MovementType != SystemParamsInFile.WORK_MOVEMENT_RETIREMENT)
            {
                throw new BusinessException("EmployeeWorkMovement_Validate_NotRetire");
            }

            IQueryable<EmployeeHistoryStatus> lsEmployeeHistoryStatus = EmployeeHistoryStatusBusiness.All.Where(o => o.EmployeeID == employeeWorkMovement.TeacherID && o.SchoolID == employeeWorkMovement.FromSchoolID).OrderBy(o => o.FromDate);
            if (lsEmployeeHistoryStatus.Count() != 0)
            {
                EmployeeHistoryStatus EmployeeHistoryStatus = lsEmployeeHistoryStatus.ToList().LastOrDefault();
                EmployeeHistoryStatus.EmployeeStatus = GlobalConstants.EMPLOYMENT_STATUS_WORKING;
                EmployeeHistoryStatus.ToDate = null;
                EmployeeHistoryStatusBusiness.Update(EmployeeHistoryStatus);
            }

            Employee Employee = EmployeeBusiness.Find(employeeWorkMovement.TeacherID);
            // Cap nhat lai trang thai cho can bo la dang lam viec
            Employee.EmploymentStatus = SystemParamsInFile.EMPLOYMENT_STATUS_WORKING;
            EmployeeBusiness.Update(Employee); 

            //Cập nhật lại trang thái trong UserAccount
            int teacherID = employeeWorkMovement.TeacherID;
            UserAccount ua = UserAccountBusiness.All.Where(o => o.UserAccountID == employeeWorkMovement.OldAccountID).SingleOrDefault();
            if (ua != null)
            {
                ua.IsActive = true;
                ua.EmployeeID = teacherID;
                UserAccountBusiness.Update(ua);
                aspnet_Membership member = aspnet_MembershipBusiness.All.Where(o => o.UserId == ua.GUID).SingleOrDefault();
                member.IsApproved = 1;
                aspnet_MembershipBusiness.Update(member);
            }
            base.Delete(EmployeeWorkMovementID);
        }

        #endregion

        #region UpdateRetire

        /// <summary>
        /// Sửa cán bộ nghỉ hưu
        /// </summary>
        /// <param name="EmployeeWorkMovement">Đối tượng EmployeeWorkMovement</param>
        /// <param name="SchoolID">ID trường.</param>
        /// <returns>
        /// Đối tượng EmployeeWorkMovement
        /// </returns>
        /// <author>hath</author>
        /// <date> 9/6/2012</date>
        public EmployeeWorkMovement UpdateRetire(EmployeeWorkMovement EmployeeWorkMovement, int SchoolID)
        {
            int type = 2;
            int employeeID = EmployeeWorkMovement.TeacherID;
            Validate(EmployeeWorkMovement, type);
            EmployeeWorkMovement ewm = EmployeeWorkMovementRepository.Find(EmployeeWorkMovement.EmployeeWorkMovementID);
            //Chưa chọn cán bộ nghỉ hưu hoặc cán bộ không tồn tại – Kiểm tra TeacherID
            EmployeeBusiness.CheckAvailable(EmployeeWorkMovement.TeacherID, "EmployeeWorkMovement_Label_TeacherID", false);
            //Quyết định nghỉ hưu phải nhỏ hơn 200 ký tự - Kiểm tra ResolutionDocument
            //Utils.ValidateMaxLength(EmployeeWorkMovement.ResolutionDocument, RESOLUTION_MAX_LENGTH, "EmployeeWorkMovement_Label_ResolutionDocument");
            //Ghi chú không được nhập quá 500 ký tự - Kiểm tra Description.
            //Utils.ValidateMaxLength(EmployeeWorkMovement.Description, DESCRIPTION_MAX_LENGTH, "EmployeeWorkMovement_Label_Description");
            //Ngày nghỉ hưu không được để trống – Kiểm tra MoveDate
            Utils.ValidateRequire(EmployeeWorkMovement.MovedDate, "EmployeeWorkMovement_Label_MovedDate");
            //Ngày nghỉ hưu phải <= ngày hiện tại. Kiểm tra MoveDate
            Utils.ValidatePastDate(EmployeeWorkMovement.MovedDate, "EmployeeWorkMovement_Label_MovedDate");
            //Ngày nghỉ hưu phải lớn hơn ngày vào biên chế - Kiểm tra MoveDate, JoinedDate
            if (EmployeeWorkMovement.Employee.JoinedDate != null)
            {
                Utils.ValidateBeforeDate(EmployeeWorkMovement.MovedDate, EmployeeWorkMovement.Employee.JoinedDate, "EmployeeWorkMovement_Label_MovedDate", "Employee_Label_JoinedDate");
            }
            //Kiểm tra  sự tồn tại của EmployeeWorkMovementID
            this.CheckAvailable(EmployeeWorkMovement.EmployeeWorkMovementID, "EmployeeWorkMovement_Label_EmployeeWorkMovementID");
            //Cán bộ nghỉ hưu này không thuộc trường truyền vào
            
            //this.CheckNotDuplicateCouple(EmployeeWorkMovement.EmployeeWorkMovementID.ToString(), SchoolID.ToString(), GlobalConstants.SCHOOL_SCHEMA, "EmployeeWorkMovement", "EmployeeWorkMovementID",
            //    "FromSchoolID", false, EmployeeWorkMovement.EmployeeWorkMovementID, "EmployeeWorkMovement_Label_EmployeeWorkMovementID");
            
            //EmployeeID, FromSchoolID(SchoolID): not compatible(Employee)
            {
                bool Exist = repository.ExistsRow(GlobalConstants.SCHOOL_SCHEMA, "Employee",
                        new Dictionary<string, object>()
                {
                    {"EmployeeID",EmployeeWorkMovement.TeacherID}
                    ,{"SchoolID",EmployeeWorkMovement.FromSchoolID}
                }, null);
                if (!Exist)
                {
                    throw new BusinessException("Common_Validate_NotAvailable");
                }
            }

            //Validate(EmployeeWorkMovement);

            //-	Description
            ewm.Description = EmployeeWorkMovement.Description;
            //-	ResolutionDocument
            ewm.ResolutionDocument = EmployeeWorkMovement.ResolutionDocument;
            //-	MovedDate
            ewm.MovedDate = EmployeeWorkMovement.MovedDate;

            //Tìm kiếm trong bảng EmployeeHistoryStatus bản ghi có EmployeeID =Entity.TeacherID, SchoolID = Entity.FromSchoolID lấy ra bản ghi có FromDate lớn nhất. Cập nhật lại bản ghi này với EmployeeStatus = EMPLOYMENT_STATUS_RETIREMENT, ToDate = Entity.MovedDate.
            IQueryable<EmployeeHistoryStatus> lsEmployeeHistoryStatus = EmployeeHistoryStatusBusiness.All.Where(o => o.EmployeeID == ewm.TeacherID && o.SchoolID == ewm.FromSchoolID).OrderBy(o => o.FromDate);
            if (lsEmployeeHistoryStatus.Count() != 0)
            {
                EmployeeHistoryStatus EmployeeHistoryStatus = lsEmployeeHistoryStatus.ToList().LastOrDefault();
                EmployeeHistoryStatus.EmployeeStatus = GlobalConstants.EMPLOYMENT_STATUS_RETIRED;
                EmployeeHistoryStatus.ToDate = EmployeeWorkMovement.MovedDate;
                EmployeeHistoryStatusBusiness.Update(EmployeeHistoryStatus);
            }
            DateTime? movedDate = EmployeeWorkMovementBusiness.All.Where(o => o.EmployeeWorkMovementID == EmployeeWorkMovement.EmployeeWorkMovementID).Select(o => o.MovedDate).FirstOrDefault();
            if (EmployeeWorkMovement.MovedDate != movedDate)
            {
                int schoolID = EmployeeWorkMovement.FromSchoolID;
                IQueryable<ConcurrentWorkReplacement> lstConcurrentWorkReplacement = ConcurrentWorkReplacementBusiness.SearchBySchool(schoolID, new Dictionary<string, object> { { "ReplacedTeacherID", employeeID }, { "IsActive", true } });
                foreach (ConcurrentWorkReplacement p in lstConcurrentWorkReplacement)
                {
                    if (p.FromDate >= EmployeeWorkMovement.MovedDate)
                    {
                        ConcurrentWorkReplacementBusiness.Delete(p.ConcurrentWorkReplacementID);
                    }
                    else
                    {
                        if (p.ToDate > EmployeeWorkMovement.MovedDate)
                        {
                            p.ToDate = EmployeeWorkMovement.MovedDate;
                            ConcurrentWorkReplacementBusiness.BaseUpdate(p);
                        }
                    }
                }
                IQueryable<HeadTeacherSubstitution> lstHeadTeacherSubstitution = HeadTeacherSubstitutionBusiness.All.Where(o => o.SchoolID == schoolID && o.SubstituedHeadTeacher == employeeID);
                foreach (HeadTeacherSubstitution p in lstHeadTeacherSubstitution)
                {
                    if (p.AssignedDate >= EmployeeWorkMovement.MovedDate)
                    {
                        HeadTeacherSubstitutionBusiness.Delete(p.HeadTeacherSubstitutionID);
                    }
                    else
                    {
                        if (p.EndDate > EmployeeWorkMovement.MovedDate)
                        {
                            p.EndDate = EmployeeWorkMovement.MovedDate;
                            HeadTeacherSubstitutionBusiness.BaseUpdate(p);
                        }
                    }
                }

            }
            return base.Update(ewm);
        }
        #endregion

        #region InsertRetire
        /// <summary>
        /// Thêm cán bộ nghỉ hưu
        /// </summary>
        /// <param name="EmployeeWorkMovement">Đối tượng EmployeeWorkMovement</param>
        /// <param name="SchoolID">ID trường.</param>
        /// <returns>
        /// Đối tượng EmployeeWorkMovement
        /// </returns>
        /// <author>hath</author>
        /// <date> 9/6/2012</date>
        public EmployeeWorkMovement InsertRetire(EmployeeWorkMovement EmployeeWorkMovement)
        {
            // Kiem tra trang thai lam viec
            int type = 2;
            Validate(EmployeeWorkMovement, type);
            Employee Employee = EmployeeBusiness.Find(EmployeeWorkMovement.TeacherID);
            if (Employee.EmploymentStatus != SystemParamsInFile.EMPLOYMENT_STATUS_WORKING)
            {
                throw new BusinessException("HeadTeacherSubstitution_Label_WorkingStatusErrr");
            }
            //Tạo đối tượng EmployeeWorkMovement mới lấy các thông tin từ Entity:
            EmployeeWorkMovement ewm = new EmployeeWorkMovement();
            //Tìm kiếm bản ghi ua trong bảng UserAccount với EmployeeID = TeacherID, IsActive = TRUE

            int EmployeeID = EmployeeWorkMovement.TeacherID;
            IQueryable<UserAccount> lsUserAccount = UserAccountBusiness.All.Where(o => o.EmployeeID.Value == EmployeeID && o.IsActive == true);
            if (lsUserAccount.Count() != 0)
            {

                //Nếu có:
                UserAccount ua = lsUserAccount.ToList().FirstOrDefault();
                //-	Update lại ua: IsActive = FALSE, EmployeeID=NULL
                ua.IsActive = true;
                ua.EmployeeID = null;
                UserAccountBusiness.Update(ua);
                aspnet_Membership member = aspnet_MembershipBusiness.All.Where(o=>o.UserId==ua.GUID).FirstOrDefault();
                member.IsApproved = 0;
                aspnet_MembershipBusiness.Update(member);
                ewm.OldAccountID = ua.UserAccountID;
            }

            //-	TeacherID
            ewm.TeacherID = EmployeeWorkMovement.TeacherID;
            //-	FromSchoolID
            ewm.FromSchoolID = EmployeeWorkMovement.FromSchoolID;
            //-	Description
            ewm.Description = EmployeeWorkMovement.Description;
            //-	ResolutionDocument
            ewm.ResolutionDocument = EmployeeWorkMovement.ResolutionDocument;
            //-	MovedDate
            ewm.MovedDate = EmployeeWorkMovement.MovedDate;
            //-	FacultyID lấy theo TeacherID
            ewm.FacultyID = Employee.SchoolFacultyID.Value;
            //Gán thông tin:
            //-	MovementType = WORK_MOVEMENT_RETIREMENT
            ewm.MovementType = GlobalConstants.WORK_MOVEMENT_RETIREMENT;

            //Tìm kiếm trong bảng EmployeeHistoryStatus bản ghi có EmployeeID = entity.TeacherID, SchoolID = entity.FromSchoolID lấy ra bản ghi có FromDate lớn nhất. Cập nhật lại bản ghi này với EmployeeStatus = EMPLOYMENT_STATUS_RETIRED, ToDate = MovedDate.

            //Cập nhật lại EmployeeStatus = EMPLOYMENT_STATUS_ RETIRED cho bảng Employee
            IQueryable<EmployeeHistoryStatus> lsEmployeeHistoryStatus = EmployeeHistoryStatusBusiness.All.Where(o => o.EmployeeID == ewm.TeacherID && o.SchoolID == ewm.FromSchoolID).OrderBy(o => o.FromDate);
            if (lsEmployeeHistoryStatus.Count() != 0)
            {
                EmployeeHistoryStatus EmployeeHistoryStatus = lsEmployeeHistoryStatus.ToList().LastOrDefault();
                EmployeeHistoryStatus.EmployeeStatus = GlobalConstants.EMPLOYMENT_STATUS_RETIRED;
                EmployeeHistoryStatus.ToDate = EmployeeWorkMovement.MovedDate;
                EmployeeHistoryStatusBusiness.Update(EmployeeHistoryStatus);

            }
            // Cap nhat lai trang thai cho can bo la nghi huu
            Employee.EmploymentStatus = SystemParamsInFile.EMPLOYMENT_STATUS_RETIRED;
            EmployeeBusiness.Update(Employee);
            int employeeID = EmployeeWorkMovement.TeacherID;
            int schoolID = EmployeeWorkMovement.FromSchoolID;
            IQueryable<ConcurrentWorkReplacement> lstConcurrentWorkReplacement = ConcurrentWorkReplacementBusiness.SearchBySchool(schoolID, new Dictionary<string, object> { { "ReplacedTeacherID", employeeID }, { "IsActive", true } });
            foreach (ConcurrentWorkReplacement p in lstConcurrentWorkReplacement)
            {
                if (p.FromDate >= EmployeeWorkMovement.MovedDate)
                {
                    ConcurrentWorkReplacementBusiness.Delete(p.ConcurrentWorkReplacementID);
                }
                else
                {
                    if (p.ToDate > EmployeeWorkMovement.MovedDate)
                    {
                        p.ToDate = EmployeeWorkMovement.MovedDate;
                        ConcurrentWorkReplacementBusiness.BaseUpdate(p);
                    }
                }
            }
            IQueryable<HeadTeacherSubstitution> lstHeadTeacherSubstitution = HeadTeacherSubstitutionBusiness.All.Where(o => o.SchoolID == schoolID && o.SubstituedHeadTeacher == employeeID);
            foreach (HeadTeacherSubstitution p in lstHeadTeacherSubstitution)
            {
                if (p.AssignedDate >= EmployeeWorkMovement.MovedDate)
                {
                    HeadTeacherSubstitutionBusiness.Delete(p.HeadTeacherSubstitutionID);
                }
                else
                {
                    if (p.EndDate > EmployeeWorkMovement.MovedDate)
                    {
                        p.EndDate = EmployeeWorkMovement.MovedDate;
                        HeadTeacherSubstitutionBusiness.BaseUpdate(p);
                    }
                }
            }
            return base.Insert(ewm);
        }

        #endregion

        #region SearchMovement
        public IQueryable<EmployeeWorkMovement> SearchMovement(IDictionary<string, object> SearchInfo, int SchoolID)
        {

            if (SchoolID == 0)
            {
                return null;
            }
            else
            {
                SearchInfo["FromSchoolID"] = SchoolID;
                SearchInfo["MovementType"] = SystemParamsInFile.WORK_MOVEMENT_WITHIN_SYSTEM.ToString() + "," + SystemParamsInFile.WORK_MOVEMENT_OTHER_ORGANIZATION;
                return this.Search(SearchInfo);
            }
        }
        #endregion

        #region InsertMovementToSchoolWithinSystem
        public EmployeeWorkMovement InsertMovementToSchoolWithinSystem(EmployeeWorkMovement employeeWorkMovement)
        {
            int type = 3;
            Validate(employeeWorkMovement, type);
            //ToSchoolID: require
            Utils.ValidateRequire(employeeWorkMovement.ToSchoolID, "EmployeeWorkMovement_Label_ToSchoolID");

            //ToProvinceID: require
            Utils.ValidateRequire(employeeWorkMovement.ToProvinceID, "EmployeeWorkMovement_Label_ToProvinceID");

            //ToDistrictID: require
            Utils.ValidateRequire(employeeWorkMovement.ToDistrictID, "EmployeeWorkMovement_Label_ToDistrictID");

            //ToProvinceID (ProvinceID), ToDistrictID (DistrictID): not compatiable(District)
            bool EmployeeDistrictCompatible = this.repository.ExistsRow(GlobalConstants.LIST_SCHEMA, "District",
                   new Dictionary<string, object>()
                {
                    {"ProvinceID",employeeWorkMovement.ToProvinceID},
                    {"DistrictID",employeeWorkMovement.ToDistrictID}
                }, null);
            if (!EmployeeDistrictCompatible)
            {
                throw new BusinessException("Common_Validate_NotCompatible");
            }
            //ToSchoolID (SchoolProfileID), ToDistrictID (DistrictID): not compatiable(SchoolProfile)
            bool EmployeeSchoolCompatible = this.repository.ExistsRow(GlobalConstants.SCHOOL_SCHEMA, "SchoolProfile",
                   new Dictionary<string, object>()
                {
                    {"SchoolProfileID",employeeWorkMovement.ToSchoolID},
                    {"DistrictID",employeeWorkMovement.ToDistrictID}
                }, null);
            if (!EmployeeSchoolCompatible)
            {
                throw new BusinessException("Common_Validate_NotCompatible");
            }

            //MovementType = WORK_MOVEMENT_WITHIN_SYSTEM
            employeeWorkMovement.MovementType = GlobalConstants.WORK_MOVEMENT_WITHIN_SYSTEM;
            //IsAccepted = FALSE
            employeeWorkMovement.IsAccepted = false;
            //OldAccountID = ua.UserAccountID


            return this.InsertMovement(employeeWorkMovement);
        }

        #endregion

        #region InsertMovementToOtherOrganization
        public EmployeeWorkMovement InsertMovementToOtherOrganization(EmployeeWorkMovement employeeWorkMovement)
        {
            //MoveTo: require
            Utils.ValidateRequire(employeeWorkMovement.MoveTo, "EmployeeWorkMovement_Label_MoveTo");
            //-	MovementType = WORK_MOVEMENT_OTHER_ORGANIZATION
            employeeWorkMovement.MovementType = GlobalConstants.WORK_MOVEMENT_OTHER_ORGANIZATION;
            //-	IsAccepted = FALSE
            employeeWorkMovement.IsAccepted = false;
            //-	OldAccountID = ua.UserAccountID


            return this.InsertMovement(employeeWorkMovement);
        }

        #endregion

        #region UpdateMovementToSchoolWithinSystem
        public EmployeeWorkMovement UpdateMovementToSchoolWithinSystem(EmployeeWorkMovement employeeWorkMovement)
        {
            //ToSchoolID: require
            Utils.ValidateRequire(employeeWorkMovement.ToSchoolID, "EmployeeWorkMovement_Label_ToSchoolID");

            //ToProvinceID: require
            Utils.ValidateRequire(employeeWorkMovement.ToProvinceID, "EmployeeWorkMovement_Label_ToProvinceID");

            //ToDistrictID: require
            Utils.ValidateRequire(employeeWorkMovement.ToDistrictID, "EmployeeWorkMovement_Label_ToDistrictID");

            //ToProvinceID (ProvinceID), ToDistrictID (DistrictID): not compatiable(District)
            bool EmployeeDistrictCompatible = this.repository.ExistsRow(GlobalConstants.LIST_SCHEMA, "District",
                   new Dictionary<string, object>()
                {
                    {"ProvinceID",employeeWorkMovement.ToProvinceID},
                    {"DistrictID",employeeWorkMovement.ToDistrictID}
                }, null);
            if (!EmployeeDistrictCompatible)
            {
                throw new BusinessException("Common_Validate_NotCompatible");
            }
            //ToSchoolID (SchoolProfileID), ToDistrictID (DistrictID): not compatiable(SchoolProfile)
            bool EmployeeSchoolCompatible = this.repository.ExistsRow(GlobalConstants.SCHOOL_SCHEMA, "SchoolProfile",
                   new Dictionary<string, object>()
                {
                    {"SchoolProfileID",employeeWorkMovement.ToSchoolID},
                    {"DistrictID",employeeWorkMovement.ToDistrictID}
                }, null);
            if (!EmployeeSchoolCompatible)
            {
                throw new BusinessException("Common_Validate_NotCompatible");
            }

            //-	MovementType = WORK_MOVEMENT_WITHIN_SYSTEM
            employeeWorkMovement.MovementType = GlobalConstants.WORK_MOVEMENT_WITHIN_SYSTEM;
            //-	MoveTo = “”
            employeeWorkMovement.MoveTo = "";


            return UpdateMovement(employeeWorkMovement);
        }

        #endregion

        #region UpdateMovementToOtherOrganization
        public EmployeeWorkMovement UpdateMovementToOtherOrganization(EmployeeWorkMovement EmployeeWorkMovement)
        {

            //MoveTo: require
            Utils.ValidateRequire(EmployeeWorkMovement.MoveTo, "EmployeeWorkMovement_Label_MoveTo");
            //-	MovementType = WORK_MOVEMENT_OTHER_ORGANIZATION
            EmployeeWorkMovement.MovementType = GlobalConstants.WORK_MOVEMENT_OTHER_ORGANIZATION;
            //-	ToSchoolID = NULL
            EmployeeWorkMovement.ToSchoolID = null;
            //-	ToProvinceID = NULL
            EmployeeWorkMovement.ToProvinceID = null;
            //-	ToDistrictID = NULL
            EmployeeWorkMovement.ToDistrictID = null;

            return UpdateMovement(EmployeeWorkMovement);
        }

        #endregion

        #region Nghỉ việc

        public IQueryable<EmployeeWorkMovement> SearchSeverance(IDictionary<string, object> SearchInfo, int SchoolID)
        {
            if (SchoolID == 0)
            {
                return null;
            }
            else
            {
                SearchInfo["FromSchoolID"] = SchoolID;
                SearchInfo["MovementType"] = SystemParamsInFile.WORK_MOVEMENT_SEVERANCEMENT.ToString() + "," + SystemParamsInFile.WORK_MOVEMENT_OTHER_ORGANIZATION.ToString();
                return this.Search(SearchInfo);
            }
        }
        public EmployeeWorkMovement InsertSeverance(EmployeeWorkMovement EmployeeWorkMovement)
        {
            // Kiem tra trang thai lam viec
            int type = 3;
            Validate(EmployeeWorkMovement, type);
            Employee Employee = EmployeeBusiness.Find(EmployeeWorkMovement.TeacherID);
            if (Employee.EmploymentStatus != SystemParamsInFile.EMPLOYMENT_STATUS_WORKING)
            {
                throw new BusinessException("HeadTeacherSubstitution_Label_WorkingStatusErrr");
            }
            //Tạo đối tượng EmployeeWorkMovement mới lấy các thông tin từ Entity:
            EmployeeWorkMovement ewm = new EmployeeWorkMovement();
            //Tìm kiếm bản ghi ua trong bảng UserAccount với EmployeeID = TeacherID, IsActive = TRUE

            int EmployeeID = EmployeeWorkMovement.TeacherID;
            IQueryable<UserAccount> lsUserAccount = UserAccountBusiness.All.Where(o => o.EmployeeID.Value == EmployeeID && o.IsActive == true);
            if (lsUserAccount.Count() != 0)
            {

                //Nếu có:
                UserAccount ua = lsUserAccount.ToList().FirstOrDefault();
                //-	Update lại ua: IsActive = FALSE, EmployeeID=NULL
                ua.IsActive = true;
                ua.EmployeeID = null;
                UserAccountBusiness.Update(ua);
                aspnet_Membership member = aspnet_MembershipBusiness.All.Where(o => o.UserId == ua.GUID).FirstOrDefault();
                member.IsApproved = 0;
                aspnet_MembershipBusiness.Update(member);
                ewm.OldAccountID = ua.UserAccountID;
            }

            //-	TeacherID
            ewm.TeacherID = EmployeeWorkMovement.TeacherID;
            //-	FromSchoolID
            ewm.FromSchoolID = EmployeeWorkMovement.FromSchoolID;
            //-	Description
            ewm.Description = EmployeeWorkMovement.Description;
            //-	ResolutionDocument
            ewm.ResolutionDocument = EmployeeWorkMovement.ResolutionDocument;
            //-	MovedDate
            ewm.MovedDate = EmployeeWorkMovement.MovedDate;
            //-	FacultyID lấy theo TeacherID
            ewm.FacultyID = Employee.SchoolFacultyID.Value;
            //Gán thông tin:
            //-	MovementType = WORK_MOVEMENT_SeveranceMENT
            ewm.MovementType = GlobalConstants.WORK_MOVEMENT_SEVERANCE;

            //Tìm kiếm trong bảng EmployeeHistoryStatus bản ghi có EmployeeID = entity.TeacherID, SchoolID = entity.FromSchoolID lấy ra bản ghi có FromDate lớn nhất. Cập nhật lại bản ghi này với EmployeeStatus = EMPLOYMENT_STATUS_SeveranceD, ToDate = MovedDate.

            //Cập nhật lại EmployeeStatus = EMPLOYMENT_STATUS_ SeveranceD cho bảng Employee
            IQueryable<EmployeeHistoryStatus> lsEmployeeHistoryStatus = EmployeeHistoryStatusBusiness.All.Where(o => o.EmployeeID == ewm.TeacherID && o.SchoolID == ewm.FromSchoolID).OrderBy(o => o.FromDate);
            if (lsEmployeeHistoryStatus.Count() != 0)
            {
                EmployeeHistoryStatus EmployeeHistoryStatus = lsEmployeeHistoryStatus.ToList().LastOrDefault();
                EmployeeHistoryStatus.EmployeeStatus = GlobalConstants.EMPLOYMENT_STATUS_SEVERANCE;
                EmployeeHistoryStatus.ToDate = EmployeeWorkMovement.MovedDate;
                EmployeeHistoryStatusBusiness.Update(EmployeeHistoryStatus);

            }
            // Cap nhat lai trang thai cho can bo la nghi viec
            Employee.EmploymentStatus = SystemParamsInFile.EMPLOYMENT_STATUS_SEVERANCE;
            EmployeeBusiness.Update(Employee);
            int employeeID = EmployeeWorkMovement.TeacherID;
            int schoolID = EmployeeWorkMovement.FromSchoolID;
            IQueryable<ConcurrentWorkReplacement> lstConcurrentWorkReplacement = ConcurrentWorkReplacementBusiness.SearchBySchool(schoolID, new Dictionary<string, object> { { "ReplacedTeacherID", employeeID }, { "IsActive", true } });
            foreach (ConcurrentWorkReplacement p in lstConcurrentWorkReplacement)
            {
                if (p.FromDate >= EmployeeWorkMovement.MovedDate)
                {
                    ConcurrentWorkReplacementBusiness.Delete(p.ConcurrentWorkReplacementID);
                }
                else
                {
                    if (p.ToDate > EmployeeWorkMovement.MovedDate)
                    {
                        p.ToDate = EmployeeWorkMovement.MovedDate;
                        ConcurrentWorkReplacementBusiness.BaseUpdate(p);
                    }
                }
            }
            IQueryable<HeadTeacherSubstitution> lstHeadTeacherSubstitution = HeadTeacherSubstitutionBusiness.All.Where(o => o.SchoolID == schoolID && o.SubstituedHeadTeacher == employeeID);
            foreach (HeadTeacherSubstitution p in lstHeadTeacherSubstitution)
            {
                if (p.AssignedDate >= EmployeeWorkMovement.MovedDate)
                {
                    HeadTeacherSubstitutionBusiness.Delete(p.HeadTeacherSubstitutionID);
                }
                else
                {
                    if (p.EndDate > EmployeeWorkMovement.MovedDate)
                    {
                        p.EndDate = EmployeeWorkMovement.MovedDate;
                        HeadTeacherSubstitutionBusiness.BaseUpdate(p);
                    }
                }
            }
            return base.Insert(ewm);
        }
        public EmployeeWorkMovement UpdateSeverance(EmployeeWorkMovement EmployeeWorkMovement, int SchoolID)
        {
            int type = 3;
            int employeeID = EmployeeWorkMovement.TeacherID;
            Validate(EmployeeWorkMovement, type);
            EmployeeWorkMovement ewm = EmployeeWorkMovementRepository.Find(EmployeeWorkMovement.EmployeeWorkMovementID);
            //Chưa chọn cán bộ nghỉ hưu hoặc cán bộ không tồn tại – Kiểm tra TeacherID
            EmployeeBusiness.CheckAvailable(EmployeeWorkMovement.TeacherID, "EmployeeWorkMovement_Label_TeacherID", false);
            //Quyết định nghỉ hưu phải nhỏ hơn 200 ký tự - Kiểm tra ResolutionDocument
            //Utils.ValidateMaxLength(EmployeeWorkMovement.ResolutionDocument, RESOLUTION_MAX_LENGTH, "EmployeeWorkMovement_Label_ResolutionDocument");
            //Ghi chú không được nhập quá 500 ký tự - Kiểm tra Description.
            //Utils.ValidateMaxLength(EmployeeWorkMovement.Description, DESCRIPTION_MAX_LENGTH, "EmployeeWorkMovement_Label_Description");
            //Ngày nghỉ hưu không được để trống – Kiểm tra MoveDate
            Utils.ValidateRequire(EmployeeWorkMovement.MovedDate, "EmployeeWorkMovement_Label_MovedDate");
            //Ngày nghỉ hưu phải <= ngày hiện tại. Kiểm tra MoveDate
            Utils.ValidatePastDate(EmployeeWorkMovement.MovedDate, "EmployeeWorkMovement_Label_MovedDate");
            //Ngày nghỉ hưu phải lớn hơn ngày vào biên chế - Kiểm tra MoveDate, JoinedDate
            if (EmployeeWorkMovement.Employee.JoinedDate != null)
            {
                Utils.ValidateBeforeDate(EmployeeWorkMovement.MovedDate, EmployeeWorkMovement.Employee.JoinedDate, "EmployeeWorkMovement_Label_MovedDate", "Employee_Label_JoinedDate");
            }
            //Kiểm tra  sự tồn tại của EmployeeWorkMovementID
            this.CheckAvailable(EmployeeWorkMovement.EmployeeWorkMovementID, "EmployeeWorkMovement_Label_EmployeeWorkMovementID");
            //Cán bộ nghỉ hưu này không thuộc trường truyền vào

            //this.CheckNotDuplicateCouple(EmployeeWorkMovement.EmployeeWorkMovementID.ToString(), SchoolID.ToString(), GlobalConstants.SCHOOL_SCHEMA, "EmployeeWorkMovement", "EmployeeWorkMovementID",
            //    "FromSchoolID", false, EmployeeWorkMovement.EmployeeWorkMovementID, "EmployeeWorkMovement_Label_EmployeeWorkMovementID");

            //EmployeeID, FromSchoolID(SchoolID): not compatible(Employee)
            {
                bool Exist = repository.ExistsRow(GlobalConstants.SCHOOL_SCHEMA, "Employee",
                        new Dictionary<string, object>()
                {
                    {"EmployeeID",EmployeeWorkMovement.TeacherID}
                    ,{"SchoolID",EmployeeWorkMovement.FromSchoolID}
                }, null);
                if (!Exist)
                {
                    throw new BusinessException("Common_Validate_NotAvailable");
                }
            }

            //Validate(EmployeeWorkMovement);

            //-	Description
            ewm.Description = EmployeeWorkMovement.Description;
            //-	ResolutionDocument
            ewm.ResolutionDocument = EmployeeWorkMovement.ResolutionDocument;
            //-	MovedDate
            ewm.MovedDate = EmployeeWorkMovement.MovedDate;

            //Tìm kiếm trong bảng EmployeeHistoryStatus bản ghi có EmployeeID =Entity.TeacherID, SchoolID = Entity.FromSchoolID lấy ra bản ghi có FromDate lớn nhất. Cập nhật lại bản ghi này với EmployeeStatus = EMPLOYMENT_STATUS_SeveranceMENT, ToDate = Entity.MovedDate.
            IQueryable<EmployeeHistoryStatus> lsEmployeeHistoryStatus = EmployeeHistoryStatusBusiness.All.Where(o => o.EmployeeID == ewm.TeacherID && o.SchoolID == ewm.FromSchoolID).OrderBy(o => o.FromDate);
            if (lsEmployeeHistoryStatus.Count() != 0)
            {
                EmployeeHistoryStatus EmployeeHistoryStatus = lsEmployeeHistoryStatus.ToList().LastOrDefault();
                EmployeeHistoryStatus.EmployeeStatus = GlobalConstants.EMPLOYMENT_STATUS_SEVERANCE;
                EmployeeHistoryStatus.ToDate = EmployeeWorkMovement.MovedDate;
                EmployeeHistoryStatusBusiness.Update(EmployeeHistoryStatus);
            }
            DateTime? movedDate = EmployeeWorkMovementBusiness.All.Where(o => o.EmployeeWorkMovementID == EmployeeWorkMovement.EmployeeWorkMovementID).Select(o => o.MovedDate).FirstOrDefault();
            if (EmployeeWorkMovement.MovedDate != movedDate)
            {
                int schoolID = EmployeeWorkMovement.FromSchoolID;
                IQueryable<ConcurrentWorkReplacement> lstConcurrentWorkReplacement = ConcurrentWorkReplacementBusiness.SearchBySchool(schoolID, new Dictionary<string, object> { { "ReplacedTeacherID", employeeID }, { "IsActive", true } });
                foreach (ConcurrentWorkReplacement p in lstConcurrentWorkReplacement)
                {
                    if (p.FromDate >= EmployeeWorkMovement.MovedDate)
                    {
                        ConcurrentWorkReplacementBusiness.Delete(p.ConcurrentWorkReplacementID);
                    }
                    else
                    {
                        if (p.ToDate > EmployeeWorkMovement.MovedDate)
                        {
                            p.ToDate = EmployeeWorkMovement.MovedDate;
                            ConcurrentWorkReplacementBusiness.BaseUpdate(p);
                        }
                    }
                }
                IQueryable<HeadTeacherSubstitution> lstHeadTeacherSubstitution = HeadTeacherSubstitutionBusiness.All.Where(o => o.SchoolID == schoolID && o.SubstituedHeadTeacher == employeeID);
                foreach (HeadTeacherSubstitution p in lstHeadTeacherSubstitution)
                {
                    if (p.AssignedDate >= EmployeeWorkMovement.MovedDate)
                    {
                        HeadTeacherSubstitutionBusiness.Delete(p.HeadTeacherSubstitutionID);
                    }
                    else
                    {
                        if (p.EndDate > EmployeeWorkMovement.MovedDate)
                        {
                            p.EndDate = EmployeeWorkMovement.MovedDate;
                            HeadTeacherSubstitutionBusiness.BaseUpdate(p);
                        }
                    }
                }

            }
            return base.Update(ewm);
        }
        public void DeleteSeverance(int EmployeeWorkMovementID, int SchoolID)
        {
            EmployeeWorkMovement employeeWorkMovement = EmployeeWorkMovementRepository.Find(EmployeeWorkMovementID);
            //EmployeeWorkMovementID: PK(EmployeeWorkMovement)
            this.CheckAvailable(EmployeeWorkMovementID, "EmployeeWorkMovement_Label_EmployeeWorkMovementID", false);
            //EmployeeID, SchoolID: not compatible(Employee)
            bool EmployeeSchoolCompatible = this.repository.ExistsRow(GlobalConstants.SCHOOL_SCHEMA, "Employee",
                  new Dictionary<string, object>()
                {
                    {"EmployeeID",employeeWorkMovement.TeacherID},
                    {"SchoolID",employeeWorkMovement.FromSchoolID}
                }, null);
            if (!EmployeeSchoolCompatible)
            {
                throw new BusinessException("Common_Validate_NotCompatible");
            }
            //SchoolID(FromSchoolID), EmployeeWorkMovementID: not compatible(EmployeeWorkMovement)
            bool EmployeeWorkMovementCompatible = this.repository.ExistsRow(GlobalConstants.SCHOOL_SCHEMA, "EmployeeWorkMovement",
                  new Dictionary<string, object>()
                {
                    {"EmployeeWorkMovementID",employeeWorkMovement.EmployeeWorkMovementID},
                    {"FromSchoolID",employeeWorkMovement.FromSchoolID}
                }, null);
            if (!EmployeeSchoolCompatible)
            {
                throw new BusinessException("Common_Validate_NotCompatible");
            }
            //EmployeeWorkMovement(EmployeeWorkMovementID).MovementType chỉ được nhận giá trị: WORK_MOVEMENT_SeveranceMENT
            if (employeeWorkMovement.MovementType != SystemParamsInFile.WORK_MOVEMENT_SEVERANCEMENT)
            {
                throw new BusinessException("EmployeeWorkMovement_Validate_NotSeverance");
            }

            IQueryable<EmployeeHistoryStatus> lsEmployeeHistoryStatus = EmployeeHistoryStatusBusiness.All.Where(o => o.EmployeeID == employeeWorkMovement.TeacherID && o.SchoolID == employeeWorkMovement.FromSchoolID).OrderBy(o => o.FromDate);
            if (lsEmployeeHistoryStatus.Count() != 0)
            {
                EmployeeHistoryStatus EmployeeHistoryStatus = lsEmployeeHistoryStatus.ToList().LastOrDefault();
                EmployeeHistoryStatus.EmployeeStatus = GlobalConstants.EMPLOYMENT_STATUS_WORKING;
                EmployeeHistoryStatus.ToDate = null;
                EmployeeHistoryStatusBusiness.Update(EmployeeHistoryStatus);
            }

            Employee Employee = EmployeeBusiness.Find(employeeWorkMovement.TeacherID);
            // Cap nhat lai trang thai cho can bo la dang lam viec
            Employee.EmploymentStatus = SystemParamsInFile.EMPLOYMENT_STATUS_WORKING;
            EmployeeBusiness.Update(Employee);

            //Cập nhật lại trang thái trong UserAccount
            int teacherID = employeeWorkMovement.TeacherID;
            UserAccount ua = UserAccountBusiness.All.Where(o => o.UserAccountID == employeeWorkMovement.OldAccountID).SingleOrDefault();
            if (ua != null)
            {
                ua.IsActive = true;
                ua.EmployeeID = teacherID;
                UserAccountBusiness.Update(ua);
                aspnet_Membership member = aspnet_MembershipBusiness.All.Where(o => o.UserId == ua.GUID).SingleOrDefault();
                member.IsApproved = 1;
                aspnet_MembershipBusiness.Update(member);
            }
            base.Delete(EmployeeWorkMovementID);
        }

        #endregion
    }
}
