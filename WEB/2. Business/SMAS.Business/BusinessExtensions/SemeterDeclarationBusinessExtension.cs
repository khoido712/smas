/**
 * The Viettel License
 *
 * Copyright 2012 Viettel Telecom. All rights reserved.
 * VIETTEL PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */

/** 
* @author  
* @version $Revision: $
*/

using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Linq.Expressions;
using System.Web;
using log4net;

using SMAS.DAL.Repository;
using SMAS.DAL.IRepository;
using SMAS.Business.IBusiness;
using SMAS.Models.Models;
using SMAS.Business.Common;

namespace SMAS.Business.Business
{
    public partial class SemeterDeclarationBusiness
    {
        /// <summary>
        /// Search
        /// </summary>
        /// <param name="SearchInfo"></param>
        /// <returns></returns>
        /// <author>
        /// vtit_dungnt77
        /// </author>
        public IQueryable<SemeterDeclaration> Search(IDictionary<String, object> SearchInfo)
        {
            //Thực hiện tìm kiếm trong bảng SemesterDeclaration theo các thông tin trong SearchInfo:
            //-	AcademicYearID: default = 0; 0 => Tìm kiếm All
            //-	SchoolID: default = 0; 0 => Tìm kiếm All
            //-	Year: default = 0; 0 => Tìm kiếm All
            //-	Semester : default = 0; 0 => Tìm kiếm All
            //-	Resolution: default = “ ” ; “ ” => Tìm kiếm All
            int AcademicYearID = Utils.GetInt(SearchInfo, "AcademicYearID");
            int SchoolID = Utils.GetInt(SearchInfo, "SchoolID");
            int Year = Utils.GetInt(SearchInfo, "Year");
            int Semester = (int)Utils.GetInt(SearchInfo, "Semester");
            string Resolution = Utils.GetString(SearchInfo, "Resolution");

            IQueryable<SemeterDeclaration> lsSemeterDeclaration = SemeterDeclarationRepository.All;
            if (AcademicYearID != 0)
            {
                lsSemeterDeclaration = lsSemeterDeclaration.Where(o => o.AcademicYearID == AcademicYearID);
            }
            if (SchoolID != 0)
            {
                lsSemeterDeclaration = lsSemeterDeclaration.Where(o => o.SchoolID == SchoolID);
            }
            if (Year != 0)
            {
                lsSemeterDeclaration = lsSemeterDeclaration.Where(o => o.Year == Year);
            }
            if (Semester != 0)
            {
                lsSemeterDeclaration = lsSemeterDeclaration.Where(o => o.Semester == Semester);
            }
            if (Resolution.Trim().Length != 0)
            {
                lsSemeterDeclaration = lsSemeterDeclaration.Where(o => o.Resolution.Contains(Resolution));
            }

            return lsSemeterDeclaration;
        }

        /// <summary>
        /// Insert
        /// </summary>
        /// <param name="SemeterDeclaration"></param>
        /// <returns></returns>
        /// <author>
        /// vtit_dungnt77
        /// </author>
        public override SemeterDeclaration Insert(SemeterDeclaration SemeterDeclaration)
        {
            //Trường có SchoolIDkhông tồn tại trong hệ thống
            SchoolProfileBusiness.CheckAvailable(SemeterDeclaration.SchoolID, "SemeterDeclaration_Label_SchoolID");
            //Số con điểm miệng phải là số nguyên dương, Kiểm tra trường InterviewMark
            Utils.ValidateIsNumber(SemeterDeclaration.InterviewMark.ToString(), "SemeterDeclaration_Label_InterviewMark");
            //Số con điểm 15 phút phái là số nguyên dương. Kiểm tra trường WritingMark
            Utils.ValidateIsNumber(SemeterDeclaration.WritingMark.ToString(), "SemeterDeclaration_Label_WritingMark");
            //Số con điểm 1 tiết phải là số nguyên dương. Kiểm tra trường TwiceCoeffiecientMark
            Utils.ValidateIsNumber(SemeterDeclaration.TwiceCoeffiecientMark.ToString(), "SemeterDeclaration_Label_TwiceCoeffiecientMark");

            //InterviewMark, WritingMark, TwiceCoeffiecientMark: 0 -> 20
            Utils.ValidateRange(SemeterDeclaration.InterviewMark, 0, 10, "SemeterDeclaration_Label_InterviewMark");
            Utils.ValidateRange(SemeterDeclaration.WritingMark, 0, 10, "SemeterDeclaration_Label_WritingMark");
            Utils.ValidateRange(SemeterDeclaration.TwiceCoeffiecientMark, 0, 10, "SemeterDeclaration_Label_TwiceCoeffiecientMark");

            Utils.ValidateRange(SemeterDeclaration.CoefficientInterview, 0, 3, "SemeterDeclaration_Label_CoefficientInterview");
            Utils.ValidateRange(SemeterDeclaration.CoefficientSemester, 0, 3, "SemeterDeclaration_Label_CoefficientSemester");
            Utils.ValidateRange(SemeterDeclaration.CoefficientTwice, 0, 3, "SemeterDeclaration_Label_CoefficientTwice");
            Utils.ValidateRange(SemeterDeclaration.CoefficientWriting, 0, 3, "SemeterDeclaration_Label_CoefficientWriting");

            AcademicYear academicYear = AcademicYearBusiness.Find(SemeterDeclaration.AcademicYearID);
            try
            {
                Utils.ValidateAfterDate(SemeterDeclaration.AppliedDate, academicYear.FirstSemesterStartDate.Value, "SemesterDeclaration_Label_AppliedDate", "AcademicYear_Label_FirstSemesterStartDate");
                Utils.ValidateAfterDate(academicYear.SecondSemesterEndDate.Value, SemeterDeclaration.AppliedDate, "AcademicYear_Label_SecondSemesterEndDate", "SemesterDeclaration_Label_AppliedDate");
            }
            catch (Exception)
            {

                throw new BusinessException("SemesterDeclaration_Label_AppliedDateOutAcademicYear");
            }

            //Học kỳ chỉ được nhận 2 giá trị
            //Xác định học kỳ 1 hay 2: tham số hệ thống SEMESTER_OF_YEAR
            //- SEMESTER_OF_YEAR_FIRST (1): Học kỳ 1
            //- SEMESTER_OF_YEAR_SECOND (2): Học kỳ 2
            
            //AcademicYearID: FK
            {
                bool Exist = repository.ExistsRow(GlobalConstants.SCHOOL_SCHEMA, "AcademicYear",
                        new Dictionary<string, object>()
                {
                    {"AcademicYearID",SemeterDeclaration.AcademicYearID}
                }, null);
                if (!Exist)
                {
                    throw new BusinessException("Common_Validate_NotAvailable");
                }
            }
            var MarkRecordDic = MarkRecordBusiness.GetMaxMarkRecord(SemeterDeclaration.AcademicYearID, SemeterDeclaration.SchoolID.Value, SemeterDeclaration.Semester);
            int MaxM = (int)MarkRecordDic[SystemParamsInFile.MARK_TYPE_M];
            int MaxP = (int)MarkRecordDic[SystemParamsInFile.MARK_TYPE_P];
            int MaxV = (int)MarkRecordDic[SystemParamsInFile.MARK_TYPE_V];
            if (SemeterDeclaration.InterviewMark < MaxM)
            {
                throw new BusinessException("SemesterDeclaration_Label_MarkError_M");
            }
            if (SemeterDeclaration.WritingMark < MaxP)
            {
                throw new BusinessException("SemesterDeclaration_Label_MarkError_P");
            }
            if (SemeterDeclaration.TwiceCoeffiecientMark < MaxV)
            {
                throw new BusinessException("SemesterDeclaration_Label_MarkError_V");
            }

            //sửa lại luồng khai báo số con điểm:
            //trước khi insert số con điểm mới thì delete số con điểm cũ đi
            List<SemeterDeclaration> lstSemesterDec = SemeterDeclarationBusiness.All.Where(s => s.SchoolID == SemeterDeclaration.SchoolID && s.AcademicYearID == SemeterDeclaration.AcademicYearID && s.AppliedDate == SemeterDeclaration.AppliedDate).ToList(); ;
            if (lstSemesterDec.Count > 0)
            {
                base.DeleteAll(lstSemesterDec);
            }
            return base.Insert(SemeterDeclaration);
        }

        /// <summary>
        /// Update
        /// </summary>
        /// <param name="SemeterDeclaration"></param>
        /// <returns></returns>
        /// <author>
        /// vtit_dungnt77
        /// </author>
        public override SemeterDeclaration Update(SemeterDeclaration SemeterDeclaration)
        {
            //check ton tai
            ValidationMetadata.ValidateObject(SemeterDeclaration);
            //Số con điểm miệng phải là số nguyên dương, Kiểm tra trường InterviewMark
            Utils.ValidateIsNumber(SemeterDeclaration.InterviewMark.ToString(), "SemeterDeclaration_Label_InterviewMark");
            //Số con điểm 15 phút phái là số nguyên dương. Kiểm tra trường WritingMark
            Utils.ValidateIsNumber(SemeterDeclaration.WritingMark.ToString(), "SemeterDeclaration_Label_WritingMark");
            //Số con điểm 1 tiết phải là số nguyên dương. Kiểm tra trường TwiceCoeffiecientMark
            Utils.ValidateIsNumber(SemeterDeclaration.TwiceCoeffiecientMark.ToString(), "SemeterDeclaration_Label_TwiceCoeffiecientMark");
            //Học kỳ chỉ được nhận 2 giá trị

            //InterviewMark, WritingMark, TwiceCoeffiecientMark: 0 -> 20
            Utils.ValidateRange(SemeterDeclaration.InterviewMark, 0, 10, "SemeterDeclaration_Label_InterviewMark");
            Utils.ValidateRange(SemeterDeclaration.WritingMark, 0, 10, "SemeterDeclaration_Label_WritingMark");
            Utils.ValidateRange(SemeterDeclaration.TwiceCoeffiecientMark, 0, 10, "SemeterDeclaration_Label_TwiceCoeffiecientMark");

            Utils.ValidateRange(SemeterDeclaration.CoefficientInterview, 0, 3, "SemeterDeclaration_Label_CoefficientInterview");
            Utils.ValidateRange(SemeterDeclaration.CoefficientSemester, 0, 3, "SemeterDeclaration_Label_CoefficientSemester");
            Utils.ValidateRange(SemeterDeclaration.CoefficientTwice, 0, 3, "SemeterDeclaration_Label_CoefficientTwice");
            Utils.ValidateRange(SemeterDeclaration.CoefficientWriting, 0, 3, "SemeterDeclaration_Label_CoefficientWriting");

            //(PeriodDeclaration.SearchBysemester(Semester, AcademicYearID, SchoolID)) -> lstPeriod
            //InterviewMark >= SUM(lstPeriod.InterviewMark )
            //WritingMark >= SUM(lstPeriod.WritingMark)
            //TwiceCoeffiecientMark >= SUM(lstPeriod.TwiceCoeffiecientMark)
          var lsPeriod =   PeriodDeclarationBusiness.SearchBySemester(new Dictionary<string, object>()
            {
                {"Semester",SemeterDeclaration.Semester}
                ,{"AcademicYearID",SemeterDeclaration.AcademicYearID}
                ,{"SchoolID",SemeterDeclaration.SchoolID}
            });

            if(lsPeriod!=null && lsPeriod.Count() > 0 )
            {
                int? InterviewMark = lsPeriod.Sum(o => o.InterviewMark);
                int? WritingMark = lsPeriod.Sum(o => o.WritingMark);
                int? TwiceCoeffiecientMark = lsPeriod.Sum(o => o.TwiceCoeffiecientMark);

                if(InterviewMark.HasValue)
                {
                   if(InterviewMark.Value > SemeterDeclaration.InterviewMark)
                   {
                       throw new BusinessException("PeriodDeclaration_Label_MaxInterviewMark");
                   }
                }
                if (WritingMark.HasValue)
                {
                    if (WritingMark.Value > SemeterDeclaration.WritingMark)
                    {
                        throw new BusinessException("PeriodDeclaration_Label_MaxWritingMark");
                    }
                }
                if (TwiceCoeffiecientMark.HasValue)
                {
                    if (TwiceCoeffiecientMark.Value > SemeterDeclaration.TwiceCoeffiecientMark)
                    {
                        throw new BusinessException("PeriodDeclaration_Label_MaxTwiceCoeffiecientMark");
                    }
                }
            }
            AcademicYear academicYear = AcademicYearBusiness.Find(SemeterDeclaration.AcademicYearID);
            //try
            //{
            //    Utils.ValidateAfterDate(SemeterDeclaration.AppliedDate, academicYear.FirstSemesterStartDate.Value, "SemesterDeclaration_Label_AppliedDate", "AcademicYear_Label_FirstSemesterStartDate");
            //    Utils.ValidateAfterDate(academicYear.SecondSemesterEndDate.Value, SemeterDeclaration.AppliedDate, "AcademicYear_Label_SecondSemesterEndDate", "SemesterDeclaration_Label_AppliedDate");
            //}
            //catch (Exception)
            //{

            //    throw new BusinessException("SemesterDeclaration_Label_AppliedDateOutAcademicYear");
            //}
            var MarkRecordDic = MarkRecordBusiness.GetMaxMarkRecord(SemeterDeclaration.AcademicYearID, SemeterDeclaration.SchoolID.Value, SemeterDeclaration.Semester);
            int MaxM = (int)MarkRecordDic[SystemParamsInFile.MARK_TYPE_M];
            int MaxP = (int)MarkRecordDic[SystemParamsInFile.MARK_TYPE_P];
            int MaxV = (int)MarkRecordDic[SystemParamsInFile.MARK_TYPE_V];
            if (SemeterDeclaration.InterviewMark < MaxM) 
            {
                throw new BusinessException("SemesterDeclaration_Label_MarkError_M");
            }
            if (SemeterDeclaration.WritingMark < MaxP) 
            {
                throw new BusinessException("SemesterDeclaration_Label_MarkError_P");
            }
            if (SemeterDeclaration.TwiceCoeffiecientMark < MaxV) 
            {
                throw new BusinessException("SemesterDeclaration_Label_MarkError_V");
            }
            return base.Update(SemeterDeclaration);
        }

    }
}
