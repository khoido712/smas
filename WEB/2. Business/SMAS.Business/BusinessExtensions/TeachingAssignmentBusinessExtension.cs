/**
 * The Viettel License
 *
 * Copyright 2012 Viettel Telecom. All rights reserved.
/**
 * The Viettel License
 *
 * Copyright 2012 Viettel Telecom. All rights reserved.
 * VIETTEL PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */

/** 
* @author  
* @version $Revision: $
*/

using System;
using System.Collections.Generic;
using System.Linq;
using SMAS.Models.Models;
using SMAS.Business.Common;
using SMAS.Business.BusinessObject;

namespace SMAS.Business.Business
{
    public partial class TeachingAssignmentBusiness
    {
        /// <summary>
        /// Kiểm tra ràng buộc object khi thêm/xóa/sửa
        /// </summary>
        /// <param name="Entity"></param>
        private void Validate(TeachingAssignment Entity)
        {
            ValidationMetadata.ValidateObject(Entity);

            // Kiểm tra tính hợp lệ dữ liệu
            IDictionary<string, object> SearchInfo = new Dictionary<string, object>();
            SearchInfo["AcademicYearID"] = Entity.AcademicYearID;
            SearchInfo["SchoolID"] = Entity.SchoolID;
            bool compatible = this.repository.ExistsRow(GlobalConstants.SCHOOL_SCHEMA, "AcademicYear", SearchInfo);
            if (!compatible)
            {
                throw new BusinessException("Common_Validate_NotCompatible");
            }

            // Kiểm tra tính hợp lệ dữ liệu
            SearchInfo = new Dictionary<string, object>();
            SearchInfo["EmployeeID"] = Entity.TeacherID;
            SearchInfo["SchoolID"] = Entity.SchoolID;
            compatible = this.repository.ExistsRow(GlobalConstants.SCHOOL_SCHEMA, "Employee", SearchInfo);
            if (!compatible)
            {
                throw new BusinessException("Common_Validate_NotCompatible");
            }

            // Kiểm tra tính hợp lệ dữ liệu
            SearchInfo = new Dictionary<string, object>();
            SearchInfo["SubjectID"] = Entity.SubjectID;
            SearchInfo["ClassID"] = Entity.ClassID;
            compatible = this.repository.ExistsRow(GlobalConstants.SCHOOL_SCHEMA, "ClassSubject", SearchInfo);
            if (!compatible)
            {
                throw new BusinessException("Common_Validate_NotCompatible");
            }

            // Kiểm tra tính hợp lệ dữ liệu
            SearchInfo = new Dictionary<string, object>();
            SearchInfo["AcademicYearID"] = Entity.AcademicYearID;
            SearchInfo["ClassProfileID"] = Entity.ClassID;
            compatible = this.repository.ExistsRow(GlobalConstants.SCHOOL_SCHEMA, "ClassProfile", SearchInfo);
            if (!compatible)
            {
                throw new BusinessException("Common_Validate_NotCompatible");
            }

            // Kiểm trùng dữ liệu
            SearchInfo = new Dictionary<string, object>();
            SearchInfo["Semester"] = Entity.Semester;
            SearchInfo["ClassID"] = Entity.ClassID;
            SearchInfo["SubjectID"] = Entity.SubjectID;
            SearchInfo["TeacherID"] = Entity.TeacherID;
            SearchInfo["IsActive"] = true;

            IDictionary<string, object> ExceptInfo = new Dictionary<string, object>();
            ExceptInfo["TeachingAssignmentID"] = Entity.TeachingAssignmentID;

            bool duplicate = this.repository.ExistsRow(GlobalConstants.SCHOOL_SCHEMA, "TeachingAssignment", SearchInfo, ExceptInfo);
            if (duplicate)
            {
                List<object> Params = new List<object>();
                Params.Add("TeachingAssignment_Label");
                throw new BusinessException("Common_Validate_Duplicate", Params);
            }

            // Kiểm tra có phải năm hiện tại không
            bool isCurrentYear = this.AcademicYearBusiness.IsCurrentYear(Entity.AcademicYearID);
            if (!isCurrentYear)
            {
                throw new BusinessException("Common_Validate_NotCurrentYear");
            }

            // Kiểm tra trạng thái giáo viên phải đang làm việc
            Employee Teacher = Entity.Employee;
            if (Teacher == null)
            {
                Teacher = this.EmployeeBusiness.Find(Entity.TeacherID);
            }
            if (Teacher.EmploymentStatus != SystemParamsInFile.EMPLOYMENT_STATUS_WORKING)
            {
                throw new BusinessException("TeachingAssignment_Validate_TeacherNotWorking");
            }
        }

        /// <summary>
        /// Tìm kiếm phân công giảng dạy
        /// </summary>
        /// <param name="semester">Học kỳ</param>
        /// <param name="EducationLevel">Khối học</param>
        /// <param name="ClassID">ID lớp</param>
        /// <param name="IsActive">Biến kích hoạt</param>
        /// <param name="AcademicYearID">ID năm</param>
        /// <param name="SchoolID">ID trường</param>
        /// 
        /// <returns>
        /// Danh sách phân công giảng dạy
        /// </returns>
        /// <author>hath</author>
        /// <date> 9/7/2012</date>
        public IQueryable<TeachingAssignmentBO> GetTeachingAssigment(int SchoolID, IDictionary<string, object> dic)
        {
            if (SchoolID == 0)
            {
                return null;
            }

            //int? UNASSIGN = 1;
            //int? ASSIGNED = 2;
            int Semester = (int)Utils.GetShort(dic, "Semester", 1);
            int EducationLevel = Utils.GetShort(dic, "EducationLevel");
            int ClassID = Utils.GetInt(dic, "ClassID");
            int AcademicYearID = Utils.GetInt(dic, "AcademicYearID");
            int FacultyID = Utils.GetInt(dic, "FacultyID");
            int SubjectID = Utils.GetInt(dic, "SubjectID");
            int TeacherID = Utils.GetInt(dic, "TeacherID");
            int AppliedLevel = Utils.GetInt(dic, "AppliedLevel");
            int Type = Utils.GetInt(dic, "Type");
            bool isCurrentYear = AcademicYearBusiness.IsCurrentYear(AcademicYearID);
            //Chiendd1: 03/07/2015, bo sung dieu kien partitionId
            int partitionId = UtilsBusiness.GetPartionId(SchoolID);
            var queryTA = this.All.Where(ta => ta.Last2digitNumberSchool == partitionId);

            if (ClassID != 0)
            {
                queryTA = queryTA.Where(o => o.ClassID == ClassID);
            }
            if (AcademicYearID != 0)
            {
                queryTA = queryTA.Where(o => o.AcademicYearID == AcademicYearID);
            }
            if (SubjectID != 0)
            {
                queryTA = queryTA.Where(o => o.SubjectID == SubjectID);
            }
            if (AppliedLevel != 0)
            {
                queryTA = queryTA.Where(o => o.ClassProfile.EducationLevel.Grade == AppliedLevel);
            }

            queryTA = queryTA.Where(o => o.IsActive == true);

            //List<TeachingAssignment> ListTA = queryTA.ToList();

            var query = from cp in ClassProfileBusiness.All.Where(c => c.IsActive == true)
                        join cs in ClassSubjectBusiness.All.Where(c => c.Last2digitNumberSchool == partitionId) on cp.ClassProfileID equals cs.ClassID
                        join sc in SubjectCatRepository.All on cs.SubjectID equals sc.SubjectCatID
                        join ta in this.All.Where(t => t.Last2digitNumberSchool == partitionId) on cs.ClassID equals ta.ClassID into g1
                        from j2 in g1.Where(o => o.IsActive == true && o.SubjectID == sc.SubjectCatID).DefaultIfEmpty()
                        join em in EmployeeRepository.All on j2.TeacherID equals em.EmployeeID into g2
                        from j3 in g2.DefaultIfEmpty()
                        select new TeachingAssignmentBO
                        {
                            TeachingAssignmentID = j2.TeachingAssignmentID,
                            Semester = j2.Semester,
                            ClassID = cp.ClassProfileID,
                            ClassName = cp.DisplayName,
                            ClassOrderNumber = cp.OrderNumber,
                            SubjectID = cs.SubjectID,
                            SubjectName = sc.DisplayName,
                            FacultyID = j2.FacultyID,
                            FacultyName = j3.SchoolFaculty.FacultyName,
                            TeacherID = j3.EmployeeID,
                            TeacherName = j3.FullName,
                            EducationLevelID = cp.EducationLevelID,
                            SchoolID = cp.SchoolID,
                            AcademicYearID = cp.AcademicYearID,
                            OnlyOneSemester = true,
                            SectionPerWeekFirst = cs.SectionPerWeekFirstSemester,
                            SectionPerWeekSecond = cs.SectionPerWeekSecondSemester,
                            AppliedLevel = cp.EducationLevel.Grade,
                            Name = j3.Name,
                            EmploymentStatus = j3.EmploymentStatus,
                            ExistInOtherSemester = queryTA.Any(
                                            o => o.SubjectID == cs.SubjectID &&
                                            o.ClassID == cs.ClassID &&
                                            o.TeacherID != j2.TeacherID &&
                                            o.Semester != j2.Semester &&
                                            o.IsActive == true
                                        )
                        };
            if (EducationLevel != 0)
            {
                query = query.Where(o => o.EducationLevelID == EducationLevel);
            }

            if (ClassID != 0)
            {
                query = query.Where(o => o.ClassID == ClassID);
            }

            if (AcademicYearID != 0)
            {
                query = query.Where(o => o.AcademicYearID == AcademicYearID);
            }

            if (FacultyID != 0)
            {
                query = query.Where(o => o.FacultyID == FacultyID);
            }

            if (SubjectID != 0)
            {
                query = query.Where(o => o.SubjectID == SubjectID);
            }

            if (TeacherID != 0)
            {
                query = query.Where(o => o.TeacherID == TeacherID);
            }
            if (AppliedLevel != 0)
            {
                query = query.Where(o => o.AppliedLevel == AppliedLevel);
            }
            if (SchoolID != 0)
            {
                query = query.Where(o => o.SchoolID == SchoolID);
            }
            //if (Type == UNASSIGN)
            //{
            //    query = query.Where(o => o.TeacherID == null);
            //}

            //if (Type == ASSIGNED)
            //{
            //    query = query.Where(o => o.TeacherID != null);
            //}

            IEnumerable<TeachingAssignmentBO> lst = query.ToList();
            List<TeachingAssignmentBO> result = new List<TeachingAssignmentBO>();
            foreach (TeachingAssignmentBO bo in lst)
            {
                if (Semester == SystemParamsInFile.SEMESTER_OF_YEAR_FIRST)
                {
                    bo.SectionPerWeek = bo.SectionPerWeekFirst;
                }
                else
                {
                    bo.SectionPerWeek = bo.SectionPerWeekSecond;
                }
                //Xét trường hợp học kỳ là học kỳ đang xét
                if (bo.Semester == Semester)
                {
                    int num = lst.Where(o => o.TeacherID == bo.TeacherID && o.SubjectID == bo.SubjectID && o.ClassID == bo.ClassID).Count();
                    if (num > 1)
                    {
                        bo.OnlyOneSemester = false;
                    }
                    if (isCurrentYear)
                    {
                        if ((bo.EmploymentStatus != null && bo.EmploymentStatus != SystemParamsInFile.EMPLOYMENT_STATUS_WORKING) || bo.EmploymentStatus == null)
                        {
                            if (result.Where(o => o.ClassID == bo.ClassID && o.SubjectID == bo.SubjectID).Count() > 0 || lst.Where(o => o.ClassID == bo.ClassID && o.SubjectID == bo.SubjectID && o.EmploymentStatus == SystemParamsInFile.EMPLOYMENT_STATUS_WORKING && o.Semester == Semester).Count() > 0)
                            {
                                continue;
                            }
                            bo.TeachingAssignmentID = null;
                            bo.FacultyID = null;
                            bo.FacultyName = null;
                            bo.TeacherID = null;
                            bo.TeacherName = null;
                            bo.OnlyOneSemester = true;
                            bo.EmploymentStatus = null;
                            bo.Name = null;
                        }
                    }
                }
                //Xét trường học học kỳ là học kỳ khác học kỳ đang xét (Bao gồm cả semester = null)
                else
                {
                    int num = lst.Where(o => o.SubjectID == bo.SubjectID && o.ClassID == bo.ClassID && o.Semester == Semester).Count();
                    if (num > 0)
                    {
                        continue;
                    }
                    else
                    {
                        bo.TeachingAssignmentID = null;
                        bo.FacultyID = null;
                        bo.FacultyName = null;
                        bo.TeacherID = null;
                        bo.TeacherName = null;
                    }
                }

                //Kiểm tra phân công giảng dạy theo lớp, khối, trường đã tồn tại phân công giang dạy môn học cho lớp học đó hay chưa? Nếu tồn tại trong học kỳ.
                /*TeachingAssignment ta = ListTA.Where(
                                            o => o.SubjectID == bo.SubjectID &&
                                            o.ClassID == bo.ClassID &&
                                            o.TeacherID != bo.TeacherID &&
                                            o.Semester != bo.Semester &&
                                            o.IsActive == true
                                        ).FirstOrDefault();

                if (ta == null)
                {
                    bo.ExistInOtherSemester = false;
                }*/
                bo.Semester = Semester;
                result.Add(bo);
            }
            if (Semester != 0)
            {
                result = result.Where(o => o.Semester == Semester).ToList();
            }
            if (TeacherID != 0)
            {
                result = result.Where(o => o.TeacherID == TeacherID).ToList();
            }
            return result.AsQueryable();

        }

        public IQueryable<TeachingAssignmentBO> GetSubjectTeacher(int SchoolID, IDictionary<string, object> dic)
        {
            if (SchoolID == 0)
            {
                return null;
            }

            int Semester = (int)Utils.GetShort(dic, "Semester");
            int EducationLevel = Utils.GetShort(dic, "EducationLevel");
            int ClassID = Utils.GetInt(dic, "ClassID");
            int AcademicYearID = Utils.GetInt(dic, "AcademicYearID");
            int FacultyID = Utils.GetInt(dic, "FacultyID");
            int SubjectID = Utils.GetInt(dic, "SubjectID");
            int TeacherID = Utils.GetInt(dic, "TeacherID");
            int AppliedLevel = Utils.GetInt(dic, "AppliedLevel");
            int Type = Utils.GetInt(dic, "Type");
            List<int> lstClassID = Utils.GetIntList(dic, "lstClassID");
            List<int> lstSubjectID = Utils.GetIntList(dic, "lstSubjectID");
            bool isCurrentYear = AcademicYearBusiness.IsCurrentYear(AcademicYearID);
            //Chiendd1: 03/07/2015, bo sung dieu kien partitionId
            int partitionId = UtilsBusiness.GetPartionId(SchoolID);
            var queryTA = this.All;

            if (ClassID != 0)
            {
                queryTA = queryTA.Where(o => o.ClassID == ClassID);
            }
            if (AcademicYearID != 0)
            {
                queryTA = queryTA.Where(o => o.AcademicYearID == AcademicYearID);
            }
            if (SubjectID != 0)
            {
                queryTA = queryTA.Where(o => o.SubjectID == SubjectID);
            }
            if (AppliedLevel != 0)
            {
                queryTA = queryTA.Where(o => o.ClassProfile.EducationLevel.Grade == AppliedLevel);
            }
            if (lstClassID.Count > 0)
            {
                queryTA = queryTA.Where(o => lstClassID.Contains(o.ClassID.Value));
            }
            if (lstSubjectID.Count > 0)
            {
                queryTA = queryTA.Where(o => lstSubjectID.Contains(o.SubjectID));
            }

            queryTA = queryTA.Where(o => o.IsActive == true);

            var query = from cp in ClassProfileBusiness.All.Where(c=>c.IsActive==true)
                        join cs in ClassSubjectRepository.All.Where(c => c.Last2digitNumberSchool == partitionId) on cp.ClassProfileID equals cs.ClassID
                        join sc in SubjectCatRepository.All on cs.SubjectID equals sc.SubjectCatID
                        join ta in this.All.Where(t => t.Last2digitNumberSchool == partitionId) on cs.ClassID equals ta.ClassID into g1
                        from j2 in g1.Where(o => o.IsActive == true && o.SubjectID == sc.SubjectCatID).DefaultIfEmpty()
                        join em in EmployeeRepository.All on j2.TeacherID equals em.EmployeeID into g2
                        from j3 in g2.DefaultIfEmpty()
                        join et in EthnicRepository.All on j3.EthnicID equals et.EthnicID into g3
                        from j4 in g3.DefaultIfEmpty()
                        where cp.IsActive.Value
                        select new TeachingAssignmentBO
                        {
                            TeachingAssignmentID = j2.TeachingAssignmentID,
                            Semester = j2.Semester,
                            ClassID = cp.ClassProfileID,
                            ClassName = cp.DisplayName,
                            ClassOrderNumber = cp.OrderNumber,
                            SubjectID = cs.SubjectID,
                            SubjectName = sc.DisplayName,
                            FacultyID = j2.FacultyID,
                            FacultyName = j2.SchoolFaculty.FacultyName,
                            TeacherID = j2.TeacherID,
                            TeacherName = j3.FullName,
                            EthnicCode = j4 != null ? j4.EthnicCode : null,
                            EducationLevelID = cp.EducationLevelID,
                            SchoolID = cp.SchoolID,
                            AcademicYearID = cp.AcademicYearID,
                            OrderInSubject = sc.OrderInSubject,
                            OnlyOneSemester = true,
                            SectionPerWeekFirst = cs.SectionPerWeekFirstSemester,
                            SectionPerWeekSecond = cs.SectionPerWeekSecondSemester,
                            AppliedLevel = cp.EducationLevel.Grade,
                            Name = j3.Name,
                            EmploymentStatus = j3.EmploymentStatus,
                            ExistInOtherSemester = queryTA.Any(
                                            o => o.SubjectID == cs.SubjectID &&
                                            o.ClassID == cs.ClassID &&
                                            o.TeacherID != j2.TeacherID &&
                                            o.Semester != j2.Semester &&
                                            o.IsActive == true
                                        )
                        };
            if (EducationLevel != 0)
            {
                query = query.Where(o => o.EducationLevelID == EducationLevel);
            }

            if (ClassID != 0)
            {
                query = query.Where(o => o.ClassID == ClassID);
            }

            if (AcademicYearID != 0)
            {
                query = query.Where(o => o.AcademicYearID == AcademicYearID);
            }

            if (FacultyID != 0)
            {
                query = query.Where(o => o.FacultyID == FacultyID);
            }

            if (SubjectID != 0)
            {
                query = query.Where(o => o.SubjectID == SubjectID);
            }

            if (TeacherID != 0)
            {
                query = query.Where(o => o.TeacherID == TeacherID);
            }
            if (AppliedLevel != 0)
            {
                query = query.Where(o => o.AppliedLevel == AppliedLevel);
            }
            if (SchoolID != 0)
            {
                query = query.Where(o => o.SchoolID == SchoolID);
            }

            if (Semester != 0)
            {
                query = query.Where(o => o.Semester == Semester);
            }
            if (TeacherID != 0)
            {
                query = query.Where(o => o.TeacherID == TeacherID);
            }
            return query;
        }


        /// <summary>
        /// Xóa phân công giảng dạy
        /// </summary>
        /// <param name="TeachingAssignmentID">ID phân công giảng dạy</param>
        /// <param name="SchoolID">ID trường.</param>
        /// <author>hath</author>
        /// <date> 9/7/2012</date>
        public void Delete(int SchoolID, int TeachingAssignmentID, bool ApplyForAll)
        {
            this.CheckAvailable(TeachingAssignmentID, "TeachingAssignment_Label");

            // Kiểm tra tính hợp lệ dữ liệu
            IDictionary<string, object> SearchInfo = new Dictionary<string, object>();
            SearchInfo["TeachingAssignmentID"] = TeachingAssignmentID;
            SearchInfo["SchoolID"] = SchoolID;
            bool compatible = this.repository.ExistsRow(GlobalConstants.SCHOOL_SCHEMA, "TeachingAssignment", SearchInfo);
            if (!compatible)
            {
                throw new BusinessException("Common_Validate_NotCompatible");
            }


            // Kiểm tra có phải năm hiện tại không
            TeachingAssignment OldEntity = this.Find(TeachingAssignmentID);
            bool isCurrentYear = this.AcademicYearBusiness.IsCurrentYear(OldEntity.AcademicYearID);
            if (!isCurrentYear)
            {
                throw new BusinessException("Common_Validate_NotCurrentYear");
            }
            //Chiendd1: 03/07/2015, bo sung dieu kien partitionId
            int partitionId = UtilsBusiness.GetPartionId(SchoolID);
            TeachingAssignment OtherEntity = this.All.Where(
                                                o => o.ClassID == OldEntity.ClassID &&
                                                o.SubjectID == OldEntity.SubjectID &&
                                                o.TeacherID == OldEntity.TeacherID &&
                                                o.Semester != OldEntity.Semester
                                                && o.Last2digitNumberSchool == partitionId
                                             ).FirstOrDefault();

            base.Delete(TeachingAssignmentID, true);

            if (ApplyForAll && OtherEntity != null)
            {
                base.Delete(OtherEntity.TeachingAssignmentID, true);
            }
        }


        /// <summary>
        /// Thêm phân công giảng dạy
        /// </summary>
        /// <param name="teachingAssignment">Đối tượng TeachingAssignment</param>
        /// <returns>
        /// Đối tượng TeachingAssignment
        /// </returns>
        /// <author>hath</author>
        /// <date> 9/7/2012</date>
        public override TeachingAssignment Insert(TeachingAssignment teachingAssignment)
        {
            this.Validate(teachingAssignment);
            return base.Insert(teachingAssignment);
        }


        /// <summary>
        /// Sửa phân công giảng dạy
        /// </summary>
        /// <param name="teachingAssignment">Đối tượng TeachingAssignment</param>
        /// <param name="SchoolID">The school ID.</param>
        /// <returns>
        /// Đối tượng TeachingAssignment
        /// </returns>
        /// <author>hath</author>
        /// <date> 9/7/2012</date>
        public TeachingAssignment Update(int SchoolID, long TeachingAssignmentID, int FacultyID, int TeacherID, bool ApplyForAll)
        {
            this.CheckAvailable(TeachingAssignmentID, "TeachingAssignment_Label");
            TeachingAssignment OldEntity = this.Find(TeachingAssignmentID);
            //Chiendd1: 03/07/2015, bo sung dieu kien partitionId
            int partitionId = UtilsBusiness.GetPartionId(SchoolID);
            // Kiểm tra tính hợp lệ dữ liệu
            IDictionary<string, object> SearchInfo = new Dictionary<string, object>();
            SearchInfo["TeachingAssignmentID"] = TeachingAssignmentID;
            SearchInfo["SchoolID"] = SchoolID;
            bool compatible = this.repository.ExistsRow(GlobalConstants.SCHOOL_SCHEMA, "TeachingAssignment", SearchInfo);
            if (!compatible)
            {
                throw new BusinessException("Common_Validate_NotCompatible");
            }

            // Kiểm tra có phải năm hiện tại không
            bool isCurrentYear = this.AcademicYearBusiness.IsCurrentYear(OldEntity.AcademicYearID);
            if (!isCurrentYear)
            {
                throw new BusinessException("Common_Validate_NotCurrentYear");
            }

            SearchInfo = new Dictionary<string, object>();
            SearchInfo["Semester"] = OldEntity.Semester;
            SearchInfo["ClassID"] = OldEntity.ClassID;
            SearchInfo["SubjectID"] = OldEntity.SubjectID;
            SearchInfo["TeacherID"] = TeacherID;
            SearchInfo["IsActive"] = true;
            IDictionary<string, object> ExceptInfo = new Dictionary<string, object>();
            ExceptInfo["TeachingAssignmentID"] = OldEntity.TeachingAssignmentID;

            bool duplicate = this.repository.ExistsRow(GlobalConstants.SCHOOL_SCHEMA, "TeachingAssignment", SearchInfo, ExceptInfo);
            if (duplicate)
            {
                List<object> Params = new List<object>();
                Params.Add("TeachingAssignment_Label");
                throw new BusinessException("Common_Validate_Duplicate", Params);
            }
            // Kiểm tra trạng thái giáo viên phải đang làm việc
            Employee Teacher = this.EmployeeBusiness.Find(TeacherID);
            if (Teacher.EmploymentStatus != SystemParamsInFile.EMPLOYMENT_STATUS_WORKING)
            {
                throw new BusinessException("TeachingAssignment_Validate_TeacherNotWorking");
            }
            TeachingAssignment OtherEntity = this.All.Where(
                                                o => o.ClassID == OldEntity.ClassID &&
                                                o.SubjectID == OldEntity.SubjectID &&
                                                o.TeacherID == OldEntity.TeacherID &&
                                                o.Semester != OldEntity.Semester &&
                                                o.IsActive == true
                                                && o.Last2digitNumberSchool == partitionId
                                             ).FirstOrDefault();

            OldEntity.TeacherID = TeacherID;
            OldEntity.FacultyID = FacultyID;
            base.Update(OldEntity);

            if (OtherEntity == null)
            {
                if (ApplyForAll)
                {
                    TeachingAssignment NewTA = new TeachingAssignment();
                    NewTA.AcademicYearID = OldEntity.AcademicYearID;
                    NewTA.AssignedDate = OldEntity.AssignedDate;
                    NewTA.ClassID = OldEntity.ClassID;
                    NewTA.FacultyID = OldEntity.FacultyID;
                    NewTA.IsActive = OldEntity.IsActive;
                    NewTA.SchoolID = OldEntity.SchoolID;
                    NewTA.Semester = (int)(OldEntity.Semester == SystemParamsInFile.SEMESTER_OF_YEAR_FIRST ?
                                     SystemParamsInFile.SEMESTER_OF_YEAR_SECOND :
                                     SystemParamsInFile.SEMESTER_OF_YEAR_FIRST);
                    NewTA.SubjectID = OldEntity.SubjectID;
                    NewTA.TeacherID = OldEntity.TeacherID;
                    NewTA.Last2digitNumberSchool = partitionId;
                    base.Insert(NewTA);
                }
            }
            else
            {
                if (ApplyForAll)
                {
                    OtherEntity.TeacherID = TeacherID;
                    OtherEntity.FacultyID = FacultyID;
                    base.Update(OtherEntity);
                }
                else
                {
                    base.Delete(OtherEntity.TeachingAssignmentID);
                }
            }

            return OldEntity;
        }

        /// <summary>
        /// Lấy danh sách các môn mà GVBM được phân công giảng dạy
        /// </summary>
        /// <param name="TeacherID"></param>
        /// <param name="AcademicYearID"></param>
        /// <param name="SchoolID"></param>
        /// <returns></returns>
        public IQueryable<SubjectCat> GetListSubjectBySubjectTeacher(int TeacherID, int AcademicYearID, int SchoolID, int Semester, int ClassID)
        {
            //Chiendd1: 03/07/2015, bo sung dieu kien partitionId
            int partitionId = UtilsBusiness.GetPartionId(SchoolID);
            return
            this.All.Where(
                o => (
                    o.TeacherID == TeacherID &&
                    o.AcademicYearID == AcademicYearID &&
                    o.SchoolID == SchoolID &&
                    o.ClassID == ClassID &&
                    o.Semester == Semester &&
                    o.IsActive &&
                    o.SubjectCat.IsActive
                    && o.Last2digitNumberSchool == partitionId
                )
            ).Select(o => o.SubjectCat).Distinct();
        }

        public IQueryable<ClassSubjectTemp> GetListSubjectBySubjectTeacher_BM(int TeacherID, int AcademicYearID, int SchoolID, int Semester, int ClassID)
        {
            //Chiendd1: 03/07/2015, bo sung dieu kien partitionId
            int partitionId = UtilsBusiness.GetPartionId(SchoolID);
            if (Semester == SystemParamsInFile.SEMESTER_OF_YEAR_FIRST || Semester == SystemParamsInFile.SEMESTER_OF_YEAR_SECOND)
            {
                return
                this.All.Where(
                    o => (
                        o.TeacherID == TeacherID &&
                        o.AcademicYearID == AcademicYearID &&
                        o.SchoolID == SchoolID &&
                        o.ClassID == ClassID &&
                        o.Semester == Semester &&
                        o.IsActive &&
                        o.SubjectCat.IsActive
                        && o.Last2digitNumberSchool == partitionId
                    )
                ).Select(o => new ClassSubjectTemp { ClassID = o.ClassID, SubjectID = o.SubjectID, DisplayName = o.SubjectCat.DisplayName, OrderInSubject = o.SubjectCat.OrderInSubject }).Distinct();
            }
            else
            {
                return
               this.All.Where(
                   o => (
                       o.TeacherID == TeacherID &&
                       o.AcademicYearID == AcademicYearID &&
                       o.SchoolID == SchoolID &&
                       o.ClassID == ClassID &&
                       (o.Semester == SystemParamsInFile.SEMESTER_OF_YEAR_FIRST || o.Semester == SystemParamsInFile.SEMESTER_OF_YEAR_SECOND) &&
                       o.IsActive &&
                       o.SubjectCat.IsActive
                       && o.Last2digitNumberSchool == partitionId
                   )
               ).Select(o => new ClassSubjectTemp { ClassID = o.ClassID, SubjectID = o.SubjectID, DisplayName = o.SubjectCat.DisplayName, OrderInSubject = o.SubjectCat.OrderInSubject }).Distinct();
            }
        }

        /// <summary>
        /// Phân công giảng dạy theo giáo viên
        /// Author: Tamhm1
        /// Date: 11/1/2013
        /// </summary>
        /// <param name="SchoolID"></param>
        /// <param name="AcademicYearID"></param>
        /// <param name="AppliedLevel"></param>
        /// <param name="TeacherID"></param>
        /// <param name="Semester"></param>
        /// <param name="lstTeachingAssignment"></param>
        /// <returns></returns>
        public void UpdateForEmployee(int SchoolID, int AcademicYearID, int AppliedLevel, int TeacherID, int Semester, List<TeachingAssignment> lstTeachingAssignment)
        {
            // Kiểm tra tính hợp lệ dữ liệu
            IDictionary<string, object> SearchInfo = new Dictionary<string, object>();
            SearchInfo["AcademicYearID"] = AcademicYearID;
            SearchInfo["SchoolID"] = SchoolID;
            bool compatible = this.repository.ExistsRow(GlobalConstants.SCHOOL_SCHEMA, "AcademicYear", SearchInfo);
            if (!compatible)
            {
                throw new BusinessException("Common_Validate_NotCompatible");
            }
            //Kiểm tra tính hợp lệ dữ liệu
            SearchInfo = new Dictionary<string, object>();
            SearchInfo["EmployeeID"] = TeacherID;
            SearchInfo["SchoolID"] = SchoolID;
            compatible = this.repository.ExistsRow(GlobalConstants.SCHOOL_SCHEMA, "Employee", SearchInfo);
            if (!compatible)
            {
                throw new BusinessException("Common_Validate_NotCompatible");
            }
            //Năm học không phải năm hiện tại
            bool isCurrentYear = this.AcademicYearBusiness.IsCurrentYear(AcademicYearID);
            if (!isCurrentYear)
            {
                throw new BusinessException("Common_Validate_NotCurrentYear");
            }

            //Kiểm tra trạng thái đang làm việc
            Employee Teacher = EmployeeBusiness.Find(TeacherID);
            if (Teacher.EmploymentStatus != SystemParamsInFile.EMPLOYMENT_STATUS_WORKING)
            {
                throw new BusinessException("TeachingAssignment_Validate_TeacherNotWorking");
            }

            //Với mỗi TeachingAssignment trong lstTeachingAssignment
            foreach (TeachingAssignment teachingAssignment in lstTeachingAssignment)
            {
                int EducationLevelID = ClassProfileBusiness.Find(teachingAssignment.ClassID).EducationLevelID;
                SearchInfo = new Dictionary<string, object>();
                SearchInfo["SubjectID"] = teachingAssignment.SubjectID;
                SearchInfo["ClassID"] = teachingAssignment.ClassID;

                compatible = this.repository.ExistsRow(GlobalConstants.SCHOOL_SCHEMA, "ClassSubject", SearchInfo);
                if (!compatible)
                {
                    throw new BusinessException("Common_Validate_NotCompatible");
                }
                SearchInfo = new Dictionary<string, object>();
                SearchInfo["ClassProfileID"] = teachingAssignment.ClassID;
                SearchInfo["AcademicYearID"] = AcademicYearID;
                compatible = this.repository.ExistsRow(GlobalConstants.SCHOOL_SCHEMA, "ClassProfile", SearchInfo);
                if (!compatible)
                {
                    throw new BusinessException("Common_Validate_NotCompatible");
                }
                SearchInfo = new Dictionary<string, object>();
                SearchInfo["Grade"] = AppliedLevel;
                SearchInfo["EducationLevelID"] = EducationLevelID;
                compatible = this.repository.ExistsRow(GlobalConstants.LIST_SCHEMA, "EducationLevel", SearchInfo);
                if (!compatible)
                {
                    throw new BusinessException("Common_Validate_NotCompatible");
                }
                if (Semester == SystemParamsInFile.SEMESTER_OF_YEAR_SECOND && teachingAssignment.Semester == SystemParamsInFile.SEMESTER_OF_YEAR_FIRST)
                {
                    throw new BusinessException("Common_Validate_NotValidData");
                }
            }
            //Chiendd1: 03/07/2015, bo sung dieu kien partitionId
            int partitionId = UtilsBusiness.GetPartionId(SchoolID);
            List<TeachingAssignment> lstTA = new List<TeachingAssignment>();
            lstTA = (from p in TeachingAssignmentRepository.All
                     where p.AcademicYearID == AcademicYearID
                     && p.TeacherID == TeacherID
                     && p.ClassProfile.EducationLevel.Grade == AppliedLevel
                     && p.Last2digitNumberSchool == partitionId
                     select p).ToList();
            List<TeachingAssignment> lstBySemester = lstTA.Where(o => o.Semester == Semester).ToList();
            List<TeachingAssignment> lstToDelete = new List<TeachingAssignment>();
            foreach (TeachingAssignment ta in lstBySemester)
            {
                if (Semester == SystemParamsInFile.SEMESTER_OF_YEAR_FIRST)
                {
                    List<TeachingAssignment> lst1 = (from p in lstTA where p.SubjectID == ta.SubjectID && p.ClassID == ta.ClassID select p).ToList();
                    foreach (var i in lst1)
                    {
                        lstToDelete.Add(i);
                    }
                }
                else
                {
                    lstToDelete.Add(ta);
                }
            }
            foreach (TeachingAssignment ta in lstToDelete)
            {
                base.Delete(ta.TeachingAssignmentID);

            }
            List<int> lstTeacherID = new List<int>();
            foreach (var i in lstToDelete)
            {
                lstTeacherID.Add(i.TeacherID);
            }
            if (lstTeachingAssignment != null && lstTeachingAssignment.Count() > 0)
            {
                foreach (TeachingAssignment ta in lstTeachingAssignment)
                {
                    TeachingAssignment teachingAssignment = new TeachingAssignment();

                    teachingAssignment = (from p in TeachingAssignmentRepository.All
                                          where p.ClassID == ta.ClassID && p.SubjectID == ta.SubjectID && p.Semester == ta.Semester
                                          && !lstTeacherID.Contains(ta.TeacherID)
                                          && p.Last2digitNumberSchool == partitionId
                                          select p).FirstOrDefault();
                    if (teachingAssignment != null)
                    {
                        throw new BusinessException("TeachingAssignment_Label_ExistTeacherForSubject");
                    }
                    else
                    {
                        ta.TeacherID = TeacherID;
                        ta.SchoolID = SchoolID;
                        ta.AcademicYearID = AcademicYearID;
                        ta.FacultyID = Teacher.SchoolFacultyID;
                        ta.AssignedDate = DateTime.Now;
                        this.Insert(ta);
                    }

                }
            }

        }

        /// <summary>
        /// Tìm kiếm phân công giảng dạy
        /// </summary>
        /// <author>phuongh1</author>
        /// <date>18/4/2013</date>
        /// <returns> IQueryable<TeachingAssignment></returns>
        public IQueryable<TeachingAssignment> Search(IDictionary<string, object> dic)
        {
            int EducationLevel = Utils.GetShort(dic, "EducationLevel");
            int ClassID = Utils.GetInt(dic, "ClassID");
            int AcademicYearID = Utils.GetInt(dic, "AcademicYearID");
            int SubjectID = Utils.GetInt(dic, "SubjectID");
            int AppliedLevel = Utils.GetInt(dic, "AppliedLevel");
            int SchoolID = Utils.GetInt(dic, "SchoolID");
            int FacultyID = Utils.GetInt(dic, "FacultyID");
            int Semester = Utils.GetInt(dic, "Semester");
            int TeacherID = Utils.GetInt(dic, "TeacherID");
            bool? IsActive = Utils.GetIsActive(dic, "IsActive");
            bool removeVNEN = Utils.GetBool(dic, "RemoveVNEN",false);
            List<int> listClassId = Utils.GetIntList(dic, "ListClassIDVNEN");


            var queryTA = this.All;
            if (ClassID != 0)
            {
                queryTA = from ta in queryTA
                          join cp in ClassProfileBusiness.All on ta.ClassID equals cp.ClassProfileID
                          where (!cp.IsActive.HasValue || (cp.IsActive.HasValue && cp.IsActive.Value))
                          && ta.ClassID == ClassID
                          select ta;
            }
            if (EducationLevel != 0)
            {
                queryTA = queryTA.Where(o => o.ClassProfile.EducationLevelID == EducationLevel);
            }
            if (AcademicYearID != 0)
            {
                queryTA = queryTA.Where(o => o.AcademicYearID == AcademicYearID);
            }
            if (SubjectID != 0)
            {
                queryTA = queryTA.Where(o => o.SubjectID == SubjectID);
            }
            if (AppliedLevel != 0)
            {
                queryTA = queryTA.Where(o => o.ClassProfile.EducationLevel.Grade == AppliedLevel);
            }
            if (SchoolID != 0)
            {
                //Chiendd1: 03/07/2015, bo sung dieu kien partitionId
                int partitionId = UtilsBusiness.GetPartionId(SchoolID);
                queryTA = queryTA.Where(o => o.SchoolID == SchoolID && o.Last2digitNumberSchool == partitionId);
            }

            if (FacultyID != 0)
            {
                queryTA = queryTA.Where(o => o.FacultyID == FacultyID);
            }
            if (Semester != 0)
            {
                queryTA = queryTA.Where(o => o.Semester == Semester);
            }
            if (TeacherID != 0)
            {
                queryTA = queryTA.Where(o => o.TeacherID == TeacherID);
            }

            if (IsActive.HasValue)
            {
                queryTA = queryTA.Where(o => o.IsActive == IsActive);
            }
            if (removeVNEN)
            {
                if (listClassId != null && listClassId.Count > 0)
                {
                    queryTA = queryTA.Where(o => o.ClassID.HasValue && listClassId.Contains(o.ClassID.Value));
                }
            }
            else queryTA = queryTA.Where(o => o.IsActive == true);
            return queryTA;

        }

        /// <summary>
        /// Tìm kiếm phân công giảng dạy theo từng trường
        /// </summary>
        /// <author>phuongh1</author>
        /// <date>18/4/2013</date>
        /// <param name="SchoolID">ID trường</param>
        /// <returns>IQueryable<AcademicYear></returns>
        public IQueryable<TeachingAssignment> SearchBySchool(int SchoolID, IDictionary<string, object> dic = null)
        {
            IQueryable<TeachingAssignment> lsTeachingAssignment = null;
            if (SchoolID != 0)
            {
                if (dic == null) dic = new Dictionary<string, object>();
                dic["SchoolID"] = SchoolID;
                lsTeachingAssignment = this.Search(dic);
            }
            return lsTeachingAssignment;
        }

        /// <summary>
        /// Tìm kiếm môn học theo giáo viên, học kỳ
        /// </summary>
        /// <param name="SchoolID"></param>
        /// <param name="dic"></param>
        /// <returns></returns>
        public IQueryable<TeachingAssignmentBO> SearchByTeacher(int schoolID, Dictionary<string, object> dic = null)
        {
            int teacherID = Utils.GetInt(dic, "TeacherID");
            int semester = Utils.GetInt(dic, "Semester");
            int academicYearID = Utils.GetInt(dic, "AcademicYearID");
            int appliedLevel = Utils.GetInt(dic, "AppliedLevel");
            dic["SchoolID"] = schoolID;

            IQueryable<TeachingAssignment> iqTA = this.SearchBySchool(schoolID, dic);
            //thực hiện sắp xếp
            IQueryable<TeachingAssignmentBO> iqResult = (from p in iqTA
                                                         join q in SubjectCatBusiness.All on p.SubjectID equals q.SubjectCatID
                                                         join r in ClassSubjectBusiness.SearchBySchool(schoolID) on p.ClassID equals r.ClassID
                                                         where p.IsActive == true && q.IsActive == true && q.AppliedLevel == appliedLevel && p.SubjectID == r.SubjectID && ((semester == 1 && r.SectionPerWeekFirstSemester > 0) || (semester == 2 && r.SectionPerWeekSecondSemester > 0))
                                                         select new TeachingAssignmentBO
                                                         {
                                                             SubjectID = q.SubjectCatID,
                                                             SubjectName = q.DisplayName,
                                                             OrderInSubject = q.OrderInSubject,
                                                             TeacherID = p.TeacherID
                                                         }).Distinct();
            return iqResult;
        }

        /// <summary>
        /// Phân công giảng dạy theo giáo viên
        /// Author: phuong1
        /// Date: 18/4/2013
        /// </summary>
        /// <param name="SchoolID"></param>
        /// <param name="AcademicYearID"></param>
        /// <param name="AppliedLevel"></param>
        /// <param name="TeacherID"></param>
        /// <param name="SubjectID"></param>
        /// <param name="lstTeachingAssignment"></param>
        /// <returns></returns>
        public void UpdateForSubjectAndEmployee(int SchoolID, int AcademicYearID, int AppliedLevel,
            int TeacherID, int SubjectID, List<TeachingAssignment> lstTeachingAssignment)
        {
            // Kiểm tra tính hợp lệ dữ liệu: năm học - trường học
            CheckCompatiable(SchoolID.ToString(), AcademicYearID.ToString(),
                    GlobalConstants.SCHOOL_SCHEMA, "AcademicYear", "SchoolID", "AcademicYearID");
            //Chiendd1: 03/07/2015, bo sung dieu kien partitionId
            int partitionId = UtilsBusiness.GetPartionId(SchoolID);
            // Kiểm tra tính hợp lệ dữ liệu: giáo viên - trường học
            Employee employee = EmployeeBusiness.Find(TeacherID);
            if (employee.SchoolID.GetValueOrDefault() != SchoolID)
            {
                throw new BusinessException("Common_Validate_NotCompatible");
            }

            //Kiểm tra trạng thái đang làm việc
            if (employee.EmploymentStatus != SystemParamsInFile.EMPLOYMENT_STATUS_WORKING)
            {
                throw new BusinessException("TeachingAssignment_Validate_TeacherNotWorking");
            }

            //Năm học không phải năm hiện tại
            if (!AcademicYearBusiness.IsCurrentYear(AcademicYearID))
            {
                throw new BusinessException("Common_Validate_NotCurrentYear");
            }

            //Kiểm tra dữ liệu trong lstTeachingAssignment
            List<int> listClass = ClassSubjectBusiness.SearchBySchool(SchoolID, new Dictionary<string, object>
            {
                {"AcademicYearID", AcademicYearID},
                {"AppliedLevel", AppliedLevel},
                {"SubjectID", SubjectID}
            }).Select(x => x.ClassID).ToList();
            if (lstTeachingAssignment.Exists(x => !listClass.Contains(x.ClassID.GetValueOrDefault())))
            {
                throw new BusinessException("Common_Validate_NotCompatible");
            }

            List<int> listSemester = new List<int> { 
                SystemParamsInFile.SEMESTER_OF_YEAR_FIRST, 
                SystemParamsInFile.SEMESTER_OF_YEAR_SECOND 
            };
            if (lstTeachingAssignment.Exists(x => !listSemester.Contains(x.Semester.GetValueOrDefault())))
            {
                throw new BusinessException("Common_Validate_NotCompatible");
            }

            //Kiểm tra xem đã qua học kỳ 1 chưa
            bool isSecondSemester = AcademicYearBusiness.IsSecondSemesterTime(AcademicYearID);
            //Xoá dữ liệu cũ
            Dictionary<string, object> searchInfo = new Dictionary<string, object>
            {
                {"AcademicYearID", AcademicYearID},
                {"AppliedLevel", AppliedLevel},
                {"SubjectID", SubjectID},
                {"TeacherID", TeacherID}
            };
            if (isSecondSemester)
            {
                searchInfo["Semester"] = SystemParamsInFile.SEMESTER_OF_YEAR_SECOND;
            }
            List<TeachingAssignment> listToDelete = this.SearchBySchool(SchoolID, searchInfo).ToList();
            TeachingAssignmentRepository.DeleteAll(listToDelete);

            //Insert dữ liệu mới
            List<TeachingAssignment> listToInsert = lstTeachingAssignment;
            if (isSecondSemester)
            {
                listToInsert = lstTeachingAssignment.FindAll(x => x.Semester == SystemParamsInFile.SEMESTER_OF_YEAR_SECOND);
            }
            foreach (TeachingAssignment ta in listToInsert)
            {
                ta.AcademicYearID = AcademicYearID;
                ta.AssignedDate = DateTime.Now;
                ta.FacultyID = employee.SchoolFacultyID;
                ta.SchoolID = SchoolID;
                ta.SubjectID = SubjectID;
                ta.TeacherID = TeacherID;
                ta.IsActive = true;
                ta.Last2digitNumberSchool = partitionId;
                TeachingAssignmentRepository.Insert(ta);
            }
            TeachingAssignmentRepository.Save();
        }

        /// <summary>
        /// QuangLM1
        /// Lay ra danh sach giao vien va so tiet day tuong ung trong hoc ky I va II
        /// </summary>
        /// <param name="SchoolID"></param>
        /// <param name="AcademicYearID"></param>
        /// <param name="AppliedLevel"></param>
        /// <returns></returns>
        public Dictionary<int, string> GetSectionPerWeekInYear(int SchoolID, int AcademicYearID, int AppliedLevel)
        {
            Dictionary<int, string> dicTA = new Dictionary<int, string>();
            Dictionary<string, object> searchInfo = new Dictionary<string, object>
            {
                {"AcademicYearID", AcademicYearID},
                {"AppliedLevel", AppliedLevel},
            };
            IQueryable<TeachingAssignment> listTA = this.SearchBySchool(SchoolID, searchInfo).Where(o => o.Semester == SystemParamsInFile.SEMESTER_OF_YEAR_FIRST
                || o.Semester == SystemParamsInFile.SEMESTER_OF_YEAR_SECOND);
            var listSPW = (from ta in listTA
                           join cs in ClassSubjectBusiness.SearchBySchool(SchoolID)
                           on ta.ClassID equals cs.ClassID
                           where ta.SubjectID == cs.SubjectID
                           select new { ta.TeacherID, ta.Semester, cs.SectionPerWeekFirstSemester, cs.SectionPerWeekSecondSemester }).ToList();

            // Group theo teacherID de lay ra danh sach giao vien va so tiet day tuong ung
            var listSPWF = listSPW.GroupBy(o => o.TeacherID).Select(o =>
                new
                {
                    TeacherID = o.Key,
                    SectionPerWeekFirstSemester = o.Sum(u => u.Semester == SystemParamsInFile.SEMESTER_OF_YEAR_FIRST ? u.SectionPerWeekFirstSemester : 0),
                    SectionPerWeekSecondSemester = o.Sum(u => u.Semester == SystemParamsInFile.SEMESTER_OF_YEAR_SECOND ? u.SectionPerWeekSecondSemester : 0)
                });
            foreach (var item in listSPWF)
            {
                dicTA[item.TeacherID] = item.SectionPerWeekFirstSemester.Value + "_" + item.SectionPerWeekSecondSemester.Value;
            }

            return dicTA;
        }


        /// <summary>
        /// Lấy số tiết / tuần theo 1 học kỳ
        /// Author: phuong1
        /// Date: 18/4/2013
        /// </summary>
        public decimal GetSectionPerWeek(int SchoolID, int AcademicYearID, int AppliedLevel,
            int TeacherID, int SubjectID, int Semester)
        {
            decimal result = 0;
            Dictionary<string, object> searchInfo = new Dictionary<string, object>
            {
                {"AcademicYearID", AcademicYearID},
                {"AppliedLevel", AppliedLevel},
                {"SubjectID", SubjectID},
                {"TeacherID", TeacherID},
                {"Semester", Semester}
            };
            IQueryable<TeachingAssignment> listTA = this.SearchBySchool(SchoolID, searchInfo);
            List<ClassSubject> listSPW = (from ta in listTA
                                          join cs in ClassSubjectBusiness.SearchBySchool(SchoolID)
                                          on ta.ClassID equals cs.ClassID
                                          where ta.SubjectID == cs.SubjectID
                                          select cs).ToList();
            if (Semester == SystemParamsInFile.SEMESTER_OF_YEAR_FIRST)
            {
                result = listSPW.Sum(x => x.SectionPerWeekFirstSemester).GetValueOrDefault();
            }
            else
            {
                result = listSPW.Sum(x => x.SectionPerWeekSecondSemester).GetValueOrDefault();
            }
            return result;
        }

        public decimal GetSectionPerClass(int ClassID, int SubjectID, int Semester)
        {
            decimal result = 0;
            ClassSubject classSubject = ClassSubjectRepository.All.Where(o => o.ClassID == ClassID && o.SubjectID == SubjectID).FirstOrDefault();
            if (Semester == SystemParamsInFile.SEMESTER_OF_YEAR_FIRST)
            {
                result = classSubject.SectionPerWeekFirstSemester.GetValueOrDefault();
            }
            else
            {
                result = classSubject.SectionPerWeekSecondSemester.GetValueOrDefault();
            }
            return result;
        }

        /// <summary>
        /// Lay danh sach lop hoc va tong so tiet tren tuan tuong ung theo mon hoc
        /// </summary>
        /// <param name="SubjectID"></param>
        /// <returns></returns>
        public Dictionary<int, string> GetSectionPerClassBySubjectInYear(int SchoolID, int AcademicYearID, int SubjectID)
        {
            int parrtitionId = UtilsBusiness.GetPartionId(SchoolID);
            Dictionary<int, string> dicCS = new Dictionary<int, string>();
            var listClassSubject = ClassSubjectRepository.All.Where(o =>
                o.ClassProfile.SchoolID == SchoolID &&
                o.ClassProfile.AcademicYearID == AcademicYearID &&
                o.SubjectID == SubjectID
                && o.Last2digitNumberSchool == parrtitionId
                ).GroupBy(o => o.ClassID)
            .Select(o => new
            {
                ClassID = o.Key,
                SectionPerWeekFirstSemester = o.Sum(u => u.SectionPerWeekFirstSemester),
                SectionPerWeekSecondSemester = o.Sum(u => u.SectionPerWeekSecondSemester)
            });
            foreach (var item in listClassSubject)
            {
                dicCS[item.ClassID] = item.SectionPerWeekFirstSemester.Value + "_" + item.SectionPerWeekSecondSemester.Value;
            }
            return dicCS;
        }

        /// <summary>
        /// 23/04/2014
        /// Quanglm1
        /// Insert hoac update danh sach phan cong giang day
        /// </summary>
        /// <param name="SchoolID"></param>
        /// <param name="AcademicYearID"></param>
        /// <param name="listTeachingAssignment"></param>
        public void InsertOrUpdateListTeachingAssignment(int SchoolID, int AcademicYearID, List<TeachingAssignment> listTeachingAssignment)
        {
            if (listTeachingAssignment == null || listTeachingAssignment.Count == 0)
            {
                return;
            }
            #region Kiem tra du lieu dau vao
            // Kiem tra nam hoc hien tai
            // Kiểm tra có phải năm hiện tại không
            bool isCurrentYear = this.AcademicYearBusiness.IsCurrentYear(AcademicYearID);
            if (!isCurrentYear)
            {
                throw new BusinessException("Common_Validate_NotCurrentYear");
            }
            List<int> listTeacherID = listTeachingAssignment.Select(o => o.TeacherID).Distinct().ToList();
            List<int> listClassID = listTeachingAssignment.Where(o => o.ClassID.HasValue).Select(o => o.ClassID.Value).Distinct().ToList();
            // Kiem tra giao vien phai thuoc truong va co trang thai dang day
            int countEmployeeWorking = EmployeeBusiness.AllNoTracking.Where(o => o.SchoolID == SchoolID && o.EmploymentStatus == SystemParamsInFile.EMPLOYMENT_STATUS_WORKING
                && listTeacherID.Contains(o.EmployeeID)).Count();
            if (listTeacherID.Count != countEmployeeWorking)
            {
                throw new BusinessException("TeachingAssignment_Label_EmployeeNotLegal");
            }
            // Kiem tra lop hoc phai thuoc truong va nam hoc tuong ung
            int countClass = ClassProfileBusiness.AllNoTracking.Where(o => o.SchoolID == SchoolID && o.AcademicYearID == AcademicYearID
                && listClassID.Contains(o.ClassProfileID) && o.IsActive==true).Count();
            if (listClassID.Count != countClass)
            {
                throw new BusinessException("Common_Validate_NotCompatible");
            }
            // Khong can kiem tra khong phan cong trung du lieu nua vi o controller da kiem tra roi
            // Thuc hien insert hoac update du lieu
            this.context.Configuration.AutoDetectChangesEnabled = false;
            this.context.Configuration.ValidateOnSaveEnabled = false;
            int count = listTeachingAssignment.Count;
            for (int i = 0; i < count; i++)
            {
                TeachingAssignment ta = listTeachingAssignment[i];
                if (ta.TeachingAssignmentID == 0)
                {
                    base.Insert(ta);
                }
                else
                {
                    base.Update(ta);
                }
            }
            this.context.Configuration.AutoDetectChangesEnabled = true;
            this.context.Configuration.ValidateOnSaveEnabled = true;
            #endregion
        }

        /// <summary>
        /// Xóa danh sách phân công giáo vụ (chỉ hủy kích hoạt, ko xóa hẳn khỏi DB)
        /// </summary>
        /// <author>AnhVD9 20140924</author>
        /// <param name="SchoolID"></param>
        /// <param name="TeacherID"></param>
        public void Delete(Dictionary<string, object> SearchInfo)
        {
            var LstTeacherAssignment = this.Search(SearchInfo).ToList();
            int Count = 0;
            if (LstTeacherAssignment != null && (Count = LstTeacherAssignment.Count) > 0)
            {
                TeachingAssignment entity;
                for (int i = 0; i < Count; i++)
                {
                    entity = LstTeacherAssignment[i];
                    base.Delete(entity.TeachingAssignmentID, true); // Deactive cả các năm học cũ - Ko xóa
                }
                this.TeacherGradeBusiness.Save();
            }
        }

        public List<int> GetListClassIdNotVNEN(int subjectId, List<int> listClassId)
        {
            List<int> listResult = (from a in ClassSubjectBusiness.All
                                    where listClassId.Contains(a.ClassID)
                                    && a.SubjectID == subjectId
                                    && ((a.IsSubjectVNEN.HasValue && a.IsSubjectVNEN.Value == false) || !a.IsSubjectVNEN.HasValue)
                                    select a.ClassID).Distinct().ToList();
            return listResult;
        }

        #region Hàm không dùng chung
        /// <summary>
        /// Lấy danh sách phân công giảng dạy của giáo viên có kết bảng
        /// </summary>
        /// <author>Anhnph1 20160622</author>
        /// <param name="lstTeachingAssignment"></param>
        /// <returns></returns>
        public List<TeachingAssignmentBO> GetTeachingAssignmentbyAcademicYear(int schoolId, int academicYearId, int applicationLevel)
        {

            IDictionary<string, object> dicSearch = new Dictionary<string, object>()
            {
                {"AcademicYearID", academicYearId},
                {"AppliedLevel", applicationLevel},
                {"IsActive", true}
            };

            //Lay danh sach mon hoc cho lop

            List<TeachingAssignmentBO> lstTABo = (from ta in TeachingAssignmentBusiness.SearchBySchool(schoolId, dicSearch)                                                  
                                                  join sc in SubjectCatBusiness.All.Where(s => s.AppliedLevel == applicationLevel) on ta.SubjectID equals sc.SubjectCatID
                                                  //join ep in EmployeeBusiness.All on ta.TeacherID equals ep.EmployeeID
                                                  join cp in ClassProfileBusiness.SearchByAcademicYear(academicYearId, dicSearch) on ta.ClassID equals cp.ClassProfileID
                                                  //join sf in SchoolFacultyBusiness.All.Where(s => s.SchoolID == schoolId && s.IsActive == true) on ta.FacultyID equals sf.SchoolFacultyID
                                                  select new TeachingAssignmentBO
                                                  {
                                                      TeachingAssignmentID = ta.TeachingAssignmentID,
                                                      TeacherID = ta.TeacherID,
                                                      FacultyID = ta.FacultyID,
                                                      //FacultyName = sf.FacultyName,
                                                      Semester = ta.Semester,
                                                      SubjectID = ta.SubjectID,
                                                      SubjectName = sc.SubjectName,
                                                      SubjcetAbbreviation = sc.Abbreviation,
                                                      ClassID = ta.ClassID,
                                                      ClassName = cp.DisplayName,
                                                      SchoolID = ta.SchoolID,
                                                      IsActive = ta.IsActive,
                                                      OrderInSubject=sc.OrderInSubject
                                                  }).ToList();
            return lstTABo;
        }



        #endregion Hàm không dùng chung
    }
}
