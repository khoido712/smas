﻿/**
 * The Viettel License
 *
 * Copyright 2012 Viettel Telecom. All rights reserved.
 * VIETTEL PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */

/** 
* @author  
* @version $Revision: $
*/

using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Linq.Expressions;
using System.Web;
using log4net;

using SMAS.DAL.Repository;
using SMAS.DAL.IRepository;
using SMAS.Business.IBusiness;
using SMAS.Models.Models;
using SMAS.Business.Common;

namespace SMAS.Business.Business
{ 
    public partial class PolicyRegimeBusiness
    {

        #region private member variable
        private const int RESOLUTION_MAX_LENGTH = 50;   //do dai lon nhat truong ten
        private const int DESCRIPTION_MAX_LENGTH = 400; //do dai lon nhat truong mieu ta
        #endregion



        #region insert
        /// <summary>
        /// Thêm mới Chế độ chính sách
        /// <author>tamhm1</author>
        /// <date>28/12/2012</date>
        /// </summary>
        /// <param name="insertPolicyRegime">Đối tượng Insert</param>
        /// <returns>Đối tượng sau khi được insert vào DB thành công</returns>
        public override PolicyRegime Insert(PolicyRegime insertPolicyRegime)
        {


            //resolution rong
            Utils.ValidateRequire(insertPolicyRegime.Resolution, "PolicyRegime_Label_Resolution");

            Utils.ValidateMaxLength(insertPolicyRegime.Resolution, RESOLUTION_MAX_LENGTH, "PolicyRegime_Label_Resolution");
            Utils.ValidateMaxLength(insertPolicyRegime.Description, DESCRIPTION_MAX_LENGTH, "PolicyRegime_Column_Description");

            //kiem tra ton tai - theo cap - can lam lai
            this.CheckDuplicate(insertPolicyRegime.Resolution, GlobalConstants.LIST_SCHEMA, "PolicyRegime", "Resolution", true, 0, "PolicyRegime_Label_Resolution");
         
            insertPolicyRegime.IsActive = true;

            return base.Insert(insertPolicyRegime);
        }
        #endregion

        #region update
        /// <summary>
        /// Cập nhật Chế độ chính sách
        /// <author>tamhm1</author>
        /// <date>28/12/2012</date>
        /// </summary>
        /// <param name="updatePolicyRegime">Đối tượng Update</param>
        /// <returns>Đối tượng sau khi được Update vào DB thành công</returns>
        public override PolicyRegime Update(PolicyRegime updatePolicyRegime)
        {

            //check avai
            new PolicyRegimeBusiness(null).CheckAvailable(updatePolicyRegime.PolicyRegimeID, "PolicyRegime_Label_PolicyRegimeID");

            //resolution rong
            Utils.ValidateRequire(updatePolicyRegime.Resolution, "PolicyRegime_Label_Resolution");

            Utils.ValidateMaxLength(updatePolicyRegime.Resolution, RESOLUTION_MAX_LENGTH, "PolicyRegime_Label_Resolution");
            Utils.ValidateMaxLength(updatePolicyRegime.Description, DESCRIPTION_MAX_LENGTH, "PolicyRegime_Column_Description");

            //kiem tra ton tai - theo cap - can lam lai
            this.CheckDuplicate(updatePolicyRegime.Resolution, GlobalConstants.LIST_SCHEMA, "PolicyRegime", "Resolution", true, updatePolicyRegime.PolicyRegimeID, "PolicyRegime_Label_Resolution");

            updatePolicyRegime.IsActive = true;

            return base.Update(updatePolicyRegime);
        }
        #endregion

        #region Delete
        /// <summary>
        /// Xóa Chế độ chính sách
        /// <author>tamhm1</author>
        /// <date>28/12/2012</date>
        /// </summary>
        /// <param name="PolicyRegimeId">ID Chế độ chính sách</param>
        public void Delete(int PolicyRegimeId)
        {
            //check avai
            new PolicyRegimeBusiness(null).CheckAvailable(PolicyRegimeId, "PolicyRegime_Label_PolicyRegimeID");

            //da su dung hay chua
            this.CheckConstraints(GlobalConstants.LIST_SCHEMA, "PolicyRegime", PolicyRegimeId, "PolicyRegime_Label_PolicyRegimeID");

            base.Delete(PolicyRegimeId, true);


        }
        #endregion

        #region Search
        /// <summary>
        /// Tìm kiếm Chế độ chính sách
        /// <author>tamhm1</author>
        /// <date>28/12/2012</date>
        /// </summary>
        /// <param name="dic">tham so doi tuong tim kiem</param>
        /// <returns>Danh sách kết quả tìm kiếm</returns>
        public IQueryable<PolicyRegime> Search(IDictionary<string, object> dic)
        {
            string resolution = Utils.GetString(dic, "Resolution");
            string description = Utils.GetString(dic, "Description");

            bool? isActive = Utils.GetIsActive(dic, "IsActive");
            IQueryable<PolicyRegime> lsPolicyRegime = this.PolicyRegimeRepository.All.OrderBy(o => o.Resolution);

            if (isActive.HasValue)
            {
                lsPolicyRegime = lsPolicyRegime.Where(em => (em.IsActive == isActive));
            }

            if (resolution.Trim().Length != 0)
            {

                lsPolicyRegime = lsPolicyRegime.Where(em => (em.Resolution.ToUpper().Contains(resolution.ToUpper())));
            }
            if (description.Trim().Length != 0)
            {

                lsPolicyRegime = lsPolicyRegime.Where(em => (em.Description.ToUpper().Contains(description.ToUpper())));
            }


            return lsPolicyRegime;
        }
        #endregion
    }
}