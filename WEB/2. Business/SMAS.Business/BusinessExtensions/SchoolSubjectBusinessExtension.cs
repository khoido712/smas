﻿/**
 * The Viettel License
 *
 * Copyright 2012 Viettel Telecom. All rights reserved.
 * VIETTEL PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */

/** 
* @author  
* @version $Revision: $
*/

using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Linq.Expressions;
using System.Web;
using log4net;

using SMAS.DAL.Repository;
using SMAS.DAL.IRepository;
using SMAS.Business.IBusiness;
using SMAS.Models.Models;
using SMAS.Business.Common;
using SMAS.Business.BusinessObject;

namespace SMAS.Business.Business
{
    public partial class SchoolSubjectBusiness
    {
        #region Them moi du lieu
        /// <summary>
        /// Them moi mon hoc cua truong vao CSDL
        /// </summary>
        /// <param name="ListSchoolSubject"></param>
        public void Insert(List<SchoolSubject> ListSchoolSubject)
        {
            if (ListSchoolSubject == null || ListSchoolSubject.Count() == 0)
            {
                throw new BusinessException("SchoolSubject_Err_EmptyList");
            }
            SchoolSubject shoolSubject = ListSchoolSubject[0];

            if (shoolSubject.SchoolID == 0)
            {
                Utils.ValidateRequire(string.Empty, "SchoolProfile_Label_SchoolProfileID");
            }
            AcademicYearBusiness.CheckAvailable(shoolSubject.AcademicYearID, "AcademicYear_Label_Year");
            SchoolProfileBusiness.CheckAvailable(shoolSubject.SchoolID, "SchoolProfile_Label_SchoolProfileID");
            // Kiem tra mon hoc da ton tai trong he thong
            List<int> listSubjectID = ListSchoolSubject.Select(o => o.SubjectID).Distinct().ToList();
            int countSubject = SubjectCatBusiness.All.Where(o => listSubjectID.Contains(o.SubjectCatID) && o.IsActive).Count();
            if (listSubjectID.Count != countSubject)
            {
                List<object> Params = new List<object>();
                Params.Add("SubjectCat_Label_SubjectCatID");
                throw new BusinessException("Common_Validate_NotAvailable", Params);
            }
            #region Kiem tra du lieu trong danh sach mon hoc cua truong hoc
            List<int> listEducationID = new List<int>();
            bool isPrimaryGrade = false;
            foreach (SchoolSubject SchoolSubject in ListSchoolSubject)
            {
                #region kiem tra thua = Bỏ
                //DateTime? StartDate = SchoolSubject.StartDate;
                //DateTime? EndDate = SchoolSubject.EndDate;
                //DateTime? FirstSemesterStartDate = SchoolSubject.AcademicYear == null ? null : SchoolSubject.AcademicYear.FirstSemesterStartDate;
                //DateTime? SecondSemesterEndDate = SchoolSubject.AcademicYear == null ? null : SchoolSubject.AcademicYear.SecondSemesterEndDate;
                //if (FirstSemesterStartDate.HasValue && SecondSemesterEndDate.HasValue)
                //{
                //    // Ngay bat dau
                //    if (StartDate.HasValue)
                //    {
                //        if ((StartDate.Value < FirstSemesterStartDate)
                //            || (StartDate.Value > SecondSemesterEndDate.Value))
                //        {
                //            throw new BusinessException("SchoolSubject_Err_StartDate");
                //        }
                //    }
                //    // Ngay ket thuc
                //    if (EndDate.HasValue)
                //    {
                //        if ((EndDate.Value < FirstSemesterStartDate)
                //            || (StartDate.Value > SecondSemesterEndDate.Value))
                //        {
                //            throw new BusinessException("SchoolSubject_Err_EndDate");
                //        }
                //    }
                //}
                // Kiem tra ngay bat dau va ngay ket thuc                
                //Utils.ValidateAfterDate(StartDate, EndDate, "Common_Label_StartDate", "Common_Label_EndDate"); 
                #endregion

                // QuangLM sua lai check de tang hieu nang                
                if (SchoolSubject.EducationLevelID.HasValue && !listEducationID.Contains(SchoolSubject.EducationLevelID.Value))
                {
                    listEducationID.Add(SchoolSubject.EducationLevelID.Value);
                    isPrimaryGrade = SchoolSubject.EducationLevel != null && SchoolSubject.EducationLevel.Grade == SystemParamsInFile.APPLIED_LEVEL_PRIMARY;
                }
                // Kiem tra cac truong cap 1 thi khong co he so cho tung hoc ky
                if (isPrimaryGrade)
                {
                    if (SchoolSubject.FirstSemesterCoefficient > 0 || SchoolSubject.SecondSemesterCoefficient > 0)
                    {
                        throw new BusinessException("SchoolSubject_Err_PrimaryEducation");
                    }
                }
                else
                {
                    // Kiem tra he so hoc ky cua cac truong tren cap 1 phai la so duong
                    if (SchoolSubject.FirstSemesterCoefficient < 0)
                    {
                        throw new BusinessException("SchoolSubject_Err_FirstSemesterCoefficient");
                    }
                    if (SchoolSubject.SecondSemesterCoefficient < 0)
                    {
                        throw new BusinessException("SchoolSubject_Err_SecondSemesterCoefficient");
                    }
                }
            }
            #endregion

            #region Them moi vao CSDL
            foreach (SchoolSubject SchoolSubject in ListSchoolSubject)
            {
                base.Insert(SchoolSubject);
            }
            #endregion
        }
        #endregion

        #region Cap nhat du lieu
        /// <summary>
        /// Cap nhat mon hoc cua truong vao CSDL
        /// </summary>
        /// <param name="ListSchoolSubject"></param>
        public void Update(List<SchoolSubject> ListSchoolSubject)
        {
            if (ListSchoolSubject == null || ListSchoolSubject.Count() == 0)
            {
                throw new BusinessException("SchoolSubject_Err_EmptyList");
            }
            SchoolSubject shoolSubject = ListSchoolSubject[0];

            if (shoolSubject.SchoolID == 0)
            {
                Utils.ValidateRequire(string.Empty, "SchoolProfile_Label_SchoolProfileID");
            }
            AcademicYearBusiness.CheckAvailable(shoolSubject.AcademicYearID, "AcademicYear_Label_Year");
            SchoolProfileBusiness.CheckAvailable(shoolSubject.SchoolID, "SchoolProfile_Label_SchoolProfileID");
            // Kiem tra mon hoc da ton tai trong he thong
            List<int> listSubjectID = ListSchoolSubject.Select(o => o.SubjectID).Distinct().ToList();
            int countSubject = SubjectCatBusiness.All.Where(o => listSubjectID.Contains(o.SubjectCatID) && o.IsActive).Count();
            if (listSubjectID.Count != countSubject)
            {
                List<object> Params = new List<object>();
                Params.Add("SubjectCat_Label_SubjectCatID");
                throw new BusinessException("Common_Validate_NotAvailable", Params);
            }
            #region Kiem tra du lieu trong danh sach mon hoc cua truong hoc
            List<int> listEducationID = new List<int>();
            bool isPrimaryGrade = false;
            foreach (SchoolSubject SchoolSubject in ListSchoolSubject)
            {
                #region kiem tra thua = Bỏ
                //DateTime? StartDate = SchoolSubject.StartDate;
                //DateTime? EndDate = SchoolSubject.EndDate;
                //DateTime? FirstSemesterStartDate = SchoolSubject.AcademicYear == null ? null : SchoolSubject.AcademicYear.FirstSemesterStartDate;
                //DateTime? SecondSemesterEndDate = SchoolSubject.AcademicYear == null ? null : SchoolSubject.AcademicYear.SecondSemesterEndDate;
                //if (FirstSemesterStartDate.HasValue && SecondSemesterEndDate.HasValue)
                //{
                //    // Ngay bat dau
                //    if (StartDate.HasValue)
                //    {
                //        if ((StartDate.Value < FirstSemesterStartDate)
                //            || (StartDate.Value > SecondSemesterEndDate.Value))
                //        {
                //            throw new BusinessException("SchoolSubject_Err_StartDate");
                //        }
                //    }
                //    // Ngay ket thuc
                //    if (EndDate.HasValue)
                //    {
                //        if ((EndDate.Value < FirstSemesterStartDate)
                //            || (StartDate.Value > SecondSemesterEndDate.Value))
                //        {
                //            throw new BusinessException("SchoolSubject_Err_EndDate");
                //        }
                //    }
                //}
                // Kiem tra ngay bat dau va ngay ket thuc                
                //Utils.ValidateAfterDate(StartDate, EndDate, "Common_Label_StartDate", "Common_Label_EndDate"); 
                #endregion

                // QuangLM sua lai check de tang hieu nang                
                if (SchoolSubject.EducationLevelID.HasValue && !listEducationID.Contains(SchoolSubject.EducationLevelID.Value))
                {
                    listEducationID.Add(SchoolSubject.EducationLevelID.Value);
                    isPrimaryGrade = SchoolSubject.EducationLevel != null && SchoolSubject.EducationLevel.Grade == SystemParamsInFile.APPLIED_LEVEL_PRIMARY;
                }
                // Kiem tra cac truong cap 1 thi khong co he so cho tung hoc ky
                if (isPrimaryGrade)
                {
                    if (SchoolSubject.FirstSemesterCoefficient > 0 || SchoolSubject.SecondSemesterCoefficient > 0)
                    {
                        throw new BusinessException("SchoolSubject_Err_PrimaryEducation");
                    }
                }
                else
                {
                    // Kiem tra he so hoc ky cua cac truong tren cap 1 phai la so duong
                    if (SchoolSubject.FirstSemesterCoefficient < 0)
                    {
                        throw new BusinessException("SchoolSubject_Err_FirstSemesterCoefficient");
                    }
                    if (SchoolSubject.SecondSemesterCoefficient < 0)
                    {
                        throw new BusinessException("SchoolSubject_Err_SecondSemesterCoefficient");
                    }
                }
            }
            #endregion

            #region Cap nhat vao CSDL
            this.context.Configuration.AutoDetectChangesEnabled = false;
            foreach (SchoolSubject SchoolSubject in ListSchoolSubject)
            {
                base.Update(SchoolSubject);
            }
            this.context.Configuration.AutoDetectChangesEnabled = true;
            #endregion
        }
        #endregion

        #region Tim kiem
        /// <summary>
        /// Tim kiem mon hoc trong truong
        /// </summary>
        /// <param name="SearchInfo"></param>
        /// <returns></returns>
        public IQueryable<SchoolSubject> Search(IDictionary<string, object> SearchInfo)
        {
            int AcademicYearID = Utils.GetInt(SearchInfo, "AcademicYearID");
            int EducationLevelID = Utils.GetInt(SearchInfo, "EducationLevelID");
            int AppliedType = Utils.GetInt(SearchInfo, "AppliedType");
            int SchoolID = Utils.GetInt(SearchInfo, "SchoolID");
            int SubjectID = Utils.GetInt(SearchInfo, "SubjectID");
            int? IsCommenting = Utils.GetNullableInt(SearchInfo, "IsCommenting");
            int AppliedLevel = Utils.GetInt(SearchInfo, "AppliedLevel");
            List<int> Comment = Utils.GetIntList(SearchInfo, "Comment");

            IQueryable<SchoolSubject> lsSchoolSubject = this.SchoolSubjectRepository.All.Where(x => x.SubjectCat.IsActive == true);

            if (AcademicYearID != 0)
            {
                lsSchoolSubject = lsSchoolSubject.Where(x => x.AcademicYearID == AcademicYearID);
            }
            if (AppliedType != 0)
            {
                lsSchoolSubject = lsSchoolSubject.Where(x => x.AppliedType == AppliedType);
            }
            if (SchoolID != 0)
            {
                lsSchoolSubject = lsSchoolSubject.Where(x => x.SchoolID == SchoolID);
            }
            if (SubjectID != 0)
            {
                lsSchoolSubject = lsSchoolSubject.Where(x => x.SubjectID == SubjectID);
            }
            if (IsCommenting.HasValue)
            {
                lsSchoolSubject = lsSchoolSubject.Where(x => x.IsCommenting == IsCommenting);
            }
            if (Comment.Count > 0)
            {
                lsSchoolSubject = lsSchoolSubject.Where(o => Comment.Contains(o.IsCommenting.Value));
            }
            if (EducationLevelID != 0)
            {
                lsSchoolSubject = lsSchoolSubject.Where(x => x.EducationLevelID == EducationLevelID);
            }
            if (AppliedLevel != 0)
            {
                lsSchoolSubject = lsSchoolSubject.Where(x => x.EducationLevel.Grade == AppliedLevel);
                lsSchoolSubject = lsSchoolSubject.Where(a => a.SubjectCat.AppliedLevel == AppliedLevel);
            }
            return lsSchoolSubject;
        }

        public IQueryable<SchoolSubject> SearchBySchool(int SchoolID, IDictionary<string, object> SearchInfo = null)
        {
            if (SchoolID == 0)
            {
                return null;
            }
            if (SearchInfo == null)
            {
                SearchInfo = new Dictionary<string, object>();
            }
            SearchInfo["SchoolID"] = SchoolID;
            return Search(SearchInfo);
        }

        public IQueryable<SchoolSubject> SearchByAcademicYear(int AcademicYearID, IDictionary<string, object> SearchInfo)
        {
            if (AcademicYearID == 0)
            {
                return null;
            }
            SearchInfo["AcademicYearID"] = AcademicYearID;
            return Search(SearchInfo);
        }
        #endregion

        #region Xoa
        public void Delete(List<SchoolSubject> ListSchoolSubject, int AcademicYearID)
        {
            if (ListSchoolSubject == null || ListSchoolSubject.Count() == 0)
            {
                return;
            }
            List<int> lstSubjectID = ListSchoolSubject.Select(o => o.SubjectID).ToList();
            // Kiem tra neu mon hoc da duoc gan cho lop thi khong cho phep xoa
            SchoolSubject shoolSubject = ListSchoolSubject[0];
            Dictionary<string, object> dic = new Dictionary<string, object>();
            dic["EducationLevelID"] = shoolSubject.EducationLevelID;
            dic["AcademicYearID"] = AcademicYearID;
            IQueryable<ClassSubject> ListClassSubject = ClassSubjectBusiness.SearchBySchool(shoolSubject.SchoolID, dic).Where(o => lstSubjectID.Contains(o.SubjectID));
            if (ListClassSubject != null && ListClassSubject.Count() > 0)
            {
                throw new BusinessException("SchoolSubject_Label_ErrClassSubject");
            }
            #region check dieu kien ko can thiet - Bỏ
            //foreach (SchoolSubject SchoolSubject in ListSchoolSubject)
            //{
            //    if (SchoolSubject.SchoolSubjectID == 0)
            //    {
            //        Utils.ValidateRequire(string.Empty, "SchoolSubject_Label_AllTitle");
            //    }
            //    new SchoolSubjectBusiness(null).CheckAvailable(SchoolSubject.SchoolSubjectID, "SchoolSubject_Label_AllTitle");
            //    // Kiem tra thong tin nam hoc
            //    // ko can thiet check
            //    //if (SchoolSubject.AcademicYearID != AcademicYearID)
            //    //{
            //    //    throw new BusinessException("SchoolSubject_Label_AcademicYearID_Err");
            //    //} 
            //} 
            #endregion
            base.DeleteAll(ListSchoolSubject);
        }
        #endregion

        #region Kiem tra rang buoc du lieu
        public bool CheckSchoolSubjectContraints(int SchoolSubjectID)
        {
            try
            {
                this.CheckConstraints(GlobalConstants.SCHOOL_SCHEMA, "SchoolSubject", SchoolSubjectID, null);
                return true;
            }
            catch
            {
                return false;
            }
        }
        #endregion

        #region Loc danh sach mon nhan tinh diem tu list danh sach mon thi
        public List<SubjectCat> GetListSubjectMarkByListSubjectID(List<int> subjectListID)
        {
            List<SubjectCat> listResult = (from s in SubjectCatBusiness.All
                                           where subjectListID.Contains(s.SubjectCatID)                                           
                                           select s).OrderBy(p=>p.OrderInSubject).ThenBy(p=>p.SubjectName).ToList();
            return listResult;
        }
        #endregion

    }
}