/**
 * The Viettel License
 *
 * Copyright 2012 Viettel Telecom. All rights reserved.
 * VIETTEL PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */

/** 
* @author  
* @version $Revision: $
*/

using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Linq.Expressions;
using System.Web;
using log4net;

using SMAS.DAL.Repository;
using SMAS.DAL.IRepository;
using SMAS.Business.IBusiness;
using SMAS.Models.Models;
using SMAS.Business.Common;
using SMAS.Business.BusinessObject;
using System.Transactions;
using System.Text;

namespace SMAS.Business.Business
{
    public partial class RegisterSMSFreeBusiness
    {
        public List<int> GetAllPupilId(int schoolId, int academicYearId)
        {
            return this.All.Where(x => x.SCHOOL_ID == schoolId).Where(o => o.ACADEMIC_YEAR_ID == academicYearId).Select(o => o.PUPIL_ID).ToList();
        }

        public List<int> GetAllPupilId(int schoolId, int academicYearId, List<int> status)
        {
            if (status == null || !status.Any())
            {
                return GetAllPupilId(schoolId, academicYearId);
            }
            else
            {
                return this.All.Where(x => x.SCHOOL_ID == schoolId).Where(o => o.ACADEMIC_YEAR_ID == academicYearId).Where(o => status.Contains(o.STATUS_REGISTER ?? 0)).Select(o => o.PUPIL_ID).ToList();
            }
        }

        public List<int> GetAllPupilIdRegisterStatus(int schoolId, int academicYearId)
        {
            return this.All.Where(x => x.SCHOOL_ID == schoolId && x.ACADEMIC_YEAR_ID == academicYearId && (x.STATUS_REGISTER != null && x.STATUS_REGISTER != 0)).Select(o => o.PUPIL_ID).ToList();
        }

        public List<SMS_REGISTER_FREE> GetByPupil(List<int> pupilIds, int academicYearId, int schoolId)
        {
            return this.All.Where(o => o.ACADEMIC_YEAR_ID == academicYearId && pupilIds.Contains(o.PUPIL_ID)).ToList();
        }

        public List<int> GetPupilIdByPupil(List<int> pupilIds, int academicYearId, int schoolId)
        {
            if (pupilIds == null || !pupilIds.Any())
            {
                return null;
            }
            return this.All.Where(o => o.ACADEMIC_YEAR_ID == academicYearId
                && pupilIds.Contains(o.PUPIL_ID)
                && o.SCHOOL_ID == schoolId)
                .Select(o => o.PUPIL_ID)
                .ToList();
        }

        public List<int> GetPupilIdByPupil(int academicYearId, int schoolId)
        {
            return this.All.Where(o => o.ACADEMIC_YEAR_ID == academicYearId
                && o.SCHOOL_ID == schoolId)
                .Select(o => o.PUPIL_ID)
                .ToList();
        }

        public void UpdateStatus(List<int> pupilIds, int academicYearId, int schoolId, int? status)
        {
            if (pupilIds == null || !pupilIds.Any())
            {
                return;
            }

            List<SMS_REGISTER_FREE> list = this.All.Where(o => o.ACADEMIC_YEAR_ID == academicYearId
                && o.SCHOOL_ID == schoolId
                && pupilIds.Contains(o.PUPIL_ID)).ToList();
            for (int i = 0; i < list.Count; i++)
            {
                SMS_REGISTER_FREE obj = list[i];
                obj.STATUS_REGISTER = status;
                this.Update(obj);
            }

            this.Save();
        }

        public void UpdateStatus(int pupilId, int academicYearId, int schoolId, int? status)
        {
            if (pupilId <= 0)
            {
                return;
            }
            SMS_REGISTER_FREE obj = this.All.FirstOrDefault(o => o.ACADEMIC_YEAR_ID == academicYearId
                && o.SCHOOL_ID == schoolId
                && o.PUPIL_ID == pupilId);

            obj.STATUS_REGISTER = status;
            this.Update(obj);


            this.Save();
        }
    }
}
