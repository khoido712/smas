﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Linq.Expressions;
using System.Web;
using log4net;
using SMAS.Models.Models;
using SMAS.Business.Common;
using SMAS.Business.BusinessObject;
using System.IO;
using SMAS.DAL.Repository;
using SMAS.DAL.IRepository;
using SMAS.VTUtils.Excel.Export;
using System.Drawing;

namespace SMAS.Business.Business
{
    public partial class SummedEndingEvaluationBusiness
    {
        public IQueryable<SummedEndingEvaluationBO> Search(IDictionary<string, object> dic)
        {
            int academicYearID = Utils.GetInt(dic, "AcademicYearID");
            int schoolID = Utils.GetInt(dic, "SchoolID");
            int partition = UtilsBusiness.GetPartionId(schoolID);
            int semester = Utils.GetInt(dic, "Semester");
            int evaluationID = Utils.GetInt(dic, "EvaluationID");
            int appliedLevelID = Utils.GetInt(dic, "AppliedLevelID");
            int educationLevelID = Utils.GetInt(dic, "EducationLevelID");
            int classID = Utils.GetInt(dic, "ClassID");
            int pupilID = Utils.GetInt(dic, "PupilID");
            int IsGetReward = Utils.GetInt(dic, "IsGetReward");
            List<int> lstClassID = Utils.GetIntList(dic, "lstClassID");

            IQueryable<SummedEndingEvaluationBO> query = from see in SummedEndingEvaluationRepository.All
                                                         join pp in PupilProfileRepository.All on see.PupilID equals pp.PupilProfileID
                                                         join cp in ClassProfileRepository.All.Where(o => o.AcademicYearID == academicYearID && o.SchoolID == schoolID) on see.ClassID equals cp.ClassProfileID
                                                         where see.AcademicYearID == academicYearID && see.LastDigitSchoolID == partition && see.SchoolID == schoolID
                                                               && cp.IsActive == true
                                                         select new SummedEndingEvaluationBO
                                                         {
                                                             SummedEndingEvaluationID = see.SummedEndingEvaluationID,
                                                             PupilID = see.PupilID,
                                                             PupilCode = pp.PupilCode,
                                                             FullName = pp.FullName,
                                                             SemesterID = see.SemesterID,
                                                             EvaluationID = see.EvaluationID,
                                                             SchoolID = see.SchoolID,
                                                             ClassID = see.ClassID,
                                                             ClassName = cp.DisplayName,
                                                             EndingEvaluation = see.EndingEvaluation,
                                                             RewardID = see.Reward,
                                                             IsCombinedClass = cp.IsCombinedClass,
                                                             IsDisabled = pp.IsDisabled,
                                                             Genre = pp.Genre,
                                                             EthnicID = pp.EthnicID,
                                                             EducationLevelID = cp.EducationLevelID,
                                                             RateAdd = see.RateAdd
                                                         };

            if (semester != 0)
            {
                query = query.Where(o => o.SemesterID == semester);
            }
            if (evaluationID != 0)
            {
                query = query.Where(o => o.EvaluationID == evaluationID);
            }
            if (lstClassID.Count > 0)
            {
                query = query.Where(o => lstClassID.Contains(o.ClassID));
            }
            if (appliedLevelID != 0)
            {
                if (appliedLevelID == 1)
                {
                    query = query.Where(p => p.EducationLevelID > 0 && p.EducationLevelID < 6);
                }
                else if (appliedLevelID == 2)
                {
                    query = query.Where(p => p.EducationLevelID > 5 && p.EducationLevelID < 10);
                }
                else
                {
                    query = query.Where(p => p.EducationLevelID > 9 && p.EducationLevelID < 13);
                }
            }
            if (classID != 0)
            {
                query = query.Where(p => p.ClassID == classID);
            }
            if (educationLevelID != 0)
            {
                query = query.Where(p => p.EducationLevelID == educationLevelID);
            }

            if (pupilID != 0)
            {
                query = query.Where(o => o.PupilID == pupilID);
            }

            if (IsGetReward != 0) 
            {
                
                query = query.Where(x => x.RewardID.HasValue && x.RewardID.Value > 0);
            }

            return query;
        }

        public List<SummedEndingEvaluation> GetSummedEndingEvaluation(IDictionary<string, object> dic)
        {
            int schoolID = Utils.GetInt(dic, "SchoolID");
            int academicYearID = Utils.GetInt(dic, "AcademicYearID");
            int classID = Utils.GetInt(dic, "ClassID");
            int semesterID = Utils.GetInt(dic, "SemesterID");//bo không where theo semester nữa
            int pupilID = Utils.GetInt(dic, "PupilID");
            int lastDigit = UtilsBusiness.GetPartionId(schoolID);
            List<int> lstPupilID = Utils.GetIntList(dic, "lstPupilID");
            List<int> lstClassID = Utils.GetIntList(dic, "lstClassID");
            bool isReward = Utils.GetBool(dic, "isReward");


            IQueryable<SummedEndingEvaluation> iqSummedEnding = SummedEndingEvaluationBusiness.All;

            if (schoolID > 0)
            {
                iqSummedEnding = iqSummedEnding.Where(p => p.SchoolID == schoolID);
            }

            if (academicYearID > 0)
            {
                iqSummedEnding = iqSummedEnding.Where(p => p.AcademicYearID == academicYearID);
            }

            if (classID > 0)
            {
                iqSummedEnding = iqSummedEnding.Where(p => p.ClassID == classID);
            }

            if (pupilID > 0)
            {
                iqSummedEnding = iqSummedEnding.Where(p => p.PupilID == pupilID);
            }
            if (lstPupilID.Count > 0)
            {
                iqSummedEnding = iqSummedEnding.Where(p => lstPupilID.Contains(p.PupilID));
            }
            if (semesterID > 0)
            {
                iqSummedEnding = iqSummedEnding.Where(p => p.SemesterID == semesterID);
            }
            if (lstClassID.Count > 0)
            {
                iqSummedEnding = iqSummedEnding.Where(p => lstClassID.Contains(p.ClassID));
            }
            //if (isReward)
            //{
            //    iqSummedEnding = iqSummedEnding.Where(p => p.SemesterID == GlobalConstants.SEMESTER_OF_EVALUATION_RESULT && p.EvaluationID == GlobalConstants.EVALUATION_RESULT);
            //}
            //else
            //{
            //    if (semesterID > 0)
            //    {
            //        iqSummedEnding = iqSummedEnding.Where(p => p.SemesterID == semesterID);
            //    }
            //}

            if (lastDigit > 0)
            {
                iqSummedEnding = iqSummedEnding.Where(p => p.LastDigitSchoolID == lastDigit);
            }

            return iqSummedEnding.ToList();
        }

        public void InsertOrUpdate(IDictionary<string, object> dic, List<SummedEndingEvaluation> lstSummedEnding)
        {

            int schoolID = Utils.GetInt(dic, "SchoolID");
            int classID = Utils.GetInt(dic, "ClassID");
            bool isRateAdd = Utils.GetBool(dic, "IsRateAdd", false);
            int partitionId = UtilsBusiness.GetPartionId(schoolID);
            List<int> lstPupilID = lstSummedEnding.Select(p => p.PupilID).Distinct().ToList();
            List<SummedEndingEvaluation> lstSummedEndingDB = (from su in SummedEndingEvaluationBusiness.All
                                                              where
                                                              su.LastDigitSchoolID == partitionId
                                                              && su.ClassID == classID
                                                              && su.SchoolID == schoolID
                                                              && lstPupilID.Contains(su.PupilID)
                                                              && su.SemesterID == GlobalConstants.SEMESTER_OF_EVALUATION_RESULT
                                                              && su.EvaluationID == GlobalConstants.EVALUATION_RESULT
                                                              select su).ToList();

            List<SummedEndingEvaluation> lstInsert = new List<SummedEndingEvaluation>();
            List<SummedEndingEvaluation> lstUpdate = new List<SummedEndingEvaluation>();
            SummedEndingEvaluation objSummedEnding = null;
            SummedEndingEvaluation checklist = null;
            for (int i = 0; i < lstSummedEnding.Count; i++)
            {
                objSummedEnding = lstSummedEnding[i];
                checklist = lstSummedEndingDB.Where(p => p.PupilID == objSummedEnding.PupilID).FirstOrDefault();
                if (checklist == null)
                {
                    objSummedEnding.SummedEndingEvaluationID = SummedEndingEvaluationBusiness.GetNextSeq<long>();
                    objSummedEnding.LastDigitSchoolID = partitionId;
                    lstInsert.Add(objSummedEnding);
                }
                else
                {
                    if (isRateAdd)
                    {
                        checklist.RateAdd = objSummedEnding.RateAdd;
                    }
                    else
                    {
                        checklist.EndingEvaluation = objSummedEnding.EndingEvaluation;
                        checklist.Reward = objSummedEnding.Reward;
                    }
                    checklist.UpdateTime = DateTime.Now;
                    lstUpdate.Add(checklist);
                }

            }
            if (lstInsert != null && lstInsert.Count > 0)
            {
                for (int i = 0; i < lstInsert.Count; i++)
                {
                    this.Insert(lstInsert[i]);
                }
            }
            if (lstUpdate != null && lstUpdate.Count > 0)
            {
                for (int i = 0; i < lstUpdate.Count; i++)
                {
                    this.Update(lstUpdate[i]);
                }
            }
            this.Save();
        }

        public void DeleteSummedEvaluation(IDictionary<string, object> dic, List<long> lstPupilID)
        {
            int schoolID = Utils.GetInt(dic, "SchoolID");
            int academicYearID = Utils.GetInt(dic, "AcademicYearID");
            int lastDigit = UtilsBusiness.GetPartionId(schoolID);
            List<SummedEndingEvaluation> lstSummedEnding = (from s in SummedEndingEvaluationBusiness.All
                                                            where s.SchoolID == schoolID
                                                            && s.AcademicYearID == academicYearID
                                                            && s.LastDigitSchoolID == lastDigit
                                                            && s.SemesterID == GlobalConstants.SEMESTER_OF_EVALUATION_RESULT
                                                            && s.EvaluationID == GlobalConstants.EVALUATION_RESULT
                                                            && lstPupilID.Contains(s.PupilID)
                                                            select s).ToList();
            SummedEndingEvaluationBusiness.DeleteAll(lstSummedEnding);
            SummedEndingEvaluationBusiness.Save();
        }

        public void InsertOrUpdatebyClass(int classId, IDictionary<string, object> dic, List<SummedEndingEvaluation> lstSummedEnding)
        {
            int schoolID = Utils.GetInt(dic, "SchoolID");
            int partitionId = Utils.GetInt(dic, "PartitionId");
            int academicYeaId = Utils.GetInt(dic, "AcademicYearID");
            int semesterId = Utils.GetInt(dic, "SemesterID");
            IDictionary<string, object> objDic = new Dictionary<string, object>()
                {
                    {"SchoolID",schoolID},
                    {"AcademicYearID",academicYeaId},
                    {"ClassID",classId}
                };
            LockMarkPrimary objLockMarkPrimary = LockMarkPrimaryBusiness.SearchLockedMark(objDic).ToList().FirstOrDefault();
            string LockMarkTitle = objLockMarkPrimary != null ? objLockMarkPrimary.LockPrimary : "";
            string ck = (semesterId == GlobalConstants.SEMESTER_OF_YEAR_FIRST) ? "CK1" : "CK2";
            bool lockCK = UtilsBusiness.CheckLockMarkBySemester(semesterId, LockMarkTitle, ck, "");
            List<SummedEndingEvaluation> lstSummedEndingDB = (from su in SummedEndingEvaluationBusiness.All
                                                              where
                                                              su.LastDigitSchoolID == partitionId
                                                              && su.AcademicYearID == academicYeaId
                                                              && su.ClassID == classId
                                                              && su.SemesterID == semesterId
                                                              select su).ToList();

            List<SummedEndingEvaluation> lstInsert = new List<SummedEndingEvaluation>();
            List<SummedEndingEvaluation> lstUpdate = new List<SummedEndingEvaluation>();
            SummedEndingEvaluation objSummedEnding = null;
            SummedEndingEvaluation checklist = null;
            for (int i = 0; i < lstSummedEnding.Count; i++)
            {
                objSummedEnding = lstSummedEnding[i];
                checklist = lstSummedEndingDB.Where(p => p.PupilID == objSummedEnding.PupilID && p.EvaluationID == objSummedEnding.EvaluationID).FirstOrDefault();
                if (checklist == null)
                {
                    if (!lockCK)
                    {
                        objSummedEnding.SummedEndingEvaluationID = SummedEndingEvaluationBusiness.GetNextSeq<long>();
                        objSummedEnding.CreateTime = DateTime.Now;
                        lstInsert.Add(objSummedEnding);
                    }
                }
                else
                {
                    if (!lockCK)
                    {
                        checklist.EndingEvaluation = objSummedEnding.EndingEvaluation;
                        checklist.UpdateTime = DateTime.Now;
                        lstUpdate.Add(checklist);
                    }
                }

            }

            //IDictionary<string, object> columnMap = ColumnMappings();
            if (lstInsert != null && lstInsert.Count > 0)
            {
                //BulkInsert(lstInsert, columnMap);
                for (int i = 0; i < lstInsert.Count; i++)
                {
                    this.Insert(lstInsert[i]);
                }
            }
            if (lstUpdate != null && lstUpdate.Count > 0)
            {
                /*List<string> columnWhere = new List<string>();
                columnWhere.Add("LAST_DIGIT_SCHOOL_ID");
                columnWhere.Add("SUMMED_ENDING_EVALUATION_ID");
                BulkUpdate(lstUpdate, columnMap, columnWhere);*/
                for (int i = 0; i < lstUpdate.Count; i++)
                {
                    this.Update(lstUpdate[i]);
                }
            }
            this.Save();
        }

        #region Export Bang tong hop danh gia
        public ProcessedReport InsertGeneralSummedEvaluationReport(int SchoolID, int AcademicYearID, int EducationLevelID, int ClassID, int SemesterID, string ClassName, Stream data, int PrintType, bool IsZip, List<string> lstPrintData)
        {
            string indexPage = "off";
            if (lstPrintData[4] == "on")
                indexPage = "on";
            string GK1 = "off";
            if (lstPrintData[0] == "on")
                GK1 = "on";
            string CK1 = "off";
            if (lstPrintData[1] == "on")
                CK1 = "on";
            string GK2 = "off";
            if (lstPrintData[2] == "on")
                GK2 = "on";
            string CN = "off";
            if (lstPrintData[3] == "on")
                CN = "on";
            string reportCode = SystemParamsInFile.REPORT_BANG_TONG_HOP_KET_QUA_DANH_GIA;
            if (PrintType == 2)
            {
                reportCode = SystemParamsInFile.REPORT_BANG_TONG_HOP_KET_QUA_DANH_GIA_A3;
            }
            ReportDefinition reportDef = ReportDefinitionBusiness.GetByCode(reportCode);
            ProcessedReport pr = new ProcessedReport();
            pr.ReportCode = reportCode;
            pr.ProcessedDate = DateTime.Now;
            pr.InputParameterHashKey = this.GetHashKeyGeneralReportSummedEnding(AcademicYearID, SchoolID, SemesterID, EducationLevelID, ClassID, PrintType, IsZip, lstPrintData);
            pr.ReportData = ReportUtils.Compress(data);          

            //Tạo tên file
            string outputNamePattern = reportDef.OutputNamePattern;
            outputNamePattern = outputNamePattern.Replace("[ClassName]", ClassID > 0 ? ClassName : "TatCa");
            pr.ReportName = ReportUtils.RemoveSpecialCharacters(outputNamePattern) + "." + reportDef.OutputFormat;
            IDictionary<string, object> dic = new Dictionary<string, object> {
                {"AcademicYearID", AcademicYearID},
                {"SchoolID", SchoolID},
                {"EducationLevelID",EducationLevelID},
                {"ClassID", ClassID},
                {"PrintType",PrintType},
                {"IsZip",IsZip},
                {"indexPage", indexPage},
                {"GK1", GK1},
                {"CK1", CK1},
                {"GK2", GK2},
                {"CN", CN},
            };
            ProcessedReportParameterRepository.Insert(dic, pr);
            ProcessedReportRepository.Insert(pr);
            ProcessedReportRepository.Save();
            return pr;
        }

        public ProcessedReport InsertGeneralSummedEvaluationReportZip(int SchoolID, int AcademicYearID, int EducationLevelID, int ClassID, int SemesterID, string ClassName, Stream data, int PrintType, bool IsZip, List<string> lstPrintData)
        {
            string indexPage = "off";
            if (lstPrintData[4] == "on")
                indexPage = "on";
            string GK1 = "off";
            if (lstPrintData[0] == "on")
                GK1 = "on";
            string CK1 = "off";
            if (lstPrintData[1] == "on")
                CK1 = "on";
            string GK2 = "off";
            if (lstPrintData[2] == "on")
                GK2 = "on";
            string CN = "off";
            if (lstPrintData[3] == "on")
                CN = "on";
            string reportCode = SystemParamsInFile.REPORT_BANG_TONG_HOP_KET_QUA_DANH_GIA_ZIP;
            if (PrintType == 2) 
            {
                reportCode = SystemParamsInFile.REPORT_BANG_TONG_HOP_KET_QUA_DANH_GIA_A3_ZIP;
            }
            ReportDefinition reportDef = ReportDefinitionBusiness.GetByCode(reportCode);
            ProcessedReport pr = new ProcessedReport();
            pr.ReportCode = reportCode;
            pr.ProcessedDate = DateTime.Now;
            pr.InputParameterHashKey = this.GetHashKeyGeneralReportSummedEnding(AcademicYearID, SchoolID, SemesterID, EducationLevelID, ClassID, PrintType, IsZip, lstPrintData);
            pr.ReportData = ClassID == 0 ? ((MemoryStream)data).ToArray() : ReportUtils.Compress(data);
                     
            //Tạo tên file
            string outputNamePattern = reportDef.OutputNamePattern;
            outputNamePattern = outputNamePattern.Replace("[ClassName]", ClassID > 0 ? ClassName : "TatCa");
            pr.ReportName = ReportUtils.RemoveSpecialCharacters(outputNamePattern) + "." + reportDef.OutputFormat;
            IDictionary<string, object> dic = new Dictionary<string, object> {
                {"AcademicYearID", AcademicYearID},
                {"SchoolID", SchoolID},
                {"EducationLevelID",EducationLevelID},
                {"ClassID", ClassID},
                {"PrintType",PrintType},
                {"IsZip",IsZip},
                {"indexPage", indexPage},
                {"GK1", GK1},
                {"CK1", CK1},
                {"GK2", GK2},
                {"CN", CN},
            };
            ProcessedReportParameterRepository.Insert(dic, pr);
            ProcessedReportRepository.Insert(pr);
            ProcessedReportRepository.Save();
            return pr;
        }

        public ProcessedReport GetProcessedReport(int AcademicYearID, int SchoolID, int TypeSemesterID, int EducationLevelID, int ClassID)
        {
            string reportCode = SystemParamsInFile.REPORT_BANG_TONG_HOP_KET_QUA_DANH_GIA;
            string inputParameterHashKey = this.GetHashKeyGeneralReport(AcademicYearID, SchoolID, TypeSemesterID, EducationLevelID, ClassID);
            return ProcessedReportBusiness.GetProcessedReport(reportCode, inputParameterHashKey);
        }

        public ProcessedReport GetProcessedReportSummedEnding(int AcademicYearID, int SchoolID, int TypeSemesterID, int EducationLevelID, int ClassID, int PrintType, bool IsZip, List<string> lstPrintData)
        {
            string reportCode = "";
            if (PrintType == 1)
            {
                reportCode = SystemParamsInFile.REPORT_BANG_TONG_HOP_KET_QUA_DANH_GIA;
                if (IsZip)
                    reportCode = SystemParamsInFile.REPORT_BANG_TONG_HOP_KET_QUA_DANH_GIA_ZIP;
            }
            else
            {
                reportCode = SystemParamsInFile.REPORT_BANG_TONG_HOP_KET_QUA_DANH_GIA_A3;
                if(IsZip)
                    reportCode = SystemParamsInFile.REPORT_BANG_TONG_HOP_KET_QUA_DANH_GIA_A3_ZIP;
            }
            string inputParameterHashKey = this.GetHashKeyGeneralReportSummedEnding(AcademicYearID, SchoolID, TypeSemesterID, EducationLevelID, ClassID, PrintType, IsZip, lstPrintData);
            return ProcessedReportBusiness.GetProcessedReport(reportCode, inputParameterHashKey);
        }

        private IDictionary<string, object> ColumnMappings()
        {
            Dictionary<string, object> columnMap = new Dictionary<string, object>();
            columnMap.Add("SummedEndingEvaluationID", "SUMMED_ENDING_EVALUATION_ID");
            columnMap.Add("PupilID", "PUPIL_ID");
            columnMap.Add("SemesterID", "SEMESTER_ID");
            columnMap.Add("EvaluationID", "EVALUATION_ID");
            columnMap.Add("SchoolID", "SCHOOL_ID");
            columnMap.Add("LastDigitSchoolID", "LAST_DIGIT_SCHOOL_ID");
            columnMap.Add("ClassID", "CLASS_ID");
            columnMap.Add("AcademicYearID", "ACADEMIC_YEAR_ID");
            columnMap.Add("EndingEvaluation", "ENDING_EVALUATION");
            columnMap.Add("EvaluationReTraining", "EVALUATION_RETRAINING");
            columnMap.Add("CreateTime", "CREATE_TIME");
            columnMap.Add("UpdateTime", "UPDATE_TIME");
            return columnMap;
        }

        private string GetHashKeyGeneralReportSummedEnding(int AcademicYearID, int SchoolID, int SemesterID, int EducationLevelID, int ClassID, int PrintType, bool IsZip, List<string> lstPrintData)
        {
            string indexPage = "off";
            if (lstPrintData[4] == "on")
                indexPage = "on";
            string GK1 = "off";
            if (lstPrintData[0] == "on")
                GK1 = "on";
            string CK1 = "off";
            if (lstPrintData[1] == "on")
                CK1 = "on";
            string GK2 = "off";
            if (lstPrintData[2] == "on")
                GK2 = "on";
            string CN = "off";
            if (lstPrintData[3] == "on")
                CN = "on";
            IDictionary<string, object> dic = new Dictionary<string, object> {
                {"AcademicYearID", AcademicYearID},
                {"SchoolID", SchoolID},
                {"EducationLevelID",EducationLevelID},
                {"ClassID", ClassID},
                {"PrintType", PrintType},
                {"IsZip", IsZip},
                {"indexPage", indexPage},
                {"GK1", GK1},
                {"CK1", CK1},
                {"GK2", GK2},
                {"CN", CN},
            };
            return ReportUtils.GetHashKey(dic);
        }

        private string GetHashKeyGeneralReport(int AcademicYearID, int SchoolID, int SemesterID, int EducationLevelID, int ClassID)
        {
            IDictionary<string, object> dic = new Dictionary<string, object> {
                {"AcademicYearID", AcademicYearID},
                {"SchoolID", SchoolID},
                {"EducationLevelID",EducationLevelID},
                {"ClassID", ClassID},
                {"SemesterID",SemesterID}
            };
            return ReportUtils.GetHashKey(dic);
        }
        
        public Stream GetGeneralReport(IDictionary<string, object> dic)
        {
            int SchoolID = Utils.GetInt(dic, "SchoolID");
            int AcademicYearID = Utils.GetInt(dic, "AcademicYearID");
            List<int> lstClassID = Utils.GetIntList(dic, "lstClassID");
            int AppliedLevel = Utils.GetInt(dic, "AppliedLevelID");
            int SemesterID = Utils.GetInt(dic, "SemesterID");//
            int TypeSemesterID = Utils.GetInt(dic, "TypeSemesterID");//
            string IsPupilInClass = Utils.GetString(dic, "IsPupilInClass");
            int NumberOfRow = Utils.GetInt(dic, "NumberOfRow");
            int PrintEnough = Utils.GetInt(dic, "PrintEnough");
            List<string> lstDataPrint = Utils.GetStringList(dic, "lstDataPrint");
            int TypePrint = Utils.GetInt(dic, "TypePrint");
            AcademicYear objAy = AcademicYearBusiness.Find(AcademicYearID);
            SchoolProfile objSP = SchoolProfileBusiness.Find(SchoolID);
            ReportDefinition reportDef = null;
            if (TypePrint == 1)
                reportDef = ReportDefinitionBusiness.GetByCode(SystemParamsInFile.REPORT_BANG_TONG_HOP_KET_QUA_DANH_GIA);
            else
            {
                reportDef = ReportDefinitionBusiness.GetByCode(SystemParamsInFile.REPORT_BANG_TONG_HOP_KET_QUA_DANH_GIA_A3);
                PrintEnough = 1;
            }
            string templatePath = SystemParamsInFile.TEMPLATE_FOLDER + reportDef.TemplateDirectory + "/" + reportDef.TemplateName;
           
            IDictionary<string, object> dicSearchClass = new Dictionary<string, object>()
            {
                {"SchoolID",SchoolID},
                {"AcademicYearID",AcademicYearID},
                {"lstClassID",lstClassID}
            };
            List<ClassProfile> lstCP = ClassProfileBusiness.Search(dicSearchClass).ToList();
            #region lay danh sach hoc sinh cua cac lop
            //lay danh sach hoc sinh theo list classID
            List<PupilOfClassBO> lstPOC = (from poc in PupilOfClassBusiness.Search(dic)
                                           join cp in ClassProfileBusiness.All on poc.ClassID equals cp.ClassProfileID
                                           join e in EmployeeBusiness.All on cp.HeadTeacherID equals e.EmployeeID into l
                                           from l1 in l.DefaultIfEmpty()
                                           where cp.IsActive.HasValue && cp.IsActive.Value
                                           select new PupilOfClassBO
                                           {
                                               PupilID = poc.PupilID,
                                               PupilCode = poc.PupilProfile.PupilCode,
                                               PupilFullName = poc.PupilProfile.FullName,
                                               ClassID = poc.ClassID,
                                               ClassName = poc.ClassProfile.DisplayName,
                                               Status = poc.Status,
                                               OrderInClass = poc.OrderInClass,
                                               Name = poc.PupilProfile.Name,
                                               Birthday = poc.PupilProfile.BirthDate,
                                               HeadTeacherName = l1.FullName,
                                               Genre = poc.PupilProfile.Genre
                                           }).OrderBy(c => c.OrderInClass).ThenBy(c => c.Name).ThenBy(c => c.PupilFullName).ToList();
            #endregion

            List<int> lstPupilRemove = new List<int>();
            if (objAy.IsShowPupil.HasValue && objAy.IsShowPupil.Value == true)
            {
                lstPupilRemove = lstPOC.Where(x => x.Status != SystemParamsInFile.PUPIL_STATUS_GRADUATED
                                            && x.Status != SystemParamsInFile.PUPIL_STATUS_STUDYING).Select(x => x.PupilID).ToList();
                lstPOC = lstPOC.Where(x => x.Status == SystemParamsInFile.PUPIL_STATUS_GRADUATED
                                            || x.Status == SystemParamsInFile.PUPIL_STATUS_STUDYING).ToList();
            }

            #region lay danh sach diem cua hoc sinh
            //lay danh sach diem cua hoc sinh
            List<RatedCommentPupilBO> lstRatedCommentPupilBO = new List<RatedCommentPupilBO>();
            IDictionary<string, object> dicRated = new Dictionary<string, object>()
            {
                {"SchoolID",SchoolID},
                {"AcademicYearID",AcademicYearID},
                {"lstClassID",lstClassID},
                //{"SemesterID",SemesterID},//
            };
            if (UtilsBusiness.IsMoveHistory(objAy))
            {
                lstRatedCommentPupilBO = (from rh in RatedCommentPupilHistoryBusiness.Search(dicRated)
                                          select new RatedCommentPupilBO
                                          {
                                              SchoolID = rh.SchoolID,
                                              AcademicYearID = rh.AcademicYearID,
                                              PupilID = rh.PupilID,
                                              ClassID = rh.ClassID,
                                              SubjectID = rh.SubjectID,
                                              SemesterID = rh.SemesterID,
                                              EvaluationID = rh.EvaluationID,
                                              PeriodicMiddleMark = rh.PeriodicMiddleMark,
                                              PeriodicEndingMark = rh.PeriodicEndingMark,
                                              PeriodicMiddleJudgement = rh.PeriodicMiddleJudgement,
                                              PeriodicEndingJudgement = rh.PeriodicEndingJudgement,
                                              MiddleEvaluation = rh.MiddleEvaluation,
                                              EndingEvaluation = rh.EndingEvaluation,
                                              CapacityMiddleEvaluation = rh.CapacityMiddleEvaluation,
                                              CapacityEndingEvaluation = rh.CapacityEndingEvaluation,
                                              QualityMiddleEvaluation = rh.QualityMiddleEvaluation,
                                              QualityEndingEvaluation = rh.QualityEndingEvaluation,
                                              Comment = rh.Comment,
                                              CreateTime = rh.CreateTime,
                                              UpdateTime = rh.UpdateTime,
                                              FullNameComment = rh.FullNameComment,
                                              LogChangeID = rh.LogChangeID,
                                          }).ToList();
            }
            else
            {
                lstRatedCommentPupilBO = (from rh in RatedCommentPupilBusiness.Search(dic)
                                          select new RatedCommentPupilBO
                                          {
                                              SchoolID = rh.SchoolID,
                                              AcademicYearID = rh.AcademicYearID,
                                              PupilID = rh.PupilID,
                                              ClassID = rh.ClassID,
                                              SubjectID = rh.SubjectID,
                                              SemesterID = rh.SemesterID,
                                              EvaluationID = rh.EvaluationID,
                                              PeriodicMiddleMark = rh.PeriodicMiddleMark,
                                              PeriodicEndingMark = rh.PeriodicEndingMark,
                                              PeriodicMiddleJudgement = rh.PeriodicMiddleJudgement,
                                              PeriodicEndingJudgement = rh.PeriodicEndingJudgement,
                                              MiddleEvaluation = rh.MiddleEvaluation,
                                              EndingEvaluation = rh.EndingEvaluation,
                                              CapacityMiddleEvaluation = rh.CapacityMiddleEvaluation,
                                              CapacityEndingEvaluation = rh.CapacityEndingEvaluation,
                                              QualityMiddleEvaluation = rh.QualityMiddleEvaluation,
                                              QualityEndingEvaluation = rh.QualityEndingEvaluation,
                                              Comment = rh.Comment,
                                              CreateTime = rh.CreateTime,
                                              UpdateTime = rh.UpdateTime,
                                              FullNameComment = rh.FullNameComment,
                                              LogChangeID = rh.LogChangeID
                                          }).ToList();
            }
            #endregion
            #region lay danh sach mon hoc
            //lay danh sach mon hoc 
            IQueryable<ClassSubject> iqSubject;
            Dictionary<string, object> dicSubject = new Dictionary<string, object>();
            dicSubject["ListClassID"] = lstClassID;
            dicSubject["AcademicYearID"] = AcademicYearID;
            dicSubject["IsApprenticeShipSubject"] = false;
            dicSubject["AppliedLevel"] = AppliedLevel;
            //dicSubject["Semester"] = SemesterID;//
            iqSubject = ClassSubjectBusiness.SearchBySchool(SchoolID, dicSubject);
            List<ClassSubjectBO> lstSubject = iqSubject.Select(o => new ClassSubjectBO()
            {
                ClassID = o.ClassID,
                SubjectID = o.SubjectID,
                OrderInSubject = o.SubjectCat.OrderInSubject,
                DisplayName = o.SubjectCat.DisplayName,
                SectionPerWeekSecondSemester = o.SectionPerWeekSecondSemester,
                SectionPerWeekFirstSemester = o.SectionPerWeekFirstSemester,
                IsCommenting = o.IsCommenting,
                Abbreviation = o.SubjectCat.Abbreviation
            }).OrderBy(p => p.IsCommenting).ThenBy(o => o.OrderInSubject).ThenBy(o => o.DisplayName).ToList();
            #endregion
            #region lay danh sach khen thuong cua hoc sinh
            List<EvaluationReward> lstEvaluationReward = new List<EvaluationReward>();
            List<SummedEndingEvaluationBO> lstSEE = new List<SummedEndingEvaluationBO>();
            IDictionary<string, object> dicSearchReward = new Dictionary<string, object>()
            {
                {"SchoolID",SchoolID},
                {"AcademicYearID",AcademicYearID},
                {"lstClassID",lstClassID},
                {"Semester",GlobalConstants.SEMESTER_OF_YEAR_BACKTRAINING},
                {"EvaluationID",GlobalConstants.EVALUATION_RESULT}
            };
            lstSEE = this.Search(dicSearchReward).ToList();

            dicSearchReward = new Dictionary<string, object>()
            {
                {"SchoolID",SchoolID},
                {"AcademicYearID",AcademicYearID},
                {"lstClassID",lstClassID}
            };
            lstEvaluationReward = EvaluationRewardBusiness.Search(dicSearchReward).ToList();
            #endregion

            if (lstPupilRemove.Count > 0)
            {
                lstRatedCommentPupilBO = lstRatedCommentPupilBO.Where(x => !lstPupilRemove.Contains(x.PupilID)).ToList();
                lstEvaluationReward = lstEvaluationReward.Where(x => !lstPupilRemove.Contains(x.PupilID)).ToList();
                lstSEE = lstSEE.Where(x => !lstPupilRemove.Contains(x.PupilID)).ToList();
            }
            Dictionary<string, object> dicReport = null;
            List<PupilOfClassBO> lstPOCtmp = null;
            List<ClassSubjectBO> lstCStmp = null;
            int ClassID = 0;
            int countSubject = 0;
            IVTWorkbook oBook = VTExport.OpenWorkbook(templatePath);
            IVTWorksheet CurrentSheet1 = oBook.GetSheet(1);
            IVTWorksheet CurrentSheet2 = oBook.GetSheet(2);
            IVTWorksheet CurrentSheet = null;
            //IVTWorksheet sheet = null;
            List<RatedCommentPupilBO> lstRatedtmp = new List<RatedCommentPupilBO>();
            ClassProfile objCP = null;
            List<SummedEndingEvaluationBO> lstSummedEndingtmp = null;
            List<EvaluationReward> lstEvaluationRewardtmp = null;
            double widthColumn = 0;
            for (int i = 0; i < lstCP.Count; i++)
            {
                for(int j = 0; j < lstDataPrint.Count; j++)
                {
                    if (lstDataPrint[j] == "on") 
                    {
                        #region Xuat Excel từng HK
                        objCP = lstCP[i];
                        ClassID = objCP.ClassProfileID;
                        lstPOCtmp = lstPOC.Where(p => p.ClassID == ClassID).ToList();                       
                        dicReport = new Dictionary<string, object>();
                        CurrentSheet = j == 4 ? oBook.CopySheetToBefore(CurrentSheet1, CurrentSheet1) : oBook.CopySheetToLast(CurrentSheet2, "CZ1000");
                        SemesterID = (j == 0 || j == 1) ? SystemParamsInFile.SEMESTER_OF_YEAR_FIRST : SystemParamsInFile.SEMESTER_OF_YEAR_SECOND;
                        if (SemesterID == SystemParamsInFile.SEMESTER_OF_YEAR_FIRST)
                        {
                            lstCStmp = lstSubject.Where(p => p.ClassID == ClassID && p.SectionPerWeekFirstSemester > 0).ToList();
                        }
                        else
                        {
                            lstCStmp = lstSubject.Where(p => p.ClassID == ClassID && p.SectionPerWeekSecondSemester > 0).ToList();
                        }
                        countSubject = lstCStmp.Count();
                        widthColumn = GetWidth(countSubject, j, TypePrint);
                        switch (j) 
                        {
                            case 4:
                                    if (TypePrint == 1)
                                    {
                                        CurrentSheet.SetCellValue("A11", "LỚP " + objCP.DisplayName.ToUpper().Replace("LỚP", ""));
                                        CurrentSheet.SetCellValue("A13", "Năm học: " + objAy.DisplayTitle);
                                        CurrentSheet.SetCellValue("J20", lstPOC.Count > 0 ? lstPOC.FirstOrDefault().HeadTeacherName : "");
                                        CurrentSheet.SetCellValue("J22", objSP.SchoolName);
                                        CurrentSheet.SetCellValue("J24", objSP.Commune != null ? objSP.Commune.CommuneName : "");
                                        CurrentSheet.SetCellValue("J28", objSP.Province != null ? objSP.Province.ProvinceName : "");
                                        CurrentSheet.SetCellValue("J26", objSP.District != null ? objSP.District.DistrictName : "");
                                    }
                                    else 
                                    {
                                        CurrentSheet.SetCellValue("B23", "LỚP " + objCP.DisplayName.ToUpper().Replace("LỚP", ""));
                                        CurrentSheet.SetCellValue("B25", "Năm học: " + objAy.DisplayTitle);
                                        CurrentSheet.SetCellValue("M49", lstPOC.Count > 0 ? lstPOC.FirstOrDefault().HeadTeacherName : "");
                                        CurrentSheet.SetCellValue("M51", objSP.SchoolName);
                                        CurrentSheet.SetCellValue("M53", objSP.Commune != null ? objSP.Commune.CommuneName : "");
                                        CurrentSheet.SetCellValue("M57", objSP.Province != null ? objSP.Province.ProvinceName : "");
                                        CurrentSheet.SetCellValue("M55", objSP.District != null ? objSP.District.DistrictName : "");
                                    }
                                    CurrentSheet.Name = "Bia";
                                    continue;
                            case 3: TypeSemesterID = 4;                                   
                                    dicReport["PageName"] = "CN";
                                break;
                            case 2: TypeSemesterID = 2;
                                    dicReport["PageName"] = "GK2";
                                break;
                            case 1: TypeSemesterID = 3;
                                    dicReport["PageName"] = "CK1";
                                break;
                            case 0: TypeSemesterID = 1;
                                    dicReport["PageName"] = "GK1";
                                break;
                            default: continue;
                        }

                        lstRatedtmp = lstRatedCommentPupilBO.Where(p => p.ClassID == ClassID && p.SemesterID == SemesterID).ToList();
                        lstSummedEndingtmp = lstSEE.Where(p => p.ClassID == ClassID).ToList();
                        lstEvaluationRewardtmp = lstEvaluationReward.Where(p => p.ClassID == ClassID).ToList();
                        
                        dicReport["TypeSemesterID"] = TypeSemesterID;
                        dicReport["HeadMasterName"] = objSP.HeadMasterName;
                        dicReport["widthColumn"] = widthColumn;
                        dicReport["IsPupilInClass"] = IsPupilInClass;
                        dicReport["NumberOfRow"] = NumberOfRow;
                        dicReport["PrintEnough"] = PrintEnough;
                        dicReport["TypePrint"] = TypePrint;
                        this.SetValueToExcel(CurrentSheet, lstRatedtmp, lstPOCtmp, lstCStmp, lstSummedEndingtmp, lstEvaluationRewardtmp, objCP, objAy, objSP, dicReport);
                        #endregion
                    }                   
                }
            }
            CurrentSheet1.Delete();
            CurrentSheet2.Delete();
            return oBook.ToStream();
        }

        public Stream GetGeneralReportZip(IDictionary<string, object> dic, ClassProfile objCP, List<string> lstDataPrint,
                                            List<PupilOfClassBO> lstPOC, IVTWorkbook oBook, AcademicYear objAy, SchoolProfile objSP,
                                            List<ClassSubjectBO> lstSubject, List<RatedCommentPupilBO> lstRatedCommentPupilBO,
                                            List<SummedEndingEvaluationBO> lstSEE, List<EvaluationReward> lstEvaluationReward)
        {
            string IsPupilInClass = Utils.GetString(dic, "IsPupilInClass");
            int NumberOfRow = Utils.GetInt(dic, "NumberOfRow");
            int PrintEnough = Utils.GetInt(dic, "PrintEnough");
            int TypePrint = Utils.GetInt(dic, "TypePrint");
            Dictionary<string, object> dicReport = null;
            List<RatedCommentPupilBO> lstRatedtmp = new List<RatedCommentPupilBO>();
            List<SummedEndingEvaluationBO> lstSummedEndingtmp = null;
            List<EvaluationReward> lstEvaluationRewardtmp = null;
            List<PupilOfClassBO> lstPOCtmp = null;
            List<ClassSubjectBO> lstCStmp = null;
            IVTWorksheet CurrentSheet1 = oBook.GetSheet(1);
            IVTWorksheet CurrentSheet2 = oBook.GetSheet(2);
            IVTWorksheet CurrentSheet = null;
            double widthColumn = 0;
            int ClassID = 0;
            int SemesterID = 0;
            int TypeSemesterID = 0;
            int countSubject = 0;
            ClassID = objCP.ClassProfileID;
            lstPOCtmp = lstPOC.Where(p => p.ClassID == ClassID).ToList();
            lstSummedEndingtmp = lstSEE.Where(p => p.ClassID == ClassID).ToList();
            lstEvaluationRewardtmp = lstEvaluationReward.Where(p => p.ClassID == ClassID).ToList();
            for (int j = 0; j < lstDataPrint.Count; j++)
            {
                if (lstDataPrint[j] == "on") 
                {
                    #region Xuất Excel HK                  
                    dicReport = new Dictionary<string, object>();
                    CurrentSheet = j == 4 ? oBook.CopySheetToBefore(CurrentSheet1, CurrentSheet1) : oBook.CopySheetToLast(CurrentSheet2, "AZ5");
                    SemesterID = (j == 0 || j == 1) ? SystemParamsInFile.SEMESTER_OF_YEAR_FIRST : SystemParamsInFile.SEMESTER_OF_YEAR_SECOND;
                    if (SemesterID == SystemParamsInFile.SEMESTER_OF_YEAR_FIRST)
                    {
                        lstCStmp = lstSubject.Where(p => p.ClassID == ClassID && p.SectionPerWeekFirstSemester > 0).ToList();
                    }
                    else
                    {
                        lstCStmp = lstSubject.Where(p => p.ClassID == ClassID && p.SectionPerWeekSecondSemester > 0).ToList();
                    }

                    countSubject = lstCStmp.Count();
                    widthColumn = GetWidth(countSubject, j, TypePrint);
                    switch (j)
                    {
                        case 4: if (TypePrint == 1)
                            {
                                CurrentSheet.SetCellValue("A11", "LỚP " + objCP.DisplayName.ToUpper().Replace("LỚP", ""));
                                CurrentSheet.SetCellValue("A13", "Năm học: " + objAy.DisplayTitle);
                                CurrentSheet.SetCellValue("J20", lstPOCtmp.Count > 0 ? lstPOCtmp.FirstOrDefault().HeadTeacherName : "");
                                CurrentSheet.SetCellValue("J22", objSP.SchoolName);
                                CurrentSheet.SetCellValue("J24", objSP.Commune != null ? objSP.Commune.CommuneName : "");
                                CurrentSheet.SetCellValue("J28", objSP.Province != null ? objSP.Province.ProvinceName : "");
                                CurrentSheet.SetCellValue("J26", objSP.District != null ? objSP.District.DistrictName : "");
                            }
                            else
                            {
                                CurrentSheet.SetCellValue("B23", "LỚP " + objCP.DisplayName.ToUpper().Replace("LỚP", ""));
                                CurrentSheet.SetCellValue("B25", "Năm học: " + objAy.DisplayTitle);
                                CurrentSheet.SetCellValue("M49", lstPOCtmp.Count > 0 ? lstPOCtmp.FirstOrDefault().HeadTeacherName : "");
                                CurrentSheet.SetCellValue("M51", objSP.SchoolName);
                                CurrentSheet.SetCellValue("M53", objSP.Commune != null ? objSP.Commune.CommuneName : "");
                                CurrentSheet.SetCellValue("M57", objSP.Province != null ? objSP.Province.ProvinceName : "");
                                CurrentSheet.SetCellValue("M55", objSP.District != null ? objSP.District.DistrictName : "");
                            }
                            CurrentSheet.Name = "Bia";
                            continue;
                        case 3: TypeSemesterID = 4;
                            dicReport["PageName"] = "CN";
                            break;
                        case 2: TypeSemesterID = 2;
                                dicReport["PageName"] = "GK2";
                            break;
                        case 1: TypeSemesterID = 3;
                                dicReport["PageName"] = "CK1";
                            break;
                        case 0: TypeSemesterID = 1;
                            dicReport["PageName"] = "GK1";
                            break;
                        default: continue;
                    }

                    lstRatedtmp = lstRatedCommentPupilBO.Where(p => p.ClassID == ClassID && p.SemesterID == SemesterID).ToList();                   
                    
                    dicReport["TypeSemesterID"] = TypeSemesterID;
                    dicReport["HeadMasterName"] = objSP.HeadMasterName;
                    dicReport["widthColumn"] = widthColumn;
                    dicReport["IsPupilInClass"] = IsPupilInClass;
                    dicReport["NumberOfRow"] = NumberOfRow;
                    dicReport["PrintEnough"] = PrintEnough;
                    dicReport["TypePrint"] = TypePrint;
                    this.SetValueToExcel(CurrentSheet, lstRatedtmp, lstPOCtmp, lstCStmp, lstSummedEndingtmp, lstEvaluationRewardtmp, objCP, objAy, objSP, dicReport);
                    #endregion
                }               
            }

            //CurrentSheet.Delete();
            CurrentSheet1.Delete();
            CurrentSheet2.Delete();
            return oBook.ToStream();
        }

        private void SetValueToExcel(IVTWorksheet sheet, List<RatedCommentPupilBO> lstRatedCommentBO, List<PupilOfClassBO> lstPOC, List<ClassSubjectBO> lstSubject, List<SummedEndingEvaluationBO> lstSEEBO
           , List<EvaluationReward> lstER, ClassProfile objCP, AcademicYear objAy, SchoolProfile objSP, Dictionary<string, object> dicReport)
        {
            int countRow = 0;
            int finalNumRow = 0;
            int finalFitIn = 0;
            int typeSemesterID = Utils.GetInt(dicReport, "TypeSemesterID");
            string headMasterName = Utils.GetString(dicReport, "HeadMasterName");
            double totalWidth = Utils.GetDouble(dicReport, "widthColumn");
            string isPupilInClass = Utils.GetString(dicReport, "IsPupilInClass");
            int numberOfRow = Utils.GetInt(dicReport, "NumberOfRow");
            int printEnough = Utils.GetInt(dicReport, "PrintEnough");
            string pageName = Utils.GetString(dicReport, "PageName");
            int typePrint = Utils.GetInt(dicReport, "TypePrint");
            #region fill thong tin chung
            //fill title
            ClassSubjectBO objCS = null;
            string className = objCP.DisplayName;
            string title = "BẢNG TỔNG HỢP KẾT QUẢ ĐÁNH GIÁ GIÁO DỤC ";
            if (typeSemesterID == SystemParamsInFile.GENERAL_SUMMED_EVALUATION_GKI)
            {
                title += "GIỮA HỌC KỲ I";
            }
            else if (typeSemesterID == SystemParamsInFile.GENERAL_SUMMED_EVALUATION_GKII)
            {
                title += "GIỮA HỌC KỲ II";
            }
            else if (typeSemesterID == SystemParamsInFile.GENERAL_SUMMED_EVALUATION_CKI)
            {
                title += "CUỐI HỌC KỲ I";
            }
            else if (typeSemesterID == SystemParamsInFile.GENERAL_SUMMED_EVALUATION_CKII)
            {
                title += "CUỐI NĂM HỌC";
            }
            title += " NĂM HỌC " + objAy.DisplayTitle + " LỚP: " + className + " TRƯỜNG: " + objSP.SchoolName;
            sheet.SetCellValue("A2", title.ToUpper());
            int startCol = 5;
            int startRow = 6;
            #endregion
            #region fill tieu de
            bool isEnableMark = (typeSemesterID == SystemParamsInFile.GENERAL_SUMMED_EVALUATION_CKI || typeSemesterID == SystemParamsInFile.GENERAL_SUMMED_EVALUATION_CKII)
                                || ((typeSemesterID == SystemParamsInFile.GENERAL_SUMMED_EVALUATION_GKI || typeSemesterID == SystemParamsInFile.GENERAL_SUMMED_EVALUATION_GKII)
                                    && (objCP.EducationLevelID == GlobalConstants.EDUCATION_LEVEL_ID4 || objCP.EducationLevelID == GlobalConstants.EDUCATION_LEVEL_ID5));
            byte fontSize = 0;
            for (int i = 0; i < lstSubject.Count; i++)
            {
                objCS = lstSubject[i];
                sheet.GetRange(4, startCol, 4, (objCS.IsCommenting == 0 && isEnableMark
                    && ((typeSemesterID == SystemParamsInFile.GENERAL_SUMMED_EVALUATION_CKI || typeSemesterID == SystemParamsInFile.GENERAL_SUMMED_EVALUATION_CKII)
                        || objCS.Abbreviation.Equals("Tt") || objCS.Abbreviation.Equals("TV"))) ? startCol + 1 : startCol).Merge();
                sheet.SetCellValue(4, startCol, objCS.DisplayName);
                fontSize = 7;
                if (objCS.Abbreviation.Equals("TNXH"))
                {
                    fontSize = 6;
                }
                
                sheet.GetRange(4, startCol, 4, startCol).SetFontName("Arial", fontSize);
                sheet.GetRange(4, startCol, 4, startCol).WrapText();
                sheet.SetCellValue(5, startCol, "Mức đạt được");
                sheet.GetRange(5, startCol, 5, startCol).SetOrientation(90);
                sheet.GetRange(5, startCol, 5, startCol).SetFontName("Arial", 7);
                if (objCS.IsCommenting == 0 && isEnableMark)
                {
                    if ((typeSemesterID == SystemParamsInFile.GENERAL_SUMMED_EVALUATION_CKI || typeSemesterID == SystemParamsInFile.GENERAL_SUMMED_EVALUATION_CKII)
                        || objCS.Abbreviation.Equals("Tt") || objCS.Abbreviation.Equals("TV"))
                    {
                        sheet.SetCellValue(5, startCol + 1, "Điểm KTĐK");
                        sheet.GetRange(5, startCol + 1, 5, startCol + 1).SetOrientation(90);
                        startCol += 2;
                    }
                    else
                    {
                        startCol += 1;
                    }
                }
                else
                {
                    startCol += 1;
                }
            }
            sheet.GetRange(3, 5, 3, startCol - 1).Merge();
            sheet.GetRange(3, 5, 3, startCol - 1).SetFontStyle(true, System.Drawing.Color.Black, false, 9, false, false);
            sheet.GetRange(5, 5, 5, startCol - 1).SetFontName("Arial", 7);
            sheet.GetRange(5, 5, 5, startCol - 1).SetHAlign(VTHAlign.xlHAlignCenter);
            sheet.GetRange(5, 5, 5, startCol - 1).SetHAlignVer(NativeExcel.XlVAlign.xlVAlignCenter);
            sheet.SetCellValue("E3", "Môn học và hoạt động giáo dục");
            int startColCap = startCol;
            //nang luc
            sheet.GetRange(3, startColCap, 3, startColCap + 2).Merge();
            sheet.GetRange(3, startColCap, 3, startColCap + 2).SetFontStyle(true, System.Drawing.Color.Black, false, 9, false, false);
            sheet.SetCellValue(3, startColCap, "Năng lực");
            sheet.GetRange(4, startColCap, 5, startColCap).Merge();
            sheet.SetCellValue(4, startColCap, "Tự phục vụ, tự quản");
            sheet.GetRange(4, startColCap + 1, 5, startColCap + 1).Merge();
            sheet.SetCellValue(4, startColCap + 1, "Hợp tác");
            sheet.GetRange(4, startColCap + 2, 5, startColCap + 2).Merge();
            sheet.SetCellValue(4, startColCap + 2, "Tự học, GQVĐ");
            sheet.GetRange(4, startColCap, 5, startColCap + 2).SetFontName("Arial", 7);
            sheet.GetRange(4, startColCap, 5, startColCap + 2).SetHAlign(VTHAlign.xlHAlignCenter);
            sheet.GetRange(4, startColCap, 5, startColCap + 2).SetHAlignVer(NativeExcel.XlVAlign.xlVAlignCenter);
            sheet.GetRange(4, startColCap, 4, startColCap + 2).SetOrientation(90);
            //pham chat
            int startcolQua = startColCap + 3;
            sheet.GetRange(3, startcolQua, 3, startcolQua + 3).Merge();
            sheet.GetRange(3, startcolQua, 3, startcolQua + 3).SetFontStyle(true, System.Drawing.Color.Black, false, 9, false, false);
            sheet.SetCellValue(3, startcolQua, "Phẩm chất");
            sheet.GetRange(4, startcolQua, 5, startcolQua).Merge();
            sheet.SetCellValue(4, startcolQua, "Chăm học, chăm làm");
            sheet.GetRange(4, startcolQua + 1, 5, startcolQua + 1).Merge();
            sheet.SetCellValue(4, startcolQua + 1, "Tự tin, trách nhiệm");
            sheet.GetRange(4, startcolQua + 2, 5, startcolQua + 2).Merge();
            sheet.SetCellValue(4, startcolQua + 2, "Trung thực kỉ luật");
            sheet.GetRange(4, startcolQua + 3, 5, startcolQua + 3).Merge();
            sheet.SetCellValue(4, startcolQua + 3, "Đoàn kết, yêu thương");
            sheet.GetRange(4, startcolQua, 5, startcolQua + 3).SetFontName("Arial", 7);
            sheet.GetRange(4, startcolQua, 5, startcolQua + 3).SetHAlign(VTHAlign.xlHAlignCenter);
            sheet.GetRange(4, startcolQua, 5, startcolQua + 3).SetHAlignVer(NativeExcel.XlVAlign.xlVAlignCenter);
            sheet.GetRange(4, startcolQua, 4, startcolQua + 3).SetOrientation(90);
            int startColNote = 0;
            if (typeSemesterID == SystemParamsInFile.GENERAL_SUMMED_EVALUATION_CKII)
            {
                //khen thuong
                sheet.GetRange(3, startcolQua + 4, 4, startcolQua + 5).Merge();
                sheet.GetRange(3, startcolQua + 4, 4, startcolQua + 5).SetFontStyle(true, System.Drawing.Color.Black, false, 9, false, false);
                sheet.GetRange(3, startcolQua + 4, 4, startcolQua + 5).WrapText();
                sheet.SetCellValue(3, startcolQua + 4, "Khen thưởng");
                if (lstSubject.Count > 11) {
                    sheet.GetRange(3, startcolQua + 4, 3, startcolQua + 4).SetFontName("Arial", 8);
                }
                sheet.SetCellValue(5, startcolQua + 4, "Khen cuối năm");
                sheet.SetCellValue(5, startcolQua + 5, "Khen đột xuất");
                sheet.GetRange(5, startcolQua + 4, 5, startcolQua + 5).SetOrientation(90);
                sheet.GetRange(5, startcolQua + 4, 5, startcolQua + 5).SetFontName("Arial", 7);
                sheet.GetRange(5, startcolQua + 4, 5, startcolQua + 5).SetHAlign(VTHAlign.xlHAlignCenter);
                sheet.GetRange(5, startcolQua + 4, 5, startcolQua + 5).SetHAlignVer(NativeExcel.XlVAlign.xlVAlignCenter);
                //hoan thanh chuong trinh lop hoc
                sheet.GetRange(3, startcolQua + 6, 5, startcolQua + 6).Merge();
                sheet.GetRange(3, startcolQua + 6, 5, startcolQua + 6).WrapText();
                sheet.GetRange(3, startcolQua + 6, 5, startcolQua + 6).SetOrientation(90);
                sheet.GetRange(3, startcolQua + 6, 5, startcolQua + 6).SetFontStyle(true, System.Drawing.Color.Black, false, 8, false, false);
                sheet.SetCellValue(3, startcolQua + 6, "Hoàn thành chương trình lớp học");
                if (lstSubject.Count > 11)
                {
                    sheet.GetRange(3, startcolQua + 6, 3, startcolQua + 6).SetFontName("Arial", 7);
                }
                //len lop
                sheet.GetRange(3, startcolQua + 7, 5, startcolQua + 7).Merge();
                sheet.GetRange(3, startcolQua + 7, 5, startcolQua + 7).WrapText();
                sheet.GetRange(3, startcolQua + 7, 5, startcolQua + 7).SetFontStyle(true, System.Drawing.Color.Black, false, 8, false, false);
                sheet.GetRange(3, startcolQua + 7, 5, startcolQua + 7).SetOrientation(90);
                sheet.SetCellValue(3, startcolQua + 7, "Lên lớp");
                if (lstSubject.Count > 11)
                {
                    sheet.GetRange(3, startcolQua + 7, 3, startcolQua + 7).SetFontName("Arial", 7);
                }
                startColNote = startcolQua + 8;
            }
            else
            {
                startColNote = startcolQua + 4;
            }
            //ghi chu
            sheet.GetRange(3, startColNote, 5, startColNote).Merge();
            sheet.GetRange(3, startColNote, 5, startColNote).SetFontStyle(true, System.Drawing.Color.Black, false, 9, false, false);
            sheet.SetColumnWidth(startColNote, 7.13);
            sheet.SetCellValue(3, startColNote, "Ghi chú");
            sheet.GetRange(2, 1, 2, startColNote).Merge();
            countRow = startColNote - 5;
            #endregion
            #region fill du lieu
            PupilOfClassBO objPOC = null;
            RatedCommentPupilBO objRatedBO = null;
            startCol = 5;
            bool isCK = (typeSemesterID == SystemParamsInFile.GENERAL_SUMMED_EVALUATION_CKI || typeSemesterID == SystemParamsInFile.GENERAL_SUMMED_EVALUATION_CKII) ? true : false;

            #region Xét số lượng học sinh
            if (isPupilInClass == "on")
            {
                //if (lstPOC.Count() > 50)
                //{
                //    lstPOC = lstPOC.Take(50).ToList();
                //    finalNumRow = 50;
                //}
                //else
                //{
                    if (lstPOC.Count() < 10)
                    {
                        finalNumRow = 10;
                    }
                    else
                    {
                        finalNumRow = lstPOC.Count;
                    }
                //}
            }
            else
            {
                if (numberOfRow > 60)
                    numberOfRow = 60;
                lstPOC = lstPOC.Take(numberOfRow).ToList();
                finalNumRow = numberOfRow;
            }

            if (printEnough == 1 || typePrint == 2)
            {
                finalFitIn = finalNumRow > 20 ? 2 : 1;
                if (typePrint == 2) 
                    finalFitIn = 1;
            }
            else
            {
                finalFitIn = printEnough - 1;
            }
            #endregion

            int sumOfListPOC = lstPOC.Count();
            string tick = '\u2713'.ToString();
            for (int i = 0; i < finalNumRow; i++)
            {
                #region Điền dữ liệu

                if (i < sumOfListPOC)
                {
                    objPOC = lstPOC[i];
                    //STT
                    sheet.SetCellValue(startRow, 1, i + 1);
                    sheet.GetRange(startRow, 1, startRow, 1).SetFontName("Arial", 10);
                    //Ho ten
                    sheet.SetCellValue(startRow, 2, objPOC.PupilFullName);
                    sheet.GetRange(startRow, 2, startRow, 2).SetFontName("Arial", 10);
                    //ngay thang nam sinh
                    sheet.SetCellValue(startRow, 3, objPOC.Birthday.ToString("dd/MM/yyyy"));
                    sheet.GetRange(startRow, 3, startRow, 3).SetFontName("Arial", 10);
                    //Gioi tinh
                    sheet.SetCellValue(startRow, 4, objPOC.Genre == GlobalConstants.GENRE_FEMALE ? tick : "");
                    //fill du lieu diem theo mon hoc
                    startCol = 5;
                    #region Dien tung mon
                    for (int j = 0; j < lstSubject.Count; j++)
                    {
                        objCS = lstSubject[j];
                        objRatedBO = lstRatedCommentBO.Where(p => p.PupilID == objPOC.PupilID && p.SubjectID == objCS.SubjectID
                                                            && p.EvaluationID == GlobalConstants.EVALUATION_SUBJECT_AND_EDUCTIONGROUP).FirstOrDefault();
                        if (objRatedBO != null)
                        {

                            sheet.SetCellValue(startRow, startCol, isCK ? (objRatedBO.EndingEvaluation == 1 ? GlobalConstants.EVALUATION_GOOD_COMPLETE : objRatedBO.EndingEvaluation == 2 ? GlobalConstants.EVALUATION_COMPLETE : objRatedBO.EndingEvaluation == 3 ? GlobalConstants.EVALUATION_NOT_COMPLETE : "")
                                            : (objRatedBO.MiddleEvaluation == 1 ? GlobalConstants.EVALUATION_GOOD_COMPLETE : objRatedBO.MiddleEvaluation == 2 ? GlobalConstants.EVALUATION_COMPLETE : objRatedBO.MiddleEvaluation == 3 ? GlobalConstants.EVALUATION_NOT_COMPLETE : ""));

                            if (objCS.IsCommenting == 0 && isEnableMark)
                            {
                                if ((typeSemesterID == SystemParamsInFile.GENERAL_SUMMED_EVALUATION_CKI || typeSemesterID == SystemParamsInFile.GENERAL_SUMMED_EVALUATION_CKII)
                                || objCS.Abbreviation.Equals("Tt") || objCS.Abbreviation.Equals("TV"))
                                {
                                    sheet.SetCellValue(startRow, startCol + 1, isCK ? objRatedBO.PeriodicEndingMark : objRatedBO.PeriodicMiddleMark);
                                    sheet.GetRange(startRow, startCol, startRow, startCol + 1).SetHAlign(VTHAlign.xlHAlignCenter);
                                    startCol += 2;
                                }
                                else
                                {
                                    startCol += 1;
                                }
                            }
                            else
                            {
                                startCol += 1;
                            }
                        }
                        else
                        {
                            if (objCS.IsCommenting == 0 && isEnableMark)
                            {
                                if ((typeSemesterID == SystemParamsInFile.GENERAL_SUMMED_EVALUATION_CKI || typeSemesterID == SystemParamsInFile.GENERAL_SUMMED_EVALUATION_CKII)
                                || objCS.Abbreviation.Equals("Tt") || objCS.Abbreviation.Equals("TV"))
                                {
                                    startCol += 2;
                                }
                                else
                                {
                                    startCol += 1;
                                }
                            }
                            else
                            {
                                startCol += 1;
                            }
                        }
                    }
                    #endregion


                    #region Cac tieu chi con lai
                    //fill nang luc pham chat
                    //tu phuc vu,tu quan
                    objRatedBO = lstRatedCommentBO.Where(p => p.PupilID == objPOC.PupilID && p.EvaluationID == GlobalConstants.EVALUATION_CAPACITY
                                                         && p.SubjectID == 1).FirstOrDefault();
                    if (objRatedBO != null)
                    {
                        sheet.SetCellValue(startRow, startColCap, isCK ? (objRatedBO.CapacityEndingEvaluation == 1 ? GlobalConstants.EVALUATION_GOOD_COMPLETE : objRatedBO.CapacityEndingEvaluation == 2 ? GlobalConstants.EVALUATION_CAPQUA_COMPLETE : objRatedBO.CapacityEndingEvaluation == 3 ? GlobalConstants.EVALUATION_NOT_COMPLETE : "")
                                            : (objRatedBO.CapacityMiddleEvaluation == 1 ? GlobalConstants.EVALUATION_GOOD_COMPLETE : objRatedBO.CapacityMiddleEvaluation == 2 ? GlobalConstants.EVALUATION_CAPQUA_COMPLETE : objRatedBO.CapacityMiddleEvaluation == 3 ? GlobalConstants.EVALUATION_NOT_COMPLETE : ""));
                    }
                    //Hop tac
                    objRatedBO = lstRatedCommentBO.Where(p => p.PupilID == objPOC.PupilID && p.EvaluationID == GlobalConstants.EVALUATION_CAPACITY
                                                         && p.SubjectID == 2).FirstOrDefault();
                    if (objRatedBO != null)
                    {
                        sheet.SetCellValue(startRow, startColCap + 1, isCK ? (objRatedBO.CapacityEndingEvaluation == 1 ? GlobalConstants.EVALUATION_GOOD_COMPLETE : objRatedBO.CapacityEndingEvaluation == 2 ? GlobalConstants.EVALUATION_CAPQUA_COMPLETE : objRatedBO.CapacityEndingEvaluation == 3 ? GlobalConstants.EVALUATION_NOT_COMPLETE : "")
                                        : (objRatedBO.CapacityMiddleEvaluation == 1 ? GlobalConstants.EVALUATION_GOOD_COMPLETE : objRatedBO.CapacityMiddleEvaluation == 2 ? GlobalConstants.EVALUATION_CAPQUA_COMPLETE : objRatedBO.CapacityMiddleEvaluation == 3 ? GlobalConstants.EVALUATION_NOT_COMPLETE : ""));
                    }
                    //Tu hoc, GQVĐ
                    objRatedBO = lstRatedCommentBO.Where(p => p.PupilID == objPOC.PupilID && p.EvaluationID == GlobalConstants.EVALUATION_CAPACITY
                                                         && p.SubjectID == 3).FirstOrDefault();
                    if (objRatedBO != null)
                    {
                        sheet.SetCellValue(startRow, startColCap + 2, isCK ? (objRatedBO.CapacityEndingEvaluation == 1 ? GlobalConstants.EVALUATION_GOOD_COMPLETE : objRatedBO.CapacityEndingEvaluation == 2 ? GlobalConstants.EVALUATION_CAPQUA_COMPLETE : objRatedBO.CapacityEndingEvaluation == 3 ? GlobalConstants.EVALUATION_NOT_COMPLETE : "")
                            : (objRatedBO.CapacityMiddleEvaluation == 1 ? GlobalConstants.EVALUATION_GOOD_COMPLETE : objRatedBO.CapacityMiddleEvaluation == 2 ? GlobalConstants.EVALUATION_CAPQUA_COMPLETE : objRatedBO.CapacityMiddleEvaluation == 3 ? GlobalConstants.EVALUATION_NOT_COMPLETE : ""));
                    }
                    //Pham chat---------------------------------
                    //Cham hoc, cham lam
                    objRatedBO = lstRatedCommentBO.Where(p => p.PupilID == objPOC.PupilID && p.EvaluationID == GlobalConstants.EVALUATION_QUALITY
                                                         && p.SubjectID == 4).FirstOrDefault();
                    if (objRatedBO != null)
                    {
                        sheet.SetCellValue(startRow, startcolQua, isCK ? (objRatedBO.QualityEndingEvaluation == 1 ? GlobalConstants.EVALUATION_GOOD_COMPLETE : objRatedBO.QualityEndingEvaluation == 2 ? GlobalConstants.EVALUATION_CAPQUA_COMPLETE : objRatedBO.QualityEndingEvaluation == 3 ? GlobalConstants.EVALUATION_NOT_COMPLETE : "")
                            : (objRatedBO.QualityMiddleEvaluation == 1 ? GlobalConstants.EVALUATION_GOOD_COMPLETE : objRatedBO.QualityMiddleEvaluation == 2 ? GlobalConstants.EVALUATION_CAPQUA_COMPLETE : objRatedBO.QualityMiddleEvaluation == 3 ? GlobalConstants.EVALUATION_NOT_COMPLETE : ""));
                    }
                    //Tu tin, trach nhiem
                    objRatedBO = lstRatedCommentBO.Where(p => p.PupilID == objPOC.PupilID && p.EvaluationID == GlobalConstants.EVALUATION_QUALITY
                                                         && p.SubjectID == 5).FirstOrDefault();
                    if (objRatedBO != null)
                    {
                        sheet.SetCellValue(startRow, startcolQua + 1, isCK ? (objRatedBO.QualityEndingEvaluation == 1 ? GlobalConstants.EVALUATION_GOOD_COMPLETE : objRatedBO.QualityEndingEvaluation == 2 ? GlobalConstants.EVALUATION_CAPQUA_COMPLETE : objRatedBO.QualityEndingEvaluation == 3 ? GlobalConstants.EVALUATION_NOT_COMPLETE : "")
                            : (objRatedBO.QualityMiddleEvaluation == 1 ? GlobalConstants.EVALUATION_GOOD_COMPLETE : objRatedBO.QualityMiddleEvaluation == 2 ? GlobalConstants.EVALUATION_CAPQUA_COMPLETE : objRatedBO.QualityMiddleEvaluation == 3 ? GlobalConstants.EVALUATION_NOT_COMPLETE : ""));
                    }
                    //Trung thuc, ky luat
                    objRatedBO = lstRatedCommentBO.Where(p => p.PupilID == objPOC.PupilID && p.EvaluationID == GlobalConstants.EVALUATION_QUALITY
                                                         && p.SubjectID == 6).FirstOrDefault();
                    if (objRatedBO != null)
                    {
                        sheet.SetCellValue(startRow, startcolQua + 2, isCK ? (objRatedBO.QualityEndingEvaluation == 1 ? GlobalConstants.EVALUATION_GOOD_COMPLETE : objRatedBO.QualityEndingEvaluation == 2 ? GlobalConstants.EVALUATION_CAPQUA_COMPLETE : objRatedBO.QualityEndingEvaluation == 3 ? GlobalConstants.EVALUATION_NOT_COMPLETE : "")
                            : (objRatedBO.QualityMiddleEvaluation == 1 ? GlobalConstants.EVALUATION_GOOD_COMPLETE : objRatedBO.QualityMiddleEvaluation == 2 ? GlobalConstants.EVALUATION_CAPQUA_COMPLETE : objRatedBO.QualityMiddleEvaluation == 3 ? GlobalConstants.EVALUATION_NOT_COMPLETE : ""));
                    }
                    //Doan ket, yeu thuong
                    objRatedBO = lstRatedCommentBO.Where(p => p.PupilID == objPOC.PupilID && p.EvaluationID == GlobalConstants.EVALUATION_QUALITY
                                                         && p.SubjectID == 7).FirstOrDefault();
                    if (objRatedBO != null)
                    {
                        sheet.SetCellValue(startRow, startcolQua + 3, isCK ? (objRatedBO.QualityEndingEvaluation == 1 ? GlobalConstants.EVALUATION_GOOD_COMPLETE : objRatedBO.QualityEndingEvaluation == 2 ? GlobalConstants.EVALUATION_CAPQUA_COMPLETE : objRatedBO.QualityEndingEvaluation == 3 ? GlobalConstants.EVALUATION_NOT_COMPLETE : "")
                            : (objRatedBO.QualityMiddleEvaluation == 1 ? GlobalConstants.EVALUATION_GOOD_COMPLETE : objRatedBO.QualityMiddleEvaluation == 2 ? GlobalConstants.EVALUATION_CAPQUA_COMPLETE : objRatedBO.QualityMiddleEvaluation == 3 ? GlobalConstants.EVALUATION_NOT_COMPLETE : ""));
                    }
                    sheet.GetRange(startRow, startColCap, startRow, startcolQua + 3).SetHAlign(VTHAlign.xlHAlignCenter);
                    if (typeSemesterID == 4)
                    {

                        //khen thuong
                        int countRewarCN = 0;
                        int countSee = 0;
                        int countER = 0;
                        countSee = lstSEEBO.Where(p => p.PupilID == objPOC.PupilID).Where(p => p.RewardID.HasValue && p.RewardID.Value > 0).Count();
                        countER = lstER.Where(p => p.PupilID == objPOC.PupilID && p.RewardID == 2 && p.SemesterID == GlobalConstants.SEMESTER_OF_YEAR_SECOND).Count();
                        countRewarCN = countSee + countER;
                        //khen thuong cuoi nam
                        sheet.GetRange(startRow, startcolQua + 4, startRow, startcolQua + 4).SetHAlign(VTHAlign.xlHAlignCenter);
                        sheet.SetCellValue(startRow, startcolQua + 4, countRewarCN > 0 ? tick : "");
                        //khen dot xuat
                        countER = lstER.Where(p => p.PupilID == objPOC.PupilID && p.RewardID == 1).Count();
                        sheet.GetRange(startRow, startcolQua + 5, startRow, startcolQua + 5).SetHAlign(VTHAlign.xlHAlignCenter);
                        sheet.SetCellValue(startRow, startcolQua + 5, countER > 0 ? tick : "");

                        //hoan thanh chuong trinh lop hoc
                        int countHT = lstSEEBO.Where(p => p.PupilID == objPOC.PupilID
                                                        && (p.EndingEvaluation == "HT" || p.RateAdd == 1)).Count();
                        sheet.GetRange(startRow, startcolQua + 6, startRow, startcolQua + 6).SetHAlign(VTHAlign.xlHAlignCenter);
                        sheet.SetCellValue(startRow, startcolQua + 6, countHT > 0 ? tick : "");

                        //len lop

                        sheet.GetRange(startRow, startcolQua + 7, startRow, startcolQua + 7).SetHAlign(VTHAlign.xlHAlignCenter);
                        sheet.SetCellValue(startRow, startcolQua + 7, countHT > 0 ? tick : "");

                    }
                    //ghi chu
                    if (objPOC.Status != GlobalConstants.PUPIL_STATUS_STUDYING)
                    {
                        sheet.GetRange(startRow, 1, startRow, startcolQua + 8).SetFontStyle(false, System.Drawing.Color.Red, false, 10, true, false);
                    }
                    #endregion
                }
                else
                {
                    sheet.SetCellValue(startRow, 1, i + 1);
                    sheet.GetRange(startRow, 1, startRow, 1).SetFontName("Arial", 10);
                }              
                #endregion

                //xu ly dat do cao cua dong
                double standard1PageTotalHeight = ((double)26.0) * 16;
                standard1PageTotalHeight = standard1PageTotalHeight - 17 * 5;
                if (typePrint == 2) 
                {
                    standard1PageTotalHeight = ((double)31.5) * 30;
                }
                double standard2PageTotalHeight = ((double)26.0) * 30;
                double rowHeight;

                if (finalFitIn == 1)
                {
                    rowHeight = standard1PageTotalHeight / ((double)finalNumRow);
                    if(typePrint == 2)
                        rowHeight = standard1PageTotalHeight / ((double)finalNumRow);
                }
                else
                {
                    rowHeight = standard2PageTotalHeight / (double)finalNumRow;
                }
                sheet.SetRowHeight(startRow, rowHeight);
                startRow++;
            }
            sheet.GetRange(6, 1, startRow - 1, 100).SetFontName("Arial", 9);
            sheet.GetRange(6, 1, startRow - 1, 100).SetHAlign(VTHAlign.xlHAlignCenter);
            sheet.GetRange(6, 1, startRow - 1, 100).SetHAlignVer(NativeExcel.XlVAlign.xlVAlignCenter);
            sheet.GetRange(6, 2, startRow - 1, 2).SetHAlign(VTHAlign.xlHAlignLeft);
            #endregion
            #region Điền Footer          
            int rowTeacherName = 6 + finalNumRow;
            sheet.GetRange(3, 1, rowTeacherName - 1, startColNote).SetBorder(VTBorderStyle.Solid, VTBorderWeight.Thin, VTBorderIndex.All);
            string HeadTeacherName = lstPOC.Count > 0 ? lstPOC.FirstOrDefault().HeadTeacherName : "";
            sheet.SetCellValue(rowTeacherName, startColNote - 4, "GV.chủ nhiệm");
            sheet.GetRange(rowTeacherName, startColNote - 4, rowTeacherName, startColNote - 4).SetHAlign(VTHAlign.xlHAlignCenter);
            sheet.GetRange(rowTeacherName, startColNote - 4, rowTeacherName, startColNote - 4).SetFontStyle(true, System.Drawing.Color.Black, false, 10, false, false);
            sheet.SetCellValue(rowTeacherName + 1, startColNote - 4, "(ký, ghi rõ họ tên)");
            sheet.GetRange(rowTeacherName + 1, startColNote - 4, rowTeacherName + 1, startColNote - 4).SetHAlign(VTHAlign.xlHAlignCenter);
            sheet.GetRange(rowTeacherName + 1, startColNote - 4, rowTeacherName + 1, startColNote - 4).SetFontStyle(true, System.Drawing.Color.Black, false, 10, false, false);           
            //sheet.SetRowHeight(rowTeacherName + 3, 0);
            sheet.SetCellValue(rowTeacherName + 4, startColNote - 4, HeadTeacherName);
            sheet.GetRange(rowTeacherName + 4, startColNote - 4, rowTeacherName + 4, startColNote - 4).SetHAlign(VTHAlign.xlHAlignCenter);

            //hieu truong
            if (typeSemesterID == 4)
            {
                sheet.SetCellValue("C" + rowTeacherName, "Hiệu trưởng");
                sheet.GetRange("C" + rowTeacherName, "C" + rowTeacherName).SetHAlign(VTHAlign.xlHAlignCenter);
                sheet.GetRange("C" + rowTeacherName, "C" + rowTeacherName).SetFontStyle(true, System.Drawing.Color.Black, false, 10, false, false);
                sheet.SetCellValue("C" + (rowTeacherName + 1), "(ký, ghi rõ họ tên, đóng dấu)");
                sheet.GetRange("C" + (rowTeacherName + 1), "C" + (rowTeacherName + 1)).SetHAlign(VTHAlign.xlHAlignCenter);
                sheet.GetRange("C" + (rowTeacherName + 1), "C" + (rowTeacherName + 1)).SetFontStyle(true, System.Drawing.Color.Black, false, 10, false, false);
                sheet.SetCellValue("C" + (rowTeacherName + 4), headMasterName);
                sheet.GetRange("C" + (rowTeacherName + 4), "C" + (rowTeacherName + 4)).SetHAlign(VTHAlign.xlHAlignCenter);
            }
            #endregion
            // Xét độ rộng cột
            double colWidth = (double)totalWidth / (double)countRow;
            for (int k = 1; k <= countRow; k++)
            {
                sheet.SetColumnWidth(k + 4, colWidth);
            }
            sheet.FitAllColumnsOnOnePage = true;
            sheet.Name = pageName;
        }

        //public Stream gggggg(IDictionary<string, object> dic, ClassProfile objCP, List<string> lstDataPrint,
        //                                    List<PupilOfClassBO> lstPOC, IVTWorkbook oBook, AcademicYear objAy, SchoolProfile objSP,
        //                                    List<ClassSubjectBO> lstSubject, List<RatedCommentPupilBO> lstRatedCommentPupilBO,
        //                                    List<SummedEndingEvaluationBO> lstSEE, List<EvaluationReward> lstEvaluationReward)
        //{
        //    string IsPupilInClass = Utils.GetString(dic, "IsPupilInClass");
        //    int NumberOfRow = Utils.GetInt(dic, "NumberOfRow");
        //    int PrintEnough = Utils.GetInt(dic, "PrintEnough");
        //    int TypePrint = Utils.GetInt(dic, "TypePrint");
        //    Dictionary<string, object> dicReport = null;
        //    List<RatedCommentPupilBO> lstRatedtmpHK1 = new List<RatedCommentPupilBO>();
        //    List<RatedCommentPupilBO> lstRatedtmpHK2 = new List<RatedCommentPupilBO>();
        //    List<SummedEndingEvaluationBO> lstSummedEndingtmp = null;
        //    List<EvaluationReward> lstEvaluationRewardtmp = null;
        //    List<PupilOfClassBO> lstPOCtmp = null;
        //    IVTWorksheet CurrentSheet1 = oBook.GetSheet(1);
        //    IVTWorksheet CurrentSheet2 = oBook.GetSheet(2);
        //    IVTWorksheet sheetGK1 = lstDataPrint[0] == "on" ? oBook.CopySheetToLast(CurrentSheet2, "CZ1000") : null;
        //    IVTWorksheet sheetHK1 = lstDataPrint[1] == "on" ? oBook.CopySheetToLast(CurrentSheet2, "CZ1000") : null;
        //    IVTWorksheet sheetGK2 = lstDataPrint[2] == "on" ? oBook.CopySheetToLast(CurrentSheet2, "CZ1000") : null;
        //    IVTWorksheet sheetHK2 = lstDataPrint[3] == "on" ? oBook.CopySheetToLast(CurrentSheet2, "CZ1000") : null;          

        //    int ClassID = objCP.ClassProfileID;
        //    lstPOCtmp = lstPOC.Where(p => p.ClassID == ClassID).ToList();
        //    List<ClassSubjectBO> lstCStmpHK1 = lstSubject.Where(p => p.ClassID == ClassID && p.SectionPerWeekFirstSemester > 0).ToList();
        //    List<ClassSubjectBO> lstCStmpHK2 = lstSubject.Where(p => p.ClassID == ClassID && p.SectionPerWeekFirstSemester > 0).ToList();
        //    int countSubjectHK1 = lstCStmpHK1.Count();
        //    int countSubjectHK2 = lstCStmpHK2.Count();
        //    double widthColumnGK1 = GetWidth(countSubjectHK1, 0, TypePrint);
        //    double widthColumnHK1 = GetWidth(countSubjectHK1, 1, TypePrint);
        //    double widthColumnGK2 = GetWidth(countSubjectHK2, 2, TypePrint);
        //    double widthColumnHK2 = GetWidth(countSubjectHK2, 4, TypePrint);

        //    lstRatedtmpHK1 = lstRatedCommentPupilBO.Where(p => p.ClassID == ClassID && p.SemesterID == 1).ToList();
        //    lstRatedtmpHK2 = lstRatedCommentPupilBO.Where(p => p.ClassID == ClassID && p.SemesterID == 2).ToList();
        //    lstSummedEndingtmp = lstSEE.Where(p => p.ClassID == ClassID).ToList();
        //    lstEvaluationRewardtmp = lstEvaluationReward.Where(p => p.ClassID == ClassID).ToList();

        //    int countRow = 0;
        //    int finalNumRow = 0;
        //    int finalFitIn = 0;

        //    #region fill thong tin chung
        //    //fill title
        //    ClassSubjectBO objCS = null;
        //    string className = objCP.DisplayName;
        //    string title = "BẢNG TỔNG HỢP KẾT QUẢ ĐÁNH GIÁ GIÁO DỤC ";
        //    if (1 == SystemParamsInFile.GENERAL_SUMMED_EVALUATION_GKI)
        //    {
        //        title += "GIỮA HỌC KỲ I";
        //    }
        //    else if (1 == SystemParamsInFile.GENERAL_SUMMED_EVALUATION_GKII)
        //    {
        //        title += "GIỮA HỌC KỲ II";
        //    }
        //    else if (1 == SystemParamsInFile.GENERAL_SUMMED_EVALUATION_CKI)
        //    {
        //        title += "CUỐI HỌC KỲ I";
        //    }
        //    else if (1 == SystemParamsInFile.GENERAL_SUMMED_EVALUATION_CKII)
        //    {
        //        title += "CUỐI NĂM HỌC";
        //    }
        //    title += " NĂM HỌC " + objAy.DisplayTitle + " LỚP: " + className + " TRƯỜNG: " + objSP.SchoolName;
        //    sheetGK1.SetCellValue("A2", title.ToUpper());
        //    sheetHK1.SetCellValue("A2", title.ToUpper());
        //    sheetGK2.SetCellValue("A2", title.ToUpper());
        //    sheetHK2.SetCellValue("A2", title.ToUpper());
        //    int startCol = 5;
        //    int startRow = 6;
        //    #endregion

        //    #region fill tieu de
        //    bool isEnableMark;
        //    byte fontSize = 0;
        //    for (int i = 0; i < lstCStmpHK1.Count; i++)
        //    {
        //        objCS = lstSubject[i];
        //        sheet.GetRange(4, startCol, 4, (objCS.IsCommenting == 0 && isEnableMark
        //            && ((typeSemesterID == SystemParamsInFile.GENERAL_SUMMED_EVALUATION_CKI || typeSemesterID == SystemParamsInFile.GENERAL_SUMMED_EVALUATION_CKII)
        //                || objCS.Abbreviation.Equals("Tt") || objCS.Abbreviation.Equals("TV"))) ? startCol + 1 : startCol).Merge();
        //        sheet.SetCellValue(4, startCol, objCS.DisplayName);
        //        fontSize = 7;
        //        if (objCS.Abbreviation.Equals("TNXH"))
        //        {
        //            fontSize = 6;
        //        }

        //        sheet.GetRange(4, startCol, 4, startCol).SetFontName("Arial", fontSize);
        //        sheet.GetRange(4, startCol, 4, startCol).WrapText();
        //        sheet.SetCellValue(5, startCol, "Mức đạt được");
        //        sheet.GetRange(5, startCol, 5, startCol).SetOrientation(90);
        //        sheet.GetRange(5, startCol, 5, startCol).SetFontName("Arial", 7);
        //        if (objCS.IsCommenting == 0 && isEnableMark)
        //        {
        //            if ((typeSemesterID == SystemParamsInFile.GENERAL_SUMMED_EVALUATION_CKI || typeSemesterID == SystemParamsInFile.GENERAL_SUMMED_EVALUATION_CKII)
        //                || objCS.Abbreviation.Equals("Tt") || objCS.Abbreviation.Equals("TV"))
        //            {
        //                sheet.SetCellValue(5, startCol + 1, "Điểm KTĐK");
        //                sheet.GetRange(5, startCol + 1, 5, startCol + 1).SetOrientation(90);
        //                startCol += 2;
        //            }
        //            else
        //            {
        //                startCol += 1;
        //            }
        //        }
        //        else
        //        {
        //            startCol += 1;
        //        }
        //    }
        //    sheet.GetRange(3, 5, 3, startCol - 1).Merge();
        //    sheet.GetRange(3, 5, 3, startCol - 1).SetFontStyle(true, System.Drawing.Color.Black, false, 9, false, false);
        //    sheet.GetRange(5, 5, 5, startCol - 1).SetFontName("Arial", 7);
        //    sheet.GetRange(5, 5, 5, startCol - 1).SetHAlign(VTHAlign.xlHAlignCenter);
        //    sheet.GetRange(5, 5, 5, startCol - 1).SetHAlignVer(NativeExcel.XlVAlign.xlVAlignCenter);
        //    sheet.SetCellValue("E3", "Môn học và hoạt động giáo dục");
        //    int startColCap = startCol;
        //    //nang luc
        //    sheet.GetRange(3, startColCap, 3, startColCap + 2).Merge();
        //    sheet.GetRange(3, startColCap, 3, startColCap + 2).SetFontStyle(true, System.Drawing.Color.Black, false, 9, false, false);
        //    sheet.SetCellValue(3, startColCap, "Năng lực");
        //    sheet.GetRange(4, startColCap, 5, startColCap).Merge();
        //    sheet.SetCellValue(4, startColCap, "Tự phục vụ, tự quản");
        //    sheet.GetRange(4, startColCap + 1, 5, startColCap + 1).Merge();
        //    sheet.SetCellValue(4, startColCap + 1, "Hợp tác");
        //    sheet.GetRange(4, startColCap + 2, 5, startColCap + 2).Merge();
        //    sheet.SetCellValue(4, startColCap + 2, "Tự học, GQVĐ");
        //    sheet.GetRange(4, startColCap, 5, startColCap + 2).SetFontName("Arial", 7);
        //    sheet.GetRange(4, startColCap, 5, startColCap + 2).SetHAlign(VTHAlign.xlHAlignCenter);
        //    sheet.GetRange(4, startColCap, 5, startColCap + 2).SetHAlignVer(NativeExcel.XlVAlign.xlVAlignCenter);
        //    sheet.GetRange(4, startColCap, 4, startColCap + 2).SetOrientation(90);
        //    //pham chat
        //    int startcolQua = startColCap + 3;
        //    sheet.GetRange(3, startcolQua, 3, startcolQua + 3).Merge();
        //    sheet.GetRange(3, startcolQua, 3, startcolQua + 3).SetFontStyle(true, System.Drawing.Color.Black, false, 9, false, false);
        //    sheet.SetCellValue(3, startcolQua, "Phẩm chất");
        //    sheet.GetRange(4, startcolQua, 5, startcolQua).Merge();
        //    sheet.SetCellValue(4, startcolQua, "Chăm học, chăm làm");
        //    sheet.GetRange(4, startcolQua + 1, 5, startcolQua + 1).Merge();
        //    sheet.SetCellValue(4, startcolQua + 1, "Tự tin, trách nhiệm");
        //    sheet.GetRange(4, startcolQua + 2, 5, startcolQua + 2).Merge();
        //    sheet.SetCellValue(4, startcolQua + 2, "Trung thực kỉ luật");
        //    sheet.GetRange(4, startcolQua + 3, 5, startcolQua + 3).Merge();
        //    sheet.SetCellValue(4, startcolQua + 3, "Đoàn kết, yêu thương");
        //    sheet.GetRange(4, startcolQua, 5, startcolQua + 3).SetFontName("Arial", 7);
        //    sheet.GetRange(4, startcolQua, 5, startcolQua + 3).SetHAlign(VTHAlign.xlHAlignCenter);
        //    sheet.GetRange(4, startcolQua, 5, startcolQua + 3).SetHAlignVer(NativeExcel.XlVAlign.xlVAlignCenter);
        //    sheet.GetRange(4, startcolQua, 4, startcolQua + 3).SetOrientation(90);
        //    int startColNote = 0;
        //    if (typeSemesterID == SystemParamsInFile.GENERAL_SUMMED_EVALUATION_CKII)
        //    {
        //        //khen thuong
        //        sheet.GetRange(3, startcolQua + 4, 4, startcolQua + 5).Merge();
        //        sheet.GetRange(3, startcolQua + 4, 4, startcolQua + 5).SetFontStyle(true, System.Drawing.Color.Black, false, 9, false, false);
        //        sheet.GetRange(3, startcolQua + 4, 4, startcolQua + 5).WrapText();
        //        sheet.SetCellValue(3, startcolQua + 4, "Khen thưởng");
        //        if (lstSubject.Count > 11)
        //        {
        //            sheet.GetRange(3, startcolQua + 4, 3, startcolQua + 4).SetFontName("Arial", 8);
        //        }
        //        sheet.SetCellValue(5, startcolQua + 4, "Khen cuối năm");
        //        sheet.SetCellValue(5, startcolQua + 5, "Khen đột xuất");
        //        sheet.GetRange(5, startcolQua + 4, 5, startcolQua + 5).SetOrientation(90);
        //        sheet.GetRange(5, startcolQua + 4, 5, startcolQua + 5).SetFontName("Arial", 7);
        //        sheet.GetRange(5, startcolQua + 4, 5, startcolQua + 5).SetHAlign(VTHAlign.xlHAlignCenter);
        //        sheet.GetRange(5, startcolQua + 4, 5, startcolQua + 5).SetHAlignVer(NativeExcel.XlVAlign.xlVAlignCenter);
        //        //hoan thanh chuong trinh lop hoc
        //        sheet.GetRange(3, startcolQua + 6, 5, startcolQua + 6).Merge();
        //        sheet.GetRange(3, startcolQua + 6, 5, startcolQua + 6).WrapText();
        //        sheet.GetRange(3, startcolQua + 6, 5, startcolQua + 6).SetOrientation(90);
        //        sheet.GetRange(3, startcolQua + 6, 5, startcolQua + 6).SetFontStyle(true, System.Drawing.Color.Black, false, 8, false, false);
        //        sheet.SetCellValue(3, startcolQua + 6, "Hoàn thành chương trình lớp học");
        //        if (lstSubject.Count > 11)
        //        {
        //            sheet.GetRange(3, startcolQua + 6, 3, startcolQua + 6).SetFontName("Arial", 7);
        //        }
        //        //len lop
        //        sheet.GetRange(3, startcolQua + 7, 5, startcolQua + 7).Merge();
        //        sheet.GetRange(3, startcolQua + 7, 5, startcolQua + 7).WrapText();
        //        sheet.GetRange(3, startcolQua + 7, 5, startcolQua + 7).SetFontStyle(true, System.Drawing.Color.Black, false, 8, false, false);
        //        sheet.GetRange(3, startcolQua + 7, 5, startcolQua + 7).SetOrientation(90);
        //        sheet.SetCellValue(3, startcolQua + 7, "Lên lớp");
        //        if (lstSubject.Count > 11)
        //        {
        //            sheet.GetRange(3, startcolQua + 7, 3, startcolQua + 7).SetFontName("Arial", 7);
        //        }
        //        startColNote = startcolQua + 8;
        //    }
        //    else
        //    {
        //        startColNote = startcolQua + 4;
        //    }
        //    //ghi chu
        //    sheet.GetRange(3, startColNote, 5, startColNote).Merge();
        //    sheet.GetRange(3, startColNote, 5, startColNote).SetFontStyle(true, System.Drawing.Color.Black, false, 9, false, false);
        //    sheet.SetColumnWidth(startColNote, 7.13);
        //    sheet.SetCellValue(3, startColNote, "Ghi chú");
        //    sheet.GetRange(2, 1, 2, startColNote).Merge();
        //    countRow = startColNote - 5;
        //    #endregion

        //    return oBook.ToStream();
        //}

        public double GetWidth(int countSubject, int index, int typePrint) 
        {
            double widthColumn = 0.0;

            if (index == 3)
            {
                if (countSubject <= 6)
                    widthColumn = 100;
                else if (countSubject > 6 && countSubject <= 12)
                    widthColumn = 70.0;
                else if (countSubject > 12 && countSubject <= 16)
                    widthColumn = 65.0;
                else
                {
                    widthColumn = 60.0;
                }
            }
            else
            {
                if (typePrint == 1)
                {
                    if (countSubject <= 6)
                        widthColumn = 80;
                    else if (countSubject > 6 && countSubject <= 16)
                        widthColumn = 75;
                    else
                    {
                        widthColumn = 70.0;
                    }
                }
                else
                {
                    if (index == 1) 
                    {

                        if (countSubject <= 12)
                            widthColumn = 75;
                        else if (countSubject > 12 && countSubject <= 14)
                            widthColumn = 70;
                        else if (countSubject > 14 && countSubject <= 16)
                            widthColumn = 65;
                        else if (countSubject > 16 && countSubject <= 18)
                            widthColumn = 68;
                        else if (countSubject > 18)
                            widthColumn = 60;
                        
                    }
                    else
                    {
                        if (countSubject <= 6)
                            widthColumn = 80;
                        else if (countSubject > 6 && countSubject <= 16)
                            widthColumn = 75;
                        else
                        {
                            widthColumn = 70.0;
                        }
                    }
                }
            }
            return widthColumn;
        }

        #endregion

        #region Export Báo cao hoc sinh luu ban
        public ProcessedReport GetProcessedRepeaterReport(IDictionary<string, object> dic)
        {
            string reportCode = Utils.GetString(dic, "ReportCode");
            int AcademicYearID = Utils.GetInt(dic, "AcademicYearID");
            int SchoolID = Utils.GetInt(dic, "SchoolID");
            int EducationLevelID = Utils.GetInt(dic, "EducationLevelID");
            int ClassID = Utils.GetInt(dic, "ClassID");
            string inputParameterHashKey = this.GetHashKeyRepeaterReport(AcademicYearID, SchoolID, EducationLevelID, ClassID);
            return ProcessedReportBusiness.GetProcessedReport(reportCode, inputParameterHashKey);
        }
        public Stream GetRepeaterReport(IDictionary<string, object> dic)
        {
            int SchoolID = Utils.GetInt(dic, "SchoolID");
            int AcademicYearID = Utils.GetInt(dic, "AcademicYearID");
            int EducationLevelID = Utils.GetInt(dic, "EducationLevelID");
            int AppliedLevelID = Utils.GetInt(dic, "AppliedLevelID");
            int ClassID = Utils.GetInt(dic, "ClassID");
            int TypeExport = Utils.GetInt(dic, "TypeExport");
            string reportCode = Utils.GetString(dic, "ReportCode");
            ReportDefinition reportDef = ReportDefinitionBusiness.GetByCode(reportCode);
            string templateName = reportDef.TemplateName;
            string templatePath = SystemParamsInFile.TEMPLATE_FOLDER + "/" + "HS" + "/" + templateName;
            #region Doc va lay du lieu
            IVTWorkbook oBook = VTExport.OpenWorkbook(templatePath);
            IVTWorksheet Currentsheet = oBook.GetSheet(1);
            SchoolProfile objSP = SchoolProfileBusiness.Find(SchoolID);
            AcademicYear objAy = AcademicYearBusiness.Find(AcademicYearID);
            ClassProfile objCP = new ClassProfile();
            string ClassName = "";
            if (ClassID > 0)
            {
                objCP = ClassProfileBusiness.Find(ClassID);
                ClassName = "Lớp " + objCP.DisplayName + " - ";
            }
            Currentsheet.SetCellValue("A2", objSP.SchoolName.ToUpper());
            string Title = string.Empty;
            Title =(EducationLevelID == 0 ? "" : "Khối " + EducationLevelID.ToString() + " ") + ClassName + "Năm học " + objAy.DisplayTitle;
            Currentsheet.SetCellValue("A4", Title.ToUpper());

            IDictionary<string, object> dicPOC = new Dictionary<string, object>()
            {
                {"SchoolID",SchoolID},
                {"AcademicYearID",AcademicYearID},
                {"AppliedLevelID",AppliedLevelID},
                {"EducationLevelID",EducationLevelID},
                {"ClassID",ClassID}
            };
            IQueryable<PupilOfClassBO> lstPOC = (from poc in PupilOfClassBusiness.Search(dicPOC)
                                                 join pf in PupilProfileBusiness.All on poc.PupilID equals pf.PupilProfileID
                                                 join cp in ClassProfileBusiness.All on poc.ClassID equals cp.ClassProfileID
                                                 where poc.Status == GlobalConstants.PUPIL_STATUS_STUDYING
                                                 && cp.IsActive.HasValue && cp.IsActive.Value
                                                 select new PupilOfClassBO
                                                 {
                                                     PupilID = poc.PupilID,
                                                     PupilCode = pf.PupilCode,
                                                     PupilFullName = pf.FullName,
                                                     ClassID = poc.ClassID,
                                                     EducationLevelID = cp.EducationLevelID,
                                                     ClassName = cp.DisplayName,
                                                     ClassOrder = cp.OrderNumber,
                                                     OrderInClass = poc.OrderInClass
                                                 }).OrderBy(p => p.EducationLevelID).ThenBy(p => p.ClassOrder)
                                                 .ThenBy(p => ClassName).ThenBy(p => p.OrderInClass)
                                                 .ThenBy(p => p.PupilFullName);

            List<int> lstPupilID = lstPOC.Select(p => p.PupilID).Distinct().ToList();

            IDictionary<string, object> dicSEE = new Dictionary<string, object>()
            {
                {"AcademicYearID",AcademicYearID},
                {"SchoolID",SchoolID},
                {"SemesterID",GlobalConstants.SEMESTER_OF_EVALUATION_RESULT},
                {"AppliedLevelID",AppliedLevelID},
                {"EducationLevelID",EducationLevelID},
                {"ClassID",ClassID},
                {"lstPupilID",lstPupilID}
            };
            IQueryable<SummedEndingEvaluationBO> iquerySEEBO = this.Search(dic);
            
            #endregion
            #region Fill du lieu
            List<SummedEndingEvaluationBO> lstResult = null;
            PupilOfClassBO objPOC = null;
            List<PupilOfClassBO> lsttmpPOC = null;
            if (TypeExport == 1)
            {
                #region Hoc sinh luu ban
                int startRow = 7;

                lstResult = iquerySEEBO.Where(p => p.RateAdd.HasValue && p.RateAdd.Value == 0).ToList();
                List<int> lstPupilRepeaterID = lstResult.Select(p => p.PupilID).Distinct().ToList();
                lsttmpPOC = lstPOC.Where(p => lstPupilRepeaterID.Contains(p.PupilID)).ToList();
                for (int i = 0; i < lsttmpPOC.Count; i++)
                {
                    objPOC = lsttmpPOC[i];
                    Currentsheet.SetCellValue(startRow, 1, (i + 1));
                    Currentsheet.SetCellValue(startRow, 2, objPOC.PupilCode);
                    Currentsheet.SetCellValue(startRow, 3, objPOC.PupilFullName);
                    Currentsheet.SetCellValue(startRow, 4, objPOC.ClassName);
                    startRow++;
                }
                //ke khung
                if (lstResult.Count > 0)
                {
                    Currentsheet.GetRange(7, 1, startRow - 1, 5).SetBorder(VTBorderStyle.Solid, VTBorderWeight.Thin, VTBorderIndex.All);
                }
                #endregion
            }
            else if (TypeExport == 2)
            {
                #region Hoc sinh danh gia bo sung
                SummedEndingEvaluationBO objSEEBO = null;
                List<RatedCommentPupilBO> lstRatedCommentPupil = new List<RatedCommentPupilBO>();
                IDictionary<string, object> dicRate = new Dictionary<string, object>()
                {
                    {"AcademicYearID",AcademicYearID},
                    {"SchoolID",SchoolID},
                    {"ClassID",ClassID},
                    {"lstPupilID",lstPupilID},
                    {"SemesterID",GlobalConstants.SEMESTER_OF_YEAR_SECOND}
                };
                //lstRatedCommentPupil = RatedCommentPupilBusiness.Search(dicRate).ToList();                
                if (UtilsBusiness.IsMoveHistory(objAy))
                {
                    lstRatedCommentPupil = (from rh in RatedCommentPupilHistoryBusiness.Search(dicRate)
                                            select new RatedCommentPupilBO
                                            {
                                                SchoolID = rh.SchoolID,
                                                AcademicYearID = rh.AcademicYearID,
                                                PupilID = rh.PupilID,
                                                ClassID = rh.ClassID,
                                                SubjectID = rh.SubjectID,
                                                SemesterID = rh.SemesterID,
                                                EvaluationID = rh.EvaluationID,
                                                PeriodicMiddleMark = rh.PeriodicMiddleMark,
                                                PeriodicEndingMark = rh.PeriodicEndingMark,
                                                PeriodicMiddleJudgement = rh.PeriodicMiddleJudgement,
                                                PeriodicEndingJudgement = rh.PeriodicEndingJudgement,
                                                MiddleEvaluation = rh.MiddleEvaluation,
                                                EndingEvaluation = rh.EndingEvaluation,
                                                CapacityMiddleEvaluation = rh.CapacityMiddleEvaluation,
                                                CapacityEndingEvaluation = rh.CapacityEndingEvaluation,
                                                QualityMiddleEvaluation = rh.QualityMiddleEvaluation,
                                                QualityEndingEvaluation = rh.QualityEndingEvaluation,
                                                Comment = rh.Comment,
                                                CreateTime = rh.CreateTime,
                                                UpdateTime = rh.UpdateTime,
                                                FullNameComment = rh.FullNameComment,
                                                LogChangeID = rh.LogChangeID,
                                            }).ToList();
                }
                else
                {
                    lstRatedCommentPupil = (from rh in RatedCommentPupilBusiness.Search(dicRate)
                                            select new RatedCommentPupilBO
                                            {
                                                SchoolID = rh.SchoolID,
                                                AcademicYearID = rh.AcademicYearID,
                                                PupilID = rh.PupilID,
                                                ClassID = rh.ClassID,
                                                SubjectID = rh.SubjectID,
                                                SemesterID = rh.SemesterID,
                                                EvaluationID = rh.EvaluationID,
                                                PeriodicMiddleMark = rh.PeriodicMiddleMark,
                                                PeriodicEndingMark = rh.PeriodicEndingMark,
                                                PeriodicMiddleJudgement = rh.PeriodicMiddleJudgement,
                                                PeriodicEndingJudgement = rh.PeriodicEndingJudgement,
                                                MiddleEvaluation = rh.MiddleEvaluation,
                                                EndingEvaluation = rh.EndingEvaluation,
                                                CapacityMiddleEvaluation = rh.CapacityMiddleEvaluation,
                                                CapacityEndingEvaluation = rh.CapacityEndingEvaluation,
                                                QualityMiddleEvaluation = rh.QualityMiddleEvaluation,
                                                QualityEndingEvaluation = rh.QualityEndingEvaluation,
                                                Comment = rh.Comment,
                                                CreateTime = rh.CreateTime,
                                                UpdateTime = rh.UpdateTime,
                                                FullNameComment = rh.FullNameComment,
                                                LogChangeID = rh.LogChangeID
                                            }).ToList();
                }


                Dictionary<string, object> dicSubject = new Dictionary<string, object>
                    {
                        {"AcademicYearID", AcademicYearID},
                        {"SchoolID", SchoolID},
                        {"AppliedLevel", AppliedLevelID},
                        {"EducationLevelID",EducationLevelID},
                        {"ClassID",ClassID}
                    };
                List<ClassSubject> lstCSALL = ClassSubjectBusiness.SearchBySchool(SchoolID, dicSubject)
                                                               .OrderBy(p => p.IsCommenting)
                                                               .ThenBy(p=>p.SubjectCat.OrderInSubject)
                                                               .ThenBy(p => p.SubjectCat.DisplayName).ToList();
                var lstClassSubjectAll = (from cs in lstCSALL
                                          select new
                                          {
                                              SubjectID = cs.SubjectID,
                                              SubjectName = cs.SubjectCat.DisplayName,
                                              IsCommenting = cs.IsCommenting
                                          }).Distinct().ToList();
                //var CSGroup = (from cs in lstClassSubjectAll
                //               group cs by new
                //               {
                //                   cs.ClassProfile.EducationLevelID,
                //                   cs.ClassID
                //               } into gcs
                //               select new
                //               {
                //                   EducationLevelID = gcs.Key.EducationLevelID,
                //                   ClassID = gcs.Key.ClassID,
                //                   Count = gcs.Count()
                //               }).OrderByDescending(p => p.Count).ToList();

                //if (ClassID == 0)
                //{
                //    ClassID = CSGroup.FirstOrDefault().ClassID;
                //}
                ////lay ra danh sach mon cua lop dau tien
                //List<ClassSubject> lstCStmp = lstClassSubjectAll.Where(p => p.ClassID == ClassID).ToList();
                List<EvaluationCriteria> lstEvaluationCriteria = EvaluationCriteriaBusiness.All.OrderBy(p => p.EvaluationCriteriaID).ToList();
                lstResult = iquerySEEBO.Where(p => "CHT".Equals(p.EndingEvaluation)).ToList();
                List<int> lstRateAddPupilID = lstResult.Select(p => p.PupilID).Distinct().ToList();
                #region Fill tieu de
                EvaluationCriteria objEC = null;
                List<RatedCommentPupilBO> lsttmp = null;
                RatedCommentPupilBO objtmp = null;
                int startcol = 4;
                //tieu de mon hoc
                for (int i = 0; i < lstClassSubjectAll.Count; i++)
                {
                    var objCSBO = lstClassSubjectAll[i];
                    Currentsheet.SetColumnWidth(startcol, 4.7);
                    if (objCSBO.IsCommenting == 0)
                    {
                        Currentsheet.GetRange(6, startcol, 6, startcol + 1).Merge();
                        if (objCSBO.SubjectName.ToUpper().Equals("TỰ NHIÊN VÀ XÃ HỘI"))
                        {
                            Currentsheet.SetCellValue(6, startcol, "TN & XH");    
                        }
                        else
                        {
                            Currentsheet.SetCellValue(6, startcol, objCSBO.SubjectName);
                        }
                        
                        Currentsheet.SetCellValue(7, startcol, "Mức đạt được");
                        Currentsheet.SetCellValue(7, startcol + 1, "Điểm KTĐK");
                        Currentsheet.SetColumnWidth(startcol + 1, 4.7);
                        startcol += 2;
                    }
                    else
                    {
                        Currentsheet.GetRange(6, startcol, 7, startcol).Merge();
                        if (objCSBO.SubjectName.ToUpper().Equals("TỰ NHIÊN VÀ XÃ HỘI"))
                        {
                            Currentsheet.SetCellValue(6, startcol, "TN & XH");
                        }
                        else
                        {
                            Currentsheet.SetCellValue(6, startcol, objCSBO.SubjectName);
                        }
                        startcol += 1;
                    }
                }
                Currentsheet.GetRange(7, 4, 7, startcol - 1).SetFontStyle(false, null, false, 10, false, false);
                Currentsheet.GetRange(7, 4, 7, startcol - 1).SetOrientation(180);
                //tieu de NL PC
                int startcolNL = startcol;
                Currentsheet.GetRange(6, startcol, 6, startcol + 2).Merge();
                Currentsheet.SetCellValue(6, startcol, "Năng lực");
                Currentsheet.SetCellValue(7, startcol, "NL1");
                Currentsheet.SetColumnWidth(startcol, 4.7);
                Currentsheet.SetCellValue(7, startcol + 1, "NL2");
                Currentsheet.SetColumnWidth(startcol + 1, 4.7);
                Currentsheet.SetCellValue(7, startcol + 2, "NL3");
                Currentsheet.SetColumnWidth(startcol + 2, 4.7);
                startcol += 3;
                Currentsheet.GetRange(6, startcol, 6, startcol + 3).Merge();
                Currentsheet.SetCellValue(6, startcol, "Phẩm chất");
                Currentsheet.SetCellValue(7, startcol, "PC1");
                Currentsheet.SetColumnWidth(startcol, 4.7);
                Currentsheet.SetCellValue(7, startcol + 1, "PC2");
                Currentsheet.SetColumnWidth(startcol + 1, 4.7);
                Currentsheet.SetCellValue(7, startcol + 2, "PC3");
                Currentsheet.SetColumnWidth(startcol + 2, 4.7);
                Currentsheet.SetCellValue(7, startcol + 3, "PC4");
                Currentsheet.SetColumnWidth(startcol + 3, 4.7);
                Currentsheet.GetRange(7, startcolNL, 7, startcol + 3).SetFontStyle(true, null, false, 11, false, false);
                startcol += 4;
                Currentsheet.GetRange(6, startcol, 7, startcol).Merge();
                Currentsheet.SetCellValue(6, startcol, "Kết quả đánh giá bổ sung");
                Currentsheet.SetColumnWidth(startcol, 4.7);
                
                Currentsheet.GetRange(6, 1, 7, startcol).SetBorder(VTBorderStyle.Solid, VTBorderWeight.Thin, VTBorderIndex.All);
                #endregion
                #region Fill du lieu
                int startRow = 8;
                int PupilID = 0;
                lsttmpPOC = lstPOC.Where(p => lstRateAddPupilID.Contains(p.PupilID)).ToList();
                for (int i = 0; i < lsttmpPOC.Count; i++)
                {
                    objPOC = lsttmpPOC[i];
                    PupilID = objPOC.PupilID;
                    Currentsheet.SetCellValue(startRow, 1, (i + 1));
                    Currentsheet.SetCellValue(startRow, 2, objPOC.PupilFullName);
                    Currentsheet.SetCellValue(startRow, 3, objPOC.ClassName);
                    startcol = 4;
                    //Fill du lieu mon hoc
                    for (int j = 0; j < lstClassSubjectAll.Count; j++)
                    {
                        var objCSBO = lstClassSubjectAll[j];
                        objtmp = lstRatedCommentPupil.Where(p => p.PupilID == PupilID && p.EvaluationID == GlobalConstants.EVALUATION_SUBJECT_AND_EDUCTIONGROUP
                                                            && p.SubjectID == objCSBO.SubjectID).FirstOrDefault();
                        if (objtmp != null)
                        {
                            Currentsheet.SetCellValue(startRow, startcol, objtmp.EndingEvaluation == 1 ? GlobalConstants.EVALUATION_GOOD_COMPLETE : objtmp.EndingEvaluation == 2 ?
                            GlobalConstants.EVALUATION_COMPLETE : objtmp.EndingEvaluation == 3 ? GlobalConstants.EVALUATION_NOT_COMPLETE : "");
                            if (objtmp.EndingEvaluation.HasValue && objtmp.EndingEvaluation.Value == 3)
                            {
                                Currentsheet.GetRange(startRow, startcol, startRow, startcol).SetFontColour(Color.Red);
                            }
                        }

                        if (objCSBO.IsCommenting == 0)//mon tinh diem
                        {
                            if (objtmp != null)
                            {
                                Currentsheet.SetCellValue(startRow, startcol + 1, objtmp.PeriodicEndingMark);
                                if (objtmp.PeriodicEndingMark.HasValue && objtmp.PeriodicEndingMark < 5)
                                {
                                    Currentsheet.GetRange(startRow, startcol + 1, startRow, startcol + 1).SetFontColour(Color.Red);
                                }
                            }
                            startcol += 2;
                        }
                        else//mon nhan xet
                        {
                            startcol += 1;
                        }
                    }
                    //fill du lieu NL PC
                    lsttmp = lstRatedCommentPupil.Where(p => p.PupilID == PupilID && (p.EvaluationID == GlobalConstants.EVALUATION_CAPACITY
                                                           || p.EvaluationID == GlobalConstants.EVALUATION_QUALITY)).ToList();
                    for (int j = 0; j < lstEvaluationCriteria.Count; j++)
                    {
                        objEC = lstEvaluationCriteria[j];
                        objtmp = lsttmp.Where(p => p.SubjectID == (objEC.EvaluationCriteriaID)).FirstOrDefault();
                        if (objtmp != null)
                        {
                            if (objEC.TypeID == GlobalConstants.EVALUATION_CAPACITY)
                            {
                                Currentsheet.SetCellValue(startRow, startcol, objtmp.CapacityEndingEvaluation == 1 ? GlobalConstants.EVALUATION_GOOD_COMPLETE : objtmp.CapacityEndingEvaluation == 2 ?
                                GlobalConstants.EVALUATION_CAPQUA_COMPLETE : objtmp.CapacityEndingEvaluation == 3 ? GlobalConstants.EVALUATION_NOT_COMPLETE : "");
                                if (objtmp.EndingEvaluation.HasValue && objtmp.EndingEvaluation.Value == 3)
                                {
                                    Currentsheet.GetRange(startRow, startcol, startRow, startcol).SetFontColour(Color.Red);
                                }
                            }
                            else
                            {
                                Currentsheet.SetCellValue(startRow, startcol, objtmp.QualityEndingEvaluation == 1 ? GlobalConstants.EVALUATION_GOOD_COMPLETE : objtmp.QualityEndingEvaluation == 2 ?
                                GlobalConstants.EVALUATION_CAPQUA_COMPLETE : objtmp.QualityEndingEvaluation == 3 ? GlobalConstants.EVALUATION_NOT_COMPLETE : "");
                                if (objtmp.QualityEndingEvaluation.HasValue && objtmp.QualityEndingEvaluation.Value == 3)
                                {
                                    Currentsheet.GetRange(startRow, startcol, startRow, startcol).SetFontColour(Color.Red);
                                }
                            }
                        }
                        startcol++;
                    }
                    //Fill Ket qua danh gia bo sung
                    objSEEBO = iquerySEEBO.Where(p => p.PupilID == PupilID).FirstOrDefault();
                    string strRateAdd = "CĐG";
                    if (objSEEBO != null)
                    {
                        if (objSEEBO.RateAdd.HasValue)
                        {
                            if (objSEEBO.RateAdd.Value == 0)
                            {
                                strRateAdd = "CHT";
                            }
                            else if (objSEEBO.RateAdd.Value == 1)
                            {
                                strRateAdd = "HT";
                            }
                            else
                            {
                                strRateAdd = "CĐG";
                            }
                        }
                    }
                    Currentsheet.SetCellValue(startRow, startcol, strRateAdd);
                    startRow++;
                }
                if (lstResult.Count > 0)
                {
                    Currentsheet.GetRange(6, 1, startRow - 1, startcol).SetBorder(VTBorderStyle.Solid, VTBorderWeight.Thin, VTBorderIndex.All);
                }
                #endregion
                Currentsheet.Name = "DanhGiaBoSung";
                #endregion
            }
            #endregion
            return oBook.ToStream();
        }
        private void setDataToFile(IVTWorksheet sheet, List<SummedEndingEvaluationBO> lstSEEBO, List<PupilOfClassBO> lstPOC, List<RatedCommentPupil> lstRatedCommentPupil,
            List<SubjectCat> lstSC, List<ClassSubject> lstCS, List<EvaluationCriteria> lstEvaluationCriteria, SchoolProfile objSP, ClassProfile objCP, AcademicYear objAy,
            int AcademicYearID, int SchoolID, int ClassID, int EducationLevelID, int TypeExport, string ClassName)
        {
            //fill thong tin chung
            List<SummedEndingEvaluationBO> lstResult = null;
            SummedEndingEvaluationBO objSEEBO = null;
            sheet.SetCellValue("A2", objSP.SchoolName.ToUpper());

            string Title = string.Empty;
            Title = "Khối " + (EducationLevelID == 0 ? "Tất cả " : EducationLevelID.ToString()) + ClassName + " - Năm học " + objAy.DisplayTitle;
            sheet.SetCellValue("A4", Title.ToUpper());

            PupilOfClassBO objPOC = null;
            int startRow = 0;
            List<PupilOfClassBO> lsttmpPOC = null;
            if (TypeExport == 1)//Fill DS HS luu ban
            {
                startRow = 7;
                lstResult = lstSEEBO.Where(p => p.RateAdd.HasValue && p.RateAdd.Value == 0).ToList();
                List<int> lstPupilRepeaterID = lstResult.Select(p => p.PupilID).Distinct().ToList();
                lsttmpPOC = lstPOC.Where(p => lstPupilRepeaterID.Contains(p.PupilID)).ToList();
                for (int i = 0; i < lsttmpPOC.Count; i++)
                {
                    objPOC = lsttmpPOC[i];
                    sheet.SetCellValue(startRow, 1, (i + 1));
                    sheet.SetCellValue(startRow, 2, objPOC.PupilCode);
                    sheet.SetCellValue(startRow, 3, objPOC.PupilFullName);
                    sheet.SetCellValue(startRow, 4, objPOC.ClassName);
                    startRow++;
                }
                //ke khung
                if (lstResult.Count > 0)
                {
                    sheet.GetRange(7, 1, startRow - 1, 5).SetBorder(VTBorderStyle.Solid, VTBorderWeight.Thin, VTBorderIndex.All);
                }
            }
            else//Fill DS HS danh gia bo sung
            {
                //lay du lieu ket qua chi tiet cac mon
                lstResult = lstSEEBO.Where(p => "CHT".Equals(p.EndingEvaluation)).ToList();
                List<int> lstRateAddPupilID = lstResult.Select(p => p.PupilID).Distinct().ToList();

                #region Fill tieu de
                EvaluationCriteria objEC = null;
                List<RatedCommentPupil> lsttmp = null;
                RatedCommentPupil objtmp = null;
                SubjectCat objSC = null;
                ClassSubject objClassSubject = null;
                int startcol = 5;
                //tieu de mon hoc
                for (int i = 0; i < lstSC.Count; i++)
                {
                    objSC = lstSC[i];
                    objClassSubject = lstCS.Where(p => p.SubjectID == objSC.SubjectCatID).FirstOrDefault();
                    if (objClassSubject.IsCommenting == 0)
                    {
                        sheet.GetRange(6, startcol, 6, startcol + 1).Merge();
                        sheet.SetCellValue(6, startcol, objSC.SubjectName);
                        sheet.SetCellValue(7, startcol, "Mức đạt được");
                        sheet.SetCellValue(7, startcol + 1, "Điểm KTĐK");
                        startcol += 2;
                    }
                    else
                    {
                        sheet.GetRange(6, startcol, 7, startcol).Merge();
                        sheet.SetCellValue(6, startcol, objSC.SubjectName);
                        startcol += 1;
                    }
                }
                //tieu de NL PC
                sheet.GetRange(6, startcol, 6, startcol + 2).Merge();
                sheet.SetCellValue(6, startcol, "Năng lực");
                sheet.SetCellValue(7, startcol, "NL1");
                sheet.SetCellValue(7, startcol + 1, "NL2");
                sheet.SetCellValue(7, startcol + 2, "NL3");
                startcol += 3;
                sheet.GetRange(6, startcol, 6, startcol + 3).Merge();
                sheet.SetCellValue(6, startcol, "Phẩm chất");
                sheet.SetCellValue(7, startcol, "PC1");
                sheet.SetCellValue(7, startcol + 1, "PC2");
                sheet.SetCellValue(7, startcol + 2, "PC3");
                sheet.SetCellValue(7, startcol + 3, "PC4");
                startcol += 4;
                sheet.GetRange(6, startcol, 7, startcol).Merge();
                sheet.SetCellValue(6, startcol, "Kết quả đánh giá bổ sung");
                sheet.GetRange(6, 1, 7, startcol).SetBorder(VTBorderStyle.Solid, VTBorderWeight.Thin, VTBorderIndex.All);
                #endregion
                #region Fill du lieu
                startRow = 8;
                startcol = 5;
                int PupilID = 0;
                lsttmpPOC = lstPOC.Where(p => lstRateAddPupilID.Contains(p.PupilID)).ToList();
                for (int i = 0; i < lsttmpPOC.Count; i++)
                {
                    objPOC = lsttmpPOC[i];
                    PupilID = objPOC.PupilID;
                    sheet.SetCellValue(startRow, 1, (i + 1));
                    sheet.SetCellValue(startRow, 2, objPOC.PupilCode);
                    sheet.SetCellValue(startRow, 3, objPOC.PupilFullName);
                    sheet.SetCellValue(startRow, 4, objPOC.ClassName);
                    startcol = 5;
                    //Fill du lieu mon hoc
                    for (int j = 0; j < lstSC.Count; j++)
                    {
                        objSC = lstSC[j];
                        objClassSubject = lstCS.Where(p => p.SubjectID == objSC.SubjectCatID).FirstOrDefault();
                        objtmp = lstRatedCommentPupil.Where(p => p.PupilID == PupilID && p.EvaluationID == GlobalConstants.EVALUATION_SUBJECT_AND_EDUCTIONGROUP
                                                            && p.SubjectID == objSC.SubjectCatID).FirstOrDefault();
                        if (objtmp != null)
                        {
                            sheet.SetCellValue(startRow, startcol, objtmp.EndingEvaluation == 1 ? GlobalConstants.EVALUATION_GOOD_COMPLETE : objtmp.EndingEvaluation == 2 ?
                            GlobalConstants.EVALUATION_COMPLETE : objtmp.EndingEvaluation == 3 ? GlobalConstants.EVALUATION_NOT_COMPLETE : "");
                            if (objtmp.EndingEvaluation.HasValue && objtmp.EndingEvaluation.Value == 3)
                            {
                                sheet.GetRange(startRow, startcol, startRow, startcol).SetFontColour(Color.Red);
                            }
                        }

                        if (objClassSubject.IsCommenting == 0)//mon tinh diem
                        {
                            if (objtmp != null)
                            {
                                sheet.SetCellValue(startRow, startcol + 1, objtmp.PeriodicEndingMark);
                                if (objtmp.PeriodicEndingMark.HasValue && objtmp.PeriodicEndingMark < 5)
                                {
                                    sheet.GetRange(startRow, startcol + 1, startRow, startcol + 1).SetFontColour(Color.Red);
                                }
                            }
                            startcol += 2;
                        }
                        else//mon nhan xet
                        {
                            startcol += 1;
                        }
                    }
                    //fill du lieu NL PC
                    lsttmp = lstRatedCommentPupil.Where(p => p.PupilID == PupilID && (p.EvaluationID == GlobalConstants.EVALUATION_CAPACITY
                                                           || p.EvaluationID == GlobalConstants.EVALUATION_QUALITY)).ToList();
                    for (int j = 0; j < lstEvaluationCriteria.Count; j++)
                    {
                        objEC = lstEvaluationCriteria[j];
                        objtmp = lsttmp.Where(p => p.SubjectID == (objEC.EvaluationCriteriaID)).FirstOrDefault();
                        if (objtmp != null)
                        {
                            if (objEC.TypeID == GlobalConstants.EVALUATION_CAPACITY)
                            {
                                sheet.SetCellValue(startRow, startcol, objtmp.CapacityEndingEvaluation == 1 ? GlobalConstants.EVALUATION_GOOD_COMPLETE : objtmp.CapacityEndingEvaluation == 2 ?
                                GlobalConstants.EVALUATION_CAPQUA_COMPLETE : objtmp.CapacityEndingEvaluation == 3 ? GlobalConstants.EVALUATION_NOT_COMPLETE : "");
                                if (objtmp.EndingEvaluation.HasValue && objtmp.EndingEvaluation.Value == 3)
                                {
                                    sheet.GetRange(startRow, startcol, startRow, startcol).SetFontColour(Color.Red);
                                }
                            }
                            else
                            {
                                sheet.SetCellValue(startRow, startcol, objtmp.QualityEndingEvaluation == 1 ? GlobalConstants.EVALUATION_GOOD_COMPLETE : objtmp.QualityEndingEvaluation == 2 ?
                                GlobalConstants.EVALUATION_CAPQUA_COMPLETE : objtmp.QualityEndingEvaluation == 3 ? GlobalConstants.EVALUATION_NOT_COMPLETE : "");
                                if (objtmp.QualityEndingEvaluation.HasValue && objtmp.QualityEndingEvaluation.Value == 3)
                                {
                                    sheet.GetRange(startRow, startcol, startRow, startcol).SetFontColour(Color.Red);
                                }
                            }
                        }
                        startcol++;
                    }
                    //Fill Ket qua danh gia bo sung
                    objSEEBO = lstSEEBO.Where(p => p.PupilID == PupilID).FirstOrDefault();
                    string strRateAdd = "CĐG";
                    if (objSEEBO != null)
                    {
                        if (objSEEBO.RateAdd.HasValue)
                        {
                            if (objSEEBO.RateAdd.Value == 0)
                            {
                                strRateAdd = "CHT";
                            }
                            else if (objSEEBO.RateAdd.Value == 1)
                            {
                                strRateAdd = "HT";
                            }
                            else
                            {
                                strRateAdd = "CĐG";
                            }
                        }
                    }
                    sheet.SetCellValue(startRow, startcol, strRateAdd);
                    startRow++;
                }
                if (lstResult.Count > 0)
                {
                    sheet.GetRange(6, 1, startRow - 1, startcol).SetBorder(VTBorderStyle.Solid, VTBorderWeight.Thin, VTBorderIndex.All);
                }
                #endregion
            }
        }

        public ProcessedReport InsertRepeaterProcessedReport(IDictionary<string, object> dicInsert, Stream data)
        {
            int AcademicYearID = Utils.GetInt(dicInsert, "AcademicYearID");
            int SchoolID = Utils.GetInt(dicInsert, "SchoolID");
            int EducationLevelID = Utils.GetInt(dicInsert, "EducationLevelID");
            int ClassID = Utils.GetInt(dicInsert, "ClassID");
            string reportCode = Utils.GetString(dicInsert, "ReportCode");
            ReportDefinition reportDef = ReportDefinitionBusiness.GetByCode(reportCode);
            ProcessedReport pr = new ProcessedReport();
            pr.ReportCode = reportCode;
            pr.ProcessedDate = DateTime.Now;
            pr.InputParameterHashKey = this.GetHashKeyRepeaterReport(AcademicYearID, SchoolID, EducationLevelID, ClassID);
            pr.ReportData = ReportUtils.Compress(data);

            //Tạo tên file
            ClassProfile objCP = ClassProfileBusiness.Find(ClassID);
            string outputNamePattern = reportDef.OutputNamePattern;
            outputNamePattern = outputNamePattern.Replace("[EducationLevel]", "_Khoi" + EducationLevelID);
            outputNamePattern = outputNamePattern.Replace("[ClassName]", ClassID > 0 ? "_Lop" + Utils.StripVNSignAndSpace(objCP.DisplayName) : "");
            pr.ReportName = ReportUtils.RemoveSpecialCharacters(outputNamePattern) + "." + reportDef.OutputFormat;
            IDictionary<string, object> dic = new Dictionary<string, object> {
                {"AcademicYearID", AcademicYearID},
                {"SchoolID", SchoolID},
                {"EducationLevelID",EducationLevelID},
                {"ClassID", ClassID}
            };
            ProcessedReportParameterRepository.Insert(dic, pr);
            ProcessedReportRepository.Insert(pr);
            ProcessedReportRepository.Save();
            return pr;
        }
        private string GetHashKeyRepeaterReport(int AcademicYearID, int SchoolID, int EducationLevelID, int ClassID)
        {
            IDictionary<string, object> dic = new Dictionary<string, object> {
                {"AcademicYearID", AcademicYearID},
                {"SchoolID", SchoolID},
                {"EducationLevelID",EducationLevelID},
                {"ClassID", ClassID}
            };
            return ReportUtils.GetHashKey(dic);
        }
        #endregion
        #region function of SMAS_MOBILE
        public void InsertOrUpdateToMobile(IDictionary<string, object> dic, SummedEndingEvaluation objSummedEnding)
        {

            int schoolID = Utils.GetInt(dic, "SchoolID");
            int classID = Utils.GetInt(dic, "ClassID");
            int academicYearID = Utils.GetInt(dic, "AcademicYearID");
            int partitionId = UtilsBusiness.GetPartionId(schoolID);
            SummedEndingEvaluation objSummedEndingDB = (from su in SummedEndingEvaluationBusiness.All
                                                        where su.LastDigitSchoolID == partitionId
                                                        && su.ClassID == classID
                                                        && su.SchoolID == schoolID
                                                        && su.AcademicYearID == academicYearID
                                                        && su.PupilID == objSummedEnding.PupilID
                                                        && su.SemesterID == GlobalConstants.SEMESTER_OF_EVALUATION_RESULT
                                                        && su.EvaluationID == GlobalConstants.EVALUATION_RESULT
                                                        select su).FirstOrDefault();

            if (objSummedEndingDB == null)
            {
                SummedEndingEvaluationBusiness.Insert(objSummedEnding);
            }
            else
            {
                objSummedEndingDB.RateAdd = objSummedEnding.RateAdd;
                objSummedEndingDB.UpdateTime = DateTime.Now;
                SummedEndingEvaluationBusiness.Update(objSummedEndingDB);
            }
            SummedEndingEvaluationBusiness.Save();
        }
        #endregion
    }
}
