﻿/**
 * The Viettel License
 *
 * Copyright 2012 Viettel Telecom. All rights reserved.
 * VIETTEL PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */

/** 
* @author  
* @version $Revision: $
*/

using System;
using log4net;
using System.Web;
using System.Data;
using System.Linq;
using System.Data.Entity;
using SMAS.Models.Models;
using SMAS.DAL.Repository;
using SMAS.DAL.IRepository;
using SMAS.Business.Common;
using SMAS.Business.IBusiness;
using System.Linq.Expressions;
using System.Collections.Generic;
using SMAS.Business.BusinessObject;
using SMAS.VTUtils.Log;
using System.Text;

namespace SMAS.Business.Business
{
    public partial class ExaminationsBusiness
    {
        public List<Examinations> GetListExamination(int academicYearID)
        {
            List<Examinations> listResult = this.All.Where(p => p.AcademicYearID == academicYearID).OrderBy(p => p.CreateTime).ToList();
            return listResult;
        }

        public IQueryable<Examinations> Search(IDictionary<string, object> search)
        {
            int academicYearID = Utils.GetInt(search, "AcademicYearID");
            int schoolID = Utils.GetInt(search, "SchoolID");
            int semesterID = Utils.GetInt(search, "SemesterID");
            int markInputType = Utils.GetInt(search, "MarkInputType");
            long examinationsID = Utils.GetLong(search, "ExaminationsID");
            int appliedLevel = Utils.GetInt(search, "AppliedLevel");

            // Đánh dấu chỉ load kì thi chưa vào điểm thi và chưa chốt điểm thi
            int markInput = Utils.GetInt(search, "MarkInput");
            int markClosing = Utils.GetInt(search, "MarkClosing");
            // Đánh dấu load kì thi kế thừa - Có ngày tạo nhỏ hơn ngày hiện tại
            int isUpdate = Utils.GetInt(search, "IsUpdate");

            IQueryable<Examinations> query = ExaminationsBusiness.All;

            if (academicYearID != 0)
            {
                query = query.Where(o => o.AcademicYearID == academicYearID);
            }
            if (schoolID != 0)
            {
                query = query.Where(o => o.SchoolID == schoolID);
            }
            if (semesterID != 0)
            {
                query = query.Where(o => o.SemesterID == semesterID);
            }
            if (markInputType != 0)
            {
                query = query.Where(o => o.MarkInputType == markInputType);
            }
            if (appliedLevel != 0)
            {
                query = query.Where(o => o.AppliedLevel == appliedLevel);
            }
            if (markInput == 1)
            {
                query = query.Where(o => o.MarkInput == false);
            }
            if (markClosing == 1)
            {
                query = query.Where(o => o.MarkClosing == false);
            }
            if (examinationsID != 0 && isUpdate == 0)
            {
                query = query.Where(o => o.ExaminationsID == examinationsID);
            }
            if (isUpdate == 1)
            {
                query = query.Where(o => o.CreateTime <= DateTime.Now && o.ExaminationsID != examinationsID);
            }

            return query;
        }

        public List<ExaminationsBO> GetListExaminations(IDictionary<string, object> search)
        {
            int academicYearID = Utils.GetInt(search, "AcademicYearID");
            int schoolID = Utils.GetInt(search, "SchoolID");
            int semesterID = Utils.GetInt(search, "SemesterID");
            int markInputType = Utils.GetInt(search, "MarkInputType");
            long examinationsID = Utils.GetLong(search, "ExaminationsID");
            int appliedLevel = Utils.GetInt(search, "AppliedLevel");

            IQueryable<Examinations> query = ExaminationsBusiness.All;

            if (academicYearID != 0)
            {
                query = query.Where(o => o.AcademicYearID == academicYearID);
            }
            if (schoolID != 0)
            {
                query = query.Where(o => o.SchoolID == schoolID);
            }
            if (semesterID != 0)
            {
                query = query.Where(o => o.SemesterID == semesterID);
            }
            if (markInputType != 0)
            {
                query = query.Where(o => o.MarkInputType == markInputType);
            }
            if (examinationsID != 0)
            {
                query = query.Where(o => o.ExaminationsID == examinationsID);
            }
            if (appliedLevel != 0)
            {
                query = query.Where(o => o.AppliedLevel == appliedLevel);
            }

            List<ExaminationsBO> listResult = query.Select(o => new ExaminationsBO()
            {
                ExaminationsID = o.ExaminationsID,
                ExaminationsName = o.ExaminationsName,
                Semester = o.SemesterID,
                DataInheritance = o.DataInheritance,
                InheritanceExam = o.InheritanceExam,
                InheritanceData = o.InheritanceData,
                MarkInputType = o.MarkInputType,
                MarkInput = o.MarkInput,
                MarkClosing = o.MarkClosing,
                CreateTime = o.CreateTime
            }).ToList();

            //List<long> listID = listResult.Select(o => o.ExaminationsID).ToList();
            //IDictionary<string, object> check = new Dictionary<string, object>();
            //ExamGroup checkGroup = null;
            //ExamSupervisory checkSupervisory = null;
            //for (int i = 0; i < listID.Count; i++)
            //{
            //    check["ExaminationsID"] = listID[i];
            //    checkGroup = ExamGroupBusiness.Search(check).FirstOrDefault();
            //    checkSupervisory = ExamSupervisoryBusiness.Search(check).FirstOrDefault();
            //    if (checkGroup == null && checkSupervisory == null)
            //    {
            //        listResult[i].IsDelete = true;
            //    }
            //    else
            //    {
            //        listResult[i].IsDelete = false;
            //    }
            //}
            for (int i = 0; i < listResult.Count; i++)
            {
                ExaminationsBO exam = listResult[i];
                long examinationsId = exam.ExaminationsID;
                bool isDelete = true;
                if (ExamInputMarkBusiness.All.Where(o => o.ExaminationsID == examinationsId).Count() > 0
                    || ExamSupervisoryViolateBusiness.All.Where(o => o.ExaminationsID == examinationsId).Count() > 0
                    || ExamPupilViolateBusiness.All.Where(o => o.ExaminationsID == examinationsId).Count() > 0
                    || ExamPupilAbsenceBusiness.All.Where(o => o.ExaminationsID == examinationsId).Count() > 0)
                {
                    isDelete = false;
                }
                exam.IsDelete = isDelete;
            }

            return listResult;
        }

        private void InsertInheritanceData(Examinations exam, IDictionary<string, object> search, bool inheritanceGroup, bool inheritancePupil, bool inheritanceSupervisory, string key)
        {
            #region Khai bao
            long inheritanceExamID = Utils.GetLong(search, "ExaminationsID");
            /* Note: Object temp - Đối tượng được kế thừa; Object new - Đối tượng tạo mới
            -------: List object - Danh sách chứa tất cả đối tượng tương ứng của kỳ thi được kế thừa
            -------: List temp - Danh sách chứa các đối tượng lọc theo điều kiện: Nhóm thi, Phòng thi
            -------: List insert - Danh sách chứa các đối tượng tạo mới nhưng chưa insert vào ngay vì còn thiếu thông tin */

            // Ke thua (1)
            List<ExamGroup> listExamGroup = null;
            List<ExamSubject> listExamSubject = null;
            List<ExamRoom> listExamRoom = null;
            List<ExamSubject> listExamSubjectTemp = null;
            List<ExamRoom> listExamRoomTemp = null;

            List<ExamGroup> listExamGroupUpdate = null;
            List<ExamSubject> listExamSubjectUpdate = null;
            List<ExamRoom> listExamRoomUpdate = null;

            ExamGroup groupTemp = null;
            ExamGroup newGroup = null;
            ExamRoom roomTemp = null;
            ExamRoom newRoom = null;
            ExamSubject subjectTemp = null;
            ExamSubject newSubject = null;

            // Ke thua (2)
            List<ExamPupilBO> listExamPupil = null;
            List<ExamBag> listBag = null;
            List<ExamCandenceBag> listCandence = null;
            List<ExamDetachableBag> listDetachable = null;
            List<ExamPupilBO> listExamPupilTemp = null;
            List<ExamPupil> listExamPupilInsert = null;
            List<ExamBag> listBagTemp = null;
            List<ExamCandenceBag> listCandenceTemp = null;
            List<ExamDetachableBag> listDetachableTemp = null;

            ExamPupilBO pupilTemp = null;
            ExamPupil newPupil = null;
            ExamPupil pupilUpdate = null;
            ExamBag bagTemp = null;
            ExamBag newBag = null;
            ExamCandenceBag candenceTemp = null;
            ExamCandenceBag newCandence = null;
            ExamDetachableBag detachableTemp = null;
            ExamDetachableBag newDetachable = null;

            // Ke thua (3)
            List<ExamSupervisory> listSupervisory = null;
            List<ExamSupervisory> listSupervisoryInsert = null;
            List<ExamSupervisoryAssignment> listAssignment = null;
            List<ExamSupervisoryAssignment> listAssignmentTemp = null;

            ExamSupervisory supervisoryTemp = null;
            ExamSupervisory newSupervisor = null;
            ExamSupervisoryAssignment assignmentTemp = null;
            ExamSupervisoryAssignment newAssignment = null;
            #endregion

            #region Lay du lieu
            // Tat ca nhom thi, mon thi va phong thi cua ky thi được kế thừa (1-1,2,3)
            if (inheritanceGroup)
            {
                listExamGroup = ExamGroupBusiness.Search(search).ToList();
                listExamRoom = ExamRoomBusiness.Search(search).ToList();
                listExamSubject = ExamSubjectBusiness.Search(search).ToList();
            }
            else
            {
                // Danh sách nhóm, môn và phòng được kế thừa
                listExamGroup = ExamGroupBusiness.Search(search).ToList();
                listExamRoom = ExamRoomBusiness.Search(search).ToList();
                listExamSubject = ExamSubjectBusiness.Search(search).ToList();

                // Danh sách nhóm, môn và phòng đã kế thừa
                search["ExaminationsID"] = exam.ExaminationsID;
                listExamGroupUpdate = ExamGroupBusiness.Search(search).ToList();
                listExamRoomUpdate = ExamRoomBusiness.Search(search).ToList();
                listExamSubjectUpdate = ExamSubjectBusiness.Search(search).ToList();
            }

            // Gán lại giá trị kỳ thi kế thừa cho Dictionary để tìm thông tin cần kế thừa
            search["ExaminationsID"] = inheritanceExamID;

            // Tat ca thi sinh, so bao danh, xep phong thi, tui thi, tui phach va danh phách của kỳ thi được kế thừa (2-1,2,3,4,5,6)
            if (inheritancePupil)
            {
                listExamPupil = ExamPupilBusiness.Search(search, exam.AcademicYearID, UtilsBusiness.GetPartionId(exam.SchoolID)).Where(o => o.Status == GlobalConstants.PUPIL_STATUS_STUDYING).ToList();
                listBag = ExamBagBusiness.Search(search).ToList();
                listCandence = ExamCandenceBagBusiness.Search(search).ToList();
                listDetachable = ExamDetachableBagBusiness.Search(search).ToList();
            }

            // Tat ca giam thi, phân công giám thị của kỳ thi được kế thừa (3-1,2)
            if (inheritanceSupervisory)
            {
                search["EmploymentStatus"] = GlobalConstants.EMPLOYMENT_STATUS_WORKING;
                listSupervisory = ExamSupervisoryBusiness.Search(search).ToList();
                listAssignment = ExamSupervisoryAssignmentBusiness.Search(search).ToList();
                listSupervisoryInsert = new List<ExamSupervisory>();
            }
            #endregion

            #region Xu ly ke thua
            // Neu nhu co ke thua danh sach giam thi (3-1)
            if (inheritanceSupervisory)
            {
                for (int i = 0; i < listSupervisory.Count; i++)
                {
                    supervisoryTemp = listSupervisory[i];
                    newSupervisor = new ExamSupervisory();
                    newSupervisor.ExamSupervisoryID = ExamSupervisoryBusiness.GetNextSeq<long>();
                    newSupervisor.AcademicYearID = supervisoryTemp.AcademicYearID;
                    newSupervisor.ExaminationsID = exam.ExaminationsID;
                    newSupervisor.TeacherID = supervisoryTemp.TeacherID;
                    newSupervisor.UpdateTime = supervisoryTemp.UpdateTime;
                    newSupervisor.CreateTime = DateTime.Now;
                    //ExamSupervisoryBusiness.Insert(newSupervisor);

                    // Thêm vào list để sử dụng lại thông tin giám thị mới
                    listSupervisoryInsert.Add(newSupervisor);
                }
            }

            List<ExamGroup> lstExamGroupInsert = new List<ExamGroup>();
            List<ExamSubject> lstExamSubjectInsert = new List<ExamSubject>();
            listExamPupilInsert = new List<ExamPupil>();

            List<ExamCandenceBag> lstExamCandenceBagInsert = new List<ExamCandenceBag>();
            List<ExamDetachableBag> lstExamDetachableBagInsert = new List<ExamDetachableBag>();
            List<ExamRoom> lstExamRoomInsert = new List<ExamRoom>();
            List<ExamBag> lstExamBagInsert = new List<ExamBag>();
            List<ExamSupervisoryAssignment> lstExamSupervisoryAssignmentInsert = new List<ExamSupervisoryAssignment>();
            // Them nhom tuong ung (1-1)
            for (int i = 0; i < listExamGroup.Count; i++)
            {
                groupTemp = listExamGroup[i];

                if (inheritanceGroup)
                {
                    newGroup = new ExamGroup();
                    newGroup.ExamGroupID = ExamGroupBusiness.GetNextSeq<long>();
                    newGroup.ExaminationsID = exam.ExaminationsID;
                    newGroup.ExamGroupCode = groupTemp.ExamGroupCode;
                    newGroup.ExamGroupName = groupTemp.ExamGroupName;
                    newGroup.UpdateTime = groupTemp.UpdateTime;
                    newGroup.CreateTime = DateTime.Now;
                    //ExamGroupBusiness.Insert(newGroup);                    
                    lstExamGroupInsert.Add(newGroup);
                }
                else
                {
                    // Nếu đã kế thừa nhóm thì lấy nhóm đó ra
                    newGroup = listExamGroupUpdate.Where(p => p.ExaminationsID == exam.ExaminationsID && p.ExamGroupCode == groupTemp.ExamGroupCode).FirstOrDefault();
                }

                // Them mon hoc tuong ung voi nhom
                listExamSubjectTemp = listExamSubject.Where(o => o.ExamGroupID == groupTemp.ExamGroupID).ToList();

                if (inheritanceGroup)
                {
                    for (int j = 0; j < listExamSubjectTemp.Count; j++)
                    {
                        subjectTemp = listExamSubjectTemp[j];
                        newSubject = new ExamSubject();
                        newSubject.ExamSubjectID = ExamSubjectBusiness.GetNextSeq<long>();
                        newSubject.ExaminationsID = exam.ExaminationsID;
                        newSubject.ExamGroupID = newGroup.ExamGroupID;
                        newSubject.UpdateTme = subjectTemp.UpdateTme;
                        newSubject.SchedulesExam = subjectTemp.SchedulesExam;
                        newSubject.SubjectID = subjectTemp.SubjectID;
                        newSubject.SubjectCode = subjectTemp.SubjectCode;
                        newSubject.CreateTime = DateTime.Now;
                        //ExamSubjectBusiness.Insert(newSubject);
                        lstExamSubjectInsert.Add(newSubject);
                    }
                }

                // Neu nhu co ke thua thi sinh, số báo danh, túi phách và đánh phách (2-1,2,4,6)
                if (inheritancePupil)
                {

                    listExamPupilTemp = listExamPupil.Where(o => o.ExamGroupID == groupTemp.ExamGroupID).ToList();
                    listCandenceTemp = listCandence.Where(o => o.ExamGroupID == groupTemp.ExamGroupID).ToList();

                    for (int j = 0; j < listExamPupilTemp.Count; j++)
                    {
                        pupilTemp = listExamPupilTemp[j];
                        newPupil = new ExamPupil();
                        newPupil.ExamPupilID = 0;// ExamPupilBusiness.GetNextSeq<long>();
                        newPupil.ExaminationsID = exam.ExaminationsID;
                        newPupil.ExamGroupID = newGroup.ExamGroupID;
                        newPupil.EducationLevelID = pupilTemp.EducationLevelID;
                        newPupil.SchoolID = pupilTemp.SchoolID;
                        newPupil.LastDigitSchoolID = pupilTemp.LastDigitSchoolID;
                        newPupil.PupilID = pupilTemp.PupilID;
                        newPupil.ExamineeNumber = pupilTemp.ExamineeNumber;
                        /*newPupil.ExamRoomID = pupilTemp.ExamRoomID; --- Khong lay sap xep phong thi */
                        newPupil.CreateTime = DateTime.Now;
                        newPupil.UpdateTime = pupilTemp.UpdateTime;
                        /*ExamPupilBusiness.Insert(newPupil);*/
                        //Thay bang insert list
                        listExamPupilInsert.Add(newPupil);
                    }

                    for (int j = 0; j < listCandenceTemp.Count; j++)
                    {
                        candenceTemp = listCandenceTemp[j];
                        newCandence = new ExamCandenceBag();
                        newCandence.ExamCandenceBagID = ExamCandenceBagBusiness.GetNextSeq<long>();
                        newCandence.ExaminationsID = exam.ExaminationsID;
                        newCandence.ExamGroupID = newGroup.ExamGroupID;
                        newCandence.ExamCandenceBagCode = candenceTemp.ExamCandenceBagCode;
                        newCandence.SubjectID = candenceTemp.SubjectID;
                        newCandence.CreateTime = DateTime.Now;
                        newCandence.UpdateTime = candenceTemp.UpdateTime;
                        //ExamCandenceBagBusiness.Insert(newCandence);
                        lstExamCandenceBagInsert.Add(newCandence);

                        // Them cac số phách tương ứng với túi phách mới tạo (2-6)
                        listDetachableTemp = listDetachable.Where(o => o.ExamGroupID == groupTemp.ExamGroupID && o.ExamCandenceBagID == candenceTemp.ExamCandenceBagID).ToList();

                        for (int k = 0; k < listDetachableTemp.Count; k++)
                        {
                            detachableTemp = listDetachableTemp[k];
                            newDetachable = new ExamDetachableBag();
                            newDetachable.ExamDetachableBagID = 0;// ExamDetachableBagBusiness.GetNextSeq<long>();
                            newDetachable.ExaminationsID = exam.ExaminationsID;
                            newDetachable.ExamGroupID = newGroup.ExamGroupID;
                            newDetachable.ExamCandenceBagID = newCandence.ExamCandenceBagID;
                            newDetachable.AcademicYearID = detachableTemp.AcademicYearID;
                            newDetachable.LastDigitSchoolID = detachableTemp.LastDigitSchoolID;
                            newDetachable.SubjectID = detachableTemp.SubjectID;

                            // Tìm mã thí sinh tương ứng trong danh sách thí sinh mới thêm
                            ExamPupilBO epb = listExamPupil.Where(o => o.ExamGroupID == groupTemp.ExamGroupID && o.ExamPupilID == detachableTemp.ExamPupilID).FirstOrDefault();
                            if (epb == null)
                            {
                                continue;
                            }
                            long pupilID = epb.PupilID;
                            long examPupilID = listExamPupilInsert.Where(o => o.ExamGroupID == newGroup.ExamGroupID && o.PupilID == pupilID).FirstOrDefault().ExamPupilID;
                            newDetachable.ExamPupilID = examPupilID;

                            newDetachable.ExamDetachableBagCode = detachableTemp.ExamDetachableBagCode;
                            newDetachable.CreateTime = DateTime.Now;
                            newDetachable.UpdateTime = detachableTemp.UpdateTime;
                            //ExamDetachableBagBusiness.Insert(newDetachable);
                            lstExamDetachableBagInsert.Add(newDetachable);
                        }
                    }
                }

                // Them phong thi tuong ung voi nhom (1-2,3)
                listExamRoomTemp = listExamRoom.Where(o => o.ExamGroupID == groupTemp.ExamGroupID).ToList();

                for (int j = 0; j < listExamRoomTemp.Count; j++)
                {
                    roomTemp = listExamRoomTemp[j];

                    if (inheritanceGroup)
                    {
                        newRoom = new ExamRoom();
                        newRoom.ExamRoomID = ExamRoomBusiness.GetNextSeq<long>();
                        newRoom.ExaminationsID = exam.ExaminationsID;
                        newRoom.ExamGroupID = newGroup.ExamGroupID;
                        newRoom.ExamRoomCode = roomTemp.ExamRoomCode;
                        newRoom.SeatNumber = roomTemp.SeatNumber;
                        newRoom.Note = roomTemp.Note;
                        newRoom.UpdatTime = roomTemp.UpdatTime;
                        newRoom.CreateTime = DateTime.Now;
                        //ExamRoomBusiness.Insert(newRoom);
                        lstExamRoomInsert.Add(newRoom);
                    }
                    else if (newGroup != null)
                    {
                        newRoom = listExamRoomUpdate.Where(p => p.ExaminationsID == exam.ExaminationsID && p.ExamGroupID == newGroup.ExamGroupID && p.ExamRoomCode == roomTemp.ExamRoomCode).FirstOrDefault(); ;
                    }

                    // Neu nhu co ke thua xep phong thi, tui thi (2-3,5)
                    if (inheritancePupil)
                    {
                        listExamPupilTemp = listExamPupil.Where(o => o.ExamGroupID == groupTemp.ExamGroupID && o.ExamRoomID == roomTemp.ExamRoomID).ToList();
                        listBagTemp = listBag.Where(o => o.ExamGroupID == groupTemp.ExamGroupID && o.ExamRoomID == roomTemp.ExamRoomID).ToList();

                        for (int k = 0; k < listBagTemp.Count; k++)
                        {
                            bagTemp = listBagTemp[k];
                            newBag = new ExamBag();
                            newBag.ExamBagID = 0;// ExamBagBusiness.GetNextSeq<long>();
                            newBag.ExaminationsID = exam.ExaminationsID;
                            newBag.ExamGroupID = newGroup.ExamGroupID;
                            newBag.ExamRoomID = newRoom.ExamRoomID;
                            newBag.SubjectID = bagTemp.SubjectID;
                            newBag.ExamBagCode = bagTemp.ExamBagCode;
                            newBag.CreateTime = DateTime.Now;
                            newBag.UpdateTime = bagTemp.UpdateTime;
                            //ExamBagBusiness.Insert(newBag);
                            lstExamBagInsert.Add(newBag);
                        }

                        // Gan phong thi tuong ung cho những thí sinh vừa mới tạo
                        for (int k = 0; k < listExamPupilTemp.Count; k++)
                        {
                            if (newGroup == null || newRoom == null)
                            {
                                continue;
                            }
                            pupilTemp = listExamPupilTemp[k];
                            pupilUpdate = listExamPupilInsert.Where(o => o.ExaminationsID == exam.ExaminationsID && o.ExamGroupID == newGroup.ExamGroupID
                                && o.PupilID == pupilTemp.PupilID).FirstOrDefault();
                            if (pupilUpdate != null)
                            {
                                pupilUpdate.ExamRoomID = newRoom.ExamRoomID;
                            }
                        }
                    }

                    // Nếu như có kế thừa phân công giám thị (3-2)
                    if (inheritanceSupervisory)
                    {
                        listAssignmentTemp = listAssignment.Where(o => o.ExamGroupID == groupTemp.ExamGroupID && o.ExamRoomID == roomTemp.ExamRoomID).ToList();

                        for (int k = 0; k < listAssignmentTemp.Count; k++)
                        {
                            if (newGroup == null || newRoom == null)
                            {
                                continue;
                            }
                            assignmentTemp = listAssignmentTemp[k];
                            newAssignment = new ExamSupervisoryAssignment();
                            newAssignment.ExamSupervisoryAssignmentID = ExamSupervisoryAssignmentBusiness.GetNextSeq<long>("EXAMSUPERVISORYASSIGN_SEQ");
                            newAssignment.ExaminationsID = exam.ExaminationsID;
                            newAssignment.ExamGroupID = newGroup.ExamGroupID;
                            newAssignment.SubjectID = assignmentTemp.SubjectID;
                            newAssignment.ExamRoomID = newRoom.ExamRoomID;
                            newAssignment.AcademicYearID = assignmentTemp.AcademicYearID;
                            newAssignment.AssignedDate = assignmentTemp.AssignedDate;

                            // Lấy mã giám thi moi them tuong ung voi giam thi được kế thừa
                            ExamSupervisory oldExamSupervisory = listSupervisory.Where(o => o.ExamSupervisoryID == assignmentTemp.ExamSupervisoryID).FirstOrDefault();
                            if (oldExamSupervisory == null)
                            {
                                continue;
                            }
                            int teacherTempID = oldExamSupervisory.TeacherID;
                            long newSupervisoryID = listSupervisoryInsert.Where(o => o.TeacherID == teacherTempID).FirstOrDefault().ExamSupervisoryID;
                            newAssignment.ExamSupervisoryID = newSupervisoryID;

                            newAssignment.CreateTime = DateTime.Now;
                            newAssignment.UpdateTime = assignmentTemp.UpdateTime;
                            //ExamSupervisoryAssignmentBusiness.Insert(newAssignment);
                            lstExamSupervisoryAssignmentInsert.Add(newAssignment);
                        }
                    }
                }

                // Them danh sach thi sinh vao du lieu neu co ke thua
                /*if (inheritancePupil)
                {
                    for (int j = 0; j < listExamPupilInsert.Count; j++)
                    {
                        newPupil = listExamPupilInsert[j];
                        ExamPupilBusiness.Insert(newPupil);
                    }
                }*/
            }
            #endregion

            #region Luu du lieu


            /*List<ExamGroup> lstExamGroupInsert = new List<ExamGroup>();
            List<ExamSubject> lstExamSubjectInsert = new List<ExamSubject>();
            listExamPupilInsert = new List<ExamPupil>();

            List<ExamCandenceBag> lstExamCandenceBagInsert = new List<ExamCandenceBag>();
            List<ExamDetachableBag> lstExamDetachableBagInsert = new List<ExamDetachableBag>();
            List<ExamRoom> lstExamRoomInsert = new List<ExamRoom>();
            List<ExamBag> lstExamBagInsert = new List<ExamBag>();
            List<ExamSupervisoryAssignment> lstExamSupervisoryAssignmentInsert = new List<ExamSupervisoryAssignment>();*/



            // Save (1)
            //ExamGroupBusiness.Save();
            //ExamSubjectBusiness.Save();
            //ExamRoomBusiness.Save();


            if (lstExamGroupInsert != null && lstExamGroupInsert.Count > 0)
            {
                ExamGroupBusiness.BulkInsert(lstExamGroupInsert, ExamGroupMapping(), "");
            }
            if (lstExamSubjectInsert != null && lstExamSubjectInsert.Count > 0)
            {
                ExamSubjectBusiness.BulkInsert(lstExamSubjectInsert, ExamSubjectMapping(), "");
            }
            if (lstExamRoomInsert != null && lstExamRoomInsert.Count > 0)
            {
                ExamRoomBusiness.BulkInsert(lstExamRoomInsert, ExamRoomMapping(), "");
            }

            // Save (2)
            if (inheritancePupil)
            {

                if (listExamPupilInsert != null && listExamPupilInsert.Count > 0)
                {
                    ExamPupilBusiness.BulkInsert(listExamPupilInsert, ExamPupilMapping(), "");
                }
                if (lstExamBagInsert != null && lstExamBagInsert.Count > 0)
                {
                    ExamBagBusiness.BulkInsert(lstExamBagInsert, ExamBagMapping(), "");
                }

                if (lstExamCandenceBagInsert != null && lstExamCandenceBagInsert.Count > 0)
                {
                    ExamCandenceBagBusiness.BulkInsert(lstExamCandenceBagInsert, ExamCandenceBagMapping(), "");
                }

                if (lstExamDetachableBagInsert != null && lstExamDetachableBagInsert.Count > 0)
                {
                    ExamDetachableBagBusiness.BulkInsert(lstExamDetachableBagInsert, ExamDetachableBagMapping(), "");
                }

                //ExamPupilBusiness.Save();
                //ExamBagBusiness.Save();
                //ExamCandenceBagBusiness.Save();
                //ExamDetachableBagBusiness.Save();
            }

            // Save (3)
            if (inheritanceSupervisory)
            {
                //ExamSupervisoryBusiness.Save();
                if (listSupervisoryInsert != null && listSupervisoryInsert.Count > 0)
                {
                    ExamSupervisoryBusiness.BulkInsert(listSupervisoryInsert, ExamSupervisoryMapping(), "");
                }
                //ExamSupervisoryAssignmentBusiness.Save();
                if (lstExamSupervisoryAssignmentInsert != null && lstExamSupervisoryAssignmentInsert.Count > 0)
                {
                    ExamSupervisoryAssignmentBusiness.BulkInsert(lstExamSupervisoryAssignmentInsert, ExamSupervisoryAssignmentMapping(), "");
                }
            }
            #endregion
        }

        public void InsertExaminations(Examinations exam)
        {
            if (exam == null)
            {
                return;
            }

            // Kiem tra trung ten ki thi
            Examinations checkContains = (from ex in ExaminationsBusiness.All
                                          where ex.AcademicYearID == exam.AcademicYearID && ex.SchoolID == exam.SchoolID
                                          && ex.AppliedLevel == exam.AppliedLevel && ex.ExaminationsName.ToLower() == exam.ExaminationsName.ToLower()
                                          select ex).FirstOrDefault();
            if (checkContains != null)
            {
                throw new BusinessException("ExaminationsName_Validate_Contains", "");
            }
            else
            {
                exam.ExaminationsID = ExaminationsBusiness.GetNextSeq<long>();
                if (exam.DataInheritance.HasValue && exam.DataInheritance.Value == true)
                {
                    IDictionary<string, object> search = new Dictionary<string, object>();
                    search.Add("AcademicYearID", exam.AcademicYearID);
                    search.Add("SchoolID", exam.SchoolID);
                    search.Add("ExaminationsID", exam.InheritanceExam.Value);

                    bool inheritancePupil = false;
                    bool inheritanceSupervisory = false;
                    if (exam.InheritanceData.Contains("2"))
                    {
                        inheritancePupil = true;
                    }
                    if (exam.InheritanceData.Contains("3"))
                    {
                        inheritanceSupervisory = true;
                    }
                    this.Insert(exam);
                    this.Save();

                    InsertInheritanceData(exam, search, true, inheritancePupil, inheritanceSupervisory, "insert");
                }
                else
                {
                    this.Insert(exam);
                    this.Save();
                }
            }
        }

        public void UpdateExaminations(Examinations exam, bool DataInheritance, long InheritanceExam, string InheritanceData)
        {
            if (exam == null)
            {
                return;
            }

            // Kiem tra trung ten ki thi
            Examinations checkContains = (from ex in ExaminationsBusiness.All
                                          where ex.AcademicYearID == exam.AcademicYearID && ex.SchoolID == exam.SchoolID
                                          && ex.AppliedLevel == exam.AppliedLevel && ex.ExaminationsID != exam.ExaminationsID
                                          && ex.ExaminationsName.ToLower() == exam.ExaminationsName.ToLower()
                                          select ex).FirstOrDefault();
            if (checkContains != null)
            {
                throw new BusinessException("ExaminationsName_Validate_Contains", "");
            }
            else
            {
                if (exam.DataInheritance.HasValue && exam.DataInheritance.Value)
                {
                    IDictionary<string, object> search = new Dictionary<string, object>();
                    search.Add("AcademicYearID", exam.AcademicYearID);
                    search.Add("SchoolID", exam.SchoolID);
                    search.Add("ExaminationsID", exam.InheritanceExam.Value);

                    bool inheritanceGroup = false;
                    bool inheritancePupil = false;
                    bool inheritanceSupervisory = false;
                    if (DataInheritance == false)
                    {
                        inheritanceGroup = true;
                    }
                    // Neu thong tin ke thua moi co ke thua (2) va thong tin cu chua co ke thua (2)
                    if (exam.InheritanceData.Contains("2") && !InheritanceData.Contains("2"))
                    {
                        inheritancePupil = true;
                    }
                    // Neu thong tin ke thua moi co ke thua (3) va thong tin cu chua co ke thua (3)
                    if (exam.InheritanceData.Contains("3") && !InheritanceData.Contains("3"))
                    {
                        inheritanceSupervisory = true;
                    }
                    InsertInheritanceData(exam, search, inheritanceGroup, inheritancePupil, inheritanceSupervisory, "update");

                    // Neu thong tin cu co ke thua va chua co thong tin ke thua moi thi them thong tin ke thua moi vao
                    if (InheritanceData != null && !InheritanceData.Contains(exam.InheritanceData))
                    {
                        if (InheritanceData == string.Empty)
                        {
                            exam.InheritanceData = InheritanceData + exam.InheritanceData;
                        }
                        else
                        {
                            exam.InheritanceData = InheritanceData + "," + exam.InheritanceData;
                        }
                    }
                    // Neu giu nguyen thi gan thong tin ke thua cu lai
                    else
                    {
                        exam.InheritanceData = InheritanceData;
                    }
                }

                this.Update(exam);
                this.Save();
            }
        }

        public void DeleteExaminations(int examID, int schoolID)
        {
            //Kiem tra co duoc xoa hay khong
            bool isDelete = true;
            if (ExamInputMarkBusiness.All.Where(o => o.ExaminationsID == examID).Count() > 0
                || ExamSupervisoryViolateBusiness.All.Where(o => o.ExaminationsID == examID).Count() > 0
                || ExamPupilViolateBusiness.All.Where(o => o.ExaminationsID == examID).Count() > 0
                || ExamPupilAbsenceBusiness.All.Where(o => o.ExaminationsID == examID).Count() > 0)
            {
                isDelete = false;
            }
            if (!isDelete)
            {
                throw new BusinessException("Đã có dữ liệu điểm thi/thí sinh vi phạm quy chế/giám thị vi phạm quy chế/ thí sinh vắng thi. Thầy cô vui lòng kiểm tra và xóa dữ liệu trước khi xóa kì thi.");
            }

            try
            {
                context.Configuration.AutoDetectChangesEnabled = false;
                context.Configuration.ValidateOnSaveEnabled = false;

                int partition = UtilsBusiness.GetPartionId(schoolID);
                //Xoa phan cong nhap diem
                List<ExamInputMarkAssigned> lstExamInputMarkAssigned = ExamInputMarkAssignedBusiness.All.Where(o => o.ExaminationsID == examID).ToList();
                ExamInputMarkAssignedBusiness.DeleteAll(lstExamInputMarkAssigned);
                ExamInputMarkAssignedBusiness.Save();

                //Xoa phach
                List<ExamDetachableBag> lstExamDetachableBag = ExamDetachableBagBusiness.All.Where(o => o.ExaminationsID == examID).ToList();
                ExamDetachableBagBusiness.DeleteAll(lstExamDetachableBag);
                ExamDetachableBagBusiness.Save();

                //Xoa tui phach
                List<ExamCandenceBag> lstExamCandenceBag = ExamCandenceBagBusiness.All.Where(o => o.ExaminationsID == examID).ToList();
                ExamCandenceBagBusiness.DeleteAll(lstExamCandenceBag);
                ExamCandenceBagBusiness.Save();

                //Xoa tui thi
                List<ExamBag> lstExamBag = ExamBagBusiness.All.Where(o => o.ExaminationsID == examID).ToList();
                ExamBagBusiness.DeleteAll(lstExamBag);
                ExamBagBusiness.Save();

                //Xoa danh sach thi sinh
                List<ExamPupil> lstExamPupil = ExamPupilBusiness.All.Where(o => o.LastDigitSchoolID == partition && o.SchoolID == schoolID && o.ExaminationsID == examID).ToList();
                ExamPupilBusiness.DeleteAll(lstExamPupil);
                ExamPupilBusiness.Save();

                //Xoa phan cong giam thi
                List<ExamSupervisoryAssignment> lstExamSupervisoryAssignment = ExamSupervisoryAssignmentBusiness.All.Where(o => o.ExaminationsID == examID).ToList();
                ExamSupervisoryAssignmentBusiness.DeleteAll(lstExamSupervisoryAssignment);
                ExamSupervisoryAssignmentBusiness.Save();

                //Xoa giam thi
                List<ExamSupervisory> lstExamSupervisory = ExamSupervisoryBusiness.All.Where(o => o.ExaminationsID == examID).ToList();
                ExamSupervisoryBusiness.DeleteAll(lstExamSupervisory);
                ExamSupervisoryBusiness.Save();

                //Xoa phong thi
                List<ExamRoom> lstExamRoom = ExamRoomBusiness.All.Where(o => o.ExaminationsID == examID).ToList();
                ExamRoomBusiness.DeleteAll(lstExamRoom);
                ExamRoomBusiness.Save();

                //Xoa danh sach mon thi
                List<ExamSubject> lstExamSubject = ExamSubjectBusiness.All.Where(o => o.ExaminationsID == examID).ToList();
                ExamSubjectBusiness.DeleteAll(lstExamSubject);
                ExamSubjectBusiness.Save();

                //xoa nhom thi
                List<ExamGroup> lstExamGroup = ExamGroupBusiness.All.Where(o => o.ExaminationsID == examID).ToList();
                ExamGroupBusiness.DeleteAll(lstExamGroup);
                ExamGroupBusiness.Save();

                //Xoa ky thi
                this.Delete(examID);
                this.Save();
            }
            catch (Exception ex)
            {
                
                string paramList = string.Format("examID={0}, schoolID={1}", examID, schoolID);
                LogExtensions.ErrorExt(logger, DateTime.Now, "DeleteExaminations", paramList, ex);
            }
            finally
            {
                context.Configuration.AutoDetectChangesEnabled = true;
                context.Configuration.ValidateOnSaveEnabled = true;
            }

        }

        public ExaminationsBO GetExaminationsByID(long examinationsID)
        {
            ExaminationsBO examinationsObj = (from a in ExaminationsBusiness.All
                                              where a.ExaminationsID == examinationsID
                                              select new ExaminationsBO
                                              {
                                                  ExaminationsID = a.ExaminationsID,
                                                  ExaminationsName = a.ExaminationsName,
                                                  AcademicYearID = a.AcademicYearID,
                                                  AppliedLevel = a.AppliedLevel,
                                                  SemesterID = a.SemesterID,
                                                  SchoolID = a.SchoolID,
                                                  MarkInputType = a.MarkInputType,
                                                  MarkInput = a.MarkInput,
                                                  MarkClosing = a.MarkClosing,
                                                  DataInheritance = a.DataInheritance,
                                                  InheritanceExam = a.InheritanceExam,
                                                  InheritanceData = a.InheritanceData,
                                                  CreateTime = a.CreateTime,
                                                  UpdateTime = a.UpdateTime
                                              }).FirstOrDefault();
            return examinationsObj;
        }

        public void ResetInheritance(long ExaminationsID)
        {
            Examinations exam = this.Find(ExaminationsID);
            if (exam != null && exam.DataInheritance.HasValue && exam.DataInheritance.Value)
            {
                ExamGroup group = ExamGroupBusiness.Search(new Dictionary<string, object>() { { "ExaminationsID", ExaminationsID } }).FirstOrDefault();
                if (group == null)
                {
                    exam.DataInheritance = false;
                    exam.InheritanceExam = 0;
                    exam.InheritanceData = string.Empty;
                    this.Update(exam);
                    this.Save();
                }
            }
        }


        #region "Mapping Columns"
        public IDictionary<string, object> ExamGroupMapping()
        {
            Dictionary<string, object> columnMap = new Dictionary<string, object>();
            columnMap.Add("ExamGroupID", "EXAM_GROUP_ID");
            columnMap.Add("ExamGroupCode", "EXAM_GROUP_CODE");
            columnMap.Add("ExamGroupName", "EXAM_GROUP_NAME");
            columnMap.Add("ExaminationsID", "EXAMINATIONS_ID");
            columnMap.Add("CreateTime", "CREATE_TIME");
            columnMap.Add("UpdateTime", "UPDATE_TIME");
            return columnMap;
        }

        public IDictionary<string, object> ExamSubjectMapping()
        {
            Dictionary<string, object> columnMap = new Dictionary<string, object>();
            columnMap.Add("ExamSubjectID", "EXAM_SUBJECT_ID");
            columnMap.Add("ExaminationsID", "EXAMINATIONS_ID");
            columnMap.Add("ExamGroupID", "EXAM_GROUP_ID");
            columnMap.Add("SubjectID", "SUBJECT_ID");
            columnMap.Add("SubjectCode", "SUBJECT_CODE");
            columnMap.Add("SchedulesExam", "SCHEDULES_EXAM");
            columnMap.Add("CreateTime", "CREATE_TIME");
            columnMap.Add("UpdateTme", "UPDATE_TIME");
            return columnMap;
        }

        public IDictionary<string, object> ExamPupilMapping()
        {
            Dictionary<string, object> columnMap = new Dictionary<string, object>();
            columnMap.Add("UpdateTime", "UPDATE_TIME");
            columnMap.Add("CreateTime", "CREATE_TIME");
            columnMap.Add("ExamRoomID", "EXAM_ROOM_ID");
            columnMap.Add("ExamineeNumber", "EXAMINEE_NUMBER");
            columnMap.Add("PupilID", "PUPIL_ID");
            columnMap.Add("LastDigitSchoolID", "LAST_DIGIT_SCHOOL_ID");
            columnMap.Add("SchoolID", "SCHOOL_ID");
            columnMap.Add("EducationLevelID", "EDUCATION_LEVEL_ID");
            columnMap.Add("ExamGroupID", "EXAM_GROUP_ID");
            columnMap.Add("ExaminationsID", "EXAMINATIONS_ID");
            columnMap.Add("ExamPupilID", "EXAM_PUPIL_ID");
            return columnMap;
        }
        public IDictionary<string, object> ExamCandenceBagMapping()
        {
            Dictionary<string, object> columnMap = new Dictionary<string, object>();
            columnMap.Add("UpdateTime", "UPDATE_TIME");
            columnMap.Add("CreateTime", "CREATE_TIME");
            columnMap.Add("SubjectID", "SUBJECT_ID");
            columnMap.Add("ExamGroupID", "EXAM_GROUP_ID");
            columnMap.Add("ExaminationsID", "EXAMINATIONS_ID");
            columnMap.Add("ExamCandenceBagCode", "EXAM_CANDENCE_BAG_CODE");
            columnMap.Add("ExamCandenceBagID", "EXAM_CANDENCE_BAG_ID");
            return columnMap;
        }

        public IDictionary<string, object> ExamDetachableBagMapping()
        {
            Dictionary<string, object> columnMap = new Dictionary<string, object>();
            columnMap.Add("UpdateTime", "UPDATE_TIME");
            columnMap.Add("CreateTime", "CREATE_TIME");
            columnMap.Add("ExamDetachableBagCode", "EXAM_DETACHABLE_BAG_CODE");
            columnMap.Add("ExamPupilID", "EXAM_PUPIL_ID");
            columnMap.Add("SubjectID", "SUBJECT_ID");
            columnMap.Add("LastDigitSchoolID", "LAST_DIGIT_SCHOOL_ID");
            columnMap.Add("AcademicYearID", "ACADEMIC_YEAR_ID");
            columnMap.Add("ExamCandenceBagID", "EXAM_CANDENCE_BAG_ID");
            columnMap.Add("ExamGroupID", "EXAM_GROUP_ID");
            columnMap.Add("ExaminationsID", "EXAMINATIONS_ID");
            columnMap.Add("ExamDetachableBagID", "EXAM_DETACHABLE_BAG_ID");
            return columnMap;
        }

        public IDictionary<string, object> ExamRoomMapping()
        {
            Dictionary<string, object> columnMap = new Dictionary<string, object>();
            columnMap.Add("UpdatTime", "UPDATE_TIME");
            columnMap.Add("CreateTime", "CREATE_TIME");
            columnMap.Add("Note", "NOTE");
            columnMap.Add("SeatNumber", "SEAT_NUMBER");
            columnMap.Add("ExamRoomCode", "EXAM_ROOM_CODE");
            columnMap.Add("ExamGroupID", "EXAM_GROUP_ID");
            columnMap.Add("ExaminationsID", "EXAMINATIONS_ID");
            columnMap.Add("ExamRoomID", "EXAM_ROOM_ID");
            return columnMap;
        }
        public IDictionary<string, object> ExamBagMapping()
        {
            Dictionary<string, object> columnMap = new Dictionary<string, object>();
            columnMap.Add("UpdateTime", "UPDATE_TIME");
            columnMap.Add("CreateTime", "CREATE_TIME");
            columnMap.Add("SubjectID", "SUBJECT_ID");
            columnMap.Add("ExamRoomID", "EXAM_ROOM_ID");
            columnMap.Add("ExamGroupID", "EXAM_GROUP_ID");
            columnMap.Add("ExaminationsID", "EXAMINATIONS_ID");
            columnMap.Add("ExamBagCode", "EXAM_BAG_CODE");
            columnMap.Add("ExamBagID", "EXAM_BAG_ID");
            return columnMap;
        }

        public IDictionary<string, object> ExamSupervisoryAssignmentMapping()
        {
            Dictionary<string, object> columnMap = new Dictionary<string, object>();
            columnMap.Add("UpdateTime", "UPDATE_TIME");
            columnMap.Add("CreateTime", "CREATE_TIME");
            columnMap.Add("AssignedDate", "ASSIGNED_DATE");
            columnMap.Add("ExamRoomID", "EXAM_ROOM_ID");
            columnMap.Add("SubjectID", "SUBJECT_ID");
            columnMap.Add("ExamGroupID", "EXAM_GROUP_ID");
            columnMap.Add("ExaminationsID", "EXAMINATIONS_ID");
            columnMap.Add("AcademicYearID", "ACADEMIC_YEAR_ID");
            columnMap.Add("ExamSupervisoryID", "EXAM_SUPERVISORY_ID");
            columnMap.Add("ExamSupervisoryAssignmentID", "EXAM_SUPERVISORY_ASSIGNMENT_ID");
            return columnMap;
        }

        public IDictionary<string, object> ExamSupervisoryMapping()
        {
            Dictionary<string, object> columnMap = new Dictionary<string, object>();
            columnMap.Add("UpdateTime", "UPDATE_TIME");
            columnMap.Add("CreateTime", "CREATE_TIME");
            columnMap.Add("TeacherID", "TEACHER_ID");
            columnMap.Add("AcademicYearID", "ACADEMIC_YEAR_ID");
            columnMap.Add("ExaminationsID", "EXAMINATIONS_ID");
            columnMap.Add("ExamSupervisoryID", "EXAM_SUPERVISORY_ID");
            return columnMap;
        }


        #endregion
    }
}