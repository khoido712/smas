﻿/**
 * The Viettel License
 *
 * Copyright 2012 Viettel Telecom. All rights reserved.
 * VIETTEL PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */

/** 
* @author  Namta
* @version $Revision: $
*/

using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Linq.Expressions;
using System.Web;
using log4net;

using SMAS.DAL.Repository;
using SMAS.DAL.IRepository;
using SMAS.Business.IBusiness;
using SMAS.Models.Models;
using SMAS.Business.Common;

namespace SMAS.Business.Business
{
    public partial class MealCatBusiness
    {
        #region
        /// <summary>
        /// Validate
        /// </summary>
        /// <param name="entity"></param>
        public void Validate(MealCat entity)
        {
            ValidationMetadata.ValidateObject(entity);
            //check validate
            //this.CheckDuplicate(entity.MealName, GlobalConstants.LIST_SCHEMA, "MealCat", "MealName", true, 0, "MealCat_Label_MealName");// chưa có ID của trường nên chưa chính xác
            this.CheckValidate(entity.MealName, entity.SchoolID);
        }
        #endregion

        public void CheckValidate(string MealName, int SchoolID) // kiểm tra tên bữa ăn đã tồn tại trong trường chưa
        {
            IQueryable<MealCat> mealCat = this.MealCatBusiness.All;
            if (SchoolID != 0)
            {
                mealCat = mealCat.Where(p => p.SchoolID == SchoolID);
            }
            if (MealName != "")
            {
                mealCat = mealCat.Where(p => p.MealName == MealName && p.IsActive == true);
                if (mealCat.Count() > 0)
                {
                    throw new BusinessException("Common_Validate_Duplicate", "MealCat_Label_MealName");
                }
            }

        }

        #region
        /// <summary>
        /// Namta
        /// </summary>
        /// Insert MealCat
        /// 
        /// <param name="MealCat"></param>
        public void Insert(MealCat MealCat)
        {
            //check gia tri
            Validate(MealCat);

            MealCat.IsActive = true;
            base.Insert(MealCat);
        }
        #endregion

        #region
        /// <summary>
        /// Namta
        /// </summary>
        /// Update MealCat
        /// 
        /// <param name="MealCat"></param>
        public void Update(MealCat MealCat)
        {
            //check gia tri
            //Validate(MealCat);

            IDictionary<string, object> SearchInfo = new Dictionary<string, object>();
            SearchInfo["SchoolID"] = MealCat.SchoolID;
            SearchInfo["MealCatID"] = MealCat.MealCatID;

            bool compatible = new MealCatRepository(this.context).ExistsRow(GlobalConstants.LIST_SCHEMA, "MealCat", SearchInfo);

            if (!compatible)
            {
                throw new BusinessException("Common_Validate_NotCompatible");
            }
            if (this.All.Where(o => o.MealName.Equals(MealCat.MealName) && o.MealCatID != MealCat.MealCatID && o.SchoolID == MealCat.SchoolID).Count() > 0)
            {
                List<object> Params = new List<object>();
                Params.Add("MealCat_Label_MealName");
                throw new BusinessException("Common_Validate_Duplicate", Params);
            }
            else
            {
                base.Update(MealCat);

            }
        }
        #endregion

        #region
        /// <summary>
        /// Namta
        /// </summary>
        /// Delete MealCat
        /// 
        /// <param name="MealCat"></param>
        public void Delete(int MealID)
        {
            //check gia tri

            MealCat MealCat = this.Find(MealID);
            if (MealCat != null)
            {
                IDictionary<string, object> SearchInfo = new Dictionary<string, object>();
                SearchInfo["SchoolID"] = MealCat.SchoolID;
                SearchInfo["MealCatID"] = MealCat.MealCatID;

                bool compatible = new MealCatRepository(this.context).ExistsRow(GlobalConstants.LIST_SCHEMA, "MealCat", SearchInfo);

                if (!compatible)
                {
                    throw new BusinessException("Common_Validate_NotCompatible");
                }
                bool checkContraint = new MealCatRepository(this.context).CheckConstraints(GlobalConstants.LIST_SCHEMA, "MealCat", MealCat.MealCatID);
                if (checkContraint)
                {
                    List<object> Params = new List<object>();
                    Params.Add("MealCat_Label_MealName");
                    throw new BusinessException("Common_Validate_Using", Params);
                }

                if (this.All.Where(o => o.MealName.Equals(MealCat.MealName) && o.MealCatID != MealCat.MealCatID && o.IsActive == true).Count() > 0 )
                {
                    List<object> Params = new List<object>();
                    Params.Add("MealCat_Label_MealName");
                    throw new BusinessException("Common_Validate_Duplicate", Params);
                }
                else
                {
                    MealCat.IsActive = false;
                    MealCat.ModifiedDate = DateTime.Today;
                    base.Update(MealCat);

                }

            }


        }
        #endregion

        #region
        /// <summary>
        /// Namta
        /// </summary>
        /// Search MealCat
        /// 
        /// <param name="MealCat"></param>
        public IQueryable<MealCat> Search(IDictionary<string, object> dic)
        {
            string MealName = Utils.GetString(dic, "MealName");
            string Note = Utils.GetString(dic, "Note");
            int SchoolID = Utils.GetInt(dic, "SchoolID");
            Boolean? IsActive = Utils.GetBool(dic, "IsActive");
            if (IsActive == null)
            {
                IsActive = true;
            }

            IQueryable<MealCat> listMealCat = this.All;

            if (MealName != "")
            {
                listMealCat = listMealCat.Where(o => o.MealName.Contains(MealName.ToLower()));
            }

            if (Note != "")
            {
                listMealCat = listMealCat.Where(o => o.Note.Contains(Note.ToLower()));
            }

            if (SchoolID != 0)
            {
                listMealCat = listMealCat.Where(o => o.SchoolID == SchoolID);
            }

            if (IsActive != null)
            {
                listMealCat = listMealCat.Where(o => o.IsActive == IsActive);
            }


            return listMealCat;

        }
        #endregion
        #region SearchBySchool
        public IQueryable<MealCat> SearchBySchool(int SchoolID, IDictionary<string, object> dic)
        {
            if (SchoolID == 0)
                return null;
            else
            {
                dic["SchoolID"] = SchoolID;
                return Search(dic);
            }
        }
        #endregion
    }

}