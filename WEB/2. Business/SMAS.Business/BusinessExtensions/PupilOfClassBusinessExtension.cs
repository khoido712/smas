/**
 * The Viettel License
 *
 * Copyright 2012 Viettel Telecom. All rights reserved.
 * VIETTEL PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */

/** 
* @author  dungnt
* @version $Revision: $
*/

using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Linq.Expressions;
using System.Web;
using log4net;

using SMAS.DAL.Repository;
using SMAS.DAL.IRepository;
using SMAS.Business.IBusiness;
using SMAS.Models.Models;
using SMAS.Business.Common;
using SMAS.Business.BusinessObject;
using SMAS.Business.SearchForm;
using System.IO;
using SMAS.VTUtils.Excel.Export;
using System.Threading.Tasks;
using Oracle.DataAccess.Client;
using SMAS.VTUtils.Log;
using System.Text.RegularExpressions;

namespace SMAS.Business.Business
{
    /// <summary>
    /// thông tin phân bổ học sinh vào lớp
    /// <author>dungnt</author>
    /// <date>05/09/2012</date>
    /// <update-by>namdv3</update-by>
    /// <update-date>13/10/2012</update-date>
    /// </summary>
    public partial class PupilOfClassBusiness
    {
        #region Export PupilOfClassReport

        public ProcessedReport InsertProcessChildrenContact(IDictionary<string, object> dicReport, Stream excel)
        {
            string reportCode = SystemParamsInFile.ChildrenContact;
            int AcademicYearID = Utils.GetInt(dicReport, "AcademicYearID");
            int SchoolID = Utils.GetInt(dicReport, "SchoolID");
            int AppliedLevelID = Utils.GetInt(dicReport, "AppliedLevelID");
            int EducationLevelID = Utils.GetInt(dicReport, "EducationLevelID");
            int ClassID = Utils.GetInt(dicReport, "ClassID");
            ReportDefinition reportDef = ReportDefinitionBusiness.GetByCode(reportCode);
            ProcessedReport pr = new ProcessedReport();
            pr.ReportCode = reportCode;
            pr.ProcessedDate = DateTime.Now;
            pr.InputParameterHashKey = GetHashKey(AcademicYearID, SchoolID, AppliedLevelID, EducationLevelID, ClassID);
            pr.ReportData = ReportUtils.Compress(excel);
            //Tạo tên file
            string AppliedLevelName = "";
            string EducationName = "";
            string ClassName = "";
            if (AppliedLevelID == GlobalConstants.APPLIED_LEVEL_CRECHE)
            {
                AppliedLevelName = "NT";
            }
            else if (AppliedLevelID == GlobalConstants.APPLIED_LEVEL_KINDER_GARTEN)
            {
                AppliedLevelName = "MG";
            }

            if (EducationLevelID > 0)
            {
                EducationName = "Khoi_" + EducationLevelID;
            }

            string outputNamePattern = reportDef.OutputNamePattern;
            string schoolLevel = ReportUtils.ConvertAppliedLevelForReportName(AppliedLevelID);
            outputNamePattern = outputNamePattern.Replace("[AppliedLevel]", AppliedLevelName);
            outputNamePattern = outputNamePattern.Replace("[EducationLevel]", EducationName);
            //outputNamePattern = outputNamePattern.Replace("[EducationLevel]", EducationName);
            pr.ReportName = ReportUtils.RemoveSpecialCharacters(outputNamePattern) + "." + reportDef.OutputFormat;

            IDictionary<string, object> dicInsert = new Dictionary<string, object> {
                {"AcademicYearID", AcademicYearID},
                {"SchoolID", SchoolID}
            };
            if (!string.IsNullOrEmpty(EducationName))
            {
                dicInsert.Add("EducationName", EducationName);
            }
            if (!string.IsNullOrEmpty(ClassName))
            {
                dicInsert.Add("ClassName", ClassName);
            }
            ProcessedReportParameterRepository.Insert(dicInsert, pr);
            ProcessedReportRepository.Insert(pr);
            ProcessedReportRepository.Save();
            return pr;
        }

        public ProcessedReport GetProcessReportChildrenContact(IDictionary<string, object> dicReport)
        {
            string reportCode = SystemParamsInFile.ChildrenContact;
            //string reportCode ="";
            int AcademicYearID = Utils.GetInt(dicReport, "AcademicYearID");
            int SchoolID = Utils.GetInt(dicReport, "SchoolID");
            int AppliedLevelID = Utils.GetInt(dicReport, "AppliedLevelID");
            int EducationLevelID = Utils.GetInt(dicReport, "EducationLevelID");
            int ClassID = Utils.GetInt(dicReport, "ClassID");
            string inputParameterHashKey = GetHashKey(AcademicYearID, SchoolID, AppliedLevelID, EducationLevelID, ClassID);
            return ProcessedReportBusiness.GetProcessedReport(reportCode, inputParameterHashKey);
        }

        public ProcessedReport InsertProcessReportMeetingInvitation(IDictionary<string, object> dicReport, Stream excel)
        {
            string reportCode = SystemParamsInFile.MeetingInvitationReport;
            int AcademicYearID = Utils.GetInt(dicReport, "AcademicYearID");
            int SchoolID = Utils.GetInt(dicReport, "SchoolID");
            int AppliedLevelID = Utils.GetInt(dicReport, "AppliedLevelID");
            int EducationLevelID = Utils.GetInt(dicReport, "EducationLevelID");
            int ClassID = Utils.GetInt(dicReport, "ClassID");
            ReportDefinition reportDef = ReportDefinitionBusiness.GetByCode(reportCode);
            ProcessedReport pr = new ProcessedReport();
            pr.ReportCode = reportCode;
            pr.ProcessedDate = DateTime.Now;
            pr.InputParameterHashKey = GetHashKey(AcademicYearID, SchoolID, AppliedLevelID, EducationLevelID, ClassID);
            pr.ReportData = ReportUtils.Compress(excel);
            //Tạo tên file
            string AppliedLevelName = "";
            string EducationName = "";
            string ClassName = "";
            if (AppliedLevelID == GlobalConstants.APPLIED_LEVEL_CRECHE)
            {
                AppliedLevelName = "NT";
            }
            else if (AppliedLevelID == GlobalConstants.APPLIED_LEVEL_KINDER_GARTEN)
            {
                AppliedLevelName = "MG";
            }

            if (EducationLevelID > 0)
            {
                EducationName = "Khoi" + EducationLevelID;
            }
            if (ClassID > 0)
            {
                ClassName = ClassProfileBusiness.Find(ClassID).DisplayName;
            }



            string outputNamePattern = reportDef.OutputNamePattern;
            string schoolLevel = ReportUtils.ConvertAppliedLevelForReportName(AppliedLevelID);
            outputNamePattern = outputNamePattern.Replace("[AppliedLevel]", AppliedLevelName);
            if (ClassID > 0)
            {
                outputNamePattern = outputNamePattern.Replace("[EducationLevel]", ClassName);
            }
            else
            {
                outputNamePattern = outputNamePattern.Replace("[EducationLevel]", EducationName);
            }

            pr.ReportName = outputNamePattern + "." + reportDef.OutputFormat;

            IDictionary<string, object> dicInsert = new Dictionary<string, object> {
                {"AcademicYearID", AcademicYearID},
                {"SchoolID", SchoolID}
            };
            if (!string.IsNullOrEmpty(EducationName))
            {
                dicInsert.Add("EducationName", EducationName);
            }
            if (!string.IsNullOrEmpty(ClassName))
            {
                dicInsert.Add("ClassName", ClassName);
            }
            ProcessedReportParameterRepository.Insert(dicInsert, pr);
            ProcessedReportRepository.Insert(pr);
            ProcessedReportRepository.Save();
            return pr;
        }

        public ProcessedReport GetProcessReportMeetingInvitation(IDictionary<string, object> dicReport)
        {
            string reportCode = SystemParamsInFile.MeetingInvitationReport;
            //string reportCode ="";
            int AcademicYearID = Utils.GetInt(dicReport, "AcademicYearID");
            int SchoolID = Utils.GetInt(dicReport, "SchoolID");
            int AppliedLevelID = Utils.GetInt(dicReport, "AppliedLevelID");
            int EducationLevelID = Utils.GetInt(dicReport, "EducationLevelID");
            int ClassID = Utils.GetInt(dicReport, "ClassID");
            string inputParameterHashKey = GetHashKey(AcademicYearID, SchoolID, AppliedLevelID, EducationLevelID, ClassID);
            return ProcessedReportBusiness.GetProcessedReport(reportCode, inputParameterHashKey);
        }

        public ProcessedReport InsertProcessReportNumberOfClass(IDictionary<string, object> dicReport, Stream excel)
        {
            string reportCode = SystemParamsInFile.HS_THONGKESISO;
            int AcademicYearID = Utils.GetInt(dicReport, "AcademicYearID");
            int SchoolID = Utils.GetInt(dicReport, "SchoolID");
            int AppliedLevelID = Utils.GetInt(dicReport, "AppliedLevelID");
            int EducationLevelID = Utils.GetInt(dicReport, "EducationLevelID");
            ReportDefinition reportDef = ReportDefinitionBusiness.GetByCode(reportCode);
            ProcessedReport pr = new ProcessedReport();
            pr.ReportCode = reportCode;
            pr.ProcessedDate = DateTime.Now;
            pr.InputParameterHashKey = GetHashKeyNumberOfClass(AcademicYearID, SchoolID, AppliedLevelID, EducationLevelID);
            pr.ReportData = ReportUtils.Compress(excel);
            //Tạo tên file
            string AppliedLevelName = "";
            string EducationName = "";
            string ClassName = "";
            if (AppliedLevelID == GlobalConstants.APPLIED_LEVEL_PRIMARY)
            {
                AppliedLevelName = "TH";
            }

            else if (AppliedLevelID == 2)
            {
                AppliedLevelName = "THCS";
            }
            else if (AppliedLevelID == 3)
            {
                AppliedLevelName = "THPT";
            }
            /* else 
                 AppliedLevelName = "MN";
             else if (AppliedLevelID == 2)
                 {
                     AppliedLevelName = "THCS";
                 }
                 else
                 {
                     AppliedLevelName = "THPT";
                 }*/
            else if (AppliedLevelID == GlobalConstants.APPLIED_LEVEL_SECONDARY)
            {
                AppliedLevelName = "THCS";
            }
            else if (AppliedLevelID == GlobalConstants.APPLIED_LEVEL_TERTIARY)
            {
                AppliedLevelName = "THPT";
            }
            else if (AppliedLevelID == GlobalConstants.APPLIED_LEVEL_CRECHE)
            {
                AppliedLevelName = "NT";
            }
            else if (AppliedLevelID == GlobalConstants.APPLIED_LEVEL_KINDER_GARTEN)
            {
                AppliedLevelName = "MG";
            }

            if (EducationLevelID > 0)
            {
                EducationName = "Khoi_" + EducationLevelID;
            }

            string outputNamePattern = reportDef.OutputNamePattern;
            string schoolLevel = ReportUtils.ConvertAppliedLevelForReportName(AppliedLevelID);
            outputNamePattern = outputNamePattern.Replace("[AppliedLevel]", AppliedLevelName);
            outputNamePattern = outputNamePattern.Replace("[EducationLevel]", EducationName);
            pr.ReportName = ReportUtils.RemoveSpecialCharacters(outputNamePattern) + "." + reportDef.OutputFormat;

            IDictionary<string, object> dicInsert = new Dictionary<string, object> {
                {"AcademicYearID", AcademicYearID},
                {"SchoolID", SchoolID}
            };
            if (!string.IsNullOrEmpty(EducationName))
            {
                dicInsert.Add("EducationName", EducationName);
            }
            if (!string.IsNullOrEmpty(ClassName))
            {
                dicInsert.Add("ClassName", ClassName);
            }
            ProcessedReportParameterRepository.Insert(dicInsert, pr);
            ProcessedReportRepository.Insert(pr);
            ProcessedReportRepository.Save();
            return pr;
        }

        public ProcessedReport InsertProcessReportSummaryEvaluation(IDictionary<string, object> dicReport, Stream excel)
        {
            string reportCode = SystemParamsInFile.SummaryResultEvaluation;
            int AcademicYearID = Utils.GetInt(dicReport, "AcademicYearID");
            int SchoolID = Utils.GetInt(dicReport, "SchoolID");
            int AppliedLevelID = Utils.GetInt(dicReport, "AppliedLevelID");
            int EducationLevelID = Utils.GetInt(dicReport, "EducationLevelID");
            int ClassID = Utils.GetInt(dicReport, "ClassID");
            ReportDefinition reportDef = ReportDefinitionBusiness.GetByCode(reportCode);
            ProcessedReport pr = new ProcessedReport();
            pr.ReportCode = reportCode;
            pr.ProcessedDate = DateTime.Now;
            pr.InputParameterHashKey = GetHashKey(AcademicYearID, SchoolID, AppliedLevelID, EducationLevelID, ClassID);
            pr.ReportData = ReportUtils.Compress(excel);
            //Tạo tên file
            string AppliedLevelName = "";
            string EducationName = "";
            string ClassName = "";
            if (AppliedLevelID == GlobalConstants.APPLIED_LEVEL_CRECHE)
            {
                AppliedLevelName = "NT";
            }
            else if (AppliedLevelID == GlobalConstants.APPLIED_LEVEL_KINDER_GARTEN)
            {
                AppliedLevelName = "MG";
            }

            if (EducationLevelID > 0)
            {
                EducationName = "Khoi_" + EducationLevelID;
            }

            string outputNamePattern = reportDef.OutputNamePattern;
            string schoolLevel = ReportUtils.ConvertAppliedLevelForReportName(AppliedLevelID);
            outputNamePattern = outputNamePattern.Replace("[AppliedLevel]", AppliedLevelName);
            outputNamePattern = outputNamePattern.Replace("[EducationLevel]", EducationName);
            pr.ReportName = ReportUtils.RemoveSpecialCharacters(outputNamePattern) + "." + reportDef.OutputFormat;

            IDictionary<string, object> dicInsert = new Dictionary<string, object> {
                {"AcademicYearID", AcademicYearID},
                {"SchoolID", SchoolID}
            };
            if (!string.IsNullOrEmpty(EducationName))
            {
                dicInsert.Add("EducationName", EducationName);
            }
            if (!string.IsNullOrEmpty(ClassName))
            {
                dicInsert.Add("ClassName", ClassName);
            }
            ProcessedReportParameterRepository.Insert(dicInsert, pr);
            ProcessedReportRepository.Insert(pr);
            ProcessedReportRepository.Save();
            return pr;
        }

        public ProcessedReport GetProcessReportSummaryEvaluation(IDictionary<string, object> dicReport)
        {
            string reportCode = SystemParamsInFile.SummaryResultEvaluation;
            //string reportCode ="";
            int AcademicYearID = Utils.GetInt(dicReport, "AcademicYearID");
            int SchoolID = Utils.GetInt(dicReport, "SchoolID");
            int AppliedLevelID = Utils.GetInt(dicReport, "AppliedLevelID");
            int EducationLevelID = Utils.GetInt(dicReport, "EducationLevelID");
            int ClassID = Utils.GetInt(dicReport, "ClassID");
            string inputParameterHashKey = GetHashKey(AcademicYearID, SchoolID, AppliedLevelID, EducationLevelID, ClassID);
            return ProcessedReportBusiness.GetProcessedReport(reportCode, inputParameterHashKey);
        }

        public ProcessedReport InsertProcessReportEvaluation(IDictionary<string, object> dicReport, Stream excel)
        {
            string reportCode = SystemParamsInFile.ReportEvaluation;
            int AcademicYearID = Utils.GetInt(dicReport, "AcademicYearID");
            int SchoolID = Utils.GetInt(dicReport, "SchoolID");
            int AppliedLevelID = Utils.GetInt(dicReport, "AppliedLevelID");
            int EducationLevelID = Utils.GetInt(dicReport, "EducationLevelID");
            int ClassID = Utils.GetInt(dicReport, "ClassID");
            ReportDefinition reportDef = ReportDefinitionBusiness.GetByCode(reportCode);
            ProcessedReport pr = new ProcessedReport();
            pr.ReportCode = reportCode;
            pr.ProcessedDate = DateTime.Now;
            pr.InputParameterHashKey = GetHashKey(AcademicYearID, SchoolID, AppliedLevelID, EducationLevelID, ClassID);
            pr.ReportData = ReportUtils.Compress(excel);
            //Tạo tên file
            string AppliedLevelName = "";
            string EducationName = "";
            string ClassName = "";
            if (AppliedLevelID == GlobalConstants.APPLIED_LEVEL_CRECHE)
            {
                AppliedLevelName = "NT";
            }
            else if (AppliedLevelID == GlobalConstants.APPLIED_LEVEL_KINDER_GARTEN)
            {
                AppliedLevelName = "MG";
            }

            if (EducationLevelID > 0)
            {
                EducationName = "Khoi_" + EducationLevelID;
            }

            string outputNamePattern = reportDef.OutputNamePattern;
            string schoolLevel = ReportUtils.ConvertAppliedLevelForReportName(AppliedLevelID);
            outputNamePattern = outputNamePattern.Replace("[AppliedLevel]", AppliedLevelName);
            outputNamePattern = outputNamePattern.Replace("[EducationLevel]", EducationName);
            pr.ReportName = ReportUtils.RemoveSpecialCharacters(outputNamePattern) + "." + reportDef.OutputFormat;

            IDictionary<string, object> dicInsert = new Dictionary<string, object> {
                {"AcademicYearID", AcademicYearID},
                {"SchoolID", SchoolID}
            };
            if (!string.IsNullOrEmpty(EducationName))
            {
                dicInsert.Add("EducationName", EducationName);
            }
            if (!string.IsNullOrEmpty(ClassName))
            {
                dicInsert.Add("ClassName", ClassName);
            }
            ProcessedReportParameterRepository.Insert(dicInsert, pr);
            ProcessedReportRepository.Insert(pr);
            ProcessedReportRepository.Save();
            return pr;
        }


        public ProcessedReport GetProcessReportEvaluation(IDictionary<string, object> dicReport)
        {
            string reportCode = SystemParamsInFile.ReportEvaluation;
            //string reportCode ="";
            int AcademicYearID = Utils.GetInt(dicReport, "AcademicYearID");
            int SchoolID = Utils.GetInt(dicReport, "SchoolID");
            int AppliedLevelID = Utils.GetInt(dicReport, "AppliedLevelID");
            int EducationLevelID = Utils.GetInt(dicReport, "EducationLevelID");
            int ClassID = Utils.GetInt(dicReport, "ClassID");
            string inputParameterHashKey = GetHashKey(AcademicYearID, SchoolID, AppliedLevelID, EducationLevelID, ClassID);
            return ProcessedReportBusiness.GetProcessedReport(reportCode, inputParameterHashKey);
        }

        public ProcessedReport InsertProcessReportNumberOfChildren(IDictionary<string, object> dicReport, Stream excel)
        {
            string reportCode = SystemParamsInFile.MN_THONGKESISO;
            //string reportCode = "";
            int AcademicYearID = Utils.GetInt(dicReport, "AcademicYearID");
            int SchoolID = Utils.GetInt(dicReport, "SchoolID");
            int AppliedLevelID = Utils.GetInt(dicReport, "AppliedLevelID");
            int EducationLevelID = Utils.GetInt(dicReport, "EducationLevelID");
            ReportDefinition reportDef = ReportDefinitionBusiness.GetByCode(reportCode);
            ProcessedReport pr = new ProcessedReport();
            pr.ReportCode = reportCode;
            pr.ProcessedDate = DateTime.Now;
            pr.InputParameterHashKey = GetHashKeyNumberOfClass(AcademicYearID, SchoolID, AppliedLevelID, EducationLevelID);
            pr.ReportData = ReportUtils.Compress(excel);
            //Tạo tên file
            string AppliedLevelName = "";
            string EducationName = "";
            string ClassName = "";
            if (AppliedLevelID == GlobalConstants.APPLIED_LEVEL_CRECHE)
            {
                AppliedLevelName = "NT";
            }
            else if (AppliedLevelID == GlobalConstants.APPLIED_LEVEL_KINDER_GARTEN)
            {
                AppliedLevelName = "MG";
            }

            if (EducationLevelID > 0)
            {
                EducationName = "Khoi_" + EducationLevelID;
            }

            string outputNamePattern = reportDef.OutputNamePattern;
            string schoolLevel = ReportUtils.ConvertAppliedLevelForReportName(AppliedLevelID);
            outputNamePattern = outputNamePattern.Replace("[AppliedLevel]", AppliedLevelName);
            outputNamePattern = outputNamePattern.Replace("[EducationLevel]", EducationName);
            pr.ReportName = ReportUtils.RemoveSpecialCharacters(outputNamePattern) + "." + reportDef.OutputFormat;

            IDictionary<string, object> dicInsert = new Dictionary<string, object> {
                {"AcademicYearID", AcademicYearID},
                {"SchoolID", SchoolID}
            };
            if (!string.IsNullOrEmpty(EducationName))
            {
                dicInsert.Add("EducationName", EducationName);
            }
            if (!string.IsNullOrEmpty(ClassName))
            {
                dicInsert.Add("ClassName", ClassName);
            }
            ProcessedReportParameterRepository.Insert(dicInsert, pr);
            ProcessedReportRepository.Insert(pr);
            ProcessedReportRepository.Save();
            return pr;
        }

        public ProcessedReport GetProcessReportNumberOfChildren(IDictionary<string, object> dicReport)
        {
            string reportCode = SystemParamsInFile.MN_THONGKESISO;
            //string reportCode ="";
            int AcademicYearID = Utils.GetInt(dicReport, "AcademicYearID");
            int SchoolID = Utils.GetInt(dicReport, "SchoolID");
            int AppliedLevelID = Utils.GetInt(dicReport, "AppliedLevelID");
            int EducationLevelID = Utils.GetInt(dicReport, "EducationLevelID");
            string inputParameterHashKey = GetHashKeyNumberOfClass(AcademicYearID, SchoolID, AppliedLevelID, EducationLevelID);
            return ProcessedReportBusiness.GetProcessedReport(reportCode, inputParameterHashKey);
        }
        public ProcessedReport InsertProcessReport(IDictionary<string, object> dicReport, Stream excel)
        {
            string reportCode = SystemParamsInFile.HS_DANHSACHHOCSINH;
            int AcademicYearID = Utils.GetInt(dicReport, "AcademicYearID");
            int SchoolID = Utils.GetInt(dicReport, "SchoolID");
            int AppliedLevelID = Utils.GetInt(dicReport, "AppliedLevelID");
            int EducationLevelID = Utils.GetInt(dicReport, "EducationLevelID");
            int ClassID = Utils.GetInt(dicReport, "ClassID");
            ReportDefinition reportDef = ReportDefinitionBusiness.GetByCode(reportCode);
            ProcessedReport pr = new ProcessedReport();
            pr.ReportCode = reportCode;
            pr.ProcessedDate = DateTime.Now;
            pr.InputParameterHashKey = GetHashKey(AcademicYearID, SchoolID, AppliedLevelID, EducationLevelID, ClassID);
            pr.ReportData = ReportUtils.Compress(excel);
            //Tạo tên file
            string AppliedLevelName = "";
            string EducationName = "";
            string ClassName = "";
            //if (AppliedLevelID == 1)
            //{
            //    AppliedLevelName = "TH";
            //}
            //else
            //    if (AppliedLevelID == 2)
            //    {
            //        AppliedLevelName = "THCS";
            //    }
            //    else
            //    {
            //        AppliedLevelName = "THPT";
            //    }

            if (AppliedLevelID == GlobalConstants.APPLIED_LEVEL_PRIMARY)
            {
                AppliedLevelName = "TH";
            }
            else if (AppliedLevelID == GlobalConstants.APPLIED_LEVEL_SECONDARY)
            {
                AppliedLevelName = "THCS";
            }
            else if (AppliedLevelID == GlobalConstants.APPLIED_LEVEL_TERTIARY)
            {
                AppliedLevelName = "THPT";
            }
            else if (AppliedLevelID == GlobalConstants.APPLIED_LEVEL_CRECHE)
            {
                AppliedLevelName = "NT";
            }
            else if (AppliedLevelID == GlobalConstants.APPLIED_LEVEL_KINDER_GARTEN)
            {
                AppliedLevelName = "MG";
            }

            if (EducationLevelID > 0)
            {
                EducationName = "Khoi_" + EducationLevelID;
            }

            if (ClassID > 0)
            {
                ClassName = ClassProfileBusiness.Find(ClassID).DisplayName;
            }

            string outputNamePattern = reportDef.OutputNamePattern;
            string schoolLevel = ReportUtils.ConvertAppliedLevelForReportName(AppliedLevelID);
            outputNamePattern = outputNamePattern.Replace("[AppliedLevel]", AppliedLevelName);
            outputNamePattern = outputNamePattern.Replace("[Education]", EducationName);
            outputNamePattern = outputNamePattern.Replace("[Class]", ClassName);
            pr.ReportName = ReportUtils.RemoveSpecialCharacters(outputNamePattern) + "." + reportDef.OutputFormat;

            IDictionary<string, object> dicInsert = new Dictionary<string, object> {
                {"AcademicYearID", AcademicYearID},
                {"SchoolID", SchoolID}
            };
            if (!string.IsNullOrEmpty(EducationName))
            {
                dicInsert.Add("EducationName", EducationName);
            }
            if (!string.IsNullOrEmpty(ClassName))
            {
                dicInsert.Add("ClassName", ClassName);
            }
            ProcessedReportParameterRepository.Insert(dicInsert, pr);
            ProcessedReportRepository.Insert(pr);
            ProcessedReportRepository.Save();
            return pr;
        }


        public ProcessedReport InsertProcessReportOfChildren(IDictionary<string, object> dicReport, Stream excel, string reportCode)
        {
            //string reportCode = SystemParamsInFile.MN_TRECHINHSACH;           
            int AcademicYearID = Utils.GetInt(dicReport, "AcademicYearID");
            int SchoolID = Utils.GetInt(dicReport, "SchoolID");
            int AppliedLevelID = Utils.GetInt(dicReport, "AppliedLevelID");
            int EducationLevelID = Utils.GetInt(dicReport, "EducationLevelID");
            int ClassID = Utils.GetInt(dicReport, "ClassID");
            ReportDefinition reportDef = ReportDefinitionBusiness.GetByCode(reportCode);
            ProcessedReport pr = new ProcessedReport();
            pr.ReportCode = reportCode;
            pr.ProcessedDate = DateTime.Now;
            pr.InputParameterHashKey = GetHashKey(AcademicYearID, SchoolID, AppliedLevelID, EducationLevelID, ClassID);
            pr.ReportData = ReportUtils.Compress(excel);
            //Tạo tên file
            string AppliedLevelName = "";
            string EducationName = "";
            string ClassName = "";

            if (AppliedLevelID == GlobalConstants.APPLIED_LEVEL_PRIMARY)
            {
                AppliedLevelName = "TH";
            }
            else if (AppliedLevelID == GlobalConstants.APPLIED_LEVEL_SECONDARY)
            {
                AppliedLevelName = "THCS";
            }
            else if (AppliedLevelID == GlobalConstants.APPLIED_LEVEL_TERTIARY)
            {
                AppliedLevelName = "THPT";
            }
            else if (AppliedLevelID == GlobalConstants.APPLIED_LEVEL_CRECHE)
            {
                AppliedLevelName = "NT";
            }
            else if (AppliedLevelID == GlobalConstants.APPLIED_LEVEL_KINDER_GARTEN)
            {
                AppliedLevelName = "MG";
            }

            if (EducationLevelID > 0)
            {
                EducationName = "Khoi_" + EducationLevelID;
            }

            if (ClassID > 0)
            {
                ClassName = ClassProfileBusiness.Find(ClassID).DisplayName;
            }

            string outputNamePattern = reportDef.OutputNamePattern;
            string schoolLevel = ReportUtils.ConvertAppliedLevelForReportName(AppliedLevelID);
            outputNamePattern = outputNamePattern.Replace("[AppliedLevel]", AppliedLevelName);
            outputNamePattern = outputNamePattern.Replace("[Education]", EducationName);
            outputNamePattern = outputNamePattern.Replace("[Class]", ClassName);
            pr.ReportName = ReportUtils.RemoveSpecialCharacters(outputNamePattern) + "." + reportDef.OutputFormat;

            IDictionary<string, object> dicInsert = new Dictionary<string, object> {
                {"AcademicYearID", AcademicYearID},
                {"SchoolID", SchoolID}
            };
            if (!string.IsNullOrEmpty(EducationName))
            {
                dicInsert.Add("EducationName", EducationName);
            }
            if (!string.IsNullOrEmpty(ClassName))
            {
                dicInsert.Add("ClassName", ClassName);
            }
            ProcessedReportParameterRepository.Insert(dicInsert, pr);
            ProcessedReportRepository.Insert(pr);
            ProcessedReportRepository.Save();
            return pr;
        }

        public ProcessedReport InsertProcessInformationBook(IDictionary<string, object> dicReport, Stream excel, string reportCode)
        {
            int AcademicYearID = Utils.GetInt(dicReport, "AcademicYearID");
            int SchoolID = Utils.GetInt(dicReport, "SchoolID");
            int AppliedLevelID = Utils.GetInt(dicReport, "AppliedLevelID");
            int EducationLevelID = Utils.GetInt(dicReport, "EducationLevelID");
            int ClassID = Utils.GetInt(dicReport, "ClassID");
            ReportDefinition reportDef = ReportDefinitionBusiness.GetByCode(reportCode);
            ProcessedReport pr = new ProcessedReport();
            pr.ReportCode = reportCode;
            pr.ProcessedDate = DateTime.Now;
            pr.InputParameterHashKey = GetHashKey(AcademicYearID, SchoolID, AppliedLevelID, EducationLevelID, ClassID);
            pr.ReportData = ReportUtils.Compress(excel);
            //Tạo tên file
            string AppliedLevelName = "";
            string EducationName = "";
            string ClassName = "";

            if (AppliedLevelID == GlobalConstants.APPLIED_LEVEL_PRIMARY)
            {
                AppliedLevelName = "TH";
            }
            else if (AppliedLevelID == GlobalConstants.APPLIED_LEVEL_SECONDARY)
            {
                AppliedLevelName = "THCS";
            }
            else if (AppliedLevelID == GlobalConstants.APPLIED_LEVEL_TERTIARY)
            {
                AppliedLevelName = "THPT";
            }
            else if (AppliedLevelID == GlobalConstants.APPLIED_LEVEL_CRECHE)
            {
                AppliedLevelName = "NT";
            }
            else if (AppliedLevelID == GlobalConstants.APPLIED_LEVEL_KINDER_GARTEN)
            {
                AppliedLevelName = "MG";
            }

            if (EducationLevelID > 0)
            {
                EducationName = "Khoi" + EducationLevelID;
            }

            if (ClassID > 0)
            {
                ClassName = ClassProfileBusiness.Find(ClassID).DisplayName;
            }

            if (!string.IsNullOrEmpty(ClassName))
                EducationName = string.Empty;

            string outputNamePattern = reportDef.OutputNamePattern;
            string schoolLevel = ReportUtils.ConvertAppliedLevelForReportName(AppliedLevelID);
            outputNamePattern = outputNamePattern.Replace("[AppliedLevel]", AppliedLevelName);
            outputNamePattern = outputNamePattern.Replace("[EducationLevel]", EducationName);
            outputNamePattern = outputNamePattern.Replace("[ClassName]", ClassName);

            if (string.IsNullOrEmpty(EducationName) && string.IsNullOrEmpty(ClassName))
                outputNamePattern = outputNamePattern.Substring(0, outputNamePattern.Length - 1);
            pr.ReportName = ReportUtils.RemoveSpecialCharacters(outputNamePattern) + "." + reportDef.OutputFormat;

            IDictionary<string, object> dicInsert = new Dictionary<string, object> {
                {"AcademicYearID", AcademicYearID},
                {"SchoolID", SchoolID}
            };
            if (!string.IsNullOrEmpty(EducationName))
            {
                dicInsert.Add("EducationName", EducationName);
            }
            if (!string.IsNullOrEmpty(ClassName))
            {
                dicInsert.Add("ClassName", ClassName);
            }
            ProcessedReportParameterRepository.Insert(dicInsert, pr);
            ProcessedReportRepository.Insert(pr);
            ProcessedReportRepository.Save();
            return pr;
        }


        public ProcessedReport InsertProcessReportSituationStatisticPupil(IDictionary<string, object> dicReport, Stream excel, string reportCode, int Type)
        {
            int YearID = Utils.GetInt(dicReport, "YearID");
            int AcademicYearID = Utils.GetInt(dicReport, "AcademicYearID");
            int SchoolID = Utils.GetInt(dicReport, "SchoolID");
            int AppliedLevelID = Utils.GetInt(dicReport, "AppliedLevelID");
            int EducationLevelID = Utils.GetInt(dicReport, "EducationLevelID");
            int ClassID = Utils.GetInt(dicReport, "ClassID");
            int MonthID = Utils.GetInt(dicReport, "MonthID");
            int DistrictID = Utils.GetInt(dicReport, "DistrictID");
            bool showSheetFemale = Utils.GetBool(dicReport, "SheetPupilFemale");
            bool showSheetEthenic = Utils.GetBool(dicReport, "SheetPupilEthnic");
            bool showSheetFemaleEthenic = Utils.GetBool(dicReport, "SheetPupilFemaleEthnic");

            ReportDefinition reportDef = ReportDefinitionBusiness.GetByCode(reportCode);
            ProcessedReport pr = new ProcessedReport();
            pr.ReportCode = reportCode;
            pr.ProcessedDate = DateTime.Now;

            IDictionary<string, object> dic = new Dictionary<string, object>();
            // Phong - so
            if (Type == 1)
            {
                dic.Add("YearID", YearID);
                dic.Add("MonthID", MonthID);
                dic.Add("DistrictID", DistrictID);
                dic.Add("AppliedLevelID", AppliedLevelID);
                dic.Add("SchoolID", 0);
                dic.Add("AcademicYearID", 0);
            }
            else // Truong
            {
                dic.Add("SchoolID", SchoolID);
                dic.Add("AcademicYearID", AcademicYearID);
                dic.Add("MonthID", MonthID);
                dic.Add("EducationLevelID", EducationLevelID);
                dic.Add("AppliedLevelID", AppliedLevelID);
            }

            dic.Add("SheetPupilFemale", showSheetFemale);
            dic.Add("SheetPupilEthnic", showSheetEthenic);
            dic.Add("SheetPupilFemaleEthnic", showSheetFemaleEthenic);

            pr.InputParameterHashKey = GetHashKeySituationStatisticPupil(dic);

            pr.ReportData = ReportUtils.Compress(excel);

            string AppliedLevelName = "";
            if (AppliedLevelID == GlobalConstants.APPLIED_LEVEL_PRIMARY)
            {
                AppliedLevelName = "TH";
            }
            else if (AppliedLevelID == GlobalConstants.APPLIED_LEVEL_SECONDARY)
            {
                AppliedLevelName = "THCS";
            }
            else if (AppliedLevelID == GlobalConstants.APPLIED_LEVEL_TERTIARY)
            {
                AppliedLevelName = "THPT";
            }

            string outputNamePattern = reportDef.OutputNamePattern;
            outputNamePattern = outputNamePattern.Replace("[AppliedLevel]", AppliedLevelName);
            outputNamePattern = outputNamePattern.Replace("[MonthID]", MonthID.ToString());
            pr.ReportName = ReportUtils.RemoveSpecialCharacters(outputNamePattern) + "." + reportDef.OutputFormat;

            if (!string.IsNullOrEmpty(AppliedLevelName))
            {
                dic.Add("AppliedLevelName", AppliedLevelName);
            }

            ProcessedReportParameterRepository.Insert(dic, pr);
            ProcessedReportRepository.Insert(pr);
            ProcessedReportRepository.Save();
            return pr;
        }

        public ProcessedReport GetProcessReportSituationStatisticPupil(IDictionary<string, object> dicReport, string reportCode)
        {
            string inputParameterHashKey = GetHashKeySituationStatisticPupil(dicReport);
            return ProcessedReportBusiness.GetProcessedReport(reportCode, inputParameterHashKey);
        }

        private string GetHashKeySituationStatisticPupil(IDictionary<string, object> dic)
        {
            return ReportUtils.GetHashKey(dic);
        }

        public ProcessedReport GetProcessReportOfChilden(IDictionary<string, object> dicReport, string reportCode)
        {
            //string reportCode = SystemParamsInFile.HS_DANHSACHHOCSINH;
            int AcademicYearID = Utils.GetInt(dicReport, "AcademicYearID");
            int SchoolID = Utils.GetInt(dicReport, "SchoolID");
            int AppliedLevelID = Utils.GetInt(dicReport, "AppliedLevelID");
            int EducationLevelID = Utils.GetInt(dicReport, "EducationLevelID");
            int ClassID = Utils.GetInt(dicReport, "ClassID");
            string inputParameterHashKey = GetHashKey(AcademicYearID, SchoolID, AppliedLevelID, EducationLevelID, ClassID);
            return ProcessedReportBusiness.GetProcessedReport(reportCode, inputParameterHashKey);
        }

        public ProcessedReport GetProcessReport(IDictionary<string, object> dicReport)
        {
            string reportCode = SystemParamsInFile.HS_DANHSACHHOCSINH;
            int AcademicYearID = Utils.GetInt(dicReport, "AcademicYearID");
            int SchoolID = Utils.GetInt(dicReport, "SchoolID");
            int AppliedLevelID = Utils.GetInt(dicReport, "AppliedLevelID");
            int EducationLevelID = Utils.GetInt(dicReport, "EducationLevelID");
            int ClassID = Utils.GetInt(dicReport, "ClassID");
            string inputParameterHashKey = GetHashKey(AcademicYearID, SchoolID, AppliedLevelID, EducationLevelID, ClassID);
            return ProcessedReportBusiness.GetProcessedReport(reportCode, inputParameterHashKey);
        }

        public ProcessedReport GetProcessReportNumberOfClass(IDictionary<string, object> dicReport)
        {
            string reportCode = SystemParamsInFile.HS_THONGKESISO;
            int AcademicYearID = Utils.GetInt(dicReport, "AcademicYearID");
            int SchoolID = Utils.GetInt(dicReport, "SchoolID");
            int AppliedLevelID = Utils.GetInt(dicReport, "AppliedLevelID");
            int EducationLevelID = Utils.GetInt(dicReport, "EducationLevelID");
            string inputParameterHashKey = GetHashKeyNumberOfClass(AcademicYearID, SchoolID, AppliedLevelID, EducationLevelID);
            return ProcessedReportBusiness.GetProcessedReport(reportCode, inputParameterHashKey);
        }
        //Tạo mảng băm cho các tham số đầu vào cho báo cáo
        private string GetHashKey(int AcademicYearID, int SchoolID, int AppliedLevelID, int EducationLevelID, int ClassID)
        {
            IDictionary<string, object> dic = new Dictionary<string, object> {
                {"AcademicYearID", AcademicYearID},
                {"SchoolID", SchoolID},
                {"AppliedLevelID",AppliedLevelID},
                {"EducationLevelID",EducationLevelID},
                {"ClassID",ClassID}
            };
            return ReportUtils.GetHashKey(dic);
        }
        private string GetHashKeyNumberOfClass(int AcademicYearID, int SchoolID, int AppliedLevelID, int EducationLevelID)
        {
            IDictionary<string, object> dic = new Dictionary<string, object> {
                {"AcademicYearID", AcademicYearID},
                {"SchoolID", SchoolID},
                {"AppliedLevelID",AppliedLevelID},
                {"EducationLevelID",EducationLevelID}
            };
            return ReportUtils.GetHashKey(dic);
        }
        #endregion

        #region private member variable
        private const int MAX_LENGTH_DESCRIPTION = 200; //do dai lon nhat truong mieu ta
        private const int MIN_RANGE_STATUS = 1; //gia tri nho nhat cua satus
        private const int MAX_RANGE_STATUS = 5; //gia tri lon nhat cua satus
        #endregion

        #region insert
        /// <summary>
        /// Thêm mới thông tin phân bổ học sinh vào lớp
        /// <author>dungnt</author>
        /// <date>02/10/2012</date>
        /// <update-by>namdv3</update-by>
        /// <update-date>13/10/2012</update-date>
        /// <param name="UserID">user id</param>
        /// <param name="Pupil">du lieu can insert</param>
        public void InsertPupilOfClass(int UserID, PupilOfClass Pupil)
        {
            var ClassProfileRepository = new ClassProfileRepository(this.context);
            var AcademicYearRepository = new AcademicYearRepository(this.context);
            //PupilID: require, FK(PupilProfile)
            Utils.ValidateRequire(Pupil.PupilOfClassID, "Common_Validate_Require");
            bool PupilOfClassIDIsPK = this.All.Where(p => p.PupilOfClassID == Pupil.PupilOfClassID).Select(p => p.PupilOfClassID).FirstOrDefault() > 0;
            if (PupilOfClassIDIsPK)
            {
                throw new BusinessException("PupilOfClass_Validate_IsNotPK");
            }
            //ClassID: require, FK(ClassProfile)
            Utils.ValidateRequire(Pupil.ClassID, "Common_Validate_Require");
            bool ClassIDIsFK = ClassProfileRepository.All.Where(p => p.ClassProfileID == Pupil.ClassID && p.IsActive == true).Select(p => p.ClassProfileID).FirstOrDefault() > 0;
            if (!ClassIDIsFK)
            {
                throw new BusinessException("PupilOfClass_Validate_IsNotClassFK");
            }
            //SchoolID: require, FK(SchoolProfile)
            Utils.ValidateRequire(Pupil.SchoolID, "Common_Validate_Require");
            bool SchoolIDIsFK = new SchoolProfileRepository(this.context).All.Where(p => p.SchoolProfileID == Pupil.SchoolID).Select(p => p.SchoolProfileID).FirstOrDefault() > 0;
            if (!SchoolIDIsFK)
            {
                throw new BusinessException("PupilOfClass_Validate_IsNotSchoolFK");
            }
            //AcademicYearID: require, FK(AcademicYear)
            Utils.ValidateRequire(Pupil.AcademicYearID, "Common_Validate_Require");
            bool AcademicYearIDIsFK = AcademicYearRepository.All.Where(p => p.AcademicYearID == Pupil.AcademicYearID).Select(p => p.AcademicYearID).FirstOrDefault() > 0;
            if (!AcademicYearIDIsFK)
            {
                throw new BusinessException("PupilOfClass_Validate_IsNotAcademicYearFK");
            }
            //Year: require.
            Utils.ValidateRequire(Pupil.Year, "Common_Validate_Require");
            //Status : require, in(1,2,3,4,5)
            Utils.ValidateRequire(Pupil.Status, "Common_Validate_Require");
            Utils.ValidateRange(Pupil.Status, MIN_RANGE_STATUS, MAX_RANGE_STATUS, "PupilOfClass_Validate_Range");
            //Description: maxlength(200)
            Utils.ValidateMaxLength(Pupil.Description, MAX_LENGTH_DESCRIPTION, "Common_Validate_MaxLength");



            //SchoolID, AcademicYearID: not compatible(AcademicYear)
            bool AcademicYearCompatible = AcademicYearRepository.ExistsRow(GlobalConstants.SCHOOL_SCHEMA, "AcademicYear",
                  new Dictionary<string, object>()
                {
                    {"SchoolID",Pupil.SchoolID},
                    {"AcademicYearID",Pupil.AcademicYearID}
                }, null);
            if (!AcademicYearCompatible)
            {
                throw new BusinessException("Common_Validate_NotCompatible");
            }
            //ClassID, AcademicYearID: not compatible(ClassProfile)
            bool ClassProfileCompatible = ClassProfileRepository.ExistsRow(GlobalConstants.SCHOOL_SCHEMA, "ClassProfile",
                 new Dictionary<string, object>()
                {
                    {"ClassProfileID",Pupil.ClassID},
                    {"AcademicYearID",Pupil.AcademicYearID}
                }, null);
            if (!ClassProfileCompatible)
            {
                throw new BusinessException("Common_Validate_NotCompatible");
            }
            //If(UtilsBusiness.HasHeadTeacherPermission(UserID, entity.ClassID) = false),không thực hiện việc thêm mới
            if (UtilsBusiness.HasHeadTeacherPermission(UserID, Pupil.ClassID))
            {
                //Khi thêm mới học sinh vào lớp sẽ không lấy max OrderInClass nữa mà để mặc định là 0 hoặc null
                //IQueryable<PupilOfClass> lsPupilOfClass = SearchBySchool(Pupil.SchoolID, new Dictionary<string, object>() { { "ClassID", Pupil.ClassID } });
                //int? q = (from t in lsPupilOfClass
                //          select (t.OrderInClass)).Max();
                //int maxOrderInClass = 0;
                //if (q.HasValue)
                //{
                //    maxOrderInClass = q.Value;
                //}

                ////Gán entity.OrderInClass = m + 1,Thực hiện insert vào bảng PupilOfClass
                //Pupil.OrderInClass = (int)(maxOrderInClass + 1);
                Pupil.OrderInClass = Pupil.OrderInClass.HasValue && Pupil.OrderInClass.Value > 0 ? Pupil.OrderInClass : 0;
                base.Insert(Pupil);

                //Cập nhật vào bảng PupilProfile p với p.CurrentClassID = ClassID,
                PupilProfile updatePupilProfile = PupilProfileBusiness.Find(Pupil.PupilID);
                if (updatePupilProfile != null)
                {
                    updatePupilProfile.CurrentClassID = Pupil.ClassID;
                    updatePupilProfile.CurrentAcademicYearID = Pupil.AcademicYearID;
                    PupilProfileRepository.Update(updatePupilProfile);
                }
                //Tìm kiếm trong bảng PupilOfSchool theo PupilID, SchoolID, AcademicYearID.
                //Nếu không tìm thấy thì thực hiện insert vào bảng PupilOfSchool theo các giá trị trên 
                //và bổ sung EnrolmentType = 3(Hình thức khác). EnrolmentDate = PupilProfile.EnrolmentDate

                IQueryable<PupilOfSchool> lsPupilOfSchool = PupilOfSchoolRepository.All.Where(o => (o.PupilID == Pupil.PupilID))
                    .Where(o => (o.SchoolID == Pupil.SchoolID));

                if (lsPupilOfSchool.Count() == 0)
                {
                    PupilOfSchool newPupilOfSchool = new PupilOfSchool();
                    newPupilOfSchool.PupilID = Pupil.PupilID;
                    newPupilOfSchool.SchoolID = Pupil.SchoolID;
                    newPupilOfSchool.EnrolmentType = SystemParamsInFile.ENROLMENT_TYPE_OTHER;
                    if (updatePupilProfile != null)
                    {
                        newPupilOfSchool.EnrolmentDate = updatePupilProfile.EnrolmentDate;
                    }
                    PupilOfSchoolRepository.Insert(newPupilOfSchool);
                }
            }
        }
        #endregion

        #region update
        /// <summary>
        /// Sửa thông tin phân bổ học sinh vào lớp
        /// <author>dungnt</author>
        /// <date>02/10/2012</date>
        /// <update-by>namdv3</update-by>
        /// <update-date>13/10/2012</update-date>
        /// </summary>
        /// <param name="UserID"></param>
        /// <param name="Pupil"></param>
        public void UpdatePupilOfClass(int UserID, PupilOfClass Pupil)
        {
            var PupilProfileRepository = new PupilProfileRepository(this.context);
            var ClassProfileRepository = new ClassProfileRepository(this.context);
            var AcademicYearRepository = new AcademicYearRepository(this.context);

            //PupilOfClassID: PK(PupilOfClass)
            Utils.ValidateRequire(Pupil.PupilOfClassID, "Common_Validate_Require");
            bool PupilOfClassIDIsPK = this.All.Where(p => p.PupilOfClassID == Pupil.PupilOfClassID).Select(p => p.PupilOfClassID).FirstOrDefault() > 0;
            if (!PupilOfClassIDIsPK)
            {
                throw new BusinessException("PupilOfClass_Validate_IsNotPK");
            }
            //PupilID: require, FK(PupilProfile)
            Utils.ValidateRequire(Pupil.PupilID, "");
            bool PupilIDIsFK = PupilProfileRepository.All.Where(p => p.PupilProfileID == Pupil.PupilProfile.PupilProfileID).Select(p => p.PupilProfileID).FirstOrDefault() > 0;
            if (!PupilIDIsFK)
            {
                throw new BusinessException("PupilOfClass_Validate_IsNotPupilProfileFK");
            }
            //ClassID: require, FK(ClassProfile)
            Utils.ValidateRequire(Pupil.ClassID, "Common_Validate_Require");
            bool ClassIDIsFK = ClassProfileRepository.All.Where(p => p.ClassProfileID == Pupil.ClassID && p.IsActive == true).Select(p => p.ClassProfileID).FirstOrDefault() > 0;
            if (!ClassIDIsFK)
            {
                throw new BusinessException("PupilOfClass_Validate_IsNotClassFK");
            }
            //SchoolID: require, FK(SchoolProfile)
            Utils.ValidateRequire(Pupil.SchoolID, "Common_Validate_Require");
            bool SchoolIDIsFK = new SchoolProfileRepository(this.context).All.Where(p => p.SchoolProfileID == Pupil.SchoolID).Select(p => p.SchoolProfileID).FirstOrDefault() > 0;
            if (!SchoolIDIsFK)
            {
                throw new BusinessException("PupilOfClass_Validate_IsNotSchoolFK");
            }
            //AcademicYearID: require, FK(AcademicYear)
            Utils.ValidateRequire(Pupil.AcademicYearID, "Common_Validate_Require");
            bool AcademicYearIDIsFK = AcademicYearRepository.All.Where(p => p.AcademicYearID == Pupil.AcademicYearID).Select(p => p.AcademicYearID).FirstOrDefault() > 0;
            if (!AcademicYearIDIsFK)
            {
                throw new BusinessException("PupilOfClass_Validate_IsNotAcademicYearFK");
            }
            //Year: require.
            //Utils.ValidateRequire(Pupil.Year, "Common_Validate_Require");
            //Status : require, in(1,2,3,4,5)
            Utils.ValidateRequire(Pupil.Status, "Common_Validate_Require");
            Utils.ValidateRange(Pupil.Status, MIN_RANGE_STATUS, MAX_RANGE_STATUS, "PupilOfClass_Validate_Range");
            //Description: maxlength(200)
            Utils.ValidateMaxLength(Pupil.Description, MAX_LENGTH_DESCRIPTION, "Common_Validate_MaxLength");

            ////PupilID, ClassID: not compatible(PupilProfile)
            //bool PupilProfileCompatible = PupilProfileRepository.ExistsRow(GlobalConstants.PUPIL_SCHEMA, "PupilProfile",
            //        new Dictionary<string, object>()
            //    {
            //        {"PupilProfileID",Pupil.PupilID},
            //        {"CurrentClassID",Pupil.ClassID}
            //    }, null);
            //if (!PupilProfileCompatible)
            //{
            //    throw new BusinessException("Common_Validate_NotCompatible");
            //}

            //SchoolID, AcademicYearID: not compatible(AcademicYear)
            bool AcademicYearCompatible = AcademicYearRepository.ExistsRow(GlobalConstants.SCHOOL_SCHEMA, "AcademicYear",
                  new Dictionary<string, object>()
                {
                    {"SchoolID",Pupil.SchoolID},
                    {"AcademicYearID",Pupil.AcademicYearID}
                }, null);
            if (!AcademicYearCompatible)
            {
                throw new BusinessException("Common_Validate_NotCompatible");
            }
            //ClassID, AcademicYearID: not compatible(ClassProfile)
            bool ClassProfileCompatible = ClassProfileRepository.ExistsRow(GlobalConstants.SCHOOL_SCHEMA, "ClassProfile",
                 new Dictionary<string, object>()
                {
                    {"ClassProfileID",Pupil.ClassID},
                    {"AcademicYearID",Pupil.AcademicYearID}
                }, null);
            if (!ClassProfileCompatible)
            {
                throw new BusinessException("Common_Validate_NotCompatible");
            }

            //If(UtilsBusiness.HasHeadTeacherPermission(UserID, entity.ClassID) = false) không thực hiện việc cập nhật
            if (UtilsBusiness.HasHeadTeacherPermission(UserID, Pupil.ClassID))
            {
                //Thực hiện Cập nhật vào bảng PupilOfClass
                base.Update(Pupil);

                PupilProfile updatePupilProfile = PupilProfileBusiness.Find(Pupil.PupilID);
                if (updatePupilProfile != null)
                {
                    updatePupilProfile.CurrentClassID = Pupil.ClassID;
                    PupilProfileRepository.Update(updatePupilProfile);
                }
            }
        }

        /// <summary>
        /// Anhnph Update - 16/01/2015 - cập nhật HS co dang ky HD
        /// </summary>
        /// <param name="PupilOfClass"></param>
        public void UpdateRegisterPupilOfClass(List<PupilOfClass> lstTempUpdate)
        {
            foreach (PupilOfClass item in lstTempUpdate)
            {
                //Thực hiện Cập nhật vào bảng PupilOfClass
                base.Update(item);
            }
            base.Save();
        }
        #endregion

        #region Delete
        /// <summary>
        /// Xóa thông tin phân bổ học sinh vào lớp
        /// </summary>
        /// <author>dungnt</author>
        /// <date>02/10/2012</date>
        /// <param name="PupilOfClassID">id cần xóa</param>
        public void Delete(int UserID, int PupilOfClassID, int SchoolID)
        {
            //PupilOfClassID: PK(PupilOfClass)
            Utils.ValidateRequire(PupilOfClassID, "Common_Validate_Require");
            bool PupilOfClassIDIsPK = this.All.Where(p => p.PupilOfClassID == PupilOfClassID).Select(p => p.PupilOfClassID).FirstOrDefault() > 0;
            if (!PupilOfClassIDIsPK)
            {
                throw new BusinessException("PupilOfClass_Validate_IsNotPK");
            }
            //PupilID, SchoolID: not compatible(PupilProfile)

            IQueryable<PupilOfClass> PupilOfClass = PupilOfClassRepository.All.Where(o => (o.PupilOfClassID == PupilOfClassID));
            PupilOfClass lsPupilOfClass = PupilOfClass.FirstOrDefault();
            bool PupilProfileCompatible = new PupilProfileRepository(this.context).ExistsRow(GlobalConstants.PUPIL_SCHEMA, "PupilProfile",
                    new Dictionary<string, object>()
                {
                    {"PupilProfileID",lsPupilOfClass.PupilID},
                    {"CurrentSchoolID",SchoolID}
                }, null);
            if (!PupilProfileCompatible)
            {
                throw new BusinessException("Common_Validate_NotCompatible");
            }

            // AcademicYearBusiness.IsCurrentYear(AcademicYearID) = FALSE => Năm học không phải năm hiện tại

            if (!AcademicYearBusiness.IsCurrentYear(lsPupilOfClass.AcademicYearID))
            {
                throw new BusinessException("Common_Validate_NotIsCurrentYear");
            }

            if (UtilsBusiness.HasHeadTeacherPermission(UserID, PupilOfClassID))
            {
                base.Delete(PupilOfClassID);
            }

        }


        #endregion

        #region DeletePupilOfClass
        /// <summary>
        /// Xóa thông tin phân bổ học sinh vào lớp
        /// </summary>
        /// <author>dungnt</author>
        /// <date>02/10/2012</date>
        /// <param name="lsPupilOfClass">danh sách phân bổ cần xóa</param>
        public void DeletePupilOfClass(List<PupilOfClass> lsPupilOfClass)
        {
            foreach (PupilOfClass item in lsPupilOfClass)
            {
                this.Delete(item.PupilOfClassID);

            }
        }
        #endregion

        #region Search
        /// <summary>
        /// Tìm kiếm thông tin phân bổ học sinh vào lớp
        /// 12/11/2012 - PhuongH sửa lại tìm kiếm theo Status, thêm AppliedLevel
        /// <author>dungnt</author>
        /// <date>02/10/2012</date>
        /// </summary>
        /// <param name="dic"></param>
        /// <returns></returns>
        public IQueryable<PupilOfClass> Search(IDictionary<string, object> SearchPupilOfClass)
        {
            IQueryable<PupilOfClass> lsPupilOfClass = this.PupilOfClassRepository.All;
            string check = Utils.GetString(SearchPupilOfClass, "Check");
            string checkWithClass = Utils.GetString(SearchPupilOfClass, "CheckWithClass");
            int PupilOfClassID = Utils.GetInt(SearchPupilOfClass, "PupilOfClassID");
            int PupilID = Utils.GetInt(SearchPupilOfClass, "PupilID");
            int EducationLevelID = Utils.GetInt(SearchPupilOfClass, "EducationLevelID");
            int ClassID = Utils.GetInt(SearchPupilOfClass, "ClassID");
            int SchoolID = Utils.GetInt(SearchPupilOfClass, "SchoolID");
            int AcademicYearID = Utils.GetInt(SearchPupilOfClass, "AcademicYearID");
            int Year = Utils.GetInt(SearchPupilOfClass, "Year");
            List<int> Status = Utils.GetIntListSpecial(SearchPupilOfClass, "Status");
            string PupilCode = Utils.GetString(SearchPupilOfClass, "PupilCode");
            string FullName = Utils.GetString(SearchPupilOfClass, "FullName");
            DateTime? AssignedDate = Utils.GetDateTime(SearchPupilOfClass, "AssignedDate");
            int AppliedLevel = Utils.GetInt(SearchPupilOfClass, "AppliedLevel");
            int EatingGroupID = Utils.GetInt(SearchPupilOfClass, "EatingGroupID");
            int EvaluationConfigID = Utils.GetInt(SearchPupilOfClass, "EvaluationConfigID");
            int OrderInClass = Utils.GetInt(SearchPupilOfClass, "OrderInClass");
            string Description = Utils.GetString(SearchPupilOfClass, "Description");
            List<int> ListClassID = Utils.GetIntList(SearchPupilOfClass, "ListClassID");
            List<int> lstPupilID = Utils.GetIntList(SearchPupilOfClass, "lstPupilID");
            int EthnicID = Utils.GetInt(SearchPupilOfClass, "EthnicID");
            int Female = Utils.GetInt(SearchPupilOfClass, "FemaleID");
            int Genre = Utils.GetInt(SearchPupilOfClass, "Genre", -1);
            List<int> lstClassID = Utils.GetIntList(SearchPupilOfClass, "lstClassID");
            lsPupilOfClass = lsPupilOfClass.Where(em => em.PupilProfile.IsActive == true);
            lsPupilOfClass = lsPupilOfClass.Where(em => em.ClassProfile.SchoolProfile.IsActive == true);
            Ethnic Ethnic_Kinh = EthnicBusiness.All.Where(o => o.EthnicCode.ToLower().Equals(SystemParamsInFile.ETHNIC_CODE_KINH.ToLower()) && o.IsActive == true).FirstOrDefault();
            Ethnic Ethnic_ForeignPeople = EthnicBusiness.All.Where(o => o.EthnicCode.ToLower().Equals(SystemParamsInFile.ETHNIC_CODE_NN.ToLower()) && o.IsActive == true).FirstOrDefault();

            if (ClassID != 0)
            {
                lsPupilOfClass = (from poc in lsPupilOfClass
                                  join cp in ClassProfileBusiness.All on poc.ClassID equals cp.ClassProfileID
                                  where (!cp.IsActive.HasValue || (cp.IsActive.HasValue && cp.IsActive.Value))
                                  && poc.ClassID == ClassID
                                  select poc);
            }
            if (EvaluationConfigID != 0)
            {
                lsPupilOfClass = lsPupilOfClass.Where(em => (em.PupilProfile.EvaluationConfigID == EvaluationConfigID));
            }


            if (Status.Count != 0)
            {
                lsPupilOfClass = lsPupilOfClass.Where(em => Status.Contains(em.Status));
            }

            if (PupilOfClassID != 0)
            {
                lsPupilOfClass = lsPupilOfClass.Where(em => (em.PupilOfClassID == PupilOfClassID));
            }
            if (PupilID != 0)
            {
                lsPupilOfClass = lsPupilOfClass.Where(em => (em.PupilID == PupilID));
            }
            if (EducationLevelID != 0)
            {
                lsPupilOfClass = lsPupilOfClass.Where(em => (em.ClassProfile.EducationLevelID == EducationLevelID));
            }
            if (SchoolID != 0)
            {
                //bo sung where theo partitionId

                lsPupilOfClass = lsPupilOfClass.Where(em => (em.SchoolID == SchoolID));
            }
            if (AcademicYearID != 0)
            {
                lsPupilOfClass = lsPupilOfClass.Where(em => (em.AcademicYearID == AcademicYearID));
            }
            if (lstClassID.Count > 0)
            {
                lsPupilOfClass = lsPupilOfClass.Where(em => lstClassID.Contains(em.ClassID));
            }
            if (Year != 0)
            {
                lsPupilOfClass = lsPupilOfClass.Where(em => (em.Year == Year));
            }
            if (OrderInClass != 0)
            {
                lsPupilOfClass = lsPupilOfClass.Where(em => (em.OrderInClass == OrderInClass));
            }

            if (Description.Length != 0)
            {
                lsPupilOfClass = lsPupilOfClass.Where(em => (em.Description.ToUpper().Contains(Description.ToUpper())));
            }

            if (AssignedDate != null)
            {
                lsPupilOfClass = lsPupilOfClass.Where(em => (em.AssignedDate == AssignedDate));
            }
            if (PupilCode.Length != 0)
            {
                lsPupilOfClass = lsPupilOfClass.Where(em => em.PupilProfile.PupilCode.ToUpper().Contains(PupilCode.ToUpper()));
            }
            if (FullName.Length != 0)
            {
                lsPupilOfClass = lsPupilOfClass.Where(em => em.PupilProfile.FullName.ToUpper().Contains(FullName.ToUpper()));
            }
            if ((ListClassID != null) && (ListClassID.Count() > 0))
            {
                lsPupilOfClass = lsPupilOfClass.Where(em => ListClassID.Contains(em.ClassID));
            }
            if (AppliedLevel != 0)
            {
                lsPupilOfClass = lsPupilOfClass.Where(em => (em.ClassProfile.EducationLevel.Grade == AppliedLevel));
            }
            if (EatingGroupID != 0)
            {
                List<EatingGroupClass> LstEatingGroupClass = EatingGroupClassBusiness.All.Where(o => o.EatingGroupID == EatingGroupID).ToList();
                List<int> lstCf = new List<int>();
                foreach (var item in LstEatingGroupClass)
                {
                    lstCf.Add(item.ClassID);
                }
                lsPupilOfClass = lsPupilOfClass.Where(em => (lstCf.Contains(em.ClassID)));
            }
            if (Genre != -1)
            {
                lsPupilOfClass = lsPupilOfClass.Where(pp => pp.PupilProfile.Genre == Genre);
            }

            if (EthnicID > 0)
            {
                lsPupilOfClass = lsPupilOfClass.Where(pp => pp.PupilProfile.EthnicID.HasValue && pp.PupilProfile.EthnicID != Ethnic_Kinh.EthnicID && pp.PupilProfile.EthnicID != Ethnic_ForeignPeople.EthnicID);
            }

            if (Female > 0)
            {
                lsPupilOfClass = lsPupilOfClass.Where(pp => pp.PupilProfile.Genre == GlobalConstants.GENRE_FEMALE);
            }
            if (lstPupilID.Count > 0)
            {
                lsPupilOfClass = lsPupilOfClass.Where(p => lstPupilID.Contains(p.PupilID));
            }
            //comment sua 1/15/2013 namta+phuongtd
            if (check.Length != 0)
            {
                var lsPupilOfClass2 = lsPupilOfClass.Where(o => o.AssignedDate == lsPupilOfClass.Where(u => u.PupilID == o.PupilID).Select(u => u.AssignedDate).Max());
                return lsPupilOfClass2;
            }

            //hieund9
            if (checkWithClass.Length != 0)
            {
                var lsPupilOfClass2 = lsPupilOfClass.Where(o => o.AssignedDate == lsPupilOfClass.Where(u => u.PupilID == o.PupilID && u.ClassID == o.ClassID).Select(u => u.AssignedDate).Max());
                return lsPupilOfClass2;
            }

            return lsPupilOfClass;
        }

        #endregion

        #region Search
        /// <summary>
        /// Tìm kiếm thông tin phân bổ học sinh vào lớp
        /// 12/11/2012 - PhuongH sửa lại tìm kiếm theo Status, thêm AppliedLevel
        /// <author>dungnt</author>
        /// <date>02/10/2012</date>
        /// </summary>
        /// <param name="dic"></param>
        /// <returns></returns>
        public IQueryable<PupilOfClass> SearchLeftJoin(IDictionary<string, object> SearchPupilOfClass)
        {
            IQueryable<PupilOfClass> lsPupilOfClass = this.PupilOfClassRepository.All;
            string check = Utils.GetString(SearchPupilOfClass, "Check");
            string checkWithClass = Utils.GetString(SearchPupilOfClass, "CheckWithClass");
            int PupilOfClassID = Utils.GetInt(SearchPupilOfClass, "PupilOfClassID");
            int PupilID = Utils.GetInt(SearchPupilOfClass, "PupilID");
            int EducationLevelID = Utils.GetInt(SearchPupilOfClass, "EducationLevelID");
            int ClassID = Utils.GetInt(SearchPupilOfClass, "ClassID");
            int SchoolID = Utils.GetInt(SearchPupilOfClass, "SchoolID");
            int AcademicYearID = Utils.GetInt(SearchPupilOfClass, "AcademicYearID");
            int Year = Utils.GetInt(SearchPupilOfClass, "Year");
            List<int> Status = Utils.GetIntListSpecial(SearchPupilOfClass, "Status");
            string PupilCode = Utils.GetString(SearchPupilOfClass, "PupilCode");
            string FullName = Utils.GetString(SearchPupilOfClass, "FullName");
            DateTime? AssignedDate = Utils.GetDateTime(SearchPupilOfClass, "AssignedDate");
            int AppliedLevel = Utils.GetInt(SearchPupilOfClass, "AppliedLevel");
            int EatingGroupID = Utils.GetInt(SearchPupilOfClass, "EatingGroupID");
            int EvaluationConfigID = Utils.GetInt(SearchPupilOfClass, "EvaluationConfigID");
            int OrderInClass = Utils.GetInt(SearchPupilOfClass, "OrderInClass");
            string Description = Utils.GetString(SearchPupilOfClass, "Description");
            List<int> ListClassID = Utils.GetIntList(SearchPupilOfClass, "ListClassID");
            List<int> lstPupilID = Utils.GetIntList(SearchPupilOfClass, "lstPupilID");
            int EthnicID = Utils.GetInt(SearchPupilOfClass, "EthnicID");
            int Female = Utils.GetInt(SearchPupilOfClass, "FemaleID");
            int Genre = Utils.GetInt(SearchPupilOfClass, "Genre", -1);
            List<int> lstClassID = Utils.GetIntList(SearchPupilOfClass, "lstClassID");
            lsPupilOfClass = lsPupilOfClass.Where(em => em.PupilProfile.IsActive == true);
            lsPupilOfClass = lsPupilOfClass.Where(em => em.ClassProfile.SchoolProfile.IsActive == true);
            Ethnic Ethnic_Kinh = EthnicBusiness.All.Where(o => o.EthnicCode.ToLower().Equals(SystemParamsInFile.ETHNIC_CODE_KINH.ToLower()) && o.IsActive == true).FirstOrDefault();
            Ethnic Ethnic_ForeignPeople = EthnicBusiness.All.Where(o => o.EthnicCode.ToLower().Equals(SystemParamsInFile.ETHNIC_CODE_NN.ToLower()) && o.IsActive == true).FirstOrDefault();

            //var results = from data in ClassProfileBusiness.All
            //              join growth in userGrowth
            //              on data.User equals growth.User into joined
            //              from j in joined.DefaultIfEmpty()
            //              select new
            //              {
            //                  UserData = data,
            //                  UserGrowth = j
            //              };

            if (ClassID != 0)
            {
                lsPupilOfClass = (from poc in lsPupilOfClass
                                  join cp in ClassProfileBusiness.All on poc.ClassID equals cp.ClassProfileID
                                  where (!cp.IsActive.HasValue || (cp.IsActive.HasValue && cp.IsActive.Value))
                                  && poc.ClassID == ClassID
                                  select poc);
            }
            if (EvaluationConfigID != 0)
            {
                lsPupilOfClass = lsPupilOfClass.Where(em => (em.PupilProfile.EvaluationConfigID == EvaluationConfigID));
            }


            if (Status.Count != 0)
            {
                lsPupilOfClass = lsPupilOfClass.Where(em => Status.Contains(em.Status));
            }

            if (PupilOfClassID != 0)
            {
                lsPupilOfClass = lsPupilOfClass.Where(em => (em.PupilOfClassID == PupilOfClassID));
            }
            if (PupilID != 0)
            {
                lsPupilOfClass = lsPupilOfClass.Where(em => (em.PupilID == PupilID));
            }
            if (EducationLevelID != 0)
            {
                lsPupilOfClass = lsPupilOfClass.Where(em => (em.ClassProfile.EducationLevelID == EducationLevelID));
            }
            if (SchoolID != 0)
            {
                //bo sung where theo partitionId

                lsPupilOfClass = lsPupilOfClass.Where(em => (em.SchoolID == SchoolID));
            }
            if (AcademicYearID != 0)
            {
                lsPupilOfClass = lsPupilOfClass.Where(em => (em.AcademicYearID == AcademicYearID));
            }
            if (lstClassID.Count > 0)
            {
                lsPupilOfClass = lsPupilOfClass.Where(em => lstClassID.Contains(em.ClassID));
            }
            if (Year != 0)
            {
                lsPupilOfClass = lsPupilOfClass.Where(em => (em.Year == Year));
            }
            if (OrderInClass != 0)
            {
                lsPupilOfClass = lsPupilOfClass.Where(em => (em.OrderInClass == OrderInClass));
            }

            if (Description.Length != 0)
            {
                lsPupilOfClass = lsPupilOfClass.Where(em => (em.Description.ToUpper().Contains(Description.ToUpper())));
            }

            if (AssignedDate != null)
            {
                lsPupilOfClass = lsPupilOfClass.Where(em => (em.AssignedDate == AssignedDate));
            }
            if (PupilCode.Length != 0)
            {
                lsPupilOfClass = lsPupilOfClass.Where(em => em.PupilProfile.PupilCode.ToUpper().Contains(PupilCode.ToUpper()));
            }
            if (FullName.Length != 0)
            {
                lsPupilOfClass = lsPupilOfClass.Where(em => em.PupilProfile.FullName.ToUpper().Contains(FullName.ToUpper()));
            }
            if ((ListClassID != null) && (ListClassID.Count() > 0))
            {
                lsPupilOfClass = lsPupilOfClass.Where(em => ListClassID.Contains(em.ClassID));
            }
            if (AppliedLevel != 0)
            {
                lsPupilOfClass = lsPupilOfClass.Where(em => (em.ClassProfile.EducationLevel.Grade == AppliedLevel));
            }
            if (EatingGroupID != 0)
            {
                List<EatingGroupClass> LstEatingGroupClass = EatingGroupClassBusiness.All.Where(o => o.EatingGroupID == EatingGroupID).ToList();
                List<int> lstCf = new List<int>();
                foreach (var item in LstEatingGroupClass)
                {
                    lstCf.Add(item.ClassID);
                }
                lsPupilOfClass = lsPupilOfClass.Where(em => (lstCf.Contains(em.ClassID)));
            }
            if (Genre != -1)
            {
                lsPupilOfClass = lsPupilOfClass.Where(pp => pp.PupilProfile.Genre == Genre);
            }

            if (EthnicID > 0)
            {
                lsPupilOfClass = lsPupilOfClass.Where(pp => pp.PupilProfile.EthnicID.HasValue && pp.PupilProfile.EthnicID != Ethnic_Kinh.EthnicID && pp.PupilProfile.EthnicID != Ethnic_ForeignPeople.EthnicID);
            }

            if (Female > 0)
            {
                lsPupilOfClass = lsPupilOfClass.Where(pp => pp.PupilProfile.Genre == GlobalConstants.GENRE_FEMALE);
            }
            if (lstPupilID.Count > 0)
            {
                lsPupilOfClass = lsPupilOfClass.Where(p => lstPupilID.Contains(p.PupilID));
            }
            //comment sua 1/15/2013 namta+phuongtd
            if (check.Length != 0)
            {
                var lsPupilOfClass2 = lsPupilOfClass.Where(o => o.AssignedDate == lsPupilOfClass.Where(u => u.PupilID == o.PupilID).Select(u => u.AssignedDate).Max());
                return lsPupilOfClass2;
            }

            //hieund9
            if (checkWithClass.Length != 0)
            {
                var lsPupilOfClass2 = lsPupilOfClass.Where(o => o.AssignedDate == lsPupilOfClass.Where(u => u.PupilID == o.PupilID && u.ClassID == o.ClassID).Select(u => u.AssignedDate).Max());
                return lsPupilOfClass2;
            }

            return lsPupilOfClass;
        }

        #endregion

        #region SearchBySchool
        /// <summary>
        /// Tìm kiếm học sinh theo trường
        /// </summary>
        /// <param name="SchoolID">id trường</param>
        /// <param name="dic">bộ tham số</param>
        /// <returns>danh sách thỏa mãn</returns>
        public IQueryable<PupilOfClass> SearchBySchool(int SchoolID, IDictionary<string, object> dic)
        {
            if (SchoolID == 0)
            {
                return null;
            }
            else
            {
                dic["SchoolID"] = SchoolID;
                return Search(dic);
            }
        }

        // <author>phuongh1</author>
        /// <date>08/08/2013</date>
        public IQueryable<PupilOfClass> SearchBySchool(int SchoolID, PupilOfClassSearchForm SearchForm = null)
        {
            if (SchoolID == 0)
            {
                return null;
            }
            else
            {
                if (SearchForm == null) SearchForm = new PupilOfClassSearchForm();
                SearchForm.SchoolID = SchoolID;

                return SearchForm.Search(PupilOfClassRepository.All.Where(o => o.PupilProfile.IsActive == true));
            }
        }
        #endregion

        #region class forwarding
        /// <summary>
        /// Chuyển lớp cho học sinh. Chức năng điều chỉnh kết chuyển
        /// <author>dungnt</author>
        /// <date>05/09/2012</date>
        /// </summary>
        /// <param name="lsPupilMove">Danh sách học sinh cần chuyển lớp</param>
        /// <param name="SchoolID">id trường</param>
        /// <param name="newClassID">Lớp mới</param>
        /// <param name="maxOrderInNewClass">Số thứ tự lớn nhất tại lớp mới</param>
        public void ClassForwarding(int UserID, List<PupilOfClass> lsPupilMove, int SchoolID, int newClassID, int maxOrderInNewClass)
        {
            //lstPupilMove chứa học sinh không thuộc quản lý của trường có SchoolID tương ứng
            if (lsPupilMove != null && lsPupilMove.Count > 0)
            {
                int OldClassID = lsPupilMove[0].ClassID;
                int AcademicYearID = lsPupilMove[0].AcademicYearID;
                AcademicYear academicYear = AcademicYearBusiness.Find(AcademicYearID);
                Dictionary<string, object> dic = new Dictionary<string, object>();
                dic["ClassID"] = OldClassID;
                dic["AcademicYearID"] = AcademicYearID;
                dic["Year"] = academicYear.Year;
                List<int> lstPupilID = lsPupilMove.Select(o => o.PupilID).Distinct().ToList();
                var hasMr = MarkRecordBusiness.SearchBySchool(SchoolID, dic)
                                .Where(o => lstPupilID.Contains(o.PupilID)).Count() > 0;
                var hasJr = JudgeRecordBusiness.SearchBySchool(SchoolID, dic)
                                    .Where(o => lstPupilID.Contains(o.PupilID)).Count() > 0;
                var hasPr = PupilRankingBusiness.SearchBySchool(SchoolID, dic)
                                    .Where(o => lstPupilID.Contains(o.PupilID)).Count() > 0;
                if (hasMr || hasJr || hasPr)
                {
                    throw new BusinessException("TransferData_Label_PupilHasMark");
                }
                List<PupilProfile> lstPupilProfile = PupilProfileBusiness.All.Where(o => o.IsActive == true && lstPupilID.Contains(o.PupilProfileID)).ToList();
                if (lstPupilProfile.Any(o => o.CurrentSchoolID != SchoolID))
                {
                    throw new BusinessException("PupilOfClass_Validate_PupilIsNotManagedBySchool");
                }



                foreach (PupilOfClass Pupil in lsPupilMove)
                {
                    //Cập nhật ClassID cho tất cả bản ghi trong lstPupilMove với lớp mới là newClassID.
                    Pupil.ClassID = newClassID;
                    //Cập nhật AssignedDate = GetDate() cho tất cả bản ghi trong lstPupilMove 
                    Pupil.AssignedDate = DateTime.Now;
                    Pupil.OrderInClass = maxOrderInNewClass;
                    base.Update(Pupil);
                    maxOrderInNewClass++;
                }
                foreach (PupilProfile pp in lstPupilProfile)
                {
                    pp.CurrentClassID = newClassID;
                    PupilProfileBusiness.Update(pp);
                }
            }
        }
        #endregion

        public void UpdateOrder(int UserID, int ClassID, List<int> lsPupilID, List<int> lsOrderInClass)
        {
            if (UtilsBusiness.HasHeadTeacherPermission(UserID, ClassID))
            {
                //kiem tra trung lap du lieu trong listOrder,Common_Label_OrderDuplicateError
                if (lsPupilID.Count != lsOrderInClass.Count)
                    throw new BusinessException("Common_Label_OrderDuplicateError");

                List<PupilOfClass> lstPOC = Search(new Dictionary<string, object>() { { "ClassID", ClassID }, { "Check", "Check" } }).ToList();

                for (int i = 0; i < lsPupilID.Count; i++)
                {
                    PupilOfClass poc = lstPOC.FirstOrDefault(u => u.PupilID == lsPupilID[i]);
                    poc.OrderInClass = lsOrderInClass[i];
                    base.Update(poc);
                }
            }
            else
            {
                throw new BusinessException("Common_Label_HasHeadTeacherPermissionError");
            }
        }

        public bool CheckValidatePupilCode(string PupilCode, int ClassID)
        {
            if (PupilOfClassBusiness.All.Where(o => o.ClassID == ClassID && o.PupilProfile.PupilCode == PupilCode).Count() > 0)
            {
                return true;
            }
            else
                return false;
        }
        public bool CheckValidateName(string Name, int ClassID)
        {
            if (PupilOfClassBusiness.All.Where(o => o.ClassID == ClassID && o.PupilProfile.FullName == Name).Count() > 0)
            {
                return true;
            }
            else
                return false;
        }
        public PupilOfClass CheckValidatePupil(string name, string pupilCode, int ClassID)
        {
            return PupilOfClassBusiness.All.Where(o => o.ClassID == ClassID && o.PupilProfile.PupilCode == pupilCode && o.PupilProfile.IsActive == true).OrderByDescending(o => o.AssignedDate).FirstOrDefault();
        }

        /// <summary>
        /// QuangLM
        /// Ham lay tat ca hoc sinh thuoc lop hoc (khoi hoc) hoc trong ky 1 cua nam hoc
        /// </summary>
        /// <param name="AcademicYearID"></param>
        /// <param name="SchoolID"></param>
        /// <param name="EducationLevelID"></param>
        /// <param name="ClassID"></param>
        /// <returns></returns>
        public List<PupilOfClassBO> GetPupilOfClassInSemester(int AcademicYearID, int SchoolID, int? EducationLevelID, int semester, int? ClassID = null, int? AppliedLevel = null)
        {
            List<PupilOfClassBO> listPupilOfClass = new List<PupilOfClassBO>();
            AcademicYear aca = AcademicYearBusiness.Find(AcademicYearID);
            DateTime endDate;
            if (semester == SystemParamsInFile.SEMESTER_OF_YEAR_FIRST)
            {
                endDate = aca.FirstSemesterEndDate.Value;
            }
            else
            {
                endDate = aca.SecondSemesterEndDate.Value;
            }
            // Lay tat ca danh sach tat ca hoc sinh dang hoc hoac da tot nghiep
            // hoc trong ky
            var listPupilStudyingAndGraduated
                = PupilOfClassBusiness.SearchBySchool(SchoolID,
                                    new Dictionary<string, object> {
                                        { "AcademicYearID", AcademicYearID },
                                        { "AppliedLevel", AppliedLevel },
                                        { "EducationLevelID", EducationLevelID.HasValue ? EducationLevelID.Value : 0 },
                                        { "ClassID", ClassID.HasValue ? ClassID.Value : 0 },
                                        { "Check", "Check" }
                                    }).Where(u => (!ClassID.HasValue || u.ClassID == ClassID.Value)
                                                && (u.Status == SystemParamsInFile.PUPIL_STATUS_STUDYING
                                                || u.Status == SystemParamsInFile.PUPIL_STATUS_GRADUATED)
                                                && u.AssignedDate <= endDate)
                                                .Select(o => new { o.ClassProfile.EducationLevelID, o.ClassID, o.PupilID })
                                                .ToList();
            // Danh sach hoc sinh thoi hoc truoc ngay so sanh nam hoc
            var listPupilOff = PupilLeavingOffBusiness.All.Where(o =>
                o.PupilProfile.IsActive
                && o.AcademicYearID == AcademicYearID
                && o.SchoolID == SchoolID
                && (!AppliedLevel.HasValue || AppliedLevel.Value == 0 || o.EducationLevel.Grade == AppliedLevel)
                && (!EducationLevelID.HasValue || EducationLevelID.Value == 0 || o.EducationLevelID == EducationLevelID)
                && (!ClassID.HasValue || ClassID.Value == 0 || o.ClassID == ClassID)
                && o.LeavingDate >= endDate
                ).Select(o => new { o.ClassProfile.EducationLevelID, o.ClassID, o.PupilID }).ToList();
            // Danh sach hoc sinh chuyen truong truoc ngay so sanh nam hoc
            var listPupiMoveSchool = SchoolMovementBusiness.All.Where(o =>
                o.PupilProfile.IsActive
                && o.AcademicYearID == AcademicYearID
                && o.SchoolID == SchoolID
                && (!AppliedLevel.HasValue || AppliedLevel.Value == 0 || o.EducationLevel.Grade == AppliedLevel)
                && (!EducationLevelID.HasValue || EducationLevelID.Value == 0 || o.EducationLevelID == EducationLevelID)
                && (!ClassID.HasValue || ClassID.Value == 0 || o.ClassID == ClassID)
                && o.MovedDate >= endDate
                ).Select(o => new { o.ClassProfile.EducationLevelID, o.ClassID, o.PupilID }).ToList();
            // Danh sach hoc sinh chuyen lop truoc ngay so sanh nam hoc  
            IQueryable<ClassMovement> iQClassMovement = ClassMovementBusiness.All.Where(o =>
                o.PupilProfile.IsActive
                && o.AcademicYearID == AcademicYearID
                && o.SchoolID == SchoolID
                && (!AppliedLevel.HasValue || AppliedLevel.Value == 0 || o.EducationLevel.Grade == AppliedLevel)
                && (!EducationLevelID.HasValue || EducationLevelID.Value == 0 || o.EducationLevelID == EducationLevelID)
                && (!ClassID.HasValue || ClassID.Value == 0 || o.FromClassID == ClassID)
                && o.MovedDate >= endDate
                );
            var listClassMovement = iQClassMovement.Where(o => o.MovedDate == (iQClassMovement.Where(u => u.PupilID == o.PupilID)).Select(u => u.MovedDate).Min())
            .Select(o => new { o.ClassProfile.EducationLevelID, ClassID = o.FromClassID, o.PupilID }).ToList();

            // Loai bo cac hoc sinh chuyen lop roi moi chuye truong, thoi hoc
            // Chuyen truong
            listPupiMoveSchool = listPupiMoveSchool.Where(u => !listClassMovement.Any(v => v.PupilID == u.PupilID && v.EducationLevelID == u.EducationLevelID)).ToList();

            // Thoi hoc
            listPupilOff = listPupilOff.Where(u => !listClassMovement.Any(v => v.PupilID == u.PupilID && v.EducationLevelID == u.EducationLevelID)).ToList();

            // Bo sung them cac hoc sinh thoi hoc, chuyen truong, chuyen lop ngay so sanh nam hoc
            listPupilOfClass = listPupilStudyingAndGraduated.Union(listPupilOff).Union(listPupiMoveSchool).Union(listClassMovement).Distinct()
                .Select(o => new PupilOfClassBO()
                {
                    ClassID = o.ClassID,
                    PupilID = o.PupilID,
                    EducationLevelID = o.EducationLevelID
                })
                .ToList();

            // Loai bo nhung truong hop 1 hoc sinh xuat hien 2 lan trong 1 khoi do chuyen lop trong cung 1 ngay
            var listGroupPupilEdu = listPupilOfClass.GroupBy(o => new { o.EducationLevelID, o.PupilID })
                .Select(o => new { o.Key.PupilID, o.Key.EducationLevelID, TotalPupil = o.Count() })
                .Where(o => o.TotalPupil > 1);
            if (listGroupPupilEdu != null && listGroupPupilEdu.Count() > 0)
            {
                Dictionary<string, DateTime> dicDate = new Dictionary<string, DateTime>();
                Dictionary<string, int> dicPos = new Dictionary<string, int>();
                for (int i = listPupilOfClass.Count - 1; i >= 0; i--)
                {
                    PupilOfClassBO item = listPupilOfClass[i];
                    if (listGroupPupilEdu.ToList().Any(o => o.PupilID == item.PupilID && o.EducationLevelID == item.EducationLevelID))
                    {
                        string key = item.PupilID + "_" + item.EducationLevelID;
                        DateTime? assignedDate = PupilOfClassBusiness.SearchBySchool(SchoolID,
                                    new Dictionary<string, object> {
                                        { "AcademicYearID", AcademicYearID },
                                        { "AppliedLevel", AppliedLevel },
                                        { "PupilID", item.PupilID },
                                        { "EducationLevelID", item.EducationLevelID },
                                        { "ClassID", item.ClassID },
                                        { "Check", "Check" }})
                                        .Where(o => o.AssignedDate <= endDate)
                                        .Select(o => o.AssignedDate).FirstOrDefault();
                        if (!assignedDate.HasValue)
                        {
                            listPupilOfClass.RemoveAt(i);
                        }
                        if (dicDate.ContainsKey(key))
                        {
                            if (assignedDate < dicDate[key])
                            {
                                dicDate[key] = assignedDate.Value;
                                dicPos[key] = i;
                            }
                        }
                        else
                        {
                            dicDate[key] = assignedDate.Value;
                            dicPos[key] = i;
                        }
                    }
                }
                // Remove khoi danh sach                
                List<int> listPos = new List<int>(dicPos.Values.OrderByDescending(o => o));
                foreach (int pos in listPos)
                {
                    listPupilOfClass.RemoveAt(pos);
                }
            }
            return listPupilOfClass;
        }

        /// <summary>
        ///Lấy danh sách học sinh theo lớp (theo khối) và không check assigndate so với endDate
        /// </summary>
        /// <param name="AcademicYearID"></param>
        /// <param name="SchoolID"></param>
        /// <param name="EducationLevelID"></param>
        /// <param name="semester"></param>
        /// <param name="ClassID"></param>
        /// <param name="AppliedLevel"></param>
        /// <returns></returns>
        public List<PupilOfClassBO> GetPupilInSemesterNoCheckAssignDate(int AcademicYearID, int SchoolID, int? EducationLevelID, int semester, int? ClassID = null, int? AppliedLevel = null)
        {
            List<PupilOfClassBO> listPupilOfClass = new List<PupilOfClassBO>();
            AcademicYear aca = AcademicYearBusiness.Find(AcademicYearID);
            DateTime endDate;
            if (semester == SystemParamsInFile.SEMESTER_OF_YEAR_FIRST)
            {
                endDate = aca.FirstSemesterEndDate.Value;
            }
            else
            {
                endDate = aca.SecondSemesterEndDate.Value;
            }
            // Lay tat ca danh sach tat ca hoc sinh dang hoc hoac da tot nghiep
            // hoc trong ky
            var listPupilStudyingAndGraduated
                = PupilOfClassBusiness.SearchBySchool(SchoolID,
                                    new Dictionary<string, object> {
                                        { "AcademicYearID", AcademicYearID },
                                        { "AppliedLevel", AppliedLevel },
                                        { "EducationLevelID", EducationLevelID.HasValue ? EducationLevelID.Value : 0 },
                                        { "ClassID", ClassID.HasValue ? ClassID.Value : 0 },
                                        { "Check", "Check" }
                                    }).Where(u => (!ClassID.HasValue || u.ClassID == ClassID.Value)
                                                && (u.Status == SystemParamsInFile.PUPIL_STATUS_STUDYING
                                                || u.Status == SystemParamsInFile.PUPIL_STATUS_GRADUATED))
                                                .Select(o => new { o.ClassProfile.EducationLevelID, o.ClassID, o.PupilID })
                                                .ToList();
            // Danh sach hoc sinh thoi hoc truoc ngay so sanh nam hoc
            var listPupilOff = PupilLeavingOffBusiness.All.Where(o =>
                o.PupilProfile.IsActive
                && o.AcademicYearID == AcademicYearID
                && o.SchoolID == SchoolID
                && (!AppliedLevel.HasValue || AppliedLevel.Value == 0 || o.EducationLevel.Grade == AppliedLevel)
                && (!EducationLevelID.HasValue || EducationLevelID.Value == 0 || o.EducationLevelID == EducationLevelID)
                && (!ClassID.HasValue || ClassID.Value == 0 || o.ClassID == ClassID)
                && o.LeavingDate >= endDate
                ).Select(o => new { o.ClassProfile.EducationLevelID, o.ClassID, o.PupilID }).ToList();
            // Danh sach hoc sinh chuyen truong truoc ngay so sanh nam hoc
            var listPupiMoveSchool = SchoolMovementBusiness.All.Where(o =>
                o.PupilProfile.IsActive
                && o.AcademicYearID == AcademicYearID
                && o.SchoolID == SchoolID
                && (!AppliedLevel.HasValue || AppliedLevel.Value == 0 || o.EducationLevel.Grade == AppliedLevel)
                && (!EducationLevelID.HasValue || EducationLevelID.Value == 0 || o.EducationLevelID == EducationLevelID)
                && (!ClassID.HasValue || ClassID.Value == 0 || o.ClassID == ClassID)
                && o.MovedDate >= endDate
                ).Select(o => new { o.ClassProfile.EducationLevelID, o.ClassID, o.PupilID }).ToList();
            // Danh sach hoc sinh chuyen lop truoc ngay so sanh nam hoc  
            var listClassMovement = ClassMovementBusiness.All.Where(o =>
                o.PupilProfile.IsActive
                && o.AcademicYearID == AcademicYearID
                && o.SchoolID == SchoolID
                && (!AppliedLevel.HasValue || AppliedLevel.Value == 0 || o.EducationLevel.Grade == AppliedLevel)
                && (!EducationLevelID.HasValue || EducationLevelID.Value == 0 || o.EducationLevelID == EducationLevelID)
                && (!ClassID.HasValue || ClassID.Value == 0 || o.FromClassID == ClassID)
                && o.MovedDate >= endDate
                ).Select(o => new { o.ClassProfile.EducationLevelID, ClassID = o.FromClassID, o.PupilID }).ToList();

            // Bo sung them cac hoc sinh thoi hoc, chuyen truong, chuyen lop ngay so sanh nam hoc
            listPupilOfClass = listPupilStudyingAndGraduated.Union(listPupilOff).Union(listPupiMoveSchool).Union(listClassMovement).Distinct()
                .Select(o => new PupilOfClassBO()
                {
                    ClassID = o.ClassID,
                    PupilID = o.PupilID,
                    EducationLevelID = o.EducationLevelID
                })
                .ToList();

            return listPupilOfClass;
        }

        public List<PupilOfClassBO> GetPupilOfClassStatusOrtherStudy(IDictionary<string, object> dic)
        {
            int schoolID = Utils.GetInt(dic, "SchoolID");
            int academicYearID = Utils.GetInt(dic, "AcademicYearID");
            int semesterID = Utils.GetInt(dic, "SemesterID");
            int classID = Utils.GetInt(dic, "ClassID");
            List<PupilOfClassBO> listPupilOfClass = new List<PupilOfClassBO>();
            // Danh sach hoc sinh thoi hoc truoc ngay so sanh nam hoc
            var listPupilOff = PupilLeavingOffBusiness.All.Where(o =>
                o.PupilProfile.IsActive
                && o.AcademicYearID == academicYearID
                && o.SchoolID == schoolID
                && o.ClassID == classID
                ).Select(o => new { pupilID = o.PupilID, moveDate = o.LeavingDate }).ToList();
            // Danh sach hoc sinh chuyen truong truoc ngay so sanh nam hoc
            var listPupiMoveSchool = SchoolMovementBusiness.All.Where(o =>
                o.PupilProfile.IsActive
                && o.AcademicYearID == academicYearID
                && o.SchoolID == schoolID
                && o.ClassID == classID
                ).Select(o => new { pupilID = o.PupilID, moveDate = o.MovedDate }).ToList();
            // Danh sach hoc sinh chuyen lop truoc ngay so sanh nam hoc  
            var listClassMovement = ClassMovementBusiness.All.Where(o =>
                o.PupilProfile.IsActive
                && o.AcademicYearID == academicYearID
                && o.SchoolID == schoolID
                && o.FromClassID == classID
                ).Select(o => new { pupilID = o.PupilID, moveDate = o.MovedDate }).ToList();

            listPupilOfClass = listPupilOff.Union(listPupiMoveSchool).Union(listClassMovement).Distinct()
                .Select(o => new PupilOfClassBO
                {
                    PupilID = o.pupilID,
                    LeavingDate = o.moveDate
                }).ToList();
            return listPupilOfClass;
        }

        //Hàm lấy lớp học và số học sinh trong lớp (sĩ số và sĩ số thực tế) loc bo ra nhung hoc sinh không đăng ký môn tự chọn
        //Type = 1: Chỉ lấy sĩ số thực tế
        //Type = 2: Chỉ lấy sĩ số
        //Type = 0: Lấy cả sĩ số và sĩ số thực tế: Chỉ dùng nếu nghiệp vụ bắt buộc vì hiệu năng
        public IQueryable<ClassInfoBO> GetClassAndTotalPupilNotRegister(IQueryable<ClassProfile> iqClass, IDictionary<string, object> dic)
        {
            //int SchoolID, int AcademicYearID, int Semester, int Type, int SubjectID = 0
            int Type = Utils.GetInt(dic, "Type");
            int SchoolID = Utils.GetInt(dic, "SchoolID");
            int SubjectID = Utils.GetInt(dic, "SubjectID");
            int AcademicYearID = Utils.GetInt(dic, "AcademicYearID");
            int Semester = Utils.GetInt(dic, "SemesterID");
            if (Type != SystemParamsInFile.GET_NUMPUPIL_IN_CLASS_TYPE3)
            {
                dic["CheckWithClass"] = "true";
            }

            AcademicYear aca = AcademicYearBusiness.Find(AcademicYearID);
            DateTime compareDate;
            if (Semester == SystemParamsInFile.SEMESTER_OF_YEAR_FIRST)
            {
                compareDate = aca.FirstSemesterEndDate.Value;
            }
            else
            {
                compareDate = aca.SecondSemesterEndDate.Value;
            }
            #region Danh sach hoc sinh thực tế theo hoc ky
            IQueryable<PupilOfClass> lsPupilOfClassSemesterAll = PupilOfClassBusiness.SearchBySchool(SchoolID, dic);
            IQueryable<PupilOfClassBO> lsPupilOfClassSemester = (from p in lsPupilOfClassSemesterAll.AddCriteriaSemester(aca, Semester)
                                                                 join cs in ClassSubjectBusiness.SearchBySchool(SchoolID) on p.ClassID equals cs.ClassID
                                                                 where (cs.SubjectID == SubjectID || SubjectID == 0)
                                                                 select new PupilOfClassBO
                                                                 {
                                                                     ClassID = p.ClassID,
                                                                     PupilID = p.PupilID,
                                                                     AppliedType = cs.AppliedType,
                                                                     IsSpecialize = cs.IsSpecializedSubject,
                                                                     SubjectID = cs.SubjectID
                                                                 });
            IDictionary<string, object> dicRegis = new Dictionary<string, object>()
            {
                {"SchoolID",SchoolID},
                {"AcademicYearID",AcademicYearID},
                {"SubjectID",SubjectID},
                {"SemesterID",Semester},
                {"YearID",aca.Year}
            };
            List<PupilOfClassBO> lstPOC = this.GetPupilOfClassInSpecialize(lsPupilOfClassSemester, dicRegis);

            List<ExemptedSubject> lstExempSubject = ExemptedSubjectBusiness.GetExmpSubjectbyAcademicYear(AcademicYearID, Semester, SubjectID, 0);


            //Khong lay cac hoc sinh mien giam
            if (lstExempSubject != null && lstExempSubject.Count > 0 && lstPOC.Count > 0)
            {
                lstPOC = lstPOC.Where(p => !lstExempSubject.Exists(s => s.PupilID == p.PupilID && p.ClassID == s.ClassID && s.SubjectID == p.SubjectID)).ToList();
            }



            var lstPupilActualInClass = (from poc in lstPOC
                                         group poc by new
                                         {
                                             poc.ClassID,
                                             poc.SubjectID
                                         } into c
                                         select new
                                         {
                                             ClassID = c.Key.ClassID,
                                             SubjectID = c.Key.SubjectID,
                                             ActualNumOfPupil = c.Count()
                                         });


            List<ClassInfoBO> lstNumberPupilActualInClass = (from c in iqClass.ToList()
                                                             join g in lstPupilActualInClass on c.ClassProfileID equals g.ClassID into g1
                                                             from e in g1.DefaultIfEmpty()
                                                             select new ClassInfoBO
                                                             {
                                                                 ClassID = c.ClassProfileID,
                                                                 SubjectID = (e == null ? 0 : e.SubjectID),
                                                                 ClassName = c.DisplayName,
                                                                 ClassOrderNumber = c.OrderNumber ?? 0,
                                                                 EducationLevelID = c.EducationLevelID,
                                                                 TeacherName = (c.Employee == null ? "" : c.Employee.FullName),
                                                                 ActualNumOfPupil = (e == null ? 0 : e.ActualNumOfPupil)
                                                             }).ToList();
            if (Type == SystemParamsInFile.GET_NUMPUPIL_IN_CLASS_TYPE1)
            {
                return lstNumberPupilActualInClass.AsQueryable();
            }
            #endregion

            #region Danh sach hoc sinh
            var lstPupilInClass = from poc in lsPupilOfClassSemesterAll.Where(o => o.AssignedDate <= compareDate)
                                  group poc by new
                                  {
                                      poc.ClassID
                                  } into c
                                  select new
                                  {
                                      ClassID = c.Key.ClassID,
                                      NumOfPupil = c.Count()
                                  };

            var lstaa = lstPupilInClass.ToList();
            List<ClassInfoBO> lstNumberPupilInClass = (from c in iqClass.ToList()
                                                       join g in lstaa on c.ClassProfileID equals g.ClassID into g1
                                                       from e in g1.DefaultIfEmpty()
                                                       select new ClassInfoBO
                                                       {
                                                           ClassID = c.ClassProfileID,
                                                           ClassName = c.DisplayName,
                                                           ClassOrderNumber = c.OrderNumber ?? 0,
                                                           EducationLevelID = c.EducationLevelID,
                                                           TeacherName = (c.Employee == null) ? "" : c.Employee.FullName,
                                                           NumOfPupil = (e == null) ? 0 : e.NumOfPupil
                                                       }).ToList();
            #endregion



            if (Type == SystemParamsInFile.GET_NUMPUPIL_IN_CLASS_TYPE2)
            {
                return lstNumberPupilInClass.AsQueryable();
            }
            else if (Type == SystemParamsInFile.GET_NUMPUPIL_IN_CLASS_TYPE0)
            {

                var lstPupil = from g in lstNumberPupilInClass
                               join g2 in lstNumberPupilActualInClass on g.ClassID equals g2.ClassID
                               select new ClassInfoBO
                               {
                                   ClassID = g.ClassID,
                                   SubjectID = g2.SubjectID,
                                   ClassName = g.ClassName,
                                   ClassOrderNumber = g.ClassOrderNumber,
                                   EducationLevelID = g.EducationLevelID,
                                   TeacherName = g.TeacherName,
                                   NumOfPupil = g.NumOfPupil,
                                   ActualNumOfPupil = g2.ActualNumOfPupil
                               };

                return lstPupil.AsQueryable();
            }
            else
            {
                return null;
            }
        }


        /// <summary>
        /// hàm lay ss,sstt danh cho thong ke hoc luc
        /// </summary>
        /// <param name="iqClass"></param>
        /// <param name="SchoolID"></param>
        /// <param name="AcademicYearID"></param>
        /// <param name="Semester"></param>
        /// <param name="Type"></param>
        /// <returns></returns>
        public IQueryable<ClassInfoBO> GetClassAndTotalPupil(IQueryable<ClassProfile> iqClass, IDictionary<string, object> dic)
        {
            //int SchoolID, int AcademicYearID, int Semester, int Type 
            int Type = Utils.GetInt(dic, "Type");
            int AcademicYearID = Utils.GetInt(dic, "AcademicYearID");
            int Semester = Utils.GetInt(dic, "Semester");
            int SchoolID = Utils.GetInt(dic, "SchoolID");
            if (Type != SystemParamsInFile.GET_NUMPUPIL_IN_CLASS_TYPE3)
            {
                dic["CheckWithClass"] = "true";
            }

            AcademicYear aca = AcademicYearBusiness.Find(AcademicYearID);
            DateTime compareDate;
            if (Semester == SystemParamsInFile.SEMESTER_OF_YEAR_FIRST)
            {
                compareDate = aca.FirstSemesterEndDate.Value;
            }
            else
            {
                compareDate = aca.SecondSemesterEndDate.Value;
            }

            #region Danh sach hoc sinh thực tế theo hoc ky
            IQueryable<PupilOfClass> lsPupilOfClassSemesterAll = PupilOfClassBusiness.SearchBySchool(SchoolID, dic);
            IQueryable<PupilOfClass> lsPupilOfClassSemester = lsPupilOfClassSemesterAll.AddCriteriaSemester(aca, Semester);

            var lstPupilActualInClass = from poc in lsPupilOfClassSemester
                                        group poc by new
                                        {
                                            poc.ClassID
                                        } into c
                                        select new
                                        {
                                            ClassID = c.Key.ClassID,
                                            ActualNumOfPupil = c.Count()
                                        };

            IQueryable<ClassInfoBO> lstNumberPupilActualInClass = from c in iqClass
                                                                  join g in lstPupilActualInClass on c.ClassProfileID equals g.ClassID into g1
                                                                  from e in g1.DefaultIfEmpty()
                                                                  select new ClassInfoBO
                                                                  {
                                                                      ClassID = c.ClassProfileID,
                                                                      ClassName = c.DisplayName,
                                                                      ClassOrderNumber = c.OrderNumber,
                                                                      EducationLevelID = c.EducationLevelID,
                                                                      TeacherName = c.Employee.FullName,
                                                                      ActualNumOfPupil = (e.ActualNumOfPupil == null ? 0 : e.ActualNumOfPupil),
                                                                      IsClassVNEN = c.IsVnenClass
                                                                  };
            if (Type == SystemParamsInFile.GET_NUMPUPIL_IN_CLASS_TYPE1)
            {
                return lstNumberPupilActualInClass;
            }
            #endregion

            #region Danh sach hoc sinh
            var lstPupilInClass = from poc in lsPupilOfClassSemesterAll.Where(o => o.AssignedDate <= compareDate)
                                  group poc by new
                                  {
                                      poc.ClassID
                                  } into c
                                  select new
                                  {
                                      ClassID = c.Key.ClassID,
                                      NumOfPupil = c.Count()
                                  };

            IQueryable<ClassInfoBO> lstNumberPupilInClass = from c in iqClass
                                                            join g in lstPupilInClass on c.ClassProfileID equals g.ClassID into g1
                                                            from e in g1.DefaultIfEmpty()
                                                            select new ClassInfoBO
                                                            {
                                                                ClassID = c.ClassProfileID,
                                                                ClassName = c.DisplayName,
                                                                ClassOrderNumber = c.OrderNumber,
                                                                EducationLevelID = c.EducationLevelID,
                                                                TeacherName = c.Employee.FullName,
                                                                NumOfPupil = (e.NumOfPupil == null ? 0 : e.NumOfPupil),
                                                                IsClassVNEN = c.IsVnenClass
                                                            };
            #endregion



            if (Type == SystemParamsInFile.GET_NUMPUPIL_IN_CLASS_TYPE2)
            {
                return lstNumberPupilInClass;
            }
            else if (Type == SystemParamsInFile.GET_NUMPUPIL_IN_CLASS_TYPE0)
            {
                var lstPupil = from g in lstNumberPupilInClass.ToList()
                               join g2 in lstNumberPupilActualInClass.ToList() on g.ClassID equals g2.ClassID
                               select new ClassInfoBO
                               {
                                   ClassID = g.ClassID,
                                   ClassName = g.ClassName,
                                   ClassOrderNumber = g.ClassOrderNumber,
                                   EducationLevelID = g.EducationLevelID,
                                   TeacherName = g.TeacherName,
                                   NumOfPupil = g.NumOfPupil,
                                   ActualNumOfPupil = g2.ActualNumOfPupil,
                                   IsClassVNEN = g2.IsClassVNEN
                               };
                return lstPupil.AsQueryable();
            }
            else
            {
                return null;
            }
        }

        /// <summary>
        /// QuangLM
        /// Bo sung ham chi lay cac thong tin can thiet cua hoc sinh
        /// Tranh truong hop phai select lai vao PupilProfile de lay thong tin hoc sinh
        /// </summary>
        /// <param name="SchoolID"></param>
        /// <param name="dic"></param>
        /// <returns></returns>
        public IQueryable<PupilOfClassBO> GetPupilInClass(int ClassID)
        {
            IQueryable<PupilOfClass> iqPocAll = PupilOfClassBusiness.AllNoTracking.Where(o => o.ClassID == ClassID);
            IQueryable<PupilOfClass> iqPoc = iqPocAll.Where(o => o.AssignedDate == iqPocAll.Where(p => p.PupilID == o.PupilID).Max(p => p.AssignedDate));
            IQueryable<PupilOfClassBO> query = (from poc in iqPoc
                                                join pp in PupilProfileBusiness.AllNoTracking.OrderBy(x => x.PupilProfileID) on poc.PupilID equals pp.PupilProfileID
                                                where pp.IsActive
                                                select new PupilOfClassBO()
                                                {
                                                    PupilOfClassID = poc.PupilOfClassID,
                                                    SchoolID = poc.SchoolID,
                                                    AcademicYearID = poc.AcademicYearID,
                                                    ClassID = poc.ClassID,
                                                    PupilID = poc.PupilID,
                                                    PupilCode = pp.PupilCode,
                                                    PupilFullName = pp.FullName,
                                                    Name = pp.Name,
                                                    OrderInClass = poc.OrderInClass,
                                                    Status = poc.Status,
                                                    EndDate = poc.EndDate,
                                                    Birthday = pp.BirthDate,
                                                    BirthPlace = pp.BirthPlace,
                                                    ResidentalAddress = pp.TempResidentalAddress,
                                                    PermanentResidentalAddress = pp.PermanentResidentalAddress,
                                                    EducationLevelID = poc.ClassProfile.EducationLevelID,
                                                    EthnicName = pp.Ethnic.EthnicName,
                                                    Genre = pp.Genre,
                                                    FatherFullName = pp.FatherFullName,
                                                    FatherJobName = pp.FatherJob,
                                                    MotherFullName = pp.MotherFullName,
                                                    MotherJobName = pp.MotherJob,
                                                    SponserFullName = pp.SponsorFullName,
                                                    SponserJobName = pp.SponsorJob,
                                                    ClassName = poc.ClassProfile.DisplayName,
                                                    FatherMobile = pp.FatherMobile,
                                                    FatherEmail = pp.FatherEmail,
                                                    MotherMobile = pp.MotherMobile,
                                                    MotherEmail = pp.MotherEmail,
                                                    SponserMobile = pp.SponsorMobile,
                                                    SponserEmail = pp.SponsorEmail,
                                                    HomeTown = pp.HomeTown,
                                                    EnrolmentDate = pp.EnrolmentDate,
                                                    StorageNumber = pp.StorageNumber
                                                });
            return query;
        }

        public List<PupilOfClassBO> GetListPupilByClassID(int SchoolID, int AcademicYearID, int? ClassID)
        {

            IDictionary<string, object> dic = new Dictionary<string, object>();
            dic["SchoolID"] = SchoolID;
            dic["AcademicYearID"] = AcademicYearID;
            dic["ClassID"] = ClassID;
            dic["Check"] = "check";

            return (from poc in PupilOfClassBusiness.Search(dic)
                    join pp in PupilProfileBusiness.All on poc.PupilID equals pp.PupilProfileID
                    join cp in ClassProfileBusiness.All.Where(c => c.AcademicYearID == AcademicYearID) on poc.ClassID equals cp.ClassProfileID
                    join ed in EducationLevelBusiness.All on cp.EducationLevelID equals ed.EducationLevelID
                    join sp in SchoolProfileBusiness.All.Where(sp => sp.SchoolProfileID == SchoolID) on poc.SchoolID equals sp.SchoolProfileID
                    where (poc.Status == GlobalConstants.PUPIL_STATUS_STUDYING)
                    && cp.IsActive == true
                    select new PupilOfClassBO
                    {
                        PupilID = poc.PupilID,
                        PupilCode = pp.PupilCode,
                        AcademicYearID = poc.AcademicYearID,
                        ClassID = poc.ClassID,
                        PupilFullName = pp.FullName,
                        ClassName = cp.DisplayName,
                        SchoolName = sp.SchoolName,
                        EducationLevelID = cp.EducationLevelID,
                        Name = pp.Name,
                        FatherFullName = pp.FatherFullName,
                        MotherFullName = pp.MotherFullName,
                        SponserFullName = pp.SponsorFullName,
                        FatherMobile = pp.FatherMobile,
                        MotherMobile = pp.MotherMobile,
                        SponserMobile = pp.SponsorMobile,
                        Genre = pp.Genre,
                        Mobile = pp.Mobile,
                        OrderInClass = poc.OrderInClass,
                        Birthday = pp.BirthDate,
                        EducationLevelName = ed.Resolution
                    }).ToList();

        }

        public IQueryable<PupilOfClassBO> GetPupilInClassTranscripReport(int ClassID)
        {
            IQueryable<PupilOfClass> iqPocAll = PupilOfClassBusiness.AllNoTracking.Where(o => o.ClassID == ClassID);
            IQueryable<PupilOfClass> iqPoc = iqPocAll.Where(o => o.AssignedDate == iqPocAll.Where(p => p.PupilID == o.PupilID).Max(p => p.AssignedDate));
            IQueryable<PupilOfClassBO> query = (from poc in iqPoc
                                                join pp in PupilProfileBusiness.AllNoTracking on poc.PupilID equals pp.PupilProfileID
                                                where pp.IsActive
                                                select new PupilOfClassBO()
                                                {
                                                    PupilOfClassID = poc.PupilOfClassID,
                                                    SchoolID = poc.SchoolID,
                                                    AcademicYearID = poc.AcademicYearID,
                                                    ClassID = poc.ClassID,
                                                    PupilID = poc.PupilID,
                                                    PupilCode = pp.PupilCode,
                                                    PupilFullName = pp.FullName,
                                                    Name = pp.Name,
                                                    OrderInClass = poc.OrderInClass,
                                                    Status = poc.Status,
                                                    EndDate = poc.EndDate,
                                                    Birthday = pp.BirthDate,
                                                    BirthPlace = pp.BirthPlace,
                                                    ResidentalAddress = pp.TempResidentalAddress,
                                                    PermanentResidentalAddress = pp.PermanentResidentalAddress,
                                                    EducationLevelID = poc.ClassProfile.EducationLevelID,
                                                    EthnicName = pp.Ethnic.EthnicName,
                                                    Genre = pp.Genre,
                                                    FatherFullName = pp.FatherFullName,
                                                    FatherJobName = pp.FatherJob,
                                                    MotherFullName = pp.MotherFullName,
                                                    MotherJobName = pp.MotherJob,
                                                    SponserFullName = pp.SponsorFullName,
                                                    SponserJobName = pp.SponsorJob,
                                                    ClassName = poc.ClassProfile.DisplayName,
                                                    FatherMobile = pp.FatherMobile,
                                                    FatherEmail = pp.FatherEmail,
                                                    MotherMobile = pp.MotherMobile,
                                                    MotherEmail = pp.MotherEmail,
                                                    SponserMobile = pp.SponsorMobile,
                                                    SponserEmail = pp.SponsorEmail,
                                                    HomeTown = pp.HomeTown,
                                                    SchoolProfile = pp.SchoolProfile,
                                                    EnrolmentDate = pp.EnrolmentDate,
                                                    StorageNumber = pp.StorageNumber,
                                                    Image = pp.Image
                                                });
            return query;
        }

        public IQueryable<PupilOfClassBO> GetStudyingPupil(int SchoolID, int AcademicYearID, int AppliedLevel, IDictionary<string, object> dic)
        {
            var iq = from poc in PupilOfClassBusiness.All
                     join pp in PupilProfileBusiness.All on poc.PupilID equals pp.PupilProfileID
                     join cp in ClassProfileBusiness.All.Where(o => o.AcademicYearID == AcademicYearID && o.IsActive == true) on poc.ClassID equals cp.ClassProfileID
                     join el in EducationLevelBusiness.All on cp.EducationLevelID equals el.EducationLevelID
                     where poc.SchoolID == SchoolID
                     && pp.IsActive == true
                     && poc.AcademicYearID == AcademicYearID
                     && el.Grade == AppliedLevel
                     && poc.Status == GlobalConstants.PUPIL_STATUS_STUDYING
                     select new PupilOfClassBO
                     {
                         PupilID = poc.PupilID,
                         ClassID = poc.ClassID,
                         PupilFullName = pp.FullName,
                         Birthday = pp.BirthDate,
                         Genre = pp.Genre,
                         EducationLevelID = cp.EducationLevelID,
                         ClassName = cp.DisplayName,
                         PupilCode = pp.PupilCode,
                         OrderInClass = poc.OrderInClass,
                         EthnicCode = pp.Ethnic.EthnicCode,
                         Name = pp.Name,
                         ClassOrder = cp.OrderNumber,
                         IsVNEN = cp.IsVnenClass
                     };

            int educationLevelID = Utils.GetInt(dic, "EducationLevelID");
            int classID = Utils.GetInt(dic, "ClassID");
            string pupilCodeOrName = Utils.GetString(dic, "PupilCodeOrName");
            List<int> lstClassID = Utils.GetIntList(dic, "lstClassID");

            if (educationLevelID != 0)
            {
                iq = iq.Where(o => o.EducationLevelID == educationLevelID);
            }
            if (classID != 0)
            {
                iq = iq.Where(o => o.ClassID == classID);
            }

            if (lstClassID.Count > 0)
            {
                iq = iq.Where(p => lstClassID.Contains(p.ClassID));
            }

            if (pupilCodeOrName.Trim().Length > 0)
            {
                iq = iq.Where(o => o.PupilCode.ToUpper().Contains(pupilCodeOrName.ToUpper())
                                || o.PupilFullName.ToUpper().Contains(pupilCodeOrName.ToUpper()));
            }
            return iq;
        }

        #region Bao cao danh sach dang ky thi tot nghiep
        public ProcessedReport GetGraduationExamRegisterReport(IDictionary<string, object> dic)
        {
            string reportCode = SystemParamsInFile.REPORT_DANH_SACH_DIEMTHI_THEO_LOP;
            string inputParameterHashKey = ReportUtils.GetHashKey(dic);
            return ProcessedReportBusiness.GetProcessedReport(reportCode, inputParameterHashKey);
        }

        public Stream CreateGraduationExamRegisterReport(IDictionary<string, object> dic)
        {
            int academicYearID = Utils.GetInt(dic["AcademicYearID"]);
            int schoolID = Utils.GetInt(dic["SchoolID"]);
            int appliedLevel = Utils.GetInt(dic["AppliedLevelID"]);
            int educationLevelID = Utils.GetInt(dic["EducationLevelID"]);
            int classID = Utils.GetInt(dic["ClassID"]);

            //Lấy đường dẫn báo cáo
            string reportCode = SystemParamsInFile.REPORT_GRADUATION_EXAM_REGISTER;
            ReportDefinition rp = ReportDefinitionBusiness.GetByCode(reportCode);
            string templatePath = SystemParamsInFile.TEMPLATE_FOLDER + rp.TemplateDirectory + "/" + rp.TemplateName;
            //Khởi tạo
            IVTWorkbook oBook = VTExport.OpenWorkbook(templatePath);

            SchoolProfile sp = SchoolProfileBusiness.Find(schoolID);
            //Lấy ra sheet đầu tiên
            IVTWorksheet sheet = oBook.GetSheet(1);

            //Lay danh sach hoc sinh
            IDictionary<string, object> tmpDic = new Dictionary<string, object>();
            tmpDic["EducationLevelID"] = educationLevelID;
            tmpDic["ClassID"] = classID;
            tmpDic["SchoolID"] = schoolID;
            tmpDic["AcademicYearID"] = academicYearID;
            tmpDic["Status"] = GlobalConstants.PUPIL_STATUS_STUDYING;

            IQueryable<PupilOfClass> iqPoc = PupilOfClassBusiness.Search(tmpDic);
            List<PupilOfClass> lstPoc = iqPoc.ToList()
                .OrderBy(o => SMAS.Business.Common.Utils.SortABC(o.PupilProfile.FullName.getOrderingName(o.PupilProfile.Ethnic != null ? o.PupilProfile.Ethnic.EthnicCode : "01"))).ToList();

            List<int> lstEducationLevel = new List<int>() { 10, 11 };
            List<int> lstPupilId = lstPoc.Select(o => o.PupilID).ToList();
            List<PupilOfClass> lstPocToGetOldClass = (from pr in PupilRankingBusiness.All
                                                      join poc in PupilOfClassBusiness.All on new { pr.AcademicYearID, pr.PupilID, pr.ClassID } equals new { poc.AcademicYearID, poc.PupilID, poc.ClassID }
                                                      where lstEducationLevel.Contains(poc.ClassProfile.EducationLevelID)
                                                      && lstPupilId.Contains(pr.PupilID)
                                                      && pr.StudyingJudgementID == SystemParamsInFile.STUDYING_JUDGEMENT_UPCLASS
                                                      select poc).ToList();


            //Fill du lieu
            int startRow = 6;
            int lastRow = startRow + lstPoc.Count - 1;
            int curRow = startRow;
            int startColumn = 1;
            int lastColumn = 62;
            int curColumn = startColumn;

            Province province;
            District district;
            Commune commune;
            for (int i = 0; i < lstPoc.Count; i++)
            {
                PupilOfClass poc = lstPoc[i];

                province = poc.PupilProfile.Province != null ? poc.PupilProfile.Province : new Province();
                district = poc.PupilProfile.District != null ? poc.PupilProfile.District : new District();
                commune = poc.PupilProfile.Commune != null ? poc.PupilProfile.Commune : new Commune();


                //STT
                sheet.SetCellValue(curRow, curColumn, i + 1);
                curColumn++;

                //So ho so
                curColumn++;

                //Ma so
                curColumn++;

                //Ho ten
                sheet.SetCellValue(curRow, curColumn, poc.PupilProfile.FullName);
                curColumn++;

                //Ten
                sheet.SetCellValue(curRow, curColumn, poc.PupilProfile.Name);
                curColumn++;

                //Gioi tinh
                sheet.SetCellValue(curRow, curColumn, 1 - poc.PupilProfile.Genre);
                curColumn++;

                //Ngay thang nam sinh
                sheet.SetCellValue(curRow, curColumn, poc.PupilProfile.BirthDate.Day.ToString("00"));
                curColumn++;

                sheet.SetCellValue(curRow, curColumn, poc.PupilProfile.BirthDate.Month.ToString("00"));
                curColumn++;

                sheet.SetCellValue(curRow, curColumn, poc.PupilProfile.BirthDate.ToString("yy"));
                curColumn++;

                //Noi sinh
                sheet.SetCellValue(curRow, curColumn, province.ProvinceName);
                curColumn++;

                //dan toc
                sheet.SetCellValue(curRow, curColumn, poc.PupilProfile.Ethnic != null ? poc.PupilProfile.Ethnic.EthnicName : String.Empty);
                curColumn++;

                //dan toc khac
                curColumn++;

                //CMND
                sheet.SetCellValue(curRow, curColumn, poc.PupilProfile.IdentifyNumber);
                curColumn++;

                //Khong co CMND
                sheet.SetCellValue(curRow, curColumn, String.IsNullOrEmpty(poc.PupilProfile.IdentifyNumber) ? "x" : "");
                curColumn++;

                //Ma tinh
                sheet.SetCellValue(curRow, curColumn, province.ProvinceExamCode);
                curColumn++;

                //Ma huyen
                sheet.SetCellValue(curRow, curColumn, district.DistrictExamCode);
                curColumn++;

                //Ma xa
                sheet.SetCellValue(curRow, curColumn, commune.CommuneExamCode);
                curColumn++;

                //Dia chi
                string permanentResidentalAddress = !String.IsNullOrEmpty(poc.PupilProfile.PermanentResidentalAddress) ?
                    poc.PupilProfile.PermanentResidentalAddress : commune.CommuneName + " - " + district.DistrictName + " - " + province.ProvinceName;
                sheet.SetCellValue(curRow, curColumn, permanentResidentalAddress);
                curColumn++;

                PupilOfClass poc10 = lstPocToGetOldClass.Where(o => o.PupilID == poc.PupilID && o.ClassProfile.EducationLevelID == 10).FirstOrDefault();
                PupilOfClass poc11 = lstPocToGetOldClass.Where(o => o.PupilID == poc.PupilID && o.ClassProfile.EducationLevelID == 11).FirstOrDefault();
                //Ma tinh lop 10
                if (poc10 != null)
                {
                    sheet.SetCellValue(curRow, curColumn, poc10.ClassProfile.SchoolProfile.Province != null ? poc10.ClassProfile.SchoolProfile.Province.ProvinceExamCode : String.Empty);
                }
                else
                {
                    sheet.SetCellValue(curRow, curColumn, sp.Province != null ? sp.Province.ProvinceExamCode : String.Empty);
                }
                curColumn++;

                //Ma truong lop 10
                if (poc10 != null)
                {
                    sheet.SetCellValue(curRow, curColumn, poc10.ClassProfile.SchoolProfile.SchoolExamCode);
                }
                else
                {
                    sheet.SetCellValue(curRow, curColumn, sp.SchoolExamCode);
                }
                curColumn++;

                //Ma tinh lop 11
                if (poc11 != null)
                {
                    sheet.SetCellValue(curRow, curColumn, poc11.ClassProfile.SchoolProfile.Province != null ? poc11.ClassProfile.SchoolProfile.Province.ProvinceExamCode : String.Empty);
                }
                else
                {
                    sheet.SetCellValue(curRow, curColumn, sp.Province != null ? sp.Province.ProvinceExamCode : String.Empty);
                }
                curColumn++;

                //Ma truong lop 11
                if (poc11 != null)
                {
                    sheet.SetCellValue(curRow, curColumn, poc11.ClassProfile.SchoolProfile.SchoolExamCode);
                }
                else
                {
                    sheet.SetCellValue(curRow, curColumn, sp.SchoolExamCode);
                }
                curColumn++;

                //Ma tinh lop 12
                sheet.SetCellValue(curRow, curColumn, sp.Province != null ? sp.Province.ProvinceExamCode : String.Empty);
                curColumn++;

                //Ma truong lop 12
                sheet.SetCellValue(curRow, curColumn, sp.SchoolExamCode);
                curColumn++;

                //ten lop 12
                sheet.SetCellValue(curRow, curColumn, poc.ClassProfile.DisplayName);
                curColumn++;

                //Dien thoai
                sheet.SetCellValue(curRow, curColumn, !String.IsNullOrEmpty(poc.PupilProfile.Mobile) ? poc.PupilProfile.Mobile : poc.PupilProfile.Telephone);
                curColumn++;

                //Email
                sheet.SetCellValue(curRow, curColumn, poc.PupilProfile.Email);
                curColumn++;

                //dia chi lien he
                sheet.SetCellValue(curRow, curColumn, !String.IsNullOrEmpty(poc.PupilProfile.TempResidentalAddress) ? poc.PupilProfile.TempResidentalAddress : permanentResidentalAddress);
                curColumn++;

                curColumn = startColumn;
                curRow++;
            }

            //Ve border
            sheet.GetRange(startRow, startColumn, lastRow, lastColumn).SetBorder(VTBorderStyle.Solid, VTBorderIndex.All);

            return oBook.ToStream(true, true);
        }

        public ProcessedReport InsertGraduationExamRegisterReport(IDictionary<string, object> dic, Stream data, int appliedLevel)
        {
            string reportCode = SystemParamsInFile.REPORT_GRADUATION_EXAM_REGISTER;

            ProcessedReport pr = new ProcessedReport();

            ReportDefinition reportDef = ReportDefinitionBusiness.GetByCode(reportCode);
            pr.ReportCode = reportCode;
            pr.ProcessedDate = DateTime.Now;
            pr.InputParameterHashKey = ReportUtils.GetHashKey(dic);
            pr.ReportData = ReportUtils.Compress(data);

            //Tạo tên file
            string outputNamePattern = reportDef.OutputNamePattern;

            pr.ReportName = ReportUtils.RemoveSpecialCharacters(outputNamePattern) + "." + reportDef.OutputFormat;

            ProcessedReportParameterRepository.Insert(dic, pr);
            ProcessedReportRepository.Insert(pr);
            ProcessedReportRepository.Save();
            return pr;
        }
        #endregion

        #region Bao cao so lieu hoc sinh theo do tuoi
        public ProcessedReport GetPupilByAgeReport(IDictionary<string, object> dic)
        {
            string reportCode = SystemParamsInFile.HS_THONG_KE_DO_TUOI;
            string inputParameterHashKey = ReportUtils.GetHashKey(dic);
            return ProcessedReportBusiness.GetProcessedReport(reportCode, inputParameterHashKey);
        }

        public Stream CreatePupilByAgeReport(IDictionary<string, object> dic)
        {
            int academicYearID = Utils.GetInt(dic["AcademicYearID"]);
            int schoolID = Utils.GetInt(dic["SchoolID"]);
            int appliedLevel = Utils.GetInt(dic["AppliedLevelID"]);
            int educationLevelID = Utils.GetInt(dic["EducationLevelID"]);
            AcademicYear aca = AcademicYearBusiness.Find(academicYearID);


            //Lấy đường dẫn báo cáo
            string reportCode = SystemParamsInFile.HS_THONG_KE_DO_TUOI;
            ReportDefinition rp = ReportDefinitionBusiness.GetByCode(reportCode);
            string templatePath = SystemParamsInFile.TEMPLATE_FOLDER + rp.TemplateDirectory + "/" + rp.TemplateName;
            //Khởi tạo
            IVTWorkbook oBook = VTExport.OpenWorkbook(templatePath);

            SchoolProfile sp = SchoolProfileBusiness.Find(schoolID);
            //Lấy ra sheet đầu tiên
            IVTWorksheet sheet = oBook.GetSheet(1);

            //Lay danh sach hoc sinh
            IDictionary<string, object> tmpDic = new Dictionary<string, object>();
            tmpDic["EducationLevelID"] = educationLevelID;
            tmpDic["SchoolID"] = schoolID;
            tmpDic["AcademicYearID"] = academicYearID;
            tmpDic["AppliedLevel"] = appliedLevel;

            List<PupilOfClass> lstPoc = this.Search(tmpDic).ToList();

            List<PupilOfClass> lstPocStudying = lstPoc.Where(o => (o.Status == GlobalConstants.PUPIL_STATUS_STUDYING || o.Status == GlobalConstants.PUPIL_STATUS_GRADUATED)).ToList();
            List<PupilOfClass> lstPocLeaveOff = lstPoc.Where(o => o.Status == GlobalConstants.PUPIL_STATUS_LEAVED_OFF).ToList();



            //Lay ra cac do tuoi
            List<int> lstAge = lstPocStudying.Select(o => aca.Year - o.PupilProfile.BirthDate.Year).Distinct().OrderBy(o => o).ToList();

            //fill do tuoi
            int ageColumn = 13;
            int ageRow = 9;

            for (int i = 0; i < lstAge.Count; i++)
            {
                sheet.SetCellValue(ageRow, ageColumn, lstAge[i]);

                sheet.SetCellValue(ageRow + 1, ageColumn, "Tổng số");
                sheet.SetCellValue(ageRow + 1, ageColumn + 1, "Nữ");
                sheet.SetCellValue(ageRow + 1, ageColumn + 2, "Dân tộc");
                sheet.SetCellValue(ageRow + 1, ageColumn + 3, "Nữ DT");
                sheet.SetColumnWidth(ageColumn, 5);
                sheet.SetColumnWidth(ageColumn + 1, 5);
                sheet.SetColumnWidth(ageColumn + 2, 5);
                sheet.SetColumnWidth(ageColumn + 3, 5);
                sheet.GetRange(ageRow, ageColumn, ageRow, ageColumn + 3).Merge();
                ageColumn = ageColumn + 3 + 1;
            }
            int columnTemp = 9;

            sheet.GetRange(ageRow + 1, columnTemp, ageRow + 1, ageColumn - 1).SetOrientation(-90);
            sheet.GetRange(ageRow + 1, columnTemp, ageRow + 1, ageColumn - 1).SetFontStyle(true, System.Drawing.Color.Black, false, 11, false, false);
            sheet.GetRange(ageRow + 1, columnTemp, ageRow + 1, ageColumn - 1).SetHAlignVer(NativeExcel.XlVAlign.xlVAlignCenter);

            if (lstAge.Count > 0)
            {
                sheet.SetCellValue(ageRow - 1, 13, "Độ tuổi học sinh hiện có");
                sheet.GetRange(ageRow - 1, 13, ageRow - 1, ageColumn - 1).Merge();
            }
            //Fill cot ghi chu
            sheet.SetCellValue(8, 13 + lstAge.Count * 4, "Ghi chú");
            sheet.GetRange(8, 13 + lstAge.Count * 4, 9, 13 + lstAge.Count * 4).Merge();

            //Lay danh sach lop
            List<ClassProfile> lstClass = ClassProfileBusiness.All.Where(o => o.AcademicYearID == academicYearID
                                                                    && o.SchoolID == schoolID
                                                                    && o.IsActive == true
                                                                    && o.EducationLevel.Grade == appliedLevel).OrderBy(o => o.EducationLevelID)
                                                                    .ThenBy(o => o.OrderNumber.HasValue ? o.OrderNumber : 0).ThenBy(o => o.DisplayName).ToList();
            if (educationLevelID != 0)
            {
                lstClass = lstClass.Where(o => o.EducationLevelID == educationLevelID).ToList();
            }

            //Lay danh sach khoi
            List<int> lstEducationLevelID = lstClass.Select(o => o.EducationLevelID).Distinct().OrderBy(o => o).ToList();

            Ethnic Ethnic_Kinh = EthnicBusiness.All.Where(o => o.EthnicCode.ToLower().Equals(SystemParamsInFile.ETHNIC_CODE_KINH.ToLower()) && o.IsActive == true).FirstOrDefault();
            Ethnic Ethnic_ForeignPeople = EthnicBusiness.All.Where(o => o.EthnicCode.ToLower().Equals(SystemParamsInFile.ETHNIC_CODE_NN.ToLower()) && o.IsActive == true).FirstOrDefault();
            int? EthnicID_Kinh = 0;
            int? EthnicID_ForeignPeople = 0;
            if (Ethnic_Kinh != null)
            {
                EthnicID_Kinh = Ethnic_Kinh.EthnicID;
            }
            if (Ethnic_ForeignPeople != null)
            {
                EthnicID_ForeignPeople = Ethnic_ForeignPeople.EthnicID;
            }

            //Fill du lieu
            int startRow = 11;
            int curRow = startRow;
            int startColumn = 1;
            int curColumn = startColumn;
            int lastCol = 13 + lstAge.Count * 4;
            List<int> lstEduRow = new List<int>();
            List<ClassProfile> lstClassEdu;
            List<PupilOfClass> lstPocClass;
            List<PupilOfClass> lstPocStudyingClass;
            List<PupilOfClass> lstPocLeaveOffClass;
            ClassProfile cp;

            int order = 1;
            for (int i = 0; i < lstEducationLevelID.Count; i++)
            {
                lstClassEdu = lstClass.Where(o => o.EducationLevelID == lstEducationLevelID[i]).ToList();
                int startEduRow = curRow;
                for (int j = 0; j < lstClassEdu.Count; j++)
                {
                    cp = lstClassEdu[j];
                    sheet.SetCellValue(curRow, curColumn, order);
                    curColumn++;

                    sheet.SetCellValue(curRow, curColumn, cp.DisplayName);
                    curColumn++;

                    sheet.SetCellValue(curRow, curColumn, cp.Employee != null ? cp.Employee.FullName : "");
                    curColumn++;

                    // si so  dau nam
                    lstPocClass = lstPoc.Where(o => o.ClassID == cp.ClassProfileID).ToList();
                    if (lstPocClass.Count > 0)
                    {
                        sheet.SetCellValue(curRow, curColumn, lstPocClass.Count);
                    }
                    else
                    {
                        sheet.SetCellValue(curRow, curColumn, "-");
                    }
                    curColumn++;

                    // si so hien tai
                    lstPocStudyingClass = lstPocStudying.Where(o => o.ClassID == cp.ClassProfileID).ToList();
                    if (lstPocStudyingClass.Count > 0)
                    {
                        sheet.SetCellValue(curRow, curColumn, lstPocStudyingClass.Count);
                    }
                    else
                    {
                        sheet.SetCellValue(curRow, curColumn, "-");
                    }
                    curColumn++;

                    // nu
                    if (lstPocStudyingClass.Where(o => o.PupilProfile.Genre == GlobalConstants.GENRE_FEMALE).Count() > 0)
                    {
                        sheet.SetCellValue(curRow, curColumn, lstPocStudyingClass.Where(o => o.PupilProfile.Genre == GlobalConstants.GENRE_FEMALE).Count());
                    }
                    else
                    {
                        sheet.SetCellValue(curRow, curColumn, "-");
                    }
                    curColumn++;

                    // dan toc
                    if (lstPocStudyingClass.Where(o => o.PupilProfile.EthnicID != null && o.PupilProfile.EthnicID != EthnicID_Kinh && o.PupilProfile.EthnicID != EthnicID_ForeignPeople).Count() > 0)
                    {
                        sheet.SetCellValue(curRow, curColumn, lstPocStudyingClass.Where(o => o.PupilProfile.EthnicID != null && o.PupilProfile.EthnicID != EthnicID_Kinh && o.PupilProfile.EthnicID != EthnicID_ForeignPeople).Count());
                    }
                    else
                    {
                        sheet.SetCellValue(curRow, curColumn, "-");
                    }
                    curColumn++;

                    // nữ dân tộc
                    if (lstPocStudyingClass.Where(o => o.PupilProfile.Genre == GlobalConstants.GENRE_FEMALE && o.PupilProfile.EthnicID != null && o.PupilProfile.EthnicID != EthnicID_Kinh && o.PupilProfile.EthnicID != EthnicID_ForeignPeople).Count() > 0)
                    {
                        sheet.SetCellValue(curRow, curColumn, lstPocStudyingClass.Where(o => o.PupilProfile.Genre == GlobalConstants.GENRE_FEMALE && o.PupilProfile.EthnicID != null && o.PupilProfile.EthnicID != EthnicID_Kinh && o.PupilProfile.EthnicID != EthnicID_ForeignPeople).Count());
                    }
                    else
                    {
                        sheet.SetCellValue(curRow, curColumn, "-");
                    }
                    curColumn++;

                    // số học sinh bỏ học tổng số
                    lstPocLeaveOffClass = lstPocLeaveOff.Where(o => o.ClassID == cp.ClassProfileID).ToList();
                    if (lstPocLeaveOffClass.Count > 0)
                    {
                        sheet.SetCellValue(curRow, curColumn, lstPocLeaveOffClass.Count);
                    }
                    else
                    {
                        sheet.SetCellValue(curRow, curColumn, "-");
                    }
                    curColumn++;

                    // bỏ học và là nữ
                    if (lstPocLeaveOffClass.Where(o => o.PupilProfile.Genre == GlobalConstants.GENRE_FEMALE).Count() > 0)
                    {
                        sheet.SetCellValue(curRow, curColumn, lstPocLeaveOffClass.Where(o => o.PupilProfile.Genre == GlobalConstants.GENRE_FEMALE).Count());
                    }
                    else
                    {
                        sheet.SetCellValue(curRow, curColumn, "-");
                    }
                    curColumn++;

                    // bỏ học và dân tộc
                    if (lstPocLeaveOffClass.Where(o => o.PupilProfile.EthnicID != null && o.PupilProfile.EthnicID != EthnicID_Kinh && o.PupilProfile.EthnicID != EthnicID_ForeignPeople).Count() > 0)
                    {
                        sheet.SetCellValue(curRow, curColumn, lstPocLeaveOffClass.Where(o => o.PupilProfile.EthnicID != null && o.PupilProfile.EthnicID != EthnicID_Kinh && o.PupilProfile.EthnicID != EthnicID_ForeignPeople).Count());
                    }
                    else
                    {
                        sheet.SetCellValue(curRow, curColumn, "-");
                    }
                    curColumn++;

                    // bỏ học, nữ dân tộc
                    if (lstPocLeaveOffClass.Where(o => o.PupilProfile.Genre == GlobalConstants.GENRE_FEMALE && o.PupilProfile.EthnicID != null && o.PupilProfile.EthnicID != EthnicID_Kinh && o.PupilProfile.EthnicID != EthnicID_ForeignPeople).Count() > 0)
                    {
                        sheet.SetCellValue(curRow, curColumn, lstPocLeaveOffClass.Where(o => o.PupilProfile.Genre == GlobalConstants.GENRE_FEMALE && o.PupilProfile.EthnicID != null && o.PupilProfile.EthnicID != EthnicID_Kinh && o.PupilProfile.EthnicID != EthnicID_ForeignPeople).Count());
                    }
                    else
                    {
                        sheet.SetCellValue(curRow, curColumn, "-");
                    }
                    curColumn++;

                    for (int k = 0; k < lstAge.Count; k++)
                    {
                        List<PupilOfClass> lstPOCTemp = lstPocStudyingClass.Where(o => aca.Year - o.PupilProfile.BirthDate.Year == lstAge[k]).ToList();
                        if (lstPOCTemp.Count > 0)
                        {
                            sheet.SetCellValue(curRow, curColumn, lstPOCTemp.Count);
                        }
                        else
                        {
                            sheet.SetCellValue(curRow, curColumn, "-");
                        }
                        curColumn++;

                        if (lstPOCTemp.Where(o => o.PupilProfile.Genre == GlobalConstants.GENRE_FEMALE).Count() > 0)
                        {
                            sheet.SetCellValue(curRow, curColumn, lstPOCTemp.Where(o => o.PupilProfile.Genre == GlobalConstants.GENRE_FEMALE).Count());
                        }
                        else
                        {
                            sheet.SetCellValue(curRow, curColumn, "-");
                        }
                        curColumn++;

                        if (lstPOCTemp.Where(o => o.PupilProfile.EthnicID != null && o.PupilProfile.EthnicID != EthnicID_Kinh && o.PupilProfile.EthnicID != EthnicID_ForeignPeople).Count() > 0)
                        {
                            sheet.SetCellValue(curRow, curColumn, lstPOCTemp.Where(o => o.PupilProfile.EthnicID != null && o.PupilProfile.EthnicID != EthnicID_Kinh && o.PupilProfile.EthnicID != EthnicID_ForeignPeople).Count());
                        }
                        else
                        {
                            sheet.SetCellValue(curRow, curColumn, "-");
                        }
                        curColumn++;

                        if (lstPOCTemp.Where(o => o.PupilProfile.Genre == GlobalConstants.GENRE_FEMALE && o.PupilProfile.EthnicID != null && o.PupilProfile.EthnicID != EthnicID_Kinh && o.PupilProfile.EthnicID != EthnicID_ForeignPeople).Count() > 0)
                        {
                            sheet.SetCellValue(curRow, curColumn, lstPOCTemp.Where(o => o.PupilProfile.Genre == GlobalConstants.GENRE_FEMALE && o.PupilProfile.EthnicID != null && o.PupilProfile.EthnicID != EthnicID_Kinh && o.PupilProfile.EthnicID != EthnicID_ForeignPeople).Count());
                        }
                        else
                        {
                            sheet.SetCellValue(curRow, curColumn, "-");
                        }
                        curColumn++;


                    }

                    curRow++;
                    curColumn = startColumn;
                    order++;
                }

                //fill dong khoi
                curColumn++;

                sheet.SetCellValue(curRow, curColumn, "K" + lstEducationLevelID[i]);
                curColumn++;

                if (lstClassEdu.Count > 0)
                {
                    FillSumFormulaEdu(sheet, curRow, startEduRow, curRow - 1, 4, lastCol - 1);
                }
                sheet.GetRange(curRow, startColumn, curRow, lastCol).SetFontStyle(true, null, false, null, false, false);
                lstEduRow.Add(curRow);

                curRow++;
                curColumn = startColumn;

            }

            //Fill dong toan truong
            curColumn++;
            sheet.SetCellValue(curRow, curColumn, "Toàn trường");
            FillSumFormulaSchool(sheet, curRow, lstEduRow, 4, lastCol - 1);
            sheet.GetRange(curRow, startColumn, curRow, lastCol).SetFontStyle(true, null, false, null, false, false);
            sheet.GetRange(curRow, 2, curRow, 3).Merge();

            //fill ngay thang
            SchoolProfile school = SchoolProfileBusiness.Find(schoolID);
            string provinceName = (school != null && school.District != null ? school.District.DistrictName : "");
            DateTime date = DateTime.Now;
            string day = "ngày " + date.Day.ToString() + " tháng " + date.Month.ToString() + " năm " + date.Year.ToString();
            string provinceAndDate = provinceName + ", " + day;
            sheet.SetCellValue(curRow + 2, lastCol - 9, provinceAndDate);
            sheet.GetRange(curRow + 2, lastCol - 9, curRow + 2, lastCol).Merge();
            sheet.GetRange(curRow + 2, lastCol - 9, curRow + 2, lastCol).SetHAlign(VTHAlign.xlHAlignCenter);

            sheet.SetCellValue(curRow + 3, lastCol - 9, "Hiệu trưởng");
            sheet.GetRange(curRow + 3, lastCol - 9, curRow + 3, lastCol).Merge();
            sheet.GetRange(curRow + 3, lastCol - 9, curRow + 3, lastCol).SetFontStyle(true, null, false, null, false, false);
            sheet.GetRange(curRow + 3, lastCol - 9, curRow + 3, lastCol).SetHAlign(VTHAlign.xlHAlignCenter);

            //Fill ten hieu truong
            sheet.SetCellValue(curRow + 9, lastCol - 9, school.HeadMasterName);
            sheet.GetRange(curRow + 9, lastCol - 9, curRow + 9, lastCol).Merge();
            sheet.GetRange(curRow + 9, lastCol - 9, curRow + 9, lastCol).SetHAlign(VTHAlign.xlHAlignCenter);

            //fill tieu de
            string schoolName = school.SchoolName.ToUpper();
            string supervisingDeptName = UtilsBusiness.GetSupervisingDeptName(school.SchoolProfileID, appliedLevel);
            sheet.SetCellValue("A2", supervisingDeptName);
            sheet.SetCellValue("A3", schoolName);

            sheet.SetCellValue("B5", "THỐNG KÊ SỐ LIỆU HỌC SINH, NĂM HỌC: " + aca.DisplayTitle);
            sheet.SetCellValue("B6", "( Tính đến thời điểm " + day + ")");

            //Ve border
            sheet.GetRange(8, 1, curRow, lastCol).SetBorder(VTBorderStyle.Solid, VTBorderIndex.All);
            sheet.GetRange(8, 4, curRow, lastCol).SetHAlign(VTHAlign.xlHAlignCenter);

            sheet.GetRange(2, 8, 2, lastCol).Merge();
            sheet.GetRange(3, 8, 3, lastCol).Merge();
            sheet.GetRange(5, 2, 5, lastCol).Merge();
            sheet.GetRange(6, 2, 6, lastCol).Merge();

            sheet.FitAllColumnsOnOnePage = true;

            return oBook.ToStream();
        }

        private void FillSumFormulaEdu(IVTWorksheet sheet, int row, int fromRow, int toRow, int fromCol, int toCol)
        {
            //Truong Hop Khong Co EducationLevel
            if (fromRow - toRow == 1)
            {
                for (int i = fromCol; i <= toCol; i++)
                {
                    sheet.SetCellValue(fromRow, i, 0);
                }
            }
            else
            {
                for (int i = fromCol; i <= toCol; i++)
                {
                    sheet.SetCellValue(row, i, "=SUM(" + UtilsBusiness.GetExcelColumnName(i) + fromRow + ":" + UtilsBusiness.GetExcelColumnName(i) + toRow + ")");
                }
            }

        }

        private void FillSumFormulaSchool(IVTWorksheet sheet, int row, List<int> lstEduRow, int fromCol, int toCol)
        {
            for (int i = fromCol; i <= toCol; i++)
            {
                string sum = "=SUM(";
                for (int j = 0; j < lstEduRow.Count; j++)
                {
                    sum = sum + UtilsBusiness.GetExcelColumnName(i) + lstEduRow[j];
                    if (j < lstEduRow.Count - 1)
                    {
                        sum = sum + ",";
                    }

                }
                sum = sum + ")";
                sheet.SetCellValue(row, i, sum);
            }
        }
        public ProcessedReport InsertPupilByAgeReport(IDictionary<string, object> dic, Stream data, int appliedLevel)
        {
            string reportCode = SystemParamsInFile.HS_THONG_KE_DO_TUOI;

            ProcessedReport pr = new ProcessedReport();

            ReportDefinition reportDef = ReportDefinitionBusiness.GetByCode(reportCode);
            pr.ReportCode = reportCode;
            pr.ProcessedDate = DateTime.Now;
            pr.InputParameterHashKey = ReportUtils.GetHashKey(dic);
            pr.ReportData = ReportUtils.Compress(data);

            //Tạo tên file
            string outputNamePattern = reportDef.OutputNamePattern;
            string strAppliedLevel = ReportUtils.ConvertAppliedLevelForReportName(appliedLevel);
            outputNamePattern = outputNamePattern.Replace("[AppliedLevel]", ReportUtils.StripVNSign(strAppliedLevel));
            pr.ReportName = ReportUtils.RemoveSpecialCharacters(outputNamePattern) + "." + reportDef.OutputFormat;

            ProcessedReportParameterRepository.Insert(dic, pr);
            ProcessedReportRepository.Insert(pr);
            ProcessedReportRepository.Save();
            return pr;
        }
        #endregion

        #region Bao cao so lieu tre em MN theo do tuoi
        public ProcessedReport GetChildrenByAgeReport(IDictionary<string, object> dic)
        {
            string reportCode = SystemParamsInFile.THONG_KE_SO_LIEU_TRE;
            string inputParameterHashKey = ReportUtils.GetHashKey(dic);
            return ProcessedReportBusiness.GetProcessedReport(reportCode, inputParameterHashKey);
        }

        /// <summary>
        /// Get a Stream For Export
        /// Updated: 2017-Jul-12
        /// UpdatedBy: KhuongDV-DVC
        /// </summary>
        /// <param name="dic"></param>
        /// <returns></returns>
        public Stream CreateChildrenByAgeReport(IDictionary<string, object> dic)
        {
            int academicYearID = Utils.GetInt(dic["AcademicYearID"]);
            int schoolID = Utils.GetInt(dic["SchoolID"]);
            int appliedLevel = Utils.GetInt(dic["AppliedLevelID"]);
            int educationLevelID = Utils.GetInt(dic["EducationLevelID"]);

            AcademicYear academicYear = AcademicYearBusiness.Find(academicYearID);

            //Lấy đường dẫn báo cáo
            string reportCode = SystemParamsInFile.THONG_KE_SO_LIEU_TRE;
            ReportDefinition rp = ReportDefinitionBusiness.GetByCode(reportCode);
            string templatePath = SystemParamsInFile.TEMPLATE_FOLDER + rp.TemplateDirectory + "/" + rp.TemplateName;
            //Khởi tạo
            IVTWorkbook oBook = VTExport.OpenWorkbook(templatePath);
            //Lấy ra sheet đầu tiên
            IVTWorksheet sheet = oBook.GetSheet(1);

            string stringToday = string.Format("ngày {0} tháng {1} năm {2}", DateTime.Today.Day, DateTime.Today.Month, DateTime.Today.Year);

            SchoolProfile schoolProfile = SchoolProfileBusiness.Find(schoolID);

            #region [Raw School Name]
            string schoolName = schoolProfile.SchoolName.ToUpper();
            string supervisingDeptName = schoolProfile.SupervisingDept.SupervisingDeptName.ToUpper();
            sheet.SetCellValue("A2", supervisingDeptName);
            sheet.SetCellValue("A3", schoolName);
            sheet.GetRange("A2", "A3").SetHAlign(VTHAlign.xlHAlignCenter);
            sheet.GetRange("A2", "A3").SetHAlignVer(NativeExcel.XlVAlign.xlVAlignCenter);
            sheet.GetRange("A3", "A3").SetFontStyle(true, null, false, null, false, true);

            sheet.SetCellValue("B5", "THỐNG KÊ SỐ LIỆU TRẺ, NĂM HỌC: " + academicYear.DisplayTitle);
            sheet.SetCellValue("B6", "( Tính đến thời điểm " + stringToday + ")");
            #endregion

            // Lấy danh sách lớp
            #region [Lay Danh Sach Khoi [EducationLevel]]
            List<ClassProfile> queryClassProfiles = ClassProfileBusiness.All.Where(o => o.AcademicYearID == academicYearID
                                                                    && o.SchoolID == schoolID
                                                                    && o.IsActive == true
                                                                    && o.EducationLevel.Grade == appliedLevel)
                                                                    .OrderBy(p => p.EducationLevelID)
                                                                    .ThenBy(u => u.OrderNumber)
                                                                    .ThenBy(u => u.DisplayName).ToList();



            if (educationLevelID == SystemParamsInFile.Combine_Class_ID)
            {
                queryClassProfiles = queryClassProfiles.Where(x => x.IsCombinedClass == true).ToList();
            }
            else if (educationLevelID != 0)
                queryClassProfiles = queryClassProfiles.Where(x => x.IsCombinedClass == false).ToList();

            //Lấy danh sách khối [EducationLevel]
            List<int> educationLevels = queryClassProfiles.Select(p => p.EducationLevelID).Distinct().ToList();
            List<int> lstClassID = queryClassProfiles.Select(p => p.ClassProfileID).Distinct().ToList();

            // Kiểm tra trường có lớp ghép hay không
            bool checkCombineClass = ClassProfileBusiness.CheckIsCombinedClass(schoolID, academicYearID, appliedLevel);

            // Nếu có lớp ghép thêm vào educationLevels
            if ((educationLevelID == 0 || educationLevelID == SystemParamsInFile.Combine_Class_ID) && checkCombineClass)
                educationLevels.Add(SystemParamsInFile.Combine_Class_ID);

            // Lấy educationLevelID khi chọn educationLevelID
            if (educationLevelID > 0)
            {
                educationLevels = educationLevels.Select((v, i) => new { v, i })
                        .Where(x => x.v == educationLevelID)
                        .Select(x => x.v).ToList<int>();
            }
            #endregion


            // Lấy danh sách học sinh
            var queryablePupilOfClass = PupilOfClassBusiness.All.Where(x => x.AcademicYearID == academicYearID && x.SchoolID == schoolID
                && x.ClassProfile.IsActive == true && x.ClassProfile.EducationLevel.Grade == appliedLevel);

            if (educationLevelID > 0 && educationLevelID != SystemParamsInFile.Combine_Class_ID)
            {
                queryablePupilOfClass = queryablePupilOfClass.Where(x => x.ClassProfile.EducationLevelID == educationLevelID);
            }

            var BirthDateOf_PupilOfClassesStudyingByAllClass = queryablePupilOfClass
                .Where(o => o.Status == GlobalConstants.PUPIL_STATUS_STUDYING)
                .Select(x => x.PupilProfile.BirthDate).ToList();

            bool hasNotColumn = false;
            int latestColumn = 13;

            #region [Insert Columns Do Tuoi & Ghi Chu]
            //Lay Danh Sach Do Tuoi Hien Co
            List<int> listChildrenAgeActive = BirthDateOf_PupilOfClassesStudyingByAllClass.Select(BirthDate => academicYear.Year - BirthDate.Year)
                                                .Distinct().OrderBy(o => o).ToList();

            if (listChildrenAgeActive.Count > 0)
            {
                int columnIndex = 13;
                int rowIndex = 9;

                double widthCol = 2.57;
                double minWidthOfDoTuoiColumn = 17;
                int doTuoiColumnCount = (listChildrenAgeActive.Where(x => x < 1).Any() ? 1 : 0) + listChildrenAgeActive.Where(x => x >= 1).Count();
                if (doTuoiColumnCount * widthCol < minWidthOfDoTuoiColumn && doTuoiColumnCount > 0)
                {
                    widthCol = minWidthOfDoTuoiColumn / doTuoiColumnCount;
                }

                for (int i = 0; i < listChildrenAgeActive.Count; i++)
                {
                    if (listChildrenAgeActive[i] < 1)
                    {
                        if (hasNotColumn == false)
                        {
                            sheet.SetCellValue(rowIndex, columnIndex, "<1");
                            sheet.SetColumnWidth(columnIndex, Math.Round(widthCol, 2));
                            hasNotColumn = true;
                            columnIndex++;
                        }
                    }
                    else
                    {
                        sheet.SetCellValue(rowIndex, columnIndex, listChildrenAgeActive[i].ToString());
                        sheet.SetColumnWidth(columnIndex, Math.Round(widthCol, 2));
                        columnIndex++;
                    }
                }

                sheet.SetCellValue(rowIndex - 1, 13, "Độ tuổi trẻ hiện có");
                sheet.SetComment(rowIndex - 1, 13, "Theo độ tuổi từ thấp nhất tới cao nhất của tất cả các trẻ trong khối/cấp học (chỉ tính các trẻ đang học)");
                sheet.GetRange(rowIndex - 1, 13, rowIndex - 1, columnIndex - 1).Merge();

                //Raw Column Ghi Chu
                sheet.SetCellValue(8, columnIndex, "Ghi chú");
                sheet.GetRange(8, columnIndex, 9, columnIndex).Merge();
                //Set Latest Column
                latestColumn = columnIndex;
            }
            else
            {
                //Raw Column Ghi Chu
                sheet.SetCellValue(8, latestColumn, "Ghi chú");
                sheet.GetRange(8, latestColumn, 9, latestColumn).Merge();
            }
            #endregion

            //Raw Data
            #region [Raw Data]
            int EthnicID_Kinh = EthnicBusiness.All.Where(o => o.EthnicCode.Equals(SystemParamsInFile.ETHNIC_CODE_KINH, StringComparison.OrdinalIgnoreCase) && o.IsActive == true).Select(x => x.EthnicID).FirstOrDefault();
            int EthnicID_ForeignPeople = EthnicBusiness.All.Where(o => o.EthnicCode.Equals(SystemParamsInFile.ETHNIC_CODE_NN, StringComparison.OrdinalIgnoreCase) && o.IsActive == true).Select(x => x.EthnicID).FirstOrDefault();
            int startRow = 10;
            int currentRow = startRow;
            int startColumn = 1;
            int siSoDauNamColumnIndex = 4;
            int startColumnSumEducation = 2;
            int currentColumn = startColumn;
            List<int> listSumEducationRow = new List<int>();
            List<ClassProfile> classProfiles;
            List<PupilOfClass> pupilOfClassesByClassId;
            List<PupilOfClass> pupilOfClassesStudyingByClassId;
            List<PupilOfClass> pupilOfClassesLeaveOffByClassId;
            ClassProfile classProfileTemp;

            //Raw By Education
            #region [Raw By Education]
            int soThuTu = 1;
            for (int i = 0; i < educationLevels.Count; i++)
            {
                //Reset currentColumn
                currentColumn = startColumn;
                int startSumEducationRow = currentRow;
                int educationLevelIDTemp = educationLevels[i];
                classProfiles = queryClassProfiles.ToList();

                if (educationLevels[i] != SystemParamsInFile.Combine_Class_ID)
                    classProfiles = queryClassProfiles.Where(x => x.EducationLevelID == educationLevels[i] && x.IsCombinedClass == false).ToList();

                if (educationLevels[i] == SystemParamsInFile.Combine_Class_ID)
                    classProfiles = queryClassProfiles.Where(x => x.IsCombinedClass == true).ToList();

                //Raw Each Row In Education
                if (classProfiles != null)
                {
                    for (int j = 0; j < classProfiles.Count; j++)
                    {
                        currentColumn = startColumn;
                        classProfileTemp = classProfiles[j];

                        //STT
                        sheet.SetCellValue(currentRow, currentColumn++, soThuTu++);

                        //Lop
                        sheet.SetCellValue(currentRow, currentColumn++, classProfileTemp.DisplayName);

                        //Ten GVCN
                        sheet.SetCellValue(currentRow, currentColumn++, classProfileTemp.Employee != null ? classProfileTemp.Employee.FullName : "");

                        //Style Lop & GVCN
                        sheet.GetRange(currentRow, currentColumn - 2, currentRow, currentColumn - 1).SetHAlign(VTHAlign.xlHAlignLeft);
                        sheet.GetRange(currentRow, currentColumn - 2, currentRow, currentColumn - 1).SetHAlignVer(NativeExcel.XlVAlign.xlVAlignCenter);

                        pupilOfClassesByClassId = queryablePupilOfClass.Where(o => o.ClassID == classProfileTemp.ClassProfileID).ToList();

                        //Si So Dau Nam ====:số lượng trẻ không phân biệt trạng thái
                        sheet.SetCellValue(currentRow, currentColumn++, pupilOfClassesByClassId.Count);

                        pupilOfClassesStudyingByClassId = pupilOfClassesByClassId.Where(o => o.Status == GlobalConstants.PUPIL_STATUS_STUDYING).ToList();

                        //So Tre Hien Co ====: số lượng trẻ có trạng thái đang học
                        sheet.SetCellValue(currentRow, currentColumn++, pupilOfClassesStudyingByClassId.Count);

                        //Female ====: số trẻ có trạng thái đang học, giới tính nữ.
                        int sumFemale = pupilOfClassesStudyingByClassId.Where(o => o.PupilProfile.Genre == GlobalConstants.GENRE_FEMALE).Count();
                        sheet.SetCellValue(currentRow, currentColumn++, sumFemale);

                        //Dan Toc ====: số trẻ là dân tộc (khác kinh và người nước ngoài) đang học.
                        int sumDanToc = pupilOfClassesStudyingByClassId.Where(o => o.PupilProfile.EthnicID != null && o.PupilProfile.EthnicID != EthnicID_Kinh && o.PupilProfile.EthnicID != EthnicID_ForeignPeople).Count();
                        sheet.SetCellValue(currentRow, currentColumn++, sumDanToc);

                        //Nữ dân tộc: số trẻ là dân tộc (khác kinh và người nước ngoài) đang học, giới tính nữ.
                        int sumDanTocFemale = pupilOfClassesStudyingByClassId.Where(o => o.PupilProfile.Genre == GlobalConstants.GENRE_FEMALE && o.PupilProfile.EthnicID != null && o.PupilProfile.EthnicID != EthnicID_Kinh && o.PupilProfile.EthnicID != EthnicID_ForeignPeople).Count();
                        sheet.SetCellValue(currentRow, currentColumn++, sumDanTocFemale);

                        pupilOfClassesLeaveOffByClassId = pupilOfClassesByClassId.Where(o => o.Status == GlobalConstants.PUPIL_STATUS_LEAVED_OFF).ToList();

                        //So Tre Bo Hoc > Tong So
                        sheet.SetCellValue(currentRow, currentColumn++, pupilOfClassesLeaveOffByClassId.Count);

                        //So Tre Bo Hoc > Female
                        sumFemale = pupilOfClassesLeaveOffByClassId.Where(o => o.PupilProfile.Genre == GlobalConstants.GENRE_FEMALE).Count();
                        sheet.SetCellValue(currentRow, currentColumn++, sumFemale);

                        //So Tre Bo Hoc > Dan Toc
                        sumDanToc = pupilOfClassesLeaveOffByClassId.Where(o => o.PupilProfile.EthnicID != null && o.PupilProfile.EthnicID != EthnicID_Kinh && o.PupilProfile.EthnicID != EthnicID_ForeignPeople).Count();
                        sheet.SetCellValue(currentRow, currentColumn++, sumDanToc);

                        //So Tre Bo Hoc > Nu Dan Toc
                        sumDanTocFemale = pupilOfClassesLeaveOffByClassId.Where(o => o.PupilProfile.Genre == GlobalConstants.GENRE_FEMALE && o.PupilProfile.EthnicID != null && o.PupilProfile.EthnicID != EthnicID_Kinh && o.PupilProfile.EthnicID != EthnicID_ForeignPeople).Count();
                        sheet.SetCellValue(currentRow, currentColumn++, sumDanTocFemale);

                        //Do Tuoi Tre Hien Co
                        #region [Do Tuoi Tre Hien Co]
                        if (listChildrenAgeActive.Count > 0)
                        {
                            //Do Tuoi Tre Hien Co > "<1"
                            if (hasNotColumn == true)
                            {
                                int sumPupilHasAgeLeftOne = pupilOfClassesStudyingByClassId.Where(o => (academicYear.Year - o.PupilProfile.BirthDate.Year) < 1).Count();
                                sheet.SetCellValue(currentRow, currentColumn++, sumPupilHasAgeLeftOne);
                            }

                            for (int k = 0; k < listChildrenAgeActive.Count(); k++)
                            {
                                if (listChildrenAgeActive[k] >= 1)
                                {
                                    int sumPupilByAgeLoop = pupilOfClassesStudyingByClassId.Where(o => (academicYear.Year - o.PupilProfile.BirthDate.Year) == listChildrenAgeActive[k]).Count();
                                    sheet.SetCellValue(currentRow, currentColumn++, sumPupilByAgeLoop);
                                }
                            }
                        }

                        //Style Cot Ghi Chu
                        sheet.GetRange(currentRow, currentColumn, currentRow, currentColumn).WrapText();
                        sheet.GetRange(currentRow, currentColumn, currentRow, currentColumn).SetHAlign(VTHAlign.xlHAlignLeft);
                        sheet.GetRange(currentRow, currentColumn, currentRow, currentColumn).SetHAlignVer(NativeExcel.XlVAlign.xlVAlignCenter);

                        //Set Format & Style Cot Number
                        sheet.GetRange(currentRow, siSoDauNamColumnIndex, currentRow, currentColumn - 1).NumberFormat("0;-0;-;@");
                        sheet.GetRange(currentRow, siSoDauNamColumnIndex, currentRow, currentColumn - 1).SetHAlign(VTHAlign.xlHAlignCenter);
                        sheet.GetRange(currentRow++, siSoDauNamColumnIndex, currentRow, currentColumn - 1).SetHAlignVer(NativeExcel.XlVAlign.xlVAlignCenter);
                    }
                    #endregion
                }

                //Raw Row Sum Education
                #region [Raw Row Sum Education]
                //Reset currentColumn
                currentColumn = startColumnSumEducation;
                //Get Resolution
                EducationLevel Resolution = EducationLevelBusiness.Find(educationLevelIDTemp);
                sheet.SetCellValue(currentRow, currentColumn++, educationLevelIDTemp == SystemParamsInFile.Combine_Class_ID ? SystemParamsInFile.Combine_Class_Name : Resolution == null ? "" : Resolution.Resolution);

                //Goto Column D
                currentColumn++;

                FillSumFormulaEdu(sheet, currentRow, startSumEducationRow, currentRow - 1, siSoDauNamColumnIndex, latestColumn - 1);
                #endregion

                sheet.GetRange(currentRow, startColumn, currentRow, latestColumn - 1).SetFontStyle(true, null, false, null, false, false);
                sheet.GetRange(currentRow, startColumn, currentRow, latestColumn - 1).NumberFormat("0");
                sheet.GetRange(currentRow, startColumn, currentRow, latestColumn - 1).SetHAlign(VTHAlign.xlHAlignCenter);
                sheet.GetRange(currentRow, startColumn, currentRow, latestColumn - 1).SetHAlignVer(NativeExcel.XlVAlign.xlVAlignCenter);

                listSumEducationRow.Add(currentRow++);

            }
            #endregion


            //Raw Row Sum School
            #region [Raw Row Sum School]
            if (educationLevelID <= 0)
            {
                //Reset currentColumn
                currentColumn = startColumnSumEducation;

                sheet.SetCellValue(currentRow, currentColumn, "Toàn trường");
                FillSumFormulaSchool(sheet, currentRow, listSumEducationRow, 4, latestColumn - 1);
                sheet.GetRange(currentRow, startColumn, currentRow, latestColumn - 1).SetFontStyle(true, null, false, null, false, false);
                sheet.GetRange(currentRow, startColumn, currentRow, latestColumn - 1).NumberFormat("0");
                sheet.GetRange(currentRow, startColumn, currentRow, latestColumn - 1).SetHAlign(VTHAlign.xlHAlignCenter);
                sheet.GetRange(currentRow, startColumn, currentRow, latestColumn - 1).SetHAlignVer(NativeExcel.XlVAlign.xlVAlignCenter);

                sheet.GetRange(currentRow, 2, currentRow, 3).Merge();
            }
            else
            {
                currentRow--;
                sheet.GetRange(currentRow, 2, currentRow, 3).Merge();
            }
            #endregion

            #endregion

            //Config Style Range Data
            #region [Config Style Range Data]
            sheet.GetRange(8, 1, currentRow, latestColumn).SetBorder(VTBorderStyle.Solid, VTBorderIndex.All);
            sheet.GetRange(8, 4, currentRow, latestColumn).SetHAlign(VTHAlign.xlHAlignCenter);

            sheet.GetRange(2, 8, 2, latestColumn).Merge();
            sheet.GetRange(3, 8, 3, latestColumn).Merge();
            sheet.GetRange(5, 2, 5, latestColumn).Merge();
            sheet.GetRange(6, 2, 6, latestColumn).Merge();
            #endregion

            //Raw Signature
            #region [Raw Signature]
            int startColumnSignature = latestColumn - 9;
            currentRow += 2;
            //Raw Address From ProvinceName
            SchoolProfile school = SchoolProfileBusiness.Find(schoolID);
            string provinceAndDate = school.Province.ProvinceName + ", " + stringToday;
            sheet.SetCellValue(currentRow, startColumnSignature, provinceAndDate);
            //Config Style
            sheet.GetRange(currentRow, startColumnSignature, currentRow, latestColumn).Merge();
            sheet.GetRange(currentRow, startColumnSignature, currentRow, latestColumn).SetFontStyle(false, null, true, null, false, false);
            sheet.GetRange(currentRow, startColumnSignature, currentRow, latestColumn).SetHAlign(VTHAlign.xlHAlignCenter);

            //Raw Text Hieu Truong
            currentRow++;
            sheet.SetCellValue(currentRow, startColumnSignature, "Hiệu trưởng");
            //Config Style
            sheet.GetRange(currentRow, startColumnSignature, currentRow, latestColumn).Merge();
            sheet.GetRange(currentRow, startColumnSignature, currentRow, latestColumn).SetFontStyle(true, null, false, null, false, false);
            sheet.GetRange(currentRow, startColumnSignature, currentRow, latestColumn).SetHAlign(VTHAlign.xlHAlignCenter);

            //Raw Ten Hieu Truong
            currentRow += 4;//theo template la +6
            sheet.SetCellValue(currentRow, startColumnSignature, school.HeadMasterName);
            sheet.GetRange(currentRow, startColumnSignature, currentRow, latestColumn).Merge();
            sheet.GetRange(currentRow, startColumnSignature, currentRow, latestColumn).SetFontStyle(true, null, false, null, false, false);
            sheet.GetRange(currentRow, startColumnSignature, currentRow, latestColumn).SetHAlign(VTHAlign.xlHAlignCenter);
            #endregion

            sheet.FitAllColumnsOnOnePage = true;

            return oBook.ToStream();
        }

        public ProcessedReport InsertChildrenByAgeReport(IDictionary<string, object> dic, Stream data, int appliedLevel)
        {
            string reportCode = SystemParamsInFile.THONG_KE_SO_LIEU_TRE;

            ProcessedReport pr = new ProcessedReport();

            ReportDefinition reportDef = ReportDefinitionBusiness.GetByCode(reportCode);
            pr.ReportCode = reportCode;
            pr.ProcessedDate = DateTime.Now;
            pr.InputParameterHashKey = ReportUtils.GetHashKey(dic);
            pr.ReportData = ReportUtils.Compress(data);

            //Tạo tên file
            string outputNamePattern = reportDef.OutputNamePattern;
            string strAppliedLevel = ReportUtils.ConvertAppliedLevelForReportName(appliedLevel);
            outputNamePattern = outputNamePattern.Replace("[AppliedLevel]", ReportUtils.StripVNSign(strAppliedLevel));
            pr.ReportName = ReportUtils.RemoveSpecialCharacters(outputNamePattern) + "." + reportDef.OutputFormat;

            ProcessedReportParameterRepository.Insert(dic, pr);
            ProcessedReportRepository.Insert(pr);
            ProcessedReportRepository.Save();
            return pr;
        }

        public Stream ExportSituationStatisticsBySchool(IDictionary<string, object> dic, List<EducationLevel> lstEducation)
        {
            string template = Path.Combine(SystemParamsInFile.TEMPLATE_FOLDER, "HS", SystemParamsInFile.HS_THCS_TKBiendongHS_112017 + ".xls");
            IVTWorkbook oBook = VTExport.OpenWorkbook(template);
            IVTWorksheet firstSheet = oBook.GetSheet(1);
            IVTWorksheet secondSheet = oBook.GetSheet(2);
            IVTWorksheet thirdSheet = oBook.GetSheet(3);
            IVTWorksheet fourthSheet = oBook.GetSheet(4);

            int schoolID = Utils.GetInt(dic, "SchoolID");
            int academicYearID = Utils.GetInt(dic, "AcademicYearID");
            int? educationLevelID = Utils.GetInt(dic, "EducationLevelID");
            int? appliedLevel = Utils.GetInt(dic, "AppliedLevel");
            int? monthID = Utils.GetInt(dic, "MonthID");
            string superVisingDeptName = Utils.GetString(dic, "SuperVisingDeptName");
            bool showSheetFemale = Utils.GetBool(dic, "SheetPupilFemale");
            bool showSheetEthenic = Utils.GetBool(dic, "SheetPupilEthnic");
            bool showSheetFemaleEthenic = Utils.GetBool(dic, "SheetPupilFemaleEthnic");
            bool fillLeavingReason = false;

            #region // Get data
            if (educationLevelID.HasValue && educationLevelID.Value > 0)
            {
                lstEducation = lstEducation.Where(x => x.EducationLevelID == educationLevelID).ToList();
            }

            List<int> lstEducationLevelID = lstEducation.Select(x => x.EducationLevelID).ToList();

            //lay danh sach lop hoc theo khoi
            IDictionary<string, object> dicClass = new Dictionary<string, object>();
            dicClass.Add("AcademicYearID", academicYearID);
            dicClass.Add("AppliedLevel", appliedLevel);

            List<ClassProfile> lstClassProfile = this.ClassProfileBusiness.SearchBySchool(schoolID, dicClass)
                                                                          .Where(x => lstEducationLevelID.Contains(x.EducationLevelID))
                                                                          .OrderBy(p => p.EducationLevelID).ThenBy(u => u.OrderNumber)
                                                                          .ThenBy(u => u.DisplayName).ToList<ClassProfile>();
            List<int> lstClassID = lstClassProfile.Select(p => p.ClassProfileID).Distinct().ToList();

            AcademicYear objYear = AcademicYearBusiness.Find(academicYearID);
            DateTime startDateOfSchool = new DateTime(objYear.FirstSemesterStartDate.Value.Year, objYear.FirstSemesterStartDate.Value.Month, 1);
            DateTime startContinueDate = startDateOfSchool.AddMonths(1).AddDays(-1);
            DateTime endDateOfSchool = objYear.SecondSemesterEndDate.Value;

            //lay danh sach hoc sinh trong lop
            List<int> lstSchoolID = new List<int>();
            lstSchoolID.Add(schoolID);
            List<int> lstAcademicYearID = new List<int>();
            lstAcademicYearID.Add(academicYearID);
            List<PupilOfClassBO> lstPupilOfClassBO = this.ListPupilOfClassByParameter(lstClassID, lstSchoolID, lstAcademicYearID);
            List<PupilOfClassBO> lstPOCtmp = new List<PupilOfClassBO>();
            List<int> lstPupilID = lstPupilOfClassBO.Select(p => p.PupilID).Distinct().ToList();

            // Danh sach hoc sinh chuyen lop
            List<ClassMovementBO> lstClassMovement = ClassMovementBusiness.ListClassMovement(schoolID, academicYearID).ToList();
            // Danh sach hoc sinh chuyen truong
            List<SchoolMovementBO> lstSchoolMovement = SchoolMovementBusiness.GetListSchoolMovementByParameter(lstSchoolID, lstAcademicYearID, lstPupilID).ToList();
            // danh sach ly do nghi hoc
            List<LeavingReason> lstLeavingReason = LeavingReasonBusiness.All.Where(x => x.IsActive).ToList();
            //Danh sach hoc sinh nghi hoc
            List<PupilLeavingOffBO> lstPupilLeavingOff = PupilLeavingOffBusiness.ListPupilLeavingOff(lstAcademicYearID).ToList();
            #endregion

            DateTime currenDate = DateTime.Now;

            int _month = 0;
            int _year = 0;
            if (monthID.HasValue && monthID.Value > 0)
            {
                string _monthID = monthID.ToString();
                int _length = _monthID.Length;
                string _strMonth = string.Empty;
                string _strYear = string.Empty;

                if (_length == 5)
                    _strMonth = _monthID.Substring(4, 1).ToString();
                else
                    _strMonth = _monthID.Substring(4, 2).ToString();

                _strYear = _monthID.Substring(0, 4).ToString();

                Int32.TryParse(_strMonth, out _month);
                Int32.TryParse(_strYear, out _year);
            }

            int _lastMonth = 0;
            int _lastYear = 0;
            if (_month == 1)
            {
                _lastYear = _year - 1;
                _lastMonth = 12;
            }
            else
            {
                _lastYear = _year;
                _lastMonth = _month - 1;
            }

            DateTime valueMinDate = DateTime.Now;
            DateTime valueMaxDate = DateTime.Now;
            DateTime valueMinLastMonth = new DateTime(_lastYear, _lastMonth, 1);
            DateTime valueMaxLastMonth = valueMinLastMonth.AddMonths(1).AddDays(-1);
            DateTime valueOfMonthSelected = new DateTime(_year, _month, 1);
            DateTime valueOfCurrent = new DateTime(currenDate.Year, currenDate.Month, 1);

            if (valueOfMonthSelected == valueOfCurrent)
            {
                valueMinDate = new DateTime(currenDate.Year, currenDate.Month, 1);
                valueMaxDate = DateTime.Now;
            }
            else
            {
                valueMinDate = new DateTime(_year, _month, 1);
                valueMaxDate = valueMinDate.AddMonths(1).AddDays(-1);
            }

            bool showLastMonth = false;
            if (valueOfMonthSelected == startDateOfSchool)
            {
                showLastMonth = true;
            }

            SchoolProfile objSchool = SchoolProfileBusiness.Find(schoolID);


            string _strDate = string.Format("[District], ngày {0} tháng {1} năm {2}", currenDate.Day, currenDate.Month, currenDate.Year);

            if (objSchool.DistrictID.HasValue)
            {
                District objDistrict = DistrictBusiness.Find(objSchool.DistrictID.Value);
                _strDate = objDistrict != null ? _strDate.Replace("[District]", objDistrict.DistrictName).ToString()
                                        : _strDate.Replace("[District]", ".........").ToString();
            }
            else
            {
                _strDate = _strDate.Replace("[District]", ".........").ToString();
            }

            #region // Fill tieu de
            firstSheet.SetCellValue("A2", superVisingDeptName);
            firstSheet.SetCellValue("A3", objSchool != null ? objSchool.SchoolName : "");
            firstSheet.SetCellValue("E4", _strDate);
            firstSheet.SetCellValue("E6", string.Format("THỐNG KÊ TÌNH HÌNH BIẾN ĐỘNG HỌC SINH THÁNG {0}/{1}", _month, _year));
            firstSheet.SetCellValue("D8", string.Format("Số HS cuối tháng {0}/{1}", _lastMonth, _lastYear));
            firstSheet.SetCellValue("E8", string.Format("Số HS cuối tháng {0}/{1}", _month, _year));
            firstSheet.SetCellValue("F8", string.Format("Số HS chuyển đến tháng {0}/{1}", _month, _year));
            firstSheet.SetCellValue("G8", string.Format("Số HS chuyển đi tháng {0}/{1}", _month, _year));
            firstSheet.SetCellValue("H8", string.Format("Số HS thôi học tháng {0}/{1}", _month, _year));

            if (showSheetFemale)
            {
                secondSheet.SetCellValue("A2", superVisingDeptName);
                secondSheet.SetCellValue("A3", objSchool != null ? objSchool.SchoolName : "");
                secondSheet.SetCellValue("E4", _strDate);
                secondSheet.SetCellValue("E6", string.Format("THỐNG KÊ TÌNH HÌNH BIẾN ĐỘNG HỌC SINH NỮ THÁNG {0}/{1}", _month, _year));
                secondSheet.SetCellValue("D8", string.Format("Số HS cuối tháng {0}/{1}", _lastMonth, _lastYear));
                secondSheet.SetCellValue("E8", string.Format("Số HS cuối tháng {0}/{1}", _month, _year));
                secondSheet.SetCellValue("F8", string.Format("Số HS chuyển đến tháng {0}/{1}", _month, _year));
                secondSheet.SetCellValue("G8", string.Format("Số HS chuyển đi tháng {0}/{1}", _month, _year));
                secondSheet.SetCellValue("H8", string.Format("Số HS thôi học tháng {0}/{1}", _month, _year));
            }

            if (showSheetEthenic)
            {
                thirdSheet.SetCellValue("A2", superVisingDeptName);
                thirdSheet.SetCellValue("A3", objSchool != null ? objSchool.SchoolName : "");
                thirdSheet.SetCellValue("E4", _strDate);
                thirdSheet.SetCellValue("E6", string.Format("THỐNG KÊ TÌNH HÌNH BIẾN ĐỘNG HỌC SINH DÂN TỘC THÁNG {0}/{1}", _month, _year));
                thirdSheet.SetCellValue("D8", string.Format("Số HS cuối tháng {0}/{1}", _lastMonth, _lastYear));
                thirdSheet.SetCellValue("E8", string.Format("Số HS cuối tháng {0}/{1}", _month, _year));
                thirdSheet.SetCellValue("F8", string.Format("Số HS chuyển đến tháng {0}/{1}", _month, _year));
                thirdSheet.SetCellValue("G8", string.Format("Số HS chuyển đi tháng {0}/{1}", _month, _year));
                thirdSheet.SetCellValue("H8", string.Format("Số HS thôi học tháng {0}/{1}", _month, _year));
            }

            if (showSheetFemaleEthenic)
            {
                fourthSheet.SetCellValue("A2", superVisingDeptName);
                fourthSheet.SetCellValue("A3", objSchool != null ? objSchool.SchoolName : "");
                fourthSheet.SetCellValue("E4", _strDate);
                fourthSheet.SetCellValue("E6", string.Format("THỐNG KÊ TÌNH HÌNH BIẾN ĐỘNG HỌC SINH NỮ DÂN TỘC THÁNG {0}/{1}", _month, _year));
                fourthSheet.SetCellValue("D8", string.Format("Số HS cuối tháng {0}/{1}", _lastMonth, _lastYear));
                fourthSheet.SetCellValue("E8", string.Format("Số HS cuối tháng {0}/{1}", _month, _year));
                fourthSheet.SetCellValue("F8", string.Format("Số HS chuyển đến tháng {0}/{1}", _month, _year));
                fourthSheet.SetCellValue("G8", string.Format("Số HS chuyển đi tháng {0}/{1}", _month, _year));
                fourthSheet.SetCellValue("H8", string.Format("Số HS thôi học tháng {0}/{1}", _month, _year));
            }

            if (lstLeavingReason.Count() > 0)
            {
                firstSheet.SetCellValue(8, 9, string.Format("Phân tích số HS thôi học tháng {0}/{1}", _month, _year));
                firstSheet.GetRange(8, 9, 8, 8 + lstLeavingReason.Count()).Merge();
                firstSheet.GetRange(8, 9, 8, 8 + lstLeavingReason.Count()).SetHAlign(VTHAlign.xlHAlignCenter);
                firstSheet.GetRange(8, 9, 8, 8 + lstLeavingReason.Count()).SetFontStyle(true, System.Drawing.Color.Black, false, 11, false, false);

                if (showSheetFemale)
                {
                    secondSheet.SetCellValue(8, 9, string.Format("Phân tích số HS thôi học tháng {0}/{1}", _month, _year));
                    secondSheet.GetRange(8, 9, 8, 8 + lstLeavingReason.Count()).Merge();
                    secondSheet.GetRange(8, 9, 8, 8 + lstLeavingReason.Count()).SetHAlign(VTHAlign.xlHAlignCenter);
                    secondSheet.GetRange(8, 9, 8, 8 + lstLeavingReason.Count()).SetFontStyle(true, System.Drawing.Color.Black, false, 11, false, false);
                }

                if (showSheetEthenic)
                {
                    thirdSheet.SetCellValue(8, 9, string.Format("Phân tích số HS thôi học tháng {0}/{1}", _month, _year));
                    thirdSheet.GetRange(8, 9, 8, 8 + lstLeavingReason.Count()).Merge();
                    thirdSheet.GetRange(8, 9, 8, 8 + lstLeavingReason.Count()).SetHAlign(VTHAlign.xlHAlignCenter);
                    thirdSheet.GetRange(8, 9, 8, 8 + lstLeavingReason.Count()).SetFontStyle(true, System.Drawing.Color.Black, false, 11, false, false);
                }

                if (showSheetFemaleEthenic)
                {
                    fourthSheet.SetCellValue(8, 9, string.Format("Phân tích số HS thôi học tháng {0}/{1}", _month, _year));
                    fourthSheet.GetRange(8, 9, 8, 8 + lstLeavingReason.Count()).Merge();
                    fourthSheet.GetRange(8, 9, 8, 8 + lstLeavingReason.Count()).SetHAlign(VTHAlign.xlHAlignCenter);
                    fourthSheet.GetRange(8, 9, 8, 8 + lstLeavingReason.Count()).SetFontStyle(true, System.Drawing.Color.Black, false, 11, false, false);
                }
            }
            #endregion
            // Selected month = 11/2017
            List<ClassProfile> lstCP = new List<ClassProfile>();
            List<PupilLeavingOffBO> totalFemaleStopLearn = new List<PupilLeavingOffBO>();
            List<PupilLeavingOffBO> totalEthnicStopLearn = new List<PupilLeavingOffBO>();
            List<PupilLeavingOffBO> totalFemaleEthnicStopLearn = new List<PupilLeavingOffBO>();

            string formular = string.Empty;
            string formularAllEducation = string.Empty;
            int startFillEducation = 11;
            int startRow = 11;
            int STT = 0;
            List<int> _lstPupilID = new List<int>();
            IDictionary<int, int> dicTotal = new Dictionary<int, int>();
            ClassProfile objClass = null;
            #region // Fill du lieu
            for (int i = 0; i < lstEducation.Count; i++)
            {
                #region
                firstSheet.SetCellValue(("B" + startRow), lstEducation[i].Resolution);
                if (showSheetFemale)
                {
                    secondSheet.SetCellValue(("B" + startRow), lstEducation[i].Resolution);
                }
                if (showSheetEthenic)
                {
                    thirdSheet.SetCellValue(("B" + startRow), lstEducation[i].Resolution);
                }
                if (showSheetFemaleEthenic)
                {
                    fourthSheet.SetCellValue(("B" + startRow), lstEducation[i].Resolution);
                }
                #endregion

                lstCP = lstClassProfile.Where(x => x.EducationLevelID == lstEducation[i].EducationLevelID).ToList<ClassProfile>();
                for (int j = 0; j < lstCP.Count(); j++)
                {
                    startRow++;
                    STT++;
                    objClass = lstCP.Where(x => x.ClassProfileID == lstCP[j].ClassProfileID).FirstOrDefault();
                    lstPOCtmp = lstPupilOfClassBO.Where(x => x.ClassID == objClass.ClassProfileID)
                                                 .Where(x => x.EnrolmentDate <= objYear.FirstSemesterStartDate).ToList();

                    // Danh sách học sinh chuyển lớp đi trong năm học hiện tại
                    var lstPupilClassMoveGo_FirstSemeter = lstClassMovement.Where(x => x.FromClassID == objClass.ClassProfileID)
                                                                            //.Where(x => x.MovedDate >= valueMinDate && x.MovedDate <= valueMaxDate)
                                                                            .Where(x => x.MovedDate > objYear.FirstSemesterStartDate).ToList();
                    lstPupilID = lstPupilClassMoveGo_FirstSemeter.Select(x => x.PupilID).ToList();
                    var lst1 = lstPOCtmp.Where(x => lstPupilID.Contains(x.PupilID) && x.Status != 1 && x.Status != 4 && x.Status != 5).ToList();
                    if (lst1.Count() > 0)
                    {
                        foreach (var item in lst1)
                        {
                            lstPOCtmp.Remove(item);
                        }
                    }

                    /*// Danh sách học sinh chuyển trường đi trong năm học hiện tại
                    var lstPuilSchoolMovementTo_FirstSemeter = lstSchoolMovement.Where(x => x.MovedDate > objYear.FirstSemesterStartDate)
                                                                                .Where(x => x.SchoolID == schoolID).ToList();
                    lstPupilID = lstPuilSchoolMovementTo_FirstSemeter.Select(x => x.PupilID).ToList();
                    lst1 = lstPOCtmp.Where(x => lstPupilID.Contains(x.PupilID) && x.Status != 1 && x.Status != 4).ToList();
                    if (lst1.Count() > 0)
                    {
                        foreach (var item in lst1)
                        {
                            lstPOCtmp.Remove(item);
                        }
                    }*/

                    #region // Tổng học sinh đầu năm học
                    // fill firstSheet
                    int _countPupil = 0;
                    var totalPupil = lstPOCtmp.ToList();
                    _countPupil = totalPupil.Count();

                    firstSheet.SetCellValue(("A" + startRow), STT);
                    firstSheet.GetRange(startRow, 1, startRow, 1).SetHAlign(VTHAlign.xlHAlignCenter);
                    firstSheet.SetCellValue(("B" + startRow), objClass.DisplayName);
                    firstSheet.SetCellValue(("C" + startRow), _countPupil);

                    #region // Sheet 2
                    if (showSheetFemale)
                    {  // fill secondSheet - tổng số HS nữ
                        totalPupil = lstPOCtmp.Where(x => x.Genre.HasValue && x.Genre.Value == 0).ToList();
                        secondSheet.SetCellValue(("A" + startRow), STT);
                        secondSheet.GetRange(startRow, 1, startRow, 1).SetHAlign(VTHAlign.xlHAlignCenter);
                        secondSheet.SetCellValue(("B" + startRow), objClass.DisplayName);
                        secondSheet.SetCellValue(("C" + startRow), totalPupil.Count());
                    }
                    #endregion

                    #region // Sheet 3
                    if (showSheetEthenic)
                    {// fill thirdSheet - tổng số HS dân tộc - Loại bỏ dân tộc kinh, người nước ngoài
                        totalPupil = lstPOCtmp.Where(x => x.EthnicID.HasValue && (x.EthnicID.Value != 1 && x.EthnicID.Value != 79)).ToList();

                        thirdSheet.SetCellValue(("A" + startRow), STT);
                        thirdSheet.GetRange(startRow, 1, startRow, 1).SetHAlign(VTHAlign.xlHAlignCenter);
                        thirdSheet.SetCellValue(("B" + startRow), objClass.DisplayName);
                        thirdSheet.SetCellValue(("C" + startRow), totalPupil.Count());
                    }
                    #endregion

                    #region // Sheet 4
                    if (showSheetFemaleEthenic)
                    {
                        // fill fourthSheet - tổng số HS nữ dân tộc - Loại bỏ dân tộc kinh, người nước ngoài                  
                        totalPupil = lstPOCtmp.Where(x => x.EthnicID.HasValue && (x.EthnicID.Value != 1 && x.EthnicID.Value != 79))
                                              .Where(x => x.Genre.HasValue && x.Genre == 0).ToList();

                        fourthSheet.SetCellValue(("A" + startRow), STT);
                        fourthSheet.GetRange(startRow, 1, startRow, 1).SetHAlign(VTHAlign.xlHAlignCenter);
                        fourthSheet.SetCellValue(("B" + startRow), objClass.DisplayName);
                        fourthSheet.SetCellValue(("C" + startRow), totalPupil.Count());
                    }
                    #endregion
                    #endregion

                    #region// Tổng học sinh cuối tháng (X - 1)
                    // fill firstSheet                   
                    totalPupil = lstPOCtmp.ToList();
                    var lstPupilInLastMonth = lstPupilOfClassBO.Where(x => x.AssignedDate <= valueMaxLastMonth)
                                                    .Where(x => x.ClassID == objClass.ClassProfileID)
                                                    .Where((x => x.Status == GlobalConstants.PUPIL_STATUS_STUDYING || x.Status == GlobalConstants.PUPIL_STATUS_GRADUATED)).ToList();
                    foreach (var itemLastMonth in lstPupilInLastMonth)
                    {
                        var check = totalPupil.Where(x => x.PupilID == itemLastMonth.PupilID).FirstOrDefault();
                        if (check != null)
                            continue;
                        totalPupil.Add(itemLastMonth);
                    }

                    lstPupilClassMoveGo_FirstSemeter = lstClassMovement.Where(x => x.FromClassID == objClass.ClassProfileID)
                                        .Where(x => x.MovedDate >= valueMinLastMonth && x.MovedDate <= valueMaxLastMonth)
                                        .Where(x => x.MovedDate > objYear.FirstSemesterStartDate).ToList();
                    lstPupilID = lstPupilClassMoveGo_FirstSemeter.Select(x => x.PupilID).ToList();
                    lst1 = lstPupilOfClassBO.Where(x => lstPupilID.Contains(x.PupilID) && x.Status == 5)
                                                .Where(x => x.ClassID == objClass.ClassProfileID).ToList();
                    if (lst1.Count() > 0)
                    {
                        foreach (var item in lst1)
                        {
                            var check = totalPupil.Where(x => x.PupilID == item.PupilID).FirstOrDefault();
                            if (check != null)
                            {
                                totalPupil.Remove(item);
                            }
                        }
                    }


                    var lstPuilSchoolMovementTo_FirstSemeter = lstSchoolMovement.Where(x => x.MovedDate >= valueMinLastMonth && x.MovedDate <= valueMaxLastMonth)
                                                                                .Where(x => x.MovedDate > objYear.FirstSemesterStartDate)
                                                                                .Where(x => x.SchoolID == schoolID).ToList();
                    lstPupilID = lstPuilSchoolMovementTo_FirstSemeter.Select(x => x.PupilID).ToList();
                    lst1 = lstPupilOfClassBO.Where(x => lstPupilID.Contains(x.PupilID) && x.Status == 3).Where(x => x.ClassID == objClass.ClassProfileID).ToList();
                    if (lst1.Count() > 0)
                    {
                        foreach (var item in lst1)
                        {
                            var check = totalPupil.Where(x => x.PupilID == item.PupilID).FirstOrDefault();
                            if (check != null)
                            {
                                totalPupil.Add(item);
                            }
                        }
                    }

                    var pupilStopLearn = lstPupilLeavingOff.Where(x => x.ClassID == objClass.ClassProfileID)
                                                            .Where(x => x.LeavingDate >= valueMinLastMonth && x.LeavingDate <= valueMaxLastMonth).ToList();
                    lstPupilID = pupilStopLearn.Select(x => x.PupilID).ToList();

                    if (lstPupilID.Count() > 0)
                    {
                        totalPupil = totalPupil.Where(x => !lstPupilID.Contains(x.PupilID)).ToList();
                    }

                    lstPupilID = totalPupil.Select(x => x.PupilID).Distinct().ToList();
                    _countPupil = lstPupilID.Count();
                    firstSheet.SetCellValue(("D" + startRow), _countPupil);

                    if (showSheetFemale)
                    {   // fill secondSheet - HS nữ
                        var totalFemale = totalPupil.Where(x => x.Genre.HasValue && x.Genre == 0).ToList();
                        lstPupilID = totalFemale.Select(x => x.PupilID).Distinct().ToList();
                        _countPupil = lstPupilID.Count();

                        secondSheet.SetCellValue(("D" + startRow), _countPupil);
                    }

                    if (showSheetEthenic)
                    {   // fill thirdSheet - HS dân tộc
                        var totalEthnic = totalPupil.Where(x => x.EthnicID.HasValue && (x.EthnicID.Value != 1 && x.EthnicID.Value != 79)).ToList();
                        lstPupilID = totalEthnic.Select(x => x.PupilID).Distinct().ToList();
                        _countPupil = lstPupilID.Count();
                        thirdSheet.SetCellValue(("D" + startRow), _countPupil);
                    }

                    if (showSheetFemaleEthenic)
                    {
                        // fill fourthSheet - HS nữ dân tộc
                        var totalFemaleEthnic = totalPupil.Where(x => x.EthnicID.HasValue && (x.EthnicID.Value != 1 && x.EthnicID.Value != 79)
                                                                 && (x.Genre.HasValue && x.Genre == 0)).ToList();
                        lstPupilID = totalFemaleEthnic.Select(x => x.PupilID).Distinct().ToList();
                        _countPupil = lstPupilID.Count();

                        fourthSheet.SetCellValue(("D" + startRow), _countPupil);
                    }
                    #endregion

                    #region // Tổng học sinh cuối tháng X
                    // fill firstSheet
                    //List<int> lstPupilNew = new List<int>();
                    totalPupil = lstPupilOfClassBO.Where(x => x.AssignedDate <= valueMaxDate)
                                                   .Where(x => x.ClassID == objClass.ClassProfileID)
                                                   .Where((x => x.Status == GlobalConstants.PUPIL_STATUS_STUDYING || x.Status == GlobalConstants.PUPIL_STATUS_GRADUATED)).ToList();

                    _countPupil = totalPupil.Count();
                    firstSheet.SetCellValue(("E" + startRow), _countPupil);
                    if (showSheetFemale)
                    {   // fill secondSheet - HS nữ
                        //lstPupilNew = new List<int>();
                        var totalFemale = totalPupil.Where(x => x.Genre.HasValue && x.Genre == 0).ToList();
                        _countPupil = totalFemale.Count();

                        secondSheet.SetCellValue(("E" + startRow), _countPupil);
                    }

                    if (showSheetEthenic)
                    {   // fill thirdSheet - HS dân tộc                                                                  
                        // lstPupilNew = new List<int>();
                        var totalEthnic = totalPupil.Where(x => x.EthnicID.HasValue && (x.EthnicID.Value != 1 && x.EthnicID.Value != 79)).ToList();
                        _countPupil = totalEthnic.Count();
                        thirdSheet.SetCellValue(("E" + startRow), _countPupil);
                    }

                    if (showSheetFemaleEthenic)
                    {   // fill fourthSheet - HS nữ dân tộc                      
                        // lstPupilNew = new List<int>();
                        var totalFemaleEthnic = totalPupil.Where(x => x.EthnicID.HasValue && (x.EthnicID.Value != 1 && x.EthnicID.Value != 79)
                                                                 && (x.Genre.HasValue && x.Genre == 0)).ToList();
                        _countPupil = totalFemaleEthnic.Count();

                        fourthSheet.SetCellValue(("E" + startRow), _countPupil);
                    }
                    #endregion

                    #region //Số HS chuyển đến tháng X
                    // fill firstSheet
                    // Chuyển lớp
                    var lstPupilClassMoveTo = lstClassMovement.Where(x => x.ToClassID == objClass.ClassProfileID)
                                                            .Where(x => x.MovedDate >= valueMinDate && x.MovedDate <= valueMaxDate).ToList();
                    lstPupilID = lstPupilClassMoveTo.Select(x => x.PupilID).ToList();
                    // Hình thức trúng tuyển (của năm học hiện tại) - Chuyển đến từ trường khác
                    var lstPupilEnrolmentTypeCurrent = lstPupilOfClassBO.Where(x => x.ClassID == objClass.ClassProfileID)
                                                 .Where(x => x.AssignedDate >= valueMinDate && x.AssignedDate <= valueMaxDate)
                                                 .Where(x => x.EnrolmentDate >= objYear.FirstSemesterStartDate)
                                                 .Where(x => x.EnrolmentType.HasValue && x.EnrolmentType == SystemParamsInFile.ENROLMENT_TYPE_MOVED_FROM_OTHER_SCHOOL)
                                                 .Where((x => x.Status == GlobalConstants.PUPIL_STATUS_STUDYING || x.Status == GlobalConstants.PUPIL_STATUS_GRADUATED)).ToList();
                    if (lstPupilID.Count() > 0)
                    {
                        lstPupilEnrolmentTypeCurrent = lstPupilEnrolmentTypeCurrent.Where(x => !lstPupilID.Contains(x.PupilID)).ToList();
                    }
                    //Tổng học sinh chuyển trường đến trong tháng x
                    var totalPuilSchoolMovementTo = lstSchoolMovement.Where(x => x.MovedDate >= valueMinDate && x.MovedDate <= valueMaxDate).ToList();

                    lstPupilID = totalPuilSchoolMovementTo.Select(x => x.PupilID).ToList();
                    if (lstPupilID.Count() > 0)
                    {
                        lstPupilEnrolmentTypeCurrent = lstPupilEnrolmentTypeCurrent.Where(x => !lstPupilID.Contains(x.PupilID)).ToList();
                    }

                    if (totalPuilSchoolMovementTo.Count() > 0)
                    {
                        // trường trong hệ thống
                        var schoolMovementInSystem = totalPuilSchoolMovementTo.Where(x => x.MovedToSchoolID.HasValue && x.MovedToSchoolID == objClass.SchoolID).ToList();
                        if (schoolMovementInSystem.Count() > 0)
                        {
                            lstPupilID = schoolMovementInSystem.Select(x => x.PupilID).ToList();
                            var totalPupilSchoolMovement = lstPupilOfClassBO.Where(x => lstPupilID.Contains(x.PupilID) && x.ClassID == objClass.ClassProfileID).ToList();
                            lstPupilID = lstPupilClassMoveTo.Select(x => x.PupilID).ToList();
                            var totalPupilMoveGo = lstPupilOfClassBO.Where(x => lstPupilID.Contains(x.PupilID) && x.ClassID == objClass.ClassProfileID).ToList();

                            _countPupil = totalPupilMoveGo.Count() + totalPupilSchoolMovement.Count() + lstPupilEnrolmentTypeCurrent.Count();

                        }
                        else
                        {// trường ngoài hệ thống
                            var schoolMovementOutsideSystem = totalPuilSchoolMovementTo.Where(x => x.MovedToClassName == objSchool.SchoolName
                                                                                        && x.MovedToClassName == objClass.DisplayName).ToList();

                            lstPupilID = lstPupilClassMoveTo.Select(x => x.PupilID).ToList();
                            var totalPupilMoveGo = lstPupilOfClassBO.Where(x => lstPupilID.Contains(x.PupilID) && x.ClassID == objClass.ClassProfileID).ToList();
                            _countPupil = totalPupilMoveGo.Count() + lstPupilEnrolmentTypeCurrent.Count();
                        }
                    }
                    else
                    {
                        lstPupilID = lstPupilClassMoveTo.Select(x => x.PupilID).ToList();
                        var totalPupilMoveGo = lstPupilOfClassBO.Where(x => lstPupilID.Contains(x.PupilID) && x.ClassID == objClass.ClassProfileID).ToList();
                        _countPupil = totalPupilMoveGo.Count() + lstPupilEnrolmentTypeCurrent.Count();
                    }

                    firstSheet.SetCellValue(("F" + startRow), _countPupil);

                    #region // fill secondSheet - HS nữ
                    if (showSheetFemale)
                    {
                        var totalFemaleClassMoveTo = lstPupilClassMoveTo.Where(x => x.Genre == 0).ToList();
                        // Hình thức trúng tuyển (của năm học hiện tại) - Chuyển đến từ trường khác
                        var lstPupilFemaleEnrolmentTypeCurrent = lstPupilEnrolmentTypeCurrent.Where(x => x.Genre.HasValue && x.Genre == 0).ToList();
                        lstPupilID = totalFemaleClassMoveTo.Select(x => x.PupilID).ToList();
                        if (lstPupilID.Count() > 0)
                        {
                            lstPupilFemaleEnrolmentTypeCurrent = lstPupilFemaleEnrolmentTypeCurrent.Where(x => !lstPupilID.Contains(x.PupilID)).ToList();
                        }
                        //Tổng học sinh chuyển trường đến trong tháng x
                        var totalPupilFemaleSchoolMovementTo = totalPuilSchoolMovementTo.Where(x => x.Genre == 0).ToList();

                        lstPupilID = totalPupilFemaleSchoolMovementTo.Select(x => x.PupilID).ToList();
                        if (lstPupilID.Count() > 0)
                        {
                            lstPupilFemaleEnrolmentTypeCurrent = lstPupilFemaleEnrolmentTypeCurrent.Where(x => !lstPupilID.Contains(x.PupilID)).ToList();
                        }

                        if (totalPupilFemaleSchoolMovementTo.Count() > 0)
                        {
                            // trường trong hệ thống
                            var schoolMovementInSystem = totalPupilFemaleSchoolMovementTo.Where(x => x.MovedToSchoolID.HasValue && x.MovedToSchoolID == objClass.SchoolID).ToList();
                            if (schoolMovementInSystem.Count() > 0)
                            {
                                lstPupilID = schoolMovementInSystem.Select(x => x.PupilID).ToList();
                                var totalPupilSchoolMovement = lstPupilOfClassBO.Where(x => lstPupilID.Contains(x.PupilID) && x.ClassID == objClass.ClassProfileID)
                                                                                .Where(x => x.Genre.HasValue && x.Genre == 0).ToList();

                                lstPupilID = totalFemaleClassMoveTo.Select(x => x.PupilID).ToList();
                                var totalPupilMoveGo = lstPupilOfClassBO.Where(x => lstPupilID.Contains(x.PupilID) && x.ClassID == objClass.ClassProfileID).ToList();

                                _countPupil = totalPupilMoveGo.Count() + totalPupilSchoolMovement.Count() + lstPupilFemaleEnrolmentTypeCurrent.Count();
                            }
                            else
                            {// trường ngoài hệ thống
                                var schoolMovementOutsideSystem = totalPuilSchoolMovementTo.Where(x => x.MovedToClassName == objSchool.SchoolName
                                                                                            && x.MovedToClassName == objClass.DisplayName).ToList();

                                lstPupilID = totalFemaleClassMoveTo.Select(x => x.PupilID).ToList();
                                var totalPupilMoveGo = lstPupilOfClassBO.Where(x => lstPupilID.Contains(x.PupilID) && x.ClassID == objClass.ClassProfileID).ToList();
                                _countPupil = totalPupilMoveGo.Count() + lstPupilFemaleEnrolmentTypeCurrent.Count();
                            }
                        }
                        else
                        {
                            lstPupilID = totalFemaleClassMoveTo.Select(x => x.PupilID).ToList();
                            var totalPupilMoveGo = lstPupilOfClassBO.Where(x => lstPupilID.Contains(x.PupilID) && x.ClassID == objClass.ClassProfileID).ToList();
                            _countPupil = totalPupilMoveGo.Count() + lstPupilFemaleEnrolmentTypeCurrent.Count();
                        }

                        secondSheet.SetCellValue(("F" + startRow), _countPupil);
                    }
                    #endregion
                    #region // fill thirdSheet - HS dân tộc
                    if (showSheetEthenic)
                    {
                        var totalEthnicClassMoveTo = lstPupilClassMoveTo.Where(x => x.EthnicID.HasValue && (x.EthnicID.Value != 1 && x.EthnicID.Value != 79)).ToList();
                        // Hình thức trúng tuyển (của năm học hiện tại) - Chuyển đến từ trường khác
                        var lstPupilEthnicEnrolmentTypeCurrent = lstPupilEnrolmentTypeCurrent.Where(x => x.EthnicID.HasValue && (x.EthnicID.Value != 1 && x.EthnicID.Value != 79)).ToList();
                        lstPupilID = totalEthnicClassMoveTo.Select(x => x.PupilID).ToList();
                        if (lstPupilID.Count() > 0)
                        {
                            lstPupilEthnicEnrolmentTypeCurrent = lstPupilEthnicEnrolmentTypeCurrent.Where(x => !lstPupilID.Contains(x.PupilID)).ToList();
                        }
                        //Tổng học sinh chuyển trường đến trong tháng x
                        var totalPupilEthnicSchoolMovementTo = totalPuilSchoolMovementTo.Where(x => x.EthnicID.HasValue && (x.EthnicID.Value != 1 && x.EthnicID.Value != 79)).ToList();

                        lstPupilID = totalPupilEthnicSchoolMovementTo.Select(x => x.PupilID).ToList();
                        if (lstPupilID.Count() > 0)
                        {
                            lstPupilEthnicEnrolmentTypeCurrent = lstPupilEthnicEnrolmentTypeCurrent.Where(x => !lstPupilID.Contains(x.PupilID)).ToList();
                        }

                        if (totalPupilEthnicSchoolMovementTo.Count() > 0)
                        {
                            // trường trong hệ thống
                            var schoolMovementInSystem = totalPupilEthnicSchoolMovementTo.Where(x => x.MovedToSchoolID.HasValue && x.MovedToSchoolID == objClass.SchoolID).ToList();
                            if (schoolMovementInSystem.Count() > 0)
                            {
                                lstPupilID = schoolMovementInSystem.Select(x => x.PupilID).ToList();
                                var totalPupilSchoolMovement = lstPupilOfClassBO.Where(x => lstPupilID.Contains(x.PupilID) && x.ClassID == objClass.ClassProfileID)
                                                                        .Where(x => x.EthnicID.HasValue && (x.EthnicID.Value != 1 && x.EthnicID.Value != 79)).ToList();

                                lstPupilID = totalEthnicClassMoveTo.Select(x => x.PupilID).ToList();
                                var totalPupilMoveGo = lstPupilOfClassBO.Where(x => lstPupilID.Contains(x.PupilID) && x.ClassID == objClass.ClassProfileID).ToList();

                                _countPupil = totalPupilMoveGo.Count() + totalPupilSchoolMovement.Count() + lstPupilEthnicEnrolmentTypeCurrent.Count();
                            }
                            else
                            {// trường ngoài hệ thống
                                var schoolMovementOutsideSystem = totalPuilSchoolMovementTo.Where(x => x.MovedToClassName == objSchool.SchoolName
                                                                                            && x.MovedToClassName == objClass.DisplayName).ToList();

                                lstPupilID = totalEthnicClassMoveTo.Select(x => x.PupilID).ToList();
                                var totalPupilMoveGo = lstPupilOfClassBO.Where(x => lstPupilID.Contains(x.PupilID) && x.ClassID == objClass.ClassProfileID).ToList();
                                _countPupil = totalPupilMoveGo.Count() + lstPupilEthnicEnrolmentTypeCurrent.Count();
                            }
                        }
                        else
                        {
                            lstPupilID = totalEthnicClassMoveTo.Select(x => x.PupilID).ToList();
                            var totalPupilMoveGo = lstPupilOfClassBO.Where(x => lstPupilID.Contains(x.PupilID) && x.ClassID == objClass.ClassProfileID).ToList();
                            _countPupil = totalPupilMoveGo.Count() + lstPupilEthnicEnrolmentTypeCurrent.Count();
                        }
                        thirdSheet.SetCellValue(("F" + startRow), _countPupil);
                    }
                    #endregion
                    #region  // fill fourthSheet - HS nữ dân tộc
                    if (showSheetFemaleEthenic)
                    {
                        var totalFemaleEthnicClassMoveTo = lstPupilClassMoveTo.Where(x => x.EthnicID.HasValue && (x.EthnicID.Value != 1 && x.EthnicID.Value != 79))
                                                                            .Where(x => x.Genre == 0).ToList();
                        // Hình thức trúng tuyển (của năm học hiện tại) - Chuyển đến từ trường khác
                        var lstPupilEthnicEnrolmentTypeCurrent = lstPupilEnrolmentTypeCurrent.Where(x => x.EthnicID.HasValue && (x.EthnicID.Value != 1 && x.EthnicID.Value != 79))
                                                                .Where(x => x.Genre.HasValue && x.Genre == 0).ToList();
                        lstPupilID = totalFemaleEthnicClassMoveTo.Select(x => x.PupilID).ToList();
                        if (lstPupilID.Count() > 0)
                        {
                            lstPupilEthnicEnrolmentTypeCurrent = lstPupilEthnicEnrolmentTypeCurrent.Where(x => !lstPupilID.Contains(x.PupilID)).ToList();
                        }
                        //Tổng học sinh chuyển trường đến trong tháng x
                        var totalPupilFemaleEthnicSchoolMovementTo = totalPuilSchoolMovementTo.Where(x => x.EthnicID.HasValue && (x.EthnicID.Value != 1 && x.EthnicID.Value != 79))
                                                                                                .Where(x => x.Genre == 0).ToList();
                        lstPupilID = totalPupilFemaleEthnicSchoolMovementTo.Select(x => x.PupilID).ToList();
                        if (lstPupilID.Count() > 0)
                        {
                            lstPupilEthnicEnrolmentTypeCurrent = lstPupilEthnicEnrolmentTypeCurrent.Where(x => !lstPupilID.Contains(x.PupilID)).ToList();
                        }

                        if (totalPupilFemaleEthnicSchoolMovementTo.Count() > 0)
                        {
                            // trường trong hệ thống
                            var schoolMovementInSystem = totalPupilFemaleEthnicSchoolMovementTo.Where(x => x.MovedToSchoolID.HasValue && x.MovedToSchoolID == objClass.SchoolID).ToList();
                            if (schoolMovementInSystem.Count() > 0)
                            {
                                lstPupilID = schoolMovementInSystem.Select(x => x.PupilID).ToList();
                                var totalPupilSchoolMovement = lstPupilOfClassBO.Where(x => lstPupilID.Contains(x.PupilID) && x.ClassID == objClass.ClassProfileID)
                                                                        .Where(x => x.EthnicID.HasValue && (x.EthnicID.Value != 1 && x.EthnicID.Value != 79)).ToList();

                                lstPupilID = totalFemaleEthnicClassMoveTo.Select(x => x.PupilID).ToList();
                                var totalPupilMoveGo = lstPupilOfClassBO.Where(x => lstPupilID.Contains(x.PupilID) && x.ClassID == objClass.ClassProfileID).ToList();

                                _countPupil = totalPupilMoveGo.Count() + totalPupilSchoolMovement.Count() + lstPupilEthnicEnrolmentTypeCurrent.Count();
                            }
                            else
                            {// trường ngoài hệ thống
                                var schoolMovementOutsideSystem = totalPuilSchoolMovementTo.Where(x => x.MovedToClassName == objSchool.SchoolName
                                                                                            && x.MovedToClassName == objClass.DisplayName).ToList();

                                lstPupilID = totalFemaleEthnicClassMoveTo.Select(x => x.PupilID).ToList();
                                var totalPupilMoveGo = lstPupilOfClassBO.Where(x => lstPupilID.Contains(x.PupilID) && x.ClassID == objClass.ClassProfileID).ToList();
                                _countPupil = totalPupilMoveGo.Count() + lstPupilEthnicEnrolmentTypeCurrent.Count();
                            }
                        }
                        else
                        {
                            lstPupilID = totalFemaleEthnicClassMoveTo.Select(x => x.PupilID).ToList();
                            var totalPupilMoveGo = lstPupilOfClassBO.Where(x => lstPupilID.Contains(x.PupilID) && x.ClassID == objClass.ClassProfileID).ToList();
                            _countPupil = totalPupilMoveGo.Count() + lstPupilEthnicEnrolmentTypeCurrent.Count();
                        }

                        fourthSheet.SetCellValue(("F" + startRow), _countPupil);
                    }
                    #endregion
                    #endregion

                    #region// Số hoc sinh chuyen di thang X
                    List<int> lstnewPupilID = new List<int>();
                    // fill firstSheet
                    var lstPupilClassMoveFrom = lstClassMovement.Where(x => x.FromClassID == objClass.ClassProfileID)
                                                           .Where(x => x.MovedDate >= valueMinDate && x.MovedDate <= valueMaxDate).ToList();
                    lstPupilID = lstPupilClassMoveFrom.Select(x => x.PupilID).ToList();
                    if (lstPupilID.Count() > 0)
                    {
                        lstnewPupilID.AddRange(lstPupilID);
                    }
                    //Tổng học sinh chuyển trường đi trong tháng x
                    totalPuilSchoolMovementTo = lstSchoolMovement.Where(x => x.ClassID == objClass.ClassProfileID)
                                                                 .Where(x => x.MovedDate >= valueMinDate && x.MovedDate <= valueMaxDate).ToList();
                    lstPupilID = totalPuilSchoolMovementTo.Select(x => x.PupilID).ToList();
                    if (lstPupilID.Count() > 0)
                    {
                        lstnewPupilID.AddRange(lstPupilID);
                    }
                    lstnewPupilID = lstnewPupilID.Distinct().ToList();
                    var totalPupilMoveGoOnMonth = lstPupilOfClassBO.Where(x => lstnewPupilID.Contains(x.PupilID) && x.ClassID == objClass.ClassProfileID).ToList();

                    firstSheet.SetCellValue(("G" + startRow), totalPupilMoveGoOnMonth.Count());

                    if (showSheetFemale)
                    {   // fill secondSheet - HS nữ
                        lstnewPupilID = new List<int>();
                        var totalFemaleClassMoveFrom = lstPupilClassMoveFrom.Where(x => x.Genre == 0).ToList();
                        lstPupilID = totalFemaleClassMoveFrom.Select(x => x.PupilID).ToList();
                        if (lstPupilID.Count() > 0)
                        {
                            lstnewPupilID.AddRange(lstPupilID);
                        }
                        totalPuilSchoolMovementTo = lstSchoolMovement.Where(x => x.ClassID == objClass.ClassProfileID)
                                                               .Where(x => x.MovedDate >= valueMinDate && x.MovedDate <= valueMaxDate)
                                                               .Where(x => x.Genre == 0).ToList();
                        lstPupilID = totalPuilSchoolMovementTo.Select(x => x.PupilID).ToList();
                        if (lstPupilID.Count() > 0)
                        {
                            lstnewPupilID.AddRange(lstPupilID);
                        }
                        lstnewPupilID = lstnewPupilID.Distinct().ToList();
                        totalPupilMoveGoOnMonth = lstPupilOfClassBO.Where(x => lstnewPupilID.Contains(x.PupilID) && x.ClassID == objClass.ClassProfileID).ToList();

                        secondSheet.SetCellValue(("G" + startRow), totalPupilMoveGoOnMonth.Count());
                    }
                    if (showSheetEthenic)
                    {   // fill thirdSheet - HS dân tộc
                        lstnewPupilID = new List<int>();
                        var totalEthnicClassMoveFrom = lstPupilClassMoveFrom.Where(x => x.EthnicID.HasValue && (x.EthnicID.Value != 1 && x.EthnicID.Value != 79))
                                                                            .Where(x => x.FromClassID == objClass.ClassProfileID).ToList();

                        lstPupilID = totalEthnicClassMoveFrom.Select(x => x.PupilID).ToList();
                        if (lstPupilID.Count() > 0)
                        {
                            lstnewPupilID.AddRange(lstPupilID);
                        }
                        totalPuilSchoolMovementTo = lstSchoolMovement.Where(x => x.ClassID == objClass.ClassProfileID)
                                                              .Where(x => x.MovedDate >= valueMinDate && x.MovedDate <= valueMaxDate)
                                                              .Where(x => x.EthnicID.HasValue && (x.EthnicID.Value != 1 && x.EthnicID.Value != 79)).ToList();
                        lstPupilID = totalPuilSchoolMovementTo.Select(x => x.PupilID).ToList();
                        if (lstPupilID.Count() > 0)
                        {
                            lstnewPupilID.AddRange(lstPupilID);
                        }
                        lstnewPupilID = lstnewPupilID.Distinct().ToList();
                        totalPupilMoveGoOnMonth = lstPupilOfClassBO.Where(x => lstnewPupilID.Contains(x.PupilID) && x.ClassID == objClass.ClassProfileID).ToList();

                        thirdSheet.SetCellValue(("G" + startRow), totalPupilMoveGoOnMonth.Count());
                    }
                    if (showSheetFemaleEthenic)
                    {   // fill fourthSheet - HS nữ dân tộc
                        lstnewPupilID = new List<int>();
                        var totalFemaleEthnicClassMoveFrom = lstPupilClassMoveFrom.Where(x => x.EthnicID.HasValue && (x.EthnicID.Value != 1 && x.EthnicID.Value != 79)
                                                                 && (x.Genre == 0)).Where(x => x.FromClassID == objClass.ClassProfileID).ToList();
                        lstPupilID = totalFemaleEthnicClassMoveFrom.Select(x => x.PupilID).ToList();
                        if (lstPupilID.Count() > 0)
                        {
                            lstnewPupilID.AddRange(lstPupilID);
                        }
                        totalPuilSchoolMovementTo = lstSchoolMovement.Where(x => x.ClassID == objClass.ClassProfileID)
                                                              .Where(x => x.MovedDate >= valueMinDate && x.MovedDate <= valueMaxDate)
                                                              .Where(x => x.Genre == 0)
                                                              .Where(x => x.EthnicID.HasValue && (x.EthnicID.Value != 1 && x.EthnicID.Value != 79)).ToList();
                        lstPupilID = totalPuilSchoolMovementTo.Select(x => x.PupilID).ToList();
                        if (lstPupilID.Count() > 0)
                        {
                            lstnewPupilID.AddRange(lstPupilID);
                        }
                        lstnewPupilID = lstnewPupilID.Distinct().ToList();
                        totalPupilMoveGoOnMonth = lstPupilOfClassBO.Where(x => lstnewPupilID.Contains(x.PupilID) && x.ClassID == objClass.ClassProfileID).ToList();

                        fourthSheet.SetCellValue(("G" + startRow), totalPupilMoveGoOnMonth.Count());
                    }
                    #endregion

                    #region// Số hoc sinh thoi hoc thang X
                    // fill firstSheet
                    lstnewPupilID = new List<int>();
                    var totalPupilStopLearn = lstPupilLeavingOff.Where(x => x.LeavingDate >= valueMinDate && x.LeavingDate <= valueMaxDate
                                                                && x.ClassID == objClass.ClassProfileID).ToList();
                    lstPupilID = totalPupilStopLearn.Select(x => x.PupilID).ToList();
                    if (lstPupilID.Count() > 0)
                    {
                        lstnewPupilID.AddRange(lstPupilID);
                    }
                    var totalPupilStopLearnInMonth = lstPupilOfClassBO.Where(x => lstnewPupilID.Contains(x.PupilID)).ToList();

                    firstSheet.SetCellValue(("H" + startRow), totalPupilStopLearnInMonth.Count());

                    if (showSheetFemale)
                    {   // fill secondSheet - HS nữ                      
                        lstnewPupilID = new List<int>();
                        totalFemaleStopLearn = totalPupilStopLearn.Where(x => x.Genre == 0).ToList();
                        lstPupilID = totalFemaleStopLearn.Select(x => x.PupilID).ToList();
                        if (lstPupilID.Count() > 0)
                        {
                            lstnewPupilID.AddRange(lstPupilID);
                        }
                        totalPupilStopLearnInMonth = lstPupilOfClassBO.Where(x => lstnewPupilID.Contains(x.PupilID)).ToList();

                        secondSheet.SetCellValue(("H" + startRow), totalPupilStopLearnInMonth.Count());
                    }
                    if (showSheetEthenic)
                    {   // fill thirdSheet - HS dân tộc                       
                        lstnewPupilID = new List<int>();
                        totalEthnicStopLearn = totalPupilStopLearn.Where(x => x.EthnicID.HasValue && (x.EthnicID.Value != 1 && x.EthnicID.Value != 79)).ToList();
                        lstPupilID = totalEthnicStopLearn.Select(x => x.PupilID).ToList();
                        if (lstPupilID.Count() > 0)
                        {
                            lstnewPupilID.AddRange(lstPupilID);
                        }
                        totalPupilStopLearnInMonth = lstPupilOfClassBO.Where(x => lstnewPupilID.Contains(x.PupilID)).ToList();
                        thirdSheet.SetCellValue(("H" + startRow), totalPupilStopLearnInMonth.Count());
                    }
                    if (showSheetFemaleEthenic)
                    {   // fill fourthSheet - HS nữ dân tộc
                        lstnewPupilID = new List<int>();
                        totalFemaleEthnicStopLearn = totalPupilStopLearn.Where(x => x.EthnicID.HasValue && (x.EthnicID.Value != 1 && x.EthnicID.Value != 79)
                                                                                    && (x.Genre == 0)).ToList();
                        lstPupilID = totalFemaleEthnicStopLearn.Select(x => x.PupilID).ToList();
                        if (lstPupilID.Count() > 0)
                        {
                            lstnewPupilID.AddRange(lstPupilID);
                        }
                        totalPupilStopLearnInMonth = lstPupilOfClassBO.Where(x => lstnewPupilID.Contains(x.PupilID)).ToList();
                        fourthSheet.SetCellValue(("H" + startRow), totalPupilStopLearnInMonth.Count());
                    }
                    #endregion

                    #region // Lý do nghĩ học
                    int p = 0;
                    foreach (var itemReason in lstLeavingReason)
                    {

                        if (!fillLeavingReason)
                        {
                            firstSheet.SetCellValue(9, (9 + p), itemReason.Resolution);
                            firstSheet.GetRange(9, (9 + p), 9, (9 + p)).SetHAlign(VTHAlign.xlHAlignCenter);
                            firstSheet.GetRange(9, (9 + p), 9, (9 + p)).WrapText();
                            firstSheet.GetRange(9, (9 + p), 9, (9 + p)).SetHAlignVer(NativeExcel.XlVAlign.xlVAlignCenter);
                            firstSheet.GetRange(9, (9 + p), 9, (9 + p)).SetFontStyle(true, System.Drawing.Color.Black, false, 10, false, false);
                            firstSheet.SetColumnWidth((9 + p), 12);

                            if (showSheetFemale)
                            {
                                secondSheet.SetCellValue(9, (9 + p), itemReason.Resolution);
                                secondSheet.GetRange(9, (9 + p), 9, (9 + p)).SetHAlign(VTHAlign.xlHAlignCenter);
                                secondSheet.GetRange(9, (9 + p), 9, (9 + p)).WrapText();
                                secondSheet.GetRange(9, (9 + p), 9, (9 + p)).SetHAlignVer(NativeExcel.XlVAlign.xlVAlignCenter);
                                secondSheet.GetRange(9, (9 + p), 9, (9 + p)).SetFontStyle(true, System.Drawing.Color.Black, false, 10, false, false);
                                secondSheet.SetColumnWidth((9 + p), 12);
                            }

                            if (showSheetEthenic)
                            {
                                thirdSheet.SetCellValue(9, (9 + p), itemReason.Resolution);
                                thirdSheet.GetRange(9, (9 + p), 9, (9 + p)).SetHAlign(VTHAlign.xlHAlignCenter);
                                thirdSheet.GetRange(9, (9 + p), 9, (9 + p)).WrapText();
                                thirdSheet.GetRange(9, (9 + p), 9, (9 + p)).SetHAlignVer(NativeExcel.XlVAlign.xlVAlignCenter);
                                thirdSheet.GetRange(9, (9 + p), 9, (9 + p)).SetFontStyle(true, System.Drawing.Color.Black, false, 10, false, false);
                                thirdSheet.SetColumnWidth((9 + p), 12);
                            }

                            if (showSheetFemaleEthenic)
                            {
                                fourthSheet.SetCellValue(9, (9 + p), itemReason.Resolution);
                                fourthSheet.GetRange(9, (9 + p), 9, (9 + p)).SetHAlign(VTHAlign.xlHAlignCenter);
                                fourthSheet.GetRange(9, (9 + p), 9, (9 + p)).WrapText();
                                fourthSheet.GetRange(9, (9 + p), 9, (9 + p)).SetHAlignVer(NativeExcel.XlVAlign.xlVAlignCenter);
                                fourthSheet.GetRange(9, (9 + p), 9, (9 + p)).SetFontStyle(true, System.Drawing.Color.Black, false, 10, false, false);
                                fourthSheet.SetColumnWidth((9 + p), 12);
                            }
                        }

                        var totalPupilStopByReason = totalPupilStopLearn.Where(x => x.LeavingReasonID == itemReason.LeavingReasonID);
                        lstPupilID = totalPupilStopByReason.Select(x => x.PupilID).ToList();
                        totalPupilStopLearnInMonth = lstPupilOfClassBO.Where(x => lstPupilID.Contains(x.PupilID)).ToList();
                        firstSheet.SetCellValue(startRow, (9 + p), totalPupilStopLearnInMonth.Count());

                        if (showSheetFemale)
                        {
                            var totalFemaleStopByReason = totalFemaleStopLearn.Where(x => x.LeavingReasonID == itemReason.LeavingReasonID);
                            lstPupilID = totalFemaleStopByReason.Select(x => x.PupilID).ToList();
                            totalPupilStopLearnInMonth = lstPupilOfClassBO.Where(x => lstPupilID.Contains(x.PupilID)).ToList();
                            secondSheet.SetCellValue(startRow, (9 + p), totalPupilStopLearnInMonth.Count());
                        }
                        if (showSheetEthenic)
                        {
                            var totalEthnicStopByReason = totalEthnicStopLearn.Where(x => x.LeavingReasonID == itemReason.LeavingReasonID);
                            lstPupilID = totalEthnicStopByReason.Select(x => x.PupilID).ToList();
                            totalPupilStopLearnInMonth = lstPupilOfClassBO.Where(x => lstPupilID.Contains(x.PupilID)).ToList();
                            thirdSheet.SetCellValue(startRow, (9 + p), totalPupilStopLearnInMonth.Count());
                        }
                        if (showSheetFemaleEthenic)
                        {
                            var totalFemaleEthnicStopByReason = totalFemaleEthnicStopLearn.Where(x => x.LeavingReasonID == itemReason.LeavingReasonID);
                            lstPupilID = totalFemaleEthnicStopByReason.Select(x => x.PupilID).ToList();
                            totalPupilStopLearnInMonth = lstPupilOfClassBO.Where(x => lstPupilID.Contains(x.PupilID)).ToList();
                            fourthSheet.SetCellValue(startRow, (9 + p), totalPupilStopLearnInMonth.Count());
                        }
                        p++;
                    }

                    if (fillLeavingReason == false)
                    {
                        fillLeavingReason = true;
                    }
                    #endregion
                }

                #region // Tính tổng
                if (lstCP.Count > 0)
                {
                    // Tổng học sinh đầu năm học
                    formular = "=SUM(C" + (startFillEducation + 1) + ":C" + (startRow) + ")";
                    firstSheet.SetCellValue(startFillEducation, 3, formular);
                    if (showSheetFemale)
                    {
                        secondSheet.SetCellValue(startFillEducation, 3, formular);
                    }
                    if (showSheetEthenic)
                    {
                        thirdSheet.SetCellValue(startFillEducation, 3, formular);
                    }
                    if (showSheetFemaleEthenic)
                    {
                        fourthSheet.SetCellValue(startFillEducation, 3, formular);
                    }

                    //Số HS cuối tháng (x- 1)
                    formular = "=SUM(D" + (startFillEducation + 1) + ":D" + (startRow) + ")";
                    firstSheet.SetCellValue(startFillEducation, 4, formular);
                    if (showSheetFemale)
                    {
                        secondSheet.SetCellValue(startFillEducation, 4, formular);
                    }
                    if (showSheetEthenic)
                    {
                        thirdSheet.SetCellValue(startFillEducation, 4, formular);
                    }
                    if (showSheetFemaleEthenic)
                    {
                        fourthSheet.SetCellValue(startFillEducation, 4, formular);
                    }
                    //Số HS cuối tháng (x)
                    formular = "=SUM(E" + (startFillEducation + 1) + ":E" + (startRow) + ")";
                    firstSheet.SetCellValue(startFillEducation, 5, formular);
                    if (showSheetFemale)
                    {
                        secondSheet.SetCellValue(startFillEducation, 5, formular);
                    }
                    if (showSheetEthenic)
                    {
                        thirdSheet.SetCellValue(startFillEducation, 5, formular);
                    }
                    if (showSheetFemaleEthenic)
                    {
                        fourthSheet.SetCellValue(startFillEducation, 5, formular);
                    }
                    //Số HS chuyển đến tháng (x)
                    formular = "=SUM(F" + (startFillEducation + 1) + ":F" + (startRow) + ")";
                    firstSheet.SetCellValue(startFillEducation, 6, formular);
                    if (showSheetFemale)
                    {
                        secondSheet.SetCellValue(startFillEducation, 6, formular);
                    }
                    if (showSheetEthenic)
                    {
                        thirdSheet.SetCellValue(startFillEducation, 6, formular);
                    }
                    if (showSheetFemaleEthenic)
                    {
                        fourthSheet.SetCellValue(startFillEducation, 6, formular);
                    }

                    //Số HS chuyển đi tháng (x)
                    formular = "=SUM(G" + (startFillEducation + 1) + ":G" + (startRow) + ")";
                    firstSheet.SetCellValue(startFillEducation, 7, formular);
                    if (showSheetFemale)
                    {
                        secondSheet.SetCellValue(startFillEducation, 7, formular);
                    }
                    if (showSheetEthenic)
                    {
                        thirdSheet.SetCellValue(startFillEducation, 7, formular);
                    }
                    if (showSheetFemaleEthenic)
                    {
                        fourthSheet.SetCellValue(startFillEducation, 7, formular);
                    }
                    //Số HS thôi học tháng (x)
                    formular = "=SUM(H" + (startFillEducation + 1) + ":H" + (startRow) + ")";
                    firstSheet.SetCellValue(startFillEducation, 8, formular);
                    if (showSheetFemale)
                    {
                        secondSheet.SetCellValue(startFillEducation, 8, formular);
                    }
                    if (showSheetEthenic)
                    {
                        thirdSheet.SetCellValue(startFillEducation, 8, formular);
                    }
                    if (showSheetFemaleEthenic)
                    {
                        fourthSheet.SetCellValue(startFillEducation, 8, formular);
                    }

                    string alpha = string.Empty;
                    for (int k = 9; k <= (8 + lstLeavingReason.Count()); k++)
                    {
                        alpha = UtilsBusiness.GetExcelColumnName(k);
                        formular = "=SUM(" + alpha + (startFillEducation + 1) + ":" + alpha + (startRow) + ")";
                        firstSheet.SetCellValue(startFillEducation, k, formular);
                        if (showSheetFemale)
                        {
                            secondSheet.SetCellValue(startFillEducation, k, formular);
                        }
                        if (showSheetEthenic)
                        {
                            thirdSheet.SetCellValue(startFillEducation, k, formular);
                        }
                        if (showSheetFemaleEthenic)
                        {
                            fourthSheet.SetCellValue(startFillEducation, k, formular);
                        }
                        alpha = string.Empty;
                    }
                    dicTotal[dicTotal.Count()] = startFillEducation;
                }
                else
                {
                    for (int k = 3; k <= (8 + lstLeavingReason.Count()); k++)
                    {
                        firstSheet.SetCellValue(startFillEducation, k, "0");

                        if (showSheetFemale)
                        {
                            secondSheet.SetCellValue(startFillEducation, k, "0");
                        }
                        if (showSheetEthenic)
                        {
                            thirdSheet.SetCellValue(startFillEducation, k, "0");
                        }
                        if (showSheetFemaleEthenic)
                        {
                            fourthSheet.SetCellValue(startFillEducation, k, "0");
                        }
                    }
                }
                #endregion

                firstSheet.GetRange(startFillEducation, 1, startFillEducation, (8 + lstLeavingReason.Count())).SetFontStyle(true, System.Drawing.Color.Black, false, 11, false, false);
                firstSheet.GetRange(startFillEducation, 1, startFillEducation, (8 + lstLeavingReason.Count())).FillColor(System.Drawing.Color.LightYellow);

                if (showSheetFemale)
                {
                    secondSheet.GetRange(startFillEducation, 1, startFillEducation, (8 + lstLeavingReason.Count())).SetFontStyle(true, System.Drawing.Color.Black, false, 11, false, false);
                    secondSheet.GetRange(startFillEducation, 1, startFillEducation, (8 + lstLeavingReason.Count())).FillColor(System.Drawing.Color.LightYellow);
                }
                if (showSheetEthenic)
                {
                    thirdSheet.GetRange(startFillEducation, 1, startFillEducation, (8 + lstLeavingReason.Count())).SetFontStyle(true, System.Drawing.Color.Black, false, 11, false, false);
                    thirdSheet.GetRange(startFillEducation, 1, startFillEducation, (8 + lstLeavingReason.Count())).FillColor(System.Drawing.Color.LightYellow);
                }
                if (showSheetFemaleEthenic)
                {
                    fourthSheet.GetRange(startFillEducation, 1, startFillEducation, (8 + lstLeavingReason.Count())).SetFontStyle(true, System.Drawing.Color.Black, false, 11, false, false);
                    fourthSheet.GetRange(startFillEducation, 1, startFillEducation, (8 + lstLeavingReason.Count())).FillColor(System.Drawing.Color.LightYellow);
                }

                startFillEducation += (lstCP.Count + 1);
                startRow++;
            }

            #region
            firstSheet.GetRange(10, 3, (startRow - 1), (8 + lstLeavingReason.Count())).SetHAlign(VTHAlign.xlHAlignCenter);
            firstSheet.GetRange(8, 1, (startRow - 1), (8 + lstLeavingReason.Count())).SetBorder(VTBorderStyle.Solid, VTBorderWeight.Thin, VTBorderIndex.All);

            if (showSheetFemale)
            {
                secondSheet.GetRange(10, 3, (startRow - 1), (8 + lstLeavingReason.Count())).SetHAlign(VTHAlign.xlHAlignCenter);
                secondSheet.GetRange(8, 1, (startRow - 1), (8 + lstLeavingReason.Count())).SetBorder(VTBorderStyle.Solid, VTBorderWeight.Thin, VTBorderIndex.All);
            }
            if (showSheetEthenic)
            {
                thirdSheet.GetRange(10, 3, (startRow - 1), (8 + lstLeavingReason.Count())).SetHAlign(VTHAlign.xlHAlignCenter);
                thirdSheet.GetRange(8, 1, (startRow - 1), (8 + lstLeavingReason.Count())).SetBorder(VTBorderStyle.Solid, VTBorderWeight.Thin, VTBorderIndex.All);
            }
            if (showSheetFemaleEthenic)
            {
                fourthSheet.GetRange(10, 3, (startRow - 1), (8 + lstLeavingReason.Count())).SetHAlign(VTHAlign.xlHAlignCenter);
                fourthSheet.GetRange(8, 1, (startRow - 1), (8 + lstLeavingReason.Count())).SetBorder(VTBorderStyle.Solid, VTBorderWeight.Thin, VTBorderIndex.All);
            }
            #endregion

            #region // Toàn trường
            if (educationLevelID.HasValue && educationLevelID.Value > 0)
            {
                firstSheet.DeleteRow(10);
                if (showSheetFemale)
                {
                    secondSheet.DeleteRow(10);
                }
                if (showSheetEthenic)
                {
                    thirdSheet.DeleteRow(10);
                }
                if (showSheetFemaleEthenic)
                {
                    fourthSheet.DeleteRow(10);
                }
            }
            else
            {
                if (lstEducation.Count > 0)
                {
                    //fill toan truong
                    firstSheet.SetCellValue("A10", "Toàn trường");
                    firstSheet.GetRange(10, 1, 10, 2).Merge();
                    firstSheet.GetRange(10, 1, 10, 2).SetHAlign(VTHAlign.xlHAlignCenter);
                    firstSheet.GetRange(10, 1, 10, (8 + lstLeavingReason.Count())).FillColor(System.Drawing.Color.LightYellow);
                    firstSheet.GetRange(10, 1, 10, (8 + lstLeavingReason.Count())).SetFontStyle(true, System.Drawing.Color.Black, false, 11, false, false);

                    if (showSheetFemale)
                    {
                        secondSheet.SetCellValue("A10", "Toàn trường");
                        secondSheet.GetRange(10, 1, 10, 2).Merge();
                        secondSheet.GetRange(10, 1, 10, 2).SetHAlign(VTHAlign.xlHAlignCenter);
                        secondSheet.GetRange(10, 1, 10, (8 + lstLeavingReason.Count())).FillColor(System.Drawing.Color.LightYellow);
                        secondSheet.GetRange(10, 1, 10, (8 + lstLeavingReason.Count())).SetFontStyle(true, System.Drawing.Color.Black, false, 11, false, false);
                    }
                    if (showSheetEthenic)
                    {
                        thirdSheet.SetCellValue("A10", "Toàn trường");
                        thirdSheet.GetRange(10, 1, 10, 2).Merge();
                        thirdSheet.GetRange(10, 1, 10, 2).SetHAlign(VTHAlign.xlHAlignCenter);
                        thirdSheet.GetRange(10, 1, 10, (8 + lstLeavingReason.Count())).FillColor(System.Drawing.Color.LightYellow);
                        thirdSheet.GetRange(10, 1, 10, (8 + lstLeavingReason.Count())).SetFontStyle(true, System.Drawing.Color.Black, false, 11, false, false);
                    }
                    if (showSheetFemaleEthenic)
                    {
                        fourthSheet.SetCellValue("A10", "Toàn trường");
                        fourthSheet.GetRange(10, 1, 10, 2).Merge();
                        fourthSheet.GetRange(10, 1, 10, 2).SetHAlign(VTHAlign.xlHAlignCenter);
                        fourthSheet.GetRange(10, 1, 10, (8 + lstLeavingReason.Count())).FillColor(System.Drawing.Color.LightYellow);
                        fourthSheet.GetRange(10, 1, 10, (8 + lstLeavingReason.Count())).SetFontStyle(true, System.Drawing.Color.Black, false, 11, false, false);
                    }

                    string formularTT = "=SUM(C";
                    string formularTT2 = "=SUM(D";
                    string formularTT3 = "=SUM(E";
                    string formularTT4 = "=SUM(F";
                    string formularTT5 = "=SUM(G";
                    string formularTT6 = "=SUM(H";
                    for (int i = 0; i < dicTotal.Count; i++)
                    {
                        formularTT += dicTotal[i] + ",C";
                        formularTT2 += dicTotal[i] + ",D";
                        formularTT3 += dicTotal[i] + ",E";
                        formularTT4 += dicTotal[i] + ",F";
                        formularTT5 += dicTotal[i] + ",G";
                        formularTT6 += dicTotal[i] + ",H";
                    }
                    formularTT = formularTT.Substring(0, formularTT.Length - 2) + ")";
                    formularTT2 = formularTT2.Substring(0, formularTT2.Length - 2) + ")";
                    formularTT3 = formularTT3.Substring(0, formularTT3.Length - 2) + ")";
                    formularTT4 = formularTT4.Substring(0, formularTT4.Length - 2) + ")";
                    formularTT5 = formularTT5.Substring(0, formularTT5.Length - 2) + ")";
                    formularTT6 = formularTT6.Substring(0, formularTT6.Length - 2) + ")";
                    //Toan truong
                    firstSheet.SetCellValue("C10", formularTT);
                    firstSheet.SetCellValue("D10", formularTT2);
                    firstSheet.SetCellValue("E10", formularTT3);
                    firstSheet.SetCellValue("F10", formularTT4);
                    firstSheet.SetCellValue("G10", formularTT5);
                    firstSheet.SetCellValue("H10", formularTT6);

                    if (showSheetFemale)
                    {
                        secondSheet.SetCellValue("C10", formularTT);
                        secondSheet.SetCellValue("D10", formularTT2);
                        secondSheet.SetCellValue("E10", formularTT3);
                        secondSheet.SetCellValue("F10", formularTT4);
                        secondSheet.SetCellValue("G10", formularTT5);
                        secondSheet.SetCellValue("H10", formularTT6);
                    }
                    if (showSheetEthenic)
                    {
                        thirdSheet.SetCellValue("C10", formularTT);
                        thirdSheet.SetCellValue("D10", formularTT2);
                        thirdSheet.SetCellValue("E10", formularTT3);
                        thirdSheet.SetCellValue("F10", formularTT4);
                        thirdSheet.SetCellValue("G10", formularTT5);
                        thirdSheet.SetCellValue("H10", formularTT6);
                    }
                    if (showSheetFemaleEthenic)
                    {
                        fourthSheet.SetCellValue("C10", formularTT);
                        fourthSheet.SetCellValue("D10", formularTT2);
                        fourthSheet.SetCellValue("E10", formularTT3);
                        fourthSheet.SetCellValue("F10", formularTT4);
                        fourthSheet.SetCellValue("G10", formularTT5);
                        fourthSheet.SetCellValue("H10", formularTT6);
                    }

                    formularTT = string.Empty;
                    string alpha = string.Empty;
                    for (int k = 9; k <= (8 + lstLeavingReason.Count()); k++)
                    {
                        alpha = UtilsBusiness.GetExcelColumnName(k);
                        formularTT = "=SUM(" + alpha;
                        for (int i = 0; i < dicTotal.Count; i++)
                        {
                            formularTT += dicTotal[i] + "," + alpha;
                        }

                        formularTT = formularTT.Substring(0, formularTT.Length - 2) + ")";
                        firstSheet.SetCellValue((alpha + 10), formularTT);

                        if (showSheetFemale)
                        {
                            secondSheet.SetCellValue((alpha + 10), formularTT);
                        }
                        if (showSheetEthenic)
                        {
                            thirdSheet.SetCellValue((alpha + 10), formularTT);
                        }
                        if (showSheetFemaleEthenic)
                        {
                            fourthSheet.SetCellValue((alpha + 10), formularTT);
                        }

                        alpha = string.Empty;
                        formularTT = string.Empty;
                    }
                }
            }
            #endregion

            if (showLastMonth)
            {
                firstSheet.SetColumnWidth('D', 0);
                if (showSheetFemale)
                {
                    secondSheet.SetColumnWidth('D', 0);

                }
                if (showSheetEthenic)
                {
                    thirdSheet.SetColumnWidth('D', 0);
                }
                if (showSheetFemaleEthenic)
                {
                    fourthSheet.SetColumnWidth('D', 0);
                }
            }

            firstSheet.SetFontName("Times New Roman", 0);
            if (showSheetFemale)
            {
                secondSheet.SetFontName("Times New Roman", 0);
            }
            else
            {
                secondSheet.Delete();
            }

            if (showSheetEthenic)
            {
                thirdSheet.SetFontName("Times New Roman", 0);
            }
            else
            {
                thirdSheet.Delete();
            }

            if (showSheetFemaleEthenic)
            {
                fourthSheet.SetFontName("Times New Roman", 0);
            }
            else
            {
                fourthSheet.Delete();
            }
            #endregion

            return oBook.ToStream();
        }

        public void InsertReportAbsencePupilForSuperVising(IDictionary<string, object> dic)
        {
            //int? monthID = Utils.GetInt(dic, "MonthID");
            int year = Utils.GetInt(dic, "Year");
            int reportType = Utils.GetInt(dic, "ReportType");
            int provinceId = Utils.GetInt(dic, "ProvinceID");
            int? districtID = Utils.GetInt(dic, "DistrictID");
            DateTime fromDate = Convert.ToDateTime(dic["FromDate"]);
            DateTime toDate = Convert.ToDateTime(dic["ToDate"]);
            //string superVisingDeptName = Utils.GetString(dic, "SuperVisingDeptName");
            bool SheetTH = Utils.GetBool(dic, "SheetTH");
            bool SheetTHCS = Utils.GetBool(dic, "SheetTHCS");
            bool SheetTHPT = Utils.GetBool(dic, "SheetTHPT");
            bool SheetMN = Utils.GetBool(dic, "SheetMN");

            #region // Get data

            List<AcademicYearBO> iqSchool = (from sp in SchoolProfileBusiness.AllNoTracking
                                             join ay in AcademicYearBusiness.All.Where(o => o.Year == year && o.IsActive == true) on sp.SchoolProfileID equals ay.SchoolID
                                             where sp.ProvinceID == provinceId
                                                   && sp.IsActive == true
                                                   && sp.IsActiveSMAS == true
                                             select new AcademicYearBO
                                             {
                                                 AcademicYearID = ay.AcademicYearID,
                                                 SchoolID = sp.SchoolProfileID,
                                                 Year = year,
                                                 School = sp,
                                                 DistrictID = sp.DistrictID,
                                                 SuperVisingID = sp.SupervisingDeptID,
                                                 EducationGrade = sp.EducationGrade
                                             }).ToList();
            if (districtID > 0)
            {
                iqSchool = iqSchool.Where(sp => sp.School.DistrictID == districtID).ToList();
            }

            if (iqSchool == null || iqSchool.Count == 0)
            {
                return;
            }

            // xoa du lieu cu
            //string ReportCode = string.Empty;
            //if (reportType == 1)
            //{
            //    ReportCode = "SheetListPupilAbsence";
            //}
            //else
            //{
            //    ReportCode = "SheetStatisticPupilAbsence";
            //}

            //IQueryable<MarkStatistic> iQMarkStatisticDelete = from m in MarkStatisticBusiness.AllNoTracking
            //                                             where m.ProvinceID == provinceId
            //                                             && m.Year == year
            //                                             && m.ReportCode == ReportCode
            //                                             && m.ProcessedDate >= fromDate && m.ProcessedDate <= toDate
            //                                             select m;
            //if (districtID > 0)
            //{
            //    iQMarkStatisticDelete = iQMarkStatisticDelete.Where(m => m.DistrictID == districtID);
            //}

            //List<MarkStatistic> lstMarkStatistic = iQMarkStatisticDelete.ToList();

            //try
            //{
            //    context.Configuration.AutoDetectChangesEnabled = false;
            //    context.Configuration.ValidateOnSaveEnabled = false;
            //    if (lstMarkStatistic != null && lstMarkStatistic.Count > 0)
            //    {
            //        MarkStatisticBusiness.DeleteAll(lstMarkStatistic);
            //        MarkStatisticBusiness.Save();
            //    }
            //}
            //catch (Exception ex)
            //{
            //    
            //    LogExtensions.ErrorExt(logger, DateTime.Now, "null", ex);
            //}
            //finally
            //{
            //    context.Configuration.AutoDetectChangesEnabled = true;
            //    context.Configuration.ValidateOnSaveEnabled = true;
            //}

            #endregion

            int startDay = fromDate.Day;
            int endDay = toDate.Day;
            TimeSpan difference = toDate - fromDate;
            double days = difference.TotalDays;
            int distantOfDay = Convert.ToInt32(days + 1);
            int sumDay = startDay + distantOfDay;

            #region
            foreach (var item in iqSchool)
            {
                DateTime date = fromDate;
                Task[] arrTask = new Task[1];
                for (int z = 0; z < 1; z++)
                {
                    int currentIndex = z;
                    arrTask[z] = Task.Factory.StartNew(() =>
                    {
                        this.InsertReportAbsencePupilForSup(item.SuperVisingID.Value, item.AcademicYearID,
                            provinceId,
                            item.DistrictID, item.SchoolID, item.EducationGrade, year, fromDate, toDate,
                            startDay, distantOfDay,
                            SheetMN, SheetTH, SheetTHCS, SheetTHPT, reportType);
                    });
                }
                Task.WaitAll(arrTask);
            }
            MarkStatisticBusiness.Save();
            #endregion
        }

        private void InsertReportAbsencePupilForSup(int supervisingDeptID, int academicYearId,
                                                    int provinceId, int? districtId,
                                                    int schoolId, int educationGrade, int year, DateTime? fromDate, DateTime? toDate,
                                                    int startDay, int distantOfDay,
                                                    bool SheetMN, bool SheetTH, bool SheetTHCS, bool SheetTHPT, int reportType)
        {
            SMASEntities contenxt1 = new SMASEntities();
            using (OracleConnection conn = new OracleConnection(contenxt1.Database.Connection.ConnectionString))
            {
                try
                {
                    int isTH = 0;
                    int isTHCS = 0;
                    int isTHPT = 0;
                    int isMN = 0;
                    #region
                    if (conn != null)
                    {
                        if (conn.State != ConnectionState.Open)
                        {
                            conn.Open();
                        }
                        using (OracleTransaction tran = conn.BeginTransaction())
                        {
                            try
                            {
                                OracleCommand command = new OracleCommand
                                {
                                    CommandType = CommandType.StoredProcedure,
                                    //CommandText = "REPORT_ABSENCE_PUPIL_SUP_THCS",
                                    Connection = conn,
                                    CommandTimeout = 0
                                };

                                command.Parameters.Add("P_LAST_2DIGIT_NUMBER_PROVINCE", UtilsBusiness.GetPartionProvince(provinceId));
                                command.Parameters.Add("P_SUPER_VISING_DEPT_ID", supervisingDeptID);
                                command.Parameters.Add("P_ACADEMIC_YEAR_ID", academicYearId);
                                command.Parameters.Add("P_PROVINCE_ID", provinceId);
                                command.Parameters.Add("P_DISTRICT_ID", districtId);
                                command.Parameters.Add("P_SCHOOL_ID", schoolId);
                                command.Parameters.Add("P_YEAR", year);
                                command.Parameters.Add("P_FROMDATE", fromDate);
                                command.Parameters.Add("P_TODATE", toDate);
                                command.Parameters.Add("P_DATE", startDay);
                                command.Parameters.Add("P_DISTANTDATE", distantOfDay);

                                if (SheetMN && (educationGrade == 8 || educationGrade == 16 || educationGrade == 24 || educationGrade == 31))
                                {
                                    isMN = 1;
                                }
                                if (SheetTH && (educationGrade == 1 || educationGrade == 3 || educationGrade == 7 || educationGrade == 31))
                                {
                                    isTH = 1;
                                }
                                if (SheetTHCS && (educationGrade == 2 || educationGrade == 3 || educationGrade == 6 || educationGrade == 7 || educationGrade == 31))
                                {
                                    isTHCS = 1;
                                }
                                if (SheetTHPT && (educationGrade == 3 || educationGrade == 4 || educationGrade == 6 || educationGrade == 7 || educationGrade == 31))
                                {
                                    isTHPT = 1;
                                }
                                if (SheetMN || SheetTH || SheetTHCS || SheetTHPT)
                                {
                                    command.Parameters.Add("P_ISTH", isTH);
                                    command.Parameters.Add("P_ISTHCS", isTHCS);
                                    command.Parameters.Add("P_ISTHPT", isTHPT);
                                    command.Parameters.Add("P_ISMN", isMN);
                                    if (reportType == 2)
                                    {
                                        command.CommandText = "REPORT_ABSENCE_PUPIL_SUP_THPT";
                                    }
                                    else if (reportType == 1)
                                    {
                                        command.CommandText = "REPORT_LISTABSENCE_SUP_THPT";
                                    }
                                    command.ExecuteNonQuery();
                                }
                                tran.Commit();
                            }//end try
                            catch (Exception)
                            {
                                tran.Rollback();
                            }
                            finally
                            {
                                tran.Dispose();
                            }
                        }
                    }
                    #endregion
                }
                finally
                {
                    if (conn != null)
                    {
                        conn.Close();
                    }
                }
            }
        }

        public ProcessedReport GetTicketTakeExamPaperReport(IDictionary<string, object> dic)
        {
            string reportCode = "";
            reportCode = SystemParamsInFile.DSHSNghiHoc_PhongSo;
            string inputParameterHashKey = GetHashKey(dic);
            return ProcessedReportBusiness.GetProcessedReport(reportCode, inputParameterHashKey);
        }

        public ProcessedReport InsertTicketTakeExamPaperReport(IDictionary<string, object> dic, Stream data)
        {
            string reportCode = "";
            reportCode = SystemParamsInFile.DSHSNghiHoc_PhongSo_1;
            ProcessedReport pr = new ProcessedReport();
            ReportDefinition reportDef = ReportDefinitionBusiness.GetByCode(reportCode);
            pr.ReportCode = reportCode;
            pr.ProcessedDate = DateTime.Now;
            pr.InputParameterHashKey = GetHashKey(dic);
            pr.ReportData = ReportUtils.Compress(data);

            //Tạo tên file
            string outputNamePattern = reportDef.OutputNamePattern;
            pr.ReportName = ReportUtils.RemoveSpecialCharacters(outputNamePattern) + "." + reportDef.OutputFormat;

            ProcessedReportParameterRepository.Insert(dic, pr);
            ProcessedReportRepository.Insert(pr);
            ProcessedReportRepository.Save();
            ProcessedReportParameterRepository.Save();
            return pr;
        }

        public ProcessedReport InsertTicketTakeExamPaperReportZip(IDictionary<string, object> diczip, Stream data)
        {
            int subjectID = Utils.GetInt(diczip, "SubjectID");
            string reportCode = reportCode = SystemParamsInFile.DSHSNghiHoc_PhongSo;
            ReportDefinition reportDef = ReportDefinitionBusiness.GetByCode(reportCode);
            ProcessedReport pr = new ProcessedReport();
            pr.ReportCode = reportCode;
            pr.ProcessedDate = DateTime.Now;
            pr.InputParameterHashKey = GetHashKey(diczip);
            pr.ReportData = subjectID == 0 ? ((MemoryStream)data).ToArray() : ReportUtils.Compress(data);

            //Tạo tên file
            string outputNamePattern = reportDef.OutputNamePattern;
            pr.ReportName = ReportUtils.RemoveSpecialCharacters(outputNamePattern) + "." + reportDef.OutputFormat;

            ProcessedReportParameterRepository.Insert(diczip, pr);
            ProcessedReportRepository.Insert(pr);
            ProcessedReportRepository.Save();
            return pr;
        }

        public string GetHashKey(IDictionary<string, object> dic)
        {
            return ReportUtils.GetHashKey(dic);
        }

        public Stream ReportStatisticsPupilAbsenceBySup(IDictionary<string, object> dic)
        {
            //Đường dẫn template & Tên file xuất ra
            string template = Path.Combine(SystemParamsInFile.TEMPLATE_FOLDER, "Phong_So", SystemParamsInFile.TKHSNghiHoc_PhongSo + ".xls");
            IVTWorkbook oBook = VTExport.OpenWorkbook(template);
            IVTWorksheet firstSheet = oBook.GetSheet(1);
            IVTWorksheet secondSheet = oBook.GetSheet(2);
            IVTWorksheet thirdSheet = oBook.GetSheet(3);
            IVTWorksheet fourthSheet = oBook.GetSheet(4);
            //int academicYearID = Utils.GetInt(dic, "AcademicYearID");
            int reportType = Utils.GetInt(dic, "ReportType");
            int year = Utils.GetInt(dic, "Year");
            int provinceId = Utils.GetInt(dic, "ProvinceID");
            int? districtID = Utils.GetInt(dic, "DistrictID");
            DateTime fromDate = Convert.ToDateTime(dic["FromDate"]);
            DateTime toDate = Convert.ToDateTime(dic["ToDate"]);
            string superVisingDeptName = Utils.GetString(dic, "SuperVisingDeptName");
            bool SheetMN = Utils.GetBool(dic, "SheetMN");
            bool SheetTH = Utils.GetBool(dic, "SheetTH");
            bool SheetTHCS = Utils.GetBool(dic, "SheetTHCS");
            bool SheetTHPT = Utils.GetBool(dic, "SheetTHPT");
            bool isSub = Utils.GetBool(dic, "isSub");
            string[] lstAlphabet = { "0", "A", "B", "C", "D", "E", "F", "G", "H", "I", "J", "K", "L", "M", "N", "O", "P", "Q", "R", "S", "T", "U", "V", "W", "X", "Y", "Z", "AA", "AB", "AC", "AD", "AE", "AF", "AG", "AH", "AI", "AJ", "AK", "AL", "AM", "AN", "AO", "AP", "AQ", "AR", "AS", "AT", "AU", "AV", "AW", "AX", "AY", "AZ", "BA", "BB", "BC", "BD", "BE", "BF", "BG", "BH", "BI", "BJ", "BK", "BL", "BM", "BN", "BO", "BP", "BQ", "BR", "BS", "BT", "BU", "BV", "BW", "BX", "BY", "BZ" };
            // process date
            int startDay = fromDate.Day;
            int endDay = toDate.Day;
            TimeSpan difference = toDate - fromDate;
            double days = difference.TotalDays;
            int distantOfDay = Convert.ToInt32(days + 1);

            int rowTitle = 8;
            int columnTitle = 4;
            DateTime fromDateTemp = fromDate;
            DateTime DateTemp = fromDate;
            IVTRange rang = firstSheet.GetRange("D8", "E1200");
            #region fill title các tháng
            for (int i = 0; i < distantOfDay; i++)
            {
                if (SheetTH)
                {
                    firstSheet.CopyPasteSameSize(rang, rowTitle, columnTitle);
                    firstSheet.GetRange(rowTitle, columnTitle, rowTitle, columnTitle + 1).Merge();
                    firstSheet.SetCellValue(rowTitle, columnTitle, fromDateTemp.ToShortDateString());
                    firstSheet.SetCellValue(rowTitle + 1, columnTitle, "SL");
                    firstSheet.SetCellValue(rowTitle + 1, columnTitle + 1, "%");
                }
                if (SheetTHCS)
                {
                    secondSheet.CopyPasteSameSize(rang, rowTitle, columnTitle);
                    secondSheet.GetRange(rowTitle, columnTitle, rowTitle, columnTitle + 1).Merge();
                    secondSheet.SetCellValue(rowTitle, columnTitle, fromDateTemp.ToShortDateString());
                    secondSheet.SetCellValue(rowTitle + 1, columnTitle, "SL");
                    secondSheet.SetCellValue(rowTitle + 1, columnTitle + 1, "%");
                }
                if (SheetTHPT)
                {
                    thirdSheet.CopyPasteSameSize(rang, rowTitle, columnTitle);
                    thirdSheet.GetRange(rowTitle, columnTitle, rowTitle, columnTitle + 1).Merge();
                    thirdSheet.SetCellValue(rowTitle, columnTitle, fromDateTemp.ToShortDateString());
                    thirdSheet.SetCellValue(rowTitle + 1, columnTitle, "SL");
                    thirdSheet.SetCellValue(rowTitle + 1, columnTitle + 1, "%");
                }
                if (SheetMN)
                {
                    fourthSheet.CopyPasteSameSize(rang, rowTitle, columnTitle);
                    fourthSheet.GetRange(rowTitle, columnTitle, rowTitle, columnTitle + 1).Merge();
                    fourthSheet.SetCellValue(rowTitle, columnTitle, fromDateTemp.ToShortDateString());
                    fourthSheet.SetCellValue(rowTitle + 1, columnTitle, "SL");
                    fourthSheet.SetCellValue(rowTitle + 1, columnTitle + 1, "%");
                }
                fromDateTemp = fromDateTemp.AddDays(1);
                columnTitle = columnTitle + 2;
            }

            if (SheetTH)
            {
                firstSheet.CopyPasteSameSize(rang, rowTitle, columnTitle);
                firstSheet.GetRange(rowTitle, columnTitle, rowTitle, columnTitle + 1).Merge();
                firstSheet.SetCellValue(rowTitle, columnTitle, "Tổng");
                firstSheet.SetCellValue(rowTitle + 1, columnTitle, "SL");
                firstSheet.SetCellValue(rowTitle + 1, columnTitle + 1, "%");
            }
            if (SheetTHCS)
            {
                secondSheet.CopyPasteSameSize(rang, rowTitle, columnTitle);
                secondSheet.GetRange(rowTitle, columnTitle, rowTitle, columnTitle + 1).Merge();
                secondSheet.SetCellValue(rowTitle, columnTitle, "Tổng");
                secondSheet.SetCellValue(rowTitle + 1, columnTitle, "SL");
                secondSheet.SetCellValue(rowTitle + 1, columnTitle + 1, "%");
            }
            if (SheetTHPT)
            {
                thirdSheet.CopyPasteSameSize(rang, rowTitle, columnTitle);
                thirdSheet.GetRange(rowTitle, columnTitle, rowTitle, columnTitle + 1).Merge();
                thirdSheet.SetCellValue(rowTitle, columnTitle, "Tổng");
                thirdSheet.SetCellValue(rowTitle + 1, columnTitle, "SL");
                thirdSheet.SetCellValue(rowTitle + 1, columnTitle + 1, "%");
            }
            if (SheetMN)
            {
                fourthSheet.CopyPasteSameSize(rang, rowTitle, columnTitle);
                fourthSheet.GetRange(rowTitle, columnTitle, rowTitle, columnTitle + 1).Merge();
                fourthSheet.SetCellValue(rowTitle, columnTitle, "Tổng");
                fourthSheet.SetCellValue(rowTitle + 1, columnTitle, "SL");
                fourthSheet.SetCellValue(rowTitle + 1, columnTitle + 1, "%");
            }
            #endregion

            #region // Get data
            Province objProvince = ProvinceBusiness.Find(provinceId);
            List<District> lstDistrict = DistrictBusiness.All.Where(x => x.ProvinceID == provinceId).ToList();
            if (districtID.HasValue && districtID.Value > 0)
            {
                lstDistrict = lstDistrict.Where(x => x.DistrictID == districtID.Value).ToList();
            }
            List<int> lstDistrictID = lstDistrict.Select(x => x.DistrictID).ToList();

            IQueryable<AcademicYearBO> iqShoolAll = from sp in SchoolProfileBusiness.AllNoTracking
                                                    join ay in AcademicYearBusiness.All.Where(o => o.Year == year && o.IsActive == true) on sp.SchoolProfileID equals ay.SchoolID
                                                    where sp.ProvinceID == provinceId
                                                          && sp.IsActive == true
                                                          && sp.IsActiveSMAS == true
                                                    select new AcademicYearBO
                                                    {
                                                        AcademicYearID = ay.AcademicYearID,
                                                        SchoolID = sp.SchoolProfileID,
                                                        Year = year,
                                                        School = sp,
                                                        DistrictID = sp.DistrictID,
                                                        SuperVisingID = sp.SupervisingDeptID,
                                                        EducationGrade = sp.EducationGrade,
                                                        SchoolName = sp.SchoolName,
                                                    };

            if (districtID > 0)
            {
                iqShoolAll = iqShoolAll.Where(sp => sp.School.DistrictID == districtID);
            }

            List<AcademicYearBO> lstSchoolAll = iqShoolAll.ToList();

            IQueryable<MarkStatistic> iQMarkStatisticDelete = from m in MarkStatisticBusiness.AllNoTracking
                                                              where m.ProvinceID == provinceId
                                                              && m.Year == year
                                                              && m.ReportCode == "SheetStatisticPupilAbsence"
                                                              && m.ProcessedDate >= fromDate && m.ProcessedDate <= toDate
                                                              select m;
            if (districtID > 0)
            {
                iQMarkStatisticDelete = iQMarkStatisticDelete.Where(m => m.DistrictID == districtID);
            }

            List<MarkStatistic> lstMark = iQMarkStatisticDelete.ToList();

            #endregion

            #region // Fill tieu de
            if (SheetTH)
            {
                firstSheet.SetCellValue("A3", superVisingDeptName);
                firstSheet.SetCellValue("A6", string.Format("Từ ngày {0} đến ngày {1}", fromDate.ToShortDateString(), toDate.ToShortDateString()));
            }

            if (SheetTHCS)
            {
                secondSheet.SetCellValue("A3", superVisingDeptName);
                secondSheet.SetCellValue("A6", string.Format("Từ ngày {0} đến ngày {1}", fromDate.ToShortDateString(), toDate.ToShortDateString()));
            }

            if (SheetTHPT)
            {
                thirdSheet.SetCellValue("A3", superVisingDeptName);
                thirdSheet.SetCellValue("A6", string.Format("Từ ngày {0} đến ngày {1}", fromDate.ToShortDateString(), toDate.ToShortDateString()));
            }

            if (SheetMN)
            {
                fourthSheet.SetCellValue("A3", superVisingDeptName);
                fourthSheet.SetCellValue("A6", string.Format("Từ ngày {0} đến ngày {1}", fromDate.ToShortDateString(), toDate.ToShortDateString()));
            }
            #endregion

            #region các biến
            int startRowTH = 11;
            int startRowTHCS = 11;
            int startRowTHPT = 11;
            int startRowMN = 11;
            int startColumn = 3;
            int sttTH, sttTHCS, sttTHPT, sttMN;

            int countSchoolTH = 0;
            int countSchoolTHCS = 0;
            int countSchoolTHPT = 0;
            int countSchoolMN = 0;
            int lastColumn = 3;
            MarkStatistic objMarkTH = null;
            MarkStatistic objMarkTHDate = null;
            MarkStatistic objMarkTHCS = null;
            MarkStatistic objMarkTHCSDate = null;
            MarkStatistic objMarkTHPT = null;
            MarkStatistic objMarkTHPTDate = null;
            MarkStatistic objMarkMN = null;
            MarkStatistic objMarkMNDate = null;
            List<MarkStatistic> lstMSTH = null;
            List<MarkStatistic> lstMSTHCS = null;
            List<MarkStatistic> lstMSTHPT = null;
            List<MarkStatistic> lstMSMN = null;
            List<MarkStatistic> lstMSTHTemp = null;
            List<MarkStatistic> lstMSTHCSTemp = null;
            List<MarkStatistic> lstMSTHPTTemp = null;
            List<MarkStatistic> lstMSMNTemp = null;
            List<int> lstDistrictTH = new List<int>();
            List<int> lstDistrictTHCS = new List<int>();
            List<int> lstDistrictTHPT = new List<int>();
            List<int> lstDistrictMN = new List<int>();
            string stringTHDistrictRow = "=";
            string stringTHCSDistrictRow = "=";
            string stringTHPTDistrictRow = "=";
            string stringMNDistrictRow = "=";
            string stringTHDistrictSumRow = "=";
            string stringTHCSDistrictSumRow = "=";
            string stringTHPTDistrictSumRow = "=";
            string stringMNDistrictSumRow = "=";
            #endregion

            #region // Fill nội dung
            foreach (var itemDis in lstDistrict)
            {
                sttTH = sttTHCS = sttTHPT = sttMN = 1;
                countSchoolTH = 0;
                countSchoolTHCS = 0;
                countSchoolTHPT = 0;
                countSchoolMN = 0;
                stringTHDistrictRow = "=";
                stringTHCSDistrictRow = "=";
                stringTHPTDistrictRow = "=";
                stringMNDistrictRow = "=";

                // Lấy số lượng trường trong tỉnh theo từng cấp
                #region
                if (SheetTH)
                {
                    lstMSTH = lstMark.Where(x => x.DistrictID == itemDis.DistrictID
                                            && x.MarkLevel37 != -1).ToList();
                    countSchoolTH = lstMSTH.Select(x => x.SchoolID).Distinct().Count();
                }

                if (SheetTHCS)
                {
                    lstMSTHCS = lstMark.Where(x => x.DistrictID == itemDis.DistrictID
                                            && x.MarkLevel38 != -1).ToList();
                    countSchoolTHCS = lstMSTHCS.Select(x => x.SchoolID).Distinct().Count();
                }

                if (SheetTHPT)
                {
                    lstMSTHPT = lstMark.Where(x => x.DistrictID == itemDis.DistrictID
                                            && x.MarkLevel39 != -1).ToList();
                    countSchoolTHPT = lstMSTHPT.Select(x => x.SchoolID).Distinct().Count();
                }

                if (SheetMN)
                {
                    lstMSMN = lstMark.Where(x => x.DistrictID == itemDis.DistrictID
                                            && x.MarkLevel40 != -1).ToList();
                    countSchoolMN = lstMSMN.Select(x => x.SchoolID).Distinct().Count();
                }
                #endregion

                var lstSchoolProfile = lstSchoolAll.Where(x => x.DistrictID == itemDis.DistrictID);
                foreach (var itemSchool in lstSchoolProfile)
                {
                    startColumn = 3;
                    DateTemp = fromDate;
                    lstMSTHTemp = new List<MarkStatistic>();
                    lstMSTHCSTemp = new List<MarkStatistic>();
                    lstMSTHPTTemp = new List<MarkStatistic>();
                    lstMSMNTemp = new List<MarkStatistic>();
                    stringTHDistrictRow = "=";
                    stringTHCSDistrictRow = "=";
                    stringTHPTDistrictRow = "=";
                    stringMNDistrictRow = "=";
                    #region fill STT, tên trường, tổng HS của các trường
                    if (SheetTH)
                    {
                        lstMSTHTemp = lstMSTH.Where(x => x.SchoolID == itemSchool.SchoolID && x.AcademicYearID == itemSchool.AcademicYearID && x.MarkLevel37 != -1).ToList();
                        objMarkTH = lstMSTHTemp.FirstOrDefault();
                        if (objMarkTH != null)
                        {
                            startRowTH++;
                            firstSheet.SetCellValue(("A" + startRowTH), sttTH);
                            //firstSheet.GetRange(startRow, 1, startRow, 1).SetHAlign(VTHAlign.xlHAlignCenter);
                            firstSheet.SetCellValue(("B" + startRowTH), itemSchool.SchoolName);
                            firstSheet.SetCellValue(startRowTH, startColumn, objMarkTH.MarkLevel37);
                            sttTH++;
                        }
                    }

                    if (SheetTHCS)
                    {
                        lstMSTHCSTemp = lstMSTHCS.Where(x => x.SchoolID == itemSchool.SchoolID && x.AcademicYearID == itemSchool.AcademicYearID && x.MarkLevel38 != -1).ToList();
                        objMarkTHCS = lstMSTHCSTemp.FirstOrDefault();
                        if (objMarkTHCS != null)
                        {
                            startRowTHCS++;
                            secondSheet.SetCellValue(("A" + startRowTHCS), sttTHCS);
                            //firstSheet.GetRange(startRow, 1, startRow, 1).SetHAlign(VTHAlign.xlHAlignCenter);
                            secondSheet.SetCellValue(("B" + startRowTHCS), itemSchool.SchoolName);
                            secondSheet.SetCellValue(startRowTHCS, startColumn, objMarkTHCS.MarkLevel38);
                            sttTHCS++;
                        }
                    }

                    if (SheetTHPT)
                    {
                        lstMSTHPTTemp = lstMSTHPT.Where(x => x.SchoolID == itemSchool.SchoolID && x.AcademicYearID == itemSchool.AcademicYearID && x.MarkLevel39 != -1).ToList();
                        objMarkTHPT = lstMSTHPTTemp.FirstOrDefault();
                        if (objMarkTHPT != null)
                        {
                            startRowTHPT++;
                            thirdSheet.SetCellValue(("A" + startRowTHPT), sttTHPT);
                            //firstSheet.GetRange(startRow, 1, startRow, 1).SetHAlign(VTHAlign.xlHAlignCenter);
                            thirdSheet.SetCellValue(("B" + startRowTHPT), itemSchool.SchoolName);
                            thirdSheet.SetCellValue(startRowTHPT, startColumn, objMarkTHPT.MarkLevel39);
                            sttTHPT++;
                        }
                    }

                    if (SheetMN)
                    {
                        lstMSMNTemp = lstMSMN.Where(x => x.SchoolID == itemSchool.SchoolID && x.AcademicYearID == itemSchool.AcademicYearID && x.MarkLevel40 != -1).ToList();
                        objMarkMN = lstMSMNTemp.FirstOrDefault();
                        if (objMarkMN != null)
                        {
                            startRowMN++;
                            fourthSheet.SetCellValue(("A" + startRowMN), sttMN);
                            //firstSheet.GetRange(startRow, 1, startRow, 1).SetHAlign(VTHAlign.xlHAlignCenter);
                            fourthSheet.SetCellValue(("B" + startRowMN), itemSchool.SchoolName);
                            fourthSheet.SetCellValue(startRowMN, startColumn, objMarkMN.MarkLevel40);
                            sttMN++;
                        }
                    }
                    #endregion
                    startColumn++;

                    #region fill từng tháng của trường
                    for (int i = 0; i <= distantOfDay; i++)
                    {
                        #region cột tổng của trường
                        if (i == distantOfDay && i != 0)
                        {
                            if (SheetTH && objMarkTH != null)
                            {
                                firstSheet.SetCellValue(startRowTH, startColumn, stringTHDistrictRow.Substring(0, stringTHDistrictRow.Length - 1));
                                firstSheet.SetCellValue(startRowTH, startColumn + 1, "=ROUND(" + lstAlphabet[startColumn] + startRowTH + "/IF(C" + startRowTH + "<=0,1,C" + startRowTH + "),3)*100");//"=(" + lstAlphabet[startColumn] + startRowTH + "/C" + startRowTH + ")/5%");
                            }
                            if (SheetTHCS && objMarkTHCS != null)
                            {
                                secondSheet.SetCellValue(startRowTHCS, startColumn, stringTHCSDistrictRow.Substring(0, stringTHCSDistrictRow.Length - 1));
                                secondSheet.SetCellValue(startRowTHCS, startColumn + 1, "=ROUND(" + lstAlphabet[startColumn] + startRowTHCS + "/IF(C" + startRowTHCS + "<=0,1,C" + startRowTHCS + "),3)*100");
                            }
                            if (SheetTHPT && objMarkTHPT != null)
                            {
                                thirdSheet.SetCellValue(startRowTHPT, startColumn, stringTHPTDistrictRow.Substring(0, stringTHPTDistrictRow.Length - 1));
                                thirdSheet.SetCellValue(startRowTHPT, startColumn + 1, "=ROUND(" + lstAlphabet[startColumn] + startRowTHPT + "/IF(C" + startRowTHPT + "<=0,1,C" + startRowTHPT + "),3)*100");
                            }
                            if (SheetMN && objMarkMN != null)
                            {
                                fourthSheet.SetCellValue(startRowMN, startColumn, stringMNDistrictRow.Substring(0, stringMNDistrictRow.Length - 1));
                                fourthSheet.SetCellValue(startRowMN, startColumn + 1, "=ROUND(" + lstAlphabet[startColumn] + startRowMN + "/IF(C" + startRowMN + "<=0,1,C" + startRowMN + "),3)*100");
                            }
                            lastColumn = startColumn;
                            break;
                        }
                        #endregion

                        if (SheetTH && objMarkTH != null)
                        {
                            stringTHDistrictRow = stringTHDistrictRow + lstAlphabet[startColumn] + startRowTH + "+";
                            objMarkTHDate = lstMSTHTemp.Where(x => x.ProcessedDate == DateTemp).FirstOrDefault();
                            firstSheet.SetCellValue(startRowTH, startColumn, objMarkTHDate == null ? 0 : objMarkTHDate.MarkLevel01);
                            firstSheet.SetCellValue(startRowTH, startColumn + 1, "=ROUND(" + lstAlphabet[startColumn] + startRowTH + "/IF(C" + startRowTH + "<=0,1,C" + startRowTH + "),3)*100");//"=" + lstAlphabet[startColumn] + startRowTH + "/" + "C" + startRowTH + "%");
                        }
                        if (SheetTHCS && objMarkTHCS != null)
                        {
                            stringTHCSDistrictRow = stringTHCSDistrictRow + lstAlphabet[startColumn] + startRowTHCS + "+";
                            objMarkTHCSDate = lstMSTHCSTemp.Where(x => x.ProcessedDate == DateTemp).FirstOrDefault();
                            secondSheet.SetCellValue(startRowTHCS, startColumn, objMarkTHCSDate == null ? 0 : objMarkTHCSDate.MarkLevel02);
                            secondSheet.SetCellValue(startRowTHCS, startColumn + 1, "=ROUND(" + lstAlphabet[startColumn] + startRowTHCS + "/IF(C" + startRowTHCS + "<=0,1,C" + startRowTHCS + "),3)*100");//"=" + lstAlphabet[startColumn] + startRowTHCS + "/" + "C" + startRowTHCS + "%");
                        }
                        if (SheetTHPT && objMarkTHPT != null)
                        {
                            stringTHPTDistrictRow = stringTHPTDistrictRow + lstAlphabet[startColumn] + startRowTHPT + "+";
                            objMarkTHPTDate = lstMSTHPTTemp.Where(x => x.ProcessedDate == DateTemp).FirstOrDefault();
                            thirdSheet.SetCellValue(startRowTHPT, startColumn, objMarkTHPTDate == null ? 0 : objMarkTHPTDate.MarkLevel03);
                            thirdSheet.SetCellValue(startRowTHPT, startColumn + 1, "=ROUND(" + lstAlphabet[startColumn] + startRowTHPT + "/IF(C" + startRowTHPT + "<=0,1,C" + startRowTHPT + "),3)*100");
                        }
                        if (SheetMN && objMarkMN != null)
                        {
                            stringMNDistrictRow = stringMNDistrictRow + lstAlphabet[startColumn] + startRowMN + "+";
                            objMarkMNDate = lstMSMNTemp.Where(x => x.ProcessedDate == DateTemp).FirstOrDefault();
                            fourthSheet.SetCellValue(startRowMN, startColumn, objMarkMNDate == null ? 0 : objMarkMNDate.MarkLevel04);
                            fourthSheet.SetCellValue(startRowMN, startColumn + 1, "=ROUND(" + lstAlphabet[startColumn] + startRowMN + "/IF(C" + startRowMN + "<=0,1,C" + startRowMN + "),3)*100");
                        }

                        DateTemp = DateTemp.AddDays(1);
                        startColumn = startColumn + 2;
                    }
                    #endregion
                }

                #region fill quận/huyện
                int rowTH = startRowTH - countSchoolTH;
                int rowTHCS = startRowTHCS - countSchoolTHCS;
                int rowTHPT = startRowTHPT - countSchoolTHPT;
                int rowMN = startRowMN - countSchoolMN;
                if (SheetTH)
                {
                    firstSheet.SetCellValue(("A" + rowTH), itemDis.DistrictName);
                    lstDistrictTH.Add(rowTH);
                    firstSheet.GetRange(rowTH, 1, rowTH, startColumn).SetFontStyle(true, System.Drawing.Color.Black, false, 12, false, false);
                    firstSheet.GetRange(rowTH, 1, rowTH, 2).SetHAlignVer(NativeExcel.XlVAlign.xlVAlignCenter);
                    firstSheet.MergeRow(rowTH, 'A', 'B');
                    firstSheet.GetRange(rowTH, 1, rowTH, 2).SetHAlign(VTHAlign.xlHAlignLeft);
                    firstSheet.SetRowHeight(rowTH, 24.75);
                }
                if (SheetTHCS)
                {
                    secondSheet.SetCellValue(("A" + rowTHCS), itemDis.DistrictName);
                    lstDistrictTHCS.Add(rowTHCS);
                    secondSheet.GetRange(rowTHCS, 1, rowTHCS, startColumn).SetFontStyle(true, System.Drawing.Color.Black, false, 12, false, false);
                    secondSheet.GetRange(rowTHCS, 1, rowTHCS, 2).SetHAlignVer(NativeExcel.XlVAlign.xlVAlignCenter);
                    secondSheet.MergeRow(rowTHCS, 'A', 'B');
                    secondSheet.GetRange(rowTHCS, 1, rowTHCS, 2).SetHAlign(VTHAlign.xlHAlignLeft);
                    secondSheet.SetRowHeight(rowTHCS, 24.75);
                }
                if (SheetTHPT)
                {
                    thirdSheet.SetCellValue(("A" + rowTHPT), itemDis.DistrictName);
                    lstDistrictTHPT.Add(rowTHPT);
                    thirdSheet.GetRange(rowTHPT, 1, rowTHPT, startColumn).SetFontStyle(true, System.Drawing.Color.Black, false, 12, false, false);
                    thirdSheet.GetRange(rowTHPT, 1, rowTHPT, 2).SetHAlignVer(NativeExcel.XlVAlign.xlVAlignCenter);
                    thirdSheet.MergeRow(rowTHPT, 'A', 'B');
                    thirdSheet.GetRange(rowTHPT, 1, rowTHPT, 2).SetHAlign(VTHAlign.xlHAlignLeft);
                    thirdSheet.SetRowHeight(rowTHPT, 24.75);
                }
                if (SheetMN)
                {
                    fourthSheet.SetCellValue(("A" + rowMN), itemDis.DistrictName);
                    lstDistrictMN.Add(rowMN);
                    fourthSheet.GetRange(rowMN, 1, rowMN, startColumn).SetFontStyle(true, System.Drawing.Color.Black, false, 12, false, false);
                    fourthSheet.GetRange(rowMN, 1, rowMN, 2).SetHAlignVer(NativeExcel.XlVAlign.xlVAlignCenter);
                    fourthSheet.MergeRow(rowMN, 'A', 'B');
                    fourthSheet.GetRange(rowMN, 1, rowMN, 2).SetHAlign(VTHAlign.xlHAlignLeft);
                    fourthSheet.SetRowHeight(rowMN, 24.75);
                }

                startColumn = 3;
                if (countSchoolTH == 0)
                {
                    firstSheet.SetCellValue(rowTH, startColumn, 0);
                }
                else
                {
                    firstSheet.SetCellValue(rowTH, startColumn, "=SUM(" + lstAlphabet[startColumn] + (countSchoolTH == 0 ? rowTH : (rowTH + 1)) + ":" + lstAlphabet[startColumn] + (rowTH + countSchoolTH) + ")");
                }
                if (countSchoolTHCS == 0)
                {
                    secondSheet.SetCellValue(rowTHCS, startColumn, 0);
                }
                else
                {
                    secondSheet.SetCellValue(rowTHCS, startColumn, "=SUM(" + lstAlphabet[startColumn] + (countSchoolTHCS == 0 ? rowTHCS : (rowTHCS + 1)) + ":" + lstAlphabet[startColumn] + (rowTHCS + countSchoolTHCS) + ")");
                }
                if (countSchoolTHPT == 0)
                {
                    thirdSheet.SetCellValue(rowTHPT, startColumn, 0);
                }
                else
                {
                    thirdSheet.SetCellValue(rowTHPT, startColumn, "=SUM(" + lstAlphabet[startColumn] + (countSchoolTHPT == 0 ? rowTHPT : (rowTHPT + 1)) + ":" + lstAlphabet[startColumn] + (rowTHPT + countSchoolTHPT) + ")");
                }
                if (countSchoolMN == 0)
                {
                    fourthSheet.SetCellValue(rowMN, startColumn, 0);
                }
                else
                {
                    fourthSheet.SetCellValue(rowMN, startColumn, "=SUM(" + lstAlphabet[startColumn] + (countSchoolMN == 0 ? rowMN : (rowMN + 1)) + ":" + lstAlphabet[startColumn] + (rowMN + countSchoolMN) + ")");
                }
                startColumn++;
                stringTHDistrictRow = "=";
                stringTHCSDistrictRow = "=";
                stringTHPTDistrictRow = "=";
                stringMNDistrictRow = "=";
                for (int i = 0; i <= distantOfDay; i++)
                {
                    #region fill cột tổng
                    if (i == distantOfDay && i != 0)
                    {
                        if (SheetTH)
                        {
                            firstSheet.SetCellValue(rowTH, startColumn, stringTHDistrictRow.Substring(0, stringTHDistrictRow.Length - 1));
                            firstSheet.SetCellValue(rowTH, startColumn + 1, "=ROUND(" + lstAlphabet[startColumn] + rowTH + "/IF(C" + rowTH + "<=0,1,C" + rowTH + "),3)*100");
                        }
                        if (SheetTHCS)
                        {
                            secondSheet.SetCellValue(rowTHCS, startColumn, stringTHCSDistrictRow.Substring(0, stringTHCSDistrictRow.Length - 1));
                            secondSheet.SetCellValue(rowTHCS, startColumn + 1, "=ROUND(" + lstAlphabet[startColumn] + rowTHCS + "/IF(C" + rowTHCS + "<=0,1,C" + rowTHCS + "),3)*100");
                        }
                        if (SheetTHPT)
                        {
                            thirdSheet.SetCellValue(rowTHPT, startColumn, stringTHPTDistrictRow.Substring(0, stringTHPTDistrictRow.Length - 1));
                            thirdSheet.SetCellValue(rowTHPT, startColumn + 1, "=ROUND(" + lstAlphabet[startColumn] + rowTHPT + "/IF(C" + rowTHPT + "<=0,1,C" + rowTHPT + "),3)*100");
                        }
                        if (SheetMN)
                        {
                            fourthSheet.SetCellValue(rowMN, startColumn, stringMNDistrictRow.Substring(0, stringMNDistrictRow.Length - 1));
                            fourthSheet.SetCellValue(rowMN, startColumn + 1, "=ROUND(" + lstAlphabet[startColumn] + rowMN + "/IF(C" + rowMN + "<=0,1,C" + rowMN + "),3)*100");
                        }
                        break;
                    }
                    #endregion

                    if (SheetTH)
                    {
                        stringTHDistrictRow = stringTHDistrictRow + lstAlphabet[startColumn] + rowTH + "+";
                        if (countSchoolTH == 0)
                        {
                            firstSheet.SetCellValue(rowTH, startColumn, 0);
                        }
                        else
                        {
                            firstSheet.SetCellValue(rowTH, startColumn, "=SUM(" + lstAlphabet[startColumn] + (countSchoolTH == 0 ? rowTH : (rowTH + 1)) + ":" + lstAlphabet[startColumn] + (rowTH + countSchoolTH) + ")");
                        }
                        firstSheet.SetCellValue(rowTH, startColumn + 1, "=ROUND(" + lstAlphabet[startColumn] + rowTH + "/IF(C" + rowTH + "<=0,1,C" + rowTH + "),3)*100");
                    }
                    if (SheetTHCS)
                    {
                        stringTHCSDistrictRow = stringTHCSDistrictRow + lstAlphabet[startColumn] + rowTHCS + "+";
                        if (countSchoolTHCS == 0)
                        {
                            secondSheet.SetCellValue(rowTHCS, startColumn, 0);
                        }
                        else
                        {
                            secondSheet.SetCellValue(rowTHCS, startColumn, "=SUM(" + lstAlphabet[startColumn] + (countSchoolTHCS == 0 ? rowTHCS : (rowTHCS + 1)) + ":" + lstAlphabet[startColumn] + (rowTHCS + countSchoolTHCS) + ")");
                        }
                        secondSheet.SetCellValue(rowTHCS, startColumn + 1, "=ROUND(" + lstAlphabet[startColumn] + rowTHCS + "/IF(C" + rowTHCS + "<=0,1,C" + rowTHCS + "),3)*100");
                    }
                    if (SheetTHPT)
                    {
                        stringTHPTDistrictRow = stringTHPTDistrictRow + lstAlphabet[startColumn] + rowTHPT + "+";
                        if (countSchoolTHPT == 0)
                        {
                            thirdSheet.SetCellValue(rowTHPT, startColumn, 0);
                        }
                        else
                        {
                            thirdSheet.SetCellValue(rowTHPT, startColumn, "=SUM(" + lstAlphabet[startColumn] + (countSchoolTHPT == 0 ? rowTHPT : (rowTHPT + 1)) + ":" + lstAlphabet[startColumn] + (rowTHPT + countSchoolTHPT) + ")");
                        }
                        thirdSheet.SetCellValue(rowTHPT, startColumn + 1, "=ROUND(" + lstAlphabet[startColumn] + rowTHPT + "/IF(C" + rowTHPT + "<=0,1,C" + rowTHPT + "),3)*100");
                    }
                    if (SheetMN)
                    {
                        stringMNDistrictRow = stringMNDistrictRow + lstAlphabet[startColumn] + rowMN + "+";
                        if (countSchoolMN == 0)
                        {
                            fourthSheet.SetCellValue(rowMN, startColumn, 0);
                        }
                        else
                        {
                            fourthSheet.SetCellValue(rowMN, startColumn, "=SUM(" + lstAlphabet[startColumn] + (countSchoolMN == 0 ? rowMN : (rowMN + 1)) + ":" + lstAlphabet[startColumn] + (rowMN + countSchoolMN) + ")");
                        }
                        fourthSheet.SetCellValue(rowMN, startColumn + 1, "=ROUND(" + lstAlphabet[startColumn] + rowMN + "/IF(C" + rowMN + "<=0,1,C" + rowMN + "),3)*100");
                    }
                    startColumn = startColumn + 2;
                }
                #endregion
                startRowTH++;
                startRowTHCS++;
                startRowTHPT++;
                startRowMN++;
            }
            firstSheet.GetRange(11, 1, startRowTH - 1, startColumn + 1).SetBorder(VTBorderStyle.Solid, VTBorderWeight.Thin, VTBorderIndex.All);
            secondSheet.GetRange(11, 1, startRowTHCS - 1, startColumn + 1).SetBorder(VTBorderStyle.Solid, VTBorderWeight.Thin, VTBorderIndex.All);
            thirdSheet.GetRange(11, 1, startRowTHPT - 1, startColumn + 1).SetBorder(VTBorderStyle.Solid, VTBorderWeight.Thin, VTBorderIndex.All);
            fourthSheet.GetRange(11, 1, startRowMN - 1, startColumn + 1).SetBorder(VTBorderStyle.Solid, VTBorderWeight.Thin, VTBorderIndex.All);


            #region fill toàn tỉnh
            if (districtID == 0)
            {
                stringTHDistrictRow = "=";
                stringTHCSDistrictRow = "=";
                stringTHPTDistrictRow = "=";
                stringMNDistrictRow = "=";
                startColumn = 3;

                #region cột tổng học sinh của toàn tỉnh
                for (int j = 0; j < lstDistrictTH.Count; j++)
                {
                    stringTHDistrictRow = stringTHDistrictRow + lstAlphabet[startColumn] + lstDistrictTH[j] + "+";
                }
                firstSheet.SetCellValue(10, startColumn, stringTHDistrictRow.Substring(0, stringTHDistrictRow.Length - 1));
                //firstSheet.SetCellValue(10, startColumn + 1, "=" + lstAlphabet[startColumn] + "10/C10%");

                for (int j = 0; j < lstDistrictTHCS.Count; j++)
                {
                    stringTHCSDistrictRow = stringTHCSDistrictRow + lstAlphabet[startColumn] + lstDistrictTHCS[j] + "+";
                }
                secondSheet.SetCellValue(10, startColumn, stringTHCSDistrictRow.Substring(0, stringTHCSDistrictRow.Length - 1));
                //secondSheet.SetCellValue(10, startColumn + 1, "=" + lstAlphabet[startColumn] + "10/C10%");

                for (int j = 0; j < lstDistrictTHPT.Count; j++)
                {
                    stringTHPTDistrictRow = stringTHPTDistrictRow + lstAlphabet[startColumn] + lstDistrictTHPT[j] + "+";
                }
                thirdSheet.SetCellValue(10, startColumn, stringTHPTDistrictRow.Substring(0, stringTHPTDistrictRow.Length - 1));
                //thirdSheet.SetCellValue(10, startColumn + 1, "=" + lstAlphabet[startColumn] + "10/C10%");

                for (int j = 0; j < lstDistrictMN.Count; j++)
                {
                    stringMNDistrictRow = stringMNDistrictRow + lstAlphabet[startColumn] + lstDistrictMN[j] + "+";
                }
                fourthSheet.SetCellValue(10, startColumn, stringMNDistrictRow.Substring(0, stringMNDistrictRow.Length - 1));
                //fourthSheet.SetCellValue(10, startColumn + 1, "=" + lstAlphabet[startColumn] + "10/C10%");
                startColumn++;
                #endregion

                #region Các tháng của toàn tỉnh
                for (int i = 0; i < distantOfDay; i++)
                {
                    stringTHDistrictRow = "=";
                    stringTHCSDistrictRow = "=";
                    stringTHPTDistrictRow = "=";
                    stringMNDistrictRow = "=";
                    for (int j = 0; j < lstDistrictTH.Count; j++)
                    { //=D11+D19
                        stringTHDistrictRow = stringTHDistrictRow + lstAlphabet[startColumn] + lstDistrictTH[j] + "+";
                    }
                    if (SheetTH)
                    {
                        stringTHDistrictSumRow = stringTHDistrictSumRow + lstAlphabet[startColumn] + 10 + "+";
                        firstSheet.SetCellValue(10, startColumn, stringTHDistrictRow.Substring(0, stringTHDistrictRow.Length - 1));
                        firstSheet.SetCellValue(10, startColumn + 1, "=ROUND(" + lstAlphabet[startColumn] + 10 + "/IF(C10<=0,1,C10),3)*100");//"=" + lstAlphabet[startColumn] +"10/C10%");
                    }

                    for (int j = 0; j < lstDistrictTHCS.Count; j++)
                    { //=D11+D19
                        stringTHCSDistrictRow = stringTHCSDistrictRow + lstAlphabet[startColumn] + lstDistrictTHCS[j] + "+";
                    }
                    if (SheetTHCS)
                    {
                        stringTHCSDistrictSumRow = stringTHCSDistrictSumRow + lstAlphabet[startColumn] + 10 + "+";
                        secondSheet.SetCellValue(10, startColumn, stringTHCSDistrictRow.Substring(0, stringTHCSDistrictRow.Length - 1));
                        secondSheet.SetCellValue(10, startColumn + 1, "=ROUND(" + lstAlphabet[startColumn] + 10 + "/IF(C10<=0,1,C10),3)*100");
                    }

                    for (int j = 0; j < lstDistrictTHPT.Count; j++)
                    { //=D11+D19
                        stringTHPTDistrictRow = stringTHPTDistrictRow + lstAlphabet[startColumn] + lstDistrictTHPT[j] + "+";
                    }
                    if (SheetTHPT)
                    {
                        stringTHPTDistrictSumRow = stringTHPTDistrictSumRow + lstAlphabet[startColumn] + 10 + "+";
                        thirdSheet.SetCellValue(10, startColumn, stringTHPTDistrictRow.Substring(0, stringTHPTDistrictRow.Length - 1));
                        thirdSheet.SetCellValue(10, startColumn + 1, "=ROUND(" + lstAlphabet[startColumn] + 10 + "/IF(C10<=0,1,C10),3)*100");
                    }

                    for (int j = 0; j < lstDistrictMN.Count; j++)
                    {
                        stringMNDistrictRow = stringMNDistrictRow + lstAlphabet[startColumn] + lstDistrictMN[j] + "+";
                    }
                    if (SheetMN)
                    {
                        stringMNDistrictSumRow = stringMNDistrictSumRow + lstAlphabet[startColumn] + 10 + "+";
                        fourthSheet.SetCellValue(10, startColumn, stringMNDistrictRow.Substring(0, stringMNDistrictRow.Length - 1));
                        fourthSheet.SetCellValue(10, startColumn + 1, "=ROUND(" + lstAlphabet[startColumn] + 10 + "/IF(C10<=0,1,C10),3)*100");
                    }

                    startColumn = startColumn + 2;
                }
                #endregion

                #region cột tổng của toàn tỉnh
                if (SheetTH)
                {
                    firstSheet.SetCellValue(10, lastColumn, stringTHDistrictSumRow.Substring(0, stringTHDistrictSumRow.Length - 1));
                    firstSheet.SetCellValue(10, lastColumn + 1, "=ROUND(" + lstAlphabet[lastColumn] + 10 + "/IF(C10<=0,1,C10),3)*100");
                }
                if (SheetTHCS)
                {
                    secondSheet.SetCellValue(10, lastColumn, stringTHCSDistrictSumRow.Substring(0, stringTHCSDistrictSumRow.Length - 1));
                    secondSheet.SetCellValue(10, lastColumn + 1, "=ROUND(" + lstAlphabet[lastColumn] + 10 + "/IF(C10<=0,1,C10),3)*100");
                }
                if (SheetTHPT)
                {
                    thirdSheet.SetCellValue(10, lastColumn, stringTHPTDistrictSumRow.Substring(0, stringTHPTDistrictSumRow.Length - 1));
                    thirdSheet.SetCellValue(10, lastColumn + 1, "=ROUND(" + lstAlphabet[lastColumn] + 10 + "/IF(C10<=0,1,C10),3)*100");
                }
                if (SheetMN)
                {
                    fourthSheet.SetCellValue(10, lastColumn, stringMNDistrictSumRow.Substring(0, stringMNDistrictSumRow.Length - 1));
                    fourthSheet.SetCellValue(10, lastColumn + 1, "=ROUND(" + lstAlphabet[lastColumn] + 10 + "/IF(C10<=0,1,C10),3)*100");
                }
                #endregion
            }
            else
            {
                firstSheet.DeleteRow(10);
                secondSheet.DeleteRow(10);
                thirdSheet.DeleteRow(10);
                fourthSheet.DeleteRow(10);
            }
            #endregion
            if (SheetTH)
            {
                firstSheet.GetRange(5, 1, 5, lastColumn + 1).Merge();
                firstSheet.GetRange(6, 1, 6, lastColumn + 1).Merge();
                firstSheet.SetFontName("Times New Roman", 0);
            }
            else
            {
                firstSheet.Delete();
            }
            if (SheetTHCS)
            {
                secondSheet.GetRange(5, 1, 5, lastColumn + 1).Merge();
                secondSheet.GetRange(6, 1, 6, lastColumn + 1).Merge();
                secondSheet.SetFontName("Times New Roman", 0);
            }
            else
            {
                secondSheet.Delete();
            }
            if (SheetTHPT)
            {
                thirdSheet.GetRange(5, 1, 5, lastColumn + 1).Merge();
                thirdSheet.GetRange(6, 1, 6, lastColumn + 1).Merge();
                thirdSheet.SetFontName("Times New Roman", 0);
            }
            else
            {
                thirdSheet.Delete();
            }
            if (SheetMN)
            {
                fourthSheet.GetRange(5, 1, 5, lastColumn + 1).Merge();
                fourthSheet.GetRange(6, 1, 6, lastColumn + 1).Merge();
                fourthSheet.SetFontName("Times New Roman", 0);
            }
            else
            {
                fourthSheet.Delete();
            }
            #endregion

            return oBook.ToStream();
        }

        public Stream ReportListPupilAbsenceBySup(IDictionary<string, object> dic)
        {
            //Đường dẫn template & Tên file xuất ra
            string template = Path.Combine(SystemParamsInFile.TEMPLATE_FOLDER, "Phong_So", SystemParamsInFile.DSHSNghiHoc_PhongSo + ".xls");
            IVTWorkbook oBook = VTExport.OpenWorkbook(template);
            IVTWorksheet firstSheet = oBook.GetSheet(1);
            IVTWorksheet secondSheet = oBook.GetSheet(2);
            IVTWorksheet thirdSheet = oBook.GetSheet(3);
            IVTWorksheet fourthSheet = oBook.GetSheet(4);
            return oBook.ToStream();
        }

        public void InsertReportSituationStatisticsBySuperVising(IDictionary<string, object> dic)
        {
            int? appliedLevelID = Utils.GetInt(dic, "AppliedLevel");
            int? monthID = Utils.GetInt(dic, "MonthID");
            int? year = Utils.GetInt(dic, "Year");
            int provinceId = Utils.GetInt(dic, "ProvinceID");
            int? districtID = Utils.GetInt(dic, "DistrictID");
            string superVisingDeptName = Utils.GetString(dic, "SuperVisingDeptName");
            bool showSheetFemale = Utils.GetBool(dic, "SheetPupilFemale");
            bool showSheetEthenic = Utils.GetBool(dic, "SheetPupilEthnic");
            bool showSheetFemaleEthenic = Utils.GetBool(dic, "SheetPupilFemaleEthnic");

            #region // Get data
            Province objProvince = ProvinceBusiness.Find(provinceId);
            IQueryable<District> lstDistrict = DistrictBusiness.All.Where(x => x.ProvinceID == provinceId);
            if (districtID.HasValue && districtID.Value > 0)
            {
                lstDistrict = lstDistrict.Where(x => x.DistrictID == districtID.Value);
            }
            List<int> lstDistrictID = lstDistrict.Select(x => x.DistrictID).ToList();

            IQueryable<EducationLevel> lstEducation = EducationLevelBusiness.All.Where(x => x.Grade == 1 || x.Grade == 2 || x.Grade == 3 && x.IsActive == true);
            if (appliedLevelID.HasValue && appliedLevelID.Value > 0)
            {
                lstEducation = lstEducation.Where(x => x.Grade == appliedLevelID);
            }
            List<int> lstEducationID = lstEducation.Select(x => x.EducationLevelID).ToList();

            // Danh sach nam hoc
            IQueryable<AcademicYear> lstYear = AcademicYearBusiness.All.Where(x => (x.Year == year.Value) && (x.IsActive == true));

            //lay danh sach truong theo nam hoc
            List<int> lstSchoolID = lstYear.Select(x => x.SchoolID).Distinct().ToList();
            IQueryable<SchoolProfile> lstSchoolAll = SchoolProfileBusiness.All.Where(x => lstSchoolID.Contains(x.SchoolProfileID)
                                                                                        && x.ProvinceID == provinceId
                                                                                        && x.DistrictID.HasValue
                                                                                        && lstDistrictID.Contains(x.DistrictID.Value)
                                                                                        && x.IsActive == true);
            #endregion

            #region
            DateTime currenDate = DateTime.Now;
            int _month = 0;
            int _year = 0;
            if (monthID.HasValue && monthID.Value > 0)
            {
                string _monthID = monthID.ToString();
                int _length = _monthID.Length;
                string _strMonth = string.Empty;
                string _strYear = string.Empty;

                if (_length == 5)
                    _strMonth = _monthID.Substring(4, 1).ToString();
                else
                    _strMonth = _monthID.Substring(4, 2).ToString();

                _strYear = _monthID.Substring(0, 4).ToString();

                Int32.TryParse(_strMonth, out _month);
                Int32.TryParse(_strYear, out _year);
            }

            int _lastMonth = 0;
            int _lastYear = 0;
            if (_month == 1)
            {
                _lastYear = _year - 1;
                _lastMonth = 12;
            }
            else
            {
                _lastYear = _year;
                _lastMonth = _month - 1;
            }

            DateTime valueMinDate = DateTime.Now;
            DateTime valueMaxDate = DateTime.Now;
            DateTime valueMinLastMonth = new DateTime(_lastYear, _lastMonth, 1);
            DateTime valueMaxLastMonth = valueMinLastMonth.AddMonths(1).AddDays(-1);
            DateTime valueOfMonthSelected = new DateTime(_year, _month, 1);
            DateTime valueOfCurrent = new DateTime(currenDate.Year, currenDate.Month, 1);

            if (valueOfMonthSelected == valueOfCurrent)
            {
                valueMinDate = new DateTime(currenDate.Year, currenDate.Month, 1);
                valueMaxDate = DateTime.Now;
            }
            else
            {
                valueMinDate = new DateTime(_year, _month, 1);
                valueMaxDate = valueMinDate.AddMonths(1).AddDays(-1);
            }
            #endregion

            var lstMark = MarkStatisticBusiness.All.Where(x => (x.ReportCode == "ALL" || x.ReportCode == "FEMALE" || x.ReportCode == "ETHENIC" || x.ReportCode == "FEMALEETHENIC")
                                                            && x.ProvinceID == provinceId
                                                            && x.Year == year).ToList();
            if (lstMark.Count() > 0)
                MarkStatisticBusiness.DeleteAll(lstMark);

            #region
            foreach (var item in lstSchoolAll)
            {
                var objYear = lstYear.Where(x => x.SchoolID == item.SchoolProfileID).FirstOrDefault();
                Task[] arrTask = new Task[1];
                for (int z = 0; z < 1; z++)
                {
                    int currentIndex = z;
                    arrTask[z] = Task.Factory.StartNew(() =>
                    {
                        this.InsertReport(provinceId, appliedLevelID.Value,
                            objYear.FirstSemesterStartDate.Value, valueMinDate, valueMaxDate, valueMinLastMonth, valueMaxLastMonth,
                            item.DistrictID, item.SchoolProfileID, item.SupervisingDeptID.Value, objYear.AcademicYearID,
                            year.Value, showSheetFemale, showSheetEthenic, showSheetFemaleEthenic);
                    });
                }
                Task.WaitAll(arrTask);
            }
            MarkStatisticBusiness.Save();

            #endregion
        }

        private void InsertReport(int provinceId, int appliedLevelId,
            DateTime firstSemeter, DateTime minMonth, DateTime maxMonth, DateTime minLastMonth, DateTime maxLastMonth,
            int? districtId, int schoolId, int supervisingDeptID, int academicYearId, int year, bool female, bool ethenic, bool femaleEthenic)
        {
            SMASEntities contenxt1 = new SMASEntities();
            using (OracleConnection conn = new OracleConnection(contenxt1.Database.Connection.ConnectionString))
            {
                try
                {
                    #region
                    if (conn != null)
                    {
                        if (conn.State != ConnectionState.Open)
                        {
                            conn.Open();
                        }
                        using (OracleTransaction tran = conn.BeginTransaction())
                        {
                            try
                            {
                                OracleCommand command = new OracleCommand
                                {
                                    CommandType = CommandType.StoredProcedure,
                                    CommandText = "STATISTICS_PUPIL_BYSUPPER_1",
                                    Connection = conn,
                                    CommandTimeout = 0
                                };

                                command.Parameters.Add("P_LAST_2DIGIT_NUMBER_PROVINCE", (provinceId % 100));
                                command.Parameters.Add("P_SUPER_VISING_DEPT_ID", supervisingDeptID);
                                command.Parameters.Add("P_ACADEMIC_YEAR_ID", academicYearId);
                                command.Parameters.Add("P_APPLIED_LEVEL_ID", appliedLevelId);
                                command.Parameters.Add("P_PROVINCE_ID", provinceId);
                                command.Parameters.Add("P_DISTRICT_ID", districtId);
                                command.Parameters.Add("P_SCHOOL_ID", schoolId);
                                command.Parameters.Add("P_YEAR", year);
                                command.Parameters.Add("P_First_Semester_Start_Date", firstSemeter);
                                command.Parameters.Add("P_ValueMinDate", minMonth);
                                command.Parameters.Add("P_ValueMaxDate", maxMonth);
                                command.Parameters.Add("P_ValueMinLastMonth", minLastMonth);
                                command.Parameters.Add("P_ValueMaxLastMonth", maxLastMonth);
                                command.ExecuteNonQuery();

                                if (female)
                                {
                                    command.CommandText = "STATISTICS_PUPIL_BYSUPPER_2";
                                    command.ExecuteNonQuery();
                                }

                                if (ethenic)
                                {
                                    command.CommandText = "STATISTICS_PUPIL_BYSUPPER_3";
                                    command.ExecuteNonQuery();
                                }

                                if (femaleEthenic)
                                {
                                    command.CommandText = "STATISTICS_PUPIL_BYSUPPER_4";
                                    command.ExecuteNonQuery();
                                }

                                tran.Commit();
                            }
                            catch (Exception)
                            {
                                tran.Rollback();
                            }
                            finally
                            {
                                tran.Dispose();
                            }
                        }
                    }
                    #endregion
                }
                finally
                {
                    if (conn != null)
                    {
                        conn.Close();
                    }
                }
            }
        }

        public Stream ExportSituationStatisticsBySuperVising(IDictionary<string, object> dic)
        {
            //Đường dẫn template & Tên file xuất ra
            string template = Path.Combine(SystemParamsInFile.TEMPLATE_FOLDER, "Phong_So", SystemParamsInFile.SGD_TKBienDongHS_112017 + ".xls");
            IVTWorkbook oBook = VTExport.OpenWorkbook(template);
            IVTWorksheet firstSheet = oBook.GetSheet(1);
            IVTWorksheet secondSheet = oBook.GetSheet(2);
            IVTWorksheet thirdSheet = oBook.GetSheet(3);
            IVTWorksheet fourthSheet = oBook.GetSheet(4);

            int academicYearID = Utils.GetInt(dic, "AcademicYearID");
            int? appliedLevelID = Utils.GetInt(dic, "AppliedLevel");
            int? monthID = Utils.GetInt(dic, "MonthID");
            int? year = Utils.GetInt(dic, "Year");
            int provinceId = Utils.GetInt(dic, "ProvinceID");
            int? districtID = Utils.GetInt(dic, "DistrictID");
            string superVisingDeptName = Utils.GetString(dic, "SuperVisingDeptName");
            bool showSheetFemale = Utils.GetBool(dic, "SheetPupilFemale");
            bool showSheetEthenic = Utils.GetBool(dic, "SheetPupilEthnic");
            bool showSheetFemaleEthenic = Utils.GetBool(dic, "SheetPupilFemaleEthnic");
            bool fillLeavingReason = false;

            #region // Get data
            Province objProvince = ProvinceBusiness.Find(provinceId);
            IQueryable<District> lstDistrict = DistrictBusiness.All.Where(x => x.ProvinceID == provinceId);
            if (districtID.HasValue && districtID.Value > 0)
            {
                lstDistrict = lstDistrict.Where(x => x.DistrictID == districtID.Value);
            }

            // Danh sach nam hoc
            IQueryable<AcademicYear> lstYear = AcademicYearBusiness.All.Where(x => (x.Year == year.Value) && (x.IsActive == true));
            List<int> lstAcademicYearID = lstYear.Select(x => x.AcademicYearID).ToList();

            //lay danh sach truong theo nam hoc          
            IQueryable<SchoolProfile> lstSchoolAll = from sp in SchoolProfileBusiness.All
                                                     join ac in lstYear on sp.SchoolProfileID equals ac.SchoolID
                                                     join dis in lstDistrict on sp.DistrictID equals dis.DistrictID
                                                     where sp.ProvinceID == provinceId
                                                     && sp.DistrictID.HasValue
                                                     && sp.IsActive
                                                     select sp;
            //Danh sach hoc sinh nghi hoc
            List<PupilLeavingOffBO> lstPupilLeavingOff = PupilLeavingOffBusiness.ListPupilLeavingOff(lstAcademicYearID).ToList();

            // Ly do nghi hoc
            List<LeavingReason> lstLeavingReason = LeavingReasonBusiness.All.Where(x => x.IsActive).ToList();

            int last2DigitNumberProvince = UtilsBusiness.GetPartionId(provinceId);
            List<MarkStatistic> lstMark = MarkStatisticBusiness.All.Where(x => (x.ReportCode == "ALL" || x.ReportCode == "FEMALE"
                                                                            || x.ReportCode == "ETHENIC" || x.ReportCode == "FEMALEETHENIC")
                                                                            && x.ProvinceID == provinceId
                                                                            && x.Last2DigitNumberProvince == last2DigitNumberProvince
                                                                            && x.Year == year).ToList();
            #endregion

            #region
            DateTime currenDate = DateTime.Now;
            int _month = 0;
            int _year = 0;
            if (monthID.HasValue && monthID.Value > 0)
            {
                string _monthID = monthID.ToString();
                int _length = _monthID.Length;
                string _strMonth = string.Empty;
                string _strYear = string.Empty;

                if (_length == 5)
                    _strMonth = _monthID.Substring(4, 1).ToString();
                else
                    _strMonth = _monthID.Substring(4, 2).ToString();

                _strYear = _monthID.Substring(0, 4).ToString();

                Int32.TryParse(_strMonth, out _month);
                Int32.TryParse(_strYear, out _year);
            }

            int _lastMonth = 0;
            int _lastYear = 0;
            if (_month == 1)
            {
                _lastYear = _year - 1;
                _lastMonth = 12;
            }
            else
            {
                _lastYear = _year;
                _lastMonth = _month - 1;
            }

            DateTime valueMinDate = DateTime.Now;
            DateTime valueMaxDate = DateTime.Now;
            DateTime valueMinLastMonth = new DateTime(_lastYear, _lastMonth, 1);
            DateTime valueMaxLastMonth = valueMinLastMonth.AddMonths(1).AddDays(-1);
            DateTime valueOfMonthSelected = new DateTime(_year, _month, 1);
            DateTime valueOfCurrent = new DateTime(currenDate.Year, currenDate.Month, 1);

            if (valueOfMonthSelected == valueOfCurrent)
            {
                valueMinDate = new DateTime(currenDate.Year, currenDate.Month, 1);
                valueMaxDate = DateTime.Now;
            }
            else
            {
                valueMinDate = new DateTime(_year, _month, 1);
                valueMaxDate = valueMinDate.AddMonths(1).AddDays(-1);
            }

            bool showLastMonth = false;
            if (_month == 8)
            {
                showLastMonth = true;
            }

            string _strDate = string.Format("[Province], ngày {0} tháng {1} năm {2}", currenDate.Day, currenDate.Month, currenDate.Year);

            if (objProvince != null)
            {
                _strDate = _strDate.Replace("[Province]", objProvince.ProvinceName).ToString();
            }
            else
            {
                _strDate = _strDate.Replace("[Province]", ".........").ToString();
            }
            #endregion

            #region // Fill tieu de
            firstSheet.SetCellValue("A3", superVisingDeptName);
            firstSheet.SetCellValue("E4", _strDate);
            firstSheet.SetCellValue("E6", string.Format("THỐNG KÊ TÌNH HÌNH BIẾN ĐỘNG HỌC SINH THÁNG {0}/{1}", _month, _year));
            firstSheet.SetCellValue("D8", string.Format("Số HS cuối tháng {0}/{1}", (_lastMonth), _lastYear));
            firstSheet.SetCellValue("E8", string.Format("Số HS cuối tháng {0}/{1}", _month, _year));
            firstSheet.SetCellValue("F8", string.Format("Số HS chuyển đến tháng {0}/{1}", _month, _year));
            firstSheet.SetCellValue("G8", string.Format("Số HS chuyển đi tháng {0}/{1}", _month, _year));
            firstSheet.SetCellValue("H8", string.Format("Số HS thôi học tháng {0}/{1}", _month, _year));

            if (showSheetFemale)
            {
                secondSheet.SetCellValue("A3", superVisingDeptName);
                secondSheet.SetCellValue("E4", _strDate);
                secondSheet.SetCellValue("E6", string.Format("THỐNG KÊ TÌNH HÌNH BIẾN ĐỘNG HỌC SINH NỮ THÁNG {0}/{1}", _month, _year));
                secondSheet.SetCellValue("D8", string.Format("Số HS cuối tháng {0}/{1}", (_lastMonth), _lastYear));
                secondSheet.SetCellValue("E8", string.Format("Số HS cuối tháng {0}/{1}", _month, _year));
                secondSheet.SetCellValue("F8", string.Format("Số HS chuyển đến tháng {0}/{1}", _month, _year));
                secondSheet.SetCellValue("G8", string.Format("Số HS chuyển đi tháng {0}/{1}", _month, _year));
                secondSheet.SetCellValue("H8", string.Format("Số HS thôi học tháng {0}/{1}", _month, _year));
            }

            if (showSheetEthenic)
            {
                thirdSheet.SetCellValue("A3", superVisingDeptName);
                thirdSheet.SetCellValue("E4", _strDate);
                thirdSheet.SetCellValue("E6", string.Format("THỐNG KÊ TÌNH HÌNH BIẾN ĐỘNG HỌC SINH DÂN TỘC THÁNG {0}/{1}", _month, _year));
                thirdSheet.SetCellValue("D8", string.Format("Số HS cuối tháng {0}/{1}", (_lastMonth), _lastYear));
                thirdSheet.SetCellValue("E8", string.Format("Số HS cuối tháng {0}/{1}", _month, _year));
                thirdSheet.SetCellValue("F8", string.Format("Số HS chuyển đến tháng {0}/{1}", _month, _year));
                thirdSheet.SetCellValue("G8", string.Format("Số HS chuyển đi tháng {0}/{1}", _month, _year));
                thirdSheet.SetCellValue("H8", string.Format("Số HS thôi học tháng {0}/{1}", _month, _year));
            }

            if (showSheetFemaleEthenic)
            {
                fourthSheet.SetCellValue("A3", superVisingDeptName);
                fourthSheet.SetCellValue("E4", _strDate);
                fourthSheet.SetCellValue("E6", string.Format("THỐNG KÊ TÌNH HÌNH BIẾN ĐỘNG HỌC SINH NỮ DÂN TỘC THÁNG {0}/{1}", _month, _year));
                fourthSheet.SetCellValue("D8", string.Format("Số HS cuối tháng {0}/{1}", (_lastMonth), _lastYear));
                fourthSheet.SetCellValue("E8", string.Format("Số HS cuối tháng {0}/{1}", _month, _year));
                fourthSheet.SetCellValue("F8", string.Format("Số HS chuyển đến tháng {0}/{1}", _month, _year));
                fourthSheet.SetCellValue("G8", string.Format("Số HS chuyển đi tháng {0}/{1}", _month, _year));
                fourthSheet.SetCellValue("H8", string.Format("Số HS thôi học tháng {0}/{1}", _month, _year));
            }

            if (lstLeavingReason.Count() > 0)
            {
                firstSheet.SetCellValue(8, 9, string.Format("Phân tích số HS thôi học tháng {0}/{1}", _month, _year));
                firstSheet.GetRange(8, 9, 8, 8 + lstLeavingReason.Count()).Merge();
                firstSheet.GetRange(8, 9, 8, 8 + lstLeavingReason.Count()).SetHAlign(VTHAlign.xlHAlignCenter);
                firstSheet.GetRange(8, 9, 8, 8 + lstLeavingReason.Count()).SetFontStyle(true, System.Drawing.Color.Black, false, 11, false, false);

                if (showSheetFemale)
                {
                    secondSheet.SetCellValue(8, 9, string.Format("Phân tích số HS thôi học tháng {0}/{1}", _month, _year));
                    secondSheet.GetRange(8, 9, 8, 8 + lstLeavingReason.Count()).Merge();
                    secondSheet.GetRange(8, 9, 8, 8 + lstLeavingReason.Count()).SetHAlign(VTHAlign.xlHAlignCenter);
                    secondSheet.GetRange(8, 9, 8, 8 + lstLeavingReason.Count()).SetFontStyle(true, System.Drawing.Color.Black, false, 11, false, false);
                }

                if (showSheetEthenic)
                {
                    thirdSheet.SetCellValue(8, 9, string.Format("Phân tích số HS thôi học tháng {0}/{1}", _month, _year));
                    thirdSheet.GetRange(8, 9, 8, 8 + lstLeavingReason.Count()).Merge();
                    thirdSheet.GetRange(8, 9, 8, 8 + lstLeavingReason.Count()).SetHAlign(VTHAlign.xlHAlignCenter);
                    thirdSheet.GetRange(8, 9, 8, 8 + lstLeavingReason.Count()).SetFontStyle(true, System.Drawing.Color.Black, false, 11, false, false);
                }

                if (showSheetFemaleEthenic)
                {
                    fourthSheet.SetCellValue(8, 9, string.Format("Phân tích số HS thôi học tháng {0}/{1}", _month, _year));
                    fourthSheet.GetRange(8, 9, 8, 8 + lstLeavingReason.Count()).Merge();
                    fourthSheet.GetRange(8, 9, 8, 8 + lstLeavingReason.Count()).SetHAlign(VTHAlign.xlHAlignCenter);
                    fourthSheet.GetRange(8, 9, 8, 8 + lstLeavingReason.Count()).SetFontStyle(true, System.Drawing.Color.Black, false, 11, false, false);
                }
            }
            #endregion

            string formular = string.Empty;
            string formularAllEducation = string.Empty;
            int startFillEducation = 11;
            int startRow = 11;
            int STT = 0;
            List<int> _lstPupilID = new List<int>();
            IDictionary<int, int> dicTotal = new Dictionary<int, int>();
            List<PupilLeavingOffBO> lstPupilStopLearnOfSchool = new List<PupilLeavingOffBO>();
            List<PupilLeavingOffBO> totalPupilStopLearnByReason = new List<PupilLeavingOffBO>();

            MarkStatistic objMark = null;
            int index = 0;
            #region // Fill nội dung
            foreach (var itemDis in lstDistrict)
            {
                #region
                firstSheet.SetCellValue(("B" + startRow), itemDis.DistrictName);
                if (showSheetFemale)
                {
                    secondSheet.SetCellValue(("B" + startRow), itemDis.DistrictName);
                }
                if (showSheetEthenic)
                {
                    thirdSheet.SetCellValue(("B" + startRow), itemDis.DistrictName);
                }
                if (showSheetFemaleEthenic)
                {
                    fourthSheet.SetCellValue(("B" + startRow), itemDis.DistrictName);
                }
                #endregion

                var lstSchoolProfile = lstSchoolAll.Where(x => x.DistrictID == itemDis.DistrictID);

                foreach (var itemSchool in lstSchoolProfile)
                {
                    startRow++;
                    STT++;

                    lstPupilStopLearnOfSchool = lstPupilLeavingOff.Where(x => x.SchoolID == itemSchool.SchoolProfileID).ToList();
                    objMark = lstMark.Where(x => x.DistrictID == itemDis.DistrictID
                                            && x.SchoolID == itemSchool.SchoolProfileID
                                            && x.ReportCode == "ALL").FirstOrDefault();
                    #region // Sheet tatca
                    firstSheet.SetCellValue(("A" + startRow), STT);
                    firstSheet.GetRange(startRow, 1, startRow, 1).SetHAlign(VTHAlign.xlHAlignCenter);
                    firstSheet.SetCellValue(("B" + startRow), itemSchool.SchoolName);
                    firstSheet.SetCellValue(("C" + startRow), objMark != null ? objMark.MarkLevel03.Value : 0);
                    firstSheet.SetCellValue(("D" + startRow), objMark != null ? objMark.MarkLevel04.Value : 0);
                    firstSheet.SetCellValue(("E" + startRow), objMark != null ? objMark.MarkLevel05.Value : 0);
                    firstSheet.SetCellValue(("F" + startRow), objMark != null ? objMark.MarkLevel06.Value : 0);
                    firstSheet.SetCellValue(("G" + startRow), objMark != null ? objMark.MarkLevel07.Value : 0);
                    firstSheet.SetCellValue(("H" + startRow), objMark != null ? objMark.MarkLevel08.Value : 0);
                    #endregion

                    #region // Sheet hoc sinh nu
                    if (showSheetFemale)
                    {
                        objMark = lstMark.Where(x => x.DistrictID == itemDis.DistrictID
                                           && x.SchoolID == itemSchool.SchoolProfileID
                                           && x.ReportCode == "FEMALE").FirstOrDefault();
                        secondSheet.SetCellValue(("A" + startRow), STT);
                        secondSheet.GetRange(startRow, 1, startRow, 1).SetHAlign(VTHAlign.xlHAlignCenter);
                        secondSheet.SetCellValue(("B" + startRow), itemSchool.SchoolName);
                        secondSheet.SetCellValue(("C" + startRow), objMark != null ? objMark.MarkLevel03.Value : 0);
                        secondSheet.SetCellValue(("D" + startRow), objMark != null ? objMark.MarkLevel04.Value : 0);
                        secondSheet.SetCellValue(("E" + startRow), objMark != null ? objMark.MarkLevel05.Value : 0);
                        secondSheet.SetCellValue(("F" + startRow), objMark != null ? objMark.MarkLevel06.Value : 0);
                        secondSheet.SetCellValue(("G" + startRow), objMark != null ? objMark.MarkLevel07.Value : 0);
                        secondSheet.SetCellValue(("H" + startRow), objMark != null ? objMark.MarkLevel08.Value : 0);
                    }
                    #endregion

                    #region // Sheet hoc sinh dan toc
                    if (showSheetEthenic)
                    {
                        objMark = lstMark.Where(x => x.DistrictID == itemDis.DistrictID
                                          && x.SchoolID == itemSchool.SchoolProfileID
                                          && x.ReportCode == "ETHENIC").FirstOrDefault();
                        thirdSheet.SetCellValue(("A" + startRow), STT);
                        thirdSheet.GetRange(startRow, 1, startRow, 1).SetHAlign(VTHAlign.xlHAlignCenter);
                        thirdSheet.SetCellValue(("B" + startRow), itemSchool.SchoolName);
                        thirdSheet.SetCellValue(("C" + startRow), objMark != null ? objMark.MarkLevel03.Value : 0);
                        thirdSheet.SetCellValue(("D" + startRow), objMark != null ? objMark.MarkLevel04.Value : 0);
                        thirdSheet.SetCellValue(("E" + startRow), objMark != null ? objMark.MarkLevel05.Value : 0);
                        thirdSheet.SetCellValue(("F" + startRow), objMark != null ? objMark.MarkLevel06.Value : 0);
                        thirdSheet.SetCellValue(("G" + startRow), objMark != null ? objMark.MarkLevel07.Value : 0);
                        thirdSheet.SetCellValue(("H" + startRow), objMark != null ? objMark.MarkLevel08.Value : 0);
                    }
                    #endregion

                    #region // Sheet hoc sinh nu dan toc
                    if (showSheetEthenic)
                    {
                        objMark = lstMark.Where(x => x.DistrictID == itemDis.DistrictID
                                          && x.SchoolID == itemSchool.SchoolProfileID
                                          && x.ReportCode == "FEMALEETHENIC").FirstOrDefault();
                        fourthSheet.SetCellValue(("A" + startRow), STT);
                        fourthSheet.GetRange(startRow, 1, startRow, 1).SetHAlign(VTHAlign.xlHAlignCenter);
                        fourthSheet.SetCellValue(("B" + startRow), itemSchool.SchoolName);
                        fourthSheet.SetCellValue(("C" + startRow), objMark != null ? objMark.MarkLevel03.Value : 0);
                        fourthSheet.SetCellValue(("D" + startRow), objMark != null ? objMark.MarkLevel04.Value : 0);
                        fourthSheet.SetCellValue(("E" + startRow), objMark != null ? objMark.MarkLevel05.Value : 0);
                        fourthSheet.SetCellValue(("F" + startRow), objMark != null ? objMark.MarkLevel06.Value : 0);
                        fourthSheet.SetCellValue(("G" + startRow), objMark != null ? objMark.MarkLevel07.Value : 0);
                        fourthSheet.SetCellValue(("H" + startRow), objMark != null ? objMark.MarkLevel08.Value : 0);
                    }
                    #endregion

                    #region// Fill lý do nghi hoc
                    int p = 0;
                    foreach (var itemReason in lstLeavingReason)
                    {
                        if (!fillLeavingReason)
                        {
                            firstSheet.SetCellValue(9, (9 + p), itemReason.Resolution);
                            firstSheet.GetRange(9, (9 + p), 9, (9 + p)).SetHAlign(VTHAlign.xlHAlignCenter);
                            firstSheet.GetRange(9, (9 + p), 9, (9 + p)).WrapText();
                            firstSheet.GetRange(9, (9 + p), 9, (9 + p)).SetHAlignVer(NativeExcel.XlVAlign.xlVAlignCenter);
                            firstSheet.GetRange(9, (9 + p), 9, (9 + p)).SetFontStyle(true, System.Drawing.Color.Black, false, 10, false, false);
                            firstSheet.SetColumnWidth((9 + p), 12);

                            if (showSheetFemale)
                            {
                                secondSheet.SetCellValue(9, (9 + p), itemReason.Resolution);
                                secondSheet.GetRange(9, (9 + p), 9, (9 + p)).SetHAlign(VTHAlign.xlHAlignCenter);
                                secondSheet.GetRange(9, (9 + p), 9, (9 + p)).WrapText();
                                secondSheet.GetRange(9, (9 + p), 9, (9 + p)).SetHAlignVer(NativeExcel.XlVAlign.xlVAlignCenter);
                                secondSheet.GetRange(9, (9 + p), 9, (9 + p)).SetFontStyle(true, System.Drawing.Color.Black, false, 10, false, false);
                                secondSheet.SetColumnWidth((9 + p), 12);
                            }

                            if (showSheetEthenic)
                            {
                                thirdSheet.SetCellValue(9, (9 + p), itemReason.Resolution);
                                thirdSheet.GetRange(9, (9 + p), 9, (9 + p)).SetHAlign(VTHAlign.xlHAlignCenter);
                                thirdSheet.GetRange(9, (9 + p), 9, (9 + p)).WrapText();
                                thirdSheet.GetRange(9, (9 + p), 9, (9 + p)).SetHAlignVer(NativeExcel.XlVAlign.xlVAlignCenter);
                                thirdSheet.GetRange(9, (9 + p), 9, (9 + p)).SetFontStyle(true, System.Drawing.Color.Black, false, 10, false, false);
                                thirdSheet.SetColumnWidth((9 + p), 12);
                            }

                            if (showSheetFemaleEthenic)
                            {
                                fourthSheet.SetCellValue(9, (9 + p), itemReason.Resolution);
                                fourthSheet.GetRange(9, (9 + p), 9, (9 + p)).SetHAlign(VTHAlign.xlHAlignCenter);
                                fourthSheet.GetRange(9, (9 + p), 9, (9 + p)).WrapText();
                                fourthSheet.GetRange(9, (9 + p), 9, (9 + p)).SetHAlignVer(NativeExcel.XlVAlign.xlVAlignCenter);
                                fourthSheet.GetRange(9, (9 + p), 9, (9 + p)).SetFontStyle(true, System.Drawing.Color.Black, false, 10, false, false);
                                fourthSheet.SetColumnWidth((9 + p), 12);
                            }
                        }

                        totalPupilStopLearnByReason = lstPupilStopLearnOfSchool.Where(x => x.LeavingReasonID == itemReason.LeavingReasonID).ToList();
                        firstSheet.SetCellValue(startRow, (9 + p), totalPupilStopLearnByReason.Count());

                        if (showSheetFemale)
                        {
                            var totalFemaleStopLearnByReason = totalPupilStopLearnByReason.Where(x => x.LeavingReasonID == itemReason.LeavingReasonID
                                                                                                      && x.Genre.HasValue && x.Genre == 0).ToList();
                            secondSheet.SetCellValue(startRow, (9 + p), totalFemaleStopLearnByReason.Count());
                        }
                        if (showSheetEthenic)
                        {
                            var totalEthnicStopLearnByReason = totalPupilStopLearnByReason.Where(x => x.LeavingReasonID == itemReason.LeavingReasonID
                                                                                                    && x.EthnicID.HasValue && (x.EthnicID != 1 && x.EthnicID != 79));
                            thirdSheet.SetCellValue(startRow, (9 + p), totalEthnicStopLearnByReason.Count());
                        }
                        if (showSheetFemaleEthenic)
                        {
                            var totalFemaleEthnicStopLearnByReason = totalPupilStopLearnByReason.Where(x => x.LeavingReasonID == itemReason.LeavingReasonID
                                                                                                        && x.Genre.HasValue && x.Genre == 0
                                                                                                        && x.EthnicID.HasValue && (x.EthnicID != 1 && x.EthnicID != 79));
                            fourthSheet.SetCellValue(startRow, (9 + p), totalFemaleEthnicStopLearnByReason.Count());
                        }
                        p++;
                    }

                    if (!fillLeavingReason)
                    {
                        fillLeavingReason = true;
                    }

                    #endregion
                }

                #region // Tính tổng
                if (lstSchoolProfile.Count() > 0)
                {
                    #region// Tổng học sinh đầu năm học
                    formular = "=SUM(C" + (startFillEducation + 1) + ":C" + (startRow) + ")";
                    firstSheet.SetCellValue(startFillEducation, 3, formular);
                    if (showSheetFemale)
                    {
                        secondSheet.SetCellValue(startFillEducation, 3, formular);
                    }
                    if (showSheetEthenic)
                    {
                        thirdSheet.SetCellValue(startFillEducation, 3, formular);
                    }
                    if (showSheetFemaleEthenic)
                    {
                        fourthSheet.SetCellValue(startFillEducation, 3, formular);
                    }
                    #endregion
                    #region //Số HS cuối tháng (x- 1)
                    formular = "=SUM(D" + (startFillEducation + 1) + ":D" + (startRow) + ")";
                    firstSheet.SetCellValue(startFillEducation, 4, formular);
                    if (showSheetFemale)
                    {
                        secondSheet.SetCellValue(startFillEducation, 4, formular);
                    }
                    if (showSheetEthenic)
                    {
                        thirdSheet.SetCellValue(startFillEducation, 4, formular);
                    }
                    if (showSheetFemaleEthenic)
                    {
                        fourthSheet.SetCellValue(startFillEducation, 4, formular);
                    }
                    #endregion
                    #region //Số HS cuối tháng (x)
                    formular = "=SUM(E" + (startFillEducation + 1) + ":E" + (startRow) + ")";
                    firstSheet.SetCellValue(startFillEducation, 5, formular);
                    if (showSheetFemale)
                    {
                        secondSheet.SetCellValue(startFillEducation, 5, formular);
                    }
                    if (showSheetEthenic)
                    {
                        thirdSheet.SetCellValue(startFillEducation, 5, formular);
                    }
                    if (showSheetFemaleEthenic)
                    {
                        fourthSheet.SetCellValue(startFillEducation, 5, formular);
                    }
                    #endregion
                    #region//Số HS chuyển đến tháng (x)
                    formular = "=SUM(F" + (startFillEducation + 1) + ":F" + (startRow) + ")";
                    firstSheet.SetCellValue(startFillEducation, 6, formular);
                    if (showSheetFemale)
                    {
                        secondSheet.SetCellValue(startFillEducation, 6, formular);
                    }
                    if (showSheetEthenic)
                    {
                        thirdSheet.SetCellValue(startFillEducation, 6, formular);
                    }
                    if (showSheetFemaleEthenic)
                    {
                        fourthSheet.SetCellValue(startFillEducation, 6, formular);
                    }
                    #endregion
                    #region//Số HS chuyển đi tháng (x)
                    formular = "=SUM(G" + (startFillEducation + 1) + ":G" + (startRow) + ")";
                    firstSheet.SetCellValue(startFillEducation, 7, formular);
                    if (showSheetFemale)
                    {
                        secondSheet.SetCellValue(startFillEducation, 7, formular);
                    }
                    if (showSheetEthenic)
                    {
                        thirdSheet.SetCellValue(startFillEducation, 7, formular);
                    }
                    if (showSheetFemaleEthenic)
                    {
                        fourthSheet.SetCellValue(startFillEducation, 7, formular);
                    }
                    #endregion
                    #region//Số HS thôi học tháng (x)
                    formular = "=SUM(H" + (startFillEducation + 1) + ":H" + (startRow) + ")";
                    firstSheet.SetCellValue(startFillEducation, 8, formular);
                    if (showSheetFemale)
                    {
                        secondSheet.SetCellValue(startFillEducation, 8, formular);
                    }
                    if (showSheetEthenic)
                    {
                        thirdSheet.SetCellValue(startFillEducation, 8, formular);
                    }
                    if (showSheetFemaleEthenic)
                    {
                        fourthSheet.SetCellValue(startFillEducation, 8, formular);
                    }
                    #endregion
                    #region // Tính tổng lý do nghĩ học
                    string alpha = string.Empty;
                    for (int k = 9; k <= (8 + lstLeavingReason.Count()); k++)
                    {
                        alpha = UtilsBusiness.GetExcelColumnName(k);
                        formular = "=SUM(" + alpha + (startFillEducation + 1) + ":" + alpha + (startRow) + ")";
                        firstSheet.SetCellValue(startFillEducation, k, formular);
                        if (showSheetFemale)
                        {
                            secondSheet.SetCellValue(startFillEducation, k, formular);
                        }
                        if (showSheetEthenic)
                        {
                            thirdSheet.SetCellValue(startFillEducation, k, formular);
                        }
                        if (showSheetFemaleEthenic)
                        {
                            fourthSheet.SetCellValue(startFillEducation, k, formular);
                        }
                        alpha = string.Empty;
                    }
                    #endregion

                    dicTotal[dicTotal.Count()] = startFillEducation;
                }
                else
                {
                    for (int k = 3; k <= (8 + lstLeavingReason.Count()); k++)
                    {
                        firstSheet.SetCellValue(startFillEducation, k, "0");

                        if (showSheetFemale)
                        {
                            secondSheet.SetCellValue(startFillEducation, k, "0");
                        }
                        if (showSheetEthenic)
                        {
                            thirdSheet.SetCellValue(startFillEducation, k, "0");
                        }
                        if (showSheetFemaleEthenic)
                        {
                            fourthSheet.SetCellValue(startFillEducation, k, "0");
                        }
                    }
                }
                #endregion

                #region // SetFontStyle, FillColor
                firstSheet.GetRange(startFillEducation, 1, startFillEducation, (8 + lstLeavingReason.Count())).SetFontStyle(true, System.Drawing.Color.Black, false, 11, false, false);
                firstSheet.GetRange(startFillEducation, 1, startFillEducation, (8 + lstLeavingReason.Count())).FillColor(System.Drawing.Color.LightYellow);

                if (showSheetFemale)
                {
                    secondSheet.GetRange(startFillEducation, 1, startFillEducation, (8 + lstLeavingReason.Count())).SetFontStyle(true, System.Drawing.Color.Black, false, 11, false, false);
                    secondSheet.GetRange(startFillEducation, 1, startFillEducation, (8 + lstLeavingReason.Count())).FillColor(System.Drawing.Color.LightYellow);
                }
                if (showSheetEthenic)
                {
                    thirdSheet.GetRange(startFillEducation, 1, startFillEducation, (8 + lstLeavingReason.Count())).SetFontStyle(true, System.Drawing.Color.Black, false, 11, false, false);
                    thirdSheet.GetRange(startFillEducation, 1, startFillEducation, (8 + lstLeavingReason.Count())).FillColor(System.Drawing.Color.LightYellow);
                }
                if (showSheetFemaleEthenic)
                {
                    fourthSheet.GetRange(startFillEducation, 1, startFillEducation, (8 + lstLeavingReason.Count())).SetFontStyle(true, System.Drawing.Color.Black, false, 11, false, false);
                    fourthSheet.GetRange(startFillEducation, 1, startFillEducation, (8 + lstLeavingReason.Count())).FillColor(System.Drawing.Color.LightYellow);
                }
                #endregion

                startFillEducation += (lstSchoolProfile.Count() + 1);
                startRow++;
                index++;
            }

            #region SetHAlign, SetBorder
            firstSheet.GetRange(10, 3, (startRow - 1), (8 + lstLeavingReason.Count())).SetHAlign(VTHAlign.xlHAlignCenter);
            firstSheet.GetRange(8, 1, (startRow - 1), (8 + lstLeavingReason.Count())).SetBorder(VTBorderStyle.Solid, VTBorderWeight.Thin, VTBorderIndex.All);

            if (showSheetFemale)
            {
                secondSheet.GetRange(10, 3, (startRow - 1), (8 + lstLeavingReason.Count())).SetHAlign(VTHAlign.xlHAlignCenter);
                secondSheet.GetRange(8, 1, (startRow - 1), (8 + lstLeavingReason.Count())).SetBorder(VTBorderStyle.Solid, VTBorderWeight.Thin, VTBorderIndex.All);
            }
            if (showSheetEthenic)
            {
                thirdSheet.GetRange(10, 3, (startRow - 1), (8 + lstLeavingReason.Count())).SetHAlign(VTHAlign.xlHAlignCenter);
                thirdSheet.GetRange(8, 1, (startRow - 1), (8 + lstLeavingReason.Count())).SetBorder(VTBorderStyle.Solid, VTBorderWeight.Thin, VTBorderIndex.All);
            }
            if (showSheetFemaleEthenic)
            {
                fourthSheet.GetRange(10, 3, (startRow - 1), (8 + lstLeavingReason.Count())).SetHAlign(VTHAlign.xlHAlignCenter);
                fourthSheet.GetRange(8, 1, (startRow - 1), (8 + lstLeavingReason.Count())).SetBorder(VTBorderStyle.Solid, VTBorderWeight.Thin, VTBorderIndex.All);
            }
            #endregion

            #region// Tính tổng toàn tỉnh
            if (districtID.HasValue && districtID.Value > 0)
            {
                firstSheet.DeleteRow(10);
                if (showSheetFemale)
                {
                    secondSheet.DeleteRow(10);
                }
                if (showSheetEthenic)
                {
                    thirdSheet.DeleteRow(10);
                }
                if (showSheetFemaleEthenic)
                {
                    fourthSheet.DeleteRow(10);
                }
            }
            else
            {
                if (lstDistrict.Count() > 0)
                {
                    #region //fill toan tinh
                    firstSheet.SetCellValue("A10", "Toàn tỉnh");
                    firstSheet.GetRange(10, 1, 10, 2).Merge();
                    firstSheet.GetRange(10, 1, 10, 2).SetHAlign(VTHAlign.xlHAlignCenter);
                    firstSheet.GetRange(10, 1, 10, (8 + lstLeavingReason.Count())).FillColor(System.Drawing.Color.LightYellow);
                    firstSheet.GetRange(10, 1, 10, (8 + lstLeavingReason.Count())).SetFontStyle(true, System.Drawing.Color.Black, false, 11, false, false);

                    if (showSheetFemale)
                    {
                        secondSheet.SetCellValue("A10", "Toàn tỉnh");
                        secondSheet.GetRange(10, 1, 10, 2).Merge();
                        secondSheet.GetRange(10, 1, 10, 2).SetHAlign(VTHAlign.xlHAlignCenter);
                        secondSheet.GetRange(10, 1, 10, (8 + lstLeavingReason.Count())).FillColor(System.Drawing.Color.LightYellow);
                        secondSheet.GetRange(10, 1, 10, (8 + lstLeavingReason.Count())).SetFontStyle(true, System.Drawing.Color.Black, false, 11, false, false);
                    }
                    if (showSheetEthenic)
                    {
                        thirdSheet.SetCellValue("A10", "Toàn tỉnh");
                        thirdSheet.GetRange(10, 1, 10, 2).Merge();
                        thirdSheet.GetRange(10, 1, 10, 2).SetHAlign(VTHAlign.xlHAlignCenter);
                        thirdSheet.GetRange(10, 1, 10, (8 + lstLeavingReason.Count())).FillColor(System.Drawing.Color.LightYellow);
                        thirdSheet.GetRange(10, 1, 10, (8 + lstLeavingReason.Count())).SetFontStyle(true, System.Drawing.Color.Black, false, 11, false, false);
                    }
                    if (showSheetFemaleEthenic)
                    {
                        fourthSheet.SetCellValue("A10", "Toàn tỉnh");
                        fourthSheet.GetRange(10, 1, 10, 2).Merge();
                        fourthSheet.GetRange(10, 1, 10, 2).SetHAlign(VTHAlign.xlHAlignCenter);
                        fourthSheet.GetRange(10, 1, 10, (8 + lstLeavingReason.Count())).FillColor(System.Drawing.Color.LightYellow);
                        fourthSheet.GetRange(10, 1, 10, (8 + lstLeavingReason.Count())).SetFontStyle(true, System.Drawing.Color.Black, false, 11, false, false);
                    }

                    string formularTT = "=SUM(C";
                    string formularTT2 = "=SUM(D";
                    string formularTT3 = "=SUM(E";
                    string formularTT4 = "=SUM(F";
                    string formularTT5 = "=SUM(G";
                    string formularTT6 = "=SUM(H";
                    for (int i = 0; i < dicTotal.Count; i++)
                    {
                        formularTT += dicTotal[i] + ",C";
                        formularTT2 += dicTotal[i] + ",D";
                        formularTT3 += dicTotal[i] + ",E";
                        formularTT4 += dicTotal[i] + ",F";
                        formularTT5 += dicTotal[i] + ",G";
                        formularTT6 += dicTotal[i] + ",H";
                    }
                    formularTT = formularTT.Substring(0, formularTT.Length - 2) + ")";
                    formularTT2 = formularTT2.Substring(0, formularTT2.Length - 2) + ")";
                    formularTT3 = formularTT3.Substring(0, formularTT3.Length - 2) + ")";
                    formularTT4 = formularTT4.Substring(0, formularTT4.Length - 2) + ")";
                    formularTT5 = formularTT5.Substring(0, formularTT5.Length - 2) + ")";
                    formularTT6 = formularTT6.Substring(0, formularTT6.Length - 2) + ")";
                    //Toan truong
                    firstSheet.SetCellValue("C10", formularTT);
                    firstSheet.SetCellValue("D10", formularTT2);
                    firstSheet.SetCellValue("E10", formularTT3);
                    firstSheet.SetCellValue("F10", formularTT4);
                    firstSheet.SetCellValue("G10", formularTT5);
                    firstSheet.SetCellValue("H10", formularTT6);

                    if (showSheetFemale)
                    {
                        secondSheet.SetCellValue("C10", formularTT);
                        secondSheet.SetCellValue("D10", formularTT2);
                        secondSheet.SetCellValue("E10", formularTT3);
                        secondSheet.SetCellValue("F10", formularTT4);
                        secondSheet.SetCellValue("G10", formularTT5);
                        secondSheet.SetCellValue("H10", formularTT6);
                    }
                    if (showSheetEthenic)
                    {
                        thirdSheet.SetCellValue("C10", formularTT);
                        thirdSheet.SetCellValue("D10", formularTT2);
                        thirdSheet.SetCellValue("E10", formularTT3);
                        thirdSheet.SetCellValue("F10", formularTT4);
                        thirdSheet.SetCellValue("G10", formularTT5);
                        thirdSheet.SetCellValue("H10", formularTT6);
                    }
                    if (showSheetFemaleEthenic)
                    {
                        fourthSheet.SetCellValue("C10", formularTT);
                        fourthSheet.SetCellValue("D10", formularTT2);
                        fourthSheet.SetCellValue("E10", formularTT3);
                        fourthSheet.SetCellValue("F10", formularTT4);
                        fourthSheet.SetCellValue("G10", formularTT5);
                        fourthSheet.SetCellValue("H10", formularTT6);
                    }

                    formularTT = string.Empty;
                    string alpha = string.Empty;
                    for (int k = 9; k <= (8 + lstLeavingReason.Count()); k++)
                    {
                        alpha = UtilsBusiness.GetExcelColumnName(k);
                        formularTT = "=SUM(" + alpha;
                        for (int i = 0; i < dicTotal.Count; i++)
                        {
                            formularTT += dicTotal[i] + "," + alpha;
                        }

                        formularTT = formularTT.Substring(0, formularTT.Length - 2) + ")";
                        firstSheet.SetCellValue((alpha + 10), formularTT);

                        if (showSheetFemale)
                        {
                            secondSheet.SetCellValue((alpha + 10), formularTT);
                        }
                        if (showSheetEthenic)
                        {
                            thirdSheet.SetCellValue((alpha + 10), formularTT);
                        }
                        if (showSheetFemaleEthenic)
                        {
                            fourthSheet.SetCellValue((alpha + 10), formularTT);
                        }

                        alpha = string.Empty;
                        formularTT = string.Empty;
                    }
                    #endregion
                }
            }
            #endregion

            if (showLastMonth)
            {
                #region
                firstSheet.SetColumnWidth('D', 0);
                if (showSheetFemale)
                {
                    secondSheet.SetColumnWidth('D', 0);
                }
                if (showSheetEthenic)
                {
                    thirdSheet.SetColumnWidth('D', 0);
                }
                if (showSheetFemaleEthenic)
                {
                    fourthSheet.SetColumnWidth('D', 0);
                }
                #endregion
            }

            firstSheet.SetFontName("Times New Roman", 0);
            if (showSheetFemale)
            {
                secondSheet.SetFontName("Times New Roman", 0);
            }
            else
            {
                secondSheet.Delete();
            }

            if (showSheetEthenic)
            {
                thirdSheet.SetFontName("Times New Roman", 0);
            }
            else
            {
                thirdSheet.Delete();
            }

            if (showSheetFemaleEthenic)
            {
                fourthSheet.SetFontName("Times New Roman", 0);
            }
            else
            {
                fourthSheet.Delete();
            }
            #endregion

            return oBook.ToStream();
        }


        //public Stream ExportSituationStatisticsBySuperVising(IDictionary<string, object> dic)
        //{
        //    //Đường dẫn template & Tên file xuất ra
        //    string template = Path.Combine(SystemParamsInFile.TEMPLATE_FOLDER, "Phong_So", SystemParamsInFile.SGD_TKBienDongHS_112017 + ".xls");
        //    IVTWorkbook oBook = VTExport.OpenWorkbook(template);
        //    IVTWorksheet firstSheet = oBook.GetSheet(1);
        //    IVTWorksheet secondSheet = oBook.GetSheet(2);
        //    IVTWorksheet thirdSheet = oBook.GetSheet(3);
        //    IVTWorksheet fourthSheet = oBook.GetSheet(4);

        //    //int schoolID = Utils.GetInt(dic, "SchoolID");
        //    int academicYearID = Utils.GetInt(dic, "AcademicYearID");
        //    // int? educationLevelID = Utils.GetInt(dic, "EducationLevel");
        //    int? appliedLevelID = Utils.GetInt(dic, "AppliedLevel");
        //    int? monthID = Utils.GetInt(dic, "MonthID");
        //    int? year = Utils.GetInt(dic, "Year");
        //    int provinceId = Utils.GetInt(dic, "ProvinceID");
        //    int? districtID = Utils.GetInt(dic, "DistrictID");
        //    string superVisingDeptName = Utils.GetString(dic, "SuperVisingDeptName");
        //    bool showSheetFemale = Utils.GetBool(dic, "SheetPupilFemale");
        //    bool showSheetEthenic = Utils.GetBool(dic, "SheetPupilEthnic");
        //    bool showSheetFemaleEthenic = Utils.GetBool(dic, "SheetPupilFemaleEthnic");
        //    bool fillLeavingReason = false;

        //    #region // Get data
        //    Province objProvince = ProvinceBusiness.Find(provinceId);
        //    IQueryable<District> lstDistrict = DistrictBusiness.All.Where(x => x.ProvinceID == provinceId);
        //    if (districtID.HasValue && districtID.Value > 0)
        //    {
        //        lstDistrict = lstDistrict.Where(x => x.DistrictID == districtID.Value);
        //    }
        //    List<int> lstDistrictID = lstDistrict.Select(x => x.DistrictID).ToList();

        //    IQueryable<EducationLevel> lstEducation = EducationLevelBusiness.All.Where(x => x.Grade == 1 || x.Grade == 2 || x.Grade == 3 && x.IsActive == true);
        //    if (appliedLevelID.HasValue && appliedLevelID.Value > 0)
        //    {
        //        lstEducation = lstEducation.Where(x => x.Grade == appliedLevelID);
        //    }
        //    List<int> lstEducationID = lstEducation.Select(x => x.EducationLevelID).ToList();

        //    // Danh sach nam hoc
        //    IQueryable<AcademicYear> lstYear = AcademicYearBusiness.All.Where(x => (x.Year == year.Value) && (x.IsActive == true));
        //    List<int> lstAcademicYearID = lstYear.Select(x => x.AcademicYearID).ToList();

        //    //lay danh sach truong theo nam hoc
        //    List<int> lstSchoolID = lstYear.Select(x => x.SchoolID).Distinct().ToList();
        //    IQueryable<SchoolProfile> lstSchoolAll = SchoolProfileBusiness.All.Where(x => lstSchoolID.Contains(x.SchoolProfileID)
        //                                                                                && x.ProvinceID == provinceId
        //                                                                                && x.DistrictID.HasValue
        //                                                                                && lstDistrictID.Contains(x.DistrictID.Value)
        //                                                                                && x.IsActive == true);
        //    lstSchoolID = lstSchoolAll.Select(x => x.SchoolProfileID).ToList();
        //    lstYear = lstYear.Where(x => lstSchoolID.Contains(x.SchoolID));
        //    lstAcademicYearID = lstYear.Select(x => x.AcademicYearID).ToList();
        //    // Danh sach lop hoc cua toan truong
        //    IQueryable<ClassProfile> lstClassProfile = this.ClassProfileBusiness.All.Where(x => x.IsActive == true
        //                                                                    && (lstSchoolID.Contains(x.SchoolID)
        //                                                                    && lstAcademicYearID.Contains(x.AcademicYearID))
        //                                                                    && lstEducationID.Contains(x.EducationLevelID))
        //                                                                  .OrderBy(p => p.EducationLevelID).ThenBy(u => u.OrderNumber)
        //                                                                  .ThenBy(u => u.DisplayName);
        //    List<int> lstClassID = lstClassProfile.Select(p => p.ClassProfileID).ToList();

        //    // học sinh trong truong
        //    IQueryable<PupilOfSchool> lstPupilOfSchool = PupilOfSchoolBusiness.All.Where(x => lstSchoolID.Contains(x.SchoolID));
        //    List<int> lstPupilID = lstPupilOfSchool.Select(x => x.PupilID).ToList();

        //    // Hoc sinh trong lop                       
        //    List<PupilOfClassBO> lstPupilOfClassBO = this.ListPupilOfClassByParameter(lstClassID, lstSchoolID, lstAcademicYearID);

        //    // Danh sach hoc sinh chuyen truong
        //    List<SchoolMovementBO> lstSchoolMovement = SchoolMovementBusiness.GetListSchoolMovementByParameter(lstSchoolID, lstAcademicYearID, lstPupilID).ToList();
        //    // danh sach ly do nghi hoc
        //    List<LeavingReason> lstLeavingReason = LeavingReasonBusiness.All.Where(x => x.IsActive).ToList();

        //    //Danh sach hoc sinh nghi hoc
        //    List<PupilLeavingOffBO> lstPupilLeavingOff = PupilLeavingOffBusiness.ListPupilLeavingOff(lstSchoolID, lstAcademicYearID).ToList();
        //    #endregion

        //    DateTime currenDate = DateTime.Now;

        //    int _month = 0;
        //    int _year = 0;
        //    if (monthID.HasValue && monthID.Value > 0)
        //    {
        //        string _monthID = monthID.ToString();
        //        int _length = _monthID.Length;
        //        string _strMonth = string.Empty;
        //        string _strYear = string.Empty;

        //        if (_length == 5)
        //            _strMonth = _monthID.Substring(4, 1).ToString();
        //        else
        //            _strMonth = _monthID.Substring(4, 2).ToString();

        //        _strYear = _monthID.Substring(0, 4).ToString();

        //        Int32.TryParse(_strMonth, out _month);
        //        Int32.TryParse(_strYear, out _year);
        //    }

        //    int _lastMonth = 0;
        //    int _lastYear = 0;
        //    if (_month == 1)
        //    {
        //        _lastYear = _year - 1;
        //        _lastMonth = 12;
        //    }
        //    else
        //    {
        //        _lastYear = _year;
        //        _lastMonth = _month - 1;
        //    }

        //    DateTime valueMinDate = DateTime.Now;
        //    DateTime valueMaxDate = DateTime.Now;
        //    DateTime valueMinLastMonth = new DateTime(_lastYear, _lastMonth, 1);
        //    DateTime valueMaxLastMonth = valueMinLastMonth.AddMonths(1).AddDays(-1);
        //    DateTime valueOfMonthSelected = new DateTime(_year, _month, 1);
        //    DateTime valueOfCurrent = new DateTime(currenDate.Year, currenDate.Month, 1);

        //    if (valueOfMonthSelected == valueOfCurrent)
        //    {
        //        valueMinDate = new DateTime(currenDate.Year, currenDate.Month, 1);
        //        valueMaxDate = DateTime.Now;
        //    }
        //    else
        //    {
        //        valueMinDate = new DateTime(_year, _month, 1);
        //        valueMaxDate = valueMinDate.AddMonths(1).AddDays(-1);
        //    }

        //    bool showLastMonth = false;
        //    if (_month == 8)
        //    {
        //        showLastMonth = true;
        //    }

        //    string _strDate = string.Format("[Province], ngày {0} tháng {1} năm {2}", currenDate.Day, currenDate.Month, currenDate.Year);

        //    if (objProvince != null)
        //    {
        //        _strDate = _strDate.Replace("[Province]", objProvince.ProvinceName).ToString();
        //    }
        //    else
        //    {
        //        _strDate = _strDate.Replace("[Province]", ".........").ToString();
        //    }

        //    #region // Fill tieu de
        //    firstSheet.SetCellValue("A3", superVisingDeptName);
        //    firstSheet.SetCellValue("E4", _strDate);
        //    firstSheet.SetCellValue("E6", string.Format("THỐNG KÊ TÌNH HÌNH BIẾN ĐỘNG HỌC SINH THÁNG {0}/{1}", _month, _year));
        //    firstSheet.SetCellValue("D8", string.Format("Số HS cuối tháng {0}/{1}", (_lastMonth), _lastYear));
        //    firstSheet.SetCellValue("E8", string.Format("Số HS cuối tháng {0}/{1}", _month, _year));
        //    firstSheet.SetCellValue("F8", string.Format("Số HS chuyển đến tháng {0}/{1}", _month, _year));
        //    firstSheet.SetCellValue("G8", string.Format("Số HS chuyển đi tháng {0}/{1}", _month, _year));
        //    firstSheet.SetCellValue("H8", string.Format("Số HS thôi học tháng {0}/{1}", _month, _year));

        //    if (showSheetFemale)
        //    {
        //        secondSheet.SetCellValue("A3", superVisingDeptName);
        //        secondSheet.SetCellValue("E4", _strDate);
        //        secondSheet.SetCellValue("E6", string.Format("THỐNG KÊ TÌNH HÌNH BIẾN ĐỘNG HỌC SINH NỮ THÁNG {0}/{1}", _month, _year));
        //        secondSheet.SetCellValue("D8", string.Format("Số HS cuối tháng {0}/{1}", (_lastMonth), _lastYear));
        //        secondSheet.SetCellValue("E8", string.Format("Số HS cuối tháng {0}/{1}", _month, _year));
        //        secondSheet.SetCellValue("F8", string.Format("Số HS chuyển đến tháng {0}/{1}", _month, _year));
        //        secondSheet.SetCellValue("G8", string.Format("Số HS chuyển đi tháng {0}/{1}", _month, _year));
        //        secondSheet.SetCellValue("H8", string.Format("Số HS thôi học tháng {0}/{1}", _month, _year));
        //    }

        //    if (showSheetEthenic)
        //    {
        //        thirdSheet.SetCellValue("A3", superVisingDeptName);
        //        thirdSheet.SetCellValue("E4", _strDate);
        //        thirdSheet.SetCellValue("E6", string.Format("THỐNG KÊ TÌNH HÌNH BIẾN ĐỘNG HỌC SINH DÂN TỘC THÁNG {0}/{1}", _month, _year));
        //        thirdSheet.SetCellValue("D8", string.Format("Số HS cuối tháng {0}/{1}", (_lastMonth), _lastYear));
        //        thirdSheet.SetCellValue("E8", string.Format("Số HS cuối tháng {0}/{1}", _month, _year));
        //        thirdSheet.SetCellValue("F8", string.Format("Số HS chuyển đến tháng {0}/{1}", _month, _year));
        //        thirdSheet.SetCellValue("G8", string.Format("Số HS chuyển đi tháng {0}/{1}", _month, _year));
        //        thirdSheet.SetCellValue("H8", string.Format("Số HS thôi học tháng {0}/{1}", _month, _year));
        //    }

        //    if (showSheetFemaleEthenic)
        //    {
        //        fourthSheet.SetCellValue("A3", superVisingDeptName);
        //        fourthSheet.SetCellValue("E4", _strDate);
        //        fourthSheet.SetCellValue("E6", string.Format("THỐNG KÊ TÌNH HÌNH BIẾN ĐỘNG HỌC SINH NỮ DÂN TỘC THÁNG {0}/{1}", _month, _year));
        //        fourthSheet.SetCellValue("D8", string.Format("Số HS cuối tháng {0}/{1}", (_lastMonth), _lastYear));
        //        fourthSheet.SetCellValue("E8", string.Format("Số HS cuối tháng {0}/{1}", _month, _year));
        //        fourthSheet.SetCellValue("F8", string.Format("Số HS chuyển đến tháng {0}/{1}", _month, _year));
        //        fourthSheet.SetCellValue("G8", string.Format("Số HS chuyển đi tháng {0}/{1}", _month, _year));
        //        fourthSheet.SetCellValue("H8", string.Format("Số HS thôi học tháng {0}/{1}", _month, _year));
        //    }

        //    if (lstLeavingReason.Count() > 0)
        //    {
        //        firstSheet.SetCellValue(8, 9, string.Format("Phân tích số HS thôi học tháng {0}/{1}", _month, _year));
        //        firstSheet.GetRange(8, 9, 8, 8 + lstLeavingReason.Count()).Merge();
        //        firstSheet.GetRange(8, 9, 8, 8 + lstLeavingReason.Count()).SetHAlign(VTHAlign.xlHAlignCenter);
        //        firstSheet.GetRange(8, 9, 8, 8 + lstLeavingReason.Count()).SetFontStyle(true, System.Drawing.Color.Black, false, 11, false, false);

        //        if (showSheetFemale)
        //        {
        //            secondSheet.SetCellValue(8, 9, string.Format("Phân tích số HS thôi học tháng {0}/{1}", _month, _year));
        //            secondSheet.GetRange(8, 9, 8, 8 + lstLeavingReason.Count()).Merge();
        //            secondSheet.GetRange(8, 9, 8, 8 + lstLeavingReason.Count()).SetHAlign(VTHAlign.xlHAlignCenter);
        //            secondSheet.GetRange(8, 9, 8, 8 + lstLeavingReason.Count()).SetFontStyle(true, System.Drawing.Color.Black, false, 11, false, false);
        //        }

        //        if (showSheetEthenic)
        //        {
        //            thirdSheet.SetCellValue(8, 9, string.Format("Phân tích số HS thôi học tháng {0}/{1}", _month, _year));
        //            thirdSheet.GetRange(8, 9, 8, 8 + lstLeavingReason.Count()).Merge();
        //            thirdSheet.GetRange(8, 9, 8, 8 + lstLeavingReason.Count()).SetHAlign(VTHAlign.xlHAlignCenter);
        //            thirdSheet.GetRange(8, 9, 8, 8 + lstLeavingReason.Count()).SetFontStyle(true, System.Drawing.Color.Black, false, 11, false, false);
        //        }

        //        if (showSheetFemaleEthenic)
        //        {
        //            fourthSheet.SetCellValue(8, 9, string.Format("Phân tích số HS thôi học tháng {0}/{1}", _month, _year));
        //            fourthSheet.GetRange(8, 9, 8, 8 + lstLeavingReason.Count()).Merge();
        //            fourthSheet.GetRange(8, 9, 8, 8 + lstLeavingReason.Count()).SetHAlign(VTHAlign.xlHAlignCenter);
        //            fourthSheet.GetRange(8, 9, 8, 8 + lstLeavingReason.Count()).SetFontStyle(true, System.Drawing.Color.Black, false, 11, false, false);
        //        }
        //    }
        //    #endregion

        //    string formular = string.Empty;
        //    string formularAllEducation = string.Empty;
        //    int startFillEducation = 11;
        //    int startRow = 11;
        //    int STT = 0;
        //    List<int> _lstPupilID = new List<int>();
        //    AcademicYear objYearOfSchool = null;
        //    IDictionary<int, int> dicTotal = new Dictionary<int, int>();
        //    List<PupilOfClassBO> lstPupilOfClassInSchool = new List<PupilOfClassBO>();
        //    List<PupilLeavingOffBO> totalFemaleStopLearn = new List<PupilLeavingOffBO>();
        //    List<PupilLeavingOffBO> totalEthnicStopLearn = new List<PupilLeavingOffBO>();
        //    List<PupilLeavingOffBO> totalFemaleEthnicStopLearn = new List<PupilLeavingOffBO>();
        //    List<PupilLeavingOffBO> totalPupilStopLearnByReason = new List<PupilLeavingOffBO>();
        //    List<PupilLeavingOffBO> totalPupilStopLearn = new List<PupilLeavingOffBO>();
        //    List<SchoolMovementBO> totalFemaleClassMoveFrom = new List<SchoolMovementBO>();
        //    List<SchoolMovementBO> totalEthnicClassMoveFrom = new List<SchoolMovementBO>();
        //    List<SchoolMovementBO> totalFemaleEthnicClassMoveFrom = new List<SchoolMovementBO>();

        //    DateTime startDateOfSchool = new DateTime();
        //    DateTime startContinueDate = new DateTime();
        //    DateTime endDateOfSchool = new DateTime();

        //    int index = 0;
        //    #region // Fill nội dung
        //    foreach (var itemDis in lstDistrict)
        //    {
        //        #region
        //        firstSheet.SetCellValue(("B" + startRow), itemDis.DistrictName);
        //        if (showSheetFemale)
        //        {
        //            secondSheet.SetCellValue(("B" + startRow), itemDis.DistrictName);
        //        }
        //        if (showSheetEthenic)
        //        {
        //            thirdSheet.SetCellValue(("B" + startRow), itemDis.DistrictName);
        //        }
        //        if (showSheetFemaleEthenic)
        //        {
        //            fourthSheet.SetCellValue(("B" + startRow), itemDis.DistrictName);
        //        }
        //        #endregion

        //        var lstSchoolProfile = lstSchoolAll.Where(x => x.DistrictID == itemDis.DistrictID);

        //        foreach (var itemSchool in lstSchoolProfile)
        //        {
        //            startRow++;
        //            STT++;
        //            objYearOfSchool = lstYear.Where(x => x.SchoolID == itemSchool.SchoolProfileID).FirstOrDefault();

        //            // Ngày bắt đầu năm học của trường
        //            startDateOfSchool = new DateTime(objYearOfSchool.FirstSemesterStartDate.Value.Year, objYearOfSchool.FirstSemesterStartDate.Value.Month, 1);
        //            startContinueDate = startDateOfSchool.AddMonths(1).AddDays(-1);
        //            endDateOfSchool = objYearOfSchool.SecondSemesterEndDate.Value;

        //            lstPupilOfClassInSchool = lstPupilOfClassBO.Where(x => x.SchoolID == itemSchool.SchoolProfileID)
        //                                                        .Where(x => x.EnrolmentDate <= objYearOfSchool.FirstSemesterStartDate).ToList();
        //            var lstPuilSchoolMovementTo_FirstSemeter = lstSchoolMovement.Where(x => x.MovedDate >= valueMinDate && x.MovedDate <= valueMaxDate)
        //                                                                        .Where(x => x.MovedDate > objYearOfSchool.FirstSemesterStartDate)
        //                                                                        .Where(x => x.MovedToSchoolID == itemSchool.SchoolProfileID).ToList();
        //            lstPupilID = lstPuilSchoolMovementTo_FirstSemeter.Select(x => x.PupilID).ToList();
        //            var lst1 = lstPupilOfClassInSchool.Where(x => lstPupilID.Contains(x.PupilID) && x.Status != 1 && x.Status != 4).ToList();
        //            if (lst1.Count() > 0)
        //            {
        //                foreach (var item in lst1)
        //                {
        //                    lstPupilOfClassInSchool.Remove(item);
        //                }
        //            }

        //            #region // Tổng học sinh đầu năm học
        //            // fill firstSheet    
        //            int _countPupil = 0;
        //            var totalPupil = lstPupilOfClassInSchool.ToList();
        //            lstPupilID = totalPupil.Select(x => x.PupilID).Distinct().ToList();
        //            _countPupil = lstPupilID.Count();

        //            firstSheet.SetCellValue(("A" + startRow), STT);
        //            firstSheet.GetRange(startRow, 1, startRow, 1).SetHAlign(VTHAlign.xlHAlignCenter);
        //            firstSheet.SetCellValue(("B" + startRow), itemSchool.SchoolName);
        //            firstSheet.SetCellValue(("C" + startRow), _countPupil);

        //            #region // Sheet 2
        //            if (showSheetFemale)
        //            {  // fill secondSheet - tổng số HS nữ                     
        //                totalPupil = lstPupilOfClassInSchool.Where(x => x.Genre.HasValue && x.Genre == 0).ToList();
        //                lstPupilID = totalPupil.Select(x => x.PupilID).Distinct().ToList();
        //                secondSheet.SetCellValue(("A" + startRow), STT);
        //                secondSheet.GetRange(startRow, 1, startRow, 1).SetHAlign(VTHAlign.xlHAlignCenter);
        //                secondSheet.SetCellValue(("B" + startRow), itemSchool.SchoolName);
        //                secondSheet.SetCellValue(("C" + startRow), lstPupilID.Count());
        //            }
        //            #endregion

        //            #region // Sheet 3
        //            if (showSheetEthenic)
        //            {// fill thirdSheet - tổng số HS dân tộc - Loại bỏ dân tộc kinh, người nước ngoài
        //                totalPupil = lstPupilOfClassInSchool.Where(x => x.EthnicID.HasValue && (x.EthnicID.Value != 1 && x.EthnicID.Value != 79)).ToList();
        //                lstPupilID = totalPupil.Select(x => x.PupilID).Distinct().ToList();
        //                thirdSheet.SetCellValue(("A" + startRow), STT);
        //                thirdSheet.GetRange(startRow, 1, startRow, 1).SetHAlign(VTHAlign.xlHAlignCenter);
        //                thirdSheet.SetCellValue(("B" + startRow), itemSchool.SchoolName);
        //                thirdSheet.SetCellValue(("C" + startRow), lstPupilID.Count());
        //            }
        //            #endregion

        //            #region // Sheet 4
        //            if (showSheetFemaleEthenic)
        //            {// fill fourthSheet - tổng số HS nữ dân tộc - Loại bỏ dân tộc kinh, người nước ngoài
        //                totalPupil = lstPupilOfClassInSchool.Where(x => x.EthnicID.HasValue && (x.EthnicID.Value != 1 && x.EthnicID.Value != 79)
        //                                                            && x.Genre.HasValue && x.Genre == 0).ToList();
        //                lstPupilID = totalPupil.Select(x => x.PupilID).Distinct().ToList();
        //                fourthSheet.SetCellValue(("A" + startRow), STT);
        //                fourthSheet.GetRange(startRow, 1, startRow, 1).SetHAlign(VTHAlign.xlHAlignCenter);
        //                fourthSheet.SetCellValue(("B" + startRow), itemSchool.SchoolName);
        //                fourthSheet.SetCellValue(("C" + startRow), lstPupilID.Count());
        //            }
        //            #endregion
        //            #endregion

        //            #region// Tổng học sinh cuối tháng (X - 1)
        //            // fill firstSheet
        //            totalPupil = lstPupilOfClassInSchool.ToList();

        //            var lstPupilInLastMonth = lstPupilOfClassBO.Where(x => x.SchoolID == itemSchool.SchoolProfileID)
        //                                            .Where(x => x.AssignedDate <= valueMaxLastMonth)
        //                                            .Where(x => x.Status == 1).ToList();
        //            foreach (var itemLastMonth in lstPupilInLastMonth)
        //            {
        //                var check = totalPupil.Where(x => x.PupilID == itemLastMonth.PupilID).FirstOrDefault();
        //                if (check != null)
        //                    continue;
        //                totalPupil.Add(itemLastMonth);
        //            }

        //            var pupilStopLearn = lstPupilLeavingOff.Where(x => x.SchoolID == itemSchool.SchoolProfileID)
        //                                                    .Where(x => x.LeavingDate >= valueMinLastMonth && x.LeavingDate <= valueMaxLastMonth).ToList();
        //            lstPupilID = pupilStopLearn.Select(x => x.PupilID).ToList();
        //            if (lstPupilID.Count() > 0)
        //            {
        //                totalPupil = totalPupil.Where(x => !lstPupilID.Contains(x.PupilID)).ToList();
        //            }

        //            lstPupilID = totalPupil.Select(x => x.PupilID).Distinct().ToList();
        //            _countPupil = lstPupilID.Count();
        //            firstSheet.SetCellValue(("D" + startRow), _countPupil);

        //            if (showSheetFemale)
        //            {
        //                // fill secondSheet - HS nữ
        //                var totalFemale = totalPupil.Where(x => x.Genre.HasValue && x.Genre == 0).ToList();
        //                lstPupilID = totalFemale.Select(x => x.PupilID).Distinct().ToList();
        //                _countPupil = lstPupilID.Count();
        //                secondSheet.SetCellValue(("D" + startRow), _countPupil);
        //            }
        //            if (showSheetEthenic)
        //            {
        //                // fill thirdSheet - HS dân tộc
        //                var totalEthnic = totalPupil.Where(x => x.EthnicID.HasValue && (x.EthnicID.Value != 1 && x.EthnicID.Value != 79)).ToList();
        //                lstPupilID = totalEthnic.Select(x => x.PupilID).Distinct().ToList();
        //                _countPupil = lstPupilID.Count();
        //                thirdSheet.SetCellValue(("D" + startRow), _countPupil);
        //            }
        //            if (showSheetFemaleEthenic)
        //            {
        //                // fill fourthSheet - HS nữ dân tộc
        //                var totalFemaleEthnic = totalPupil.Where(x => x.EthnicID.HasValue && (x.EthnicID.Value != 1 && x.EthnicID.Value != 79)
        //                                                         && (x.Genre.HasValue && x.Genre == 0)).ToList();
        //                lstPupilID = totalFemaleEthnic.Select(x => x.PupilID).Distinct().ToList();
        //                _countPupil = lstPupilID.Count();
        //                fourthSheet.SetCellValue(("D" + startRow), _countPupil);
        //            }
        //            #endregion

        //            #region // Tổng học sinh cuối tháng X
        //            // fill firstSheet
        //            totalPupil = lstPupilOfClassBO.Where(x => x.SchoolID == itemSchool.SchoolProfileID)
        //                                           .Where(x => x.AssignedDate <= valueMaxDate)
        //                                           .Where(x => x.Status == 1).ToList();

        //            _countPupil = totalPupil.Count();
        //            firstSheet.SetCellValue(("E" + startRow), _countPupil);

        //            if (showSheetFemale)
        //            {
        //                // fill secondSheet - HS nữ
        //                var totalFemale = totalPupil.Where(x => x.Genre.HasValue && x.Genre == 0).ToList();

        //                _countPupil = totalFemale.Count();
        //                secondSheet.SetCellValue(("E" + startRow), _countPupil);
        //            }
        //            if (showSheetEthenic)
        //            {
        //                // fill thirdSheet - HS dân tộc
        //                var totalEthnic = totalPupil.Where(x => x.EthnicID.HasValue && (x.EthnicID.Value != 1 && x.EthnicID.Value != 79)).ToList();

        //                _countPupil = totalEthnic.Count();
        //                thirdSheet.SetCellValue(("E" + startRow), _countPupil);
        //            }
        //            if (showSheetFemaleEthenic)
        //            {
        //                // fill fourthSheet - HS nữ dân tộc
        //                var totalFemaleEthnic = totalPupil.Where(x => x.EthnicID.HasValue && (x.EthnicID.Value != 1 && x.EthnicID.Value != 79)
        //                                                         && (x.Genre.HasValue && x.Genre == 0)).ToList();

        //                _countPupil = totalFemaleEthnic.Count();
        //                fourthSheet.SetCellValue(("E" + startRow), _countPupil);
        //            }
        //            #endregion

        //            #region //Số HS chuyển đến tháng X
        //            // fill firstSheet                   
        //            // Hình thức trúng tuyển (của năm học hiện tại) - Chuyển đến từ trường khác
        //            var lstPupilEnrolmentTypeCurrent = lstPupilOfClassBO.Where(x => x.SchoolID == itemSchool.SchoolProfileID)
        //                                    .Where(x => x.AssignedDate >= valueMinDate && x.AssignedDate <= valueMaxDate)
        //                                    .Where(x => x.EnrolmentDate >= objYearOfSchool.FirstSemesterStartDate)
        //                                    .Where(x => x.EnrolmentType.HasValue && x.EnrolmentType == SystemParamsInFile.ENROLMENT_TYPE_MOVED_FROM_OTHER_SCHOOL)
        //                                    .Where(x => x.Status == 1).ToList();
        //            //Tổng học sinh chuyển trường đến trong tháng x
        //            var lstPupilSchoolMoveTo = lstSchoolMovement.Where(x => x.MovedToSchoolID == itemSchool.SchoolProfileID)
        //                                                    .Where(x => x.MovedDate >= valueMinDate && x.MovedDate <= valueMaxDate).ToList();
        //            lstPupilID = lstPupilSchoolMoveTo.Select(x => x.PupilID).ToList();
        //            if (lstPupilID.Count() > 0)
        //            {
        //                lstPupilEnrolmentTypeCurrent = lstPupilEnrolmentTypeCurrent.Where(x => !lstPupilID.Contains(x.PupilID)).ToList();
        //            }

        //            var totalPupilSchoolMoveTo = lstPupilOfClassBO.Where(x => x.SchoolID == itemSchool.SchoolProfileID && lstPupilID.Contains(x.PupilID))
        //                                                        .Where(x => x.AssignedDate >= valueMinDate && x.AssignedDate <= valueMaxDate)
        //                                                        .Where(x => x.Status == 1).ToList();

        //            firstSheet.SetCellValue(("F" + startRow), (totalPupilSchoolMoveTo.Count() + lstPupilEnrolmentTypeCurrent.Count()));

        //            if (showSheetFemale)
        //            {
        //                // fill secondSheet - HS nữ
        //                // Hình thức trúng tuyển (của năm học hiện tại) - Chuyển đến từ trường khác
        //                var lstPupilFemaleEnrolmentTypeCurrent = lstPupilEnrolmentTypeCurrent.Where(x => x.Genre.HasValue && x.Genre == 0).ToList();
        //                // Tổng học sinh nữ chuyển trường đến trong tháng x
        //                var totalFemaleClassMoveTo = lstPupilSchoolMoveTo.Where(x => x.Genre == 0).ToList();
        //                lstPupilID = totalFemaleClassMoveTo.Select(x => x.PupilID).ToList();
        //                totalPupilSchoolMoveTo = lstPupilOfClassBO.Where(x => x.SchoolID == itemSchool.SchoolProfileID && lstPupilID.Contains(x.PupilID))
        //                                                        .Where(x => x.AssignedDate >= valueMinDate && x.AssignedDate <= valueMaxDate)
        //                                                        .Where(x => x.Status == 1).ToList();

        //                secondSheet.SetCellValue(("F" + startRow), (totalPupilSchoolMoveTo.Count() + lstPupilFemaleEnrolmentTypeCurrent.Count()));
        //            }
        //            if (showSheetEthenic)
        //            {
        //                // fill thirdSheet - HS dân tộc
        //                // Hình thức trúng tuyển (của năm học hiện tại) - Chuyển đến từ trường khác
        //                var lstPupilEthnicEnrolmentTypeCurrent = lstPupilEnrolmentTypeCurrent.Where(x => x.EthnicID.HasValue && (x.EthnicID.Value != 1 && x.EthnicID.Value != 79)).ToList();
        //                // Tổng học sinh dân tộc chuyển trường đến trong tháng x
        //                var totalEthnicClassMoveTo = lstPupilSchoolMoveTo.Where(x => x.EthnicID.HasValue && (x.EthnicID.Value != 1 && x.EthnicID.Value != 79)).ToList();
        //                lstPupilID = totalEthnicClassMoveTo.Select(x => x.PupilID).ToList();

        //                totalPupilSchoolMoveTo = lstPupilOfClassBO.Where(x => x.SchoolID == itemSchool.SchoolProfileID && lstPupilID.Contains(x.PupilID))
        //                                                        .Where(x => x.AssignedDate >= valueMinDate && x.AssignedDate <= valueMaxDate)
        //                                                        .Where(x => x.Status == 1).ToList();

        //                thirdSheet.SetCellValue(("F" + startRow), (totalPupilSchoolMoveTo.Count() + lstPupilEthnicEnrolmentTypeCurrent.Count()));
        //            }
        //            if (showSheetFemaleEthenic)
        //            {
        //                // fill fourthSheet - HS nữ dân tộc
        //                // Hình thức trúng tuyển (của năm học hiện tại) - Chuyển đến từ trường khác
        //                var lstPupilFemaleEthnicEnrolmentTypeCurrent = lstPupilEnrolmentTypeCurrent.Where(x => x.EthnicID.HasValue && (x.EthnicID.Value != 1 && x.EthnicID.Value != 79)
        //                                                                                            && (x.Genre.HasValue && x.Genre == 0)).ToList();
        //                // Tổng học sinh dân tộc nữ chuyển trường đến trong tháng x
        //                var totalFemaleEthnicClassMoveTo = lstPupilSchoolMoveTo.Where(x => x.EthnicID.HasValue && (x.EthnicID.Value != 1 && x.EthnicID.Value != 79)
        //                                                         && (x.Genre == 0)).ToList();
        //                lstPupilID = totalFemaleEthnicClassMoveTo.Select(x => x.PupilID).ToList();

        //                totalPupilSchoolMoveTo = lstPupilOfClassBO.Where(x => x.SchoolID == itemSchool.SchoolProfileID && lstPupilID.Contains(x.PupilID))
        //                                                        .Where(x => x.AssignedDate >= valueMinDate && x.AssignedDate <= valueMaxDate)
        //                                                        .Where(x => x.Status == 1).ToList();

        //                fourthSheet.SetCellValue(("F" + startRow), (totalPupilSchoolMoveTo.Count() + lstPupilFemaleEthnicEnrolmentTypeCurrent.Count()));
        //            }
        //            #endregion

        //            #region// Số hoc sinh chuyen di thang X
        //            // fill firstSheet
        //            var lstPupilSchoolMoveFrom = lstSchoolMovement.Where(x => x.SchoolID == itemSchool.SchoolProfileID)
        //                                                   .Where(x => x.MovedDate >= valueMinDate && x.MovedDate <= valueMaxDate).ToList();
        //            lstPupilID = lstPupilSchoolMoveFrom.Select(x => x.PupilID).ToList();
        //            var totalPupilMoveGoOnMonth = lstPupilOfClassBO.Where(x => x.SchoolID == itemSchool.SchoolProfileID && lstPupilID.Contains(x.PupilID)).ToList();
        //            firstSheet.SetCellValue(("G" + startRow), totalPupilMoveGoOnMonth.Count());

        //            if (showSheetFemale)
        //            {
        //                // fill secondSheet - HS nữ
        //                totalFemaleClassMoveFrom = lstPupilSchoolMoveFrom.Where(x => x.Genre == 0).ToList();
        //                lstPupilID = totalFemaleClassMoveFrom.Select(x => x.PupilID).ToList();
        //                totalPupilMoveGoOnMonth = lstPupilOfClassBO.Where(x => x.SchoolID == itemSchool.SchoolProfileID && lstPupilID.Contains(x.PupilID)).ToList();
        //                secondSheet.SetCellValue(("G" + startRow), totalPupilMoveGoOnMonth.Count());
        //            }
        //            if (showSheetEthenic)
        //            {
        //                // fill thirdSheet - HS dân tộc
        //                totalEthnicClassMoveFrom = lstPupilSchoolMoveFrom.Where(x => x.EthnicID.HasValue && (x.EthnicID.Value != 1 && x.EthnicID.Value != 79)).ToList();
        //                lstPupilID = totalEthnicClassMoveFrom.Select(x => x.PupilID).ToList();
        //                totalPupilMoveGoOnMonth = lstPupilOfClassBO.Where(x => x.SchoolID == itemSchool.SchoolProfileID && lstPupilID.Contains(x.PupilID)).ToList();
        //                thirdSheet.SetCellValue(("G" + startRow), totalPupilMoveGoOnMonth.Count());
        //            }
        //            if (showSheetFemaleEthenic)
        //            {
        //                // fill fourthSheet - HS nữ dân tộc
        //                totalFemaleEthnicClassMoveFrom = lstPupilSchoolMoveFrom.Where(x => x.EthnicID.HasValue && (x.EthnicID.Value != 1 && x.EthnicID.Value != 79)
        //                                                         && (x.Genre == 0)).ToList();
        //                lstPupilID = totalFemaleEthnicClassMoveFrom.Select(x => x.PupilID).ToList();
        //                totalPupilMoveGoOnMonth = lstPupilOfClassBO.Where(x => x.SchoolID == itemSchool.SchoolProfileID && lstPupilID.Contains(x.PupilID)).ToList();
        //                fourthSheet.SetCellValue(("G" + startRow), totalPupilMoveGoOnMonth.Count());
        //            }
        //            #endregion

        //            #region// Số hoc sinh thoi hoc thang X
        //            // fill firstSheet
        //            totalPupilStopLearn = lstPupilLeavingOff.Where(x => x.LeavingDate >= valueMinDate && x.LeavingDate <= valueMaxDate
        //                                                        && x.SchoolID == itemSchool.SchoolProfileID).ToList();

        //            lstPupilID = totalPupilStopLearn.Select(x => x.PupilID).ToList();
        //            var totalPupilStopLearnOnMonth = lstPupilOfClassBO.Where(x => x.SchoolID == itemSchool.SchoolProfileID && lstPupilID.Contains(x.PupilID))
        //                                                                .Where(x => x.Status == 4).ToList();

        //            firstSheet.SetCellValue(("H" + startRow), totalPupilStopLearnOnMonth.Count());
        //            if (showSheetFemale)
        //            {
        //                // fill secondSheet - HS nữ
        //                totalFemaleStopLearn = totalPupilStopLearn.Where(x => x.Genre == 0).ToList();
        //                lstPupilID = totalFemaleStopLearn.Select(x => x.PupilID).ToList();
        //                totalPupilStopLearnOnMonth = lstPupilOfClassBO.Where(x => x.SchoolID == itemSchool.SchoolProfileID && lstPupilID.Contains(x.PupilID))
        //                                                                .Where(x => x.Status == 4).ToList();
        //                secondSheet.SetCellValue(("H" + startRow), totalPupilStopLearnOnMonth.Count());
        //            }
        //            if (showSheetEthenic)
        //            {
        //                // fill thirdSheet - HS dân tộc
        //                totalEthnicStopLearn = totalPupilStopLearn.Where(x => x.EthnicID.HasValue && (x.EthnicID.Value != 1 && x.EthnicID.Value != 79)).ToList();
        //                lstPupilID = totalEthnicStopLearn.Select(x => x.PupilID).ToList();
        //                totalPupilStopLearnOnMonth = lstPupilOfClassBO.Where(x => x.SchoolID == itemSchool.SchoolProfileID && lstPupilID.Contains(x.PupilID))
        //                                                                .Where(x => x.Status == 4).ToList();
        //                thirdSheet.SetCellValue(("H" + startRow), totalPupilStopLearnOnMonth.Count());
        //            }
        //            if (showSheetFemaleEthenic)
        //            {
        //                // fill fourthSheet - HS nữ dân tộc
        //                totalFemaleEthnicStopLearn = totalPupilStopLearn.Where(x => x.EthnicID.HasValue && (x.EthnicID.Value != 1 && x.EthnicID.Value != 79)
        //                                                         && (x.Genre == 0)).ToList();
        //                lstPupilID = totalFemaleEthnicStopLearn.Select(x => x.PupilID).ToList();
        //                totalPupilStopLearnOnMonth = lstPupilOfClassBO.Where(x => x.SchoolID == itemSchool.SchoolProfileID && lstPupilID.Contains(x.PupilID))
        //                                                                .Where(x => x.Status == 4).ToList();
        //                fourthSheet.SetCellValue(("H" + startRow), totalPupilStopLearnOnMonth.Count());
        //            }
        //            #endregion

        //            #region// Fill lý do nghi hoc
        //            int p = 0;
        //            foreach (var itemReason in lstLeavingReason)
        //            {
        //                // fill tieu đề khi fillLeavingReason = false
        //                if (!fillLeavingReason)
        //                {
        //                    firstSheet.SetCellValue(9, (9 + p), itemReason.Resolution);
        //                    firstSheet.GetRange(9, (9 + p), 9, (9 + p)).SetHAlign(VTHAlign.xlHAlignCenter);
        //                    firstSheet.GetRange(9, (9 + p), 9, (9 + p)).WrapText();
        //                    firstSheet.GetRange(9, (9 + p), 9, (9 + p)).SetHAlignVer(NativeExcel.XlVAlign.xlVAlignCenter);
        //                    firstSheet.GetRange(9, (9 + p), 9, (9 + p)).SetFontStyle(true, System.Drawing.Color.Black, false, 10, false, false);
        //                    firstSheet.SetColumnWidth((9 + p), 12);

        //                    if (showSheetFemale)
        //                    {
        //                        secondSheet.SetCellValue(9, (9 + p), itemReason.Resolution);
        //                        secondSheet.GetRange(9, (9 + p), 9, (9 + p)).SetHAlign(VTHAlign.xlHAlignCenter);
        //                        secondSheet.GetRange(9, (9 + p), 9, (9 + p)).WrapText();
        //                        secondSheet.GetRange(9, (9 + p), 9, (9 + p)).SetHAlignVer(NativeExcel.XlVAlign.xlVAlignCenter);
        //                        secondSheet.GetRange(9, (9 + p), 9, (9 + p)).SetFontStyle(true, System.Drawing.Color.Black, false, 10, false, false);
        //                        secondSheet.SetColumnWidth((9 + p), 12);
        //                    }

        //                    if (showSheetEthenic)
        //                    {
        //                        thirdSheet.SetCellValue(9, (9 + p), itemReason.Resolution);
        //                        thirdSheet.GetRange(9, (9 + p), 9, (9 + p)).SetHAlign(VTHAlign.xlHAlignCenter);
        //                        thirdSheet.GetRange(9, (9 + p), 9, (9 + p)).WrapText();
        //                        thirdSheet.GetRange(9, (9 + p), 9, (9 + p)).SetHAlignVer(NativeExcel.XlVAlign.xlVAlignCenter);
        //                        thirdSheet.GetRange(9, (9 + p), 9, (9 + p)).SetFontStyle(true, System.Drawing.Color.Black, false, 10, false, false);
        //                        thirdSheet.SetColumnWidth((9 + p), 12);
        //                    }

        //                    if (showSheetFemaleEthenic)
        //                    {
        //                        fourthSheet.SetCellValue(9, (9 + p), itemReason.Resolution);
        //                        fourthSheet.GetRange(9, (9 + p), 9, (9 + p)).SetHAlign(VTHAlign.xlHAlignCenter);
        //                        fourthSheet.GetRange(9, (9 + p), 9, (9 + p)).WrapText();
        //                        fourthSheet.GetRange(9, (9 + p), 9, (9 + p)).SetHAlignVer(NativeExcel.XlVAlign.xlVAlignCenter);
        //                        fourthSheet.GetRange(9, (9 + p), 9, (9 + p)).SetFontStyle(true, System.Drawing.Color.Black, false, 10, false, false);
        //                        fourthSheet.SetColumnWidth((9 + p), 12);
        //                    }
        //                }

        //                totalPupilStopLearnByReason = totalPupilStopLearn.Where(x => x.LeavingReasonID == itemReason.LeavingReasonID).ToList();
        //                lstPupilID = totalPupilStopLearnByReason.Select(x => x.PupilID).ToList();
        //                totalPupilStopLearnOnMonth = lstPupilOfClassBO.Where(x => x.SchoolID == itemSchool.SchoolProfileID && lstPupilID.Contains(x.PupilID))
        //                                                                .Where(x => x.Status == 4).ToList();
        //                firstSheet.SetCellValue(startRow, (9 + p), totalPupilStopLearnOnMonth.Count());

        //                if (showSheetFemale)
        //                {
        //                    var totalFemaleStopLearnByReason = totalFemaleStopLearn.Where(x => x.LeavingReasonID == itemReason.LeavingReasonID);
        //                    lstPupilID = totalFemaleStopLearnByReason.Select(x => x.PupilID).ToList();
        //                    totalPupilStopLearnOnMonth = lstPupilOfClassBO.Where(x => x.SchoolID == itemSchool.SchoolProfileID && lstPupilID.Contains(x.PupilID))
        //                                                                .Where(x => x.Status == 4).ToList();
        //                    secondSheet.SetCellValue(startRow, (9 + p), totalPupilStopLearnOnMonth.Count());
        //                }
        //                if (showSheetEthenic)
        //                {
        //                    var totalEthnicStopLearnByReason = totalEthnicStopLearn.Where(x => x.LeavingReasonID == itemReason.LeavingReasonID);
        //                    lstPupilID = totalEthnicStopLearnByReason.Select(x => x.PupilID).ToList();
        //                    totalPupilStopLearnOnMonth = lstPupilOfClassBO.Where(x => x.SchoolID == itemSchool.SchoolProfileID && lstPupilID.Contains(x.PupilID))
        //                                                                .Where(x => x.Status == 4).ToList();
        //                    thirdSheet.SetCellValue(startRow, (9 + p), totalPupilStopLearnOnMonth.Count());
        //                }
        //                if (showSheetFemaleEthenic)
        //                {
        //                    var totalFemaleEthnicStopLearnByReason = totalFemaleEthnicStopLearn.Where(x => x.LeavingReasonID == itemReason.LeavingReasonID);
        //                    lstPupilID = totalFemaleEthnicStopLearnByReason.Select(x => x.PupilID).ToList();
        //                    totalPupilStopLearnOnMonth = lstPupilOfClassBO.Where(x => x.SchoolID == itemSchool.SchoolProfileID && lstPupilID.Contains(x.PupilID))
        //                                                                .Where(x => x.Status == 4).ToList();
        //                    fourthSheet.SetCellValue(startRow, (9 + p), totalPupilStopLearnOnMonth.Count());
        //                }
        //                p++;
        //            }

        //            if (!fillLeavingReason)
        //            {
        //                fillLeavingReason = true;
        //            }

        //            #endregion
        //        }

        //        #region // Tính tổng
        //        if (lstSchoolProfile.Count() > 0)
        //        {
        //            #region// Tổng học sinh đầu năm học
        //            formular = "=SUM(C" + (startFillEducation + 1) + ":C" + (startRow) + ")";
        //            firstSheet.SetCellValue(startFillEducation, 3, formular);
        //            if (showSheetFemale)
        //            {
        //                secondSheet.SetCellValue(startFillEducation, 3, formular);
        //            }
        //            if (showSheetEthenic)
        //            {
        //                thirdSheet.SetCellValue(startFillEducation, 3, formular);
        //            }
        //            if (showSheetFemaleEthenic)
        //            {
        //                fourthSheet.SetCellValue(startFillEducation, 3, formular);
        //            }
        //            #endregion
        //            #region //Số HS cuối tháng (x- 1)
        //            formular = "=SUM(D" + (startFillEducation + 1) + ":D" + (startRow) + ")";
        //            firstSheet.SetCellValue(startFillEducation, 4, formular);
        //            if (showSheetFemale)
        //            {
        //                secondSheet.SetCellValue(startFillEducation, 4, formular);
        //            }
        //            if (showSheetEthenic)
        //            {
        //                thirdSheet.SetCellValue(startFillEducation, 4, formular);
        //            }
        //            if (showSheetFemaleEthenic)
        //            {
        //                fourthSheet.SetCellValue(startFillEducation, 4, formular);
        //            }
        //            #endregion
        //            #region //Số HS cuối tháng (x)
        //            formular = "=SUM(E" + (startFillEducation + 1) + ":E" + (startRow) + ")";
        //            firstSheet.SetCellValue(startFillEducation, 5, formular);
        //            if (showSheetFemale)
        //            {
        //                secondSheet.SetCellValue(startFillEducation, 5, formular);
        //            }
        //            if (showSheetEthenic)
        //            {
        //                thirdSheet.SetCellValue(startFillEducation, 5, formular);
        //            }
        //            if (showSheetFemaleEthenic)
        //            {
        //                fourthSheet.SetCellValue(startFillEducation, 5, formular);
        //            }
        //            #endregion
        //            #region//Số HS chuyển đến tháng (x)
        //            formular = "=SUM(F" + (startFillEducation + 1) + ":F" + (startRow) + ")";
        //            firstSheet.SetCellValue(startFillEducation, 6, formular);
        //            if (showSheetFemale)
        //            {
        //                secondSheet.SetCellValue(startFillEducation, 6, formular);
        //            }
        //            if (showSheetEthenic)
        //            {
        //                thirdSheet.SetCellValue(startFillEducation, 6, formular);
        //            }
        //            if (showSheetFemaleEthenic)
        //            {
        //                fourthSheet.SetCellValue(startFillEducation, 6, formular);
        //            }
        //            #endregion
        //            #region//Số HS chuyển đi tháng (x)
        //            formular = "=SUM(G" + (startFillEducation + 1) + ":G" + (startRow) + ")";
        //            firstSheet.SetCellValue(startFillEducation, 7, formular);
        //            if (showSheetFemale)
        //            {
        //                secondSheet.SetCellValue(startFillEducation, 7, formular);
        //            }
        //            if (showSheetEthenic)
        //            {
        //                thirdSheet.SetCellValue(startFillEducation, 7, formular);
        //            }
        //            if (showSheetFemaleEthenic)
        //            {
        //                fourthSheet.SetCellValue(startFillEducation, 7, formular);
        //            }
        //            #endregion
        //            #region//Số HS thôi học tháng (x)
        //            formular = "=SUM(H" + (startFillEducation + 1) + ":H" + (startRow) + ")";
        //            firstSheet.SetCellValue(startFillEducation, 8, formular);
        //            if (showSheetFemale)
        //            {
        //                secondSheet.SetCellValue(startFillEducation, 8, formular);
        //            }
        //            if (showSheetEthenic)
        //            {
        //                thirdSheet.SetCellValue(startFillEducation, 8, formular);
        //            }
        //            if (showSheetFemaleEthenic)
        //            {
        //                fourthSheet.SetCellValue(startFillEducation, 8, formular);
        //            }
        //            #endregion
        //            #region // Tính tổng lý do nghĩ học
        //            string alpha = string.Empty;
        //            for (int k = 9; k <= (8 + lstLeavingReason.Count()); k++)
        //            {
        //                alpha = UtilsBusiness.GetExcelColumnName(k);
        //                formular = "=SUM(" + alpha + (startFillEducation + 1) + ":" + alpha + (startRow) + ")";
        //                firstSheet.SetCellValue(startFillEducation, k, formular);
        //                if (showSheetFemale)
        //                {
        //                    secondSheet.SetCellValue(startFillEducation, k, formular);
        //                }
        //                if (showSheetEthenic)
        //                {
        //                    thirdSheet.SetCellValue(startFillEducation, k, formular);
        //                }
        //                if (showSheetFemaleEthenic)
        //                {
        //                    fourthSheet.SetCellValue(startFillEducation, k, formular);
        //                }
        //                alpha = string.Empty;
        //            }
        //            #endregion

        //            dicTotal[dicTotal.Count()] = startFillEducation;
        //        }
        //        else
        //        {
        //            for (int k = 3; k <= (8 + lstLeavingReason.Count()); k++)
        //            {
        //                firstSheet.SetCellValue(startFillEducation, k, "0");

        //                if (showSheetFemale)
        //                {
        //                    secondSheet.SetCellValue(startFillEducation, k, "0");
        //                }
        //                if (showSheetEthenic)
        //                {
        //                    thirdSheet.SetCellValue(startFillEducation, k, "0");
        //                }
        //                if (showSheetFemaleEthenic)
        //                {
        //                    fourthSheet.SetCellValue(startFillEducation, k, "0");
        //                }
        //            }
        //        }
        //        #endregion

        //        #region // SetFontStyle, FillColor
        //        firstSheet.GetRange(startFillEducation, 1, startFillEducation, (8 + lstLeavingReason.Count())).SetFontStyle(true, System.Drawing.Color.Black, false, 11, false, false);
        //        firstSheet.GetRange(startFillEducation, 1, startFillEducation, (8 + lstLeavingReason.Count())).FillColor(System.Drawing.Color.LightYellow);

        //        if (showSheetFemale)
        //        {
        //            secondSheet.GetRange(startFillEducation, 1, startFillEducation, (8 + lstLeavingReason.Count())).SetFontStyle(true, System.Drawing.Color.Black, false, 11, false, false);
        //            secondSheet.GetRange(startFillEducation, 1, startFillEducation, (8 + lstLeavingReason.Count())).FillColor(System.Drawing.Color.LightYellow);
        //        }
        //        if (showSheetEthenic)
        //        {
        //            thirdSheet.GetRange(startFillEducation, 1, startFillEducation, (8 + lstLeavingReason.Count())).SetFontStyle(true, System.Drawing.Color.Black, false, 11, false, false);
        //            thirdSheet.GetRange(startFillEducation, 1, startFillEducation, (8 + lstLeavingReason.Count())).FillColor(System.Drawing.Color.LightYellow);
        //        }
        //        if (showSheetFemaleEthenic)
        //        {
        //            fourthSheet.GetRange(startFillEducation, 1, startFillEducation, (8 + lstLeavingReason.Count())).SetFontStyle(true, System.Drawing.Color.Black, false, 11, false, false);
        //            fourthSheet.GetRange(startFillEducation, 1, startFillEducation, (8 + lstLeavingReason.Count())).FillColor(System.Drawing.Color.LightYellow);
        //        }
        //        #endregion

        //        startFillEducation += (lstSchoolProfile.Count() + 1);
        //        startRow++;
        //        index++;
        //    }

        //    #region SetHAlign, SetBorder
        //    firstSheet.GetRange(10, 3, (startRow - 1), (8 + lstLeavingReason.Count())).SetHAlign(VTHAlign.xlHAlignCenter);
        //    firstSheet.GetRange(8, 1, (startRow - 1), (8 + lstLeavingReason.Count())).SetBorder(VTBorderStyle.Solid, VTBorderWeight.Thin, VTBorderIndex.All);

        //    if (showSheetFemale)
        //    {
        //        secondSheet.GetRange(10, 3, (startRow - 1), (8 + lstLeavingReason.Count())).SetHAlign(VTHAlign.xlHAlignCenter);
        //        secondSheet.GetRange(8, 1, (startRow - 1), (8 + lstLeavingReason.Count())).SetBorder(VTBorderStyle.Solid, VTBorderWeight.Thin, VTBorderIndex.All);
        //    }
        //    if (showSheetEthenic)
        //    {
        //        thirdSheet.GetRange(10, 3, (startRow - 1), (8 + lstLeavingReason.Count())).SetHAlign(VTHAlign.xlHAlignCenter);
        //        thirdSheet.GetRange(8, 1, (startRow - 1), (8 + lstLeavingReason.Count())).SetBorder(VTBorderStyle.Solid, VTBorderWeight.Thin, VTBorderIndex.All);
        //    }
        //    if (showSheetFemaleEthenic)
        //    {
        //        fourthSheet.GetRange(10, 3, (startRow - 1), (8 + lstLeavingReason.Count())).SetHAlign(VTHAlign.xlHAlignCenter);
        //        fourthSheet.GetRange(8, 1, (startRow - 1), (8 + lstLeavingReason.Count())).SetBorder(VTBorderStyle.Solid, VTBorderWeight.Thin, VTBorderIndex.All);
        //    }
        //    #endregion

        //    #region// Tính tổng toàn tỉnh
        //    if (districtID.HasValue && districtID.Value > 0)
        //    {
        //        firstSheet.DeleteRow(10);
        //        if (showSheetFemale)
        //        {
        //            secondSheet.DeleteRow(10);
        //        }
        //        if (showSheetEthenic)
        //        {
        //            thirdSheet.DeleteRow(10);
        //        }
        //        if (showSheetFemaleEthenic)
        //        {
        //            fourthSheet.DeleteRow(10);
        //        }
        //    }
        //    else
        //    {
        //        if (lstDistrict.Count() > 0)
        //        {
        //            #region //fill toan tinh
        //            firstSheet.SetCellValue("A10", "Toàn tỉnh");
        //            firstSheet.GetRange(10, 1, 10, 2).Merge();
        //            firstSheet.GetRange(10, 1, 10, 2).SetHAlign(VTHAlign.xlHAlignCenter);
        //            firstSheet.GetRange(10, 1, 10, (8 + lstLeavingReason.Count())).FillColor(System.Drawing.Color.LightYellow);
        //            firstSheet.GetRange(10, 1, 10, (8 + lstLeavingReason.Count())).SetFontStyle(true, System.Drawing.Color.Black, false, 11, false, false);

        //            if (showSheetFemale)
        //            {
        //                secondSheet.SetCellValue("A10", "Toàn tỉnh");
        //                secondSheet.GetRange(10, 1, 10, 2).Merge();
        //                secondSheet.GetRange(10, 1, 10, 2).SetHAlign(VTHAlign.xlHAlignCenter);
        //                secondSheet.GetRange(10, 1, 10, (8 + lstLeavingReason.Count())).FillColor(System.Drawing.Color.LightYellow);
        //                secondSheet.GetRange(10, 1, 10, (8 + lstLeavingReason.Count())).SetFontStyle(true, System.Drawing.Color.Black, false, 11, false, false);
        //            }
        //            if (showSheetEthenic)
        //            {
        //                thirdSheet.SetCellValue("A10", "Toàn tỉnh");
        //                thirdSheet.GetRange(10, 1, 10, 2).Merge();
        //                thirdSheet.GetRange(10, 1, 10, 2).SetHAlign(VTHAlign.xlHAlignCenter);
        //                thirdSheet.GetRange(10, 1, 10, (8 + lstLeavingReason.Count())).FillColor(System.Drawing.Color.LightYellow);
        //                thirdSheet.GetRange(10, 1, 10, (8 + lstLeavingReason.Count())).SetFontStyle(true, System.Drawing.Color.Black, false, 11, false, false);
        //            }
        //            if (showSheetFemaleEthenic)
        //            {
        //                fourthSheet.SetCellValue("A10", "Toàn tỉnh");
        //                fourthSheet.GetRange(10, 1, 10, 2).Merge();
        //                fourthSheet.GetRange(10, 1, 10, 2).SetHAlign(VTHAlign.xlHAlignCenter);
        //                fourthSheet.GetRange(10, 1, 10, (8 + lstLeavingReason.Count())).FillColor(System.Drawing.Color.LightYellow);
        //                fourthSheet.GetRange(10, 1, 10, (8 + lstLeavingReason.Count())).SetFontStyle(true, System.Drawing.Color.Black, false, 11, false, false);
        //            }

        //            string formularTT = "=SUM(C";
        //            string formularTT2 = "=SUM(D";
        //            string formularTT3 = "=SUM(E";
        //            string formularTT4 = "=SUM(F";
        //            string formularTT5 = "=SUM(G";
        //            string formularTT6 = "=SUM(H";
        //            for (int i = 0; i < dicTotal.Count; i++)
        //            {
        //                formularTT += dicTotal[i] + ",C";
        //                formularTT2 += dicTotal[i] + ",D";
        //                formularTT3 += dicTotal[i] + ",E";
        //                formularTT4 += dicTotal[i] + ",F";
        //                formularTT5 += dicTotal[i] + ",G";
        //                formularTT6 += dicTotal[i] + ",H";
        //            }
        //            formularTT = formularTT.Substring(0, formularTT.Length - 2) + ")";
        //            formularTT2 = formularTT2.Substring(0, formularTT2.Length - 2) + ")";
        //            formularTT3 = formularTT3.Substring(0, formularTT3.Length - 2) + ")";
        //            formularTT4 = formularTT4.Substring(0, formularTT4.Length - 2) + ")";
        //            formularTT5 = formularTT5.Substring(0, formularTT5.Length - 2) + ")";
        //            formularTT6 = formularTT6.Substring(0, formularTT6.Length - 2) + ")";
        //            //Toan truong
        //            firstSheet.SetCellValue("C10", formularTT);
        //            firstSheet.SetCellValue("D10", formularTT2);
        //            firstSheet.SetCellValue("E10", formularTT3);
        //            firstSheet.SetCellValue("F10", formularTT4);
        //            firstSheet.SetCellValue("G10", formularTT5);
        //            firstSheet.SetCellValue("H10", formularTT6);

        //            if (showSheetFemale)
        //            {
        //                secondSheet.SetCellValue("C10", formularTT);
        //                secondSheet.SetCellValue("D10", formularTT2);
        //                secondSheet.SetCellValue("E10", formularTT3);
        //                secondSheet.SetCellValue("F10", formularTT4);
        //                secondSheet.SetCellValue("G10", formularTT5);
        //                secondSheet.SetCellValue("H10", formularTT6);
        //            }
        //            if (showSheetEthenic)
        //            {
        //                thirdSheet.SetCellValue("C10", formularTT);
        //                thirdSheet.SetCellValue("D10", formularTT2);
        //                thirdSheet.SetCellValue("E10", formularTT3);
        //                thirdSheet.SetCellValue("F10", formularTT4);
        //                thirdSheet.SetCellValue("G10", formularTT5);
        //                thirdSheet.SetCellValue("H10", formularTT6);
        //            }
        //            if (showSheetFemaleEthenic)
        //            {
        //                fourthSheet.SetCellValue("C10", formularTT);
        //                fourthSheet.SetCellValue("D10", formularTT2);
        //                fourthSheet.SetCellValue("E10", formularTT3);
        //                fourthSheet.SetCellValue("F10", formularTT4);
        //                fourthSheet.SetCellValue("G10", formularTT5);
        //                fourthSheet.SetCellValue("H10", formularTT6);
        //            }

        //            formularTT = string.Empty;
        //            string alpha = string.Empty;
        //            for (int k = 9; k <= (8 + lstLeavingReason.Count()); k++)
        //            {
        //                alpha = UtilsBusiness.GetExcelColumnName(k);
        //                formularTT = "=SUM(" + alpha;
        //                for (int i = 0; i < dicTotal.Count; i++)
        //                {
        //                    formularTT += dicTotal[i] + "," + alpha;
        //                }

        //                formularTT = formularTT.Substring(0, formularTT.Length - 2) + ")";
        //                firstSheet.SetCellValue((alpha + 10), formularTT);

        //                if (showSheetFemale)
        //                {
        //                    secondSheet.SetCellValue((alpha + 10), formularTT);
        //                }
        //                if (showSheetEthenic)
        //                {
        //                    thirdSheet.SetCellValue((alpha + 10), formularTT);
        //                }
        //                if (showSheetFemaleEthenic)
        //                {
        //                    fourthSheet.SetCellValue((alpha + 10), formularTT);
        //                }

        //                alpha = string.Empty;
        //                formularTT = string.Empty;
        //            }
        //            #endregion
        //        }
        //    }
        //    #endregion

        //    if (showLastMonth)
        //    {
        //        #region
        //        firstSheet.SetColumnWidth('D', 0);
        //        if (showSheetFemale)
        //        {
        //            secondSheet.SetColumnWidth('D', 0);
        //        }
        //        if (showSheetEthenic)
        //        {
        //            thirdSheet.SetColumnWidth('D', 0);
        //        }
        //        if (showSheetFemaleEthenic)
        //        {
        //            fourthSheet.SetColumnWidth('D', 0);
        //        }
        //        #endregion
        //    }

        //    firstSheet.SetFontName("Times New Roman", 0);
        //    if (showSheetFemale)
        //    {
        //        secondSheet.SetFontName("Times New Roman", 0);
        //    }
        //    else
        //    {
        //        secondSheet.Delete();
        //    }

        //    if (showSheetEthenic)
        //    {
        //        thirdSheet.SetFontName("Times New Roman", 0);
        //    }
        //    else
        //    {
        //        thirdSheet.Delete();
        //    }

        //    if (showSheetFemaleEthenic)
        //    {
        //        fourthSheet.SetFontName("Times New Roman", 0);
        //    }
        //    else
        //    {
        //        fourthSheet.Delete();
        //    }
        //    #endregion

        //    return oBook.ToStream();
        //}

        private List<PupilOfClassBO> ListPupilOfClassByParameter(List<int> lstClassID, List<int> lstSchoolID, List<int> lstAcademicYearID)
        {
            List<PupilOfClassBO> lstPupilOfClassBO = new List<PupilOfClassBO>();
            lstPupilOfClassBO = (from poc in PupilOfClassBusiness.All
                                 join pf in PupilProfileBusiness.All on poc.PupilID equals pf.PupilProfileID
                                 join pos in PupilOfSchoolRepository.All on poc.SchoolID equals pos.SchoolID
                                 where lstSchoolID.Contains(poc.SchoolID)
                                 && lstAcademicYearID.Contains(poc.AcademicYearID)
                                 && lstClassID.Contains(poc.ClassID)
                                 && pf.IsActive == true
                                 && pos.PupilID == pf.PupilProfileID
                                 select new PupilOfClassBO
                                 {
                                     SchoolID = poc.SchoolID,
                                     AcademicYearID = poc.AcademicYearID,
                                     PupilID = poc.PupilID,
                                     ClassID = poc.ClassID,
                                     PupilFullName = poc.PupilProfile.FullName,
                                     PupilCode = poc.PupilProfile.PupilCode,
                                     Birthday = poc.PupilProfile.BirthDate,
                                     Genre = poc.PupilProfile.Genre,
                                     EthnicName = poc.PupilProfile.Ethnic.EthnicName,
                                     OrderInClass = poc.OrderInClass,
                                     Name = pf.Name,
                                     AssignedDate = poc.AssignedDate,
                                     Status = poc.Status,
                                     EthnicID = pf.EthnicID,
                                     EnrolmentType = pos.EnrolmentType,
                                     EnrolmentDate = pf.EnrolmentDate
                                 }).OrderBy(p => p.OrderInClass.HasValue ? p.OrderInClass : 0)
                                 .ThenBy(p => p.PupilFullName).ThenBy(p => p.Name).ToList();
            return lstPupilOfClassBO;
        }
        #endregion

        #region // TemporaryGraduationPaperReport
        public ProcessedReport GetTemporaryGraduationPaperReport(IDictionary<string, object> dic, string reportCode)
        {
            int classID = Utils.GetInt(dic, "ClassID");
            int academicYearID = Utils.GetInt(dic, "AcademicYearID");
            int schoolID = Utils.GetInt(dic, "SchoolID");
            IDictionary<string, object> dicPR = new Dictionary<string, object>();
            dicPR.Add("ClassID", classID);
            dicPR.Add("AcademicYearID", academicYearID);
            dicPR.Add("SchoolID", schoolID);

            string inputParameterHashKey = GetHashKey(dicPR);
            return ProcessedReportBusiness.GetProcessedReport(reportCode, inputParameterHashKey);
        }

        public ProcessedReport InsertTemporaryGraduationPaperReport(IDictionary<string, object> dic, Stream data, string reportCode)
        {
            int classID = Utils.GetInt(dic, "ClassID");
            int academicYearID = Utils.GetInt(dic, "AcademicYearID");
            int schoolID = Utils.GetInt(dic, "SchoolID");
            string sheetName = Utils.GetString(dic, "SheetName");
            IDictionary<string, object> dicPR = new Dictionary<string, object>();
            dicPR.Add("ClassID", classID);
            dicPR.Add("AcademicYearID", academicYearID);
            dicPR.Add("SchoolID", schoolID);

            ProcessedReport pr = new ProcessedReport();
            ReportDefinition reportDef = ReportDefinitionBusiness.GetByCode(reportCode);
            pr.ReportCode = reportCode;
            pr.ProcessedDate = DateTime.Now;
            pr.InputParameterHashKey = GetHashKey(dicPR);
            pr.ReportData = ReportUtils.Compress(data);

            //Tạo tên file
            string outputNamePattern = reportDef.OutputNamePattern;
            pr.ReportName = ReportUtils.RemoveSpecialCharacters(outputNamePattern) + "_" + sheetName + "." + reportDef.OutputFormat;

            ProcessedReportParameterRepository.Insert(dicPR, pr);
            ProcessedReportRepository.Insert(pr);
            ProcessedReportRepository.Save();
            ProcessedReportParameterRepository.Save();
            return pr;
        }

        public Stream ExportTemporaryGraduationPaper(IDictionary<string, object> dic)
        {
            dic["GraduationLevel"] = 2; // Cap tot nghiep
            dic["EducationLevelID"] = 9; //Lop 9;

            int classID = Utils.GetInt(dic, "ClassID");
            int academicYearID = Utils.GetInt(dic, "AcademicYearID");
            int schoolID = Utils.GetInt(dic, "SchoolID");
            //string pupilCode = Utils.GetString(dic, "PupilCode");
            //string fullName = Utils.GetString(dic, "FullName");
            string content = Utils.GetString(dic, "Content");
            //string Check = Utils.GetString(dic, "Check");
            bool checkAllOnePage = Utils.GetInt(dic, "checkAllOnePage") == 1 ? true : false;
            bool checkAllClass = Utils.GetInt(dic, "checkAllClass") == 1 ? true : false;
            List<int> lstPupilID = Utils.GetIntList(dic, "lstPupilID");

            string strPattern = @"[^a-zA-Z0-9-!@#$%^&()_+~.<>{}';.,|]";
            Regex rgx = new Regex(strPattern);

            string sheetName = "TatCa";
            if (classID > 0)
            {
                ClassProfile objClass = ClassProfileBusiness.Find(classID);
                sheetName = objClass != null ? objClass.DisplayName : "TatCa";
            }
            sheetName = rgx.Replace(ReportUtils.StripVNSign(sheetName), "-");
            sheetName = sheetName.Replace("[", "").Replace("]", "");
            dic.Add("SheetName", sheetName);

            SchoolProfile objSchool = SchoolProfileBusiness.Find(schoolID);
            List<PupilGraduationBO> lstPupilGraduation = PupilGraduationBusiness.SearchPupilGraduation(schoolID, academicYearID, 2, dic);
            //if (lstPupilID.Count() > 0)
            //{
            //    lstPupilGraduation = lstPupilGraduation.Where(x => lstPupilID.Contains(x.PupilID)).ToList();
            //}
            PupilGraduationBO objPupilGraduation = null;

            if (lstPupilGraduation.Count > 0 && lstPupilGraduation != null)
            {
                if (classID > 0)
                {
                    lstPupilGraduation = lstPupilGraduation.OrderBy(p => p.OrderInClass).ThenBy(p => SMAS.Business.Common.Utils.SortABC(p.Name + " " + p.FullName)).ToList();
                }
                else
                {
                    lstPupilGraduation = lstPupilGraduation.OrderBy(p => SMAS.Business.Common.Utils.SortABC(p.Name + " " + p.FullName)).ToList();
                }
            }

            //Đường dẫn template & Tên file xuất ra
            string template = Path.Combine(SystemParamsInFile.TEMPLATE_FOLDER, "HS", SystemParamsInFile.GiayTotNghiepTamThoi + ".xls");
            IVTWorkbook oBook = VTExport.OpenWorkbook(template);

            //Lấy sheet hoc sinh dien chinh sach
            IVTWorksheet firstSheet = oBook.GetSheet(1);
            IVTWorksheet secondSheet = oBook.GetSheet(2);
            IVTWorksheet sheetFill = oBook.CopySheetToBeforeLast(firstSheet);
            sheetFill.Name = sheetName;

            IVTRange rang = firstSheet.GetRange("A2", "S26");

            string day = DateTime.Now.Day.ToString().Length == 1 ? ("0" + DateTime.Now.Day.ToString()) : DateTime.Now.Day.ToString();
            string month = DateTime.Now.Month.ToString().Length == 1 ? ("0" + DateTime.Now.Month.ToString()) : DateTime.Now.Month.ToString();
            string dateString = "{0}, ngày " + day + " tháng " + month + " năm " + DateTime.Now.Year;
            dateString = string.Format(dateString, objSchool.ProvinceID.HasValue ? objSchool.Province.ProvinceName : ".........");

            #region // secondSheet
            secondSheet.SetCellValue("B2", objSchool.SupervisingDept.SupervisingDeptName.ToUpper());
            secondSheet.SetCellValue("B3", objSchool.SchoolName.ToUpper());
            secondSheet.SetCellValue("B11", objSchool.SchoolName);
            secondSheet.SetCellValue("B13", content);
            secondSheet.SetCellValue("B16", dateString);
            secondSheet.SetCellValue("B19", objSchool.HeadMasterName);
            #endregion

            int startRow = 2;
            int startRang = 2;
            string graduationType = "";
            for (int i = 0; i < lstPupilGraduation.Count(); i++)
            {
                objPupilGraduation = lstPupilGraduation[i];
                sheetFill.CopyPasteSameSize(rang, startRang, 1);

                sheetFill.SetCellValue("A" + startRow, "=ThongTinChung!$B$2");
                startRow++;
                sheetFill.SetCellValue("A" + startRow, "=ThongTinChung!$B$3");
                startRow += 2;
                sheetFill.SetCellValue("A" + startRow, "=ThongTinChung!$B$4");
                startRow++;
                sheetFill.SetCellValue("A" + startRow, "=ThongTinChung!$B$5");
                startRow += 2;
                sheetFill.SetCellValue("D" + startRow, objPupilGraduation.FullName);
                sheetFill.SetCellValue("N" + startRow, (objPupilGraduation.Genre == 1 ? "Nam" : "Nữ"));
                startRow++;
                sheetFill.SetCellValue("D" + startRow, objPupilGraduation.BirthDate.ToString("dd/MM/yyyy"));
                sheetFill.SetCellValue("N" + startRow, objPupilGraduation.BirthPlace);
                startRow++;
                sheetFill.SetCellValue("D" + startRow, objPupilGraduation.ClassName);
                sheetFill.SetCellValue("L" + startRow, "=ThongTinChung!$B$11");
                startRow++;
                sheetFill.SetCellValue("D" + startRow, !string.IsNullOrEmpty(objPupilGraduation.TempResidentalAddress) ? objPupilGraduation.TempResidentalAddress : objPupilGraduation.PermanentResidentalAddress);
                startRow++;
                sheetFill.SetCellValue("A" + startRow, "=ThongTinChung!$B$13");
                startRow++;
                graduationType = objPupilGraduation.GraduationGrade == 1 ? "Giỏi" : objPupilGraduation.GraduationGrade == 2 ? "Khá" : objPupilGraduation.GraduationGrade == 3 ? "Trung bình khá" : objPupilGraduation.GraduationGrade == 4 ? "Trung bình" : "";
                sheetFill.SetCellValue("E" + startRow, graduationType);
                startRow += 2;
                sheetFill.SetCellValue("A" + startRow, "=ThongTinChung!$B$15");
                sheetFill.SetCellValue("J" + startRow, "=ThongTinChung!$B$16");
                startRow++;
                sheetFill.SetCellValue("J" + startRow, "=ThongTinChung!$B$17");
                startRow++;
                sheetFill.SetCellValue("J" + startRow, "=ThongTinChung!$B$18");
                startRow += 4;
                sheetFill.SetCellValue("J" + startRow, "=ThongTinChung!$B$19");

                if (i % 2 != 0)
                {
                    sheetFill.SetBreakPage((startRow + 1));
                    startRow += 2;
                }
                else
                {
                    startRow += 7;
                }

                startRang = startRow;

            }

            sheetFill.SetFontName("Times New Roman", 0);
            secondSheet.SetFontName("Times New Roman", 0);
            firstSheet.Delete();
            return oBook.ToStream();
        }
        #endregion
    }
}